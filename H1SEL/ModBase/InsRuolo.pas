//[/TONI20020920\]DEBUGOK
unit InsRuolo;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ExtCtrls, Grids, DBGrids, Db, DBTables, StdCtrls, Buttons, ADODB,
     U_ADOLinkCl, dxTL, dxDBCtrl, dxDBGrid, dxCntner, TB97;

type
     TInsRuoloForm = class(TForm)
          CBInsComp: TCheckBox;
          TRuoli: TADOQuery;
          DsRuoli: TDataSource;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1Ruolo: TdxDBGridMaskColumn;
          dxDBGrid1Area: TdxDBGridMaskColumn;
          Label1: TLabel;
          ERuolo: TEdit;
          Panel1: TPanel;
          BitBtn1: TToolbarButton97;
          BAnnulla: TToolbarButton97;
          SpeedButton1: TToolbarButton97;
          BAreeRuoliITA: TToolbarButton97;
          BAreeRuoliENG: TToolbarButton97;
          Panel2: TPanel;
          SpeedButton2: TToolbarButton97;
          Panel3: TPanel;
          procedure BitBtn1Click(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure BAreeRuoliITAClick(Sender: TObject);
          procedure BAreeRuoliENGClick(Sender: TObject);
          procedure SpeedButton1Click(Sender: TObject);
          procedure SpeedButton2Click(Sender: TObject);
          procedure BAnnullaClick(Sender: TObject);
     private
          xStringa: string;
          { Private declarations }
     public
          { Public declarations }
     end;

var
     InsRuoloForm: TInsRuoloForm;

implementation

uses ModuloDati, uUtilsVarie, Main;

{$R *.DFM}

procedure TInsRuoloForm.BitBtn1Click(Sender: TObject);
begin
     InsRuoloForm.ModalResult := mrOk;
     if TRuoli.RecordCount = 0 then begin
          MessageDlg('Nessun ruolo selezionato !', mtError, [mbOK], 0);
          ModalResult := mrNone;
          Abort;
     end;
end;

procedure TInsRuoloForm.FormShow(Sender: TObject);
begin
     Caption := '[S/12] - ' + caption;
     if (Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 7)) = 'FERRARI') or
          (Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 3)) = 'EBC') then begin
          BAreeRuoliITA.visible := True;
          BAreeRuoliENG.visible := True;
     end;

     //Grafica
     InsRuoloForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel3.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel3.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel3.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     if TRuoli.Active = false then TRuoli.open;
end;

procedure TInsRuoloForm.BAreeRuoliITAClick(Sender: TObject);
begin
     dxDBGrid1Area.FieldName := 'Area';
     dxDBGrid1Ruolo.FieldName := 'Ruolo';
end;

procedure TInsRuoloForm.BAreeRuoliENGClick(Sender: TObject);
begin
     dxDBGrid1Area.FieldName := 'Area_ENG';
     dxDBGrid1Ruolo.FieldName := 'Ruolo_ENG';
end;

procedure TInsRuoloForm.SpeedButton1Click(Sender: TObject);
begin
     TRuoli.Close;
     TRuoli.SQL.text := 'select Mansioni.ID, IDArea, Mansioni.Descrizione Ruolo, Aree.Descrizione Area, ' +
          'Mansioni.Descrizione_ENG Ruolo_ENG, Aree.Descrizione_ENG Area_ENG ' +
          'from Mansioni,Aree where Mansioni.IDArea=Aree.ID and Mansioni.Descrizione like :xTesto ' +
          'order by Aree.Descrizione, Mansioni.Descrizione';
     TRuoli.Parameters[0].Value := '%' + ERuolo.text + '%';
     TRuoli.Open;
end;

procedure TInsRuoloForm.SpeedButton2Click(Sender: TObject);
begin
     ApriHelp('030');
end;

procedure TInsRuoloForm.BAnnullaClick(Sender: TObject);
begin
     InsRuoloForm.ModalResult := mrCancel;
end;

end.

