unit ConoscInfo;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     DBTables, Db, ExtCtrls, dxTL, dxDBCtrl, dxDBGrid, dxCntner, dxDBTLCl,
     dxGrClms, TB97, StdCtrls, Buttons, ADODB, U_ADOLinkCl;

type
     TConoscInfoForm = class(TForm)       
          Panel1: TPanel;
          DsQConoscInfo: TDataSource;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1ID: TdxDBGridMaskColumn;
          BVoceNew: TToolbarButton97;
          BVoceDel: TToolbarButton97;
          BVoceCan: TToolbarButton97;
          BVoceOK: TToolbarButton97;
          BitBtn1: TBitBtn;
          BitBtn2: TBitBtn;
          QConoscInfo: TADOLinkedQuery;
          QConoscInfoID: TAutoIncField;
          QConoscInfoArea: TStringField;
          QConoscInfoSottoArea: TStringField;
          QConoscInfoProprieta: TStringField;
          QConoscInfoProcedura: TStringField;
          QConoscInfoProcedura_ENG: TStringField;
          QConoscInfoSoftware: TStringField;
          dxDBGrid1software: TdxDBGridColumn;
          procedure DsQConoscInfoStateChange(Sender: TObject);
          procedure QConoscInfo_OLDAfterPost(DataSet: TDataSet);
          procedure BVoceNewClick(Sender: TObject);
          procedure BVoceDelClick(Sender: TObject);
          procedure BVoceOKClick(Sender: TObject);
          procedure BVoceCanClick(Sender: TObject);
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
          procedure FormShow(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     ConoscInfoForm: TConoscInfoForm;

implementation

uses ModuloDati;

{$R *.DFM}

procedure TConoscInfoForm.DsQConoscInfoStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsQConoscInfo.State in [dsEdit, dsInsert];
     BVoceNew.Enabled := not b;
     BVoceDel.Enabled := not b;
     BVoceOK.Enabled := b;
     BVoceCan.Enabled := b;
end;

procedure TConoscInfoForm.QConoscInfo_OLDAfterPost(DataSet: TDataSet);
begin
     with QConoscInfo do begin
          //[/TONI20020726\]
          //          Data.DB.BeginTrans;
          Data.DB.BeginTrans;
          try
               //               ApplyUpdates;
               //               Data.DB.CommitTrans;
               Data.DB.CommitTrans;
          except
               //               Data.DB.RollbackTrans;
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               raise;
          end;
          //          CommitUpdates;
          //[/TONI20020726\]FINE
     end;
end;

procedure TConoscInfoForm.BVoceNewClick(Sender: TObject);
begin
     QConoscInfo.Insert;
end;

procedure TConoscInfoForm.BVoceDelClick(Sender: TObject);
begin
     // DA FARE:  controllo integritÓ referenziale
     QConoscInfo.Delete;
end;

procedure TConoscInfoForm.BVoceOKClick(Sender: TObject);
begin
     QConoscInfo.Post;
end;

procedure TConoscInfoForm.BVoceCanClick(Sender: TObject);
begin
     QConoscInfo.Cancel;
end;

procedure TConoscInfoForm.FormClose(Sender: TObject;
     var Action: TCloseAction);
begin
     if DsQConoscInfo.State in [dsInsert, dsEdit] then QConoscInfo.Post;
end;

procedure TConoscInfoForm.FormShow(Sender: TObject);
begin
     QConoscInfo.Open;
end;

end.

