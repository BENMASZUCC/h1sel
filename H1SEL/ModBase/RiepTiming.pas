unit RiepTiming;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, Grids, DBGrids, Db, DBTables, Mask, DBCtrls, ADODB,
     U_ADOLinkCl, TB97, ExtCtrls;

type
     TRiepTimingForm = class(TForm)
          DsQClassifica: TDataSource;
          DBGrid1: TDBGrid;
          Label1: TLabel;
          DsQTotTempo: TDataSource;
          DBEdit1: TDBEdit;
          Label2: TLabel;
          QClassifica: TADOLinkedQuery;
          QTotTempo: TADOLinkedQuery;
          Panel1: TPanel;
          BOK: TToolbarButton97;
          procedure DBGrid1TitleClick(Column: TColumn);
          procedure FormShow(Sender: TObject);
          procedure BOKClick(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     RiepTimingForm: TRiepTimingForm;

implementation

uses ModuloDati, Main;
{$R *.DFM}

procedure TRiepTimingForm.DBGrid1TitleClick(Column: TColumn);
begin
     QClassifica.Close;
     QClassifica.SQL.Text := 'select Progressivo,Descrizione,sum(TotSec) Tot ' +
          'from TimingRic,EBC_Ricerche,EBC_Clienti ' +
          'where TimingRic.IDRicerca=EBC_Ricerche.ID ' +
          '	 and EBC_Ricerche.IDCliente=EBC_Clienti.ID ' +
          //[ALBERTO 20020910]

     'and TimingRic.IDUtente=:xIDutente: ' +

     //[ALBERTO 20020910]FINE
     'group by Progressivo,Descrizione ';
     if Column = DBGrid1.Columns[0] then
          QClassifica.SQL.Add('order by Descrizione');
     if Column = DBGrid1.Columns[1] then
          QClassifica.SQL.Add('order by Progressivo');
     if Column = DBGrid1.Columns[2] then
          QClassifica.SQL.Add('order by Tot desc');
     QClassifica.open;
end;

procedure TRiepTimingForm.FormShow(Sender: TObject);
begin
     Caption := '[M/056] - ' + Caption;

     //Grafici
     RiepTimingForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
end;

procedure TRiepTimingForm.BOKClick(Sender: TObject);
begin
     ModalResult := mrOk;
end;

end.

