unit TestELearning;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ExtCtrls, AdvPanel, dxCntner, dxTL, dxDBCtrl, dxDBGrid, Db, ADODB, TB97,
     StdCtrls, Buttons;

type
     TTestELearningForm = class(TForm)
          AdvPanel1: TAdvPanel;
          dxDBGrid1: TdxDBGrid;
          bIns: TToolbarButton97;
          qTest: TADOQuery;
          dsTest: TDataSource;
          qTestID: TAutoIncField;
          qTestDescrizioneTest: TStringField;
          qTestCodiceTest: TStringField;
          dxDBGrid1ID: TdxDBGridMaskColumn;
          dxDBGrid1DescrizioneTest: TdxDBGridMaskColumn;
          dxDBGrid1CodiceTest: TdxDBGridMaskColumn;
          bDel: TToolbarButton97;
          bCanc: TToolbarButton97;
          bOK: TToolbarButton97;
          BitBtn1: TBitBtn;
          procedure bInsClick(Sender: TObject);
          procedure bOKClick(Sender: TObject);
          procedure bCancClick(Sender: TObject);
          procedure dsTestStateChange(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure bDelClick(Sender: TObject);
     private
    { Private declarations }
     public
    { Public declarations }
     end;

var
     TestELearningForm: TTestELearningForm;

implementation

uses ModuloDati;

{$R *.DFM}

procedure TTestELearningForm.bInsClick(Sender: TObject);
begin
     qTest.Insert;
end;

procedure TTestELearningForm.bOKClick(Sender: TObject);
begin
     qTest.Post;
end;

procedure TTestELearningForm.bCancClick(Sender: TObject);
begin
     qTest.cancel;
end;

procedure TTestELearningForm.dsTestStateChange(Sender: TObject);
var b: boolean;
begin
     b := dsTest.State in [dsEdit, dsInsert];
     bins.Enabled := not b;
     bDel.Enabled := not b;
     bok.Enabled := b;
     bCanc.Enabled := b;

end;

procedure TTestELearningForm.FormShow(Sender: TObject);
begin
     if qTest.Active = false then qTest.open;
end;

procedure TTestELearningForm.bDelClick(Sender: TObject);
begin
     if Messagedlg('Vuoi cancellare l''associzione al test selezionato? Eventuali associazioni del test a candidati verranno eliminate', mtConfirmation, [mbyes, mbNo], 0) = mrYes then begin
          Data.QTemp.close;
          data.Qtemp.sql.text := 'delete from anagtestelearning where idtest=:x0';
          data.qtemp.Parameters[0].Value := qtestID.Value;
          data.qtemp.ExecSQL;
          qtest.delete;
     end;
end;

end.

