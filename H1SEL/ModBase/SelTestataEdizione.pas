unit SelTestataEdizione;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, Grids, DBGrids, Db, DBTables, ADODB, U_ADOLinkCl;

type
     TSelTestataEdizioneForm = class(TForm)
          BitBtn1: TBitBtn;
          BitBtn2: TBitBtn;
          QTestEdiz_OLD: TQuery;
          DsQTestEdiz: TDataSource;
          DBGrid1: TDBGrid;
          QTestEdiz_OLDIDTestata: TIntegerField;
          QTestEdiz_OLDIDEdizione: TAutoIncField;
          QTestEdiz_OLDTestata: TStringField;
          QTestEdiz_OLDEdizione: TStringField;
          QTestEdiz: TADOLinkedQuery;
          procedure FormShow(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     SelTestataEdizioneForm: TSelTestataEdizioneForm;

implementation

{$R *.DFM}
uses ModuloDati;

procedure TSelTestataEdizioneForm.FormShow(Sender: TObject);
begin
     Caption := '[S/40] - ' + Caption;
end;

end.
