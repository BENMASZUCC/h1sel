unit ModificaStoricoAnagrafica;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, ExtCtrls, Mask, DBCtrls,   Db,
     ADODB, SelNazione, TB97, dxCntner, dxEditor, dxExEdtr, dxEdLib,
  dxDBELib;

type
     TModificaStoricoAnagraficaForm = class(TForm)
          Panel1: TPanel;
          Panel2: TPanel;
          GroupBox1: TGroupBox;
          Label1: TLabel;
          DBEdit1: TDBEdit;
          Label2: TLabel;
          DBEdit2: TDBEdit;
          Label4: TLabel;
          Label5: TLabel;
          DBEdit5: TDBEdit;
          Label6: TLabel;
          DBEdit6: TDBEdit;
          Label7: TLabel;
          DBEdit7: TDBEdit;
          Label8: TLabel;
          Label9: TLabel;
          DBEdit4: TDBEdit;
          Label3: TLabel;
          DBEdit3: TDBEdit;
          Label10: TLabel;
          Q: TADOQuery;
          DBLookupComboBox1: TDBLookupComboBox;
          GroupBox2: TGroupBox;
          Label11: TLabel;
          Label12: TLabel;
          DBEdit9: TDBEdit;
          Label13: TLabel;
          DBEdit10: TDBEdit;
          Label14: TLabel;
          DBEdit11: TDBEdit;
          Label15: TLabel;
          DBEdit12: TDBEdit;
          Label16: TLabel;
          DBEdit13: TDBEdit;
          Label17: TLabel;
          DBEdit14: TDBEdit;
          Label18: TLabel;
          DBEdit15: TDBEdit;
          Label19: TLabel;
          DBEdit16: TDBEdit;
          Label20: TLabel;
          DBEdit17: TDBEdit;
          SpeedButton78: TSpeedButton;
          SpeedButton54: TSpeedButton;
          DBComboBox1: TDBComboBox;
          BAnagSelNazione1: TSpeedButton;
          GroupBox3: TGroupBox;
          Label21: TLabel;
          Label22: TLabel;
          Label23: TLabel;
          Label24: TLabel;
          Label25: TLabel;
          Label26: TLabel;
          Label27: TLabel;
          Label28: TLabel;
          Label29: TLabel;
          Label30: TLabel;
          SpeedButton1: TSpeedButton;
          DBEdit8: TDBEdit;
          DBEdit18: TDBEdit;
          DBEdit19: TDBEdit;
          DBEdit20: TDBEdit;
          DBEdit21: TDBEdit;
          DBEdit22: TDBEdit;
          DBEdit23: TDBEdit;
          DBEdit24: TDBEdit;
          DBEdit25: TDBEdit;
          DBComboBox2: TDBComboBox;
          GroupBox4: TGroupBox;
          Label31: TLabel;
          DBEdit26: TDBEdit;
          Label32: TLabel;
          DBEdit27: TDBEdit;
          Label33: TLabel;
          DBEdit28: TDBEdit;
          Label34: TLabel;
          DBEdit29: TDBEdit;
          Label35: TLabel;
          DBEdit30: TDBEdit;
          Label36: TLabel;
          DBEdit31: TDBEdit;
          Label37: TLabel;
          DBEdit32: TDBEdit;
          GroupBox5: TGroupBox;
          Label38: TLabel;
          Label39: TLabel;
          Label40: TLabel;
          DBEdit33: TDBEdit;
          Label41: TLabel;
          DBEdit34: TDBEdit;
          Label42: TLabel;
          DBEdit35: TDBEdit;
          DBEdit36: TDBEdit;
          Label43: TLabel;
          Label44: TLabel;
          BOK: TToolbarButton97;
          Besci: TToolbarButton97;
    dxDBDateEdit1: TdxDBDateEdit;
    DBDateDallaData: TdxDBDateEdit;
    dxDBDateEdit2: TdxDBDateEdit;
    dxDBDateEdit3: TdxDBDateEdit;
    dxDBDateEdit4: TdxDBDateEdit;
          procedure SpeedButton78Click(Sender: TObject);
          procedure SpeedButton54Click(Sender: TObject);
          procedure BAnagSelNazione1Click(Sender: TObject);
          procedure SpeedButton1Click(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure BOKClick(Sender: TObject);
          procedure BesciClick(Sender: TObject);
          procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
     private
    { Private declarations }
     public
    { Public declarations }
          xModalita: string;
     end;

var
     ModificaStoricoAnagraficaForm: TModificaStoricoAnagraficaForm;

implementation

uses StoricoAnagrafica, ModuloDati, Main;

{$R *.DFM}

procedure TModificaStoricoAnagraficaForm.SpeedButton78Click(Sender: TObject);
begin
     SelNazioneForm := TSelNazioneForm.create(self);
     SelNazioneForm.ShowModal;
     if (SelNazioneForm.ModalResult = mrOK) and (SelNazioneForm.QNazioni.RecordCount > 0) then begin
          if not (StoricoAnagraficaForm.DsStoricoAnag.state = dsEdit) then StoricoAnagraficaForm.QStoricoAnag.Edit;
          StoricoAnagraficaForm.QStoricoAnagStatoNascita.Value := SelNazioneForm.QNazioniAbbrev.Value;
          DBEdit7.text := SelNazioneForm.QNazioniAbbrev.Value;
     end;
     SelNazioneForm.Free;
end;

procedure TModificaStoricoAnagraficaForm.SpeedButton54Click(Sender: TObject);
begin
     SelNazioneForm := TSelNazioneForm.create(self);
     SelNazioneForm.ShowModal;
     if (SelNazioneForm.ModalResult = mrOK) and (SelNazioneForm.QNazioni.RecordCount > 0) then begin
          if not (StoricoAnagraficaForm.DsStoricoAnag.state = dsEdit) then StoricoAnagraficaForm.QStoricoAnag.Edit;
          StoricoAnagraficaForm.QStoricoAnagNazionalita.Value := SelNazioneForm.QNazioniDescNazionalita.AsString;
          DBEdit4.text := SelNazioneForm.QNazioniDescNazionalita.AsString;
     end else begin
          if not (StoricoAnagraficaForm.DsStoricoAnag.state = dsEdit) then StoricoAnagraficaForm.QStoricoAnag.Edit;
          StoricoAnagraficaForm.QStoricoAnagNazionalita.Value := '';
          DBEdit4.text := '';
     end;
     SelNazioneForm.Free;
end;

procedure TModificaStoricoAnagraficaForm.BAnagSelNazione1Click(Sender: TObject);
begin
     SelNazioneForm := TSelNazioneForm.create(self);
     SelNazioneForm.ShowModal;
     if (SelNazioneForm.ModalResult = mrOK) and (SelNazioneForm.QNazioni.RecordCount > 0) then begin
          if not (StoricoAnagraficaForm.DsStoricoAnag.state = dsEdit) then StoricoAnagraficaForm.QStoricoAnag.Edit;
          StoricoAnagraficaForm.QStoricoAnagStato.Value := SelNazioneForm.QNazioniAbbrev.Value;
          DBEdit17.text := SelNazioneForm.QNazioniAbbrev.AsString;
     end else begin
          if not (StoricoAnagraficaForm.DsStoricoAnag.state = dsEdit) then StoricoAnagraficaForm.QStoricoAnag.Edit;
          StoricoAnagraficaForm.QStoricoAnagStato.Value := '';
          DBEdit17.text := '';
     end;
     SelNazioneForm.Free;
end;

procedure TModificaStoricoAnagraficaForm.SpeedButton1Click(Sender: TObject);
begin
     SelNazioneForm := TSelNazioneForm.create(self);
     SelNazioneForm.ShowModal;
     if (SelNazioneForm.ModalResult = mrOK) and (SelNazioneForm.QNazioni.RecordCount > 0) then begin
          if not (StoricoAnagraficaForm.DsStoricoAnag.state = dsEdit) then StoricoAnagraficaForm.QStoricoAnag.Edit;
          StoricoAnagraficaForm.QStoricoAnagDomicilioStato.Value := SelNazioneForm.QNazioniAbbrev.Value;
          DBEdit25.text := SelNazioneForm.QNazioniAbbrev.AsString;
     end else begin
          if not (StoricoAnagraficaForm.DsStoricoAnag.state = dsEdit) then StoricoAnagraficaForm.QStoricoAnag.Edit;
          StoricoAnagraficaForm.QStoricoAnagDomicilioStato.Value := '';
          DBEdit25.text := '';
     end;
     SelNazioneForm.Free;
end;

procedure TModificaStoricoAnagraficaForm.FormShow(Sender: TObject);
begin
     // grafica
     Panel1.color := mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.color := mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;
end;

procedure TModificaStoricoAnagraficaForm.BOKClick(Sender: TObject);
begin
     ModificaStoricoAnagraficaForm.ModalResult := mrOk;
end;

procedure TModificaStoricoAnagraficaForm.BesciClick(Sender: TObject);
begin
     ModificaStoricoAnagraficaForm.ModalResult := mrCancel;
end;

procedure TModificaStoricoAnagraficaForm.FormCloseQuery(Sender: TObject;
     var CanClose: Boolean);

begin
     if ModificaStoricoAnagraficaForm.ModalResult = mrOk then begin

          if xModalita = 'UPDATE' then begin
               q.Close;
               q.SQL.Text := 'select ID from StoricoAnagrafica where idanagrafica=' + StoricoAnagraficaForm.QStoricoAnagIDAnagrafica.AsString +
                    ' and id <>' + StoricoAnagraficaForm.QStoricoAnagID.asstring + ' and dalladata=:xdata';
               q.Parameters.ParamByName('xdata').value := DbDateDallaData.Date;
               q.open;
          end;
          if xModalita = 'INSERT' then begin
               q.Close;
               q.SQL.Text := 'select ID from StoricoAnagrafica where idanagrafica=' + StoricoAnagraficaForm.QStoricoAnagIDAnagrafica.AsString +
                    ' and dalladata=:xdata';
               q.Parameters.ParamByName('xdata').value := DbDateDallaData.Date;
               q.open;
          end;

          if q.RecordCount > 0 then begin
               MessageDlg('Esiste gi� una riga di storico con questa data' + chr(13) + 'Impossibile proseguire', mtInformation, [mbOk], 0);
               CanClose := false;
               exit;
          end else
               CanClose := true;


          if DbDateDallaData.Date > StoricoAnagraficaForm.xMaxdata then begin
               MessageDlg('La data inserita risulta maggiore di quella consentita', mtInformation, [mbOk], 0);
               CanClose := false;
          end;

     end;

end;

end.

