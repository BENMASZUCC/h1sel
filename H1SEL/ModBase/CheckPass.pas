unit CheckPass;

interface

function EncodePwd(s: string): string;
function CheckPassword(const s, PwdFromDB: string): boolean;

implementation

uses SysUtils;

{ MD2 Hash stuff }

const MD2_DIGEST_LENGTH = 16;
     MD2_BLOCK = 16;

type MD2Ctx = record
          num: integer;
          data: array[0..MD2_BLOCK - 1] of byte;
          cksm, state: array[0..MD2_BLOCK - 1] of word;
     end;

procedure MD2Final(var md; var c: MD2CTX); forward;
procedure MD2Update(var c: MD2Ctx; Data: PChar; Len: word); forward;
procedure MD2Init(var c: MD2Ctx); forward;

{ MD2 Hash stuff }

{$R-}

procedure MD2Block(var c: MD2CTX; d: PChar); forward;

const S: array[0..255] of word = (
          $29, $2E, $43, $C9, $A2, $D8, $7C, $01, $3D, $36, $54, $A1, $EC, $F0, $06, $13,
          $62, $A7, $05, $F3, $C0, $C7, $73, $8C, $98, $93, $2B, $D9, $BC, $4C, $82, $CA,
          $1E, $9B, $57, $3C, $FD, $D4, $E0, $16, $67, $42, $6F, $18, $8A, $17, $E5, $12,
          $BE, $4E, $C4, $D6, $DA, $9E, $DE, $49, $A0, $FB, $F5, $8E, $BB, $2F, $EE, $7A,
          $A9, $68, $79, $91, $15, $B2, $07, $3F, $94, $C2, $10, $89, $0B, $22, $5F, $21,
          $80, $7F, $5D, $9A, $5A, $90, $32, $27, $35, $3E, $CC, $E7, $BF, $F7, $97, $03,
          $FF, $19, $30, $B3, $48, $A5, $B5, $D1, $D7, $5E, $92, $2A, $AC, $56, $AA, $C6,
          $4F, $B8, $38, $D2, $96, $A4, $7D, $B6, $76, $FC, $6B, $E2, $9C, $74, $04, $F1,
          $45, $9D, $70, $59, $64, $71, $87, $20, $86, $5B, $CF, $65, $E6, $2D, $A8, $02,
          $1B, $60, $25, $AD, $AE, $B0, $B9, $F6, $1C, $46, $61, $69, $34, $40, $7E, $0F,
          $55, $47, $A3, $23, $DD, $51, $AF, $3A, $C3, $5C, $F9, $CE, $BA, $C5, $EA, $26,
          $2C, $53, $0D, $6E, $85, $28, $84, $09, $D3, $DF, $CD, $F4, $41, $81, $4D, $52,
          $6A, $DC, $37, $C8, $6C, $C1, $AB, $FA, $24, $E1, $7B, $08, $0C, $BD, $B1, $4A,
          $78, $88, $95, $8B, $E3, $63, $E8, $6D, $E9, $CB, $D5, $FE, $3B, $00, $1D, $39,
          $F2, $EF, $B7, $0E, $66, $58, $D0, $E4, $A6, $77, $72, $F8, $EB, $75, $4B, $0A,
          $31, $44, $50, $B4, $8F, $ED, $1F, $1A, $DB, $99, $8D, $33, $9F, $11, $83, $14);

procedure MD2Init(var c: MD2Ctx);
begin
     with c do begin
          Num := 0;
          FillChar(State, sizeof(State), 0);
          FillChar(Cksm, sizeof(Cksm), 0);
          FillChar(Data, sizeof(Data), 0);
     end;
end;

procedure MD2Update(var c: MD2Ctx; Data: PChar; Len: word);
var P: PChar;
begin
     if Len = 0 then Exit;
     p := @c.Data;
     if c.Num <> 0 then begin
          if c.Num + Len >= MD2_BLOCK then begin
               move(Data^, p[c.Num], MD2_BLOCK - c.Num);
               MD2Block(c, @c.Data);
               inc(Data, MD2_BLOCK - c.Num);
               dec(Len, MD2_BLOCK - c.Num);
               c.Num := 0;
          end else begin
               move(Data^, p[c.Num], Len);
               inc(c.Num, Len);
               Exit;
          end;
     end;
     while len >= MD2_BLOCK do begin
          MD2Block(c, data);
          inc(data, MD2_BLOCK);
          dec(len, MD2_BLOCK);
     end;
     move(Data^, p^, Len);
     c.Num := Len;
end;

type TWordArray = array[0..0] of word;
     PWord = ^TWordArray;

procedure MD2Block(var c: MD2CTX; d: PChar);
var t: word; sp1, sp2: PWord; i, j: integer; state: array[0..47] of word;
begin
     sp1 := @c.State;
     sp2 := @c.Cksm;
     i := MD2_BLOCK - 1; j := sp2^[i];
     for i := 0 to 15 do begin
          state[i] := sp1^[i];
          t := ord(d[i]);
          state[i + 16] := t;
          state[i + 32] := (t xor sp1^[i]);
          sp2^[i] := sp2^[i] xor S[t xor j];
          j := sp2^[i];
     end;
     t := 0;
     for i := 0 to 17 do begin
          j := 0;
          while j < 48 do begin
               t := state[j] xor S[t];
               state[j] := t;
               inc(j);
          end;
          t := (t + i) and $FF;
     end;
     move(State, sp1^, 16 * sizeof(word));
     FillChar(State, sizeof(State), 0);
end;

procedure MD2Final(var md; var c: MD2CTX);
var i, v: integer; cp: PChar; p1, p2: PWord; Digest: array[0..15] of byte absolute md;
begin
     cp := @c.Data;
     p1 := @c.State;
     p2 := @c.Cksm;
     v := MD2_BLOCK - c.Num;
     for i := c.Num to MD2_BLOCK - 1 do cp[i] := chr(v);
     MD2Block(c, cp);
     for i := 0 to MD2_BLOCK - 1 do cp[i] := chr(p2^[i]);
     MD2Block(c, cp);
     for i := 0 to 15 do Digest[i] := p1^[i] and $FF;
     FillChar(c, sizeof(MD2Ctx), 0);
end;

///////////

function EncodePwd(s: string): string;
var MDC: MD2Ctx; md: array[0..MD2_DIGEST_LENGTH - 1] of byte; i: integer;
begin
     MD2Init(MDC);
     MD2Update(MDC, PChar(s), length(s));
     MD2Final(md, MDC);
     Result := '';
     for i := 0 to MD2_DIGEST_LENGTH - 1 do Result := Result + Format('%x', [md[i]]);
     Result := UpperCase(Result);
end;

function CheckPassword(const s, PwdFromDB: string): boolean;
begin
     CheckPassword := EncodePwd(s) = Uppercase(PwdFromDB);
end;

end.
