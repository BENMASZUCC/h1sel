unit InsCaratt;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, Grids, DBGrids, Db, DBTables, ExtCtrls, ADODB,
     U_ADOLinkCl, TB97;

type
     TInsCarattForm = class(TForm)
          Panel1: TPanel;
          DsCaratt: TDataSource;
          DBGrid1: TDBGrid;
          TCaratt: TADOLinkedTable;
          Panel2: TPanel;
          BitBtn1: TToolbarButton97;
          BAnnulla: TToolbarButton97;
          procedure DBGrid1KeyPress(Sender: TObject; var Key: Char);
          procedure BitBtn1Click(Sender: TObject);
          procedure BAnnullaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
     private
          xStringa: string;
          { Private declarations }
     public
          { Public declarations }
     end;

var
     InsCarattForm: TInsCarattForm;

implementation
uses
     //[/TONI20020729\]
     ModuloDati, Main;
//[/TONI20020729\]FINE
{$R *.DFM}

procedure TInsCarattForm.DBGrid1KeyPress(Sender: TObject; var Key: Char);
begin
     if Key = chr(13) then begin
          BitBtn1.Click;
     end else begin
          xStringa := xStringa + key;
          TCaratt.FindNearestADO(VarArrayOf([xStringa]));
     end;
end;

procedure TInsCarattForm.BitBtn1Click(Sender: TObject);
begin
     InsCarattForm.ModalResult := mrOK;
     {if TCaratt.EOF then begin
          MessageDlg('Nessuna caratteristica selezionata !',mtError, [mbOK],0);
          ModalResult:=mrNone;
          Abort;
     end; }
end;

procedure TInsCarattForm.BAnnullaClick(Sender: TObject);
begin  
     InsCarattForm.ModalResult := mrCancel;
end;

procedure TInsCarattForm.FormShow(Sender: TObject);
begin
//Grafica
     InsCarattForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color; 
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
end;

end.

