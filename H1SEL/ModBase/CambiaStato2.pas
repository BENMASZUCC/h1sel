unit CambiaStato2;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, Grids, DBGrids, Mask, DBCtrls, ExtCtrls, Db, DBTables,
     DtEdit97, ADODB, U_ADOLinkCl, dxTL, dxDBCtrl, dxDBGrid, dxCntner, TB97;

type
     TCambiaStato2Form = class(TForm)
          Panel1: TPanel;
          PanStato: TPanel;
          Label2: TLabel;
          Panel4: TPanel;
          DBGrid1_old: TDBGrid;
          Panel5: TPanel;
          Panel6: TPanel;
          Label1: TLabel;
          Edit1: TEdit;
          Label4: TLabel;
          DEData: TDateEdit97;
          Panel127: TPanel;
          dsQNextStati: TDataSource;
          ECogn: TEdit;
          ENome: TEdit;
          EStato: TEdit;
          QNextStati: TADOLinkedQuery;
          DBGrid1: TdxDBGrid;
          QNextStatiID: TAutoIncField;
          QNextStatiEvento: TStringField;
          QNextStatiIDaStato: TIntegerField;
          QNextStatiAStato: TStringField;
          QNextStatiIDProcedura: TIntegerField;
          QNextStatiIDTipoStatoA: TIntegerField;
          DBGrid1ID: TdxDBGridMaskColumn;
          DBGrid1Evento: TdxDBGridMaskColumn;
          DBGrid1IDaStato: TdxDBGridMaskColumn;
          DBGrid1AStato: TdxDBGridMaskColumn;
          DBGrid1IDProcedura: TdxDBGridMaskColumn;
          DBGrid1IDTipoStatoA: TdxDBGridMaskColumn;
          Panel3: TPanel;
          Panel2: TPanel;
          BOK: TToolbarButton97;
          BAnnulla: TToolbarButton97;
          procedure FormShow(Sender: TObject);
          procedure FormCreate(Sender: TObject);
          procedure BOKClick(Sender: TObject);
          procedure BAnnullaClick(Sender: TObject);
     private
          { Private declarations }
     public
          xIDDaStato: integer;
     end;

var
     CambiaStato2Form: TCambiaStato2Form;

implementation

uses ModuloDati, ModuloDati2, Main, InSelezione, Inserimento, SelData;

{$R *.DFM}

procedure TCambiaStato2Form.FormShow(Sender: TObject);
begin
     //Grafica
     CambiaStato2Form.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel3.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel3.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel3.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel4.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel4.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel4.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel127.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel127.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel127.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel5.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel5.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel5.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel6.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel6.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel6.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     PanStato.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     PanStato.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     PanStato.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     Caption := '[M/050] - ' + Caption;
     QNextStati.SetSQLText(['select EBC_Eventi.ID,EBC_Eventi.Evento,EBC_Eventi.IDaStato,' +
          'EBC_Stati.Stato AStato,EBC_Eventi.IDProcedura,EBC_Stati.IDTipoStato IDTipoStatoA ' +
               ' from EBC_Eventi join EBC_Stati on EBC_Eventi.IDaStato=EBC_Stati.ID where EBC_Eventi.IDdaStato=:xDaStato: ' +
               ' and EBC_Stati.IDTipoStato not in (6,7,11,15)' +
               ' order by ordinamento',
               xIDDaStato]);
     QNextStati.Open;

     if pos('EMERGENCY', UpperCase(data.Global.fieldbyname('NomeAzienda').asString)) > 0 then
          Panel6.Visible := true;

end;

procedure TCambiaStato2Form.FormCreate(Sender: TObject);
begin
     DoubleBuffered := True;
end;

procedure TCambiaStato2Form.BOKClick(Sender: TObject);
begin
     CambiaStato2Form.ModalResult := mrOk;
end;

procedure TCambiaStato2Form.BAnnullaClick(Sender: TObject);
begin
     CambiaStato2Form.ModalResult := mrCancel;
end;

end.

