unit ModStoricoAnagPos;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, DtEdit97;

type
     TModStoricoAnagPosForm = class(TForm)
          BitBtn1: TBitBtn;
          BitBtn2: TBitBtn;
          Label3: TLabel;
          ENote: TEdit;
          DEAllaData: TDateEdit97;
          GroupBox1: TGroupBox;
          SpeedButton1: TSpeedButton;
          ECogn2: TEdit;
          ENome2: TEdit;
          GroupBox2: TGroupBox;
          SpeedButton2: TSpeedButton;
          ECogn1: TEdit;
          ENome1: TEdit;
          DEDallaData: TDateEdit97;
          Label1: TLabel;
          Label2: TLabel;
          procedure SpeedButton2Click(Sender: TObject);
          procedure SpeedButton1Click(Sender: TObject);
          procedure FormCreate(Sender: TObject);
          procedure BitBtn1Click(Sender: TObject);
     private
          { Private declarations }
     public
          xIDAnagrafica, xIDAutorizzatoDa: integer;
     end;

var
     ModStoricoAnagPosForm: TModStoricoAnagPosForm;

implementation

uses ElencoDip;

{$R *.DFM}

procedure TModStoricoAnagPosForm.SpeedButton2Click(Sender: TObject);
begin
     ElencoDipForm := TElencoDipForm.create(self);
     ElencoDipForm.ShowModal;
     if ElencoDipForm.ModalResult = mrOK then begin
          xIDAnagrafica := ElencoDipForm.TAnagDip.FieldByName('ID').AsInteger;
          ECogn1.Text := ElencoDipForm.TAnagDip.FieldByName('Cognome').AsString;
          ENome1.Text := ElencoDipForm.TAnagDip.FieldByName('Nome').AsString;
     end;
     ElencoDipForm.Free;
end;

procedure TModStoricoAnagPosForm.SpeedButton1Click(Sender: TObject);
begin
     ElencoDipForm := TElencoDipForm.create(self);
     ElencoDipForm.ShowModal;
     if ElencoDipForm.ModalResult = mrOK then begin
          xIDAutorizzatoDa := ElencoDipForm.TAnagDip.FieldByName('ID').AsInteger;
          ECogn2.Text := ElencoDipForm.TAnagDip.FieldByName('Cognome').AsString;
          ENome2.Text := ElencoDipForm.TAnagDip.FieldByName('Nome').AsString;
     end;
     ElencoDipForm.Free;
end;

procedure TModStoricoAnagPosForm.FormCreate(Sender: TObject);
begin
     xIDAnagrafica := 0;
     xIDAutorizzatoDa := 0;
     DEDallaData.date := date;
     DEAllaData.date := date;
end;

procedure TModStoricoAnagPosForm.BitBtn1Click(Sender: TObject);
begin
     if xIDAnagrafica = 0 then begin
          ModalResult := mrNone;
          Showmessage('� necessario specificare il soggetto');
          Abort;
     end;
end;

end.
