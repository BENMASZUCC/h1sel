unit uSendMailMAPI;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, ShellAPI,
     StdCtrls, Buttons, ExtCtrls, AFQuickMail;

function SendEMailMAPI(xSubject, xTesto, xDestinatari, xDestBcc, xAllegati: string; xRemote, xDebug, xShowDialog: boolean): boolean;

implementation

function TS_ShellExecute(hwnd: integer; lpOperation, lpFile, lpParameters, lpDirectory: widestring; nShowCmd: integer): integer; cdecl; external 'H1TS_Server.dll';


function SendEMailMAPI(xSubject, xTesto, xDestinatari, xDestBcc, xAllegati: string; xRemote, xDebug, xShowDialog: boolean): boolean;
var xMail: TAFQuickMail;
     xComando_ws, xParameters_ws: string;
     res: integer;
     xErrorMsg: string;
begin
     if xRemote then begin

          // --- REMOTO ------------------------------

          xComando_ws := 'c:\temp\InvioMailMAPI.exe'; // ### >>> quale cartella ?
          if not inputQuery('DEBUG', 'Comando_ws', xComando_ws) then exit;

          if xDebug then
               xParameters_ws := '"' + xSubject + '" "' + xTesto + '" "' + xDestinatari + '" "' + xDestBcc + '" "' + xAllegati + '" "1"'
          else
               xParameters_ws := '"' + xSubject + '" "' + xTesto + '" "' + xDestinatari + '" "' + xDestBcc + '" "' + xAllegati + '" "0"';

          // chiamata remota
          res := TS_ShellExecute(0, 'Open', xComando_ws, xParameters_ws, '', SW_SHOW);

          if (res < 0) or (res > 32) then
               Result := True
          else begin

               case res of
                    0: xErrorMsg := '0 = The operating system is out of memory or resources.';
                    1: xErrorMsg := '1 = Non riesco ad aprire il virtual channel (TS_ShellExecute)';
                    //2: xErrorMsg := '2 = The specified file was not found';
                    2: xErrorMsg := '2 = il virtual channel si chiude prima di inviare i dati (TS_ShellExecute)';
                    //3: xErrorMsg := '3 = The specified path was not found.';
                    3: xErrorMsg := '3 = fallito l''invio dei dati al client (TS_ShellExecute)';

                    5: xErrorMsg := '5 = Windows 95 only: The operating system denied access to the specified file';
                    8: xErrorMsg := '8 = Windows 95 only: There was not enough memory to complete the operation.';
                    10: xErrorMsg := '10 = Wrong Windows version';
                    11: xErrorMsg := '11 = The.EXE file is invalid(non - Win32.EXE or error in .EXE image).';
                    12: xErrorMsg := '12 = Application was designed for a different operating system.';
                    13: xErrorMsg := '13 = Application was designed for MS - DOS 4.0';
                    15: xErrorMsg := '15 = Attempt to load a real - mode program';
                    16: xErrorMsg := '16 = Attempt to load a second instance of an application with non - readonly data segments';
                    19: xErrorMsg := '19 = Attempt to load a compressed application file';
                    20: xErrorMsg := '20 = Dynamic - link library(DLL)file failure".';
                    26: xErrorMsg := '26 = A sharing violation occurred.';
                    27: xErrorMsg := '27 = The filename association is incomplete or invalid.';
                    28: xErrorMsg := '28 = The DDE transaction could not be completed because the request timed out.';
                    29: xErrorMsg := '29 = The DDE transaction failed.';
                    30: xErrorMsg := '30 = The DDE transaction could not be completed because other DDE transactions were being processed.';
                    31: xErrorMsg := '31 = There is no application associated with the given filename extension.';
                    32: xErrorMsg := '32 = Windows 95 only: The specified dynamic - link library was not found.';

               end;

               MessageDlg('ERRORE CONNESSIONE REMOTA: ' + chr(13) + xErrorMsg, mtError, [mbOK], 0);

               Result := False;
          end; 

     end else begin
          // --- LOCALE ------------------------------

          xMail := TAFQuickMail.create(nil);

          // oggetto
          xMail.Subject := xSubject;
          // destinatari
          xMail.ToEmail.text := xDestinatari;
          // Bcc
          if xDestBcc='-' then xMail.ToBCCEmail.text:=''
          else xMail.ToBCCEmail.text := xDestBcc;
          // testo
          xMail.TextToSend.text := xTesto;
          // allegati
          if xAllegati='-' then xMail.FileNames.text:=''
          else xMail.FileNames.text := xAllegati;
          // dialog
          xMail.ShowDialog := xShowDialog;

          Result := xMail.Execute;

          xmail.free;

     end;
end;

end.

