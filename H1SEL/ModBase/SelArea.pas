unit SelArea;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, Grids, DBGrids, Db, DBTables, ADODB, U_ADOLinkCl,
  TB97, ExtCtrls;

type
     TSelAreaForm = class(TForm)
          DBGrid1: TDBGrid;
          DsQAree: TDataSource;
          QAree: TADOLinkedQuery;
          Panel1: TPanel;
          BitBtn1: TToolbarButton97;
          BitBtn2: TToolbarButton97;
          procedure FormShow(Sender: TObject);
          procedure BitBtn1Click(Sender: TObject);
          procedure BitBtn2Click(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     SelAreaForm: TSelAreaForm;

implementation

uses ModuloDati, ModuloDati2, Main;

{$R *.DFM}

procedure TSelAreaForm.FormShow(Sender: TObject);
begin
     //Grafica
     SelAreaForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     Caption := '[S/34] - ' + Caption;
end;

procedure TSelAreaForm.BitBtn1Click(Sender: TObject);
begin
     SelAreaForm.ModalResult := mrOk;
end;

procedure TSelAreaForm.BitBtn2Click(Sender: TObject);
begin
     SelAreaForm.ModalResult := mrCancel;
end;

end.

