unit AgendaSett;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, Spin, Grids, AdvGrid, ImgList, ExtCtrls, Db, DBTables, asPrev,
     DBGrids, TB97, Menus, ComCtrls, CandUtente, dxPSCore, dxPSdxTLLnk,
     dxPSdxDBGrLnk, dxDBTLCl, dxGrClms, dxTL, dxDBCtrl, dxDBGrid, dxCntner,
     dxPSdxDBCtrlLnk, ADODB, U_ADOLinkCl, BaseGrid;

type
     TAgendaSettForm = class(TForm)
          ImageList1: TImageList;
          DsUsers: TDataSource;
          PM1: TPopupMenu;
          modificaimpegno1: TMenuItem;
          nuovoimpegno1: TMenuItem;
          eliminaimpegno1: TMenuItem;
          Panel5: TPanel;
          BEsci: TToolbarButton97;
          PM2: TPopupMenu;
          vedispostamessaggio1: TMenuItem;
          nuovomessaggio1: TMenuItem;
          eliminamessaggio1: TMenuItem;
          PageControl1: TPageControl;
          TSAgenda: TTabSheet;
          TSCandidati: TTabSheet;
          Panel1: TPanel;
          Panel2: TPanel;
          Panel3: TPanel;
          BNuovoImpegno: TToolbarButton97;
          BCancella: TToolbarButton97;
          BSpostaImpegno: TToolbarButton97;
          BGestisciRic: TToolbarButton97;
          ASG1: TAdvStringGrid;
          Panel4: TPanel;
          Panel6: TPanel;
          ASG2: TAdvStringGrid;
          Panel7: TPanel;
          BNuovoProm: TToolbarButton97;
          BEliminaProm: TToolbarButton97;
          BModProm: TToolbarButton97;
          BSettPrec: TToolbarButton97;
          BSettSucc: TToolbarButton97;
          Splitter1: TSplitter;
          Panel8: TPanel;
          DsQCandidati: TDataSource;
          Label2: TLabel;
          ToolbarButton971: TToolbarButton97;
          ToolbarButton972: TToolbarButton97;
          TSAgendaRis: TTabSheet;
          Panel9: TPanel;
          BSettPrec1: TToolbarButton97;
          BSettSucc1: TToolbarButton97;
          Panel10: TPanel;
          Panel11: TPanel;
          ASGRis: TAdvStringGrid;
          PanUtenti: TPanel;
          Label1: TLabel;
          DBGrid1: TDBGrid;
          PanRisorse: TPanel;
          Label3: TLabel;
          DsRisorse: TDataSource;
          DBGrid2: TDBGrid;
          BAgendaStampa: TToolbarButton97;
          BSpostaImpegnoRis: TToolbarButton97;
          ToolbarButton973: TToolbarButton97;
          ToolbarButton974: TToolbarButton97;
          ToolbarButton975: TToolbarButton97;
          TSCandUtente: TTabSheet;
          CandUtenteFrame1: TCandUtenteFrame;
          BArchiviaProm: TToolbarButton97;
          BArchivioProm: TToolbarButton97;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1Rif: TdxDBGridMaskColumn;
          dxDBGrid1Cliente: TdxDBGridMaskColumn;
          dxDBGrid1Ruolo: TdxDBGridMaskColumn;
          dxDBGrid1Nominativo: TdxDBGridMaskColumn;
          dxDBGrid1Stato: TdxDBGridMaskColumn;
          dxDBGrid1DataIns: TdxDBGridDateColumn;
          BSituazCandStampa: TToolbarButton97;
          dxPrinter1: TdxComponentPrinter;
          dxPrinter1Link1: TdxDBGridReportLink;
          QAgenda: TADOLinkedQuery;
          Q: TADOLinkedQuery;
          QAgendaABS: TADOLinkedQuery;
          QUsers: TADOLinkedQuery;
          QCandidati: TADOLinkedQuery;
          QRisorseDisp: TADOLinkedQuery;
          QCheck: TADOLinkedQuery;
          QPromemoria: TADOLinkedQuery;
          TRisorse: TADOLinkedQuery;
          TRisorseABS: TADOLinkedTable;
          TPromABS: TADOLinkedTable;
          TUsersLK: TADOLinkedTable;
          TRisorseID: TAutoIncField;
          TRisorseRisorsa: TStringField;
          TRisorseColor: TStringField;
          QRisorseDispID: TAutoIncField;
          QRisorseDispData: TDateTimeField;
          QRisorseDispOre: TDateTimeField;
          QRisorseDispIDUtente: TIntegerField;
          QRisorseDispTipo: TSmallintField;
          QRisorseDispIDCandRic: TIntegerField;
          QRisorseDispDescrizione: TStringField;
          QRisorseDispIDRisorsa: TIntegerField;
          QRisorseDispAlleOre: TDateTimeField;
          QRisorseDispNominativo: TStringField;
          PMExport: TPopupMenu;
          Stampagriglia1: TMenuItem;
          EsportainExcel1: TMenuItem;
          QCandidatiIDRicerca: TAutoIncField;
          QCandidatiRif: TStringField;
          QCandidatiCliente: TStringField;
          QCandidatiRuolo: TStringField;
          QCandidatiNominativo: TStringField;
          QCandidatiStato: TStringField;
          QCandidatiDataIns: TDateTimeField;
          QCandidatiID: TAutoIncField;
          QCandidatiIDUtente: TIntegerField;
          CBCommAttive: TCheckBox;
          QTemp: TADOQuery;
          Barray: TButton;
          MyRisorse: TADOQuery;
          SpeedButton1: TToolbarButton97;
          procedure ASG1ClickCell(Sender: TObject; Arow, Acol: Integer);
          procedure ASG1GetAlignment(Sender: TObject; ARow, ACol: Integer;
               var AAlignment: TAlignment);
          procedure ASG1GetCellColor(Sender: TObject; ARow, ACol: Integer;
               AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
          procedure FormShow(Sender: TObject);
          procedure TUsersAfterScroll(DataSet: TDataSet);
          procedure ASG2GetAlignment(Sender: TObject; ARow, ACol: Integer;
               var AAlignment: TAlignment);
          procedure ASG2GetCellColor(Sender: TObject; ARow, ACol: Integer;
               AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
          procedure BSettPrecClick(Sender: TObject);
          procedure BSettSuccClick(Sender: TObject);
          procedure BNuovoImpegnoClick(Sender: TObject);
          procedure BCancellaClick(Sender: TObject);
          procedure BSpostaImpegnoClick(Sender: TObject);
          procedure BEsciClick(Sender: TObject);
          procedure modificaimpegno1Click(Sender: TObject);
          procedure nuovoimpegno1Click(Sender: TObject);
          procedure eliminaimpegno1Click(Sender: TObject);
          procedure ASG1DblClick(Sender: TObject);
          procedure BModPromClick(Sender: TObject);
          procedure vedispostamessaggio1Click(Sender: TObject);
          procedure nuovomessaggio1Click(Sender: TObject);
          procedure eliminamessaggio1Click(Sender: TObject);
          procedure ASG2ClickCell(Sender: TObject; Arow, Acol: Integer);
          procedure ASG2DblClick(Sender: TObject);
          procedure BNuovoPromClick(Sender: TObject);
          procedure BEliminaPromClick(Sender: TObject);
          procedure BGestisciRicClick(Sender: TObject);
          procedure PageControl1Change(Sender: TObject);
          procedure ToolbarButton971Click(Sender: TObject);
          procedure ToolbarButton972Click(Sender: TObject);
          procedure ASGRisClickCell(Sender: TObject; Arow, Acol: Integer);
          procedure ASGRisGetAlignment(Sender: TObject; ARow, ACol: Integer;
               var AAlignment: TAlignment);
          procedure TRisorse_OLDAfterScroll(DataSet: TDataSet);
          procedure BSettPrec1Click(Sender: TObject);
          procedure BSettSucc1Click(Sender: TObject);
          procedure ASGRisGetCellColor(Sender: TObject; ARow, ACol: Integer;
               AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
          procedure FormCreate(Sender: TObject);
          procedure QUsers_OLDAfterScroll(DataSet: TDataSet);
          procedure BAgendaStampaClick(Sender: TObject);
          procedure BSpostaImpegnoRisClick(Sender: TObject);
          procedure ToolbarButton974Click(Sender: TObject);
          procedure ToolbarButton975Click(Sender: TObject);
          procedure BArchiviaPromClick(Sender: TObject);
          procedure BArchivioPromClick(Sender: TObject);
          procedure Stampagriglia1Click(Sender: TObject);
          procedure EsportainExcel1Click(Sender: TObject);
          procedure SpeedButton1Click(Sender: TObject);
          procedure CBCommAttiveClick(Sender: TObject);
          procedure BarrayClick(Sender: TObject);
          procedure QAgendaAfterScroll(DataSet: TDataSet);
          procedure CandUtenteFrame1BLegendaClick(Sender: TObject);
     private
          { Private declarations }
          carica: boolean;
          xColonnaSel: TColumn;
          procedure InsMessaggi;
          procedure InsImpegniAgenda;
          procedure InsImpegniAgendaRis;
          procedure ColonneOre;
          procedure ColonneOreRis;
          function LogEsportazioniCand: boolean;
     public
          xSelRow, xSelCol, xSelRow2, xSelCol2, xSelRowRis, xSelColRis, xIDSel: integer;
          xPrimaData: TDateTime;
          xGiorni: array[1..6] of integer;
          xOre: array[1..50] of integer;
          xArrayCol: array[1..6, 1..50] of string;
          xArrayIDAgenda: array[1..6, 1..50] of integer;
          xArrayIDProm: array[1..6, 1..20] of integer;
          procedure ComponiGriglia;
          procedure ComponiGrigliaRis;
     end;

var
     AgendaSettForm: TAgendaSettForm;

implementation

uses NuovoApp, Sposta, ModMessaggio, InsMessaggio, SelPers, MDRicerche,
     ModuloDati2, ModuloDati, uUtilsVarie, RepAgenda, Main, ArchivioMess;

{$R *.DFM}

procedure TAgendaSettForm.ASG1ClickCell(Sender: TObject; Arow,
     Acol: Integer);
begin
     xSelRow := ARow;
     xSelCol := Acol;
     if data.global.FieldByName('debughrms').asboolean = true then showmessage('clik su griglia: ' + 'xSelRow=' + inttostr(xSelRow) + ' xSelCol=' + inttostr(xSelCol));
end;

procedure TAgendaSettForm.ASG1GetAlignment(Sender: TObject; ARow,
     ACol: Integer; var AAlignment: TAlignment);
begin
     if (ACol = 0) or (ARow = 0) then AAlignment := taCenter;
end;

procedure TAgendaSettForm.ASG1GetCellColor(Sender: TObject; ARow,
     ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
var i, k: integer;
begin
     if (ARow > 0) and (ACol > 0) then
          ABrush.Color := clWindow;
     if ARow = 0 then
          afont.style := [fsBold];
     for i := 1 to ASG1.ColCount do begin
          for k := 1 to ASG1.RowCount do begin
               if xArrayCol[i, k] <> '' then begin
                    if (ACol = i) and (ARow = k) then
                         if xArrayCol[i, k] = 'clLime' then ABrush.Color := clLime
                         else
                              if xArrayCol[i, k] = 'clRed' then ABrush.Color := clRed
                              else
                                   if xArrayCol[i, k] = 'clYellow' then ABrush.Color := clYellow
                                   else
                                        if xArrayCol[i, k] = 'clAqua' then ABrush.Color := clAqua;
               end;
          end;
     end;
end;

procedure TAgendaSettForm.InsImpegniAgenda;
var xAnno, xMese, xGiorno, xOra, xMin, xSec, xMSec, xOra2, xMin2, xSec2, xMSec2: Word;
     k, i, h, j, xRiga, xColonna: integer;
     xMinStr, xMinStr2, xDesc: string;
begin
     ShortTimeFormat := 'hh.mm';
     //[/TONI20020724\]
{     QAgenda.Prepare;
     QAgenda.Params[0].AsDateTime:=xPrimaData;
     QAgenda.Params[1].AsDateTime:=xPrimaData+5;
     QAgenda.Params[2].Value:=QUsersID.Value;}

     if xPrimaData > 0 then
     begin
          QAgenda.Close;
          QAgenda.ReloadSQL;
          //[/TONI20020724\]FINE
          QAgenda.ParamByName['xDal'] := datetostr(xPrimaData);
          QAgenda.ParamByName['xAl'] := DateToStr(xPrimaData + 5);
          QAgenda.ParamByName['xUtente'] := Qusers.FieldByName('ID').AsInteger;
          QAgenda.Open;
          if data.Global.fieldbyname('debughrms').asboolean = true then qagenda.sql.SaveToFile('QAgenda_' + Qusers.FieldByName('ID').asstring + '.sql');
          //showmessage(inttostr(QAgenda.RecordCount));
     end;
     for i := 1 to 6 do
          for k := 1 to ASG1.RowCount do xArrayCol[i, k] := '';
     // repulisti array IDagenda
     for i := 1 to 6 do
          for k := 1 to 50 do xArrayIDAgenda[i, k] := 0;


     while not QAgenda.EOF do
     begin
          //[/TONI20020724\]
          with QAgenda do
          begin
               {DecodeDate(QAgendaData.Value,xAnno,xMese,xGiorno);
               DecodeTime(QAgendaOre.Value,xOra,xMin,xSec,xMSec);
               DecodeTime(QAgendaAlleOre.Value,xOra2,xMin2,xSec2,xMSec2);}
               DecodeDate(FieldByName('Data').AsDateTime, xAnno, xMese, xGiorno);
               DecodeTime(FieldByName('Ore').AsDateTime, xOra, xMin, xSec, xMSec);
               DecodeTime(FieldByName('AlleOre').AsDateTime, xOra2, xMin2, xSec2, xMSec2);
               //[/TONI20020724\]FINE
               if xMin < 10 then xMinStr := '0' + IntToStr(xMin)
               else xMinStr := IntToStr(xMin);
               if xMin2 < 10 then xMinStr2 := '0' + IntToStr(xMin2)
               else xMinStr2 := IntToStr(xMin2);
               // cerca colonna giusta
               for k := 1 to 6 do if xGiorni[k] = xGiorno then begin xColonna := k; break; end;
               // cerca riga giusta
               for h := 1 to 50 do if xore[h] = xOra then begin xRiga := h; break; end;

               while ASG1.Cells[xColonna, xRiga] <> '' do begin
                    if xOre[xRiga + 1] <> xOra then
                         ASG1.InsertRows(xRiga + 1, 1);
                    for i := 0 to ASG1.RowCount - 1 do ASG1.RowHeights[i] := 16;
                    inc(xRiga);
               end;

               //[/TONI20020724\]
     //          ASG1.AddImageIdx(xColonna,xRiga,QAgendaTipo.Value,habeforetext,vaCenter);
               ASG1.AddImageIdx(xColonna, xRiga, FieldByName('Tipo').Value, habeforetext, vaCenter);
               {          if QAgendaIDCandRic.AsString='' then begin
                              if xOra2>0 then
                                   ASG1.Cells[xColonna,xRiga]:=IntToStr(xOra)+'.'+xMinStr+'-'+IntToStr(xOra2)+'.'+xMinStr2+' '+LowerCase(QAgendaDescrizione.Value)
                              else ASG1.Cells[xColonna,xRiga]:=IntToStr(xOra)+'.'+xMinStr+' '+LowerCase(QAgendaDescrizione.Value);
                              xArrayIDAgenda[xColonna,xRiga]:=QAgendaID.Value}
               if FieldByname('IDCandRic').AsString = '' then begin
                    if xOra2 > 0 then
                         ASG1.Cells[xColonna, xRiga] := IntToStr(xOra) + '.' + xMinStr + '-' + IntToStr(xOra2) + '.' + xMinStr2 + ' ' + LowerCase(FieldBYname('Descrizione').Value)
                    else ASG1.Cells[xColonna, xRiga] := IntToStr(xOra) + '.' + xMinStr + ' ' + LowerCase(FieldByName('Descrizione').Value);
                    if data.Global.fieldbyname('debughrms').asboolean = true then showmessage('valorizzo xArrayIDAgenda=' + FieldByName('ID').asstring + chr(13) + 'xcolonna=' + inttostr(xColonna) + ' xriga=' + inttostr(xriga));
                    xArrayIDAgenda[xColonna, xRiga] := FieldByName('ID').Value
               end else begin
                    Q.Close;
                    {               Q.SQL.text:='select EBC_Ricerche.Progressivo,EBC_Clienti.Descrizione Cliente '+
                                        'from EBC_CandidatiRicerche,EBC_Ricerche,EBC_Clienti '+
                                        'where EBC_CandidatiRicerche.IDRicerca=EBC_Ricerche.ID '+
                                        'and EBC_Ricerche.IDCliente=EBC_Clienti.ID and EBC_CandidatiRicerche.ID='+QAgendaIDCandRic.asString;}
                    Q.SQL.text := 'select EBC_Ricerche.Progressivo,EBC_Clienti.Descrizione Cliente ' +
                         'from EBC_CandidatiRicerche,EBC_Ricerche,EBC_Clienti ' +
                         'where EBC_CandidatiRicerche.IDRicerca=EBC_Ricerche.ID ' +
                         'and EBC_Ricerche.IDCliente=EBC_Clienti.ID and EBC_CandidatiRicerche.ID=' + FieldByName('IDCandRic').asString;
                    Q.Open;

                    //               xDesc:=LowerCase(QAgendaDescrizione.Value)+' (Rif.'+Q.FieldByName('Progressivo').asString+') '+
                    //                    Q.FieldByName('Cliente').asString;
                    xDesc := LowerCase(FieldByName('Descrizione').Value) + ' (Rif.' + Q.FieldByName('Progressivo').asString + ') ' +
                         Q.FieldByName('Cliente').asString;
                    if xOra2 > 0 then
                         ASG1.Cells[xColonna, xRiga] := IntToStr(xOra) + '.' + xMinStr + '-' + IntToStr(xOra2) + '.' + xMinStr2 + ' ' + xDesc
                    else ASG1.Cells[xColonna, xRiga] := IntToStr(xOra) + '.' + xMinStr + ' ' + xDesc;
                    //               xArrayIDAgenda[xColonna,xRiga]:=QAgendaID.Value;
                    if data.Global.fieldbyname('debughrms').asboolean = true then showmessage('valorizzo xArrayIDAgenda=' + FieldByName('ID').asstring + chr(13) + 'xcolonna=' + inttostr(xColonna) + ' xriga=' + inttostr(xriga));
                    xArrayIDAgenda[xColonna, xRiga] := FieldByName('ID').Value;
               end;
               // colore a seconda della risorsa
               TRisorseABS.Open;
               //          if QAgendaIDRisorsa.asString<>'' then begin
               if FieldByName('IDRisorsa').asString <> '' then begin
                    //               if TRisorseABS.FindKey([FieldByName ('IDRisorsa').AsInteger]) then
                    if TRisorseABS.Locate('ID', FieldByName('IDRisorsa').AsInteger, []) then
                         xArrayCol[xColonna, xRiga] := TrimRight(TRisorseABS.FieldByName('Color').AsString);
               end;
               TRisorseABS.Close;
               xSelRow := xRiga;
               xSelCol := xColonna;
               ColonneOre;
               Next;
          end;
          //[/TONI20020724\]FINE
     end;
     if data.Global.fieldbyname('debughrms').asboolean = true then showmessage('ultima riga=' + inttostr(xArrayIDAgenda[1, 1]));
end;

procedure TAgendaSettForm.InsImpegniAgendaRis;
var xAnno, xMese, xGiorno, xOra, xMin, xSec, xMSec, xOra2, xMin2, xSec2, xMSec2: Word;
     k, i, h, j, xRiga, xColonna: integer;
     xMinStr, xMinStr2, xDesc: string;
begin
     ShortTimeFormat := 'hh.mm';
     QRisorseDisp.Close;
     QRisorseDisp.ReloadSQL;
     if TRisorse.Active = false then TRisorse.Open;
     //[/TONI20020725\]
{     QRisorseDisp.Prepare;
     QRisorseDisp.Params[0].AsDateTime:=xPrimaData;
     QRisorseDisp.Params[1].AsDateTime:=xPrimaData+5;
     QRisorseDisp.Params[2].Value:=TRisorseID.Value;}
     if xPrimaData <> 0 then //TONI - DEBUG
     begin
          QRisorseDisp.ParamByName['xDal'] := DateToStr(xPrimaData);
          QRisorseDisp.ParamByName['xAl'] := DateToStr(xPrimaData + 5);
          QRisorseDisp.ParamByName['xRis'] := TRisorseID.Value;
          QRisorseDisp.Open;
     end; //TONI - DEBUG
     while not QRisorseDisp.EOF do begin
          {          DecodeDate(QRisorseDispData.Value,xAnno,xMese,xGiorno);
                    DecodeTime(QRisorseDispOre.Value,xOra,xMin,xSec,xMSec);
                    DecodeTime(QRisorseDispAlleOre.Value,xOra2,xMin2,xSec2,xMSec2);}
          DecodeDate(QRisorseDisp.FieldByName('Data').AsDateTime, xAnno, xMese, xGiorno);
          DecodeTime(QRisorseDisp.FieldByName('Ore').AsDateTime, xOra, xMin, xSec, xMSec);
          DecodeTime(QRisorseDisp.FieldByName('AlleOre').AsDateTime, xOra2, xMin2, xSec2, xMSec2);
          //[/TONI20020725\]FINE
          if xMin < 10 then xMinStr := '0' + IntToStr(xMin)
          else xMinStr := IntToStr(xMin);
          if xMin2 < 10 then xMinStr2 := '0' + IntToStr(xMin2)
          else xMinStr2 := IntToStr(xMin2);
          // cerca colonna giusta
          for k := 1 to 6 do if xGiorni[k] = xGiorno then begin xColonna := k; break; end;
          // cerca riga giusta
          for h := 1 to 50 do if xore[h] = xOra then begin xRiga := h; break; end;

          while ASGRis.Cells[xColonna, xRiga] <> '' do begin
               if xOre[xRiga + 1] <> xOra then
                    ASGRis.InsertRows(xRiga + 1, 1);
               for i := 0 to ASGRis.RowCount - 1 do ASGRis.RowHeights[i] := 16;
               inc(xRiga);
          end;

          ASGRis.AddImageIdx(xColonna, xRiga, QRisorseDisp.FieldByName('Tipo').AsInteger, habeforetext, vaCenter);
          //[/TONI20020725\]
     {         if xOra2>0 then
                    ASGRis.Cells[xColonna,xRiga]:=IntToStr(xOra)+'.'+xMinStr+'-'+IntToStr(xOra2)+'.'+xMinStr2+' '+LowerCase(QRisorseDispUtente.Value)
               else ASGRis.Cells[xColonna,xRiga]:=IntToStr(xOra)+'.'+xMinStr+' '+LowerCase(QRisorseDispUtente.Value);
               xArrayIDAgenda[xColonna,xRiga]:=QRisorseDispID.Value;}
          if xOra2 > 0 then
               //[/TONI20021909DEBUG]
               //               ASGRis.Cells[xColonna,xRiga]:=IntToStr(xOra)+'.'+xMinStr+'-'+IntToStr(xOra2)+'.'+xMinStr2+' '+LowerCase(QRisorseDisp.FieldByName ('Utente').Value)
               //          else ASGRis.Cells[xColonna,xRiga]:=IntToStr(xOra)+'.'+xMinStr+' '+LowerCase(QRisorseDisp.FieldByName ('Utente').Value);
               ASGRis.Cells[xColonna, xRiga] := IntToStr(xOra) + '.' + xMinStr + '-' + IntToStr(xOra2) + '.' + xMinStr2 + ' ' + LowerCase(QRisorseDisp.FieldByName('Nominativo').AsString)
          else ASGRis.Cells[xColonna, xRiga] := IntToStr(xOra) + '.' + xMinStr + ' ' + LowerCase(QRisorseDisp.FieldByName('Nominativo').AsString);
          //[/TONI20021909DEBUG]FINE
          xArrayIDAgenda[xColonna, xRiga] := QRisorseDisp.FieldByName('ID').AsInteger;
          //[/TONI20020725\]FINE
          xSelRowRis := xRiga;
          xSelColRis := xColonna;
          ColonneOreRis;
          QRisorseDisp.Next;
     end;
end;

procedure TAgendaSettForm.ColonneOre;
var k, x: integer;
begin
     x := 8;
     for k := 1 to ASG1.RowCount - 1 do begin
          if ASG1.Cells[0, k] <> '' then inc(x);
          xOre[k] := x;
     end;
end;

procedure TAgendaSettForm.ColonneOreRis;
var k, x: integer;
begin
     x := 8;
     for k := 1 to ASGRis.RowCount - 1 do begin
          if ASGRis.Cells[0, k] <> '' then inc(x);
          xOre[k] := x;
     end;
end;

procedure TAgendaSettForm.FormShow(Sender: TObject);
var xData: TDateTime;
begin
     barray.visible := data.global.FieldByName('debughrms').asboolean;
     //[/TONI20020725\]
     //DEBUGKO
  {     QUsers.ParamByName('xOggi').asDateTime:=Date;
       QUsers.Open;}

     //if xIDSel > 0 then
       //   QUsers.Locate('ID', xIDSel, []);
     if DayOfWeek(Date) = 2 then xPrimaData := Date
     else begin
          xData := Date;
          while DayOfWeek(xData) <> 2 do xData := xData - 1;
          xPrimaData := xData;
     end;

     qusers.close;
     QUSers.SQL.Text := 'select ID,Nominativo,Descrizione from Users where not ((DataScadenza is not null and DataScadenza<:xoggi: )' +
          'or (DataRevoca is not null and DataRevoca<:xoggi:) or DataCreazione is null or Tipo=0) order by Nominativo';
     QUsers.Parameters[0].Value := DateToStr(Date());
     QUsers.Parameters[1].Value := DateToStr(Date());
     QUsers.Open;
     QUsers.Locate('ID', xIDSel, []);

     if data.global.fieldbyname('debughrms').asboolean = true then showmessage('IDutente=' + QUsers.fieldbyname('id').asstring);

     PageControl1.ActivePage := TSAgenda;
     PageControl1Change(self);
     carica := true;
     // ComponiGriglia;
     // ComponiGrigliaRis;
     Caption := '[M/4] - ' + Caption;

     //Grafica
     AgendaSettForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel3.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel3.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel3.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel4.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel4.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel4.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel5.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel5.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel5.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel6.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel6.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel6.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel7.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel7.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel7.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel8.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel8.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel8.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel9.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel9.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel9.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel10.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel10.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel10.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel11.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel11.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel11.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     PanRisorse.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     PanRisorse.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     PanRisorse.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     PanUtenti.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     PanUtenti.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     PanUtenti.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     CandUtenteFrame1.Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     CandUtenteFrame1.Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     CandUtenteFrame1.Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     CandUtenteFrame1.Panel12.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     CandUtenteFrame1.Panel12.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     CandUtenteFrame1.Panel12.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
end;

procedure TAgendaSettForm.ComponiGriglia;
const GiorniSett: array[1..7] of string = ('Lu', 'Ma', 'Me', 'Gi', 'Ve', 'Sa', 'Do');
var i, k: integer;
     xAnno, xMese, xGiorno, xAnnoOggi, xMeseOggi, xGiornoOggi: Word;
begin
     if data.global.fieldbyname('debughrms').asboolean = true then showmessage(datetostr(xPrimaData));
     ASG1.Clear;
     ASG1.RowCount := 13;
     ASG1.ColCount := 7;
     for i := 0 to ASG1.RowCount - 1 do begin
          ASG1.RowHeights[i] := 16;
          if i = 0 then ASG1.Cells[0, i] := 'orario'
          else if i < 13 then ASG1.Cells[0, i] := IntToStr(i + 8) + '.00';
     end;
     for i := 0 to ASG1.ColCount - 1 do begin
          if i = 0 then ASG1.ColWidths[i] := 40
          else ASG1.ColWidths[i] := 120;
          if i > 0 then begin
               ASG1.Cells[i, 0] := GiorniSett[i] + ' ' + DateToStr(xPrimaData + i - 1);
               DecodeDate(xPrimaData + i - 1, xAnno, xMese, xGiorno);
               xGiorni[i] := xGiorno;
          end;
     end;
     ColonneOre;
     InsImpegniAgenda;

     // griglia promemoria
     ASG2.Clear;
     ASG2.RowCount := 5;
     ASG2.ColCount := 7;
     for i := 0 to ASG2.RowCount - 1 do begin
          ASG2.RowHeights[i] := 16;
          if i = 0 then ASG2.Cells[0, i] := ''
          else if i < 5 then ASG2.Cells[0, i] := '';
     end;
     for i := 0 to ASG2.ColCount - 1 do begin
          if i = 0 then ASG2.ColWidths[i] := 40
          else ASG2.ColWidths[i] := 120;
          if i > 0 then begin
               ASG2.Cells[i, 0] := GiorniSett[i] + ' ' + DateToStr(xPrimaData + i - 1);
          end;
     end;
     InsMessaggi;
end;

procedure TAgendaSettForm.ComponiGrigliaRis;
const GiorniSett: array[1..7] of string = ('Lu', 'Ma', 'Me', 'Gi', 'Ve', 'Sa', 'Do');
var i, k: integer;
     xAnno, xMese, xGiorno, xAnnoOggi, xMeseOggi, xGiornoOggi: Word;
begin
     ASGRis.Clear;
     ASGRis.RowCount := 13;
     ASGRis.ColCount := 7;
     for i := 0 to ASGRis.RowCount - 1 do begin
          ASGRis.RowHeights[i] := 16;
          if i = 0 then ASGRis.Cells[0, i] := 'orario'
          else if i < 13 then ASGRis.Cells[0, i] := IntToStr(i + 8) + '.00';
     end;
     for i := 0 to ASGRis.ColCount - 1 do begin
          if i = 0 then ASGRis.ColWidths[i] := 40
          else ASGRis.ColWidths[i] := 120;
          if i > 0 then begin
               ASGRis.Cells[i, 0] := GiorniSett[i] + ' ' + DateToStr(xPrimaData + i - 1);
               DecodeDate(xPrimaData + i - 1, xAnno, xMese, xGiorno);
               xGiorni[i] := xGiorno;
          end;
     end;
     ColonneOreRis;
     InsImpegniAgendaRis;
end;

procedure TAgendaSettForm.TUsersAfterScroll(DataSet: TDataSet);
begin
     ComponiGriglia;
end;

procedure TAgendaSettForm.ASG2GetAlignment(Sender: TObject; ARow,
     ACol: Integer; var AAlignment: TAlignment);
begin
     if (ACol = 0) or (ARow = 0) then AAlignment := taCenter;
end;

procedure TAgendaSettForm.ASG2GetCellColor(Sender: TObject; ARow,
     ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
     if ARow = 0 then
          afont.style := [fsBold];
end;

procedure TAgendaSettForm.InsMessaggi;
var xAnno, xMese, xGiorno, xOra, xMin, xSec, xMSec: Word;
     k, i, h, xRiga, xColonna: integer;
     xMinStr, xDesc: string;
begin
     QPromemoria.Close;
     //[/TONI20020725\]  -- DEBUGOK --
  //     QPromemoria.Prepare;
  {     QPromemoria.Params[0].AsDateTime:=xPrimaData;
       QPromemoria.Params[1].AsDateTime:=xPrimaData+5;
       QPromemoria.Params[2].Value:=QUsersID.Value;}
     if xprimadata > 0 then
     begin
          QPromemoria.SQL.Text := 'select * from Promemoria where DataDaLeggere>=:xDal: and DataDaLeggere<=:xAl: ' +
               ' and IDUtente=:xUtente: and DataLettura is null order by DataDaLeggere';
          QPromemoria.ParamByname['xDal'] := xPrimaData;
          QPromemoria.ParamByname['xAl'] := DateToStr(xPrimaData + 5);
          QPromemoria.ParamByname['xUtente'] := QUSers.FieldByName('ID').AsInteger;
          {     QPromemoria.Params[0].AsDateTime:=xPrimaData;
               QPromemoria.Params[1].AsDateTime:=xPrimaData+5;
               QPromemoria.Params[2].Value:=QUsersID.Value;}
          QPromemoria.Open;
     end;
     while not QPromemoria.EOF do begin
          //          DecodeDate(QPromemoriaDataDaLeggere.Value,xAnno,xMese,xGiorno);
          DecodeDate(QPromemoria.FieldByName('DataDaLeggere').Value, xAnno, xMese, xGiorno);
          // cerca colonna giusta
          for k := 1 to 6 do
               if xGiorni[k] = xGiorno then begin xColonna := k; break; end;
          xRiga := 1;
          while ASG2.Cells[xColonna, xRiga] <> '' do begin
               inc(xRiga);
          end;
          if xRiga >= 5 then ASG2.InsertRows(xRiga, 1);

          {          ASG2.Cells[xColonna,xRiga]:=QPromemoriaTesto.Value;
                    xArrayIDProm[xColonna,xRiga]:=QPromemoriaID.Value;}
          ASG2.Cells[xColonna, xRiga] := QPromemoria.FieldByName('Testo').AsString;
          xArrayIDProm[xColonna, xRiga] := QPromemoria.FieldByName('ID').AsInteger;
          QPromemoria.Next;
     end;
     for i := 0 to ASG2.RowCount - 1 do ASG2.RowHeights[i] := 16;
     //[/TONI20020725\]FINE
end;

procedure TAgendaSettForm.BSettPrecClick(Sender: TObject);
begin
     xPrimaData := xPrimaData - 7;
     ComponiGriglia;
end;

procedure TAgendaSettForm.BSettSuccClick(Sender: TObject);
begin
     xPrimaData := xPrimaData + 7;
     ComponiGriglia;
end;

procedure TAgendaSettForm.BNuovoImpegnoClick(Sender: TObject);
var xHour, xMin, xSec, xMSec, xHour2, xMin2, xSec2, xMSec2: Word;
     xVai: boolean;
     xidagenda: integer;
begin
     NuovoImpForm := TNuovoImpForm.create(self);
     if PageControl1.ActivePageIndex = 0 then begin
          NuovoImpForm.DEData.Date := xPrimaData + xSelCol - 1;
          NuovoImpForm.MEOre.Text := IntToStr(xOre[xSelRow]) + '.00';
     end else begin
          NuovoImpForm.DEData.Date := xPrimaData + xSelColRis - 1;
          NuovoImpForm.MEOre.Text := IntToStr(xOre[xSelRowRis]) + '.00';
          //[/TONI20020724\]
//          while (not NuovoImpForm.TRisorse.EOF)and(TRisorseID.Value<>NuovoImpForm.TRisorseID.Value) do
          while (not NuovoImpForm.TRisorse.EOF) and (TRisorse.FieldByName('ID').Value <> NuovoImpForm.TRisorse.FieldByName('ID').Value) do
               //[/TONI20020724\]FINE
               NuovoImpForm.TRisorse.Next;
          NuovoImpForm.ActiveControl := NuovoImpForm.MEAlleOre;
     end;
     //NuovoImpForm.MEAlleOre.Text:=IntToStr(xOre[xSelRow])+'.30';
     NuovoImpForm.ShowModal;
     xVai := True;
     if NuovoImpForm.ModalResult = mrOK then begin
          //[/TONI20020724\]
//          Data.DB.BeginTrans;


          Data.DB.BeginTrans;
          try
               xidagenda := 0;
               Data.Q1.Close;
               if NuovoImpForm.MEAlleOre.Text <> '  .  ' then
                    Data.Q1.SQL.text := 'insert into Agenda (Data,Ore,AlleOre,Descrizione,IDUtente,Tipo,IDRisorsa) ' +
                         'values (:xData:,:xOre:,:xAlleOre:,:xDescrizione:,:xIDUtente:,:xTipo:,:xIDRisorsa:)'
               else
                    Data.Q1.SQL.text := 'insert into Agenda (Data,Ore,Descrizione,IDUtente,Tipo,IDRisorsa) ' +
                         'values (:xData:,:xOre:,:xDescrizione:,:xIDUtente:,:xTipo:,:xIDRisorsa:)';
               Data.Q1.ParambyName['xData'] := NuovoImpForm.DEData.Date;
               Data.Q1.ParambyName['xOre'] := StrToDateTime(DatetoStr(NuovoImpForm.DEData.Date) + ' ' + NuovoImpForm.MEOre.Text);
               if NuovoImpForm.MEAlleOre.Text <> '  .  ' then
                    Data.Q1.ParambyName['xAlleOre'] := StrToDateTime(DatetoStr(NuovoImpForm.DEData.Date) + ' ' + NuovoImpForm.MEAlleOre.Text);
               Data.Q1.ParambyName['xDescrizione'] := NuovoImpForm.EDescriz.Text;
               Data.Q1.ParambyName['xTipo'] := NuovoImpForm.RGTipo.Itemindex + 1;
               Data.Q1.ParambyName['xIDUtente'] := QUsers.FieldByName('ID').AsInteger;
               if (NuovoImpForm.trisorse.RecordCount > 0) and (not NuovoImpForm.CBLuogo.checked) then
                    Data.Q1.ParambyName['xIDRisorsa'] := NuovoImpForm.TRisorse.FieldByName('ID').AsInteger
               else Data.Q1.ParambyName['xIDRisorsa'] := 0;
               Data.Q1.ExecSQL;
               //               Data.DB.CommitTrans;
               Data.DB.CommitTrans;

               QTemp.close;
               QTemp.SQL.Text := ' select @@IDENTITY as idagenda';
               qtemp.Open;
               xidagenda := QTemp.fieldbyname('idagenda').asinteger;


               QAgenda.Close;
               QAgenda.Open;
               if PageControl1.ActivePageIndex = 0 then
                    ComponiGriglia
               else ComponiGrigliaRis;

          except
               //               Data.DB.RollbackTrans;
               Data.DB.RollBackTrans;
               MessageDlg('ERRORE SUL DATABASE:  operazione non completata', mtError, [mbOK], 0);
          end;

          // registrazione log operazione
          Log_Operation(mainform.xIDUtenteAttuale, 'Agenda', xidagenda, 'I', QUsers.FieldByName('ID').AsString + '-' + NuovoImpForm.EDescriz.Text + NuovoImpForm.DEData.Text + '-' + nuovoimpform.MEOre.Text);


          //[/TONI20020724\]FINE
     end;
     NuovoImpForm.Free;
end;

procedure TAgendaSettForm.BCancellaClick(Sender: TObject);
var xidAgenda: integer;
begin
     if ASG1.Cells[xSelCol, xSelRow] <> '' then begin
          //showmessage(IntToStr(xArrayIDAgenda[xSelCol, xSelRow]));
          if Messagedlg('Sei sicuro di eliminare l''appuntamento ? (' + ASG1.Cells[xSelCol, xSelRow] + ')', mtWarning, [mbNo, mbYes], 0) = mrYes then begin
               //[/TONI20020724\]
               with Data do begin
                    //                    DB.BeginTrans;
                    DB.BeginTrans;
                    try
                         xIDAgenda := xArrayIDAgenda[xSelCol, xSelRow];
                         Q1.Close;
                         Q1.SQL.text := 'delete from Agenda where ID=' + IntToStr(xArrayIDAgenda[xSelCol, xSelRow]);
                         Q1.ExecSQL;
                         //                         DB.CommitTrans;
                         DB.CommitTrans;
                    except
                         //                         DB.RollbackTrans;
                         DB.RollbackTrans;
                         MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                    end;
               end;

               // registrazione log operazione
               Log_Operation(mainform.xIDUtenteAttuale, 'Agenda', xidagenda, 'D', datetostr(date));


               QAgenda.Close;
               QAgenda.Open;
               ComponiGriglia;
          end;
          //[/TONI20020724\]FINE
     end;
end;

procedure TAgendaSettForm.BSpostaImpegnoClick(Sender: TObject);
var xOra, xMin, xSec, xMSec, xOra2, xMin2, xSec2, xMSec2: Word;
     xIDAgenda, n: integer;
     xMinStr, xMinStr2, xIDCandRic: string;
begin
     xIDAgenda := xArrayIDAgenda[xSelCol, xSelRow];
     if data.global.fieldbyname('debughrms').asboolean = true then showmessage('idagenda=' + inttostr(xIDAgenda) + chr(13) + 'xSelCol=' + inttostr(xSelCol) + ' xSelRow=' + inttostr(xSelRow));
     if xIDAgenda > 0 then begin
          //[/TONI20020724\]
          QAgendaABS.Close;
          //          QAgendaABS.Params[0].asInteger:=xIDAgenda;
          QAgendaABS.SQL.text := 'select * from Agenda where ID=' + IntToStr(xIDAgenda);
          QAgendaABS.Open;

          trisorse.Close;
          trisorse.Open;
          SpostaForm := TSpostaForm.create(self);
          if trisorse.RecordCount = 0 then spostaform.CBLuogo.Checked := true;
          {          SpostaForm.DEData.Date:=QAgendaABSData.Value;
                    while (not SpostaForm.TRisorse.EOF)and(QAgendaABSIDRisorsa.Value<>SpostaForm.TRisorseID.Value) do}
          SpostaForm.DEData.Date := QAgendaABS.FieldByName('Data').Value;
          while (not SpostaForm.TRisorse.EOF) and (QAgendaABS.FieldByName('IDRisorsa').Value <> SpostaForm.TRisorse.FieldByName('ID').Value) do
               SpostaForm.TRisorse.Next;
          n := trisorse.RecordCount;
          // dalle ore
//          DecodeTime(QAgendaABSOre.Value,xOra,xMin,xSec,xMSec);
          DecodeTime(QAgendaABS.FieldByName('Ore').Value, xOra, xMin, xSec, xMSec);
          if xMin < 10 then xMinStr := '0' + IntToStr(xMin)
          else xMinStr := IntToStr(xMin);
          SpostaForm.MEOre.Text := IntToStr(xOra) + '.' + xMinStr;
          // alle ore
//          DecodeTime(QAgendaABSAlleOre.Value,xOra2,xMin2,xSec2,xMSec2);
          if QAgendaABS.FieldByName('AlleOre').Value <> NULL then
               DecodeTime(QAgendaABS.FieldByName('AlleOre').Value, xOra2, xMin2, xSec2, xMSec2)
          else begin xOra2 := 0; xMin2 := 0; xSec2 := 0; xMSec2 := 0 end;
          if xMin2 < 10 then xMinStr2 := '0' + IntToStr(xMin2)
          else xMinStr2 := IntToStr(xMin2);
          if xOra2 > 0 then SpostaForm.MEAlleOre.Text := IntToStr(xOra2) + '.' + xMinStr2;
          {          SpostaForm.EDescriz.Text:=QAgendaABSDescrizione.Value;
                    SpostaForm.RGTipo.Itemindex:=QAgendaABSTipo.Value-1;}
          SpostaForm.EDescriz.Text := QAgendaABS.FieldByName('Descrizione').Value;
          SpostaForm.RGTipo.Itemindex := QAgendaABS.FieldByName('Tipo').Value - 1;
          if SpostaForm.RGTipo.ItemIndex = 0 then
               SpostaForm.PanLuogo.Visible := true
          else SpostaForm.PanLuogo.Visible := False;
          SpostaForm.ShowModal;
          if SpostaForm.ModalResult = mrOK then begin
               // controllo concomitanza in SpostaForm
//               Data.DB.BeginTrans;
               Data.DB.BeginTrans;
               try
                    Data.Q1.Close;
                    //COntrollo Date, eliminazione utilizzo parametri
{                    if SpostaForm.MEAlleOre.Text<>'  .  ' then
                        Data.Q1.SQL.text:='update Agenda set Data=:xData,Ore=:xOre,AlleOre=:xAlleOre,Descrizione=:xDescrizione,IDRisorsa=:xIDRisorsa,Tipo=:xTipo '+
                             'where ID='+IntToStr(xIDAgenda)
                   else
                        Data.Q1.SQL.text:='update Agenda set Data=:xData,Ore=:xOre,Descrizione=:xDescrizione,IDRisorsa=:xIDRisorsa,Tipo=:xTipo '+
                             'where ID='+IntToStr(xIDAgenda);
                   Data.Q1.ParambyName('xData').asDateTime:=SpostaForm.DEData.Date;
                   Data.Q1.ParambyName('xOre').asDateTime:=StrToDateTime(DatetoStr(SpostaForm.DEData.Date)+' '+SpostaForm.MEOre.Text);
                   if SpostaForm.MEAlleOre.Text<>'  .  ' then
                        Data.Q1.ParambyName('xAlleOre').asDateTime:=StrToDateTime(DatetoStr(SpostaForm.DEData.Date)+' '+SpostaForm.MEAlleOre.Text);
                   Data.Q1.ParambyName('xDescrizione').asString:=SpostaForm.EDescriz.Text;
                   if (SpostaForm.RGTipo.ItemIndex=0)and(not SpostaForm.CBLuogo.checked) then
                        Data.Q1.ParambyName('xIDRisorsa').asInteger:=SpostaForm.TRisorseID.Value
                   else Data.Q1.ParambyName('xIDRisorsa').asInteger:=0;
                   Data.Q1.ParambyName('xTipo').asInteger:=SpostaForm.RGTipo.Itemindex+1;}
                    Data.Q1.SQL.text := 'update Agenda set ' +
                         'Data=''' + DateToStr(SpostaForm.DEData.Date) + ''',' +
                         'Ore=:xOre:,AlleOre=:xAlleOre:,';
                    Data.Q1.SQL.Add('Descrizione=:xDescrizione:,');
                    Data.Q1.SQL.Add('IDRisorsa=:xIDRisorsa:,');
                    Data.Q1.SQL.Add('Tipo=:xTipo: ');
                    Data.Q1.SQL.Add('where ID=' + IntToStr(xIDAgenda));

                    //                    Data.Q1.ParambyName['xData']:=SpostaForm.DEData.Date;
                    Data.Q1.ParambyName['xOre'] := StrToDateTime(DatetoStr(SpostaForm.DEData.Date) + ' ' + SpostaForm.MEOre.Text);
                    if SpostaForm.MEAlleOre.Text <> '  .  ' then
                         Data.Q1.ParambyName['xAlleOre'] := StrToDateTime(DatetoStr(SpostaForm.DEData.Date) + ' ' + SpostaForm.MEAlleOre.Text)
                    else Data.Q1.ParambyName['xAlleOre'] := NULL;
                    Data.Q1.ParambyName['xDescrizione'] := SpostaForm.EDescriz.Text;
                    if (n > 0) and (spostaForm.CBLuogo.checked) then
                         Data.Q1.ParambyName['xIDRisorsa'] := SpostaForm.TRisorse.FieldByName('ID').AsInteger
                    else Data.Q1.ParambyName['xIDRisorsa'] := 0;
                    Data.Q1.ParambyName['xTipo'] := SpostaForm.RGTipo.Itemindex + 1;

                    Data.Q1.ExecSQL;


                    Data.Q1.Close;
                    Data.Q1.SQL.Text := 'select IDCandRic from Agenda where id = ' + IntToStr(xIDAgenda);
                    Data.Q1.Open;

                    if Data.Q1.FieldByName('IDCandRic').AsString <> '' then begin
                         xIDCandRic := Data.Q1.FieldByName('IDCandRic').AsString;
                         Data.Q1.Close;
                         Data.Q1.SQL.Text := 'Update EBC_CandidatiRicerche set DataImpegno = :xDataImpegno: where ID = ' + xIDCandRic;
                         Data.Q1.ParambyName['xDataImpegno'] := DateToStr(SpostaForm.DEData.Date);
                         Data.Q1.ExecSQL;
                    end;

                    //                    Data.DB.CommitTrans;
                    Data.DB.CommitTrans;
                    QAgenda.Close;
                    QAgenda.Open;
                    ComponiGriglia;
               except
                    //                    Data.DB.RollbackTrans;
                    Data.DB.RollBackTrans;
                    MessageDlg('ERRORE SUL DATABASE:  operazione non completata', mtError, [mbOK], 0);
               end;

               // registrazione log operazione
               Log_Operation(mainform.xIDUtenteAttuale, 'Agenda', xidagenda, 'U', QUsers.FieldByName('ID').AsString + '-' + SpostaForm.EDescriz.Text + SpostaForm.DEData.Text + '-' + SpostaForm.MEOre.Text);


          end;
          SpostaForm.Free;
          QAgendaABS.Close;
          //[/TONI20020724\]FINE
     end;
end;

procedure TAgendaSettForm.BEsciClick(Sender: TObject);
begin
     close
end;

procedure TAgendaSettForm.modificaimpegno1Click(Sender: TObject);
begin
     BSpostaImpegnoClick(self);
end;

procedure TAgendaSettForm.nuovoimpegno1Click(Sender: TObject);
begin
     BNuovoImpegnoClick(self);
end;

procedure TAgendaSettForm.eliminaimpegno1Click(Sender: TObject);
begin
     BCancellaClick(self);
end;

procedure TAgendaSettForm.ASG1DblClick(Sender: TObject);
begin
     BSpostaImpegnoClick(self);
end;

procedure TAgendaSettForm.BModPromClick(Sender: TObject);
var xOra, xMin, xSec, xMSec: Word;
     xMinStr, xCella: string;
     xTent: integer;
begin
     if ASG2.Cells[xSelCol2, xSelRow2] <> '' then begin
          Q.Close;
          Q.SQL.text := 'select * from Promemoria where ID=' + IntToStr(xArrayIDProm[xSelCol2, xSelRow2]);
          Q.Open;
          ModMessaggioForm := TModMessaggioForm.create(self);
          ModMessaggioForm.DEData.Date := Q.FieldbyName('DataDaLeggere').asDateTime;
          ModMessaggioForm.EDescriz.Text := Q.FieldbyName('Testo').asString;
          ModMessaggioForm.ShowModal;
          if ModMessaggioForm.ModalResult = mrOK then begin
               //[/TONI20020725\]
               with Data do begin
                    //                    DB.BeginTrans;
                    DB.BeginTrans;
                    try
                         //COntrollo Date, eliminazione utilizzo parametri
                         Q1.Close;
                         Q1.SQL.text := 'update Promemoria set DataDaLeggere=:xDataDaLeggere:,Testo=:xTesto: ' +
                              'where ID=' + Q.FieldbyName('ID').asString;
                         Q1.ParamByName['xDataDaLeggere'] := ModMessaggioForm.DEData.Date;
                         Q1.ParamByName['xTesto'] := ModMessaggioForm.EDescriz.Text;
                         Q1.ExecSQL;
                         //                         DB.CommitTrans;
                         DB.CommitTrans;
                    except
                         //                        DB.RollbackTrans;
                         DB.RollbackTrans;
                         MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                    end;
               end;
               QPromemoria.Close;
               QPromemoria.Open;
               ComponiGriglia;
          end;
          //[/TONI20020725\]FINE
          Q.Close;
          ModMessaggioForm.Free;
     end;
end;

procedure TAgendaSettForm.vedispostamessaggio1Click(Sender: TObject);
begin
     BModPromClick(self);
end;

procedure TAgendaSettForm.nuovomessaggio1Click(Sender: TObject);
begin
     BNuovoPromClick(self);
end;

procedure TAgendaSettForm.eliminamessaggio1Click(Sender: TObject);
begin
     BEliminaPromClick(self);
end;

procedure TAgendaSettForm.ASG2ClickCell(Sender: TObject; Arow,
     Acol: Integer);
begin
     xSelRow2 := ARow;
     xSelCol2 := Acol;
end;

procedure TAgendaSettForm.ASG2DblClick(Sender: TObject);
begin
     BModPromClick(self);
end;

procedure TAgendaSettForm.BNuovoPromClick(Sender: TObject);
begin
     TPromABS.open;
     TPromABS.Insert;
     //[/TONI20020725\]
   {     TPromABSIDUtente.Value:=QUsersID.Value;
        TPromABSDataIns.Value:=Date;
        TPromABSEvaso.Value:=False;
        TPromABSDataDaLeggere.Value:=xPrimaData+xSelCol2-1;}
     TPromABS.FieldBYName('IDUtente').AsInteger := QUsers.FieldByName('ID').AsInteger;
     TPromABS.FieldByName('DataIns').Value := Date;
     TPromABS.FieldBYName('Evaso').AsBoolean := False;
     TPromABS.FieldByName('DataDaLeggere').Value := xPrimaData + xSelCol2 - 1;
     //[/TONI20020725\]FINE
     InsMessaggioForm := TInsMessaggioForm.create(self);
     InsMessaggioForm.ShowModal;
     if InsMessaggioForm.ModalResult = MrOK then begin
          TPromABS.Post;
          ComponiGriglia;
     end else TPromABS.Cancel;
     InsMessaggioForm.Free;
     TPromABS.Open;
end;

procedure TAgendaSettForm.BEliminaPromClick(Sender: TObject);
var xCella: string;
begin
     if ASG2.Cells[xSelCol2, xSelRow2] <> '' then begin
          if Messagedlg('Sei sicuro di voler eliminare DEFINITIVAMENTE il messaggio ?', mtWarning, [mbNo, mbYes], 0) = mrYes then begin
               with Data do begin
                    //[/TONI20020725\]
  //                    DB.BeginTrans;
                    DB.BeginTrans;
                    try
                         Q1.Close;
                         Q1.SQL.text := 'delete from Promemoria where ID=' + IntToStr(xArrayIDProm[xSelCol2, xSelRow2]);
                         Q1.ExecSQL;
                         //                         DB.CommitTrans;
                         DB.CommitTrans;
                    except
                         //                         DB.RollbackTrans;
                         DB.RollbackTrans;
                         MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                    end;
                    //[/TONI20020725\]FINE
               end;
               QPromemoria.Close;
               QPromemoria.Open;
               ComponiGriglia;
          end;
     end;
end;

procedure TAgendaSettForm.BGestisciRicClick(Sender: TObject);
var xTestoErrore: string;
     xSolaLettura, xCheckSoloLettura: boolean;
begin
     if ASG1.Cells[xSelCol, xSelRow] <> '' then begin
          QAgendaABS.Close;
          QAgendaABS.SQL.Text := 'select * from Agenda where ID=' + IntToStr(xArrayIDAgenda[xSelCol, xSelRow]);
          QAgendaABS.Open;
          if QAgendaABS.FieldByName('IDCandRic').asString <> '' then begin
               Q.Close;
               Q.SQL.text := 'select IDRicerca from EBC_CandidatiRicerche where ID=' + QAgendaABS.FieldByname('IDCandRic').asString;
               Q.Open;

               xSolaLettura := false;
               xCheckSoloLettura := Data.Global.FieldByName('UtentiCommessaSoloLettura').AsBoolean;


               CaricaApriRicPend(Q.FieldByName('IDRicerca').asinteger);

               // controllo selezionatore: gli aggregati possono entrare come il titolare !
               if xCheckSoloLettura then begin
                    Data.QTemp.Close;
                    Data.QTemp.SQL.text := 'select IDUtente from EBC_RicercheUtenti where IDRicerca=' + DataRicerche.QRicAttive.FieldByName('ID').AsString;
                    Data.QTemp.Open;
                    if not Data.QTemp.Locate('IDUtente', MainForm.xIDUtenteAttuale, []) then begin
                         if not MainForm.CheckProfile('27', False) then begin
                              MessageDlg('L''utente connesso non ha diritti di accesso a progetti/commesse di altri utenti' + chr(13) +
                                   'IMPOSSIBILE ACCEDERE', mtError, [mbOK], 0);
                              Data.QTemp.Close;
                              Exit;
                         end else
                              if (MainForm.xUtenteAttuale <> 'Administrator') then begin
                                   MessageDlg('ATTENZIONE: l''utente connesso � diverso dal titolare del progetto/commessa e dagli associati:' + chr(13) +
                                        'non sar� quindi possibile modificare i dati ', mtWarning, [mbOK], 0);
                                   xSolaLettura := True;
                              end;
                    end;
               end;

               xTestoErrore := CheckRicercheInUso(Q.FieldByName('IDRicerca').asinteger, MainForm.xIDUtenteAttuale);
               if xTestoErrore <> '' then begin
                    MessageDlg(xTestoErrore, mtWarning, [mbOK], 0);
                    xSolaLettura := True;
               end;
               Data.QTemp.Close;

               SelPersForm := TSelPersForm.create(self);
               SelPersForm.CBOpzioneCand.Checked := True;
               SelPersForm.EseguiQueryCandidati;
               SelPersForm.xFromOrgTL := False;
               if xSolaLettura then SelPersForm.ImpostaSoloLettura;

               SelPersForm.ShowModal;
               try SelPersForm.Free
               except
               end;


               DataRicerche.QRicAttive.Close;
               Mainform.EseguiQRicAttive;
               if Data2.QRicClienti.Active then begin
                    Data2.QRicClienti.Close;
                    Data2.QRicClienti.open;
               end;
               if Data2.QAnagRicerche.Active then begin
                    Data2.QAnagRicerche.Close;
                    Data2.QAnagRicerche.Open;
               end;
               DataRicerche.TRicerchePend.Close;
          end;
          QAgenda.Close;
          QAgenda.Open;
          ComponiGriglia;
     end;
end;

procedure TAgendaSettForm.PageControl1Change(Sender: TObject);
begin
     if PageControl1.ActivePage = TSAgenda then begin
          PanUtenti.Visible := True;
          PanRisorse.Visible := False;
          ComponiGriglia;
     end;
     //
     if PageControl1.ActivePage = TSAgendaRis then begin
          PanUtenti.Visible := False;
          PanRisorse.Visible := True;
          {          if TRisorse.FieldByName ('Color').AsString='clLime' then DBGrid2.Color:=clLime
                    else
                         if TRisorse.FieldByName ('Color').AsString='clRed' then DBGrid2.Color:=clRed
                         else
                              if TRisorse.FieldByName ('Color').AsString='clYellow' then DBGrid2.Color:=clYellow
                              else
                                   if TRisorse.FieldByName ('Color').AsString='clAqua' then DBGrid2.Color:=clAqua;}
          ComponiGrigliaRis;
          //[/TONI20020918DEBUG\]
          if not TRisorse.Eof then
               if TrimRight(TRisorse.FieldByName('Color').AsString) = 'clLime' then
                    DBGrid2.Color := clLime
               else
                    if TrimRight(TRisorse.FieldByName('Color').AsString) = 'clRed' then
                         DBGrid2.Color := clRed
                    else
                         if TrimRight(TRisorse.FieldByName('Color').AsString) = 'clYellow' then
                              DBGrid2.Color := clYellow
                         else
                              if TrimRight(TRisorse.FieldByName('Color').AsString) = 'clAqua' then
                                   DBGrid2.Color := clAqua;
          //[/TONI20020918DEBUG\]END
     end;
     //
     if PageControl1.ActivePage = TSCandidati then
     begin
          QCandidati.Close;
          if CBCommAttive.Checked = true then
          begin
               QCandidati.parameters.ParamByName('Xconclusa').value := 1;
               QCandidati.parameters.ParamByName('XIDTipoStato').value := 3;
          end else
          begin
               QCandidati.parameters.ParamByName('Xconclusa').value := -1;
               QCandidati.parameters.ParamByName('XIDTipoStato').value := 0;
          end;
          QCandidati.Open;
          PanUtenti.Visible := true;

     end else QCandidati.Close;
     //
     if PageControl1.ActivePage = TSCandUtente then begin
          CandUtenteFrame1.QCandUtente.Close;
          //[/TONI20020725\]
//          CandUtenteFrame1.QCandUtente.ParamByName('xIDUtente').asInteger:=QUsersID.Value;
          {CandUtenteFrame1.QCandUtente.SQL.Text := 'select distinct Anagrafica.ID,CVNumero,Cognome,Nome,DataNascita,RecapitiTelefonici,Cellulare,' +
               'DATEDIFF(day, EBC_Colloqui.Data, getdate()) ggDataUltimoColloquio,' +
               'DATEDIFF(day, EBC_ContattiCandidati.Data, getdate()) ggDataUltimoContatto ' +
               ' from Anagrafica,EBC_Colloqui,EBC_ContattiCandidati' +
               ' where Anagrafica.ID *= EBC_Colloqui.IDAnagrafica' +
               ' and EBC_Colloqui.Data= (select max(Data) from EBC_Colloqui' +
               ' where Anagrafica.ID *= EBC_Colloqui.IDAnagrafica)' +
               ' and Anagrafica.ID *= EBC_ContattiCandidati.IDAnagrafica' +
               ' and EBC_ContattiCandidati.Data=(select max(Data) from EBC_ContattiCandidati' +
               ' where Anagrafica.ID *= EBC_ContattiCandidati.IDAnagrafica)' +
               ' and Anagrafica.IDUtente=' + QUsers.FieldByName('ID').AsString + ' order by Cognome,Nome';}
          //Modifica Query di Thomas
          CandUtenteFrame1.QCandUtente.SQL.Text := 'select distinct Anagrafica.ID,CVNumero,Cognome,Nome,DataNascita,RecapitiTelefonici,Cellulare,' +
               'DATEDIFF(day, EBC_Colloqui.Data, getdate()) ggDataUltimoColloquio,' +
               'DATEDIFF(day, EBC_ContattiCandidati.Data, getdate()) ggDataUltimoContatto ' +
               ' from Anagrafica left join EBC_Colloqui on Anagrafica.ID = EBC_Colloqui.IDAnagrafica ' +
               ' and EBC_Colloqui.Data= (select max(Data) from Anagrafica A left join EBC_Colloqui' +
               ' on A.ID = EBC_Colloqui.IDAnagrafica where Anagrafica.ID = A.ID)' +
               ' left join EBC_ContattiCandidati on Anagrafica.ID = EBC_ContattiCandidati.IDAnagrafica' +
               ' and EBC_ContattiCandidati.Data=(select max(Data) from Anagrafica B left join EBC_ContattiCandidati' +
               ' on B.ID = EBC_ContattiCandidati.IDAnagrafica where Anagrafica.ID = B.ID)' +
               ' where Anagrafica.IDUtente=' + QUsers.FieldByName('ID').AsString + ' order by Cognome,Nome';
          CandUtenteFrame1.QCandUtente.Open;
          //          CandUtenteFrame1.xIDUtente:=QUsersID.Value;
          CandUtenteFrame1.xIDUtente := QUsers.FieldByName('ID').Value;
          //[/TONI20020725\]FINE
          CandUtenteFrame1.xColumnRic := CandUtenteFrame1.RxDBGrid1.Columns[1];
          PanUtenti.Visible := true;
          PanRisorse.Visible := False;
     end;
end;

procedure TAgendaSettForm.ToolbarButton971Click(Sender: TObject);
var xTestoErrore: string;
     xSolaLettura, xCheckSolaLettura: boolean;
begin
     xSolaLettura := false;
     xCheckSolaLettura := Data.Global.FieldByName('UtentiCommessaSoloLettura').AsBoolean;

     CaricaApriRicPend(QCandidati.FieldByName('IDRicerca').Value);

     if xCheckSolaLettura then begin
          // controllo selezionatore: gli aggregati possono entrare come il titolare !
          Data.QTemp.Close;
          Data.QTemp.SQL.text := 'select IDUtente from EBC_RicercheUtenti where IDRicerca=' + QCandidati.FieldByName('IDRicerca').asString;
          Data.QTemp.Open;
          if not Data.QTemp.Locate('IDUtente', MainForm.xIDUtenteAttuale, []) then begin
               if not MainForm.CheckProfile('27', False) then begin
                    MessageDlg('L''utente connesso non ha diritti di accesso a progetti/commesse di altri utenti' + chr(13) +
                         'IMPOSSIBILE ACCEDERE', mtError, [mbOK], 0);
                    Data.QTemp.Close;
                    Exit;
               end else
                    if (MainForm.xUtenteAttuale <> 'Administrator') then begin
                         MessageDlg('ATTENZIONE: l''utente connesso � diverso dal titolare del progetto/commessa e dagli associati:' + chr(13) +
                              'non sar� quindi possibile modificare i dati ', mtWarning, [mbOK], 0);
                         xSolaLettura := true;
                    end;
          end;
     end;
     Data.QTemp.Close;

     xTestoErrore := CheckRicercheInUso(QCandidati.FieldByName('IDRicerca').asinteger, MainForm.xIDUtenteAttuale);
     if xTestoErrore <> '' then begin
          MessageDlg(xTestoErrore, mtWarning, [mbOK], 0);
          xSolaLettura := True;
     end;

     SelPersForm := TSelPersForm.create(self);
     SelPersForm.xIDRicercaInt := QCandidati.FieldByName('IDRicerca').asinteger;
     SelPersForm.CBOpzioneCand.Checked := True;
     SelPersForm.EseguiQueryCandidati;
     SelPersForm.xFromOrgTL := False;
     if xSolaLettura then SelPersForm.ImpostaSoloLettura;
     SelPersForm.ShowModal;
     try SelPersForm.Free
     except
     end;
     DataRicerche.TRicerchePend.Close;
end;

procedure TAgendaSettForm.ToolbarButton972Click(Sender: TObject);
var xDic: string;
     SavePlace: TBookmark;
begin
     if not QCandidati.EOF then begin
          //[/TONI20020725\]
          xDic := TrimRight(QCandidati.FieldByname('Stato').AsString);
          if InputQuery('Modifica stato', 'Nuova descrizione:', xDic) then begin
               SavePlace := QCandidati.GetBookmark;
               //[/TONI20020725\]
//               Data.DB.BeginTrans;
               Data.DB.BeginTrans;
               try
                    Data.Q1.Close;
                    {                    Data.Q1.SQL.text:='update EBC_CandidatiRicerche set Stato=:xStato where ID='+QCandidatiID.asString;
                                        Data.Q1.ParambyName('xStato').asString:=xDic;}
                    Data.Q1.SQL.text := 'update EBC_CandidatiRicerche set Stato=''' + xDic + ''' where ID=' + QCandidati.FieldByName('ID').asString;
                    Data.Q1.ExecSQL;
                    //                    Data.DB.CommitTrans;
                    Data.DB.CommitTrans;
                    QCandidati.Close;
                    QCandidati.Open;
                    QCandidati.GotoBookmark(SavePlace);
                    QCandidati.FreeBookmark(SavePlace);
               except
                    //                    Data.DB.RollbackTrans;
                    Data.DB.RollBackTrans;
                    //[/TONI20020725\]FINE
                    MessageDlg('ERRORE SUL DATABASE:  operazione non completata', mtError, [mbOK], 0);
               end;
          end;
     end;
end;

procedure TAgendaSettForm.ASGRisClickCell(Sender: TObject; Arow,
     Acol: Integer);
begin
     xSelRowRis := ARow;
     xSelColRis := Acol;
end;

procedure TAgendaSettForm.ASGRisGetAlignment(Sender: TObject; ARow,
     ACol: Integer; var AAlignment: TAlignment);
begin
     if (ACol = 0) or (ARow = 0) then AAlignment := taCenter;
end;

procedure TAgendaSettForm.TRisorse_OLDAfterScroll(DataSet: TDataSet);
begin
     //[/TONI20020725\]
     //[/TONI20020918DEBUGOK\]
 {     if TRisorseColor.Value='clLime' then DBGrid2.Color:=clLime
      else
           if TRisorseColor.Value='clRed' then DBGrid2.Color:=clRed
           else
                if TRisorseColor.Value='clYellow' then DBGrid2.Color:=clYellow
                else
                     if TRisorseColor.Value='clAqua' then DBGrid2.Color:=clAqua;}
     ComponiGrigliaRis;
     if not TRisorse.Active then TRisorse.Open;
     if TrimRight(TRisorse.FieldByName('Color').AsString) = 'clLime' then DBGrid2.Color := clLime
     else
          if TrimRight(TRisorse.FieldByName('Color').AsString) = 'clRed' then DBGrid2.Color := clRed
          else
               if TrimRight(TRisorse.FieldByName('Color').AsString) = 'clYellow' then DBGrid2.Color := clYellow
               else
                    if TrimRight(TRisorse.FieldByName('Color').AsString) = 'clAqua' then DBGrid2.Color := clAqua;

     //[/TONI20020725\]FINE
     //ComponiGrigliaRis;
end;

procedure TAgendaSettForm.BSettPrec1Click(Sender: TObject);
begin
     xPrimaData := xPrimaData - 7;
     ComponiGrigliaRis;
end;

procedure TAgendaSettForm.BSettSucc1Click(Sender: TObject);
begin
     xPrimaData := xPrimaData + 7;
     ComponiGrigliaRis;
end;

procedure TAgendaSettForm.ASGRisGetCellColor(Sender: TObject; ARow,
     ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
     if ARow = 0 then
          afont.style := [fsBold];
end;

procedure TAgendaSettForm.FormCreate(Sender: TObject);
begin
     //     xIDSel := 0;
     carica := false;
end;

procedure TAgendaSettForm.QUsers_OLDAfterScroll(DataSet: TDataSet);
begin
     if carica = true then ComponiGriglia;
     if PageControl1.ActivePage = TSCandUtente then begin
          CandUtenteFrame1.QCandUtente.Close;
          //[/TONI20020725\]
//          CandUtenteFrame1.QCandUtente.ParamByName('xIDUtente').asInteger:=QUsersID.Value;
          {CandUtenteFrame1.QCandUtente.SQL.Text := 'select distinct Anagrafica.ID,CVNumero,Cognome,Nome,DataNascita,RecapitiTelefonici,Cellulare,' +
               'DATEDIFF(day, EBC_Colloqui.Data, getdate()) ggDataUltimoColloquio,' +
               'DATEDIFF(day, EBC_ContattiCandidati.Data, getdate()) ggDataUltimoContatto ' +
               ' from Anagrafica,EBC_Colloqui,EBC_ContattiCandidati' +
               ' where Anagrafica.ID *= EBC_Colloqui.IDAnagrafica' +
               ' and EBC_Colloqui.Data= (select max(Data) from EBC_Colloqui' +
               ' where Anagrafica.ID *= EBC_Colloqui.IDAnagrafica)' +
               ' and Anagrafica.ID *= EBC_ContattiCandidati.IDAnagrafica' +
               ' and EBC_ContattiCandidati.Data=(select max(Data) from EBC_ContattiCandidati' +
               ' where Anagrafica.ID *= EBC_ContattiCandidati.IDAnagrafica)' +
               ' and Anagrafica.IDUtente=' + QUsers.FieldByName('ID').AsString + ' order by Cognome,Nome'; }
          //Modifica Query di Thomas
          CandUtenteFrame1.QCandUtente.SQL.Text := 'select distinct Anagrafica.ID,CVNumero,Cognome,Nome,DataNascita,RecapitiTelefonici,Cellulare,' +
               'DATEDIFF(day, EBC_Colloqui.Data, getdate()) ggDataUltimoColloquio,' +
               'DATEDIFF(day, EBC_ContattiCandidati.Data, getdate()) ggDataUltimoContatto ' +
               ' from Anagrafica left join EBC_Colloqui on Anagrafica.ID = EBC_Colloqui.IDAnagrafica ' +
               ' and EBC_Colloqui.Data= (select max(Data) from Anagrafica A left join EBC_Colloqui' +
               ' on A.ID = EBC_Colloqui.IDAnagrafica where Anagrafica.ID = A.ID)' +
               ' left join EBC_ContattiCandidati on Anagrafica.ID = EBC_ContattiCandidati.IDAnagrafica' +
               ' and EBC_ContattiCandidati.Data=(select max(Data) from Anagrafica B left join EBC_ContattiCandidati' +
               ' on B.ID = EBC_ContattiCandidati.IDAnagrafica where Anagrafica.ID = B.ID)' +
               ' where Anagrafica.IDUtente=' + QUsers.FieldByName('ID').AsString + ' order by Cognome,Nome';
          CandUtenteFrame1.QCandUtente.Open;
          //          CandUtenteFrame1.xIDUtente:=QUsersID.Value;
          CandUtenteFrame1.xIDUtente := QUsers.FieldBYName('ID').Value;
          //[/TONI20020725\]FINE
     end;
end;

procedure TAgendaSettForm.BAgendaStampaClick(Sender: TObject);
begin
     {QRAgenda:=TQRAgenda.create(self);
     QRAgenda.QRDate.caption:='Settimana dal '+DateToStr(xPrimaData)+' al '+DateToStr(xPrimaData+5);
     //[/TONI20020725\]
     QRAgenda.QDate.SQL.text:='select distinct Data from agenda '+
          'where Data>=:xDal: and Data<=:xAl: ';
     QRAgenda.QDate.ParamByName['xDal']:=DateToStr(xPrimaData);
     QRAgenda.QDate.ParamByName['xAl']:=DateToStr(xPrimaData+5);
     QRAgenda.QAgenda.SQL.text:='select * from agenda '+
          'where IDUtente='+QUsers.FieldBYName('ID').AsString+' order by Ore ';
     //[/TONI20020725\]FINE
     QRAgenda.QDate.open;
     QRAgenda.QAgenda.open;
     QRAgenda.QR1.Preview;
     QRAgenda.Free;    }
     QRAgenda := TQRAgenda.create(self);
     QRAgenda.QRDate.caption := 'Settimana dal ' + DateToStr(xPrimaData) + ' al ' + DateToStr(xPrimaData + 5);
     //QRAgenda.QDate.SQL.text:='select distinct Data from agenda '+
     //     'where Data>=:xDal: and Data<=:xAl: ';
     //QRAgenda.QDate.ParamByName['xDal']:=DateToStr(xPrimaData);
     //QRAgenda.QDate.ParamByName['xAl']:=DateToStr(xPrimaData+5);
     QRAgenda.QAgenda.SQL.text := 'select * from agenda ' +
          'where IDUtente=' + QUsers.FieldBYName('ID').AsString +
          ' and Data>=:xDal: and Data<=:xAl: ' +
          'order by Data,Ore ';
     QRAgenda.QAgenda.ParamByName['xDal'] := DateToStr(xPrimaData);
     QRAgenda.QAgenda.ParamByName['xAl'] := DateToStr(xPrimaData + 5);
     //QRAgenda.QDate.open;
     QRAgenda.QAgenda.open;
     QRAgenda.QR1.Preview;
     QRAgenda.Free;


end;

procedure TAgendaSettForm.BSpostaImpegnoRisClick(Sender: TObject);
var xOra, xMin, xSec, xMSec, xOra2, xMin2, xSec2, xMSec2: Word;
     xMinStr, xMinStr2: string;
begin
     if ASGRis.Cells[xSelColRis, xSelRowRis] <> '' then begin
          QAgendaABS.Close;
          //[/TONI20020725\]
        //          QAgendaABS.Params[0].asInteger:=xArrayIDAgenda[xSelColRis,xSelRowRis];
          QAgendaABS.SQL.text := 'select * from Agenda where ID=' + IntToStr(xArrayIDAgenda[xSelColRis, xSelRowRis]);
          QAgendaABS.Open;

          SpostaForm := TSpostaForm.create(self);
          //          SpostaForm.DEData.Date:=QAgendaABSData.Value;
          SpostaForm.DEData.Date := QAgendaABS.FieldBYNAme('Data').Value;
          //          while (not SpostaForm.TRisorse.EOF)and(QAgendaABSIDRisorsa.Value<>SpostaForm.TRisorseID.Value) do
          while (not SpostaForm.TRisorse.EOF) and (QAgendaABS.FieldByName('IDRisorsa').Value <> SpostaForm.TRisorse.FieldByname('ID').Value) do
               SpostaForm.TRisorse.Next;
          // dalle ore
//          DecodeTime(QAgendaABSOre.Value,xOra,xMin,xSec,xMSec);
          DecodeTime(QAgendaABS.FieldByName('Ore').Value, xOra, xMin, xSec, xMSec);
          if xMin < 10 then xMinStr := '0' + IntToStr(xMin)
          else xMinStr := IntToStr(xMin);
          SpostaForm.MEOre.Text := IntToStr(xOra) + '.' + xMinStr;
          // alle ore
//          DecodeTime(QAgendaABSAlleOre.Value,xOra2,xMin2,xSec2,xMSec2);
          DecodeTime(QAgendaABS.FieldBYName('AlleOre').Value, xOra2, xMin2, xSec2, xMSec2);
          if xMin2 < 10 then xMinStr2 := '0' + IntToStr(xMin2)
          else xMinStr2 := IntToStr(xMin2);
          if xOra2 > 0 then SpostaForm.MEAlleOre.Text := IntToStr(xOra2) + '.' + xMinStr2;
          //          SpostaForm.EDescriz.Text:=QAgendaABSDescrizione.Value;
          SpostaForm.EDescriz.Text := QAgendaABS.FieldByName('Descrizione').Value;
          //          SpostaForm.RGTipo.Itemindex:=QAgendaABSTipo.Value-1;
          SpostaForm.RGTipo.Itemindex := QAgendaABS.FieldBYName('Tipo').Value - 1;
          if SpostaForm.RGTipo.ItemIndex = 0 then
               SpostaForm.PanLuogo.Visible := true
          else SpostaForm.PanLuogo.Visible := False;
          SpostaForm.ShowModal;
          if SpostaForm.ModalResult = mrOK then begin
               // controllo concomitanza in SpostaForm
//               Data.DB.BeginTrans;
               Data.DB.BeginTrans;
               try
                    Data.Q1.Close;
                    {                    if SpostaForm.MEAlleOre.Text<>'  .  ' then
                                             Data.Q1.SQL.text:='update Agenda set Data=:xData,Ore=:xOre,AlleOre=:xAlleOre,Descrizione=:xDescrizione,IDRisorsa=:xIDRisorsa,Tipo=:xTipo '+
                                                  'where Data=:kData and Ore=:kOre and IDUtente=:xIDUtente'
                                        else
                                             Data.Q1.SQL.text:='update Agenda set Data=:xData,Ore=:xOre,Descrizione=:xDescrizione,IDRisorsa=:xIDRisorsa,Tipo=:xTipo '+
                                                  'where Data=:kData and Ore=:kOre and IDUtente=:xIDUtente';
                                        Data.Q1.ParambyName('xData').asDateTime:=SpostaForm.DEData.Date;
                                        Data.Q1.ParambyName('xOre').asDateTime:=StrToDateTime(DatetoStr(SpostaForm.DEData.Date)+' '+SpostaForm.MEOre.Text);
                                        if SpostaForm.MEAlleOre.Text<>'  .  ' then
                                             Data.Q1.ParambyName('xAlleOre').asDateTime:=StrToDateTime(DatetoStr(SpostaForm.DEData.Date)+' '+SpostaForm.MEAlleOre.Text);
                                        Data.Q1.ParambyName('xDescrizione').asString:=SpostaForm.EDescriz.Text;
                                        Data.Q1.ParambyName('kData').asDateTime:=QAgendaABSData.Value;
                                        Data.Q1.ParambyName('kOre').asDateTime:=QAgendaABSOre.Value;
                                        Data.Q1.ParambyName('xIDUtente').asInteger:=QUsersID.Value;
                                        if (SpostaForm.RGTipo.ItemIndex=0)and(not SpostaForm.CBLuogo.checked) then
                                             Data.Q1.ParambyName('xIDRisorsa').asInteger:=SpostaForm.TRisorseID.Value
                                        else Data.Q1.ParambyName('xIDRisorsa').asInteger:=0;
                                        Data.Q1.ParambyName('xTipo').asInteger:=SpostaForm.RGTipo.Itemindex+1;}
                    if SpostaForm.MEAlleOre.Text <> '  .  ' then
                         if (SpostaForm.RGTipo.ItemIndex = 0) and (not SpostaForm.CBLuogo.checked) then
                              Data.Q1.SetSQLText(['update Agenda set Data=:xData:,Ore=:xOre:,AlleOre=:xAlleOre:,Descrizione=:xDescrizione:,IDRisorsa=:xIDRisorsa:,Tipo=:xTipo: ' +
                                   'where Data=:kData: and Ore=:kOre: and IDUtente=:xIDUtente:',
                                        FormatDateTime('dd/mm/yyyy', SpostaForm.DEData.Date),
                                        FormatDateTime('dd/mm/yyyy', StrToDateTime(DatetoStr(SpostaForm.DEData.Date) + ' ' + SpostaForm.MEOre.Text)),
                                        FormatDateTime('dd/mm/yyyy', StrToDateTime(DatetoStr(SpostaForm.DEData.Date) + ' ' + SpostaForm.MEAlleOre.Text)),
                                        SpostaForm.EDescriz.Text,
                                        FormatDateTime('dd/mm/yyyy', QAgendaABS.FieldByName('Data').Value),
                                        FormatDateTime('dd/mm/yyyy', QAgendaABS.FieldByName('Ore').Value),
                                        QUsers.FieldByName('ID').AsINteger,
                                        SpostaForm.TRisorse.FieldBYName('ID').Value,
                                        SpostaForm.RGTipo.Itemindex + 1])
                         else
                         begin
                              Data.Q1.SetSQLText(['update Agenda set Data=:xData:,Ore=:xOre:,AlleOre=:xAlleOre:,Descrizione=:xDescrizione:,IDRisorsa=:xIDRisorsa:,Tipo=:xTipo: ' +
                                   'where Data=:kData: and Ore=:kOre: and IDUtente=:xIDUtente:',
                                        FormatDateTime('dd/mm/yyyy', SpostaForm.DEData.Date),
                                        FormatDateTime('dd/mm/yyyy', StrToDateTime(DatetoStr(SpostaForm.DEData.Date) + ' ' + SpostaForm.MEOre.Text)),
                                        FormatDateTime('dd/mm/yyyy', StrToDateTime(DatetoStr(SpostaForm.DEData.Date) + ' ' + SpostaForm.MEAlleOre.Text)),
                                        SpostaForm.EDescriz.Text,
                                        FormatDateTime('dd/mm/yyyy', QAgendaABS.FieldByName('Data').Value),
                                        FormatDateTime('dd/mm/yyyy', QAgendaABS.FieldByName('Ore').Value),
                                        QUsers.FieldByName('ID').AsINteger,
                                        0,
                                        SpostaForm.RGTipo.Itemindex + 1]);
                         end
                    else
                         if (SpostaForm.RGTipo.ItemIndex = 0) and (not SpostaForm.CBLuogo.checked) then
                              Data.Q1.SetSQLText(['update Agenda set Data=:xData:,Ore=:xOre:,Descrizione=:xDescrizione:,IDRisorsa=:xIDRisorsa:,Tipo=:xTipo: ' +
                                   'where Data=:kData: and Ore=:kOre: and IDUtente=:xIDUtente:',
                                        FormatDateTime('dd/mm/yyyy', SpostaForm.DEData.Date),
                                        FormatDateTime('dd/mm/yyyy', StrToDateTime(DatetoStr(SpostaForm.DEData.Date) + ' ' + SpostaForm.MEOre.Text)),
                                        SpostaForm.EDescriz.Text,
                                        FormatDateTime('dd/mm/yyyy', QAgendaABS.FieldByName('Data').Value),
                                        FormatDateTime('dd/mm/yyyy', QAgendaABS.FieldByName('Ore').Value),
                                        QUsers.FieldByName('ID').AsINteger,
                                        SpostaForm.TRisorse.FieldBYName('ID').Value,
                                        SpostaForm.RGTipo.Itemindex + 1])
                         else
                              Data.Q1.SetSQLText(['update Agenda set Data=:xData:,Ore=:xOre:,Descrizione=:xDescrizione:,IDRisorsa=:xIDRisorsa:,Tipo=:xTipo: ' +
                                   'where Data=:kData: and Ore=:kOre: and IDUtente=:xIDUtente:',
                                        FormatDateTime('dd/mm/yyyy', SpostaForm.DEData.Date),
                                        FormatDateTime('dd/mm/yyyy', StrToDateTime(DatetoStr(SpostaForm.DEData.Date) + ' ' + SpostaForm.MEOre.Text)),
                                        SpostaForm.EDescriz.Text,
                                        FormatDateTime('dd/mm/yyyy', QAgendaABS.FieldByName('Data').Value),
                                        FormatDateTime('dd/mm/yyyy', QAgendaABS.FieldByName('Ore').Value),
                                        QUsers.FieldByName('ID').AsINteger,
                                        0,
                                        SpostaForm.RGTipo.Itemindex + 1]);

                    Data.Q1.ExecSQL;
                    //                    Data.DB.CommitTrans;
                    Data.DB.CommitTrans;
                    QAgenda.Close;
                    QAgenda.Open;
                    ComponiGrigliaRis;
               except
                    //                    Data.DB.RollbackTrans;
                    Data.DB.RollBackTrans;
                    MessageDlg('ERRORE SUL DATABASE:  operazione non completata', mtError, [mbOK], 0);
               end;
          end;
          SpostaForm.Free;
          QAgendaABS.Close;
     end;
end;

procedure TAgendaSettForm.ToolbarButton974Click(Sender: TObject);
begin
     if ASG1.Cells[xSelColRis, xSelRowRis] <> '' then begin
          if Messagedlg('Sei sicuro di eliminare l''appuntamento ?', mtWarning, [mbNo, mbYes], 0) = mrYes then begin
               with Data do begin
                    //[/TONI20020725\]
            //                    DB.BeginTrans;
                    DB.BeginTrans;
                    try
                         Q1.Close;
                         Q1.SQL.text := 'delete from Agenda where ID=' + IntToStr(xArrayIDAgenda[xSelColRis, xSelRowRis]);
                         Q1.ExecSQL;
                         //                         DB.CommitTrans;
                         DB.CommitTrans;
                    except
                         //                         DB.RollbackTrans;
                         DB.RollbackTrans;
                         MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                    end;
               end;
               //[/TONI20020725\]FINE
               QAgenda.Close;
               QAgenda.Open;
               ComponiGrigliaRis;
          end;
     end;
end;

procedure TAgendaSettForm.ToolbarButton975Click(Sender: TObject);
var xTestoErrore: string;
     xSolaLettura, xCheckSolaLettura: boolean;
begin
     if ASG1.Cells[xSelColRis, xSelRowRis] <> '' then begin
          QAgendaABS.Close;
          QAgendaABS.SQL.text := 'select * from Agenda where ID=' + IntToStr(xArrayIDAgenda[xSelColRis, xSelRowRis]);
          QAgendaABS.Open;
          if QAgendaABS.FieldByName('IDCandRic').asString <> '' then begin
               Q.Close;
               Q.SQL.text := 'select IDRicerca from EBC_CandidatiRicerche where ID=' + QAgendaABS.FieldByName('IDCandRic').asString;
               Q.Open;
               xSolaLettura := false;
               xCheckSolaLettura := Data.Global.FieldByName('UtentiCommessaSoloLettura').AsBoolean;


               CaricaApriRicPend(Q.FieldByName('IDRicerca').asinteger);
               if xCheckSolaLettura then begin
                    // controllo selezionatore: gli aggregati possono entrare come il titolare !
                    Data.QTemp.Close;
                    Data.QTemp.SQL.text := 'select IDUtente from EBC_RicercheUtenti where IDRicerca=' + DataRicerche.QRicAttive.FieldByName('ID').AsString;
                    Data.QTemp.Open;
                    if not Data.QTemp.Locate('IDUtente', MainForm.xIDUtenteAttuale, []) then begin
                         if not MainForm.CheckProfile('27', False) then begin
                              MessageDlg('L''utente connesso non ha diritti di accesso a progetti/commesse di altri utenti' + chr(13) +
                                   'IMPOSSIBILE ACCEDERE', mtError, [mbOK], 0);
                              Data.QTemp.Close;
                              Exit;
                         end else
                              if (MainForm.xUtenteAttuale <> 'Administrator') then begin
                                   MessageDlg('ATTENZIONE: l''utente connesso � diverso dal titolare del progetto/commessa e dagli associati:' + chr(13) +
                                        'non sar� quindi possibile modificare i dati ', mtWarning, [mbOK], 0);
                                   xSolaLettura := true;
                              end;
                    end;
               end;
               Data.QTemp.Close;

               xTestoErrore := CheckRicercheInUso(Q.FieldByName('IDRicerca').asinteger, MainForm.xIDUtenteAttuale);
               if xTestoErrore <> '' then begin
                    MessageDlg(xTestoErrore, mtWarning, [mbOK], 0);
                    xSolaLettura := True;
               end;

               SelPersForm := TSelPersForm.create(self);
               SelPersForm.xFromOrgTL := False;
               if xSolaLettura then SelPersForm.ImpostaSoloLettura;
               SelPersForm.ShowModal;
               try SelPersForm.Free
               except
               end;

               DataRicerche.QRicAttive.Close;
               Mainform.EseguiQRicAttive;
               if Data2.QRicClienti.Active then begin
                    Data2.QRicClienti.Close;
                    Data2.QRicClienti.Open;
               end;
               if Data2.QAnagRicerche.Active then begin
                    Data2.QAnagRicerche.Close;
                    Data2.QAnagRicerche.Open;
               end;
               DataRicerche.TRicerchePend.Close;
          end;
          QAgenda.Close;
          QAgenda.Open;
          ComponiGrigliaRis;
     end;
end;

procedure TAgendaSettForm.BArchiviaPromClick(Sender: TObject);
begin
     if ASG2.Cells[xSelCol2, xSelRow2] <> '' then begin
          if Messagedlg('Sei sicuro di voler ARCHIVIARE il messaggio ?', mtWarning, [mbNo, mbYes], 0) = mrYes then begin
               with Data do begin
                    //[/TONI20020725\]
                    //                  DB.BeginTrans;
                    DB.BeginTrans;
                    try
                         Q1.Close;
                         //                         Q1.SQL.text:='update Promemoria set DataLettura=:xDataLettura '+
                         Q1.SQL.text := 'update Promemoria set DataLettura=:xDataLettura: ' +
                              'where ID=' + IntToStr(xArrayIDProm[xSelCol2, xSelRow2]);
                         Q1.ParamByName['xDataLettura'] := Date;
                         Q1.ExecSQL;
                         //                       DB.CommitTrans;
                         DB.CommitTrans;
                    except
                         //                       DB.RollbackTrans;
                         DB.RollbackTrans;
                         MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                         //[/TONI20020725\]FINE
                    end;
               end;
               QPromemoria.Close;
               QPromemoria.Open;
               ComponiGriglia;
          end;
     end;
end;

procedure TAgendaSettForm.BArchivioPromClick(Sender: TObject);
begin
     ArchivioMessForm := TArchivioMessForm.create(self);
     //[/TONI20020725\]
     //     ArchivioMessForm.QMessLetti.ParamByName('xIDUtente').asInteger:=QUsersID.Value;
     ArchivioMessForm.QMessLetti.SQL.Text := 'select * from Promemoria' +
          ' where IDUtente= ' + QUsers.FieldByName('ID').AsString + ' and DataLettura is not null';
     //[/TONI20020725\]FINE
     ArchivioMessForm.QMessLetti.Open;
     ArchivioMessForm.ShowModal;
     ArchivioMessForm.Free;
     ComponiGriglia;
end;

function TAgendaSettForm.LogEsportazioniCand: boolean;
var xCriteri: string;
begin
     // restituisce FALSE se l'utente ha abortito l'operazione
     Result := True;
     if MessageDlg('ATTENZIONE:  la stampa o l''esportazione di dati � un''operazione soggetta a registrazione' + chr(13) +
          'secondo la normativa sulla privacy.  SEI SICURO DI VOLER PROSEGUIRE ?', mtWarning, [mbYes, mbNo], 0) = mrNo then begin
          Result := False;
          exit;
     end;
     with Data do begin
          //[/TONI20020725\]
//          DB.BeginTrans;
          DB.BeginTrans;
          try
               xCriteri := 'da Agenda-Situaz.cand.';
               {               Q1.SQL.text:='insert into LogEsportazCand (IDUtente,Data,Criteri,StringaSQL,ResultNum) '+
                                   ' values (:xIDUtente,:xData,:xCriteri,:xStringaSQL,:xResultNum)';
                              Q1.ParambyName('xIDUtente').asInteger:=MainForm.xIDUtenteAttuale;
                              Q1.ParambyName('xData').asDateTime:=Date;
                              Q1.ParambyName('xCriteri').asString:=xCriteri;
                              Q1.ParambyName('xStringaSQL').asString:=QCandidati.SQL.text;
                              Q1.ParambyName('xResultNum').asInteger:=QCandidati.RecordCount;}
               Q1.SQL.Text := 'insert into LogEsportazCand (IDUtente,Data,Criteri,StringaSQL,ResultNum) ' +
                    ' values (:xIDUtente:,:xData:,:xCriteri:,:xStringaSQL:,:xResultNum:)';
               Q1.ParambyName['xIDUtente'] := MainForm.xIDUtenteAttuale;
               Q1.ParambyName['xData'] := DateTostr(Date);
               Q1.ParambyName['xCriteri'] := xCriteri;
               Q1.ParambyName['xStringaSQL'] := StringReplace(QCandidati.SQL.text, '''', '''''', [rfReplaceAll]);
               Q1.ParambyName['xResultNum'] := QCandidati.RecordCount;
               Q1.ExecSQL;
               //               DB.CommitTrans;
               DB.CommitTrans;
          except
               //               DB.RollbackTrans;
               DB.RollbackTrans;
               //[/TONI20020725\]FINE
               MessageDlg('ERRORE SUL DATABASE: operazione non effettuata', mtError, [mbOK], 0);
          end;
     end;
end;

procedure TAgendaSettForm.Stampagriglia1Click(Sender: TObject);
begin
     if not MainForm.CheckProfile('40') then Exit;
     if not LogEsportazioniCand then exit;
     dxPrinter1.Preview(True, nil);
end;

procedure TAgendaSettForm.EsportainExcel1Click(Sender: TObject);
begin
     MainForm.Save('xls', 'Microsoft Excel 4.0 Worksheet (*.xls)|*.xls', 'ExpGrid.xls', dxDBGrid1.SaveToXLS);
end;

procedure TAgendaSettForm.SpeedButton1Click(Sender: TObject);
begin
     Aprihelp('4');
end;

procedure TAgendaSettForm.CBCommAttiveClick(Sender: TObject);
begin
     QCandidati.Close;
     if CBCommAttive.Checked = true then
     begin
          QCandidati.parameters.ParamByName('Xconclusa').value := 1;
          QCandidati.parameters.ParamByName('XIDTipoStato').value := 3;
     end
     else
     begin
          QCandidati.parameters.ParamByName('Xconclusa').value := -1;
          QCandidati.parameters.ParamByName('XIDTipoStato').value := 0;
     end;
     QCandidati.open;

end;

procedure TAgendaSettForm.BarrayClick(Sender: TObject);
var r, c: integer;
begin
     for r := 1 to 6 do begin
          for c := 1 to 12 do begin
               showmessage(inttostr(r) + '    ' + inttostr(c) + chr(13) + IntToStr(xarrayidagenda[r, c]));
          end;
     end;



end;

procedure TAgendaSettForm.QAgendaAfterScroll(DataSet: TDataSet);
begin
     if data.Global.FieldByName('debughrms').asboolean = true then showmessage('scroll=' + QAgenda.fieldbyname('id').asstring);
end;

procedure TAgendaSettForm.CandUtenteFrame1BLegendaClick(Sender: TObject);
begin
     CandUtenteFrame1.BLegendaClick(Sender);
end;

end.

