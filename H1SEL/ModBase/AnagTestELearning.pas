unit AnagTestELearning;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, ADODB, dxCntner, dxTL, dxDBCtrl, dxDBGrid, ExtCtrls, AdvPanel, TB97;

type
     TAnagTestelearningForm = class(TForm)
          AdvPanel1: TAdvPanel;
          dxDBGrid1: TdxDBGrid;
          qAnagtest: TADOQuery;
          dsAnagTest: TDataSource;
          ToolbarButton971: TToolbarButton97;
          ToolbarButton972: TToolbarButton97;
          ToolbarButton973: TToolbarButton97;
          qAnagtestID: TAutoIncField;
          qAnagtestIDAnagrafica: TIntegerField;
          qAnagtestIDTest: TIntegerField;
          qAnagtestID_1: TAutoIncField;
          qAnagtestDescrizioneTest: TStringField;
          qAnagtestCodiceTest: TStringField;
          dxDBGrid1ID: TdxDBGridMaskColumn;
          dxDBGrid1IDAnagrafica: TdxDBGridMaskColumn;
          dxDBGrid1IDTest: TdxDBGridMaskColumn;
          dxDBGrid1ID_1: TdxDBGridMaskColumn;
          dxDBGrid1Descrizione: TdxDBGridMaskColumn;
          dxDBGrid1Codice: TdxDBGridMaskColumn;
          procedure ToolbarButton971Click(Sender: TObject);
          procedure ToolbarButton973Click(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure ToolbarButton972Click(Sender: TObject);
     private
    { Private declarations }
     public
    { Public declarations }
     end;

var
     AnagTestelearningForm: TAnagTestelearningForm;

implementation

uses ModuloDati, TestELearning;

{$R *.DFM}

procedure TAnagTestelearningForm.ToolbarButton971Click(Sender: TObject);
begin
     TestELearningForm := TTestELearningForm.Create(self);
     TestELearningForm.bIns.Visible := true;
     TestELearningForm.bCanc.Visible := true;
     TestELearningForm.bOk.Visible := true;
     TestELearningForm.bDel.Visible := true;
     TestELearningForm.BitBtn1.Visible := false;
     TestELearningForm.dxDBGrid1.OptionsBehavior := [edgoAutoSort, edgoEditing, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoStoreToIniFile, edgoTabThrough, edgoVertThrough];

     TestELEarningForm.ShowModal;

     TestELearningForm.Free;
end;

procedure TAnagTestelearningForm.ToolbarButton973Click(Sender: TObject);
begin
     if Messagedlg('Vuoi cancellare l''associzione al test selezionato?', mtConfirmation, [mbyes, mbNo], 0) = mrYes then begin
          Data.QTemp.close;
          data.Qtemp.sql.text := 'delete from anagtestelearning where idtest=:x0';
          data.qtemp.Parameters[0].Value := qAnagtestIDTest.Value;
          data.qtemp.ExecSQL;

          qanagtest.close;
          qanagtest.open;
     end;
end;

procedure TAnagTestelearningForm.FormShow(Sender: TObject);
begin
     qAnagtest.Close;
     qAnagtest.Parameters[0].Value := Data.TAnagraficaID.Value;
     qanagTest.Open;
end;

procedure TAnagTestelearningForm.ToolbarButton972Click(Sender: TObject);
begin
     TestELearningForm := TTestELearningForm.Create(self);
     TestELearningForm.bIns.Visible := false;
     TestELearningForm.bCanc.Visible := false;
     TestELearningForm.bOk.Visible := false;
     TestELearningForm.bDel.Visible := false;
     TestELearningForm.BitBtn1.Visible := true;
     TestELearningForm.dxDBGrid1.OptionsBehavior := [edgoAutoSort, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoStoreToIniFile, edgoTabThrough, edgoVertThrough];

     TestELEarningForm.ShowModal;
     if TestELearningForm.ModalResult = mrOk then begin
          Data.QTemp.close;
          Data.qtemp.SQL.Text := 'insert into AnagTestELearning(IDAnagrafica,IDTest) values(:x0,:x1)';
          Data.qtemp.Parameters[0].Value := data.TAnagraficaID.Value;
          Data.qtemp.Parameters[1].Value := TestELearningForm.qTestID.Value;
          Data.QTemp.ExecSQL;

          qAnagtest.Close;
          qAnagtest.Open;
     end;
     TestELearningForm.Free;
end;

end.

