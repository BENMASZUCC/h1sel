unit StoriaContatti;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, Grids, DBGrids, Db, DBTables, ADODB, U_ADOLinkCl,
     dxDBTLCl, dxGrClms, dxTL, dxDBCtrl, dxDBGrid, dxCntner, TB97, ExtCtrls;

type
     TStoriaContattiForm = class(TForm)
          dsQStoriaCont: TDataSource;
          QStoriaCont: TADOLinkedQuery;
          QStoriaContID: TAutoIncField;
          QStoriaContIDRicerca: TIntegerField;
          QStoriaContIDAnagrafica: TIntegerField;
          QStoriaContData: TDateTimeField;
          QStoriaContOra: TDateTimeField;
          QStoriaContEsito: TStringField;
          QStoriaContEvaso: TBooleanField;
          QStoriaContNote: TStringField;
          QStoriaContIDMotivoNonAcc: TIntegerField;
          QStoriaContIDUtente: TIntegerField;
          QStoriaContMotivo: TStringField;
          QStoriaContUtente: TStringField;
          QStoriaContTipoContatto: TStringField;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1TipoContatto: TdxDBGridMaskColumn;
          dxDBGrid1Data: TdxDBGridDateColumn;
          dxDBGrid1Ora: TdxDBGridDateColumn;
          dxDBGrid1Esito: TdxDBGridMaskColumn;
          dxDBGrid1Motivo: TdxDBGridMaskColumn;
          dxDBGrid1Utente: TdxDBGridMaskColumn;
    Panel1: TPanel;
    BitBtn1: TToolbarButton97;
    BitBtn2: TToolbarButton97;
    dxDBGrid1Note: TdxDBGridColumn;
          procedure BitBtn1Click(Sender: TObject);
          procedure BitBtn2Click(Sender: TObject);
          procedure FormShow(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     StoriaContattiForm: TStoriaContattiForm;

implementation

uses MDRicerche, ModuloDati, Main;

{$R *.DFM}

procedure TStoriaContattiForm.BitBtn1Click(Sender: TObject);
begin
     close
end;

procedure TStoriaContattiForm.BitBtn2Click(Sender: TObject);
begin
     if QStoriaCont.IsEmpty then exit;
     if MessageDlg('Sei sicuro di voler eliminare la riga ?', mtWarning, [mbNo, mbYes], 0) <> mrYes then exit;
     with Data do begin
          DB.BeginTrans;
          try
               Q1.Close;
               Q1.SQL.text := 'delete from EBC_ContattiCandidati ' +
                    'where ID=' + QStoriaCont.FieldByName('ID').AsString;
               Q1.ExecSQL;
               DB.CommitTrans;
               QStoriaCont.Close;
               QStoriaCont.Open;
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
          end;
     end;

end;

procedure TStoriaContattiForm.FormShow(Sender: TObject);
begin
     //Grafica
     StoriaContattiForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;   
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     Caption := '[M/216] - ' + Caption;
end;

end.
