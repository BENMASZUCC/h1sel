unit StoricoInviiContatto;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     dxDBCtrl, dxDBGrid, dxDBTLCl, dxGrClms, dxTL, dxCntner, Db, ADODB,
     StdCtrls, Buttons, ExtCtrls, TB97;

type
     TStoricoInviiContattoForm = class(TForm)
          Panel1: TPanel;
          QStoricoInvii: TADOQuery;
          DsQStoricoInvii: TDataSource;
          QStoricoInviiID: TAutoIncField;
          QStoricoInviiIDContattoCli: TIntegerField;
          QStoricoInviiIDTipoInvio: TIntegerField;
          QStoricoInviiData: TDateTimeField;
          QStoricoInviiAbitazione: TBooleanField;
          QStoricoInviiNote: TStringField;
          QStoricoInviiTipo: TStringField;
          QStoricoInviiTipoInvio: TStringField;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1Data: TdxDBGridDateColumn;
          dxDBGrid1Abitazione: TdxDBGridCheckColumn;
          dxDBGrid1Note: TdxDBGridMaskColumn;
          dxDBGrid1Tipo: TdxDBGridMaskColumn;
          dxDBGrid1TipoInvio: TdxDBGridMaskColumn;
          BStoricoInviiOk: TToolbarButton97;
          BStoricoInviiCanc: TToolbarButton97;
          BStoricoInviiMod: TToolbarButton97;
          BStoricoInviiDel: TToolbarButton97;
          BEsci: TToolbarButton97;
          procedure DsQStoricoInviiDataChange(Sender: TObject; Field: TField);
          procedure BStoricoInviiModClick(Sender: TObject);
          procedure BStoricoInviiDelClick(Sender: TObject);
          procedure BStoricoInviiOkClick(Sender: TObject);
          procedure BStoricoInviiCancClick(Sender: TObject);
          procedure BEsciClick(Sender: TObject);
          procedure FormShow(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     StoricoInviiContattoForm: TStoricoInviiContattoForm;

implementation

uses ModuloDati, Main;

{$R *.DFM}

procedure TStoricoInviiContattoForm.DsQStoricoInviiDataChange(
     Sender: TObject; Field: TField);
var b: boolean;
begin
     b := DsQStoricoInvii.State in [dsEdit];
     BStoricoInviiOk.Enabled := b;
     BStoricoInviiCanc.Enabled := b;
end;

procedure TStoricoInviiContattoForm.BStoricoInviiModClick(Sender: TObject);
var i: integer;
begin
     if QStoricoInvii.RecordCount = 0 then exit
          //ShowMessage('questo archivio � archivio vuoto: nessun dato da modificare')
     else begin
          {if MessageDlg('ATTENZIONE: Con la seguente procedura verr� modificato lo Storico Invii !!', mtWarning,
               [mbNo, mbYes], 0) = mrNo then exit; }
          for i := 0 to dxDBGrid1.ColumnCount - 1 do
               dxDBGrid1.Columns[i].DisableEditor := false;
          QStoricoInvii.Edit;
     end;
end;

procedure TStoricoInviiContattoForm.BStoricoInviiDelClick(Sender: TObject);
var xfile: TADOQuery;
begin
     if QStoricoInvii.RecordCount = 0 then exit;
     if MessageDlg('ATTENZIONE: la procedura � IRREVERSIBILE e DEFINITIVA !!' + chr(13) +
          'SEI DAVVERO SICURO DI VOLER PROCEDERE ?', mtWarning,
          [mbNo, mbYes], 0) = mrNo then exit;
     xfile := TADOQuery.Create(self);
     xfile.Connection := Data.DB;
     xfile.SQL.text := 'delete from StoricoInviiContatto where id=' + QStoricoInviiID.asstring;
     xfile.ExecSQL;
     QStoricoInvii.Close;
     QStoricoInvii.Open;
     xfile.Close;
     xfile.Free;

     //QStoricoInvii.Delete;
end;

procedure TStoricoInviiContattoForm.BStoricoInviiOkClick(Sender: TObject);
var i: integer;
begin
     for i := 0 to dxDBGrid1.ColumnCount - 1 do
          dxDBGrid1.Columns[i].DisableEditor := True;
     QStoricoInvii.Post;
end;

procedure TStoricoInviiContattoForm.BStoricoInviiCancClick(Sender: TObject);
var i: integer;
begin
     for i := 0 to dxDBGrid1.ColumnCount - 1 do
          dxDBGrid1.Columns[i].DisableEditor := True;
     QStoricoInvii.Cancel;
end;

procedure TStoricoInviiContattoForm.BEsciClick(Sender: TObject);
begin
     StoricoInviiContattoForm.ModalResult := mrOk;
end;

procedure TStoricoInviiContattoForm.FormShow(Sender: TObject);
begin
     StoricoInviiContattoForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
end;

end.

