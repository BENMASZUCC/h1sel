unit RepStatBudgetForm;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, Quickrpt, QRCtrls, TeeProcs, TeEngine, Chart,
  DBChart, QrTee, Series, TeeFunci;

type
  TQRStatBudgetForm = class(TQuickRep)
    QRDBChart1: TQRDBChart;
    QRChart1: TQRChart;
    Series1: TLineSeries;
    Series2: TLineSeries;
    Series3: TLineSeries;
    TeeFunction1: TSubtractTeeFunction;
    QRDBChart2: TQRDBChart;
    QRChart2: TQRChart;
    Series4: TBarSeries;
    Series5: TBarSeries;
    QRDBChart3: TQRDBChart;
    QRChart3: TQRChart;
    Series6: TPieSeries;
    QRDBChart4: TQRDBChart;
    QRChart4: TQRChart;
    Series7: TPieSeries;
    QRLabel1: TQRLabel;
    QRSysData1: TQRSysData;
  private

  public

  end;

var
  QRStatBudgetForm: TQRStatBudgetForm;

implementation

uses MOduloDati6;

{$R *.DFM}

end.
