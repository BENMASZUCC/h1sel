unit ModuloDati2;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, DBTables, FileCtrl, ADODB, U_ADOLinkCl;

type
     TData2 = class(TDataModule)
          DsAziende: TDataSource;
          DsMansioni: TDataSource;
          DsAree: TDataSource;
          DsCompetenze: TDataSource;
          DsCompMansioni: TDataSource;
          DsCompDipendente: TDataSource;
          DsCarattDip: TDataSource;
          DsCaratteristiche: TDataSource;
          DsSpec: TDataSource;
          DsAreeComp: TDataSource;
          dsEBCClienti: TDataSource;
          DsContattiClienti: TDataSource;
          DsInserimCliente: TDataSource;
          DsAttivita: TDataSource;
          DsRicClienti: TDataSource;
          DsTotCompenso: TDataSource;
          DsFattClienti: TDataSource;
          DsAnagRicerche: TDataSource;
          DsCliContattiFatti: TDataSource;
          DsCliOfferte: TDataSource;
          DsQTotFattCli: TDataSource;
          DsNoteSpeseCli: TDataSource;
          DsCompensiRic: TDataSource;
          DsQTotComp2: TDataSource;
          DsQCliCV: TDataSource;
          DsQCliContratti: TDataSource;
          DsQJobDesc: TDataSource;
          DsQCliRefCli: TDataSource;
          DsQFornContiCosto: TDataSource;
          DsQFornCosti: TDataSource;
          DsQAreeAttivita: TDataSource;
          DsQClienteFile: TDataSource;
          TAziende: TADOLinkedTable;
          TCompMansioni: TADOLinkedTable;
          TAreeComp: TADOLinkedTable;
          TAreeCompLK: TADOLinkedTable;
          TCaratteristiche: TADOLinkedTable;
          TSpec: TADOLinkedTable;
          TClientiABS_old: TADOLinkedTable;
          TCompMansIDX: TADOLinkedTable;
          TStoricoCompABS: TADOLinkedTable;
          TMansioniLK: TADOLinkedTable;
          TStatoRicLK: TADOLinkedTable;
          TCompetenzeLK: TADOLinkedTable;
          TUsersLK: TADOLinkedTable;
          TAreeLK: TADOLinkedTable;
          TInserimentiABS: TADOLinkedTable;
          TClientiLK: TADOLinkedTable;
          TAnagMansIDX: TADOLinkedTable;
          TAnagIDX: TADOLinkedTable;
          TOfferteLK: TADOLinkedTable;
          TAnagCompIDX: TADOLinkedTable;
          TAreaCompIDX: TADOLinkedTable;
          TFattureLK: TADOLinkedTable;
          TCompIDX: TADOLinkedTable;
          TTimingRic: TADOLinkedTable;
          QTotComp2_old: TADOLinkedQuery;
          TCompetenze: TADOLinkedQuery;
          QJobDesc: TADOLinkedQuery;
          TCliOfferte_old: TADOLinkedQuery;
          TCompensiRic: TADOLinkedQuery;
          TCompDipendente: TADOLinkedQuery;
          QAnagRicerche: TADOLinkedQuery;
          QCompRuolo: TADOLinkedQuery;
          QFornCosti: TADOLinkedQuery;
          QAreeAttivita: TADOLinkedQuery;
          QTotCompenso_old: TADOLinkedQuery;
          TMansioniABS: TADOLinkedQuery;
          TAreeABS: TADOLinkedQuery;
          QInserimCli: TADOLinkedQuery;
          QCheck: TADOLinkedQuery;
          QAttivita: TADOLinkedQuery;
          TNoteSpeseCli: TADOLinkedQuery;
          TCarattDip: TADOLinkedQuery;
          QCliCV: TADOLinkedQuery;
          QCliRefCli: TADOLinkedQuery;
          QTotFattCli: TADOLinkedQuery;
          QFornContiCosto: TADOLinkedQuery;
          TAree: TADOLinkedQuery;
          TContattiClienti: TADOLinkedQuery;
          QTemp: TADOLinkedQuery;
          TFattClientiOLD: TADOLinkedTable;
          TCompensiRicID: TAutoIncField;
          TCompensiRicIDRicerca: TIntegerField;
          TCompensiRicTipo: TStringField;
          TCompensiRicImporto: TFloatField;
          TCompensiRicDataPrevFatt: TDateTimeField;
          TCompensiRicIDFattura: TIntegerField;
          TCompensiRicNote: TStringField;
          TCompensiRicIDOffertaDett: TIntegerField;
          QTotCompenso_oldTotCompenso: TFloatField;
          QTotComp2_oldTotCompenso: TFloatField;
          QTotFattCliTotCliente: TFloatField;
          TNoteSpeseCliID: TAutoIncField;
          TNoteSpeseCliIDCliente: TIntegerField;
          TNoteSpeseCliIDRicerca: TIntegerField;
          TNoteSpeseCliIDUtente: TIntegerField;
          TNoteSpeseCliIDAnagrafica: TIntegerField;
          TNoteSpeseCliData: TDateTimeField;
          TNoteSpeseCliDescrizione: TStringField;
          TNoteSpeseCliAutomobile: TFloatField;
          TNoteSpeseCliAutostrada: TFloatField;
          TNoteSpeseCliAereoTreno: TFloatField;
          TNoteSpeseCliTaxi: TFloatField;
          TNoteSpeseCliAlbergo: TFloatField;
          TNoteSpeseCliRistoranteBar: TFloatField;
          TNoteSpeseCliParking: TFloatField;
          TNoteSpeseCliAltroDesc: TStringField;
          TNoteSpeseCliAltroImporto: TFloatField;
          TNoteSpeseCliIDFattura: TIntegerField;
          TNoteSpeseCliRifRicerca: TStringField;
          TNoteSpeseCliUtente: TStringField;
          TNoteSpeseCliTotale: TFloatField;
          TFattClientiOLDID: TAutoIncField;
          TFattClientiOLDTipo: TStringField;
          TFattClientiOLDRifProg: TIntegerField;
          TFattClientiOLDIDCliente: TIntegerField;
          TFattClientiOLDData: TDateTimeField;
          TFattClientiOLDDecorrenza: TDateTimeField;
          TFattClientiOLDImporto: TFloatField;
          TFattClientiOLDIVA: TFloatField;
          TFattClientiOLDTotale: TFloatField;
          TFattClientiOLDModalitaPagamento: TStringField;
          TFattClientiOLDAssegno: TStringField;
          TFattClientiOLDScadenzaPagamento: TDateTimeField;
          TFattClientiOLDPagata: TBooleanField;
          TFattClientiOLDPagataInData: TDateTimeField;
          TFattClientiOLDAppoggioBancario: TStringField;
          TFattClientiOLDIDRicerca: TIntegerField;
          TFattClientiOLDResponsabile: TStringField;
          TFattClientiOLDFlagFuoriAppIVA63372: TBooleanField;
          TCompetenzeLKID: TAutoIncField;
          TCompetenzeLKDescrizione: TStringField;
          TCompetenzeLKTipologia: TStringField;
          TCompMansioniID: TAutoIncField;
          TCompMansioniIDCompetenza: TIntegerField;
          TCompMansioniIDMansione: TIntegerField;
          TCompMansioniOperatore: TStringField;
          TCompMansioniValore: TIntegerField;
          TCompMansioniIndispensabile: TBooleanField;
          TCompMansioniLKMansione: TStringField;
          TCompMansioniDescCompetenza: TStringField;
          TCompMansioniTipologia: TStringField;
          TAreeCompLKID: TAutoIncField;
          TAreeCompLKDescrizione: TStringField;
          TAreeCompLKIDAzienda: TIntegerField;
          TCompetenzeLKIDArea: TIntegerField;
          TCompetenzeLKArea: TStringField;
          TCompMansioniArea: TStringField;
          TMansioni: TADOLinkedQuery;
          QContrattiLK: TADOLinkedQuery;
          QContrattiLKID: TAutoIncField;
          QContrattiLKIDCliente: TIntegerField;
          QContrattiLKData: TDateTimeField;
          QContrattiLKDescrizione: TStringField;
          QContrattiLKStato: TStringField;
          QContrattiLKCodice: TStringField;
          QAnagRicercheProgressivo: TStringField;
          QAnagRicercheCliente: TStringField;
          QAnagRicercheRuolo: TStringField;
          QAnagRicercheEscluso: TBooleanField;
          QAnagRicercheStatoRic: TStringField;
          QAnagRicercheDataInizio: TDateTimeField;
          QAnagRicercheDataFine: TDateTimeField;
          QAnagRicercheID: TAutoIncField;
          QAnagRicercheIDRicerca: TIntegerField;
          QAnagRicercheUtente: TStringField;
          QAnagRicercheIDAnagrafica: TIntegerField;
          QCliRefCliID: TAutoIncField;
          QCliRefCliIDCliente: TIntegerField;
          QCliRefCliIDClienteRef: TIntegerField;
          QCliRefCliDescrizione: TStringField;
          QCliRefCliCheckCand: TBooleanField;
          QCliRefCliClienteRef: TStringField;
          QCompRuoloID: TAutoIncField;
          QCompRuoloIDCompetenza: TIntegerField;
          QCompRuoloIDMansione: TIntegerField;
          QCompRuoloOperatore: TStringField;
          QCompRuoloValore: TIntegerField;
          QCompRuoloIndispensabile: TBooleanField;
          QCompRuoloLKMansione: TStringField;
          QCompRuoloDescCompetenza: TStringField;
          DsQRelCliUtenti: TDataSource;
          QTipoRelCliUtente: TADOQuery;
          QTipoRelCliUtenteID: TAutoIncField;
          QTipoRelCliUtenteTipoRelCliUtente: TStringField;
          QUsersLK: TADOLinkedQuery;
          QAttivitaID: TAutoIncField;
          QAttivitaAttivita: TStringField;
          Q: TADOLinkedQuery;
          QTipiAziendeLK: TADOLinkedQuery;
          QTipiAziendeLKID: TAutoIncField;
          QTipiAziendeLKTipoAzienda: TStringField;
          DsQCliModulistica: TDataSource;
          QSinonimi: TADOLinkedQuery;
          DsQSinonimi: TDataSource;
          QSinonimiID: TAutoIncField;
          QSinonimiIDMansione: TIntegerField;
          QSinonimiSinonimo: TStringField;
          TCompensiRicRimbSpese: TBooleanField;
          TFattClientiOLDRimbSpese: TBooleanField;
          TCompensiRicemessa: TDateTimeField;
          QAttivitaIDAreaSettore: TIntegerField;
          QAttivitaAreaSettore: TStringField;
          TFattClientiOLDProgressivo: TStringField;
          QAnagRicercheTipo: TStringField;
          TCompensiRicProgFatt: TStringField;
          TNoteSpeseCliRifFattura: TStringField;
          TCompMansioniFlag_Internet: TBooleanField;
          TContattiClientiIDMansione: TIntegerField;
          TContattiClientiID: TAutoIncField;
          TContattiClientiIDCliente: TIntegerField;
          TContattiClientiContatto: TStringField;
          TContattiClientiTelefono: TStringField;
          TContattiClientiFax: TStringField;
          TContattiClientiNote: TMemoField;
          TContattiClientiemail: TStringField;
          TContattiClientiCompleannoGiorno: TSmallintField;
          TContattiClientiCompleannoMese: TSmallintField;
          TContattiClientiCaricaAziendale: TStringField;
          TContattiClientiTitoloStudio: TStringField;
          TContattiClientiNomeSegretaria: TStringField;
          TContattiClientiIndirizzoPrivato: TStringField;
          TContattiClientiCapPrivato: TStringField;
          TContattiClientiProvinciaPrivato: TStringField;
          TContattiClientiTelefonoAbitazione: TStringField;
          TContattiClientiDataIns: TDateTimeField;
          TContattiClientiIDUtente: TIntegerField;
          TContattiClientiTitolo: TStringField;
          TContattiClientiStato: TStringField;
          TContattiClientiCellulareAzienda: TStringField;
          TContattiClientiCellularePrivato: TStringField;
          TContattiClientiFaxPrivato: TStringField;
          TContattiClientiEmailPrivato: TStringField;
          TContattiClientiEtichettaAbitazione: TBooleanField;
          TContattiClientiComunePrivato: TStringField;
          TContattiClientiSesso: TStringField;
          TContattiClientiTipoStradaPrivato: TStringField;
          TContattiClientiNumCivicoPrivato: TStringField;
          TContattiClientiDivisioneAziendale: TStringField;
          TContattiClientiSoloMaster: TBooleanField;
          TContattiClientiIDAttributo: TIntegerField;
          TContattiClientiIDAnagrafica: TIntegerField;
          TContattiClientiLKruolo: TStringField;
          TMansioniLKID: TAutoIncField;
          TMansioniLKDescrizione: TStringField;
          TMansioniLKIDArea: TIntegerField;
          TMansioniLKFileMansionario: TStringField;
          TMansioniLKMansioni: TMemoField;
          TMansioniLKLKArea: TStringField;
          TMansioniLKTolleranzaPerc: TIntegerField;
          TMansioniLKDescrizione_ENG: TStringField;
          TMansioniLKidazienda: TIntegerField;
          TMansioniLKIDGrade: TIntegerField;
          TMansioniLKRapportiRuolo: TMemoField;
          QRegoleClienti: TADOQuery;
          QRegoleClientiID: TAutoIncField;
          QRegoleClientiDescrizione: TStringField;
          QRegoleClientiFlag: TBooleanField;
          DSRegoleClienti: TDataSource;
          TCliOfferte: TADOQuery;
          Q1: TADOQuery;
          Q2: TADOQuery;
          QCompRuoloPeso: TBCDField;
          TCompMansioniPeso: TBCDField;
          TFattClientiOLDLetteraIntento: TBooleanField;
          TFattClienti: TADOQuery;
          TFattClientiOLDIDFilialeOrigine: TIntegerField;
          TFattClientiOLDIDFilialeChiusura: TIntegerField;
          TEbcClienti: TADOQuery;
          TEbcClientiID: TAutoIncField;
          TEbcClientiDescrizione: TStringField;
          TEbcClientiStato: TStringField;
          TEbcClientiCap: TStringField;
          TEbcClientiComune: TStringField;
          TEbcClientiProvincia: TStringField;
          TEbcClientiIDAttivita: TIntegerField;
          TEbcClientiTelefono: TStringField;
          TEbcClientiFax: TStringField;
          TEbcClientiPartitaIVA: TStringField;
          TEbcClientiCodiceFiscale: TStringField;
          TEbcClientiBancaAppoggio: TStringField;
          TEbcClientiSistemaPagamento: TStringField;
          TEbcClientiNoteContratto: TMemoField;
          TEbcClientiResponsabile: TStringField;
          TEbcClientiConosciutoInData: TDateTimeField;
          TEbcClientiCapLegale: TStringField;
          TEbcClientiSitoInternet: TStringField;
          TEbcClientiProvinciaLegale: TStringField;
          TEbcClientiNumDipendenti: TSmallintField;
          TEbcClientiFatturato: TFloatField;
          TEbcClientiCartellaDoc: TStringField;
          TEbcClientiTipoStrada: TStringField;
          TEbcClientiNumCivico: TStringField;
          TEbcClientiTipoStradaLegale: TStringField;
          TEbcClientiNumCivicoLegale: TStringField;
          TEbcClientiNazioneAbb: TStringField;
          TEbcClientiNazioneAbbLegale: TStringField;
          TEbcClientiIDRegolaCliente: TIntegerField;
          TEbcClientiIDClienteFiliale: TIntegerField;
          TEbcClientiBlocco1: TBooleanField;
          TEbcClientiEnteCertificatore: TStringField;
          TEbcClientiLibPrivacy: TBooleanField;
          TEbcClientiIDTipoAzienda: TIntegerField;
          TEbcClientiDescrizAnnunci: TStringField;
          TEbcClientiAttivita: TStringField;
          TEbcClientiTipoAzienda: TStringField;
          QCliContratti: TADOQuery;
          TCliContattiFatti: TADOQuery;
          TCliContattiFattiID: TAutoIncField;
          TCliContattiFattiIDCliente: TIntegerField;
          TCliContattiFattiTipoContatto: TStringField;
          TCliContattiFattiData: TDateTimeField;
          TCliContattiFattiOre: TDateTimeField;
          TCliContattiFattiParlatoCon: TStringField;
          TCliContattiFattiDescrizione: TStringField;
          TCliContattiFattiIDOfferta: TIntegerField;
          TCliContattiFattiIDContattoCli: TIntegerField;
          TCliContattiFattiIDUtente: TIntegerField;
          QRelCliUtenti: TADOQuery;
          QRelCliUtentiIDCliente: TIntegerField;
          QRelCliUtentiIDUtente: TIntegerField;
          QRelCliUtentiIDTipoRelCliUtente: TIntegerField;
          QRelCliUtentiNote: TMemoField;
          QRelCliUtentiiTipoRelCliUtente: TStringField;
          QRelCliUtentiUtente: TStringField;
          QCliModulistica: TADOQuery;
          QCliModulisticaNomeModulo: TStringField;
          QCliModulisticaID: TAutoIncField;
          QCliModulisticaIDCliente: TIntegerField;
          QCliModulisticaIDModulo: TIntegerField;
          QCliModulisticaFlagOK: TBooleanField;
          QCliModulisticaUltimaData: TDateTimeField;
          QRelCliUtentiID: TAutoIncField;
          QClienteFile: TADOQuery;
          QRicClienti: TADOQuery;
          qUsers2LK: TADOQuery;
          qUsers2LKID: TAutoIncField;
          qUsers2LKNominativo: TStringField;
          TCliContattiFattiUtente: TStringField;
          TEbcClientiIndirizzo: TStringField;
          TEbcClientiComuneLegale: TStringField;
          TEbcClientiIndirizzoLegale: TStringField;
          QTotCompenso: TADOQuery;
          QTotCompensoTotCompenso: TFloatField;
          QTotComp2: TADOQuery;
          TClientiABS: TADOQuery;
          TEbcClientiIDConoscTramite: TIntegerField;
          QTabConoscTramiteLK: TADOQuery;
          QTabConoscTramiteLKID: TAutoIncField;
          QTabConoscTramiteLKTramite: TStringField;
          TEbcClientiConoscTramite: TStringField;
          TMansioniID: TAutoIncField;
          TMansioniDescrizione: TStringField;
          TMansioniIDArea: TIntegerField;
          TMansioniFileMansionario: TStringField;
          TMansioniMansioni: TMemoField;
          TMansioniLKArea: TStringField;
          TMansioniTolleranzaPerc: TIntegerField;
          TMansioniDescrizione_ENG: TStringField;
          TMansioniidazienda: TIntegerField;
          TMansioniIDGrade: TIntegerField;
          TMansioniRapportiRuolo: TMemoField;
          TMansioniFileType: TStringField;
          TMansioniDirFileMansionario: TStringField;
          TMansioniTolleranza_2: TIntegerField;
          TMansioniCodice: TStringField;
          TMansioniVisibileWeb: TBooleanField;
          TNoteSpeseCliCandidato: TStringField;
          TNoteSpeseCliCognome: TStringField;
          TNoteSpeseCliNome: TStringField;
          TEbcClientiLetteraIntento: TBooleanField;
          TEbcClientiDataLetteraIntenti: TDateTimeField;
          TCliContattiFattiDataProssimoContatto: TDateTimeField;
          TAreeID: TAutoIncField;
          TAreeDescrizione: TStringField;
          TAreeIDAzienda: TIntegerField;
          TAreeDescrizione_ENG: TStringField;
          TCliOfferteSedeOperativa: TStringField;
          TCliOfferteID: TIntegerField;
          TCliOfferteIDCliente: TIntegerField;
          TCliOfferteRif: TStringField;
          TCliOfferteData: TDateTimeField;
          TCliOfferteAMezzo: TStringField;
          TCliOfferteAttenzioneDi: TStringField;
          TCliOfferteAnticipoRichiesto: TFloatField;
          TCliOfferteCondizioni: TStringField;
          TCliOfferteEsito: TStringField;
          TCliOfferteNote: TStringField;
          TCliOfferteStato: TStringField;
          TCliOfferteTipo: TStringField;
          TCliOfferteIDUtente: TIntegerField;
          TCliOfferteImportoTotale: TFloatField;
          TCliOfferteIDRicerca: TIntegerField;
          TCliOfferteIDContratto: TIntegerField;
          TCliOfferteIDContrattoCG: TIntegerField;
          TCliOfferteIDArea: TIntegerField;
          TCliOfferteIDLineaProdotto: TIntegerField;
          TCliOfferteIDSedeOperativa: TIntegerField;
          TCliOffertePrimaVendita: TBooleanField;
          TCliOfferteUpSelling: TBooleanField;
          TCliOfferteTotProdServ: TFloatField;
          TCliOfferteCodContratto: TStringField;
          TCliOfferteTitoloCliente: TStringField;
          DSTCliModulistica: TDataSource;
          TCliModulistica: TADOQuery;
          TCliModulisticaID: TAutoIncField;
          TCliModulisticaIDCliente: TIntegerField;
          TCliModulisticaIDModulo: TIntegerField;
          TCliModulisticaFlagOK: TBooleanField;
          TCliModulisticaUltimaData: TDateTimeField;
          TCliTabModulisticaLK: TADOQuery;
          TCliModulisticaNomeModulo: TStringField;
          TCompensiRicDescrizione: TStringField;
          TCliOfferteIDCondizione: TIntegerField;
          TContattiClientiUscito: TBooleanField;
    TCliOfferteDataPrevChiusura: TDateTimeField;
    QClienteLogo: TADOQuery;
    DsQClienteLogo: TDataSource;
    QClienteLogoID: TAutoIncField;
    QClienteLogoLogo: TBlobField;
    QClienteLogoExtLogo: TStringField;
    TCliContattiFattiIDEsitoContatto: TIntegerField;
    TCliContattiFattiIDContattoAttivitaFutura: TIntegerField;
    qEsitoContattiLK: TADOQuery;
    qEsitoContattiLKID: TAutoIncField;
    qEsitoContattiLKCodice: TStringField;
    qEsitoContattiLKDescrizione: TStringField;
    qContattiAttivitaFutureLK: TADOQuery;
    qContattiAttivitaFutureLKID: TAutoIncField;
    qContattiAttivitaFutureLKCodice: TStringField;
    qContattiAttivitaFutureLKDescrizione: TStringField;
    TCliContattiFattiEsitoContatto: TStringField;
    TCliContattiFattiAttivitaFuture: TStringField;
          procedure DsCaratteristicheStateChange(Sender: TObject);
          procedure DsCompMansioniStateChange(Sender: TObject);
          procedure Data2Create(Sender: TObject);
          procedure TMansDipAfterScroll(DataSet: TDataSet);
          procedure TCompMansioni_OLDAfterPost(DataSet: TDataSet);
          procedure TAnagMansIDX_OLDAfterPost(DataSet: TDataSet);
          procedure TCompMansioni_OLDAfterInsert(DataSet: TDataSet);
          procedure dsEBCClientiStateChange(Sender: TObject);
          procedure TEBCClienti_OLDAfterPost(DataSet: TDataSet);
          procedure TEBCClienti_OLDBeforePost(DataSet: TDataSet);
          procedure TFattClienti_OLDBeforeDelete(DataSet: TDataSet);
          procedure TCompetenzeLK_OLDBeforeOpen(DataSet: TDataSet);
          procedure TCompetenzeLK_OLDAfterClose(DataSet: TDataSet);
          procedure TSpec_OLDBeforeOpen(DataSet: TDataSet);
          procedure TSpec_OLDAfterClose(DataSet: TDataSet);
          procedure TMansioniLK_OLDBeforeOpen(DataSet: TDataSet);
          procedure TMansioniLK_OLDAfterClose(DataSet: TDataSet);
          procedure TAnagMansIDX_OLDBeforeOpen(DataSet: TDataSet);
          procedure TAnagMansIDX_OLDAfterClose(DataSet: TDataSet);
          procedure TCompMansioni_OLDBeforeOpen(DataSet: TDataSet);
          procedure TCompMansioni_OLDAfterClose(DataSet: TDataSet);
          procedure QCompRuolo_OLDBeforePost(DataSet: TDataSet);
          procedure QCompRuolo_OLDAfterClose(DataSet: TDataSet);
          procedure TCliContattiFatti_OLDBeforeOpen(DataSet: TDataSet);
          procedure TCliContattiFatti_OLDAfterClose(DataSet: TDataSet);
          procedure TNoteSpeseCli_OLDCalcFields(DataSet: TDataSet);
          procedure TCaratteristiche_OLDAfterPost(DataSet: TDataSet);
          procedure TEBCClienti_OLDAfterScroll(DataSet: TDataSet);
          procedure QRicClienti_OLDCalcFields(DataSet: TDataSet);
          procedure TEBCClienti_OLDAfterClose(DataSet: TDataSet);
          procedure DsQJobDescStateChange(Sender: TObject);
          procedure DsAttivitaStateChange(Sender: TObject);
          procedure QAttivita_OLDAfterPost(DataSet: TDataSet);
          procedure QRicClienti_OLDAfterScroll(DataSet: TDataSet);
          procedure TEBCClientiOLDAfterOpen(DataSet: TDataSet);
          procedure QUsersLKBeforeOpen(DataSet: TDataSet);
          procedure DsQRelCliUtentiStateChange(Sender: TObject);
          procedure QRelCliUtentiAfterInsert(DataSet: TDataSet);
          procedure QRelCliUtentiBeforePost(DataSet: TDataSet);
          procedure QRelCliUtentiBeforeEdit(DataSet: TDataSet);
          procedure TEBCClientiOLDBeforeEdit(DataSet: TDataSet);
          procedure DsMansioniDataChange(Sender: TObject; Field: TField);
          procedure QCliModulisticaBeforeOpen(DataSet: TDataSet);
          procedure DsQCliModulisticaStateChange(Sender: TObject);
          procedure DsQSinonimiStateChange(Sender: TObject);
          procedure TMansioniAfterOpen(DataSet: TDataSet);
          procedure TMansioniAfterClose(DataSet: TDataSet);
          procedure QSinonimiAfterPost(DataSet: TDataSet);
          procedure QSinonimiAfterInsert(DataSet: TDataSet);
          procedure QRicClientiOLDBeforeOpen(DataSet: TDataSet);
          procedure QAnagRicercheBeforeOpen(DataSet: TDataSet);
          procedure TCliOfferteBeforeOpen(DataSet: TDataSet);
          procedure TCliContattiFattiBeforeOpen(DataSet: TDataSet);
          procedure QCliRefCliBeforeOpen(DataSet: TDataSet);
          procedure DSTCliModulisticaStateChange(Sender: TObject);
     private
          { Private declarations }
          xVecchioVal, xIDComp, xIDCaratt, xValore, xIDAreaComp, xIDAnag: integer;
          xVecchioCC, xVecchioDip, xVecchioIDDip, xIDCentroCosto, xIDOldCliente: integer;
          xComp, xAreaComp, xGradoCC: string;
          xNuovaComp, xCompSingola: boolean;
     public
          { Public declarations }
          xValutazDipForm, xColloquioForm, xSelComp,
               xRaffrontoForm, xValutazCandForm, xNuovoRuolo, xSelArea, xCheckLivello: boolean;
          xMot: string;
          xIDAttivita: integer;
          procedure AllineaIndirizzoPrincipale(xIDCliente: integer);
     end;

var
     Data2: TData2;

implementation

uses ModuloDati, Main, SelArea, ValutazDip, CompInserite, InsCompetenza,
     InsRuolo, InsCaratt, NuovaComp, Competenza, ElencoDip, ElencoRicCliente;

{$R *.DFM}

procedure TData2.DsCaratteristicheStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsCaratteristiche.State in [dsEdit, dsInsert];
     MainForm.TbBCarattNew.Enabled := not b;
     MainForm.TbBCarattDel.Enabled := not b;
     MainForm.TbBCarattOK.Enabled := b;
     MainForm.TbBCarattCan.Enabled := b;
end;

procedure TData2.DsCompMansioniStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsCompMansioni.State in [dsEdit, dsInsert];
     MainForm.TbBCompMansNew.Enabled := not b;
     MainForm.TbBCompMansDel.Enabled := not b;
     MainForm.TbBCompMansOK.Enabled := b;
     MainForm.TbBCompMansCan.Enabled := b;
end;

procedure TData2.Data2Create(Sender: TObject);
begin
     xValutazDipForm := False;
     xValutazCandForm := False;
     xColloquioForm := False;
     xRaffrontoForm := False;
     xCheckLivello := True;
     xSelComp := True;
     xMot := '';
     xVecchioCC := 0;
     xVecchioIDDip := 0;
     xNuovaComp := False;
     xSelArea := True;
end;

procedure TData2.TMansDipAfterScroll(DataSet: TDataSet);
begin
     {     if xRaffrontoForm then begin
             RaffrontoCompForm.DBChart3.RefreshData;
             RaffrontoCompForm.DBChart4.RefreshData;
          end;
     }
     if xValutazDipForm then begin
          ValutazDipForm.DBChart3.RefreshData;
          ValutazDipForm.DBChart4.RefreshData;
     end;
end;

procedure TData2.TCompMansioni_OLDAfterPost(DataSet: TDataSet);
var B: TBookmark;
     xTotPerc: integer;
     AlmenoUnoVuoto: boolean;
begin
     TCompetenzeLK.Filter := '';
     TCompetenzeLK.Filtered := False;
     if TCompDipendente.Active then TCompDipendente.Refresh;

     // controllo somma percentuali
     TCompMansioni.DisableControls;
     B := TCompMansioni.GetBookmark;
     TCompMansioni.First;
     xTotPerc := 0;
     AlmenoUnoVuoto := False;
     while not TCompMansioni.EOF do begin
          xTotPerc := xTotPerc + TCompMansioni.FieldByName('Peso').asInteger;
          if TCompMansioni.FieldByName('Peso').asString = '' then AlmenoUnoVuoto := True;
          TCompMansioni.Next;
     end;
     TCompMansioni.GotoBookmark(B);
     TCompMansioni.FreeBookmark(B);
     TCompMansioni.EnableControls;
     if (xTotPerc <> 100) and (not AlmenoUnoVuoto) then MessageDlg('Totale percentuali diverso da 100 !!', mtError, [mbOK], 0);
end;

procedure TData2.TAnagMansIDX_OLDAfterPost(DataSet: TDataSet);
var xIDAnag, xIDMans, xTotComp, i: integer;
     xComp: array[1..50] of integer;
begin
end;

procedure TData2.TCompMansioni_OLDAfterInsert(DataSet: TDataSet);
begin
     //     TCompMansioniIDCompetenza.Value:=InsCompetenzaForm.TCompAreaID.Value;
     //     InsCompetenzaForm.Free;
end;

procedure TData2.dsEBCClientiStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsEBCclienti.State in [dsEdit, dsInsert];
     MainForm.TbBClientiNew.Enabled := not b;
     MainForm.TbBClientiDel.Enabled := not b;
     MainForm.TbBClientiMod.Enabled := not b;
     MainForm.PanCliente.Enabled := b;
     MainForm.TbBClientiOK.Enabled := b;
     MainForm.TbBClientiCan.Enabled := b;
     MainForm.BSaveFatture.Visible := b;
     //MainForm.TbBClientiNoteOK.Enabled:=b;
     //MainForm.TbBClientiNoteCan.Enabled:=b;
end;

//[/TONI20020724\]

procedure TData2.TEBCClienti_OLDAfterPost(DataSet: TDataSet);
begin
     MainForm.LTotAz.Caption := IntToStr(TEBCclienti.RecordCount);
     with Data2.TEBCclienti do begin
          Data.DB.BeginTrans;
          try
               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               raise;
          end;
     end;
     AllineaIndirizzoPrincipale(TEBCClientiID.Value);
end;

procedure TData2.TEBCClienti_OLDBeforePost(DataSet: TDataSet);
begin
     // controllo sulla partitaIVA per SELTIS - OPENJOB
     if pos('OPENJOB', UpperCase(data.Global.fieldbyname('NomeAzienda').asString)) > 0 then begin
          if (MainForm.DBEdit100.Text <> '') and (Length(MainForm.DBEdit100.Text) < 11) then begin
               MessageDlg('La Partita IVA Deve avere la lunghezza di 11 cifre', mtError, [mbOK], 0);
               MainForm.DBEdit100.Text := '';
               TEbcClientiPartitaIVA.AsString := '';
          end;
     end;
     if trim(DataSet.FieldByName('Descrizione').value) = '' then begin
          MessageDlg('ATTENZIONE: denominazione vuota ', mtError, [mbOK], 0);
          Abort;
     end;
     Data.QTemp.Close;
     Data.QTemp.SQL.text := 'update EBC_Clienti set IDAttivita=' + IntToStr(xIDAttivita) + ' where ID=' + TEBCClientiID.asString;
     Data.QTemp.ExecSQL;
end;

procedure TData2.TFattClienti_OLDBeforeDelete(DataSet: TDataSet);
begin
     // cancellazione Dettagli
     QTemp.Close;
     QTemp.SQL.Clear;
     QTemp.SQL.Add('delete from FattDett where IDFattura=' + DataSet.FieldByName('ID').asString);
     QTemp.ExecSQL;

     // recupero inserimenti
     if MessageDlg('Vuoi recuperare gli inserimenti per poterli inserire in altre fatture ?', mtInformation, [mbYes, mbNo], 0) = mrYes then begin
          QTemp.Close;
          QTemp.SQL.Clear;
          QTemp.SQL.Add('update EBC_Inserimenti set ProgFattura=null where ProgFattura=' + DataSet.FieldByName('Progressivo').asString);
          QTemp.ExecSQL;
     end;
end;

procedure TData2.TCompetenzeLK_OLDBeforeOpen(DataSet: TDataSet);
begin
     TAreeCompLK.Open;
end;

procedure TData2.TCompetenzeLK_OLDAfterClose(DataSet: TDataSet);
begin
     TAreeCompLK.Close;
end;

procedure TData2.TSpec_OLDBeforeOpen(DataSet: TDataSet);
begin
     TAreeLK.Open;
end;

procedure TData2.TSpec_OLDAfterClose(DataSet: TDataSet);
begin
     TAreeLK.Close;
end;

procedure TData2.TMansioniLK_OLDBeforeOpen(DataSet: TDataSet);
begin
     TAreeLK.Open;
end;

procedure TData2.TMansioniLK_OLDAfterClose(DataSet: TDataSet);
begin
     TAreeLK.Close;
end;

procedure TData2.TAnagMansIDX_OLDBeforeOpen(DataSet: TDataSet);
begin
     TMansioniLK.Open;
end;

procedure TData2.TAnagMansIDX_OLDAfterClose(DataSet: TDataSet);
begin
     TMansioniLK.Close;
end;

procedure TData2.TCompMansioni_OLDBeforeOpen(DataSet: TDataSet);
begin
     TCompetenzeLK.Open;
end;

procedure TData2.TCompMansioni_OLDAfterClose(DataSet: TDataSet);
begin
     TCompetenzeLK.Close;
end;

procedure TData2.QCompRuolo_OLDBeforePost(DataSet: TDataSet);
begin
     TCompetenzeLK.Open;
end;

procedure TData2.QCompRuolo_OLDAfterClose(DataSet: TDataSet);
begin
     TCompetenzeLK.Close;
end;

procedure TData2.TCliContattiFatti_OLDBeforeOpen(DataSet: TDataSet);
begin
     TOfferteLK.Open;
end;

procedure TData2.TCliContattiFatti_OLDAfterClose(DataSet: TDataSet);
begin
     TOfferteLK.Close;
end;

procedure TData2.TNoteSpeseCli_OLDCalcFields(DataSet: TDataSet);
begin
     TNoteSpeseCliTotale.Value := TNoteSpeseCliAutomobile.Value +
          TNoteSpeseCliAutostrada.Value +
          TNoteSpeseCliAereoTreno.Value +
          TNoteSpeseCliTaxi.Value +
          TNoteSpeseCliAlbergo.Value +
          TNoteSpeseCliRistoranteBar.Value +
          TNoteSpeseCliParking.Value +
          TNoteSpeseCliAltroImporto.Value;
end;

procedure TData2.TCaratteristiche_OLDAfterPost(DataSet: TDataSet);
begin
     TCaratteristiche.Close;
     TCaratteristiche.open;
end;

procedure TData2.TEBCClienti_OLDAfterScroll(DataSet: TDataSet);
begin
     if TEBCClienti.FieldByName('Stato').AsString = 'fornitore' then MainForm.TSCliForn.TabVisible := True
     else MainForm.TSCliForn.TabVisible := False;
     if MainForm.PCClienti.ActivePage = MainForm.TSClientiContratto then begin
          if TEBCClienti.isempty then exit;
          if TEBCClienti.FieldByName('CartellaDoc').AsString <> '' then begin
               if DirectoryExists(TEBCClienti.FieldByName('CartellaDoc').AsString) then
                    MainForm.DirCartellaCli.Directory := DataSet.FieldByname('CartellaDoc').Value;
          end else MainForm.DirCartellaCli.Directory := 'c:\';
     end;
     if MainForm.PCClienti.ActivePage = MainForm.TSClientiCV then begin
          if TEBCClienti.isempty then exit;
          MainForm.CliCandidatiFrame1.xIDCliente := DataSet.FieldByname('ID').AsInteger;
          MainForm.CliCandidatiFrame1.xIDRicerca := 0;
          MainForm.CliCandidatiFrame1.CreaAlbero;
          QCliRefCli.Close;
          QCliRefCli.Open;
     end;
     if MainForm.PCClienti.ActivePage = MainForm.TSClientiAnnunci then begin
          if TEBCClienti.isempty then exit;
          MainForm.ClienteAnnunciFrame.xIDCliente := DataSet.FieldByname('ID').AsInteger;
          MainForm.ClienteAnnunciFrame.xCliente := DataSet.FieldByname('Descrizione').AsString;
          MainForm.ClienteAnnunciFrame.QAnnunciCli.Close;
          MainForm.ClienteAnnunciFrame.QListinoCliente.Close;
          MainForm.CLienteAnnunciFrame.QAnnunciCli.SQL.Text := 'select Ann_AnnEdizData.ID, Ann_AnnEdizData.IDAnnuncio,' +
               'Ann_AnnEdizData.Data, Ann_AnnEdizData.NumModuli,' +
               'NomeEdizione, Ann_Testate.Denominazione Testata,Users.Nominativo from Ann_AnnEdizData, Ann_Edizioni, Ann_Testate, Ann_Annunci,Users ' +
               'where Ann_AnnEdizData.IDEdizione=Ann_Edizioni.ID and Ann_Edizioni.IDTestata=Ann_Testate.ID' +
               ' and Ann_AnnEdizData.IDAnnuncio=Ann_Annunci.ID   and Ann_Annunci.IDUtente=Users.ID' +
               ' and Ann_Annunci.IDCliente=:xIDCliente: order by Ann_AnnEdizData.Data desc';

          {MainForm.CLienteAnnunciFrame.QListinoCliente.SQL.Text := 'select NomeEdizione, Ann_Testate.Denominazione Testata,Ann_EdizListino.*,Ann_ListinoClienti.* ' +
               'from Ann_EdizListino,Ann_Edizioni,Ann_Testate,Ann_ListinoClienti where Ann_EdizListino.IDEdizione=Ann_Edizioni.ID' +
               ' and Ann_Edizioni.IDTestata=Ann_Testate.ID and Ann_EdizListino.ID *= Ann_ListinoClienti.IDEdizListino' +
               ' and Ann_ListinoClienti.IDCliente=:xIDCliente: order by QtaDa,QtaA,dallaData desc';}
          //modifica query da Thomas
          MainForm.CLienteAnnunciFrame.QListinoCliente.SQL.Text := 'select NomeEdizione, Ann_Testate.Denominazione Testata,Ann_EdizListino.*,Ann_ListinoClienti.* ' +
               ' from Ann_EdizListino join Ann_Edizioni on Ann_EdizListino.IDEdizione=Ann_Edizioni.ID' +
               ' join Ann_Testate on Ann_Edizioni.IDTestata=Ann_Testate.ID' +
               ' left join Ann_ListinoClienti on Ann_EdizListino.ID = Ann_ListinoClienti.IDEdizListino ' +
               ' and Ann_ListinoClienti.IDCliente=:xIDCliente: order by QtaDa,QtaA,dallaData desc';
          MainForm.ClienteAnnunciFrame.QAnnunciCli.ParambyName['xIDCliente'] := DataSet.FieldByname('ID').AsInteger;
          MainForm.ClienteAnnunciFrame.QListinoCliente.ParambyName['xIDCliente'] := DataSet.FieldByname('ID').AsInteger;
          MainForm.ClienteAnnunciFrame.QAnnunciCli.Open;
          MainForm.ClienteAnnunciFrame.QListinoCliente.Open;
     end;

     /// campi pers
     if MainForm.PCClienti.ActivePage = MainForm.TSCampiPers then begin
          //if TEBCClienti.isempty then exit;
          if MainForm.CampiPersFrame2.TCampiPers.Active then MainForm.CampiPersFrame2.TCampiPers.close;
          MainForm.CampiPersFrame2.CaricaValori;
          MainForm.CampiPersFrame2.ImpostaCampi;
     end;

     /// Sintesi
     if MainForm.PCClienti.ActivePage = MainForm.TSCliSintesi then begin
          //if TEBCClienti.isempty then exit;
          MainForm.RelazioniCandFrame2.xTipo := 'C';
          MainForm.RelazioniCandFrame2.CreaAlbero;
     end;

     /// Offerte
     if MainForm.PCClienti.ActivePage = MainForm.TSContattiOfferte then begin
          TCliOfferte.Close;
          TCliOfferte.Open;
     end;

     // contatti avuti con il cliente
     if MainForm.PCClienti.ActivePage = MainForm.TSCliContatti then begin
          TCliContattiFatti.Open;
          QRelCliUtenti.Open;
          //QCliModulistica.Open;
          TCliModulistica.Close;
          TCliModulistica.parameters.ParamByName('ID').value := TEBCClientiID.Value;
          TCliModulistica.Open;
     end else begin
          TCliContattiFatti.Close;
          QRelCliUtenti.Close;
          //QCliModulistica.Close;
          TCliModulistica.Close;
     end;

end;

procedure TData2.QRicClienti_OLDCalcFields(DataSet: TDataSet);
begin
     DataSet.FieldByName('CompensoStimato').Value := DataSet.FieldBYName('StipendioLordo').Value * DataSet.FieldByname('NumRicercati').Value;
end;

procedure TData2.TEBCClienti_OLDAfterClose(DataSet: TDataSet);
begin
     TContattiClienti.Close;
end;

procedure TData2.DsQJobDescStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsQJobDesc.State in [dsEdit, dsInsert];
     MainForm.BJobDescOK.Enabled := b;
     MainForm.BJobDescCan.Enabled := b;
end;

procedure TData2.DsAttivitaStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsAttivita.State in [dsEdit, dsInsert];
     MainForm.TbBAttivitaNew.Enabled := not b;
     MainForm.TbBAttivitaDel.Enabled := not b;
     MainForm.TbBAttivitaOK.Enabled := b;
     MainForm.TbBAttivitaCan.Enabled := b;
end;

procedure TData2.QAttivita_OLDAfterPost(DataSet: TDataSet);
begin
     with QAttivita do begin
          Data.DB.BeginTrans;
          try
               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               raise;
          end;
     end;
end;

procedure TData2.QRicClienti_OLDAfterScroll(DataSet: TDataSet);
begin
     if MainForm.PCClienti.ActivePage = MainForm.TSClientiRic then begin
          if (not MainForm.CheckProfile('126', False)) and (DataSet.FieldBYName('IDUtente').Value <> MainForm.xIDUtenteAttuale) then
               MainForm.PanCliCompensi.visible := False
          else MainForm.PanCliCompensi.visible := True;
          QTotCompenso.Close;
          QTotComp2.Close;

          QTotCompenso.Parameters[0].Value := QRicClienti.FieldByName('ID').Value;
          QTotComp2.Parameters[0].Value := QRicClienti.FieldByName('ID').Value;

          QTotCompenso.Open;
          QTotComp2.Open;
     end;
end;

procedure TData2.TEBCClientiOLDAfterOpen(DataSet: TDataSet);
begin
     MainForm.LTotAz.Caption := IntToStr(DataSet.RecordCount);
     if Dataset.RecordCount > 0 then begin
          TContattiClienti.open;
          QRegoleClienti.Open;
          MainForm.QFiliali.Open;
          QClienteLogo.Open;
     end;
end;

procedure TData2.QUsersLKBeforeOpen(DataSet: TDataSet);
begin
     QUsersLK.ParamByName['xoggi'] := DateToStr(Date());
end;

procedure TData2.DsQRelCliUtentiStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsQRelCliUtenti.State in [dsEdit, dsInsert];
     MainForm.BRelCliUtenteNew.Enabled := not b;
     MainForm.BRelCliUtenteDel.Enabled := not b;
     MainForm.BRelCliUtenteOK.Enabled := b;
     MainForm.BRelCliUtenteCan.Enabled := b;
end;

procedure TData2.QRelCliUtentiAfterInsert(DataSet: TDataSet);
begin
     QRelCliUtentiIDCliente.Value := TEBCClienti.FieldByName('ID').asInteger;
     xIDOldCliente := 0;
end;

procedure TData2.QRelCliUtentiBeforePost(DataSet: TDataSet);
begin
     if (xIDOldCliente <> MainForm.xIDUtenteAttuale) and (MainForm.xIDUtenteAttuale <> 9) and (xIDOldCliente > 0) then begin
          MessageDlg('Se non si � l''utente "Administrator", si pu� modificare soltanto la relazione con se'' stessi', mtError, [mbOK], 0);
          Abort;
     end;
end;

procedure TData2.QRelCliUtentiBeforeEdit(DataSet: TDataSet);
begin
     xIDOldCliente := QRelCliUtentiIDUtente.Value;
end;

procedure TData2.TEBCClientiOLDBeforeEdit(DataSet: TDataSet);
begin
     xIDAttivita := TEBCClientiIDAttivita.Value;
end;

procedure TData2.DsMansioniDataChange(Sender: TObject; Field: TField);
begin
     qjobdesc.close;
     QJobDesc.SQL.Text := 'Select Mansioni from mansioni where id = :xID:';
     QJobDesc.ParamByName['xID'] := TMansioni.fieldbyname('id').asinteger;
     QJobDesc.Open;
end;

procedure TData2.QCliModulisticaBeforeOpen(DataSet: TDataSet);
begin
     // impostazione parametro
     QCliModulistica.parameters.ParamByName('ID').value := TEBCClientiID.Value;
     // inserimento di tutti i moduli (laddove mancanti)
     Q.Close;
     Q.SQL.Text := 'insert into CliModulistica (IDCliente,IDModulo) ' +
          'select ' + TEBCClientiID.asString + ',ID from CliTabModulistica ' +
          'where ID not in (select distinct IDModulo from ' +
          'CliModulistica where IDCLiente=' + TEBCClientiID.asString + ')';
     Q.ExecSQL;
end;

procedure TData2.DsQCliModulisticaStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsQCliModulistica.State in [dsEdit, dsInsert];
     MainForm.BCliModOK.Enabled := b;
     MainForm.BCliModCan.Enabled := b;
end;

procedure TData2.DsQSinonimiStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsQSinonimi.State in [dsEdit, dsInsert];
     MainForm.BSinonimoNew.Enabled := not b;
     MainForm.BSinonimoDel.Enabled := not b;
     MainForm.BSinonimoOK.Enabled := b;
     MainForm.BSinonimoCan.Enabled := b;
end;

procedure TData2.TMansioniAfterOpen(DataSet: TDataSet);
begin
     QSinonimi.Open;
end;

procedure TData2.TMansioniAfterClose(DataSet: TDataSet);
begin
     QSinonimi.Close;
end;

procedure TData2.QSinonimiAfterPost(DataSet: TDataSet);
var xID: integer;
begin
     xID := QSinonimi.FieldByName('ID').AsInteger;
     QSinonimi.Close;
     QSinonimi.Open;
     QSinonimi.Locate('ID', xID, []);
end;

procedure TData2.QSinonimiAfterInsert(DataSet: TDataSet);
begin
     QSinonimiIDMansione.Value := TMansioni.FieldByName('ID').asInteger;
end;

procedure TData2.QRicClientiOLDBeforeOpen(DataSet: TDataSet);
begin
     Data.Q1.Close;
     Data.Q1.SQL.Text := 'select * from usersobj where codoggetto = ''26'' and idsoftware = 1 and idutente = ' + inttostr(Mainform.xIDUtenteAttuale);
     Data.Q1.Open;
     if data.q1.RecordCount > 0 then begin
          //Data2.QRicClienti.close;
          Data2.QRicClienti.SQL.Add(' and EBC_ricerche.IDUtente = ' + inttostr(MainForm.xIDUtenteAttuale));
          //Data2.QRicClienti.Open;
     end;
     Data.Q1.close;
end;

procedure TData2.QAnagRicercheBeforeOpen(DataSet: TDataSet);
begin
     Data.Q1.Close;
     Data.Q1.SQL.Text := 'select * from usersobj where codoggetto = ''26'' and idsoftware = 1 and idutente = ' + inttostr(Mainform.xIDUtenteAttuale);
     Data.Q1.Open;
     if data.q1.RecordCount > 0 then
          QAnagRicerche.SQL.Add(' and EBC_Ricerche.IDUtente= ' + inttostr(Mainform.xIDUtenteAttuale));
     Data.q1.Close;
end;

procedure TData2.TCliOfferteBeforeOpen(DataSet: TDataSet);
begin
     TCliOfferte.Parameters.ParamByName('ID').value := TEBCClientiID.Value;
end;

procedure TData2.AllineaIndirizzoPrincipale(xIDCliente: integer);
begin
     // allineamento indirizzo principale
     Q2.Close;
     Q2.SQL.clear;
     Q2.SQL.text := 'select TipoStrada,Indirizzo,Comune,Provincia,NumCivico,CAP ' +
          ' from EBC_Clienti where ID=' + IntToStr(xIDCliente);
     Q2.open;

     Q1.Close;
     Q1.SQL.clear;
     Q1.SQL.text := 'select ID from EBC_ClienteIndirizzi where Principale=1 and IDCliente=' + IntToStr(xIDCliente);
     Q1.open;
     Q.Close;
     Q.SQL.Clear;
     if Q1.IsEmpty then begin
          // nessun indirizzo principale > da inserire
          Q.SQL.text := 'insert into ebc_clienteindirizzi (IDCliente,Descrizione,Principale,TipoStrada,Indirizzo,Comune,Provincia,NumCivico,CAP) ' +
               ' values (:xIDCliente,:xDescrizione,1,:xTipoStrada,:xIndirizzo,:xComune,:xProvincia,:xNumCivico,:xCAP)';
          Q.Parameters[0].value := xIDCliente;
          Q.Parameters[1].value := 'Sede principale';
          Q.Parameters[2].value := Q2.FieldByName('TipoStrada').AsString;
          Q.Parameters[3].value := Q2.FieldByName('Indirizzo').AsString;
          Q.Parameters[4].value := Q2.FieldByName('Comune').AsString;
          Q.Parameters[5].value := Q2.FieldByName('Provincia').AsString;
          Q.Parameters[6].value := Q2.FieldByName('NumCivico').AsString;
          Q.Parameters[7].value := Q2.FieldByName('Cap').AsString;
          Q.ExecSQL;
     end else begin
          // indirizzo principale esistente > da modificare
          Q.SQL.text := 'update ebc_clienteindirizzi set TipoStrada=:x0,Indirizzo=:x1,Comune=:x2,Provincia=:x3,NumCivico=:x4,CAP=:x5 ' +
               ' where ID = ' + Q1.FieldbyName('ID').AsString;
          Q.Parameters[0].value := Q2.FieldByName('TipoStrada').AsString;
          Q.Parameters[1].value := Q2.FieldByName('Indirizzo').AsString;
          Q.Parameters[2].value := Q2.FieldByName('Comune').AsString;
          Q.Parameters[3].value := Q2.FieldByName('Provincia').AsString;
          Q.Parameters[4].value := Q2.FieldByName('NumCivico').AsString;
          Q.Parameters[5].value := Q2.FieldByName('Cap').AsString;
          Q.ExecSQL;
     end;
end;

procedure TData2.TCliContattiFattiBeforeOpen(DataSet: TDataSet);
begin
     QUsers2LK.close;
     QUsers2LK.Open;
end;

procedure TData2.QCliRefCliBeforeOpen(DataSet: TDataSet);
begin
     QCliRefCli.Parameters[0].Value := TEbcClientiID.Value;
end;

procedure TData2.DSTCliModulisticaStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsTCliModulistica.State in [dsEdit, dsInsert];
     MainForm.BCliModOK.Enabled := b;
     MainForm.BCliModCan.Enabled := b;
end;

end.

