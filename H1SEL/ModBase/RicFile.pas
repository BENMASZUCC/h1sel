//[TONIDEBUGOK]
unit RicFile;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Mask, ToolEdit, StdCtrls, Buttons, TB97, ExtCtrls;

type
     TRicFileForm = class(TForm)
          Label2: TLabel;
          Label3: TLabel;
          EDesc: TEdit;
          FEFile: TFilenameEdit;
          Panel1: TPanel;
          BitBtn1: TToolbarButton97;
          BitBtn2: TToolbarButton97;
          procedure FormShow(Sender: TObject);
          procedure BitBtn1Click(Sender: TObject);
          procedure BitBtn2Click(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     RicFileForm: TRicFileForm;

implementation

uses Main;

{$R *.DFM}

procedure TRicFileForm.FormShow(Sender: TObject);
begin
     //Grafica
     RicFileForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
end;

procedure TRicFileForm.BitBtn1Click(Sender: TObject);
begin
     RicFileForm.ModalResult := mrOK;
end;

procedure TRicFileForm.BitBtn2Click(Sender: TObject);
begin
     RicFileForm.ModalResult := mrCancel;
end;

end.

