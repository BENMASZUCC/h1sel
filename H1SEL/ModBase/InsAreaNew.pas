unit InsAreaNew;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, ExtCtrls, Grids, DBGrids, Db, ADODB, dxTL, dxDBCtrl,
     dxDBGrid, dxCntner, TB97;

type
     TInsAreaNewForm = class(TForm)
          Panel73: TPanel;
          Panel3: TPanel;
          ECercaArea: TEdit;
          TAree: TADOQuery;
          TAreeID: TAutoIncField;
          TAreeDescrizione: TStringField;
          DsAree: TDataSource;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1Column1: TdxDBGridColumn;
          Panel6: TPanel;
          BOK: TToolbarButton97;
          BNO: TToolbarButton97;
          Label1: TLabel;
          procedure ECercaAreaKeyPress(Sender: TObject; var Key: Char);
          procedure BOKClick(Sender: TObject);
          procedure BNOClick(Sender: TObject);
          procedure FormShow(Sender: TObject);
     private
    { Private declarations }
     public
    { Public declarations }
     end;

var
     InsAreaNewForm: TInsAreaNewForm;

implementation

uses ModuloDati, InsRuolo, Main;

{$R *.DFM}

procedure TInsAreaNewForm.ECercaAreaKeyPress(Sender: TObject;
     var Key: Char);
var QTemp2: tadoquery;
begin
     if key = chr(13) then begin
          QTemp2 := tadoquery.create(nil);
          QTemp2.Connection := Data.DB;
          qtemp2.sql.Text := 'select id,descrizione from Aree where descrizione like ''%' + StringReplace(ECercaArea.Text, '''', '''''', [rfReplaceAll]) + '%''' +
               ' order by descrizione';
          QTemp2.Open;
          if QTemp2.RecordCount <= 0 then
               MessageDlg('Nessun Area trovato', mtInformation, [mbOk], 0)
          else
               taree.Locate('ID', qtemp2.fieldbyname('ID').Value, []);
          QTemp2.Close;
          QTemp2.free;
     end;
end;

procedure TInsAreaNewForm.BOKClick(Sender: TObject);
begin
     InsAreaNewForm.modalResult := mrOK;
end;

procedure TInsAreaNewForm.BNOClick(Sender: TObject);
begin
     InsAreaNewForm.modalResult := mrcancel;
end;

procedure TInsAreaNewForm.FormShow(Sender: TObject);
begin
     //grafica
     Panel3.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel3.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel3.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel6.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel6.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel6.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     TAree.Close;
     TAree.Open;
end;

end.

