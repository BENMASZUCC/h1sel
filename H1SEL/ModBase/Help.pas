unit Help;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ExtCtrls, Db, StdCtrls, DBCtrls, ADODB, dxCntner, dxEditor, dxExEdtr,
     dxEdLib, dxDBELib, Buttons, TB97;

type
     THelpForm = class(TForm)
          qHelp: TADOQuery;
          qHelpID: TAutoIncField;
          qHelpIDSoftware: TIntegerField;
          qHelpLinkHelp: TStringField;
          qHelpNoteCliente: TMemoField;
          HelpMemo: TDBMemo;
          dsHelp: TDataSource;
          Panel1: TPanel;
          dxDBHyperLinkEdit1: TdxDBHyperLinkEdit;
          lHelp: TLabel;
          lNoteCli: TLabel;
          qHelpCodSezione: TStringField;
          Llinkcliente: TLabel;
          dxDBHyperLinkEdit2: TdxDBHyperLinkEdit;
          qHelpLinkCliente: TStringField;
          Panel2: TPanel;
    BitBtn1: TToolbarButton97;
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
          procedure FormShow(Sender: TObject);
          procedure BitBtn1Click(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     HelpForm: THelpForm;

implementation

uses ModuloDati, Main;

{$R *.DFM}

{ THelpForm }



procedure THelpForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     if dsHelp.State = dsEdit then qHelp.Post;

end;

procedure THelpForm.FormShow(Sender: TObject);
begin
     //Grafica
     HelpForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     dxDBHyperLinkEdit1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;

     HelpMemo.SetFocus;
     Helpmemo.SelStart := Length(HelpMemo.Lines.Text);
end;

procedure THelpForm.BitBtn1Click(Sender: TObject);
begin
     HelpForm.ModalResult := mrOK;
end;

end.

