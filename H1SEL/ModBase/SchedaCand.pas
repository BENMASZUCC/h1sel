unit SchedaCand;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, DBCtrls, ExtCtrls, Grids, DBGrids, DtEdit97, DtEdDB97,
     ComCtrls, Buttons, Mask, TB97, DB, DBTables, dxDBTLCl, dxGrClms, dxTL,
     dxDBCtrl, dxDBGrid, dxCntner, FrameDBRichEdit2, ADODB, U_ADOLinkCl, ShellAPI,
     dxExEdtr, dxEdLib, dxDBELib, jpeg, RelazioniCand, dxGrClEx, JvDBImage,
     ModelComponentsTVD5, ModelCompositionTVD5b, IdGlobal, Menus, FrmCampiPers,
     JclFileUtils;

type
     TSchedaCandForm = class(TForm)
          Panel1: TPanel;
          Panel2: TPanel;
          Label1: TLabel;
          Label2: TLabel;
          Label11: TLabel;
          DBECognome: TDBEdit;
          DBEdit2: TDBEdit;
          DBEdit14: TDBEdit;
          PageControl2: TPageControl;
          TSAnagDatiAnag: TTabSheet;
          PanAnag: TPanel;
          Label38: TLabel;
          Label100: TLabel;
          Label101: TLabel;
          Label35: TLabel;
          DBLookupComboBox1: TDBLookupComboBox;
          DBEdit65: TDBEdit;
          DBEdit16: TDBEdit;
          DbDateEdit971: TDbDateEdit97;
          TSAnagRicerche: TTabSheet;
          DBGrid7: TDBGrid;
          Panel12: TPanel;
          TSAnagComp: TTabSheet;
          DBGrid14: TDBGrid;
          Panel73: TPanel;
          TsAnagMans: TTabSheet;
          Splitter4: TSplitter;
          Panel75: TPanel;
          DBGrid41: TDBGrid;
          Panel17: TPanel;
          Panel74: TPanel;
          DBGrid19: TDBGrid;
          TSAnagStorico: TTabSheet;
          Panel7: TPanel;
          Panel19: TPanel;
          Label15: TLabel;
          DBEdit42: TDBEdit;
          Panel126: TPanel;
          Panel127: TPanel;
          GroupBox1: TGroupBox;
          Label7: TLabel;
          Label8: TLabel;
          Label9: TLabel;
          DBEdit7: TDBEdit;
          DBEdit8: TDBEdit;
          DBEdit9: TDBEdit;
          GroupBox2: TGroupBox;
          Label4: TLabel;
          Label19: TLabel;
          Label22: TLabel;
          DBEdit4: TDBEdit;
          DBEdit10: TDBEdit;
          DBEdit11: TDBEdit;
          Panel3: TPanel;
          BModifica: TToolbarButton97;
          Panel4: TPanel;
          Label88: TLabel;
          Label89: TLabel;
          Label90: TLabel;
          Label10: TLabel;
          DBEdit21: TDBEdit;
          DBEdit22: TDBEdit;
          DBEdit5: TDBEdit;
          DBEdit20: TDBEdit;
          Panel5: TPanel;
          Label5: TLabel;
          Label12: TLabel;
          Label13: TLabel;
          Label85: TLabel;
          DBEdit1: TDBEdit;
          DBEdit12: TDBEdit;
          DBEdit18: TDBEdit;
          DBEdit70: TDBEdit;
          PanCF: TPanel;
          Label49: TLabel;
          LabPartitaIVA: TLabel;
          DBEdit17: TDBEdit;
          DBEPartitaIVA: TDBEdit;
          PanOldCV: TPanel;
          Label86: TLabel;
          DBEdit71: TDBEdit;
          TSAnagNoteInt: TTabSheet;
          DsQAnagNote: TDataSource;
          DBEdit6: TDBEdit;
          DBComboBox7: TDBComboBox;
          DBEdit54: TDBEdit;
          Label6: TLabel;
          Label93: TLabel;
          Label99: TLabel;
          DBEdit3: TDBEdit;
          DBComboBox8: TDBComboBox;
          DBEdit79: TDBEdit;
          Label3: TLabel;
          Label105: TLabel;
          TbBAnagMansNew: TToolbarButton97;
          TbBAnagMansDel: TToolbarButton97;
          TbBAnagMansMod: TToolbarButton97;
          QAnagNote: TADOLinkedQuery;
          TSAnagFile: TTabSheet;
          Panel123: TPanel;
          BFileCandOpen: TToolbarButton97;
          dxDBGrid14: TdxDBGrid;
          dxDBGrid14DataCreazione: TdxDBGridDateColumn;
          dxDBGrid14Tipo: TdxDBGridMaskColumn;
          dxDBGrid14Descrizione: TdxDBGridMaskColumn;
          dxDBGrid14NomeFile: TdxDBGridMaskColumn;
          ToolbarButton9745: TToolbarButton97;
          DbAnagFoto_old: TdxDBGraphicEdit;
          Label42: TLabel;
          Panel6: TPanel;
          TbBCompDipNew: TToolbarButton97;
          ToolbarButton9716: TToolbarButton97;
          TbBCompDipDel: TToolbarButton97;
          ToolbarButton979: TToolbarButton97;
          ToolbarButton9710: TToolbarButton97;
          Panel8: TPanel;
          TbBCarattDipNew: TToolbarButton97;
          TbBCarattDipMod: TToolbarButton97;
          TbBCarattDipDel: TToolbarButton97;
          Panel9: TPanel;
          ToolbarButton977: TToolbarButton97;
          BVediStoriaContatti: TToolbarButton97;
          BAnagFileDel: TToolbarButton97;
          BAnagFileMod: TToolbarButton97;
          BAnagFileNew: TToolbarButton97;
          TSRelazCand: TTabSheet;
          RelazioniCandFrame1: TRelazioniCandFrame;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1ID: TdxDBGridMaskColumn;
          dxDBGrid1IDAnagrafica: TdxDBGridMaskColumn;
          dxDBGrid1IDEvento: TdxDBGridMaskColumn;
          dxDBGrid1DataEvento: TdxDBGridDateColumn;
          dxDBGrid1Annotazioni: TdxDBGridMaskColumn;
          dxDBGrid1IDRicerca: TdxDBGridMaskColumn;
          dxDBGrid1IDUtente: TdxDBGridMaskColumn;
          dxDBGrid1IDCliente: TdxDBGridMaskColumn;
          dxDBGrid1Nominativo: TdxDBGridMaskColumn;
          dxDBGrid1DescEvento: TdxDBGridMaskColumn;
          dxDBGrid1ProgRicerca: TdxDBGridMaskColumn;
          dxDBGrid1DataInizioRic: TdxDBGridDateColumn;
          dxDBGrid1CLiente: TdxDBGridMaskColumn;
          dxDBGrid14Column5: TdxDBGridCheckColumn;
          dxDBGrid14Column6: TdxDBGridCheckColumn;
          dxDBGrid1Column14: TdxDBGridColumn;
          ToolbarButton971: TToolbarButton97;
          dxDBGrid14Column7: TdxDBGridCheckColumn;
          DbAnagFoto: TJvDBImage;
          BFileDaModello: TToolbarButton97;
          pmFoto: TPopupMenu;
          Inserisci1: TMenuItem;
          Rimuovi1: TMenuItem;
          Label14: TLabel;
          Panel10: TPanel;
          pNoteInterne: TPanel;
          FrameDBRich1: TFrameDBRich;
          pNoteCommesse: TPanel;
          Panel153: TPanel;
          dxDBGrid5: TdxDBGrid;
          dxDBGrid5ID: TdxDBGridMaskColumn;
          dxDBGrid5Progressivo: TdxDBGridMaskColumn;
          dxDBGrid5Note: TdxDBGridBlobColumn;
          pNoteCliente: TPanel;
          DBMemo8: TDBMemo;
          Panel181: TPanel;
          pNoteCandidato: TPanel;
          DBMemo10: TDBMemo;
          Panel11: TPanel;
          Splitter1: TSplitter;
          Splitter2: TSplitter;
          Splitter3: TSplitter;
          Panel13: TPanel;
          TSCampiPers: TTabSheet;
          CampiPersFrame1: TCampiPersFrame;
          AdattaStretch: TMenuItem;
          TSSchedCandCommesse: TTabSheet;
          Panel14: TPanel;
          BInsRicerca: TToolbarButton97;
          ToolbarButton975: TToolbarButton97;
          ToolbarButton976: TToolbarButton97;
          Panel48: TPanel;
          CBFiltroAnagRic: TCheckBox;
          dxDBGrid22: TdxDBGrid;
          dxDBGrid22Progressivo: TdxDBGridMaskColumn;
          dxDBGrid22Cliente: TdxDBGridMaskColumn;
          dxDBGrid22Ruolo: TdxDBGridMaskColumn;
          dxDBGrid22Tipo: TdxDBGridMaskColumn;
          dxDBGrid22Escluso: TdxDBGridCheckColumn;
          dxDBGrid22StatoRic: TdxDBGridMaskColumn;
          dxDBGrid22DataInizio: TdxDBGridDateColumn;
          dxDBGrid22DataFine: TdxDBGridDateColumn;
          dxDBGrid22ID: TdxDBGridMaskColumn;
          dxDBGrid22IDRicerca: TdxDBGridMaskColumn;
          dxDBGrid22Utente: TdxDBGridMaskColumn;
          dxDBGrid22IDAnagrafica: TdxDBGridMaskColumn;
          DBEdit13: TDBEdit;
          Label17: TLabel;
          Panel15: TPanel;
          Label16: TLabel;
          Label18: TLabel;
          Label20: TLabel;
          Label21: TLabel;
          DBEdit15: TDBEdit;
          DBEdit19: TDBEdit;
          DBEdit23: TDBEdit;
          DBEdit24: TDBEdit;
          GroupBox26: TGroupBox;
          Label23: TLabel;
          Label41: TLabel;
          Label48: TLabel;
          DBEdit44: TDBEdit;
          DBEdit50: TDBEdit;
          DBEStatoNascita: TDBEdit;
          DBEdit25: TDBEdit;
          Label24: TLabel;
          Label25: TLabel;
          DBEdit26: TDBEdit;
          BOK: TToolbarButton97;
          BAnnulla: TToolbarButton97;
          DBComboBox1: TDBComboBox;
          Label26: TLabel;
          ToolbarButton9747: TToolbarButton97;
          Panel16: TPanel;
          ToolbarButton973: TToolbarButton97;
          BValutaz: TToolbarButton97;
          ToolbarButton972: TToolbarButton97;
          ToolbarButton974: TToolbarButton97;
          Panel18: TPanel;
          Label27: TLabel;
          Label28: TLabel;
          Label29: TLabel;
          Label30: TLabel;
          Panel20: TPanel;
          DBEdit27: TDBEdit;
          DBEdit28: TDBEdit;
          DBEdit29: TDBEdit;
          DBEdit30: TDBEdit;
          SpeedButton57: TToolbarButton97;
          SpeedButton60: TToolbarButton97;
          SpeedButton59: TToolbarButton97;
          SpeedButton2: TToolbarButton97;
          SpeedButton3: TToolbarButton97;
          SpeedButton4: TToolbarButton97;
          Label110: TLabel;
          ToolbarButton978: TToolbarButton97;
          Label31: TLabel;
          DBEdit82: TDBEdit;
          DBEdit93: TDBEdit;
          ToolbarButton9711: TToolbarButton97;
          ClassCandidato: TGroupBox;
          CBClassPotenziale: TComboBox;
    dxDBGrid14FlagEsportaCVGeneraPres: TdxDBGridCheckColumn;
          procedure PageControl2Change(Sender: TObject);
          procedure SpeedButton2Click(Sender: TObject);
          procedure SpeedButton4Click(Sender: TObject);
          procedure SpeedButton3Click(Sender: TObject);
          procedure BModificaClick(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
          procedure DBEdit15Exit(Sender: TObject);
          procedure Sc(Sender: TObject);
          procedure DsQAnagNoteStateChange(Sender: TObject);
          procedure TbBAnagMansNewClick(Sender: TObject);
          procedure TbBAnagMansModClick(Sender: TObject);
          procedure TbBAnagMansDelClick(Sender: TObject);
          procedure ToolbarButton9745Click(Sender: TObject);
          procedure TbBCompDipNewClick(Sender: TObject);
          procedure ToolbarButton9716Click(Sender: TObject);
          procedure TbBCompDipDelClick(Sender: TObject);
          procedure ToolbarButton979Click(Sender: TObject);
          procedure ToolbarButton9710Click(Sender: TObject);
          procedure TbBCarattDipNewClick(Sender: TObject);
          procedure TbBCarattDipModClick(Sender: TObject);
          procedure TbBCarattDipDelClick(Sender: TObject);
          procedure ToolbarButton977Click(Sender: TObject);
          procedure BVediStoriaContattiClick(Sender: TObject);
          procedure DbAnagFoto_oldGetGraphicClass(Sender: TObject;
               var GraphicClass: TGraphicClass);
          procedure BAnagFileNewClick(Sender: TObject);
          procedure BAnagFileModClick(Sender: TObject);
          procedure BAnagFileDelClick(Sender: TObject);
          procedure BFileCandOpenClick(Sender: TObject);
          procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
          procedure ToolbarButton971Click(Sender: TObject);
          procedure dxDBGrid5DblClick(Sender: TObject);
          procedure DBEdit12Enter(Sender: TObject);
          procedure BFileDaModelloClick(Sender: TObject);
          procedure Inserisci1Click(Sender: TObject);
          procedure Rimuovi1Click(Sender: TObject);
          procedure Panel181DblClick(Sender: TObject);
          procedure Panel10DblClick(Sender: TObject);
          procedure Panel13DblClick(Sender: TObject);
          procedure Panel153DblClick(Sender: TObject);
          procedure AdattaStretchClick(Sender: TObject);
          procedure BInsRicercaClick(Sender: TObject);
          procedure DBEdit1Enter(Sender: TObject);
          procedure DBEdit70Enter(Sender: TObject);
          procedure DBEdit18Enter(Sender: TObject);
          procedure DBEdit15Enter(Sender: TObject);
          procedure DBEdit24Enter(Sender: TObject);
          procedure BOKClick(Sender: TObject);
          procedure BAnnullaClick(Sender: TObject);
          procedure SpeedButton57Click(Sender: TObject);
          procedure SpeedButton60Click(Sender: TObject);
          procedure SpeedButton59Click(Sender: TObject);
          procedure CBFiltroAnagRicClick(Sender: TObject);
          procedure ToolbarButton975Click(Sender: TObject);
          procedure ToolbarButton976Click(Sender: TObject);
          procedure ToolbarButton9747Click(Sender: TObject);
          procedure ToolbarButton973Click(Sender: TObject);
          procedure ToolbarButton972Click(Sender: TObject);
          procedure ToolbarButton974Click(Sender: TObject);
          procedure CampiPersFrame1ToolbarButton971Click(Sender: TObject);
          procedure ToolbarButton978Click(Sender: TObject);
          procedure ToolbarButton9711Click(Sender: TObject);
          procedure CBClassPotenzialeChange(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
          xChiamante: string;
          procedure Sistemanote;
     end;

var
     SchedaCandForm: TSchedaCandForm;

implementation

uses ModuloDati2, ModuloDati, Comuni, ValutazColloquio, InsRuolo, CompInserite,
     Main, ModRuolo, MDRicerche, SelRicCand, SelContatti, InsCompetenza, uUtilsVarie,
     LeggiNote, SceltaModelloAnag, ElencoRicPend, SelNazione, CodFiscale,
     ComunitaNazStato, InputPassword, CheckPass, SelPers, OpzioniEliminaz,
     SelData;

{$R *.DFM}

//[/TONI20020921\]DEBUGOK

procedure TSchedaCandForm.PageControl2Change(Sender: TObject);
begin
     if Data.DsAnagrafica.State = dsEdit then begin
          with Data.TAnagrafica do begin
               if not Data.DB.InTransaction then Data.DB.BeginTrans;
               try
                    Post;
                    Data.DB.CommitTrans;
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                    raise;
               end;
          end;
          PanAnag.Enabled := False;
          BModifica.Enabled := True;
          BOk.Enabled := False;
          BAnnulla.Enabled := False;
     end;
     if DsQAnagNote.State = dsEdit then begin
          with QAnagNote do begin
               if not Data.DB.InTransaction then Data.DB.BeginTrans;
               try
                    Post;
                    Data.DB.CommitTrans;
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                    raise;
               end;
          end;
     end;

     // CAMPI PERSONALIZZATI
     if PageControl2.ActivePage = TSCampiPers then begin
          if (not MainForm.CheckProfile('1022', false)) then begin //ALE 03012003
               TSCampiPers.Visible := False;
               Exit;
          end; //ALE 03012003 FINE
          CampiPersFrame1.xTipo := 'Anagrafica';
          CampiPersFrame1.CaricaValori;
          CampiPersFrame1.ImpostaCampi;
     end else begin
          CampiPersFrame1.QCampiPers.Close;
     end;


     // RICERCHE
     if PageControl2.ActivePage = TSAnagRicerche then begin
          Data2.QAnagRicerche.Open;
     end else begin
          Data2.QAnagRicerche.Close;
     end;

     // COMPETENZE
     if PageControl2.ActivePage = TSAnagComp then begin
          Data2.TCompDipendente.Open;
     end else begin
          Data2.TCompDipendente.Close;
     end;

     // RUOLI e CARATTERISTICHE
     if PageControl2.ActivePage = TsAnagMans then begin
          Data.TAnagMansioni.Open;
          Data2.TCarattDip.Open;
     end else begin
          Data.TAnagMansioni.Close;
          Data2.TCarattDip.Close;
     end;

     // STORICO
     if PageControl2.ActivePage = TSAnagStorico then begin
          Data.QStorico.Open;
     end else begin
          Data.QStorico.Close;
     end;

     // NOTE INTERNE
     if PageControl2.ActivePage = TSAnagNoteInt then begin
          if FrameDBRich1.Editor.DataSource = nil then
               FrameDBRich1.Editor.DataSource := Data.DsQAnagNote;
          QAnagNote.Close;
          DataRicerche.QAnagNoteRic.Close;
          QAnagNote.Open;
          DataRicerche.QAnagNoteRic.Open;
     end else begin
          QAnagNote.Close;
          DataRicerche.QAnagNoteRic.Close;
     end;

     // FILE
     if PageControl2.ActivePage = TSAnagFile then begin
          Data.QAnagFile.Open;
          if POS('INTERMEDIA', Uppercase(Data.Global.FieldByname('Nomeazienda').Value)) > 0 then begin
               ToolbarButton9745.DropdownMenu := Mainform.pmGeneraPresIntermedia;
          end;
     end else begin
          Data.QAnagFile.Close;
     end;

     // Commesse
     if PageControl2.ActivePage = TSSchedCandCommesse then
          Data2.QAnagRicerche.Open
     else
          Data2.QAnagRicerche.Close;
     // RELAZIONI CANDIDATO
     //if PageControl2.ActivePage = TSRelazCand then begin
     //end else begin
     //end;

     if Data.TAnagrafica.State = dsEdit then Data.TAnagrafica.Post;
end;

procedure TSchedaCandForm.SpeedButton2Click(Sender: TObject);
begin
     //SpeedButton2Click(nil);
     ComuniForm := TComuniForm.create(self);
     ComuniForm.EProv.text := MainForm.xUltimaProv;
     ComuniForm.ShowModal;
     if not (Data.DsAnagrafica.State in [dsEdit, dsInsert]) then Data.TAnagrafica.Edit;
     if ComuniForm.ModalResult = mrOK then begin
          Data.TAnagrafica.FieldByName('Comune').AsString := TrimRight(ComuniForm.QComuni.FieldByName('Descrizione').AsString);
          Data.TAnagrafica.FieldByName('Cap').AsString := TrimRight(ComuniForm.QComuni.FieldByName('Cap').AsString);
          Data.TAnagrafica.FieldByName('Provincia').AsString := TrimRight(ComuniForm.QComuni.FieldByName('Prov').AsString);
          Data.TAnagrafica.FieldByName('IDComuneRes').AsInteger := ComuniForm.QComuni.FieldByName('ID').Asinteger;
          Data.TAnagrafica.FieldByName('IDZonaRes').AsInteger := ComuniForm.QComuni.FieldByName('IDZona').AsInteger;
     end else begin
          Data.TAnagrafica.FieldByName('Comune').AsString := '';
          Data.TAnagrafica.FieldByName('Cap').AsString := '';
          Data.TAnagrafica.FieldByName('Provincia').AsString := '';
          Data.TAnagrafica.FieldByName('IDComuneRes').AsInteger := 0;
          Data.TAnagrafica.FieldByName('IDZonaRes').AsInteger := 0;
     end;
     BOKClick(nil);
     MainForm.xUltimaProv := ComuniForm.EProv.text;
     ComuniForm.Free;
end;
//[/TONI20020921\]DEBUGOKFINE

//[/TONI20020921\]DEBUGOK

procedure TSchedaCandForm.SpeedButton4Click(Sender: TObject);
begin
     //MainForm.SpeedButton4Click(nil);
     ComuniForm := TComuniForm.create(self);
     ComuniForm.EProv.text := MainForm.xUltimaProv;
     ComuniForm.ShowModal;
     if not (Data.DsAnagrafica.State in [dsEdit, dsInsert]) then Data.TAnagrafica.Edit;
     if ComuniForm.ModalResult = mrOK then begin
          Data.TAnagrafica.FieldByName('DomicilioComune').asString := TrimRight(ComuniForm.QComuni.FieldByName('Descrizione').AsString);
          Data.TAnagrafica.FieldByName('DomicilioCap').AsString := TrimRight(ComuniForm.QComuni.FieldByName('Cap').AsString);
          Data.TAnagrafica.FieldByName('DomicilioProvincia').AsString := ComuniForm.QComuni.FieldByName('Prov').AsString;
          Data.TAnagrafica.FieldByName('IDComuneDom').AsInteger := ComuniForm.QComuni.FieldByName('ID').AsInteger;
          Data.TAnagrafica.FieldByName('IDZonaDom').AsInteger := ComuniForm.QComuni.FieldByName('IDZona').AsInteger;
     end else begin
          Data.TAnagrafica.FieldByName('DomicilioComune').AsString := '';
          Data.TAnagrafica.FieldByName('DomicilioCap').AsString := '';
          Data.TAnagrafica.FieldByName('DomicilioProvincia').AsString := '';
          Data.TAnagrafica.FieldByName('IDComuneDom').AsInteger := 0;
          Data.TAnagrafica.FieldByName('IDZonaDom').AsInteger := 0;
     end;
     BOKClick(nil);
     MainForm.xUltimaProv := ComuniForm.EProv.text;
     ComuniForm.Free;
end;
//[/TONI20020921\]DEBUGOKFINE

//[/TONI20020921\]DEBUGOK

procedure TSchedaCandForm.SpeedButton3Click(Sender: TObject);
begin
     if not (Data.DsAnagrafica.State in [dsEdit, dsInsert]) then Data.TAnagrafica.Edit;
     Data.TAnagrafica.FieldByName('DomicilioIndirizzo').AsString := Data.TAnagrafica.FieldByName('Indirizzo').AsString;
     Data.TAnagrafica.FieldByName('DomicilioCap').AsString := Data.TAnagrafica.FieldByName('Cap').AsString;
     Data.TAnagrafica.FieldByName('DomicilioComune').AsString := Data.TAnagrafica.FieldByName('Comune').AsString;
     Data.TAnagrafica.FieldByName('DomicilioProvincia').AsString := Data.TAnagrafica.FieldByName('Provincia').AsString;
     Data.TAnagrafica.FieldByName('IDComuneDom').AsInteger := Data.TAnagrafica.FieldByName('IDComuneRes').Asinteger;
     Data.TAnagrafica.FieldByName('IDZonaDom').AsInteger := Data.TAnagrafica.FieldByName('IDZonaRes').ASInteger;
     Data.TAnagrafica.FieldByName('DomicilioNumCivico').value := Data.TAnagrafica.FieldByName('NumCivico').value;
     Data.TAnagrafica.FieldByName('DomicilioTipoStrada').value := Data.TAnagrafica.FieldByName('TipoStrada').value;
end;
//[/TONI20020921\]DEBUGOKFINE

//[/TONI20020921\]DEBUGOK

procedure TSchedaCandForm.BModificaClick(Sender: TObject);
begin
     if not MainForm.CheckProfile('002') then Exit;
     if Data.TAnagrafica.EOF then
          ShowMessage('questo archivio � archivio vuoto: nessuna scheda da modificare')
     else begin
          Data.TAnagrafica.Edit;
          Data.qAnagAltreInfo.Edit;
          PanAnag.Enabled := True;
          Panel2.Enabled := true;
          GroupBox26.Enabled := true;
          //dbEdit42.ReadOnly := true;
          BModifica.Enabled := False;
          BOk.Enabled := True;
          BAnnulla.Enabled := True;
     end;
end;
//[/TONI20020921\]DEBUGOKFINE

//[/TONI20020921\]DEBUGOK

procedure TSchedaCandForm.FormShow(Sender: TObject);
begin
     //Grafica
     SchedaCandForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel18.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel18.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel18.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     PanAnag.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     PanAnag.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     PanAnag.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel5.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel5.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel5.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     PanCF.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     PanCF.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     PanCF.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     PanOldCV.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     PanOldCV.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     PanOldCV.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel15.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel15.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel15.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel3.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel3.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel3.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel12.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel12.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel12.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel126.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel126.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel126.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel127.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel127.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel127.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel73.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel73.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel73.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel6.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel6.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel6.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel75.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel75.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel75.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel17.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel17.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel17.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel74.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel74.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel74.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel8.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel8.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel8.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel4.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel4.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel4.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel7.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel7.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel7.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel9.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel9.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel9.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel19.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel19.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel19.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel10.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel10.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel10.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     pNoteInterne.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     pNoteInterne.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     pNoteInterne.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel13.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel13.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel13.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     pNoteCommesse.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     pNoteCommesse.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     pNoteCommesse.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel153.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel153.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel153.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     pNoteCliente.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     pNoteCliente.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     pNoteCliente.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel181.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel181.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel181.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     pNoteCandidato.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     pNoteCandidato.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     pNoteCandidato.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel11.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel11.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel11.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel123.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel123.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel123.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel14.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel14.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel14.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel48.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel48.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel48.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     DBEdit42.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel16.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel16.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel16.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel20.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel20.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel20.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     CampiPersFrame1.PanTop.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     CampiPersFrame1.Pan1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     CampiPersFrame1.Pan2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     CampiPersFrame1.Pan3.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     CampiPersFrame1.Pan4.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     CampiPersFrame1.Pan5.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     CampiPersFrame1.Pan6.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     CampiPersFrame1.Pan7.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     CampiPersFrame1.Pan9.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     CampiPersFrame1.Pan8.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     CampiPersFrame1.Pan10.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     CampiPersFrame1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     RelazioniCandFrame1.Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     RelazioniCandFrame1.dxTreeList.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     RelazioniCandFrame1.dxTreeList.HeaderColor := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     RelazioniCandFrame1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;


     Caption := '[M/00] - ' + Caption;
     Data.xSchedaCand := True;
     QAnagNote.Open;
     DataRicerche.QAnagNoteRic.Open;
     FrameDBRich1.ToolButton1.Visible := False;
     AdattaStretch.Checked := True;
     DbAnagFoto.Stretch := True;
     PanAnag.Enabled := False;
     Panel2.Enabled := False;
     BModifica.Enabled := True;
     BOk.Enabled := False;
     BAnnulla.Enabled := False;
     // IMPOSTAZIONI A SECONDA DEL CLIENTE (da GLOBAL)
     if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 5)) = 'ERGON' then begin
          //PanCF.Visible := False;
          PageControl2.Pages[2].TabVisible := False;
     end;

     if pos('INTERMEDIA', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) > 0 then begin
          Panel181.Visible := False;
          DBMemo8.Visible := False;

          TSSchedCandCommesse.caption := 'Progetti';
          TsAnagMans.Caption := 'Ruoli';
          BInsRicerca.caption := 'Inserisci in un progetto';
          ToolbarButton9747.caption := 'Riprendi il progetto';
          ToolbarButton975.caption := 'Elimina dal progetto';
          Panel20.caption := '  Ulteriori informazioni sul progetto selezionato';
     end;

     if pos('LEDERMANN', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) > 0 then begin
          ToolbarButton9745.DropdownMenu := MainForm.pmGeneraPresLedermann;
     end;

     PageControl2.ActivePage := TSRelazCand;
     RelazioniCandFrame1.xChiamante := 'Scheda';
     RelazioniCandFrame1.xTipo := 'A';
     RelazioniCandFrame1.CreaAlbero;

     Data.QClassificazioni.Close;
     Data.QClassificazioni.Open;
     CBClassPotenziale.items.Clear;
     Data.QClassificazioni.First;
     CBClassPotenziale.items.Add('');
     while not data.QClassificazioni.EOF do begin
          CBClassPotenziale.items.Add(data.QClassificazioni.FieldByName('Descrizione').asString);
          data.QClassificazioni.Next;
     end;
     if Data.QClassificazioni.Locate('ID', Data.TAnagraficaIDClassificazione.AsInteger, []) then
          CBClassPotenziale.ItemIndex := Data.QClassificazioni.RecNo // - 1
     else
          CBClassPotenziale.ItemIndex := 0;
end;

procedure TSchedaCandForm.FormClose(Sender: TObject;
     var Action: TCloseAction);
begin
     Data.xSchedaCand := False;
     if Data.DsAnagrafica.State = dsEdit then begin
          with Data.TAnagrafica do begin
               Data.DB.BeginTrans;
               try
                    Post;
                    //                    ApplyUpdates;
                    Data.DB.CommitTrans;
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                    raise;
               end;
               //               CommitUpdates;
          end;
     end;
     if DsQAnagNote.State = dsEdit then begin
          with QAnagNote do begin
               Data.DB.BeginTrans;
               try
                    Post;
                    //                    ApplyUpdates;
                    Data.DB.CommitTrans;
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                    raise;
               end;
               //               CommitUpdates;
          end;
     end;
end;
//[/TONI20020921\]DEBUGFINE

procedure TSchedaCandForm.DBEdit15Exit(Sender: TObject);
begin
     {  if (DBEdit12.Text <> '') then
            if (pos('-', DBEdit12.Text) = 0) and (pos('/', DBEdit12.Text) = 0) then begin
                 Showmessage('ATTENZIONE: Numero di cellulare non correttamente formattato.' + chr(13) +
                      'Utilizzare la linea (-) o la barra (/) per separare il prefisso dal numero');
                 DBEdit12.SetFocus;
            end;  }
end;

//[/TONI20020921\]DEBUGOK

procedure TSchedaCandForm.Sc(Sender: TObject);
begin
     ValutazColloquioForm := TValutazColloquioForm.create(self);
     ValutazColloquioForm.xchiamante := 'SchedaCand';
     ValutazColloquioForm.xIDAnag := Data.TAnagrafica.FieldByName('ID').AsInteger;
     ValutazColloquioForm.ShowModal;
     ValutazColloquioForm.Free;
end;
//[/TONI20020921\]DEBUGOKFINE

procedure TSchedaCandForm.DsQAnagNoteStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsQAnagNote.State in [dsEdit, dsInsert];
end;

procedure TSchedaCandForm.TbBAnagMansNewClick(Sender: TObject);
var xIDAnag, xIDMans, xTotComp, i: integer;
     xComp: array[1..50] of integer;
begin
     if not MainForm.CheckProfile('030') then Exit;
     InsRuoloForm := TInsRuoloForm.create(self);
     InsRuoloForm.ShowModal;
     if InsRuoloForm.ModalResult = mrOK then begin
          // controllo esistenza ruolo
          Data.QTemp.Close;
          Data.QTemp.SQL.text := 'select count(ID) Tot from AnagMansioni where IDAnagrafica=:xIDAnag: and IDMansione=:xIDMans:';
          Data.QTemp.ParamByName['xIDAnag'] := Data.TAnagrafica.FieldByName('ID').AsInteger;
          Data.QTemp.ParamByName['xIDMans'] := InsRuoloForm.TRuoli.FieldbyName('ID').AsInteger;
          Data.QTemp.Open;
          if Data.QTemp.FieldByName('Tot').asInteger > 0 then begin
               MessageDlg('Ruolo gi� associato al candidato. ' + chr(13) +
                    'IMPOSSIBILE INSERIRE.', mtError, [mbOK], 0);
               exit;
          end;
          Data.QTemp.Close;

          with Data do begin
               DB.BeginTrans;
               try
                    // inserimento competenze (se � stato richiesto)
                    if InsRuoloForm.CBInsComp.Checked then begin
                         CompInseriteForm := TCompInseriteForm.create(self);
                         CompInseriteForm.ERuolo.Text := InsRuoloForm.TRuoli.FieldByName('Ruolo').AsString;
                         Data2.QCompRuolo.Close;
                         Data2.QCompRuolo.ReloadSQL;
                         //                         Data2.QCompRuolo.Prepare;
                         Data2.QCompRuolo.ParamByName['x'] := InsRuoloForm.TRuoli.FieldByName('ID').Asinteger;
                         Data2.QCompRuolo.Open;
                         Data2.QCompRuolo.First;
                         xTotComp := 0;
                         Data2.TAnagCompIDX.Open;
                         CompInseriteForm.SG1.RowCount := 2;
                         CompInseriteForm.SG1.RowHeights[0] := 16;
                         while not Data2.QCompRuolo.EOF do begin
                              //                              if not Data2.TAnagCompIDX.FindKey([xIDAnag,Data2.QCompRuolo.FieldByName ('IDCompetenza').AsInteger]) then begin
                              if not Data2.TAnagCompIDX.FindKeyADO(VarArrayOf([xIDAnag, Data2.QCompRuolo.FieldByName('IDCompetenza').AsInteger])) then begin
                                   CompInseriteForm.SG1.RowHeights[xTotComp + 1] := 16;
                                   CompInseriteForm.SG1.Cells[0, xTotComp + 1] := Data2.QCompRuolo.FieldByName('DescCompetenza').AsString;
                                   CompInseriteForm.SG1.Cells[1, xTotComp + 1] := '0';
                                   if TAnagrafica.FieldByName('IDTipoStato').AsInteger <> 2 then begin
                                        CompInseriteForm.SG1.Cells[2, xTotComp + 1] := '�';
                                        CompInseriteForm.SG1.RowColor[xTotComp + 1] := clLime;
                                   end else begin
                                        CompInseriteForm.SG1.Cells[2, xTotComp + 1] := 'x';
                                        CompInseriteForm.SG1.RowColor[xTotComp + 1] := clRed;
                                   end;
                                   xComp[xTotComp + 1] := Data2.QCompRuolo.FieldByName('IDCompetenza').AsInteger;
                                   Inc(xTotComp);
                                   CompInseriteForm.SG1.RowCount := CompInseriteForm.SG1.RowCount + 1;
                              end;
                              Data2.QCompRuolo.Next;
                         end;
                         CompInseriteForm.SG1.RowCount := CompInseriteForm.SG1.RowCount - 1;
                         if xTotComp > 0 then CompInseriteForm.ShowModal;
                         if CompInseriteForm.ModalResult = mrOK then begin
                              for i := 1 to CompInseriteForm.SG1.RowCount - 1 do begin

                                   //if CompInseriteForm.SG1.RowColor[i] = clLime then begin
                                   {if CompInseriteForm.SG1.Cells[2, i] = '�' then begin
                                        Q1.Close;
                                        Q1.SQL.text := 'insert into CompetenzeAnagrafica (IDAnagrafica,IDCompetenza,Valore) ' +
                                             'values (:xIDAnagrafica:,:xIDCompetenza:,:xValore:)';
                                        Q1.ParamByName['xIDAnagrafica'] := Data.TAnagrafica.FieldByName('ID').AsInteger;
                                        Q1.ParamByName['xIDCompetenza'] := xComp[i];
                                        Q1.ParamByName['xValore'] := StrToInt(CompInseriteForm.SG1.Cells[1, i]);
                                        Q1.ExecSQL;

                                        Q1.SQL.text := 'insert into StoricoCompAnag (IDCompetenza,IDAnagrafica,DallaData,MotivoAumento,valore) ' +
                                             'values (:xIDCompetenza:,:xIDAnagrafica:,:xDallaData:,:xMotivoAumento:,:xvalore:)';
                                        Q1.ParamByName['xIDCompetenza'] := xComp[i];
                                        Q1.ParamByName['xIDAnagrafica'] := Data.TAnagrafica.FieldByName('ID').AsString;
                                        Q1.ParamByName['xDallaData'] := Date;
                                        Q1.ParamByName['xMotivoAumento'] := 'valore iniziale';
                                        Q1.ParamByName['xValore'] := StrToInt(CompInseriteForm.SG1.Cells[1, i]);
                                        Q1.ExecSQL;
                                   end;}


                              end;
                         end;
                    end;
                    // inserimento ruolo
                    Q1.Close;
                    Q1.SQL.text := 'insert into AnagMansioni (IDAnagrafica,IDMansione,IDArea,Attuale,AnniEsperienza,Disponibile) ' +
                         'values (:xIDAnagrafica:,:xIDMansione:,:xIDArea:,:xAttuale:,:xAnniEsperienza:,:xDisponibile:)';
                    Q1.ParamByName['xIDAnagrafica'] := Data.TAnagrafica.FieldByName('ID').AsInteger;
                    Q1.ParamByName['xIDMansione'] := InsRuoloForm.TRuoli.fieldByName('ID').AsInteger;
                    Q1.ParamByName['xIDArea'] := InsRuoloForm.TRuoli.FieldByName('IDArea').AsInteger;
                    Q1.ParamByName['xAttuale'] := 0;
                    Q1.ParamByName['xAnniEsperienza'] := 0;
                    Q1.ParamByName['xDisponibile'] := 1;
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    Data.TAnagMansioni.Close;
                    Data.TAnagMansioni.Open;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               end;

          end;
          InsRuoloForm.Free;
     end;
end;

procedure TSchedaCandForm.TbBAnagMansModClick(Sender: TObject);
begin
     if not Mainform.CheckProfile('032') then Exit;
     if Data.TAnagMansioni.RecordCount = 0 then exit;
     ModRuoloForm := TModRuoloForm.create(self);
     ModRuoloForm.SEAnniEsp.Value := Data.TAnagMansioni.FieldByName('AnniEsperienza').AsInteger;
     ModRuoloForm.CBAttuale.checked := Data.TAnagMansioni.FieldByName('Attuale').AsBoolean;
     ModRuoloForm.CBDisponibile.checked := Data.TAnagMansioni.FieldByName('Disponibile').AsBoolean;
     ModRuoloForm.Caption := 'Ruolo: ' + TrimRight(Data.TAnagMansioni.FieldByName('Mansione').AsString);
     ModRuoloForm.ShowModal;
     if ModRuoloForm.Modalresult = mrOK then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text := 'update AnagMansioni set AnniEsperienza=:xAnniEsperienza:,Attuale=:xAttuale:,Disponibile=:xDisponibile: ' +
                         'where ID=' + Data.TAnagMansioni.FieldByName('ID').AsString;
                    Q1.ParamByName['xAnniEsperienza'] := Integer(Round(ModRuoloForm.SEAnniEsp.AsInteger));
                    Q1.ParamByName['xAttuale'] := ModRuoloForm.CBAttuale.checked;
                    Q1.ParamByName['xDisponibile'] := ModRuoloForm.CBDisponibile.checked;
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    Data.TAnagMansioni.Close;
                    Data.TAnagMansioni.Open;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               end;
          end;
     end;
end;
//[/TONI20020921\]DEBUGFINE

//[/TONI20020921\]DEBUGOK

procedure TSchedaCandForm.TbBAnagMansDelClick(Sender: TObject);
begin
     if not Mainform.CheckProfile('031') then Exit;
     if Data.TAnagMansioni.RecordCount > 0 then
          if MessageDlg('Sei sicuro di voler cancellare ?', mtWarning,
               [mbNo, mbYes], 0) = mrYes then begin
               with Data do begin
                    DB.BeginTrans;
                    try
                         Q1.Close;
                         Q1.SQL.text := 'delete from AnagMansioni ' +
                              'where ID=' + Data.TAnagMansioni.FieldByName('ID').AsString;
                         Q1.ExecSQL;
                         DB.CommitTrans;
                         Data.TAnagMansioni.Close;
                         Data.TAnagMansioni.Open;
                    except
                         DB.RollbackTrans;
                         MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                    end;
               end;
          end;
end;
//[/TONI20020921\]DEBUGFINEOK

procedure TSchedaCandForm.ToolbarButton9745Click(Sender: TObject);
begin
     MessageDlg('ATTENZIONE!! Prima di procedere con la presentazione di personale ' +
          'accertarsi che il/i candidato/i sia/siano consenziente/i. (Decreto Legislativo 30 Giugno 2003 N. 196)', mtInformation, [mbOK], 0);
     if not MainForm.CheckProfile('073') then Exit;

     if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 7)) = 'FERRARI' then begin
          // FERRARI
          MainForm.CompilaFilePresPosFerrari;
          exit;
     end;
     // OpenJob
     if Pos('OPENJOB', UpperCase(Data.Global.FieldByName('NomeAzienda').AsString)) > 0 then
     begin
          SelRicCandForm := TSelRicCandForm.create(self);
          SelRicCandForm.QRicCand.ParambyName['xIDAnag'] := Data.TAnagrafica.FieldByName('ID').AsINteger;
          SelRicCandForm.QRicCand.Open;
          if SelRicCandForm.QRicCand.RecordCount = 0 then begin
               MessageDlg('Non risultano ricerche attive associate al soggetto', mtError, [mbOK], 0);
               SelRicCandForm.Free;
               exit;
          end else
          begin
               SelRicCandForm.ShowModal;
               if SelRicCandForm.ModalResult = mrOK then
               begin
                    MainForm.CompilaFilePresPosOpenJob(SelRicCandForm.QRicCand.FieldByName('Posizione').asString,
                         SelRicCandForm.QRicCand.FieldByName('IDCandRic').asinteger,
                         SelRicCandForm.QRicCand.FieldByName('IDCliente').asInteger,
                         SelRicCandForm.QRicCand.FieldByName('IDMansione').asInteger,
                         SelRicCandForm.QRicCand.FieldByName('IDricerca').asInteger);
                    exit;
               end else exit;
          end;
     end;



     SelRicCandForm := TSelRicCandForm.create(self);
     SelRicCandForm.QRicCand.ParambyName['xIDAnag'] := Data.TAnagrafica.FieldByName('ID').AsINteger;
     SelRicCandForm.QRicCand.Open;
     if SelRicCandForm.QRicCand.RecordCount = 0 then begin
          MessageDlg('Non risultano ricerche attive associate al soggetto', mtError, [mbOK], 0);
          SelRicCandForm.Free;
          exit;
     end else begin
          SelRicCandForm.ShowModal;
          if SelRicCandForm.ModalResult = mrOK then begin
               // GENERA PRESENTAZIONE
               {if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 6)) = 'LEDERM' then begin
                    // SOLO LEDERMANN
                    SelContattiForm := TSelContattiForm.create(self);
                    SelContattiForm.QContatti.SQL.text := 'select ID,Contatto,Telefono ' +
                         'from EBC_ContattiClienti where IDCliente=' + SelRicCandForm.QRicCand.FieldByName('IDCliente').asString;
                    SelContattiForm.QContatti.Open;
                    SelContattiForm.ShowModal;
                    if SelContattiForm.Modalresult = mrOK then
                         MainForm.CompilaFilePresPosLWG(SelRicCandForm.QRicCand.FieldByName('Posizione').asString,
                              SelRicCandForm.QRicCand.FieldByName('IDCandRic').asinteger,
                              SelRicCandForm.QRicCand.FieldByName('IDCliente').asInteger,
                              SelContattiForm.QContatti.FieldByName('ID').AsINteger);
                    SelContattiForm.Free;
               end else begin     }


                         // STV
               if Pos('STV', UpperCase(Data.Global.FieldByName('NomeAzienda').AsString)) > 0 then
               begin
                    mainform.CompilaFilePresPosSTV(SelRicCandForm.QRicCand.FieldByName('TitoloCliente').asString,
                         SelRicCandForm.QRicCand.FieldByName('IDCandRic').asinteger,
                         SelRicCandForm.QRicCand.FieldByName('IDCliente').asInteger,
                         SelRicCandForm.QRicCand.FieldByName('IDMansione').asInteger);
                    exit;
               end;

               if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 6)) = 'PROPOS' then begin
                    // PROPOSTE
                    MainForm.CompilaFilePresPosProposte(SelRicCandForm.QRicCand.FieldByName('Posizione').asString,
                         SelRicCandForm.QRicCand.FieldByName('IDCandRic').asinteger,
                         SelRicCandForm.QRicCand.FieldByName('IDCliente').asInteger,
                         SelRicCandForm.QRicCand.FieldByName('IDMansione').asInteger);
               end else begin
                    if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 6)) = 'MCS' then begin
                         // MCS
                         MainForm.CompilaFilePresPosMCS(SelRicCandForm.QRicCand.FieldByName('Posizione').asString,
                              SelRicCandForm.QRicCand.FieldByName('IDCandRic').asinteger,
                              SelRicCandForm.QRicCand.FieldByName('IDCliente').asInteger,
                              SelRicCandForm.QRicCand.FieldByName('IDMansione').asInteger);
                    end else begin
                         if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 6)) = 'BOYDEN' then begin
                              // BOYDEN
                              MainForm.CompilaFilePresPosBoyden(SelRicCandForm.QRicCand.FieldByName('TitoloCliente').asString,
                                   SelRicCandForm.QRicCand.FieldByName('IDCandRic').asinteger,
                                   SelRicCandForm.QRicCand.FieldByName('IDCliente').asInteger,
                                   SelRicCandForm.QRicCand.FieldByName('IDMansione').asInteger);
                         end else begin
                              // TUTTI GLI ALTRI
                              MainForm.CompilaFilePresPos(SelRicCandForm.QRicCand.FieldByName('Cliente').asString,
                                   SelRicCandForm.QRicCand.FieldByName('Posizione').asString,
                                   SelRicCandForm.QRicCand.FieldByName('IDCandRic').asinteger);
                         end;
                    end;
               end;
               //end;
          end;
          SelRicCandForm.Free;
     end;
end;

procedure TSchedaCandForm.TbBCompDipNewClick(Sender: TObject);
begin
     MainForm.TbBCompDipNewClick(self);
end;

procedure TSchedaCandForm.ToolbarButton9716Click(Sender: TObject);
begin
     Mainform.ToolbarButton9716Click(self);
end;

procedure TSchedaCandForm.TbBCompDipDelClick(Sender: TObject);
begin
     Mainform.TbBCompDipDelClick(self);

end;

procedure TSchedaCandForm.ToolbarButton979Click(Sender: TObject);
begin

     Mainform.ToolbarButton979Click(self);
end;

procedure TSchedaCandForm.ToolbarButton9710Click(Sender: TObject);
begin
     Mainform.ToolbarButton9710Click(self);
end;

procedure TSchedaCandForm.TbBCarattDipNewClick(Sender: TObject);
begin
     Mainform.TbBCarattDipNewClick(self)
end;

procedure TSchedaCandForm.TbBCarattDipModClick(Sender: TObject);
begin
     Mainform.TbBCarattDipModClick(self)
end;

procedure TSchedaCandForm.TbBCarattDipDelClick(Sender: TObject);
begin
     Mainform.TbBCarattDipDelClick(self)
end;

procedure TSchedaCandForm.ToolbarButton977Click(Sender: TObject);
begin
     MainForm.TbBGestStoricoClick(self);
end;

procedure TSchedaCandForm.BVediStoriaContattiClick(Sender: TObject);
begin
     MainForm.BVediStoriaContattiClick(self);
end;

procedure TSchedaCandForm.DbAnagFoto_oldGetGraphicClass(Sender: TObject;
     var GraphicClass: TGraphicClass);
begin
     if (pos('BIESSE', UpperCase(Data.Global.FieldByName('NomeAzienda').Asstring)) > 0) or
          (pos('INTERMEDIA', UpperCase(Data.Global.FieldByName('NomeAzienda').Asstring)) > 0) or
          (pos('BFK', UpperCase(Data.Global.FieldByName('NomeAzienda').Asstring)) > 0) then
          GraphicClass := TJPEGImage
     else
          GraphicClass := TBitmap;
end;

procedure TSchedaCandForm.BAnagFileNewClick(Sender: TObject);
begin
     Mainform.BAnagFileNewClick(self);
end;

procedure TSchedaCandForm.BAnagFileModClick(Sender: TObject);
begin
     MainForm.BAnagFileModClick(self);
end;

procedure TSchedaCandForm.BAnagFileDelClick(Sender: TObject);
begin
     MainForm.BAnagFileDelClick(self);
end;

procedure TSchedaCandForm.BFileCandOpenClick(Sender: TObject);
begin
     if data.qanagfile.Eof then exit;
     aprifileDue(data.qanagfile.fieldbyname('id').asinteger, 'schedacandform');
end;

procedure TSchedaCandForm.FormCloseQuery(Sender: TObject;
     var CanClose: Boolean);
begin
     MainForm.OnCloseQuery(self, canclose);
end;

procedure TSchedaCandForm.ToolbarButton971Click(Sender: TObject);
begin
     salvafileDue(data.qanagfile.fieldbyname('id').asinteger, 'schedacandform');
end;

procedure TSchedaCandForm.dxDBGrid5DblClick(Sender: TObject);
begin
     LeggiNoteForm := TLeggiNoteForm.Create(self);
     LeggiNoteForm.mNote.Text := DataRicerche.QAnagNoteRic.FieldByName('Note').AsString;
     LeggiNoteForm.ShowModal;
     LeggiNoteForm.Free;
end;

procedure TSchedaCandForm.DBEdit12Enter(Sender: TObject);
begin
     if pos('INTERMEDIA', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) > 0 then
          if DBEdit12.Text = '' then DBEdit12.Text := '+39';
end;

procedure TSchedaCandForm.BFileDaModelloClick(Sender: TObject);
begin
     MainForm.BFileDaModelloClick(Self);
     {if not MainForm.CheckProfile('072') then Exit;
     SceltaModelloAnagForm := TSceltaModelloAnagForm.create(self);
     SceltaModelloAnagForm.Height := 274;
     SceltaModelloAnagForm.QModelliWord.SQL.text := 'select * from ModelliWord where H1Sel>0';
     SceltaModelloAnagForm.QModelliWord.Open;
     SceltaModelloAnagForm.ShowModal;
     if SceltaModelloAnagForm.ModalResult = mrOK then begin
          //xsalva := SceltaModelloAnagForm.CBSalva.Checked;
          if data.global.fieldbyname('AnagFileDentroDB').AsBoolean then
               xsalva := true
          else
               xsalva := SceltaModelloAnagForm.CBSalva.Checked;
          xStampa := False;
          xFile := CreaFileWordAnag(Data.TAnagrafica.FieldByName('ID').AsInteger, SceltaModelloAnagForm.QModelliWord.FieldByName('ID').AsInteger,
               SceltaModelloAnagForm.QModelliWord.FieldByName('NomeModello').AsString,
               xSalva, xStampa, TModelCompositionTV.Create(self));
          if xsalva and (xfile <> '') then begin
               // associa al soggetto
               with Data do begin
                    DB.BeginTrans;
                    try
                         Q1.Close;
                         Q1.SQL.text := 'insert into AnagFile (IDAnagrafica,IDTipo,Descrizione,NomeFile,DataCreazione,SoloNome,FileType,FlagInvio,VisibleCliente) ' +
                              'values (:xIDAnagrafica:,:xIDTipo:,:xDescrizione:,:xNomeFile:,:xDataCreazione:,:xSoloNome:,:xFileType:,:xFlagInvio:,:xVisibleCliente:)';
                         Q1.ParambyName['xIDAnagrafica'] := Data.TAnagrafica.FieldByName('ID').AsInteger;
                         Q1.ParambyName['xIDTipo'] := 3;
                         Q1.ParambyName['xDescrizione'] := SceltaModelloAnagForm.QModelliWord.FieldByName('Descrizione').AsString;
                         Q1.ParambyName['xNomeFile'] := xFile;
                         Q1.ParambyName['xSoloNome'] := ExtractFileName(xFile);
                         Q1.ParambyName['xDataCreazione'] := Date;
                         Q1.ParambyName['xFileType'] := GetMIMETypeFromFile(xFile);
                         Q1.ParambyName['xFlagInvio'] := SceltaModelloAnagForm.CBFlagInvio.checked;
                         Q1.ParambyName['xVisibleCliente'] := SceltaModelloAnagForm.CBVisibleCliente.checked;
                         Q1.ExecSQL;
               end;
          end;
     end;
     SceltaModelloAnagForm.Free; }
end;

procedure TSchedaCandForm.Inserisci1Click(Sender: TObject);
var xfilename: string;
begin

     if MainForm.OpenDialog1.Execute then begin
          xfilename := MainForm.OpenDialog1.FileName;

          Data.TAnagrafica.Edit;

          Data.TAnagraficaFoto.LoadFromFile(xFileName);
          Data.TAnagrafica.Post;
          //dbanagfoto.LoadFromFile;

         { with Data.QAnagFoto do begin
               Data.DB.BeginTrans;
               try
                    Data.DB.CommitTrans;
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                    raise;
               end;
          end;    }
               //Data.QAnagFoto.Close;
               //Data.QAnagFoto.Open;
     end;
end;

procedure TSchedaCandForm.Rimuovi1Click(Sender: TObject);
begin
     if Data.TAnagrafica.RecordCount = 0 then Exit;
     if MessageDlg('sei sicuro di voler cancellare la foto per questo soggetto', mtWarning, [mbYes, mbNo], 0) = mrNo then exit;
     if Data.DsAnagrafica.state <> dsEdit then Data.TAnagrafica.Edit;
     Data.TAnagrafica.FieldByName('Foto').Value := '';
     Data.TAnagrafica.Post;

     Data.Tanagrafica.Close;
     Data.TAnagrafica.Open;
end;

procedure TSchedaCandForm.Panel181DblClick(Sender: TObject);
begin
     if PNoteCliente.Align = altop then begin
          PNoteCliente.Align := alClient;
          PNoteInterne.Visible := false;
          PNoteCommesse.Visible := false;
          PNoteCandidato.Visible := false;

     end else begin
          SistemaNote;
     end;
end;

procedure TSchedaCandForm.Panel10DblClick(Sender: TObject);
begin
     if PNoteINterne.visible = true then begin
          PNoteInterne.Visible := false;
          PNoteCommesse.Visible := false;
          PNotecliente.Visible := false;

     end else begin
          SistemaNote;
     end;
end;

procedure TSchedaCandForm.Sistemanote;
begin
     PNoteInterne.Align := alTop;
     PNoteInterne.visible := true;
     PNoteInterne.top := 0;
     PNoteInterne.Height := 177;

     if pos('INTERMEDIA', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) = 0 then begin
          PNoteCliente.Align := altop;
          PNoteCliente.Visible := true;
          PNoteCliente.Height := 100;
          PNoteCliente.top := 275;
     end;

     PNoteCommesse.align := altop;
     PNoteCommesse.Visible := true;
     PNoteCommesse.Height := 94;
     PNoteCommesse.top := 179;

     PNoteCandidato.Align := alClient;
     PNoteCandidato.Visible := true;
     PNoteCandidato.top := 377;



     //  if pos('INTERMEDIA', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) > 0 then
         //   PNoteCliente.visible := false;


end;

procedure TSchedaCandForm.Panel13DblClick(Sender: TObject);
begin
     if PNoteInterne.Align = alTop then begin
          PNoteInterne.Align := alClient;
          PNoteCliente.Visible := false;
          PNoteCommesse.Visible := false;
          PNoteCandidato.Visible := false;
     end else begin
          SistemaNote;

     end;
end;

procedure TSchedaCandForm.Panel153DblClick(Sender: TObject);
begin
     if PNoteCommesse.align = altop then begin
          PNoteCommesse.align := alClient;
          PNoteInterne.Visible := false;
          PNoteCandidato.Visible := false;
          PNotecliente.Visible := false;

     end else begin
          SistemaNote;
     end;
end;

procedure TSchedaCandForm.AdattaStretchClick(Sender: TObject);
begin
     if AdattaStretch.Checked then begin
          AdattaStretch.Checked := False;
          DbAnagFoto.Stretch := false;
     end else begin
          AdattaStretch.Checked := True;
          DbAnagFoto.Stretch := True;
     end;
end;

procedure TSchedaCandForm.BInsRicercaClick(Sender: TObject);
begin
     MainForm.BInsRicercaClick(self);
end;

procedure TSchedaCandForm.DBEdit1Enter(Sender: TObject);
begin
     if pos('INTERMEDIA', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) > 0 then
          if DBEdit1.Text = '' then DBEdit1.Text := '+39';
end;

procedure TSchedaCandForm.DBEdit70Enter(Sender: TObject);
begin
     if pos('INTERMEDIA', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) > 0 then
          if DBEdit70.Text = '' then DBEdit70.Text := '+39';
end;

procedure TSchedaCandForm.DBEdit18Enter(Sender: TObject);
begin
     if pos('INTERMEDIA', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) > 0 then
          if DBEdit18.Text = '' then DBEdit18.Text := '+39';
end;

procedure TSchedaCandForm.DBEdit15Enter(Sender: TObject);
begin
     if pos('INTERMEDIA', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) > 0 then
          if DBEdit15.Text = '' then DBEdit15.Text := '+39';
end;

procedure TSchedaCandForm.DBEdit24Enter(Sender: TObject);
begin
     if pos('INTERMEDIA', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) > 0 then
          if DBEdit24.Text = '' then DBEdit24.Text := '+39';
end;

procedure TSchedaCandForm.BOKClick(Sender: TObject);
begin
     with Data do begin

          if Data.Global.FieldByName('Storicizza_Sel').asBoolean then begin
             // controllo modifica campi x storicizzazione
               if (xCodiceFiscale <> DBEdit17.Text) or (xDataNascita <> DbDateEdit971.Text) or (xLuogoNascita <> DBEdit65.Text) or
                    (xStatoNascita <> DBEStatoNascita.text) or (xStatoCivile <> DBLookupComboBox1.Text) or
                    (xTipoStrada <> DBComboBox7.text) or (xIndirizzo <> DBEdit6.text) or (xNumCivico > DBEdit54.text) or
                    (xCap <> DBEdit7.text) or (xComune <> DBEdit8.text) or (xProvincia <> DBEdit9.text) or (xStato <> DBEdit82.text) or
                    (xDomicilioTipoStrada <> DBComboBox8.text) or (xDomicilioIndirizzo <> DBEdit3.text) or (xDomicilioNumCivico <> DBEdit79.text) or
                    (xDomicilioCap <> DBEdit4.text) or (xDomicilioComune <> DBEdit10.text) or (xDomicilioProvincia <> DBEdit11.text) or (xDomicilioStato <> DBEdit93.text) or
                    (xRecapitiTelefonici <> DBEdit1.text) or (xCellulare <> DBEdit12.text) or (xTelUfficio <> DBEdit70.text) or (xEmail <> DBEdit25.text) or
                    (xEmailUfficio <> DBEdit26.text) then begin

                    SelDataForm := TSelDataForm.create(self);
                    SelDataForm.Caption := 'Data di storicizzazione';
                    SelDataForm.DEData.Date := Date;
                    SelDataForm.ShowModal;
                    if SelDataForm.ModalResult = mrOK then begin

                        StoricizzaAnagrafica(SelDataForm.DEData.Date, TAnagraficaID.Value, False);

                    end;
                    SelDataForm.Free;
               end;
          end;

          Data.DB.BeginTrans;
          BModifica.Enabled := True;
          BOk.Enabled := False;
          BAnnulla.Enabled := False;
          try
               qAnagAltreInfo.Post;
               TAnagrafica.Post;
               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               raise;
          end;
          PanAnag.Enabled := False;
          Panel2.Enabled := False;
     end;
     MainForm.AnagraficaSearch;
end;

procedure TSchedaCandForm.BAnnullaClick(Sender: TObject);
begin
     BModifica.Enabled := True;
     BOk.Enabled := False;
     BAnnulla.Enabled := False;
     Data.TAnagrafica.Cancel;
     Data.qAnagAltreInfo.Cancel;
     PanAnag.Enabled := False;
     Panel2.Enabled := False;
end;

procedure TSchedaCandForm.SpeedButton57Click(Sender: TObject);
begin
     //MainForm.SpeedButton78Click(nil);
     SelNazioneForm := TSelNazioneForm.create(self);
     SelNazioneForm.Caption := 'Selezionare Nazionalit�';
     SelNazioneForm.dxDBGrid1.ColumnByFieldName('DescNazione').Visible := false;
     SelNazioneForm.dxDBGrid1.ColumnByFieldName('AreaGeografica').Visible := false;
     SelNazioneForm.dxDBGrid1.ColumnByFieldName('DescNazionalita').Visible := true;
     SelNazioneForm.ShowModal;
     if (SelNazioneForm.ModalResult = mrOK) and (SelNazioneForm.QNazioni.RecordCount > 0) then
          Data.qAnagAltreInfo.FieldByName('Nazionalita').AsString := SelNazioneForm.QNazioni.FieldByName('DescNazionalita').AsString;
     SelNazioneForm.Free;
end;

procedure TSchedaCandForm.SpeedButton60Click(Sender: TObject);
begin
     //MainForm.SpeedButton60Click(nil);
     SelNazioneForm := TSelNazioneForm.create(self);
     SelNazioneForm.ShowModal;
     if (SelNazioneForm.ModalResult = mrOK) and (SelNazioneForm.QNazioni.RecordCount > 0) then
          Data.TAnagraficaStatoNascita.Value := SelNazioneForm.QNazioniAbbrev.Value;
     SelNazioneForm.Free;
end;

procedure TSchedaCandForm.SpeedButton59Click(Sender: TObject);
begin
     //MainForm.SpeedButton59Click(nil);
     ComunitaNazStatoForm := TComunitaNazStatoForm.create(self);
     ComunitaNazStatoForm.showmodal;
     if ComunitaNazStatoForm.ModalResult = mrok then
          Data.qAnagAltreInfoIDComunitaNazStato.Value := ComunitaNazStatoForm.QComunitaNaz_StatoID.Value;
     ComunitaNazStatoForm.free;
end;

procedure TSchedaCandForm.CBFiltroAnagRicClick(Sender: TObject);
begin
     Data2.QAnagRicerche.Close;
     if CBFiltroAnagRic.Checked then begin
          Data2.QAnagRicerche.Filter := 'Escluso=0';
          Data2.QAnagRicerche.Filtered := True;
     end else Data2.QAnagRicerche.Filtered := False;
     Data2.QAnagRicerche.Open;

end;

procedure TSchedaCandForm.ToolbarButton975Click(Sender: TObject);
var xid: integer;
begin
     xid := Data2.QAnagRicercheID.AsInteger;
     MainForm.ToolbarButton9729Click(self);
     Data2.QAnagRicerche.Locate('ID', xid, []);
end;

procedure TSchedaCandForm.ToolbarButton976Click(Sender: TObject);
begin
     MainForm.ToolbarButton9730Click(self);
end;

procedure TSchedaCandForm.ToolbarButton9747Click(Sender: TObject);
begin
     SchedaCandForm.xChiamante := 'SchedaCand';
     MainForm.ToolbarButton977Click(self);
end;

procedure TSchedaCandForm.ToolbarButton973Click(Sender: TObject);
begin
     SchedaCandForm.ModalResult := mrOK;
end;

procedure TSchedaCandForm.ToolbarButton972Click(Sender: TObject);
begin
     DBEdit17.Clear;
     //BOKClick(nil);
end;

procedure TSchedaCandForm.ToolbarButton974Click(Sender: TObject);
var cf: string;
begin
     //MainForm.SpeedButton51Click(nil);
     if (DBEdit65.text = '') and ((DBEStatoNascita.text = '') or (DBEStatoNascita.text = 'ITA')) then begin
          MessageDlg('Inserire il luogo di nascita' + cf, mtWarning, [mbOK], 0);
          exit;
     end;

     if DbDateEdit971.Text = '' then begin
          MessageDlg('Inserire l''anno di nascita ' + cf, mtWarning, [mbOK], 0);
          exit;
     end;
     if DBComboBox1.Text = '' then begin
          MessageDlg('Inserire il sesso ' + cf, mtWarning, [mbOk], 0);
          exit;
     end;

     cf := calc_CF(DBECognome.Text, DBEdit2.text, DbDateEdit971.Text,
          DBComboBox1.Text[1], DBEdit65.Text, uppercase(DBEStatoNascita.text));
     if ch = true then begin
          if DBEdit17.Text = '' then begin
               DBEdit17.Text := cf;
               BOKClick(nil);
          end else begin

               if (trim(cf) <> trim(DBEdit17.Text)) then begin
                    if MessageDlg('Codice Fiscale Sbagliato - Vuoi Sostituire con quello Calcolato?  ' + cf, mtWarning, [mbYes, MbNo], 0) = mrYes then
                         DBEdit17.Text := cf;
                    BOKClick(nil);
               end
               else
                    if MessageDlg('Codice Fiscale Giusto', mtConfirmation, [mbOK], 0) = mrOK then exit;
          end;
     end;
end;

procedure TSchedaCandForm.CampiPersFrame1ToolbarButton971Click(
     Sender: TObject);
begin
     CampiPersFrame1.ToolbarButton971Click(Sender);
end;

procedure TSchedaCandForm.ToolbarButton978Click(Sender: TObject);
begin
     //MainForm.BAnagSelNazione1Click(nil);
     SelNazioneForm := TSelNazioneForm.create(self);
     SelNazioneForm.ShowModal;
     if (SelNazioneForm.ModalResult = mrOK) and (SelNazioneForm.QNazioni.RecordCount > 0) then begin
          Data.TAnagrafica.FieldByName('Stato').AsString := SelNazioneForm.QNazioni.FieldByName('DescNazione').AsString;
     end else begin
          Data.TAnagrafica.FieldByName('Stato').AsString := '';
     end;
     BOKClick(nil);
     SelNazioneForm.Free;
end;

procedure TSchedaCandForm.ToolbarButton9711Click(Sender: TObject);
begin
     //MainForm.BAnagSelNazione2Click(nil);
     SelNazioneForm := TSelNazioneForm.create(self);
     SelNazioneForm.ShowModal;
     if (SelNazioneForm.ModalResult = mrOK) and (SelNazioneForm.QNazioni.RecordCount > 0) then begin
          Data.TAnagrafica.FieldByName('DomicilioStato').AsString := SelNazioneForm.QNazioni.FieldByName('DescNazione').AsString;
     end else begin
          Data.TAnagrafica.FieldByName('DomicilioStato').AsString := '';
     end;
     BOKClick(nil);
     SelNazioneForm.Free;
end;

procedure TSchedaCandForm.CBClassPotenzialeChange(Sender: TObject);
begin
     MainForm.xClassificazione := CBClassPotenziale.Text;
end;

end.

