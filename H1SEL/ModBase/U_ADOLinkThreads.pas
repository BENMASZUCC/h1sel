unit U_ADOLinkThreads;

interface

uses
  Classes,ADODB;

type
  TADOLinkedDetailThread = class(TThread)
  private
    { Private declarations }
    FQuery :TCustomADODataSet;
  protected
    procedure Execute; override;
  public
    constructor Create (CreateSuspend : BOOLEAN);
    property DetailDataSet :TCustomADODataSet read FQuery write FQuery;
  end;

implementation

{ TADOLinkedDetailThread }
uses
  U_ADOLinkCl,DB;

constructor TADOLinkedDetailThread.Create(CreateSuspend: BOOLEAN);
begin
  inherited Create (CreateSuSpend);
end;

procedure TADOLinkedDetailThread.Execute;
begin
  while not Terminated do
  begin
    if FQuery.State = dsBrowse then
    begin
      if FQuery is TADOLinkedTable then
      begin //Table
        TADOLinkedTable (FQuery).Filtered := FALSE;
        if TADOLinkedTable (FQuery).MasterDataSet.FieldByName (TADOLinkedTable (FQuery).LinkedMasterField).AsVariant <> NULL THEN
        begin
          if TADOLinkedTable (FQuery).MasterDataSet.FieldByName (TADOLInkedTable (FQuery).LinkedMasterField).DataType <> ftString then
            TADOLInkedTable (FQuery).Filter := '('+TADOLinkedTable(FQuery).LinkedDetailField+'='+TADOLinkedTable (FQuery).MasterDataSet.FieldByName (TADOLInkedTable (FQuery).LinkedMasterField).AsString+')'
          else
            TADOLInkedTable (FQuery).Filter := '('+TADOLinkedTable(FQuery).LinkedDetailField+'='''+TADOLinkedTable (FQuery).MasterDataSet.FieldByName (TADOLInkedTable (FQuery).LinkedMasterField).AsString+''')';
        end
        else
          TADOLInkedTable (FQuery).Filter := '('+TADOLInkedTable (FQuery).LinkedDetailField+'=NULL)';
        //Sincronizzazione
        if Assigned (TADOLinkedTable (TADOLinkedTable (FQuery).MasterDataSet).MasterDataSet) then
          while not (TADOLinkedTable (FQuery).MasterDataSet.Filtered) do;
        TADOLinkedTable (FQuery).Filtered := TRUE;
        //Aggiorna Master per nr di Thread
        if TADOLinkedTable (FQuery).MasterDataSet is TThreadADOLinkedTable then
          TThreadADOLinkedTable(TADOLinkedTable (FQuery).MasterDataSet).IncThreads;
      end
      else
      begin //Query
        //TODO
      end;
    end
    else
      if TADOLinkedTable (FQuery).MasterDataSet is TThreadADOLinkedTable then
        Synchronize (TThreadADOLinkedTable(TADOLinkedTable (FQuery).MasterDataSet).IncThreads);
    Suspend;
  end;
end;

end.




