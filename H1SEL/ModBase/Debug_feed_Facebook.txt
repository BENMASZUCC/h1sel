PARAMETRI
xURLFeed = http://h1web.ebcconsulting.com/EBC_H1Sel_Cand_Pubb/Annunci
xURLPrelievoFeed = http://h1web.ebcconsulting.com/EBC_H1Sel_Cand_Pubb/PrelevaSalvaFile.aspx?Operazione=Feed%20#Qualefeed&Tabella=Ann_TemplateHTML&Campo=Feed_#Qualefeed&ID=1&NomeFile=#NomeFile
Query = select distinct ANN.ID, AED.ID IDPubb, Rif, AED.Data DataPubb, AED.NomeFileStatico, Ruolo= case   when ann.qualeruolousare=0 then (select top 1 descrizione from Ann_AnnunciRuoli A JOIN mansioni m on m.id=a.idmansione where a.idannuncio=ann.id)   when ann.qualeruolousare=1 then (select      test=      case      when r.titolocliente='' or r.titolocliente=null then m.descrizione      when r.titolocliente < '' or r.titolocliente > '' then r.titolocliente      end      from Ann_AnnunciRicerche AR 	join EBC_Ricerche R on AR.IDRicerca = R.ID 	join Mansioni M on R.IDMansione = M.Id where AR.IDAnnuncio=ann.id)   when ann.qualeruolousare=2 then ann.JobTitle end ,   cast(ANN.SoloTesto as varchar(2000)) SoloTesto, SETT.Attivita Attivita,   AP.Regione, AP.Provincia, TC.Cap, GL.LinkForm from Ann_Annunci ANN join Ann_AnnEdizData AED on ANN.ID = AED.IDAnnuncio join Ann_Edizioni ED on AED.IDEdizione = ED.ID join Ann_AnnunciProvince AP on ANN.ID = AP.IDAnnuncio  join TabCom TC on (AP.Provincia = TC.Prov and substring(TC.CAP,3,3) = '100' and TC.Codice is not null ) join Ann_AnnunciSettori ASET on ANN.ID = ASET.IDAnnuncio join EBC_Attivita SETT on ASET.IDSettore = SETT.ID  join Global GL on GL.LinkForm is not null where (DataScadenza is null or DataScadenza>getdate()) and AP.Flag_feed = 1 and ASET.Flag_feed = 1 
and AED.Feed_Facebook = 1 

Records = 3
XML = <?xml version="1.0" encoding="UTF-8"?> 
<rss xmlns:dc="http://purl.org/dc/elements/1.1/" version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
<channel>
 <title>Annunci di lavoro - OPENJOB</title>
 <link>http://h1web.ebcconsulting.com/EBC_H1Sel_Cand_Pubb/Annunci</link>
 <description>Notifiche annunci di OPENJOB</description>
 <atom:link href="http://h1web.ebcconsulting.com/EBC_H1Sel_Cand_Pubb/Annunci/feed_Facebook.xml" rel="self" type="application/rss+xml"/>
 <item> 
  <title>Annuncio rif. 19-ADdc - Praticante Studio Commercialista</title> 
  <link>http://h1web.ebcconsulting.com/EBC_H1Sel_Cand_Pubb/Annunci/</link> 
  <guid>http://h1web.ebcconsulting.com/EBC_H1Sel_Cand_Pubb/Annunci/</guid> 
  <description>Praticante Studio Commercialista</description> 
  <dc:creator>OPENJOB</dc:creator> 
  <pubDate>Wed, 3 Jan 2007 08:00:00 GMT</pubDate> 

 </item>

 <item> 
  <title>Annuncio rif. 30-ALia - Impiegata/o Amministrativa/o</title> 
  <link>http://h1web.ebcconsulting.com/EBC_H1Sel_Cand_Pubb/Annunci/</link> 
  <guid>http://h1web.ebcconsulting.com/EBC_H1Sel_Cand_Pubb/Annunci/</guid> 
  <description>Impiegata/o Amministrativa/o</description> 
  <dc:creator>OPENJOB</dc:creator> 
  <pubDate>Mon, 26 Feb 2007 08:00:00 GMT</pubDate> 

 </item>

 <item> 
  <title>Annuncio rif. 3131-ZVrs - Resp. Stabilimento Verniciatura</title> 
  <link>http://h1web.ebcconsulting.com/EBC_H1Sel_Cand_Pubb/Annunci/Resp._Stabilimento_Verniciatura-ZINCOL_ITALIA_SPA-PN-141.html</link> 
  <guid>http://h1web.ebcconsulting.com/EBC_H1Sel_Cand_Pubb/Annunci/Resp._Stabilimento_Verniciatura-ZINCOL_ITALIA_SPA-PN-141.html</guid> 
  <description>Resp. Stabilimento Verniciatura</description> 
  <dc:creator>OPENJOB</dc:creator> 
  <pubDate>Thu, 7 Aug 2008 08:00:00 GMT</pubDate> 

 </item>

 </channel>
</rss>

xURLPrelievoFeed = http://h1web.ebcconsulting.com/EBC_H1Sel_Cand_Pubb/PrelevaSalvaFile.aspx?Operazione=Feed%20Facebook&Tabella=Ann_TemplateHTML&Campo=Feed_Facebook&ID=1&NomeFile=Feed_Facebook.xml
xNomeFile = Feed_Facebook.xml
