unit IndirizziCliente;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     dxTL, dxDBCtrl, dxDBGrid, Db, ADODB, dxCntner, ExtCtrls, TB97,
     StdCtrls, Buttons, dxDBTLCl, dxGrClms;

type
     TIndirizziClienteForm = class(TForm)
          Panel1: TPanel;
          Panel2: TPanel;
          dbgClientiIndirizzi: TdxDBGrid;
          qClienteIndirizzi: TADOQuery;
          dsClienteIndirizzi: TDataSource;
          qClienteIndirizziID: TAutoIncField;
          qClienteIndirizziIDCliente: TIntegerField;
          qClienteIndirizziTipoStrada: TStringField;
          qClienteIndirizziIndirizzo: TStringField;
          qClienteIndirizziComune: TStringField;
          qClienteIndirizziProvincia: TStringField;
          qClienteIndirizziNumCivico: TStringField;
          qClienteIndirizziCAP: TStringField;
          qClienteIndirizziTelefono: TStringField;
          qClienteIndirizziFax: TStringField;
          qClienteIndirizziNazioneAbb: TStringField;
          dbgClientiIndirizziID: TdxDBGridMaskColumn;
          dbgClientiIndirizziIDCliente: TdxDBGridMaskColumn;
          dbgClientiIndirizziTipoStrada: TdxDBGridMaskColumn;
          dbgClientiIndirizziIndirizzo: TdxDBGridMaskColumn;
          dbgClientiIndirizziComune: TdxDBGridMaskColumn;
          dbgClientiIndirizziProvincia: TdxDBGridMaskColumn;
          dbgClientiIndirizziNumCivico: TdxDBGridMaskColumn;
          dbgClientiIndirizziCAP: TdxDBGridMaskColumn;
          dbgClientiIndirizziTelefono: TdxDBGridMaskColumn;
          dbgClientiIndirizziFax: TdxDBGridMaskColumn;
          dbgClientiIndirizziNazioneAbb: TdxDBGridMaskColumn;
          BIndCliNew: TToolbarButton97;
          BIndCliOK: TToolbarButton97;
          BIndCliDel: TToolbarButton97;
          BIndCliCan: TToolbarButton97;
          bIndCliMod: TToolbarButton97;
          qClienteIndirizziDescrizione: TStringField;
          dbgClientiIndirizziColumn12: TdxDBGridColumn;
          qClienteIndirizziPrincipale: TBooleanField;
          dbgClientiIndirizziPrincipale: TdxDBGridCheckColumn;
          qClienteIndirizziIDTipologia: TIntegerField;
          QTipoIndirizzi: TADOQuery;
          QTipoIndirizziID: TAutoIncField;
          QTipoIndirizziTipo: TStringField;
          DSTipoIndirizzi: TDataSource;
          qClienteIndirizziLKTipoIndirizzo: TStringField;
          dbgClientiIndirizziColumn14: TdxDBGridColumn;
          Q1: TADOQuery;
          ToolbarButton971: TToolbarButton97;
          procedure BIndCliNewClick(Sender: TObject);
          procedure BIndCliDelClick(Sender: TObject);
          procedure BIndCliOKClick(Sender: TObject);
          procedure BIndCliCanClick(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure dsClienteIndirizziStateChange(Sender: TObject);
          procedure qClienteIndirizziBeforeOpen(DataSet: TDataSet);
          procedure bIndCliModClick(Sender: TObject);
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
          procedure qClienteIndirizziBeforeEdit(DataSet: TDataSet);
          procedure ToolbarButton971Click(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
          xIdCliente: Integer;
     end;

var
     IndirizziClienteForm: TIndirizziClienteForm;

implementation

uses ModuloDati, InsIndirizzo, uUtilsVarie, Main;

{$R *.DFM}

procedure TIndirizziClienteForm.BIndCliNewClick(Sender: TObject);
begin
     {qClienteIndirizzi.Insert;
     qClienteIndirizziIDCliente.Value := xIdCliente; }

     InsindirizzoForm := TInsindirizzoForm.create(self);
     InsindirizzoForm.ShowModal;
     if InsindirizzoForm.ModalResult = mrOk then begin
          Data.Q1.close;
          Data.Q1.SQL.Text := 'Insert into EBC_ClienteIndirizzi (IDCliente, TipoStrada,Indirizzo,Comune, ' +
               'Provincia, NumCivico, CAP, Telefono, Fax, NazioneAbb, Descrizione,IDTipologia) ' +
               'values (:xIDCliente:, :xTipoStrada:,:xIndirizzo:,:xComune:, ' +
               ':xProvincia:, :xNumCivico:, :xCAP:, :xTelefono:, :xFax:, :xNazioneAbb:, :xDescrizione:, :xIDTipologia:)';
          Data.Q1.ParamByName['xIDCliente'] := xIdCliente;
          Data.Q1.ParamByName['xTipoStrada'] := InsindirizzoForm.cbTipoStrada.text;
          Data.Q1.ParamByName['xIndirizzo'] := InsindirizzoForm.eIndirizzo.text;
          Data.Q1.ParamByName['xComune'] := InsindirizzoForm.eComune.text;
          Data.Q1.ParamByName['xProvincia'] := InsindirizzoForm.eProvincia.text;
          Data.Q1.ParamByName['xNumCivico'] := InsindirizzoForm.eNumCivico.text;
          Data.Q1.ParamByName['xCAP'] := InsindirizzoForm.eCAP.text;
          Data.Q1.ParamByName['xTelefono'] := InsindirizzoForm.eTelefono.text;
          Data.Q1.ParamByName['xFax'] := InsindirizzoForm.eFax.text;
          Data.Q1.ParamByName['xNazioneAbb'] := InsindirizzoForm.eNazione.text;
          Data.Q1.ParamByName['xDescrizione'] := InsindirizzoForm.EDescriz.text;

          if InsindirizzoForm.DBLKTipoIndirizzo.LookupKeyValue = NULL then
               Data.Q1.ParamByName['xIDTipologia'] := NULL
          else
               Data.Q1.ParamByName['xIDTipologia'] := InsindirizzoForm.DBLKTipoIndirizzo.LookupKeyValue;

          Data.Q1.ExecSQL;

          qClienteIndirizzi.close;
          qClienteIndirizzi.open;
          InsindirizzoForm.Free;
     end;
end;

procedure TIndirizziClienteForm.BIndCliDelClick(Sender: TObject);
begin
     if qClienteIndirizzi.Eof then Exit;
     if not VerifIntegrRef('Ann_Annunci', 'IDIndirizzoCliente', qClienteIndirizziID.asString) then exit;

     if not VerifIntegrRef('EBC_Ricerche', 'IDIndirizzoCliente', qClienteIndirizziID.asString) then exit;

     if Messagedlg('Sei sicuro di voler eliminare l''indirizzo selezionato?', mtWarning, [mbYes, mbNO], 0) = mrYes then begin
          qClienteIndirizzi.delete;
          qClienteIndirizzi.Close;
          qClienteIndirizzi.Open;
     end;
end;

procedure TIndirizziClienteForm.BIndCliOKClick(Sender: TObject);
begin
     qClienteIndirizzi.Post;
end;

procedure TIndirizziClienteForm.BIndCliCanClick(Sender: TObject);
begin
     qClienteIndirizzi.cancel;
end;

procedure TIndirizziClienteForm.FormShow(Sender: TObject);
begin
     qClienteIndirizzi.Open;
     IndirizziClienteForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
end;

procedure TIndirizziClienteForm.dsClienteIndirizziStateChange(
     Sender: TObject);
var b: boolean;
begin
     b := dsClienteIndirizzi.State in [dsEdit, dsInsert];
     BIndCliNew.Enabled := not b;
     BIndCliDel.Enabled := not b;
     BIndCliOK.Enabled := b;
     BIndCliCAn.Enabled := b;
end;

procedure TIndirizziClienteForm.qClienteIndirizziBeforeOpen(
     DataSet: TDataSet);
begin
     QTipoIndirizzi.close;
     QTipoIndirizzi.Open;
     qClienteIndirizzi.Parameters.ParamByName('xIDCliente').Value := xIdCliente;
end;

procedure TIndirizziClienteForm.bIndCliModClick(Sender: TObject);
begin
     if qClienteIndirizziPrincipale.value then begin
          MessageDlg('Non � possibile modificare un indirizzo principale', mtError, [mbOK], 0);
          Exit;
     end;

     InsindirizzoForm := TInsindirizzoForm.create(self);
     InsindirizzoForm.EDescriz.Text := qClienteIndirizziDescrizione.Value;
     InsindirizzoForm.cbTipoStrada.Text := qClienteIndirizziTipoStrada.Value;
     InsindirizzoForm.eIndirizzo.Text := qClienteIndirizziIndirizzo.Value;
     InsindirizzoForm.eNumCivico.Text := qClienteIndirizziNumCivico.Value;
     InsindirizzoForm.eComune.Text := qClienteIndirizziComune.Value;
     InsindirizzoForm.eProvincia.Text := qClienteIndirizziProvincia.Value;
     InsindirizzoForm.ecap.Text := qClienteIndirizziCap.Value;
     InsindirizzoForm.eNazione.Text := qClienteIndirizziNazioneAbb.Value;
     InsindirizzoForm.eTelefono.Text := qClienteIndirizziTelefono.Value;
     InsindirizzoForm.eFax.Text := qClienteIndirizziFax.Value;
     InsindirizzoForm.ShowModal;
     if InsindirizzoForm.ModalResult = mrOk then begin


          Q1.close;
          Q1.SQL.Text := 'Update EBC_ClienteIndirizzi set TipoStrada=:x0,Indirizzo=:x1, Comune=:x2, ' +
               'Provincia=:x3, NumCivico=:x4, CAP=:x5, Telefono=:x6, Fax=:x7, NazioneAbb=:x8, ' +
               'Descrizione=:x9, IDTipologia=:x10' +
               ' where id = :x11';
          //Q1.Paramters[0].value:= xIdCliente;
          Q1.Parameters[0].value := InsindirizzoForm.cbTipoStrada.text;
          Q1.Parameters[1].value := InsindirizzoForm.eIndirizzo.text;
          Q1.Parameters[2].value := InsindirizzoForm.eComune.text;
          Q1.Parameters[3].value := InsindirizzoForm.eProvincia.text;
          Q1.Parameters[4].value := InsindirizzoForm.eNumCivico.text;
          Q1.Parameters[5].value := InsindirizzoForm.eCAP.text;
          Q1.Parameters[6].value := InsindirizzoForm.eTelefono.text;
          Q1.Parameters[7].value := InsindirizzoForm.eFax.text;
          Q1.Parameters[8].value := InsindirizzoForm.eNazione.text;
          Q1.Parameters[9].value := InsindirizzoForm.eDescriz.Text;

          if InsindirizzoForm.DBLKTipoIndirizzo.LookupKeyValue = NULL then
               Q1.Parameters[10].value := NULL
          else
               Q1.Parameters[10].value := InsindirizzoForm.DBLKTipoIndirizzo.LookupKeyValue;

          Q1.Parameters[11].value := qClienteIndirizziID.Value;

          Q1.ExecSQL;

          qClienteIndirizzi.close;
          qClienteIndirizzi.open;

     end;
     InsindirizzoForm.Free;
end;

procedure TIndirizziClienteForm.FormClose(Sender: TObject;
     var Action: TCloseAction);
begin
     if dsClienteIndirizzi.state in [dsInsert, dsEdit] then qClienteIndirizzi.Post;
end;

procedure TIndirizziClienteForm.qClienteIndirizziBeforeEdit(DataSet: TDataSet);
begin
     if qClienteIndirizziPrincipale.value then begin
          MessageDlg('Non � possibile modificare un indirizzo principale', mtError, [mbOK], 0);
          abort;
     end;
end;

procedure TIndirizziClienteForm.ToolbarButton971Click(Sender: TObject);
begin
     IndirizziClienteForm.ModalResult := mrOK;
end;

end.

