unit MDRicerche;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, DBTables, ADODB, U_ADOLinkCl;

type
     TDataRicerche = class(TDataModule)
          DsRicerchePend: TDataSource;
          DsQCandRic: TDataSource;
          DsQCandTutte: TDataSource;
          DsAnagColloqui: TDataSource;
          DsQRicAttive: TDataSource;
          DsStoricoRic: TDataSource;
          DsQCandAzienda: TDataSource;
          DsQOrgNew: TDataSource;
          DsQTipoCommesse: TDataSource;
          DsQTipiComm: TDataSource;
          DsQAnnunciCand: TDataSource;
          DsAziende: TDataSource;
          DsQRicLineeQuery: TDataSource;
          DsQAnagIncompClienti: TDataSource;
          DsQAnagNoteRic: TDataSource;
          DsQRicTiming: TDataSource;
          dsQOrgNodoRuolo: TDataSource;
          DsQRicUtenti: TDataSource;
          DsQRicTargetList: TDataSource;
          DsQTLCandidati: TDataSource;
          DsQRicTimeSheet: TDataSource;
          DsQUsersLK: TDataSource;
          DsQGruppiProfLK: TDataSource;
          DsQTimeRepDett: TDataSource;
          DsQAnagTitoloMansAtt: TDataSource;
          QTipoCommesse: TADOLinkedQuery;
          TStoricoRic_old: TADOLinkedQuery;
          QRicLineeQuery: TADOLinkedQuery;
          QTotCandRic: TADOLinkedQuery;
          QRicAttive: TADOLinkedQuery;
          QRicTiming: TADOLinkedQuery;
          QAnagNoteRicOLD: TADOLinkedQuery;
          QTipiComm: TADOLinkedQuery;
          QAnagTitoloMansAtt: TADOLinkedQuery;
          QAnagIncompClienti: TADOLinkedQuery;
          QCandTutte: TADOLinkedQuery;
          QRicUtenti_old: TADOLinkedQuery;
          QTemp: TADOLinkedQuery;
          QTotCompDaFattLK: TADOLinkedQuery;
          QCausaliLK: TADOLinkedQuery;
          QAziende: TADOLinkedQuery;
          QGen: TADOLinkedQuery;
          QTitoliStudioLK: TADOLinkedQuery;
          QCandAzienda: TADOLinkedQuery;
          QOrgNew_OLD: TADOLinkedQuery;
          QTRepCausali: TADOLinkedQuery;
          QRicTargetList: TADOLinkedQuery;
          QOrgNodoRuolo: TADOLinkedQuery;
          QMansioniLK: TADOLinkedQuery;
          QTLCandidati: TADOLinkedQuery;
          QOfferteLK: TADOLinkedQuery;
          QUsersLK: TADOLinkedQuery;
          QGruppiProfLK: TADOLinkedQuery;
          TClientiLK: TADOLinkedTable;
          TRicAnagIDX: TADOLinkedTable;
          TRicercheLK: TADOLinkedTable;
          TStoricoRicABS: TADOLinkedTable;
          TCandRicABS: TADOLinkedTable;
          TAnagColloqui: TADOLinkedTable;
          TRicercheIDX: TADOLinkedTable;
          TColloquiABS: TADOLinkedTable;
          TContattiABS: TADOLinkedTable;
          QAnnunciCand_old: TADOLinkedQuery;
          QRicTimeSheet: TADOLinkedQuery;
          QTimeRepDett: TADOLinkedQuery;
          QCandRic_old: TADOLinkedQuery;
          TRicerchePend: TADOLinkedQuery;
          TRicerchePendID: TAutoIncField;
          TRicerchePendIDCliente: TIntegerField;
          TRicerchePendIDUtente: TIntegerField;
          TRicerchePendProgressivo: TStringField;
          TRicerchePendDataInizio: TDateTimeField;
          TRicerchePendDataFine: TDateTimeField;
          TRicerchePendStato: TStringField;
          TRicerchePendConclusa: TBooleanField;
          TRicerchePendIDStatoRic: TIntegerField;
          TRicerchePendDallaData: TDateTimeField;
          TRicerchePendIDArea: TIntegerField;
          TRicerchePendIDMansione: TIntegerField;
          TRicerchePendIDSpec: TIntegerField;
          TRicerchePendNumRicercati: TSmallintField;
          TRicerchePendNote: TMemoField;
          TRicerchePendIDFattura: TIntegerField;
          TRicerchePendSospesa: TBooleanField;
          TRicerchePendSospesaDal: TDateTimeField;
          TRicerchePendStipendioNetto: TFloatField;
          TRicerchePendStipendioLordo: TFloatField;
          TRicerchePendFatturata: TBooleanField;
          TRicerchePendSpecifiche: TMemoField;
          TRicerchePendSpecifEst: TMemoField;
          TRicerchePendggDiffApRic: TSmallintField;
          TRicerchePendggDiffUcRic: TSmallintField;
          TRicerchePendggDiffColRic: TSmallintField;
          TRicerchePendDataPrevChiusura: TDateTimeField;
          TRicerchePendArea: TStringField;
          TRicerchePendMansione: TStringField;
          TRicerchePendIDAreaMans: TIntegerField;
          TRicerchePendCliente: TStringField;
          TRicerchePendUtente: TStringField;
          TRicerchePendStatoRic: TStringField;
          TRicerchePendTipo: TStringField;
          TRicerchePendIDContrattoCG: TIntegerField;
          TRicerchePendIDSede: TIntegerField;
          TRicerchePendTitoloCliente: TStringField;
          TRicerchePendIDContattoCli: TIntegerField;
          TRicerchePendIDOfferta: TIntegerField;
          TRicerchePendSede: TStringField;
          TRicerchePendIDUtente2: TIntegerField;
          TRicerchePendIDUtente3: TIntegerField;
          TRicerchePendRifinterno: TStringField;
          TRicerchePendProvvigione: TBooleanField;
          TRicerchePendProvvigionePerc: TIntegerField;
          TRicerchePendQuandoPagamUtente: TStringField;
          TRicerchePendOreLavUtente1: TIntegerField;
          TRicerchePendOreLavUtente2: TIntegerField;
          TRicerchePendOreLavUtente3: TIntegerField;
          TRicerchePendIDCausale: TIntegerField;
          TRicerchePendCausale: TStringField;
          QTimeRepDettID: TAutoIncField;
          QTimeRepDettIDRicTimeSheet: TIntegerField;
          QTimeRepDettIDCausale: TIntegerField;
          QTimeRepDettOre: TFloatField;
          QTimeRepDettNote: TMemoField;
          QTimeRepDettCausale: TStringField;
          QOrgNew_OLDID: TAutoIncField;
          QOrgNew_OLDIDLivello: TIntegerField;
          QOrgNew_OLDTipo: TStringField;
          QOrgNew_OLDDescrizione: TStringField;
          QOrgNew_OLDDescBreve: TStringField;
          QOrgNew_OLDDataAgg: TDateTimeField;
          QOrgNew_OLDTipoSubordine: TStringField;
          QOrgNew_OLDNodoPadre: TIntegerField;
          QOrgNew_OLDIDMansione: TIntegerField;
          QOrgNew_OLDIDDipendente: TIntegerField;
          QOrgNew_OLDPercentuale: TSmallintField;
          QOrgNew_OLDCentroCosto: TIntegerField;
          QOrgNew_OLDMonteOreRichiesto: TSmallintField;
          QOrgNew_OLDTassoAssenteismo: TSmallintField;
          QOrgNew_OLDInterim: TBooleanField;
          QOrgNew_OLDInStaff: TBooleanField;
          QOrgNew_OLDValutazGen: TFloatField;
          QOrgNew_OLDIndiceSost: TFloatField;
          QOrgNew_OLDIndiceImp: TFloatField;
          QOrgNew_OLDUltimoAgg: TDateTimeField;
          QOrgNew_OLDPARENT: TIntegerField;
          QOrgNew_OLDWIDTH: TIntegerField;
          QOrgNew_OLDHEIGHT: TIntegerField;
          QOrgNew_OLDTYPE: TStringField;
          QOrgNew_OLDCOLOR: TIntegerField;
          QOrgNew_OLDIMAGE: TIntegerField;
          QOrgNew_OLDIMAGEALIGN: TStringField;
          QOrgNew_OLDORDINE: TIntegerField;
          QOrgNew_OLDALIGN: TStringField;
          QOrgNew_OLDCognome: TStringField;
          QOrgNew_OLDNome: TStringField;
          QOrgNew_OLDCVNumero: TIntegerField;
          QOrgNew_OLDMansione: TStringField;
          QOrgNew_OLDDicNodo: TStringField;
          QOrgNew_OLDIDReparto: TIntegerField;
          QOrgNew_OLDIDCliente: TIntegerField;
          QOrgNew_OLDReparto: TStringField;
          QRicTimeSheetID: TAutoIncField;
          QRicTimeSheetIDRicerca: TIntegerField;
          QRicTimeSheetIDUtente: TIntegerField;
          QRicTimeSheetData: TDateTimeField;
          QRicTimeSheetIDGruppoProfess: TIntegerField;
          QRicTimeSheetOreConsuntivo: TFloatField;
          QRicTimeSheetNote: TMemoField;
          QRicTimeSheetOrePreventivo: TIntegerField;
          QUsersLKID: TAutoIncField;
          QUsersLKNominativo: TStringField;
          QUsersLKDescrizione: TStringField;
          QUsersLKIDGruppoProfess: TIntegerField;
          QRicTimeSheetUtente: TStringField;
          QGruppiProfLKID: TAutoIncField;
          QGruppiProfLKGruppoProfessionale: TStringField;
          QGruppiProfLKRateOrarioPrev: TFloatField;
          QGruppiProfLKRateOrarioCons: TFloatField;
          QGruppiProfLKIDMansione: TIntegerField;
          QGruppiProfLKRateOrarioDirectPrev: TFloatField;
          QGruppiProfLKRateOrarioDirectCons: TFloatField;
          QRicTimeSheetGruppoProfess: TStringField;
          QAziendeID: TAutoIncField;
          QAziendeDescrizione: TStringField;
          QAziendeStato: TStringField;
          QAziendeIndirizzo: TStringField;
          QAziendeCap: TStringField;
          QAziendeComune: TStringField;
          QAziendeProvincia: TStringField;
          QAziendeIDAttivita: TIntegerField;
          QAziendeTelefono: TStringField;
          QAziendeFax: TStringField;
          QAziendePartitaIVA: TStringField;
          QAziendeCodiceFiscale: TStringField;
          QAziendeBancaAppoggio: TStringField;
          QAziendeSistemaPagamento: TStringField;
          QAziendeNoteContratto: TMemoField;
          QAziendeResponsabile: TStringField;
          QAziendeConosciutoInData: TDateTimeField;
          QAziendeIndirizzoLegale: TStringField;
          QAziendeCapLegale: TStringField;
          QAziendeComuneLegale: TStringField;
          QAziendeProvinciaLegale: TStringField;
          QAziendeCartellaDoc: TStringField;
          QAziendeTipoStrada: TStringField;
          QAziendeTipoStradaLegale: TStringField;
          QAziendeNumCivico: TStringField;
          QAziendeNumCivicoLegale: TStringField;
          QAziendeNazioneAbb: TStringField;
          QAziendeNazioneAbbLegale: TStringField;
          QAziendeNote: TMemoField;
          QAziendeFatturato: TFloatField;
          QAziendeNumDipendenti: TSmallintField;
          QAziendeBlocco1: TBooleanField;
          QAziendeEnteCertificatore: TStringField;
          QAziendeLibPrivacy: TBooleanField;
          QAziendeIdAzienda: TIntegerField;
          QAziendeSitoInternet: TStringField;
          QAziendeDescAttivitaAzienda: TStringField;
          QAziendeDallAnno: TIntegerField;
          QAziendeParentCompany: TStringField;
          QAziendeCCIAA: TStringField;
          QAziendeAttivita: TStringField;
          QOrgNew: TADOLinkedTable;
          QOrgNewID: TAutoIncField;
          QOrgNewIDLivello: TIntegerField;
          QOrgNewTipo: TStringField;
          QOrgNewDescrizione: TStringField;
          QOrgNewDescBreve: TStringField;
          QOrgNewDataAgg: TDateTimeField;
          QOrgNewTipoSubordine: TStringField;
          QOrgNewNodoPadre: TIntegerField;
          QOrgNewIDMansione: TIntegerField;
          QOrgNewIDDipendente: TIntegerField;
          QOrgNewPercentuale: TSmallintField;
          QOrgNewCentroCosto: TIntegerField;
          QOrgNewMonteOreRichiesto: TSmallintField;
          QOrgNewTassoAssenteismo: TSmallintField;
          QOrgNewInterim: TBooleanField;
          QOrgNewInStaff: TBooleanField;
          QOrgNewValutazGen: TFloatField;
          QOrgNewIndiceSost: TFloatField;
          QOrgNewIndiceImp: TFloatField;
          QOrgNewUltimoAgg: TDateTimeField;
          QOrgNewPARENT: TIntegerField;
          QOrgNewWIDTH: TIntegerField;
          QOrgNewHEIGHT: TIntegerField;
          QOrgNewTYPE: TStringField;
          QOrgNewCOLOR: TIntegerField;
          QOrgNewIMAGE: TIntegerField;
          QOrgNewIMAGEALIGN: TStringField;
          QOrgNewORDINE: TIntegerField;
          QOrgNewALIGN: TStringField;
          QOrgNewIDReparto: TIntegerField;
          QOrgNewIDCliente: TIntegerField;
          QOrgNewIDAzienda: TIntegerField;
          QOrgNewRALmin: TFloatField;
          QOrgNewRALmax: TFloatField;
          QOrgNewMinMesiAccesso: TIntegerField;
          QOrgNewMansione: TStringField;
          QOrgNewDicNodo: TStringField;
          QAnagLK: TADOLinkedQuery;
          QAnagLKID: TAutoIncField;
          QAnagLKCognome: TStringField;
          QAnagLKNome: TStringField;
          QOrgNewCognome: TStringField;
          QOrgNewNome: TStringField;
          QRepartiLK: TADOLinkedQuery;
          QRepartiLKID: TAutoIncField;
          QRepartiLKTipo: TStringField;
          QRepartiLKReparto: TStringField;
          QRepartiLKCodice: TStringField;
          QRepartiLKIDAzienda: TIntegerField;
          QOrgNewReparto: TStringField;
          QOrgNodoRuoloID: TAutoIncField;
          QOrgNodoRuoloCVNumero: TIntegerField;
          QOrgNodoRuoloCognome: TStringField;
          QOrgNodoRuoloNome: TStringField;
          QOrgNodoRuoloIDProprietaCV: TIntegerField;
          QCandRic_oldID: TIntegerField;
          QCandRic_oldIDRicerca: TIntegerField;
          QCandRic_oldCognome: TStringField;
          QCandRic_oldNome: TStringField;
          QCandRic_oldCVNumero: TIntegerField;
          QCandRic_oldIDStato: TIntegerField;
          QCandRic_oldCellulare: TStringField;
          QCandRic_oldTelUfficio: TStringField;
          QCandRic_oldDataNascita: TDateTimeField;
          QCandRic_oldRecapitiTelefonici: TStringField;
          QCandRic_oldIDAnagrafica: TIntegerField;
          QCandRic_oldDataImpegno: TDateTimeField;
          QCandRic_oldCodstato: TSmallintField;
          QCandRic_oldNote: TStringField;
          QCandRic_oldTipoStato: TStringField;
          QCandRic_oldDataIns: TDateTimeField;
          QCandRic_oldMiniVal: TStringField;
          QCandRic_oldDataUltimoContatto: TDateTimeField;
          QCandRic_oldIDTitoloStudioMin: TIntegerField;
          QCandRic_oldAzienda: TStringField;
          QCandRic_oldTelAzienda: TStringField;
          QTitoliStudioLKID: TAutoIncField;
          QTitoliStudioLKDescrizione: TStringField;
          QCandRic_oldDiploma: TStringField;
          QCandRic_oldEta: TStringField;
          QCandRic_oldTelUffCell: TStringField;
          TRicAnagIDXID: TAutoIncField;
          TRicAnagIDXIDRicerca: TIntegerField;
          TRicAnagIDXIDAnagrafica: TIntegerField;
          QRicAttiveID: TAutoIncField;
          QRicAttiveProgressivo: TStringField;
          QRicAttiveCliente: TStringField;
          QRicAttiveDataInizio: TDateTimeField;
          QRicAttiveNumRicercati: TSmallintField;
          QRicAttiveTitoloCliente: TStringField;
          QRicAttiveRuolo: TStringField;
          QRicAttiveSelezionatore: TStringField;
          QRicAttiveStato: TStringField;
          QRicAttiveConclusa: TBooleanField;
          QRicAttiveIDStatoRic: TIntegerField;
          QRicAttiveStatoRic: TStringField;
          QRicAttiveTipo: TStringField;
          QRicAttiveDataPrevChiusura: TDateTimeField;
          QRicAttiveIDOfferta: TIntegerField;
          QRicAttiveRifOfferta: TStringField;
          QRicAttiveTotCompdaFatt: TFloatField;
          QCommessaCL: TADOQuery;
          DsQCommessaCL: TDataSource;
          QCommessaCLID: TAutoIncField;
          QCommessaCLIDRicerca: TIntegerField;
          QCommessaCLIDVoceStandard: TIntegerField;
          QCommessaCLVoce: TStringField;
          QCLStat: TADOQuery;
          DsQCLStat: TDataSource;
          QCandRic_oldFlagAccPres: TBooleanField;
          TRicerchePendDataPresuntaPres: TDateTimeField;
          QTotCompFattLK: TADOLinkedQuery;
          QTotCompDaFattLKIDRicerca: TIntegerField;
          QTotCompDaFattLKImpDaFatt: TFloatField;
          QTotCompFattLKIDRicerca: TIntegerField;
          QTotCompFattLKImpFatturato: TFloatField;
          QRicAttiveTotCompFatturato: TFloatField;
          QRicAttiveDataFIne: TDateTimeField;
          qTotRimbSpeseLK: TADOQuery;
          QRicAttiveRimbSpese: TFloatField;
          qTotRimbSpeseLKIDRicerca: TIntegerField;
          qTotRimbSpeseLKRimbSpese: TFloatField;
          QCandRic_oldStato: TStringField;
          qCollaboratoriRicLK: TADOQuery;
          qCollaboratoriRicLKNominativo: TStringField;
          qCollaboratoriRicLKIDricerca: TIntegerField;
          QCandRic_oldVisibleCliente: TBooleanField;
          DsQStatiCliente: TDataSource;
          QStatiCliente: TADOLinkedQuery;
          QStatiClienteID: TAutoIncField;
          QStatiClienteDescrizione: TStringField;
          QCandRic_oldStatoCliente: TStringField;
          QCandRic_oldIDStatoCliente: TIntegerField;
          QCandRic_oldEscluso: TIntegerField;
          QCandRic_oldIDEvento: TIntegerField;
          qCandRic: TADOQuery;
          qCandRicID: TAutoIncField;
          qCandRicIDRicerca: TIntegerField;
          qCandRicCognome: TStringField;
          qCandRicNome: TStringField;
          qCandRicCVNumero: TIntegerField;
          qCandRicIDStato: TIntegerField;
          qCandRicCellulare: TStringField;
          qCandRicTelUfficio: TStringField;
          qCandRicDataNascita: TDateTimeField;
          qCandRicRecapitiTelefonici: TStringField;
          qCandRicIDAnagrafica: TIntegerField;
          qCandRicDataImpegno: TDateTimeField;
          qCandRicCodstato: TSmallintField;
          qCandRicStato: TStringField;
          qCandRicTipoStato: TStringField;
          qCandRicDataIns: TDateTimeField;
          qCandRicMiniVal: TStringField;
          qCandRicDataUltimoContatto: TDateTimeField;
          qCandRicFlagAccPres: TBooleanField;
          qCandRicIDStatoCliente: TIntegerField;
          qCandRicVisibleCliente: TBooleanField;
          qCandRicIDEvento: TIntegerField;
          qCandRicEta: TStringField;
          qCandRicTelUffCell: TStringField;
          qCandRicEscluso: TIntegerField;
          qCandRicColore: TIntegerField;
          QTLCandidatiEta: TIntegerField;
          QTLCandidatiDataNascita: TDateTimeField;
          QTLCandidatiID: TAutoIncField;
          QTLCandidatiIDAnagrafica: TAutoIncField;
          QTLCandidatiDescrizioneMansione: TMemoField;
          QTLCandidatiCVNumero: TIntegerField;
          QTLCandidatiCognome: TStringField;
          QTLCandidatiNome: TStringField;
          QTLCandidatiRuoloAttuale: TStringField;
          QTLCandidatiNote: TStringField;
          QTLCandidatiIDStato: TIntegerField;
          QTLCandidatiCellulare: TStringField;
          QTLCandidatiTelUfficio: TStringField;
          QTLCandidatiRecapitiTelefonici: TStringField;
          QAnnunciCand_oldIDAnnuncio: TAutoIncField;
          QAnnunciCand_oldRif: TStringField;
          QAnnunciCand_oldTestata: TStringField;
          QAnnunciCand_oldNomeEdizione: TStringField;
          QAnnunciCand_oldIDAnnEdizData: TAutoIncField;
          QAnnunciCand_oldVisionato: TBooleanField;
          QAnnunciCand_oldData: TDateTimeField;
          QAnnunciCand_oldCVNumero: TIntegerField;
          QAnnunciCand_oldCognome: TStringField;
          QAnnunciCand_oldNome: TStringField;
          QAnnunciCand_oldCVInseritoIndata: TDateTimeField;
          QAnnunciCand_oldIDAnagrafica: TAutoIncField;
          QAnnunciCand_oldIDProprietaCV: TIntegerField;
          QAnnunciCand_oldIDCandRic: TAutoIncField;
          QAnnunciCand_oldDataPervenuto: TDateTimeField;
          qCandRicTipo: TStringField;
          qCandRicInquadramento: TStringField;
          qCandRicRuolo: TStringField;
          qAnnunciCand: TADOQuery;
          qCandRicRetribTotale: TStringField;
          QRicAttiveSede: TStringField;
          qCandRicRetribuzione: TStringField;
          qCandRicRetribVariabile: TStringField;
          qCandRicNoteCliente: TStringField;
          qCandRicBloccato: TBooleanField;
          qCandRicNote: TStringField;
          QRicAttiveSupporto: TStringField;
          TRicerchePendInBudget: TBooleanField;
          TRicerchePendStatoSecondario: TStringField;
          TRicerchePendIDStatoSecondario: TIntegerField;
          TRicerchePendNoteStatoSec: TMemoField;
          QRicAttiveStatoSecondario: TStringField;
          TRicerchePendIDIndirizzoCliente: TIntegerField;
          TRicerchePendIndProv: TStringField;
          TRicerchePendDescIndirizzo: TStringField;
          QOrgNewIDSoftware: TStringField;
          QCkRegoleClienti: TADOQuery;
          QAnagNoteRic: TADOQuery;
          QAnagNoteRicID: TAutoIncField;
          QAnagNoteRicNote: TStringField;
          QAnagNoteRicProgressivo: TStringField;
          QAnagNoteRicCliente: TStringField;
          QAnagNoteRicIDAnagrafica: TIntegerField;
          QRicUtenti: TADOQuery;
          QTLCandidatiIDEspLav: TAutoIncField;
          DsQValutazCand: TDataSource;
          QValutazCand: TADOQuery;
          QValutazCandID: TAutoIncField;
          QValutazCandCandidato: TStringField;
          QValutazCandVoce: TStringField;
          QValutazCandLegenda: TStringField;
          QValutazCandSelezionatore: TStringField;
          QValutazCandValore: TSmallintField;
          QValutazCandNote: TMemoField;
          QRicAttiveArea: TStringField;
          QInsertDist: TADOQuery;
          QDistanza: TADOQuery;
          qCandRicDomicilioComune: TStringField;
          qCandRicdomicilioprovincia: TStringField;
          qCandRicdomicilioindirizzo: TStringField;
          qCandRicdomiciliotipostrada: TStringField;
          qCandRicdomiciliocap: TStringField;
          qCandRicdomicilionumcivico: TStringField;
          qCandRicProvincia: TStringField;
          qCandRicComune: TStringField;
          qCandRicindirizzo: TStringField;
          qCandRiccap: TStringField;
          qCandRicnumcivico: TStringField;
          qCandRictipostrada: TStringField;
          qStatiLK: TADOQuery;
          qStatiLKID: TAutoIncField;
          qStatiLKStato: TStringField;
          qStatiLKIDTipoStato: TIntegerField;
          qStatiLKPRman: TBooleanField;
          qStatiLKIDAzienda: TIntegerField;
          TRicerchePendIDTipologiaContratto: TIntegerField;
          TRicerchePendTipologiaContratto: TStringField;
          QRicQuest: TADOQuery;
          DsQRicQuest: TDataSource;
          QRicQuestID: TAutoIncField;
          QRicQuestIDRicerca: TIntegerField;
          QRicQuestIDQuest: TIntegerField;
          QRicQuestNote: TMemoField;
          QQuestLK: TADOQuery;
          QQuestLKID: TAutoIncField;
          QQuestLKQuestionario: TStringField;
          QRicQuestQuestionario: TStringField;
          QRicQuestTipologia: TStringField;
          QRicQuestGruppoQuestionario: TStringField;
          QRicQuestCodice: TStringField;
          QRicQuest_Anag: TADOQuery;
          DsQRicQuest_Anag: TDataSource;
          QRicQuest_AnagID: TAutoIncField;
          QRicQuest_AnagCandidato: TStringField;
          QRicQuest_AnagDataOraInvio: TDateTimeField;
          QRicQuest_AnagIDAnagQuest: TAutoIncField;
          QRicQuest_AnagDataOraInizio: TDateTimeField;
          QRicQuest_AnagDataOraFIne: TDateTimeField;
          QRicQuest_AnagEmail: TStringField;
          QRicQuest_AnagIDAnagrafica: TAutoIncField;
          QRicQuest_AnagGUID: TStringField;
          QRicQuest_AnagIDAnagInvio: TAutoIncField;
          QRicQuest_AnagTotPunteggio: TFloatField;
          QRicQuest_Risp: TADOQuery;
          DsQRicQuest_Risp: TDataSource;
          QRicQuest_RispID: TAutoIncField;
          QRicQuest_RispCandidato: TStringField;
          QRicQuest_RispRaggruppamento: TStringField;
          QRicQuest_RispTotPunti: TFloatField;
          qCandRicVisionato: TBooleanField;
          qCandRicdistanza: TStringField;
          qCandRictempo: TStringField;
          qCandRicDataPrevRichiamo: TDateTimeField;
          qCandRicDescMotivoStatoSosp: TStringField;
          qCandRicDataFineStatoSOSPESO: TDateTimeField;
          qCandRicDescMotivoRichiamoCandidato: TStringField;
          qCandRicStatoCand: TStringField;
          qCandRicCVInseritoInData: TDateTimeField;
          qCandRicDomicilioStato: TStringField;
          qCandRicProprietaCV: TStringField;
          TStoricoRic: TADOQuery;
          TStoricoRicID: TAutoIncField;
          TStoricoRicIDRicerca: TIntegerField;
          TStoricoRicIDStatoRic: TIntegerField;
          TStoricoRicDallaData: TDateTimeField;
          TStoricoRicNote: TStringField;
          TStoricoRicTotGiorni: TSmallintField;
          TStoricoRicIDEventoRic: TIntegerField;
          TStoricoRicIDStatoSecondario: TIntegerField;
          TStoricoRicNoteStatoSec: TMemoField;
          TStoricoRicID_1: TAutoIncField;
          TStoricoRicStatoRic: TStringField;
          TStoricoRicIdAzienda: TIntegerField;
          TStoricoRicIDTipoStatoRic: TIntegerField;
          TStoricoRicCheckSgancio: TBooleanField;
          TStoricoRicID_2: TAutoIncField;
          TStoricoRicEventoRic: TStringField;
          TStoricoRicID_3: TAutoIncField;
          TStoricoRicStatoSecondario: TStringField;
          qCandRicIDIndicazione: TIntegerField;
          qCandRicAzienda: TStringField;
          qCandRicSettoreAz: TStringField;
          qCandRicTelAzienda: TStringField;
          qCandRicDiploma: TStringField;
          qCandRicJobTitle: TStringField;
          QRicTargetListID: TAutoIncField;
          QRicTargetListIDRicerca: TIntegerField;
          QRicTargetListIDCliente: TIntegerField;
          QRicTargetListNote: TMemoField;
          QRicTargetListAzienda: TStringField;
          QRicTargetListAttivita: TStringField;
          QRicTargetListIDAttivita: TIntegerField;
          QRicTargetListtelefono: TStringField;
          QRicTargetListComune: TStringField;
          QRicTargetListProvincia: TStringField;
          QRicTargetListDataUltimaEsploraz: TDateTimeField;
          qCandRicStatoCliente: TStringField;
          qCandRicDomicilioRegione: TStringField;
          qCandRicRegione: TStringField;
          qCandRicEmail: TStringField;
          qAnnunciCandIDAnnuncio: TAutoIncField;
          qAnnunciCandRif: TStringField;
          qAnnunciCandTestata: TStringField;
          qAnnunciCandIDAnagAnnEdizData: TAutoIncField;
          qAnnunciCandNomeEdizione: TStringField;
          qAnnunciCandIDAnnEdizData: TAutoIncField;
          qAnnunciCandVisionato: TBooleanField;
          qAnnunciCandData: TDateTimeField;
          qAnnunciCandCVNumero: TIntegerField;
          qAnnunciCandCognome: TStringField;
          qAnnunciCandNome: TStringField;
          qAnnunciCandDataNascita: TDateTimeField;
          qAnnunciCandCVInseritoIndata: TDateTimeField;
          qAnnunciCandIDAnagrafica: TAutoIncField;
          qAnnunciCandIDProprietaCV: TIntegerField;
          qAnnunciCandIDCandRic: TAutoIncField;
          qAnnunciCandDataPervenuto: TDateTimeField;
          qAnnunciCandPesoRaggiunto: TFloatField;
          qAnnunciCandIDStato: TIntegerField;
          qAnnunciCandIDAnagQuest: TAutoIncField;
          qAnnunciCandTotPunteggio: TFloatField;
          qAnnunciCandProvincia: TStringField;
          qAnnunciCandDomicilioProvincia: TStringField;
          qAnnunciCandRetribuzione: TStringField;
          qAnnunciCandAziendaAtt: TStringField;
          qAnnunciCandRuoloAtt: TStringField;
          qAnnunciCandJobTitleEspLAv: TStringField;
          qAnnunciCandJobTitle: TStringField;
          QStatiClientiAnnunci: TADOQuery;
          DSQStatiClientiAnnunci: TDataSource;
          qAnnunciCandIDStatoCliente: TIntegerField;
          qAnnunciCandStatoCliente: TStringField;
          qAnnunciCandEta: TIntegerField;
          QStatiClientiAnnunciID: TAutoIncField;
          QStatiClientiAnnunciDescrizione: TStringField;
          QRicAttiveAttivitaPrincipale: TStringField;
          QCandAziendaAree: TADOQuery;
          QCandAziendaMansioni: TADOQuery;
          qCandRicClassificazione: TStringField;
          qCandRicResidenzaStato: TStringField;
          qCandRicMansioneCand: TStringField;
          qCandRicAreaCand: TStringField;
          qAnnunciCandStato: TStringField;
          QAttivita: TADOQuery;
          DsQAttivita: TDataSource;
          QAttivitaID: TAutoIncField;
          QAttivitaIDRicerca: TIntegerField;
          QAttivitaData: TDateTimeField;
          QAttivitaIDEvento: TIntegerField;
          QAttivitaMinuti: TSmallintField;
          QAttivitaCosto: TFloatField;
          QAttivitaIDAnagrafica: TIntegerField;
          QAttivitaIDUtente: TIntegerField;
          QAttivitaNote: TMemoField;
          QTabEventiAttCommLK: TADOQuery;
          QTabEventiAttCommLKID: TAutoIncField;
          QTabEventiAttCommLKEvento: TStringField;
          QAttivitaEvento: TStringField;
          QCandidatiLK: TADOQuery;
          QCandidatiLKID: TAutoIncField;
          QCandidatiLKCandidato: TStringField;
          QAttivitaCandidato: TStringField;
          QUtentiRicLK: TADOQuery;
          QUtentiRicLKID: TAutoIncField;
          QUtentiRicLKUtente: TStringField;
          QAttivitaUtente: TStringField;
          QAttivitaIDFattura: TIntegerField;
          QFattLK: TADOQuery;
          QFattLKProgressivo: TStringField;
          QFattLKID: TAutoIncField;
          QAttivitaProgFatt: TStringField;
    QStatCommesse: TADOQuery;
    QStatCommesseTotCand: TIntegerField;
    QStatCommesseTotEsclusi: TIntegerField;
    QStatCommesseTotAnnunci: TIntegerField;
    QStatCommesseTotColloqui: TIntegerField;
    QStatCommesseTotPresentati: TIntegerField;
    QStatCommesseTotAssunti: TIntegerField;
    DsQStatCommesse: TDataSource;
    QStatCommessa: TADOQuery;
    DsQStatCommessa: TDataSource;
    QStatCommesseTot: TIntegerField;
    QStatCommessaTot: TIntegerField;
    QStatCommessaTotCand: TIntegerField;
    QStatCommessaTotEsclusi: TIntegerField;
    QStatCommessaTotAnnunci: TIntegerField;
    QStatCommessaTotColloqui: TIntegerField;
    QStatCommessaTotPresentati: TIntegerField;
    QStatCommessaTotAssunti: TIntegerField;
    QRicAttiveAreaSettore: TStringField;
    qCandRicAreaSettore: TStringField;
    QCommittentiCommesse: TADOQuery;
    DSCommittentiCommesse: TDataSource;
    QCommittentiCommesseID: TAutoIncField;
    QCommittentiCommesseIDRicerca: TIntegerField;
    QCommittentiCommesseIDContattoCli: TIntegerField;
    QCommittentiCommesseIDAnagrafica: TIntegerField;
    QCommittentiCommesseNominativo: TStringField;
    QTemp2: TADOQuery;
          procedure DataRicercheCreate(Sender: TObject);
          procedure TCandRicAfterPost(DataSet: TDataSet);
          procedure TMansDipAfterScroll(DataSet: TDataSet);
          procedure QRicAttive_OLDAfterOpen(DataSet: TDataSet);
          procedure QOrgNew_OLDAfterClose(DataSet: TDataSet);
          procedure QOrgNew_OLDBeforeOpen(DataSet: TDataSet);
          procedure DsQOrgNewStateChange(Sender: TObject);
          procedure QOrgNew_OLDCalcFields(DataSet: TDataSet);
          procedure QOrgNew_OLDAfterInsert(DataSet: TDataSet);
          procedure QOrgNew_OLDAfterScroll(DataSet: TDataSet);
          procedure QRicAttive_OLDBeforeOpen(DataSet: TDataSet);
          procedure QRicAttive_OLDAfterClose(DataSet: TDataSet);
          procedure QOrgNew_OLDAfterOpen(DataSet: TDataSet);
          procedure QOrgNew_OLDBeforeClose(DataSet: TDataSet);
          procedure QCandRic_oldCalcFields(DataSet: TDataSet);
          procedure TRicerchePendBeforeEdit(DataSet: TDataSet);
          procedure TRicerchePendAfterPost(DataSet: TDataSet);
          procedure QCandRic_oldAfterPost(DataSet: TDataSet);
          procedure QOrgNodoRuolo_OLDBeforeOpen(DataSet: TDataSet);
          procedure QRicTargetList_OLDAfterPost(DataSet: TDataSet);
          procedure QRicTargetList_OLDAfterOpen(DataSet: TDataSet);
          procedure QRicTargetList_OLDAfterClose(DataSet: TDataSet);
          procedure QTLCandidati_OLDCalcFields(DataSet: TDataSet);
          procedure QCandRic_oldAfterClose(DataSet: TDataSet);
          procedure QCandRic_oldBeforeOpen(DataSet: TDataSet);
          procedure QLastEspLav_OLDBeforeOpen(DataSet: TDataSet);
          procedure QLastEspLav_OLDAfterClose(DataSet: TDataSet);
          procedure QRicTimeSheet_OLDAfterPost(DataSet: TDataSet);
          procedure QRicTimeSheet_OLDNewRecord(DataSet: TDataSet);
          procedure DsQRicTimeSheetStateChange(Sender: TObject);
          procedure QRicTimeSheet_OLDAfterInsert(DataSet: TDataSet);
          procedure QRicTimeSheet_OLDBeforeOpen(DataSet: TDataSet);
          procedure QRicTimeSheet_OLDAfterClose(DataSet: TDataSet);
          procedure QRicTimeSheet_OLDAfterDelete(DataSet: TDataSet);
          procedure QRicTimeSheet_OLDBeforeDelete(DataSet: TDataSet);
          procedure QRicTimeSheet_OLDBeforePost(DataSet: TDataSet);
          procedure DsQTimeRepDettStateChange(Sender: TObject);
          procedure QTimeRepDett_OLDBeforeOpen(DataSet: TDataSet);
          procedure QTimeRepDett_OLDAfterClose(DataSet: TDataSet);
          procedure QTimeRepDett_OLDAfterPost(DataSet: TDataSet);
          procedure QTimeRepDett_OLDAfterInsert(DataSet: TDataSet);
          procedure QUsersLKBeforeOpen(DataSet: TDataSet);
          procedure QOrgNewAfterClose(DataSet: TDataSet);
          procedure QTLCandidatiBeforeOpen(DataSet: TDataSet);
          procedure QRicTargetListAfterScroll(DataSet: TDataSet);
          procedure QRicTargetListBeforeClose(DataSet: TDataSet);
          procedure QOrgNodoRuoloAfterOpen(DataSet: TDataSet);
          procedure DsQCommessaCLStateChange(Sender: TObject);
          procedure QCommessaCLAfterInsert(DataSet: TDataSet);
          procedure QCommessaCLBeforeClose(DataSet: TDataSet);
          procedure QCommessaCLBeforeOpen(DataSet: TDataSet);
          procedure QCLStatBeforeOpen(DataSet: TDataSet);
          procedure qCandRicAfterClose(DataSet: TDataSet);
          procedure qCandRicCalcFields(DataSet: TDataSet);
          procedure qCandRicBeforeEdit(DataSet: TDataSet);
          procedure qCandRicBeforePost(DataSet: TDataSet);
          procedure qCandRicAfterPost(DataSet: TDataSet);
          procedure QRicAttiveAfterScroll(DataSet: TDataSet);
          procedure qAnnunciCandCalcFields(DataSet: TDataSet);
          procedure QRicUtentiBeforeOpen(DataSet: TDataSet);
          procedure QValutazCandAfterOpen(DataSet: TDataSet);
          procedure DsQValutazCandStateChange(Sender: TObject);
          procedure QValutazCandBeforeOpen(DataSet: TDataSet);
          procedure qCandRicAfterOpen(DataSet: TDataSet);
          procedure QLastEspLavAfterOpen(DataSet: TDataSet);
          procedure qCandRicBeforeOpen(DataSet: TDataSet);
          procedure DsQAttivitaStateChange(Sender: TObject);
          procedure QAttivitaBeforeOpen(DataSet: TDataSet);
          procedure QCandidatiLKBeforeOpen(DataSet: TDataSet);
          procedure QUtentiRicLKBeforeOpen(DataSet: TDataSet);
          procedure QAttivitaAfterInsert(DataSet: TDataSet);
          procedure QAttivitaBeforeClose(DataSet: TDataSet);
    procedure QStatCommesseBeforeOpen(DataSet: TDataSet);
    procedure QStatCommesseCalcFields(DataSet: TDataSet);
    procedure QStatCommessaCalcFields(DataSet: TDataSet);
    procedure QCommittentiCommesseBeforeOpen(DataSet: TDataSet);
     private
          { Private declarations }
          xVecchioVal, xIDTimeSheet: integer;
          xVecchiadataInizio: TDateTime;
     public
          { Public declarations }
          xValutazDipForm: boolean;
     end;

var
     DataRicerche: TDataRicerche;

implementation

uses ModuloDati, SelArea, ValutazDip, Main, SelPers, ModuloDati2,
     uUtilsVarie, CheckPass;

{$R *.DFM}


procedure TDataRicerche.DataRicercheCreate(Sender: TObject);
begin
     xValutazDipForm := False;
end;

procedure TDataRicerche.TCandRicAfterPost(DataSet: TDataSet);
begin
     if QCandRic.Active then begin
          QCandRic.Close;
          QCandRic.Open;
     end;
     QCandTutte.Close;
     QCandTutte.Open;
end;

procedure TDataRicerche.TMansDipAfterScroll(DataSet: TDataSet);
begin
     if xValutazDipForm then begin
          ValutazDipForm.DBChart3.RefreshData;
          ValutazDipForm.DBChart4.RefreshData;
     end;
end;

procedure TDataRicerche.QRicAttive_OLDAfterOpen(DataSet: TDataSet);
begin
     MainForm.LTotRic.Caption := IntToStr(QRicAttive.RecordCount);
     if data.global.fieldbyname('DebugHrms').asBoolean = true then
          QRicAttive.SQL.SaveToFile('QRicAttive.sql');
end;

//[/TONI20020724\]

procedure TDataRicerche.QOrgNew_OLDAfterClose(DataSet: TDataSet);
begin
     QMansioniLK.Close;
end;

procedure TDataRicerche.QOrgNew_OLDBeforeOpen(DataSet: TDataSet);
begin
     //QOrgNew.PArambyName['xIDCliente']:=QAziendeID.Value;
     QOrgNew.Filter := 'IDCliente=' + QAziendeID.AsString + ' and IDSoftware=1 ';
     QOrgNew.Filtered := true;
     QMansioniLK.Open;
end;

procedure TDataRicerche.DsQOrgNewStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsQOrgNew.State in [dsEdit, dsInsert];
     MainForm.TbBOrganigMod.Enabled := not b;
     MainForm.PanOrgDett.Enabled := b;
     MainForm.TbBOrganigOK.Enabled := b;
     MainForm.TbBOrganigCan.Enabled := b;
end;

//[/TONI20020724\]

procedure TDataRicerche.QOrgNew_OLDCalcFields(DataSet: TDataSet);
begin
     {    QOrgNewDicNodo.value:=QOrgNewReparto.Value+chr(13)+
               QOrgNewDescBreve.value+chr(13)+
               copy(QOrgNewCognome.Value,1,1)+
               LowerCase(copy(QOrgNewCognome.Value,2,Length(QOrgNewCognome.Value)-1))+' '+
               copy(QOrgNewNome.value,1,1)+'.';}

     QOrgNew.FieldByName('DicNodo').AsString := QOrgNew.FieldByName('Reparto').AsString + chr(13) +
          QOrgNew.FieldByName('DescBreve').AsString + chr(13) +
          copy(QOrgNew.FieldByName('Cognome').AsString, 1, 1) +
          LowerCase(copy(QOrgNew.FieldByName('Cognome').AsString, 2, Length(QOrgNew.FieldByName('Cognome').AsString) - 1)) + ' ' +
          copy(QOrgNew.FieldByName('Nome').AsString, 1, 1) + '.';

     //     QOrgNew.FieldByName('DicNodo').AsString:=QOrgNew.FieldByName('DescBreve').AsString+chr(13);
end;

procedure TDataRicerche.QOrgNew_OLDAfterInsert(DataSet: TDataSet);
begin
     {     with QOrgNew,MainForm.DBTree do begin
               QOrgNewInterim.Value:=False;
               QOrgNewInStaff.value:=False;
               QOrgNewIDCliente.value:=QAziendeID.Value;
               FindField('Height').AsInteger:=DefaultNodeHeight;
               FindField('Width').AsInteger:=DefaultNodeWidth;
               FindField('Type').AsString:='Rectangle';
               FindField('Color').AsInteger:=clWhite;
               FindField('Image').AsInteger:=-1;
               FindField('ImageAlign').AsString:='Left-Top';
               FindField('Align').AsString:='Center';
          end;}
     with QOrgNew, MainForm.DBTree do begin
          FieldByName('Interim').AsBoolean := False;
          FieldByName('InStaff').AsBoolean := False;
          FieldByName('IDCliente').AsInteger := QAziende.FieldByName('ID').AsInteger;
          FindField('Height').AsInteger := DefaultNodeHeight;
          FindField('Width').AsInteger := DefaultNodeWidth;
          FindField('Type').AsString := 'Rectangle';
          FindField('Color').AsInteger := clWhite;
          FindField('Image').AsInteger := -1;
          FindField('ImageAlign').AsString := 'Left-Top';
          FindField('Align').AsString := 'Center';
          FindField('IDSoftware').AsInteger := 1;
     end;

end;

procedure TDataRicerche.QOrgNew_OLDAfterScroll(DataSet: TDataSet);
begin
     {     MainForm.PColor.Color:=QOrgNewCOLOR.Value;
          if MainForm.PCAziendaOrg.activepage=MainForm.TSAziendaOrgRuolo then begin
               QOrgNodoRuolo.Close;
               QOrgNodoRuolo.Open;
          end;}

     MainForm.PColor.Color := QOrgNew.FieldByName('COLOR').Value;
     if MainForm.PCAziendaOrg.activepage = MainForm.TSAziendaOrgRuolo then begin
          QOrgNodoRuolo.Close;
          QOrgNodoRuolo.Open;
     end;
     if QOrgNodoRuolo.active then begin
          QOrgNodoRuolo.Close;
          QOrgNodoRuolo.ParamByName['xIDMansione'] := QOrgNewIDMansione.Value;
          QOrgNodoRuolo.Open;
     end;

               // Campi personalizzati
     if MainForm.PCAziendaOrg.ActivePage = MainForm.TSOrgcampPers then begin
          MainForm.CampiPersFrame3.TCampiPers.close;
          MainForm.CampiPersFrame3.CaricaValori;
          MainForm.CampiPersFrame3.ImpostaCampi;
     end;
end;
//[/TONI20020724\]FINE

procedure TDataRicerche.QRicAttive_OLDBeforeOpen(DataSet: TDataSet);
begin
     //QTotCandRic.Open;
     //QTotCompDaFattLK.Open;
end;

procedure TDataRicerche.QRicAttive_OLDAfterClose(DataSet: TDataSet);
begin
     QTotCandRic.Close;
     QTotCompDaFattLK.Close;
end;

procedure TDataRicerche.QOrgNew_OLDAfterOpen(DataSet: TDataSet);
begin
     MainForm.PCAziendaOrg.Enabled := True;
end;

procedure TDataRicerche.QOrgNew_OLDBeforeClose(DataSet: TDataSet);
begin
     MainForm.PCAziendaOrg.Enabled := False;
end;

//[/TONI20020724\]

procedure TDataRicerche.QCandRic_oldCalcFields(DataSet: TDataSet);
var xAnno, xMese, xgiorno: Word;
begin
     if QcandRicDataNascita.asString = '' then
          QcandRicEta.asString := ''
     else begin
          DecodeDate((Date - QcandRicDataNascita.Value), xAnno, xMese, xgiorno);
          // TAnagraficaEta.Value:=StrToInt(copy(xAnno,3,2));
          QcandRicEta.Value := copy(IntToStr(xAnno), 3, 2);
     end;
     QcandRicTelUffCell.Value := QcandRicCellulare.Value + ' ' + QcandRicTelUfficio.Value;
     {if QcandRic.FieldByName('DataNascita').asString='' then
          QcandRic.FIeldByName('Eta').asString:=''
     else begin
          DecodeDate((Date-QcandRic.FieldByName('DataNascita').Value),xAnno,xMese,xgiorno);
          // TAnagraficaEta.Value:=StrToInt(copy(xAnno,3,2));
          QcandRic.FieldByName('Eta').Value:=copy(IntToStr(xAnno),3,2);
     end;
     QcandRic.FieldByName('TelUffCell').Value:=QcandRic.FieldByName('Cellulare').AsString+' '+QcandRic.FieldByname('TelUfficio').AsString;}
end;

procedure TDataRicerche.TRicerchePendBeforeEdit(DataSet: TDataSet);
begin
     //     xVecchiadataInizio:=TRicerchePendDataInizio.Value;
     xVecchiadataInizio := TRicerchePend.FieldByName('DataInizio').asDateTime;
end;

//[TONI20021002]DEBUGOK

procedure TDataRicerche.TRicerchePendAfterPost(DataSet: TDataSet);
begin
     {     TRicerchePend.ApplyUpdates;
          TRicerchePend.CommitUpdates;}
     //     TRicerchePend.Post;
end;

//[TONI20021002]DEBUGOK

procedure TDataRicerche.QCandRic_oldAfterPost(DataSet: TDataSet);
begin
     {     QCandRic.ApplyUpdates;
          QCandRic.CommitUpdates;}
     //     QCandRic.Post;
end;

procedure TDataRicerche.QOrgNodoRuolo_OLDBeforeOpen(DataSet: TDataSet);
begin {     if (QOrgNewIDMansione.asString='')or(QOrgNewIDMansione.value=0) then
     MainForm.LTotOrgRuolo.caption:='0'
else begin
     // conta soggetti
     QTemp.Close;
     QTemp.SQL.text:='select count(AnagMansioni.ID) Tot from AnagMansioni '+
          'where AnagMansioni.IDMansione='+QOrgNewIDMansione.asString;
     QTemp.Open;
     MainForm.LTotOrgRuolo.caption:=IntToStr(QTemp.FieldByName('Tot').asInteger);
     //if QTemp.FieldByName('Tot').asInteger>100 then begin
     //     if MessageDlg('ATTENZIONE:  il numero di soggetti supera i 100. '+chr(13)+
     //          'Vuoi visualizzare lo stesso la griglia ?',mtWarning, [mbYes,mbNo],0)=mrNo then Abort;
     //end;
end;}
     QORgNodoRuolo.ReloadSQL;
     QOrgNodoRuolo.ParamByName['xIDMansione'] := QOrgNewIDMansione.Value;
     if (QOrgNew.FieldByName('IDMansione').asString = '') or (QOrgNew.FieldByName('IDMansione').AsInteger = 0) then
          MainForm.LTotOrgRuolo.caption := '0'
     else
     begin
          // conta soggetti
          QTemp.Close;
          QTemp.SQL.text := 'select count(AnagMansioni.ID) Tot from AnagMansioni ' +
               'where AnagMansioni.IDMansione=' + QOrgNew.FieldByName('IDMansione').asString;
          QTemp.Open;
          MainForm.LTotOrgRuolo.caption := IntToStr(QTemp.FieldByName('Tot').asInteger);
     end;
end;

procedure TDataRicerche.QRicTargetList_OLDAfterPost(DataSet: TDataSet);
begin
     {     with QRicTargetList do begin
               Data.DB.BeginTrans;
               try
                    ApplyUpdates;
                    Data.DB.CommitTrans;
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
                    raise;
               end;
               CommitUpdates;
          end;}
end;
//[TONI20020724]FINE

procedure TDataRicerche.QRicTargetList_OLDAfterOpen(DataSet: TDataSet);
begin
     if not QRicTargetList.IsEmpty then begin
          QTLCandidati.Close;
          //QTLCandidati.ParamByName['xIDTargetList'] := QRicTargetListID.Value;
          //QTLCandidati.ParamByName['xIDAzienda'] := QRicTargetListIDCliente.Value;
          QTLCandidati.Open;
     end;
end;

procedure TDataRicerche.QRicTargetList_OLDAfterClose(DataSet: TDataSet);
begin
     QTLCandidati.Close;
end;

//[TONI20020724]

procedure TDataRicerche.QTLCandidati_OLDCalcFields(DataSet: TDataSet);
var xAnno, xMese, xgiorno: Word;
begin
     {     if QTLCandidatiDataNascita.asString='' then
               QTLCandidatiEta.asString:=''
          else begin
               DecodeDate((Date-QTLCandidatiDataNascita.Value),xAnno,xMese,xgiorno);
               // TAnagraficaEta.Value:=StrToInt(copy(xAnno,3,2));
               QTLCandidatiEta.Value:=copy(IntToStr(xAnno),3,2);
          end;}
     if QTLCandidati.FieldByName('DataNascita').asString = '' then
          QTLCandidati.FieldByName('Eta').asString := ''
     else begin
          DecodeDate((Date - QTLCandidati.FieldByName('DataNascita').Value), xAnno, xMese, xgiorno);
          // TAnagraficaEta.Value:=StrToInt(copy(xAnno,3,2));
          QTLCandidati.FieldByName('Eta').Value := copy(IntToStr(xAnno), 3, 2);
     end;
end;
//[TONI20020724]FINE

procedure TDataRicerche.QCandRic_oldAfterClose(DataSet: TDataSet);
begin
     //QLastEspLav.Close;
     //QTitoliStudioLK.Close;
end;

procedure TDataRicerche.QCandRic_oldBeforeOpen(DataSet: TDataSet);
begin
     //QLastEspLav.Open;
     //QTitoliStudioLK.Open;
end;

procedure TDataRicerche.QLastEspLav_OLDBeforeOpen(DataSet: TDataSet);
begin
     //QEspLavLK.Open;
end;

procedure TDataRicerche.QLastEspLav_OLDAfterClose(DataSet: TDataSet);
begin
     //QEspLavLK.Close;
end;

procedure TDataRicerche.QRicTimeSheet_OLDAfterPost(DataSet: TDataSet);
var xID: integer;
begin
     with QRicTimeSheet do begin
          Data.DB.BeginTrans;
          try
               // se c'� il controllo di gestione, allora inserisci o aggiorna time report
               if copy(Data.Global.FieldByName('CheckBoxIndici').Value, 6, 1) = '1' then begin
                    if TRicerchePend.FieldByName('IDContrattoCG').AsString <> '' then begin
                         Data.QTemp.Close;
                         Data.QTemp.SQL.text := 'select ID from CG_ContrattiTimeSheet ' +
                              'where IDTimeRepH1Sel=' + QRicTimeSheet.FieldByName('ID').AsString;
                         Data.QTemp.Open;
                         if Data.QTemp.IsEmpty then begin
                              // ID questo record inserito
                              QTemp.Close;
                              QTemp.SQL.text := 'select @@IDENTITY as LastID';
                              QTemp.Open;
                              xID := QTemp.FieldByName('LastID').asInteger;
                              QTemp.Close;
                              Data.Q1.Close;
                              Data.Q1.SQL.text := 'insert into CG_ContrattiTimeSheet (IDContratto, IDUtente, Data, OreConsuntivo, Note, IDGruppoProfess, IDTimeRepH1Sel) ' +
                                   'values (:xIDContratto:, :xIDUtente:, :xData:, :xOreConsuntivo:, :xNote:, :xIDGruppoProfess:, :xIDTimeRepH1Sel:)';
                              Data.Q1.ParamByName['xIDContratto'] := TRicerchePend.FieldByname('IDContrattoCG').Asinteger;
                              Data.Q1.ParamByName['xIDUtente'] := QRicTimeSheet.FieldByName('IDUtente').Asinteger;
                              Data.Q1.ParamByName['xData'] := QRicTimeSheet.FieldByName('Data').Value;
                              Data.Q1.ParamByName['xOreConsuntivo'] := QRicTimeSheet.FieldByName('OreConsuntivo').AsFloat;
                              Data.Q1.ParamByName['xNote'] := QRicTimeSheet.FieldByName('Note').AsString;
                              Data.Q1.ParamByName['xIDGruppoProfess'] := QRicTimeSheet.FieldByName('IDGruppoProfess').Asinteger;
                              Data.Q1.ParamByName['xIDTimeRepH1Sel'] := xID;
                              Data.Q1.ExecSQL;
                         end else begin
                              Data.Q1.Close;
                              Data.Q1.SQL.text := 'update CG_ContrattiTimeSheet set IDUtente=:xIDUtente:, Data=:xData:,' +
                                   ' OreConsuntivo=:xOreConsuntivo:, Note=:xNote:, IDGruppoProfess=:xIDGruppoProfess: ' +
                                   'where ID=:xID:';
                              Data.Q1.ParamByName['xIDUtente'] := QRicTimeSheet.FieldByName('IDUtente').AsINteger;
                              Data.Q1.ParamByName['xData'] := QRicTimeSheet.FieldByName('Data').Value;
                              Data.Q1.ParamByName['xOreConsuntivo'] := QRicTimeSheet.FieldByName('OreConsuntivo').AsFloat;
                              Data.Q1.ParamByName['xNote'] := QRicTimeSheet.FieldByName('Note').AsString;
                              Data.Q1.ParamByName['xIDGruppoProfess'] := QRicTimeSheet.FieldByName('IDGruppoProfess').Asinteger;
                              Data.Q1.ParamByName['xID'] := Data.QTemp.FieldByName('ID').asInteger;
                              Data.Q1.ExecSQL;
                         end;
                         Data.QTemp.Close;
                    end;
               end;
               Data.DB.CommitTrans;
               xID := FieldByname('ID').Value;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               raise;
          end;
     end;
end;

procedure TDataRicerche.QRicTimeSheet_OLDNewRecord(DataSet: TDataSet);
begin
     QRicTimeSheet.FieldByName('IDUtente').Value := MainForm.xIDUtenteAttuale;
     QRicTimeSheet.FieldByName('Data').Value := Date;
     QUsersLK.locate('ID', MainForm.xIDUtenteAttuale, []);
     QRicTimeSheet.FieldByName('IDGruppoProfess').Value := QUsersLK.FieldByName('IDGruppoProfess').Value;
end;

procedure TDataRicerche.DsQRicTimeSheetStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsQRicTimeSheet.State in [dsEdit, dsInsert];
     SelPersForm.BTimeSheetNew.Enabled := not b;
     SelPersForm.BTimeSheetDel.Enabled := not b;
     SelPersForm.BTimeSheetOK.Enabled := b;
     SelPersForm.BTimeSheetCan.Enabled := b;
end;

procedure TDataRicerche.QRicTimeSheet_OLDAfterInsert(DataSet: TDataSet);
begin
     QRicTimeSheet.FieldbyName('IDRicerca').AsInteger := TRicerchePend.FieldbyName('ID').AsInteger;
end;

procedure TDataRicerche.QRicTimeSheet_OLDBeforeOpen(DataSet: TDataSet);
begin
     QGruppiProfLK.Open;
end;

procedure TDataRicerche.QRicTimeSheet_OLDAfterClose(DataSet: TDataSet);
begin
     QGruppiProfLK.Close;
end;

//[/TONI20020724\]

procedure TDataRicerche.QRicTimeSheet_OLDAfterDelete(DataSet: TDataSet);
begin
     {     // se c'� il controllo di gestione, allora inserisci o aggiorna time report
          with QRicTimeSheet do begin
               Data.DB.BeginTrans;
               try
                    ApplyUpdates;
                    if copy(Data.GlobalCheckBoxIndici.Value,6,1)='1' then begin
                         QTemp.Close;
                         QTemp.SQL.text:='delete from CG_ContrattiTimeSheet '+
                              'where IDTimeRepH1Sel=:xIDTimeRepH1Sel';
                         QTemp.ParambyName('xIDTimeRepH1Sel').asInteger:=xIDTimeSheet;
                         QTemp.ExecSQL;
                    end;
                    Data.DB.CommitTrans;
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: operazione non effettuata',mtError, [mbOK],0);
               end;
          end;}

          // se c'� il controllo di gestione, allora inserisci o aggiorna time report
     with QRicTimeSheet do begin
          Data.DB.BeginTrans;
          try
               if copy(Data.Global.FieldByNAme('CheckBoxIndici').Value, 6, 1) = '1' then begin
                    QTemp.Close;
                    QTemp.SQL.text := 'delete from CG_ContrattiTimeSheet ' +
                         'where IDTimeRepH1Sel=' + IntToStr(xIDTimeSheet);
                    QTemp.ExecSQL;
               end;
               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: operazione non effettuata', mtError, [mbOK], 0);
          end;
     end;
end;

procedure TDataRicerche.QRicTimeSheet_OLDBeforeDelete(DataSet: TDataSet);
begin
     //     xIDTimeSheet:=QRicTimeSheetID.Value;
     xIDTimeSheet := QRicTimeSheet.FIeldByName('ID').Value;
end;
//[/TONI20020724\]FINE

procedure TDataRicerche.QRicTimeSheet_OLDBeforePost(DataSet: TDataSet);
begin
     // controllo utente-data
{     Data.QTemp.Close;
     Data.QTemp.SQL.text:='select count(*) Tot from EBC_RicercheTimeSheet '+
          'where IDRicerca=:xIDRicerca and IDUtente=:xIDUtente and Data=:xData';
     Data.QTemp.ParamByName('xIDRicerca').asInteger:=TRicerchePendID.Value;
     Data.QTemp.ParamByName('xIDUtente').asInteger:=QRicTimeSheetIDUtente.Value;
     Data.QTemp.ParamByName('xData').asDateTime:=QRicTimeSheetData.Value;
     Data.QTemp.Open;
     if Data.QTemp.FieldByName('Tot').asInteger>0 then begin
        ShowMessage('Duplicazione utente-data - IMPOSSIBILE REGISTRARE');
        Abort;
     end;}
end;

procedure TDataRicerche.DsQTimeRepDettStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsQTimeRepDett.State in [dsEdit, dsInsert];
     SelPersForm.BTRepDettNew.Enabled := not b;
     SelPersForm.BTRepDettDel.Enabled := not b;
     SelPersForm.BTRepDettOK.Enabled := b;
     SelPersForm.BTRepDettCan.Enabled := b;
end;

procedure TDataRicerche.QTimeRepDett_OLDBeforeOpen(DataSet: TDataSet);
begin
     QTRepCausali.Open;
end;

procedure TDataRicerche.QTimeRepDett_OLDAfterClose(DataSet: TDataSet);
begin
     QTRepCausali.Close;
end;

//[TONI20020724]
//[/TONI20021003\]DEBUGOK

procedure TDataRicerche.QTimeRepDett_OLDAfterPost(DataSet: TDataSet);
begin
     {     with QTimeRepDett do begin
               Data.DB.BeginTrans;
               try
                    ApplyUpdates;
                    Data.DB.CommitTrans;
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
                    raise;
               end;
               CommitUpdates;
          end;}
end;

procedure TDataRicerche.QTimeRepDett_OLDAfterInsert(DataSet: TDataSet);
begin
     //     QTimeRepDettIDRicTimeSheet.Value:=QRicTimeSheetID.Value;
     QTimeRepDett.FieldByName('IDRicTimeSheet').Asinteger := QRicTimeSheet.FieldByName('ID').AsInteger;
end;
//[TONI20020724]FINE

procedure TDataRicerche.QUsersLKBeforeOpen(DataSet: TDataSet);
begin
     QUsersLK.SQL.text := 'select ID,Nominativo,Descrizione,IDGruppoProfess from Users ' +
          'where not ((DataScadenza is not null and DataScadenza<' + DateToStr(Date) + ') ' +
          '   or (DataRevoca is not null and DataRevoca<' + DateToStr(Date) + ') ' +
          '   or DataCreazione is null ' +
          '   or Tipo=0) ' +
          'order by Nominativo';
end;

procedure TDataRicerche.QOrgNewAfterClose(DataSet: TDataSet);
begin
     QAnagLK.Close;
     QRepartiLK.Close;
end;

procedure TDataRicerche.QTLCandidatiBeforeOpen(DataSet: TDataSet);
begin
     if not QRicTargetList.IsEmpty then begin
          if (Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 3)) = 'RAY') or (Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 7)) = 'MISTRAL') then begin
               {QTLCandidati.SQL.text := 'select EBC_CandidatiRicerche.ID, Anagrafica.ID IDAnagrafica, EsperienzeLavorative.ID IDEspLav, ' +
                    'CVNumero,Cognome,Nome, Mansioni.Descrizione RuoloAttuale,EBC_CandidatiRicerche.Note2 Note, ' +
                    'Anagrafica.IDStato,Cellulare,TelUfficio,DataNascita,RecapitiTelefonici, DescrizioneMansione ' +
                    'from EBC_CandidatiRicerche, Anagrafica, EsperienzeLavorative, Mansioni ' +
                    'where EBC_CandidatiRicerche.IDAnagrafica = Anagrafica.ID ' +
                    'and Anagrafica.ID=EsperienzeLavorative.IDAnagrafica ' +
                    'and EsperienzeLavorative.IDMansione *= Mansioni.ID ' +
                    'and EsperienzeLavorative.IDAzienda=' + QRicTargetList.FieldByName('IDCliente').AsString + ' ' +
                    'and EsperienzeLavorative.Attuale = 1 ' +
                    'and EBC_CandidatiRicerche.IDTargetList=' + QRicTargetList.FieldByName('ID').AsString; }
               //modifica query da Thomas
               QTLCandidati.SQL.text := 'select EBC_CandidatiRicerche.ID, Anagrafica.ID IDAnagrafica, EsperienzeLavorative.ID IDEspLav, ' +
                    'CVNumero,Cognome,Nome, Mansioni.Descrizione RuoloAttuale,EBC_CandidatiRicerche.Note2 Note, ' +
                    'Anagrafica.IDStato,Cellulare,TelUfficio,DataNascita,RecapitiTelefonici, DescrizioneMansione ' +
                    ' from EBC_CandidatiRicerche join Anagrafica on EBC_CandidatiRicerche.IDAnagrafica = Anagrafica.ID' +
                    ' join EsperienzeLavorative on Anagrafica.ID=EsperienzeLavorative.IDAnagrafica' +
                    ' left join Mansioni on EsperienzeLavorative.IDMansione = Mansioni.ID ' +
                    'where EsperienzeLavorative.IDAzienda=' + QRicTargetList.FieldByName('IDCliente').AsString + ' ' +
                    'and EsperienzeLavorative.Attuale = 1 ' +
                    'and EBC_CandidatiRicerche.IDTargetList=' + QRicTargetList.FieldByName('ID').AsString;
               if SelPersForm.CBOpzioneCand2.Checked then
                    QTLCandidati.SQL.add('and Escluso=0');
          end else begin
               {QTLCandidati.SQL.text := 'select EBC_CandidatiRicerche.ID, Anagrafica.ID IDAnagrafica, EsperienzeLavorative.ID IDEspLav, ' +
                    'CVNumero,Cognome,Nome, Mansioni.Descrizione RuoloAttuale,EBC_CandidatiRicerche.Note2 Note, ' +
                    'Anagrafica.IDStato,Cellulare,TelUfficio,DataNascita,RecapitiTelefonici, DescrizioneMansione ' +
                    'from EBC_CandidatiRicerche, Anagrafica, EsperienzeLavorative, Mansioni ' +
                    'where EBC_CandidatiRicerche.IDAnagrafica = Anagrafica.ID ' +
                    'and Anagrafica.ID=EsperienzeLavorative.IDAnagrafica ' +
                    'and EsperienzeLavorative.IDMansione *= Mansioni.ID ' +
                    'and EBC_CandidatiRicerche.IDTargetList=' + QRicTargetList.FieldByName('ID').AsString + ' ' +
                    'and EsperienzeLavorative.IDAzienda=' + QRicTargetList.FieldByName('IDCliente').AsString;}
               //modifica query da Thomas
               QTLCandidati.SQL.text := 'select EBC_CandidatiRicerche.ID, Anagrafica.ID IDAnagrafica, EsperienzeLavorative.ID IDEspLav, ' +
                    'CVNumero,Cognome,Nome, Mansioni.Descrizione RuoloAttuale,EBC_CandidatiRicerche.Note2 Note, ' +
                    'Anagrafica.IDStato,Cellulare,TelUfficio,DataNascita,RecapitiTelefonici, DescrizioneMansione ' +
                    ' from EBC_CandidatiRicerche join Anagrafica on EBC_CandidatiRicerche.IDAnagrafica = Anagrafica.ID' +
                    ' join EsperienzeLavorative on Anagrafica.ID=EsperienzeLavorative.IDAnagrafica' +
                    ' left join Mansioni on EsperienzeLavorative.IDMansione = Mansioni.ID' +
                    ' where EBC_CandidatiRicerche.IDTargetList=' + QRicTargetList.FieldByName('ID').AsString + ' ' +
                    ' and EsperienzeLavorative.IDAzienda=' + QRicTargetList.FieldByName('IDCliente').AsString;
               if SelPersForm.CBOpzioneCand2.Checked then
                    QTLCandidati.SQL.add('and Escluso=0');
          end;
     end else Abort;
end;

procedure TDataRicerche.QRicTargetListAfterScroll(DataSet: TDataSet);
begin
     if DataRicerche.QTLCandidati.State = dsEdit then DataRicerche.QTLCandidati.Post;
     QTLCandidati.Close;
     //QTLCandidati.ParamByName['xIDTargetList'] := QRicTargetListID.Value;
     //QTLCandidati.ParamByName['xIDAzienda'] := QRicTargetListIDCliente.Value;
     QTLCandidati.Open;
end;

procedure TDataRicerche.QRicTargetListBeforeClose(DataSet: TDataSet);
begin
     if DsQRicTargetList.State = dsEdit then QRicTargetList.Post;
end;

procedure TDataRicerche.QOrgNodoRuoloAfterOpen(DataSet: TDataSet);
begin
     if MainForm.LTotOrgRuolo.caption = '0' then
          QOrgNodoRuolo.Close;
end;

procedure TDataRicerche.DsQCommessaCLStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsQCommessaCL.State in [dsEdit, dsInsert];
     SelPersForm.BCLNew.Enabled := not b;
     SelPersForm.BCLDel.Enabled := not b;
     SelPersForm.BCLOK.Enabled := b;
     SelPersForm.BCLCan.Enabled := b;
end;

procedure TDataRicerche.QCommessaCLAfterInsert(DataSet: TDataSet);
begin
     QCommessaCLIDRicerca.Value := TRicerchePendID.Value;
end;

procedure TDataRicerche.QCommessaCLBeforeClose(DataSet: TDataSet);
begin
     if DsQCommessaCL.state in [dsEdit, dsInsert] then QCommessaCL.Post;
end;

procedure TDataRicerche.QCommessaCLBeforeOpen(DataSet: TDataSet);
begin
     QCommessaCL.Parameters[0].Value := TRicerchePendID.Value;
end;

procedure TDataRicerche.QCLStatBeforeOpen(DataSet: TDataSet);
begin
     QCLStat.Parameters[0].Value := TRicerchePendID.Value;
end;

procedure TDataRicerche.qCandRicAfterClose(DataSet: TDataSet);
begin
     //QLastEspLav.Close;
     //QTitoliStudioLK.Close;
end;

procedure TDataRicerche.qCandRicCalcFields(DataSet: TDataSet);
var xAnno, xMese, xgiorno: Word;
begin

     if QcandRicDataNascita.asString = '' then
          QcandRicEta.asString := ''
     else begin
          DecodeDate((Date - QcandRicDataNascita.Value), xAnno, xMese, xgiorno);
          // TAnagraficaEta.Value:=StrToInt(copy(xAnno,3,2));
          QcandRicEta.Value := copy(IntToStr(xAnno), 3, 2);
     end;
     QcandRicTelUffCell.Value := QcandRicCellulare.Value + ' ' + QcandRicTelUfficio.Value;

end;

procedure TDataRicerche.qCandRicBeforeEdit(DataSet: TDataSet);
begin
     // controllo se � bloccato
     //if qCandRicBloccato.Value then begin
     //    MessageDlg('Il record � bloccato per una modifica da parte di un altro utente - impossibile modificare',mtError,[mbOK],0);
     //    Abort;
     //end;

      InsertStoricoNoteQCandRic(inttostr(mainform.xIDUtenteAttuale), qCandRicID.asString, 'NoteBefore', Now);
end;

procedure TDataRicerche.qCandRicBeforePost(DataSet: TDataSet);
begin
//     qCandRicBloccato.value:=False;
end;

procedure TDataRicerche.qCandRicAfterPost(DataSet: TDataSet);
begin
      InsertStoricoNoteQCandRic(inttostr(mainform.xIDUtenteAttuale), qCandRicID.asString, 'NoteAfter', Now);
end;

procedure TDataRicerche.QRicAttiveAfterScroll(DataSet: TDataSet);
begin
     if Pos('BFK', UpperCase(Data.Global.FieldByName('NomeAzienda').AsString)) = 0 then begin
           // Campi personalizzati
        {  if (assigned(selpersform)) and (assigned(selpersform.TSComCampPers)) then begin
               if selpersform.PCSelPers.ActivePage = selpersform.TSComCampPers then begin
                    SelPersForm.CampiPersFrame1.TCampiPers.close;
                    SelPersForm.CampiPersFrame1.CaricaValori;
                    SelPersForm.CampiPersFrame1.ImpostaCampi;
               end;
          end;  }
     end;
end;

procedure TDataRicerche.qAnnunciCandCalcFields(DataSet: TDataSet);
var xAnno, XMese, xGiorno: Word;
begin
     if DataSet.FieldByName('DataNascita').asString = '' then
          DataSet.FieldByName('Eta').asString := ''
     else begin
          DecodeDate((Date - DataSet.FieldByName('DataNascita').Value), xAnno, xMese, xgiorno);
          // TAnagraficaEta.Value:=StrToInt(copy(xAnno,3,2));
          DataSet.FieldByName('Eta').Value := copy(IntToStr(xAnno), 3, 2);
     end;
end;

procedure TDataRicerche.QRicUtentiBeforeOpen(DataSet: TDataSet);
begin
     QRicUtenti.Parameters.ParamByName('xIDUtente').Value := TRicerchePendIDUtente.Value;
     QRicUtenti.Parameters.ParamByName('xIDRicerca').Value := TRicerchePendID.Value;
end;

procedure TDataRicerche.QValutazCandAfterOpen(DataSet: TDataSet);
begin
     if QValutazCand.RecordCount > 0 then begin
          SelPersForm.dxDBGrid9.FullExpand;
     end;
end;

procedure TDataRicerche.DsQValutazCandStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsQValutazCand.State in [dsEdit, dsInsert];
     SelPersForm.BValutazNew.Enabled := not b;
     SelPersForm.BValutazDel.Enabled := not b;
     SelPersForm.BValutazOK.Enabled := b;
     SelPersForm.BValutazCan.Enabled := b;
end;

procedure TDataRicerche.QValutazCandBeforeOpen(DataSet: TDataSet);
begin
     QValutazCand.Parameters.ParamByName('xidricerca').value := TRicerchePendID.Value;
end;

procedure TDataRicerche.qCandRicAfterOpen(DataSet: TDataSet);
begin

   //  if data.Global.FieldByName('DebugHrms').asBoolean = true then DataRicerche.QCandRic.SQL.savetofile('qcandric.sql');

end;

procedure TDataRicerche.QLastEspLavAfterOpen(DataSet: TDataSet);
begin
     //if data.Global.FieldByName('DebugHrms').asBoolean = true then QLastEspLav.SQL.SaveToFile('QLastEspLav.sql');
end;

procedure TDataRicerche.qCandRicBeforeOpen(DataSet: TDataSet);
begin
   //  QEspLavLK.Open;
   //  QLastEspLav.Open;

end;

procedure TDataRicerche.DsQAttivitaStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsQAttivita.State in [dsEdit, dsInsert];
     SelPersForm.BAttivitaNew.Enabled := not b;
     SelPersForm.BAttivitaDel.Enabled := not b;
     SelPersForm.BAttivitaOK.Enabled := b;
     SelPersForm.BAttivitaCan.Enabled := b;
end;

procedure TDataRicerche.QAttivitaBeforeOpen(DataSet: TDataSet);
begin
     QAttivita.Parameters[0].value := TRicerchePendID.Value;
     QCandidatiLK.Parameters[0].value := TRicerchePendID.Value;
     QUtentiRicLK.Parameters[0].value := TRicerchePendID.Value;
end;

procedure TDataRicerche.QCandidatiLKBeforeOpen(DataSet: TDataSet);
begin
     //QCandidatiLK.Parameters[0].value := TRicerchePendID.Value;
end;

procedure TDataRicerche.QUtentiRicLKBeforeOpen(DataSet: TDataSet);
begin
     //QUtentiRicLK.Parameters[0].value := TRicerchePendID.Value;
end;

procedure TDataRicerche.QAttivitaAfterInsert(DataSet: TDataSet);
begin
     QAttivitaIDRicerca.value := TRicerchePendID.Value;
     QAttivitaData.Value := Date;
end;

procedure TDataRicerche.QAttivitaBeforeClose(DataSet: TDataSet);
begin
     if DSQAttivita.state in [dsInsert, dsEdit] then QAttivita.Post;
end;

procedure TDataRicerche.QStatCommesseBeforeOpen(DataSet: TDataSet);
begin
     QStatCommesse.Parameters[0].value:=MainForm.SE_MesiStatCommesse.value;
     QStatCommesse.Parameters[1].value:=MainForm.SE_MesiStatCommesse.value;
     QStatCommesse.Parameters[2].value:=MainForm.SE_MesiStatCommesse.value;
     QStatCommesse.Parameters[3].value:=MainForm.SE_MesiStatCommesse.value;
end;

procedure TDataRicerche.QStatCommesseCalcFields(DataSet: TDataSet);
begin
     QStatCommesseTot.value:=QStatCommesseTotCand.value+QStatCommesseTotEsclusi.value;
end;

procedure TDataRicerche.QStatCommessaCalcFields(DataSet: TDataSet);
begin
     QStatCommessaTot.value:=QStatCommessaTotCand.value+QStatCommessaTotEsclusi.value;
end;

procedure TDataRicerche.QCommittentiCommesseBeforeOpen(DataSet: TDataSet);
begin
 QCommittentiCommesse.Parameters.ParamByName('xIDRicerca').Value := TRicerchePendID.Value;
end;

end.

