unit CandUtente;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, DBTables, Grids, DBGrids, ExtCtrls, RXDBCtrl, TB97, StdCtrls, Buttons,
     ADODB, U_ADOLinkCl, dxTL, dxDBCtrl, dxDBGrid, dxCntner;

type
     TCandUtenteFrame = class(TFrame)
          QCandUtente_OLD: TQuery;
          Panel12: TPanel;
          Panel1: TPanel;
          DsQCandUtente: TDataSource;
          QCandUtente_OLDID: TAutoIncField;
          QCandUtente_OLDCVNumero: TIntegerField;
          QCandUtente_OLDCognome: TStringField;
          QCandUtente_OLDNome: TStringField;
          QCandUtente_OLDDataNascita: TDateTimeField;
          QCandUtente_OLDRecapitiTelefonici: TStringField;
          QCandUtente_OLDCellulare: TStringField;
          QCandUtente_OLDggDataUltimoColloquio: TIntegerField;
          QCandUtente_OLDggDataUltimoContatto: TIntegerField;
          QCandUtente: TADOLinkedQuery;
          BLegenda: TToolbarButton97;
          RxDBGrid1: TdxDBGrid;
          RxDBGrid1CVNumero: TdxDBGridColumn;
          RxDBGrid1Cognome: TdxDBGridColumn;
          RxDBGrid1Nome: TdxDBGridColumn;
          RxDBGrid1DataNascita: TdxDBGridColumn;
          RxDBGrid1RecapitiTelefonici: TdxDBGridColumn;
          RxDBGrid1Cellulare: TdxDBGridColumn;
          RxDBGrid1ggDataUltimoColloquio: TdxDBGridColumn;
          RxDBGrid1ggDataUltimoContatto: TdxDBGridColumn;
          QCandUtenteID: TAutoIncField;
          QCandUtenteCVNumero: TIntegerField;
          QCandUtenteCognome: TStringField;
          QCandUtenteNome: TStringField;
          QCandUtenteDataNascita: TDateTimeField;
          QCandUtenteRecapitiTelefonici: TStringField;
          QCandUtenteCellulare: TStringField;
          QCandUtenteggDataUltimoColloquio: TIntegerField;
          QCandUtenteggDataUltimoContatto: TIntegerField;
          procedure RxDBGrid1TitleClick(Column: TColumn);
          procedure RxDBGrid1GetCellParams(Sender: TObject; Field: TField;
               AFont: TFont; var Background: TColor; Highlight: Boolean);
          procedure BLegendaClick(Sender: TObject);
     private
          xggDiffCol, xggDiffUc: integer;
     public
          xIDUtente: integer;
          //xColumnRic: TColumn;
          xColumnRic: TdxDBTreeListColumn;
     end;

implementation

uses ModuloDati, Main, LegendaCandUtente;

{$R *.DFM}


procedure TCandUtenteFrame.RxDBGrid1TitleClick(Column: TColumn);
var
     temp: string;
begin
     //QCandUtente.Close;
     {     QCandUtente.SQL.text:='select Anagrafica.ID,CVNumero,Cognome,Nome,DataNascita,RecapitiTelefonici,Cellulare, '+
                                'DATEDIFF(day, EBC_Colloqui.Data, getdate()) ggDataUltimoColloquio, '+
                                'DATEDIFF(day, EBC_ContattiCandidati.Data, getdate()) ggDataUltimoContatto '+
                                'from Anagrafica,EBC_Colloqui,EBC_ContattiCandidati '+
                                'where Anagrafica.ID *= EBC_Colloqui.IDAnagrafica '+
                                'and EBC_Colloqui.Data=(select max(Data) from EBC_Colloqui where Anagrafica.ID *= EBC_Colloqui.IDAnagrafica) '+
                                'and Anagrafica.ID *= EBC_ContattiCandidati.IDAnagrafica '+
                                'and EBC_ContattiCandidati.Data=(select max(Data) from EBC_ContattiCandidati '+
                                'where Anagrafica.ID *= EBC_ContattiCandidati.IDAnagrafica) '+
                                'and IDUtente=:xIDUtente';
          if Column=RxDBGrid1.Columns[0] then
             QCandUtente.SQL.Add('order by CVNumero');
          if Column=RxDBGrid1.Columns[1] then
             QCandUtente.SQL.Add('order by Cognome,Nome');
          if Column=RxDBGrid1.Columns[3] then
             QCandUtente.SQL.Add('order by DataNascita');
          QCandUtente.ParamByName('xIDUtente').asInteger:=xIDUtente;}
     {if Column = RxDBGrid1.Columns[0] then
          temp := ' order by CVNumero';
     if Column = RxDBGrid1.Columns[1] then
          temp := ' order by Cognome,Nome';
     if Column = RxDBGrid1.Columns[3] then
          temp := ' order by DataNascita';
     QCandUtente.SetSQLText(['select distinct Anagrafica.ID,CVNumero,Cognome,Nome,DataNascita,RecapitiTelefonici,Cellulare, ' +
          '       DATEDIFF(day, EBC_Colloqui.Data, getdate()) ggDataUltimoColloquio, ' +
               '       DATEDIFF(day, EBC_ContattiCandidati.Data, getdate()) ggDataUltimoContatto ' +
               'from Anagrafica ' +
               'left outer join EBC_Colloqui on (Anagrafica.ID = EBC_Colloqui.IDAnagrafica ' +
               '	and EBC_Colloqui.Data= ' +
               '		(select max(Data) from EBC_Colloqui ' +
               '		 where Anagrafica.ID = EBC_Colloqui.IDAnagrafica)) ' +
               'left outer join EBC_ContattiCandidati on (Anagrafica.ID = EBC_ContattiCandidati.IDAnagrafica  ' +
               '	and EBC_ContattiCandidati.Data=  ' +
               '		(select max(Data) from EBC_ContattiCandidati  ' +
               '		 where Anagrafica.ID = EBC_ContattiCandidati.IDAnagrafica))  ' +
               'where Anagrafica.IDUtente=:xIDUtente:  ' +
               //'order by Cognome,Nome ' + temp,
          //xIDUtente]);
          temp, xIDUtente]);
     QCandUtente.Open;        }
end;

procedure TCandUtenteFrame.RxDBGrid1GetCellParams(Sender: TObject;
     Field: TField; AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
     // trova parametro utente
     Data.Q1.Close;
     Data.Q1.SQL.text := 'select ggDiffCol,ggDiffUc from Users where ID=' + IntToStr(MainForm.xIDUtenteAttuale);
     Data.Q1.Open;
     xggDiffCol := Data.Q1.FieldByName('ggDiffCol').asInteger;
     xggDiffUc := Data.Q1.FieldByName('ggDiffUc').asInteger;
     Data.Q1.Close;
     if (Field.FieldName = 'ggDataUltimoColloquio') and (QCandUtente.FieldByname('ggDataUltimoColloquio').Value >= xggDiffCol)
          then begin Afont.Color := clRed; Afont.Style := [fsBold]; end; //Background:=clAqua;
     if (Field.FieldName = 'ggDataUltimoContatto') and (QCandUtente.FieldByName('ggDataUltimoContatto').Value >= xggDiffUc)
          then begin Afont.Color := clFuchsia; Afont.Style := [fsBold]; end; //Background:=clAqua;
end;

procedure TCandUtenteFrame.BLegendaClick(Sender: TObject);
begin
     LegendaCandUtenteForm := TLegendacandUtenteForm.create(self);
     LegendacandUtenteForm.ShowModal;
     LegendacandUtenteForm.Free;

     Data.Q1.Close;
     Data.Q1.SQL.text := 'select ggDiffCol,ggDiffUc from Users where ID=' + IntToStr(MainForm.xIDUtenteAttuale);
     Data.Q1.Open;
     xggDiffCol := Data.Q1.FieldByName('ggDiffCol').asInteger;
     xggDiffUc := Data.Q1.FieldByName('ggDiffUc').asInteger;
     Data.Q1.Close;

     // refresh griglia
     //RxDBGrid1TitleClick(xColumnRic);
end;

end.

