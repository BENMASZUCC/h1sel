//[TONI20021003]DEBUGOK
unit ElencoTipiAnnunci;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, ExtCtrls, TipiAnnunciFrame, Db, ADODB, U_ADOLinkCl,
     DBTables, TB97;

type
     TElencoTipiAnnunciForm = class(TForm)
          Panel1: TPanel;
          FrmTipiAnnunci: TFrmTipiAnnunci;
          BitBtn1: TToolbarButton97;
          BitBtn2: TToolbarButton97;
          procedure FormShow(Sender: TObject);
          procedure FrmTipiAnnunciToolbarButton971Click(Sender: TObject);
          procedure FrmTipiAnnunciToolbarButton972Click(Sender: TObject);
          procedure FrmTipiAnnunciToolbarButton973Click(Sender: TObject);
          procedure BitBtn1Click(Sender: TObject);
          procedure BitBtn2Click(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     ElencoTipiAnnunciForm: TElencoTipiAnnunciForm;

implementation
//[/TONI20020726\]
uses ModuloDati, Main;
//[/TONI20020726\]FINE

{$R *.DFM}

procedure TElencoTipiAnnunciForm.FormShow(Sender: TObject);
begin
     //Grafica
     ElencoTipiAnnunciForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     FrmTipiAnnunci.Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     FrmTipiAnnunci.Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     FrmTipiAnnunci.Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     Caption := '[S/11] - ' + caption;
end;

procedure TElencoTipiAnnunciForm.FrmTipiAnnunciToolbarButton971Click(
     Sender: TObject);
begin
     FrmTipiAnnunci.ToolbarButton971Click(Sender);

end;

procedure TElencoTipiAnnunciForm.FrmTipiAnnunciToolbarButton972Click(
     Sender: TObject);
begin
     FrmTipiAnnunci.ToolbarButton972Click(Sender);

end;

procedure TElencoTipiAnnunciForm.FrmTipiAnnunciToolbarButton973Click(
     Sender: TObject);
begin
     FrmTipiAnnunci.ToolbarButton973Click(Sender);

end;

procedure TElencoTipiAnnunciForm.BitBtn1Click(Sender: TObject);
begin
     ElencoTipiAnnunciForm.ModalResult := mrOk;
end;

procedure TElencoTipiAnnunciForm.BitBtn2Click(Sender: TObject);
begin
     ElencoTipiAnnunciForm.ModalResult := mrCancel;
end;

end.

