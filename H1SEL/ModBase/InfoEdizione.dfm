�
 TINFOEDIZIONEFORM 0J  TPF0TInfoEdizioneFormInfoEdizioneFormLeft2Top� Width�Height� Caption Informazioni aggiuntive edizioneFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style PixelsPerInch`
TextHeight TPageControlPageControl4Left Top Width�Height� 
ActivePageTSEdizCostiAlignalClientTabOrder  	TTabSheetTSEdizCostiCaptionCosti 	TGroupBox	GroupBox3LeftTopWidth� HeightjCaptionCosto a parolaTabOrder  TLabelLabel23LeftTop Width"HeightCaptionFeriale:  TLabelLabel24LeftTop8Width%HeightCaptionFestivo:  TLabelLabel26LeftTopPWidthHeightCaptionRPQ:  TLabelLabel27LeftaTopWidthHeightCaptionLireFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel28Left� TopWidthHeightCaptionEuroFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TDBEditDBEdit5Left7TopWidth=Height	DataFieldCostoParolaFerLire
DataSourceDataAnnunci.DsEdizioniTestTabOrder   TDBEditDBEdit18Left7Top4Width=Height	DataFieldCostoParolaFestLire
DataSourceDataAnnunci.DsEdizioniTestTabOrder  TDBEditDBEdit19Left7TopLWidth=Height	DataFieldCostoParolaRPQLire
DataSourceDataAnnunci.DsEdizioniTestTabOrder  TDBEditDBEdit20LeftxTopWidth=HeightColor	clBtnFace	DataFieldCostoParolaFerEuro
DataSourceDataAnnunci.DsEdizioniTestTabOrder  TDBEditDBEdit21LeftxTop4Width=HeightColor	clBtnFace	DataFieldCostoParolaFestEuro
DataSourceDataAnnunci.DsEdizioniTestTabOrder  TDBEditDBEdit22LeftxTopLWidth=HeightColor	clBtnFace	DataFieldCostoParolaRPQEuro
DataSourceDataAnnunci.DsEdizioniTestTabOrder   	TGroupBox	GroupBox4Left� TopWidth� HeightjCaptionCosto a moduloTabOrder TLabelLabel29LeftTop Width"HeightCaptionFeriale:  TLabelLabel30LeftTop8Width%HeightCaptionFestivo:  TLabelLabel34LeftTopPWidthHeightCaptionRPQ:  TLabelLabel36LeftaTopWidthHeightCaptionLireFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel37Left� TopWidthHeightCaptionEuroFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TDBEditDBEdit25Left7TopWidth=Height	DataFieldCostoModuloFerLire
DataSourceDataAnnunci.DsEdizioniTestTabOrder   TDBEditDBEdit26Left7Top4Width=Height	DataFieldCostoModuloFestLire
DataSourceDataAnnunci.DsEdizioniTestTabOrder  TDBEditDBEdit27Left7TopLWidth=Height	DataFieldCostoModuloRPQLire
DataSourceDataAnnunci.DsEdizioniTestTabOrder  TDBEditDBEdit28LeftxTopWidth=HeightColor	clBtnFace	DataFieldCostoModuloFerEuro
DataSourceDataAnnunci.DsEdizioniTestTabOrder  TDBEditDBEdit29LeftxTop4Width=HeightColor	clBtnFace	DataFieldCostoModuloFestEuro
DataSourceDataAnnunci.DsEdizioniTestTabOrder  TDBEditDBEdit30LeftxTopLWidth=HeightColor	clBtnFace	DataFieldCostoModuloRPQEuro
DataSourceDataAnnunci.DsEdizioniTestTabOrder    	TTabSheetTSEdizPenetrCaptionPenetrazione TLabelLabel54LeftTopWidth9HeightCaption
Giorno RPQFocusControlDBEdit36  TLabelLabel55LeftETopWidthHeightCaption(1-7)  	TGroupBox	GroupBox5LeftTopWidth� HeighteCaptionPenetrazioneTabOrder  TLabelLabel39Left� Top8WidthHeightCaptionProv.FocusControlDBEdit31  TLabelLabel52LeftTopWidth'HeightCaptionComuneFocusControlDBEdit34  TLabelLabel53LeftTop8WidthHeightCaptionZonaFocusControlDBEdit35  TDBEditDBEdit31Left� TopHWidthHeight	DataFieldPenetrazioneProvincia
DataSourceDataAnnunci.DsEdizioniTestTabOrder   TDBEditDBEdit34LeftTop Width� Height	DataFieldPenetrazioneComune
DataSourceDataAnnunci.DsEdizioniTestTabOrder  TDBEditDBEdit35LeftTopHWidth� Height	DataFieldPenetrazioneZona
DataSourceDataAnnunci.DsEdizioniTestTabOrder   TDBEditDBEdit36LeftaTopWidthHeight	DataField	GiornoRPQ
DataSourceDataAnnunci.DsEdizioniTestTabOrder   	TTabSheetTSEdizScontiCaptionSconti TDBGridDBGrid26LeftTopWidth� Heightr
DataSourceDataAnnunci.DsScontiTabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style Columns	FieldNameDaModuliTitle.Captionda mod.Width2 	FieldNameAModuliTitle.Captiona mod.Width/ 	FieldName
PercScontoTitle.Caption% scontoWidth9     	TTabSheetTSEdizGiorniCaptionGiorni indicati TDBGridDBGrid27LeftTopWidthHeightq
DataSourceDataAnnunci.DsGiorniIndicatiTabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style Columns	FieldNameGiornoTitle.CaptionGiorno (1-7) 	FieldName	Qualifica       