unit SelQuest;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     dxDBTLCl, dxGrClms, dxTL, dxDBCtrl, dxDBGrid, dxCntner, Db, ADODB,
     StdCtrls, Buttons, ExtCtrls, TB97;

type
     TSelQuestForm = class(TForm)
          Panel1: TPanel;
          QElencoQuest: TADOQuery;
          DsQElencoQuest: TDataSource;
          QElencoQuestID: TAutoIncField;
          QElencoQuestQuestionario: TStringField;
          QElencoQuestTipologia: TStringField;
          QElencoQuestGruppoQuestionario: TStringField;
          QElencoQuestAttivo: TBooleanField;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1ID: TdxDBGridMaskColumn;
          dxDBGrid1Questionario: TdxDBGridMaskColumn;
          dxDBGrid1Tipologia: TdxDBGridMaskColumn;
          dxDBGrid1GruppoQuestionario: TdxDBGridMaskColumn;
          dxDBGrid1Attivo: TdxDBGridCheckColumn;
          bitbtn1: TToolbarButton97;
          BitBtn2: TToolbarButton97;
          procedure FormShow(Sender: TObject);
          procedure BitBtn2Click(Sender: TObject);
          procedure bitbtn1Click(Sender: TObject);
     private
    { Private declarations }
     public
    { Public declarations }
     end;

var
     SelQuestForm: TSelQuestForm;

implementation

uses ModuloDati, Main;

{$R *.DFM}

procedure TSelQuestForm.FormShow(Sender: TObject);
begin
     //Grafica
     SelQuestForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     QElencoQuest.Open;
end;

procedure TSelQuestForm.BitBtn2Click(Sender: TObject);
begin
     SelQuestForm.ModalResult := mrCancel;
end;

procedure TSelQuestForm.bitbtn1Click(Sender: TObject);
begin
     SelQuestForm.ModalResult := mrOk;
end;

end.

