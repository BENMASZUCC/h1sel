unit CercaAnnuncio;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, Db, Grids, DBGrids, DBTables, ExtCtrls, dxDBTLCl,
     dxGrClms, dxTL, dxDBCtrl, dxDBGrid, dxCntner, ADODB, U_ADOLinkCl, TB97;

type
     TCercaAnnuncioForm = class(TForm)
          DsQAnnAttivi: TDataSource;
          Panel1: TPanel;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1Rif: TdxDBGridMaskColumn;
          dxDBGrid1Data: TdxDBGridDateColumn;
          dxDBGrid1NomeEdizione: TdxDBGridMaskColumn;
          dxDBGrid1Denominazione: TdxDBGridMaskColumn;
          dxDBGrid1Civetta: TdxDBGridCheckColumn;
          dxDBGrid1ID: TdxDBGridMaskColumn;
          dxDBGrid1Archiviato: TdxDBGridCheckColumn;
          dxDBGrid1IDAnnEdizData: TdxDBGridMaskColumn;
          dxDBGrid1Codice: TdxDBGridColumn;
    Panel2: TPanel;
    BOK: TToolbarButton97;
    BAnnulla: TToolbarButton97;
    dxDBGrid1Ruoli: TdxDBGridColumn;
    dxDBGrid1Clienti: TdxDBGridColumn;
    QAnnAttivi: TADOQuery;
    QAnnAttiviRif: TStringField;
    QAnnAttiviData: TDateTimeField;
    QAnnAttiviIDAnnEdizData: TAutoIncField;
    QAnnAttiviNomeEdizione: TStringField;
    QAnnAttiviDenominazione: TStringField;
    QAnnAttiviCivetta: TBooleanField;
    QAnnAttiviID: TAutoIncField;
    QAnnAttiviArchiviato: TBooleanField;
    QAnnAttivicodice: TStringField;
    QAnnAttiviClienti: TStringField;
    QAnnAttiviRuoli: TStringField;
          procedure FormShow(Sender: TObject);
    procedure BOKClick(Sender: TObject);
    procedure BAnnullaClick(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     CercaAnnuncioForm: TCercaAnnuncioForm;

implementation

uses SceltaStato, ModuloDati, Main;

{$R *.DFM}

procedure TCercaAnnuncioForm.FormShow(Sender: TObject);
begin
     //Grafica
     CercaAnnuncioForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     Caption := '[S/16] - ' + Caption;
     //dxDBGrid1.Filter.Add(dxDBGrid1Archiviato,'False','No');
end;

procedure TCercaAnnuncioForm.BOKClick(Sender: TObject);
begin
       CercaAnnuncioForm.ModalResult:=mrOK
end;

procedure TCercaAnnuncioForm.BAnnullaClick(Sender: TObject);
begin
CercaAnnuncioForm.ModalResult:=mrCancel;
end;

end.
