unit Testata;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, TB97, ExtCtrls;

type
     TTestataForm = class(TForm)
          Label1: TLabel;
          Label2: TLabel;
          EDenom: TEdit;
          ETipo: TEdit;
          Label3: TLabel;
          ENote: TEdit;
          Panel1: TPanel;
          BitBtn1: TToolbarButton97;
          BitBtn2: TToolbarButton97;
          procedure FormShow(Sender: TObject);
          procedure BitBtn1Click(Sender: TObject);
          procedure BitBtn2Click(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     TestataForm: TTestataForm;

implementation

uses Main;

{$R *.DFM}

procedure TTestataForm.FormShow(Sender: TObject);
begin
     //Grafica
     TestataForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
end;

procedure TTestataForm.BitBtn1Click(Sender: TObject);
begin
     TestataForm.ModalResult := mrOk;
end;

procedure TTestataForm.BitBtn2Click(Sender: TObject);
begin
     TestataForm.ModalResult := mrCancel;
end;

end.

