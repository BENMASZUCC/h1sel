unit RitoccaImage;

interface

uses Windows, Classes, Graphics, Forms, Controls, ImageEn, Menus,
  ImageEnView, RulerBox, ExtCtrls, SysUtils, ImageEnProc, ImageEnIO,
  StdCtrls, ComCtrls, Buttons;

type
  TRitoccaImageForm = class(TForm)
    ImageEnProc1: TImageEnProc;
    ImageEnView1: TImageEnView;
    ImageEnIO1: TImageEnIO;
    SpeedPanel: TPanel;
    SaveBtn: TSpeedButton;
    CutBtn: TSpeedButton;
    Bevel1: TBevel;
    Bevel2: TBevel;
    SpeedButton3: TSpeedButton;
    SpeedButton5: TSpeedButton;
    TrackBar1: TTrackBar;
    Label1: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ImageEn1ImageChange(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure SpeedButton5Click(Sender: TObject);
    procedure CutBtnClick(Sender: TObject);
  private
    { Private declarations }
  public
    Changed:boolean;
    xFileName:string;
    procedure ApriImmagine;
  end;

implementation

uses Main, Dialogs, View;

{$R *.DFM}

////////////////////////////////////////////////////////////////////////////////////

procedure TRitoccaImageForm.ApriImmagine;
begin
     ImageEnIO1.LoadFromFile(xFileName);
     ImageEnProc1.ClearUndo;
     ImageEnView1.Background:=clWhite;
end;

procedure TRitoccaImageForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
end;

////////////////////////////////////////////////////////////////////////////////////
// Imaged changed
procedure TRitoccaImageForm.ImageEn1ImageChange(Sender: TObject);
begin
     Changed:=true;
end;

////////////////////////////////////////////////////////////////////////////////////
procedure TRitoccaImageForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
{  CanClose:=true;
  if Changed then
	  case MessageDlg('Save Changes to '+Caption+'?',mtConfirmation,[mbYes,mbNo,mbCancel],0) of
	  		mrYes:
	      	MainForm.FileSaveItemClick(self);
	      mrCancel:
	      	CanClose:=false;
	  end;
  }
end;

////////////////////////////////////////////////////////////////////////////////////
procedure TRitoccaImageForm.SpeedButton5Click(Sender: TObject);
begin
     ImageEnView1.MouseInteract:=[miSelect];
     ImageEnView1.Cursor:=1785;
end;

procedure TRitoccaImageForm.CutBtnClick(Sender: TObject);
begin
     ImageEnProc1.SelCutToClip;
end;

end.
