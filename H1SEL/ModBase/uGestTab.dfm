object GestTabForm: TGestTabForm
  Left = 377
  Top = 171
  Width = 524
  Height = 449
  ActiveControl = ECerca
  Caption = 'Gestione tabella'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 516
    Height = 42
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 17
      Width = 40
      Height = 13
      Caption = 'Ricerca:'
    end
    object BitBtn1: TBitBtn
      Left = 310
      Top = 4
      Width = 101
      Height = 34
      Anchors = [akTop, akRight]
      TabOrder = 0
      Kind = bkOK
    end
    object BitBtn2: TBitBtn
      Left = 411
      Top = 4
      Width = 101
      Height = 34
      Anchors = [akTop, akRight]
      Caption = 'Annulla'
      TabOrder = 1
      Kind = bkCancel
    end
    object ECerca: TEdit
      Left = 54
      Top = 14
      Width = 125
      Height = 21
      TabOrder = 2
      OnChange = ECercaChange
    end
  end
  object dxDBGrid27: TdxDBGrid
    Left = 0
    Top = 42
    Width = 516
    Height = 380
    Bands = <
      item
      end>
    DefaultLayout = True
    HeaderPanelRowCount = 1
    KeyField = 'ID'
    SummaryGroups = <>
    SummarySeparator = ', '
    Align = alClient
    TabOrder = 1
    DataSource = DsQTab
    Filter.Criteria = {00000000}
    OptionsBehavior = [edgoAutoSearch, edgoAutoSort, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoTabThrough, edgoVertThrough]
    OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
    OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoIndicator, edgoUseBitmap]
    object dxDBGrid27ID: TdxDBGridMaskColumn
      Visible = False
      BandIndex = 0
      RowIndex = 0
      FieldName = 'ID'
    end
    object dxDBGrid27Codice: TdxDBGridMaskColumn
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Codice'
    end
    object dxDBGrid27Descrizione: TdxDBGridMaskColumn
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Descrizione'
    end
  end
  object QTab: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select ID, Codice, Descrizione'
      'from MP_Tab_CareerBuilder_EmployeeTypes')
    Left = 80
    Top = 120
    object QTabID: TIntegerField
      FieldName = 'ID'
    end
    object QTabCodice: TStringField
      FieldName = 'Codice'
    end
    object QTabDescrizione: TStringField
      FieldName = 'Descrizione'
      Size = 100
    end
  end
  object DsQTab: TDataSource
    DataSet = QTab
    Left = 80
    Top = 152
  end
end
