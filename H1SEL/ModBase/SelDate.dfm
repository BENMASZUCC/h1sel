�
 TSELDATEFORM 0�  TPF0TSelDateFormSelDateFormLeftSTop� BorderStylebsDialogCaptionSelezione date e orario comuneClientHeight� ClientWidthColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterPixelsPerInch`
TextHeight TLabelLabel1LeftTopWidth3HeightCaptionDalla data:  TLabelLabel2LeftTop#Width,HeightCaption
Alla data:  TBitBtnBitBtn1Left� TopWidthVHeight TabOrder OnClickBitBtn1ClickKindbkOK  TBitBtnBitBtn2Left� Top*WidthVHeight CaptionAnnullaTabOrderKindbkCancel  TDateEdit97DataDalLeft<TopWidth~HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderColorCalendar.ColorValidclBlueDayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanClear
doCanPopup
doIsMaskeddoShowCanceldoShowToday   TDateEdit97DataAlLeft<TopWidth~HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderColorCalendar.ColorValidclBlueDayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanClear
doCanPopup
doIsMaskeddoShowCanceldoShowToday   	TGroupBox	GroupBox1LeftTop� Width� Height*CaptionOrario comuneTabOrder TLabelLabel3LeftTopWidth+HeightCaption
dalle ore:  TLabelLabel4LeftbTopWidth%HeightCaption	alle ore:  	TMaskEdit
MEDalleOreLeft8TopWidth%HeightEditMask
!90:00;1;_	MaxLengthTabOrder Text  .    	TMaskEdit	MEAlleOreLeft� TopWidth#HeightEditMask
!90:00;1;_	MaxLengthTabOrderText  .     	TGroupBox	GroupBox2LeftTop8Width� HeightZCaptionGiorni della settimanaTabOrder 	TCheckBoxCBLunLeftTopWidthLHeightCaptionLuned�Checked	State	cbCheckedTabOrder   	TCheckBoxCBMarLeftTop,WidthLHeightCaptionMarted�Checked	State	cbCheckedTabOrder  	TCheckBoxCBMerLeftTopDWidthLHeightCaption	Mercoled�Checked	State	cbCheckedTabOrder  	TCheckBoxCBGioLeft`TopWidthLHeightCaptionGioved�Checked	State	cbCheckedTabOrder  	TCheckBoxCBVenLeft`Top,WidthIHeightCaptionVenerd�Checked	State	cbCheckedTabOrder  	TCheckBoxCBSabLeft`TopDWidthIHeightCaptionSabatoChecked	State	cbCheckedTabOrder    