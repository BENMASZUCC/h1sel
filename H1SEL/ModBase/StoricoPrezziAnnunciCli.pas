//[/TONI20020921\]DEBUGOK
unit StoricoPrezziAnnunciCli;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, Grids, DBGrids, RXDBCtrl, DBTables, StdCtrls, ExtCtrls, Buttons,
     ComCtrls, Mask, DBCtrls, ADODB, U_ADOLinkCl, TB97, dxTL, dxDBCtrl,
  dxDBGrid, dxCntner;

type
     TStoricoPrezziAnnunciCliForm = class(TForm)
          Panel1: TPanel;
          Label1: TLabel;
          ECliente: TEdit;
          QStoricoCli_OLD: TQuery;
          dsQStoricoCli: TDataSource;
          QStoricoCli_OLDData: TDateTimeField;
          QStoricoCli_OLDCostoLire: TFloatField;
          QStoricoCli_OLDCostoEuro: TFloatField;
          QStoricoCli_OLDCostoANoiLire: TFloatField;
          QStoricoCli_OLDCostoANoiEuro: TFloatField;
          QStoricoCli_OLDNomeEdizione: TStringField;
          QStoricoCli_OLDTestata: TStringField;
          QStoricoCli_OLDScontoAlClienteLire: TFloatField;
          QStoricoCli_OLDScontoAlClienteEuro: TFloatField;
          QStoricoCli_OLDScontoANoiLire: TFloatField;
          QStoricoCli_OLDScontoANoiEuro: TFloatField;
          PCValute: TPageControl;
          TSLire: TTabSheet;
          TSEuro: TTabSheet;
          QStoricoCli_OLDNumModuli: TIntegerField;
          QStoricoCli_OLDGuadagnoLire: TFloatField;
          QStoricoCli_OLDGuadagnoEuro: TFloatField;
          Panel2: TPanel;
          QTotali_OLD: TQuery;
          QTotali_OLDsumCostoLire: TFloatField;
          QTotali_OLDsumCostoEuro: TFloatField;
          QTotali_OLDsumCostoANoiLire: TFloatField;
          QTotali_OLDsumCostoANoiEuro: TFloatField;
          QTotali_OLDsumScontoAlClienteLire: TFloatField;
          QTotali_OLDsumScontoAlClienteEuro: TFloatField;
          QTotali_OLDsumScontoANoiLire: TFloatField;
          QTotali_OLDsumScontoANoiEuro: TFloatField;
          DBEdit1: TDBEdit;
          DsQTotali: TDataSource;
          DBEdit2: TDBEdit;
          DBEdit3: TDBEdit;
          DBEdit4: TDBEdit;
          QTotali_OLDGuadagnoLire: TFloatField;
          QTotali_OLDGuadagnoEuro: TFloatField;
          DBEdit5: TDBEdit;
          Label2: TLabel;
          QTotali_OLDTotmoduli: TIntegerField;
          DBEdit6: TDBEdit;
          Panel3: TPanel;
          Label3: TLabel;
          DBEdit7: TDBEdit;
          DBEdit8: TDBEdit;
          DBEdit9: TDBEdit;
          DBEdit10: TDBEdit;
          DBEdit11: TDBEdit;
          DBEdit12: TDBEdit;
          QStoricoCli: TADOLinkedQuery;
          QStoricoCliGuadagnoEuro: TFloatField;
          QStoricoCliData: TDateTimeField;
          QStoricoCliCostoLire: TFloatField;
          QStoricoCliCostoEuro: TFloatField;
          QStoricoCliCostoANoiLire: TFloatField;
          QStoricoCliCostoANoiEuro: TFloatField;
          QStoricoCliNumModuli: TIntegerField;
          QStoricoCliScontoAlClienteLire: TFloatField;
          QStoricoCliScontoAlClienteEuro: TFloatField;
          QStoricoCliScontoANoiLire: TFloatField;
          QStoricoCliScontoANoiEuro: TFloatField;
          QStoricoCliNomeEdizione: TStringField;
          QStoricoCliTestata: TStringField;
          QStoricoCliGuadagnoLire1: TFloatField;
          Panel4: TPanel;
    BOK: TToolbarButton97;
    RxDBGrid2: TdxDBGrid;
    RxDBGrid2Data: TdxDBGridColumn;
    RxDBGrid2Testata: TdxDBGridColumn;
    RxDBGrid2NomeEdizione: TdxDBGridColumn;
    RxDBGrid2NumModuli: TdxDBGridColumn;
    RxDBGrid2CostoEuro: TdxDBGridColumn;
    RxDBGrid2ScontoAlClienteEuro: TdxDBGridColumn;
    RxDBGrid2CostoANoiEuro: TdxDBGridColumn;
    RxDBGrid2ScontoANoiEuro: TdxDBGridColumn;
    RxDBGrid2GuadagnoEuro: TdxDBGridColumn;
    RxDBGrid1: TdxDBGrid;
    dxDBGridData: TdxDBGridColumn;
    dxDBGridTestata: TdxDBGridColumn;
    dxDBGridNomeEdizione: TdxDBGridColumn;
    dxDBGridNumModuli: TdxDBGridColumn;
    dxDBGridCostoLire: TdxDBGridColumn;
    dxDBGridScontoAlClienteLire: TdxDBGridColumn;
    dxDBGridCostoANoiLire: TdxDBGridColumn;
    dxDBGridScontoANoiLire: TdxDBGridColumn;
    dxDBGridGuadagnoLire: TdxDBGridColumn;
    QTotali: TADOLinkedQuery;
    QTotaliGuadagnoLire: TIntegerField;
    QTotaliGuadagnoEuro: TFloatField;
    QTotalisumCostoLire: TFloatField;
    QTotalisumCostoEuro: TFloatField;
    QTotalisumCostoANoiLire: TFloatField;
    QTotalisumCostoANoiEuro: TFloatField;
    QTotalisumScontoAlClienteLire: TFloatField;
    QTotalisumScontoAlClienteEuro: TFloatField;
    QTotalisumScontoANoiLire: TFloatField;
    QTotalisumScontoANoiEuro: TFloatField;
    QTotaliTotModuli: TIntegerField;
    QStoricoCliID: TAutoIncField;
          procedure FormCreate(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure RxDBGrid1TitleClick(Column: TColumn);
          procedure QStoricoCli_OLDCalcFields(DataSet: TDataSet);
          procedure QTotali_OLDCalcFields(DataSet: TDataSet);
          procedure BOKClick(Sender: TObject);
     private
          { Private declarations }
     public
          xIDCliente: integer;
     end;

var
     StoricoPrezziAnnunciCliForm: TStoricoPrezziAnnunciCliForm;

implementation
uses
     ModuloDati, Main;
{$R *.DFM}

procedure TStoricoPrezziAnnunciCliForm.FormCreate(Sender: TObject);
begin
     xIDCliente := 0;
end;

//[ALBERTO 20020912]
//[/TONI20020920\]DEBUGOK

procedure TStoricoPrezziAnnunciCliForm.FormShow(Sender: TObject);
begin
     //DEBUG
     QTotali.Close;
     QTotali.ReloadSQL;
     //DEBUG FINE
     QTotali.ParamByName['xIDCliente'] := xIDCliente;
     QTotali.open;
     //DEBUG
     QStoricoCli.Close;
     QStoricoCli.ReloadSQL;
     //DEBUG FINE
     QStoricoCli.ParamByName['xIDCliente'] := xIDCliente;
     QStoricoCli.open;
     caption := '[M/180] - ' + caption;
     PCValute.ActivePage := TSEuro;

     //Grafici
     StoricoPrezziAnnunciCliForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel3.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel3.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel3.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel4.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel4.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel4.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
end;
//[/TONI20020920\]DEBUGFINE

//[ALBERTO 20020912]FINE

procedure TStoricoPrezziAnnunciCliForm.RxDBGrid1TitleClick(Column: TColumn);
begin
     {QStoricoCli.Close;
     QStoricoCli.SQL.text := 'select Ann_AnnEdizData.Data,CostoLire,CostoEuro, ' +
          '       CostoANoiLire,CostoANoiEuro,Ann_AnnEdizData.NumModuli, ' +
          '       ScontoAlClienteLire,ScontoAlClienteEuro, ' +
          '       ScontoANoiLire,ScontoANoiEuro, ' +
          '       NomeEdizione, Ann_Testate.Denominazione Testata ' +
          'from Ann_AnnEdizData, Ann_Edizioni, Ann_Testate, Ann_Annunci ' +
          'where Ann_AnnEdizData.IDEdizione=Ann_Edizioni.ID ' +
          '  and Ann_Edizioni.IDTestata=Ann_Testate.ID ' +
          '  and Ann_AnnEdizData.IDAnnuncio=Ann_Annunci.ID ' +
          '  and Ann_Annunci.IDCliente=:xIDCliente: ';
     if Column = RxDBGrid1.Columns[0] then
          QStoricoCli.SQL.Add('order by Ann_AnnEdizData.Data desc');
     if Column = RxDBGrid1.Columns[1] then
          QStoricoCli.SQL.Add('order by Ann_Testate.Denominazione,NomeEdizione');
     if Column = RxDBGrid1.Columns[2] then
          QStoricoCli.SQL.Add('order by NomeEdizione');
     if Column = RxDBGrid1.Columns[3] then
          QStoricoCli.SQL.Add('order by NumModuli');
     if Column = RxDBGrid1.Columns[4] then
          QStoricoCli.SQL.Add('order by CostoLire');
     if Column = RxDBGrid1.Columns[5] then
          QStoricoCli.SQL.Add('order by ScontoAlClienteLire');
     if Column = RxDBGrid1.Columns[6] then
          QStoricoCli.SQL.Add('order by CostoANoiLire');
     if Column = RxDBGrid1.Columns[7] then
          QStoricoCli.SQL.Add('order by ScontoANoiLire');
     QStoricoCli.Open; }
end;

procedure TStoricoPrezziAnnunciCliForm.QStoricoCli_OLDCalcFields(
     DataSet: TDataSet);
begin
     QStoricoCli.FieldByName('GuadagnoLire').Value := QStoricoCli.FieldByName('CostoLire').Value - QStoricoCli.FieldByName('CostoANoiLire').Value;
     QStoricoCli.FieldByName('GuadagnoEuro').Value := QStoricoCli.FieldByName('CostoEuro').Value - QStoricoCli.FieldByName('CostoANoiEuro').Value;
end;

procedure TStoricoPrezziAnnunciCliForm.QTotali_OLDCalcFields(
     DataSet: TDataSet);
begin
     QTotali.FieldByName('GuadagnoLire').Value := QTotali.FieldByName('sumCostoLire').AsInteger - QTotali.FieldByName('sumCostoANoiLire').AsInteger;
     QTotali.FieldByName('GuadagnoEuro').Value := QTotali.FieldByName('sumCostoEuro').Value - QTotali.FieldByName('sumCostoANoiEuro').Value;
end;

procedure TStoricoPrezziAnnunciCliForm.BOKClick(
     Sender: TObject);
begin
     ModalResult := mrOk;
end;

end.

