unit LeggiNote;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, ExtCtrls, TB97;

type
     TLeggiNoteForm = class(TForm)
          mNote: TMemo;
          Panel1: TPanel;
          Panel2: TPanel;
          BAnnulla: TToolbarButton97;
    Bsave: TToolbarButton97;
          procedure FormShow(Sender: TObject);
          procedure BsaveClick(Sender: TObject);
          procedure BAnnullaClick(Sender: TObject);
     private
    { Private declarations }
     public
          modificabile: boolean;
    { Public declarations }
     end;

var
     LeggiNoteForm: TLeggiNoteForm;

implementation

uses Main;

{$R *.DFM}

procedure TLeggiNoteForm.FormShow(Sender: TObject);
begin
     //Grafica
     LeggiNoteForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     if modificabile then begin
          Bsave.Visible := true;
          mNote.ReadOnly := false;
     end
     else bsave.Visible := false;

end;

procedure TLeggiNoteForm.BsaveClick(Sender: TObject);
begin
     LeggiNoteForm.ModalResult := mrOk;
end;

procedure TLeggiNoteForm.BAnnullaClick(Sender: TObject);
begin
     LeggiNoteForm.ModalResult := mrNo;
end;

end.

