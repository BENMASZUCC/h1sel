select distinct Ann_Annunci.ID IDAnnuncio,Rif,Ann_Testate.Denominazione Testata,
	   Ann_AnagAnnEdizData.ID IDAnagAnnEdizData, Ann_Edizioni.NomeEdizione,
	   Ann_AnnEdizData.ID IDAnnEdizData,Ann_anagAnnEdizData.Visionato,Ann_AnnEdizData.Data,
	   CVNumero,Cognome,Nome,DataNascita,CVInseritoIndata,Anagrafica.ID IDAnagrafica,
	   IDProprietaCV,EBC_CandidatiRicerche.ID IDCandRic, Ann_anagAnnEdizData.DataPervenuto,
                   PesoRaggiunto,Anagrafica.IDStato, AQ.ID IDAnagQuest, TotPunteggio,
                   Anagrafica.Provincia,Anagrafica.DomicilioProvincia,ai.Retribuzione,
                   EspLavAttuale.Azienda AziendaAtt,EspLavAttuale.Ruolo RuoloAtt, 
                   EspLavAttuale.JobTitle JobTitleEspLAv,
JobTitle=
case
when Ann_Annunci.qualeruolousare=0 then (select top 1 descrizione from Ann_AnnunciRuoli A JOIN mansioni m on m.id=a.idmansione where a.idannuncio=Ann_Annunci.id)
when Ann_Annunci.qualeruolousare=1 then (select
test=
case 
when r.titolocliente='' or r.titolocliente=null then m.descrizione
when r.titolocliente < '' or r.titolocliente > '' then r.titolocliente
end
 from Ann_AnnunciRicerche AR  
	join EBC_Ricerche R on AR.IDRicerca = R.ID 
	join Mansioni M on R.IDMansione = M.Id where AR.IDAnnuncio=Ann_Annunci.id)
when Ann_Annunci.qualeruolousare=2 then Ann_Annunci.JobTitle
end

from Ann_AnagAnnEdizData
join Ann_AnnEdizData on Ann_AnagAnnEdizData.IDAnnEdizData=Ann_AnnEdizData.ID
join Anagrafica on Ann_AnagAnnEdizData.IDAnagrafica=Anagrafica.ID
join Ann_Edizioni on Ann_AnnEdizData.IDEdizione=Ann_Edizioni.ID
join Ann_Testate on Ann_Edizioni.IDTestata=Ann_Testate.ID
join Ann_Annunci on Ann_AnnEdizData.IDAnnuncio=Ann_Annunci.ID
left outer join EBC_CandidatiRicerche on ( Ann_AnagAnnEdizData.IDAnagrafica = EBC_CandidatiRicerche.IDAnagrafica  and EBC_CandidatiRicerche.idricerca=Ann_AnagAnnEdizData.idricerca)
left outer join Ann_AnagAnnEdizDataQuery on Ann_AnagAnnEdizData.ID = ann_AnagAnnEdizDataQuery.IDAnagannedizdata  and ann_AnagAnnEdizDataQuery.idutente=:xIDUtente 
left outer join Quest_AnagQuest AQ on (Ann_Annunci.IDQuestionario = AQ.IDQuest and Ann_AnagAnnEdizData.IDAnagrafica = AQ.IDAnagrafica)
join anagaltreinfo ai on ai.idanagrafica=Ann_AnagAnnEdizData.IDAnagrafica

-- esperienza lavorativa attuale (ultima, se pi� di una) 
LEFT OUTER JOIN (
	SELECT IDAnagrafica
		,max(ID) MaxID
	FROM EsperienzeLavorative
	WHERE Attuale = 1
	GROUP BY IDAnagrafica
	) EspLav_MaxID ON Anagrafica.ID = EspLav_MaxID.IDAnagrafica
LEFT OUTER JOIN (
	SELECT Esperienzelavorative.ID
		,Esperienzelavorative.IDAnagrafica
		,Azienda = CASE 
			WHEN Esperienzelavorative.IDAzienda IS NULL
				THEN Esperienzelavorative.AziendaNome
			ELSE EBC_Clienti.Descrizione
			END
		--,titolomansione
		,Mansioni.Descrizione Ruolo
		--,cast(descrizionemansione AS VARCHAR(255)) descrizionemansione
		,ruolopresentaz AS JobTitle
		,Aree.Descrizione Area
		,ebc_attivita.attivita Settore
	    ,ebc_clienti.Telefono
	FROM Esperienzelavorative
	LEFT JOIN EBC_Clienti ON Esperienzelavorative.IDAzienda = EBC_Clienti.ID
	LEFT JOIN Mansioni ON Esperienzelavorative.IDMansione = Mansioni.ID
	JOIN aree ON Mansioni.IDArea = aree.id
	LEFT JOIN ebc_attivita ON ebc_attivita.id = esperienzelavorative.idsettore
	) EspLavAttuale ON EspLav_MaxID.MaxID = EspLavAttuale.ID

where Ann_AnagAnnEdizData.IDRicerca=:xIDRicerca
order by Cognome,Nome


 
 
 
 
 
 
 
