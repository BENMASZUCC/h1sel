unit SchedaSintetica;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, DtEdit97, Spin, Buttons, ExtCtrls, Grids, AdvGrid, TB97, Db,
     DBTables, DBGrids, ComCtrls, ProgBarEx, Mask, DBCtrls, OleCtrls;

type
     TSchedaSinteticaForm=class(TForm)
          Label2: TLabel;
          ECognome: TEdit;
          ENome: TEdit;
          DEDataNascita: TDateEdit97;
          Label3: TLabel;
          Label4: TLabel;
          CBSesso: TComboBox;
          Label7: TLabel;
          EDiploma1: TEdit;
          Label8: TLabel;
          EDiploma2: TEdit;
          Label9: TLabel;
          EArea1: TEdit;
          ERuolo1: TEdit;
          Label10: TLabel;
          EArea2: TEdit;
          ERuolo2: TEdit;
          Label11: TLabel;
          EArea3: TEdit;
          ERuolo3: TEdit;
          Shape1: TShape;
          Label12: TLabel;
          ESettore1: TEdit;
          EAzienda1: TEdit;
          Label13: TLabel;
          ESettore2: TEdit;
          EAzienda2: TEdit;
          Label14: TLabel;
          ESettore3: TEdit;
          EAzienda3: TEdit;
          Label15: TLabel;
          ELingua1: TEdit;
          CBConLingua1: TComboBox;
          Label16: TLabel;
          Label17: TLabel;
          ELingua2: TEdit;
          CBConLingua2: TComboBox;
          Label18: TLabel;
          Label19: TLabel;
          ELingua3: TEdit;
          CBConLingua3: TComboBox;
          Label20: TLabel;
          Label21: TLabel;
          ELingua4: TEdit;
          CBConLingua4: TComboBox;
          Label22: TLabel;
          Shape2: TShape;
          Shape3: TShape;
          Shape4: TShape;
          Shape5: TShape;
          Panel1: TPanel;
          CBRuoloAtt1: TCheckBox;
          CBRuoloAtt2: TCheckBox;
          CBRuoloAtt3: TCheckBox;
          CBEspAtt1: TCheckBox;
          CBEspAtt2: TCheckBox;
          CBEspAtt3: TCheckBox;
          GroupBox1: TGroupBox;
          Label5: TLabel;
          Label6: TLabel;
          Label24: TLabel;
          EComuneDom: TEdit;
          EProv: TEdit;
          ECapDom: TEdit;
          Label28: TLabel;
          ETelefoni: TEdit;
          Label1: TLabel;
          Label29: TLabel;
          ECellulare: TEdit;
          EEmail: TEdit;
          Label23: TLabel;
          EProvInteresse: TEdit;
          Label31: TLabel;
          Label33: TLabel;
          EInquadramento: TEdit;
          ERetribuzione: TEdit;
          GBAltriDati1: TGroupBox;
          CBMobilita: TCheckBox;
          CBCassaInteg: TCheckBox;
          CBDispPartTime: TCheckBox;
          CBDispInterinale: TCheckBox;
          CBCategProtette: TCheckBox;
          Label26: TLabel;
          ETelUfficio: TEdit;
          Q: TQuery;
          ToolbarButton971: TToolbarButton97;
          ToolbarButton972: TToolbarButton97;
    EStato: TEdit;
    Label25: TLabel;
    BitBtn1: TBitBtn;
          procedure FormShow(Sender: TObject);
          procedure ToolbarButton971Click(Sender: TObject);
          procedure ToolbarButton972Click(Sender: TObject);
     private
    // "variabili" dipendenti dal cliente (da tabella GLOBAL)
     public
          xIDAnag: integer;
     end;

var
     SchedaSinteticaForm: TSchedaSinteticaForm;

implementation

uses ModuloDati, Curriculum, Main, SchedaCand, uUtilsVarie ;

{$R *.DFM}

procedure TSchedaSinteticaForm.FormShow(Sender: TObject);
begin
     // caricamento dati
     Q.SQL.text:='select Cognome,Nome,DataNascita,Sesso,DomicilioComune, '+
          'DomicilioCap,DomicilioProvincia,RecapitiTelefonici,TelUfficio,Cellulare,Email,EBC_Stati.Stato '+
          'from Anagrafica,EBC_Stati where Anagrafica.IDStato=EBC_Stati.ID and Anagrafica.ID='+IntToStr(xIDAnag);
     Q.Open;
     ECognome.text:=Q.FieldByName('Cognome').asString;
     ENome.text:=Q.FieldByName('Nome').asString;
     DEDataNascita.Date:=Q.FieldByName('DataNascita').asDateTime;
     CBSesso.text:=Q.FieldByName('Sesso').asString;
     EComuneDom.text:=Q.FieldByName('DomicilioComune').asString;
     ECapDom.text:=Q.FieldByName('DomicilioCap').asString;
     EProv.text:=Q.FieldByName('DomicilioProvincia').asString;
     ETelefoni.text:=Q.FieldByName('RecapitiTelefonici').asString;
     ETelUfficio.text:=Q.FieldByName('TelUfficio').asString;
     ECellulare.text:=Q.FieldByName('Cellulare').asString;
     EEmail.text:=Q.FieldByName('EMail').asString;
     EStato.Text:=Q.FieldByName('Stato').asString;
     // Titoli di studio
     Q.Close;
     Q.SQL.text:='select Diplomi.Tipo,Diplomi.Descrizione from TitoliStudio,Diplomi '+
          'where TitoliStudio.IDDiploma=Diplomi.ID and TitoliStudio.IDAnagrafica='+IntToStr(xIDAnag);
     Q.Open;
     EDiploma1.Text:=Q.FieldByName('Tipo').asString+' - '+Q.FieldByName('Descrizione').asString;
     if Q.RecordCount>1 then begin
          Q.Next;
          EDiploma2.Text:=Q.FieldByName('Tipo').asString+' - '+Q.FieldByName('Descrizione').asString;
     end;
     // Aree e ruoli
     Q.Close;

//[ALBERTO 20020902]

     if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 5))='ERGON' then

//[ALBERTO 20020902]FINE

          Q.SQL.text:='select Aree.descrizione Area, Mansioni.Descrizione Ruolo, Attuale from esperienzeLavorative,Aree,mansioni '+
               'where esperienzeLavorative.IDArea=Aree.ID and esperienzeLavorative.IDMansione=Mansioni.ID '+
               'and IDAnagrafica='+IntToStr(xIDAnag)
     else
          Q.SQL.text:='select Aree.Descrizione Area, Mansioni.descrizione Ruolo,Attuale '+
               'from AnagMansioni,Aree,Mansioni where AnagMansioni.IDArea=Aree.ID '+
               'and AnagMansioni.IDMansione=Mansioni.ID and IDAnagrafica='+IntToStr(xIDAnag);
     Q.Open;
     if Q.RecordCount>0 then begin
          EArea1.Text:=Q.FieldByName('Area').asString;
          ERuolo1.text:=Q.FieldByName('Ruolo').asString;
          if Q.FieldByName('Attuale').AsBoolean then CBRuoloAtt1.Checked:=True else CBRuoloAtt1.Checked:=false;
          if Q.RecordCount>1 then begin
               Q.Next;
               EArea2.Text:=Q.FieldByName('Area').asString;
               ERuolo2.text:=Q.FieldByName('Ruolo').asString;
               if Q.FieldByName('Attuale').AsBoolean then CBRuoloAtt2.Checked:=True else CBRuoloAtt2.Checked:=false;
               if Q.RecordCount=3 then begin
                    Q.Next;
                    EArea3.Text:=Q.FieldByName('Area').asString;
                    ERuolo3.text:=Q.FieldByName('Ruolo').asString;
                    if Q.FieldByName('Attuale').AsBoolean then CBRuoloAtt3.Checked:=True else CBRuoloAtt3.Checked:=false;
               end;
          end;
     end;
     // Aziende (Esp.Lav.)
     Q.Close;
     Q.SQL.text:='select EBC_Attivita.Attivita Settore, EBC_Clienti.Descrizione Azienda, Attuale '+
          'from EsperienzeLavorative,EBC_Attivita,EBC_Clienti '+
          'where EsperienzeLavorative.IDSettore *= EBC_Attivita.ID '+
          'and EsperienzeLavorative.IDAzienda *= EBC_Clienti.ID and IDAnagrafica='+IntToStr(xIDAnag);
     Q.Open;
     if Q.RecordCount>0 then begin
          ESettore1.text:=Q.FieldByName('Settore').asString;
          EAzienda1.text:=Q.FieldByName('Azienda').asString;
          if Q.FieldByName('Attuale').AsBoolean then CBEspAtt1.Checked:=True else CBEspAtt1.Checked:=false;
          if Q.RecordCount>1 then begin
               Q.Next;
               ESettore2.text:=Q.FieldByName('Settore').asString;
               EAzienda2.text:=Q.FieldByName('Azienda').asString;
               if Q.FieldByName('Attuale').AsBoolean then CBEspAtt2.Checked:=True else CBEspAtt2.Checked:=false;
          end;
          if Q.RecordCount=3 then begin
               Q.Next;
               ESettore3.text:=Q.FieldByName('Settore').asString;
               EAzienda3.text:=Q.FieldByName('Azienda').asString;
               if Q.FieldByName('Attuale').AsBoolean then CBEspAtt2.Checked:=True else CBEspAtt2.Checked:=false;
          end;
     end;
     // Lingue
     CBConLingua1.Text:='';
     CBConLingua2.Text:='';
     CBConLingua3.Text:='';
     CBConLingua4.Text:='';
     Q.Close;
     Q.SQL.text:='select Lingua,Livello,livellolingue.LivelloConoscenza from LingueConosciute,livellolingue '+
          'where LingueConosciute.Livello *= livellolingue.ID and IDAnagrafica='+IntToStr(xIDAnag);
     Q.Open;
     if Q.RecordCount>0 then begin
          ELingua1.text:=Q.FieldByName('Lingua').asString;
          CBConLingua1.Text:=Q.FieldByName('Livello').asString+' '+Q.FieldByName('LivelloConoscenza').asString;
          if Q.RecordCount>1 then begin
               Q.Next;
               ELingua2.text:=Q.FieldByName('Lingua').asString;
               CBConLingua2.Text:=Q.FieldByName('Livello').asString+' '+Q.FieldByName('LivelloConoscenza').asString;
          end;
          if Q.RecordCount=3 then begin
               Q.Next;
               ELingua3.text:=Q.FieldByName('Lingua').asString;
               CBConLingua3.Text:=Q.FieldByName('Livello').asString+' '+Q.FieldByName('LivelloConoscenza').asString;
          end;
     end;
     // Altri dati
     Q.Close;
     Q.SQL.text:='select ProvInteresse,Inquadramento,Retribuzione, '+
                 'Mobilita,CassaIntegrazione,DispPartTime,DispInterinale,CateogorieProtette '+
                 'from AnagAltreInfo '+
          'where IDAnagrafica='+IntToStr(xIDAnag);
     Q.Open;
     if Q.RecordCount>0 then begin
        EProvInteresse.Text:=Q.FieldByName('ProvInteresse').asString;
        EInquadramento.text:=Q.FieldByName('Inquadramento').asString;
        ERetribuzione.text:=Q.FieldByName('Retribuzione').asString;
        CBMobilita.Checked:=Q.FieldByName('Mobilita').asBoolean;
        CBCassaInteg.Checked:=Q.FieldByName('CassaIntegrazione').asBoolean;
        CBDispPartTime.Checked:=Q.FieldByName('DispPartTime').asBoolean;
        CBDispInterinale.Checked:=Q.FieldByName('DispInterinale').asBoolean;
        CBCategProtette.Checked:=Q.FieldByName('CateogorieProtette').asBoolean;
     end;
end;

procedure TSchedaSinteticaForm.ToolbarButton971Click(Sender: TObject);
var x: string;
     i: integer;
begin
     PosizionaAnag(xIDAnag);

     Data.TTitoliStudio.Open;
     Data.TCorsiExtra.Open;
     Data.TLingueConosc.Open;
     Data.TEspLav.Open;
     CurriculumForm.ShowModal;

     Data.TTitoliStudio.Close;
     Data.TCorsiExtra.Close;
     Data.TLingueConosc.Close;
     Data.TEspLav.Close;
     MainForm.Pagecontrol5.ActivePage:=MainForm.TSStatoTutti;
     FormShow(self);
end;

procedure TSchedaSinteticaForm.ToolbarButton972Click(Sender: TObject);
begin
     SchedaCandForm:=TSchedaCandForm.create(self);
     if not PosizionaAnag(xIDAnag) then exit;
     SchedaCandForm.ShowModal;
     SchedaCandForm.Free;
     FormShow(self);
end;

end.

