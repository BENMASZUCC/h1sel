object ModificaStoricoAnagraficaForm: TModificaStoricoAnagraficaForm
  Left = 399
  Top = 120
  Width = 673
  Height = 711
  Caption = 'Modifica storico anagrafica'
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 650
    Width = 665
    Height = 34
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object BOK: TToolbarButton97
      Left = 32
      Top = 1
      Width = 81
      Height = 30
      Caption = 'OK'
      Glyph.Data = {
        76060000424D7606000000000000360400002800000018000000180000000100
        080000000000400200000000000000000000000100000000000000000000FFFF
        FF00FF00FF0040D86800B1AEAC00545149001D973F0076967600C4F1D1002827
        270079D2910045A35E00716D5E00CFC7C00024C04D009B958D005FBA77005CE6
        8000E4EFE70090B69900B1D9BC005C8864003D3C390076B3870034934E003ABD
        5D007C7B730054D073005F5E5E00938F7800AAA695002CD358009BC6A60062A4
        73006AC98300E0D5C70049EC7500C1BBB7004FB96A00D5EEDB00697C6D008187
        820081C091002AAF4D00B5E9C300F1F7F2009D9D9C003432310044C666005397
        60009F9B7F0037CC5E004A49430039A0550072A67F007C9B83005F7763006E6E
        6D0055A66C006BB87F00A7A6A5008B8C880030C257004ADC700086B48800668B
        6C0060B06C0077C48B00259F470092C29C00C8C2B00054C27100E7FAEC0065B0
        790085827000C5C3C20056DC790071D08A005B584E005BCA79009A939900DCF3
        E3004DCC6E0086C4960080AC8C0032B054007572710064C17D003DA95B0041B4
        6000C9E7D100A39F9200B9F1C800F8FBF900D9D1C000BBB5B50046E67000B1AE
        A20048AB630080C793008DBB91006D9276006EAF7F0064D081004EE3750043CF
        680078BB890056B66F002ABD52007FB68D00CEECD6003FC262007E7E7B006F88
        7000329F500094908800D0CCB90031CC5A00C1B9AE00489B5F00A7A49E003BD1
        6300C4BBC100679F6F0077CD8E00CCC7C60055E57A004FA66700888886005DB5
        720072C0870095BD9700ACABA8005CA76F008CC09A006360610030BD56004DD1
        700046BF66005BC37700CBC8B600BBB8B90038B7590049A85B00AFAA98008482
        7500A09A8D0073C9890033D05D00B5B2A80059B16F00D9EEDF0087BB940033C6
        5A0096C3A1006DC4850069AB7A0040DE6A0051D775005B59590049D76D009B98
        80008F8D8500ECF6EF00E5F4E90028C35100B8AFB1003FCB63005AE07E009C97
        880065CA7F007B967A0062B47300D3EAD9004949480042BA610079767500A2A1
        9F006CCD8600A2A1990070B5820074B98600D4CFBC00C4BFB9002BC756003CA4
        58004AC66B008DB68E007CC08D00BFBDBE0068BD7E003CC6610049E773003131
        2F00E0F2E500C9C1BF0045E06E004FD472006B866D0082817D0094BE9F005FBF
        790065A97700C1EDCD003BD6640053E1790063786600B1AFA700ABA9A4007CCC
        9100C9BFB40037BC5A003DB45D00657C6A009BC1A600D1EFD90051B56C007DBA
        8E007A7870009B9B9B007BB88B00D9F2DF00B8ECC60026B04B002BB34F004C9A
        61008F8F8C00E8F5EB0059C3740093C69F0062B478008CB6970085BE9300A5A4
        A20055D476009D969A0053B96E005CBE76007CC78F00DEEEE300CAEDD300D3CB
        BD00BFB6B400B7B3B20056544B00DCD3C200CFC9BC00C6BDBD0037CF5F0041DA
        6A0042A45D00ADADAC0057E17C00A9A8A800A4A0950086868300020202020202
        022E39AEC10909C1AE392E020202020202020202020202B11C2F1C507ABDBD7A
        EB872F9F2E0202020202020202028016567DE5E488757588E4E57DB016FF0202
        0202020202C734A69A3E033FCDFCFCCD3FCC3E455F34700202020202E205F7AC
        94A03F69687E1111A84CA09481C3F43D0202023C4EF2AC7579CC30E31B60C068
        CDFC4CC57596254EE902021A7383B833796FC26EA3EA24C0C43F4C9E89B8640F
        DA023C0C0D2B993319C26E6E6EA31B60C4F9039EC5A7DF0D0CE973A9BB6C998E
        C26E6ED76ED7A3529D03797989526C4092A29176428855C2AD6EDD4851D727A3
        BACC7933A752198FD2914A23358AA4ADAD51081F5CC22727A38AF8339930AF06
        234A1DB6B98BD797C2087579945CA42727A3AF9999D3AF448C1DA174FAEC47D7
        CBA533333375DEA42797A3D46CE026724632A1F5776BEDB24FBF3E999933B8DE
        E39797A35859D8185EA1A9F67B961057224D6752BF3E3E0E2CE3EF122D966231
        F1A9B346ABE6E6BE9B937C0A0A0A4DB2AAF02D12A3ADFA0746FE841ECF21493B
        8243EED1D1D17C939BC95A5DAD0BE1D0908402FEF671B5B46ABC63636363EE43
        823B81147F3AC6F6FE0202FB5BB741D9DC2A535353532ABCB53B4985851525FE
        FB0202028461CF41869886868686E8D91766CA9C15D095D00202020202D0CF5F
        CEE7209A9A86986D17173638F395D00202020202020284787D3D283713D6C854
        65D53D7D7884020202020202020202FBB1044B8D2E2929DB8D4B04B1FB020202
        020202020202020202FBB1DBB1FDFD2EDBB18402020202020202}
      OnClick = BOKClick
    end
    object Besci: TToolbarButton97
      Left = 520
      Top = 1
      Width = 81
      Height = 30
      Caption = 'Esci'
      Glyph.Data = {
        76060000424D7606000000000000360400002800000018000000180000000100
        080000000000400200000000000000000000000100000000000000000000FFFF
        FF00FF00FF0082817D001A1AA9005A65E500B1B2EC003D3C3A00DFDCC100B1AE
        A2004F4FA3008787C20062615D003437D300D8D9F6006566BF00626284009F9B
        7E00C3C3BB007878A6007E81E300282827009F9FD6003736B1004E4EC500C4C4
        DE009D9C9C00ECECF800535249007474CE006D6D6D002525BD004C53DC008F8F
        8C009090E2006362A700938E77006363D1004346B4005858B8003F47CE005656
        9500C1BFA9009191CC00AAA59300D4D0BA00A9ACDC00333330006D6C8D008484
        D3009D9FE7007C7CBB00D0D2EB00BFBFED00484848007B7A7300706E5D002D2E
        C900282AAE006A6CDC00616197005A5BCA00E1E1F0009391BF00B6B6AE006F6F
        B70085816F0073729A00565AD700A4A3A100CDCCC400B8B8E2004343C3008080
        8A008B89B6007C7CC800A19D8A003338C5004D4DB5003030A700C9C5AD007373
        C10088878600545EE2005151D1006969A0002D32B500474BD60096938400BBBB
        B600898ED500ADACAA00343BBA005F5FB300595A57006060C0004949AB00777C
        D4009898CF00C7C8E700ABABD2003B3ED1008482B600E4E5F7004A4ACC005957
        4D009A9A92005656C0008A8ACB005C5C8000D6D7EE00BDBDBF00646DE300C1C0
        B200A3A096006E6ECB00292DBD002323B000AFAB9700DDDDEA008383C800CFCB
        BB004146D9005157C8004040C8007171D5002D2FCE0075746D003F3FB4004949
        BA004E4EBD00D3D3F5009C988500393ACA00BCBBAE00ABAAA6005961E0006C6C
        BD003333CD00DDDEF1009696CA007E7ED200D9D5BB002929C9008E8D87008D8D
        DC00898A8E008C8CC400A3A5D7009B978000BFC0E100A7A49C0068678D007877
        9A00A1A09E009494DE004244D300B5B3A7004B54C700B4B6EE006068E1006E6E
        C200F0F1F800C8C7BC004447C9004E4FD500545ADD00777BCD004A4942005B5B
        BF008B8ABE00E7E7F300838174006262CB00B5B5E300424AD3005151CA005F5F
        BC007C7CC300CAC8C500393ED5009397D400848483004645AF0066668500C6C5
        C0003636BB0030302F00E1E1F600DCD9BD00CECBC0002F2FB100CAC7B7005C5C
        5B00C5C3B6003939B5007E7E7B007777D600EBEBF3004546D0004E56DF004949
        BF00B1AFAB005151C2005E5EC7008686CE007A7AC7003434C700BBBDE2005D64
        E0005355D800565DDD00DADBF0005353C6007E7ECC00D0CDB8003638CE009D9D
        94009B9BD3006161B9008C8CCD007B7BD000B8B7AA00454ADA00B1AFA6005A5A
        CE005E5BB500383BD100383DCE00414BCB009494CE008484C100E9E9F8003C3B
        3600C4C5E7003335B2003535C3003538C8004747B6005959BB005B5ECA006262
        D5006A6AC1007272C500E2E3F200E0E0EE00D8D8F10056544B002929BC002B2B
        C0003031C90079786E00A4A09300606081009C998900A7A6A400020202020202
        021A1E36BB1515BB361E1A0202020202020202020202029AC12F0C6A126F6F12
        D90C2F5E1A02020202020202020252077FB3E6F04DE4E44D3D2BB337E9B60202
        020202020203A8DE8CEDDFA688D1D188C87AED2B86A8C40202020202211CC25F
        B420A6D388A07070A0D1A474A9A3F790020202FF6986B10D5626AFC853535388
        D14494960DEF716945020237583F7EA46EA261C85305055320A7ABA2D27EAAFE
        FB02FF38BEF88A328BD434AF530505539E8B8B9FA59C1FBE384590844A8F8A3B
        676E3E5A205353E5638B6757A5A539664C90AC2AE2CF8A7E06D46EF47BC8282E
        8B6714E39CA585B750AC42081768FAD8E30ED4D4345CB5F48B9FE365D8C77C04
        0842242DC3B0688AD8A567D4D434F4D483B465D88AED4875C02495D780D5E154
        8A8A448B6E6E6E6E9CE3D88AFAF918BFC21195BDB7D53D25F1853AD06E6E6EEA
        EBFA8A39EC48CB4F8E95FED7236BCCAD27E73E8B6EBCF66ED427BAB0D5C9810A
        2D847250995F5FA119C6F53E1B22351B3EC664608281EE4350725B2CE05D5FAE
        ABF5F4E891C57D9B67F4F57789EE6087768702727930F34B1B1BF6318D8DDD1D
        73AE1BAB474E9879FC02025BFCC23CCE4B16D6CDCDCDD6CEF30FF2DA0F2971FC
        5B0202028709E03C6C786CDCDCCD78CE5189DBDB29879D87020202020287E086
        FD0B62E62B6C78B25151416D409D87020202020202025B974692B81393622B33
        5510924697870202020202020202025B9ACAB9591A49491A59B9CA9A5B020202
        0202020202020202025B451A9A87871A1A9A5B02020202020202}
      OnClick = BesciClick
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 665
    Height = 650
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 45
      Height = 13
      Caption = 'Cognome'
      FocusControl = DBEdit1
    end
    object Label2: TLabel
      Left = 188
      Top = 8
      Width = 27
      Height = 13
      Caption = 'Nome'
      FocusControl = DBEdit2
    end
    object Label3: TLabel
      Left = 358
      Top = 8
      Width = 18
      Height = 13
      Caption = 'CID'
      FocusControl = DBEdit3
    end
    object Label44: TLabel
      Left = 543
      Top = 9
      Width = 48
      Height = 13
      Caption = 'Dalla data'
    end
    object GroupBox1: TGroupBox
      Left = 9
      Top = 51
      Width = 646
      Height = 102
      Caption = 'Dati anagrafici'
      TabOrder = 2
      object Label4: TLabel
        Left = 60
        Top = 17
        Width = 23
        Height = 13
        Caption = 'Data'
      end
      object Label5: TLabel
        Left = 174
        Top = 16
        Width = 29
        Height = 13
        Caption = 'Luogo'
        FocusControl = DBEdit5
      end
      object Label6: TLabel
        Left = 403
        Top = 16
        Width = 26
        Height = 13
        Caption = 'Prov.'
        FocusControl = DBEdit6
      end
      object Label7: TLabel
        Left = 433
        Top = 16
        Width = 26
        Height = 13
        Caption = 'Stato'
        FocusControl = DBEdit7
      end
      object Label8: TLabel
        Left = 9
        Top = 34
        Width = 44
        Height = 13
        Caption = 'Nascita:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label9: TLabel
        Left = 8
        Top = 56
        Width = 52
        Height = 13
        Caption = 'Nazionalit�'
        FocusControl = DBEdit4
      end
      object Label10: TLabel
        Left = 186
        Top = 56
        Width = 52
        Height = 13
        Caption = 'Stato civile'
      end
      object SpeedButton78: TSpeedButton
        Left = 614
        Top = 31
        Width = 23
        Height = 22
        Hint = 'seleziona la nazione'
        Glyph.Data = {
          42020000424D4202000000000000420000002800000010000000100000000100
          1000030000000002000000000000000000000000000000000000007C0000E003
          00001F0000000000000029252925292529250000000000000000000000000000
          00000000000000005C123912BC2EBC2E782E2925292529252925292500000000
          00000000000000009F0EAF310000292598117819972178219819561529252925
          00000000000000009F12D12D29253605F404F2041009D300F40436117821140D
          2925000000000000DF2219161401D300F400150181011201F400D300F3045619
          1215292500000000FF2EBE221401560178019801C001A10157013601F4007015
          93250E19000000000000FF2EFA119901B901ED010002E501B701990157018709
          8D19311529250000000099011F3BDB01160232024E021D02FC01CE01C0018001
          860D4F0929250000000099013F43F6326E0A74067F027F023E021902E001A401
          69016D05292500000000380A730E7F53092F0A2B1C2BDF1E7F063E020702C701
          A0018C0D292500000000380AA9164F3F9F636E434A3B9F47DF1E5F022C02E801
          A00191152925000000000000E42627379453DF6FFD73B9571F2F940A1D020502
          2B1229250000000000000000102BD41E7547FB67FF7FB55B052F7A061D02380A
          2925000079260000000000000000172F172F96477747985F564B57067E122925
          0000000079260000000000000000000000001737143FF82E3D431A43DB3E8C31
          8C317926792600000000000000000000000000000000000000000000FD3EFD3E
          DD2E79260000}
        ParentShowHint = False
        ShowHint = True
        OnClick = SpeedButton78Click
      end
      object SpeedButton54: TSpeedButton
        Left = 158
        Top = 71
        Width = 23
        Height = 22
        Hint = 'seleziona la nazione'
        Glyph.Data = {
          42020000424D4202000000000000420000002800000010000000100000000100
          1000030000000002000000000000000000000000000000000000007C0000E003
          00001F0000000000000029252925292529250000000000000000000000000000
          00000000000000005C123912BC2EBC2E782E2925292529252925292500000000
          00000000000000009F0EAF310000292598117819972178219819561529252925
          00000000000000009F12D12D29253605F404F2041009D300F40436117821140D
          2925000000000000DF2219161401D300F400150181011201F400D300F3045619
          1215292500000000FF2EBE221401560178019801C001A10157013601F4007015
          93250E19000000000000FF2EFA119901B901ED010002E501B701990157018709
          8D19311529250000000099011F3BDB01160232024E021D02FC01CE01C0018001
          860D4F0929250000000099013F43F6326E0A74067F027F023E021902E001A401
          69016D05292500000000380A730E7F53092F0A2B1C2BDF1E7F063E020702C701
          A0018C0D292500000000380AA9164F3F9F636E434A3B9F47DF1E5F022C02E801
          A00191152925000000000000E42627379453DF6FFD73B9571F2F940A1D020502
          2B1229250000000000000000102BD41E7547FB67FF7FB55B052F7A061D02380A
          2925000079260000000000000000172F172F96477747985F564B57067E122925
          0000000079260000000000000000000000001737143FF82E3D431A43DB3E8C31
          8C317926792600000000000000000000000000000000000000000000FD3EFD3E
          DD2E79260000}
        ParentShowHint = False
        ShowHint = True
        OnClick = SpeedButton54Click
      end
      object DBEdit5: TDBEdit
        Left = 174
        Top = 32
        Width = 225
        Height = 21
        DataField = 'LuogoNascita'
        DataSource = StoricoAnagraficaForm.DSStoricoAnag
        TabOrder = 1
      end
      object DBEdit6: TDBEdit
        Left = 403
        Top = 32
        Width = 26
        Height = 21
        DataField = 'ProvNascita'
        DataSource = StoricoAnagraficaForm.DSStoricoAnag
        TabOrder = 2
      end
      object DBEdit7: TDBEdit
        Left = 433
        Top = 32
        Width = 176
        Height = 21
        DataField = 'StatoNascita'
        DataSource = StoricoAnagraficaForm.DSStoricoAnag
        TabOrder = 3
      end
      object DBEdit4: TDBEdit
        Left = 8
        Top = 72
        Width = 145
        Height = 21
        DataField = 'Nazionalita'
        DataSource = StoricoAnagraficaForm.DSStoricoAnag
        TabOrder = 5
      end
      object DBLookupComboBox1: TDBLookupComboBox
        Left = 185
        Top = 72
        Width = 123
        Height = 21
        DataField = 'StatoCivileLK'
        DataSource = StoricoAnagraficaForm.DSStoricoAnag
        TabOrder = 4
      end
      object dxDBDateEdit1: TdxDBDateEdit
        Left = 59
        Top = 32
        Width = 112
        TabOrder = 0
        DataField = 'DataNascita'
        DataSource = StoricoAnagraficaForm.DSStoricoAnag
      end
    end
    object DBEdit1: TDBEdit
      Left = 8
      Top = 24
      Width = 177
      Height = 21
      Color = clBtnFace
      DataField = 'Cognome'
      DataSource = StoricoAnagraficaForm.DSStoricoAnag
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 7
    end
    object DBEdit2: TDBEdit
      Left = 188
      Top = 24
      Width = 166
      Height = 21
      Color = clBtnFace
      DataField = 'Nome'
      DataSource = StoricoAnagraficaForm.DSStoricoAnag
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 8
    end
    object DBEdit3: TDBEdit
      Left = 358
      Top = 24
      Width = 168
      Height = 21
      DataField = 'CID'
      DataSource = StoricoAnagraficaForm.DSStoricoAnag
      TabOrder = 0
    end
    object GroupBox2: TGroupBox
      Left = 9
      Top = 158
      Width = 647
      Height = 142
      Caption = 'Residenza'
      TabOrder = 3
      object Label11: TLabel
        Left = 8
        Top = 16
        Width = 54
        Height = 13
        Caption = 'Tipo strada'
      end
      object Label12: TLabel
        Left = 92
        Top = 16
        Width = 40
        Height = 13
        Caption = 'Indirizzo'
        FocusControl = DBEdit9
      end
      object Label13: TLabel
        Left = 380
        Top = 16
        Width = 40
        Height = 13
        Caption = 'N�Civico'
        FocusControl = DBEdit10
      end
      object Label14: TLabel
        Left = 448
        Top = 16
        Width = 33
        Height = 13
        Caption = 'Edificio'
        FocusControl = DBEdit11
      end
      object Label15: TLabel
        Left = 8
        Top = 55
        Width = 32
        Height = 13
        Caption = 'Presso'
        FocusControl = DBEdit12
      end
      object Label16: TLabel
        Left = 211
        Top = 55
        Width = 19
        Height = 13
        Caption = 'Cap'
        FocusControl = DBEdit13
      end
      object Label17: TLabel
        Left = 267
        Top = 55
        Width = 39
        Height = 13
        Caption = 'Comune'
        FocusControl = DBEdit14
      end
      object Label18: TLabel
        Left = 8
        Top = 96
        Width = 41
        Height = 13
        Caption = 'Frazione'
        FocusControl = DBEdit15
      end
      object Label19: TLabel
        Left = 548
        Top = 56
        Width = 26
        Height = 13
        Caption = 'Prov.'
        FocusControl = DBEdit16
      end
      object Label20: TLabel
        Left = 197
        Top = 96
        Width = 38
        Height = 13
        Caption = 'Nazione'
        FocusControl = DBEdit17
      end
      object BAnagSelNazione1: TSpeedButton
        Left = 294
        Top = 110
        Width = 27
        Height = 23
        Hint = 'seleziona la nazione'
        Flat = True
        Glyph.Data = {
          36050000424D3605000000000000360400002800000010000000100000000100
          08000000000000010000C40E0000C40E000000010000000100009B3500009C36
          0000A23C0000A33D0000A43D0000A7410000A846000092470000B14B0000B650
          0000B8520000BE570000C15A0000C1610000C8620000CA64000001670000CE68
          000000690000BC6900000F6B0000266C0000056D000000710000397300007775
          00006A7A0000417B0000E17B00002A7D0000007E000029810000E8810000C882
          0000EA8300003F85000000860000608C0000F48D0000928F0000F58F0000FC96
          0000FE980000B1830100739201004D5C0300CC670300D97403009C360400FF9B
          04000E6607009F380800FE9D0900A03A0B00923D0B00D09C0C00A19F0E00A33C
          0F00B64F0F006D5C0F00BC970F00709F1000864511003B621300A2A11300C689
          15007D52160031661800FFA21800A3441D0061651E0099981E00FFA62100D57D
          23005C892300F59D2300B14B2400C0672500E3972500CD89260094432800894C
          2800B2522800886629004FA8290080582A00C9822C00C061300077473100C25E
          3300B65535006C663600A7B63A00FFB73B00C55F4100FFB34200C65F4300F6AD
          4600BF624700CC99490024BE49009E634A004D4D4D0052C15000E1C3550083C1
          5600BAC55800F9C558002CC55900E2AB5A00C2985B004DC35B008A735D00EAB3
          5E00C1BA5E00FFBF5F00B7BE61007F6E6200666666003DCF6C00B9C16D00FAC2
          710056D47500D9B47900EABE7B007DD07C00A1C47F00FBC88000D7C3830077D9
          8600EFC98700BBDD8A00A8DE8B00B6E08E00FFE08F00B0D79500A0E4A500FFDC
          A600CDEEAA00AFE9B300C5E6B800F9E7C100DDF8CC00FAF2DF00EBF9E000FCFE
          FB00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000929266666666
          92929292929292929292924E4F6D6D6E6666666666929292929292447592664D
          59626057526666929292924870663A35363E30394C5E45669292925F56050104
          0632070200335A50669292736105090C0D17140B080355655892929273490F11
          1A241D130E0A3F5B516692922E792F2B272C221C19171043426692922E7F743D
          38312A28211E152D3B6692924147896F67685D34262318124666929241547D8D
          817A865D29251B1653669292926477888F908A6B40221F4A6692929292695C84
          8E918B6C372041669263929292926A6A85838C873C4B66929263929292929292
          787E7282807B7676636392929292929292929292927C7C716392}
        ParentShowHint = False
        ShowHint = True
        OnClick = BAnagSelNazione1Click
      end
      object DBEdit9: TDBEdit
        Left = 91
        Top = 32
        Width = 285
        Height = 21
        DataField = 'Indirizzo'
        DataSource = StoricoAnagraficaForm.DSStoricoAnag
        TabOrder = 1
      end
      object DBEdit10: TDBEdit
        Left = 380
        Top = 32
        Width = 64
        Height = 21
        DataField = 'NumCivico'
        DataSource = StoricoAnagraficaForm.DSStoricoAnag
        TabOrder = 2
      end
      object DBEdit11: TDBEdit
        Left = 448
        Top = 32
        Width = 190
        Height = 21
        DataField = 'Edificio'
        DataSource = StoricoAnagraficaForm.DSStoricoAnag
        TabOrder = 3
      end
      object DBEdit12: TDBEdit
        Left = 8
        Top = 71
        Width = 200
        Height = 21
        DataField = 'Presso'
        DataSource = StoricoAnagraficaForm.DSStoricoAnag
        TabOrder = 4
      end
      object DBEdit13: TDBEdit
        Left = 211
        Top = 71
        Width = 53
        Height = 21
        DataField = 'Cap'
        DataSource = StoricoAnagraficaForm.DSStoricoAnag
        TabOrder = 5
      end
      object DBEdit14: TDBEdit
        Left = 267
        Top = 71
        Width = 278
        Height = 21
        DataField = 'Comune'
        DataSource = StoricoAnagraficaForm.DSStoricoAnag
        TabOrder = 6
      end
      object DBEdit15: TDBEdit
        Left = 8
        Top = 112
        Width = 185
        Height = 21
        DataField = 'Frazione'
        DataSource = StoricoAnagraficaForm.DSStoricoAnag
        TabOrder = 7
      end
      object DBEdit16: TDBEdit
        Left = 548
        Top = 72
        Width = 34
        Height = 21
        DataField = 'Provincia'
        DataSource = StoricoAnagraficaForm.DSStoricoAnag
        TabOrder = 8
      end
      object DBEdit17: TDBEdit
        Left = 197
        Top = 112
        Width = 92
        Height = 21
        DataField = 'Stato'
        DataSource = StoricoAnagraficaForm.DSStoricoAnag
        TabOrder = 9
      end
      object DBComboBox1: TDBComboBox
        Left = 8
        Top = 32
        Width = 79
        Height = 21
        DataField = 'TipoStrada'
        DataSource = StoricoAnagraficaForm.DSStoricoAnag
        ItemHeight = 13
        Items.Strings = (
          'Via'
          'Viale'
          'Piazza'
          'Largo'
          'Corso')
        TabOrder = 0
      end
    end
    object GroupBox3: TGroupBox
      Left = 9
      Top = 304
      Width = 647
      Height = 142
      Caption = 'Domicilio'
      TabOrder = 4
      object Label21: TLabel
        Left = 8
        Top = 16
        Width = 54
        Height = 13
        Caption = 'Tipo strada'
      end
      object Label22: TLabel
        Left = 92
        Top = 16
        Width = 40
        Height = 13
        Caption = 'Indirizzo'
        FocusControl = DBEdit8
      end
      object Label23: TLabel
        Left = 380
        Top = 16
        Width = 40
        Height = 13
        Caption = 'N�Civico'
        FocusControl = DBEdit18
      end
      object Label24: TLabel
        Left = 448
        Top = 16
        Width = 33
        Height = 13
        Caption = 'Edificio'
        FocusControl = DBEdit19
      end
      object Label25: TLabel
        Left = 8
        Top = 55
        Width = 32
        Height = 13
        Caption = 'Presso'
        FocusControl = DBEdit20
      end
      object Label26: TLabel
        Left = 211
        Top = 55
        Width = 19
        Height = 13
        Caption = 'Cap'
        FocusControl = DBEdit21
      end
      object Label27: TLabel
        Left = 267
        Top = 55
        Width = 39
        Height = 13
        Caption = 'Comune'
        FocusControl = DBEdit22
      end
      object Label28: TLabel
        Left = 8
        Top = 96
        Width = 41
        Height = 13
        Caption = 'Frazione'
        FocusControl = DBEdit23
      end
      object Label29: TLabel
        Left = 548
        Top = 56
        Width = 26
        Height = 13
        Caption = 'Prov.'
        FocusControl = DBEdit24
      end
      object Label30: TLabel
        Left = 197
        Top = 96
        Width = 38
        Height = 13
        Caption = 'Nazione'
        FocusControl = DBEdit25
      end
      object SpeedButton1: TSpeedButton
        Left = 294
        Top = 110
        Width = 27
        Height = 23
        Hint = 'seleziona la nazione'
        Flat = True
        Glyph.Data = {
          36050000424D3605000000000000360400002800000010000000100000000100
          08000000000000010000C40E0000C40E000000010000000100009B3500009C36
          0000A23C0000A33D0000A43D0000A7410000A846000092470000B14B0000B650
          0000B8520000BE570000C15A0000C1610000C8620000CA64000001670000CE68
          000000690000BC6900000F6B0000266C0000056D000000710000397300007775
          00006A7A0000417B0000E17B00002A7D0000007E000029810000E8810000C882
          0000EA8300003F85000000860000608C0000F48D0000928F0000F58F0000FC96
          0000FE980000B1830100739201004D5C0300CC670300D97403009C360400FF9B
          04000E6607009F380800FE9D0900A03A0B00923D0B00D09C0C00A19F0E00A33C
          0F00B64F0F006D5C0F00BC970F00709F1000864511003B621300A2A11300C689
          15007D52160031661800FFA21800A3441D0061651E0099981E00FFA62100D57D
          23005C892300F59D2300B14B2400C0672500E3972500CD89260094432800894C
          2800B2522800886629004FA8290080582A00C9822C00C061300077473100C25E
          3300B65535006C663600A7B63A00FFB73B00C55F4100FFB34200C65F4300F6AD
          4600BF624700CC99490024BE49009E634A004D4D4D0052C15000E1C3550083C1
          5600BAC55800F9C558002CC55900E2AB5A00C2985B004DC35B008A735D00EAB3
          5E00C1BA5E00FFBF5F00B7BE61007F6E6200666666003DCF6C00B9C16D00FAC2
          710056D47500D9B47900EABE7B007DD07C00A1C47F00FBC88000D7C3830077D9
          8600EFC98700BBDD8A00A8DE8B00B6E08E00FFE08F00B0D79500A0E4A500FFDC
          A600CDEEAA00AFE9B300C5E6B800F9E7C100DDF8CC00FAF2DF00EBF9E000FCFE
          FB00000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          0000000000000000000000000000000000000000000000000000929266666666
          92929292929292929292924E4F6D6D6E6666666666929292929292447592664D
          59626057526666929292924870663A35363E30394C5E45669292925F56050104
          0632070200335A50669292736105090C0D17140B080355655892929273490F11
          1A241D130E0A3F5B516692922E792F2B272C221C19171043426692922E7F743D
          38312A28211E152D3B6692924147896F67685D34262318124666929241547D8D
          817A865D29251B1653669292926477888F908A6B40221F4A6692929292695C84
          8E918B6C372041669263929292926A6A85838C873C4B66929263929292929292
          787E7282807B7676636392929292929292929292927C7C716392}
        ParentShowHint = False
        ShowHint = True
        OnClick = SpeedButton1Click
      end
      object DBEdit8: TDBEdit
        Left = 91
        Top = 32
        Width = 285
        Height = 21
        DataField = 'DomicilioIndirizzo'
        DataSource = StoricoAnagraficaForm.DSStoricoAnag
        TabOrder = 1
      end
      object DBEdit18: TDBEdit
        Left = 380
        Top = 32
        Width = 64
        Height = 21
        DataField = 'NumCivico'
        DataSource = StoricoAnagraficaForm.DSStoricoAnag
        TabOrder = 2
      end
      object DBEdit19: TDBEdit
        Left = 448
        Top = 32
        Width = 190
        Height = 21
        DataField = 'Edificio'
        DataSource = StoricoAnagraficaForm.DSStoricoAnag
        TabOrder = 3
      end
      object DBEdit20: TDBEdit
        Left = 8
        Top = 71
        Width = 200
        Height = 21
        DataField = 'DomicilioPresso'
        DataSource = StoricoAnagraficaForm.DSStoricoAnag
        TabOrder = 4
      end
      object DBEdit21: TDBEdit
        Left = 211
        Top = 71
        Width = 53
        Height = 21
        DataField = 'DomicilioCap'
        DataSource = StoricoAnagraficaForm.DSStoricoAnag
        TabOrder = 5
      end
      object DBEdit22: TDBEdit
        Left = 267
        Top = 71
        Width = 278
        Height = 21
        DataField = 'Comune'
        DataSource = StoricoAnagraficaForm.DSStoricoAnag
        TabOrder = 6
      end
      object DBEdit23: TDBEdit
        Left = 8
        Top = 112
        Width = 185
        Height = 21
        DataField = 'DomicilioFrazione'
        DataSource = StoricoAnagraficaForm.DSStoricoAnag
        TabOrder = 7
      end
      object DBEdit24: TDBEdit
        Left = 548
        Top = 72
        Width = 34
        Height = 21
        DataField = 'Provincia'
        DataSource = StoricoAnagraficaForm.DSStoricoAnag
        TabOrder = 8
      end
      object DBEdit25: TDBEdit
        Left = 197
        Top = 112
        Width = 92
        Height = 21
        DataField = 'DomicilioStato'
        DataSource = StoricoAnagraficaForm.DSStoricoAnag
        TabOrder = 9
      end
      object DBComboBox2: TDBComboBox
        Left = 8
        Top = 32
        Width = 79
        Height = 21
        DataField = 'DomicilioTipoStrada'
        DataSource = StoricoAnagraficaForm.DSStoricoAnag
        ItemHeight = 13
        Items.Strings = (
          'Via'
          'Viale'
          'Piazza'
          'Largo'
          'Corso')
        TabOrder = 0
      end
    end
    object GroupBox4: TGroupBox
      Left = 9
      Top = 450
      Width = 647
      Height = 102
      Caption = 'Recapiti telefonici'
      TabOrder = 5
      object Label31: TLabel
        Left = 8
        Top = 16
        Width = 42
        Height = 13
        Caption = 'Telefono'
        FocusControl = DBEdit26
      end
      object Label32: TLabel
        Left = 168
        Top = 16
        Width = 41
        Height = 13
        Caption = 'Cellulare'
        FocusControl = DBEdit27
      end
      object Label33: TLabel
        Left = 336
        Top = 16
        Width = 18
        Height = 13
        Caption = 'Fax'
        FocusControl = DBEdit28
      end
      object Label34: TLabel
        Left = 479
        Top = 16
        Width = 74
        Height = 13
        Caption = 'Telefono ufficio'
        FocusControl = DBEdit29
      end
      object Label35: TLabel
        Left = 8
        Top = 57
        Width = 24
        Height = 13
        Caption = 'Email'
        FocusControl = DBEdit30
      end
      object Label36: TLabel
        Left = 289
        Top = 56
        Width = 56
        Height = 13
        Caption = 'Email ufficio'
        FocusControl = DBEdit31
      end
      object Label37: TLabel
        Left = 517
        Top = 56
        Width = 72
        Height = 13
        Caption = 'Casella postale'
        FocusControl = DBEdit32
      end
      object DBEdit26: TDBEdit
        Left = 8
        Top = 32
        Width = 157
        Height = 21
        DataField = 'RecapitiTelefonici'
        DataSource = StoricoAnagraficaForm.DSStoricoAnag
        TabOrder = 0
      end
      object DBEdit27: TDBEdit
        Left = 168
        Top = 32
        Width = 165
        Height = 21
        DataField = 'Cellulare'
        DataSource = StoricoAnagraficaForm.DSStoricoAnag
        TabOrder = 1
      end
      object DBEdit28: TDBEdit
        Left = 336
        Top = 32
        Width = 140
        Height = 21
        DataField = 'Fax'
        DataSource = StoricoAnagraficaForm.DSStoricoAnag
        TabOrder = 2
      end
      object DBEdit29: TDBEdit
        Left = 479
        Top = 32
        Width = 158
        Height = 21
        DataField = 'TelUfficio'
        DataSource = StoricoAnagraficaForm.DSStoricoAnag
        TabOrder = 3
      end
      object DBEdit30: TDBEdit
        Left = 8
        Top = 73
        Width = 277
        Height = 21
        DataField = 'Email'
        DataSource = StoricoAnagraficaForm.DSStoricoAnag
        TabOrder = 4
      end
      object DBEdit31: TDBEdit
        Left = 288
        Top = 73
        Width = 226
        Height = 21
        DataField = 'EmailUfficio'
        DataSource = StoricoAnagraficaForm.DSStoricoAnag
        TabOrder = 5
      end
      object DBEdit32: TDBEdit
        Left = 517
        Top = 72
        Width = 120
        Height = 21
        DataField = 'CasellaPostale'
        DataSource = StoricoAnagraficaForm.DSStoricoAnag
        TabOrder = 6
      end
    end
    object GroupBox5: TGroupBox
      Left = 9
      Top = 555
      Width = 647
      Height = 87
      Caption = 'Documenti di identit�'
      TabOrder = 6
      object Label38: TLabel
        Left = 9
        Top = 35
        Width = 91
        Height = 13
        Caption = 'Carta d'#39'identit�:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label39: TLabel
        Left = 9
        Top = 60
        Width = 67
        Height = 13
        Caption = 'Passaporto:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label40: TLabel
        Left = 106
        Top = 16
        Width = 37
        Height = 13
        Caption = 'Numero'
        FocusControl = DBEdit33
      end
      object Label41: TLabel
        Left = 228
        Top = 16
        Width = 22
        Height = 13
        Caption = 'Ente'
        FocusControl = DBEdit34
      end
      object Label42: TLabel
        Left = 413
        Top = 16
        Width = 41
        Height = 13
        Caption = 'Data Ril.'
      end
      object Label43: TLabel
        Left = 528
        Top = 41
        Width = 46
        Height = 13
        Caption = 'Scadenza'
      end
      object DBEdit33: TDBEdit
        Left = 106
        Top = 32
        Width = 119
        Height = 21
        DataField = 'CartaIdentitaNum'
        DataSource = StoricoAnagraficaForm.DSStoricoAnag
        TabOrder = 0
      end
      object DBEdit34: TDBEdit
        Left = 228
        Top = 32
        Width = 182
        Height = 21
        DataField = 'CartaIdentitaEnte'
        DataSource = StoricoAnagraficaForm.DSStoricoAnag
        TabOrder = 1
      end
      object DBEdit35: TDBEdit
        Left = 106
        Top = 57
        Width = 119
        Height = 21
        DataField = 'PassaportoNum'
        DataSource = StoricoAnagraficaForm.DSStoricoAnag
        TabOrder = 3
      end
      object DBEdit36: TDBEdit
        Left = 228
        Top = 56
        Width = 182
        Height = 21
        DataField = 'PassaportoEnte'
        DataSource = StoricoAnagraficaForm.DSStoricoAnag
        TabOrder = 4
      end
      object dxDBDateEdit2: TdxDBDateEdit
        Left = 414
        Top = 32
        Width = 109
        TabOrder = 2
        DataField = 'CartaIdentitaDataRil'
        DataSource = StoricoAnagraficaForm.DSStoricoAnag
      end
      object dxDBDateEdit3: TdxDBDateEdit
        Left = 414
        Top = 56
        Width = 109
        TabOrder = 5
        DataField = 'PassaportoDataRil'
        DataSource = StoricoAnagraficaForm.DSStoricoAnag
      end
      object dxDBDateEdit4: TdxDBDateEdit
        Left = 527
        Top = 56
        Width = 109
        TabOrder = 6
        DataField = 'PassaportoScad'
        DataSource = StoricoAnagraficaForm.DSStoricoAnag
      end
    end
    object DBDateDallaData: TdxDBDateEdit
      Left = 542
      Top = 24
      Width = 112
      TabOrder = 1
      DataField = 'DallaData'
      DataSource = StoricoAnagraficaForm.DSStoricoAnag
    end
  end
  object Q: TADOQuery
    Connection = Data.DB
    Parameters = <>
    Left = 592
    Top = 8
  end
end
