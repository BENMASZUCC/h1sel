//[TONI20021003]DEBUGOK
unit TipiMailCand;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     TB97, StdCtrls, Buttons, DBCtrls, Mask, Db, ExtCtrls, Grids, DBGrids,
     DBTables, ADODB, U_ADOLinkCl;

type
     TTipiMailCandForm = class(TForm)
          DsQTipiCand: TDataSource;
          DBGrid1: TDBGrid;
          Panel1: TPanel;
          Label1: TLabel;
          DBEdit1: TDBEdit;
          Label2: TLabel;
          DBMemo1: TDBMemo;
          Label3: TLabel;
          DBMemo2: TDBMemo;
          Label4: TLabel;
          DBEdit2: TDBEdit;
          DBCheckBox1: TDBCheckBox;
          Panel2: TPanel;
          BNew: TToolbarButton97;
          Bdel: TToolbarButton97;
          BOK: TToolbarButton97;
          BCan: TToolbarButton97;
          DBCheckBox2: TDBCheckBox;
          QTipiCand: TADOLinkedQuery;
          DBCheckBox3: TDBCheckBox;
          QTipiCli: TADOLinkedQuery;
          dsQTipiCli: TDataSource;
          Panel3: TPanel;
          BCliNew: TToolbarButton97;
          BCliDel: TToolbarButton97;
          BCliOK: TToolbarButton97;
          BCliCan: TToolbarButton97;
    Panel4: TPanel;
    BitBtn1: TToolbarButton97;
          procedure DsQTipiCandStateChange(Sender: TObject);
          procedure BOKClick(Sender: TObject);
          procedure BCanClick(Sender: TObject);
          procedure BNewClick(Sender: TObject);
          procedure BdelClick(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure BCliNewClick(Sender: TObject);
          procedure BCliDelClick(Sender: TObject);
          procedure BCliOKClick(Sender: TObject);
          procedure BCliCanClick(Sender: TObject);
          procedure dsQTipiCliStateChange(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     TipiMailCandForm: TTipiMailCandForm;

implementation

uses ModuloDati, Main;

{$R *.DFM}
//[TONI20021003]DEBUGOK

procedure TTipiMailCandForm.DsQTipiCandStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsQTipiCand.State in [dsEdit, dsInsert];
     BNew.Enabled := not b;
     BDel.Enabled := not b;
     BOK.Enabled := b;
     BCan.Enabled := b;
end;

//[TONI20021003]DEBUGOK

procedure TTipiMailCandForm.BOKClick(Sender: TObject);
begin
     with QTipiCand do begin
          Data.DB.BeginTrans;
          try
               //               ApplyUpdates;
               Post;
               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               raise;
          end;
          //          CommitUpdates;
     end;
end;

//[TONI20021003]DEBUGOK

procedure TTipiMailCandForm.BCanClick(Sender: TObject);
begin
     QTipiCand.Cancel;
end;

//[TONI20021003]DEBUGOK

procedure TTipiMailCandForm.BNewClick(Sender: TObject);
var xDesc: string;
begin
     if InputQuery('Nuovo tipo di mail', 'Descrizione:', xDesc) then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text := 'insert into TipiMailCand (Descrizione) ' +
                         'values (:xDescrizione:)';
                    //[ALBERTO 20020902]

                    Q1.ParamByName['xDescrizione'] := xDesc;

                    //[ALBERTO 20020902]FINE
                    Q1.ExecSQL;
                    DB.CommitTrans;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               end;
          end;
          QTipiCand.Close;
          QTipiCand.Open;
          QTipiCand.Last;
     end;
end;
//[TONI20021003]DEBUGOK

procedure TTipiMailCandForm.BdelClick(Sender: TObject);
begin
     if MessageDlg('Sei sicuro di voler eliminare questo tipo ?', mtWarning, [mbYes, mbNo], 0) = mrNo then Exit;
     with Data do begin
          DB.BeginTrans;
          try
               Q1.Close;
               Q1.SQL.text := 'delete from TipiMailCand where ID=' + QTipiCand.FieldByName('ID').asString;
               Q1.ExecSQL;
               DB.CommitTrans;
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
          end;
     end;
     QTipiCand.Close;
     QTipiCand.Open;
end;
//[TONI20021003]DEBUGOK

procedure TTipiMailCandForm.FormShow(Sender: TObject);
begin
     //Grafica
     TipiMailCandForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel3.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel3.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel3.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel4.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel4.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel4.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     Caption := '[S/43] - ' + Caption;
end;

procedure TTipiMailCandForm.BCliNewClick(Sender: TObject);
var xDesc: string;
begin
     if InputQuery('Nuovo tipo di mail', 'Descrizione:', xDesc) then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text := 'insert into TipiMailContattiCli (Descrizione) ' +
                         'values (:xDescrizione:)';
                    //[ALBERTO 20020902]

                    Q1.ParamByName['xDescrizione'] := xDesc;

                    //[ALBERTO 20020902]FINE
                    Q1.ExecSQL;
                    DB.CommitTrans;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               end;
          end;
          QTipiCli.Close;
          QTipiCli.Open;
          QTipiCli.Last;
     end;
end;

procedure TTipiMailCandForm.BCliDelClick(Sender: TObject);
begin
     if MessageDlg('Sei sicuro di voler eliminare questo tipo ?', mtWarning, [mbYes, mbNo], 0) = mrNo then Exit;
     with Data do begin
          DB.BeginTrans;
          try
               Q1.Close;
               Q1.SQL.text := 'delete from tipiMailContattiCLi where ID=' + QTipiCli.FieldByName('ID').asString;
               Q1.ExecSQL;
               DB.CommitTrans;
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
          end;
     end;
     QTipiCli.Close;
     QTipiCli.Open;
end;

procedure TTipiMailCandForm.BCliOKClick(Sender: TObject);
begin
     with QTipiCli do begin
          Data.DB.BeginTrans;
          try
               //               ApplyUpdates;
               Post;
               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               raise;
          end;
          //          CommitUpdates;
     end;
end;

procedure TTipiMailCandForm.BCliCanClick(Sender: TObject);
begin
     QTipiCli.Cancel;
end;

procedure TTipiMailCandForm.dsQTipiCliStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsQTipiCli.State in [dsEdit, dsInsert];
     BCliNew.Enabled := not b;
     BCliDel.Enabled := not b;
     BCliOK.Enabled := b;
     BCliCan.Enabled := b;
end;

procedure TTipiMailCandForm.BitBtn1Click(Sender: TObject);
begin
      TipiMailCandForm.ModalResult:=mrOK;
end;

end.
