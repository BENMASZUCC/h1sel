unit Lingue;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, TB97, Grids, DBGrids, Db, DBTables, ExtCtrls, Wall, Buttons,
     ADODB, U_ADOLinkCl, dxTL, dxDBCtrl, dxDBGrid, dxCntner;

type
     TLingueForm = class(TForm)
          DsLingue: TDataSource;
          Panel1: TPanel;
          Panel2: TPanel;
          Label2: TLabel;
          DsQLivLingue: TDataSource;
          DBGrid2: TDBGrid;
          Label3: TLabel;
          ESoggiorno: TEdit;
          Label4: TLabel;
          DBGrid3: TDBGrid;
          DsQLivLingue2: TDataSource;
          QLivLingue: TADOLinkedQuery;
          QLivLingue2: TADOLinkedQuery;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1Lingua: TdxDBGridMaskColumn;
          BitBtn1: TToolbarButton97;
          BAnnulla: TToolbarButton97;
          BITA: TToolbarButton97;
          BENG: TToolbarButton97;
          ECerca: TEdit;
          Label1: TLabel;
          Lingue: TADOQuery;
          Lingue_Old: TADOLinkedTable;
    Label5: TLabel;
    DBGrid1: TDBGrid;
    QLivLingue3: TADOLinkedQuery;
    DsQLivLingue3: TDataSource;
          procedure FormShow(Sender: TObject);
          procedure BITAClick(Sender: TObject);
          procedure BENGClick(Sender: TObject);
          procedure BitBtn1Click(Sender: TObject);
          procedure BAnnullaClick(Sender: TObject);
          procedure ECercaChange(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     LingueForm: TLingueForm;

implementation

uses Curriculum, ModuloDati, Main;

{$R *.DFM}

procedure TLingueForm.FormShow(Sender: TObject);
begin
     //Grafica
     LingueForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     Caption := '[S/50] - ' + Caption;
     {     if (Uppercase(copy(Data.GlobalNomeAzienda.Value,1,7))='FERRARI') or
             (Uppercase(copy(Data.GlobalNomeAzienda.Value,1,3))='EBC') then begin}
     if (Uppercase(copy(Data.Global.FieldByName('NomeAzienda').Value, 1, 7)) = 'FERRARI') or
          (Uppercase(copy(Data.Global.FieldByname('NomeAzienda').Value, 1, 3)) = 'EBC') then begin
          BITA.visible := True;
          BENG.visible := True;
     end;

    
end;

procedure TLingueForm.BITAClick(Sender: TObject);
begin
     dxDBGrid1Lingua.FieldName := 'Lingua';
     dxDBGrid1Lingua.Caption := 'Lingua';
     DBGrid2.Columns[0].FieldName := 'LivelloConoscenza';
     DBGrid3.Columns[0].FieldName := 'LivelloConoscenza';
end;

procedure TLingueForm.BENGClick(Sender: TObject);
begin
     dxDBGrid1Lingua.FieldName := 'Lingua_ENG';
     dxDBGrid1Lingua.Caption := 'Lingua (ENG)';
     DBGrid2.Columns[0].FieldName := 'LivelloConoscenza_ENG';
     DBGrid3.Columns[0].FieldName := 'LivelloConoscenza_ENG';
end;


procedure TLingueForm.BitBtn1Click(Sender: TObject);
begin
     LingueForm.ModalResult := mrOk;
end;

procedure TLingueForm.BAnnullaClick(Sender: TObject);
begin
     LingueForm.ModalResult := mrCancel;
end;

procedure TLingueForm.ECercaChange(Sender: TObject);
begin
     Lingue.Close;
     Lingue.SQL.Text := 'select * from Lingue' +      
          ' where Lingua like :xCerca' +
          ' order by Lingua';
     Lingue.Parameters.ParamByName('xCerca').Value := '%' + ECerca.text + '%';
     Lingue.Open;
end;

end.

