//[TONI20021004]DEBUGOK
unit ElencoSedi;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Grids, DBGrids, Db, DBTables, StdCtrls, Buttons, ADODB, U_ADOLinkCl,
  TB97, ExtCtrls;

type
     TElencoSediForm = class(TForm)
          DsQSedi: TDataSource;
          DBGrid1: TDBGrid;
          QSedi: TADOLinkedQuery;
          QSediID: TAutoIncField;
          QSediDescrizione: TStringField;
          QSediIndirizzo: TStringField;
          QSediCap: TStringField;
          QSediComune: TStringField;
          QSediProvincia: TStringField;
          QSediTipoStrada: TStringField;
          QSediNumCivico: TStringField;
          Panel1: TPanel;
          BOk: TToolbarButton97;
          BAnnulla: TToolbarButton97;
          procedure FormShow(Sender: TObject);
          procedure BOkClick(Sender: TObject);
          procedure BAnnullaClick(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     ElencoSediForm: TElencoSediForm;

implementation

//[/TONI20020726\]
uses ModuloDati, Main;
//[/TONI20020726\]FINE

{$R *.DFM}

procedure TElencoSediForm.FormShow(Sender: TObject);
begin
     Caption := '[S/10] - ' + caption;
     ElencoSediForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
end;

procedure TElencoSediForm.BOkClick(Sender: TObject);
begin
     ElencoSediForm.ModalResult := mrOk;
end;

procedure TElencoSediForm.BAnnullaClick(Sender: TObject);
begin
     ElencoSediForm.ModalResult := mrCancel;
end;

end.

