unit RelazCand_config;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     dxDBTLCl, dxGrClms, dxTL, dxDBCtrl, dxDBGrid, dxCntner, Db, ADODB,
     StdCtrls, Buttons, ExtCtrls, TB97;

type
     TRelazCand_configForm = class(TForm)
          Panel1: TPanel;
          QTab: TADOQuery;
          DsQTab: TDataSource;
          QTabID: TAutoIncField;
          QTabDicNodoMaster: TStringField;
          QTabOrdine: TSmallintField;
          QTabInvisibile: TBooleanField;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1ID: TdxDBGridMaskColumn;
          dxDBGrid1DicNodoMaster: TdxDBGridMaskColumn;
          dxDBGrid1Ordine: TdxDBGridSpinColumn;
          dxDBGrid1Invisibile: TdxDBGridCheckColumn;
          BitBtn1: TToolbarButton97;
          procedure FormShow(Sender: TObject);
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
          procedure BitBtn1Click(Sender: TObject);
     private
          { Private declarations }
     public
          xTipo: string;
     end;

var
     RelazCand_configForm: TRelazCand_configForm;

implementation

uses ModuloDati, Main;

{$R *.DFM}

procedure TRelazCand_configForm.FormShow(Sender: TObject);
begin
     QTab.SQL.text := 'select * from AnagSintesiModel where Applicazione= ''H1Sel'' and  Tipo=:x';
      QTab.Parameters[0].value:=xTipo;
     QTab.Open;

     RelazCand_configForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
end;

procedure TRelazCand_configForm.FormClose(Sender: TObject;
     var Action: TCloseAction);
begin
     if DsQTab.state = dsEdit then QTab.Post;
end;

procedure TRelazCand_configForm.BitBtn1Click(Sender: TObject);
begin
     RelazCand_configForm.ModalResult := mrOk;
end;

end.

