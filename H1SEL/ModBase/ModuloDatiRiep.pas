unit ModuloDatiRiep;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ADODB;

type
  TDatiRiep = class(TDataModule)
    QCVInseriti: TADOQuery;
    QAzIns: TADOQuery;
    ClientiAtt: TADOQuery;
    Fatturato: TADOQuery;
    NrFatture: TADOQuery;
    Fatturatoanno: TIntegerField;
    Fatturatonumero: TFloatField;
    NrFatturenumero: TIntegerField;
    NrFattureanno: TIntegerField;
    QCVTot: TADOQuery;
    QAzTot: TADOQuery;
    QAzAttTot: TADOQuery;
    QAzAttTottot: TIntegerField;
    QFatt: TADOQuery;
    QFatttot: TFloatField;
    QNrFatt: TADOQuery;
    QNrFatttot: TIntegerField;
    DSCVTot: TDataSource;
    DSQAzTot: TDataSource;
    DSQAzAttTot: TDataSource;
    DSFatt: TDataSource;
    DSQNrFatt: TDataSource;
    QAzTottot: TIntegerField;
    QCVTottot: TIntegerField;
    QFattAnnoScorso: TADOQuery;
    DSFattAS: TDataSource;
    QNRFattAS: TADOQuery;
    DSNrFattAS: TDataSource;
    QNrColl: TADOQuery;
    DSNrColl: TDataSource;
    QNrColltot: TIntegerField;
    QNrCollAS: TADOQuery;
    IntegerField3: TIntegerField;
    DSQNrCollAS: TDataSource;
    QFattAnnoScorsotot: TFloatField;
    QNRFattAStot: TIntegerField;
    QNrPres: TADOQuery;
    IntegerField1: TIntegerField;
    DSNrPres: TDataSource;
    QNrPressAS: TADOQuery;
    IntegerField2: TIntegerField;
    DSNrPressAS: TDataSource;
    QNROff: TADOQuery;
    IntegerField4: TIntegerField;
    DSNROff: TDataSource;
    DSNrOffAS: TDataSource;
    QNrOffAS: TADOQuery;
    IntegerField5: TIntegerField;
    QCVInseritinumero: TIntegerField;
    QCVInseritinrmese: TIntegerField;
    QCVInseritimese: TIntegerField;
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DatiRiep: TDatiRiep;

implementation

uses Modulodati;
{$R *.DFM}

procedure TDatiRiep.DataModuleDestroy(Sender: TObject);
var
  lauf : WORD;
begin
  for lauf :=0 to  DatiRiep.ComponentCount-1 do
  begin
    if (DatiRiep.Components[lauf] is TADOQuery) or (DatiRiep.Components[lauf] is TADOTable)  then
      TADODataSet (DatiRiep.Components[lauf]).Close;
//    DatiRiep.Components[0].Free;
  end;
end;

end.
