unit StoricoAnagrafica;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, ADODB, ExtCtrls, dxCntner,  dxDBCtrl, dxDBGrid, TB97, dxDBTLCl,
  dxGrClms, dxTL;

type
     TStoricoAnagraficaForm = class(TForm)
          Panel1: TPanel;
          Panel2: TPanel;
          QStoricoAnag: TADOQuery;
          DSStoricoAnag: TDataSource;
          QStoricoAnagID: TAutoIncField;
          QStoricoAnagIDAnagrafica: TIntegerField;
          QStoricoAnagidazienda: TIntegerField;
          QStoricoAnagCodiceFiscale: TStringField;
          QStoricoAnagCognome: TStringField;
          QStoricoAnagNome: TStringField;
          QStoricoAnagMatricola: TStringField;
          QStoricoAnagCID: TStringField;
          QStoricoAnagCodicePaghe: TStringField;
          QStoricoAnagCodiceBadge: TStringField;
          QStoricoAnagSesso: TStringField;
          QStoricoAnagDataNascita: TDateTimeField;
          QStoricoAnagLuogoNascita: TStringField;
          QStoricoAnagProvNascita: TStringField;
          QStoricoAnagStatoNascita: TStringField;
          QStoricoAnagIndirizzo: TStringField;
          QStoricoAnagEdificio: TStringField;
          QStoricoAnagPresso: TStringField;
          QStoricoAnagCap: TStringField;
          QStoricoAnagFrazione: TStringField;
          QStoricoAnagComune: TStringField;
          QStoricoAnagProvincia: TStringField;
          QStoricoAnagStato: TStringField;
          QStoricoAnagDomicilioIndirizzo: TStringField;
          QStoricoAnagDomicilioEdificio: TStringField;
          QStoricoAnagDomicilioPresso: TStringField;
          QStoricoAnagDomicilioCap: TStringField;
          QStoricoAnagDomicilioFrazione: TStringField;
          QStoricoAnagDomicilioComune: TStringField;
          QStoricoAnagDomicilioProvincia: TStringField;
          QStoricoAnagDomicilioStato: TStringField;
          QStoricoAnagNazionalita: TStringField;
          QStoricoAnagIDStatoCivile: TIntegerField;
          QStoricoAnagIDDIploma: TIntegerField;
          QStoricoAnagRecapitiTelefonici: TStringField;
          QStoricoAnagCellulare: TStringField;
          QStoricoAnagTelUfficio: TStringField;
          QStoricoAnagEmail: TStringField;
          QStoricoAnagDallaData: TDateTimeField;
          QStoricoAnagIdStato: TIntegerField;
          QStoricoAnagIDTipoStato: TIntegerField;
          QStoricoAnagAzienda: TStringField;
          QStoricoAnagStatoCivile: TStringField;
          QStoricoAnagDiploma: TStringField;
          QStoricoAnagCasellaPostale: TStringField;
          QStoricoAnagEmailUfficio: TStringField;
          QStoricoAnagFax: TStringField;
          QStoricoAnagCartaIdentitaNum: TStringField;
          QStoricoAnagCartaIdentitaEnte: TStringField;
          QStoricoAnagCartaIdentitaDataRil: TDateTimeField;
          QStoricoAnagPassaportoNum: TStringField;
          QStoricoAnagPassaportoDataRil: TDateTimeField;
          QStoricoAnagPassaportoEnte: TStringField;
          QStoricoAnagPassaportoScad: TDateTimeField;
          QStoricoAnagComunitaNazionale: TStringField;
          QStoricoAnagTipoStrada: TStringField;
          QStoricoAnagNumCivico: TStringField;
          QStoricoAnagDomicilioTipoStrada: TStringField;
          QStoricoAnagDomicilioNumCivico: TStringField;
          QStoricoAnagCancellato: TBooleanField;
          ToolbarButton971: TToolbarButton97;
          BNew: TToolbarButton97;
          BMod: TToolbarButton97;
          BDel: TToolbarButton97;
          QStatiCiviliLK: TADOQuery;
          QStatiCiviliLKID: TAutoIncField;
          QStatiCiviliLKDescrizione: TStringField;
          QStoricoAnagStatoCivileLK: TStringField;
          Q: TADOQuery;
    Qtemp: TADOQuery;
    dxDBGrid1: TdxDBGrid;
    dxDBGrid1CID: TdxDBGridMaskColumn;
    dxDBGrid1DataNascita: TdxDBGridDateColumn;
    dxDBGrid1LuogoNascita: TdxDBGridMaskColumn;
    dxDBGrid1ProvNascita: TdxDBGridMaskColumn;
    dxDBGrid1StatoNascita: TdxDBGridMaskColumn;
    dxDBGrid1Indirizzo: TdxDBGridMaskColumn;
    dxDBGrid1Edificio: TdxDBGridMaskColumn;
    dxDBGrid1Presso: TdxDBGridMaskColumn;
    dxDBGrid1Cap: TdxDBGridMaskColumn;
    dxDBGrid1Frazione: TdxDBGridMaskColumn;
    dxDBGrid1Comune: TdxDBGridMaskColumn;
    dxDBGrid1Provincia: TdxDBGridMaskColumn;
    dxDBGrid1Stato: TdxDBGridMaskColumn;
    dxDBGrid1DomicilioIndirizzo: TdxDBGridMaskColumn;
    dxDBGrid1DomicilioEdificio: TdxDBGridMaskColumn;
    dxDBGrid1DomicilioPresso: TdxDBGridMaskColumn;
    dxDBGrid1DomicilioCap: TdxDBGridMaskColumn;
    dxDBGrid1DomicilioFrazione: TdxDBGridMaskColumn;
    dxDBGrid1DomicilioComune: TdxDBGridMaskColumn;
    dxDBGrid1DomicilioProvincia: TdxDBGridMaskColumn;
    dxDBGrid1DomicilioStato: TdxDBGridMaskColumn;
    dxDBGrid1Nazionalita: TdxDBGridMaskColumn;
    dxDBGrid1RecapitiTelefonici: TdxDBGridMaskColumn;
    dxDBGrid1Cellulare: TdxDBGridMaskColumn;
    dxDBGrid1TelUfficio: TdxDBGridMaskColumn;
    dxDBGrid1Email: TdxDBGridMaskColumn;
    dxDBGrid1DallaData: TdxDBGridDateColumn;
    dxDBGrid1StatoCivile: TdxDBGridMaskColumn;
    dxDBGrid1CasellaPostale: TdxDBGridMaskColumn;
    dxDBGrid1EmailUfficio: TdxDBGridMaskColumn;
    dxDBGrid1CartaIdentitaNum: TdxDBGridMaskColumn;
    dxDBGrid1CartaIdentitaEnte: TdxDBGridMaskColumn;
    dxDBGrid1CartaIdentitaDataRil: TdxDBGridDateColumn;
    dxDBGrid1PassaportoNum: TdxDBGridMaskColumn;
    dxDBGrid1PassaportoDataRil: TdxDBGridDateColumn;
    dxDBGrid1PassaportoEnte: TdxDBGridMaskColumn;
    dxDBGrid1PassaportoScad: TdxDBGridDateColumn;
    dxDBGrid1TipoStrada: TdxDBGridMaskColumn;
    dxDBGrid1NumCivico: TdxDBGridMaskColumn;
    dxDBGrid1DomicilioTipoStrada: TdxDBGridMaskColumn;
    dxDBGrid1DomicilioNumCivico: TdxDBGridMaskColumn;
          procedure FormShow(Sender: TObject);
          procedure ToolbarButton971Click(Sender: TObject);
          procedure BModClick(Sender: TObject);
          procedure BNewClick(Sender: TObject);
          procedure QStoricoAnagBeforePost(DataSet: TDataSet);
          procedure BDelClick(Sender: TObject);
          procedure QStoricoAnagAfterInsert(DataSet: TDataSet);
          procedure QStoricoAnagBeforeOpen(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
     private
    { Private declarations }
          xIDMax: integer;
     public
    { Public declarations }
          xMaxData: TDateTime;
     end;

var
     StoricoAnagraficaForm: TStoricoAnagraficaForm;

implementation

uses ModuloDati, Main,  ModificaStoricoAnagrafica;

{$R *.DFM}

procedure TStoricoAnagraficaForm.FormShow(Sender: TObject);
begin
     // grafica
     Panel1.color := mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.color := mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;

     QStoricoAnag.Open;


     if QStoricoAnag.RecordCount > 0 then begin
          q.Close;
          q.SQL.Text := 'select top 1 ID, DallaData from StoricoAnagrafica where idanagrafica=' + QStoricoAnagIDAnagrafica.AsString +
               ' order by DallaData Desc ';
          q.open;
          xMaxData := q.fieldbyname('DallaData').asDateTime;
          xIdmax := q.fieldbyname('ID').asInteger;
     end;


    //dxDBGrid1DBBandedTableView1.RestoreFromRegistry('\Software\H1', True, false, [], 'DbGridStoricoAnagrafica');


end;

procedure TStoricoAnagraficaForm.ToolbarButton971Click(Sender: TObject);
begin
     StoricoAnagraficaForm.ModalResult := mrCancel;
end;

procedure TStoricoAnagraficaForm.BModClick(Sender: TObject);
var xID: integer;
begin
     //if not MainForm.CheckProfile('101211') then Exit;

     if QStoricoAnagID.AsInteger = xIDMax then begin
          MessageDlg('Non � possibile modificare la riga pi� recente nello storico' + chr(13) + 'Modificare dalla schermata principale', mtInformation, [mbOk], 0);
          exit;
     end;

     ModificaStoricoAnagraficaForm := TModificaStoricoAnagraficaForm.create(self);
     ModificaStoricoAnagraficaForm.xModalita := 'UPDATE';
     ModificaStoricoAnagraficaForm.showmodal;
     if ModificaStoricoAnagraficaForm.ModalResult = mrok then begin
          if DSStoricoAnag.State in [dsEdit, dsInsert] then
               QStoricoAnag.Post;
     end else begin
          QStoricoAnag.cancel;
     end;
     ModificaStoricoAnagraficaForm.free;

     // refresh
     xID := QStoricoAnagID.value;
     QStoricoAnag.Close;
     QStoricoAnag.Open;
     QStoricoAnag.Locate('ID', xID, []);
end;

procedure TStoricoAnagraficaForm.BNewClick(Sender: TObject);
var xID: integer;
begin
     //if not MainForm.CheckProfile('101210') then Exit;
     QStoricoAnag.Insert;

     //BModClick(self);

     ModificaStoricoAnagraficaForm := TModificaStoricoAnagraficaForm.create(self);
     ModificaStoricoAnagraficaForm.xModalita := 'INSERT';
     ModificaStoricoAnagraficaForm.showmodal;
     if ModificaStoricoAnagraficaForm.ModalResult = mrok then begin
          if DSStoricoAnag.State in [dsEdit, dsInsert] then
               QStoricoAnag.Post;
     end else begin
          QStoricoAnag.cancel;
     end;
     ModificaStoricoAnagraficaForm.free;

     // refresh
     xID := QStoricoAnagID.value;
     QStoricoAnag.Close;
     QStoricoAnag.Open;
     QStoricoAnag.Locate('ID', xID, []);
end;

procedure TStoricoAnagraficaForm.QStoricoAnagBeforePost(DataSet: TDataSet);
begin
     QStoricoAnagStatoCivile.value := QStoricoAnagStatoCivileLK.value;
end;

procedure TStoricoAnagraficaForm.BDelClick(Sender: TObject);
var   xUltimoID:integer;
begin
     if QStoricoAnag.IsEmpty then exit;

     // non posso cancellare la riga piu recente se no il dato attuale non coincide piu cn lo storico
     qtemp.close;
     qtemp.sql.text := '  select top 1 ID from  StoricoAnagrafica where idanagrafica=' + Data.TAnagraficaID.AsString + ' order by dalladata desc ';
     qtemp.Open;
     xUltimoID := qtemp.fieldbyname('ID').asInteger;

     if xUltimoID = QStoricoAnagID.AsInteger then begin
          MessageDlg('Il record selezionato corrisponde alla situazione attuale.' + chr(13) + 'Eliminare il dipendente dalla schermata principale', mtWarning, [mbOK], 0);
          exit;
     end;

     if MessageDlg('Sei sicuro di voler cancellare ?', mtWarning, [mbYes, mbNo], 0) = mrNo then exit;
     Q.Close;
     Q.SQL.clear;
     Q.SQL.Text := 'Delete from StoricoAnagrafica where id = ' + QStoricoAnagID.AsString;
     Q.ExecSQL;
     QStoricoAnag.close;
     QStoricoAnag.open;
end;

procedure TStoricoAnagraficaForm.QStoricoAnagAfterInsert(DataSet: TDataSet);
begin
     QStoricoAnagIDAnagrafica.Value := Data.TAnagraficaID.Value;
     QStoricoAnagCognome.Value := Data.TAnagraficaCognome.Value;
     QStoricoAnagNome.Value := Data.TAnagraficaNome.Value;
end;

procedure TStoricoAnagraficaForm.QStoricoAnagBeforeOpen(DataSet: TDataSet);
begin
     QStoricoAnag.sql.text := 'select * from Storicoanagrafica where IDAnagrafica=:idanag';
     QStoricoAnag.Parameters.ParamByName('idanag').value := Data.TAnagraficaID.value;
end;

procedure TStoricoAnagraficaForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
   //dxDBGrid1DBBandedTableView1.StoreToRegistry('\Software\H1', False, [], 'DbGridStoricoAnagrafica');  

end;

end.

