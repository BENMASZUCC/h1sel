unit ContattoCli;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Mask, DBCtrls, Buttons, ExtCtrls, TB97, Menus, Clipbrd, uSendMailMapi;

type
     TContattoCliForm = class(TForm)
          Panel1: TPanel;
          DBEDenomCliente: TDBEdit;
          Label1: TLabel;
          SpeedButton1: TSpeedButton;
          Panel3: TPanel;
          ENote: TEdit;
          EEmail: TEdit;
          Label5: TLabel;
          Label6: TLabel;
          Label4: TLabel;
          EFax: TEdit;
          ETelefono: TEdit;
          Label3: TLabel;
          Label2: TLabel;
          Label7: TLabel;
          ETitolo: TEdit;
          EContattoCognome: TEdit;
          Label8: TLabel;
          Label9: TLabel;
          EContattoNome: TEdit;
          ECellulare: TEdit;
          Panel4: TPanel;
          BSelezAnag: TToolbarButton97;
          Panel2: TPanel;
          BitBtn1: TToolbarButton97;
          BAnnulla: TToolbarButton97;
          PMCopia: TPopupMenu;
          CopiaEmail: TMenuItem;
          InviaMail: TMenuItem;
          Label10: TLabel;
          CBUscito: TCheckBox;
          procedure FormShow(Sender: TObject);
          procedure SpeedButton1Click(Sender: TObject);
          procedure BSelezAnagClick(Sender: TObject);
          procedure BitBtn1Click(Sender: TObject);
          procedure BAnnullaClick(Sender: TObject);
          procedure CopiaEmailClick(Sender: TObject);
          procedure InviaMailClick(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
          xIDAnag: integer;
          xInsDaRicerca: boolean;
     end;

var
     ContattoCliForm: TContattoCliForm;

implementation

uses uUtilsVarie, ElencoDip, Main;

{$R *.DFM}

procedure TContattoCliForm.FormShow(Sender: TObject);
begin
     //grafica
     ContattoCliForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel3.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel3.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel3.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel4.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel4.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel4.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Caption := '[M/103] - ' + Caption;
     xIDAnag := -1;



     if xInsDaRicerca then begin
          if MessageDlg('Attenzione: prima di inserire un nuovo nominativo verificare che non sia gi� presente all''interno dell''archivio anagrafico', mtWarning, [mbYes, mbNo], 0) = mrYes then begin
               BSelezAnagClick(self);
          end;
     end;
end;

procedure TContattoCliForm.SpeedButton1Click(Sender: TObject);
begin
     ApriHelp('103');
end;

procedure TContattoCliForm.BSelezAnagClick(Sender: TObject);
var xPressed: boolean;
begin
     ElencoDipForm := TElencoDipForm.create(self);
     ElencoDipForm.ShowModal;
     if ElencoDipForm.ModalResult = mrOK then begin
          xIDAnag := elencodipform.TAnagDip.fieldbyname('ID').asInteger;
          ETitolo.Text := elencodipform.TAnagDip.fieldbyname('Titolo').asString;
          EContattoCognome.Text := elencodipform.TAnagDip.fieldbyname('Cognome').asString;
          EContattonome.Text := elencodipform.TAnagDip.fieldbyname('Nome').asString;
          ETelefono.Text := elencodipform.TAnagDip.fieldbyname('RecapitiTelefonici').asString;
          ECellulare.Text := elencodipform.TAnagDip.fieldbyname('Cellulare').asString;
          EFax.Text := elencodipform.TAnagDip.fieldbyname('Fax').asString;
          EEmail.Text := elencodipform.TAnagDip.fieldbyname('Email').asString;
     end;
     ElencoDipForm.free;

end;

procedure TContattoCliForm.BitBtn1Click(Sender: TObject);
begin
     ContattoCliForm.ModalResult := mrOk;
end;

procedure TContattoCliForm.BAnnullaClick(Sender: TObject);
begin
     ContattoCliForm.ModalResult := mrCancel;
end;

procedure TContattoCliForm.CopiaEmailClick(Sender: TObject);
begin
     Clipboard.asText := EEmail.Text;
end;

procedure TContattoCliForm.InviaMailClick(Sender: TObject);
begin
     SendEMailMAPI('(inserire oggetto)', '(inserire testo)', Trim(EEmail.Text), ' ', ' ', MainForm.xSendMail_Remote, MainForm.xDebug, True, False);
end;

end.

