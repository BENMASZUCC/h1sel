unit DettSede;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, TB97, ExtCtrls;

type
     TDettSedeForm = class(TForm)
          Label1: TLabel;
          Label2: TLabel;
          Label3: TLabel;
          Label4: TLabel;
          EIndirizzo: TEdit;
          ECap: TEdit;
          EComune: TEdit;
          EProv: TEdit;
          Label5: TLabel;
          EDesc: TEdit;
          CBTipoStrada: TComboBox;
          ENumCivico: TEdit;
          Panel1: TPanel;
    BitBtn1: TToolbarButton97;
    BitBtn2: TToolbarButton97;
          procedure FormShow(Sender: TObject);
          procedure BitBtn1Click(Sender: TObject);
          procedure BitBtn2Click(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     DettSedeForm: TDettSedeForm;

implementation

uses Main;

{$R *.DFM}

procedure TDettSedeForm.FormShow(Sender: TObject);
begin
     //Grafico
     DettSedeForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     Caption := '[S/22] - ' + Caption;
end;

procedure TDettSedeForm.BitBtn1Click(Sender: TObject);
begin
     DettSedeForm.ModalResult := mrOk;
end;

procedure TDettSedeForm.BitBtn2Click(Sender: TObject);
begin
     DettSedeForm.ModalResult := mrCancel;
end;

end.

