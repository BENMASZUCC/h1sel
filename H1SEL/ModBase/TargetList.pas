unit TargetList;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  dxTL, dxDBCtrl, dxDBGrid, dxCntner, Db, DBTables, StdCtrls, Buttons,
  ExtCtrls, dxDBTLCl, dxGrClms, dxGridMenus, Menus, TB97, dxPSCore,
  dxPSdxTLLnk, dxPSdxDBGrLnk;

type
  TTargetListForm = class(TForm)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    QTargetList: TQuery;
    DsQTargetList: TDataSource;
    dxDBGrid1: TdxDBGrid;
    dxDBGrid1ID: TdxDBGridMaskColumn;
    dxDBGrid1Azienda: TdxDBGridMaskColumn;
    dxDBGrid1Attivita: TdxDBGridMaskColumn;
    dxDBGrid1telefono: TdxDBGridMaskColumn;
    dxDBGrid1Comune: TdxDBGridMaskColumn;
    dxDBGrid1DataUltimaEsploraz: TdxDBGridDateColumn;
    dxPrinter1: TdxComponentPrinter;
    dxPrinter1Link1: TdxDBGridReportLink;
    BStampaElenco: TToolbarButton97;
    PMStampa: TPopupMenu;
    Stampaelenco1: TMenuItem;
    EsportainExcel1: TMenuItem;
    esportainHTML1: TMenuItem;
    ToolbarButton971: TToolbarButton97;
    ToolbarButton972: TToolbarButton97;
    QTargetListID: TAutoIncField;
    QTargetListAzienda: TStringField;
    QTargetListAttivita: TStringField;
    QTargetListtelefono: TStringField;
    QTargetListComune: TStringField;
    QTargetListDataUltimaEsploraz: TDateTimeField;
    procedure dxDBGrid1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Stampaelenco1Click(Sender: TObject);
    procedure EsportainExcel1Click(Sender: TObject);
    procedure esportainHTML1Click(Sender: TObject);
    procedure ToolbarButton971Click(Sender: TObject);
    procedure ToolbarButton972Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  TargetListForm: TTargetListForm;

implementation

uses Main, RicClienti, uUtilsVarie;

{$R *.DFM}

procedure TTargetListForm.dxDBGrid1MouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
     if (Button<>mbRight)or(Shift<> []) then Exit;
     TdxDBGridPopupMenuManager.Instance.ShowGridPopupMenu(Sender as TdxDBGrid);
end;

procedure TTargetListForm.Stampaelenco1Click(Sender: TObject);
begin
     dxPrinter1.Preview(True,nil);
end;

procedure TTargetListForm.EsportainExcel1Click(Sender: TObject);
begin
     Mainform.Save('xls','Microsoft Excel 4.0 Worksheet (*.xls)|*.xls','ExpGrid.xls',dxDBGrid1.SaveToXLS);
end;

procedure TTargetListForm.esportainHTML1Click(Sender: TObject);
begin
     Mainform.Save('htm','HTML File (*.htm; *.html)|*.htm','ExpGrid.htm',dxDBGrid1.SaveToHTML);
end;

procedure TTargetListForm.ToolbarButton971Click(Sender: TObject);
begin
     if not MainForm.CheckProfile('171') then Exit;
     RicClientiForm:=TRicClientiForm.create(self);
     RicClientiForm.BAddTargetList.visible:=True;
     // riempimento combobox1 con le tabelle
     with RicClientiForm do begin
          QTabelle.Close;
          QTabelle.Open;
          QTabelle.First;
          ComboBox1.Items.Clear;
          while not QTabelle.EOF do begin
               ComboBox1.Items.Add(QTabelleDescTabella.asString);
               QTabelle.Next;
          end;
          QRes1.Close;
     end;
     RicClientiForm.Showmodal;
     RicClientiForm.Free;
     QTargetList.Close;
     QTargetList.Open;
end;

procedure TTargetListForm.ToolbarButton972Click(Sender: TObject);
begin
     MainForm.xIDOrg:=QTargetListID.Value;
     Close;
end;

procedure TTargetListForm.BitBtn1Click(Sender: TObject);
begin
     MainForm.xIDOrg:=0;
end;

end.
