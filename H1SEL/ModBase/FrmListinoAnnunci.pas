//[/TONI20020920\]DEBUGOKFINE
unit FrmListinoAnnunci;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ExtCtrls, Db, DBTables, Grids, DBGrids, StdCtrls, DBCtrls, RXDBCtrl,
     ComCtrls, TB97, Mask, ADODB, U_ADOLinkCl, dxTL, dxDBCtrl, dxDBGrid,
     dxCntner;

type
     TListinoAnnunciFrame = class(TFrame)
          Panel2: TPanel;
          Panel3: TPanel;
          Panel4: TPanel;
          Panel5: TPanel;
          Panel1: TPanel;
          QEdizioni_OLD: TQuery;
          DsQEdizioni: TDataSource;
          DBGrid1: TDBGrid;
          DBText1: TDBText;
          QEdizioni_OLDIDEdizione: TAutoIncField;
          QEdizioni_OLDTestata: TStringField;
          QEdizioni_OLDEdizione: TStringField;
          QEdizioni_OLDTestataEdizione: TStringField;
          QListinoEdiz_OLD: TQuery;
          DsQListinoEdiz: TDataSource;
          QListinoEdiz_OLDID: TAutoIncField;
          QListinoEdiz_OLDIDEdizione: TIntegerField;
          QListinoEdiz_OLDDallaData: TDateTimeField;
          QListinoEdiz_OLDPrezzoListinoLire: TFloatField;
          QListinoEdiz_OLDPrezzoANoiLire: TFloatField;
          QListinoEdiz_OLDPrezzoAlClienteLire: TFloatField;
          QListinoEdiz_OLDPrezzoListinoEuro: TFloatField;
          QListinoEdiz_OLDPrezzoANoiEuro: TFloatField;
          QListinoEdiz_OLDPrezzoAlClienteEuro: TFloatField;
          QListinoEdiz_OLDNote: TStringField;
          QListinoEdiz_OLDQtaDa: TSmallintField;
          QListinoEdiz_OLDQtaA: TSmallintField;
          QListinoEdiz_OLDDiffLire: TFloatField;
          QListinoEdiz_OLDDiffEuro: TFloatField;
          PCValuta: TPageControl;
          TSLire: TTabSheet;
          TSEuro: TTabSheet;
          ToolbarButton972: TToolbarButton97;
          ToolbarButton973: TToolbarButton97;
          ToolbarButton974: TToolbarButton97;
          Splitter1: TSplitter;
          QMinPrezziTestata_OLD: TQuery;
          DsQMinPrezziTestata: TDataSource;
          QEdizioni_OLDIDTestata: TIntegerField;
          QMinPrezziTestata_OLDminPrezzoListinoLire: TFloatField;
          QMinPrezziTestata_OLDminPrezzoANoiLire: TFloatField;
          QMinPrezziTestata_OLDminPrezzoAlClienteLire: TFloatField;
          QMinPrezziTot_OLD: TQuery;
          DsQMinPrezziTot: TDataSource;
          QMinPrezziTot_OLDminPrezzoListinoLire: TFloatField;
          QMinPrezziTot_OLDminPrezzoANoiLire: TFloatField;
          QMinPrezziTot_OLDminPrezzoAlClienteLire: TFloatField;
          QMinPrezziTestata_OLDminPrezzoListinoEuro: TFloatField;
          QMinPrezziTestata_OLDminPrezzoANoiLireEuro: TFloatField;
          QMinPrezziTestata_OLDminPrezzoAlClienteEuro: TFloatField;
          QMinPrezziTot_OLDminPrezzoListinoEuro: TFloatField;
          QMinPrezziTot_OLDminPrezzoANoiLireEuro: TFloatField;
          QMinPrezziTot_OLDminPrezzoAlClienteEuro: TFloatField;
          Panel7: TPanel;
          DBText2: TDBText;
          Label1: TLabel;
          Panel8: TPanel;
          DBEdit1: TDBEdit;
          DBEdit2: TDBEdit;
          DBEdit3: TDBEdit;
          DBEdit4: TDBEdit;
          DBEdit5: TDBEdit;
          DBEdit6: TDBEdit;
          Panel9: TPanel;
          DBText3: TDBText;
          Label2: TLabel;
          Panel10: TPanel;
          DBEdit7: TDBEdit;
          DBEdit8: TDBEdit;
          DBEdit9: TDBEdit;
          DBEdit10: TDBEdit;
          DBEdit11: TDBEdit;
          DBEdit12: TDBEdit;
          Label3: TLabel;
          Label4: TLabel;
          Label5: TLabel;
          Label6: TLabel;
          Label7: TLabel;
          Label8: TLabel;
          Panel6: TPanel;
          LCliente: TLabel;
          QListinoEdiz_OLDID_1: TAutoIncField;
          QListinoEdiz_OLDPrezzoQuestoClienteLire: TFloatField;
          QListinoEdiz_OLDPrezzoQuestoClienteEuro: TFloatField;
          BModPrezzoQuestoCli: TToolbarButton97;
          QListinoEdiz_OLDIDCliente_1: TIntegerField;
          QListinoEdiz: TADOLinkedQuery;
          QEdizioni: TADOLinkedQuery;
          QMinPrezziTestata: TADOLinkedQuery;
          QMinPrezziTot: TADOLinkedQuery;
          QEdizioniIDEdizione: TAutoIncField;
          QEdizioniIDTestata: TIntegerField;
          QEdizioniTestata: TStringField;
          QEdizioniEdizione: TStringField;
          QEdizioniTestataEdizione: TStringField;
          QListinoEdizID: TAutoIncField;
          QListinoEdizIDEdizione: TIntegerField;
          QListinoEdizDallaData: TDateTimeField;
          QListinoEdizPrezzoListinoLire: TFloatField;
          QListinoEdizPrezzoANoiLire: TFloatField;
          QListinoEdizPrezzoAlClienteLire: TFloatField;
          QListinoEdizPrezzoListinoEuro: TFloatField;
          QListinoEdizPrezzoANoiEuro: TFloatField;
          QListinoEdizPrezzoAlClienteEuro: TFloatField;
          QListinoEdizNote: TStringField;
          QListinoEdizQtaDa: TSmallintField;
          QListinoEdizQtaA: TSmallintField;
          QListinoEdizID_1: TAutoIncField;
          QListinoEdizIDCliente: TIntegerField;
          QListinoEdizPrezzoQuestoClienteLire: TFloatField;
          QListinoEdizPrezzoQuestoClienteEuro: TFloatField;
          QListinoEdizDiffLire: TIntegerField;
          QListinoEdizDiffEuro: TFloatField;
          QMinPrezziTestataminPrezzoListinoLire: TFloatField;
          QMinPrezziTestataminPrezzoANoiLire: TFloatField;
          QMinPrezziTestataminPrezzoAlClienteLire: TFloatField;
          QMinPrezziTestataminPrezzoListinoEuro: TFloatField;
          QMinPrezziTestataminPrezzoANoiLireEuro: TFloatField;
          QMinPrezziTestataminPrezzoAlClienteEuro: TFloatField;
          QMinPrezziTotminPrezzoListinoLire: TFloatField;
          QMinPrezziTotminPrezzoANoiLire: TFloatField;
          QMinPrezziTotminPrezzoAlClienteLire: TFloatField;
          QMinPrezziTotminPrezzoListinoEuro: TFloatField;
          QMinPrezziTotminPrezzoANoiLireEuro: TFloatField;
          QMinPrezziTotminPrezzoAlClienteEuro: TFloatField;
          RxDBGrid2: TdxDBGrid;
          dxDBGrid1DallaData: TdxDBGridColumn;
          dxDBGrid1QtaDa: TdxDBGridColumn;
          dxDBGrid1QtaA: TdxDBGridColumn;
          dxDBGrid1PrezzoListinoEuro: TdxDBGridColumn;
          dxDBGrid1PrezzoANoiEuro: TdxDBGridColumn;
          dxDBGrid1PrezzoAlClienteEuro: TdxDBGridColumn;
          dxDBGrid1DiffEuro: TdxDBGridColumn;
          dxDBGrid1PrezzoQuestoClienteEuro: TdxDBGridColumn;
          RxDBGrid1: TdxDBGrid;
          dxDBGridDallaData: TdxDBGridColumn;
          dxDBGridQtaDa: TdxDBGridColumn;
          dxDBGridQtaA: TdxDBGridColumn;
          dxDBGridPrezzoListinoEuro: TdxDBGridColumn;
          dxDBGridPrezzoANoiEuro: TdxDBGridColumn;
          dxDBGridPrezzoAlClienteEuro: TdxDBGridColumn;
          dxDBGridDiffEuro: TdxDBGridColumn;
          dxDBGridPrezzoQuestoClienteEuro: TdxDBGridColumn;
          procedure QEdizioni_OLDCalcFields(DataSet: TDataSet);
          procedure QListinoEdiz_OLDCalcFields(DataSet: TDataSet);
          procedure ToolbarButton972Click(Sender: TObject);
          procedure ToolbarButton973Click(Sender: TObject);
          procedure QListinoEdiz_OLDAfterOpen(DataSet: TDataSet);
          procedure QListinoEdiz_OLDAfterScroll(DataSet: TDataSet);
          procedure RxDBGrid1GetCellProps(Sender: TObject; Field: TField;
               AFont: TFont; var Background: TColor);
          procedure RxDBGrid2GetCellProps(Sender: TObject; Field: TField;
               AFont: TFont; var Background: TColor);
          procedure ToolbarButton974Click(Sender: TObject);
          procedure BModPrezzoQuestoCliClick(Sender: TObject);
          procedure QEdizioniAfterScroll(DataSet: TDataSet);
     private
          xQtadal, xQtaAl, xIDRow: integer;
     public
          xIDCliente: integer;
     end;

implementation

uses PrezzoListino, ModuloDati, ModuloDati4, ModPrezzo, Main;

{$R *.DFM}

//[/TONI20020729\]
//[/TONI20020919\]DEBUGOK

procedure TListinoAnnunciFrame.QEdizioni_OLDCalcFields(DataSet: TDataSet);
begin
     //     QEdizioniTestataEdizione.Value:=QEdizioniTestata.Value+' - '+QEdizioniEdizione.Value;
     QEdizioni.FieldByName('TestataEdizione').AsString := QEdizioni.FieldByName('Testata').ASString + ' - ' + QEdizioni.FieldbyName('Edizione').AsString;
end;

procedure TListinoAnnunciFrame.QListinoEdiz_OLDCalcFields(DataSet: TDataSet);
begin
     {     QListinoEdizDiffLire.Value:=QListinoEdizPrezzoAlClienteLire.Value-QListinoEdizPrezzoANoiLire.Value;
          QListinoEdizDiffEuro.Value:=QListinoEdizPrezzoAlClienteEuro.Value-QListinoEdizPrezzoANoiEuro.Value;}
     QListinoEdiz.FieldByName('DiffLire').AsInteger := QListinoEdiz.FieldByName('PrezzoAlClienteLire').AsINteger - QListinoEdiz.FieldByName('PrezzoANoiLire').AsInteger;
     QListinoEdiz.FieldByName('DiffEuro').AsFloat := QListinoEdiz.FieldByName('PrezzoAlClienteEuro').AsFloat - QListinoEdiz.FieldbyName('PrezzoANoiEuro').AsFloat;
end;
//[/TONI20020919\]DEBUGOKFINE
//[/TONI20020729\]FINE

//[/TONI20020729\]
//[/TONI20020920\]DEBUGOK

procedure TListinoAnnunciFrame.ToolbarButton972Click(Sender: TObject);
var xIDClienteListino: integer;
begin
     if not MainForm.CheckProfile('3500') then Exit;
     PrezzoListinoForm := TPrezzoListinoForm.create(self);
     PrezzoListinoForm.ShowModal;
     if PrezzoListinoForm.ModalResult = mrOK then begin
          if PrezzoListinoForm.DEDallaData.Date = 0 then begin
               MessageDlg('Non � stata inserita la data', mtWarning, [mbOK], 0);
               exit;
          end;
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    {                    Q1.SQL.text:='insert into Ann_EdizListino (IDEdizione,DallaData,PrezzoListinoLire,PrezzoANoiLire,PrezzoAlClienteLire,'+
                                             '                             PrezzoListinoEuro,PrezzoANoiEuro,PrezzoAlClienteEuro,Note,QtaDa,QtaA,IDCliente) '+
                                             'values (:xIDEdizione,:xDallaData,:xPrezzoListinoLire,:xPrezzoANoiLire,:xPrezzoAlClienteLire,'+
                                             '        :xPrezzoListinoEuro,:xPrezzoANoiEuro,:xPrezzoAlClienteEuro,:xNote,:xQtaDa,:xQtaA,:xIDCliente)';
                                        Q1.ParamByName('xIDEdizione').asInteger:=QEdizioniIDEdizione.Value;
                                        Q1.ParamByName('xDallaData').asDateTime:=PrezzoListinoForm.DEDallaData.Date;
                                        Q1.ParamByName('xPrezzoListinoLire').AsFloat:=PrezzoListinoForm.ListinoLire.Value;
                                        Q1.ParamByName('xPrezzoANoiLire').AsFloat:=PrezzoListinoForm.AnoiLire.Value;
                                        Q1.ParamByName('xPrezzoAlClienteLire').AsFloat:=PrezzoListinoForm.AlClienteLire.Value;
                                        Q1.ParamByName('xPrezzoListinoEuro').AsFloat:=PrezzoListinoForm.ListinoEuro.Value;
                                        Q1.ParamByName('xPrezzoANoiEuro').AsFloat:=PrezzoListinoForm.AnoiEuro.Value;
                                        Q1.ParamByName('xPrezzoAlClienteEuro').AsFloat:=PrezzoListinoForm.AlClienteEuro.Value;
                                        Q1.ParamByName('xNote').asString:=PrezzoListinoForm.ENote.text;
                                        Q1.ParamByName('xQtaDa').asInteger:=Round(PrezzoListinoForm.RxSpinEdit1.value);
                                        Q1.ParamByName('xQtaA').asInteger:=Round(PrezzoListinoForm.RxSpinEdit2.value);
                                        Q1.ParamByName('xIDCliente').asInteger:=xIDClienteListino;}
                    Q1.SQL.text := 'insert into Ann_EdizListino (IDEdizione,DallaData,PrezzoListinoLire,PrezzoANoiLire,PrezzoAlClienteLire,' +
                         '                             PrezzoListinoEuro,PrezzoANoiEuro,PrezzoAlClienteEuro,Note,QtaDa,QtaA,IDCliente) ' +
                         'values (:xIDEdizione:,:xDallaData:,:xPrezzoListinoLire:,:xPrezzoANoiLire:,:xPrezzoAlClienteLire:,' +
                         '        :xPrezzoListinoEuro:,:xPrezzoANoiEuro:,:xPrezzoAlClienteEuro:,:xNote:,:xQtaDa:,:xQtaA:,:xIDCliente:)';
                    Q1.ParamByName['xIDEdizione'] := QEdizioni.FieldByName('IDEdizione').Value;
                    Q1.ParamByName['xDallaData'] := PrezzoListinoForm.DEDallaData.Date;
                    Q1.ParamByName['xPrezzoListinoLire'] := PrezzoListinoForm.ListinoLire.Value;
                    Q1.ParamByName['xPrezzoANoiLire'] := PrezzoListinoForm.AnoiLire.Value;
                    Q1.ParamByName['xPrezzoAlClienteLire'] := PrezzoListinoForm.AlClienteLire.Value;
                    Q1.ParamByName['xPrezzoListinoEuro'] := PrezzoListinoForm.ListinoEuro.Value;
                    Q1.ParamByName['xPrezzoANoiEuro'] := PrezzoListinoForm.AnoiEuro.Value;
                    Q1.ParamByName['xPrezzoAlClienteEuro'] := PrezzoListinoForm.AlClienteEuro.Value;
                    Q1.ParamByName['xNote'] := PrezzoListinoForm.ENote.text;
                    Q1.ParamByName['xQtaDa'] := INteger(Round(PrezzoListinoForm.RxSpinEdit1.value));
                    Q1.ParamByName['xQtaA'] := Integer(Round(PrezzoListinoForm.RxSpinEdit2.value));
                    //Q1.ParamByName['xIDCliente'] := xIDClienteListino;
                    Q1.ParamByName['xIDCliente'] := xIDCliente;

                    Q1.ExecSQL;
                    DB.CommitTrans;
                    QListinoEdiz.Close;
                    QListinoEdiz.Open;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               end;
          end;
     end;
     PrezzoListinoForm.free;
end;
//[/TONI20020729\]FINE
//[/TONI20020920\]DEBUGOKFINE

//[/TONI20020729\]
//[/TONI20020919\]DEBUGOK

procedure TListinoAnnunciFrame.ToolbarButton973Click(Sender: TObject);
var xIDClienteListino: integer;
begin
     if not MainForm.CheckProfile('3501') then Exit;
     if QListinoEdiz.RecordCount = 0 then exit;
     PrezzoListinoForm := TPrezzoListinoForm.create(self);
     {     PrezzoListinoForm.DEDallaData.Date:=QListinoEdizDallaData.Value;
          PrezzoListinoForm.ListinoLire.Value:=QListinoEdizPrezzoListinoLire.Value;
          PrezzoListinoForm.AnoiLire.Value:=QListinoEdizPrezzoANoiLire.Value;
          PrezzoListinoForm.AlClienteLire.Value:=QListinoEdizPrezzoAlClienteLire.Value;
          PrezzoListinoForm.ListinoEuro.Value:=QListinoEdizPrezzoListinoEuro.value;
          PrezzoListinoForm.AnoiEuro.Value:=QListinoEdizPrezzoANoiEuro.Value;
          PrezzoListinoForm.AlClienteEuro.Value:=QListinoEdizPrezzoAlClienteEuro.Value;
          PrezzoListinoForm.ENote.text:=QListinoEdizNote.Value;
          PrezzoListinoForm.RxSpinEdit1.value:=QListinoEdizQtaDa.Value;
          PrezzoListinoForm.RxSpinEdit2.value:=QListinoEdizQtaA.Value;
          PrezzoListinoForm:=TPrezzoListinoForm.create(self);}
     PrezzoListinoForm.DEDallaData.Date := QListinoEdiz.FieldByName('DallaData').AsDateTIme;
     PrezzoListinoForm.ListinoLire.Value := QListinoEdiz.FieldByName('PrezzoListinoLire').AsINteger;
     PrezzoListinoForm.AnoiLire.Value := QListinoEdiz.FieldByName('PrezzoANoiLire').Asinteger;
     PrezzoListinoForm.AlClienteLire.Value := QListinoEdiz.FieldByName('PrezzoAlClienteLire').AsInteger;
     PrezzoListinoForm.ListinoEuro.Value := QListinoEdiz.FieldByName('PrezzoListinoEuro').AsFloat;
     PrezzoListinoForm.AnoiEuro.Value := QListinoEdiz.FieldByName('PrezzoANoiEuro').AsFloat;
     PrezzoListinoForm.AlClienteEuro.Value := QListinoEdiz.FieldByName('PrezzoAlClienteEuro').AsFloat;
     PrezzoListinoForm.ENote.text := TrimRight(QListinoEdiz.FieldByName('Note').AsString);
     PrezzoListinoForm.RxSpinEdit1.value := QListinoEdiz.FieldByName('QtaDa').AsInteger;
     PrezzoListinoForm.RxSpinEdit2.value := QListinoEdiz.FieldByName('QtaA').AsInteger;
     PrezzoListinoForm.ShowModal;
     if PrezzoListinoForm.ModalResult = mrOK then begin
          if PrezzoListinoForm.DEDallaData.Date = 0 then begin
               MessageDlg('Non � stata inserita la data', mtWarning, [mbOK], 0);
               exit;
          end;
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    {                    Q1.SQL.text:='update Ann_EdizListino set DallaData=:xDallaData,PrezzoListinoLire=:xPrezzoListinoLire,PrezzoANoiLire=:xPrezzoANoiLire,'+
                                             'PrezzoAlClienteLire=:xPrezzoAlClienteLire,PrezzoListinoEuro=:xPrezzoListinoEuro, '+
                                             'PrezzoANoiEuro=:xPrezzoANoiEuro,PrezzoAlClienteEuro=:xPrezzoAlClienteEuro,Note=:xNote,QtaDa=:XQtaDa,QtaA=:xQtaA,IDCliente=:xIDCliente '+
                                             'where ID='+QListinoEdizID.asString;
                                        Q1.ParamByName('xDallaData').asDateTime:=PrezzoListinoForm.DEDallaData.Date;
                                        Q1.ParamByName('xPrezzoListinoLire').AsFloat:=PrezzoListinoForm.ListinoLire.Value;
                                        Q1.ParamByName('xPrezzoANoiLire').AsFloat:=PrezzoListinoForm.AnoiLire.Value;
                                        Q1.ParamByName('xPrezzoAlClienteLire').AsFloat:=PrezzoListinoForm.AlClienteLire.Value;
                                        Q1.ParamByName('xPrezzoListinoEuro').AsFloat:=PrezzoListinoForm.ListinoEuro.Value;
                                        Q1.ParamByName('xPrezzoANoiEuro').AsFloat:=PrezzoListinoForm.AnoiEuro.Value;
                                        Q1.ParamByName('xPrezzoAlClienteEuro').AsFloat:=PrezzoListinoForm.AlClienteEuro.Value;
                                        Q1.ParamByName('xNote').asString:=PrezzoListinoForm.ENote.text;
                                        Q1.ParamByName('xQtaDa').asInteger:=Round(PrezzoListinoForm.RxSpinEdit1.value);
                                        Q1.ParamByName('xQtaA').asInteger:=Round(PrezzoListinoForm.RxSpinEdit2.value);
                                        Q1.ParamByName('xIDCliente').asInteger:=xIDClienteListino;}
                    Q1.SQL.text := 'update Ann_EdizListino set DallaData=:xDallaData:,PrezzoListinoLire=:xPrezzoListinoLire:,PrezzoANoiLire=:xPrezzoANoiLire:,' +
                         'PrezzoAlClienteLire=:xPrezzoAlClienteLire:,PrezzoListinoEuro=:xPrezzoListinoEuro:, ' +
                         'PrezzoANoiEuro=:xPrezzoANoiEuro:,PrezzoAlClienteEuro=:xPrezzoAlClienteEuro:,Note=:xNote:,QtaDa=:xQtaDa:,QtaA=:xQtaA:,IDCliente=:xIDCliente: ' +
                         'where ID=' + QListinoEdiz.FieldByName('ID').asString;
                    Q1.ParamByName['xDallaData'] := PrezzoListinoForm.DEDallaData.Date;
                    Q1.ParamByName['xPrezzoListinoLire'] := PrezzoListinoForm.ListinoLire.AsInteger;
                    Q1.ParamByName['xPrezzoANoiLire'] := PrezzoListinoForm.AnoiLire.AsInteger;
                    Q1.ParamByName['xPrezzoAlClienteLire'] := PrezzoListinoForm.AlClienteLire.AsInteger;
                    Q1.ParamByName['xPrezzoListinoEuro'] := PrezzoListinoForm.ListinoEuro.Value;
                    Q1.ParamByName['xPrezzoANoiEuro'] := PrezzoListinoForm.AnoiEuro.Value;
                    Q1.ParamByName['xPrezzoAlClienteEuro'] := PrezzoListinoForm.AlClienteEuro.Value;
                    Q1.ParamByName['xNote'] := PrezzoListinoForm.ENote.text;
                    Q1.ParamByName['xQtaDa'] := PrezzoListinoForm.RxSpinEdit1.Value;
                    Q1.ParamByName['xQtaA'] := PrezzoListinoForm.RxSpinEdit2.Value;
                    //Q1.ParamByName['xIDCliente'] := xIDClienteListino;
                    Q1.ParamByName['xIDCliente'] := xIDCliente;
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    QListinoEdiz.Close;
                    QListinoEdiz.Open;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               end;
          end;
     end;
     PrezzoListinoForm.free;
end;
//[/TONI20020919DEBUG\]FINEOK
//[/TONI20020729\]FINE

//[/TONI20020920\]DEBUGOK

procedure TListinoAnnunciFrame.QListinoEdiz_OLDAfterOpen(DataSet: TDataSet);
begin
     xQtadal := -1;
     xQtaAl := -1;
end;

procedure TListinoAnnunciFrame.QListinoEdiz_OLDAfterScroll(DataSet: TDataSet);
begin
     xQtadal := -1;
     xQtaAl := -1;
end;
//[/TONI20020920\]DEBUGOKFINE

//[/TONI20020729\]
//[/TONI20020729\]DEBUGOK

procedure TListinoAnnunciFrame.RxDBGrid1GetCellProps(Sender: TObject;
     Field: TField; AFont: TFont; var Background: TColor);
begin
     {     if (QListinoEdizQtaDa.Value<>xQtadal)and(QListinoEdizQtaA.Value<>xQtaAl) then begin
               Background:=clYellow;
               xQtadal:=QListinoEdizQtaDa.Value;
               xQtaAl:=QListinoEdizQtaA.Value;
          end;}
     if (QListinoEdiz.FieldByName('QtaDa').AsInteger <> xQtadal) and (QListinoEdiz.FieldByName('QtaA').Value <> xQtaAl) then begin
          Background := clYellow;
          xQtadal := QListinoEdiz.FieldByName('QtaDa').AsInteger;
          xQtaAl := QListinoEdiz.FieldByName('QtaA').AsInteger;
     end;

end;

procedure TListinoAnnunciFrame.RxDBGrid2GetCellProps(Sender: TObject;
     Field: TField; AFont: TFont; var Background: TColor);
begin
     {     if (QListinoEdizQtaDa.Value<>xQtadal)and(QListinoEdizQtaA.Value<>xQtaAl) then begin
               Background:=clYellow;
               xQtadal:=QListinoEdizQtaDa.Value;
               xQtaAl:=QListinoEdizQtaA.Value;
          end;}
     if (QListinoEdiz.FieldByName('QtaDa').Value <> xQtadal) and (QListinoEdiz.FieldByName('QtaA').Value <> xQtaAl) then begin
          Background := clYellow;
          xQtadal := QListinoEdiz.FieldByName('QtaDa').AsInteger;
          xQtaAl := QListinoEdiz.FieldByName('QtaA').AsInteger;
     end;
end;
//[/TONI20020729\]DEBUGOKFINE
//[/TONI20020729\]FINE

//[/TONI20020729\]
//[/TONI20020920\]DEBUGOK

procedure TListinoAnnunciFrame.ToolbarButton974Click(Sender: TObject);
begin
     if not MainForm.CheckProfile('3502') then Exit;
     if QListinoEdiz.RecordCount = 0 then exit;
     if MessageDlg('Sei sicuro di voler eliminare questa riga di listino ?', mtWarning,
          [mbNo, mbYes], 0) = mrYes then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    {                    Q1.SQL.text:='delete from Ann_EdizListino '+
                                             'where ID='+QListinoEdizID.asString;}
                    Q1.SQL.text := 'delete from Ann_EdizListino ' +
                         'where ID=' + QListinoEdiz.FieldByName('ID').asString;
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    QListinoEdiz.Close;
                    QListinoEdiz.Open;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               end;
          end;
     end;
end;
//[/TONI20020920\]DEBUGOKFINE
//[/TONI20020729\]FINE

//[/TONI20020729\]
//[/TONI20020920\]DEBUGOK

procedure TListinoAnnunciFrame.BModPrezzoQuestoCliClick(Sender: TObject);
begin
     if not MainForm.CheckProfile('3503') then Exit;
     if not MainForm.CheckProfile('181') then Exit;
     if QListinoEdiz.RecordCount = 0 then exit;
     ModPrezzoForm := TModPrezzoForm.create(self);
     {     if QListinoEdizPrezzoQuestoClienteLire.asString<>'' then begin
               ModPrezzoForm.PrezzoLire.value:=QListinoEdizPrezzoQuestoClienteLire.value;
               ModPrezzoForm.PrezzoEuro.value:=QListinoEdizPrezzoQuestoClienteEuro.value;}

     if QListinoEdiz.FieldByName('PrezzoQuestoClienteLire').asString <> '' then begin
          ModPrezzoForm.PrezzoLire.value := QListinoEdiz.FieldByName('PrezzoQuestoClienteLire').value;
          ModPrezzoForm.PrezzoEuro.value := QListinoEdiz.FieldByName('PrezzoQuestoClienteEuro').value;
     end;
     ModPrezzoForm.ShowModal;
     if ModPrezzoForm.ModalResult = mrOK then begin
          //          if QListinoEdizIDCliente_1.AsString='' then begin
          //if QListinoEdiz.FieldByName('IDCliente').AsString = '' then begin
          if QListinoEdiz.FieldByName('ID_1').AsString = '' then begin
               with Data do begin
                    //if QListinoEdiz.FieldByName('IDCliente').AsString = '' then begin
                    //if QListinoEdiz.FieldByName('IDCliente').AsString = '' then begin
                    DB.BeginTrans;
                    try
                         Q1.Close;
                         {                         Q1.SQL.text:='insert into Ann_ListinoClienti (IDEdizListino,IDCliente,PrezzoQuestoClienteLire,PrezzoQuestoClienteEuro) '+
                                                       ' values (:xIDEdizListino,:xIDCliente,:xPrezzoQuestoClienteLire,:xPrezzoQuestoClienteEuro)';
                                                  Q1.ParamByName('xIDEdizListino').asInteger:=QListinoEdizID.Value;
                                                  Q1.ParamByName('xIDCliente').asInteger:=xIDCliente;
                                                  Q1.ParamByName('xPrezzoQuestoClienteLire').asInteger:=round(ModPrezzoForm.PrezzoLire.value);
                                                  Q1.ParamByName('xPrezzoQuestoClienteEuro').asFloat:=ModPrezzoForm.PrezzoEuro.value;}
                         Q1.SQL.text := 'insert into Ann_ListinoClienti (IDEdizListino,IDCliente,PrezzoQuestoClienteLire,PrezzoQuestoClienteEuro) ' +
                              ' values (:xIDEdizListino:,:xIDCliente:,:xPrezzoQuestoClienteLire:,:xPrezzoQuestoClienteEuro:)';
                         Q1.ParamByName['xIDEdizListino'] := QListinoEdiz.FieldByName('ID').AsInteger;
                         Q1.ParamByName['xIDCliente'] := xIDCliente;
                         Q1.ParamByName['xPrezzoQuestoClienteLire'] := Integer(round(ModPrezzoForm.PrezzoLire.value));
                         Q1.ParamByName['xPrezzoQuestoClienteEuro'] := ModPrezzoForm.PrezzoEuro.value;

                         Q1.ExecSQL;
                         DB.CommitTrans;
                         QListinoEdiz.Close;
                         QListinoEdiz.Open;
                    except
                         DB.RollbackTrans;
                         MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                    end;
               end;
          end else begin
               with Data do begin
                    DB.BeginTrans;
                    try
                         Q1.Close;
                         {                         Q1.SQL.text:='update Ann_ListinoClienti set PrezzoQuestoClienteLire=:xPrezzoQuestoClienteLire, '+
                                                       ' PrezzoQuestoClienteEuro=:xPrezzoQuestoClienteEuro '+
                                                       ' where ID='+QListinoEdizID_1.asString;
                                                  Q1.ParamByName('xPrezzoQuestoClienteLire').asInteger:=round(ModPrezzoForm.PrezzoLire.value);
                                                  Q1.ParamByName('xPrezzoQuestoClienteEuro').AsFloat:=ModPrezzoForm.PrezzoEuro.value;}
                         Q1.SQL.text := 'update Ann_ListinoClienti set PrezzoQuestoClienteLire=:xPrezzoQuestoClienteLire:, ' +
                              ' PrezzoQuestoClienteEuro=:xPrezzoQuestoClienteEuro: ' +
                              //' where ID=' + QListinoEdiz.FieldByName('ID').asString;
                         ' where IDEdizListino=' + QListinoEdiz.FieldByName('ID').asString;
                         Q1.ParamByName['xPrezzoQuestoClienteLire'] := Integer(round(ModPrezzoForm.PrezzoLire.value));
                         Q1.ParamByName['xPrezzoQuestoClienteEuro'] := ModPrezzoForm.PrezzoEuro.value;
                         Q1.ExecSQL;
                         DB.CommitTrans;
                         QListinoEdiz.Close;
                         QListinoEdiz.Open;
                    except
                         DB.RollbackTrans;
                         MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                    end;
               end;
          end;
     end;
     ModPrezzoForm.Free;

end;
//[/TONI20020920\]DEBUGOKFINE
//[/TONI20020729\]FINE

procedure TListinoAnnunciFrame.QEdizioniAfterScroll(DataSet: TDataSet);
begin
     QListinoEdiz.Close;
     QListinoEdiz.Parameters[0].Value := QEdizioniIDEdizione.Value;
     //QListinoEdiz.ParamByName['xIDEdizione'] :=  QEdizioniIDEdizione.Value;
     QListinoEdiz.Open;
end;

end.

