unit RaffrontoComp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables, StdCtrls, Grids, DBGrids, TeEngine, Series, ExtCtrls,
  TeeProcs, Chart, DBChart, Buttons, TB97, Wall, Mask, DBCtrls;

type
  TRaffrontoCompForm = class(TForm)
    Q1: TQuery;
    DsQ1: TDataSource;
    Q1IDMansione: TIntegerField;
    Q1Descrizione: TStringField;
    Q1Tipologia: TStringField;
    Q1Peso: TSmallintField;
    Q1ValRichiesto: TIntegerField;
    Q1ValNome: TIntegerField;
    Q1IDAnagrafica: TIntegerField;
    Q1Punteggio: TFloatField;
    QTot: TQuery;
    QTotTot: TFloatField;
    Panel1: TPanel;
    DBGrid17: TDBGrid;
    DBChart3: TDBChart;
    BarSeries1: TBarSeries;
    Panel2: TPanel;
    DBEdit12: TDBEdit;
    DBEdit14: TDBEdit;
    BitBtn2: TBitBtn;
    Panel3: TPanel;
    DBGrid1: TDBGrid;
    DBChart4: TDBChart;
    BarSeries3: TBarSeries;
    BarSeries2: TLineSeries;
    Wallpaper1: TPanel;
    ToolbarButton972: TToolbarButton97;
    ToolbarButton973: TToolbarButton97;
    TCompAnagIDX: TTable;
    TCompAnagIDXID: TAutoIncField;
    TCompAnagIDXIDCompetenza: TIntegerField;
    TCompAnagIDXIDAnagrafica: TIntegerField;
    TCompAnagIDXValore: TIntegerField;
    Q1IDCompetenza: TIntegerField;
    procedure Q1CalcFields(DataSet: TDataSet);
    procedure ToolbarButton973Click(Sender: TObject);
    procedure ToolbarButton972Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  RaffrontoCompForm: TRaffrontoCompForm;

implementation

uses ModuloDati2, ModuloDati, SuggFormaz;

{$R *.DFM}


procedure TRaffrontoCompForm.Q1CalcFields(DataSet: TDataSet);
begin
     Q1Punteggio.Value:=(Q1ValRichiesto.Value-Q1ValNOme.Value)*Q1Peso.Value/100;
end;

procedure TRaffrontoCompForm.ToolbarButton973Click(Sender: TObject);
begin
     SuggFormazForm:=TSuggFormazForm.create(self);
     if SuggFormazForm.Q1.RecordCount=0 then
        MessageDlg('Nessuna competenza con valori inferiori ai richiesti',mtError,[mbOK],0)
     else SuggFormazForm.ShowModal;
     SuggFormazForm.Free;
end;

procedure TRaffrontoCompForm.ToolbarButton972Click(Sender: TObject);
var xVal:string;
begin
     TCompAnagIDX.FindKey([Data.TAnagraficaID.Value,Q1IDCompetenza.Value]);
     xVal:=TCompAnagIDXValore.asString;
     InputQuery(Q1Descrizione.asString,'valore:',xVal);
     TCompAnagIDX.Edit;
     TCompAnagIDXValore.Value:=StrToInt(xVal);
     TCompAnagIDX.Post;
     Q1.Close;
     Q1.Open;
     QTot.Close;
     QTot.Open;
end;


procedure TRaffrontoCompForm.FormShow(Sender: TObject);
begin
     Data2.xRaffrontoForm:=True;
end;

end.
