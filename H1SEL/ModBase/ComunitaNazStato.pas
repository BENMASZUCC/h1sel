unit ComunitaNazStato;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ExtCtrls, dxCntner, dxTL, dxDBCtrl, dxDBGrid, Db, ADODB, TB97;

type
     TComunitaNazStatoForm = class(TForm)
          QComunitaNaz_Stato: TADOQuery;
          DSQComunitaNaz_Stato: TDataSource;
          QComunitaNaz_StatoID: TAutoIncField;
          QComunitaNaz_StatoDescrizione: TStringField;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1Column1: TdxDBGridColumn;
          ToolbarButton971: TToolbarButton97;
          ToolbarButton972: TToolbarButton97;
    Panel1: TPanel;
          procedure ToolbarButton971Click(Sender: TObject);
          procedure ToolbarButton972Click(Sender: TObject);
          procedure FormShow(Sender: TObject);
     private
    { Private declarations }
     public
    { Public declarations }
     end;

var
     ComunitaNazStatoForm: TComunitaNazStatoForm;

implementation

uses ModuloDati, main;

{$R *.DFM}

procedure TComunitaNazStatoForm.ToolbarButton971Click(Sender: TObject);
begin
     ComunitaNazStatoForm.ModalResult := mrOk;
end;

procedure TComunitaNazStatoForm.ToolbarButton972Click(Sender: TObject);
begin
     ComunitaNazStatoForm.ModalResult := mrCancel;
end;

procedure TComunitaNazStatoForm.FormShow(Sender: TObject);
begin
//Grafica
     ComunitaNazStatoForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;

     QComunitaNaz_Stato.Open;
end;

end.

