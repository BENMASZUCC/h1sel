unit U_ADOLinkCl;

interface
uses
  adodb, Classes, db;

const
  BEGIN_AUTO_ADD='/*BEGIN_AUTO_ADD*/';
  END_AUTO_ADD='/*END_AUTO_ADD*/';

type


  TADOLinkedConnection = class (TADOCOnnection)
  public
    procedure StartTransaction;
    procedure Commit;
    procedure RollBack;
  end;

  TADOLinkedTable = class (TADOTable)
    private
      FMasterDataSet : TCustomAdoDataSet;
      FDetailDataSet: TList;
      FLinkedMasterField,FLinkedDetailField : string;
      //aggiunto da Silverio il 01/10/2002
      function AddWhereClause(aSql:string;aFieldName,aValue:string):string;
      //aggiunto da Silverio e Toni il 02/10/2002
      function ClearSql(aSql:string):string;
    protected
      procedure SetFMasterDataSet (val : TCustomAdoDataSet);
      function GetDetailDataSet (pos : WORD) :TCustomADODataSet;virtual;
      procedure SetFLinkedMasterField (val : string);
      function GetFLInkedMasterFieldName : string;
      procedure SetFLInkedDetailField (val : string);
      function GetFLInkedDetailField : string;
      procedure DoAfterScroll; override;
//      procedure DoBeforePost;override;
    public
      constructor Create(Aowner:Tcomponent); override;
      destructor destroy; override;
      procedure SetDetailDataSet(const Value: TCustomAdoDataSet);virtual;
      procedure EmptyTable;
      function FindNearestADO(const arr :Variant):BOOLEAN;
      function FindKeyADO (const arr: Variant):BOOLEAN;
      property DetailDataSet[pos:WORD] :TCustomADODataSet read GetDetailDataSet;
    published
      //Link Master
      property MasterDataSet : TCustomAdoDataSet read FMasterDataSet write SetFMasterDataSet;
      property LinkedMasterField : string read GetFLInkedMasterFieldName write SetFLinkedMasterField;
      property LinkedDetailField : string read GetFLInkedDetailField write SetFLInkedDetailField;
  end;

  TThreadADOLinkedTable = class (TADOLinkedTable)
  private
    FThreads : WORD;
  protected
    procedure DoAfterScroll; override;
    function GetDetailDataSet (pos : WORD) :TCustomADODataSet;override;
  public
    procedure SetDetailDataSet(const Value: TCustomAdoDataSet);override;
    procedure IncThreads;virtual;
    destructor Destroy;override;
  end;


    TADOLinkedQuery = class (TADOQuery)
    private
      FMasterDataSet : TCustomAdoDataSet;
      FDetailDataSet: TList;
      FLinkedMasterField,FLinkedDetailField :string;
      FOriginalSQL : TStrings;
      fUseFilter:Boolean;
      fActualWhereClause:string;
      procedure SetParamByName(name: string; val: Variant);
      function GetUseFilter: Boolean;
      procedure SetUseFilter(const Value: Boolean);
      //aggiunto da Silverio il 01/10/2002
      function AddWhereClause(aSql:string;aFieldName,aValue:string):string;
      //aggiunto da Silverio e Toni il 02/10/2002
      function ClearSql(aSql:string):string;
    protected
      procedure SetFMasterDataSet (val : TCustomAdoDataSet);
      function GetFLInkedMasterFieldName : string;
      procedure SetFLinkedMasterField (val : string);
      procedure SetFLInkedDetailField (val : string);
      function GetFLInkedDetailField : string;
      procedure DoAfterScroll; override;
      procedure DoBeforeOpen; override;
//      procedure DoBeforePost;override;
      function GetDetailDataSet (pos : WORD) :TCustomADODataSet;
      function GetOriginalSQL: TStrings;
      procedure SetOriginalSQL(const Value: TStrings);
    public
      constructor Create(Aowner:Tcomponent); override;
      destructor destroy; override;
      procedure SetDetailDataSet(const Value: TCustomAdoDataSet);
      property DetailDataSet[pos:WORD] :TCustomADODataSet read GetDetailDataSet;
      procedure SetSQLText (const valArray :array of Variant);
      procedure ReloadSQL;virtual;
      property ParamByName [pos :String]: Variant write SetParamByName;
    published
      //Link Master
      property MasterDataSet : TCustomAdoDataSet read FMasterDataSet write SetFMasterDataSet;
      property LinkedMasterField : string read GetFLInkedMasterFieldName write SetFLinkedMasterField;
      property LinkedDetailField : string read GetFLInkedDetailField write SetFLInkedDetailField;
      property OriginalSQL : TStrings read GetOriginalSQL write SetOriginalSQL;
      //aggiunto da Silverio il 01/10/2002
      property UseFilter : Boolean read GetUseFilter write SetUseFilter;
  end;

procedure Register;

implementation

uses
  SysUtils, U_ADOLinkThreads;

constructor TADOLinkedTable.create(Aowner: Tcomponent);
begin
  inherited;
  //Creo lista Detail
  FDetailDataSet:= TList.Create;
  FLinkedMasterField := '';
  FLinkedDetailField := '';
  FMasterDataSet := nil;
end;

destructor TADOLinkedTable.destroy;
var
  lauf : WORD;
begin
  if FDetailDataSet.Count >0 then //Setta rif a me (master) dei miei dettagli a null
  begin
    for lauf :=FDetailDataSet.Count -1 downto 0 do
    begin
      if (TCustomADODataSet (FDetailDataSet.Items[lauf]) is TADOLinkedTable) then
        TADOLinkedTable (FDetailDataSet.Items[lauf]).MasterDataSet :=nil
      else
        if (TCustomADODataSet (FDetailDataSet.Items[lauf]) is TADOLinkedQuery) then
          TADOLinkedQuery (FDetailDataSet.Items[lauf]).MasterDataSet :=nil;
    end;
    FDetailDataSet.Clear;
  end;
  FDetailDataSet.Free;
  //Setto rif a me del MIO master a null
  if Assigned (FMasterDataSet) then
  begin
    //Cerca puntatore a me
    lauf:=0;
    if FMasterDataSet is TADOLinkedTable then
    begin
      while TCustomADODataSet(TADOlinkedTable (FMasterDataSet).FDetailDataSet.Items[lauf]).Name<> TCustomADODataSet(self).Name do
        inc(lauf);
      //togli
      TADOLInkedTable (FMasterDataSet).FDetailDataSet.Delete (lauf);
      //Tolgo rif. a master
    end
    else
      if FMasterDataSet is TADOLinkedQuery then
      begin
        while TADOlinkedQuery (FMasterDataSet).FDetailDataSet.Items[lauf]<> self do
          inc(lauf);
        //togli
        TADOLInkedQuery (FMasterDataSet).FDetailDataSet.Delete (lauf);
        //Tolgo rif. a master
      end;
    FMasterDataSet :=nil;
  end;
  inherited;
end;

procedure TADOLinkedTable.DoAfterScroll;
var
  temp, whereStr: string;
  lauf : WORD;
  tempQ : TADOQuery;
  OldActive:BOOLEAN;
begin
  If FDetailDataSet.Count>0 then
  begin   //se presenti DS da agg
    if State in [dsBrowse] then
    begin
      for lauf :=0 to FDetailDataSet.Count -1 do
      begin
        if (TCustomADODataSet (FDetailDataSet[lauf]) Is TADOLinkedTable)  then
        begin //Se � una table
          if FindField (TADOLInkedTable (FDetailDataSet[lauf]).FLinkedMasterField) <> nil then
          begin
            TADOLInkedTable (FDetailDataSet[lauf]).Filtered := FALSE;
            if FieldByName (TADOLInkedTable (FDetailDataSet[lauf]).FLinkedMasterField).AsVariant <> NULL THEN
            begin
              if FieldByName (TADOLInkedTable (FDetailDataSet[lauf]).FLinkedMasterField).DataType <> ftString then
                TADOLInkedTable (FDetailDataSet[lauf]).Filter := '('+TADOLinkedTable(FDetailDataSet[lauf]).FLinkedDetailField+'='+FieldByName (TADOLInkedTable (FDetailDataSet[lauf]).FLinkedMasterField).AsString+')'
              else
                TADOLInkedTable (FDetailDataSet[lauf]).Filter := '('+TADOLinkedTable(FDetailDataSet[lauf]).FLinkedDetailField+'='''+FieldByName (TADOLInkedTable (FDetailDataSet[lauf]).FLinkedMasterField).AsString+''')';
            end
            else //TADOLInkedTable (FDetailDataSet[lauf]).Filtered := FALSE;
              TADOLInkedTable (FDetailDataSet[lauf]).Filter := '('+TADOLInkedQuery (FDetailDataSet[lauf]).FLinkedDetailField+'=NULL)';
            TADOLInkedTable (FDetailDataSet[lauf]).Filtered := TRUE;
          end;
        end
        else
        begin
          if FindField (TADOLInkedQuery (FDetailDataSet[lauf]).FLinkedMasterField) <> nil then
          begin
            if TADOLInkedQuery (FDetailDataSet[lauf]).UseFilter then
            begin
              TADOLInkedQuery (FDetailDataSet[lauf]).Filtered := FALSE;
              if FieldByName (TADOLInkedQuery (FDetailDataSet[lauf]).FLinkedMasterField).AsVariant <> NULL THEN
              begin
                if FieldByName (TADOLInkedQuery (FDetailDataSet[lauf]).FLinkedMasterField).DataType <> ftString then
                  TADOLInkedQuery (FDetailDataSet[lauf]).Filter := '('+TADOLInkedQuery (FDetailDataSet[lauf]).FLinkedDetailField+'='+FieldByName (TADOLInkedQuery (FDetailDataSet[lauf]).FLinkedMasterField).AsString+')'
                else
                  TADOLInkedQuery (FDetailDataSet[lauf]).Filter := '('+TADOLInkedQuery (FDetailDataSet[lauf]).FLinkedDetailField+'='''+FieldByName (TADOLInkedQuery (FDetailDataSet[lauf]).FLinkedMasterField).AsString+''')';
              end
              else
              begin
                TADOLInkedQuery (FDetailDataSet[lauf]).Filter := '('+TADOLInkedQuery (FDetailDataSet[lauf]).FLinkedDetailField+'=NULL)';
              end;
              TADOLInkedQuery (FDetailDataSet[lauf]).Filtered := TRUE;
            end else
            begin
              //aggiunto da Silverio il 01/10/2002
              OldActive:=TADOLInkedQuery (FDetailDataSet[lauf]).Active;
              TADOLInkedQuery (FDetailDataSet[lauf]).Active:=False;
              if FieldByName (TADOLInkedQuery (FDetailDataSet[lauf]).FLinkedMasterField).AsVariant <> NULL THEN
              begin
                if (TADOLInkedQuery (FDetailDataSet[lauf]).SQL.Text='') and (TADOLInkedQuery (FDetailDataSet[lauf]).OriginalSQL.Text<>'') then
                    TADOLInkedQuery (FDetailDataSet[lauf]).SQL.Text := TADOLInkedQuery (FDetailDataSet[lauf]).OriginalSQL.Text;
                if FieldByName (TADOLInkedQuery (FDetailDataSet[lauf]).FLinkedMasterField).DataType <> ftString then
                begin
                   TADOLInkedQuery (FDetailDataSet[lauf]).SQL.Text:=
                      AddWhereClause(TADOLInkedQuery (FDetailDataSet[lauf]).SQL.Text,
                      TADOLInkedQuery (FDetailDataSet[lauf]).FLinkedDetailField,
                      FieldByName (TADOLInkedQuery (FDetailDataSet[lauf]).FLinkedMasterField).AsString)
                end else
                begin
                   TADOLInkedQuery (FDetailDataSet[lauf]).SQL.Text:=
                      AddWhereClause(TADOLInkedQuery (FDetailDataSet[lauf]).SQL.Text,
                      TADOLInkedQuery (FDetailDataSet[lauf]).FLinkedDetailField,''''+
                      StringReplace (FieldByName (TADOLInkedQuery (FDetailDataSet[lauf]).FLinkedMasterField).AsString,'''','''''',[rfreplaceall]) +'''');
                end
              end else
              begin
                if (TADOLInkedQuery (FDetailDataSet[lauf]).SQL.Text='') and (TADOLInkedQuery (FDetailDataSet[lauf]).OriginalSQL.Text<>'') then
                    TADOLInkedQuery (FDetailDataSet[lauf]).SQL.Text := TADOLInkedQuery (FDetailDataSet[lauf]).OriginalSQL.Text;
                TADOLInkedQuery (FDetailDataSet[lauf]).SQL.Text:=
                   AddWhereClause(TADOLInkedQuery (FDetailDataSet[lauf]).SQL.Text,'','');
              end;
              TADOLInkedQuery (FDetailDataSet[lauf]).Active:=OldActive;
            end;
          end;
        end;
      end;
    end;
  end;
  inherited;
end;

function TADOLinkedTable.GetFLInkedDetailField: string;
begin
  Result :=FLinkedDetailField;
end;

function TADOLinkedTable.GetFLInkedMasterFieldName: string;
begin
  Result :=FLinkedMasterField;
end;

procedure TADOLinkedTable.SetFLInkedDetailField(val: string);
begin
  if FLinkedDetailField<>val  then
//    if FindField (val)<> nil then
      FLinkedDetailField := val
//    else FLinkedDetailField:='';
end;

procedure TADOLinkedTable.SetFLinkedMasterField(val: string);
begin
  if FLinkedMasterField<>val  then
//    if Assigned (FMasterDataSet) then
//      if TCustomADODataSet (FMasterDataSet).FindField (val)<> nil then
        FLinkedMasterField := val
//      else FLinkedMasterField:=''
//    else FLinkedMasterField:='';
end;

procedure TADOLinkedTable.SetDetailDataSet(const Value: TCustomAdoDataSet);
var
  i : WORD;
  presente : BOOLEAN;
begin
  if FDetailDataSet.Count >0 then
  begin
    presente := FALSE;
    i:=0;
    while (i< FDetailDataSet.Count) and NOT presente do
    begin
      if FDetailDataSet.Items[i]=value then presente := TRUE;
      Inc(i);
    end;
    if not presente then FDetailDataSet.Add (value);
  end
  else FDetailDataSet.Add (value);
end;

function TADOLinkedQuery.GetDetailDataSet(pos: WORD): TCustomADODataSet;
begin
  if FDetailDataSet.Count >-1 then
  begin
    if (pos>=0) and (pos<=FDetailDataSet.Count-1) then
      if (TCustomADODataSet (FDetailDataSet.Items[pos]) Is  TADOLinkedTAble) then
        Result := TADOlinkedTable (FDetailDataSet.Items[pos])
      else
        Result := TADOlinkedQuery (FDetailDataSet.Items[pos]);
  end
  else Result := nil;
end;


function TADOLinkedTable.GetDetailDataSet(pos: WORD): TCustomADODataSet;
begin
  if FDetailDataSet.Count >-1 then
  begin
    if (pos>=0) and (pos<=FDetailDataSet.Count-1) then
      if (TCustomADODataSet (FDetailDataSet.Items[pos]) is TADOLinkedTAble) then
        Result := TADOlinkedTable (FDetailDataSet.Items[pos])
      else
        Result := TADOlinkedQuery (FDetailDataSet.Items[pos]);
  end
  else Result := nil;
end;

procedure TADOLinkedTable.SetFMasterDataSet (val : TCustomAdoDataSet);
begin
  if Assigned (val) then
  begin
    if (val <> FMAsterDataSet) and (val <> self) then
    begin
      //Setta riferimento a master (solo uno)
      FMasterDataSet := val;
      if (val is TADOLinkedTable) then
      begin
//        FADOLinkType := linkedTable;
        //Setta riferimento al detail
        TADOLinkedTable (FMAsterDataSet).SetDetailDataSet (self);
      end
      else
        if (val Is TADOLinkedQuery) then
        begin
//          FADOLinkType := linkedQuery;
            TADOLinkedQuery(FMasterDataset).SetDetailDataSet (self);
        end
        else
          FMAsterDataSet := nil;
    end
  end
  else
  begin
    FMasterDataSet := nil;
  end;
end;


function TADOLinkedTable.FindNearestADO(const arr :Variant):BOOLEAN;
var
  i:BYTE;
  campi: string;
begin
  //Cerca con indice selezionato
  if IndexName <>'' then
  begin
    if IndexFieldCount > 0 then
    begin
      campi := '';
      i := 0;
      if IndexFieldCount=1 then
      begin
        campi :=campi+ IndexFields [0].FieldName;
        Result := Locate (campi,arr[0],[loPartialKey,locaseinsensitive]);
      end
      else
      begin
        while i< IndexFieldCount do
        begin
          campi:= campi+ IndexFields [i].FieldName+';';
          Inc (i);
        end;
        Result := Locate (campi,arr,[loPartialKey,locaseinsensitive]);
      end;
    end
    else Result := FALSE;
  end
  else
  begin
    if IndexFieldNames <> '' then
      Result := Locate (IndexFieldNames,arr,[loPartialKey,locaseinsensitive])
    else Result := FALSE;
  end;
end;

function TADOLinkedTable.FindKeyADO(const arr: Variant):BOOLEAN;
var
  i:BYTE;
  campi : string;
begin
  //Cerca con indice selezionato
  if IndexName <>'' then
  begin
    if IndexFieldCount > 0 then
    begin
      campi := '';
      i := 0;
      if IndexFieldCount=1 then
      begin
        campi :=campi+ IndexFields [0].FieldName;
        Result := Locate (campi,arr[0],[locaseinsensitive]);
      end
      else
      begin
        while i< IndexFieldCount do
        begin
          campi:= campi+ IndexFields [i].FieldName+';';
          Inc (i);
        end;
        Result :=Locate (campi,arr,[locaseinsensitive]);
      end;
    end
    else Result := FALSE;
  end
  else
  begin
    if INdexFieldNames <> '' then
      Result :=Locate (IndexFieldNames,arr,[locaseinsensitive])
    else
    begin
      Result := FALSE;
    end;
  end;
end;

function TADOLinkedTable.ClearSql(aSql: string): string;
var pBeginTag:Integer;
begin
  aSQL:=StringReplace(Asql,#13#10,' ',[rfreplaceall]);
  pBeginTag:=Pos(BEGIN_AUTO_ADD,aSql);
  while pBeginTag<>0 do
  begin
    System.Delete(aSql,pBeginTag,(Pos(END_AUTO_ADD,aSql)-pBeginTag)+Length(END_AUTO_ADD));
    pBeginTag:=Pos(BEGIN_AUTO_ADD,aSql);
  end;
  result:=TrimRight (aSql);
end;

{ TADOLinkedQuery }

constructor TADOLinkedQuery.create(Aowner: Tcomponent);
begin
  inherited;
  FDetailDataSet := TList.Create;
  ForiginalSQl := TStringList.Create;
  TStringList(FOriginalSQL).OnChange := QueryChanged;
end;

destructor TADOLinkedQuery.destroy;
var
  lauf : WORD;
begin
  if FDetailDataSet.Count >0 then //Setta rif a master dei dettagli a null
  begin
    for lauf :=0 to FDetailDataSet.Count -1 do
      if TCustomADODataSet (FDetailDataSet.Items[lauf]) is TADOLinkedTable then
        TADOLinkedTable (FDetailDataSet.Items[lauf]).MasterDataSet :=nil
      else
        if TCustomADODataSet (FDetailDataSet.Items[lauf]) is TADOLinkedQuery then
          TADOLinkedQuery (FDetailDataSet.Items[lauf]).MasterDataSet :=nil;
    FDetailDataSet.Clear;
  end;
  FDetailDataSet.Free;
  //Setto rif a me del MIO master a null
  if Assigned (FMasterDataSet) then
  begin
    //Cerca puntatore a me
    lauf:=0;
    if FMasterDataSet is TADOLinkedTable then
    begin
      while TCustomADODataSet(TADOlinkedTable (FMasterDataSet).FDetailDataSet.Items[lauf]).Name<> TCustomADODataSet(self).Name do
        inc(lauf);
      //togli
      TADOLInkedTable (FMasterDataSet).FDetailDataSet.Delete (lauf);
      //Tolgo rif. a master
    end
    else
      if FMasterDataSet is TADOLinkedQuery then
      begin
        while TADOlinkedQuery (FMasterDataSet).FDetailDataSet.Items[lauf]<> self do
          inc(lauf);
        //togli
        TADOLInkedQuery (FMasterDataSet).FDetailDataSet.Delete (lauf);
        //Tolgo rif. a master
      end;
    FMasterDataSet :=nil;
  end;
  inherited;
end;

procedure TADOLinkedQuery.DoAfterScroll;

var
  temp,wherestr : string;
  lauf : WORD;

  //aggiunto da Silverio il 01/10/2002
  OldActive:Boolean;
begin
  inherited;
  //Controlla Pres. Master!!!!!!!!
  //DA FARE
  If FDetailDataSet.Count>0 then
  begin   //se presenti DS da agg
    if State in [dsBrowse] then
    begin
      for lauf :=0 to FDetailDataSet.Count -1 do
      begin
        if (TCustomADODataSet (FDetailDataSet[lauf]) Is TADOLinkedTable)  then
        begin //Se '� una table
          //gestione NULL
          if FindField (TADOLInkedTable (FDetailDataSet[lauf]).FLinkedMasterField) <> nil then
          begin
            TADOLInkedTable (FDetailDataSet[lauf]).Filtered := FALSE;
            if FieldByName (TADOLInkedTable (FDetailDataSet[lauf]).FLinkedMasterField).AsVariant <> NULL THEN
            begin
              if FieldByName (TADOLInkedTable (FDetailDataSet[lauf]).FLinkedMasterField).DataType <> ftString then
                TADOLInkedTable (FDetailDataSet[lauf]).Filter := '('+TADOLinkedTable(FDetailDataSet[lauf]).FLinkedDetailField+'='+FieldByName (TADOLInkedTable (FDetailDataSet[lauf]).FLinkedMasterField).AsString+')'
              else
                TADOLInkedTable (FDetailDataSet[lauf]).Filter := '('+TADOLinkedTable(FDetailDataSet[lauf]).FLinkedDetailField+'='''+FieldByName (TADOLInkedTable (FDetailDataSet[lauf]).FLinkedMasterField).AsString+''')';
            end
            else
              TADOLInkedTable (FDetailDataSet[lauf]).Filter := '('+TADOLInkedQuery (FDetailDataSet[lauf]).FLinkedDetailField+'=NULL)';
            TADOLInkedTable (FDetailDataSet[lauf]).Filtered := TRUE;
          end;
        end else if (TCustomADODataSet (FDetailDataSet[lauf]) Is  TADOLinkedQuery) then
        begin
          if FindField (TADOLInkedQuery (FDetailDataSet[lauf]).FLinkedMasterField) <> nil then
          begin
            if TADOLInkedQuery (FDetailDataSet[lauf]).UseFilter then
            begin
              TADOLInkedQuery (FDetailDataSet[lauf]).Filtered := FALSE;
              if FieldByName (TADOLInkedQuery (FDetailDataSet[lauf]).FLinkedMasterField).AsVariant <> NULL THEN
              begin
                if FieldByName (TADOLInkedQuery (FDetailDataSet[lauf]).FLinkedMasterField).DataType <> ftString then
                  TADOLInkedQuery (FDetailDataSet[lauf]).Filter := '('+TADOLInkedQuery (FDetailDataSet[lauf]).FLinkedDetailField+'='+FieldByName (TADOLInkedQuery (FDetailDataSet[lauf]).FLinkedMasterField).AsString+')'
                else
                  TADOLInkedQuery (FDetailDataSet[lauf]).Filter := '('+TADOLInkedQuery (FDetailDataSet[lauf]).FLinkedDetailField+'='''+FieldByName (TADOLInkedQuery (FDetailDataSet[lauf]).FLinkedMasterField).AsString+''')';
              end
              else
              begin
                TADOLInkedQuery (FDetailDataSet[lauf]).Filter := '('+TADOLInkedQuery (FDetailDataSet[lauf]).FLinkedDetailField+'=NULL)';
              end;
              TADOLInkedQuery (FDetailDataSet[lauf]).Filtered := TRUE;
            end else
            begin
              //aggiunto da Silverio il 01/10/2002
              OldActive:=TADOLInkedQuery (FDetailDataSet[lauf]).Active;
              TADOLInkedQuery (FDetailDataSet[lauf]).Active:=False;
              if FieldByName (TADOLInkedQuery (FDetailDataSet[lauf]).FLinkedMasterField).AsVariant <> NULL THEN
              begin
                if (TADOLInkedQuery (FDetailDataSet[lauf]).SQL.Text='') and (TADOLInkedQuery (FDetailDataSet[lauf]).OriginalSQL.Text<>'') then
                    TADOLInkedQuery (FDetailDataSet[lauf]).SQL.Text := TADOLInkedQuery (FDetailDataSet[lauf]).OriginalSQL.Text;
                if FieldByName (TADOLInkedQuery (FDetailDataSet[lauf]).FLinkedMasterField).DataType <> ftString then
                begin
                   TADOLInkedQuery (FDetailDataSet[lauf]).SQL.Text:=
                      AddWhereClause(TADOLInkedQuery (FDetailDataSet[lauf]).SQL.Text,
                      TADOLInkedQuery (FDetailDataSet[lauf]).FLinkedDetailField,
                      FieldByName (TADOLInkedQuery (FDetailDataSet[lauf]).FLinkedMasterField).AsString)
                end else
                begin
                   TADOLInkedQuery (FDetailDataSet[lauf]).SQL.Text:=
                      AddWhereClause(TADOLInkedQuery (FDetailDataSet[lauf]).SQL.Text,
                      TADOLInkedQuery (FDetailDataSet[lauf]).FLinkedDetailField,''''+
                      StringReplace (FieldByName (TADOLInkedQuery (FDetailDataSet[lauf]).FLinkedMasterField).AsString,'''','''''',[rfreplaceall]) +'''');
                end
              end else
              begin
                if (TADOLInkedQuery (FDetailDataSet[lauf]).SQL.Text='') and (TADOLInkedQuery (FDetailDataSet[lauf]).OriginalSQL.Text<>'') then
                    TADOLInkedQuery (FDetailDataSet[lauf]).SQL.Text := TADOLInkedQuery (FDetailDataSet[lauf]).OriginalSQL.Text;
                TADOLInkedQuery (FDetailDataSet[lauf]).SQL.Text:=
                   AddWhereClause(TADOLInkedQuery (FDetailDataSet[lauf]).SQL.Text,'','');
              end;
              TADOLInkedQuery (FDetailDataSet[lauf]).Active:=OldActive;
            end;
          end;
        end;
      end;
    end;
  end;
end;

procedure TADOLinkedQuery.SetDetailDataSet(const Value: TCustomAdoDataSet);
var
  presente : BOOLEAN;
  i:WORD;
begin
  if FDetailDataSet.Count >0 then
  begin
    presente := FALSE;
    i:=0;
    while (i< FDetailDataSet.Count) and NOT presente do
    begin
      if FDetailDataSet.Items[i]=value then presente := TRUE;
      Inc(i);
    end;
    if not presente then FDetailDataSet.Add (value);
  end
  else FDetailDataSet.Add (value);
end;

procedure TADOLinkedQuery.SetFMasterDataSet (val : TCustomAdoDataSet);
begin
  if Assigned (val) then
  begin
    if (val <> FMAsterDataSet) and (val <> self) then
    begin
      //Setta riferimento a master (solo uno)
      FMasterDataSet := val;
      if (val Is TADOLinkedTable) then
      begin
//        FADOLinkType := linkedTable;
        //Setta riferimento al detail
        TADOLinkedTable (FMAsterDataSet).SetDetailDataSet (self);
      end
      else
        if (val Is TADOLinkedQuery) then
        begin
//          FADOLinkType := linkedQuery;
            TADOLinkedQuery(FMasterDataset).SetDetailDataSet (self);
        end
        else
          FMAsterDataSet := nil;
    end
  end
  else
  begin
    FMasterDataSet := nil;
  end;
end;

function TADOLinkedQuery.GetFLInkedMasterFieldName: string;
begin
  Result :=FLinkedMasterField;
end;

procedure TADOLinkedQuery.SetFLInkedDetailField(val: string);
begin
  if FLinkedDetailField<>val  then
//    if FindField (val)<> nil then
      FLinkedDetailField := val
//    else FLinkedDetailField:='';
end;

procedure TADOLinkedQuery.SetFLinkedMasterField(val: string);
begin
  if FLinkedMasterField<>val  then
    FLinkedMasterField := val
end;

function TADOLinkedQuery.GetFLInkedDetailField: string;
begin
  Result :=FLinkedDetailField;
end;

procedure TADOLinkedQuery.SetSQLText  (const valArray :array of Variant);
var
  lauf:BYTE;
  strSQL : string;
begin
  //Prendi str SQL originale
  strSQL := LowerCase (valArray[0]);
  //Sost. nomi par. con valori
  for lauf :=1 to Length (valArray)-1 do
  begin
    case VarType (valArray[lauf]) of
      varString :strSQL:=StringReplace (strSQL,Copy (strSQL,Pos (':',strSQL),Pos (':',Copy (strSQL,Pos (':',strSQL)+1,Length (strSQL)))+1),''''+valArray[lauf]+'''',[rfReplaceAll] + [rfIgnoreCase]);
      varDate:strSQL:=StringReplace (strSQL,Copy (strSQL,Pos (':',strSQL),Pos (':',Copy (strSQL,Pos (':',strSQL)+1,Length (strSQL)))+1),''''+StringReplace (TimeToStr (valArray[lauf]),'.',':',[rfReplaceAll]) + ' ' + DateToStr (valArray[lauf])+'''',[rfReplaceAll] + [rfIgnoreCase]);
{      varSingle,
      varDouble : strSQL:=StringReplace (strSQL,Copy (strSQL,Pos (name,strSQL)-1,Pos (':',Copy (strSQL,Pos (name,strSQL),Length (strSQL)))+1),''''+StringReplace (FloatToStr (valArray[lauf]),',','.',[rfReplaceAll])+'''',[rfReplaceAll] + [rfIgnoreCase]);
      varInteger: strSQL:=StringReplace (strSQL,Copy (strSQL,Pos (name,strSQL)-1,Pos (':',Copy (strSQL,Pos (name,strSQL),Length (strSQL)))+1),INtToStr (valArray[lauf]),[rfReplaceAll] + [rfIgnoreCase]);}
    else
      strSQL:=StringReplace (strSQL,Copy (strSQL,Pos (':',strSQL),Pos (':',Copy (strSQL,Pos (':',strSQL)+1,Length (strSQL)))+1),valArray[lauf],[rfReplaceAll] + [rfIgnoreCase])  end;
    end;
  if State <> dsInactive then
    Close;
  Self.SQL.Text := strSQL;
end;

procedure TADOLinkedQuery.SetParamByName(name: string; val: Variant);
var
  temp : string;
begin
  temp := SQL.Text;
  name := lowercase (name);
  if (name <> '') and (pos (LowerCase (name),LowerCase (temp))>0) then
  begin
    case VarType (val) of
{      varString : temp:=StringReplace (temp,Copy (temp,Pos (Lowercase (name),lowercase(temp))-1,Pos (':',Copy (temp,Pos (Lowercase (name),lowercase(temp)),Length (temp)))+1),''''+TrimRight (val)+'''',[rfReplaceAll] + [rfIgnoreCase]);
      varDate : temp:=StringReplace (temp,Copy (temp,Pos (Lowercase (name),lowercase(temp))-1,Pos (':',Copy (temp,Pos (Lowercase (name),lowercase(temp)),Length (temp)))+1),''''+StringReplace (TimeToStr (val),'.',':',[rfReplaceAll]) + ' ' + DateToStr (val)+'''',[rfReplaceAll] + [rfIgnoreCase]);
      varSingle,varDouble : temp:=StringReplace (temp,Copy (temp,Pos (Lowercase (name),lowercase(temp))-1,Pos (':',Copy (temp,Pos (Lowercase (name),lowercase(temp)),Length (temp)))+1),''''+StringReplace (FloatToStr (val),',','.',[rfReplaceAll])+'''',[rfReplaceAll] + [rfIgnoreCase]);
      varInteger: temp:=StringReplace (temp,Copy (temp,Pos (Lowercase (name),lowercase(temp))-1,Pos (':',Copy (temp,Pos (Lowercase (name),lowercase(temp)),Length (temp)))+1),INtToStr (val),[rfReplaceAll] + [rfIgnoreCase]);
      varBoolean:
       if val then
          temp:=StringReplace (temp,Copy (temp,Pos (Lowercase (name),lowercase(temp))-1,Pos (':',Copy (temp,Pos (Lowercase (name),lowercase(temp)),Length (temp)))+1),IntTOstr (1),[rfReplaceAll] + [rfIgnoreCase])
       else
          temp:=StringReplace (temp,Copy (temp,Pos (Lowercase (name),lowercase(temp))-1,Pos (':',Copy (temp,Pos (Lowercase (name),lowercase(temp)),Length (temp)))+1),IntToStr (0),[rfReplaceAll] + [rfIgnoreCase]);}
      varString : temp:=StringReplace (temp,Copy (temp,Pos (Lowercase (name),lowercase(temp))-1,Pos (':',Copy (temp,Pos (Lowercase (name),lowercase(temp)),Length (temp)))+1),''''+StringReplace (TrimRight (val),'''','''''',[rfreplaceall])+'''',[rfReplaceAll] + [rfIgnoreCase]);
      varDate : temp:=StringReplace (temp,Copy (temp,Pos (Lowercase (name),lowercase(temp))-1,Pos (':',Copy (temp,Pos (Lowercase (name),lowercase(temp)),Length (temp)))+1),''''+StringReplace (TimeToStr (val),'.',':',[rfReplaceAll]) + ' ' + DateToStr (val)+'''',[rfReplaceAll] + [rfIgnoreCase]);
      varSingle,varDouble : temp:=StringReplace (temp,Copy (temp,Pos (Lowercase (name),lowercase(temp))-1,Pos (':',Copy (temp,Pos (Lowercase (name),lowercase(temp)),Length (temp)))+1),''''+StringReplace (FloatToStr (val),',','.',[rfReplaceAll])+'''',[rfReplaceAll] + [rfIgnoreCase]);
      varInteger: temp:=StringReplace (temp,Copy (temp,Pos (Lowercase (name),lowercase(temp))-1,Pos (':',Copy (temp,Pos (Lowercase (name),lowercase(temp)),Length (temp)))+1),INtToStr (val),[rfReplaceAll] + [rfIgnoreCase]);
      varBoolean:
       if val then
          temp:=StringReplace (temp,Copy (temp,Pos (Lowercase (name),lowercase(temp))-1,Pos (':',Copy (temp,Pos (Lowercase (name),lowercase(temp)),Length (temp)))+1),IntTOstr (1),[rfReplaceAll] + [rfIgnoreCase])
       else
          temp:=StringReplace (temp,Copy (temp,Pos (Lowercase (name),lowercase(temp))-1,Pos (':',Copy (temp,Pos (Lowercase (name),lowercase(temp)),Length (temp)))+1),IntToStr (0),[rfReplaceAll] + [rfIgnoreCase]);
    end;
    if State <> dsInactive then
      Close;
    SQL.Text := temp;
  end;
end;

//Ricarica Stringa SQL originale
procedure TADOLinkedQuery.ReloadSQL;
begin
  if (State <> dsInactive) and (RecordCount>0) then Close;
  if FOriginalSQL.Text <>'' then SQL.Text := FOriginalSQL.Text;
end;

function TADOLinkedQuery.GetOriginalSQL: TStrings;
begin
  Result := FOriginalSQL;
end;

procedure TADOLinkedQuery.SetOriginalSQL(const Value: TStrings);
begin
  FOriginalSQL.Assign(Value);
end;

function TADOLinkedQuery.GetUseFilter: Boolean;
begin
  result:=fUseFilter;
end;

procedure TADOLinkedQuery.SetUseFilter(const Value: Boolean);
begin
  if Value<>fUseFilter then
  begin
    fUseFilter:=Value;
  end;
end;

function TADOLinkedQuery.AddWhereClause(aSql,aFieldName,aValue:string): string;
var pwhere:Integer;
    pgroupby, porderby : INTEGER;
    pOther:Integer;
    pFieldName:Integer;
    tbName:string;
    i:Integer;
    c:Char;
begin

  aSql:=ClearSql(aSql);
  if aFieldName<>'' then
  begin
    pFieldName:=pos('.'+Lowercase (aFieldName),Lowercase (aSql));
    tbName:='';
    if pFieldName>0 then
    begin
      i:=pFieldName-1;
      c:=aSQL[i];
      while (i>0)and ((c in ['A'..'Z','_'])or(c in ['a'..'z'])or(c in ['0'..'9'])) do
      begin
        tbName:=c+tbName;
        i:=i-1;
        c:=aSQL[i];
      end;
    end;

    if tbName<>'' then
      aFieldName:=tbName+'.'+aFieldName;
  end;
  pWhere:= Pos('where',LowerCase(aSql));
  pOther:=length(aSql);
  pGroupby:= pos ('group', lowercase (aSQL));
  pOrderby:= pos ('order', lowercase (aSQL));

  if pGroupby>0 then
    pOther:=pGroupby-1
  else if pOrderby>0 then
    pOther:=pOrderby-1;

  if pwhere>0 then
  begin
    System.Insert(BEGIN_AUTO_ADD+' ( '+END_AUTO_ADD,aSql,pWhere+5);
    pOther:=pOther+Length(BEGIN_AUTO_ADD+' ( '+END_AUTO_ADD)+1;
    if aFieldName<>'' then
      System.insert (BEGIN_AUTO_ADD+' ) '+END_AUTO_ADD+BEGIN_AUTO_ADD+' AND '+'('+aFieldName+'='+aValue+')'+END_AUTO_ADD,aSQL,pOther)
    else
      System.insert (BEGIN_AUTO_ADD+' ) AND (1=0) '+END_AUTO_ADD,aSQL,pOther);
    result:=aSql;
  end else
  begin
    if aFieldName<>'' then
      System.Insert(BEGIN_AUTO_ADD+' WHERE ('+aFieldName+'='+aValue+')'+END_AUTO_ADD,aSql,pOther+1)
    else
      System.insert (BEGIN_AUTO_ADD+' WHERE (1=0) '+END_AUTO_ADD,aSQL,pOther+1);
    result:=aSql;
  end;
end;


function TADOLinkedQuery.ClearSql(aSql: string): string;
var pBeginTag:Integer;
begin
  aSQL:=StringReplace(Asql,#13#10,' ',[rfreplaceall]);
  pBeginTag:=Pos(BEGIN_AUTO_ADD,aSql);
  while pBeginTag<>0 do
  begin
    System.Delete(aSql,pBeginTag,(Pos(END_AUTO_ADD,aSql)-pBeginTag)+Length(END_AUTO_ADD));
    pBeginTag:=Pos(BEGIN_AUTO_ADD,aSql);
  end;
  result:=TrimRight (aSql);
end;

procedure TADOLinkedQuery.DoBeforeOpen;
begin
  inherited;
  If FMasterDataSet <> nil then
  begin
    if FMasterDataSet.RecordCount=0 then
    begin
      if (SQL.Text='') and (OriginalSQL.Text<>'') then
          SQL.Text := OriginalSQL.Text;
      SQL.Text:=AddWhereClause(SQL.Text,'','');
    end;
  end;
end;

{procedure TADOLinkedQuery.DoBeforePost;
var
  i:BYTE;
begin
  for i:=0 to Fields.Count -1 do
  begin
    if Fields[i].DataType  = ftString then
      Fields[i].AsString := StringReplace (Fields[i].AsString,'''','''',[rfreplaceall]);
  end;
  inherited;
end;}

{TThreadADOLinkedTable}
destructor TThreadADOLinkedTable.Destroy;
var
  lauf : WORD;
begin
  //tolgo rif ai detagli
  if FDetailDataSet.Count >0 then
    For lauf := 0 to FDetailDataSet.Count -1 do
    begin
      TADOLinkedDetailThread (FDetailDataSet[lauf]).Free;
      //e tolgo rif dei dettagli a me
      if TADOLinkedDetailThread (FDetailDataSet[lauf]).DetailDataSet is TADOLinkedTable then
        TADOLinkedTable (TADOLinkedDetailThread (FDetailDataSet[lauf]).DetailDataSet).FMasterDataSet :=nil
      else
        if TADOLinkedDetailThread (FDetailDataSet[lauf]).DetailDataSet is TADOLinkedQuery then
          TADOLinkedQuery (TADOLinkedDetailThread (FDetailDataSet[lauf]).DetailDataSet).FMasterDataSet :=nil;
      FDetailDataSet.Clear;
    end;
  if Assigned (FMasterDataSet) then
  begin
    if (FMasterDataSet is TADOLinkedTable) then
    begin
      //Cerca puntatore a me
      lauf:=0;
      while TADOLinkedDetailThread (TADOlinkedTable (FMasterDataSet).FDetailDataSet.Items[lauf]).DetailDataSet<> self do
        inc(lauf);
      with TADOLinkedDetailThread (TADOlinkedTable (FMasterDataSet).FDetailDataSet.Items[lauf])do
      begin
        Terminate;
        Resume;
        WaitFor;
        Free;
      end;
      TADOlinkedTable (FMasterDataSet).FDetailDataSet.Delete (lauf);
    end
    else
    begin
      if (FMAsterDataSet is TADOLinkedQuery) then
      begin
        lauf:=0;
        while TADOLinkedDetailThread (TADOlinkedQuery (FMasterDataSet).FDetailDataSet.Items[lauf]).DetailDataSet<> self do
          inc(lauf);
        TADOLinkedDetailThread (TADOlinkedQuery (FMasterDataSet).FDetailDataSet.Items[lauf]).Free;
        TADOlinkedQuery (FMasterDataSet).FDetailDataSet.Delete (lauf);
      end;
    end;
    //Tolgo rif. a master
    FMasterDataSet :=nil;
  end;
  inherited;
end;

procedure TThreadADOLinkedTable.DoAfterScroll;
var
  lauf : WORD;
begin
  If FDetailDataSet.Count>0 then
  begin   //se presenti DS da agg
    if State in [dsBrowse] then
    begin
      //Resetto contatore
      FThreads := 0;
      for lauf :=0 to FDetailDataSet.Count -1 do
      begin
        if TADOLinkedDetailThread (FDetailDataSet.Items[lauf]).DetailDataSet.State = dsBrowse then
          TADOLinkedDetailThread (FDetailDataSet.Items[lauf]).Resume
        else inc (FThreads);
      end;
      //ASpetta suspend dei thread
      while FThreads <= FDetailDataSet.Count-1 do;
    end;
  end;
end;

function TThreadADOLinkedTable.GetDetailDataSet(
  pos: WORD): TCustomADODataSet;
begin
  if FDetailDataSet.Count >-1 then
  begin
    if (pos>=0) and (pos<=FDetailDataSet.Count-1) then
      Result := TADOLinkedDetailThread (FDetailDataset.Items [pos]).DetailDataSet
    else
      Result := nil;
  end
  else Result := nil;
end;

procedure TThreadADOLinkedTable.IncThreads;
begin
  Inc (FThreads);
end;

procedure TThreadADOLinkedTable.SetDetailDataSet(const Value: TCustomAdoDataSet);
var
  thread : TADOLinkedDetailThread;
  presente : BOOLEAN;
  i:WORD;
begin
  if FDetailDataSet.Count >0 then
  begin
    presente := FALSE;
    i:=0;
    while (i< FDetailDataSet.Count) and NOT presente do
    begin
      if TADOLinkedDetailThread (FDetailDataSet.Items[i]).DetailDataSet.Name =value.Name then presente := TRUE;
      Inc(i);
    end;
    if not presente then
    begin
      //Crea thread, sopsendi esecuzione
      thread := TADOLinkedDetailThread.Create (TRUE);
      thread.DetailDataSet:= value;
      //Aggiungi lista
      FDetailDataSet.Add (thread);
    end;
  end
  else
  begin
    //Crea thread, sopsendi esecuzione
    thread := TADOLinkedDetailThread.Create (TRUE);
    thread.DetailDataSet:= value;
    //Aggiungi lista
    FDetailDataSet.Add (thread);
  end;
end;

{ TADOLinkedConnection }

procedure TADOLinkedConnection.Commit;
begin
  CommitTrans;
end;

procedure TADOLinkedConnection.RollBack;
begin
  RollbackTrans;
end;

procedure TADOLinkedConnection.StartTransaction;
begin
  BeginTrans;
end;

procedure TADOLinkedTable.EmptyTable;
begin
  DeleteRecords (arAll);
end;

procedure Register;
begin
  RegisterComponents ('Samples',[TADOLinkedTable,TADOLinkedQuery,TThreadADOLinkedTable,TADOLinkedConnection]);
end;

{procedure TADOLinkedTable.DoBeforePost;
var
  i:BYTE;
begin
  for i:=0 to Fields.Count -1 do
  begin
    if Fields[i].DataType  = ftString then
      Fields[i].AsString := StringReplace (Fields[i].AsString,'''','''''',[rfreplaceall]);
  end;
  inherited;
end;}

function TADOLinkedTable.AddWhereClause(aSql,aFieldName,aValue:string): string;
var pwhere:Integer;
    pgroupby, porderby : INTEGER;
    pOther:Integer;
    pFieldName:Integer;
    tbName:string;
    i:Integer;
    c:Char;
begin

  aSql:=ClearSql(aSql);
  if aFieldName<>'' then
  begin
    pFieldName:=pos('.'+Lowercase (aFieldName),Lowercase (aSql));
    tbName:='';
    if pFieldName>0 then
    begin
      i:=pFieldName-1;
      c:=aSQL[i];
      while (i>0)and ((c in ['A'..'Z','_'])or(c in ['a'..'z'])or(c in ['0'..'9'])) do
      begin
        tbName:=c+tbName;
        i:=i-1;
        c:=aSQL[i];
      end;
    end;

    if tbName<>'' then
      aFieldName:=tbName+'.'+aFieldName;
  end;
  pWhere:= Pos('where',LowerCase(aSql));
  pOther:=length(aSql);
  pGroupby:= pos ('group', lowercase (aSQL));
  pOrderby:= pos ('order', lowercase (aSQL));

  if pGroupby>0 then
    pOther:=pGroupby-1
  else if pOrderby>0 then
    pOther:=pOrderby-1;

  if pwhere>0 then
  begin
    System.Insert(BEGIN_AUTO_ADD+' ( '+END_AUTO_ADD,aSql,pWhere+5);
    pOther:=pOther+Length(BEGIN_AUTO_ADD+' ( '+END_AUTO_ADD)+1;
    if aFieldName<>'' then
      System.insert (BEGIN_AUTO_ADD+' ) '+END_AUTO_ADD+BEGIN_AUTO_ADD+' AND '+'('+aFieldName+'='+aValue+')'+END_AUTO_ADD,aSQL,pOther)
    else
      System.insert (BEGIN_AUTO_ADD+' ) AND (1=0) '+END_AUTO_ADD,aSQL,pOther);
    result:=aSql;
  end else
  begin
    if aFieldName<>'' then
      System.Insert(BEGIN_AUTO_ADD+' WHERE ('+aFieldName+'='+aValue+')'+END_AUTO_ADD,aSql,pOther+1)
    else
      System.insert (BEGIN_AUTO_ADD+' WHERE (1=0) '+END_AUTO_ADD,aSQL,pOther+1);
    result:=aSql;
  end;
end;

end.
