unit DataModule;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables;

type
  TData = class(TDataModule)
    Clienti: TTable;
    Luoghi: TTable;
    DsClienti: TDataSource;
    DsLuoghi: TDataSource;
    ClientiID: TIntegerField;
    ClientiDenominazione: TStringField;
    ClientiIndirizzo: TStringField;
    ClientiCap: TStringField;
    ClientiLocalita: TStringField;
    ClientiProvincia: TStringField;
    ClientiStessaSede: TBooleanField;
    ClientiIndirizzoLegale: TStringField;
    ClientiCapLegale: TStringField;
    ClientiLocalitaLegale: TStringField;
    ClientiProvinciaLegale: TStringField;
    ClientiCodiceFiscale: TStringField;
    ClientiPartitaIva: TStringField;
    ClientiTelefono: TStringField;
    ClientiFax: TStringField;
    ClientiNote: TMemoField;
    LuoghiID: TAutoIncField;
    LuoghiLocalit: TStringField;
    LuoghiProvincia: TStringField;
    Fatture: TTable;
    FattDett: TTable;
    DsFatture: TDataSource;
    DsFattDett: TDataSource;
    FattureID: TAutoIncField;
    FattureProgressivo: TIntegerField;
    FattureIDCliente: TIntegerField;
    FattureData: TDateField;
    FattureImporto: TFloatField;
    FattureIVA: TFloatField;
    FattureTotale: TFloatField;
    FattureScadenzaPagamento: TDateField;
    FatturePagata: TBooleanField;
    FatturePagataInData: TDateField;
    FattDettID: TAutoIncField;
    FattDettIDFattura: TIntegerField;
    FattDettIDCorsa: TIntegerField;
    FattDettDescrizione: TStringField;
    FattDettImporto: TFloatField;
    FattDettPercentualeIVA: TFloatField;
    FattDettIVA: TFloatField;
    FattDettTotale: TFloatField;
    DsFattClienti: TDataSource;
    FattClienti: TTable;
    FattClientiID: TAutoIncField;
    FattClientiProgressivo: TIntegerField;
    FattClientiIDCliente: TIntegerField;
    FattClientiData: TDateField;
    FattClientiImporto: TFloatField;
    FattClientiIVA: TFloatField;
    FattClientiTotale: TFloatField;
    FattClientiScadenzaPagamento: TDateField;
    FattClientiPagata: TBooleanField;
    FattClientiPagataInData: TDateField;
    ClientiModPagDefault: TStringField;
    ClientiLuogoDaDefault: TIntegerField;
    ClientiDescLuogoDa: TStringField;
    Progress: TTable;
    ProgressFatture: TSmallintField;
    DsProgress: TDataSource;
    FattureTipo: TStringField;
    FattureModalitaPagamento: TStringField;
    FattureAssegno: TStringField;
    ProgressEstrattiConto: TSmallintField;
    FattClientiTipo: TStringField;
    FattClientiModalitaPagamento: TStringField;
    FattClientiAssegno: TStringField;
    FattClientiGiorni: TIntegerField;
    CliDaFatt: TQuery;
    DsCliDaFatt: TDataSource;
    CliDaFattID: TIntegerField;
    CliDaFattDenominazione: TStringField;
    AnagPers: TTable;
    DsAnagPers: TDataSource;
    AnagPersID: TAutoIncField;
    AnagPersCognome: TStringField;
    AnagPersNome: TStringField;
    AnagPersLuogoNascita: TStringField;
    AnagPersDataNascita: TDateField;
    AnagPersIndirizzo: TStringField;
    AnagPersCAP: TStringField;
    AnagPersCitta: TStringField;
    AnagPersProvincia: TStringField;
    AnagPersDomicilioIndirizzo: TStringField;
    AnagPersDomicilioCAP: TStringField;
    AnagPersDomicilioComune: TStringField;
    AnagPersDomicilioProvincia: TStringField;
    AnagPersTelefono: TStringField;
    AnagPersCellulare: TStringField;
    AnagPersCodiceFiscale: TStringField;
    Eventi: TTable;
    DsEventi: TDataSource;
    EventiID: TAutoIncField;
    EventiData: TDateField;
    EventiIDCliente: TIntegerField;
    EventiDescrizione: TStringField;
    EventiCliente: TStringField;
    EventiPers: TTable;
    DsEventiPers: TDataSource;
    EventiPersID: TAutoIncField;
    EventiPersIDEvento: TIntegerField;
    EventiPersIDPersonale: TIntegerField;
    EventiPersDisposizioni: TStringField;
    EventiXPers: TTable;
    EventiXCliente: TTable;
    DsEventiXPers: TDataSource;
    DsEventiXCliente: TDataSource;
    EventiXPersID: TAutoIncField;
    EventiXPersIDEvento: TIntegerField;
    EventiXPersIDPersonale: TIntegerField;
    EventiXPersDisposizioni: TStringField;
    EventiXPersDescrizEvento: TStringField;
    EventiXClienteID: TAutoIncField;
    EventiXClienteData: TDateField;
    EventiXClienteIDCliente: TIntegerField;
    EventiXClienteDescrizione: TStringField;
    EventiPersRimborsoLire: TCurrencyField;
    EventiPersRimborsoEuro: TFloatField;
    EventiPersDataRimborso: TDateField;
    EventiXPersRimborsoLire: TCurrencyField;
    EventiXPersRimborsoEuro: TFloatField;
    EventiXPersDataRimborso: TDateField;
    EventiXClienteFatturata: TBooleanField;
    EventiXClienteFattNumero: TSmallintField;
    EventiDaFatt: TTable;
    EventiXClienteImportoLire: TCurrencyField;
    EventiXClienteImportoEuro: TFloatField;
    EventiImportoLire: TCurrencyField;
    EventiFatturata: TBooleanField;
    AnagPersFoto: TGraphicField;
    Ruoli: TTable;
    DsRuoli: TDataSource;
    RuoliID: TAutoIncField;
    RuoliRuolo: TStringField;
    EventiPersIDRuolo: TIntegerField;
    EventiPersRuolo: TStringField;
    EventiXPersIDRuolo: TIntegerField;
    EventiXPersRuolo: TStringField;
    EventiDaFattID: TAutoIncField;
    EventiDaFattData: TDateField;
    EventiDaFattIDCliente: TIntegerField;
    EventiDaFattDescrizione: TStringField;
    EventiDaFattImportoLire: TCurrencyField;
    EventiDaFattImportoEuro: TFloatField;
    EventiDaFattFatturata: TBooleanField;
    EventiDaFattFattNumero: TSmallintField;
    AnagPersLK: TTable;
    AnagPersLKID: TAutoIncField;
    AnagPersLKCognome: TStringField;
    AnagPersLKNome: TStringField;
    EventiPersPersonale: TStringField;
    procedure DsClientiStateChange(Sender: TObject);
    procedure DsFattureStateChange(Sender: TObject);
    procedure FattureAfterInsert(DataSet: TDataSet);
    procedure FattDettAfterPost(DataSet: TDataSet);
    procedure FattDettCalcFields(DataSet: TDataSet);
    procedure FattDettAfterInsert(DataSet: TDataSet);
    procedure DsFattDettStateChange(Sender: TObject);
    procedure FattureBeforePost(DataSet: TDataSet);
    procedure FattClientiCalcFields(DataSet: TDataSet);
    procedure FattureAfterPost(DataSet: TDataSet);
    procedure DsAnagPersStateChange(Sender: TObject);
    procedure DsEventiStateChange(Sender: TObject);
    procedure EventiAfterInsert(DataSet: TDataSet);
    procedure DsEventiPersStateChange(Sender: TObject);
    procedure EventiAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Data: TData;

implementation

uses Main;

{$R *.DFM}

procedure TData.DsClientiStateChange(Sender: TObject);
var b:boolean;
begin
     b:=DsClienti.State in [dsEdit,dsInsert];
     MainForm.TBClientiNew.Enabled:=not b;
     MainForm.TBClientiDel.Enabled:=not b;
     MainForm.TBClientiOK.Enabled:=b;
     MainForm.TBClientiCan.Enabled:=b;
end;

procedure TData.DsFattureStateChange(Sender: TObject);
var b:boolean;
begin
     b:=DsFatture.State in [dsEdit,dsInsert];
     MainForm.TBFattNew.Enabled:=not b;
     MainForm.TBFattDel.Enabled:=not b;
     MainForm.TBFattOK.Enabled:=b;
     MainForm.TBFattCancel.Enabled:=b;

     if DsFatture.State=dsInsert then begin
        MainForm.DBGrid7.Enabled:=False;
        MainForm.SbFattDettNew.Enabled:=False;
        MainForm.SbFattDettDel.Enabled:=False;
        MainForm.BitBtn2.Enabled:=False;
     end;
end;

procedure TData.FattureAfterInsert(DataSet: TDataSet);
begin
     FattureData.Value:=Date;
     FattureScadenzaPagamento.Value:=Date+30;
end;

procedure TData.FattDettAfterPost(DataSet: TDataSet);
var TempTotal, TempImporto:real;
    xPercIVA:double;
begin
    {aggiornamento TOTALI}
    FattDett.DisableControls;
    FattDett.First;
    TempImporto := 0;
    TempTotal := 0;
    while not FattDett.EOF do
    begin
      TempImporto := TempImporto + FattDettImporto.Value;
      TempTotal := TempTotal + FattDettTotale.Value;
      FattDett.Next;
    end;
    FattDett.EnableControls;
      Fatture.edit;
      FattureImporto.Value := TempImporto;
      FattureIVA.Value := TempImporto*0.2;
      FattureTotale.Value := TempTotal;
      Fatture.post;
end;

procedure TData.FattDettCalcFields(DataSet: TDataSet);
begin
     FattDettTotale.Value:=FattDettImporto.Value+(FattDettImporto.Value*FattDettPercentualeIVA.Value)/100;
end;

procedure TData.FattDettAfterInsert(DataSet: TDataSet);
begin
     FattDettPercentualeIVA.Value:=20;
end;

procedure TData.DsFattDettStateChange(Sender: TObject);
var b:boolean;
begin
     b:=DsFattDett.State in [dsEdit,dsInsert];
     MainForm.SbFattDettNew.Enabled:=not b;
     MainForm.SbFattDettDel.Enabled:=not b;
     MainForm.SbFattDettOK.Enabled:=b;
     MainForm.SbFattDettCan.Enabled:=b;
end;

procedure TData.FattureBeforePost(DataSet: TDataSet);
begin
     if DsFatture.State=dsInsert then begin
        // calcolo progressivo fattura
        Progress.Edit;
        if FattureTipo.Value='Fatt' then
        ProgressFatture.Value:=ProgressFatture.Value+1
        else ProgressEstrattiConto.Value:=ProgressEstrattiConto.Value+1;
        Progress.Post;

        if FattureTipo.Value='Fatt' then
        FattureProgressivo.Value:=ProgressFatture.Value
        else FattureProgressivo.Value:=ProgressEstrattiConto.Value;
     end;
     FattClienti.Close;
     FattClienti.Open;
end;


procedure TData.FattClientiCalcFields(DataSet: TDataSet);
var Year,Month,Day:Word;
    xdata:TdateTime;
begin
     if (FattClientiPagataInData.AsString='') and
        (Date>FattClientiScadenzaPagamento.value) then begin
     DecodeDate(Date-FattClientiScadenzaPagamento.value, Year, Month, Day);
     FattClientiGiorni.Value:=Day;
     end;
end;

procedure TData.FattureAfterPost(DataSet: TDataSet);
begin
     MainForm.DBGrid7.Enabled:=True;
     MainForm.SbFattDettNew.Enabled:=True;
     MainForm.SbFattDettDel.Enabled:=True;
     MainForm.BitBtn2.Enabled:=True;
     FattClienti.close;
     FattClienti.Open;
end;

procedure TData.DsAnagPersStateChange(Sender: TObject);
var b:boolean;
begin
     b:=DsAnagPers.State in [dsEdit,dsInsert];
     MainForm.TBAnagNew.Enabled:=not b;
     MainForm.TBAnagdel.Enabled:=not b;
     MainForm.TBAnagOK.Enabled:=b;
     MainForm.TBAnagCan.Enabled:=b;
end;

procedure TData.DsEventiStateChange(Sender: TObject);
var b:boolean;
begin
     b:=DsEventi.State in [dsEdit,dsInsert];
     MainForm.TBEventiNew.Enabled:=not b;
     MainForm.TBEventiDel.Enabled:=not b;
     MainForm.TBEventiOK.Enabled:=b;
     MainForm.TBEventiCan.Enabled:=b;
end;

procedure TData.EventiAfterInsert(DataSet: TDataSet);
begin
     EventiData.Value:=Date;
     EventiFatturata.Value:=False;
end;

procedure TData.DsEventiPersStateChange(Sender: TObject);
var b:boolean;
begin
     b:=DsEventiPers.State in [dsEdit,dsInsert];
     MainForm.TbBEventiPersNew.Enabled:=not b;
     MainForm.TbBEventiPersDel.Enabled:=not b;
     MainForm.TbBEventiPersOK.Enabled:=b;
     MainForm.TbBEventiPersCan.Enabled:=b;
end;

procedure TData.EventiAfterPost(DataSet: TDataSet);
begin
     MainForm.Panel15.Enabled:=True;
end;

end.
