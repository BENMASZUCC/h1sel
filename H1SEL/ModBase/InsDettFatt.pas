unit InsDettFatt;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ExtCtrls, StdCtrls, Buttons, Db, Grids, DBGrids, DBTables, Mask, DBCtrls,
     ComCtrls, AdvGrid, ADODB, U_ADOLinkCl, dxDBTLCl, dxGrClms, dxTL,
     dxDBCtrl, dxDBGrid, dxCntner, BaseGrid, TB97;

type
     TInsDettFattForm = class(TForm)
          DsQInsNonFatt: TDataSource;
          DsQCompensi: TDataSource;
          Panel3: TPanel;
          Label1: TLabel;
          DBEdit1: TDBEdit;
          PC: TPageControl;
          TSCompensi: TTabSheet;
          TSNoteSpese: TTabSheet;
          TSAnnunci: TTabSheet;
          DBGrid2_old: TDBGrid;
          RGOpz: TRadioGroup;
          Panel2: TPanel;
          PCSoggetti: TPageControl;
          TSSoggInseriti: TTabSheet;
          TSSoggCandRic: TTabSheet;
          Panel1: TPanel;
          Panel4: TPanel;
          DBGrid1: TDBGrid;
          Panel5: TPanel;
          Panel6: TPanel;
          ASG1: TAdvStringGrid;
          DsQNoteSpeseCli: TDataSource;
          Panel7: TPanel;
          DsQAnnunciCli: TDataSource;
          DBGrid4: TDBGrid;
          DsQAnnunciCli2: TDataSource;
          Panel8: TPanel;
          DBGrid5: TDBGrid;
          QCompensi: TADOLinkedQuery;
          QInsNonFatt_old: TADOLinkedQuery;
          TInserimentiABS: TADOLinkedTable;
          Q: TADOLinkedQuery;
          TAnagABS: TADOLinkedTable;
          QNoteSpeseCli: TADOLinkedQuery;
          QAnnunciCli: TADOLinkedQuery;
          QAnnunciCli2: TADOLinkedQuery;
          DBGrid2: TdxDBGrid;
          DBGrid2Tipo: TdxDBGridMaskColumn;
          DBGrid2Importo: TdxDBGridMaskColumn;
          DBGrid2IDFattura: TdxDBGridMaskColumn;
          DBGrid2RifRicerca: TdxDBGridMaskColumn;
          DBGrid2IDRicerca: TdxDBGridMaskColumn;
          DBGrid2Note: TdxDBGridMaskColumn;
          DBGrid2ID: TdxDBGridMaskColumn;
          DBGrid2RimbSpese: TdxDBGridCheckColumn;
          QInsNonFatt_oldID: TAutoIncField;
          QInsNonFatt_oldCognome: TStringField;
          QInsNonFatt_oldNome: TStringField;
          QInsNonFatt_oldSesso: TStringField;
          QInsNonFatt_oldDallaData: TDateTimeField;
          QInsNonFatt_oldProgressivo: TStringField;
          QInsNonFatt_oldDataInizio: TDateTimeField;
          QInsNonFatt_oldRuolo: TStringField;
          QInsNonFatt_oldStipendioLordo: TFloatField;
          QInsNonFatt_oldidric: TAutoIncField;
          QInsNonFatt: TADOQuery;
          QInsNonFattID: TAutoIncField;
          QInsNonFattCognome: TStringField;
          QInsNonFattNome: TStringField;
          QInsNonFattSesso: TStringField;
          QInsNonFattDallaData: TDateTimeField;
          QInsNonFattProgressivo: TStringField;
          QInsNonFattDataInizio: TDateTimeField;
          QInsNonFattRuolo: TStringField;
          QInsNonFattStipendioLordo: TFloatField;
          QInsNonFattidric: TAutoIncField;
          QNoteSpeseCliID: TAutoIncField;
          QNoteSpeseCliData: TDateTimeField;
          QNoteSpeseCliDescrizione: TStringField;
          QNoteSpeseCliAutomobile: TFloatField;
          QNoteSpeseCliAutostrada: TFloatField;
          QNoteSpeseCliAereotreno: TFloatField;
          QNoteSpeseCliTaxi: TFloatField;
          QNoteSpeseCliAlbergo: TFloatField;
          QNoteSpeseCliRistoranteBar: TFloatField;
          QNoteSpeseCliParking: TFloatField;
          QNoteSpeseCliAltroImporto: TFloatField;
          QNoteSpeseCliRifRicerca: TStringField;
          QNoteSpeseCliTotSpese: TFloatField;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1ID: TdxDBGridMaskColumn;
          dxDBGrid1Data: TdxDBGridDateColumn;
          dxDBGrid1Descrizione: TdxDBGridMaskColumn;
          dxDBGrid1Automobile: TdxDBGridMaskColumn;
          dxDBGrid1Autostrada: TdxDBGridMaskColumn;
          dxDBGrid1Aereotreno: TdxDBGridMaskColumn;
          dxDBGrid1Taxi: TdxDBGridMaskColumn;
          dxDBGrid1Albergo: TdxDBGridMaskColumn;
          dxDBGrid1RistoranteBar: TdxDBGridMaskColumn;
          dxDBGrid1Parking: TdxDBGridMaskColumn;
          dxDBGrid1AltroImporto: TdxDBGridMaskColumn;
          dxDBGrid1RifRicerca: TdxDBGridMaskColumn;
          dxDBGrid1TotSpese: TdxDBGridColumn;
          QCandRicCliente: TADOQuery;
          QCandRicClienteID: TAutoIncField;
          QCandRicClienteCognome: TStringField;
          QCandRicClienteNome: TStringField;
          QCandRicClienteRifRicerca: TStringField;
          QCandRicClienteIDRicerca: TAutoIncField;
          QFindAnag: TADOQuery;
          QFindAnagID: TAutoIncField;
          QFindAnagCognome: TStringField;
          QFindAnagNome: TStringField;
          QFindAnagSesso: TStringField;
          BitBtn4: TToolbarButton97;
          Panel9: TPanel;
          BOK: TToolbarButton97;
          BNotaSpeseIns: TToolbarButton97;
          BInsAnnuncio: TToolbarButton97;
          BInsAnnuncio2: TToolbarButton97;
          DBGrid2Column9: TdxDBGridColumn;
          QCompensiID: TAutoIncField;
          QCompensiIDRicerca: TIntegerField;
          QCompensiTipo: TStringField;
          QCompensiImporto: TFloatField;
          QCompensiDataPrevFatt: TDateTimeField;
          QCompensiIDFattura: TIntegerField;
          QCompensiNote: TStringField;
          QCompensiIDOffertaDett: TIntegerField;
          QCompensiRimbSpese: TBooleanField;
          QCompensiCosto: TBooleanField;
          QCompensiDescrizione: TStringField;
          QCompensiRifRicerca: TStringField;
          TSAttCommessa: TTabSheet;
          Panel10: TPanel;
          dxDBGrid2: TdxDBGrid;
          ToolbarButton971: TToolbarButton97;
          QAttivita: TADOQuery;
          DsQAttivita: TDataSource;
          QAttivitaID: TAutoIncField;
          QAttivitaProgressivo: TStringField;
          QAttivitaData: TDateTimeField;
          QAttivitaEvento: TStringField;
          QAttivitaCandidato: TStringField;
          QAttivitaUtente: TStringField;
          QAttivitaMinuti: TSmallintField;
          QAttivitaCosto: TFloatField;
          dxDBGrid2ID: TdxDBGridMaskColumn;
          dxDBGrid2Progressivo: TdxDBGridMaskColumn;
          dxDBGrid2Data: TdxDBGridDateColumn;
          dxDBGrid2Evento: TdxDBGridMaskColumn;
          dxDBGrid2Candidato: TdxDBGridMaskColumn;
          dxDBGrid2Utente: TdxDBGridMaskColumn;
          dxDBGrid2Minuti: TdxDBGridMaskColumn;
          dxDBGrid2Costo: TdxDBGridCurrencyColumn;
          procedure RGOpzClick(Sender: TObject);
          procedure BitBtn4Click(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure BNotaSpeseInsClick(Sender: TObject);
          procedure QNoteSpeseCli_OLDCalcFields(DataSet: TDataSet);
          procedure BInsAnnuncioClick(Sender: TObject);
          procedure BInsAnnuncio2Click(Sender: TObject);
          procedure QCompensiBeforeOpen(DataSet: TDataSet);
          procedure QNoteSpeseCliBeforeOpen(DataSet: TDataSet);
          procedure QAnnunciCliBeforeOpen(DataSet: TDataSet);
          procedure QAnnunciCli2BeforeOpen(DataSet: TDataSet);
          procedure QCandRicClienteBeforeOpen(DataSet: TDataSet);
          procedure BOKClick(Sender: TObject);
          procedure QAttivitaBeforeOpen(DataSet: TDataSet);
          procedure ToolbarButton971Click(Sender: TObject);

     private
          xArrayIDAnag: array[1..3000] of integer;
          procedure RiempiASG1;
          procedure RicalcolaTotFattura;
     public
          { Public declarations }
     end;

var
     InsDettFattForm: TInsDettFattForm;

implementation

uses Fattura, ModuloDati2, ModuloDati, Main, MDRicerche;

{$R *.DFM}

procedure TInsDettFattForm.RGOpzClick(Sender: TObject);
begin
     case RGOpz.ItemIndex of
          0: PCSoggetti.visible := False;
          1: begin PCSoggetti.visible := True;
                    PCSoggetti.ActivePage := TSSoggInseriti;
                    qinsnonfatt.close;
                    qinsnonfatt.parameters.ParamByName('idric').value := QCompensi.fieldbyname('idricerca').value;
                    qinsnonfatt.Open;
               end;
          2: begin
                    PCSoggetti.visible := True;
                    PCSoggetti.ActivePage := TSSoggCandRic;
                    if ASG1.Cells[0, 1] = '' then
                         RiempiASG1;
               end;
     end;
end;

procedure TInsDettFattForm.BitBtn4Click(Sender: TObject);
var xDic, xCand, xTipo, xProgr, xDicDettFatt: string;
     i, k, xIDIns: integer;
     Vero: boolean;
     xTotImponibile: real;
begin
     if {DBGrid2.SelectedRows.Count=0} DBGrid2.SelectedCount = 0 then begin
          showMessage('nessuna riga selezionata');
          exit;
     end;
     if DBGrid2.SelectedCount > 0 {DBGrid2.SelectedRows.Count>1} then begin
          // controllo stesso tipo

          xtipo := QCompensi.FieldByName('Tipo').value;

          for k := 0 to DBGrid2.SelectedCount - 1 do begin
               QCompensi.BookMark := DBGrid2.SelectedRows[k];
               if xTipo <> QCompensi.FieldByName('Tipo').value then
                    if MessageDlg('ATTENZIONE: sono stati selezionati compensi di tipo diverso ! ' +
                         'SEI SICURO DI VOLER PROCEDERE ?', mtWarning, [mbYes, mbNO], 0) = mrNo then exit;
          end;

          // controllo stessa ricerca
          xProgr := QCompensi.FieldByName('RifRicerca').Value;

          for k := 0 to DBGrid2.SelectedCount - 1 do begin
               QCompensi.BookMark := DBGrid2.SelectedRows[k];
               //                    if xProgr<>QCompensiRifRicerca.Value then
               if xProgr <> QCompensi.FieldByName('RifRicerca').Value then
                    if MessageDlg('ATTENZIONE: sono stati selezionati compensi associati a ricerche/commesse diverse ! ' +
                         'SEI SICURO DI VOLER PROCEDERE ?', mtWarning, [mbYes, mbNO], 0) = mrNo then exit;

          end;
     end;
     if not QCompensi.IsEmpty then begin
          // dettaglio fattura
          case RGOpz.ItemIndex of
               0: begin
                         with Data do begin
                              DB.BeginTrans;
                              try
                                   xTotImponibile := 0;

                                   for k := 0 to DBGrid2.SelectedCount - 1 do begin
                                        QCompensi.BookMark := DBGrid2.SelectedRows[k];
                                        //                                             xTotImponibile:=xTotImponibile+QCompensiImporto.Value;
                                        xTotImponibile := xTotImponibile + QCompensi.FieldByName('Importo').Value;
                                        // aggiornamento Compensi
                                        Q.SQL.text := 'update EBC_CompensiRicerche set IDFattura=' + FatturaForm.TFattura.FieldbyName('ID').asString + ' where ID=' + QCompensi.FieldByName('ID').asString;
                                        Q.ExecSQL;
                                   end;
                                   QTemp.Close;
                                   QTemp.SQL.Text := 'select DicDettFatt from Global';
                                   QTemp.Open;

                                   if QTemp.FieldByName('DicDettFatt').asString <> '' then
                                        xDicDettFatt := QTemp.FieldByName('DicDettFatt').asString
                                   else xDicDettFatt := 'Per attivit� di consulenza in ambito risorse umane';

                                   Q1.SQL.text := 'insert into FattDett (IDFattura,Descrizione,Imponibile,PercentualeIVA,IDCompenso) ' +
                                        'values (:xIDFattura:,:xDescrizione:,:xImponibile:,:xPercentualeIVA:,:xIDCompenso:)';
                                   Q1.ParamByName['xIDFattura'] := FatturaForm.TFattura.FieldByName('ID').Asinteger;

                                   if QCompensi.FieldByName('Descrizione').asString = '' then begin
                                        Q1.ParamByName['xDescrizione'] := xDicDettFatt + ' - ' +
                                             QCompensi.FieldByName('Tipo').AsString + ' (rif.ric.n� ' + QCompensi.FieldByName('RifRicerca').AsString + ')';
                                   end else
                                        Q1.ParamByName['xDescrizione'] := QCompensi.FieldByName('Descrizione').asString;

                                   Q1.ParamByName['xImponibile'] := xTotImponibile;

                                   if fatturaform.DBCBFlagIVA.Checked = true then
                                        Q1.ParamByName['xPercentualeIVA'] := 0
                                   else
                                        Q1.ParamByName['xPercentualeIVA'] := data.Global.fieldbyname('Iva').value;

                                   Q1.ParamByName['xIDCompenso'] := QCompensi.FieldByName('ID').value;
                                   Q1.ExecSQL;
                                   RicalcolaTotFattura;
                                   DB.CommitTrans;

                              except
                                   DB.RollbackTrans;
                                   MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                                   raise
                              end;
                         end;
                    end;
               1: begin
                         if QInsNonFatt.RecordCount = 0 then begin
                              MessageDlg('Non ci sono soggetti inseriti cui far riferimento', mtError, [mbOK], 0);
                              exit;
                         end;

                         xIDIns := QInsNonFatt.FieldByName('ID').Value;

                         if QInsNonFatt.FieldByName('Sesso').Value = 'F' then xDic := 'Sig.ra' else xDic := 'Sig.';

                         with Data do begin
                              DB.BeginTrans;
                              try
                                   xTotImponibile := 0;

                                   for k := 0 to DBGrid2.SelectedCount - 1 do begin
                                        QCompensi.BookMark := DBGrid2.SelectedRows[k];
                                        //                                             xTotImponibile:=xTotImponibile+QCompensiImporto.Value;
                                        xTotImponibile := xTotImponibile + QCompensi.FieldByName('Importo').Value;
                                        // aggiornamento Compensi
                                        Q.SQL.text := 'update EBC_CompensiRicerche set IDFattura=' + FatturaForm.TFattura.FieldByName('ID').asString + ' where ID=' + QCompensi.FieldByName('ID').asString;
                                        Q.ExecSQL;
                                   end;
                                   Q1.Close;

                                   Q1.SQL.text := 'insert into FattDett (IDFattura,Descrizione,Imponibile,PercentualeIVA,IDCompenso) ' +
                                        'values (:xIDFattura:,:xDescrizione:,:xImponibile:,:xPercentualeIVA:,:xIDCompenso:)';
                                   Q1.ParamByName['xIDFattura'] := FatturaForm.TFattura.FieldByName('ID').asINteger;
                                   Q1.ParamByName['xDescrizione'] := 'Per attivit� di consulenza in ambito risorse umane - ' +
                                        QCompensi.FieldByName('Tipo').AsString + ' (rif.ric.n� ' + QCompensi.FieldByName('RifRicerca').AsString +
                                        ') ' + xDic + ' ' + QInsNonFatt.FieldByName('Cognome').AsString + ' ' +
                                        QInsNonFatt.FieldByName('Nome').AsString + '. Ruolo: ' + QInsNonFatt.FieldByName('Ruolo').AsString;
                                   Q1.ParamByName['xImponibile'] := xTotImponibile;
                                   Q1.ParamByName['xPercentualeIVA'] := data.Global.fieldbyname('Iva').value;
                                   Q1.ParamByName['xIDCompenso'] := QCompensi.FieldByName('ID').AsInteger;

                                   Q1.ExecSQL;
                                   // agg. inserimenti
                                   Q.SQL.text := 'update EBC_Inserimenti set ProgFattura=' + FatturaForm.TFattura.FieldByName('Progressivo').asString + ' where ID=' + IntToStr(xIDIns);
                                   Q.ExecSQL;

                                   RicalcolaTotFattura;

                                   DB.CommitTrans;
                                   FatturaForm.xUltimoInserito := xIDIns;
                              except
                                   DB.RollbackTrans;
                                   MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                              end;
                         end;
                    end;
               2: begin
                         // elenco selezionati
                         TAnagABS.Open;
                         xCand := '';
                         for i := 1 to ASG1.RowCount - 1 do begin
                              ASG1.GetCheckBoxState(0, i, Vero);
                              if Vero then begin

                                   QFindAnag.Close;
                                   QFindAnag.Parameters[0].value := xArrayIDAnag[i];
                                   QFindAnag.Open;

                                   //TAnagABS.FindKeyADO(VarArrayOf([xArrayIDAnag[i]]));

                                   if QFindAnag.FieldByName('Sesso').Value = 'F' then xDic := 'Sig.ra' else xDic := 'Sig.';

                                   xCand := xCand + xDic + ' ' + QFindAnag.FieldByName('Cognome').Value + ' ' + QFindAnag.FieldByName('Nome').Value + ', ';
                              end;
                         end;
                         QFindAnag.Close;
                         if xCand = '' then begin
                              MessageDlg('Non ci sono soggetti cui far riferimento', mtError, [mbOK], 0);
                              exit;
                         end;

                         with Data do begin
                              DB.BeginTrans;
                              try
                                   xTotImponibile := 0;

                                   for k := 0 to DBGrid2.SelectedCount - 1 do begin
                                        QCompensi.BookMark := DBGrid2.SelectedRows[k];
                                        //                                             xTotImponibile:=xTotImponibile+QCompensiImporto.Value;
                                        xTotImponibile := xTotImponibile + QCompensi.FieldByName('Importo').Value;
                                        // aggiornamento Compensi
                                        Q.SQL.text := 'update EBC_CompensiRicerche set IDFattura=' + FatturaForm.TFattura.FieldByName('ID').asString + ' where ID=' + QCompensi.FieldByName('ID').asString;
                                        Q.ExecSQL; ;
                                   end;
                                   Q1.Close;

                                   Q1.SQL.text := 'insert into FattDett (IDFattura,Descrizione,Imponibile,PercentualeIVA,IDCompenso) ' +
                                        'values (:xIDFattura:,:xDescrizione:,:xImponibile:,:xPercentualeIVA:,:xIDCompenso:)';
                                   Q1.ParamByName['xIDFattura'] := FatturaForm.TFattura.FieldByName('ID').Asinteger;
                                   Q1.ParamByName['xDescrizione'] := 'Per attivit� di consulenza in ambito risorse umane - ' +
                                        QCompensi.FieldByName('Tipo').AsString + ' (rif.ric.n� ' + QCompensi.FieldByName('RifRicerca').AsString +
                                        ') ' + xCand;
                                   Q1.ParamByName['xImponibile'] := xTotImponibile;
                                   Q1.ParamByName['xPercentualeIVA'] := data.Global.fieldbyname('Iva').value;
                                   Q1.ParamByName['xIDCompenso'] := QCompensi.FieldByname('ID').Asinteger;
                                   Q1.ExecSQL;

                                   RicalcolaTotFattura;

                                   DB.CommitTrans;
                              except
                                   DB.RollbackTrans;
                                   MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                              end;
                         end;
                    end;
          end;
     end;
     QCompensi.Close;
     QCompensi.Open;
end;

procedure TInsDettFattForm.FormShow(Sender: TObject);
var i: integer;
begin
     TSSoggInseriti.TabVisible := False;
     TSSoggCandRic.TabVisible := False;
     QCompensi.Open;
     QNoteSpeseCli.Open;
     QAnnunciCli.Open;
     QAnnunciCli2.Open;
     QAttivita.Open;
     Caption := '[M/1300] - ' + caption;

     PC.ActivePageIndex := 0;

     //Grafici
     InsDettFattForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel3.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel3.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel3.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel4.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel4.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel4.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel5.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel5.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel5.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel6.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel6.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel6.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel7.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel7.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel7.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel8.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel8.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel8.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel9.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel9.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel9.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
end;

procedure TInsDettFattForm.RiempiASG1;
var i: integer;
begin
     QCandRicCliente.Open;
     ASG1.RowCount := QCandRicCliente.Recordset.RecordCount + 1;
     //ASG1.RowCount :=10;
     i := 1;
     while not QCandRicCliente.EOF do begin
          ASG1.addcheckbox(0, i, false, false);
          ASG1.Cells[0, i] := ' ' + QCandRicCliente.FieldByName('Cognome').Value + '  ' + QCandRicCliente.FieldByName('Nome').Value + '  (Rif.ric.n� ' + QCandRicCliente.FieldByName('RifRicerca').Value + ')';
          xArrayIDAnag[i] := QCandRicCliente.FieldByName('ID').Value;
          QCandRicCliente.Next;
          inc(i);
          // if i=5 then exit;
     end;
     QCandRicCliente.Close;
end;

procedure TInsDettFattForm.BNotaSpeseInsClick(Sender: TObject);
begin
     if QNoteSpeseCli.RecordCount = 0 then exit;
     with Data do begin
          DB.BeginTrans;
          try
               Q1.Close;
               Q1.SQL.text := 'insert into FattDett (IDFattura,Descrizione,Imponibile,PercentualeIVA) ' +
                    'values (:xIDFattura:,:xDescrizione:,:xImponibile:,:xPercentualeIVA:)';
               Q1.ParamByName['xIDFattura'] := FatturaForm.TFattura.FieldByName('ID').Asinteger;
               Q1.ParamByName['xDescrizione'] := 'Per spese sostenute - (rif.ric.n� ' + QNoteSpeseCli.FieldByName('RifRicerca').AsString + ') - vedi allegato';
               Q1.ParamByName['xImponibile'] := QNoteSpeseCli.FieldByName('TotSpese').Value;

               if fatturaform.DBCBFlagIVA.Checked = true then
                    Q1.ParamByName['xPercentualeIVA'] := 0
               else
                    Q1.ParamByName['xPercentualeIVA'] := data.Global.fieldbyname('Iva').value;

               Q1.ExecSQL;
               // aggiornamento Nota spese
               Q.SQL.text := 'update RicNoteSpese set IDFattura=' + FatturaForm.TFattura.FieldbyName('ID').asString + ' where ID=' + QNoteSpeseCli.FieldByName('ID').asString;
               Q.ExecSQL;
               RicalcolaTotFattura;
               DB.CommitTrans;
               QNoteSpeseCli.Close;
               QNoteSpeseCli.Open;
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
          end;
     end;

end;

procedure TInsDettFattForm.QNoteSpeseCli_OLDCalcFields(DataSet: TDataSet);
begin
     QNoteSpeseCliTotSpese.Value := QNoteSpeseCli.FieldByName('Automobile').Value +
          QNoteSpeseCli.FieldByName('Autostrada').Value +
          QNoteSpeseCli.FieldByName('Aereotreno').Value +
          QNoteSpeseCli.FieldByName('Taxi').Value +
          QNoteSpeseCli.FieldByname('Albergo').Value +
          QNoteSpeseCli.FieldbyName('RistoranteBar').Value +
          QNoteSpeseCli.FieldByName('Parking').Value +
          QNoteSpeseCli.FieldByName('AltroImporto').Value;
end;

procedure TInsDettFattForm.BInsAnnuncioClick(Sender: TObject);
begin
     if QAnnunciCli.RecordCount = 0 then exit;
     with Data do begin
          DB.BeginTrans;
          try
               Q1.Close;
               Q1.SQL.text := 'insert into FattDett (IDFattura,Descrizione,Imponibile,PercentualeIVA,IDAnnuncio) ' +
                    'values (:xIDFattura:,:xDescrizione:,:xImponibile:,:xPercentualeIVA:,:xIDAnnuncio:)';
               Q1.ParamByName['xIDFattura'] := FatturaForm.TFattura.FieldByName('ID').Asinteger;
               Q1.ParamByName['xDescrizione'] := 'Per pubblicazione annuncio - (rif.annuncio n� ' + QAnnunciCli.FieldByName('Rif').AsString + ' - rif.ric.n� ' + QAnnunciCli.FieldByName('RifRicerca').AsString + ')';
               Q1.ParamByName['xImponibile'] := QAnnunciCli.FieldByName('TotaleEuro').Value;
               if fatturaform.DBCBFlagIVA.Checked = true then
                    Q1.ParamByName['xPercentualeIVA'] := 0
               else
                    Q1.ParamByName['xPercentualeIVA'] := data.Global.fieldbyname('Iva').value;
               Q1.ParamByName['xIDAnnuncio'] := QAnnunciCli.FieldbyName('ID').Asinteger;
               Q1.ExecSQL;
               // aggiornamento Annuncio
               Q.SQL.text := 'update Ann_annunci set IDFattura=' + FatturaForm.TFattura.FieldbyName('ID').asString + ' where ID=' + QAnnunciCli.FieldByName('ID').asString;
               Q.ExecSQL;
               RicalcolaTotFattura;
               DB.CommitTrans;
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
          end;
     end;
end;

procedure TInsDettFattForm.RicalcolaTotFattura;
var tImporto, tTotal: double;
     xIDFattura: integer;
begin
     with FatturaForm do begin
          // ricalcolo imponibile fattura
          TFattDett.DisableControls;
          TFattDett.Close;
          TFattDett.Open;
          tImporto := 0;
          tTotal := 0;
          while not TFattDett.EOF do begin
               {               tImporto:=tImporto+TFattDettImponibile.Value;
                              tTotal:=tTotal+TFattDettTotale.Value;}
               tImporto := tImporto + TFattDett.FieldByName('Imponibile').Value;
               tTotal := tTotal + TFattDett.FieldByName('Totale').Value;
               TFattDett.Next;
          end;
          //          Qtemp.SQL.text:='update Fatture set Importo=:xImporto,Totale=:xTotale '+
          //               'where ID='+TFatturaID.asString;
          Qtemp.SQL.text := 'update Fatture set Importo=:xImporto:,Totale=:xTotale: ' +
               'where ID=' + TFattura.FieldByName('ID').asString;
          Qtemp.ParamByName['xImporto'] := tImporto;
          Qtemp.ParamByName['xTotale'] := tTotal;
          Qtemp.ExecSQL;
          TFattDett.EnableControls;
          // refresh fattura e dettaglio
          TFattDett.Close;
          //          xIDfattura:=TFatturaID.Value;
          xIDfattura := TFattura.FieldByName('ID').Asinteger;
          //[/TONI20020929\]FINE
          TFattura.Close;
          TFattura.ParamByName['xID'] := xIDfattura;
          TFattura.open;
          TFattDett.Open;
     end;
end;

procedure TInsDettFattForm.BInsAnnuncio2Click(Sender: TObject);
begin
     if QAnnunciCli2.RecordCount = 0 then exit;
     with Data do begin
          DB.BeginTrans;
          try
               Q1.Close;
               Q1.SQL.text := 'insert into FattDett (IDFattura,Descrizione,Imponibile,PercentualeIVA,IDAnnuncio) ' +
                    'values (:xIDFattura:,:xDescrizione:,:xImponibile:,:xPercentualeIVA:,:xIDAnnuncio:)';
               Q1.ParamByName['xIDFattura'] := FatturaForm.TFattura.FieldByName('ID').AsInteger;
               Q1.ParamByName['xDescrizione'] := 'Per pubblicazione annuncio - (rif.annuncio n� ' + QAnnunciCli2.FieldByName('Rif').AsString + ')';
               Q1.ParamByName['xImponibile'] := QAnnunciCli2.FieldByName('TotaleEuro').value;
               if fatturaform.DBCBFlagIVA.Checked = true then
                    Q1.ParamByName['xPercentualeIVA'] := 0
               else
                    Q1.ParamByName['xPercentualeIVA'] := data.Global.fieldbyname('Iva').value;
               Q1.ParamByName['xIDAnnuncio'] := QAnnunciCli.FieldByName('ID').AsINteger;
               Q1.ExecSQL;
               // aggiornamento Annuncio
               Q.SQL.text := 'update Ann_annunci set IDFattura=' + FatturaForm.TFattura.FieldByName('ID').asString + ' where ID=' + QAnnunciCli2.FieldByName('ID').asString;
               Q.ExecSQL;
               RicalcolaTotFattura;
               DB.CommitTrans;
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
          end;
     end;
end;

procedure TInsDettFattForm.QCompensiBeforeOpen(DataSet: TDataSet);
begin
     QCompensi.SQL.text := 'select EBC_CompensiRicerche.*, EBC_Ricerche.Progressivo RifRicerca ' +
          'from EBC_CompensiRicerche,EBC_Ricerche ' +
          'where EBC_CompensiRicerche.IDRicerca=EBC_Ricerche.ID ' +
          'and EBC_CompensiRicerche.IDFattura is null ' +
          'and EBC_Ricerche.IDCliente=' + Data2.TEBCClienti.FieldByName('ID').asString +
          ' order by EBC_Ricerche.Progressivo';
end;

procedure TInsDettFattForm.QNoteSpeseCliBeforeOpen(DataSet: TDataSet);
begin
     QNoteSpeseCli.SQL.text := 'select RicNoteSpese.ID,RicNoteSpese.Data,RicNoteSpese.Descrizione, ' +
          'RicNoteSpese.Automobile, ' +
          'RicNoteSpese.Autostrada, ' +
          'RicNoteSpese.Aereotreno, ' +
          'RicNoteSpese.Taxi, ' +
          'RicNoteSpese.Albergo, ' +
          'RicNoteSpese.RistoranteBar, ' +
          'RicNoteSpese.Parking, ' +
          'RicNoteSpese.AltroImporto, ' +
          'EBC_Ricerche.Progressivo RifRicerca ' +
          'from RicNoteSpese,EBC_Ricerche  ' +
          'where RicNoteSpese.IDRicerca=EBC_Ricerche.ID ' +
          'and (RicNoteSpese.IDFattura is null or RicNoteSpese.IDFattura=0) ' +
          'and RicNoteSpese.IDCliente=' + Data2.TEBCClienti.FieldByName('ID').asString +
          'order by Data';
end;

procedure TInsDettFattForm.QAnnunciCliBeforeOpen(DataSet: TDataSet);
begin
     QAnnunciCli.SQL.Text := 'select Ann_annunci.Data, ' +
          'Ann_annunci.Rif, ' +
          'Ann_annunci.ID, ' +
          'Ann_annunci.NumModuli, ' +
          'Ann_annunci.NumParole, ' +
          'Ann_annunci.TotaleLire, ' +
          'Ann_annunci.TotaleEuro, ' +
          'EBC_Ricerche.Progressivo RifRicerca  ' +
          'from Ann_annunci,Ann_annunciRicerche,EBC_Ricerche ' +
          'where Ann_annunci.ID=Ann_annunciRicerche.IDAnnuncio  ' +
          'and Ann_annunciRicerche.IDRicerca=EBC_Ricerche.ID ' +
          'and (Ann_annunci.IDFattura is null or Ann_annunci.IDFattura=0) ' +
          'and EBC_Ricerche.IDcliente=' + Data2.TEBCClienti.FieldByName('ID').asString +
          'order by Data';
end;

procedure TInsDettFattForm.QAnnunciCli2BeforeOpen(DataSet: TDataSet);
begin
     QAnnunciCli2.SQL.Text := 'select Ann_annunci.Data, ' +
          'Ann_annunci.Rif, ' +
          'Ann_annunci.ID, ' +
          'Ann_annunci.NumModuli, ' +
          'Ann_annunci.NumParole, ' +
          'Ann_annunci.TotaleLire, ' +
          'Ann_annunci.TotaleEuro ' +
          'from Ann_annunci ' +
          'where Ann_annunci.IDcliente=' + Data2.TEBCClienti.FieldByName('ID').asString +
          '  and (Ann_annunci.IDFattura is null or Ann_annunci.IDFattura=0) ' +
          'order by Data ';
end;

procedure TInsDettFattForm.QCandRicClienteBeforeOpen(DataSet: TDataSet);
begin
     QCandRicCliente.Parameters[0].value := Data2.TEBCClienti.FieldByName('ID').value;
end;

procedure TInsDettFattForm.BOKClick(Sender: TObject);
begin
     ModalResult := mrOk;
end;

procedure TInsDettFattForm.QAttivitaBeforeOpen(DataSet: TDataSet);
begin
     QAttivita.SQL.text := 'select RA.ID, R.Progressivo, RA.Data, ' +
          '	TE.Evento, A.Cognome+'' ''+A.Nome Candidato, ' +
          '	U.Nominativo Utente, ' +
          '	RA.Minuti, RA.Costo ' +
          'from EBC_Ricerche_attivita RA ' +
          'join EBC_Ricerche R on RA.IDRicerca = R.ID ' +
          'join TabEventiAttCommessa TE on RA.IDEvento = TE.ID ' +
          'left join Anagrafica A on RA.IDAnagrafica = A.ID ' +
          'left join Users U on RA.IDUtente = U.ID ' +
          'where RA.IDFattura is null ' +
          'and IDCliente = ' + Data2.TEBCClienti.FieldByName('ID').asString;
end;

procedure TInsDettFattForm.ToolbarButton971Click(Sender: TObject);
var xTotImponibile: real;
     k: integer;
     xDicDettFatt: string;
begin

     Q.Close;
     xTotImponibile := 0;

     for k := 0 to dxDBGrid2.SelectedCount - 1 do begin
          QAttivita.BookMark := dxDBGrid2.SelectedRows[k];
          xTotImponibile := xTotImponibile + QAttivitaCosto.Value;

          Q.SQL.text := 'update EBC_Ricerche_attivita set IDFattura=' + FatturaForm.TFattura.FieldbyName('ID').AsString + ' where ID=' + QAttivitaID.asString;
          Q.ExecSQL;

          xDicDettFatt := 'Per attivit�: ' + QAttivitaEvento.value;
          if QAttivitaCandidato.value <> '' then
               xDicDettFatt := xDicDettFatt + ' relativa al candidato ' + QAttivitaCandidato.value;
          if QAttivitaData.AsString <> '' then
               xDicDettFatt := xDicDettFatt + ' in data ' + QAttivitaData.AsString;
          if QAttivitaMinuti.asString <> '' then
               xDicDettFatt := xDicDettFatt + ' per minuti ' + QAttivitaMinuti.asString;

          // inserimento dettaglio fattura
          with Data do begin
               Q1.SQL.text := 'insert into FattDett (IDFattura,Descrizione,Imponibile,PercentualeIVA,IDAttivitaCommessa) ' +
                    'values (:xIDFattura:,:xDescrizione:,:xImponibile:,:xPercentualeIVA:,:xIDAttivitaCommessa:)';
               Q1.ParamByName['xIDFattura'] := FatturaForm.TFattura.FieldByName('ID').Asinteger;
               Q1.ParamByName['xDescrizione'] := xDicDettFatt;
               Q1.ParamByName['xImponibile'] := xTotImponibile;
               if fatturaform.DBCBFlagIVA.Checked = true then
                    Q1.ParamByName['xPercentualeIVA'] := 0
               else
                    Q1.ParamByName['xPercentualeIVA'] := data.Global.fieldbyname('Iva').value;
               Q1.ParamByName['xIDAttivitaCommessa'] := QAttivitaID.asInteger;
               Q1.ExecSQL;

               if DataRicerche.QFattLK.active then begin
                    DataRicerche.QFattLK.Close;
                    DataRicerche.QFattLK.Open;
               end;
          end;
          RicalcolaTotFattura;
     end;

     QAttivita.Close;
     QAttivita.Open;

end;

end.

