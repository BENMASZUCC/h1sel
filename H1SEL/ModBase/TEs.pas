unit TEs;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, ShellAPI,
     StdCtrls, Buttons, ExtCtrls;

type
     TForm1 = class(TForm)
          Label1: TLabel;
          Label2: TLabel;
          Label3: TLabel;
          Label4: TLabel;
          Label5: TLabel;
          Edit1: TEdit;
          Memo1: TMemo;
          Memo2: TMemo;
          Memo3: TMemo;
          Memo4: TMemo;
          BitBtn1: TBitBtn;
          Label6: TLabel;
          Edit2: TEdit;
          Bevel1: TBevel;
          RadioGroup1: TRadioGroup;
          CheckBox1: TCheckBox;
          Button1: TButton;
          Bevel2: TBevel;
          Label7: TLabel;
          Edit3: TEdit;
          Label8: TLabel;
          Edit4: TEdit;
          BitBtn2: TBitBtn;
          CB_Debug: TCheckBox;
          procedure BitBtn1Click(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure Button1Click(Sender: TObject);
          procedure BitBtn2Click(Sender: TObject);
     private
    { Private declarations }
     public
    { Public declarations }
     end;

var
     Form1: TForm1;

implementation

{$R *.DFM}
                                                                                                                            //stdcall
//function TS_ShellExecute(hwnd: integer; lpOperation, lpFile, lpParameters, lpDirectory: widestring; nShowCmd: integer): integer; cdecl; external 'H1TS_Server.dll';

procedure TForm1.BitBtn1Click(Sender: TObject);
var xComando, xParameters, xErrorMsg: string;
     //xComando_ws, xParameters_ws: widestring;
     xComando_ws, xParameters_ws: string;
     res: integer;
begin

     if RadioGroup1.ItemIndex = 0 then begin
          xComando := Edit2.text;
          xParameters := '"' + Edit1.text + '" "' + Memo1.text + '" "' + Memo2.text + '" "' + Memo3.text + '" "' + Memo4.text + '"';
          res := ShellExecute(0, 'Open', pchar(xComando), pchar(xParameters), '', SW_SHOW);
     end else begin
          xComando_ws := Edit2.text;
          inputquery('TEST0','Comando',xComando_ws);

          if CB_Debug.Checked then
               xParameters_ws := '"' + Edit1.text + '" "' + Memo1.text + '" "' + Memo2.text + '" "' + Memo3.text + '" "' + Memo4.text + '" "1"'
          else
               xParameters_ws := '"' + Edit1.text + '" "' + Memo1.text + '" "' + Memo2.text + '" "' + Memo3.text + '" "' + Memo4.text + '" "0"';

          if CheckBox1.checked then
               res := TS_ShellExecute(0, 'Open', xComando_ws, '', '', SW_SHOW)
          else
               res := TS_ShellExecute(0, 'Open', xComando_ws, xParameters_ws, '', SW_SHOW);
     end;

     case res of
          0: xErrorMsg := '0 = The operating system is out of memory or resources.';
          1: xErrorMsg := '1 = Non riesco ad aprire il virtual channel (TS_ShellExecute)';
          //2: xErrorMsg := '2 = The specified file was not found';
          2: xErrorMsg := '2 = il virtual channel si chiude prima di inviare i dati (TS_ShellExecute)';
          //3: xErrorMsg := '3 = The specified path was not found.';
          3: xErrorMsg := '3 = fallito l''invio dei dati al client (TS_ShellExecute)';

          5: xErrorMsg := '5 = Windows 95 only: The operating system denied access to the specified file';
          8: xErrorMsg := '8 = Windows 95 only: There was not enough memory to complete the operation.';
          10: xErrorMsg := '10 = Wrong Windows version';
          11: xErrorMsg := '11 = The.EXE file is invalid(non - Win32.EXE or error in .EXE image).';
          12: xErrorMsg := '12 = Application was designed for a different operating system.';
          13: xErrorMsg := '13 = Application was designed for MS - DOS 4.0';
          15: xErrorMsg := '15 = Attempt to load a real - mode program';
          16: xErrorMsg := '16 = Attempt to load a second instance of an application with non - readonly data segments';
          19: xErrorMsg := '19 = Attempt to load a compressed application file';
          20: xErrorMsg := '20 = Dynamic - link library(DLL)file failure".';
          26: xErrorMsg := '26 = A sharing violation occurred.';
          27: xErrorMsg := '27 = The filename association is incomplete or invalid.';
          28: xErrorMsg := '28 = The DDE transaction could not be completed because the request timed out.';
          29: xErrorMsg := '29 = The DDE transaction failed.';
          30: xErrorMsg := '30 = The DDE transaction could not be completed because other DDE transactions were being processed.';
          31: xErrorMsg := '31 = There is no application associated with the given filename extension.';
          32: xErrorMsg := '32 = Windows 95 only: The specified dynamic - link library was not found.';
     else xErrorMsg := IntToStr(res) + ' = (errore non documentato)';
     end;
     if (res < 0) or (res > 32) then
          xErrorMsg := 'OK (' + inttostr(res) + ')';

        // CODICI ERRORE COMUNICATI DA STEFANO ma NON gestiti:
        // 1: non riesce ad aprire il virtual channel
        // 2: il virtual channel si chiude prima di inviare i dati
        // 3: fallisce l'invio dei dai al client
        // 0: la ShellExecute fallisce (quindi la comunicazione � corretta ma c'� qualcosa che non va nel passaggio dei parametri)

     ShowMessage(xErrorMsg);

end;

procedure TForm1.FormShow(Sender: TObject);
begin
     Edit2.text := ExtractFileDir(Application.ExeName) + '\InvioMailMAPI.exe';
end;

procedure TForm1.Button1Click(Sender: TObject);
var res: integer;
     s: widestring;
begin
     s := 'mailto:fpasserini@ebcconsulting.com';
     res := TS_ShellExecute(0, 'Open', s, '', '', SW_SHOW)
end;

procedure TForm1.BitBtn2Click(Sender: TObject);
begin
     if FileExists(Edit4.text) then
          showmessage('Il file � gi� stato copiato sul client');

     if CopyFile(pchar(Edit3.text), pchar(Edit4.text), True) then
          ShowMEssage('Copia file completata');
end;

end.

