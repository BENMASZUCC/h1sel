unit Curriculum;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, DBCtrls, Buttons, Mask, ComCtrls, ExtCtrls, TB97, DBCGrids, DB,
     Wall, checklst, DtEdit97, DtEdDB97, DBTables, Grids, DBGrids, ComObj, RXCtrls,
     AnagConoscInfo, FrmMultiDBCheckBox, ADODB, U_ADOLinkCl, dxCntner,
     dxEditor, dxExEdtr, dxEdLib, dxDBELib, FrmCampiPers, dxDBTLCl,
     dxGrClms, AdvSplitter, dxTL, dxDBCtrl, dxDBGrid, dxDBEdtr;

type
     TCurriculumForm = class(TForm)
          PageControl1: TPageControl;
          TSCVAltriDati: TTabSheet;
          TSCVTitoliStudio: TTabSheet;
          TSCVCorsiStage: TTabSheet;
          TSCVLingue: TTabSheet;
          TSCVespLav: TTabSheet;
          GroupBox1: TGroupBox;
          DBCheckBox1: TDBCheckBox;
          Label1: TLabel;
          DBEdit4: TDBEdit;
          DBCheckBox2: TDBCheckBox;
          GroupBox2: TGroupBox;
          Label2: TLabel;
          Label3: TLabel;
          DBEdit7: TDBEdit;
          Label4: TLabel;
          DBEdit8: TDBEdit;
          Label5: TLabel;
          DBEdit9: TDBEdit;
          DBComboBox1: TDBComboBox;
          DBCtrlGrid1: TDBCtrlGrid;
          Panel9: TPanel;
          TbBTitoliNew: TToolbarButton97;
          TbBTitoliDel: TToolbarButton97;
          TbBTitoliOK: TToolbarButton97;
          TbBTitoliCan: TToolbarButton97;
          Label9: TLabel;
          DBEdit6: TDBEdit;
          Label10: TLabel;
          DBCheckBox3: TDBCheckBox;
          Label12: TLabel;
          DBEdit13: TDBEdit;
          Label16: TLabel;
          DBEdit15: TDBEdit;
          DBCtrlGrid2: TDBCtrlGrid;
          Label13: TLabel;
          DBComboBox4: TDBComboBox;
          Label17: TLabel;
          DBEdit17: TDBEdit;
          Label18: TLabel;
          DBMemo1: TDBMemo;
          DBEdit18: TDBEdit;
          Label20: TLabel;
          DBEdit19: TDBEdit;
          Label22: TLabel;
          Label21: TLabel;
          DBEdit20: TDBEdit;
          Panel2: TPanel;
          TbBCorsiExtraNew: TToolbarButton97;
          TbBCorsiExtraDel: TToolbarButton97;
          TbBCorsiExtraOK: TToolbarButton97;
          TbBCorsiExtraCan: TToolbarButton97;
          ToolbarButton972: TToolbarButton97;
          Wallpaper1: TPanel;
          ToolbarButton973: TToolbarButton97;
          Label15: TLabel;
          Label23: TLabel;
          DBCtrlGrid3: TDBCtrlGrid;
          Panel1: TPanel;
          TbBLinConNew: TToolbarButton97;
          TbBLinConDel: TToolbarButton97;
          TbBLinConMod: TToolbarButton97;
          Label30: TLabel;
          Label24: TLabel;
          DBEdit16: TDBEdit;
          Label25: TLabel;
          Label26: TLabel;
          DBEdit22: TDBEdit;
          Panel3: TPanel;
          TbBEspLavDel: TToolbarButton97;
          TbBEspLavMod: TToolbarButton97;
          ToolbarButton9710: TToolbarButton97;
          Label34: TLabel;
          PanCatProtette: TPanel;
          Label6: TLabel;
          Label7: TLabel;
          Label8: TLabel;
          DBEdit11: TDBEdit;
          DBComboBox2: TDBComboBox;
          DBComboBox3: TDBComboBox;
          Label47: TLabel;
          DBLookupComboBox2: TDBLookupComboBox;
          Label11: TLabel;
          Label45: TLabel;
          DBEdit38: TDBEdit;
          GroupBox5: TGroupBox;
          Label51: TLabel;
          Label53: TLabel;
          ToolbarButton971: TToolbarButton97;
          Label14: TLabel;
          DBEdit14: TDBEdit;
          DbDateEdit971: TDbDateEdit97;
          Panel7: TPanel;
          DBEdit1: TDBEdit;
          DBEdit2: TDBEdit;
          TbBEspLavNew: TToolbarButton97;
          GroupBox6: TGroupBox;
          DBCheckBox4: TDBCheckBox;
          DBCheckBox5: TDBCheckBox;
          DBCheckBox6: TDBCheckBox;
          DBCheckBox7: TDBCheckBox;
          ToolbarButton974: TToolbarButton97;
          Label43: TLabel;
          ToolbarButton975: TToolbarButton97;
          dsAnagAltriDati: TDataSource;
          Label54: TLabel;
          DBEdit40: TDBEdit;
          Label58: TLabel;
          DBEdit42: TDBEdit;
          GroupBox7: TGroupBox;
          RGProprieta: TRadioGroup;
          DBEdit21: TDBEdit;
          DBEdit44: TDBEdit;
          DBCBProvCV: TDBComboBox;
          Label52: TLabel;
          DBEdit31: TDBEdit;
          Label60: TLabel;
          DBEdit45: TDBEdit;
          Label62: TLabel;
          DBEdit46: TDBEdit;
          LDataRealeCV: TLabel;
          DbDEDataRealeCV: TDbDateEdit97;
          DbCbNoCV: TDBCheckBox;
          Label37: TLabel;
          DbDateEdit973: TDbDateEdit97;
          DBCheckBox11: TDBCheckBox;
          DBCheckBox12: TDBCheckBox;
          DBCheckBox13: TDBCheckBox;
          DBCheckBox14: TDBCheckBox;
          Label48: TLabel;
          Label49: TLabel;
          DBEdit10: TDBEdit;
          TSCVConoscInfo: TTabSheet;
          BEspLavRetrib: TToolbarButton97;
          MultiDBCheckBoxFrame1: TMultiDBCheckBoxFrame;
          Label32: TLabel;
          DBEdit34: TDBEdit;
          PanEspProfMaturata: TPanel;
          Label41: TLabel;
          DBLookupComboBox3: TDBLookupComboBox;
          TSCVCampiPers: TTabSheet;
          PanFerrari: TPanel;
          DBCheckBox18: TDBCheckBox;
          DBCheckBox19: TDBCheckBox;
          Label46: TLabel;
          DBEdit39: TDBEdit;
          Label50: TLabel;
          DBEdit47: TDBEdit;
          Label55: TLabel;
          DBEdit48: TDBEdit;
          QTipiRetribLK: TADOLinkedQuery;
          TAnagAltriDati: TADOLinkedQuery;
          QEspProfMatLK: TADOLinkedQuery;
          TDispMovLK: TADOLinkedTable;
          TAnagMansioni: TADOLinkedTable;
          TMansioniLK: TADOLinkedTable;
          TAnagAltriDatiTipoRetib: TStringField;
          TAnagAltriDatiDispMovimento: TStringField;
          TAnagAltriDatiIDAnagrafica: TIntegerField;
          TAnagAltriDatiFiguraChiave: TBooleanField;
          TAnagAltriDatiAssunzioneObbligatoria: TBooleanField;
          TAnagAltriDatiIdoneoAffiancamento: TBooleanField;
          TAnagAltriDatiCateogorieProtette: TStringField;
          TAnagAltriDatiInvalidita: TStringField;
          TAnagAltriDatiPercentualeInvalidita: TIntegerField;
          TAnagAltriDatiPatente: TBooleanField;
          TAnagAltriDatiTipoPatente: TStringField;
          TAnagAltriDatiDispAuto: TBooleanField;
          TAnagAltriDatiServizioMilitareStato: TStringField;
          TAnagAltriDatiServizioMilitareDove: TStringField;
          TAnagAltriDatiServizioMilitareQuando: TStringField;
          TAnagAltriDatiServizioMilitareGrado: TStringField;
          TAnagAltriDatiTipoContratto: TStringField;
          TAnagAltriDatiQualificaContrattuale: TStringField;
          TAnagAltriDatiDataAssunzione: TDateTimeField;
          TAnagAltriDatiDataScadContratto: TDateTimeField;
          TAnagAltriDatiDataUscita: TDateTimeField;
          TAnagAltriDatiDataFineProva: TDateTimeField;
          TAnagAltriDatiDataProssScatto: TDateTimeField;
          TAnagAltriDatiDataPensione: TDateTimeField;
          TAnagAltriDatiIDStabilimento: TIntegerField;
          TAnagAltriDatiPosizioneIdeale: TStringField;
          TAnagAltriDatiOrarioIdeale: TStringField;
          TAnagAltriDatiIDDispMov: TIntegerField;
          TAnagAltriDatiDisponibContratti: TStringField;
          TAnagAltriDatiCVProvenienza: TStringField;
          TAnagAltriDatiCVFile: TStringField;
          TAnagAltriDatiAbilitazProfessioni: TStringField;
          TAnagAltriDatiNote: TMemoField;
          TAnagAltriDatiMobilita: TBooleanField;
          TAnagAltriDatiCassaIntegrazione: TBooleanField;
          TAnagAltriDatiDispPartTime: TBooleanField;
          TAnagAltriDatiDispInterinale: TBooleanField;
          TAnagAltriDatiProvInteresse: TStringField;
          TAnagAltriDatiInquadramento: TStringField;
          TAnagAltriDatiRetribuzione: TStringField;
          TAnagAltriDatiBenefits: TStringField;
          TAnagAltriDatiPercPartTime: TFloatField;
          TAnagAltriDatiDataAssunzRicon: TDateTimeField;
          TAnagAltriDatiDataIngressoAzienda: TDateTimeField;
          TAnagAltriDatiCodSindacato: TStringField;
          TAnagAltriDatiCodTipoAssunzione: TStringField;
          TAnagAltriDatiPeriodoProvaGG: TSmallintField;
          TAnagAltriDatiIDMotAssunz: TIntegerField;
          TAnagAltriDatiNazionalita: TStringField;
          TAnagAltriDatiIDTipoRetrib: TIntegerField;
          TAnagAltriDatiIDSindacato: TIntegerField;
          TAnagAltriDatiNoteInfoPos: TStringField;
          TAnagAltriDatiTemporaryMG: TBooleanField;
          TAnagAltriDatiNoCV: TBooleanField;
          TAnagAltriDatiCassaIntegrStraord3: TBooleanField;
          TAnagAltriDatiCassaIntegrStraord24: TBooleanField;
          TAnagAltriDatiDisoccupato24: TBooleanField;
          TAnagAltriDatiDisoccupato12: TBooleanField;
          TAnagAltriDatiDispTraferteGiorn: TBooleanField;
          TAnagAltriDatiDispPernottamento: TBooleanField;
          TAnagAltriDatiDisptrasferimento: TBooleanField;
          TAnagAltriDatiIDContrattoNaz: TIntegerField;
          TAnagAltriDatiIDCatProfess: TIntegerField;
          TAnagAltriDatiDataLicenziamento: TDateTimeField;
          TAnagAltriDatiDataProroga: TDateTimeField;
          TAnagAltriDatiDataUltimaProm: TDateTimeField;
          TAnagAltriDatiIDEspProfMaturata: TIntegerField;
          TAnagAltriDatiDomandePrec: TBooleanField;
          TAnagAltriDatiPartecSelezPrec: TBooleanField;
          TAnagAltriDatiPartecSelezQuando: TStringField;
          TAnagAltriDatiTipoSelezione: TStringField;
          TAnagAltriDatiParentiInAzienda: TStringField;
          TAnagAltriDatiTipoPersona: TStringField;
          TAnagAltriDatiDirigenteTipo: TStringField;
          TAnagAltriDatiDirigenteStato: TStringField;
          TAnagAltriDatiDirigenteIDCliente: TIntegerField;
          TAnagAltriDatiIDTDgTipologia: TIntegerField;
          TAnagAltriDatiEspProfMaturata: TStringField;
          FrmAnagConoscInfo1: TFrmAnagConoscInfo;
          DBEdit49: TDBEdit;
          Button1: TButton;
          Label57: TLabel;
          DBEdit50: TDBEdit;
          TAnagAltriDatiNumPassaporto: TStringField;
          GroupBox8: TGroupBox;
          Label59: TLabel;
          DBEdit43: TDBEdit;
          Label61: TLabel;
          DBLookupComboBox1: TDBLookupComboBox;
          Label63: TLabel;
          Label64: TLabel;
          TAnagAltriDatiRetribVariabile: TStringField;
          TAnagAltriDatiRetribTotale: TStringField;
          DBEdit51: TDBEdit;
          DBEdit52: TDBEdit;
          TAnagAltriDatiIDContrattoSettore: TIntegerField;
          QContrattiSettoriLK: TADOQuery;
          QContrattiSettoriLKID: TAutoIncField;
          QContrattiSettoriLKSettore: TStringField;
          TAnagAltriDatiContrattoSettore: TStringField;
          Label65: TLabel;
          DBLookupComboBox4: TDBLookupComboBox;
          DBCtrlGrid4: TDBCtrlGrid;
          Label38: TLabel;
          LTipoContratto: TLabel;
          LLivelloContr: TLabel;
          Label42: TLabel;
          Label44: TLabel;
          LMensilita: TLabel;
          Label56: TLabel;
          Label19: TLabel;
          Label40: TLabel;
          Label39: TLabel;
          GroupBox3: TGroupBox;
          Label27: TLabel;
          Label28: TLabel;
          Label29: TLabel;
          Label31: TLabel;
          Label33: TLabel;
          Label35: TLabel;
          DBEdit23: TDBEdit;
          DBEdit24: TDBEdit;
          DBEdit25: TDBEdit;
          DBEdit26: TDBEdit;
          DBEdit28: TDBEdit;
          DBEdit29: TDBEdit;
          DBEdit32: TDBEdit;
          DBEdit33: TDBEdit;
          DBCTipoContratto_old: TDBComboBox;
          DBELivelloContr: TDBEdit;
          DBEdit35: TDBEdit;
          GroupBox4: TGroupBox;
          DBEdit3: TDBEdit;
          DBEdit5: TDBEdit;
          DBEMensilita: TDBEdit;
          DBEdit41: TDBEdit;
          DBCheckBox8: TDBCheckBox;
          DBEdit30: TDBEdit;
          DBEdit12: TDBEdit;
          DBEdit36: TDBEdit;
          DBEdit37: TDBEdit;
          Label66: TLabel;
          DBEdit53: TDBEdit;
          DBCheckBox20: TDBCheckBox;
          DBCheckBox21: TDBCheckBox;
          DBCheckBox22: TDBCheckBox;
          DBCheckBox23: TDBCheckBox;
          Label67: TLabel;
          DBComboBox5: TDBComboBox;
          TSNote: TTabSheet;
          Panel4: TPanel;
          RetribIntermedia: TdxDBCurrencyEdit;
          RetribVarIntermedia: TdxDBCurrencyEdit;
          RetribTotIntermedia: TdxDBCurrencyEdit;
          Label68: TLabel;
          TAnagAltriDatiNoteCurriculum: TMemoField;
          lNote: TLabel;
          TAnagAltriDatiNoteAltriDati: TMemoField;
          eNotecv: TDBMemo;
          TAnagAltriDatiDispAvvicinSede: TBooleanField;
          Label69: TLabel;
          DBEdit54: TDBEdit;
          DBLookupComboBox5: TDBLookupComboBox;
          Label70: TLabel;
          DBCTipoContratto: TDBEdit;
          ToolbarButton976: TToolbarButton97;
          DBEdit55: TDBEdit;
          Label71: TLabel;
          Label72: TLabel;
          DBEdit56: TDBEdit;
          Label73: TLabel;
          DBMemo2: TDBMemo;
          Label74: TLabel;
          DBEdit57: TDBEdit;
          TAnagAltriDatiSpecCVProvenienza: TStringField;
          TSCampiPers: TTabSheet;
          CampiPersFrame1: TCampiPersFrame;
          TSAreeInteresse: TTabSheet;
          Panel10: TPanel;
          Label75: TLabel;
          NuovaAreaInteresse: TToolbarButton97;
          BSalvaAreeInteresse: TToolbarButton97;
          BeliminaArea: TToolbarButton97;
          dxDBGrid2: TdxDBGrid;
          AdvSplitter1: TAdvSplitter;
          Panel11: TPanel;
          Label76: TLabel;
          BNuovoInteressiSviluppo: TToolbarButton97;
          BSalvaInteressiSviluppo: TToolbarButton97;
          BEliminaInteressiSviluppo: TToolbarButton97;
          BTabAmbitiSviluppo: TToolbarButton97;
          dxDBGrid3: TdxDBGrid;
          dxDBGrid3Descrizione: TdxDBGridColumn;
          dxDBGrid3Note: TdxDBGridBlobColumn;
          QAnagAreeInteresse: TADOQuery;
          QAnagAreeInteresseID: TAutoIncField;
          QAnagAreeInteresseArea: TStringField;
          QAnagAreeInteresseNote: TMemoField;
          DSAnagAreeInteresse: TDataSource;
          DSAnagInteressiSviluppo: TDataSource;
          QAnagInteressiSviluppo: TADOQuery;
          QAnagInteressiSviluppoID: TAutoIncField;
          QAnagInteressiSviluppoIDanagrafica: TIntegerField;
          QAnagInteressiSviluppoOrdine: TIntegerField;
          QAnagInteressiSviluppoNote: TMemoField;
          QAnagInteressiSviluppoDescrizione: TStringField;
          dxDBGrid2Column1: TdxDBGridColumn;
          dxDBGrid2Column2: TdxDBGridBlobColumn;
          TSAlbiOrdini: TTabSheet;
          Panel16: TPanel;
          DBCtrlGrid5: TDBCtrlGrid;
          Label77: TLabel;
          Label78: TLabel;
          Label79: TLabel;
          Label80: TLabel;
          Label81: TLabel;
          Label82: TLabel;
          Label83: TLabel;
          Label84: TLabel;
          Label85: TLabel;
          Label86: TLabel;
          dxDBLookupEdit1: TdxDBLookupEdit;
          dxDBDateEdit2: TdxDBDateEdit;
          DBEdit58: TDBEdit;
          DBEdit59: TDBEdit;
          DBCheckBox25: TDBCheckBox;
          dxDBDateEdit3: TdxDBDateEdit;
          DBEdit60: TDBEdit;
          DBEdit61: TDBEdit;
          DBEdit62: TDBEdit;
          DBEdit63: TDBEdit;
          DBEdit64: TDBEdit;
          QAnagCatProfess: TADOQuery;
          QAnagCatProfessID: TAutoIncField;
          QAnagCatProfessIDAnagrafica: TIntegerField;
          QAnagCatProfessIDCatProfess: TIntegerField;
          QAnagCatProfessIstitutoConseg: TStringField;
          QAnagCatProfessLuogoConseg: TStringField;
          QAnagCatProfessProvConseg: TStringField;
          QAnagCatProfessCittaEsteraConseg: TStringField;
          QAnagCatProfessDataConseg: TDateTimeField;
          QAnagCatProfessNumAbil: TStringField;
          QAnagCatProfessEquipollente: TBooleanField;
          QAnagCatProfessDataEquip: TDateTimeField;
          QAnagCatProfessNumEquip: TStringField;
          QAnagCatProfessCatProfessLK: TStringField;
          QAnagCatProfessQTipoLK: TStringField;
          QCatProfessLK: TADOQuery;
          QCatProfessLKID: TAutoIncField;
          QCatProfessLKDescrizione: TStringField;
          QCatProfessLKTipo: TStringField;
          DSanagcatprofess: TDataSource;
          Panel12: TPanel;
          BAggiungiAlbi: TToolbarButton97;
          bEliminaAlbi: TToolbarButton97;
          bAnnullaAlbi: TToolbarButton97;
          BConfermaAlbi: TToolbarButton97;
          btabTipoCatprofess: TToolbarButton97;
          ToolbarButton977: TToolbarButton97;
          SpeedButton1: TToolbarButton97;
          SpeedButton2: TToolbarButton97;
          BAzienda: TToolbarButton97;
          BTabProvCV: TToolbarButton97;
          SpeedButton3: TToolbarButton97;
          Panel5: TPanel;
          BTitoloITA: TToolbarButton97;
          BTitoloENG: TToolbarButton97;
          BLinguaENG: TToolbarButton97;
          BLinguaITA: TToolbarButton97;
          BEspLavITA: TToolbarButton97;
          BEspLavENG: TToolbarButton97;
          BEspProfmatITA: TToolbarButton97;
          BEspProfmatENG: TToolbarButton97;
          Label87: TLabel;
          MultiDBCheckBoxFrame2: TMultiDBCheckBoxFrame;
          GroupBox9: TGroupBox;
          DBCheckBox24: TDBCheckBox;
          DBCheckBox15: TDBCheckBox;
          DBCheckBox16: TDBCheckBox;
          DBCheckBox17: TDBCheckBox;
          DBCheckBox9: TDBCheckBox;
          DBCheckBox10: TDBCheckBox;
          MultiDBCheckBoxFrame3: TMultiDBCheckBoxFrame;
          DBCheckBox26: TDBCheckBox;
          cbVisibileWeb: TDBCheckBox;
          GroupBox10: TGroupBox;
          DBEdit27: TDBEdit;
          DBEdit65: TDBEdit;
          TSAltriInteressi: TTabSheet;
          Panel6: TPanel;
          BSalvaInteressiPers: TToolbarButton97;
          BAnnullaAltriInteressi: TToolbarButton97;
          Panel8: TPanel;
          Label97: TLabel;
          DBMemo5: TDBMemo;
          Splitter1: TSplitter;
          Panel13: TPanel;
          Label36: TLabel;
          DBMemo3: TDBMemo;
          Panel14: TPanel;
          Label88: TLabel;
          DBMemo4: TDBMemo;
          Panel15: TPanel;
          Label89: TLabel;
          DBMemo6: TDBMemo;
          QAnagOnteressiPers: TADOQuery;
          DSAnagInteressiPers: TDataSource;
          QAnagOnteressiPersID: TAutoIncField;
          QAnagOnteressiPersIDAnagrafica: TIntegerField;
          QAnagOnteressiPersHobbySport: TMemoField;
          QAnagOnteressiPersAbilita: TMemoField;
          QAnagOnteressiPersAspirazioni: TMemoField;
          QAnagOnteressiPersNotePersonali: TMemoField;
          QAnagOnteressiPersParolaChiave1: TStringField;
          QAnagOnteressiPersParolaChiave2: TStringField;
          QAnagOnteressiPersParolaChiave3: TStringField;
          QAnagOnteressiPersParolaChiave4: TStringField;
          QAnagOnteressiPersParolaChiave5: TStringField;
          Splitter2: TSplitter;
          Splitter3: TSplitter;
          Qtemp: TADOQuery;
    Label90: TLabel;
    DBEdit66: TDBEdit;
    Panel17: TPanel;
    DBNote: TDBMemo;
    Panel18: TPanel;
    Panel19: TPanel;
    DBMemo7: TDBMemo;
    Panel20: TPanel;
    Splitter4: TSplitter;
    TAnagAltriDatiPresentazioneBreve: TMemoField;
    DBEdit67: TDBEdit;
          procedure TbBTitoliNewClick(Sender: TObject);
          procedure TbBTitoliDelClick(Sender: TObject);
          procedure TbBTitoliOKClick(Sender: TObject);
          procedure TbBTitoliCanClick(Sender: TObject);
          procedure ToolbarButton972Click(Sender: TObject);
          procedure TbBCorsiExtraNewClick(Sender: TObject);
          procedure TbBCorsiExtraDelClick(Sender: TObject);
          procedure TbBCorsiExtraOKClick(Sender: TObject);
          procedure TbBCorsiExtraCanClick(Sender: TObject);
          procedure ToolbarButton973Click(Sender: TObject);
          procedure TbBLinConNewClick(Sender: TObject);
          procedure TbBLinConDelClick(Sender: TObject);
          procedure TbBLinConModClick(Sender: TObject);
          procedure TbBLinConCanClick(Sender: TObject);
          procedure TbBEspLavNewClick(Sender: TObject);
          procedure TbBEspLavDelClick(Sender: TObject);
          procedure TbBEspLavModClick(Sender: TObject);
          procedure ToolbarButton9710Click(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure DBLookupComboBox4Click(Sender: TObject);
          procedure ToolbarButton971Click(Sender: TObject);
          procedure CheckListBox1ClickCheck(Sender: TObject);
          procedure ToolbarButton974Click(Sender: TObject);
          procedure SpeedButton1Click(Sender: TObject);
          procedure ToolbarButton975Click(Sender: TObject);
          procedure PageControl1Changing(Sender: TObject;
               var AllowChange: Boolean);
          procedure PageControl1Change(Sender: TObject);
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
          procedure TAnagMansioni_OLDBeforeOpen(DataSet: TDataSet);
          procedure TAnagMansioni_OLDAfterClose(DataSet: TDataSet);
          procedure RGProprietaClick(Sender: TObject);
          procedure BAziendaClick(Sender: TObject);
          procedure TAnagAltriDati_OLDAfterPost(DataSet: TDataSet);
          procedure BTabProvCVClick(Sender: TObject);
          procedure SpeedButton3Click(Sender: TObject);
          procedure BEspLavRetribClick(Sender: TObject);
          procedure BTitoloITAClick(Sender: TObject);
          procedure BTitoloENGClick(Sender: TObject);
          procedure BLinguaITAClick(Sender: TObject);
          procedure BLinguaENGClick(Sender: TObject);
          procedure BEspProfmatITAClick(Sender: TObject);
          procedure BEspProfmatENGClick(Sender: TObject);
          procedure BEspLavITAClick(Sender: TObject);
          procedure BEspLavENGClick(Sender: TObject);
          procedure FrmAnagConoscInfo1BVoceDelClick(Sender: TObject);
          procedure DbDateEdit971Change(Sender: TObject);
          procedure DBCTipoContratto_oldDropDown(Sender: TObject);
          procedure MultiDBCheckBoxFrame1SpeedButton3Click(Sender: TObject);
          procedure FrmAnagConoscInfo1BVoceNewClick(Sender: TObject);
          procedure SpeedButton2Click(Sender: TObject);
          procedure DBComboBox5Change(Sender: TObject);
          procedure BitBtn1Click(Sender: TObject);
          procedure ToolbarButton976Click(Sender: TObject);
          procedure BSalvaAreeInteresseClick(Sender: TObject);
          procedure BSalvaInteressiSviluppoClick(Sender: TObject);
          procedure BeliminaAreaClick(Sender: TObject);
          procedure NuovaAreaInteresseClick(Sender: TObject);
          procedure BTabAmbitiSviluppoClick(Sender: TObject);
          procedure BEliminaInteressiSviluppoClick(Sender: TObject);
          procedure BNuovoInteressiSviluppoClick(Sender: TObject);
          procedure bEliminaAlbiClick(Sender: TObject);
          procedure BConfermaAlbiClick(Sender: TObject);
          procedure bAnnullaAlbiClick(Sender: TObject);
          procedure BAggiungiAlbiClick(Sender: TObject);
          procedure btabTipoCatprofessClick(Sender: TObject);
          procedure DSanagcatprofessStateChange(Sender: TObject);
          procedure DSAnagAreeInteresseStateChange(Sender: TObject);
          procedure DSAnagInteressiSviluppoStateChange(Sender: TObject);
          procedure ToolbarButton977Click(Sender: TObject);
          procedure FrmAnagConoscInfo1BSelezAreaClick(Sender: TObject);
          procedure FrmAnagConoscInfo1BSelezSoftwareClick(Sender: TObject);
          procedure BSalvaInteressiPersClick(Sender: TObject);
          procedure BAnnullaAltriInteressiClick(Sender: TObject);
          procedure DSAnagInteressiPersStateChange(Sender: TObject);
     private
          xPaginaPrec: TTabsheet;
          { Private declarations }
     public
          { Public declarations }
          xModificata, xModPropCV: boolean;
          xIDAziendaProp: integer;
          xQualeScheda: string;
          procedure ChangeColor(NomeForm: TForm);
     end;

var
     CurriculumForm: TCurriculumForm;

implementation

uses ModuloDati, DettaglioTesi, Lingue, DettEspLav, SelArea, ModuloDati2,
     SelRuolo, SelDiploma, InsRuolo, View, CompInserite, EspLav, uUtilsVarie,
     CercaAnnuncio, Main, ElencoAziende, AnagAnnunci, CliAltriSettori,
     EspLavRetrib, SelQualifContr, TabCatProfess, InsAreaNew;

{$R *.DFM}

procedure TCurriculumForm.TbBTitoliNewClick(Sender: TObject);
begin
     if not MainForm.CheckProfile('05110') then Exit;
     SelDiplomaForm := TSelDiplomaForm.create(self);
     SelDiplomaForm.ShowModal;
     if SelDiplomaForm.ModalResult = mrcancel then begin
          SelDiplomaForm.Free;
     end else begin
          with Data.TTitoliStudio do begin
               Data.DB.BeginTrans;
               try
                    Insert;
                    Data.TTitoliStudio.FieldByname('IDAnagrafica').value := data.TAnagrafica.FieldByName('ID').value;
                    Data.TTitoliStudio.FieldByName('IDDiploma').value := SelDiplomaForm.Diplomi.FieldBYName('ID').value;
                    Post;

                    Data.QTemp.Close;
                    Data.QTemp.SQL.text := 'select @@IDENTITY as LastID';
                    Data.QTemp.Open;
                    Log_Operation(MainForm.xIDUtenteAttuale, 'TitoliStudio', Data.QTemp.FieldByName('LastID').asInteger, 'I');
                    Data.QTemp.Close;

                    Data.DB.CommitTrans;

                    Data.TTitoliStudio.close;
                    Data.TTitoliStudio.Open;
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                    raise;
               end;

          end;
          SelDiplomaForm.Free;
     end;
end;

procedure TCurriculumForm.TbBTitoliDelClick(Sender: TObject);
begin
     //[/TONI20020726\]
     //     if not MainForm.CheckProfile('05111') then Exit;
     if MainForm.CheckProfile('05111') then
     begin
          if not Data.TTitoliStudio.EOF then
               if MessageDlg('Sei sicuro di voler eliminarlo?', mtWarning,
                    [mbNo, mbYes], 0) = mrYes then begin
                    Data.QTemp.Close;
                    Data.QTemp.SQL.text := 'delete from TitoliStudio ' +
                         'where ID=' + Data.TTitoliStudio.FieldByName('ID').asString;
                    Data.QTemp.ExecSQL;
                    Data.TTitoliStudio.Close;
                    Data.TTitoliStudio.Open;
                    //Data.TTitoliStudio.Delete;
               end;
     end;
     //[/TONI20020726\]FINE
end;

procedure TCurriculumForm.TbBTitoliOKClick(Sender: TObject);
begin
     with Data.TTitoliStudio do begin
          //[/TONI20020726\]
          //          Data.DB.BeginTrans;
          Data.DB.BeginTrans;
          try
               //               ApplyUpdates;
               Post;
               Data.DB.CommitTrans;
          except
               //               Data.DB.RollbackTrans;
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               raise;
          end;
          //          CommitUpdates;
          //[/TONI20020726\]FINE
     end;
end;

procedure TCurriculumForm.TbBTitoliCanClick(Sender: TObject);
begin
     //[/TONI20020726\]
     with Data.TTitoliStudio do begin
          //          Data.DB.BeginTrans;
          Data.DB.BeginTrans;
          try
               //               CancelUpdates;
               //               Data.DB.CommitTrans;
               Post;
               Data.DB.CommitTrans;
          except
               //               Data.DB.RollbackTrans;
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               raise;
          end;
          //          CommitUpdates;
          //[/TONI20020726\]FINE
     end;
end;

procedure TCurriculumForm.ToolbarButton972Click(Sender: TObject);
begin
     DettaglioTesiForm := TDettaglioTesiForm.Create(self);
     DettaglioTesiForm.ShowModal;
     DettaglioTesiForm.Free;
end;

procedure TCurriculumForm.TbBCorsiExtraNewClick(Sender: TObject);
begin
     //[/TONI20020726\]
     //     if not MainForm.CheckProfile('05120') then Exit;
     if MainForm.CheckProfile('05120') then
     begin
          Data.TCorsiExtra.Insert;
          //     Data.TCorsiExtraIDAnagrafica.Value:=Data.TAnagraficaID.Value;
          Data.TCorsiExtra.FieldByname('IDAnagrafica').Value := Data.TAnagrafica.FieldByName('ID').Value;
     end;
     //[/TONI20020726\]FINE
end;

procedure TCurriculumForm.TbBCorsiExtraDelClick(Sender: TObject);
begin
     //[/TONI20020726\]
     //     if not MainForm.CheckProfile('05121') then Exit;
     if MainForm.CheckProfile('05121') then
     begin
          if not Data.TCorsiExtra.EOF then
               if MessageDlg('Sei sicuro di voler eliminarlo?', mtWarning,
                    [mbNo, mbYes], 0) = mrYes then Data.TCorsiExtra.Delete;
     end;
     //[/TONI20020726\]FINE
end;

procedure TCurriculumForm.TbBCorsiExtraOKClick(Sender: TObject);
begin
     with Data.TCorsiExtra do begin
          //[/TONI20020726\]
          Data.DB.Begintrans;
          try
               //               ApplyUpdates;
               Post;
               //               Data.DB.CommitTrans;
               Data.DB.CommitTrans;
               {               Close;
                              Open;}
          except
               //               Data.DB.RollbackTrans;
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               raise;
          end;
          //          CommitUpdates;
     end;
end;

procedure TCurriculumForm.TbBCorsiExtraCanClick(Sender: TObject);
begin
     with Data.TCorsiExtra do begin
          //[/TONI20020726\]
          //          Data.DB.BeginTrans;
          Data.DB.BeginTrans;
          try
               CancelUpdates;
               //               Data.DB.CommitTrans;
               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               raise;
          end;
          //          CommitUpdates;
          //[/TONI20020726\]FINE
     end;
end;

procedure TCurriculumForm.ToolbarButton973Click(Sender: TObject);
begin
     close
          // Procedura di attualizzazione dello stato "attuale" nei ruoli
{Data.Q2.Close;
Data.Q2.SQL.Text := 'select idmansione from esperienzelavorative where attuale = 1 and idanagrafica = ' + data.TAnagrafica.FieldByName('ID').AsString;
Data.Q2.Open;
if Data.Q2.RecordCount > 0 then begin
Data.Q3.Close;
Data.Q3.SQL.Text := 'update anagmansioni set attuale = 1
end;  }
end;

procedure TCurriculumForm.TbBLinConNewClick(Sender: TObject);
begin
     //[/TONI20020726\]
     //     if not MainForm.CheckProfile('05130') then Exit;
     if MainForm.CheckProfile('05130') then
     begin
          LingueForm := TLingueForm.Create(self);
          LingueForm.ShowModal;
          if LingueForm.ModalResult = mrOK then begin
               with Data do begin
                    //               DB.BeginTrans;
                    DB.BeginTrans;
                    try
                         Q1.Close;
                         {                    Q1.SQL.text:='insert into LingueConosciute (IDAnagrafica,IDLingua,Lingua,Livello,Soggiorno,LivelloScritto) '+
                                                  'values (:xIDAnagrafica,:xIDLingua,:xLingua,:xLivello,:xSoggiorno,:xLivelloScritto)';
                                             Q1.ParamByName('xIDAnagrafica').asInteger:=Data.TAnagraficaID.Value;
                                             Q1.ParamByName('xIDLingua').asInteger:=LingueForm.LingueID.Value;
                                             Q1.ParamByName('xLingua').asString:=LingueForm.LingueLingua.Value;
                                             Q1.ParamByName('xLivello').asInteger:=LingueForm.QLivLingueID.Value;
                                             Q1.ParamByName('xSoggiorno').asString:=LingueForm.ESoggiorno.text;
                                             Q1.ParamByName('xLivelloScritto').asInteger:=LingueForm.QLivLingue2ID.Value;}
                         {Q1.SetSQLText(['insert into LingueConosciute (IDAnagrafica,IDLingua,Lingua,Livello,Soggiorno,LivelloScritto) '+
                         'values (:xIDAnagrafica:,:xIDLingua:,:xLingua:,:xLivello:,:xSoggiorno:,:xLivelloScritto:)',
                              Data.TAnagrafica.FieldByName('ID').Value,
                              LingueForm.Lingue.FIeldByName('ID').Value,
                              LingueForm.Lingue.FieldByNAme('Lingua').Value,
                              LingueForm.QLivLingue.FieldByName('ID').Value,
                              LingueForm.ESoggiorno.text,
                              LingueForm.QLivLingue2.FieldByname('ID').Value]);
                    Q1.ExecSQL;  }
                         Q1.SQL.Text := 'insert into LingueConosciute (IDAnagrafica,IDLingua,Lingua,Livello,Soggiorno,LivelloScritto, IDComprensione) ' +
                              'values (:xIDAnagrafica:, :xIDLingua:, :xLingua:, :xLivello:,:xSoggiorno:, :xLivelloScritto:, :xIDComprensione:)';
                         Q1.ParamByName['xIDAnagrafica'] := Data.TAnagrafica.FieldByName('ID').Value;
                         Q1.ParamByName['xIDLingua'] := LingueForm.Lingue.FieldByName('ID').Value;
                         Q1.ParamByName['xLingua'] := LingueForm.Lingue.FieldByName('Lingua').Value;
                         Q1.ParamByName['xLivello'] := LingueForm.QLivLingue.FieldByName('ID').Value;
                         Q1.ParamByName['xSoggiorno'] := LingueForm.ESoggiorno.text;
                         Q1.ParamByName['xLivelloScritto'] := LingueForm.QLivLingue2.FieldByName('ID').Value;
                         Q1.ParamByName['xIDComprensione'] := LingueForm.QLivLingue3.FieldByName('ID').Value;
                         Q1.ExecSQL;

                         Data.QTemp.Close;
                         Data.QTemp.SQL.text := 'select @@IDENTITY as LastID';
                         Data.QTemp.Open;
                         Log_Operation(MainForm.xIDUtenteAttuale, 'LingueConosciute', Data.QTemp.FieldByName('LastID').asInteger, 'I');
                         Data.QTemp.Close;

                         DB.CommitTrans;

                         TLingueConosc.close;
                         TLingueConosc.Open;
                    except
                         DB.RollbackTRans;
                         MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                         raise;
                    end;
               end;
          end;
          LingueForm.Free;
     end;
     //[/TONI20020726\]FINE
end;

procedure TCurriculumForm.TbBLinConDelClick(Sender: TObject);
begin
     //[/TONI20020726\]
     //     if not MainForm.CheckProfile('05131') then Exit;
     if MainForm.CheckProfile('05131') then
     begin
          if Data.TLingueConosc.RecordCount > 0 then
               if MessageDlg('Sei sicuro di voler eliminarlo ?', mtWarning,
                    [mbNo, mbYes], 0) = mrYes then begin
                    with Data do begin
                         //                    DB.BeginTrans;
                         DB.BeginTrans;
                         try
                              Q1.Close;
                              {                         Q1.SQL.text:='delete from LingueConosciute '+
                                                            'where ID='+TLingueConoscID.asString;}
                              Q1.SQL.text := 'delete from LingueConosciute ' +
                                   'where ID=' + TLingueConosc.FieldByName('ID').asString;
                              Q1.ExecSQL;
                              //                         Log_Operation(MainForm.xIDUtenteAttuale,'LingueConosciute',Data.TLingueConoscID.value,'D');
                              Log_Operation(MainForm.xIDUtenteAttuale, 'LingueConosciute', Data.TLingueConosc.FieldByName('ID').value, 'D');

                              //                         DB.CommitTrans;
                              DB.CommitTrans;
                              //[/GIULIO20020928\]
                              TLingueConosc.close;
                              TLingueConosc.Open;
                         except
                              //                         DB.RollbackTrans;
                              DB.RollbackTrans;
                              MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                              raise;
                         end;
                    end;
               end;
     end;
     //[/TONI20020726\]FINE
end;

procedure TCurriculumForm.TbBLinConModClick(Sender: TObject);
var xID: integer;
begin
     //[/TONI20020726\]
     //     if not MainForm.CheckProfile('05130') then Exit;
     if MainForm.CheckProfile('05130') then
     begin
          LingueForm := TLingueForm.Create(self);
          {     LingueForm.Lingue.FindKey([Data.TLingueConoscLingua.value]);
               LingueForm.ESoggiorno.text:=Data.TLingueConoscSoggiorno.Value;
               LingueForm.QLivLingue.locate('ID',Data.TLingueConoscLivello.Value, []);
               LingueForm.QLivLingue2.locate('ID',Data.TLingueConoscLivelloScritto.Value, []);}

          LingueForm.Lingue.Close;
          LingueForm.Lingue.Open;

          LingueForm.Lingue.Locate('ID', Data.TLingueConosc.FieldByName('IDLingua').value, []);
          LingueForm.ESoggiorno.text := Data.TLingueConosc.FieldBYName('Soggiorno').AsString;
          LingueForm.QLivLingue.locate('ID', Data.TLingueConosc.FieldByName('Livello').Asinteger, []);
          LingueForm.QLivLingue2.locate('ID', Data.TLingueConosc.FieldByName('LivelloScritto').AsInteger, []);
           LingueForm.QLivLingue3.locate('ID', Data.TLingueConosc.FieldByName('IDComprensione').AsInteger, []);


          LingueForm.ShowModal;
          if LingueForm.ModalResult = mrOK then begin
               with Data do begin
                    //               DB.BeginTrans;
                    DB.BeginTrans;
                    try
                         Q1.Close;
                         {                    Q1.SQL.text:='update LingueConosciute set IDLingua=:xIDLingua, Lingua=:xLingua,Livello=:xLivello,LivelloScritto=:xLivelloScritto,Soggiorno=:xSoggiorno '+
                                                  'where ID='+Data.TLingueConoscID.asString;
                                             Q1.ParamByName('xIDLingua').asInteger:=LingueForm.LingueID.Value;
                                             Q1.ParamByName('xLingua').asString:=LingueForm.LingueLingua.Value;
                                             Q1.ParamByName('xLivello').asInteger:=LingueForm.QLivLingueID.Value;
                                             Q1.ParamByName('xLivelloScritto').asInteger:=LingueForm.QLivLingue2ID.Value;
                                             Q1.ParamByName('xSoggiorno').asString:=LingueForm.ESoggiorno.text;}
                         Q1.SetSQLtext(['update LingueConosciute set IDLingua=:xIDLingua:, Lingua=:xLingua:,Livello=:xLivello:,LivelloScritto=:xLivelloScritto:,Soggiorno=:xSoggiorno:, IDComprensione=:xIDComprensione: ' +
                              'where ID=' + Data.TLingueConosc.FieldByName('ID').asString,
                                   LingueForm.Lingue.FieldByName('ID').Value,
                                   LingueForm.Lingue.FieldByName('Lingua').Value,
                                   LingueForm.QLivLingue.FieldByName('ID').Value,
                                   LingueForm.QLivLingue2.FieldByName('ID').Value,
                                   LingueForm.ESoggiorno.text,
                                   LingueForm.QLivLingue3.FieldByName('ID').Value]);
                         Q1.ExecSQL;

                         Log_Operation(MainForm.xIDUtenteAttuale, 'LingueConosciute', Data.TLingueConosc.fieldbyname('ID').asInteger, 'U');

                         //                    DB.CommitTrans;
                         DB.CommitTrans;
                         //[/GIULIO20020928\]
                         // ripristinato per fare refresh e inserito locate su ID
                         xID := TLingueConosc.FieldByName('ID').value;
                         TLingueConosc.close;
                         TLingueConosc.Open;
                         TLingueConosc.Locate('ID', xID, []);
                    except
                         //                    DB.RollbackTrans;
                         DB.RollbackTrans;
                         MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                         raise;
                    end;
               end;
          end;
          LingueForm.Free;
     end;
     //[/TONI20020726\]FINE
end;

procedure TCurriculumForm.TbBLinConCanClick(Sender: TObject);
begin
     with Data.TLingueConosc do begin
          //[/TONI20020726\]
          //          Data.DB.BeginTrans;
          Data.DB.BeginTrans;
          try
               CancelUpdates;
               //               Data.DB.CommitTrans;
               Data.DB.CommitTrans;
          except
               //               Data.DB.RollbackTrans;
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               raise;
          end;
          //          CommitUpdates;
     end;
     //[/TONI20020726\]FINE
end;

procedure TCurriculumForm.TbBEspLavNewClick(Sender: TObject);
var xIDAnag, xTotComp, i: integer;
     xComp: array[1..500] of integer;
begin
     if MainForm.CheckProfile('05140') then
     begin
          xIDAnag := Data.Tanagrafica.FieldByName('ID').Value;
          EspLavForm := TEspLavForm.create(self);
          EspLavForm.ShowModal;
          if EspLavForm.ModalResult = mrOK then begin
               Data.TEspLav.Insert;
               Data.TEspLav.FieldByName('IDAnagrafica').Value := Data.TAnagrafica.FieldByName('ID').Value;
               if EspLavForm.RGOpzione.Itemindex = 0 then begin
                    Data.TEspLav.FieldByName('IDAzienda').Value := 0;
                    Data.TEspLav.FieldByName('AziendaNome').Value := '';
                    Data.TEspLav.FieldByName('IDSettore').Value := EspLavForm.TSettori.FieldByName('ID').Value;
               end;
               if EspLavForm.RGOpzione.Itemindex = 1 then begin
                    Data.TEspLav.FieldBYNAme('IDAzienda').Value := EspLavForm.TElencoAziende.FieldByName('ID').Value;
                    Data.TEspLav.FieldByName('AziendaNome').Value := EspLavForm.TElencoAziende.FieldBYName('Descrizione').Value;
                    Data.TEspLav.FieldByName('IDSettore').Value := EspLavForm.TElencoAziende.FieldByName('IDAttivita').Value;
                    // altri settori per l'azienda
                    Data.QTemp.Close;
                    Data.QTemp.SQL.text := 'select count(*) Tot from EBC_ClientiAttivita where IDCliente=' + EspLavForm.TElencoAziende.FieldByName('ID').asString;
                    Data.QTemp.Open;
                    if Data.QTemp.FieldByName('Tot').asInteger > 0 then begin
                         CliAltriSettoriForm := TCliAltriSettoriForm.create(self);
                         CliAltriSettoriForm.BitBtn2.Visible := True;
                         CliAltriSettoriForm.BitBtn1.Caption := 'OK';
                         CaricaApriClienti(EspLavForm.TElencoAziende.FieldByNAme('Descrizione').Value, '');
                         CliAltriSettoriForm.Caption := 'Altre attivit� (Annulla per tenere quella princ.)';
                         CliAltriSettoriForm.ShowModal;
                         if CliAltriSettoriForm.ModalResult = mrOK then
                              Data.TEspLav.FieldByName('IDSettore').Value := CliAltriSettoriForm.QCliSettori.FieldByNAme('IDAttivita').Value;
                         CliAltriSettoriForm.Free;
                    end;
                    Data.QTemp.Close;
               end;
               if EspLavForm.RGOpzione.Itemindex = 2 then begin
                    Data.TEspLav.FieldByname('IDAzienda').Value := 0;
                    Data.TEspLav.FieldByName('AziendaNome').Value := '';
                    Data.TEspLav.FieldBYName('IDSettore').Value := 0;
               end;
               Data.TEspLav.FieldByName('IDMansione').Value := EspLavForm.TRuoli.FieldByNAme('ID').Value;
               //in Calenti Scazza Non so perch� by Tom 13/01/2015
               //Data.TEspLav.FieldByNAme('IDArea').Value := EspLavForm.TAree.FieldBYNAme('ID').Value;
               Data.TEspLav.FieldByName('IDArea').Value := EspLavForm.TRuoli.FieldByNAme('IDArea').Value;
               Data.TEspLav.FieldByName('Attuale').Value := False;

               // ruolo presentazione - nella mail di Agrillo 13/5, non va valorizzato a default !
               //Data.TEspLav.FieldByNAme('RuoloPresentaz').Value := EspLavForm.TRuoli.FieldByName('Descrizione').Value;

               Data.DB.BeginTrans;
               try
                    Data.TEspLav.Post;

                    Data.QTemp.Close;
                    Data.QTemp.SQL.text := 'select @@IDENTITY as LastID';
                    Data.QTemp.Open;
                    Log_Operation(MainForm.xIDUtenteAttuale, 'EsperienzeLavorative', Data.QTemp.FieldByName('LastID').asInteger, 'I');
                    Data.QTemp.Close;

                    // controlla AnagMansioni
                    Data.QTemp.SQL.text := 'select ID from AnagMansioni where IDAnagrafica=:xIDAnagrafica: and IDMansione=:xIDMansione:';
                    Data.QTemp.ParamByName['xIDAnagrafica'] := Data.TAnagrafica.FieldByName('ID').asInteger;
                    Data.QTemp.ParamByName['xIDMansione'] := EspLavForm.TRuoli.FieldBYNAme('ID').Value;
                    Data.QTemp.Open;
                    if Data.QTemp.IsEmpty then begin
                         // attribuisci il ruolo e le competenze
                         {Data.Q1.SetSQLText(['insert into AnagMansioni (IDAnagrafica,IDMansione,Disponibile,IDArea) '+
                              'values (:xIDAnagrafica:,:xIDMansione:,:xDisponibile:,:xIDArea:)',
                                   Data.TAnagrafica.FieldByName('ID').Value,
                                   EspLavForm.TRuoli.FieldBYNAme('ID').Value,
                                   True,
                                   EspLavForm.TAree.FieldByName('ID').Value]);
                         Data.Q1.ExecSQL; }
                         Data.Q1.Close;
                         Data.Q1.SQL.Text := 'insert into AnagMansioni (IDAnagrafica,IDMansione,Disponibile,IDArea) ' +
                              'values (:xIDAnagrafica:, :xIDMansione:, :xDisponibile:, :xIDArea:)';
                         Data.Q1.ParamByName['xIDAnagrafica'] := Data.TAnagrafica.FieldByName('ID').Value;
                         Data.Q1.ParamByName['xIDMansione'] := EspLavForm.TRuoli.FieldByNAme('ID').Value;
                         Data.Q1.ParamByName['xDisponibile'] := TRUE;
                         Data.Q1.ParamByName['xIDArea'] := EspLavForm.TAree.FieldByName('ID').Value;
                         Data.Q1.ExecSQL;

                         if EspLavForm.CBInsComp.Checked then begin
                              // inserisci le competenze se mancanti
                              CompInseriteForm := TCompInseriteForm.create(self);
                              //                         CompInseriteForm.ERuolo.Text:=EspLavForm.TRuoliDescrizione.Value;
                              CompInseriteForm.ERuolo.Text := trim(EspLavForm.TRuoli.FieldByName('Descrizione').Value);
                              Data2.QCompRuolo.Close;
                              {                         Data2.QCompRuolo.Prepare;
                                                       Data2.QCompRuolo.Params[0].asInteger:=Data.TEspLavIDMansione.Value;}
                              //[/GIULIO20020928\]
                              Data2.QCOmpRuolo.SQL.Text := 'select CompetenzeMansioni.*, Descrizione DescCompetenza ' +
                                   'from CompetenzeMansioni, Competenze where CompetenzeMansioni.IDCompetenza = Competenze.ID ' +
                                   'and IDMansione=' + data.TEspLav.FieldByname('IDMansione').AsString;
                              Data2.QCompRuolo.Open;
                              Data2.QCompRuolo.First;
                              xTotComp := 0;
                              Data2.TAnagCompIDX.Open;
                              CompInseriteForm.SG1.RowCount := 2;
                              CompInseriteForm.SG1.RowHeights[0] := 16;
                              while not Data2.QCompRuolo.EOF do begin
                                   //                              if not Data2.TAnagCompIDX.FindKey([xIDAnag,Data2.QCompRuoloIDCompetenza.Value]) then begin
                                   if not Data2.TAnagCompIDX.Locate('idcompetenza;idanagrafica', vararrayof([xIDAnag, Data2.QCompRuolo.FieldByName('IDCompetenza').asInteger]), []) then begin
                                        CompInseriteForm.SG1.RowHeights[xTotComp + 1] := 16;
                                        //                                   CompInseriteForm.SG1.Cells[0,xTotComp+1]:=Data2.QCompRuoloDescCompetenza.Value;
                                        CompInseriteForm.SG1.Cells[0, xTotComp + 1] := Data2.QCompRuolo.FieldByName('DescCompetenza').Value;
                                        CompInseriteForm.SG1.Cells[1, xTotComp + 1] := '0';
                                        //                                   if Data.TAnagraficaIDTipoStato.Value<>2 then begin
                                        if Data.TAnagrafica.FieldByName('IDTipoStato').Value <> 2 then begin
                                             CompInseriteForm.SG1.Cells[2, xTotComp + 1] := '�';
                                             CompInseriteForm.SG1.RowColor[xTotComp + 1] := clLime;
                                        end else begin
                                             CompInseriteForm.SG1.Cells[2, xTotComp + 1] := 'x';
                                             CompInseriteForm.SG1.RowColor[xTotComp + 1] := clRed;
                                        end;
                                        //                                   xComp[xTotComp+1]:=Data2.QCompRuoloIDCompetenza.Value;
                                        xComp[xTotComp + 1] := Data2.QCompRuolo.FieldByName('IDCompetenza').Value;
                                        //                                   CompInseriteForm.SG1.Cells[0,xTotComp+1]:=Data2.QCompRuoloDescCompetenza.Value;
                                        CompInseriteForm.SG1.Cells[0, xTotComp + 1] := Data2.QCompRuolo.FieldByName('DescCompetenza').Value;
                                        Inc(xTotComp);
                                        CompInseriteForm.SG1.RowCount := CompInseriteForm.SG1.RowCount + 1;
                                   end;
                                   Data2.QCompRuolo.Next;
                              end;
                              CompInseriteForm.SG1.RowCount := CompInseriteForm.SG1.RowCount - 1;
                              if xTotComp > 0 then CompInseriteForm.ShowModal;
                              if CompInseriteForm.ModalResult = mrOK then begin
                                   for i := 1 to CompInseriteForm.SG1.RowCount - 1 do begin


                                        //if CompInseriteForm.SG1.RowColor[i] = clLime then begin
                                        {if CompInseriteForm.SG1.Cells[2, i] = '�' then begin
                                             Data.Q1.SQL.Text := 'insert into CompetenzeAnagrafica (IDAnagrafica,IDCompetenza,Valore) ' +
                                                  'values (' + inttostr(xIDAnag) + ',' + inttostr(xComp[i]) + ',' + CompInseriteForm.SG1.Cells[1, i] + ')';
                                             Data.Q1.ExecSQL;
                                             // storico competenza
                                             Data.Q1.SQL.text := 'insert into StoricoCompAnag (IDCompetenza,IDAnagrafica,DallaData,MotivoAumento,valore) ' +
                                                  'values (' + IntToStr(xComp[i]) + ',' + IntToStr(xIDAnag) + ',' + DateToStr(Date) + ',''valore iniziale'',' + CompInseriteForm.SG1.Cells[1, i] + ')';
                                             Data.Q1.ExecSQL;
                                        end; }


                                   end;
                              end;
                              Data2.TAnagCompIDX.Close;
                              CompInseriteForm.Free;
                         end;
                    end;
                    Data.QTemp.Close;
                    Data.DB.CommitTrans;
               except
                    Data.DB.RollBackTrans;
                    MessageDlg('ATTENZIONE: errore sul database - Inserimento non avvenuto', mtError, [mbOK], 0);
                    raise;
               end;
               //[/GIULIO20020928\]
               Data.TEspLav.Close;
               Data.TEspLav.Open;
               Data.TEspLav.Last;
          end;
          EspLavForm.Free;
     end;
     //[/TONI20020726\]FINE
end;

procedure TCurriculumForm.TbBEspLavDelClick(Sender: TObject);
begin
     //[/TONI20020726\]
     //     if not MainForm.CheckProfile('05141') then Exit;
     if MainForm.CheckProfile('05141') then
     begin
          if not Data.TEspLav.IsEmpty then
               if MessageDlg('Sei sicuro di voler eliminarlo?', mtWarning,
                    [mbNo, mbYes], 0) = mrYes then begin
                    with Data do begin
                         DB.BeginTrans;
                         try
                              Q1.Close;
                              Q1.SQL.text := 'delete from EsperienzeLavorative ' +
                                   'where ID=' + TEspLav.FieldByName('ID').asString;
                              Q1.ExecSQL;
                              Log_Operation(MainForm.xIDUtenteAttuale, 'EsperienzeLavorative', TEspLav.FieldByName('ID').value, 'D');
                              DB.CommitTrans;
                              TEspLav.Close;
                              TEspLav.Open;
                         except
                              DB.RollbackTrans;
                              MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                              raise;
                         end;
                    end;

               end;
     end;
     //[/TONI20020726\]FINE
end;

procedure TCurriculumForm.TbBEspLavModClick(Sender: TObject);
var xIDVecchioRuolo: integer;
     xIDAnag, xTotComp, i: integer;
     xComp: array[1..50] of integer;
begin
     //[/TONI20020726\]
     //     if not MainForm.CheckProfile('05142') then Exit;
     if MainForm.CheckProfile('05142') then
     begin
          if Data.TEspLav.RecordCount > 0 then begin
               EspLavForm := TEspLavForm.create(self);
               //          if Data.TEspLavIDAzienda.asString<>'' then begin
               if Data.TEspLav.FieldByName('IDAzienda').asString <> '' then begin
                    EspLavForm.RGOpzione.ItemIndex := 1;
                    //EspLavForm.PanAziende.Visible := True;
                    //EspLavForm.PanSettori.Visible := False;
                    EspLavForm.GroupBox1.Visible := True;
                    EspLavForm.GroupBox2.Visible := False;
                    //               EspLavForm.EAzienda.text:=Data.TEspLavAzDenominazione.Value;
                    //[/GIULIO20020928\]
                    if Data.TEspLav.FieldByNAme('AzDenominazione').Value <> NULL then begin
                         EspLavForm.EAzienda.text := trim(Data.TEspLav.FieldByNAme('AzDenominazione').Value);
                         EspLavForm.EAziendaChange(self);
                    end;
               end else begin
                    //               EspLavForm.TSettori.Locate('ID',Data.TEspLavIDSettore.Value, []);
                    EspLavForm.TSettori.Locate('ID', Data.TEspLav.FieldByName('IDSettore').Value, []);
                    EspLavForm.RGOpzione.ItemIndex := 0;
                    //EspLavForm.PanAziende.Visible := False;
                    //EspLavForm.PanSettori.Visible := True;
                    EspLavForm.GroupBox1.Visible := False;
                    EspLavForm.GroupBox2.Visible := True;
               end;
               EspLavForm.GroupBox3.Visible := False;
               EspLavForm.Height := 300;
               EspLavForm.ShowModal;
               if EspLavForm.ModalResult = mrOK then begin
                    // Data.TEspLav.Edit;
                    Data.QTemp.close;
                    if EspLavForm.RGOpzione.ItemIndex = 0 then begin
                         Data.QTemp.SQL.text := 'update esperienzelavorative set idsettore = :xidsettore:, aziendanome =:xaziendanome: where id = ' + Data.TEspLavID.asstring;
                         Data.QTemp.ParamByName['xidsettore'] := EspLavForm.TSettori.FieldByName('ID').Value;
                         Data.QTemp.ParamByName['xaziendanome'] := '';
                         Data.QTemp.ExecSQL;
                         //    Data.TEspLav.FieldByName('IDSettore').Value := EspLavForm.TSettori.FieldByName('ID').Value;
                           //  Data.TEspLav.FieldByName('AziendaNome').Value := '';
                    end else begin
                         if EspLavForm.RGOpzione.ItemIndex = 1 then begin
                              Data.QTemp.SQL.text := 'update esperienzelavorative set idsettore = :xidsettore:, aziendanome =:xaziendanome:, aziendacomune =:aziendacomune: , aziendaprovincia =:aziendaprovincia: , idazienda = :xidazienda: where id = ' + Data.TEspLavID.asstring;
                              Data.QTemp.ParamByName['xidsettore'] := EspLavForm.TElencoAziende.FieldByName('IDAttivita').Value;
                              Data.QTemp.ParamByName['xaziendanome'] := EspLavForm.TElencoAziende.FieldByName('Descrizione').Value;
                              Data.QTemp.ParamByName['xidazienda'] := EspLavForm.TElencoAziende.FieldByName('ID').Value;
                              Data.QTemp.ExecSQL;
                         //Data.TEspLav.FieldByName('IDSettore').Value := EspLavForm.TElencoAziende.FieldByName('IDAttivita').Value;
                         //Data.TEspLav.FieldByName('IDAzienda').Value := EspLavForm.TElencoAziende.FieldByName('ID').Value;
                         //Data.TEspLav.FieldByName('AziendaNome').Value := EspLavForm.TElencoAziende.FieldByName('Descrizione').Value;

                         // altri settori per l'azienda
                              Data.QTemp.Close;
                              Data.QTemp.SQL.text := 'select count(*) Tot from EBC_ClientiAttivita where IDCliente=' + EspLavForm.TElencoAziende.FieldByName('ID').asString;
                              Data.QTemp.Open;
                              if Data.QTemp.FieldByName('Tot').asInteger > 0 then begin
                                   CliAltriSettoriForm := TCliAltriSettoriForm.create(self);
                                   CliAltriSettoriForm.BitBtn2.Visible := True;
                                   CliAltriSettoriForm.BitBtn1.Caption := 'OK';
                                   CaricaApriClienti(EspLavForm.TElencoAziende.FieldByName('Descrizione').Value, '');
                                   CliAltriSettoriForm.Caption := 'Altre attivit� (Annulla per tenere quella princ.)';
                                   CliAltriSettoriForm.ShowModal;
                                   if CliAltriSettoriForm.ModalResult = mrOK then begin
                                        Data.QTemp.Close;
                                        Data.QTemp.SQL.text := 'update esperienzelavorative set idsettore = :xidsettore: where id = ' + Data.TEspLavID.asstring;
                                        Data.QTemp.ParamByName['xidsettore'] := CliAltriSettoriForm.QCliSettori.FieldByName('IDAttivita').Value;
                                        Data.QTemp.ExecSQL;
                                   //     Data.TEspLav.FieldByName('IDSettore').Value := CliAltriSettoriForm.QCliSettori.FieldByName('IDAttivita').Value;

                                   end;
                                   CliAltriSettoriForm.Free;

                              end;
                              Data.QTemp.Close;
                         end else begin
                              if EspLavForm.RGOpzione.ItemIndex = 2 then begin
                                   Data.QTemp.SQL.text := 'update esperienzelavorative set idsettore = :xidsettore:, aziendanome =:xaziendanome:, aziendacomune =:aziendacomune: , aziendaprovincia =:aziendaprovincia: , idazienda = :xidazienda: where id = ' + Data.TEspLavID.asstring;
                                   Data.QTemp.ParamByName['xidsettore'] := 0;
                                   Data.QTemp.ParamByName['xaziendanome'] := '';
                                   Data.QTemp.ParamByName['xidazienda'] := 0;
                                   Data.QTemp.ExecSQL;
                              end;
                         end;
                         //  exit;
                    end;
                    // Data.DB.BeginTrans;
                     //try
                    //      Data.TEspLav.Post;
                       //   Data.DB.CommitTrans;
                     //except
                       //   Data.DB.RollBackTrans;
                         // MessageDlg('ATTENZIONE: errore sul database - Inserimento non avvenuto', mtError, [mbOK], 0);
                          //raise;
                     //end;
               end else begin
                    if MessageDlg('Vuoi azzerare i dati sull''azienda ?', mtInformation, [mbYes, mbNo], 0) = mrYes then begin
                         Data.TEspLav.Edit;
                         Data.TEspLav.FieldByName('IDSettore').asString := '';
                         Data.TEspLav.FieldByName('IDAzienda').asString := '';
                         Data.TEspLav.FieldByname('AziendaNome').asString := '';
                         Data.DB.BeginTrans;
                         try
                              Data.TESpLav.Post;
                              Data.DB.CommitTrans;
                         except
                              Data.DB.RollBackTrans;
                              MessageDlg('ATTENZIONE: errore sul database - Inserimento non avvenuto', mtError, [mbOK], 0);
                              raise;
                         end;
                    end;
               end;
               //[/GIULIO20020928\]
               Data.TEspLav.Close;
               Data.TEspLav.Open;
               EspLavForm.Free;
          end;
     end;
     //[/TONI20020726\]FINE
end;

procedure TCurriculumForm.ToolbarButton9710Click(Sender: TObject);
begin
     DettEspLavForm := TDettEspLavForm.Create(self);
     DettEspLavForm.ShowModal;
     if DettEspLavForm.ModalResult = mrOK then begin
          //[/TONI20020726\]
         //          Data.DB.BeginTrans;
          Data.DB.BeginTrans;
          try
               //               Data.TEspLav.ApplyUpdates;
               Data.TEspLav.Edit;
               Data.TEspLav.Post;
               //               Data.DB.CommitTrans;
               Data.DB.CommitTrans;
          except
               //               Data.DB.RollbackTrans;
               Data.DB.RollBackTrans;
               MessageDlg('ATTENZIONE: errore sul database - Inserimento non avvenuto', mtError, [mbOK], 0);
               raise;
          end;
          //          Data.TEspLav.CommitUpdates;
           //[/TONI20020726\]FINE
     end;
     DettEspLavForm.Free;
end;

procedure TCurriculumForm.FormShow(Sender: TObject);
var i: integer;
     x: string;
begin
     //Grafica
     Panel8.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel8.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel8.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     Panel6.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel6.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel6.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     Panel14.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel14.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel14.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     Panel15.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel15.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel15.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     Panel13.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel13.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel13.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     CurriculumForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     PanCatProtette.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     PanCatProtette.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     PanCatProtette.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel9.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel9.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel9.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel3.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel3.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel3.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     PanEspProfMaturata.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     PanEspProfMaturata.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     PanEspProfMaturata.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     PanFerrari.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     PanFerrari.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     PanFerrari.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel4.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel4.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel4.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel10.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel10.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel10.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel11.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel11.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel11.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel16.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel16.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel16.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel12.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel12.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel12.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel7.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel7.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel7.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     DBCtrlGrid1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     DBCtrlGrid2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     DBCtrlGrid3.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     DBCtrlGrid4.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     DBCtrlGrid5.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     DBCtrlGrid1.SelectedColor := MainForm.AdvToolBarOfficeStyler1.BorderColor;
     DBCtrlGrid2.SelectedColor := MainForm.AdvToolBarOfficeStyler1.BorderColor;
     DBCtrlGrid3.SelectedColor := MainForm.AdvToolBarOfficeStyler1.BorderColor;
     DBCtrlGrid4.SelectedColor := MainForm.AdvToolBarOfficeStyler1.BorderColor;
     DBCtrlGrid5.SelectedColor := MainForm.AdvToolBarOfficeStyler1.BorderColor;
     FrmAnagConoscInfo1.Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     FrmAnagConoscInfo1.DBCtrlGrid1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     FrmAnagConoscInfo1.DBCtrlGrid1.SelectedColor := MainForm.AdvToolBarOfficeStyler1.BorderColor;
     Wallpaper1.color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     CampiPersFrame1.PanTop.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     CampiPersFrame1.Pan1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     CampiPersFrame1.Pan2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     CampiPersFrame1.Pan3.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     CampiPersFrame1.Pan4.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     CampiPersFrame1.Pan5.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     CampiPersFrame1.Pan6.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     CampiPersFrame1.Pan7.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     CampiPersFrame1.Pan8.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     CampiPersFrame1.Pan9.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     CampiPersFrame1.Pan10.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     MultiDBCheckBoxFrame1.PanTitolo.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     MultiDBCheckBoxFrame1.PanTitolo.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     MultiDBCheckBoxFrame1.PanTitolo.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     MultiDBCheckBoxFrame1.Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     MultiDBCheckBoxFrame1.Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     MultiDBCheckBoxFrame1.Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel5.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel5.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel5.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     MultiDBCheckBoxFrame2.PanTitolo.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     MultiDBCheckBoxFrame2.PanTitolo.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     MultiDBCheckBoxFrame2.PanTitolo.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     MultiDBCheckBoxFrame2.Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     MultiDBCheckBoxFrame2.Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     MultiDBCheckBoxFrame2.Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     MultiDBCheckBoxFrame3.PanTitolo.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     MultiDBCheckBoxFrame3.PanTitolo.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     MultiDBCheckBoxFrame3.PanTitolo.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     MultiDBCheckBoxFrame3.Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     MultiDBCheckBoxFrame3.Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     MultiDBCheckBoxFrame3.Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     if data.Global.fieldbyname('debughrms').AsBoolean then Showmessage('1');
     Caption := '[M/051] - curriculum vitae';
     if data.Global.fieldbyname('debughrms').AsBoolean then Showmessage('2');
     PageControl1.ActivePage := TSCVAltriDati;
     if data.Global.fieldbyname('debughrms').AsBoolean then Showmessage('3');
     if not mainForm.CheckProfile('0510') then TSCVAltriDati.Visible := False

     else TSCVAltriDati.Visible := true;
     if data.Global.fieldbyname('debughrms').AsBoolean then Showmessage('5');
     xModificata := False;
     if data.Global.fieldbyname('debughrms').AsBoolean then Showmessage('6');


     if pos('EMERGENCY', UpperCase(data.Global.fieldbyname('NomeAzienda').asString)) > 0 then begin
          label56.Caption := 'Anni';
     end;


     TAnagAltriDati.SQL.text := 'select * from AnagAltreInfo where IDAnagrafica=' + Data.TAnagrafica.FieldByName('ID').asString;
     TAnagAltriDati.Open;
     if data.Global.fieldbyname('debughrms').AsBoolean then Showmessage('7');
     DBCtrlGrid4.Height := 335;
     if data.Global.fieldbyname('debughrms').AsBoolean then Showmessage('8');
     if Data.TAnagrafica.FieldByName('Sesso').Value = 'F' then CurriculumForm.GroupBox2.Visible := False
     else CurriculumForm.GroupBox2.Visible := True;
     //     if (Data.TAnagraficaIDProprietaCV.value=0)or(Data.TAnagraficaIDProprietaCV.asString='') then begin
     if data.Global.fieldbyname('debughrms').AsBoolean then Showmessage('9');
     if (Data.TAnagrafica.FieldByName('IDProprietaCV').value = 0) or (Data.TAnagrafica.FieldByName('IDProprietaCV').asString = '') then begin
          if data.Global.fieldbyname('debughrms').AsBoolean then Showmessage('91');
          RGProprieta.ItemIndex := 0;
          BAzienda.Visible := False;
          xIDAziendaProp := 0;
     end else begin
          if data.Global.fieldbyname('debughrms').AsBoolean then Showmessage('92');
          RGProprieta.ItemIndex := 1;
          BAzienda.Visible := True;
          xIDAziendaProp := Data.TAnagrafica.FieldByName('IDProprietaCV').value;
     end;
     xModPropCV := False;
     // caricamento DBCBProvCV con le provenienze da tabella
     Data.Q1.Close;
     Data.Q1.SQL.text := 'select Provenienza from ProvenienzeCV';
     Data.Q1.Open;
     if data.Global.fieldbyname('debughrms').AsBoolean then Showmessage('10');
     DBCBProvCV.items.Clear;
     while not Data.Q1.EOF do begin
          if data.Global.fieldbyname('debughrms').AsBoolean then Showmessage('11');
          DBCBProvCV.items.Add(Data.Q1.FieldByName('Provenienza').asString);
          Data.Q1.Next;
     end;
     Data.Q1.Close;

     // impostazioni generalizzate
     PageControl1.Pages[5].TabVisible := true;
     PageControl1.Pages[6].TabVisible := true;
     //PanCatProtette.Visible := False;

     // Categorie protette invisibile per TUTTI tranne che per ZUCCHETTI
     // if Uppercase(copy(Data.GlobalNomeAzienda.Value,1,9))='ZUCCHETTI' then begin
     if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').Value, 1, 9)) = 'ZUCCHETTI' then begin
          PageControl1.Pages[5].TabVisible := True;
          PanCatProtette.Visible := True;
     end;

     if pos('INTERMEDIA', Uppercase(Data.Global.FieldByName('NomeAzienda').Value)) > 0 then begin //Uppercase(copy(Data.Global.FieldByName('NomeAzienda').Value,1,6))='BOYDEN' then begin
          DBEdit43.Visible := false;
          dbedit43.DataField := '';
          RetribIntermedia.DataField := 'Retribuzione';
          RetribIntermedia.Visible := true;
          Retribintermedia.Top := dbedit43.Top;
          Retribintermedia.Width := dbedit43.Width;
          Retribintermedia.Height := dbedit43.Height;
          Retribintermedia.Left := dbedit43.Left;
          RetribIntermedia.TabOrder := 0;
          if data.Global.fieldbyname('debughrms').AsBoolean then Showmessage('23');
          Data.TEspLavAttuale.DisplayValues := 'attuale;non attuale';
          {RetribVarIntermedia.DataField := 'RetribVariabile';
          RetribVarIntermedia.Visible := true;
          RetribVarIntermedia.Top := dbedit51.Top;
          RetribVarIntermedia.Width := dbedit51.Width;
          RetribVarIntermedia.TabOrder := 2;
          DBEdit51.Visible := false;
          dbedit51.DataField := '';
          RetribVarIntermedia.DisplayFormat := '';
          if data.Global.fieldbyname('debughrms').AsBoolean then Showmessage('24');
          RetribTotIntermedia.DataField := 'RetribTotale';
          RetribTotIntermedia.Visible := true;
          RetribTotIntermedia.Top := dbedit52.Top;
          RetribTotIntermedia.Width := dbedit52.Width;
          RetribTotIntermedia.TabOrder := 3;
          DBEdit52.Visible := false;
          dbedit52.DataField := '';
          RetribTotIntermedia.DisplayFormat := '';
          if data.Global.fieldbyname('debughrms').AsBoolean then Showmessage('25');
          DBEdit52.Visible := false;  }
          if data.Global.fieldbyname('debughrms').AsBoolean then Showmessage('20');
          GroupBox6.Visible := false;
          MultiDBCheckBoxFrame1.Visible := false;
          MultiDBCheckBoxFrame2.Visible := false;
          MultiDBCheckBoxFrame3.Visible := false;
          DBCtrlGrid4.RowCount := 2;
          DBCtrlGrid4.PanelHeight := 170;
          if data.Global.fieldbyname('debughrms').AsBoolean then Showmessage('21');
          DBComboBox4.Items.clear;
          DBComboBox4.Items.Add('Formazione/Specializzazione');
          DBComboBox4.Items.Add('Dottorato di Ricerca/Ph D');
          DBComboBox4.Items.Add('Stage');
          if data.Global.fieldbyname('debughrms').AsBoolean then Showmessage('22');
          GroupBox8.Top := 130;
          eNoteCv.visible := true;
          lnote.visible := true;
          if data.Global.fieldbyname('debughrms').AsBoolean then Showmessage('26');
     end;

     //     if Uppercase(copy(Data.GlobalNomeAzienda.Value,1,9))='LEDERM' then begin
     if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').Value, 1, 9)) = 'LEDERM' then begin
          GroupBox1.Visible := False;
          GroupBox6.Visible := False;
          GroupBox2.Visible := False;
          DBCheckBox9.Visible := False;
          MultiDBCheckBoxFrame1.Visible := False;
          Label54.Visible := False;
          DBEdit40.Visible := False;
          Label58.Visible := False;
          DBEdit42.Visible := False;
          Label45.Visible := False;
          DBEdit38.Visible := False;
     end;
     //     if Uppercase(copy(Data.GlobalNomeAzienda.Value,1,6))='BOYDEN' then begin
     if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').Value, 1, 6)) = 'BOYDEN' then begin
          DbCbNoCV.Visible := True;
          DbDEDataRealeCV.Visible := False;
          LDataRealeCV.Visible := False;
     end;

     if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').Value, 1, 4)) = 'RM BOYDEN' then begin
          DbCbNoCV.Visible := True;
          DbDEDataRealeCV.Visible := False;
          LDataRealeCV.Visible := False;
     end;

     //     if Uppercase(copy(Data.GlobalNomeAzienda.Value,1,6))='ADVANT' then begin
     if (Uppercase(copy(Data.Global.FieldByName('NomeAzienda').Value, 1, 6)) = 'ADVANT') or (pos('MCS', UpperCase(Data.Global.FieldByName('NomeAzienda').Value)) > 0) then begin
          DBCtrlGrid4.Height := 395;
          LTipoContratto.Visible := False;
          DBCTipoContratto.Visible := False;
          LLivelloContr.Visible := False;
          DBELivelloContr.Visible := False;
          LMensilita.Visible := False;
          DBEMensilita.Visible := False;
     end;

     //     if Uppercase(copy(Data.GlobalNomeAzienda.Value,1,7))='FERRARI' then begin
     if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 7)) = 'FERRARI' then begin
          PageControl1.Pages[5].TabVisible := True;
          PageControl1.Pages[6].TabVisible := True;
          PanEspProfMaturata.Visible := True;
          BTitoloITA.Visible := True;
          BTitoloENG.Visible := True;
          BLinguaITA.Visible := True;
          BLinguaENG.Visible := True;
          BEspLavITA.Visible := True;
          BEspLavENG.Visible := True;
          DBEdit24.DataField := 'AziendaSede';
     end;

     if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 3)) = 'CSP' then
          PageControl1.Pages[5].TabVisible := True;


     if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 10)) = 'TESSILFORM' then
          PageControl1.Pages[5].TabVisible := True;

     // ### PER TEST: le impostazioni per FERRARI anche per EBC
   //     if Uppercase(copy(Data.GlobalNomeAzienda.Value,1,3))='EBC' then begin
     if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 3)) = 'EBC' then begin
          PageControl1.Pages[5].TabVisible := True;
          PageControl1.Pages[6].TabVisible := True;
          PanEspProfMaturata.Visible := True;
          BTitoloITA.Visible := True;
          BTitoloENG.Visible := True;
          BLinguaITA.Visible := True;
          BLinguaENG.Visible := True;
          BEspLavITA.Visible := True;
          BEspLavENG.Visible := True;
     end;

     if pos('ALFA', Uppercase(Data.Global.FieldByName('NomeAzienda').Value)) > 0 then begin //Uppercase(copy(Data.Global.FieldByName('NomeAzienda').Value,1,6))='BOYDEN' then begin
          PageControl1.Pages[5].TabVisible := True;
          PanCatProtette.Visible := True;
          DBComboBox3.Visible := False;
          DBEdit11.Visible := False;
          Label6.Caption := 'Categoria protetta ai sensi della L.68/1999';
          Label7.Visible := false;
          Label8.Visible := false;

          { FrmAnagConoscInfo1.DBComboBox1.Items.Clear;
           FrmAnagConoscInfo1.DBComboBox1.Items.Add('Elementare');
           FrmAnagConoscInfo1.DBComboBox1.Items.Add('Intermedio');
           FrmAnagConoscInfo1.DBComboBox1.Items.Add('Avanzato');   }
     end;

     if pos('SOGETEL', Uppercase(Data.Global.FieldByName('NomeAzienda').Value)) > 0 then begin //Uppercase(copy(Data.Global.FieldByName('NomeAzienda').Value,1,6))='BOYDEN' then begin
          PanCatProtette.Visible := true;
     end;

     // nuovo MultiDBCheckbox
     if MultiDBCheckBoxFrame1.Visible then begin
          with MultiDBCheckBoxFrame1 do begin
               Pantitolo.Caption := ' Disponibilit� nei confronti delle tipologie di contratto';
               //          xIDAnag:=Data.TAnagraficaID.Value;
               xIDAnag := Data.TAnagrafica.FieldByName('ID').Value;
               xTabBase := 'DispContratti';
               xTabCorrelaz := 'AnagDispContratti';
               xCampoVis := 'DispContratto';
               xNomeCampo := 'Tipologia di contratto';
               xForeignKey := 'IDDispContratto';
               LoadCLBox;
          end;
     end;

     // nuovo MultiDBCheckbox
     if MultiDBCheckBoxFrame2.Visible then begin
          with MultiDBCheckBoxFrame2 do begin
               Pantitolo.Caption := ' Condizioni Attuali';
               xIDAnag := Data.TAnagrafica.FieldByName('ID').Value;
               xTabBase := 'TabCondizioniAttuali';
               xTabCorrelaz := 'AnagCondizioniAttuali';
               xCampoVis := 'Voce';
               xNomeCampo := 'Condizioni Attuali';
               xForeignKey := 'IDTabCondizioniAttuali';
               LoadCLBox;
          end;
     end;

     // nuovo MultiDBCheckbox
     if MultiDBCheckBoxFrame3.Visible then begin
          with MultiDBCheckBoxFrame3 do begin
               Pantitolo.Caption := ' Disponibilit� nei Movimenti';
               xIDAnag := Data.TAnagrafica.FieldByName('ID').Value;
               xTabBase := 'DispMovimento';
               xTabCorrelaz := 'AnagDispMovimento';
               xCampoVis := 'Descrizione';
               xNomeCampo := 'Disponibilit�';
               xForeignKey := 'IDDispMovimento';
               LoadCLBox;
          end;
     end;

     if xQualeScheda = 'EspLav' then begin
          PageControl1.activePage := TSCVespLav;
          PageControl1Change(self);
     end;
     if xQualeScheda = 'Lingue' then begin
          PageControl1.activePage := TSCVLingue;
          PageControl1Change(self);
     end;
     if xQualeScheda = 'TitStudio' then begin
          PageControl1.activePage := TSCVTitoliStudio;
          PageControl1Change(self);
     end;
end;

procedure TCurriculumForm.DBLookupComboBox4Click(Sender: TObject);
begin
     // selezione area e filtro
     SelAreaForm := TSelAreaForm.create(self);
     SelAreaForm.ShowModal;
     if SelAreaForm.ModalResult = mrOK then begin
          Data.TMansioniLK.Filtered := True;
          //[/TONI20020726\]
         //          Data.TMansioniLK.Filter:='IDArea='+Data2.TAreeID.asString;
          Data.TMansioniLK.Filter := 'IDArea=' + Data2.TAree.FieldByName('ID').asString;
     end;
     SelAreaForm.Free;

     //     Data.DB.BeginTrans;
     Data.DB.BeginTrans;
     try
          if Data.Q1.Active then Data.Q1.Close;
          {          Data.Q1.SQL.text:='update EsperienzeLavorative set IDArea='+Data2.TAreeID.asString+
                         ' where ID='+Data.TEspLavID.asString;}
          Data.Q1.SQL.text := 'update EsperienzeLavorative set IDArea=' + Data2.TAree.FieldByName('ID').asString +
               ' where ID=' + Data.TEspLav.FieldByName('ID').asString;
          Data.Q1.ExecSQL;
          //          Data.DB.CommitTrans;
          Data.DB.CommitTrans;
     except
          //          Data.DB.RollbackTrans;
          Data.DB.RollBackTrans;
          //[/TONI20020726\]FINE
          MessageDlg('ATTENZIONE: errore sul database - Inserimento non avvenuto', mtError, [mbOK], 0);
          raise;
     end;
end;

procedure TCurriculumForm.ToolbarButton971Click(Sender: TObject);
var xFile: string;
     xProcedi: boolean;
begin
     //     ApriCV(Data.TAnagraficaID.value);
     ApriCV(Data.TAnagrafica.FieldByName('ID').value);
end;

procedure TCurriculumForm.CheckListBox1ClickCheck(Sender: TObject);
begin
     xModificata := True;
end;

procedure TCurriculumForm.ToolbarButton974Click(Sender: TObject);
begin
     //     ApriFileWord(Data.TAnagraficaCVNumero.asString);
     ApriFileWord(Data.TAnagrafica.FieldByName('CVNumero').asString);
end;

procedure TCurriculumForm.SpeedButton1Click(Sender: TObject);
begin
     AnagAnnunciForm := TAnagAnnunciForm.create(self);
     AnagAnnunciForm.ShowModal;
     AnagAnnunciForm.Free;
end;

procedure TCurriculumForm.ToolbarButton975Click(Sender: TObject);
var xIDVecchioRuolo: integer;
     xIDAnag, xTotComp, xIDCompAnag, i: integer;
     xComp: array[1..50] of integer;
begin
     //  if not MainForm.CheckProfile('05143') then Exit;
     if MainForm.CheckProfile('05143') then
     begin
          xIDAnag := Data.TAnagraficaID.AsInteger;
          if not Data.TEspLav.IsEmpty then begin
               if Data.TEspLav.FieldByName('IDMansione').Value <> NULL then
                    xIDVecchioRuolo := Data.TEspLav.FieldByName('IDMansione').Value
               else xIDVecchioRuolo := 0;
               InsRuoloForm := TInsRuoloForm.create(self);
               if Data.TEspLav.FieldByName('IDArea').asString <> '' then begin
                    InsRuoloForm.TRuoli.Locate('IDArea', Data.TEspLav.FieldByName('IDArea').Value, []);
               end;
               InsRuoloForm.ShowModal;
               if InsRuoloForm.ModalResult = mrOK then begin
                    Data.TEspLav.edit;
                    Data.TEspLav.FieldByName('IDMansione').Value := InsRuoloForm.TRuoli.FieldByName('ID').Value;
                    Data.TEspLav.FieldByName('IDArea').Value := InsRuoloForm.TRuoli.FieldByName('IDArea').Value;
                    //Data.TEspLav.FieldByName('RuoloPresentaz').Value := InsRuoloForm.TRuoli.FieldByName('Ruolo').Value;

                    Data.DB.BeginTrans;
                    try
                         Data.TESPLav.Post;
                         // se il ruolo � nuovo inseriscilo e inserisci le competenze
                         if xIDVecchioRuolo <> Data.TEspLav.FieldByName('IDMansione').Value then begin

                              Data2.TAnagMansIDX.Open;
                              if not Data2.TAnagMansIDX.FindKeyADO(VarArrayOf([Data.TAnagrafica.FieldByName('ID').AsInteger, InsRuoloForm.TRuoli.FieldByName('ID').AsInteger])) then begin
                                   // attribuisci il ruolo e le competenze
                                   Data.Q1.SetSQLtext(['insert into AnagMansioni (IDAnagrafica,IDMansione,Disponibile,IDArea) ' +
                                        'values (:xIDAnagrafica:,:xIDMansione:,:xDisponibile:,:xIDArea:)',
                                             Data.TAnagrafica.FieldByName('ID').AsInteger,
                                             InsRuoloForm.TRuoli.FieldByName('ID').AsInteger,
                                             True,
                                             InsRuoloForm.TRuoli.FieldByName('IDArea').AsInteger]);
                                   Data.Q1.ExecSQL;

                                   if InsRuoloForm.CBInsComp.Checked then begin
                                        // inserisci le competenze se mancanti
                                        CompInseriteForm := TCompInseriteForm.create(self);
                                        //                                   CompInseriteForm.ERuolo.Text:=InsRuoloForm.TRuoliDescrizione.Value;
                                        CompInseriteForm.ERuolo.Text := InsRuoloForm.TRuoli.FieldByName('Ruolo').AsString;
                                        Data2.QCompRuolo.Close;
                                        Data2.QCOmpRuolo.SetSQLText(['select * from CompetenzeMansioni where IDMansione=:x:',
                                             Data.TEspLav.FieldByname('IDMansione').AsINteger]);
                                        Data2.QCompRuolo.Open;
                                        Data2.QCompRuolo.First;
                                        xTotComp := 0;
                                        Data2.TAnagCompIDX.Open;
                                        CompInseriteForm.SG1.RowCount := 2;
                                        CompInseriteForm.SG1.RowHeights[0] := 16;
                                        while not Data2.QCompRuolo.EOF do begin
                                             // if not Data2.TAnagCompIDX.FindKey([xIDAnag,Data2.QCompRuoloIDCompetenza.Value]) then begin
                                             if not Data2.TAnagCompIDX.FindKeyADO(VarArrayOf([xIDAnag, Data2.QCompRuolo.FieldByName('IDCompetenza').AsInteger])) then begin
                                                  CompInseriteForm.SG1.RowHeights[xTotComp + 1] := 16;
                                                  //  CompInseriteForm.SG1.Cells[0,xTotComp+1]:=Data2.QCompRuoloDescCompetenza.Value;
                                                  CompInseriteForm.SG1.Cells[0, xTotComp + 1] := Data2.QCompRuolo.FieldByName('DescCompetenza').AsString;
                                                  CompInseriteForm.SG1.Cells[1, xTotComp + 1] := '0';
                                                  //  if Data.TAnagraficaIDTipoStato.Value<>2 then begin
                                                  if Data.TAnagrafica.FieldByName('IDTipoStato').AsInteger <> 2 then begin
                                                       CompInseriteForm.SG1.Cells[2, xTotComp + 1] := '�';
                                                       CompInseriteForm.SG1.RowColor[xTotComp + 1] := clLime;
                                                  end else begin
                                                       CompInseriteForm.SG1.Cells[2, xTotComp + 1] := 'x';
                                                       CompInseriteForm.SG1.RowColor[xTotComp + 1] := clRed;
                                                  end;
                                                  // xComp[xTotComp+1]:=Data2.QCompRuoloIDCompetenza.Value;
                                                  xComp[xTotComp + 1] := Data2.QCompRuolo.FieldByName('IDCompetenza').AsInteger;
                                                  Inc(xTotComp);
                                                  CompInseriteForm.SG1.RowCount := CompInseriteForm.SG1.RowCount + 1;
                                             end;
                                             Data2.QCompRuolo.Next;
                                        end;
                                        CompInseriteForm.SG1.RowCount := CompInseriteForm.SG1.RowCount - 1;
                                        if xTotComp > 0 then CompInseriteForm.ShowModal;
                                        if CompInseriteForm.ModalResult = mrOK then begin
                                             for i := 1 to CompInseriteForm.SG1.RowCount - 1 do begin


                                                  //if CompInseriteForm.SG1.RowColor[i] = clLime then begin
                                                  {if CompInseriteForm.SG1.Cells[2, i] = '�' then begin
                                                       xIDCompAnag := CheckAssociazCandCompetenza(xIDAnag, xComp[i]);
                                                       if xIDCompAnag < 0 then begin

                                                            Data.Q1.SQL.Text := 'insert into CompetenzeAnagrafica (IDAnagrafica,IDCompetenza,Valore) ' +
                                                                 'values (' + inttostr(xIDAnag) + ',' + inttostr(xComp[i]) + ',' + CompInseriteForm.SG1.Cells[1, i] + ')';
                                                            //  data.Q1.SQL.SaveToFile('q1.sql');
                                                            Data.Q1.ExecSQL;
                                                            // storico competenza
                                                            Data.Q1.SQL.text := 'insert into StoricoCompAnag (IDCompetenza,IDAnagrafica,DallaData,MotivoAumento,valore) ' +
                                                                 'values (' + IntToStr(xComp[i]) + ',' + IntToStr(xIDAnag) + ',' + DateToStr(Date) + ',''valore iniziale'',' + CompInseriteForm.SG1.Cells[1, i] + ')';
                                                            Data.Q1.ExecSQL;
                                                       end else begin
                                                            // competenza gi� associata: prendi il valore pi� alto
                                                            Data.QTemp.Close;
                                                            Data.QTemp.SQL.Clear;
                                                            Data.QTemp.SQL.text := 'select Valore from CompetenzeAnagrafica where ID=' + IntToStr(xIDCompAnag);
                                                            Data.QTemp.Open;
                                                            if StrToInt(CompInseriteForm.SG1.Cells[1, i]) > Data.QTemp.FieldByName('Valore').asInteger then begin
                                                                 Data.Q1.Close;
                                                                 Data.Q1.SQL.text := 'update CompetenzeAnagrafica set Valore=:x where ID=' + IntToStr(xIDCompAnag);
                                                                 Data.Q1.Parameters[0].value := StrToInt(CompInseriteForm.SG1.Cells[1, i]);
                                                                 Data.Q1.ExecSQL;

                                                                 Data.Q1.SQL.text := 'insert into StoricoCompAnag (IDCompetenza,IDAnagrafica,DallaData,MotivoAumento,valore) ' +
                                                                      'values (:xIDCompetenza:,:xIDAnagrafica:,:xDallaData:,:xMotivoAumento:,:xvalore:)';
                                                                 Data.Q1.ParamByName['xIDCompetenza'] := xComp[i];
                                                                 Data.Q1.ParamByName['xIDAnagrafica'] := xIDAnag;
                                                                 Data.Q1.ParamByName['xDallaData'] := Date;
                                                                 Data.Q1.ParamByName['xMotivoAumento'] := 'nuovo ruolo - valore pi� alto';
                                                                 Data.Q1.ParamByName['xValore'] := StrToInt(CompInseriteForm.SG1.Cells[1, i]);
                                                                 Data.Q1.ExecSQL;
                                                            end;
                                                            Data.QTemp.Close;

                                                       end;
                                                  end;}


                                             end;
                                        end;
                                        Data2.TAnagCompIDX.Close;
                                        CompInseriteForm.Free;
                                   end;
                              end;
                              Data2.TAnagMansIDX.Close;
                         end;
                         Data.DB.CommitTrans;
                    except
                         Data.DB.RollBackTrans;
                         MessageDlg('ATTENZIONE: errore sul database - Inserimento non avvenuto', mtError, [mbOK], 0);
                         raise;
                    end;
               end else begin
                    if MessageDlg('Vuoi azzerare i dati relativi al ruolo/mansione ?', mtInformation, [mbYes, mbNo], 0) = mrYes then begin
                         Data.TEspLav.Edit;
                         Data.TEspLav.FieldByName('IDMansione').asString := '';
                         Data.TEspLav.FieldByName('IDArea').asString := '';
                         //Data.TEspLav.FieldByName('RuoloPresentaz').asString := '';
                         Data.DB.BeginTrans;
                         try
                              Data.TEspLav.Post;
                              Data.DB.CommitTrans;
                              Data.DB.CommitTrans;
                         except
                              Data.DB.RollbackTrans;
                              Data.DB.RollBackTrans;
                              MessageDlg('ATTENZIONE: errore sul database - Inserimento non avvenuto', mtError, [mbOK], 0);
                              raise;
                         end;
                    end;
               end;
               InsRuoloForm.Free;
               Data.TEspLav.Close;
               Data.TEspLav.Open;
          end;
     end;
     //[/TONI20020726\]FINE
end;

procedure TCurriculumForm.PageControl1Changing(Sender: TObject;
     var AllowChange: Boolean);
begin
     xPaginaPrec := PageControl1.ActivePage;
end;

procedure TCurriculumForm.PageControl1Change(Sender: TObject);
begin
     dbcombobox5.Items.Clear;
     data.qregioni.close;
     data.qregioni.open;

     while not Data.qregioni.EOF do begin
          DBComboBox5.Items.Add(data.Qregioni.fieldbyname('regione').Asstring);
          data.qregioni.next;
     end;

     // rivista tutta per aprire le query solo quando serve
     if (PageControl1.ActivePage = TSCVAltriDati) and
          (not mainForm.CheckProfile('0510')) then begin
          PageControl1.ActivePage := xPaginaPrec;
          Exit;
     end;

     if PageControl1.ActivePage = TSCVTitoliStudio then begin
          if not MainForm.CheckProfile('0511') then begin
               PageControl1.ActivePage := xPaginaPrec;
               Exit;
          end;
//          Data.TTitoliStudio.SQL.text := 'select TitoliStudio.*, Diplomi.* from TitoliStudio, Diplomi ' +
//               'where TitoliStudio.IDDiploma = Diplomi.ID and IDAnagrafica=' + Data.TAnagrafica.FieldByName('ID').asString;

          Data.TTitoliStudio.SQL.text := 'select TitoliStudio.*, Diplomi.ID,IDArea,Tipo,isnull(Descrizione, TitoliStudio.diplomadesc) as Descrizione,PunteggioMax,Diplomi.IDAzienda,Tipo_ENG,Descrizione_ENG,IDGruppo,Codice,Tipo_DEU,Descrizione_DEU,Tipo_ESP,Descrizione_ESP from TitoliStudio ' +
                'left join diplomi on diplomi.id=TitoliStudio.iddiploma where IDAnagrafica=' + Data.TAnagrafica.FieldByName('ID').asString;

          Data.TTitoliStudio.Open;



     end else begin
          if Data.DsTitoliStudio.state in [dsInsert, dsEdit] then Data.TTitoliStudio.Post;
          Data.TTitoliStudio.Close;
     end;

     if PageControl1.ActivePage = TSCVCorsiStage then begin
          if not MainForm.CheckProfile('0512') then begin
               PageControl1.ActivePage := xPaginaPrec;
               Exit;
          end;
          Data.TCorsiExtra.SQL.text := 'select * from CorsiExtra where IDAnagrafica=' + Data.TAnagrafica.FieldByName('ID').asString;
          Data.TCorsiExtra.Open;
     end else begin
          if Data.DsCorsiExtra.state in [dsInsert, dsEdit] then Data.TCorsiExtra.Post;
          Data.TCorsiExtra.Close;
     end;

     if PageControl1.ActivePage = TSCVLingue then begin
          if not MainForm.CheckProfile('0513') then begin
               PageControl1.ActivePage := xPaginaPrec;
               Exit;
          end;
          {Data.TLingueConosc.SQL.text := 'select LingueConosciute.ID, LingueConosciute.IDAnagrafica, Lingue.Lingua, ' +
               'Livello, LivelloScritto, LingueConosciute.IDLingua, ' +
               'Soggiorno, LivParlato.LivelloConoscenza LParlato, ' +
               'LivScritto.LivelloConoscenza LScritto ' +
               'from Lingue, LingueConosciute,LivelloLingue LivScritto,LivelloLingue LivParlato ' +
               'where LingueConosciute.IDLingua = Lingue.ID ' +
               '  and LingueConosciute.Livello *= LivParlato.ID ' +
               '  and LingueConosciute.LivelloScritto *= LivScritto.ID ' +
               'and IDAnagrafica=' + Data.TAnagrafica.FieldByName('ID').asString;}
          //Modifica Query (left join) by Thomas
          Data.TLingueConosc.SQL.text := 'select LingueConosciute.ID, LingueConosciute.IDAnagrafica, Lingue.Lingua, LingueConosciute.IDComprensione, ' +
               'Livello, LivelloScritto, LingueConosciute.IDLingua, ' +
               'Soggiorno, LivParlato.LivelloConoscenza LParlato, ' +
               'LivScritto.LivelloConoscenza LScritto' +
               ' from Lingue join LingueConosciute on LingueConosciute.IDLingua = Lingue.ID ' +
               ' left join LivelloLingue LivScritto on LingueConosciute.Livello = LivScritto.ID ' +
               ' left join LivelloLingue LivParlato on LingueConosciute.LivelloScritto = LivParlato.ID' +
               ' where IDAnagrafica=' + Data.TAnagrafica.FieldByName('ID').asString;
          Data.TLingueConosc.Open;
     end else begin
          if Data.DsLingueConosc.state in [dsInsert, dsEdit] then Data.TLingueConosc.Post;
          Data.TLingueConosc.Close;
     end;

     //altri interessi hobby e sport
     if (PageControl1.ActivePage = TSAltriInteressi) then begin

          qtemp.close;
          qtemp.sql.text := ' declare @ID as int ' +
               ' set @ID =  ' +  data.tanagraficaid.asstring+ 
               ' if not exists(select id from anaginteressipers where idanagrafica=@ID) ' +
               ' begin ' +
               ' insert into anaginteressipers (idanagrafica) values (@ID ) ' +
               ' end  ';
          qtemp.ExecSQL;

          QAnagOnteressiPers.close;
          QAnagOnteressiPers.open;

     end;


     if (PageControl1.ActivePage = TSCVespLav) then begin
          if not MainForm.CheckProfile('0514') then begin
               PageControl1.ActivePage := xPaginaPrec;
               Exit;
          end;

//          if pos('S.C.R.', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) > 0 then begin
               cbVisibileWeb.Visible := true;
//          end;



          if pos('INTERMEDIA', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) > 0 then begin
               Label42.visible := false;
               LMensilita.Visible := False;
               DBEdit35.visible := false;
               DBEMensilita.Visible := false;
               LTipoContratto.Visible := false;
               DBCTipoContratto.visible := false;
               LLivelloContr.visible := false;
               dbelivellocontr.Visible := false;
               ToolbarButton9710.visible := false;
               BEspLavRetrib.Visible := false;
          end;
          {Data.TEspLav.SQL.text := 'select EsperienzeLavorative.*, Mansioni.*, Aree.*, MansioniTemp.*, ' +
               'EBC_Attivita.Attivita Settore, EBC_Clienti.Descrizione AzDenominazione, ' +
               'EBC_Clienti.Indirizzo AzIndirizzo, EBC_Clienti.Comune AzComune, ' +
               'EBC_Clienti.Provincia AzProv, NumDipendenti, Fatturato, Telefono ' +
               'from EsperienzeLavorative,Mansioni,Aree,MansioniTemp, EBC_Attivita, ' +
               'EBC_Clienti ' +
               'where EsperienzeLavorative.IDMansione *= Mansioni.ID ' +
               'and EsperienzeLavorative.IDArea *= Aree.ID ' +
               'and EsperienzeLavorative.IDMansioneTemp *= MansioniTemp.ID ' +
               'and EsperienzeLavorative.IDSettore *= EBC_Attivita.ID ' +
               'and EsperienzeLavorative.IDAzienda *= EBC_Clienti.ID ' +
               'and EsperienzeLavorative.IDAnagrafica=' + Data.TAnagrafica.FieldByName('ID').asString +
               ' order by AnnoDal desc';}
          //Modifica Query (left join) by Thomas
          Data.TEspLav.SQL.text := 'select EsperienzeLavorative.*, Mansioni.ID,Mansioni.Descrizione,Mansioni.IDArea,Mansioni.FileMansionario,Mansioni.Mansioni, ' +
               'Mansioni.LKArea,Mansioni.TolleranzaPerc,Mansioni.Descrizione_ENG,Mansioni.IDAzienda, ' +
               'Mansioni.IDGrade,Mansioni.RApportiRuolo,Mansioni.FileType,Mansioni.DirFileMansionario, ' +
               'Mansioni.Tolleranza_2,Mansioni.Codice,Mansioni.VisibileWeb MansioniVisibileWeb, Aree.*, MansioniTemp.*, ' +
               'AreeSettori.AreaSettore,EBC_Attivita.Attivita Settore, EBC_Clienti.Descrizione AzDenominazione, ' +
               'EBC_Clienti.Indirizzo AzIndirizzo, EBC_Clienti.Comune AzComune, ' +
               'EBC_Clienti.Provincia AzProv, NumDipendenti, Fatturato, Telefono ' +
               ' from EsperienzeLavorative left join Mansioni on EsperienzeLavorative.IDMansione = Mansioni.ID' +
               ' left join Aree on EsperienzeLavorative.IDArea = Aree.ID' +
               ' left join MansioniTemp on EsperienzeLavorative.IDMansioneTemp = MansioniTemp.ID  ' +
               ' left join EBC_Attivita on EsperienzeLavorative.IDSettore = EBC_Attivita.ID  ' +
               ' left join EBC_Clienti on EsperienzeLavorative.IDAzienda = EBC_Clienti.ID' +
               ' left join AreeSettori on AreeSettori.ID=EBC_Attivita.IDAreaSettore' +
               ' where EsperienzeLavorative.IDAnagrafica=' + Data.TAnagrafica.FieldByName('ID').asString +
               ' order by AnnoDal desc';
          Data.TEspLav.Open;
     end else begin
          if Data.DsEspLav.state in [dsInsert, dsEdit] then Data.TEspLav.Post;
          Data.TEspLav.Close;
     end;

     //CONOSCENZE INFORMATICHE
     if PageControl1.ActivePage = TSCVConoscInfo then begin
          //FrmAnagConoscInfo1.QAnagConoscInfo.ParamByName('xIDAnag').asInteger:=Data.TAnagraficaID.Value;
          FrmAnagConoscInfo1.QAnagConoscInfo.Open;
     end else begin
          if FrmAnagConoscInfo1.DsQAnagConoscInfo.state in [dsedit, dsInsert] then
               FrmAnagConoscInfo1.QAnagConoscInfo.Post;
          FrmAnagConoscInfo1.QAnagConoscInfo.Close;
     end;

     //Aree di Interesse
     if PageControl1.ActivePage = TSAreeInteresse then begin
          if not MainForm.CheckProfile('0515') then Exit;
          QAnagAreeInteresse.close;
          QAnagAreeInteresse.open;
          QAnagInteressiSviluppo.close;
          QAnagInteressiSviluppo.Open;
     end;

     //albi/ordini
     if PageControl1.ActivePage = TSAlbiOrdini then begin
          if not MainForm.CheckProfile('0516') then Exit;
          QAnagCatProfess.close;
          QAnagCatProfess.open;
          QCatProfessLK.close;
          QCatProfessLK.open;
          if QAnagCatProfessID.AsString <> '' then
               DBCtrlGrid5.enabled := true
          else
               DBCtrlGrid5.enabled := false;
     end;

     if (not Mainform.CheckProfile('1022', false)) then begin //ALE 03012003
          TSCampiPers.Visible := False;
          Exit;
     end; //ALE 03012003 FINE
     CampiPersFrame1.xTipo := 'Anagrafica';
     CampiPersFrame1.CaricaValori;
     CampiPersFrame1.ImpostaCampi;
end;

procedure TCurriculumForm.FormClose(Sender: TObject;
     var Action: TCloseAction);
var x: string;
     i: integer;
begin
     if Data.DsTitoliStudio.State in [dsInsert, dsEdit] then begin
          with Data.TTitoliStudio do begin
               //[/TONI20020726\]
              //             Data.DB.BeginTrans;
               Data.DB.BeginTrans;
               try
                    //               ApplyUpdates;
                    Post;
                    //               Data.DB.CommitTrans;
                    Data.DB.CommitTrans;
               except
                    //                  Data.DB.RollbackTrans;
                    Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                    raise;
               end;
               //               CommitUpdates;
          end;
     end;
     if Data.DsCorsiExtra.State in [dsInsert, dsEdit] then begin
          with Data.TCorsiExtra do begin
               //               Data.DB.BeginTrans;
               Data.DB.BeginTrans;
               try
                    //                ApplyUpdates;
                    Post;
                    //                Data.DB.CommitTrans;
                    Data.DB.CommitTrans;
               except
                    //                    Data.DB.RollbackTrans;
                    Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate in CorsiExtra', mtError, [mbOK], 0);
                    raise;
               end;
               //               CommitUpdates;
          end;
     end;
     if Data.DsLingueConosc.State in [dsInsert, dsEdit] then begin
          with Data.TLingueConosc do begin
               //               Data.DB.BeginTrans;
               Data.DB.BeginTrans;
               try
                    //                 ApplyUpdates;
                    //                 Data.DB.CommitTrans;
                    Post;
                    Data.DB.Committrans;
               except
                    //                    Data.DB.RollbackTrans;
                    Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate in LingueConosc', mtError, [mbOK], 0);
                    raise;
               end;
               //               CommitUpdates;
          end;
     end;
     if Data.DsEspLav.State in [dsInsert, dsEdit] then begin
          with Data.TEspLav do begin
               //               Data.DB.BeginTrans;
               Data.DB.BeginTrans;
               try
                    //                ApplyUpdates;
                    //                Data.DB.CommitTrans;
                    POst;
                    Data.DB.CommitTrans;
               except
                    //                    Data.DB.RollbackTrans;
                    Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate in EspLav', mtError, [mbOK], 0);
                    raise;
               end;
               //               CommitUpdates;
          end;
     end;
     // risistemazione campo disp.contratto
     if dsAnagAltriDati.State in [dsInsert, dsEdit] then begin
          with TAnagAltriDati do begin
               //               Data.DB.BeginTrans;
               Data.DB.BeginTrans;
               try
                    //                    ApplyUpdates;
                    Post;
                    //                    Data.DB.CommitTrans;
                    Data.DB.CommitTrans;
               except
                    //                    Data.DB.RollbackTrans;
                    Data.DB.RollbackTRans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate in AnagAltriDati', mtError, [mbOK], 0);
                    raise;
               end;
               //               CommitUpdates;
          end;
     end;
     if Data.dsAnagrafica.State in [dsInsert, dsEdit] then begin
          with Data.TAnagrafica do begin
               //               Data.DB.BeginTrans;
               Data.DB.BeginTrans;
               try
                    //                    ApplyUpdates;
                    Post;
                    //                    Data.DB.CommitTrans;
                    Data.DB.CommitTrans;
               except
                    //                    Data.DB.RollbackTrans;
                    Data.DB.RollbackTRans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate in AnagAltriDati', mtError, [mbOK], 0);
                    raise;
               end;
               //               CommitUpdates;
          end;
     end;
     if xModPropCV then begin
          // *********************************************
          // ATTENZIONE:  NON FUNZIONA --> blocca tutto !!
          // *********************************************
          Data.QTemp.Close;
          {          Data.QTemp.SQL.Text:='update Anagrafica set IDProprietaCV='+IntToStr(xIDAziendaProp)+
                         ' where ID='+Data.TAnagraficaID.asString;}
          Data.QTemp.SQL.Text := 'update Anagrafica set IDProprietaCV=' + IntToStr(xIDAziendaProp) +
               ' where ID=' + Data.TAnagrafica.FieldByName('ID').asString;
          Data.QTemp.ExecSQL;
     end;
     TAnagAltriDati.Close;

     if Data.TTitoliStudio.active then Data.TTitoliStudio.Close;
     if Data.TCorsiExtra.active then Data.TCorsiExtra.Close;
     if Data.TLingueConosc.active then Data.TLingueConosc.Close;
     if Data.TEspLav.active then Data.TEspLav.Close;

     if FrmAnagConoscInfo1.DsQAnagConoscInfo.state in [dsedit, dsInsert] then
          FrmAnagConoscInfo1.QAnagConoscInfo.Post;
     if MultiDBCheckBoxFrame1.Visible then
          MultiDBCheckBoxFrame1.SaveToDb;
     if MultiDBCheckBoxFrame2.Visible then
          MultiDBCheckBoxFrame2.SaveToDb;
     if MultiDBCheckBoxFrame3.Visible then
          MultiDBCheckBoxFrame3.SaveToDb;
     //[/TONI20020726\]FINE
end;

procedure TCurriculumForm.TAnagMansioni_OLDBeforeOpen(DataSet: TDataSet);
begin
     TMansioniLK.Open;
end;

procedure TCurriculumForm.TAnagMansioni_OLDAfterClose(DataSet: TDataSet);
begin
     TMansioniLK.Close;
end;

procedure TCurriculumForm.RGProprietaClick(Sender: TObject);
begin
     if RGProprieta.ItemIndex = 0 then begin
          BAzienda.Visible := False;
          xIDAziendaProp := 0;
     end else BAzienda.Visible := True;
     xModPropCV := True;
end;

procedure TCurriculumForm.BAziendaClick(Sender: TObject);
begin
     ElencoAziendeForm := TElencoAziendeForm.create(self);
     //[/TONI20020726\]
    {     if not((Data.TAnagraficaIDProprietaCV.value=0)or(Data.TAnagraficaIDProprietaCV.asString='')) then begin
              ElencoAziendeForm.TElencoAziende.Close;
              ElencoAziendeForm.TElencoAziende.SQl.text:='select * from EBC_Clienti '+
                   'where ID='+Data.TAnagraficaIDProprietaCV.asString;
              ElencoAziendeForm.TElencoAziende.open;
         end;}
     if not ((Data.TAnagrafica.FieldByName('IDProprietaCV').AsINteger = 0) or (Data.TAnagrafica.FieldByName('IDProprietaCV').asString = '')) then begin
          ElencoAziendeForm.TElencoAziende.Close;
          ElencoAziendeForm.TElencoAziende.SQl.text := 'select * from EBC_Clienti ' +
               'where ID=' + Data.TAnagrafica.FieldByName('IDProprietaCV').asString;
          ElencoAziendeForm.TElencoAziende.open;
     end;
     ElencoAziendeForm.Showmodal;
     if ElencoAziendeForm.ModalResult = mrOK then begin
          //          xIDAziendaProp:=ElencoAziendeForm.TElencoAziendeID.Value;
          xIDAziendaProp := ElencoAziendeForm.TElencoAziende.FieldByName('ID').Value;
          //[/TONI20020726\]FINE
     end;
     ElencoAziendeForm.Free;
end;

procedure TCurriculumForm.TAnagAltriDati_OLDAfterPost(DataSet: TDataSet);
begin
     //[/TONI20020726\]
    //     Log_Operation(MainForm.xIDUtenteAttuale,'AnagAltreInfo',Data.TAnagraficaID.value,'U');
     Log_Operation(MainForm.xIDUtenteAttuale, 'AnagAltreInfo', Data.TAnagrafica.FieldByName('ID').value, 'U');
     //[/TONI20020726\]FINE
end;

procedure TCurriculumForm.BTabProvCVClick(Sender: TObject);
begin
     OpenSelFromTab('ProvenienzeCV', 'ID', 'Provenienza', 'Provenienza', '');
     // caricamento DBCBProvCV con le provenienze da tabella
     Data.Q1.Close;
     Data.Q1.SQL.text := 'select Provenienza from ProvenienzeCV';
     Data.Q1.Open;
     DBCBProvCV.items.Clear;
     while not Data.Q1.EOF do begin
          DBCBProvCV.items.Add(Data.Q1.FieldByName('Provenienza').asString);
          Data.Q1.Next;
     end;
     Data.Q1.Close;
end;

procedure TCurriculumForm.SpeedButton3Click(Sender: TObject);
begin
     OpenTab('TipiRetribuzione', ['ID', 'TipoRetrib', 'TipoRetribEng'], ['Dicitura italiano', 'Dicitura Inglese']);
     QTipiRetribLK.Close;
     QTipiRetribLK.Open;
end;

procedure TCurriculumForm.BEspLavRetribClick(Sender: TObject);
begin
     if Data.TEspLav.IsEmpty then exit;
     EspLavRetribForm := TEspLavRetribForm.create(self);
     EspLavRetribForm.showModal;
     EspLavRetribForm.Free;
end;

procedure TCurriculumForm.BTitoloITAClick(Sender: TObject);
begin
     Data.TTitoliStudio.Close;
     //Data.TTitoliStudio.FieldByName('Diploma').LookupResultField:='Descrizione';
     Data.TTitoliStudioDiploma.LookupResultField := 'Descrizione';
     Data.TTitoliStudio.Open;
end;

procedure TCurriculumForm.BTitoloENGClick(Sender: TObject);
begin
     Data.TTitoliStudio.Close;
     //Data.TTitoliStudio.FieldByName('Diploma').LookupResultField:='Descrizione_ENG';
     Data.TTitoliStudioDiploma.LookupResultField := 'Descrizione_ENG';
     Data.TTitoliStudio.Open;
end;

procedure TCurriculumForm.BLinguaITAClick(Sender: TObject);
begin
     Data.TLingueConosc.Close;
     Data.TLingueConoscLingua.LookupResultField := 'Lingua';
     Data.TLingueConoscDescLivello.LookupResultField := 'LivelloConoscenza';
     Data.TLingueConoscDescLivelloScritto.LookupResultField := 'LivelloConoscenza';
     Data.TLingueConosc.Open;
end;

procedure TCurriculumForm.BLinguaENGClick(Sender: TObject);
begin
     Data.TLingueConosc.Close;
     Data.TLingueConoscLingua.LookupResultField := 'Lingua_ENG';
     Data.TLingueConoscDescLivello.LookupResultField := 'LivelloConoscenza_ENG';
     Data.TLingueConoscDescLivelloScritto.LookupResultField := 'LivelloConoscenza_ENG';
     Data.TLingueConosc.Open;
end;

procedure TCurriculumForm.BEspProfmatITAClick(Sender: TObject);
begin
     TAnagAltriDati.Close;
     TAnagAltriDati.FieldByName('EspProfMaturata').LookupResultField := 'EspProfMaturata';
     TAnagAltriDati.Open;
end;

procedure TCurriculumForm.BEspProfmatENGClick(Sender: TObject);
begin
     TAnagAltriDati.Close;
     TAnagAltriDati.FieldByName('EspProfMaturata').LookupResultField := 'EspProfMaturata_ENG';
     TAnagAltriDati.Open;
end;

procedure TCurriculumForm.BEspLavITAClick(Sender: TObject);
begin
     Data.TEspLav.Close;
     Data.TEspLav.FieldByName('Settore').LookupResultField := 'Attivita';
     Data.TEspLav.Open;
     DBEdit5.DataField := 'Descrizione';
     DBEdit3.DataField := 'Descrizione_1';
end;

procedure TCurriculumForm.BEspLavENGClick(Sender: TObject);
begin
     Data.TEspLav.Close;
     Data.TEspLav.FieldByName('Settore').LookupResultField := 'Attivita_ENG';
     Data.TEspLav.Open;
     DBEdit5.DataField := 'Descrizione_ENG';
     DBEdit3.DataField := 'Descrizione_ENG_1';
end;

procedure TCurriculumForm.FrmAnagConoscInfo1BVoceDelClick(Sender: TObject);
begin
     FrmAnagConoscInfo1.BVoceDelClick(Sender);

end;

procedure TCurriculumForm.DbDateEdit971Change(Sender: TObject);
begin
     //if Data.DsAnagrafica.state<>dsEdit then Data.TAnagrafica.Edit;
end;

procedure TCurriculumForm.DBCTipoContratto_oldDropDown(Sender: TObject);
begin
     DBCTipoContratto_old.Items.Clear;
     Data.Q3.Close;
     Data.Q3.SQL.Text := 'select tipocontratto from tipicontratti order by tipocontratto';
     Data.Q3.Open;
     while not Data.Q3.Eof do begin
          DBCTipoContratto_old.Items.Add(Data.Q3.fieldbyname('tipocontratto').AsString);
          Data.Q3.Next;
     end;
end;

procedure TCurriculumForm.MultiDBCheckBoxFrame1SpeedButton3Click(
     Sender: TObject);
begin
     MultiDBCheckBoxFrame1.SpeedButton3Click(Sender);

end;

procedure TCurriculumForm.FrmAnagConoscInfo1BVoceNewClick(Sender: TObject);
begin
     FrmAnagConoscInfo1.BVoceNewClick(Sender);

end;

procedure TCurriculumForm.SpeedButton2Click(Sender: TObject);
begin
     OpenTab('ContrattiSettori', ['ID', 'Settore'], ['Settore']);
     QContrattiSettoriLK.Close;
     QContrattiSettoriLK.Open;
end;

procedure TCurriculumForm.DBComboBox5Change(Sender: TObject);
begin
     //data.Qregioni.Locate('regione',DBComboBox5.Text,0);
     //data.qregioni.fieldbyname('ID').asInteger;


end;

procedure TCurriculumForm.BitBtn1Click(Sender: TObject);
var xcontratto, xqualifica, xlivello: string;
     xidqualifcontr: integer;
begin
     SelQualifContrForm := TSelQualifContrForm.Create(self);
     SelQualifContrForm.ShowModal;
     if SelQualifContrForm.ModalResult = mrOk then begin
          xcontratto := SelQualifContrForm.QQualifContratto.Value;
          xqualifica := SelQualifContrForm.QQualifTipoContratto.Value;
          xlivello := SelQualifContrForm.QQualifQualifica.Value;
          xidqualifcontr := SelQualifContrForm.QQualifID.Value;

          DBCTipoContratto.Text := xqualifica;
          DBELivelloContr.Text := xlivello;
     end;
end;

procedure TCurriculumForm.ToolbarButton976Click(Sender: TObject);
var xcontratto, xqualifica, xlivello: string;
     xidqualifcontr: integer;
     qtemp: TAdoQuery;
begin
     SelQualifContrForm := TSelQualifContrForm.Create(self);
     SelQualifContrForm.ShowModal;
     if SelQualifContrForm.ModalResult = mrOk then begin
          Data.TEspLav.Edit;
          xcontratto := SelQualifContrForm.QQualifContratto.Value;
          xqualifica := SelQualifContrForm.QQualifTipoContratto.Value;
          xlivello := SelQualifContrForm.QQualifQualifica.Value;
          xidqualifcontr := SelQualifContrForm.QQualifID.Value;

          DBCTipoContratto.Text := xqualifica;
          DBELivelloContr.Text := xlivello;
          Data.TESPLav.Post;

          qtemp := Tadoquery.create(self);
          qtemp.Connection := data.DB;
          qtemp.SQL.Text := 'update esperienzelavorative set idqualifcontr=:xidqualifcontr where id=:xid';
          qtemp.Parameters[0].Value := xidqualifcontr;
          qtemp.Parameters[1].Value := Data.tesplavid.Value;
          qtemp.ExecSQL;

          qtemp.free;

     end;
end;

procedure TCurriculumForm.BSalvaAreeInteresseClick(Sender: TObject);
begin
     if not MainForm.CheckProfile('05152') then Exit;
     if QAnagAreeInteresseID.AsString <> '' then begin
          QAnagAreeInteresse.Edit;
          QAnagAreeInteresse.post;
     end;
end;

procedure TCurriculumForm.BSalvaInteressiSviluppoClick(Sender: TObject);
begin
     if not MainForm.CheckProfile('05155') then Exit;
     if QAnagInteressiSviluppoID.AsString <> '' then begin
          QAnagInteressiSviluppo.edit;
          QAnagInteressiSviluppo.Post;
     end;
end;

procedure TCurriculumForm.BeliminaAreaClick(Sender: TObject);
var xtemp: TADOQuery;
begin
     if not MainForm.CheckProfile('05151') then Exit;
     if MessageDlg('Sei sicuro di voler eliminare la voce selezionata?', mtWarning, [mbYes, mbNo], 0) = mryes then begin
          xtemp := TADOQuery.Create(nil);
          xtemp.Connection := Data.DB;
          xtemp.SQL.Text := 'delete anagareeinteresse where id=''' + QAnagAreeInteresseID.AsString + '''';
          xtemp.ExecSQL;
          xtemp.close;
          xtemp.Free;
          QAnagAreeInteresse.Close;
          QAnagAreeInteresse.Open;
     end;
end;

procedure TCurriculumForm.NuovaAreaInteresseClick(Sender: TObject);
var xtemp: TADOQuery;
begin
     if not MainForm.CheckProfile('05150') then Exit;
     InsAreaNewForm := TInsAreaNewForm.create(self);
     InsAreaNewForm.ShowModal;
     if InsAreaNewForm.ModalResult = mrOK then begin
          xtemp := TADOQuery.Create(nil);
          xtemp.Connection := Data.DB;
          xtemp.sql.text := 'insert into AnagAreeInteresse (idanagrafica,idarea) values (:xidanagrafica,:xidarea)';
          xtemp.Parameters.ParamByName('xidanagrafica').Value := data.TAnagraficaID.Value;
          xtemp.Parameters.ParamByName('xidarea').value := InsAreaNewForm.TAreeID.Value;
          xtemp.ExecSQL;
          xtemp.close;
          xtemp.Free;
     end;
     QAnagAreeInteresse.close;
     QAnagAreeInteresse.open;
     InsAreaNewForm.Close;
     InsAreaNewForm.Free;
end;

procedure TCurriculumForm.BTabAmbitiSviluppoClick(Sender: TObject);
begin
     if not MainForm.CheckProfile('05156') then Exit;
     OpenTab('AmbitiSviluppo', ['ID', 'Descrizione'], ['Descrizione'], true, true, 'Descrizione', '', 'Ambiti Interesse/Sviluppo');
end;

procedure TCurriculumForm.BEliminaInteressiSviluppoClick(Sender: TObject);
var xtemp: tadoquery;
begin
     if not MainForm.CheckProfile('05154') then Exit;
     if MessageDlg('Sei sicuro di voler eleinare la voce selezionata?' + chr(13) + QAnagInteressiSviluppoDescrizione.AsString, mtWarning, [mbYes, mbNo], 0) = mryes then begin
          xtemp := TADOQuery.Create(nil);
          xtemp.Connection := Data.DB;
          xtemp.SQL.Text := 'delete AnagInteressiSviluppo where id=''' + QAnagInteressiSviluppoID.AsString + '''';
          xtemp.ExecSQL;
          xtemp.close;
          xtemp.Free;
          QAnagInteressiSviluppo.close;
          QAnagInteressiSviluppo.open;
     end;
end;

procedure TCurriculumForm.BNuovoInteressiSviluppoClick(Sender: TObject);
var xIDAmbitoSviluppo: string;
     xtemp: tadoquery;
begin
     if not MainForm.CheckProfile('05153') then Exit;
     xIDAmbitoSviluppo := '';
     xIDAmbitoSviluppo := OpenSelFromTab('AmbitiSviluppo', 'ID', 'Descrizione', 'Ambiti Sviluppo', '', false);
     if xIDAmbitoSviluppo <> '' then begin
          xtemp := TADOQuery.Create(nil);
          xtemp.Connection := Data.DB;
          xtemp.sql.text := 'insert into AnagInteressiSviluppo (idanagrafica,IDAmbitoSviluppo) values (:xidanagrafica,:xIDAmbitoSviluppo) ';
          xtemp.Parameters.ParamByName('xidanagrafica').value := data.TAnagraficaID.Value;
          xtemp.Parameters.ParamByName('xIDAmbitoSviluppo').value := xIDAmbitoSviluppo;
          xtemp.ExecSQL;
          xtemp.close;
          xtemp.Free;
          QAnagInteressiSviluppo.close;
          QAnagInteressiSviluppo.open;
     end;
end;

procedure TCurriculumForm.bEliminaAlbiClick(Sender: TObject);
var xtemp: tadoquery;
begin
     if not MainForm.CheckProfile('05161') then Exit;
     if MessageDlg('Sei sicuro di voler cancellare "' + QAnagCatProfessCatProfessLK.AsString + '" ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then begin
          xtemp := TADOQuery.Create(nil);
          xtemp.Connection := Data.DB;
          xtemp.sql.Text := ' delete AnagCatProfess where id=''' + QAnagCatProfessID.AsString + '''';
          xtemp.ExecSQL;
          xtemp.close;
          xtemp.Free;
          QAnagCatProfess.close;
          QAnagCatProfess.open;
          if QAnagCatProfessID.AsString <> '' then
               DBCtrlGrid5.enabled := true
          else
               DBCtrlGrid5.enabled := false;
     end;
end;

procedure TCurriculumForm.BConfermaAlbiClick(Sender: TObject);
begin
     if not MainForm.CheckProfile('05162') then Exit;
     if QAnagCatProfessID.AsString <> '' then begin
          QAnagCatProfess.edit;
          QAnagCatProfess.Post;
     end;

end;

procedure TCurriculumForm.bAnnullaAlbiClick(Sender: TObject);
begin
     QAnagCatProfess.cancel;
end;

procedure TCurriculumForm.BAggiungiAlbiClick(Sender: TObject);
var xtemp: tadoquery;
begin
     if not MainForm.CheckProfile('05160') then Exit;
     TabCatProfessForm := TTabCatProfessForm.create(self);
     TabCatProfessForm.BOK.Visible := True;
     TabCatProfessForm.ShowModal;
     if TabCatProfessForm.ModalResult = mrok then begin
          xtemp := TADOQuery.Create(nil);
          xtemp.Connection := Data.DB;
          xtemp.sql.Text := ' insert into AnagCatProfess (idanagrafica,IDcatProfess) values (:xidanag , :xIdcatprofess) ';
          xtemp.Parameters.ParamByName('xidanag').value := data.TAnagraficaID.Value;
          xtemp.Parameters.ParamByName('xIdcatprofess').value := TabCatProfessForm.QCatProfessid.Value;
          xtemp.ExecSQL;
          xtemp.close;
          xtemp.Free;
          QAnagCatProfess.close;
          QAnagCatProfess.open;
          QCatProfessLK.Close;
          QCatProfessLK.Open;
          if QAnagCatProfessID.AsString <> '' then
               DBCtrlGrid5.enabled := true
          else
               DBCtrlGrid5.enabled := false;
     end;

     TabCatProfessForm.Free;
end;

procedure TCurriculumForm.btabTipoCatprofessClick(Sender: TObject);
begin
     if not MainForm.CheckProfile('05163') then Exit;
     TabCatProfessForm := TTabCatProfessForm.create(self);
     TabCatProfessForm.ShowModal;
     TabCatProfessForm.Free;
end;

procedure TCurriculumForm.DSanagcatprofessStateChange(Sender: TObject);
var b: boolean;
begin
     b := DSanagcatprofess.State in [dsEdit, dsInsert];
     BAggiungiAlbi.Enabled := not b;
     bEliminaAlbi.Enabled := not b;
     BConfermaAlbi.Enabled := b;
     bAnnullaAlbi.Enabled := b;
end;

procedure TCurriculumForm.DSAnagAreeInteresseStateChange(Sender: TObject);
var b: boolean;
begin
     b := DSAnagAreeInteresse.State in [dsEdit, dsInsert];
     BSalvaAreeInteresse.Enabled := b;
end;

procedure TCurriculumForm.DSAnagInteressiSviluppoStateChange(Sender: TObject);
var b: boolean;
begin
     b := DSAnagInteressiSviluppo.State in [dsEdit, dsInsert];
     BSalvaInteressiSviluppo.Enabled := b;
end;

procedure TCurriculumForm.ToolbarButton977Click(Sender: TObject);
begin
     ChangeColor(CurriculumForm);
end;

procedure TCurriculumForm.ChangeColor(NomeForm: Tform);
var i: integer;
     l: tstringlist;
begin
     l := tstringlist.Create;
     for i := 0 to NomeForm.ComponentCount - 1 do begin
          if NomeForm.Components[i].Classname = 'TPanel' then begin
               // showmessage( NomeForm.Components[i].name);
               l.Add(NomeForm.Components[i].name + '.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;');
               l.Add(NomeForm.Components[i].name + '.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;');
               l.Add(NomeForm.Components[i].name + '.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;');
          end;

          if (NomeForm.Components[i].Classname = 'tgroupbox') then begin
               l.Add(NomeForm.Components[i].name + '.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;');
          end;

          if NomeForm.Components[i].Classname = 'TDBCtrlGrid' then
               l.Add(NomeForm.Components[i].name + '.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;');

          {if ((NomeForm.Components[i].Classname <> 'TPanel') and
               (NomeForm.Components[i].Classname <> 'tgroupbox')) then begin
               l.Add('            ' + NomeForm.Components[i].name);
          end;
          }
     end;
     l.SaveToFile('grafica' + NomeForm.Name + '.txt');
     showmessage('OK.. Terminato..');
end;

procedure TCurriculumForm.FrmAnagConoscInfo1BSelezAreaClick(
     Sender: TObject);
begin
     FrmAnagConoscInfo1.BSelezAreaClick(Sender);
end;

procedure TCurriculumForm.FrmAnagConoscInfo1BSelezSoftwareClick(
     Sender: TObject);
begin
     FrmAnagConoscInfo1.BSelezSoftwareClick(Sender);

end;

procedure TCurriculumForm.BSalvaInteressiPersClick(Sender: TObject);
begin
     QAnagOnteressiPers.post;
end;

procedure TCurriculumForm.BAnnullaAltriInteressiClick(Sender: TObject);
begin
     QAnagOnteressiPers.cancel;
end;

procedure TCurriculumForm.DSAnagInteressiPersStateChange(Sender: TObject);
var
     b: Boolean;
begin
     b := DSAnagInteressiPers.State in [dsEdit, dsInsert];
     BSalvaInteressiPers.Enabled := b;
     BAnnullaAltriInteressi.Enabled := b;

end;

end.

