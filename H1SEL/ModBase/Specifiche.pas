
//[TONI20021002]DEBUGOK
unit Specifiche;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, ExtCtrls, ComCtrls, DBCtrls, TB97, ImgList, LookOut,
     Mask, DB, FrameDBRichEdit2;

type
     TSpecificheForm = class(TForm)
          Panel1: TPanel;
          Panel2: TPanel;
          Label1: TLabel;
          DBEdit1: TDBEdit;
          Label2: TLabel;
          DBEdit2: TDBEdit;
          Label3: TLabel;
          DBEdit3: TDBEdit;
          Label4: TLabel;
          Label5: TLabel;
          DBEdit4: TDBEdit;
          DBEdit5: TDBEdit;
          Panel3: TPanel;
          ImageList1: TImageList;
          ToolbarButton971: TToolbarButton97;
          PCSpec: TPageControl;
          TSInterne: TTabSheet;
          TSEsterne: TTabSheet;
          Timer1: TTimer;
          PanMess: TPanel;
          PanMess2: TPanel;
          FrameDBRichInt: TFrameDBRich;
          FrameDBRichEst: TFrameDBRich;
          SpeedButton1: TToolbarButton97;
          procedure ToolbarButton971Click(Sender: TObject);
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
          procedure Timer1Timer(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure FrameDBRichEstToolButton1Click(Sender: TObject);
          procedure FrameDBRichIntToolButton1Click(Sender: TObject);
          procedure FrameDBRichIntEditorEnter(Sender: TObject);
          procedure SpeedButton1Click(Sender: TObject);
    procedure FrameDBRichIntPrintButtonClick(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     SpecificheForm: TSpecificheForm;

implementation

uses ModuloDati, ModuloDati2, REMain, MDRicerche, Main, UnitPrintMemo,
     uUtilsVarie, IndirizziCliente;

{$R *.DFM}

procedure TSpecificheForm.ToolbarButton971Click(Sender: TObject);
begin
     SpecificheForm.ModalResult:=mrOK;
     //close;
end;

//[TONI20021002]DEBUGOK

procedure TSpecificheForm.FormClose(Sender: TObject;
     var Action: TCloseAction);
begin
     //if DataRicerche.DsRicerchePend.State=dsEdit then begin
     with DataRicerche.TRicerchePend do begin
          Data.DB.BeginTrans;
          try
               //               ApplyUpdates;
               if State = dsEdit then
                    Post;
               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               raise;
          end;
          //          CommitUpdates;
     end;
     //end;
     DataRicerche.QRicAttive.Close;
     Mainform.EseguiQRicAttive;
end;

procedure TSpecificheForm.Timer1Timer(Sender: TObject);
begin
     if FrameDBRichInt.Editor.Lines.Count > 0 then begin
          ScriviRegistry('H1SpecifInt', FrameDBRichInt.Editor.Lines.Text);
          PanMess2.caption := '  Ultimo salvataggio specifiche INTERNE nella chiave del Registry "HKEY_CURRENT_USER\Software\H1\H1SpecifInt.txt" alle ore ' + TimeToStr(Now);
     end;
     if FrameDBRichEst.Editor.Lines.Count > 0 then begin
          ScriviRegistry('H1SpecifEst', FrameDBRichEst.Editor.Lines.Text);
          PanMess.caption := '  Ultimo salvataggio specifiche INTERNE nella chiave del Registry "HKEY_CURRENT_USER\Software\H1\H1SpecifEst.txt" alle ore ' + TimeToStr(Now);
     end;
end;

procedure TSpecificheForm.FormShow(Sender: TObject);
begin
     Caption := '[M/22] - ' + caption;
     SpecificheForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel3.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color; 
     Panel3.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel3.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     PanMess.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     PanMess.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     PanMess.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     PanMess2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color; 
     PanMess2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     PanMess2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
end;

//[TONI20021002]DEBUGOK

procedure TSpecificheForm.FrameDBRichEstToolButton1Click(Sender: TObject);
begin
     FrameDBRichEst.PostExecute(Sender);
end;

procedure TSpecificheForm.FrameDBRichIntToolButton1Click(Sender: TObject);
begin
     FrameDBRichInt.PostExecute(Sender);
end;

procedure TSpecificheForm.FrameDBRichIntEditorEnter(Sender: TObject);
begin
     DataRicerche.TRicerchePend.Edit;
end;

procedure TSpecificheForm.SpeedButton1Click(Sender: TObject);
begin
     Aprihelp('22');
end;

procedure TSpecificheForm.FrameDBRichIntPrintButtonClick(Sender: TObject);
begin
  FrameDBRichInt.FilePrintCmdExecute(Sender);

end;

end.

