unit SchedaTestate;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls, Dialogs,
  StdCtrls, Forms, DBCtrls, DB, Mask, ExtCtrls, TB97, Wall;

type
  TSchedaTestateForm = class(TForm)
    ScrollBox: TScrollBox;
    Label1: TLabel;
    EditDenominazione: TDBEdit;
    Label2: TLabel;
    Label3: TLabel;
    EditAgenzia: TDBEdit;
    Label4: TLabel;
    EditResponsabile: TDBEdit;
    Label5: TLabel;
    EditTelefono: TDBEdit;
    Label6: TLabel;
    EditFax: TDBEdit;
    Label7: TLabel;
    EditEmail: TDBEdit;
    Label8: TLabel;
    Label9: TLabel;
    EditCosto: TDBEdit;
    Panel2: TPanel;
    Wallpaper1: TPanel;
    TbBTestataNew: TToolbarButton97;
    TbBTestataDel: TToolbarButton97;
    TbBTestataCan: TToolbarButton97;
    DBComboBox1: TDBComboBox;
    DBComboBox2: TDBComboBox;
    procedure TbBTestataNewClick(Sender: TObject);
    procedure TbBTestataDelClick(Sender: TObject);
    procedure TbBTestataCanClick(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  SchedaTestateForm: TSchedaTestateForm;

implementation

uses ModuloDati3;

{$R *.DFM}


procedure TSchedaTestateForm.TbBTestataNewClick(Sender: TObject);
begin
     DataSel_EBC.TTestate.Insert;
end;

procedure TSchedaTestateForm.TbBTestataDelClick(Sender: TObject);
begin
     if MessageDlg('Sei sicuro di voler eminimarla ?',mtWarning,
        [mbNo,mbYes],0)=mrYes then DataSel_EBC.TTestate.Delete;
end;

procedure TSchedaTestateForm.TbBTestataCanClick(Sender: TObject);
begin
     if DataSel_EBC.DsTestate.State in [dsInsert,dsEdit] then
        DataSel_EBC.TTestate.Post;
     close
end;

end.
