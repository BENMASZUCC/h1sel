unit MotiviRichiamoCand;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     TB97, ExtCtrls, Db, ADODB, dxDBTLCl, dxGrClms, dxTL, dxDBCtrl,
     dxDBGrid, dxCntner, StdCtrls, DtEdit97;

type
     TMotiviRichiamoCandForm = class(TForm)
          Panel1: TPanel;
          BAnnulla: TToolbarButton97;
          BOk: TToolbarButton97;
          QMotiviRichiamo: TADOQuery;
          QMotiviRichiamoID: TAutoIncField;
          QMotiviRichiamoCodice: TStringField;
          QMotiviRichiamoDescMotivoRichiamoCandidato: TStringField;
          QMotiviRichiamoPeriodoRichiamoSTD: TIntegerField;
          QMotiviRichiamoPeriodoPreavvisoFineRichiamo: TIntegerField;
          DSMotiviRichiamo: TDataSource;
          dxDBGrid33: TdxDBGrid;
          dxDBGridColumn1: TdxDBGridColumn;
          dxDBGridColumn2: TdxDBGridColumn;
          dxDBGridSpinColumn1: TdxDBGridSpinColumn;
          dxDBGridSpinColumn2: TdxDBGridSpinColumn;
          GroupBox1: TGroupBox;
          DateEdit1: TDateEdit97;
          QMotiviRichiamoDataCalc: TDateTimeField;
          procedure FormShow(Sender: TObject);
          procedure BAnnullaClick(Sender: TObject);
          procedure BOkClick(Sender: TObject);
          procedure QMotiviRichiamoAfterScroll(DataSet: TDataSet);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     MotiviRichiamoCandForm: TMotiviRichiamoCandForm;

implementation

uses main, modulodati;

{$R *.DFM}

procedure TMotiviRichiamoCandForm.FormShow(Sender: TObject);
begin
     //grafica
     Panel1.Color := mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;
     MotiviRichiamoCandForm.color := mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;
     DateEdit1.date := now;
     QMotiviRichiamo.Open;
end;

procedure TMotiviRichiamoCandForm.BAnnullaClick(Sender: TObject);
begin
     MotiviRichiamoCandForm.ModalResult := mrCancel;
end;

procedure TMotiviRichiamoCandForm.BOkClick(Sender: TObject);
begin
     dxDBGrid33.SetFocus;
     MotiviRichiamoCandForm.ModalResult := mrOK;
end;

procedure TMotiviRichiamoCandForm.QMotiviRichiamoAfterScroll(
     DataSet: TDataSet);
begin
     if pos('EMERGENCY', UpperCase(data.Global.fieldbyname('NomeAzienda').asString)) = 0 then DateEdit1.date := QMotiviRichiamoDataCalc.Value;
end;

end.

