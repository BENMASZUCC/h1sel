unit VideoSegreto;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  OleCtrls, SHDocVw;

type
  TVideoSegretoForm = class(TForm)
    WebBrowser1: TWebBrowser;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    xUrl : string
  end;

var
  VideoSegretoForm: TVideoSegretoForm;

implementation

{$R *.DFM}

procedure TVideoSegretoForm.FormShow(Sender: TObject);
begin
WebBrowser1.Navigate(xUrl);
end;

end.
