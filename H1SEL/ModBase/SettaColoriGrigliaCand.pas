unit SettaColoriGrigliaCand;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, ExtCtrls, Db, ADODB, dxCntner, dxTL, dxDBCtrl,
     dxDBGrid, dxDBTLCl, dxGrClms, TB97;

type
     TSettaColoriGrigliaCandForm = class(TForm)
          ColorDialog1: TColorDialog;
          dxDBGrid1: TdxDBGrid;
          DsColori: TDataSource;
          QColori: TADOQuery;
          QUsersLK: TADOQuery;
          Panel1: TPanel;
          BitBtn2: TBitBtn;
          QUsersLKID: TAutoIncField;
          QUsersLKNominativo: TStringField;
          QColoriID: TAutoIncField;
          QColoriIDUtente: TIntegerField;
          QColoriIDEvento: TIntegerField;
          QColoriColore: TIntegerField;
          QColoriNote: TMemoField;
          QColoriUtente: TStringField;
          QEventiLK: TADOQuery;
          QEventiLKID: TAutoIncField;
          QEventiLKEvento: TStringField;
          QColoriEvento: TStringField;
          dxDBGrid1ID: TdxDBGridMaskColumn;
          dxDBGrid1IDUtente: TdxDBGridMaskColumn;
          dxDBGrid1IDEvento: TdxDBGridMaskColumn;
          dxDBGrid1Colore: TdxDBGridMaskColumn;
          dxDBGrid1Note: TdxDBGridMemoColumn;
          dxDBGrid1Utente: TdxDBGridLookupColumn;
          dxDBGrid1Evento: TdxDBGridLookupColumn;
          BNew: TToolbarButton97;
          BDel: TToolbarButton97;
          BOK: TToolbarButton97;
          BCan: TToolbarButton97;
          ToolbarButton971: TToolbarButton97;
          procedure DsColoriStateChange(Sender: TObject);
          procedure BNewClick(Sender: TObject);
          procedure BDelClick(Sender: TObject);
          procedure BOKClick(Sender: TObject);
          procedure BCanClick(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure ToolbarButton971Click(Sender: TObject);
          procedure dxDBGrid1CustomDrawCell(Sender: TObject; ACanvas: TCanvas;
               ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
               ASelected, AFocused, ANewItemRow: Boolean; var AText: string;
               var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
               var ADone: Boolean);
          procedure QColoriAfterInsert(DataSet: TDataSet);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     SettaColoriGrigliaCandForm: TSettaColoriGrigliaCandForm;

implementation

uses ModuloDati, Main;

{$R *.DFM}

procedure TSettaColoriGrigliaCandForm.DsColoriStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsColori.State in [dsEdit, dsInsert];
     BNew.Enabled := not b;
     BDel.Enabled := not b;
     BOK.Enabled := b;
     BCan.Enabled := b;
end;

procedure TSettaColoriGrigliaCandForm.BNewClick(Sender: TObject);
begin
     QColori.Insert;
end;

procedure TSettaColoriGrigliaCandForm.BDelClick(Sender: TObject);
begin
     if MessageDlg('Sei sicuro di voler eliminare la riga selezionata ?', mtWarning, [mbNo, mbYes], 0) <> mrYes then exit;
     QColori.Delete;
end;

procedure TSettaColoriGrigliaCandForm.BOKClick(Sender: TObject);
begin
     QColori.Post;
end;

procedure TSettaColoriGrigliaCandForm.BCanClick(Sender: TObject);
begin
     QColori.Cancel;
end;

procedure TSettaColoriGrigliaCandForm.FormShow(Sender: TObject);
begin
     QColori.Open;
end;

procedure TSettaColoriGrigliaCandForm.ToolbarButton971Click(Sender: TObject);
begin
     if QColori.IsEmpty then exit;
     if dsColori.State in [dsInsert, dsEdit] then QColori.Post;
     if ColorDialog1.Execute then begin
          QColori.Edit;
          QColoriColore.value := ColorDialog1.Color;
          QColori.Post;
     end;
end;

procedure TSettaColoriGrigliaCandForm.dxDBGrid1CustomDrawCell(
     Sender: TObject; ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
     AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
     var AText: string; var AColor: TColor; AFont: TFont;
     var AAlignment: TAlignment; var ADone: Boolean);
begin
     if ANode.Values[dxDBGrid1Colore.index] <> null then
          AFont.Color := ANode.Values[dxDBGrid1Colore.index];
end;

procedure TSettaColoriGrigliaCandForm.QColoriAfterInsert(DataSet: TDataSet);
begin
     QColoriIDUtente.value := MainForm.xIDUtenteAttuale;
end;

end.
