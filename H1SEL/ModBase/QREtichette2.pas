{ QuickReport master detail template }

unit QREtichette2;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Qrctrls, quickrpt, DB, DBTables, ExtCtrls, ADODB;

type
     TQREtichette2Form = class(TQuickRep)
          DetailBand1: TQRBand;
          QRLabel5: TQRLabel;
          QEtichette: TADOQuery;
          QRDBText1: TQRDBText;
          QEtichetteID: TAutoIncField;
          QEtichetteCliente: TStringField;
          QEtichetteIndirizzo1: TStringField;
          QEtichetteIndirizzo2: TStringField;
          QEtichetteIndirizzo3: TStringField;
          QEtichetteContatto: TStringField;
          QEtichetteNote: TStringField;
          QRDBText2: TQRDBText;
          QRDBText3: TQRDBText;
          QRDBText4: TQRDBText;
          QRDBText5: TQRDBText;
          QRLabel1: TQRLabel;
          QRDBNote: TQRDBText;
          procedure QRDBNotePrint(sender: TObject; var Value: string);
     private
          CurLbl, NumCopie: integer;
     public
          xStampaNote: boolean;
     end;

var
     QREtichette2Form: TQREtichette2Form;

implementation

uses RicClienti, ModuloDati;

{$R *.DFM}

procedure TQREtichette2Form.QRDBNotePrint(sender: TObject; var Value: string);
begin
     if xStampaNote then value := QEtichetteNote.Value
     else value := '';
end;

end.
