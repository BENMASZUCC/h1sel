unit uModelliWord_remote;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, ShellAPI, ModuloDati,
     StdCtrls, Buttons, ExtCtrls, ComObj, IdComponent, IdBaseComponent, IdMessage, FileCtrl, EmailMessage, Main, Splash3,
     Registry, StrUtils, ADODB, JclFileUtils, uSendMailMapi, JclUnicode;

function GetDocPath: string;
function GetCartellaTemp_remota: string;
function CreaModelloWord_remoto(xIDModello: integer): string;
function ApriModelloWord_remoto(xIDModello: integer): string;
function RiempiModelloWord_remoto(xIDModello, xID: integer): string;
function CreaFilePresentaz(xModelloWord, xFileWord, xCliente, xPosizione, xIDAnag: string; xIDCandRic: integer; xAllegaFile: boolean): string;
function CreaFilePresentazPROPOSTE(xModelloWord, xFileWord, xCliente, xPosizione, xIDAnag: string; xIDMansione, xIDCandRic: integer; xAllegaFile: boolean): string;
function RiempiModelloGenerico_remoto(xQuale, xFileModello, xFileWord, xXMLDati: string; xFileAdd: string = ''): string;

implementation

// ------------- funzioni per "tunnel" TS -------------------------------------------------------------

function TS_ShellExecute(hwnd: integer; lpOperation, lpFile, lpParameters, lpDirectory: widestring; nShowCd: integer): integer; cdecl; external 'H1TS_Server.dll';

function TS_UserTemp(lpbuffer: PWideChar): integer; cdecl; external 'H1TS_Server.dll';

function GetCartellaTemp_remota: string;
var ps: PWideChar;
     res: integer;
     xarray: array[1..1024] of WideChar;
begin
     // cartella temporanea
     ps := @xarray;
     res := TS_UserTemp(ps);
     Result := WideCharToString(ps);
end;

function CreaModelloWord_remoto(xIDModello: integer): string;
var xTempFilePath, xComando_ws, xParameters_ws, xCartellaTemp, xCartellaTempTS, xs, xFile, xFileName, xBody, xAttachments, xFormato, xDest, xBcc: string;
     res, i, j: integer;
     xErrorMsg: string;
     CopyRes: boolean;
     xLog, xFiles, xXML: TStringList;
     xarray: array[1..1024] of WideChar;
     ps, xReg_Chiave, xReg_Path, xReg_Res: PWideChar;
     reg: TRegistry;
     regpostastring, key, find: string;
     Q1: TADOQuery;
     xModelloWord, xCampiXML: string;
     xModTone: boolean;
begin
     xTempFilePath := data.Global.fieldbyname('DirFileLog').asstring;

     xLog := TStringList.create;

     // cartella temporanea
     ps := @xarray;
     res := TS_UserTemp(ps);
     xCartellaTemp := WideCharToString(ps);
     xLog.Add('Risultato chiamata TS_UserTemp: ' + xCartellaTemp); xLog.SaveToFile(xTempFilePath + '\Log_CreaModelloWord_remoto.txt');

     xCartellaTempTS := '\\tsclient\' + StringReplace(xCartellaTemp, ':', '', []);
     if not DirectoryExists(xCartellaTempTS) then begin
          xLog.Add('Cartella temp remota (client) NON ESISTE: ' + xCartellaTempTS); xLog.SaveToFile(xTempFilePath + '\Log_CreaModelloWord_remoto.txt')
     end else
          xLog.Add('Cartella temp remota (client): ' + xCartellaTempTS); xLog.SaveToFile(xTempFilePath + '\Log_CreaModelloWord_remoto.txt');

     Q1 := TADOQuery.create(nil);
     Q1.ConnectionString := Data.DB.ConnectionString;
     Q1.SQL.Text := 'select * from ModelliWord where ID=' + IntToStr(xIDModello);
     Q1.Open;
     xModelloWord := Q1.FieldByName('NomeModello').asString;

     Q1.close;
     Q1.sql.Clear;
     Q1.SQL.Text := 'select * from ModelliWordCampi where IDModello=' + IntToStr(xIDModello);
     Q1.Open;
     xModTone := False;
     if Q1.IsEmpty then begin
          Q1.close;
          Q1.sql.Clear;
          Q1.SQL.Text := 'select tabellare, etichetta from modellidefinizioni md join vistecampi vc on md.idcampo=vc.id where idmodello=' + IntToStr(xIDModello);
          Q1.Open;
          xModTone := True;
     end;

     Q1.First;
     //xCampiXML := '<campi>' + chr(13);
     while not Q1.EOF do begin
          if xModTone then
               xCampiXML := xCampiXML + '    <campo etichetta="' + Q1.FieldByName('etichetta').asString + '" tabellare="' + Q1.FieldByName('tabellare').asString + '"/>' + chr(13)
          else xCampiXML := xCampiXML + '    <campo etichetta="' + Q1.FieldByName('NomeCampo').asString + '" tabellare="0"/>' + chr(13);
          Q1.Next;
     end;
     //xCampiXML := xCampiXML + '</campi>' + chr(13);
     Q1.close;

     // creazione file XML
     xXML := TStringList.create;
     xXML.Text := '<?xml version="1.0" encoding="ISO-8859-1"?> ' + chr(13) +
          '<operazioni> ' + chr(13) +
          '  <operazione nome="CreaModelloWord" nomefile="' + xModelloWord + '"> ' + chr(13) +
          xCampiXML +
          '  </operazione> ' + chr(13) +
          '</operazioni> ';

     // Salvataggio file XML sul client
     xXML.SaveToFile(xCartellaTempTS + 'h1client.xml');

     xLog.Add('Copiato file "h1client.xml" sul client.  XML: ' + chr(13) + xXML.text);
     xLog.SaveToFile(xTempFilePath + '\Log_CreaModelloWord_remoto.txt');

     xComando_ws := xCartellaTemp + '\H1Client.exe';
     xParameters_ws := xCartellaTemp + 'h1client.xml';

     // controllo esistenza H1Client.exe
     if not FileExists(xCartellaTempTS + 'h1client.exe') then begin
          if FileExists(ExtractFileDir(Application.ExeName) + '\H1Client.exe') then begin
               CopyRes := CopyFile(pchar(ExtractFileDir(Application.ExeName) + '\H1Client.exe'), pchar(xCartellaTempTS + 'H1Client.exe'), false);
               if CopyRes then begin
                    xLog.Add('Non esiste H1Client.exe sul client: copiato dal server'); xLog.SaveToFile(xTempFilePath + '\Log_RiempiModelloWord_remoto.txt');
               end else begin
                    xLog.Add('Non esiste H1Client.exe sul client: copia dal server NON riuscita'); xLog.SaveToFile(xTempFilePath + '\Log_RiempiModelloWord_remoto.txt');
               end;
          end else begin
               MessageDlg('Il file "H1Client.exe" non esiste n� sul client n� sul server' + chr(13) + 'IMPOSSIBILE CONTINUARE', mtError, [mbOK], 0);
               xLog.Add('Non esiste H1Client.exe sul client, ma nemmeno sul server !'); xLog.SaveToFile(xTempFilePath + '\Log_RiempiModelloWord_remoto.txt');
               exit;
          end;
     end;

     try
          xLog.Add('Pronto alla chiamata: ' + chr(13) + '  Comando = ' + xComando_ws + chr(13) + '  Parametri: ' + xParameters_ws + chr(13)); xLog.SaveToFile(xTempFilePath + '\Log_CreaModelloWord_remoto.txt');

          res := TS_ShellExecute(0, 'Open', xComando_ws, xParameters_ws, '', SW_HIDE);

     finally
          // cancellazione file sul client
          // >>> NON FUNZIONA, perch� lo cancella prima che il client predisponga il messaggio
          //  i file vengono cancellati all'uscita dal programma (alla fine di MainForm.OnClose)

     end;

     case res of
          0: xErrorMsg := '0 = The operating system is out of memory or resources.';
          1: xErrorMsg := '1 = Non riesco ad aprire il virtual channel (TS_ShellExecute)';
          //2: xErrorMsg := '2 = The specified file was not found';
          2: xErrorMsg := '2 = il virtual channel si chiude prima di inviare i dati (TS_ShellExecute)';
          //3: xErrorMsg := '3 = The specified path was not found.';
          3: xErrorMsg := '3 = fallito l''invio dei dati al client (TS_ShellExecute)';

          5: xErrorMsg := '5 = Windows 95 only: The operating system denied access to the specified file';
          8: xErrorMsg := '8 = Windows 95 only: There was not enough memory to complete the operation.';
          10: xErrorMsg := '10 = Wrong Windows version';
          11: xErrorMsg := '11 = The.EXE file is invalid(non - Win32.EXE or error in .EXE image).';
          12: xErrorMsg := '12 = Application was designed for a different operating system.';
          13: xErrorMsg := '13 = Application was designed for MS - DOS 4.0';
          15: xErrorMsg := '15 = Attempt to load a real - mode program';
          16: xErrorMsg := '16 = Attempt to load a second instance of an application with non - readonly data segments';
          19: xErrorMsg := '19 = Attempt to load a compressed application file';
          20: xErrorMsg := '20 = Dynamic - link library(DLL)file failure".';
          26: xErrorMsg := '26 = A sharing violation occurred.';
          27: xErrorMsg := '27 = The filename association is incomplete or invalid.';
          28: xErrorMsg := '28 = The DDE transaction could not be completed because the request timed out.';
          29: xErrorMsg := '29 = The DDE transaction failed.';
          30: xErrorMsg := '30 = The DDE transaction could not be completed because other DDE transactions were being processed.';
          31: xErrorMsg := '31 = There is no application associated with the given filename extension.';
          32: xErrorMsg := '32 = Windows 95 only: The specified dynamic - link library was not found.';
     else xErrorMsg := IntToStr(res) + ' = (errore non documentato)';
     end;
     if (res < 0) or (res > 32) then
          xErrorMsg := 'OK (' + inttostr(res) + ')';

     // CODICI ERRORE COMUNICATI DA STEFANO ma NON gestiti:
     // 1: non riesce ad aprire il virtual channel
     // 2: il virtual channel si chiude prima di inviare i dati
     // 3: fallisce l'invio dei dai al client
     // 0: la ShellExecute fallisce (quindi la comunicazione � corretta ma c'� qualcosa che non va nel passaggio dei parametri)

     xLog.Add('Chiamata effettuata con risultato: ' + xErrorMsg); xLog.SaveToFile(xTempFilePath + '\Log_CreaModelloWord_remoto.txt');

     if copy(xErrorMsg, 1, 2) <> 'OK' then
          MessageDlg('ERRORE: ' + xErrorMsg, mtError, [mbOK], 0);

     // salvataggio modello
     ShowMessage('Modificare il modello sul client e poi premere OK');

     CopyRes := CopyFile(pchar(xCartellaTempTS + xModelloWord + '.doc'), pchar(GetDocPath + '\' + xModelloWord + '.doc'), false);
     if CopyRes then begin
          xLog.Add('Copia file dal client: ' + xCartellaTempTS + xModelloWord + '.doc > ' + GetDocPath + '\' + xModelloWord + '.doc > OK'); xLog.SaveToFile(xTempFilePath + '\Log_CreaModelloWord_remoto.txt');
     end else begin
          xLog.Add('Copia file dal client: ' + xCartellaTempTS + xModelloWord + '.doc > ' + GetDocPath + '\' + xModelloWord + '.doc > ERRORE'); xLog.SaveToFile(xTempFilePath + '\Log_CreaModelloWord_remoto.txt');
     end;

     // cancellazione file temporaneo sul client: aggiunto alla lista per la cancellazione all'uscita di H1
     MainForm.xFileTempClient.Add(xCartellaTempTS + xModelloWord + '.doc');
end;

function ApriModelloWord_remoto(xIDModello: integer): string;
var xTempFilePath, xComando_ws, xParameters_ws, xCartellaTemp, xCartellaTempTS, xs, xFile, xFileName, xBody, xAttachments, xFormato, xDest, xBcc: string;
     res, i, j: integer;
     xErrorMsg: string;
     CopyRes: boolean;
     xLog, xFiles, xXML: TStringList;
     xarray: array[1..1024] of WideChar;
     ps, xReg_Chiave, xReg_Path, xReg_Res: PWideChar;
     reg: TRegistry;
     regpostastring, key, find: string;
     Q1: TADOQuery;
     xModelloWord, xCampiXML: string;
     xModTone: boolean;
begin
     xTempFilePath := data.Global.fieldbyname('DirFileLog').asstring;

     xLog := TStringList.create;

     // cartella temporanea
     ps := @xarray;
     res := TS_UserTemp(ps);
     xCartellaTemp := WideCharToString(ps);
     xLog.Add('Risultato chiamata TS_UserTemp: ' + xCartellaTemp); xLog.SaveToFile(xTempFilePath + '\Log_ApriModelloWord_remoto.txt');

     xCartellaTempTS := '\\tsclient\' + StringReplace(xCartellaTemp, ':', '', []);
     if not DirectoryExists(xCartellaTempTS) then begin
          xLog.Add('Cartella temp remota (client) NON ESISTE: ' + xCartellaTempTS); xLog.SaveToFile(xTempFilePath + '\Log_ApriModelloWord_remoto.txt')
     end else
          xLog.Add('Cartella temp remota (client): ' + xCartellaTempTS); xLog.SaveToFile(xTempFilePath + '\Log_ApriModelloWord_remoto.txt');

     Q1 := TADOQuery.create(nil);
     Q1.ConnectionString := Data.DB.ConnectionString;
     Q1.SQL.Text := 'select * from ModelliWord where ID=' + IntToStr(xIDModello);
     Q1.Open;
     xModelloWord := Q1.FieldByName('NomeModello').asString;

     // controllo esistenza modello
     if not FileExists(GetDocPath + '\' + xModelloWord + '.doc') then begin
          MessageDlg('Modello non trovato. File:' + chr(13) + GetDocPath + '\' + xModelloWord + '.doc', mtError, [mbOK], 0);
          Q1.Close;
          xLog.Add('Modello non trovato: ' + GetDocPath + '\' + xModelloWord + '.doc'); xLog.SaveToFile(xTempFilePath + '\Log_RiempiModelloWord_remoto.txt');
          exit;
     end;

     // Copia file doc sul client
     CopyRes := CopyFile(pchar(GetDocPath + '\' + xModelloWord + '.doc'), pchar(xCartellaTempTS + xModelloWord + '.doc'), false);
     if CopyRes then begin
          xLog.Add('Copia file sul client: ' + GetDocPath + '\' + xModelloWord + '.doc > ' + xCartellaTempTS + xModelloWord + '.doc > OK'); xLog.SaveToFile(xTempFilePath + '\Log_ApriModelloWord_remoto.txt');
     end else begin
          xLog.Add('Copia file sul client: ' + GetDocPath + '\' + xModelloWord + '.doc > ' + xCartellaTempTS + xModelloWord + '.doc > ERRORE'); xLog.SaveToFile(xTempFilePath + '\Log_ApriModelloWord_remoto.txt');
     end;

     // creazione file XML
     xXML := TStringList.create;
     xXML.Text := '<?xml version="1.0" encoding="ISO-8859-1"?> ' + chr(13) +
          '<operazioni> ' + chr(13) +
          '  <operazione nome="ApriModelloWord" nomefile="' + xModelloWord + '"> ' + chr(13) +
          '  </operazione> ' + chr(13) +
          '</operazioni> ';

     // Salvataggio file XML sul client
     xXML.SaveToFile(xCartellaTempTS + 'h1client.xml');
     xLog.Add('Copiato file "h1client.xml" sul client.  XML: ' + chr(13) + xXML.text);
     xLog.SaveToFile(xTempFilePath + '\Log_ApriModelloWord_remoto.txt');

     xComando_ws := xCartellaTemp + '\H1Client.exe';
     xParameters_ws := xCartellaTemp + 'h1client.xml';

     // controllo esistenza H1Client.exe
     if not FileExists(xCartellaTempTS + 'h1client.exe') then begin
          if FileExists(ExtractFileDir(Application.ExeName) + '\H1Client.exe') then begin
               CopyRes := CopyFile(pchar(ExtractFileDir(Application.ExeName) + '\H1Client.exe'), pchar(xCartellaTempTS + 'H1Client.exe'), false);
               if CopyRes then begin
                    xLog.Add('Non esiste H1Client.exe sul client: copiato dal server'); xLog.SaveToFile(xTempFilePath + '\Log_RiempiModelloWord_remoto.txt');
               end else begin
                    xLog.Add('Non esiste H1Client.exe sul client: copia dal server NON riuscita'); xLog.SaveToFile(xTempFilePath + '\Log_RiempiModelloWord_remoto.txt');
               end;
          end else begin
               MessageDlg('Il file "H1Client.exe" non esiste n� sul client n� sul server' + chr(13) + 'IMPOSSIBILE CONTINUARE', mtError, [mbOK], 0);
               xLog.Add('Non esiste H1Client.exe sul client, ma nemmeno sul server !'); xLog.SaveToFile(xTempFilePath + '\Log_RiempiModelloWord_remoto.txt');
               exit;
          end;
     end;

     try
          xLog.Add('Pronto alla chiamata: ' + chr(13) + '  Comando = ' + xComando_ws + chr(13) + '  Parametri: ' + xParameters_ws + chr(13)); xLog.SaveToFile(xTempFilePath + '\Log_ApriModelloWord_remoto.txt');

          res := TS_ShellExecute(0, 'Open', xComando_ws, xParameters_ws, '', SW_HIDE);

     finally
          // cancellazione file sul client
          // >>> NON FUNZIONA, perch� lo cancella prima che il client predisponga il messaggio
          //  i file vengono cancellati all'uscita dal programma (alla fine di MainForm.OnClose)

     end;

     case res of
          0: xErrorMsg := '0 = The operating system is out of memory or resources.';
          1: xErrorMsg := '1 = Non riesco ad aprire il virtual channel (TS_ShellExecute)';
          //2: xErrorMsg := '2 = The specified file was not found';
          2: xErrorMsg := '2 = il virtual channel si chiude prima di inviare i dati (TS_ShellExecute)';
          //3: xErrorMsg := '3 = The specified path was not found.';
          3: xErrorMsg := '3 = fallito l''invio dei dati al client (TS_ShellExecute)';

          5: xErrorMsg := '5 = Windows 95 only: The operating system denied access to the specified file';
          8: xErrorMsg := '8 = Windows 95 only: There was not enough memory to complete the operation.';
          10: xErrorMsg := '10 = Wrong Windows version';
          11: xErrorMsg := '11 = The.EXE file is invalid(non - Win32.EXE or error in .EXE image).';
          12: xErrorMsg := '12 = Application was designed for a different operating system.';
          13: xErrorMsg := '13 = Application was designed for MS - DOS 4.0';
          15: xErrorMsg := '15 = Attempt to load a real - mode program';
          16: xErrorMsg := '16 = Attempt to load a second instance of an application with non - readonly data segments';
          19: xErrorMsg := '19 = Attempt to load a compressed application file';
          20: xErrorMsg := '20 = Dynamic - link library(DLL)file failure".';
          26: xErrorMsg := '26 = A sharing violation occurred.';
          27: xErrorMsg := '27 = The filename association is incomplete or invalid.';
          28: xErrorMsg := '28 = The DDE transaction could not be completed because the request timed out.';
          29: xErrorMsg := '29 = The DDE transaction failed.';
          30: xErrorMsg := '30 = The DDE transaction could not be completed because other DDE transactions were being processed.';
          31: xErrorMsg := '31 = There is no application associated with the given filename extension.';
          32: xErrorMsg := '32 = Windows 95 only: The specified dynamic - link library was not found.';
     else xErrorMsg := IntToStr(res) + ' = (errore non documentato)';
     end;
     if (res < 0) or (res > 32) then
          xErrorMsg := 'OK (' + inttostr(res) + ')';

     // CODICI ERRORE COMUNICATI DA STEFANO ma NON gestiti:
     // 1: non riesce ad aprire il virtual channel
     // 2: il virtual channel si chiude prima di inviare i dati
     // 3: fallisce l'invio dei dai al client
     // 0: la ShellExecute fallisce (quindi la comunicazione � corretta ma c'� qualcosa che non va nel passaggio dei parametri)

     xLog.Add('Chiamata effettuata con risultato: ' + xErrorMsg); xLog.SaveToFile(xTempFilePath + '\Log_ApriModelloWord_remoto.txt');

     if copy(xErrorMsg, 1, 2) <> 'OK' then
          MessageDlg('ERRORE: ' + xErrorMsg, mtError, [mbOK], 0);

     // salvataggio modello
     ShowMessage('Modificare il modello sul client e poi premere OK');

     CopyRes := CopyFile(pchar(xCartellaTempTS + xModelloWord + '.doc'), pchar(GetDocPath + '\' + xModelloWord + '.doc'), false);
     if CopyRes then begin
          xLog.Add('Copia file dal client: ' + xCartellaTempTS + xModelloWord + '.doc > ' + GetDocPath + '\' + xModelloWord + '.doc > OK'); xLog.SaveToFile(xTempFilePath + '\Log_ApriModelloWord_remoto.txt');
     end else begin
          xLog.Add('Copia file dal client: ' + xCartellaTempTS + xModelloWord + '.doc > ' + GetDocPath + '\' + xModelloWord + '.doc > ERRORE'); xLog.SaveToFile(xTempFilePath + '\Log_ApriModelloWord_remoto.txt');
     end;

     // cancellazione file temporaneo sul client: aggiunto alla lista per la cancellazione all'uscita di H1
     MainForm.xFileTempClient.Add(xCartellaTempTS + xModelloWord + '.doc');
end;

function RiempiModelloWord_remoto(xIDModello, xID: integer): string;
var xTempFilePath, xComando_ws, xParameters_ws, xCartellaTemp, xCartellaTempTS, xs, xFile, xFileName, xBody, xAttachments, xFormato, xDest, xBcc: string;
     res, i, j, k: integer;
     xErrorMsg: string;
     CopyRes: boolean;
     xLog, xFiles, xXML: TStringList;
     xarray: array[1..1024] of WideChar;
     ps, xReg_Chiave, xReg_Path, xReg_Res: PWideChar;
     reg: TRegistry;
     regpostastring, key, find: string;
     Q1, Q2, QVal: TADOQuery;
     xModelloWord, xCampiXML, xValore, xFileWord: string;
     xModTone: boolean;
     xAnno, xMese, xGiorno: Word;
begin
     xTempFilePath := data.Global.fieldbyname('DirFileLog').asstring;

     xLog := TStringList.create;

     // cartella temporanea
     ps := @xarray;
     res := TS_UserTemp(ps);
     xCartellaTemp := WideCharToString(ps);
     xLog.Add('Risultato chiamata TS_UserTemp: ' + xCartellaTemp); xLog.SaveToFile(xTempFilePath + '\Log_RiempiModelloWord_remoto.txt');

     xCartellaTempTS := '\\tsclient\' + StringReplace(xCartellaTemp, ':', '', []);
     if not DirectoryExists(xCartellaTempTS) then begin
          xLog.Add('Cartella temp remota (client) NON ESISTE: ' + xCartellaTempTS); xLog.SaveToFile(xTempFilePath + '\Log_RiempiModelloWord_remoto.txt')
     end else
          xLog.Add('Cartella temp remota (client): ' + xCartellaTempTS); xLog.SaveToFile(xTempFilePath + '\Log_RiempiModelloWord_remoto.txt');

     Q1 := TADOQuery.create(nil);
     Q1.ConnectionString := Data.DB.ConnectionString;
     Q1.SQL.Text := 'select * from ModelliWord where ID=' + IntToStr(xIDModello);
     Q1.Open;
     xModelloWord := Q1.FieldByName('NomeModello').asString;
     DecodeDate(Date, xAnno, xMese, xGiorno);
     xFileWord := Q1.FieldByName('NomeModello').asString + IntToStr(xID) + '-' + IntToStr(xGiorno) + IntToStr(xMese) + '.doc';

     // controllo esistenza modello
     if not FileExists(GetDocPath + '\' + xModelloWord + '.doc') then begin
          MessageDlg('Modello non trovato. File:' + chr(13) + GetDocPath + '\' + xModelloWord + '.doc', mtError, [mbOK], 0);
          Q1.Close;
          xLog.Add('Modello non trovato: ' + GetDocPath + '\' + xModelloWord + '.doc'); xLog.SaveToFile(xTempFilePath + '\Log_RiempiModelloWord_remoto.txt');
          exit;
     end;

     // Copia modello sul client
     CopyRes := CopyFile(pchar(GetDocPath + '\' + xModelloWord + '.doc'), pchar(xCartellaTempTS + xModelloWord + '.doc'), false);
     if CopyRes then begin
          xLog.Add('Copia file sul client: ' + GetDocPath + '\' + xModelloWord + '.doc > ' + xCartellaTempTS + xModelloWord + '.doc > OK'); xLog.SaveToFile(xTempFilePath + '\Log_RiempiModelloWord_remoto.txt');
     end else begin
          xLog.Add('Copia file sul client: ' + GetDocPath + '\' + xModelloWord + '.doc > ' + xCartellaTempTS + xModelloWord + '.doc > ERRORE'); xLog.SaveToFile(xTempFilePath + '\Log_RiempiModelloWord_remoto.txt');
     end;

     Q1.close;
     Q1.sql.Clear;
     Q1.SQL.Text := 'select M.TabellaMaster, C.Campo, C.NomeCampo ' +
          'from ModelliWord M ' +
          'join ModelliWordCampi C on M.ID = C.IDMOdello ' +
          'where M.ID=' + IntToStr(xIDModello);
     Q1.Open;

     xModTone := False;
     if Q1.IsEmpty then begin
          xModTone := True;
          xLog.Add('Q1.SQL.text = ' + chr(13) + Q1.sql.text + chr(13) + '>> Numerorecord = ' + IntToStr(Q1.RecordCount) + chr(13) + 'Modalit� Tone/avanzata'); xLog.SaveToFile(xTempFilePath + '\Log_RiempiModelloWord_remoto.txt');
     end else
          xLog.Add('Q1.SQL.text = ' + chr(13) + Q1.sql.text + chr(13) + '>> Numerorecord = ' + IntToStr(Q1.RecordCount) + chr(13) + 'Modalit� base'); xLog.SaveToFile(xTempFilePath + '\Log_RiempiModelloWord_remoto.txt');

     QVal := TADOQuery.create(nil);
     QVal.ConnectionString := Data.DB.ConnectionString;

     if xModTone then begin
          // MODALITA' AVANZATA (Tone)
          Q2 := TADOQuery.create(nil);
          Q2.ConnectionString := Data.DB.ConnectionString;
          Q2.close;
          Q2.sql.Clear;
          Q2.SQL.Text := 'select ''select ''+vc.Campo+'' from ''+DefinizioneVista+'' where ''+vcfk.campo+'' = ' + IntToStr(xID) + '; '' Query ' +
               ',vc.Etichetta,Tabellare, idparent ' +
               'from modellidefinizioni md ' +
               'join vistecampi vc on md.idcampo=vc.id ' +
               'join vistedefinizioni vd on vc.idtabella = vd.id ' +
               'join visteassociazioni va on vd.id = va.idtab ' +
               'join vistecampi vcFK on va.idcampofk=vcfk.id ' +
               'where IDModello = ' + IntToStr(xIDModello);
          Q2.Open;
          xLog.Add('Modalit� Tone - Q2.SQL.text = ' + chr(13) + Q2.sql.text + chr(13) + '>> Numerorecord = ' + IntToStr(Q2.RecordCount)); xLog.SaveToFile(xTempFilePath + '\Log_RiempiModelloWord_remoto.txt');
          while not Q2.eof do begin
               QVal.Close;
               QVal.SQL.clear;
               QVal.SQL.text := Q2.FieldByName('Query').asString;
               QVal.Open;
               xLog.Add('Apertura query: ' + chr(13) + QVal.SQL.text + chr(13) + '>> Numerorecord = ' + IntToStr(QVal.RecordCount)); xLog.SaveToFile(xTempFilePath + '\Log_RiempiModelloWord_remoto.txt');

               if QVal.RecordCount = 1 then
                    xValore := QVal.Fields[0].asString
               else begin
                    xValore := '';
                    while not QVal.EOF do begin
                         xValore := xValore + QVal.Fields[0].asString + ';';
                         QVal.Next;
                    end;
               end;

               xCampiXML := xCampiXML + '    <campo etichetta="' + Q2.FieldByName('etichetta').asString + '" tabellare="' + Q2.FieldByName('tabellare').asString + '" valore="' + xValore + '"/>' + chr(13);

               Q2.Next;
          end;
     end else begin
          // MODALITA' NORMALE
          Q1.First;
          while not Q1.EOF do begin
               // calcolo valore
               QVal.Close;
               QVal.SQL.clear;
               QVal.SQL.text := 'select ' + Q1.FieldByName('Campo').asString + ' from ' + Q1.FieldByName('TabellaMaster').asString + ' where ID=' + IntToStr(xID);
               QVal.Open;
               xCampiXML := xCampiXML + '    <campo etichetta="' + Q1.FieldByName('NomeCampo').asString + '" tabellare="0" valore="' + QVal.Fields[0].asString + '"/>' + chr(13);
               Q1.Next;
          end;
     end;
     QVal.Close;
     Q1.close;

     // creazione file XML
     xXML := TStringList.create;
     xXML.Text := '<?xml version="1.0" encoding="ISO-8859-1"?> ' + chr(13) +
          '<operazioni> ' + chr(13) +
          '  <operazione nome="RiempiModelloWord" nomefile="' + xModelloWord + '" fileword="' + xFileWord + '"> ' + chr(13) +
          xCampiXML +
          '  </operazione> ' + chr(13) +
          '</operazioni> ';

     // Salvataggio file XML sul client
     xXML.SaveToFile(xCartellaTempTS + 'h1client.xml');
     xLog.Add('Copiato file "h1client.xml" sul client.  XML: ' + chr(13) + xXML.text);
     xLog.SaveToFile(xTempFilePath + '\Log_RiempiModelloWord_remoto.txt');

     xComando_ws := xCartellaTemp + '\H1Client.exe';
     xParameters_ws := xCartellaTemp + 'h1client.xml';

     // controllo esistenza H1Client.exe
     if not FileExists(xCartellaTempTS + 'h1client.exe') then begin
          if FileExists(ExtractFileDir(Application.ExeName) + '\H1Client.exe') then begin
               CopyRes := CopyFile(pchar(ExtractFileDir(Application.ExeName) + '\H1Client.exe'), pchar(xCartellaTempTS + 'H1Client.exe'), false);
               if CopyRes then begin
                    xLog.Add('Non esiste H1Client.exe sul client: copiato dal server'); xLog.SaveToFile(xTempFilePath + '\Log_RiempiModelloWord_remoto.txt');
               end else begin
                    xLog.Add('Non esiste H1Client.exe sul client: copia dal server NON riuscita'); xLog.SaveToFile(xTempFilePath + '\Log_RiempiModelloWord_remoto.txt');
               end;
          end else begin
               MessageDlg('Il file "H1Client.exe" non esiste n� sul client n� sul server' + chr(13) + 'IMPOSSIBILE CONTINUARE', mtError, [mbOK], 0);
               xLog.Add('Non esiste H1Client.exe sul client, ma nemmeno sul server !'); xLog.SaveToFile(xTempFilePath + '\Log_RiempiModelloWord_remoto.txt');
               exit;
          end;
     end;

     try
          xLog.Add('Pronto alla chiamata: ' + chr(13) + '  Comando = ' + xComando_ws + chr(13) + '  Parametri: ' + xParameters_ws + chr(13)); xLog.SaveToFile(xTempFilePath + '\Log_RiempiModelloWord_remoto.txt');

          res := TS_ShellExecute(0, 'Open', xComando_ws, xParameters_ws, '', SW_HIDE);

     finally
          // cancellazione file sul client
          // >>> NON FUNZIONA, perch� lo cancella prima che il client predisponga il messaggio
          //  i file vengono cancellati all'uscita dal programma (alla fine di MainForm.OnClose)

     end;

     case res of
          0: xErrorMsg := '0 = The operating system is out of memory or resources.';
          1: xErrorMsg := '1 = Non riesco ad aprire il virtual channel (TS_ShellExecute)';
          //2: xErrorMsg := '2 = The specified file was not found';
          2: xErrorMsg := '2 = il virtual channel si chiude prima di inviare i dati (TS_ShellExecute)';
          //3: xErrorMsg := '3 = The specified path was not found.';
          3: xErrorMsg := '3 = fallito l''invio dei dati al client (TS_ShellExecute)';

          5: xErrorMsg := '5 = Windows 95 only: The operating system denied access to the specified file';
          8: xErrorMsg := '8 = Windows 95 only: There was not enough memory to complete the operation.';
          10: xErrorMsg := '10 = Wrong Windows version';
          11: xErrorMsg := '11 = The.EXE file is invalid(non - Win32.EXE or error in .EXE image).';
          12: xErrorMsg := '12 = Application was designed for a different operating system.';
          13: xErrorMsg := '13 = Application was designed for MS - DOS 4.0';
          15: xErrorMsg := '15 = Attempt to load a real - mode program';
          16: xErrorMsg := '16 = Attempt to load a second instance of an application with non - readonly data segments';
          19: xErrorMsg := '19 = Attempt to load a compressed application file';
          20: xErrorMsg := '20 = Dynamic - link library(DLL)file failure".';
          26: xErrorMsg := '26 = A sharing violation occurred.';
          27: xErrorMsg := '27 = The filename association is incomplete or invalid.';
          28: xErrorMsg := '28 = The DDE transaction could not be completed because the request timed out.';
          29: xErrorMsg := '29 = The DDE transaction failed.';
          30: xErrorMsg := '30 = The DDE transaction could not be completed because other DDE transactions were being processed.';
          31: xErrorMsg := '31 = There is no application associated with the given filename extension.';
          32: xErrorMsg := '32 = Windows 95 only: The specified dynamic - link library was not found.';
     else xErrorMsg := IntToStr(res) + ' = (errore non documentato)';
     end;
     if (res < 0) or (res > 32) then
          xErrorMsg := 'OK (' + inttostr(res) + ')';

     // CODICI ERRORE COMUNICATI DA STEFANO ma NON gestiti:
     // 1: non riesce ad aprire il virtual channel
     // 2: il virtual channel si chiude prima di inviare i dati
     // 3: fallisce l'invio dei dai al client
     // 0: la ShellExecute fallisce (quindi la comunicazione � corretta ma c'� qualcosa che non va nel passaggio dei parametri)

     xLog.Add('Chiamata effettuata con risultato: ' + xErrorMsg); xLog.SaveToFile(xTempFilePath + '\Log_RiempiModelloWord_remoto.txt');

     if copy(xErrorMsg, 1, 2) <> 'OK' then
          MessageDlg('ERRORE: ' + xErrorMsg, mtError, [mbOK], 0);

     // salvataggio modello
     ShowMessage('Modificare il file sul client e poi premere OK');

     // copia del file Word dal client
     CopyRes := CopyFile(pchar(xCartellaTempTS + xFileWord), pchar(GetDocPath + '\' + xFileWord), false);
     if CopyRes then begin
          xLog.Add('Copia file dal client: ' + xCartellaTempTS + xFileWord + ' > ' + GetDocPath + '\' + xFileWord + ' > OK'); xLog.SaveToFile(xTempFilePath + '\Log_RiempiModelloWord_remoto.txt');
          Result := GetDocPath + '\' + xFileWord;
     end else begin
          xLog.Add('Copia file dal client: ' + xCartellaTempTS + xFileWord + ' > ' + GetDocPath + '\' + xFileWord + ' > ERRORE'); xLog.SaveToFile(xTempFilePath + '\Log_RiempiModelloWord_remoto.txt');
          Result := '';
     end;

     // cancellazione file temporaneo sul client: aggiunto alla lista per la cancellazione all'uscita di H1
     MainForm.xFileTempClient.Add(xCartellaTempTS + xModelloWord + '.doc');
     MainForm.xFileTempClient.Add(xCartellaTempTS + xFileWord);
end;

function CreaFilePresentaz(xModelloWord, xFileWord, xCliente, xPosizione, xIDAnag: string; xIDCandRic: integer; xAllegaFile: boolean): string;
var xTempFilePath, xComando_ws, xParameters_ws, xCartellaTemp, xCartellaTempTS, xs, xFile, xFileName, xBody, xAttachments, xFormato, xDest, xBcc: string;
     res, i, j, k, xCRpos, xIDAnagFile: integer;
     xErrorMsg: string;
     CopyRes, xApriSolo: boolean;
     xLog, xFiles, xXML: TStringList;
     xarray: array[1..1024] of WideChar;
     ps, xReg_Chiave, xReg_Path, xReg_Res: PWideChar;
     reg: TRegistry;
     regpostastring, key, find: string;
     Q1, Q2, QVal: TADOQuery;
     xCampiXML, xValore, xInquadramento, xRetribuzione: string;
     xModTone: boolean;
     xAnno, xMese, xGiorno: Word;
begin
     xTempFilePath := data.Global.fieldbyname('DirFileLog').asstring;

     xLog := TStringList.create;

     // cartella temporanea
     ps := @xarray;
     res := TS_UserTemp(ps);
     xCartellaTemp := WideCharToString(ps);
     xLog.Add('Risultato chiamata TS_UserTemp: ' + xCartellaTemp); xLog.SaveToFile(xTempFilePath + '\Log_CreaFilePresentaz_remoto.txt');

     xCartellaTempTS := '\\tsclient\' + StringReplace(xCartellaTemp, ':', '', []);
     if not DirectoryExists(xCartellaTempTS) then begin
          xLog.Add('Cartella temp remota (client) NON ESISTE: ' + xCartellaTempTS); xLog.SaveToFile(xTempFilePath + '\Log_CreaFilePresentaz_remoto.txt')
     end else
          xLog.Add('Cartella temp remota (client): ' + xCartellaTempTS); xLog.SaveToFile(xTempFilePath + '\Log_CreaFilePresentaz_remoto.txt');

     Q1 := TADOQuery.create(nil);
     Q1.ConnectionString := Data.DB.ConnectionString;

     // verifica esistenza file
     xApriSolo := False;
     xIDAnagFile := 0;
     if data.Global.FieldByName('AnagFileDentroDB').AsBoolean then begin
          xFileWord := PathGetTempPath + ExtractFileName(xFileWord);
          Q1.Close;
          Q1.SQL.clear;
          Q1.SQL.text := 'select id, nomefile from anagfile where nomefile =:xnomefile';
          Q1.Parameters[0].Value := ExtractFileName(xFileWord) + '.doc';
          xLog.Add('Verifica esistenza file (nel DB): ' + ExtractFileName(xFileWord) + '.doc'); xLog.SaveToFile(xTempFilePath + '\Log_CreaFilePresentaz_remoto.txt');
          Q1.Open;
          if Q1.RecordCount > 0 then begin
               //if MessageDlg('Il file esiste gi� - Aprirlo (Yes) o crearne uno nuovo, riscrivendo il vecchio (No) ?', mtWarning, [mbYes, mbNo], 0) = mrYes then begin
               //     xIDAnagFile := Q1.FieldByName('ID').asInteger;
               //     xApriSolo := True;
               //end;
               MessageDlg('Il file esiste gi� (nel database) - Impossibile proseguire', mtWarning, [mbOK], 0);
               Result := 'ABORT';
               Exit;
          end;
     end else begin
          if FileExists(xFileWord) then begin
               //if MessageDlg('Il file esiste gi� - Aprirlo (Yes) o crearne uno nuovo, riscrivendo il vecchio (No) ?', mtWarning, [mbYes, mbNo], 0) = mrYes then begin
               //     xApriSolo := True;
               //end;
               MessageDlg('Il file esiste gi� (nel file system) - Impossibile proseguire', mtWarning, [mbOK], 0);
               Result := 'ABORT';
               Exit;
          end;
     end;

     if xApriSolo then begin

          // #### RIVEDERE ############

          //if xIDAnagFile > 0 then begin
          //     aprifiledue(xIDAnagFile, 'main');
          //end else
          //     CopiaApriFileSulClient(xFileWord + '.doc');

     end else begin

          // Copia modello sul client
          CopyRes := CopyFile(pchar(GetDocPath + '\' + xModelloWord + '.doc'), pchar(xCartellaTempTS + xModelloWord + '.doc'), false);
          if CopyRes then begin
               xLog.Add('Copia file sul client: ' + GetDocPath + '\' + xModelloWord + '.doc > ' + xCartellaTempTS + xModelloWord + '.doc > OK'); xLog.SaveToFile(xTempFilePath + '\Log_CreaFilePresentaz_remoto.txt');
          end else begin
               xLog.Add('Copia file sul client: ' + GetDocPath + '\' + xModelloWord + '.doc > ' + xCartellaTempTS + xModelloWord + '.doc > ERRORE'); xLog.SaveToFile(xTempFilePath + '\Log_CreaFilePresentaz_remoto.txt');
          end;

          xCampiXML := xCampiXML + '    <campo etichetta="#cliente" tabellare="0" valore="' + xCliente + '"/>' + chr(13);
          xCampiXML := xCampiXML + '    <campo etichetta="#posizione" tabellare="0" valore="' + xPosizione + '"/>' + chr(13);

          // dati anagrafici
          Q1.close;
          Q1.sql.Clear;
          Q1.SQL.Text := 'select SC.Descrizione StatoCivile, A.* from Anagrafica A join StatiCivili SC on A.IDStatoCivile = SC.ID where A.ID=' + xIDAnag;
          Q1.Open;
          xCampiXML := xCampiXML + '    <campo etichetta="#soggetto" tabellare="0" valore="' + Q1.FieldByName('Cognome').asString + ' ' + Q1.FieldByName('Nome').asString + '"/>' + chr(13);
          xCampiXML := xCampiXML + '    <campo etichetta="#cliente2" tabellare="0" valore="' + xCliente + '"/>' + chr(13);
          xCampiXML := xCampiXML + '    <campo etichetta="#Dataoggi" tabellare="0" valore="' + DateToStr(Date) + '"/>' + chr(13);
          xCampiXML := xCampiXML + '    <campo etichetta="#LuogoDataNasc" tabellare="0" valore="' + Q1.FieldByName('LuogoNascita').AsString + ', ' + Q1.FieldByName('DataNascita').asString + '"/>' + chr(13);
          xCampiXML := xCampiXML + '    <campo etichetta="#statocivile" tabellare="0" valore="' + Q1.FieldByName('StatoCivile').asString + ' ' + Q1.FieldByName('Nome').asString + '"/>' + chr(13);
          xCampiXML := xCampiXML + '    <campo etichetta="#indirizzo" tabellare="0" valore="' + Q1.FieldByName('Indirizzo').asString + ' ' + Q1.FieldByName('Nome').asString + '"/>' + chr(13);
          xCampiXML := xCampiXML + '    <campo etichetta="#telefonoab" tabellare="0" valore="' + Q1.FieldByName('RecapitiTelefonici').asString + ' ' + Q1.FieldByName('Nome').asString + '"/>' + chr(13);
          xCampiXML := xCampiXML + '    <campo etichetta="#telufficio" tabellare="0" valore="' + Q1.FieldByName('TelUfficio').asString + ' ' + Q1.FieldByName('Nome').asString + '"/>' + chr(13);
          xCampiXML := xCampiXML + '    <campo etichetta="#cellulare" tabellare="0" valore="' + Q1.FieldByName('Cellulare').asString + ' ' + Q1.FieldByName('Nome').asString + '"/>' + chr(13);
          // altri dati
          Q1.close;
          Q1.sql.Clear;
          Q1.SQL.Text := 'select ServizioMilitareStato,Inquadramento,Retribuzione from AnagAltreInfo where IDAnagrafica=' + xIDAnag;
          Q1.Open;
          xCampiXML := xCampiXML + '    <campo etichetta="#ServMilStato" tabellare="0" valore="' + Q1.FieldByName('ServizioMilitareStato').asString + '"/>' + chr(13);
          xInquadramento := Q1.FieldByName('Inquadramento').asString;
          xRetribuzione := Q1.FieldByName('Retribuzione').asString;
          // FORMAZIONE
          Q1.close;
          Q1.sql.Clear;
          Q1.SQL.text := 'select tipo, Descrizione Titolo,LuogoConseguimento,DataConseguimento ' +
               'from TitoliStudio,Diplomi where TitoliStudio.IDDiploma=Diplomi.ID ' +
               'and IDAnagrafica=' + xIDAnag;
          Q1.Open;
          if Q1.RecordCount > 0 then begin
               xCampiXML := xCampiXML + '    <campo etichetta="#titoliStudio" tabellare="0" valore="';
               while not Q1.EOF do begin
                    xCampiXML := xCampiXML + Q1.FieldByName('Titolo').asString + ' conseguita presso: ' +
                         Q1.FieldByName('LuogoConseguimento').asString + ' nel ' +
                         Q1.FieldByName('DataConseguimento').asString;
                    Q1.Next;
                    if not Q1.EOF then xCampiXML := xCampiXML + '�';
               end;
               xCampiXML := xCampiXML + '"/>' + chr(13);
          end;
          // LINGUE
          Q1.close;
          Q1.sql.Clear;
          Q1.SQL.text := 'select Lingua, livelloConoscenza Livello' +
               ' from LingueConosciute left join LivelloLingue' +
               ' on LingueConosciute.Livello = LivelloLingue.ID' +
               ' where IDAnagrafica=' + xIDAnag;
          Q1.Open;
          if Q1.RecordCount > 0 then begin
               xCampiXML := xCampiXML + '    <campo etichetta="#lingue" tabellare="0" valore="';
               while not Q1.EOF do begin
                    xCampiXML := xCampiXML + Q1.FieldByName('Lingua').asString + ': ' + Q1.FieldByName('Livello').asString;
                    Q1.Next;
                    if not Q1.EOF then xCampiXML := xCampiXML + '�';
               end;
               xCampiXML := xCampiXML + '"/>' + chr(13);
          end;
          // PERCORSO PROFESSIONALE
          Q1.close;
          Q1.sql.Clear;
          Q1.SQL.text := 'select EBC_Clienti.Descrizione Azienda, EBC_Clienti.Comune, EBC_Clienti.Provincia, ' +
               'EBC_Clienti.NumDipendenti, Attivita, Mansioni.Descrizione Posizione, ' +
               'AnnoDal,AnnoAl,DescrizioneMansione ' +
               'from EsperienzeLavorative,EBC_Clienti,Mansioni,EBC_Attivita ' +
               'where EsperienzeLavorative.IDAzienda=EBC_Clienti.ID ' +
               'and EsperienzeLavorative.IDMansione=Mansioni.ID ' +
               'and EsperienzeLavorative.IDSettore=EBC_Attivita.ID and IDAnagrafica=' + xIDAnag;
          Q1.Open;
          if Q1.RecordCount > 0 then begin
               xCampiXML := xCampiXML + '    <campo etichetta="#EspLav" tabellare="0" valore="';
               while not Q1.EOF do begin
                    xCampiXML := xCampiXML + Q1.FieldByName('Azienda').asString + ' - ' + Q1.FieldByName('Comune').asString + ' (' + Q1.FieldByName('Provincia').asString + ')   ' +
                         Q1.FieldByName('AnnoDal').asString + '-' + Q1.FieldByName('AnnoAl').asString + '�' +
                         Q1.FieldByName('Posizione').asString + '�' +
                         'Dip.: ' + Q1.FieldByName('NumDipendenti').asString + '�' +
                         'Settore: ' + Data.Q1.FieldByName('Attivita').asString + '�' +
                         '�' +
                         'Attivit� principali' + '�';
                    xS := Q1.FieldByName('DescrizioneMansione').asString;
                    if xs <> '' then begin
                         while pos(chr(13), xS) > 0 do begin
                              xCRpos := pos(chr(13), xS);
                              xCampiXML := xCampiXML + copy(xS, 1, xCRpos - 1);
                              xS := copy(xS, xCRpos + 1, length(xS));
                         end;
                    end;
                    xCampiXML := xCampiXML + '�';
                    Q1.Next;
                    if not Q1.EOF then xCampiXML := xCampiXML + '�';
               end;
               xCampiXML := xCampiXML + '"/>' + chr(13);
          end;

          // condizioni di inserimento attuali
          if xInquadramento <> '' then begin
               xCampiXML := xCampiXML + '    <campo etichetta="#livello" tabellare="0" valore="' + xInquadramento + '"/>' + chr(13);
          end;
          if xRetribuzione <> '' then begin
               xCampiXML := xCampiXML + '    <campo etichetta="#retribuzione" tabellare="0" valore="' + xRetribuzione + '"/>' + chr(13);
          end;

          // ######## DA FARE ############ ALLEGA FILE

          // creazione file XML
          xXML := TStringList.create;
          xXML.Text := '<?xml version="1.0" encoding="ISO-8859-1"?> ' + chr(13) +
               '<operazioni> ' + chr(13) +
               '  <operazione nome="RiempiModelloWord" nomefile="' + xModelloWord + '" fileword="' + ExtractFileName(xFileWord) + '"> ' + chr(13) +
               xCampiXML +
               '  </operazione> ' + chr(13) +
               '</operazioni> ';
     end;

     // Salvataggio file XML sul client
     xXML.SaveToFile(xCartellaTempTS + 'h1client.xml');
     xLog.Add('Copiato file "h1client.xml" sul client.  XML: ' + chr(13) + xXML.text);
     xLog.SaveToFile(xTempFilePath + '\Log_CreaFilePresentaz_remoto.txt');

     xComando_ws := xCartellaTemp + '\H1Client.exe';
     xParameters_ws := xCartellaTemp + 'h1client.xml';

     // controllo esistenza H1Client.exe
     if not FileExists(xCartellaTempTS + 'h1client.exe') then begin
          if FileExists(ExtractFileDir(Application.ExeName) + '\H1Client.exe') then begin
               CopyRes := CopyFile(pchar(ExtractFileDir(Application.ExeName) + '\H1Client.exe'), pchar(xCartellaTempTS + 'H1Client.exe'), false);
               if CopyRes then begin
                    xLog.Add('Non esiste H1Client.exe sul client: copiato dal server'); xLog.SaveToFile(xTempFilePath + '\Log_CreaFilePresentaz_remoto.txt');
               end else begin
                    xLog.Add('Non esiste H1Client.exe sul client: copia dal server NON riuscita'); xLog.SaveToFile(xTempFilePath + '\Log_CreaFilePresentaz_remoto.txt');
               end;
          end else begin
               MessageDlg('Il file "H1Client.exe" non esiste n� sul client n� sul server' + chr(13) + 'IMPOSSIBILE CONTINUARE', mtError, [mbOK], 0);
               xLog.Add('Non esiste H1Client.exe sul client, ma nemmeno sul server !'); xLog.SaveToFile(xTempFilePath + '\Log_CreaFilePresentaz_remoto.txt');
               exit;
          end;
     end;

     try
          xLog.Add('Pronto alla chiamata: ' + chr(13) + '  Comando = ' + xComando_ws + chr(13) + '  Parametri: ' + xParameters_ws + chr(13)); xLog.SaveToFile(xTempFilePath + '\Log_CreaFilePresentaz_remoto.txt');

          res := TS_ShellExecute(0, 'Open', xComando_ws, xParameters_ws, '', SW_HIDE);

     finally
          // cancellazione file sul client
          // >>> NON FUNZIONA, perch� lo cancella prima che il client predisponga il messaggio
          //  i file vengono cancellati all'uscita dal programma (alla fine di MainForm.OnClose)

     end;

     case res of
          0: xErrorMsg := '0 = The operating system is out of memory or resources.';
          1: xErrorMsg := '1 = Non riesco ad aprire il virtual channel (TS_ShellExecute)';
          //2: xErrorMsg := '2 = The specified file was not found';
          2: xErrorMsg := '2 = il virtual channel si chiude prima di inviare i dati (TS_ShellExecute)';
          //3: xErrorMsg := '3 = The specified path was not found.';
          3: xErrorMsg := '3 = fallito l''invio dei dati al client (TS_ShellExecute)';

          5: xErrorMsg := '5 = Windows 95 only: The operating system denied access to the specified file';
          8: xErrorMsg := '8 = Windows 95 only: There was not enough memory to complete the operation.';
          10: xErrorMsg := '10 = Wrong Windows version';
          11: xErrorMsg := '11 = The.EXE file is invalid(non - Win32.EXE or error in .EXE image).';
          12: xErrorMsg := '12 = Application was designed for a different operating system.';
          13: xErrorMsg := '13 = Application was designed for MS - DOS 4.0';
          15: xErrorMsg := '15 = Attempt to load a real - mode program';
          16: xErrorMsg := '16 = Attempt to load a second instance of an application with non - readonly data segments';
          19: xErrorMsg := '19 = Attempt to load a compressed application file';
          20: xErrorMsg := '20 = Dynamic - link library(DLL)file failure".';
          26: xErrorMsg := '26 = A sharing violation occurred.';
          27: xErrorMsg := '27 = The filename association is incomplete or invalid.';
          28: xErrorMsg := '28 = The DDE transaction could not be completed because the request timed out.';
          29: xErrorMsg := '29 = The DDE transaction failed.';
          30: xErrorMsg := '30 = The DDE transaction could not be completed because other DDE transactions were being processed.';
          31: xErrorMsg := '31 = There is no application associated with the given filename extension.';
          32: xErrorMsg := '32 = Windows 95 only: The specified dynamic - link library was not found.';
     else xErrorMsg := IntToStr(res) + ' = (errore non documentato)';
     end;
     if (res < 0) or (res > 32) then
          xErrorMsg := 'OK (' + inttostr(res) + ')';

     // CODICI ERRORE COMUNICATI DA STEFANO ma NON gestiti:
     // 1: non riesce ad aprire il virtual channel
     // 2: il virtual channel si chiude prima di inviare i dati
     // 3: fallisce l'invio dei dai al client
     // 0: la ShellExecute fallisce (quindi la comunicazione � corretta ma c'� qualcosa che non va nel passaggio dei parametri)

     xLog.Add('Chiamata effettuata con risultato: ' + xErrorMsg); xLog.SaveToFile(xTempFilePath + '\Log_CreaFilePresentaz_remoto.txt');

     if copy(xErrorMsg, 1, 2) <> 'OK' then
          MessageDlg('ERRORE: ' + xErrorMsg, mtError, [mbOK], 0);

     // salvataggio modello
     ShowMessage('Modificare il file sul client e poi premere OK');

     // copia del file Word dal client
     CopyRes := CopyFile(pchar(xCartellaTempTS + ExtractFileName(xFileWord) + '.doc'), pchar(GetDocPath + '\' + ExtractFileName(xFileWord) + '.doc'), false);
     if CopyRes then begin
          xLog.Add('Copia file dal client: ' + xCartellaTempTS + ExtractFileName(xFileWord) + '.doc > ' + GetDocPath + '\' + ExtractFileName(xFileWord) + '.doc > OK'); xLog.SaveToFile(xTempFilePath + '\Log_CreaFilePresentaz_remoto.txt');
          Result := GetDocPath + '\' + ExtractFileName(xFileWord) + '.doc'
     end else begin
          xLog.Add('Copia file dal client: ' + xCartellaTempTS + ExtractFileName(xFileWord) + '.doc > ' + GetDocPath + '\' + ExtractFileName(xFileWord) + '.doc > ERRORE'); xLog.SaveToFile(xTempFilePath + '\Log_CreaFilePresentaz_remoto.txt');
          Result := '';
     end;

     // cancellazione file temporaneo sul client: aggiunto alla lista per la cancellazione all'uscita di H1
     MainForm.xFileTempClient.Add(xCartellaTempTS + xModelloWord + '.doc');
     MainForm.xFileTempClient.Add(xCartellaTempTS + ExtractFileName(xFileWord) + '.doc');
end;

function CreaFilePresentazPROPOSTE(xModelloWord, xFileWord, xCliente, xPosizione, xIDAnag: string; xIDMansione, xIDCandRic: integer; xAllegaFile: boolean): string;
var xTempFilePath, xComando_ws, xParameters_ws, xCartellaTemp, xCartellaTempTS, xs, xFile, xFileName, xBody, xAttachments, xFormato, xDest, xBcc: string;
     res, i, j, k, xCRpos, xIDAnagFile: integer;
     xErrorMsg: string;
     CopyRes, xApriSolo: boolean;
     xLog, xFiles, xXML: TStringList;
     xarray: array[1..1024] of WideChar;
     ps, xReg_Chiave, xReg_Path, xReg_Res: PWideChar;
     reg: TRegistry;
     regpostastring, key, find: string;
     Q1, Q2, QVal: TADOQuery;
     xCampiXML, xValore, xInquadramento, xRetribuzione, xAreaRuolo, xBenefits: string;
     xModTone: boolean;
     xAnno, xMese, xGiorno: Word;
begin
     xTempFilePath := data.Global.fieldbyname('DirFileLog').asstring;

     xLog := TStringList.create;

     // cartella temporanea
     ps := @xarray;
     res := TS_UserTemp(ps);
     xCartellaTemp := WideCharToString(ps);
     xLog.Add('Risultato chiamata TS_UserTemp: ' + xCartellaTemp); xLog.SaveToFile(xTempFilePath + '\Log_CreaFilePresentaz_remoto.txt');

     xCartellaTempTS := '\\tsclient\' + StringReplace(xCartellaTemp, ':', '', []);
     if not DirectoryExists(xCartellaTempTS) then begin
          xLog.Add('Cartella temp remota (client) NON ESISTE: ' + xCartellaTempTS); xLog.SaveToFile(xTempFilePath + '\Log_CreaFilePresentaz_remoto.txt')
     end else
          xLog.Add('Cartella temp remota (client): ' + xCartellaTempTS); xLog.SaveToFile(xTempFilePath + '\Log_CreaFilePresentaz_remoto.txt');

     Q1 := TADOQuery.create(nil);
     Q1.ConnectionString := Data.DB.ConnectionString;

     // verifica esistenza file
     xApriSolo := False;
     xIDAnagFile := 0;
     if data.Global.FieldByName('AnagFileDentroDB').AsBoolean then begin
          xFileWord := PathGetTempPath + ExtractFileName(xFileWord);
          Q1.Close;
          Q1.SQL.clear;
          Q1.SQL.text := 'select id, nomefile from anagfile where nomefile =:xnomefile';
          Q1.Parameters[0].Value := ExtractFileName(xFileWord) + '.doc';
          xLog.Add('Verifica esistenza file (nel DB): ' + ExtractFileName(xFileWord) + '.doc'); xLog.SaveToFile(xTempFilePath + '\Log_CreaFilePresentaz_remoto.txt');
          Q1.Open;
          if Q1.RecordCount > 0 then begin
               MessageDlg('Il file esiste gi� (nel database) - Impossibile proseguire', mtWarning, [mbOK], 0);
               Result := 'ABORT';
               Exit;
          end;
     end else begin
          if FileExists(xFileWord) then begin
               MessageDlg('Il file esiste gi� (nel file system) - Impossibile proseguire', mtWarning, [mbOK], 0);
               Result := 'ABORT';
               Exit;
          end;
     end;

     if xApriSolo then begin

          // #### RIVEDERE ############

     end else begin

          // Copia modello sul client
          CopyRes := CopyFile(pchar(GetDocPath + '\' + xModelloWord + '.doc'), pchar(xCartellaTempTS + xModelloWord + '.doc'), false);
          if CopyRes then begin
               xLog.Add('Copia file sul client: ' + GetDocPath + '\' + xModelloWord + '.doc > ' + xCartellaTempTS + xModelloWord + '.doc > OK'); xLog.SaveToFile(xTempFilePath + '\Log_CreaFilePresentaz_remoto.txt');
          end else begin
               xLog.Add('Copia file sul client: ' + GetDocPath + '\' + xModelloWord + '.doc > ' + xCartellaTempTS + xModelloWord + '.doc > ERRORE'); xLog.SaveToFile(xTempFilePath + '\Log_CreaFilePresentaz_remoto.txt');
          end;

          Q1.Close;
          Q1.sql.Clear;
          Q1.SQL.text := 'select IDAnagrafica,Note,Retribuzione,Nazionalita,Inquadramento, ' +
               'TipoRetrib,TipoRetribEng, Benefits, retribvariabile,retribtotale,settore ' +
               'from AnagAltreInfo ' +
               'left join TipiRetribuzione on AnagAltreInfo.IDTipoRetrib = TipiRetribuzione.ID ' +
               'left join ContrattiSettori on AnagAltreInfo.IDContrattoSettore = ContrattiSettori.ID ' +
               'where AnagAltreInfo.IDAnagrafica = ' + xIDAnag;
          Q1.Open;
          xRetribuzione := Q1.FieldByName('Retribuzione').AsString + '          ';
          xInquadramento := Q1.FieldByName('Inquadramento').AsString + '          ';
          xBenefits := Q1.FieldByName('Benefits').AsString + '          '; ;

          // pagina copertina
          xCampiXML := xCampiXML + '    <campo etichetta="#cliente" tabellare="0" valore="' + xCliente + '"/>' + chr(13);

          Q1.Close;
          Q1.sql.Clear;
          Q1.SQL.text := 'select Aree.Descrizione area from Aree,Mansioni ' +
               'where Aree.ID=Mansioni.IDArea and Mansioni.ID=' + IntToStr(xIDMansione);
          Q1.Open;
          xAreaRuolo := Q1.FieldByName('Area').asString + ' - ' + xPosizione;
          Q1.Close;
          xCampiXML := xCampiXML + '    <campo etichetta="#arearuolo" tabellare="0" valore="' + xAreaRuolo + '"/>' + chr(13);
          Q1.close;
          Q1.sql.Clear;
          Q1.SQL.Text := 'select SC.Descrizione StatoCivile, A.* from Anagrafica A left join StatiCivili SC on A.IDStatoCivile = SC.ID where A.ID=' + xIDAnag;
          Q1.Open;
          xCampiXML := xCampiXML + '    <campo etichetta="#cognomeNome" tabellare="0" valore="' + Q1.FieldByName('Cognome').asString + ' ' + Q1.FieldByName('Nome').asString + '"/>' + chr(13);
          xCampiXML := xCampiXML + '    <campo etichetta="#data" tabellare="0" valore="' + DateToStr(Date) + '"/>' + chr(13);

          // pagina 1
          xCampiXML := xCampiXML + '    <campo etichetta="#cognomeNome" tabellare="0" valore="' + Q1.FieldByName('Cognome').asString + ' ' + Q1.FieldByName('Nome').asString + '"/>' + chr(13);
          xCampiXML := xCampiXML + '    <campo etichetta="#LuogoDataNascita" tabellare="0" valore="' + Q1.FieldByName('LuogoNascita').AsString + ', ' + Q1.FieldByName('DataNascita').asString + '"/>' + chr(13);
          xCampiXML := xCampiXML + '    <campo etichetta="#Residenza" tabellare="0" valore="' + Q1.FieldByName('Comune').asString + ' ' + Q1.FieldByName('Provincia').asString + '"/>' + chr(13);
          xCampiXML := xCampiXML + '    <campo etichetta="#statocivile" tabellare="0" valore="' + Q1.FieldByName('StatoCivile').asString + ' ' + Q1.FieldByName('Nome').asString + '"/>' + chr(13);

          // FORMAZIONE
          Q1.close;
          Q1.sql.Clear;
          Q1.SQL.text := 'select tipo, Descrizione Titolo,LuogoConseguimento,DataConseguimento ' +
               'from TitoliStudio,Diplomi where TitoliStudio.IDDiploma=Diplomi.ID ' +
               'and IDAnagrafica=' + xIDAnag;
          Q1.Open;
          if Q1.RecordCount > 0 then begin
               xCampiXML := xCampiXML + '    <campo etichetta="#titoliStudio" tabellare="0" valore="';
               while not Q1.EOF do begin
                    xCampiXML := xCampiXML + Q1.FieldByName('Titolo').asString + ' conseguita presso: ' +
                         Q1.FieldByName('LuogoConseguimento').asString + ' nel ' +
                         Q1.FieldByName('DataConseguimento').asString;
                    Q1.Next;
                    if not Q1.EOF then xCampiXML := xCampiXML + '�';
               end;
               xCampiXML := xCampiXML + '"/>' + chr(13);
          end;
          // LINGUE
          Q1.close;
          Q1.sql.Clear;
          Q1.SQL.text := 'select Lingua, LivParlato.livelloConoscenza LivelloParlato, LivScritto.livelloConoscenza LivelloScritto' +
               ' from LingueConosciute left join LivelloLingue LivParlato on LingueConosciute.Livello = LivParlato.ID' +
               ' left join LivelloLingue LivScritto on LingueConosciute.LivelloScritto = LivScritto.ID' +
               ' where IDAnagrafica=' + xIDAnag;
          Q1.Open;
          if Q1.RecordCount > 0 then begin
               xCampiXML := xCampiXML + '    <campo etichetta="#lingue" tabellare="0" valore="';
               while not Q1.EOF do begin
                    xCampiXML := xCampiXML + Q1.FieldByName('Lingua').asString + ': ' + Q1.FieldByName('LivelloParlato').asString;
                    Q1.Next;
                    if not Q1.EOF then xCampiXML := xCampiXML + '�';
               end;
               xCampiXML := xCampiXML + '"/>' + chr(13);
          end;
          // PERCORSO PROFESSIONALE
          Q1.close;
          Q1.sql.Clear;
          Q1.SQL.text := 'select EBC_Clienti.Descrizione Azienda, EBC_Clienti.Comune, EBC_Clienti.Provincia, ' +
               'EBC_Clienti.NumDipendenti, Attivita, Mansioni.Descrizione Posizione, ' +
               'AnnoDal,AnnoAl,DescrizioneMansione ' +
               'from EsperienzeLavorative,EBC_Clienti,Mansioni,EBC_Attivita ' +
               'where EsperienzeLavorative.IDAzienda=EBC_Clienti.ID ' +
               'and EsperienzeLavorative.IDMansione=Mansioni.ID ' +
               'and EsperienzeLavorative.IDSettore=EBC_Attivita.ID and IDAnagrafica=' + xIDAnag;
          Q1.Open;
          if Q1.RecordCount > 0 then begin
               xCampiXML := xCampiXML + '    <campo etichetta="#EspLav" tabellare="0" valore="';
               while not Q1.EOF do begin
                    xCampiXML := xCampiXML + Q1.FieldByName('AnnoDal').asString + '-' + Q1.FieldByName('AnnoAl').asString + '�' +
                         Q1.FieldByName('Azienda').asString + ' - ' + Q1.FieldByName('Comune').asString + ' (' + Q1.FieldByName('Provincia').asString + ') �' +
                         '(' + Data.Q1.FieldByName('Attivita').asString + ') �' +
                         '�' +
                         Q1.FieldByName('Posizione').asString + '�' +
                         '�';
                    xS := Q1.FieldByName('DescrizioneMansione').asString;
                    if xs <> '' then begin
                         while pos(chr(13), xS) > 0 do begin
                              xCRpos := pos(chr(13), xS);
                              xCampiXML := xCampiXML + copy(xS, 1, xCRpos - 1);
                              xS := copy(xS, xCRpos + 1, length(xS));
                         end;
                    end;
                    xCampiXML := xCampiXML + '�';
                    Q1.Next;
                    if not Q1.EOF then xCampiXML := xCampiXML + '�';
               end;
               xCampiXML := xCampiXML + '"/>' + chr(13);
          end;

          if xInquadramento <> '' then begin
               xCampiXML := xCampiXML + '    <campo etichetta="#inquadramento" tabellare="0" valore="' + xInquadramento + '"/>' + chr(13);
          end;
          if xRetribuzione <> '' then begin
               xCampiXML := xCampiXML + '    <campo etichetta="#retribuzione" tabellare="0" valore="' + xRetribuzione + '"/>' + chr(13);
          end;
          if xBenefits <> '' then begin
               xCampiXML := xCampiXML + '    <campo etichetta="#benefits" tabellare="0" valore="' + xBenefits + '"/>' + chr(13);
          end;

          // creazione file XML
          xXML := TStringList.create;
          xXML.Text := '<?xml version="1.0" encoding="ISO-8859-1"?> ' + chr(13) +
               '<operazioni> ' + chr(13) +
               '  <operazione nome="RiempiModelloWord" nomefile="' + xModelloWord + '" fileword="' + ExtractFileName(xFileWord) + '"> ' + chr(13) +
               xCampiXML +
               '  </operazione> ' + chr(13) +
               '</operazioni> ';
     end;

     // Salvataggio file XML sul client
     xXML.SaveToFile(xCartellaTempTS + 'h1client.xml');
     xLog.Add('Copiato file "h1client.xml" sul client.  XML: ' + chr(13) + xXML.text);
     xLog.SaveToFile(xTempFilePath + '\Log_CreaFilePresentaz_remoto.txt');

     xComando_ws := xCartellaTemp + '\H1Client.exe';
     xParameters_ws := xCartellaTemp + 'h1client.xml';

     // controllo esistenza H1Client.exe
     if not FileExists(xCartellaTempTS + 'h1client.exe') then begin
          if FileExists(ExtractFileDir(Application.ExeName) + '\H1Client.exe') then begin
               CopyRes := CopyFile(pchar(ExtractFileDir(Application.ExeName) + '\H1Client.exe'), pchar(xCartellaTempTS + 'H1Client.exe'), false);
               if CopyRes then begin
                    xLog.Add('Non esiste H1Client.exe sul client: copiato dal server'); xLog.SaveToFile(xTempFilePath + '\Log_CreaFilePresentaz_remoto.txt');
               end else begin
                    xLog.Add('Non esiste H1Client.exe sul client: copia dal server NON riuscita'); xLog.SaveToFile(xTempFilePath + '\Log_CreaFilePresentaz_remoto.txt');
               end;
          end else begin
               MessageDlg('Il file "H1Client.exe" non esiste n� sul client n� sul server' + chr(13) + 'IMPOSSIBILE CONTINUARE', mtError, [mbOK], 0);
               xLog.Add('Non esiste H1Client.exe sul client, ma nemmeno sul server !'); xLog.SaveToFile(xTempFilePath + '\Log_CreaFilePresentaz_remoto.txt');
               exit;
          end;
     end;

     try
          xLog.Add('Pronto alla chiamata: ' + chr(13) + '  Comando = ' + xComando_ws + chr(13) + '  Parametri: ' + xParameters_ws + chr(13)); xLog.SaveToFile(xTempFilePath + '\Log_CreaFilePresentaz_remoto.txt');

          res := TS_ShellExecute(0, 'Open', xComando_ws, xParameters_ws, '', SW_HIDE);

     finally
          // cancellazione file sul client
          // >>> NON FUNZIONA, perch� lo cancella prima che il client predisponga il messaggio
          //  i file vengono cancellati all'uscita dal programma (alla fine di MainForm.OnClose)

     end;

     case res of
          0: xErrorMsg := '0 = The operating system is out of memory or resources.';
          1: xErrorMsg := '1 = Non riesco ad aprire il virtual channel (TS_ShellExecute)';
          //2: xErrorMsg := '2 = The specified file was not found';
          2: xErrorMsg := '2 = il virtual channel si chiude prima di inviare i dati (TS_ShellExecute)';
          //3: xErrorMsg := '3 = The specified path was not found.';
          3: xErrorMsg := '3 = fallito l''invio dei dati al client (TS_ShellExecute)';

          5: xErrorMsg := '5 = Windows 95 only: The operating system denied access to the specified file';
          8: xErrorMsg := '8 = Windows 95 only: There was not enough memory to complete the operation.';
          10: xErrorMsg := '10 = Wrong Windows version';
          11: xErrorMsg := '11 = The.EXE file is invalid(non - Win32.EXE or error in .EXE image).';
          12: xErrorMsg := '12 = Application was designed for a different operating system.';
          13: xErrorMsg := '13 = Application was designed for MS - DOS 4.0';
          15: xErrorMsg := '15 = Attempt to load a real - mode program';
          16: xErrorMsg := '16 = Attempt to load a second instance of an application with non - readonly data segments';
          19: xErrorMsg := '19 = Attempt to load a compressed application file';
          20: xErrorMsg := '20 = Dynamic - link library(DLL)file failure".';
          26: xErrorMsg := '26 = A sharing violation occurred.';
          27: xErrorMsg := '27 = The filename association is incomplete or invalid.';
          28: xErrorMsg := '28 = The DDE transaction could not be completed because the request timed out.';
          29: xErrorMsg := '29 = The DDE transaction failed.';
          30: xErrorMsg := '30 = The DDE transaction could not be completed because other DDE transactions were being processed.';
          31: xErrorMsg := '31 = There is no application associated with the given filename extension.';
          32: xErrorMsg := '32 = Windows 95 only: The specified dynamic - link library was not found.';
     else xErrorMsg := IntToStr(res) + ' = (errore non documentato)';
     end;
     if (res < 0) or (res > 32) then
          xErrorMsg := 'OK (' + inttostr(res) + ')';

     // CODICI ERRORE COMUNICATI DA STEFANO ma NON gestiti:
     // 1: non riesce ad aprire il virtual channel
     // 2: il virtual channel si chiude prima di inviare i dati
     // 3: fallisce l'invio dei dai al client
     // 0: la ShellExecute fallisce (quindi la comunicazione � corretta ma c'� qualcosa che non va nel passaggio dei parametri)

     xLog.Add('Chiamata effettuata con risultato: ' + xErrorMsg); xLog.SaveToFile(xTempFilePath + '\Log_CreaFilePresentaz_remoto.txt');

     if copy(xErrorMsg, 1, 2) <> 'OK' then
          MessageDlg('ERRORE: ' + xErrorMsg, mtError, [mbOK], 0);

     // salvataggio modello
     ShowMessage('Modificare il file sul client e poi premere OK');

     // copia del file Word dal client
     CopyRes := CopyFile(pchar(xCartellaTempTS + ExtractFileName(xFileWord) + '.doc'), pchar(GetDocPath + '\' + ExtractFileName(xFileWord) + '.doc'), false);
     if CopyRes then begin
          xLog.Add('Copia file dal client: ' + xCartellaTempTS + ExtractFileName(xFileWord) + '.doc > ' + GetDocPath + '\' + ExtractFileName(xFileWord) + '.doc > OK'); xLog.SaveToFile(xTempFilePath + '\Log_CreaFilePresentaz_remoto.txt');
          Result := GetDocPath + '\' + ExtractFileName(xFileWord) + '.doc'
     end else begin
          xLog.Add('Copia file dal client: ' + xCartellaTempTS + ExtractFileName(xFileWord) + '.doc > ' + GetDocPath + '\' + ExtractFileName(xFileWord) + '.doc > ERRORE'); xLog.SaveToFile(xTempFilePath + '\Log_CreaFilePresentaz_remoto.txt');
          Result := '';
     end;

     // cancellazione file temporaneo sul client: aggiunto alla lista per la cancellazione all'uscita di H1
     MainForm.xFileTempClient.Add(xCartellaTempTS + xModelloWord + '.doc');
     MainForm.xFileTempClient.Add(xCartellaTempTS + ExtractFileName(xFileWord) + '.doc');
end;


function RiempiModelloGenerico_remoto(xQuale, xFileModello, xFileWord, xXMLDati: string; xFileAdd: string = ''): string;
var xTempFilePath, xComando_ws, xParameters_ws, xCartellaTemp, xCartellaTempTS, xs, xFile, xFileName, xBody, xAttachments, xFormato, xDest, xBcc: string;
     res, i, j, k: integer;
     xErrorMsg: string;
     CopyRes: boolean;
     xLog, xFiles, xXML: TStringList;
     xarray: array[1..1024] of WideChar;
     ps, xReg_Chiave, xReg_Path, xReg_Res: PWideChar;
     reg: TRegistry;
     regpostastring, key, find: string;
     Q1, Q2, QVal: TADOQuery;
     xCampiXML, xValore: string;
     xModTone: boolean;
     xAnno, xMese, xGiorno: Word;
begin
     xTempFilePath := data.Global.fieldbyname('DirFileLog').asstring;

     xLog := TStringList.create;

     // cartella temporanea
     ps := @xarray;
     res := TS_UserTemp(ps);
     xCartellaTemp := WideCharToString(ps);
     xLog.Add('Risultato chiamata TS_UserTemp: ' + xCartellaTemp); xLog.SaveToFile(xTempFilePath + '\Log_RiempiModello_' + xQuale + '_remoto.txt');

     xCartellaTempTS := '\\tsclient\' + StringReplace(xCartellaTemp, ':', '', []);
     if not DirectoryExists(xCartellaTempTS) then begin
          xLog.Add('Cartella temp remota (client) NON ESISTE: ' + xCartellaTempTS); xLog.SaveToFile(xTempFilePath + '\Log_RiempiModello_' + xQuale + '_remoto.txt')
     end else
          xLog.Add('Cartella temp remota (client): ' + xCartellaTempTS); xLog.SaveToFile(xTempFilePath + '\Log_RiempiModello_' + xQuale + '_remoto.txt');

     // controllo esistenza modello
     if not FileExists(GetDocPath + '\' + xFileModello) then begin
          MessageDlg('Modello non trovato. File:' + chr(13) + GetDocPath + '\' + xFileModello, mtError, [mbOK], 0);
          Q1.Close;
          xLog.Add('Modello non trovato: ' + GetDocPath + '\' + xFileModello); xLog.SaveToFile(xTempFilePath + '\Log_RiempiModello_' + xQuale + '_remoto.txt');
          exit;
     end;

     // Copia modello sul client
     CopyRes := CopyFile(pchar(GetDocPath + '\' + xFileModello), pchar(xCartellaTempTS + xFileModello), false);
     if CopyRes then begin
          xLog.Add('Copia file sul client: ' + GetDocPath + '\' + xFileModello + ' > ' + xCartellaTempTS + xFileModello + ' > OK'); xLog.SaveToFile(xTempFilePath + '\Log_RiempiModello_' + xQuale + '_remoto.txt');
     end else begin
          xLog.Add('Copia file sul client: ' + GetDocPath + '\' + xFileModello + ' > ' + xCartellaTempTS + xFileModello + ' > ERRORE'); xLog.SaveToFile(xTempFilePath + '\Log_RiempiModello_' + xQuale + '_remoto.txt');
     end;

     if xFileAdd <> '' then begin
          // copia file da inserire
          CopyRes := CopyFile(pchar(GetDocPath + '\' + xFileAdd), pchar(xCartellaTempTS + xFileAdd), false);
          if CopyRes then begin
               xLog.Add('Copia file DA AGGIUNGERE sul client: ' + GetDocPath + '\' + xFileAdd + ' > ' + xCartellaTempTS + xFileAdd + ' > OK'); xLog.SaveToFile(xTempFilePath + '\Log_RiempiModello_' + xQuale + '_remoto.txt');
          end else begin
               xLog.Add('Copia file DA AGGIUNGERE sul client: ' + GetDocPath + '\' + xFileAdd + ' > ' + xCartellaTempTS + xFileAdd + ' > ERRORE'); xLog.SaveToFile(xTempFilePath + '\Log_RiempiModello_' + xQuale + '_remoto.txt');
          end;
          // cancella file
          DeleteFile(GetDocPath + '\' + xFileAdd);
     end;

     // Salvataggio file XML sul client
     xXML := TStringList.create;
     xXML.text := xXMLDati;
     xXML.Text := WideStringToUTF8(xXML.Text);
     xXML.SaveToFile(xCartellaTempTS + 'h1client.xml');
     xLog.Add('Copiato file "h1client.xml" sul client.  XML: ' + chr(13) + xXML.text);
     xLog.SaveToFile(xTempFilePath + '\Log_RiempiModello_' + xQuale + '_remoto.txt');

     xComando_ws := xCartellaTemp + '\H1Client.exe';
     xParameters_ws := xCartellaTemp + 'h1client.xml';

     // controllo esistenza H1Client.exe
     if not FileExists(xCartellaTempTS + 'h1client.exe') then begin
          if FileExists(ExtractFileDir(Application.ExeName) + '\H1Client.exe') then begin
               CopyRes := CopyFile(pchar(ExtractFileDir(Application.ExeName) + '\H1Client.exe'), pchar(xCartellaTempTS + 'H1Client.exe'), false);
               if CopyRes then begin
                    xLog.Add('Non esiste H1Client.exe sul client: copiato dal server'); xLog.SaveToFile(xTempFilePath + '\Log_RiempiModello_' + xQuale + '_remoto.txt');
               end else begin
                    xLog.Add('Non esiste H1Client.exe sul client: copia dal server NON riuscita'); xLog.SaveToFile(xTempFilePath + '\Log_RiempiModello_' + xQuale + '_remoto.txt');
               end;
          end else begin
               MessageDlg('Il file "H1Client.exe" non esiste n� sul client n� sul server' + chr(13) + 'IMPOSSIBILE CONTINUARE', mtError, [mbOK], 0);
               xLog.Add('Non esiste H1Client.exe sul client, ma nemmeno sul server !'); xLog.SaveToFile(xTempFilePath + '\Log_RiempiModello_' + xQuale + '_remoto.txt');
               exit;
          end;
     end;

     try
          xLog.Add('Pronto alla chiamata: ' + chr(13) + '  Comando = ' + xComando_ws + chr(13) + '  Parametri: ' + xParameters_ws + chr(13)); xLog.SaveToFile(xTempFilePath + '\Log_RiempiModello_' + xQuale + '_remoto.txt');

          res := TS_ShellExecute(0, 'Open', xComando_ws, xParameters_ws, '', SW_HIDE);

     finally
          // cancellazione file sul client
          // >>> NON FUNZIONA, perch� lo cancella prima che il client predisponga il messaggio
          //  i file vengono cancellati all'uscita dal programma (alla fine di MainForm.OnClose)

     end;

     case res of
          0: xErrorMsg := '0 = The operating system is out of memory or resources.';
          1: xErrorMsg := '1 = Non riesco ad aprire il virtual channel (TS_ShellExecute)';
          //2: xErrorMsg := '2 = The specified file was not found';
          2: xErrorMsg := '2 = il virtual channel si chiude prima di inviare i dati (TS_ShellExecute)';
          //3: xErrorMsg := '3 = The specified path was not found.';
          3: xErrorMsg := '3 = fallito l''invio dei dati al client (TS_ShellExecute)';

          5: xErrorMsg := '5 = Windows 95 only: The operating system denied access to the specified file';
          8: xErrorMsg := '8 = Windows 95 only: There was not enough memory to complete the operation.';
          10: xErrorMsg := '10 = Wrong Windows version';
          11: xErrorMsg := '11 = The.EXE file is invalid(non - Win32.EXE or error in .EXE image).';
          12: xErrorMsg := '12 = Application was designed for a different operating system.';
          13: xErrorMsg := '13 = Application was designed for MS - DOS 4.0';
          15: xErrorMsg := '15 = Attempt to load a real - mode program';
          16: xErrorMsg := '16 = Attempt to load a second instance of an application with non - readonly data segments';
          19: xErrorMsg := '19 = Attempt to load a compressed application file';
          20: xErrorMsg := '20 = Dynamic - link library(DLL)file failure".';
          26: xErrorMsg := '26 = A sharing violation occurred.';
          27: xErrorMsg := '27 = The filename association is incomplete or invalid.';
          28: xErrorMsg := '28 = The DDE transaction could not be completed because the request timed out.';
          29: xErrorMsg := '29 = The DDE transaction failed.';
          30: xErrorMsg := '30 = The DDE transaction could not be completed because other DDE transactions were being processed.';
          31: xErrorMsg := '31 = There is no application associated with the given filename extension.';
          32: xErrorMsg := '32 = Windows 95 only: The specified dynamic - link library was not found.';
     else xErrorMsg := IntToStr(res) + ' = (errore non documentato)';
     end;
     if (res < 0) or (res > 32) then
          xErrorMsg := 'OK (' + inttostr(res) + ')';

     // CODICI ERRORE COMUNICATI DA STEFANO ma NON gestiti:
     // 1: non riesce ad aprire il virtual channel
     // 2: il virtual channel si chiude prima di inviare i dati
     // 3: fallisce l'invio dei dai al client
     // 0: la ShellExecute fallisce (quindi la comunicazione � corretta ma c'� qualcosa che non va nel passaggio dei parametri)

     xLog.Add('Chiamata effettuata con risultato: ' + xErrorMsg); xLog.SaveToFile(xTempFilePath + '\Log_RiempiModello_' + xQuale + '_remoto.txt');

     if copy(xErrorMsg, 1, 2) <> 'OK' then
          MessageDlg('ERRORE: ' + xErrorMsg, mtError, [mbOK], 0);

     // salvataggio modello
     ShowMessage('Modificare il file sul client e poi premere OK');

     xFileWord := xFileWord;// + '.doc';
     // copia del file Word dal client
     CopyRes := CopyFile(pchar(xCartellaTempTS + xFileWord), pchar(GetDocPath + '\' + xFileWord), false);
     if CopyRes then begin
          xLog.Add('Copia file dal client: ' + xCartellaTempTS + xFileWord + ' > ' + GetDocPath + '\' + xFileWord + ' > OK'); xLog.SaveToFile(xTempFilePath + '\Log_RiempiModello_' + xQuale + '_remoto.txt');
          Result := GetDocPath + '\' + xFileWord;
     end else begin
          xLog.Add('Copia file dal client: ' + xCartellaTempTS + xFileWord + ' > ' + GetDocPath + '\' + xFileWord + ' > ERRORE'); xLog.SaveToFile(xTempFilePath + '\Log_RiempiModello_' + xQuale + '_remoto.txt');
          Result := '';
     end;

     // cancellazione file temporaneo sul client: aggiunto alla lista per la cancellazione all'uscita di H1
     MainForm.xFileTempClient.Add(xCartellaTempTS + xFileModello);
     MainForm.xFileTempClient.Add(xCartellaTempTS + xFileWord);
end;

function GetDocPath: string;
var xH1DocPath: string;
begin
     with Data.Q2 do begin
          Close;
          SQL.text := 'select DirFileDoc from Global';
          Open;
          if IsEmpty then Result := ''
          else begin
               if Pos('\', copy(FieldByName('DirFileDoc').asString, length(FieldByName('DirFileDoc').asString) - 1, length(FieldByName('DirFileDoc').asString))) > 0 then
                    Result := Copy(FieldByName('DirFileDoc').asString, 1, length(FieldByName('DirFileDoc').asString) - 1)
               else
                    Result := FieldByName('DirFileDoc').asString;
          end;
          Close;
     end;
end;

end.

