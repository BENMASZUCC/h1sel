unit FileAperto;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, ExtCtrls;

type
     TFileApertoForm = class(TForm)
          Panel1: TPanel;
          BitBtn1: TBitBtn;
          Label1: TLabel;
          LNomeFile: TLabel;
          Label2: TLabel;
          Label3: TLabel;
          procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
          procedure BitBtn1Click(Sender: TObject);
          procedure FormShow(Sender: TObject);
     private
    { Private declarations }
     public
    { Public declarations }
          xCanClose: boolean;
          xFile: string;
          xHandle: DWord;
     end;

var
     FileApertoForm: TFileApertoForm;

implementation

uses uUtilsVarie;

{$R *.DFM}

procedure TFileApertoForm.FormCloseQuery(Sender: TObject;
     var CanClose: Boolean);
begin
     CanClose := xcanClose;
end;

procedure TFileApertoForm.BitBtn1Click(Sender: TObject);
begin
     xCanClose := true;
end;

procedure TFileApertoForm.FormShow(Sender: TObject);
begin
xCanClose := false;
end;

end.

