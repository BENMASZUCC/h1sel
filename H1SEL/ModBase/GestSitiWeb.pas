unit GestSitiWeb;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ExtCtrls, dxExEdtr, dxEdLib, dxDBELib, jpeg, Buttons, DBCtrls, dxDBTLCl,
     dxGrClms, dxTL, dxDBCtrl, dxDBGrid, dxCntner, Db, ADODB, uUtilsVarie,
     StdCtrls, TB97;

type
     TGestSitiWebForm = class(TForm)
          Panel1: TPanel;
          DsQWebLinks: TDataSource;
          QWebLinks: TADOQuery;
          QWebLinksID: TAutoIncField;
          QWebLinksDescrizione: TStringField;
          QWebLinksLogo: TBlobField;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1ID: TdxDBGridMaskColumn;
          dxDBGrid1Column4: TdxDBGridGraphicColumn;
          dxDBGrid1Descrizione: TdxDBGridMaskColumn;
          DBNavigator1: TDBNavigator;
          OpenDialog1: TOpenDialog;
          QWebLinksVisibileHome: TBooleanField;
          dxDBGrid1VisibileHome: TdxDBGridCheckColumn;
          BOK: TToolbarButton97;
          SpeedButton1: TToolbarButton97;
          procedure SpeedButton1Click(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
          procedure QWebLinksBeforeDelete(DataSet: TDataSet);
          procedure dxDBGrid1Column4GetGraphicClass(Sender: TObject;
               Node: TdxTreeListNode; var GraphicClass: TGraphicClass);
          procedure BOKClick(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     GestSitiWebForm: TGestSitiWebForm;

implementation

uses ModuloDati, Main;

{$R *.DFM}

procedure TGestSitiWebForm.SpeedButton1Click(Sender: TObject);
var xfilename: string;
begin
     //OpenDialog1.Filter := 'JPG File Extension (*.jpg)|*.JPG';
     if OpenDialog1.Execute then begin
          xfilename := OpenDialog1.FileName;

          QWebLinks.Edit;

          QWebLinksLogo.LoadFromFile(xFileName);
          QWebLinks.Post;

          QWebLinks.Close;
          QWebLinks.Open;
     end;
end;

procedure TGestSitiWebForm.FormShow(Sender: TObject);
begin
     //Grafica
     GestSitiWebForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     QWebLinks.Open;
     OpenDialog1.Filter := 'JPG File Extension (*.jpg)|*.JPG';
end;

procedure TGestSitiWebForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     if DsQWebLinks.state in [dsInsert, dsEdit] then QWebLinks.Post;
end;

procedure TGestSitiWebForm.QWebLinksBeforeDelete(DataSet: TDataSet);
begin
     // controllo integritÓ referenziale
     if not VerifIntegrRef('AnagWebLinks', 'IDWebLink', QWebLinksID.AsString) then abort;
end;

procedure TGestSitiWebForm.dxDBGrid1Column4GetGraphicClass(Sender: TObject;
     Node: TdxTreeListNode; var GraphicClass: TGraphicClass);
begin
     GraphicClass := TJPEGImage;
end;

procedure TGestSitiWebForm.BOKClick(Sender: TObject);
begin
     GestSitiWebForm.ModalResult := mrOK;
end;

end.

