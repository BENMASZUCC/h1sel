select distinct Anagrafica.ID,Anagrafica.dacontattocliente,Anagrafica.Cognome,Anagrafica.Nome,Anagrafica.CVNumero,Anagrafica.OldCVNumero,
    Anagrafica.TelUfficio,Anagrafica.Cellulare,CVInseritoInData,CVDataReale,CVDataAgg,RecapitiTelefonici,
    Anagrafica.DataNascita,Anagrafica.IDStato,Anagrafica.IDTipoStato, 
    Anagrafica.Provincia, Anagrafica.Comune,anagrafica.indirizzo,anagrafica.cap,anagrafica.numcivico,anagrafica.tipostrada,
    anagrafica.domiciliotipostrada,anagrafica.domiciliocap,anagrafica.domicilionumcivico,anagrafica.domicilioindirizzo,anagrafica.domiciliocomune,anagrafica.domicilioprovincia, 
    DATEDIFF(day, EBC_CandidatiRicerche.DataIns, getdate()) ggUltimaDataIns,
    DATEDIFF(day, EBC_ContattiCandidati.Data, getdate()) ggDataUltimoContatto,
    EBC_CandidatiRicerche.DataImpegno DataProssimoColloquio,
    DATEDIFF(day, EBC_Colloqui.Data, getdate()) ggDataUltimoColloquio,
    DATEDIFF(day, Anagrafica.CVInseritoInData, getdate()) ggDataInsCV, NoCv,AnagAltreInfo.Retribuzione,EMail, AnagAltreInfo.Inquadramento, cast (anagfileD.IDAnagfile/anagfileD.IDAnagfile as bit) as FileCheck, 
    googledistanzaric.distanza,googledistanzaric.tempo,emailufficio,
    EspLavAttuale.Azienda, EspLavAttuale.titolomansione, EspLavAttuale.Ruolo, EspLavAttuale.descrizionemansione, 
    EspLavAttuale.JobTitle, EspLavAttuale.Area, EspLavAttuale.Settore 
from Anagrafica
join AnagAltreInfo on Anagrafica.ID=AnagAltreInfo.IDAnagrafica
left outer join AnagCampiPers on AnagcampiPers.IDAnagrafica = Anagrafica.ID
left outer join (select distinct idanagrafica as IDAnagFile from anagfile) as AnagFileD on Anagrafica.ID = anagfileD.IDAnagfile
left outer join googledistanzaric on Anagrafica.id = googledistanzaric.idanagrafica

join EsperienzeLavorative on Anagrafica.ID=EsperienzeLavorative.IDAnagrafica
-- ultimo inserimento in una (eventuale) ricerca
left outer join EBC_CandidatiRicerche on Anagrafica.ID = EBC_CandidatiRicerche.IDAnagrafica
     and EBC_CandidatiRicerche.DataIns =
     (select max(DataIns) from EBC_CandidatiRicerche
      where Anagrafica.ID = EBC_CandidatiRicerche.IDAnagrafica)
-- ultimo contatto
left outer join EBC_ContattiCandidati on Anagrafica.ID = EBC_ContattiCandidati.IDAnagrafica
    and EBC_ContattiCandidati.Data=
    (select max(Data) from EBC_ContattiCandidati
     where Anagrafica.ID = EBC_ContattiCandidati.IDAnagrafica)
-- ultimo colloquio
left outer join EBC_Colloqui on Anagrafica.ID = EBC_Colloqui.IDAnagrafica
	and EBC_Colloqui.Data=
    (select max(Data) from EBC_Colloqui
     where Anagrafica.ID = EBC_Colloqui.IDAnagrafica)
-- esperienza lavorativa attuale (ultima, se pi� di una)
left outer join (
	select IDAnagrafica,max(ID) MaxID
	from EsperienzeLavorative 
	where Attuale=1
	group by IDAnagrafica
	) EspLav_MaxID on Anagrafica.ID = EspLav_MaxID.IDAnagrafica
left outer join (
	select Esperienzelavorative.ID, Esperienzelavorative.IDAnagrafica,
		   Azienda= case
			when Esperienzelavorative.IDAzienda is null then Esperienzelavorative.AziendaNome
			else
			EBC_Clienti.Descrizione 
			end,
	 titolomansione,
	 Mansioni.Descrizione Ruolo, cast(descrizionemansione as varchar(255)) descrizionemansione, 
	 ruolopresentaz as JobTitle,
	 Aree.Descrizione Area, ebc_attivita.attivita Settore
	from Esperienzelavorative 
	left join EBC_Clienti
	on Esperienzelavorative.IDAzienda = EBC_Clienti.ID
	left join Mansioni
	on Esperienzelavorative.IDMansione = Mansioni.ID
	join aree
	on Mansioni.IDArea=aree.id
	left join ebc_attivita
	on ebc_attivita.id=esperienzelavorative.idsettore
  ) EspLavAttuale on EspLav_MaxID.MaxID = EspLavAttuale.ID
where Anagrafica.ID is not null

-- ulteriori filtri
and
 EsperienzeLavorative.IDArea = 38

-- filtri sullo stato 
and Anagrafica.IDTipoStato<>3
and Anagrafica.IDTipoStato<>5
and Anagrafica.IDTipoStato<>6
and Anagrafica.IDTipoStato<>7
and Anagrafica.IDTipoStato<>8
and Anagrafica.IDTipoStato<>11
and Anagrafica.IDTipoStato<>12
and (Anagrafica.IDProprietaCV is null or Anagrafica.IDProprietaCV=0)

order by Anagrafica.Cognome,Anagrafica.Nome
