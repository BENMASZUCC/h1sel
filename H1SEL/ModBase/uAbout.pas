unit uAbout;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     URLLabel, ExtCtrls, StdCtrls, TB97, Db, dxmdaset, LbCipher, LbClass,
     LbAsym, LbRSA, MSXML_TLB, LbUtils, ShellApi, dxDBTLCl, dxGrClms,
     dxDBCtrl, dxDBGrid, dxTL, dxCntner;

type
     TAboutForm = class(TForm)
          Panel1: TPanel;
          Label9: TLabel;
          Memo2: TMemo;
          Panel3: TPanel;
          Label4: TLabel;
          Label5: TLabel;
          Label6: TLabel;
          Label7: TLabel;
          Label10: TLabel;
          EDDataScadLicenza: TEdit;
          EDNumClient: TEdit;
          EDNumAnag: TEdit;
          EDDataScadCanone: TEdit;
          EDCliente: TEdit;
          Panel2: TPanel;
          Image2: TImage;
          Label11: TLabel;
          URLLabel1: TURLLabel;
          Image1: TImage;
          Label12: TLabel;
          Label13: TLabel;
          URLLabel2: TURLLabel;
          Panel4: TPanel;
          ToolbarButton971: TToolbarButton97;
          Label1: TLabel;
          Label8: TLabel;
          MemData1: TdxMemData;
          MemData1NomeModulo: TStringField;
          MemData1NomeFunzione: TStringField;
          MemData1Attivo: TBooleanField;
          MemData1LinkModulo: TStringField;
          MemData1DataScadenzaFunz: TStringField;
          DS1: TDataSource;
          LbRSASSA1: TLbRSASSA;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1RecId: TdxDBGridColumn;
          dxDBGrid1NomeModulo: TdxDBGridMaskColumn;
          dxDBGrid1NomeFunzione: TdxDBGridMaskColumn;
          dxDBGrid1Attivo: TdxDBGridCheckColumn;
          dxDBGrid1LinkModulo: TdxDBGridMaskColumn;
          dxDBGrid1DataScadenzaFunz: TdxDBGridMaskColumn;
    Label2: TLabel;
          procedure FormShow(Sender: TObject);
          procedure LbRSASSA1GetSignature(Sender: TObject;
               var Sig: TRSASignatureBlock);
          procedure ToolbarButton971Click(Sender: TObject);
     private
    { Private declarations }
          xguid: string;
          procedure ParsaConfigFile;
          procedure Riempimemory;

     public
    { Public declarations }
          xFirma: string;
     end;

var
     AboutForm: TAboutForm;

implementation

{$R *.DFM}

procedure TAboutForm.FormShow(Sender: TObject);
begin
     ParsaConfigFile;

     Riempimemory;
end;

procedure TAboutForm.ParsaConfigFile;
var
     xNomeCliente: string;
     xFile, xTestoDaVerif: TstringList;
     xNumMaxSoggetti, xNumClient, xNumrighe, i, n, k, nClient: integer;
     xDataScadenza, s, xNomeFunzione, xLink: string;

     XMLDocument1: IXMLDOMDocument;
     ANode: IXMLDOMNode;
     nodes_se: IXMLDomNodeList;
begin
   // controllo firma
     xFile := TStringList.Create;
     xTestoDaVerif := TStringList.Create;
     xFile.LoadFromFile(ExtractFileDir(Application.ExeName) + '\ConfigFile.xml');
     xNumrighe := xfile.Count;

  //  showmessage('l= '+inttostr(length(xfile.strings[xNumrighe-1])));
     xFirma := Copy(xfile.strings[xNumrighe - 1], 5, length(xfile.strings[xNumrighe - 1]) - 7);

  // showmessage(xSignature);


     for i := 0 to xFile.Count - 2 do
          xTestoDaVerif.Add(xFile.strings[i]);

        //  showmessage(xTestoDaVerif.text);

     LbRSASSA1.PublicKey.LoadFromFile(ExtractFilePath(Application.ExeName) + '\RSASSAPublic.txt');


     if LbRSASSA1.VerifyString(xTestoDaVerif.Text) = false then begin
          ShowMessage('Esito verifica = ERRORE Licenza Non valida');
          exit;
     end;

     XMLDocument1 := CoDOMDocument.create;
     XMLDocument1.loadXML(WideString(xFile.text));

     xFile.Free;
     xTestoDaVerif.Free;
  // fine controllo firma




     anode := XMLDocument1.documentElement.selectSingleNode('//IntestatarioLicenza/Software/LicenzaParametri/@DataScadenza');
     EDDataScadLicenza.text := anode.text;

     anode := XMLDocument1.documentElement.selectSingleNode('//IntestatarioLicenza/@NomeCliente');
     EDCliente.text := anode.text;

     anode := XMLDocument1.documentElement.selectSingleNode('//IntestatarioLicenza/Software/LicenzaParametri/@NumSoggettiAnag');
     EDNumAnag.text := anode.text;

         // anode := XMLDocument1.documentElement.selectSingleNode('//IntestatarioLicenza/Software/LicenzaParametri/@Distributore');
        //  xDistributore := anode.text;

         // anode := XMLDocument1.documentElement.selectSingleNode('//IntestatarioLicenza/@DataGenerazione');
         // xDataGenerazLicenza := StrToDateTime(anode.text);

     anode := XMLDocument1.documentElement.selectSingleNode('//IntestatarioLicenza/Software/LicenzaParametri/@NumSoggettiAnag');
     xNumMaxSoggetti := strtoint(anode.text);
     if xNumMaxSoggetti = 0 then
          EDNumAnag.text := 'illimitati'
     else
          EDNumAnag.text := anode.text;


     anode := XMLDocument1.documentElement.selectSingleNode('//IntestatarioLicenza/Software/LicenzaParametri/@Numclient');
     if anode.text = '0' then
          EDNumClient.text := 'illimitati'
     else
          EDNumClient.text := anode.text;



     anode := XMLDocument1.documentElement.selectSingleNode('//IntestatarioLicenza/Software/LicenzaParametri/@GuidLicenza');
     xguid := anode.text;

     anode := XMLDocument1.documentElement.selectSingleNode('//IntestatarioLicenza/Software/LicenzaParametri/@ScadenzaCanone');
     EDDataScadCanone.text := anode.text;


    //machineID
     anode := XMLDocument1.documentElement.selectSingleNode('//IntestatarioLicenza/Software/LicenzaParametri/@Numclient');
     xNumClient := strtoint(anode.text);
     if xNumClient > 0 then begin

          nodes_se := XMLDocument1.selectNodes('//IntestatarioLicenza/Software/LicenzaParametri/MachineID/Client');
          nClient := nodes_se.length;
          for k := 0 to nClient - 1 do begin
               anode := nodes_se.item[k];
               memo2.Lines.Add(anode.text);
          end;
     end;




end;

procedure TAboutForm.LbRSASSA1GetSignature(Sender: TObject;
     var Sig: TRSASignatureBlock);
begin
     HexToBuffer(xFirma, Sig, SizeOf(Sig));
end;

procedure TAboutForm.ToolbarButton971Click(Sender: TObject);
begin
     if xguid <> '-1' then begin
          ShellExecute(Application.Handle, 'open',
               pchar('http://h1web.ebcconsulting.com/ServizioClienti/Default.aspx?guid=' + xguid),
               nil, nil, SW_SHOWNORMAL);
     end;
end;

procedure TAboutForm.Riempimemory;
var XMLDocument1: IXMLDOMDocument;
     ANode, anodetemp: IXMLDOMNode;
     nodes_se: IXMLDomNodeList;
     xFile: tStringList;
     n, k: integer;
     xNomeFunzione, xNomeModulo, xAttivo, xDataScadenzaFunz: string;
begin

     xFile := TStringList.Create;
     xFile.LoadFromFile(ExtractFileDir(Application.ExeName) + '\ConfigFile.xml');
     XMLDocument1 := CoDOMDocument.create;
     XMLDocument1.loadXML(WideString(xFile.text));
     xfile.free;


     nodes_se := XMLDocument1.selectNodes('//IntestatarioLicenza/Software/LicenzaParametri/FunzioniModuli/Funzione');
     n := nodes_se.length;

    // anode := XMLDocument1.documentElement.selectSingleNode('//IntestatarioLicenza/Software/LicenzaParametri/@DataScadenza');
   //  EDDataScadLicenza.text := anode.text;



     MemData1.Open;
     for k := 0 to n - 1 do begin
          anodetemp := nodes_se.item[k].selectSingleNode('@NomeFunzione');
          xNomeFunzione := anodetemp.text;

          anodetemp := nodes_se.item[k].selectSingleNode('@Modulo');
          xNomeModulo := anodetemp.text;

          anodetemp := nodes_se.item[k].selectSingleNode('@Attivo');
          xAttivo := anodetemp.text;

          anodetemp := nodes_se.item[k].selectSingleNode('@DataScadenzaFunz');
          xDataScadenzaFunz := anodetemp.text;

          MemData1.Insert;
          MemData1NomeFunzione.Value := xNomeFunzione;
          MemData1NomeModulo.Value := xNomeModulo;
          if xAttivo = '1' then
               MemData1Attivo.Value := true
          else
               MemData1Attivo.Value := false;


          if xDataScadenzaFunz <> '' then
               MemData1DataScadenzaFunz.Value := xDataScadenzaFunz
          else
               MemData1DataScadenzaFunz.Value := '';


          MemData1.post;


        //  showmessage(s);

     end;

end;

end.

