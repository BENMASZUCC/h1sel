�
 TINSEVENTOFORM 0�!  TPF0TInsEventoFormInsEventoFormLeftMToprBorderStylebsDialogCaptionInserimento EventoClientHeight�ClientWidthOColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnCreate
FormCreateOnShowFormShowPixelsPerInch`
TextHeight TLabelLabel1LeftTop7Width>HeightCaptionData evento:  TLabelLabel2LeftTopLWidthsHeightCaptionDescrizione/Annotazioni  TDateEdit97
DEData_oldLeftITop3WidthqHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder ColorCalendar.ColorValidclBlueDayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanClear
doCanPopup
doIsMaskeddoShowCanceldoShowToday   TPanelPanel19LeftTopzWidthEHeight	AlignmenttaLeftJustifyCaption  Elenco eventiColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TEditEDescLeftTop\WidthEHeight	MaxLength TabOrder  TDBGridDBGrid2LeftTopvWidthHeight� 
DataSourceDsUsersOptionsdgTitlesdgIndicatordgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgConfirmDeletedgCancelOnExit TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldName
NominativoWidth^Visible	 Expanded	FieldNameDescrizioneWidth� Visible	    TPanelPanel2LeftTop`WidthHeight	AlignmenttaLeftJustifyCaption-  Utente (tra quelli abilitati e non scaduti)ColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  	TdxDBGrid	dxDBGrid1LeftTop� WidthDHeight� Bands  DefaultLayout	HeaderPanelRowCountKeyFieldIDShowGroupPanel	SummaryGroups SummarySeparator, TabOrder
DataSourceDsEventiFilter.Criteria
       OptionsBehavioredgoAutoSortedgoDragScrolledgoEnterShowEditoredgoImmediateEditoredgoTabThroughedgoVertThrough 	OptionsDBedgoCancelOnExitedgoCanDeleteedgoCanInsertedgoCanNavigationedgoConfirmDeleteedgoLoadAllRecordsedgoUseBookmarks OptionsViewedgoAutoWidthedgoBandHeaderWidthedgoIndicatoredgoUseBitmap  TdxDBGridLookupColumndxDBGrid1DaStatoCaptionDallo statoSortedcsUpWidth� 	BandIndex RowIndex 	FieldNameDaStato  TdxDBGridMaskColumndxDBGrid1EventoWidth� 	BandIndex RowIndex 	FieldNameEvento  TdxDBGridLookupColumndxDBGrid1AStatoCaption
Allo statoWidth� 	BandIndex RowIndex 	FieldNameAStato   TJvDateTimePickerDEDataLeftJTop4WidthnHeightCalAlignmentdtaLeftDate �J��@Time �J��@
DateFormatdfShortDateMode
dmComboBoxKinddtkDate
ParseInputTabOrderDropDownDate      �@  TPanelPanel1Left�TopWidth� Height,
BevelOuterbvNoneTabOrder TToolbarButton97BOkLeftTopWidthPHeight&CaptionOK
Glyph.Data
z  v  BMv      6  (               @                     ��� � � @�h ��� TQI �? v�v ��� ('' yґ E�^ qm^ ��� $�M ��� _�w \� ��� ��� �ټ \�d =<9 v�� 4�N :�] |{s T�s _^^ ��x ��� ,�X �Ʀ b�s jɃ ��� I�u ��� O�j ��� i|m ��� ��� *�M ��� ��� ��� 421 D�f S�` �� 7�^ JIC 9�U r� |�� _wc nnm U�l k� ��� ��� 0�W J�p ��� f�l `�l wċ %�G � �° T�q ��� e�y ��p ��� V�y qЊ [XN [�y ��� ��� M�n �Ė ��� 2�T urq d�} =�[ A�` ��� ��� ��� ��� ��� ��� F�p ��� H�c �Ǔ ��� m�v n� dЁ N�u C�h x�� V�o *�R �� ��� ?�b ~~{ o�p 2�P ��� �̹ 1�Z ��� H�_ ��� ;�c Ļ� g�o w͎ ��� U�z O�g ��� ]�r r�� ��� ��� \�o ��� c`a 0�V M�p F�f [�w �ȶ ��� 8�Y I�[ ��� ��u ��� sɉ 3�] ��� Y�o ��� ��� 3�Z �á mą i�z @�j Q�u [YY I�m ��� ��� ��� ��� (�Q ��� ?�c Z�~ ��� e� {�z b�s ��� IIH B�a yvu ��� l͆ ��� p�� t�� �ϼ Ŀ� +�V <�X J�k ��� |�� ��� h�~ <�a I�s 11/ ��� ��� E�n O�r k�m ��} ��� _�y e�w ��� ;�d S�y cxf ��� ��� |̑ ɿ� 7�Z =�] e|j ��� ��� Q�l }�� zxp ��� {�� ��� ��� &�K +�O L�a ��� ��� Y�t �Ɵ b�x ��� ��� ��� U�v ��� S�n \�v |Ǐ ��� ��� �˽ ��� ��� VTK ��� �ɼ ƽ� 7�_ A�j B�] ��� W�| ��� ��� ��� .9��		��9.�/Pz��z�/�.�V}��uu���}���4��>?����?�>E_4p�����?ih~�L�����=<N�uy�0�`�h��L�u�%N�s��3yo�n��$��?L���d�<+�3�nnn�`���ŧ��s��l���nn�nףR�yy�Rl@���vB�U­n�HQ�'���y3�R�ґJ#5����Q\�''���3�0�#J���ח�uy�\�''����ӯD��t��G�˥333uޤ'���l�&rF2��wk��O�>��3��㗗�XY�^���{�W"MgR�>>,��-�b1�F��澛�|


M���-���F���!I;�C����|���Z]��А���q��j�cccc�C�;�:����[�A��*SSSS*��;I��%���a�A��������fʜЕ���_�� ����m68���x}=(7��Te�=}x���K�.))ۍK����۱��.۱�OnClickBOkClick  TToolbarButton97BAnnullaLeft]TopWidthPHeight&CaptionAnnulla
Glyph.Data
z  v  BMv      6  (               @                     ��� � � ��} � Ze� ��� =<: ��� ��� OO� ��� ba] 47� ��� ef� bb� ��~ �û xx� ~�� ((' ��� 76� NN� ��� ��� ��� SRI tt� mmm %%� LS� ��� ��� cb� ��w cc� CF� XX� ?G� VV� ��� ��� ��� �к ��� 330 ml� ��� ��� ||� ��� ��� HHH {zs pn] -.� (*� jl� aa� Z[� ��� ��� ��� oo� ��o sr� VZ� ��� ��� ��� CC� ��� ��� ||� ��� 38� MM� 00� �ŭ ss� ��� T^� QQ� ii� -2� GK� ��� ��� ��� ��� 4;� __� YZW ``� II� w|� ��� ��� ��� ;>� ��� ��� JJ� YWM ��� VV� ��� \\� ��� ��� dm� ��� ��� nn� )-� ##� ��� ��� ��� �˻ AF� QW� @@� qq� -/� utm ??� II� NN� ��� ��� 9:� ��� ��� Ya� ll� 33� ��� ��� ~~� �ջ ))� ��� ��� ��� ��� ��� ��� ��� ��� hg� xw� ��� ��� BD� ��� KT� ��� `h� nn� ��� �Ǽ DG� NO� TZ� w{� JIB [[� ��� ��� ��t bb� ��� BJ� QQ� __� ||� ��� 9>� ��� ��� FE� ff� ��� 66� 00/ ��� �ٽ ��� //� �Ƿ \\[ �ö 99� ~~{ ww� ��� EF� NV� II� ��� QQ� ^^� ��� zz� 44� ��� ]d� SU� V]� ��� SS� ~~� �͸ 68� ��� ��� aa� ��� {{� ��� EJ� ��� ZZ� ^[� 8;� 8=� AK� ��� ��� ��� <;6 ��� 35� 55� 58� GG� YY� [^� bb� jj� rr� ��� ��� ��� VTK ))� ++� 01� yxn ��� ``� ��� ��� 6��6��/joo�/^R���M��M=+�7��ތ�ߦ��ш�z�+���!�_� �ӈ�pp�Ѥt�����i��V&��SSS��D���qiE7X?~�n�a�SS ����~����8���2��4�SS�������8E��J��;gn>Z SS�c�gW��9fL��*�ϊ~�n�{�(.�g㜥��P�Bh�����4\���e��|B$-ðh�إg��4�ԃ�e؊�Hu�$�׀��T��D�nnnn��؊��������=%�:�nnn����9�H�O����#k̭'�>�n��n�'���Ɂ
-�rP�__���>"5>�d`���CPr[,�]_������}�g��w��`�v�ry0�K�1���s��GN�y�[��<�K���������)q�[�	�<lxl���x�Q���)�������b�+lx�QQAm@��[�F���b+3U�F��[�ʹYIIY�ʚ[[E����[OnClickBAnnullaClick   TDataSourceDsEventiDataSetTEventiLeft4Top�   TDataSourceDsUsersDataSetQUsersLeft?Top�  TADOLinkedTableTEventi
ConnectionData.DB
CursorTypectStatic	IndexName
IdxDaStato	TableName
EBC_EventiLeftTop�  TAutoIncField	TEventiID	FieldNameIDReadOnly	  TIntegerFieldTEventiIDdaStato	FieldName	IDdaStato  TStringFieldTEventiEvento	FieldNameEventoSize  TIntegerFieldTEventiIDaStato	FieldNameIDaStato  TBooleanFieldTEventiNonCancellabile	FieldNameNonCancellabile  TIntegerFieldTEventiIDProcedura	FieldNameIDProcedura  TStringFieldTEventiDaStato	FieldKindfkLookup	FieldNameDaStatoLookupDataSetTStatiLKLookupKeyFieldsIDLookupResultFieldStato	KeyFields	IDdaStatoSizeLookup	  TStringFieldTEventiAStato	FieldKindfkLookup	FieldNameAStatoLookupDataSetTStatiLKLookupKeyFieldsIDLookupResultFieldStato	KeyFieldsIDaStatoSizeLookup	   TADOLinkedTableTStatiLK
ConnectionData.DB
CursorTypectStatic	TableName	EBC_StatiLeftxTop�  TAutoIncField
TStatiLKID	FieldNameIDReadOnly	  TStringFieldTStatiLKStato	FieldNameStatoSize  TIntegerFieldTStatiLKIDTipoStato	FieldNameIDTipoStato  TBooleanFieldTStatiLKPRman	FieldNamePRman  TIntegerFieldTStatiLKIDAzienda	FieldName	IDAzienda   TADOLinkedQueryQUsers
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsset dateformat dmy select ID,Nominativo,Descrizionefrom Users where DataScadenza>=getdate()   or DataRevoca>=getdate()1   or DataCreazione is null or datarevoca is nullorder by Nominativo   	UseFilterLeft`Top�   