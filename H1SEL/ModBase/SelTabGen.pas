//TONI OK 30/4
unit SelTabGen;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, Grids, DBGrids, Db, DBTables, ExtCtrls, GridDB, dxCntner,
     dxTL, dxDBCtrl, dxDBGrid, ADODB, TB97;

type
     TSelTabGenForm = class(TForm)
          FrameDB1: TFrameDB;
          Panel3: TPanel;
          Panel4: TPanel;
          Panel2: TPanel;
          Panel5: TPanel;
          QTab: TADOQuery;
          Label1: TLabel;
          BitBtn1: TToolbarButton97;
          BitBtn2: TToolbarButton97;
          procedure QTab_AfterOpen(DataSet: TDataSet);
          procedure FormShow(Sender: TObject);
          procedure FormCreate(Sender: TObject);
          procedure FrameDB1BInsClick(Sender: TObject);
          procedure FrameDB1BModClick(Sender: TObject);
          procedure FrameDB1BDelClick(Sender: TObject);
          procedure BitBtn1Click(Sender: TObject);
          procedure BitBtn2Click(Sender: TObject);
     private
          { Private declarations }
     public
          xFieldName, xFieldTitle, xtable: string;
          xFields: array[1..20] of string;
          xTitles: array[1..10] of string;
          xModify: boolean;
          xCancellabile: boolean;
          xparamGenerico: string;
          n, xAltezzaLabel: integer;
     end;

var
     SelTabGenForm: TSelTabGenForm;

implementation

uses ModuloDati, FrmFrameDB, dxGrClms, Main;

{$R *.DFM}

procedure TSelTabGenForm.QTab_AfterOpen(DataSet: TDataSet);
var i, p: integer;
begin
{    //ORIGINALE
FrameDB1.DBGrid1.Columns[0].Visible := False;
     if xFieldName <> '' then
          FrameDB1.DBGrid1.Columns[1].FieldName := xFieldName;
     if Length(xFields) > 0 then
          i := 1;
     while xFields[i] <> '' do
     begin
          FrameDB1.DBGrid1.Columns[i].FieldName := xfields[i];
          inc(i);
     end;
     if xFieldTitle <> '' then
          FrameDB1.DBGrid1.Columns[1].Title.Caption := xFieldTitle;
     if xTitles[1] <> '' then begin
          for i := 1 to FrameDB1.DBGrid1.Columns.Count - 1 do begin
               FrameDB1.DBGrid1.Columns[i].Title.Caption := xTitles[i];
          end;
     end;}
     if n <> 0 then begin
     // creo le colonne nella griglia in base a quanti sono i campi nella query eseguita
          for p := 0 to QTab.Fields.Count - 1 do begin
          //showmessage(QTab.FieldList.Strings[p]); // stampa nome del campo
               if QTab.FieldList.Fields[p].ClassName = 'TBooleanField' then
                    FrameDB1.DBGrid2.CreateColumn(TdxDBGridCheckColumn)
               else
                    FrameDB1.DBGrid2.CreateColumn(TdxDBTreeListColumn);

               framedb1.dbgrid2.Columns[p].DisableEditor := true;
          //framedb1.DBGrid2.Columns[p].Name := QTab.FieldList.Strings[p];

          //framedb1.DBGrid2.CreateColumnEx(QTab.Fields.ClassType, TdxDBTreeListColumn);
       // FrameDB1.DBGrid2.CreateDefaultColumns(FrameDB1.DataSourceGrid,TdxTreeListNode);
          end;
     end;


     FrameDB1.DBGrid2.Columns[0].Visible := False;
     if xFieldName <> '' then
          FrameDB1.DBGrid2.Columns[1].FieldName := xFieldName;
     if Length(xFields) > 0 then
          i := 1;
     while xFields[i] <> '' do
     begin
          FrameDB1.DBGrid2.Columns[i].FieldName := xfields[i];
          inc(i);
     end;
     if xFieldTitle <> '' then
          FrameDB1.DBGrid2.Columns[1].Caption := xFieldTitle;
     if xTitles[1] <> '' then begin
          for i := 1 to FrameDB1.DBGrid2.ColumnCount - 1 do begin
               FrameDB1.DBGrid2.Columns[i].Caption := xTitles[i];
               if FrameDB1.DBGrid2.Columns[i].Width < 300 then FrameDB1.DBGrid2.Columns[i].Width := 300;
          end;
     end;
     //filtro griglia
     FrameDB1.DBGrid2.KeyField := 'ID';
     FrameDB1.DBGrid2.Filter.Active := true;

end;

procedure TSelTabGenForm.FormShow(Sender: TObject);
var i: integer;
begin

// grafica
     SelTabGenForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel5.Color := mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;
     FrameDB1.PanelControl.Color := mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;

     FrameDB1.DBGrid2.OptionsView := ([edgoBandHeaderWidth, edgoIndicator, edgoUseBitmap, edgoAutoWidth]);
     if not xModify then begin
          FrameDB1.BIns.visible := FAlse;
          FrameDB1.BMod.visible := FAlse;
          FrameDB1.BDel.visible := FAlse;

     end else begin
          FrameDB1.BIns.visible := True;
          FrameDB1.BMod.visible := True;
          FrameDB1.BDel.visible := True;

         // FrameDB1.BIns.Enabled := False;
         // FrameDB1.BMod.Enabled := False;
         // FrameDB1.BDel.Enabled := False;
     end;

     if not xCancellabile then begin
          //Commentato l'insert perch� potrebbe dare problemi avere solo la possibilit� di inserire senza poter cancellare
          FrameDB1.BIns.visible := False;
          FrameDB1.BDel.visible := False;
     end;

     FrameDB1.xAltezzaLabel := xAltezzaLabel;
end;

procedure TSelTabGenForm.FormCreate(Sender: TObject);
var i: integer;
begin
     n := 1;
     for i := 1 to 10 do
          xTitles[i] := '';


end;

procedure TSelTabGenForm.FrameDB1BInsClick(Sender: TObject);
begin
     n := 0;
     FrameDB1.BInsClick(Sender);
end;

procedure TSelTabGenForm.FrameDB1BModClick(Sender: TObject);
begin
     FrameDB1.BModClick(Sender);

end;

procedure TSelTabGenForm.FrameDB1BDelClick(Sender: TObject);
begin
    { if (xparamGenerico = 'TabObiettiviPers') or (xparamGenerico = 'TabPrestazioni') then
     begin
          if CkDeleteObMbo(qtab.fieldbyname('id').asinteger, xparamGenerico) then
               FrameDB1.BDelClick(Sender)
          else
               showmessage('L''Obiettivo selezionato risulta associato ad un Obiettivo Operativo' + #10#13 + 'Impossibile Eliminare');

     end else  }
     FrameDB1.BDelClick(Sender);

end;

procedure TSelTabGenForm.BitBtn1Click(Sender: TObject);
begin
     SelTabGenForm.ModalResult := mrOk;
end;

procedure TSelTabGenForm.BitBtn2Click(Sender: TObject);
begin
     SelTabGenForm.ModalResult := mrCancel;
end;

end.

