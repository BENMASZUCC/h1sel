object FrameWebLinks: TFrameWebLinks
  Left = 0
  Top = 0
  Width = 475
  Height = 355
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  ParentFont = False
  TabOrder = 0
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 475
    Height = 41
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object BCan: TToolbarButton97
      Left = 80
      Top = 1
      Width = 77
      Height = 39
      Caption = 'Annulla modifiche'
      Enabled = False
      Opaque = False
      WordWrap = True
      OnClick = BCanClick
    end
    object BOK: TToolbarButton97
      Left = 3
      Top = 1
      Width = 77
      Height = 39
      Caption = 'Conferma modifiche'
      Enabled = False
      Opaque = False
      WordWrap = True
      OnClick = BOKClick
    end
    object SpeedButton1: TToolbarButton97
      Left = 376
      Top = 1
      Width = 98
      Height = 38
      Caption = 'Gestione Siti'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Glyph.Data = {
        36050000424D3605000000000000360400002800000010000000100000000100
        08000000000000010000120B0000120B00000001000000010000000000009C63
        00009C633100FF9C3100525252009C6363009C9C6300FF9C6300A5A5A500F7CE
        A500FFCECE00FFEFCE009CFFCE00FFFFCE00F7FFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000F0F0F000000
        0F0F0F0F0F0F0F0F0F0F0F0F00060D000004040404040404040F0F0F00060700
        0600090909090909040F0F0F0F0000000D06000909090909040F0F0F0F04000F
        0A0D060009090909040F0F0F0F000F0E0F0A0D0600090909040F0F0F000F0E09
        0E0F0A0C07000909040F0F000F0D0A0E090E0F0A0F000909040F00060A000D01
        0E0A0A0F0F000A0A040F000206000006010D0B07000B0A0A040F0000000D0200
        06020700020B0B0B040F0F000F0F0D02000600070B0B0B0B040F0F0F000F0F0A
        06000B0B0B0B0808040F0F0F0F000F0F0A02000B0B0B0503040F0F0F0F04000F
        0F0D000B0B0B02040F0F0F0F0F040400000004040404040F0F0F}
      Opaque = False
      ParentFont = False
      WordWrap = True
      OnClick = SpeedButton1Click
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 41
    Width = 475
    Height = 314
    Align = alClient
    BevelOuter = bvLowered
    TabOrder = 1
    object dxDBGrid1: TdxDBGrid
      Left = 1
      Top = 1
      Width = 473
      Height = 312
      Bands = <
        item
        end>
      DefaultLayout = True
      HeaderPanelRowCount = 1
      KeyField = 'ID'
      SummaryGroups = <>
      SummarySeparator = ', '
      Align = alClient
      TabOrder = 0
      DataSource = DsQWebLinks
      DefaultRowHeight = 35
      Filter.Criteria = {00000000}
      OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
      OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoIndicator, edgoUseBitmap]
      object dxDBGrid1ID: TdxDBGridMaskColumn
        Visible = False
        Width = 650
        BandIndex = 0
        RowIndex = 0
        FieldName = 'ID'
      end
      object dxDBGrid1Column4: TdxDBGridGraphicColumn
        Alignment = taCenter
        Caption = 'Sito'
        Width = 116
        BandIndex = 0
        RowIndex = 0
        FieldName = 'Logo'
        CustomGraphic = True
        DblClickActivate = False
        QuickDraw = True
        ShadowSelection = False
        OnGetGraphicClass = dxDBGrid1Column4GetGraphicClass
      end
      object dxDBGrid1Descrizione: TdxDBGridMaskColumn
        Alignment = taLeftJustify
        VertAlignment = tlCenter
        Visible = False
        Width = 167
        BandIndex = 0
        RowIndex = 0
        FieldName = 'Descrizione'
      end
      object dxDBGrid1Column5: TdxDBGridHyperLinkColumn
        VertAlignment = tlCenter
        Width = 341
        BandIndex = 0
        RowIndex = 0
        FieldName = 'Link'
      end
    end
  end
  object DsQWebLinks: TDataSource
    DataSet = QWebLinks
    OnStateChange = DsQWebLinksStateChange
    Left = 64
    Top = 169
  end
  object QWebLinks: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    BeforeOpen = QWebLinksBeforeOpen
    Parameters = <
      item
        Name = 'x'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 1
      end>
    SQL.Strings = (
      'select *'
      'from AnagWebLinks AWL'
      'join WebLinks WL on AWL.IDWebLink = WL.ID'
      'where AWL.IDAnagrafica=:x')
    Left = 64
    Top = 137
    object QWebLinksID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QWebLinksDescrizione: TStringField
      FieldName = 'Descrizione'
      Size = 1024
    end
    object QWebLinksLogo: TBlobField
      FieldName = 'Logo'
      BlobType = ftBlob
    end
    object QWebLinksLink: TStringField
      DisplayWidth = 512
      FieldName = 'Link'
      Size = 512
    end
  end
  object OpenDialog1: TOpenDialog
    Left = 40
    Top = 225
  end
end
