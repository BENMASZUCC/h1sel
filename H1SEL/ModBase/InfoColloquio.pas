unit InfoColloquio;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, ExtCtrls, Db, dxDBTLCl, dxGrClms, dxTL, dxDBCtrl,
     dxDBGrid, dxCntner, ComCtrls, ADODB, U_ADOLinkCl, Mask, DBCtrls,
     dxEditor, dxExEdtr, dxExGrEd, dxExELib, dxLayout, Grids, AdvGrid, DtEdit97,
     DBGrids, Spin, TB97, ImgList, Menus, kbmMemTable, BaseGrid;

type
     TInfoColloquioForm = class(TForm)
          Panel1: TPanel;
          DsQEspLavRetribuz: TDataSource;
          PCUnico: TPageControl;
          TSDatiRetribuz: TTabSheet;
          Panel3: TPanel;
          Panel12: TPanel;
          DsQEspLavAttuale: TDataSource;
          QEspLavAttuale: TADOLinkedQuery;
          QEspLavAttualeAzienda: TStringField;
          QEspLavAttualeAttivita: TStringField;
          QEspLavAttualeID: TAutoIncField;
          QEspLavAttualeIDMansione: TIntegerField;
          QEspLavAttualeAnnoDal: TSmallintField;
          QEspLavAttualeMeseDal: TSmallintField;
          Label1: TLabel;
          DBEdit1: TDBEdit;
          Label2: TLabel;
          DBEdit2: TDBEdit;
          Label3: TLabel;
          DBEdit3: TDBEdit;
          Label4: TLabel;
          DBEdit4: TDBEdit;
          Label5: TLabel;
          TSMotIns: TTabSheet;
          TSCheckList: TTabSheet;
          DsQRuoli: TDataSource;
          QRuoli: TADOLinkedQuery;
          dxDBGridLayoutList1: TdxDBGridLayoutList;
          dxDBGridLayoutListRuoli: TdxDBGridLayout;
          dxDBExtLookupEdit1: TdxDBExtLookupEdit;
          QRuoliID: TAutoIncField;
          QRuoliRuolo: TStringField;
          QRuoliArea: TStringField;
          QEspLavAttualeRuolo: TStringField;
          Label6: TLabel;
          Label7: TLabel;
          Label8: TLabel;
          ASG1: TAdvStringGrid;
          QGriglia: TADOLinkedQuery;
          QUpd: TADOLinkedQuery;
          Q: TADOLinkedQuery;
          QRuoliIDArea: TAutoIncField;
          QEspLavDB: TADOLinkedQuery;
          DsQCLColloquioTemp: TDataSource;
          dxDBGrid2: TdxDBGrid;
          dxDBGrid2Voce: TdxDBGridMaskColumn;
          dxDBGrid2Flag: TdxDBGridCheckColumn;
          dxDBGrid2Risposta: TdxDBGridMaskColumn;
          Panel5: TPanel;
          Label9: TLabel;
          ECogn: TEdit;
          ENome: TEdit;
          TSEsito: TTabSheet;
          TSelezionatori_OLD: TADOLinkedTable;
          DsSelezionatori: TDataSource;
          Label10: TLabel;
          Label11: TLabel;
          Panel6: TPanel;
          Label12: TLabel;
          Label13: TLabel;
          Label14: TLabel;
          CBEsito: TComboBox;
          CBGiudizio: TComboBox;
          SEPunteggio: TSpinEdit;
          DataColloquio: TDateEdit97;
          Panel2: TPanel;
          Label16: TLabel;
          ToolbarButton971: TToolbarButton97;
          PMFunzioniCand: TPopupMenu;
          Schedacandidato1: TMenuItem;
          Curriculum1: TMenuItem;
          NuovaEsperienzalavorativaattuale1: TMenuItem;
          Valutazionecandidato1: TMenuItem;
          N2: TMenuItem;
          Documenti1: TMenuItem;
          AppuntiWord1: TMenuItem;
          ImageList1: TImageList;
          CBFlagAccPres: TCheckBox;
          QQualifContr: TADOLinkedQuery;
          DsQQualifContr: TDataSource;
          dxDBGridLayoutList1QualifContr: TdxDBGridLayout;
          QEspLavAttualeIDQualifContr: TIntegerField;
          QQualifContrID: TAutoIncField;
          QQualifContrTipoContratto: TStringField;
          QQualifContrQualifica: TStringField;
          QQualifContrContrattoNaz: TStringField;
          QQualifContrTipoQualif: TStringField;
          QEspLavAttualeContrattoNaz: TStringField;
          DBEdit5: TDBEdit;
          QEspLavAttualeTipoQualif: TStringField;
          dxDBExtLookupEdit2: TdxDBExtLookupEdit;
          N1: TMenuItem;
          Tabellaqualificheecontratti1: TMenuItem;
          Tabellamoticidiinsoddisfazione1: TMenuItem;
          QEspLavRetribuz: TkbmMemTable;
          QEspLavRetribuzID: TIntegerField;
          QEspLavRetribuzVoce: TStringField;
          QEspLavRetribuzFlag: TBooleanField;
          QEspLavRetribuzValore: TFloatField;
          QEspLavRetribuzIDColonna: TIntegerField;
          QEspLavDBID: TAutoIncField;
          QEspLavDBVoce: TStringField;
          QEspLavDBValore: TFloatField;
          QEspLavDBFlag: TBooleanField;
          QEspLavDBIDColonna: TAutoIncField;
          QCLColloquioTemp: TkbmMemTable;
          QCLColloquioTempIDCommessaVoce: TIntegerField;
          QCLColloquioTempVoce: TStringField;
          QCLColloquioTempFlag: TBooleanField;
          QCLColloquioTempRisposta: TStringField;
          QCLColloquioTempID: TIntegerField;
          Q2: TADOLinkedQuery;
          Tabella1: TMenuItem;
          Q6: TADOQuery;
          eMotivoElim: TEdit;
          Label15: TLabel;
          GBColloquioAzienda: TGroupBox;
          CBColloquioCliente: TCheckBox;
          CBDettCollCliente: TComboBox;
          ToolbarButton9711: TToolbarButton97;
          Panel4: TPanel;
          BOK: TToolbarButton97;
          BAnnulla: TToolbarButton97;
          Panel7: TPanel;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1ID: TdxDBGridMaskColumn;
          dxDBGrid1Voce: TdxDBGridMaskColumn;
          dxDBGrid1Flag: TdxDBGridCheckColumn;
          dxDBGrid1Valore: TdxDBGridMaskColumn;
          Panel8: TPanel;
          Panel9: TPanel;
          GBCliente: TGroupBox;
          DBEdit6: TDBEdit;
          GroupBox7: TGroupBox;
          DBEdit7: TDBEdit;
          GroupBox3: TGroupBox;
          Label21: TLabel;
          DBEdit8: TDBEdit;
          DBEdit9: TDBEdit;
          DBEdit13: TDBEdit;
          QRicPend: TADOQuery;
          DSQRicPend: TDataSource;
          QRicPendProgressivo: TStringField;
          QRicPendMansione: TStringField;
          QRicPendCliente: TStringField;
          QRicPendRuolo: TStringField;
          QRicPendArea: TStringField;
          QRicPendTitoloCliente: TStringField;
          DBEdit10: TDBEdit;
          DBEdit11: TDBEdit;
          TSelezionatori: TADOQuery;
          TSelezionatoriID: TAutoIncField;
          TSelezionatoriNominativo: TStringField;
          CBSelezionatori: TComboBox;
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
          procedure FormShow(Sender: TObject);
          procedure ASG1GetAlignment(Sender: TObject; ARow, ACol: Integer;
               var AAlignment: TAlignment);
          procedure QEspLavAttualeAfterPost(DataSet: TDataSet);
          procedure ASG1RightClickCell(Sender: TObject; Arow, Acol: Integer);
          procedure FormDestroy(Sender: TObject);
          procedure FormCreate(Sender: TObject);
          procedure ASG1CheckBoxClick(Sender: TObject; aCol, aRow: Integer;
               state: Boolean);
          procedure NuovaEsperienzalavorativaattuale1Click(Sender: TObject);
          procedure Valutazionecandidato1Click(Sender: TObject);
          procedure Schedacandidato1Click(Sender: TObject);
          procedure Curriculum1Click(Sender: TObject);
          procedure Documenti1Click(Sender: TObject);
          procedure AppuntiWord1Click(Sender: TObject);
          procedure Tabellamoticidiinsoddisfazione1Click(Sender: TObject);
          procedure Tabellaqualificheecontratti1Click(Sender: TObject);
          procedure Tabella1Click(Sender: TObject);
          procedure CBEsitoChange(Sender: TObject);
          procedure ToolbarButton9711Click(Sender: TObject);
          procedure CBColloquioClienteClick(Sender: TObject);
          procedure Schedavalutazione1Click(Sender: TObject);
          procedure BOKClick(Sender: TObject);
          procedure BAnnullaClick(Sender: TObject);
          procedure CBSelezionatoriChange(Sender: TObject);
     private
          procedure ComponiGriglia;
          procedure SalvaGriglia;
          procedure CreaTabellaTemporanea;
          procedure NascondiFunzioni;
     public
          xIDAnagrafica, xIDRicerca, xIDCandRic: integer;
          procedure SalvaCLColloquio(xIDColloquio: integer);
          function NuovaEspLavAttuale: boolean;
          procedure RiempiDBDettColloquio;
     end;

var
     InfoColloquioForm: TInfoColloquioForm;

implementation

uses ModuloDati, SelCliente, ValutazColloquio, SelEspLav,
     GestEspLavMotInsModel, ValutazCollDett, SchedaCand, Curriculum,
     uUtilsVarie, GestQualifContr, Main, TabContratti;

{$R *.DFM}

procedure TInfoColloquioForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     if (QEspLavAttuale.active) and (not QEspLavAttuale.IsEmpty) then begin
          // salvataggio dati (se in edit)
          if dsQEspLavAttuale.state = dsEdit then QEspLavAttuale.Post;
          if dsQEspLavRetribuz.state = dsEdit then QEspLavRetribuz.Post;
          if DsQCLColloquioTemp.state = dsEdit then QCLColloquioTemp.Post;
          // registrazione dalla tabella temporanea a quella definitiva
          QEspLavRetribuz.First;
          Q.Close;
          while not QEspLavRetribuz.EOF do begin
               if QEspLavRetribuzID.asString <> '0' then
                    Q.SQL.text := 'update EspLavRetribuzioni set Valore=:xValore:,Flag=:xFlag: ' +
                         ' where ID=' + QEspLavRetribuzID.asString
               else begin
                    Q.SQL.text := 'insert into EspLavRetribuzioni (IDEspLav,IDColonna,Valore,Flag,ConsideraTot) ' +
                         ' values (:xIDEspLav:,:xIDColonna:,:xValore:,:xFlag:,:xConsideraTot:)';
                    Q.ParamByName['xIDEspLav'] := QEspLavAttualeID.Value;
                    Q.ParamByName['xIDColonna'] := QEspLavRetribuzIDColonna.Value;
                    Q.ParamByName['xConsideraTot'] := True;
               end;
               Q.ParamByName['xValore'] := QEspLavRetribuzValore.Value;
               Q.ParamByName['xFlag'] := QEspLavRetribuzFlag.Value;
               Q.ExecSQL;

               QEspLavRetribuz.Next;
          end;
          QEspLavRetribuz.Close;

          // salvataggio motivi insoddisfazione
          SalvaGriglia;
     end;
end;

procedure TInfoColloquioForm.FormShow(Sender: TObject);
var i: integer;
begin
     //Grafica
     InfoColloquioForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel3.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel3.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel3.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel4.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel4.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel4.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel5.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel5.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel5.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel6.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel6.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel6.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel7.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel7.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel7.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel8.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel8.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel8.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel9.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel9.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel9.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel12.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel12.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel12.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     TSMotIns.TabVisible:=false; //fede 29 agosto 2018..cavato via dopo aver parlato con Zorro..non salva da nessuna parte non serve a un cazzo

     if (QEspLavAttuale.active) and (not QEspLavAttuale.IsEmpty) then begin
          dxDBGrid1.visible := True;
          panel7.Enabled := True;
     end else begin
          dxDBGrid1.visible := False;
          panel7.caption := 'Non � presente alcun Esperienza Lavorativa Attuale.';
          panel7.Enabled := False;
          panel3.Enabled := False;
     end;

     Caption := '[E/6b] - ' + Caption;

     if pos('INTERMEDIA', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) > 0 then begin
          label7.visible := false;
          label8.visible := false;
          dbedit5.visible := false;
          dxDBExtLookupEdit2.visible := false;
     end;

     // nome e cognome soggetto
     Q.Close;
     Q.SQL.text := 'select Cognome,Nome from Anagrafica where ID=' + IntToStr(xIDAnagrafica);
     Q.Open;
     ECogn.Text := Q.FieldByName('Cognome').asString;
     ENome.Text := Q.FieldByName('Nome').asString;
     Q.Close;

     CreaTabellaTemporanea;
     // si posiziona sui dati retributivi
     PCUnico.ActivePageIndex := 0;
     // composizione griglia motivi di insoddisfazione
     ComponiGriglia;

     // Riempimento MemTable Check-list colloquio
     QCLColloquioTemp.Open;
     if xIDRicerca = 0 then begin
          // valori dell'ultimo colloquio
          Q2.Close;
          Q2.SQL.text := 'select IDCommessaVoce,Voce,Flag,Risposta ' +
               'from CLColloquio, CLCommessaVoci where CLColloquio.IDCommessaVoce = CLCommessaVoci.ID ' +
               'and IDColloquio = (select max(ID) from EBC_Colloqui where IDAnagrafica=' + IntToStr(xIDAnagrafica) + ')';
          Q2.Open;
          while not Q2.EOF do begin
               QCLColloquioTemp.InsertRecord([i, Q2.FieldByName('IDCommessaVoce').asInteger, Q2.FieldByName('Voce').asString, Q2.FieldByName('Flag').asboolean, Q2.FieldByName('Risposta').asString]);
               Q2.Next;
          end;
          Q2.Close;

          // non far vedere l'esito colloquio

          //TSEsito.TabVisible := False;
          // rendi read-only la Check-list
          dxDBGrid2Flag.DisableEditor := True;
          dxDBGrid2Risposta.DisableEditor := True;

     end else begin
          Q.Close;
          Q.SQL.text := 'select ID,Voce from CLCommessaVoci where IDRicerca=' + IntToStr(xIDRicerca);
          Q.Open;
          i := 1;
          while not Q.EOF do begin
               QCLColloquioTemp.InsertRecord([i, Q.FieldByName('ID').asInteger, Q.FieldByName('Voce').asString]);
               Q.Next;
               inc(i);
          end;
          Q.Close;
          // flag accettazione
          Q.Close;
          Q.SQL.text := 'select EBC_Clienti.Descrizione Cliente from EBC_Clienti, EBC_Ricerche ' +
               'where EBC_Clienti.ID = EBC_Ricerche.IDCLiente and EBC_Ricerche.ID=' + IntToStr(xIDRicerca);
          Q.Open;
          CBFlagAccPres.caption := 'Accetta di essere presentato all''azienda ' + Q.FieldByName('Cliente').asString;
          Q.Close;
     end;
     QRicPend.Close;
     QRicPend.Parameters.ParamByName('xid').Value := xIDRicerca;
     QRicPend.Open;
     RiempiDBDettColloquio;

     TSelezionatori.Close;
     TSelezionatori.Open;
     CBSelezionatori.items.Clear;
     TSelezionatori.First;
     CBSelezionatori.items.Add('');
     while not TSelezionatori.EOF do begin
          CBSelezionatori.items.Add(TSelezionatori.FieldByName('Nominativo').asString);
          TSelezionatori.Next;
     end;
     InfoColloquioForm.TSelezionatori.Locate('ID', mainForm.xIDUtenteAttuale, []);
     CBSelezionatori.Text := TSelezionatori.FieldByName('Nominativo').asString;

     data.QGiudiziColloquio.Close;
     data.QGiudiziColloquio.Open;
     CBGiudizio.items.Clear;
     data.QGiudiziColloquio.First;
     CBGiudizio.items.Add('');
     while not data.QGiudiziColloquio.EOF do begin
          CBGiudizio.items.Add(data.QGiudiziColloquio.FieldByName('Descrizione').asString);
          data.QGiudiziColloquio.Next;
     end;

end;

procedure TInfoColloquioForm.ComponiGriglia;
var i, k: integer;
begin
     // composizione griglia
     QGriglia.Close;
     QGriglia.ReloadSQL;
     QGriglia.ParamByName['xIDEspLav'] := QEspLavAttualeID.Value;
     QGriglia.Open;
     ASG1.RowCount := QGriglia.RecordCount + 1;
     ASG1.ColCount := 6;
     // altezze righe
     for k := 1 to ASG1.RowCount - 1 do
          ASG1.RowHeights[k] := 20;
     // prima riga: intestazioni
     ASG1.cells[0, 0] := 'voce';
     ASG1.cells[1, 0] := '1';
     ASG1.cells[2, 0] := '2';
     ASG1.cells[3, 0] := '3';
     ASG1.cells[4, 0] := '4';
     ASG1.cells[5, 0] := '5';
     i := 1;
     while not QGriglia.EOF do begin
          // prima colonna: voce
          ASG1.ColWidths[0] := 220;
          ASG1.Cells[0, i] := QGriglia.FieldByName('Voce').Value;
          // seconda colonna -> PRIMA VOCE
          ASG1.ColWidths[1] := 80;
          ASG1.Cells[1, i] := QGriglia.FieldByName('Val1Dic').AsString;
          if QGriglia.FieldByName('Valore').AsInteger = 1 then
               ASG1.AddCheckBox(1, i, True, False)
          else ASG1.AddCheckBox(1, i, False, false);
          // terza colonna -> SECONDA VOCE
          ASG1.ColWidths[2] := 80;
          ASG1.Cells[2, i] := QGriglia.FieldByName('Val2Dic').AsString;
          if QGriglia.FieldByName('Valore').AsInteger = 2 then
               ASG1.AddCheckBox(2, i, True, false)
          else ASG1.AddCheckBox(2, i, False, false);
          // Quarta colonna -> TERZA VOCE
          ASG1.ColWidths[3] := 80;
          ASG1.Cells[3, i] := QGriglia.FieldByName('Val3Dic').AsString;
          if QGriglia.FieldByName('Valore').AsInteger = 3 then
               ASG1.AddCheckBox(3, i, True, false)
          else ASG1.AddCheckBox(3, i, False, false);
          // Quinta colonna -> QUARTA VOCE
          ASG1.ColWidths[4] := 80;
          ASG1.Cells[4, i] := QGriglia.FieldByName('Val4Dic').AsString;
          if QGriglia.FieldByName('Valore').AsInteger = 4 then
               ASG1.AddCheckBox(4, i, True, false)
          else ASG1.AddCheckBox(4, i, False, false);
          // Sesta colonna -> QUINTA VOCE
          ASG1.ColWidths[5] := 80;
          ASG1.Cells[5, i] := QGriglia.FieldByName('Val5Dic').AsString;
          if QGriglia.FieldByName('Valore').AsInteger = 5 then
               ASG1.AddCheckBox(5, i, True, false)
          else ASG1.AddCheckBox(5, i, False, false);

          QGriglia.Next;
          inc(i);
     end;
     QGriglia.Close;
end;

procedure TInfoColloquioForm.ASG1GetAlignment(Sender: TObject; ARow,
     ACol: Integer; var AAlignment: TAlignment);
begin
     if (ACol > 0) and (Arow = 0) then AAlignment := taCenter;
end;

procedure TInfoColloquioForm.SalvaGriglia;
var i, k, xTot: integer;
     Vero, xTrans: boolean;
begin
     // salvataggio valori sul DB
     xtrans := Data.DB.InTransaction;
     Q.Close;
     Q.SQL.text := 'select count(*) Tot from EspLavMotIns ' +
          'where IDEspLav=:xIDEspLav: and IDVoce=:xIDVoce:';
     Q.OriginalSQL.Text := Q.SQL.text;
     Q.ParamByName['IDEspLav'] := QEspLavAttualeID.Value;
     xTot := 0;
     for i := 1 to ASG1.RowCount - 1 do begin
          for k := 1 to ASG1.ColCount - 1 do begin
               ASG1.GetCheckBoxState(k, i, Vero);
               if Vero then begin
                    Q.Close;
                    Q.ReloadSQL;
                    Q.ParamByName['xIDEspLav'] := QEspLavAttualeID.Value;
                    Q.ParamByName['xIDVoce'] := i;
                    Q.Open;
                    QUpd.Close;
                    xTot := xTot + k;
                    if Q.FieldByName('Tot').asInteger > 0 then begin
                         // aggiorna
                         QUpd.SQL.text := 'update EspLavMotIns set Valore=:xVal: ' +
                              'where IDEspLav=:xIDEspLav: and IDVoce=:xIDVoce:';
                         QUpd.ParamByName['xVal'] := k;
                         QUpd.ParamByName['xIDEspLav'] := QEspLavAttualeID.Value;
                         QUpd.ParamByName['xIDVoce'] := i;
                         with Data do begin
                              if not xtrans then DB.BeginTrans;
                              try
                                   QUpd.ExecSQL;
                                   if not xtrans then DB.CommitTrans;
                              except
                                   if not xtrans then DB.RollbackTrans;
                                   MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                              end;
                         end;
                    end else begin
                         // inserisci
                         QUpd.SQL.text := 'insert into EspLavMotIns (IDEspLav,IDVoce,Valore) ' +
                              'values (:xIDEspLav:,:xIDVoce:,:xValore:)';
                         QUpd.ParamByName['xIDEspLav'] := QEspLavAttualeID.Value;
                         QUpd.ParamByName['xIDVoce'] := i;
                         QUpd.ParamByName['xValore'] := k;
                         with Data do begin
                              if not xtrans then DB.BeginTrans;
                              try
                                   QUpd.ExecSQL;
                                   if not xtrans then DB.CommitTrans;
                              except
                                   if not xtrans then DB.RollbackTrans;
                                   MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                              end;
                         end;
                    end;
               end;
          end;
     end;
     Q.Close;
end;

procedure TInfoColloquioForm.QEspLavAttualeAfterPost(DataSet: TDataSet);
begin
     // allineamento IDArea con IDMansione
     Q.Close;
     Q.SQL.text := 'update EsperienzeLavorative set IDArea=:xIDArea: ' +
          'where ID=' + QEspLavAttualeID.AsString;
     Q.ParamByName['xIDArea'] := QRuoliIDArea.Value;
     Q.ExecSQL;
end;

procedure TInfoColloquioForm.CreaTabellaTemporanea;
begin

     if not QEspLavAttuale.IsEmpty then begin
          // RIEMPI MemTable
          QEspLavDB.Close;
          {QEspLavDB.SQL.text := 'select distinct EspLavRetribuzioni.ID,NomeColonna Voce,Valore,Flag, ' +
               'EspLavRetribColonne.ID IDColonna ' +
               ' from EspLavRetribuzioni, EspLavRetribColonne ' +
               'where EspLavRetribColonne.ID *= EspLavRetribuzioni.IDColonna ' +
               '  and EspLavRetribuzioni.IDEspLav = ' + QEspLavAttualeID.AsString;}
          //modifica Query da Thomas
          QEspLavDB.SQL.text := 'select distinct EspLavRetribuzioni.ID,NomeColonna Voce,Valore,Flag, ' +
               'EspLavRetribColonne.ID IDColonna ' +
               ' from EspLavRetribColonne left join EspLavRetribuzioni' +
               ' on EspLavRetribColonne.ID = EspLavRetribuzioni.IDColonna' +
               '  and EspLavRetribuzioni.IDEspLav = ' + QEspLavAttualeID.AsString;
          QEspLavDB.Open;
          QEspLavRetribuz.Open;
          while not QEspLavDB.EOF do begin
               QEspLavRetribuz.InsertRecord([QEspLavDBID.Value, QEspLavDBVoce.Value, QEspLavDBFlag.Value, QEspLavDBValore.Value, QEspLavDBIDColonna.Value]);
               QEspLavDB.Next;
          end;
          QEspLavDB.Close;
     end;
end;

procedure TInfoColloquioForm.SalvaCLColloquio(xIDColloquio: integer);
begin
     // salvataggio da tabella temporanea
     // (chiamata da uGestEventi)
     // ATTENZIONE: alla fine distrugge anche la tabella temporanea
     Q.Close;
     QCLColloquioTemp.First;
     while not QCLColloquioTemp.EOF do begin
          Q.SQL.text := 'insert into CLColloquio (IDColloquio,IDCommessaVoce,Flag,Risposta) ' +
               ' values (:xIDColloquio:,:xIDCommessaVoce:,:xFlag:,:xRisposta:)';
          Q.ParamByName['xIDColloquio'] := xIDColloquio;
          Q.ParamByName['xIDCommessaVoce'] := QCLColloquioTempIDCommessaVoce.value;
          Q.ParamByName['xFlag'] := QCLColloquioTempFlag.Value;
          Q.ParamByName['xRisposta'] := QCLColloquioTempRisposta.Value;
          Q.ExecSQL;
          QCLColloquioTemp.Next;
     end;
end;

function TInfoColloquioForm.NuovaEspLavAttuale: boolean;
var xLastID: integer;
begin
     // prima controlla se ha esp.lav. non attuali
     SelEspLavForm := TSelEspLavForm.create(self);
     SelEspLavForm.xIDAnagrafica := xIDAnagrafica;
     SelEspLavForm.QEspLav.Close;
     SelEspLavForm.QEspLav.Parameters[0].Value := xIDAnagrafica;
     SelEspLavForm.QEspLav.Open;
     if SelEspLavForm.QEspLav.IsEmpty then begin
          // seleziona l'azienda
          SelEspLavForm.QEspLav.Close;
          SelClienteForm := TSelClienteForm.create(self);
          SelClienteForm.Caption := 'Selezione azienda per esp.lav. attuale';
          SelClienteForm.ShowModal;
          if SelClienteForm.ModalResult = mrOK then begin
               Q6.Close;
               // togli flag di ATTUALE su tutte le esperienze lav. per il soggetto
               Q6.SQL.text := 'update EsperienzeLavorative set Attuale=0 where IDAnagrafica=' + IntToStr(xIDAnagrafica);
               Q6.ExecSQL;
               // inserimento nuova esperienza lavorativa attuale per il soggetto
               Q6.SQL.text := 'insert into EsperienzeLavorative (IDAnagrafica,IDAzienda,IDSettore,Attuale) ' +
                    'values (:xIDAnagrafica,:xIDAzienda,:xIDSettore,:xAttuale)';
               Q6.Parameters.ParambyName('xIDAnagrafica').Value := xIDAnagrafica;
               Q6.Parameters.ParambyName('xIDAzienda').Value := SelClienteForm.TClienti.FieldbyName('ID').asInteger;
               Q6.Parameters.ParambyName('xIDSettore').Value := SelClienteForm.TClienti.FieldbyName('IDAttivita').asInteger;
               Q6.Parameters.ParambyName('xAttuale').Value := 1;
               Q6.ExecSQL;

               //Q6.Close;
               //Q6.SQL.text := 'select @@Identity as LastID';
               //Q6.Open;
               //xLastID := Q6.FieldByName('LastID').Value;

               //Q6.Close;
               //Q6.SQL.Text := 'update EsperienzeLavorative set Attuale=0 where idanagrafica = :xIDAnagrafica ' +
                 //   'and ID <> :xID';
               //Q6.Parameters.ParamByName('xIDAnagrafica').Value := xIDAnagrafica;
               //Q6.Parameters.ParamByName('xID').Value := xLastID;
               //Q6.ExecSQL;
               Result := True;
          end else Result := False;
          SelClienteForm.Free;
     end else begin
          SelEspLavForm.Showmodal;
          if SelEspLavForm.ModalResult = mrOK then begin
               Q.Close;
               Q.SQL.text := 'update EsperienzeLavorative set Attuale=1 where ID=' + SelEspLavForm.QEspLavID.asstring;
               Q.ExecSQL;
               Result := True;
          end else Result := False;
     end;
     SelEspLavForm.Free;
end;

procedure TInfoColloquioForm.ASG1RightClickCell(Sender: TObject; Arow, Acol: Integer);
begin
     // descrizione significato cella
     ValutazCollDettForm.Top := InfoColloquioForm.top + 80 + ((Arow + 1) * 20);
     ValutazCollDettForm.Left := InfoColloquioForm.left + (ACol * 120);
     Q.Close;
     Q.SQL.text := 'select * from EspLavMotInsModel where ID=:xID:';
     Q.ParamByName['xID'] := Arow;
     Q.Open;
     case Acol of
          0: ValutazCollDettForm.Label1.caption := Q.Fieldbyname('DescVoce').asString;
          1: ValutazCollDettForm.Label1.caption := Q.Fieldbyname('Val1Desc').asString;
          2: ValutazCollDettForm.Label1.caption := Q.Fieldbyname('Val2Desc').asString;
          3: ValutazCollDettForm.Label1.caption := Q.Fieldbyname('Val3Desc').asString;
          4: ValutazCollDettForm.Label1.caption := Q.Fieldbyname('Val4Desc').asString;
          5: ValutazCollDettForm.Label1.caption := Q.Fieldbyname('Val5Desc').asString;
     end;
     if ValutazCollDettForm.Label1.caption <> '' then
          ValutazCollDettForm.Show;
end;

procedure TInfoColloquioForm.FormDestroy(Sender: TObject);
begin
     //ValutazCollDettForm.Free;
end;

procedure TInfoColloquioForm.FormCreate(Sender: TObject);
begin
     ValutazCollDettForm := TValutazCollDettForm.create(self);
end;

procedure TInfoColloquioForm.ASG1CheckBoxClick(Sender: TObject; aCol, aRow: Integer; state: Boolean);
var i: integer;
begin

     if State = True then
          // solo uno pu� essere selezionato
          for i := 1 to ASG1.ColCount - 1 do
               if ACol <> i then begin
                    //ASG1.AddCheckBox(i, Arow, False, False);
                    ASG1.SetCheckBoxState(i, Arow, false);
                    ASG1.AddCheckBox(i, Arow, False, False);
               end;
end;

procedure TInfoColloquioForm.NuovaEsperienzalavorativaattuale1Click(Sender: TObject);
begin
     // seleziona l'azienda
     SelClienteForm := TSelClienteForm.create(self);
     SelClienteForm.Caption := 'Selezione azienda per esp.lav. attuale';
     SelClienteForm.ShowModal;
     if SelClienteForm.ModalResult = mrOK then begin
          Q.Close;
          // togli flag di ATTUALE su tutte le esperienze lav. per il soggetto
          Q.SQL.text := 'update EsperienzeLavorative set Attuale=0 where IDAnagrafica=' + IntToStr(xIDAnagrafica);
          Q.ExecSQL;
          // inserimento nuova esperienza lavorativa attuale per il soggetto
          Q.SQL.text := 'insert into EsperienzeLavorative (IDAnagrafica,IDAzienda,IDSettore,Attuale) ' +
               'values (:xIDAnagrafica:,:xIDAzienda:,:xIDSettore:,:xAttuale:)';
          Q.ParambyName['xIDAnagrafica'] := xIDAnagrafica;
          Q.ParambyName['xIDAzienda'] := SelClienteForm.TClienti.FieldbyName('ID').asInteger;
          Q.ParambyName['xIDSettore'] := SelClienteForm.TClienti.FieldbyName('IDAttivita').asInteger;
          Q.ParambyName['xAttuale'] := 1;
          Q.ExecSQL;
          // caricamento griglia
          ComponiGriglia;
          // posizionati sui dati retributivi
          PCUnico.ActivePageIndex := 0;
          // refresh tabelle
          dxDBGrid1.Visible := True;
          Panel7.Enabled := True;
          QEspLavAttuale.Close;
          QEspLavAttuale.Open;
          QEspLavRetribuz.Close;
          QEspLavRetribuz.Open;
          CreaTabellaTemporanea;
     end;
     SelClienteForm.Free;
end;

procedure TInfoColloquioForm.Valutazionecandidato1Click(Sender: TObject);
begin
     ValutazColloquioForm := TValutazColloquioForm.create(self);
     ValutazColloquioForm.xIDAnag := xIDAnagrafica;
     ValutazColloquioForm.BInfoCand.Visible := False;
     ValutazColloquioForm.ShowModal;
     if ValutazColloquioForm.ETot.Text <> '' then
          SEPunteggio.Value := StrToInt(ValutazColloquioForm.ETot.Text);
     ValutazColloquioForm.Free;
end;

procedure TInfoColloquioForm.Schedacandidato1Click(Sender: TObject);
begin
     SchedaCandForm := TSchedaCandForm.create(self);
     if not PosizionaAnag(xIDAnagrafica) then exit;
     SchedaCandForm.ShowModal;
     SchedaCandForm.Free;
end;

procedure TInfoColloquioForm.Curriculum1Click(Sender: TObject);
begin
     PosizionaAnag(xIDAnagrafica);
     CurriculumForm.ShowModal;
     QEspLavAttuale.Close;
     QEspLavAttuale.ParamByName['xIDAnag'] := xIDAnagrafica;
     QEspLavAttuale.Open;
     if (QEspLavAttuale.active) and (not QEspLavAttuale.IsEmpty) then begin
          dxDBGrid1.visible := True;
          panel7.Enabled := True;
     end else begin
          dxDBGrid1.visible := False;
          panel7.caption := 'Non � presente alcun esperienza lavorativa.';
          panel7.Enabled := False;
     end;
end;

procedure TInfoColloquioForm.Documenti1Click(Sender: TObject);
begin
     ApriCV(xIDAnagrafica);
end;

procedure TInfoColloquioForm.AppuntiWord1Click(Sender: TObject);
begin
     PosizionaAnag(xIDAnagrafica);
     ApriFileWord(Data.TAnagrafica.FieldByName('CVNumero').asString);
end;

procedure TInfoColloquioForm.Tabellamoticidiinsoddisfazione1Click(Sender: TObject);
begin
     // prima salva la griglia
     SalvaGriglia;
     GestEspLavMotInsModelForm := TGestEspLavMotInsModelForm.create(self);
     GestEspLavMotInsModelForm.ShowModal;
     GestEspLavMotInsModelForm.Free;
     // ricomposizione griglia
     ComponiGriglia;
end;

procedure TInfoColloquioForm.Tabellaqualificheecontratti1Click(Sender: TObject);
begin
     {
     GestQualifContrForm := TGestQualifContrForm.create(self);
     GestQualifContrForm.ShowModal;
     GestQualifContrForm.Free;
     QQualifContr.Close;
     QQualifContr.Open;
     }
     TabContrattiForm := TTabContrattiForm.Create(self);
     TabContrattiForm.ShowModal;
     TabContrattiForm.Free;
end;

procedure TInfoColloquioForm.Tabella1Click(Sender: TObject);
begin
     if (QEspLavAttuale.active) and (not QEspLavAttuale.IsEmpty) then begin
          {if QEspLavRetribuz.IsEmpty then begin
               MessageDlg('Nessuna esperienza lavorativa attuale. Impossibile aprire la maschera', mtError, [mbOK], 0);
               exit;
          end;}
          // registrazione dalla tabella temporanea a quella definitiva
          if (QEspLavAttuale.active) and (not QEspLavAttuale.IsEmpty) then begin
               QEspLavRetribuz.First;
               Q.Close;
               while not QEspLavRetribuz.EOF do begin
                    if QEspLavRetribuzID.asString <> '0' then
                         Q.SQL.text := 'update EspLavRetribuzioni set Valore=:xValore:,Flag=:xFlag: ' +
                              ' where ID=' + QEspLavRetribuzID.asString
                    else begin
                         Q.SQL.text := 'insert into EspLavRetribuzioni (IDEspLav,IDColonna,Valore,Flag,ConsideraTot) ' +
                              ' values (:xIDEspLav:,:xIDColonna:,:xValore:,:xFlag:,:xConsideraTot:)';
                         Q.ParamByName['xIDEspLav'] := QEspLavAttualeID.Value;
                         Q.ParamByName['xIDColonna'] := QEspLavRetribuzIDColonna.Value;
                         Q.ParamByName['xConsideraTot'] := True;
                    end;
                    Q.ParamByName['xValore'] := QEspLavRetribuzValore.Value;
                    Q.ParamByName['xFlag'] := QEspLavRetribuzFlag.Value;
                    Q.ExecSQL;

                    QEspLavRetribuz.Next;
               end;
               QEspLavRetribuz.EmptyTable;
               OpenTab('EspLavRetribColonne', ['ID', 'NomeColonna', 'Descrizione'], ['Voce', 'Descrizione']);
               CreaTabellaTemporanea;
          end;
     end;
end;

procedure TInfoColloquioForm.CBEsitoChange(Sender: TObject);
begin
     if CBEsito.Text = 'eliminare dalla ricerca' then begin
          cbGiudizio.Visible := false;
          SEPunteggio.Visible := false;
          Label13.Visible := false;
          Label14.visible := false;
          CBFlagAccPres.Visible := false;
          EmotivoElim.visible := true;
          Label15.visible := true;
     end else begin
          cbGiudizio.Visible := true;
          SEPunteggio.Visible := true;
          Label13.Visible := true;
          Label14.visible := true;
          CBFlagAccPres.Visible := true;
          EmotivoElim.visible := false;
          Label15.visible := false;
     end;
end;

procedure TInfoColloquioForm.ToolbarButton9711Click(Sender: TObject);
begin
     OpenTab('TabColloquiClienti', ['ID', 'DettColloquio'], ['Voce']);
     RiempiDBDettColloquio;
end;

procedure TInfoColloquioForm.RiempiDBDettColloquio;
begin
     // riempimento combo dett cliente
     CBDettCollCliente.Items.Clear;
     Q.Close;
     Q.SQL.Clear;
     Q.SQL.text := 'select * from TabColloquiClienti';
     Q.Open;
     while not Q.EOF do begin
          CBDettCollCliente.Items.Add(Q.FieldByName('DettColloquio').asString);
          Q.Next;
     end;
     Q.Close;

end;

procedure TInfoColloquioForm.CBColloquioClienteClick(Sender: TObject);
begin
     if CBColloquioCliente.checked then begin
          CBDettCollCliente.Enabled := True;
          CBDettCollCliente.Color := clWindow;
     end else begin
          CBDettCollCliente.Enabled := False;
          CBDettCollCliente.Color := clBtnFace;
     end;
end;

procedure TInfoColloquioForm.Schedavalutazione1Click(Sender: TObject);
begin
     ValutazColloquioForm := TValutazColloquioForm.create(self);
     ValutazColloquioForm.xIDAnag := xIDAnagrafica;
     ValutazColloquioForm.ShowModal;
     ValutazColloquioForm.Free;
end;

procedure TInfoColloquioForm.BOKClick(Sender: TObject);
begin
     if (QEspLavAttuale.active) and (not QEspLavAttuale.IsEmpty) then begin
          QEspLavAttuale.Edit;
          QEspLavAttuale.Post;
     end;
     InfoColloquioForm.ModalResult := mrOK;
end;

procedure TInfoColloquioForm.BAnnullaClick(Sender: TObject);
begin
     if (QEspLavAttuale.active) and (not QEspLavAttuale.IsEmpty) then
          QEspLavAttuale.Cancel;
     InfoColloquioForm.ModalResult := mrCancel;
end;

procedure TInfoColloquioForm.NascondiFunzioni;
begin
     // Naaconde le funzioni:
     // --> funziona solo con alcune
     TSCheckList.TabVisible := MainForm.VerificaFunzLicenzeNew('309') = true;
end;

procedure TInfoColloquioForm.CBSelezionatoriChange(Sender: TObject);
begin
     TSelezionatori.Locate('Nominativo', CBSelezionatori.Text, []);
end;

end.

