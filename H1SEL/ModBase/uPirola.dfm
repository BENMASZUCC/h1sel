object PirolaForm: TPirolaForm
  Left = 167
  Top = 68
  Width = 969
  Height = 770
  VertScrollBar.Position = 1708
  Caption = 'PirolaForm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = -1657
    Width = 945
    Height = 2
    Cursor = crVSplit
    Align = alTop
  end
  object Splitter2: TSplitter
    Left = 0
    Top = -1626
    Width = 945
    Height = 0
    Cursor = crVSplit
    Align = alTop
  end
  object Splitter4: TSplitter
    Left = 0
    Top = -1626
    Width = 945
    Height = 2
    Cursor = crVSplit
    Align = alTop
  end
  object Panel1: TPanel
    Left = 0
    Top = -1708
    Width = 945
    Height = 25
    Align = alTop
    TabOrder = 0
    object bCV: TToolbarButton97
      Left = 1
      Top = 1
      Width = 76
      Height = 22
      Caption = 'Vedi CV'
      OnClick = bCVClick
    end
    object bFile: TToolbarButton97
      Left = 77
      Top = 1
      Width = 76
      Height = 22
      Caption = 'Vedi doc.'
      OnClick = bFileClick
    end
    object ToolbarButton9731: TToolbarButton97
      Left = 165
      Top = 2
      Width = 84
      Height = 22
      Caption = 'Associa test'
      OnClick = ToolbarButton9731Click
    end
    object BitBtn1: TBitBtn
      Left = 693
      Top = 0
      Width = 75
      Height = 25
      TabOrder = 0
      Kind = bkOK
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = -1683
    Width = 945
    Height = 26
    Align = alTop
    TabOrder = 1
    object Label1: TLabel
      Left = 3
      Top = 5
      Width = 45
      Height = 13
      Caption = 'Cognome'
      FocusControl = DBECognome
    end
    object Label2: TLabel
      Left = 218
      Top = 5
      Width = 28
      Height = 13
      Caption = 'Nome'
      FocusControl = DBEdit2
    end
    object Label100: TLabel
      Left = 392
      Top = 5
      Width = 71
      Height = 13
      Caption = 'Data di nascita'
    end
    object Label101: TLabel
      Left = 557
      Top = 5
      Width = 78
      Height = 13
      Caption = 'Luogo di nascita'
      FocusControl = DBEdit65
    end
    object DBECognome: TDBEdit
      Left = 51
      Top = 2
      Width = 161
      Height = 21
      CharCase = ecUpperCase
      Color = clInfoBk
      DataField = 'cognome'
      DataSource = dsAnag
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
    end
    object DBEdit2: TDBEdit
      Left = 251
      Top = 2
      Width = 136
      Height = 21
      CharCase = ecUpperCase
      Color = clInfoBk
      DataField = 'Nome'
      DataSource = dsAnag
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 1
    end
    object DbDateEdit971: TDbDateEdit97
      Left = 468
      Top = 2
      Width = 85
      Height = 21
      Color = clInfoBk
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      BevelOuter = bvNone
      ColorCalendar.ColorValid = clBlue
      Date = 23875
      DayNames.Monday = 'lu'
      DayNames.Tuesday = 'ma'
      DayNames.Wednesday = 'me'
      DayNames.Thursday = 'gi'
      DayNames.Friday = 've'
      DayNames.Saturday = 'sa'
      DayNames.Sunday = 'do'
      MonthNames.January = 'gennaio'
      MonthNames.February = 'febbraio'
      MonthNames.March = 'marzo'
      MonthNames.April = 'aprile'
      MonthNames.May = 'maggio'
      MonthNames.June = 'giugno'
      MonthNames.July = 'luglio'
      MonthNames.August = 'agosto'
      MonthNames.September = 'settembre'
      MonthNames.October = 'ottobre'
      MonthNames.November = 'novembre'
      MonthNames.December = 'dicembre'
      Options = [doButtonTabStop, doIsMasked, doShowToday]
      DataField = 'datanascita'
      DataSource = dsAnag
      ReadOnly = True
    end
    object DBEdit65: TDBEdit
      Left = 641
      Top = 2
      Width = 146
      Height = 21
      Color = clInfoBk
      DataField = 'LuogoNascita'
      DataSource = dsAnag
      ReadOnly = True
      TabOrder = 3
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = -1655
    Width = 945
    Height = 29
    Align = alTop
    TabOrder = 2
    object Label3: TLabel
      Left = 3
      Top = 5
      Width = 85
      Height = 13
      Caption = 'Data ricezione CV'
    end
    object Label4: TLabel
      Left = 178
      Top = 5
      Width = 37
      Height = 13
      Caption = 'Ricerca'
    end
    object Label7: TLabel
      Left = 601
      Top = 8
      Width = 68
      Height = 13
      Caption = 'Data invio test'
    end
    object bInvioiTest: TToolbarButton97
      Left = 794
      Top = 3
      Width = 84
      Height = 25
      Caption = 'Invio Test'
      OnClick = bInvioiTestClick
    end
    object DbDateEdit972: TDbDateEdit97
      Left = 92
      Top = 1
      Width = 85
      Height = 21
      Color = clInfoBk
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      BevelOuter = bvNone
      ColorCalendar.ColorValid = clBlue
      Date = 23875
      DayNames.Monday = 'lu'
      DayNames.Tuesday = 'ma'
      DayNames.Wednesday = 'me'
      DayNames.Thursday = 'gi'
      DayNames.Friday = 've'
      DayNames.Saturday = 'sa'
      DayNames.Sunday = 'do'
      MonthNames.January = 'gennaio'
      MonthNames.February = 'febbraio'
      MonthNames.March = 'marzo'
      MonthNames.April = 'aprile'
      MonthNames.May = 'maggio'
      MonthNames.June = 'giugno'
      MonthNames.July = 'luglio'
      MonthNames.August = 'agosto'
      MonthNames.September = 'settembre'
      MonthNames.October = 'ottobre'
      MonthNames.November = 'novembre'
      MonthNames.December = 'dicembre'
      Options = [doButtonTabStop, doIsMasked, doShowToday]
      DataField = 'DataUltimoCV'
      DataSource = dsAnag
      ReadOnly = True
    end
    object DBEdit1: TDBEdit
      Left = 222
      Top = 2
      Width = 371
      Height = 21
      CharCase = ecUpperCase
      Color = clInfoBk
      DataField = 'Ricerca'
      DataSource = dsAnag
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 1
    end
    object DbDateEdit974: TDbDateEdit97
      Left = 679
      Top = 4
      Width = 110
      Height = 21
      Color = clInfoBk
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      BevelOuter = bvNone
      ColorCalendar.ColorValid = clBlue
      Date = 23875
      DayNames.Monday = 'lu'
      DayNames.Tuesday = 'ma'
      DayNames.Wednesday = 'me'
      DayNames.Thursday = 'gi'
      DayNames.Friday = 've'
      DayNames.Saturday = 'sa'
      DayNames.Sunday = 'do'
      MonthNames.January = 'gennaio'
      MonthNames.February = 'febbraio'
      MonthNames.March = 'marzo'
      MonthNames.April = 'aprile'
      MonthNames.May = 'maggio'
      MonthNames.June = 'giugno'
      MonthNames.July = 'luglio'
      MonthNames.August = 'agosto'
      MonthNames.September = 'settembre'
      MonthNames.October = 'ottobre'
      MonthNames.November = 'novembre'
      MonthNames.December = 'dicembre'
      Options = [doButtonTabStop, doCanClear, doCanPopup, doIsMasked, doShowCancel, doShowToday]
      DataField = 'DataInvioTest'
      DataSource = dsSelPirola
      ReadOnly = True
    end
  end
  object Panel4: TPanel
    Left = 968
    Top = -1616
    Width = 886
    Height = 37
    TabOrder = 3
    Visible = False
    object Label8: TLabel
      Left = 8
      Top = 56
      Width = 81
      Height = 13
      Caption = 'Data 1� colloquio'
    end
    object Label9: TLabel
      Left = 214
      Top = 56
      Width = 23
      Height = 13
      Caption = 'Note'
    end
    object Label10: TLabel
      Left = 502
      Top = 184
      Width = 81
      Height = 13
      Caption = 'Esito 1� colloquio'
    end
    object Label11: TLabel
      Left = 264
      Top = 184
      Width = 106
      Height = 13
      Caption = 'Data esito 1� colloquio'
    end
    object bSalva1: TToolbarButton97
      Left = 798
      Top = 200
      Width = 75
      Height = 25
      Caption = 'Salva'
      OnClick = bSalva1Click
    end
    object DbDateEdit975: TDbDateEdit97
      Left = 99
      Top = 53
      Width = 110
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      BevelOuter = bvNone
      ColorCalendar.ColorValid = clBlue
      Date = 23875
      DayNames.Monday = 'lu'
      DayNames.Tuesday = 'ma'
      DayNames.Wednesday = 'me'
      DayNames.Thursday = 'gi'
      DayNames.Friday = 've'
      DayNames.Saturday = 'sa'
      DayNames.Sunday = 'do'
      MonthNames.January = 'gennaio'
      MonthNames.February = 'febbraio'
      MonthNames.March = 'marzo'
      MonthNames.April = 'aprile'
      MonthNames.May = 'maggio'
      MonthNames.June = 'giugno'
      MonthNames.July = 'luglio'
      MonthNames.August = 'agosto'
      MonthNames.September = 'settembre'
      MonthNames.October = 'ottobre'
      MonthNames.November = 'novembre'
      MonthNames.December = 'dicembre'
      Options = [doButtonTabStop, doCanClear, doCanPopup, doIsMasked, doShowCancel, doShowToday]
      DataField = 'DataPrimoColloquio'
      DataSource = dsSelPirola
    end
    object DBMemo1: TDBMemo
      Left = 242
      Top = 54
      Width = 630
      Height = 120
      DataField = 'NotePrimocolloquio'
      DataSource = dsSelPirola
      TabOrder = 1
    end
    object DbDateEdit976: TDbDateEdit97
      Left = 374
      Top = 181
      Width = 110
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      BevelOuter = bvNone
      ColorCalendar.ColorValid = clBlue
      Date = 23875
      DayNames.Monday = 'lu'
      DayNames.Tuesday = 'ma'
      DayNames.Wednesday = 'me'
      DayNames.Thursday = 'gi'
      DayNames.Friday = 've'
      DayNames.Saturday = 'sa'
      DayNames.Sunday = 'do'
      MonthNames.January = 'gennaio'
      MonthNames.February = 'febbraio'
      MonthNames.March = 'marzo'
      MonthNames.April = 'aprile'
      MonthNames.May = 'maggio'
      MonthNames.June = 'giugno'
      MonthNames.July = 'luglio'
      MonthNames.August = 'agosto'
      MonthNames.September = 'settembre'
      MonthNames.October = 'ottobre'
      MonthNames.November = 'novembre'
      MonthNames.December = 'dicembre'
      Options = [doButtonTabStop, doCanClear, doCanPopup, doIsMasked, doShowCancel, doShowToday]
      DataField = 'DataEsitoPrimoColloquio'
      DataSource = dsSelPirola
    end
    object DBComboBox1: TDBComboBox
      Left = 589
      Top = 181
      Width = 205
      Height = 21
      DataField = 'EsitoPrimoColloquio'
      DataSource = dsSelPirola
      ItemHeight = 13
      Items.Strings = (
        ''
        'Positivo'
        'Negativo')
      TabOrder = 3
    end
  end
  object Panel5: TPanel
    Left = 0
    Top = 729
    Width = 945
    Height = 22
    Align = alBottom
    TabOrder = 4
    Visible = False
    object bCancEsito2: TToolbarButton97
      Left = 718
      Top = 12
      Width = 155
      Height = 25
      Caption = 'Cancella esito precedente'
      Glyph.Data = {
        E2010000424DE20100000000000042000000280000000F0000000D0000000100
        100003000000A001000000000000000000000000000000000000007C0000E003
        00001F0000001863186318631863186318631863186318631863186318631863
        1863186300001863104200000000000000000000104218631863186318631863
        186318630000186300001863FF7F1863FF7F1863000000001863186318631863
        18631863000018630000FF7F1863FF7F1863FF7F000010420000186318631863
        186318630000186300001863FF7F1863FF7F1863000010421042000018631863
        18631863000018631042FF7FFF7FFF7FFF7FFF7FFF7F00001042104200001863
        186318630000186318630000FF7FFF7FFF7FFF7FFF7FFF7F0000104210420000
        1863186300001863186318630000FF7FFF7FFF7FFF7FFF7FFF7F000010421042
        10421863000018631863186318630000FF7FFF7FFF7FFF7FFF7FFF7F00001042
        000018630000186318631863186318630000FF7FFF7FFF7FFF7FFF7FFF7F1042
        0000186300001863186318631863186318630000FF7FFF7FFF7FFF7FFF7FFF7F
        0000186300001863186318631863186318631863104200000000000000001042
        1042186300001863186318631863186318631863186318631863186318631863
        186318630000}
      OnClick = bCancEsito2Click
    end
    object bSalva3: TToolbarButton97
      Left = 798
      Top = 200
      Width = 75
      Height = 25
      Caption = 'Salva'
      OnClick = bSalva3Click
    end
    object Label17: TLabel
      Left = 494
      Top = 48
      Width = 47
      Height = 13
      Caption = 'Decisione'
    end
    object Label16: TLabel
      Left = 256
      Top = 48
      Width = 71
      Height = 13
      Caption = 'Data decisione'
    end
    object DBMemo3: TDBMemo
      Left = 246
      Top = 70
      Width = 628
      Height = 120
      DataField = 'NoteDecisione'
      DataSource = dsSelPirola
      TabOrder = 0
    end
    object DBComboBox3: TDBComboBox
      Left = 552
      Top = 45
      Width = 215
      Height = 21
      DataField = 'Decisione'
      DataSource = dsSelPirola
      ItemHeight = 13
      TabOrder = 1
    end
    object DbDateEdit979: TDbDateEdit97
      Left = 366
      Top = 45
      Width = 110
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      BevelOuter = bvNone
      ColorCalendar.ColorValid = clBlue
      Date = 23875
      DayNames.Monday = 'lu'
      DayNames.Tuesday = 'ma'
      DayNames.Wednesday = 'me'
      DayNames.Thursday = 'gi'
      DayNames.Friday = 've'
      DayNames.Saturday = 'sa'
      DayNames.Sunday = 'do'
      MonthNames.January = 'gennaio'
      MonthNames.February = 'febbraio'
      MonthNames.March = 'marzo'
      MonthNames.April = 'aprile'
      MonthNames.May = 'maggio'
      MonthNames.June = 'giugno'
      MonthNames.July = 'luglio'
      MonthNames.August = 'agosto'
      MonthNames.September = 'settembre'
      MonthNames.October = 'ottobre'
      MonthNames.November = 'novembre'
      MonthNames.December = 'dicembre'
      Options = [doButtonTabStop, doCanClear, doCanPopup, doIsMasked, doShowCancel, doShowToday]
      DataField = 'DataDecisione'
      DataSource = dsSelPirola
    end
  end
  object Panel6: TPanel
    Left = 1032
    Top = -1562
    Width = 913
    Height = 31
    TabOrder = 5
    Visible = False
    object Label12: TLabel
      Left = 502
      Top = 184
      Width = 81
      Height = 13
      Caption = 'Esito 2� colloquio'
    end
    object Label13: TLabel
      Left = 264
      Top = 184
      Width = 106
      Height = 13
      Caption = 'Data esito 2� colloquio'
    end
    object Label14: TLabel
      Left = 214
      Top = 56
      Width = 23
      Height = 13
      Caption = 'Note'
    end
    object Label15: TLabel
      Left = 8
      Top = 56
      Width = 81
      Height = 13
      Caption = 'Data 2� colloquio'
    end
    object Label18: TLabel
      Left = 8
      Top = 16
      Width = 72
      Height = 13
      Caption = 'Resp. divisione'
    end
    object bSalva2: TToolbarButton97
      Left = 798
      Top = 200
      Width = 75
      Height = 25
      Caption = 'Salva'
      OnClick = bSalva2Click
    end
    object bCancEsito1: TToolbarButton97
      Left = 718
      Top = 12
      Width = 155
      Height = 25
      Caption = 'Cancella esito precedente'
      Glyph.Data = {
        E2010000424DE20100000000000042000000280000000F0000000D0000000100
        100003000000A001000000000000000000000000000000000000007C0000E003
        00001F0000001863186318631863186318631863186318631863186318631863
        1863186300001863104200000000000000000000104218631863186318631863
        186318630000186300001863FF7F1863FF7F1863000000001863186318631863
        18631863000018630000FF7F1863FF7F1863FF7F000010420000186318631863
        186318630000186300001863FF7F1863FF7F1863000010421042000018631863
        18631863000018631042FF7FFF7FFF7FFF7FFF7FFF7F00001042104200001863
        186318630000186318630000FF7FFF7FFF7FFF7FFF7FFF7F0000104210420000
        1863186300001863186318630000FF7FFF7FFF7FFF7FFF7FFF7F000010421042
        10421863000018631863186318630000FF7FFF7FFF7FFF7FFF7FFF7F00001042
        000018630000186318631863186318630000FF7FFF7FFF7FFF7FFF7FFF7F1042
        0000186300001863186318631863186318630000FF7FFF7FFF7FFF7FFF7FFF7F
        0000186300001863186318631863186318631863104200000000000000001042
        1042186300001863186318631863186318631863186318631863186318631863
        186318630000}
      OnClick = bCancEsito1Click
    end
    object DBComboBox2: TDBComboBox
      Left = 589
      Top = 181
      Width = 205
      Height = 21
      DataField = 'EsitoSecondoColloquio'
      DataSource = dsSelPirola
      ItemHeight = 13
      Items.Strings = (
        ''
        'Positivo'
        'Negativo')
      TabOrder = 0
    end
    object DbDateEdit977: TDbDateEdit97
      Left = 374
      Top = 181
      Width = 110
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      BevelOuter = bvNone
      ColorCalendar.ColorValid = clBlue
      Date = 23875
      DayNames.Monday = 'lu'
      DayNames.Tuesday = 'ma'
      DayNames.Wednesday = 'me'
      DayNames.Thursday = 'gi'
      DayNames.Friday = 've'
      DayNames.Saturday = 'sa'
      DayNames.Sunday = 'do'
      MonthNames.January = 'gennaio'
      MonthNames.February = 'febbraio'
      MonthNames.March = 'marzo'
      MonthNames.April = 'aprile'
      MonthNames.May = 'maggio'
      MonthNames.June = 'giugno'
      MonthNames.July = 'luglio'
      MonthNames.August = 'agosto'
      MonthNames.September = 'settembre'
      MonthNames.October = 'ottobre'
      MonthNames.November = 'novembre'
      MonthNames.December = 'dicembre'
      Options = [doButtonTabStop, doCanClear, doCanPopup, doIsMasked, doShowCancel, doShowToday]
      DataField = 'DataEsitoSecondoColloquio'
      DataSource = dsSelPirola
    end
    object DBMemo2: TDBMemo
      Left = 242
      Top = 54
      Width = 630
      Height = 120
      DataField = 'NoteSecondocolloquio'
      DataSource = dsSelPirola
      TabOrder = 2
    end
    object DbDateEdit978: TDbDateEdit97
      Left = 99
      Top = 53
      Width = 110
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      BevelOuter = bvNone
      ColorCalendar.ColorValid = clBlue
      Date = 23875
      DayNames.Monday = 'lu'
      DayNames.Tuesday = 'ma'
      DayNames.Wednesday = 'me'
      DayNames.Thursday = 'gi'
      DayNames.Friday = 've'
      DayNames.Saturday = 'sa'
      DayNames.Sunday = 'do'
      MonthNames.January = 'gennaio'
      MonthNames.February = 'febbraio'
      MonthNames.March = 'marzo'
      MonthNames.April = 'aprile'
      MonthNames.May = 'maggio'
      MonthNames.June = 'giugno'
      MonthNames.July = 'luglio'
      MonthNames.August = 'agosto'
      MonthNames.September = 'settembre'
      MonthNames.October = 'ottobre'
      MonthNames.November = 'novembre'
      MonthNames.December = 'dicembre'
      Options = [doButtonTabStop, doCanClear, doCanPopup, doIsMasked, doShowCancel, doShowToday]
      DataField = 'DataEsitoSecondoColloquio'
      DataSource = dsSelPirola
    end
    object DBEdit4: TDBEdit
      Left = 99
      Top = 12
      Width = 384
      Height = 21
      CharCase = ecUpperCase
      Color = clInfoBk
      DataField = 'Responsabile'
      DataSource = dsAnag
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 4
    end
  end
  object Panel7: TPanel
    Left = 0
    Top = -1624
    Width = 945
    Height = 41
    Align = alTop
    TabOrder = 6
    object Label5: TLabel
      Left = 8
      Top = 16
      Width = 43
      Height = 13
      Caption = 'Data test'
    end
    object Label6: TLabel
      Left = 190
      Top = 16
      Width = 43
      Height = 13
      Caption = 'Esito test'
    end
    object bCancTest: TToolbarButton97
      Left = 687
      Top = 12
      Width = 20
      Height = 23
      Glyph.Data = {
        E2010000424DE20100000000000042000000280000000F0000000D0000000100
        100003000000A001000000000000000000000000000000000000007C0000E003
        00001F0000001863186318631863186318631863186318631863186318631863
        1863186300001863104200000000000000000000104218631863186318631863
        186318630000186300001863FF7F1863FF7F1863000000001863186318631863
        18631863000018630000FF7F1863FF7F1863FF7F000010420000186318631863
        186318630000186300001863FF7F1863FF7F1863000010421042000018631863
        18631863000018631042FF7FFF7FFF7FFF7FFF7FFF7F00001042104200001863
        186318630000186318630000FF7FFF7FFF7FFF7FFF7FFF7F0000104210420000
        1863186300001863186318630000FF7FFF7FFF7FFF7FFF7FFF7F000010421042
        10421863000018631863186318630000FF7FFF7FFF7FFF7FFF7FFF7F00001042
        000018630000186318631863186318630000FF7FFF7FFF7FFF7FFF7FFF7F1042
        0000186300001863186318631863186318630000FF7FFF7FFF7FFF7FFF7FFF7F
        0000186300001863186318631863186318631863104200000000000000001042
        1042186300001863186318631863186318631863186318631863186318631863
        186318630000}
      OnClick = bCancTestClick
    end
    object ToolbarButton971: TToolbarButton97
      Left = 710
      Top = 11
      Width = 79
      Height = 25
      Caption = 'Ricezione Test'
      OnClick = ToolbarButton971Click
    end
    object ToolbarButton9728: TToolbarButton97
      Left = 791
      Top = 11
      Width = 88
      Height = 25
      Caption = 'Invia a colloquio'
      OnClick = ToolbarButton9728Click
    end
    object DbDateEdit973: TDbDateEdit97
      Left = 62
      Top = 13
      Width = 87
      Height = 21
      Color = clInfoBk
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      BevelOuter = bvNone
      ColorCalendar.ColorValid = clBlue
      Date = 23875
      DayNames.Monday = 'lu'
      DayNames.Tuesday = 'ma'
      DayNames.Wednesday = 'me'
      DayNames.Thursday = 'gi'
      DayNames.Friday = 've'
      DayNames.Saturday = 'sa'
      DayNames.Sunday = 'do'
      MonthNames.January = 'gennaio'
      MonthNames.February = 'febbraio'
      MonthNames.March = 'marzo'
      MonthNames.April = 'aprile'
      MonthNames.May = 'maggio'
      MonthNames.June = 'giugno'
      MonthNames.July = 'luglio'
      MonthNames.August = 'agosto'
      MonthNames.September = 'settembre'
      MonthNames.October = 'ottobre'
      MonthNames.November = 'novembre'
      MonthNames.December = 'dicembre'
      Options = [doButtonTabStop, doIsMasked, doShowCancel, doShowToday]
      DataField = 'DataTest'
      DataSource = dsSelPirola
    end
    object DBEdit3: TDBEdit
      Left = 242
      Top = 13
      Width = 445
      Height = 21
      Color = clInfoBk
      DataField = 'EsitoTest'
      DataSource = dsSelPirola
      TabOrder = 1
    end
  end
  object Coll1: TAdvPanel
    Left = 0
    Top = -1583
    Width = 945
    Height = 220
    Align = alTop
    TabOrder = 7
    UseDockManager = True
    OnDblClick = Coll1DblClick
    Version = '1.9.0.3'
    Caption.Color = clHighlight
    Caption.ColorTo = clNone
    Caption.Font.Charset = DEFAULT_CHARSET
    Caption.Font.Color = clBlack
    Caption.Font.Height = -11
    Caption.Font.Name = 'MS Sans Serif'
    Caption.Font.Style = []
    Caption.Text = '1� Colloquio'
    Caption.Visible = True
    StatusBar.Font.Charset = DEFAULT_CHARSET
    StatusBar.Font.Color = clWindowText
    StatusBar.Font.Height = -11
    StatusBar.Font.Name = 'Tahoma'
    StatusBar.Font.Style = []
    FullHeight = 220
    object Label19: TLabel
      Left = 8
      Top = 24
      Width = 81
      Height = 13
      Caption = 'Data 1� colloquio'
    end
    object Label20: TLabel
      Left = 214
      Top = 24
      Width = 23
      Height = 13
      Caption = 'Note'
    end
    object Label21: TLabel
      Left = 241
      Top = 161
      Width = 106
      Height = 13
      Caption = 'Data esito 1� colloquio'
    end
    object Label22: TLabel
      Left = 487
      Top = 161
      Width = 81
      Height = 13
      Caption = 'Esito 1� colloquio'
    end
    object ToolbarButton972: TToolbarButton97
      Left = 800
      Top = 159
      Width = 75
      Height = 25
      Caption = 'Salva'
      OnClick = bSalva1Click
    end
    object ToolbarButton9730: TToolbarButton97
      Left = 799
      Top = 185
      Width = 75
      Height = 25
      Caption = 'Val. finale'
      OnClick = ToolbarButton9721Click
    end
    object Label65: TLabel
      Left = 8
      Top = 72
      Width = 103
      Height = 13
      Caption = 'Resonsabile colloquio'
    end
    object DbDateEdit9710: TDbDateEdit97
      Left = 99
      Top = 24
      Width = 104
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      BevelOuter = bvNone
      ColorCalendar.ColorValid = clBlue
      Date = 23875
      DayNames.Monday = 'lu'
      DayNames.Tuesday = 'ma'
      DayNames.Wednesday = 'me'
      DayNames.Thursday = 'gi'
      DayNames.Friday = 've'
      DayNames.Saturday = 'sa'
      DayNames.Sunday = 'do'
      MonthNames.January = 'gennaio'
      MonthNames.February = 'febbraio'
      MonthNames.March = 'marzo'
      MonthNames.April = 'aprile'
      MonthNames.May = 'maggio'
      MonthNames.June = 'giugno'
      MonthNames.July = 'luglio'
      MonthNames.August = 'agosto'
      MonthNames.September = 'settembre'
      MonthNames.October = 'ottobre'
      MonthNames.November = 'novembre'
      MonthNames.December = 'dicembre'
      Options = [doButtonTabStop, doCanClear, doCanPopup, doIsMasked, doShowCancel, doShowToday]
      DataField = 'DataPrimoColloquio'
      DataSource = dsSelPirola
    end
    object DBMemo4: TDBMemo
      Left = 242
      Top = 24
      Width = 630
      Height = 122
      DataField = 'NotePrimocolloquio'
      DataSource = dsSelPirola
      TabOrder = 1
    end
    object DbDateEdit9711: TDbDateEdit97
      Left = 359
      Top = 158
      Width = 106
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      BevelOuter = bvNone
      ColorCalendar.ColorValid = clBlue
      Date = 23875
      DayNames.Monday = 'lu'
      DayNames.Tuesday = 'ma'
      DayNames.Wednesday = 'me'
      DayNames.Thursday = 'gi'
      DayNames.Friday = 've'
      DayNames.Saturday = 'sa'
      DayNames.Sunday = 'do'
      MonthNames.January = 'gennaio'
      MonthNames.February = 'febbraio'
      MonthNames.March = 'marzo'
      MonthNames.April = 'aprile'
      MonthNames.May = 'maggio'
      MonthNames.June = 'giugno'
      MonthNames.July = 'luglio'
      MonthNames.August = 'agosto'
      MonthNames.September = 'settembre'
      MonthNames.October = 'ottobre'
      MonthNames.November = 'novembre'
      MonthNames.December = 'dicembre'
      Options = [doButtonTabStop, doCanClear, doCanPopup, doIsMasked, doShowCancel, doShowToday]
      DataField = 'DataEsitoPrimoColloquio'
      DataSource = dsSelPirola
    end
    object DBComboBox4: TDBComboBox
      Left = 574
      Top = 158
      Width = 205
      Height = 21
      DataField = 'EsitoPrimoColloquio'
      DataSource = dsSelPirola
      ItemHeight = 13
      Items.Strings = (
        ''
        'Positivo'
        'Negativo')
      TabOrder = 3
    end
    object DBEdit5: TDBEdit
      Left = 8
      Top = 88
      Width = 193
      Height = 21
      DataField = 'UtentePrimoColloquio'
      DataSource = dsSelPirola
      ReadOnly = True
      TabOrder = 4
    end
  end
  object Coll2: TAdvPanel
    Left = 0
    Top = -1363
    Width = 945
    Height = 234
    Align = alTop
    TabOrder = 8
    UseDockManager = True
    OnDblClick = Coll2DblClick
    Version = '1.9.0.3'
    Caption.Color = clHighlight
    Caption.ColorTo = clNone
    Caption.Font.Charset = DEFAULT_CHARSET
    Caption.Font.Color = clBlack
    Caption.Font.Height = -11
    Caption.Font.Name = 'MS Sans Serif'
    Caption.Font.Style = []
    Caption.Text = '2� Colloquio'
    Caption.Visible = True
    StatusBar.Font.Charset = DEFAULT_CHARSET
    StatusBar.Font.Color = clWindowText
    StatusBar.Font.Height = -11
    StatusBar.Font.Name = 'Tahoma'
    StatusBar.Font.Style = []
    FullHeight = 234
    object Label23: TLabel
      Left = 8
      Top = 29
      Width = 73
      Height = 13
      Caption = 'Resp. colloquio'
    end
    object Label24: TLabel
      Left = 8
      Top = 56
      Width = 81
      Height = 13
      Caption = 'Data 2� colloquio'
    end
    object Label25: TLabel
      Left = 239
      Top = 56
      Width = 23
      Height = 13
      Caption = 'Note'
    end
    object Label26: TLabel
      Left = 269
      Top = 184
      Width = 106
      Height = 13
      Caption = 'Data esito 2� colloquio'
    end
    object Label27: TLabel
      Left = 527
      Top = 184
      Width = 81
      Height = 13
      Caption = 'Esito 2� colloquio'
    end
    object ToolbarButton973: TToolbarButton97
      Left = 823
      Top = 181
      Width = 75
      Height = 25
      Caption = 'Salva'
      OnClick = bSalva2Click
    end
    object ToolbarButton974: TToolbarButton97
      Left = 743
      Top = 24
      Width = 155
      Height = 25
      Caption = 'Cancella esito precedente'
      Glyph.Data = {
        E2010000424DE20100000000000042000000280000000F0000000D0000000100
        100003000000A001000000000000000000000000000000000000007C0000E003
        00001F0000001863186318631863186318631863186318631863186318631863
        1863186300001863104200000000000000000000104218631863186318631863
        186318630000186300001863FF7F1863FF7F1863000000001863186318631863
        18631863000018630000FF7F1863FF7F1863FF7F000010420000186318631863
        186318630000186300001863FF7F1863FF7F1863000010421042000018631863
        18631863000018631042FF7FFF7FFF7FFF7FFF7FFF7F00001042104200001863
        186318630000186318630000FF7FFF7FFF7FFF7FFF7FFF7F0000104210420000
        1863186300001863186318630000FF7FFF7FFF7FFF7FFF7FFF7F000010421042
        10421863000018631863186318630000FF7FFF7FFF7FFF7FFF7FFF7F00001042
        000018630000186318631863186318630000FF7FFF7FFF7FFF7FFF7FFF7F1042
        0000186300001863186318631863186318630000FF7FFF7FFF7FFF7FFF7FFF7F
        0000186300001863186318631863186318631863104200000000000000001042
        1042186300001863186318631863186318631863186318631863186318631863
        186318630000}
      OnClick = ToolbarButton974Click
    end
    object ToolbarButton9729: TToolbarButton97
      Left = 823
      Top = 205
      Width = 75
      Height = 25
      Caption = 'Val. finale'
      OnClick = ToolbarButton9721Click
    end
    object Label66: TLabel
      Left = 8
      Top = 104
      Width = 103
      Height = 13
      Caption = 'Resonsabile colloquio'
    end
    object DbDateEdit9712: TDbDateEdit97
      Left = 99
      Top = 53
      Width = 118
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      BevelOuter = bvNone
      ColorCalendar.ColorValid = clBlue
      Date = 23875
      DayNames.Monday = 'lu'
      DayNames.Tuesday = 'ma'
      DayNames.Wednesday = 'me'
      DayNames.Thursday = 'gi'
      DayNames.Friday = 've'
      DayNames.Saturday = 'sa'
      DayNames.Sunday = 'do'
      MonthNames.January = 'gennaio'
      MonthNames.February = 'febbraio'
      MonthNames.March = 'marzo'
      MonthNames.April = 'aprile'
      MonthNames.May = 'maggio'
      MonthNames.June = 'giugno'
      MonthNames.July = 'luglio'
      MonthNames.August = 'agosto'
      MonthNames.September = 'settembre'
      MonthNames.October = 'ottobre'
      MonthNames.November = 'novembre'
      MonthNames.December = 'dicembre'
      Options = [doButtonTabStop, doCanClear, doCanPopup, doIsMasked, doShowCancel, doShowToday]
      DataField = 'DataEsitoSecondoColloquio'
      DataSource = dsSelPirola
    end
    object DbDateEdit9713: TDbDateEdit97
      Left = 381
      Top = 181
      Width = 119
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      BevelOuter = bvNone
      ColorCalendar.ColorValid = clBlue
      Date = 23875
      DayNames.Monday = 'lu'
      DayNames.Tuesday = 'ma'
      DayNames.Wednesday = 'me'
      DayNames.Thursday = 'gi'
      DayNames.Friday = 've'
      DayNames.Saturday = 'sa'
      DayNames.Sunday = 'do'
      MonthNames.January = 'gennaio'
      MonthNames.February = 'febbraio'
      MonthNames.March = 'marzo'
      MonthNames.April = 'aprile'
      MonthNames.May = 'maggio'
      MonthNames.June = 'giugno'
      MonthNames.July = 'luglio'
      MonthNames.August = 'agosto'
      MonthNames.September = 'settembre'
      MonthNames.October = 'ottobre'
      MonthNames.November = 'novembre'
      MonthNames.December = 'dicembre'
      Options = [doButtonTabStop, doCanClear, doCanPopup, doIsMasked, doShowCancel, doShowToday]
      DataField = 'DataEsitoSecondoColloquio'
      DataSource = dsSelPirola
    end
    object DBComboBox5: TDBComboBox
      Left = 614
      Top = 181
      Width = 205
      Height = 21
      DataField = 'EsitoSecondoColloquio'
      DataSource = dsSelPirola
      ItemHeight = 13
      Items.Strings = (
        ''
        'Positivo'
        'Negativo')
      TabOrder = 2
    end
    object DBMemo5: TDBMemo
      Left = 267
      Top = 54
      Width = 630
      Height = 120
      DataField = 'NoteSecondocolloquio'
      DataSource = dsSelPirola
      TabOrder = 3
    end
    object DBEdit6: TDBEdit
      Left = 8
      Top = 120
      Width = 193
      Height = 21
      DataField = 'UtenteSecondoColloquio'
      DataSource = dsSelPirola
      ReadOnly = True
      TabOrder = 4
    end
  end
  object Coll3: TAdvPanel
    Left = 0
    Top = -1129
    Width = 945
    Height = 234
    Align = alTop
    TabOrder = 9
    UseDockManager = True
    OnDblClick = Coll3DblClick
    Version = '1.9.0.3'
    Caption.Color = clHighlight
    Caption.ColorTo = clNone
    Caption.Font.Charset = DEFAULT_CHARSET
    Caption.Font.Color = clBlack
    Caption.Font.Height = -11
    Caption.Font.Name = 'MS Sans Serif'
    Caption.Font.Style = []
    Caption.Text = '3� Colloquio'
    Caption.Visible = True
    StatusBar.Font.Charset = DEFAULT_CHARSET
    StatusBar.Font.Color = clWindowText
    StatusBar.Font.Height = -11
    StatusBar.Font.Name = 'Tahoma'
    StatusBar.Font.Style = []
    FullHeight = 234
    object Label28: TLabel
      Left = 8
      Top = 29
      Width = 73
      Height = 13
      Caption = 'Resp. colloquio'
    end
    object Label29: TLabel
      Left = 8
      Top = 56
      Width = 81
      Height = 13
      Caption = 'Data 3� colloquio'
    end
    object Label30: TLabel
      Left = 239
      Top = 56
      Width = 23
      Height = 13
      Caption = 'Note'
    end
    object Label31: TLabel
      Left = 269
      Top = 184
      Width = 106
      Height = 13
      Caption = 'Data esito 3� colloquio'
    end
    object Label32: TLabel
      Left = 527
      Top = 184
      Width = 81
      Height = 13
      Caption = 'Esito 3� colloquio'
    end
    object ToolbarButton975: TToolbarButton97
      Left = 823
      Top = 180
      Width = 75
      Height = 25
      Caption = 'Salva'
      OnClick = ToolbarButton975Click
    end
    object ToolbarButton976: TToolbarButton97
      Left = 743
      Top = 24
      Width = 155
      Height = 25
      Caption = 'Cancella esito precedente'
      Glyph.Data = {
        E2010000424DE20100000000000042000000280000000F0000000D0000000100
        100003000000A001000000000000000000000000000000000000007C0000E003
        00001F0000001863186318631863186318631863186318631863186318631863
        1863186300001863104200000000000000000000104218631863186318631863
        186318630000186300001863FF7F1863FF7F1863000000001863186318631863
        18631863000018630000FF7F1863FF7F1863FF7F000010420000186318631863
        186318630000186300001863FF7F1863FF7F1863000010421042000018631863
        18631863000018631042FF7FFF7FFF7FFF7FFF7FFF7F00001042104200001863
        186318630000186318630000FF7FFF7FFF7FFF7FFF7FFF7F0000104210420000
        1863186300001863186318630000FF7FFF7FFF7FFF7FFF7FFF7F000010421042
        10421863000018631863186318630000FF7FFF7FFF7FFF7FFF7FFF7F00001042
        000018630000186318631863186318630000FF7FFF7FFF7FFF7FFF7FFF7F1042
        0000186300001863186318631863186318630000FF7FFF7FFF7FFF7FFF7FFF7F
        0000186300001863186318631863186318631863104200000000000000001042
        1042186300001863186318631863186318631863186318631863186318631863
        186318630000}
      OnClick = ToolbarButton976Click
    end
    object ToolbarButton9721: TToolbarButton97
      Left = 823
      Top = 205
      Width = 75
      Height = 25
      Caption = 'Val. finale'
      OnClick = ToolbarButton9721Click
    end
    object Label67: TLabel
      Left = 8
      Top = 104
      Width = 103
      Height = 13
      Caption = 'Resonsabile colloquio'
    end
    object DbDateEdit9714: TDbDateEdit97
      Left = 99
      Top = 53
      Width = 118
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      BevelOuter = bvNone
      ColorCalendar.ColorValid = clBlue
      Date = 23875
      DayNames.Monday = 'lu'
      DayNames.Tuesday = 'ma'
      DayNames.Wednesday = 'me'
      DayNames.Thursday = 'gi'
      DayNames.Friday = 've'
      DayNames.Saturday = 'sa'
      DayNames.Sunday = 'do'
      MonthNames.January = 'gennaio'
      MonthNames.February = 'febbraio'
      MonthNames.March = 'marzo'
      MonthNames.April = 'aprile'
      MonthNames.May = 'maggio'
      MonthNames.June = 'giugno'
      MonthNames.July = 'luglio'
      MonthNames.August = 'agosto'
      MonthNames.September = 'settembre'
      MonthNames.October = 'ottobre'
      MonthNames.November = 'novembre'
      MonthNames.December = 'dicembre'
      Options = [doButtonTabStop, doCanClear, doCanPopup, doIsMasked, doShowCancel, doShowToday]
      DataField = 'DataEsitoTerzoColloquio'
      DataSource = dsSelPirola
    end
    object DbDateEdit9715: TDbDateEdit97
      Left = 381
      Top = 181
      Width = 119
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      BevelOuter = bvNone
      ColorCalendar.ColorValid = clBlue
      Date = 23875
      DayNames.Monday = 'lu'
      DayNames.Tuesday = 'ma'
      DayNames.Wednesday = 'me'
      DayNames.Thursday = 'gi'
      DayNames.Friday = 've'
      DayNames.Saturday = 'sa'
      DayNames.Sunday = 'do'
      MonthNames.January = 'gennaio'
      MonthNames.February = 'febbraio'
      MonthNames.March = 'marzo'
      MonthNames.April = 'aprile'
      MonthNames.May = 'maggio'
      MonthNames.June = 'giugno'
      MonthNames.July = 'luglio'
      MonthNames.August = 'agosto'
      MonthNames.September = 'settembre'
      MonthNames.October = 'ottobre'
      MonthNames.November = 'novembre'
      MonthNames.December = 'dicembre'
      Options = [doButtonTabStop, doCanClear, doCanPopup, doIsMasked, doShowCancel, doShowToday]
      DataField = 'DataEsitoTerzoColloquio'
      DataSource = dsSelPirola
    end
    object DBComboBox6: TDBComboBox
      Left = 614
      Top = 181
      Width = 205
      Height = 21
      DataField = 'EsitoTerzoColloquio'
      DataSource = dsSelPirola
      ItemHeight = 13
      Items.Strings = (
        ''
        'Positivo'
        'Negativo')
      TabOrder = 2
    end
    object DBMemo6: TDBMemo
      Left = 267
      Top = 54
      Width = 630
      Height = 120
      DataField = 'NoteTerzocolloquio'
      DataSource = dsSelPirola
      TabOrder = 3
    end
    object DBEdit7: TDBEdit
      Left = 8
      Top = 120
      Width = 193
      Height = 21
      DataField = 'UtenteTerzoColloquio'
      DataSource = dsSelPirola
      ReadOnly = True
      TabOrder = 4
    end
  end
  object Coll4: TAdvPanel
    Left = 0
    Top = -895
    Width = 945
    Height = 234
    Align = alTop
    TabOrder = 10
    UseDockManager = True
    OnDblClick = Coll4DblClick
    Version = '1.9.0.3'
    Caption.Color = clHighlight
    Caption.ColorTo = clNone
    Caption.Font.Charset = DEFAULT_CHARSET
    Caption.Font.Color = clBlack
    Caption.Font.Height = -11
    Caption.Font.Name = 'MS Sans Serif'
    Caption.Font.Style = []
    Caption.Text = '4� Colloquio'
    Caption.Visible = True
    StatusBar.Font.Charset = DEFAULT_CHARSET
    StatusBar.Font.Color = clWindowText
    StatusBar.Font.Height = -11
    StatusBar.Font.Name = 'Tahoma'
    StatusBar.Font.Style = []
    FullHeight = 234
    object Label33: TLabel
      Left = 8
      Top = 29
      Width = 73
      Height = 13
      Caption = 'Resp. colloquio'
    end
    object Label34: TLabel
      Left = 8
      Top = 56
      Width = 81
      Height = 13
      Caption = 'Data 4� colloquio'
    end
    object Label35: TLabel
      Left = 239
      Top = 56
      Width = 23
      Height = 13
      Caption = 'Note'
    end
    object Label36: TLabel
      Left = 269
      Top = 184
      Width = 106
      Height = 13
      Caption = 'Data esito 4� colloquio'
    end
    object Label37: TLabel
      Left = 527
      Top = 184
      Width = 81
      Height = 13
      Caption = 'Esito 4� colloquio'
    end
    object ToolbarButton977: TToolbarButton97
      Left = 823
      Top = 180
      Width = 75
      Height = 25
      Caption = 'Salva'
      OnClick = ToolbarButton977Click
    end
    object ToolbarButton978: TToolbarButton97
      Left = 743
      Top = 24
      Width = 155
      Height = 25
      Caption = 'Cancella esito precedente'
      Glyph.Data = {
        E2010000424DE20100000000000042000000280000000F0000000D0000000100
        100003000000A001000000000000000000000000000000000000007C0000E003
        00001F0000001863186318631863186318631863186318631863186318631863
        1863186300001863104200000000000000000000104218631863186318631863
        186318630000186300001863FF7F1863FF7F1863000000001863186318631863
        18631863000018630000FF7F1863FF7F1863FF7F000010420000186318631863
        186318630000186300001863FF7F1863FF7F1863000010421042000018631863
        18631863000018631042FF7FFF7FFF7FFF7FFF7FFF7F00001042104200001863
        186318630000186318630000FF7FFF7FFF7FFF7FFF7FFF7F0000104210420000
        1863186300001863186318630000FF7FFF7FFF7FFF7FFF7FFF7F000010421042
        10421863000018631863186318630000FF7FFF7FFF7FFF7FFF7FFF7F00001042
        000018630000186318631863186318630000FF7FFF7FFF7FFF7FFF7FFF7F1042
        0000186300001863186318631863186318630000FF7FFF7FFF7FFF7FFF7FFF7F
        0000186300001863186318631863186318631863104200000000000000001042
        1042186300001863186318631863186318631863186318631863186318631863
        186318630000}
      OnClick = ToolbarButton978Click
    end
    object ToolbarButton9722: TToolbarButton97
      Left = 823
      Top = 205
      Width = 75
      Height = 25
      Caption = 'Val. finale'
      OnClick = ToolbarButton9721Click
    end
    object Label68: TLabel
      Left = 8
      Top = 104
      Width = 103
      Height = 13
      Caption = 'Resonsabile colloquio'
    end
    object DbDateEdit9716: TDbDateEdit97
      Left = 99
      Top = 53
      Width = 118
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      BevelOuter = bvNone
      ColorCalendar.ColorValid = clBlue
      Date = 23875
      DayNames.Monday = 'lu'
      DayNames.Tuesday = 'ma'
      DayNames.Wednesday = 'me'
      DayNames.Thursday = 'gi'
      DayNames.Friday = 've'
      DayNames.Saturday = 'sa'
      DayNames.Sunday = 'do'
      MonthNames.January = 'gennaio'
      MonthNames.February = 'febbraio'
      MonthNames.March = 'marzo'
      MonthNames.April = 'aprile'
      MonthNames.May = 'maggio'
      MonthNames.June = 'giugno'
      MonthNames.July = 'luglio'
      MonthNames.August = 'agosto'
      MonthNames.September = 'settembre'
      MonthNames.October = 'ottobre'
      MonthNames.November = 'novembre'
      MonthNames.December = 'dicembre'
      Options = [doButtonTabStop, doCanClear, doCanPopup, doIsMasked, doShowCancel, doShowToday]
      DataField = 'DataEsitoQuartoColloquio'
      DataSource = dsSelPirola
    end
    object DbDateEdit9717: TDbDateEdit97
      Left = 381
      Top = 181
      Width = 119
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      BevelOuter = bvNone
      ColorCalendar.ColorValid = clBlue
      Date = 23875
      DayNames.Monday = 'lu'
      DayNames.Tuesday = 'ma'
      DayNames.Wednesday = 'me'
      DayNames.Thursday = 'gi'
      DayNames.Friday = 've'
      DayNames.Saturday = 'sa'
      DayNames.Sunday = 'do'
      MonthNames.January = 'gennaio'
      MonthNames.February = 'febbraio'
      MonthNames.March = 'marzo'
      MonthNames.April = 'aprile'
      MonthNames.May = 'maggio'
      MonthNames.June = 'giugno'
      MonthNames.July = 'luglio'
      MonthNames.August = 'agosto'
      MonthNames.September = 'settembre'
      MonthNames.October = 'ottobre'
      MonthNames.November = 'novembre'
      MonthNames.December = 'dicembre'
      Options = [doButtonTabStop, doCanClear, doCanPopup, doIsMasked, doShowCancel, doShowToday]
      DataField = 'DataEsitoQuartoColloquio'
      DataSource = dsSelPirola
    end
    object DBComboBox7: TDBComboBox
      Left = 614
      Top = 181
      Width = 205
      Height = 21
      DataField = 'EsitoQuartoColloquio'
      DataSource = dsSelPirola
      ItemHeight = 13
      Items.Strings = (
        ''
        'Positivo'
        'Negativo')
      TabOrder = 2
    end
    object DBMemo7: TDBMemo
      Left = 267
      Top = 54
      Width = 630
      Height = 119
      DataField = 'NoteQuartocolloquio'
      DataSource = dsSelPirola
      TabOrder = 3
    end
    object DBEdit8: TDBEdit
      Left = 8
      Top = 120
      Width = 193
      Height = 21
      DataField = 'UtenteQuartoColloquio'
      DataSource = dsSelPirola
      ReadOnly = True
      TabOrder = 4
    end
  end
  object Coll5: TAdvPanel
    Left = 0
    Top = -661
    Width = 945
    Height = 234
    Align = alTop
    TabOrder = 11
    UseDockManager = True
    OnDblClick = Coll5DblClick
    Version = '1.9.0.3'
    Caption.Color = clHighlight
    Caption.ColorTo = clNone
    Caption.Font.Charset = DEFAULT_CHARSET
    Caption.Font.Color = clBlack
    Caption.Font.Height = -11
    Caption.Font.Name = 'MS Sans Serif'
    Caption.Font.Style = []
    Caption.Text = '5� Colloquio'
    Caption.Visible = True
    StatusBar.Font.Charset = DEFAULT_CHARSET
    StatusBar.Font.Color = clWindowText
    StatusBar.Font.Height = -11
    StatusBar.Font.Name = 'Tahoma'
    StatusBar.Font.Style = []
    FullHeight = 234
    object Label38: TLabel
      Left = 8
      Top = 29
      Width = 73
      Height = 13
      Caption = 'Resp. colloquio'
    end
    object Label39: TLabel
      Left = 8
      Top = 56
      Width = 81
      Height = 13
      Caption = 'Data 5� colloquio'
    end
    object Label40: TLabel
      Left = 239
      Top = 56
      Width = 23
      Height = 13
      Caption = 'Note'
    end
    object Label41: TLabel
      Left = 269
      Top = 184
      Width = 106
      Height = 13
      Caption = 'Data esito 5� colloquio'
    end
    object Label42: TLabel
      Left = 527
      Top = 184
      Width = 81
      Height = 13
      Caption = 'Esito 5� colloquio'
    end
    object ToolbarButton979: TToolbarButton97
      Left = 823
      Top = 180
      Width = 75
      Height = 25
      Caption = 'Salva'
      OnClick = ToolbarButton979Click
    end
    object ToolbarButton9710: TToolbarButton97
      Left = 743
      Top = 24
      Width = 155
      Height = 25
      Caption = 'Cancella esito precedente'
      Glyph.Data = {
        E2010000424DE20100000000000042000000280000000F0000000D0000000100
        100003000000A001000000000000000000000000000000000000007C0000E003
        00001F0000001863186318631863186318631863186318631863186318631863
        1863186300001863104200000000000000000000104218631863186318631863
        186318630000186300001863FF7F1863FF7F1863000000001863186318631863
        18631863000018630000FF7F1863FF7F1863FF7F000010420000186318631863
        186318630000186300001863FF7F1863FF7F1863000010421042000018631863
        18631863000018631042FF7FFF7FFF7FFF7FFF7FFF7F00001042104200001863
        186318630000186318630000FF7FFF7FFF7FFF7FFF7FFF7F0000104210420000
        1863186300001863186318630000FF7FFF7FFF7FFF7FFF7FFF7F000010421042
        10421863000018631863186318630000FF7FFF7FFF7FFF7FFF7FFF7F00001042
        000018630000186318631863186318630000FF7FFF7FFF7FFF7FFF7FFF7F1042
        0000186300001863186318631863186318630000FF7FFF7FFF7FFF7FFF7FFF7F
        0000186300001863186318631863186318631863104200000000000000001042
        1042186300001863186318631863186318631863186318631863186318631863
        186318630000}
      OnClick = ToolbarButton9710Click
    end
    object ToolbarButton9723: TToolbarButton97
      Left = 823
      Top = 205
      Width = 75
      Height = 25
      Caption = 'Val. finale'
      OnClick = ToolbarButton9721Click
    end
    object Label69: TLabel
      Left = 8
      Top = 104
      Width = 103
      Height = 13
      Caption = 'Resonsabile colloquio'
    end
    object DbDateEdit9718: TDbDateEdit97
      Left = 99
      Top = 53
      Width = 118
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      BevelOuter = bvNone
      ColorCalendar.ColorValid = clBlue
      Date = 23875
      DayNames.Monday = 'lu'
      DayNames.Tuesday = 'ma'
      DayNames.Wednesday = 'me'
      DayNames.Thursday = 'gi'
      DayNames.Friday = 've'
      DayNames.Saturday = 'sa'
      DayNames.Sunday = 'do'
      MonthNames.January = 'gennaio'
      MonthNames.February = 'febbraio'
      MonthNames.March = 'marzo'
      MonthNames.April = 'aprile'
      MonthNames.May = 'maggio'
      MonthNames.June = 'giugno'
      MonthNames.July = 'luglio'
      MonthNames.August = 'agosto'
      MonthNames.September = 'settembre'
      MonthNames.October = 'ottobre'
      MonthNames.November = 'novembre'
      MonthNames.December = 'dicembre'
      Options = [doButtonTabStop, doCanClear, doCanPopup, doIsMasked, doShowCancel, doShowToday]
      DataField = 'DataEsitoQuintoColloquio'
      DataSource = dsSelPirola
    end
    object DbDateEdit9719: TDbDateEdit97
      Left = 381
      Top = 181
      Width = 119
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      BevelOuter = bvNone
      ColorCalendar.ColorValid = clBlue
      Date = 23875
      DayNames.Monday = 'lu'
      DayNames.Tuesday = 'ma'
      DayNames.Wednesday = 'me'
      DayNames.Thursday = 'gi'
      DayNames.Friday = 've'
      DayNames.Saturday = 'sa'
      DayNames.Sunday = 'do'
      MonthNames.January = 'gennaio'
      MonthNames.February = 'febbraio'
      MonthNames.March = 'marzo'
      MonthNames.April = 'aprile'
      MonthNames.May = 'maggio'
      MonthNames.June = 'giugno'
      MonthNames.July = 'luglio'
      MonthNames.August = 'agosto'
      MonthNames.September = 'settembre'
      MonthNames.October = 'ottobre'
      MonthNames.November = 'novembre'
      MonthNames.December = 'dicembre'
      Options = [doButtonTabStop, doCanClear, doCanPopup, doIsMasked, doShowCancel, doShowToday]
      DataField = 'DataEsitoQuintoColloquio'
      DataSource = dsSelPirola
    end
    object DBComboBox8: TDBComboBox
      Left = 614
      Top = 181
      Width = 205
      Height = 21
      DataField = 'EsitoQuintoColloquio'
      DataSource = dsSelPirola
      ItemHeight = 13
      Items.Strings = (
        ''
        'Positivo'
        'Negativo')
      TabOrder = 2
    end
    object DBMemo8: TDBMemo
      Left = 267
      Top = 54
      Width = 630
      Height = 119
      DataField = 'NoteQuartocolloquio'
      DataSource = dsSelPirola
      TabOrder = 3
    end
    object DBEdit9: TDBEdit
      Left = 8
      Top = 120
      Width = 193
      Height = 21
      DataField = 'UtenteQuintoColloquio'
      DataSource = dsSelPirola
      ReadOnly = True
      TabOrder = 4
    end
  end
  object Coll6: TAdvPanel
    Left = 0
    Top = -427
    Width = 945
    Height = 234
    Align = alTop
    TabOrder = 12
    UseDockManager = True
    OnDblClick = Coll6DblClick
    Version = '1.9.0.3'
    Caption.Color = clHighlight
    Caption.ColorTo = clNone
    Caption.Font.Charset = DEFAULT_CHARSET
    Caption.Font.Color = clBlack
    Caption.Font.Height = -11
    Caption.Font.Name = 'MS Sans Serif'
    Caption.Font.Style = []
    Caption.Text = '6� Colloquio'
    Caption.Visible = True
    StatusBar.Font.Charset = DEFAULT_CHARSET
    StatusBar.Font.Color = clWindowText
    StatusBar.Font.Height = -11
    StatusBar.Font.Name = 'Tahoma'
    StatusBar.Font.Style = []
    FullHeight = 234
    object Label43: TLabel
      Left = 8
      Top = 29
      Width = 73
      Height = 13
      Caption = 'Resp. colloquio'
    end
    object Label44: TLabel
      Left = 8
      Top = 56
      Width = 81
      Height = 13
      Caption = 'Data 6� colloquio'
    end
    object Label45: TLabel
      Left = 239
      Top = 56
      Width = 23
      Height = 13
      Caption = 'Note'
    end
    object Label46: TLabel
      Left = 269
      Top = 184
      Width = 106
      Height = 13
      Caption = 'Data esito 6� colloquio'
    end
    object Label47: TLabel
      Left = 527
      Top = 184
      Width = 81
      Height = 13
      Caption = 'Esito 6� colloquio'
    end
    object ToolbarButton9711: TToolbarButton97
      Left = 823
      Top = 180
      Width = 75
      Height = 25
      Caption = 'Salva'
      OnClick = ToolbarButton9711Click
    end
    object ToolbarButton9712: TToolbarButton97
      Left = 743
      Top = 24
      Width = 155
      Height = 25
      Caption = 'Cancella esito precedente'
      Glyph.Data = {
        E2010000424DE20100000000000042000000280000000F0000000D0000000100
        100003000000A001000000000000000000000000000000000000007C0000E003
        00001F0000001863186318631863186318631863186318631863186318631863
        1863186300001863104200000000000000000000104218631863186318631863
        186318630000186300001863FF7F1863FF7F1863000000001863186318631863
        18631863000018630000FF7F1863FF7F1863FF7F000010420000186318631863
        186318630000186300001863FF7F1863FF7F1863000010421042000018631863
        18631863000018631042FF7FFF7FFF7FFF7FFF7FFF7F00001042104200001863
        186318630000186318630000FF7FFF7FFF7FFF7FFF7FFF7F0000104210420000
        1863186300001863186318630000FF7FFF7FFF7FFF7FFF7FFF7F000010421042
        10421863000018631863186318630000FF7FFF7FFF7FFF7FFF7FFF7F00001042
        000018630000186318631863186318630000FF7FFF7FFF7FFF7FFF7FFF7F1042
        0000186300001863186318631863186318630000FF7FFF7FFF7FFF7FFF7FFF7F
        0000186300001863186318631863186318631863104200000000000000001042
        1042186300001863186318631863186318631863186318631863186318631863
        186318630000}
      OnClick = ToolbarButton9712Click
    end
    object ToolbarButton9724: TToolbarButton97
      Left = 823
      Top = 205
      Width = 75
      Height = 25
      Caption = 'Val. finale'
      OnClick = ToolbarButton9721Click
    end
    object Label70: TLabel
      Left = 8
      Top = 104
      Width = 103
      Height = 13
      Caption = 'Resonsabile colloquio'
    end
    object DbDateEdit9720: TDbDateEdit97
      Left = 99
      Top = 53
      Width = 118
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      BevelOuter = bvNone
      ColorCalendar.ColorValid = clBlue
      Date = 23875
      DayNames.Monday = 'lu'
      DayNames.Tuesday = 'ma'
      DayNames.Wednesday = 'me'
      DayNames.Thursday = 'gi'
      DayNames.Friday = 've'
      DayNames.Saturday = 'sa'
      DayNames.Sunday = 'do'
      MonthNames.January = 'gennaio'
      MonthNames.February = 'febbraio'
      MonthNames.March = 'marzo'
      MonthNames.April = 'aprile'
      MonthNames.May = 'maggio'
      MonthNames.June = 'giugno'
      MonthNames.July = 'luglio'
      MonthNames.August = 'agosto'
      MonthNames.September = 'settembre'
      MonthNames.October = 'ottobre'
      MonthNames.November = 'novembre'
      MonthNames.December = 'dicembre'
      Options = [doButtonTabStop, doCanClear, doCanPopup, doIsMasked, doShowCancel, doShowToday]
      DataField = 'DataEsitoSestoColloquio'
      DataSource = dsSelPirola
    end
    object DbDateEdit9721: TDbDateEdit97
      Left = 381
      Top = 181
      Width = 119
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      BevelOuter = bvNone
      ColorCalendar.ColorValid = clBlue
      Date = 23875
      DayNames.Monday = 'lu'
      DayNames.Tuesday = 'ma'
      DayNames.Wednesday = 'me'
      DayNames.Thursday = 'gi'
      DayNames.Friday = 've'
      DayNames.Saturday = 'sa'
      DayNames.Sunday = 'do'
      MonthNames.January = 'gennaio'
      MonthNames.February = 'febbraio'
      MonthNames.March = 'marzo'
      MonthNames.April = 'aprile'
      MonthNames.May = 'maggio'
      MonthNames.June = 'giugno'
      MonthNames.July = 'luglio'
      MonthNames.August = 'agosto'
      MonthNames.September = 'settembre'
      MonthNames.October = 'ottobre'
      MonthNames.November = 'novembre'
      MonthNames.December = 'dicembre'
      Options = [doButtonTabStop, doCanClear, doCanPopup, doIsMasked, doShowCancel, doShowToday]
      DataField = 'DataEsitoSestoColloquio'
      DataSource = dsSelPirola
    end
    object DBComboBox9: TDBComboBox
      Left = 614
      Top = 181
      Width = 205
      Height = 21
      DataField = 'EsitoSestoColloquio'
      DataSource = dsSelPirola
      ItemHeight = 13
      Items.Strings = (
        ''
        'Positivo'
        'Negativo')
      TabOrder = 2
    end
    object DBMemo9: TDBMemo
      Left = 267
      Top = 54
      Width = 630
      Height = 119
      DataField = 'NoteSestocolloquio'
      DataSource = dsSelPirola
      TabOrder = 3
    end
    object DBEdit10: TDBEdit
      Left = 8
      Top = 120
      Width = 193
      Height = 21
      DataField = 'UtenteSestoColloquio'
      DataSource = dsSelPirola
      ReadOnly = True
      TabOrder = 4
    end
  end
  object Coll7: TAdvPanel
    Left = 0
    Top = -193
    Width = 945
    Height = 234
    Align = alTop
    TabOrder = 13
    UseDockManager = True
    OnDblClick = Coll7DblClick
    Version = '1.9.0.3'
    Caption.Color = clHighlight
    Caption.ColorTo = clNone
    Caption.Font.Charset = DEFAULT_CHARSET
    Caption.Font.Color = clBlack
    Caption.Font.Height = -11
    Caption.Font.Name = 'MS Sans Serif'
    Caption.Font.Style = []
    Caption.Text = '7� Colloquio'
    Caption.Visible = True
    StatusBar.Font.Charset = DEFAULT_CHARSET
    StatusBar.Font.Color = clWindowText
    StatusBar.Font.Height = -11
    StatusBar.Font.Name = 'Tahoma'
    StatusBar.Font.Style = []
    FullHeight = 234
    object Label48: TLabel
      Left = 8
      Top = 29
      Width = 73
      Height = 13
      Caption = 'Resp. colloquio'
    end
    object Label49: TLabel
      Left = 8
      Top = 56
      Width = 81
      Height = 13
      Caption = 'Data 7� colloquio'
    end
    object Label50: TLabel
      Left = 239
      Top = 56
      Width = 23
      Height = 13
      Caption = 'Note'
    end
    object Label51: TLabel
      Left = 269
      Top = 184
      Width = 106
      Height = 13
      Caption = 'Data esito 7� colloquio'
    end
    object Label52: TLabel
      Left = 527
      Top = 184
      Width = 81
      Height = 13
      Caption = 'Esito 7� colloquio'
    end
    object ToolbarButton9713: TToolbarButton97
      Left = 823
      Top = 180
      Width = 75
      Height = 25
      Caption = 'Salva'
      OnClick = ToolbarButton9713Click
    end
    object ToolbarButton9714: TToolbarButton97
      Left = 743
      Top = 24
      Width = 155
      Height = 25
      Caption = 'Cancella esito precedente'
      Glyph.Data = {
        E2010000424DE20100000000000042000000280000000F0000000D0000000100
        100003000000A001000000000000000000000000000000000000007C0000E003
        00001F0000001863186318631863186318631863186318631863186318631863
        1863186300001863104200000000000000000000104218631863186318631863
        186318630000186300001863FF7F1863FF7F1863000000001863186318631863
        18631863000018630000FF7F1863FF7F1863FF7F000010420000186318631863
        186318630000186300001863FF7F1863FF7F1863000010421042000018631863
        18631863000018631042FF7FFF7FFF7FFF7FFF7FFF7F00001042104200001863
        186318630000186318630000FF7FFF7FFF7FFF7FFF7FFF7F0000104210420000
        1863186300001863186318630000FF7FFF7FFF7FFF7FFF7FFF7F000010421042
        10421863000018631863186318630000FF7FFF7FFF7FFF7FFF7FFF7F00001042
        000018630000186318631863186318630000FF7FFF7FFF7FFF7FFF7FFF7F1042
        0000186300001863186318631863186318630000FF7FFF7FFF7FFF7FFF7FFF7F
        0000186300001863186318631863186318631863104200000000000000001042
        1042186300001863186318631863186318631863186318631863186318631863
        186318630000}
      OnClick = ToolbarButton9714Click
    end
    object ToolbarButton9725: TToolbarButton97
      Left = 823
      Top = 205
      Width = 75
      Height = 25
      Caption = 'Val. finale'
      OnClick = ToolbarButton9721Click
    end
    object Label71: TLabel
      Left = 8
      Top = 104
      Width = 103
      Height = 13
      Caption = 'Resonsabile colloquio'
    end
    object DbDateEdit9722: TDbDateEdit97
      Left = 99
      Top = 53
      Width = 118
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      BevelOuter = bvNone
      ColorCalendar.ColorValid = clBlue
      Date = 23875
      DayNames.Monday = 'lu'
      DayNames.Tuesday = 'ma'
      DayNames.Wednesday = 'me'
      DayNames.Thursday = 'gi'
      DayNames.Friday = 've'
      DayNames.Saturday = 'sa'
      DayNames.Sunday = 'do'
      MonthNames.January = 'gennaio'
      MonthNames.February = 'febbraio'
      MonthNames.March = 'marzo'
      MonthNames.April = 'aprile'
      MonthNames.May = 'maggio'
      MonthNames.June = 'giugno'
      MonthNames.July = 'luglio'
      MonthNames.August = 'agosto'
      MonthNames.September = 'settembre'
      MonthNames.October = 'ottobre'
      MonthNames.November = 'novembre'
      MonthNames.December = 'dicembre'
      Options = [doButtonTabStop, doCanClear, doCanPopup, doIsMasked, doShowCancel, doShowToday]
      DataField = 'DataEsitoSettimoColloquio'
      DataSource = dsSelPirola
    end
    object DbDateEdit9723: TDbDateEdit97
      Left = 381
      Top = 181
      Width = 119
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      BevelOuter = bvNone
      ColorCalendar.ColorValid = clBlue
      Date = 23875
      DayNames.Monday = 'lu'
      DayNames.Tuesday = 'ma'
      DayNames.Wednesday = 'me'
      DayNames.Thursday = 'gi'
      DayNames.Friday = 've'
      DayNames.Saturday = 'sa'
      DayNames.Sunday = 'do'
      MonthNames.January = 'gennaio'
      MonthNames.February = 'febbraio'
      MonthNames.March = 'marzo'
      MonthNames.April = 'aprile'
      MonthNames.May = 'maggio'
      MonthNames.June = 'giugno'
      MonthNames.July = 'luglio'
      MonthNames.August = 'agosto'
      MonthNames.September = 'settembre'
      MonthNames.October = 'ottobre'
      MonthNames.November = 'novembre'
      MonthNames.December = 'dicembre'
      Options = [doButtonTabStop, doCanClear, doCanPopup, doIsMasked, doShowCancel, doShowToday]
      DataField = 'DataEsitoSettimoColloquio'
      DataSource = dsSelPirola
    end
    object DBComboBox10: TDBComboBox
      Left = 614
      Top = 181
      Width = 205
      Height = 21
      DataField = 'EsitoSettimoColloquio'
      DataSource = dsSelPirola
      ItemHeight = 13
      Items.Strings = (
        ''
        'Positivo'
        'Negativo')
      TabOrder = 2
    end
    object DBMemo10: TDBMemo
      Left = 267
      Top = 54
      Width = 630
      Height = 119
      DataField = 'NoteSettimocolloquio'
      DataSource = dsSelPirola
      TabOrder = 3
    end
    object DBEdit11: TDBEdit
      Left = 8
      Top = 120
      Width = 193
      Height = 21
      DataField = 'utenteSettimoColloquio'
      DataSource = dsSelPirola
      ReadOnly = True
      TabOrder = 4
    end
  end
  object Coll9: TAdvPanel
    Left = 0
    Top = 275
    Width = 945
    Height = 234
    Align = alTop
    TabOrder = 14
    UseDockManager = True
    OnDblClick = Coll9DblClick
    Version = '1.9.0.3'
    Caption.Color = clHighlight
    Caption.ColorTo = clNone
    Caption.Font.Charset = DEFAULT_CHARSET
    Caption.Font.Color = clBlack
    Caption.Font.Height = -11
    Caption.Font.Name = 'MS Sans Serif'
    Caption.Font.Style = []
    Caption.Text = '9� Colloquio'
    Caption.Visible = True
    StatusBar.Font.Charset = DEFAULT_CHARSET
    StatusBar.Font.Color = clWindowText
    StatusBar.Font.Height = -11
    StatusBar.Font.Name = 'Tahoma'
    StatusBar.Font.Style = []
    FullHeight = 234
    object Label53: TLabel
      Left = 8
      Top = 29
      Width = 73
      Height = 13
      Caption = 'Resp. colloquio'
    end
    object Label54: TLabel
      Left = 8
      Top = 56
      Width = 81
      Height = 13
      Caption = 'Data 9� colloquio'
    end
    object Label55: TLabel
      Left = 239
      Top = 56
      Width = 23
      Height = 13
      Caption = 'Note'
    end
    object Label56: TLabel
      Left = 269
      Top = 184
      Width = 106
      Height = 13
      Caption = 'Data esito 9� colloquio'
    end
    object Label57: TLabel
      Left = 527
      Top = 184
      Width = 81
      Height = 13
      Caption = 'Esito 9� colloquio'
    end
    object ToolbarButton9715: TToolbarButton97
      Left = 823
      Top = 180
      Width = 75
      Height = 25
      Caption = 'Salva'
      OnClick = ToolbarButton9715Click
    end
    object ToolbarButton9716: TToolbarButton97
      Left = 743
      Top = 24
      Width = 155
      Height = 25
      Caption = 'Cancella esito precedente'
      Glyph.Data = {
        E2010000424DE20100000000000042000000280000000F0000000D0000000100
        100003000000A001000000000000000000000000000000000000007C0000E003
        00001F0000001863186318631863186318631863186318631863186318631863
        1863186300001863104200000000000000000000104218631863186318631863
        186318630000186300001863FF7F1863FF7F1863000000001863186318631863
        18631863000018630000FF7F1863FF7F1863FF7F000010420000186318631863
        186318630000186300001863FF7F1863FF7F1863000010421042000018631863
        18631863000018631042FF7FFF7FFF7FFF7FFF7FFF7F00001042104200001863
        186318630000186318630000FF7FFF7FFF7FFF7FFF7FFF7F0000104210420000
        1863186300001863186318630000FF7FFF7FFF7FFF7FFF7FFF7F000010421042
        10421863000018631863186318630000FF7FFF7FFF7FFF7FFF7FFF7F00001042
        000018630000186318631863186318630000FF7FFF7FFF7FFF7FFF7FFF7F1042
        0000186300001863186318631863186318630000FF7FFF7FFF7FFF7FFF7FFF7F
        0000186300001863186318631863186318631863104200000000000000001042
        1042186300001863186318631863186318631863186318631863186318631863
        186318630000}
      OnClick = ToolbarButton9716Click
    end
    object ToolbarButton9727: TToolbarButton97
      Left = 823
      Top = 205
      Width = 75
      Height = 25
      Caption = 'Val. finale'
      OnClick = ToolbarButton9721Click
    end
    object Label73: TLabel
      Left = 8
      Top = 104
      Width = 103
      Height = 13
      Caption = 'Resonsabile colloquio'
    end
    object DbDateEdit9724: TDbDateEdit97
      Left = 99
      Top = 53
      Width = 118
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      BevelOuter = bvNone
      ColorCalendar.ColorValid = clBlue
      Date = 23875
      DayNames.Monday = 'lu'
      DayNames.Tuesday = 'ma'
      DayNames.Wednesday = 'me'
      DayNames.Thursday = 'gi'
      DayNames.Friday = 've'
      DayNames.Saturday = 'sa'
      DayNames.Sunday = 'do'
      MonthNames.January = 'gennaio'
      MonthNames.February = 'febbraio'
      MonthNames.March = 'marzo'
      MonthNames.April = 'aprile'
      MonthNames.May = 'maggio'
      MonthNames.June = 'giugno'
      MonthNames.July = 'luglio'
      MonthNames.August = 'agosto'
      MonthNames.September = 'settembre'
      MonthNames.October = 'ottobre'
      MonthNames.November = 'novembre'
      MonthNames.December = 'dicembre'
      Options = [doButtonTabStop, doCanClear, doCanPopup, doIsMasked, doShowCancel, doShowToday]
      DataField = 'DataEsitoNonoColloquio'
      DataSource = dsSelPirola
    end
    object DbDateEdit9725: TDbDateEdit97
      Left = 381
      Top = 181
      Width = 119
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      BevelOuter = bvNone
      ColorCalendar.ColorValid = clBlue
      Date = 23875
      DayNames.Monday = 'lu'
      DayNames.Tuesday = 'ma'
      DayNames.Wednesday = 'me'
      DayNames.Thursday = 'gi'
      DayNames.Friday = 've'
      DayNames.Saturday = 'sa'
      DayNames.Sunday = 'do'
      MonthNames.January = 'gennaio'
      MonthNames.February = 'febbraio'
      MonthNames.March = 'marzo'
      MonthNames.April = 'aprile'
      MonthNames.May = 'maggio'
      MonthNames.June = 'giugno'
      MonthNames.July = 'luglio'
      MonthNames.August = 'agosto'
      MonthNames.September = 'settembre'
      MonthNames.October = 'ottobre'
      MonthNames.November = 'novembre'
      MonthNames.December = 'dicembre'
      Options = [doButtonTabStop, doCanClear, doCanPopup, doIsMasked, doShowCancel, doShowToday]
      DataField = 'DataEsitoNonoColloquio'
      DataSource = dsSelPirola
    end
    object DBComboBox11: TDBComboBox
      Left = 614
      Top = 181
      Width = 205
      Height = 21
      DataField = 'EsitoNonoColloquio'
      DataSource = dsSelPirola
      ItemHeight = 13
      Items.Strings = (
        ''
        'Positivo'
        'Negativo')
      TabOrder = 2
    end
    object DBMemo11: TDBMemo
      Left = 267
      Top = 54
      Width = 630
      Height = 119
      DataField = 'NoteNonocolloquio'
      DataSource = dsSelPirola
      TabOrder = 3
    end
    object DBEdit13: TDBEdit
      Left = 8
      Top = 120
      Width = 193
      Height = 21
      DataField = 'UtenteNonoColloquio'
      DataSource = dsSelPirola
      ReadOnly = True
      TabOrder = 4
    end
  end
  object Coll8: TAdvPanel
    Left = 0
    Top = 41
    Width = 945
    Height = 234
    Align = alTop
    TabOrder = 15
    UseDockManager = True
    OnDblClick = Coll8DblClick
    Version = '1.9.0.3'
    Caption.Color = clActiveCaption
    Caption.ColorTo = clNone
    Caption.Font.Charset = DEFAULT_CHARSET
    Caption.Font.Color = clBlack
    Caption.Font.Height = -11
    Caption.Font.Name = 'MS Sans Serif'
    Caption.Font.Style = []
    Caption.Text = '8� Colloquio'
    Caption.Visible = True
    StatusBar.Font.Charset = DEFAULT_CHARSET
    StatusBar.Font.Color = clWindowText
    StatusBar.Font.Height = -11
    StatusBar.Font.Name = 'Tahoma'
    StatusBar.Font.Style = []
    FullHeight = 234
    object Label58: TLabel
      Left = 8
      Top = 29
      Width = 73
      Height = 13
      Caption = 'Resp. colloquio'
    end
    object Label59: TLabel
      Left = 8
      Top = 56
      Width = 81
      Height = 13
      Caption = 'Data 8� colloquio'
    end
    object Label60: TLabel
      Left = 239
      Top = 56
      Width = 23
      Height = 13
      Caption = 'Note'
    end
    object Label61: TLabel
      Left = 269
      Top = 184
      Width = 106
      Height = 13
      Caption = 'Data esito 8� colloquio'
    end
    object Label62: TLabel
      Left = 527
      Top = 184
      Width = 81
      Height = 13
      Caption = 'Esito 8� colloquio'
    end
    object ToolbarButton9717: TToolbarButton97
      Left = 823
      Top = 180
      Width = 75
      Height = 25
      Caption = 'Salva'
      OnClick = ToolbarButton9717Click
    end
    object ToolbarButton9718: TToolbarButton97
      Left = 743
      Top = 24
      Width = 155
      Height = 25
      Caption = 'Cancella esito precedente'
      Glyph.Data = {
        E2010000424DE20100000000000042000000280000000F0000000D0000000100
        100003000000A001000000000000000000000000000000000000007C0000E003
        00001F0000001863186318631863186318631863186318631863186318631863
        1863186300001863104200000000000000000000104218631863186318631863
        186318630000186300001863FF7F1863FF7F1863000000001863186318631863
        18631863000018630000FF7F1863FF7F1863FF7F000010420000186318631863
        186318630000186300001863FF7F1863FF7F1863000010421042000018631863
        18631863000018631042FF7FFF7FFF7FFF7FFF7FFF7F00001042104200001863
        186318630000186318630000FF7FFF7FFF7FFF7FFF7FFF7F0000104210420000
        1863186300001863186318630000FF7FFF7FFF7FFF7FFF7FFF7F000010421042
        10421863000018631863186318630000FF7FFF7FFF7FFF7FFF7FFF7F00001042
        000018630000186318631863186318630000FF7FFF7FFF7FFF7FFF7FFF7F1042
        0000186300001863186318631863186318630000FF7FFF7FFF7FFF7FFF7FFF7F
        0000186300001863186318631863186318631863104200000000000000001042
        1042186300001863186318631863186318631863186318631863186318631863
        186318630000}
      OnClick = ToolbarButton9718Click
    end
    object ToolbarButton9726: TToolbarButton97
      Left = 823
      Top = 205
      Width = 75
      Height = 25
      Caption = 'Val. finale'
      OnClick = ToolbarButton9721Click
    end
    object Label72: TLabel
      Left = 8
      Top = 104
      Width = 103
      Height = 13
      Caption = 'Resonsabile colloquio'
    end
    object DbDateEdit9726: TDbDateEdit97
      Left = 99
      Top = 53
      Width = 118
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      BevelOuter = bvNone
      ColorCalendar.ColorValid = clBlue
      Date = 23875
      DayNames.Monday = 'lu'
      DayNames.Tuesday = 'ma'
      DayNames.Wednesday = 'me'
      DayNames.Thursday = 'gi'
      DayNames.Friday = 've'
      DayNames.Saturday = 'sa'
      DayNames.Sunday = 'do'
      MonthNames.January = 'gennaio'
      MonthNames.February = 'febbraio'
      MonthNames.March = 'marzo'
      MonthNames.April = 'aprile'
      MonthNames.May = 'maggio'
      MonthNames.June = 'giugno'
      MonthNames.July = 'luglio'
      MonthNames.August = 'agosto'
      MonthNames.September = 'settembre'
      MonthNames.October = 'ottobre'
      MonthNames.November = 'novembre'
      MonthNames.December = 'dicembre'
      Options = [doButtonTabStop, doCanClear, doCanPopup, doIsMasked, doShowCancel, doShowToday]
      DataField = 'DataEsitoOttavoColloquio'
      DataSource = dsSelPirola
    end
    object DbDateEdit9727: TDbDateEdit97
      Left = 381
      Top = 181
      Width = 119
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      BevelOuter = bvNone
      ColorCalendar.ColorValid = clBlue
      Date = 23875
      DayNames.Monday = 'lu'
      DayNames.Tuesday = 'ma'
      DayNames.Wednesday = 'me'
      DayNames.Thursday = 'gi'
      DayNames.Friday = 've'
      DayNames.Saturday = 'sa'
      DayNames.Sunday = 'do'
      MonthNames.January = 'gennaio'
      MonthNames.February = 'febbraio'
      MonthNames.March = 'marzo'
      MonthNames.April = 'aprile'
      MonthNames.May = 'maggio'
      MonthNames.June = 'giugno'
      MonthNames.July = 'luglio'
      MonthNames.August = 'agosto'
      MonthNames.September = 'settembre'
      MonthNames.October = 'ottobre'
      MonthNames.November = 'novembre'
      MonthNames.December = 'dicembre'
      Options = [doButtonTabStop, doCanClear, doCanPopup, doIsMasked, doShowCancel, doShowToday]
      DataField = 'DataEsitoOttavoColloquio'
      DataSource = dsSelPirola
    end
    object DBComboBox12: TDBComboBox
      Left = 614
      Top = 181
      Width = 205
      Height = 21
      DataField = 'EsitoOttavoColloquio'
      DataSource = dsSelPirola
      ItemHeight = 13
      Items.Strings = (
        ''
        'Positivo'
        'Negativo')
      TabOrder = 2
    end
    object DBMemo12: TDBMemo
      Left = 267
      Top = 54
      Width = 630
      Height = 119
      DataField = 'NoteOttavocolloquio'
      DataSource = dsSelPirola
      TabOrder = 3
    end
    object DBEdit12: TDBEdit
      Left = 8
      Top = 120
      Width = 193
      Height = 21
      DataField = 'UtenteottavoColloquio'
      DataSource = dsSelPirola
      ReadOnly = True
      TabOrder = 4
    end
  end
  object coll10: TAdvPanel
    Left = 0
    Top = 509
    Width = 945
    Height = 242
    Align = alTop
    TabOrder = 16
    UseDockManager = True
    Version = '1.9.0.3'
    Caption.Color = clHighlight
    Caption.ColorTo = clNone
    Caption.Font.Charset = DEFAULT_CHARSET
    Caption.Font.Color = clBlack
    Caption.Font.Height = -11
    Caption.Font.Name = 'MS Sans Serif'
    Caption.Font.Style = []
    Caption.Text = 'Valutazione Finale'
    Caption.Visible = True
    StatusBar.Font.Charset = DEFAULT_CHARSET
    StatusBar.Font.Color = clWindowText
    StatusBar.Font.Height = -11
    StatusBar.Font.Name = 'Tahoma'
    StatusBar.Font.Style = []
    FullHeight = 0
    object ToolbarButton9719: TToolbarButton97
      Left = 718
      Top = 20
      Width = 155
      Height = 25
      Caption = 'Cancella decisione finale'
      Glyph.Data = {
        E2010000424DE20100000000000042000000280000000F0000000D0000000100
        100003000000A001000000000000000000000000000000000000007C0000E003
        00001F0000001863186318631863186318631863186318631863186318631863
        1863186300001863104200000000000000000000104218631863186318631863
        186318630000186300001863FF7F1863FF7F1863000000001863186318631863
        18631863000018630000FF7F1863FF7F1863FF7F000010420000186318631863
        186318630000186300001863FF7F1863FF7F1863000010421042000018631863
        18631863000018631042FF7FFF7FFF7FFF7FFF7FFF7F00001042104200001863
        186318630000186318630000FF7FFF7FFF7FFF7FFF7FFF7F0000104210420000
        1863186300001863186318630000FF7FFF7FFF7FFF7FFF7FFF7F000010421042
        10421863000018631863186318630000FF7FFF7FFF7FFF7FFF7FFF7F00001042
        000018630000186318631863186318630000FF7FFF7FFF7FFF7FFF7FFF7F1042
        0000186300001863186318631863186318630000FF7FFF7FFF7FFF7FFF7FFF7F
        0000186300001863186318631863186318631863104200000000000000001042
        1042186300001863186318631863186318631863186318631863186318631863
        186318630000}
      OnClick = bCancEsito2Click
    end
    object ToolbarButton9720: TToolbarButton97
      Left = 798
      Top = 208
      Width = 75
      Height = 25
      Caption = 'Salva'
      OnClick = bSalva3Click
    end
    object Label63: TLabel
      Left = 494
      Top = 56
      Width = 47
      Height = 13
      Caption = 'Decisione'
    end
    object Label64: TLabel
      Left = 256
      Top = 56
      Width = 71
      Height = 13
      Caption = 'Data decisione'
    end
    object DBMemo13: TDBMemo
      Left = 246
      Top = 78
      Width = 628
      Height = 120
      DataField = 'NoteDecisione'
      DataSource = dsSelPirola
      TabOrder = 0
    end
    object DBComboBox13: TDBComboBox
      Left = 552
      Top = 53
      Width = 215
      Height = 21
      DataField = 'Decisione'
      DataSource = dsSelPirola
      ItemHeight = 13
      Items.Strings = (
        ''
        'Positivo'
        'Negativo')
      TabOrder = 1
    end
    object DbDateEdit9728: TDbDateEdit97
      Left = 366
      Top = 53
      Width = 110
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      BevelOuter = bvNone
      ColorCalendar.ColorValid = clBlue
      Date = 23875
      DayNames.Monday = 'lu'
      DayNames.Tuesday = 'ma'
      DayNames.Wednesday = 'me'
      DayNames.Thursday = 'gi'
      DayNames.Friday = 've'
      DayNames.Saturday = 'sa'
      DayNames.Sunday = 'do'
      MonthNames.January = 'gennaio'
      MonthNames.February = 'febbraio'
      MonthNames.March = 'marzo'
      MonthNames.April = 'aprile'
      MonthNames.May = 'maggio'
      MonthNames.June = 'giugno'
      MonthNames.July = 'luglio'
      MonthNames.August = 'agosto'
      MonthNames.September = 'settembre'
      MonthNames.October = 'ottobre'
      MonthNames.November = 'novembre'
      MonthNames.December = 'dicembre'
      Options = [doButtonTabStop, doCanClear, doCanPopup, doIsMasked, doShowCancel, doShowToday]
      DataField = 'DataDecisione'
      DataSource = dsSelPirola
    end
  end
  object qAnag: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'x0'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'select a.id,cognome,nome,datanascita,luogonascita,isnull(cvdataa' +
        'gg,cvinseritoindata) DataUltimoCV,'
      'c.descrizione as cliente,m.descrizione as Ruolo,r.progressivo, '
      
        'r.progressivo+'#39' - '#39' + m.descrizione as Ricerca,isnull(u.descrizi' +
        'one,u.nominativo) as Responsabile,u.email as RespEMAil'
      ',a.username,a.password'
      'from ebc_candidatiricerche cr'
      'join ebc_ricerche r'
      'on r.id=cr.idricerca'
      'join anagrafica a on cr.idanagrafica=a.id'
      'join anagaltreinfo aai '
      'on aai.idanagrafica=a.id'
      'join ebc_clienti c'
      'on c.id=r.idcliente'
      'join mansioni m'
      'on m.id = r.idmansione'
      'join ebc_ricercheutenti ru'
      'on ru.idricerca=r.id'
      'join users u on u.id=ru.idutente'
      'where a.id = :x0'
      'and RU.idruoloric = 1')
    Left = 336
    Top = 1
    object qAnagid: TAutoIncField
      FieldName = 'id'
      ReadOnly = True
    end
    object qAnagcognome: TStringField
      FieldName = 'cognome'
      Size = 30
    end
    object qAnagnome: TStringField
      FieldName = 'nome'
      Size = 30
    end
    object qAnagdatanascita: TDateTimeField
      FieldName = 'datanascita'
    end
    object qAnagluogonascita: TStringField
      FieldName = 'luogonascita'
      Size = 100
    end
    object qAnagDataUltimoCV: TDateTimeField
      FieldName = 'DataUltimoCV'
      ReadOnly = True
    end
    object qAnagcliente: TStringField
      FieldName = 'cliente'
      Size = 50
    end
    object qAnagRuolo: TStringField
      FieldName = 'Ruolo'
      Size = 100
    end
    object qAnagprogressivo: TStringField
      FieldName = 'progressivo'
      Size = 15
    end
    object qAnagRicerca: TStringField
      FieldName = 'Ricerca'
      ReadOnly = True
      Size = 68
    end
    object qAnagResponsabile: TStringField
      FieldName = 'Responsabile'
      ReadOnly = True
      Size = 50
    end
    object qAnagRespEMAil: TStringField
      FieldName = 'RespEMAil'
      Size = 50
    end
    object qAnagusername: TStringField
      FieldName = 'username'
      Size = 50
    end
    object qAnagpassword: TStringField
      FieldName = 'password'
      Size = 50
    end
  end
  object dsAnag: TDataSource
    DataSet = qAnag
    Left = 368
    Top = 1
  end
  object qSelPirola: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    AfterOpen = qSelPirolaAfterOpen
    AfterPost = qSelPirolaAfterPost
    Parameters = <
      item
        Name = 'x0'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select * from SelPirola'
      'where idcandric=:x0')
    Left = 552
    object qSelPirolaID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object qSelPirolaIDAnagrafica: TIntegerField
      FieldName = 'IDAnagrafica'
    end
    object qSelPirolaIDRicerca: TIntegerField
      FieldName = 'IDRicerca'
    end
    object qSelPirolaDataInvioTest: TDateTimeField
      FieldName = 'DataInvioTest'
    end
    object qSelPirolaDataTest: TDateTimeField
      FieldName = 'DataTest'
    end
    object qSelPirolaEsitoTest: TStringField
      FieldName = 'EsitoTest'
      Size = 500
    end
    object qSelPirolaDataPrimoColloquio: TDateTimeField
      FieldName = 'DataPrimoColloquio'
    end
    object qSelPirolaNotePrimocolloquio: TMemoField
      FieldName = 'NotePrimocolloquio'
      BlobType = ftMemo
    end
    object qSelPirolaDataEsitoPrimoColloquio: TDateTimeField
      FieldName = 'DataEsitoPrimoColloquio'
    end
    object qSelPirolaEsitoPrimoColloquio: TStringField
      FieldName = 'EsitoPrimoColloquio'
      Size = 200
    end
    object qSelPirolaDataSecondoColloquio: TDateTimeField
      FieldName = 'DataSecondoColloquio'
    end
    object qSelPirolaNoteSecondocolloquio: TMemoField
      FieldName = 'NoteSecondocolloquio'
      BlobType = ftMemo
    end
    object qSelPirolaDataEsitoSecondoColloquio: TDateTimeField
      FieldName = 'DataEsitoSecondoColloquio'
    end
    object qSelPirolaEsitoSecondoColloquio: TStringField
      FieldName = 'EsitoSecondoColloquio'
      Size = 200
    end
    object qSelPirolaDataDecisione: TDateTimeField
      FieldName = 'DataDecisione'
    end
    object qSelPirolaDecisione: TStringField
      FieldName = 'Decisione'
      Size = 200
    end
    object qSelPirolaNoteDecisione: TMemoField
      FieldName = 'NoteDecisione'
      BlobType = ftMemo
    end
    object qSelPirolaIDCandRic: TIntegerField
      FieldName = 'IDCandRic'
    end
    object qSelPirolaIDUtenteRespPrimoColloquio: TIntegerField
      FieldName = 'IDUtenteRespPrimoColloquio'
    end
    object qSelPirolaIDUtenteRespSecondoColloquio: TIntegerField
      FieldName = 'IDUtenteRespSecondoColloquio'
    end
    object qSelPirolaDataTerzoColloquio: TDateTimeField
      FieldName = 'DataTerzoColloquio'
    end
    object qSelPirolaNoteTerzocolloquio: TMemoField
      FieldName = 'NoteTerzocolloquio'
      BlobType = ftMemo
    end
    object qSelPirolaDataEsitoTerzoColloquio: TDateTimeField
      FieldName = 'DataEsitoTerzoColloquio'
    end
    object qSelPirolaEsitoTerzoColloquio: TStringField
      FieldName = 'EsitoTerzoColloquio'
      Size = 200
    end
    object qSelPirolaIDUtenteRespTerzoColloquio: TIntegerField
      FieldName = 'IDUtenteRespTerzoColloquio'
    end
    object qSelPirolaDataQuartoColloquio: TDateTimeField
      FieldName = 'DataQuartoColloquio'
    end
    object qSelPirolaNoteQuartocolloquio: TMemoField
      FieldName = 'NoteQuartocolloquio'
      BlobType = ftMemo
    end
    object qSelPirolaDataEsitoQuartoColloquio: TDateTimeField
      FieldName = 'DataEsitoQuartoColloquio'
    end
    object qSelPirolaEsitoQuartoColloquio: TStringField
      FieldName = 'EsitoQuartoColloquio'
      Size = 200
    end
    object qSelPirolaIDUtenteRespQuartoColloquio: TIntegerField
      FieldName = 'IDUtenteRespQuartoColloquio'
    end
    object qSelPirolaDataQuintoColloquio: TDateTimeField
      FieldName = 'DataQuintoColloquio'
    end
    object qSelPirolaNoteQuintocolloquio: TMemoField
      FieldName = 'NoteQuintocolloquio'
      BlobType = ftMemo
    end
    object qSelPirolaDataEsitoQuintoColloquio: TDateTimeField
      FieldName = 'DataEsitoQuintoColloquio'
    end
    object qSelPirolaEsitoQuintoColloquio: TStringField
      FieldName = 'EsitoQuintoColloquio'
      Size = 200
    end
    object qSelPirolaIDUtenteRespQuintoColloquio: TIntegerField
      FieldName = 'IDUtenteRespQuintoColloquio'
    end
    object qSelPirolaDataSestoColloquio: TDateTimeField
      FieldName = 'DataSestoColloquio'
    end
    object qSelPirolaNoteSestocolloquio: TMemoField
      FieldName = 'NoteSestocolloquio'
      BlobType = ftMemo
    end
    object qSelPirolaDataEsitoSestoColloquio: TDateTimeField
      FieldName = 'DataEsitoSestoColloquio'
    end
    object qSelPirolaEsitoSestoColloquio: TStringField
      FieldName = 'EsitoSestoColloquio'
      Size = 200
    end
    object qSelPirolaIDUtenteRespSestoColloquio: TIntegerField
      FieldName = 'IDUtenteRespSestoColloquio'
    end
    object qSelPirolaDataSettimoColloquio: TDateTimeField
      FieldName = 'DataSettimoColloquio'
    end
    object qSelPirolaNoteSettimocolloquio: TMemoField
      FieldName = 'NoteSettimocolloquio'
      BlobType = ftMemo
    end
    object qSelPirolaDataEsitoSettimoColloquio: TDateTimeField
      FieldName = 'DataEsitoSettimoColloquio'
    end
    object qSelPirolaEsitoSettimoColloquio: TStringField
      FieldName = 'EsitoSettimoColloquio'
      Size = 200
    end
    object qSelPirolaIDUtenteRespSettimoColloquio: TIntegerField
      FieldName = 'IDUtenteRespSettimoColloquio'
    end
    object qSelPirolaDataOttavoColloquio: TDateTimeField
      FieldName = 'DataOttavoColloquio'
    end
    object qSelPirolaNoteOttavocolloquio: TMemoField
      FieldName = 'NoteOttavocolloquio'
      BlobType = ftMemo
    end
    object qSelPirolaDataEsitoOttavoColloquio: TDateTimeField
      FieldName = 'DataEsitoOttavoColloquio'
    end
    object qSelPirolaEsitoOttavoColloquio: TStringField
      FieldName = 'EsitoOttavoColloquio'
      Size = 200
    end
    object qSelPirolaIDUtenteRespOttavoColloquio: TIntegerField
      FieldName = 'IDUtenteRespOttavoColloquio'
    end
    object qSelPirolaDataNonoColloquio: TDateTimeField
      FieldName = 'DataNonoColloquio'
    end
    object qSelPirolaNoteNonocolloquio: TMemoField
      FieldName = 'NoteNonocolloquio'
      BlobType = ftMemo
    end
    object qSelPirolaDataEsitoNonoColloquio: TDateTimeField
      FieldName = 'DataEsitoNonoColloquio'
    end
    object qSelPirolaEsitoNonoColloquio: TStringField
      FieldName = 'EsitoNonoColloquio'
      Size = 200
    end
    object qSelPirolaIDUtenteRespNonoColloquio: TIntegerField
      FieldName = 'IDUtenteRespNonoColloquio'
    end
    object qSelPirolaNumColloquioAttivo: TIntegerField
      FieldName = 'NumColloquioAttivo'
    end
    object qSelPirolaUntentePrimoColloquio: TStringField
      FieldKind = fkLookup
      FieldName = 'UtentePrimoColloquio'
      LookupDataSet = qUtentiLK
      LookupKeyFields = 'ID'
      LookupResultField = 'Nominativo'
      KeyFields = 'IDUtenteRespPrimoColloquio'
      Size = 50
      Lookup = True
    end
    object qSelPirolaUtenteSecondoColloquio: TStringField
      FieldKind = fkLookup
      FieldName = 'UtenteSecondoColloquio'
      LookupDataSet = qUtentiLK
      LookupKeyFields = 'ID'
      LookupResultField = 'Nominativo'
      KeyFields = 'IDUtenteRespSecondoColloquio'
      Size = 50
      Lookup = True
    end
    object qSelPirolaUtenteTerzoColloquio: TStringField
      FieldKind = fkLookup
      FieldName = 'UtenteTerzoColloquio'
      LookupDataSet = qUtentiLK
      LookupKeyFields = 'ID'
      LookupResultField = 'Nominativo'
      KeyFields = 'IDUtenteRespTerzoColloquio'
      Size = 50
      Lookup = True
    end
    object qSelPirolaUtenteQuartoColloquio: TStringField
      FieldKind = fkLookup
      FieldName = 'UtenteQuartoColloquio'
      LookupDataSet = qUtentiLK
      LookupKeyFields = 'ID'
      LookupResultField = 'Nominativo'
      KeyFields = 'IDUtenteRespQuartoColloquio'
      Size = 50
      Lookup = True
    end
    object qSelPirolaUtenteQuintoColloquio: TStringField
      FieldKind = fkLookup
      FieldName = 'UtenteQuintoColloquio'
      LookupDataSet = qUtentiLK
      LookupKeyFields = 'ID'
      LookupResultField = 'Nominativo'
      KeyFields = 'IDUtenteRespQuintoColloquio'
      Size = 50
      Lookup = True
    end
    object qSelPirolaUtenteSestoColloquio: TStringField
      FieldKind = fkLookup
      FieldName = 'UtenteSestoColloquio'
      LookupDataSet = qUtentiLK
      LookupKeyFields = 'ID'
      LookupResultField = 'Nominativo'
      KeyFields = 'IDUtenteRespSestoColloquio'
      Size = 50
      Lookup = True
    end
    object qSelPirolautenteSettimoColloquio: TStringField
      FieldKind = fkLookup
      FieldName = 'utenteSettimoColloquio'
      LookupDataSet = qUtentiLK
      LookupKeyFields = 'ID'
      LookupResultField = 'Nominativo'
      KeyFields = 'IDUtenteRespSettimoColloquio'
      Size = 50
      Lookup = True
    end
    object qSelPirolaUtenteottavoColloquio: TStringField
      FieldKind = fkLookup
      FieldName = 'UtenteottavoColloquio'
      LookupDataSet = qUtentiLK
      LookupKeyFields = 'ID'
      LookupResultField = 'Nominativo'
      KeyFields = 'IDUtenteRespOttavoColloquio'
      Size = 50
      Lookup = True
    end
    object qSelPirolaUtenteNonoColloquio: TStringField
      FieldKind = fkLookup
      FieldName = 'UtenteNonoColloquio'
      LookupDataSet = qUtentiLK
      LookupKeyFields = 'ID'
      LookupResultField = 'Nominativo'
      KeyFields = 'IDUtenteRespNonoColloquio'
      Size = 50
      Lookup = True
    end
    object qSelPirolaIDUtenteValFinale: TIntegerField
      FieldName = 'IDUtenteValFinale'
    end
  end
  object dsSelPirola: TDataSource
    DataSet = qSelPirola
    Left = 584
  end
  object OpenDialog1: TOpenDialog
    Left = 480
    Top = 16
  end
  object qTemp: TADOQuery
    Connection = Data.DB
    Parameters = <>
    Left = 264
    Top = 65528
  end
  object qUtentiLK: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from users')
    Left = 832
    Top = 16
    object qUtentiLKID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object qUtentiLKNominativo: TStringField
      FieldName = 'Nominativo'
      Size = 30
    end
    object qUtentiLKDescrizione: TStringField
      FieldName = 'Descrizione'
      Size = 50
    end
    object qUtentiLKEmail: TStringField
      FieldName = 'Email'
      Size = 50
    end
  end
  object q1: TADOQuery
    Connection = Data.DB
    Parameters = <>
    Left = 880
    Top = 8
  end
end
