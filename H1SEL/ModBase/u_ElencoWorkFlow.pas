unit u_ElencoWorkFlow;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     TB97, ExtCtrls, Db, ADODB, dxDBTLCl, dxGrClms, dxTL, dxDBCtrl, dxDBGrid,
     dxCntner, StdCtrls;

type
     TElencoWorkFlowForm = class(TForm)
          Panel2: TPanel;
          BitBtn1: TToolbarButton97;
          BAnnulla: TToolbarButton97;
          Panel1: TPanel;
          QElencoWF: TADOQuery;
          QElencoWFCognome: TStringField;
          QElencoWFNome: TStringField;
          QElencoWFIDAnagrafica: TIntegerField;
          QElencoWFDataTermineWF: TDateTimeField;
          QElencoWFID: TAutoIncField;
          QElencoWFProgressivo: TStringField;
          QElencoWFConcluso: TAutoIncField;
          DSElencoWF: TDataSource;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1Cognome: TdxDBGridMaskColumn;
          dxDBGrid1Nome: TdxDBGridMaskColumn;
          dxDBGrid1IDAnagrafica: TdxDBGridMaskColumn;
          dxDBGrid1DataTermineWF: TdxDBGridDateColumn;
          dxDBGrid1ID: TdxDBGridMaskColumn;
          dxDBGrid1Progressivo: TdxDBGridMaskColumn;
          dxDBGrid1Concluso: TdxDBGridMaskColumn;
          Label1: TLabel;
          ToolbarButton971: TToolbarButton97;
          QTemp: TADOQuery;
          QCK: TADOQuery;
          procedure BitBtn1Click(Sender: TObject);
          procedure BAnnullaClick(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure ToolbarButton971Click(Sender: TObject);
     private
    { Private declarations }
     public
          xIDRicerca: integer;
    { Public declarations }
     end;

var
     ElencoWorkFlowForm: TElencoWorkFlowForm;

implementation

uses ModuloDati, Main;

{$R *.DFM}

procedure TElencoWorkFlowForm.BitBtn1Click(Sender: TObject);
begin
     ModalResult := mrOK;
end;

procedure TElencoWorkFlowForm.BAnnullaClick(Sender: TObject);
begin
     ModalResult := mrCancel;
end;

procedure TElencoWorkFlowForm.FormShow(Sender: TObject);
begin
 //grafica



     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;


     QElencoWF.close;
     QElencoWF.Parameters[0].value := xIDRicerca;
     QElencoWF.Open;

end;

procedure TElencoWorkFlowForm.ToolbarButton971Click(Sender: TObject);
begin



     QCK.Close;
     QCK.SQL.Text := ' select ID from WFScadenzeConclusioni where IDxx=' + QElencoWFID.AsString;
     QCK.Open;
     if QCK.RecordCount > 0 then begin
          MessageDlg('Questo Work Flow � gi� stato gestito e concluso, impossibile proseguire', mtWarning, [mbOk], 0);
          exit;
     end else begin


          QCK.Close;
          QCK.SQL.Text := 'select * from WFScadenze where IDxx=' + QElencoWFID.AsString;
          QCK.Open;
          if QCK.RecordCount > 0 then begin
               MessageDlg('Questo Work Flow � gi� stato gestito, ma non � concluso ', mtWarning, [mbOk], 0);
               exit;

          end else begin

               if MessageDlg('sei sicuro di voler cancellare il Work Flow ' + QElencoWFCognome.asString + ' ' + QElencoWFNome.asString + '? ', mtWarning, [mbYes, mbNo], 0) = mrYes then begin
                    QTemp.close;
                    QTemp.SQL.Text := 'delete WFScadenze_Ricerche where id=' + QElencoWFID.AsString;
                    QTemp.ExecSQL;

                    QElencoWF.close;
                    QElencoWF.Parameters[0].value := xIDRicerca;
                    QElencoWF.Open;
               end;

          end;
     end;



end;

end.

