{
  TAFQuickMail   v2.01
  Copyright (C) 2002  Accalai Ferruccio - AfSoftware

  mailto:faccalai@tiscalinet.it - info@afsoftware.it

  webpage:www.afsoftware.it;

  Last Changed 23 september 2002:

                 removed property ToName.
                 
                 Added property ToCCEmail, TOBCCEmail.

                 Property Filename, ToEmail modified  to
                 TStrings for multiFile attachments and multiAddresses sending.

					30 september 2002:

                 Added property FromEmail: String;
                 Added property ComboAccounts: TComboBox;

                 Now is possible to send Messages from non predefined
                 Account. Mapi defined Accounts available by ComboAccounts (if assigned).

					23 october 2002:

					  Added function GetAddressBook(const ToList,CCList,BCCList: TStrings;
					  NumberOfEditFields: Cardinal; DialogCaption: String): Boolean;
					  ToList,CCList,BCCList: The Lists you have to populate.
					  NumberOfEditFields: the number of Fields you want to display in 
							the dialog - 1 display only field "TO";
										  - 2 display Fields "TO" and "CC";
										  - 3 display Fields "TO", "CC" and "BCC";
										  - 0 display a simply Dialog without selections fields.
					  DialogCaption: The caption of the AddressBook Dialog.

					  I.E. If you need a simple Dialog just to refresh or add recipients to address book
						  the code will be:
						  AfQuickMail1.GetAddressBook(nil,nil,nil,0,'Address Book');
						  In this case the result is not important.

					 Added property ShowDialog to show Sending Dialog.

					 Added some NotifyEvents.

                 
     
  Free for non-commercial use.
  For commercial use an acknowledgment 
  in your product documentation would be 
  appreciated but is not required.
                           
}
unit AFQuickMail;

interface

uses Windows, Messages, SysUtils, Classes,Mapi, Forms, Dialogs, Controls,
  CommDlg, Registry, StdCtrls;

type
  AFUlong = Cardinal; //Windows.ULONG Type compatibility for D3
  TAFQuickMail = class(TComponent) 
  private
          { Private declarations }
  DefaultAccount: String;
  DefaultEmail: String;
  ComboIndex: Integer;
  fFromEmail: STring;
  FTextToSend: TStrings;
  fToEmail: TStrings;
  fToCCEmail: TStrings;
  fToBCCEmail: TStrings;
  fSubject: String;
  fErrorMessage: String;
  fFileName: TStrings;
  FAccounts: TStrings;
  fComboAccounts: TComboBox;
  fShowDialog: Boolean;
  fAddressBook: TStrings;
  fOnAttachmentNotFound: TNotifyEvent;
  fOnAttachmentOpenFailure: TNotifyEvent;
  fOnTooManyFiles: TNotifyEvent;
  fOnTooManyRecips: TNotifyEvent;
  fOnTextTooLarge: TNotifyEvent;
  fOnUserAbort: TNotifyEvent;
  Procedure SetToEmail(value: TStrings);
  Procedure SetToCCEmail(value: TStrings);
  Procedure SetToBCCEmail(value: TStrings);
  Procedure SetFileNAme(value: TStrings);
  procedure SetErrorMessage(value: String);
  procedure SetTextToSend(Value: TStrings);
  procedure SetCombo(Value: TComboBox);
  procedure GetAccounts; 
  protected
								 { Protected declarations }
  function SendMail(const Subject, Body,SenderName, SenderEMail: string;
	 BCCEmail,CCEmail, RecipientEMail,Filenamelist: TStrings): Integer;
  function AccountExists(Account:String):Boolean;
  function CheckIfOnEvent(ErrorResult: Integer): Boolean;
  procedure ReplaceDefaultAccount; 
  public
								 { Public declarations }
  constructor Create(AOwner: TComponent); override;
  destructor Destroy; override;
  function Execute: Boolean; 
  procedure ResetAll;
  function GetAddressBook(const ToList,CCList,BCCList: TStrings;
	  NumberOfEditFields: Cardinal; DialogCaption: String): Boolean;
  published
          { Published declarations }
  property FromEmail: String read fFromEmail write fFromEmail; 
  property TextToSend: TStrings read fTextToSend write SetTextToSend; 
  property ToEmail:TStrings read fToEmail write SetToEmail; 
  property Subject: String read fSubject write fSubject; 
  property ToCCEmail: tStrings read fToCCEmail write SetToCCEmail; 
  property ToBCCEmail: tStrings read fToBCCEmail write SetToBCCEmail; 
  property ErrorMessage: String read fErrorMessage write setErrorMessage; 
  property FileNames: tstrings read fFileName write setFileName; 
  Property ComboAccounts: TComboBox read fComboAccounts write SetCombo; 
  property ShowDialog: Boolean read fShowDialog write fShowDialog;
  property OnAttachmentNotFound: TNotifyEvent read fOnAttachmentNotFound write
				fOnAttachmentNotFound; 
  property OnAttachmentOpenFailure: TNotifyEvent read fOnAttachmentOpenFailure write 
				fOnAttachmentOpenFailure; 
  property OnTooManyFiles: TNotifyEvent read fOnTooManyFiles write fOnTooManyFiles;
  property OnTooManyRecips: TNotifyEvent read fOnTooManyRecips write fOnTooManyRecips; 
  property OnTextTooLarge: TNotifyEvent read fOnTextTooLarge write fOnTextTooLarge; 
  property OnUserAbort: TNotifyEvent read fOnUserAbort write fOnUserAbort;
end;
const
  WKey: String = ('\Software\Microsoft\Internet Account Manager');
procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('AF', [TAFQuickMail]);
end;
constructor TAFQuickMail.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  fTextToSend := TStringList.Create;
  fFileName := TStringList.Create;
  fToEmail := TSTringList.Create;
  fToCCEmail := TSTringList.Create;
  fToBCCEmail := TSTringList.Create;
  fAccounts := TStringlist.Create;
  fShowDialog := False;
  errormessage := '';
  GetAccounts;
end;
destructor TAfQuickMail.Destroy;
begin
  fTextToSend.Free;
  ffilename.Free;
  fToEmail.Free;
  fToCCEmail.Free;
  fToBCCEmail.Free;
  fAccounts.Free;
  inherited Destroy;
end;
function TAFQuickMail.SendMail(const Subject, Body, SenderName, SenderEMail:
  string; BCCEmail,CCEmail, RecipientEMail,FileNamelist:Tstrings): Integer;
TYPE
  TAttachAccessArray = ARRAY [0..0] OF TMapiFileDesc;
  PAttachAccessArray = ^TAttachAccessArray;
var
  MailMessage: TMapiMessage;
  lpSender: TMapiRecipDesc;
  lpRecipient,pRecips: PMapiRecipDesc;
  SM: TFNMapiSendMail;
  MAPIModule: HModule;
  Attachments : PAttachAccessArray;
  iCount,rcount,i : INTEGER;
  FileName : STRING;
  ReplaceDefault: Boolean;
begin

  FillChar(mailMessage, SizeOf(mailMessage), 0);
  Mailmessage.NRecipCount := bccEmail.Count+ccemail.Count+RecipientEmail.
    Count;
  getmem(lprecipient, Mailmessage.nrecipcount*sizeof(TMapiRecipDesc));
  try with MailMessage do
      begin
        if (Subject <> '') then lpszSubject := PChar(Subject);
        if (Body <> '') then lpszNoteText := PChar(Body);
        if (SenderEmail <> '') then
          If Uppercase(SenderEmail) <> Uppercase(DefaultEmail) then
            ReplaceDefault := AccountExists(SenderEmail);
        pRecips := lpRecipient;
        if nrecipcount > 0 then
        begin
          for i := 0 to RecipientEmail.Count-1 do
          begin
            pRecips^.ulRecipClass := MAPI_TO;
            pRecips^.lpszName := PChar(RecipientEMail.Strings[i]);
            pRecips^.lpszAddress := StrNew(PChar('SMTP:' + RecipientEmail.
              Strings[i]));
            pRecips^.ulReserved := 0;
            pRecips^.ulEIDSize := 0;
            pRecips^.lpEntryID := nil;
            Inc(pRecips);
          end;
          for i := 0 to CCEmail.Count-1 do
          begin
            pRecips^.ulRecipClass := MAPI_CC;
            pRecips^.lpszName := PChar(ccEMail.Strings[i]);
            pRecips^.lpszAddress := StrNew(PChar('SMTP:' + ccEmail.Strings[i])
              );
            pRecips^.ulReserved := 0;
            pRecips^.ulEIDSize := 0;
            pRecips^.lpEntryID := nil;
            Inc(pRecips);
          end ;
          for i := 0 to BCCEmail.Count-1 do
          begin
            pRecips^.ulRecipClass := MAPI_BCC;
            pRecips^.lpszName := PChar(bccEMail.Strings[i]);
            pRecips^.lpszAddress := StrNew(PChar('SMTP:' + bccEmail.Strings[i]
              ));
            pRecips^.ulReserved := 0;
            pRecips^.ulEIDSize := 0;
            pRecips^.lpEntryID := nil;
            Inc(pRecips);
          end ;
        end;
        lpRecips := lpRecipient;
        GetMem(Attachments,SizeOf(TMapiFileDesc) * filenamelist.Count);
        nFileCount := filenamelist.Count;
        if filenamelist.Count > 0 then
        begin
          FOR iCount := 0 TO (filenamelist.Count - 1) do
          begin
            FileName := filenamelist[iCount];
            Attachments[iCount].ulReserved := 0;
            Attachments[iCount].flFlags := 0;
            Attachments[iCount].nPosition := AFULONG($FFFFFFFF);
            Attachments[iCount].lpszPathName := StrNew(PChar(FileName));
            Attachments[iCount].lpszFileName := StrNew(PChar(ExtractFileName(
              FileName)));
            Attachments[iCount].lpFileType := NIL;
          END;
          lpFiles := @Attachments^;
        end
        else
        begin
          nFileCount := 0;
          lpFiles := nil;
        end;
      end;
  MAPIModule := LoadLibrary(PChar(MAPIDLL));
  if MAPIModule = 0 then Result := -1
  else try @SM := GetProcAddress(MAPIModule, 'MAPISendMail');
  if @SM <> nil then
  begin
    if fShowDialog then Result := SM(0, Application.Handle, MailMessage,
      MAPI_DIALOG or MAPI_LOGON_UI or MAPI_NEW_SESSION, 0)
    else Result := SM(0, Application.Handle, MailMessage,0, 0);
  end
  else Result := 1;
  finally FreeLibrary(MAPIModule);
end;
  finally PRecips := lpRecipient;
  for i := 1 to MailMessage.nRecipCount do
  begin
    StrDispose(PRecips^.lpszAddress);
    Inc(PRecips)
  end;
  FreeMem(lpRecipient, MailMessage.nRecipCount * sizeof(TMapiRecipDesc));
  If ReplaceDefault then ReplaceDefaultAccount;
end;
end;
procedure TAFQuickMail.SetErrorMessage(value: String);
const
  BaseError = 'Error sending mail';
begin
  if value = '' then value := BaseError;
  if fErrorMessage <> value then FErrorMessage := value;
end;
Function TAFQuickMail.Execute: Boolean;
var
  OutResult: Integer;
  TheEvent: TNotifyEvent;
begin
  result := False;
  OutResult := SendMail(FSubject,fTextToSend.Text,'',fFromEMail,fTOBCCEmail,
    fTOCCEmail, fToEmail,FFileName);
  if OutResult <> 0 then
  begin
    result := False;
    if not CheckIfOnEvent(OutResult) then MessageDlg(fErrorMessage+' (' +
      IntToStr(OutResult) + ').', mtError,[mbOK], 0);
  end
  else result := True;
end;
procedure TAFQuickMail.SetTextToSend(Value: TStrings);
begin
  fTextToSend.Assign(value);
end;

procedure TAFQuickMail.SetFileNAme(Value: tStrings);
begin
  if fFileName <> value then FFileName := Value;
end;
procedure TAFQuickMail.SetToEmail(Value: tStrings);
begin
  if fToEmail <> value then FToEmail := Value;
end;
procedure TAFQuickMail.SetToCCEmail(Value: tStrings);
begin
  if fToCCEmail <> value then FToCCEmail := Value;
end;
procedure TAFQuickMail.SetToBCCEmail(Value: tStrings);
begin
  if fToBCCEmail <> value then FToBCCEmail := Value;
end;
procedure TAFQuickMail.ResetAll;
begin
  FToBCCEmail.Clear;
  FToCCEmail.Clear;
  FToEmail.Clear;
  FFilename.Clear;
  fTextToSend.Clear;
  FSubject := '';
end;

procedure TAFQuickMail.getAccounts;
var
  Reg: TRegistry;
  MyList: Tstrings;
  i,counter: Integer;
begin
  counter := 0;
  MyList := TStringList.Create;
  Reg := Tregistry.Create;
  Reg.RootKey := HKEY_CURRENT_USER;
  Reg.openKey(WKey,False);
  DefaultAccount := Reg.ReadString('Default Mail Account');
  Reg.CloseKey;
  Reg.OpenKey(WKey+'\Accounts',False);
  Reg.GetKeyNames(MyList);
  Reg.CloseKey;
  For i := 0 to MyList.Count-1 do
  begin
    Reg.OpenKey(WKey+'\Accounts\'+MyList[i],False);
    If Reg.ValueExists('SMTP Email Address') then
    begin
      fAccounts.Add(reg.ReadString('SMTP Email Address'));
      If MyList[i] = DefaultAccount then
      begin
        ComboIndex := Counter;
        DefaultEmail := reg.ReadString('SMTP Email Address');
      end;
      Inc(Counter);
    end;
    Reg.CloseKey;
  end;
  MyList.Free;
  reg.Free;
end;
procedure TAFQuickMail.SetCombo(value: TComboBox);
begin
  If value <> fComboAccounts then fComboAccounts := value;
  fComboAccounts.items.Assign(fAccounts);
  If Not (csDesigning in ComponentState) then fcomboAccounts.ItemIndex :=
    ComboIndex;
end;
function TAFQuickMail.AccountExists(Account:String):Boolean;
var
  Reg: Tregistry;
  i: Integer;
  MyList : TStrings;
  NewDefault:String;
begin
  MyList := TStringList.Create;
  Reg := Tregistry.Create;
  reg.RootKey := HKEY_CURRENT_USER;
  Reg.OpenKey(WKey+'\Accounts',False);
  Reg.GetKeyNames(MyList);
  Reg.CloseKey;
  For i := 0 to MyList.Count-1 do
  begin
    Reg.OpenKey(WKey+'\Accounts\'+MyList[i],False);
    If Reg.ValueExists('SMTP Email Address') then
      If Uppercase(Reg.ReadString('SMTP Email Address')) = Uppercase(Account)
        then
      begin
        NewDefault := MyList[i];
        Reg.CloseKey;
        Reg.OpenKey(WKey,False);
        Reg.WriteString('Default Mail Account',MyList[i]);
        Reg.CloseKey;
        result := True;
        Break;
      end;
  end;
  MyList.Free;
  Reg.Free;
end;
procedure TAFQuickMail.ReplaceDefaultAccount;
var
  Reg: Tregistry;
begin
  Reg := TRegistry.Create;
  Reg.OpenKey(WKey,False);
  Reg.WriteString('Default Mail Account',DefaultAccount);
  Reg.CloseKey;
end;

Function TAFQuickMail.GetAddressBook(const ToList,CCList,BCCList: TStrings;
  NumberOfEditFields: Cardinal;DialogCaption: String): Boolean;
var
  lpRecip: TMapiRecipDesc;
  intRecips: AFULONG;
  lpRecips: PMapiRecipDesc;
  i: Integer;
begin
  result := False;
  if (MAPIAddress(0, Application.Handle, PChar(DialogCaption),
    NumberOfEditFields, '', 0, lpRecip, 0, 0, @intRecips, lpRecips) =
    SUCCESS_SUCCESS) then
  begin
    If NumberOfEditFields > 0 then
    begin
      for i := 0 to intRecips-1 do
        case PMapiRecipDesc(PChar(lpRecips) + i*SizeOf(TMapiRecipDesc))^.
          ulRecipClass of
          MAPI_TO:
            If ToList <> nil then ToList.Add(PMapiRecipDesc(PChar(lpRecips) +
              i*SizeOf(TMapiRecipDesc))^.lpszAddress);
          MAPI_CC:
            If CCList <> nil then CCList.Add(PMapiRecipDesc(PChar(lpRecips) +
              i*SizeOf(TMapiRecipDesc))^.lpszAddress);
          MAPI_BCC:
            If BCCList <> nil then BCCList.Add(PMapiRecipDesc(PChar(lpRecips)
              + i*SizeOf(TMapiRecipDesc))^.lpszAddress);
        end;
      result := True;
    end;
  end;
  MAPIFreeBuffer(lpRecips);
end;
function TAFQuickMail.CheckIfOnEvent(ErrorResult: Integer):Boolean;
begin
  result := False;
  case ErrorResult of
    MAPI_E_ATTACHMENT_NOT_FOUND:
      If Assigned(fOnAttachmentNotFound) then
      begin
        fOnAttachmentNotFound(self);
        result := True;
      end;
    MAPI_E_ATTACHMENT_OPEN_FAILURE:
      If Assigned(fOnAttachmentOpenFailure) then
      begin
        fOnAttachmentOpenFailure(self);
        result := True;
      end;
    MAPI_E_TEXT_TOO_LARGE:
      If Assigned(fOnTextTooLarge) then
      begin
        fOnTextTooLarge(self);
        result := True;
      end;
    MAPI_E_TOO_MANY_FILES:
      If Assigned(fOnTooManyFiles) then
      begin
        fOnTooManyFiles(self);
        result := True;
      end;
    MAPI_E_TOO_MANY_RECIPIENTS:
      If Assigned(fOnTooManyRecips) then
      begin
        fOnTooManyRecips(self);
        result := True;
      end;
    MAPI_E_USER_ABORT:
      If Assigned(fOnUserAbort) then
      begin
        fOnUserAbort(self);
        result := True;
      end;
  end;
end;

end.
