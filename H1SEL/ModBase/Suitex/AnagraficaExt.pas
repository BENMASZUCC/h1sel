unit AnagraficaExt;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     Db,DBTables,ExtCtrls,DBCtrls,StdCtrls,Buttons,TB97,Mask,dxTL,
     dxDBCtrl,dxDBTLCl,dxGrClms,dxCntner,dxDBGrid,dxGrClEx,dxLayout,
     ComCtrls;

type
     TFrmSUITEXAnagraficaExt=class(TFrame)
          PanTitolo: TPanel;
          QSUITEXAnagExt: TQuery;
          DsQSUITEXAnagExt: TDataSource;
          QProdArticolo: TQuery;
          QProdFascia: TQuery;
          QProdImmagine: TQuery;
          QProdMarchio: TQuery;
          QProdArticoloID: TAutoIncField;
          QProdArticoloArticolo: TStringField;
          QProdFasciaID: TAutoIncField;
          QProdFasciaFascia: TStringField;
          QProdImmagineID: TAutoIncField;
          QProdImmagineImmagine: TStringField;
          QProdMarchioID: TAutoIncField;
          QProdMarchioMarchio: TStringField;
          QProdTipoDisp: TQuery;
          QProdTipoDispID: TAutoIncField;
          QProdTipoDispTipoDist: TStringField;
          QTDgTipologia: TQuery;
          QTDgTipologiaID: TAutoIncField;
          QTDgTipologiaTipologia: TStringField;
          UpdQSUITEXAnagExt: TUpdateSQL;
          QAnagSUITEX: TQuery;
          DsQAnagSUITEX: TDataSource;
          UpdQAnagSUITEX: TUpdateSQL;
          QAnagSUITEXID: TAutoIncField;
          QAnagSUITEXIDAnagrafica: TIntegerField;
          QAnagSUITEXIDProdArticolo: TIntegerField;
          QAnagSUITEXIDProdFascia: TIntegerField;
          QAnagSUITEXIDProdImmagine: TIntegerField;
          QAnagSUITEXIDProdMarchio: TIntegerField;
          QAnagSUITEXIDProdTipoDist: TIntegerField;
          QAnagSUITEXIDCliente: TIntegerField;
          QAnagSUITEXArticolo: TStringField;
          QAnagSUITEXFascia: TStringField;
          QAnagSUITEXImmagine: TStringField;
          QAnagSUITEXMarchio: TStringField;
          QAnagSUITEXTipoDist: TStringField;
          QClientiLK: TQuery;
          QAnagSUITEXCliente: TStringField;
          QClienti2LK: TQuery;
          QClienti2LKID: TAutoIncField;
          QClienti2LKDescrizione: TStringField;
          QSUITEXAnagExtDirigenteIDCliente: TIntegerField;
          QSUITEXAnagExtDirCliente: TStringField;
          QSUITEXAnagExtIDAnagrafica: TIntegerField;
          QSUITEXAnagExtTipoPersona: TStringField;
          QSUITEXAnagExtDirigenteTipo: TStringField;
          QSUITEXAnagExtDirigenteStato: TStringField;
          QAnagSoci: TQuery;
          DsQAnagSoci: TDataSource;
          UpdQAnagSoci: TUpdateSQL;
          QAnagSociID: TAutoIncField;
          QAnagSociIDAnagrafica: TIntegerField;
          QAnagSociIDAnagSocio: TIntegerField;
          QAnagSociNote: TMemoField;
          QAnagLK: TQuery;
          QAnagLKID: TAutoIncField;
          QAnagLKCognome: TStringField;
          QAnagLKNome: TStringField;
          DsQAnagLK: TDataSource;
          QAnagSociSocio: TStringField;
          PCSuitex: TPageControl;
          TabSheet1: TTabSheet;
          TabSheet2: TTabSheet;
          TabSheet3: TTabSheet;
          Label7: TLabel;
          DBComboBox1: TDBComboBox;
          GroupBox2: TGroupBox;
          Label8: TLabel;
          Label9: TLabel;
          Label10: TLabel;
          DBEdit1: TDBEdit;
          DBEdit3: TDBEdit;
          DBLookupComboBox1: TDBLookupComboBox;
          Panel1: TPanel;
          dxDBGrid1: TdxDBGrid;
          Panel3: TPanel;
          BRigaNew: TToolbarButton97;
          BRigaDel: TToolbarButton97;
          SpeedButton1: TSpeedButton;
          SpeedButton2: TSpeedButton;
          SpeedButton3: TSpeedButton;
          SpeedButton4: TSpeedButton;
          SpeedButton5: TSpeedButton;
          SpeedButton6: TSpeedButton;
          Bevel1: TBevel;
          Label1: TLabel;
          BRigaOK: TToolbarButton97;
          BRigaCan: TToolbarButton97;
          Panel4: TPanel;
          Panel8: TPanel;
          dxDBGrid3: TdxDBGrid;
          Panel9: TPanel;
          BAspirazNew: TToolbarButton97;
          BAspirazDel: TToolbarButton97;
          BAspirazOK: TToolbarButton97;
          BAspirazCan: TToolbarButton97;
          Panel10: TPanel;
          QAnagAspiraz: TQuery;
          DsQAnagAspiraz: TDataSource;
          UpdQAnagAspiraz: TUpdateSQL;
          TabSheet4: TTabSheet;
          Panel5: TPanel;
          Panel6: TPanel;
          Panel7: TPanel;
          BSocioNew: TToolbarButton97;
          BSocioOK: TToolbarButton97;
          BSocioCan: TToolbarButton97;
          dxDBGrid2: TdxDBGrid;
          BSocioDel: TToolbarButton97;
          QAnagAspirazID: TAutoIncField;
          QAnagAspirazIDAnagrafica: TIntegerField;
          QAnagAspirazIDSettore: TIntegerField;
          QAnagAspirazIDProdArticolo: TIntegerField;
          QAnagAspirazIDProdFascia: TIntegerField;
          QAnagAspirazIDProdImmagine: TIntegerField;
          QAnagAspirazIDProdMarchio: TIntegerField;
          QAnagAspirazIDProdTipoDist: TIntegerField;
          QAnagAspirazNazione: TStringField;
          QAnagAspirazRegione: TStringField;
          QSettoriLK: TQuery;
          QSettoriLKID: TAutoIncField;
          QSettoriLKAttivita: TStringField;
          QAnagAspirazSettore: TStringField;
          QAnagAspirazArticolo: TStringField;
          QAnagAspirazFascia: TStringField;
          QAnagAspirazImmagine: TStringField;
          QAnagAspirazMarchio: TStringField;
          QAnagAspirazTipoDist: TStringField;
          dxDBGrid3Nazione: TdxDBGridMaskColumn;
          dxDBGrid3Regione: TdxDBGridMaskColumn;
          dxDBGrid3Settore: TdxDBGridLookupColumn;
          dxDBGrid3Articolo: TdxDBGridLookupColumn;
          dxDBGrid3Fascia: TdxDBGridLookupColumn;
          dxDBGrid3Immagine: TdxDBGridLookupColumn;
          dxDBGrid3Marchio: TdxDBGridLookupColumn;
          dxDBGrid3TipoDist: TdxDBGridLookupColumn;
    dxDBGrid2Socio: TdxDBGridLookupColumn;
    dxDBGrid2Note: TdxDBGridBlobColumn;
    QSUITEXAnagExtIDTDgTipologia: TIntegerField;
    QSUITEXAnagExtTdGTipologia: TStringField;
    Label2: TLabel;
    DBLookupComboBox2: TDBLookupComboBox;
    QAnagSUITEXIDSettore: TIntegerField;
    QAnagSUITEXDataDal: TDateTimeField;
    QAnagSUITEXDataAl: TDateTimeField;
    QAnagSUITEXAttuale: TBooleanField;
    QAnagSUITEXNazione: TStringField;
    QAnagSUITEXRegione: TStringField;
    QAnagSUITEXSettore: TStringField;
    dxDBGrid1Articolo: TdxDBGridLookupColumn;
    dxDBGrid1Fascia: TdxDBGridLookupColumn;
    dxDBGrid1Immagine: TdxDBGridLookupColumn;
    dxDBGrid1Marchio: TdxDBGridLookupColumn;
    dxDBGrid1TipoDist: TdxDBGridLookupColumn;
    dxDBGrid1Cliente: TdxDBGridLookupColumn;
    dxDBGrid1DataDal: TdxDBGridDateColumn;
    dxDBGrid1DataAl: TdxDBGridDateColumn;
    dxDBGrid1Attuale: TdxDBGridCheckColumn;
    dxDBGrid1Nazione: TdxDBGridMaskColumn;
    dxDBGrid1Regione: TdxDBGridMaskColumn;
    dxDBGrid1Settore: TdxDBGridLookupColumn;
          procedure SpeedButton1Click(Sender: TObject);
          procedure SpeedButton2Click(Sender: TObject);
          procedure SpeedButton3Click(Sender: TObject);
          procedure SpeedButton4Click(Sender: TObject);
          procedure SpeedButton5Click(Sender: TObject);
          procedure SpeedButton6Click(Sender: TObject);
          procedure QSUITEXAnagExtAfterPost(DataSet: TDataSet);
          procedure QSUITEXAnagExtBeforeClose(DataSet: TDataSet);
          procedure QSUITEXAnagExtAfterOpen(DataSet: TDataSet);
          procedure BRigaNewClick(Sender: TObject);
          procedure BRigaDelClick(Sender: TObject);
          procedure BRigaOKClick(Sender: TObject);
          procedure BRigaCanClick(Sender: TObject);
          procedure QAnagSUITEXAfterPost(DataSet: TDataSet);
          procedure DsQAnagSUITEXStateChange(Sender: TObject);
          procedure QAnagSUITEXAfterInsert(DataSet: TDataSet);
          procedure QAnagSociAfterPost(DataSet: TDataSet);
          procedure QAnagSociAfterInsert(DataSet: TDataSet);
          procedure BSocioNewClick(Sender: TObject);
          procedure BSocioDelClick(Sender: TObject);
          procedure BSocioOKClick(Sender: TObject);
          procedure BSocioCanClick(Sender: TObject);
          procedure DsQAnagSociStateChange(Sender: TObject);
          procedure QAnagSUITEXBeforeClose(DataSet: TDataSet);
          procedure QAnagSociBeforeClose(DataSet: TDataSet);
          procedure QAnagAspirazAfterPost(DataSet: TDataSet);
          procedure QAnagAspirazAfterInsert(DataSet: TDataSet);
          procedure QAnagAspirazBeforeClose(DataSet: TDataSet);
          procedure DsQAnagAspirazStateChange(Sender: TObject);
          procedure BAspirazNewClick(Sender: TObject);
          procedure BAspirazDelClick(Sender: TObject);
          procedure BAspirazOKClick(Sender: TObject);
          procedure BAspirazCanClick(Sender: TObject);
    procedure PCSuitexChange(Sender: TObject);
    procedure QAnagSociAfterOpen(DataSet: TDataSet);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

implementation

uses ModuloDati,uUtilsVarie;

{$R *.DFM}

procedure TFrmSUITEXAnagraficaExt.SpeedButton1Click(Sender: TObject);
begin
     OpenTab('SUITEX_ProdArticolo', ['ID','Articolo'], ['Articolo']);
     QProdArticolo.Close;
     QProdArticolo.Open;
end;

procedure TFrmSUITEXAnagraficaExt.SpeedButton2Click(Sender: TObject);
begin
     OpenTab('SUITEX_ProdFascia', ['ID','Fascia'], ['Fascia']);
     QProdFascia.Close;
     QProdFascia.Open;
end;

procedure TFrmSUITEXAnagraficaExt.SpeedButton3Click(Sender: TObject);
begin
     OpenTab('SUITEX_ProdImmagine', ['ID','Immagine'], ['Immagine']);
     QProdImmagine.Close;
     QProdImmagine.Open;
end;

procedure TFrmSUITEXAnagraficaExt.SpeedButton4Click(Sender: TObject);
begin
     OpenTab('SUITEX_ProdMarchio', ['ID','Marchio'], ['Marchio']);
     QProdMarchio.Close;
     QProdMarchio.Open;
end;

procedure TFrmSUITEXAnagraficaExt.SpeedButton5Click(Sender: TObject);
begin
     OpenTab('SUITEX_ProdTipoDist', ['ID','TipoDist'], ['TipoDist']);
     QProdTipoDisp.Close;
     QProdTipoDisp.Open;
end;

procedure TFrmSUITEXAnagraficaExt.SpeedButton6Click(Sender: TObject);
begin
     OpenTab('SUITEX_TDgTipologia', ['ID','Tipologia'], ['Tipologia']);
     QTDgTipologia.Close;
     QTDgTipologia.Open;
end;

procedure TFrmSUITEXAnagraficaExt.QSUITEXAnagExtAfterPost(
     DataSet: TDataSet);
begin
     with QSUITEXAnagExt do begin
          Data.DB.BeginTrans;
          try
               ApplyUpdates;
               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate in AnagAltriDati',mtError, [mbOK],0);
          end;
          CommitUpdates;
     end;
end;

procedure TFrmSUITEXAnagraficaExt.QSUITEXAnagExtBeforeClose(
     DataSet: TDataSet);
begin
     if DsQSUITEXAnagExt.state=dsEdit then QSUITEXAnagExt.Post;
     QAnagSUITEX.Close;
end;

procedure TFrmSUITEXAnagraficaExt.QSUITEXAnagExtAfterOpen(
     DataSet: TDataSet);
begin
     QAnagSUITEX.Open;
end;

procedure TFrmSUITEXAnagraficaExt.BRigaNewClick(Sender: TObject);
begin
     QAnagSUITEX.Insert;
end;

procedure TFrmSUITEXAnagraficaExt.BRigaDelClick(Sender: TObject);
begin
     QAnagSUITEX.Delete;
end;

procedure TFrmSUITEXAnagraficaExt.BRigaOKClick(Sender: TObject);
begin
     QAnagSUITEX.Post;
end;

procedure TFrmSUITEXAnagraficaExt.BRigaCanClick(Sender: TObject);
begin
     QAnagSUITEX.Cancel;
end;

procedure TFrmSUITEXAnagraficaExt.QAnagSUITEXAfterPost(DataSet: TDataSet);
begin
     with QAnagSUITEX do begin
          Data.DB.BeginTrans;
          try
               ApplyUpdates;
               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate in AnagAltriDati',mtError, [mbOK],0);
          end;
          CommitUpdates;
     end;
end;

procedure TFrmSUITEXAnagraficaExt.DsQAnagSUITEXStateChange(Sender: TObject);
var b: boolean;
begin
     b:=DsQAnagSUITEX.State in [dsEdit,dsInsert];
     BRigaNew.Enabled:=not b;
     BRigaDel.Enabled:=not b;
     BRigaOK.Enabled:=b;
     BRigaCan.Enabled:=b;
end;

procedure TFrmSUITEXAnagraficaExt.QAnagSUITEXAfterInsert(
     DataSet: TDataSet);
begin
     QAnagSUITEXIDAnagrafica.Value:=Data.TAnagraficaID.Value;
end;

procedure TFrmSUITEXAnagraficaExt.QAnagSociAfterPost(DataSet: TDataSet);
begin
     with QAnagSoci do begin
          Data.DB.BeginTrans;
          try
               ApplyUpdates;
               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate in AnagAltriDati',mtError, [mbOK],0);
          end;
          CommitUpdates;
     end;
end;

procedure TFrmSUITEXAnagraficaExt.QAnagSociAfterInsert(DataSet: TDataSet);
begin
     QAnagSociIDAnagrafica.Value:=Data.TAnagraficaID.Value;
end;

procedure TFrmSUITEXAnagraficaExt.BSocioNewClick(Sender: TObject);
begin
     QAnagSoci.Insert;
end;

procedure TFrmSUITEXAnagraficaExt.BSocioDelClick(Sender: TObject);
begin
     QAnagSoci.Delete;
end;

procedure TFrmSUITEXAnagraficaExt.BSocioOKClick(Sender: TObject);
begin
     QAnagSoci.Post;
end;

procedure TFrmSUITEXAnagraficaExt.BSocioCanClick(Sender: TObject);
begin
     QAnagSoci.Cancel;
end;

procedure TFrmSUITEXAnagraficaExt.DsQAnagSociStateChange(Sender: TObject);
var b: boolean;
begin
     b:=DsQAnagSoci.State in [dsEdit,dsInsert];
     BSocioNew.Enabled:=not b;
     BSocioDel.Enabled:=not b;
     BSocioOK.Enabled:=b;
     BSocioCan.Enabled:=b;
end;

procedure TFrmSUITEXAnagraficaExt.QAnagSUITEXBeforeClose(
     DataSet: TDataSet);
begin
     if DsQAnagSUITEX.state in [dsinsert,dsedit] then QAnagSUITEX.Post;
end;

procedure TFrmSUITEXAnagraficaExt.QAnagSociBeforeClose(DataSet: TDataSet);
begin
     if DsQAnagSoci.state in [dsinsert,dsedit] then QAnagSoci.Post;
     QAnagLK.Close;
end;

procedure TFrmSUITEXAnagraficaExt.QAnagAspirazAfterPost(DataSet: TDataSet);
begin
     with QAnagAspiraz do begin
          Data.DB.BeginTrans;
          try
               ApplyUpdates;
               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate in AnagAltriDati',mtError, [mbOK],0);
          end;
          CommitUpdates;
     end;
end;

procedure TFrmSUITEXAnagraficaExt.QAnagAspirazAfterInsert(
     DataSet: TDataSet);
begin
     QAnagAspirazIDAnagrafica.Value:=Data.TAnagraficaID.Value;
end;

procedure TFrmSUITEXAnagraficaExt.QAnagAspirazBeforeClose(
     DataSet: TDataSet);
begin
     if DsQAnagAspiraz.state in [dsinsert,dsedit] then QAnagAspiraz.Post;
end;

procedure TFrmSUITEXAnagraficaExt.DsQAnagAspirazStateChange(
     Sender: TObject);
var b: boolean;
begin
     b:=DsQAnagAspiraz.State in [dsEdit,dsInsert];
     BAspirazNew.Enabled:=not b;
     BAspirazDel.Enabled:=not b;
     BAspirazOK.Enabled:=b;
     BAspirazCan.Enabled:=b;
end;

procedure TFrmSUITEXAnagraficaExt.BAspirazNewClick(Sender: TObject);
begin
     QAnagAspiraz.Insert;
end;

procedure TFrmSUITEXAnagraficaExt.BAspirazDelClick(Sender: TObject);
begin
     QAnagAspiraz.Delete;
end;

procedure TFrmSUITEXAnagraficaExt.BAspirazOKClick(Sender: TObject);
begin
     QAnagAspiraz.Post;
end;

procedure TFrmSUITEXAnagraficaExt.BAspirazCanClick(Sender: TObject);
begin
     QAnagAspiraz.Cancel;
end;

procedure TFrmSUITEXAnagraficaExt.PCSuitexChange(Sender: TObject);
begin
     if PCSuitex.ActivePageIndex=0 then begin
        QAnagSUITEX.Open;
     end else begin
        QAnagSUITEX.Close;
     end;
     if PCSuitex.ActivePageIndex=1 then begin
        QAnagSoci.Open;
     end else begin
        QAnagSoci.Close;
     end;
     if PCSuitex.ActivePageIndex=2 then begin
        QAnagAspiraz.Open;
     end else begin
        QAnagAspiraz.Close;
     end;
end;

procedure TFrmSUITEXAnagraficaExt.QAnagSociAfterOpen(DataSet: TDataSet);
begin
QAnagLK.Open;
end;

end.

