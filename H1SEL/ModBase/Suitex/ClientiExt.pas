unit ClientiExt;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     DBTables,Db,TB97,dxDBCtrl,dxDBGrid,dxTL,dxDBTLCl,dxGrClms,
     dxCntner,ExtCtrls;

type
     TFrmSUITEXClientiExt=class(TFrame)
          Panel8: TPanel;
          dxDBGrid3: TdxDBGrid;
          Panel9: TPanel;
          BNew: TToolbarButton97;
          BDel: TToolbarButton97;
          BOK: TToolbarButton97;
          BCan: TToolbarButton97;
          Panel10: TPanel;
          QDitteExt: TQuery;
          DsQDitteExt: TDataSource;
    UpdQDitteExt: TUpdateSQL;
    QProdArticolo: TQuery;
    QProdArticoloID: TAutoIncField;
    QProdArticoloArticolo: TStringField;
    QProdFascia: TQuery;
    QProdFasciaID: TAutoIncField;
    QProdFasciaFascia: TStringField;
    QProdImmagine: TQuery;
    QProdImmagineID: TAutoIncField;
    QProdImmagineImmagine: TStringField;
    QProdMarchio: TQuery;
    QProdMarchioID: TAutoIncField;
    QProdMarchioMarchio: TStringField;
    QProdTipoDisp: TQuery;
    QProdTipoDispID: TAutoIncField;
    QProdTipoDispTipoDist: TStringField;
    QSettoriLK: TQuery;
    QSettoriLKID: TAutoIncField;
    QSettoriLKAttivita: TStringField;
    QDitteExtID: TAutoIncField;
    QDitteExtIDCliente: TIntegerField;
    QDitteExtIDSettore: TIntegerField;
    QDitteExtIDProdArticolo: TIntegerField;
    QDitteExtIDProdFascia: TIntegerField;
    QDitteExtIDProdImmagine: TIntegerField;
    QDitteExtIDProdMarchio: TIntegerField;
    QDitteExtIDProdTipoDist: TIntegerField;
    QDitteExtArticolo: TStringField;
    QDitteExtSettore: TStringField;
    QDitteExtFascia: TStringField;
    QDitteExtImmagine: TStringField;
    QDitteExtMarchio: TStringField;
    QDitteExtTipoDist: TStringField;
    dxDBGrid3Settore: TdxDBGridLookupColumn;
    dxDBGrid3Articolo: TdxDBGridLookupColumn;
    dxDBGrid3Fascia: TdxDBGridLookupColumn;
    dxDBGrid3Immagine: TdxDBGridLookupColumn;
    dxDBGrid3Marchio: TdxDBGridLookupColumn;
    dxDBGrid3TipoDist: TdxDBGridLookupColumn;
          procedure BNewClick(Sender: TObject);
          procedure BDelClick(Sender: TObject);
          procedure BOKClick(Sender: TObject);
          procedure BCanClick(Sender: TObject);
    procedure QDitteExtAfterPost(DataSet: TDataSet);
    procedure QDitteExtAfterInsert(DataSet: TDataSet);
    procedure QDitteExtBeforeClose(DataSet: TDataSet);
    procedure DsQDitteExtStateChange(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

implementation

uses ModuloDati, ModuloDati2;

{$R *.DFM}

procedure TFrmSUITEXClientiExt.BNewClick(Sender: TObject);
begin
     QDitteExt.Insert;
end;

procedure TFrmSUITEXClientiExt.BDelClick(Sender: TObject);
begin
     if MessageDlg('Sei sicuro di voler eliminare la riga ?',mtWarning, [mbNo,mbYes],0)=mrNo then exit;
     QDitteExt.Delete;
end;

procedure TFrmSUITEXClientiExt.BOKClick(Sender: TObject);
begin
     QDitteExt.Post;
end;

procedure TFrmSUITEXClientiExt.BCanClick(Sender: TObject);
begin
     QDitteExt.Cancel;
end;

procedure TFrmSUITEXClientiExt.QDitteExtAfterPost(DataSet: TDataSet);
begin
     with QDitteExt do begin
          Data.DB.BeginTrans;
          try
               ApplyUpdates;
               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate in AnagAltriDati',mtError, [mbOK],0);
          end;
          CommitUpdates;
     end;
end;

procedure TFrmSUITEXClientiExt.QDitteExtAfterInsert(DataSet: TDataSet);
begin
     QDitteExtIDCliente.Value:=Data2.TEBCClientiID.Value;
end;

procedure TFrmSUITEXClientiExt.QDitteExtBeforeClose(DataSet: TDataSet);
begin
     if DsQDitteExt.state in [dsInsert,dsEdit] then QDitteExt.Post;
end;

procedure TFrmSUITEXClientiExt.DsQDitteExtStateChange(Sender: TObject);
var b: boolean;
begin
     b:=DsQDitteExt.State in [dsEdit,dsInsert];
     BNew.Enabled:=not b;
     BDel.Enabled:=not b;
     BOK.Enabled:=b;
     BCan.Enabled:=b;
end;

end.

