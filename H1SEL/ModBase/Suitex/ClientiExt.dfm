object FrmSUITEXClientiExt: TFrmSUITEXClientiExt
  Left = 0
  Top = 0
  Width = 460
  Height = 298
  TabOrder = 0
  object Panel8: TPanel
    Left = 0
    Top = 0
    Width = 460
    Height = 298
    Align = alClient
    BevelOuter = bvLowered
    Caption = 'Panel1'
    TabOrder = 0
    object dxDBGrid3: TdxDBGrid
      Left = 1
      Top = 61
      Width = 458
      Height = 236
      Bands = <
        item
        end>
      DefaultLayout = False
      HeaderPanelRowCount = 2
      KeyField = 'ID'
      SummaryGroups = <>
      SummarySeparator = ', '
      Align = alClient
      TabOrder = 0
      DataSource = DsQDitteExt
      Filter.Criteria = {00000000}
      OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
      OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoIndicator, edgoUseBitmap]
      object dxDBGrid3Articolo: TdxDBGridLookupColumn
        Color = clInfoBk
        Width = 484
        BandIndex = 0
        RowIndex = 0
        FieldName = 'Articolo'
      end
      object dxDBGrid3Fascia: TdxDBGridLookupColumn
        Color = clInfoBk
        Width = 484
        BandIndex = 0
        RowIndex = 0
        FieldName = 'Fascia'
      end
      object dxDBGrid3Immagine: TdxDBGridLookupColumn
        Color = clInfoBk
        Width = 484
        BandIndex = 0
        RowIndex = 0
        FieldName = 'Immagine'
      end
      object dxDBGrid3Marchio: TdxDBGridLookupColumn
        Color = clInfoBk
        Width = 484
        BandIndex = 0
        RowIndex = 0
        FieldName = 'Marchio'
      end
      object dxDBGrid3TipoDist: TdxDBGridLookupColumn
        Caption = 'Tipo distrib.'
        Width = 220
        BandIndex = 0
        RowIndex = 1
        FieldName = 'TipoDist'
        StoredRowIndex = 1
      end
      object dxDBGrid3Settore: TdxDBGridLookupColumn
        Width = 222
        BandIndex = 0
        RowIndex = 1
        FieldName = 'Settore'
        StoredRowIndex = 1
      end
    end
    object Panel9: TPanel
      Left = 1
      Top = 22
      Width = 458
      Height = 39
      Align = alTop
      BevelOuter = bvLowered
      TabOrder = 1
      object BNew: TToolbarButton97
        Left = 1
        Top = 1
        Width = 61
        Height = 37
        Caption = 'Nuovo'
        Opaque = False
        WordWrap = True
        OnClick = BNewClick
      end
      object BDel: TToolbarButton97
        Left = 62
        Top = 1
        Width = 61
        Height = 37
        Caption = 'Elimina'
        Opaque = False
        WordWrap = True
        OnClick = BDelClick
      end
      object BOK: TToolbarButton97
        Left = 123
        Top = 1
        Width = 61
        Height = 37
        Caption = 'Conferma modifiche'
        Enabled = False
        Opaque = False
        WordWrap = True
        OnClick = BOKClick
      end
      object BCan: TToolbarButton97
        Left = 184
        Top = 1
        Width = 61
        Height = 37
        Caption = 'Annulla modifiche'
        Enabled = False
        Opaque = False
        WordWrap = True
        OnClick = BCanClick
      end
    end
    object Panel10: TPanel
      Left = 1
      Top = 1
      Width = 458
      Height = 21
      Align = alTop
      Alignment = taLeftJustify
      Caption = '  Campi personalizzati SUITEX'
      Color = clGray
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
    end
  end
  object QDitteExt: TQuery
    CachedUpdates = True
    BeforeClose = QDitteExtBeforeClose
    AfterInsert = QDitteExtAfterInsert
    AfterPost = QDitteExtAfterPost
    AfterDelete = QDitteExtAfterPost
    DatabaseName = 'EBCDB'
    DataSource = Data2.DsEBCclienti
    SQL.Strings = (
      'select * from SUITEX_DittaVarie'
      'where IDCLiente=:ID')
    UpdateObject = UpdQDitteExt
    Left = 160
    Top = 144
    ParamData = <
      item
        DataType = ftAutoInc
        Name = 'ID'
        ParamType = ptUnknown
      end>
    object QDitteExtID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.SUITEX_DittaVarie.ID'
    end
    object QDitteExtIDCliente: TIntegerField
      FieldName = 'IDCliente'
      Origin = 'EBCDB.SUITEX_DittaVarie.IDCliente'
    end
    object QDitteExtIDSettore: TIntegerField
      FieldName = 'IDSettore'
      Origin = 'EBCDB.SUITEX_DittaVarie.IDSettore'
    end
    object QDitteExtIDProdArticolo: TIntegerField
      FieldName = 'IDProdArticolo'
      Origin = 'EBCDB.SUITEX_DittaVarie.IDProdArticolo'
    end
    object QDitteExtIDProdFascia: TIntegerField
      FieldName = 'IDProdFascia'
      Origin = 'EBCDB.SUITEX_DittaVarie.IDProdFascia'
    end
    object QDitteExtIDProdImmagine: TIntegerField
      FieldName = 'IDProdImmagine'
      Origin = 'EBCDB.SUITEX_DittaVarie.IDProdImmagine'
    end
    object QDitteExtIDProdMarchio: TIntegerField
      FieldName = 'IDProdMarchio'
      Origin = 'EBCDB.SUITEX_DittaVarie.IDProdMarchio'
    end
    object QDitteExtIDProdTipoDist: TIntegerField
      FieldName = 'IDProdTipoDist'
      Origin = 'EBCDB.SUITEX_DittaVarie.IDProdTipoDist'
    end
    object QDitteExtSettore: TStringField
      FieldKind = fkLookup
      FieldName = 'Settore'
      LookupDataSet = QSettoriLK
      LookupKeyFields = 'ID'
      LookupResultField = 'Attivita'
      KeyFields = 'IDSettore'
      Size = 50
      Lookup = True
    end
    object QDitteExtArticolo: TStringField
      FieldKind = fkLookup
      FieldName = 'Articolo'
      LookupDataSet = QProdArticolo
      LookupKeyFields = 'ID'
      LookupResultField = 'Articolo'
      KeyFields = 'IDProdArticolo'
      Size = 80
      Lookup = True
    end
    object QDitteExtFascia: TStringField
      FieldKind = fkLookup
      FieldName = 'Fascia'
      LookupDataSet = QProdFascia
      LookupKeyFields = 'ID'
      LookupResultField = 'Fascia'
      KeyFields = 'IDProdFascia'
      Size = 80
      Lookup = True
    end
    object QDitteExtImmagine: TStringField
      FieldKind = fkLookup
      FieldName = 'Immagine'
      LookupDataSet = QProdImmagine
      LookupKeyFields = 'ID'
      LookupResultField = 'Immagine'
      KeyFields = 'IDProdImmagine'
      Size = 80
      Lookup = True
    end
    object QDitteExtMarchio: TStringField
      FieldKind = fkLookup
      FieldName = 'Marchio'
      LookupDataSet = QProdMarchio
      LookupKeyFields = 'ID'
      LookupResultField = 'Marchio'
      KeyFields = 'IDProdMarchio'
      Size = 80
      Lookup = True
    end
    object QDitteExtTipoDist: TStringField
      FieldKind = fkLookup
      FieldName = 'TipoDist'
      LookupDataSet = QProdTipoDisp
      LookupKeyFields = 'ID'
      LookupResultField = 'TipoDist'
      KeyFields = 'IDProdTipoDist'
      Size = 80
      Lookup = True
    end
  end
  object DsQDitteExt: TDataSource
    DataSet = QDitteExt
    OnStateChange = DsQDitteExtStateChange
    Left = 160
    Top = 176
  end
  object UpdQDitteExt: TUpdateSQL
    ModifySQL.Strings = (
      'update SUITEX_DittaVarie'
      'set'
      '  IDCliente = :IDCliente,'
      '  IDSettore = :IDSettore,'
      '  IDProdArticolo = :IDProdArticolo,'
      '  IDProdFascia = :IDProdFascia,'
      '  IDProdImmagine = :IDProdImmagine,'
      '  IDProdMarchio = :IDProdMarchio,'
      '  IDProdTipoDist = :IDProdTipoDist'
      'where'
      '  ID = :OLD_ID')
    InsertSQL.Strings = (
      'insert into SUITEX_DittaVarie'
      
        '  (IDCliente, IDSettore, IDProdArticolo, IDProdFascia, IDProdImm' +
        'agine, '
      '   IDProdMarchio, IDProdTipoDist)'
      'values'
      
        '  (:IDCliente, :IDSettore, :IDProdArticolo, :IDProdFascia, :IDPr' +
        'odImmagine, '
      '   :IDProdMarchio, :IDProdTipoDist)')
    DeleteSQL.Strings = (
      'delete from SUITEX_DittaVarie'
      'where'
      '  ID = :OLD_ID')
    Left = 160
    Top = 208
  end
  object QProdArticolo: TQuery
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select * from SUITEX_ProdArticolo')
    Left = 328
    Top = 24
    object QProdArticoloID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.SUITEX_ProdArticolo.ID'
    end
    object QProdArticoloArticolo: TStringField
      FieldName = 'Articolo'
      Origin = 'EBCDB.SUITEX_ProdArticolo.Articolo'
      FixedChar = True
      Size = 80
    end
  end
  object QProdFascia: TQuery
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select * from SUITEX_ProdFascia')
    Left = 360
    Top = 22
    object QProdFasciaID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.SUITEX_ProdFascia.ID'
    end
    object QProdFasciaFascia: TStringField
      FieldName = 'Fascia'
      Origin = 'EBCDB.SUITEX_ProdFascia.Fascia'
      FixedChar = True
      Size = 80
    end
  end
  object QProdImmagine: TQuery
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select * from SUITEX_ProdImmagine')
    Left = 392
    Top = 26
    object QProdImmagineID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.SUITEX_ProdImmagine.ID'
    end
    object QProdImmagineImmagine: TStringField
      FieldName = 'Immagine'
      Origin = 'EBCDB.SUITEX_ProdImmagine.Immagine'
      FixedChar = True
      Size = 80
    end
  end
  object QProdMarchio: TQuery
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select * from SUITEX_ProdMarchio')
    Left = 424
    Top = 24
    object QProdMarchioID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.SUITEX_ProdMarchio.ID'
    end
    object QProdMarchioMarchio: TStringField
      FieldName = 'Marchio'
      Origin = 'EBCDB.SUITEX_ProdMarchio.Marchio'
      FixedChar = True
      Size = 80
    end
  end
  object QProdTipoDisp: TQuery
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select * from SUITEX_ProdTipoDist')
    Left = 360
    Top = 56
    object QProdTipoDispID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.SUITEX_ProdTipoDist.ID'
    end
    object QProdTipoDispTipoDist: TStringField
      FieldName = 'TipoDist'
      Origin = 'EBCDB.SUITEX_ProdTipoDist.TipoDist'
      FixedChar = True
      Size = 80
    end
  end
  object QSettoriLK: TQuery
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select * from EBC_Attivita'
      'order by attivita')
    Left = 401
    Top = 57
    object QSettoriLKID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.EBC_Attivita.ID'
    end
    object QSettoriLKAttivita: TStringField
      FieldName = 'Attivita'
      Origin = 'EBCDB.EBC_Attivita.Attivita'
      FixedChar = True
      Size = 50
    end
  end
end
