�
 TANALISICOMPFORM 0�  TPF0TAnalisiCompFormAnalisiCompFormLeftWTopnWidthWHeight
HelpContext� CaptionAnalisi competenzeColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrder	PositionpoScreenCenterPixelsPerInch`
TextHeight TDBChartDBChart1Left Top)WidthOHeight�BackWall.Brush.StylebsClearTitle.Text.Strings1confronto tra i totali ponderati delle competenze Legend.VisibleAlignalClient
BevelOuter	bvLoweredTabOrder  THorizBarSeriesSeries1Marks.ArrowLengthMarks.StylesmsValueMarks.Visible	
DataSourceTTotValueFormat
#,##0.### XLabelsSourceCognome	HorizAxisaTopAxisSeriesColorclRedXValues.DateTimeXValues.NameBarXValues.MultiplierXValues.OrderloNoneXValues.ValueSourceTotYValues.DateTimeYValues.NameYYValues.MultiplierYValues.OrderloNone   TPanelPanel1Left Top WidthOHeight)AlignalTop
BevelOuterbvNoneTabOrder TPanelPanel2Left Top Width�Height)AlignalLeft
BevelOuterbvNoneTabOrder  TLabelLabel1LeftTopWidthPHeightCaptionSuperiore diretto:  TDBEditDBEdit1Left
Top	Width� HeightColorclWhite	DataFieldDescrizioneEnabledFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontReadOnly	TabOrder   TDBEditDBEdit2Left[Top	Width� HeightColorclYellow	DataFieldCognomeEnabledFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontReadOnly	TabOrder   TBitBtnBitBtn1Left�TopWidth^Height!TabOrderKindbkOK   TQueryQTotDatabaseNameEBCDBSQL.StringsSELECT ( sum( CompetenzeMansioni."Peso" *) CompetenzeAnagrafica."Valore" ) /100 ) / ( sum( CompetenzeMansioni."Peso" */ CompetenzeMansioni."Valore" ) /100 ) * 100 TotIFROM "CompetenzeMansioni" CompetenzeMansioni , "Competenze" Competenze , , "CompetenzeAnagrafica" CompetenzeAnagrafica9WHERE ( CompetenzeMansioni.IDCompetenza = Competenze.ID )KAND ( CompetenzeMansioni.IDCompetenza = CompetenzeAnagrafica.IDCompetenza )-AND CompetenzeMansioni.IDMansione=:IDMansione)AND CompetenzeAnagrafica.IDAnagrafica=:ID Left0Top`	ParamDataDataType	ftUnknownName
IDMansione	ParamType	ptUnknown DataType	ftUnknownNameID	ParamType	ptUnknown   TFloatFieldQTotTot	FieldNameTotDisplayFormat### %   TTableTTotActive	DatabaseNameHR	IndexName	IdxTotAsc	TableNamedbo.TempComparazTotLeftTopa TIntegerFieldTTotIDAnagrafica	FieldNameIDAnagrafica  TIntegerFieldTTotIDMansione	FieldName
IDMansione  TStringFieldTTotCognome	FieldNameCognomeSize  TStringFieldTTotNome	FieldNameNomeSize  TSmallintFieldTTotTot	FieldNameTot   TDataSourceDsTotDataSetTTotLeftTop�   TDataSourceDsQTotDataSetQTotLeft1Top�   TQueryQuery1DatabaseNameEBCDBLeftTop8  TQueryQ1DatabaseNameEBCDBSQL.Strings.select IDDipendente ID,Cognome,Nome,IDMansionefrom Organigramma,AnagraficaJwhere Organigramma.IDDipendente=Anagrafica.ID and IDDipendente is not nulland NodoPadre=10 Left0Top8 TIntegerFieldQ1ID	FieldNameIDOrigin"Organigramma".IDDipendente  TStringField	Q1Cognome	FieldNameCognomeOrigin"Anagrafica".CognomeSize  TStringFieldQ1Nome	FieldNameNomeOrigin"Anagrafica".NomeSize  TIntegerFieldQ1IDMansione	FieldName
IDMansioneOrigin"Organigramma".IDDipendente    