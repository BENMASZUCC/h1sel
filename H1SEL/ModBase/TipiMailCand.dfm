object TipiMailCandForm: TTipiMailCandForm
  Left = 273
  Top = 149
  BorderStyle = bsDialog
  Caption = 'Gestione tipi di mail'
  ClientHeight = 408
  ClientWidth = 781
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 5
    Top = 6
    Width = 339
    Height = 396
    Anchors = [akLeft, akTop, akRight, akBottom]
    DataSource = DsQTipiCand
    ReadOnly = True
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'Descrizione'
        Visible = True
      end>
  end
  object Panel1: TPanel
    Left = 350
    Top = 6
    Width = 345
    Height = 396
    Anchors = [akTop, akRight, akBottom]
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 1
    object Label1: TLabel
      Left = 8
      Top = 50
      Width = 55
      Height = 13
      Caption = 'Descrizione'
      FocusControl = DBEdit1
    end
    object Label2: TLabel
      Left = 8
      Top = 130
      Width = 57
      Height = 13
      Caption = 'Intestazione'
      FocusControl = DBMemo1
    end
    object Label3: TLabel
      Left = 8
      Top = 246
      Width = 28
      Height = 13
      Caption = 'Corpo'
      FocusControl = DBMemo2
    end
    object Label4: TLabel
      Left = 8
      Top = 90
      Width = 38
      Height = 13
      Caption = 'Oggetto'
      FocusControl = DBEdit2
    end
    object DBEdit1: TDBEdit
      Left = 8
      Top = 66
      Width = 329
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      DataField = 'Descrizione'
      DataSource = DsQTipiCand
      TabOrder = 0
    end
    object DBMemo1: TDBMemo
      Left = 8
      Top = 146
      Width = 329
      Height = 57
      Anchors = [akLeft, akTop, akRight]
      DataField = 'Intestazione'
      DataSource = DsQTipiCand
      TabOrder = 1
    end
    object DBMemo2: TDBMemo
      Left = 8
      Top = 262
      Width = 329
      Height = 108
      Anchors = [akLeft, akTop, akRight, akBottom]
      DataField = 'Corpo'
      DataSource = DsQTipiCand
      TabOrder = 2
    end
    object DBEdit2: TDBEdit
      Left = 8
      Top = 106
      Width = 304
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      DataField = 'Oggetto'
      DataSource = DsQTipiCand
      TabOrder = 3
    end
    object DBCheckBox1: TDBCheckBox
      Left = 8
      Top = 210
      Width = 129
      Height = 17
      Caption = 'riporta dati ricerca'
      DataField = 'InfoRicerca'
      DataSource = DsQTipiCand
      TabOrder = 4
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object Panel2: TPanel
      Left = 7
      Top = 8
      Width = 330
      Height = 38
      Anchors = [akLeft, akTop, akRight]
      BevelOuter = bvLowered
      TabOrder = 5
      object BNew: TToolbarButton97
        Left = 1
        Top = 1
        Width = 67
        Height = 36
        Caption = 'Nuovo tipo'
        WordWrap = True
        OnClick = BNewClick
      end
      object Bdel: TToolbarButton97
        Left = 68
        Top = 1
        Width = 67
        Height = 36
        Caption = 'Elimina'
        WordWrap = True
        OnClick = BdelClick
      end
      object BOK: TToolbarButton97
        Left = 135
        Top = 1
        Width = 67
        Height = 36
        Caption = 'Conferma modifiche'
        Enabled = False
        WordWrap = True
        OnClick = BOKClick
      end
      object BCan: TToolbarButton97
        Left = 202
        Top = 1
        Width = 67
        Height = 36
        Caption = 'Annulla modifiche'
        Enabled = False
        WordWrap = True
        OnClick = BCanClick
      end
    end
    object DBCheckBox2: TDBCheckBox
      Left = 8
      Top = 228
      Width = 189
      Height = 17
      Caption = 'Aggiungi specifiche (esterne)'
      DataField = 'AddSpecifiche'
      DataSource = DsQTipiCand
      TabOrder = 6
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object DBCheckBox3: TDBCheckBox
      Left = 8
      Top = 371
      Width = 189
      Height = 17
      Caption = 'Aggiungi link a questionario'
      DataField = 'AddQuest'
      DataSource = DsQTipiCand
      TabOrder = 7
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object Panel3: TPanel
      Left = 63
      Top = 351
      Width = 330
      Height = 38
      Anchors = [akLeft, akTop, akRight]
      BevelOuter = bvLowered
      TabOrder = 8
      Visible = False
      object BCliNew: TToolbarButton97
        Left = 1
        Top = 1
        Width = 67
        Height = 36
        Caption = 'Nuovo tipo'
        WordWrap = True
        OnClick = BCliNewClick
      end
      object BCliDel: TToolbarButton97
        Left = 68
        Top = 1
        Width = 67
        Height = 36
        Caption = 'Elimina'
        WordWrap = True
        OnClick = BCliDelClick
      end
      object BCliOK: TToolbarButton97
        Left = 135
        Top = 1
        Width = 67
        Height = 36
        Caption = 'Conferma modifiche'
        Enabled = False
        WordWrap = True
        OnClick = BCliOKClick
      end
      object BCliCan: TToolbarButton97
        Left = 202
        Top = 1
        Width = 67
        Height = 36
        Caption = 'Annulla modifiche'
        Enabled = False
        WordWrap = True
        OnClick = BCliCanClick
      end
    end
  end
  object Panel4: TPanel
    Left = 696
    Top = 0
    Width = 85
    Height = 408
    Align = alRight
    Anchors = [akRight]
    BevelOuter = bvNone
    TabOrder = 2
    object BitBtn1: TToolbarButton97
      Left = 2
      Top = 7
      Width = 81
      Height = 41
      Caption = 'Esci'
      Glyph.Data = {
        76060000424D7606000000000000360400002800000018000000180000000100
        080000000000400200000000000000000000000100000000000000000000FFFF
        FF00FF00FF0040D86800B1AEAC00545149001D973F0076967600C4F1D1002827
        270079D2910045A35E00716D5E00CFC7C00024C04D009B958D005FBA77005CE6
        8000E4EFE70090B69900B1D9BC005C8864003D3C390076B3870034934E003ABD
        5D007C7B730054D073005F5E5E00938F7800AAA695002CD358009BC6A60062A4
        73006AC98300E0D5C70049EC7500C1BBB7004FB96A00D5EEDB00697C6D008187
        820081C091002AAF4D00B5E9C300F1F7F2009D9D9C003432310044C666005397
        60009F9B7F0037CC5E004A49430039A0550072A67F007C9B83005F7763006E6E
        6D0055A66C006BB87F00A7A6A5008B8C880030C257004ADC700086B48800668B
        6C0060B06C0077C48B00259F470092C29C00C8C2B00054C27100E7FAEC0065B0
        790085827000C5C3C20056DC790071D08A005B584E005BCA79009A939900DCF3
        E3004DCC6E0086C4960080AC8C0032B054007572710064C17D003DA95B0041B4
        6000C9E7D100A39F9200B9F1C800F8FBF900D9D1C000BBB5B50046E67000B1AE
        A20048AB630080C793008DBB91006D9276006EAF7F0064D081004EE3750043CF
        680078BB890056B66F002ABD52007FB68D00CEECD6003FC262007E7E7B006F88
        7000329F500094908800D0CCB90031CC5A00C1B9AE00489B5F00A7A49E003BD1
        6300C4BBC100679F6F0077CD8E00CCC7C60055E57A004FA66700888886005DB5
        720072C0870095BD9700ACABA8005CA76F008CC09A006360610030BD56004DD1
        700046BF66005BC37700CBC8B600BBB8B90038B7590049A85B00AFAA98008482
        7500A09A8D0073C9890033D05D00B5B2A80059B16F00D9EEDF0087BB940033C6
        5A0096C3A1006DC4850069AB7A0040DE6A0051D775005B59590049D76D009B98
        80008F8D8500ECF6EF00E5F4E90028C35100B8AFB1003FCB63005AE07E009C97
        880065CA7F007B967A0062B47300D3EAD9004949480042BA610079767500A2A1
        9F006CCD8600A2A1990070B5820074B98600D4CFBC00C4BFB9002BC756003CA4
        58004AC66B008DB68E007CC08D00BFBDBE0068BD7E003CC6610049E773003131
        2F00E0F2E500C9C1BF0045E06E004FD472006B866D0082817D0094BE9F005FBF
        790065A97700C1EDCD003BD6640053E1790063786600B1AFA700ABA9A4007CCC
        9100C9BFB40037BC5A003DB45D00657C6A009BC1A600D1EFD90051B56C007DBA
        8E007A7870009B9B9B007BB88B00D9F2DF00B8ECC60026B04B002BB34F004C9A
        61008F8F8C00E8F5EB0059C3740093C69F0062B478008CB6970085BE9300A5A4
        A20055D476009D969A0053B96E005CBE76007CC78F00DEEEE300CAEDD300D3CB
        BD00BFB6B400B7B3B20056544B00DCD3C200CFC9BC00C6BDBD0037CF5F0041DA
        6A0042A45D00ADADAC0057E17C00A9A8A800A4A0950086868300020202020202
        022E39AEC10909C1AE392E020202020202020202020202B11C2F1C507ABDBD7A
        EB872F9F2E0202020202020202028016567DE5E488757588E4E57DB016FF0202
        0202020202C734A69A3E033FCDFCFCCD3FCC3E455F34700202020202E205F7AC
        94A03F69687E1111A84CA09481C3F43D0202023C4EF2AC7579CC30E31B60C068
        CDFC4CC57596254EE902021A7383B833796FC26EA3EA24C0C43F4C9E89B8640F
        DA023C0C0D2B993319C26E6E6EA31B60C4F9039EC5A7DF0D0CE973A9BB6C998E
        C26E6ED76ED7A3529D03797989526C4092A29176428855C2AD6EDD4851D727A3
        BACC7933A752198FD2914A23358AA4ADAD51081F5CC22727A38AF8339930AF06
        234A1DB6B98BD797C2087579945CA42727A3AF9999D3AF448C1DA174FAEC47D7
        CBA533333375DEA42797A3D46CE026724632A1F5776BEDB24FBF3E999933B8DE
        E39797A35859D8185EA1A9F67B961057224D6752BF3E3E0E2CE3EF122D966231
        F1A9B346ABE6E6BE9B937C0A0A0A4DB2AAF02D12A3ADFA0746FE841ECF21493B
        8243EED1D1D17C939BC95A5DAD0BE1D0908402FEF671B5B46ABC63636363EE43
        823B81147F3AC6F6FE0202FB5BB741D9DC2A535353532ABCB53B4985851525FE
        FB0202028461CF41869886868686E8D91766CA9C15D095D00202020202D0CF5F
        CEE7209A9A86986D17173638F395D00202020202020284787D3D283713D6C854
        65D53D7D7884020202020202020202FBB1044B8D2E2929DB8D4B04B1FB020202
        020202020202020202FBB1DBB1FDFD2EDBB18402020202020202}
      OnClick = BitBtn1Click
    end
  end
  object DsQTipiCand: TDataSource
    DataSet = QTipiCand
    OnStateChange = DsQTipiCandStateChange
    Left = 32
    Top = 64
  end
  object QTipiCand: TADOLinkedQuery
    Active = True
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from TipiMailCand with (updlock)')
    UseFilter = False
    Left = 32
    Top = 24
  end
  object QTipiCli: TADOLinkedQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from TipiMailContattiCli with (updlock)')
    UseFilter = False
    Left = 88
    Top = 24
  end
  object dsQTipiCli: TDataSource
    DataSet = QTipiCli
    OnStateChange = dsQTipiCliStateChange
    Left = 88
    Top = 64
  end
end
