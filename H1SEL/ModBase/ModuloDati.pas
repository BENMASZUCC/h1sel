unit ModuloDati;

interface

uses
     SysUtils, Windows, Classes, Graphics, Controls,
     Forms, Dialogs, DB, DBTables, ADODB, U_ADOLinkCl, UCrypter;

type
     TData = class(TDataModule)
          DsAnagrafica: TDataSource;
          DsTitoliStudio: TDataSource;
          DsStati: TDataSource;
          DsStorico: TDataSource;
          DsCorsiExtra: TDataSource;
          DsLingueConosc: TDataSource;
          DsEspLav: TDataSource;
          DsGlobal: TDataSource;
          DsUsers: TDataSource;
          DsPromemoria: TDataSource;
          DsDisponibAnag: TDataSource;
          DsDiplomi: TDataSource;
          DsAreeDiplomi: TDataSource;
          DsEventi: TDataSource;
          DsAnagMansioni: TDataSource;
          DsPromScaduti: TDataSource;
          DsCVFile: TDataSource;
          DsQPromOggi: TDataSource;
          DsAziende: TDataSource;
          DsLingue: TDataSource;
          DsComuni: TDataSource;
          DsZone: TDataSource;
          DsRisorse: TDataSource;
          DsQAnagNote: TDataSource;
          DsQAnagFile: TDataSource;
          DsQSedi: TDataSource;
          DsQProvCV: TDataSource;
          DsQNazioni: TDataSource;
          DsQTipiStrada: TDataSource;
          TDiplomiLK: TADOLinkedTable;
          DB: TADOConnection;
          QSettoriLK: TADOLinkedQuery;
          TLivelloLingue: TADOLinkedTable;
          QLingueLK: TADOLinkedQuery;
          TAreeLK: TADOLinkedTable;
          TRicercheLK: TADOLinkedTable;
          TAnagLK: TADOLinkedTable;
          TMansioniLK: TADOLinkedTable;
          TPromemABS: TADOLinkedTable;
          TStatiLK: TADOLinkedTable;
          TTipiStatiLK: TADOLinkedTable;
          QClientiLK: TADOLinkedQuery;
          QTipologieSoggLK: TADOLinkedQuery;
          TAziendeLK: TADOLinkedQuery;
          QNazioni: TADOLinkedQuery;
          QTipiStrada: TADOLinkedQuery;
          QPromOggi: TADOLinkedQuery;
          QPromFuturi: TADOLinkedQuery;
          Q2: TADOLinkedQuery;
          TPromScaduti: TADOLinkedTable;

          TAgenda: TADOLinkedTable;
          TRisorse: TADOLinkedTable;
          Users: TADOLinkedQuery;
          TStati: TADOLinkedTable;
          QProvCV: TADOLinkedQuery;
          QSedi: TADOLinkedQuery;
          TEventi: TADOLinkedTable;
          TTipiStato: TADOLinkedTable;
          TStatiCivili: TADOLinkedTable;
          TAreeDiplomi: TADOLinkedTable;
          TEventiLK: TADOLinkedTable;
          TZone: TADOLinkedTable;
          TLingue: TADOLinkedTable;
          TAnagABS: TADOLinkedQuery;
          Q1: TADOLinkedQuery;
          TAziende: TADOLinkedQuery;
          QTemp: TADOLinkedQuery;
          QComuni: TADOLinkedQuery;
          TTitoliStudio: TADOLinkedQuery;
          TCorsiExtra: TADOLinkedQuery;
          TLingueConosc: TADOLinkedQuery;
          TEspLav: TADOLinkedQuery;
          TAnagrafica: TADOLinkedQuery;
          QAnagNote: TADOLinkedQuery;
          TAnagMansioni: TADOLinkedQuery;
          QStorico: TADOLinkedQuery;
          QAnagFile: TADOLinkedQuery;
          TCVFile: TADOLinkedTable;
          TDisponibAnag: TADOLinkedTable;
          TPromemoria: TADOLinkedTable;
          Global: TADOLinkedQuery;
          TStorico: TADOLinkedTable;
          DSStatiCivili: TDataSource;
          TAnagMansioniID: TAutoIncField;
          TAnagMansioniAttuale: TBooleanField;
          TAnagMansioniAnniEsperienza: TSmallintField;
          TAnagMansioniDisponibile: TBooleanField;
          TAnagMansioniIDMansione: TIntegerField;
          TAnagMansioniMansione: TStringField;
          TAnagMansioniArea: TStringField;
          TAnagMansioniMansione_ENG: TStringField;
          TAnagMansioniArea_ENG: TStringField;
          TAnagMansioniAspirazione: TBooleanField;
          TAnagMansioniIDAnagrafica: TIntegerField;
          TEspLavID: TAutoIncField;
          TEspLavIDAnagrafica: TIntegerField;
          TEspLavIDAzienda: TIntegerField;
          TEspLavIDSettore: TIntegerField;
          TEspLavAttuale: TBooleanField;
          TEspLavAziendaProvincia: TStringField;
          TEspLavAziendaFatturato: TFloatField;
          TEspLavAziendaNumDipendenti: TSmallintField;
          TEspLavPosizioneGerarchia: TStringField;
          TEspLavAnnoDal: TSmallintField;
          TEspLavAnnoAl: TSmallintField;
          TEspLavDurataMesi: TSmallintField;
          TEspLavTipoContratto: TStringField;
          TEspLavLivelloContrattuale: TStringField;
          TEspLavRetribNettaMensile: TIntegerField;
          TEspLavMensilita: TSmallintField;
          TEspLavTitoloMansione: TStringField;
          TEspLavDescrizioneMansione: TMemoField;
          TEspLavIDArea: TIntegerField;
          TEspLavIDMansione: TIntegerField;
          TEspLavMotivoCessazione: TMemoField;
          TEspLavIDAreaSettore: TIntegerField;
          TEspLavDescrizioneAttivitaAz: TMemoField;
          TEspLavPreavvisoGiorni: TSmallintField;
          TEspLavPreavvisoCalendario: TBooleanField;
          TEspLavPreavvisoTermQualsGiorno: TBooleanField;
          TEspLavPreavvisoTermGiorno: TSmallintField;
          TEspLavIDTipoContratto: TIntegerField;
          TEspLavLKMansione: TStringField;
          TEspLavLKArea: TStringField;
          TEspLavIDMansioneTemp: TIntegerField;
          TEspLavMeseDal: TSmallintField;
          TEspLavMeseAl: TSmallintField;
          TEspLavID_1: TAutoIncField;
          TEspLavDescrizione: TStringField;
          TEspLavIDArea_1: TIntegerField;
          TEspLavFileMansionario: TStringField;
          TEspLavMansioni: TMemoField;
          TEspLavIDAzienda_1: TIntegerField;
          TEspLavLKArea_1: TStringField;
          TEspLavTolleranzaPerc: TIntegerField;
          TEspLavDescrizione_ENG: TStringField;
          TEspLavID_2: TAutoIncField;
          TEspLavDescrizione_1: TStringField;
          TEspLavIDAzienda_2: TIntegerField;
          TEspLavDescrizione_ENG_1: TStringField;
          TEspLavID_3: TAutoIncField;
          TEspLavRuolo: TStringField;
          TEspLavIDArea_2: TIntegerField;
          TEspLavSettore: TStringField;
          TEspLavAzDenominazione: TStringField;
          TEspLavAzIndirizzo: TStringField;
          TEspLavAzComune: TStringField;
          TEspLavAzProv: TStringField;
          TEspLavNumDipendenti: TSmallintField;
          TEspLavFatturato: TFloatField;
          TEspLavTelefono: TStringField;
          TAreeDiplomiID: TAutoIncField;
          TAreeDiplomiArea: TStringField;
          TAreeDiplomiIdAzienda: TIntegerField;
          TAnagraficaID: TAutoIncField;
          TAnagraficaCognome: TStringField;
          TAnagraficaNome: TStringField;
          TAnagraficaTitolo: TStringField;
          TAnagraficaDataNascita: TDateTimeField;
          TAnagraficaGiornoNascita: TSmallintField;
          TAnagraficaMeseNascita: TSmallintField;
          TAnagraficaLuogoNascita: TStringField;
          TAnagraficaSesso: TStringField;
          TAnagraficaIndirizzo: TStringField;
          TAnagraficaCap: TStringField;
          TAnagraficaComune: TStringField;
          TAnagraficaIDComuneRes: TIntegerField;
          TAnagraficaIDZonaRes: TIntegerField;
          TAnagraficaProvincia: TStringField;
          TAnagraficaDomicilioIndirizzo: TStringField;
          TAnagraficaDomicilioCap: TStringField;
          TAnagraficaDomicilioComune: TStringField;
          TAnagraficaIDComuneDom: TIntegerField;
          TAnagraficaIDZonaDom: TIntegerField;
          TAnagraficaDomicilioProvincia: TStringField;
          TAnagraficaRecapitiTelefonici: TStringField;
          TAnagraficaCellulare: TStringField;
          TAnagraficaFax: TStringField;
          TAnagraficaCodiceFiscale: TStringField;
          TAnagraficaPartitaIVA: TStringField;
          TAnagraficaEmail: TStringField;
          TAnagraficaIDStatoCivile: TIntegerField;
          TAnagraficaIDStato: TIntegerField;
          TAnagraficaIDTipoStato: TIntegerField;
          TAnagraficaCVNumero: TIntegerField;
          TAnagraficaCVIDAnnData: TIntegerField;
          TAnagraficaCVinseritoInData: TDateTimeField;
          TAnagraficaFoto: TBlobField;
          TAnagraficaIDTipologia: TIntegerField;
          TAnagraficaIDUtenteMod: TIntegerField;
          TAnagraficaDubbiIns: TStringField;
          TAnagraficaIDProprietaCV: TIntegerField;
          TAnagraficaTelUfficio: TStringField;
          TAnagraficaOldCVNumero: TStringField;
          TAnagraficaDescStato: TStringField;
          TAnagraficaTipoStrada: TStringField;
          TAnagraficaDomicilioTipoStrada: TStringField;
          TAnagraficaNumCivico: TStringField;
          TAnagraficaDomicilioNumCivico: TStringField;
          TAnagraficaIDUtente: TIntegerField;
          TAnagraficaLibPrivacy: TBooleanField;
          TAnagraficaCVDataReale: TDateTimeField;
          TAnagraficaCVDataAgg: TDateTimeField;
          TStatiCiviliID: TAutoIncField;
          TStatiCiviliDescrizione: TStringField;
          TStatiCiviliSesso: TStringField;
          TStatiCiviliIDAzienda: TIntegerField;
          TStatiCiviliDescrizione_ENG: TStringField;
          TAnagraficaStatoCivile: TStringField;
          TEventiID: TAutoIncField;
          TEventiIDdaStato: TIntegerField;
          TEventiEvento: TStringField;
          TEventiIDaStato: TIntegerField;
          TEventiNonCancellabile: TBooleanField;
          TEventiIDProcedura: TIntegerField;
          TStatiLKID: TAutoIncField;
          TStatiLKStato: TStringField;
          TStatiLKIDTipoStato: TIntegerField;
          TStatiLKPRman: TBooleanField;
          TStatiLKIDAzienda: TIntegerField;
          TEventiDaStato: TStringField;
          TEventiAStato: TStringField;
          TEventiIDTipoStatoA: TIntegerField;
          TAnagraficaEta: TStringField;
          TwCrypter1: TTwCrypter;
          TTitoliStudioDiploma: TStringField;
          TLivelloLingueID: TAutoIncField;
          TLivelloLingueLivelloConoscenza: TStringField;
          TLivelloLingueIDAzienda: TIntegerField;
          TLivelloLingueLivelloConoscenza_ENG: TStringField;
          TLingueConoscID: TAutoIncField;
          TLingueConoscIDAnagrafica: TIntegerField;
          TLingueConoscLivello: TSmallintField;
          TLingueConoscSoggiorno: TStringField;
          TLingueConoscLivelloScritto: TSmallintField;
          TLingueConoscIdLingua: TIntegerField;
          TLingueConoscDescLivello: TStringField;
          TLingueConoscDescLivelloScritto: TStringField;
          QLingueLKID: TAutoIncField;
          QLingueLKLingua: TStringField;
          QLingueLKIDAzienda: TIntegerField;
          QLingueLKLingua_ENG: TStringField;
          TLingueConoscLingua: TStringField;
          QTempPath: TADOLinkedQuery;
          TLivelloLingueScritto: TADOLinkedTable;
          AutoIncField1: TAutoIncField;
          StringField1: TStringField;
          IntegerField1: TIntegerField;
          StringField2: TStringField;
          TEspLavNomeAzienda: TStringField;
          TEspLavProvinciaAzienda: TStringField;
          TEspLavComuneAzienda: TStringField;
          QAnagFoto: TADOLinkedQuery;
          QGruppiDiplomi: TADOQuery;
          DsQGruppiDiplomi: TDataSource;
          QGruppiDipLK: TADOQuery;
          QGruppiDipLKID: TAutoIncField;
          QGruppiDipLKGruppo: TStringField;
          QGruppiDiplomiID: TAutoIncField;
          QGruppiDiplomiGruppo: TStringField;
          Qado1: TADOQuery;
          Q3: TADOLinkedQuery;
          TAnagraficaInteressanteInternet: TBooleanField;
          Q4: TADOQuery;
          qEsplavRay: TADOQuery;
          dsQEsplavray: TDataSource;
          qEsplavRayID: TAutoIncField;
          qEsplavRayIDAnagrafica: TIntegerField;
          qEsplavRayIDSettore: TIntegerField;
          qEsplavRayAttuale: TBooleanField;
          qEsplavRayAziendaNome: TStringField;
          qEsplavRayAziendaSede: TStringField;
          qEsplavRayAziendaComune: TStringField;
          qEsplavRayAziendaProvincia: TStringField;
          qEsplavRayAziendaStato: TStringField;
          qEsplavRayAziendaFatturato: TFloatField;
          qEsplavRayAziendaNumDipendenti: TSmallintField;
          qEsplavRayAziendaSettore: TStringField;
          qEsplavRayPosizioneGerarchia: TStringField;
          qEsplavRayAnnoDal: TSmallintField;
          qEsplavRayAnnoAl: TSmallintField;
          qEsplavRayDurataMesi: TSmallintField;
          qEsplavRayTipoContratto: TStringField;
          qEsplavRayLivelloContrattuale: TStringField;
          qEsplavRayRetribNettaMensile: TIntegerField;
          qEsplavRayMensilita: TSmallintField;
          qEsplavRayTitoloMansione: TStringField;
          qEsplavRayDescrizioneMansione: TMemoField;
          qEsplavRayIDArea: TIntegerField;
          qEsplavRayIDMansione: TIntegerField;
          qEsplavRayMotivoCessazione: TMemoField;
          qEsplavRayIDAzienda: TIntegerField;
          qEsplavRayIDAreaSettore: TIntegerField;
          qEsplavRayDescrizioneAttivitaAz: TMemoField;
          qEsplavRayPreavvisoGiorni: TSmallintField;
          qEsplavRayPreavvisoCalendario: TBooleanField;
          qEsplavRayPreavvisoTermQualsGiorno: TBooleanField;
          qEsplavRayPreavvisoTermGiorno: TSmallintField;
          qEsplavRayIDTipoContratto: TIntegerField;
          qEsplavRayLKMansione: TStringField;
          qEsplavRayLKArea: TStringField;
          qEsplavRayIDMansioneTemp: TIntegerField;
          qEsplavRayMeseDal: TSmallintField;
          qEsplavRayMeseAl: TSmallintField;
          qEsplavRayIDQualifContr: TIntegerField;
          qEsplavRayAzDenominazione: TStringField;
          qEsplavRayRuolo: TStringField;
          TAnagraficaVIPList: TBooleanField;
          qEsplavRayNomeAzienda: TStringField;
          QGruppiDiplomiGruppo_ENG: TStringField;
          TAnagraficaTipologia: TStringField;
          DsAnagAltreInfo: TDataSource;
          qAnagAltreInfo: TADOLinkedQuery;
          qAnagAltreInfoIDAnagrafica: TIntegerField;
          qAnagAltreInfoNoteCliente: TMemoField;
          TStatiID: TAutoIncField;
          TStatiStato: TStringField;
          TStatiIDTipoStato: TIntegerField;
          TStatiPRman: TBooleanField;
          TStatiIDAzienda: TIntegerField;
          TStatiTipoStato: TStringField;
          TAnagraficaTelAziendaCentralino: TStringField;
          TAnagraficaTelAziendaSegretaria: TStringField;
          TAnagraficaNoteCellulare: TStringField;
          TAnagraficaCellulare2: TStringField;
          TAnagraficaNoteCellulare2: TStringField;
          TAnagraficaEMailUfficio: TStringField;
          TAnagraficaRegione: TStringField;
          TAnagraficaDomicilioRegione: TStringField;
          QAnagFileFile: TADOQuery;
          QAnagFileFileID: TAutoIncField;
          QAnagFileFileFileBloccato: TBooleanField;
          QAnagFileFileFileContent: TMemoField;
          QAnagFileID: TAutoIncField;
          QAnagFileIDAnagrafica: TIntegerField;
          QAnagFileFileType: TStringField;
          QAnagFileFileBloccato: TBooleanField;
          QAnagFileFileContent: TMemoField;
          QAnagFileIDTipo: TIntegerField;
          QAnagFileDescrizione: TStringField;
          QAnagFileDataCreazione: TDateTimeField;
          QAnagFileFlagInvio: TBooleanField;
          QAnagFileSoloNome: TStringField;
          QAnagFileVisibleCliente: TBooleanField;
          QAnagFileInseritoH1Web: TBooleanField;
          QAnagFileDocFile: TBlobField;
          QAnagFileDocExt: TStringField;
          DsQanagFoto: TDataSource;
          QAnagFotoID: TAutoIncField;
          QAnagFotoFoto: TBlobField;
          QAnagFileTipo: TStringField;
          TAnagMansioniCalc_Attuale: TStringField;
          TAnagMansioniCalc_Asp: TStringField;
          TAnagMansioniCalc_Disp: TStringField;
          TEspLavRuoloPresentaz: TStringField;
          TEspLavFlag_CdA: TBooleanField;
          TEspLavFlag_CollegioSind: TBooleanField;
          TEspLavFlag_ConsiglioGest: TBooleanField;
          TEspLavFlag_ConsiglioSorv: TBooleanField;
          QAnagFileFileDocFile: TBlobField;
          QAnagFileFileDocExt: TStringField;
          qFileTemp: TADOQuery;
          qFileTempID: TAutoIncField;
          qFileTempIDAnagrafica: TIntegerField;
          qFileTempIDTipo: TIntegerField;
          qFileTempDescrizione: TStringField;
          qFileTempNomeFile: TStringField;
          qFileTempDataCreazione: TDateTimeField;
          qFileTempFlagInvio: TBooleanField;
          qFileTempSoloNome: TStringField;
          qFileTempVisibleCliente: TBooleanField;
          qFileTempFileType: TStringField;
          qFileTempInseritoH1Web: TBooleanField;
          qFileTempDocFile: TBlobField;
          qFileTempDocExt: TStringField;
          qFileTempFileBloccato: TBooleanField;
          TEventiOrdinamento: TIntegerField;
          TEspLavIDRegione: TIntegerField;
          TEspLavRegione: TStringField;
          Qregioni: TADOQuery;
          Qregioniregione: TStringField;
          QregioniID: TAutoIncField;
          TEspLavIDQualifContr: TIntegerField;
          TEspLavIDGrade: TIntegerField;
          TEspLavRapportiRuolo: TMemoField;
          QStorComm: TADOQuery;
          DSstorComm: TDataSource;
          QStorCommdata: TDateTimeField;
          QStorCommEsito: TStringField;
          QStorCommEvento: TStringField;
          QStorCommcandidato: TStringField;
          QStorCommutente: TStringField;
          qAnagAltreInfoNote: TMemoField;
          qAnagAltreInfoNoteCandidato: TMemoField;
          QAnagFileFileIDUtente: TIntegerField;
          QChekAnagfile: TADOQuery;
          QChekAnagfilefilebloccato: TBooleanField;
          QChekAnagfilesolonome: TStringField;
          QChekAnagfileidutente: TIntegerField;
          QChekAnagfileidanagrafica: TIntegerField;
          QChekAnagfilenominativoCand: TStringField;
          QChekAnagfilenominativoUtente: TStringField;
          QChekAnagfiledocfile: TBlobField;
          QChekAnagfileid: TAutoIncField;
          QLKNominatUtente: TADOQuery;
          DSAnagFile: TDataSource;
          QLKNominatUtenteid: TAutoIncField;
          QLKNominatUtentenominativo: TStringField;
          QAnagFileIDutente: TIntegerField;
          QAnagFileApriCV: TADOQuery;
          DSAnagFileApriCV: TDataSource;
          QApriFile: TADOQuery;
          QApriFileID: TAutoIncField;
          QApriFileIDAnagrafica: TIntegerField;
          QApriFileIDTipo: TIntegerField;
          QApriFileDescrizione: TStringField;
          QApriFileNomeFile: TStringField;
          QApriFileDataCreazione: TDateTimeField;
          QApriFileFlagInvio: TBooleanField;
          QApriFileSoloNome: TStringField;
          QApriFileVisibleCliente: TBooleanField;
          QApriFileFileType: TStringField;
          QApriFileInseritoH1Web: TBooleanField;
          QApriFileDocFile: TBlobField;
          QApriFileDocExt: TStringField;
          QApriFileFileBloccato: TBooleanField;
          QApriFileFileContent: TMemoField;
          QApriFileIDutente: TIntegerField;
          QApriFileNominativo: TStringField;
          QprocedureAuto: TADOQuery;
          QProcAutoParam: TADOQuery;
          TAnagraficadacontattocliente: TBooleanField;
          TEspLavAziendaNome: TStringField;
          TEspLavAziendaSede: TStringField;
          TEspLavAziendaStato: TStringField;
          TEspLavAziendaSettore: TStringField;
          TEspLavAziendaComune: TStringField;
          QAnagFileNomeFile: TStringField;
          QCF: TADOQuery;
          QCod_Comuni: TADOQuery;
          QUltCar: TADOQuery;
          QControllo: TADOQuery;
          qAnagAltreInfoNazionalita: TStringField;
          qAnagAltreInfoServizioMilitareStato: TStringField;
          qAnagAltreInfoServizioMilitareDove: TStringField;
          qAnagAltreInfoServizioMilitareQuando: TStringField;
          qAnagAltreInfoServizioMilitareGrado: TStringField;
          qAnagAltreInfoCateogorieProtette: TStringField;
          qAnagAltreInfoPercentualeInvalidita: TIntegerField;
          qAnagAltreInfoRetribuzione: TStringField;
          qAnagAltreInfoInquadramento: TStringField;
          qAnagAltreInfoBenefits: TStringField;
          QAnagFotoExtFoto: TStringField;
          TAnagraficaStatoNascita: TStringField;
          TAnagraficaDomicilioStato: TStringField;
          TAnagraficaStato: TStringField;
          TEspLavBenefits: TStringField;
          QOrarioAttuale: TADOQuery;
          QOrarioAttualeOraCompleta: TStringField;
          QOrarioAttualeOre: TStringField;
          QOrarioAttualeMinuti: TStringField;
          QOrarioAttualeOreMagg: TStringField;
          QOrarioAttualeMinutiMagg: TStringField;
          TEspLavIDCodCondContrattuale: TIntegerField;
          qCodConContrLK: TADOQuery;
          qCodConContrLKID: TAutoIncField;
          qCodConContrLKCodice: TStringField;
          qCodConContrLKDescrizione: TStringField;
          qCodConContrLKCodGPress2: TStringField;
          TEspLavCondizioneContrattuale: TStringField;
          QMotiviSosp: TADOQuery;
          QMotiviSospID: TAutoIncField;
          QMotiviSospCodice: TStringField;
          QMotiviSospDescMotivoStatoSosp: TStringField;
          QMotiviSospPeriodoSospSTD: TIntegerField;
          QMotiviSospPeriodoPreavvisoFineSosp: TIntegerField;
          DSMotiviSosp: TDataSource;
          QMotiviRichiamoCand: TADOQuery;
          QMotiviRichiamoCandID: TAutoIncField;
          QMotiviRichiamoCandCodice: TStringField;
          QMotiviRichiamoCandDescMotivoRichiamoCandidato: TStringField;
          QMotiviRichiamoCandPeriodoRichiamoSTD: TIntegerField;
          QMotiviRichiamoCandPeriodoPreavvisoFineRichiamo: TIntegerField;
          DSMotiviRichiamoCand: TDataSource;
          TEspLavNote: TMemoField;
          TTitoliStudioNazioneConseg: TStringField;
          TTitoliStudioNote: TMemoField;
          TEspLavSettoreCalc: TStringField;
          TDiplomi: TADOLinkedTable;
          TDiplomiID: TAutoIncField;
          TDiplomiIDArea: TIntegerField;
          TDiplomiTipo: TStringField;
          TDiplomiPunteggioMax: TSmallintField;
          TDiplomiIDAzienda: TStringField;
          TDiplomiTipo_ENG: TStringField;
          TDiplomiDescrizione_ENG: TStringField;
          TDiplomiArea: TStringField;
          TDiplomiIDGruppo: TIntegerField;
          TDiplomiGruppo: TStringField;
          TDiplomiDescrizione: TStringField;
          TAnagraficaChiaveOmonimia: TStringField;
          QPswProcedure: TADOQuery;
          QPswProcedureID: TAutoIncField;
          QPswProcedureDescrizione: TStringField;
          QPswProcedurePassword: TStringField;
          QPswProcedureCodProcedura: TIntegerField;
          QComunitaNaz_Stato: TADOQuery;
          DSComunitaNaz_Stato: TDataSource;
          qAnagAltreInfoIDComunitaNazStato: TIntegerField;
          qAnagAltreInfoComunitaNazStatoLK: TStringField;
          QComunitaNaz_StatoID: TAutoIncField;
          QComunitaNaz_StatoDescrizione: TStringField;
          DSQClassificazioni: TDataSource;
          QClassificazioni: TADOQuery;
          QClassificazioniID: TAutoIncField;
          QClassificazioniDescrizione: TStringField;
          QClassificazioniCodice: TStringField;
          QClassificazioniValore: TIntegerField;
          TAnagraficaIDClassificazione: TIntegerField;
          QGiudiziColloquio: TADOQuery;
          QValutazModel: TADOQuery;
          TTitoliStudioMassimoTitolo: TBooleanField;
          QAnagFileFlagEsportaCVGeneraPres: TBooleanField;
          qAnagAltreInfoPartitaIva: TStringField;
          TEspLavVisibileWeb: TBooleanField;
          TEspLavAreaSettore: TStringField;
    TLingueConoscIDComprensione: TIntegerField;
    TLivelloLingueComprensione: TADOLinkedTable;
    AutoIncField2: TAutoIncField;
    StringField3: TStringField;
    IntegerField2: TIntegerField;
    StringField4: TStringField;
    TLingueConoscDescLivelloComprensione: TStringField;
    QTemp2: TADOQuery;
    qAnagAltreInfoCittadinanza: TStringField;
    QTempAdoStd1: TADOQuery;
          procedure DsAnagraficaStateChange(Sender: TObject);
          procedure DsTitoliStudioStateChange(Sender: TObject);
          procedure DsCorsiExtraStateChange(Sender: TObject);
          procedure TAnagrafica_oldCalcFields(DataSet: TDataSet);
          procedure TAnagrafica_oldBeforePost(DataSet: TDataSet);
          procedure TPromemoria_OLDAfterPost(DataSet: TDataSet);
          procedure DataCreate(Sender: TObject);
          procedure DsStatiStateChange(Sender: TObject);
          procedure TTitoliStudio_oldAfterInsert(DataSet: TDataSet);
          procedure DsDiplomiStateChange(Sender: TObject);
          procedure DsAreeDiplomiStateChange(Sender: TObject);
          procedure DsEventiStateChange(Sender: TObject);
          procedure TEventiAfterInsert(DataSet: TDataSet);
          procedure TEspLav_oldAfterInsert(DataSet: TDataSet);
          procedure TAnagrafica_oldAfterPost(DataSet: TDataSet);
          procedure TStorico_oldAfterPost(DataSet: TDataSet);
          procedure TPromemABS_OLDAfterInsert(DataSet: TDataSet);
          procedure TPromemABS_OLDAfterPost(DataSet: TDataSet);
          procedure TAnagrafica_oldAfterInsert(DataSet: TDataSet);
          procedure DsLingueStateChange(Sender: TObject);
          procedure TAnagrafica_oldAfterScroll(DataSet: TDataSet);
          procedure TAnagrafica_oldAfterDelete(DataSet: TDataSet);
          procedure TTitoliStudio_oldBeforeEdit(DataSet: TDataSet);
          procedure TCorsiExtra_oldBeforeEdit(DataSet: TDataSet);
          procedure TLingueConosc_oldBeforeEdit(DataSet: TDataSet);
          procedure TEspLav_oldBeforeEdit(DataSet: TDataSet);
          procedure QStorico_oldBeforeOpen(DataSet: TDataSet);
          procedure QStorico_oldAfterClose(DataSet: TDataSet);
          procedure TEspLav_oldBeforeOpen(DataSet: TDataSet);
          procedure TEspLav_oldAfterClose(DataSet: TDataSet);
          procedure TDisponibAnag_OldBeforeOpen(DataSet: TDataSet);
          procedure TDisponibAnag_OldAfterClose(DataSet: TDataSet);
          procedure TLingueConosc_oldBeforeOpen(DataSet: TDataSet);
          procedure TLingueConosc_oldAfterClose(DataSet: TDataSet);
          procedure TTitoliStudio_oldBeforeOpen(DataSet: TDataSet);
          procedure TTitoliStudio_oldAfterClose(DataSet: TDataSet);
          procedure TAziendeBeforeOpen(DataSet: TDataSet);
          procedure TAziendeAfterClose(DataSet: TDataSet);
          procedure TMansioniLK_oldBeforeOpen(DataSet: TDataSet);
          procedure TStati_OLDAfterPost(DataSet: TDataSet);
          procedure TDiplomi_oldAfterPost(DataSet: TDataSet);
          procedure TAreeDiplomi_oldAfterPost(DataSet: TDataSet);
          procedure TLingue_oldAfterPost(DataSet: TDataSet);
          procedure DsQAnagNoteStateChange(Sender: TObject);
          procedure DsGlobalStateChange(Sender: TObject);
          procedure TTitoliStudio_oldAfterPost(DataSet: TDataSet);
          procedure TTitoliStudio_oldAfterDelete(DataSet: TDataSet);
          procedure TCorsiExtra_oldAfterPost(DataSet: TDataSet);
          procedure TCorsiExtra_oldAfterDelete(DataSet: TDataSet);
          procedure TEspLav_oldAfterPost(DataSet: TDataSet);
          procedure TZone_oldAfterPost(DataSet: TDataSet);
          procedure TRisorse_OLDAfterPost(DataSet: TDataSet);
          procedure QAnagNote_oldAfterPost(DataSet: TDataSet);
          procedure TEventiAfterPost(DataSet: TDataSet);
          procedure DB_OLDBeforeConnect(Sender: TObject);
          procedure TAnagrafica_oldBeforeOpen(DataSet: TDataSet);
          procedure TAnagrafica_oldAfterClose(DataSet: TDataSet);
          procedure TAnagrafica_oldBeforeEdit(DataSet: TDataSet);
          procedure TEspLavCalcFields(DataSet: TDataSet);
          procedure DsQGruppiDiplomiStateChange(Sender: TObject);
          procedure QGruppiDiplomiAfterPost(DataSet: TDataSet);
          procedure DataModuleDestroy(Sender: TObject);
          procedure qEsplavRayBeforeOpen(DataSet: TDataSet);
          procedure qEsplavRayCalcFields(DataSet: TDataSet);
          procedure TCorsiExtraAfterInsert(DataSet: TDataSet);
          procedure TAnagraficaAfterOpen(DataSet: TDataSet);
          procedure TAnagMansioniCalcFields(DataSet: TDataSet);
          procedure QAnagFotoAfterOpen(DataSet: TDataSet);
          procedure TEspLavBeforePost(DataSet: TDataSet);
          procedure QProcAutoParamBeforeOpen(DataSet: TDataSet);
          procedure QAnagFileBeforeOpen(DataSet: TDataSet);
          procedure DSMotiviSospStateChange(Sender: TObject);
          procedure DSMotiviRichiamoCandStateChange(Sender: TObject);
          procedure DSComunitaNaz_StatoStateChange(Sender: TObject);
          procedure qAnagAltreInfoBeforeOpen(DataSet: TDataSet);
          procedure TAnagraficaBeforeCancel(DataSet: TDataSet);
    procedure QAnagNoteBeforePost(DataSet: TDataSet);
     private
          { private declarations }
          xIDRuolo, xIDArea, xIDFeature, xVecchioIDAnag: integer;
          xcognomeGlobale, xnomeGlobale: string;
     public
          { public declarations }
          xInterrForm, xValutazForm, xIntervCons, xOKcheck, xFiltraArea, xControllaComune, xSelAziende: boolean;
          xNuovoRuolo, xSelRuolo, xInsLingua, xControlla, xSchedaCand: boolean;
          xLinkAnnuncio: integer;
          xPwdSa: TStringList;
          // campi per storicizzazione anagrafica
          xCodiceFiscale, xCognome, xNome, xCID, xSesso, xDataNascita, xLuogoNascita, xProvNascita, xStatoNascita, xStatoCivile, xidstatocivile,
               xTipoStrada, xIndirizzo, xNumCivico, xEdificio, xPresso, xCap, xFrazione, xComune, xProvincia, xRegione, xStato,
               xDomicilioTipoStrada, xDomicilioIndirizzo, xDomicilioNumCivico, xDomicilioEdificio, xDomicilioPresso, xDomicilioCap,
               xDomicilioFrazione, xDomicilioComune, xDomicilioProvincia, xdomicilioregione, xDomicilioStato,
               xRecapitiTelefonici, xCellulare, xTelUfficio, xEmail, xEmailUfficio, xCPostale, xFax: string;

          function CheckVersione(xVer: string): boolean;
     end;

var
     Data: TData;

implementation

uses Main, Curriculum, SelArea,
     ModuloDati2, ValutazDip, CompInserite, InsRuolo,
     InsCaratt, Lingue, uUtilsVarie, SchedaCand, Foto;

{$R *.DFM}

procedure TData.DsAnagraficaStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsAnagrafica.State in [dsEdit, dsInsert];
     MainForm.TbBAnagNew.Enabled := not b;
     MainForm.TbBAnagDel.Enabled := not b;
     MainForm.TbBAnagMod.Enabled := not b;
     MainForm.BRiepCand.Enabled := not b;
     MainForm.PanAnag.Enabled := b;
     MainForm.PanAnag2.Enabled := b;
     MainForm.TbBAnagOK.Enabled := b;
     MainForm.TbBAnagCan.Enabled := b;
     //     ColloquioForm.TbBAnagOK.Enabled:=b;
     //     ColloquioForm.TbBAnagCan.Enabled:=b;
end;

procedure TData.DsTitoliStudioStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsTitoliStudio.State in [dsEdit, dsInsert];
     CurriculumForm.TbBTitoliNew.Enabled := not b;
     CurriculumForm.TbBTitoliDel.Enabled := not b;
     CurriculumForm.TbBTitoliOK.Enabled := b;
     CurriculumForm.TbBTitoliCan.Enabled := b;
end;

procedure TData.DsCorsiExtraStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsCorsiExtra.State in [dsEdit, dsInsert];
     CurriculumForm.TbBCorsiExtraNew.Enabled := not b;
     CurriculumForm.TbBCorsiExtraDel.Enabled := not b;
     CurriculumForm.TbBCorsiExtraOK.Enabled := b;
     CurriculumForm.TbBCorsiExtraCan.Enabled := b;
end;

//[/TONI20020724\]

procedure TData.TAnagrafica_oldCalcFields(DataSet: TDataSet);
var xAnno, xMese, xgiorno: Word;
begin
     {     if TAnagraficaDataNascita.asString='' then
               TAnagraficaEta.asString:=''
          else begin
               DecodeDate((Date-TAnagraficaDataNascita.Value),xAnno,xMese,xgiorno);
               // TAnagraficaEta.Value:=StrToInt(copy(xAnno,3,2));
               TAnagraficaEta.Value:=copy(IntToStr(xAnno),3,2);
          end;}
     if DataSet.FieldByName('DataNascita').asString = '' then
          DataSet.FieldByName('Eta').asString := ''
     else begin
          DecodeDate((Date - DataSet.FieldByName('DataNascita').Value), xAnno, xMese, xgiorno);
          // TAnagraficaEta.Value:=StrToInt(copy(xAnno,3,2));
          DataSet.FieldByName('Eta').Value := copy(IntToStr(xAnno), 3, 2);
     end;

end;

procedure TData.TAnagrafica_oldBeforePost(DataSet: TDataSet);
var xAnno, xMese, xgiorno: Word;
     xData: TDateTime;
     i, k: integer;
     xCogn, xNome: string;
begin
     {     if xOKcheck then begin
               if (TAnagraficaCognome.Value='')or(TAnagraficaNome.Value='') then
                    MessageDlg('ATTENZION: il cognome o il nome sono vuoti !!',mtError, [mbOK],0);
               // controllo omonimia
               xCogn:=TAnagraficaCognome.Value;
               xNome:=TAnagraficaNome.Value;
               Q1.close;
               Q1.SQL.clear;
               Q1.SQL.add('select count(*) Num from Anagrafica where cognome like "%'+xCogn+'%"');
               Q1.SQL.add('and Nome like "%'+xNome+'%"');
               Q1.Open;
               if Data.Q1.FieldByName('Num').asInteger>1 then
                    MessageDlg('ATTENZIONE: verificare possibile omonimia',mtWarning, [mbOK],0);
          end;

          if TAnagraficaDataNascita.asString<>'' then begin
               DecodeDate(TAnagraficaDataNascita.VAlue,xAnno,xMese,xGiorno);
               TAnagraficaGiornoNascita.Value:=xGiorno;
               TAnagraficaMeseNascita.Value:=xMese;
          end;
          // Ultimo utente ad aver aggiornato i dati
          TAnagraficaIDUtenteMod.Value:=MainForm.xIDUtenteAttuale;}

      // controllo sulla partitaIVA per SELTIS - OPENJOB
     if pos('OPENJOB', UpperCase(data.Global.fieldbyname('NomeAzienda').asString)) > 0 then begin
          if (MainForm.DBEPartitaIVA.Text <> '') and (Length(MainForm.DBEPartitaIVA.Text) < 11) then begin
               MessageDlg('La Partita IVA Deve avere la lunghezza di 11 cifre', mtError, [mbOK], 0);
               MainForm.DBEPartitaIVA.Text := '';
               qAnagAltreInfoPartitaIva.AsString := '';
               //TAnagraficaPartitaIVA.AsString := '';
          end;
     end;

     if ((xcognomeglobale <> MainForm.DBECognome.Text) or (xnomeglobale <> MainForm.DBEdit2.Text)) then begin
          Log_Operation(MainForm.xIDUtenteAttuale, 'Anagrafica', Data.TAnagrafica.FieldByName('ID').AsInteger, 'C',
               xcognomeglobale + ' ' + xnomeglobale + ' sono cambiati in ' + MainForm.DBECognome.Text + ' ' + MainForm.DBEdit2.Text);
     end;

     if xOKcheck then begin
          if (DataSet.FieldByName('Cognome').AsString = '') or (DataSet.FieldByName('Nome').AsString = '') then
               MessageDlg('ATTENZIONE: il cognome o il nome sono vuoti !!', mtError, [mbOK], 0);
          // controllo omonimia
          xCogn := TrimRight(DataSet.FieldByName('Cognome').AsString);
          xNome := TrimRight(DataSet.FieldByName('Nome').AsString);
          Q1.close;
          Q1.SQL.clear;
          Q1.SQL.add('select count(*) Num from Anagrafica where cognome like ''"%' + StringReplace(xCogn, '''', '''''', [rfreplaceall]) + '%"''');
          Q1.SQL.add('and Nome like ''"%' + StringReplace(xNome, '''', '''''', [rfreplaceall]) + '%"''');
          Q1.Open;
          {Q1.SQL.Text:='select count(*) Num from Anagrafica where cognome like '':xCognome:%'' and nome like '':xNome:%''';
          Q1.ParamByName['xCognome']:=xCogn;
          Q1.ParamByName['xNome']:=xNome;
          Showmessage(Q1.SQL.Text);
          Q1.Open; }

          if Data.Q1.FieldByName('Num').asInteger > 1 then
               MessageDlg('ATTENZIONE: verificare possibile omonimia', mtWarning, [mbOK], 0);
     end;

     if DataSet.FieldByName('DataNascita').asString <> '' then begin
          DecodeDate(DataSet.FieldByName('DataNascita').Value, xAnno, xMese, xGiorno);
          DataSet.FieldByName('GiornoNascita').AsInteger := xGiorno;
          DataSet.FieldByName('MeseNascita').AsInteger := xMese;
     end;
     // Ultimo utente ad aver aggiornato i dati
     DataSet.FieldByName('IDUtenteMod').AsInteger := MainForm.xIDUtenteAttuale;
     if QClassificazioni.Locate('Descrizione', MainForm.xClassificazione, []) then
          TAnagraficaIDClassificazione.Value := QClassificazioniID.Value
     else
          TAnagraficaIDClassificazione.Value := 0;
end;
//[/TONI20020724\]FINE

procedure TData.TPromemoria_OLDAfterPost(DataSet: TDataSet);
begin
     QPromOggi.Close;
     QPromOggi.Open;
     case QPromOggi.RecordCount of
          0: begin
                    MainForm.StatusBar1.Panels[2].Text := 'nessun messaggio in scadenza oggi';
                    MainForm.AdvOfficeStatusBar1.Panels[2].text := 'nessun messaggio in scadenza oggi';
               end;
          1: begin
                    MainForm.StatusBar1.Panels[2].Text := '� presente un messaggio in scadenza oggi';
                    MainForm.AdvOfficeStatusBar1.Panels[2].text := '� presente un messaggio in scadenza oggi';
               end;
     else begin
               MainForm.StatusBar1.Panels[2].Text := 'Sono presenti ' + IntToStr(QPromOggi.RecordCount) + ' messaggi in scadenza oggi';
               MainForm.AdvOfficeStatusBar1.Panels[2].text := 'Sono presenti ' + IntToStr(QPromOggi.RecordCount) + ' messaggi in scadenza oggi';
          end;
     end;
     QPromOggi.Close;
end;

procedure TData.DataCreate(Sender: TObject);
var xStringList: TStringList;
     xStringa: string; begin
     DB.Connected := False;
     xStringList := TStringList.Create;
     // carica stringa di connessione
     if FileExists(ExtractFileDir(Application.ExeName) + '\H1ConnString.txt') then begin
          xStringList.LoadFromFile(ExtractFileDir(Application.ExeName) + '\H1ConnString.txt');
          xStringa := xStringList.Strings[0];
          DB.Connected := False;
          DB.ConnectionString := TwCrypter1.DecryptString(xStringa);
          DB.Connected := True;
     end else begin
          messageDlg('FILE "H1ConnString.txt" NON TROVATO', mtError, [mbOK], 0);
          //DB.Connected:=False;
          //DB.Connected:=True;
          //Application.Terminate;
     end;
     xStringList.Free;

     xInterrForm := False;
     xValutazForm := False;
     xIntervCons := False;
     xSelRuolo := True;
     xNuovoRuolo := False;
     xInsLingua := True;
     xOKcheck := True;
     xFiltraArea := True;
     xLinkAnnuncio := 0;
     xControlla := False;
     xSchedaCand := False;
     xControllaComune := True;
end;

procedure TData.DsStatiStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsStati.State in [dsEdit, dsInsert];
     MainForm.TbBStatiNew.Enabled := not b;
     MainForm.TbBStatiDel.Enabled := not b;
     MainForm.TbBStatiOK.Enabled := b;
     MainForm.TbBStatiCan.Enabled := b;
end;

//[/TONI20020724\]

procedure TData.TTitoliStudio_oldAfterInsert(DataSet: TDataSet);
begin
     //    TTidolistudioLaureaBreve.Value:=False;
     DataSet.FieldByname('LaureaBreve').Value := False;
end;
//[/TONI20020724\]FINE

procedure TData.DsDiplomiStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsDiplomi.State in [dsEdit, dsInsert];
     MainForm.TbBDiplomiNew.Enabled := not b;
     MainForm.TbBDiplomiDel.Enabled := not b;
     MainForm.TbBDiplomiOK.Enabled := b;
     MainForm.TbBDiplomiCan.Enabled := b;
end;

procedure TData.DsAreeDiplomiStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsAreeDiplomi.State in [dsEdit, dsInsert];
     MainForm.TbBAreeDiplNew.Enabled := not b;
     MainForm.TbBAreeDiplDel.Enabled := not b;
     MainForm.TbBAreeDiplOK.Enabled := b;
     MainForm.TbBAreeDiplCan.Enabled := b;
end;

procedure TData.DsEventiStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsEventi.State in [dsEdit, dsInsert];
     MainForm.TbBEventiNew.Enabled := not b;
     MainForm.TbBEventiDel.Enabled := not b;
     MainForm.TbBEventiMod.Enabled := not b;

     //MainForm.DBGEventi.ReadOnly:=not b;
     MainForm.TbBEventiOK.Enabled := b;
     MainForm.TbBEventiCan.Enabled := b;
end;

//[/TONI20020724\]

procedure TData.TEventiAfterInsert(DataSet: TDataSet);
begin
     //     TEventiNonCancellabile.Value:=False;
     DataSet.FieldByName('NonCancellabile').Value := False;
end;

procedure TData.TEspLav_oldAfterInsert(DataSet: TDataSet);
begin
     //TEspLavMensilita.Value:=14;

     if pos('EMERGENCY', UpperCase(data.Global.fieldbyname('NomeAzienda').asString)) < 0 then
          DataSet.FieldByName('Mensilita').Value := 14;
end;

procedure TData.TAnagrafica_oldAfterPost(DataSet: TDataSet);
begin
     MainForm.LTotCand.Caption := IntToStr(TAnagrafica.RecordCount);
     MainForm.lblIDAnag.Caption := TAnagraficaID.AsString;
     //     Log_Operation(MainForm.xIDUtenteAttuale,'Anagrafica',TAnagraficaID.Value,'U');
     Log_Operation(MainForm.xIDUtenteAttuale, 'Anagrafica', DataSet.FieldByName('ID').Value, 'U');
end;

procedure TData.TPromemABS_OLDAfterInsert(DataSet: TDataSet);
begin
     {     TPromemABSIDUtente.Value:=MainForm.xIDUtenteAttuale;
          TPromemABSIDUtenteDa.Value:=MainForm.xIDUtenteAttuale;
          TPromemABSDataIns.Value:=Date;
          TPromemABSDataDaLeggere.Value:=Date;
          TPromemABSEvaso.Value:=False;}
     DataSet.FieldByName('IDUtente').Value := MainForm.xIDUtenteAttuale;
     DataSet.FieldByName('IDUtenteDa').Value := MainForm.xIDUtenteAttuale;
     DataSet.FieldByName('DataIns').Value := Date;
     DataSet.FieldByName('DataDaLeggere').Value := Date;
     DataSet.FieldByName('Evaso').Value := False;
end;
//[/TONI20020724\]FINE

procedure TData.TStorico_oldAfterPost(DataSet: TDataSet);
begin
     QStorico.Close;
     QStorico.Open;
end;

procedure TData.TPromemABS_OLDAfterPost(DataSet: TDataSet);
begin
     if TPromemoria.Active then TPromemoria.Refresh;
     QPromOggi.Close;
     QPromOggi.Open;
end;

procedure TData.TAnagrafica_oldAfterInsert(DataSet: TDataSet);
begin
     if MainForm.Pagecontrol5.ActivePage = MainForm.TsStatoDip then begin
          MainForm.TSAnagStorico.TabVisible := True;
     end;
     if MainForm.Pagecontrol5.ActivePage = MainForm.TsStatoEsterno then begin
          MainForm.TSAnagStorico.TabVisible := True;
     end;
     if MainForm.Pagecontrol5.ActivePage = MainForm.TsStatoUscito then begin
          MainForm.TSAnagStorico.TabVisible := True;
     end;
end;

procedure TData.DsLingueStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsLingue.State in [dsEdit, dsInsert];
     MainForm.TbBLingueNew.Enabled := not b;
     MainForm.TbBLingueDel.Enabled := not b;
     MainForm.TbBLingueOK.Enabled := b;
     MainForm.TbBLingueCan.Enabled := b;
end;

//[/TONI20020724\]

procedure TData.TAnagrafica_oldAfterScroll(DataSet: TDataSet);
begin
     {     if xControlla then SettaDiritti(TAnagraficaID.Value);
          if (TAnagraficaIDProprietaCV.asString='')or(TAnagraficaIDProprietaCV.Value=0) then
               Mainform.EPropCV.Text:=GlobalNomeAzienda.Value
          else begin
               Q1.Close;
               Q1.SQL.text:='select Descrizione from EBC_Clienti where ID='+TAnagraficaIDProprietaCV.asString;
               Q1.Open;
               Mainform.EPropCV.text:=Q1.FieldbyName('Descrizione').asString;
               Q1.Close;
          end;
          // Utente associato al candidato
          if (TAnagraficaIDUtente.asString<>'')and(TAnagraficaIDUtente.Value>0) then begin
               Q1.Close;
               Q1.SQL.text:='select Nominativo from Users where ID='+TAnagraficaIDUtente.asString;
               Q1.Open;
               Mainform.ECandUtente.Text:=Q1.FieldbyName('Nominativo').asString;
               Q1.Close;
          end else Mainform.ECandUtente.Text:='';}

  

     xcognomeglobale := TAnagraficaCognome.AsString;
     xnomeglobale := TAnagraficaNome.AsString;

     if xControlla then SettaDiritti(DataSet.FieldByName('ID').AsInteger);
     if (DataSet.FieldByName('IDProprietaCV').asString = '') or (DataSet.FieldByName('IDProprietaCV').AsInteger = 0) then
          Mainform.EPropCV.Text := Global.FieldByName('NomeAzienda').AsString
     else begin
          Q1.Close;
          Q1.SQL.text := 'select Descrizione from EBC_Clienti where ID=' + DataSet.FieldByName('IDProprietaCV').asString;
          Q1.Open;
          Mainform.EPropCV.text := Q1.FieldByName('Descrizione').asString;
          Q1.Close;
     end;
     // Utente associato al candidato
     if (DataSet.FieldByName('IDUtente').asString <> '') and (DataSet.FieldByName('IDUtente').AsInteger > 0) then begin
          Q1.Close;
          Q1.SQL.text := 'select Nominativo from Users where ID=' + DataSet.FieldByName('IDUtente').asString;
          Q1.Open;
          Mainform.ECandUtente.Text := Q1.FieldByName('Nominativo').asString;
          Q1.Close;
     end else Mainform.ECandUtente.Text := '';
     if (Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 3)) = 'RAY') or (Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 7)) = 'MISTRAL') then begin
          qEsplavRay.Close;
          qEsplavRay.Open;
     end;

     //dati
     if MainForm.PageControl2.ActivePage = MainForm.TSAnagDatiAnag then begin
          MainForm.FrmLinkwebHome1.QWebLink.Close;
          MainForm.FrmLinkwebHome1.QWebLink.Open;
     end;

     // Sintesi
     if MainForm.PageControl2.ActivePage = MainForm.TSAnagSintesi then begin
          MainForm.RelazioniCandFrame1.xTipo := 'A';
          MainForm.RelazioniCandFrame1.CreaAlbero;
     end;

     // Campi personalizzati
     if MainForm.PageControl2.ActivePage = MainForm.TSCampPers then begin
          if MainForm.CampiPersFrame1.TCampiPers.Active then MainForm.CampiPersFrame1.TCampiPers.close;
          MainForm.CampiPersFrame1.CaricaValori;
          MainForm.CampiPersFrame1.ImpostaCampi;
     end;

     // link web
     if MainForm.PageControl2.ActivePage = MainForm.TSAnagWeb then begin
          MainForm.FrameWebLinks1.xID := Data.TAnagraficaID.Value;
          MainForm.FrameWebLinks1.QWebLinks.Close;
          MainForm.FrameWebLinks1.InsRecords;
          MainForm.FrameWebLinks1.QWebLinks.Open;
     end;

     QClassificazioni.Close;
     QClassificazioni.Open;
     MainForm.CBClassPotenziale.items.Clear;
     QClassificazioni.First;
     MainForm.CBClassPotenziale.items.Add('');
     while not QClassificazioni.EOF do begin
          MainForm.CBClassPotenziale.items.Add(QClassificazioni.FieldByName('Descrizione').asString);
          QClassificazioni.Next;
     end;
     if QClassificazioni.Locate('ID', TAnagraficaIDClassificazione.AsString, []) then
          MainForm.CBClassPotenziale.ItemIndex := QClassificazioni.RecNo
     else
          MainForm.CBClassPotenziale.ItemIndex := 0;

     if not Data.TAnagrafica.Active then begin
          mainform.LTotCand.Caption := '';
          mainform.lblIDAnag.Caption := '';
     end else begin
          mainform.LTotCand.Caption := IntToStr(TAnagrafica.RecordCount);
          mainform.lblIDAnag.Caption := TAnagraficaID.AsString;
     end;

end;
//[/TONI20020724\]FINE

procedure TData.TAnagrafica_oldAfterDelete(DataSet: TDataSet);
begin
     MainForm.LTotCand.Caption := IntToStr(TAnagrafica.RecordCount);
     MainForm.lblIDAnag.Caption := TAnagraficaID.AsString;
end;

procedure TData.TTitoliStudio_oldBeforeEdit(DataSet: TDataSet);
begin
     if not MainForm.CheckProfile('05112') then Abort;
end;

procedure TData.TCorsiExtra_oldBeforeEdit(DataSet: TDataSet);
begin
     if not MainForm.CheckProfile('05122') then Abort;
end;

procedure TData.TLingueConosc_oldBeforeEdit(DataSet: TDataSet);
begin
     if not MainForm.CheckProfile('05132') then Abort;
end;

procedure TData.TEspLav_oldBeforeEdit(DataSet: TDataSet);
begin
     if not MainForm.CheckProfile('05142') then Exit;
end;

procedure TData.QStorico_oldBeforeOpen(DataSet: TDataSet);
begin
     TEventiLK.Open;
     TRicercheLK.Open;
     //TClientiLK.Open;
end;

procedure TData.QStorico_oldAfterClose(DataSet: TDataSet);
begin
     TEventiLK.Close;
     TRicercheLK.Close;
     //TClientiLK.Close;
end;

procedure TData.TEspLav_oldBeforeOpen(DataSet: TDataSet);
begin
     //[/GIULIO20020925\]
     //     TMansioniLK.Open;
     //     QSettoriLK.Open;
     //     TAziendeLK.Open;
end;

procedure TData.TEspLav_oldAfterClose(DataSet: TDataSet);
begin
     //     TMansioniLK.Close;
     //     TAziendeLK.Close;
     //     QSettoriLK.Close;
end;

procedure TData.TDisponibAnag_OldBeforeOpen(DataSet: TDataSet);
begin
     TMansioniLK.Open;
end;

procedure TData.TDisponibAnag_OldAfterClose(DataSet: TDataSet);
begin
     TMansioniLK.Close;
end;

procedure TData.TLingueConosc_oldBeforeOpen(DataSet: TDataSet);
begin
     TLivelloLingue.Open;
     QLingueLK.Open;
end;

procedure TData.TLingueConosc_oldAfterClose(DataSet: TDataSet);
begin
     TLivelloLingue.Close;
     QLingueLK.Close;
end;

procedure TData.TTitoliStudio_oldBeforeOpen(DataSet: TDataSet);
begin
     TDiplomiLK.Open;
end;

procedure TData.TTitoliStudio_oldAfterClose(DataSet: TDataSet);
begin
     TDiplomiLK.Close;
end;

procedure TData.TAziendeBeforeOpen(DataSet: TDataSet);
begin
     QSettoriLK.Open;
end;

procedure TData.TAziendeAfterClose(DataSet: TDataSet);
begin
     QSettoriLK.Close;
end;

procedure TData.TMansioniLK_oldBeforeOpen(DataSet: TDataSet);
begin
     if not TAreeLK.Active then TAreeLK.Open;
end;

procedure TData.TStati_OLDAfterPost(DataSet: TDataSet);
begin
     TStati.Close;
     TStati.Open;
end;

procedure TData.TDiplomi_oldAfterPost(DataSet: TDataSet);
var xID: integer;
begin
     xID := TDiplomiID.Value;
     TDiplomi.Close;
     TDiplomi.Open;
     TDiplomi.Locate('ID', xID, []);
end;

procedure TData.TAreeDiplomi_oldAfterPost(DataSet: TDataSet);
begin
     TAreeDiplomi.Close;
     TAreeDiplomi.Open;
end;

procedure TData.TLingue_oldAfterPost(DataSet: TDataSet);
begin
     TLingue.Close;
     TLingue.Open;
end;

procedure TData.DsQAnagNoteStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsQAnagNote.State in [dsEdit, dsInsert];
     MainForm.BAnagNoteOK.Enabled := b;
     MainForm.BAnagNoteCan.Enabled := b;
end;

procedure TData.DsGlobalStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsGlobal.State in [dsEdit, dsInsert];
     MainForm.BGlobalOK.Enabled := b;
     MainForm.BGlobalCAn.Enabled := b;
end;

//[/TONI20020724\]

procedure TData.TTitoliStudio_oldAfterPost(DataSet: TDataSet);
begin
     //     Log_Operation(MainForm.xIDUtenteAttuale,'TitoliStudio',TTitoliStudioID.Value,'U');
     Log_Operation(MainForm.xIDUtenteAttuale, 'TitoliStudio', DataSet.FieldByname('ID').Value, 'U');
end;

procedure TData.TTitoliStudio_oldAfterDelete(DataSet: TDataSet);
begin
     //     Log_Operation(MainForm.xIDUtenteAttuale,'TitoliStudio',TTitoliStudioID.Value,'D');
     Log_Operation(MainForm.xIDUtenteAttuale, 'TitoliStudio', DataSet.FieldByname('ID').Value, 'D');
end;

procedure TData.TCorsiExtra_oldAfterPost(DataSet: TDataSet);
begin
     //     Log_Operation(MainForm.xIDUtenteAttuale,'CorsiExtra',TCorsiExtraID.Value,'U');
     Log_Operation(MainForm.xIDUtenteAttuale, 'CorsiExtra', DataSet.FieldByName('ID').Value, 'U');
end;

procedure TData.TCorsiExtra_oldAfterDelete(DataSet: TDataSet);
begin
     //     Log_Operation(MainForm.xIDUtenteAttuale,'CorsiExtra',TCorsiExtraID.Value,'D');
     Log_Operation(MainForm.xIDUtenteAttuale, 'CorsiExtra', DataSet.FieldByName('ID').Value, 'D');
end;

procedure TData.TEspLav_oldAfterPost(DataSet: TDataSet);
begin
     //     Log_Operation(MainForm.xIDUtenteAttuale,'EsperienzeLavorative',TEspLavID.value,'U');
     Log_Operation(MainForm.xIDUtenteAttuale, 'EsperienzeLavorative', DataSet.FieldByName('ID').value, 'U');
end;

procedure TData.TZone_oldAfterPost(DataSet: TDataSet);
begin
     TZone.Close;
     TZone.Open;
end;

procedure TData.TRisorse_OLDAfterPost(DataSet: TDataSet);
begin
     TRisorse.Close;
     TRisorse.Open;
end;

procedure TData.QAnagNote_oldAfterPost(DataSet: TDataSet);
begin
     //     QAnagNote.ApplyUpdates;
     //     QAnagNote.CommitUpdates;
end;

procedure TData.TEventiAfterPost(DataSet: TDataSet);
begin
     TEventi.Close;
     TEventi.Open;
end;

procedure TData.DB_OLDBeforeConnect(Sender: TObject);
begin
     // gestione temporanea password
     xPwdSa := TStringList.create;
     //     DB.Params.clear;                         TADOConnection
     if FileExists(ExtractFileDir(Application.ExeName) + '\Ugh1PdSa.txt') then begin
          xPwdSa.LoadFromFile(ExtractFileDir(Application.ExeName) + '\Ugh1PdSa.txt');
          //          DB.Params.add('USER NAME='+xPwdSa.Strings[0]);
          //          DB.Params.add('PASSWORD='+xPwdSa.Strings[1]);
     end else begin
          //          DB.Params.add('USER NAME=sa');
          //          DB.Params.add('PASSWORD=sa');
     end;
     //DB.Connected:=true;
     xPwdSa.Free;
end;
//[/TONI20020724\]FINE

procedure TData.TAnagrafica_oldBeforeOpen(DataSet: TDataSet);
begin
     QTipologieSoggLK.Open;
end;

procedure TData.TAnagrafica_oldAfterClose(DataSet: TDataSet);
begin
     QTipologieSoggLK.Close;
     qAnagAltreInfo.Close;
end;

procedure TData.TAnagrafica_oldBeforeEdit(DataSet: TDataSet);
begin
     if Global.FieldByName('Storicizza_Sel').asBoolean then begin
          xCodiceFiscale := TAnagraficaCodiceFiscale.asString;
          xCognome := TAnagraficaCognome.asString;
          xNome := TAnagraficaNome.asString;
          //xCID := TAnagraficaCID.asString;
          xSesso := TAnagraficaSesso.asString;
          xDataNascita := TAnagraficaDataNascita.asString;
          xLuogoNascita := TAnagraficaLuogoNascita.asString;
          //xProvNascita := TAnagraficaProvNascita.asString;
          xStatoNascita := TAnagraficaStatoNascita.asString;
          xStatoCivile := TAnagraficaStatoCivile.asString;
          xidstatocivile := TAnagraficaIDStatoCivile.asString;

          xTipoStrada := TAnagraficaTipoStrada.asString;
          xIndirizzo := TAnagraficaIndirizzo.asString;
          xNumCivico := TAnagraficaNumCivico.asString;
          //xEdificio := TAnagraficaEdificio.asString;
          //xPresso := TAnagraficaPresso.asString;
          xCap := TAnagraficaCap.asString;
          //xFrazione := TAnagraficaFrazione.asString;
          xComune := TAnagraficaComune.asString;
          xProvincia := TAnagraficaProvincia.asString;
          xregione := TAnagraficaRegione.asString;
          xStato := TAnagraficaStato.asString;

          xDomicilioTipoStrada := TAnagraficaDomicilioTipoStrada.asString;
          xDomicilioIndirizzo := TAnagraficaDomicilioIndirizzo.asString;
          xDomicilioNumCivico := TAnagraficaDomicilioNumCivico.asString;
          //xDomicilioEdificio := TAnagraficaDomicilioEdificio.asString;
          //xDomicilioPresso := TAnagraficaPresso.asString;
          xDomicilioCap := TAnagraficaDomicilioCap.asString;
          //xDomicilioFrazione := TAnagraficaDomicilioFrazione.asString;
          xDomicilioComune := TAnagraficaDomicilioComune.asString;
          xDomicilioProvincia := TAnagraficaDomicilioProvincia.asString;
          xdomicilioregione := TAnagraficaDomicilioRegione.asString;
          xDomicilioStato := TAnagraficaDomicilioStato.asString;

          xRecapitiTelefonici := TAnagraficaRecapitiTelefonici.asString;
          xCellulare := TAnagraficaCellulare.asString;
          xTelUfficio := TAnagraficaTelUfficio.asString;
          xEmail := TAnagraficaEmail.asString;
          xEmailUfficio := TAnagraficaEmailUfficio.asString;
          //xCPostale := TAnagraficaCasellaPostale.asString;
          xFax := TAnagraficaFax.asString;
     end;

     //     if CurriculumForm.dsAnagAltriDati.State in [dsInsert,dsEdit] then
     if (CurriculumForm.TAnagAltriDati.Active) and (CurriculumForm.dsAnagAltriDati.state = dsEdit) then begin
          with CurriculumForm.TAnagAltriDati do begin
               Data.DB.BeginTrans;
               try
                    //                    ApplyUpdates;
                    Post;
                    Data.DB.CommitTrans;
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate in AnagAltriDati', mtError, [mbOK], 0);

               end;
               //               CommitUpdates;
          end;
     end;
end;

procedure TData.TEspLavCalcFields(DataSet: TDataSet);
begin
     if TEspLavAziendaNome.Value <> '' then
          TEspLavNomeAzienda.Value := TEspLavAziendaNome.Value
     else TEspLavNomeAzienda.Value := TEspLavAzDenominazione.Value;

     if TEspLavAziendaProvincia.Value <> '' then
          TEspLavProvinciaAzienda.Value := TEspLavAziendaProvincia.Value
     else TEspLavProvinciaAzienda.Value := TEspLavAzProv.Value;

     if TEspLavAziendaComune.Value <> '' then
          TEspLavComuneAzienda.Value := TEspLavAziendaComune.Value
     else TEspLavComuneAzienda.Value := TEspLavAzComune.Value;

     if TEspLavSettore.Value <> '' then begin
          TEspLavsettoreCalc.value := TEspLavSettore.value
     end else begin
          TEspLavsettoreCalc.value := TEspLavAziendaSettore.value;
     end;

end;

procedure TData.DsQGruppiDiplomiStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsQGruppiDiplomi.State in [dsEdit, dsInsert];
     MainForm.BGruppiDipNew.Enabled := not b;
     MainForm.BGruppiDipDel.Enabled := not b;
     MainForm.BGruppiDipOK.Enabled := b;
     MainForm.BGruppiDipCan.Enabled := b;
end;

procedure TData.QGruppiDiplomiAfterPost(DataSet: TDataSet);
begin
     QGruppiDipLK.Close;
     QGruppiDipLK.Open;
end;

procedure TData.DataModuleDestroy(Sender: TObject);
begin
     DB.Close;
     DB.Free;
end;

procedure TData.qEsplavRayBeforeOpen(DataSet: TDataSet);
begin
     qEsplavRay.Parameters[0].Value := TAnagraficaID.Value;
end;

procedure TData.qEsplavRayCalcFields(DataSet: TDataSet);
begin
     if qEspLavRayAziendaNome.Value <> '' then
          qEspLavRayNomeAzienda.Value := qEspLavRayAziendaNome.Value
     else qEspLavRayNomeAzienda.Value := qEspLavRayAzDenominazione.Value;
end;

procedure TData.TCorsiExtraAfterInsert(DataSet: TDataSet);
begin
     Data.TCorsiExtra.FieldByname('IDAnagrafica').Value := Data.TAnagrafica.FieldByName('ID').Value;
end;

procedure TData.TAnagraficaAfterOpen(DataSet: TDataSet);
begin
     Data.qAnagAltreInfo.Open;
     QAnagFoto.Open;
     if data.Global.FieldByName('DebugHrms').AsBoolean = true then data.TAnagrafica.SQL.SaveToFile('Tanagrafica.sql');
     MainForm.FrmLinkwebHome1.QWebLink.Open;
end;

procedure TData.TAnagMansioniCalcFields(DataSet: TDataSet);
begin
     if TAnagMansioniAttuale.value then TAnagMansioniCalc_Attuale.value := 'SI'
     else TAnagMansioniCalc_Attuale.value := '';
     if TAnagMansioniDisponibile.value then TAnagMansioniCalc_Disp.value := 'SI'
     else TAnagMansioniCalc_Disp.value := '';
     if TAnagMansioniAspirazione.value then TAnagMansioniCalc_Asp.value := 'SI'
     else TAnagMansioniCalc_Asp.value := '';
end;

procedure TData.QAnagFotoAfterOpen(DataSet: TDataSet);
begin
     //     fotoform.CBAdattaClick(self)
end;

function TData.CheckVersione(xVer: string): boolean;
begin
     // confronta i primi due indici della versione con quella registrata nel DB
     if not Global.active then Global.open;
     if Global.FieldByName('verH1Sel').Value <> '' then
          Result := pos(Global.FieldByName('verH1Sel').Value, xVer) = 1
     else Result := False;
end;

procedure TData.TEspLavBeforePost(DataSet: TDataSet);
begin
     if qRegioni.Locate('Regione', CurriculumForm.DBComboBox5.Text, []) then
          TEspLavIDRegione.Value := qregioniid.Value;

     if tEspLavIDAnagrafica.Value = 0 then
          TEspLavIDAnagrafica.Value := TAnagraficaID.value;
end;

procedure TData.QProcAutoParamBeforeOpen(DataSet: TDataSet);
begin
     QProcAutoParam.Parameters.ParamByName('ID').value := QprocedureAuto.fieldbyname('ID').value;
end;

procedure TData.QAnagFileBeforeOpen(DataSet: TDataSet);
begin
     if data.Global.FieldByName('anagfileDentrodb').asboolean = true then begin
          {QAnagFile.SQL.Text := 'select * from AnagFile,TipiFileCand' +
               ' where AnagFile.IDTipo *= TipiFileCand.ID ' +
               ' and (docext is not null) ' +
               ' and anagfile.IDAnagrafica=:ID';}
          //modifica query (left join) da Thomas
          QAnagFile.SQL.Text := 'select * from AnagFile left join TipiFileCand' +
               ' on AnagFile.IDTipo = TipiFileCand.ID' +
               ' where (docext is not null)' +
               ' and anagfile.IDAnagrafica=:ID';
     end else begin
          {QAnagFile.SQL.Text := 'select * from AnagFile,TipiFileCand' +
               ' where AnagFile.IDTipo *= TipiFileCand.ID ' +
               ' and anagfile.IDAnagrafica=:ID';  }
          //modifica query (left join) da Thomas
          QAnagFile.SQL.Text := 'select * from AnagFile left join TipiFileCand ' +
               ' on AnagFile.IDTipo = TipiFileCand.ID' +
               ' where anagfile.IDAnagrafica=:ID';
     end;
end;

procedure TData.DSMotiviSospStateChange(Sender: TObject);
var b: boolean;
begin
     b := DSMotiviSosp.State in [dsEdit, dsInsert];
     MainForm.bSalvaMotiviSosp.Enabled := b;
     MainForm.toolbarbutton9767.Enabled := not b;
     MainForm.toolbarbutton9770.Enabled := not b;
end;

procedure TData.DSMotiviRichiamoCandStateChange(Sender: TObject);
var b: boolean;
begin
     b := DSMotiviRichiamoCand.State in [dsEdit, dsInsert];
     MainForm.BSaveMotiviRichiamo.Enabled := b;
     MainForm.toolbarbutton9771.Enabled := not b;
     MainForm.toolbarbutton9772.Enabled := not b;
end;

procedure TData.DSComunitaNaz_StatoStateChange(Sender: TObject);
var b: boolean;
begin
     b := DSComunitaNaz_Stato.State in [dsEdit, dsInsert];
     MainForm.BSalvaComunitazNaz.Enabled := b;
     mainform.BAnnullaComunitazNaz.Enabled := b;

end;

procedure TData.qAnagAltreInfoBeforeOpen(DataSet: TDataSet);
begin
     QComunitaNaz_Stato.Open;
end;

procedure TData.TAnagraficaBeforeCancel(DataSet: TDataSet);
begin
     if QClassificazioni.Locate('ID', TAnagraficaIDClassificazione.AsString, []) then
          MainForm.CBClassPotenziale.ItemIndex := QClassificazioni.RecNo
     else
          MainForm.CBClassPotenziale.ItemIndex := 0;
end;

procedure TData.QAnagNoteBeforePost(DataSet: TDataSet);
begin
  //showmessage('QAnagNoteBeforePost');
end;

end.

