unit StatisticheFrame;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, ADODB, TeEngine, TeeDBEdit, TeeDBCrosstab, Series, ExtCtrls,
     TeeProcs, Chart, DBChart, DBCtrls, StdCtrls, ComCtrls, dxCntner, dxTL,
     dxDBCtrl, dxDBGrid, TeeEdit, Grids, AdvGrid, dxmdaset, Menus, BaseGrid;

type
     TFrmStatistiche = class(TFrame)
          Panel182: TPanel;
          Label127: TLabel;
          Label128: TLabel;
          Label129: TLabel;
          Label131: TLabel;
          Label132: TLabel;
          Label133: TLabel;
          DBText16: TDBText;
          DBText17: TDBText;
          DBText18: TDBText;
          DBText19: TDBText;
          DBText20: TDBText;
          Label134: TLabel;
          DBText21: TDBText;
          Label135: TLabel;
          DBText22: TDBText;
          Label136: TLabel;
          DBText23: TDBText;
          DBText24: TDBText;
          Label144: TLabel;
          Label150: TLabel;
          DBText25: TDBText;
          DBText26: TDBText;
          Label151: TLabel;
          Label152: TLabel;
          DBText27: TDBText;
          Label153: TLabel;
          DBText28: TDBText;
          Panel181: TPanel;
          QCVInseriti: TADOQuery;
          QAzIns: TADOQuery;
          ClientiAtt: TADOQuery;
          FatturatoxAnno: TADOQuery;
          Fatturatoanno: TIntegerField;
          Fatturatonumero: TFloatField;
          NrFatturexAnno: TADOQuery;
          NrFatturenumero: TIntegerField;
          NrFattureanno: TIntegerField;
          QCVTot: TADOQuery;
          QCVTottot: TIntegerField;
          QAzTot: TADOQuery;
          QAzTottot: TIntegerField;
          QAzAttTot: TADOQuery;
          QAzAttTottot: TIntegerField;
          QFattAnnoAttuale: TADOQuery;
          QFatttot: TFloatField;
          QNrFatt: TADOQuery;
          QNrFatttot: TIntegerField;
          DSCVTot: TDataSource;
          DSQAzTot: TDataSource;
          DSQAzAttTot: TDataSource;
          DSFatt: TDataSource;
          DSQNrFatt: TDataSource;
          QFattAnnoScorso: TADOQuery;
          QFattAnnoScorsotot: TFloatField;
          DSFattAS: TDataSource;
          QNRFattAS: TADOQuery;
          QNRFattAStot: TIntegerField;
          DSNrFattAS: TDataSource;
          QNrCollUltimoMese: TADOQuery;
          DSNrColl: TDataSource;
          QNrCollUltimoMeseAS: TADOQuery;
          DSQNrCollAS: TDataSource;
          QNrPresPerMese: TADOQuery;
          DSNrPres: TDataSource;
          QNrPresPerMeseAS: TADOQuery;
          DSNrPressAS: TDataSource;
          QNROff: TADOQuery;
          IntegerField4: TIntegerField;
          DSNROff: TDataSource;
          DSNrOffAS: TDataSource;
          QNrOffAS: TADOQuery;
          IntegerField5: TIntegerField;
          ClientiATTAS: TADOQuery;
          QCVInseritinumero: TIntegerField;
          QCVInseritimese: TWideStringField;
          QCVInseritiAS: TADOQuery;
          IntegerField6: TIntegerField;
          WideStringField1: TWideStringField;
          QAzInsAS: TADOQuery;
          QAndFattSingoloUt: TADOQuery;
          QAndFattSingoloUtAS: TADOQuery;
          QAndFattPUtenteCon: TADOQuery;
          QAndFattPrevistoTot: TADOQuery;
          QTotCVPerAnno: TADOQuery;
          QTotClientiPerAnno: TADOQuery;
          QTotCliAttPerAnno: TADOQuery;
          QNrPresPerMeseASnumero: TIntegerField;
          QNrPresPerMeseASmese: TWideStringField;
          QNrPresPerAnno: TADOQuery;
          QNrPresPerMeseUC: TADOQuery;
          QNrPresPerMeseUCAS: TADOQuery;
          IntegerField1: TIntegerField;
          WideStringField2: TWideStringField;
          QNrPresPerAnnoUC: TADOQuery;
          QNrPresUltimoMese: TADOQuery;
          QNrPresUltimoMesetot: TIntegerField;
          QNrPresUltimoMeseAS: TADOQuery;
          QNrPresUltimoMeseAStot: TIntegerField;
          QNrColloquiAz: TADOQuery;
          QNrColloquiAzAS: TADOQuery;
          IntegerField2: TIntegerField;
          WideStringField3: TWideStringField;
          QNrColloquiAzPerAnno: TADOQuery;
          QNrCollUC: TADOQuery;
          QNrCollUCAS: TADOQuery;
          IntegerField3: TIntegerField;
          WideStringField4: TWideStringField;
          QNrCollUCPerAnno: TADOQuery;
          QCommAperteAz: TADOQuery;
          QCommAperteAZAS: TADOQuery;
          IntegerField7: TIntegerField;
          WideStringField5: TWideStringField;
          QCommAperteAzPerAnno: TADOQuery;
          QCommAperteUC: TADOQuery;
          QCommAperteASUC: TADOQuery;
          IntegerField8: TIntegerField;
          WideStringField6: TWideStringField;
          QCommAperteUCPerAnno: TADOQuery;
          QAndFattPrevistoTotPerAnno: TADOQuery;
          QAndFattPUtenteConPerAnno: TADOQuery;
          QAndFattAz: TADOQuery;
          QAndFattAZAS: TADOQuery;
          QAndFattUCPerAnno: TADOQuery;
          Splitter1: TSplitter;
          PageControl1: TPageControl;
          TabSheet1: TTabSheet;
          TabSheet2: TTabSheet;
          ScrollBox1: TScrollBox;
          DBChartFatt: TDBChart;
          SeriesFatt: TLineSeries;
          DBChartNrFatt: TDBChart;
          Button1: TButton;
          SeriesNrFatt: TLineSeries;
          Panel1: TPanel;
          DBChartCVIns: TDBChart;
          BarSeriesCVInseriti: TBarSeries;
          BarSeriesCVInseritiAS: TBarSeries;
          DBChart4: TDBChart;
          BarSeriesTotCVPerAnno: TBarSeries;
          PanelFattPrevTot: TPanel;
          DBChart3: TDBChart;
          BarSeries10: TBarSeries;
          DBChart5: TDBChart;
          BarSeriesQAndFattPrevPerAnno: TBarSeries;
          Panel3: TPanel;
          DBChartAzIns: TDBChart;
          BarSeriesCliAtt: TBarSeries;
          BarSeriesCliAttaS: TBarSeries;
          DBChart6: TDBChart;
          BarSeriesCliAttPerAnno: TBarSeries;
          Panel4: TPanel;
          DBChartInsCli: TDBChart;
          BarSeries5: TBarSeries;
          BarSeries6: TBarSeries;
          DBChart7: TDBChart;
          BarSeries14: TBarSeries;
          PanelFattPrevUC: TPanel;
          DBChart2: TDBChart;
          BarSeries9: TBarSeries;
          DBChart8: TDBChart;
          BarSeriesFattPrevUCPerAnno: TBarSeries;
          PanelFattTotUtenteConn: TPanel;
          DBChart1: TDBChart;
          BarSeriesAndFattUC: TBarSeries;
          BarSeriesAndFattUCAS: TBarSeries;
          DBChart9: TDBChart;
          BarSeriesFattUCPerAnno: TBarSeries;
          DBChartFattPrev: TDBChart;
          BarSeriesFattAz: TBarSeries;
          BarSeriesFattAzAS: TBarSeries;
          Panel8: TPanel;
          DBChart13: TDBChart;
          BarSeriesPresUCPerMese: TBarSeries;
          SeriesNrPresUCPerMeseAS: TBarSeries;
          DBChart14: TDBChart;
          BarSeriesNrPresPerAnnoUC: TBarSeries;
          Panel9: TPanel;
          DBChart15: TDBChart;
          BarSeriesCommUC: TBarSeries;
          Series1: TBarSeries;
          DBChart16: TDBChart;
          BarSeriesCommApUCPerAnno: TBarSeries;
          Panel10: TPanel;
          DBChart17: TDBChart;
          BarSeriesNrCollUC: TBarSeries;
          BarSeriesNrCollUCAS: TBarSeries;
          DBChart18: TDBChart;
          SeriesCollUCPerAnno: TBarSeries;
          Panel7: TPanel;
          DBChartPres: TDBChart;
          BarSeriesPresPerMese: TBarSeries;
          BarSeriesPresPerMeseAS: TBarSeries;
          DBChart12: TDBChart;
          BarSeriesNrPresPerAnnoAz: TBarSeries;
          Panel11: TPanel;
          DBChart19: TDBChart;
          BarSeriesCollAz: TBarSeries;
          BarSeriesCollAzAS: TBarSeries;
          DBChart20: TDBChart;
          BarSeriesNrColloquiAzPerAnno: TBarSeries;
          Panel12: TPanel;
          DBChart21: TDBChart;
          BarSeriesCommAz: TBarSeries;
          BarSeriesCommAperteAzAS: TBarSeries;
          DBChart22: TDBChart;
          BarSeries35: TBarSeries;
          ChartEditor1: TChartEditor;
          ChartPreviewer1: TChartPreviewer;
          QAppuntamenti: TADOQuery;
          QUtenti: TADOQuery;
          Panel2: TPanel;
          Panel5: TPanel;
          DBLookupComboBox1: TDBLookupComboBox;
          Label1: TLabel;
          QUtentinominativo: TStringField;
          QUtentiid: TAutoIncField;
          DSQUtenti: TDataSource;
          Button2: TButton;
          QInserimenti: TADOQuery;
          QAppuntamentimese: TWideStringField;
          QAppuntamentinrmese: TIntegerField;
          QAppuntamentitot: TIntegerField;
          QInserimentiRic: TADOQuery;
          QPresentazioni: TADOQuery;
          QRicerche: TADOQuery;
          DTDal: TDateTimePicker;
          DTAl: TDateTimePicker;
          Label2: TLabel;
          Label3: TLabel;
          dataGrid: TAdvStringGrid;
          QRicercheChiuse: TADOQuery;
          QRicerchemese: TWideStringField;
          QRicerchenrmese: TIntegerField;
          QRicercheTot: TIntegerField;
          QRicercheChiusemese: TWideStringField;
          QRicercheChiusenrmese: TIntegerField;
          QRicercheChiuseTot: TIntegerField;
          PopupMenu1: TPopupMenu;
          Esporta1: TMenuItem;
          FoglioExcel1: TMenuItem;
          SaveDialog1: TSaveDialog;
          QFattUCPerMese: TADOQuery;
          QFattUCPerMesetot: TFloatField;
          QFattUCPerMesenrmese: TIntegerField;
          QFattUCPerMesemese: TWideStringField;
          Copia1: TMenuItem;
          FormatoExcel1: TMenuItem;
          QOfferte: TADOQuery;
          QOffertemese: TWideStringField;
          QOffertenrmese: TIntegerField;
          QOffertetot: TIntegerField;
          QEventiComm: TADOQuery;
          WideStringField7: TWideStringField;
          IntegerField9: TIntegerField;
          IntegerField10: TIntegerField;
          Label4: TLabel;
          FoglioCSV1: TMenuItem;
          procedure DBChartCVInsClick(Sender: TObject);
          procedure QAppuntamentiBeforeOpen(DataSet: TDataSet);
          procedure Button2Click(Sender: TObject);
          procedure QRicercheBeforeOpen(DataSet: TDataSet);
          procedure QInserimentiBeforeOpen(DataSet: TDataSet);
          procedure FoglioExcel1Click(Sender: TObject);
          procedure FormatoExcel1Click(Sender: TObject);
          procedure FoglioCSV1Click(Sender: TObject);
     private
          function GetMonth(m: BYTE): string;
          function CalcolaSomma(q: TADOQuery; campo: string): INTEGER;
          procedure ShowSaveDlg(filtro: string);
          { Private declarations }
     public
          procedure LookOutButton1Click(Sender: TObject);
          procedure RefreshQueries;
          procedure CloseQueries;
          procedure CheckRights;
          { Public declarations }
     end;

implementation

uses Main, ModuloDati;

{$R *.DFM}

procedure TFrmStatistiche.LookOutButton1Click(Sender: TObject);
begin
end;

function TFrmStatistiche.GetMonth(m: BYTE): string;
begin
     case m of
          1: Result := 'Gennaio';
          2: Result := 'Febbraio';
          3: Result := 'Marzo';
          4: Result := 'Aprile';
          5: Result := 'Maggio';
          6: Result := 'Giugno';
          7: Result := 'Luglio';
          8: Result := 'Agosto';
          9: Result := 'Settembre';
          10: Result := 'Ottobre';
          11: Result := 'Novembre';
          12: Result := 'Dicembre';
     end;
end;

function TFrmStatistiche.CalcolaSomma(q: TADOQuery; campo: string): INTEGER;
begin
     Result := 0;
     if q <> nil then
          while not q.EoF do
          begin
               Result := Result + QCVInseriti.FieldByName(campo).AsInteger;
               q.Next;
          end;
end;

procedure TFrmStatistiche.RefreshQueries;
begin
     //CV Inseriti
     QCVInseriti.Close;
     QCVInseritiAS.Close;
     QCVTot.Close;
     QTotCVPerAnno.Close;
     QCVInseriti.Open;
     QCVInseritiAS.Open;
     QCVTot.Open;
     QTotCVPerAnno.Open;
     //Aziende inserite
     QAzIns.Close;
     QAzINsAS.Close;
     QAzTot.Close;
     QTotClientiPerAnno.Close;
     QAzINs.Open;
     QAzINsAs.open;
     QAzTot.Open;
     QTotClientiPerAnno.Open;
     //Aziende Attive
     QAzAttTot.Close;
     ClientiAtt.Close;
     ClientiAttAS.Close;
     QTotCliAttPerAnno.Close;
     ClientiAtt.Open;
     ClientiAttAS.Open;
     QTotCliAttPerAnno.Open;
     QAzAttTot.Open;
     //Fatturato & nr fatture per anno
     QAndFattAZ.Close;
     QAndFattAZAS.Close;
     if MainForm.xReportMasterFattAz then
     begin
          QAndFattAZ.Open;
          QAndFattAZAS.Open;
     end;
     NrFatturexanno.Close;
     Fatturatoxanno.Close;
     if MainForm.xReportMasterFattAz then NrFatturexanno.Open;
     if MainForm.xReportMasterFattAz then Fatturatoxanno.Open;
     //Colloqui Azienda
     QNrColloquiAz.Close;
     QNrColloquiAz.Open;
     QNrColloquiAzAS.Close;
     QNrColloquiAzAS.Open;
     QNrColloquiAzPerAnno.Close;
     QNrColloquiAzPerAnno.Open;
     //Colloqui Utente Connesso
     QNrCollUC.Close;
     QNrCollUC.Parameters.Items[0].Value := MainForm.xIDUtenteAttuale;
     QNrCollUC.Open;
     QNrCollUCAS.Close;
     QNrCollUCAS.Parameters.Items[0].Value := MainForm.xIDUtenteAttuale;
     QNrCollUCAS.OPen;
     QNrCollUCPerAnno.Close;
     QNrCollUCPerAnno.Parameters.Items[0].Value := MainForm.xIDUtenteAttuale;
     QNrCollUCPerAnno.OPen;
     //Offerte Azienda
     QNROff.Close;
     QNROff.Open;
     QNrOffAS.Close;
     QNrOffAS.Open;
     //Nr Presentzioni azienda
     QNrPresUltimoMese.Close;
     QNrPresUltimoMese.Open;
     QNrPresUltimoMeseAS.Close;
     QNrPresUltimoMeseAS.Open;
     QNrPresPerAnno.Close;
     QNrPresPerAnno.Open;
     QNrPresPerMese.Close;
     QNrPresPerMese.Open;
     QNrPresPerMeseAS.Close;
     QNrPresPerMeseAS.Open;
     //Presentazioni per utente connesso
     QNrPresPerMeseUC.Close;
     QNrPresPerMeseUC.Parameters.Items[0].Value := MainForm.xIDUtenteAttuale;
     QNrPresPerMeseUC.Open;
     QNrPresPerMeseUCAS.Close;
     QNrPresPerMeseUCAS.Parameters.Items[0].Value := MainForm.xIDUtenteAttuale;
     QNrPresPerMeseUCAS.Open;
     QNrPresPerAnnoUC.Close;
     QNrPresPerAnnoUC.Parameters.Items[0].Value := MainForm.xIDUtenteAttuale;
     QNrPresPerAnnoUC.Open;
     //Commesse Azienda
     QCommAperteAz.Close;
     QCommAperteAz.Open;
     QCommAperteAZAS.Close;
     QCommAperteAZAS.Open;
     QCommAperteAzPerAnno.Close;
     QCommAperteAzPerAnno.Open;
     //Commese Utente Connesso
     QCommAperteUC.Close;
     QCommAperteUC.Parameters.Items[0].Value := MainForm.xIDUtenteAttuale;
     QCommAperteUC.Open;
     QCommAperteASUC.Close;
     QCommAperteASUC.Parameters.Items[0].Value := MainForm.xIDUtenteAttuale;
     QCommAperteASUC.Open;
     QCommAperteUCPerAnno.Close;
     QCommAperteUCPerAnno.Parameters.Items[0].Value := MainForm.xIDUtenteAttuale;
     QCommAperteUCPerAnno.Open;
     //Fatturato Utente Connesso
     QAndFattSingoloUtAS.Close;
     QAndFattSingoloUtAS.Parameters.Items[0].Value := MainForm.xIDUtenteAttuale;
     QAndFattSingoloUtAS.Open;
     QAndFattSingoloUt.Close;
     QAndFattSingoloUt.Parameters.Items[0].Value := MainForm.xIDUtenteAttuale;
     QAndFattSingoloUt.Open;
     QAndFattPUtenteCon.Close;
     QAndFattPUtenteCon.Parameters.Items[0].Value := MainForm.xIDUtenteAttuale;
     QAndFattPUtenteCon.Open;
     QAndFattUCPerAnno.Close;
     QAndFattUCPerAnno.Parameters.Items[0].Value := MainForm.xIDUtenteAttuale;
     QAndFattUCPerAnno.Open;
     QAndFattPrevistoTot.CLose;
     QAndFattPrevistoTotPerAnno.Close;
     if MainForm.xReportMasterFattAz then
     begin
          QAndFattPrevistoTot.Open;
          QAndFattPrevistoTotPerAnno.Open;
     end;
     QAndFattPUtenteConPerAnno.Close;
     QAndFattPUtenteConPerAnno.Parameters.Items[0].Value := MainForm.xIDUtenteAttuale;
     QAndFattPUtenteConPerAnno.Open;
     //Dati riepilogativi
     QFattAnnoAttuale.Close;
     QFattAnnoAttuale.Open;
     QFattAnnoScorso.Close;
     QFattAnnoScorso.Open;
     QNrFatt.Close;
     QNrFatt.Open;
     QNRFattAS.Close;
     QNRFattAS.Open;
     QNrCollUltimoMese.Close;
     QNrCollUltimoMese.Open;
     QNrCollUltimoMeseAS.Close;
     QNrCollUltimoMeseAS.Open;
     QUtenti.Open;
end;

procedure TFrmStatistiche.CheckRights;
begin
     //fatturato Azienda
     PanelFattPrevTot.Visible := MainForm.xReportMasterFattAz;
     DBChartFattPrev.Visible := MainForm.xReportMasterFattAz;
     DBChartFatt.Visible := MainForm.xReportMasterFattAz;
     DBChartNrFatt.Visible := MainForm.xReportMasterFattAz;
     Label132.Visible := MainForm.xReportMasterFattAz;
     Label133.Visible := MainForm.xReportMasterFattAz;
     Label134.Visible := MainForm.xReportMasterFattAz;
     Label135.Visible := MainForm.xReportMasterFattAz;
     DBText19.Visible := MainForm.xReportMasterFattAz;
     DBText20.Visible := MainForm.xReportMasterFattAz;
     DBText21.Visible := MainForm.xReportMasterFattAz;
     DBText22.Visible := MainForm.xReportMasterFattAz;
     //Fatturato Proprio
     PanelFattTotUtenteConn.Visible := MainForm.xReportMasterFattProprio;
     PanelFattPrevUC.Visible := MainForm.xReportMasterFattProprio;
end;

procedure TFrmStatistiche.DBChartCVInsClick(Sender: TObject);
begin
     ChartEditor1.Chart := TDBChart(Sender);
     ChartEditor1.Execute;
end;

procedure TFrmStatistiche.QAppuntamentiBeforeOpen(DataSet: TDataSet);
begin
     //Id
     TADOQuery(Dataset).Parameters.ParamByName('id1').Value := QUtenti.FIeldByname('id').asInteger;
     TADOQuery(Dataset).Parameters.ParamByName('id2').Value := QUtenti.FIeldByname('id').asInteger;
     //Data dal
     TADOQuery(Dataset).Parameters.ParamByName('Dal').Value := FormatDateTime('dd/mm/yyyy', DTDal.Date);
     TADOQuery(Dataset).Parameters.ParamByName('Dal1').Value := FormatDateTime('dd/mm/yyyy', DTDal.Date);
     TADOQuery(Dataset).Parameters.ParamByName('Dal2').Value := FormatDateTime('dd/mm/yyyy', DTDal.Date);
     //...al
     TADOQuery(Dataset).Parameters.ParamByName('al').Value := FormatDateTime('dd/mm/yyyy', DTal.Date);
     TADOQuery(Dataset).Parameters.ParamByName('al1').Value := FormatDateTime('dd/mm/yyyy', DTal.Date);
     TADOQuery(Dataset).Parameters.ParamByName('al2').Value := FormatDateTime('dd/mm/yyyy', DTal.Date);
end;

procedure TFrmStatistiche.Button2Click(Sender: TObject);

     procedure InserisciRiga(q: TADOQUery; riga: INTEGER; suf: string);
     var
          lauf: WORD;
          tot: REal;
     begin
          //Inizializza riga
          if riga + 1 > dataGrid.RowCount then datagrid.RowCount := riga + 1;
          for lauf := 1 to 13 do dataGrid.Cells[lauf, riga] := '0';
          q.first;
          tot := 0;
          while not q.eof do
          begin
               tot := tot + q.FieldByName('tot').value;
               dataGrid.Cells[q.FieldByName('nrmese').AsInteger, riga] := q.FieldByName('tot').AsString + suf;
               q.next;
          end;
          case q.FieldByName('tot').DataType of
               ftInteger: dataGrid.Cells[13, riga] := IntToStr(Round(tot)) + suf;
               ftFloat: dataGrid.Cells[13, riga] := FloatToStr(tot) + suf;
          end;
     end;

begin
     if FileExists(Data.Global.FieldByName('DirFileDoc').AsString + '\ModAnalisiUtenti.xls') then
     begin
          Screen.Cursor := crHourGlass;
          //    dataGrid.LoadFromCSV (Data.Global.FieldByName('DirFileDoc').AsString + '\ModAnalisiUtenti.csv');
          dataGrid.Cells[0, 0] := 'Descrizione';
          dataGrid.Cells[1, 0] := 'Gennaio';
          dataGrid.Cells[2, 0] := 'Febbraio';
          dataGrid.Cells[3, 0] := 'Marzo';
          dataGrid.Cells[4, 0] := 'Aprile';
          dataGrid.Cells[5, 0] := 'Maggio';
          dataGrid.Cells[6, 0] := 'Giugno';
          dataGrid.Cells[7, 0] := 'Luglio';
          dataGrid.Cells[8, 0] := 'Agosto';
          dataGrid.Cells[9, 0] := 'Settembre';
          dataGrid.Cells[10, 0] := 'Ottobre';
          dataGrid.Cells[11, 0] := 'Novembre';
          dataGrid.Cells[12, 0] := 'Dicembre';
          dataGrid.Cells[13, 0] := 'Totale';
          QAppuntamenti.Close;
          QInserimenti.Close;
          QInserimentiRic.Close;
          QPresentazioni.Close;
          QRicerche.Close;
          QRicercheChiuse.Close;
          QFattUCPerMese.CLose;
          QOfferte.Close;
          QEventiComm.Close;
          //DA FARE!
          //  QRicerche_DaFinire.Close;
          QAppuntamenti.Open;
          QInserimenti.Open;
          QInserimentiRic.Open;
          QPresentazioni.Open;
          QRicerche.Open;
          QRicercheChiuse.Open;
          QFattUCPerMese.Open;
          QOfferte.Open;
          QEventiComm.Open;
          datagrid.FixedCols := 1;
          datagrid.RowCount := 2;
          datagrid.ColCount := 14;
          datagrid.FixedColWidth := 160;
          dataGrid.FixedRows := 1;
          dataGrid.Cells[0, 1] := 'Interviste a candidati';
          InserisciRiga(qappuntamenti, 1, '');
          dataGrid.Cells[0, 2] := 'Inserimento candidati in H1';
          InserisciRiga(Qinserimenti, 2, '');
          dataGrid.Cells[0, 3] := 'Candidati associati a commesse';
          InserisciRiga(QinserimentiRic, 3, '');
          dataGrid.Cells[0, 4] := 'Candidato Incontra Cliente';
          //Vuota
          dataGrid.Cells[0, 5] := 'Presentazioni segnate in H1';
          InserisciRiga(qPresentazioni, 5, '');
          dataGrid.Cells[0, 6] := 'Nuove Commesse';
          InserisciRiga(qricerche, 6, '');
          dataGrid.Cells[0, 7] := 'Visite Cliente';
          dataGrid.Cells[0, 8] := 'Commesse Chiuse';
          InserisciRiga(qricerchechiuse, 8, '');
          dataGrid.Cells[0, 9] := 'Eventi Sociali';
          dataGrid.Cells[0, 10] := 'Fatturato Utente';
          InserisciRiga(QFattUCPerMese, 10, ' �');
          dataGrid.Cells[0, 11] := 'Eventi in Commessa';
          InserisciRiga(QEventiComm, 11, '');
          dataGrid.Cells[0, 12] := 'Offerte Presentate';
          InserisciRiga(QOfferte, 12, '');
          dataGrid.Cells[0, 13] := 'Dal ' + DateToStr(DTDal.Date) + ' Al ' + DateToStr(DTal.Date);
          QAppuntamenti.Close;
          QInserimenti.Close;
          QInserimentiRic.Close;
          QPresentazioni.Close;
          QRicerche.Close;
          QRicercheChiuse.Close;
          QFattUCPerMese.CLose;
          QOfferte.Close;
          Qeventicomm.close;
          Screen.Cursor := crDefault;
     end
     else ShowMessage('Modello "ModAnalisiUtente.xls" non trovato! Verificarne l''esistenza e/o le impostazioni dei percorsi.');
end;

procedure TFrmStatistiche.QRicercheBeforeOpen(DataSet: TDataSet);
begin
     TADOQuery(Dataset).Parameters.ParamByName('Dal').Value := FormatDateTime('dd/mm/yyyy', DTDal.Date);
     TADOQuery(Dataset).Parameters.ParamByName('al').Value := FormatDateTime('dd/mm/yyyy', DTal.Date);
end;

procedure TFrmStatistiche.QInserimentiBeforeOpen(DataSet: TDataSet);
begin
     //Data dal
     TADOQuery(Dataset).Parameters.ParamByName('Dal').Value := FormatDateTime('dd/mm/yyyy', DTDal.Date);
     //...al
     TADOQuery(Dataset).Parameters.ParamByName('al').Value := FormatDateTime('dd/mm/yyyy', DTal.Date);
end;

procedure TFrmStatistiche.FoglioExcel1Click(Sender: TObject);
begin
     ShowSaveDlg('Foglio Elettronico|*.xls');
end;

procedure TFrmStatistiche.ShowSaveDlg(filtro: string);
var
     pos: BYTE;
begin
     SaveDialog1.Filter := filtro;
     if System.pos('csv', filtro) > 0 then pos := 0
     else pos := 1;
     if SaveDialog1.Execute then
          case pos of
               0: datagrid.SaveToCSV(SaveDialog1.Filename);
               1: dataGrid.SaveToXLS(SaveDialog1.FileName);
          end;
end;

procedure TFrmStatistiche.FormatoExcel1Click(Sender: TObject);
begin
     dataGrid.ExcelClipboardFormat := TRUE;
     datagrid.CopyToClipBoard;
end;

procedure TFrmStatistiche.CloseQueries;
begin
     //CV Inseriti
     QCVInseriti.Close;
     QCVInseritiAS.Close;
     QCVTot.Close;
     QTotCVPerAnno.Close;
     //Aziende inserite
     QAzIns.Close;
     QAzINsAS.Close;
     QAzTot.Close;
     QTotClientiPerAnno.Close;
     //Aziende Attive
     QAzAttTot.Close;
     ClientiAtt.Close;
     ClientiAttAS.Close;
     QTotCliAttPerAnno.Close;
     //Fatturato & nr fatture per anno
     QAndFattAZ.Close;
     QAndFattAZAS.Close;
     NrFatturexanno.Close;
     Fatturatoxanno.Close;
     //Colloqui Azienda
     QNrColloquiAz.Close;
     QNrColloquiAzAS.Close;
     QNrColloquiAzPerAnno.Close;
     //Colloqui Utente Connesso
     QNrCollUC.Close;
     QNrCollUCAS.Close;
     QNrCollUCPerAnno.Close;
     //Offerte Azienda
     QNROff.Close;
     QNrOffAS.Close;
     //Nr Presentzioni azienda
     QNrPresUltimoMese.Close;
     QNrPresUltimoMeseAS.Close;
     QNrPresPerAnno.Close;
     QNrPresPerMese.Close;
     QNrPresPerMeseAS.Close;
     //Presentazioni per utente connesso
     QNrPresPerMeseUC.Close;
     QNrPresPerMeseUCAS.Close;
     QNrPresPerAnnoUC.Close;
     //Commesse Azienda
     QCommAperteAz.Close;
     QCommAperteAZAS.Close;
     QCommAperteAzPerAnno.Close;
     //Commese Utente Connesso
     QCommAperteUC.Close;
     QCommAperteASUC.Close;
     QCommAperteUCPerAnno.Close;
     //Fatturato Utente Connesso
     QAndFattSingoloUtAS.Close;
     QAndFattSingoloUt.Close;
     QAndFattPUtenteCon.Close;
     QAndFattUCPerAnno.Close;
     QAndFattPrevistoTot.CLose;
     QAndFattPrevistoTotPerAnno.Close;
     QAndFattPUtenteConPerAnno.Close;
     //Dati riepilogativi
     QFattAnnoAttuale.Close;
     QFattAnnoScorso.Close;
     QNrFatt.Close;
     QNRFattAS.Close;
     QNrCollUltimoMese.Close;
     QNrCollUltimoMeseAS.Close;
end;

procedure TFrmStatistiche.FoglioCSV1Click(Sender: TObject);
begin
     ShowSaveDlg('Foglio CSV|*.csv');
end;

end.

