�
 TINSDETTFATTFORM 0�q  TPF0TInsDettFattFormInsDettFattFormLeft�TopxBorderStylebsDialogCaptionInserimento dettaglio fatturaClientHeight ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TPanelPanel3LeftTopWidth6Height+
BevelOuter	bvLoweredEnabledTabOrder  TLabelLabel1LeftTopWidth HeightCaptionClienteFocusControlDBEdit1  TDBEditDBEdit1LeftTopWidth,HeightColorclAqua	DataFieldDescrizione
DataSourceData2.dsEBCClientiTabOrder    TPageControlPCLeftTop2Width�Height�
ActivePageTSAttCommessaTabOrder 	TTabSheet
TSCompensiCaptionCompensi TToolbarButton97BitBtn4LeftZTopWidthYHeight)Caption	InserisciFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333�3333�39�3330 373337w39�3330 3��337w9���33337ww333?9���333 7wws333w39�3333 373333w39�3333337s3333?3333333 3333�33w333<333 3337�33w333<�3333����?�<����Ù37wwwww�<����Ù37wwwwsw3333<�3333337s33�333<330 3337337w3333330 3333337w3333333333333333	NumGlyphs
ParentFontOnClickBitBtn4Click  TDBGridDBGrid2_oldLeftTopWidthOHeightq
DataSourceDsQCompensiOptionsdgTitlesdgIndicatordgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgConfirmDeletedgCancelOnExitdgMultiSelect TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameTipoWidth� Visible	 Expanded	FieldNameImportoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold Title.Font.CharsetDEFAULT_CHARSETTitle.Font.ColorclWindowTextTitle.Font.Height�Title.Font.NameMS Sans SerifTitle.Font.StylefsBold WidthEVisible	 Expanded	FieldName
RifRicercaTitle.CaptionRic.n�Width'Visible	 Expanded	FieldNameNoteWidth� Visible	 Expanded	FieldName	RimbSpeseVisible	    TRadioGroupRGOpzLeftTop� Width!HeightGCaptionOpzione inserimento	ItemIndex Items.Strings%non far riferimento ad alcun soggetto'far riferimento ad un soggetto inserito/far riferimento a uno o pi� soggetti in ricerca TabOrderOnClick
RGOpzClick  TPanelPanel2LeftTopWidthPHeight	AlignmenttaLeftJustifyCaptionD  Compensi non ancora fatturati al cliente (in ordine di n� ricerca)ColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TPageControl
PCSoggettiLeftTop� Width
Height� 
ActivePageTSSoggInseritiTabOrderVisible 	TTabSheetTSSoggInseritiCaptionTSSoggInseriti TPanelPanel1LeftTopWidth�Height� 
BevelOuter	bvLoweredCaptionPanel1TabOrder  TPanelPanel4LeftTopWidth�HeightAlignalTop	AlignmenttaLeftJustifyCaptionP  Inserimenti presso il cliente (e non ancora fatturati) per la ricerca relativaColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   TDBGridDBGrid1LeftTopWidth�HeightAlignalClient
DataSourceDsQInsNonFattTabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameCognomeWidth}Visible	 Expanded	FieldNameNomeWidthwVisible	 Expanded	FieldNameRuoloWidth� Visible	      	TTabSheetTSSoggCandRicCaptionTSSoggCandRic
ImageIndex TPanelPanel6Left Top WidthHeightAlignalTop	AlignmenttaLeftJustifyCaptionR  Candidati associati alle ricerche del cliente (non esclusi) in ordine alfabeticoColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   TAdvStringGridASG1Left TopWidthHeight� Cursor	crDefaultAlignalClientColCountDefaultRowHeight	FixedCols OptionsgoFixedVertLinegoFixedHorzLine
goVertLine
goHorzLinegoRangeSelectgoColSizing	goEditing 
ScrollBars
ssVerticalTabOrder	HintColorclYellowActiveCellFont.CharsetDEFAULT_CHARSETActiveCellFont.ColorclWindowTextActiveCellFont.Height�ActiveCellFont.NameTahomaActiveCellFont.StylefsBold CellNode.NodeTypecnFlat'ControlLook.DropDownHeader.Font.CharsetDEFAULT_CHARSET%ControlLook.DropDownHeader.Font.ColorclWindowText&ControlLook.DropDownHeader.Font.Height�$ControlLook.DropDownHeader.Font.NameTahoma%ControlLook.DropDownHeader.Font.Style "ControlLook.DropDownHeader.Visible	"ControlLook.DropDownHeader.Buttons 'ControlLook.DropDownFooter.Font.CharsetDEFAULT_CHARSET%ControlLook.DropDownFooter.Font.ColorclWindowText&ControlLook.DropDownFooter.Font.Height�$ControlLook.DropDownFooter.Font.NameMS Sans Serif%ControlLook.DropDownFooter.Font.Style "ControlLook.DropDownFooter.Visible	"ControlLook.DropDownFooter.Buttons EnhRowColMoveFilter FilterDropDown.Font.CharsetDEFAULT_CHARSETFilterDropDown.Font.ColorclWindowTextFilterDropDown.Font.Height�FilterDropDown.Font.NameMS Sans SerifFilterDropDown.Font.Style FilterDropDownClear(All)FixedColWidth�FixedRowHeightFixedFont.CharsetDEFAULT_CHARSETFixedFont.ColorclWindowTextFixedFont.Height�FixedFont.NameMS Sans SerifFixedFont.Style FloatFormat%.2fMouseActions.DirectEdit	PrintSettings.DateFormat
dd/mm/yyyyPrintSettings.Font.CharsetDEFAULT_CHARSETPrintSettings.Font.ColorclWindowTextPrintSettings.Font.Height�PrintSettings.Font.NameMS Sans SerifPrintSettings.Font.Style PrintSettings.FixedFont.CharsetDEFAULT_CHARSETPrintSettings.FixedFont.ColorclWindowTextPrintSettings.FixedFont.Height�PrintSettings.FixedFont.NameMS Sans SerifPrintSettings.FixedFont.Style  PrintSettings.HeaderFont.CharsetDEFAULT_CHARSETPrintSettings.HeaderFont.ColorclWindowTextPrintSettings.HeaderFont.Height�PrintSettings.HeaderFont.NameMS Sans SerifPrintSettings.HeaderFont.Style  PrintSettings.FooterFont.CharsetDEFAULT_CHARSETPrintSettings.FooterFont.ColorclWindowTextPrintSettings.FooterFont.Height�PrintSettings.FooterFont.NameMS Sans SerifPrintSettings.FooterFont.Style PrintSettings.Borders
pbNoborderPrintSettings.CenteredPrintSettings.PagePrefixpagePrintSettings.PageNumSep/ScrollWidthSearchFooter.FindNextCaption
Find &nextSearchFooter.FindPrevCaptionFind &previousSearchFooter.Font.CharsetDEFAULT_CHARSETSearchFooter.Font.ColorclWindowTextSearchFooter.Font.Height�SearchFooter.Font.NameMS Sans SerifSearchFooter.Font.Style SearchFooter.HighLightCaption	HighlightSearchFooter.HintCloseCloseSearchFooter.HintFindNextFind next occurenceSearchFooter.HintFindPrevFind previous occurenceSearchFooter.HintHighlightHighlight occurencesSearchFooter.MatchCaseCaption
Match caseSelectionColorclHighlightSelectionTextColorclHighlightTextURLColorclBlack
VAlignment	vtaCenterVersion5.0.0.3WordWrap	ColWidths�     	TdxDBGridDBGrid2LeftTopWidthPHeightqBands  DefaultLayout	HeaderPanelRowCountKeyFieldIDSummaryGroups SummarySeparator, TabOrder
DataSourceDsQCompensiFilter.Active	Filter.Criteria
       OptionsBehavioredgoAutoSortedgoDragScrolledgoEnterShowEditoredgoImmediateEditoredgoMultiSelectedgoTabThroughedgoVertThrough 	OptionsDBedgoCancelOnExitedgoCanDeleteedgoCanInsertedgoCanNavigationedgoConfirmDeleteedgoLoadAllRecordsedgoUseBookmarks OptionsViewedgoAutoWidthedgoBandHeaderWidthedgoIndicatoredgoUseBitmap  TdxDBGridMaskColumnDBGrid2TipoWidth� 	BandIndex RowIndex 	FieldNameTipo  TdxDBGridColumnDBGrid2Column9Width� 	BandIndex RowIndex 	FieldNameDescrizione  TdxDBGridMaskColumnDBGrid2Importo	BandIndex RowIndex 	FieldNameImporto  TdxDBGridMaskColumnDBGrid2IDFatturaVisibleWidth	BandIndex RowIndex 	FieldName	IDFattura  TdxDBGridMaskColumnDBGrid2RifRicercaCaptionRif.commessaWidth?	BandIndex RowIndex 	FieldName
RifRicerca  TdxDBGridMaskColumnDBGrid2IDRicercaVisibleWidth	BandIndex RowIndex 	FieldName	IDRicerca  TdxDBGridMaskColumnDBGrid2NoteWidthV	BandIndex RowIndex 	FieldNameNote  TdxDBGridMaskColumn	DBGrid2IDVisibleWidth	BandIndex RowIndex 	FieldNameID  TdxDBGridCheckColumnDBGrid2RimbSpeseCaption
Rimb.speseWidth3	BandIndex RowIndex 	FieldName	RimbSpeseValueCheckedTrueValueUncheckedFalse    	TTabSheetTSNoteSpeseCaption
Note Spese
ImageIndex TToolbarButton97BNotaSpeseInsLeftYTopWidthYHeight)Caption	InserisciFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333�3333�39�3330 373337w39�3330 3��337w9���33337ww333?9���333 7wws333w39�3333 373333w39�3333337s3333?3333333 3333�33w333<333 3337�33w333<�3333����?�<����Ù37wwwww�<����Ù37wwwwsw3333<�3333337s33�333<330 3337337w3333330 3333337w3333333333333333	NumGlyphs
ParentFontOnClickBNotaSpeseInsClick  TPanelPanel5LeftTopWidthPHeight	AlignmenttaLeftJustifyCaption@  Note spese non ancora fatturate al cliente (in ordine di data)ColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   	TdxDBGrid	dxDBGrid1LeftTopWidthPHeight�Bands  DefaultLayout	HeaderPanelRowCountKeyFieldIDSummaryGroups SummarySeparator, TabOrder
DataSourceDsQNoteSpeseCliFilter.Active	Filter.Criteria
       OptionsBehavioredgoAutoSortedgoDragScrolledgoEnterShowEditoredgoImmediateEditoredgoTabThroughedgoVertThrough 	OptionsDBedgoCancelOnExitedgoCanDeleteedgoCanInsertedgoCanNavigationedgoConfirmDeleteedgoLoadAllRecordsedgoUseBookmarks OptionsViewedgoAutoWidthedgoBandHeaderWidthedgoIndicatoredgoUseBitmap  TdxDBGridMaskColumndxDBGrid1IDVisibleWidthN	BandIndex RowIndex 	FieldNameID  TdxDBGridDateColumndxDBGrid1DataWidth[	BandIndex RowIndex 	FieldNameData  TdxDBGridMaskColumndxDBGrid1DescrizioneWidth� 	BandIndex RowIndex 	FieldNameDescrizione  TdxDBGridMaskColumndxDBGrid1AutomobileVisibleWidthN	BandIndex RowIndex 	FieldName
Automobile  TdxDBGridMaskColumndxDBGrid1AutostradaVisibleWidthN	BandIndex RowIndex 	FieldName
Autostrada  TdxDBGridMaskColumndxDBGrid1AereotrenoVisibleWidthN	BandIndex RowIndex 	FieldName
Aereotreno  TdxDBGridMaskColumndxDBGrid1TaxiVisibleWidthN	BandIndex RowIndex 	FieldNameTaxi  TdxDBGridMaskColumndxDBGrid1AlbergoVisibleWidthN	BandIndex RowIndex 	FieldNameAlbergo  TdxDBGridMaskColumndxDBGrid1RistoranteBarVisibleWidthU	BandIndex RowIndex 	FieldNameRistoranteBar  TdxDBGridMaskColumndxDBGrid1ParkingVisibleWidthN	BandIndex RowIndex 	FieldNameParking  TdxDBGridMaskColumndxDBGrid1AltroImportoVisibleWidthN	BandIndex RowIndex 	FieldNameAltroImporto  TdxDBGridMaskColumndxDBGrid1RifRicercaCaptionRif.commessaWidth\	BandIndex RowIndex 	FieldName
RifRicerca  TdxDBGridColumndxDBGrid1TotSpeseCaptionTotale speseWidthY	BandIndex RowIndex 	FieldNameTotSpese    	TTabSheet	TSAnnunciCaptionAnnunci
ImageIndex TToolbarButton97BInsAnnuncioLeftYTopWidthYHeight)Caption	InserisciFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333�3333�39�3330 373337w39�3330 3��337w9���33337ww333?9���333 7wws333w39�3333 373333w39�3333337s3333?3333333 3333�33w333<333 3337�33w333<�3333����?�<����Ù37wwwww�<����Ù37wwwwsw3333<�3333337s33�333<330 3337337w3333330 3333337w3333333333333333	NumGlyphs
ParentFontOnClickBInsAnnuncioClick  TToolbarButton97BInsAnnuncio2LeftZTop� WidthYHeight)Caption	InserisciFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333�3333�39�3330 373337w39�3330 3��337w9���33337ww333?9���333 7wws333w39�3333 373333w39�3333337s3333?3333333 3333�33w333<333 3337�33w333<�3333����?�<����Ù37wwwww�<����Ù37wwwwsw3333<�3333337s33�333<330 3337337w3333330 3333337w3333333333333333	NumGlyphs
ParentFontOnClickBInsAnnuncio2Click  TPanelPanel7LeftTopWidthPHeight	AlignmenttaLeftJustifyCaption[  Annunci non ancora fatturati al cliente (in ordine di rif.annuncio) - riferiti a ricercheColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   TDBGridDBGrid4LeftTopWidthPHeight� 
DataSourceDsQAnnunciCliTabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameDataWidthJVisible	 Expanded	FieldNameRifTitle.CaptionRif.annuncioVisible	 Expanded	FieldName	NumModuliTitle.Caption	N� ModuliVisible	 Expanded	FieldName	NumParoleTitle.Caption	N� ParoleVisible	 Expanded	FieldName
TotaleEuroFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold Title.CaptionTotale EuroTitle.Font.CharsetDEFAULT_CHARSETTitle.Font.ColorclWindowTextTitle.Font.Height�Title.Font.NameMS Sans SerifTitle.Font.StylefsBold WidthLVisible	 Expanded	FieldName
RifRicercaTitle.Caption
Rif.ric.n�Visible	    TPanelPanel8LeftTop� WidthPHeight	AlignmenttaLeftJustifyCaption_  Annunci non ancora fatturati al cliente (in ordine di rif.annuncio) - NON riferiti a ricercheColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBGridDBGrid5LeftTop� WidthPHeight� 
DataSourceDsQAnnunciCli2TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameDataWidthJVisible	 Expanded	FieldNameRifTitle.CaptionRif.annuncioVisible	 Expanded	FieldName	NumModuliTitle.Caption	N� ModuliVisible	 Expanded	FieldName	NumParoleTitle.Caption	N� ParoleVisible	 Expanded	FieldName
TotaleEuroFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold Title.CaptionTotale EuroTitle.Font.CharsetDEFAULT_CHARSETTitle.Font.ColorclWindowTextTitle.Font.Height�Title.Font.NameMS Sans SerifTitle.Font.StylefsBold WidthLVisible	     	TTabSheetTSAttCommessaCaptionAttivit� commessa
ImageIndex TToolbarButton97ToolbarButton971LeftZTopWidthYHeight)Caption	InserisciFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333�3333�39�3330 373337w39�3330 3��337w9���33337ww333?9���333 7wws333w39�3333 373333w39�3333337s3333?3333333 3333�33w333<333 3337�33w333<�3333����?�<����Ù37wwwww�<����Ù37wwwwsw3333<�3333337s33�333<330 3337337w3333330 3333337w3333333333333333	NumGlyphs
ParentFontOnClickToolbarButton971Click  TPanelPanel10LeftTopWidthPHeight	AlignmenttaLeftJustifyCaption>  Attivit� singole collegate a commessa e non ancora fatturateColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   	TdxDBGrid	dxDBGrid2LeftTopWidthQHeight�Bands  DefaultLayout	HeaderPanelRowCountKeyFieldIDShowSummaryFooter	SummaryGroups SummarySeparator, TabOrder
DataSourceDsQAttivitaFilter.Active	Filter.Criteria
       OptionsBehavioredgoAutoSortedgoDragScrolledgoEnterShowEditoredgoImmediateEditoredgoMultiSelectedgoTabThroughedgoVertThrough 	OptionsDBedgoCancelOnExitedgoCanDeleteedgoCanInsertedgoCanNavigationedgoConfirmDeleteedgoLoadAllRecordsedgoUseBookmarks OptionsViewedgoAutoWidthedgoBandHeaderWidthedgoIndicatoredgoUseBitmap  TdxDBGridMaskColumndxDBGrid2IDVisibleWidthp	BandIndex RowIndex 	FieldNameID  TdxDBGridMaskColumndxDBGrid2ProgressivoCaptionRif.CommessaWidthH	BandIndex RowIndex 	FieldNameProgressivo  TdxDBGridDateColumndxDBGrid2DataWidthJ	BandIndex RowIndex 	FieldNameData  TdxDBGridMaskColumndxDBGrid2EventoWidthb	BandIndex RowIndex 	FieldNameEvento  TdxDBGridMaskColumndxDBGrid2CandidatoWidth� 	BandIndex RowIndex 	FieldName	Candidato  TdxDBGridMaskColumndxDBGrid2UtenteWidth]	BandIndex RowIndex 	FieldNameUtente  TdxDBGridMaskColumndxDBGrid2MinutiWidth8	BandIndex RowIndex 	FieldNameMinuti  TdxDBGridCurrencyColumndxDBGrid2CostoWidth6	BandIndex RowIndex 	FieldNameCostoSummaryFooterTypecstSumSummaryFooterFieldCostoSummaryFooterFormat� ,0.00;-� ,0.00Nullable     TPanelPanel9LeftTop Width� Height9
BevelOuterbvNoneTabOrder TToolbarButton97BOKLeft\TopWidthYHeight)CaptionEsci
Glyph.Data
z  v  BMv      6  (               @                     ��� � � @�h ��� TQI �? v�v ��� ('' yґ E�^ qm^ ��� $�M ��� _�w \� ��� ��� �ټ \�d =<9 v�� 4�N :�] |{s T�s _^^ ��x ��� ,�X �Ʀ b�s jɃ ��� I�u ��� O�j ��� i|m ��� ��� *�M ��� ��� ��� 421 D�f S�` �� 7�^ JIC 9�U r� |�� _wc nnm U�l k� ��� ��� 0�W J�p ��� f�l `�l wċ %�G � �° T�q ��� e�y ��p ��� V�y qЊ [XN [�y ��� ��� M�n �Ė ��� 2�T urq d�} =�[ A�` ��� ��� ��� ��� ��� ��� F�p ��� H�c �Ǔ ��� m�v n� dЁ N�u C�h x�� V�o *�R �� ��� ?�b ~~{ o�p 2�P ��� �̹ 1�Z ��� H�_ ��� ;�c Ļ� g�o w͎ ��� U�z O�g ��� ]�r r�� ��� ��� \�o ��� c`a 0�V M�p F�f [�w �ȶ ��� 8�Y I�[ ��� ��u ��� sɉ 3�] ��� Y�o ��� ��� 3�Z �á mą i�z @�j Q�u [YY I�m ��� ��� ��� ��� (�Q ��� ?�c Z�~ ��� e� {�z b�s ��� IIH B�a yvu ��� l͆ ��� p�� t�� �ϼ Ŀ� +�V <�X J�k ��� |�� ��� h�~ <�a I�s 11/ ��� ��� E�n O�r k�m ��} ��� _�y e�w ��� ;�d S�y cxf ��� ��� |̑ ɿ� 7�Z =�] e|j ��� ��� Q�l }�� zxp ��� {�� ��� ��� &�K +�O L�a ��� ��� Y�t �Ɵ b�x ��� ��� ��� U�v ��� S�n \�v |Ǐ ��� ��� �˽ ��� ��� VTK ��� �ɼ ƽ� 7�_ A�j B�] ��� W�| ��� ��� ��� .9��		��9.�/Pz��z�/�.�V}��uu���}���4��>?����?�>E_4p�����?ih~�L�����=<N�uy�0�`�h��L�u�%N�s��3yo�n��$��?L���d�<+�3�nnn�`���ŧ��s��l���nn�nףR�yy�Rl@���vB�U­n�HQ�'���y3�R�ґJ#5����Q\�''���3�0�#J���ח�uy�\�''����ӯD��t��G�˥333uޤ'���l�&rF2��wk��O�>��3��㗗�XY�^���{�W"MgR�>>,��-�b1�F��澛�|


M���-���F���!I;�C����|���Z]��А���q��j�cccc�C�;�:����[�A��*SSSS*��;I��%���a�A��������fʜЕ���_�� ����m68���x}=(7��Te�=}x���K�.))ۍK����۱��.۱�OnClickBOKClick   TDataSourceDsQInsNonFattDataSetQInsNonFattLeft� Top�  TDataSourceDsQCompensiDataSet	QCompensiLeft(Top�   TDataSourceDsQNoteSpeseCliDataSetQNoteSpeseCliLeft�Top�  TDataSourceDsQAnnunciCliDataSetQAnnunciCliLeft�Top�  TDataSourceDsQAnnunciCli2DataSetQAnnunciCli2Left7Top�  TADOLinkedQuery	QCompensi
ConnectionData.DB
CursorTypectStatic
BeforeOpenQCompensiBeforeOpen
Parameters SQL.StringsCselect EBC_CompensiRicerche.*, EBC_Ricerche.Progressivo RifRicerca 'from EBC_CompensiRicerche,EBC_Ricerche 5where EBC_CompensiRicerche.IDRicerca=EBC_Ricerche.ID +and EBC_CompensiRicerche.IDFattura is null and EBC_Ricerche.IDCliente=1" order by EBC_Ricerche.Progressivo 	UseFilterLeft'Top�  TAutoIncFieldQCompensiID	FieldNameIDReadOnly	  TIntegerFieldQCompensiIDRicerca	FieldName	IDRicerca  TStringFieldQCompensiTipo	FieldNameTipoSize  TFloatFieldQCompensiImporto	FieldNameImporto  TDateTimeFieldQCompensiDataPrevFatt	FieldNameDataPrevFatt  TIntegerFieldQCompensiIDFattura	FieldName	IDFattura  TStringFieldQCompensiNote	FieldNameNoteSizeP  TIntegerFieldQCompensiIDOffertaDett	FieldNameIDOffertaDett  TBooleanFieldQCompensiRimbSpese	FieldName	RimbSpese  TBooleanFieldQCompensiCosto	FieldNameCosto  TStringFieldQCompensiDescrizione	FieldNameDescrizioneSize�   TStringFieldQCompensiRifRicerca	FieldName
RifRicercaSize   TADOLinkedQueryQInsNonFatt_old
ConnectionData.DB
CursorTypectStatic
ParametersNameidric:
AttributespaSigned DataType	ftInteger	Precision
SizeValue   SQL.StringsGselect EBC_Inserimenti.ID,Cognome,Nome,Sesso,EBC_Inserimenti.DallaData,I          EBC_Ricerche.Progressivo,DataInizio,Mansioni.Descrizione Ruolo,;          EBC_Ricerche.StipendioLordo,EBC_Ricerche.Id idric5from EBC_Inserimenti,Anagrafica,EBC_Ricerche,Mansioni0where EBC_Inserimenti.IDAnagrafica=Anagrafica.ID1    and EBC_Inserimenti.IDRicerca=EBC_Ricerche.ID+    and EBC_Ricerche.IDMansione=Mansioni.ID    and ProgFattura is nulland EBC_Ricerche.Id =:idric: 	UseFilterLeft� Topd TAutoIncFieldQInsNonFatt_oldID	FieldNameIDReadOnly	  TStringFieldQInsNonFatt_oldCognome	FieldNameCognomeSize  TStringFieldQInsNonFatt_oldNome	FieldNameNomeSize  TStringFieldQInsNonFatt_oldSesso	FieldNameSessoSize  TDateTimeFieldQInsNonFatt_oldDallaData	FieldName	DallaData  TStringFieldQInsNonFatt_oldProgressivo	FieldNameProgressivoSize  TDateTimeFieldQInsNonFatt_oldDataInizio	FieldName
DataInizio  TStringFieldQInsNonFatt_oldRuolo	FieldNameRuoloSized  TFloatFieldQInsNonFatt_oldStipendioLordo	FieldNameStipendioLordo  TAutoIncFieldQInsNonFatt_oldidric	FieldNameidricReadOnly	   TADOLinkedTableTInserimentiABS
ConnectionData.DB
CursorTypectStatic	TableNameEBC_InserimentiLeft� TopT  TADOLinkedQueryQ
ConnectionData.DB
Parameters 	UseFilterLeft(Topd  TADOLinkedTableTAnagABS
ConnectionData.DB	TableName
AnagraficaLeftpTop�  TADOLinkedQueryQNoteSpeseCli
ConnectionData.DB
CursorTypectStatic
BeforeOpenQNoteSpeseCliBeforeOpenOnCalcFieldsQNoteSpeseCli_OLDCalcFields
Parameters SQL.StringsBselect RicNoteSpese.ID,RicNoteSpese.Data,RicNoteSpese.Descrizione,"          RicNoteSpese.Automobile,"          RicNoteSpese.Autostrada,"          RicNoteSpese.Aereotreno,          RicNoteSpese.Taxi,          RicNoteSpese.Albergo,%          RicNoteSpese.RistoranteBar,          RicNoteSpese.Parking,$          RicNoteSpese.AltroImporto,/          EBC_Ricerche.Progressivo RifRicerca  from RicNoteSpese,EBC_Ricerche,where RicNoteSpese.IDRicerca=EBC_Ricerche.IDB  and (RicNoteSpese.IDFattura is null or RicNoteSpese.IDFattura=0)order by Data LinkedMasterFieldIDLinkedDetailField	IDCliente	UseFilterLeft�TopM TAutoIncFieldQNoteSpeseCliID	FieldNameIDReadOnly	  TDateTimeFieldQNoteSpeseCliData	FieldNameData  TStringFieldQNoteSpeseCliDescrizione	FieldNameDescrizioneSize2  TFloatFieldQNoteSpeseCliAutomobile	FieldName
Automobile  TFloatFieldQNoteSpeseCliAutostrada	FieldName
Autostrada  TFloatFieldQNoteSpeseCliAereotreno	FieldName
Aereotreno  TFloatFieldQNoteSpeseCliTaxi	FieldNameTaxi  TFloatFieldQNoteSpeseCliAlbergo	FieldNameAlbergo  TFloatFieldQNoteSpeseCliRistoranteBar	FieldNameRistoranteBar  TFloatFieldQNoteSpeseCliParking	FieldNameParking  TFloatFieldQNoteSpeseCliAltroImporto	FieldNameAltroImporto  TStringFieldQNoteSpeseCliRifRicerca	FieldName
RifRicercaSize  TFloatFieldQNoteSpeseCliTotSpese	FieldKindfkCalculated	FieldNameTotSpese
Calculated	   TADOLinkedQueryQAnnunciCli
ConnectionData.DB
CursorTypectStatic
BeforeOpenQAnnunciCliBeforeOpen
Parameters SQL.Stringsselect Ann_annunci.Data,          Ann_annunci.Rif,          Ann_annunci.ID,           Ann_annunci.NumModuli,           Ann_annunci.NumParole,!          Ann_annunci.TotaleLire,!          Ann_annunci.TotaleEuro,-          EBC_Ricerche.Progressivo RifRicerca1from Ann_annunci,Ann_annunciRicerche,EBC_Ricerche3where Ann_annunci.ID=Ann_annunciRicerche.IDAnnuncio3  and Ann_annunciRicerche.IDRicerca=EBC_Ricerche.ID@  and (Ann_annunci.IDFattura is null or Ann_annunci.IDFattura=0)order by Data  LinkedMasterFieldIDLinkedDetailField	IDCliente	UseFilterLeft�TopM  TADOLinkedQueryQAnnunciCli2
ConnectionData.DB
CursorTypectStatic
BeforeOpenQAnnunciCli2BeforeOpen
Parameters SQL.Stringsselect Ann_annunci.Data,          Ann_annunci.Rif,          Ann_annunci.ID,           Ann_annunci.NumModuli,           Ann_annunci.NumParole,!          Ann_annunci.TotaleLire,           Ann_annunci.TotaleEurofrom Ann_annunci@where (Ann_annunci.IDFattura is null or Ann_annunci.IDFattura=0)order by Data LinkedMasterFieldIDLinkedDetailField	IDCliente	UseFilterLeft7TopJ  	TADOQueryQInsNonFatt
ConnectionData.DB
ParametersNameidric
AttributespaSigned DataType	ftInteger	Precision
SizeValue   SQL.StringsGselect EBC_Inserimenti.ID,Cognome,Nome,Sesso,EBC_Inserimenti.DallaData,I          EBC_Ricerche.Progressivo,DataInizio,Mansioni.Descrizione Ruolo,;          EBC_Ricerche.StipendioLordo,EBC_Ricerche.Id idric5from EBC_Inserimenti,Anagrafica,EBC_Ricerche,Mansioni0where EBC_Inserimenti.IDAnagrafica=Anagrafica.ID1    and EBC_Inserimenti.IDRicerca=EBC_Ricerche.ID+    and EBC_Ricerche.IDMansione=Mansioni.ID    and ProgFattura is nulland EBC_Ricerche.Id =:idric Left8Top� TAutoIncFieldQInsNonFattID	FieldNameIDReadOnly	  TStringFieldQInsNonFattCognome	FieldNameCognomeSize  TStringFieldQInsNonFattNome	FieldNameNomeSize  TStringFieldQInsNonFattSesso	FieldNameSessoSize  TDateTimeFieldQInsNonFattDallaData	FieldName	DallaData  TStringFieldQInsNonFattProgressivo	FieldNameProgressivoSize  TDateTimeFieldQInsNonFattDataInizio	FieldName
DataInizio  TStringFieldQInsNonFattRuolo	FieldNameRuoloSized  TFloatFieldQInsNonFattStipendioLordo	FieldNameStipendioLordo  TAutoIncFieldQInsNonFattidric	FieldNameidricReadOnly	   	TADOQueryQCandRicCliente
ConnectionData.DB
BeforeOpenQCandRicClienteBeforeOpen
ParametersNamex
AttributespaSigned
paNullable DataType	ftInteger	Precision
SizeValue  SQL.Strings8select Anagrafica.ID,Anagrafica.Cognome,Anagrafica.Nome,.          EBC_Ricerche.Progressivo RifRicerca,#          EBC_Ricerche.ID IDRicerca2from EBC_CandidatiRicerche,EBC_Ricerche,Anagrafica5where EBC_CandidatiRicerche.IDRicerca=EBC_Ricerche.ID6  and EBC_CandidatiRicerche.IDAnagrafica=Anagrafica.IDP  and (EBC_CandidatiRicerche.Escluso=0 or EBC_CandidatiRicerche.Escluso is null)and EBC_Ricerche.IDCliente =:xorder by Cognome,Nome LeftpTopY TAutoIncFieldQCandRicClienteID	FieldNameIDReadOnly	  TStringFieldQCandRicClienteCognome	FieldNameCognomeSize  TStringFieldQCandRicClienteNome	FieldNameNomeSize  TStringFieldQCandRicClienteRifRicerca	FieldName
RifRicercaSize  TAutoIncFieldQCandRicClienteIDRicerca	FieldName	IDRicercaReadOnly	   	TADOQuery	QFindAnag
ConnectionData.DB
ParametersNamex
AttributespaSigned DataType	ftInteger	Precision
SizeValue  SQL.Stringsselect ID,Cognome,Nome,Sessofrom Anagraficawhere ID = :x Left Top� TAutoIncFieldQFindAnagID	FieldNameIDReadOnly	  TStringFieldQFindAnagCognome	FieldNameCognomeSize  TStringFieldQFindAnagNome	FieldNameNomeSize  TStringFieldQFindAnagSesso	FieldNameSessoSize   	TADOQuery	QAttivita
ConnectionData.DB
BeforeOpenQAttivitaBeforeOpen
Parameters SQL.Strings &select RA.ID, R.Progressivo, RA.Data, +	TE.Evento, A.Cognome+' '+A.Nome Candidato,	U.Nominativo Utente,	RA.Minuti, RA.Costofrom EBC_Ricerche_attivita RA*join EBC_Ricerche R on RA.IDRicerca = R.ID3join TabEventiAttCommessa TE on RA.IDEvento = TE.ID0left join Anagrafica A on RA.IDAnagrafica = A.ID'left join Users U on RA.IDUtente = U.IDwhere RA.IDFattura is nulland IDCliente = 4240order by RA.id desc; Left� Top�  TAutoIncFieldQAttivitaID	FieldNameIDReadOnly	  TStringFieldQAttivitaProgressivo	FieldNameProgressivoSize  TDateTimeFieldQAttivitaData	FieldNameData  TStringFieldQAttivitaEvento	FieldNameEventoSized  TStringFieldQAttivitaCandidato	FieldName	CandidatoReadOnly	Size�   TStringFieldQAttivitaUtente	FieldNameUtenteSize  TSmallintFieldQAttivitaMinuti	FieldNameMinuti  TFloatFieldQAttivitaCosto	FieldNameCosto   TDataSourceDsQAttivitaDataSet	QAttivitaLeft� Top�    