unit EspLavRetrib;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     FrmDatiRetribuz, StdCtrls, Buttons, ExtCtrls, Db, ADODB, U_ADOLinkCl,
     DBTables, TB97;

type
     TEspLavRetribForm = class(TForm)
          Panel1: TPanel;
          DatiRetribuzFrame1: TDatiRetribuzFrame;
          BOK: TToolbarButton97;
          procedure FormShow(Sender: TObject);
          procedure BOKClick(Sender: TObject);
          procedure DatiRetribuzFrame1ToolbarButton971Click(Sender: TObject);
          procedure DatiRetribuzFrame1BModClick(Sender: TObject);
          procedure DatiRetribuzFrame1BStoricoClick(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     EspLavRetribForm: TEspLavRetribForm;

implementation

uses ModuloDati, DettEspLav, Main;
{$R *.DFM}

procedure TEspLavRetribForm.FormShow(Sender: TObject);
begin
     //Grafica
     EspLavRetribForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     DatiRetribuzFrame1.Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     DatiRetribuzFrame1.Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     DatiRetribuzFrame1.Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     DatiRetribuzFrame1.Panel3.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     DatiRetribuzFrame1.Panel3.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     DatiRetribuzFrame1.Panel3.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     DatiRetribuzFrame1.Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     DatiRetribuzFrame1.Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     DatiRetribuzFrame1.Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     DatiRetribuzFrame1.Panel74.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     DatiRetribuzFrame1.Panel74.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     DatiRetribuzFrame1.Panel74.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     DatiRetribuzFrame1.QDatiRetribuz.Open;
     DatiRetribuzFrame1.QTot.Open;
end;

procedure TEspLavRetribForm.BOKClick(Sender: TObject);
begin
     EspLavRetribForm.ModalResult := mrOK;
end;

procedure TEspLavRetribForm.DatiRetribuzFrame1ToolbarButton971Click(
     Sender: TObject);
begin
     DatiRetribuzFrame1.ToolbarButton971Click(Sender);
end;

procedure TEspLavRetribForm.DatiRetribuzFrame1BModClick(Sender: TObject);
begin
     if DatiRetribuzFrame1.QDatiRetribuz.IsEmpty then exit;
     DatiRetribuzFrame1.BModClick(Sender);
end;

procedure TEspLavRetribForm.DatiRetribuzFrame1BStoricoClick(
     Sender: TObject);
begin
     if DatiRetribuzFrame1.QDatiRetribuz.IsEmpty then exit;
     DatiRetribuzFrame1.BStoricoClick(Sender);
end;

end.

