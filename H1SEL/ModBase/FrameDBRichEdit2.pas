unit FrameDBRichEdit2;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, DBTables, ActnList, ImgList, StdCtrls, ComCtrls, DBCtrls, ToolWin,
     ExtCtrls, ColorBox, ColorBtn;

type
     TFrameDBRich = class(TFrame)
          Ruler: TPanel;
          FirstInd: TLabel;
          LeftInd: TLabel;
          RulerLine: TBevel;
          RightInd: TLabel;
          Bevel1: TBevel;
          StatusBar: TStatusBar;
          StandardToolBar: TToolBar;
          SaveButton: TToolButton;
          PrintButton: TToolButton;
          ToolButton5: TToolButton;
          CutButton: TToolButton;
          CopyButton: TToolButton;
          PasteButton: TToolButton;
          UndoButton: TToolButton;
          ToolButton10: TToolButton;
          FontName: TComboBox;
          ToolButton11: TToolButton;
          FontSize: TEdit;
          UpDown1: TUpDown;
          ToolButton2: TToolButton;
          BoldButton: TToolButton;
          ItalicButton: TToolButton;
          UnderlineButton: TToolButton;
          ToolButton16: TToolButton;
          LeftAlign: TToolButton;
          CenterAlign: TToolButton;
          RightAlign: TToolButton;
          ToolButton20: TToolButton;
          BulletsButton: TToolButton;
          Editor: TDBRichEdit;
          OpenDialog: TOpenDialog;
          SaveDialog: TSaveDialog;
          PrintDialog: TPrintDialog;
          FontDialog1: TFontDialog;
          ToolbarImages: TImageList;
          ActionList1: TActionList;
          FileSaveCmd: TAction;
          FilePrintCmd: TAction;
          FileExitCmd: TAction;
          FileSaveAsCmd: TAction;
          ActionList2: TActionList;
          EditUndoCmd: TAction;
          EditCutCmd: TAction;
          EditCopyCmd: TAction;
          EditPasteCmd: TAction;
          EditFontCmd: TAction;
          ToolButton1: TToolButton;
          Post: TAction;

          constructor create(AOwner: TComponent); override;
          procedure SelectionChange(Sender: TObject);
          procedure ShowHint(Sender: TObject);
          procedure FileNew(Sender: TObject);
          procedure FileOpen(Sender: TObject);
          procedure FileSave(Sender: TObject);
          procedure FileSaveAs(Sender: TObject);
          procedure FilePrint(Sender: TObject);
          procedure FileExit(Sender: TObject);
          procedure EditUndo(Sender: TObject);
          procedure EditCut(Sender: TObject);
          procedure EditCopy(Sender: TObject);
          procedure EditPaste(Sender: TObject);
          procedure SelectFont(Sender: TObject);
          procedure RulerResize(Sender: TObject);
          procedure FormResize(Sender: TObject);
          procedure FormPaint(Sender: TObject);
          procedure BoldButtonClick(Sender: TObject);
          procedure ItalicButtonClick(Sender: TObject);
          procedure FontSizeChange(Sender: TObject);
          procedure AlignButtonClick(Sender: TObject);
          procedure FontNameChange(Sender: TObject);
          procedure UnderlineButtonClick(Sender: TObject);
          procedure BulletsButtonClick(Sender: TObject);
          procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
          procedure RulerItemMouseDown(Sender: TObject; Button: TMouseButton;
               Shift: TShiftState; X, Y: Integer);
          procedure RulerItemMouseMove(Sender: TObject; Shift: TShiftState; X,
               Y: Integer);
          procedure FirstIndMouseUp(Sender: TObject; Button: TMouseButton;
               Shift: TShiftState; X, Y: Integer);
          procedure LeftIndMouseUp(Sender: TObject; Button: TMouseButton;
               Shift: TShiftState; X, Y: Integer);
          procedure RightIndMouseUp(Sender: TObject; Button: TMouseButton;
               Shift: TShiftState; X, Y: Integer);
          procedure FormShow(Sender: TObject);
          procedure ActionList2Update(Action: TBasicAction;
               var Handled: Boolean);
          procedure EditorChange(Sender: TObject);
          procedure PostExecute(Sender: TObject);
          procedure FileSaveCmdExecute(Sender: TObject);
          procedure FilePrintCmdExecute(Sender: TObject);
          procedure FileSaveAsCmdExecute(Sender: TObject);
     public
          Intestazione: string;
          procedure StampaConIntestazione;
     private
          FFileName: string;
          FUpdating: Boolean;
          FDragOfs: Integer;
          FDragging: Boolean;
          function CurrText: TTextAttributes;
          procedure GetFontNames;
          procedure SetFileName(const FileName: string);
          procedure CheckFileSave;
          procedure SetupRuler;
          procedure SetEditRect;
          procedure UpdateCursorPos;
          procedure WMDropFiles(var Msg: TWMDropFiles); message WM_DROPFILES;
          procedure PerformFileOpen(const AFileName: string);
          procedure SetModified(Value: Boolean);
     end;

implementation

{$R *.DFM}


uses RichEdit, ShellAPI, ModuloDati;

resourcestring
     sSaveChanges = 'Save changes to %s?';
     sOverWrite = 'OK to overwrite %s';
     sUntitled = 'Untitled';
     sModified = 'Modified';
     sColRowInfo = 'Line: %3d   Col: %3d';

const
     RulerAdj = 4 / 3;
     GutterWid = 6;


procedure TFrameDBRich.SelectionChange(Sender: TObject);
begin
     with Editor.Paragraph do
     try
          FUpdating := True;
          FirstInd.Left := Trunc(FirstIndent * RulerAdj) - 4 + GutterWid;
          LeftInd.Left := Trunc((LeftIndent + FirstIndent) * RulerAdj) - 4 + GutterWid;
          RightInd.Left := Ruler.ClientWidth - 6 - Trunc((RightIndent + GutterWid) * RulerAdj);
          BoldButton.Down := fsBold in Editor.SelAttributes.Style;
          ItalicButton.Down := fsItalic in Editor.SelAttributes.Style;
          UnderlineButton.Down := fsUnderline in Editor.SelAttributes.Style;
          BulletsButton.Down := Boolean(Numbering);
          FontSize.Text := IntToStr(Editor.SelAttributes.Size);
          FontName.Text := Editor.SelAttributes.Name;
          case Ord(Alignment) of
               0: LeftAlign.Down := True;
               1: RightAlign.Down := True;
               2: CenterAlign.Down := True;
          end;
          UpdateCursorPos;
     finally
          FUpdating := False;
     end;
end;

function TFrameDBRich.CurrText: TTextAttributes;
begin
     if Editor.SelLength > 0 then Result := Editor.SelAttributes
     else Result := Editor.DefAttributes;
end;

function EnumFontsProc(var LogFont: TLogFont; var TextMetric: TTextMetric;
     FontType: Integer; Data: Pointer): Integer; stdcall;
begin
     TStrings(Data).Add(LogFont.lfFaceName);
     Result := 1;
end;

procedure TFrameDBRich.GetFontNames;
var
     DC: HDC;
begin
     DC := GetDC(0);
     EnumFonts(DC, nil, @EnumFontsProc, Pointer(FontName.Items));
     ReleaseDC(0, DC);
     FontName.Sorted := True;
end;

procedure TFrameDBRich.SetFileName(const FileName: string);
begin
     FFileName := FileName;
     Caption := Format('%s - %s', [ExtractFileName(FileName), Application.Title]);
end;

procedure TFrameDBRich.CheckFileSave;
var
     SaveResp: Integer;
begin
     if not Editor.Modified then Exit;
     SaveResp := MessageDlg(Format(sSaveChanges, [FFileName]),
          mtConfirmation, mbYesNoCancel, 0);
     case SaveResp of
          idYes: FileSave(Self);
          idNo: {Nothing};
          idCancel: Abort;
     end;
end;

procedure TFrameDBRich.SetupRuler;
var
     I: Integer;
     S: string;
begin
     SetLength(S, 201);
     I := 1;
     while I < 200 do
     begin
          S[I] := #9;
          S[I + 1] := '|';
          Inc(I, 2);
     end;
     Ruler.Caption := S;
end;

procedure TFrameDBRich.SetEditRect;
var
     R: TRect;
begin
     with Editor do
     begin
          R := Rect(GutterWid, 0, ClientWidth - GutterWid, ClientHeight);
          SendMessage(Handle, EM_SETRECT, 0, Longint(@R));
     end;
end;

{ Event Handlers }



procedure TFrameDBRich.ShowHint(Sender: TObject);
begin
     if Length(Application.Hint) > 0 then
     begin
          StatusBar.SimplePanel := True;
          StatusBar.SimpleText := Application.Hint;
     end
     else StatusBar.SimplePanel := False;
end;

procedure TFrameDBRich.FileNew(Sender: TObject);
begin
     SetFileName(sUntitled);
     Editor.Lines.Clear;
     Editor.Modified := False;
     SetModified(False);
end;

procedure TFrameDBRich.PerformFileOpen(const AFileName: string);
begin
     Editor.Lines.LoadFromFile(AFileName);
     SetFileName(AFileName);
     Editor.SetFocus;
     Editor.Modified := False;
     SetModified(False);
end;

procedure TFrameDBRich.FileOpen(Sender: TObject);
begin
     CheckFileSave;
     if OpenDialog.Execute then
     begin
          PerformFileOpen(OpenDialog.FileName);
          Editor.ReadOnly := ofReadOnly in OpenDialog.Options;
     end;
end;

procedure TFrameDBRich.FileSave(Sender: TObject);
begin
     if FFileName = sUntitled then
          FileSaveAs(Sender)
     else
     begin
          Editor.Lines.SaveToFile(FFileName);
          Editor.Modified := False;
          SetModified(False);
     end;
end;

procedure TFrameDBRich.FileSaveAs(Sender: TObject);
begin
     if SaveDialog.Execute then
     begin
          if FileExists(SaveDialog.FileName) then
               if MessageDlg(Format(sOverWrite, [SaveDialog.FileName]),
                    mtConfirmation, mbYesNoCancel, 0) <> idYes then Exit;
          Editor.Lines.SaveToFile(SaveDialog.FileName);
          SetFileName(SaveDialog.FileName);
          Editor.Modified := False;
          SetModified(False);
     end;
end;

procedure TFrameDBRich.FilePrint(Sender: TObject);
begin
     if PrintDialog.Execute then
          Editor.Print(FFileName);
end;

procedure TFrameDBRich.FileExit(Sender: TObject);
begin
     Hide;
end;

procedure TFrameDBRich.EditUndo(Sender: TObject);
begin
     with Editor do
          if HandleAllocated then SendMessage(Handle, EM_UNDO, 0, 0);
end;

procedure TFrameDBRich.EditCut(Sender: TObject);
begin
     Editor.CutToClipboard;
end;

procedure TFrameDBRich.EditCopy(Sender: TObject);
begin
     Editor.CopyToClipboard;
end;

procedure TFrameDBRich.EditPaste(Sender: TObject);
begin
     Editor.PasteFromClipboard;
end;

procedure TFrameDBRich.SelectFont(Sender: TObject);
begin
     FontDialog1.Font.Assign(Editor.SelAttributes);
     if FontDialog1.Execute then
          CurrText.Assign(FontDialog1.Font);
     SelectionChange(Self);
     Editor.SetFocus;
end;

procedure TFrameDBRich.RulerResize(Sender: TObject);
begin
     RulerLine.Width := Ruler.ClientWidth - (RulerLine.Left * 2);
end;

procedure TFrameDBRich.FormResize(Sender: TObject);
begin
     SetEditRect;
     SelectionChange(Sender);
end;

procedure TFrameDBRich.FormPaint(Sender: TObject);
begin
     SetEditRect;
end;

procedure TFrameDBRich.BoldButtonClick(Sender: TObject);
begin
     editor.DataSource.DataSet.Edit;
     if FUpdating then Exit;
     if BoldButton.Down then
          CurrText.Style := CurrText.Style + [fsBold]
     else
          CurrText.Style := CurrText.Style - [fsBold];
end;

procedure TFrameDBRich.ItalicButtonClick(Sender: TObject);
begin
     editor.DataSource.DataSet.Edit;
     if FUpdating then Exit;
     if ItalicButton.Down then
          CurrText.Style := CurrText.Style + [fsItalic]
     else
          CurrText.Style := CurrText.Style - [fsItalic];
end;

procedure TFrameDBRich.FontSizeChange(Sender: TObject);
begin
     try
          if not (editor.DataSource.DataSet.Active) then
               editor.DataSource.DataSet.Open;
          editor.DataSource.DataSet.Edit;
          if FUpdating then Exit;
          CurrText.Size := StrToInt(FontSize.Text);
     except
     end;
end;

procedure TFrameDBRich.AlignButtonClick(Sender: TObject);
begin
     editor.DataSource.DataSet.Edit;
     if FUpdating then Exit;
     Editor.Paragraph.Alignment := TAlignment(TControl(Sender).Tag);
end;

procedure TFrameDBRich.FontNameChange(Sender: TObject);
begin
     editor.DataSource.DataSet.Edit;
     if FUpdating then Exit;
     CurrText.Name := FontName.Items[FontName.ItemIndex];
end;

procedure TFrameDBRich.UnderlineButtonClick(Sender: TObject);
begin
     editor.DataSource.DataSet.Edit;
     if FUpdating then Exit;
     if UnderlineButton.Down then
          CurrText.Style := CurrText.Style + [fsUnderline]
     else
          CurrText.Style := CurrText.Style - [fsUnderline];
end;

procedure TFrameDBRich.BulletsButtonClick(Sender: TObject);
begin
     editor.DataSource.DataSet.Edit;
     if FUpdating then Exit;
     Editor.Paragraph.Numbering := TNumberingStyle(BulletsButton.Down);
end;

procedure TFrameDBRich.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
     try
          CheckFileSave;
     except
          CanClose := False;
     end;
end;

{ Ruler Indent Dragging }

procedure TFrameDBRich.RulerItemMouseDown(Sender: TObject; Button: TMouseButton;
     Shift: TShiftState; X, Y: Integer);
begin
     FDragOfs := (TLabel(Sender).Width div 2);
     TLabel(Sender).Left := TLabel(Sender).Left + X - FDragOfs;
     FDragging := True;
end;

procedure TFrameDBRich.RulerItemMouseMove(Sender: TObject; Shift: TShiftState;
     X, Y: Integer);
begin
     if FDragging then
          TLabel(Sender).Left := TLabel(Sender).Left + X - FDragOfs
end;

procedure TFrameDBRich.FirstIndMouseUp(Sender: TObject; Button: TMouseButton;
     Shift: TShiftState; X, Y: Integer);
begin
     editor.DataSource.DataSet.Edit;
     FDragging := False;
     Editor.Paragraph.FirstIndent := Trunc((FirstInd.Left + FDragOfs - GutterWid) / RulerAdj);
     LeftIndMouseUp(Sender, Button, Shift, X, Y);
end;

procedure TFrameDBRich.LeftIndMouseUp(Sender: TObject; Button: TMouseButton;
     Shift: TShiftState; X, Y: Integer);
begin
     editor.DataSource.DataSet.Edit;
     FDragging := False;
     Editor.Paragraph.LeftIndent := Trunc((LeftInd.Left + FDragOfs - GutterWid) / RulerAdj) - Editor.Paragraph.FirstIndent;
     SelectionChange(Sender);
end;

procedure TFrameDBRich.RightIndMouseUp(Sender: TObject; Button: TMouseButton;
     Shift: TShiftState; X, Y: Integer);
begin
     editor.DataSource.DataSet.Edit;
     FDragging := False;
     Editor.Paragraph.RightIndent := Trunc((Ruler.ClientWidth - RightInd.Left + FDragOfs - 2) / RulerAdj) - 2 * GutterWid;
     SelectionChange(Sender);
end;

procedure TFrameDBRich.UpdateCursorPos;
var
     CharPos: TPoint;
begin
     CharPos.Y := SendMessage(Editor.Handle, EM_EXLINEFROMCHAR, 0,
          Editor.SelStart);
     CharPos.X := (Editor.SelStart -
          SendMessage(Editor.Handle, EM_LINEINDEX, CharPos.Y, 0));
     Inc(CharPos.Y);
     Inc(CharPos.X);
     StatusBar.Panels[0].Text := Format(sColRowInfo, [CharPos.Y, CharPos.X]);
end;

procedure TFrameDBRich.FormShow(Sender: TObject);
begin
     UpdateCursorPos;
     DragAcceptFiles(Handle, True);
     EditorChange(nil);
     Editor.SetFocus;
     { Check if we should load a file from the command line }
     if (ParamCount > 0) and FileExists(ParamStr(1)) then
          PerformFileOpen(ParamStr(1));
end;

procedure TFrameDBRich.WMDropFiles(var Msg: TWMDropFiles);
var
     CFileName: array[0..MAX_PATH] of Char;
begin
     try
          if DragQueryFile(Msg.Drop, 0, CFileName, MAX_PATH) > 0 then
          begin
               CheckFileSave;
               PerformFileOpen(CFileName);
               Msg.Result := 0;
          end;
     finally
          DragFinish(Msg.Drop);
     end;
end;

procedure TFrameDBRich.SetModified(Value: Boolean);
begin
     if Value then StatusBar.Panels[1].Text := sModified
     else StatusBar.Panels[1].Text := '';
end;

procedure TFrameDBRich.ActionList2Update(Action: TBasicAction;
     var Handled: Boolean);
begin
     { Update the status of the edit commands }
     EditCutCmd.Enabled := Editor.SelLength > 0;
     EditCopyCmd.Enabled := EditCutCmd.Enabled;
     if Editor.HandleAllocated then
     begin
          EditUndoCmd.Enabled := Editor.Perform(EM_CANUNDO, 0, 0) <> 0;
          EditPasteCmd.Enabled := Editor.Perform(EM_CANPASTE, 0, 0) <> 0;
     end;
end;

procedure TFrameDBRich.EditorChange(Sender: TObject);
begin
     SetModified(Editor.Modified);
end;

procedure TFrameDBRich.PostExecute(Sender: TObject);
begin
     Editor.text := Editor.text + ' ';

     if Editor.DataSource = Data.DsQAnagNote then begin
          //if editor.DataSource.DataSet.State in [dsEdit] then
               if Data.DsQAnagNote.State in [dsEdit] then
                   Data.QAnagNote.post;
          //editor.DataSource.DataSet.Post;
       //else
         // editor.DataSource.DataSet.cancel;
     end;
end;

procedure TFrameDBRich.FileSaveCmdExecute(Sender: TObject);
begin
     if FFileName = sUntitled then
          FileSaveAs(Sender)
     else
     begin
          Editor.Lines.SaveToFile(FFileName);
          Editor.Modified := False;
          SetModified(False);
     end;
end;

procedure TFrameDBRich.StampaConIntestazione;
begin
     if PrintDialog.Execute then
          Editor.Print(Intestazione);
end;

procedure TFrameDBRich.FilePrintCmdExecute(Sender: TObject);
var xNote: string;
begin
     xNote := Editor.Text;
     if Editor.DataSource = Data.DsQAnagNote then begin
          Intestazione := 'Note di ' + Data.TAnagrafica.FieldByName('Cognome').asString + ' ' + Data.TAnagrafica.FieldByName('Nome').asString;
          //xNote := Editor.Text;
          Editor.Text := 'Note di ' + Data.TAnagrafica.FieldByName('Cognome').asString + ' ' + Data.TAnagrafica.FieldByName('Nome').asString + chr(13) + chr(13) + Editor.Text;
     end;
     StampaConIntestazione;
     //if Editor.DataSource = Data.DsAnagrafica then
     Editor.Text := xNote;

     //fede 11/03/2016 questa merda ci vuole se no quando si esce dalla stampa rimane in edit e da un errore
     if Editor.DataSource = Data.DsQAnagNote then begin
          if Data.DsQAnagNote.State in [dsEdit] then
               Data.QAnagNote.cancel;
     end;
end;

procedure TFrameDBRich.FileSaveAsCmdExecute(Sender: TObject);
begin
     filesaveas(self);
end;

constructor TFrameDBRich.create(AOwner: TComponent);
begin
     inherited;
     OpenDialog.InitialDir := ExtractFilePath(ParamStr(0));
     SaveDialog.InitialDir := OpenDialog.InitialDir;
     SetFileName(sUntitled);
     GetFontNames;
     SetupRuler;
     SelectionChange(Self);

     CurrText.Name := DefFontData.Name;
     CurrText.Size := -MulDiv(DefFontData.Height, 72, Screen.PixelsPerInch);
end;

end.

