object StoricoPrezziAnnunciCliForm: TStoricoPrezziAnnunciCliForm
  Left = 374
  Top = 180
  BorderStyle = bsDialog
  Caption = 'Storico Prezzi Annunci per il cliente'
  ClientHeight = 467
  ClientWidth = 737
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 3
    Top = 4
    Width = 246
    Height = 45
    BevelOuter = bvLowered
    Enabled = False
    TabOrder = 0
    object Label1: TLabel
      Left = 5
      Top = 4
      Width = 35
      Height = 13
      Caption = 'Cliente:'
    end
    object ECliente: TEdit
      Left = 5
      Top = 18
      Width = 235
      Height = 21
      Color = clAqua
      TabOrder = 0
    end
  end
  object PCValute: TPageControl
    Left = 3
    Top = 55
    Width = 733
    Height = 407
    ActivePage = TSEuro
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 1
    object TSLire: TTabSheet
      Caption = 'Prezzi in Lire'
      TabVisible = False
      object Panel2: TPanel
        Left = 0
        Top = 342
        Width = 725
        Height = 37
        Align = alBottom
        BevelOuter = bvLowered
        TabOrder = 0
        object Label2: TLabel
          Left = 215
          Top = 12
          Width = 49
          Height = 13
          Caption = 'TOTALI:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object DBEdit1: TDBEdit
          Left = 315
          Top = 8
          Width = 78
          Height = 21
          DataField = 'sumCostoLire'
          DataSource = DsQTotali
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 470
          Top = 8
          Width = 78
          Height = 21
          DataField = 'sumCostoANoiLire'
          DataSource = DsQTotali
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 393
          Top = 8
          Width = 77
          Height = 21
          Color = 15456255
          DataField = 'sumScontoAlClienteLire'
          DataSource = DsQTotali
          TabOrder = 2
        end
        object DBEdit4: TDBEdit
          Left = 548
          Top = 8
          Width = 77
          Height = 21
          Color = 13431295
          DataField = 'sumScontoANoiLire'
          DataSource = DsQTotali
          TabOrder = 3
        end
        object DBEdit5: TDBEdit
          Left = 625
          Top = 8
          Width = 91
          Height = 21
          Color = clAqua
          DataField = 'GuadagnoLire'
          DataSource = DsQTotali
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clPurple
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 4
        end
        object DBEdit6: TDBEdit
          Left = 274
          Top = 8
          Width = 41
          Height = 21
          DataField = 'Totmoduli'
          DataSource = DsQTotali
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 5
        end
      end
      object RxDBGrid1: TdxDBGrid
        Left = 0
        Top = 0
        Width = 725
        Height = 342
        Bands = <
          item
          end>
        DefaultLayout = True
        HeaderPanelRowCount = 1
        KeyField = 'ID'
        SummaryGroups = <>
        SummarySeparator = ', '
        Align = alClient
        TabOrder = 1
        DataSource = dsQStoricoCli
        Filter.Criteria = {00000000}
        OptionsBehavior = [edgoAutoSort, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoTabThrough, edgoVertThrough]
        OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
        OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoIndicator, edgoUseBitmap]
        object dxDBGridData: TdxDBGridColumn
          Width = 42
          BandIndex = 0
          RowIndex = 0
          FieldName = 'Data'
        end
        object dxDBGridTestata: TdxDBGridColumn
          Width = 103
          BandIndex = 0
          RowIndex = 0
          FieldName = 'Testata'
        end
        object dxDBGridNomeEdizione: TdxDBGridColumn
          Caption = 'Edizione'
          Width = 137
          BandIndex = 0
          RowIndex = 0
          FieldName = 'NomeEdizione'
        end
        object dxDBGridNumModuli: TdxDBGridColumn
          Caption = 'Moduli'
          Width = 68
          BandIndex = 0
          RowIndex = 0
          FieldName = 'NumModuli'
        end
        object dxDBGridCostoLire: TdxDBGridColumn
          Caption = 'Costo al Ciente'
          Width = 82
          BandIndex = 0
          RowIndex = 0
          FieldName = 'CostoLire'
        end
        object dxDBGridScontoAlClienteLire: TdxDBGridColumn
          Caption = 'Sconto'
          Width = 68
          BandIndex = 0
          RowIndex = 0
          FieldName = 'ScontoAlClienteLire'
        end
        object dxDBGridCostoANoiLire: TdxDBGridColumn
          Caption = 'Costo A Noi'
          Width = 68
          BandIndex = 0
          RowIndex = 0
          FieldName = 'CostoANoiLire'
        end
        object dxDBGridScontoANoiLire: TdxDBGridColumn
          Caption = 'Sconto A Noi'
          Width = 74
          BandIndex = 0
          RowIndex = 0
          FieldName = 'ScontoANoiLire'
        end
        object dxDBGridGuadagnoLire: TdxDBGridColumn
          Caption = 'Guadagno'
          Width = 67
          BandIndex = 0
          RowIndex = 0
          FieldName = 'GuadagnoLire'
        end
      end
    end
    object TSEuro: TTabSheet
      Caption = 'Prezzi in Euro'
      ImageIndex = 1
      object Panel3: TPanel
        Left = 0
        Top = 342
        Width = 725
        Height = 37
        Align = alBottom
        BevelOuter = bvLowered
        TabOrder = 0
        object Label3: TLabel
          Left = 208
          Top = 12
          Width = 49
          Height = 13
          Caption = 'TOTALI:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object DBEdit7: TDBEdit
          Left = 317
          Top = 8
          Width = 78
          Height = 21
          DataField = 'sumCostoEuro'
          DataSource = DsQTotali
          TabOrder = 0
        end
        object DBEdit8: TDBEdit
          Left = 472
          Top = 8
          Width = 78
          Height = 21
          DataField = 'sumCostoANoiEuro'
          DataSource = DsQTotali
          TabOrder = 1
        end
        object DBEdit9: TDBEdit
          Left = 395
          Top = 8
          Width = 77
          Height = 21
          Color = 15456255
          DataField = 'sumScontoAlClienteEuro'
          DataSource = DsQTotali
          TabOrder = 2
        end
        object DBEdit10: TDBEdit
          Left = 550
          Top = 8
          Width = 77
          Height = 21
          Color = 13431295
          DataField = 'sumScontoANoiEuro'
          DataSource = DsQTotali
          TabOrder = 3
        end
        object DBEdit11: TDBEdit
          Left = 627
          Top = 8
          Width = 91
          Height = 21
          Color = clAqua
          DataField = 'GuadagnoEuro'
          DataSource = DsQTotali
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clPurple
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 4
        end
        object DBEdit12: TDBEdit
          Left = 276
          Top = 8
          Width = 41
          Height = 21
          DataField = 'Totmoduli'
          DataSource = DsQTotali
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 5
        end
      end
      object RxDBGrid2: TdxDBGrid
        Left = 0
        Top = 0
        Width = 725
        Height = 342
        Bands = <
          item
          end>
        DefaultLayout = True
        HeaderPanelRowCount = 1
        KeyField = 'ID'
        SummaryGroups = <>
        SummarySeparator = ', '
        Align = alClient
        TabOrder = 1
        DataSource = dsQStoricoCli
        Filter.Criteria = {00000000}
        OptionsBehavior = [edgoAutoSort, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoTabThrough, edgoVertThrough]
        OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
        OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoIndicator, edgoUseBitmap]
        object RxDBGrid2Data: TdxDBGridColumn
          Caption = 'Data'
          Width = 42
          BandIndex = 0
          RowIndex = 0
        end
        object RxDBGrid2Testata: TdxDBGridColumn
          Width = 103
          BandIndex = 0
          RowIndex = 0
          FieldName = 'Testata'
        end
        object RxDBGrid2NomeEdizione: TdxDBGridColumn
          Caption = 'Edizione'
          Width = 137
          BandIndex = 0
          RowIndex = 0
          FieldName = 'NomeEdizione'
        end
        object RxDBGrid2NumModuli: TdxDBGridColumn
          Caption = 'Moduli'
          Width = 68
          BandIndex = 0
          RowIndex = 0
          FieldName = 'NumModuli'
        end
        object RxDBGrid2CostoEuro: TdxDBGridColumn
          Caption = 'Costo al Ciente'
          Width = 82
          BandIndex = 0
          RowIndex = 0
          FieldName = 'CostoEuro'
        end
        object RxDBGrid2ScontoAlClienteEuro: TdxDBGridColumn
          Caption = 'Sconto'
          Width = 68
          BandIndex = 0
          RowIndex = 0
          FieldName = 'ScontoAlClienteEuro'
        end
        object RxDBGrid2CostoANoiEuro: TdxDBGridColumn
          Caption = 'Costo A Noi'
          Width = 68
          BandIndex = 0
          RowIndex = 0
          FieldName = 'CostoANoiEuro'
        end
        object RxDBGrid2ScontoANoiEuro: TdxDBGridColumn
          Caption = 'Sconto A Noi'
          Width = 74
          BandIndex = 0
          RowIndex = 0
          FieldName = 'ScontoANoiEuro'
        end
        object RxDBGrid2GuadagnoEuro: TdxDBGridColumn
          Caption = 'Guadagno'
          Width = 67
          BandIndex = 0
          RowIndex = 0
          FieldName = 'GuadagnoEuro'
        end
      end
    end
  end
  object Panel4: TPanel
    Left = 560
    Top = 0
    Width = 185
    Height = 49
    BevelOuter = bvNone
    TabOrder = 2
    object BOK: TToolbarButton97
      Left = 75
      Top = 3
      Width = 98
      Height = 42
      Caption = 'Esci'
      Glyph.Data = {
        76060000424D7606000000000000360400002800000018000000180000000100
        080000000000400200000000000000000000000100000000000000000000FFFF
        FF00FF00FF0040D86800B1AEAC00545149001D973F0076967600C4F1D1002827
        270079D2910045A35E00716D5E00CFC7C00024C04D009B958D005FBA77005CE6
        8000E4EFE70090B69900B1D9BC005C8864003D3C390076B3870034934E003ABD
        5D007C7B730054D073005F5E5E00938F7800AAA695002CD358009BC6A60062A4
        73006AC98300E0D5C70049EC7500C1BBB7004FB96A00D5EEDB00697C6D008187
        820081C091002AAF4D00B5E9C300F1F7F2009D9D9C003432310044C666005397
        60009F9B7F0037CC5E004A49430039A0550072A67F007C9B83005F7763006E6E
        6D0055A66C006BB87F00A7A6A5008B8C880030C257004ADC700086B48800668B
        6C0060B06C0077C48B00259F470092C29C00C8C2B00054C27100E7FAEC0065B0
        790085827000C5C3C20056DC790071D08A005B584E005BCA79009A939900DCF3
        E3004DCC6E0086C4960080AC8C0032B054007572710064C17D003DA95B0041B4
        6000C9E7D100A39F9200B9F1C800F8FBF900D9D1C000BBB5B50046E67000B1AE
        A20048AB630080C793008DBB91006D9276006EAF7F0064D081004EE3750043CF
        680078BB890056B66F002ABD52007FB68D00CEECD6003FC262007E7E7B006F88
        7000329F500094908800D0CCB90031CC5A00C1B9AE00489B5F00A7A49E003BD1
        6300C4BBC100679F6F0077CD8E00CCC7C60055E57A004FA66700888886005DB5
        720072C0870095BD9700ACABA8005CA76F008CC09A006360610030BD56004DD1
        700046BF66005BC37700CBC8B600BBB8B90038B7590049A85B00AFAA98008482
        7500A09A8D0073C9890033D05D00B5B2A80059B16F00D9EEDF0087BB940033C6
        5A0096C3A1006DC4850069AB7A0040DE6A0051D775005B59590049D76D009B98
        80008F8D8500ECF6EF00E5F4E90028C35100B8AFB1003FCB63005AE07E009C97
        880065CA7F007B967A0062B47300D3EAD9004949480042BA610079767500A2A1
        9F006CCD8600A2A1990070B5820074B98600D4CFBC00C4BFB9002BC756003CA4
        58004AC66B008DB68E007CC08D00BFBDBE0068BD7E003CC6610049E773003131
        2F00E0F2E500C9C1BF0045E06E004FD472006B866D0082817D0094BE9F005FBF
        790065A97700C1EDCD003BD6640053E1790063786600B1AFA700ABA9A4007CCC
        9100C9BFB40037BC5A003DB45D00657C6A009BC1A600D1EFD90051B56C007DBA
        8E007A7870009B9B9B007BB88B00D9F2DF00B8ECC60026B04B002BB34F004C9A
        61008F8F8C00E8F5EB0059C3740093C69F0062B478008CB6970085BE9300A5A4
        A20055D476009D969A0053B96E005CBE76007CC78F00DEEEE300CAEDD300D3CB
        BD00BFB6B400B7B3B20056544B00DCD3C200CFC9BC00C6BDBD0037CF5F0041DA
        6A0042A45D00ADADAC0057E17C00A9A8A800A4A0950086868300020202020202
        022E39AEC10909C1AE392E020202020202020202020202B11C2F1C507ABDBD7A
        EB872F9F2E0202020202020202028016567DE5E488757588E4E57DB016FF0202
        0202020202C734A69A3E033FCDFCFCCD3FCC3E455F34700202020202E205F7AC
        94A03F69687E1111A84CA09481C3F43D0202023C4EF2AC7579CC30E31B60C068
        CDFC4CC57596254EE902021A7383B833796FC26EA3EA24C0C43F4C9E89B8640F
        DA023C0C0D2B993319C26E6E6EA31B60C4F9039EC5A7DF0D0CE973A9BB6C998E
        C26E6ED76ED7A3529D03797989526C4092A29176428855C2AD6EDD4851D727A3
        BACC7933A752198FD2914A23358AA4ADAD51081F5CC22727A38AF8339930AF06
        234A1DB6B98BD797C2087579945CA42727A3AF9999D3AF448C1DA174FAEC47D7
        CBA533333375DEA42797A3D46CE026724632A1F5776BEDB24FBF3E999933B8DE
        E39797A35859D8185EA1A9F67B961057224D6752BF3E3E0E2CE3EF122D966231
        F1A9B346ABE6E6BE9B937C0A0A0A4DB2AAF02D12A3ADFA0746FE841ECF21493B
        8243EED1D1D17C939BC95A5DAD0BE1D0908402FEF671B5B46ABC63636363EE43
        823B81147F3AC6F6FE0202FB5BB741D9DC2A535353532ABCB53B4985851525FE
        FB0202028461CF41869886868686E8D91766CA9C15D095D00202020202D0CF5F
        CEE7209A9A86986D17173638F395D00202020202020284787D3D283713D6C854
        65D53D7D7884020202020202020202FBB1044B8D2E2929DB8D4B04B1FB020202
        020202020202020202FBB1DBB1FDFD2EDBB18402020202020202}
      OnClick = BOKClick
    end
  end
  object QStoricoCli_OLD: TQuery
    OnCalcFields = QStoricoCli_OLDCalcFields
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select Ann_AnnEdizData.Data,CostoLire,CostoEuro,'
      '       CostoANoiLire,CostoANoiEuro,Ann_AnnEdizData.NumModuli,'
      '       ScontoAlClienteLire,ScontoAlClienteEuro,'
      '       ScontoANoiLire,ScontoANoiEuro,'
      '       NomeEdizione, Ann_Testate.Denominazione Testata'
      'from Ann_AnnEdizData, Ann_Edizioni, Ann_Testate, Ann_Annunci'
      'where Ann_AnnEdizData.IDEdizione=Ann_Edizioni.ID'
      '  and Ann_Edizioni.IDTestata=Ann_Testate.ID'
      '  and Ann_AnnEdizData.IDAnnuncio=Ann_Annunci.ID'
      '  and Ann_Annunci.IDCliente=:xIDCliente'
      'order by Ann_AnnEdizData.Data desc')
    Left = 16
    Top = 128
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'xIDCliente'
        ParamType = ptUnknown
      end>
    object QStoricoCli_OLDData: TDateTimeField
      FieldName = 'Data'
      Origin = 'EBCDB.Ann_AnnEdizData.Data'
    end
    object QStoricoCli_OLDNomeEdizione: TStringField
      FieldName = 'NomeEdizione'
      Origin = 'EBCDB.Ann_Edizioni.NomeEdizione'
      FixedChar = True
      Size = 50
    end
    object QStoricoCli_OLDTestata: TStringField
      FieldName = 'Testata'
      Origin = 'EBCDB.Ann_Testate.Denominazione'
      FixedChar = True
      Size = 50
    end
    object QStoricoCli_OLDCostoLire: TFloatField
      FieldName = 'CostoLire'
      Origin = 'EBCDB.Ann_AnnEdizData.CostoLire'
      DisplayFormat = '#,###'
    end
    object QStoricoCli_OLDCostoEuro: TFloatField
      FieldName = 'CostoEuro'
      Origin = 'EBCDB.Ann_AnnEdizData.CostoEuro'
      DisplayFormat = '#,###.00'
    end
    object QStoricoCli_OLDCostoANoiLire: TFloatField
      FieldName = 'CostoANoiLire'
      Origin = 'EBCDB.Ann_AnnEdizData.CostoANoiLire'
      DisplayFormat = '#,###'
    end
    object QStoricoCli_OLDCostoANoiEuro: TFloatField
      FieldName = 'CostoANoiEuro'
      Origin = 'EBCDB.Ann_AnnEdizData.CostoANoiEuro'
      DisplayFormat = '#,###.00'
    end
    object QStoricoCli_OLDScontoAlClienteLire: TFloatField
      FieldName = 'ScontoAlClienteLire'
      Origin = 'EBCDB.Ann_AnnEdizData.ScontoAlClienteLire'
      DisplayFormat = '#,###'
    end
    object QStoricoCli_OLDScontoAlClienteEuro: TFloatField
      FieldName = 'ScontoAlClienteEuro'
      Origin = 'EBCDB.Ann_AnnEdizData.ScontoAlClienteEuro'
      DisplayFormat = '#,###.00'
    end
    object QStoricoCli_OLDScontoANoiLire: TFloatField
      FieldName = 'ScontoANoiLire'
      Origin = 'EBCDB.Ann_AnnEdizData.ScontoANoiLire'
      DisplayFormat = '#,###'
    end
    object QStoricoCli_OLDScontoANoiEuro: TFloatField
      FieldName = 'ScontoANoiEuro'
      Origin = 'EBCDB.Ann_AnnEdizData.ScontoANoiEuro'
      DisplayFormat = '#,###.00'
    end
    object QStoricoCli_OLDNumModuli: TIntegerField
      FieldName = 'NumModuli'
    end
    object QStoricoCli_OLDGuadagnoLire: TFloatField
      FieldKind = fkCalculated
      FieldName = 'GuadagnoLire'
      DisplayFormat = '#,###'
      Calculated = True
    end
    object QStoricoCli_OLDGuadagnoEuro: TFloatField
      FieldKind = fkCalculated
      FieldName = 'GuadagnoEuro'
      DisplayFormat = '#,###.00'
      Calculated = True
    end
  end
  object dsQStoricoCli: TDataSource
    DataSet = QStoricoCli
    Left = 16
    Top = 160
  end
  object QTotali_OLD: TQuery
    OnCalcFields = QTotali_OLDCalcFields
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select sum(CostoLire) sumCostoLire,'
      '       sum(CostoEuro) sumCostoEuro,'
      '       sum(CostoANoiLire) sumCostoANoiLire,'
      '       sum(CostoANoiEuro) sumCostoANoiEuro,'
      '       sum(ScontoAlClienteLire) sumScontoAlClienteLire,'
      '       sum(ScontoAlClienteEuro) sumScontoAlClienteEuro,'
      '       sum(ScontoANoiLire) sumScontoANoiLire,'
      '       sum(ScontoANoiEuro) sumScontoANoiEuro,'
      '       sum(Ann_AnnEdizData.NumModuli) TotModuli'
      'from Ann_AnnEdizData, Ann_Annunci'
      'where Ann_AnnEdizData.IDAnnuncio=Ann_Annunci.ID'
      '  and Ann_Annunci.IDCliente=:xIDCLiente')
    Left = 15
    Top = 327
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'xIDCLiente'
        ParamType = ptUnknown
      end>
    object QTotali_OLDsumCostoLire: TFloatField
      FieldName = 'sumCostoLire'
      Origin = 'EBCDB.Ann_AnnEdizData.CostoLire'
      DisplayFormat = '#,###'
    end
    object QTotali_OLDsumCostoEuro: TFloatField
      FieldName = 'sumCostoEuro'
      Origin = 'EBCDB.Ann_AnnEdizData.CostoEuro'
      DisplayFormat = '#,###.##'
    end
    object QTotali_OLDsumCostoANoiLire: TFloatField
      FieldName = 'sumCostoANoiLire'
      Origin = 'EBCDB.Ann_AnnEdizData.CostoANoiLire'
      DisplayFormat = '#,###'
    end
    object QTotali_OLDsumCostoANoiEuro: TFloatField
      FieldName = 'sumCostoANoiEuro'
      Origin = 'EBCDB.Ann_AnnEdizData.CostoANoiEuro'
      DisplayFormat = '#,###.00'
    end
    object QTotali_OLDsumScontoAlClienteLire: TFloatField
      FieldName = 'sumScontoAlClienteLire'
      Origin = 'EBCDB.Ann_AnnEdizData.ScontoAlClienteLire'
      DisplayFormat = '#,###'
    end
    object QTotali_OLDsumScontoAlClienteEuro: TFloatField
      FieldName = 'sumScontoAlClienteEuro'
      Origin = 'EBCDB.Ann_AnnEdizData.ScontoAlClienteEuro'
      DisplayFormat = '#,###.00'
    end
    object QTotali_OLDsumScontoANoiLire: TFloatField
      FieldName = 'sumScontoANoiLire'
      Origin = 'EBCDB.Ann_AnnEdizData.ScontoANoiLire'
      DisplayFormat = '#,###'
    end
    object QTotali_OLDsumScontoANoiEuro: TFloatField
      FieldName = 'sumScontoANoiEuro'
      Origin = 'EBCDB.Ann_AnnEdizData.ScontoANoiEuro'
      DisplayFormat = '#,###.00'
    end
    object QTotali_OLDGuadagnoLire: TFloatField
      FieldKind = fkCalculated
      FieldName = 'GuadagnoLire'
      DisplayFormat = '#,###'
      Calculated = True
    end
    object QTotali_OLDGuadagnoEuro: TFloatField
      FieldKind = fkCalculated
      FieldName = 'GuadagnoEuro'
      DisplayFormat = '#,###.00'
      Calculated = True
    end
    object QTotali_OLDTotmoduli: TIntegerField
      FieldName = 'Totmoduli'
    end
  end
  object DsQTotali: TDataSource
    DataSet = QTotali
    Left = 15
    Top = 359
  end
  object QStoricoCli: TADOLinkedQuery
    Connection = Data.DB
    CursorType = ctStatic
    OnCalcFields = QStoricoCli_OLDCalcFields
    Parameters = <>
    SQL.Strings = (
      
        'select Ann_AnnEdizData.ID, Ann_AnnEdizData.Data,CostoLire,CostoE' +
        'uro,'
      '       CostoANoiLire,CostoANoiEuro,Ann_AnnEdizData.NumModuli,'
      '       ScontoAlClienteLire,ScontoAlClienteEuro,'
      '       ScontoANoiLire,ScontoANoiEuro,'
      '       NomeEdizione, Ann_Testate.Denominazione Testata'
      'from Ann_AnnEdizData, Ann_Edizioni, Ann_Testate, Ann_Annunci'
      'where Ann_AnnEdizData.IDEdizione=Ann_Edizioni.ID'
      '  and Ann_Edizioni.IDTestata=Ann_Testate.ID'
      '  and Ann_AnnEdizData.IDAnnuncio=Ann_Annunci.ID'
      'order by Ann_AnnEdizData.Data desc')
    OriginalSQL.Strings = (
      
        'select Ann_AnnEdizData.ID, Ann_AnnEdizData.Data,CostoLire,CostoE' +
        'uro,'
      '       CostoANoiLire,CostoANoiEuro,Ann_AnnEdizData.NumModuli,'
      '       ScontoAlClienteLire,ScontoAlClienteEuro,'
      '       ScontoANoiLire,ScontoANoiEuro,'
      '       NomeEdizione, Ann_Testate.Denominazione Testata'
      'from Ann_AnnEdizData, Ann_Edizioni, Ann_Testate, Ann_Annunci'
      'where Ann_AnnEdizData.IDEdizione=Ann_Edizioni.ID'
      '  and Ann_Edizioni.IDTestata=Ann_Testate.ID'
      '  and Ann_AnnEdizData.IDAnnuncio=Ann_Annunci.ID'
      '  and Ann_Annunci.IDCliente=:xIDCliente:'
      'order by Ann_AnnEdizData.Data desc')
    UseFilter = False
    Left = 15
    Top = 95
    object QStoricoCliGuadagnoLire1: TFloatField
      FieldKind = fkCalculated
      FieldName = 'GuadagnoLire'
      Calculated = True
    end
    object QStoricoCliGuadagnoEuro: TFloatField
      FieldKind = fkCalculated
      FieldName = 'GuadagnoEuro'
      Calculated = True
    end
    object QStoricoCliData: TDateTimeField
      FieldName = 'Data'
    end
    object QStoricoCliCostoLire: TFloatField
      FieldName = 'CostoLire'
    end
    object QStoricoCliCostoEuro: TFloatField
      FieldName = 'CostoEuro'
    end
    object QStoricoCliCostoANoiLire: TFloatField
      FieldName = 'CostoANoiLire'
    end
    object QStoricoCliCostoANoiEuro: TFloatField
      FieldName = 'CostoANoiEuro'
    end
    object QStoricoCliNumModuli: TIntegerField
      FieldName = 'NumModuli'
    end
    object QStoricoCliScontoAlClienteLire: TFloatField
      FieldName = 'ScontoAlClienteLire'
    end
    object QStoricoCliScontoAlClienteEuro: TFloatField
      FieldName = 'ScontoAlClienteEuro'
    end
    object QStoricoCliScontoANoiLire: TFloatField
      FieldName = 'ScontoANoiLire'
    end
    object QStoricoCliScontoANoiEuro: TFloatField
      FieldName = 'ScontoANoiEuro'
    end
    object QStoricoCliNomeEdizione: TStringField
      FieldName = 'NomeEdizione'
      FixedChar = True
      Size = 50
    end
    object QStoricoCliTestata: TStringField
      FieldName = 'Testata'
      FixedChar = True
      Size = 50
    end
    object QStoricoCliID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
  end
  object QTotali: TADOLinkedQuery
    Connection = Data.DB
    CursorType = ctStatic
    OnCalcFields = QTotali_OLDCalcFields
    Parameters = <>
    SQL.Strings = (
      'select sum(CostoLire) as sumCostoLire,'
      '       sum(CostoEuro) as sumCostoEuro,'
      '       sum(CostoANoiLire) as sumCostoANoiLire,'
      '       sum(CostoANoiEuro) as sumCostoANoiEuro,'
      '       sum(ScontoAlClienteLire) as sumScontoAlClienteLire,'
      '       sum(ScontoAlClienteEuro) as sumScontoAlClienteEuro,'
      '       sum(ScontoANoiLire) as sumScontoANoiLire,'
      '       sum(ScontoANoiEuro) as sumScontoANoiEuro,'
      '       sum(Ann_AnnEdizData.NumModuli) as TotModuli'
      'from Ann_AnnEdizData, Ann_Annunci'
      'where Ann_AnnEdizData.IDAnnuncio=Ann_Annunci.ID')
    OriginalSQL.Strings = (
      'select sum(CostoLire) as sumCostoLire,'
      '       sum(CostoEuro) as sumCostoEuro,'
      '       sum(CostoANoiLire) as sumCostoANoiLire,'
      '       sum(CostoANoiEuro) as sumCostoANoiEuro,'
      '       sum(ScontoAlClienteLire) as sumScontoAlClienteLire,'
      '       sum(ScontoAlClienteEuro) as sumScontoAlClienteEuro,'
      '       sum(ScontoANoiLire) as sumScontoANoiLire,'
      '       sum(ScontoANoiEuro) as sumScontoANoiEuro,'
      '       sum(Ann_AnnEdizData.NumModuli) as TotModuli'
      'from Ann_AnnEdizData, Ann_Annunci'
      'where Ann_AnnEdizData.IDAnnuncio=Ann_Annunci.ID'
      '  and Ann_Annunci.IDCliente=:xIDCLiente:')
    UseFilter = False
    Left = 15
    Top = 295
    object QTotaliGuadagnoLire: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'GuadagnoLire'
      Calculated = True
    end
    object QTotaliGuadagnoEuro: TFloatField
      FieldKind = fkCalculated
      FieldName = 'GuadagnoEuro'
      Calculated = True
    end
    object QTotalisumCostoLire: TFloatField
      FieldName = 'sumCostoLire'
      ReadOnly = True
    end
    object QTotalisumCostoEuro: TFloatField
      FieldName = 'sumCostoEuro'
      ReadOnly = True
    end
    object QTotalisumCostoANoiLire: TFloatField
      FieldName = 'sumCostoANoiLire'
      ReadOnly = True
    end
    object QTotalisumCostoANoiEuro: TFloatField
      FieldName = 'sumCostoANoiEuro'
      ReadOnly = True
    end
    object QTotalisumScontoAlClienteLire: TFloatField
      FieldName = 'sumScontoAlClienteLire'
      ReadOnly = True
    end
    object QTotalisumScontoAlClienteEuro: TFloatField
      FieldName = 'sumScontoAlClienteEuro'
      ReadOnly = True
    end
    object QTotalisumScontoANoiLire: TFloatField
      FieldName = 'sumScontoANoiLire'
      ReadOnly = True
    end
    object QTotalisumScontoANoiEuro: TFloatField
      FieldName = 'sumScontoANoiEuro'
      ReadOnly = True
    end
    object QTotaliTotModuli: TIntegerField
      FieldName = 'TotModuli'
      ReadOnly = True
    end
  end
end
