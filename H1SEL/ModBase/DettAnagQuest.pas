unit DettAnagQuest;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ComCtrls, ExtCtrls, dxTL, dxDBCtrl, dxDBGrid, dxCntner, Db, ADODB,
     StdCtrls, Buttons, DBCtrls, TB97;

type
     TDettAnagQuestForm = class(TForm)
          Panel1: TPanel;
          DsQRisposte: TDataSource;
          QRisposte: TADOQuery;
          QRisposteID: TAutoIncField;
          QRisposteRaggruppamento: TStringField;
          QRisposteSezione: TStringField;
          QRisposteRif: TStringField;
          QRisposteDomanda: TStringField;
          QRispostePeso: TIntegerField;
          QRisposteRisposta: TStringField;
          QRispostePunteggio: TFloatField;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1ID: TdxDBGridMaskColumn;
          dxDBGrid1Raggruppamento: TdxDBGridMaskColumn;
          dxDBGrid1Sezione: TdxDBGridMaskColumn;
          dxDBGrid1Rif: TdxDBGridMaskColumn;
          dxDBGrid1Domanda: TdxDBGridMaskColumn;
          dxDBGrid1Peso: TdxDBGridMaskColumn;
          dxDBGrid1Risposta: TdxDBGridMaskColumn;
          dxDBGrid1Punteggio: TdxDBGridMaskColumn;
          Label1: TLabel;
          DBText1: TDBText;
          Label2: TLabel;
          DBText2: TDBText;
          QInfo: TADOQuery;
          DsQInfo: TDataSource;
          QInfoSoggetto: TStringField;
          QInfoQuestionario: TStringField;
          BOK: TToolbarButton97;
          procedure FormShow(Sender: TObject);
          procedure BOKClick(Sender: TObject);
     private
    { Private declarations }
     public
          xIDAnagQuest: integer;
     end;

var
     DettAnagQuestForm: TDettAnagQuestForm;

implementation

uses ModuloDati, Main;

{$R *.DFM}

procedure TDettAnagQuestForm.FormShow(Sender: TObject);
begin
     //Grafica
     DettAnagQuestForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     QInfo.Parameters[0].value := xIDAnagQuest;
     QInfo.Open;
     QRisposte.Parameters[0].value := xIDAnagQuest;
     QRisposte.Open;
end;

procedure TDettAnagQuestForm.BOKClick(Sender: TObject);
begin
     DettAnagQuestForm.ModalResult := mrOk;
end;

end.

