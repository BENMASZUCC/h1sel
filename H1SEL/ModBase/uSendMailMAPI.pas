unit uSendMailMAPI;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, ShellAPI, ModuloDati,
     StdCtrls, Buttons, ExtCtrls, ComObj, IdComponent, IdBaseComponent, IdMessage, FileCtrl, EmailMessage, Main, Splash3,
     Registry, StrUtils;

function SendEMailMAPI(xSubject, xTesto, xDestinatari, xDestBcc, xAllegati: string; xRemote, xDebug, xShowDialog, xHTML: boolean): boolean;
procedure SendMailOutlook(aRecipient, aSubject, aNote, aFile, aBcc: string; Silent, HTML: boolean);
procedure SendMailThroughClient(SubMail, BodyMail, ToMail, BccMail, AttMail: string; HTMLMail: boolean; var j: integer);
function CopiaApriFileSulClient(xFile: string; xTaggaDaCanc: boolean = false): string;
function CopiaRimuoviDalClient(xFile: string; xCartellaDest: string = ''): string;
function CopiaFileSulClient(xFile: string; xTaggaDaCanc: boolean = true): string;


implementation

// ------------- funzioni per "tunnel" TS -------------------------------------------------------------

function TS_ShellExecute(hwnd: integer; lpOperation, lpFile, lpParameters, lpDirectory: widestring; nShowCd: integer): integer; cdecl; external 'H1TS_Server.dll';

function TS_UserTemp(lpbuffer: PWideChar): integer; cdecl; external 'H1TS_Server.dll';

//function TS_LeggiReg(Chiave, path, Risultato: PWideChar): integer; cdecl; external 'H1TS_Server.dll';

function SendEMailMAPI(xSubject, xTesto, xDestinatari, xDestBcc, xAllegati: string; xRemote, xDebug, xShowDialog, xHTML: boolean): boolean;
var xTempFilePath, xComando_ws, xParameters_ws, xCartellaTemp, xCartellaTempTS, xs, xFile, xFileName, xBody, xAttachments, xFormato, xDest, xBcc: string;
     res, i, j: integer;
     xErrorMsg: string;
     CopyRes: boolean;
     xLog, xFiles, xXML: TStringList;
     xarray: array[1..1024] of WideChar;
     ps, xReg_Chiave, xReg_Path, xReg_Res: PWideChar;
     reg: TRegistry;
     regpostastring, key, find: string;
begin

     xTempFilePath := data.Global.fieldbyname('DirFileLog').asstring;

     if xRemote then begin
          //{
          // --- REMOTO ------------------------------

          xLog := TStringList.create;

          xBody := xTesto;
          if xHTML then
               xBody := StringReplace(xBody, chr(13), '<br>', [rfReplaceAll]);
          xBody := StringReplace(xBody, '<', '&lt;', [rfReplaceAll]);
          xBody := StringReplace(xBody, '>', '&gt;', [rfReplaceAll]);

          xLog.Add('Alcuni parametri: ' + chr(13) + ' Subject = ' + xSubject + chr(13) + ' Dest = ' + xDestinatari + chr(13) + ' Bcc = ' + xDestBcc + chr(13)); xLog.SaveToFile(xTempFilePath + '\Log_InvioMailRemota.txt');

          // porto i file sul client
          xLog.Add('File da copiare: ' + xAllegati); xLog.SaveToFile(xTempFilePath + '\Log_InvioMailRemota.txt');

          ps := @xarray;
          res := TS_UserTemp(ps);

          xCartellaTemp := WideCharToString(ps);
          xLog.Add('Risultato chiamata TS_UserTemp: ' + xCartellaTemp); xLog.SaveToFile(xTempFilePath + '\Log_InvioMailRemota.txt');

          xCartellaTempTS := '\\tsclient\' + StringReplace(xCartellaTemp, ':', '', []);

          if not DirectoryExists(xCartellaTempTS) then begin
               xLog.Add('Cartella temp remota (client) NON ESISTE: ' + xCartellaTempTS); xLog.SaveToFile(xTempFilePath + '\Log_InvioMailRemota.txt')
          end else
               xLog.Add('Cartella temp remota (client): ' + xCartellaTempTS); xLog.SaveToFile(xTempFilePath + '\Log_InvioMailRemota.txt');

          Splash3Form := TSplash3Form.create(nil);
          Splash3Form.Show;
          Application.ProcessMessages;

          if (xAllegati = '-') or (xAllegati = '') or (not DirectoryExists(xCartellaTempTS)) then
               xAttachments := '<Allegati/>'
          else begin

               xAttachments := '<Allegati>';

               xS := xAllegati;

               while pos(';', xS) > 0 do begin

                    xFile := copy(xS, 1, pos(';', xS) - 1);

                    if FileExists(xFile) then begin
                         xFileName := ExtractFileName(xFile);
                         CopyRes := CopyFile(pchar(xFile), pchar(xCartellaTempTS + xFileName), false);
                         if CopyRes then begin

                              xAttachments := xAttachments + '<Allegato>' + xCartellaTemp + xFileName + '</Allegato>' + chr(13);

                              MainForm.xFileTempClient.Add(xCartellaTempTS + xFileName);

                              xLog.Add('Copia file sul client: ' + xFile + ' > ' + xCartellaTempTS + xFileName + ' > OK'); xLog.SaveToFile(xTempFilePath + '\Log_InvioMailRemota.txt');
                         end else begin
                              xLog.Add('Copia file sul client: ' + xFile + ' > ' + xCartellaTempTS + xFileName + ' > ERRORE'); xLog.SaveToFile(xTempFilePath + '\Log_InvioMailRemota.txt');
                         end;
                    end else
                         xLog.Add('Copia file sul client: ' + xFile + ' NON TROVATO'); xLog.SaveToFile(xTempFilePath + '\Log_InvioMailRemota.txt');

                    xS := copy(xS, pos(';', xS) + 1, length(xS));

                    Application.ProcessMessages;
               end;
               xAttachments := xAttachments + '</Allegati>';

          end;

          if xHTML then xFormato := 'HTML' else xFormato := '';
          if xDestinatari = '' then xDest := '(inserire)' else xDest := xDestinatari;
          //if xDestBcc = '' then
          //   xBcc := '-'
          //else
          xBcc := xDestBcc;

          // creazione file XML
          xXML := TStringList.create;
          xXML.Text := '<?xml version="1.0" encoding="ISO-8859-1"?> ' + chr(13) +
               '<operazioni> ' + chr(13) +
               '	<operazione nome="InvioMail" tipo="' + xFormato + '"> ' + chr(13) +
               '		  <oggetto>' + xSubject + '</oggetto>  ' + chr(13) +
               '		  <to>' + xDest + '</to> ' + chr(13) +
               '		  <cc></cc> ' + chr(13) +
               '		  <ccn>' + xBcc + '</ccn> ' + chr(13) +
               '		  <body>' + xBody + '</body> ' + chr(13) +
               xAttachments + chr(13) +
               '</operazione> ' + chr(13) +
               '</operazioni> ';

          xXML.SaveToFile(xCartellaTempTS + 'h1client.xml');

          xLog.Add('Copiato file "h1client.xml" sul client.  XML: ' + chr(13) + xXML.text);
          xLog.SaveToFile(xTempFilePath + '\Log_InvioMailRemota.txt');

          Splash3Form.Hide;
          Splash3Form.Free;

          xComando_ws := xCartellaTemp + '\H1Client.exe';

          xParameters_ws := xCartellaTemp + 'h1client.xml';

          // controllo esistenza H1Client.exe
          if not FileExists(xCartellaTempTS + 'h1client.exe') then begin
               if FileExists(ExtractFileDir(Application.ExeName) + '\H1Client.exe') then begin
                    CopyRes := CopyFile(pchar(ExtractFileDir(Application.ExeName) + '\H1Client.exe'), pchar(xCartellaTempTS + 'H1Client.exe'), false);
                    if CopyRes then begin
                         xLog.Add('Non esiste H1Client.exe sul client: copiato dal server'); xLog.SaveToFile(xTempFilePath + '\Log_InvioMailRemota.txt');
                    end else begin
                         xLog.Add('Non esiste H1Client.exe sul client: copia dal server NON riuscita'); xLog.SaveToFile(xTempFilePath + '\Log_InvioMailRemota.txt');
                    end;
               end else begin
                    MessageDlg('Il file "H1Client.exe" non esiste n� sul client n� sul server' + chr(13) + 'IMPOSSIBILE CONTINUARE', mtError, [mbOK], 0);
                    xLog.Add('Non esiste H1Client.exe sul client, ma nemmeno sul server !'); xLog.SaveToFile(xTempFilePath + '\Log_InvioMailRemota.txt');
                    exit;
               end;
          end;

          try
               xLog.Add('Pronto alla chiamata: ' + chr(13) + '  Comando = ' + xComando_ws + chr(13) + '  Parametri: ' + xParameters_ws + chr(13)); xLog.SaveToFile(xTempFilePath + '\Log_InvioMailRemota.txt');

               res := TS_ShellExecute(0, 'Open', xComando_ws, xParameters_ws, '', SW_HIDE);

          finally
               // cancellazione file sul client
               // >>> NON FUNZIONA, perch� lo cancella prima che il client predisponga il messaggio
               //  i file vengono cancellati all'uscita dal programma (alla fine di MainForm.OnClose)

          end;

          case res of
               0: xErrorMsg := '0 = The operating system is out of memory or resources.';
               1: xErrorMsg := '1 = Non riesco ad aprire il virtual channel (TS_ShellExecute)';
               //2: xErrorMsg := '2 = The specified file was not found';
               2: xErrorMsg := '2 = il virtual channel si chiude prima di inviare i dati (TS_ShellExecute)';
               //3: xErrorMsg := '3 = The specified path was not found.';
               3: xErrorMsg := '3 = fallito l''invio dei dati al client (TS_ShellExecute)';

               5: xErrorMsg := '5 = Windows 95 only: The operating system denied access to the specified file';
               8: xErrorMsg := '8 = Windows 95 only: There was not enough memory to complete the operation.';
               10: xErrorMsg := '10 = Wrong Windows version';
               11: xErrorMsg := '11 = The.EXE file is invalid(non - Win32.EXE or error in .EXE image).';
               12: xErrorMsg := '12 = Application was designed for a different operating system.';
               13: xErrorMsg := '13 = Application was designed for MS - DOS 4.0';
               15: xErrorMsg := '15 = Attempt to load a real - mode program';
               16: xErrorMsg := '16 = Attempt to load a second instance of an application with non - readonly data segments';
               19: xErrorMsg := '19 = Attempt to load a compressed application file';
               20: xErrorMsg := '20 = Dynamic - link library(DLL)file failure".';
               26: xErrorMsg := '26 = A sharing violation occurred.';
               27: xErrorMsg := '27 = The filename association is incomplete or invalid.';
               28: xErrorMsg := '28 = The DDE transaction could not be completed because the request timed out.';
               29: xErrorMsg := '29 = The DDE transaction failed.';
               30: xErrorMsg := '30 = The DDE transaction could not be completed because other DDE transactions were being processed.';
               31: xErrorMsg := '31 = There is no application associated with the given filename extension.';
               32: xErrorMsg := '32 = Windows 95 only: The specified dynamic - link library was not found.';
          else xErrorMsg := IntToStr(res) + ' = (errore non documentato)';
          end;
          if (res < 0) or (res > 32) then
               xErrorMsg := 'OK (' + inttostr(res) + ')';

          // CODICI ERRORE COMUNICATI DA STEFANO ma NON gestiti:
          // 1: non riesce ad aprire il virtual channel
          // 2: il virtual channel si chiude prima di inviare i dati
          // 3: fallisce l'invio dei dai al client
          // 0: la ShellExecute fallisce (quindi la comunicazione � corretta ma c'� qualcosa che non va nel passaggio dei parametri)

          xLog.Add('Chiamata effettuata con risultato: ' + xErrorMsg); xLog.SaveToFile(xTempFilePath + '\Log_InvioMailRemota.txt');

          {if copy(xErrorMsg, 1, 2) = 'OK' then
               MessageDlg('Operazione completata', mtInformation, [mbOK], 0)
          else MessageDlg('ERRORE: ' + xErrorMsg, mtError, [mbOK], 0);}
          if copy(xErrorMsg, 1, 2) <> 'OK' then
               MessageDlg('ERRORE: ' + xErrorMsg, mtError, [mbOK], 0);

          //}

     end else begin
          // --- LOCALE ------------------------------

             //!!.. Controllo registro Qual'� la client posta Predefinta. ..!!
          reg := TRegistry.Create;
          reg.RootKey := HKEY_CURRENT_USER;
          key := 'Software\Clients\Mail';
          find := '';
          Reg.OpenKeyReadOnly(key);
          regpostastring := reg.ReadString(find);
          reg.CloseKey;
          reg.Free;
          //!!.. Serve dopo nella sendmailthrough ..!!
          j := 0;
          //showmessage('Contenuto del registro - ' + regpostastring);
          //showmessage(xDestinatari + xSubject + xTesto + xAllegati + xDestBcc);

          // --- OUTLOOK ---------------------------------------
          if regpostastring = 'Microsoft Outlook' then begin
               //showmessage('invio mail - 11');
               SendMailOutlook(xDestinatari, xSubject, xTesto, xAllegati, xDestBcc, False, xHTML);
               //showmessage('invio mail - 1');
          end else if regpostastring = '' then
          try
               //showmessage('invio mail - 21');
               SendMailOutlook(xDestinatari, xSubject, xTesto, xAllegati, xDestBcc, False, xHTML);
               //showmessage('invio mail - 2');
          except
               //showmessage('invio mail - 31');
               SendMailThroughClient(xSubject, xTesto, xDestinatari, xDestBcc, xAllegati, xHTML, j);
               //showmessage('invio mail - 3');
          end
          else begin
               //showmessage('invio mail - 41');
               SendMailThroughClient(xSubject, xTesto, xDestinatari, xDestBcc, xAllegati, xHTML, j);
               //showmessage('invio mail - 4');
          end
     end;
end;

procedure SendMailOutlook(aRecipient, aSubject, aNote, aFile, aBcc: string; Silent, HTML: boolean);
const olMailItem = 0;
var ii: integer; MyOutlook, MyMail: variant;
     xS: string;
begin
     MyOutlook := CreateOLEObject('Outlook.Application');
     MyMail := MyOutlook.CreateItem(olMailItem);

     //*** create a mail message...
     MyMail. to := aRecipient;
     MyMail.Subject := aSubject;

     if aNote <> '' then begin
          if HTML then
               MyMail.HTMLBody := aNote
          else begin
               MyMail.Body := aNote;
          end;
     end;

     //*** bcc ***
     MyMail.Bcc := aBcc;

     //*** Attachment...
     if (aFile <> '') and (aFile <> '-') then begin
          xS := aFile;
          while pos(';', xS) > 0 do begin
               MyMail.Attachments.Add(copy(xS, 1, pos(';', xS) - 1));
               xS := copy(xS, pos(';', xS) + 1, length(xS));
          end;
     end;

     if Silent then
          MyMail.Send
     else
          MyMail.Display;

     MyOutlook := UnAssigned;
end;

procedure SendMailThroughClient(SubMail, BodyMail, ToMail, BccMail, AttMail: string; HTMLMail: boolean; var j: integer);
var
     i: integer;
     Pathname, adMail: string;
     html: TStrings;
     textpart: TIdText;
     IdMessage1: TIdMessage;
begin
     //setup mail message
     IdMessage1 := TIdMessage.Create(nil);
     if AttMail <> '' then
          TIdAttachment.Create(IdMessage1.MessageParts, AttMail);
     html := TStringList.Create;
     html.Add('<html>');
     html.Add('<head>');
     html.Add('</head>');
     html.Add('<body>');
     html.Add(BodyMail + '<br/>');
     html.Add('</body>');
     html.Add('</html>');
     IdMessage1.Recipients.EMailAddresses := ToMail;
     IdMessage1.Subject := SubMail;
     IdMessage1.ExtraHeaders.Add('Bcc:' + BccMail);
     IdMessage1.ExtraHeaders.Add('X-Unsent: 1');
     if HTMLMail then begin
          IdMessage1.ContentType := 'multipart/mixed';
          IdMessage1.Body.Assign(html);
          IdMessage1.Body.Clear;
          textpart := TIdText.Create(IdMessage1.MessageParts);
          textpart.ContentType := 'text/plain';
          textpart.Body.Text := '';
          textpart.Free;
          textpart := TIdText.Create(IdMessage1.MessageParts, html);
          textpart.ContentType := 'text/html';
          textpart.Free;
     end
     else
          IdMessage1.Body.Text := BodyMail;

     try
          Pathname := Data.Global.fieldbyname('DirFileLog').asString + '\';
          IdMessage1.SaveToFile(Pathname + 'InvioMail' + MainForm.xUtenteAttuale + '_' +
               inttostr(j) + '.eml');

     except
          if not FileExists(Pathname + 'InvioMail' + MainForm.xUtenteAttuale + '_' +
               inttostr(j) + '.eml') then
          begin
               try
                    Pathname := ExtractFilePath(Application.ExeName);
                    IdMessage1.SaveToFile(Pathname + 'InvioMail' + MainForm.xUtenteAttuale + '_' +
                         inttostr(j) + '.eml');
               except
                    on e: Exception do begin
                         if not FileExists(Pathname + 'InvioMail' + MainForm.xUtenteAttuale + '_' +
                              inttostr(j) + '.eml') then
                         begin
                              if copy(e.message, 1, 16) = 'Cannot open file' then
                                   MessageDlg('ERRORE: problemi a salvare il file - possibili cause:' + chr(13) +
                                        '- il file potrebbe essere aperto da un''altra applicazione: occorre chiuderla;' + chr(13) +
                                        '- il file potrebbe essere stato cancellato dalla cartella', mtError, [mbOK], 0)
                              else
                                   MessageDlg('ERRORE: ' + e.message, mtError, [mbOK], 0);
                              exit;
                         end;
                         //on E: Exception do ShowMessage(E.Message);
                    end;
               end;
          end;
     end;
     try
          if FileExists(Pathname + 'InvioMail' + MainForm.xUtenteAttuale + '_' +
               inttostr(j) + '.eml') then
               ShellExecute(0, nil, PAnsiChar(Pathname + 'InvioMail' + MainForm.xUtenteAttuale + '_' +
                    inttostr(j) + '.eml'), nil, nil, SW_SHOW)
          else
               MessageDlg('File ' + Pathname + 'InvioMail' + MainForm.xUtenteAttuale + '_' +
                    inttostr(j) + '.eml' + ' Non Trovato', mtError, [mbOK], 0);
     except
          on E: Exception do begin
               MessageDlg('ERRORE, ' + E.Message, mtError, [mbOK], 0);
          end;
     end;
     j := j + 1;
     IdMessage1.Free;
end;

function CopiaApriFileSulClient(xFile: string; xTaggaDaCanc: boolean = false): string;
var xTempFilePath, xCartellaTemp, xCartellaTempTS, xFileName, xErrorMsg, xFileLog: string;
     xLog: TStringList;
     xarray: array[1..1024] of WideChar;
     ps: PWideChar;
     res: integer;
     CopyRes: boolean;
begin
     //{
     // log
     xTempFilePath := data.Global.fieldbyname('DirFileLog').asstring;
     xFileLog := xTempFilePath + '\Log_CopiaFileSulClient.txt';

     xLog := TStringList.create;
     xLog.Add('File da copiare: ' + xFile); xLog.SaveToFile(xFileLog);

     if FileExists(xFile) then begin

          ps := @xarray;
          res := TS_UserTemp(ps);

          xCartellaTemp := WideCharToString(ps);
          xCartellaTempTS := '\\tsclient\' + StringReplace(xCartellaTemp, ':', '', []);

          if not DirectoryExists(xCartellaTempTS) then begin
               xLog.Add('Cartella temp remota (client) NON ESISTE: ' + xCartellaTempTS); xLog.SaveToFile(xFileLog);
          end else
               xLog.Add('Cartella temp remota (client): ' + xCartellaTempTS); xLog.SaveToFile(xFileLog);

          Splash3Form := TSplash3Form.create(nil);
          Splash3Form.Show;
          Application.ProcessMessages;

          xFileName := ExtractFileName(xFile);
          CopyRes := CopyFile(pchar(xFile), pchar(xCartellaTempTS + xFileName), false);

          xFileName := ExtractFileName(xFile);
          CopyRes := CopyFile(pchar(xFile), pchar(xCartellaTempTS + xFileName), false);
          if CopyRes then begin

               xLog.Add('Copia file sul client: ' + xFile + ' > ' + xCartellaTemp + xFileName + ' > OK'); xLog.SaveToFile(xFileLog);

               Res := TS_ShellExecute(0, 'Open', pchar(xCartellaTemp + xFileName), pchar(''), '', SW_SHOW);

               case res of
                    0: xErrorMsg := '0 = The operating system is out of memory or resources.';
                    1: xErrorMsg := '1 = Non riesco ad aprire il virtual channel (TS_ShellExecute)';
                    //2: xErrorMsg := '2 = The specified file was not found';
                    2: xErrorMsg := '2 = il virtual channel si chiude prima di inviare i dati (TS_ShellExecute)';
                    //3: xErrorMsg := '3 = The specified path was not found.';
                    3: xErrorMsg := '3 = fallito l''invio dei dati al client (TS_ShellExecute)';

                    5: xErrorMsg := '5 = Windows 95 only: The operating system denied access to the specified file';
                    8: xErrorMsg := '8 = Windows 95 only: There was not enough memory to complete the operation.';
                    10: xErrorMsg := '10 = Wrong Windows version';
                    11: xErrorMsg := '11 = The.EXE file is invalid(non - Win32.EXE or error in .EXE image).';
                    12: xErrorMsg := '12 = Application was designed for a different operating system.';
                    13: xErrorMsg := '13 = Application was designed for MS - DOS 4.0';
                    15: xErrorMsg := '15 = Attempt to load a real - mode program';
                    16: xErrorMsg := '16 = Attempt to load a second instance of an application with non - readonly data segments';
                    19: xErrorMsg := '19 = Attempt to load a compressed application file';
                    20: xErrorMsg := '20 = Dynamic - link library(DLL)file failure".';
                    26: xErrorMsg := '26 = A sharing violation occurred.';
                    27: xErrorMsg := '27 = The filename association is incomplete or invalid.';
                    28: xErrorMsg := '28 = The DDE transaction could not be completed because the request timed out.';
                    29: xErrorMsg := '29 = The DDE transaction failed.';
                    30: xErrorMsg := '30 = The DDE transaction could not be completed because other DDE transactions were being processed.';
                    31: xErrorMsg := '31 = There is no application associated with the given filename extension.';
                    32: xErrorMsg := '32 = Windows 95 only: The specified dynamic - link library was not found.';
               else xErrorMsg := IntToStr(res) + ' = (errore non documentato)';
               end;
               if (res < 0) or (res > 32) then
                    xErrorMsg := 'OK (' + inttostr(res) + ')';

               // CODICI ERRORE COMUNICATI DA STEFANO ma NON gestiti:
               // 1: non riesce ad aprire il virtual channel
               // 2: il virtual channel si chiude prima di inviare i dati
               // 3: fallisce l'invio dei dai al client
               // 0: la ShellExecute fallisce (quindi la comunicazione � corretta ma c'� qualcosa che non va nel passaggio dei parametri)

               Result := xErrorMsg;

               xLog.Add('Chiamata effettuata con risultato: ' + xErrorMsg); xLog.SaveToFile(xFileLog);

               {if copy(xErrorMsg, 1, 2) = 'OK' then
                    MessageDlg('File aperto sul client', mtInformation, [mbOK], 0)
               else MessageDlg('ERRORE: ' + xErrorMsg, mtError, [mbOK], 0); }
               if copy(xErrorMsg, 1, 2) <> 'OK' then
                    MessageDlg('ERRORE: ' + xErrorMsg, mtError, [mbOK], 0);

               if xTaggaDaCanc then
                    MainForm.xFileTempClient.Add(xCartellaTempTS + xFileName);

          end else begin
               Result := 'ERRORE copia (vedi log)';
               xLog.Add('Copia file sul client: ' + xFile + ' > ' + xCartellaTempTS + xFileName + ' > ERRORE'); xLog.SaveToFile(xFileLog);
          end;
     end else begin
          Result := 'ERRORE: ' + xFile + ' non trovato';
          xLog.Add('Copia file sul client: ' + xFile + ' NON TROVATO'); xLog.SaveToFile(xFileLog);
     end;

     Splash3Form.Hide;
     Splash3Form.Free;

     Application.ProcessMessages;

     xLog.Free;
     //}

end;

function CopiaRimuoviDalClient(xFile: string; xCartellaDest: string = ''): string;
var xTempFilePath, xCartellaTemp, xCartellaTempTS, xFileName, xErrorMsg, xFileLog, xPathDest: string;
     xLog: TStringList;
     xarray: array[1..1024] of WideChar;
     ps: PWideChar;
     res: integer;
     CopyRes, DelRes: boolean;
begin
     //{
     // log
     xTempFilePath := data.Global.fieldbyname('DirFileLog').asstring;
     xFileLog := xTempFilePath + '\Log_CopiaRimuoviDalClient.txt';

     xFileName := ExtractFileName(xFile);

     xLog := TStringList.create;
     xLog.Add('File da copiare dal client: ' + xFileName); xLog.SaveToFile(xFileLog);

     ps := @xarray;
     res := TS_UserTemp(ps);

     xCartellaTemp := WideCharToString(ps);
     xCartellaTempTS := '\\tsclient\' + StringReplace(xCartellaTemp, ':', '', []);

     if not DirectoryExists(xCartellaTempTS) then begin
          xLog.Add('Cartella temp remota (client) NON ESISTE: ' + xCartellaTempTS); xLog.SaveToFile(xFileLog);
     end else
          xLog.Add('Cartella temp remota (client): ' + xCartellaTempTS); xLog.SaveToFile(xFileLog);

     Splash3Form := TSplash3Form.create(nil);
     Splash3Form.Show;
     Application.ProcessMessages;

     if xCartellaDest = '' then
          xPathDest := data.Global.fieldbyname('DirFileApriCV').asstring + '\'
     else xPathDest := xCartellaDest;

     if FileExists(xCartellaTempTS + xFileName) then begin

          // copia
          CopyRes := CopyFile(pchar(xCartellaTempTS + xFileName), pchar(xPathDest + xFileName), false);

          if CopyRes then begin
               xLog.Add('Copia file dal client: ' + xCartellaTempTS + xFileName + ' > ' + xPathDest + xFileName + ' > OK'); xLog.SaveToFile(xFileLog);
          end else begin
               Result := 'ERRORE copia (vedi log)';
               xLog.Add('Copia file dal client: ' + xCartellaTempTS + xFileName + ' > ' + xPathDest + xFileName + ' > ERRORE'); xLog.SaveToFile(xFileLog);
          end;

         // showmessage('fine copia');

          // cancellazione
          DelRes := DeleteFile(pchar(xCartellaTempTS + xFileName));

          if DelRes then begin
               xLog.Add('Cancellazione file sul client: ' + xCartellaTempTS + xFileName + ' > OK'); xLog.SaveToFile(xFileLog);
          end else begin
               Result := 'ERRORE cancellazione (vedi log)';
               xLog.Add('Cancellazione file sul client: ' + xCartellaTempTS + xFileName + ' > ERRORE'); xLog.SaveToFile(xFileLog);
          end;

     end else begin
          Result := 'ERRORE: ' + xCartellaTempTS + xFileName + ' non trovato';
          xLog.Add('Copia file dal client: ' + xCartellaTempTS + xFileName + ' NON TROVATO'); xLog.SaveToFile(xFileLog);
     end;

     Splash3Form.Hide;
     Splash3Form.Free;

     Application.ProcessMessages;

     xLog.Free;
     //}

end;

function CopiaFileSulClient(xFile: string; xTaggaDaCanc: boolean = true): string;
var xTempFilePath, xCartellaTemp, xCartellaTempTS, xFileName, xErrorMsg, xFileLog: string;
     xLog: TStringList;
     xarray: array[1..1024] of WideChar;
     ps: PWideChar;
     res: integer;
     CopyRes: boolean;
begin
     //{
     // log
     xTempFilePath := data.Global.fieldbyname('DirFileLog').asstring;
     xFileLog := xTempFilePath + '\Log_CopiaFileSulClient.txt';

     xLog := TStringList.create;
     xLog.Add('File da copiare: ' + xFile); xLog.SaveToFile(xFileLog);

     if FileExists(xFile) then begin

          ps := @xarray;
          res := TS_UserTemp(ps);

          xCartellaTemp := WideCharToString(ps);
          xCartellaTempTS := '\\tsclient\' + StringReplace(xCartellaTemp, ':', '', []);

          if not DirectoryExists(xCartellaTempTS) then begin
               xLog.Add('Cartella temp remota (client) NON ESISTE: ' + xCartellaTempTS); xLog.SaveToFile(xFileLog);
          end else
               xLog.Add('Cartella temp remota (client): ' + xCartellaTempTS); xLog.SaveToFile(xFileLog);

          xFileName := ExtractFileName(xFile);
          CopyRes := CopyFile(pchar(xFile), pchar(xCartellaTempTS + xFileName), false);

          xFileName := ExtractFileName(xFile);
          CopyRes := CopyFile(pchar(xFile), pchar(xCartellaTempTS + xFileName), false);
          if CopyRes then begin

               xLog.Add('Copia file sul client: ' + xFile + ' > ' + xCartellaTemp + xFileName + ' > OK'); xLog.SaveToFile(xFileLog);

               if xTaggaDaCanc then
                    MainForm.xFileTempClient.Add(xCartellaTempTS + xFileName);

          end else begin
               Result := 'ERRORE copia (vedi log)';
               xLog.Add('Copia file sul client: ' + xFile + ' > ' + xCartellaTempTS + xFileName + ' > ERRORE'); xLog.SaveToFile(xFileLog);
          end;
     end else begin
          Result := 'ERRORE: ' + xFile + ' non trovato';
          xLog.Add('Copia file sul client: ' + xFile + ' NON TROVATO'); xLog.SaveToFile(xFileLog);
     end;

     Application.ProcessMessages;

     xLog.Free;
     //}

end;


end.

