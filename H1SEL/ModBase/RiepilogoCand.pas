unit riepilogocand;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, DtEdit97, Spin, Buttons, ExtCtrls, Grids, AdvGrid, TB97, Db,
     DBTables, DBGrids, ComCtrls, ProgBarEx, Mask, DBCtrls, OleCtrls,
     RXSpin, ImgList, ImageWindow, ADODB, U_ADOLinkCl;

type
     TRiepilogoCandForm = class(TForm)
          Label2: TLabel;
          ECognome: TEdit;
          ENome: TEdit;
          DEDataNascita: TDateEdit97;
          Label3: TLabel;
          Label4: TLabel;
          CBSesso: TComboBox;
          Label7: TLabel;
          EDiploma1: TEdit;
          Label9: TLabel;
          EArea1: TEdit;
          ERuolo1: TEdit;
          Label10: TLabel;
          EArea2: TEdit;
          ERuolo2: TEdit;
          Label11: TLabel;
          EArea3: TEdit;
          ERuolo3: TEdit;
          Shape1: TShape;
          Label12: TLabel;
          ESettore1: TEdit;
          EAzienda1: TEdit;
          Label13: TLabel;
          ESettore2: TEdit;
          EAzienda2: TEdit;
          Label14: TLabel;
          ESettore3: TEdit;
          EAzienda3: TEdit;
          BSelDiploma: TBitBtn;
          BSelRuolo1: TBitBtn;
          BSelRuolo2: TBitBtn;
          BSelRuolo3: TBitBtn;
          BSelAzienda1: TBitBtn;
          BSelAzienda2: TBitBtn;
          BSelAzienda3: TBitBtn;
          Label15: TLabel;
          ELingua1: TEdit;
          BSelLingua1: TBitBtn;
          Label16: TLabel;
          Label17: TLabel;
          ELingua2: TEdit;
          BSelLingua2: TBitBtn;
          Label18: TLabel;
          Label19: TLabel;
          ELingua3: TEdit;
          BSelLingua3: TBitBtn;
          Label20: TLabel;
          Label21: TLabel;
          ELingua4: TEdit;
          BSelLingua4: TBitBtn;
          Label22: TLabel;
          Shape2: TShape;
          Shape3: TShape;
          Shape4: TShape;
          Shape5: TShape;
          Panel1: TPanel;
          BRegistra: TToolbarButton97;
          BEsci: TToolbarButton97;
          BAltraScheda: TToolbarButton97;
          Image1: TImage;
          DsStati: TDataSource;
          DBGrid1: TDBGrid;
          Timer1: TTimer;
          CBRuoloAtt1: TCheckBox;
          CBRuoloAtt2: TCheckBox;
          CBRuoloAtt3: TCheckBox;
          CBEspAtt1: TCheckBox;
          CBEspAtt2: TCheckBox;
          CBEspAtt3: TCheckBox;
          DsQCVAnnData: TDataSource;
          SpeedButton1: TSpeedButton;
          PanAnnuncio: TPanel;
          Label32: TLabel;
          Label37: TLabel;
          Label48: TLabel;
          Label49: TLabel;
          Label55: TLabel;
          Label57: TLabel;
          DBEdit30: TDBEdit;
          DBEdit36: TDBEdit;
          DBEdit39: TDBEdit;
          DBEdit37: TDBEdit;
          Label27: TLabel;
          GroupBox1: TGroupBox;
          Label5: TLabel;
          Label6: TLabel;
          Label24: TLabel;
          EComuneDom: TEdit;
          EProv: TEdit;
          BitBtn1: TBitBtn;
          ECapDom: TEdit;
          Label28: TLabel;
          ETelefoni: TEdit;
          Label1: TLabel;
          Label29: TLabel;
          ECellulare: TEdit;
          EEmail: TEdit;
          Label23: TLabel;
          EProvInteresse: TEdit;
          Label31: TLabel;
          Label33: TLabel;
          EInquadramento: TEdit;
          ERetribuzione: TEdit;
          Label25: TLabel;
          LUtente: TLabel;
          BCompetenze: TToolbarButton97;
          GBAltriDati1: TGroupBox;
          CBMobilita: TCheckBox;
          CBCassaInteg: TCheckBox;
          CBDispPartTime: TCheckBox;
          CBDispInterinale: TCheckBox;
          CBCategProtette: TCheckBox;
          BCategProtette: TSpeedButton;
          CBPropCV: TGroupBox;
          CBPropMio: TCheckBox;
          EPropCV: TEdit;
          BPropCV: TSpeedButton;
          Label26: TLabel;
          ETelUfficio: TEdit;
          CBAzienda1: TCheckBox;
          CBAzienda2: TCheckBox;
          CBAzienda3: TCheckBox;
          BitBtn2: TBitBtn;
          SEDal1: TRxSpinEdit;
          SEAl1: TRxSpinEdit;
          SEDal2: TRxSpinEdit;
          SEAl2: TRxSpinEdit;
          SEDal3: TRxSpinEdit;
          SEAl3: TRxSpinEdit;
          RGincontrato: TRadioGroup;
          BIncontro: TSpeedButton;
          LBenefits: TLabel;
          EBenefits: TEdit;
          LNumCV: TLabel;
          GBProvCV: TGroupBox;
          CBProvCV: TComboBox;
          iDubbioRuolo1: TImageBox;
          ImageList1: TImageList;
          iDubbioRuolo2: TImageBox;
          iDubbioRuolo3: TImageBox;
          BEmailAnn: TToolbarButton97;
          DsQLivLingue1: TDataSource;
          DsQLivLingue2: TDataSource;
          DsQLivLingue3: TDataSource;
          DsQLivLingue4: TDataSource;
          DBGrid2: TDBGrid;
          DBGrid3: TDBGrid;
          DBGrid4: TDBGrid;
          DBGrid5: TDBGrid;
          CBLibPrivacy: TCheckBox;
          CbMaster: TCheckBox;
          Label8: TLabel;
          EArea: TEdit;
          EAreaSettore1: TEdit;
          EAreaSettore2: TEdit;
          EAreaSettore3: TEdit;
          CbTempMG: TCheckBox;
          DEDataAggCV: TDateEdit97;
          Label30: TLabel;
          EDescPos1: TEdit;
          EDescPos2: TEdit;
          EDescPos3: TEdit;
          BitBtn3: TBitBtn;
          BitBtn4: TBitBtn;
          BitBtn5: TBitBtn;
          ToolbarButton971: TToolbarButton97;
          Panel2: TPanel;
          CBNoCv: TCheckBox;
          TAnag: TADOLinkedTable;
          TCVPagine: TADOLinkedTable;
          TTitoliStudio: TADOLinkedTable;
          TAnagRuoli: TADOLinkedTable;
          TEspLav: TADOLinkedTable;
          TLingueConosc: TADOLinkedTable;
          TStati: TADOLinkedTable;
          TGlobal: TADOLinkedTable;
          TAnagComp: TADOLinkedTable;
          TGlobalLastLogin: TStringField;
          TGlobalLastLoginID: TIntegerField;
          TGlobalUltimoProgRicerca: TSmallintField;
          TGlobalUltimoProgFatt: TSmallintField;
          TGlobalOrarioInizio1: TDateTimeField;
          TGlobalOrarioFine1: TDateTimeField;
          TGlobalOrarioInizio2: TDateTimeField;
          TGlobalOrarioFine2: TDateTimeField;
          TGlobalCheckBoxIndici: TStringField;
          TGlobalIndiciPeso1: TSmallintField;
          TGlobalIndiciPeso2: TSmallintField;
          TGlobalIndiciPeso3: TSmallintField;
          TGlobalIndiciPeso4: TSmallintField;
          TGlobalCoefficiente: TFloatField;
          TGlobalNomeAzienda: TStringField;
          TGlobalSedeAzienda: TStringField;
          TGlobalSettoreAzienda: TStringField;
          TGlobalComuneAzienda: TStringField;
          TGlobalAppoggioBancario1: TStringField;
          TGlobalAppoggioBancario2: TStringField;
          TGlobalAppoggioBancario3: TStringField;
          TGlobalggDiffDi: TSmallintField;
          TGlobalggDiffUc: TSmallintField;
          TGlobalggDiffCol: TSmallintField;
          TGlobalggDiffDataIns: TSmallintField;
          TGlobalggDiffDataUltimoContatto: TSmallintField;
          TGlobalggDiffApRic: TSmallintField;
          TGlobalggDiffUcRic: TSmallintField;
          TGlobalggDiffColRic: TSmallintField;
          TGlobalNazioneAbbAzienda: TStringField;
          TGlobalDirFileReports: TStringField;
          TGlobalOggettoMailreply: TStringField;
          TGlobalTestoMailReply: TMemoField;
          TGlobalVersione: TStringField;
          TGlobalCheckRicCandCV: TBooleanField;
          TGlobalCheckRicCandTel: TBooleanField;
          TGlobalAddAnnoRic: TBooleanField;
          TGlobalUltimoProgOfferta: TSmallintField;
          TGlobalUltimoProgAnnuncio: TSmallintField;
          QCheckCVNum: TADOLinkedQuery;
          QCheckNomi: TADOLinkedQuery;
          Q2: TADOLinkedQuery;
          Q: TADOLinkedQuery;
          Q1: TADOLinkedQuery;
          QCVAnnData: TADOLinkedQuery;
          QCompRich: TADOLinkedQuery;
          QLivLingue1: TADOLinkedQuery;
          QLivLIngue2: TADOLinkedQuery;
          QLivLIngue3: TADOLinkedQuery;
          QLivLIngue4: TADOLinkedQuery;
          qMasAnag: TADOLinkedQuery;
          qMasLingue: TADOLinkedQuery;
          qMasTitoliStudio: TADOLinkedQuery;
          qMasAnagRuoli: TADOLinkedQuery;
          qMasEspLav: TADOLinkedQuery;
          qMasMaster: TADOLinkedQuery;
          qMasAnagAltriDati: TADOLinkedQuery;
          BSalva: TToolbarButton97;
          BModifica: TToolbarButton97;
          BAnnulla: TToolbarButton97;
          TGlobalDirFileDoc: TStringField;
          TGlobalDirFileCV: TStringField;
          procedure BSelRuolo1Click(Sender: TObject);
          procedure BSelRuolo2Click(Sender: TObject);
          procedure BSelRuolo3Click(Sender: TObject);
          procedure BSelAzienda1Click(Sender: TObject);
          procedure BSelLingua2Click(Sender: TObject);
          procedure BSelLingua1Click(Sender: TObject);
          procedure BSelAzienda2Click(Sender: TObject);
          procedure BSelAzienda3Click(Sender: TObject);
          procedure BSelDiplomaClick(Sender: TObject);
          procedure BSelLingua3Click(Sender: TObject);
          procedure BSelLingua4Click(Sender: TObject);
           procedure FormShow(Sender: TObject);
          procedure ASG1CanEditCell(Sender: TObject; Arow, Acol: Integer;
               var canedit: Boolean);
          //procedure BRegistraClick(Sender: TObject);

          procedure BAltraSchedaClick(Sender: TObject);
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
          procedure BitBtn1Click(Sender: TObject);
          procedure Timer1Timer(Sender: TObject);
          procedure CBRuoloAtt1Click(Sender: TObject);
          procedure CBRuoloAtt2Click(Sender: TObject);
          procedure CBRuoloAtt3Click(Sender: TObject);
          procedure CBEspAtt1Click(Sender: TObject);
          procedure CBEspAtt2Click(Sender: TObject);
          procedure CBEspAtt3Click(Sender: TObject);
          procedure SpeedButton1Click(Sender: TObject);
          procedure ECognomeChange(Sender: TObject);
          procedure BCompetenzeClick(Sender: TObject);
          procedure BCategProtetteClick(Sender: TObject);
          procedure CBCategProtetteClick(Sender: TObject);
          procedure CBPropMioClick(Sender: TObject);
          procedure BPropCVClick(Sender: TObject);
          procedure ENomeExit(Sender: TObject);
          procedure ECellulareExit(Sender: TObject);
          procedure BitBtn2Click(Sender: TObject);
          procedure RGincontratoClick(Sender: TObject);
          procedure BIncontroClick(Sender: TObject);
          procedure CBProvCVDropDown(Sender: TObject);
          procedure iDubbioRuolo1Click(Sender: TObject);
          procedure iDubbioRuolo2Click(Sender: TObject);
          procedure iDubbioRuolo3Click(Sender: TObject);
          procedure BEmailAnnClick(Sender: TObject);
          procedure BitBtn3Click(Sender: TObject);
          procedure BitBtn4Click(Sender: TObject);
          procedure BitBtn5Click(Sender: TObject);
          procedure ToolbarButton971Click(Sender: TObject);
          procedure BSalvaClick(Sender: TObject);
          procedure BModificaClick(Sender: TObject);
          procedure BAnnullaClick(Sender: TObject);
          procedure BEsciClick(Sender: TObject);
          procedure DEDataNascitaEnter(Sender: TObject);

     private
          // "variabili" dipendenti dal cliente (da tabella GLOBAL)
          cTitolo: string;
          cCVNumAutomatico, cAltriDati1, cPropCV: boolean;
          xRegistrato: boolean;
          xOraInizio, xOraFine: TDateTime;
          xArrayIDComp: array[1..100] of integer;
          procedure Log_Operation(xIDUtente: integer; xTabella: string; xKeyValue: integer; xOperation: string);
          function GetDocPath: string;
     public
          xIDDiploma1, xIDDiploma2, xIDRuolo1, xIDRuolo2, xIDRuolo3, xIDArea1, xIDArea2, xIDArea3, xIDAnnData,
               xIDAzienda1, xIDAzienda2, xIDAzienda3, xIDLingua1, xIDLingua2, xIDLingua3, xIDLingua4,
               xIDAzSettore1, xIDAzSettore2, xIDAzSettore3, xIDComuneDom, xIDZonaDom, xIDZonaInt,
               xIDAreaSettore1, xIDAreaSettore2, xIDAreaSettore3: integer;
          xTipoStrada, xIndirizzo, xNumCivico, xNazione: string;
          xIDUtente, xIDCliente, xIDRicerca, xIDMansione, xIDEmailAnnunciRic: integer;
          xIDTipoDiploma1, xIDTipoDiploma2: string;
          xDescAttivitaAz1, xDescAttivitaAz2, xDescAttivitaAz3: string;
          xUltimaProv, xFileExt: string;
          xDataIncontro, xDataRicEMail: TDateTime;
          xIDUtenteIncontro, xIDFileAttach: integer;
          xDaEmail: boolean;
          xFILEIDTipo: integer;
          xFILEDescrizione, xFILENome: string;
          xIDCandidato: string;
          xEspLavAtt1, xEspLavAtt2, xEspLavAtt3: boolean;
          xRuoloAtt1, xRuoloAtt2, xRuoloAtt3: boolean;
          xEspAtt1, xEspAtt2, xEspAtt3: boolean;
          xLibPrivacy, xTempMG, xNoCV: boolean;
          xRecLingua1, xRecLingua2, xRecLingua3, xRecLingua4, xRecTitoliStudio1,
               xRecAnagRuolo1, xRecAnagRuolo2, xRecAnagRuolo3, xRecEspLav1, xRecEspLav2, xRecEspLav3,
               xRecMaster: integer;
          xnoteAZ: string;

     end;

var
     RiepilogoCandForm: TRiepilogoCandForm;

implementation

uses SelDiploma, InsRuolo, Lingue, EspLav, Comuni, Competenze,
     SelFromQuery, AnagFile, ModuloDati, Splash3, Indirizzo, Main, CercaAnnuncio {,CercaAnnuncio,
     ,Zone,Invalidita,Clienti,Indirizzo,Incontro,
     EmailAnnunci,SceltaCandidato,};

{$R *.DFM}

procedure TRiepilogoCandForm.BSelRuolo1Click(Sender: TObject);
var xRow: integer;
begin
     InsRuoloForm := TInsRuoloForm.create(self);
     InsRuoloForm.ShowModal;
     if InsRuoloForm.ModalResult = mrOK then begin
          xIDRuolo1 := InsRuoloForm.TRuoli.FieldByName('ID').AsInteger;
          xIDArea1 := InsRuoloForm.TRuoli.FieldByName('IDArea').ASInteger;
          EArea1.text := InsRuoloForm.TRuoli.FieldByName('Area').AsString;
          ERuolo1.Text := InsRuoloForm.TRuoli.FieldByName('Ruolo').AsString;
          // riempimento griglia competenze richieste
     end else begin
          xIDRuolo1 := 0;
          xIDArea1 := 0;
          EArea1.text := '';
          ERuolo1.Text := '';
          iDubbioRuolo1.ImageIndex := 2;
     end;
     InsRuoloForm.Free;
end;

procedure TRiepilogoCandForm.BSelRuolo2Click(Sender: TObject);
begin
     InsRuoloForm := TInsRuoloForm.create(self);
     InsRuoloForm.ShowModal;
     if InsRuoloForm.ModalResult = mrOK then begin
          xIDRuolo2 := InsRuoloForm.TRuoli.FieldByName('ID').AsInteger;
          xIDArea2 := InsRuoloForm.TRuoli.FieldByName('IDArea').ASInteger;
          EArea2.text := InsRuoloForm.TRuoli.FieldByName('Area').AsString;
          ERuolo2.Text := InsRuoloForm.TRuoli.FieldByName('Ruolo').AsString;
          iDubbioRuolo2.ImageIndex := 0;
     end else begin
          xIDRuolo2 := 0;
          xIDArea2 := 0;
          EArea2.text := '';
          ERuolo2.Text := '';
          iDubbioRuolo2.ImageIndex := 2;
     end;
     InsRuoloForm.Free;
end;

procedure TRiepilogoCandForm.BSelRuolo3Click(Sender: TObject);
begin
     InsRuoloForm := TInsRuoloForm.create(self);
     InsRuoloForm.ShowModal;
     if InsRuoloForm.ModalResult = mrOK then begin
          xIDRuolo3 := InsRuoloForm.TRuoli.FieldByName('ID').AsInteger;
          xIDArea3 := InsRuoloForm.TRuoli.FieldByName('IDArea').AsInteger;
          EArea3.text := InsRuoloForm.TRuoli.FieldByName('Area').AsString;
          ERuolo3.Text := InsRuoloForm.TRuoli.FieldByName('Ruolo').AsString;
          iDubbioRuolo3.ImageIndex := 0;
     end else begin
          xIDRuolo3 := 0;
          xIDArea3 := 0;
          EArea3.text := '';
          ERuolo3.Text := '';
          iDubbioRuolo3.ImageIndex := 2;
     end;
     InsRuoloForm.Free;
end;

procedure TRiepilogoCandForm.BSelAzienda1Click(Sender: TObject);
begin
     EspLavForm := TEspLavForm.create(self);
     EspLavForm.ShowModal;
     if EspLavForm.ModalResult = mrOK then begin
          case EspLavForm.RGOpzione.ItemIndex of
               0: begin
                         // solo settore
                         xIDAzienda1 := 0;
                         EAzienda1.Text := '';
                         xIDAzSettore1 := EspLavForm.TSettori.FieldByName('ID').AsInteger;
                         xIDAreaSettore1 := EspLavForm.TSettori.FieldByName('IDAreaSettore').AsInteger;
                         ESettore1.text := EspLavForm.TSettori.FieldByName('Attivita').AsString;
                         EAreaSettore1.text := EspLavForm.TSettori.FieldByName('AreaSettore').AsString;
                    end;
               1: begin
                         // settore e azienda
                         xIDAzienda1 := EspLavForm.TElencoAziende.FieldByName('ID').AsInteger;
                         xIDAzSettore1 := EspLavForm.TElencoAziende.FieldByName('IDAttivita').AsInteger;
                         xIDAreaSettore1 := EspLavForm.TSettori.FieldByName('IDAreaSettore').AsInteger;
                         ESettore1.text := EspLavForm.TElencoAziende.FieldByName('Attivita').AsString;
                         EAreaSettore1.text := EspLavForm.TSettori.FieldByName('AreaSettore').AsString;
                         EAzienda1.Text := EspLavForm.TElencoAziende.FieldByName('Descrizione').AsString;
                    end;
               2: begin
                         // nessuno dei due
                         xIDAzienda1 := 0;
                         xIDAzSettore1 := 0;
                         xIDAreaSettore1 := 0;
                         ESettore1.text := '(nd)';
                         EAreaSettore1.text := '';
                         EAzienda1.Text := '';
                    end;
          end;
          if EArea1.Text = '' then begin
               EArea1.Text := EspLavForm.TAree.FieldByName('Descrizione').AsString;
               ERuolo1.Text := EspLavForm.TRuoli.FieldByName('Descrizione').AsString;
               xIDArea1 := EspLavForm.TAree.FieldByName('ID').AsInteger;
               xIDRuolo1 := EspLavForm.TRuoli.FieldByName('ID').AsInteger;
          end;
     end else begin
          xIDAzienda1 := 0;
          xIDAzSettore1 := 0;
          xIDAreaSettore1 := 0;
          ESettore1.text := '';
          EAreaSettore1.text := '';
          EAzienda1.Text := '';
     end;
     EspLavForm.Free;

end;

procedure TRiepilogoCandForm.BSelLingua2Click(Sender: TObject);
begin
     LingueForm := TLingueForm.create(self);
     LingueForm.ShowModal;
     if LingueForm.ModalResult = mrOK then begin
          xIDLingua2 := LingueForm.Lingue.FieldByName('ID').AsInteger;
          ELingua2.text := LingueForm.Lingue.FieldByName('Lingua').AsString;
     end else begin
          xIDLingua2 := 0;
          ELingua2.text := '';
     end;
     LingueForm.Free;
end;

procedure TRiepilogoCandForm.BSelLingua1Click(Sender: TObject);
begin
     LingueForm := TLingueForm.create(self);
     LingueForm.ShowModal;
     if LingueForm.ModalResult = mrOK then begin
          xIDLingua1 := LingueForm.Lingue.FieldByName('ID').AsInteger;
          ELingua1.text := LingueForm.Lingue.FieldByName('Lingua').AsString;
     end else begin
          xIDLingua1 := 0;
          ELingua1.text := '';
     end;
     LingueForm.Free;
end;

procedure TRiepilogoCandForm.BSelAzienda2Click(Sender: TObject);
begin
     EspLavForm := TEspLavForm.create(self);
     EspLavForm.ShowModal;
     if EspLavForm.ModalResult = mrOK then begin
          case EspLavForm.RGOpzione.ItemIndex of
               0: begin
                         // solo settore
                         xIDAzienda2 := 0;
                         EAzienda2.Text := '';
                         xIDAzSettore2 := EspLavForm.TSettori.FieldByName('ID').AsInteger;
                         xIDAreaSettore2 := EspLavForm.TSettori.FieldByName('IDAreaSettore').AsInteger;
                         ESettore2.text := EspLavForm.TSettori.FieldByName('Attivita').AsString;
                         EAreaSettore2.text := EspLavForm.TSettori.FieldByName('AreaSettore').AsString;
                    end;
               1: begin
                         // settore e azienda
                         xIDAzienda2 := EspLavForm.TElencoAziende.FieldByName('ID').AsInteger;
                         xIDAzSettore2 := EspLavForm.TElencoAziende.FieldByName('IDAttivita').AsInteger;
                         xIDAreaSettore2 := EspLavForm.TSettori.FieldByName('IDAreaSettore').AsInteger;
                         ESettore2.text := EspLavForm.TElencoAziende.FieldByName('Attivita').AsString;
                         EAreaSettore2.text := EspLavForm.TSettori.FieldByName('AreaSettore').AsString;
                         EAzienda2.Text := EspLavForm.TElencoAziende.FieldByName('Descrizione').AsString;
                    end;
               2: begin
                         // nessuno dei due
                         xIDAzienda2 := 0;
                         xIDAzSettore2 := 0;
                         xIDAreaSettore2 := 0;
                         ESettore2.text := '(nd)';
                         EAreaSettore2.text := '';
                         EAzienda2.Text := '';
                    end;
          end;
          if EArea2.Text = '' then begin
               EArea2.Text := EspLavForm.TAree.FieldByName('Descrizione').AsString;
               ERuolo2.Text := EspLavForm.TRuoli.FieldByName('Descrizione').AsString;
               xIDArea2 := EspLavForm.TAree.FieldByName('ID').AsInteger;
               xIDRuolo2 := EspLavForm.TRuoli.FieldByName('ID').AsInteger;
          end;
     end else begin
          xIDAzienda2 := 0;
          xIDAzSettore2 := 0;
          xIDAreaSettore2 := 0;
          ESettore2.text := '';
          EAreaSettore3.text := '';
          EAzienda2.Text := '';
     end;
     EspLavForm.Free;
end;

procedure TRiepilogoCandForm.BSelAzienda3Click(Sender: TObject);
begin
     EspLavForm := TEspLavForm.create(self);
     EspLavForm.ShowModal;
     if EspLavForm.ModalResult = mrOK then begin
          case EspLavForm.RGOpzione.ItemIndex of
               0: begin
                         // solo settore
                         xIDAzienda3 := 0;
                         EAzienda3.Text := '';
                         xIDAzSettore3 := EspLavForm.TSettori.FieldByName('ID').AsInteger;
                         xIDAreaSettore3 := EspLavForm.TSettori.FieldByName('IDAreaSettore').AsInteger;
                         ESettore3.text := EspLavForm.TSettori.FieldByName('Attivita').AsString;
                         EAreaSettore3.text := EspLavForm.TSettori.FieldByName('AreaSettore').AsString;
                    end;
               1: begin
                         // settore e azienda
                         xIDAzienda3 := EspLavForm.TElencoAziende.FieldByName('ID').AsInteger;
                         xIDAzSettore3 := EspLavForm.TElencoAziende.FieldByName('IDAttivita').AsInteger;
                         xIDAreaSettore3 := EspLavForm.TSettori.FieldByName('IDAreaSettore').AsInteger;
                         ESettore3.text := EspLavForm.TElencoAziende.FieldByName('Attivita').AsString;
                         EAreaSettore3.text := EspLavForm.TSettori.FieldByName('AreaSettore').AsString;
                         EAzienda3.Text := EspLavForm.TElencoAziende.FieldByName('Descrizione').AsString;
                    end;
               2: begin
                         // nessuno dei due
                         xIDAzienda3 := 0;
                         xIDAzSettore3 := 0;
                         xIDAreaSettore3 := 0;
                         ESettore3.text := '(nd)';
                         EAreaSettore3.text := '';
                         EAzienda3.Text := '';
                    end;
          end;
          if EArea3.Text = '' then begin
               EArea3.Text := EspLavForm.TAree.FieldByName('Descrizione').AsString;
               ERuolo3.Text := EspLavForm.TRuoli.FieldByName('Descrizione').AsString;
               xIDArea3 := EspLavForm.TAree.FieldByName('ID').AsInteger;
               xIDRuolo3 := EspLavForm.TRuoli.FieldByName('ID').AsInteger;
          end;
     end else begin
          xIDAzienda3 := 0;
          xIDAzSettore3 := 0;
          xIDAreaSettore3 := 0;
          ESettore3.text := '';
          EAreaSettore3.text := '';
          EAzienda3.Text := '';
     end;
     EspLavForm.Free;
end;

procedure TRiepilogoCandForm.BSelDiplomaClick(Sender: TObject);
begin
     SelDiplomaForm := TSelDiplomaForm.create(self);
     SelDiplomaForm.ShowModal;
     if SelDiplomaForm.ModalResult = mrOK then begin
          xIDDiploma1 := SelDiplomaForm.Diplomi.FieldByName('ID').AsInteger;
          xIDTipoDiploma1 := SelDiplomaForm.Diplomi.FieldByName('Tipo').AsString;
          EDiploma1.text := SelDiplomaForm.Diplomi.FieldByName('Descrizione').AsString;
     end else begin
          xIDDiploma1 := 0;
          xIDTipoDiploma1 := '';
          EDiploma1.text := '';
     end;
     SelDiplomaForm.Free;
end;

procedure TRiepilogoCandForm.BSelLingua3Click(Sender: TObject);
begin
     LingueForm := TLingueForm.create(self);
     LingueForm.ShowModal;
     if LingueForm.ModalResult = mrOK then begin
          xIDLingua3 := LingueForm.Lingue.FieldByName('ID').AsInteger;
          ELingua3.text := LingueForm.Lingue.FieldByName('Lingua').AsString;
     end else begin
          xIDLingua3 := 0;
          ELingua3.text := '';
     end;
     LingueForm.Free;
end;

procedure TRiepilogoCandForm.BSelLingua4Click(Sender: TObject);
begin
     LingueForm := TLingueForm.create(self);
     LingueForm.ShowModal;
     if LingueForm.ModalResult = mrOK then begin
          xIDLingua4 := LingueForm.Lingue.FieldByName('ID').AsInteger;
          ELingua4.text := LingueForm.Lingue.FieldByName('Lingua').AsString;
     end else begin
          xIDLingua4 := 0;
          ELingua4.text := '';
     end;
     LingueForm.Free;
end;


procedure TRiepilogoCandForm.FormShow(Sender: TObject);
var x: integer;
     xVersione: string;
begin
     //Svuota campi
     ECognome.text := '';
     ENome.text := '';
     DEDataNascita.ClearDate(self);
     DEDataAggCV.ClearDate(self);
     CBSesso.Text := 'M';
     xTipoStrada := '';
     xIndirizzo := '';
     xNumCivico := '';
     xNazione := '';
     ECellulare.text := '';
     EEmail.text := '';
     ETelefoni.text := '';
     ETelUfficio.text := '';
     EComuneDom.Text := '';
     ECapDom.Text := '';
     EProv.text := '';
     EDiploma1.text := '';
     CbMaster.Checked := False;
     EArea1.text := '';
     ERuolo1.text := '';
     EDescPos1.text := '';
     CBAzienda1.Checked := True;
     EArea2.text := '';
     ERuolo2.text := '';
     EDescPos2.text := '';
     CBAzienda2.Checked := True;
     EArea3.text := '';
     ERuolo3.text := '';
     EDescPos3.text := '';
     CBAzienda3.Checked := True;
     ESettore1.text := '';
     EAreaSettore1.text := '';
     EAzienda1.text := '';
     ESettore2.text := '';
     EAreaSettore2.text := '';
     EAzienda2.text := '';
     ESettore3.text := '';
     EAreaSettore3.text := '';
     EAzienda3.text := '';
     ELingua1.text := '';
     ELingua2.text := '';
     ELingua3.text := '';
     ELingua4.text := '';
     if QCVAnnData.Active then QCVAnnData.Close;
     EProvInteresse.text := '';
     EInquadramento.text := '';
     ERetribuzione.text := '';
     EBenefits.text := '';
     // propriet� CV
     EPropCV.Text := '';
     EPropCV.Visible := False;
     CBPropMio.Checked := True;
     // altri dati
     CBMobilita.Checked := False;
     CBCassaInteg.Checked := False;
     CBCategProtette.Checked := False;
     CBDispPartTime.Checked := False;
     CBDispInterinale.Checked := False;
     // provenienza CV
     CBProvCV.text := '';

     EArea.Text := '';

     CBRuoloAtt1.Checked := False;
     iDubbioRuolo1.imageindex := 2;
     CBRuoloAtt2.Checked := False;
     iDubbioRuolo2.imageindex := 2;
     CBRuoloAtt3.Checked := False;
     iDubbioRuolo3.imageindex := 2;
     CBEspAtt1.Checked := False;
     CBEspAtt2.Checked := False;
     CBEspAtt3.Checked := False;
     {if Uppercase(copy(TGlobal.FieldByName('NomeAzienda').AsString,1,5))='ERGON' then begin
          CBRuoloAtt1.Checked:=true;
          CBEspAtt1.Checked:=true;
     end;
     SEDAl1.Value:=0; SEAl1.Value:=0;
     SEDAl2.Value:=0; SEAl2.Value:=0;
     SEDAl3.Value:=0; SEAl3.Value:=0;}

     CBLibPrivacy.Checked := False;
     CbTempMG.Checked := False;


     xVersione := 'versione 1.0.0.2';
     //Titolo:='Scheda riassuntiva candidato '+xVersione;
     cTitolo := 'Maschera riepilogativa candidato';
     cCVNumAutomatico := True;
     cAltriDati1 := False;
     cPropCV := False;
     CBCategProtette.Visible := False;
     BCategProtette.Visible := False;
     xUltimaProv := 'MI';

     //Caption:='[I/1] - '+cTitolo;
     Caption := cTitolo;
     if not cCVNumAutomatico then LNumCV.caption := 'N� CV manuale';
     GBAltriDati1.Visible := cAltriDati1;
     CBPropCV.Visible := cPropCV;
     xRegistrato := False;
     TStati.Open;
     TStati.Locate('ID', 28, []);
     xidcandidato := MainForm.xIDAnagrafica;

     with qMasAnag do begin;
          close;
          sql.clear;
          sql.Add('select * from anagrafica');
          sql.add(' where id = ' + xIDCandidato);
          open;
     end;


     // inizializzazione variabili

     {xIDDiploma1:=0;
     xIDRuolo1:=0;
     xIDRuolo2:=0;
     xIDRuolo3:=0;
     xIDArea1:=0;
     xIDArea2:=0;
     xIDArea3:=0;
     xIDAzienda1:=0;
     xIDAzienda2:=0;
     xIDAzienda3:=0;
     xIDLingua1:=0;
     xIDLingua2:=0;
     xIDLingua3:=0;
     xIDLingua4:=0;



     xIDAzSettore1:=0;
     xIDAreaSettore1:=0;
     xIDAzSettore2:=0;
     xIDAreaSettore2:=0;
     xIDAzSettore3:=0;
     xIDAreaSettore3:=0;
     xIDComuneDom:=0;
     xIDZonaDom:=0;
     xIDTipoDiploma1:='';

     xIDUtenteIncontro:=0;
     xIDRicerca:=0;
     xIDMansione:=0;

     xIDFileAttach:=0;
     xFileExt:='';
     xDescAttivitaAz1:='';
     xDescAttivitaAz2:='';
     xDescAttivitaAz3:=''; }




     // Solo lettura
     ECognome.readonly := true;
     ENome.readonly := true;
     DEDataNascita.enabled := false;
     DEDataAggCV.enabled := false;
     CBSesso.Enabled := false;
     ECellulare.readonly := true;
     EEmail.readonly := true;
     ETelefoni.readonly := true;
     ETelUfficio.readonly := true;
     EComuneDom.readonly := true;
     ECapDom.readonly := true;
     EProv.readonly := true;
     EDiploma1.readonly := true;
     CbMaster.enabled := false;
     EArea1.readonly := true;
     ERuolo1.readonly := true;
     EDescPos1.readonly := true;
     CBAzienda1.enabled := false;
     EArea2.readonly := true;
     ERuolo2.readonly := true;
     EDescPos2.readonly := true;
     CBAzienda2.enabled := false;
     EArea3.readonly := true;
     ERuolo3.readonly := true;
     EDescPos3.readonly := true;
     CBAzienda3.enabled := false;
     ESettore1.readonly := true;
     EAreaSettore1.readonly := true;
     EAzienda1.readonly := true;
     ESettore2.readonly := true;
     EAreaSettore2.readonly := true;
     EAzienda2.readonly := true;
     ESettore3.readonly := true;
     EAreaSettore3.readonly := true;
     EAzienda3.readonly := true;
     ELingua1.readonly := true;
     ELingua2.readonly := true;
     ELingua3.readonly := true;
     ELingua4.readonly := true;
     //if QCVAnnData.Active then QCVAnnData.Close;
     EProvInteresse.readonly := true;
     EInquadramento.readonly := true;
     ERetribuzione.readonly := true;
     EBenefits.readonly := true;
     // propriet� CV
     EPropCV.readonly := true;
     EPropCV.Visible := False;
     CBPropMio.enabled := false;
     // altri dati
     {CBMobilita.Checked:=False;
     CBCassaInteg.Checked:=False;
     CBCategProtette.Checked:=False;
     CBDispPartTime.Checked:=False;
     CBDispInterinale.Checked:=False;}
     // provenienza CV
     CBProvCV.text := '';

     EArea.readonly := true;

     //CBRuoloAtt1.Checked:=False;
     iDubbioRuolo1.imageindex := 2;
     //CBRuoloAtt2.Checked:=False;
     iDubbioRuolo2.imageindex := 2;
     //CBRuoloAtt3.Checked:=False;
     iDubbioRuolo3.imageindex := 2;
     //CBEspAtt1.Checked:=False;
     //CBEspAtt2.Checked:=False;
     //CBEspAtt3.Checked:=False;
     CBEspAtt1.Enabled := false;
     CBEspAtt2.Enabled := false;
     CBEspAtt3.Enabled := false;
     CBRuoloAtt1.Enabled := false;
     CBRuoloAtt2.Enabled := false;
     CBRuoloAtt3.Enabled := false;
     CBNoCv.Enabled := false;
     CBTempMG.Enabled := false;
     CBLibPrivacy.Enabled := false;

     BitBtn1.Enabled := false;
     BitBtn2.Enabled := false;
     BSelDiploma.Enabled := false;
     BSelRuolo1.Enabled := false;
     BSelRuolo2.Enabled := false;
     BSelRuolo3.Enabled := false;
     BSelAzienda1.Enabled := false;
     BSelAzienda2.Enabled := false;
     BSelAzienda3.Enabled := false;
     BSelLingua1.Enabled := false;
     BSelLingua2.Enabled := false;
     BSelLingua3.Enabled := false;
     BSelLingua4.Enabled := false;

     {BitBtn3.Enabled:=false;
     BitBtn4.Enabled:=false;
     BitBtn5.Enabled:=false;}


     DBGrid2.Enabled := false;
     DBGrid3.Enabled := false;
     DBGrid4.Enabled := false;
     DBGrid5.Enabled := false;

     BitBtn3.Enabled := false;
     BitBtn4.Enabled := false;
     BitBtn5.Enabled := false;



     {CBLibPrivacy.Checked:=False;
     CbTempMG.Checked:=False;}

    { // inizializzazione variabili
     xTipoStrada:='';
     xIndirizzo:='';
     xNumCivico:='';
     xNazione:='';
     xIDDiploma1:=0;
     xIDDiploma2:=0;
     xIDRuolo1:=0;
     xIDRuolo2:=0;
     xIDRuolo3:=0;
     xIDArea1:=0;
     xIDArea2:=0;
     xIDArea3:=0;
     xIDAzienda1:=0;
     xIDAzienda2:=0;
     xIDAzienda3:=0;
     xIDLingua1:=0;
     xIDLingua2:=0;
     xIDLingua3:=0;
     xIDLingua4:=0;
     xIDZonaInt:=0;
     xIDCliente:=0;
     xIDAnnData:=0;
     xIDAzSettore1:=0;
     xIDAreaSettore1:=0;
     xIDAzSettore2:=0;
     xIDAreaSettore2:=0;
     xIDAzSettore3:=0;
     xIDAreaSettore3:=0;
     xIDComuneDom:=0;
     xIDZonaDom:=0;
     xIDTipoDiploma1:='';
     xIDTipoDiploma2:='';
     xIDUtenteIncontro:=0;
     xIDRicerca:=0;
     xIDMansione:=0;
     xIDEmailAnnunciRic:=0;
     xDaEmail:=False;
     xIDFileAttach:=0;
     xFileExt:='';
     xDescAttivitaAz1:='';
     xDescAttivitaAz2:='';
     xDescAttivitaAz3:='';
     xFILEIDTipo:=0;
     xFILEDescrizione:='';
     xFILENome:='';}




     // livello conoscenza lingue: tutte sul buono
     QlivLIngue1.Open;
     QlivLIngue2.Open;
     QlivLIngue3.Open;
     QlivLIngue4.Open;
     QLivLingue1.Locate('ID', 4, []);
     QLivLingue2.Locate('ID', 4, []);
     QLivLingue3.Locate('ID', 4, []);
     QLivLingue4.Locate('ID', 4, []);

     //Inizializzazione variabili
     xRecMaster := 0;

     //Valorizzazione campi
     Ecognome.Text := qMasAnag.fieldbyname('cognome').Asstring;
     Enome.Text := qMasAnag.fieldbyname('nome').Asstring;
     if qMasAnag.FieldByName('datanascita').AsDateTime <> 0 then
          //DEDataNascita.TExt:=DateToStr(qMasAnag.fieldbyname('datanascita').AsDatetime);
          DEDataNascita.Date := QMasAnag.fieldbyname('datanascita').AsDatetime;
     CBSesso.Text := qMasAnag.fieldbyname('sesso').Asstring;
     ECellulare.text := qMasAnag.fieldbyname('cellulare').AsString;
     Eemail.text := qMasAnag.fieldbyname('Email').AsString;
     ETelefoni.text := qMasAnag.FieldByName('RecapitiTelefonici').AsString;
     ETelUfficio.text := qMasAnag.FieldByName('TelUfficio').AsString;
     if qMasAnag.FieldByName('CVDataAgg').AsDateTime <> 0 then
          DEDataAggCV.Date := qMasAnag.FieldByName('CVDataAgg').AsDateTime;
     xLibPrivacy := qMasAnag.FieldByName('LibPrivacy').AsBoolean;
     if xLibPrivacy then
          CBLibPrivacy.checked := true
     else
          CBLibPrivacy.checked := false;

     //Indirizzo
     xIDComuneDom := qMasAnag.fieldbyname('IDComuneDom').AsInteger;
     Q1.Close;
     //Q1.SQL.text := 'select * from TabCom,zone where TabCom.IDZona *= Zone.ID and TabCom.ID=' + inttostr(xIdComuneDom); //qMasAnag.fieldbyname('IDComuneDom').AsString;
     //Modifica Query da Thomas
     Q1.SQL.text :=  'select * from TabCom left join zone on TabCom.IDZona = Zone.ID where TabCom.ID=' + inttostr(xIdComuneDom);
     Q1.Open;
     EComuneDom.text := Q1.FieldByName('Descrizione').asString;
     ECapDom.text := Q1.FieldByName('CAP').asString;
     Eprov.text := Q1.FieldByName('Prov').asString;
     EArea.Text := Q1.FieldByName('AreaNielsen').asstring + ' ' + Q1.FieldByName('Regione').AsString;

     xTipoStrada := qMasAnag.fieldbyname('DomiciliotipoStrada').asString;
     xIndirizzo := qMasAnag.fieldbyname('DomicilioIndirizzo').asString;
     xNumCivico := qMasAnag.fieldbyname('DomicilioNumCivico').asString;
     xNazione := qMasAnag.fieldbyname('DomicilioStato').asString;
     // Lingue
     qMasLingue.Close;
     qMasLingue.SQL.Text := 'select * from lingueconosciute where idAnagrafica =' + xidcandidato;
     qMasLingue.open;

     qMasLingue.First;
     if qMasLingue.RecordCount > 0 then begin
          for x := 1 to 4 do begin
               if x <= qMasLingue.RecordCount then begin
                    if x = 1 then begin
                         xRecLingua1 := qMasLingue.fieldbyname('id').AsInteger;
                         xIDLingua1 := qMasLingue.fieldbyname('IDLingua').AsInteger;
                         Q1.Close;
                         Q1.SQL.text := 'select * from Lingue where ID=' + inttostr(xIdLingua1); //qMasAnag.fieldbyname('IDComuneDom').AsString;
                         Q1.Open;
                         Elingua1.Text := Q1.fieldByName('Lingua').AsString;
                         QLivLingue1.Open;
                         QLivLingue1.Locate('ID', qMasLingue.fieldByName('Livello').AsString, []);
                    end;
                    if x = 2 then begin
                         xRecLingua2 := qMasLingue.fieldbyname('id').AsInteger;
                         xIDLingua2 := qMasLingue.fieldbyname('IDLingua').AsInteger;
                         Q1.Close;
                         Q1.SQL.text := 'select * from Lingue where ID=' + inttostr(xIdLingua2); //qMasAnag.fieldbyname('IDComuneDom').AsString;
                         Q1.Open;
                         Elingua2.Text := Q1.fieldByName('Lingua').AsString;
                         QLivLingue2.Open;
                         QLivLingue2.Locate('ID', qMasLingue.fieldByName('Livello').AsString, []);
                    end;
                    if x = 3 then begin
                         xRecLingua3 := qMasLingue.fieldbyname('id').AsInteger;
                         xIDLingua3 := qMasLingue.fieldbyname('IDLingua').AsInteger;
                         Q1.Close;
                         Q1.SQL.text := 'select * from Lingue where ID=' + inttostr(xIdLingua3); //qMasAnag.fieldbyname('IDComuneDom').AsString;
                         Q1.Open;
                         Elingua3.Text := Q1.fieldByName('Lingua').AsString;
                         QLivLingue3.Open;
                         QLivLingue3.Locate('ID', qMasLingue.fieldByName('Livello').AsString, []);
                    end;
                    if x = 4 then begin
                         xRecLingua4 := qMasLingue.fieldbyname('id').AsInteger;
                         xIDLingua4 := qMasLingue.fieldbyname('IDLingua').AsInteger;
                         Q1.Close;
                         Q1.SQL.text := 'select * from Lingue where ID=' + inttostr(xIdLingua4); //qMasAnag.fieldbyname('IDComuneDom').AsString;
                         Q1.Open;
                         Elingua4.Text := Q1.fieldByName('Lingua').AsString;
                         QLivLingue4.Open;
                         QLivLingue4.Locate('ID', qMasLingue.fieldByName('Livello').AsString, []);
                    end;
                    qMasLingue.Next;
               end
               else
          end;
     end;
     //Titolo di Studio
     qMasTitoliStudio.Close;
     qMasTitoliStudio.SQL.Text := 'select titolistudio.*, diplomi.descrizione from titolistudio, diplomi where titolistudio.iddiploma = diplomi.id and idanagrafica = ' + xidcandidato;
     qMasTitoliStudio.open;
     xRecTitoliStudio1 := qMasTitoliStudio.FieldByName('id').ASInteger;
     EDiploma1.text := qMasTitoliStudio.FieldByName('descrizione').AsString;
     //Master
     qMasMaster.close;
     qMasMaster.SQL.Text := 'select * from corsiextra where idanagrafica = ' + xIDCandidato + 'and Tipologia = ' + '''Master ''';
     qMasMaster.Open;
     xRecMaster := qMasMaster.fieldbyname('id').AsInteger;
     if xrecmaster > 0 then
          CbMaster.checked := true
     else
          CBMaster.Checked := false;

     //Esperienze Lavorative
     qMasEspLav.Close;
     qMasEspLav.SQL.Text := 'select * from esperienzelavorative where idanagrafica = ' + xIDCandidato;
     qMasEspLav.Open;

     qMasEspLav.First;

     if qMasEspLav.RecordCount > 0 then begin
          for x := 1 to 3 do begin
               if x <= qMasEspLav.RecordCount then begin
                    if x = 1 then begin
                         xRecEspLav1 := qMasEspLav.fieldbyname('ID').AsInteger;
                         xIDAzSettore1 := qMasEspLav.fieldbyname('IDSettore').AsInteger;
                         xIDAreaSettore1 := qMasEspLav.fieldbyname('IDAreaSettore').AsInteger;
                         xIDAzienda1 := qMasEspLav.fieldbyname('IDAzienda').AsInteger;
                         xEspAtt1 := qMasEspLav.FieldByName('Attuale').AsBoolean;
                         xIDArea1 := qMasEspLav.fieldbyname('IDArea').AsInteger;
                         xIDRuolo1 := qMasEspLav.fieldbyname('IDMansione').AsInteger;
                         xDescAttivitaAz1 := qMasEspLav.fieldbyname('DescrizioneAttivitaAz').AsString;
                         //Azienda Attuale
                         if xEspAtt1 then
                              CBEspAtt1.Checked := true
                         else
                              CBEspAtt1.Checked := False;
                         //Descrizione Posizione
                         EDescPos1.text := qMasEspLav.FieldByName('DescrizioneMansione').Asstring;
                         //Settore Azienda
                         Q1.Close;
                         Q1.SQL.text := 'select * from EBC_Attivita where ID=' + inttostr(xIDAzSettore1);
                         Q1.Open;
                         if xIdAzSettore1 <> 0 then
                              ESettore1.Text := Q1.fieldByName('Attivita').AsString
                         else
                              ESettore1.Text := '(nd)';
                         //Area Settore
                         Q1.Close;
                         Q1.SQL.text := 'select * from Areesettori where ID=' + inttostr(xIDAreaSettore1);
                         Q1.Open;
                         EAreaSettore1.Text := Q1.fieldByName('AreaSettore').AsString;

                         //Nome Azienda
                         Q1.Close;
                         Q1.SQL.text := 'select * from EBC_Clienti where ID=' + inttostr(xIDAzienda1);
                         Q1.Open;
                         EAzienda1.Text := Q1.fieldByName('Descrizione').AsString;
                         //Area
                         Q1.Close;
                         Q1.SQL.text := 'select * from Aree where ID=' + inttostr(xIdArea1);
                         Q1.Open;
                         EArea1.Text := Q1.fieldByName('Descrizione').AsString;
                         //Ruolo
                         Q1.Close;
                         Q1.SQL.text := 'select * from Mansioni where ID=' + inttostr(xIDRuolo1);
                         Q1.Open;
                         ERuolo1.Text := Q1.fieldByName('Descrizione').AsString;
                    end;
                    if x = 2 then begin
                         xRecEspLav2 := qMasEspLav.fieldbyname('ID').AsInteger;
                         xIDAzSettore2 := qMasEspLav.fieldbyname('IDSettore').AsInteger;
                         xIDAreaSettore2 := qMasEspLav.fieldbyname('IDAreaSettore').AsInteger;
                         xIDAzienda2 := qMasEspLav.fieldbyname('IDAzienda').AsInteger;
                         xEspAtt2 := qMasEspLav.FieldByName('Attuale').AsBoolean;
                         xIDArea2 := qMasEspLav.fieldbyname('IDArea').AsInteger;
                         xIDRuolo2 := qMasEspLav.fieldbyname('IDMansione').AsInteger;
                         xDescAttivitaAz2 := qMasEspLav.fieldbyname('DescrizioneAttivitaAz').AsString;
                         //Azienda Attuale
                         if xEspAtt2 = true then
                              CBEspAtt2.Checked := true
                         else
                              CBEspAtt2.Checked := False;
                         //Descrizione Posizione
                         EDescPos2.text := qMasEspLav.FieldByName('DescrizioneMansione').Asstring;
                         //Settore Azienda
                         Q1.Close;
                         Q1.SQL.text := 'select * from EBC_Attivita where ID=' + inttostr(xIDAzSettore2);
                         Q1.Open;
                         if xIdAzSettore2 <> 0 then
                              ESettore2.Text := Q1.fieldByName('Attivita').AsString
                         else
                              ESettore2.Text := '(nd)';
                         //Area Settore
                         Q1.Close;
                         Q1.SQL.text := 'select * from Areesettori where ID=' + inttostr(xIDAreaSettore2);
                         Q1.Open;
                         EAreaSettore2.Text := Q1.fieldByName('AreaSettore').AsString;
                         EAreaSettore2.Text := Q1.fieldByName('AreaSettore').AsString;
                         //Nome Azienda
                         Q1.Close;
                         Q1.SQL.text := 'select * from EBC_Clienti where ID=' + inttostr(xIDAzienda2);
                         Q1.Open;
                         EAzienda2.Text := Q1.fieldByName('Descrizione').AsString;
                         //Ruolo
                         Q1.Close;
                         Q1.SQL.text := 'select * from Aree where ID=' + inttostr(xIdArea2);
                         Q1.Open;
                         EArea2.Text := Q1.fieldByName('Descrizione').AsString;
                         //Area
                         Q1.Close;
                         Q1.SQL.text := 'select * from Mansioni where ID=' + inttostr(xIDRuolo2);
                         Q1.Open;
                         ERuolo2.Text := Q1.fieldByName('Descrizione').AsString;
                    end;
                    if x = 3 then begin
                         xRecEspLav3 := qMasEspLav.fieldbyname('ID').AsInteger;
                         xIDAzSettore3 := qMasEspLav.fieldbyname('IDSettore').AsInteger;
                         xIDAreaSettore3 := qMasEspLav.fieldbyname('IDAreaSettore').AsInteger;
                         xIDAzienda3 := qMasEspLav.fieldbyname('IDAzienda').AsInteger;
                         xEspAtt3 := qMasEspLav.FieldByName('Attuale').AsBoolean;
                         xIDArea3 := qMasEspLav.fieldbyname('IDArea').AsInteger;
                         xIDRuolo3 := qMasEspLav.fieldbyname('IDMansione').AsInteger;
                         xDescAttivitaAz3 := qMasEspLav.fieldbyname('DescrizioneAttivitaAz').AsString;
                         //Azienda Attuale
                         if xEspLavAtt3 then
                              CBEspAtt3.Checked := true
                         else
                              CBEspAtt3.Checked := False;
                         //Descrizione Posizione
                         EDescPos3.text := qMasEspLav.FieldByName('DescrizioneMansione').Asstring;
                         //Settore Azienda
                         Q1.Close;
                         Q1.SQL.text := 'select * from EBC_Attivita where ID=' + inttostr(xIDAzSettore3);
                         Q1.Open;
                         if xIdAzSettore3 <> 0 then
                              ESettore3.Text := Q1.fieldByName('Attivita').AsString
                         else
                              ESettore3.Text := '(nd)';
                         //Area Settore
                         Q1.Close;
                         Q1.SQL.text := 'select * from Areesettori where ID=' + inttostr(xIDAreaSettore3);
                         Q1.Open;
                         EAreaSettore3.Text := Q1.fieldByName('AreaSettore').AsString;
                         //Nome Azienda
                         Q1.Close;
                         Q1.SQL.text := 'select * from EBC_Clienti where ID=' + inttostr(xIDAzienda3);
                         Q1.Open;
                         EAzienda3.Text := Q1.fieldByName('Descrizione').AsString;
                         //Area
                         Q1.Close;
                         Q1.SQL.text := 'select * from Aree where ID=' + inttostr(xIdArea3);
                         Q1.Open;
                         EArea3.Text := Q1.fieldByName('Descrizione').AsString;
                         //Ruolo
                         Q1.Close;
                         Q1.SQL.text := 'select * from Mansioni where ID=' + inttostr(xIDRuolo3);
                         Q1.Open;
                         ERuolo3.Text := Q1.fieldByName('Descrizione').AsString;
                    end;
                    qMasEspLav.Next;
               end
               else
          end;
     end;

     //Ruoli
     {qMasAnagRuoli.Close;
     qMasAnagRuoli.SQL.Text:='select * from anagmansioni where idanagrafica = '+xIDCandidato;
     qMasAnagRuoli.Open;

     qMasAnagRuoli.First;
     if qMasAnagRuoli.RecordCount>0 then begin
          for x:=1 to 3 do begin
               if x<=qMasAnagRuoli.RecordCount then begin
                    if x=1 then begin
                         xRecAnagRuolo1:=qMasAnagRuoli.fieldbyname('ID').AsInteger;
                         xIDArea1:=qMasEspLav.fieldbyname('IDArea').AsInteger;
                         xIDRuolo1:=qMasEspLav.fieldbyname('IDMansione').AsInteger;
                         xRuoloAtt1:=qMasAnagRuoli.fieldbyname('Attuale').ASBoolean;
                         if xEspAtt1 then
                              CBRuoloAtt1.Checked:=true
                         else
                              CBRuoloAtt1.Checked:=false;
                         Q1.Close;
                         Q1.SQL.text:='select * from Aree where ID='+inttostr(xIdArea1);
                         Q1.Open;
                         EArea1.Text:=Q1.fieldByName('Descrizione').AsString;
                         Q1.Close;
                         Q1.SQL.text:='select * from Mansioni where ID='+inttostr(xIDRuolo1);
                         Q1.Open;
                         ERuolo1.Text:=Q1.fieldByName('Descrizione').AsString;

                    end;
                    if x=2 then begin
                         xRecAnagRuolo2:=qMasAnagRuoli.fieldbyname('ID').AsInteger;
                         {xIDArea2:=qMasEspLav.fieldbyname('IDArea').AsInteger;
                         xIDRuolo2:=qMasEspLav.fieldbyname('IDMansione').AsInteger;
                         xRuoloAtt2:=qMasAnagRuoli.fieldbyname('Attuale').ASBoolean;
                         if xEspAtt2 then
                              CBRuoloAtt2.Checked:=true
                         else
                              CBRuoloAtt2.Checked:=false;
                         Q1.Close;
                         Q1.SQL.text:='select * from Aree where ID='+inttostr(xIdArea2);
                         Q1.Open;
                         EArea2.Text:=Q1.fieldByName('Descrizione').AsString;
                         Q1.Close;
                         Q1.SQL.text:='select * from Mansioni where ID='+inttostr(xIDRuolo2);
                         Q1.Open;
                         ERuolo2.Text:=Q1.fieldByName('Descrizione').AsString;
                    end;
                    if x=3 then begin
                         xRecAnagRuolo3:=qMasAnagRuoli.fieldbyname('ID').AsInteger;
                         {xIDArea3:=qMasEspLav.fieldbyname('IDArea').AsInteger;
                         xIDRuolo3:=qMasEspLav.fieldbyname('IDMansione').AsInteger;
                         xRuoloAtt3:=qMasAnagRuoli.fieldbyname('Attuale').ASBoolean;
                         if xEspAtt3 then
                              CBRuoloAtt3.Checked:=true
                         else
                              CBRuoloAtt3.Checked:=false;
                         Q1.Close;
                         Q1.SQL.text:='select * from Aree where ID='+inttostr(xIdArea3);
                         Q1.Open;
                         EArea3.Text:=Q1.fieldByName('Descrizione').AsString;
                         Q1.Close;
                         Q1.SQL.text:='select * from Mansioni where ID='+inttostr(xIDRuolo3);
                         Q1.Open;
                         ERuolo3.Text:=Q1.fieldByName('Descrizione').AsString;
                    end;
                    qMasAnagRuoli.Next;
               end
               else
          end;
     end; }


     //AnagAltriDati
     qMasAnagAltriDati.Close;
     qMasAnagAltriDati.SQL.Text := 'select * from anagaltreinfo where idanagrafica = ' + xIDCandidato;
     qMasAnagAltriDati.open;
     EProvInteresse.Text := qMasAnagAltriDati.FieldByName('ProvInteresse').ASString;
     EInquadramento.Text := qMasAnagAltriDati.FieldByName('Inquadramento').ASString;
     ERetribuzione.Text := qMasAnagAltriDati.FieldByName('Retribuzione').ASString;
     EBenefits.Text := qMasAnagAltriDati.FieldByName('Benefits').ASString;
     xTempMG := qMasAnagAltriDati.FieldByName('TemporaryMG').ASBoolean;
     if xtempMG then
          CbTempMG.checked := true
     else
          CbTempMG.Checked := false;
     xNoCV := qMasAnagAltriDati.FieldByName('NoCV').ASBoolean;
     if xNoCV then
          CBNoCv.checked := true
     else
          CBNoCv.checked := false;
end;

procedure TRiepilogoCandForm.ASG1CanEditCell(Sender: TObject; Arow,
     Acol: Integer; var canedit: Boolean);
begin
     if (acol = 0) or (acol = 1) then canedit := False;
end;

{procedure TRiepilogoCandForm.BRegistraClick(Sender: TObject);
var xVai,xVai2: boolean;
     xLettera: char;
     xOra,xMin,xSec,xMsec: Word;
     h,k,xIDAnag: integer;
     {xRuoloAtt1,xRuoloAtt2,xRuoloAtt3: boolean;
     xEspAtt1,xEspAtt2,xEspAtt3, xNumeroValido: boolean;
     xDicDubbi,xCVNumero,xExtTemp: string;
begin
     xVai:=True;
     // - controllo nome e cognomi nulli
     if (ECognome.Text='')or(ENome.Text='') then begin
          MessageDlg('Il soggetto non pu� avere cognome o nome vuoti',mtError, [mbOK],0);
          xVai:=False;
     end;
     // - controllo omonimia
     QCheckNomi.Close;
     QCheckNomi.ReloadSQL;
     QCheckNomi.ParamByName['xCogn']:=trim(ECognome.Text);
     QCheckNomi.ParamByName['xNome']:=trim(ENome.Text);
     QCheckNomi.Open;
     if QCheckNomi.RecordCount>0 then begin
          if MessageDlg('CONTROLLARE POSSIBILE OMONIMIA con CV n� '+QCheckNomi.FieldByName('CVNumero').asString+chr(13)+
               'Registrare comunque ?',mtError, [mbYes,mbNo],0)=mrYes then
               xVai:=True
          else xVai:=False;
     end;
     // controllo liberatoria privacy
     if not CBLibPrivacy.Checked then begin
          if MessageDlg('LIBERATORIA PRIVACY NON PRESENTE: Proseguire comunque ?',mtError, [mbYes,mbNo],0)=mrYes then
               xVai:=True
          else xVai:=False;
     end;

     if not xVai then exit;

     // NUMERO CV
     if not cCVNumAutomatico then begin
          // manuale
          xCVNumero:='0';
          xNumeroValido:=False;
          while not xNumeroValido do begin
               if not InputQuery('Numero CV manuale','Numero CV:',xCVNumero) then begin
                    xVai:=False;
                    Break;
               end else begin
                    if xCVNumero='' then xCVNumero:='0';
                    if xCVNumero='0' then begin
                         if MessageDlg('ATTENZIONE: Non � possibile inserire un n� CV uguale a ZERO'+chr(13)+
                              'Verr� associato il progressivo autoincrementante interno.'+chr(13)+
                              'Vuoi procedere (Yes) o Annullare l''inserimento (no) ?',mtWarning, [mbYes,mbNo],0)=mrNo then begin
                              xVai:=False;
                              Break;
                         end;
                    end;
                    // controllo esistenza n� CV
                    Q.SQL.text:='select Cognome,Nome from Anagrafica with (updlock) where CVNumero='+xCVNumero;
                    Q.Open;
                    if Q.RecordCount>0 then begin
                         if MessageDlg('ATTENZIONE: esiste gi� il numero CV '+xCVNumero+' abbinato al soggetto:'+chr(13)+
                              Q.FieldByName('Cognome').asString+' '+Q.FieldByName('Nome').asString+chr(13)+chr(13)+
                              'Vuoi procedere (Yes) o Annullare l''inserimento (no) ?',mtWarning, [mbYes,mbNo],0)=mrNo then begin
                              xVai:=False;
                              break;
                         end;
                    end else xNumeroValido:=True;
                    Q.Close;
               end;
          end;
     end else xCVNumero:='0'; // automatico

     // REGISTRAZIONE
     if xVai then begin
          Splash3Form:=TSplash3Form.Create(nil);
          Splash3Form.Show;
          Splash3Form.Update;
          xOraFine:=Now;

          data.DB.BeginTrans;
          try
               if Uppercase(copy(TGlobal.FieldByName('NomeAzienda').AsString,1,7))='ERREMME' then begin
                    // solo per ERREEMME --> residenza e non domicilio
                    Q.SQL.Text:='insert into Anagrafica (CVNumero,Cognome,Nome,Sesso,TipoStrada,Indirizzo,NumCivico,'+
                         'Stato,Comune,Cap,Provincia,IDStato,IDTipoStato,'+
                         'CVInseritoInData,CVIDAnnData,IDComuneDom,IDZonaDom,RecapitiTelefonici,'+
                         'Cellulare,Email,IDUtenteMod,DubbiIns,IDProprietaCV,TelUfficio,LibPrivacy,CVDataAgg';
               end else begin
                    Q.SQL.Text:='insert into Anagrafica (CVNumero,Cognome,Nome,Sesso,DomicilioTipoStrada,DomicilioIndirizzo,DomicilioNumCivico,'+
                         'DomicilioStato,DomicilioComune,DomicilioCap,DomicilioProvincia,IDStato,IDTipoStato,'+
                         'CVInseritoInData,CVIDAnnData,IDComuneDom,IDZonaDom,RecapitiTelefonici,'+
                         'Cellulare,Email,IDUtenteMod,DubbiIns,IDProprietaCV,TelUfficio,LibPrivacy,CVDataAgg';
               end;
               if DEDataNascita.Text<>'' then
                    Q.SQL.Add(',DataNascita');
               Q.SQL.Add(')');
               Q.SQL.Add('values (:xCVNumero:,:xCognome:,:xNome:,:xSesso:,:xDomicilioTipoStrada:,:xDomicilioIndirizzo:,:xDomicilioNumCivico:,'+
                    ':xDomicilioStato:,:xDomicilioComune:,:xDomicilioCap:,:xDomicilioProvincia:,:xIDStato:,:xIDTipoStato:,'+
                    ':xCVInseritoInData:,:xCVIDAnnData:,:xIDComuneDom:,:xIDZonaDom:,:xRecapitiTelefonici:,'+
                    ':xCellulare:,:xEmail:,:xIDUtenteMod:,:xDubbiIns:,:xIDProprietaCV:,:xTelUfficio:,:xLibPrivacy:,:xCVDataAgg:');
               if DEDataNascita.Text<>'' then
                    Q.SQL.Add(',:xDataNascita:');
               Q.SQL.Add(')');
               Q.ParamByName['xCVNumero']:=StrtoInt(xCVNumero);
               Q.ParamByName['xCognome']:=ECognome.Text;
               Q.ParamByName['xNome']:=ENome.Text;
               if DEDataNascita.Text<>'' then
                    Q.ParamByName['xDataNascita']:=DEDataNascita.Date;
               Q.ParamByName['xSesso']:=CBSesso.Text;
               Q.ParamByName['xDomicilioTipoStrada']:=xTipoStrada;
               Q.ParamByName['xDomicilioIndirizzo']:=xIndirizzo;
               Q.ParamByName['xDomicilioNumcivico']:=xNumCivico;
               Q.ParamByName['xDomicilioStato']:=xNazione;
               Q.ParamByName['xDomicilioComune']:=EComuneDom.Text;
               Q.ParamByName['xDomicilioCap']:=ECapDom.Text;
               Q.ParamByName['xDomicilioProvincia']:=EProv.Text;
               Q.ParamByName['xIDStato']:=TStati.FieldByName('ID').AsInteger;
               Q.ParamByName['xIDTipoStato']:=TStati.FieldByName('IDTipoStato').AsInteger;
               Q.ParamByName['xCVInseritoInData']:=Date;
               Q.ParamByName['xCVIDAnnData']:=0; // non pi� utilizzato
               Q.ParamByName['xIDComuneDom']:=xIDComuneDom;
               Q.ParamByName['xIDZonaDom']:=xIDZonaDom;
               Q.ParamByName['xRecapitiTelefonici']:=ETelefoni.text;
               Q.ParamByName['xTelUfficio']:=ETelUfficio.text;
               Q.ParamByName['xCellulare']:=ECellulare.text;
               Q.ParamByName['xEmail']:=EEmail.text;
               Q.ParamByName['xIDUtenteMod']:=xIDUtente;
               Q.ParamByName['xLibPrivacy']:=CBLibPrivacy.Checked;
               if DEDataAggCV.Text<>'' then
                    Q.ParamByName['xCVDataAgg']:=DEDataAggCV.Date
               else Q.ParamByName['xCVDataAgg']:=0;
               // se ci sono dubbi
               xDicDubbi:='';
               if (iDubbioRuolo1.ImageIndex=1)or(iDubbioRuolo2.ImageIndex=1)or(iDubbioRuolo3.ImageIndex=1) then begin
                    if iDubbioRuolo1.ImageIndex=1 then xDicDubbi:='Ruolo 1 ';
                    if iDubbioRuolo2.ImageIndex=1 then xDicDubbi:=xDicDubbi+'Ruolo 2 ';
                    if iDubbioRuolo3.ImageIndex=1 then xDicDubbi:=xDicDubbi+'Ruolo 3 ';
               end;
               Q.ParamByName['xDubbiIns']:=xDicDubbi;
               Q.ParamByName['xIDProprietaCV']:=xIDCliente;
               Q.ExecSQL;

               //  Prendi il valore dell'ID
               Q.SQL.text:='select @@IDENTITY as LastID';
               Q.Open;
               xIDAnag:=Q.FieldByName('LastID').asInteger;
               Q.Close;
               Log_Operation(xIDUtente,'Anagrafica',xIDAnag,'I');

               // ALTRE INFO
               Q.SQL.text:='insert into AnagAltreInfo (IDAnagrafica,CateogorieProtette,Invalidita,PercentualeInvalidita,TemporaryMG,'+
                    'Mobilita,CassaIntegrazione,DispPartTime,DispInterinale,ProvInteresse,Inquadramento,Retribuzione,Benefits,CVProvenienza,NoCV) '+
                    ' values (:xIDAnagrafica:,:xCateogorieProtette:,:xInvalidita:,:xPercentualeInvalidita:,:xTemporaryMG:,'+
                    ':xMobilita:,:xCassaIntegrazione:,:xDispPartTime:,:xDispInterinale:,:xProvInteresse:,:xInquadramento:,:xRetribuzione:,:xBenefits:,:xCVProvenienza:,:xNoCV:)';
               Q.ParamByName['xIDAnagrafica']:=xIDAnag;
               Q.ParamByName['xProvInteresse']:=EProvInteresse.text;
               Q.ParamByName['xTemporaryMG']:=CbTempMG.Checked;

               Q.ParamByName['xInquadramento']:=EInquadramento.text;
               Q.ParamByName['xRetribuzione']:=ERetribuzione.text;
               Q.ParamByName['xBenefits']:=EBenefits.text;
               Q.ParamByName['xCVProvenienza']:=CBProvCV.Text;
               Q.ParamByName['xMobilita']:=CBMobilita.Checked;
               Q.ParamByName['xCassaIntegrazione']:=CBCassaInteg.Checked;
               Q.ParamByName['xDispPartTime']:=CBDispPartTime.Checked;
               Q.ParamByName['xDispInterinale']:=CBDispInterinale.Checked;
               if CBCategProtette.Checked then begin
                    Q.ParamByName['xCateogorieProtette']:='iscritto';
                    Q.ParamByName['xInvalidita']:=InvaliditaForm.CBInvalidita.Text;
                    Q.ParamByName['xPercentualeInvalidita']:=InvaliditaForm.SEPercInv.Value;
               end else begin
                    Q.ParamByName['xCateogorieProtette']:='';
                    Q.ParamByName['xInvalidita']:='';
                    Q.ParamByName['xPercentualeInvalidita']:=0;
               end;
               Q.ParamByName['xNoCV']:=CBNoCv.Checked;
               Q.ExecSQL;
               Log_Operation(xIDUtente,'AnagAltreInfo',xIDAnag,'I');

               // associazione all'annuncio
               if xIDAnnData>0 then begin
                    Q.Close;
                    Q.SQL.text:='Insert into Ann_AnagAnnEdizData (IDAnagrafica,IDAnnEdizData,DataPervenuto,IDMansione,IDRicerca,IDEmailAnnunciRic) '+
                         'values (:xIDAnagrafica:,:xIDAnnEdizData:,:xDataPervenuto:,:xIDMansione:,:xIDRicerca:,:xIDEmailAnnunciRic:)';
                    Q.ParamByName['xIDAnagrafica']:=xIDAnag;
                    Q.ParamByName['xIDAnnEdizData']:=xIDAnnData;
                    Q.ParamByName['xDataPervenuto']:=Date;
                    Q.ParamByName['xIDMansione']:=xIDMansione;
                    Q.ParamByName['xIDRicerca']:=xIDRicerca;
                    Q.ParamByName['xIDEmailAnnunciRic']:=xIDEmailAnnunciRic;
                    Q.ExecSQL;
               end;

               // CV Numero
               if xCVNumero='0' then begin
                    Q.Close;
                    Q.SQL.text:='update Anagrafica set CVNumero='+IntToStr(xIDAnag)+' where ID='+IntToStr(xIDAnag);
                    xCVNumero:=IntToStr(xIDAnag);
                    Q.ExecSQL;
               end;

               // titoli di studio
               if xIDDiploma1>0 then begin
                    Q.Close;
                    Q.SQL.text:='insert into TitoliStudio (IDAnagrafica,IDDiploma,TipoDiploma)'+
                         'values (:xIDAnagrafica:,:xIDDiploma:,:xTipoDiploma:)';
                    Q.ParamByName['xIDAnagrafica']:=xIDAnag;
                    Q.ParamByName['xIDDiploma']:=xIDDiploma1;
                    Q.ParamByName['xTipoDiploma']:=xIDTipoDiploma1;
                    Q.ExecSQL;
                    //  Prendi il valore dell'ID e logga l'operazione per la tabella
                    Q.SQL.text:='select @@IDENTITY as LastID';
                    Q.Open;
                    Log_Operation(xIDUtente,'TitoliStudio',Q.FieldByName('LastID').asInteger,'I');
                    Q.Close;
               end;
               if xIDDiploma2>0 then begin
                    Q.Close;
                    Q.SQL.text:='insert into TitoliStudio (IDAnagrafica,IDDiploma,TipoDiploma)'+
                         'values (:xIDAnagrafica:,:xIDDiploma:,:xTipoDiploma:)';
                    Q.ParamByName['xIDAnagrafica']:=xIDAnag;
                    Q.ParamByName['xIDDiploma']:=xIDDiploma2;
                    Q.ParamByName['xTipoDiploma']:=xIDTipoDiploma2;
                    Q.ExecSQL;
                    //  Prendi il valore dell'ID e logga l'operazione per la tabella
                    Q.SQL.text:='select @@IDENTITY as LastID';
                    Q.Open;
                    Log_Operation(xIDUtente,'TitoliStudio',Q.FieldByName('LastID').asInteger,'I');
                    Q.Close;
               end;
               // ruoli
               xRuoloAtt1:=CBRuoloAtt1.Checked;
               xRuoloAtt2:=CBRuoloAtt2.Checked;
               xRuoloAtt3:=CBRuoloAtt3.Checked;
               if xIDRuolo1>0 then begin
                    Q.Close;
                    Q.SQL.text:='insert into AnagMansioni (IDAnagrafica,IDMansione,Disponibile,Attuale,IDArea)'+
                         'values (:xIDAnagrafica:,:xIDMansione:,:xDisponibile:,:xAttuale:,:xIDArea:)';
                    Q.ParamByName['xIDAnagrafica']:=xIDAnag;
                    Q.ParamByName['xIDMansione']:=xIDRuolo1;
                    Q.ParamByName['xDisponibile']:=True;
                    Q.ParamByName['xAttuale']:=xRuoloAtt1;
                    Q.ParamByName['xIDArea']:=xIDArea1;
                    Q.ExecSQL;
                    //  Prendi il valore dell'ID e logga l'operazione per la tabella
                    Q.SQL.text:='select @@IDENTITY as LastID';
                    Q.Open;
                    Log_Operation(xIDUtente,'AnagMansioni',Q.FieldByName('LastID').asInteger,'I');
                    Q.Close;
               end;
               if xIDRuolo2>0 then begin
                    Q.Close;
                    Q.SQL.text:='insert into AnagMansioni (IDAnagrafica,IDMansione,Disponibile,Attuale,IDArea)'+
                         'values (:xIDAnagrafica:,:xIDMansione:,:xDisponibile:,:xAttuale:,:xIDArea:)';
                    Q.ParamByName['xIDAnagrafica']:=xIDAnag;
                    Q.ParamByName['xIDMansione']:=xIDRuolo2;
                    Q.ParamByName['xDisponibile']:=True;
                    Q.ParamByName['xAttuale']:=xRuoloAtt2;
                    Q.ParamByName['xIDArea']:=xIDArea2;
                    Q.ExecSQL;
                    //  Prendi il valore dell'ID e logga l'operazione per la tabella
                    Q.SQL.text:='select @@IDENTITY as LastID';
                    Q.Open;
                    Log_Operation(xIDUtente,'AnagMansioni',Q.FieldByName('LastID').asInteger,'I');
                    Q.Close;
               end;
               if xIDRuolo3>0 then begin
                    Q.Close;
                    Q.SQL.text:='insert into AnagMansioni (IDAnagrafica,IDMansione,Disponibile,Attuale,IDArea)'+
                         'values (:xIDAnagrafica:,:xIDMansione:,:xDisponibile:,:xAttuale:,:xIDArea:)';
                    Q.ParamByName['xIDAnagrafica']:=xIDAnag;
                    Q.ParamByName['xIDMansione']:=xIDRuolo3;
                    Q.ParamByName['xDisponibile']:=True;
                    Q.ParamByName['xAttuale']:=xRuoloAtt3;
                    Q.ParamByName['xIDArea']:=xIDArea3;
                    Q.ExecSQL;
                    //  Prendi il valore dell'ID e logga l'operazione per la tabella
                    Q.SQL.text:='select @@IDENTITY as LastID';
                    Q.Open;
                    Log_Operation(xIDUtente,'AnagMansioni',Q.FieldByName('LastID').asInteger,'I');
                    Q.Close;
               end;
               // esperienze lavorative (aziende)
               xEspAtt1:=CBEspAtt1.Checked;
               xEspAtt2:=CBEspAtt2.Checked;
               xEspAtt3:=CBEspAtt3.Checked;
               if (ESettore1.text<>'')or(EAzienda1.Text<>'')or(xDescAttivitaAz1<>'') then begin
                    Q.Close;
                    Q.SQL.text:='insert into EsperienzeLavorative (IDAnagrafica,IDAzienda,AziendaNome,IDSettore,IDAreaSettore,Attuale,IDArea,IDMansione,AnnoDal,AnnoAl,DescrizioneMansione,DescrizioneAttivitaAz)'+
                         'values (:xIDAnagrafica:,:xIDAzienda:,:xAziendaNome:,:xIDSettore:,:xIDAreaSettore:,:xAttuale:,:xIDArea:,:xIDMansione:,:xAnnoDal:,:xAnnoAl:,:xDescrizioneMansione:,:xDescrizioneAttivitaAz:)';
                    Q.ParamByName['xIDAnagrafica']:=xIDAnag;
                    Q.ParamByName['xIDAzienda']:=xIDAzienda1;
                    Q.ParamByName['xAziendaNome']:=EAzienda1.Text;
                    Q.ParamByName['xIDSettore']:=xIDAzSettore1;
                    Q.ParamByName['xIDAreaSettore']:=xIDAreaSettore1;
                    Q.ParamByName['xAttuale']:=xEspAtt1;
                    if CBAzienda1.Checked then begin
                         Q.ParamByName['xIDArea']:=xIDArea1;
                         Q.ParamByName['xIDMansione']:=xIDRuolo1;
                    end else begin
                         Q.ParamByName['xIDArea']:=0;
                         Q.ParamByName['xIDMansione']:=0;
                    end;
                    if (SEDal1.visible)and(SEDal2.visible) then begin
                         Q.ParamByName['xAnnoDal']:=SEDal1.Value;
                         Q.ParamByName['xAnnoAl']:=SEAl1.Value;
                    end else begin
                         Q.ParamByName['xAnnoDal']:=0;
                         Q.ParamByName['xAnnoAl']:=0;
                    end;
                    Q.ParamByName['xDescrizioneMansione']:=EDescPos1.Text;
                    Q.ParamByName['xDescrizioneAttivitaAz']:=xDescAttivitaAz1;
                    Q.ExecSQL;
                    //  Prendi il valore dell'ID e logga l'operazione per la tabella
                    Q.SQL.text:='select @@IDENTITY as LastID';
                    Q.Open;
                    Log_Operation(xIDUtente,'EsperienzeLavorative',Q.FieldByName('LastID').asInteger,'I');
                    Q.Close;
               end;
               if (ESettore2.text<>'')or(EAzienda2.Text<>'')or(xDescAttivitaAz2<>'') then begin
                    Q.Close;
                    Q.SQL.text:='insert into EsperienzeLavorative (IDAnagrafica,IDAzienda,AziendaNome,IDSettore,IDAreaSettore,Attuale,IDArea,IDMansione,AnnoDal,AnnoAl,DescrizioneMansione,DescrizioneAttivitaAz)'+
                         'values (:xIDAnagrafica:,:xIDAzienda:,:xAziendaNome:,:xIDSettore:,:xIDAreaSettore:,:xAttuale:,:xIDArea:,:xIDMansione:,:xAnnoDal:,:xAnnoAl:,:xDescrizioneMansione:,:xDescrizioneAttivitaAz:)';
                    Q.ParamByName['xIDAnagrafica']:=xIDAnag;
                    Q.ParamByName['xIDAzienda']:=xIDAzienda2;
                    Q.ParamByName['xAziendaNome']:=EAzienda2.Text;
                    Q.ParamByName['xIDSettore']:=xIDAzSettore2;
                    Q.ParamByName['xIDAreaSettore']:=xIDAreaSettore2;
                    Q.ParamByName['xAttuale']:=xEspAtt2;
                    if CBAzienda2.Checked then begin
                         Q.ParamByName['xIDArea']:=xIDArea2;
                         Q.ParamByName['xIDMansione']:=xIDRuolo2;
                    end else begin
                         Q.ParamByName['xIDArea']:=0;
                         Q.ParamByName['xIDMansione']:=0;
                    end;
                    if (SEDal1.visible)and(SEDal2.visible) then begin
                         Q.ParamByName['xAnnoDal']:=SEDal2.Value;
                         Q.ParamByName['xAnnoAl']:=SEAl2.Value;
                    end else begin
                         Q.ParamByName['xAnnoDal']:=0;
                         Q.ParamByName['xAnnoAl']:=0;
                    end;
                    Q.ParamByName['xDescrizioneMansione']:=EDescPos2.Text;
                    Q.ParamByName['xDescrizioneAttivitaAz']:=xDescAttivitaAz2;
                    Q.ExecSQL;
                    //  Prendi il valore dell'ID e logga l'operazione per la tabella
                    Q.SQL.text:='select @@IDENTITY as LastID';
                    Q.Open;
                    Log_Operation(xIDUtente,'EsperienzeLavorative',Q.FieldByName('LastID').asInteger,'I');
                    Q.Close;
               end;
               if (ESettore3.text<>'')or(EAzienda3.Text<>'')or(xDescAttivitaAz3<>'') then begin
                    Q.Close;
                    Q.SQL.text:='insert into EsperienzeLavorative (IDAnagrafica,IDAzienda,AziendaNome,IDSettore,IDAreaSettore,Attuale,IDArea,IDMansione,AnnoDal,AnnoAl,DescrizioneMansione,DescrizioneAttivitaAz)'+
                         'values (:xIDAnagrafica:,:xIDAzienda:,:xAziendaNome:,:xIDSettore:,:xIDAreaSettore:,:xAttuale:,:xIDArea:,:xIDMansione:,:xAnnoDal:,:xAnnoAl:,:xDescrizioneMansione:,:xDescrizioneAttivitaAz:)';
                    Q.ParamByName['xIDAnagrafica']:=xIDAnag;
                    Q.ParamByName['xIDAzienda']:=xIDAzienda3;
                    Q.ParamByName['xAziendaNome']:=EAzienda3.Text;
                    Q.ParamByName['xIDSettore']:=xIDAzSettore3;
                    Q.ParamByName['xIDAreaSettore']:=xIDAzSettore3;
                    Q.ParamByName['xAttuale']:=xEspAtt3;
                    if CBAzienda3.Checked then begin
                         Q.ParamByName['xIDArea']:=xIDArea3;
                         Q.ParamByName['xIDMansione']:=xIDRuolo3;
                    end else begin
                         Q.ParamByName['xIDArea']:=0;
                         Q.ParamByName['xIDMansione']:=0;
                    end;
                    if (SEDal1.visible)and(SEDal2.visible) then begin
                         Q.ParamByName['xAnnoDal']:=SEDal3.Value;
                         Q.ParamByName['xAnnoAl']:=SEAl3.Value;
                    end else begin
                         Q.ParamByName['xAnnoDal']:=0;
                         Q.ParamByName['xAnnoAl']:=0;
                    end;
                    Q.ParamByName['xDescrizioneMansione']:=EDescPos3.Text;
                    Q.ParamByName['xDescrizioneAttivitaAz']:=xDescAttivitaAz3;
                    Q.ExecSQL;
                    //  Prendi il valore dell'ID e logga l'operazione per la tabella
                    Q.SQL.text:='select @@IDENTITY as LastID';
                    Q.Open;
                    Log_Operation(xIDUtente,'EsperienzeLavorative',Q.FieldByName('LastID').asInteger,'I');
                    Q.Close;
               end;
               // lingue
               if xIDLingua1>0 then begin
                    Q.Close;
                    Q.SQL.text:='insert into LingueConosciute (IDAnagrafica,Lingua,Livello,IDLingua)'+
                         'values (:xIDAnagrafica:,:xLingua:,:xLivello:,:xIDLingua:)';
                    Q.ParamByName['xIDAnagrafica']:=xIDAnag;
                    Q.ParamByName['xLingua']:=ELingua1.Text;
                    Q.ParamByName['xLivello']:=QLivLingue1.FieldByName('ID').AsInteger;
                    Q.ParamByName['xIDLingua']:=xIDLingua1;
                    Q.ExecSQL;
                    //  Prendi il valore dell'ID e logga l'operazione per la tabella
                    Q.SQL.text:='select @@IDENTITY as LastID';
                    Q.Open;
                    Log_Operation(xIDUtente,'LingueConosciute',Q.FieldByName('LastID').asInteger,'I');
                    Q.Close;
               end;
               if xIDLingua2>0 then begin
                    Q.Close;
                    Q.SQL.text:='insert into LingueConosciute (IDAnagrafica,Lingua,Livello,IDLingua)'+
                         'values (:xIDAnagrafica:,:xLingua:,:xLivello:,:xIDLingua:)';
                    Q.ParamByName['xIDAnagrafica']:=xIDAnag;
                    Q.ParamByName['xLingua']:=ELingua2.Text;
                    Q.ParamByName['xLivello']:=QLivLingue2.FieldByName('ID').AsInteger;
                    Q.ParamByName['xIDLingua']:=xIDLingua1;
                    Q.ExecSQL;
                    //  Prendi il valore dell'ID e logga l'operazione per la tabella
                    Q.SQL.text:='select @@IDENTITY as LastID';
                    Q.Open;
                    Log_Operation(xIDUtente,'LingueConosciute',Q.FieldByName('LastID').asInteger,'I');
                    Q.Close;
               end;
               if xIDLingua3>0 then begin
                    Q.Close;
                    Q.SQL.text:='insert into LingueConosciute (IDAnagrafica,Lingua,Livello,IDLingua)'+
                         'values (:xIDAnagrafica:,:xLingua:,:xLivello:,:xIDLingua:)';
                    Q.ParamByName['xIDAnagrafica']:=xIDAnag;
                    Q.ParamByName['xLingua']:=ELingua3.Text;
                    Q.ParamByName['xLivello']:=QLivLingue3.FieldByName('ID').AsInteger;
                    Q.ParamByName['xIDLingua']:=xIDLingua1;
                    Q.ExecSQL;
                    //  Prendi il valore dell'ID e logga l'operazione per la tabella
                    Q.SQL.text:='select @@IDENTITY as LastID';
                    Q.Open;
                    Log_Operation(xIDUtente,'LingueConosciute',Q.FieldByName('LastID').asInteger,'I');
                    Q.Close;
               end;
               if xIDLingua4>0 then begin
                    Q.Close;
                    Q.SQL.text:='insert into LingueConosciute (IDAnagrafica,Lingua,Livello,IDLingua)'+
                         'values (:xIDAnagrafica:,:xLingua:,:xLivello:,:xIDLingua:)';
                    Q.ParamByName['xIDAnagrafica']:=xIDAnag;
                    Q.ParamByName['xLingua']:=ELingua4.Text;
                    Q.ParamByName['xLivello']:=QLivLingue4.FieldByName('ID').AsInteger;
                    Q.ParamByName['xIDLingua']:=xIDLingua1;
                    Q.ExecSQL;
               end;

               // Master
               if CbMaster.Checked then begin
                    Q.Close;
                    Q.SQL.text:='insert into CorsiExtra (IDAnagrafica,Tipologia)'+
                         'values (:xIDAnagrafica:,:xTipologia:)';
                    Q.ParamByName['xIDAnagrafica']:=xIDAnag;
                    Q.ParamByName['xTipologia']:='Master';
                    Q.ExecSQL;
               end;

               // competenze
               if (CompetenzeForm.RGCompetenze.itemIndex=0)and(QCompRich.Active) then begin
                    if QCompRich.RecordCount>0 then begin
                         for k:=1 to QCompRich.RecordCount do begin
                              Q.Close;
                              Q.SQL.text:='insert into CompetenzeAnagrafica (IDCompetenza,IDAnagrafica,Valore)'+
                                   'values (:xIDCompetenza:,:xIDAnagrafica:,:xValore:)';
                              Q.ParamByName['xIDCompetenza']:=xArrayIDComp[k];
                              Q.ParamByName['xIDAnagrafica']:=xIDAnag;
                              Q.ParamByName['xValore']:=StrToInt(CompetenzeForm.ASG1.Cells[2,k]);
                              Q.ExecSQL;
                              //  Prendi il valore dell'ID e logga l'operazione per la tabella
                              Q.SQL.text:='select @@IDENTITY as LastID';
                              Q.Open;
                              Log_Operation(xIDUtente,'CompetenzeAnagrafica',Q.FieldByName('LastID').asInteger,'I');
                              Q.Close;
                         end;
                    end;
               end;

               // File associati
               if xFILEIDTipo>0 then begin
                    Q1.close;
                    Q1.SQL.text:='insert into AnagFile (IDAnagrafica,IDTipo,Descrizione,NomeFile,DataCreazione) '+
                         'values (:xIDAnagrafica:,:xIDTipo:,:xDescrizione:,:xNomeFile:,:xDataCreazione:)';
                    Q1.ParamByName['xIDAnagrafica']:=xIDAnag;
                    Q1.ParamByName['xIDTipo']:=xFILEIDTipo;
                    Q1.ParamByName['xDescrizione']:=xFILEDescrizione;
                    Q1.ParamByName['xNomeFile']:=xFILENome;
                    Q1.ParamByName['xDataCreazione']:=FileDateToDateTime(FileAge(xFILENome));
                    Q1.ExecSQL;
               end;


               // storico: chi l'ha registrato
               Q.SQL.text:='insert into Storico (IDAnagrafica,IDEvento,DataEvento,IDUtente,Annotazioni) '+
                    'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xIDUtente:,:xAnnotazioni:)';
               Q.ParamByName['xIDAnagrafica']:=xIDAnag;
               Q.ParamByName['xIDEvento']:=82;
               Q.ParamByName['xdataEvento']:=Date;
               Q.ParamByName['xIDUtente']:=xIDUtente;
               Q.ParamByName['xAnnotazioni']:='(dal modulo "inserimento veloce")';
               Q.ExecSQL;
               if xIDUtenteIncontro>0 then begin
                    // storico: colloquio pregresso
                    Q.SQL.text:='insert into Storico (IDAnagrafica,IDEvento,DataEvento,IDUtente,Annotazioni) '+
                         'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xIDUtente:,:xAnnotazioni:)';
                    Q.ParamByName['xIDAnagrafica']:=xIDAnag;
                    Q.ParamByName['xIDEvento']:=64;
                    Q.ParamByName['xdataEvento']:=xDataIncontro;
                    Q.ParamByName['xIDUtente']:=xIDUtenteIncontro;
                    Q.ParamByName['xAnnotazioni']:='incontro (inserito da ins.veloce)';
                    Q.ExecSQL;
               end;
               if xDaEmail then begin
                    // associazione a ricerca
                    if xIDRicerca>0 then begin
                         Q.SQL.text:='insert into EBC_CandidatiRicerche (IDRicerca,IDAnagrafica,DataIns,Escluso,Stato) '+
                              'values (:xIDRicerca:,:xIDAnagrafica:,:xDataIns:,:xEscluso:,:xStato:)';
                         Q.ParamByName['xIDRicerca']:=xIDRicerca;
                         Q.ParamByName['xIDAnagrafica']:=xIDAnag;
                         Q.ParamByName['xDataIns']:=Date;
                         Q.ParamByName['xEscluso']:=False;
                         Q.ParamByName['xStato']:='da ins.veloce (CV pervenuto via e-mail)';
                         Q.ExecSQL;
                    end;
                    // se ci sono allegati, aggiungili sempre ai file associatai al soggetto
                    xlettera:='a';
                    xExtTemp:=xFileExt;
                    for h:=1 to 10 do begin
                         if FileExists(EmailAnnunciForm.TmpCA+'att'+IntToStr(EmailAnnunciForm.xID)+xLettera+copy(xExtTemp,1,pos(';',xExtTemp)-1)) then begin
                              RenameFile(EmailAnnunciForm.TmpCA+'att'+IntToStr(EmailAnnunciForm.xID)+xLettera+copy(xExtTemp,1,pos(';',xExtTemp)-1),TGlobalDirFileDoc.Value+'\att'+IntToStr(xIDAnag)+xLettera+copy(xExtTemp,1,pos(';',xExtTemp)-1));
                              Q.SQL.text:='insert into AnagFile (IDAnagrafica,IDTipo,Descrizione,NomeFile,DataCreazione) '+
                                   'values (:xIDAnagrafica:,:xIDTipo:,:xDescrizione:,:xNomeFile:,:xDataCreazione:)';
                              Q.ParamByName['xIDAnagrafica']:=xIDAnag;
                              Q.ParamByName['xIDTipo']:=2;
                              Q.ParamByName['xDescrizione']:='allegato all''e-mail';
                              Q.ParamByName['xNomeFile']:=TGlobal.FieldByName('DirFileDoc').AsString+'\att'+IntToStr(xIDAnag)+xLettera+copy(xExtTemp,1,pos(';',xExtTemp)-1);
                              Q.ParamByName['xDataCreazione']:=Date;
                              Q.ExecSQL;
                         end;
                         xExtTemp:=copy(xExtTemp,pos(';',xExtTemp)+1,length(xExtTemp));
                         xLettera:=char(Ord(xLettera)+1);
                    end;
                    // salva il testo in AnagFile
                    EmailAnnunciForm.Memo1.lines.SaveToFile(TGlobalDirFileDoc.Value+'\e'+IntToStr(xIDAnag)+'.txt');
                    Q.SQL.text:='insert into AnagFile (IDAnagrafica,IDTipo,Descrizione,NomeFile,DataCreazione) '+
                         'values (:xIDAnagrafica:,:xIDTipo:,:xDescrizione:,:xNomeFile:,:xDataCreazione:)';
                    Q.ParamByName['xIDAnagrafica']:=xIDAnag;
                    Q.ParamByName['xIDTipo']:=2;
                    Q.ParamByName['xDescrizione']:='testo dell''e-mail';
                    Q.ParamByName['xNomeFile']:=TGlobal.FieldByName('DirFileDoc').AsString+'\e'+IntToStr(xIDAnag)+'.txt';
                    Q.ParamByName['xDataCreazione']:=Date;
                    Q.ExecSQL;

                    // aggiorna IDAnagrafica di Email
                    Q.SQL.text:='update EmailAnnunciRic set IDAnagrafica=:xIDAnagrafica: '+
                         'where ID='+IntToStr(EmailAnnunciForm.xID);
                    Q.ParamByName['xIDAnagrafica']:=xIDAnag;
                    Q.ExecSQL;
               end;

               DataModule1.DB.CommitTrans;

               TTitoliStudio.Close;
               TAnagRuoli.Close;
               TEspLav.Close;
               TLingueConosc.Close;
               TAnagComp.Close;

               xRegistrato:=True;
               BAltraScheda.Enabled:=True;
               BRegistra.Enabled:=False;
               DecodeTime(xOraFine-xOraInizio,xOra,xMin,xSec,xMsec);
               if MessageDlg('INSERIMENTO AVVENUTO per il CV n� '+xCVNumero+chr(13)+
                    'Tempo impiegato per questo CV:  '+IntToStr(xMin)+' minuti e '+IntToStr(xSec)+' secondi... - ALTRA SCHEDA ?',mtInformation, [mbYes,mbNo],0)=mrYes then
                    BAltraSchedaClick(self);
          except
               DataModule1.DB.RollbackTrans;
               MessageDlg('ATTENZIONE: errore sul database'+chr(13)+
                    'Inserimenti NON effettuati !',mtError, [mbOK],0);
               raise;
          end;

          Splash3Form.Hide;
          Splash3Form.Free;
     end;
end;    }



procedure TRiepilogoCandForm.BAltraSchedaClick(Sender: TObject);
begin
     // repulisti campi e azzeramento variabili
     //iOdometerX1.value:=0;
     //ProgressBarEx1.Position:=0;
     ECognome.text := '';
     ENome.text := '';
     DEDataNascita.ClearDate(self);
     DEDataAggCV.ClearDate(self);
     CBSesso.Text := 'M';
     xTipoStrada := '';
     xIndirizzo := '';
     xNumCivico := '';
     xNazione := '';
     ECellulare.text := '';
     EEmail.text := '';
     ETelefoni.text := '';
     ETelUfficio.text := '';
     EComuneDom.Text := '';
     ECapDom.Text := '';
     EProv.text := '';
     EDiploma1.text := '';
     CbMaster.Checked := False;
     EArea1.text := '';
     ERuolo1.text := '';
     EDescPos1.text := '';
     CBAzienda1.Checked := True;
     EArea2.text := '';
     ERuolo2.text := '';
     EDescPos2.text := '';
     CBAzienda2.Checked := True;
     EArea3.text := '';
     ERuolo3.text := '';
     EDescPos3.text := '';
     CBAzienda3.Checked := True;
     ESettore1.text := '';
     EAreaSettore1.text := '';
     EAzienda1.text := '';
     ESettore2.text := '';
     EAreaSettore3.text := '';
     EAzienda2.text := '';
     ESettore3.text := '';
     EAreaSettore3.text := '';
     EAzienda3.text := '';
     ELingua1.text := '';
     ELingua2.text := '';
     ELingua3.text := '';
     ELingua4.text := '';
     if QCVAnnData.Active then QCVAnnData.Close;
     EProvInteresse.text := '';
     EInquadramento.text := '';
     ERetribuzione.text := '';
     EBenefits.text := '';
     // propriet� CV
     EPropCV.Text := '';
     EPropCV.Visible := False;
     CBPropMio.Checked := True;
     // altri dati
     CBMobilita.Checked := False;
     CBCassaInteg.Checked := False;
     CBCategProtette.Checked := False;
     CBDispPartTime.Checked := False;
     CBDispInterinale.Checked := False;
     // provenienza CV
     CBProvCV.text := '';

     EArea.Text := '';

     CBRuoloAtt1.Checked := False;
     iDubbioRuolo1.imageindex := 2;
     CBRuoloAtt2.Checked := False;
     iDubbioRuolo2.imageindex := 2;
     CBRuoloAtt3.Checked := False;
     iDubbioRuolo3.imageindex := 2;
     CBEspAtt1.Checked := False;
     CBEspAtt2.Checked := False;
     CBEspAtt3.Checked := False;
     if Uppercase(copy(TGlobal.FieldByName('NomeAzienda').AsString, 1, 5)) = 'ERGON' then begin
          CBRuoloAtt1.Checked := true;
          CBEspAtt1.Checked := true;
     end;
     SEDAl1.Value := 0; SEAl1.Value := 0;
     SEDAl2.Value := 0; SEAl2.Value := 0;
     SEDAl3.Value := 0; SEAl3.Value := 0;

     CBLibPrivacy.Checked := False;
     CbTempMG.Checked := False;

     xIDDiploma1 := 0;
     xIDDiploma2 := 0;
     xIDRuolo1 := 0;
     xIDRuolo2 := 0;
     xIDRuolo3 := 0;
     xIDAzienda1 := 0;
     xIDAzienda2 := 0;
     xIDAzienda3 := 0;
     xIDLingua1 := 0;
     xIDLingua2 := 0;
     xIDLingua3 := 0;
     xIDLingua4 := 0;
     xIDZonaInt := 0;
     xIDCliente := 0;
     xIDAnnData := 0;
     xIDAzSettore1 := 0;
     xIDAreaSettore1 := 0;
     xIDAzSettore2 := 0;
     xIDAreaSettore2 := 0;
     xIDAzSettore3 := 0;
     xIDAreaSettore3 := 0;
     xIDComuneDom := 0;
     xIDZonaDom := 0;
     xIDUtenteIncontro := 0;
     xIDRicerca := 0;
     xIDMansione := 0;
     xIDEmailAnnunciRic := 0;
     xDaEmail := False;
     xIDFileAttach := 0;
     xFileExt := '';
     // livello conoscenza lingue: tutte sul buono
     QLivLingue1.Locate('ID', 4, []);
     QLivLingue2.Locate('ID', 4, []);
     QLivLingue3.Locate('ID', 4, []);
     QLivLingue4.Locate('ID', 4, []);

     xDescAttivitaAz1 := '';
     xDescAttivitaAz2 := '';
     xDescAttivitaAz3 := '';

     xFILEIDTipo := 0;
     xFILEDescrizione := '';
     xFILENome := '';

     CBNoCv.Checked := False;

     ECognome.SetFocus;

     BAltraScheda.Enabled := False;
     BRegistra.Enabled := True;
end;

procedure TRiepilogoCandForm.FormClose(Sender: TObject;
     var Action: TCloseAction);
begin
     {     if not xRegistrato then
               if MessageDlg('I dati non sono stati registrati - Vuoi uscire comunque ?',mtWarning, [mbYes,mbNo],0)=mrNo then
                    abort;
          // aggiornamento log accessi
          DataModule1.DB.BeginTrans;
          try
               Q.Close;
               Q.SQL.Text:='update Log_accessi set OraUscita=:xOraUscita: '+
                    'where ID = (select max(ID) from Log_accessi where IDUtente=:xIDUtente: and Data=:xData: and Modulo=:xModulo:)';
               Q.ParamByName['xOraUscita']:=Now;
               Q.ParamByName['xIDUtente']:=xIDUtente;
               Q.ParamByName['xData']:=Date;
               Q.ParamByName['xModulo']:='IV';
               Q.ExecSQL;
               DataModule1.DB.CommitTrans;
          except
               DataModule1.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
          end;   }
end;

procedure TRiepilogoCandForm.BitBtn1Click(Sender: TObject);
begin
     ComuniForm := TComuniForm.create(self);
     ComuniForm.EProv.Text := xUltimaProv;
     ComuniForm.ShowModal;
     if ComuniForm.ModalResult = mrOK then begin
          EComuneDom.Text := ComuniForm.QComuni.FieldByName('Descrizione').AsString;
          ECapDom.Text := ComuniForm.QComuni.FieldByName('Cap').AsString;
          EProv.Text := ComuniForm.QComuni.FieldByName('Prov').AsString;
          xIDComuneDom := ComuniForm.QComuni.FieldByName('ID').AsInteger;
          xIDZonaDom := ComuniForm.QComuni.FieldByName('IDZona').AsInteger;
          EArea.Text := ComuniForm.QComuni.FieldByName('AreaNielsen').asstring + ' ' + ComuniForm.QComuni.FieldByName('Regione').AsString;
     end else begin
          EComuneDom.Text := '';
          EProv.Text := '';
          ECapDom.Text := '';
          xIDComuneDom := 0;
          xIDZonaDom := 0;
          EArea.Text := '';
     end;
     xUltimaProv := ComuniForm.EProv.Text;
     ComuniForm.Free;
end;

procedure TRiepilogoCandForm.Timer1Timer(Sender: TObject);
var xOra, xMin, xSec, xMsec: Word;
begin
     //ProgressBarEx1.Position:=ProgressBarEx1.Position+0.1;
     //ProgressBarEx1.Refresh;
     //DecodeTime(Time-xOraInizio,xOra,xMin,xSec,xMsec);
     //iOdometerX1.value:=iOdometerX1.value+0.1;
     {     if (xMin>2) and (xSec>30) then begin
             Timer1.Enabled:=False;
             ShowMessage('ALLORA:  sono gi� trascorsi 2,5 minuti.  Subito a rapporto da Sabattini ! DAI, DAI !!');
             Timer1.Enabled:=True;
             ProgressBarEx1.Position:=0;
             xOraInizio:=Time;
          end;
     }
end;

procedure TRiepilogoCandForm.CBRuoloAtt1Click(Sender: TObject);
begin
     if CBRuoloAtt1.Checked then begin
          CBRuoloAtt2.Checked := False;
          CBRuoloAtt3.Checked := False;
     end;
end;

procedure TRiepilogoCandForm.CBRuoloAtt2Click(Sender: TObject);
begin
     if CBRuoloAtt2.Checked then begin
          CBRuoloAtt1.Checked := False;
          CBRuoloAtt3.Checked := False;
     end;
end;

procedure TRiepilogoCandForm.CBRuoloAtt3Click(Sender: TObject);
begin
     if CBRuoloAtt3.Checked then begin
          CBRuoloAtt1.Checked := False;
          CBRuoloAtt2.Checked := False;
     end;
end;

procedure TRiepilogoCandForm.CBEspAtt1Click(Sender: TObject);
begin
     if CBEspAtt1.Checked then begin
          CBEspAtt2.Checked := False;
          CBEspAtt3.Checked := False;
     end;
end;

procedure TRiepilogoCandForm.CBEspAtt2Click(Sender: TObject);
begin
     if CBEspAtt2.Checked then begin
          CBEspAtt1.Checked := False;
          CBEspAtt3.Checked := False;
     end;
end;

procedure TRiepilogoCandForm.CBEspAtt3Click(Sender: TObject);
begin
     if CBEspAtt3.Checked then begin
          CBEspAtt1.Checked := False;
          CBEspAtt2.Checked := False;
     end;
end;

procedure TRiepilogoCandForm.SpeedButton1Click(Sender: TObject);
begin
     CercaAnnuncioForm := TCercaAnnuncioForm.create(self);
     CercaAnnuncioForm.ShowModal;
     if CercaAnnuncioForm.ModalResult = mrOK then begin
          xIDRicerca := 0;
          xIDMansione := 0;
          Q1.Close;
          Q1.SQL.text := 'select Ann_AnnunciRicerche.ID,IDRicerca,EBC_Ricerche.IDMansione,Codice,EBC_Ricerche.Progressivo Rif, ' +
               'EBC_Ricerche.NumRicercati Num,Mansioni.Descrizione Ruolo,EBC_Clienti.Descrizione Cliente ' +
               'from Ann_AnnunciRicerche,EBC_Ricerche,Mansioni,EBC_Clienti ' +
               'where Ann_AnnunciRicerche.IDRicerca=EBC_Ricerche.ID and EBC_Ricerche.IDMansione=Mansioni.ID ' +
               'and EBC_Ricerche.IDCliente=EBC_Clienti.ID and Ann_AnnunciRicerche.IDAnnuncio=' + CercaAnnuncioForm.QAnnAttivi.FieldByName('ID').asString;
          Q1.Open;
          if Q1.RecordCount > 1 then begin
               SelFromQueryForm := TSelFromQueryForm.create(self);
               SelFromQueryForm.QGen.SQL.text := Q1.SQL.text;
               SelFromQueryForm.QGen.Open;
               SelFromQueryForm.xNumInvisibleCols := 3;
               SelFromQueryForm.Caption := 'seleziona ricerca/commessa associata all''annuncio';
               SelFromQueryForm.ShowModal;
               if SelFromQueryForm.ModalResult = mrOK then begin
                    xIDRicerca := SelFromQueryForm.QGen.FieldByName('IDRicerca').asInteger;
                    xIDMansione := SelFromQueryForm.QGen.FieldByName('IDMansione').asInteger;
               end;
               SelFromQueryForm.Free;
          end else begin
               xIDRicerca := Q1.FieldByName('IDRicerca').asInteger;
               xIDMansione := Q1.FieldByName('IDMansione').asInteger;
          end;
          Q1.Close;
          // rif ruolo
          Q1.SQL.text := 'select Ann_AnnunciRuoli.ID,Codice,IDMansione, Mansioni.Descrizione Ruolo ' +
               'from Ann_AnnunciRuoli,Mansioni where Ann_AnnunciRuoli.IDMansione=Mansioni.ID ' +
               'and Ann_AnnunciRuoli.IDAnnuncio=' + CercaAnnuncioForm.QAnnAttivi.FieldByName('ID').asString;
          Q1.Open;
          if Q1.RecordCount > 1 then begin
               SelFromQueryForm := TSelFromQueryForm.create(self);
               SelFromQueryForm.QGen.SQL.text := Q1.SQL.text;
               SelFromQueryForm.QGen.Open;
               SelFromQueryForm.xNumInvisibleCols := 1;
               SelFromQueryForm.Caption := 'seleziona ruolo associata all''annuncio';
               SelFromQueryForm.ShowModal;
               if SelFromQueryForm.ModalResult = mrOK then begin
                    xIDMansione := SelFromQueryForm.QGen.FieldByName('IDMansione').asInteger;
               end;
               SelFromQueryForm.Free;
          end else begin
               if Q1.RecordCount = 1 then
                    xIDMansione := Q1.FieldByName('IDMansione').asInteger;
          end;

          xIDAnnData := CercaAnnuncioForm.QAnnAttivi.FieldByName('IDAnnEdizData').AsInteger;
          QCVAnnData.Close;
          QCVAnnData.ReloadSQL;
          QCVAnnData.ParamByName['x'] := xIDAnnData;
          QCVAnnData.Open;
     end else begin
          xIDAnnData := 0;
          xIDRicerca := 0;
          xIDMansione := 0;
          QCVAnnData.Close;
     end;
     CercaAnnuncioForm.Free;
end;

procedure TRiepilogoCandForm.ECognomeChange(Sender: TObject);
begin
     BAltraScheda.Enabled := False;
     BRegistra.Enabled := True;
     // scatta il tempo
     xOraInizio := Now;
end;

procedure TRiepilogoCandForm.BCompetenzeClick(Sender: TObject);
begin
     CompetenzeForm.ShowModal;
end;

procedure TRiepilogoCandForm.BCategProtetteClick(Sender: TObject);
begin
     //InvaliditaForm.ShowModal;
end;

procedure TRiepilogoCandForm.CBCategProtetteClick(Sender: TObject);
begin
     BCategProtette.Enabled := CBCategProtette.Checked;
end;

procedure TRiepilogoCandForm.CBPropMioClick(Sender: TObject);
begin
     EPropCV.Visible := not CBPropMio.Checked;
     BPropCV.Visible := not CBPropMio.Checked;
end;

procedure TRiepilogoCandForm.BPropCVClick(Sender: TObject);
begin
     {ClientiForm:=TClientiForm.create(self);
     ClientiForm.ShowModal;
     if ClientiForm.ModalResult=mrOK then begin
          xIDCliente:=ClientiForm.TClienti.FieldByName('ID').AsInteger;
          EPropCV.text:=ClientiForm.TClienti.FieldByName('Descrizione').AsString;
     end else begin
          xIDCliente:=0;
          EPropCV.text:='';
     end;
     ClientiForm.Free; }
end;

procedure TRiepilogoCandForm.ENomeExit(Sender: TObject);
begin
     // - controllo omonimia
   {  QCheckNomi.Close;
     QCheckNomi.ReloadSQL;
     QCheckNomi.ParamByName['xCogn']:=trim(ECognome.Text);
     QCheckNomi.ParambyName['xNome']:=trim(ENome.Text);
     QCheckNomi.Open;
     if QCheckNomi.RecordCount>0 then
          MessageDlg('CONTROLLARE POSSIBILE OMONIMIA con CV n� '+QCheckNomi.FieldByName('CVNumero').asString+chr(13),
               mtWarning, [mbOK],0);  }
end;

procedure TRiepilogoCandForm.ECellulareExit(Sender: TObject);
begin
     if (ECellulare.Text <> '') then
          if (pos('-', ECellulare.Text) = 0) and (pos('/', ECellulare.Text) = 0) then begin
               Showmessage('ATTENZIONE: Numero di cellulare non correttamente formattato.' + chr(13) +
                    'Utilizzare la linea (-) o la barra (/) per separare il prefisso dal numero');
               ECellulare.SetFocus;
          end;
end;

procedure TRiepilogoCandForm.BitBtn2Click(Sender: TObject);
begin
     IndirizzoForm := TIndirizzoForm.create(self);
     if xTipoStrada <> '' then IndirizzoForm.CBTipoStrada.Text := xTipoStrada;
     if xIndirizzo <> '' then IndirizzoForm.EIndirizzo.Text := xIndirizzo;
     if xNumCivico <> '' then IndirizzoForm.ENumCivico.Text := xNumCivico;
     if xNazione <> '' then IndirizzoForm.ENazione.Text := xNazione;
     IndirizzoForm.ShowModal;
     if IndirizzoForm.ModalResult = mrOK then begin
          xTipoStrada := IndirizzoForm.CBTipoStrada.Text;
          xIndirizzo := IndirizzoForm.EIndirizzo.Text;
          xNumCivico := IndirizzoForm.ENumCivico.Text;
          xNazione := IndirizzoForm.ENazione.Text;
     end;
     IndirizzoForm.Free;
end;

procedure TRiepilogoCandForm.Log_Operation(xIDUtente: integer; xTabella: string; xKeyValue: integer; xOperation: string);
begin
     // inserisci l'operazione nel log delle operazioni
     // ATTENZIONE: LA TRANSAZIONE DOVREBBE GIA' ESSERE IN CORSO !!
     if xKeyValue = 0 then exit;
     Q.Close;
     Q.SQL.text := 'insert into Log_TableOp (IDUtente,DataOra,Tabella,KeyValue,Operation) ' +
         'values (:xIDUtente,:xDataOra,:xTabella,:xKeyValue,:xOperation)';
          //'values (:xIDUtente:,:xDataOra:,:xTabella:,:xKeyValue:,:xOperation:)';
     Q.Parameters.paramByName('xIDUtente').Value := xIDUtente;
     Q.Parameters.paramByName('xDataOra').Value := Now;
     Q.Parameters.paramByName('xTabella').Value := xTabella;
     Q.Parameters.paramByName('xKeyValue').Value := xKeyValue;
     Q.Parameters.paramByName('xOperation').Value := xOperation;
     {Q.ParamByName['xIDUtente'] := xIDUtente;
     Q.ParamByName['xDataOra'] := Now;
     Q.ParamByName['xTabella'] := xTabella;
     Q.ParamByName['xKeyValue'] := xKeyValue;
     Q.ParamByName['xOperation'] := xOperation; }

     Q.ExecSQL;
end;

procedure TRiepilogoCandForm.RGincontratoClick(Sender: TObject);
begin
     if RGincontrato.itemindex = 1 then
          BIncontro.Enabled := True;
end;

procedure TRiepilogoCandForm.BIncontroClick(Sender: TObject);
begin
     {IncontroForm:=TIncontroForm.create(self);
     IncontroForm.DEData.date:=Date;
     IncontroForm.ShowModal;
     if IncontroForm.ModalResult=mrOK then begin
          xDataIncontro:=IncontroForm.DEData.Date;
          xIDUtenteIncontro:=IncontroForm.QUsers.FieldByName('ID').AsInteger;
     end;
     IncontroForm.Free; }
end;

procedure TRiepilogoCandForm.CBProvCVDropDown(Sender: TObject);
begin
     // caricamento DBCBProvCV con le provenienze da tabella
     Q.Close;
     Q.SQL.text := 'select Provenienza from ProvenienzeCV';
     Q.Open;
     CBProvCV.items.Clear;
     while not Q.EOF do begin
          CBProvCV.items.Add(Q.FieldByName('Provenienza').asString);
          Q.Next;
     end;
     Q.Close;
end;

procedure TRiepilogoCandForm.iDubbioRuolo1Click(Sender: TObject);
begin
     if xIDRuolo1 = 0 then exit;
     if iDubbioRuolo1.ImageIndex = 0 then iDubbioRuolo1.ImageIndex := 1
     else iDubbioRuolo1.ImageIndex := 0;
end;

procedure TRiepilogoCandForm.iDubbioRuolo2Click(Sender: TObject);
begin
     if xIDRuolo2 = 0 then exit;
     if iDubbioRuolo2.ImageIndex = 0 then iDubbioRuolo2.ImageIndex := 1
     else iDubbioRuolo2.ImageIndex := 0;
end;

procedure TRiepilogoCandForm.iDubbioRuolo3Click(Sender: TObject);
begin
     if xIDRuolo3 = 0 then exit;
     if iDubbioRuolo3.ImageIndex = 0 then iDubbioRuolo3.ImageIndex := 1
     else iDubbioRuolo3.ImageIndex := 0;
end;

procedure TRiepilogoCandForm.BEmailAnnClick(Sender: TObject);
begin
     //EmailAnnunciForm.Show;
end;

procedure TRiepilogoCandForm.BitBtn3Click(Sender: TObject);
begin
     InputQuery('Descrizione attivit� azienda 1', 'testo:', xDescAttivitaAz1);
     with Data do begin
          DB.BeginTrans;
          try
               Q1.close;
               Q1.SQL.text := 'update EsperienzeLavorative set descrizioneattivitaaz=:xNoteAz:' +
                    'where id = ' + IntToStr(xRecEspLav1);
               Q1.ParamByName['xNoteAz'] := xDescAttivitaAz1;
               Q1.ExecSQL;
               DB.CommitTrans;
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: operazione non effettuata', mtError, [mbOK], 0);

          end;
     end;
end;

procedure TRiepilogoCandForm.BitBtn4Click(Sender: TObject);
begin
     //InputQuery('Descrizione attivit� azienda 2','testo:',xDescAttivitaAz2);
     InputQuery('Descrizione attivit� azienda 2', 'testo:', xDescAttivitaAz2);
     with Data do begin
          DB.BeginTrans;
          try
               Q1.close;
               Q1.SQL.text := 'update EsperienzeLavorative set descrizioneattivitaaz=:xNoteAz:' +
                    'where id = ' + IntToStr(xRecEspLav2);
               Q1.ParamByName['xNoteAz'] := xDescAttivitaAz2;
               Q1.ExecSQL;
               DB.CommitTrans;
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: operazione non effettuata', mtError, [mbOK], 0);

          end;
     end;
end;

procedure TRiepilogoCandForm.BitBtn5Click(Sender: TObject);
begin
     //InputQuery('Descrizione attivit� azienda 3','testo:',xDescAttivitaAz3);
     InputQuery('Descrizione attivit� azienda 3', 'testo:', xDescAttivitaAz3);
     with Data do begin
          DB.BeginTrans;
          try
               Q1.close;
               Q1.SQL.text := 'update EsperienzeLavorative set descrizioneattivitaaz=:xNoteAz:' +
                    'where id = ' + IntToStr(xRecEspLav3);
               Q1.ParamByName['xNoteAz'] := xDescAttivitaAz3;
               Q1.ExecSQL;
               DB.CommitTrans;
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: operazione non effettuata', mtError, [mbOK], 0);

          end;
     end;
end;

procedure TRiepilogoCandForm.ToolbarButton971Click(Sender: TObject);
begin
     if xFILEIDTipo > 0 then begin
          if MessageDlg('Attenzione: file gi� associato. Vuoi sostituirlo con un altro ?', mtWarning, [mbYes, mbNo], 0) = mrNo then
               exit;
     end;
     AnagFileForm := TAnagFileForm.create(self);
     AnagFileForm.FEFile.InitialDir := GetDocPath;
     AnagFileForm.ShowModal;
     if AnagFileForm.ModalResult = mrOK then begin
          xFILEIDTipo := AnagFileForm.QTipiFile.FieldByName('ID').AsInteger;
          xFILEDescrizione := AnagFileForm.EDesc.text;
          xFILENome := AnagFileForm.FEFile.filename;
     end else begin
          xFILEIDTipo := 0;
          xFILEDescrizione := '';
          xFILENome := '';
     end;
     AnagFileForm.Free;
end;

function TRiepilogoCandForm.GetDocPath: string;
var xH1SelPaths: TStringList;
begin
     // restituisce il path della cartella documenti
     if FileExists('c:\H1SelPaths.txt') then begin
          xH1SelPaths := TStringList.create;
          xH1SelPaths.LoadFromFile('c:\H1SelPaths.txt');
          Result := xH1SelPaths.Strings[0];
          xH1SelPaths.Free;
     end else begin
          with Q2 do begin
               Close;
               SQL.text := 'select DirFileDoc from Global';
               Open;
               if IsEmpty then Result := ''
               else Result := FieldByName('DirFileDoc').asString;
               Close;
          end;
     end;
end;



procedure TRiepilogoCandForm.BSalvaClick(Sender: TObject);
begin

     //Dati anagrafici
     ECognome.SetFocus;
     Data.DB.BeginTrans;
     try

          Q1.Close;
          Q1.SQL.text := 'Update Anagrafica set Cognome=:xCognome:, Nome=:xNome:, DataNascita=:xDataNascita:, Sesso=:xSesso:, ' +
               'Cellulare=:xCellulare:, EMail=:xEMail:, RecapitiTelefonici=:xRecapitiTelefonici:, TelUfficio=:xTelUfficio:, ' +
               'CVDataAgg=:xCVDataAgg:, LibPrivacy=:xLibPrivacy:,IDcomunedom=:xIDComuneDom:,DomicilioTipoStrada=:xDomicilioTipoStrada:,' +
               'DomicilioIndirizzo=:xDomicilioIndirizzo:,DomicilioNumcivico=:xDomicilioNumcivico:,DomicilioStato=:xDomicilioStato:,' +
               'DomicilioComune=:xDomicilioComune:,DomicilioCap=:xDomicilioCap:,DomicilioProvincia=:xDomicilioProvincia:,IDZonaDom=:xIDZonaDom:' +
               'where id = ' + xIDCandidato;
          Q1.ParamByName['xCognome'] := ECognome.Text;
          Q1.ParamByName['xNome'] := ENome.Text;
          {if DEDataNascita.Text<>'' then
               Q1.ParamByName['xDataNascita']:=DEDataNascita.text
          else Q1.ParamByName['xDataNascita']:='';}
          if DEDataNascita.Text <> '' then
               Q1.ParamByName['xDataNascita'] := DEDataNascita.Date; //StrToDateTime(DEDataNascita.Text);
          Q1.ParamByName['xSesso'] := CBSesso.Text;
          Q1.ParamByName['xCellulare'] := ECellulare.Text;
          Q1.ParamByName['xEMail'] := EEmail.text;
          Q1.ParamByName['xRecapitiTelefonici'] := ETelefoni.Text;
          Q1.ParamByName['xTelUfficio'] := ETelUfficio.Text;
          Q1.ParamByName['xIDComuneDom'] := xIDComuneDom;
          if DEDataAggCV.Text <> '' then
               Q1.ParamByName['xcvdataagg'] := DEDataAggCV.Date;
          Q1.ParamByName['xLibPrivacy'] := CBLibPrivacy.Checked;
          Q1.ParamByName['xDomicilioTipoStrada'] := xTipoStrada;
          Q1.ParamByName['xDomicilioIndirizzo'] := xIndirizzo;
          Q1.ParamByName['xDomicilioNumcivico'] := xNumCivico;
          Q1.ParamByName['xDomicilioStato'] := xNazione;
          Q1.ParamByName['xDomicilioComune'] := EComuneDom.Text;
          Q1.ParamByName['xDomicilioCap'] := ECapDom.Text;
          Q1.ParamByName['xDomicilioProvincia'] := EProv.Text;

          Q1.ParamByName['xIDZonaDom'] := xIDZonaDom;
          Q1.ExecSQL;

          //Titoli di studio
          if EDiploma1.text <> '' then begin
               if xRecTitoliStudio1 > 0 then begin
                    if xIDDiploma1 <> 0 then begin
                         Q1.Close;
                         Q1.SQL.Text := 'Update TitoliStudio set IDDiploma=:xIDDiploma:,TipoDiploma=:xTipoDiploma:' +
                              'where id = ' + IntToStr(xRecTitoliStudio1);
                         Q1.ParamByName['xIDDiploma'] := xIDDiploma1;
                         Q1.ParamByname['xTipoDiploma'] := xIDTipoDiploma1;
                         Q1.ExecSQL;
                    end;
               end
               else begin
                    Q1.Close;
                    Q1.SQL.Text := 'Insert into TitoliStudio (IDAnagrafica,IDDiploma,TipoDiploma) values (:xIDCandidato:,:xIDDiploma:,:xTipoDiploma:)';
                    Q1.ParamByName['xIDCandidato'] := xIDCandidato;
                    Q1.ParamByName['xIDDiploma'] := xIDDiploma1;
                    Q1.ParamByname['xTipoDiploma'] := xIDTipoDiploma1;
                    Q1.ExecSQL;

               end;
          end;
          //Master
          if CbMaster.Checked then begin
               if xRecMaster = 0 then begin
                    Q1.close;
                    Q1.sql.Text := 'insert into corsiextra (idanagrafica, tipologia) values (:xIDCandidato:,:xTipologia:)';
                    Q1.ParamByName['xIDCandidato'] := xIDCandidato;
                    Q1.ParamByName['xTipologia'] := 'Master';
                    Q1.ExecSQL;
               end
          end else
               if xRecMaster > 0 then begin
                    Q1.close;
                    Q1.sql.Text := 'delete from corsiextra where id = ' + IntToStr(xRecMaster);
                    Q1.ExecSQL;
               end;


          //Altri dati anagrafici
          Q1.Close;
          Q1.SQL.text := 'Update AnagAltreInfo set ProvInteresse=:xProvInteresse:, Inquadramento=:xInquadramento:,' +
               'Retribuzione=:xRetribuzione:, Benefits=:xBenefits:, TemporaryMG=:xTemporaryMG:, NoCV=:xNoCV:' +
               'where idanagrafica = ' + xIDCandidato;
          Q1.ParamByName['xProvinteresse'] := EProvInteresse.Text;
          Q1.ParamByName['xInquadramento'] := EInquadramento.Text;
          Q1.ParamByName['xRetribuzione'] := ERetribuzione.text;
          Q1.ParamByname['xBenefits'] := EBenefits.text;
          Q1.ParamByName['xTemporaryMG'] := CbTempMG.Checked;
          Q1.ParamByName['xNoCV'] := CBNoCv.Checked;
          Q1.ExecSQL;

          //Lingue
          if ELingua1.Text <> '' then begin
               if xRecLingua1 > 0 then begin
                    Q1.Close;
                    Q1.SQL.Text := 'Update Lingueconosciute set Lingua=:xLingua:, Livello=:xlivello:, IDLingua=:xIDLingua:' +
                         'where id = ' + inttostr(xRecLingua1);
                    Q1.ParamByName['xLingua'] := ELingua1.Text;
                    Q1.ParamByName['xLivello'] := QLivLingue1.FieldByName('ID').AsInteger;
                    Q1.ParamByname['xIDLingua'] := xIDLingua1;
                    Q1.ExecSQL;
               end
               else
               begin
                    Q1.Close;
                    Q1.SQL.Text := 'insert into LingueConosciute (IDAnagrafica,Lingua,Livello,IDLingua)' +
                         'values (:xIDCandidato:,:xLingua:,:xLivello:,:xIDLingua:)';
                    Q1.ParamByName['xIDCandidato'] := xIDCandidato;
                    Q1.ParamByName['xLingua'] := ELingua1.Text;
                    Q1.ParamByName['xLivello'] := QLivLingue1.FieldByName('ID').AsInteger;
                    Q1.ParamByName['xIDLingua'] := xIDLingua1;
                    Q1.ExecSQL;
               end;
          end;
          if ELingua2.Text <> '' then begin
               if xRecLingua2 > 0 then begin
                    Q1.Close;
                    Q1.SQL.Text := 'Update Lingueconosciute set Lingua=:xLingua:, Livello=:xlivello:, IDLingua=:xIDLingua:' +
                         'where id = ' + inttostr(xRecLingua2);
                    Q1.ParamByName['xLingua'] := ELingua2.Text;
                    Q1.ParamByName['xLivello'] := QLivLingue2.FieldByName('ID').AsInteger;
                    Q1.ParamByname['xIDLingua'] := xIDLingua2;
                    Q1.ExecSQL;
               end
               else
               begin
                    Q1.Close;
                    Q1.SQL.Text := 'insert into LingueConosciute (IDAnagrafica,Lingua,Livello,IDLingua)' +
                         'values (:xIDCandidato:,:xLingua:,:xLivello:,:xIDLingua:)';
                    Q1.ParamByName['xIDCandidato'] := xIDCandidato;
                    Q1.ParamByName['xLingua'] := ELingua2.Text;
                    Q1.ParamByName['xLivello'] := QLivLingue2.FieldByName('ID').AsInteger;
                    Q1.ParamByName['xIDLingua'] := xIDLingua2;
                    Q1.ExecSQL;
               end;
          end;
          if ELingua3.Text <> '' then begin
               if xRecLingua3 > 0 then begin
                    Q1.Close;
                    Q1.SQL.Text := 'Update Lingueconosciute set Lingua=:xLingua:, Livello=:xlivello:, IDLingua=:xIDLingua:' +
                         'where id = ' + inttostr(xRecLingua3);
                    Q1.ParamByName['xLingua'] := ELingua3.Text;
                    Q1.ParamByName['xLivello'] := QLivLingue3.FieldByName('ID').AsInteger;
                    Q1.ParamByname['xIDLingua'] := xIDLingua3;
                    Q1.ExecSQL;
               end
               else
               begin
                    Q1.Close;
                    Q1.SQL.Text := 'insert into LingueConosciute (IDAnagrafica,Lingua,Livello,IDLingua)' +
                         'values (:xIDCandidato:,:xLingua:,:xLivello:,:xIDLingua:)';
                    Q1.ParamByName['xIDCandidato'] := xIDCandidato;
                    Q1.ParamByName['xLingua'] := ELingua3.Text;
                    Q1.ParamByName['xLivello'] := QLivLingue3.FieldByName('ID').AsInteger;
                    Q1.ParamByName['xIDLingua'] := xIDLingua3;
                    Q1.ExecSQL;
               end;
          end;
          if ELingua4.Text <> '' then begin
               if xRecLingua4 > 0 then begin
                    Q1.Close;
                    Q1.SQL.Text := 'Update Lingueconosciute set Lingua=:xLingua:, Livello=:xlivello:, IDLingua=:xIDLingua:' +
                         'where id = ' + inttostr(xRecLingua4);
                    Q1.ParamByName['xLingua'] := ELingua4.Text;
                    Q1.ParamByName['xLivello'] := QLivLingue4.FieldByName('ID').AsInteger;
                    Q1.ParamByname['xIDLingua'] := xIDLingua4;
                    Q1.ExecSQL;
               end
               else
               begin
                    Q1.Close;
                    Q1.SQL.Text := 'insert into LingueConosciute (IDAnagrafica,Lingua,Livello,IDLingua)' +
                         'values (:xIDCandidato:,:xLingua:,:xLivello:,:xIDLingua:)';
                    Q1.ParamByName['xIDCandidato'] := xIDCandidato;
                    Q1.ParamByName['xLingua'] := ELingua4.Text;
                    Q1.ParamByName['xLivello'] := QLivLingue4.FieldByName('ID').AsInteger;
                    Q1.ParamByName['xIDLingua'] := xIDLingua4;
                    Q1.ExecSQL;
               end;
          end;
          //Esperienze Lavorative
          if ESettore1.text <> '' then begin
               if xRecEspLav1 > 0 then begin
                    Q1.Close;
                    Q1.SQL.Text := 'Update Esperienzelavorative set IDAzienda=:xIDAzienda:,AziendaNome=:xAziendaNome:,' +
                         'IDSettore=:xIDSettore: ,Attuale=:xAttuale: ,IDArea=:xIDArea: ,IDMansione=:xIDMansione:,' +
                         'DescrizioneMansione=:xDescrizioneMansione:  where id = ' + IntToStr(xRecEspLav1);
                    Q1.ParamByName['xIDAzienda'] := xIDAzienda1;
                    Q1.ParamByName['xAziendaNome'] := copy(EAzienda1.Text, 1, 30);
                    Q1.ParamByName['xIDSettore'] := xIDAzSettore1;
                    Q1.ParamByName['xIDAreaSettore'] := xIDAreaSettore1;
                    Q1.ParamByName['xAttuale'] := CBEspAtt1.Checked;
                    Q1.ParamByName['xDescrizioneMansione'] := EDescPos1.Text;
                    //Q1.ParamByName['xDescrizionAttivitaAz']:=xDescAttivitaAz1;

                    if CBAzienda1.Checked then begin
                         Q1.ParamByName['xIDArea'] := xIDArea1;
                         Q1.ParamByName['xIDMansione'] := xIDRuolo1;
                    end else begin
                         Q1.ParamByName['xIDArea'] := 0;
                         Q1.ParamByName['xIDMansione'] := 0;
                    end;
                    Q1.ExecSQL;
               end
               else
               begin
                    Q1.SQL.text := 'insert into EsperienzeLavorative (IDAnagrafica,IDAzienda,AziendaNome,IDSettore,IDAreaSettore,Attuale,IDArea,IDMansione,DescrizioneMansione)' +
                         'values (:xIDCandidato:,:xIDAzienda:,:xAziendaNome:,:xIDSettore:,:xIDAreaSettore:,:xAttuale:,:xIDArea:,:xIDMansione:,:xDescrizioneMansione:)';
                    Q1.ParamByName['xIDCandidato'] := xIDCandidato;
                    Q1.ParamByName['xIDAzienda'] := xIDAzienda1;
                    Q1.ParamByName['xAziendaNome'] := copy(EAzienda1.Text, 1, 30);
                    Q1.ParamByName['xIDSettore'] := xIDAzSettore1;
                    Q1.ParamByName['xIDAreaSettore'] := xIDAreaSettore1;
                    Q1.ParamByName['xAttuale'] := CBEspAtt1.Checked;
                    Q1.ParamByName['xDescrizioneMansione'] := EDescPos1.text;
                    //Q1.ParamByName['xDescrizionAttivitaAz']:=xDescAttivitaAz1;

                    if CBAzienda1.Checked then begin
                         Q1.ParamByName['xIDArea'] := xIDArea1;
                         Q1.ParamByName['xIDMansione'] := xIDRuolo1;
                    end else begin
                         Q1.ParamByName['xIDArea'] := 0;
                         Q1.ParamByName['xIDMansione'] := 0;
                    end;
                    Q1.ExecSQL;
               end;
          end;

          if ESettore2.text <> '' then begin
               if xRecEspLav2 > 0 then begin
                    Q1.Close;
                    Q1.SQL.Text := 'Update Esperienzelavorative set IDAzienda=:xIDAzienda:,AziendaNome=:xAziendaNome:,' +
                         'IDSettore=:xIDSettore: ,Attuale=:xAttuale: ,IDArea=:xIDArea: ,IDMansione=:xIDMansione:,' +
                         'DescrizioneMansione=:xDescrizioneMansione: where id = ' + IntToStr(xRecEspLav2);
                    Q1.ParamByName['xIDAzienda'] := xIDAzienda2;
                    Q1.ParamByName['xAziendaNome'] := copy(EAzienda2.Text, 1, 30);
                    Q1.ParamByName['xIDSettore'] := xIDAzSettore2;
                    Q1.ParamByName['xIDAreaSettore'] := xIDAreaSettore2;
                    Q1.ParamByName['xAttuale'] := CBEspAtt2.Checked;
                    Q1.ParamByName['xDescrizioneMansione'] := EDescPos2.text;
                    //Q1.ParamByName['xDescrizionAttivitaAz']:=xDescAttivitaAz2;

                    if CBAzienda2.Checked then begin
                         Q1.ParamByName['xIDArea'] := xIDArea2;
                         Q1.ParamByName['xIDMansione'] := xIDRuolo2;
                    end else begin
                         Q1.ParamByName['xIDArea'] := 0;
                         Q1.ParamByName['xIDMansione'] := 0;
                    end;
                    Q1.ExecSQL;
               end
               else
               begin
                    Q1.SQL.text := 'insert into EsperienzeLavorative (IDAnagrafica,IDAzienda,AziendaNome,IDSettore,IDAreaSettore,Attuale,IDArea,IDMansione,DescrizioneMansione)' +
                         'values (:xIDCandidato:,:xIDAzienda:,:xAziendaNome:,:xIDSettore:,:xIDAreaSettore:,:xAttuale:,:xIDArea:,:xIDMansione:,:xDescrizioneMansione:)';
                    Q1.ParamByName['xIDCandidato'] := xIDCandidato;
                    Q1.ParamByName['xIDAzienda'] := xIDAzienda2;
                    Q1.ParamByName['xAziendaNome'] := copy(EAzienda2.Text, 1, 30);
                    Q1.ParamByName['xIDSettore'] := xIDAzSettore2;
                    Q1.ParamByName['xIDAreaSettore'] := xIDAreaSettore2;
                    Q1.ParamByName['xAttuale'] := CBEspAtt2.Checked;
                    Q1.ParamByName['xDescrizioneMansione'] := EDescPos2.text;
                    //Q1.ParamByName['xDescrizionAttivitaAz']:=xDescAttivitaAz2;

                    if CBAzienda2.Checked then begin
                         Q1.ParamByName['xIDArea'] := xIDArea2;
                         Q1.ParamByName['xIDMansione'] := xIDRuolo2;
                    end else begin
                         Q1.ParamByName['xIDArea'] := 0;
                         Q1.ParamByName['xIDMansione'] := 0;
                    end;
                    Q1.ExecSQL;
               end;
          end;


          if ESettore3.text <> '' then begin
               if xRecEspLav3 > 0 then begin
                    Q1.Close;
                    Q1.SQL.Text := 'Update Esperienzelavorative set IDAzienda=:xIDAzienda:,AziendaNome=:xAziendaNome:,' +
                         'IDSettore=:xIDSettore: ,Attuale=:xAttuale: ,IDArea=:xIDArea: ,IDMansione=:xIDMansione:,' +
                         'DescrizioneMansione=:xDescrizioneMansione: where id = ' + IntToStr(xRecEspLav3);
                    Q1.ParamByName['xIDAzienda'] := xIDAzienda3;
                    Q1.ParamByName['xAziendaNome'] := copy(EAzienda3.Text, 1, 30);
                    Q1.ParamByName['xIDSettore'] := xIDAzSettore3;
                    Q1.ParamByName['xIDAreaSettore'] := xIDAreaSettore3;
                    Q1.ParamByName['xAttuale'] := CBEspAtt3.Checked;
                    Q1.ParamByName['xDescrizioneMansione'] := EDescPos3.text;
                    //Q1.ParamByName['xDescrizionAttivitaAz']:=xDescAttivitaAz3;

                    if CBAzienda3.Checked then begin
                         Q1.ParamByName['xIDArea'] := xIDArea3;
                         Q1.ParamByName['xIDMansione'] := xIDRuolo3;
                    end else begin
                         Q1.ParamByName['xIDArea'] := 0;
                         Q1.ParamByName['xIDMansione'] := 0;
                    end;
                    Q1.ExecSQL;
               end
               else
               begin
                    Q1.SQL.text := 'insert into EsperienzeLavorative (IDAnagrafica,IDAzienda,AziendaNome,IDSettore,IDAreaSettore,Attuale,IDArea,IDMansione,DescrizioneMansione)' +
                         'values (:xIDCandidato:,:xIDAzienda:,:xAziendaNome:,:xIDSettore:,:xIDAreaSettore:,:xAttuale:,:xIDArea:,:xIDMansione:,:xDescrizioneMansione:)';
                    Q1.ParamByName['xIDCandidato'] := xIDCandidato;
                    Q1.ParamByName['xIDAzienda'] := xIDAzienda3;
                    Q1.ParamByName['xAziendaNome'] := copy(EAzienda3.Text, 1, 30);
                    Q1.ParamByName['xIDSettore'] := xIDAzSettore3;
                    Q1.ParamByName['xIDAreaSettore'] := xIDAreaSettore3;
                    Q1.ParamByName['xAttuale'] := CBEspAtt3.Checked;
                    Q1.ParamByName['xDescrizioneMansione'] := EDescPos3.text;
                    //Q1.ParamByName['xDescrizionAttivitaAz']:=xDescAttivitaAz3;

                    if CBAzienda3.Checked then begin
                         Q1.ParamByName['xIDArea'] := xIDArea3;
                         Q1.ParamByName['xIDMansione'] := xIDRuolo3;
                    end else begin
                         Q1.ParamByName['xIDArea'] := 0;
                         Q1.ParamByName['xIDMansione'] := 0;
                    end;
                    Q1.ExecSQL;
               end;
          end;

          ECognome.readonly := true;
          ENome.readonly := true;
          DEDataNascita.enabled := false;
          DEDataAggCV.enabled := false;
          CBSesso.Enabled := false;
          ECellulare.readonly := true;
          EEmail.readonly := true;
          ETelefoni.readonly := true;
          ETelUfficio.readonly := true;
          EComuneDom.readonly := true;
          ECapDom.readonly := true;
          EProv.readonly := true;
          EDiploma1.readonly := true;
          CbMaster.enabled := false;
          EArea1.readonly := true;
          ERuolo1.readonly := true;
          EDescPos1.readonly := true;
          CBAzienda1.enabled := false;
          EArea2.readonly := true;
          ERuolo2.readonly := true;
          EDescPos2.readonly := true;
          CBAzienda2.enabled := false;
          EArea3.readonly := true;
          ERuolo3.readonly := true;
          EDescPos3.readonly := true;
          CBAzienda3.enabled := false;
          ESettore1.readonly := true;
          EAreaSettore1.readonly := true;
          EAzienda1.readonly := true;
          ESettore2.readonly := true;
          EAreaSettore3.readonly := true;
          EAzienda2.readonly := true;
          ESettore3.readonly := true;
          EAreaSettore3.readonly := true;
          EAzienda3.readonly := true;
          ELingua1.readonly := true;
          ELingua2.readonly := true;
          ELingua3.readonly := true;
          ELingua4.readonly := true;
          //if QCVAnnData.Active then QCVAnnData.Close;
          EProvInteresse.readonly := true;
          EInquadramento.readonly := true;
          ERetribuzione.readonly := true;
          EBenefits.readonly := true;
          // propriet� CV
          EPropCV.readonly := true;
          EPropCV.Visible := False;
          CBPropMio.enabled := false;
          // altri dati
          {CBMobilita.Checked:=False;
          CBCassaInteg.Checked:=False;
          CBCategProtette.Checked:=False;
          CBDispPartTime.Checked:=False;
          CBDispInterinale.Checked:=False;}
          // provenienza CV
          CBProvCV.text := '';

          EArea.readonly := true;

          //CBRuoloAtt1.Checked:=False;
          iDubbioRuolo1.imageindex := 2;
          //CBRuoloAtt2.Checked:=False;
          iDubbioRuolo2.imageindex := 2;
          //CBRuoloAtt3.Checked:=False;
          iDubbioRuolo3.imageindex := 2;
          //CBEspAtt1.Checked:=False;
          //CBEspAtt2.Checked:=False;
          //CBEspAtt3.Checked:=False;
          CBEspAtt1.Enabled := false;
          CBEspAtt2.Enabled := false;
          CBEspAtt3.Enabled := false;
          CBRuoloAtt1.Enabled := false;
          CBRuoloAtt2.Enabled := false;
          CBRuoloAtt3.Enabled := false;
          CBNoCv.Enabled := false;
          CBTempMG.Enabled := false;
          CBLibPrivacy.Enabled := false;

          BitBtn1.Enabled := false;
          BitBtn2.Enabled := false;
          BSelDiploma.Enabled := false;
          BSelRuolo1.Enabled := false;
          BSelRuolo2.Enabled := false;
          BSelRuolo3.Enabled := false;
          BSelAzienda1.Enabled := false;
          BSelAzienda2.Enabled := false;
          BSelAzienda3.Enabled := false;
          BSelLingua1.Enabled := false;
          BSelLingua2.Enabled := false;
          BSelLingua3.Enabled := false;
          BSelLingua4.Enabled := false;

          BitBtn3.Enabled := false;
          BitBtn4.Enabled := false;
          BitBtn5.Enabled := false;

          DBGrid2.Enabled := false;
          DBGrid3.Enabled := false;
          DBGrid4.Enabled := false;
          DBGrid5.Enabled := false;

          Data.DB.CommitTrans;

          TTitoliStudio.Close;
          TAnagRuoli.Close;
          TEspLav.Close;
          TLingueConosc.Close;
          TAnagComp.Close;


     except
          Data.DB.RollbackTrans;
          MessageDlg('ATTENZIONE: errore sul database' + chr(13) +
               'Inserimenti NON effettuati !', mtError, [mbOK], 0);
          raise;
     end;
     Bmodifica.Enabled := true;
     BSalva.Enabled := false;
     BAnnulla.Enabled := false;
end;

procedure TRiepilogoCandForm.BModificaClick(Sender: TObject);
begin


     ECognome.readonly := false;
     ENome.readonly := false;
     DEDataNascita.enabled := true;
     DEDataAggCV.enabled := true;
     CBSesso.enabled := true;
     ECellulare.readonly := false;
     EEmail.readonly := false;
     ETelefoni.readonly := false;
     ETelUfficio.readonly := false;
     EComuneDom.readonly := false;
     ECapDom.readonly := false;
     EProv.readonly := false;
     EDiploma1.readonly := false;
     CbMaster.enabled := true;
     EArea1.readonly := false;
     ERuolo1.readonly := false;
     EDescPos1.readonly := false;
     CBAzienda1.enabled := true;
     EArea2.readonly := false;
     ERuolo2.readonly := false;
     EDescPos2.readonly := false;
     CBAzienda2.enabled := true;
     EArea3.readonly := false;
     ERuolo3.readonly := false;
     EDescPos3.readonly := false;
     CBAzienda3.enabled := true;
     ESettore1.readonly := false;
     EAreaSettore1.readonly := false;
     EAzienda1.readonly := false;
     ESettore2.readonly := false;
     EAreaSettore3.readonly := false;
     EAzienda2.readonly := false;
     ESettore3.readonly := false;
     EAreaSettore3.readonly := false;
     EAzienda3.readonly := false;
     ELingua1.readonly := false;
     ELingua2.readonly := false;
     ELingua3.readonly := false;
     ELingua4.readonly := false;
     //if QCVAnnData.Active then QCVAnnData.Close;
     EProvInteresse.readonly := false;
     EInquadramento.readonly := false;
     ERetribuzione.readonly := false;
     EBenefits.readonly := false;
     // propriet� CV
     EPropCV.readonly := false;
     EPropCV.Visible := False;
     CBPropMio.enabled := true;
     // altri dati
     {CBMobilita.Checked:=False;
     CBCassaInteg.Checked:=False;
     CBCategProtette.Checked:=False;
     CBDispPartTime.Checked:=False;
     CBDispInterinale.Checked:=False;}
     // provenienza CV
     CBProvCV.text := '';

     EArea.readonly := true;

     //CBRuoloAtt1.Checked:=False;
     iDubbioRuolo1.imageindex := 2;
     //CBRuoloAtt2.Checked:=False;
     iDubbioRuolo2.imageindex := 2;
     //CBRuoloAtt3.Checked:=False;
     iDubbioRuolo3.imageindex := 2;
     //CBEspAtt1.Checked:=False;
     //CBEspAtt2.Checked:=False;
     //CBEspAtt3.Checked:=False;

     CBEspAtt1.Enabled := true;
     CBEspAtt2.Enabled := true;
     CBEspAtt3.Enabled := true;
     CBRuoloAtt1.Enabled := true;
     CBRuoloAtt2.Enabled := true;
     CBRuoloAtt3.Enabled := true;
     CBNoCv.Enabled := true;
     CBTempMG.Enabled := true;
     CBLibPrivacy.Enabled := true;

     BitBtn1.enabled := true;
     BitBtn2.enabled := true;
     BSelDiploma.enabled := true;
     BSelRuolo1.enabled := true;
     BSelRuolo2.enabled := true;
     BSelRuolo3.enabled := true;
     BSelAzienda1.enabled := true;
     BSelAzienda2.enabled := true;
     BSelAzienda3.enabled := true;
     BSelLingua1.enabled := true;
     BSelLingua2.enabled := true;
     BSelLingua3.enabled := true;
     BSelLingua4.enabled := true;

     BitBtn3.Enabled := true;
     BitBtn4.Enabled := true;
     BitBtn5.Enabled := true;

     BSalva.Enabled := true;
     BAnnulla.Enabled := true;
     Bmodifica.Enabled := false;


     DBGrid2.Enabled := true;
     DBGrid3.Enabled := true;
     DBGrid4.Enabled := true;
     DBGrid5.Enabled := true;

end;

procedure TRiepilogoCandForm.BAnnullaClick(Sender: TObject);
begin

     RiepilogoCandForm.FormShow(self);

     BSalva.Enabled := false;
     BAnnulla.Enabled := false;
     Bmodifica.Enabled := true;
end;

procedure TRiepilogoCandForm.BEsciClick(Sender: TObject);
begin
     close
end;

procedure TRiepilogoCandForm.DEDataNascitaEnter(Sender: TObject);
begin
     //     if qMasAnag.state<>dsinactive then
       //        DEDataNascita.Date:=qMasAnag.FieldByName('DataNascita').AsDatetime;
end;

end.
