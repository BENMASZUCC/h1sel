unit RelazCand;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ExtCtrls, RelazioniCand, StdCtrls, Buttons;

type
     TRelazCandForm = class(TForm)
          Panel1: TPanel;
          BitBtn1: TBitBtn;
          RelazioniCandFrame1: TRelazioniCandFrame;
          ECognome: TEdit;
          ENome: TEdit;
          procedure FormShow(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     RelazCandForm: TRelazCandForm;

implementation

{$R *.DFM}

procedure TRelazCandForm.FormShow(Sender: TObject);
begin
     RelazioniCandFrame1.xTipo := 'A';
     RelazioniCandFrame1.CreaAlbero;
end;

end.

