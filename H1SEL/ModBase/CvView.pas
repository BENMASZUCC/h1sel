unit CvView;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ImageEnView, ImageEn, ExtCtrls, StdCtrls, Buttons, ComCtrls;

type
  TCvViewForm = class(TForm)
    Panel1: TPanel;
    ImageEn1: TImageEn;
    TrackBar1: TTrackBar;
    Label1: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    ZoomLbl: TEdit;
    BitBtn3: TBitBtn;
    procedure TrackBar1Change(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CvViewForm: TCvViewForm;

implementation

{$R *.DFM}

procedure TCvViewForm.TrackBar1Change(Sender: TObject);
begin
     ImageEn1.Zoom:=TrackBar1.Position;
     ZoomLbl.Text:=IntToStr(TrackBar1.Position);
end;

procedure TCvViewForm.BitBtn1Click(Sender: TObject);
begin
     ImageEn1.Fit;
     ZoomLbl.Text:=IntToStr(ImageEn1.Zoom);
end;

procedure TCvViewForm.BitBtn2Click(Sender: TObject);
begin
     ImageEn1.Zoom:=100;
     ZoomLbl.Text:=IntToStr(100);
end;

procedure TCvViewForm.BitBtn3Click(Sender: TObject);
begin
     close
end;

end.
