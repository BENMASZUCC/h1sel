unit Azienda;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Grids, DBGrids, Db, DBTables, StdCtrls, Buttons, ExtCtrls, FloatEdit, Mask,
     ToolEdit, CurrEdit, dxCntner, dxEditor, dxExEdtr, dxEdLib, ADODB, U_ADOLinkCl;
type
     TAziendaForm = class(TForm)
          Label1: TLabel;
          EDescrizione: TEdit;
          BitBtn1: TBitBtn;
          BitBtn2: TBitBtn;
          Label2: TLabel;
          Label3: TLabel;
          ECap: TEdit;
          EComune: TEdit;
          Label5: TLabel;
          EProv: TEdit;
          DsSettori: TDataSource;
          DBGrid1: TDBGrid;
          RG1: TRadioGroup;
          GroupBox1: TGroupBox;
          Label6: TLabel;
          Label7: TLabel;
          FENumDip: TFloatEdit;
          SpeedButton1: TSpeedButton;
          EIndirizzo: TEdit;
          CBTipoStrada: TComboBox;
          ENumCivico: TEdit;
          Label8: TLabel;
          ENazione: TEdit;
          BNaz: TSpeedButton;
          FEFatturato: TRxCalcEdit;
          ETelefono: TEdit;
          EFax: TEdit;
          Label4: TLabel;
          Label9: TLabel;
          Label10: TLabel;
          ESitoInternet: TdxHyperLinkEdit;
          Label11: TLabel;
          EDescAttAzienda: TMemo;
          Q: TADOLinkedQuery;
          TSettori: TADOLinkedQuery;
          TSettoriID: TAutoIncField;
          TSettoriAttivita: TStringField;
          TSettoriIDAreaSettore: TIntegerField;
          TSettoriIDAzienda: TStringField;
          TSettoriAttivita_ENG: TStringField;
          Label12: TLabel;
          Label13: TLabel;
          cbStato: TComboBox;
          cbTipoAzienda: TComboBox;
          qTipiAzienda: TADOQuery;
          qTipiAziendaTipoAzienda: TStringField;
          dbgTipoAzienda: TDBGrid;
          dsTipiAziende: TDataSource;
          qTipiAziendaID: TAutoIncField;
          procedure DBGrid1KeyPress(Sender: TObject; var Key: Char);
          procedure SpeedButton1Click(Sender: TObject);
          procedure CBTipoStradaDropDown(Sender: TObject);
          procedure BNazClick(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure cbTipoAziendaDropDown(Sender: TObject);
     private
          { Private declarations }
     public
          xUltimaProv: string;
     end;

var
     AziendaForm: TAziendaForm;

implementation

uses Comuni, SelNazione,
     //[/TONI20020725\]
     ModuloDati;
//[/TONI20020725\]FINE

{$R *.DFM}

procedure TAziendaForm.DBGrid1KeyPress(Sender: TObject; var Key: Char);
begin
     if key = chr(13) then BitBtn1.Click;
end;

procedure TAziendaForm.SpeedButton1Click(Sender: TObject);
begin
     ComuniForm := TComuniForm.create(self);
     ComuniForm.EProv.Text := xUltimaProv;
     ComuniForm.ShowModal;
     if ComuniForm.ModalResult = mrOK then begin
          //[/TONI20020725\]
     {          ECap.Text:=ComuniForm.QComuniCap.Value;
               EComune.Text:=ComuniForm.QComuniDescrizione.Value;
               EProv.text:=ComuniForm.QComuniProv.Value;}
          ECap.Text := ComuniForm.QComuni.FieldByName('Cap').Value;
          EComune.Text := ComuniForm.QComuni.FieldByName('Descrizione').Value;
          EProv.text := ComuniForm.QComuni.FieldByName('Prov').Value;
          //[/TONI20020725\]FINE
     end else begin
          ECap.Text := '';
          EComune.Text := '';
          EProv.text := '';
     end;
     ComuniForm.Free;
end;

procedure TAziendaForm.CBTipoStradaDropDown(Sender: TObject);
var xNazioneAzienda: string;
begin
     // tipo di strada in funzione della Nazione - RESIDENZA
     Q.Close;
     Q.SQL.text := 'select NazioneAbbAzienda from Global';
     Q.Open;
     xNazioneAzienda := Q.FieldByName('NazioneAbbAzienda').asString;
     Q.Close;
     //[/TONI20020725\]
{     Q.SQL.text:='select TipoStrada from TipiStrade where NazioneAbb=:xNazioneAbb';
     if ENazione.text<>'' then
          Q.ParamByName('xNazioneAbb').asString:=ENazione.text
     else Q.ParamByName('xNazioneAbb').asString:=xNazioneAzienda;}
     if ENazione.text <> '' then
          Q.SetSQLText(['select TipoStrada from TipiStrade where NazioneAbb=:xNazioneAbb:',
               ENazione.text])
     else
          Q.SetSQLText(['select TipoStrada from TipiStrade where NazioneAbb=:xNazioneAbb:',
               xNazioneAzienda]);
     //[/TONI20020725\]FINE
     Q.Open;
     CBTipoStrada.Items.Clear;
     while not Q.EOF do begin
          CBTipoStrada.Items.Add(Q.FieldByName('TipoStrada').asString);
          Q.next;
     end;
     Q.Close;
end;

procedure TAziendaForm.BNazClick(Sender: TObject);
begin
     SelNazioneForm := TSelNazioneForm.create(self);
     SelNazioneForm.ShowModal;
     if (SelNazioneForm.ModalResult = mrOK) and (SelNazioneForm.QNazioni.RecordCount > 0) then begin
          //[/TONI20020725\]
     //          ENazione.text:=SelNazioneForm.QNazioniAbbrev.Value;
          ENazione.text := SelNazioneForm.QNazioni.FieldByName('Abbrev').Value;
          //[/TONI20020725\]FINE
     end else begin
          ENazione.text := '';
     end;
     SelNazioneForm.Free;
end;

procedure TAziendaForm.FormShow(Sender: TObject);
begin
     Caption := '[S/15] - ' + Caption;
     if cbStato.Text = '' then cbStato.Text := 'non attivo';
end;

procedure TAziendaForm.cbTipoAziendaDropDown(Sender: TObject);
begin
     cbTipoAzienda.Items.Clear;
     if qTipiAzienda.Active = false then qTipiAzienda.Open;
     while not qTipiAzienda.Eof do begin
          cbTipoAzienda.Items.Add(qTipiAziendaTipoAzienda.Value);
          qTipiAzienda.Next;
     end;
end;

end.
