unit SelPersNew;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Mask, DBCtrls, Grids, DBGrids, Db, DBTables, Buttons, TB97,
     ExtCtrls, ComCtrls, ComObj, RXDBCtrl, dxDBGrid, dxDBTLCl, dxGrClms, dxTL,
     dxDBCtrl, dxCntner, dxGridMenus, ImgList, Menus, dxPSCore, dxPSdxTLLnk,
     dxPSdxDBGrLnk, Spin, dxPSdxDBCtrlLnk, ADODB, U_ADOLinkCl, DBCGrids,
     dxEditor, dxExEdtr, dxEdLib, dxDBELib, OleCtrls, SHDocVw, ToolEdit,
     JvBaseDlg, JvSelectDirectory, BaseGrid, AdvGrid, DBAdvGrid, ActnList;

type
     TSaveMethod = procedure(const FileName: string; ASaveAll: Boolean) of object;
     TSelPersNewForm = class(TForm)
          DsTabQuery: TDataSource;
          DsTabOp: TDataSource;
          DsLinee: TDataSource;
          DsTabLookup: TDataSource;
          DsQRes1: TDataSource;
          DsQTabLista: TDataSource;
          PanRic: TPanel;
          PMExport: TPopupMenu;
          Stampaelenco1: TMenuItem;
          EsportainExcel1: TMenuItem;
          EsportainHTML1: TMenuItem;
          ImageList1: TImageList;
          SaveDialog: TSaveDialog;
          Stampagriglia1: TMenuItem;
          dxPrinter1: TdxComponentPrinter;
          cancellasoggetti1: TMenuItem;
          PMStati: TPopupMenu;
          PMStati1: TMenuItem;
          PMStati2: TMenuItem;
          PMStati3: TMenuItem;
          PMStati4: TMenuItem;
          PMStati5: TMenuItem;
          cancellatuttiisoggettiselezionati1: TMenuItem;
          mettiinstatoeliminato1: TMenuItem;
          TLinee: TADOLinkedQuery;
          QTabelleLista: TADOLinkedQuery;
          TTabLookup: TADOLinkedTable;
          TTabQuery: TADOLinkedQuery;
          QTabelle: TADOLinkedQuery;
          Query1: TADOLinkedQuery;
          QRicLineeQuery: TADOLinkedQuery;
          TTabOp: TADOLinkedTable;
          TRicLineeQuery: TADOLinkedTable;
          Q: TADOLinkedQuery;
          QRes1: TADOLinkedQuery;
          TStatiLK: TADOLinkedTable;
          TTipiStatiLK: TADOLinkedTable;
          QLastEspLav: TADOLinkedQuery;
          QEspLavLK: TADOLinkedQuery;
          QRes1ID: TAutoIncField;
          QRes1Cognome: TStringField;
          QRes1Nome: TStringField;
          QRes1CVNumero: TIntegerField;
          QRes1OldCVNumero: TStringField;
          QRes1TelUfficio: TStringField;
          QRes1Cellulare: TStringField;
          QRes1DataNascita: TDateTimeField;
          QRes1IDStato: TIntegerField;
          QRes1IDTipoStato: TIntegerField;
          QRes1DomicilioComune: TStringField;
          QRes1ggUltimaDataIns: TIntegerField;
          QRes1ggDataUltimoContatto: TIntegerField;
          QRes1DataProssimoColloquio: TDateTimeField;
          QRes1ggDataUltimoColloquio: TIntegerField;
          QRes1ggDataInsCV: TIntegerField;
          TStatiLKID: TAutoIncField;
          TStatiLKStato: TStringField;
          TStatiLKIDTipoStato: TIntegerField;
          TStatiLKPRman: TBooleanField;
          TStatiLKIDAzienda: TIntegerField;
          TTipiStatiLKID: TAutoIncField;
          TTipiStatiLKTipoStato: TStringField;
          QRes1Eta: TStringField;
          QEspLavLKID: TAutoIncField;
          QEspLavLKAzienda: TStringField;
          QEspLavLKRuolo: TStringField;
          QLastEspLavIDAnagrafica: TIntegerField;
          QLastEspLavMaxID: TIntegerField;
          QLastEspLavAzienda: TStringField;
          QLastEspLavRuolo: TStringField;
          QRes1TelUffCell: TStringField;
          OpenDialog1: TOpenDialog;
          QRes1cvinseritoindata: TDateTimeField;
          QRes1cvdatareale: TDateTimeField;
          QRes1cvdataagg: TDateTimeField;
          TTabLookupID: TAutoIncField;
          TTabLookupAttivita: TStringField;
          QRes1DomicilioProvincia: TStringField;
          QRes1Provincia: TStringField;
          QRes1Comune: TStringField;
          PCTipoRic: TPageControl;
          TSRicSemplice: TTabSheet;
          TSRicAv: TTabSheet;
          PanCriteri: TPanel;
          Panel2: TPanel;
          PanCriteriButtons: TPanel;
          BCriterioNew: TToolbarButton97;
          BCriterioMod: TToolbarButton97;
          BCriterioDel: TToolbarButton97;
          BLineeOK: TToolbarButton97;
          BLineeCan: TToolbarButton97;
          BCriterioDelTutti: TToolbarButton97;
          DBGrid3: TdxDBGrid;
          DBGrid3DescTabella: TdxDBGridButtonColumn;
          DBGrid3DescCampo: TdxDBGridButtonColumn;
          DBGrid3DescOperatore: TdxDBGridButtonColumn;
          DBGrid3Valore: TdxDBGridButtonColumn;
          DBGrid3Descrizione: TdxDBGridMaskColumn;
          DBGrid3LivLinguaParlato: TdxDBGridButtonColumn;
          DBGrid3LivLinguaScritto: TdxDBGridButtonColumn;
          DBGrid3OpNext: TdxDBGridPickColumn;
          DBGrid3ID: TdxDBGridColumn;
          DBGrid3QueryLookup: TdxDBGridColumn;
          Panel4: TPanel;
          PanInfoRet: TPanel;
          Label6: TLabel;
          CBIRnote: TCheckBox;
          EIRNote: TEdit;
          CBIRNoteInflectional: TCheckBox;
          CBIRFileSystem: TCheckBox;
          CBIRaltreRegole: TCheckBox;
          DBGrid2: TdxDBGrid;
          DBGrid2ID: TdxDBGridMaskColumn;
          DBGrid2DescCompleta: TdxDBGridMaskColumn;
          DBGrid2Valore: TdxDBGridButtonColumn;
          DBGrid2QueryLookup: TdxDBGridColumn;
          dxPrinter1Link1: TdxDBGridReportLink;
          Q2: TADOLinkedQuery;
          QRes1Retribuzione: TStringField;
          QRes1NoCV: TBooleanField;
          QRes1EMail: TStringField;
          pmInsRic: TPopupMenu;
          Aggiungiinricerca1: TMenuItem;
          Aggiungiadunaricerca1: TMenuItem;
          QRes1FileCheck: TBooleanField;
          QLastEspLavCodPersUnica: TStringField;
          QEspLavLKtitolomansione: TStringField;
          QEspLavLKdescrizionemansione: TMemoField;
          QLastEspLavDEscrizioneMansione: TStringField;
          QRes1RecapitiTelefonici: TStringField;
          Query2: TMenuItem;
          ApriQuery1: TMenuItem;
          SalvaQuery1: TMenuItem;
          N1: TMenuItem;
          Salvaquerysufile1: TMenuItem;
          qUserLineeQuerySalvate: TADOLinkedQuery;
          EliminaQuery1: TMenuItem;
          QEspLavLKJobTitle: TStringField;
          QLastEspLavJobTitle: TStringField;
          TSMotRic: TTabSheet;
          DBCtrlGrid1: TDBCtrlGrid;
          QMotoriRicerca: TADOQuery;
          DSMotoriRicerca: TDataSource;
          QsaveMotoriRicercaPers: TADOQuery;
          dxDBHyperLinkEdit1: TdxDBHyperLinkEdit;
          dxDBHyperLinkEdit2: TdxDBHyperLinkEdit;
          DBMemo1: TDBMemo;
          DBMemo2: TDBMemo;
          Label1: TLabel;
          Label2: TLabel;
          Label3: TLabel;
          Label4: TLabel;
          QMotoriRicercaID: TAutoIncField;
          QMotoriRicercaLinkMotore: TStringField;
          QMotoriRicercaNoteazienda: TMemoField;
          QMotoriRicercaLinkpers: TStringField;
          QMotoriRicercaNotepers: TMemoField;
          qDatiIns: TADOQuery;
          qDatiMod: TADOQuery;
          qDatiModNominativo: TStringField;
          qDatiModDataOra: TDateTimeField;
          qDatiModKeyValue: TIntegerField;
          qDatiInsNominativo: TStringField;
          qDatiInsDataOra: TDateTimeField;
          qDatiInsKeyValue: TIntegerField;
          QRes1daContattocliente: TBooleanField;
          pmPulsantiDx: TPopupMenu;
          Esegui1: TMenuItem;
          N2: TMenuItem;
          Infocolloquio1: TMenuItem;
          Infocand1: TMenuItem;
          Curriculum1: TMenuItem;
          Documenti1: TMenuItem;
          Appuntiword1: TMenuItem;
          aggiungi1: TMenuItem;
          aquestaricerca1: TMenuItem;
          adaltrericerche1: TMenuItem;
          pmWizard: TPopupMenu;
          Nuovocriterio1: TMenuItem;
          Modificacriterio1: TMenuItem;
          Eliminacriterio1: TMenuItem;
          Eliminatutti1: TMenuItem;
          QLastEspLavArea: TStringField;
          QEspLavLKArea: TStringField;
          WebBrowser1: TWebBrowser;
          Timer1: TTimer;
          PMGoogleMaps: TPopupMenu;
          AnagraficaResidenza1: TMenuItem;
          AnagraficaDomicilio1: TMenuItem;
          QRes1domiciliotipostrada: TStringField;
          QRes1domiciliocap: TStringField;
          QRes1domicilionumcivico: TStringField;
          QRes1domicilioindirizzo: TStringField;
          QRes1indirizzo: TStringField;
          QRes1cap: TStringField;
          QRes1numcivico: TStringField;
          QRes1tipostrada: TStringField;
          Estraifilecandidati1: TMenuItem;
          Qestraifile: TADOQuery;
          QestraifileDescrizione: TStringField;
          QestraifileNomeFile: TStringField;
          QestraifileFileType: TStringField;
          QestraifileDocFile: TBlobField;
          QestraifileDocExt: TStringField;
          JvSelectDirectory1: TJvSelectDirectory;
          QestraifileSoloNome: TStringField;
          QPrimoRuolo: TADOQuery;
          QPrimoRuoloIDAnagrafica: TIntegerField;
          QPrimoRuoloMaxID: TIntegerField;
          QprimoRuoloLK: TADOQuery;
          QPrimoRuoloPrimoRuoloLK: TStringField;
          BGrafica: TButton;
          Panel1: TPanel;
          Panel10: TPanel;
          LTot: TLabel;
          Label5: TLabel;
          Panel3: TPanel;
          BLegenda: TToolbarButton97;
          ToolbarButton972: TToolbarButton97;
          BEmail: TToolbarButton97;
          GroupBox2: TGroupBox;
          CBColloquiati: TCheckBox;
          CBPropri: TCheckBox;
          GroupBox3: TGroupBox;
          CBAnzCV: TCheckBox;
          SEAnzCV: TSpinEdit;
          cbanzianita: TComboBox;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1CVNumero: TdxDBGridMaskColumn;
          dxDBGrid1ggUltimaDataIns: TdxDBGridMaskColumn;
          dxDBGrid1ggDataUltimoContatto: TdxDBGridMaskColumn;
          dxDBGrid1DataProssimoColloquio: TdxDBGridDateColumn;
          dxDBGrid1ggDataUltimoColloquio: TdxDBGridMaskColumn;
          dxDBGrid1Cognome: TdxDBGridMaskColumn;
          dxDBGrid1Nome: TdxDBGridMaskColumn;
          dxDBGrid1DataNascita: TdxDBGridDateColumn;
          dxDBGrid1Eta: TdxDBGridColumn;
          dxDBGrid1TelUffCell: TdxDBGridColumn;
          dxDBGrid1DomicilioComune: TdxDBGridMaskColumn;
          dxDBGrid1InsDaContatti: TdxDBGridCheckColumn;
          dxDBGrid1OldCVNumero: TdxDBGridMaskColumn;
          dxDBGrid1Ruolo: TdxDBGridLookupColumn;
          dxDBGrid1Azienda: TdxDBGridLookupColumn;
          dxDBGrid1TelUfficio: TdxDBGridMaskColumn;
          dxDBGrid1Cellulare: TdxDBGridMaskColumn;
          dxDBGrid1ggDataInsCV: TdxDBGridMaskColumn;
          dxDBGrid1IDStato: TdxDBGridColumn;
          dxDBGrid1Column21: TdxDBGridDateColumn;
          dxDBGrid1DataRealeCV: TdxDBGridDateColumn;
          dxDBGrid1Column23: TdxDBGridColumn;
          dxDBGrid1Column24: TdxDBGridColumn;
          dxDBGrid1NoCv: TdxDBGridCheckColumn;
          dxDBGrid1DomicilioProvincia: TdxDBGridMaskColumn;
          dxDBGrid1Provincia: TdxDBGridMaskColumn;
          dxDBGrid1Comune: TdxDBGridMaskColumn;
          dxDBGrid1Retribuzione: TdxDBGridColumn;
          dxDBGrid1EMail: TdxDBGridColumn;
          dxDBGrid1IDAnagrafica: TdxDBGridColumn;
          dxDBGrid1File: TdxDBGridCheckColumn;
          dxDBGrid1Column32: TdxDBGridColumn;
          dxDBGrid1Column33: TdxDBGridColumn;
          dxDBGrid1Column34: TdxDBGridColumn;
          dxDBGrid1Column35: TdxDBGridDateColumn;
          dxDBGrid1Column36: TdxDBGridDateColumn;
          dxDBGrid1Column38: TdxDBGridColumn;
          dxDBGrid1Distanza: TdxDBGridColumn;
          dxDBGrid1Tempo: TdxDBGridColumn;
          dxDBGrid1PrimoRuolo: TdxDBGridColumn;
          PPulsanti: TPanel;
          BEvento: TToolbarButton97;
          BInfoCand: TToolbarButton97;
          BCurriculum: TToolbarButton97;
          BDocumenti: TToolbarButton97;
          BInfoColloquio: TToolbarButton97;
          BAppunti: TToolbarButton97;
          Besci: TToolbarButton97;
          BAggiungiRic: TToolbarButton97;
          Besegui: TToolbarButton97;
          ToolbarButton971: TToolbarButton97;
          BRiepCand: TBitBtn;
          Splitter1: TSplitter;
          QEspLavLKSettore: TStringField;
          QLastEspLavSettore: TStringField;
          dxDBGrid1Settore: TdxDBGridColumn;
          AttacDS: TButton;
          CloseRes1: TButton;
          QRes1emailufficio: TStringField;
          dxDBGrid1EMailUfficio: TdxDBGridColumn;
          QRes1distanza: TStringField;
          QRes1tempo: TStringField;
          BCalcolaDistanza: TToolbarButton97;
          Esportadaticandidati1: TMenuItem;
          QRes_test: TADOQuery;
          QRes_testID: TAutoIncField;
          QRes_testdacontattocliente: TBooleanField;
          QRes_testCognome: TStringField;
          QRes_testNome: TStringField;
          QRes_testCVNumero: TIntegerField;
          QRes_testOldCVNumero: TStringField;
          QRes_testTelUfficio: TStringField;
          QRes_testCellulare: TStringField;
          QRes_testCVInseritoInData: TDateTimeField;
          QRes_testCVDataReale: TDateTimeField;
          QRes_testCVDataAgg: TDateTimeField;
          QRes_testRecapitiTelefonici: TStringField;
          QRes_testDataNascita: TDateTimeField;
          QRes_testIDStato: TIntegerField;
          QRes_testIDTipoStato: TIntegerField;
          QRes_testProvincia: TStringField;
          QRes_testComune: TStringField;
          QRes_testindirizzo: TStringField;
          QRes_testcap: TStringField;
          QRes_testnumcivico: TStringField;
          QRes_testtipostrada: TStringField;
          QRes_testdomiciliotipostrada: TStringField;
          QRes_testdomiciliocap: TStringField;
          QRes_testdomicilionumcivico: TStringField;
          QRes_testdomicilioindirizzo: TStringField;
          QRes_testdomiciliocomune: TStringField;
          QRes_testdomicilioprovincia: TStringField;
          QRes_testggUltimaDataIns: TIntegerField;
          QRes_testggDataUltimoContatto: TIntegerField;
          QRes_testDataProssimoColloquio: TDateTimeField;
          QRes_testggDataUltimoColloquio: TIntegerField;
          QRes_testggDataInsCV: TIntegerField;
          QRes_testNoCv: TBooleanField;
          QRes_testRetribuzione: TStringField;
          QRes_testEMail: TStringField;
          QRes_testInquadramento: TStringField;
          QRes_testFileCheck: TBooleanField;
          QRes_testdistanza: TStringField;
          QRes_testtempo: TStringField;
          QRes_testemailufficio: TStringField;
          SpeedButton1: TSpeedButton;
          QRes_testAzienda: TStringField;
          QRes_testRuolo: TStringField;
          QRes_testdescrizionemansione: TStringField;
          QRes_testJobTitle: TStringField;
          QRes_testArea: TStringField;
          QRes_testSettore: TStringField;
          QRes_testTipoStato: TStringField;
          QRes_testEta: TStringField;
          QRes_testCodPers: TStringField;
          QRes_testTelUffCell: TStringField;
          QRes_testDataIns: TDateTimeField;
          QRes_testDataAgg: TDateTimeField;
          QRes_testPrimoRuolo: TStringField;
          QRes1Inquadramento: TStringField;
          QRes1Azienda: TStringField;
          QRes1CodPers: TStringField;
          QRes1Ruolo: TStringField;
          QRes1descrizionemansione: TStringField;
          QRes1JobTitle: TStringField;
          QRes1Area: TStringField;
          QRes1Settore: TStringField;
          QRes1DataIns: TDateTimeField;
          QRes1DataAgg: TDateTimeField;
          QRes1PrimoRuolo: TStringField;
          QRes1TipoStato: TStringField;
          DsQRicLineeQuery: TDataSource;
          dxDBGrid2: TdxDBGrid;
          dxDBGrid2DescTabella: TdxDBGridButtonColumn;
          dxDBGrid2DescCampo: TdxDBGridButtonColumn;
          dxDBGrid2DescOperatore: TdxDBGridButtonColumn;
          dxDBGrid2Valore: TdxDBGridButtonColumn;
          dxDBGrid2Descrizione: TdxDBGridMaskColumn;
          dxDBGrid2LivLinguaParlato: TdxDBGridButtonColumn;
          dxDBGrid2LivLinguaScritto: TdxDBGridButtonColumn;
          dxDBGrid2OpNext: TdxDBGridPickColumn;
          dxDBGrid2ID: TdxDBGridColumn;
          dxDBGrid2QueryLookup: TdxDBGridColumn;
          ActionList1: TActionList;
          dxDBGrid3: TdxDBGrid;
          dxDBGrid3DescCompleta: TdxDBGridMaskColumn;
          dxDBGrid3ID: TdxDBGridMaskColumn;
          dxDBGrid3Valore: TdxDBGridButtonColumn;
          dxDBGrid3QueryLookup: TdxDBGridColumn;
          QRes1domicilioRegione: TStringField;
          QRes1Regione: TStringField;
          QRes1domicilioStato: TStringField;
          QRes1Stato: TStringField;
          dxDBGrid1domicilioRegione: TdxDBGridColumn;
          dxDBGrid1domicilioStato: TdxDBGridColumn;
          dxDBGrid1Stato: TdxDBGridColumn;
          dxDBGrid1Regione: TdxDBGridColumn;
          QRicLineeQueryID: TAutoIncField;
          QRicLineeQueryIDRicerca: TIntegerField;
          QRicLineeQueryDescrizione: TStringField;
          QRicLineeQueryStringa: TStringField;
          QRicLineeQueryTabella: TStringField;
          QRicLineeQueryOpSucc: TStringField;
          QRicLineeQueryNext: TStringField;
          QRicLineeQueryLivLinguaParlato: TIntegerField;
          QRicLineeQueryLivLinguaScritto: TIntegerField;
          QRicLineeQueryDescTabella: TStringField;
          QRicLineeQueryDescCampo: TStringField;
          QRicLineeQueryDescOperatore: TStringField;
          QRicLineeQueryValore: TStringField;
          QRicLineeQueryQueryLookup: TStringField;
          QRicLineeQueryDescCompleta: TStringField;
          QRicLineeQueryIDTabQueryOp: TIntegerField;
          QRicLineeQueryNoteFullText: TMemoField;
          TLineeID: TAutoIncField;
          TLineeIDAnagrafica: TIntegerField;
          TLineeDescrizione: TStringField;
          TLineeStringa: TStringField;
          TLineeTabella: TStringField;
          TLineeLivLinguaParlato: TIntegerField;
          TLineeLivLinguaScritto: TIntegerField;
          TLineeDescTabella: TStringField;
          TLineeDescCampo: TStringField;
          TLineeDescOperatore: TStringField;
          TLineeValore: TStringField;
          TLineeQueryLookup: TStringField;
          TLineeDescCompleta: TStringField;
          TLineeIDTabQueryOp: TIntegerField;
          TLineeNoteFullText: TMemoField;
          dxDBGrid1AutoCandidatura: TdxDBGridCheckColumn;
          QRes1Autocandidatura: TBooleanField;
          dxDBGrid1Indirizzo: TdxDBGridColumn;
          dxDBGrid1domicilioindirizzo: TdxDBGridColumn;
          dxDBGrid1Cap: TdxDBGridColumn;
          dxDBGrid1CapDomicilio: TdxDBGridColumn;
          QRes1UltimoAnnuncio: TStringField;
          dxDBGrid1UltimoAnnuncio: TdxDBGridColumn;
          QRes1StatoAna: TStringField;
          dxDBGrid1StatoAna: TdxDBGridColumn;
          BFiltroStati: TToolbarButton97;
          dxDBGrid1TipoStato: TdxDBGridColumn;
          MiniProfilo1: TMenuItem;
          MiniProfiloAnonimo1: TMenuItem;
          TLineeDistanzaComuneKm: TIntegerField;
          QRicLineeQueryDistanzaComuneKm: TIntegerField;
          dxDBGrid2Km: TdxDBGridButtonColumn;
          TLineeOpSucc: TStringField;
          TLineeOpNext: TStringField;
          DBGrid3Km: TdxDBGridButtonColumn;
          PMStati6: TMenuItem;
          PMStati7: TMenuItem;
          cbAdvFullText: TCheckBox;
          QRes1AreaSettore: TStringField;
          dxDBGrid1AreaSettore: TdxDBGridColumn;
          QestraifileTipo: TStringField;
    CBSenzaTitoloStudio: TCheckBox;
          procedure ComboBox1Change(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure DBGrid4KeyPress(Sender: TObject; var Key: Char);
          procedure BDocumentiClick(Sender: TObject);
          //procedure BAggiungiRicClick(Sender: TObject);
          procedure TLinee_OLDBeforePost(DataSet: TDataSet);
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
          procedure BCurriculumClick(Sender: TObject);
          procedure BAppunClick(Sender: TObject);
          procedure BitBtn10Click(Sender: TObject);
          procedure QRes1_OLDCalcFields(DataSet: TDataSet);
          procedure CBColloquiatiClick(Sender: TObject);
          procedure QRes1_OLDBeforeOpen(DataSet: TDataSet);
          procedure QRes1_OLDAfterClose(DataSet: TDataSet);
          procedure dxDBGrid1Click(Sender: TObject);
          procedure dxDBGrid1CustomDrawCell(Sender: TObject; ACanvas: TCanvas;
               ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
               ASelected, AFocused, ANewItemRow: Boolean; var AText: string;
               var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
               var ADone: Boolean);
          procedure dxDBGrid1MouseUp(Sender: TObject; Button: TMouseButton;
               Shift: TShiftState; X, Y: Integer);
          procedure Stampaelenco1Click(Sender: TObject);
          procedure EsportainHTML1Click(Sender: TObject);
          procedure EsportainExcel1Click(Sender: TObject);
          procedure ToolbarButton971Click(Sender: TObject);
          procedure ToolbarButton973Click(Sender: TObject);
          procedure Stampagriglia1Click(Sender: TObject);
          procedure TTabQuery_OLDAfterOpen(DataSet: TDataSet);
          procedure cancellasoggetti1Click(Sender: TObject);
          procedure PMStati1Click(Sender: TObject);
          procedure PMStati2Click(Sender: TObject);
          procedure PMStati3Click(Sender: TObject);
          procedure PMStati4Click(Sender: TObject);
          procedure PMStati5Click(Sender: TObject);
          procedure cancellatuttiisoggettiselezionati1Click(Sender: TObject);
          procedure mettiinstatoeliminato1Click(Sender: TObject);
          procedure DBGrid3LivLinguaParlatoButtonClick(Sender: TObject;
               AbsoluteIndex: Integer);
          procedure DBGrid3LivLinguaScrittoButtonClick(Sender: TObject;
               AbsoluteIndex: Integer);
          procedure Salvaquerysufile1Click(Sender: TObject);
          procedure BRiepCandClick(Sender: TObject);
          procedure DBGrid3DescCampoButtonClick(Sender: TObject;
               AbsoluteIndex: Integer);
          procedure DBGrid3DescTabellaButtonClick(Sender: TObject;
               AbsoluteIndex: Integer);
          procedure DBGrid3DescOperatoreButtonClick(Sender: TObject;
               AbsoluteIndex: Integer);
          procedure CBIRFileSystemClick(Sender: TObject);
          procedure DsLineeStateChange(Sender: TObject);
          procedure BLineeOKClick(Sender: TObject);
          procedure BLineeCanClick(Sender: TObject);
          procedure DBGrid3CustomDrawCell(Sender: TObject; ACanvas: TCanvas;
               ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
               ASelected, AFocused, ANewItemRow: Boolean; var AText: string;
               var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
               var ADone: Boolean);
          procedure DBGrid3Editing(Sender: TObject; Node: TdxTreeListNode;
               var Allow: Boolean);
          procedure DBGrid3ValoreEditButtonClick(Sender: TObject);
          procedure BCriterioNewClick(Sender: TObject);
          procedure BCriterioModClick(Sender: TObject);
          procedure BCriterioDelClick(Sender: TObject);
          procedure BCriterioDelTuttiClick(Sender: TObject);
          procedure DBGrid2CustomDrawCell(Sender: TObject; ACanvas: TCanvas;
               ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
               ASelected, AFocused, ANewItemRow: Boolean; var AText: string;
               var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
               var ADone: Boolean);
          procedure DBGrid2ValoreEditButtonClick(Sender: TObject);
          procedure BInfoColloquioClick(Sender: TObject);
          procedure PCTipoRicChange(Sender: TObject);
          procedure Aggiungiinricerca1Click(Sender: TObject);
          procedure BitBtn2Click(Sender: TObject);
          procedure ApriQuery1DrawItem(Sender: TObject; ACanvas: TCanvas;
               ARect: TRect; Selected: Boolean);
          procedure PMExportPopup(Sender: TObject);
          procedure SalvaQuery1Click(Sender: TObject);
          procedure Button1Click(Sender: TObject);
          procedure Aggiungiadunaricerca1Click(Sender: TObject);
          procedure Esegui1Click(Sender: TObject);
          procedure Infocolloquio1Click(Sender: TObject);
          procedure Infocand1Click(Sender: TObject);
          procedure Curriculum1Click(Sender: TObject);
          procedure aquestaricerca1Click(Sender: TObject);
          procedure adaltrericerche1Click(Sender: TObject);
          procedure Panel1ContextPopup(Sender: TObject; MousePos: TPoint;
               var Handled: Boolean);
          procedure dxDBGrid1ContextPopup(Sender: TObject; MousePos: TPoint;
               var Handled: Boolean);
          procedure bEvento_oldClick(Sender: TObject);
          procedure Nuovocriterio1Click(Sender: TObject);
          procedure Modificacriterio1Click(Sender: TObject);
          procedure Eliminacriterio1Click(Sender: TObject);
          procedure Eliminatutti1Click(Sender: TObject);
          procedure BCalcolaDistanza_oldClick(Sender: TObject);
          procedure WebBrowser1DocumentComplete(Sender: TObject;
               const pDisp: IDispatch; var URL: OleVariant);
          procedure Timer1Timer(Sender: TObject);
          procedure AnagraficaResidenza1Click(Sender: TObject);
          procedure AnagraficaDomicilio1Click(Sender: TObject);
          procedure Estraifilecandidati1Click(Sender: TObject);
          procedure EIRNoteKeyPress(Sender: TObject; var Key: Char);
          procedure BGraficaClick(Sender: TObject);
          procedure BesciClick(Sender: TObject);
          procedure AttacDSClick(Sender: TObject);
          procedure CloseRes1Click(Sender: TObject);
          procedure BCalcolaDistanzaClick(Sender: TObject);
          procedure Esportadaticandidati1Click(Sender: TObject);
          procedure SpeedButton1Click(Sender: TObject);
          procedure QRes_testCalcFields(DataSet: TDataSet);
          procedure DsQRicLineeQueryStateChange(Sender: TObject);
          procedure dxDBGrid2DescTabellaButtonClick(Sender: TObject;
               AbsoluteIndex: Integer);
          procedure dxDBGrid2DescCampoButtonClick(Sender: TObject;
               AbsoluteIndex: Integer);
          procedure dxDBGrid2DescOperatoreButtonClick(Sender: TObject;
               AbsoluteIndex: Integer);
          procedure dxDBGrid2ValoreEditButtonClick(Sender: TObject);
          procedure dxDBGrid2LivLinguaParlatoButtonClick(Sender: TObject;
               AbsoluteIndex: Integer);
          procedure dxDBGrid2LivLinguaScrittoButtonClick(Sender: TObject;
               AbsoluteIndex: Integer);
          procedure dxDBGrid2CustomDrawCell(Sender: TObject; ACanvas: TCanvas;
               ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
               ASelected, AFocused, ANewItemRow: Boolean; var AText: string;
               var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
               var ADone: Boolean);
          procedure QRicLineeQueryBeforePost(DataSet: TDataSet);
          procedure dxDBGrid3ValoreEditButtonClick(Sender: TObject);
          procedure dxDBGrid3CustomDrawCell(Sender: TObject; ACanvas: TCanvas;
               ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
               ASelected, AFocused, ANewItemRow: Boolean; var AText: string;
               var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
               var ADone: Boolean);
          procedure MiniProfilo1Click(Sender: TObject);
          procedure MiniProfiloAnonimo1Click(Sender: TObject);
          procedure PMStati6Click(Sender: TObject);
          procedure PMStati7Click(Sender: TObject);
     private
          { Private declarations }
          xStringa, xIndexName, xOrdine, xGroup: string;
          xFormID: integer;
          xggDiffDi, xggDiffUc, xggDiffCol: integer;
          xCheckRicCandCV: boolean;
          procedure Save(ADefaultExt, AFilter, AFileName: string; AMethod: TSaveMethod);
          function LogEsportazioniCand: boolean;
          procedure SalvaDaWizard(x: integer = 0);
          procedure RicreaStringaSQL;
          procedure PredisponiWizard;
          procedure CaricaRicSemplice;
          procedure EseguiQueryRic_OLD;
          procedure EseguiQueryRic2;

     public
          xDaEscludere: array[1..1000] of integer;
          xChiamante, xTipoRic, xTipoQuery, xIDRicerca: integer;
          xIsFulltextInstalled, xAnagAltreInfoOK, xFileSystemOK, xRegoleCollImpostate: boolean;
          xIRFileSystemScope: string;
          xEIrNote: string;
          xidIndirizzoCliente: string;
          IndirizzoCliente, IndirizzoAna: string;
          xidusers: integer;
          //  xidricerca: string;
          procedure MyPopup(Sender: TObject);
          procedure MyPopupDel(Sender: TObject);
     end;

var
     SelPersNewForm: TSelPersNewForm;

implementation

uses ModuloDati, RepElencoSel, View, SelPers, MDRicerche, InsRuolo,
     SelCompetenza, Main, Curriculum, uUtilsVarie, Comuni, SchedaCand,
     FaxPresentaz, LegendaRic, SchedaSintetica, CheckPass, SelDiploma,
     riepilogocand, SelCriterioWizard, InfoColloquio, uASACand, ElencoRicPend,
     ModuloDati2, motoriricerca, CambiaStato2, uGestEventi,
     MidItems, ActiveX, MSHTML, Math, GoogleApi, InputPassword, shellapi,
     GoogleMap, ModelliWord, ElencoDip, CaricaPriimiProgress;

{$R *.DFM}

var TipoIndirizzoGoogleMaps: integer;


     //[TONI20021003]DEBUGOK

procedure TSelPersNewForm.ComboBox1Change(Sender: TObject);
begin
end;

//[TONI20021003]DEBUGOK

procedure TSelPersNewForm.FormShow(Sender: TObject);
var lista: string;
     i: integer;
begin
     //grafica
     if data.Global.fieldbyname('Graficasel').asinteger = 2 then begin
          SelPersNewForm.Color := Mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;
          PanRic.Color := Mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;
          PanRic.font.color := Mainform.AdvToolBarOfficeStyler1.font.Color;
          PanRic.font.name := Mainform.AdvToolBarOfficeStyler1.font.name;
          Panel1.Color := Mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;
          Panel1.font.color := Mainform.AdvToolBarOfficeStyler1.font.Color;
          Panel1.font.name := Mainform.AdvToolBarOfficeStyler1.font.name;
          Panel3.Color := Mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;
          Panel3.font.color := Mainform.AdvToolBarOfficeStyler1.font.Color;
          Panel3.font.name := Mainform.AdvToolBarOfficeStyler1.font.name;
          Panel10.Color := Mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;
          Panel10.font.color := Mainform.AdvToolBarOfficeStyler1.font.Color;
          Panel10.font.name := Mainform.AdvToolBarOfficeStyler1.font.name;
          PanCriteri.Color := Mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;
          PanCriteri.font.color := Mainform.AdvToolBarOfficeStyler1.font.Color;
          PanCriteri.font.name := Mainform.AdvToolBarOfficeStyler1.font.name;
          Panel2.Color := Mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;
          Panel2.font.color := Mainform.AdvToolBarOfficeStyler1.font.Color;
          Panel2.font.name := Mainform.AdvToolBarOfficeStyler1.font.name;
          PanCriteriButtons.Color := Mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;
          PanCriteriButtons.font.color := Mainform.AdvToolBarOfficeStyler1.font.Color;
          PanCriteriButtons.font.name := Mainform.AdvToolBarOfficeStyler1.font.name;
          Panel4.Color := Mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;
          Panel4.font.color := Mainform.AdvToolBarOfficeStyler1.font.Color;
          Panel4.font.name := Mainform.AdvToolBarOfficeStyler1.font.name;
          PanInfoRet.Color := Mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;
          PanInfoRet.font.color := Mainform.AdvToolBarOfficeStyler1.font.Color;
          PanInfoRet.font.name := Mainform.AdvToolBarOfficeStyler1.font.name;
          DBCtrlGrid1.Color := Mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;
          PPulsanti.Color := Mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;
          PPulsanti.font.color := Mainform.AdvToolBarOfficeStyler1.font.Color;
          PPulsanti.font.name := Mainform.AdvToolBarOfficeStyler1.font.name;
     end;


     //possibilit� di estrarre i file dal db e salvarli su disco (per tutte le anagrafica ricercate)
     Estraifilecandidati1.Visible := true; //data.Global.FieldByName('anagfilEdentrodb').asboolean;
     if Data.Global.fieldbyname('debughrms').AsBoolean = TRUE then showmessage('1');

     // carico nel webbrowser il file html contenente il javascript per calcolare la distanza
     if FileExists(ExtractFilePath(Application.ExeName) + 'CalcolaDistanza.html') then begin
          Timer1.enabled := false;
          BCalcolaDistanza.Visible := true;
          BCalcolaDistanza.Enabled := true;
          //dxDBGrid1Distanza.Visible := true;
          //dxDBGrid1Tempo.Visible := true;
          // try
               // WebBrowser1.Navigate(ExtractFilePath(Application.ExeName) + 'CalcolaDistanza.html');
          // except
          //      MessageDlg('Impossibile effettuare la chiamata a google api: ', mtError, [mbOK], 0);
         //  end;
     end else begin
          BCalcolaDistanza.Visible := false;
          //dxDBGrid1Distanza.Visible := true;
          //dxDBGrid1Tempo.Visible := true;
     end;


     /// carico gli stati da considerare nella ricerca
     lista := '';
     if xChiamante = 1 then
          lista := LeggiRegistry('StatiRicercaCommessa')
     else lista := LeggiRegistry('StatiRicerca');
     if Data.Global.fieldbyname('debughrms').AsBoolean = TRUE then showmessage('12');
     if lista <> '' then begin
          for i := 0 to (pmstati.Items.Count - 1) do begin
               if lista[i + 1] = '0' then
                    pmstati.Items.Items[i].Checked := false
               else
                    pmstati.Items.Items[i].Checked := true;
          end;
     end;
     if Data.Global.fieldbyname('debughrms').AsBoolean = TRUE then showmessage('3');
     {xGroup := ' Group By  Anagrafica.ID,Anagrafica.dacontattocliente,Anagrafica.Cognome,Anagrafica.Nome,Anagrafica.CVNumero,Anagrafica.OldCVNumero,' + chr(13) +
          '   Anagrafica.TelUfficio,Anagrafica.Cellulare,CVInseritoInData,CVDataReale,CVDataAgg,RecapitiTelefonici,' + chr(13) +
          '   Anagrafica.DataNascita,Anagrafica.IDStato,Anagrafica.IDTipoStato,' + chr(13) +
          '   Anagrafica.Provincia, Anagrafica.Comune,anagrafica.indirizzo,anagrafica.cap,anagrafica.numcivico,anagrafica.tipostrada,' + chr(13) +
          '   anagrafica.domiciliotipostrada,anagrafica.domiciliocap,anagrafica.domicilionumcivico,anagrafica.domicilioindirizzo,anagrafica.domiciliocomune,anagrafica.domicilioprovincia,' + chr(13) +
          '   anagrafica.domicilioRegione,anagrafica.Regione,anagrafica.domicilioStato,anagrafica.Stato,' + chr(13) +
          '   NoCv,AnagAltreInfo.Retribuzione,EMail, AnagAltreInfo.Inquadramento, googledistanzaric.distanza,googledistanzaric.tempo,emailufficio,' + chr(13) +
          '   EspLavAttuale.Azienda, EspLavAttuale.Ruolo, EspLavAttuale.descrizionemansione,' + chr(13) +
          '   EspLavAttuale.JobTitle, EspLavAttuale.Area, EspLavAttuale.Settore, EBC_TipiStato.TipoStato, PrimoRuolo.Mansione,' + chr(13) +
          //'   LogAnag_I.DataIns,  LogAnag_U.LastAgg, PrimoRuolo.Mansione,' + chr(13) +
     '   EBC_Colloqui.Data, EBC_CandidatiRicerche.DataImpegno, EBC_CandidatiRicerche.DataIns, EBC_ContattiCandidati.Data, Anagrafica.CVInseritoInData,' + chr(13) +
          '   EspLavAttuale.titolomansione, anagfileD.IDAnagfile, anagfileD.IDAnagfile,EBC_stati.Stato ' + chr(13);
     xOrdine := 'order by Anagrafica.Cognome,Anagrafica.Nome';}
     EIRNote.Text := '';
     if xChiamante = 1 then begin
          DBGrid3.Visible := False;
          dxDBGrid2.Visible := True;
          DBGrid2.Visible := False;
          dxDBGrid3.Visible := True;
          QRicLineeQuery.Close;
          QRicLineeQuery.ParamByName['xIDRic'] := DataRicerche.TRicerchePend.FieldByName('ID').asString;
          if Data.Global.fieldbyname('debughrms').AsBoolean = TRUE then showmessage('4');
          QRicLineeQuery.Open;
          while not QRicLineeQuery.Eof do begin
               if QRicLineeQuery.FieldByName('NoteFullText').asString <> '' then
                    EIRNote.Text := QRicLineeQuery.FieldByName('NoteFullText').asString;
               QRicLineeQuery.Next;
          end;
          QRicLineeQuery.first;
     end else begin
          DBGrid3.Visible := True;
          dxDBGrid2.Visible := False;
          DBGrid2.Visible := True;
          dxDBGrid3.Visible := False;
          TLinee.Close;
          TLinee.ParamByName['xIDAnag'] := MainForm.xIDUtenteAttuale;
          if Data.Global.fieldbyname('debughrms').AsBoolean = TRUE then showmessage('4');
          TLinee.Open;
          while not TLinee.Eof do begin
               if TLinee.FieldByName('NoteFullText').asString <> '' then
                    EIRNote.Text := TLinee.FieldByName('NoteFullText').asString;
               TLinee.Next;
          end;
          TLinee.first;
     end;
     // se viene da Ricerca -> caricamento criteri salvati
    { if xChiamante = 1 then begin
         // BCalcolaDistanza.Visible := true;
         // dxDBGrid1Distanza.Visible := true;
        //  dxDBGrid1Tempo.Visible := true;
          Query1.SQL.Clear;
          Query1.SQL.Add('delete from UserLineeQuery where IDAnagrafica=' + IntToStr(MainForm.xIDUtenteAttuale));
          Query1.ExecSQL;
          if Data.Global.fieldbyname('debughrms').AsBoolean = TRUE then showmessage('5');
          TLinee.Close;
          TLinee.Open;
          if Data.Global.fieldbyname('debughrms').AsBoolean = TRUE then showmessage('6');
          QRicLineeQuery.Close;
          QRicLIneeQuery.ReloadSQL;
          QRicLineeQuery.ParamByName['xIDRic'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
          if Data.Global.fieldbyname('debughrms').AsBoolean = TRUE then showmessage('7');
          if Data.Global.fieldbyname('debughrms').AsBoolean = TRUE then showmessage('IDRicPend' + DataRicerche.TRicerchePend.FieldByName('ID').Asstring);
          QRicLineeQuery.Open;
          while not QRicLineeQuery.EOF do begin
               TLinee.InsertRecord([QRicLineeQuery.FieldByName('Descrizione').AsString,
                    QRicLineeQuery.FieldByName('Stringa').AsString,
                         QRicLineeQuery.FieldByName('Tabella').AsString,
                         QRicLineeQuery.FieldByName('OpSucc').AsString,
                         QRicLineeQuery.FieldByName('Next').AsString,
                         QRicLineeQuery.FieldByName('LivLinguaParlato').Value,
                         QRicLineeQuery.FieldByName('LivLinguaScritto').Value,
                         QRicLineeQuery.FieldByName('DescTabella').AsString,
                         QRicLineeQuery.FieldByName('DescOperatore').AsString,
                         QRicLineeQuery.FieldByName('Valore').AsString,
                         QRicLineeQuery.FieldByName('DescCampo').AsString,
                         //QRicLineeQuery.FieldByName('DescCampo').AsString,
                         //QRicLineeQuery.FieldByName('DescOperatore').AsString,
                         //QRicLineeQuery.FieldByName('Valore').AsString,
                    QRicLineeQuery.FieldByName('QueryLookup').AsString,
                         MainForm.xIDUtenteAttuale]);
               //TLinee.Edit;
               //TLineeDescrizione.Value := QRicLineeQuery.FieldByName('Descrizione').AsString;
               //TLineeStringa.Value := QRicLineeQRicLineeQuery.FieldByName('Stringa').AsString;
               //TLineeTabella

               QRicLineeQuery.Next;
          end;
          TLinee.Close;
          TLinee.Open;
     end else begin
        //  BCalcolaDistanza.Visible := false;
        //  dxDBGrid1Distanza.Visible := false;
       //   dxDBGrid1Tempo.Visible := false;
     end;   }
     // parametri sulle date da GLOBAL
     Q.Close;
     Q.SQl.text := 'select ggDiffDi,ggDiffUc,ggDiffCol,CheckRicCandCV from global ';
     Q.Open;
     xggDiffDi := Q.FieldByname('ggDiffDi').asInteger;
     xggDiffUc := Q.FieldByname('ggDiffUc').asInteger;
     xggDiffCol := Q.FieldByname('ggDiffCol').asInteger;
     xCheckRicCandCV := Q.FieldByname('CheckRicCandCV').asBoolean;
     Q.Close;
     // personalizzazione Ergon:
     if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 5)) = 'ERGON' then begin
          BInfoCand.caption := 'scheda cand.';
          BInfoCand.Font.Style := [];
          BCurriculum.Visible := False;
     end;
     if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 6)) = 'BOYDEN' then
          dxDBGrid1DataRealeCV.DisableCustomizing := True;

     if (Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 7)) = 'FERRARI') or (Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 8)) = 'MASERATI') then begin
          CBPropri.Checked := True;
          PMStati5.Checked := True;
     end;

     if (Uppercase(copy(Data.Global.FieldByName('NomeAzienda').Value, 1, 6)) = 'ADVANT') or (pos('MCS', UpperCase(Data.Global.FieldByName('NomeAzienda').Value)) > 0) then begin
          BCurriculum.enabled := False;
          BDocumenti.enabled := False;
          CBIRaltreRegole.Checked := true;
          CBIRnote.Checked := false;
          CBIRnote.Enabled := false;
          dxDBGrid1Column32.Visible := true;
          // CBIRFileSystem.Checked := false;
     end;
     //BCurriculum.enabled := False;
     //BitBtn8.enabled := False;
     CBIRaltreRegole.Checked := true;
     CBIRnote.Checked := false;
     CBIRnote.Enabled := false;
     // CBIRFileSystem.Checked := false;


     Caption := '[M/5] - ' + Caption;

     // Bottone Maschera riepilogativa
     if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 6)) = 'BOYDEN' then
          BRiepCand.visible := true
     else
          BRiepCand.visible := false;

     // INFORMATION RETRIEVAL
     // -- 1) verifica corretta installazione componente full-text
     //showmessage(vartostr(MainForm.xIsFulltextInstalled));
     xIsFulltextInstalled := MainForm.xIsFulltextInstalled;
     if (not xIsFulltextInstalled) and (Data.Global.FieldByName('AnagFileDentroDB').AsBoolean = not TRUE) then begin
          PanInfoRet.Visible := False;
          //showmessage ('panInforet false');
     end else begin
          // -- 3) verifica esistenza indice full-text sulla tabella AnagAltreInfo
          Q.Close;
          Q.SQL.text := 'select OBJECTPROPERTY ( object_id(''AnagAltreInfo''),''TableHasActiveFulltextIndex'') AnagAltreInfoOK';
          Q.Open;
          xAnagAltreInfoOK := Q.FieldByName('AnagAltreInfoOK').asInteger = 1;
          // -- VERIFICA SERVIZIO DI INDICIZZAZIONE a livello di File-Systemo o esistenza indice full-text su AnagFile
          if Data.Global.FieldByName('AnagFileDentroDB').AsBoolean = TRUE then begin
               Q.Close;
               Q.SQL.text := 'select OBJECTPROPERTY ( object_id(''AnagFile''),''TableHasActiveFulltextIndex'') FileSystemOK';
               Q.Open;
               xFileSystemOK := Q.FieldByName('FileSystemOK').asInteger = 1;
          end else begin
               Q.Close;
               Q.SQL.text := 'select count(*) FileSystemOK from master.dbo.sysservers where srvname=''FileSystem''';
               Q.Open;
               xFileSystemOK := Q.FieldByName('FileSystemOK').asInteger = 1;
          end;
          // abilitazione checkbox
          CBIRnote.Enabled := xAnagAltreInfoOK;
          CBIRnote.Checked := xAnagAltreInfoOK;
          CBIRFileSystem.Enabled := xFileSystemOK;
          CBIRFileSystem.Checked := xFileSystemOK;
          //showmessage ('panInforet true');
     end;

     // riempimento colonne griglia
     //RiempiColDescTabella;
     SelCriterioWizardForm := TSelCriterioWizardForm.create(self);

     // tipologia di ricerca a default (impostato per utente in Users.TipoRicDefault)
     Q.Close;
     Q.SQL.text := 'select TipoRicDefault from Users where ID=' + IntToStr(MainForm.xIDUtenteAttuale);
     Q.Open;
     xTipoRic := Q.FieldByName('TipoRicDefault').asInteger;
     if xTipoRic = 1 then begin
          //
          PCTipoRic.ActivePageIndex := 0;
          CaricaRicSemplice;
     end else begin
          PCTipoRic.ActivePageIndex := 1;
     end;
     Q.Close;

     // RICERCA SEMPLICE
     // riempire i campi che non ci sono gi�
     {Q2.Close;
     Q2.SQL.text := 'select DescTabella,Tabella,Campo,DescCampo,DescOperatore,DescCompleta,TabQueryOperatori.ID IDTabQueryOp ' +
          'from TabQuery,TabQueryOperatori where TabQuery.ID=TabQueryOperatori.IDTabQuery ' +
          'and DescCompleta is not null and TabQueryOperatori.ID not in  ' +
          ' (select distinct IDTabQueryOp from UserLineeQuery where IDTabQueryOp is not null  ' +
          '  and IDAnagrafica=' + IntToStr(MainForm.xIDUtenteAttuale) + ')';
     Q2.Open;
     while not Q2.EOF do begin
          with SelCriterioWizardForm do begin
               TLinee.Insert;
               TLineeIDAnagrafica.Value := MainForm.xIDUtenteAttuale;
               TLineeDescTabella.Value := Q2.FieldByName('DescTabella').asString;
               TLineeTabella.Value := Q2.FieldByName('Tabella').asString;
               TLineeDescCampo.Value := Q2.FieldByName('DescCampo').asString;
               TLineeDescOperatore.Value := Q2.FieldByName('DescOperatore').asString;
               TLineeDescCompleta.Value := Q2.FieldByName('DescCompleta').asString;
               TLineeIDTabQueryOp.Value := Q2.FieldByName('IDTabQueryOp').asInteger;
               TLineeOpSucc.Value := 'and';
               TLineeOpNext.Value := 'e';
               TLinee.Post;
          end;
          Q2.Next;
     end;
     Q2.Close;}

     // ### Non far vedere le pagine
     //for i:=0 to PCTipoRic.PageCount-1 do
     //PCTipoRic.Pages[0].TabVisible := False;

     // personalizzazioni SCR
     if Pos('S.C.R.', UpperCase(Data.Global.FieldByName('NomeAzienda').asString)) > 0 then begin
          MiniProfilo1.Visible := True;
          MiniProfiloanonimo1.Visible := True;
          DBGrid3OpNext.Items.Clear;
          DBGrid3OpNext.Items.Add('e');
          DBGrid3OpNext.Items.Add('e*');
          DBGrid3OpNext.Items.Add('o');
          DBGrid3OpNext.Items.Add('o*');
          DBGrid3OpNext.Items.Add('fine');
     end else begin
          MiniProfilo1.Visible := False;
          MiniProfiloanonimo1.Visible := False;
     end;
end;

procedure TSelPersNewForm.DBGrid4KeyPress(Sender: TObject; var Key: Char);
begin
     if TTabLookup.IndexName <> '' then begin
          if Key <> chr(13) then begin
               xStringa := xStringa + key;
               //               TTabLookup.FindNearest([xStringa]);
               TTabLookup.FindNearestADO(VarArrayOf([xStringa]));
          end;
     end;
end;

procedure TSelPersNewForm.BDocumentiClick(Sender: TObject);
var xFile: string;
     xProcedi: boolean;
begin
     if QRes1.EOF then exit;
     // prosegui timing
     if xChiamante = 1 then SelPersForm.RiprendiAttivita;
     if pos('INTERMEDIA', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) = 0 then begin
          if not OkIDAnag(QRes1.FieldByName('ID').AsInteger) then exit;
     end;
     ApriCV(QRes1.FieldByName('ID').AsInteger);
     // prosegui timing
     if xChiamante = 1 then SelPersForm.RiprendiAttivita;

     RegistraVisioneCand(MainForm.xIDUtenteAttuale, QRes1ID.Value);
end;
//[TONI20021003]DEBUGOK

{procedure TSelPersNewForm.BAggiungiRicClick(Sender: TObject);
var i: integer;
  xMiniVal, xRic: string;
  xVai: boolean;
  SavePlace: TBookmark;
  xIDEvento, xIDClienteBlocco, xIDLocate: integer;
  xLivProtez, xDicMess, xPassword, xUtenteResp, xClienteBlocco: string;
  QLocal: TADOLinkedQuery;
begin

  if not Mainform.CheckProfile('210') then Exit;
  if not OkIDAnag(QRes1.FieldByName('ID').AsInteger) then exit;
  if QRes1.FieldByName('IDTipoStato').AsInteger = 3 then
    if MessageDlg('Il candidato risulta INSERITO in azienda. Continuare?', mtWarning, [mbYes, mbNo], 0) = mrNo then exit;
  xMiniVal := '';
  if not InputQuery('Inserimento nella ricerca', 'Valutazione (1 lettera):', xMiniVal) then exit;
  Q.Close;
  Q.SQL.Text := 'select ID from EBC_CandidatiRicerche where IDRicerca=' + Dataricerche.TRicerchePend.FieldByName('ID').asString + ' and IDAnagrafica=' + QRes1.FieldByname('ID').asString;
  Q.Open;
  if Q.IsEmpty then begin
    xVai := True;
    // controllo se � in selezione
    if QRes1.FieldByName('IDTipoStato').AsInteger = 1 then begin
      if MessageDlg('Il soggetto risulta IN SELEZIONE: ' + chr(13) + xRic + chr(13) + 'PROCEDERE COMUNQUE ?', mtWarning, [mbYes, mbNo], 0) = mrNo then xVai := False;
    end;
    // controllo se � gi� associato ad un'altra ricerca per la STESSA azienda
    Q.Close; Q.SQL.clear;
    Q.SQl.Add('select EBC_Ricerche.Progressivo,EBC_StatiRic.StatoRic from EBC_CandidatiRicerche,EBC_Ricerche,EBC_StatiRic ');
    Q.SQl.Add('where EBC_CandidatiRicerche.IDRicerca=EBC_Ricerche.ID and EBC_Ricerche.IDStatoRic=EBC_StatiRic.ID');
    Q.SQl.Add('and EBC_CandidatiRicerche.IDAnagrafica=:xIDAnag: and EBC_Ricerche.IDCliente=:xIDCliente:');
    //          Q.Prepare;
    Q.ParamByName['xIDAnag'] := QRes1.FieldByName('ID').AsInteger;
    Q.ParamByName['xIDCliente'] := DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger;
    Q.Open;
    if not Q.IsEmpty then begin
      xRic := '';
      while not Q.EOF do begin xRic := xRic + 'N�' + Q.FieldByName('Progressivo').asString + ' (' + Q.FieldByName('StatoRic').asString + ') '; Q.Next; end;
      if MessageDlg('Il soggetto � associato gi� alle seguenti ricerche per lo stesso cliente: ' + chr(13) + xRic + chr(13) + 'PROCEDERE COMUNQUE ?', mtWarning, [mbYes, mbNo], 0) = mrNo then xVai := False;
    end;
    // controllo incompatibilit� (per questa azienda e per le aziende a questa correlate)
    if IncompAnagCli(QRes1.FieldByName('ID').AsInteger, DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger) then
      if MessageDlg('Il soggetto risulta INCOMPATIBILE con questo cliente o con un cliente associato' + chr(13) +
        'PROCEDERE COMUNQUE ?', mtWarning, [mbYes, mbNo], 0) = mrNo then xVai := False;
    // controllo appartenenza CV
    Q.Close;
    Q.SQL.Text := 'select IDProprietaCV from Anagrafica where ID=' + QRes1.FieldByName('ID').asString;
    Q.Open;
    if (not ((Q.FieldByName('IDProprietaCV').asString = '') or (Q.FieldByName('IDProprietaCV').asInteger = 0))) and
      (Q.FieldByName('IDProprietaCV').asInteger <> DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger) then
      if MessageDlg('ATTENZIONE: il CV risulta di propriet� di un altro cliente.' + chr(13) +
        'Vuoi proseguire lo stesso ?', mtWarning, [mbYes, mbNo], 0) = mrNo then
        xVai := False;
    // controllo blocco livello 1 o 2
    xIDClienteBlocco := CheckAnagInseritoBlocco1(QRes1.FieldByName('ID').AsInteger);
    if xIDClienteBlocco > 0 then begin
      xLivProtez := copy(IntToStr(xIDClienteBlocco), 1, 1);
      if xLivProtez = '1' then begin
        xIDClienteBlocco := xIDClienteBlocco - 10000;
        xClienteBlocco := GetDescCliente(xIDClienteBlocco);
        xDicMess := 'Stai inserendo in commessa un candidato appartenente ad una azienda (' + xClienteBlocco + ') con blocco di livello 1, ';
      end else begin
        xIDClienteBlocco := xIDClienteBlocco - 20000;
        xClienteBlocco := GetDescCliente(xIDClienteBlocco);
        xDicMess := 'Stai inserendo in commessa un candidato appartenente ad una azienda ATTIVA (' + xClienteBlocco + '), ';
      end;
      if MessageDlg('ATTENZIONE!! PROTEZIONE CLIENTI LIVELLO ' + xLivProtez + chr(13) + xDicMess +
        'protetta dalle normative interne, se sei autorizzato premi SI, altrimenti premi NO. ' +
        'Se si prosegue, l''operazione verr� registrata nello storico del soggetto come forzatura. ' +
        'Verr� inviato un messaggio di promemoria al responsabile diretto', mtWarning, [mbYes, mbNo], 0) = mrNo then xVai := False
      else begin
        // pwd
        xUtenteResp := GetDescUtenteResp(MainForm.xIDUtenteAttuale);
        if not InputQuery('Forzatura blocco livello ' + xLivProtez, 'Password di ' + xUtenteResp, xPassword) then exit;
        if not CheckPassword(xPassword, GetPwdUtenteResp(MainForm.xIDUtenteAttuale)) then begin
          MessageDlg('Password errata', mtError, [mbOK], 0);
          xVai := False;
        end;
        if xVai then begin
          // promemoria al resp.
          QLocal := CreateQueryFromString('insert into Promemoria (IDUtente,IDUtenteDa,DataIns,Testo,Evaso,DataDaLeggere) ' +
            'values (:xIDUtente:,:xIDUtenteDa:,:xDataIns:,:xTesto:,:xEvaso:,:xDataDaLeggere:)');
          QLocal.ParamByName['xIDUtente'] := GetIDUtenteResp(MainForm.xIDUtenteAttuale);
          QLocal.ParamByName['xIDUtenteDa'] := MainForm.xIDUtenteAttuale;
          QLocal.ParamByName['xDataIns'] := Date;
          QLocal.ParamByName['xTesto'] := 'forzatura livello ' + xLivProtez + ' per CV n� ' + QRes1.FieldByName('CVNumero').AsString;
          QLocal.ParamByName['xEvaso'] := 0;
          QLocal.ParamByName['xDataDaLeggere'] := Date;
          QLocal.ExecSQL;
          // registra nello storico soggetto
          xIDEvento := GetIDEvento('forzatura Blocco livello ' + xLivProtez);
          if xIDEvento > 0 then begin
            with Data.Q1 do begin
              close;
              SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
                'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xIDCliente:)';
              ParamByName['xIDAnagrafica'] := QRes1.FieldByName('ID').AsInteger;
              ParamByName['xIDEvento'] := xIDevento;
              ParamByName['xDataEvento'] := Date;
              ParamByName['xAnnotazioni'] := 'cliente: ' + xClienteBlocco;
              ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
              ParamByName['xIDUtente'] := MainForm.xIDUtenteAttuale;
              ParamByName['xIDCliente'] := xIDClienteBlocco;
              ExecSQL;
            end;
          end;
        end;
      end;
    end;

    if xVai then begin
      Data.DB.BeginTrans;
      try
        if Q.Active then Q.Close;
        Q.SQL.text := 'insert into EBC_CandidatiRicerche (IDRicerca,IDAnagrafica,Escluso,Stato,DataIns,Minival) ' +
          'values (:xIDRicerca:,:xIDAnagrafica:,:xEscluso:,:xStato:,:xDataIns:,:xMinival:)';
        Q.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
        Q.ParamByName['xIDAnagrafica'] := QRes1.FieldByName('ID').AsInteger;
        Q.ParamByName['xEscluso'] := 0;
        Q.ParamByName['xStato'] := 'inserito da ricerca nomi';
        Q.ParamByName['xDataIns'] := Date;
        Q.ParamByName['xMiniVal'] := xMiniVal;
        Q.ExecSQL;
        // aggiornamento stato anagrafica
        if QRes1.FieldByName('IDStato').AsInteger <> 27 then begin
          if Q.Active then Q.Close;
          Q.SQL.text := 'update Anagrafica set IDStato=27,IDTipoStato=1 where ID=' + QRes1.FieldByName('ID').asString;
          Q.ExecSQL;
        end;
        // aggiornamento storico
        if Q.Active then Q.Close;
        Q.SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
          'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xIDCliente:)';
        Q.ParamByName['xIDAnagrafica'] := QRes1.FieldByName('ID').AsInteger;
        Q.ParamByName['xIDEvento'] := 19;
        Q.ParamByName['xDataEvento'] := date;
        Q.ParamByName['xAnnotazioni'] := 'ric.n� ' + TrimRight(DataRicerche.TRicerchePend.FieldByName('Progressivo').AsString) + ' (' + TrimRight(DataRicerche.TRicerchePend.FieldByName('Cliente').AsString) + ')';
        Q.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
        Q.ParamByName['xIDUtente'] := DataRicerche.TRicerchePend.FieldByName('IDUtente').AsInteger;
        Q.ParamByName['xIDCliente'] := DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger;
        Q.ExecSQL;
        Data.DB.CommitTrans;
        //DataRicerche.QCandRic.Next;
        //SavePlace:=DataRicerche.QCandRic.GetBookmark;
        DataRicerche.QCandRic.Close;
        DataRicerche.QCandRic.Open;
        //DataRicerche.QCandRic.GotoBookmark(SavePlace);
        //DataRicerche.QCandRic.FreeBookmark(SavePlace);
  // metti anche questo in quelli da escludere per le prossime ricerche
        for i := 1 to 500 do if xDaEscludere[i] = 0 then break;
        xDaEscludere[i] := QRes1.FieldByName('ID').AsInteger;
        //recuper ID per locate su record sucessivo
        QRes1.Next;
        xIDLocate := Qres1.FieldByname('ID').AsInteger;

        BitBtn2Click(self);

        Qres1.Locate('ID', xIDLocate, []);
      except
        Data.DB.RollbackTrans;
        MessageDlg('Errore sul database:  inserimento non avvenuto', mtError, [mbOK], 0);
      end;
    end;
  end;
  // prosegui timing
  if xChiamante = 1 then SelPersForm.RiprendiAttivita;
end;                                                            }

procedure TSelPersNewForm.TLinee_OLDBeforePost(DataSet: TDataSet);
begin
     if (TLinee.Eof = false) and (TLinee.FieldByName('opNext').AsString = 'fine') then TLinee.FieldByName('opNext').AsString := 'e';


     if TLinee.FieldByName('OpNext').AsString = 'e' then TLinee.FieldByName('OpSucc').AsString := 'and';
     if TLinee.FieldByName('OpNext').AsString = 'o' then TLinee.FieldByName('OpSucc').AsString := 'or';
     if Trim(TLinee.FieldByName('OpNext').AsString) = 'o*' then TLinee.FieldByName('OpSucc').AsString := 'or*';
     if Trim(TLinee.FieldByName('OpNext').AsString) = 'e*' then TLinee.FieldByName('OpSucc').AsString := 'and*';
     if TLinee.FieldByName('OpNext').AsString = 'fine' then TLinee.FieldByName('OpSucc').AsString := '';
     RicreaStringaSQL;
end;

procedure TSelPersNewForm.FormClose(Sender: TObject;
     var Action: TCloseAction);
begin
     {if xChiamante = 1 then begin
          // cancellazione vecchie linee query
          Query1.Close;
          Query1.SQL.Clear;
          Query1.SQL.Add('delete from RicLineeQuery where IDRicerca=' + DataRicerche.TRicerchePend.FieldByName('ID').asString);
          Query1.ExecSQL;
          TLinee.First;
          // salvataggio linee query
          TRicLineeQuery.Open;
          while not TLinee.EOF do begin
               Data.DB.BeginTrans;
               try
                    Q.Close;
                    Q.SQL.text := 'Insert Into RicLineeQuery (IDRicerca,Descrizione,Stringa,Tabella,OpSucc,Next,LivLinguaParlato,LivLinguaScritto,DescTabella,DescCampo,DescOperatore,Valore,QueryLookup) ' +
                         'values (:xIDRicerca:,:xDescrizione:,:xStringa:,:xTabella:,:xOpSucc:,:xNext:,:xLivLinguaParlato:,:xLivLinguaScritto:,:xDescTabella:,:xDescCampo:,:xDescOperatore:,:xValore:,:xQueryLookup:)';
                    Q.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
                    Q.ParamByName['xDescrizione'] := TLinee.FieldByName('Descrizione').AsString;
                    Q.ParamByName['xStringa'] := TLinee.FieldByName('Stringa').AsString;
                    Q.ParamByName['xTabella'] := TLinee.FieldByName('Tabella').AsString;
                    Q.ParamByName['xOpSucc'] := TLinee.FieldByName('OpSucc').AsString;
                    Q.ParamByName['xNext'] := TLinee.FieldByName('OpNext').AsString;
                    if TLineeLivLinguaParlato.Value = 0 then
                         Q.ParamByName['xLivLinguaParlato'] := NULL
                    else
                         Q.ParamByName['xLivLinguaParlato'] := TLineeLivLinguaParlato.Value;
                    if TLineeLivLinguaScritto.Value = 0 then
                         Q.ParamByName['xLivLinguaScritto'] := NULL
                    else
                         Q.ParamByName['xLivLinguaScritto'] := TLineeLivLinguaScritto.Value;
                    Q.ParamByName['xDescTabella'] := TLineeDescTabella.Value;
                    Q.ParamByName['xDescCampo'] := TLineeDescCampo.Value;
                    Q.ParamByName['xDescOperatore'] := TLineeDescOperatore.Value;
                    Q.ParamByName['xValore'] := TLineeValore.Value;
                    Q.ParamByName['xQueryLookup'] := TLineeQueryLookup.Value;
                    Q.ExecSQL;
                    Data.DB.CommitTrans;
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE:  operazione non completata', mtError, [mbOK], 0);
                    raise;
               end;
               TLinee.Next;
          end;
          TRicLineeQuery.Close;
          // registrazione evento di prima visione archivio, se non c'�
          if not TLinee.IsEmpty then begin
               Data.QTemp.Close;
               Data.QTemp.SQL.text := 'select count(*) Tot from EBC_StoricoRic where IDEventoRic=1 and IDRicerca=' + DataRicerche.TRicerchePend.FieldByName('ID').AsString;
               Data.QTemp.Open;
               if Data.QTemp.FieldByname('Tot').asInteger = 0 then begin
                    Q.Close;
                    Q.SQL.text := 'insert into EBC_StoricoRic (IDRicerca,DallaData,IDEventoRic) ' +
                         ' values (:xIDRicerca:,:xDallaData:,:xIDEventoRic:)';
                    Q.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
                    Q.ParamByName['xDallaData'] := Date;
                    Q.ParamByName['xIDEventoRic'] := 1;
                    Q.execSQL;
               end;
               Data.QTemp.Close;
          end;
     end; }
     SelCriterioWizardForm.Free;
end;

procedure TSelPersNewForm.BCurriculumClick(Sender: TObject);
var x: string;
     i: integer;
begin
     if QRes1.EOF then exit;
     // prosegui timing
     if xChiamante = 1 then SelPersForm.RiprendiAttivita;
     if not OkIDAnag(QRes1.FieldByName('ID').AsInteger) then exit;
     if not QRes1.Eof then begin
          PosizionaAnag(QRes1.FieldByName('ID').AsInteger);

          CurriculumForm.ShowModal;

          MainForm.Pagecontrol5.ActivePage := MainForm.TSStatoTutti;
     end;
     // prosegui timing
     if xChiamante = 1 then SelPersForm.RiprendiAttivita;

     RegistraVisioneCand(MainForm.xIDUtenteAttuale, QRes1ID.Value);
end;

procedure TSelPersNewForm.BAppunClick(Sender: TObject);
begin
     if QRes1.EOF then exit;
     // prosegui timing
     if xChiamante = 1 then SelPersForm.RiprendiAttivita;
     if not OkIDAnag(QRes1.FieldByName('ID').AsInteger) then exit;
     ApriFileWord(QRes1.FieldByName('CVNumero').asString);
     // prosegui timing
     if xChiamante = 1 then SelPersForm.RiprendiAttivita;

     RegistraVisioneCand(MainForm.xIDUtenteAttuale, QRes1ID.Value);
end;

//[TONI20021003]DEBUGOK

procedure TSelPersNewForm.BitBtn10Click(Sender: TObject);
var i, xID: integer;
     xSQL: string;
begin
     if QRes1.EOF then exit;
     // prosegui timing
     if xChiamante = 1 then SelPersForm.RiprendiAttivita;
     if not OkIDAnag(QRes1.FieldByName('ID').AsInteger) then exit;
     if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 5)) = 'ERGON' then begin
          SchedaSinteticaForm := TSchedaSinteticaForm.create(self);
          SchedaSinteticaForm.xIDAnag := QRes1.FieldByName('ID').AsInteger;
          SchedaSinteticaForm.ShowModal;
          SchedaSinteticaForm.Free;
     end else begin
          if (Uppercase(copy(Data.Global.FieldByName('NomeAzienda').Value, 1, 6)) = 'ADVANT') or (pos('MCS', UpperCase(Data.Global.FieldByName('NomeAzienda').Value)) > 0) then begin
               // QRes1 dalla 10a riga in poi
              { xSQL := 'select distinct Anagrafica.ID';
               for i := 9 to QRes1.SQL.Count - 2 do begin
                 xSQL := xSQL + chr(13) + QRes1.SQL.Strings[i];
               end;    }
               // predisponi Data.TAnagrafica
               Data.Tanagrafica.Close;
               Data.TAnagrafica.SQL.text := 'SELECT Anagrafica.ID, Cognome, Nome, Titolo,' + chr(13) +
                    'DataNascita, GiornoNascita, MeseNascita, LuogoNascita,' + chr(13) +
                    'Sesso, Indirizzo, Cap, Comune, IDComuneRes, IDZonaRes,' + chr(13) +
                    'Provincia, DomicilioIndirizzo, DomicilioCap, DomicilioComune,' + chr(13) +
                    'IDComuneDom, IDZonaDom, DomicilioProvincia, DomicilioStato,' + chr(13) +
                    'RecapitiTelefonici, Cellulare, Fax, CodiceFiscale, PartitaIVA,' + chr(13) +
                    'Email, IDStatoCivile, Anagrafica.IDStato, Anagrafica.IDTipoStato,' + chr(13) +
                    'CVNumero, CVIDAnnData, CVinseritoInData, Foto, IDTipologia, ' + chr(13) +
                    'IDUtenteMod, DubbiIns, IDProprietaCV, TelUfficio, OldCVNumero, ' + chr(13) +
                    'EBC_Stati.Stato DescStato, TipoStrada, DomicilioTipoStrada, NumCivico, DomicilioNumCivico, ' + chr(13) +
                    'IDUtente, Anagrafica.Stato, LibPrivacy, CVDataReale, CVDataAgg, Interessanteinternet, VIPList, ' + chr(13) +
                    'telaziendacentralino, telaziendasegretaria,notecellulare,cellulare2,notecellulare2,emailufficio,regione,domicilioregione ' + chr(13) +
                    'FROM Anagrafica ' + chr(13) +
                    'JOIN EBC_Stati on Anagrafica.IDStato=EBC_Stati.ID ' + chr(13) +
                    'WHERE anagrafica.id = ' + Qres1Id.asstring;
               //'and Anagrafica.ID in (' + chr(13) + xSQL + ')';
               //Data.TAnagrafica.SQL.SaveToFile('AnagQRes1.txt');
               Data.TAnagrafica.Open;
               xID := QRes1ID.Value;
               Data.TAnagrafica.Locate('ID', xID, []);
               Data.TAnagrafica.FieldByName('ID').FocusControl;
               ASASchedaCandForm := TASASchedaCandForm.create(self);
               ASASchedaCandForm.ShowModal;
               ASASchedaCandForm.Free;
               QRes1.Close;
               QRes1.Open;
               if Data.TAnagrafica.active then xID := Data.TAnagraficaID.value;
               QRes1.Locate('ID', xID, []);
               QRes1.FieldByName('ID').FocusControl;
          end else begin

               if QRes1.RecordCount > 0 then begin
                    SchedaCandForm := TSchedaCandForm.create(self);
                    if not PosizionaAnag(QRes1.FieldByName('ID').AsInteger) then exit;
                    SchedaCandForm.xChiamante := 'SelPersNew';
                    SchedaCandForm.ShowModal;
                    SchedaCandForm.Free;
               end;
          end;
     end;
     // prosegui timing
     if xChiamante = 1 then SelPersForm.RiprendiAttivita;

     RegistraVisioneCand(MainForm.xIDUtenteAttuale, QRes1ID.Value);
end;

procedure TSelPersNewForm.QRes1_OLDCalcFields(DataSet: TDataSet);
var xAnno, xMese, xgiorno: Word;
begin
     if QRes1.FieldByName('DataNascita').asString = '' then
          QRes1.FieldByName('Eta').asString := ''
     else begin
          DecodeDate((Date - QRes1.FieldByName('DataNascita').AsDateTime), xAnno, xMese, xgiorno);
          // TAnagraficaEta.Value:=StrToInt(copy(xAnno,3,2));
          QRes1.FieldByName('Eta').Value := copy(IntToStr(xAnno), 3, 2);
     end;
     QRes1.FieldByName('TelUffCell').AsString := QRes1.FieldByName('TelUfficio').AsString + ' - ' + QRes1.FieldByName('Cellulare').AsString;
end;

procedure TSelPersNewForm.CBColloquiatiClick(Sender: TObject);
var i: integer;
begin
     xOrdine := 'order by Anagrafica.CVNumero';
     if QRes1.Active then begin
          xTipoQuery := 0;
          EseguiQueryRic2;
     end;
end;

procedure TSelPersNewForm.QRes1_OLDBeforeOpen(DataSet: TDataSet);
begin
     //QEspLavLK.Open;
     //QLastEspLav.Open;
end;

procedure TSelPersNewForm.QRes1_OLDAfterClose(DataSet: TDataSet);
begin
     //QEspLavLK.Close;
     //QLastEspLav.Close;
end;

procedure TSelPersNewForm.dxDBGrid1Click(Sender: TObject);
begin
     //if xChiamante=1 then SelPersForm.RiprendiAttivita;
end;

procedure TSelPersNewForm.dxDBGrid1CustomDrawCell(Sender: TObject;
     ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
     AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
     var AText: string; var AColor: TColor; AFont: TFont;
     var AAlignment: TAlignment; var ADone: Boolean);
var Value: variant;
     xFile: string;
begin
     if Data.Global.fieldbyname('debughrms').VAlue <> TRue then begin
          if ANode.HasChildren then Exit;
          if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 6)) <> 'BOYDEN' then begin
               if AColumn = dxDBGrid1CVNumero then begin
                    Value := ANode.Values[dxDBGrid1ggDataInsCV.Index];
                    if not VarIsNull(Value) then begin
                         if Value > 30 then AFont.Color := clGreen; // verde: almeno 30 gg. di vita
                         if Value > 60 then AFont.Color := $000080FF; // arancione: almeno 60 gg. di vita
                         if Value > 90 then AFont.Color := clYellow; // giallo: almeno 90 gg. di vita
                         if Value > 120 then AFont.Color := clRed; // rosso: almeno 120 gg. di vita
                         if Value > 180 then AFont.Color := clMaroon; // marrone: almeno 180 gg. di vita
                    end;
               end;
          end;
          if (Acolumn = dxDBGrid1ggUltimaDataIns) and (ANode.Values[dxDBGrid1ggUltimaDataIns.index] < xggDiffDi)
               and (ANode.Values[dxDBGrid1ggUltimaDataIns.index] > 0) then Acolor := clRed;
          if (Acolumn = dxDBGrid1ggDataUltimoContatto) and (ANode.Values[dxDBGrid1ggDataUltimoContatto.index] < xggDiffUc)
               and (ANode.Values[dxDBGrid1ggDataUltimoContatto.Index] > 0) then Acolor := clRed;
          if (Acolumn = dxDBGrid1DataProssimoColloquio) and (ANode.Values[dxDBGrid1DataProssimoColloquio.index] > 0)
               and (ANode.Values[dxDBGrid1DataProssimoColloquio.index] >= date) then Acolor := clYellow;
          if (Acolumn = dxDBGrid1ggDataUltimoColloquio) and (ANode.Values[dxDBGrid1ggDataUltimoColloquio.index] < xggDiffCol)
               and (ANode.Values[dxDBGrid1ggDataUltimoColloquio.index] > 0) then Acolor := clAqua;



          if (pos('INTERMEDIA', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) = 0) and (pos('EMERGENCY', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) = 0) then begin
               if ((Acolumn = dxDBGrid1Cognome) or (Acolumn = dxDBGrid1Nome)) and
                    (CheckVisioneCand(MainForm.xIDUtenteAttuale, ANode.Values[dxDBGrid1IDAnagrafica.index]))
                    then begin
                    AColor := $00FFB66C;
               end;
          end;
     end;
end;

procedure TSelPersNewForm.dxDBGrid1MouseUp(Sender: TObject;
     Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
     if (Button <> mbRight) or (Shift <> []) then Exit;
     TdxDBGridPopupMenuManager.Instance.ShowGridPopupMenu(Sender as TdxDBGrid);
end;

procedure TSelPersNewForm.Stampaelenco1Click(Sender: TObject);
begin
     if QRes1.EOF then exit;
     if not MainForm.CheckProfile('51') then Exit;
     if not LogEsportazioniCand then exit;
     QRElencoSel := TQRElencoSel.create(self);
     QRElencoSel.QRMemo1.Lines.Clear;
     if xChiamante = 1 then begin
          QRicLineeQuery.First;
          while not QRicLineeQuery.EOF do begin
               QRElencoSel.QRMemo1.Lines.Add(QRicLineeQuery.FieldByName('Descrizione').AsString);
               QRicLineeQuery.Next;
          end;
     end else begin
          TLinee.First;
          while not TLinee.EOF do begin
               QRElencoSel.QRMemo1.Lines.Add(TLinee.FieldByName('Descrizione').AsString);
               TLinee.Next;
          end;
     end;
     if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 5)) = 'ERGON' then begin
          QRElencoSel.QRLabel1.Caption := 'vecchio n�';
          QRElencoSel.QRDBText1.DataField := 'OLDCVNumero';
          // non funziona: QRElencoSel.QRLabel6.Visible:=False;
          //               QRElencoSel.QRDBText4.Visible:=False;
     end;
     QRElencoSel.Preview;
     QRElencoSel.Free;
end;

procedure TSelPersNewForm.Save(ADefaultExt, AFilter, AFileName: string; AMethod: TSaveMethod);
begin
     with SaveDialog do
     begin
          DefaultExt := ADefaultExt;
          Filter := AFilter;
          FileName := AFileName;
          if Execute then AMethod(FileName, True);
     end;
end;

procedure TSelPersNewForm.EsportainHTML1Click(Sender: TObject);
begin
     if not MainForm.CheckProfile('53') then Exit;
     if QRes1.EOF then exit;
     if not LogEsportazioniCand then exit;
     Save('htm', 'HTML File (*.htm; *.html)|*.htm', 'ExpGrid.htm', dxDBGrid1.SaveToHTML);
end;

procedure TSelPersNewForm.EsportainExcel1Click(Sender: TObject);
begin
     if not MainForm.CheckProfile('53') then Exit;
     if QRes1.EOF then exit;
     if not LogEsportazioniCand then exit;
     Save('xls', 'Microsoft Excel 4.0 Worksheet (*.xls)|*.xls', 'ExpGrid.xls', dxDBGrid1.SaveToXLS);
end;

procedure TSelPersNewForm.ToolbarButton971Click(Sender: TObject);
begin
     if not MainForm.CheckProfile('50') then Exit;
     LegendaRicForm := TLegendaRicForm.create(self);
     LegendaRicForm.ShowModal;
     LegendaRicForm.Free;
     Q.Close;
     Q.SQl.text := 'select ggDiffDi,ggDiffUc,ggDiffCol,CheckRicCandCV from global ';
     Q.Open;
     xggDiffDi := Q.FieldByname('ggDiffDi').asInteger;
     xggDiffUc := Q.FieldByname('ggDiffUc').asInteger;
     xggDiffCol := Q.FieldByname('ggDiffCol').asInteger;
     xCheckRicCandCV := Q.FieldByname('CheckRicCandCV').asBoolean;
     Q.Close;
     xTipoQuery := 0;
     EseguiQueryRic2;
end;

procedure TSelPersNewForm.ToolbarButton973Click(Sender: TObject);
var i: integer;
begin
     if not MainForm.CheckProfile('52') then Exit;
     if QRes1.isEmpty then exit;
     if Qres1.RecordCount > 501 then begin
          Messagedlg('Attenzione: il risultato della ricerca � troppo alto (Max 500) per procedere con l''invio delle mail', mtWarning, [mbOk], 0);
     end else begin
          FaxPresentazForm := TFaxPresentazForm.create(self);
          FaxPresentazForm.ASG1.RowCount := QRes1.RecordCount + 1;
          QRes1.First;
          QRes1.DisableControls;
          i := 1;
          while not QRes1.EOF do begin
               FaxPresentazForm.ASG1.addcheckbox(0, i, false, false);
               FaxPresentazForm.ASG1.Cells[0, i] := QRes1.FieldByName('Cognome').AsString + ' ' + QRes1.FieldByName('Nome').AsString;
               FaxPresentazForm.xArrayIDAnag[i] := QRes1.FieldByName('ID').AsInteger;
               QRes1.Next;
               inc(i);
          end;
          QRes1.EnableControls;
          FaxPresentazForm.PanClienti.Visible := False;
          FaxPresentazForm.xTotRighe := QRes1.RecordCount;
          FaxPresentazForm.ShowModal;
          FaxPresentazForm.Free;
     end;
end;

procedure TSelPersNewForm.Stampagriglia1Click(Sender: TObject);
begin
     if not MainForm.CheckProfile('53') then Exit;
     if QRes1.EOF then exit;
     if not LogEsportazioniCand then exit;
     //dxPrinter1.Preview(True, nil);
     dxPrinter1.Preview(True, dxPrinter1Link1);
end;

procedure TSelPersNewForm.TTabQuery_OLDAfterOpen(DataSet: TDataSet);
begin
     TTabOp.Open;
end;

function TSelPersNewForm.LogEsportazioniCand: boolean;
var xCriteri: string;
begin
     // restituisce FALSE se l'utente ha abortito l'operazione
     Result := True;
     if MessageDlg('ATTENZIONE:  l''esportazione di dati � un''operazione soggetta a registrazione' + chr(13) +
          'secondo la normativa sulla privacy.  SEI SICURO DI VOLER PROSEGUIRE ?', mtWarning, [mbYes, mbNo], 0) = mrNo then begin
          Result := False;
          exit;
     end;
     with Data do begin
          DB.BeginTrans;
          try
               if xChiamante = 1 then begin
                    QRicLineeQuery.First;
                    xCriteri := '';
                    while not QRicLineeQuery.EOF do begin
                         xCriteri := xCriteri + QRicLineeQuery.FieldByName('Descrizione').AsString + chr(13);
                         QRicLineeQuery.Next;
                    end;
               end else begin
                    TLinee.First;
                    xCriteri := '';
                    while not TLinee.EOF do begin
                         xCriteri := xCriteri + TLinee.FieldByName('Descrizione').AsString + chr(13);
                         TLinee.Next;
                    end;
               end;
               Q1.SQL.text := 'insert into LogEsportazCand (IDUtente,Data,Criteri,StringaSQL,ResultNum) ' +
                    ' values (:xIDUtente:,:xData:,:xCriteri:,:xStringaSQL:,:xResultNum:)';
               Q1.ParamByName['xIDUtente'] := MainForm.xIDUtenteAttuale;
               Q1.ParamByName['xData'] := Date;
               Q1.ParamByName['xCriteri'] := xCriteri;
               Q1.ParamByName['xStringaSQL'] := QRes1.SQL.Text;
               Q1.ParamByName['xResultNum'] := QRes1.RecordCount;
               Q1.ExecSQL;

               DB.CommitTrans;
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: operazione non effettuata', mtError, [mbOK], 0);
          end;
     end;
end;

procedure TSelPersNewForm.cancellasoggetti1Click(Sender: TObject);
var xPassword: string;
begin
     if QRes1.IsEmpty then exit;
     // cancellazione soggetti selezionati
     if MessageDlg('ATTENZIONE: la procedura � IRREVERSIBILE e DEFINITIVA !!' + chr(13) +
          'E'' necessario verificare il rispetto della normativa sulla Privacy e sulla' + chr(13) +
          'conservazione dei relativi dati.' + chr(13) + chr(13) +
          'SEI DAVVERO SICURO DI VOLER PROCEDERE ?', mtWarning,
          [mbNo, mbYes], 0) = mrNo then exit;

     // richiesta password amministratore
     xPassword := '';
     {
     if not InputQuery('cancellazione soggetti', 'Password di amministratore', xPassword) then exit;
     Q.Close;
     Q.SQL.text := 'select Password from Users where Nominativo=''Administrator''';
     Q.Open;
     if not CheckPassword(xPassword, Q.Fieldbyname('Password').asString) then begin
          MessageDlg('Password di amministratore errata', mtError, [mbOK], 0);
          Q.Close;
          exit;
     end;
     Q.Close;
     }
     InputPasswordForm.EPassword.text := 'EPassword';
     InputPasswordForm.caption := 'Inserimento password Administrator';
     InputPasswordForm.ShowModal;
     if InputPasswordForm.ModalResult = mrOK then begin
          xPassword := InputPasswordForm.EPassword.Text;
     end else exit;
     if not CheckPassword(xPassword, GetPwdAdministrator) then begin
          MessageDlg('Password errata', mtError, [mbOK], 0);
          exit;
     end;

     //fede:  14/11/2014 questa cazzo di procedura � da mettere a posto perch� se vengono fatti i filtri in griglia vengono eliminate anche le anagrafiche fuori dal filtro non va bene niente sta cosa
     if MessageDlg('Verranno eliminati TUTTI i dati di TUTTE le tabelle collegate per TUTTI i SOGGETTI !' + chr(13) +
          'IMPORTANTE: L''OPERAZIONE E'' IRREVERSIBILE e  non sar� pi� possibile recuperare i dati !!!' + chr(13) + chr(13) +
          'SEI DAVVERO SICURO DI VOLER PROCEDERE ?', mtWarning,
          [mbNo, mbYes], 0) = mrYes then begin
          // vai - DEFINITIVA per tutti i soggetti !!
          QRes1.First;
          while not QRes1.EOF do begin
               CancellaSogg(QRes1.FieldByName('ID').AsInteger);
               QRes1.Next;
          end;
          xTipoQuery := 0;
          EseguiQueryRic2;
     end;
end;

procedure TSelPersNewForm.PMStati1Click(Sender: TObject);
begin
     PMStati1.Checked := not PMStati1.Checked;
end;

procedure TSelPersNewForm.PMStati2Click(Sender: TObject);
begin
     PMStati2.Checked := not PMStati2.Checked;
end;

procedure TSelPersNewForm.PMStati3Click(Sender: TObject);
begin
     PMStati3.Checked := not PMStati3.Checked;
end;

procedure TSelPersNewForm.PMStati4Click(Sender: TObject);
begin
     PMStati4.Checked := not PMStati4.Checked;
end;

procedure TSelPersNewForm.PMStati5Click(Sender: TObject);
begin
     PMStati5.Checked := not PMStati5.Checked;
end;

procedure TSelPersNewForm.cancellatuttiisoggettiselezionati1Click(Sender: TObject);
var xPassword: string;
     k: integer;
begin
     if QRes1.IsEmpty then exit;
     // cancellazione soggetti selezionati
     if MessageDlg('ATTENZIONE: la procedura � IRREVERSIBILE e DEFINITIVA !!' + chr(13) +
          'E'' necessario verificare il rispetto della normativa sulla Privacy e sulla' + chr(13) +
          'conservazione dei relativi dati.' + chr(13) + chr(13) +
          'SEI DAVVERO SICURO DI VOLER PROCEDERE ?', mtWarning,
          [mbNo, mbYes], 0) = mrNo then exit;

     // richiesta password amministratore
     xPassword := '';
     {
     if not InputQuery('cancellazione soggetti', 'Password di amministratore', xPassword) then exit;
     Q.Close;
     Q.SQL.text := 'select Password from Users where Nominativo=''Administrator''';
     Q.Open;

     if not CheckPassword(xPassword, Q.Fieldbyname('Password').asString) then begin
          MessageDlg('Password di amministratore errata', mtError, [mbOK], 0);
          Q.Close;
          exit;
     end;
     Q.Close;
     }
     InputPasswordForm.EPassword.text := 'EPassword';
     InputPasswordForm.caption := 'Inserimento password Administrator';
     InputPasswordForm.ShowModal;
     if InputPasswordForm.ModalResult = mrOK then begin
          xPassword := InputPasswordForm.EPassword.Text;
     end else exit;
     if not CheckPassword(xPassword, GetPwdAdministrator) then begin
          MessageDlg('Password errata', mtError, [mbOK], 0);
          exit;
     end;
     if MessageDlg('Verranno eliminati TUTTI i dati di TUTTE le tabelle collegate per TUTTI i SOGGETTI SELEZIONATI !' + chr(13) +
          'IMPORTANTE: L''OPERAZIONE E'' IRREVERSIBILE e  non sar� pi� possibile recuperare i dati !!!' + chr(13) + chr(13) +
          'SEI DAVVERO SICURO DI VOLER PROCEDERE ?', mtWarning,
          [mbNo, mbYes], 0) = mrYes then begin
          // vai - DEFINITIVA per tutti i soggetti SELEZIONATI !!
          for k := 0 to dxDBGrid1.SelectedCount - 1 do begin
               QRes1.BookMark := dxDBGrid1.SelectedRows[k];
               CancellaSogg(QRes1.FieldByName('ID').AsInteger);
          end;
          xTipoQuery := 0;
          EseguiQueryRic2;
     end;
end;

procedure TSelPersNewForm.mettiinstatoeliminato1Click(Sender: TObject);
var k: integer;
begin
     if QRes1.EOF then exit;
     if MessageDlg('TUTTI i SOGGETTI SELEZIONATI verranno messi nello stato "eliminato".' + chr(13) +
          'SEI DAVVERO SICURO DI VOLER PROCEDERE ?', mtWarning,
          [mbNo, mbYes], 0) = mrYes then begin
          // metti in stato ELIMINATO
          with Data do begin
               DB.BeginTrans;
               try
                    for k := 0 to dxDBGrid1.SelectedCount - 1 do begin
                         QRes1.BookMark := dxDBGrid1.SelectedRows[k];
                         // modifica stato e tipostato
                         Q1.SQL.text := 'update Anagrafica set IDStato=:xIDStato:,IDTipoStato=:xIDTipoStato: where ID=' + QRes1.FieldByName('ID').AsString;
                         Q1.ParamByName['xIDStato'] := 30;
                         Q1.ParamByName['xIDTipoStato'] := 5;
                         Q1.ExecSQL;
                         // evento in storico
                         Q1.SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
                              'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xIDCliente:)';
                         Q1.ParamByName['xIDAnagrafica'] := QRes1.FieldByName('ID').AsInteger;
                         Q1.ParamByName['xIDEvento'] := 80;
                         Q1.ParamByName['xDataEvento'] := Date;
                         Q1.ParamByName['xAnnotazioni'] := 'eliminazione da motore di ricerca';
                         Q1.ParamByName['xIDRicerca'] := 0;
                         Q1.ParamByName['xIDUtente'] := MainForm.xIDUtenteAttuale;
                         Q1.ParamByName['xIDCliente'] := 0;
                         Q1.ExecSQL;
                    end;
                    DB.CommitTrans;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: operazione non effettuata', mtError, [mbOK], 0);
                    raise;
               end;
          end;
          xTipoQuery := 0;
          EseguiQueryRic2;
     end;
end;

procedure TSelPersNewForm.DBGrid3LivLinguaParlatoButtonClick(
     Sender: TObject; AbsoluteIndex: Integer);
var xID: string;
begin
     if xChiamante = 1 then begin
          if TrimRight(QRicLineeQuery.fieldbyname('Tabella').Value) = 'LingueConosciute' then begin
               xID := OpenSelFromTab('LivelloLingue', 'ID', 'LivelloConoscenza', 'Livello conoscenza', TrimRight(QRicLineeQuery.fieldbyname('LivLinguaParlato').asString), False);
               if xID <> '' then begin
                    if not (dsQRicLineeQuery.state = dsEdit) then QRicLineeQuery.Edit;
                    QRicLineeQuery.fieldbyname('LivLinguaParlato').Value := StrToIntDef(xID, 0);
                    QRicLineeQuery.Post;
               end;
          end;
     end else begin
          if TrimRight(TLineeTabella.Value) = 'LingueConosciute' then begin
               xID := OpenSelFromTab('LivelloLingue', 'ID', 'LivelloConoscenza', 'Livello conoscenza', TrimRight(TLineeLivLinguaParlato.asString), False);
               if xID <> '' then begin
                    if not (dsLinee.state = dsEdit) then TLinee.Edit;
                    TLineeLivLinguaParlato.Value := StrToIntDef(xID, 0);
                    TLinee.Post;
               end;
          end;
     end;
end;

procedure TSelPersNewForm.DBGrid3LivLinguaScrittoButtonClick(
     Sender: TObject; AbsoluteIndex: Integer);
var xID: string;
begin
     if xChiamante = 1 then begin
          if TrimRight(QRicLineeQuery.fieldbyname('Tabella').Value) = 'LingueConosciute' then begin
               xID := OpenSelFromTab('LivelloLingue', 'ID', 'LivelloConoscenza', 'Livello conoscenza', TrimRight(QRicLineeQuery.fieldbyname('LivLinguaScritto').asString), False);
               if xID <> '' then begin
                    if not (dsLinee.state = dsEdit) then QRicLineeQuery.Edit;
                    QRicLineeQuery.fieldbyname('LivLinguaScritto').Value := StrToIntDef(xID, 0);
                    QRicLineeQuery.Post;
               end;
          end;
     end else begin
          if TrimRight(TLineeTabella.Value) = 'LingueConosciute' then begin
               xID := OpenSelFromTab('LivelloLingue', 'ID', 'LivelloConoscenza', 'Livello conoscenza', TrimRight(TLineeLivLinguaScritto.asString), False);
               if xID <> '' then begin
                    if not (dsLinee.state = dsEdit) then TLinee.Edit;
                    TLineeLivLinguaScritto.Value := StrToIntDef(xID, 0);
                    TLinee.Post;
               end;
          end;
     end;
end;

procedure TSelPersNewForm.Salvaquerysufile1Click(Sender: TObject);
begin
     if not QRes1.Active then exit;
     if opendialog1.Execute then begin
          QRes1.SQL.SaveToFile(OpenDialog1.FileName);
     end;
end;

procedure TSelPersNewForm.BRiepCandClick(Sender: TObject);
var xID: integer;
begin
     MainForm.xIDAnagrafica := QRes1.FieldByName('ID').AsString;
     RiepilogoCandForm := TRiepilogoCandForm.Create(self);
     RiepilogoCandForm.ShowModal;
     RiepilogoCandForm.Free;
     xID := QRes1.FieldByName('ID').asInteger;
     QRes1.Close;
     QRes1.Open;
     QRes1.Locate('ID', xID, []);
end;

procedure TSelPersNewForm.DBGrid3DescCampoButtonClick(Sender: TObject; AbsoluteIndex: Integer);
begin
     PredisponiWizard;
     // setta variabile "pagina attiva"
     SelCriterioWizardForm.xPaginaAttiva := 1;
     SelCriterioWizardForm.ShowModal;
     if SelCriterioWizardForm.ModalResult = mrOK then begin
          SalvaDaWizard;
          RicreaStringaSQL;
     end;
end;

procedure TSelPersNewForm.DBGrid3DescTabellaButtonClick(Sender: TObject; AbsoluteIndex: Integer);
begin
     PredisponiWizard;
     // setta variabile "pagina attiva"
     SelCriterioWizardForm.xPaginaAttiva := 0;
     SelCriterioWizardForm.ShowModal;
     if SelCriterioWizardForm.ModalResult = mrOK then begin
          SalvaDaWizard;
          RicreaStringaSQL;
     end;
end;

procedure TSelPersNewForm.RicreaStringaSQL;
var xStringa, xTabella, xCampo, xTipoCampo, xValoriPossibili, xTabLookup, xOperatore, xValore: string;
     xVal1, xVal0, xQueryLookup, xSubQuery: string;
     xIDTabQuery: integer;
begin
     // ricrea la stringa SQL per la riga
     if xChiamante = 1 then begin
          Q.Close;
          Q.SQL.text := 'select * from TabQuery where DescTabella=:xDescTabella: and DescCampo=:xDescCampo: and visibile=1';
          Q.ParamByName['xDescTabella'] := QRicLineeQuery.fieldbyname('DescTabella').Value;
          Q.ParamByName['xDescCampo'] := QRicLineeQuery.fieldbyname('DescCampo').Value;
          Q.Open;
          // tabella e campo
          xTabella := Q.FieldByName('Tabella').asString;
          xCampo := Q.FieldByName('Campo').asString;
          xTipoCampo := Q.FieldByName('TipoCampo').asString;
          xSubQuery := Q.FieldByName('SubQuery').AsString;
          if (Q.FieldByName('TabLookup').asString <> '') {and (xSubQuery = '')} then
               xQueryLookup := 'select ' + Q.FieldByName('FieldLookup').asString + ' Dicitura from ' + Q.FieldByName('TabLookup').asString + ' where id=:xID:'
          else xQueryLookup := '';
          xValoriPossibili := Q.FieldByName('ValoriPossibili').asString;
          if xValoriPossibili <> '' then begin
               xVal1 := copy(xValoriPossibili, 1, pos(',', xValoriPossibili) - 1);
               xVal0 := copy(xValoriPossibili, pos(',', xValoriPossibili) + 1, length(xValoriPossibili));
          end;
          xTabLookup := Q.FieldByName('TabLookup').asString;
          xIDTabQuery := Q.FieldByName('ID').asInteger;

          // operatore
          Q.Close;
          Q.SQL.text := 'select * from TabQueryOperatori where IDTabQuery=:xIDTabQuery: and DescOperatore=:xDescOperatore:';
          Q.ParamByName['xIDTabQuery'] := xIDTabQuery;
          Q.ParamByName['xDescOperatore'] := QRicLineeQuery.fieldbyname('DescOperatore').Value;
          Q.Open;
          xOperatore := Q.FieldByName('Operatore').asString;
          // casistica valori
          if xTipoCampo = '' then begin
               if xOperatore = 'like' then xValore := '''%' + QRicLineeQuery.fieldbyname('Valore').Value + '%'''
               else xValore := '''' + QRicLineeQuery.fieldbyname('Valore').Value + '''';
          end;
          if xTipoCampo = 'boolean' then begin
               if QRicLineeQuery.fieldbyname('Valore').Value = xVal1 then xValore := '1'
               else xValore := '0';
          end;
          if xTipoCampo = 'date' then begin
               xValore := '''' + QRicLineeQuery.fieldbyname('Valore').Value + '''';
          end;
          if Uppercase(xTipoCampo) = 'DATESP' then begin
               if UpperCase(QRicLineeQuery.fieldbyname('DescCampo').Value) = 'COMPLEANNO' then begin
                    // xStringa := 'month (' + TTabQueryCampo.AsString + ') =  Month(GetDate()) ' +
                      //    'and day (' + TTabQueryCampo.AsString + ') =  day(GetDate())';

               end;
          end;
          if xTipoCampo = 'integer' then begin
               xValore := QRicLineeQuery.fieldbyname('Valore').Value;
          end;
          // composizione stringa
          if xSubQuery = '' then begin
               if UpperCase(xTipoCampo) = 'DATESP' then begin
                    if UpperCase(QRicLineeQuery.fieldbyname('DescCampo').Value) = 'COMPLEANNO' then
                         xStringa := 'month (' + xCampo + ') =  Month(GetDate()) ' +
                              'and day (' + xCampo + ') =  day(GetDate())';
               end else begin
                    if xTipoCampo = 'date' then
                         xStringa := 'DATEADD(dd, 0, DATEDIFF(dd, 0,' + xTabella + '.' + xCampo + '))' + ' ' + xOperatore + ' ' + xValore
                    else
                         xStringa := xTabella + '.' + xCampo + ' ' + xOperatore + ' ' + xValore;
               end
          end else begin
               xStringa := StringReplace(xSubQuery, ':xOperatore', xOperatore, [rfReplaceAll, rfIgnoreCase]);
               xStringa := StringReplace(xStringa, ':xValore', xValore, [rfReplaceAll, rfIgnoreCase]);
               if UpperCase(copy(xSubQuery, 1, 3)) = 'IN ' then
                    xStringa := 'Anagrafica.ID' + ' ' + xStringa;
          end;


          // query specifica per l'et�
          if copy(QRicLineeQuery.fieldbyname('DescCampo').Value, 1, 3) = 'et�' then
               xStringa := 'DATEDIFF(year, Anagrafica.DataNascita, getdate()) ' + xOperatore + ' ' + xValore;

          // salvataggio
          QRicLineeQuery.fieldbyname('Stringa').Value := xStringa;
          QRicLineeQuery.fieldbyname('QueryLookup').Value := xQueryLookup;
     end else begin
          Q.Close;
          Q.SQL.text := 'select * from TabQuery where DescTabella=:xDescTabella: and DescCampo=:xDescCampo: and visibile=1';
          Q.ParamByName['xDescTabella'] := TLineeDescTabella.Value;
          Q.ParamByName['xDescCampo'] := TLineeDescCampo.Value;
          Q.Open;
          // tabella e campo
          xTabella := Q.FieldByName('Tabella').asString;
          xCampo := Q.FieldByName('Campo').asString;
          xTipoCampo := Q.FieldByName('TipoCampo').asString;
          xSubQuery := Q.FieldByName('SubQuery').AsString;
          if (Q.FieldByName('TabLookup').asString <> '') {and (xSubQuery = '')} then
               xQueryLookup := 'select ' + Q.FieldByName('FieldLookup').asString + ' Dicitura from ' + Q.FieldByName('TabLookup').asString + ' where id=:xID:'
          else xQueryLookup := '';
          xValoriPossibili := Q.FieldByName('ValoriPossibili').asString;
          if xValoriPossibili <> '' then begin
               xVal1 := copy(xValoriPossibili, 1, pos(',', xValoriPossibili) - 1);
               xVal0 := copy(xValoriPossibili, pos(',', xValoriPossibili) + 1, length(xValoriPossibili));
          end;
          xTabLookup := Q.FieldByName('TabLookup').asString;
          xIDTabQuery := Q.FieldByName('ID').asInteger;

          // operatore
          Q.Close;
          Q.SQL.text := 'select * from TabQueryOperatori where IDTabQuery=:xIDTabQuery: and DescOperatore=:xDescOperatore:';
          Q.ParamByName['xIDTabQuery'] := xIDTabQuery;
          Q.ParamByName['xDescOperatore'] := TLineeDescOperatore.Value;
          Q.Open;
          xOperatore := Q.FieldByName('Operatore').asString;
          // casistica valori
          if xTipoCampo = '' then begin
               if xOperatore = 'like' then xValore := '''%' + TLineeValore.Value + '%'''
               else xValore := '''' + TLineeValore.Value + '''';
          end;
          if xTipoCampo = 'boolean' then begin
               if TLineeValore.Value = xVal1 then xValore := '1'
               else xValore := '0';
          end;
          if xTipoCampo = 'date' then begin
               xValore := '''' + TLineeValore.Value + '''';
          end;
          if Uppercase(xTipoCampo) = 'DATESP' then begin
               if UpperCase(TLineeDescCampo.Value) = 'COMPLEANNO' then begin
                    // xStringa := 'month (' + TTabQueryCampo.AsString + ') =  Month(GetDate()) ' +
                      //    'and day (' + TTabQueryCampo.AsString + ') =  day(GetDate())';

               end;
          end;
          if xTipoCampo = 'integer' then begin
               xValore := TLineeValore.Value;
          end;
          // composizione stringa
          if xSubQuery = '' then begin
               if UpperCase(xTipoCampo) = 'DATESP' then begin
                    if UpperCase(TLineeDescCampo.Value) = 'COMPLEANNO' then
                         xStringa := 'month (' + xCampo + ') =  Month(GetDate()) ' +
                              'and day (' + xCampo + ') =  day(GetDate())';
               end else begin
                    if xTipoCampo = 'date' then
                         xStringa := 'DATEADD(dd, 0, DATEDIFF(dd, 0,' + xTabella + '.' + xCampo + '))' + ' ' + xOperatore + ' ' + xValore
                    else
                         xStringa := xTabella + '.' + xCampo + ' ' + xOperatore + ' ' + xValore;
               end
          end else begin
               xStringa := StringReplace(xSubQuery, ':xOperatore', xOperatore, [rfReplaceAll, rfIgnoreCase]);
               xStringa := StringReplace(xStringa, ':xValore', xValore, [rfReplaceAll, rfIgnoreCase]);
               if UpperCase(copy(xSubQuery, 1, 3)) = 'IN ' then
                    xStringa := 'Anagrafica.ID' + ' ' + xStringa;
          end;


          // query specifica per l'et�
          if copy(TLineeDescCampo.Value, 1, 3) = 'et�' then
               xStringa := 'DATEDIFF(year, Anagrafica.DataNascita, getdate()) ' + xOperatore + ' ' + xValore;

          // salvataggio
          TLineeStringa.Value := xStringa;
          TLineeQueryLookup.Value := xQueryLookup;
     end
end;

procedure TSelPersNewForm.SalvaDaWizard(x: integer);
var xdesc: string;
begin

     if Trim(SelCriterioWizardForm.QTabelle.FieldByName('DescTabella').asString) = 'DistanzaLuogo' then begin
          case xChiamante of
               0: begin
                         Data.QTemp.Close;
                         Data.QTemp.SQL.text := 'select count(*) as tot from RicLineeQuery ' +
                              ' where IDRicerca=:xIDRic: ' +
                              ' and Tabella=''DistanzaLuogo ''' +
                              ' and Valore is not null and Valore<>'''' ';
                         Data.QTemp.ParamByName['xIDRic'] := DataRicerche.TRicerchePend.FieldByName('ID').asString;
                    end;
          else begin
                    Data.QTemp.Close;
                    Data.QTemp.SQL.text := 'select count(*) as tot from UserLineeQuery ' +
                         ' where IDAnagrafica=:xIDAnag: ' +
                         ' and Tabella=''DistanzaLuogo ''' +
                         ' and Valore is not null and Valore<>'''' ';
                    Data.QTemp.ParamByName['xIDAnag'] := MainForm.xIDUtenteAttuale;
               end;
          end;
          Data.QTemp.Open;
          if Data.QTemp.FieldByName('tot').AsInteger > 0 then begin
               showmessage('exit');
               exit;
          end;
     end;
     // x=0 --> normale
     // x=1 --> senza chiamare la form SelCriterioWizardForm
     if xchiamante = 1 then begin
          if not (DsQRicLineeQuery.State = dsEdit) then QRicLineeQuery.edit;

          QRicLineeQuery.FieldByName('DescTabella').asString := SelCriterioWizardForm.QTabelle.FieldByName('DescTabella').asString;

          Q.Close;
          Q.SqL.Text := 'select Tabella from TabQuery where visibile=1 and id=' + SelCriterioWizardForm.QCampi.fieldbyname('ID').asstring;
          q.open;

          QRicLineeQuery.FieldByName('Tabella').Value := q.fieldbyname('Tabella').asstring;

          if SelCriterioWizardForm.QCampi.Active then
               QRicLineeQuery.FieldByName('DescCampo').Value := SelCriterioWizardForm.QCampi.FieldByName('DescCampo').asString;
          if SelCriterioWizardForm.QOperatori.Active then
               QRicLineeQuery.fieldbyname('DescOperatore').Value := SelCriterioWizardForm.QOperatori.FieldByName('DescOperatore').asString;
          if x = 0 then begin
               with SelCriterioWizardForm do begin
                    if dxEdit1.visible then begin
                         QRicLineeQuery.fieldbyname('Valore').Value := dxEdit1.Text;
                         xDesc := dxEdit1.Text;
                    end;
                    if dxPickEdit1.visible then begin
                         QRicLineeQuery.fieldbyname('Valore').Value := dxPickEdit1.Text;
                         xDesc := dxPickEdit1.Text;
                    end;
                    if dxDateEdit1.visible then begin
                         QRicLineeQuery.fieldbyname('Valore').Value := DateToStr(dxDateEdit1.Date);
                         xDesc := DateToStr(dxDateEdit1.Date);
                    end;
                    if dxSpinEdit1.visible then begin
                         QRicLineeQuery.fieldbyname('Valore').Value := IntToStr(round(dxSpinEdit1.value));
                         xDesc := IntToStr(round(dxSpinEdit1.value));
                    end;
                    if PanGrid.visible then begin
                         QRicLineeQuery.fieldbyname('Valore').Value := QTable.FieldByName('ID').AsString;
                         xDesc := QTable.Fields[1].AsString;
                    end;
               end;
          end;
          QRicLineeQuery.fieldbyname('DescCompleta').Value := SelCriterioWizardForm.QCampi.FieldByName('DescCampo').asString + ' ' +
               SelCriterioWizardForm.QOperatori.FieldByName('DescOperatore').asString;
          QRicLineeQuery.fieldbyname('IDTabQueryOp').Value := SelCriterioWizardForm.QOperatori.FieldByName('ID').asInteger;
          QRicLineeQuery.fieldbyname('Descrizione').Value := SelCriterioWizardForm.QCampi.FieldByName('DescCampo').asString + ' ' +
               SelCriterioWizardForm.QOperatori.FieldByName('DescOperatore').asString + ' ' + xDesc;

          if QRicLineeQuery.fieldbyname('OpSucc').asString = '' then QRicLineeQuery.fieldbyname('OpSucc').asString := 'and';
          if QRicLineeQuery.fieldbyname('Next').asString = '' then QRicLineeQuery.fieldbyname('Next').asString := 'e';

     end else begin
          if not (DsLinee.State = dsEdit) then TLinee.edit;
          TLineeDescTabella.Value := SelCriterioWizardForm.QTabelle.FieldByName('DescTabella').asString;

          Q.Close;
          Q.SqL.Text := 'select Tabella from TabQuery where visibile=1 and id=' + SelCriterioWizardForm.QCampi.fieldbyname('ID').asstring;
          q.open;

          TLineeTabella.Value := q.fieldbyname('Tabella').asstring; //SelCriterioWizardForm.QTabelle.FieldByName('Tabella').asString;

          //Q.Close;
          if SelCriterioWizardForm.QCampi.Active then
               TLineeDescCampo.Value := SelCriterioWizardForm.QCampi.FieldByName('DescCampo').asString;
          if SelCriterioWizardForm.QOperatori.Active then
               TLineeDescOperatore.Value := SelCriterioWizardForm.QOperatori.FieldByName('DescOperatore').asString;
          if x = 0 then begin
               with SelCriterioWizardForm do begin
                    if dxEdit1.visible then begin
                         TLineeValore.Value := dxEdit1.Text;
                         xDesc := dxEdit1.Text;
                    end;
                    if dxPickEdit1.visible then begin
                         TLineeValore.Value := dxPickEdit1.Text;
                         xDesc := dxPickEdit1.Text;
                    end;
                    if dxDateEdit1.visible then begin
                         TLineeValore.Value := DateToStr(dxDateEdit1.Date);
                         xDesc := DateToStr(dxDateEdit1.Date);
                    end;
                    if dxSpinEdit1.visible then begin
                         TLineeValore.Value := IntToStr(round(dxSpinEdit1.value));
                         xDesc := IntToStr(round(dxSpinEdit1.value));
                    end;
                    if PanGrid.visible then begin
                         TLineeValore.Value := QTable.FieldByName('ID').AsString;
                         xDesc := QTable.Fields[1].AsString;
                    end;
               end;
          end;
          // descrizione completa (per ricerca semplice)
          TLineeDescCompleta.Value := SelCriterioWizardForm.QCampi.FieldByName('DescCampo').asString + ' ' +
               SelCriterioWizardForm.QOperatori.FieldByName('DescOperatore').asString; //SelCriterioWizardForm.QOperatori.FieldByName('DescCompleta').asString;
          // puntatore alla riga in TabQueryOperatori
          TLineeIDTabQueryOp.Value := SelCriterioWizardForm.QOperatori.FieldByName('ID').asInteger;
          //TLineeDescrizione.Value := SelCriterioWizardForm.QOperatori.FieldByName('DescCompleta').asString;
          TLineeDescrizione.Value := SelCriterioWizardForm.QCampi.FieldByName('DescCampo').asString + ' ' +
               SelCriterioWizardForm.QOperatori.FieldByName('DescOperatore').asString + ' ' + xDesc;
          {TTabQueryDescCampo.asString + ' ' +
                    TTabOpDescOperatore.asString + ' ' +
                         xValDesc}
          if TLineeOpSucc.Value = '' then TLineeOpSucc.Value := 'and';
          if TLineeOpNext.Value = '' then TLineeOpNext.Value := 'e';
     end;
end;

procedure TSelPersNewForm.PredisponiWizard;
begin
     // carica dati / predisponi wizard (setta le sue variabli)
     if xChiamante = 1 then begin
          with SelCriterioWizardForm do begin
               // TABELLA
               if not QTabelle.Active then QTabelle.Open;
               xTabella := QRicLineeQuery.fieldbyname('DescTabella').Value;
               if xTabella <> '' then QTabelle.Locate('DescTabella', xTabella, []);
               // CAMPO
               xCampo := QRicLineeQuery.fieldbyname('DescCampo').Value;
               if xCampo <> '' then begin
                    if not QCampi.Active then QCampi.Open else begin QCampi.Close; QCampi.Open; end;
                    QCampi.Locate('DescCampo', xCampo, []);
               end;
               // OPERATORE
               xOperatore := QRicLineeQuery.fieldbyname('DescOperatore').Value;
               if xOperatore <> '' then begin
                    if not QOperatori.Active then QOperatori.Open else begin QOperatori.Close; QOperatori.Open; end;
                    QOperatori.Locate('DescOperatore', xOperatore, []);
               end;
               // VALORE
               xValore := QRicLineeQuery.fieldbyname('Valore').Value;
          end;
     end else begin
          with SelCriterioWizardForm do begin
               // TABELLA
               if not QTabelle.Active then QTabelle.Open;
               xTabella := TLineeDescTabella.Value;
               if xTabella <> '' then QTabelle.Locate('DescTabella', xTabella, []);
               // CAMPO
               xCampo := TLineeDescCampo.Value;
               if xCampo <> '' then begin
                    if not QCampi.Active then QCampi.Open else begin QCampi.Close; QCampi.Open; end;
                    QCampi.Locate('DescCampo', xCampo, []);
               end;
               // OPERATORE
               xOperatore := TLineeDescOperatore.Value;
               if xOperatore <> '' then begin
                    if not QOperatori.Active then QOperatori.Open else begin QOperatori.Close; QOperatori.Open; end;
                    QOperatori.Locate('DescOperatore', xOperatore, []);
               end;
               // VALORE
               xValore := TLineeValore.Value;
          end;
     end;
end;

procedure TSelPersNewForm.DBGrid3DescOperatoreButtonClick(Sender: TObject; AbsoluteIndex: Integer);
begin
     PredisponiWizard;
     // setta variabile "pagina attiva"
     SelCriterioWizardForm.xPaginaAttiva := 2;
     SelCriterioWizardForm.ShowModal;
     if SelCriterioWizardForm.ModalResult = mrOK then begin
          SalvaDaWizard;
          RicreaStringaSQL;
          if xchiamante = 1 then begin
               QRicLineeQuery.edit;
               QRicLineeQuery.Post;
          end else begin
               Tlinee.Edit;
               TLinee.Post;
          end;
     end;
end;

procedure TSelPersNewForm.CBIRFileSystemClick(Sender: TObject);
begin
     CBIRaltreRegole.Visible := CBIRFileSystem.Checked;
end;

procedure TSelPersNewForm.DsLineeStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsLinee.State in [dsEdit, dsInsert];
     BCriterioNew.Enabled := not b;
     BCriterioMod.Enabled := not b;
     BCriterioDel.Enabled := not b;
     BCriterioDelTutti.Enabled := not b;
     BLineeOK.Enabled := b;
     BLineeCan.Enabled := b;
end;

procedure TSelPersNewForm.BLineeOKClick(Sender: TObject);
begin
     if xChiamante = 1 then begin
          QRicLineeQuery.edit;
          QRicLineeQuery.Post;
     end else begin
          Tlinee.Edit;
          TLinee.Post;
     end;
     xTipoQuery := 1;
     EseguiQueryRic2;
end;

procedure TSelPersNewForm.BLineeCanClick(Sender: TObject);
begin
     if xChiamante = 1 then
          QRicLineeQuery.Cancel
     else TLinee.Cancel;
end;

procedure TSelPersNewForm.DBGrid3CustomDrawCell(Sender: TObject;
     ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
     AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
     var AText: string; var AColor: TColor; AFont: TFont;
     var AAlignment: TAlignment; var ADone: Boolean);
var xID: integer;
begin
     if AColumn = DBGrid3Valore then begin
          if ANode.Strings[DBGrid3QueryLookup.Index] <> '' then begin
               xID := ANode.Values[DBGrid3ID.index];
               Q.Close;
               Q.SQL.text := ANode.Strings[DBGrid3QueryLookup.Index];
               Q.ParamByName['xID'] := StrToIntDef(ANode.Strings[DBGrid3Valore.Index], 0);
               Q.Open;
               AText := Q.FieldByName('Dicitura').asString;
          end;
     end;
end;

procedure TSelPersNewForm.DBGrid3Editing(Sender: TObject;
     Node: TdxTreeListNode; var Allow: Boolean);
begin
     //Allow:=Node.Strings[DBGrid3QueryLookup.Index]='';
end;

procedure TSelPersNewForm.DBGrid3ValoreEditButtonClick(Sender: TObject);
begin
     PredisponiWizard;
     // setta variabile "pagina attiva"
     SelCriterioWizardForm.xPaginaAttiva := 3;
     SelCriterioWizardForm.ShowModal;
     if SelCriterioWizardForm.ModalResult = mrOK then begin
          SalvaDaWizard;
          if xchiamante = 1 then begin
               QRicLineeQuery.edit;
               QRicLineeQuery.Post;
          end else begin
               Tlinee.Edit;
               TLinee.Post;
          end;
     end;
end;

procedure TSelPersNewForm.BCriterioNewClick(Sender: TObject);
var K: INTEGER;
begin
     SelCriterioWizardForm.xPaginaAttiva := 0;
     Panel2.Caption := '  Criteri impostati';
     with SelCriterioWizardForm do begin
          xTabella := '';
          xCampo := '';
          xOperatore := '';
          xValore := '';
     end;
     if not SelCriterioWizardForm.QTabelle.Active then SelCriterioWizardForm.QTabelle.Open;
     SelCriterioWizardForm.ShowModal;
     {if SelCriterioWizardForm.ModalResult = mrOK then begin
          TLinee.Insert;
          TLineeIDAnagrafica.Value := MainForm.xIDUtenteAttuale;
          SalvaDaWizard;
          TLinee.Post;
     end;    }
     if SelCriterioWizardForm.ModalResult = mrOK then begin
          {TLinee.Insert;
          TLineeIDAnagrafica.Value := MainForm.xIDUtenteAttuale;
          if (SelCriterioWizardForm.PanGrid.Visible = true) and (SelCriterioWizardForm.dxDBgrid1.SelectedCount > 1) then begin
               for k := 0 to SelCriterioWizardForm.dxDBGrid1.SelectedCount - 1 do begin
                    SalvaDaWizard(0, k);
                    SelCriterioWizardForm.QTabelle.Next;
               end;
          end else begin
               TLinee.Insert;
               TLineeIDAnagrafica.Value := MainForm.xIDUtenteAttuale;
               SalvaDaWizard;
               TLinee.Post;
          end; }
          //TLinee.Post;
          if SelCriterioWizardForm.PanGrid.Visible then begin
               if xChiamante = 1 then begin
                    for k := 0 to SelCriterioWizardForm.dxDBGrid1.SelectedCount - 1 do begin
                         SelCriterioWizardForm.QTable.BookMark := SelCriterioWizardForm.dxDBGrid1.SelectedRows[k];
                         QRicLineeQuery.Insert;
                         QRicLineeQuery.FieldByName('IDRicerca').AsInteger := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
                         SalvaDaWizard;
                         QRicLineeQuery.Post;
                         if Trim(QRicLineeQuery.FieldByName('Tabella').asString) = 'DistanzaLuogo' then
                              exit;
                    end;
               end else begin
                    for k := 0 to SelCriterioWizardForm.dxDBGrid1.SelectedCount - 1 do begin
                         SelCriterioWizardForm.QTable.BookMark := SelCriterioWizardForm.dxDBGrid1.SelectedRows[k];
                         TLinee.Insert;
                         TLineeIDAnagrafica.Value := MainForm.xIDUtenteAttuale;
                         SalvaDaWizard;
                         TLinee.Post;
                         if Trim(TLinee.FieldByName('Tabella').asString) = 'DistanzaLuogo' then
                              exit;
                    end;
               end;
          end else begin
               if xChiamante = 1 then begin
                    QRicLineeQuery.Insert;
                    QRicLineeQuery.FieldByName('IDRicerca').AsInteger := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
               end else begin
                    TLinee.Insert;
                    TLineeIDAnagrafica.Value := MainForm.xIDUtenteAttuale;
               end;
               SalvaDaWizard;
               if xChiamante = 1 then begin
                    QRicLineeQuery.Post;
                    //QRicLineeQuery.Close;
                    //QRicLineeQuery.FieldByName('IDRicerca').AsInteger := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
                    //QRicLineeQuery.Open;
                    if Trim(QRicLineeQuery.FieldByName('Tabella').asString) = 'DistanzaLuogo' then
                         exit;
               end else begin
                    TLinee.Post;
                    //TLinee.Close;
                    //TLineeIDAnagrafica.Value := MainForm.xIDUtenteAttuale;
                    //TLinee.Open;
                    if Trim(TLinee.FieldByName('Tabella').asString) = 'DistanzaLuogo' then
                         exit;
               end;
          end;
     end;
     // TLinee.Close;
      //TLinee.Open;

     xTipoQuery := 1;
     EseguiQueryRic2;
end;

procedure TSelPersNewForm.BCriterioModClick(Sender: TObject);
begin
     if xChiamante = 1 then begin
          if QRicLineeQuery.IsEmpty then exit;
     end else begin
          if TLinee.IsEmpty then exit;
     end;
     PredisponiWizard;
     // setta variabile "pagina attiva"
     SelCriterioWizardForm.xPaginaAttiva := 0;
     SelCriterioWizardForm.ShowModal;
     if SelCriterioWizardForm.ModalResult = mrOK then begin
          SalvaDaWizard;
          if xChiamante = 1 then begin
               QRicLineeQuery.Edit;
               QRicLineeQuery.Post;
          end else begin
               TLinee.Edit;
               TLinee.Post;
          end;
     end;
     xTipoQuery := 1;
     EseguiQueryRic2;
end;

procedure TSelPersNewForm.BCriterioDelClick(Sender: TObject);
begin
     if xChiamante = 1 then begin
          if QRicLineeQuery.IsEmpty then exit;
     end else begin
          if TLinee.IsEmpty then exit;
     end;
     // prosegui timing
     if xChiamante = 1 then SelPersForm.RiprendiAttivita;
     dxDBGrid1.Visible := False;
     LTot.Visible := False;
     if xChiamante = 1 then begin
          QRicLineeQuery.Delete;
          QRicLineeQuery.Close;
          QRicLineeQuery.Open;
     end else begin
          TLinee.Delete;
          TLinee.Close;
          TLinee.Open;
     end;
     xTipoQuery := 1;
     EseguiQueryRic2;
end;

procedure TSelPersNewForm.BCriterioDelTuttiClick(Sender: TObject);
begin
     if xChiamante = 1 then begin
          if QRicLineeQuery.IsEmpty then exit;
     end else begin
          if TLinee.IsEmpty then exit;
     end;
     // prosegui timing
     if xChiamante = 1 then SelPersForm.RiprendiAttivita;
     dxDBGrid1.Visible := False;
     LTot.Visible := False;
     if xChiamante = 1 then begin
          Query1.Close;
          Query1.SQl.Clear;
          Query1.SQl.add('delete from RicLineeQuery where IDRicerca =' + DataRicerche.TRicerchePend.FieldByName('ID').AsString);
          Query1.ExecSQL;
          QRicLineeQuery.Close;
          QRicLineeQuery.Open;
     end else begin
          dxDBGrid1.Visible := False;
          LTot.Visible := False;
          Query1.Close;
          Query1.SQl.Clear;
          Query1.SQl.add('delete from UserLineeQuery where IDAnagrafica=' + IntToStr(MainForm.xIDUtenteAttuale));
          Query1.ExecSQL;
          TLinee.Close;
          TLinee.Open;
     end;
end;

procedure TSelPersNewForm.DBGrid2CustomDrawCell(Sender: TObject;
     ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
     AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
     var AText: string; var AColor: TColor; AFont: TFont;
     var AAlignment: TAlignment; var ADone: Boolean);
var xID: integer;
begin
     if AColumn = DBGrid2Valore then begin
          if ANode.Strings[DBGrid2QueryLookup.Index] <> '' then begin
               xID := ANode.Values[DBGrid2ID.index];
               Q.Close;
               Q.SQL.text := ANode.Strings[DBGrid2QueryLookup.Index];
               Q.ParamByName['xID'] := StrToIntDef(ANode.Strings[DBGrid2Valore.Index], 0);
               Q.Open;
               AText := Q.FieldByName('Dicitura').asString;
          end;
     end;
end;

procedure TSelPersNewForm.DBGrid2ValoreEditButtonClick(Sender: TObject);
begin
     PredisponiWizard;
     // setta variabile "pagina attiva"
     SelCriterioWizardForm.xPaginaAttiva := 3;
     SelCriterioWizardForm.ShowModal;
     if SelCriterioWizardForm.ModalResult = mrOK then begin
          SalvaDaWizard;
          if xchiamante = 1 then begin
               QRicLineeQuery.edit;
               QRicLineeQuery.Post;
          end else begin
               Tlinee.Edit;
               TLinee.Post;
          end;
     end;
end;

procedure TSelPersNewForm.BInfoColloquioClick(Sender: TObject);
begin
     if QRes1.EOF then exit;
     InfoColloquioForm := TInfoColloquioForm.create(self);
     InfoColloquioForm.xIDAnagrafica := QRes1ID.Value;
     // esperienza lavorativa
     with InfoColloquioForm do begin
          // Apre i valori retributivi con l'ID dell'esp.lav. attuale
          QEspLavAttuale.ParamByName['xIDAnag'] := QRes1ID.value;
          QEspLavAttuale.Open;
          // se non c'� esp.lav.attuale ?
          if QEspLavAttuale.IsEmpty then
               MessageDlg('Attenzione, Manca l''esperienza lavorativa attuale del candidato.', mtWarning, [mbOK], 0); ;
          ShowModal;
     end;
     InfoColloquioForm.Free;
end;

procedure TSelPersNewForm.CaricaRicSemplice;
begin
     Q2.Close;
     Q2.SQL.text := 'select DescTabella,Tabella,Campo,DescCampo,DescOperatore,DescCompleta,TabQueryOperatori.ID IDTabQueryOp ' +
          'from TabQuery ' +
          'join TabQueryOperatori on TabQuery.ID=TabQueryOperatori.IDTabQuery ' +
          'where DescCompleta is not null and TabQueryOperatori.ID not in  ' +
          ' (select distinct IDTabQueryOp from UserLineeQuery where IDTabQueryOp is not null  ' +
          '  and IDAnagrafica=' + IntToStr(MainForm.xIDUtenteAttuale) + ') and visibile=1';
     Q2.Open;
     while not Q2.EOF do begin
          if xChiamante = 1 then begin
               with SelCriterioWizardForm do begin
                    QRicLineeQuery.Insert;
                    QRicLineeQuery.FieldByName('IDRicerca').Value := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
                    QRicLineeQuery.FieldByName('DescTabella').Value := Q2.FieldByName('DescTabella').asString;
                    QRicLineeQuery.FieldByName('Tabella').Value := Q2.FieldByName('Tabella').asString;
                    QRicLineeQuery.FieldByName('DescCampo').Value := Q2.FieldByName('DescCampo').asString;
                    QRicLineeQuery.FieldByName('DescOperatore').Value := Q2.FieldByName('DescOperatore').asString;
                    QRicLineeQuery.FieldByName('DescCompleta').Value := Q2.FieldByName('DescCompleta').asString;
                    QRicLineeQuery.FieldByName('IDTabQueryOp').Value := Q2.FieldByName('IDTabQueryOp').asInteger;
                    QRicLineeQuery.FieldByName('OpSucc').Value := 'and';
                    QRicLineeQuery.FieldByName('Next').Value := 'e';
                    QRicLineeQuery.Post;
               end;
          end else begin
               with SelCriterioWizardForm do begin
                    TLinee.Insert;
                    TLineeIDAnagrafica.Value := MainForm.xIDUtenteAttuale;
                    TLineeDescTabella.Value := Q2.FieldByName('DescTabella').asString;
                    TLineeTabella.Value := Q2.FieldByName('Tabella').asString;
                    TLineeDescCampo.Value := Q2.FieldByName('DescCampo').asString;
                    TLineeDescOperatore.Value := Q2.FieldByName('DescOperatore').asString;
                    TLineeDescCompleta.Value := Q2.FieldByName('DescCompleta').asString;
                    TLineeIDTabQueryOp.Value := Q2.FieldByName('IDTabQueryOp').asInteger;
                    TLineeOpSucc.Value := 'and';
                    TLineeOpNext.Value := 'e';
                    TLinee.Post;
               end;
          end;
          Q2.Next;
     end;
     Q2.Close;
end;

procedure TSelPersNewForm.PCTipoRicChange(Sender: TObject);
begin
     //if (PCTipoRic.ActivePageIndex = 0) and (xTipoRic = 0) then
      //    CaricaRicSemplice;
     if (PCTipoRic.ActivePageIndex = 0) or (PCTipoRic.ActivePageIndex = 1) then
     begin
          PCTipoRic.Height := 193;
          ToolbarButton971.visible := false;
          bEvento.Visible := true;
          Besegui.visible := true;
          Esegui1.Visible := true;

          //BAggiungiRic_old.visible := true;
          if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 6)) = 'BOYDEN' then
               BRiepCand.visible := true
          else
               BRiepCand.visible := false;

          BInfoColloquio.visible := true;
          Infocolloquio1.Visible := true;

          BInfoCand.visible := true;
          infocand1.Visible := true;

          BCurriculum.visible := true;
          curriculum1.visible := true;

          BDocumenti.visible := true;
          Documenti1.Visible := true;

          BAppunti.visible := true;
          Appuntiword1.Visible := true;
          BAggiungiRic.Visible := true;
     end
     else
     begin
          PCTipoRic.Height := panric.Height - 16;
          QMotoriRicerca.Parameters[0].value := main.MainForm.xIDUtenteAttuale;
          QMotoriRicerca.Open;
          ToolbarButton971.visible := true;


          BRiepCand.visible := false;
          bEvento.Visible := false;
          Besegui.visible := false;
          Esegui1.Visible := false;

          //BAggiungiRic_old.visible := true;

          BRiepCand.visible := false;


          BInfoColloquio.visible := false;
          Infocolloquio1.Visible := false;

          BInfoCand.visible := false;
          infocand1.Visible := false;

          BCurriculum.visible := false;
          curriculum1.visible := false;

          BDocumenti.visible := false;
          Documenti1.Visible := false;

          BAppunti.visible := false;
          Appuntiword1.Visible := false;
          BAggiungiRic.Visible := false;


     end;
end;

procedure TSelPersNewForm.Aggiungiinricerca1Click(Sender: TObject);
var i: integer;
     xMiniVal, xRic: string;
     xVai: boolean;
     SavePlace: TBookmark;
     xIDEvento, xIDClienteBlocco, xIDLocate: integer;
     xLivProtez, xDicMess, xPassword, xUtenteResp, xClienteBlocco: string;
     QLocal: TADOLinkedQuery;
begin

     if not Mainform.CheckProfile('210') then Exit;

     if pos('INTERMEDIA', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) > 0 then begin

          DataRicerche.QTemp.Close;
          DataRicerche.Qtemp.SQL.Text := 'select count(*) tot ' +
               ' from esperienzelavorative el' +
               ' where el.idanagrafica=' + QRes1.FieldByName('ID').asString + ' and el.attuale=1 ' +
               ' and el.AziendaNome is not NULL and el.AziendaNome <> '''' ' +
               ' and el.IDMansione is not NULL and el.IDMansione <> 0 ';
          DataRicerche.Qtemp.Open;
          if DataRicerche.Qtemp.FieldByName('tot').asinteger <= 0 then begin
               MessageDlg('Inserire l''esperienza Lavorativa Attuale del candidato per proseguire (con Azienda e Ruolo)', mtError, [mbOk], 0);
               exit;
          end;

          DataRicerche.QCkRegoleClienti.Close;
          DataRicerche.QCkRegoleClienti.SQL.Text := 'select c.id idcliente,el.attuale,el.idanagrafica,el.aziendanome,c.descrizione,c.idregolacliente,trc.descrizione regola, trc.flag FlagRegola ' +
               ' from esperienzelavorative el' +
               ' join ebc_clienti c on c.id=el.idazienda join tabregolecliente trc on trc.id=c.idregolacliente' +
               ' where el.idanagrafica=' + QRes1.FieldByname('ID').asString + ' and el.attuale=1';
          DataRicerche.QCkRegoleClienti.Open;
          if DataRicerche.QCkRegoleClienti.RecordCount > 0 then
               if DataRicerche.QCkRegoleClienti.FieldByName('FlagRegola').asboolean then
                    if MessageDlg('Limite per questo soggetto: ' + DataRicerche.QCkRegoleClienti.fieldByName('Regola').asstring + #10#13 + 'Vuoi Proseguire ?', mtWarning, [mbYes, mbNo], 0) = mrNO then
                         exit;
     end;


     if not OkIDAnag(QRes1.FieldByName('ID').AsInteger) then exit;
     if QRes1.FieldByName('IDTipoStato').AsInteger = 3 then
          if MessageDlg('Il candidato risulta INSERITO in azienda. Continuare?', mtWarning, [mbYes, mbNo], 0) = mrNo then exit;
          //Personalizzazione per non si sa chi.. tolto il 10/08/2015
     {xMiniVal := '';
     if not InputQuery('Inserimento nella ricerca', 'Valutazione (1 lettera):', xMiniVal) then exit; }
     Q.Close;
     Q.SQL.Text := 'select ID from EBC_CandidatiRicerche where IDRicerca=' + Dataricerche.TRicerchePend.FieldByName('ID').asString + ' and IDAnagrafica=' + QRes1.FieldByname('ID').asString;
     Q.Open;
     if Q.IsEmpty then begin
          xVai := True;
          // controllo se � in selezione
          if QRes1.FieldByName('IDTipoStato').AsInteger = 1 then begin
               if MessageDlg('Il soggetto risulta IN SELEZIONE: ' + chr(13) + xRic + chr(13) + 'PROCEDERE COMUNQUE ?', mtWarning, [mbYes, mbNo], 0) = mrNo then xVai := False;
          end;
          // controllo se � gi� associato ad un'altra ricerca per la STESSA azienda
          Q.Close; Q.SQL.clear;
          Q.SQl.Add('select EBC_Ricerche.Progressivo,EBC_StatiRic.StatoRic from EBC_CandidatiRicerche ');
          Q.SQl.Add('join EBC_Ricerche on EBC_CandidatiRicerche.IDRicerca=EBC_Ricerche.ID');
          Q.SQl.Add('join EBC_StatiRic on EBC_Ricerche.IDStatoRic=EBC_StatiRic.ID');
          Q.SQl.Add('where EBC_CandidatiRicerche.IDAnagrafica=:xIDAnag: and EBC_Ricerche.IDCliente=:xIDCliente:');
          //          Q.Prepare;
          Q.ParamByName['xIDAnag'] := QRes1.FieldByName('ID').AsInteger;
          Q.ParamByName['xIDCliente'] := DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger;
          Q.Open;
          if not Q.IsEmpty then begin
               xRic := '';
               while not Q.EOF do begin xRic := xRic + 'N�' + Q.FieldByName('Progressivo').asString + ' (' + Q.FieldByName('StatoRic').asString + ') '; Q.Next; end;
               if MessageDlg('Il soggetto � associato gi� alle seguenti ricerche per lo stesso cliente: ' + chr(13) + xRic + chr(13) + 'PROCEDERE COMUNQUE ?', mtWarning, [mbYes, mbNo], 0) = mrNo then xVai := False;
          end;
          // controllo incompatibilit� (per questa azienda e per le aziende a questa correlate)
          if IncompAnagCli(QRes1.FieldByName('ID').AsInteger, DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger) then
               if MessageDlg('Il soggetto risulta INCOMPATIBILE con questo cliente o con un cliente associato' + chr(13) +
                    'PROCEDERE COMUNQUE ?', mtWarning, [mbYes, mbNo], 0) = mrNo then xVai := False;
          // controllo appartenenza CV
          Q.Close;
          Q.SQL.Text := 'select IDProprietaCV from Anagrafica where ID=' + QRes1.FieldByName('ID').asString;
          Q.Open;
          if (not ((Q.FieldByName('IDProprietaCV').asString = '') or (Q.FieldByName('IDProprietaCV').asInteger = 0))) and
               (Q.FieldByName('IDProprietaCV').asInteger <> DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger) then
               if MessageDlg('ATTENZIONE: il CV risulta di propriet� di un altro cliente.' + chr(13) +
                    'Vuoi proseguire lo stesso ?', mtWarning, [mbYes, mbNo], 0) = mrNo then
                    xVai := False;
          // controllo blocco livello 1 o 2
          xIDClienteBlocco := CheckAnagInseritoBlocco1(QRes1.FieldByName('ID').AsInteger);
          if xIDClienteBlocco > 0 then begin
               xLivProtez := copy(IntToStr(xIDClienteBlocco), 1, 1);
               if xLivProtez = '1' then begin
                    xIDClienteBlocco := xIDClienteBlocco - 10000;
                    xClienteBlocco := GetDescCliente(xIDClienteBlocco);
                    xDicMess := 'Stai inserendo in commessa un candidato appartenente ad una azienda (' + xClienteBlocco + ') con blocco di livello 1, ';
               end else begin
                    xIDClienteBlocco := xIDClienteBlocco - 20000;
                    xClienteBlocco := GetDescCliente(xIDClienteBlocco);
                    xDicMess := 'Stai inserendo in commessa un candidato appartenente ad una azienda ATTIVA (' + xClienteBlocco + '), ';
               end;
               if MessageDlg('ATTENZIONE!! PROTEZIONE CLIENTI LIVELLO ' + xLivProtez + chr(13) + xDicMess +
                    'protetta dalle normative interne, se sei autorizzato premi SI, altrimenti premi NO. ' +
                    'Se si prosegue, l''operazione verr� registrata nello storico del soggetto come forzatura.', mtWarning, [mbYes, mbNo], 0) = mrNo then xVai := False
               else begin
                    if pos('INTERMEDIA', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) = 0 then begin
                         // pwd
                         xUtenteResp := GetDescUtenteResp(MainForm.xIDUtenteAttuale);
                         if not InputQuery('Forzatura blocco livello ' + xLivProtez, 'Password di ' + xUtenteResp, xPassword) then exit;
                         if not CheckPassword(xPassword, GetPwdUtenteResp(MainForm.xIDUtenteAttuale)) then begin
                              MessageDlg('Password errata', mtError, [mbOK], 0);
                              xVai := False;
                         end;
                    end;
                    if xVai then begin
                         // promemoria al resp.
                         QLocal := CreateQueryFromString('insert into Promemoria (IDUtente,IDUtenteDa,DataIns,Testo,Evaso,DataDaLeggere) ' +
                              'values (:xIDUtente:,:xIDUtenteDa:,:xDataIns:,:xTesto:,:xEvaso:,:xDataDaLeggere:)');
                         QLocal.ParamByName['xIDUtente'] := GetIDUtenteResp(MainForm.xIDUtenteAttuale);
                         QLocal.ParamByName['xIDUtenteDa'] := MainForm.xIDUtenteAttuale;
                         QLocal.ParamByName['xDataIns'] := Date;
                         QLocal.ParamByName['xTesto'] := 'forzatura livello ' + xLivProtez + ' per CV n� ' + QRes1.FieldByName('CVNumero').AsString;
                         QLocal.ParamByName['xEvaso'] := 0;
                         QLocal.ParamByName['xDataDaLeggere'] := Date;
                         QLocal.ExecSQL;
                         // registra nello storico soggetto
                         xIDEvento := GetIDEvento('forzatura Blocco livello ' + xLivProtez);
                         if xIDEvento > 0 then begin
                              with Data.Q1 do begin
                                   close;
                                   SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
                                        'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xIDCliente:)';
                                   ParamByName['xIDAnagrafica'] := QRes1.FieldByName('ID').AsInteger;
                                   ParamByName['xIDEvento'] := xIDevento;
                                   ParamByName['xDataEvento'] := Date;
                                   ParamByName['xAnnotazioni'] := 'cliente: ' + xClienteBlocco;
                                   ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
                                   ParamByName['xIDUtente'] := MainForm.xIDUtenteAttuale;
                                   ParamByName['xIDCliente'] := xIDClienteBlocco;
                                   ExecSQL;
                              end;
                         end;
                    end;
               end;
          end;

          if xVai then begin
               Data.DB.BeginTrans;
               try
                    if Q.Active then Q.Close;
                    Q.SQL.text := 'insert into EBC_CandidatiRicerche (IDRicerca,IDAnagrafica,Escluso,Stato,DataIns,Minival) ' +
                         'values (:xIDRicerca:,:xIDAnagrafica:,:xEscluso:,:xStato:,:xDataIns:,:xMinival:)';
                    Q.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
                    Q.ParamByName['xIDAnagrafica'] := QRes1.FieldByName('ID').AsInteger;
                    Q.ParamByName['xEscluso'] := 0;
                    Q.ParamByName['xStato'] := 'inserito da ricerca nomi';
                    Q.ParamByName['xDataIns'] := Date;
                    Q.ParamByName['xMiniVal'] := xMiniVal;
                    Q.ExecSQL;
                    // aggiornamento stato anagrafica
                    if pos('EMERGENCY', UpperCase(data.Global.fieldbyname('NomeAzienda').asString)) > 0 then begin
                         if QRes1.FieldByName('IDStato').AsInteger <> 28 then begin
                              if Q.Active then Q.Close;
                              Q.SQL.text := 'update Anagrafica set IDStato=28,IDTipoStato=2 where ID=' + QRes1.FieldByName('ID').asString;
                              Q.ExecSQL;
                         end;
                    end else begin
                         if (QRes1.FieldByName('IDStato').AsInteger <> 27) and (not (QRes1.FieldByName('IDTipoStato').AsInteger in [6, 7, 8])) then begin

                              if Q.Active then Q.Close;
                              Q.SQL.text := 'update Anagrafica set IDStato=27,IDTipoStato=1 where ID=' + QRes1.FieldByName('ID').asString;
                              Q.ExecSQL;
                         end;
                    end;

                    // aggiornamento storico
                    if Q.Active then Q.Close;
                    Q.SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
                         'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xIDCliente:)';
                    Q.ParamByName['xIDAnagrafica'] := QRes1.FieldByName('ID').AsInteger;
                    Q.ParamByName['xIDEvento'] := 19;
                    Q.ParamByName['xDataEvento'] := date;
                    Q.ParamByName['xAnnotazioni'] := 'ric.n� ' + TrimRight(DataRicerche.TRicerchePend.FieldByName('Progressivo').AsString) + ' (' + TrimRight(DataRicerche.TRicerchePend.FieldByName('Cliente').AsString) + ')';
                    Q.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
                    Q.ParamByName['xIDUtente'] := MainForm.xIDUtenteAttuale;
                    Q.ParamByName['xIDCliente'] := DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger;
                    Q.ExecSQL;
                    Data.DB.CommitTrans;
                    //DataRicerche.QCandRic.Next;
                    //SavePlace:=DataRicerche.QCandRic.GetBookmark;
                    DataRicerche.QCandRic.Close;
                    DataRicerche.QCandRic.Open;
                    //DataRicerche.QCandRic.GotoBookmark(SavePlace);
                    //DataRicerche.QCandRic.FreeBookmark(SavePlace);
              // metti anche questo in quelli da escludere per le prossime ricerche
                    for i := 1 to 500 do if xDaEscludere[i] = 0 then break;
                    xDaEscludere[i] := QRes1.FieldByName('ID').AsInteger;
                    //recuper ID per locate su record sucessivo
                    QRes1.Next;
                    xIDLocate := Qres1.FieldByname('ID').AsInteger;

                    xTipoQuery := 0;
                    EseguiQueryRic2;

                    Qres1.Locate('ID', xIDLocate, []);
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('Errore sul database:  inserimento non avvenuto', mtError, [mbOK], 0);
               end;
          end;
     end;
     // prosegui timing
     if xChiamante = 1 then SelPersForm.RiprendiAttivita;
end;

procedure AggiungiARicerca;
var i: integer;
     xMiniVal, xRic: string;
     xVai: boolean;
     SavePlace: TBookmark;
     xIDEvento, xIDClienteBlocco, xIDLocate: integer;
     xLivProtez, xDicMess, xPassword, xUtenteResp, xClienteBlocco: string;
     QLocal: TADOLinkedQuery;
begin

     if not Mainform.CheckProfile('210') then Exit;

     //showmessage(QRes1Cognome.AsString + ' ' + QRes1Nome.AsString);

     if not OkIDAnag(selpersnewform.QRes1.FieldByName('ID').AsInteger) then exit;

     //solo per intermedia, controllo dei limits
     if pos('INTERMEDIA', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) > 0 then begin

          DataRicerche.QTemp.Close;
          DataRicerche.Qtemp.SQL.Text := 'select count(*) tot ' +
               ' from esperienzelavorative el' +
               ' where el.idanagrafica=' + selpersnewform.QRes1.FieldByName('ID').asString + ' and el.attuale=1 ' +
               ' and el.AziendaNome is not NULL and el.AziendaNome <> '''' ' +
               ' and el.IDMansione is not NULL and el.IDMansione <> 0 ';
          DataRicerche.Qtemp.Open;
          if DataRicerche.Qtemp.FieldByName('tot').asinteger <= 0 then begin
               MessageDlg('Inserire l''esperienza Lavorativa Attuale del candidato per proseguire (con Azienda e Ruolo)', mtError, [mbOk], 0);
               exit;
          end;

          DataRicerche.QCkRegoleClienti.Close;
          DataRicerche.QCkRegoleClienti.SQL.Text := 'select c.id idcliente,el.attuale,el.idanagrafica,el.aziendanome,c.descrizione,c.idregolacliente,trc.descrizione regola, trc.flag FlagRegola ' +
               ' from esperienzelavorative el' +
               ' join ebc_clienti c on c.id=el.idazienda join tabregolecliente trc on trc.id=c.idregolacliente' +
               ' where el.idanagrafica=' + selpersnewform.QRes1.FieldByname('ID').asString + ' and el.attuale=1';
          DataRicerche.QCkRegoleClienti.Open;
          if DataRicerche.QCkRegoleClienti.RecordCount > 0 then
               if DataRicerche.QCkRegoleClienti.FieldByName('FlagRegola').asboolean then
                    if MessageDlg('Limite per ' + selpersnewform.QRes1Cognome.AsString + ' ' + selpersnewform.QRes1Nome.AsString + ': ' + DataRicerche.QCkRegoleClienti.fieldByName('Regola').asstring + #10#13 + 'Vuoi Proseguire ?', mtWarning, [mbYes, mbNo], 0) = mrNO then
                         exit;
     end;

     if selpersnewform.QRes1.FieldByName('IDTipoStato').AsInteger = 3 then
          if MessageDlg(selpersnewform.QRes1Cognome.AsString + ' ' + selpersnewform.QRes1Nome.AsString + ' risulta INSERITO in azienda. Continuare?', mtWarning, [mbYes, mbNo], 0) = mrNo then exit;
     xMiniVal := '';

     ElencoRicPendForm := TElencoRicPendForm.create(nil);

     ElencoRicpendform.xall := True;
     ElencoRicpendform.CBElencoRicPend.Visible := True;
     ElencoRicPendForm.CBElencoRicPendClick(nil);

     //ElencoRicpendform.dxDBGrid1.Filter.Add(ElencoRicpendform.dxDBGrid1Stato, 'attiva', 'attiva');
     ElencoRicPendForm.ShowModal;
     DataRicerche.TRicAnagIDX.Open;
     if ElencoRicPendForm.ModalResult = mrOK then begin
          xVai := true;
          // controllo se � gi� associato ad un'altra ricerca per la STESSA azienda
          Data.Q1.Close;
          Data.Q1.SQL.clear;
          Data.Q1.SQl.Add('select EBC_Ricerche.Progressivo,EBC_StatiRic.StatoRic from EBC_CandidatiRicerche ');
          Data.Q1.SQl.Add('join EBC_Ricerche on EBC_CandidatiRicerche.IDRicerca=EBC_Ricerche.ID ');
          Data.Q1.SQl.Add('join EBC_StatiRic on EBC_Ricerche.IDStatoRic=EBC_StatiRic.ID');
          Data.Q1.SQl.Add('where EBC_CandidatiRicerche.IDAnagrafica=:xIDAnag: and EBC_Ricerche.IDCliente=:xIDCliente:');
          Data.Q1.ParamByName['xIDAnag'] := selpersnewform.QRes1ID.Value; //data.TAnagrafica.FieldByName('ID').AsINteger;
          Data.Q1.ParamByName['xIDCliente'] := ElencoRicPendForm.QRicAttive.FieldByName('IDCliente').AsInteger;
          Data.Q1.Open;
          if not Data.Q1.IsEmpty then begin
               xRic := '';
               while not Data.Q1.EOF do begin xRic := xRic + 'N�' + Data.Q1.FieldByName('Progressivo').asString + ' (' + Data.Q1.FieldByName('StatoRic').asString + ') '; Data.Q1.Next; end;
               if MessageDlg(selpersnewform.QRes1Cognome.AsString + ' ' + selpersnewform.QRes1Nome.AsString + ' � associato gi� alle seguenti ricerche per lo stesso cliente: ' + chr(13) + xRic + chr(13) + 'PROCEDERE COMUNQUE ?', mtWarning, [mbYes, mbNo], 0) = mrNo then xVai := False;
          end;
          Data.Q1.Close;

          //controllo se il candidato � gia presente nella commessa che ho scelto
          data.Q1.close;
          data.q1.sql.text := ' select * from ebc_candidatiricerche where ' +
               ' idricerca=' + Elencoricpendform.QRicAttive.fieldbyname('id').asstring +
               ' and idanagrafica=' + selpersnewform.QRes1ID.AsString;
          data.Q1.Open;
          if data.Q1.RecordCount > 0 then begin
               MessageDlg(selpersnewform.QRes1Cognome.AsString + ' ' + selpersnewform.QRes1Nome.AsString + ' � gi� associato alla ricerca selezionata', mtWarning, [mbOK], 0);
               exit;
          end;




          // controllo incompatibilit�
      //          if IncompAnagCli(Data.TAnagraficaID.Value,ElencoRicPendForm.QRicAttiveIDCliente.Value) then
          if IncompAnagCli(selpersnewform.QRes1ID.Value, ElencoRicPendForm.QRicAttive.FieldByname('IDCliente').AsInteger) then
               if MessageDlg(selpersnewform.QRes1Cognome.AsString + ' ' + selpersnewform.QRes1Nome.AsString + ' risulta INCOMPATIBILE con questo cliente o con un cliente associato' + chr(13) +
                    'PROCEDERE COMUNQUE ?', mtWarning, [mbYes, mbNo], 0) = mrNo then begin
                    DataRicerche.TRicAnagIDX.Close;
                    ElencoRicPendForm.Free;
                    exit;
               end;
          // controllo blocco livello 1 o 2
      //          xIDClienteBlocco:=CheckAnagInseritoBlocco1(Data.TAnagraficaID.Value);
          xIDClienteBlocco := CheckAnagInseritoBlocco1(selpersnewform.QRes1ID.Value);
          if xIDClienteBlocco > 0 then begin
               xLivProtez := copy(IntToStr(xIDClienteBlocco), 1, 1);
               if xLivProtez = '1' then begin
                    xIDClienteBlocco := xIDClienteBlocco - 10000;
                    xClienteBlocco := GetDescCliente(xIDClienteBlocco);
                    xDicMess := 'Stai inserendo in commessa ' + selpersnewform.QRes1Cognome.AsString + ' ' + selpersnewform.QRes1Nome.AsString + ' appartenente ad una azienda (' + xClienteBlocco + ') con blocco di livello 1, ';
               end else begin
                    xIDClienteBlocco := xIDClienteBlocco - 20000;
                    xClienteBlocco := GetDescCliente(xIDClienteBlocco);
                    xDicMess := 'Stai inserendo in commessa ' + selpersnewform.QRes1Cognome.AsString + ' ' + selpersnewform.QRes1Nome.AsString + ' appartenente ad una azienda ATTIVA (' + xClienteBlocco + '), ';
               end;
               if MessageDlg('ATTENZIONE!! PROTEZIONE CLIENTI LIVELLO ' + xLivProtez + chr(13) + xDicMess +
                    'protetta dalle normative interne, se sei autorizzato premi SI, altrimenti premi NO. ' +
                    'Se si prosegue, l''operazione verr� registrata nello storico di ' + selpersnewform.QRes1Cognome.AsString + ' ' + selpersnewform.QRes1Nome.AsString + ' come forzatura.', mtWarning, [mbYes, mbNo], 0) = mrNo then xVai := False
               else begin
                    if pos('INTERMEDIA', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) = 0 then begin
                         // pwd
                         xUtenteResp := GetDescUtenteResp(MainForm.xIDUtenteAttuale);
                         if not InputQuery('Forzatura blocco livello ' + xLivProtez, 'Password di ' + xUtenteResp, xPassword) then exit;
                         if not CheckPassword(xPassword, GetPwdUtenteResp(MainForm.xIDUtenteAttuale)) then begin
                              MessageDlg('Password errata', mtError, [mbOK], 0);
                              xVai := False;
                         end;
                    end;
                    if xVai then begin
                         // promemoria al resp.
                         QLocal := CreateQueryFromString('insert into Promemoria (IDUtente,IDUtenteDa,DataIns,Testo,Evaso,DataDaLeggere) ' +
                              'values (:xIDUtente:,:xIDUtenteDa:,:xDataIns:,:xTesto:,:xEvaso:,:xDataDaLeggere:)');
                         QLocal.ParamByName['xIDUtente'] := GetIDUtenteResp(MainForm.xIDUtenteAttuale);
                         QLocal.ParamByName['xIDUtenteDa'] := MainForm.xIDUtenteAttuale;
                         QLocal.ParamByName['xDataIns'] := Date;
                         QLocal.ParamByName['xTesto'] := 'forzatura livello ' + xLivProtez + ' per CV n� ' + selpersnewform.QRes1CVNumero.AsString;
                         QLocal.ParamByName['xEvaso'] := 0;
                         QLocal.ParamByName['xDataDaLeggere'] := Date;

                         QLocal.ExecSQL;
                         // registra nello storico soggetto
                         xIDEvento := GetIDEvento('forzatura Blocco livello ' + xLivProtez);
                         if xIDEvento > 0 then begin
                              with Data.Q1 do begin
                                   close;
                                   SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
                                        'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xIDCliente:)';
                                   ParamByName['xIDAnagrafica'] := selpersnewform.QRes1ID.Value;
                                   ParamByName['xIDEvento'] := xIDevento;
                                   ParamByName['xDataEvento'] := Date;
                                   ParamByName['xAnnotazioni'] := 'cliente: ' + xClienteBlocco;
                                   ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
                                   ParamByName['xIDUtente'] := MainForm.xIDUtenteAttuale;
                                   ParamByName['xIDCliente'] := xIDClienteBlocco;

                                   ExecSQL;
                              end;
                         end;
                    end;
               end;
          end;
          if xvai then begin
               //               if not DataRicerche.TRicAnagIDX.FindKey([ElencoRicPendForm.QRicAttiveID.Value,Data.TAnagraficaID.Value]) then
               if not DataRicerche.TRicAnagIDX.FindKeyADO(VarArrayOF([ElencoRicPendForm.QRicAttive.FieldByName('ID').AsINteger, selpersnewform.QRes1ID.Value])) then
                    Data.DB.BeginTrans;
               try
                    Data.Q1.Close;
                    {                    Data.Q1.SQL.text:='insert into EBC_CandidatiRicerche (IDRicerca,IDAnagrafica,Escluso,Stato,DataIns) '+
                                             'values (:xIDRicerca,:xIDAnagrafica,:xEscluso,:xStato,:xDataIns)';
                                        Data.Q1.ParambyName('xIDRicerca').asInteger:=ElencoRicPendForm.QRicAttiveID.Value;
                                        Data.Q1.ParambyName('xIDAnagrafica').asInteger:=Data.TAnagraficaID.Value;
                                        Data.Q1.ParambyName('xEscluso').asBoolean:=False;
                                        Data.Q1.ParambyName('xStato').asString:='inserito direttam. in data '+DateToStr(Date);
                                        Data.Q1.ParambyName('xDataIns').asDateTime:=Date;}
                    Data.Q1.SQL.text := 'insert into EBC_CandidatiRicerche (IDRicerca,IDAnagrafica,Escluso,Stato,DataIns) ' +
                         'values (:xIDRicerca:,:xIDAnagrafica:,:xEscluso:,:xStato:,:xDataIns:)';
                    Data.Q1.ParambyName['xIDRicerca'] := ElencoRicPendForm.QRicAttive.FieldByName('ID').AsInteger;
                    Data.Q1.ParambyName['xIDAnagrafica'] := selpersnewform.QRes1ID.Value;
                    Data.Q1.ParambyName['xEscluso'] := 0;
                    Data.Q1.ParambyName['xStato'] := 'inserito direttam. in data ' + DateToStr(Date);
                    Data.Q1.ParambyName['xDataIns'] := Date;

                    Data.Q1.ExecSQL;
                    // storico
                    Data.Q1.SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
                         'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xIDCliente:)';
                    Data.Q1.ParambyName['xIDAnagrafica'] := selpersnewform.QRes1ID.Value;
                    Data.Q1.ParambyName['xIDEvento'] := 58;
                    Data.Q1.ParambyName['xDataEvento'] := Date;
                    Data.Q1.ParambyName['xAnnotazioni'] := copy('inserito nella ric.' + ElencoRicPendForm.QRicAttive.FieldByName('Progressivo').AsString + ' (' + ElencoRicPendForm.QRicAttive.FieldByName('Cliente').AsString + ')', 1, 256);
                    Data.Q1.ParambyName['xIDRicerca'] := ElencoRicPendForm.QRicAttive.FieldByname('ID').AsInteger;
                    Data.Q1.ParambyName['xIDUtente'] := MainForm.xIDUtenteAttuale;
                    Data.Q1.ParambyName['xIDCliente'] := ElencoRicPendForm.QRicAttive.FieldByName('IDCliente').AsInteger;

                    Data.Q1.ExecSQL;
                    // Anagrafica: cambio stato
            //                    Data.Q1.SQL.text:='update Anagrafica set IDTipoStato=1,IDStato=27 where ID='+Data.TAnagraficaID.asString;

                    if pos('EMERGENCY', UpperCase(data.Global.fieldbyname('NomeAzienda').asString)) > 0 then begin
                         if (selpersnewform.QRes1.FieldByName('IDStato').AsInteger <> 28) and (not (selpersnewform.QRes1.FieldByName('IDTipoStato').AsInteger in [6, 7, 8])) then begin
                              if Data.Q1.Active then Data.Q1.Close;
                              Data.Q1.SQL.text := 'update Anagrafica set IDStato=28,IDTipoStato=2 where ID=' + selpersnewform.QRes1.FieldByName('ID').asString;
                              Data.Q1.ExecSQL;
                         end;
                    end else begin
                         if (selpersnewform.QRes1.FieldByName('IDStato').AsInteger <> 27) and (not (selpersnewform.QRes1.FieldByName('IDTipoStato').AsInteger in [6, 7, 8])) then begin
                              if Data.Q1.Active then Data.Q1.Close;
                              Data.Q1.SQL.text := 'update Anagrafica set IDStato=27,IDTipoStato=2 where ID=' + selpersnewform.QRes1.FieldByName('ID').asString;
                              Data.Q1.ExecSQL;

                         end;
                    end;



                    Data.DB.CommitTrans;
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE:  operazione non completata', mtError, [mbOK], 0);
               end;
          end;
     end;
     DataRicerche.TRicAnagIDX.Close;
     //Data2.QAnagRicerche.Close;
     //Data2.QAnagRicerche.Open;
     ElencoRicPendForm.Free;
     //[/TONI20020909\]FINE
   //end;
     // prosegui timing
     if selpersnewform.xChiamante = 1 then SelPersForm.RiprendiAttivita;
end;


procedure TSelPersNewForm.EseguiQueryRic_OLD;
var xTab: array[1..5] of string;
     i, j, k, xIDEvento: integer;
     xS, xS1, xd, xLineeStringa, xStringaSQL, xTempQueryDistanza, xTempQueryCount: string;
     xApertaPar, xEspLav, xIRNote, xIRFileSystem: boolean;
     xQueryCount, xQueryDistanza: TADOQuery;
     xTimeStamp: TStringList;
begin
     xTimeStamp := TStringList.create;
     xTimeStamp.Add('INIZIO: ' + FormatDateTime('hh:nn:ss:zzz', Now));

     // prosegui timing
     if xChiamante = 1 then SelPersForm.RiprendiAttivita;

     if data.Global.FieldByName('debughrms').asboolean = true then showmessage('EseguiQueryRic');

     if DsLinee.State in [dsInsert, dsEdit] then begin
          TLinee.Post;
          TLinee.Close;
          TLinee.Open;
     end;
     TLinee.Last;
     if TLinee.FieldByName('OpSucc').AsString <> '' then begin
          TLinee.Edit;
          TLinee.FieldByName('OpNext').AsString := 'fine';
          TLinee.Post;
     end;
     TLinee.Close;
     TLinee.SQL.text := 'select * from UserLineeQuery where IDAnagrafica=:xIDAnag: and Valore is not null and Valore<>''''';
     TLinee.ParamByName['xIDAnag'] := MainForm.xIDUtenteAttuale;
     TLinee.Open;
     // filtro solo dove ci sono valori

     // tabelle implicate tranne Anagrafica
     QTabelleLista.Close;
     QTabelleLista.SQL.clear;
     QTabelleLista.SQL.add('select distinct Tabella from UserLineeQuery ');
     QTabelleLista.SQL.add('where Tabella<>''Anagrafica'' and Tabella<>''EBC_CandRic'' and IDAnagrafica=' + IntToStr(MAinForm.xIDUtenteAttuale));
     QTabelleLista.Open;
     QTabelleLista.First;
     for i := 1 to 5 do xTab[i] := '';
     i := 1;
     while not QTabelleLista.EOF do begin
          xTab[i] := TrimRight(QTabelleLista.FieldByName('Tabella').AsString);
          QTabelleLista.Next; inc(i);
     end;

     xEspLav := False;
     xS := 'from Anagrafica,AnagAltreInfo,AnagCampiPers';
     k := 1;
     while k <= 5 do begin
          //if xTab[i]='EsperienzeLavorative' then xEspLav:=True;
          //if (xTab[i]<>'')and(xTab[i]<>'EsperienzeLavorative') then begin
          if (xTab[k] <> '') and (UpperCase(xTab[k]) <> 'ANAGALTREINFO') and (UpperCase(xTab[k]) <> 'ANAGCAMPIPERS') then
               xS := xs + ',' + xTab[k];
          // if xTab[k] = 'AnagCampiPers' then
           //     xS := xS + ',CampiPers';
          inc(k);
     end;

     xTimeStamp.Add('Inizio assemblaggio QRes1: ' + FormatDateTime('hh:nn:ss:zzz', Now));

     QRes1.Close;
     QRes1.SQL.Clear;
     xTempQueryCount := '';

     QRes1.SQL.Add('select distinct Anagrafica.ID,Anagrafica.dacontattocliente,Anagrafica.Cognome,Anagrafica.Nome,Anagrafica.CVNumero,Anagrafica.OldCVNumero,');
     QRes1.SQL.Add('                Anagrafica.TelUfficio,Anagrafica.Cellulare,CVInseritoInData,CVDataReale,CVDataAgg,RecapitiTelefonici,');
     QRes1.SQL.Add('                Anagrafica.DataNascita,Anagrafica.IDStato,Anagrafica.IDTipoStato, ');
     QRes1.SQL.Add('                Anagrafica.Provincia, Anagrafica.Comune,anagrafica.indirizzo,anagrafica.cap,anagrafica.numcivico,anagrafica.tipostrada,');
     QRes1.SQL.Add('               anagrafica.domiciliotipostrada,anagrafica.domiciliocap,anagrafica.domicilionumcivico,anagrafica.domicilioindirizzo,anagrafica.domiciliocomune,anagrafica.domicilioprovincia, ');
     QRes1.SQL.Add('                DATEDIFF(day, EBC_CandidatiRicerche.DataIns, getdate()) ggUltimaDataIns,');
     QRes1.SQL.Add('                DATEDIFF(day, EBC_ContattiCandidati.Data, getdate()) ggDataUltimoContatto,');
     QRes1.SQL.Add('                EBC_CandidatiRicerche.DataImpegno DataProssimoColloquio,');
     QRes1.SQL.Add('                DATEDIFF(day, EBC_Colloqui.Data, getdate()) ggDataUltimoColloquio,');
     QRes1.SQL.Add('                DATEDIFF(day, Anagrafica.CVInseritoInData, getdate()) ggDataInsCV, NoCv,AnagAltreInfo.Retribuzione,EMail, AnagAltreInfo.Inquadramento, cast (anagfileD.IDAnagfile/anagfileD.IDAnagfile as bit) as FileCheck, ');
     QRes1.SQL.Add('                googledistanzaric.distanza,googledistanzaric.tempo,emailufficio ');
     // colonne visibili o meno a seconda del cliente
     if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 5)) = 'ERGON' then begin
          dxDBGrid1CVNumero.FieldName := 'OldCVNumero';
          dxDBGrid1CVNumero.Caption := 'Old n�CV';
          dxDBGrid1TipoStato.Visible := False;
          dxDBGrid1DomicilioComune.Visible := False;
     end;
     if (EIRNote.Text <> '') and (Data.Global.FieldByName('AnagFileDentroDB').AsBoolean) then
          QRes1.SQL.Add(xS + ',EBC_CandidatiRicerche,EBC_ContattiCandidati,EBC_Colloqui, (select distinct idanagrafica as IDAnagFile from anagfile) as anagfileD , AnagFile, googledistanzaric  ')
     else
          QRes1.SQL.Add(xS + ',EBC_CandidatiRicerche,EBC_ContattiCandidati,EBC_Colloqui, (select distinct idanagrafica as IDAnagFile from anagfile) as anagfileD , googledistanzaric ');
     QRes1.SQL.Add('where Anagrafica.ID is not null and Anagrafica.ID=AnagAltreInfo.IDAnagrafica and AnagcampiPers.IDAnagrafica =* Anagrafica.ID');
     QRes1.SQL.Add(' and anagfileD.IDAnagfile =* anagrafica.id ');
     if not xEspLav then begin
          //QRes1.SQL.Add('and Anagrafica.ID *= EsperienzeLavorative.IDAnagrafica');
          //QRes1.SQL.Add('and EsperienzeLavorative.ID = (select max(ID) from EsperienzeLavorative');
          //QRes1.SQL.Add('                               where Anagrafica.ID *= EsperienzeLavorative.IDAnagrafica)');
     end else begin
          // *** Questo codice non viene pi� considerato (vedi riga 458 e 459 commentate)
          QRes1.SQL.Add('and Anagrafica.ID = EsperienzeLavorative.IDAnagrafica');
          QRes1.SQL.Add('and EsperienzeLavorative.ID = (select max(ID) from EsperienzeLavorative');
          QRes1.SQL.Add('                               where Anagrafica.ID = EsperienzeLavorative.IDAnagrafica and');
          // oondizioni su EspLav
          TLinee.First;
          xLineeStringa := '';
          xApertaPar := False;
          while not TLinee.EOF do begin
               xLineeStringa := TrimRight(TLinee.FieldByName('Stringa').asString);
               if copy(TrimRight(TLinee.FieldByName('Stringa').AsString), 1, 13) = 'EsperienzeLav' then begin
                    TLinee.Next;
                    if TLinee.EOF then
                         // ultimo record -> senza AND alla fine
                         if xApertaPar then QRes1.SQL.Add(' ' + xLineeStringa + ')')
                         else QRes1.SQL.Add(' ' + xLineeStringa)
                    else begin
                         TLinee.Prior;
                         if TLinee.FieldByName('OpSucc').AsString = 'and' then
                              if not xApertaPar then
                                   QRes1.SQL.Add(' ' + xLineeStringa + ' and ')
                              else begin
                                   QRes1.SQL.Add(' ' + xLineeStringa + ') and ');
                                   xApertaPar := False;
                              end;
                         if TLinee.FieldbYnAME('OpSucc').AsString = 'or' then
                              if not xApertaPar then begin
                                   QRes1.SQL.Add(' (' + xLineeStringa + ' or ');
                                   xApertaPar := True;
                              end else begin
                                   QRes1.SQL.Add(' ' + xLineeStringa + ' or ');
                                   xApertaPar := True;
                              end;
                    end;
               end;
               TLinee.Next;
          end;
          // verifica ultime tre lettere sono "and" o "or" --> allora toglile
          xd := QRes1.SQL[QRes1.SQL.count - 1];
          if copy(xd, Length(xd) - 3, 3) = 'and' then
               QRes1.SQL[QRes1.SQL.count - 1] := copy(xd, 1, Length(xd) - 4);
          if copy(xd, Length(xd) - 2, 2) = 'or' then
               QRes1.SQL[QRes1.SQL.count - 1] := copy(xd, 1, Length(xd) - 3);
          QRes1.SQL.Add(')');
     end;

     // anzianit� CV
     if CBAnzCV.Checked then begin
          if cbanzianita.Text = 'Minore di mesi' then
               QRes1.SQL.Add('and (anagrafica.id in (select id from anagrafica where CVInseritoInData>=:xCVInseritoInData:) ' +
                    'or ' +
                    'anagrafica.id in (select id from anagrafica where CVdataagg>=:xCVInseritoInData:))')
          else
               QRes1.SQL.Add('and (anagrafica.id in (select id from anagrafica where CVInseritoInData<=:xCVInseritoInData:) ' +
                    'or ' +
                    'anagrafica.id in (select id from anagrafica where CVdataagg<=:xCVInseritoInData:))');
     end;
     // togliere gli esclusi (cio� gi� presenti in ricerca)
     if xChiamante = 1 then begin
          for j := 1 to DataRicerche.QCandRic.RecordCount do
               QRes1.SQL.Add('and Anagrafica.ID<>' + IntToStr(xDaEscludere[j]));
     end;

     xS1 := '';
     for i := 1 to 5 do begin
          if xTab[i] <> '' then
               if xTab[i] = '' then
                    xS1 := xS1 + ' and Anagrafica.ID=' + xTab[i] + '.IDAnagrafica and CampiPers.ID = AnagCampiPers.IDCampo'
               else xS1 := xS1 + ' and Anagrafica.ID=' + xTab[i] + '.IDAnagrafica';
     end;
     QRes1.SQL.Add(xS1);
     if TLinee.RecordCount > 0 then
          QRes1.SQL.Add('and');

     TLinee.First;
     xLineeStringa := '';
     xApertaPar := False;
     while not TLinee.EOF do begin
          xLineeStringa := TrimRight(TLinee.FieldByName('Stringa').asString);
          // per le LINGUE --> apposita clausola
          if Trim(TLinee.FieldByName('Tabella').AsString) = 'LingueConosciute' then begin
               // ## OLD ## xLineeStringa:='Anagrafica.ID in (select IDAnagrafica from LingueConosciute where lingua='''+copy(TLineeDescrizione.Value,9,Length(TLineeDescrizione.Value))+''')';
               xLineeStringa := 'Anagrafica.ID in (select IDAnagrafica from LingueConosciute where ' + TLineeStringa.Value;
               if TLineeLivLinguaParlato.asString <> '' then
                    xLineeStringa := xLineeStringa + ' and livello>=' + TrimRight(TLineeLivLinguaParlato.asString);
               if TLineeLivLinguaScritto.asString <> '' then
                    xLineeStringa := xLineeStringa + ' and livelloScritto>=' + TrimRight(TLineeLivLinguaScritto.asString);
               xLineeStringa := TrimRight(xLineeStringa) + ')';
          end;
          // per gli eventi occorsi --> apposita clausola
          if copy(TLinee.FieldByName('Stringa').asString, 1, 16) = 'Storico.IDEvento' then begin
               if pos('=', xLineeStringa) > 0 then begin
                    // evento occorso
                    xIDEvento := StrToIntDef(copy(xLineeStringa, pos('=', xLineeStringa) + 2, 4), 0);
                    xLineeStringa := IntToStr(xIDEvento) + ' in (select distinct IDEvento from storico st where anagrafica.id=st.idanagrafica ';
               end else begin
                    // evento non occorso
                    xIDEvento := StrToIntDef(copy(xLineeStringa, pos('>', xLineeStringa) + 2, 4), 0);
                    xLineeStringa := IntToStr(xIDEvento) + ' not in (select distinct IDEvento from storico st where anagrafica.id=st.idanagrafica ';
               end;
               //if TLineeGiorniDiff.Value>0 then
               //     xLineeStringa:=xLineeStringa+' and datediff(day,st.DataEvento,getdate())>'+TLineeGiorniDiff.AsString;
               xLineeStringa := xLineeStringa + ')';
          end;
          if copy(TLinee.FieldByName('Stringa').asString, 1, 16) = 'Storico.IDUtente' then begin
               if pos('=', xLineeStringa) > 0 then begin
                    // evento occorso
                    xIDEvento := StrToIntDef(copy(xLineeStringa, pos('=', xLineeStringa) + 2, 4), 0);
                    xLineeStringa := IntToStr(xIDEvento) + ' in (select distinct IDUtente from storico st where anagrafica.id=st.idanagrafica ';
               end else begin
                    // evento non occorso
                    xIDEvento := StrToIntDef(copy(xLineeStringa, pos('>', xLineeStringa) + 2, 4), 0);
                    xLineeStringa := IntToStr(xIDEvento) + ' not in (select distinct IDUtente from storico st where anagrafica.id=st.idanagrafica ';
               end;
               //if TLineeGiorniDiff.Value>0 then
               //     xLineeStringa:=xLineeStringa+' and datediff(day,st.DataEvento,getdate())>'+TLineeGiorniDiff.AsString;
               xLineeStringa := xLineeStringa + ')';
          end;


          TLinee.Next;
          if TLinee.Eof then
               // ultimo record -> senza AND alla fine
               if xApertaPar then QRes1.SQL.Add(' ' + TrimRight(xLineeStringa) + ')')
               else QRes1.SQL.Add(' ' + xLineeStringa)
          else begin
               TLinee.Prior;
               if TrimRight(TLinee.FieldByName('OpSucc').AsString) = 'and' then
                    if not xApertaPar then
                         QRes1.SQL.Add(' ' + xLineeStringa + ' and ')
                    else begin
                         QRes1.SQL.Add(' ' + xLineeStringa + ') and ');
                         xApertaPar := False;
                    end;
               if TrimRight(TLinee.FieldByName('OpSucc').AsString) = 'or' then
                    if not xApertaPar then begin
                         QRes1.SQL.Add(' (' + xLineeStringa + ' or ');
                         xApertaPar := True;
                    end else begin
                         QRes1.SQL.Add(' ' + xLineeStringa + ' or ');
                         xApertaPar := True;
                    end;
          end;
          TLinee.Next;
     end;

     // data ultimo inserimento in una ricerca
     QRes1.SQL.Add('and  Anagrafica.ID *= EBC_CandidatiRicerche.IDAnagrafica');
     QRes1.SQL.Add('     and EBC_CandidatiRicerche.DataIns =');
     QRes1.SQL.Add('     (select max(DataIns) from EBC_CandidatiRicerche');
     QRes1.SQL.Add('      where Anagrafica.ID *= EBC_CandidatiRicerche.IDAnagrafica)');
     // data ultimo contatto con il soggetto
     QRes1.SQL.Add('and Anagrafica.ID *= EBC_ContattiCandidati.IDAnagrafica');
     QRes1.SQL.Add('    and EBC_ContattiCandidati.Data=');
     QRes1.SQL.Add('    (select max(Data) from EBC_ContattiCandidati');
     QRes1.SQL.Add('     where Anagrafica.ID *= EBC_ContattiCandidati.IDAnagrafica)');
     // data ultimo colloquio
     QRes1.SQL.Add('and Anagrafica.ID *= EBC_Colloqui.IDAnagrafica');
     QRes1.SQL.Add('and EBC_Colloqui.Data=');
     QRes1.SQL.Add('    (select max(Data) from EBC_Colloqui');
     QRes1.SQL.Add('     where Anagrafica.ID *= EBC_Colloqui.IDAnagrafica)');

     // stati secondo la selezione
     if not PMStati1.Checked then
          QRes1.SQL.Add('and Anagrafica.IDTipoStato<>1');
     if not PMStati2.Checked then
          QRes1.SQL.Add('and Anagrafica.IDTipoStato<>2');
     if not PMStati3.Checked then
          QRes1.SQL.Add('and Anagrafica.IDTipoStato<>3');
     if not PMStati4.Checked then
          QRes1.SQL.Add('and Anagrafica.IDTipoStato<>5');
     if not PMStati5.Checked then
          QRes1.SQL.Add('and Anagrafica.IDTipoStato<>9');


     // Escludi dipendenti e altri stati H1HRMS
     QRes1.SQL.Add('and Anagrafica.IDTipoStato<>6');
     //QRes1.SQL.Add('and Anagrafica.IDTipoStato<>7');
     QRes1.SQL.Add('and Anagrafica.IDTipoStato<>8');
     QRes1.SQL.Add('and Anagrafica.IDTipoStato<>11');
     //QRes1.SQL.Add('and Anagrafica.IDTipoStato<>12');
     QRes1.SQL.Add('and Anagrafica.IDTipoStato<>15');

     if CBColloquiati.Checked then
          QRes1.SQL.Add('and Anagrafica.ID in (select IDAnagrafica from EBC_Colloqui)');

     if not CBPropri.Checked then
          QRes1.SQL.Add('and (Anagrafica.IDProprietaCV is null or Anagrafica.IDProprietaCV=0)');

     //aggiunta per il calcolo distanza google
     QRes1.SQL.Add('  and (googledistanzaric.idanagrafica=*anagrafica.id ');
     if xChiamante = 1 then
          QRes1.SQL.Add(' and googledistanzaric.idricerca=' + IntToStr(xIDRicerca));
     QRes1.SQL.Add(') ');
     ///////////////fine distanza google//////////////////////////

   // COMPOSIZIONE FINALE STRINGA SQL
     xStringaSQL := QRes1.SQL.Text;
     QRes1.SQL.Clear;
     QRes1.SQL.Add('SET DATEFORMAT dmy');
     QRes1.SQL.Add(xStringaSQL);


     //     if data.Global.FieldByName('debughrms').asboolean = true then
             //  ShowMessage('xIsFulltextInstalled=' + VarToStr(xIsFulltextInstalled) + ' xAnagAltreInfoOK=' + VarToStr(xAnagAltreInfoOK));

             // sapere se � abilitata sul CAMPO NOTE (AnagAltreInfo.Note)
     if data.Global.FieldByName('debughrms').asboolean = true then begin
          if xIsFulltextInstalled = true then showmessage('xIsFulltextInstalled=true') else showmessage('xIsFulltextInstalled=false');
          if xAnagAltreInfoOK = true then showmessage('xAnagAltreInfoOK=true') else showmessage('xAnagAltreInfoOK=false');
          if CBIRnote.Checked = true then showmessage('CBIRnote.Checked=true') else showmessage('CBIRnote.Checked=false');
     end;


     if (xIsFulltextInstalled) and (xAnagAltreInfoOK) and (CBIRnote.Checked) then begin
          if data.Global.FieldByName('debughrms').asboolean = true then showmessage('setto xIRNote=true');
          xIRNote := True;
     end
     else xIRNote := False;

     // INFORMATION RETRIEVAL
     if EIRNote.text <> '' then begin

          if not cbAdvFullText.checked then begin
               xEIRNote := '"*' + EIRNote.Text + '*"';
               if pos(' AND ', UpperCase(xEIRNote)) > 0 then xEIRNote := StringReplace(xEIrNote, ' and ', '*" and "*', [rfIgnoreCase, rfReplaceAll]);
               if pos(' OR ', UpperCase(xEIrNote)) > 0 then xEIRNote := StringReplace(xEIrNote, ' or ', '*" or "*', [rfIgnoreCase, rfReplaceAll]);
               if pos(' NEAR ', UpperCase(xEIrNote)) > 0 then xEIRNote := StringReplace(xEIrNote, ' near ', '*" near "*', [rfIgnoreCase, rfReplaceAll]);
          end else
               xEIRNote := EIRNote.Text;

          //FILE DENTRO DB
          if (CBIRFileSystem.Checked = true) and (CBIRFileSystem.Checked = true) then begin
               if data.Global.FieldByName('debughrms').asboolean = true then showmessage('file dentro db');
               if Data.Global.FieldByName('AnagFileDentroDB').AsBoolean then begin
                    QRes1.SQL.Add(' and anagrafica.id *= anagfile.idanagrafica');
                    // QRes1.SQL.Add(' and CONTAINS(AnagFile.DocFile, ''' + xEIRNote + ''')');
                    qRes1.sql.Add(' and anagrafica.id in (select idanagrafica from  anagfile where CONTAINS(DocFile, ''' + xEIRNote + ''') )');
               end
               else begin

                    // SU FILE SYSTEM


                    // sotto-query file-system
                    if (xIsFulltextInstalled) and (xFileSystemOK) and (CBIRFileSystem.Checked) then begin
                         if data.Global.FieldByName('debughrms').asboolean = true then showmessage('sotto-query file-system');
                         xIRFileSystem := True;
                         Q.Close;
                         Q.SQL.text := 'select IRFileSystemScope from global';
                         Q.Open;
                         xIRFileSystemScope := Q.FieldByName('IRFileSystemScope').asString;
                         // sotto-query AnagFile
                         QRes1.SQL.Add('and (Anagrafica.ID in ( SELECT distinct IDAnagrafica FROM ' +
                              'OPENQUERY(FileSystem, ''SELECT FileName,Characterization,DocAuthor,DocComments,DocSubject,DocTitle ' +
                              ' FROM SCOPE ('''' "' + xIRFileSystemScope + '" '''') ' +
                              ' WHERE CONTAINS( Contents, ''''' + xEIrNote + ''''' ) '') AS Q, AnagFile ' +
                              ' WHERE Q.FileName = AnagFile.SoloNome COLLATE SQL_Latin1_General_CP1_CI_AS ');
                         // altre regole
                         if CBIRaltreRegole.Checked then begin
                              if data.Global.FieldByName('debughrms').asboolean = true then showmessage('altre regole');
                              Q.Close;
                              Q.SQL.text := 'select Regola from IR_LinkRules';
                              Q.Open;
                              while not Q.EOF do begin
                                   QRes1.SQL.Add('UNION');
                                   QRes1.SQL.Add('SELECT distinct Anagrafica.ID IDAnagrafica FROM ' +
                                        'OPENQUERY(FileSystem, ''SELECT FileName,Characterization,DocAuthor,DocComments,DocSubject,DocTitle ' +
                                        ' FROM SCOPE ('''' "' + xIRFileSystemScope + '" '''') ' +
                                        ' WHERE CONTAINS( Contents, ''''' + xEIrNote + ''''' ) '') AS Q, Anagrafica ' +
                                        ' WHERE ' + Q.FieldByName('Regola').asString);
                                   Q.Next;
                              end;
                         end;

                         QRes1.SQL.Add(')');
                         Q.Close;
                         QRes1.SQL.Add(')');

                    end else xIRFileSystem := False;
               end;
          end;

          // INFORMATION RETRIEVAL SUL CAMPO NOTE (AnagAltreInfo.Note)
          if xIRNote then begin
               if data.Global.FieldByName('debughrms').asboolean = true then showmessage('INFORMATION RETRIEVAL SUL CAMPO NOTE ');
               if (CBIRFileSystem.Visible = true) and (CBIRFileSystem.Checked = true) then begin
                    QRes1.SQL.Add('UNION');
                    xTempQueryCount := Qres1.SQl.text;
                    xTempQueryCount := xTempQueryCount + 'select count (distinct anagrafica.id) ' + copy(xStringaSQL, pos('FROM', UpperCase(xStringaSQL)), length(xStringaSQL));
                    QRes1.SQL.add(xStringaSQL); // ancora tutta la stringa
                    QRes1.SQL.Add('and CONTAINS(AnagAltreInfo.Note, ''' + xEIrNote + ''') ');
               end else begin
                    QRes1.SQL.Add('and CONTAINS(AnagAltreInfo.Note, ''' + xEIrNote + ''') ');
                    xTempQueryCount := Qres1.SQl.text;
               end;

               { if not xIRFileSystem then begin
                     if (CBIRFileSystem.Visible = true) and (CBIRFileSystem.Checked = true) then
                          QRes1.SQL.Add('or CONTAINS(AnagAltreInfo.Note, ''' + xEIrNote + ''') ')
                     else
                          QRes1.SQL.Add('and CONTAINS(AnagAltreInfo.Note, ''' + xEIrNote + ''') ');
                end else begin
                     QRes1.SQL.Add('UNION');
                     QRes1.SQL.add(xStringaSQL); // ancora tutta la stringa
                     QRes1.SQL.Add('and CONTAINS(AnagAltreInfo.Note, ''' + xEIrNote + ''') ');
                end;  }
          end;
     end;



     // ORDINAMENTO
     case xTipoQuery of
          0: QRes1.SQL.Add(xOrdine);
     end;


     // anzianit� CV
     if CBAnzCV.Checked then
          QRes1.ParamByName['xCVInseritoInData'] := DateToStr(Date - (SEAnzCV.Value * 30));

     ScriviRegistry('QRes1', QRes1.SQL.text);
     if Data.Global.FieldByName('DebugHRMS').AsBoolean then
          //QRes1.SQL.SaveToFile(ExtractFileDir(application.exename) + '\Qres1.sql');
          QRes1.SQL.SaveToFile(Data.Global.fieldbyname('DirFileLog').AsString + '\Qres1.sql');
     if Data.Global.FieldByName('DebugHRMS').AsBoolean then
          //xQueryCount.SQL.SaveToFile(ExtractFileDir(application.exename) + '\QueryCount.sql');
          xQueryCount.SQL.SaveToFile(Data.Global.fieldbyname('DirFileLog').AsString + '\QueryCount.sql');


     case xTipoQuery of
          0: begin
                    if Data.Global.FieldByName('DebugHRMS').AsBoolean then
               //QRes1.SQL.SaveToFile(ExtractFileDir(application.exename) + '\Qres1.txt');
                         QRes1.SQL.SaveToFile(Data.Global.fieldbyname('DirFileLog').AsString + '\Qres1.txt');

                    xTimeStamp.Add('  Inizio esecuzione QRes1: ' + FormatDateTime('hh:nn:ss:zzz', Now));
                    QRes1.Open;
                    xTimeStamp.Add('    Fine esecuzione QRes1: ' + FormatDateTime('hh:nn:ss:zzz', Now));
               end;
          1: begin
                    xQueryCount := TADOQuery.Create(self);
                    xQueryCount.Connection := Data.DB;
                    if xTempQueryCount = '' then xTempQueryCount := QRes1.SQL.Text;
                    xQueryCount.SQL.Text := 'select count (distinct anagrafica.id) ' + copy(xTempQueryCount, pos('FROM', UpperCase(xTempQueryCount)), length(xTempQueryCount));
          //xQueryCount.SQL.Text := xTempQueryCount;

                    xTimeStamp.Add('Inizio esecuzione xQueryCount: ' + FormatDateTime('hh:nn:ss:zzz', Now));
                    xQueryCount.Open;
                    xTimeStamp.Add('Fine esecuzione xQueryCount: ' + FormatDateTime('hh:nn:ss:zzz', Now));
               end;
          2: begin
                    xQueryDistanza := TADOQuery.Create(self);
                    xQueryDistanza.Connection := Data.DB;
                    if xTempQueryDistanza = '' then xTempQueryDistanza := QRes1.SQL.Text;
                    xQueryDistanza.SQL.Text := 'select ' +
                         ' Anagrafica.ID,Anagrafica.Cognome,Anagrafica.Nome,anagrafica.tipostrada,anagrafica.cap, ' +
                         ' Anagrafica.Provincia,Anagrafica.Comune,anagrafica.indirizzo,anagrafica.numcivico, ' +
                         ' anagrafica.domiciliotipostrada,anagrafica.domiciliocap,anagrafica.domicilionumcivico, ' +
                         ' anagrafica.domicilioindirizzo,anagrafica.domiciliocomune,anagrafica.domicilioprovincia, ' +
                         copy(xTempQueryDistanza, pos('FROM', UpperCase(xTempQueryDistanza)), length(xTempQueryDistanza));

                    xTimeStamp.Add('Inizio esecuzione xQueryDistanza: ' + FormatDateTime('hh:nn:ss:zzz', Now));
                    xQueryDistanza.Open;
                    xTimeStamp.Add('Fine esecuzione xQueryDistanza: ' + FormatDateTime('hh:nn:ss:zzz', Now));


               end;
     end;

     TLinee.Close;
     TLinee.SQL.text := 'select * from UserLineeQuery where IDAnagrafica=:xIDAnag:';
     TLinee.ParamByName['xIDAnag'] := MainForm.xIDUtenteAttuale;
     TLinee.Open;
     case xTipoQuery of
          0: begin
                    dxDBGrid1.Visible := True;
                    LTot.Caption := IntToStr(QRes1.RecordCount);
                    LTot.Visible := True;
               end;
          1: begin
                    dxDBGrid1.Visible := False;
                    LTot.Caption := IntToStr(xQueryCount.Fields[0].Value);
                    LTot.Visible := True;
                    xQueryCount.Close;
                    xQueryCount.Free;
               end;
     end;

     xTimeStamp.Add('FINE: ' + FormatDateTime('hh:nn:ss:zzz', Now));
     ScriviRegistry('EseguiQueryRic_time', xTimeStamp.text);

end;

procedure TSelPersNewForm.BitBtn2Click(Sender: TObject);
var i: integer;
     lista, b: string;
begin
     dsQRes1.DataSet := QRes1;
     if FileExists(ExtractFilePath(Application.ExeName) + 'CalcolaDistanza.html') then begin
          case xChiamante of
               1: begin
                         Data.QTemp.Close;
                         Data.QTemp.SQL.text := 'select count(*) as tot from RicLineeQuery ' +
                              ' where IDRicerca=:xIDRic: ' +
                              ' and Tabella=''DistanzaLuogo ''' +
                              ' and Valore is not null and Valore<>'''' ';
                         Data.QTemp.ParamByName['xIDRic'] := DataRicerche.TRicerchePend.FieldByName('ID').asString;
                    end;
          else begin
                    Data.QTemp.Close;
                    Data.QTemp.SQL.text := 'select count(*) as tot from UserLineeQuery ' +
                         ' where IDAnagrafica=:xIDAnag: ' +
                         ' and Tabella=''DistanzaLuogo ''' +
                         ' and Valore is not null and Valore<>'''' ';
                    Data.QTemp.ParamByName['xIDAnag'] := MainForm.xIDUtenteAttuale;
               end;
          end;
          Data.QTemp.Open;
          if Data.QTemp.FieldByName('tot').AsInteger > 0 then begin
               xTipoQuery := 2;
               EseguiQueryRic2;
          end;
     end;
     xTipoQuery := 0;

     // EseguiQueryRic;
     EseguiQueryRic2;

     // memorizzo sul registro di sistema gli stati ricerca che voglio considerare
     for i := 0 to (pmstati.Items.Count - 1) do begin
          if pmstati.Items.Items[i].Checked = true then
               b := '1' else b := '0';
          lista := lista + b;
     end;
     if xChiamante = 1 then
          ScriviRegistry('StatiRicercaCommessa', lista)
     else ScriviRegistry('StatiRicerca', lista);
end;

procedure TSelPersNewForm.ApriQuery1DrawItem(Sender: TObject;
     ACanvas: TCanvas; ARect: TRect; Selected: Boolean);
begin;

end;

procedure TSelPersNewForm.PMExportPopup(Sender: TObject);
var MyPopUpItems: array[0..100] of TMenuItem;
     MyPopUpItemsDel: array[0..100] of TMenuItem;
     i, k: Integer;
     xMenuQuery: TADOQuery;
begin
     xMenuQuery := TADOQuery.Create(self);
     xMenuQuery.Connection := Data.DB;
     xMenuQuery.SQL.Text := 'select distinct NomeSalvataggio from UserLineeQuerySalvate where idanagrafica = :xidanagrafica';
     xMenuQuery.Parameters[0].Value := MainForm.xIDUtenteAttuale;
     xMenuQuery.Open;

     for k := 0 to ApriQuery1.Count - 1 do begin
          ApriQuery1.Delete(0);
          EliminaQuery1.Delete(0);
     end;

     i := 0;
     while not xMenuQuery.Eof do begin
          MyPopUpItems[i] := TMenuItem.Create(Self);
          MyPopUpItems[i].Caption := xMenuQuery.Fields[0].AsString;
          ApriQuery1.Insert(i, MyPopupItems[i]);
          MyPopUpItems[i].OnClick := MyPopup;

          MyPopUpItemsDel[i] := TMenuItem.Create(Self);
          MyPopUpItemsDel[i].Caption := xMenuQuery.Fields[0].AsString;
          EliminaQuery1.Insert(i, MyPopUpItemsDel[i]);
          MyPopUpItemsDel[i].OnClick := MyPopupDel;

          inc(i);
          xMenuQuery.Next;
     end;

     xMenuQuery.Free;
end;


procedure TSelPersNewForm.MyPopup(Sender: TObject);
var xNome: string;
begin

     {with Sender as TMenuItem do begin

     end;    }

     //with Sender as TMenuItem do xNome := Copy(Caption, 2, length(Caption));
     with Sender as TMenuItem do xNome := Caption;
     //showmessage(xnome);

     Query1.Close;
     Query1.SQL.text := 'delete from userlineequery where idanagrafica = ' + inttostr(MainForm.xIDUtenteAttuale);
     Query1.ExecSQL;

     Query1.Close;
     Query1.SQL.text := 'insert into userlineequery (IDAnagrafica ,Descrizione ,Stringa ,Tabella ,OpSucc , ' +
          'OpNext ,LivLinguaParlato ,LivLinguaScritto ,DescTabella ,DescCampo , ' +
          'DescOperatore ,Valore ,QueryLookup ,DescCompleta ,IDTabQueryOp ) ' +
          'select IDAnagrafica ,Descrizione ,Stringa ,Tabella ,OpSucc , ' +
          'OpNext ,LivLinguaParlato ,LivLinguaScritto ,DescTabella ,DescCampo , ' +
          'DescOperatore ,Valore ,QueryLookup ,DescCompleta ,IDTabQueryOp ' +
          'from userlineequerysalvate ' +
          'where IDAnagrafica=:xIDAnag: and NomeSalvataggio=:xNomeSalvataggio:';
     Query1.ParamByName['xIDAnag'] := MainForm.xIDUtenteAttuale;
     Query1.ParamByName['xNomeSalvataggio'] := xNome;
     Query1.ExecSQL;

     Query1.close;

     TLinee.Close;
     TLinee.Open;
     Panel2.Caption := '  Criteri impostati caricati da Query "' + xNome + '"';
     xTipoQuery := 1;
     EseguiQueryRic2;

end;

procedure TSelPersNewForm.SalvaQuery1Click(Sender: TObject);
var xNomeQuery: string;
begin
     if InputQuery('Salva con nome', 'Nome:', xNomeQuery) then begin
          Query1.Close;
          Query1.sql.Text := 'select * from userlineequerysalvate where IDAnagrafica=:xIDAnag: ' +
               ' and NomeSalvataggio=:xNomeSalvataggio:';
          Query1.ParamByName['xIDAnag'] := MainForm.xIDUtenteAttuale;
          Query1.ParamByName['xNomeSalvataggio'] := xNomeQuery;
          Query1.Open;

          if (Query1.RecordCount > 0) and (MessageDlg('Attenzione: Nome gi� esistente - Vuoi sovrascrivere il salvataggio precedente', mtWarning, [mbYes, mbNo], 0) = mrNO) then begin

               SalvaQuery1Click(self);

          end else begin
               if Query1.RecordCount > 0 then begin
                    Query1.Close;
                    Query1.sql.Text := 'delete from userlineequerysalvate where IDAnagrafica=:xIDAnag: ' +
                         ' and NomeSalvataggio=:xNomeSalvataggio:';
                    Query1.ParamByName['xIDAnag'] := MainForm.xIDUtenteAttuale;
                    Query1.ParamByName['xNomeSalvataggio'] := xNomeQuery;
                    Query1.ExecSQL;
               end;
               Query1.Close;
               Query1.SQL.Text := 'insert into userlineequerysalvate (IDAnagrafica ,Descrizione ,Stringa ,Tabella ,OpSucc , ' +
                    'OpNext ,LivLinguaParlato ,LivLinguaScritto ,DescTabella ,DescCampo , ' +
                    'DescOperatore ,Valore ,QueryLookup ,DescCompleta ,IDTabQueryOp,NomeSalvataggio ) ' +
                    'select :xIDAnag:, Descrizione ,Stringa ,Tabella ,OpSucc , ' +
                    'OpNext ,LivLinguaParlato ,LivLinguaScritto ,DescTabella ,DescCampo , ' +
                    'DescOperatore ,Valore ,QueryLookup ,DescCompleta ,IDTabQueryOp, :xNomeSalvataggio: ' +
                    'from userlineequery where IDAnagrafica= ' + inttostr(MainForm.xIDUtenteAttuale);
               Query1.ParamByName['xIDAnag'] := MainForm.xIDUtenteAttuale;
               Query1.ParamByName['xNomeSalvataggio'] := xNomeQuery;
               Query1.ExecSQL;
          end;
     end;
end;

procedure TSelPersNewForm.MyPopupDel(Sender: TObject);
var xNome: string;
begin

     {with Sender as TMenuItem do begin

     end;    }

     //with Sender as TMenuItem do xNome := Copy(Caption, 2, length(Caption));
     with Sender as TMenuItem do xNome := Caption;
     //showmessage(xnome);
     if MessageDlg('Sei sicuro di voler eliminare questa interrogazione?', mtWarning, [mbYes, mbNo], 0) = mrYes then begin
          Query1.Close;
          Query1.SQL.text := 'delete from userlineequerysalvate where idanagrafica = :xIDAnag: and ' +
               'NomeSalvataggio = :xNomeSalvataggio: ';
          Query1.ParamByName['xIDAnag'] := MainForm.xIDUtenteAttuale;
          Query1.ParamByName['xNomeSalvataggio'] := xNome;
          Query1.ExecSQL;

          PMExportPopup(self);

     end;
end;

procedure TSelPersNewForm.Button1Click(Sender: TObject);
begin
     formMotoriRicerca := TformMotoriRicerca.create(self);
     formMotoriRicerca.Edit1.text := QMotoriRicerca.fieldbyname('LinkPers').asString;
     formMotoriRicerca.memo1.text := QMotoriRicerca.fieldbyname('Notepers').asString;
     formMotoriRicerca.ShowModal;

     QsaveMotoriRicercaPers.Parameters[0].Value := main.MainForm.xIDUtenteAttuale;
     QsaveMotoriRicercaPers.Parameters[1].value := QMotoriRicerca.fieldbyname('ID').asInteger;
     QsaveMotoriRicercaPers.Parameters[2].value := formmotoriricerca.edit1.text;
     QsaveMotoriRicercaPers.Parameters[3].value := formmotoriricerca.Memo1.text;
     QsaveMotoriRicercaPers.Parameters[4].value := main.MainForm.xIDUtenteAttuale;
     QsaveMotoriRicercaPers.Parameters[5].value := QMotoriRicerca.fieldbyname('ID').asInteger;
     QsaveMotoriRicercaPers.Parameters[6].value := main.MainForm.xIDUtenteAttuale;
     QsaveMotoriRicercaPers.Parameters[7].value := QMotoriRicerca.fieldbyname('ID').asInteger;
     QsaveMotoriRicercaPers.Parameters[8].value := formmotoriricerca.edit1.text;
     QsaveMotoriRicercaPers.Parameters[9].value := formmotoriricerca.Memo1.text;
     QsaveMotoriRicercaPers.ExecSQL;

     formmotoriricerca.Free;

     QMotoriRicerca.close;
     QMotoriRicerca.Parameters[0].value := main.MainForm.xIDUtenteAttuale;
     QMotoriRicerca.Open;
end;

procedure TSelPersNewForm.Aggiungiadunaricerca1Click(Sender: TObject);
var k: integer;
begin
     //     for k := 0 to dxDBGrid1.SelectedCount - 1 do begin
              // qres1.Bookmark := dxDBGrid1.SelectedRows[k];
     if QRes1.State in [dsInactive] then exit
     else
          if QRes1.RecordCount = 0 then exit;


     AggiungiARicerca;
     // end;
end;

procedure TSelPersNewForm.Esegui1Click(Sender: TObject);
begin
     BitBtn2Click(self);
end;

procedure TSelPersNewForm.Infocolloquio1Click(Sender: TObject);
begin
     BInfoColloquioClick(self);
end;

procedure TSelPersNewForm.Infocand1Click(Sender: TObject);
begin
     BitBtn10Click(self);
end;

procedure TSelPersNewForm.Curriculum1Click(Sender: TObject);
begin
     BCurriculumClick(self);
end;

procedure TSelPersNewForm.aquestaricerca1Click(Sender: TObject);
begin
     Aggiungiinricerca1Click(self);
end;

procedure TSelPersNewForm.adaltrericerche1Click(Sender: TObject);
begin
     Aggiungiadunaricerca1Click(self);
end;

procedure TSelPersNewForm.Panel1ContextPopup(Sender: TObject;
     MousePos: TPoint; var Handled: Boolean);
begin
     if Qres1.active = false then begin
          Infocolloquio1.Enabled := false;
          Infocand1.Enabled := false;
          Curriculum1.Enabled := false;
          Documenti1.Enabled := false;
          Appuntiword1.Enabled := false;
          Aggiungi1.Enabled := false;
     end else begin
          Infocolloquio1.Enabled := true;
          Infocand1.Enabled := true;
          Curriculum1.Enabled := true;
          Documenti1.Enabled := true;
          Appuntiword1.Enabled := true;
          Aggiungi1.Enabled := true;
     end;
end;

procedure TSelPersNewForm.dxDBGrid1ContextPopup(Sender: TObject;
     MousePos: TPoint; var Handled: Boolean);
begin
     Infocolloquio1.Enabled := true;
     Infocand1.Enabled := true;
     Curriculum1.Enabled := true;
     Documenti1.Enabled := true;
     Appuntiword1.Enabled := true;
     Aggiungi1.Enabled := true;
end;

procedure TSelPersNewForm.bEvento_oldClick(Sender: TObject);
var i: integer;
     Vero, xInserito: boolean;
     xID: integer;
begin
     if not Mainform.CheckProfile('050') then Exit;
     if QRes1.EOF then exit;
     if QRes1.RecordCount > 0 then begin
          CambiaStato2Form := TCambiaStato2Form.create(self);
          CambiaStato2Form.ECogn.Text := QRes1.FieldByName('Cognome').AsString;
          CambiaStato2Form.ENome.Text := QRes1.FieldByName('Nome').AsString;
          CambiaStato2Form.PanStato.Visible := False;
          CambiaStato2Form.xIDDaStato := QRes1.FieldByName('IDStato').AsInteger;
          CambiaStato2Form.DEData.Date := Date;
          CambiaStato2Form.ShowModal;
          if CambiaStato2Form.ModalResult = mrOK then begin
               xInserito := InsEvento(QRes1.FieldByName('ID').AsInteger,
                    CambiaStato2Form.QNextStati.FieldByName('IDProcedura').AsInteger,
                    CambiaStato2Form.QNextStati.FieldByName('ID').AsInteger,
                    CambiaStato2Form.QNextStati.FieldByName('IDaStato').AsInteger,
                    CambiaStato2Form.QNextStati.FieldByName('IDTipoStatoA').AsInteger,
                    CambiaStato2Form.DEData.Date,
                    QRes1.FieldByName('Cognome').AsString,
                    QRes1.FieldByName('Nome').AsString,
                    CambiaStato2Form.Edit1.Text,
                    0,
                    MainForm.xIDUtenteAttuale,
                    0);
               xID := QRes1.FieldByName('ID').AsInteger;
               QRes1.Close;
               QRes1.Open;
               QRes1.locate('ID', xID, []);
               QRes1.FieldByName('ID').FocusControl;
               // LTotCand.Caption := IntToStr(DataRicerche.QCandRic.RecordCount);
          end;
          CambiaStato2Form.Free;
          //RiprendiAttivita;
     end;
end;

procedure TSelPersNewForm.Nuovocriterio1Click(Sender: TObject);
begin
     BCriterioNewClick(self);
end;

procedure TSelPersNewForm.Modificacriterio1Click(Sender: TObject);
begin
     BCriterioModClick(self);
end;

procedure TSelPersNewForm.Eliminacriterio1Click(Sender: TObject);
begin
     BCriterioDelclick(self);
end;

procedure TSelPersNewForm.Eliminatutti1Click(Sender: TObject);
begin
     BCriterioDelTuttiClick(self);
end;


function TogliWhiteSpace(str: string): string;
var
     i: integer;
begin
     for i := 0 to (length(str) - 1) do
          if (str[i] = ' ') then
               str[i] := '+';
     Result := str;
end;


procedure TSelPersNewForm.BCalcolaDistanza_oldClick(Sender: TObject);
var //IndirizzoCliente, IndirizzoAna: string;
     i, idtemp: integer;
     km, tempo, a, xstatus: string;
     xStartLat, xStartLng, xEndLat, xEndLng: string;
     km1, tempo1: extended;
begin
     //  ShowMessage(QRes1ID.asstring);

     if xidIndirizzoCliente = '' then begin
          MessageDlg('Attenzione, Nessun indirizzo cliente impostato nella commessa', MTWarning, [mbOk], 0);
          exit;
     end;

     if dxDBGrid1.SelectedCount = 0 then
          MessageDlg('Attenzione, Nessuna anagrafica selezionata', MTWarning, [mbOk], 0);


     for i := 0 to dxDBGrid1.SelectedCount - 1 do begin
          QRes1.Bookmark := dxDBGrid1.SelectedRows[i];

          // Memo1.Lines.Add('idaangrafica= ' + QRes1ID.asstring);
          DataRicerche.QDistanza.close;
          DataRicerche.QDistanza.SQL.Text := 'select tipostrada,comune,indirizzo,cap,provincia,numcivico from ebc_clienteindirizzi ' +
               ' where id=' + xidIndirizzoCliente;
          DataRicerche.QDistanza.Open;
          IndirizzoCliente := '';
          IndirizzoCliente := trim(DataRicerche.QDistanza.fieldbyname('tipostrada').asstring) + '+' + trim(DataRicerche.QDistanza.fieldbyname('indirizzo').asstring) + '+' +
               trim(DataRicerche.QDistanza.fieldbyname('numcivico').asstring) + '+' + {DataRicerche.QDistanza.fieldbyname('cap').asstring + ' ' + }
               DataRicerche.QDistanza.fieldbyname('comune').asstring; // + '+' + trim(DataRicerche.QDistanza.fieldbyname('provincia').asstring);
          IndirizzoCliente := StringReplace(IndirizzoCliente, '''', '\''', [rfReplaceAll]);


          case TipoIndirizzoGoogleMaps of
               1: begin //domicilio
                         IndirizzoAna := '';
                         if QRes1DomicilioComune.AsString <> '' then begin
                              IndirizzoAna := QRes1DomicilioComune.AsString;
                              if (QRes1domiciliotipostrada.AsString <> '') and
                                   (QRes1domicilioindirizzo.AsString <> '') then

                                   IndirizzoAna := IndirizzoAna + '+' + trim(QRes1domiciliotipostrada.AsString) + '+' +
                                        trim(QRes1domicilioindirizzo.AsString);
                         end;

                    end;
               2: begin //residenza
                         IndirizzoAna := '';
                         if QRes1Comune.AsString <> '' then begin
                              IndirizzoAna := QRes1Comune.AsString;
                              if (QRes1tipostrada.AsString <> '') and
                                   (QRes1indirizzo.AsString <> '') then

                                   IndirizzoAna := IndirizzoAna + '+' + trim(QRes1tipostrada.AsString) + '+' +
                                        trim(QRes1indirizzo.AsString);
                         end;
                    end;
          end;


          IndirizzoCliente := TogliWhiteSpace(IndirizzoCliente);
          IndirizzoAna := TogliWhiteSpace(IndirizzoAna);

          if data.Global.fieldbyname('debughrms').asboolean = true then
               showmessage('indirizzoana= ' + IndirizzoAna + chr(13) + 'indirizzocliente= ' + IndirizzoCliente);

          try

               if (IndirizzoCliente <> '') and (IndirizzoAna <> '') then begin
                    Data.qtemp2.close;
                    Data.qtemp2.SQL.text := 'select IDKey from FormWeb_social where social = ''Google Distance'' ';
                    Data.qtemp2.Open;
                    if Data.Qtemp2.RecordCount > 0 then begin
                         CalcolaDistanceDurata(IndirizzoCliente, IndirizzoAna, 'Driving', km, tempo, xStartLat, xStartLng, xEndLat, xEndLng, xstatus);

                         if data.Global.fieldbyname('debughrms').asboolean = true then
                              showmessage('km=' + km + chr(13) + 'tempo=' + tempo);


                         try
                              DataRicerche.QInsertDist.Close;
                              DataRicerche.QInsertDist.Parameters.ParamByName('xIdRicerca').value := xIDRicerca;
                              DataRicerche.QInsertDist.Parameters.ParamByName('xIdAnagrafica').value := SelPersNewForm.QRes1ID.Value;
                              DataRicerche.QInsertDist.Parameters.ParamByName('xDistanza').value := km; //FormatFloat('0.00', km1);
                              DataRicerche.QInsertDist.Parameters.ParamByName('xTempo').value := tempo; //FormatFloat('0.00', tempo1);
                              DataRicerche.QInsertDist.Parameters.ParamByName('xLatitudineCand').value := xStartLat;
                              DataRicerche.QInsertDist.Parameters.ParamByName('xLongitudineCand').value := xStartLng;
                              DataRicerche.QInsertDist.Parameters.ParamByName('xLatitudineCliente').value := xEndLat;
                              DataRicerche.QInsertDist.Parameters.ParamByName('xLongitudineCliente').value := xEndLng;
                              DataRicerche.QInsertDist.ExecSQL;
                         except
                              on E: Exception do ShowMessage(E.Message);
                         end;
                    end; // mm
               end;

               (*   try
                   //    Memo1.Lines.Add('iniz');
                       ExecuteJavaScript(WebBrowser1, 'distanza(' + '''' + IndirizzoCliente
                            + '''' + ',' + '''' + IndirizzoAna + '''' + ');');
                   //    Memo1.Lines.Add('2');
                       //Sleep(1000);
                       Delay(1000);
                    //   Memo1.Lines.Add('3');
                       a := '';
                       a := GetElementIdValue(SelPersNewForm.WebBrowser1, 'input', 'resulterrore', 'value');
                       if a = 'vuoto' then begin
                        //    showmessage(GetElementIdValue(WebBrowser1, 'input', 'resultkm', 'value') + ' km');
                        //    showmessage(GetElementIdValue(WebBrowser1, 'input', 'resulttempo', 'value') + ' minuti');
                            km := GetElementIdValue(SelPersNewForm.WebBrowser1, 'input', 'resultkm', 'value');
                            tempo := GetElementIdValue(SelPersNewForm.WebBrowser1, 'input', 'resulttempo', 'value');
                            km1 := StrToFloat(StringReplace(km, '.', ',', [rfReplaceAll]));
                            tempo1 := StrToFloat(StringReplace(tempo, '.', ',', [rfReplaceAll]));
                          //   ShowMessage(km);
                            try
                                 DataRicerche.QInsertDist.Close;
                                 DataRicerche.QInsertDist.Parameters.ParamByName('xIdRicerca').value := xIDRicerca;
                                 DataRicerche.QInsertDist.Parameters.ParamByName('xIdAnagrafica').value := SelPersNewForm.QRes1ID.Value;
                                 DataRicerche.QInsertDist.Parameters.ParamByName('xDistanza').Value := FormatFloat('0.00', km1);
                                 DataRicerche.QInsertDist.Parameters.ParamByName('xTempo').value := FormatFloat('0.00', tempo1);
                                 DataRicerche.QInsertDist.ExecSQL;
                            except
                                 on E: Exception do ShowMessage(E.Message);
                            end;

                       end else
                            showmessage(a);

                     //  Memo1.Lines.Add('------');

                    *)
          except
               on E: Exception do ShowMessage(E.Message);
          end;
          //ShowMessage(xidcliente);

     end;
     //   Memo1.Lines.SaveToFile('LogGoogleDistanza.txt');
     idtemp := QRes1id.AsInteger;
     QRes1.close;
     qres1.Open;
     QRes1.Locate('id', idtemp, []);
end;

procedure TSelPersNewForm.WebBrowser1DocumentComplete(Sender: TObject;
     const pDisp: IDispatch; var URL: OleVariant);
begin
     BCalcolaDistanza.Enabled := true;
end;

procedure TSelPersNewForm.Timer1Timer(Sender: TObject);
var a, km, tempo: string;
begin
     {  ShowMessage('2');
       timer1.enabled := false;
       a := '';
       a := GetElementIdValue(WebBrowser1, 'input', 'resulterrore', 'value');
       if a = 'vuoto' then begin
           // showmessage(GetElementIdValue(WebBrowser1, 'input', 'resultkm', 'value') + ' km');
           // showmessage(GetElementIdValue(WebBrowser1, 'input', 'resulttempo', 'value') + ' minuti');
            km := GetElementIdValue(WebBrowser1, 'input', 'resultkm', 'value');
            tempo := GetElementIdValue(WebBrowser1, 'input', 'resulttempo', 'value');
            try
                 QInsertDist.Close;
                 QInsertDist.Parameters.ParamByName('xIdRicerca').value := dataricerche.QRicAttiveID.Value;
                 QInsertDist.Parameters.ParamByName('xIdAnagrafica').value := QRes1ID.Value;
                 QInsertDist.Parameters.ParamByName('xDistanza').value := km;
                 QInsertDist.Parameters.ParamByName('xTempo').value := tempo;
                 QInsertDist.ExecSQL;
            except
                 on E: Exception do ShowMessage(E.Message);
            end;

       end else
            showmessage(a);}
end;

procedure TSelPersNewForm.AnagraficaResidenza1Click(Sender: TObject);
begin
     TipoIndirizzoGoogleMaps := 2;
     BCalcolaDistanzaClick(self);
end;

procedure TSelPersNewForm.AnagraficaDomicilio1Click(Sender: TObject);
begin
     TipoIndirizzoGoogleMaps := 1;
     BCalcolaDistanzaClick(self);
end;

procedure TSelPersNewForm.Estraifilecandidati1Click(Sender: TObject);
var xpassword, cartella: string;
     i: integer;
begin
     if QRes1.EOF then exit;
     InputPasswordForm.EPassword.text := 'EPassword';
     InputPasswordForm.caption := 'Inserimento password Administrator';
     InputPasswordForm.ShowModal;
     if InputPasswordForm.ModalResult = mrOK then begin
          xPassword := InputPasswordForm.EPassword.Text;
     end else exit;

     if not CheckPassword(xPassword, GetPwdAdministrator) then begin
          MessageDlg('Password errata', mtError, [mbOK], 0);
          exit;
     end else begin
          //showmessage('estrazione');
          if JvSelectDirectory1.Execute then cartella := JvSelectDirectory1.Directory else exit;

          for i := 0 to dxDBGrid1.SelectedCount - 1 do begin
               QRes1.Bookmark := dxDBGrid1.SelectedRows[i];

               Qestraifile.Close;
               Qestraifile.Parameters.ParamByName('ID').Value := QRes1.FieldByName('ID').value;
               Qestraifile.Open;

               while not Qestraifile.Eof do begin

                    if data.Global.fieldbyname('anagfiledentrodb').AsString = '1' then begin

                         if QestraifileDocFile.Value <> NULL then
                              QestraifileDocFile.SaveToFile(cartella + '\' + QestraifileTipo.AsString + '_' + QRes1Cognome.asstring + ' ' + QRes1Nome.AsString + '_' + QRes1ID.AsString + '_' + Qestraifilesolonome.AsString + '.' + QestraifileDocExt.AsString);

                    end else begin
                         if QestraifileNomeFile.Value <> NULL then
                              CopyFile(pchar(QestraifileNomeFile.AsString), pchar(cartella + '\' + QestraifileTipo.AsString + '_' + QRes1Cognome.asstring + ' ' + QRes1Nome.AsString + '_' + QRes1ID.AsString + '_' + ExtractFileName(QestraifileNomeFile.Value)), false);
                    end;
                    Qestraifile.next;
                    Application.ProcessMessages;
               end;
          end;
          if MessageDlg('Salvataggio file completato.' + chr(13) + 'Aprire la cartella in cui sono stati esportati i file?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
               ShellExecute(0, 'Open', pchar(cartella), '', '', SW_NORMAL);
     end;
end;

procedure TSelPersNewForm.EIRNoteKeyPress(Sender: TObject; var Key: Char);
begin
     if key = chr(13) then BitBtn2Click(self);
end;

procedure TSelPersNewForm.BGraficaClick(Sender: TObject);
var l: tstringlist;
     i: integer;
begin
     l := tstringlist.Create;
     for i := 0 to SelPersNewForm.ComponentCount - 1 do begin
          if SelPersNewForm.Components[i].Classname = 'TPanel' then begin
               // showmessage( CurriculumForm.Components[i].name);
               l.Add(SelPersNewForm.Components[i].name + '.Color := Mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;');
               l.Add(SelPersNewForm.Components[i].name + '.font.color := Mainform.AdvToolBarOfficeStyler1.font.Color;');
               l.Add(SelPersNewForm.Components[i].name + '.font.name := Mainform.AdvToolBarOfficeStyler1.font.name;');
          end;

          if (SelPersNewForm.Components[i].Classname = 'tgroupbox') then begin
               l.Add(SelPersNewForm.Components[i].name + '.Color := Mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;');
          end;

          if SelPersNewForm.Components[i].Classname = 'TDBCtrlGrid' then
               l.Add(SelPersNewForm.Components[i].name + '.Color := Mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;');

          { if ((SelPersNewForm.Components[i].Classname <> 'TPanel') and
                (SelPersNewForm.Components[i].Classname <> 'tgroupbox') and
                (SelPersNewForm.Components[i].Classname <> 'TDBCtrlGrid')) then begin
                l.Add('            ' + SelPersNewForm.Components[i].name);
           end;}
          {if (SelPersNewForm.Components[i].Classname = 'TWallpaper') then begin
               l.Add(SelPersNewForm.Components[i].name + '.Color := Mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;');
          end; }
     end;
     l.SaveToFile('graficaSelPersNew.txt');
     l.free;

end;

procedure TSelPersNewForm.BesciClick(Sender: TObject);
begin
     SelPersNewForm.ModalResult := mrCancel;
end;

procedure TSelPersNewForm.AttacDSClick(Sender: TObject);
begin
     dxDBGrid1.DataSource := DsQRes1;
end;

procedure TSelPersNewForm.CloseRes1Click(Sender: TObject);
begin
     QRes1.Close;
end;

procedure TSelPersNewForm.BCalcolaDistanzaClick(Sender: TObject);
var IndirizzoCliente, IndirizzoAna: string;
     i, idtemp: integer;
     km, tempo, a, xstatus: string;
     xStartLat, xStartLng, xEndLat, xEndLng: string;
     km1, tempo1: extended;
begin
     //  ShowMessage(QRes1ID.asstring);

     if xidIndirizzoCliente = '' then begin
          MessageDlg('Attenzione, Nessun indirizzo cliente impostato nella commessa', MTWarning, [mbOk], 0);
          exit;
     end;

     if dxDBGrid1.SelectedCount = 0 then
          MessageDlg('Attenzione, Nessuna anagrafica selezionata', MTWarning, [mbOk], 0);


     DataRicerche.QDistanza.close;
     DataRicerche.QDistanza.SQL.Text := 'select tipostrada,comune,indirizzo,cap,provincia,numcivico from EBC_ClienteIndirizzi ' +
          ' where id=' + xidIndirizzoCliente;
     DataRicerche.QDistanza.Open;
     IndirizzoCliente := '';
     IndirizzoCliente := trim(DataRicerche.QDistanza.fieldbyname('tipostrada').asstring) + '+' + trim(DataRicerche.QDistanza.fieldbyname('indirizzo').asstring) + '+' +
          trim(DataRicerche.QDistanza.fieldbyname('numcivico').asstring) + '+' + {DataRicerche.QDistanza.fieldbyname('cap').asstring + ' ' + }
          DataRicerche.QDistanza.fieldbyname('comune').asstring; // + '+' + trim(DataRicerche.QDistanza.fieldbyname('provincia').asstring);
     IndirizzoCliente := StringReplace(IndirizzoCliente, '''', '\''', [rfReplaceAll]);


     for i := 0 to dxDBGrid1.SelectedCount - 1 do begin
          QRes1.Bookmark := dxDBGrid1.SelectedRows[i];


          case TipoIndirizzoGoogleMaps of
               1: begin //domicilio
                         IndirizzoAna := '';
                         if QRes1DomicilioComune.AsString <> '' then begin
                              IndirizzoAna := QRes1DomicilioComune.AsString;
                              if (QRes1domiciliotipostrada.AsString <> '') and
                                   (QRes1domicilioindirizzo.AsString <> '') then

                                   IndirizzoAna := IndirizzoAna + '+' + trim(QRes1domiciliotipostrada.AsString) + '+' +
                                        trim(QRes1domicilioindirizzo.AsString);
                         end;
                    end;
               2: begin //residenza
                         IndirizzoAna := '';
                         if QRes1Comune.AsString <> '' then begin
                              IndirizzoAna := QRes1Comune.AsString;
                              if (qres1tipostrada.AsString <> '') and
                                   (QRes1indirizzo.AsString <> '') then

                                   IndirizzoAna := IndirizzoAna + '+' + trim(qres1tipostrada.AsString) + '+' +
                                        trim(QRes1indirizzo.AsString);
                         end;
                    end;
          end;

          IndirizzoAna := StringReplace(IndirizzoAna, '''', '\''', [rfReplaceAll]);


          IndirizzoCliente := TogliWhiteSpace(IndirizzoCliente);
          IndirizzoAna := TogliWhiteSpace(IndirizzoAna);

          if data.Global.fieldbyname('debughrms').asboolean = true then
               showmessage('indirizzoana= ' + IndirizzoAna + chr(13) + 'indirizzocliente= ' + IndirizzoCliente);

          try

               if (IndirizzoCliente <> '') and (IndirizzoAna <> '') then begin



                    Data.qtemp2.close;
                    Data.qtemp2.SQL.text := 'select IDKey from FormWeb_social where social = ''Google Distance'' ';
                    Data.qtemp2.Open;
                    if Data.Qtemp2.RecordCount > 0 then begin
                         CalcolaDistanceDurata(IndirizzoCliente, IndirizzoAna, 'Driving', km, tempo, xStartLat, xStartLng, xEndLat, xEndLng, xstatus);

                         if data.Global.fieldbyname('debughrms').asboolean = true then
                              showmessage('km=' + km + chr(13) + 'tempo=' + tempo);

                         try
                              DataRicerche.QInsertDist.Close;
                              DataRicerche.QInsertDist.Parameters.ParamByName('xIdRicerca').value := xIDRicerca;
                              DataRicerche.QInsertDist.Parameters.ParamByName('xIdAnagrafica').value := SelPersNewForm.QRes1ID.Value;
                              DataRicerche.QInsertDist.Parameters.ParamByName('xDistanza').value := km; //FormatFloat('0.00', km1);
                              DataRicerche.QInsertDist.Parameters.ParamByName('xTempo').value := tempo; //FormatFloat('0.00', tempo1);
                              DataRicerche.QInsertDist.Parameters.ParamByName('xLatitudineCand').value := xStartLat;
                              DataRicerche.QInsertDist.Parameters.ParamByName('xLongitudineCand').value := xStartLng;
                              DataRicerche.QInsertDist.Parameters.ParamByName('xLatitudineCliente').value := xEndLat;
                              DataRicerche.QInsertDist.Parameters.ParamByName('xLongitudineCliente').value := xEndLng;
                              DataRicerche.QInsertDist.ExecSQL;
                         except
                              on E: Exception do ShowMessage(E.Message);
                         end;
                    end; //mm
               end;
          except
               on E: Exception do ShowMessage(E.Message);
          end;

          //ShowMessage(xidcliente);

     end;
     //   Memo1.Lines.SaveToFile('LogGoogleDistanza.txt');
     idtemp := QRes1id.AsInteger;
     QRes1.close;
     qres1.Open;
     QRes1.Locate('id', idtemp, []);
     QRes1.FieldByName('ID').FocusControl;

end;

procedure TSelPersNewForm.Esportadaticandidati1Click(Sender: TObject);
var xidanag, xPassword: string;
     i: integer;
begin
     if QRes1.EOF then exit;
     if MainForm.VerificaFunzLicenze('108') = true then begin
          MessageDlg('FUNZIONE O MODULO ' + GetFunctionName('108') + ' NON COMPRESA NELLA LICENZA' + #13 + 'Consulta l''About per maggiori informazioni', mtError, [mbOK], 0);
          Exit;
     end;
     xidanag := '';
     if dxDBGrid1.SelectedCount <= 0 then begin
          MessageDlg('Nessun Candidato Selezionato', mtInformation, [mbOK], 0);
          exit;
     end;
     for i := 0 to dxDBGrid1.SelectedCount - 1 do begin
          QRes1.Bookmark := dxDBGrid1.SelectedRows[i];
          xidanag := xidanag + QRes1.FieldByName('ID').AsString + ',';
     end;
     xidanag[Length(xidanag)] := ' ';
     if not MainForm.CheckProfile('90') then Exit;
     InputPasswordForm.EPassword.text := 'EPassword';
     InputPasswordForm.caption := 'Inserimento password di Esportazione';
     InputPasswordForm.ShowModal;
     if InputPasswordForm.ModalResult = mrOK then begin
          xPassword := InputPasswordForm.EPassword.Text;
     end else exit;
     if not CheckPassword(xPassword, LeggiPswProcedure(10)) then begin
          MessageDlg('Password errata', mtError, [mbOK], 0);
          exit;
     end;
     if xidanag <> '' then
          MainForm.CallMeExport(xidanag, data.Global.Fieldbyname('nomeazienda').AsString, ';', 4, 4, 4, 4, handle, 'ricerca');
end;

procedure TSelPersNewForm.SpeedButton1Click(Sender: TObject);
var xFile: string;
     xInizio, xFine: TDateTime;
     xHour, xMin, xSec, xMSec: Word;
begin
     QRes_test.close;
     xFile := ExtractFileDir(Application.ExeName) + '\QRes_test.sql';
     if FileExists(xFile) then begin
          QRes_test.SQL.clear;
          QRes_test.SQL.LoadFromFile(xFile);
     end;

     xInizio := Now;
     xTipoQuery := 0;
     EseguiQueryRic2;
     dsQRes1.DataSet := QRes_test;
     dxDBGrid1.visible := True;
     xFine := Now;

     DecodeTime(xFine - xInizio, xHour, xMin, xSec, xMSec);

     ShowMessage('Tempo impiegato: Sec.= ' + IntToStr(xSec) + ' / Millisec.= ' + IntToStr(xMSec));
end;

procedure TSelPersNewForm.EseguiQueryRic2;
var xTab, xArrayStati: array[1..5] of string;
     i, j, k, xIDEvento: integer;
     xS, xS1, xd, xLineeStringa, xStringaSQL, xTempQueryDistanza, xTempQueryCount, xStati, xNotStati: string;
     xApertaPar, xEspLav, xIRNote, xIRFileSystem: boolean;
     xQueryCount, xQueryDistanza: TADOQuery;
     xTimeStamp: TStringList;
     xPartenza, xDestinazione, xMezzo, xDistance,
          xDuration, xStartLatitudine, xStartLongitudine,
          xEndLatitudine, xEndLongitudine, xstatus, xoperatoreprecedente: string;
     xDistanceNum: integer;
     xElencoPartenze: AnsiString;
     xProg: integer;
begin

     xTimeStamp := TStringList.create;
     xTimeStamp.Add('INIZIO: ' + FormatDateTime('hh:nn:ss:zzz', Now));

     // prosegui timing
     if xChiamante = 1 then SelPersForm.RiprendiAttivita;

     //tronco la tabella prima di rifare i calcoli
     {if (xTipoQuery = 2) then begin
          data.Q1.Close;
          data.Q1.SQL.Text := 'truncate table googledistanzaric';
          data.Q1.ExecSQL;
     end;}

     if data.Global.FieldByName('debughrms').asboolean = true then showmessage('EseguiQueryRic2');
     if xChiamante = 1 then begin
          if DsQRicLineeQuery.State in [dsInsert, dsEdit] then begin
               QRicLineeQuery.Post;
               QRicLineeQuery.Close;
               QRicLineeQuery.Open;
          end;
          QRicLineeQuery.Last;
          if QRicLineeQuery.FieldByName('OpSucc').AsString <> '' then begin
               QRicLineeQuery.Edit;
               QRicLineeQuery.FieldByName('Next').AsString := 'fine';
               QRicLineeQuery.Post;
          end;
          QRicLineeQuery.First;
          while not QRicLineeQuery.EOF do begin
               QRicLineeQuery.Edit;
               QRicLineeQuery.FieldByName('NoteFullText').AsString := EIRNote.Text;
               QRicLineeQuery.Post;
               QRicLineeQuery.Next;
          end;
          QRicLineeQuery.Close;
          //QRicLineeQuery.SQL.text := 'select * from RicLineeQuery where IDRicerca=:xIDRic: and Valore is not null and Valore<>''''';
          QRicLineeQuery.ParamByName['xIDRic'] := DataRicerche.TRicerchePend.FieldByName('ID').asString;
          QRicLineeQuery.Open;
     end else begin
          if DsLinee.State in [dsInsert, dsEdit] then begin
               TLinee.Post;
               TLinee.Close;
               TLinee.Open;
          end;
          TLinee.Last;
          if TLinee.FieldByName('OpSucc').AsString <> '' then begin
               TLinee.Edit;
               TLinee.FieldByName('OpNext').AsString := 'fine';
               TLinee.Post;
          end;
          TLinee.First;
          while not TLinee.EOF do begin
               TLinee.Edit;
               TLinee.FieldByName('NoteFullText').AsString := EIRNote.Text;
               TLinee.Post;
               TLinee.Next;
          end;
          TLinee.Close;
          //TLinee.SQL.text := 'select * from UserLineeQuery where IDAnagrafica=:xIDAnag: and Valore is not null and Valore<>''''';
          TLinee.ParamByName['xIDAnag'] := MainForm.xIDUtenteAttuale;
          TLinee.Open;
     end;
     // filtro solo dove ci sono valori

     // tabelle implicate tranne Anagrafica
     QTabelleLista.Close;
     QTabelleLista.SQL.clear;
     if xChiamante = 1 then begin
          QTabelleLista.SQL.add('select distinct Tabella from RicLineeQuery ');
          QTabelleLista.SQL.add('where Tabella<>''Anagrafica'' and Tabella<>''EBC_CandRic''');
          QTabelleLista.SQL.add(' and IDRicerca=' + DataRicerche.TRicerchePend.FieldByName('ID').asString);
     end else begin
          QTabelleLista.SQL.add('select distinct Tabella from UserLineeQuery ');
          QTabelleLista.SQL.add('where Tabella<>''Anagrafica'' and Tabella<>''EBC_CandRic''');
          QTabelleLista.SQL.add(' and IDAnagrafica=' + IntToStr(MAinForm.xIDUtenteAttuale));
     end;
     QTabelleLista.Open;
     QTabelleLista.First;
     for i := 1 to 5 do xTab[i] := '';
     i := 1;
     while not QTabelleLista.EOF do begin
          xTab[i] := TrimRight(QTabelleLista.FieldByName('Tabella').AsString);
          QTabelleLista.Next; inc(i);
     end;

     xTimeStamp.Add('Inizio assemblaggio QRes1: ' + FormatDateTime('hh:nn:ss:zzz', Now));

     xEspLav := False;

     //k := 1;
     //while k <= 5 do begin
     //     if (xTab[k] <> '') and (UpperCase(xTab[k]) <> 'ANAGALTREINFO') and (UpperCase(xTab[k]) <> 'ANAGCAMPIPERS') then
     //          xS := xs + ',' + xTab[k];
     //     inc(k);
     //end;

     QRes1.Close;
     QRes1.SQL.Clear;
     xTempQueryCount := '';

     QRes1.SQL.Add('select distinct Anagrafica.ID,Anagrafica.dacontattocliente,Anagrafica.Cognome,Anagrafica.Nome,Anagrafica.CVNumero,Anagrafica.OldCVNumero,');
     QRes1.SQL.Add('                Anagrafica.TelUfficio,Anagrafica.Cellulare,CVInseritoInData,CVDataReale,CVDataAgg,RecapitiTelefonici,');
     QRes1.SQL.Add('                Anagrafica.DataNascita,Anagrafica.IDStato,Anagrafica.IDTipoStato, EBC_stati.Stato StatoAna, ');
     QRes1.SQL.Add('                Anagrafica.Provincia, Anagrafica.Comune,anagrafica.indirizzo,anagrafica.cap,anagrafica.numcivico,anagrafica.tipostrada,');
     QRes1.SQL.Add('                anagrafica.domiciliotipostrada,anagrafica.domiciliocap,anagrafica.domicilionumcivico,anagrafica.domicilioindirizzo,anagrafica.domiciliocomune,anagrafica.domicilioprovincia, ');
     QRes1.SQL.Add('                anagrafica.domicilioRegione,anagrafica.Regione,anagrafica.domicilioStato,anagrafica.Stato,');
     QRes1.SQL.Add('                DATEDIFF(day, EBC_CandidatiRicerche.DataIns, getdate()) ggUltimaDataIns,');
     QRes1.SQL.Add('                DATEDIFF(day, EBC_ContattiCandidati.Data, getdate()) ggDataUltimoContatto,');
     QRes1.SQL.Add('                EBC_CandidatiRicerche.DataImpegno DataProssimoColloquio,');
     QRes1.SQL.Add('                DATEDIFF(day, EBC_Colloqui.Data, getdate()) ggDataUltimoColloquio,');
     QRes1.SQL.Add('                DATEDIFF(day, Anagrafica.CVInseritoInData, getdate()) ggDataInsCV, NoCv,AnagAltreInfo.Retribuzione,EMail, AnagAltreInfo.Inquadramento, cast (anagfileD.IDAnagfile/anagfileD.IDAnagfile as bit) as FileCheck, ');
    // QRes1.SQL.Add('                cast(googledistanzaric.distanzanum as varchar) distanza,googledistanzaric.tempo,emailufficio, ');     //distanzaricmod
     QRes1.SQL.Add('                 googledistanzaric.distanza,googledistanzaric.tempo,emailufficio, ');
     QRes1.SQL.Add('                EspLavAttuale.Azienda, EspLavAttuale.titolomansione CodPers, EspLavAttuale.Ruolo, EspLavAttuale.descrizionemansione, ');
     QRes1.SQL.Add('                EspLavAttuale.JobTitle, EspLavAttuale.Area, EspLavAttuale.Settore,EspLavAttuale.AreaSettore, EBC_TipiStato.TipoStato, ');
     //QRes1.SQL.Add('                LogAnag_I.DataIns, LogAnag_U.LastAgg DataAgg, PrimoRuolo.Mansione PrimoRuolo');
     QRes1.SQL.Add('                Anagrafica.CVInseritoInData DataIns, Anagrafica.CVDataAgg DataAgg, PrimoRuolo.Mansione PrimoRuolo ');
     QRes1.SQL.Add('                ,cast((select (case when count(*) > 0 then 0 else 1 end) from Ann_AnagAnnEdizData where Ann_AnagAnnEdizData.IDAnagrafica=Anagrafica.id) as bit) as Autocandidatura ');
     //,cast((select (case when count(*) > 0 then 0 else 1 end) from Ann_AnagAnnEdizData where Ann_AnagAnnEdizData.IDAnagrafica=Anagrafica.id Group By IDAnagrafica*) as bit) as Autocandidatura ');

//Ultimo Annuncio richiesto da Emergency
     QRes1.SQL.Add('                ,(select top 1 rif from Ann_AnagAnnEdizData ' +
          'join Ann_AnnEdizData on Ann_AnagAnnEdizData.IDAnnEdizData=Ann_AnnEdizData.id ' +
          'join Ann_Annunci on Ann_AnnEdizData.IDAnnuncio = Ann_Annunci.id ' +
          'where Ann_AnagAnnEdizData.IDAnagrafica=Anagrafica.id ' +
          'order by Ann_AnagAnnEdizData.ID) as UltimoAnnuncio ');


     QRes1.SQL.Add(' from Anagrafica ');
     QRes1.SQL.Add('join AnagAltreInfo on Anagrafica.ID=AnagAltreInfo.IDAnagrafica ');
     QRes1.SQL.Add('join EBC_Stati on EBC_Stati.ID=Anagrafica.IDStato ');
     QRes1.SQL.Add('join EBC_TipiStato on EBC_TipiStato.ID=Anagrafica.IDTipostato ');
     QRes1.SQL.Add('left outer join AnagCampiPers on AnagcampiPers.IDAnagrafica = Anagrafica.ID');
     QRes1.SQL.Add('left outer join (select distinct idanagrafica as IDAnagFile from anagfile) as AnagFileD on Anagrafica.ID = anagfileD.IDAnagfile ');
     QRes1.SQL.Add('left join googledistanzaric on Anagrafica.id = googledistanzaric.idanagrafica and googledistanzaric.idutente=:xIDUtente:');
     //aggiunta per il calcolo distanza google
     (*if xChiamante = 1 then
          QRes1.SQL.Add(' and googledistanzaric.idricerca=' + IntToStr(xIDRicerca)); *)

     QRes1.SQL.Add('-- ultimo inserimento in una (eventuale) ricerca ');
     QRes1.SQL.Add('left outer join EBC_CandidatiRicerche on Anagrafica.ID = EBC_CandidatiRicerche.IDAnagrafica ');
     QRes1.SQL.Add('     and EBC_CandidatiRicerche.DataIns = ');
     QRes1.SQL.Add('     (select max(DataIns) from EBC_CandidatiRicerche ');
     QRes1.SQL.Add('      where Anagrafica.ID = EBC_CandidatiRicerche.IDAnagrafica) ');
     QRes1.SQL.Add('     and EBC_CandidatiRicerche.DataImpegno = ');
     QRes1.SQL.Add('     (select min(DataImpegno) from EBC_CandidatiRicerche ');
     QRes1.SQL.Add('      where Anagrafica.ID = EBC_CandidatiRicerche.IDAnagrafica) ');
     QRes1.SQL.Add('-- ultimo contatto ');
     QRes1.SQL.Add('left outer join EBC_ContattiCandidati on Anagrafica.ID = EBC_ContattiCandidati.IDAnagrafica ');
     QRes1.SQL.Add('    and EBC_ContattiCandidati.Data= ');
     QRes1.SQL.Add('    (select max(Data) from EBC_ContattiCandidati ');
     QRes1.SQL.Add('     where Anagrafica.ID = EBC_ContattiCandidati.IDAnagrafica) ');
     QRes1.SQL.Add('-- ultimo colloquio ');
     QRes1.SQL.Add('left outer join EBC_Colloqui on Anagrafica.ID = EBC_Colloqui.IDAnagrafica ');
     QRes1.SQL.Add('	and EBC_Colloqui.Data= ');
     QRes1.SQL.Add('    (select max(Data) from EBC_Colloqui ');
     QRes1.SQL.Add('     where Anagrafica.ID = EBC_Colloqui.IDAnagrafica) ');
     QRes1.SQL.Add('-- esperienza lavorativa attuale (ultima, se pi� di una) ');
     QRes1.SQL.Add('left outer join ( ');
     QRes1.SQL.Add('	select IDAnagrafica,max(ID) MaxID ');
     QRes1.SQL.Add('	from EsperienzeLavorative ');
     QRes1.SQL.Add('	where Attuale=1 ');
     QRes1.SQL.Add('	group by IDAnagrafica ');
     QRes1.SQL.Add('	) EspLav_MaxID on Anagrafica.ID = EspLav_MaxID.IDAnagrafica ');
     QRes1.SQL.Add('left outer join ( ');
     QRes1.SQL.Add('	select Esperienzelavorative.ID, Esperienzelavorative.IDAnagrafica, ');
     QRes1.SQL.Add('		   Azienda= case ');
     QRes1.SQL.Add('			when Esperienzelavorative.IDAzienda is null then Esperienzelavorative.AziendaNome ');
     QRes1.SQL.Add('			else ');
     QRes1.SQL.Add('			EBC_Clienti.Descrizione ');
     QRes1.SQL.Add('			end, ');
     QRes1.SQL.Add('	 titolomansione, ');
     QRes1.SQL.Add('	 Mansioni.Descrizione Ruolo, cast(descrizionemansione as varchar(255)) descrizionemansione, ');
     QRes1.SQL.Add('	 ruolopresentaz as JobTitle, ');
     QRes1.SQL.Add('	 Aree.Descrizione Area, ebc_attivita.attivita Settore,AreeSettori.AreaSettore ');
     QRes1.SQL.Add('	from Esperienzelavorative ');
     QRes1.SQL.Add('	left join EBC_Clienti ');
     QRes1.SQL.Add('	on Esperienzelavorative.IDAzienda = EBC_Clienti.ID ');
     QRes1.SQL.Add('	left join Mansioni ');
     QRes1.SQL.Add('	on Esperienzelavorative.IDMansione = Mansioni.ID ');
     QRes1.SQL.Add('	join aree ');
     QRes1.SQL.Add('	on Mansioni.IDArea=aree.id ');
     QRes1.SQL.Add('	left join ebc_attivita ');
     QRes1.SQL.Add('	on ebc_attivita.id=esperienzelavorative.idsettore ');
     QRes1.SQL.Add('	left join AreeSettori ');
     QRes1.SQL.Add('	on AreeSettori.id=ebc_attivita.idareasettore ');
     QRes1.SQL.Add('  ) EspLavAttuale on EspLav_MaxID.MaxID = EspLavAttuale.ID ');
     QRes1.SQL.Add('-- primo ruolo ');
     QRes1.SQL.Add('left outer join ( ');
     QRes1.SQL.Add('	select IDAnagrafica,max(ID) MaxID ');
     QRes1.SQL.Add('	from anagmansioni ');
     QRes1.SQL.Add('	group by idanagrafica ');
     QRes1.SQL.Add('  ) Ruoli_MaxID on Anagrafica.ID = Ruoli_MaxID.IDAnagrafica ');
     QRes1.SQL.Add('left outer join ( ');
     QRes1.SQL.Add('	select am.id,m.descrizione mansione,a.descrizione area ');
     QRes1.SQL.Add('	from anagmansioni am ');
     QRes1.SQL.Add('	join mansioni m on am.idmansione=m.id ');
     QRes1.SQL.Add('	join aree a on a.id=m.idarea ');
     QRes1.SQL.Add('  ) PrimoRuolo on Ruoli_MaxID.MaxID = PrimoRuolo.ID ');
     QRes1.SQL.Add('-- Data inserimento ');
     (* QRes1.SQL.Add('left outer join ( ');
      QRes1.SQL.Add('	select KeyValue, max(DataOra) DataIns ');
      QRes1.SQL.Add('	from Log_TableOp ');
      QRes1.SQL.Add('	where Tabella=''Anagrafica'' ');
      QRes1.SQL.Add('	and Operation=''I'' ');
      QRes1.SQL.Add('	group by KeyValue ');
      QRes1.SQL.Add(') LogAnag_I on Anagrafica.id = LogAnag_I.KeyValue ');
      QRes1.SQL.Add('-- Data ultimo aggiornamento ');
      QRes1.SQL.Add('left outer join ( ');
      QRes1.SQL.Add('	select KeyValue, max(DataOra) LastAgg ');
      QRes1.SQL.Add('	from Log_TableOp ');
      QRes1.SQL.Add('	where Tabella=''Anagrafica'' ');
      QRes1.SQL.Add('	and Operation=''u'' ');
      QRes1.SQL.Add('	group by KeyValue ');
      QRes1.SQL.Add(') LogAnag_U on Anagrafica.id = LogAnag_U.KeyValue ');   *)

     if (EIRNote.Text <> '') and (Data.Global.FieldByName('AnagFileDentroDB').AsBoolean) then
          QRes1.SQL.Add(' left outer join AnagFile on anagrafica.id = anagfile.idanagrafica');

     // altre join
     k := 1;
     while k <= 5 do begin
          if (xTab[k] <> '') and (UpperCase(xTab[k]) <> 'ANAGALTREINFO') and (UpperCase(xTab[k]) <> 'ANAGCAMPIPERS')
               and (UpperCase(xTab[k]) <> 'LINGUECONOSCIUTE')
               and (trim(UpperCase(xTab[k])) <> 'DISTANZALUOGO')
               and (UpperCase(xTab[k]) <> 'EBC_CANDIDATIRICERCHE') then
               QRes1.SQL.Add(' left join ' + xTab[k] + ' on Anagrafica.ID = ' + xTab[k] + '.IDAnagrafica ');
          inc(k);
     end;

     // FINE JOIN
     QRes1.SQL.Add('where Anagrafica.ID is not null ');

//TEST MB 20191121
     if CBSenzaTitoloStudio.Checked then begin
          QRes1.SQL.Add('and  Anagrafica.ID not in (select IDAnagrafica from TitoliStudio where  IDAnagrafica is not null)');
     end;
//TEST MB 20191121


     // anzianit� CV
     if CBAnzCV.Checked then begin
          if cbanzianita.Text = 'Minore di mesi' then
               QRes1.SQL.Add('and (anagrafica.id in (select id from anagrafica where CVInseritoInData>=:xCVInseritoInData:) ' +
                    'or ' +
                    'anagrafica.id in (select id from anagrafica where CVdataagg>=:xCVInseritoInData:))')
          else
               QRes1.SQL.Add('and (anagrafica.id in (select id from anagrafica where CVInseritoInData<=:xCVInseritoInData:) ' +
                    'or ' +
                    'anagrafica.id in (select id from anagrafica where CVdataagg<=:xCVInseritoInData:))');
     end;
     // togliere gli esclusi (cio� gi� presenti in ricerca)
     if xChiamante = 1 then begin
          for j := 1 to DataRicerche.QCandRic.RecordCount do
               QRes1.SQL.Add('and Anagrafica.ID<>' + IntToStr(xDaEscludere[j]));
     end;

     //xS1 := '';
     //for i := 1 to 5 do begin
     //     if xTab[i] <> '' then
     //          if xTab[i] = '' then
     //               xS1 := xS1 + ' and Anagrafica.ID=' + xTab[i] + '.IDAnagrafica and CampiPers.ID = AnagCampiPers.IDCampo'
     //          else xS1 := xS1 + ' and Anagrafica.ID=' + xTab[i] + '.IDAnagrafica';
     //end;
     //QRes1.SQL.Add(xS1);

     if xChiamante = 1 then begin
          QRicLineeQuery.First;

          if (xTipoQuery = 0) then begin
               if QRicLineeQuery.RecordCount > 0 then
                    QRes1.SQL.Add('and')
          end else begin
               if (Trim(QRicLineeQuery.FieldByName('Tabella').asString) = 'DistanzaLuogo') then begin
                    QRicLineeQuery.Next;
                    if not QRicLineeQuery.Eof then
                         QRes1.SQL.Add(' and ');
               end else begin
                    if not QRicLineeQuery.Eof then
                         QRes1.SQL.Add(' and ');
               end;
          end;

          xLineeStringa := '';
          xApertaPar := False;
          while not QRicLineeQuery.EOF do begin

               if Trim(QRicLineeQuery.FieldByName('Tabella').asString) <> 'DistanzaLuogo' then
                    xLineeStringa := TrimRight(QRicLineeQuery.FieldByName('Stringa').asString);


               // per le LINGUE --> apposita clausola
               if Trim(QRicLineeQuery.FieldByName('Tabella').AsString) = 'LingueConosciute' then begin
                    xLineeStringa := 'Anagrafica.ID in (select IDAnagrafica from LingueConosciute where ' + QRicLineeQuery.FieldByName('Stringa').Value;
                    if QRicLineeQuery.FieldByName('LivLinguaParlato').asString <> '' then
                         xLineeStringa := xLineeStringa + ' and livello>=' + TrimRight(QRicLineeQuery.FieldByName('LivLinguaParlato').asString);
                    if QRicLineeQuery.FieldByName('LivLinguaScritto').asString <> '' then
                         xLineeStringa := xLineeStringa + ' and livelloScritto>=' + TrimRight(QRicLineeQuery.FieldByName('LivLinguaScritto').asString);
                    xLineeStringa := TrimRight(xLineeStringa) + ')';
               end;
               // per gli eventi occorsi --> apposita clausola
               if copy(QRicLineeQuery.FieldByName('Stringa').asString, 1, 16) = 'Storico.IDEvento' then begin
                    if pos('=', xLineeStringa) > 0 then begin
                         // evento occorso
                         xIDEvento := StrToIntDef(copy(xLineeStringa, pos('=', xLineeStringa) + 2, 4), 0);
                         xLineeStringa := IntToStr(xIDEvento) + ' in (select distinct IDEvento from storico st where anagrafica.id=st.idanagrafica ';
                    end else begin
                         // evento non occorso
                         xIDEvento := StrToIntDef(copy(xLineeStringa, pos('>', xLineeStringa) + 2, 4), 0);
                         xLineeStringa := IntToStr(xIDEvento) + ' not in (select distinct IDEvento from storico st where anagrafica.id=st.idanagrafica ';
                    end;
                    xLineeStringa := xLineeStringa + ')';
               end;
               if copy(QRicLineeQuery.FieldByName('Stringa').asString, 1, 16) = 'Storico.IDUtente' then begin
                    if pos('=', xLineeStringa) > 0 then begin
                         // evento occorso
                         xIDEvento := StrToIntDef(copy(xLineeStringa, pos('=', xLineeStringa) + 2, 4), 0);
                         xLineeStringa := IntToStr(xIDEvento) + ' in (select distinct IDUtente from storico st where anagrafica.id=st.idanagrafica ';
                    end else begin
                         // evento non occorso
                         xIDEvento := StrToIntDef(copy(xLineeStringa, pos('>', xLineeStringa) + 2, 4), 0);
                         xLineeStringa := IntToStr(xIDEvento) + ' not in (select distinct IDUtente from storico st where anagrafica.id=st.idanagrafica ';
                    end;
                    xLineeStringa := xLineeStringa + ')';
               end;


               if (xTipoQuery = 0) then begin
                    if Trim(QRicLineeQuery.FieldByName('Tabella').asString) = 'DistanzaLuogo' then begin
                         xLineeStringa := ' Anagrafica.ID in (select Distinct GDR.idanagrafica from GoogleDistanzaRic GDR where ' +
                              '  anagrafica.id=GDR.idanagrafica and IDUtente = ' + IntToStr(MAinForm.xIDUtenteAttuale) + 'and idricerca = ' + IntToStr(xIDRicerca) + ' and DistanzaNum<''' + inttostr(QRicLineeQuery.FieldByName('DistanzaComuneKm').asInteger * 1000) + ''' )';
                    end;
               end;



               QRicLineeQuery.Next;

               if (Trim(QRicLineeQuery.FieldByName('Tabella').asString) = 'DistanzaLuogo') and
                    (xTipoQuery <> 0) then begin
                    QRicLineeQuery.Next;
               end;

               if QRicLineeQuery.Eof then
                    // ultimo record -> senza AND alla fine
                    if xApertaPar then QRes1.SQL.Add(' ' + TrimRight(xLineeStringa) + ')')
                    else QRes1.SQL.Add(' ' + xLineeStringa)
               else begin
                    QRicLineeQuery.Prior;

                    if (Trim(QRicLineeQuery.FieldByName('Tabella').asString) = 'DistanzaLuogo') and
                         (xTipoQuery <> 0) then begin
                         QRicLineeQuery.Prior;
                    end;

                    if TrimRight(QRicLineeQuery.FieldByName('OpSucc').AsString) = 'and' then
                         if not xApertaPar then
                              QRes1.SQL.Add(' ' + xLineeStringa + ' and ')
                         else begin
                              QRes1.SQL.Add(' ' + xLineeStringa + ') and ');
                              xApertaPar := False;
                         end;
                    if TrimRight(QRicLineeQuery.FieldByName('OpSucc').AsString) = 'or' then
                         if not xApertaPar then begin
                              QRes1.SQL.Add(' (' + xLineeStringa + ' or ');
                              xApertaPar := True;
                         end else begin
                              QRes1.SQL.Add(' ' + xLineeStringa + ' or ');
                              xApertaPar := True;
                         end;
               end;
               QRicLineeQuery.Next;

               if (Trim(QRicLineeQuery.FieldByName('Tabella').asString) = 'DistanzaLuogo') and
                    (xTipoQuery <> 0) then begin
                    QRicLineeQuery.Next;
               end;
          end;
     end else begin

          TLinee.First;

          if (xTipoQuery = 0) then begin
               if TLinee.RecordCount > 0 then
                    QRes1.SQL.Add('and')
          end else begin
               if (Trim(TLinee.FieldByName('Tabella').asString) = 'DistanzaLuogo') then begin
                    TLinee.Next;
                    if not TLinee.Eof then
                         QRes1.SQL.Add(' and ');
               end else begin
                    if not TLinee.Eof then
                         QRes1.SQL.Add(' and ');
               end;
          end;

          xLineeStringa := '';
          xApertaPar := False;



          while not TLinee.EOF do begin


               if Trim(TLinee.FieldByName('Tabella').asString) <> 'DistanzaLuogo' then
                    xLineeStringa := TrimRight(TLinee.FieldByName('Stringa').asString);

               // per le LINGUE --> apposita clausola
               if Trim(TLinee.FieldByName('Tabella').AsString) = 'LingueConosciute' then begin
                    xLineeStringa := 'Anagrafica.ID in (select IDAnagrafica from LingueConosciute where ' + TLineeStringa.Value;
                    if TLineeLivLinguaParlato.asString <> '' then
                         xLineeStringa := xLineeStringa + ' and livello>=' + TrimRight(TLineeLivLinguaParlato.asString);
                    if TLineeLivLinguaScritto.asString <> '' then
                         xLineeStringa := xLineeStringa + ' and livelloScritto>=' + TrimRight(TLineeLivLinguaScritto.asString);
                    xLineeStringa := TrimRight(xLineeStringa) + ')';
               end;
               // per gli eventi occorsi --> apposita clausola
               {if copy(TLinee.FieldByName('Stringa').asString, 1, 16) = 'Storico.IDEvento' then begin
                    if pos('=', xLineeStringa) > 0 then begin
                         // evento occorso
                         xIDEvento := StrToIntDef(copy(xLineeStringa, pos('=', xLineeStringa) + 2, 4), 0);
                         xLineeStringa := IntToStr(xIDEvento) + ' in (select distinct IDEvento from storico st where anagrafica.id=st.idanagrafica ';
                    end else begin
                         // evento non occorso
                         xIDEvento := StrToIntDef(copy(xLineeStringa, pos('>', xLineeStringa) + 2, 4), 0);
                         xLineeStringa := IntToStr(xIDEvento) + ' not in (select distinct IDEvento from storico st where anagrafica.id=st.idanagrafica ';
                    end;
                    xLineeStringa := xLineeStringa + ')';
               end;
               if copy(TLinee.FieldByName('Stringa').asString, 1, 16) = 'Storico.IDUtente' then begin
                    if pos('=', xLineeStringa) > 0 then begin
                         // evento occorso
                         xIDEvento := StrToIntDef(copy(xLineeStringa, pos('=', xLineeStringa) + 2, 4), 0);
                         xLineeStringa := IntToStr(xIDEvento) + ' in (select distinct IDUtente from storico st where anagrafica.id=st.idanagrafica ';
                    end else begin
                         // evento non occorso
                         xIDEvento := StrToIntDef(copy(xLineeStringa, pos('>', xLineeStringa) + 2, 4), 0);
                         xLineeStringa := IntToStr(xIDEvento) + ' not in (select distinct IDUtente from storico st where anagrafica.id=st.idanagrafica ';
                    end;
                    xLineeStringa := xLineeStringa + ')';
               end;}

               if (xTipoQuery = 0) then begin
                    if Trim(TLinee.FieldByName('Tabella').asString) = 'DistanzaLuogo' then begin
                         xLineeStringa := ' Anagrafica.ID in (select Distinct GDR.idanagrafica from GoogleDistanzaRic GDR where ' +
                              '  anagrafica.id=GDR.idanagrafica and IDUtente = ' + IntToStr(MAinForm.xIDUtenteAttuale) + 'and idricerca is null and DistanzaNum<''' + inttostr(TLinee.FieldByName('DistanzaComuneKm').asinteger * 1000) + ''')';
                    end;
               end;

               if Pos('S.C.R.', UpperCase(Data.Global.FieldByName('NomeAzienda').asString)) > 0 then begin


                    TLinee.Next;

                    if (Trim(TLinee.FieldByName('Tabella').asString) = 'DistanzaLuogo') and
                         (xTipoQuery <> 0) then begin
                         TLinee.Next;
                    end;

                    if TLinee.Eof then begin
                         if pos('*', xoperatoreprecedente) > 0 then
                              QRes1.SQL.Add(' (' + xLineeStringa + '))')
                         else
                              QRes1.SQL.Add(' (' + xLineeStringa + ')')
                    end else begin
                 //   if not TLinee.bof then
                         TLinee.Prior;

                         if (Trim(TLinee.FieldByName('Tabella').asString) = 'DistanzaLuogo') and
                              (xTipoQuery <> 0) then begin
                              TLinee.Prior;
                         end;

                         if TrimRight(TLinee.FieldByName('OpSucc').AsString) = 'and' then begin
                              if pos('*', xoperatoreprecedente) > 0 then
                                   QRes1.SQL.Add(' (' + xLineeStringa + ')) and ')
                              else
                                   QRes1.SQL.Add(' (' + xLineeStringa + ') and ');
                         end;

                         if TrimRight(TLinee.FieldByName('OpSucc').AsString) = 'and*' then begin
                              if pos('*', xoperatoreprecedente) > 0 then
                                   QRes1.SQL.Add('(' + xLineeStringa + ') and ')
                              else
                                   QRes1.SQL.Add('((' + xLineeStringa + ') and ')
                         end;

                         if TrimRight(TLinee.FieldByName('OpSucc').AsString) = 'or' then begin
                              if pos('*', xoperatoreprecedente) > 0 then
                                   QRes1.SQL.Add(' (' + xLineeStringa + ')) or ')
                              else
                                   QRes1.SQL.Add(' (' + xLineeStringa + ') or ');
                         end;

                         if TrimRight(TLinee.FieldByName('OpSucc').AsString) = 'or*' then begin
                              if pos('*', xoperatoreprecedente) > 0 then
                                   QRes1.SQL.Add('(' + xLineeStringa + ') or ')
                              else
                                   QRes1.SQL.Add('((' + xLineeStringa + ') or ')
                         end;

                         if TrimRight(TLinee.FieldByName('OpSucc').AsString) = '' then begin
                              if pos('*', xoperatoreprecedente) > 0 then
                                   QRes1.SQL.Add(' ' + xLineeStringa + ')  and ')
                              else
                                   QRes1.SQL.Add(' (' + xLineeStringa + ')  and ');
                         end;
                    end;
               end else begin


                    TLinee.Next;

                    if (Trim(TLinee.FieldByName('Tabella').asString) = 'DistanzaLuogo') and
                         (xTipoQuery <> 0) then begin
                         TLinee.Next;
                    end;

                    if TLinee.Eof then
                    // ultimo record -> senza AND alla fine
                         if xApertaPar then QRes1.SQL.Add(' ' + TrimRight(xLineeStringa) + ')')
                         else QRes1.SQL.Add(' ' + xLineeStringa)
                    else begin
                 //   if not TLinee.bof then
                         TLinee.Prior;

                         if (Trim(TLinee.FieldByName('Tabella').asString) = 'DistanzaLuogo') and
                              (xTipoQuery <> 0) then begin
                              TLinee.Prior;
                         end;

                         if TrimRight(TLinee.FieldByName('OpSucc').AsString) = 'and' then begin
                              if not xApertaPar then begin
                                   QRes1.SQL.Add('' + xLineeStringa + ' and ')
                              end else begin
                                   QRes1.SQL.Add(' ' + xLineeStringa + ') and ');
                                   xApertaPar := False;
                              end;
                         end;
                         if TrimRight(TLinee.FieldByName('OpSucc').AsString) = 'or' then
                              if not xApertaPar then begin
                                   QRes1.SQL.Add(' (' + xLineeStringa + ' or ');
                                   xApertaPar := True;
                              end else begin
                                   QRes1.SQL.Add(' ' + xLineeStringa + ' or ');
                                   xApertaPar := True;
                              end;
                    end;

               end;
               xoperatoreprecedente := TLinee.FieldByName('OpSucc').AsString;
               TLinee.Next;

               if (Trim(TLinee.FieldByName('Tabella').asString) = 'DistanzaLuogo') and
                    (xTipoQuery <> 0) then begin
                    TLinee.Next;
               end;

          end;
     end;
     // stati secondo la selezione
     if not PMStati1.Checked then
          QRes1.SQL.Add('and Anagrafica.IDTipoStato<>1');
     if not PMStati2.Checked then
          QRes1.SQL.Add('and Anagrafica.IDTipoStato<>2');
     if not PMStati3.Checked then
          QRes1.SQL.Add('and Anagrafica.IDTipoStato<>3');
     if not PMStati4.Checked then
          QRes1.SQL.Add('and Anagrafica.IDTipoStato<>5');
     if not PMStati5.Checked then
          QRes1.SQL.Add('and Anagrafica.IDTipoStato<>9');

     // Escludi dipendenti e altri stati H1HRMS
     QRes1.SQL.Add('and Anagrafica.IDTipoStato<>6');
     if not PMStati6.Checked then
          QRes1.SQL.Add('and Anagrafica.IDTipoStato<>7');
     if not PMStati7.Checked then
          QRes1.SQL.Add('and Anagrafica.IDTipoStato<>8');
     QRes1.SQL.Add('and Anagrafica.IDTipoStato<>12');

     if CBColloquiati.Checked then
          QRes1.SQL.Add('and Anagrafica.ID in (select IDAnagrafica from EBC_Colloqui)');

     if CBPropri.Checked then
          QRes1.SQL.Add('and (Anagrafica.IDProprietaCV is null or Anagrafica.IDProprietaCV=0)');



     // COMPOSIZIONE FINALE STRINGA SQL
     xStringaSQL := QRes1.SQL.Text;
     QRes1.SQL.Clear;
     QRes1.SQL.Add('SET DATEFORMAT dmy');
     QRes1.SQL.Add(xStringaSQL);

     // sapere se � abilitata sul CAMPO NOTE (AnagAltreInfo.Note)
     if data.Global.FieldByName('debughrms').asboolean = true then begin
          if xIsFulltextInstalled = true then showmessage('xIsFulltextInstalled=true') else showmessage('xIsFulltextInstalled=false');
          if xAnagAltreInfoOK = true then showmessage('xAnagAltreInfoOK=true') else showmessage('xAnagAltreInfoOK=false');
          if CBIRnote.Checked = true then showmessage('CBIRnote.Checked=true') else showmessage('CBIRnote.Checked=false');
          if xFileSystemOK = true then showmessage('xFileSystemOK=true') else showmessage('xFileSystemOK=false');
     end;

     if (xIsFulltextInstalled) and (xAnagAltreInfoOK) and (CBIRnote.Checked) then begin
          if data.Global.FieldByName('debughrms').asboolean = true then showmessage('setto xIRNote=true');
          xIRNote := True;
     end else xIRNote := False;

     // INFORMATION RETRIEVAL
     if EIRNote.text <> '' then begin

          Q.Close;
          Q.SQL.text := 'select @@version as versione';
          Q.Open;
          if not cbAdvFullText.checked then begin


               if pos('2005', UpperCase(Q.fieldbyname('versione').AsString)) > 0 then begin
                    xEIRNote := '"' + EIRNote.Text + '"';
                    if pos(' AND ', UpperCase(xEIRNote)) > 0 then xEIRNote := StringReplace(xEIrNote, ' and ', '" and "', [rfIgnoreCase, rfReplaceAll]);
                    if pos(' OR ', UpperCase(xEIrNote)) > 0 then xEIRNote := StringReplace(xEIrNote, ' or ', '" or "', [rfIgnoreCase, rfReplaceAll]);
                    if pos(' NEAR ', UpperCase(xEIrNote)) > 0 then xEIRNote := StringReplace(xEIrNote, ' near ', '" near "', [rfIgnoreCase, rfReplaceAll]);
               end else begin
                    xEIRNote := '"*' + EIRNote.Text + '*"';
                    if pos(' AND ', UpperCase(xEIRNote)) > 0 then xEIRNote := StringReplace(xEIrNote, ' and ', '*" and "*', [rfIgnoreCase, rfReplaceAll]);
                    if pos(' OR ', UpperCase(xEIrNote)) > 0 then xEIRNote := StringReplace(xEIrNote, ' or ', '*" or "*', [rfIgnoreCase, rfReplaceAll]);
                    if pos(' NEAR ', UpperCase(xEIrNote)) > 0 then xEIRNote := StringReplace(xEIrNote, ' near ', '*" near "*', [rfIgnoreCase, rfReplaceAll]);

               end;

              // TO DO: MODIFICARE PER AZURE SQL
              //
              //  if pos('AZURE', UpperCase(Q.fieldbyname('versione').AsString)) > 0 then begin
              //  //WHERE CONTAINS(description, 'FORMSOF(FREETEXT, ride)');
              //      xEIRNote := '"' + EIRNote.Text + '"';
              //      xEIRNote := '"' + 'FORMSOF(FREETEXT, ' + EIRNote.Text + ')' + '"';
              //      if pos(' AND ', UpperCase(xEIRNote)) > 0 then xEIRNote := StringReplace(xEIrNote, ' and ', '" and "', [rfIgnoreCase, rfReplaceAll]);
              //      if pos(' OR ', UpperCase(xEIrNote)) > 0 then xEIRNote := StringReplace(xEIrNote, ' or ', '" or "', [rfIgnoreCase, rfReplaceAll]);
              //      if pos(' NEAR ', UpperCase(xEIrNote)) > 0 then xEIRNote := StringReplace(xEIrNote, ' near ', '" near "', [rfIgnoreCase, rfReplaceAll]);

          //end;

          end else
               xEIRNote := EIRNote.Text;


          //FILE DENTRO DB
          if (CBIRFileSystem.Checked = true) and (CBIRFileSystem.Visible = true) then begin
               if data.Global.FieldByName('debughrms').asboolean = true then showmessage('file dentro db');
               if Data.Global.FieldByName('AnagFileDentroDB').AsBoolean then begin
                    QRes1.sql.Add(' and anagrafica.id in (select idanagrafica from  anagfile where CONTAINS(DocFile, ''' + xEIRNote + ''') )');
               end else begin
                    // SU FILE SYSTEM
                    // sotto-query file-system

                    if (xIsFulltextInstalled) and (xFileSystemOK) and (CBIRFileSystem.Checked) then begin
                         if data.Global.FieldByName('debughrms').asboolean = true then showmessage('sotto-query file-system');
                         xIRFileSystem := True;
                         Q.Close;
                         Q.SQL.text := 'select IRFileSystemScope from global';
                         Q.Open;
                         xIRFileSystemScope := Q.FieldByName('IRFileSystemScope').asString;
                         // sotto-query AnagFile
                         QRes1.SQL.Add('and (Anagrafica.ID in ( SELECT distinct IDAnagrafica FROM ' +
                              'OPENQUERY(FileSystem, ''SELECT FileName,Characterization,DocAuthor,DocComments,DocSubject,DocTitle ' +
                              ' FROM SCOPE ('''' "' + xIRFileSystemScope + '" '''') ' +
                              ' WHERE CONTAINS( Contents, ''''' + xEIrNote + ''''' ) '') AS Q, AnagFile ' +
                              ' WHERE Q.FileName = AnagFile.SoloNome COLLATE SQL_Latin1_General_CP1_CI_AS ');
                         // altre regole
                         if CBIRaltreRegole.Checked then begin
                              if data.Global.FieldByName('debughrms').asboolean = true then showmessage('altre regole');
                              Q.Close;
                              Q.SQL.text := 'select Regola from IR_LinkRules';
                              Q.Open;
                              while not Q.EOF do begin
                                   QRes1.SQL.Add('UNION');
                                   QRes1.SQL.Add('SELECT distinct Anagrafica.ID IDAnagrafica FROM ' +
                                        'OPENQUERY(FileSystem, ''SELECT FileName,Characterization,DocAuthor,DocComments,DocSubject,DocTitle ' +
                                        ' FROM SCOPE ('''' "' + xIRFileSystemScope + '" '''') ' +
                                        ' WHERE CONTAINS( Contents, ''''' + xEIrNote + ''''' ) '') AS Q, Anagrafica ' +
                                        ' WHERE ' + Q.FieldByName('Regola').asString);
                                   Q.Next;
                              end;
                         end;
                         QRes1.SQL.Add(')');
                         Q.Close;
                         QRes1.SQL.Add(')');

                    end else xIRFileSystem := False;
               end;
          end;

          // INFORMATION RETRIEVAL SUL CAMPO NOTE (AnagAltreInfo.Note)
          if xIRNote then begin
               if data.Global.FieldByName('debughrms').asboolean = true then showmessage('INFORMATION RETRIEVAL SUL CAMPO NOTE ');
               if (CBIRFileSystem.Visible = true) and (CBIRFileSystem.Checked = true) then begin
                    QRes1.SQL.Add('UNION');
                    //xTempQueryCount := QRes1.SQl.text;
                    xTempQueryCount := xTempQueryCount + 'select count (distinct anagrafica.id) ' + copy(xStringaSQL, pos('FROM Anagrafica', UpperCase(xStringaSQL)), length(xStringaSQL));
                    QRes1.SQL.add(xStringaSQL); // ancora tutta la stringa
                    QRes1.SQL.Add('and CONTAINS(AnagAltreInfo.Note, ''' + xEIrNote + ''') ');
               end else begin
                    QRes1.SQL.Add('and CONTAINS(AnagAltreInfo.Note, ''' + xEIrNote + ''') ');
                    //xTempQueryCount := QRes1.SQl.text;
                    xTempQueryCount := xTempQueryCount + 'select count (distinct anagrafica.id) ' + copy(xStringaSQL, pos('FROM Anagrafica', UpperCase(xStringaSQL)), length(xStringaSQL));
               end;
          end;
     end;


     // ORDINAMENTO - Aggregazione
     case xTipoQuery of
          0: begin

                    xGroup := ' Group By  Anagrafica.ID,Anagrafica.dacontattocliente,Anagrafica.Cognome,Anagrafica.Nome,Anagrafica.CVNumero,Anagrafica.OldCVNumero,' + chr(13) +
                         '   Anagrafica.TelUfficio,Anagrafica.Cellulare,CVInseritoInData,CVDataReale,CVDataAgg,RecapitiTelefonici,' + chr(13) +
                         '   Anagrafica.DataNascita,Anagrafica.IDStato,Anagrafica.IDTipoStato,' + chr(13) +
                         '   Anagrafica.Provincia, Anagrafica.Comune,anagrafica.indirizzo,anagrafica.cap,anagrafica.numcivico,anagrafica.tipostrada,' + chr(13) +
                         '   anagrafica.domiciliotipostrada,anagrafica.domiciliocap,anagrafica.domicilionumcivico,anagrafica.domicilioindirizzo,anagrafica.domiciliocomune,anagrafica.domicilioprovincia,' + chr(13) +
                         '   anagrafica.domicilioRegione,anagrafica.Regione,anagrafica.domicilioStato,anagrafica.Stato,' + chr(13) +
                         '   NoCv,AnagAltreInfo.Retribuzione,EMail, AnagAltreInfo.Inquadramento, googledistanzaric.distanza,googledistanzaric.tempo,emailufficio,' + chr(13) + //distanzaricmod
                         '   EspLavAttuale.Azienda, EspLavAttuale.Ruolo, EspLavAttuale.descrizionemansione,' + chr(13) +
                         '   EspLavAttuale.JobTitle, EspLavAttuale.Area, EspLavAttuale.Settore, EspLavAttuale.AreaSettore, EBC_TipiStato.TipoStato, PrimoRuolo.Mansione,' + chr(13) +
                         '   EBC_Colloqui.Data, EBC_CandidatiRicerche.DataImpegno, EBC_CandidatiRicerche.DataIns, EBC_ContattiCandidati.Data, Anagrafica.CVInseritoInData,' + chr(13) +
                         '   EspLavAttuale.titolomansione, anagfileD.IDAnagfile, anagfileD.IDAnagfile,EBC_stati.Stato ' + chr(13);
                    xOrdine := 'order by Anagrafica.Cognome,Anagrafica.Nome';
                    QRes1.SQL.Add(xGroup);
                    QRes1.SQL.Add(xOrdine);
               end;
          2: begin
                    xGroup := ' Group By  Anagrafica.ID,Anagrafica.Cognome,Anagrafica.Nome, ' + chr(13) +
                         ' Anagrafica.Provincia, Anagrafica.Comune,anagrafica.indirizzo,anagrafica.cap, ' +
                         ' anagrafica.numcivico,anagrafica.tipostrada,' + chr(13) +
                         ' anagrafica.domiciliotipostrada,anagrafica.domiciliocap, ' +
                         ' anagrafica.domicilionumcivico,anagrafica.domicilioindirizzo, ' +
                         ' anagrafica.domiciliocomune,anagrafica.domicilioprovincia,' + chr(13) +
                         ' anagrafica.domicilioRegione,anagrafica.Regione,anagrafica.domicilioStato,anagrafica.Stato ';
                    QRes1.SQL.Add(xGroup);
               end;
     end;

     // anzianit� CV
     if CBAnzCV.Checked then begin
          QRes1.ParamByName['xCVInseritoInData'] := DateToStr(Date - (SEAnzCV.Value * 30));
          //QRes1.Parameters[0].value := DateToStr(Date - (SEAnzCV.Value * 30));
          //QRes1.Parameters[1].value := DateToStr(Date - (SEAnzCV.Value * 30));
     end;

     QRes1.ParamByName['xIDUtente'] := MainForm.xIDUtenteAttuale;

     ScriviRegistry('QRes1', QRes1.SQL.text);
     ScriviRegistry('xTempQueryCount', xTempQueryCount);

     case xTipoQuery of
          0: begin
                    xTimeStamp.Add('  Inizio esecuzione QRes1: ' + FormatDateTime('hh:nn:ss:zzz', Now));
                    QRes1.Open;
                    xTimeStamp.Add('    Fine esecuzione QRes1: ' + FormatDateTime('hh:nn:ss:zzz', Now));
               end;
          1: begin
                    xQueryCount := TADOQuery.Create(self);
                    xQueryCount.Connection := Data.DB;
                    if xTempQueryCount = '' then xTempQueryCount := QRes1.SQL.Text;
                    xQueryCount.SQL.Text := 'select count (distinct anagrafica.id) ' + copy(xTempQueryCount, pos('FROM ANAGRAFICA', UpperCase(xTempQueryCount)), length(xTempQueryCount));
                    xTimeStamp.Add('Inizio esecuzione xQueryCount: ' + FormatDateTime('hh:nn:ss:zzz', Now));
                    ScriviRegistry('xQueryCount', xQueryCount.SQL.text);
                    xQueryCount.Open;
                    xTimeStamp.Add('Fine esecuzione xQueryCount: ' + FormatDateTime('hh:nn:ss:zzz', Now));
               end;
          2: begin
                    case xChiamante of
                         1: begin
                                   Data.QTemp.Close;
                                   Data.QTemp.SQL.text := 'select Valore,DescCampo from RicLineeQuery ' +
                                        ' where IDRicerca=:xIDRic: ' +
                                        ' and Tabella=''DistanzaLuogo ''' +
                                        ' and Valore is not null and Valore<>'''' ' +
                                        ' order by RicLineeQuery.ID';
                                   Data.QTemp.ParamByName['xIDRic'] := DataRicerche.TRicerchePend.FieldByName('ID').asString;
                                   Data.QTemp.Open;
                              end;
                    else begin
                              Data.QTemp.Close;
                              Data.QTemp.SQL.text := 'select Valore,DescCampo from UserLineeQuery ' +
                                   ' where IDAnagrafica=:xIDAnag: ' +
                                   ' and Tabella=''DistanzaLuogo ''' +
                                   ' and Valore is not null and Valore<>'''' ' +
                                   ' order by UserLineeQuery.ID';
                              Data.QTemp.ParamByName['xIDAnag'] := MainForm.xIDUtenteAttuale;
                              Data.QTemp.Open;
                         end;
                    end;
                    if Data.QTemp.EOF then
                         exit;



                    Data.qtemp2.close;
                    Data.qtemp2.SQL.text := 'select IDKey from FormWeb_social where social = ''Google Distance'' ';
                    Data.qtemp2.Open;




                    if Data.Qtemp2.RecordCount > 0 then begin


                         xDestinazione := Data.QTemp.FieldByName('Valore').AsString;

                         xQueryDistanza := TADOQuery.Create(self);
                         xQueryDistanza.Connection := Data.DB;
                         if xTempQueryDistanza = '' then xTempQueryDistanza := QRes1.SQL.Text;
                         xQueryDistanza.SQL.Text := 'select Anagrafica.ID,Anagrafica.Cognome,Anagrafica.Nome, ' + chr(13) +
                              ' Anagrafica.Provincia, Anagrafica.Comune,anagrafica.indirizzo,anagrafica.cap, ' +
                              ' anagrafica.numcivico,anagrafica.tipostrada,' + chr(13) +
                              ' anagrafica.domiciliotipostrada,anagrafica.domiciliocap, ' +
                              ' anagrafica.domicilionumcivico,anagrafica.domicilioindirizzo, ' +
                              ' anagrafica.domiciliocomune,anagrafica.domicilioprovincia,' + chr(13) +
                              ' anagrafica.domicilioRegione,anagrafica.Regione,anagrafica.domicilioStato,anagrafica.Stato ' +
                              copy(xTempQueryDistanza, pos('FROM ANAGRAFICA', UpperCase(xTempQueryDistanza)), length(xTempQueryDistanza));
                         xTimeStamp.Add('Inizio esecuzione xQueryDistanza: ' + FormatDateTime('hh:nn:ss:zzz', Now));
                         ScriviRegistry('xQueryDistanza', xQueryDistanza.SQL.text);
                         xQueryDistanza.Open;


                         CaricaPrimiProgressForm := TCaricaPrimiProgressForm.create(nil);
                         CaricaPrimiProgressForm.Show;
                         CaricaPrimiProgressForm.Caption := 'Recupero indirizzi per il calcolo delle distanze';
                         //CaricaPrimiProgressForm.Caption := 'Calcolo della distanza per le Anagrafiche Ricercate';
                         CaricaPrimiProgressForm.ProgressBar1.Min := 0;
                         CaricaPrimiProgressForm.ProgressBar1.Position := 0;
                         CaricaPrimiProgressForm.Update;
                         CaricaPrimiProgressForm.ProgressBar1.Max := xQueryDistanza.RecordCount;

                         {Data.Q1.Close;
                         Data.Q1.SQL.text := ' truncate table GoogleDistanzaRic ';
                         Data.Q1.ExecSql;  }

                         //CAncello record nella tabella [GoogleDistanzaRic]
                         Data.Q1.Close;
                         Data.Q1.SQl.text := 'delete from  googledistanzaric  where idutente = :xidutente: ';
                         Data.Q1.ParamByName['xidutente'] := MainForm.xIDUtenteAttuale;
                         if xChiamante = 1 then begin
                              Data.q1.SQL.add('and IDRicerca = :xidricerca:');
                              Data.Q1.ParamByName['xidricerca'] := DataRicerche.TRicerchePend.FieldByName('ID').asString;
                         end else
                              Data.q1.SQL.add('and IDRicerca is NULL');
                         Data.Q1.execSQL;

                         while not xQueryDistanza.Eof do begin

                              xPartenza := '';
                              if (Data.QTemp.FieldByName('DescCampo').AsString = 'Luogo di Residenza') then begin
                                   if xQueryDistanza.FieldByName('Comune').AsString <> '' then begin
                                        if (xQueryDistanza.FieldByName('indirizzo').AsString = '') then begin
                                             xPartenza := ''
                                        end else begin
                                             if (xQueryDistanza.FieldByName('tipostrada').AsString <> '') then
                                                  xPartenza := xPartenza + trim(xQueryDistanza.FieldByName('tipostrada').AsString) + '+';
                                             if (xQueryDistanza.FieldByName('indirizzo').AsString <> '') then
                                                  xPartenza := xPartenza + trim(xQueryDistanza.FieldByName('indirizzo').AsString) + '+';
                                             if (xQueryDistanza.FieldByName('numcivico').AsString <> '') then
                                                  xPartenza := xPartenza + trim(xQueryDistanza.FieldByName('numcivico').AsString) + '+';
                                        end;
                                        xPartenza := xPartenza + xQueryDistanza.FieldByName('Comune').AsString;
                                   end;
                              end else begin
                                   if (Data.QTemp.FieldByName('DescCampo').AsString = 'Luogo di Domicilio') then begin
                                        if xQueryDistanza.FieldByName('DomicilioComune').AsString <> '' then begin
                                             if (xQueryDistanza.FieldByName('Domicilioindirizzo').AsString = '') then begin
                                                  xPartenza := ''
                                             end else begin
                                                  if (xQueryDistanza.FieldByName('Domiciliotipostrada').AsString <> '') then
                                                       xPartenza := xPartenza + trim(xQueryDistanza.FieldByName('Domiciliotipostrada').AsString) + '+';
                                                  if (xQueryDistanza.FieldByName('Domicilioindirizzo').AsString <> '') then
                                                       xPartenza := xPartenza + trim(xQueryDistanza.FieldByName('Domicilioindirizzo').AsString) + '+';
                                                  if (xQueryDistanza.FieldByName('Domicilionumcivico').AsString <> '') then
                                                       xPartenza := xPartenza + trim(xQueryDistanza.FieldByName('Domicilionumcivico').AsString) + '+';
                                             end;
                                             xPartenza := xPartenza + xQueryDistanza.FieldByName('DomicilioComune').AsString;
                                        end;
                                   end;
                              end;

                              if (xPartenza <> '') and (xDestinazione <> '') then begin

                                   xDistance := ''; xDuration := ''; xStartLatitudine := '';
                                   xStartLongitudine := ''; xEndLatitudine := ''; xEndLongitudine := '';

                                   inc(xProg);


                                   Data.Q1.Close;
                                   Data.Q1.SQl.text := 'Insert Into googledistanzaric  (IDRicerca,IDAnagrafica,IDUtente,Prog,Partenza) ' +
                                        ' values (:xIDRicerca:,:xIDAnagrafica:,:xIDUtente:,:xProg:,:xPartenza:) ';
                                   if xChiamante = 0 then
                                        Data.Q1.ParamByName['xIDRicerca'] := NULL
                                   else
                                        Data.Q1.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').asString;
                                   Data.Q1.ParamByName['xIDAnagrafica'] := xQueryDistanza.FieldByName('ID').AsString;
                                   Data.Q1.ParamByName['xIDUtente'] := MainForm.xIDUtenteAttuale;
                                   Data.Q1.ParamByName['xProg'] := xProg;
                                   Data.Q1.ParamByName['xPartenza'] := xPartenza;
                                   Data.Q1.execSQL;





                               (*    CalcolaDistanceDurata(xPartenza, xDestinazione, 'Driving', xDistance, xDuration, xStartLatitudine,
                                        xStartLongitudine, xEndLatitudine, xEndLongitudine, xstatus);
                                   //DistanceMatrix(xPartenza, xDestinazione, 'Driving', '');
                                   if xstatus = 'Calcolo Avvenuto' then begin
                                        DataRicerche.QInsertDist.Close;
                                        DataRicerche.QInsertDist.Parameters.ParamByName('xIdRicerca').value := 0;
                                        DataRicerche.QInsertDist.Parameters.ParamByName('xIdAnagrafica').value := xQueryDistanza.FieldByName('ID').AsString;
                                        DataRicerche.QInsertDist.Parameters.ParamByName('xDistanza').value := xDistance; //FormatFloat('0.00', km1);
                                        DataRicerche.QInsertDist.Parameters.ParamByName('xTempo').value := xDuration; //FormatFloat('0.00', tempo1);
                                        DataRicerche.QInsertDist.Parameters.ParamByName('xLatitudineCand').value := xStartLatitudine;
                                        DataRicerche.QInsertDist.Parameters.ParamByName('xLongitudineCand').value := xStartLongitudine;
                                        DataRicerche.QInsertDist.Parameters.ParamByName('xLatitudineCliente').value := xEndLatitudine;
                                        DataRicerche.QInsertDist.Parameters.ParamByName('xLongitudineCliente').value := xEndLongitudine;
                                        DataRicerche.QInsertDist.ExecSQL;

                                        Data.Q1.Close;
                                        Data.Q1.SQL.Text := 'select Distanza from GoogleDistanzaRic where idanagrafica =' + xQueryDistanza.FieldByName('ID').AsString;
                                        Data.Q1.Open;
                                        if (Data.Q1.FieldByName('Distanza').AsString <> '') then begin
                                             if Pos('KM', UpperCase(Data.Q1.FieldByName('Distanza').AsString)) > 0 then begin
                                                  xDistance := copy(Data.Q1.FieldByName('Distanza').AsString, 1, pos(' ', Data.Q1.FieldByName('Distanza').AsString) - 1);
                                                  xDistance := StringReplace(trim(xDistance), ',', '.', [rfReplaceAll]);
                                                  Data.Q1.Close;
                                                  Data.Q1.SQL.Text := 'update GoogleDistanzaRic set DistanzaNum=' + xDistance +
                                                       ' where idanagrafica =' + xQueryDistanza.FieldByName('ID').AsString;
                                                  Data.Q1.ExecSQL;
                                             end else begin
                                                  if Pos('M', UpperCase(Data.Q1.FieldByName('Distanza').AsString)) > 0 then begin
                                                       xDistance := copy(Data.Q1.FieldByName('Distanza').AsString, 1, pos(' ', Data.Q1.FieldByName('Distanza').AsString) - 1);
                                                       xDistance := StringReplace(trim(xDistance), ',', '.', [rfReplaceAll]);
                                                       if Length(xDistance) = 1 then xDistance := '0.00' + xDistance;
                                                       if Length(xDistance) = 2 then xDistance := '0.0' + xDistance;
                                                       if Length(xDistance) = 3 then xDistance := '0.' + xDistance;
                                                       Data.Q1.Close;
                                                       Data.Q1.SQL.Text := 'update GoogleDistanzaRic set DistanzaNum=' + xDistance +
                                                            ' where idanagrafica =' + xQueryDistanza.FieldByName('ID').AsString;
                                                       Data.Q1.ExecSQL;
                                                  end;
                                             end;
                                        end;
                                   end;  *)
                              end;

                                   {   if Data.Qtemp2.RecordCount > 0 then begin
                                        Data.Q1.Close;
                                        Data.Q1.SQL.text := 'insert into googledistanzaric(IDAnagrafica,Partenza) values (:x0,:x1)';
                                        Data.q1.Parameters[0].Value := xQueryDistanza.FieldByName('ID').AsString;
                                        Data.q1.Parameters[1].Value := xPartenza;
                                        Data.q1.ExecSQL;

                                        xElencoPartenze := xElencoPartenze + xPartenza + '|';

                                   end;                                                          }

                              xQueryDistanza.Next;





                              CaricaPrimiProgressForm.ProgressBar1.Position := xQueryDistanza.RecNo;
                              CaricaPrimiProgressForm.ProgressBar1.Update;
                              Application.ProcessMessages;
                         end;
                         CaricaPrimiProgressForm.Close;
                         CaricaPrimiProgressForm.Free;
                    end;
                         //DistanceMatrix(xElencoPartenze, xDestinazione, 'Driving', '');


                    if xChiamante = 0 then
                         DistanceMatrix(Mainform.xIDUtenteAttuale, 0, xDestinazione)
                    else
                         DistanceMatrix(Mainform.xIDUtenteAttuale, DataRicerche.TRicerchePend.FieldByName('ID').AsInteger, xDestinazione);

                    xTimeStamp.Add('Fine esecuzione xQueryDistanza: ' + FormatDateTime('hh:nn:ss:zzz', Now));


               end;
     end;


     if xChiamante = 1 then begin
          QRicLineeQuery.Close;
          //QRicLineeQuery.SQL.text := 'select * from RicLineeQuery where IDRicerca=:xIDRic:';
          QRicLineeQuery.SQL.text := 'select * from RicLineeQuery ' +
               ' where IDRicerca=:xIDRic: ' +
               ' and Valore is not null and Valore<>'''' ' +
               ' order by RicLineeQuery.ID';
          QRicLineeQuery.ParamByName['xIDRic'] := DataRicerche.TRicerchePend.FieldByName('ID').asString;
          QRicLineeQuery.Open;
     end else begin
          TLinee.Close;
          //TLinee.SQL.text := 'select * from UserLineeQuery where IDAnagrafica=:xIDAnag:';
          TLinee.SQL.text := 'select * from UserLineeQuery ' +
               ' where IDAnagrafica=:xIDAnag: ' +
               ' and Valore is not null and Valore<>'''' ' +
               ' order by UserLineeQuery.ID';
          TLinee.ParamByName['xIDAnag'] := MainForm.xIDUtenteAttuale;
          TLinee.Open;
     end;
     case xTipoQuery of
          0: begin
                    dxDBGrid1.Visible := True;
                    if QRes1.RecordCount > 0 then
                         LTot.Caption := IntToStr(QRes1.RecordCount)
                    else LTot.Caption := '';
                    LTot.Visible := True;
               end;
          1: begin
                    dxDBGrid1.Visible := False;
                    if xQueryCount.RecordCount > 0 then
                         LTot.Caption := IntToStr(xQueryCount.Fields[0].Value)
                    else LTot.Caption := '';
                    LTot.Visible := True;

                    xQueryCount.Close;
                    xQueryCount.Free;
               end;
          2: begin
                    dxDBGrid1.Visible := False;
                    if xQueryDistanza.RecordCount > 0 then
                         LTot.Caption := IntToStr(xQueryDistanza.Fields[0].Value)
                    else LTot.Caption := '';
                    LTot.Visible := True;
                    xQueryDistanza.Close;
                    xQueryDistanza.Free;
               end;
     end;

     xTimeStamp.Add('FINE: ' + FormatDateTime('hh:nn:ss:zzz', Now));
     ScriviRegistry('EseguiQueryRic_time', xTimeStamp.text);

     // colonne visibili o meno a seconda del cliente
     if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 5)) = 'ERGON' then begin
          dxDBGrid1CVNumero.FieldName := 'OldCVNumero';
          dxDBGrid1CVNumero.Caption := 'Old n�CV';
          dxDBGrid1TipoStato.Visible := False;
          dxDBGrid1DomicilioComune.Visible := False;
     end;
end;

procedure TSelPersNewForm.QRes_testCalcFields(DataSet: TDataSet);
var xAnno, xMese, xgiorno: Word;
begin
     if QRes_test.FieldByName('DataNascita').asString = '' then
          QRes_test.FieldByName('Eta').asString := ''
     else begin
          DecodeDate((Date - QRes_test.FieldByName('DataNascita').AsDateTime), xAnno, xMese, xgiorno);
          // TAnagraficaEta.Value:=StrToInt(copy(xAnno,3,2));
          QRes_test.FieldByName('Eta').Value := copy(IntToStr(xAnno), 3, 2);
     end;
     QRes_test.FieldByName('TelUffCell').AsString := QRes_test.FieldByName('TelUfficio').AsString + ' - ' + QRes_test.FieldByName('Cellulare').AsString;
end;

procedure TSelPersNewForm.DsQRicLineeQueryStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsQRicLineeQuery.State in [dsEdit, dsInsert];
     BCriterioNew.Enabled := not b;
     BCriterioMod.Enabled := not b;
     BCriterioDel.Enabled := not b;
     BCriterioDelTutti.Enabled := not b;
     BLineeOK.Enabled := b;
     BLineeCan.Enabled := b;
end;

procedure TSelPersNewForm.dxDBGrid2DescTabellaButtonClick(Sender: TObject;
     AbsoluteIndex: Integer);
begin
     PredisponiWizard;
     // setta variabile "pagina attiva"
     SelCriterioWizardForm.xPaginaAttiva := 0;
     SelCriterioWizardForm.ShowModal;
     if SelCriterioWizardForm.ModalResult = mrOK then begin
          SalvaDaWizard;
          RicreaStringaSQL;
     end;
end;

procedure TSelPersNewForm.dxDBGrid2DescCampoButtonClick(Sender: TObject;
     AbsoluteIndex: Integer);
begin
     PredisponiWizard;
     // setta variabile "pagina attiva"
     SelCriterioWizardForm.xPaginaAttiva := 1;
     SelCriterioWizardForm.ShowModal;
     if SelCriterioWizardForm.ModalResult = mrOK then begin
          SalvaDaWizard;
          RicreaStringaSQL;
     end;
end;

procedure TSelPersNewForm.dxDBGrid2DescOperatoreButtonClick(
     Sender: TObject; AbsoluteIndex: Integer);
begin
     PredisponiWizard;
     // setta variabile "pagina attiva"
     SelCriterioWizardForm.xPaginaAttiva := 2;
     SelCriterioWizardForm.ShowModal;
     if SelCriterioWizardForm.ModalResult = mrOK then begin
          SalvaDaWizard;
          RicreaStringaSQL;
          if xchiamante = 1 then begin
               QRicLineeQuery.edit;
               QRicLineeQuery.Post;
          end;
     end;
end;

procedure TSelPersNewForm.dxDBGrid2ValoreEditButtonClick(Sender: TObject);
begin
     PredisponiWizard;
     // setta variabile "pagina attiva"
     SelCriterioWizardForm.xPaginaAttiva := 3;
     SelCriterioWizardForm.ShowModal;
     if SelCriterioWizardForm.ModalResult = mrOK then begin
          SalvaDaWizard;
          if xchiamante = 1 then begin
               QRicLineeQuery.edit;
               QRicLineeQuery.Post;
          end else begin
               Tlinee.Edit;
               TLinee.Post;
          end;
     end;
end;

procedure TSelPersNewForm.dxDBGrid2LivLinguaParlatoButtonClick(
     Sender: TObject; AbsoluteIndex: Integer);
var xID: string;
begin
     if xChiamante = 1 then begin
          if TrimRight(QRicLineeQuery.fieldbyname('Tabella').Value) = 'LingueConosciute' then begin
               xID := OpenSelFromTab('LivelloLingue', 'ID', 'LivelloConoscenza', 'Livello conoscenza', TrimRight(QRicLineeQuery.fieldbyname('LivLinguaParlato').asString), False);
               if xID <> '' then begin
                    if not (dsQRicLineeQuery.state = dsEdit) then QRicLineeQuery.Edit;
                    QRicLineeQuery.fieldbyname('LivLinguaParlato').Value := StrToIntDef(xID, 0);
                    QRicLineeQuery.Post;
               end;
          end;
     end else begin
          if TrimRight(TLineeTabella.Value) = 'LingueConosciute' then begin
               xID := OpenSelFromTab('LivelloLingue', 'ID', 'LivelloConoscenza', 'Livello conoscenza', TrimRight(TLineeLivLinguaParlato.asString), False);
               if xID <> '' then begin
                    if not (dsLinee.state = dsEdit) then TLinee.Edit;
                    TLineeLivLinguaParlato.Value := StrToIntDef(xID, 0);
                    TLinee.Post;
               end;
          end;
     end;
end;

procedure TSelPersNewForm.dxDBGrid2LivLinguaScrittoButtonClick(
     Sender: TObject; AbsoluteIndex: Integer);
var xID: string;
begin
     if xChiamante = 1 then begin
          if TrimRight(QRicLineeQuery.fieldbyname('Tabella').Value) = 'LingueConosciute' then begin
               xID := OpenSelFromTab('LivelloLingue', 'ID', 'LivelloConoscenza', 'Livello conoscenza', TrimRight(QRicLineeQuery.fieldbyname('LivLinguaScritto').asString), False);
               if xID <> '' then begin
                    if not (dsLinee.state = dsEdit) then QRicLineeQuery.Edit;
                    QRicLineeQuery.fieldbyname('LivLinguaScritto').Value := StrToIntDef(xID, 0);
                    QRicLineeQuery.Post;
               end;
          end;
     end else begin
          if TrimRight(TLineeTabella.Value) = 'LingueConosciute' then begin
               xID := OpenSelFromTab('LivelloLingue', 'ID', 'LivelloConoscenza', 'Livello conoscenza', TrimRight(TLineeLivLinguaScritto.asString), False);
               if xID <> '' then begin
                    if not (dsLinee.state = dsEdit) then TLinee.Edit;
                    TLineeLivLinguaScritto.Value := StrToIntDef(xID, 0);
                    TLinee.Post;
               end;
          end;
     end;
end;

procedure TSelPersNewForm.dxDBGrid2CustomDrawCell(Sender: TObject;
     ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
     AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
     var AText: string; var AColor: TColor; AFont: TFont;
     var AAlignment: TAlignment; var ADone: Boolean);
var xID: integer;
begin
     if AColumn = dxDBGrid2Valore then begin
          if ANode.Strings[dxDBGrid2QueryLookup.Index] <> '' then begin
               xID := ANode.Values[dxDBGrid2ID.index];
               Q.Close;
               Q.SQL.text := ANode.Strings[dxDBGrid2QueryLookup.Index];
               Q.ParamByName['xID'] := StrToIntDef(ANode.Strings[dxDBGrid2Valore.Index], 0);
               Q.Open;
               AText := Q.FieldByName('Dicitura').asString;
          end;
     end;
end;

procedure TSelPersNewForm.QRicLineeQueryBeforePost(DataSet: TDataSet);
begin
     if (QRicLineeQuery.Eof = false) and (QRicLineeQuery.FieldByName('Next').AsString = 'fine') then QRicLineeQuery.FieldByName('Next').AsString := 'e';
     if QRicLineeQuery.FieldByName('Next').AsString = 'e' then QRicLineeQuery.FieldByName('OpSucc').AsString := 'and';
     if QRicLineeQuery.FieldByName('Next').AsString = 'o' then QRicLineeQuery.FieldByName('OpSucc').AsString := 'or';
     if QRicLineeQuery.FieldByName('Next').AsString = 'fine' then QRicLineeQuery.FieldByName('OpSucc').AsString := '';
     RicreaStringaSQL;
end;

procedure TSelPersNewForm.dxDBGrid3ValoreEditButtonClick(Sender: TObject);
begin
     PredisponiWizard;
     // setta variabile "pagina attiva"
     SelCriterioWizardForm.xPaginaAttiva := 3;
     SelCriterioWizardForm.ShowModal;
     if SelCriterioWizardForm.ModalResult = mrOK then begin
          SalvaDaWizard;
          if xchiamante = 1 then begin
               QRicLineeQuery.edit;
               QRicLineeQuery.Post;
          end else begin
               Tlinee.Edit;
               TLinee.Post;
          end;
     end;
end;

procedure TSelPersNewForm.dxDBGrid3CustomDrawCell(Sender: TObject;
     ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
     AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
     var AText: string; var AColor: TColor; AFont: TFont;
     var AAlignment: TAlignment; var ADone: Boolean);
var xID: integer;
begin
     if AColumn = dxDBGrid3Valore then begin
          if ANode.Strings[dxDBGrid3QueryLookup.Index] <> '' then begin
               xID := ANode.Values[dxDBGrid3ID.index];
               Q.Close;
               Q.SQL.text := ANode.Strings[dxDBGrid3QueryLookup.Index];
               Q.ParamByName['xID'] := StrToIntDef(ANode.Strings[dxDBGrid3Valore.Index], 0);
               Q.Open;
               AText := Q.FieldByName('Dicitura').asString;
          end;
     end;
end;

procedure TSelPersNewForm.MiniProfilo1Click(Sender: TObject);
var k: integer;
     xIDs: string;
begin
     if dxDBGrid1.SelectedCount = 0 then begin
          MessageDlg('Nessun record selezionato. Impossibile proseguire', mtError, [mbOK], 0);
          exit;
     end;

     xIDs := '';
     for k := 0 to dxDBGrid1.SelectedCount - 1 do begin
          QRes1.BookMark := dxDBGrid1.SelectedRows[k];
          xIDs := xIDs + QRes1.FieldByName('ID').AsString + ',';
     end;
     xIDs := copy(xIDS, 1, length(xIDs) - 1);
     CreaMiniProfiliSCR('Motore', 0, xIDs, False, Data.DB.ConnectionString);
end;

procedure TSelPersNewForm.MiniProfiloAnonimo1Click(Sender: TObject);
var k: integer;
     xIDs: string;
begin
     if dxDBGrid1.SelectedCount = 0 then begin
          MessageDlg('Nessun record selezionato. Impossibile proseguire', mtError, [mbOK], 0);
          exit;
     end;

     xIDs := '';
     for k := 0 to dxDBGrid1.SelectedCount - 1 do begin
          QRes1.BookMark := dxDBGrid1.SelectedRows[k];
          xIDs := xIDs + QRes1.FieldByName('ID').AsString + ',';
     end;
     xIDs := copy(xIDS, 1, length(xIDs) - 1);
     CreaMiniProfiliSCR('Motore', 0, xIDs, True, Data.DB.ConnectionString);
end;

procedure TSelPersNewForm.PMStati6Click(Sender: TObject);
begin
     PMStati6.Checked := not PMStati6.Checked;
end;

procedure TSelPersNewForm.PMStati7Click(Sender: TObject);
begin
     PMStati7.Checked := not PMStati7.Checked;
end;

end.

