unit CliCandidati;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ComCtrls, ExtCtrls, Db, DBTables, ImgList, Menus, StdCtrls, ADODB,
     U_ADOLinkCl;

type
     TCliCandidatiFrame = class(TFrame)
          Panel1: TPanel;
          Panel104: TPanel;
          Q1_OLD: TQuery;
          TV: TTreeView;
          ImageList1: TImageList;
          ImageList3: TImageList;
          PopupMenu3: TPopupMenu;
          Infocandidato1: TMenuItem;
          curriculum1: TMenuItem;
          documenti1: TMenuItem;
          fileWord1: TMenuItem;
          Label1: TLabel;
          Q1: TADOLinkedQuery;
          CBCandRic: TCheckBox;
          Label2: TLabel;
          procedure Infocandidato1Click(Sender: TObject);
          procedure curriculum1Click(Sender: TObject);
          procedure documenti1Click(Sender: TObject);
          procedure fileWord1Click(Sender: TObject);
          procedure CBCandRicClick(Sender: TObject);
          procedure CancSituazCandClick(Sender: TObject);
     private
     public
          xIDCliente, xIDRicerca: integer;
          procedure CreaAlbero;
          procedure CancNodoSituazCand();
     end;

implementation

uses SchedaCand, uUtilsVarie, ModuloDati, Curriculum, Main, View, ElencoDip,
     ModuloDati2, InsInserimento, InputPassword, CheckPass;

{$R *.DFM}

procedure TCliCandidatiFrame.CreaAlbero;
var N, N1: TTreeNode;
     xDicNodo: string;
begin
     TV.Items.BeginUpdate;
     TV.Items.Clear;


     if CBCandRic.Checked = false then begin
     // primo nodo: INSERITI
          N := TV.Items.AddChildObject(nil, 'CANDIDATI INSERITI IN AZIENDA', pointer(0));
          N.ImageIndex := 0;
          N.SelectedIndex := 0;
          Q1.Close;
          Q1.SQL.clear;
          Q1.SQL.Text := 'select EBC_Inserimenti.IDAnagrafica,Anagrafica.Cognome,Anagrafica.Nome,Anagrafica.CVNumero, ' +
               'EBC_Ricerche.Progressivo RifRicerca, ' +
               'EBC_Inserimenti.DallaData,EBC_Inserimenti.AllaData,ProgFattura, ' +
               'EBC_Inserimenti.MotivoCessazione, mansioni.descrizione ruolo, ' +
               'EBC_Inserimenti.IDCliente, EBC_Inserimenti.IDRicerca' +
               ' from EBC_Inserimenti ' +
               ' join EBC_Ricerche on EBC_Inserimenti.IDRicerca=EBC_Ricerche.ID ' +
               ' join Anagrafica on EBC_Inserimenti.IDAnagrafica=Anagrafica.ID ' +
               ' join Mansioni on ebc_ricerche.idmansione=mansioni.id ' +
               'where EBC_Inserimenti.IDCliente=' + IntToStr(xIDCliente) +
               'and EBC_Ricerche.id<>' + IntToStr(xIDRicerca) + '  ' +
               'order by Cognome,Nome';
          Q1.Open;
          while not Q1.EOF do begin
               xDicNodo := trim(Q1.FieldByName('Cognome').asString) + ' ' + trim(Q1.FieldByName('Nome').asString) + '  dal ' + Q1.FieldByName('DallaData').asString + ' (Ricerca n� ' + trim(Q1.FieldByName('RifRicerca').asString) + ')' + ' - Ruolo: ' + trim(Q1.FieldByName('Ruolo').asString);
               N1 := TV.Items.AddChildObject(N, xDicNodo, pointer(Q1.FieldByName('IDAnagrafica').asInteger));
               N1.ImageIndex := 1;
               N1.SelectedIndex := 1;
               Q1.Next;
          end;
     end;
          // primo bis nodo: INSERITI PPER QUESTA COMMESSA
     N := TV.Items.AddChildObject(nil, 'CANDIDATI INSERITI IN AZIENDA (per questa ricerca)', pointer(0));
     N.ImageIndex := 0;
     N.SelectedIndex := 0;
     Q1.Close;
     Q1.SQL.clear;
     Q1.SQL.Text := 'select EBC_Inserimenti.IDAnagrafica,Anagrafica.Cognome,Anagrafica.Nome,Anagrafica.CVNumero, ' +
          'EBC_Ricerche.Progressivo RifRicerca, ' +
          'EBC_Inserimenti.DallaData,EBC_Inserimenti.AllaData,ProgFattura, ' +
          'EBC_Inserimenti.MotivoCessazione, mansioni.descrizione ruolo ' +
          'from EBC_Inserimenti ' +
          ' join EBC_Ricerche on EBC_Inserimenti.IDRicerca=EBC_Ricerche.ID ' +
          ' join Anagrafica on EBC_Inserimenti.IDAnagrafica=Anagrafica.ID ' +
          ' join Mansioni on ebc_ricerche.idmansione=mansioni.id ' +
          'where EBC_Ricerche.ID=' + IntToStr(xIDRicerca) + ' ' +
          'order by Cognome,Nome';
     Q1.Open;
     while not Q1.EOF do begin
          xDicNodo := trim(Q1.FieldByName('Cognome').asString) + ' ' + trim(Q1.FieldByName('Nome').asString) + '  dal ' + Q1.FieldByName('DallaData').asString + ' (Ricerca n� ' + trim(Q1.FieldByName('RifRicerca').asString) + ')' + ' - Ruolo: ' + trim(Q1.FieldByName('Ruolo').asString);
          N1 := TV.Items.AddChildObject(N, xDicNodo, pointer(Q1.FieldByName('IDAnagrafica').asInteger));
          N1.ImageIndex := 1;
          N1.SelectedIndex := 1;
          Q1.Next;
     end;


     // secondo nodo: PRESENTATI

     if xIDRicerca > 0 then
          N := TV.Items.AddChildObject(nil, 'CANDIDATI PRESENTATI (per questa ricerca)', pointer(1))
     else N := TV.Items.AddChildObject(nil, 'CANDIDATI PRESENTATI', pointer(1));
     N.ImageIndex := 0;
     N.SelectedIndex := 0;
     Q1.Close;
     Q1.SQL.clear;
     Q1.SQL.Text := 'select Storico.IDAnagrafica,Anagrafica.Cognome,Anagrafica.Nome,Anagrafica.CVNumero, ' +
          '       DataEvento,Evento, Users.Nominativo Utente ' +
          'from Storico ' +
          ' join Anagrafica on Storico.IDAnagrafica=Anagrafica.ID ' +
          ' join EBC_Eventi on Storico.IDEvento=EBC_Eventi.ID ' +
          ' join Users on Storico.IDUtente=Users.ID ' +
          'where IDEvento in (20,76,21) ' +
          'and Storico.IDCliente=' + IntToStr(xIDCliente);
     if xIDRicerca > 0 then
          Q1.SQL.add('and IDRicerca=' + IntToStr(xIDRicerca));
     Q1.SQL.add('order by Cognome,Nome');
     //q1.sql.savetofile('ff.sql');
     Q1.Open;
     while not Q1.EOF do begin
          xDicNodo := trim(Q1.FieldByName('Cognome').asString) + ' ' + trim(Q1.FieldByName('Nome').asString) + ' ' + trim(Q1.FieldByName('Evento').asString) + ' ' +
               ' il ' + Q1.FieldByName('DataEvento').asString + ' (utente: ' + trim(Q1.FieldByName('utente').asString) + ')';
          N1 := TV.Items.AddChildObject(N, xDicNodo, pointer(Q1.FieldByName('IDAnagrafica').asInteger));
          N1.ImageIndex := 1;
          N1.SelectedIndex := 1;
          Q1.Next;
     end;

     // terzo nodo: candidati visti a colloquio
     if CBCandRic.Checked = false then begin
          N := TV.Items.AddChildObject(nil, 'CANDIDATI VISTI A COLLOQUIO', pointer(2));
          N.ImageIndex := 0;
          N.SelectedIndex := 0;
          Q1.Close;
          Q1.SQL.clear;
          Q1.SQL.Text := 'select Anagrafica.ID IDAnagrafica,Anagrafica.Cognome,Anagrafica.Nome,Anagrafica.CVNumero, ' +
               '       Data,ValutazioneGenerale, Users.Nominativo Utente ' +
               'from EBC_Colloqui ' +
               ' join Anagrafica on EBC_Colloqui.IDAnagrafica=Anagrafica.ID ' +
               ' join EBC_Ricerche on EBC_Colloqui.IDRicerca=EBC_Ricerche.ID ' +
               ' join Users on EBC_Colloqui.IDSelezionatore=Users.ID ' +
               'where EBC_Ricerche.IDCliente=' + IntToStr(xIDCliente) + ' ' +
               '  and ebc_ricerche.id<>' + IntToStr(xIDRicerca) +
               '  order by Cognome,Nome ';
          Q1.Open;
          while not Q1.EOF do begin
               xDicNodo := trim(Q1.FieldByName('Cognome').asString) + ' ' + trim(Q1.FieldByName('Nome').asString) + '  colloquio del ' +
                    Q1.FieldByName('Data').asString + ' - ' + Q1.FieldByName('ValutazioneGenerale').asString +
                    ' (' + trim(Q1.FieldByName('utente').asString) + ')';
               N1 := TV.Items.AddChildObject(N, xDicNodo, pointer(Q1.FieldByName('IDAnagrafica').asInteger));
               N1.ImageIndex := 1;
               N1.SelectedIndex := 1;
               Q1.Next;
          end;
     end;

     // terzo nodo B: candidati visti a colloquio (per questa commessa)
     N := TV.Items.AddChildObject(nil, 'CANDIDATI VISTI A COLLOQUIO (per questa ricerca)', pointer(2));
     N.ImageIndex := 0;
     N.SelectedIndex := 0;
     Q1.Close;
     Q1.SQL.clear;
     Q1.SQL.Text := 'select Anagrafica.ID IDAnagrafica,Anagrafica.Cognome,Anagrafica.Nome,Anagrafica.CVNumero, ' +
          '       Data,ValutazioneGenerale, Users.Nominativo Utente ' +
          'from EBC_Colloqui ' +
          ' join Anagrafica on EBC_Colloqui.IDAnagrafica=Anagrafica.ID ' +
          ' join EBC_Ricerche on EBC_Colloqui.IDRicerca=EBC_Ricerche.ID ' +
          ' join Users on EBC_Colloqui.IDSelezionatore=Users.ID ' +
          'where EBC_Ricerche.ID=' + IntToStr(xIDRicerca) + ' ' +
          '  order by Cognome,Nome ';
     Q1.Open;
     while not Q1.EOF do begin
          xDicNodo := trim(Q1.FieldByName('Cognome').asString) + ' ' + trim(Q1.FieldByName('Nome').asString) + '  colloquio del ' +
               Q1.FieldByName('Data').asString + ' - ' + Q1.FieldByName('ValutazioneGenerale').asString +
               ' (' + trim(Q1.FieldByName('utente').asString) + ')';
          N1 := TV.Items.AddChildObject(N, xDicNodo, pointer(Q1.FieldByName('IDAnagrafica').asInteger));
          N1.ImageIndex := 1;
          N1.SelectedIndex := 1;
          Q1.Next;
     end;


     // terzo nodo bis: candidati visti a colloquio (presso il cliente)
     if CBCandRic.Checked = false then begin
          N := TV.Items.AddChildObject(nil, 'CANDIDATI VISTI A COLLOQUIO (PRESSO IL CLIENTE)', pointer(2));
          N.ImageIndex := 0;
          N.SelectedIndex := 0;
          Q1.Close;
          Q1.SQL.clear;
          Q1.SQL.Text := 'select Anagrafica.ID IDAnagrafica,Anagrafica.Cognome,Anagrafica.Nome,Anagrafica.CVNumero, ' +
               //'       Data,ValutazioneGenerale,Nominativo Utente ' +
               '       Data,ValutazioneCliente, Users.Nominativo Utente ' +
               ' from EBC_Colloqui ' +
               ' join Anagrafica on EBC_Colloqui.IDAnagrafica=Anagrafica.ID ' +
               ' join EBC_Ricerche on EBC_Colloqui.IDRicerca=EBC_Ricerche.ID ' +
               ' join Users on EBC_Colloqui.IDSelezionatore=Users.ID ' +
               'where EBC_Ricerche.IDCliente=' + IntToStr(xIDCliente) + ' ' +
               '  and EBC_Colloqui.PressoCliente=1 ' +
               'order by Cognome,Nome ';
          Q1.Open;
          while not Q1.EOF do begin
               xDicNodo := trim(Q1.FieldByName('Cognome').asString) + ' ' + trim(Q1.FieldByName('Nome').asString) + '  colloquio del ' +
                    //Q1.FieldByName('Data').asString + ' - ' + Q1.FieldByName('ValutazioneGenerale').asString +
                    Q1.FieldByName('Data').asString + ' - ' + Q1.FieldByName('ValutazioneCliente').asString +
                    ' (' + trim(Q1.FieldByName('utente').asString) + ')';
               N1 := TV.Items.AddChildObject(N, xDicNodo, pointer(Q1.FieldByName('IDAnagrafica').asInteger));
               N1.ImageIndex := 1;
               N1.SelectedIndex := 1;
               Q1.Next;
          end;
     // quarto nodo: CV PROPRI
          N := TV.Items.AddChildObject(nil, 'CANDIDATI "PROPRI"', pointer(3));
          N.ImageIndex := 0;
          N.SelectedIndex := 0;
          Q1.Close;
          Q1.SQL.clear;
          Q1.SQL.Text := 'select Anagrafica.ID IDAnagrafica,Anagrafica.Cognome,Anagrafica.Nome,Anagrafica.CVNumero, ' +
               '       Anagrafica.CVInseritoInData ' +
               'from Anagrafica ' +
               'where IDProprietaCV=' + IntToStr(xIDCliente) + ' ' +
               'order by Cognome,Nome ';
          Q1.Open;
          while not Q1.EOF do begin
               xDicNodo := trim(Q1.FieldByName('Cognome').asString) + ' ' + trim(Q1.FieldByName('Nome').asString) + '  inserito in data ' + Q1.FieldByName('CVInseritoInData').asString;
               N1 := TV.Items.AddChildObject(N, xDicNodo, pointer(Q1.FieldByName('IDAnagrafica').asInteger));
               N1.ImageIndex := 1;
               N1.SelectedIndex := 1;
               Q1.Next;
          end;
     // quinto nodo: soggetti incompatibili
          N := TV.Items.AddChildObject(nil, 'CANDIDATI INCOMPATIBILI', pointer(4));
          N.ImageIndex := 0;
          N.SelectedIndex := 0;
          Q1.Close;
          Q1.SQL.clear;
          Q1.SQL.Text := 'select Anagrafica.ID IDAnagrafica,Anagrafica.Cognome,Anagrafica.Nome,Anagrafica.CVNumero, ' +
               '       AnagIncompClienti.Descrizione ' +
               'from AnagIncompClienti ' +
               'join Anagrafica on AnagIncompClienti.IDAnagrafica=Anagrafica.ID ' +
               'where IDCliente=' + IntToStr(xIDCliente) + ' ' +
               'order by Cognome,Nome ';
          Q1.Open;
          while not Q1.EOF do begin
               xDicNodo := trim(Q1.FieldByName('Cognome').asString) + ' ' + trim(Q1.FieldByName('Nome').asString) + ' - ' + trim(Q1.FieldByName('Descrizione').asString);
               N1 := TV.Items.AddChildObject(N, xDicNodo, pointer(Q1.FieldByName('IDAnagrafica').asInteger));
               N1.ImageIndex := 1;
               N1.SelectedIndex := 1;
               Q1.Next;
          end;
     // sesto nodo: soggetti nel CdA
          N := TV.Items.AddChildObject(nil, 'APPARTENENTI AL CDA', pointer(4));
          N.ImageIndex := 0;
          N.SelectedIndex := 0;
          Q1.Close;
          Q1.SQL.clear;
          Q1.SQL.Text := 'select A.ID IDAnagrafica, A.Cognome,A.Nome from EsperienzeLavorative E ' +
               'join Anagrafica A on E.IDAnagrafica = A.ID and E.IDAzienda=' + IntToStr(xIDCliente) + ' and Flag_CdA=1';
          Q1.Open;
          while not Q1.EOF do begin
               xDicNodo := trim(Q1.FieldByName('Cognome').asString) + ' ' + trim(Q1.FieldByName('Nome').asString);
               N1 := TV.Items.AddChildObject(N, xDicNodo, pointer(Q1.FieldByName('IDAnagrafica').asInteger));
               N1.ImageIndex := 1;
               N1.SelectedIndex := 1;
               Q1.Next;
          end;
     // settimo nodo: soggetti nel Collegio Sindacale
          N := TV.Items.AddChildObject(nil, 'APPARTENENTI AL COLLEGIO SINDACALE', pointer(4));
          N.ImageIndex := 0;
          N.SelectedIndex := 0;
          Q1.Close;
          Q1.SQL.clear;
          Q1.SQL.Text := 'select A.ID IDAnagrafica, A.Cognome,A.Nome from EsperienzeLavorative E ' +
               'join Anagrafica A on E.IDAnagrafica = A.ID and E.IDAzienda=' + IntToStr(xIDCliente) + ' and Flag_CollegioSind=1';
          Q1.Open;
          while not Q1.EOF do begin
               xDicNodo := trim(Q1.FieldByName('Cognome').asString) + ' ' + trim(Q1.FieldByName('Nome').asString);
               N1 := TV.Items.AddChildObject(N, xDicNodo, pointer(Q1.FieldByName('IDAnagrafica').asInteger));
               N1.ImageIndex := 1;
               N1.SelectedIndex := 1;
               Q1.Next;
          end;
     // ottavo nodo: soggetti nel Consiglio di gestione
          N := TV.Items.AddChildObject(nil, 'APPARTENENTI AL CONSIGLIO DI GESTIONE', pointer(4));
          N.ImageIndex := 0;
          N.SelectedIndex := 0;
          Q1.Close;
          Q1.SQL.clear;
          Q1.SQL.Text := 'select A.ID IDAnagrafica, A.Cognome,A.Nome from EsperienzeLavorative E ' +
               'join Anagrafica A on E.IDAnagrafica = A.ID and E.IDAzienda=' + IntToStr(xIDCliente) + ' and Flag_ConsiglioGest=1';
          Q1.Open;
          while not Q1.EOF do begin
               xDicNodo := trim(Q1.FieldByName('Cognome').asString) + ' ' + trim(Q1.FieldByName('Nome').asString);
               N1 := TV.Items.AddChildObject(N, xDicNodo, pointer(Q1.FieldByName('IDAnagrafica').asInteger));
               N1.ImageIndex := 1;
               N1.SelectedIndex := 1;
               Q1.Next;
          end;
     // nono nodo: soggetti nel Consiglio di sorveglianza
          N := TV.Items.AddChildObject(nil, 'APPARTENENTI AL CONSIGLIO DI SORVEGLIANZA', pointer(4));
          N.ImageIndex := 0;
          N.SelectedIndex := 0;
          Q1.Close;
          Q1.SQL.clear;
          Q1.SQL.Text := 'select A.ID IDAnagrafica, A.Cognome,A.Nome from EsperienzeLavorative E ' +
               'join Anagrafica A on E.IDAnagrafica = A.ID and E.IDAzienda=' + IntToStr(xIDCliente) + ' and Flag_ConsiglioSorv=1';
          Q1.Open;
          while not Q1.EOF do begin
               xDicNodo := trim(Q1.FieldByName('Cognome').asString) + ' ' + trim(Q1.FieldByName('Nome').asString);
               N1 := TV.Items.AddChildObject(N, xDicNodo, pointer(Q1.FieldByName('IDAnagrafica').asInteger));
               N1.ImageIndex := 1;
               N1.SelectedIndex := 1;
               Q1.Next;
          end;
     end;
     Q1.Close;
     TV.Items.EndUpdate;
     TV.FullExpand;
     TV.Selected := TV.Items[0];
end;

procedure TCliCandidatiFrame.Infocandidato1Click(Sender: TObject);
begin
     if TV.Selected.level = 0 then exit;
     SchedaCandForm := TSchedaCandForm.create(self);
     if not PosizionaAnag(longint(TV.Selected.Data)) then exit;
     SchedaCandForm.ShowModal;
     SchedaCandForm.Free;
end;

procedure TCliCandidatiFrame.curriculum1Click(Sender: TObject);
begin
     if TV.Selected.level = 0 then exit;
     if not PosizionaAnag(longint(TV.Selected.Data)) then exit;
     CurriculumForm.ShowModal;
     MainForm.Pagecontrol5.ActivePage := MainForm.TSStatoTutti;
end;

procedure TCliCandidatiFrame.documenti1Click(Sender: TObject);
var xFile: string;
     xProcedi: boolean;
begin
     if TV.Selected.level = 0 then exit;
     ApriCV(longint(TV.Selected.Data));
end;

procedure TCliCandidatiFrame.fileWord1Click(Sender: TObject);
begin
     if TV.Selected.level = 0 then exit;
     if not PosizionaAnag(longint(TV.Selected.Data)) then exit;
     ApriFileWord(Data.TAnagrafica.FieldByName('CVNumero').AsString);
end;

procedure TCliCandidatiFrame.CBCandRicClick(Sender: TObject);
begin
     CreaAlbero;
end;

procedure TCliCandidatiFrame.CancSituazCandClick(Sender: TObject);
var xPassword: string;
     xcanc: TADOQuery;
begin
     InputPasswordForm.EPassword.text := 'EPassword';
     InputPasswordForm.caption := 'Inserimento Password Administrator';
     InputPasswordForm.ShowModal;
     if InputPasswordForm.ModalResult = mrOK then begin
          xPassword := InputPasswordForm.EPassword.Text;
     end else exit;
     if not CheckPassword(xPassword, GetPwdUtenteResp(9)) then begin
          MessageDlg('Password errata', mtError, [mbOK], 0);
          exit;
     end;
     CancNodoSituazCand();
end;

procedure TCliCandidatiFrame.CancNodoSituazCand();
begin
     {if TV.Selected.level = 0 then exit;
     if not PosizionaAnag(longint(TV.Selected.Data)) then exit;
     ShowMessage(TV.Items.Item[0].Text);
     //case TV.Items.Item[0].
     xcanc := TADOQuery.Create(nil);
     xcanc.Connection := Data.DB;
     xcanc.SQL.Text := 'Delete from ';
     xcanc.ExecSQL;
     xcanc.Close;
     xcanc.Free; }
end;

end.

