unit EmailConnectInfo;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, DBTables, ExtCtrls, StdCtrls, Buttons, ADODB, U_ADOLinkCl;

type
     TEmailConnectInfoForm = class(TForm)
          BitBtn1: TBitBtn;
          BitBtn2: TBitBtn;
          Label1: TLabel;
          Label2: TLabel;
          Label4: TLabel;
          EHost: TEdit;
          EPort: TEdit;
          EUserID: TEdit;
          Label3: TLabel;
          EName: TEdit;
          Bevel1: TBevel;
          Label6: TLabel;
          EAddress: TEdit;
          Panel1: TPanel;
          Label5: TLabel;
          Q_OLD: TQuery;
          EUtente: TEdit;
          Label7: TLabel;
          Memo1: TMemo;
          Q: TADOLinkedQuery;
          procedure FormShow(Sender: TObject);
     private
          { Private declarations }
     public
          xIDUser: integer;
     end;

var
     EmailConnectInfoForm: TEmailConnectInfoForm;

implementation

uses ModuloDati;

{$R *.DFM}

procedure TEmailConnectInfoForm.FormShow(Sender: TObject);
var xInfo, xFirma: string;
     xCRpos: integer;
begin
     Caption := '[S/24] - ' + Caption;
     // riempimento campi
     Q.SQL.text := 'select Nominativo,EmailConnectInfo,FirmaMail ' +
          'from Users where ID=' + IntToStr(xIDUser);
     Q.Open;
     EUtente.text := Q.FieldByName('Nominativo').asString;
     // host
     xInfo := Q.FieldByName('EmailConnectInfo').asString;
     if xInfo = '' then begin
          EHost.text := '';
          EPort.text := '';
          EUserID.text := '';
          EName.text := '';
          EAddress.text := '';
     end else begin
          //xCRpos:=pos(chr(13),xInfo);
          xCRpos := pos(';', xInfo);
          EHost.text := copy(xInfo, 1, xCRpos - 1);
          // port
          xinfo := copy(xInfo, xCRpos + 1, length(xInfo));
          //xCRpos:=pos(chr(13),xInfo);
          xCRpos := pos(';', xInfo);
          EPort.text := copy(xInfo, 1, xCRpos - 1);
          // userID
          xinfo := copy(xInfo, xCRpos + 1, length(xInfo));
          //xCRpos:=pos(chr(13),xInfo);
          xCRpos := pos(';', xInfo);
          EUserID.text := copy(xInfo, 1, xCRpos - 1);
          // name
          xinfo := copy(xInfo, xCRpos + 1, length(xInfo));
          //xCRpos:=pos(chr(13),xInfo);
          xCRpos := pos(';', xInfo);
          EName.text := copy(xInfo, 1, xCRpos - 1);
          // address
          xinfo := copy(xInfo, xCRpos + 1, length(xInfo));
          EAddress.text := xInfo;
     end;
     // firma
     xFirma := Q.FieldByName('FirmaMail').asString;
     Memo1.lines.Clear;
     while pos(chr(13), xFirma) > 0 do begin
          xCRpos := pos(chr(13), xFirma);
          memo1.lines.add(copy(xFirma, 1, xCRpos - 1));
          xFirma := copy(xFirma, xCRpos + 1, length(xFirma));
     end;
     // ultima riga
     //xFirma:=copy(xFirma, xCRpos+1, length(xFirma));
     memo1.lines.add(xFirma);
end;

end.
