unit TipoCommessa;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, TB97, ExtCtrls;

type
     TTipoCommessaForm = class(TForm)
          Label1: TLabel;
          ETipo: TEdit;
          Label2: TLabel;
          CBColore: TComboBox;
    Panel1: TPanel;
    BitBtn1: TToolbarButton97;
    BitBtn2: TToolbarButton97;
          procedure FormShow(Sender: TObject);
          procedure BitBtn1Click(Sender: TObject);
          procedure BitBtn2Click(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     TipoCommessaForm: TTipoCommessaForm;

implementation

uses Risorsa, Main;

{$R *.DFM}

procedure TTipoCommessaForm.FormShow(Sender: TObject);
begin
     //Grafica
     TipoCommessaForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     Caption := '[S/44] - ' + Caption;
end;

procedure TTipoCommessaForm.BitBtn1Click(Sender: TObject);
begin
     TipoCommessaForm.ModalResult := mrOk;
end;

procedure TTipoCommessaForm.BitBtn2Click(Sender: TObject);
begin
     TipoCommessaForm.ModalResult := mrCancel;
end;

end.

