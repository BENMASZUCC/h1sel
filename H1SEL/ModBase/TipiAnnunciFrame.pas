//[TONI20021809DEBUGOK]
unit TipiAnnunciFrame;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     TB97, Grids, DBGrids, RXDBCtrl, Db, DBTables, ExtCtrls, ADODB, U_ADOLinkCl,
     dxTL, dxDBCtrl, dxDBGrid, dxCntner;

type
     TFrmTipiAnnunci = class(TFrame)
          Panel1: TPanel;
          DsQTipiAnnunci: TDataSource;
          ToolbarButton971: TToolbarButton97;
          ToolbarButton972: TToolbarButton97;
          ToolbarButton973: TToolbarButton97;
          QTipiAnnunci: TADOLinkedQuery;  
          RxDBGrid1: TdxDBGrid;
          dxDBGrid1TipoAnnuncio: TdxDBGridColumn;
          procedure ToolbarButton971Click(Sender: TObject);
          procedure ToolbarButton972Click(Sender: TObject);
          procedure ToolbarButton973Click(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

implementation

uses ModuloDati;

{$R *.DFM}

//[/TONI\]

procedure TFrmTipiAnnunci.ToolbarButton971Click(Sender: TObject);
var xDic: string;
     myQTemp: TADOQuery;
begin
     xDic := '';
     if InputQuery('Nuovo tipo di annuncio', 'Tipo:', xDic) then
          with Data do begin
               //DB.BeginTrans; In ADO � BEGINTRANS
               myQTemp := TADOQuery.Create(self);
               myQTemp.Connection := Data.DB;
               Data.DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text := 'insert into Ann_TipiAnnuncio (TipoAnnuncio) ' +
                         'values (:xTipoAnnuncio:)';
                    Q1.ParamByName['xTipoAnnuncio'] := xDic;
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    QTipiAnnunci.Close;
                    QTipiAnnunci.Open;
               except
                    //DB.RollbackTrans;   In ADO � Roll Back trans
                    DB.RollBackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               end;
               QTemp.Free;
          end;
end;
//[/TONIEND\]

//[/TONI\]

procedure TFrmTipiAnnunci.ToolbarButton972Click(Sender: TObject);
var xDic: string;
begin

     //[ALBERTO 20020912]
     xDic := QTipiAnnunci.FieldByName('TipoAnnuncio').AsString;
     //[ALBERTO 20020912]FINE

     if not InputQuery('Modifica tipo di annuncio', 'Tipo:', xDic) then exit;
     with Data do begin
          //DB.BeginTrans;
          DB.BeginTrans;
          try
               Q1.Close;
               Q1.SQL.text := 'update Ann_TipiAnnuncio set TipoAnnuncio=:xTipoAnnuncio: ' +
                    'where ID=' + QTipiAnnunci.FieldbyName('ID').AsString;
               Q1.ParamByName['xTipoAnnuncio'] := xDic;
               Q1.ExecSQL;
               DB.CommitTrans;
               QTipiAnnunci.Close;
               QTipiAnnunci.Open;
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
          end;
     end;
end;
//[/TONIEND\]

procedure TFrmTipiAnnunci.ToolbarButton973Click(Sender: TObject);
begin
     Data.Q1.Close;
     Data.Q1.SQL.text := 'select count(*) Tot from Ann_Annunci where IDTipoAnnuncio=' + QTipiAnnunci.FieldByName('ID').asstring;
     Data.Q1.Open;
     if Data.Q1.FieldbyName('Tot').asInteger > 0 then begin
          MessageDlg('Ci sono annunci associati a questo tipo - IMPOSSIBILE CONTINUARE', mtError, [mbOK], 0);
          Data.Q1.Close;
          Exit;
     end;
     Data.Q1.Close;
     if MessageDlg('Sei sicuro di voler cancellare il ruolo selezionato ?', mtWarning, [mbNo, mbYes], 0) <> mrYes then exit;
     with Data do begin
          DB.BeginTrans;
          try
               Q1.Close;
               Q1.SQL.text := 'delete from Ann_TipiAnnuncio ' +
                    'where ID=' + QTipiAnnunci.FieldByName('ID').AsString;
               Q1.ExecSQL;
               DB.CommitTrans;
               QTipiAnnunci.Close;
               QTipiAnnunci.Open;
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
          end;
     end;

end;

end.

