unit SelEdiz;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     ExtCtrls,StdCtrls,Grids,DBGrids,Db,Buttons,DBTables,DtEdit97,
     Mask,ToolEdit,CurrEdit,FrmListinoAnnunci,TB97,RXSpin,ComCtrls,
     FrmContrattiAnnunci;

type
     TSelEdizForm=class(TForm)
          BitBtn1: TBitBtn;
          BitBtn2: TBitBtn;
          PanData: TPanel;
          Label3: TLabel;
          DEData: TDateEdit97;
          GroupBox1: TGroupBox;
          Label1: TLabel;
          Label4: TLabel;
          Label5: TLabel;
          Label6: TLabel;
          Label7: TLabel;
          Bevel1: TBevel;
          Bevel2: TBevel;
          ListinoLire: TRxCalcEdit;
          AnoiLire: TRxCalcEdit;
          AlClienteLire: TRxCalcEdit;
          ListinoEuro: TRxCalcEdit;
          AnoiEuro: TRxCalcEdit;
          AlClienteEuro: TRxCalcEdit;
          Panel1: TPanel;
          BCopiaValori: TToolbarButton97;
          ToolbarButton972: TToolbarButton97;
          Label2: TLabel;
          ScontoAnoiLire: TRxCalcEdit;
          ScontoAnoiEuro: TRxCalcEdit;
          Label8: TLabel;
          ScontoAlClienteLire: TRxCalcEdit;
          ScontoAlClienteEuro: TRxCalcEdit;
          GroupBox2: TGroupBox;
          SENumModuli: TRxSpinEdit;
          Label9: TLabel;
          GuadagnoLire: TRxCalcEdit;
          GuadagnoEuro: TRxCalcEdit;
          RGOpzioneListino: TRadioGroup;
          PCListini: TPageControl;
          TSListino: TTabSheet;
          TSContratti: TTabSheet;
          ContrattiAnnunciFrame1: TContrattiAnnunciFrame;
          ListinoAnnunciFrame1: TListinoAnnunciFrame;
          procedure FormShow(Sender: TObject);
          procedure BCopiaValoriClick(Sender: TObject);
          procedure ListinoLireChange(Sender: TObject);
          procedure AnoiLireChange(Sender: TObject);
          procedure AlClienteLireChange(Sender: TObject);
          procedure ToolbarButton972Click(Sender: TObject);
          procedure ScontoAnoiLireChange(Sender: TObject);
          procedure ScontoAlClienteLireChange(Sender: TObject);
          procedure PCListiniChanging(Sender: TObject; var AllowChange: Boolean);
          procedure ListinoEuroChange(Sender: TObject);
          procedure AnoiEuroChange(Sender: TObject);
          procedure AlClienteEuroChange(Sender: TObject);
     private
          { Private declarations }
     public
          xIDCliente: integer;
          xCliente: string;
     end;

var
     SelEdizForm: TSelEdizForm;

implementation

uses StoricoPrezziAnnunciCli,Main;

{$R *.DFM}

procedure TSelEdizForm.FormShow(Sender: TObject);
begin
     Caption:='[M/350] - '+caption;
     ListinoAnnunciFrame1.QEdizioni.Open;
     ListinoAnnunciFrame1.xIDCliente:=xIDCliente;
     ListinoAnnunciFrame1.QListinoEdiz.Open;
     ListinoAnnunciFrame1.QMinPrezziTestata.Open;
     ListinoAnnunciFrame1.QMinPrezziTot.Open;
     ContrattiAnnunciFrame1.QAnnContratti.Open;
     PCListini.ActivePage:=TSListino;
end;

procedure TSelEdizForm.BCopiaValoriClick(Sender: TObject);
begin
     if SENumModuli.Value=0 then begin
          MessageDlg('Il numero dei moduli deve essere maggiore di zero',mtError, [mbOK],0);
          exit;
     end;

//[ALBERTO 20020902]

     if PCListini.ActivePageIndex=0 then begin
          // posizionati sulla riga del listino a seconda del numero dei moduli selezionati
          if RGOpzioneListino.ItemIndex=0 then begin
               ListinoAnnunciFrame1.QListinoEdiz.First;
               while not ListinoAnnunciFrame1.QListinoEdiz.EOF do begin
                    if not((SENumModuli.Value>=Listino
                    AnnunciFrame1.QListinoEdiz.FieldByName('QtaDa').AsInteger)and
                         (SENumModuli.Value<=ListinoAnnunciFrame1.QListinoEdiz.FieldByName('QtaDa').AsInteger)) then
                         ListinoAnnunciFrame1.QListinoEdiz.Next
                    else break;
               end;
          end;
          ListinoLire.Value:=SENumModuli.Value*ListinoAnnunciFrame1.QListinoEdizPrezzoListinoLire.Value;
          AnoiLire.Value:=SENumModuli.Value*ListinoAnnunciFrame1.QListinoEdizPrezzoANoiLire.Value;
          // prezzo al cliente
          if ListinoAnnunciFrame1.QListinoEdizPrezzoQuestoClienteLire.asString<>'' then begin
           AlClienteLire.Value:=SENumModuli.Value*ListinoAnnunciFrame1.QListinoEdizPrezzoQuestoClienteLire.Value;
               AlClienteEuro.Value:=SENumModuli.Value*ListinoAnnunciFrame1.QListinoEdizPrezzoQuestoClienteEuro.Value;
          end else begin
               AlClienteLire.Value:=SENumModuli.Value*ListinoAnnunciFrame1.QListinoEdizPrezzoAlClienteLire.Value;
               AlClienteEuro.Value:=SENumModuli.Value*ListinoAnnunciFrame1.QListinoEdizPrezzoAlClienteEuro.Value;
          end;
          ListinoEuro.Value:=SENumModuli.Value*ListinoAnnunciFrame1.QListinoEdizPrezzoListinoEuro.Value;
          AnoiEuro.Value:=SENumModuli.Value*ListinoAnnunciFrame1.QListinoEdizPrezzoANoiEuro.Value;


          ScontoANoiLire.value:=0;
          ScontoANoiEuro.value:=0;
          ScontoAlClienteLire.value:=0;
          ScontoAlClienteEuro.value:=0;
          GuadagnoLire.Value:=AlClienteLire.Value-AnoiLire.Value;
          GuadagnoEuro.Value:=AlClienteEuro.Value-AnoiEuro.Value;
     end else begin
          // controllo numero moduli disponibili
          if ContrattiAnnunciFrame1.QAnnContrattiModuliUtilizzati.Value+SENumModuli.Value>ContrattiAnnunciFrame1.QAnnContrattiTotaleModuli.Value then
               MessageDlg('ATTENZIONE:  i moduli disponibili nel contratto non sono sufficienti',mtWarning, [mbOK],0);
          AnoiLire.Value:=SENumModuli.Value*ContrattiAnnunciFrame1.QAnnContrattiCostoSingoloLire.Value;
          AnoiEuro.Value:=SENumModuli.Value*ContrattiAnnunciFrame1.QAnnContrattiCostoSingoloEuro.Value;
          AlClienteLire.Value:=SENumModuli.Value*ContrattiAnnunciFrame1.QAnnContrattiCostoSingoloLire.Value;
          AlClienteEuro.Value:=SENumModuli.Value*ContrattiAnnunciFrame1.QAnnContrattiCostoSingoloEuro.Value;
          ScontoANoiLire.value:=0;
          ScontoANoiEuro.value:=0;
          ScontoAlClienteLire.value:=0;
          ScontoAlClienteEuro.value:=0;
          GuadagnoLire.Value:=0;
          GuadagnoEuro.Value:=0;
     end;
end;

procedure TSelEdizForm.ListinoLireChange(Sender: TObject);
begin
     if ListinoLire.Focused then
          ListinoEuro.Value:=ListinoLire.value/1936.27;
end;

procedure TSelEdizForm.AnoiLireChange(Sender: TObject);
begin
     //if AnoiLire.value<ListinoAnnunciFrame1.QListinoEdizPrezzoANoiLire.value then
     if AnoiLire.Focused then begin
          ScontoAnoiLire.value:=SENumModuli.value*ListinoAnnunciFrame1.QListinoEdizPrezzoANoiLire.value-AnoiLire.value;
          AnoiEuro.Value:=AnoiLire.value/1936.27;
          GuadagnoLire.Value:=AlClienteLire.Value-AnoiLire.Value;
          GuadagnoEuro.Value:=AlClienteEuro.Value-AnoiEuro.Value;
     end;
end;

procedure TSelEdizForm.AlClienteLireChange(Sender: TObject);
begin
     //if AlClienteLire.value<ListinoAnnunciFrame1.QListinoEdizPrezzoAlClienteLire.value then
     if AlClienteLire.Focused then begin
          ScontoAlClienteLire.value:=SENumModuli.value*ListinoAnnunciFrame1.QListinoEdizPrezzoAlClienteLire.value-AlClienteLire.value;
          AlClienteEuro.Value:=AlClienteLire.value/1936.27;
          GuadagnoLire.Value:=AlClienteLire.Value-AnoiLire.Value;
          GuadagnoEuro.Value:=AlClienteEuro.Value-AnoiEuro.Value;
     end;
end;

procedure TSelEdizForm.ToolbarButton972Click(Sender: TObject);
begin
     if not MainForm.CheckProfile('3507') then Exit;
     if not MainForm.CheckProfile('180') then Exit;
     StoricoPrezziAnnunciCliForm:=TStoricoPrezziAnnunciCliForm.create(self);
     StoricoPrezziAnnunciCliForm.xIDCliente:=xIDCliente;
     StoricoPrezziAnnunciCliForm.ECliente.text:=xCliente;
     StoricoPrezziAnnunciCliForm.ShowModal;
     StoricoPrezziAnnunciCliForm.Free;
end;

procedure TSelEdizForm.ScontoAnoiLireChange(Sender: TObject);
begin
     ScontoAnoiEuro.Value:=ScontoAnoiLire.value/1936.27;
end;

procedure TSelEdizForm.ScontoAlClienteLireChange(Sender: TObject);
begin
     ScontoAlClienteEuro.Value:=ScontoAlClienteLire.value/1936.27;
end;

procedure TSelEdizForm.PCListiniChanging(Sender: TObject; var AllowChange: Boolean);
begin
     if ContrattiAnnunciFrame1.QAnnContratti.IsEmpty then begin
          MessageDlg('Nessun contratto in essere',mtError, [mbOK],0);
          Abort;
     end;                                                                         
end;

procedure TSelEdizForm.ListinoEuroChange(Sender: TObject);
begin
     if ListinoEuro.Focused then
          ListinoLire.Value:=ListinoEuro.value*1936.27;
end;

procedure TSelEdizForm.AnoiEuroChange(Sender: TObject);
begin
     if AnoiEuro.Focused then begin
          ScontoAnoiEuro.value:=SENumModuli.value*ListinoAnnunciFrame1.QListinoEdizPrezzoANoiEuro.value-AnoiEuro.value;
          AnoiLire.Value:=AnoiEuro.value*1936.27;
          GuadagnoLire.Value:=AlClienteLire.Value-AnoiLire.Value;
          GuadagnoEuro.Value:=AlClienteEuro.Value-AnoiEuro.Value;
     end;
end;

procedure TSelEdizForm.AlClienteEuroChange(Sender: TObject);
begin
     if AlClienteEuro.Focused then begin
          ScontoAlClienteEuro.value:=SENumModuli.value*ListinoAnnunciFrame1.QListinoEdizPrezzoAlClienteEuro.value-AlClienteEuro.value;
          AlClienteLire.Value:=AlClienteEuro.value*1936.27;
          GuadagnoLire.Value:=AlClienteLire.Value-AnoiLire.Value;
          GuadagnoEuro.Value:=AlClienteEuro.Value-AnoiEuro.Value;
     end;
end;

//[ALBERTO 20020902]FINE

end.

