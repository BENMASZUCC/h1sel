unit ModStatoRic;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     DtEdit97, Grids, DBGrids, Db, DBTables, StdCtrls, Mask, DBCtrls,
     ExtCtrls, Buttons, ADODB, U_ADOLinkCl, TB97;

type
     TModStatoRicForm = class(TForm)
          Panel1: TPanel;
          Label1: TLabel;
          DBEdit1: TDBEdit;
          DBEdit5: TDBEdit;
          Label2: TLabel;
          DBEdit6: TDBEdit;
          Label3: TLabel;
          DsStatiRic: TDataSource;
          DBGrid1: TDBGrid;
          Label4: TLabel;
          Label5: TLabel;
          DEDallaData: TDateEdit97;
          Label6: TLabel;
          ENote: TEdit;
          Label7: TLabel;
          DEDataFine: TDateEdit97;
          TStatiRic: TADOLinkedTable;
          Panel2: TPanel;
          BitBtn1: TToolbarButton97;
          BAnnulla: TToolbarButton97;
          procedure BitBtn1Click(Sender: TObject);
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
          procedure FormShow(Sender: TObject);
          procedure BAnnullaClick(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     ModStatoRicForm: TModStatoRicForm;

implementation

uses ModuloDati, MDRicerche, Main;

{$R *.DFM}

procedure TModStatoRicForm.BitBtn1Click(Sender: TObject);
begin
     DEDallaData.SetFocus;
     ModStatoRicForm.ModalResult := mrOK;
     if (TStatiRic.FieldByName('StatoRic').AsString = 'conclusa') and (DEDataFine.Text = '') then begin
          //MessageDlg('Data fine procedura mancante', mtError, [mbOK], 0);
          DEDataFine.Date := Date;
          ModStatoRicForm.ModalResult := mrOK;
          //ModalResult := mrNone;
          //Abort;
     end;
     if (TStatiRic.FieldByName('StatoRic').AsString = 'attiva') and (DEDataFine.Text <> '') then begin
          ModalResult := mrNone;
          MEssageDlg('Se la ricerca � attiva non ci pu� essere data di fine procedura', mtError, [mbOK], 0);
          Abort;
     end;
end;

procedure TModStatoRicForm.FormClose(Sender: TObject; var Action: TCloseAction);
var x: integer;
begin
     // se la ricerca viene conclusa:
     // mettere in archivio tutti tranne chi � stato inserito ???
//     if TStatiRicStatoRic.Value='conclusa' then
//        messageDlg('S T O P:  riflettete su cosa volete che il programma faccia automaticamente'+chr(13)+
//                   'a ricerca conclusa (e poi, ordinatamente e con precisione, me lo dite ...!!!)'+chr(13)+
//                   'PER ORA NON FA NIENTE !',mtWarning,[mbOK],0);
end;

procedure TModStatoRicForm.FormShow(Sender: TObject);
begin
     Caption := '[M/23] - ' + caption;
     TStatiRic.Open;
     ModStatoRicForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     DEDataFine.Date := Date;
     DEDataFine.Text := DatetoStr(Date);
end;

procedure TModStatoRicForm.BAnnullaClick(Sender: TObject);
begin
     ModStatoRicForm.ModalResult := mrCancel;
end;

end.

