�
 TRIEPTIMINGFORM 0  TPF0TRiepTimingFormRiepTimingFormLeft� TopnBorderStylebsDialogCaption!Riepilogo tempi Utente - RicercheClientHeight�ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TLabelLabel1LeftTopWidth$HeightCaptionCClassifica clienti-ricerche in ordine di numero secondi di attivit�  TLabelLabel2Left� Top�WidthYHeightCaptionTotale secondi:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TDBGridDBGrid1LeftTopWidthZHeightw
DataSourceDsQClassificaTabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style OnTitleClickDBGrid1TitleClickColumnsExpanded	FieldNameDescrizioneTitle.CaptionClienteWidth� Visible	 Expanded	FieldNameProgressivoTitle.CaptionRif.RicercaWidth=Visible	 Expanded	FieldNameTotTitle.AlignmenttaRightJustifyTitle.Caption
N� secondiVisible	    TDBEditDBEdit1Left
Top�WidthAHeight	DataFieldTotale
DataSourceDsQTotTempoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder  TPanelPanel1LefthTopWidthYHeight;
BevelOuterbvNoneTabOrder TToolbarButton97BOKLeftTopWidthPHeight&CaptionOK
Glyph.Data
z  v  BMv      6  (               @                     ��� � � @�h ��� TQI �? v�v ��� ('' yґ E�^ qm^ ��� $�M ��� _�w \� ��� ��� �ټ \�d =<9 v�� 4�N :�] |{s T�s _^^ ��x ��� ,�X �Ʀ b�s jɃ ��� I�u ��� O�j ��� i|m ��� ��� *�M ��� ��� ��� 421 D�f S�` �� 7�^ JIC 9�U r� |�� _wc nnm U�l k� ��� ��� 0�W J�p ��� f�l `�l wċ %�G � �° T�q ��� e�y ��p ��� V�y qЊ [XN [�y ��� ��� M�n �Ė ��� 2�T urq d�} =�[ A�` ��� ��� ��� ��� ��� ��� F�p ��� H�c �Ǔ ��� m�v n� dЁ N�u C�h x�� V�o *�R �� ��� ?�b ~~{ o�p 2�P ��� �̹ 1�Z ��� H�_ ��� ;�c Ļ� g�o w͎ ��� U�z O�g ��� ]�r r�� ��� ��� \�o ��� c`a 0�V M�p F�f [�w �ȶ ��� 8�Y I�[ ��� ��u ��� sɉ 3�] ��� Y�o ��� ��� 3�Z �á mą i�z @�j Q�u [YY I�m ��� ��� ��� ��� (�Q ��� ?�c Z�~ ��� e� {�z b�s ��� IIH B�a yvu ��� l͆ ��� p�� t�� �ϼ Ŀ� +�V <�X J�k ��� |�� ��� h�~ <�a I�s 11/ ��� ��� E�n O�r k�m ��} ��� _�y e�w ��� ;�d S�y cxf ��� ��� |̑ ɿ� 7�Z =�] e|j ��� ��� Q�l }�� zxp ��� {�� ��� ��� &�K +�O L�a ��� ��� Y�t �Ɵ b�x ��� ��� ��� U�v ��� S�n \�v |Ǐ ��� ��� �˽ ��� ��� VTK ��� �ɼ ƽ� 7�_ A�j B�] ��� W�| ��� ��� ��� .9��		��9.�/Pz��z�/�.�V}��uu���}���4��>?����?�>E_4p�����?ih~�L�����=<N�uy�0�`�h��L�u�%N�s��3yo�n��$��?L���d�<+�3�nnn�`���ŧ��s��l���nn�nףR�yy�Rl@���vB�U­n�HQ�'���y3�R�ґJ#5����Q\�''���3�0�#J���ח�uy�\�''����ӯD��t��G�˥333uޤ'���l�&rF2��wk��O�>��3��㗗�XY�^���{�W"MgR�>>,��-�b1�F��澛�|


M���-���F���!I;�C����|���Z]��А���q��j�cccc�C�;�:����[�A��*SSSS*��;I��%���a�A��������fʜЕ���_�� ����m68���x}=(7��Te�=}x���K�.))ۍK����۱��.۱�OnClickBOKClick   TDataSourceDsQClassificaDataSetQClassificaLeft(Topp  TDataSourceDsQTotTempoDataSet	QTotTempoLeftxTop�   TADOLinkedQueryQClassifica
ConnectionData.DB
CursorTypectStatic
ParametersName
xIDutente:
AttributespaSigned DataType	ftInteger	Precision
SizeValue   SQL.Strings.select Progressivo,Descrizione,sum(TotSec) Tot(from TimingRic,EBC_Ricerche,EBC_Clienti )where TimingRic.IDRicerca=EBC_Ricerche.ID-    and EBC_Ricerche.IDCliente=EBC_Clienti.ID&    and TimingRic.IDUtente=:xIDutente: group by Progressivo,Descrizioneorder by Tot desc 	UseFilterLeft(TopP  TADOLinkedQuery	QTotTempo
ConnectionData.DB
ParametersName
xIDUtente:
AttributespaSigned DataType	ftInteger	Precision
SizeValue   SQL.Stringsselect sum(TotSec) Totalefrom TimingRic where IDUtente=:xIDUtente: 	UseFilterLeftxToph   