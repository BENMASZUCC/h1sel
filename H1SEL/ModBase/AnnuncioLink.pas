unit AnnuncioLink;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls;

type
     TAnnuncioLinkForm = class(TForm)
          eLink: TEdit;
          bCopia: TButton;
          procedure bCopiaClick(Sender: TObject);
          procedure FormShow(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     AnnuncioLinkForm: TAnnuncioLinkForm;

implementation

uses ModuloDati, ModuloDati4;

{$R *.DFM}

procedure TAnnuncioLinkForm.bCopiaClick(Sender: TObject);
begin
     eLink.CopyToClipboard;
     Close;
end;

procedure TAnnuncioLinkForm.FormShow(Sender: TObject);
var xLink:string;
begin
     // SINTASSI LINK:
     // vecchio = http://europa/h1form/default.asp?page=form.asp&IDCliente=1&Password=1234&IDAnnEdizData=
     // nuovo = http://85.18.201.89:7080/H1SelWebCand2_Pubb/MainHR.aspx?Pag=Sel_NuovoCV&Params=#;&Tipo=Griglia

     xLink:=Data.Global.FieldByName('LinkForm').AsString;
     if pos('IDAnnEdizData',xLink)>0 then
        eLink.Text := StringReplace(xLink,'IDAnnEdizData',DataAnnunci.QAnnEdizDataXAnn.FieldByName('ID').AsString,[rfReplaceAll])
        //xLink + DataAnnunci.QAnnEdizDataXAnn.FieldByName('ID').AsString
     else
        eLink.Text := StringReplace(xLink,'#',DataAnnunci.QAnnEdizDataXAnn.FieldByName('IDAnnuncio').AsString,[rfReplaceAll]);
end;

end.
