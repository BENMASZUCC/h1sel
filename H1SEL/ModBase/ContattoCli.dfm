object ContattoCliForm: TContattoCliForm
  Left = 379
  Top = 261
  BorderStyle = bsDialog
  Caption = 'Riferimento interno'
  ClientHeight = 341
  ClientWidth = 445
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object SpeedButton1: TSpeedButton
    Left = 256
    Top = 56
    Width = 20
    Height = 20
    Glyph.Data = {
      AA030000424DAA03000000000000360000002800000011000000110000000100
      1800000000007403000000000000000000000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FF00FFFFFFFFFFFFFFFFFFDEDFDEBDAEA5EFDFD6F7EFE7F7EFE7F7EFE7E7D7CE
      7B696B525152DEDFDEFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFF7F7EF
      E7DEF7F7F7E7DFDECEB6A5DEB6A5E7CFBDEFEFEFF7F7F7DECFC6393031CECFCE
      FFFFFFFFFFFFFFFFFF00FFFFFFF7F7EFF7F7EFF7F7EFD69E7BC66131C6714ADE
      B6A5C66131C66131DEB69CF7F7F7EFDFD6393831FFFFFFFFFFFFFFFFFF00FFFF
      FFF7E7DEF7F7F7CE795AC66129CE6129C69E84FFFFFFDE966BCE6131C65929D6
      9E7BF7F7F7C6B6B5848684FFFFFFFFFFFF00F7EFE7FFFFFFDEAE8CCE6129CE61
      31CE6131CE6939DE8E63CE6129CE6131CE6131C65929E7C7B5F7EFEF524942FF
      FFFFFFFFFF00F7DFD6FFF7F7CE6939CE6931CE6131CE6131CE8663F7E7D6D671
      42CE6131CE6131CE6131CE7952F7FFFFA5968CADAEADFFFFFF00FFEFEFF7DFCE
      CE6131D66931CE6131CE6131C6795AFFFFFFE7A684CE6129CE6131CE6931CE69
      31F7F7EFDECFBD9C9E9CFFFFFF00FFEFEFF7D7C6D66931D66931CE6131CE6131
      CE6131D6AE9CFFFFFFE79E73CE6129CE6931CE6931F7EFEFDECFC69C9E9CFFFF
      FF00FFEFEFFFE7DEE77142DE6939CE6129CE6129CE6131CE6129E7C7BDFFFFF7
      D67139D66939D67142FFF7F7D6C7BDB5B6B5FFFFFF00F7E7DEFFFFFFF79E6BEF
      7942D68E6BEFDFCEDE7952CE6129DEA684FFFFFFDE8E63DE7139E79E73FFFFFF
      A5968CFFFFFFFFFFFF00F7EFE7FFFFFFFFE7CEFF9E63E78E63EFEFEFFFEFE7EF
      BEA5FFF7EFF7EFEFE78652EF8652FFEFDEFFF7EFC6BEBDFFFFFFFFFFFF00FFFF
      FFF7E7DEFFFFFFFFE7C6FFBE84EFBE94EFE7DEE7E7E7EFE7DEFFB68CFF9E6BFF
      DFC6FFFFFFDECFC6FFFFFFFFFFFFFFFFFF00FFFFFFF7F7EFF7E7E7FFFFFFFFFF
      EFFFF7CEFFE7B5FFD7A5FFD79CFFDFB5FFF7EFFFFFFFEFDFDEEFEFEFFFFFFFFF
      FFFFFFFFFF00FFFFFFFFFFFFF7F7EFF7E7D6FFFFF7FFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFF7F7F7EFE7F7F7EFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF
      FFFFFFFFFFFFF7EFE7F7E7DEFFEFE7FFEFEFFFEFE7EFE7DEFFF7F7FFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFF00}
    OnClick = SpeedButton1Click
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 445
    Height = 48
    Align = alTop
    BevelOuter = bvLowered
    Enabled = False
    TabOrder = 0
    object Label1: TLabel
      Left = 6
      Top = 5
      Width = 35
      Height = 13
      Caption = 'Cliente:'
    end
    object DBEDenomCliente: TDBEdit
      Left = 6
      Top = 20
      Width = 265
      Height = 21
      Color = clAqua
      DataField = 'Descrizione'
      DataSource = Data2.dsEBCClienti
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 81
    Width = 445
    Height = 260
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Label5: TLabel
      Left = 21
      Top = 172
      Width = 23
      Height = 13
      Caption = 'Note'
    end
    object Label6: TLabel
      Left = 17
      Top = 140
      Width = 28
      Height = 13
      Caption = 'E-mail'
    end
    object Label4: TLabel
      Left = 27
      Top = 111
      Width = 17
      Height = 13
      Caption = 'Fax'
    end
    object Label3: TLabel
      Left = 3
      Top = 79
      Width = 42
      Height = 13
      Caption = 'Telefono'
    end
    object Label2: TLabel
      Left = 1
      Top = 47
      Width = 45
      Height = 13
      Caption = 'Cognome'
    end
    object Label7: TLabel
      Left = 20
      Top = 16
      Width = 26
      Height = 13
      Caption = 'Titolo'
    end
    object Label8: TLabel
      Left = 191
      Top = 79
      Width = 40
      Height = 13
      Caption = 'Cellulare'
    end
    object Label9: TLabel
      Left = 202
      Top = 47
      Width = 28
      Height = 13
      Caption = 'Nome'
    end
    object Label10: TLabel
      Left = 14
      Top = 202
      Width = 30
      Height = 13
      Caption = 'Uscito'
    end
    object ENote: TEdit
      Left = 52
      Top = 164
      Width = 320
      Height = 21
      MaxLength = 200
      TabOrder = 7
    end
    object EEmail: TEdit
      Left = 52
      Top = 135
      Width = 197
      Height = 21
      MaxLength = 50
      PopupMenu = PMCopia
      TabOrder = 6
    end
    object EFax: TEdit
      Left = 52
      Top = 107
      Width = 133
      Height = 21
      MaxLength = 30
      TabOrder = 5
    end
    object ETelefono: TEdit
      Left = 52
      Top = 76
      Width = 132
      Height = 21
      MaxLength = 30
      TabOrder = 3
    end
    object ETitolo: TEdit
      Left = 52
      Top = 11
      Width = 101
      Height = 21
      MaxLength = 20
      TabOrder = 2
    end
    object EContattoCognome: TEdit
      Left = 52
      Top = 43
      Width = 133
      Height = 21
      MaxLength = 50
      TabOrder = 0
    end
    object EContattoNome: TEdit
      Left = 236
      Top = 43
      Width = 147
      Height = 21
      MaxLength = 50
      TabOrder = 1
    end
    object ECellulare: TEdit
      Left = 236
      Top = 75
      Width = 147
      Height = 21
      MaxLength = 30
      TabOrder = 4
    end
    object Panel2: TPanel
      Left = 99
      Top = 218
      Width = 178
      Height = 41
      BevelOuter = bvNone
      TabOrder = 8
      object BitBtn1: TToolbarButton97
        Left = 4
        Top = 3
        Width = 80
        Height = 35
        Caption = 'OK'
        Glyph.Data = {
          76060000424D7606000000000000360400002800000018000000180000000100
          080000000000400200000000000000000000000100000000000000000000FFFF
          FF00FF00FF0040D86800B1AEAC00545149001D973F0076967600C4F1D1002827
          270079D2910045A35E00716D5E00CFC7C00024C04D009B958D005FBA77005CE6
          8000E4EFE70090B69900B1D9BC005C8864003D3C390076B3870034934E003ABD
          5D007C7B730054D073005F5E5E00938F7800AAA695002CD358009BC6A60062A4
          73006AC98300E0D5C70049EC7500C1BBB7004FB96A00D5EEDB00697C6D008187
          820081C091002AAF4D00B5E9C300F1F7F2009D9D9C003432310044C666005397
          60009F9B7F0037CC5E004A49430039A0550072A67F007C9B83005F7763006E6E
          6D0055A66C006BB87F00A7A6A5008B8C880030C257004ADC700086B48800668B
          6C0060B06C0077C48B00259F470092C29C00C8C2B00054C27100E7FAEC0065B0
          790085827000C5C3C20056DC790071D08A005B584E005BCA79009A939900DCF3
          E3004DCC6E0086C4960080AC8C0032B054007572710064C17D003DA95B0041B4
          6000C9E7D100A39F9200B9F1C800F8FBF900D9D1C000BBB5B50046E67000B1AE
          A20048AB630080C793008DBB91006D9276006EAF7F0064D081004EE3750043CF
          680078BB890056B66F002ABD52007FB68D00CEECD6003FC262007E7E7B006F88
          7000329F500094908800D0CCB90031CC5A00C1B9AE00489B5F00A7A49E003BD1
          6300C4BBC100679F6F0077CD8E00CCC7C60055E57A004FA66700888886005DB5
          720072C0870095BD9700ACABA8005CA76F008CC09A006360610030BD56004DD1
          700046BF66005BC37700CBC8B600BBB8B90038B7590049A85B00AFAA98008482
          7500A09A8D0073C9890033D05D00B5B2A80059B16F00D9EEDF0087BB940033C6
          5A0096C3A1006DC4850069AB7A0040DE6A0051D775005B59590049D76D009B98
          80008F8D8500ECF6EF00E5F4E90028C35100B8AFB1003FCB63005AE07E009C97
          880065CA7F007B967A0062B47300D3EAD9004949480042BA610079767500A2A1
          9F006CCD8600A2A1990070B5820074B98600D4CFBC00C4BFB9002BC756003CA4
          58004AC66B008DB68E007CC08D00BFBDBE0068BD7E003CC6610049E773003131
          2F00E0F2E500C9C1BF0045E06E004FD472006B866D0082817D0094BE9F005FBF
          790065A97700C1EDCD003BD6640053E1790063786600B1AFA700ABA9A4007CCC
          9100C9BFB40037BC5A003DB45D00657C6A009BC1A600D1EFD90051B56C007DBA
          8E007A7870009B9B9B007BB88B00D9F2DF00B8ECC60026B04B002BB34F004C9A
          61008F8F8C00E8F5EB0059C3740093C69F0062B478008CB6970085BE9300A5A4
          A20055D476009D969A0053B96E005CBE76007CC78F00DEEEE300CAEDD300D3CB
          BD00BFB6B400B7B3B20056544B00DCD3C200CFC9BC00C6BDBD0037CF5F0041DA
          6A0042A45D00ADADAC0057E17C00A9A8A800A4A0950086868300020202020202
          022E39AEC10909C1AE392E020202020202020202020202B11C2F1C507ABDBD7A
          EB872F9F2E0202020202020202028016567DE5E488757588E4E57DB016FF0202
          0202020202C734A69A3E033FCDFCFCCD3FCC3E455F34700202020202E205F7AC
          94A03F69687E1111A84CA09481C3F43D0202023C4EF2AC7579CC30E31B60C068
          CDFC4CC57596254EE902021A7383B833796FC26EA3EA24C0C43F4C9E89B8640F
          DA023C0C0D2B993319C26E6E6EA31B60C4F9039EC5A7DF0D0CE973A9BB6C998E
          C26E6ED76ED7A3529D03797989526C4092A29176428855C2AD6EDD4851D727A3
          BACC7933A752198FD2914A23358AA4ADAD51081F5CC22727A38AF8339930AF06
          234A1DB6B98BD797C2087579945CA42727A3AF9999D3AF448C1DA174FAEC47D7
          CBA533333375DEA42797A3D46CE026724632A1F5776BEDB24FBF3E999933B8DE
          E39797A35859D8185EA1A9F67B961057224D6752BF3E3E0E2CE3EF122D966231
          F1A9B346ABE6E6BE9B937C0A0A0A4DB2AAF02D12A3ADFA0746FE841ECF21493B
          8243EED1D1D17C939BC95A5DAD0BE1D0908402FEF671B5B46ABC63636363EE43
          823B81147F3AC6F6FE0202FB5BB741D9DC2A535353532ABCB53B4985851525FE
          FB0202028461CF41869886868686E8D91766CA9C15D095D00202020202D0CF5F
          CEE7209A9A86986D17173638F395D00202020202020284787D3D283713D6C854
          65D53D7D7884020202020202020202FBB1044B8D2E2929DB8D4B04B1FB020202
          020202020202020202FBB1DBB1FDFD2EDBB18402020202020202}
        OnClick = BitBtn1Click
      end
      object BAnnulla: TToolbarButton97
        Left = 93
        Top = 3
        Width = 80
        Height = 35
        Caption = 'Annulla'
        Glyph.Data = {
          76060000424D7606000000000000360400002800000018000000180000000100
          080000000000400200000000000000000000000100000000000000000000FFFF
          FF00FF00FF0082817D001A1AA9005A65E500B1B2EC003D3C3A00DFDCC100B1AE
          A2004F4FA3008787C20062615D003437D300D8D9F6006566BF00626284009F9B
          7E00C3C3BB007878A6007E81E300282827009F9FD6003736B1004E4EC500C4C4
          DE009D9C9C00ECECF800535249007474CE006D6D6D002525BD004C53DC008F8F
          8C009090E2006362A700938E77006363D1004346B4005858B8003F47CE005656
          9500C1BFA9009191CC00AAA59300D4D0BA00A9ACDC00333330006D6C8D008484
          D3009D9FE7007C7CBB00D0D2EB00BFBFED00484848007B7A7300706E5D002D2E
          C900282AAE006A6CDC00616197005A5BCA00E1E1F0009391BF00B6B6AE006F6F
          B70085816F0073729A00565AD700A4A3A100CDCCC400B8B8E2004343C3008080
          8A008B89B6007C7CC800A19D8A003338C5004D4DB5003030A700C9C5AD007373
          C10088878600545EE2005151D1006969A0002D32B500474BD60096938400BBBB
          B600898ED500ADACAA00343BBA005F5FB300595A57006060C0004949AB00777C
          D4009898CF00C7C8E700ABABD2003B3ED1008482B600E4E5F7004A4ACC005957
          4D009A9A92005656C0008A8ACB005C5C8000D6D7EE00BDBDBF00646DE300C1C0
          B200A3A096006E6ECB00292DBD002323B000AFAB9700DDDDEA008383C800CFCB
          BB004146D9005157C8004040C8007171D5002D2FCE0075746D003F3FB4004949
          BA004E4EBD00D3D3F5009C988500393ACA00BCBBAE00ABAAA6005961E0006C6C
          BD003333CD00DDDEF1009696CA007E7ED200D9D5BB002929C9008E8D87008D8D
          DC00898A8E008C8CC400A3A5D7009B978000BFC0E100A7A49C0068678D007877
          9A00A1A09E009494DE004244D300B5B3A7004B54C700B4B6EE006068E1006E6E
          C200F0F1F800C8C7BC004447C9004E4FD500545ADD00777BCD004A4942005B5B
          BF008B8ABE00E7E7F300838174006262CB00B5B5E300424AD3005151CA005F5F
          BC007C7CC300CAC8C500393ED5009397D400848483004645AF0066668500C6C5
          C0003636BB0030302F00E1E1F600DCD9BD00CECBC0002F2FB100CAC7B7005C5C
          5B00C5C3B6003939B5007E7E7B007777D600EBEBF3004546D0004E56DF004949
          BF00B1AFAB005151C2005E5EC7008686CE007A7AC7003434C700BBBDE2005D64
          E0005355D800565DDD00DADBF0005353C6007E7ECC00D0CDB8003638CE009D9D
          94009B9BD3006161B9008C8CCD007B7BD000B8B7AA00454ADA00B1AFA6005A5A
          CE005E5BB500383BD100383DCE00414BCB009494CE008484C100E9E9F8003C3B
          3600C4C5E7003335B2003535C3003538C8004747B6005959BB005B5ECA006262
          D5006A6AC1007272C500E2E3F200E0E0EE00D8D8F10056544B002929BC002B2B
          C0003031C90079786E00A4A09300606081009C998900A7A6A400020202020202
          021A1E36BB1515BB361E1A0202020202020202020202029AC12F0C6A126F6F12
          D90C2F5E1A02020202020202020252077FB3E6F04DE4E44D3D2BB337E9B60202
          020202020203A8DE8CEDDFA688D1D188C87AED2B86A8C40202020202211CC25F
          B420A6D388A07070A0D1A474A9A3F790020202FF6986B10D5626AFC853535388
          D14494960DEF716945020237583F7EA46EA261C85305055320A7ABA2D27EAAFE
          FB02FF38BEF88A328BD434AF530505539E8B8B9FA59C1FBE384590844A8F8A3B
          676E3E5A205353E5638B6757A5A539664C90AC2AE2CF8A7E06D46EF47BC8282E
          8B6714E39CA585B750AC42081768FAD8E30ED4D4345CB5F48B9FE365D8C77C04
          0842242DC3B0688AD8A567D4D434F4D483B465D88AED4875C02495D780D5E154
          8A8A448B6E6E6E6E9CE3D88AFAF918BFC21195BDB7D53D25F1853AD06E6E6EEA
          EBFA8A39EC48CB4F8E95FED7236BCCAD27E73E8B6EBCF66ED427BAB0D5C9810A
          2D847250995F5FA119C6F53E1B22351B3EC664608281EE4350725B2CE05D5FAE
          ABF5F4E891C57D9B67F4F57789EE6087768702727930F34B1B1BF6318D8DDD1D
          73AE1BAB474E9879FC02025BFCC23CCE4B16D6CDCDCDD6CEF30FF2DA0F2971FC
          5B0202028709E03C6C786CDCDCCD78CE5189DBDB29879D87020202020287E086
          FD0B62E62B6C78B25151416D409D87020202020202025B974692B81393622B33
          5510924697870202020202020202025B9ACAB9591A49491A59B9CA9A5B020202
          0202020202020202025B451A9A87871A1A9A5B02020202020202}
        OnClick = BAnnullaClick
      end
    end
    object CBUscito: TCheckBox
      Left = 52
      Top = 199
      Width = 14
      Height = 17
      TabOrder = 9
    end
  end
  object Panel4: TPanel
    Left = 0
    Top = 48
    Width = 445
    Height = 33
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object BSelezAnag: TToolbarButton97
      Left = 8
      Top = 3
      Width = 113
      Height = 30
      Caption = 'Selez. Anagrafica'
      Glyph.Data = {
        76060000424D7606000000000000360400002800000018000000180000000100
        080000000000400200000000000000000000000100000000000000000000FFFF
        FF00FF00FF00CB872F0004CC360061B0D50053555500B2A4930052B46B00A8DE
        F8000B6C23004C2E0700905B1600DDD0BE007477AE00877C6D003E87B000E3B7
        7F00959FCB002AA8490089C6990039383600A2763D0083C8E700684A2000647F
        89000B9C3100DBDFE3007A9A9F007361490096908900626D6E0072BA860098A0
        A5004B9BC400B273230053473800683E0600C5A67E006092A700C0D8E400A18B
        7000AEAEAD005A547400F2F0ED0048371E00332923008CB6C8004F7C950008B4
        33007C4E1200C1E6F7006EBEE20097D3EF00392407003BA75600907147006864
        5C00E4DACF00A0671C007B5E360062B178007E705F006EA9C700A79680006157
        470016A93B006C8B9600A67F4D0076B5D400BDB1A40082AAC00091877B00868D
        8F005CA6C8007A726F00593709004991B8009D9B98005B849B0044B05F00BC7D
        2900473F3600A7A5A500B5E0F3005F4219008CD4F700687A7D00E7E9EA000B60
        2000A79B8C00D4D6DD007497AA007AC1E20087521000514B4300568EAC0005C1
        3300778C99006BB5D70009A7310088817A0052432E0095949300A0D8F40057AB
        6E00706455008CCCEA003D2A0E007ABD8B0084766400C5802700A47334005A53
        4C009F917F007ABCDA005F50410064879400442A06007E7A7000413832006D43
        0A00665C4C00635F570073B0CE005F6868006673720095CEE90039342F009F7B
        4D0082521600594C38005DB1720055809800B7771F0072B7DA00939AC600AD9E
        8900A09E9D00998E82005232080013A437004A433B004F98BF006C6150007770
        6900ABABA900A2A2A20067B47B00FAFBFF0006BA34004F433500AB9F8F00887A
        6700AFDEF30007AD3100F5F2F200EDEDEB00623C09009A9791000B652100D9CC
        BC003E270800A5DBF300493419006D4D210075BCDD00A06A210066B0D200683F
        0B007F74650081C5E3006FB2D500765C360058B1700053350C005E390A00687E
        86009ED6F0009D641B009797960098948E0098D5F30004C834005C534800A9A8
        A80084511000537B9400A69784008B847A0095D1EC0089CAE8007EC2E2005C52
        43009D9C9B00A2938100473F3A00AF74250051463B00AAA7A400635A4D008373
        62005F533F0055330A0084541400A2691E003D280B00663D0900885511008252
        12008BB3C900E4D9CC000B68220008B0320083C9EA000AA4310066B1D5006F64
        5200B6E2F5000B62210006BD34005C38090077BEDE00A3681C009272490065B3
        7900A1927D00817A700057360900805214005E554800A4A2A0009B9B9900A7DD
        F60007B8330008B233006D41090082C6E50073BBDC0049403700524942004B2E
        09005032090006BF3300643C090009A93200684009006A410B0079BFE2006FBC
        E2009D661B0078BD8A006E64560089CAEA000B6A23005C390900020202022A91
        A476CE97248341E3E702FE0A0A0A0202020202020202E6A2F49EDDF18CB0E40B
        A24BFEF5310A020202020202020236CBF6CFF4F2DDDDFFE4252BD431610A0202
        0202020202026CDDECECF64CF4A5ADE0380ED49604FE02020202020202022DF7
        BABA79A9A93CDBDBA0A0D4EAB7FEFE0A0A0A020202026632B3B3D1E5A981DB3D
        69378DEBDCF3DCEBD70A02020202CA8223CDA7CCD044DB6D948450429BD5D564
        1A0A020202029FAFFA51C53BDF1659DBDBDBDB0813A0D4D4D4FE02020202021E
        550CDF038698589D885BDBE1AEA002020202020202020202B5185E6F26959C2C
        120DDB2094A0020202020202020202022ABD25700128D2471B115914FBDB0202
        020202020202020202B91D29B6AC108F221C5959DBDB02020202020202020202
        02029243564D63DEA6F8B1C72A02020202020202020202020202025CF9D8EEAB
        C05D6021020202020202020202020202020249156305ED6BFD173F6202020202
        02020202020202020202C427344A35A3B2BF7C4F020202020202020202020202
        02B92EA87519E9DA9ABE458502020202020202020202020202E880C674570933
        547F7330020202020202020202020202026778B86AC92F0968D687BB02020202
        020202020202020202B4526A6E407D1F7BC87E06020202020202020202020202
        0293EF3EE2AAF0FC0F72C371020202020202020202020202022A5F9948658B5A
        0789C18A0202020202020202020202020202C28E463AD3A1BC7A4E0202020202
        02020202020202020202024E397AD99077530202020202020202}
      WordWrap = True
      OnClick = BSelezAnagClick
    end
  end
  object PMCopia: TPopupMenu
    Left = 272
    Top = 209
    object CopiaEmail: TMenuItem
      Caption = 'Copia Email'
      OnClick = CopiaEmailClick
    end
    object InviaMail: TMenuItem
      Caption = 'Invia Mail'
      OnClick = InviaMailClick
    end
  end
end
