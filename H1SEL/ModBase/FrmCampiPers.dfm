object CampiPersFrame: TCampiPersFrame
  Left = 0
  Top = 0
  Width = 525
  Height = 448
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  ParentFont = False
  TabOrder = 0
  object PanTop: TPanel
    Left = 0
    Top = 0
    Width = 525
    Height = 40
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object BOK: TToolbarButton97
      Left = 2
      Top = 1
      Width = 82
      Height = 37
      Caption = 'Conferma Modifica'
      WordWrap = True
      OnClick = BOKClick
    end
    object BCan: TToolbarButton97
      Left = 84
      Top = 1
      Width = 82
      Height = 37
      Caption = 'Annulla modifiche'
      WordWrap = True
      OnClick = BCanClick
    end
    object ToolbarButton971: TToolbarButton97
      Left = 200
      Top = 1
      Width = 82
      Height = 37
      Caption = 'Definisci campi'
      Glyph.Data = {
        16050000424D16050000000000003604000028000000100000000E0000000100
        080000000000E0000000130B0000130B00000001000000000000102830006048
        3000103040002038400020507000407840004058700040607000506070007167
        630040805000508860006098700050A8600050B0600050B87000A0A075006078
        80002088B00070A8800060C080002098D00020A0D00030A8D00020B0E00030B0
        E00030B8F00060B0D00040C0F00050C8F00060C0E00060D0F00070D8F0008088
        9000B0989000B0A09000B7A59A008090A0008098A00090A0A00090A0B00090A8
        B000B0A0A000B0A8A000C0A09000C0A89000E0A89000E0B09000C0A8A000C0B0
        A000D0B0A000D0B8A000E0B8A00090D0A000E0C0A000F0C8B000B0B8C00080D0
        E00080E0F000C0C8C000C0C8D000E0D2C300E0D3C500E0D4C700F0D0C000F0E0
        D000D8E9EC00E0E0E000F0E8E000FFF0E000E0E8F000FFF8F000FFFFF000FFFF
        FF00000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000000000000000000000000000000000002C0101010101
        010101010101424242422D47454441403736342F2E01424242423048302A4423
        222222222F01424242423048484745444140373634014242424230482B2A4723
        2222222236014242424231484848484745444140370142424242314848484848
        473B07414001424242423149461143483C0817032A014242100A324929390838
        081E081A00020009050D334946293A0839081F07191512040B0E33333331293A
        083A061F1D1C18160C0F424242423F293A063A201F1D1C1A131442424242423E
        293A3A3A201F1B2142354242424242423D292827262521244242}
      WordWrap = True
      OnClick = ToolbarButton971Click
    end
    object ToolbarButton972: TToolbarButton97
      Left = 282
      Top = 1
      Width = 82
      Height = 37
      Caption = 'Elimina campi'
      Glyph.Data = {
        36050000424D3605000000000000360400002800000010000000100000000100
        080000000000000100000000000000000000000100000000000000000000FFFF
        FF00FF00FF00858279002D2ECD00A9A9E60044433B006D6EC900B0AE9B00DADA
        F4005E5E99005F5F5E008F8DB2004952DD008588E4004343AD002C2C27007878
        9E00C3C4E3009E9883009999D1005D68E3005656C200A3A0B2008282C7004343
        C700ECECF700BDBBAB009F9D9B00706D5D006B6BB400777570008E8D87009B9C
        E8008180B300AEAEAE00B6B8E1003838350084849F008C8ECD005C61D4009591
        77009292A3003838C300949494006060BD004347D5004D4FCB00C1C1F1008085
        D500CCCEE8006E6E9E004848B9009090E1005859B400827F6C007979C200535F
        E3008D8CC000E3E4F2003C3DD0007575D000F7F7FA003E3EBA00686764006466
        CB003436D4002F2FC200A09B90005B5A5500737396008F8FD7007C7CD200A4A0
        82008B8A7A006169D600ADB0E100A3A2A2004C4ED500A9A9A8009C9AB3008686
        CE003333C9007373C500BDBDE100A6A29A005056DF007D7D9A008787C100B1AF
        A700C8C8EA00BBB7A5007C7B6E007C7CC8006060CA004343BB004E4EC4006565
        BD009191C9009F9BAD00E8E8F3003739CF005864E400E5E5F800DDDEF0005C5C
        D0003133CF004A4AC900B2B4DF00B9B7AD00E1E1ED0088877600474BD4008E8E
        A3006F6FC4004349D0005D5DC0009C9C93004343C200505AE200B0AA98006868
        C9005E5D5700464FDB00565DE000A1A09E0081819D008A8AC3007F7FCA003636
        CC00A19D81009090B500E9E9F700A4A39E008989CE008281AF002F33C800C5C6
        E8005D5D5B006E6B5A007E7D7100ABABAB006168E200A3A1AE006268CE008282
        CB00E1E1F1003336D000656563005761E20086857B00C3C4F1004049D2004748
        D2002C2FCA00787871005159DE00ACABA8008F8FCD008C8DCA00E6E6F400AFAD
        A6008C8C85006F6FC7008382A100BBB9AD004E54DF009797D1007171C400DCDC
        F1003232CB004246D3004E4FD6009E9B9400A09F9C00EBECF500E6E7F2009C99
        8300908EB6008988C100E8E8F100B0AC9800A4A3A000EAEAF600E5E5F3003032
        CE003232C800817E6D00ADADAD00ABAAA700AAAAAA009F9C91009E9C9C003938
        3600E3E3F300E2E2F1003335CF003737CC00BAB7A600B0AD9A00A5A19A00E9E9
        F60044433C002F2FC300BDBAAB004E4EC500959277005964E400AEADAD00ABAB
        AA00ABAAA800ABAAAA00A19F9E00A09F9D007C7CD1009090B400000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000020202022C0B
        251010C18A2C0202020202028D4006A2D727278320CA94BE0202028D7A5C6270
        567C7CA6AB9F8C45D102029B6F0788739C95158E2841794A1F027D8B3A6A124C
        0DCF667B246742B31DAE96C7434EB7B04B397731AF0EACCB0803BB175204213B
        32989064309399BA503729B26BC5B9973B6C68092E65812B22CE820C60693C9A
        89A0C32FC4AACB768749B18F165E74149284A9543F19CD5F6313BF5B36A36EB4
        C935051AB47F340FC6449D78A472B8C247483DD65A3EA811B5BD0255CC335D91
        865180532D610A1BC8020223756D5758A79E18381E46A5ADBC020202D0B6A14D
        2A267E714D5985BC020202020202D1D4C0D24F1CD5D302020202}
      WordWrap = True
      OnClick = ToolbarButton972Click
    end
  end
  object Pan1: TPanel
    Left = 0
    Top = 40
    Width = 522
    Height = 30
    Alignment = taLeftJustify
    Anchors = [akLeft, akTop, akRight]
    BevelOuter = bvLowered
    TabOrder = 1
    Visible = False
    object Label1: TLabel
      Left = 7
      Top = 8
      Width = 31
      Height = 13
      Caption = 'Label1'
    end
    object dxDBEdit1: TdxDBEdit
      Left = 190
      Top = 3
      Width = 317
      TabOrder = 0
      Anchors = [akLeft, akTop, akRight]
      DataSource = DsQCampiPers
    end
    object dxDBDateEdit1: TdxDBDateEdit
      Left = 190
      Top = 3
      Width = 97
      TabOrder = 1
      Visible = False
      DataSource = DsQCampiPers
    end
    object dxDBCheckEdit1: TdxDBCheckEdit
      Left = 190
      Top = 4
      Width = 22
      TabOrder = 2
      Visible = False
      DataSource = DsQCampiPers
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object dxDBSpinEdit1: TdxDBSpinEdit
      Left = 190
      Top = 3
      Width = 57
      TabOrder = 3
      Visible = False
      DataSource = DsQCampiPers
    end
    object dxDBMemo1: TdxDBMemo
      Left = 192
      Top = 5
      Width = 311
      TabOrder = 4
      Anchors = [akLeft, akTop, akRight, akBottom]
      DataSource = DsQCampiPers
      ScrollBars = ssVertical
      Height = 18
    end
  end
  object Pan2: TPanel
    Left = 0
    Top = 70
    Width = 522
    Height = 30
    Alignment = taLeftJustify
    Anchors = [akLeft, akTop, akRight]
    BevelOuter = bvLowered
    TabOrder = 2
    Visible = False
    object Label2: TLabel
      Left = 7
      Top = 6
      Width = 31
      Height = 13
      Caption = 'Label1'
    end
    object dxDBEdit2: TdxDBEdit
      Left = 190
      Top = 3
      Width = 317
      TabOrder = 0
      Anchors = [akLeft, akTop, akRight]
      DataSource = DsQCampiPers
    end
    object dxDBDateEdit2: TdxDBDateEdit
      Left = 190
      Top = 3
      Width = 97
      TabOrder = 1
      Visible = False
      DataSource = DsQCampiPers
    end
    object dxDBCheckEdit2: TdxDBCheckEdit
      Left = 190
      Top = 4
      Width = 22
      TabOrder = 2
      Visible = False
      DataSource = DsQCampiPers
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object dxDBSpinEdit2: TdxDBSpinEdit
      Left = 190
      Top = 3
      Width = 57
      TabOrder = 3
      Visible = False
      DataSource = DsQCampiPers
    end
    object dxDBMemo2: TdxDBMemo
      Left = 192
      Top = 5
      Width = 311
      TabOrder = 4
      Anchors = [akLeft, akTop, akRight, akBottom]
      DataSource = DsQCampiPers
      ScrollBars = ssVertical
      Height = 18
    end
  end
  object Pan3: TPanel
    Left = 0
    Top = 103
    Width = 522
    Height = 30
    Alignment = taLeftJustify
    Anchors = [akLeft, akTop, akRight]
    BevelOuter = bvLowered
    TabOrder = 3
    Visible = False
    object Label3: TLabel
      Left = 7
      Top = 6
      Width = 31
      Height = 13
      Caption = 'Label1'
    end
    object dxDBEdit3: TdxDBEdit
      Left = 190
      Top = 3
      Width = 317
      TabOrder = 0
      Anchors = [akLeft, akTop, akRight]
      DataSource = DsQCampiPers
    end
    object dxDBDateEdit3: TdxDBDateEdit
      Left = 190
      Top = 3
      Width = 97
      TabOrder = 1
      Visible = False
      DataSource = DsQCampiPers
    end
    object dxDBCheckEdit3: TdxDBCheckEdit
      Left = 190
      Top = 4
      Width = 22
      TabOrder = 2
      Visible = False
      DataSource = DsQCampiPers
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object dxDBSpinEdit3: TdxDBSpinEdit
      Left = 190
      Top = 3
      Width = 57
      TabOrder = 3
      Visible = False
      DataSource = DsQCampiPers
    end
    object dxDBMemo3: TdxDBMemo
      Left = 192
      Top = 5
      Width = 311
      TabOrder = 4
      Anchors = [akLeft, akTop, akRight, akBottom]
      DataSource = DsQCampiPers
      ScrollBars = ssVertical
      Height = 18
    end
  end
  object Pan4: TPanel
    Left = 0
    Top = 136
    Width = 522
    Height = 30
    Alignment = taLeftJustify
    Anchors = [akLeft, akTop, akRight]
    BevelOuter = bvLowered
    TabOrder = 4
    Visible = False
    object Label4: TLabel
      Left = 7
      Top = 6
      Width = 31
      Height = 13
      Caption = 'Label1'
    end
    object dxDBEdit4: TdxDBEdit
      Left = 190
      Top = 3
      Width = 317
      TabOrder = 0
      Anchors = [akLeft, akTop, akRight]
      DataSource = DsQCampiPers
    end
    object dxDBDateEdit4: TdxDBDateEdit
      Left = 190
      Top = 3
      Width = 97
      TabOrder = 1
      Visible = False
      DataSource = DsQCampiPers
    end
    object dxDBCheckEdit4: TdxDBCheckEdit
      Left = 190
      Top = 4
      Width = 22
      TabOrder = 2
      Visible = False
      DataSource = DsQCampiPers
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object dxDBSpinEdit4: TdxDBSpinEdit
      Left = 190
      Top = 3
      Width = 57
      TabOrder = 3
      Visible = False
      DataSource = DsQCampiPers
    end
    object dxDBMemo4: TdxDBMemo
      Left = 192
      Top = 5
      Width = 311
      TabOrder = 4
      Anchors = [akLeft, akTop, akRight, akBottom]
      DataSource = DsQCampiPers
      ScrollBars = ssVertical
      Height = 18
    end
  end
  object Pan5: TPanel
    Left = 0
    Top = 169
    Width = 522
    Height = 30
    Alignment = taLeftJustify
    Anchors = [akLeft, akTop, akRight]
    BevelOuter = bvLowered
    TabOrder = 5
    Visible = False
    object Label5: TLabel
      Left = 7
      Top = 6
      Width = 31
      Height = 13
      Caption = 'Label1'
    end
    object dxDBEdit5: TdxDBEdit
      Left = 190
      Top = 3
      Width = 317
      TabOrder = 0
      Anchors = [akLeft, akTop, akRight]
      DataSource = DsQCampiPers
    end
    object dxDBDateEdit5: TdxDBDateEdit
      Left = 190
      Top = 3
      Width = 97
      TabOrder = 1
      Visible = False
      DataSource = DsQCampiPers
    end
    object dxDBCheckEdit5: TdxDBCheckEdit
      Left = 190
      Top = 4
      Width = 22
      TabOrder = 2
      Visible = False
      DataSource = DsQCampiPers
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object dxDBSpinEdit5: TdxDBSpinEdit
      Left = 190
      Top = 3
      Width = 57
      TabOrder = 3
      Visible = False
      DataSource = DsQCampiPers
    end
    object dxDBMemo5: TdxDBMemo
      Left = 192
      Top = 5
      Width = 311
      TabOrder = 4
      Anchors = [akLeft, akTop, akRight, akBottom]
      DataSource = DsQCampiPers
      ScrollBars = ssVertical
      Height = 18
    end
  end
  object Pan6: TPanel
    Left = 0
    Top = 201
    Width = 522
    Height = 30
    Alignment = taLeftJustify
    Anchors = [akLeft, akTop, akRight]
    BevelOuter = bvLowered
    TabOrder = 6
    Visible = False
    object Label6: TLabel
      Left = 7
      Top = 6
      Width = 31
      Height = 13
      Caption = 'Label1'
    end
    object dxDBEdit6: TdxDBEdit
      Left = 190
      Top = 3
      Width = 317
      TabOrder = 0
      Anchors = [akLeft, akTop, akRight]
      DataSource = DsQCampiPers
    end
    object dxDBDateEdit6: TdxDBDateEdit
      Left = 190
      Top = 3
      Width = 97
      TabOrder = 1
      Visible = False
      DataSource = DsQCampiPers
    end
    object dxDBCheckEdit6: TdxDBCheckEdit
      Left = 190
      Top = 4
      Width = 22
      TabOrder = 2
      Visible = False
      DataSource = DsQCampiPers
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object dxDBSpinEdit6: TdxDBSpinEdit
      Left = 190
      Top = 3
      Width = 57
      TabOrder = 3
      Visible = False
      DataSource = DsQCampiPers
    end
    object dxDBMemo6: TdxDBMemo
      Left = 192
      Top = 5
      Width = 311
      TabOrder = 4
      Anchors = [akLeft, akTop, akRight, akBottom]
      DataSource = DsQCampiPers
      ScrollBars = ssVertical
      Height = 18
    end
  end
  object Pan7: TPanel
    Left = 0
    Top = 233
    Width = 522
    Height = 30
    Alignment = taLeftJustify
    Anchors = [akLeft, akTop, akRight]
    BevelOuter = bvLowered
    TabOrder = 7
    Visible = False
    object Label7: TLabel
      Left = 7
      Top = 6
      Width = 31
      Height = 13
      Caption = 'Label1'
    end
    object dxDBEdit7: TdxDBEdit
      Left = 190
      Top = 3
      Width = 317
      TabOrder = 0
      Anchors = [akLeft, akTop, akRight]
      DataSource = DsQCampiPers
    end
    object dxDBDateEdit7: TdxDBDateEdit
      Left = 190
      Top = 3
      Width = 97
      TabOrder = 1
      Visible = False
      DataSource = DsQCampiPers
    end
    object dxDBCheckEdit7: TdxDBCheckEdit
      Left = 190
      Top = 4
      Width = 22
      TabOrder = 2
      Visible = False
      DataSource = DsQCampiPers
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object dxDBSpinEdit7: TdxDBSpinEdit
      Left = 190
      Top = 3
      Width = 57
      TabOrder = 3
      Visible = False
      DataSource = DsQCampiPers
    end
    object dxDBMemo7: TdxDBMemo
      Left = 192
      Top = 5
      Width = 311
      TabOrder = 4
      Anchors = [akLeft, akTop, akRight, akBottom]
      DataSource = DsQCampiPers
      ScrollBars = ssVertical
      Height = 18
    end
  end
  object Pan8: TPanel
    Left = 0
    Top = 265
    Width = 522
    Height = 30
    Alignment = taLeftJustify
    Anchors = [akLeft, akTop, akRight]
    BevelOuter = bvLowered
    TabOrder = 8
    Visible = False
    object Label8: TLabel
      Left = 7
      Top = 6
      Width = 31
      Height = 13
      Caption = 'Label1'
    end
    object dxDBEdit8: TdxDBEdit
      Left = 190
      Top = 3
      Width = 317
      TabOrder = 0
      Anchors = [akLeft, akTop, akRight]
      DataSource = DsQCampiPers
    end
    object dxDBDateEdit8: TdxDBDateEdit
      Left = 190
      Top = 3
      Width = 97
      TabOrder = 1
      Visible = False
      DataSource = DsQCampiPers
    end
    object dxDBCheckEdit8: TdxDBCheckEdit
      Left = 190
      Top = 4
      Width = 22
      TabOrder = 2
      Visible = False
      DataSource = DsQCampiPers
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object dxDBSpinEdit8: TdxDBSpinEdit
      Left = 190
      Top = 3
      Width = 57
      TabOrder = 3
      Visible = False
      DataSource = DsQCampiPers
    end
    object dxDBMemo8: TdxDBMemo
      Left = 192
      Top = 5
      Width = 311
      TabOrder = 4
      Anchors = [akLeft, akTop, akRight, akBottom]
      DataSource = DsQCampiPers
      ScrollBars = ssVertical
      Height = 18
    end
  end
  object Pan9: TPanel
    Left = 0
    Top = 297
    Width = 522
    Height = 30
    Alignment = taLeftJustify
    Anchors = [akLeft, akTop, akRight]
    BevelOuter = bvLowered
    TabOrder = 9
    Visible = False
    object Label9: TLabel
      Left = 7
      Top = 6
      Width = 31
      Height = 13
      Caption = 'Label1'
    end
    object dxDBEdit9: TdxDBEdit
      Left = 190
      Top = 3
      Width = 317
      TabOrder = 0
      Anchors = [akLeft, akTop, akRight]
      DataSource = DsQCampiPers
    end
    object dxDBDateEdit9: TdxDBDateEdit
      Left = 190
      Top = 3
      Width = 97
      TabOrder = 1
      Visible = False
      DataSource = DsQCampiPers
    end
    object dxDBCheckEdit9: TdxDBCheckEdit
      Left = 190
      Top = 4
      Width = 22
      TabOrder = 2
      Visible = False
      DataSource = DsQCampiPers
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object dxDBSpinEdit9: TdxDBSpinEdit
      Left = 190
      Top = 3
      Width = 57
      TabOrder = 3
      Visible = False
      DataSource = DsQCampiPers
    end
    object dxDBMemo9: TdxDBMemo
      Left = 192
      Top = 5
      Width = 311
      TabOrder = 4
      Anchors = [akLeft, akTop, akRight, akBottom]
      DataSource = DsQCampiPers
      ScrollBars = ssVertical
      Height = 18
    end
  end
  object Pan10: TPanel
    Left = 0
    Top = 329
    Width = 522
    Height = 30
    Alignment = taLeftJustify
    Anchors = [akLeft, akTop, akRight]
    BevelOuter = bvLowered
    TabOrder = 10
    Visible = False
    object Label10: TLabel
      Left = 7
      Top = 6
      Width = 31
      Height = 13
      Caption = 'Label1'
    end
    object dxDBEdit10: TdxDBEdit
      Left = 190
      Top = 3
      Width = 317
      TabOrder = 0
      Anchors = [akLeft, akTop, akRight]
      DataSource = DsQCampiPers
    end
    object dxDBDateEdit10: TdxDBDateEdit
      Left = 190
      Top = 3
      Width = 97
      TabOrder = 1
      Visible = False
      DataSource = DsQCampiPers
    end
    object dxDBCheckEdit10: TdxDBCheckEdit
      Left = 190
      Top = 4
      Width = 22
      TabOrder = 2
      Visible = False
      DataSource = DsQCampiPers
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object dxDBSpinEdit10: TdxDBSpinEdit
      Left = 190
      Top = 3
      Width = 57
      TabOrder = 3
      Visible = False
      DataSource = DsQCampiPers
    end
    object dxDBMemo10: TdxDBMemo
      Left = 192
      Top = 5
      Width = 311
      TabOrder = 4
      Anchors = [akLeft, akTop, akRight, akBottom]
      DataSource = DsQCampiPers
      ScrollBars = ssVertical
      Height = 18
    end
  end
  object Pan11: TPanel
    Left = 1
    Top = 363
    Width = 522
    Height = 30
    Alignment = taLeftJustify
    Anchors = [akLeft, akTop, akRight]
    BevelOuter = bvLowered
    TabOrder = 11
    Visible = False
    object Label11: TLabel
      Left = 7
      Top = 6
      Width = 31
      Height = 13
      Caption = 'Label1'
    end
    object dxDBEdit11: TdxDBEdit
      Left = 190
      Top = 3
      Width = 315
      TabOrder = 0
      Anchors = [akLeft, akTop, akRight]
      DataSource = DsQCampiPers
    end
    object dxDBDateEdit11: TdxDBDateEdit
      Left = 190
      Top = 3
      Width = 97
      TabOrder = 1
      Visible = False
      DataSource = DsQCampiPers
    end
    object dxDBCheckEdit11: TdxDBCheckEdit
      Left = 190
      Top = 4
      Width = 22
      TabOrder = 2
      Visible = False
      DataSource = DsQCampiPers
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object dxDBSpinEdit11: TdxDBSpinEdit
      Left = 190
      Top = 3
      Width = 57
      TabOrder = 3
      Visible = False
      DataSource = DsQCampiPers
    end
    object dxDBMemo11: TdxDBMemo
      Left = 192
      Top = 5
      Width = 313
      TabOrder = 4
      Anchors = [akLeft, akTop, akRight, akBottom]
      DataSource = DsQCampiPers
      ScrollBars = ssVertical
      Height = 18
    end
  end
  object Pan12: TPanel
    Left = 3
    Top = 395
    Width = 522
    Height = 30
    Alignment = taLeftJustify
    Anchors = [akLeft, akTop, akRight]
    BevelOuter = bvLowered
    TabOrder = 12
    Visible = False
    object Label12: TLabel
      Left = 7
      Top = 6
      Width = 31
      Height = 13
      Caption = 'Label1'
    end
    object dxDBEdit12: TdxDBEdit
      Left = 190
      Top = 3
      Width = 315
      TabOrder = 0
      Anchors = [akLeft, akTop, akRight]
      DataSource = DsQCampiPers
    end
    object dxDBDateEdit12: TdxDBDateEdit
      Left = 190
      Top = 3
      Width = 97
      TabOrder = 1
      Visible = False
      DataSource = DsQCampiPers
    end
    object dxDBCheckEdit12: TdxDBCheckEdit
      Left = 190
      Top = 4
      Width = 22
      TabOrder = 2
      Visible = False
      DataSource = DsQCampiPers
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object dxDBSpinEdit12: TdxDBSpinEdit
      Left = 190
      Top = 3
      Width = 57
      TabOrder = 3
      Visible = False
      DataSource = DsQCampiPers
    end
    object dxDBMemo12: TdxDBMemo
      Left = 192
      Top = 5
      Width = 313
      TabOrder = 4
      Anchors = [akLeft, akTop, akRight, akBottom]
      DataSource = DsQCampiPers
      ScrollBars = ssVertical
      Height = 18
    end
  end
  object QCampiPers: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from AnagCampiPers'
      'where IDAnagrafica=875')
    Left = 24
    Top = 288
  end
  object DsQCampiPers: TDataSource
    DataSet = TCampiPers
    OnStateChange = DsQCampiPersStateChange
    Left = 24
    Top = 320
  end
  object Q: TADOQuery
    Connection = Data.DB
    Parameters = <>
    Left = 200
    Top = 296
  end
  object TCampiPers: TADOTable
    Connection = Data.DB
    BeforeClose = TCampiPersBeforeClose
    Left = 64
    Top = 288
  end
  object Qattivitaformaz: TADOQuery
    Connection = Data.DB
    Parameters = <>
    SQL.Strings = (
      ''
      'select Form_Attivita.*, '
      '          AnagTutor.Cognome TutorCognome, '
      
        '          AnagTutor.Nome TutorNome, AnagTutor.Matricola TutorMat' +
        'ricola,'
      '          Form_Attivita.id idcorso'
      'from Form_Attivita'
      
        'left outer join Anagrafica AnagTutor on Form_Attivita.IDTutor = ' +
        'AnagTutor.ID'
      'where Form_Attivita.SoloAnagrafica = 0')
    Left = 328
    Top = 320
    object QattivitaformazID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QattivitaformazIDProgettoForm: TIntegerField
      FieldName = 'IDProgettoForm'
    end
    object QattivitaformazCodiceCorso: TStringField
      FieldName = 'CodiceCorso'
      Size = 15
    end
    object QattivitaformazInterno: TBooleanField
      FieldName = 'Interno'
    end
    object QattivitaformazIDArea: TIntegerField
      FieldName = 'IDArea'
    end
    object QattivitaformazIDResponsabile: TIntegerField
      FieldName = 'IDResponsabile'
    end
    object QattivitaformazIDEnteFormaz: TIntegerField
      FieldName = 'IDEnteFormaz'
    end
    object QattivitaformazIDTutor: TIntegerField
      FieldName = 'IDTutor'
    end
    object QattivitaformazCodiceArgomento: TStringField
      FieldName = 'CodiceArgomento'
      Size = 3
    end
    object QattivitaformazLivello: TStringField
      FieldName = 'Livello'
      Size = 1
    end
    object QattivitaformazAttiva: TBooleanField
      FieldName = 'Attiva'
    end
    object QattivitaformazStato: TStringField
      FieldName = 'Stato'
    end
    object QattivitaformazTipo: TStringField
      FieldName = 'Tipo'
    end
    object QattivitaformazDataInizio: TDateTimeField
      FieldName = 'DataInizio'
    end
    object QattivitaformazDataFine: TDateTimeField
      FieldName = 'DataFine'
    end
    object QattivitaformazPeriodoPrevisto: TStringField
      FieldName = 'PeriodoPrevisto'
      Size = 30
    end
    object QattivitaformazDurataPrevista: TStringField
      FieldName = 'DurataPrevista'
      Size = 30
    end
    object QattivitaformazOrarioDalle: TDateTimeField
      FieldName = 'OrarioDalle'
    end
    object QattivitaformazOrarioAlle: TDateTimeField
      FieldName = 'OrarioAlle'
    end
    object QattivitaformazDescrizione: TStringField
      FieldName = 'Descrizione'
      Size = 255
    end
    object QattivitaformazLuogoSvolgimento: TStringField
      FieldName = 'LuogoSvolgimento'
      Size = 40
    end
    object QattivitaformazProgramma: TMemoField
      FieldName = 'Programma'
      BlobType = ftMemo
    end
    object QattivitaformazNote: TMemoField
      FieldName = 'Note'
      BlobType = ftMemo
    end
    object QattivitaformazNumMinPartec: TIntegerField
      FieldName = 'NumMinPartec'
    end
    object QattivitaformazNumMaxPartec: TIntegerField
      FieldName = 'NumMaxPartec'
    end
    object QattivitaformazTotOreLavoro: TIntegerField
      FieldName = 'TotOreLavoro'
    end
    object QattivitaformazTotOreExtra: TIntegerField
      FieldName = 'TotOreExtra'
    end
    object QattivitaformazImportoFinanziatoLire: TFloatField
      FieldName = 'ImportoFinanziatoLire'
    end
    object QattivitaformazImportoFinanziatoEuro: TFloatField
      FieldName = 'ImportoFinanziatoEuro'
    end
    object QattivitaformazCostoOraLavorative: TFloatField
      FieldName = 'CostoOraLavorative'
    end
    object QattivitaformazCostoOraStraord: TFloatField
      FieldName = 'CostoOraStraord'
    end
    object QattivitaformazCostoOraLavFestiva: TFloatField
      FieldName = 'CostoOraLavFestiva'
    end
    object QattivitaformazNumProgetto: TStringField
      FieldName = 'NumProgetto'
    end
    object QattivitaformazFondoSociale: TBooleanField
      FieldName = 'FondoSociale'
    end
    object QattivitaformazIDMacroGruppo: TIntegerField
      FieldName = 'IDMacroGruppo'
    end
    object QattivitaformazTotOrePrev: TIntegerField
      FieldName = 'TotOrePrev'
    end
    object QattivitaformazCrediti: TSmallintField
      FieldName = 'Crediti'
    end
    object QattivitaformazIDQuest: TIntegerField
      FieldName = 'IDQuest'
    end
    object QattivitaformazIDFormTipiAtt: TIntegerField
      FieldName = 'IDFormTipiAtt'
    end
    object QattivitaformazSoloAnagrafica: TBooleanField
      FieldName = 'SoloAnagrafica'
    end
    object QattivitaformazTutorCognome: TStringField
      FieldName = 'TutorCognome'
      Size = 30
    end
    object QattivitaformazTutorNome: TStringField
      FieldName = 'TutorNome'
      Size = 30
    end
    object Qattivitaformazidcorso: TAutoIncField
      FieldName = 'idcorso'
      ReadOnly = True
    end
  end
end
