unit UnitPrintMemo;

interface

uses printers, dialogs, graphics, sysutils, windows, stdctrls, forms, classes,
     winprocs;

procedure PrintMemo(MC: TMemo);
procedure Printintestazione(Canvas: TCanvas; Align: TAlignment; Text: string);
procedure PrintMemoConIntestazione(MC: TMemo; Intestazione: string);
function perc(p: real; value: integer): integer;

var scalex, scaley: real;
implementation


function perc(p: real; value: integer): integer;
begin
     result := round(value * p / 100);
end;

procedure PrintIntestazione(Canvas: TCanvas; Align: TAlignment; Text: string);
begin
     with Canvas do
     begin
          font.Size := perc(1.5, printer.PageHeight);
          case Align of
               taLeftJustify: TextOut(20, 20 + perc(1.5, printer.PageHeight), Text);
               taRightJustify: TextOut(20 - Textwidth(Text), 20 + perc(1.5, printer.PageHeight), Text);
               taCenter: TextOut((printer.PageWidth div 2) - (Textwidth(Text) div 2), 20 + perc(1.5, printer.PageHeight), Text);
          end;
     end;
end;

procedure PrintMemo(MC: TMemo);
var C: array[0..255] of char;
     CLen: integer;
     MemoRect: TRect;
     dialog: TPrintDialog;
     copia: integer;

begin
     if Printer.Printers.Count > 0 then
     begin
          Dialog := TPrintDialog.Create(application);
          with Printer.Canvas, Printer do
          begin
               if (Dialog.Execute) then
               begin
                    BeginDoc;
                    brush.Style := bsSolid;
                    pen.Color := clblack;
                    Font.Assign(mc.Font);
                    font.Size := perc(0.4, PageHeight);
                    ScaleX := WinProcs.GetDeviceCaps(Handle, LOGPIXELSX) / screen.PixelsPerInch;
                    ScaleY := WinProcs.GetDeviceCaps(Handle, LOGPIXELSY) / screen.PixelsPerInch;
                    for Copia := 1 to Dialog.Copies do
                    begin
                         //printmemo
                         MemoRect.Left := perc(3.5, Pagewidth) + Round(7 * ScaleX);
                         MemoRect.Right := perc(96.5, Pagewidth) - Round(7 * ScaleX);
                         MemoRect.Top := perc(4, Pageheight) + Round(4 * ScaleY); ;
                         MemoRect.Bottom := perc(95.5, PageHeight) - Round(8 * ScaleY); ;
                         CLen := MC.GetTextBuf(C, 255);
                         if (C[CLen - 1] = #10) and (C[CLen - 2] = #13) then
                              CLen := CLen - 2;
                         WinProcs.DrawText(printer.Canvas.Handle, C, CLen, MemoRect, DT_Left or DT_WORDBREAK {or DT_CALCRECT});
                         //print memo
                         if (Copia < Dialog.Copies) then
                              Printer.NewPage;
                    end;
                    if Printing then EndDoc;
               end;
          end; {with}
          Dialog.free;
     end
     else {non ci sono stampanti selezionate}
          ShowMessage('Attenzione! Non ci sono stampanti installate sul computer.');
     //print scheda
end;

procedure PrintMemoConIntestazione(MC: TMemo; Intestazione: string);
var C: array[0..255] of char;
     CLen: integer;
     MemoRect: TRect;
     dialog: TPrintDialog;
     copia: integer;

begin
     if Printer.Printers.Count > 0 then
     begin
          Dialog := TPrintDialog.Create(application);
          with Printer.Canvas, Printer do
          begin
               if (Dialog.Execute) then
               begin
                    BeginDoc;
                    brush.Style := bsSolid;
                    pen.Color := clblack;
                    Font.Assign(mc.Font);
                    ScaleX := WinProcs.GetDeviceCaps(Handle, LOGPIXELSX) / screen.PixelsPerInch;
                    ScaleY := WinProcs.GetDeviceCaps(Handle, LOGPIXELSY) / screen.PixelsPerInch;
                    for Copia := 1 to Dialog.Copies do
                    begin
                         //printmemo
                         printIntestazione(printer.canvas, taCenter, Intestazione);
                         font.Size := perc(0.4, PageHeight);
                         MemoRect.Left := perc(3.5, Pagewidth) + Round(7 * ScaleX);
                         MemoRect.Right := perc(96.5, Pagewidth) - Round(7 * ScaleX);
                         MemoRect.Top := perc(9, Pageheight) + Round(4 * ScaleY); ;
                         MemoRect.Bottom := perc(91, PageHeight) - Round(8 * ScaleY); ;
                         CLen := MC.GetTextBuf(C, 255);
                         if (C[CLen - 1] = #10) and (C[CLen - 2] = #13) then
                              CLen := CLen - 2;
                         WinProcs.DrawText(printer.Canvas.Handle, C, CLen, MemoRect, DT_Left or DT_WORDBREAK {or DT_CALCRECT});
                         //print memo
                         if (Copia < Dialog.Copies) then
                              Printer.NewPage;
                    end;
                    if Printing then EndDoc;
               end;
          end; {with}
          Dialog.free;
     end
     else {non ci sono stampanti selezionate}
          ShowMessage('Attenzione! Non ci sono stampanti installate sul computer.');
     //print scheda
end;

end.
