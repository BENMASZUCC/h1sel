object StoricoAnagPosForm: TStoricoAnagPosForm
  Left = 266
  Top = 137
  BorderStyle = bsDialog
  Caption = 'Storico posizione organigramma'
  ClientHeight = 342
  ClientWidth = 722
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inline StoricoAnagPosFrame1: TStoricoAnagPosFrame
    Width = 722
    Height = 300
    Align = alClient
    inherited PanButtons: TPanel
      Width = 722
      inherited ToolbarButton971: TToolbarButton97
        OnClick = StoricoAnagPosFrame1ToolbarButton971Click
      end
      inherited ToolbarButton973: TToolbarButton97
        OnClick = StoricoAnagPosFrame1ToolbarButton973Click
      end
    end
    inherited RxDBGrid1: TdxDBGrid
      Width = 722
      Height = 261
      Filter.Criteria = {00000000}
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 300
    Width = 722
    Height = 42
    Align = alBottom
    BevelOuter = bvLowered
    TabOrder = 1
    object BitBtn1: TBitBtn
      Left = 631
      Top = 4
      Width = 88
      Height = 34
      Caption = 'Esci'
      TabOrder = 0
      OnClick = BitBtn1Click
      Kind = bkOK
    end
  end
end
