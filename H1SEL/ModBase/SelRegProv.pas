unit SelRegProv;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     dxCntner, dxTL, dxDBCtrl, dxDBGrid, ExtCtrls, Db, ADODB, StdCtrls,
     Buttons, TB97;

type
     TSelRegProvForm = class(TForm)
          Panel1: TPanel;
          QReg: TADOQuery;
          DsQReg: TDataSource;
          QRegID: TAutoIncField;
          QRegRegione: TStringField;
          Panel2: TPanel;
          Splitter1: TSplitter;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1ID: TdxDBGridMaskColumn;
          dxDBGrid1Regione: TdxDBGridMaskColumn;
          QProv: TADOQuery;
          DsQProv: TDataSource;
          Panel3: TPanel;
          Panel4: TPanel;
          dxDBGrid2: TdxDBGrid;
          QRegCodRegioneH1: TStringField;
          QProvProv: TStringField;
          QProvRegione: TStringField;
          dxDBGrid2Prov: TdxDBGridMaskColumn;
          dxDBGrid2Regione: TdxDBGridMaskColumn;
          CBAncheProv: TCheckBox;
          BitBtn1: TToolbarButton97;
          BitBtn2: TToolbarButton97;
          procedure FormShow(Sender: TObject);
          procedure CBAncheProvClick(Sender: TObject);
          procedure BitBtn1Click(Sender: TObject);
          procedure BitBtn2Click(Sender: TObject);
     private
    { Private declarations }
     public
    { Public declarations }
     end;

var
     SelRegProvForm: TSelRegProvForm;

implementation

uses ModuloDati, Main;

{$R *.DFM}

procedure TSelRegProvForm.FormShow(Sender: TObject);
begin
     //Grafica
     SelRegProvForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;   
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel3.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel3.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel3.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel4.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel4.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel4.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     QReg.Open;
     QProv.Open;

     dxDBGrid1.SetFocus;
end;

procedure TSelRegProvForm.CBAncheProvClick(Sender: TObject);
begin
     if CBAncheProv.checked then
          dxDBGrid2.Color := clWindow
     else dxDBGrid2.Color := clSilver;
end;

procedure TSelRegProvForm.BitBtn1Click(Sender: TObject);
begin
     SelRegProvForm.ModalResult := mrOk;
     if (CBAncheProv.Checked) and (dxDBGrid2.SelectedCount = 0) then begin
          MessageDlg('Nessuna provincia selezionata', mtError, [MbOK], 0);
          ModalResult := mrNone;
          Abort;
     end;
end;

procedure TSelRegProvForm.BitBtn2Click(Sender: TObject);
begin
     SelRegProvForm.ModalResult := mrCancel;
end;

end.

