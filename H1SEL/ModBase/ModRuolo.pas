unit ModRuolo;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, RXSpin, Buttons, TB97, ExtCtrls;

type
     TModRuoloForm = class(TForm)
          CBAttuale: TCheckBox;
          SEAnniEsp: TRxSpinEdit;
          Label1: TLabel;
          CBDisponibile: TCheckBox;
          CBAspirazione: TCheckBox;
    Panel1: TPanel;
    BOK: TToolbarButton97;
    BAnnulla: TToolbarButton97;
          procedure FormShow(Sender: TObject);
    procedure BOKClick(Sender: TObject);
    procedure BAnnullaClick(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     ModRuoloForm: TModRuoloForm;

implementation

uses Main;

{$R *.DFM}

procedure TModRuoloForm.FormShow(Sender: TObject);
begin
//Grafica
     ModRuoloForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     Caption := '[S/29] - ' + Caption;
end;

procedure TModRuoloForm.BOKClick(Sender: TObject);
begin
     ModRuoloForm.ModalResult:=mrOK;
end;

procedure TModRuoloForm.BAnnullaClick(Sender: TObject);
begin
ModRuoloForm.ModalResult:=mrCancel;
end;

end.
