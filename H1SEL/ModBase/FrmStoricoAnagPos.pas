unit FrmStoricoAnagPos;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, DBTables, Grids, DBGrids, RXDBCtrl, ExtCtrls, TB97, ADODB, U_ADOLinkCl,
     dxTL, dxDBCtrl, dxDBGrid, dxCntner;

type
     TStoricoAnagPosFrame = class(TFrame)
          PanButtons: TPanel;
          QStoricoPos_OLD: TQuery;
          dsQStoricoPos: TDataSource;
          ToolbarButton971: TToolbarButton97;
          ToolbarButton972: TToolbarButton97;
          QStoricoPos_OLDCognome: TStringField;
          QStoricoPos_OLDNome: TStringField;
          QStoricoPos_OLDID: TAutoIncField;
          QStoricoPos_OLDIDAnagrafica: TIntegerField;
          QStoricoPos_OLDIDOrganigramma: TIntegerField;
          QStoricoPos_OLDDallaData: TDateTimeField;
          QStoricoPos_OLDAlladata: TDateTimeField;
          QStoricoPos_OLDNote: TStringField;
          QStoricoPos_OLDIDAutorizzatoDa: TIntegerField;
          QStoricoPos_OLDAutorizzatoDa: TStringField;
          QStoricoPos_OLDDescrizione: TStringField;
          QStoricoPos_OLDAutorizzatoDaNome: TStringField;
          ToolbarButton973: TToolbarButton97;
          QStoricoPos: TADOLinkedQuery;
          RxDBGrid1: TdxDBGrid;
          RxDBGrid1Descrizione: TdxDBGridColumn;
          RxDBGrid1Cognome: TdxDBGridColumn;
          RxDBGrid1Nome: TdxDBGridColumn;
          RxDBGrid1DallaData: TdxDBGridColumn;
          RxDBGrid1Alladata: TdxDBGridColumn;
          RxDBGrid1AutorizzatoDa: TdxDBGridColumn;
          RxDBGrid1Note: TdxDBGridColumn;
          procedure ToolbarButton971Click(Sender: TObject);
          procedure ToolbarButton972Click(Sender: TObject);
          procedure ToolbarButton973Click(Sender: TObject);
     private
          { Private declarations }
     public
          xIDOrganigramma, xIDAnagrafica: integer;
     end;

implementation

uses ModStoricoAnagPos,
     //[/TONI20020729\]
     ModuloDati;
//[/TONI20020729\]FINE
{$R *.DFM}

//[TONI20021004]DEBUGOK

procedure TStoricoAnagPosFrame.ToolbarButton971Click(Sender: TObject);
begin
     ModStoricoAnagPosForm := TModStoricoAnagPosForm.create(self);
     //[/TONI20020729\]
     {     ModStoricoAnagPosForm.xIDAnagrafica:=QStoricoPosIDAnagrafica.Value;
          ModStoricoAnagPosForm.ECogn1.Text:=QStoricoPosCognome.Value;
          ModStoricoAnagPosForm.ENome1.Text:=QStoricoPosNome.Value;
          ModStoricoAnagPosForm.xIDAutorizzatoDa:=QStoricoPosIDAutorizzatoDa.Value;
          ModStoricoAnagPosForm.ECogn2.Text:=QStoricoPosAutorizzatoDa.Value;
          ModStoricoAnagPosForm.ENome2.Text:=QStoricoPosAutorizzatoDaNome.Value;
          ModStoricoAnagPosForm.DEDallaData.Date:=QStoricoPosDallaData.Value;
          ModStoricoAnagPosForm.DEAllaData.Date:=QStoricoPosAllaData.Value;
          ModStoricoAnagPosForm.ENote.text:=QStoricoPosNote.Value;}
     ModStoricoAnagPosForm.xIDAnagrafica := QStoricoPos.FieldByName('IDAnagrafica').AsInteger;
     ModStoricoAnagPosForm.ECogn1.Text := QStoricoPos.FieldByName('Cognome').AsString;
     ModStoricoAnagPosForm.ENome1.Text := QStoricoPos.FieldByName('Nome').AsString;
     ModStoricoAnagPosForm.xIDAutorizzatoDa := QStoricoPos.FieldByName('IDAutorizzatoDa').AsInteger;
     ModStoricoAnagPosForm.ECogn2.Text := QStoricoPos.FieldByName('AutorizzatoDa').AsString;
     ModStoricoAnagPosForm.ENome2.Text := QStoricoPos.FieldByName('AutorizzatoDaNome').AsString;
     ModStoricoAnagPosForm.DEDallaData.Date := QStoricoPos.FieldByName('DallaData').AsDatetime;
     ModStoricoAnagPosForm.DEAllaData.Date := QStoricoPos.FieldByName('AllaData').AsDateTime;
     ModStoricoAnagPosForm.ENote.text := QStoricoPos.FieldByName('Note').AsString;

     ModStoricoAnagPosForm.ShowModal;
     if ModStoricoAnagPosForm.Modalresult = mrOK then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    {                    Q1.SQL.text:='update StoricoAnagPosOrg set IDAnagrafica=:xIDAnagrafica,'+
                                             'DallaData=:xDallaData,Note=:xNote,IDAutorizzatoDa=:xIDAutorizzatoDa';
                                        if ModStoricoAnagPosForm.DEAllaData.Text<>'' then
                                             Q1.SQL.Add(',Alladata=:xAlladata ')
                                        else Q1.SQL.Add(',Alladata=NULL ');
                                        Q1.SQL.Add('where ID='+QStoricoPosID.asString);
                                             Q1.ParamByName['xIDAnagrafica').asInteger:=ModStoricoAnagPosForm.xIDAnagrafica;
                                             Q1.ParamByName['xDallaData').asDateTime:=ModStoricoAnagPosForm.DEDallaData.Date;
                                             if ModStoricoAnagPosForm.DEAllaData.Text<>'' then
                                                  Q1.ParamByName['xAlladata').asDateTime:=ModStoricoAnagPosForm.DEAllaData.Date;
                                             Q1.ParamByName['xNote').asString:=ModStoricoAnagPosForm.ENote.text;
                                             Q1.ParamByName['xIDAutorizzatoDa').asInteger:=ModStoricoAnagPosForm.xIDAutorizzatoDa;}
                    Q1.SQL.text := 'update StoricoAnagPosOrg set IDAnagrafica=:xIDAnagrafica:,' +
                         'DallaData=:xDallaData:,Note=:xNote:,IDAutorizzatoDa=:xIDAutorizzatoDa:';
                    if ModStoricoAnagPosForm.DEAllaData.Text <> '' then
                         Q1.SQL.Add(',Alladata=:xAlladata: ')
                    else Q1.SQL.Add(',Alladata=NULL ');
                    Q1.SQL.Add('where ID=' + QStoricoPos.FieldByName('ID').asString);
                    Q1.ParamByName['xIDAnagrafica'] := ModStoricoAnagPosForm.xIDAnagrafica;
                    Q1.ParamByName['xDallaData'] := ModStoricoAnagPosForm.DEDallaData.Date;
                    if ModStoricoAnagPosForm.DEAllaData.Text <> '' then
                         Q1.ParamByName['xAlladata'] := ModStoricoAnagPosForm.DEAllaData.Date;
                    Q1.ParamByName['xNote'] := ModStoricoAnagPosForm.ENote.text;
                    Q1.ParamByName['xIDAutorizzatoDa'] := ModStoricoAnagPosForm.xIDAutorizzatoDa;
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    QStoricoPos.Close;
                    QStoricoPos.Open;
                    //[/TONI20020729\]FINE
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               end;
          end;
     end;
     ModStoricoAnagPosForm.Free;
end;

//[TONI20021004]DEBUGOK

procedure TStoricoAnagPosFrame.ToolbarButton972Click(Sender: TObject);
begin
     if QStoricoPos.RecordCount = 0 then exit;
     if MessageDlg('Sei sicuro di voler eliminare DEFINITIVAMENTE la riga di storico ?', mtWarning,
          [mbNo, mbYes], 0) = mrYes then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    //[/TONI20020729\]
                    {                    Q1.SQL.text:='delete from StoricoAnagPosOrg '+
                                             'where ID='+QStoricoPosID.AsString;}
                    Q1.SQL.text := 'delete from StoricoAnagPosOrg ' +
                         'where ID=' + QStoricoPos.FieldByName('ID').AsString;
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    QStoricoPos.Close;
                    QStoricoPos.Open;
                    //[/TONI20020729\]FINE
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               end;
          end;
     end;

end;

//[TONI20021004]DEBUGOK

procedure TStoricoAnagPosFrame.ToolbarButton973Click(Sender: TObject);
begin
     ModStoricoAnagPosForm := TModStoricoAnagPosForm.create(self);
     ModStoricoAnagPosForm.DEDallaData.Date := Date;
     ModStoricoAnagPosForm.DEAllaData.Date := Date;
     ModStoricoAnagPosForm.ENote.text := '';
     ModStoricoAnagPosForm.ShowModal;
     if ModStoricoAnagPosForm.Modalresult = mrOK then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    //[/TONI20020729\]
                    {                    if ModStoricoAnagPosForm.DEAllaData.Text<>'' then
                                             Q1.SQL.text:='insert into StoricoAnagPosOrg (IDAnagrafica,IDOrganigramma,DallaData,Alladata,Note,IDAutorizzatoDa) '+
                                                  ' values (:xIDAnagrafica,:xIDOrganigramma,:xDallaData,:xAlladata,:xNote,:xIDAutorizzatoDa)'
                                        else
                                             Q1.SQL.text:='insert into StoricoAnagPosOrg (IDAnagrafica,IDOrganigramma,DallaData,Note,IDAutorizzatoDa) '+
                                                  ' values (:xIDAnagrafica,:xIDOrganigramma,:xDallaData,:xNote,:xIDAutorizzatoDa)';
                                        Q1.ParamByName['xIDAnagrafica').asInteger:=ModStoricoAnagPosForm.xIDAnagrafica;
                                        Q1.ParamByName['xIDOrganigramma').asInteger:=xIDOrganigramma;
                                        Q1.ParamByName['xDallaData').asDateTime:=ModStoricoAnagPosForm.DEDallaData.Date;
                                        if ModStoricoAnagPosForm.DEAllaData.Text<>'' then
                                             Q1.ParamByName['xAlladata').asDateTime:=ModStoricoAnagPosForm.DEAllaData.Date;
                                        Q1.ParamByName['xNote').asString:=ModStoricoAnagPosForm.ENote.text;
                                        Q1.ParamByName['xIDAutorizzatoDa').asInteger:=ModStoricoAnagPosForm.xIDAutorizzatoDa;}
                    if ModStoricoAnagPosForm.DEAllaData.Text <> '' then
                         Q1.SQL.text := 'insert into StoricoAnagPosOrg (IDAnagrafica,IDOrganigramma,DallaData,Alladata,Note,IDAutorizzatoDa) ' +
                              ' values (:xIDAnagrafica:,:xIDOrganigramma:,:xDallaData:,:xAlladata:,:xNote:,:xIDAutorizzatoDa:)'
                    else
                         Q1.SQL.text := 'insert into StoricoAnagPosOrg (IDAnagrafica,IDOrganigramma,DallaData,Note,IDAutorizzatoDa) ' +
                              ' values (:xIDAnagrafica:,:xIDOrganigramma:,:xDallaData:,:xNote:,:xIDAutorizzatoDa:)';
                    Q1.ParamByName['xIDAnagrafica'] := ModStoricoAnagPosForm.xIDAnagrafica;
                    Q1.ParamByName['xIDOrganigramma'] := xIDOrganigramma;
                    Q1.ParamByName['xDallaData'] := ModStoricoAnagPosForm.DEDallaData.Date;
                    if ModStoricoAnagPosForm.DEAllaData.Text <> '' then
                         Q1.ParamByName['xAlladata'] := ModStoricoAnagPosForm.DEAllaData.Date;
                    Q1.ParamByName['xNote'] := ModStoricoAnagPosForm.ENote.text;
                    Q1.ParamByName['xIDAutorizzatoDa'] := ModStoricoAnagPosForm.xIDAutorizzatoDa;

                    Q1.ExecSQL;
                    DB.CommitTrans;
                    QStoricoPos.Close;
                    QStoricoPos.Open;
                    //[/TONI20020729\]FINE
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               end;
          end;
     end;
     ModStoricoAnagPosForm.Free;
end;

end.

