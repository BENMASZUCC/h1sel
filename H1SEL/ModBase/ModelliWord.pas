unit ModelliWord;

interface


uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, ShellApi, JclFileUtils,
     Main, ModuloDati, uUtilsVarie, ComObj, AdoDB, jpeg, DBCtrls, ExtCtrls, Word2000, IDGlobal, DB, uModelliWord_remote;


procedure CreaModelloEmergency_Old(xIDAnag, xIDCandRic: integer; xConnString: string);
procedure CreaModelloEmergency(xIDAnag, xIDCandRic: integer; xConnString: string);
procedure CreaMiniProfiliSCR(xDaDove: string; IDRicerca: integer; xIDs: string; xAnonimo: boolean; xConnString: string);
procedure CompilaPropostePres_cloud();
procedure CompilaCVSetUPJob(xIDanagrafica, xConnString: string);

implementation

uses SelRicCand, Splash3, ScegliAnagFile, uSendMailMAPI;

procedure CreaMiniProfiliSCR(xDaDove: string; IDRicerca: integer; xIDs: string; xAnonimo: boolean; xConnString: string);
var xFileWord, xModello, xContenuto, xLingue, xCampiXML, xFileRes, xNotaEstero, xAreePaesi, xAreePaesiVorrei, xNoteCV: string;
     Q1, Q2: TAdoQuery;
     xXML: TStringList;
begin
     //controllo se esiste il modello
     if xAnonimo then
          xModello := '\Modello_MiniProfilo_Anonimo.doc'
     else xModello := '\Modello_MiniProfilo.doc';

     if xDaDove = 'Commessa' then begin
          if xAnonimo then
               xFileWord := 'MiniProfili_commessa_' + IntToStr(IDRicerca) + '_anonimo.doc'
          else
               xFileWord := 'MiniProfili_commessa_' + IntToStr(IDRicerca) + '.doc';
     end else
          InputQuery('Salva con nome', 'Nome file', xFileWord);

     Q1 := TADOQuery.Create(nil);
     Q1.ConnectionString := xConnString;
     Q2 := TADOQuery.Create(nil);
     Q2.ConnectionString := xConnString;

     Q1.close;
     if xDaDove = 'Commessa' then
          Q1.SQL.Text := 'select R.ID ' +
               '	,A.ID IDAnagrafica ' +
               '        ,A.CVNumero ' +
               '	,R.Progressivo RifCommessa ' +
               '	,isnull(O.Rif,''(n.d.)'') RifOfferta ' +
               '	,upper(A.Cognome+'' ''+A.Nome) Cognome_Nome ' +
               '	,isnull(convert(nvarchar,A.DataNascita,105),''(n.d.)'') DataNascita ' +
               '	,isnull(Comune,''(n.d.)'') Citta ' +
               '	,isnull(Provincia,''(n.d.)'') Prov ' +
               '	,isnull(CR.Note2,''(n.d.)'') NoteCurriculum ' +
               'from EBC_CandidatiRicerche CR ' +
               'join EBC_Ricerche R on CR.IDRicerca = R.ID ' +
               'join Anagrafica A on CR.IDAnagrafica = A.ID ' +
               'join AnagAltreInfo AA on A.ID = AA.IDAnagrafica ' +
               'left join EBC_Offerte O on R.IDOfferta = O.ID ' +
               'where R.ID=' + inttostr(IDRicerca) +
               ' and A.ID in (' + xIDs + ') order by A.Cognome, A.Nome'
     else
          Q1.SQL.Text := 'select A.ID IDAnagrafica ' +
               ',A.CVNumero ' +
               ',''(n.d.)'' RifCommessa ' +
               ',upper(A.Cognome+'' ''+A.Nome) Cognome_Nome ' +
               ',isnull(convert(nvarchar,A.DataNascita,105),''(n.d.)'') DataNascita ' +
               ',isnull(Comune,''(n.d.)'') Citta ' +
               ',isnull(Provincia,''(n.d.)'') Prov ' +
               ',isnull(AA.NoteCurriculum,''(n.d.)'') NoteCurriculum ' +
               ',isnull(FlagEstero,0) FlagEstero ' +
               ',isnull(RuoloInteresseEstero,''(n.d.)'') RuoloInteresseEstero ' +
               ',isnull(AreaOperato1,'''') AreaOperato1 ' +
               ',isnull(AreaOperato2,'''') AreaOperato2 ' +
               ',isnull(AreaOperato3,'''') AreaOperato3 ' +
               ',isnull(PaesiOperato,'''') PaesiOperato ' +
               ',isnull(AreaOpererei1,'''') AreaOpererei1 ' +
               ',isnull(AreaOpererei2,'''') AreaOpererei2 ' +
               ',isnull(AreaOpererei3,'''') AreaOpererei3 ' +
               ',isnull(PaesiOpererei,'''') PaesiOpererei ' +
               ',isnull(TrasferteEstero,'''') TrasferteEstero ' +
               'from Anagrafica A ' +
               'join AnagAltreInfo AA on A.ID = AA.IDAnagrafica ' +
               'left join AnagCampiPers CP on A.ID = CP.IDAnagrafica ' +
               'where A.ID in (' + xIDs + ') ' +
               'order by A.Cognome, A.Nome';
     Q1.open;

     xCampiXML := '';
     while not Q1.EOF do begin
          xCampiXML := xCampiXML + '<record>' + chr(13);

          xCampiXML := xCampiXML + ' <campo etichetta="#NumOfferta" valore="' + Q1.FieldByName('RifCommessa').asString + '"/>' + chr(13);
          if xAnonimo then begin
               xCampiXML := xCampiXML + ' <campo etichetta="#NumeroCV" valore="' + Q1.FieldByName('CVNumero').asString + '"/>' + chr(13);
          end else begin
               xCampiXML := xCampiXML + ' <campo etichetta="#CognomeNome" valore="' + Q1.FieldByName('Cognome_Nome').asString + '"/>' + chr(13);
               xCampiXML := xCampiXML + ' <campo etichetta="#DataNascita" valore="' + Q1.FieldByName('DataNascita').asString + '"/>' + chr(13);
               xCampiXML := xCampiXML + ' <campo etichetta="#Citta" valore="' + Q1.FieldByName('Citta').asString + '"/>' + chr(13);
               xCampiXML := xCampiXML + ' <campo etichetta="#Prov" valore="' + Q1.FieldByName('Prov').asString + '"/>' + chr(13);
          end;

          // titolo di studio principale
          Q2.Close;
          Q2.SQL.text := 'select top 1 Ord, IDAnagrafica, TSP_Tipo, TSP_Titolo, TSP_Area, MassimoTitolo from ( ' +
               '	select 1 Ord, TS.IDAnagrafica, TS.MassimoTitolo ' +
               '		,isnull(upper(D.Tipo),''(n.d.)'') TSP_Tipo ' +
               '		,isnull(upper(D.Descrizione),''(n.d.)'') TSP_Titolo ' +
               '		,isnull(upper(AD.Area),''(n.d.)'') TSP_Area ' +
               '	from TitoliStudio TS ' +
               '	join Diplomi D on TS.IDDiploma = D.ID ' +
               '	left join AreeDiplomi AD on D.IDArea = AD.ID ' +
               '	where TS.IDAnagrafica = ' + Q1.FieldByName('IDAnagrafica').asString +
               '	and MassimoTitolo = 1 ' +
               '	UNION ' +
               '	select top 1 2 Ord, TS.IDAnagrafica, TS.MassimoTitolo ' +
               '		,isnull(upper(D.Tipo),''(n.d.)'') TSP_Tipo ' +
               '		,isnull(upper(D.Descrizione),''(n.d.)'') TSP_Titolo ' +
               '		,isnull(upper(AD.Area),''(n.d.)'') TSP_Area ' +
               '	from TitoliStudio TS ' +
               '	join Diplomi D on TS.IDDiploma = D.ID ' +
               '	left join AreeDiplomi AD on D.IDArea = AD.ID ' +
               '	left join GruppoDiplomi GD on D.IDGruppo = GD.ID ' +
               '	where TS.IDAnagrafica = ' + Q1.FieldByName('IDAnagrafica').asString +
               '	and (TS.MassimoTitolo is null or TS.MassimoTitolo=0) ' +
               '	order by GD.Codice ' +
               ') MaxTitolo';
          Q2.Open;

          xCampiXML := xCampiXML + ' <campo etichetta="#TSPTipo" valore="' + Q2.FieldByName('TSP_Tipo').asString + '"/>' + chr(13);
          xCampiXML := xCampiXML + ' <campo etichetta="#TSPTitolo" valore="' + Q2.FieldByName('TSP_Titolo').asString + '"/>' + chr(13);
          xCampiXML := xCampiXML + ' <campo etichetta="#TSPArea" valore="' + Q2.FieldByName('TSP_Area').asString + '"/>' + chr(13);

          // lingue
          Q2.Close;
          Q2.SQL.text := 'select LC.ID ' +
               '	,upper(L.Lingua) Lingua ' +
               '	,isnull(LL.LivelloConoscenza,''n.d.'') Livello ' +
               'from LingueConosciute LC ' +
               'join Lingue L on LC.IDLingua = L.ID ' +
               'left join LivelloLingue LL on LC.Livello = LL.ID ' +
               'where IDAnagrafica = ' + Q1.FieldByName('IDAnagrafica').asString;
          Q2.Open;
          xLingue := '';
          while not Q2.EOF do begin
               xLingue := xLingue + Q2.FieldByName('Lingua').asString + ' (' + Q2.FieldByName('Livello').asString + ')  ';
               Q2.Next;
          end;
          xCampiXML := xCampiXML + ' <campo etichetta="#Lingue" valore="' + xLingue + '"/>' + chr(13);

          // ultimo campo
          if (xDaDove = 'Motore') and (Q1.FieldByName('FlagEstero').asboolean) then begin

               xAreePaesi := '';
               if Q1.FieldByName('AreaOperato1').asString <> '' then
                    xAreePaesi := xAreePaesi + Q1.FieldByName('AreaOperato1').asString;
               if Q1.FieldByName('AreaOperato2').asString <> '' then
                    xAreePaesi := xAreePaesi + ', ' + Q1.FieldByName('AreaOperato2').asString;
               if Q1.FieldByName('AreaOperato3').asString <> '' then
                    xAreePaesi := xAreePaesi + ', ' + Q1.FieldByName('AreaOperato3').asString;
               if Q1.FieldByName('PaesiOperato').asString <> '' then
                    xAreePaesi := xAreePaesi + ', ' + Q1.FieldByName('PaesiOperato').asString;
               if xAreePaesi = '' then xAreePaesi := '(n.d.)';

               xAreePaesiVorrei := '';
               if Q1.FieldByName('AreaOpererei1').asString <> '' then
                    xAreePaesiVorrei := xAreePaesiVorrei + Q1.FieldByName('AreaOpererei1').asString;
               if Q1.FieldByName('AreaOpererei2').asString <> '' then
                    xAreePaesiVorrei := xAreePaesiVorrei + ', ' + Q1.FieldByName('AreaOpererei2').asString;
               if Q1.FieldByName('AreaOpererei3').asString <> '' then
                    xAreePaesiVorrei := xAreePaesiVorrei + ', ' + Q1.FieldByName('AreaOpererei3').asString;
               if Q1.FieldByName('PaesiOpererei').asString <> '' then
                    xAreePaesiVorrei := xAreePaesiVorrei + ', ' + Q1.FieldByName('PaesiOpererei').asString;
               if xAreePaesiVorrei = '' then xAreePaesiVorrei := '(n.d.)';

               xNotaEstero := Q1.FieldByName('RuoloInteresseEstero').asString + chr(13) +
                    'Il professionista � disponibile ad operare all''estero. Le aree o i paesi in cui ha maturato esperienza professionale sono: ' + chr(13) +
                    xAreePaesi + chr(13) +
                    'Egli � attualmente principalmente interessato a una nuova esperienza professionale nelle seguenti aree o paesi: ' + chr(13) +
                    xAreePaesiVorrei + chr(13) +
                    'Per ci� che concerne la tipologia e quantit� di giornate annuali all''estero, il professionista risulta disponibile a ';
               if Q1.FieldByName('TrasferteEstero').asString <> '' then
                    xNotaEstero := xNotaEstero + Q1.FieldByName('TrasferteEstero').asString
               else xNotaEstero := xNotaEstero + 'valutare le singole proposte';

               xNoteCV := xNotaEstero;
          end else
               xNoteCV := Q1.FieldByName('NoteCurriculum').asString;

               // sostituzione caratteri
          xNoteCV := StringReplace(xNoteCV, '&', '&#38;', [rfReplaceAll]);
          xNoteCV := StringReplace(xNoteCV, '"', '&#34;', [rfReplaceAll]);
          xNoteCV := StringReplace(xNoteCV, '<', '&#60;', [rfReplaceAll]);
          xNoteCV := StringReplace(xNoteCV, '>', '&#62;', [rfReplaceAll]);
          xCampiXML := xCampiXML + ' <campo etichetta="#NoteCV" valore="' + xNoteCV + '"/>' + chr(13);

          xCampiXML := xCampiXML + '</record>' + chr(13);
          Q1.Next;

     end;
     Q1.Close;
     Q1.Free;
     Q2.Free;

     // confezionamento XML
     xXML := TStringList.create;
     if xAnonimo then
          xXML.Text := '<?xml version="1.0" encoding="UTF-8"?> ' + chr(13) +
               '<operazioni> ' + chr(13) +
               '  <operazione nome="riempimodellowordricorsivo" nomefile="Modello_MiniProfilo_Anonimo" fileword="' + ExtractFileName(xFileWord) + '"> ' + chr(13) +
               xCampiXML +
               '  </operazione> ' + chr(13) +
               '</operazioni> '
     else
          xXML.Text := '<?xml version="1.0" encoding="UTF-8"?> ' + chr(13) +
               '<operazioni> ' + chr(13) +
               '  <operazione nome="riempimodellowordricorsivo" nomefile="Modello_MiniProfilo" fileword="' + ExtractFileName(xFileWord) + '"> ' + chr(13) +
               xCampiXML +
               '  </operazione> ' + chr(13) +
               '</operazioni> ';

     //xXML.saveToFile('c:\temp\test_ric.xml');
     xFileRes := RiempiModelloGenerico_remoto('Modello_MiniProfilo', xModello, ExtractFileName(xFileWord), xXML.text, '');

end;


procedure CreaModelloEmergency(xIDAnag, xIDCandRic: integer; xConnString: string);
var xFileWord, xgetfilepath, xFileBase, DescrizioneFile, xNome, xCognome, xNoteCurriculum, s, xperiodo: string;
     MSWord, MSWord1: variant;
     risp, xIDAnagFile, xContatore: integer;
     crearecord: boolean;
     QQuery, qQueryTemp, Q1: TAdoQuery;
     xeditmemo: TDBRichEdit;
     xsource: TDataSource;
     xnotecliente: string;
begin
     //controllo se esiste il modello
     xFileBase := GetDocPath + '\Summary_Emergency.doc';
     if not FileExists(xFileBase) then begin
          MessageDlg('Modello report non trovato - IMPOSSIBILE CONTINUARE', mtError, [mbOK], 0);
          exit;
     end;

     qQueryTemp := TADOQuery.Create(nil);
     qQueryTemp.ConnectionString := xConnString;
     qQueryTemp.close;

     //fede: tencnicamente si potevamo prendere i valori da data.tanagrafica e data.qanagnote senza rifare la query
     //ma poi non riuscivo a gestire le date quindi ho rifatto tutta la query con tutti i campi
     QQuery := TADOQuery.Create(nil);
     QQuery.ConnectionString := xConnString;
     QQuery.close;
     QQuery.SQL.Text := 'select Cognome, Nome, a.sesso ,ebc_stati.Stato StatoH1, isnull(ai.Nazionalita,'' '') Nazionalita, DataNascita, isnull(LuogoNascita,'' '') LuogoNascita,  ' +
          ' Cellulare, isnull(RecapitiTelefonici,'' '') RecapitiTelefonici, isnull(email,'' '') email, ' +
          ' DomicilioIndirizzo, DomicilioCap, DomicilioComune, DomicilioRegione ,DomicilioProvincia,  DomicilioStato, a.CVInseritoInData, ' +
          '  CVProvenienza, SpecCVProvenienza, ebc_clienti.Descrizione ProprietaCV, ai.Note, ai.NoteCurriculum, ' +
          ' a.CVDataAgg, ai.NoteCandidato ' +
          ' from anagrafica a join anagaltreinfo ai on ai.idanagrafica=a.id ' +
          ' join ebc_stati on ebc_stati.id=a.idstato ' +
          ' left join ebc_clienti on ebc_clienti.id=a.IDProprietaCV ' +
          ' left join StatiCivili sc on sc.id=a.idStatoCivile ' +
          ' where a.id=' + inttostr(xIDAnag);
     QQuery.open;

     xnome := QQuery.fieldbyname('Nome').asstring;
     xcognome := QQuery.fieldbyname('Cognome').asstring;

     xgetfilepath := GetPathFromTable('SchedaSintesi');



     try
          MsWord := CreateOleObject('Word.Application');
     except
          ShowMessage('Non riesco ad aprire Microsoft Word.');
          Exit;
     end;

     DescrizioneFile := xcognome + ' ' + xnome + ' - Summary.doc';
     //dove salvare il file
     MainForm.SaveDialog.Title := 'Dove vuoi salvare il file?';
     MainForm.SaveDialog.FileName := DescrizioneFile;
     MainForm.SaveDialog.Filter := 'Word (*.doc)|*.doc';
     if FileExists(DescrizioneFile) then begin
          if MessageDlg('Il file � gi� esistente' + chr(13) + 'Vuoi procedere Comunque?', mtWarning, [mbYes, mbNo], 0) = mrNo then
               exit;
     end;
     if MainForm.SaveDialog.Execute then begin
          xFileWord := MainForm.SaveDialog.FileName;
     end else exit;


     // INIZIO --> APERTURAFILE
     MSWord.documents.open(xFileBase);
     MsWord.Selection.Find.Forward := True;
     MsWord.Selection.Find.MatchAllWordForms := False;
     MsWord.Selection.Find.MatchCase := False;
     MsWord.Selection.Find.MatchWildcards := False;
     MsWord.Selection.Find.MatchSoundsLike := False;
     MsWord.Selection.Find.MatchWholeWord := False;
     MsWord.Selection.Find.MatchFuzzy := False;
     MsWord.Selection.Find.Wrap := wdFindContinue;
     MsWord.Selection.Find.Format := False;
     Msword.visible := true;
     MsWord.Selection.Find.ClearFormatting;

     MsWord.Selection.Find.Text := '#Nome';
     MsWord.Selection.Find.Replacement.Text := xnome + ' ';
     MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
     MsWord.Selection.Find.Text := '#Cognome';
     MsWord.Selection.Find.Replacement.Text := xCognome + ' ';
     MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
     MsWord.Selection.Find.Text := '#DataCreazione';
     MsWord.Selection.Find.Replacement.Text := datetostr(date) + ' ';
     MsWord.Selection.Find.Execute(Replace := wdReplaceAll);

     //ruoli
     qQueryTemp.close;
     qQueryTemp.SQL.Text := 'select m.Descrizione from anagmansioni am join mansioni m on m.id=am.idmansione ' +
          'where idanagrafica=' + inttostr(xIDAnag);
     qQueryTemp.Open;
     if qQueryTemp.RecordCount > 0 then begin
          MsWord.WordBasic.EditFind('#Ruoli');
          while not qQueryTemp.EOF do begin
               MsWord.WordBasic.Insert(qQueryTemp.fieldbyname('Descrizione').asstring);
               qQueryTemp.next;
               if not qQueryTemp.EOF then
                    MsWord.WordBasic.Insert(',')
               else MsWord.WordBasic.Insert('.');
          end;
     end else begin
          MsWord.Selection.Find.Text := '#Ruoli';
          MsWord.Selection.Find.Replacement.Text := ' ';
          MsWord.Selection.Find.Execute(Replace := wdReplaceAll)
     end;
     MsWord.Selection.Find.Text := '#Status';
     if QQuery.fieldbyname('StatoH1').asstring <> '' then
          MsWord.Selection.Find.Replacement.Text := QQuery.fieldbyname('StatoH1').asstring
     else
          MsWord.Selection.Find.Replacement.Text := ' ';
     MsWord.Selection.Find.Execute(Replace := wdReplaceAll);

     MsWord.Selection.Find.Text := '#DataNascita';
     if QQuery.fieldbyname('DataNascita').asstring <> '' then
          MsWord.Selection.Find.Replacement.Text := DatetoStr(QQuery.fieldbyname('DataNascita').AsDateTime)
     else
          MsWord.Selection.Find.Replacement.Text := ' ';
     MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
     MsWord.Selection.Find.Text := '#LuogoNascita';
     if QQuery.fieldbyname('LuogoNascita').asstring <> '' then
          MsWord.Selection.Find.Replacement.Text := QQuery.fieldbyname('LuogoNascita').asstring
     else
          MsWord.Selection.Find.Replacement.Text := ' ';
     MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
     MsWord.Selection.Find.Text := '#Sesso';
     if QQuery.fieldbyname('Sesso').asstring <> '' then
          MsWord.Selection.Find.Replacement.Text := QQuery.fieldbyname('Sesso').asstring
     else
          MsWord.Selection.Find.Replacement.Text := ' ';
     MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
     MsWord.Selection.Find.Text := '#Nazionalita';
     if QQuery.fieldbyname('Nazionalita').asstring <> '' then
          MsWord.Selection.Find.Replacement.Text := QQuery.fieldbyname('Nazionalita').asstring
     else
          MsWord.Selection.Find.Replacement.Text := ' ';
     MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
     MsWord.Selection.Find.Text := '#Cellulare';
     if QQuery.fieldbyname('Cellulare').asstring <> '' then
          MsWord.Selection.Find.Replacement.Text := QQuery.fieldbyname('Cellulare').asstring
     else
          MsWord.Selection.Find.Replacement.Text := ' ';
     MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
     MsWord.Selection.Find.Text := '#RecapitiTelefonici';
     if QQuery.fieldbyname('RecapitiTelefonici').asstring <> '' then
          MsWord.Selection.Find.Replacement.Text := QQuery.fieldbyname('RecapitiTelefonici').asstring
     else
          MsWord.Selection.Find.Replacement.Text := ' ';
     MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
     MsWord.Selection.Find.Text := '#Email';
     if QQuery.fieldbyname('Email').asstring <> '' then
          MsWord.Selection.Find.Replacement.Text := QQuery.fieldbyname('Email').asstring
     else
          MsWord.Selection.Find.Replacement.Text := ' ';
     MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
     MsWord.Selection.Find.Text := '#DomicilioIndirizzo';
     if QQuery.fieldbyname('DomicilioIndirizzo').asstring <> '' then
          MsWord.Selection.Find.Replacement.Text := QQuery.fieldbyname('DomicilioIndirizzo').asstring + ', '
     else
          MsWord.Selection.Find.Replacement.Text := ' ';
     MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
     MsWord.Selection.Find.Text := '#DomicilioCap';
     if QQuery.fieldbyname('DomicilioCap').asstring <> '' then
          MsWord.Selection.Find.Replacement.Text := QQuery.fieldbyname('DomicilioCap').asstring
     else
          MsWord.Selection.Find.Replacement.Text := ' ';
     MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
     MsWord.Selection.Find.Text := '#DomicilioComune';
     if QQuery.fieldbyname('DomicilioComune').asstring <> '' then
          MsWord.Selection.Find.Replacement.Text := QQuery.fieldbyname('DomicilioComune').asstring
     else
          MsWord.Selection.Find.Replacement.Text := ' ';
     MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
     { MsWord.Selection.Find.Text := '#DomicilioProvincia';
      if QQuery.fieldbyname('DomicilioProvincia').asstring <> '' then
           MsWord.Selection.Find.Replacement.Text := QQuery.fieldbyname('DomicilioProvincia').asstring
      else
           MsWord.Selection.Find.Replacement.Text := ' ';
      MsWord.Selection.Find.Execute(Replace := wdReplaceAll);   }
     MsWord.Selection.Find.Text := '#DomicilioRegione';
     if QQuery.fieldbyname('DomicilioRegione').asstring <> '' then
          MsWord.Selection.Find.Replacement.Text := ' (' + QQuery.fieldbyname('DomicilioRegione').asstring + '), '
     else
          MsWord.Selection.Find.Replacement.Text := ' ';
     MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
     MsWord.Selection.Find.Text := '#DomicilioStato';
     if QQuery.fieldbyname('DomicilioStato').asstring <> '' then
          MsWord.Selection.Find.Replacement.Text := QQuery.fieldbyname('DomicilioStato').asstring
     else
          MsWord.Selection.Find.Replacement.Text := ' ';
     MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
     MsWord.Selection.Find.Text := '#CVProvenienza';
     if QQuery.fieldbyname('CVProvenienza').asstring <> '' then
          MsWord.Selection.Find.Replacement.Text := QQuery.fieldbyname('CVProvenienza').asstring
     else
          MsWord.Selection.Find.Replacement.Text := ' ';
     MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
     MsWord.Selection.Find.Text := '#SpecCVProvenienza';
     if QQuery.fieldbyname('SpecCVProvenienza').asstring <> '' then
          MsWord.Selection.Find.Replacement.Text := QQuery.fieldbyname('SpecCVProvenienza').asstring
     else
          MsWord.Selection.Find.Replacement.Text := ' ';
     MsWord.Selection.Find.Execute(Replace := wdReplaceAll);

     //nome azienda dell'esperienza lavorativa attuale
     //se pi� d'una deve essere quella con flagattuale=1
     qQueryTemp.close;
     qQueryTemp.SQL.Text := ' declare @idanag as int ' +
          'set @idanag=' + inttostr(xIDAnag) +
          'if (select count(id) from esperienzelavorative where idanagrafica=@idanag and attuale=1)>0' +
          '     BEGIN  ' +
          '             select AziendaNome,AziendaComune,AziendaStato from esperienzelavorative where idanagrafica=@idanag and attuale=1 ' +
          '     END  ' +
          'if (select count(id) from esperienzelavorative where idanagrafica=@idanag and attuale=0)>=1' +
          '     BEGIN  ' +
          '             select AziendaNome,AziendaComune,AziendaStato from esperienzelavorative where idanagrafica=@idanag and attuale=0 ' +
          '     END ELSE BEGIN ' +
          '             select AziendaNome,AziendaComune,AziendaStato from esperienzelavorative where idanagrafica=@idanag and attuale=0 ' +
          //'             select AziendaNome,AziendaSede,AziendaStato from esperienzelavorative where idanagrafica=@idanag and attuale=0 ' +
     '     END  ';
     qQueryTemp.Open;
     if qQueryTemp.RecordCount > 0 then begin
          MsWord.Selection.Find.Text := '#AziendaNome';
          if qQueryTemp.fieldbyname('AziendaNome').asstring <> '' then
               MsWord.Selection.Find.Replacement.Text := qQueryTemp.fieldbyname('AziendaNome').asstring
          else
               MsWord.Selection.Find.Replacement.Text := ' ';
          MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
          MsWord.Selection.Find.Text := '#AziendaSede';
          if qQueryTemp.fieldbyname('AziendaComune').asstring <> '' then
               MsWord.Selection.Find.Replacement.Text := qQueryTemp.fieldbyname('AziendaComune').asstring
          else
               MsWord.Selection.Find.Replacement.Text := ' ';
          MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
          MsWord.Selection.Find.Text := '#AziendaStato';
          if qQueryTemp.fieldbyname('AziendaStato').asstring <> '' then
               MsWord.Selection.Find.Replacement.Text := qQueryTemp.fieldbyname('AziendaStato').asstring
          else
               MsWord.Selection.Find.Replacement.Text := ' ';
          MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
     end else begin
          MsWord.Selection.Find.Text := '#AziendaNome';
          MsWord.Selection.Find.Replacement.Text := ' ';
          MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
          MsWord.Selection.Find.Text := '#AziendaSede';
          MsWord.Selection.Find.Replacement.Text := ' ';
          MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
          MsWord.Selection.Find.Text := '#AziendaStato';
          MsWord.Selection.Find.Replacement.Text := ' ';
          MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
     end;

     // propriet� CV
     MsWord.Selection.Find.Text := '#ProprietaCV';
     if QQuery.fieldbyname('ProprietaCV').asstring <> '' then
          MsWord.Selection.Find.Replacement.Text := QQuery.fieldbyname('ProprietaCV').asstring + ' '
     else
          MsWord.Selection.Find.Replacement.Text := ' ';
     MsWord.Selection.Find.Execute(Replace := wdReplaceAll);

     //NOTE (non formatatte)
     xsource := TDataSource.Create(nil);
     xsource.DataSet := QQuery;
     xeditmemo := TDBRichEdit.Create(nil);
     xeditmemo.DataSource := xsource;
     xeditmemo.ParentWindow := mainform.Handle;
     xeditmemo.PlainText := False;
     xeditmemo.Visible := False;
     xeditmemo.DataField := 'Note';
     //showmessage(xeditmemo.Text);
     if xeditmemo.Text <> '' then begin
          MsWord.WordBasic.EditFind('#NoteInterne');
          MsWord.WordBasic.Insert(xeditmemo.Text);
     end else begin
          MsWord.Selection.Find.Text := '#NoteInterne';
          MsWord.Selection.Find.Replacement.Text := ' ';
          MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
     end;
     xsource.Free;
     xeditmemo.free;

     {if QQuery.fieldbyname('Note').asstring  <> '' then begin
          MsWord.WordBasic.EditFind('#NoteInterne');
          MsWord.WordBasic.Insert(QQuery.fieldbyname('Note').asstring);
     end else begin
          MsWord.Selection.Find.Text := '#NoteInterne';
          MsWord.Selection.Find.Replacement.Text := ' ';
          MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
     end;}

     // BEFORE INTERVIEW INFORMATION (short - see next pages for detailed CV and Q)
     MsWord.Selection.Find.Text := '#CVInseritoInData';
     if QQuery.fieldbyname('CVInseritoInData').asstring <> '' then
          MsWord.Selection.Find.Replacement.Text := DatetoStr(QQuery.fieldbyname('CVInseritoInData').AsDateTime) + ' '
     else
          MsWord.Selection.Find.Replacement.Text := ' ';
     MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
     MsWord.Selection.Find.Text := '#CVDataAgg';
     if QQuery.fieldbyname('CVDataAgg').asstring <> '' then
          MsWord.Selection.Find.Replacement.Text := DatetoStr(QQuery.fieldbyname('CVDataAgg').AsDateTime) + ' '
     else
          MsWord.Selection.Find.Replacement.Text := ' ';
     MsWord.Selection.Find.Execute(Replace := wdReplaceAll);

     qQueryTemp.close;
     qQueryTemp.SQL.Text := ' SELECT NoteCliente, MiniVal from EBC_CandidatiRicerche  where id=' + inttostr(xIDCandRic);
     qQueryTemp.open;
     if qQueryTemp.RecordCount > 0 then begin
          //MsWord.Selection.Find.Text := '#NoteCliente';
          MsWord.WordBasic.EditFind('#NoteCliente');
          if qQueryTemp.fieldbyname('NoteCliente').asstring <> '' then begin
               xnotecliente := qQueryTemp.fieldbyname('NoteCliente').asstring + ' ';
               MsWord.WordBasic.Insert(xnotecliente);
               //MsWord.Selection.Find.Replacement.Text := xnotecliente;
          end else
               //MsWord.Selection.Find.Replacement.Text := ' ';
               MsWord.WordBasic.Insert(' ');
          MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
          MsWord.Selection.Find.Text := '#MiniVal';
          if qQueryTemp.fieldbyname('MiniVal').asstring <> '' then
               MsWord.Selection.Find.Replacement.Text := qQueryTemp.fieldbyname('MiniVal').asstring + ' '
          else
               MsWord.Selection.Find.Replacement.Text := ' ';
          MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
     end else begin
          MsWord.Selection.Find.Text := '#NoteCliente';
          MsWord.Selection.Find.Replacement.Text := ' ';
          MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
          MsWord.Selection.Find.Text := '#MiniVal';
          MsWord.Selection.Find.Replacement.Text := ' ';
          MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
     end;

     //INTERVIEW INFORMATION
     qQueryTemp.close;
     qQueryTemp.SQL.Text := 'select top 1 DataEvento from storico where idevento=1239 and idanagrafica= ' + inttostr(xIDAnag) +
          'order by DataEvento desc';
     qQueryTemp.open;
     if qQueryTemp.RecordCount > 0 then begin
          MsWord.Selection.Find.Text := '#DataEvento';
          if QQueryTemp.fieldbyname('DataEvento').asstring <> '' then
               MsWord.Selection.Find.Replacement.Text := DatetoStr(qQueryTemp.fieldbyname('DataEvento').AsDateTime) + ' '
          else
               MsWord.Selection.Find.Replacement.Text := ' ';
          MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
     end else begin
          MsWord.Selection.Find.Text := '#DataEvento';
          MsWord.Selection.Find.Replacement.Text := ' ';
          MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
     end;
     //inglese
     qQueryTemp.close;
     qQueryTemp.SQL.Text := 'select Valore from competenzeanagrafica where idanagrafica=' + inttostr(xIDAnag) +
          ' and idcompetenza in (select id from competenze where descrizione like ''%English (tested%'') ';
     qQueryTemp.open;
     MsWord.Selection.Find.Text := '#TestInglese';
     if qQueryTemp.RecordCount > 0 then begin
          MsWord.Selection.Find.Replacement.Text := QQueryTemp.fieldbyname('Valore').AsString;
     end else
          MsWord.Selection.Find.Replacement.Text := '-';
     MsWord.Selection.Find.Execute(Replace := wdReplaceAll);

     //Francese
     qQueryTemp.close;
     qQueryTemp.SQL.Text := 'select Valore from competenzeanagrafica where idanagrafica=' + inttostr(xIDAnag) +
          ' and idcompetenza in (select id from competenze where descrizione like ''%French (tested%'') ';
     qQueryTemp.open;
     MsWord.Selection.Find.Text := '#TestFrancese';
     if qQueryTemp.RecordCount > 0 then begin
          MsWord.Selection.Find.Replacement.Text := QQueryTemp.fieldbyname('Valore').AsString;
     end else
          MsWord.Selection.Find.Replacement.Text := '-';
     MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
     {
    //Competences
    qQueryTemp.close;
    qQueryTemp.SQL.Text := 'select c.Descrizione competenze, ca.Valore, a.descrizione area from CompetenzeAnagrafica ca '+
    ' join Anagrafica ana on a.id = ca.IDAnagrafica '+
    ' join Competenze c on c.id = ca.IDCompetenza '+
    ' join Aree a on a.ID = c.IDArea '+
    ' where ana.id=' + inttostr(xIDAnag);
    qQueryTemp.open;

    if qQueryTemp.RecordCount > 0 then begin
         MsWord.WordBasic.EditFind('#Valutaz');
         while not qQueryTemp.eof do begin
              MsWord.WordBasic.Insert(qQueryTemp.fieldbyname('competenze').asstring);
              MsWord.WordBasic.NextCell;
              MsWord.WordBasic.Insert(qQueryTemp.fieldbyname('area').asstring);
              MsWord.WordBasic.NextCell;
              MsWord.WordBasic.Insert(qQueryTemp.fieldbyname('Valore').asstring);
              qQueryTemp.next;
              if not qQueryTemp.EOF then
                   MsWord.WordBasic.NextCell;
         end
    end else begin
         MsWord.Selection.Find.Text := '#Valutaz';
         MsWord.Selection.Find.Replacement.Text := ' ';
         MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
    end;

    }
    //ProjRecommended
     qQueryTemp.close;
     qQueryTemp.SQL.Text := 'select ProjRecommended from anagcampipers where idanagrafica=' + inttostr(xIDAnag);
     qQueryTemp.open;
     MsWord.Selection.Find.Text := '#ProjRecommended';
     if qQueryTemp.fieldbyname('ProjRecommended').asstring <> '' then
          MsWord.Selection.Find.Replacement.Text := qQueryTemp.fieldbyname('ProjRecommended').asstring + ' '
     else
          MsWord.Selection.Find.Replacement.Text := ' ';
     MsWord.Selection.Find.Execute(Replace := wdReplaceAll);

     //AvailabFrom (Availability starting)
     qQueryTemp.close;
     qQueryTemp.SQL.Text := 'select AvailabFrom from anagcampipers where idanagrafica=' + inttostr(xIDAnag);
     qQueryTemp.open;
     MsWord.Selection.Find.Text := '#AvailabFrom';
     if qQueryTemp.fieldbyname('AvailabFrom').asstring <> '' then
          MsWord.Selection.Find.Replacement.Text := qQueryTemp.fieldbyname('AvailabFrom').asstring + ' '
     else
          MsWord.Selection.Find.Replacement.Text := ' ';
     MsWord.Selection.Find.Execute(Replace := wdReplaceAll);

     //AvailabLength
     qQueryTemp.close;
     qQueryTemp.SQL.Text := 'select AvailabLength from anagcampipers where idanagrafica=' + inttostr(xIDAnag);
     qQueryTemp.open;
     MsWord.Selection.Find.Text := '#AvailabLength';
     if qQueryTemp.fieldbyname('AvailabLength').asstring <> '' then
          MsWord.Selection.Find.Replacement.Text := qQueryTemp.fieldbyname('AvailabLength').asstring + ' '
     else
          MsWord.Selection.Find.Replacement.Text := ' ';
     MsWord.Selection.Find.Execute(Replace := wdReplaceAll);

     // AvailabNotes
     qQueryTemp.close;
     qQueryTemp.SQL.Text := 'select AvailabNotes from anagcampipers where idanagrafica=' + inttostr(xIDAnag);
     qQueryTemp.open;
     MsWord.Selection.Find.Text := '#AvailabNotes';
     if qQueryTemp.fieldbyname('AvailabNotes').asstring <> '' then
          MsWord.Selection.Find.Replacement.Text := qQueryTemp.fieldbyname('AvailabNotes').asstring + ' '
     else
          MsWord.Selection.Find.Replacement.Text := ' ';
     MsWord.Selection.Find.Execute(Replace := wdReplaceAll);

     // EsperienzaPVS
     qQueryTemp.close;
     qQueryTemp.SQL.Text := 'select EsperienzaPVS from anagcampipers where idanagrafica=' + inttostr(xIDAnag);
     qQueryTemp.open;
     MsWord.Selection.Find.Text := '#EsperienzaPVS';
     if qQueryTemp.fieldbyname('EsperienzaPVS').asstring <> '' then
          MsWord.Selection.Find.Replacement.Text := qQueryTemp.fieldbyname('EsperienzaPVS').asstring + ' '
     else
          MsWord.Selection.Find.Replacement.Text := ' ';
     MsWord.Selection.Find.Execute(Replace := wdReplaceAll);


     //LastInterview
     qQueryTemp.close;
     qQueryTemp.SQL.Text := '  select top 1  ' +
          ' Dicitura= case idevento  ' +
          '	when (1220)  then   ''FIT'' ' +
          '	when (1238)  then   ''FIT'' ' +
          '	when (1330)  then   ''FIT'' ' +
          '	when (1530)  then   ''FIT'' ' +
          //'	when (1381)  then   ''FIT'' ' +
     '	when (1215)  then   ''UNFIT'' ' +
          '	when (1237)  then   ''UNFIT'' ' +
          '	when (1554)  then   ''UNFIT'' ' +
          '	when (1536)  then   ''UNFIT'' ' +
          '	when (1377)  then   ''UNFIT'' ' +
          //'	when (1382)  then   ''UNFIT'' ' +
     '	when (1239)  then   ''RECALL'' ' +
          '	when (1300)  then   ''RECALL'' ' +
          '	when (1549)  then   ''RECALL'' ' +
          '	when (1537)  then   ''RECALL'' ' +
          '	when (1376)  then   ''RECALL'' ' +
          //'	when (1380)  then   ''RECALL'' ' +
     ' end from storico where idevento in (1220, 1238, 1330, 1530, 1215, 1237, 1554, 1536, 1377, 1239, 1300, 1549, 1537, 1376) ' +
          ' and idanagrafica= ' + inttostr(xIDAnag) +
          ' order by dataEvento desc';
     qQueryTemp.open;

     MsWord.Selection.Find.Text := '#LastInterview';
     if qQueryTemp.fieldbyname('Dicitura').asstring <> '' then
          MsWord.Selection.Find.Replacement.Text := qQueryTemp.fieldbyname('Dicitura').asstring + ' '
     else
          MsWord.Selection.Find.Replacement.Text := ' ';
     MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
     if qQueryTemp.fieldbyname('Dicitura').asstring = 'RECALL' then begin
          qQueryTemp.close;
          qQueryTemp.SQL.Text := ' SELECT DescMotivoRichiamoCandidato,DataPrevRichiamo from EBC_CandidatiRicerche ' +
               ' left join MotiviRichiamoCandidati on MotiviRichiamoCandidati.id=EBC_CandidatiRicerche.IDMotivoRichiamoCandidato ' +
               ' where EBC_CandidatiRicerche.id=' + inttostr(xIDCandRic);
          qQueryTemp.open;
          MsWord.Selection.Find.Text := '#DescMotivoRichiamoCandidato';
          if qQueryTemp.fieldbyname('DescMotivoRichiamoCandidato').asstring <> '' then
               MsWord.Selection.Find.Replacement.Text := qQueryTemp.fieldbyname('DescMotivoRichiamoCandidato').asstring + ', '
          else
               MsWord.Selection.Find.Replacement.Text := ' ';
          MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
          MsWord.Selection.Find.Text := '#DataPrevRichiamo';
          if qQueryTemp.fieldbyname('DataPrevRichiamo').asstring <> '' then
               MsWord.Selection.Find.Replacement.Text := DatetoStr(qQueryTemp.fieldbyname('DataPrevRichiamo').AsDateTime) + ' '
          else
               MsWord.Selection.Find.Replacement.Text := ' ';
          MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
     end else begin
          MsWord.Selection.Find.Text := '#DescMotivoRichiamoCandidato';
          MsWord.Selection.Find.Replacement.Text := ' ';
          MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
          MsWord.Selection.Find.Text := '#DataPrevRichiamo';
          MsWord.Selection.Find.Replacement.Text := ' ';
          MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
     end;

     // eventi rilevanti   HISTORY WITH EMERGENCY
     MsWord.Selection.Find.Text := '#EventiRilevanti';
     qQueryTemp.close;
     qQueryTemp.SQL.Text := ' select s.DataEvento,ev.Evento, s.Annotazioni from storico s ' +
          ' join ebc_eventi ev on ev.id=s.idevento ' +
          ' where ev.FlagRilevante=1 and s.idanagrafica= ' + inttostr(xIDAnag) +
          ' order by s.dataEvento';
     qQueryTemp.open;
     if qQueryTemp.RecordCount > 0 then begin
          MsWord.WordBasic.EditFind('#EventiRilevanti');
          while not qQueryTemp.eof do begin
               MsWord.WordBasic.Insert(DatetoStr(qQueryTemp.fieldbyname('DataEvento').AsDateTime));
               MsWord.WordBasic.NextCell;
               MsWord.WordBasic.Insert(qQueryTemp.fieldbyname('Evento').asstring);
               MsWord.WordBasic.NextCell;
               MsWord.WordBasic.Insert(qQueryTemp.fieldbyname('Annotazioni').asstring);
               qQueryTemp.next;
               if not qQueryTemp.EOF then
                    MsWord.WordBasic.NextCell;
          end
     end else begin
          MsWord.Selection.Find.Text := '#EventiRilevanti';
          MsWord.Selection.Find.Replacement.Text := ' ';
          MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
     end;

     //Storico Dipendenti Organigramma  MISSIONS DONE
     Q1 := TADOQuery.Create(nil);
     Q1.ConnectionString := xConnString;

     xContatore := 1;
     qQueryTemp.close;
     qQueryTemp.SQL.Text := ' select distinct idorganigramma from StoricoDipOrg where iddipendente= ' + inttostr(xIDAnag);
     qQueryTemp.open;
     if qQueryTemp.RecordCount > 0 then begin
          MsWord.WordBasic.EditFind('#StDpOg');
          while not qQueryTemp.eof do begin
               Q1.close;
               Q1.sql.text := 'declare @IdDip As INT ' +
                    'declare @IdNodo As INT ' +
                    'SET @IdDip= ' + inttostr(xIDAnag) +
                    ' set @IdNodo=  ' + qQueryTemp.fieldbyname('idorganigramma').asString +
                    ' if ((select count(id) from  StoricoDipOrg where iddipendente =@IdDip and cancellato=0 and idorganigramma=@IdNodo)>=1) ' +
                    ' and ((select count(id) from  StoricoDipOrg where iddipendente =@IdDip and cancellato=1 and idorganigramma=@IdNodo)>=1) ' +
                    ' begin ' +
                    '  select az.Descrizione Azienda, m.Descrizione Mansione,s.Cancellato, s.Mese from ' +
                    ' (select * from StoricoDipOrg where iddipendente =@IdDip and idorganigramma=@IdNodo and cancellato=0 ' +
                    ' union ' +
                    ' select * from StoricoDipOrg where iddipendente =@IdDip and idorganigramma=@IdNodo and cancellato=1) s ' +
                    ' join organigramma o on o.id=s.idorganigramma ' +
                    ' left join aziende az on az.id=o.idazienda  ' +
                    ' left join mansioni m on m.id=o.idmansione  ' +
                    ' order by s.mese ' +
                    ' end  ';
               //  q1.sql.savetofile('q1.sql');
               Q1.open;
               while not q1.eof do begin
                    MsWord.WordBasic.Insert(inttostr(xContatore));
                    xContatore := xContatore + 1;
                    MsWord.WordBasic.NextCell;
                    MsWord.WordBasic.Insert(Q1.fieldbyname('Azienda').asstring);
                    MsWord.WordBasic.NextCell;
                    MsWord.WordBasic.Insert(Q1.fieldbyname('Mansione').asstring);
                    MsWord.WordBasic.NextCell;
                    MsWord.WordBasic.Insert(Q1.fieldbyname('Mese').asstring);
                    MsWord.WordBasic.NextCell;
                    q1.next;
                    MsWord.WordBasic.Insert(Q1.fieldbyname('Mese').asstring);
                    q1.next;
                    if not Q1.EOF then
                         MsWord.WordBasic.NextCell;
               end;
               qQueryTemp.next;
               if not qQueryTemp.EOF then
                    MsWord.WordBasic.NextCell;
          end;
     end else begin
          MsWord.Selection.Find.Text := '#StDpOg';
          MsWord.Selection.Find.Replacement.Text := ' ';
          MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
     end;
     q1.free;

     //Annunci
     qQueryTemp.close;
     qQueryTemp.SQL.Text := 'select AA.DataPervenuto, Ann.Rif ' +
          ' from Ann_AnagAnnEdizData AA join Ann_AnnEdizData AED on AA.IDAnnEdizData = AED.ID ' +
          ' join Ann_Edizioni Ediz on AED.IDEdizione = Ediz.ID ' +
          ' join Ann_Annunci Ann on AED.IDAnnuncio = Ann.ID ' +
          ' join Ann_Testate T on Ediz.IDTestata = T.ID ' +
          ' join EBC_Ricerche R on AA.IDRicerca = R.ID ' +
          ' where AA.idanagrafica=' + inttostr(xIDAnag) +
          ' order by AA.DataPervenuto';
     qQueryTemp.open;
     if qQueryTemp.RecordCount > 0 then begin
          MsWord.WordBasic.EditFind('#Annunci');
          while not qQueryTemp.eof do begin
               if qQueryTemp.fieldbyname('DataPervenuto').asstring <> '' then
                    //MsWord.WordBasic.Insert(DatetoStr(qQueryTemp.fieldbyname('DataPervenuto').AsDateTime))
                    MsWord.WordBasic.Insert(qQueryTemp.fieldbyname('DataPervenuto').asstring)
               else
                    MsWord.WordBasic.Insert(' ');
               MsWord.WordBasic.NextCell;
               if qQueryTemp.fieldbyname('Rif').asstring <> '' then
                    MsWord.WordBasic.Insert(qQueryTemp.fieldbyname('Rif').asstring)
               else
                    MsWord.WordBasic.Insert(' ');
               qQueryTemp.next;
               if not qQueryTemp.EOF then
                    MsWord.WordBasic.NextCell;
          end;
          qQueryTemp.First;
          while not qQueryTemp.eof do begin
               if qQueryTemp.fieldbyname('DataPervenuto').AsDateTime = QQuery.fieldbyname('CVInseritoInData').AsDateTime then begin
                    MsWord.Selection.Find.Text := '#AnnunciCVInseritoInData';
                    MsWord.Selection.Find.Replacement.Text := qQueryTemp.fieldbyname('Rif').asstring;
                    MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
               end;
               qQueryTemp.next;
          end;
     end else begin
          MsWord.Selection.Find.Text := '#Annunci';
          MsWord.Selection.Find.Replacement.Text := ' ';
          MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
          MsWord.Selection.Find.Text := '#AnnunciCVInseritoInData';
          MsWord.Selection.Find.Replacement.Text := ' ';
          MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
     end;

     {xsource := TDataSource.Create(nil);
     xsource.DataSet := QQuery;
     xeditmemo := TDBRichEdit.Create(nil);
     xeditmemo.DataSource := xsource;
     xeditmemo.ParentWindow := mainform.Handle;
     xeditmemo.PlainText := False;
     xeditmemo.Visible := False;
     xeditmemo.DataField := 'NoteCandidato';
     //showmessage(xeditmemo.Text);
     if xeditmemo.Text <> '' then begin
          MsWord.WordBasic.EditFind('#NoteCandidato');
          MsWord.WordBasic.Insert(xeditmemo.Text)
     end else begin
          MsWord.Selection.Find.Text := '#NoteCandidato';
          MsWord.Selection.Find.Replacement.Text := ' ';
          MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
     end;
     xsource.Free;
     xeditmemo.free; }
     if QQuery.fieldbyname('NoteCandidato').asstring <> '' then begin
          MsWord.WordBasic.EditFind('#NoteCandidato');
          MsWord.WordBasic.Insert(QQuery.fieldbyname('NoteCandidato').asstring);
     end else begin
          MsWord.Selection.Find.Text := '#NoteCandidato';
          MsWord.Selection.Find.Replacement.Text := ' ';
          MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
     end;

     //Lingue
        {il case nella query l'ho messo per crearmi un ordine, cosi lalingua inglese viene sempre come prima quella francese come seconda e tutte le altre dopo}
     qQueryTemp.close;
     qQueryTemp.SQL.Text := ' select l.lingua, ll.livelloconoscenza LivelloParlato, lls.livelloconoscenza LivelloScritto, case l.lingua ' +
          ' when ''French''  then 2 ' +
          ' when ''English'' then 1 ' +
          ' else 999 ' +
          ' end ordine  ' +
          ' from lingueconosciute lc ' +
          '  join lingue l on l.id=lc.idlingua ' +
          ' left join livellolingue ll on ll.id=lc.livello ' +
          ' left join livellolingue lls on lls.id=lc.livelloScritto ' +
          ' where idanagrafica=' + inttostr(xIDAnag) +
          ' order by ordine ';
     qQueryTemp.open;
     if qQueryTemp.RecordCount > 0 then begin
          MsWord.WordBasic.EditFind('#Lingue');
          while not qQueryTemp.eof do begin
               if qQueryTemp.fieldbyname('Lingua').asstring <> '' then
                    MsWord.WordBasic.Insert(qQueryTemp.fieldbyname('Lingua').asstring)
               else
                    MsWord.WordBasic.Insert(' ');
               MsWord.WordBasic.NextCell;
               if qQueryTemp.fieldbyname('LivelloParlato').asstring <> '' then
                    MsWord.WordBasic.Insert(qQueryTemp.fieldbyname('LivelloParlato').asstring)
               else
                    MsWord.WordBasic.Insert(' ');
               MsWord.WordBasic.NextCell;
               if qQueryTemp.fieldbyname('LivelloScritto').asstring <> '' then
                    MsWord.WordBasic.Insert(qQueryTemp.fieldbyname('LivelloScritto').asstring)
               else
                    MsWord.WordBasic.Insert(' ');
               qQueryTemp.next;
               if not qQueryTemp.EOF then
                    MsWord.WordBasic.NextCell;
          end;
     end else begin
          MsWord.Selection.Find.Text := '#Lingue';
          MsWord.Selection.Find.Replacement.Text := ' ';
          MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
     end;

     // Massimo Titolo Di studio
     qQueryTemp.close;
     qQueryTemp.SQL.Text := 'select d.Descrizione Diploma, ts.Specializzazione, ts.DataConseguimento, ts.NazioneConseg, ' +
          ' ts.LuogoConseguimento, ts.Note NoteMaxTitStudio, AD.Area AreaDiploma ' +
          ' from TitoliStudio ts ' +
          ' left join Diplomi D on d.id=ts.IDDiploma ' +
          ' left join AreeDiplomi AD on AD.ID = D.IDArea ' +
          //' where MassimoTitolo=1 and idanagrafica=' + inttostr(xIDAnag);
     ' where idanagrafica=' + inttostr(xIDAnag);
     qQueryTemp.open;
     if qQueryTemp.RecordCount > 0 then begin
          MsWord.WordBasic.EditFind('#AreaDiploma');
          while not qQueryTemp.eof do begin
               MsWord.WordBasic.Insert('Area of study:');
               MsWord.WordBasic.NextCell;
               if qQueryTemp.fieldbyname('Diploma').asstring <> '' then
                    MsWord.WordBasic.Insert(qQueryTemp.fieldbyname('Diploma').asstring)
               else
                    MsWord.WordBasic.Insert(' ');
               MsWord.WordBasic.NextCell;
               MsWord.WordBasic.Insert('Level:');
               MsWord.WordBasic.NextCell;
               if qQueryTemp.fieldbyname('AreaDiploma').asstring <> '' then
                    MsWord.WordBasic.Insert(qQueryTemp.fieldbyname('AreaDiploma').asstring)
               else
                    MsWord.WordBasic.Insert(' ');
               MsWord.WordBasic.NextCell;
               MsWord.WordBasic.Insert('Area of Specialty:');
               MsWord.WordBasic.NextCell;
               if qQueryTemp.fieldbyname('Specializzazione').asstring <> '' then
                    MsWord.WordBasic.Insert(qQueryTemp.fieldbyname('Specializzazione').asstring)
               else
                    MsWord.WordBasic.Insert(' ');
               MsWord.WordBasic.NextCell;
               MsWord.WordBasic.Insert('Completion date:');
               MsWord.WordBasic.NextCell;
               if qQueryTemp.fieldbyname('DataConseguimento').asstring <> '' then
                    MsWord.WordBasic.Insert(qQueryTemp.fieldbyname('DataConseguimento').asstring)
               else
                    MsWord.WordBasic.Insert(' ');
               MsWord.WordBasic.NextCell;
               MsWord.WordBasic.Insert('Obtained in (Country):');
               MsWord.WordBasic.NextCell;
               if qQueryTemp.fieldbyname('NazioneConseg').asstring <> '' then
                    MsWord.WordBasic.Insert(qQueryTemp.fieldbyname('NazioneConseg').asstring)
               else
                    MsWord.WordBasic.Insert(' ');
               MsWord.WordBasic.NextCell;
               MsWord.WordBasic.Insert('Obtained at (Institute):');
               MsWord.WordBasic.NextCell;
               if qQueryTemp.fieldbyname('LuogoConseguimento').asstring <> '' then
                    MsWord.WordBasic.Insert(qQueryTemp.fieldbyname('LuogoConseguimento').asstring)
               else
                    MsWord.WordBasic.Insert(' ');
               MsWord.WordBasic.NextCell;
               MsWord.WordBasic.Insert('Notes:');
               MsWord.WordBasic.NextCell;
               if qQueryTemp.fieldbyname('NoteMaxTitStudio').asstring <> '' then
                    MsWord.WordBasic.Insert(qQueryTemp.fieldbyname('NoteMaxTitStudio').asstring)
               else
                    MsWord.WordBasic.Insert(' ');
               qQueryTemp.next;
               if not qQueryTemp.EOF then begin
                    MsWord.WordBasic.NextCell;
                    MsWord.WordBasic.NextCell;
                    MsWord.WordBasic.NextCell;
               end;
          end;
     end else begin
          MsWord.Selection.Find.Text := '#AreaDiploma';
          MsWord.Selection.Find.Replacement.Text := ' ';
          MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
     end;


     // tutte le esperienze lavorative    JOB EXPERIENCES
     qQueryTemp.close;
     qQueryTemp.SQL.Text := ' select AziendaNome,IDSettore,at.Attivita Settore, el.AziendaSettore,AziendaComune, ' +
          ' AziendaStato, DurataMesi,Note,el.MeseDal,el.AnnoDal,el.MeseAl,el.AnnoAl ' +
          ' from EsperienzeLavorative el ' +
          ' left join ebc_attivita at on at.id=el.IDSettore where el.idanagrafica=' + inttostr(xIDAnag) +
          ' order by el.id';
     qQueryTemp.open;
     if qQueryTemp.RecordCount > 0 then begin
          MsWord.WordBasic.EditFind('#EspLavTutte');
          while not qQueryTemp.eof do begin
               MsWord.WordBasic.Insert('Hospital or Company:');
               MsWord.WordBasic.NextCell;
               if qQueryTemp.fieldbyname('AziendaNome').asstring <> '' then
                    MsWord.WordBasic.Insert(qQueryTemp.fieldbyname('AziendaNome').asstring)
               else
                    MsWord.WordBasic.Insert(' ');
               MsWord.WordBasic.NextCell;
               MsWord.WordBasic.Insert('Unit or Division:');
               MsWord.WordBasic.NextCell;
               if qQueryTemp.fieldbyname('AziendaSettore').asstring <> '' then
                    MsWord.WordBasic.Insert(qQueryTemp.fieldbyname('AziendaSettore').asstring)
               else
                    MsWord.WordBasic.Insert(' ');
               MsWord.WordBasic.NextCell;
               MsWord.WordBasic.Insert('Town:');
               MsWord.WordBasic.NextCell;
               if qQueryTemp.fieldbyname('AziendaComune').asstring <> '' then
                    MsWord.WordBasic.Insert(qQueryTemp.fieldbyname('AziendaComune').asstring)
               else
                    MsWord.WordBasic.Insert(' ');
               MsWord.WordBasic.NextCell;
               MsWord.WordBasic.Insert('Country:');
               MsWord.WordBasic.NextCell;
               if qQueryTemp.fieldbyname('AziendaStato').asstring <> '' then
                    MsWord.WordBasic.Insert(qQueryTemp.fieldbyname('AziendaStato').asstring)
               else
                    MsWord.WordBasic.Insert(' ');
               MsWord.WordBasic.NextCell;
               MsWord.WordBasic.Insert('Start / end date:');
               MsWord.WordBasic.NextCell;
               xperiodo := '';
               if qQueryTemp.fieldbyname('MeseDal').asstring <> '' then
                    xperiodo := xperiodo + qQueryTemp.fieldbyname('MeseDal').asstring + '/'
               else
                    xperiodo := xperiodo + '/';
               if qQueryTemp.fieldbyname('AnnoDal').asstring <> '' then
                    xperiodo := xperiodo + qQueryTemp.fieldbyname('AnnoDal').asstring + ' - '
               else
                    xperiodo := xperiodo + ' - ';
               if qQueryTemp.fieldbyname('MeseAl').asstring <> '' then
                    xperiodo := xperiodo + qQueryTemp.fieldbyname('MeseAl').asstring + '/'
               else
                    xperiodo := xperiodo + '/';
               if qQueryTemp.fieldbyname('AnnoAl').asstring <> '' then
                    xperiodo := xperiodo + qQueryTemp.fieldbyname('AnnoAl').asstring
               else
                    xperiodo := xperiodo + ' ';
               MsWord.WordBasic.Insert(xperiodo);
               MsWord.WordBasic.NextCell;
               MsWord.WordBasic.Insert('Lenght (years):');
               MsWord.WordBasic.NextCell;
               if qQueryTemp.fieldbyname('DurataMesi').asstring <> '' then
                    MsWord.WordBasic.Insert(qQueryTemp.fieldbyname('DurataMesi').asstring)
               else
                    MsWord.WordBasic.Insert(' ');
               MsWord.WordBasic.NextCell;
               MsWord.WordBasic.Insert('Notes:');
               MsWord.WordBasic.NextCell;
               if qQueryTemp.fieldbyname('Note').asstring <> '' then
                    MsWord.WordBasic.Insert(qQueryTemp.fieldbyname('Note').asstring)
               else
                    MsWord.WordBasic.Insert(' ');
               qQueryTemp.next;
               if not qQueryTemp.EOF then begin
                    MsWord.WordBasic.NextCell;
                    MsWord.WordBasic.NextCell;
                    MsWord.WordBasic.NextCell;
               end;
          end;
     end else begin
          MsWord.Selection.Find.Text := '#EspLavTutte';
          MsWord.Selection.Find.Replacement.Text := ' ';
          MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
     end;


     //NoteCurriculum     UPDATES FROM CANDIDATE
     {xsource := TDataSource.Create(nil);
     xsource.DataSet := QQuery;
     xeditmemo := TDBRichEdit.Create(nil);
     xeditmemo.DataSource := xsource;
     xeditmemo.ParentWindow := mainform.Handle;
     xeditmemo.PlainText := False;
     xeditmemo.Visible := False;
     xeditmemo.DataField := 'NoteCurriculum';
     //showmessage(xeditmemo.Text);
     if xeditmemo.Text <> '' then begin
          MsWord.WordBasic.EditFind('#NoteCurriculum');
          MsWord.WordBasic.Insert(xeditmemo.Text);
     end else begin
          MsWord.Selection.Find.Text := '#NoteCurriculum';
          MsWord.Selection.Find.Replacement.Text := ' ';
          MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
     end;
     xsource.Free;
     xeditmemo.free; }
     if QQuery.fieldbyname('NoteCurriculum').asstring <> '' then begin
          MsWord.WordBasic.EditFind('#NoteCurriculum');
          MsWord.WordBasic.Insert(QQuery.fieldbyname('NoteCurriculum').asstring);
     end else begin
          MsWord.Selection.Find.Text := '#NoteCurriculum';
          MsWord.Selection.Find.Replacement.Text := ' ';
          MsWord.Selection.Find.Execute(Replace := wdReplaceAll);
     end;

     //pi� di pagina
     MsWord.WordBasic.Viewfooter;
     MsWord.WordBasic.EditSelectAll;
     MsWord.WordBasic.EditFind('#Nome');
     MsWord.WordBasic.Insert(xNome);
     MsWord.WordBasic.EditFind('#Cognome');
     MsWord.WordBasic.Insert(xCognome);

     qquery.free;
     qQueryTemp.free;

     // FINE --> SALVATAGGIO
     MsWord.ActiveDocument.SaveAs(xfileword);
     MsWord.ActiveDocument.Close;
     MsWord.Quit;

     if FileExists(xfileword) then begin
          ShellExecute(0, 'Open', pchar(xfileword), '', '', SW_NORMAL)
     end else MessageDlg('Il file non esiste:' + chr(13) + xfileword, mtError, [mbOK], 0);
end;


procedure CreaModelloEmergency_Old(xIDAnag, xIDCandRic: integer; xConnString: string);
var xFileWord, xgetfilepath, xFileBase, DescrizioneFile, xNome, xCognome, s: string;
     MSWord: variant;
     risp, xIDAnagFile, xContatore: integer;
     crearecord: boolean;
     QQuery, qQueryTemp, Q1: TAdoQuery;
     xeditmemo: TDBRichEdit;
     xsource: TDataSource;
begin
     //controllo se esiste il modello
     xFileBase := GetDocPath + '\ModelloPresentazioneCand.doc';
     if not FileExists(xFileBase) then begin
          MessageDlg('Modello report non trovato - IMPOSSIBILE CONTINUARE', mtError, [mbOK], 0);
          exit;
     end;

     qQueryTemp := TADOQuery.Create(nil);
     qQueryTemp.ConnectionString := xConnString;
     qQueryTemp.close;

     //fede: tencnicamente si potevamo prendere i valori da data.tanagrafica e data.qanagnote senza rifare la query
     //ma poi non riuscivo a gestire le date quindi ho rifatto tutta la query con tutti i campi
     QQuery := TADOQuery.Create(nil);
     QQuery.ConnectionString := xConnString;
     QQuery.close;
     QQuery.SQL.Text := 'select Cognome, Nome ,ebc_stati.Stato StatoH1, isnull(ai.Nazionalita,'' '') Nazionalita, DataNascita, isnull(LuogoNascita,'' '') LuogoNascita,  ' +
          ' Cellulare, isnull(RecapitiTelefonici,'' '') RecapitiTelefonici, isnull(email,'' '') email, ' +
          ' DomicilioIndirizzo, DomicilioCap, DomicilioComune, DomicilioProvincia,  DomicilioStato, a.CVInseritoInData, ' +
          '  CVProvenienza, SpecCVProvenienza, ebc_clienti.Descrizione ProprietaCV, ai.Note, ai.NoteCurriculum ' +
          ' from anagrafica a join anagaltreinfo ai on ai.idanagrafica=a.id ' +
          ' join ebc_stati on ebc_stati.id=a.idstato ' +
          ' left join ebc_clienti on ebc_clienti.id=a.IDProprietaCV ' +
          ' left join StatiCivili sc on sc.id=a.idStatoCivile ' +
          ' where a.id=' + inttostr(xIDAnag);
     //QQuery.SQL.SaveToFile('Qquery.sql');
     QQuery.open;


     xnome := QQuery.fieldbyname('Nome').asstring;
     xcognome := QQuery.fieldbyname('Cognome').asstring;

     xgetfilepath := GetPathFromTable('SchedaSintesi');

     try
          MsWord := CreateOleObject('Word.Basic');
     except
          ShowMessage('Non riesco ad aprire Microsoft Word.');
          Exit;
     end;


     DescrizioneFile := 'Scheda sintesi di ' + xnome + ' ' + xcognome;
     //dove salvare il file
     MainForm.SaveDialog.Title := 'Dove vuoi salvare il file?';
     MainForm.SaveDialog.FileName := DescrizioneFile;
     MainForm.SaveDialog.Filter := 'Word (*.doc)|*.doc';
     if MainForm.SaveDialog.Execute then begin
          xFileWord := MainForm.SaveDialog.FileName;
     end else exit;


     // INIZIO --> APERTURAFILE
     MsWord.AppShow;
     MsWord.FileOpen(xFileBase); // file da aprire


     MsWord.EditFind('#Nome');
     MsWord.Insert(xnome);
     MsWord.EditFind('#Cognome');
     MsWord.Insert(xcognome);
     MsWord.EditFind('#DataCreazione');
     MsWord.Insert(datetostr(date));
     MsWord.EditFind('#Nome');
     MsWord.Insert(xnome);
     MsWord.EditFind('#Cognome');
     MsWord.Insert(xcognome);


     //ruoli
     qQueryTemp.close;
     qQueryTemp.SQL.Text := 'select m.Descrizione from anagmansioni am join mansioni m on m.id=am.idmansione ' +
          'where idanagrafica=' + inttostr(xIDAnag);
     qQueryTemp.Open;
     MsWord.EditFind('#ruoli');
     if qQueryTemp.RecordCount > 0 then begin
          while not qQueryTemp.EOF do begin
               MsWord.Insert(qQueryTemp.fieldbyname('Descrizione').asstring);

               qQueryTemp.next;
               if not qQueryTemp.EOF then begin
                    MsWord.NextCell;
                    MsWord.NextCell;
               end;
          end;
     end else
          MsWord.Insert(' ');
     //attenzione: graficamente su word lo status � nella stessa tabella dei ruoli
     MsWord.NextCell;
     MsWord.Insert('Status:');
     MsWord.NextCell;
     MsWord.Insert(qQuery.fieldbyname('StatoH1').asstring);


     MsWord.EditFind('#DataNascita');
     MsWord.Insert(QQuery.fieldbyname('DataNascita').asstring + ' ');
     MsWord.EditFind('#LuogoNascita');
     MsWord.Insert(QQuery.fieldbyname('LuogoNascita').asstring + ' ');
     MsWord.EditFind('#Nazionalita');
     MsWord.Insert(QQuery.fieldbyname('Nazionalita').asstring + ' ');

     MsWord.EditFind('#Cellulare');
     MsWord.Insert(QQuery.fieldbyname('Cellulare').asstring + ' ');
     MsWord.EditFind('#RecapitiTelefonici');
     MsWord.Insert(QQuery.fieldbyname('RecapitiTelefonici').asstring + ' ');
     MsWord.EditFind('#Mail');
     MsWord.Insert(QQuery.fieldbyname('eMail').asstring + ' ');

     MsWord.EditFind('#DomicilioIndirizzo');
     if QQuery.fieldbyname('DomicilioIndirizzo').asstring <> '' then
          MsWord.Insert(QQuery.fieldbyname('DomicilioIndirizzo').asstring)
     else
          MsWord.Insert('-');

     MsWord.EditFind('#DomicilioCap');
     if QQuery.fieldbyname('DomicilioCap').asstring <> '' then
          MsWord.Insert(QQuery.fieldbyname('DomicilioCap').asstring)
     else
          MsWord.Insert('-');

     MsWord.EditFind('#DomicilioComune');
     if QQuery.fieldbyname('DomicilioComune').asstring <> '' then
          MsWord.Insert(QQuery.fieldbyname('DomicilioComune').asstring)
     else
          MsWord.Insert('-');

     MsWord.EditFind('#DomicilioProv');
     if QQuery.fieldbyname('DomicilioProvincia').asstring <> '' then
          MsWord.Insert(QQuery.fieldbyname('DomicilioProvincia').asstring)
     else
          MsWord.Insert('-');

     MsWord.EditFind('#DomicilioStato');
     if QQuery.fieldbyname('DomicilioStato').asstring <> '' then
          MsWord.Insert(QQuery.fieldbyname('DomicilioStato').asstring)
     else
          MsWord.Insert('-');

     MsWord.EditFind('#CVProvenienza');
     if QQuery.fieldbyname('CVProvenienza').asstring <> '' then
          MsWord.Insert(QQuery.fieldbyname('CVProvenienza').asstring)
     else
          MsWord.Insert('-');

     MsWord.EditFind('#SpecCVProvenienza');
     if QQuery.fieldbyname('SpecCVProvenienza').asstring <> '' then
          MsWord.Insert(QQuery.fieldbyname('SpecCVProvenienza').asstring)
     else
          MsWord.Insert('-');

     //nome azienda dell'esperienza lavorativa attuale
     //se pi� d'una deve essere quella con flagattuale=1
     qQueryTemp.close;
     qQueryTemp.SQL.Text := ' declare @idanag as int ' +
          'set @idanag=' + inttostr(xIDAnag) +
          'if (select count(id) from esperienzelavorative where idanagrafica=@idanag)>1 ' +
          '	BEGIN  ' +
          ' select AziendaNome from esperienzelavorative where idanagrafica=@idanag and attuale=1 ' +
          '	END  ' +
          ' if (select count(id) from esperienzelavorative where idanagrafica=@idanag)=1 ' +
          '	BEGIN  ' +
          ' select AziendaNome from esperienzelavorative where idanagrafica=@idanag ' +
          '	END ';
     qQueryTemp.Open;

     MsWord.EditFind('#EspLavAttuale');
     if qQueryTemp.RecordCount > 0 then begin
          MsWord.Insert(QQueryTemp.fieldbyname('AziendaNome').asstring)
     end else
          MsWord.Insert('-');


     // propriet� CV
     MsWord.EditFind('#ProprietaCV');
     if QQuery.fieldbyname('ProprietaCV').asstring <> '' then
          MsWord.Insert(QQuery.fieldbyname('ProprietaCV').asstring)
     else
          MsWord.Insert('-');


     //NOTE (non formatatte)
     MsWord.EditFind('#Note');

     xsource := TDataSource.Create(nil);
     xsource.DataSet := QQuery;
     xeditmemo := TDBRichEdit.Create(nil);
     xeditmemo.DataSource := xsource;
     xeditmemo.ParentWindow := mainform.Handle;
     xeditmemo.PlainText := False;
     xeditmemo.Visible := False;
     xeditmemo.DataField := 'Note';
     //showmessage(xeditmemo.Text);
     MsWord.Insert(xeditmemo.Text);
     xsource.Free;
     xeditmemo.free;


     // BEFORE INTERVIEW INFORMATION (short - see next pages for detailed CV and Q)
     MsWord.EditFind('#CVInseritoInData');
     if QQuery.fieldbyname('CVInseritoInData').asstring <> '' then
          MsWord.Insert(QQuery.fieldbyname('CVInseritoInData').asstring + ' ')
     else
          MsWord.Insert('-');

     qQueryTemp.close;
     qQueryTemp.SQL.Text := ' SELECT NoteCliente, MiniVal from EBC_CandidatiRicerche  where id=' + inttostr(xIDCandRic);
     qQueryTemp.open;
     if qQueryTemp.RecordCount > 0 then begin
          MsWord.EditFind('#NoteCliente');
          if qQueryTemp.fieldbyname('NoteCliente').asstring <> '' then
               MsWord.Insert(qQueryTemp.fieldbyname('NoteCliente').asstring)
          else
               MsWord.Insert('-');

          MsWord.EditFind('#Minival');
          if qQueryTemp.fieldbyname('Minival').asstring <> '' then
               MsWord.Insert(qQueryTemp.fieldbyname('Minival').asstring)
          else
               MsWord.Insert('-');
     end;


     //INTERVIEW INFORMATION
     qQueryTemp.close;
     qQueryTemp.SQL.Text := 'select top 1 DataEvento from storico where idevento=1235 and idanagrafica= ' + inttostr(xIDAnag) +
          'order by DataEvento desc';
     qQueryTemp.open;
     MsWord.EditFind('#DataEvento');
     if qQueryTemp.RecordCount > 0 then begin
          MsWord.Insert(QQueryTemp.fieldbyname('DataEvento').asstring)
     end else
          MsWord.Insert('-');

     //inglese
     qQueryTemp.close;
     qQueryTemp.SQL.Text := 'select Valore from competenzeanagrafica where idanagrafica=' + inttostr(xIDAnag) +
          ' and idcompetenza in (select id from competenze where descrizione like ''%English (tested%'') ';
     qQueryTemp.open;
     MsWord.EditFind('#TestInglese');
     if QQuery.RecordCount > 0 then begin
          if QQueryTemp.fieldbyname('Valore').asInteger > 0 then
               MsWord.Insert('OK')
          else
               MsWord.Insert('NO');
     end else
          MsWord.Insert('NO');

     //Francese
     qQueryTemp.close;
     qQueryTemp.SQL.Text := 'select Valore from competenzeanagrafica where idanagrafica=' + inttostr(xIDAnag) +
          ' and idcompetenza in (select id from competenze where descrizione like ''%French (tested%'') ';
     qQueryTemp.open;
     MsWord.EditFind('#TestFrancese');
     if QQuery.RecordCount > 0 then begin
          if QQueryTemp.fieldbyname('Valore').asInteger > 0 then
               MsWord.Insert('OK')
          else
               MsWord.Insert('NO');
     end else
          MsWord.Insert('NO');


     //ProjRecommended
     qQueryTemp.close;
     qQueryTemp.SQL.Text := 'select ProjRecommended from anagcampipers where idanagrafica=' + inttostr(xIDAnag);
     qQueryTemp.open;
     MsWord.EditFind('#ProjRecommended');
     if qQueryTemp.fieldbyname('ProjRecommended').asstring <> '' then
          MsWord.Insert(qQueryTemp.fieldbyname('ProjRecommended').asstring)
     else
          MsWord.Insert('-');


     //AvailabFrom (Availability starting)
     qQueryTemp.close;
     qQueryTemp.SQL.Text := 'select AvailabFrom from anagcampipers where idanagrafica=' + inttostr(xIDAnag);
     qQueryTemp.open;
     MsWord.EditFind('#AvailabFrom');
     if qQueryTemp.fieldbyname('AvailabFrom').asstring <> '' then
          MsWord.Insert(qQueryTemp.fieldbyname('AvailabFrom').asstring)
     else
          MsWord.Insert('-');


     //AvailabLength
     qQueryTemp.close;
     qQueryTemp.SQL.Text := 'select AvailabLength from anagcampipers where idanagrafica=' + inttostr(xIDAnag);
     qQueryTemp.open;
     MsWord.EditFind('#AvailabLength');
     if qQueryTemp.fieldbyname('AvailabLength').asstring <> '' then
          MsWord.Insert(qQueryTemp.fieldbyname('AvailabLength').asstring)
     else
          MsWord.Insert('-');


     // AvailabNotes
     qQueryTemp.close;
     qQueryTemp.SQL.Text := 'select AvailabNotes from anagcampipers where idanagrafica=' + inttostr(xIDAnag);
     qQueryTemp.open;
     MsWord.EditFind('#AvailabNotes');
     if qQueryTemp.fieldbyname('AvailabNotes').asstring <> '' then
          MsWord.Insert(qQueryTemp.fieldbyname('AvailabNotes').asstring)
     else
          MsWord.Insert('-');


     //LastInterview
     qQueryTemp.close;
     qQueryTemp.SQL.Text := '  select top 1  ' +
          ' Dicitura= case idevento  ' +
          '	when (1220)  then   ''FIT'' ' +
          '	when (1238)  then   ''FIT''  ' +
          '	when (1215)  then   ''UNFIT'' ' +
          '	when (1237)  then   ''UNFIT''  ' +
          '	when (1239)  then   ''RECALL'' ' +
          ' end from storico where idevento in (1215 , 1229, 1237, 1238, 1239) ' +
          ' and idanagrafica= ' + inttostr(xIDAnag) +
          ' order by dataEvento desc';
     qQueryTemp.open;
     MsWord.EditFind('#Dicitura');
     if qQueryTemp.fieldbyname('Dicitura').asstring <> '' then
          MsWord.Insert(qQueryTemp.fieldbyname('Dicitura').asstring)
     else
          MsWord.Insert('-');

     qQueryTemp.close;
     qQueryTemp.SQL.Text := ' SELECT DescMotivoRichiamoCandidato,DataPrevRichiamo from EBC_CandidatiRicerche ' +
          ' left join MotiviRichiamoCandidati on MotiviRichiamoCandidati.id=EBC_CandidatiRicerche.IDMotivoRichiamoCandidato ' +
          ' where EBC_CandidatiRicerche.id=' + inttostr(xIDCandRic);
     qQueryTemp.open;
     MsWord.EditFind('#DescMotivoRichiamoCandidato');
     if qQueryTemp.fieldbyname('DescMotivoRichiamoCandidato').asstring <> '' then
          MsWord.Insert(qQueryTemp.fieldbyname('DescMotivoRichiamoCandidato').asstring)
     else
          MsWord.Insert('-');

     MsWord.EditFind('#DataPrevRichiamo');
     if qQueryTemp.fieldbyname('DataPrevRichiamo').asstring <> '' then
          MsWord.Insert(qQueryTemp.fieldbyname('DataPrevRichiamo').asstring)
     else
          MsWord.Insert('-');


     //Storico Dipendenti Organigramma  MISSIONS DONE
     Q1 := TADOQuery.Create(nil);
     Q1.ConnectionString := xConnString;

     MsWord.EditFind('#StoricoDipOrg');
     xContatore := 1;
     qQueryTemp.close;
     qQueryTemp.SQL.Text := ' select distinct idorganigramma from StoricoDipOrg where iddipendente= ' + inttostr(xIDAnag);
     qQueryTemp.open;

     if qQueryTemp.RecordCount > 0 then begin
          while not qQueryTemp.eof do begin
               Q1.close;
               Q1.sql.text := 'declare @IdDip As INT ' +
                    'declare @IdNodo As INT ' +
                    'SET @IdDip= ' + inttostr(xIDAnag) +
                    ' set @IdNodo=  ' + qQueryTemp.fieldbyname('idorganigramma').asString +
                    ' if ((select count(id) from  StoricoDipOrg where iddipendente =@IdDip and cancellato=0 and idorganigramma=@IdNodo)>=1) ' +
                    ' and ((select count(id) from  StoricoDipOrg where iddipendente =@IdDip and cancellato=1 and idorganigramma=@IdNodo)>=1) ' +
                    ' begin ' +
                    '  select az.Descrizione Azienda, m.Descrizione Mansione,s.Cancellato, s.Mese from ' +
                    ' (select * from StoricoDipOrg where iddipendente =@IdDip and idorganigramma=@IdNodo and cancellato=0 ' +
                    ' union ' +
                    ' select * from StoricoDipOrg where iddipendente =@IdDip and idorganigramma=@IdNodo and cancellato=1) s ' +
                    ' join organigramma o on o.id=s.idorganigramma ' +
                    ' left join aziende az on az.id=o.idazienda  ' +
                    ' left join mansioni m on m.id=o.idmansione  ' +
                    ' order by s.mese ' +
                    ' end  ';
               //  q1.sql.savetofile('q1.sql');
               Q1.open;
               while not q1.eof do begin
                    MsWord.Insert(inttostr(xContatore));
                    xContatore := xContatore + 1;
                    MsWord.NextCell;
                    MsWord.Insert(Q1.fieldbyname('Azienda').asstring);
                    MsWord.NextCell;
                    MsWord.Insert(Q1.fieldbyname('Mansione').asstring);
                    MsWord.NextCell;
                    MsWord.Insert(Q1.fieldbyname('Mese').asstring);
                    MsWord.NextCell;
                    q1.next;
                    MsWord.Insert(Q1.fieldbyname('Mese').asstring);
                    q1.next;
                    if not Q1.EOF then
                         MsWord.NextCell;
               end;

               qQueryTemp.next;
               if not qQueryTemp.EOF then
                    MsWord.NextCell;

          end;
     end else
          MsWord.Insert(' -  ');

     q1.free;


     // eventi rilevanti   HISTORY WITH EMERGENCY
     qQueryTemp.close;
     qQueryTemp.SQL.Text := ' select s.DataEvento,ev.Evento, s.Annotazioni from storico s ' +
          ' join ebc_eventi ev on ev.id=s.idevento ' +
          ' where ev.FlagRilevante=1 and s.idanagrafica= ' + inttostr(xIDAnag);
     qQueryTemp.open;
     MsWord.EditFind('#EventiRilevanti');
     if qQueryTemp.RecordCount = 0 then
          MsWord.Insert(' -  ');
     while not qQueryTemp.eof do begin
          MsWord.Insert(qQueryTemp.fieldbyname('DataEvento').asstring);
          MsWord.NextCell;
          MsWord.Insert(qQueryTemp.fieldbyname('Evento').asstring);
          MsWord.NextCell;
          MsWord.Insert(qQueryTemp.fieldbyname('Annotazioni').asstring);

          qQueryTemp.next;
          if not qQueryTemp.EOF then
               MsWord.NextCell;

     end;


     // ATTACHED DOCUMENTS
     qQueryTemp.close;
     qQueryTemp.SQL.Text := '  select convert(varchar(10),DataCreazione,103) DataCreazione, SoloNome from anagfile ' +
          ' where Idtipo in ( ' +
          ' select ID from TipiFileCand where tipo like ''%Interview Form%'') ' +
          ' and idanagrafica= ' + inttostr(xIDAnag);
     qQueryTemp.open;
     MsWord.EditFind('#FileAllegatiInterviewForm');
     if qQueryTemp.RecordCount = 0 then
          MsWord.Insert(' -  ');
     while not qQueryTemp.eof do begin
          MsWord.Insert(qQueryTemp.fieldbyname('DataCreazione').asstring);
          MsWord.NextCell;
          MsWord.Insert(qQueryTemp.fieldbyname('SoloNome').asstring);

          qQueryTemp.next;
          if not qQueryTemp.EOF then
               MsWord.NextCell;
     end;


     //Lingue
     {il case nella query l'ho messo per crearmi un ordine, cosi lalingua inglese viene sempre come prima quella francese come seconda e tutte le altre dopo}
     qQueryTemp.close;
     qQueryTemp.SQL.Text := ' select l.lingua, ll.livelloconoscenza LivelloParlato, lls.livelloconoscenza LivelloScritto, case l.lingua ' +
          ' when ''French''  then 2 ' +
          ' when ''English'' then 1 ' +
          ' else 999 ' +
          ' end ordine  ' +
          ' from lingueconosciute lc ' +
          '  join lingue l on l.id=lc.idlingua ' +
          ' left join livellolingue ll on ll.id=lc.livello ' +
          ' left join livellolingue lls on lls.id=lc.livelloScritto ' +
          ' where idanagrafica=' + inttostr(xIDAnag) +
          ' order by ordine ';
     qQueryTemp.open;
     MsWord.EditFind('#Lingue');
     if qQueryTemp.RecordCount = 0 then
          MsWord.Insert(' -  ');
     while not qQueryTemp.eof do begin
          MsWord.Insert(qQueryTemp.fieldbyname('Lingua').asstring);
          MsWord.NextCell;
          MsWord.Insert(qQueryTemp.fieldbyname('LivelloParlato').asstring);
          MsWord.NextCell;
          MsWord.Insert(qQueryTemp.fieldbyname('LivelloScritto').asstring);

          qQueryTemp.next;
          if not qQueryTemp.EOF then
               MsWord.NextCell;
     end;


     // Massimo Titolo Di studio
     qQueryTemp.close;
     qQueryTemp.SQL.Text := 'select d.Descrizione Diploma, ts.Specializzazione, ts.DataConseguimento, ts.NazioneConseg, ts.LuogoConseguimento, ts.Note ' +
          ' from TitoliStudio ts ' +
          ' left join Diplomi D on d.id=ts.IDDiploma ' +
          ' where MassimoTitolo=1 and idanagrafica=' + inttostr(xIDAnag);
     qQueryTemp.open;

     MsWord.EditFind('#Diploma');
     if qQueryTemp.fieldbyname('Diploma').asstring <> '' then
          MsWord.Insert(qQueryTemp.fieldbyname('Diploma').asstring)
     else
          MsWord.Insert('-');

     MsWord.EditFind('#Specializzazione');
     if qQueryTemp.fieldbyname('Specializzazione').asstring <> '' then
          MsWord.Insert(qQueryTemp.fieldbyname('Specializzazione').asstring)
     else
          MsWord.Insert('-');

     MsWord.EditFind('#DataConseguimento');
     if qQueryTemp.fieldbyname('DataConseguimento').asstring <> '' then
          MsWord.Insert(qQueryTemp.fieldbyname('DataConseguimento').asstring)
     else
          MsWord.Insert('-');

     MsWord.EditFind('#NazioneConseg');
     if qQueryTemp.fieldbyname('NazioneConseg').asstring <> '' then
          MsWord.Insert(qQueryTemp.fieldbyname('NazioneConseg').asstring)
     else
          MsWord.Insert('-');

     MsWord.EditFind('#LuogoConseguimento');
     if qQueryTemp.fieldbyname('LuogoConseguimento').asstring <> '' then
          MsWord.Insert(qQueryTemp.fieldbyname('LuogoConseguimento').asstring)
     else
          MsWord.Insert('-');

     MsWord.EditFind('#NoteMaxTitStudio');
     if qQueryTemp.fieldbyname('Note').asstring <> '' then
          MsWord.Insert(qQueryTemp.fieldbyname('Note').asstring)
     else
          MsWord.Insert('-');



     // tutte le esperienze lavorative    JOB EXPERIENCES

     qQueryTemp.close;
     qQueryTemp.SQL.Text := ' select AziendaNome,IDSettore,at.Attivita Settore,AziendaComune, ' +
          ' AziendaStato, DurataMesi,Note ' +
          ' from EsperienzeLavorative el ' +
          ' left join ebc_attivita at on at.id=el.IDSettore where el.idanagrafica=' + inttostr(xIDAnag);
     qQueryTemp.open;

     MsWord.EditFind('#EspLavTutte');
     while not qQueryTemp.eof do begin
          MsWord.Insert('Hospital or Company:');
          MsWord.NextCell;
          MsWord.Insert(qQueryTemp.fieldbyname('AziendaNome').asstring);
          MsWord.NextCell;
          MsWord.Insert('Unit or Division:');
          MsWord.NextCell;
          MsWord.Insert(qQueryTemp.fieldbyname('Settore').asstring);
          MsWord.NextCell;
          MsWord.Insert('Town:');
          MsWord.NextCell;
          MsWord.Insert(qQueryTemp.fieldbyname('AziendaComune').asstring);
          MsWord.NextCell;
          MsWord.Insert('Country:');
          MsWord.NextCell;
          MsWord.Insert(qQueryTemp.fieldbyname('AziendaStato').asstring);
          MsWord.NextCell;
          MsWord.Insert('Lenght (years):');
          MsWord.NextCell;
          MsWord.Insert(qQueryTemp.fieldbyname('DurataMesi').asstring);
          MsWord.NextCell;
          MsWord.Insert('Notes:');
          MsWord.NextCell;
          MsWord.Insert(qQueryTemp.fieldbyname('Note').asstring);


          qQueryTemp.next;
          if not qQueryTemp.EOF then begin
               MsWord.NextCell;
               MsWord.NextCell;
               MsWord.NextCell;
          end;
     end;


     //NoteCurriculum     UPDATES FROM CANDIDATE
     MsWord.EditFind('#NoteCurriculum');
     if QQuery.fieldbyname('NoteCurriculum').asstring <> '' then
          MsWord.Insert(QQuery.fieldbyname('NoteCurriculum').asstring)
     else
          MsWord.Insert('-');



     // File CV
     qQueryTemp.close;
     qQueryTemp.SQL.Text := '  select convert(varchar(10),DataCreazione,103) DataCreazione, SoloNome from anagfile ' +
          ' where Idtipo in ( ' +
          ' select ID from TipiFileCand where tipo like ''%CV%'' or tipo like ''%CVscannerizzato%'' or tipo like ''%CVEsterno%'' or tipo like ''%CVEuropeo%'') ' +
          ' and idanagrafica= ' + inttostr(xIDAnag);
     qQueryTemp.open;
     MsWord.EditFind('#FileCV');
     if qQueryTemp.RecordCount = 0 then
          MsWord.Insert(' -  ');
     while not qQueryTemp.eof do begin
          MsWord.Insert(qQueryTemp.fieldbyname('DataCreazione').asstring);
          MsWord.NextCell;
          MsWord.Insert(qQueryTemp.fieldbyname('SoloNome').asstring);

          qQueryTemp.next;
          if not qQueryTemp.EOF then
               MsWord.NextCell;
     end;


     // File Questionnaire
     qQueryTemp.close;
     qQueryTemp.SQL.Text := '  select convert(varchar(10),DataCreazione,103) DataCreazione, SoloNome from anagfile ' +
          ' where Idtipo in ( ' +
          ' select ID from TipiFileCand where tipo like ''%Questionnaire%'' ) ' +
          ' and idanagrafica= ' + inttostr(xIDAnag);
     qQueryTemp.open;
     MsWord.EditFind('#FileQuestionnaire');
     if qQueryTemp.RecordCount = 0 then
          MsWord.Insert(' -  ');
     while not qQueryTemp.eof do begin
          MsWord.Insert(qQueryTemp.fieldbyname('DataCreazione').asstring);
          MsWord.NextCell;
          MsWord.Insert(qQueryTemp.fieldbyname('SoloNome').asstring);

          qQueryTemp.next;
          if not qQueryTemp.EOF then
               MsWord.NextCell;
     end;

     // File EOM Report
     qQueryTemp.close;
     qQueryTemp.SQL.Text := '  select convert(varchar(10),DataCreazione,103) DataCreazione, SoloNome from anagfile ' +
          ' where Idtipo in ( ' +
          ' select ID from TipiFileCand where tipo like ''%EOM Report%'' ) ' +
          ' and idanagrafica= ' + inttostr(xIDAnag);
     qQueryTemp.open;
     MsWord.EditFind('#FileEOMReport');
     if qQueryTemp.RecordCount = 0 then
          MsWord.Insert(' -  ');
     while not qQueryTemp.eof do begin
          MsWord.Insert(qQueryTemp.fieldbyname('DataCreazione').asstring);
          MsWord.NextCell;
          MsWord.Insert(qQueryTemp.fieldbyname('SoloNome').asstring);

          qQueryTemp.next;
          if not qQueryTemp.EOF then
               MsWord.NextCell;
     end;

     //pi� di pagina
     MsWord.Viewfooter;
     MsWord.EditSelectAll;
     MsWord.EditFind('#Nome');
     MsWord.Insert(xNome);
     MsWord.EditFind('#Cognome');
     MsWord.Insert(xCognome);

     qquery.free;
     qQueryTemp.free;

     // FINE --> SALVATAGGIO
     MsWord.FileSaveAs(xfileword);
     MsWord.FileClose;
     MsWord.AppClose;

end;


procedure CompilaPropostePres_cloud;
var xXML: TStringList;
     xModello, xFileWord, xFileRes, xCampiXML, xEspLav, xFileAdd, xFileID, xTempFileName, xRifCommessa, xCliente, xPosizione, xSelFileQuery, xDiplomi, xLingue, xEspLavAttuale: string;
     xFileID_int: integer;
begin

     SelRicCandForm := TSelRicCandForm.create(nil);
     SelRicCandForm.GBLingua.visible := true;
     SelRicCandForm.QRicCand.Close;
     SelRicCandForm.QRicCand.ParambyName['xIDAnag'] := Data.TAnagrafica.FieldByName('ID').AsINteger;
     SelRicCandForm.QRicCand.Open;
     if SelRicCandForm.QRicCand.RecordCount = 0 then begin
          MessageDlg('Non risultano ricerche attive associate al soggetto', mtError, [mbOK], 0);
          SelRicCandForm.Free;
          exit;
     end else
     begin
          SelRicCandForm.ShowModal;
          if SelRicCandForm.ModalResult = mrOK then begin

               Splash3Form := TSplash3Form.create(nil);
               Splash3Form.Show;
               Application.ProcessMessages;

               xCliente := SelRicCandForm.QRicCand.FieldByName('Cliente').asString;
               xPosizione := SelRicCandForm.QRicCand.FieldByName('Posizione').asString;

               Data.QTemp.Close;
               Data.QTemp.SQL.text := 'select Progressivo from EBC_Ricerche where ID = ' + SelRicCandForm.QRicCand.FieldByName('IDRicerca').asString;
               Data.QTemp.Open;
               xRifCommessa := Data.QTemp.FieldByName('Progressivo').asString;

               if (SelRicCandForm.CBLingua.text = 'ITA') then
                    xModello := 'ModCand_Proposition_ITA'
               else
                    xModello := 'ModCand_Proposition_ENG';


               xFileWord := 'pc' + SelRicCandForm.QRicCand.FieldByName('IDCandRic').asString + '.doc';

               //prova per header
              // xCampiXML := xCampiXML + '    <campo etichetta="#Cognome" tabellare="0" valore="' + Data.TAnagrafica.FieldByName('Cognome').asString  + '"/>' + chr(13);
              // xCampiXML := xCampiXML + '    <campo etichetta="#Nome" tabellare="0" valore="' + Data.TAnagrafica.FieldByName('Nome').asString  + '"/>' + chr(13);




               // campi
               xCampiXML := xCampiXML + '    <campo etichetta="#Cliente" tabellare="0" valore="' + SelRicCandForm.QRicCand.FieldByName('Cliente').asString + '"/>' + chr(13);
               xCampiXML := xCampiXML + '    <campo etichetta="#Candidato" tabellare="0" valore="' + Data.TAnagrafica.FieldByName('Cognome').asString + ' ' + Data.TAnagrafica.FieldByName('Nome').asString + '"/>' + chr(13);


                 // Inserimento Foto
               if Data.QAnagFotoFoto.asString <> '' then begin
                    xTempFileName := GetDocPath + '\Foto' + data.QAnagFotoExtFoto.Value;
                    Data.QAnagFotoFoto.SaveToFile(xTempFileName);

                    Application.ProcessMessages;
                    CopiaFileSulClient(xTempFileName);
                    Application.ProcessMessages;

                    DeleteFile(xTempFileName);

                    xCampiXML := xCampiXML + '    <campo etichetta="#InsertPicture" valore="' + GetCartellaTemp_remota + 'Foto' + data.QAnagFotoExtFoto.Value + '" top="70" left="10" width="90" height="100"/>' + chr(13);
                    xCampiXML := xCampiXML + '    <campo etichetta="#InsertPictureSP" tabellare="0" valore="' + chr(13) + chr(13) + chr(13) + chr(13) + chr(13) + chr(13) + '"/>' + chr(13);
               end else begin
                    xCampiXML := xCampiXML + '    <campo etichetta="#InsertPicture" valore="" top="550" left="10" width="130" height="150"/>' + chr(13);
                    xCampiXML := xCampiXML + '    <campo etichetta="#InsertPictureSP" tabellare="0" valore="' + chr(13) + '"/>' + chr(13);
               end;


               xCampiXML := xCampiXML + '    <campo etichetta="#LuogoNascita" tabellare="0" valore="' + Data.TAnagrafica.FieldByname('LuogoNascita').AsString + '"/>' + chr(13);
               xCampiXML := xCampiXML + '    <campo etichetta="#DataNascita" tabellare="0" valore="' + Data.TAnagrafica.FieldByname('DataNascita').AsString + '"/>' + chr(13);
               xCampiXML := xCampiXML + '    <campo etichetta="#Residenza" tabellare="0" valore="' + Data.TAnagrafica.FieldByname('TipoStrada').AsString + ' ' +
                    Data.TAnagrafica.FieldByname('Indirizzo').AsString + ' - ' + Data.TAnagrafica.FieldByname('Cap').AsString + ' - ' + Data.TAnagrafica.FieldByname('Comune').AsString +
                    '(' + Data.TAnagrafica.FieldByname('Provincia').AsString + ')' + '"/>' + chr(13);

              //  xCampiXML := xCampiXML + '    <campo etichetta="#Cittadinanza" tabellare="0" valore="' + Data.TAnagrafica.FieldByname('Cittadinanza').AsString + '"/>' + chr(13);
               xCampiXML := xCampiXML + '    <campo etichetta="#Nazionalita" tabellare="0" valore="' + Data.qAnagAltreInfo.FieldByname('Nazionalita').AsString + '"/>' + chr(13);
               xCampiXML := xCampiXML + '    <campo etichetta="#StatoCivile" tabellare="0" valore="' + Data.TAnagrafica.FieldByname('StatoCivile').AsString + '"/>' + chr(13);



               xCampiXML := xCampiXML + '    <campo etichetta="#Posizione" tabellare="0" valore="' + SelRicCandForm.QRicCand.FieldByName('Posizione').asString + '"/>' + chr(13);
               xCampiXML := xCampiXML + '    <campo etichetta="#RifCommessa" tabellare="0" valore="' + xRifCommessa + '"/>' + chr(13);

               xCampiXML := xCampiXML + '    <campo etichetta="#Cliente" tabellare="0" valore="' + SelRicCandForm.QRicCand.FieldByName('Cliente').asString + '"/>' + chr(13);
               xCampiXML := xCampiXML + '    <campo etichetta="#Posizione" tabellare="0" valore="' + SelRicCandForm.QRicCand.FieldByName('Posizione').asString + '"/>' + chr(13);
               xCampiXML := xCampiXML + '    <campo etichetta="#Candidato" tabellare="0" valore="' + Data.TAnagrafica.FieldByName('Cognome').asString + ' ' + Data.TAnagrafica.FieldByName('Nome').asString + '"/>' + chr(13);




               //studi/diplomi
               Data.QTemp.Close;
               xDiplomi := '';
               Data.QTemp.SQL.text :=
                    ' select ' +
                    ' ts.ID, d.Descrizione Diploma, ' +
                    ' ''conseguito presso ''+ LuogoConseguimento Presso,  ' +
                    ' ''nel '' +DataConseguimento  Data, ' +
                    ' ''con la votazione di ''+ Votazione Votazione  ' +
                    '   from TitoliStudio ts ' +
                    ' join Diplomi d on d.ID=ts.IDDiploma ' +
                    ' where ts.idanagrafica=' + Data.TAnagrafica.FieldByName('ID').asString;

               Data.QTemp.Open;
               while not Data.QTemp.EOF do begin

                    xDiplomi := xDiplomi + trim(Data.QTemp.FieldByName('Diploma').asString) + '$' + trim(Data.QTemp.FieldByName('Presso').asString) + '$' +
                         trim(Data.QTemp.FieldByName('Data').asString) + '$' + trim(Data.QTemp.FieldByName('Votazione').asString) + chr(13);


                    Data.QTemp.Next;
                    if not Data.QTemp.EOF then
                         xDiplomi := xDiplomi + '$';
               end;
               Data.QTemp.Close;
               xCampiXML := xCampiXML + '    <campo etichetta="#Studi" tabellare="0" valore="' + xDiplomi + '"/>' + chr(13);



               //lingue
               Data.QTemp.Close;
               Data.QTemp.SQL.text := '  select  lc.ID,  l.Lingua, ' +
                    '  llo.LivelloConoscenza LivelloOrale, ' +
                    '  lls.LivelloConoscenza LivelloScritto, ' +
                    '  llc.LivelloConoscenza LivelloComprensione  ' +
                    '   from LingueConosciute lc ' +
                    '   join Lingue l on l.id=lc.IdLingua ' +
                    '   left join LivelloLingue llo on llo.ID=lc.Livello ' +
                    '   left join LivelloLingue lls on lls.ID=lc.Livelloscritto ' +
                    '   left join LivelloLingue llc on llc.ID=lc.IDComprensione ' +
                    '  where lc.idanagrafica=' + Data.TAnagrafica.FieldByName('ID').asString;
               xLingue := '';
               Data.QTemp.Open;
               while not Data.QTemp.EOF do begin

                    xLingue := xLingue + Data.QTemp.FieldByName('Lingua').asString + '$' + ' Livello Orale:' + Data.QTemp.FieldByName('LivelloOrale').asString + chr(13) +
                         ' Livello Scritto:' + Data.QTemp.FieldByName('LivelloScritto').asString + chr(13) + ' Livello Comprensione:' + Data.QTemp.FieldByName('LivelloComprensione').asString + chr(13);


                    Data.QTemp.Next;
                    if not Data.QTemp.EOF then
                         xLingue := xLingue + '$';
               end;
               Data.QTemp.Close;
               xCampiXML := xCampiXML + '    <campo etichetta="#Lingue" tabellare="1" valore="' + xLingue + '"/>' + chr(13);


               // esperienze Lavorativa attuale
               Data.QTemp.Close;
               Data.QTemp.SQL.text := 'select e.ID,  E.AziendaNome, M.Descrizione Posizione, E.AnnoDal ' +
                    'from EsperienzeLavorative E ' +
                    'left outer join Mansioni M on E.IDMansione = M.ID ' +
                    'where e.Attuale=1 and  E.IDAnagrafica = ' + Data.TAnagrafica.FieldByName('ID').asString +
                    ' order by annoDal desc ';

               xEspLavAttuale := '';
               Data.QTemp.Open;
               while not Data.QTemp.EOF do begin

                    xEspLavAttuale := xEspLavAttuale + 'Dal ' + Data.QTemp.FieldByName('AnnoDal').asString + '$' + Data.QTemp.FieldByName('AziendaNome').asString + '$' +
                         Data.QTemp.FieldByName('Posizione').asString + chr(13);

                    Data.QTemp.Next;
                    if not Data.QTemp.EOF then
                         xEspLavAttuale := xEspLavAttuale + '$';
               end;
               Data.QTemp.Close;
               xCampiXML := xCampiXML + '    <campo etichetta="#EspLavAttuale" tabellare="0" valore="' + xEspLavAttuale + '"/>' + chr(13);


               if not Data.QAnagNote.active then Data.QAnagNote.Open;

               xCampiXML := xCampiXML + '    <campo etichetta="#Inquadramento" tabellare="0" valore="' + Data.QAnagNote.FieldByname('Inquadramento').AsString + '"/>' + chr(13);
               xCampiXML := xCampiXML + '    <campo etichetta="#RAL" tabellare="0" valore="' + Data.QAnagNote.FieldByname('Retribuzione').AsString + '"/>' + chr(13);
               xCampiXML := xCampiXML + '    <campo etichetta="#Bonus" tabellare="0" valore="' + Data.QAnagNote.FieldByname('RetribVariabile').AsString + '"/>' + chr(13);
               xCampiXML := xCampiXML + '    <campo etichetta="#Benefits" tabellare="0" valore="' + Data.QAnagNote.FieldByname('Benefits').AsString + '"/>' + chr(13);


               // esperienze Lavorative
               Data.QTemp.Close;
               Data.QTemp.SQL.text := 'select distinct E.AziendaNome, M.Descrizione Posizione, E.AnnoDal ' +
                    'from EsperienzeLavorative E ' +
                    'left outer join Mansioni M on E.IDMansione = M.ID ' +
                    'where e.Attuale<>1 and E.IDAnagrafica = ' + Data.TAnagrafica.FieldByName('ID').asString +
                    ' order by annoDal desc ';

               xEspLav := '';
               Data.QTemp.Open;
               while not Data.QTemp.EOF do begin

                    xEspLav := xEspLav + 'Dal ' + Data.QTemp.FieldByName('AnnoDal').asString + '$' + Data.QTemp.FieldByName('AziendaNome').asString + '$' +
                         Data.QTemp.FieldByName('Posizione').asString + chr(13);


                    Data.QTemp.Next;
                    if not Data.QTemp.EOF then
                         xEspLav := xEspLav + '  $';
               end;
               Data.QTemp.Close;
               xCampiXML := xCampiXML + '    <campo etichetta="#EspLav" tabellare="0" valore="' + xEspLav + '"/>' + chr(13);



            {   if not Data.QAnagNote.active then Data.QAnagNote.Open;

               xCampiXML := xCampiXML + '    <campo etichetta="#Inquadramento" tabellare="0" valore="' + Data.QAnagNote.FieldByname('Inquadramento').AsString + '"/>' + chr(13);
               xCampiXML := xCampiXML + '    <campo etichetta="#RAL" tabellare="0" valore="' + Data.QAnagNote.FieldByname('Retribuzione').AsString + '"/>' + chr(13);
               xCampiXML := xCampiXML + '    <campo etichetta="#Bonus" tabellare="0" valore="' + Data.QAnagNote.FieldByname('RetribVariabile').AsString + '"/>' + chr(13);
               xCampiXML := xCampiXML + '    <campo etichetta="#Benefits" tabellare="0" valore="' + Data.QAnagNote.FieldByname('Benefits').AsString + '"/>' + chr(13);
                 }

               // FILE CV inserito alla fine
               xFileAdd := '';
               (*
               xFileID := '';
               xSelFileQuery := 'select AF.ID, T.Tipo+'' ''+AF.SoloNome+'' del ''+convert(nvarchar,AF.DataCreazione,105)+'' ''+AF.Descrizione Dic ' +
                    'from AnagFile AF ' +
                    'join TipiFileCand T on AF.IDTipo = T.ID ' +
                    'where AF.IDAnagrafica = ' + Data.TAnagrafica.FieldByName('ID').asString +
                    ' order by DataCreazione desc ';
               Data.QTemp.Close;
               Data.QTemp.SQL.text := xSelFileQuery;
               Data.QTemp.Open;
               if Data.QTemp.RecordCount = 0 then begin
                    xFileAdd := '';
               end else begin
                    ScegliAnagFileForm := TScegliAnagFileForm.Create(nil);
                    ScegliAnagFileForm.caption := 'Scegli file da allegare alla presentazione candidato';
                    ScegliAnagFileForm.showmodal;
                    if ScegliAnagFileForm.modalresult = mrOK then begin
                         if ScegliAnagFileForm.CBNessunFile.Checked then begin
                              xFileID := '';
                              xFileAdd := '';
                         end else
                              xFileID := data.QAnagFile.fieldByName('ID').asString;
                    end else begin
                         abort;
                         ScegliAnagFileForm.free;
                         Splash3Form.Hide;
                         Splash3Form.Free;
                         exit;
                    end;

                    ScegliAnagFileForm.free;

                    // xFileID := OpenSelFromQuery(xSelFileQuery, 'ID', 'Dic', 'File', '', False);
                    { if xFileID = '' then begin
                          abort;
                          Splash3Form.Hide;
                          Splash3Form.Free;
                          exit;
                          }
                    if xFileID <> '' then begin
                         Data.QTemp.Close;
                         Data.QTemp.SQL.text := 'select AF.ID, AF.SoloNome, AF.Descrizione, AF.DataCreazione ' +
                              'from AnagFile AF ' +
                              'where AF.ID = ' + xFileID;
                         Data.QTemp.Open;
                         xFileAdd := Data.QTemp.FieldByName('SoloNome').asString;
                    end;
               end;
               *)
               if xFileAdd <> '' then begin
                    // copiare il file in GetDocPath
                    data.QApriFile.Close;
                    data.QApriFile.Parameters.ParamByName('xIDFile').value := StrToInt(xFileID);
                    data.QApriFile.Open;
                    xTempFileName := GetDocPath + '\' + xFileAdd;
                    data.QApriFileDocFile.SaveToFile(xTempFileName);

                    Application.ProcessMessages;
                    CopiaFileSulClient(xTempFileName);
                    Application.ProcessMessages;

                    data.QApriFile.Close;

                    xFileAdd := GetCartellaTemp_remota + xFileAdd;

                    DeleteFile(xTempFileName);
               end;
               xCampiXML := xCampiXML + '    <campo etichetta="#InsertFile" tabellare="0" valore="' + xFileAdd + '"/>' + chr(13);

               // Inserimento Foto
             {  if Data.QAnagFotoFoto.asString <> '' then begin
                    xTempFileName := GetDocPath + '\Foto' + data.QAnagFotoExtFoto.Value;
                    Data.QAnagFotoFoto.SaveToFile(xTempFileName);

                    Application.ProcessMessages;
                    CopiaFileSulClient(xTempFileName);
                    Application.ProcessMessages;

                    DeleteFile(xTempFileName);

                    xCampiXML := xCampiXML + '    <campo etichetta="#InsertPicture" valore="' + GetCartellaTemp_remota + 'Foto' + data.QAnagFotoExtFoto.Value + '" top="550" left="10" width="130" height="150"/>' + chr(13);

               end else
                    xCampiXML := xCampiXML + '    <campo etichetta="#InsertPicture" valore="" top="550" left="10" width="130" height="150"/>' + chr(13);

                    }

               // confezionamento XML
               xXML := TStringList.create;
               xXML.Text := '<?xml version="1.0" encoding="UTF-8"?> ' + chr(13) +
                    '<operazioni> ' + chr(13) +
                    '  <operazione nome="RiempiModelloWord" nomefile="' + xModello + '" fileword="' + ExtractFileName(xFileWord) + '"> ' + chr(13) +
                    xCampiXML +
                    '  </operazione> ' + chr(13) +
                    '</operazioni> ';

               // vai
               xFileRes := RiempiModelloGenerico_remoto(xModello, xModello + '.doc', ExtractFileName(xFileWord), xXML.text, '');

               if xFileRes <> '' then begin

                  //  with Data do begin
                         //DB.BeginTrans;
                    try
                         Data.Q1.Close;
                         Data.Q1.SQL.text := 'insert into AnagFile (IDAnagrafica,IDTipo,Descrizione,NomeFile,DataCreazione,SoloNome,FileType) ' +
                              'values (:xIDAnagrafica:,:xIDTipo:,:xDescrizione:,:xNomeFile:,:xDataCreazione:,:xSoloNome:,:xFileType:)';
                         Data.Q1.ParambyName['xIDAnagrafica'] := Data.TAnagrafica.FieldByName('ID').AsInteger;
                         Data.Q1.ParambyName['xIDTipo'] := 3;
                         Data.Q1.ParambyName['xDescrizione'] := Data.TAnagrafica.FieldByname('Nome').AsString + ' ' + Data.TAnagrafica.FieldByname('Cognome').AsString + '/' +
                              xCliente + '/' + xPosizione;
                         Data.Q1.ParambyName['xNomeFile'] := ExtractFileName(xFileWord);
                         Data.Q1.ParambyName['xSoloNome'] := ExtractFileName(xFileWord);
                         Data.Q1.ParambyName['xDataCreazione'] := Date;
                         Data.Q1.ParambyName['xFileType'] := GetMIMETypeFromFile(xFileWord);
                         Data.Q1.ExecSQL;

                         xFileWord := GetDocPath + '\' + xFileWord;

                         MainForm.qFilePresentazioni.close;
                         MainForm.qfilepresentazioni.Parameters[0].Value := ExtractFileName(xFileWord);
                         MainForm.qFilePresentazioni.Open;
                         MainForm.qFilePresentazioni.Edit;
                         MainForm.qFilePresentazioniDocFile.LoadFromFile(xfileWord);
                         MainForm.qFilePresentazioniDocExt.Value := Stringreplace(ExtractFileExt(xfileWord), '.', '', [rfReplaceAll]);
                         MainForm.qFilePresentazioniFileBloccato.value := False;
                         MainForm.qFilePresentazioni.Post;
                              // DB.CommitTrans;

                         FileDelete(xfileWord, False);
                         Data.QAnagFile.close;
                         Data.QAnagFile.Open;
                    except
                              //   DB.RollbackTrans;
                         MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                         raise;
                    end;
                    //end;

               end;
               Splash3Form.Hide;
               Splash3Form.Free;

          end;

     end;
end;


procedure CompilaCVSetUPJob(xIDanagrafica, xConnString: string);
var xFileBase, xgetfilepath, xFileWord: string;
     MSWord: variant;
     xRiempi, xCrea: boolean;
     i: integer;
begin
  //controllo se esiste il modello
     xFileBase := GetDocPath + '\ModCandSetUP.docx';
     if not FileExists(xFileBase) then begin
          MessageDlg('Modello report non trovato - IMPOSSIBILE CONTINUARE', mtError, [mbOK], 0);
          exit;
     end;

     xgetfilepath := GetPathFromTable('PresentazioneCandidato');

     try
          MsWord := CreateOleObject('Word.Application');
     except
          ShowMessage('Non riesco ad aprire Microsoft Word.');
          Exit;
     end;

     if xgetfilepath = '' then
          xFileWord := GetDocPath + '\pc_setup' + data.tanagraficaid.AsString + '.doc'
     else
          xFileWord := xgetfilepath + data.tanagraficaid.AsString + '.doc';


     xRiempi := True;
     xCrea := True;
     if data.Global.FieldByName('AnagFileDentroDB').AsBoolean then begin
          xFileWord := PathGetTempPath + ExtractFileName(xFileWord);
          MainForm.qFilePresentazioni.Close;
          MainForm.qFilepresentazioni.Parameters[0].Value := ExtractFileName(xFileWord);
          MainForm.qFilePresentazioni.Open;
          if MainForm.qFilePresentazioni.RecordCount > 0 then begin

               if MessageDlg('Il file esiste gi� - Aprirlo (Yes) o crearne uno nuovo, riscrivendo il vecchio (No) ?', mtWarning, [mbYes, mbNo], 0) = mrYes then begin
                    try
                         data.QApriFileDocFile.SaveToFile(xFileWord);
                         MainForm.qFilePresentazioni.Edit;
                         MainForm.qFilePresentazioniFileBloccato.value := True;
                         MainForm.qFilePresentazioniIDUtente.value := mainform.xIDUtenteAttuale;
                         MainForm.qFilePresentazioni.Post;
                         MainForm.qFilePresentazioni.Close
                    except
                    end;
                    MsWord.AppShow;
                    MsWord.FileOpen(xFileWord); // file da aprire
                    xRiempi := False; // non riempire i campi
               end else begin
                    MsWord.AppShow;
                    MsWord.FileOpen(xFileBase);
                    xcrea := false;
               end;

          end else begin
               MsWord.WordBasic.AppShow;
               MsWord.WordBasic.FileOpen(xFileBase);
          end;
     end else begin
          if FileExists(xFileWord) then begin
               if MessageDlg('Il file esiste gi� - Aprirlo (Yes) o crearne uno nuovo, riscrivendo il vecchio (No) ?', mtWarning, [mbYes, mbNo], 0) = mrYes then begin
                    MsWord.WordBasic.AppShow;
                    MsWord.WordBasic.FileOpen(xFileWord); // file da aprire
                    xRiempi := False; // non riempire i campi
               end else begin
                    MsWord.WordBasic.AppShow;
                    MsWord.WordBasic.FileOpen(xFileBase);
                    xcrea := false;
               end;
          end else begin
               MsWord.WordBasic.AppShow;
               MsWord.WordBasic.FileOpen(xFileBase);
          end;

     end;


     if xRiempi = true then begin
          MsWord.WordBasic.EditFind('#Nominativo');
          MsWord.WordBasic.Insert(data.TAnagraficaNome.AsString + ' ' + copy(data.TAnagraficaCognome.AsString, 1, 1));


          MsWord.WordBasic.EditFind('#Residenza');
          MsWord.WordBasic.Insert(data.TAnagraficaComune.AsString + ' (' + data.TAnagraficaProvincia.asstring + ')');


          MsWord.WordBasic.EditFind('#DataDiNascita');
          MsWord.WordBasic.Insert(data.TAnagraficaDataNascita.AsString);

           //titoli studio
          Data.Q1.close;
          Data.Q1.SQL.text := ' select Tipo,Descrizione as Titolo, LuogoConseguimento,DataConseguimento,Votazione ' +
               ' from TitoliStudio,Diplomi where TitoliStudio.IDDiploma=Diplomi.ID ' +
               ' and IDAnagrafica=' + Data.TAnagrafica.FieldByname('ID').asString +
               ' order by DataConseguimento ';
          Data.Q1.Open;
          i := 0;
          MsWord.WordBasic.EditFind('#TitoliStudio');
          while not Data.Q1.EOF do begin
               i := i + 1;

               MsWord.WordBasic.Insert(Data.Q1.FieldByName('Tipo').asString + ' - ' + Data.Q1.FieldByName('Titolo').asString);
               if Data.Q1.FieldByName('DataConseguimento').asString <> '' then
                    MsWord.WordBasic.Insert(' - ' + Data.Q1.FieldByName('DataConseguimento').asString);

               if Data.Q1.FieldByName('Votazione').asString <> '' then
                    MsWord.WordBasic.Insert(' - ' + Data.Q1.FieldByName('Votazione').asString);


               if i < Data.Q1.RecordCount then MsWord.Insert(#13);
               data.q1.next;
          end;



          //esperienze lavorative
          Data.Q1.Close;
          Data.Q1.SQL.text := ' select  ' +
               ' case attuale ' +
               ' when 1 then  cast(isnull(AnnoDal,'''') as varchar(10))+ '' - oggi: ''+ AziendaNome ' +
               ' else  cast(isnull(AnnoDal,'''') as varchar(10))+'' - ''+ cast(isnull(AnnoAl,'''') as varchar(10))+ '': ''+AziendaNome  '+
               ' end Periodo, ' +
               ' m.Descrizione Ruolo, ' +
               ' DescrizioneMansione  ' +
               '   from EsperienzeLavorative el ' +
               '   left join mansioni m on m.id=el.IDMansione ' +
               '  where idanagrafica=  ' + Data.TAnagrafica.FieldByName('ID').asString +
               ' order by AnnoDal desc';
          Data.Q1.Open;
          i := 0;
          MsWord.WordBasic.EditFind('#EsperienzeLavorative');
          while not Data.Q1.EOF do begin
               i := i + 1;

               MSWord.Selection.Font.Bold := true;
               MsWord.WordBasic.Insert(Data.Q1.FieldByName('Periodo').asString);
               MsWord.WordBasic.Insert(#13);
               MSWord.Selection.Font.Bold := false;
               MsWord.WordBasic.Insert(Data.Q1.FieldByName('Ruolo').asString);
               MsWord.WordBasic.Insert(#13);
               MsWord.WordBasic.Insert(Data.Q1.FieldByName('DescrizioneMansione').asString);

               if i < Data.Q1.RecordCount then begin
                MsWord.WordBasic.Insert(#13);
                MsWord.WordBasic.Insert(#13);
                 end;
               data.q1.next;
          end;

     end;



       // FINE --> SALVATAGGIO
     MsWord.WordBasic.FileSaveAs(xFileWord);
     MsWord.WordBasic.FileClose;
     MsWord.WordBasic.AppClose;
     if Data.Global.FieldByname('AnagFileDentroDB').Value then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text := 'insert into AnagFile (IDAnagrafica,IDTipo,Descrizione,NomeFile,DataCreazione,SoloNome,FileType) ' +
                         'values (:xIDAnagrafica:,:xIDTipo:,:xDescrizione:,:xNomeFile:,:xDataCreazione:,:xSoloNome:,:xFileType:)';
                    Q1.ParambyName['xIDAnagrafica'] := Data.TAnagrafica.FieldByName('ID').AsInteger;
                    Q1.ParambyName['xIDTipo'] := 3;
                    Q1.ParambyName['xDescrizione'] := Data.TAnagrafica.FieldByname('Nome').AsString + ' ' + Data.TAnagrafica.FieldByname('Cognome').AsString;
                    Q1.ParambyName['xNomeFile'] := ExtractFileName(xFileWord);
                    Q1.ParambyName['xSoloNome'] := ExtractFileName(xFileWord);
                    Q1.ParambyName['xDataCreazione'] := Date;
                    Q1.ParambyName['xFileType'] := GetMIMETypeFromFile(xFileWord);
                    Q1.ExecSQL;

                    MainForm.qFilePresentazioni.close;
                    MainForm.qfilepresentazioni.Parameters[0].Value := ExtractFileName(xFileWord);
                    MainForm.qFilePresentazioni.Open;
                    MainForm.qFilePresentazioni.Edit;
                    MainForm.qFilePresentazioniDocFile.LoadFromFile(xfileWord);
                    MainForm.qFilePresentazioniDocExt.Value := Stringreplace(ExtractFileExt(xfileWord), '.', '', [rfReplaceAll]);
                    MainForm.qFilePresentazioniFileBloccato.value := False;
                    MainForm.qFilePresentazioni.Post;
                    DB.CommitTrans;

                    FileDelete(xfileWord, False);
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               end;
               Data.QAnagFile.Close;
               Data.QAnagFile.Open;
          end;

     end else begin

               // aggiornamento AnagFile
          if xcrea = true then begin
               with Data do begin
                    DB.BeginTrans;
                    try
                         Q1.Close;
                         Q1.SQL.text := 'insert into AnagFile (IDAnagrafica,IDTipo,Descrizione,NomeFile,DataCreazione,SoloNome,FileType) ' +
                              'values (:xIDAnagrafica:,:xIDTipo:,:xDescrizione:,:xNomeFile:,:xDataCreazione:,:xSoloNome:,:xFileType:)';
                         Q1.ParambyName['xIDAnagrafica'] := Data.TAnagrafica.FieldByName('ID').AsInteger;
                         Q1.ParambyName['xIDTipo'] := 3;
                         Q1.ParambyName['xDescrizione'] := Data.TAnagrafica.FieldByname('Nome').AsString + ' ' + Data.TAnagrafica.FieldByname('Cognome').AsString;
                         Q1.ParambyName['xNomeFile'] := xFileWord;
                         Q1.ParambyName['xSoloNome'] := ExtractFileName(xFileWord);
                         Q1.ParambyName['xDataCreazione'] := Date;
                         Q1.ParambyName['xFileType'] := GetMIMETypeFromFile(xFileWord);
                         Q1.ExecSQL;
                         DB.CommitTrans;
                    except
                         DB.RollbackTrans;
                         MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                    end;
               end;
          end;

          Data.QAnagFile.Close;
          Data.QAnagFile.Open;
     end;

end; // fine procedura  CompilaCVSetUPJob


end.

