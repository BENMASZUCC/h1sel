object Test_MailRemotaForm: TTest_MailRemotaForm
  Left = 396
  Top = 154
  Width = 622
  Height = 492
  Caption = 'Test_MailRemotaForm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 93
    Width = 41
    Height = 13
    Caption = 'Oggetto:'
  end
  object Label2: TLabel
    Left = 8
    Top = 117
    Width = 30
    Height = 13
    Caption = 'Testo:'
  end
  object Label3: TLabel
    Left = 8
    Top = 181
    Width = 53
    Height = 13
    Caption = 'Destinatari:'
  end
  object Label4: TLabel
    Left = 8
    Top = 245
    Width = 74
    Height = 13
    Caption = 'Destinatari bcc:'
  end
  object Label5: TLabel
    Left = 8
    Top = 309
    Width = 37
    Height = 13
    Caption = 'Allegati:'
  end
  object Label6: TLabel
    Left = 8
    Top = 9
    Width = 51
    Height = 13
    Caption = 'Eseguibile:'
  end
  object Bevel1: TBevel
    Left = 8
    Top = 79
    Width = 577
    Height = 2
  end
  object Bevel2: TBevel
    Left = 8
    Top = 401
    Width = 577
    Height = 2
  end
  object Label7: TLabel
    Left = 8
    Top = 412
    Width = 72
    Height = 13
    Caption = 'File da copiare:'
  end
  object Label8: TLabel
    Left = 9
    Top = 434
    Width = 50
    Height = 13
    Caption = 'Copiare in:'
  end
  object Edit1: TEdit
    Left = 88
    Top = 90
    Width = 367
    Height = 21
    TabOrder = 0
    Text = 'Questo � l'#39'oggetto'
  end
  object Memo1: TMemo
    Left = 88
    Top = 117
    Width = 367
    Height = 59
    Lines.Strings = (
      'Questo � il testo'
      'FINE')
    TabOrder = 1
  end
  object Memo2: TMemo
    Left = 88
    Top = 181
    Width = 369
    Height = 57
    Lines.Strings = (
      'gborsari@ebcconsulting.com'
      'giulioborsari@tin.it')
    TabOrder = 2
  end
  object Memo3: TMemo
    Left = 88
    Top = 243
    Width = 369
    Height = 57
    Lines.Strings = (
      'mmmmmmmm@ebcconsulting.com')
    TabOrder = 3
  end
  object Memo4: TMemo
    Left = 88
    Top = 305
    Width = 369
    Height = 87
    Lines.Strings = (
      'c:\cv.doc')
    TabOrder = 4
  end
  object BitBtn1: TBitBtn
    Left = 463
    Top = 90
    Width = 125
    Height = 41
    Caption = 'Invia'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 5
    OnClick = BitBtn1Click
  end
  object Edit2: TEdit
    Left = 88
    Top = 5
    Width = 499
    Height = 21
    TabOrder = 6
    Text = 'c:\temp\InvioMailMAPI.exe'
  end
  object RadioGroup1: TRadioGroup
    Left = 88
    Top = 32
    Width = 225
    Height = 40
    Caption = 'Dove lanciare'
    Columns = 2
    ItemIndex = 1
    Items.Strings = (
      'locale'
      'remoto')
    TabOrder = 7
  end
  object CheckBox1: TCheckBox
    Left = 325
    Top = 45
    Width = 169
    Height = 17
    Caption = 'lancia solo l'#39'eseguibile'
    TabOrder = 8
  end
  object Button1: TButton
    Left = 488
    Top = 160
    Width = 75
    Height = 25
    Caption = 'Button1'
    TabOrder = 9
    OnClick = Button1Click
  end
  object Edit3: TEdit
    Left = 88
    Top = 409
    Width = 369
    Height = 21
    TabOrder = 10
    Text = 'c:\m69.xls'
  end
  object Edit4: TEdit
    Left = 88
    Top = 432
    Width = 369
    Height = 21
    TabOrder = 11
    Text = '\\tsclient\c\temp\m69.xls'
  end
  object BitBtn2: TBitBtn
    Left = 472
    Top = 414
    Width = 97
    Height = 33
    Caption = 'Copia'
    TabOrder = 12
    OnClick = BitBtn2Click
  end
  object CB_Debug: TCheckBox
    Left = 504
    Top = 46
    Width = 73
    Height = 17
    Caption = 'Debug'
    TabOrder = 13
  end
end
