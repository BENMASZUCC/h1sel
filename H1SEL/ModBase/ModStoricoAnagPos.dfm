object ModStoricoAnagPosForm: TModStoricoAnagPosForm
  Left = 482
  Top = 106
  BorderStyle = bsDialog
  Caption = 'Modifica dati storico posizione'
  ClientHeight = 253
  ClientWidth = 312
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel
    Left = 4
    Top = 213
    Width = 55
    Height = 13
    Caption = 'Annotazioni'
  end
  object Label1: TLabel
    Left = 4
    Top = 69
    Width = 48
    Height = 13
    Caption = 'Dalla data'
  end
  object Label2: TLabel
    Left = 4
    Top = 107
    Width = 41
    Height = 13
    Caption = 'Alla data'
  end
  object BitBtn1: TBitBtn
    Left = 216
    Top = 2
    Width = 94
    Height = 35
    TabOrder = 5
    OnClick = BitBtn1Click
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 216
    Top = 38
    Width = 94
    Height = 35
    Caption = 'Annulla'
    TabOrder = 6
    Kind = bkCancel
  end
  object ENote: TEdit
    Left = 4
    Top = 228
    Width = 305
    Height = 21
    MaxLength = 80
    TabOrder = 4
  end
  object DEAllaData: TDateEdit97
    Left = 4
    Top = 121
    Width = 119
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    ColorCalendar.ColorValid = clBlue
    DayNames.Monday = 'lu'
    DayNames.Tuesday = 'ma'
    DayNames.Wednesday = 'me'
    DayNames.Thursday = 'gi'
    DayNames.Friday = 've'
    DayNames.Saturday = 'sa'
    DayNames.Sunday = 'do'
    MonthNames.January = 'gennaio'
    MonthNames.February = 'febbraio'
    MonthNames.March = 'marzo'
    MonthNames.April = 'aprile'
    MonthNames.May = 'maggio'
    MonthNames.June = 'giugno'
    MonthNames.July = 'luglio'
    MonthNames.August = 'agosto'
    MonthNames.September = 'settembre'
    MonthNames.October = 'ottobre'
    MonthNames.November = 'novembre'
    MonthNames.December = 'dicembre'
    Options = [doButtonTabStop, doCanClear, doCanPopup, doIsMasked, doShowCancel, doShowToday]
  end
  object GroupBox1: TGroupBox
    Left = 4
    Top = 146
    Width = 205
    Height = 64
    Caption = 'Autorizzato da:'
    TabOrder = 3
    object SpeedButton1: TSpeedButton
      Left = 171
      Top = 14
      Width = 28
      Height = 26
      Hint = 'seleziona soggetto'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        33333333333333333333333333333333333333333333333333FF333333333333
        3000333333FFFFF3F77733333000003000B033333777773777F733330BFBFB00
        E00033337FFF3377F7773333000FBFB0E000333377733337F7773330FBFBFBF0
        E00033F7FFFF3337F7773000000FBFB0E000377777733337F7770BFBFBFBFBF0
        E00073FFFFFFFF37F777300000000FB0E000377777777337F7773333330BFB00
        000033333373FF77777733333330003333333333333777333333333333333333
        3333333333333333333333333333333333333333333333333333333333333333
        3333333333333333333333333333333333333333333333333333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = SpeedButton1Click
    end
    object ECogn2: TEdit
      Left = 7
      Top = 15
      Width = 161
      Height = 21
      ReadOnly = True
      TabOrder = 0
    end
    object ENome2: TEdit
      Left = 7
      Top = 37
      Width = 161
      Height = 21
      TabOrder = 1
    end
  end
  object GroupBox2: TGroupBox
    Left = 4
    Top = 2
    Width = 205
    Height = 64
    Caption = 'Soggetto'
    TabOrder = 0
    object SpeedButton2: TSpeedButton
      Left = 171
      Top = 14
      Width = 28
      Height = 26
      Hint = 'seleziona soggetto'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        33333333333333333333333333333333333333333333333333FF333333333333
        3000333333FFFFF3F77733333000003000B033333777773777F733330BFBFB00
        E00033337FFF3377F7773333000FBFB0E000333377733337F7773330FBFBFBF0
        E00033F7FFFF3337F7773000000FBFB0E000377777733337F7770BFBFBFBFBF0
        E00073FFFFFFFF37F777300000000FB0E000377777777337F7773333330BFB00
        000033333373FF77777733333330003333333333333777333333333333333333
        3333333333333333333333333333333333333333333333333333333333333333
        3333333333333333333333333333333333333333333333333333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = SpeedButton2Click
    end
    object ECogn1: TEdit
      Left = 7
      Top = 15
      Width = 161
      Height = 21
      Color = clYellow
      ReadOnly = True
      TabOrder = 0
    end
    object ENome1: TEdit
      Left = 7
      Top = 37
      Width = 161
      Height = 21
      Color = clYellow
      TabOrder = 1
    end
  end
  object DEDallaData: TDateEdit97
    Left = 4
    Top = 83
    Width = 100
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    ColorCalendar.ColorValid = clBlue
    DayNames.Monday = 'lu'
    DayNames.Tuesday = 'ma'
    DayNames.Wednesday = 'me'
    DayNames.Thursday = 'gi'
    DayNames.Friday = 've'
    DayNames.Saturday = 'sa'
    DayNames.Sunday = 'do'
    MonthNames.January = 'gennaio'
    MonthNames.February = 'febbraio'
    MonthNames.March = 'marzo'
    MonthNames.April = 'aprile'
    MonthNames.May = 'maggio'
    MonthNames.June = 'giugno'
    MonthNames.July = 'luglio'
    MonthNames.August = 'agosto'
    MonthNames.September = 'settembre'
    MonthNames.October = 'ottobre'
    MonthNames.November = 'novembre'
    MonthNames.December = 'dicembre'
    Options = [doButtonTabStop, doCanPopup, doIsMasked, doShowCancel, doShowToday]
  end
end
