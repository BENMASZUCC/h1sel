unit SelQualifContr;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, Grids, DBGrids, Db, DBTables, ADODB, U_ADOLinkCl,
     dxTL, dxDBCtrl, dxDBGrid, dxCntner, TB97, ExtCtrls;

type
     TSelQualifContrForm = class(TForm)
          QQualif_OLD: TQuery;
          dsQQualif: TDataSource;
          DBGrid1: TDBGrid;
          QQualif_OLDID: TAutoIncField;
          QQualif_OLDTipoContratto: TStringField;
          QQualif_OLDQualifica: TStringField;
          QQualif: TADOLinkedQuery;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1ID: TdxDBGridMaskColumn;
          dxDBGrid1TipoContratto: TdxDBGridMaskColumn;
          dxDBGrid1Qualifica: TdxDBGridMaskColumn;
          dxDBGrid1Contratto: TdxDBGridMaskColumn;
          QQualifID: TAutoIncField;
          QQualifTipoContratto: TStringField;
          QQualifQualifica: TStringField;
          QQualifContratto: TStringField;
          Panel1: TPanel;
          BitBtn1: TToolbarButton97;
          BitBtn2: TToolbarButton97;
          BitBtn3: TToolbarButton97;
          procedure BitBtn3Click(Sender: TObject);
          procedure BitBtn1Click(Sender: TObject);
          procedure BitBtn2Click(Sender: TObject);
          procedure FormShow(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     SelQualifContrForm: TSelQualifContrForm;

implementation

uses ModuloDati, TabContratti, Main;

{$R *.DFM}

procedure TSelQualifContrForm.BitBtn3Click(Sender: TObject);
begin
     TabContrattiForm := TTabContrattiForm.create(self);
     TabContrattiForm.ShowModal;
     TabContrattiForm.Free;
     QQualif.Close;
     QQualif.Open;
end;

procedure TSelQualifContrForm.BitBtn1Click(Sender: TObject);
begin
     SelQualifContrForm.ModalResult := mrok;
end;

procedure TSelQualifContrForm.BitBtn2Click(Sender: TObject);
begin
     SelQualifContrForm.ModalResult := mrCancel;
end;

procedure TSelQualifContrForm.FormShow(Sender: TObject);
begin
     //Grafica
     SelQualifContrForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
end;

end.

