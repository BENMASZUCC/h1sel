unit ContattoCand2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, DBDateTimePicker, DBCtrls, Mask, Buttons, ExtCtrls, DB,
  Grids, DBGrids;

type
  TContattoCand2Form = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Panel2: TPanel;
    RadioGroup2: TRadioGroup;
    GroupBox1: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    Data: TDateTimePicker;
    Ora: TMaskEdit;
    Label7: TLabel;
    DBEdit2: TDBEdit;
    Label8: TLabel;
    DBEdit4: TDBEdit;
    DBGrid1: TDBGrid;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure RadioGroup2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    xContPrec,xContNuovo:Integer;
    xQuery:string;
  end;

var
  ContattoCand2Form: TContattoCand2Form;

implementation

uses ModuloDati3;

{$R *.DFM}

procedure TContattoCand2Form.BitBtn1Click(Sender: TObject);
var xDavantiA,xTipoContatto:string;
begin
     if not (DataSel_EBC.DsContattiCand2.State in [dsInsert,dsEdit]) then
        DataSel_EBC.TContattiCand2.Edit;

     if RadioGroup2.Itemindex=-1 then showmessage('Attenzione, nessun esito !')
     else begin
        DataSel_EBC.TContattiCand2TipoContatto.value:='telefonico';
        DataSel_EBC.TCandRicAnag.FindKey([DataSel_EBC.TContattiCand2IDRicerca.Value,
                                          DataSel_EBC.TContattiCand2IDAnagrafica.Value]);
        //aggiornamento Contatto precedente ->Evaso
        DataSel_EBC.TContattiCand2.FindKey([xContPrec]);
        DataSel_EBC.TContattiCand2.Edit;
        DataSel_EBC.TContattiCand2Evaso.Value:=True;
        DataSel_EBC.TContattiCand2.Post;
     end;

     if RadioGroup2.Itemindex=0 then begin
        //da richiamare
        DataSel_EBC.TContattiCand2.FindKey([xContNuovo]);
        DataSel_EBC.TContattiCand2.Edit;
        DataSel_EBC.TContattiCand2RichiamareData.Value:=Data.Date;
        DataSel_EBC.TContattiCand2RichiamareOra.Value:=StrToTime(Ora.Text);
        DataSel_EBC.TCandRicAnag.Edit;
        DataSel_EBC.TCandRicAnagStato.Value:='da richiamare in data '+
                    DataSel_EBC.TContattiCand2RichiamareData.asString+' alle ore '+
                    DataSel_EBC.TContattiCand2RichiamareOra.asString;
        DataSel_EBC.TCandRicAnag.Post;
        // aggiornamento promemoria generale
        if xQuery='DaRic' then
        DataSel_EBC.TPromemoria.InsertRecord([DataSel_EBC.QTelDaRicSelIDUtente.Value,
                                              null,
                                              Date,
                                              DataSel_EBC.TContattiCand2RichiamareData.Value,
                                              'richiamare '+DataSel_EBC.TAnagCognome.asString+' alle ore '+
                                               DataSel_EBC.TContattiCand2RichiamareOra.asString+
                                               ' per ricerca '+DataSel_EBC.QTelDaRicSelProgressivo.asString+' ('+
                                               DataSel_EBC.QTelDaRicSelMansioneRic.asString+')',
                                              null,False])
        else
        DataSel_EBC.TPromemoria.InsertRecord([DataSel_EBC.QTelDaFareSelIDUtente.Value,
                                              null,
                                              Date,
                                              DataSel_EBC.TContattiCand2RichiamareData.Value,
                                              'richiamare '+DataSel_EBC.TAnagCognome.asString+' alle ore '+
                                               DataSel_EBC.TContattiCand2RichiamareOra.asString+
                                               ' per ricerca '+DataSel_EBC.QTelDaFareSelProgressivo.asString+' ('+
                                               DataSel_EBC.QTelDaFareSelMansioneRic.asString+')',
                                              null,False]);

     end;
     if RadioGroup2.Itemindex=1 then begin
        //deve richiamare
        DataSel_EBC.TContattiCand2.FindKey([xContNuovo]);
        DataSel_EBC.TContattiCand2.Edit;
        DataSel_EBC.TContattiCand2RichiamaData.Value:=Data.Date;
        DataSel_EBC.TContattiCand2RichiamaOra.Value:=StrToTime(Ora.Text);
        DataSel_EBC.TCandRicAnag.Edit;
        DataSel_EBC.TCandRicAnagStato.Value:='deve richiamare in data '+
                    DataSel_EBC.TContattiCand2RichiamaData.asString+' alle ore '+
                    DataSel_EBC.TContattiCand2RichiamaOra.asString;
        DataSel_EBC.TCandRicAnag.Post;
        // aggiornamento promemoria generale
        if xQuery='DaRic' then
        DataSel_EBC.TPromemoria.InsertRecord([DataSel_EBC.QTelDaRicSelIDUtente.Value,
                                              null,
                                              Date,
                                              DataSel_EBC.TContattiCand2RichiamaData.Value,
                                              'deve richiamare '+DataSel_EBC.TAnagCognome.asString+' alle ore '+
                                               DataSel_EBC.TContattiCand2RichiamaOra.asString+
                                               ' per ricerca '+DataSel_EBC.QTelDaRicSelProgressivo.asString+' ('+
                                               DataSel_EBC.QTelDaRicSelMansioneRic.asString+')',
                                              null,False])
        else
        DataSel_EBC.TPromemoria.InsertRecord([DataSel_EBC.QTelDaFareSelIDUtente.Value,
                                              null,
                                              Date,
                                              DataSel_EBC.TContattiCand2RichiamaData.Value,
                                              'deve richiamare '+DataSel_EBC.TAnagCognome.asString+' alle ore '+
                                               DataSel_EBC.TContattiCand2RichiamaOra.asString+
                                               ' per ricerca '+DataSel_EBC.QTelDaFareSelProgressivo.asString+' ('+
                                               DataSel_EBC.QTelDaFareSelMansioneRic.asString+')',
                                              null,False]);
     end;
     if RadioGroup2.Itemindex=2 then begin
        //fissazione colloquio
        xDavantia:=DataSel_EBC.TSelezionatoriNominativo.asString;
        DataSel_EBC.TColloqui.InsertRecord([DataSel_EBC.TSelezionatoriID.Value,
                                            Data.Date,StrToTime(Ora.Text)]);
        DataSel_EBC.TCandRicAnag.Edit;
        DataSel_EBC.TCandRicAnagStato.Value:='fissato colloquio per il giorno '+
                    DateToStr(Data.Date)+' alle ore '+Ora.Text+' ('+xDavantiA+')';
        DataSel_EBC.TCandRicAnag.Post;
        // aggiornamento promemoria generale
        if xQuery='DaRic' then
        DataSel_EBC.TPromemoria.InsertRecord([DataSel_EBC.QTelDaRicSelIDUtente.Value,
                                              null,
                                              Date,
                                              Data.Date,
                                              'colloquio con '+DataSel_EBC.TAnagCognome.asString+' alle ore '+
                                               Ora.Text+
                                               ' per ricerca '+DataSel_EBC.QTelDaRicSelProgressivo.asString+' ('+
                                               DataSel_EBC.QTelDaRicSelMansioneRic.asString+')',
                                              null,False])
        else
        DataSel_EBC.TPromemoria.InsertRecord([DataSel_EBC.QTelDaFareSelIDUtente.Value,
                                              null,
                                              Date,
                                              Data.Date,
                                              'colloquio con '+DataSel_EBC.TAnagCognome.asString+' alle ore '+
                                               Ora.Text+
                                               ' per ricerca '+DataSel_EBC.QTelDaFareSelProgressivo.asString+' ('+
                                               DataSel_EBC.QTelDaFareSelMansioneRic.asString+')',
                                              null,False]);
     end;

     if DataSel_EBC.DsContattiCand2.State in [dsInsert,dsEdit] then
        DataSel_EBC.TContattiCand2.Post;

     Close;
end;

procedure TContattoCand2Form.BitBtn2Click(Sender: TObject);
begin
     DataSel_EBC.TContattiCand.Cancel;
     Close;
end;

procedure TContattoCand2Form.RadioGroup2Click(Sender: TObject);
begin
     if Radiogroup2.ItemIndex=2 then DBGrid1.Visible:=True;
end;

end.
