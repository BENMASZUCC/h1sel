unit FaxPresentaz;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, ComObj,
     StdCtrls, Buttons, Grids, AdvGrid, DBGrids, Db, DBTables, ExtCtrls, ShellApi, idGlobal, JclFileUtils,
     Psock, NMsmtp, ADODB, U_ADOLinkCl, ImgList, ComCtrls, LookOut,
     SHFileOp, shdocvw, ModelCompositionTVD5b, MidItems, MSHTML, ActiveX,
     OleCtrls, BaseGrid, Registry, StrUtils, IdComponent, IdBaseComponent, IdMessage,
     TB97;

type
     TFaxPresentazForm = class(TForm)
          DsClienti: TDataSource;
          DsContattiCli: TDataSource;
          Label4: TLabel;
          Label5: TLabel;
          Label6: TLabel;
          Label7: TLabel;
          Label8: TLabel;
          Label9: TLabel;
          BatchMove1: TBatchMove;
          DsQAnag: TDataSource;
          Panel2: TPanel;
          ImageList1: TImageList;
          Panel8: TPanel;
          Panel9: TPanel;
          ASG1: TAdvStringGrid;
          Panel1: TPanel;
          StatusBar1: TStatusBar;
          DsTipiMailCand: TDataSource;
          PanCandidati: TPanel;
          ExpressButton1: TExpressButton;
          ExpressButton4: TExpressButton;
          Bevel2: TBevel;
          Label13: TLabel;
          PanClienti: TPanel;
          ExpressButton2: TExpressButton;
          ExpressButton3: TExpressButton;
          Bevel3: TBevel;
          Label14: TLabel;
          ExpressButton5: TExpressButton;
          ExpressButton6: TExpressButton;
          ExpressButton7: TExpressButton;
          QTipiMailCand: TADOLinkedQuery;
          QClienti: TADOLinkedQuery;
          QAnag: TADOLinkedQuery;
          QAnagAltreInfo: TADOLinkedQuery;
          QAnag2: TADOLinkedQuery;
          TContattiCli: TADOLinkedTable;
          TAnag: TADOLinkedTable;
          TStoricoIDX: TADOLinkedTable;
          QTitoliStudio: TADOLinkedQuery;
          TTitStudio: TADOLinkedTable;
          TAnagMansioni: TADOLinkedTable;
          SrcAnagMansioni: TADOLinkedQuery;
          TEspLav: TADOLinkedTable;
          Src: TADOLinkedTable;
          Dest: TADOLinkedTable;
          Q: TADOLinkedQuery;
          TLingue: TADOLinkedTable;
          TAnagComp: TADOLinkedTable;
          TLingueConosc: TADOLinkedQuery;
          SrcAnagComp: TADOLinkedQuery;
          SrcAnagEspLav: TADOLinkedQuery;
          QQuest: TADOQuery;
          DsQQuest: TDataSource;
          Q1: TADOQuery;
          CheckBox1: TCheckBox;
          PFiltro: TPanel;
          CBFiltroStatoCliente: TComboBox;
          QStatiCliente: TADOQuery;
          QStatiClienteID: TAutoIncField;
          QStatiClienteDescrizione: TStringField;
          Splitter1: TSplitter;
          Splitter2: TSplitter;
          Panel19: TPanel;
          SpeedButton4: TSpeedButton;
          Label20: TLabel;
          Label21: TLabel;
          BitBtn1: TToolbarButton97;
          PCSel: TPageControl;
          TSMailCand: TTabSheet;
          SpeedButton1: TSpeedButton;
          Panel4: TPanel;
          BitBtn2: TToolbarButton97;
          GroupBox1: TGroupBox;
          Label2: TLabel;
          Label3: TLabel;
          Label10: TLabel;
          Label11: TLabel;
          Bevel1: TBevel;
          Label12: TLabel;
          EHost: TEdit;
          EPort: TEdit;
          EUserID: TEdit;
          EName: TEdit;
          EAddress: TEdit;
          Panel13: TPanel;
          DBGrid2: TDBGrid;
          Panel14: TPanel;
          PanQuest: TPanel;
          Label22: TLabel;
          DBGrid4: TDBGrid;
          Panel3: TPanel;
          Panel18: TPanel;
          CBcandMailSMS: TCheckBox;
          EavvisoSMS: TEdit;
          CBDispatcher1: TComboBox;
          TabSheet1: TTabSheet;
          Panel12: TPanel;
          CBNominativo: TCheckBox;
          RGOpzioneExp: TRadioGroup;
          RGOpzioneCV: TRadioGroup;
          Panel5: TPanel;
          BitBtn4: TToolbarButton97;
          TSFax: TTabSheet;
          Panel6: TPanel;
          CBFax: TCheckBox;
          PanContatto: TPanel;
          Label1: TLabel;
          DBGrid1: TDBGrid;
          Panel7: TPanel;
          TSSms: TTabSheet;
          Label18: TLabel;
          Label19: TLabel;
          Label23: TLabel;
          Panel10: TPanel;
          Panel11: TPanel;
          ETestoSMS: TEdit;
          CBDispatcher2: TComboBox;
          RG_TipoSMS: TRadioGroup;
          EMittente: TEdit;
          TSClientiCVMail: TTabSheet;
          Label15: TLabel;
          SpeedButton3: TSpeedButton;
          Label16: TLabel;
          Panel15: TPanel;
          Panel16: TPanel;
          MMail: TMemo;
          CBCliMailCand: TCheckBox;
          EClimailOgg: TEdit;
          CBCliMailRifRic: TCheckBox;
          Panel17: TPanel;
          Label17: TLabel;
          DBGrid3: TDBGrid;
          GroupBox2: TGroupBox;
          CBCliMailCognNome: TCheckBox;
          RGCliMailOpz: TRadioGroup;
          BitBtn5: TToolbarButton97;
          BitBtn7: TToolbarButton97;
          BitBtn3: TToolbarButton97;
          BitBtn6: TToolbarButton97;
          SpeedButton5: TToolbarButton97;
          Panel20: TPanel;
          WebBrowser2: TWebBrowser;
          WebBrowser1: TWebBrowser;
          CBCliMailNomeTipoFile: TCheckBox;
          procedure CBFaxClick(Sender: TObject);
          procedure BitBtn3Click(Sender: TObject);
          procedure BitBtn4Click(Sender: TObject);
          procedure QAnag_OLDCalcFields(DataSet: TDataSet);
          procedure FormShow(Sender: TObject);
          procedure ExpressButton1Click(Sender: TObject);
          procedure ExpressButton2Click(Sender: TObject);
          procedure ExpressButton3Click(Sender: TObject);
          procedure ExpressButton4Click(Sender: TObject);
          procedure BitBtn2Click(Sender: TObject);
          {procedure NMSMTP1Connect(Sender: TObject);
          procedure NMSMTP1Disconnect(Sender: TObject);
          procedure NMSMTP1ConnectionFailed(Sender: TObject);
          procedure NMSMTP1Status(Sender: TComponent; Status: string);
          procedure NMSMTP1EncodeStart(Filename: string);
          procedure NMSMTP1EncodeEnd(Filename: string);
          procedure NMSMTP1Failure(Sender: TObject);
          procedure NMSMTP1HostResolved(Sender: TComponent);
          procedure NMSMTP1PacketSent(Sender: TObject);
          procedure NMSMTP1RecipientNotFound(Recipient: string);
          procedure NMSMTP1SendStart(Sender: TObject);
          procedure NMSMTP1Success(Sender: TObject);
          procedure NMSMTP1HeaderIncomplete(var handled: Boolean;
               hiType: Integer); }
          procedure SpeedButton1Click(Sender: TObject);
          procedure SpeedButton5Click(Sender: TObject);
          procedure ExpressButton5Click(Sender: TObject);
          procedure BitBtn6Click(Sender: TObject);
          procedure CBCliMailCandClick(Sender: TObject);
          procedure CBcandMailSMSClick(Sender: TObject);
          procedure BitBtn5Click(Sender: TObject);
          procedure ASG1GetCellColor(Sender: TObject; ARow, ACol: Integer;
               AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
          procedure ASG1CanEditCell(Sender: TObject; Arow, Acol: Integer;
               var canedit: Boolean);
          procedure ExpressButton6Click(Sender: TObject);
          procedure ExpressButton7Click(Sender: TObject);
          procedure QTipiMailCandAfterOpen(DataSet: TDataSet);
          procedure QTipiMailCandAfterScroll(DataSet: TDataSet);
          procedure BitBtn7Click(Sender: TObject);
          procedure WebBrowser1DocumentComplete(Sender: TObject;
               const pDisp: IDispatch; var URL: OleVariant);
          procedure BitBtn8Click(Sender: TObject);
          procedure WebBrowser2DocumentComplete(Sender: TObject;
               const pDisp: IDispatch; var URL: OleVariant);
          procedure SpeedButton4Click(Sender: TObject);
          procedure CheckBox1Click(Sender: TObject);
          procedure BitBtn1Click(Sender: TObject);

     private
          FileList: TStringList;
          BaseDir: string;
          procedure GetFileList;
          procedure DeleteAllFiles;
          function WinExecAndWait32(FileName: string; Visibility: integer = SW_SHOWNORMAL): cardinal;
          procedure CaricaInfoConnect;

     public
          xTotRighe: integer;
          Browser: TWebBrowser;
          xArrayIDAnag: array[1..5000] of integer;
          xS_Res_InvioSMS, xS_Res_Credito: string;
          xIDAnagSMS: integer;
          function Decodifica: string;
          function Decodifica2: string;
     end;

var
     FaxPresentazForm: TFaxPresentazForm;

implementation

uses ModuloDati, MDRicerche, Main, Splash3, EmailConnectInfo, TipiMailCand,
     uUtilsVarie, EmailMessage, SceltaModello, SceltaModelloAnag, SelContatti,
     uSendMailMAPI, uModelliWord_remote;

{$R *.DFM}

procedure TFaxPresentazForm.CBFaxClick(Sender: TObject);
begin
     PanContatto.Visible := CBFax.Checked;
end;

procedure TFaxPresentazForm.BitBtn3Click(Sender: TObject);
var xFileWord, xGiaInviati: string;
     MSWord: Variant;
     i, xTot: integer;
     Vero, xProcedi, xRiempi: boolean;
begin
     // messaggio PRIVACY
     MessageDlg('ATTENZIONE!! Prima di procedere con la presentazione di personale ' +
          'accertarsi che il/i candidato/i sia/siano consenziente/i. (Decreto Legislativo 30 Giugno 2003 N. 196)', mtInformation, [mbOK], 0);
     xTot := 0;
     for i := 1 to xTotRighe do begin
          ASG1.GetCheckBoxState(0, i, Vero);
          if Vero then inc(xTot);
     end;
     if xtot = 0 then begin
          MessageDlg('Nessun candidato selezionato !', mtError, [mbOK], 0);
          Abort;
     end;

     xProcedi := True;
     // controllo gi� inviati
     TStoricoIDX.Open;
     xGiaInviati := '';
     for i := 1 to xTotRighe do begin
          ASG1.GetCheckBoxState(0, i, Vero);
          if Vero then
               if TStoricoIDX.FindKeyADO(VarArrayOf([xArrayIDAnag[i], 20, DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger])) then
                    xGiaInviati := xGiaInviati + ASG1.Cells[0, i] + ' il ' + TStoricoIDX.FieldByName('DataEvento').asString + chr(13);
     end;
     TStoricoIDX.Close;
     if xGiaInviati <> '' then
          if MessageDlg('ATTENZIONE - Gi� effettuati i seguenti invii: ' + chr(13) +
               xGiaInviati + chr(13) +
               '-- VUOI CONTINUARE ? --', mtWarning, [mbYes, mbNo], 0) = mrNo then xProcedi := False;

     if xProcedi then begin
          try
               MsWord := CreateOleObject('Word.Basic');
          except
               ShowMessage('Non riesco ad aprire Microsoft Word.');
               Exit;
          end;

          xFileWord := GetDocPath + '\p' + DataRicerche.TRicerchePend.FieldByName('ID').asString + '.doc';
          xRiempi := True;
          if FileExists(xFileWord) then begin
               if MessageDlg('Il file esiste gi� - Aprirlo (Yes) o crearne uno nuovo, riscrivendo il vecchio (No) ?', mtWarning, [mbYes, mbNo], 0) = mrYes then begin
                    MsWord.AppShow;
                    MsWord.FileOpen(xFileWord); // file da aprire
                    xRiempi := False; // non riempire i campi
               end else begin
                    MsWord.AppShow;
                    MsWord.FileOpen(GetDocPath + '\ModPresCand.doc');
               end;
          end else begin
               MsWord.AppShow;
               MsWord.FileOpen(GetDocPath + '\ModPresCand.doc');
          end;

          if xRiempi then begin
               MsWord.EditFind('#data');
               MsWord.Insert(DateToStr(Date));
               MsWord.EditFind('#NomeAzienda');
               //               MsWord.Insert(QClientiDescrizione.Value);
               MsWord.Insert(QClienti.FieldByName('Descrizione').Value);
               MsWord.EditFind('#IndirizzoAzienda');
               //               MsWord.Insert(QClientiTipoStrada.Value+' '+QClientiIndirizzo.Value+' '+QClientiNumCivico.Value);
               MsWord.Insert(QClienti.FieldByName('TipoStrada').Value + ' ' + QClienti.FieldByName('Indirizzo').Value + ' ' + QClienti.FieldByName('NumCivico').Value);
               MsWord.EditFind('#CapLuogoProvAzienda');
               //               MsWord.Insert(QClientiCap.Value+' '+QClientiComune.Value+' ('+QClientiProvincia.Value+')');
               MsWord.Insert(QClienti.FieldByName('Cap').Value + ' ' + QClienti.FieldByName('Comune').Value + ' (' + QClienti.FieldByName('Provincia').Value + ')');
               if (CBFax.Checked) and (TContattiCli.RecordCount > 0) then begin
                    MsWord.EditFind('#AttenzioneDi');
                    //                    MsWord.Insert(TContattiCliContatto.Value);
                    MsWord.Insert(TContattiCli.FieldByName('Contatto').Value);
               end;
               MsWord.EditFind('#NumeroFax');
               {               if QClientiFax.Value<>'' then
                                   MsWord.Insert(QClientiFax.Value)}
               if QClienti.FieldByName('Fax').Value <> '' then
                    MsWord.Insert(QClienti.FieldByName('Fax').Value)
               else MsWord.Insert('---');
               MsWord.EditFind('#Ruolo');
               //               MsWord.Insert(DataRicerche.TRicerchePendMansione.Value);
               MsWord.Insert(DataRicerche.TRicerchePend.FieldByName('Mansione').Value);
               MsWord.EditFind('#CV');
               for i := 1 to xTotRighe do begin
                    ASG1.GetCheckBoxState(0, i, Vero);
                    if Vero then begin
                         QAnag.Close;
                         QAnag.SQl.Text := 'select * from Anagrafica where ID=:xID:';
                         //                         QAnag.ParamByName('xID').asInteger:=xArrayIDAnag[i];
                         QAnag.ParamByName['xID'] := xArrayIDAnag[i];
                         QAnag.Open;
                         // dati candidato
               {                         MsWord.Insert('C.V. n�'+QAnagCVNumero.asString+'  del '+QAnagCVinseritoInData.asString+chr(13));
                         MsWord.Insert('Et�: '+QAnagEta.value+chr(13));
                         MsWord.Insert('Comune di Residenza: '+QAnagComune.value+chr(13));}
                         MsWord.Insert('C.V. n�' + QAnag.FieldByName('CVNumero').asString + '  del ' + QAnag.FieldByName('CVinseritoInData').asString + chr(13));
                         MsWord.Insert('Et�: ' + QAnag.FieldByName('Eta').value + chr(13));
                         MsWord.Insert('Comune di Residenza: ' + QAnag.FieldByName('Comune').value + chr(13));
                         MsWord.Insert('Titolo di studio: ');
                         QTitoliStudio.Open;
                         while not QTitoliStudio.EOF do begin
                              //                              MsWord.Insert(QTitoliStudioTitoloStudio.value);
                              MsWord.Insert(QTitoliStudio.FieldByName('TitoloStudio').value);
                              QTitoliStudio.Next;
                              if not QTitoliStudio.EOF then MsWord.Insert(', ');
                         end;
                         QTitoliStudio.Close;
                         MsWord.Insert(chr(13));
                         MsWord.Insert('Lingue Conosciute: ');
                         TLingueConosc.Open;
                         while not TLingueConosc.EOF do begin
                              //                              MsWord.Insert(TLingueConoscLingua.value);
                              MsWord.Insert(TLingueConosc.FieldByName('Lingua').value);
                              TLingueConosc.Next;
                              if not TLingueConosc.EOF then MsWord.Insert(', ');
                         end;
                         TLingueConosc.Close;
                         MsWord.Insert(chr(13));
                         MsWord.Insert('----------------------------------------------------------------------------------------------');
                         MsWord.Insert(chr(13));

                         Data.DB.BeginTrans;
                         try
                              // aggiornamento dati candidato-ricerca
                              Data.Q1.Close;
                              {                              Data.Q1.SQL.text:='update EBC_CandidatiRicerche set Escluso=:xEscluso,DataImpegno=null,Stato=:xStato '+
                                                                 'where IDRicerca='+DataRicerche.TRicerchePendID.asString+' and IDAnagrafica='+QAnagID.asString;
                                                            Data.Q1.ParamByName['xEscluso').asBoolean:=False;
                                                            Data.Q1.ParamByName['xStato').asString:='invio FAX di presentazione il '+DateToStr(Date);}
                              Data.Q1.SQL.text := 'update EBC_CandidatiRicerche set Escluso=:xEscluso:,DataImpegno=null,Stato=:xStato: ' +
                                   'where IDRicerca=' + DataRicerche.TRicerchePend.FieldByName('ID').asString + ' and IDAnagrafica=' + QAnag.FieldByName('ID').asString;
                              Data.Q1.ParamByName['xEscluso'] := 0;
                              Data.Q1.ParamByName['xStato'] := 'invio FAX di presentazione il ' + DateToStr(Date);
                              Data.Q1.ExecSQL;

                              // registrazione nello storico
                  {                              Data.Q1.SQL.text:='insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) '+
                                   'values (:xIDAnagrafica,:xIDEvento,:xDataEvento,:xAnnotazioni,:xIDRicerca,:xIDUtente,:xIDCliente)';
                              Data.Q1.ParamByName['xIDAnagrafica').asInteger:=QAnagID.Value;
                              Data.Q1.ParamByName['xIDEvento').asInteger:=20;
                              Data.Q1.ParamByName['xDataEvento').asDateTime:=Date;
                              Data.Q1.ParamByName['xAnnotazioni').asString:='';
                              Data.Q1.ParamByName['xIDRicerca').asInteger:=DataRicerche.TRicerchePendID.Value;
                              Data.Q1.ParamByName['xIDUtente').asInteger:=MainForm.xIDUtenteAttuale;
                              Data.Q1.ParamByName['xIDCliente').asInteger:=DataRicerche.TRicerchePendIDCliente.Value;}
                              Data.Q1.SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
                                   'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xIDCliente:)';
                              Data.Q1.ParamByName['xIDAnagrafica'] := QAnag.FieldByName('ID').AsInteger;
                              Data.Q1.ParamByName['xIDEvento'] := 20;
                              Data.Q1.ParamByName['xDataEvento'] := Date;
                              Data.Q1.ParamByName['xAnnotazioni'] := '';
                              Data.Q1.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
                              Data.Q1.ParamByName['xIDUtente'] := MainForm.xIDUtenteAttuale;
                              Data.Q1.ParamByName['xIDCliente'] := DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger;
                              Data.Q1.ExecSQL;

                              Data.DB.CommitTrans;
                         except
                              Data.DB.RollbackTrans;
                              MessageDlg('ERRORE SUL DATABASE:  operazione non completata', mtError, [mbOK], 0);
                         end;
                    end;
               end;
               MsWord.EditFind('#FirmaConsulente');
               //               MsWord.Insert(DataRicerche.TRicerchePendUtente.asString);
               MsWord.Insert(DataRicerche.TRicerchePend.FieldByName('Utente').asString);
               MsWord.EditFind('#quanti');
               MsWord.Insert(IntToStr(xTot));
               MsWord.FileSaveAs(xFileWord);
          end;
          //[/TONI20020729\]FINE
     end;
end;

procedure TFaxPresentazForm.BitBtn4Click(Sender: TObject);
var MyStringList: TStringList;
     xPath, xGiaInviati, xCognome, xNome: string;
     Vero, xProcedi: boolean;
     xFileName: PChar;
     i: integer;
     xLettera, xCar: char;
     xstring: string;
begin
     // messaggio PRIVACY
     MessageDlg('ATTENZIONE!! Prima di procedere con la presentazione di personale ' +
          'accertarsi che il/i candidato/i sia/siano consenziente/i. (Decreto Legislativo 30 Giugno 2003 N. 196)', mtInformation, [mbOK], 0);
     //[/TONI20020729\]
          // DISABILITAZIONE MISTRAL Milano -- ALIAS='MISTRAL'
     //     if Uppercase(copy(Data.GlobalNomeAzienda.Value,1,7))='MISTRAL' then begin
     if (Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 3)) = 'RAY') or (Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 7)) = 'MISTRAL') then begin
          ShowMessage('FIDELITY:  FUNZIONE NON ABILITATA');
          Exit;
     end;
     MyStringList := TStringList.Create;
     Session.GetAliasParams('EBCtemp', MyStringList);
     xPath := copy(MyStringList[0], 6, Length(MyStringList[0]));
     if xPath[length(xPath)] <> '\' then xPath := xPath + '\'; // mette la barra finale se non c'e'
     BaseDir := xPath; // BaseDir corrisponde alla cartella dell'alias "EBCtemp"
     if not FileExists(BaseDir + 'pkzip.exe') then begin
          MessageDlg('File PKZIP.EXE non trovato - impossibile proseguire', mtError, [mbOK], 0);
          exit;
     end;

     if (not FileExists(xPath + 'Anagrafica.db')) or
          (not FileExists(xPath + 'TitoliStudio.db')) or
          (not FileExists(xPath + 'AreeDiplomi.db')) or
          (not FileExists(xPath + 'AnagMansioni.db')) then begin
          MessageDlg('Mancano file necessari delle tabelle in EBCtemp - impossibile proseguire', mtError, [mbOK], 0);
          exit;
     end;
     xProcedi := True;
     // controllo gi� inviati
     TStoricoIDX.Open;
     xGiaInviati := '';
     for i := 1 to xTotRighe do begin
          ASG1.GetCheckBoxState(0, i, Vero);
          if Vero then
               //               if TStoricoIDX.FindKey([xArrayIDAnag[i],21,DataRicerche.TRicerchePendIDCliente.Value]) then
               if TStoricoIDX.FindKeyADO(VarArrayOf([xArrayIDAnag[i], 21, DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger])) then
                    //                    xGiaInviati:=xGiaInviati+ASG1.Cells[0,i]+' il '+TStoricoIDXDataEvento.asString+chr(13);
                    xGiaInviati := xGiaInviati + ASG1.Cells[0, i] + ' il ' + TStoricoIDX.FieldByName('DataEvento').asString + chr(13);
     end;
     TStoricoIDX.Close;
     if xGiaInviati <> '' then
          if MessageDlg('ATTENZIONE - Gi� effettuati i seguenti invii: ' + chr(13) +
               xGiaInviati + chr(13) +
               '-- VUOI CONTINUARE ? --', mtWarning, [mbYes, mbNo], 0) = mrNo then xProcedi := False;

     if xProcedi then begin
          Splash3Form := TSplash3Form.Create(nil);
          Splash3Form.Show;
          Splash3Form.Update;

          // svuotamento tabelle
          TAnag.Close;
          //          TAnag.Exclusive:=True;
          TAnag.EmptyTable;
          //          TAnag.Exclusive:=False;
          TTitStudio.Close;
          //          TTitStudio.Exclusive:=True;
          TTitStudio.EmptyTable;
          //          TTitStudio.Exclusive:=False;
          TAnagMansioni.Close;
          //          TAnagMansioni.Exclusive:=True;
          TAnagMansioni.EmptyTable;
          //          TAnagMansioni.Exclusive:=False;
          TEspLav.Close;
          //          TEspLav.Exclusive:=True;
          TEspLav.EmptyTable;
          //          TEspLav.Exclusive:=False;
          TLingue.Close;
          //          TLingue.Exclusive:=True;
          TLingue.EmptyTable;
          //          TLingue.Exclusive:=False;
                    //TAnagComp.Close;
                    //TAnagComp.Exclusive:=True;
                    //TAnagComp.EmptyTable;
                    //TAnagComp.Exclusive:=False;

          TAnag.Open;
          TTitStudio.Open;
          TAnagMansioni.Open;
          TEspLav.Open;
          TLingue.Open;
          //TAnagComp.Open;

          // cancellazione vecchi CV --> tutti i file GIF
          FileList := TStringList.Create;
          GetFileList;
          DeleteAllFiles;
          FileList.Free;
          MyStringList.Free;

          // Riempimento tabelle
          for i := 1 to xTotRighe do begin
               ASG1.GetCheckBoxState(0, i, Vero);
               if Vero then begin
                    QAnag.Close;
                    //                    QAnag.ParamByName['xID').asInteger:=xArrayIDAnag[i];
                    QAnag.SQL.text := 'select * from Anagrafica where ID=:xID:';
                    QAnag.ParamByName['xID'] := xArrayIDAnag[i];
                    QAnag.Open;
                    QAnagAltreInfo.Close;
                    //                    QAnagAltreInfo.ParamByName['xID').asInteger:=xArrayIDAnag[i];
                    QAnagAltreInfo.SQL.Text := 'select * from AnagAltreInfo where IDAnagrafica=:xID:';
                    QAnagAltreInfo.ParamByName['xID'] := xArrayIDAnag[i];
                    QAnagAltreInfo.Open;
                    if CBNominativo.Checked then begin
                         xCognome := ''; xNome := '';
                    end else begin
                         {                         xCognome:=QAnagCognome.Value;
                                                  xNome:=QAnagNome.Value;}
                         xCognome := QAnag.FieldByName('Cognome').Value;
                         xNome := QAnag.FieldByName('Nome').Value;
                    end;
                    {                    TAnag.InsertRecord([xCognome,xNome,
                                             QAnagTitolo.Value,
                                                  QAnagDataNascita.Value,
                                                  QAnagSesso.Value,
                                                  QAnagDomicilioCap.Value,
                                                  QAnagDomicilioComune.Value,
                                                  QAnagIDComuneDom.Value,
                                                  QAnagIDZonaDom.Value,
                                                  QAnagCVNumero.Value,
                                                  null,
                                                  QAnagCVinseritoInData.Value,
                                                  null,
                                                  null,
                                                  QAnagAltreInfoMobilita.Value,
                                                  QAnagAltreInfoCassaIntegrazione.Value,
                                                  QAnagAltreInfoDispPartTime.Value,
                                                  QAnagAltreInfoDispInterinale.Value,
                                                  QAnagAltreInfoCateogorieProtette.Value,
                                                  QAnagAltreInfoInvalidita.Value,
                                                  QAnagAltreInfoPercentualeInvalidita.Value]);}
                    TAnag.InsertRecord([xCognome, xNome,
                         QAnag.FieldByName('Titolo').Value,
                              QAnag.FieldByName('DataNascita').Value,
                              QAnag.FieldByName('Sesso').Value,
                              QAnag.FieldByName('DomicilioCap').Value,
                              QAnag.FieldByName('DomicilioComune').Value,
                              QAnag.FieldByName('IDComuneDom').Value,
                              QAnag.FieldByName('IDZonaDom').Value,
                              QAnag.FieldByName('CVNumero').Value,
                              null,
                              QAnag.FieldByName('CVinseritoInData').Value,
                              null,
                              null,
                              QAnagAltreInfo.FieldByName('Mobilita').Value,
                              QAnagAltreInfo.FieldByName('CassaIntegrazione').Value,
                              QAnagAltreInfo.FieldByName('DispPartTime').Value,
                              QAnagAltreInfo.FieldByName('DispInterinale').Value,
                              QAnagAltreInfo.FieldByName('CateogorieProtette').Value,
                              QAnagAltreInfo.FieldByName('Invalidita').Value,
                              QAnagAltreInfo.FieldByName('PercentualeInvalidita').Value]);

                    // titoli di studio
                    QTitoliStudio.Open;
                    while not QTitoliStudio.EOF do begin
                         //                         TTitStudio.InsertRecord([TAnagID.value,QTitoliStudioIDDiploma.value]);
                         TTitStudio.InsertRecord([TAnag.FieldByName('ID').value, QTitoliStudio.FieldByName('IDDiploma').value]);
                         QTitoliStudio.Next;
                    end;
                    QTitoliStudio.Close;
                    // ruoli posseduti
                    SrcAnagMansioni.Open;
                    while not SrcAnagMansioni.EOF do begin
                         //                         TAnagMansioni.InsertRecord([TAnagID.value,SrcAnagMansioniIDMansione.Value]);
                         TAnagMansioni.InsertRecord([TAnag.FieldByName('ID').value, SrcAnagMansioni.FieldByName('IDMansione').Value]);
                         SrcAnagMansioni.Next;
                    end;
                    SrcAnagMansioni.Close;
                    // esperienze lavorative
                    SrcAnagEspLav.Open;
                    while not SrcAnagEspLav.EOF do begin
                         //                         TEspLav.InsertRecord([TAnagID.value,SrcAnagEspLavIDSettore.Value]);
                         TEspLav.InsertRecord([TAnag.FieldByName('ID').value, SrcAnagEspLav.FieldByName('IDSettore').Value]);
                         SrcAnagEspLav.Next;
                    end;
                    SrcAnagEspLav.Close;
                    // lingue conosciute
                    TLingueConosc.Open;
                    while not TLingueConosc.EOF do begin
                         //                         TLingue.InsertRecord([TAnagID.value,TLingueConoscLingua.value,TLingueConoscLivello.value]);
                         TLingue.InsertRecord([TAnag.FieldByName('ID').value, TLingueConosc.FieldByName('Lingua').value, TLingueConosc.FieldByName('Livello').value]);
                         TLingueConosc.Next;
                    end;
                    TLingueConosc.close;
                    // competenze possedute --> PER ORA NO (TROPPO GRANDE LA TABELLA DI BASE)
                    {SrcAnagComp.Open;
                    while not SrcAnagComp.EOF do begin
                          TAnagComp.InsertRecord([TAnagID.value,SrcAnagCompIDCompetenza.value,SrcAnagCompValore.Value]);
                       SrcAnagComp.Next;
                    end;
                    SrcAnagComp.Close;}

                    // copia file GIF
                    if RGOpzioneCV.ItemIndex = 0 then xCar := char('e') else xCar := char('i');
                    {                    if FileExists(GetCVPath+'\'+xCar+QAnagCVNumero.AsString+'a.gif') then begin
                                             CopyFile(Pointer(GetCVPath+'\'+xCar+QAnagCVNumero.AsString+'a.gif'),Pointer(xPath+'\'+xCar+QAnagCVNumero.AsString+'a.gif'),False);
                                             xLettera:=char('b');}
                    if FileExists(GetCVPath + '\' + xCar + QAnag.FieldByName('CVNumero').AsString + 'a.gif') then begin
                         CopyFile(Pointer(GetCVPath + '\' + xCar + QAnag.FieldByName('CVNumero').AsString + 'a.gif'), Pointer(xPath + '\' + xCar + QAnag.FieldByName('CVNumero').AsString + 'a.gif'), False);
                         xLettera := char('b');
                         {                         while FileExists(GetCVPath+'\'+xCar+QAnagCVNumero.AsString+xlettera+'.gif') do begin
                                                       CopyFile(Pointer(GetCVPath+'\'+xCar+QAnagCVNumero.AsString+'a.gif'),Pointer(xPath+'\'+xCar+QAnagCVNumero.AsString+xLettera+'.gif'),False);}
                         while FileExists(GetCVPath + '\' + xCar + QAnag.FieldByName('CVNumero').AsString + xlettera + '.gif') do begin
                              CopyFile(Pointer(GetCVPath + '\' + xCar + QAnag.FieldByName('CVNumero').AsString + 'a.gif'), Pointer(xPath + '\' + xCar + QAnag.FieldByName('CVNumero').AsString + xLettera + '.gif'), False);
                              xLettera := char(Ord(xLettera) + 1);
                         end;
                    end;

                    Data.DB.BeginTrans;
                    try
                         // aggiornamento storico
                         if Q.Active then Q.Close;
                         {                         Q.SQL.text:='insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) '+
                                                       'values (:xIDAnagrafica,:xIDEvento,:xDataEvento,:xAnnotazioni,:xIDRicerca,:xIDUtente,:xIDCliente)';
                                                  Q.ParamByName['xIDAnagrafica').asInteger:=QAnagID.Value;
                                                  Q.ParamByName['xIDEvento').asInteger:=21;
                                                  Q.ParamByName['xDataEvento').asDateTime:=date;
                                                  Q.ParamByName['xAnnotazioni').asString:='';
                                                  Q.ParamByName['xIDRicerca').asInteger:=DataRicerche.TRicerchePendID.Value;
                                                  Q.ParamByName['xIDUtente').asInteger:=DataRicerche.TRicerchePendIDUtente.Value;
                                                  Q.ParamByName['xIDCliente').asInteger:=DataRicerche.TRicerchePendIDCliente.Value;}
                         Q.SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
                              'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xIDCliente:)';
                         Q.ParamByName['xIDAnagrafica'] := QAnag.FieldByName('ID').AsInteger;
                         Q.ParamByName['xIDEvento'] := 21;
                         Q.ParamByName['xDataEvento'] := date;
                         Q.ParamByName['xAnnotazioni'] := '';
                         Q.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldBynAme('ID').AsInteger;
                         Q.ParamByName['xIDUtente'] := MainForm.xIDUtenteAttuale;
                         Q.ParamByName['xIDCliente'] := DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger;

                         Q.ExecSQL;
                         // aggiornamento stato anagrafica
                         if Q.Active then Q.Close;
                         Q.SQL.text := 'update EBC_CandidatiRicerche set Escluso=0,DataImpegno=null,Stato=''invio CV via e-mail il ' + DateToStr(Date) + '''' +
                              //                              ' where IDRicerca='+DataRicerche.TRicerchePendID.asString+' and IDAnagrafica='+QAnagID.asString;
                         ' where IDRicerca=' + DataRicerche.TRicerchePend.FieldByName('ID').asString + ' and IDAnagrafica=' + QAnag.FieldByName('ID').asString;
                         //[/TONI20020729\]FINE
                         Q.ExecSQL;
                         Data.DB.CommitTrans;
                    except
                         Data.DB.RollbackTrans;
                         MessageDlg('ATTENZIONE: errore sul database' + chr(13) +
                              'aggiornamento scheda e storico non avvenuto per' + chr(13) +
                              xCognome + ' ' + xNome, mtError, [mbOK], 0);
                    end;
               end;
          end;

          TAnag.Close;
          TTitStudio.Close;
          TAnagMansioni.Close;
          TEspLav.Close;
          TLingue.Close;
          //TAnagComp.Close;

          // Tabelle di base --> BATCHMOVE
          Src.Close; Src.TableName := 'dbo.AreeDiplomi';
          Dest.Close; Dest.TableName := 'AreeDiplomi.db';
          Src.Open; Dest.Open;
          BatchMove1.Execute;
          Src.Close; Src.TableName := 'dbo.Diplomi';
          Dest.Close; Dest.TableName := 'Diplomi.db';
          Src.Open; Dest.Open;
          BatchMove1.Execute;
          {Src.Close; Src.TableName:='dbo.CompetenzeMansioni';      // MOLTO GRANDE --> da evitare !!
          Dest.Close; Dest.TableName:='CompetenzeMansioni.db';
          Src.Open; Dest.Open;
          BatchMove1.Execute;}
          Src.Close; Src.TableName := 'dbo.Mansioni';
          Dest.Close; Dest.TableName := 'Mansioni.db';
          Src.Open; Dest.Open;
          BatchMove1.Execute;
          Src.Close; Src.TableName := 'dbo.EBC_attivita';
          Dest.Close; Dest.TableName := 'EBC_attivita.db';
          Src.Open; Dest.Open;
          BatchMove1.Execute;
          Src.Close; Src.TableName := 'dbo.Lingue';
          Dest.Close; Dest.TableName := 'Lingue.db';
          Src.Open; Dest.Open;
          BatchMove1.Execute;
          {Src.Close; Src.TableName:='dbo.Competenze';
          Dest.Close; Dest.TableName:='Competenze.db';
          Src.Open; Dest.Open;
          BatchMove1.Execute;}
          {Src.Close; Src.TableName:='dbo.AreeCompetenze';
          Dest.Close; Dest.TableName:='AreeCompetenze.db';
          Src.Open; Dest.Open;
          BatchMove1.Execute;}
          Src.Close; Src.TableName := 'dbo.Aree';
          Dest.Close; Dest.TableName := 'Aree.db';
          Src.Open; Dest.Open;
          BatchMove1.Execute;
          Dest.Close;

          // cancellazione file ZIP
          if FileExists(BaseDir + 'EBCdb.zip ') then DeleteFile(BaseDir + 'EBCdb.zip ');
          if FileExists(BaseDir + 'EBCgif.zip ') then DeleteFile(BaseDir + 'EBCgif.zip ');
          // compattazione
          if FileExists(BaseDir + 'pkzip.exe') then begin
               if RGOpzioneExp.ItemIndex = 0 then begin
                    //xstring:=BaseDir+'pkzip '+BaseDir+'EBCdb.zip '+BaseDir+'*.db '+BaseDir+'*.val '+BaseDir+'*.xg? '+BaseDir+'*.yg? '+BaseDir+'*.px '+BaseDir+'*.mb';
                    xstring := BaseDir + 'pkzip ' + BaseDir + 'EBCdb.zip ' + BaseDir + '*.db ' + BaseDir + '*.xg? ' + BaseDir + '*.yg?';
                    WinExecAndWait32(xString);
                    xstring := BaseDir + 'pkzip ' + BaseDir + 'EBCdb.zip ' + BaseDir + '*.px ' + BaseDir + '*.mb ' + BaseDir + '*.val';
                    WinExecAndWait32(xString);
               end;
               WinExecAndWait32(BaseDir + 'pkzip ' + BaseDir + 'EBCgif.zip ' + BaseDir + '*.gif');
               ShowMessage('Esportazione terminata: i file EBCdb.zip e EBCgif.zip sono nella directory ' + BaseDir);
          end else
               MessageDlg('ATTENZIONE: non � stato trovato il file PKZIP.EXE in ' + BaseDir + chr(13) +
                    'Compattazione non effettuata', mtError, [mbOK], 0);
          Splash3Form.Hide;
          Splash3Form.Free;
     end;
end;

procedure TFaxPresentazForm.GetFileList;
var TSR: TSearchRec; Found: integer;
begin
     Found := FindFirst(BaseDir + '*.gif', $21, TSR); // $21=readonly+archive
     while Found = 0 do begin
          if TSR.Name[1] <> '.' then FileList.Add(BaseDir + TSR.Name);
          Found := FindNext(TSR);
     end;
     FindClose(TSR);
end;

procedure TFaxPresentazForm.DeleteAllFiles;
var i: integer; s: string;
begin
     for i := 0 to FileList.Count - 1 do begin
          s := FileList[i];
          DeleteFile(s);
     end;
end;

function TFaxPresentazForm.WinExecAndWait32(FileName: string; Visibility: integer = SW_SHOWNORMAL): cardinal;
{ returns 0 if the Exec failed, otherwise returns the process' exit
  code when the process terminates }
var
     zAppName: array[0..512] of char;
     lpCommandLine: array[0..512] of char;
     zCurDir: array[0..255] of char;
     WorkDir: string;
     StartupInfo: TStartupInfo;
     ProcessInfo: TProcessInformation;
begin
     StrPCopy(zAppName, '');
     StrPCopy(lpCommandLine, FileName);
     GetDir(0, WorkDir);
     StrPCopy(zCurDir, WorkDir);
     FillChar(StartupInfo, Sizeof(StartupInfo), #0);
     StartupInfo.cb := Sizeof(StartupInfo);

     StartupInfo.dwFlags := STARTF_USESHOWWINDOW;
     StartupInfo.wShowWindow := Visibility;
     if not CreateProcess(
          nil, { pointer to command line string }
          lpCommandLine,
          nil, { pointer to process security attributes }
          nil, { pointer to thread security attributes}
          false, { handle inheritance flag }
          CREATE_NEW_CONSOLE or { creation flags }
          NORMAL_PRIORITY_CLASS,
          nil, { pointer to new environment block}
          nil, { pointer to current directory name }
          StartupInfo, { pointer to STARTUPINFO }
          ProcessInfo) then Result := 0 { pointer to PROCESS_INF }
     else begin
          WaitforSingleObject(ProcessInfo.hProcess, INFINITE);
          GetExitCodeProcess(ProcessInfo.hProcess, Result);
     end;
end;

procedure TFaxPresentazForm.QAnag_OLDCalcFields(DataSet: TDataSet);
var xAnno, xMese, xgiorno: Word;
begin
     //[/TONI20020729\]
     {     if QAnagDataNascita.asString='' then
               QAnagEta.asString:=''
          else begin
               DecodeDate((Date-QAnagDataNascita.Value),xAnno,xMese,xgiorno);
               // TAnagraficaEta.Value:=StrToInt(copy(xAnno,3,2));
               QAnagEta.Value:=copy(IntToStr(xAnno),3,2);
          end;}
     if QAnag.FieldByName('DataNascita').asString = '' then
          QAnag.FieldByName('Eta').asString := ''
     else begin
          DecodeDate((Date - QAnag.FieldByName('DataNascita').Value), xAnno, xMese, xgiorno);
          // TAnagraficaEta.Value:=StrToInt(copy(xAnno,3,2));
          QAnag.FieldByName('Eta').Value := copy(IntToStr(xAnno), 3, 2);
     end;
     //[/TONI20020729\]FINE
end;

procedure TFaxPresentazForm.FormShow(Sender: TObject);
var i: integer;
begin
     //grafica
     panel1.color := mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;
     panel2.color := mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;
     panel4.color := mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;
     panel5.color := mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;
     panel7.color := mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;
     panel11.color := mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;
     panel16.color := mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;
     panel17.color := mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;
     PanContatto.color := mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;
     panel13.color := mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;
     panel18.color := mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;
     panel8.color := mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;
     panel19.color := mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;
     PanClienti.color := mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;
     PanCandidati.color := mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;
     FaxPresentazForm.color := mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;

     WebBrowser1.Visible := False;
     WebBrowser2.Visible := False;

     //SetWindowPos(Self.Handle,
     //     HWND_TOPMOST,
     //     0, 0, 0, 0,
     //     SWP_NOMOVE or
     //     SWP_NOACTIVATE or
     //     SWP_NOSIZE);

     Caption := '[M/213] - ' + Caption;
     PCSel.Visible := False;
     for i := 0 to PCSel.PageCount - 1 do
          PCSel.Pages[i].TabVisible := False;

     if not Data.Global.active then Data.Global.Open;
     EMittente.text := copy(Data.Global.FieldByName('NomeAzienda').asString, 1, 16);
     EMittente.text := StringReplace(EMittente.text, ' ', '_', [rfReplaceAll]);
     if pos('EMERGENCY', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) > 0 then
          CBCliMailNomeTipoFile.Checked := True;
end;

procedure TFaxPresentazForm.ExpressButton1Click(Sender: TObject);
begin
     PCSel.ActivePageIndex := 0;
     PCSel.Visible := true;
     ExpressButton1.Down := True;
     ExpressButton2.Down := False;
     ExpressButton3.Down := False;
     ExpressButton4.Down := False;
     ExpressButton5.Down := False;
     QTipiMailCand.Open;
     CaricaInfoConnect;
end;

procedure TFaxPresentazForm.ExpressButton2Click(Sender: TObject);
begin
     PCSel.ActivePageIndex := 1;
     PCSel.Visible := true;
     ExpressButton1.Down := False;
     ExpressButton2.Down := True;
     ExpressButton3.Down := False;
     ExpressButton4.Down := False;
     ExpressButton5.Down := False;
     QTipiMailCand.Close;
end;

procedure TFaxPresentazForm.ExpressButton3Click(Sender: TObject);
begin
     PCSel.ActivePageIndex := 2;
     PCSel.Visible := true;
     ExpressButton1.Down := False;
     ExpressButton2.Down := False;
     ExpressButton3.Down := True;
     ExpressButton4.Down := False;
     ExpressButton5.Down := False;
     QTipiMailCand.Close;
end;

procedure TFaxPresentazForm.ExpressButton4Click(Sender: TObject);
begin
     PCSel.ActivePageIndex := 3;
     PCSel.Visible := true;
     ExpressButton1.Down := False;
     ExpressButton2.Down := False;
     ExpressButton3.Down := False;
     ExpressButton4.Down := True;
     ExpressButton5.Down := False;
     QTipiMailCand.Close;
end;

procedure TFaxPresentazForm.BitBtn2Click(Sender: TObject);
var i, xTot, xCRpos, Shellerror, j: integer;
     xMess, xSl: TstringList;
     Vero: boolean;
     xNonInviati, xNonInviatiSMS, xFirma, xS, xURL, xCCNStringa, pathname: string;
     xCcn: boolean;
begin
     //Browser := TWebBrowser.Create(self);
     xCcnStringa := '';
     xCcn := False;
     xTot := 0;
     if Panel9.Visible then begin
          for i := 1 to xTotRighe do begin
               // showmessage(asg1.Cells[0, i]);
               ASG1.GetCheckBoxState(0, i, Vero);
               if Vero then
                    inc(xTot);
          end;
          if (xtot = 0) then begin
               MessageDlg('Nessun candidato selezionato !', mtError, [mbOK], 0);
               Exit;
          end; // else showmessage(inttostr(xtot));
     end;
     if CBcandMailSMS.Checked then // invio SMS avviso
          showMessage('Chiudere SMSDispatcher - se � aperto - e premere OK');

     // COMPOSIZIONE MESSAGGIO
     xMess := TStringList.create;
     // intestazione (da tabella)
     xMess.Add(QTipiMailCand.FieldByName('Intestazione').Value);
     xMess.Add('');
     if PanClienti.Visible = true then begin
          if DataRicerche.TRicerchePend.Active = false then DataRicerche.TRicerchePend.Open;
          if DataRicerche.TRicerchePend.FieldByName('IDUtente').asString <> '' then begin
               Q.Close;
               Q.SQL.Text := 'select Descrizione,FirmaMail from Users where ID=' + DataRicerche.TRicerchePend.FieldByName('IDUtente').asString;
               Q.Open;
          end else begin
               Q.Close;
               Q.SQL.Text := 'select Descrizione,FirmaMail from Users where ID=' + IntToStr(MainForm.xIDUtenteAttuale);
               Q.Open;
          end;
          // dati ricerca (FISSI)
          if DataRicerche.TRicerchePend.FieldByName('IDUtente').asString <> '' then begin
               if QTipiMailCand.FieldByName('InfoRicerca').AsBoolean then begin
                    xMess.Add('Codice Ricerca:  ' + DataRicerche.TRicerchePend.FieldByName('Progressivo').Value);
                    xMess.Add('Posizione ricercata:  ' + DataRicerche.TRicerchePend.FieldByName('Mansione').asstring + ' (area: ' + DataRicerche.TRicerchePend.FieldByName('Area').asstring + ')');
                    xMess.Add('Selezionatore: ' + Q.FieldByName('Descrizione').asString);
                    xMess.Add('');
               end;
               // specifiche ricerca (ESTERNE)
               if QTipiMailCand.FieldByName('AddSpecifiche').Value then begin
                    xMess.Add('Specifiche ricerca:');
                    xS := DataRicerche.TRicerchePend.FieldByName('SpecifEst').Value;
                    while pos(chr(13), xS) > 0 do begin
                         xCRpos := pos(chr(13), xS);
                         xMess.Add(copy(xS, 1, xCRpos - 1));
                         xS := copy(xS, xCRpos + 1, length(xS));
                    end;
                    xMess.Add(xS);
                    xMess.Add('');
               end;
          end;
     end else begin
          Q.Close;
          Q.SQL.Text := 'select Descrizione,FirmaMail from Users where ID=' + IntToStr(MainForm.xIDUtenteAttuale);
          Q.Open;
     end;
     // corpo (da tabella)
     xMess.Add(QTipiMailCand.FieldByName('Corpo').Value);
     // saluti e firma (da tabella Utenti) con ritorno a capo
     xMess.Add('');

     if QTipiMailCand.FieldByName('AddQuest').Value then begin
          // aggiunta questionario
          Q1.Close;
          Q1.SQL.text := 'select URL_ASP from QUEST_Params';
          Q1.Open;
          xURL := Q1.FieldByName('URL_ASP').asString;
          xURL := StringReplace(xURL, '#', QQuest.FieldByName('ID').asstring, []) + '��';
          xURL := StringReplace(xURL, '&', '%26', [rfReplaceAll]);
          Q1.Close;
          xMess.Add('La preghiamo di compilare il questionario che trova al seguente indirizzo:' + xURL);
     end;

     xS := Q.FieldByName('FirmaMail').asString;
     while pos(chr(13), xS) > 0 do begin
          xCRpos := pos(chr(13), xS);
          xMess.Add(copy(xS, 1, xCRpos - 1));
          xS := copy(xS, xCRpos + 1, length(xS));
     end;
     xMess.Add(xS);

     // edit messaggio
     EmailMessageForm := TEmailMessageForm.create(self);
     EmailMessageForm.Memo1.Lines := xMess;
     EmailMessageForm.ShowModal;
     if EmailMessageForm.ModalResult = mrCancel then begin
          EmailMessageForm.Free;
          exit;
     end;
     xmess.Clear;

     //for i := 0 to EmailMessageForm.Memo1.Lines.count - 1 do
     //     xMess.Add(EmailMessageForm.Memo1.Lines[i]); // + '%0D%0A');
     xMess.text := EmailMessageForm.Memo1.text;

     if Panel9.Visible then begin
          //ShowMessage('Predisporre connessione Internet e premere OK quando pronti');
          EmailMessageForm.Free;
          // connessione
          StatusBar1.Visible := True;
          // invio vero e proprio dei messaggi
          xNonInviati := '';
          xNonInviatiSMS := '';
          if xtot > 1 then begin
               if MessageDlg('Attenzione!:Stai per inviare il messaggio a pi� destinatari: Vuoi inserire tutti gli indirizzi in Copia Conoscenza nascosta (CCN)?', mtWarning, [mbYes, mbNo], 0) = mrYes then
                    xCcn := True
               else
                    xCcn := False;
          end;
          for i := 1 to xTotRighe do begin
               // showmessage(asg1.Cells[i,0] );

               ASG1.GetCheckBoxState(0, i, Vero);
               {se nell'indirizzo email privato dell'anagrafica non c'� niente h1 non esegue niente e da fuori sembra un problema del software
               si potrebbe fare un controllo}
               if data.global.fieldbyname('debughrms').asboolean = true then begin
                    if vero = true then showmessage('vero=true');
                    if vero = false then showmessage('vero=false')
               end;
               if Vero then begin
                    // showmessage(IntToStr(xArrayIDAnag[i]));

                    QAnag2.Close;
                    QAnag2.SQL.Text := 'select Email,Cognome,Nome from Anagrafica where ID=' + IntToStr(xArrayIDAnag[i]);
                    QAnag2.Open;
                    if QAnag2.FieldByName('Email').asString = '' then begin
                         xNonInviati := xNonInviati + QAnag2.FieldByName('Cognome').asString + ' ' + QAnag2.FieldByName('Nome').asString + chr(13);
                    end else begin
                         // questionari
                         if pos('��', xMess.Text) > 0 then
                              xMess.Text := StringReplace(xMess.Text, '��', IntToStr(xArrayIDAnag[i]), []);
                         try
                              if xCcn then
                                   xCCNStringa := xCCNStringa + QAnag2.FieldByName('Email').asString + '; '
                              else begin
                                   // OLD --> Shellerror := ShellExecute(0, 'Open', pchar('mailto:' + QAnag2.FieldByName('Email').asString + '?subject=' + StringReplace(Trim(QTipiMailCand.FieldByName('Oggetto').AsString), '"', '''''', [rfReplaceAll]) + '&body=' + stringreplace(xMess.Text, '&', '%26', [rfReplaceAll])), '', '', SW_SHOW);
                                   try
                                        SendEMailMAPI(QTipiMailCand.FieldByName('Oggetto').AsString, xMess.Text, QAnag2.FieldByName('Email').asString, '', '', MainForm.xSendMail_Remote, MainForm.xDebug, True, False);
                                        //Shellerror := ShellExecute(0, 'Open', pchar('mailto:' + QAnag2.FieldByName('Email').asString + '?subject=' + StringReplace(Trim(QTipiMailCand.FieldByName('Oggetto').AsString), '"', '''''', [rfReplaceAll]) + '&body=' + stringreplace(xMess.Text, '&', '%26', [rfReplaceAll])), '', '', SW_SHOW);

                                   finally begin
                                             //  showmessaGE('Operazione Completata');
                                             //       SetWindowPos(Self.Handle,
                                             //            HWND_TOPMOST,
                                             //            0, 0, 0, 0,
                                             //            SWP_NOMOVE or
                                             //            SWP_NOACTIVATE or
                                             //            SWP_NOSIZE);
                                             //  ModalResult := mrOK;
                                        end;
                                   end;
                              end;

                              if data.global.fieldbyname('debughrms').asboolean = true then showmessage('ritorno della ShellExecute: ' + inttostr(Shellerror));


                              if CBcandMailSMS.Checked then begin
                                   // invio SMS di avviso
                                   if not AccodaSMSaDispatcher(xArrayIDAnag[i], DataRicerche.TRicerchePend.FieldByName('ID').value,
                                        DataRicerche.TRicerchePend.FieldByName('IDUtente').AsInteger,
                                        DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger,
                                        EavvisoSMS.Text,
                                        CBDispatcher1.text,
                                        True) then begin
                                        xNonInviatiSMS := xNonInviatiSMS + QAnag2.FieldByName('Cognome').asString + ' ' + QAnag2.FieldByName('Nome').asString + chr(13);
                                   end;
                              end;
                              // registrazione nello storico del candidato

                              with Data do begin
                                   DB.BeginTrans;

                                   try
                                        if PanClienti.Visible then begin
                                             Q1 := CreateQueryFromString('insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
                                                  'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xIDCliente:)');
                                             Q1.ParamByName['xIDAnagrafica'] := xArrayIDAnag[i];
                                             Q1.ParamByName['xIDEvento'] := 71; // 71=inviata via e-mail
                                             Q1.ParamByName['xDataEvento'] := Date;
                                             Q1.ParamByName['xAnnotazioni'] := 'invio e-mail di ' + QTipiMailCand.FieldByName('Descrizione').AsString;
                                             Q1.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').Asinteger;
                                             Q1.ParamByName['xIDUtente'] := MainForm.xIDUtenteAttuale;
                                             Q1.ParamByName['xIDCliente'] := DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger;
                                        end else begin
                                             Q1 := CreateQueryFromString('insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDUtente,IDCliente) ' +
                                                  'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDUtente:,:xIDCliente:)');
                                             Q1.ParamByName['xIDAnagrafica'] := xArrayIDAnag[i];
                                             Q1.ParamByName['xIDEvento'] := 71; // 71=inviata via e-mail
                                             Q1.ParamByName['xDataEvento'] := Date;
                                             Q1.ParamByName['xAnnotazioni'] := 'invio e-mail di ' + QTipiMailCand.FieldByName('Descrizione').AsString;
                                             Q1.ParamByName['xIDUtente'] := MainForm.xIDUtenteAttuale;
                                             Q1.ParamByName['xIDCliente'] := NULL;
                                        end;
                                        Q1.ExecSQL;
                                        DB.CommitTrans;
                                   except
                                        DB.RollbackTrans;
                                        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                                   end;
                              end;

                         except
                              xNonInviati := xNonInviati + QAnag2.FieldByName('Cognome').asString + ' ' + QAnag2.FieldByName('Nome').asString + chr(13);
                         end;
                    end;
                    QAnag2.Close;
               end;
          end;
          if xCcn then begin
               // OLD --> ShellExecute(0, 'Open', pchar('mailto:?subject=' + StringReplace(Trim(QTipiMailCand.FieldByName('Oggetto').AsString), '"', '''''', [rfReplaceAll]) + '&bcc=' + Copy(xCCNStringa, 1, length(xCCNStringa) - 1) + '&body=' + stringreplace(xMess.Text, '&', '%26', [rfReplaceAll])), '', '', SW_SHOW);
               try
                    SendEMailMAPI(QTipiMailCand.FieldByName('Oggetto').AsString, xMess.Text, '', Copy(xCCNStringa, 1, length(xCCNStringa) - 1), '', MainForm.xSendMail_Remote, MainForm.xDebug, True, False);
                    //ShellExecute(0, 'Open', pchar('mailto:?subject=' + StringReplace(Trim(QTipiMailCand.FieldByName('Oggetto').AsString), '"', '''''', [rfReplaceAll]) + '&bcc=' + Copy(xCCNStringa, 1, length(xCCNStringa) - 1) + '&body=' + stringreplace(xMess.Text, '&', '%26', [rfReplaceAll])), '', '', SW_SHOW);
               finally
                    //   ShowMessage('Operazione completata');
                    //   SetWindowPos(Self.Handle,
                    //        HWND_TOPMOST,
                    //        0, 0, 0, 0,
                    //        SWP_NOMOVE or
                    //        SWP_NOACTIVATE or
                    //       SWP_NOSIZE);
                    // ModalResult := mrOK;
               end;

          end;
          // disconnessione
          Q.Close;
          xMess.Free;
          StatusBar1.Visible := False;
     end else begin
          // OLD --> ShellExecute(0, 'Open', pchar('mailto:' + Data.TAnagrafica.FieldByName('Email').asString + '?subject=' + StringReplace(Trim(QTipiMailCand.FieldByName('Oggetto').AsString), '"', '''''', [rfReplaceAll]) + '&body=' + stringreplace(xMess.Text, '&', '%26', [rfReplaceAll])), '', '', SW_SHOW);
          try
               SendEMailMAPI(QTipiMailCand.FieldByName('Oggetto').AsString, xMess.Text, Data.TAnagrafica.FieldByName('Email').asString, '', '-', MainForm.xSendMail_Remote, MainForm.xDebug, True, False);
               //ShellExecute(0, 'Open', pchar('mailto:' + Data.TAnagrafica.FieldByName('Email').asString + '?subject=' + StringReplace(Trim(QTipiMailCand.FieldByName('Oggetto').AsString), '"', '''''', [rfReplaceAll]) + '&body=' + stringreplace(xMess.Text, '&', '%26', [rfReplaceAll])), '', '', SW_SHOW);
          finally
               //  ShowMessage('Operazione completata');
               //  SetWindowPos(Self.Handle,
               //       HWND_TOPMOST,
               //       0, 0, 0, 0,
               //       SWP_NOMOVE or
               //       SWP_NOACTIVATE or
               //       SWP_NOSIZE);
               //ModalResult := mrOK;
          end;

          Q.Close;
          Data.DB.BeginTrans;
          try
               Data.Q1 := CreateQueryFromString('insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDUtente,IDCliente) ' +
                    'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDUtente:,:xIDCliente:)');
               Data.Q1.ParamByName['xIDAnagrafica'] := Data.TAnagraficaID.Value;
               Data.Q1.ParamByName['xIDEvento'] := 71; // 71=inviata via e-mail
               Data.Q1.ParamByName['xDataEvento'] := Date;
               Data.Q1.ParamByName['xAnnotazioni'] := 'invio e-mail di ' + QTipiMailCand.FieldByName('Descrizione').AsString;
               Data.Q1.ParamByName['xIDUtente'] := MainForm.xIDUtenteAttuale;
               Data.Q1.ParamByName['xIDCliente'] := NULL;
               Data.Q1.ExecSQL;

               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
          end;
     end;

     Application.ProcessMessages;

     //Browser.Free;
     {if xNonInviati <> '' then
          ShowMessage('Messaggi non inviati ai seguenti soggetti per mancanza o errato indirizzo e-mail:' + chr(13) +
               xnonInviati)
     else ShowMessage('Messaggi inviati con successo');}
     if CBcandMailSMS.checked then begin
          if xNonInviatiSMS <> '' then
               ShowMessage('SMS non inviati ai seguenti soggetti per mancanza o errato numero cellulare:' + chr(13) +
                    xNonInviatiSMS)
          else ShowMessage('SMS accodati con successo per tutti i destinatari. ' + chr(13) +
                    'Usare SMSDispatcher per inviare i messaggi stessi.');
     end;

     Sleep(2000);
     Pathname := ExtractFilePath(Application.ExeName);
     for i := 0 to j - 1 do
          DeleteFile(Pathname + 'test' + inttostr(i) + '.eml');
end;


//Functions non pi� in utilizzo per passaggio all'embarcadero - Thomas 26-11-2013
{procedure TFaxPresentazForm.NMSMTP1Connect(Sender: TObject);
begin
     StatusBar1.SimpleText := 'Connesso all''Host ' + EHost.Text;
end;

procedure TFaxPresentazForm.NMSMTP1Disconnect(Sender: TObject);
begin
     if StatusBar1 <> nil then
          StatusBar1.SimpleText := 'Disconnesso';
end;

procedure TFaxPresentazForm.NMSMTP1ConnectionFailed(Sender: TObject);
begin
     ShowMessage('Connessione fallita all''Host ' + EHost.Text);
end;

procedure TFaxPresentazForm.NMSMTP1Status(Sender: TComponent;
     Status: string);
begin
     if StatusBar1 <> nil then
          StatusBar1.SimpleText := status;
end;

procedure TFaxPresentazForm.NMSMTP1EncodeStart(Filename: string);
begin
     StatusBar1.SimpleText := 'Encoding ' + Filename;
end;

procedure TFaxPresentazForm.NMSMTP1EncodeEnd(Filename: string);
begin
     StatusBar1.SimpleText := 'Finished encoding ' + Filename;
end;

procedure TFaxPresentazForm.NMSMTP1Failure(Sender: TObject);
begin
     StatusBar1.SimpleText := 'Spedizione Fallita ';
end;

procedure TFaxPresentazForm.NMSMTP1HostResolved(Sender: TComponent);
begin
     StatusBar1.SimpleText := 'Risoluzione Host avvenuta';
end;

procedure TFaxPresentazForm.NMSMTP1PacketSent(Sender: TObject);
begin
     StatusBar1.SimpleText := 'inviati ' + IntToStr(NMSMTP1.BytesSent) +
          ' bytes dei ' + IntToStr(NMSMTP1.BytesTotal) + ' totali da inviare';
end;

procedure TFaxPresentazForm.NMSMTP1RecipientNotFound(Recipient: string);
begin
     ShowMessage('ERRORE: Recipient "' + Recipient + '" not found');
end;

procedure TFaxPresentazForm.NMSMTP1SendStart(Sender: TObject);
begin
     StatusBar1.simpleText := 'Messaggio in spedizione';
end;

procedure TFaxPresentazForm.NMSMTP1Success(Sender: TObject);
begin
     StatusBar1.SimpleText := 'Messaggio spedito con successo';
end;

procedure TFaxPresentazForm.NMSMTP1HeaderIncomplete(var handled: Boolean;
     hiType: Integer);
begin
     ShowMessage('Header (intestazione) incompleto.');
end;  }

procedure TFaxPresentazForm.SpeedButton1Click(Sender: TObject);
var xInfo, xFirma: string;
     xCRpos, i: integer;
begin
     EmailConnectInfoForm := TEmailConnectInfoForm.create(self);
     EmailConnectInfoForm.xIDUser := MainForm.xIDUtenteAttuale;
     EmailConnectInfoForm.ShowModal;
     if EmailConnectInfoForm.ModalResult = mrOK then begin
          // aggiornamento database
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text := 'update Users set EmailConnectInfo=:xEmailConnectInfo:, FirmaMail=:xFirmaMail: ' +
                         'where ID=' + IntToStr(MainForm.xIDUtenteAttuale);
                    Q1.ParamByName['xEmailConnectInfo'] := EmailConnectInfoForm.EHost.text + ';' +
                         EmailConnectInfoForm.EPort.text + ';' +
                         EmailConnectInfoForm.EUserID.text + ';' +
                         EmailConnectInfoForm.EName.text + ';' +
                         EmailConnectInfoForm.EAddress.text;
                    xFirma := '';
                    for i := 0 to EmailConnectInfoForm.Memo1.lines.count - 1 do
                         xFirma := xFirma + EmailConnectInfoForm.Memo1.lines[i] + chr(13);
                    //                    Q1.ParamByName['xFirmaMail').asString:=xFirma;
                    Q1.ParamByName['xFirmaMail'] := xFirma;
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    // RIcaricamento info connessione SMTP
                    Q.Close;
                    Q.SQL.text := 'select Nominativo,EmailConnectInfo ' +
                         'from Users where ID=' + IntToStr(MainForm.xIDUtenteAttuale);
                    Q.Open;
                    // host
                    xInfo := Q.FieldByName('EmailConnectInfo').asString;
                    if xInfo = '' then begin
                         EHost.text := '';
                         EPort.text := '';
                         EUserID.text := '';
                         EName.text := '';
                         EAddress.text := '';
                    end else begin
                         //xCRpos:=pos(chr(13),xInfo);
                         xCRpos := pos(';', xInfo);
                         EHost.text := copy(xInfo, 1, xCRpos - 1);
                         // port
                         xinfo := copy(xInfo, xCRpos + 1, length(xInfo));
                         //xCRpos:=pos(chr(13),xInfo);
                         xCRpos := pos(';', xInfo);
                         EPort.text := copy(xInfo, 1, xCRpos - 1);
                         // userID
                         xinfo := copy(xInfo, xCRpos + 1, length(xInfo));
                         //xCRpos:=pos(chr(13),xInfo);
                         xCRpos := pos(';', xInfo);
                         EUserID.text := copy(xInfo, 1, xCRpos - 1);
                         // name
                         xinfo := copy(xInfo, xCRpos + 1, length(xInfo));
                         //xCRpos:=pos(chr(13),xInfo);
                         xCRpos := pos(';', xInfo);
                         EName.text := copy(xInfo, 1, xCRpos - 1);
                         // address
                         xinfo := copy(xInfo, xCRpos + 1, length(xInfo));
                         EAddress.text := xInfo;
                    end;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               end;
          end;
     end;
     EmailConnectInfoForm.Free;
end;

procedure TFaxPresentazForm.SpeedButton5Click(Sender: TObject);
begin
     TipiMailCandForm := TTipiMailCandForm.Create(self);
     TipiMailCandForm.ShowModal;
     TipiMailCandForm.Free;
     QTipiMailCand.Close;
     QTipiMailCand.Open;
end;

procedure TFaxPresentazForm.ExpressButton5Click(Sender: TObject);
begin
     PCSel.ActivePageIndex := 4;
     PCSel.Visible := true;
     ExpressButton1.Down := False;
     ExpressButton2.Down := False;
     ExpressButton3.Down := False;
     ExpressButton4.Down := False;
     ExpressButton5.Down := True;
     QTipiMailCand.Close;
     CaricaInfoConnect;
end;

procedure TFaxPresentazForm.BitBtn6Click(Sender: TObject);
var i, n, z, xTot, xCRpos, j, count: integer;
     xMess, xMail, xFileTemp, Mail, xallega: TstringList;
     xAttach: TStrings;
     Vero: boolean;
     xTipoCV, xS, xBody, xnomefiletemp, xStr, xAttachments, pathname: string;
begin
     // controllo esistenza destinatario
     if TContattiCli.IsEmpty then begin
          MessageDlg('nessun contatto per il cliente. Impossibile proseguire', mtError, [mbOK], 0);
          Exit;
     end;
     // controllo e-mail destinatario
     if TContattiCli.FieldByName('Email').Value = '' then begin
          MessageDlg('nessun indirizzo di posta elettronica ' + chr(13) +
               'disponibile per il destinario selezionato. Impossibile proseguire', mtError, [mbOK], 0);
          Exit;
     end;

     xMail := TStringList.Create;
     xFileTemp := TStringList.Create;
     // messaggio PRIVACY
     MessageDlg('ATTENZIONE!! Prima di procedere con la presentazione di personale ' +
          'accertarsi che il/i candidato/i sia/siano consenziente/i. (Decreto Legislativo 30 Giugno 2003 N. 196)', mtInformation, [mbOK], 0);
     xTot := 0;
     for i := 1 to xTotRighe do begin
          ASG1.GetCheckBoxState(0, i, Vero);
          if Vero then inc(xTot);
     end;
     if (xtot = 0) and (CBCliMailCand.Checked) then begin
          MessageDlg('Nessun candidato selezionato!', mtError, [mbOK], 0);
          Exit;
     end;

     // COMPOSIZIONE MESSAGGIO
     xmail.values['subject'] := EClimailOgg.Text;
     xMess := TStringList.create;
     // intestazione (da tabella)
     for i := 0 to MMail.lines.count - 1 do
          xMess.Add(MMail.lines[i]);
     xMess.Add('');
     Q.Close;
     Q.SQL.Text := 'select Descrizione,FirmaMail from Users where ID=' + DataRicerche.TRicerchePend.FieldByName('IDUtente').asString;
     Q.Open;
     // dati ricerca
     if CBCliMailRifRic.Checked then begin
          xMess.Add('Codice Ricerca:  ' + DataRicerche.TRicerchePend.FieldByName('Progressivo').AsString);
          xMess.Add('Posizione ricercata:  ' + DataRicerche.TRicerchePend.FieldByName('Mansione').AsString + ' (area: ' + DataRicerche.TRicerchePend.FieldByName('Area').AsString + ')');
          xMess.Add('Selezionatore: ' + Q.FieldByName('Descrizione').asString);
          xMess.Add('');
     end;
     // elenco soggetti e CV allegati
     xAttach := TStringList.Create;
     xAttachments := '';
     if CBCliMailCand.Checked then begin
          if RGCliMailOpz.ItemIndex = 0 then xTipoCV := 'i' else xTipoCV := 'e';
          xMess.Add('Elenco CV inviati:');
          n := 0;
          for i := 1 to xTotRighe do begin
               ASG1.GetCheckBoxState(0, i, Vero);
               if Vero then begin
                    QAnag2.Close;
                    QAnag2.SQL.Text := 'select CVNumero,Cognome,Nome from Anagrafica where ID=' + IntToStr(xArrayIDAnag[i]);
                    QAnag2.Open;
                    {if CBCliMailCognNome.checked then
                         xMess.Add(QAnag2.FieldByName('CVNumero').asString + ' - ' + QAnag2.FieldByName('Cognome').asString + ' ' + QAnag2.FieldByName('Nome').asString)
                    else xMess.Add(QAnag2.FieldByName('CVNumero').asString);}
                    xMess.Add(QAnag2.FieldByName('CVNumero').asString);
                    count := xMess.Count - 1;
                    if CBCliMailCognNome.checked then
                         xMess.Strings[count] := xMess.Strings[count] + ' - ' + QAnag2.FieldByName('Cognome').asString + ' ' + QAnag2.FieldByName('Nome').asString;
                    // cerca e aggiungi pagina/e CV
                    if FileExists(GetCVPath + '\' + xTipoCV + QAnag2.FieldbyName('CVNumero').asString + 'a.gif') then begin
                         //xAttach.Add(GetCVPath + '\' + xTipoCV + QAnag2.FieldbyName('CVNumero').asString + 'a.gif');
                         xmail.values['attachment' + inttostr(n)] := GetCVPath + '\' + xTipoCV + QAnag2.FieldbyName('CVNumero').asString + 'a.gif';
                         n := n + 1;
                    end;
                    if FileExists(GetCVPath + '\' + xTipoCV + QAnag2.FieldbyName('CVNumero').asString + 'b.gif') then begin
                         //xAttach.Add(GetCVPath + '\' + xTipoCV + QAnag2.FieldbyName('CVNumero').asString + 'b.gif');
                         xmail.values['attachment' + inttostr(n)] := GetCVPath + '\' + xTipoCV + QAnag2.FieldbyName('CVNumero').asString + 'b.gif';
                         n := n + 1;
                    end;
                    if FileExists(GetCVPath + '\' + xTipoCV + QAnag2.FieldbyName('CVNumero').asString + 'c.gif') then begin
                         //xAttach.Add(GetCVPath + '\' + xTipoCV + QAnag2.FieldbyName('CVNumero').asString + 'c.gif');
                         xmail.values['attachment' + inttostr(n)] := GetCVPath + '\' + xTipoCV + QAnag2.FieldbyName('CVNumero').asString + 'c.gif';
                         n := n + 1;
                    end;
                    if FileExists(GetCVPath + '\' + xTipoCV + QAnag2.FieldbyName('CVNumero').asString + 'd.gif') then begin
                         //xAttach.Add(GetCVPath + '\' + xTipoCV + QAnag2.FieldbyName('CVNumero').asString + 'd.gif');
                         xmail.values['attachment' + inttostr(n)] := GetCVPath + '\' + xTipoCV + QAnag2.FieldbyName('CVNumero').asString + 'd.gif';
                         n := n + 1;
                    end;
                    if FileExists(GetCVPath + '\' + xTipoCV + QAnag2.FieldbyName('CVNumero').asString + 'e.gif') then begin
                         //xAttach.Add(GetCVPath + '\' + xTipoCV + QAnag2.FieldbyName('CVNumero').asString + 'e.gif');
                         xmail.values['attachment' + inttostr(n)] := GetCVPath + '\' + xTipoCV + QAnag2.FieldbyName('CVNumero').asString + 'e.gif';
                         n := n + 1;
                    end;

                    // se non sono stati trovati file sopra, apre AnagFile: considera quelli con FlagInvio=1
                    QAnag2.Close;
                    QAnag2.SQL.Text := 'select NomeFile,FlagInvio,DocFile,solonome,TipiFileCand.Tipo from AnagFile' +
                         ' join TipiFileCand on AnagFile.IDTipo = TipiFileCand.ID' +
                         ' where FlagInvio=1 and IDAnagrafica=' + IntToStr(xArrayIDAnag[i]);
                    QAnag2.Open;
                    if Data.Global.FieldByName('AnagFileDentroDB').asBoolean then begin
                         //Data.qFileTemp.Close;
                         //data.qFileTemp.SQL.Text := 'select * from anagfile where FlagInvio = 1 and IDAnagrafica =' + IntToStr(xArrayIDAnag[i]);
                         //Data.qFileTemp.Filtered := true;
                         // Data.qFileTemp.Filter := 'FlagInvio = 1 and IDAnagrafica = ' + IntToStr(xArrayIDAnag[i]);
                         // Data.qfileTemp.Open;
                         //while not Data.qFileTemp.EOF do begin
                              //xnomefiletemp := PathGetTempPath + Data.qFileTempSoloNome.Value + '.' + Data.qFileTempDocExt.Value;
                              //xnomefiletemp := PathGetTempPath + Data.qFileTempSoloNome.Value;
                              //xnomefiletemp := data.Global.fieldbyname('DirFileLog').asstring + '\' + Data.qFileTempSoloNome.Value;
                              //xFileTemp.Add(xnomefiletemp);
                              //Data.qFileTempDocFile.SaveToFile(xnomefiletemp);
                              //if FileExists(QAnag2.FieldByName('NomeFile').asString) then begin
                              //xAttach.Add(xnomefiletemp);
                              //Data.qFileTemp.Next;
                         //end;
                         //QAnag2.Close;
                         //QAnag2.SQL.Text := 'select NomeFile,FlagInvio,DocFile,solonome,fileType from AnagFile where FlagInvio=1 and IDAnagrafica=' + IntToStr(xArrayIDAnag[i]);
                         //QAnag2.Open;
                         while not QAnag2.EOF do begin
                              xallega := TStringList.Create;
                              if QAnag2.fieldbyname('DocFile').asstring <> '' then begin
                                   xallega.Add(QAnag2.fieldbyname('DocFile').asstring);
                                   xnomefiletemp := data.Global.fieldbyname('DirFileLog').asstring + '\' + QAnag2.FieldByName('solonome').asString;
                                   xFileTemp.Add(xnomefiletemp);
                                   xallega.SaveToFile(xnomefiletemp);
                                   xallega.Free;
                                   xAttachments := xAttachments + xnomefiletemp + ';';
                                   xmail.values['attachment' + inttostr(n)] := xnomefiletemp;
                                   n := n + 1;
                              end;
                              QAnag2.Next;
                         end;
                         //QAnag2.Close;
                    end else begin
                         //QAnag2.Close;
                         //QAnag2.SQL.Text := 'select NomeFile,FlagInvio,DocFile,solonome,fileType from AnagFile where FlagInvio=1 and IDAnagrafica=' + IntToStr(xArrayIDAnag[i]);
                         //QAnag2.Open;
                         while not QAnag2.EOF do begin
                              if FileExists(QAnag2.FieldByName('NomeFile').asString) then begin
                                   //xAttach.Add(QAnag2.FieldByName('NomeFile').asString);
                                   xAttachments := xAttachments + QAnag2.FieldByName('NomeFile').asString + ';';
                                   xmail.values['attachment' + inttostr(n)] := QAnag2.FieldByName('NomeFile').asString;
                                   n := n + 1;
                              end;
                              QAnag2.Next;
                         end;
                         //QAnag2.Close;
                    end;
                    if CBCliMailNomeTipoFile.checked then
                         xMess.Strings[count] := xMess.Strings[count] + ' - ' + QAnag2.FieldByName('solonome').asString + ' - ' + QAnag2.FieldByName('Tipo').asString;
                    QAnag2.Close;


                    // registrazione nello storico del candidato e del cliente
                    with Data do begin
                         DB.BeginTrans;
                         try
                              Q1.close;
                              // storico candidato
                              Q1 := CreateQueryFromString('insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
                                   'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xIDCliente:)');
                              Q1.ParamByName['xIDAnagrafica'] := xArrayIDAnag[i];
                              Q1.ParamByName['xIDEvento'] := 83; // 83=presentato via e-mail...
                              Q1.ParamByName['xDataEvento'] := Date;
                              Q1.ParamByName['xAnnotazioni'] := DataRicerche.TRicerchePend.FieldByName('Cliente').AsString + ' ' + EClimailOgg.Text;
                              Q1.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsINteger;
                              Q1.ParamByName['xIDUtente'] := MainForm.xIDUtenteAttuale;
                              Q1.ParamByName['xIDCliente'] := DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger;
                              Q1.ExecSQL;
                              Q1.close;
                              // storico invio cliente
                              Q1 := CreateQueryFromString('insert into StoricoInvioClienti (IDCliente,Tipo,Data,Descrizione) ' +
                                   'values (:xIDCliente:,:xTipo:,:xData:,:xDescrizione:)');
                              Q1.ParamByName['xIDCliente'] := DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger;
                              Q1.ParamByName['xTipo'] := 'e-mail';
                              Q1.ParamByName['xData'] := Date;
                              Q1.ParamByName['xDescrizione'] := 'invio CV in allegato';
                              Q1.ExecSQL;
                              DB.CommitTrans;
                         except
                              DB.RollbackTrans;
                              MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                         end;
                    end;
               end;
               QAnag2.Close;
          end;
          //aggiungi tutti gli attachment
          //NMSMTP1.PostMessage.Attachments.AddStrings(xAttach);
     end;
     // saluti e firma (da tabella Utenti) con ritorno a capo
     xMess.Add('');
     xS := Q.FieldByName('FirmaMail').asString;
     while pos(chr(13), xS) > 0 do begin
          xCRpos := pos(chr(13), xS);
          xMess.Add(copy(xS, 1, xCRpos - 1));
          xS := copy(xS, xCRpos + 1, length(xS));
     end;
     xMess.Add(xS);
     // messaggio CONFIDENTIAL
     xMess.Add('');
     xMess.Add('ATTENTION:  Privileged - Confidential information may be contained in this message.');
     xMess.Add(' If you are not the addressee indicated in this message, you may not copy or deliver this message to anyone.');
     // edit messaggio
     EmailMessageForm := TEmailMessageForm.create(self);
     EmailMessageForm.Memo1.Lines := xMess;
     EmailMessageForm.ShowModal;
     if EmailMessageForm.ModalResult = mrCancel then begin
          EmailMessageForm.Free;
          exit;
     end;
     xmess.Clear;
     for i := 0 to EmailMessageForm.Memo1.Lines.count - 1 do begin
          xMess.Add(EmailMessageForm.Memo1.Lines[i]); // + '%0D%0A');
          xBody := xBody + chr(13) + EmailMessageForm.Memo1.Lines[i];
     end;
     xmail.values['body'] := xBody;
     //ShowMessage('Predisporre connessione Internet e premere OK quando pronti');
     EmailMessageForm.Free;
     StatusBar1.Visible := True;
     xmail.values['to'] := TContattiCli.FieldByName('Email').Value;

     // invio
     try
          SendEMailMAPI(EClimailOgg.Text, xBody, TContattiCli.FieldByName('Email').Value, '', xAttachments, MainForm.xSendMail_Remote, MainForm.xDebug, True, False);
     finally
          //  ShowMessage('Operazione completata');

          //  SetWindowPos(Self.Handle,
          //       HWND_TOPMOST,
          //       0, 0, 0, 0,
          //       SWP_NOMOVE or
          //       SWP_NOACTIVATE or
          //       SWP_NOSIZE);
     end;

     Application.ProcessMessages;

     Q.Close;
     //Browser.Free;

     if data.Global.FieldByname('AnagFileDentroDB').AsBoolean then
          for z := 0 to xFileTemp.Count - 1 do begin
               DeleteFile(PChar(xFileTemp[z]));
          end;
     xMess.Free;
     xMail.Free;
     xFileTemp.Free;
     StatusBar1.Visible := False;
     xAttach.Free;
     //ShowMessage('Messaggi inviati con successo');
     if j > i then
          Sleep(2000);
     Pathname := ExtractFilePath(Application.ExeName);
     for i := 0 to j - 1 do
          DeleteFile(Pathname + 'test' + inttostr(i) + '.eml');
end;

procedure TFaxPresentazForm.CBCliMailCandClick(Sender: TObject);
begin
     GroupBox2.Visible := CBCliMailCand.Checked;
end;

procedure TFaxPresentazForm.CaricaInfoConnect;
var xInfo: string;
     xCRpos: integer;
begin
     // caricamento info connessione SMTP
     Q.Close;
     Q.SQL.text := 'select Nominativo,EmailConnectInfo ' +
          'from Users where ID=' + IntToStr(MainForm.xIDUtenteAttuale);
     Q.Open;
     // host
     xInfo := Q.FieldByName('EmailConnectInfo').asString;
     if xInfo = '' then begin
          EHost.text := '';
          EPort.text := '';
          EUserID.text := '';
          EName.text := '';
          EAddress.text := '';
     end else begin
          if pos(';', xInfo) = 0 then begin
               MessageDlg('ATTENZIONE: correggere parametri di configurazione e-mail', mtError, [mbOK], 0);
               exit;
          end else begin
               //xCRpos:=pos(chr(13),xInfo);
               xCRpos := pos(';', xInfo);
               EHost.text := copy(xInfo, 1, xCRpos - 1);
               // port
               xinfo := copy(xInfo, xCRpos + 1, length(xInfo));
               //xCRpos:=pos(chr(13),xInfo);
               xCRpos := pos(';', xInfo);
               EPort.text := copy(xInfo, 1, xCRpos - 1);
               // userID
               xinfo := copy(xInfo, xCRpos + 1, length(xInfo));
               //xCRpos:=pos(chr(13),xInfo);
               xCRpos := pos(';', xInfo);
               EUserID.text := copy(xInfo, 1, xCRpos - 1);
               // name
               xinfo := copy(xInfo, xCRpos + 1, length(xInfo));
               //xCRpos:=pos(chr(13),xInfo);
               xCRpos := pos(';', xInfo);
               EName.text := copy(xInfo, 1, xCRpos - 1);
               // address
               xinfo := copy(xInfo, xCRpos + 1, length(xInfo));
               EAddress.text := xInfo;
          end;
     end;
end;

procedure TFaxPresentazForm.CBcandMailSMSClick(Sender: TObject);
begin
     EavvisoSMS.Visible := CBcandMailSMS.Checked;
     CBDispatcher1.Visible := CBcandMailSMS.Checked;
     if CBcandMailSMS.Checked then
          //[/TONI20020729\]
          //          EavvisoSMS.text:=EavvisoSMS.text+' '+Data.GlobalNomeAzienda.Value;
          EavvisoSMS.text := EavvisoSMS.text + ' ' + Data.Global.FieldByName('NomeAzienda').Value;
     //[/TONI20020729\]FINE
end;

procedure TFaxPresentazForm.BitBtn5Click(Sender: TObject);
var i: integer;
     vero: boolean;
     xUrl, xNonInviati, xDest: string;
     xSMStrend: TStringList;
begin
     //INVIO SMS (tramite SMStrend)
     if not FileExists(ExtractFileDir(Application.ExeName) + '\SMStrend.txt') then begin
          MessageDlg('File "SMStrend.txt" non trovato nella cartella:' + chr(13) +
               ExtractFileDir(Application.ExeName), mtError, [mbOK], 0);
          exit;
     end;

     xSMStrend := TStringList.create;
     xSMStrend.LoadFromFile(ExtractFileDir(Application.ExeName) + '\SMStrend.txt');

     for i := 1 to xTotRighe do begin
          ASG1.GetCheckBoxState(0, i, Vero);
          if Vero then begin
               // recupero numero di cellulare
               Data.QTemp.Close;
               Data.QTemp.SQL.Clear;
               Data.QTemp.SQL.text := 'select Cognome+'' ''+Nome Nominativo, Cellulare from Anagrafica where ID=' + IntToStr(xArrayIDAnag[i]);
               Data.QTemp.Open;
               xDest := Data.QTemp.FieldByName('Cellulare').asString;
               if xDest = '' then begin
                    MessageDlg('Impossibile inviare l''SMS a ' + Data.QTemp.FieldByName('Nominativo').asString + chr(13) +
                         'Numero di cellulare mancante', mtError, [mbOK], 0);
               end else begin
                    // sostituzione eventuali caratteri
                    xDest := StringReplace(xDest, '/', '', [rfReplaceAll]);
                    xDest := StringReplace(xDest, '-', '', [rfReplaceAll]);
                    xDest := StringReplace(xDest, ' ', '', [rfReplaceAll]);

                    if MessageDlg('Confermare invio a: ' + xDest, mtInformation, [mbYes, mbNo], 0) = mrYes then begin
                         xUrl := xSMStrend.Values['URL'] + '?login=' + xSMStrend.Values['login'] + '&password=' + xSMStrend.Values['password'] + '&message_type=' + xSMStrend.Values['message_type'] + '&recipient=' + xDest + '&message=' + ETestoSMS.text + '&sender=' + EMittente.Text;
                         xIDAnagSMS := xArrayIDAnag[i];
                         
                         WebBrowser1.Navigate(xUrl);
                         sleep(2000);
                    end;
               end;

          end;
     end;

     xSMStrend.Free;
end;

procedure TFaxPresentazForm.ASG1GetCellColor(Sender: TObject; ARow,
     ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
     // controllo esistenza indirizzo e-mail e cellulare
     if ARow > 0 then begin
          QAnag2.Close;
          QAnag2.SQL.Text := 'select email,cellulare from Anagrafica where ID=' + IntToStr(xArrayIDAnag[aRow]);
          QAnag2.Open;
          if ACol = 1 then begin // e-mail
               if QAnag2.FieldByName('email').asString = '' then
                    ABrush.Color := clRed
               else ABrush.Color := clLime;
          end;
          if ACol = 2 then begin // cellulare
               if QAnag2.FieldByName('cellulare').asString = '' then
                    ABrush.Color := clRed
               else ABrush.Color := clLime;
          end;
          QAnag2.Close;
     end;
end;

procedure TFaxPresentazForm.ASG1CanEditCell(Sender: TObject; Arow,
     Acol: Integer; var canedit: Boolean);
begin
     if (ARow > 0) and (Acol > 0) then canedit := False
     else canedit := True;
end;

procedure TFaxPresentazForm.ExpressButton6Click(Sender: TObject);
var xFile: string;
     xSalva, xStampa, Vero: boolean;
     i: integer;
begin
     SceltaModelloAnagForm := TSceltaModelloAnagForm.create(self);
     SceltaModelloAnagForm.Height := 274;
     SceltaModelloAnagForm.QModelliWord.SQL.text := 'select * from ModelliWord ' +
          'where TabellaMaster=:xTabMaster:';
     SceltaModelloAnagForm.QModelliWord.ParamByName['xTabMaster'] := 'Anagrafica';
     SceltaModelloAnagForm.QModelliWord.Open;
     SceltaModelloAnagForm.ShowModal;
     if SceltaModelloAnagForm.ModalResult = mrOK then begin
          xsalva := SceltaModelloAnagForm.CBSalva.Checked;
          xStampa := False;
          for i := 1 to xTotRighe do begin
               ASG1.GetCheckBoxState(0, i, Vero);
               if Vero then begin
                    PosizionaAnag(xArrayIDAnag[i]);
                    if MainForm.xSendMail_Remote then
                         xFile := RiempiModelloWord_remoto(SceltaModelloAnagForm.QModelliWord.FieldByName('ID').AsInteger, Data.TAnagrafica.FieldByName('ID').AsInteger)
                    else
                         xFile := CreaFileWordAnag(xArrayIDAnag[i], SceltaModelloAnagForm.QModelliWord.FieldByName('ID').Value,
                              SceltaModelloAnagForm.QModelliWord.FieldByName('NomeModello').Value,
                              xSalva, xStampa, TModelCompositionTV.Create(self));
                    if xsalva then begin
                         // associa al soggetto
                         with Data do begin
                              DB.BeginTrans;
                              try
                                   Q1.Close;
                                   Q1.SQL.text := 'insert into AnagFile (IDAnagrafica,IDTipo,Descrizione,NomeFile,DataCreazione,SoloNome,FileType,FlagInvio,VisibleCliente) ' +
                                        'values (:xIDAnagrafica:,:xIDTipo:,:xDescrizione:,:xNomeFile:,:xDataCreazione:,:xSoloNome:,:xFileType:,:xFlagInvio:,:xVisibleCliente:)';
                                   Q1.ParamByName['xIDAnagrafica'] := xArrayIDAnag[i];
                                   Q1.ParamByName['xIDTipo'] := 3;
                                   Q1.ParamByName['xDescrizione'] := SceltaModelloAnagForm.QModelliWord.FieldByName('Descrizione').AsString;
                                   Q1.ParamByName['xNomeFile'] := xFile;
                                   Q1.ParambyName['xSoloNome'] := ExtractFileName(xFile);
                                   Q1.ParamByName['xDataCreazione'] := Date;
                                   Q1.ParambyName['xFileType'] := GetMIMETypeFromFile(xFile);
                                   Q1.ParambyName['xFlagInvio'] := SceltaModelloAnagForm.CBFlagInvio.checked;
                                   Q1.ParambyName['xVisibleCliente'] := SceltaModelloAnagForm.CBVisibleCliente.checked;
                                   Q1.ExecSQL;
                                   DB.CommitTrans;
                              except
                                   DB.RollbackTrans;
                                   MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                              end;
                         end;
                    end;
               end;
          end;
     end;
     SceltaModelloAnagForm.Free;
end;

procedure TFaxPresentazForm.ExpressButton7Click(Sender: TObject);
var xFile: string;
     Vero: boolean;
     i: integer;
begin
     ShowMessage('Utilizzare pulsante in anagrafica soggetti');
     Exit;

     // DISABILITATO !!
     // presentazione a seconda del cliente - sorgente in MAINFORM
   //[/TONI20020729\]
   //     if Uppercase(copy(Data.GlobalNomeAzienda.Value,1,6))='LEDERM' then begin
     if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').Value, 1, 6)) = 'LEDERM' then begin
          // seleziona contatto
      //          SelContattiForm.QContatti.ParamByName['xIDCliente').asInteger:=DataRicerche.TRicerchePendIDCliente.asInteger;
          SelContattiForm := TSelContattiForm.create(self);
          SelContattiForm.QContatti.SQL.Text := 'select ID,Contatto,Telefono,IDAnagrafica from EBC_ContattiClienti where IDCliente=' + DataRicerche.TRicerchePend.FieldByName('IDCliente').AsString;
          SelContattiForm.QContatti.Open;
          SelContattiForm.ShowModal;
          if SelContattiForm.Modalresult = mrOK then
               for i := 1 to xTotRighe do begin
                    ASG1.GetCheckBoxState(0, i, Vero);
                    if Vero then begin
                         PosizionaAnag(xArrayIDAnag[i]);

                         {                         MainForm.CompilaFilePresPosLWG(DataRicerche.TRicerchePendMansione.Value,
                                                       DataRicerche.QCandRicID.Value,
                                                       DataRicerche.TRicerchePendIDCliente.Value,
                                                       SelContattiForm.QContattiID.Value);}
                         MainForm.CompilaFilePresPosLWG(DataRicerche.TRicerchePend.FieldByName('Mansione').Value,
                              DataRicerche.QCandRic.FieldByName('ID').Asinteger,
                              DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger,
                              SelContattiForm.QContatti.FieldbyName('ID').AsInteger, 'Ledermann');
                    end;
                    SelContattiForm.Free;
               end;
     end else begin
          for i := 1 to xTotRighe do begin
               ASG1.GetCheckBoxState(0, i, Vero);
               if Vero then begin
                    PosizionaAnag(xArrayIDAnag[i]);
                    {                    MainForm.CompilaFilePresPos(DataRicerche.TRicerchePendCliente.Value,
                                             DataRicerche.TRicerchePendMansione.Value,
                                             DataRicerche.QCandRicID.Value);}
                    MainForm.CompilaFilePresPos(DataRicerche.TRicerchePend.FieldByName('Cliente').Value,
                         DataRicerche.TRicerchePend.FieldByName('Mansione').Value,
                         DataRicerche.QCandRic.FieldByName('ID').Value);
                    //[/TONI20020729\]FINE
               end;
          end;
     end;
end;

procedure TFaxPresentazForm.QTipiMailCandAfterOpen(DataSet: TDataSet);
begin
     if QTipiMailcand.FieldByName('AddQuest').value then begin
          PanQuest.Visible := True;
          QQuest.Open;
     end;
end;

procedure TFaxPresentazForm.QTipiMailCandAfterScroll(DataSet: TDataSet);
begin
     if QTipiMailcand.FieldByName('AddQuest').value then begin
          PanQuest.Visible := True;
          if not QQuest.active then QQuest.Open;
     end else PanQuest.Visible := False;
end;

procedure TFaxPresentazForm.BitBtn7Click(Sender: TObject);
begin
     FaxPresentazForm.Close;
end;

function TFaxPresentazForm.Decodifica: string;
var iall: IHTMLElement;
     IDoc: IHTMLDocument2;
     Strl: TStringList;
     sHTMLFile, xUrl: string;
     v: Variant;
begin
     with FaxPresentazForm do begin
          if Assigned(WebBrowser1.Document) then
          begin
               Strl := TStringList.Create;
               iall := (WebBrowser1.Document as IHTMLDocument2).body;

               while iall.parentElement <> nil do
               begin
                    iall := iall.parentElement;
               end;
               Strl.Text := iall.outerHTML;

               //sHTMLFile := edit1.text;

               try
                    Idoc := CreateComObject(Class_HTMLDOcument) as IHTMLDocument2;
                    try
                         IDoc.designMode := 'on';
                         while IDoc.readyState <> 'complete' do
                              Application.ProcessMessages;
                         v := VarArrayCreate([0, 0], VarVariant);
                         v[0] := Strl.Text;
                         IDoc.Write(PSafeArray(System.TVarData(v).VArray));
                         IDoc.designMode := 'off';
                         while IDoc.readyState <> 'complete' do
                              Application.ProcessMessages;

                         Result := IDoc.body.innerText;

                    finally
                         IDoc := nil;
                    end;
               finally
                    Strl.Free;
               end;
               exit;
          end;
     end;
end;

procedure TFaxPresentazForm.WebBrowser1DocumentComplete(Sender: TObject;
     const pDisp: IDispatch; var URL: OleVariant);
begin
     xS_Res_InvioSMS := Decodifica;

     if copy(xS_Res_InvioSMS, 1, 2) = 'OK' then begin
          // storicizzazione
          Data.Q1.Close;
          Data.Q1.SQL.Clear;
          Data.Q1.SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
               'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xIDCliente:)';
          Data.Q1.ParamByName['xIDAnagrafica'] := xIDAnagSMS;
          Data.Q1.ParamByName['xIDEvento'] := 72;
          Data.Q1.ParamByName['xDataEvento'] := Date;
          Data.Q1.ParamByName['xAnnotazioni'] := 'Stringa restituita da SMStrend: ' + xS_Res_InvioSMS;
          Data.Q1.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
          Data.Q1.ParamByName['xIDUtente'] := MainForm.xIDUtenteAttuale;
          Data.Q1.ParamByName['xIDCliente'] := DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger;
          Data.Q1.ExecSQL;
     end else begin
          MessageDlg('ERRORE invio - Stringa SMStrend: ' + xS_Res_InvioSMS, mtError, [mbOK], 0);
     end;

end;

procedure TFaxPresentazForm.BitBtn8Click(Sender: TObject);
var xSMStrend: TStringList;
     xUrl: string;
begin
     if not FileExists(ExtractFileDir(Application.ExeName) + '\SMStrend.txt') then begin
          MessageDlg('File "SMStrend.txt" non trovato nella cartella:' + chr(13) +
               ExtractFileDir(Application.ExeName), mtError, [mbOK], 0);
          exit;
     end;

     xSMStrend := TStringList.create;
     xSMStrend.LoadFromFile(ExtractFileDir(Application.ExeName) + '\SMStrend.txt');

     xUrl := xSMStrend.Values['URL_check'] + '?login=' + xSMStrend.Values['login'] + '&password=' + xSMStrend.Values['password'];
     WebBrowser2.Navigate(xUrl);
end;

procedure TFaxPresentazForm.WebBrowser2DocumentComplete(Sender: TObject;
     const pDisp: IDispatch; var URL: OleVariant);
begin
     xS_Res_Credito := Decodifica2;
     ShowMessage(xS_Res_Credito);
end;

function TFaxPresentazForm.Decodifica2: string;
var iall: IHTMLElement;
     IDoc: IHTMLDocument2;
     Strl: TStringList;
     sHTMLFile, xUrl: string;
     v: Variant;
begin
     with FaxPresentazForm do begin
          if Assigned(WebBrowser2.Document) then
          begin
               Strl := TStringList.Create;
               iall := (WebBrowser2.Document as IHTMLDocument2).body;

               while iall.parentElement <> nil do
               begin
                    iall := iall.parentElement;
               end;
               Strl.Text := iall.outerHTML;

               //sHTMLFile := edit1.text;

               try
                    Idoc := CreateComObject(Class_HTMLDOcument) as IHTMLDocument2;
                    try
                         IDoc.designMode := 'on';
                         while IDoc.readyState <> 'complete' do
                              Application.ProcessMessages;
                         v := VarArrayCreate([0, 0], VarVariant);
                         v[0] := Strl.Text;
                         IDoc.Write(PSafeArray(System.TVarData(v).VArray));
                         IDoc.designMode := 'off';
                         while IDoc.readyState <> 'complete' do
                              Application.ProcessMessages;

                         Result := IDoc.body.innerText;

                    finally
                         IDoc := nil;
                    end;
               finally
                    Strl.Free;
               end;
               exit;
          end;
     end;
end;

procedure TFaxPresentazForm.SpeedButton4Click(Sender: TObject);
var Mail: TstringList;
begin
     Mail := TstringList.create;
     try
          mail.values['to'] := 'gborsari@ebcconsulting.com';
          mail.values['subject'] := 'Oggetto';
          mail.values['body'] := 'Testo';
          //mail.values['attachment0'] := 'C:\Test.txt';
          // mail.values['attachment1']:='C:\Test2.txt';
          sendEMail(Application.Handle, mail);
     finally
          mail.Free;
          ModalResult := mrOK;

          // non funziona
          // Application.processMessages;
          // FaxPresentazForm.Show;

     end;
end;

procedure TFaxPresentazForm.CheckBox1Click(Sender: TObject);
var i: integer;
begin
     if CheckBox1.Checked then
          asg1.CheckAll(0)
     else
          asg1.UnCheckAll(0);

end;

procedure TFaxPresentazForm.BitBtn1Click(Sender: TObject);
begin
     FaxPresentazForm.ModalResult := mrOK;
     FaxPresentazForm.Close;
end;

end.

