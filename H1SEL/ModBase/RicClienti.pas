unit RicClienti;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Mask, DBCtrls, Grids, DBGrids, Db, DBTables, Buttons, TB97,
     ExtCtrls, ComCtrls, ComObj, RXDBCtrl, Psock, NMsmtp, dxDBTLCl, dxGrClms,
     dxTL, dxDBCtrl, dxDBGrid, dxCntner, dxGridMenus, dxPSCore, dxPSdxTLLnk,
     dxPSdxDBGrLnk, ImgList, Menus, dxPSdxDBCtrlLnk, ADODB, U_ADOLinkCl, shdocvw, shellApi, uSendMailMAPI;

type
     TSaveMethod = procedure(const FileName: string; ASaveAll: Boolean) of object;
     TRicClientiForm = class(TForm)
          DsTabQuery: TDataSource;
          DsTabOp: TDataSource;
          DsLinee: TDataSource;
          DsTabLookup: TDataSource;
          DsQRes1: TDataSource;
          DsQTabLista: TDataSource;
          PanRic: TPanel;
          DBGrid3: TDBGrid;
          BitBtn3: TBitBtn;
          GroupBox1: TGroupBox;
          Label1: TLabel;
          Label2: TLabel;
          Label3: TLabel;
          Label4: TLabel;
          DBGrid4: TDBGrid;
          ComboBox1: TComboBox;
          DBGrid1: TDBGrid;
          DBGrid2: TDBGrid;
          Edit1: TEdit;
          BitBtn1: TBitBtn;
          ComboBox2: TComboBox;
          BitBtn6: TBitBtn;
          BitBtn2: TBitBtn;
          Panel1: TPanel;
          BApriForm: TBitBtn;
          Panel2: TPanel;
          Panel3: TPanel;
          Panel10: TPanel;
          LTot: TLabel;
          BitBtn4: TBitBtn;
          BitBtn5: TBitBtn;
          BitBtn7: TBitBtn;
          BitBtn8: TBitBtn;
          DBGrid5: TdxDBGrid;
          PMExport: TPopupMenu;
          Stampagriglia1: TMenuItem;
          EsportainExcel1: TMenuItem;
          EsportainHTML1: TMenuItem;
          ImageList1: TImageList;
          dxPrinter1: TdxComponentPrinter;
          dxPrinter1Link1: TdxDBGridReportLink;
          SaveDialog: TSaveDialog;
          ToolbarButton972: TToolbarButton97;
          DBGrid5Cliente: TdxDBGridMaskColumn;
          DBGrid5Stato: TdxDBGridMaskColumn;
          DBGrid5Comune: TdxDBGridMaskColumn;
          DBGrid5Provincia: TdxDBGridMaskColumn;
          DBGrid5Contatto: TdxDBGridMaskColumn;
          DBGrid5Telefono: TdxDBGridMaskColumn;
          DBGrid5IDCliente: TdxDBGridMaskColumn;
          DBGrid5IDContatto: TdxDBGridMaskColumn;
          DBGrid5DataUltimoCont: TdxDBGridDateColumn;
          DBGrid5Titolo: TdxDBGridMaskColumn;
          DBGrid5StatoNaz: TdxDBGridMaskColumn;
          DBGrid5Sesso: TdxDBGridMaskColumn;
          BAddTargetList: TBitBtn;
          TLinee: TADOLinkedQuery;
          QTabelleLista: TADOLinkedQuery;
          QRes1: TADOLinkedQuery;
          QTabelle: TADOLinkedQuery;
          Query1: TADOLinkedQuery;
          TTabOp: TADOLinkedTable;
          Q: TADOLinkedQuery;
          TTabQuery: TADOLinkedTable;
          TTabLookup: TADOLinkedTable;
          TTabLookupID: TAutoIncField;
          TTabLookupLingua: TStringField;
          RGOpzione: TRadioGroup;
          DBGrid5Note: TdxDBGridColumn;
          BModDati: TToolbarButton97;
          PMModDati: TPopupMenu;
          Modificanoteazienda1: TMenuItem;
          Modificacontatto1: TMenuItem;
          ModiifcaTitolo1: TMenuItem;
          Modificanotecontatto1: TMenuItem;
          DBGrid5eMail: TdxDBGridColumn;
          BitBtn10: TBitBtn;
          DBGrid5IndirizzoCLI: TdxDBGridColumn;
          DBGrid5TipoStradaCLI: TdxDBGridColumn;
          DBGrid5NumCivicoCLI: TdxDBGridColumn;
          DBGrid5CapCLI: TdxDBGridColumn;
          DBGrid5Utente: TdxDBGridColumn;
          DBGrid5DataIns: TdxDBGridColumn;
          DBGrid5CONTATTOTipo: TdxDBGridColumn;
          DBGrid5CONTATTOParlatoCon: TdxDBGridColumn;
          DBGrid5CONTATTOData: TdxDBGridDateColumn;
          DBGrid5CONTATTODescrizione: TdxDBGridBlobColumn;
          DBGrid5DataProssCont: TdxDBGridDateColumn;
          QTabLookup: TADOQuery;
          DSQTabLookup: TDataSource;
          DBGrid5EsitoContatto: TdxDBGridColumn;
          DBGrid5AttivitaFuture: TdxDBGridColumn;
          procedure ComboBox1Change(Sender: TObject);
          procedure BitBtn1Click(Sender: TObject);
          procedure TTabQueryAfterScroll(DataSet: TDataSet);
          procedure BitBtn3Click(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure BitBtn6Click(Sender: TObject);
          procedure BitBtn2Click(Sender: TObject);
          procedure DBGrid4KeyPress(Sender: TObject; var Key: Char);
          procedure TLinee_OLDBeforePost(DataSet: TDataSet);
          procedure BApriFormClick(Sender: TObject);
          procedure BitBtn4Click(Sender: TObject);
          procedure BitBtn5Click(Sender: TObject);
          procedure BitBtn7Click(Sender: TObject);
          procedure BitBtn8Click(Sender: TObject);
          procedure DBGrid5MouseUp(Sender: TObject; Button: TMouseButton;
               Shift: TShiftState; X, Y: Integer);
          procedure Stampagriglia1Click(Sender: TObject);
          procedure EsportainExcel1Click(Sender: TObject);
          procedure EsportainHTML1Click(Sender: TObject);
          procedure BAddTargetListClick(Sender: TObject);
          procedure TLinee_OLDAfterPost(DataSet: TDataSet);
          procedure QRes1BeforeOpen(DataSet: TDataSet);
          procedure Modificanoteazienda1Click(Sender: TObject);
          procedure PMModDatiPopup(Sender: TObject);
          procedure Modificacontatto1Click(Sender: TObject);
          procedure ModiifcaTitolo1Click(Sender: TObject);
          procedure Modificanotecontatto1Click(Sender: TObject);
          procedure BitBtn10Click(Sender: TObject);
     private
          { Private declarations }
          xStringa, xIndexName, xOrdine: string;
          xFormID: integer;
          xggDiffDi, xggDiffUc, xggDiffCol: integer;
          xInvio: integer;
          function CreaFileWorddaRicClienti(xIDModello: integer; xModelloWord: string; xSalva, xStampa: boolean): string;
          //function SpedisciMail: string;
          procedure Save(ADefaultExt, AFilter, AFileName: string; AMethod: TSaveMethod);
     public
          xDaEscludere: array[1..1000] of integer;
          xChiamante, xIDRicerca, xIDClienteRic: integer;
          procedure StoricizzaInvio(xIDContattoCli, xIDTipoInvio: integer; xAbitazione: boolean; xTipo, xNote: string);
     end;

var
     RicClientiForm: TRicClientiForm;

implementation

uses ModuloDati, Main, InsRuolo, SelCompetenza, Comuni,
     SceltaModello, SceltaModelloMailCli, EmailMessage,
     OpzioniEtichette, uutilsvarie, SelPers, NoteCliente, MDRicerche,
     QREtichette2, ModuloDati2, uASAAzienda, Login, CheckPass,
     InputPassword, GestModelliWord;

{$R *.DFM}

//[TONI20021004]DEBUGOK

procedure TRicClientiForm.ComboBox1Change(Sender: TObject);
var MyList: TStrings;
     i: integer;
     xTrovato: boolean;

     //[ALBERTO 20020910]

begin
     BApriForm.Visible := False;
     if not TTabQuery.Active then TTabQuery.Open;
     TTabQuery.Filtered := FALSE;
     TTabQuery.Filter := 'DescTabella=''' + ComboBox1.Text + '''';
     TTabQuery.Filtered := TRUE;
     if TTabQuery.FieldByName('TabLookup').AsString <> '' then begin
          if TTabQuery.FieldByName('FormLookupID').asString <> '' then begin
               BApriForm.Visible := True;
               xFormID := TTabQuery.FieldByName('FormLookupID').AsInteger;
               xIndexName := TrimRight(TTabQuery.FieldByName('TabLookupIdx').AsString);
          end;
          ComboBox2.Visible := False;
          ComboBox2.Enabled := False;
          ComboBox2.Color := clBtnFace;
          Edit1.Visible := False;
          Edit1.Enabled := False;
          Edit1.Color := clBtnFace;

          {TTabLookup.Close;
          if TTabQuery.FieldByName('TabLookupIdx').AsString <> '' then
               //TTabLookup.IndexName:=TrimRight(TTabQuery.FieldByName('TabLookupIdx').AsString)
               TTabLookup.IndexFieldNames := TTabQuery.FieldByName('FieldLookup').AsString
          else TTabLookup.IndexFieldNames := '';

          TTabLookup.TableName := 'dbo.' + TrimRight(TTabQuery.FieldByname('TabLookup').asString);
          TTabLookup.Fields[1].FieldName := 'Pippo';
          TTabLookup.Fields[1].FieldName := TrimRight(TTabQuery.FieldByName('FieldLookup').AsString);
          TTabLookup.Fields[1].Size := TTabQuery.FieldByName('FieldLookupSize').AsINteger;
          DBGrid4.Columns[0].FieldName := TrimRight(TTabQuery.FieldByName('FieldLookup').AsString);
          TTabLookup.Open;}

          QTabLookup.Close;
          if TrimRight(TTabQuery.FieldByname('TabLookup').asString) = 'Users' then
               QTabLookup.SQL.Text := 'select Users.ID,' + TrimRight(TTabQuery.FieldByName('FieldLookup').AsString) +
                    ' from Users join UsersSoftwares on Users.id = UsersSoftwares.IDUtente where IDSoftware=1 order by Users.Nominativo'
          else
               QTabLookup.SQL.Text := 'select ID,' + TrimRight(TTabQuery.FieldByName('FieldLookup').AsString) +
                    ' from ' + TrimRight(TTabQuery.FieldByname('TabLookup').asString) +
                    ' order by ' + TrimRight(TTabQuery.FieldByName('FieldLookup').AsString);
          DBGrid4.DataSource := DSQTabLookup;
          QTabLookup.Open;
          DBGrid4.Columns[0].FieldName := TrimRight(TTabQuery.FieldByName('FieldLookup').AsString);

          DBGrid4.Visible := True;
          DBGrid4.Enabled := True;
          DBGrid4.Color := clWindow;
          xStringa := '';
     end else begin
          DBGrid4.Visible := False;
          DBGrid4.Enabled := False;
          DBGrid4.Color := clBtnFace;
          BApriForm.Visible := False;
          Edit1.Visible := True;
          Edit1.Enabled := true;
          Edit1.Color := clWindow;
     end;
     DBGrid1.Enabled := True;
     DBGrid1.Color := clWindow;
     DBGrid2.Enabled := True;
     DBGrid2.Color := clWindow;
end;

//[TONI20021004]DEBUGOK

procedure TRicClientiForm.BitBtn1Click(Sender: TObject);
var xVal, xValDesc, xOp: string;
     xTipoStringa: boolean;
begin
     if (xInvio > 0) and (TrimRight(TTabQuery.FieldByName('Tabella').asString) = 'ContattoTipoInvio') then begin
          ShowMessage('esiste gi� un invio selezionato');
          exit;
     end;

     DBGrid5.Visible := False;
     LTot.Visible := False;
     if TTabQuery.FieldByName('ValoriPossibili').asString <> '' then begin
          xVal := ComboBox2.text;
          if ComboBox2.text = 's�' then xVal := '1';
          if ComboBox2.text = 'no' then xVal := '0';
          xValDesc := ComboBox2.text;
     end else begin
          xVal := UpperCase(Edit1.text);
          xValDesc := Edit1.Text;
     end;

     xTipoStringa := False;
     if not TTabOp.Active then TTabOp.Open;
     if TrimRIght(TTabOp.FieldByName('Operatore').asString) = 'like' then
          xVal := '''%' + xVal + '%'''
     else
          if not ((TrimRight(TTabQuery.FieldByName('TipoCampo').asString) = 'boolean') or
               (Trimright(TTabQuery.FieldByName('TipoCampo').asString) = 'integer')) then begin
               xVal := '''' + xVal + '''';
               xTipoStringa := True;
          end;

     if TTabQuery.FieldByName('TabLookUp').AsString <> '' then begin
          //xVal := TrimRight(TTabLookUp.FieldByName('ID').asString);
          //xValDesc := TrimRight(TTabLookup.Fields[1].asString);
          xVal := TrimRight(QTabLookup.FieldByName('ID').asString);
          xValDesc := TrimRight(QTabLookup.Fields[1].asString);
     end;

     if xTipoStringa then begin
          if TrimRight(TTabQuery.FieldByName('TipoCampo').asString) = 'date' then
               TLinee.InsertRecord([TrimRight(TTabQuery.FieldByName('DescCampo').asString) + ' ' +
                    TrimRight(TTabOp.FieldByName('DescOperatore').asString) + ' ' +
                         xValDesc,
                         TrimRight(TTabQuery.FieldByName('Tabella').asString) + '.' + TrimRight(TTabQuery.FieldByName('Campo').asString) +
                         TrimRight(TTabOp.FieldByName('Operatore').asString) + ' ' +
                         xVal,
                         TTabQuery.FieldByName('Tabella').asString,
                         'and', 'e',
                         MainForm.xIDUtenteAttuale])
          else
               TLinee.InsertRecord([TrimRight(TTabQuery.FieldByName('DescCampo').asString) + ' ' +
                    TrimRight(TTabOp.FieldByName('DescOperatore').asString) + ' ' +
                         xValDesc,
                         'UPPER(' + TrimRight(TTabQuery.FieldByName('Tabella').asString) + '.' + TrimRight(TTabQuery.FieldByName('Campo').asString) + ') ' +
                         TrimRight(TTabOp.FieldByName('Operatore').asString) + ' ' +
                         xVal,
                         TTabQuery.FieldByName('Tabella').asString,
                         'and', 'e',
                         MainForm.xIDUtenteAttuale]);
     end else
          TLinee.InsertRecord([TrimRight(TTabQuery.FieldByName('DescCampo').asString) + ' ' +
               TrimRight(TTabOp.FieldByName('DescOperatore').asString) + ' ' +
                    xValDesc,
                    TrimRight(TTabQuery.FieldByName('Tabella').asString) + '.' + TrimRight(TTabQuery.FieldByName('Campo').asString) + ' ' +
                    TrimRight(TTabOp.FieldByName('Operatore').asString) + ' ' +
                    xVal,
                    TTabQuery.FieldByName('Tabella').asString,
                    'and', 'e',
                    MainForm.xIDUtenteAttuale]);
     if TrimRight(TTabQuery.FieldByName('Tabella').asString) = 'ContattoTipoInvio' then
          //xInvio := TTabLookup.FieldByName('ID').AsInteger;
          xInvio := QTabLookup.FieldByName('ID').AsInteger;
     TLinee.Close;
     TLinee.Open;
end;

//[TONI20021004]DEBUGOK

procedure TRicClientiForm.TTabQueryAfterScroll(DataSet: TDataSet);
var MyList: TStrings;
     i, xPos: integer;
     xTrovato: boolean;
     xS: string;
begin
     if (TTabQuery.FieldByName('ValoriPossibili').asString <> '') and (TTabQuery.FieldByName('TabLookUp').AsString = '') then begin
          if TrimRight(TTabQuery.FieldByName('TipoCampo').asString) <> 'boolean' then begin
               // riempimento
               ComboBox2.items.Clear;
               xS := TrimRight(TTabQuery.FieldByName('ValoriPossibili').asString);
               while pos(',', xS) > 0 do begin
                    ComboBox2.items.Add(copy(xS, 1, pos(',', xS) - 1));
                    xS := copy(xS, pos(',', xS) + 1, length(xS));
               end;
               ComboBox2.items.Add(xS);
          end;
          ComboBox2.Visible := True;
          ComboBox2.Enabled := True;
          ComboBox2.Color := clWindow;
          DBGrid4.Visible := False;
          BApriForm.Visible := False;
          Edit1.Visible := False;
          Edit1.Enabled := False;
          Edit1.Color := clBtnFace;
     end else begin
          if TTabQuery.FieldByName('TabLookUp').AsString <> '' then begin
               if TTabQuery.FieldByName('FormLookUpID').asString <> '' then begin
                    BApriForm.Visible := True;
                    xFormID := TTabQuery.FieldByName('FormLookupID').AsInteger;
                    xIndexName := TrimRight(TTabQuery.FieldByName('TabLookupIdx').AsString);
               end;
               ComboBox2.Visible := False;
               ComboBox2.Enabled := False;
               ComboBox2.Color := clBtnFace;
               Edit1.Visible := False;
               Edit1.Enabled := False;
               Edit1.Color := clBtnFace;

               {TTabLookup.Close;
               TTabLookup.TableName := 'dbo.' + TrimRight(TTabQuery.FieldByName('TabLookup').AsString);
               if TTabQuery.FieldByName('TabLookupIdx').AsString <> '' then
                    //TTabLookup.IndexName:=TrimRIght(TTabQuery.FieldByName('TabLookupIdx').AsString)
                    TTabLookup.IndexFieldNames := TTabQuery.FieldByName('FieldLookup').AsString
               else TTabLookup.IndexFieldNames := '';
               TTabLookup.Fields[1].FieldName := 'Pippo';
               TTabLookup.Fields[1].FieldName := TrimRight(TTabQuery.FieldByName('FieldLookup').AsString);
               TTabLookup.Fields[1].Size := TTabQuery.FieldByName('FieldLookupSize').AsINteger;
               DBGrid4.Columns[0].FieldName := TrimRight(TTabQuery.FieldByName('FieldLookup').AsString);
               TTabLookup.Open;}

               QTabLookup.Close;
               if TrimRight(TTabQuery.FieldByname('TabLookup').asString) = 'Users' then
                    QTabLookup.SQL.Text := 'select Users.ID,' + TrimRight(TTabQuery.FieldByName('FieldLookup').AsString) +
                         ' from Users join UsersSoftwares on Users.id = UsersSoftwares.IDUtente where IDSoftware=1 order by Users.Nominativo'
               else
                    QTabLookup.SQL.Text := 'select ID,' + TrimRight(TTabQuery.FieldByName('FieldLookup').AsString) +
                         ' from ' + TrimRight(TTabQuery.FieldByname('TabLookup').asString) +
                         ' order by ' + TrimRight(TTabQuery.FieldByName('FieldLookup').AsString);
               DBGrid4.DataSource := DSQTabLookup;
               QTabLookup.Open;
               DBGrid4.Columns[0].FieldName := TrimRight(TTabQuery.FieldByName('FieldLookup').AsString);

               DBGrid4.Visible := True;
               DBGrid4.Enabled := True;
               xStringa := '';
               DBGrid4.Color := clWindow;
          end else begin
               DBGrid4.Visible := False;
               DBGrid4.Enabled := False;
               DBGrid4.Color := clBtnFace;
               BApriForm.Visible := False;
               Edit1.Visible := True;
               Edit1.Enabled := true;
               Edit1.Color := clWindow;
          end;

          ComboBox2.Visible := False;
          ComboBox2.Enabled := False;
          ComboBox2.Color := clBtnFace;
          {        Edit1.Visible:=True;
                  Edit1.Enabled:=true;
                  Edit1.Color:=clWindow;
                  Edit1.text:='';
          }
     end;
end;

procedure TRicClientiForm.BitBtn3Click(Sender: TObject);
begin
     DBGrid5.Visible := False;
     LTot.Visible := False;
     //if TLinee.Eof then exit;
     if TLinee.RecordCount = 0 then exit;
     if TLinee.FieldByName('Tabella').AsString = 'ContattoTipoInvio' then xInvio := 0;
     TLinee.Delete;
end;

//[TONI20021004]DEBUGOK

procedure TRicClientiForm.FormShow(Sender: TObject);
var i: integer;
begin
     //Grafica
     RicClientiForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel3.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel3.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel3.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel10.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel10.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel10.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     PanRic.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     PanRic.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     PanRic.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     Caption := '[M/171] - ' + Caption;
     if (pos('ADVANT', Uppercase(Data.Global.FieldByName('NomeAzienda').Value)) > 0) or
          (pos('MCS', Uppercase(Data.Global.FieldByName('NomeAzienda').Value)) > 0) then
          BitBtn10.Visible := true;

     xOrdine := 'order by EBC_Clienti.Descrizione';
     Edit1.Enabled := False;
     Edit1.Color := clBtnFace;
     xInvio := 0;
     TLInee.ReloadSQL;
     TLinee.ParamByName['xIDAnag'] := MainForm.xIDUtenteAttuale;
     TLinee.Open;
     TTabOp.Open;
     TTabQuery.Open;
     while not TLinee.EOF do begin
          if TLinee.FieldByName('Tabella').AsString = 'ContattoTipoInvio' then begin
               TLinee.Delete;
               break;
          end;
          TLinee.Next;
     end;
end;

procedure TRicClientiForm.BitBtn6Click(Sender: TObject);
begin
     DBGrid5.Visible := False;
     LTot.Visible := False;
     Query1.Close;
     Query1.SQl.Clear;
     Query1.SQl.add('delete from CliLineeQuery where IDAnagrafica=' + IntToStr(MainForm.xIDUtenteAttuale));
     Query1.ExecSQL;
     TTabQuery.Refresh;
     TLinee.Close;
     TLinee.Open;
     xInvio := 0;
end;

procedure TRicClientiForm.BitBtn2Click(Sender: TObject);
var xTab: array[1..5] of string;
     i, j: integer;
     xS, xS1, xd, xLineeStringa: string;
     xApertaPar, xEspLav: boolean;
begin
     //
     if DsLinee.State in [dsInsert, dsEdit] then TLinee.Post;
     TLinee.Last;
     if TLinee.FieldByName('OpSucc').AsString <> '' then begin
          TLinee.Edit;
          TLinee.FieldByName('OpNext').AsString := 'fine';
          TLinee.Post;
     end;
     // tabelle implicate
     QTabelleLista.Close;
     QTabelleLista.SQL.clear;
     QTabelleLista.SQL.add('select distinct Tabella from CliLineeQuery ');
     QTabelleLista.SQL.add('where Tabella<>''EBC_Clienti'' and Tabella<>''EBC_ContattiClienti'' and IDAnagrafica=' + IntToStr(MainForm.xIDUtenteAttuale));
     QTabelleLista.Open;
     QTabelleLista.First;
     for i := 1 to 5 do xTab[i] := '';
     i := 1;
     while not QTabelleLista.EOF do begin
          xTab[i] := TrimRight(QTabellelista.FieldByName('Tabella').AsString);
          QTabelleLista.Next; inc(i);
     end;

     xEspLav := False;
     {if (RGOpzione.ItemIndex = 1) or (RGOpzione.ItemIndex = 2) then
          xS := 'from EBC_Clienti'
     else xS := 'from EBC_Clienti,EBC_ContattiClienti,EBC_ContattiFatti,Users,ClienteLinkUtenti '; //*********
     }
     {case rgOpzione.ItemIndex of
          0: begin
                    xS := 'from EBC_Clienti,EBC_ContattiClienti,EBC_ContattiFatti';
               end;
          1, 2: begin
                    xS := 'from EBC_Clienti';
               end;
          3: begin
                    xS := 'from EBC_Clienti,EBC_ContattiClienti,EBC_ContattiFatti,Users,ClienteLinkUtenti ';//*********
               end;
     end;
     for i := 1 to 5 do begin
          if xTab[i] <> '' then
               xS := xs + ',' + xTab[i];
     end;

     QRes1.Close;
     QRes1.SQL.Clear;
     case rgOpzione.ItemIndex of
          0: begin
                    if (pos('ADVANT', Uppercase(Data.Global.FieldByName('NomeAzienda').Value)) > 0) or (pos('MCS', Uppercase(Data.Global.FieldByName('NomeAzienda').Value)) > 0) then
                         QRes1.SQL.Add('select distinct EBC_Clienti.ID IDCliente, EBC_Clienti.Descrizione Cliente,EBC_Clienti.Stato,Comune,Provincia,Sesso, ' +
                              'EBC_ContattiClienti.Contatto, EBC_Clienti.Telefono, EBC_ContattiClienti.ID IDContatto, EBC_ContattiFatti.Data DataUltimoCont, EBC_ContattiFatti.DataProssimoContatto DataProssCont, ' +
                              'EBC_Clienti.Indirizzo,EBC_Clienti.Cap,EBC_ContattiClienti.Fax,EBC_ContattiClienti.email,NazioneAbb, CaricaAziendale, DivisioneAziendale, ' +
                              'EtichettaAbitazione,IndirizzoPrivato,CapPrivato,ProvinciaPrivato,Titolo,EBC_ContattiClienti.Stato Paese,ComunePrivato, ' +
                              'TipoStradaPrivato,NumCivicoPrivato,EBC_Clienti.TipoStrada,EBC_Clienti.NumCivico,CAST(EBC_ContattiClienti.Note AS CHAR(50)) Note')
                              //'Users.Nominativo Utente, EBC_ContattiClienti.DataIns ')
                    else
                         QRes1.SQL.Add('select distinct EBC_Clienti.ID IDCliente, EBC_Clienti.Descrizione Cliente,EBC_Clienti.Stato,Comune,Provincia,Sesso, ' +
                              'EBC_ContattiClienti.Contatto, EBC_ContattiClienti.Telefono, EBC_ContattiClienti.ID IDContatto, EBC_ContattiFatti.Data DataUltimoCont, EBC_ContattiFatti.DataProssimoContatto DataProssCont, ' +
                              'EBC_Clienti.Indirizzo,EBC_Clienti.Cap,EBC_ContattiClienti.Fax,EBC_ContattiClienti.email,NazioneAbb, CaricaAziendale, DivisioneAziendale, ' +
                              'EtichettaAbitazione,IndirizzoPrivato,CapPrivato,ProvinciaPrivato,Titolo,EBC_ContattiClienti.Stato Paese,ComunePrivato, ' +
                              'TipoStradaPrivato,NumCivicoPrivato,EBC_Clienti.TipoStrada,EBC_Clienti.NumCivico,CAST(EBC_ContattiClienti.Note AS CHAR(50)) Note ');
                              //'Users.Nominativo Utente, EBC_ContattiClienti.DataIns ');
               end;
          1, 2: begin
                    if (pos('ADVANT', Uppercase(Data.Global.FieldByName('NomeAzienda').Value)) > 0) or (pos('MCS', Uppercase(Data.Global.FieldByName('NomeAzienda').Value)) > 0) then
                         QRes1.SQL.Add('select distinct EBC_Clienti.ID IDCliente, EBC_Clienti.Descrizione Cliente,EBC_Clienti.Stato,Comune,Provincia, ' +
                              'EBC_Clienti.Indirizzo,EBC_Clienti.Cap, EBC_Clienti.TipoStrada,EBC_Clienti.NumCivico,EBC_Clienti.Telefono ')
                    else
                         QRes1.SQL.Add('select distinct EBC_Clienti.ID IDCliente, EBC_Clienti.Descrizione Cliente,EBC_Clienti.Stato,Comune,Provincia, ' +
                              'EBC_Clienti.Indirizzo,EBC_Clienti.Cap, EBC_Clienti.TipoStrada,EBC_Clienti.NumCivico,EBC_Clienti.DataIns,EBC_Clienti.UtenteIns Utente, EBC_Clienti.Telefono');
               end;
          3: begin
                    QRes1.SQl.Add('select distinct ebc_contattifatti.*,EBC_Clienti.ID IDCliente, EBC_Clienti.Descrizione Cliente,EBC_Clienti.Stato,Comune,Provincia,Sesso,  ' +
                         'EBC_ContattiClienti.Contatto, EBC_ContattiClienti.Telefono, EBC_ContattiClienti.ID IDContatto, ' +
                         'EBC_Clienti.Indirizzo,EBC_Clienti.Cap,EBC_ContattiClienti.Fax,EBC_ContattiClienti.email,NazioneAbb, CaricaAziendale, DivisioneAziendale, ' +
                         'EtichettaAbitazione,IndirizzoPrivato,CapPrivato,ProvinciaPrivato,Titolo,EBC_ContattiClienti.Stato Paese,ComunePrivato, ' +
                         'TipoStradaPrivato,NumCivicoPrivato,EBC_Clienti.TipoStrada,EBC_Clienti.NumCivico,CAST(EBC_ContattiClienti.Note AS CHAR(50)) Note, ' +
                         'Users.Nominativo Utente, EBC_ContattiClienti.DataIns ');
               end;
     end;}

     {if (RGOpzione.ItemIndex = 1) or (RGOpzione.ItemIndex = 2) then begin
          if (pos('ADVANT', Uppercase(Data.Global.FieldByName('NomeAzienda').Value)) > 0) or (pos('MCS', Uppercase(Data.Global.FieldByName('NomeAzienda').Value)) > 0) then
               QRes1.SQL.Add('select distinct EBC_Clienti.ID IDCliente, EBC_Clienti.Descrizione Cliente,EBC_Clienti.Stato,Comune,Provincia, ' +
                    'EBC_Clienti.Indirizzo,EBC_Clienti.Cap, EBC_Clienti.TipoStrada,EBC_Clienti.NumCivico,EBC_Clienti.Telefono ')
          else
               QRes1.SQL.Add('select distinct EBC_Clienti.ID IDCliente, EBC_Clienti.Descrizione Cliente,EBC_Clienti.Stato,Comune,Provincia, ' +
                    'EBC_Clienti.Indirizzo,EBC_Clienti.Cap, EBC_Clienti.TipoStrada,EBC_Clienti.NumCivico,EBC_Clienti.DataIns,EBC_Clienti.UtenteIns Utente, EBC_Clienti.Telefono');
     end else begin
          if (pos('ADVANT', Uppercase(Data.Global.FieldByName('NomeAzienda').Value)) > 0) or (pos('MCS', Uppercase(Data.Global.FieldByName('NomeAzienda').Value)) > 0) then
               QRes1.SQL.Add('select distinct EBC_Clienti.ID IDCliente, EBC_Clienti.Descrizione Cliente,EBC_Clienti.Stato,Comune,Provincia,Sesso, ' +
                    'EBC_ContattiClienti.Contatto, EBC_Clienti.Telefono, EBC_ContattiClienti.ID IDContatto, EBC_ContattiFatti.Data DataUltimoCont, ' +
                    'EBC_Clienti.Indirizzo,EBC_Clienti.Cap,EBC_ContattiClienti.Fax,EBC_ContattiClienti.email,NazioneAbb, CaricaAziendale, DivisioneAziendale, ' +
                    'EtichettaAbitazione,IndirizzoPrivato,CapPrivato,ProvinciaPrivato,Titolo,EBC_ContattiClienti.Stato Paese,ComunePrivato, ' +
                    'TipoStradaPrivato,NumCivicoPrivato,EBC_Clienti.TipoStrada,EBC_Clienti.NumCivico,CAST(EBC_ContattiClienti.Note AS CHAR(50)) Note, ' +
                    'Users.Nominativo Utente, EBC_ContattiClienti.DataIns ')
          else
               QRes1.SQL.Add('select distinct EBC_Clienti.ID IDCliente, EBC_Clienti.Descrizione Cliente,EBC_Clienti.Stato,Comune,Provincia,Sesso, ' +
                    'EBC_ContattiClienti.Contatto, EBC_ContattiClienti.Telefono, EBC_ContattiClienti.ID IDContatto, EBC_ContattiFatti.Data DataUltimoCont, ' +
                    'EBC_Clienti.Indirizzo,EBC_Clienti.Cap,EBC_ContattiClienti.Fax,EBC_ContattiClienti.email,NazioneAbb, CaricaAziendale, DivisioneAziendale, ' +
                    'EtichettaAbitazione,IndirizzoPrivato,CapPrivato,ProvinciaPrivato,Titolo,EBC_ContattiClienti.Stato Paese,ComunePrivato, ' +
                    'TipoStradaPrivato,NumCivicoPrivato,EBC_Clienti.TipoStrada,EBC_Clienti.NumCivico,CAST(EBC_ContattiClienti.Note AS CHAR(50)) Note, ' +
                    'Users.Nominativo Utente, EBC_ContattiClienti.DataIns ');
     end;    }
     //QRes1.SQL.Add(xS + ' ');
    { if (RGOpzione.ItemIndex = 1) or (RGOpzione.ItemIndex = 2) then begin
          QRes1.SQL.Add('where EBC_Clienti.ID is not null');
     end else begin
          QRes1.SQL.Add('where EBC_Clienti.ID = EBC_ContattiClienti.IDCliente');
          QRes1.SQL.Add('and EBC_Clienti.ID *= EBC_ContattiFatti.IDCLiente');
          QRes1.SQL.Add('and EBC_ContattiClienti.IDUtente *= Users.ID ');
          QRes1.SQL.Add('and EBC_ContattiFatti.Data = (select max(Data) from EBC_ContattiFatti ');
          QRes1.SQL.Add('                              where EBC_Clienti.ID *= EBC_ContattiFatti.IDCLiente)');
     end;  }

     {case rgOpzione.ItemIndex of
          0: begin
                    QRes1.SQL.Add('where EBC_Clienti.ID = EBC_ContattiClienti.IDCliente');
                    QRes1.SQL.Add('and EBC_Clienti.ID *= EBC_ContattiFatti.IDCLiente');
                    QRes1.SQL.Add('and EBC_ContattiFatti.Data = (select max(Data) from EBC_ContattiFatti ');
                    QRes1.SQL.Add('                              where EBC_Clienti.ID *= EBC_ContattiFatti.IDCLiente)');
               end;
          1, 2: begin
                    QRes1.SQL.Add('where EBC_Clienti.ID is not null');
               end;
          3: begin
                    QRes1.SQL.Add('where EBC_Clienti.ID = EBC_ContattiClienti.IDCliente');
                    QRes1.SQL.Add('and EBC_Clienti.ID = EBC_ContattiFatti.IDCLiente');
                    QRes1.SQL.Add('and EBC_ContattiClienti.IDUtente *= Users.ID');
                    QRes1.SQL.Add('and ClienteLinkUtenti.idcliente=ebc_clienti.id');
                    QRes1.SQL.Add('and EBC_ContattiFatti.IDContattocli = ebc_contatticlienti.id');
               end;

     end; }
     {xS1 := '';
     for i := 1 to 5 do begin }
          {if RGOpzione.ItemIndex = 0 then begin
               if xTab[i] <> '' then begin
                    if xTab[i] = 'ContattoTipoInvio' then
                         xS1 := xS1 + ' and EBC_ContattiClienti.ID=' + xTab[i] + '.IDContattocli'
                    else xS1 := xS1 + ' and EBC_Clienti.ID=' + xTab[i] + '.IDCliente ';
               end;
          end else begin
               if xTab[i] = 'EBC_Clienti' then
                    xS1 := xS1 + ' and EBC_Clienti.ID=' + xTab[i] + '.IDCliente';
          end; }

          {case rgOpzione.ItemIndex of
               0: begin
                         if xTab[i] <> '' then begin
                              if xTab[i] = 'ContattoTipoInvio' then
                                   xS1 := xS1 + ' and EBC_ContattiClienti.ID=' + xTab[i] + '.IDContattocli'
                              else xS1 := xS1 + ' and EBC_Clienti.ID=' + xTab[i] + '.IDCliente ';
                         end;
                    end;
               1, 2, 3: begin
                         if xTab[i] <> '' then begin }
                              {if xTab[i] = 'EBC_Clienti' then
                                   xS1 := xS1 + ' and EBC_Clienti.ID=' + xTab[i] + '.IDCliente';}
                             { xS1 := xS1 + ' and EBC_Clienti.ID=' + xTab[i] + '.IDCliente ';
                         end;
                    end;
          end;
     end;}
     {case rgopzione.ItemIndex of
          0, 1: begin
                    xS1 := xS1 + ' and EBC_Clienti.IDTipoAzienda<>4'
               end;
          2: begin
                    xS1 := xS1 + ' and EBC_Clienti.IDTipoAzienda=4';
               end;
     end;}
    { if (RGOpzione.ItemIndex = 0) or (RGOpzione.ItemIndex = 1) then
          xS1 := xS1 + ' and EBC_Clienti.IDTipoAzienda<>4'
     else xS1 := xS1 + ' and EBC_Clienti.IDTipoAzienda=4';    }
     QRes1.Close;
     QRes1.SQL.Clear;
     case rgopzione.ItemIndex of
          0: begin
                    if (pos('ADVANT', Uppercase(Data.Global.FieldByName('NomeAzienda').Value)) > 0) or (pos('MCS', Uppercase(Data.Global.FieldByName('NomeAzienda').Value)) > 0) then
                         QRes1.SQL.Add('select distinct EBC_Clienti.ID IDCliente, EBC_Clienti.Descrizione Cliente,EBC_Clienti.Stato,Comune,Provincia,Sesso, ' +
                              'EBC_ContattiClienti.Contatto, EBC_Clienti.Telefono, EBC_ContattiClienti.ID IDContatto, EBC_ContattiFatti.Data DataUltimoCont, EBC_ContattiFatti.DataProssimoContatto DataProssCont, ' +
                              'EBC_Clienti.Indirizzo,EBC_Clienti.Cap,EBC_ContattiClienti.Fax,EBC_ContattiClienti.email,NazioneAbb StatoNaz, CaricaAziendale, DivisioneAziendale, ' +
                              'EtichettaAbitazione,IndirizzoPrivato,CapPrivato,ProvinciaPrivato,Titolo,EBC_ContattiClienti.Stato Paese,ComunePrivato, ' +
                              'TipoStradaPrivato,NumCivicoPrivato,EBC_Clienti.TipoStrada,EBC_Clienti.NumCivico,CAST(EBC_ContattiClienti.Note AS CHAR(50)) Note,EBC_EsitoContatti.Descrizione EsitoContatto,EBC_ContattiAttivitaFuture.Descrizione AttivitaFuture')
                              //'Users.Nominativo Utente, EBC_ContattiClienti.DataIns ')
                    else
                         QRes1.SQL.Add('select distinct EBC_Clienti.ID IDCliente, EBC_Clienti.Descrizione Cliente,EBC_Clienti.Stato,Comune,Provincia,Sesso, ' +
                              'EBC_ContattiClienti.Contatto, EBC_ContattiClienti.Telefono, EBC_ContattiClienti.ID IDContatto, EBC_ContattiFatti.Data DataUltimoCont, EBC_ContattiFatti.DataProssimoContatto DataProssCont, ' +
                              'EBC_Clienti.Indirizzo,EBC_Clienti.Cap,EBC_ContattiClienti.Fax,EBC_ContattiClienti.email,NazioneAbb StatoNaz, CaricaAziendale, DivisioneAziendale, ' +
                              'EtichettaAbitazione,IndirizzoPrivato,CapPrivato,ProvinciaPrivato,Titolo,EBC_ContattiClienti.Stato Paese,ComunePrivato, ' +
                              'TipoStradaPrivato,NumCivicoPrivato,EBC_Clienti.TipoStrada,EBC_Clienti.NumCivico,CAST(EBC_ContattiClienti.Note AS CHAR(50)) Note,EBC_EsitoContatti.Descrizione EsitoContatto,EBC_ContattiAttivitaFuture.Descrizione AttivitaFuture ');
                    //'Users.Nominativo Utente, EBC_ContattiClienti.DataIns ');
                    xS := ' from EBC_Clienti join EBC_ContattiClienti on EBC_Clienti.ID = EBC_ContattiClienti.IDCliente' +

                         ' left join EBC_ContattiFatti on EBC_Clienti.ID = EBC_ContattiFatti.IDCLiente' +
                         ' and EBC_ContattiFatti.IDContattoCLi=EBC_ContattiClienti.ID ' + //ALE aggiunto in data 12/07/2018
                         ' and EBC_ContattiFatti.Data = (select max(Data)' +
                         '					from EBC_Clienti A left join EBC_ContattiFatti' +
                         '					on A.ID = EBC_ContattiFatti.IDCliente' +
                         '                    where A.ID = EBC_Clienti.ID)'+
                         ' left join EBC_EsitoContatti' +
                         ' on EBC_EsitoContatti.id=EBC_ContattiFatti.IDEsitoContatto' +
                         ' left join EBC_ContattiAttivitaFuture' +
                         ' on EBC_ContattiAttivitaFuture.id=EBC_ContattiFatti.IDContattoAttivitaFutura' ;
               end;
          1, 2: begin
                    if (pos('ADVANT', Uppercase(Data.Global.FieldByName('NomeAzienda').Value)) > 0) or (pos('MCS', Uppercase(Data.Global.FieldByName('NomeAzienda').Value)) > 0) then
                         QRes1.SQL.Add('select distinct EBC_Clienti.ID IDCliente, EBC_Clienti.Descrizione Cliente,EBC_Clienti.Stato,Comune,Provincia, ' +
                              'EBC_Clienti.Indirizzo,EBC_Clienti.Cap, EBC_Clienti.TipoStrada,EBC_Clienti.NumCivico, NazioneAbb StatoNaz,EBC_Clienti.Telefono ')
                    else
                         QRes1.SQL.Add('select distinct EBC_Clienti.ID IDCliente, EBC_Clienti.Descrizione Cliente,EBC_Clienti.Stato,Comune,Provincia, ' +
                              'EBC_Clienti.Indirizzo,EBC_Clienti.Cap, EBC_Clienti.TipoStrada,EBC_Clienti.NumCivico, NazioneAbb StatoNaz,EBC_Clienti.DataIns,EBC_Clienti.UtenteIns Utente, EBC_Clienti.Telefono');

//                              QRes1.SQL.Add('select distinct EBC_Clienti.ID IDCliente, EBC_Clienti.Descrizione Cliente,EBC_Clienti.Stato,Comune,Provincia,Sesso, EBC_ContattiClienti.Contatto, ' +
//                              'EBC_ContattiClienti.Telefono, EBC_ContattiClienti.ID IDContatto, EBC_Clienti.Indirizzo,EBC_Clienti.Cap,EBC_ContattiClienti.Fax,EBC_ContattiClienti.email,NazioneAbb StatoNaz, ' +
//                              'CaricaAziendale, DivisioneAziendale, EtichettaAbitazione,IndirizzoPrivato,CapPrivato,ProvinciaPrivato,Titolo,EBC_ContattiClienti.Stato Paese,ComunePrivato, TipoStradaPrivato, ' +
//                              'NumCivicoPrivato,EBC_Clienti.TipoStrada,EBC_Clienti.NumCivico,CAST(EBC_ContattiClienti.Note AS CHAR(50)) Note');

                    //xS := ' from EBC_Clienti';
                    xS := ' from EBC_Clienti left join EBC_ContattiClienti on EBC_Clienti.ID = EBC_ContattiClienti.IDCliente';
                    //' where EBC_Clienti.ID is not null';
               end;
          3: begin
                    QRes1.SQl.Add('select distinct ebc_contattifatti.*,EBC_Clienti.ID IDCliente, EBC_Clienti.Descrizione Cliente,EBC_Clienti.Stato,Comune,Provincia,Sesso,  ' +
                         'EBC_ContattiClienti.Contatto, EBC_ContattiClienti.Telefono, EBC_ContattiClienti.ID IDContatto, ' +
                         'EBC_Clienti.Indirizzo,EBC_Clienti.Cap,EBC_ContattiClienti.Fax,EBC_ContattiClienti.email,NazioneAbb StatoNaz, CaricaAziendale, DivisioneAziendale, ' +
                         'EtichettaAbitazione,IndirizzoPrivato,CapPrivato,ProvinciaPrivato,Titolo,EBC_ContattiClienti.Stato Paese,ComunePrivato, ' +
                         'TipoStradaPrivato,NumCivicoPrivato,EBC_Clienti.TipoStrada,EBC_Clienti.NumCivico,CAST(EBC_ContattiClienti.Note AS CHAR(50)) Note, ' +
                         'Users.Nominativo Utente, EBC_ContattiClienti.DataIns,EBC_EsitoContatti.Descrizione EsitoContatto,EBC_ContattiAttivitaFuture.Descrizione AttivitaFuture ');
                    xS := ' from EBC_Clienti join EBC_ContattiClienti on EBC_Clienti.ID = EBC_ContattiClienti.IDCliente' +
                         ' join EBC_ContattiFatti on EBC_Clienti.ID = EBC_ContattiFatti.IDCLiente' +

                         ' left join Users on EBC_ContattiFatti.IDUtente = Users.ID'+
                         ' left join EBC_EsitoContatti' +
                         ' on EBC_EsitoContatti.id=EBC_ContattiFatti.IDEsitoContatto' +
                         ' left join EBC_ContattiAttivitaFuture' +
                         ' on EBC_ContattiAttivitaFuture.id=EBC_ContattiFatti.IDContattoAttivitaFutura' ;
               end;
     end;
     QRes1.SQL.Add(xS + ' ');
     xS1 := '';
     for i := 1 to 5 do begin
          case rgOpzione.ItemIndex of
               0: begin
                         if xTab[i] <> '' then begin
                              if xTab[i] = 'ContattoTipoInvio' then
                                   xS1 := xS1 + ' join ' + xTab[i] + ' on EBC_ContattiClienti.ID=' + xTab[i] + '.IDContattocli'
                              else xS1 := xS1 + ' join ' + xTab[i] + ' on EBC_Clienti.ID=' + xTab[i] + '.IDCliente';
                         end;
                    end;
               1, 2, 3: begin
                         if xTab[i] <> '' then begin
                              if xTab[i] = 'ContattoTipoInvio' then
                                   xS1 := xS1 + ' join ' + xTab[i] + ' on EBC_ContattiClienti.ID=' + xTab[i] + '.IDContattocli'
                              else xS1 := xS1 + ' join ' + xTab[i] + ' on EBC_Clienti.ID=' + xTab[i] + '.IDCliente';
                         end;
                    end;
          end;
     end;
     case rgopzione.ItemIndex of
          1, 2: begin
                    xS1 := xS1 + ' where EBC_Clienti.ID is not null';
               end;
          3: begin
                    xS1 := xS1 + ' where EBC_ContattiFatti.IDContattocli = ebc_contatticlienti.id';
               end;
     end;
     case rgopzione.ItemIndex of
          0: begin
                    xS1 := xS1 + ' where EBC_Clienti.IDTipoAzienda<>4'
               end;
          1: begin
                    xS1 := xS1 + ' and EBC_Clienti.IDTipoAzienda<>4'
               end;
          2: begin
                    xS1 := xS1 + ' and EBC_Clienti.IDTipoAzienda=4';
               end;
     end;
     QRes1.SQL.Add(xS1);
     if TLinee.RecordCount > 0 then
          QRes1.SQL.Add('and');

     TLinee.First;
     xLineeStringa := '';
     xApertaPar := False;
     while not TLinee.EOF do begin
          {if RGOpzione.ItemIndex = 0 then
               xLineeStringa := TLinee.FieldByName('Stringa').asString
          else begin
               //if TrimRight(TLinee.FieldByName('Tabella').asString) = 'EBC_Clienti' then   //TRimmato ALE 18/03/2004
               xLineeStringa := TLinee.FieldByName('Stringa').asString
          end;}
          xLineeStringa := TLinee.FieldByName('Stringa').asString;

          TLinee.Next;
          if TLinee.EOF then
               // ultimo record -> senza AND alla fine
               if xApertaPar then QRes1.SQL.Add(' ' + xLineeStringa + ')')
               else QRes1.SQL.Add(' ' + xLineeStringa)
          else begin
               TLinee.Prior;
               if TrimRight(TLinee.FieldByName('OpSucc').AsString) = 'and' then
                    if not xApertaPar then
                         QRes1.SQL.Add(' ' + xLineeStringa + ' and ')
                    else begin
                         QRes1.SQL.Add(' ' + xLineeStringa + ') and ');
                         xApertaPar := False;
                         // Showmessage(QRes1.SQL.GetText);
                    end;
               if TrimRight(TLinee.FieldByName('OpSucc').AsString) = 'or' then
                    if not xApertaPar then begin
                         QRes1.SQL.Add(' (' + xLineeStringa + ' or ');
                         xApertaPar := True;
                    end else begin
                         QRes1.SQL.Add(' ' + xLineeStringa + ' or ');
                         xApertaPar := True;
                    end;
          end;
          //Showmessage(QRes1.SQL.GetText);
          TLinee.Next;
     end;

     QRes1.SQL.Add(xOrdine);
     //QRes1.SQL.SaveToFile('QRes1.sql');
     ScriviRegistry('QRes1Clienti', QRes1.SQL.text);

     //QRes1.ExecSQL;
     QRes1.Open;
     DBGrid5.Visible := True;
     // totale
     LTot.Caption := IntToStr(QRes1.RecordCount);
     LTot.Visible := True;
end;

procedure TRicClientiForm.DBGrid4KeyPress(Sender: TObject; var Key: Char);
begin
     if TTabLookup.IndexName <> '' then begin
          if Key <> chr(13) then begin
               xStringa := xStringa + key;
               //               TTabLookup.FindNearest([xStringa]);
               TTabLookup.FindNearestADO(VarArrayOf([xStringa]));
          end;
     end;
end;

procedure TRicClientiForm.TLinee_OLDBeforePost(DataSet: TDataSet);
begin
     if TLinee.FieldByName('OpNext').AsString = 'e' then TLinee.FieldByName('OpSucc').AsString := 'and';
     if TLinee.FieldByName('OpNext').AsString = 'o' then TLinee.FieldByName('OpSucc').AsString := 'or';
     if TLinee.FieldByName('OpNext').AsString = 'fine' then TLinee.FieldByName('OpSucc').AsString := '';
end;

procedure TRicClientiForm.BApriFormClick(Sender: TObject);
begin
     case xFormID of
          1: begin
                    //
               end;
     end;
end;

procedure TRicClientiForm.BitBtn4Click(Sender: TObject);
begin
     Close;
end;

//[TONI20021004]DEBUGOK

procedure TRicClientiForm.BitBtn5Click(Sender: TObject);
var xFile, xDesc: string;
     xSalva, xStampa: boolean;
     k: integer;
     xfilesave: TADOQuery;
begin
     if (not (QRes1.active)) or (QRes1.RecordCount = 0) then exit;
     SceltaModelloForm := TSceltaModelloForm.create(self);
     SceltaModelloForm.QModelliWord.SQL.text := 'select * from ModelliWord ' +
          'where TabellaMaster=:xTabMaster: and H1Sel>0';
     SceltaModelloForm.QModelliWord.ParamByName['xTabMaster'] := '*RicClienti';
     SceltaModelloForm.QModelliWord.Open;
     SceltaModelloForm.CBSalva.Checked := true;
     //Storicizzazione in StoricoInviiContatto - Da verificare utilit�..
     {if xInvio > 0 then begin
          SceltaModelloForm.Height := 350;
          // posizionati sul modello word (--> SE ASSOCIATO AL TIPO DI INVIO)
          Data.Q1.Close;
          Data.Q1.SQL.text := 'select IDModelloWord from TipiInviiContatti where ID=' + IntTostr(xInvio);
          Data.Q1.Open;
          if Data.Q1.FieldByName('IDModelloWord').asString <> '' then
               while (not SceltaModelloForm.QModelliWord.Eof) and (SceltaModelloForm.QModelliWord.FieldByName('ID').AsInteger <> Data.Q1.FieldByName('IDModelloWord').asInteger) do
                    SceltaModelloForm.QModelliWord.Next;
          Data.Q1.Close;
          if SceltaModelloForm.CBStoricizza.Checked then
                    // storicizzazione (se impostata)
               StoricizzaInvio(QRes1.FieldByName('IDContatto').Value,
                    SceltaModelloForm.QModelliWord.FieldByName('ID').AsInteger,
                    QRes1.FieldByName('EtichettaAbitazione').AsBoolean, //ALBERTO:  bit: ASInteger?
                    'W',
                    SceltaModelloForm.QModelliWord.FieldByName('Descrizione').AsString);
     end;}
     SceltaModelloForm.ShowModal;
     if SceltaModelloForm.ModalResult = mrOK then begin
          if (SceltaModelloForm.RGOpzione.ItemIndex = 0) or (SceltaModelloForm.RGOpzione.ItemIndex = 2) then xStampa := True
          else xStampa := False;
          xSalva := SceltaModelloForm.CBSalva.Checked;
          if SceltaModelloForm.RGOpzione.ItemIndex = 0 then begin
               // PER tutti quelli selezionati
               for k := 0 to DBGrid5.SelectedCount - 1 do begin
                    QRes1.BookMark := DBGrid5.SelectedRows[k];
                    // crea file Word (con opzioni stampa/salva)
                    xFile := MainForm.CreaFileWorddaClienti(SceltaModelloForm.QModelliWord.FieldByName('ID').AsInteger,
                         SceltaModelloForm.QModelliWord.FieldByName('NomeModello').AsString,
                         xSalva, xStampa, '*RicClienti');
               end;
          end else
               // SINGOLO SELEZIONATO
               {xFile := Mainform.CreaFileWorddaRicClienti(SceltaModelloForm.QModelliWord.FieldByName('ID').AsInteger,
                    SceltaModelloForm.QModelliWord.FieldByName('NomeModello').AsString,
                    xSalva, xStampa);}
               xFile := Mainform.CreaFileWorddaClienti(SceltaModelloForm.QModelliWord.FieldByName('ID').AsInteger,
                    SceltaModelloForm.QModelliWord.FieldByName('NomeModello').AsString,
                    xSalva, xStampa, '*RicClienti');
          xDesc := SceltaModelloForm.QModelliWord.FieldByName('Descrizione').AsString;
          // salvataggio file in CliFileWord - DA VERIFICARE NECESSITA' e se va bene qui !!
          if (xFile <> '') and (xSalva) then begin
               with Data do begin
                    DB.BeginTrans;
                    try
                         Q1.Close;
                         Q1.SQL.Text := 'insert into CliFileWord (IDCliente,IDModelloWord,Descrizione,NomeFile,Data) ' +
                              'values (:xIDCliente:,:xIDModelloWord:,:xDescrizione:,:xNomeFile:,:xData:)';
                         Q1.ParamByName['xIDCliente'] := QRes1.FieldByName('IDCliente').AsInteger;
                         Q1.ParamByName['xIDModelloWord'] := SceltaModelloForm.QModelliWord.FieldByName('ID').AsInteger;
                         Q1.ParamByName['xDescrizione'] := xDesc;
                         Q1.ParamByName['xNomeFile'] := xFile;
                         Q1.ParamByName['xData'] := Date;
                         Q1.ExecSql;
                         //Associa al Cliente..
                         Q1.Close;
                         Q1.SQL.text := 'insert into EBC_ClientiFile (IDCliente,IDTipo,Descrizione,DataCreazione,NomeFile) ' +
                              'values (:xIDCliente:,0,:xDescrizione:,:xDataCreazione:,:xNomeFile:)';
                         Q1.ParambyName['xIDCliente'] := QRes1.FieldByName('IDCliente').AsInteger;
                         Q1.ParambyName['xDescrizione'] := xDesc;
                         Q1.ParambyName['xDataCreazione'] := Date;
                         Q1.ParambyName['xNomeFile'] := xFile;
                         Q1.ExecSQL;
                         Data2.QClienteFile.Close;
                         Data2.QClienteFile.Open;
                         DB.CommitTrans;
                    except
                         DB.RollbackTrans;
                         MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                    end;
               end;
          end;
     end;
     SceltaModelloForm.Free;
end;

function TRicClientiForm.CreaFileWorddaRicClienti(xIDModello: integer; xModelloWord: string; xSalva, xStampa: boolean): string;
var xFileModello, xFileWord, xcampo: string;
     xAnno, xMese, xGiorno: Word;
     MSWord: Variant;
     xApriWord, xNuovoFile: boolean;
begin
     if not FileExists(GetDocPath + '\' + xModelloWord + '.doc') then begin
          MessageDlg('Il modello non esiste o la cartella non � impostata correttamente' + chr(13) +
               'IMPOSSIBILE PROSEGUIRE', mtError, [mbOK], 0);
          Result := '';
          exit;
     end;
     with Data.QTemp do begin
          Close;
          SQL.text := 'select * from ModelliWord where ID=' + IntToStr(xIDModello);
          Open;
          DecodeDate(Date, xAnno, xMese, xGiorno);
          xFileWord := GetDocPath + '\' +
               FieldbyName('InizialiFileGenerato').asString + IntToStr(QRes1.FieldByName('IDCliente').AsInteger) +
               '-' + IntToStr(xGiorno) + IntToStr(xMese) + '.doc';
          // Apri Word
          try MsWord := CreateOleObject('Word.Basic');
          except
               ShowMessage('Non riesco ad aprire Microsoft Word.');
               exit;
          end;
          MsWord.AppShow;
          // apri modello
          MsWord.FileOpen(GetDocPath + '\' + xModelloWord + '.doc');
          // riempimento campi
          Close;
          SQL.text := 'select NomeCampo,Campo,TipoCampo from ModelliWordCampi where IDModello=:xIDMod: order by ID';
          ParamByName['xIDMod'] := xIDModello;
          Open;
          while not EOF do begin
               MsWord.EditFind(FieldbyName('NomeCampo').asString);
               xcampo := trim(FieldByName('Campo').asString);
               if QRes1.FieldbyName(xcampo).asString <> '' then
                    MsWord.Insert(QRes1.FieldbyName(xcampo).asString)
               else
                    MSWord.Insert(' ');
               Next;
          end;
          Close;
          Data.QTemp.Close;

          if xSalva then
               // salvataggio
               MsWord.FileSaveAs(xFileWord);
          Result := xFileWord;
          if xStampa then begin
               MsWord.FilePrint;
               MsWord.FileClose;
               MsWord.AppClose;
          end;
     end;
end;

procedure TRicClientiForm.BitBtn7Click(Sender: TObject);
var xNonInviati, xinviato, xCCNStringa: string;
     k, i: integer;
     Browser: TWebBrowser;
     xMess, xMail: TstringList;
     xCCn: boolean;
begin
     xCCNStringa := '';
     if DBGrid5.SelectedCount <= 0 then begin
          MessageDlg('Nessun contatto selezionato - Possibilit� di selezionare pi� candidati tenendo premuto CTRL', mtError, [mbOk], 0);
          Exit;
     end;

     if ((RGOpzione.ItemIndex = 0)) then begin
          Browser := TWebBrowser.Create(self);
          SceltaModelloMailCliForm := TSceltaModelloMailCliForm.create(self);
          SceltaModelloMailCliForm.BModTesti.Visible := true;
          SceltaModelloMailCliForm.ShowModal;

          if SceltaModelloMailCliForm.ModalResult = mrOk then begin
               xMess := TStringList.Create;
               xMess.Add(SceltaModelloMailCliForm.QTipiMailContattiCli.FieldByName('Intestazione').AsString);
               xMess.Add('');
               // corpo (da tabella)
               xMess.Add(SceltaModelloMailCliForm.QTipiMailContattiCli.FieldByName('Corpo').Value);
               // saluti e firma (da tabella Utenti) con ritorno a capo
               xMess.Add('');

               EmailMessageForm := TEmailMessageForm.create(self);
               EmailMessageForm.Memo1.Lines := xMess;
               EmailMessageForm.ShowModal;
               if EmailMessageForm.ModalResult = mrCancel then begin
                    EmailMessageForm.Free;
                    exit;
               end;
               xmess.Clear;
               for i := 0 to EmailMessageForm.Memo1.Lines.count - 1 do
                    //xMess.Add(EmailMessageForm.Memo1.Lines[i] + '%0D%0A');
                    xMess.Add(EmailMessageForm.Memo1.Lines[i]);

               //ShowMessage('Predisporre connessione Internet e premere OK quando pronti');
               EmailMessageForm.Free;
               // connessione
               //StatusBar1.Visible := True;

               if DBGrid5.SelectedCount > 1 then begin
                    if MessageDlg('Attenzione!:Stai per inviare il messaggio a pi� destinatari: Vuoi inserire tutti gli indirizzi in Copia Conoscenza nascosta (CCN)?', mtWarning, [mbYes, mbNo], 0) = mrYes then
                         xCcn := True
                    else
                         xCcn := False;
               end;

               for k := 0 to DBGrid5.SelectedCount - 1 do begin
                    QRes1.BookMark := DBGrid5.SelectedRows[k];
                    if xCcn then
                         xCCNStringa := xCCNStringa + QRes1.FieldByName('Email').asString + '; '
                    else begin
                         try
                              //ShellExecute(0, 'Open', pchar('mailto:' + QRes1.FieldByName('Email').asString + '?subject=' + stringReplace(Trim(SceltaModelloMailCliForm.QTipiMailContattiCli.FieldByName('Oggetto').AsString), '"', '''''', [rfReplaceAll]) + '&body=' + stringreplace(xMess.Text,'&','%26',[rfReplaceAll])), '', '', SW_SHOW);
                              SendEMailMAPI(stringReplace(Trim(SceltaModelloMailCliForm.QTipiMailContattiCli.FieldByName('Oggetto').AsString), '"', '''''', [rfReplaceAll]), stringreplace(xMess.Text, '&', '%26', [rfReplaceAll]), QRes1.FieldByName('Email').asString, '', '', MainForm.xSendMail_Remote, MainForm.xDebug, True, False);
                              if SceltaModelloMailCliForm.CBStoricoInvio.checked then begin
                                   StoricizzaInvio(QRes1.FieldByName('IDContatto').Value,
                                        SceltaModelloMailCliForm.QTipiMailContattiCli.FieldByName('ID').AsInteger,
                                        QRes1.FieldByName('EtichettaAbitazione').AsBoolean,
                                        'M',
                                        SceltaModelloMailCliForm.QTipiMailContattiCli.FieldByName('Descrizione').AsString);
                              end;
                         except
                         end;
                    end;
               end;
               if xCcn then begin
                    try
                         SendEMailMAPI(stringReplace(Trim(SceltaModelloMailCliForm.QTipiMailContattiCli.FieldByName('Oggetto').AsString), '"', '''''', [rfReplaceAll]), stringreplace(xMess.Text, '&', '%26', [rfReplaceAll]), xCCNStringa, '', '', MainForm.xSendMail_Remote, MainForm.xDebug, True, False);
                         if SceltaModelloMailCliForm.CBStoricoInvio.checked then begin
                              StoricizzaInvio(QRes1.FieldByName('IDContatto').Value,
                                   SceltaModelloMailCliForm.QTipiMailContattiCli.FieldByName('ID').AsInteger,
                                   QRes1.FieldByName('EtichettaAbitazione').AsBoolean,
                                   'M',
                                   SceltaModelloMailCliForm.QTipiMailContattiCli.FieldByName('Descrizione').AsString);
                         end;
                    except
                    end;
               end;
          end;

          xMess.Free;
          Browser.Free;
     end else
          MessageDlg('Per inviare una mail � necessario estrarre l''elenco dei contatti', mtError, [mbOk], 0);

     {if (not (QRes1.active)) or (QRes1.RecordCount = 0) then exit;
     SceltaModelloMailCliForm := TSceltaModelloMailCliForm.create(self);

     if xInvio > 0 then begin
       // posizionati sul modello di mail (--> SE ASSOCIATO AL TIPO DI INVIO)
       Data.Q1.Close;
       Data.Q1.SQL.text := 'select IDTipoMailContatto from TipiInviiContatti where ID=' + IntTostr(xInvio);
       Data.Q1.Open;
       if Data.Q1.FieldByName('IDTipoMailContatto').asString <> '' then
         while (not SceltaModelloMailCliForm.QTipiMailContattiCli.Eof) and (SceltaModelloMailCliForm.QTipiMailContattiCli.FieldByName('ID').AsInteger <> Data.Q1.FieldByName('IDTipoMailContatto').asInteger) do
           SceltaModelloMailCliForm.QTipiMailContattiCli.Next;
       Data.Q1.Close;
     end;

     SceltaModelloMailCliForm.ShowModal;
     if SceltaModelloMailCliForm.ModalResult = mrOK then begin
       // PER tutti quelli selezionati
       xNonInviati := '';
       for k := 0 to DBGrid5.SelectedCount - 1 do begin
         xinviato := '';
         QRes1.BookMark := DBGrid5.SelectedRows[k];

         xinviato := TrimRight(SpedisciMail);
         xNonInviati := xNonInviati + xinviato + chr(13);
         // se ha spedito ==> STORICO
         if xinviato <> '' then
           // storicizzazione (se impostata)
           StoricizzaInvio(QRes1.FieldByName('IDContatto').Value,
             SceltaModelloMailCliForm.QTipiMailContattiCli.FieldByName('ID').AsInteger,
             QRes1.FieldByName('EtichettaAbitazione').AsBoolean,
             'M',
             SceltaModelloMailCliForm.QTipiMailContattiCli.FieldByName('Descrizione').AsString);

         if (xinviato <> '') and (SceltaModelloMailCliForm.CBStoricoInvio.checked) then begin
         end;
       end;
       if (xNonInviati <> '') and (xNonInviati <> chr(13)) then
         ShowMessage('Messaggi non inviati ai seguenti soggetti per mancanza indirizzo e-mail:' + chr(13) +
           xnonInviati)
       else ShowMessage('Messaggi inviati con successo');
       {Browser := TWebBrowser.Create(self);
       xMail := TStringList.Create;

       xmail.values['subject'] := SceltaModelloMailCliForm.QTipiMailContattiCli.FieldByName('Oggetto').AsString;
       xMess := TStringList.create;
       // intestazione (da tabella)
       //for i := 0 to MMail.lines.count - 1 do
         //xMess.Add(MMail.lines[i]);
       xMess.Add(SceltaModelloMailCliForm.QTipiMailContattiCli.FieldByName('Corpo').AsString);


       Browser.Free;
       xMail.Free;
     end;
     SceltaModelloMailCliForm.Free;  }

end;

//Function non pi� in utilizzo per passaggio all'embarcadero - Thomas 26-11-2013
{function TRicClientiForm.SpedisciMail: string;
var i, xTot, xCRpos: integer;
     xMess, xSl: TstringList;
     Vero: boolean;
     xNonInviati, xNonInviatiSMS, xFirma, xS: string;
begin
     // impostazione parametri
     NMSMTP1.Host := SceltaModelloMailCliForm.EHost.Text;
     NMSMTP1.Port := StrToInt(SceltaModelloMailCliForm.EPort.Text);
     NMSMTP1.UserID := SceltaModelloMailCliForm.EUserID.Text;
     // oggetto (da tabella)
     NMSMTP1.PostMessage.Subject := SceltaModelloMailCliForm.QTipiMailContattiCli.FieldByName('Oggetto').AsString;
     // COMPOSIZIONE MESSAGGIO
     xMess := TStringList.create;
     // intestazione (da tabella)
     xMess.Add(SceltaModelloMailCliForm.QTipiMailContattiCli.FieldByName('Intestazione').AsString);
     xMess.Add('');
     Q.Close;
     Q.SQL.Text := 'select Descrizione,FirmaMail from Users where ID=' + IntToStr(MainForm.xIDUtenteAttuale);
     Q.Open;
     // corpo (da tabella)
     xMess.Add(SceltaModelloMailCliForm.QTipiMailContattiCli.FieldByName('Corpo').AsString);
     // saluti e firma (da tabella Utenti) con ritorno a capo
     xMess.Add('');
     xS := Q.FieldByName('FirmaMail').asString;
     while pos(chr(13), xS) > 0 do begin
          xCRpos := pos(chr(13), xS);
          xMess.Add(copy(xS, 1, xCRpos - 1));
          xS := copy(xS, xCRpos + 1, length(xS));
     end;
     xMess.Add(xS);

     // edit messaggio
     EmailMessageForm := TEmailMessageForm.create(self);
     EmailMessageForm.Memo1.Lines := xMess;
     EmailMessageForm.ShowModal;
     if EmailMessageForm.ModalResult = mrCancel then begin
          EmailMessageForm.Free;
          exit;
     end;
     xmess.Clear;
     for i := 0 to EmailMessageForm.Memo1.Lines.count - 1 do
          xMess.Add(EmailMessageForm.Memo1.Lines[i]);
     ShowMessage('Predisporre connessione Internet e premere OK quando pronti');
     EmailMessageForm.Free;
     NMSMTP1.PostMessage.Body := xMess;
     // connessione
     NMSMTP1.Connect;
     // header (mittente)
     NMSMTP1.PostMessage.FromAddress := SceltaModelloMailCliForm.EAddress.Text;
     NMSMTP1.PostMessage.FromName := SceltaModelloMailCliForm.EName.Text;
     // invio vero e proprio dei messaggi
     xNonInviati := '';
     xNonInviatiSMS := '';

     if QRes1.FieldByName('EMail').AsString = '' then
          Result := QRes1.FieldByName('Contatto').AsString
     else begin
          NMSMTP1.PostMessage.ToAddress.Add(QRes1.FieldByName('EMail').AsString);
          // invio
          NMSMTP1.SendMail;
          Result := '';
     end;

     // disconnessione
     Q.Close;
     NMSMTP1.Disconnect;
     xMess.Free;
end;    }

procedure TRicClientiForm.StoricizzaInvio(xIDContattoCli, xIDTipoInvio: integer; xAbitazione: boolean; xTipo, xNote: string);
begin
     with Data do begin
          DB.BeginTrans;
          try
               Q1.Close;
               Q1.SQL.text := 'insert into StoricoInviiContatto (IDContattoCli,IDTipoInvio,Data,Abitazione,Note,Tipo) ' +
                    'values (:xIDContattoCli:,:xIDTipoInvio:,:xData:,:xAbitazione:,:xNote:,:xTipo:)';
               Q1.ParamByName['xIDContattoCli'] := xIDContattoCli;
               Q1.ParamByName['xIDTipoInvio'] := xIDTipoInvio;
               Q1.ParamByName['xData'] := Date;
               Q1.ParamByName['xAbitazione'] := xAbitazione;
               Q1.ParamByName['xNote'] := xNote;
               Q1.ParamByName['xTipo'] := xTipo;
               Q1.ExecSQL;
               DB.CommitTrans;
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
          end;
     end;
end;

procedure TRicClientiForm.BitBtn8Click(Sender: TObject);
var k: integer;
begin
     OpzioniEtichetteForm := TOpzioniEtichetteForm.create(self);
     OpzioniEtichetteForm.ShowModal;
     if OpzioniEtichetteForm.ModalResult = mrOK then begin
          // riempimento tabella temporanea
          Q.Close;
          Q.SQL.text := 'truncate table EtichetteCliTemp';
          Q.ExecSQL;
          for k := 0 to DBGrid5.SelectedCount - 1 do begin
               QRes1.BookMark := DBGrid5.SelectedRows[k];
               Q.Close;
               Q.SQL.text := 'insert into EtichetteCliTemp (Cliente,Indirizzo1,Indirizzo2,Indirizzo3,Contatto,Note) ' +
                    'values (:xCliente:,:xIndirizzo1:,:xIndirizzo2:,:xIndirizzo3:,:xContatto:,:xNote:)';
               Q.ParamByName['xCliente'] := QRes1.FieldByName('Cliente').asString;
               Q.ParamByName['xIndirizzo1'] := QRes1.FieldByName('TipoStrada').asString + ' ' + QRes1.FieldByName('Indirizzo').asString + ' ' + QRes1.FieldByName('NumCivico').asString;
               Q.ParamByName['xIndirizzo2'] := QRes1.FieldByName('Cap').asString + ' - ' + QRes1.FieldByName('Comune').asString;
               Q.ParamByName['xIndirizzo3'] := UpperCase(QRes1.FieldByName('Provincia').asString);
               Q.ParamByName['xContatto'] := QRes1.FieldByName('Titolo').asString + ' ' + QRes1.FieldByName('Contatto').asString;
               Q.ParamByName['xNote'] := QRes1.FieldByName('Note').asString;
               Q.ExecSQL;
          end;
          QREtichette2Form := TQREtichette2Form.create(self);
          QREtichette2Form.xStampaNote := OpzioniEtichetteForm.CBNote.Checked;
          if OpzioniEtichetteForm.RGStampa.ItemIndex = 0 then
               QREtichette2Form.Preview
          else QREtichette2Form.Print;
          QREtichette2Form.Free;
          if OpzioniEtichetteForm.CBStoricizza.Checked then begin
               for k := 0 to DBGrid5.SelectedCount - 1 do begin
                    QRes1.BookMark := DBGrid5.SelectedRows[k];
                    // storicizzazione invio etichetta
                    StoricizzaInvio(QRes1.FieldByName('IDContatto').Value,
                         xInvio,
                         QRes1.FieldByName('EtichettaAbitazione').AsBoolean,
                         'E',
                         'etichetta');
               end;
          end;
     end;
     OpzioniEtichetteForm.Free;
end;

procedure TRicClientiForm.DBGrid5MouseUp(Sender: TObject;
     Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
     if (Button <> mbRight) or (Shift <> []) then Exit;
     TdxDBGridPopupMenuManager.Instance.ShowGridPopupMenu(Sender as TdxDBGrid);
end;

procedure TRicClientiForm.Stampagriglia1Click(Sender: TObject);
begin
     dxPrinter1.Preview(True, nil);
end;

procedure TRicClientiForm.EsportainExcel1Click(Sender: TObject);
begin
     Save('xls', 'Microsoft Excel 4.0 Worksheet (*.xls)|*.xls', 'ExpGrid.xls', DbGrid5.SaveToXLS);
end;

procedure TRicClientiForm.Save(ADefaultExt, AFilter, AFileName: string;
     AMethod: TSaveMethod);
begin
     with SaveDialog do
     begin
          DefaultExt := ADefaultExt;
          Filter := AFilter;
          FileName := AFileName;
          if Execute then AMethod(FileName, True);
     end;
end;

procedure TRicClientiForm.EsportainHTML1Click(Sender: TObject);
begin
     Save('htm', 'HTML File (*.htm; *.html)|*.htm', 'ExpGrid.htm', DbGrid5.SaveToHTML);
end;

//[TONI20021004]DEBUGOK

procedure TRicClientiForm.BAddTargetListClick(Sender: TObject);
var xIDTargetList: integer;
     xpassword: string;
begin
     // stesso cliente della commessa
     if xIDClienteRic = QRes1.FieldByName('IDCliente').AsInteger then begin
          messageDlg('L''azienda � il cliente della commessa !!', mtError, [mbOK], 0);
          exit;
     end;
     // controllo se � gi� un cliente in target list
     Data.QTemp.Close;
     Data.QTemp.SQL.text := 'select count(*) Tot from EBC_RicercheTargetList ' +
          'where IDRicerca=:xIDRicerca: and IDCliente=:xIDCliente:';
     Data.QTemp.ParamByName['xIDRicerca'] := xIDRicerca;
     Data.QTemp.ParamByName['xIDCliente'] := QRes1.FieldByName('IDCliente').AsInteger;
     Data.QTemp.Open;
     if Data.QTemp.FieldByName('Tot').asInteger > 0 then begin
          messageDlg('Azienda gi� presente nella target list !', mtError, [mbOK], 0);
          Data.QTemp.Close;
          exit;
     end;
     // controllo blocco 1
     Data.QTemp.Close;
     Data.QTemp.SQL.text := 'select count(*) Tot from EBC_Clienti where ID=:xID: and Blocco1=1';
     Data.QTemp.ParamByName['xID'] := QRes1.FieldByName('IDCliente').AsInteger;
     Data.QTemp.Open;
     if Data.QTemp.FieldByName('Tot').asInteger > 0 then begin
          if messageDlg('ATTENZIONE:  l''azienda ha impostato il BLOCCO di livello 1 - VUOI PROSEGUIRE ? ', mtWarning, [mbYes, mbNo], 0) = mrNo then begin
               Data.QTemp.Close;
               exit;
          end;
     end;


     // controllo storico target list
     if SelPersForm.CheckStoricoTL(QRes1.FieldByName('IDCliente').AsInteger) = False then exit;
     if messageDlg('Sei sicuro di voler aggiungere l''azienda ' + QRes1.FieldByName('Cliente').AsString + ' alla target List ?',
          mtWarning, [mbYes, mbNo], 0) = mrNo then exit;
     // controllo blocco 2
     Data.QTemp.Close;
     Data.QTemp.SQL.text := 'select count(*) Tot from EBC_Clienti where stato= ''attivo'' and IDTipoAzienda = 1 and ID = :xID:';
     Data.QTemp.ParamByName['xID'] := QRes1.FieldByName('IDCliente').AsInteger;
     Data.QTemp.Open;
     if Data.QTemp.FieldByName('Tot').asInteger > 0 then begin
          if MessageDlg('ATTENZIONE!! PROTEZIONE CLIENTI LIVELLO 2' + chr(13) + 'Stai inserendo in Target List un candidato appartenente ad una azienda cliente ATTIVA' + chr(13) +
               'L''operazione � protetta dalle normative interne, se sei autorizzato premi SI, altrimenti premi NO. ' +
               'Se si prosegue, l''operazione verr� registrata nello storico del cliente come forzatura. '
               , mtWarning, [mbYes, mbNo], 0) = mrYes then begin
               if pos('INTERMEDIA', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) = 0 then begin
                    {if not InputQuery('Forzatura blocco livello 2', 'Password di Administrator', xPassword) then exit;}
                    InputPasswordForm.EPassword.text := 'EPassword';
                    InputPasswordForm.caption := 'Inserimento Password Administrator';
                    InputPasswordForm.ShowModal;
                    if InputPasswordForm.ModalResult = mrOK then begin
                         xPassword := InputPasswordForm.EPassword.Text;
                    end else exit;
                    if not CheckPassword(xPassword, GetPwdUtenteResp(9)) then begin
                         MessageDlg('Password errata', mtError, [mbOK], 0);
                         exit;
                    end;
               end;
          end else
               exit;

     end;
     Data.QTemp.Close;
     with Data do begin
          DB.BeginTrans;
          try
               Q1.SQL.text := 'insert into EBC_RicercheTargetList (IDRicerca,IDCliente) ' +
                    'values (:xIDRicerca:,:xIDCliente:)';
               Q1.ParamByName['xIDRicerca'] := xIDRicerca;
               Q1.ParamByName['xIDCliente'] := QRes1.FieldByName('IDCliente').AsInteger;
               Q1.ExecSQL;
               Q1.SQL.text := 'select @@IDENTITY as LastID';
               Q1.Open;
               xIDTargetList := Q1.FieldByName('LastID').asInteger;
               Q1.Close;
               Q1.SQL.text := 'insert into EBC_LogEsplorazioni (IDTargetList,Data) ' +
                    'values (:xIDTargetList:,:xData:)';
               Q1.ParamByName['xIDTargetList'] := xIDTargetList;
               Q1.ParamByName['xData'] := Date;
               Q1.ExecSQL;
               DB.CommitTrans;
               ShowMessage('Inserimento in Target list avvenuto');
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: operazione non effettuata', mtError, [mbOK], 0);
          end;
     end;
end;

//[ALBERTO 20020910]FINE

procedure TRicClientiForm.TLinee_OLDAfterPost(DataSet: TDataSet);
begin
     TLinee.Close;
     TLinee.Open;
end;

procedure TRicClientiForm.QRes1BeforeOpen(DataSet: TDataSet);
begin
     case rgOpzione.ItemIndex of
          0: begin
                    DBGrid5.KeyField := 'IDContatto';
                    DBGrid5Titolo.Visible := True;
                    DBGrid5COntatto.Visible := True;
                    DBGrid5DataUltimoCont.Visible := True;
                    DBGrid5Sesso.Visible := True;

                    DBGrid5CONTATTOParlatoCon.visible := False;
                    DBGrid5CONTATTOData.visible := False;
                    DBGrid5CONTATTOTipo.visible := False;
                    DBGrid5CONTATTODescrizione.visible := False; ;
                    DBGrid5EsitoContatto.Visible := True;
                    DBGrid5AttivitaFuture.Visible := True;
               end;
          1, 2: begin
                    DBGrid5.KeyField := 'IDCliente';
                    DBGrid5Titolo.Visible := False;
                    DBGrid5COntatto.Visible := False;
                    DBGrid5DataUltimoCont.Visible := False;
                    DBGrid5Sesso.Visible := False;

                    DBGrid5CONTATTOParlatoCon.visible := False;
                    DBGrid5CONTATTOData.visible := False;
                    DBGrid5CONTATTOTipo.visible := False;
                    DBGrid5CONTATTODescrizione.visible := False;

                    DBGrid5EsitoContatto.Visible := False;
                    DBGrid5AttivitaFuture.Visible := False;
               end;
          3: begin
                    DBGrid5.KeyField := 'IDCliente';
                    DBGrid5CONTATTOParlatoCon.visible := True;
                    DBGrid5CONTATTOData.visible := True;
                    DBGrid5CONTATTOTipo.visible := True;
                    DBGrid5CONTATTODescrizione.visible := True;
                    DBGrid5Titolo.Visible := False;
                    DBGrid5COntatto.Visible := False;
                    DBGrid5DataUltimoCont.Visible := False;

                    DBGrid5EsitoContatto.Visible := True;
                    DBGrid5AttivitaFuture.Visible := True;

               end;

     end;
end;

procedure TRicClientiForm.Modificanoteazienda1Click(Sender: TObject);
begin
     NoteClienteForm := TNoteClienteForm.create(self);
     NoteClienteForm.QNoteCliente.SQL.text := 'select ID,Notecontratto as Note from EBC_Clienti where ID=' + QRes1.FieldByName('IDCliente').AsString;
     NoteClienteForm.QNoteCliente.Open;
     NoteClienteForm.ShowModal;
     NoteClienteForm.Free;
end;

procedure TRicClientiForm.PMModDatiPopup(Sender: TObject);
begin
     if QRes1.IsEmpty then begin
          Modificanoteazienda1.Enabled := False;
          Modificacontatto1.Enabled := False;
          ModiifcaTitolo1.Enabled := False;
          Modificanotecontatto1.Enabled := False;
     end else begin
          Modificanoteazienda1.Enabled := True;
          Modificacontatto1.Enabled := True;
          ModiifcaTitolo1.Enabled := True;
          Modificanotecontatto1.Enabled := True;
     end;

     if (RGOpzione.ItemIndex = 1) or (RGOpzione.ItemIndex = 2) then begin
          Modificacontatto1.Enabled := False;
          ModiifcaTitolo1.Enabled := False;
          Modificanotecontatto1.Enabled := False;
     end else begin
          Modificacontatto1.Enabled := True;
          ModiifcaTitolo1.Enabled := True;
          Modificanotecontatto1.Enabled := True;
     end;
end;

procedure TRicClientiForm.Modificacontatto1Click(Sender: TObject);
var xS: string;
     xID: integer;
begin
     xS := QRes1.FieldByName('Contatto').asString;
     if InputQuery('modifica nome contatto', 'Nuovo nome:', xs) then begin
          Q.close;
          Q.SQL.text := 'update EBC_ContattiClienti set Contatto=:xContatto: ' +
               'where ID=' + QRes1.FieldByName('IDContatto').asString;
          Q.ParamByName['xContatto'] := xS;
          Q.ExecSQL;
          xID := QRes1.FieldByName('IDContatto').asInteger;
          QRes1.Close;
          QRes1.Open;
          QRes1.Locate('IDContatto', xID, []);
     end;
end;

procedure TRicClientiForm.ModiifcaTitolo1Click(Sender: TObject);
var xS: string;
     xID: integer;
begin
     xS := QRes1.FieldByName('Titolo').asString;
     if InputQuery('modifica titolo contatto', 'Nuovo titolo:', xs) then begin
          Q.close;
          Q.SQL.text := 'update EBC_ContattiClienti set Titolo=:xTitolo: ' +
               'where ID=' + QRes1.FieldByName('IDContatto').asString;
          Q.ParamByName['xTitolo'] := xS;
          Q.ExecSQL;
          xID := QRes1.FieldByName('IDContatto').asInteger;
          QRes1.Close;
          QRes1.Open;
          QRes1.Locate('IDContatto', xID, []);
     end;
end;

procedure TRicClientiForm.Modificanotecontatto1Click(Sender: TObject);
var xS: string;
     xID: integer;
begin
     xS := QRes1.FieldByName('Note').asString;
     if InputQuery('modifica note contatto', 'Nuove note:', xs) then begin
          Q.close;
          Q.SQL.text := 'update EBC_ContattiClienti set Note=:xNote: ' +
               'where ID=' + QRes1.FieldByName('IDContatto').asString;
          Q.ParamByName['xNote'] := xS;
          Q.ExecSQL;
          xID := QRes1.FieldByName('IDContatto').asInteger;
          QRes1.Close;
          QRes1.Open;
          QRes1.Locate('IDContatto', xID, []);
     end;
end;

procedure TRicClientiForm.BitBtn10Click(Sender: TObject);
begin
     if (pos('ADVANT', Uppercase(Data.Global.FieldByName('NomeAzienda').Value)) > 0) or
          (pos('MCS', Uppercase(Data.Global.FieldByName('NomeAzienda').Value)) > 0) then begin
          // QRes1 dalla 10a riga in poi
          //xSQL := 'select distinct Anagrafica.ID';
          {for i := 9 to QRes1.SQL.Count - 2 do begin
            xSQL := xSQL + chr(13) + QRes1.SQL.Strings[i];
          end; }
          // predisponi Data.TAnagrafica
          if Data2.TEBCClienti.Active then Data2.TEBCClienti.Close;
          Data2.TEBCClienti.SQL.text := 'SELECT EBC_Clienti.ID, EBC_Clienti.Descrizione, EBC_Clienti.Stato, ' +
               'EBC_Clienti.Indirizzo, EBC_Clienti.Cap, EBC_Clienti.Comune, ' +
               'EBC_Clienti.Provincia, EBC_Clienti.IDAttivita, EBC_Clienti.Telefono, ' +
               'EBC_Clienti.Fax, EBC_Clienti.PartitaIVA, EBC_Clienti.CodiceFiscale, ' +
               'EBC_Clienti.BancaAppoggio, EBC_Clienti.SistemaPagamento, ' +
               'EBC_Clienti.NoteContratto, EBC_Clienti.Responsabile, ' +
               'EBC_Clienti.ConosciutoInData, EBC_Clienti.IndirizzoLegale, ' +
               'EBC_Clienti.CapLegale, EBC_Clienti.ComuneLegale, SitoInternet, ' +
               'EBC_Clienti.ProvinciaLegale, Ebc_attivita.Attivita, NumDipendenti, Fatturato, ' +
               'EBC_Clienti.CartellaDoc, EBC_Clienti.TipoStrada,EBC_Clienti.NumCivico, ' +
               'EBC_Clienti.TipoStradaLegale, EBC_Clienti.NumCivicoLegale, ' +
               'EBC_Clienti.NazioneAbb, EBC_Clienti.NazioneAbbLegale,EBC_Clienti.IDClienteFiliale, ' +
               'Blocco1,EnteCertificatore,LibPrivacy, CCIAA, IDTipoAzienda,DescrizAnnunci,IDRegolaCliente,IDConoscTramite, ' +
               'LetteraIntento, DataLetteraIntenti ' +
               'FROM EBC_Clienti,EBC_Attivita ' +
               'WHERE  (EBC_Clienti.IDAttivita = Ebc_attivita.ID) ' +
               'and EBC_Clienti.ID = ' + QRes1.FieldByName('IDCliente').AsString +
               ' ORDER BY EBC_Clienti.Descrizione';


          Data2.TEBCClienti.Open;
          ASAAziendaForm := TASAAziendaForm.create(self);
          ASAAziendaForm.ShowModal;
          ASAAziendaForm.Free;
     end;
end;

end.

