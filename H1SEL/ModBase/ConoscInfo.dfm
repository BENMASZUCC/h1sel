object ConoscInfoForm: TConoscInfoForm
  Left = 361
  Top = 242
  Width = 619
  Height = 401
  Caption = ' '
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 611
    Height = 44
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object BVoceNew: TToolbarButton97
      Left = 4
      Top = 4
      Width = 62
      Height = 37
      Caption = 'Nuovo'
      Opaque = False
      WordWrap = True
      OnClick = BVoceNewClick
    end
    object BVoceDel: TToolbarButton97
      Left = 66
      Top = 4
      Width = 62
      Height = 37
      Caption = 'Elimina'
      Opaque = False
      WordWrap = True
      OnClick = BVoceDelClick
    end
    object BVoceCan: TToolbarButton97
      Left = 190
      Top = 4
      Width = 62
      Height = 37
      Caption = 'Annulla modifica'
      Enabled = False
      Opaque = False
      WordWrap = True
      OnClick = BVoceCanClick
    end
    object BVoceOK: TToolbarButton97
      Left = 128
      Top = 4
      Width = 62
      Height = 37
      Caption = 'Conferma modifica'
      Enabled = False
      Opaque = False
      WordWrap = True
      OnClick = BVoceOKClick
    end
    object BitBtn1: TBitBtn
      Left = 406
      Top = 4
      Width = 99
      Height = 38
      TabOrder = 0
      Kind = bkOK
    end
    object BitBtn2: TBitBtn
      Left = 508
      Top = 4
      Width = 99
      Height = 38
      Caption = 'Annulla'
      TabOrder = 1
      Kind = bkCancel
    end
  end
  object dxDBGrid1: TdxDBGrid
    Left = 0
    Top = 44
    Width = 611
    Height = 330
    Bands = <
      item
      end>
    DefaultLayout = True
    HeaderPanelRowCount = 1
    KeyField = 'ID'
    SummaryGroups = <>
    SummarySeparator = ', '
    Align = alClient
    TabOrder = 1
    DataSource = DsQConoscInfo
    Filter.Criteria = {00000000}
    OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
    OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoIndicator, edgoUseBitmap]
    object dxDBGrid1ID: TdxDBGridMaskColumn
      Visible = False
      Width = 164
      BandIndex = 0
      RowIndex = 0
      FieldName = 'ID'
    end
    object dxDBGrid1software: TdxDBGridColumn
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Software'
    end
  end
  object DsQConoscInfo: TDataSource
    DataSet = QConoscInfo
    OnStateChange = DsQConoscInfoStateChange
    Left = 64
    Top = 216
  end
  object QConoscInfo: TADOLinkedQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from ConoscenzeInfo'
      '')
    UseFilter = False
    Left = 24
    Top = 160
    object QConoscInfoID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QConoscInfoArea: TStringField
      FieldName = 'Area'
      FixedChar = True
      Size = 30
    end
    object QConoscInfoSottoArea: TStringField
      FieldName = 'SottoArea'
      FixedChar = True
      Size = 30
    end
    object QConoscInfoProprieta: TStringField
      FieldName = 'Proprieta'
      FixedChar = True
      Size = 30
    end
    object QConoscInfoProcedura: TStringField
      FieldName = 'Procedura'
      FixedChar = True
      Size = 50
    end
    object QConoscInfoProcedura_ENG: TStringField
      FieldName = 'Procedura_ENG'
      Size = 50
    end
    object QConoscInfoSoftware: TStringField
      FieldName = 'Software'
      Size = 800
    end
  end
end
