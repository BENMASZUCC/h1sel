//[/TONI20021909DEBUGOK]
unit PrezzoListino;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, RXSpin, Mask, ToolEdit, CurrEdit, DtEdit97, ExtCtrls,
     TB97;

type
     TPrezzoListinoForm = class(TForm)
          Label3: TLabel;
          Label4: TLabel;
          Label5: TLabel;
          ListinoLire: TRxCalcEdit;
          AnoiLire: TRxCalcEdit;
          AlClienteLire: TRxCalcEdit;
          ListinoEuro: TRxCalcEdit;
          AnoiEuro: TRxCalcEdit;
          AlClienteEuro: TRxCalcEdit;
          GroupBox1: TGroupBox;
          Label1: TLabel;
          Label2: TLabel;
          RxSpinEdit1: TRxSpinEdit;
          RxSpinEdit2: TRxSpinEdit;
          Label6: TLabel;
          Label7: TLabel;
          Bevel1: TBevel;
          Bevel2: TBevel;
          Label8: TLabel;
          DEDallaData: TDateEdit97;
          Label9: TLabel;
          ENote: TEdit;
          Panel1: TPanel;
          BitBtn1: TToolbarButton97;
          BitBtn2: TToolbarButton97;
          procedure ListinoLireChange(Sender: TObject);
          procedure AnoiLireChange(Sender: TObject);
          procedure AlClienteLireChange(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure ListinoEuroChange(Sender: TObject);
          procedure AnoiEuroChange(Sender: TObject);
          procedure AlClienteEuroChange(Sender: TObject);
          procedure BitBtn1Click(Sender: TObject);
          procedure BitBtn2Click(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     PrezzoListinoForm: TPrezzoListinoForm;

implementation

uses ModuloDati, Main;

{$R *.DFM}

procedure TPrezzoListinoForm.ListinoLireChange(Sender: TObject);
begin
     if ListinoLire.Focused then
          ListinoEuro.Value := ListinoLire.value / 1936.27;
end;

procedure TPrezzoListinoForm.AnoiLireChange(Sender: TObject);
begin
     if AnoiLire.Focused then
          AnoiEuro.Value := AnoiLire.value / 1936.27;
end;

procedure TPrezzoListinoForm.AlClienteLireChange(Sender: TObject);
begin
     if AlClienteLire.Focused then
          AlClienteEuro.Value := AlClienteLire.value / 1936.27;
end;

procedure TPrezzoListinoForm.FormShow(Sender: TObject);
begin
      //Grafica
     PrezzoListinoForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     Caption := '[M/3500] - ' + caption;
     if pos('BFK', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) > 0 then begin
          Label7.Caption := 'chf.';


          ListinoEuro.DisplayFormat := 'chf. ,0.00';
          ANoiEuro.DisplayFormat := 'chf. ,0.00';
          AlClienteEuro.DisplayFormat := 'chf. ,0.00';

     end;
end;

procedure TPrezzoListinoForm.ListinoEuroChange(Sender: TObject);
begin
     if ListinoEuro.Focused then
          ListinoLire.Value := ListinoEuro.value * 1936.27;
end;

procedure TPrezzoListinoForm.AnoiEuroChange(Sender: TObject);
begin
     if AnoiEuro.Focused then
          AnoiLire.Value := AnoiEuro.value * 1936.27;
end;

procedure TPrezzoListinoForm.AlClienteEuroChange(Sender: TObject);
begin
     if AlClienteEuro.Focused then
          AlClienteLire.Value := AlClienteEuro.value * 1936.27;
end;

procedure TPrezzoListinoForm.BitBtn1Click(Sender: TObject);
begin
     PrezzoListinoForm.ModalResult := mrOk;
end;

procedure TPrezzoListinoForm.BitBtn2Click(Sender: TObject);
begin
     PrezzoListinoForm.ModalResult := mrCancel;
end;

end.

