object CandUtenteFrame: TCandUtenteFrame
  Left = 0
  Top = 0
  Width = 666
  Height = 373
  TabOrder = 0
  object Panel12: TPanel
    Left = 0
    Top = 0
    Width = 666
    Height = 21
    Align = alTop
    Alignment = taLeftJustify
    Caption = '  Situazione candidati associati all'#39'utente attualmente connesso'
    Color = clGray
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
  end
  object Panel1: TPanel
    Left = 0
    Top = 21
    Width = 666
    Height = 41
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 1
    object BLegenda: TToolbarButton97
      Left = 557
      Top = 1
      Width = 108
      Height = 39
      Hint = 'legenda e parametri'
      Caption = 'Legenda e Parametri'
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000120B0000120B00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFAAA4A48181817F7F7F7F7F7F838383CDCDCDFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDCDAD8BBA8A2E8DAD4F7EAE6F7
        EAE6F5EAE6E0D2C9796D69565252DADADAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        F9F4F2EEE2DAF5F7F5E4DCDACDB5A6DAB5A2E0C9BDEDEAE8F5F5F3DACDC73D37
        35CDCDCDFFFFFFFFFFFFFFFFFFF4F0ECF5F0E8F3F0EED49C7FC16231C1734BDE
        B5A0C56233C36637D8B19CF1F5F5E8DAD33D3937FFFFFFFFFFFFFFFFFFF0E4DC
        F5F0F0CE7F5CC5602FCC632FC19C87FDFFFFD89068CA6331C35E2DD49E7FF5F5
        F7C7B7B1858585FFFFFFF0E8E2FBFBFBDEA98CC8602FCE6331CE6331CE6B3BDA
        8E66CE652FCE6331CC6331C35E2BE2C3B3F7EEEC524A45FFFFFFF0DED6F9F4F2
        CA693BCE6931CE6731CE6331CA8564F7E2D6D47343CE6331CE6331CC6331CB7D
        56F7FBFBA2918BACACACFBEEE8F0DACBCC6733D26B35CE6331CE6331C77F5CF9
        FBFBE4A281CE632FCE6331CE6931C86937F3F0ECD8CABF989898FBEEEAF0D4C3
        D46D37D26B35CE6331CE6331CC6531D0AD9EFDFDFBE09A77CE632FCE6B33CE69
        35F7EEE8DCCEC69A9A9AFBECE8F9E4DAE07741D86F3BCE652FCE632DCE6331CC
        652FE2C5B9FDF9F4D2713FD66D39D07341FDF7F4D4C5BDB0B0B0F2E4DAFFFFFF
        F59C6DE87D47D08E6CEED8CBD87F54CE632DDCA285FFFFFFDC8A64DC773FE69A
        75FFFFFFA0948CFFFFFFF2E8E2FDF9F9FFE0CFFD9C64E28D67EEEEEEF9ECE6EA
        B9A2FBF2EEF0EEEAE68354EB8150F9E8DEF9F2EEC5BFBBFFFFFFFFFFFFF0E2D8
        FFFFFFFFE4C7FFBF87EEBB96ECE0D8E6E2E2EEE2DAF9B38BFD9E68FFDAC3FFFD
        FDD8C9C1FFFFFFFFFFFFFFFFFFF4F0ECF0E4E0FFFFFFFFFBECFFF0CEFFE6B3FF
        D6A0FFD69EFFD8B1FFF4EAFFFFFFECDED8EEEAE8FFFFFFFFFFFFFFFFFFFFFFFF
        F4F0ECF0E0D6FDF9F4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDF4F0F5E8E4F4F0
        ECFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2E8E2F0E4DAFBECE6FB
        EEEAFBEAE4EEE2DAFBF6F2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      WordWrap = True
      OnClick = BLegendaClick
    end
  end
  object RxDBGrid1: TdxDBGrid
    Left = 0
    Top = 62
    Width = 666
    Height = 311
    Bands = <
      item
      end>
    DefaultLayout = True
    HeaderPanelRowCount = 1
    SummaryGroups = <>
    SummarySeparator = ', '
    Align = alClient
    TabOrder = 2
    DataSource = DsQCandUtente
    Filter.Active = True
    Filter.Criteria = {00000000}
    OptionsBehavior = [edgoAutoSort, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoTabThrough, edgoVertThrough]
    object RxDBGrid1CVNumero: TdxDBGridColumn
      Caption = 'N� CV'
      Width = 60
      BandIndex = 0
      RowIndex = 0
      FieldName = 'CVNumero'
    end
    object RxDBGrid1Cognome: TdxDBGridColumn
      Width = 123
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Cognome'
    end
    object RxDBGrid1Nome: TdxDBGridColumn
      Width = 114
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Nome'
    end
    object RxDBGrid1DataNascita: TdxDBGridColumn
      Caption = 'Nato il'
      Width = 71
      BandIndex = 0
      RowIndex = 0
      FieldName = 'DataNascita'
    end
    object RxDBGrid1RecapitiTelefonici: TdxDBGridColumn
      Caption = 'Telefono'
      Width = 98
      BandIndex = 0
      RowIndex = 0
      FieldName = 'RecapitiTelefonici'
    end
    object RxDBGrid1Cellulare: TdxDBGridColumn
      Width = 96
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Cellulare'
    end
    object RxDBGrid1ggDataUltimoColloquio: TdxDBGridColumn
      Caption = 'Col'
      Width = 45
      BandIndex = 0
      RowIndex = 0
      FieldName = 'ggDataUltimoColloquio'
    end
    object RxDBGrid1ggDataUltimoContatto: TdxDBGridColumn
      Caption = 'Uc'
      Width = 39
      BandIndex = 0
      RowIndex = 0
      FieldName = 'ggDataUltimoContatto'
    end
  end
  object QCandUtente_OLD: TQuery
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      
        'select distinct Anagrafica.ID,CVNumero,Cognome,Nome,DataNascita,' +
        'RecapitiTelefonici,Cellulare,'
      
        '       DATEDIFF(day, EBC_Colloqui.Data, getdate()) ggDataUltimoC' +
        'olloquio,'
      
        '       DATEDIFF(day, EBC_ContattiCandidati.Data, getdate()) ggDa' +
        'taUltimoContatto  '
      'from Anagrafica,EBC_Colloqui,EBC_ContattiCandidati'
      'where Anagrafica.ID *= EBC_Colloqui.IDAnagrafica'
      'and EBC_Colloqui.Data='
      '    (select max(Data) from EBC_Colloqui'
      '     where Anagrafica.ID *= EBC_Colloqui.IDAnagrafica)'
      'and Anagrafica.ID *= EBC_ContattiCandidati.IDAnagrafica'
      '    and EBC_ContattiCandidati.Data='
      '    (select max(Data) from EBC_ContattiCandidati'
      '     where Anagrafica.ID *= EBC_ContattiCandidati.IDAnagrafica)'
      'and Anagrafica.IDUtente=:xIDUtente'
      'order by Cognome,Nome')
    Left = 96
    Top = 200
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'xIDUtente'
        ParamType = ptUnknown
      end>
    object QCandUtente_OLDID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.Anagrafica.ID'
    end
    object QCandUtente_OLDCVNumero: TIntegerField
      FieldName = 'CVNumero'
      Origin = 'EBCDB.Anagrafica.CVNumero'
    end
    object QCandUtente_OLDCognome: TStringField
      FieldName = 'Cognome'
      Origin = 'EBCDB.Anagrafica.Cognome'
      FixedChar = True
      Size = 30
    end
    object QCandUtente_OLDNome: TStringField
      FieldName = 'Nome'
      Origin = 'EBCDB.Anagrafica.Nome'
      FixedChar = True
      Size = 30
    end
    object QCandUtente_OLDDataNascita: TDateTimeField
      FieldName = 'DataNascita'
      Origin = 'EBCDB.Anagrafica.DataNascita'
    end
    object QCandUtente_OLDRecapitiTelefonici: TStringField
      FieldName = 'RecapitiTelefonici'
      Origin = 'EBCDB.Anagrafica.RecapitiTelefonici'
      FixedChar = True
      Size = 30
    end
    object QCandUtente_OLDCellulare: TStringField
      FieldName = 'Cellulare'
      Origin = 'EBCDB.Anagrafica.Cellulare'
      FixedChar = True
      Size = 15
    end
    object QCandUtente_OLDggDataUltimoColloquio: TIntegerField
      FieldName = 'ggDataUltimoColloquio'
    end
    object QCandUtente_OLDggDataUltimoContatto: TIntegerField
      FieldName = 'ggDataUltimoContatto'
    end
  end
  object DsQCandUtente: TDataSource
    DataSet = QCandUtente
    Left = 168
    Top = 200
  end
  object QCandUtente: TADOLinkedQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'xIDUtente:'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'select distinct Anagrafica.ID,CVNumero,Cognome,Nome,DataNascita,' +
        'RecapitiTelefonici,Cellulare,'
      
        '       DATEDIFF(day, EBC_Colloqui.Data, getdate()) ggDataUltimoC' +
        'olloquio,'
      
        '       DATEDIFF(day, EBC_ContattiCandidati.Data, getdate()) ggDa' +
        'taUltimoContatto  '
      'from Anagrafica'
      
        'left outer join EBC_Colloqui on (Anagrafica.ID = EBC_Colloqui.ID' +
        'Anagrafica'
      #9'and EBC_Colloqui.Data='
      #9#9'(select max(Data) from EBC_Colloqui'
      #9#9' where Anagrafica.ID = EBC_Colloqui.IDAnagrafica))'
      
        'left outer join EBC_ContattiCandidati on (Anagrafica.ID = EBC_Co' +
        'ntattiCandidati.IDAnagrafica'
      #9'and EBC_ContattiCandidati.Data='
      #9#9'(select max(Data) from EBC_ContattiCandidati'
      #9#9' where Anagrafica.ID = EBC_ContattiCandidati.IDAnagrafica))'
      'where Anagrafica.IDUtente=:xIDUtente:'
      'order by Cognome,Nome'
      '')
    OriginalSQL.Strings = (
      
        'select distinct Anagrafica.ID,CVNumero,Cognome,Nome,DataNascita,' +
        'RecapitiTelefonici,Cellulare,'
      
        '       DATEDIFF(day, EBC_Colloqui.Data, getdate()) ggDataUltimoC' +
        'olloquio,'
      
        '       DATEDIFF(day, EBC_ContattiCandidati.Data, getdate()) ggDa' +
        'taUltimoContatto  '
      'from Anagrafica'
      
        'left outer join EBC_Colloqui on (Anagrafica.ID = EBC_Colloqui.ID' +
        'Anagrafica'
      #9'and EBC_Colloqui.Data='
      #9#9'(select max(Data) from EBC_Colloqui'
      #9#9' where Anagrafica.ID = EBC_Colloqui.IDAnagrafica))'
      
        'left outer join EBC_ContattiCandidati on (Anagrafica.ID = EBC_Co' +
        'ntattiCandidati.IDAnagrafica'
      #9'and EBC_ContattiCandidati.Data='
      #9#9'(select max(Data) from EBC_ContattiCandidati'
      #9#9' where Anagrafica.ID = EBC_ContattiCandidati.IDAnagrafica))'
      'where Anagrafica.IDUtente=:xIDUtente:'
      'order by Cognome,Nome')
    UseFilter = False
    Left = 136
    Top = 200
    object QCandUtenteID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QCandUtenteCVNumero: TIntegerField
      FieldName = 'CVNumero'
    end
    object QCandUtenteCognome: TStringField
      FieldName = 'Cognome'
      Size = 100
    end
    object QCandUtenteNome: TStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QCandUtenteDataNascita: TDateTimeField
      FieldName = 'DataNascita'
    end
    object QCandUtenteRecapitiTelefonici: TStringField
      FieldName = 'RecapitiTelefonici'
      Size = 30
    end
    object QCandUtenteCellulare: TStringField
      FieldName = 'Cellulare'
      Size = 30
    end
    object QCandUtenteggDataUltimoColloquio: TIntegerField
      FieldName = 'ggDataUltimoColloquio'
      ReadOnly = True
    end
    object QCandUtenteggDataUltimoContatto: TIntegerField
      FieldName = 'ggDataUltimoContatto'
      ReadOnly = True
    end
  end
end
