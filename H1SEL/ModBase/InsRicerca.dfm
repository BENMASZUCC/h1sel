�
 TINSRICERCAFORM 0�4  TPF0TInsRicercaFormInsRicercaFormLeftATop� BorderStylebsDialogCaptionNuova commessa/ricercaClientHeight'ClientWidthRColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnCloseQueryFormCloseQueryOnCreate
FormCreateOnShowFormShowPixelsPerInch`
TextHeight TLabelLabel1LeftTopWidth4HeightCaptionData inizio:  TLabelLabel2LeftTopWidth7HeightCaptionN� ricercati:  TLabelLabel10LeftTop:Width9HeightCaptionTipo offerta:  TLabelLabel4LeftTop�WidthuHeightCaptionData prevista di chiusuraFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel5LeftTop�Width� HeightCaptionData prevista di presentazioneFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel6LeftTopRWidth>HeightCaptionStato iniziale:  TDateEdit97DEDataLeftLTopWidthaHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder ColorCalendar.ColorValidclBlueDayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanPopup
doIsMaskeddoShowCanceldoShowToday   	TSpinEditSENumRicLeftLTopWidth)HeightMaxValuedMinValue TabOrderValue   	TGroupBox	GroupBox3LeftTopWidthHeightgCaptionRuolo richiestoTabOrder TLabelLabel43LeftTop=Width� HeightCaption0Ruolo richiesto (titolo attribuito dal cliente):Font.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TToolbarButton97SpeedButton1Left� TopWidthHeightHint"inserisci o cambia ruolo richiesto
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333�3333�39�3330 373337w39�3330 3��337w9���33337ww333?9���333 7wws333w39�3333 373333w39�3333337s3333?3333333 3333�33w333<333 3337�33w333<�3333����?�<����Ù37wwwww�<����Ù37wwwwsw3333<�3333337s33�333<330 3337337w3333330 3333337w3333333333333333	NumGlyphsOnClickSpeedButton1Click  TEditERuoloLeftTopWidth� HeightReadOnly	TabOrder   TEditEAreaLeftTop%Width� HeightColor	clBtnFaceReadOnly	TabOrder  TEditETitoloCLienteLeftTopKWidth� HeightTabOrder   	TGroupBox	GroupBox4LeftTop�Width� Height*CaptionSelez.princ. (capo commessa)TabOrder TDBGridDBGrid1LeftTopWidth� Height
DataSourceDsUsersOptionsdgColumnResizedgTabsdgConfirmDeletedgCancelOnExit TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style VisibleColumnsExpanded	FieldName
NominativoWidthiVisible	    	TComboBoxcbUtenteLeftTopWidth� HeightStylecsDropDownList
ItemHeightTabOrder   	TGroupBox	GBClienteLeftTop� Width%HeightDCaptionClienteTabOrder TLabelLabel3LeftTop+Width[HeightCaptionRiferimento interno:  TToolbarButton97BTrovaClienteLeftTopWidthHeightHintseleziona cliente
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333333333333333333333333�3333330 333����w330  0 �337ww7w�33�� � 33�3w�w33 ��� 33ws37�w30����� 3���37�w0  ��� 7wws37�w������ s����7�w0   �� 7wwws7�w333�   333s�www3330 3333337w3333333333333333333333333333333333333333333333333333333333333333333	NumGlyphsOnClickBTrovaClienteClick  TToolbarButton97SpeedButton2LeftTop'WidthHeightHint,seleziona riferimento interno per il cliente
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333333333333333333333333�3333330 333����w330  0 �337ww7w�33�� � 33�3w�w33 ��� 33ws37�w30����� 3���37�w0  ��� 7wws37�w������ s����7�w0   �� 7wwws7�w333�   333s�www3330 3333337w3333333333333333333333333333333333333333333333333333333333333333333	NumGlyphsOnClickSpeedButton2Click  TEditEClienteLeftTopWidth� HeightReadOnly	TabOrder   TEditERifInternoLefthTop(Width� HeightTabOrder   	TComboBoxCBTipoLeftKTop6Width� Height
ItemHeightTabOrderItems.Stringsricerca direttaricerca mistaricerca d'archiviopubblicazione su stamparicerca internazionalealtro   	TGroupBox	GroupBox1LeftTop�Width� Height*CaptionSede operativaTabOrder TDBGridDBGrid2LeftTopWidth� Height
DataSourceDsQSediOptionsdgColumnResizedgTabsdgConfirmDeletedgCancelOnExit TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameDescrizioneWidth� Visible	     TDateEdit97DEDataPrevChiusLeft� Top�WidthvHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderColorCalendar.ColorValidclBlueDayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanClear
doCanPopup
doIsMaskeddoShowCanceldoShowToday   TDateEdit97DEDataPrevPresLeft� Top�WidthvHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder	ColorCalendar.ColorValidclBlueDayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanClear
doCanPopup
doIsMaskeddoShowCanceldoShowToday   	TCheckBox
CBInBudgetLeftTopWidthUHeightCaption	In budgetChecked	State	cbCheckedTabOrder
  TDBGridDBGrid3LeftKTopOWidth� Height� 
DataSourceDsQStatiTabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameStatoRicReadOnly	Title.CaptionStatoWidth`Visible	    TPanelPanel1Left� Top WidthZHeighty
BevelOuterbvNoneTabOrder TToolbarButton97BitBtn1LeftTopWidthUHeight#CaptionOK
Glyph.Data
z  v  BMv      6  (               @                     ��� � � @�h ��� TQI �? v�v ��� ('' yґ E�^ qm^ ��� $�M ��� _�w \� ��� ��� �ټ \�d =<9 v�� 4�N :�] |{s T�s _^^ ��x ��� ,�X �Ʀ b�s jɃ ��� I�u ��� O�j ��� i|m ��� ��� *�M ��� ��� ��� 421 D�f S�` �� 7�^ JIC 9�U r� |�� _wc nnm U�l k� ��� ��� 0�W J�p ��� f�l `�l wċ %�G � �° T�q ��� e�y ��p ��� V�y qЊ [XN [�y ��� ��� M�n �Ė ��� 2�T urq d�} =�[ A�` ��� ��� ��� ��� ��� ��� F�p ��� H�c �Ǔ ��� m�v n� dЁ N�u C�h x�� V�o *�R �� ��� ?�b ~~{ o�p 2�P ��� �̹ 1�Z ��� H�_ ��� ;�c Ļ� g�o w͎ ��� U�z O�g ��� ]�r r�� ��� ��� \�o ��� c`a 0�V M�p F�f [�w �ȶ ��� 8�Y I�[ ��� ��u ��� sɉ 3�] ��� Y�o ��� ��� 3�Z �á mą i�z @�j Q�u [YY I�m ��� ��� ��� ��� (�Q ��� ?�c Z�~ ��� e� {�z b�s ��� IIH B�a yvu ��� l͆ ��� p�� t�� �ϼ Ŀ� +�V <�X J�k ��� |�� ��� h�~ <�a I�s 11/ ��� ��� E�n O�r k�m ��} ��� _�y e�w ��� ;�d S�y cxf ��� ��� |̑ ɿ� 7�Z =�] e|j ��� ��� Q�l }�� zxp ��� {�� ��� ��� &�K +�O L�a ��� ��� Y�t �Ɵ b�x ��� ��� ��� U�v ��� S�n \�v |Ǐ ��� ��� �˽ ��� ��� VTK ��� �ɼ ƽ� 7�_ A�j B�] ��� W�| ��� ��� ��� .9��		��9.�/Pz��z�/�.�V}��uu���}���4��>?����?�>E_4p�����?ih~�L�����=<N�uy�0�`�h��L�u�%N�s��3yo�n��$��?L���d�<+�3�nnn�`���ŧ��s��l���nn�nףR�yy�Rl@���vB�U­n�HQ�'���y3�R�ґJ#5����Q\�''���3�0�#J���ח�uy�\�''����ӯD��t��G�˥333uޤ'���l�&rF2��wk��O�>��3��㗗�XY�^���{�W"MgR�>>,��-�b1�F��澛�|


M���-���F���!I;�C����|���Z]��А���q��j�cccc�C�;�:����[�A��*SSSS*��;I��%���a�A��������fʜЕ���_�� ����m68���x}=(7��Te�=}x���K�.))ۍK����۱��.۱�OnClickBitBtn1Click  TToolbarButton97BAnnullaLeftTop8WidthUHeight#CaptionAnnulla
Glyph.Data
z  v  BMv      6  (               @                     ��� � � ��} � Ze� ��� =<: ��� ��� OO� ��� ba] 47� ��� ef� bb� ��~ �û xx� ~�� ((' ��� 76� NN� ��� ��� ��� SRI tt� mmm %%� LS� ��� ��� cb� ��w cc� CF� XX� ?G� VV� ��� ��� ��� �к ��� 330 ml� ��� ��� ||� ��� ��� HHH {zs pn] -.� (*� jl� aa� Z[� ��� ��� ��� oo� ��o sr� VZ� ��� ��� ��� CC� ��� ��� ||� ��� 38� MM� 00� �ŭ ss� ��� T^� QQ� ii� -2� GK� ��� ��� ��� ��� 4;� __� YZW ``� II� w|� ��� ��� ��� ;>� ��� ��� JJ� YWM ��� VV� ��� \\� ��� ��� dm� ��� ��� nn� )-� ##� ��� ��� ��� �˻ AF� QW� @@� qq� -/� utm ??� II� NN� ��� ��� 9:� ��� ��� Ya� ll� 33� ��� ��� ~~� �ջ ))� ��� ��� ��� ��� ��� ��� ��� ��� hg� xw� ��� ��� BD� ��� KT� ��� `h� nn� ��� �Ǽ DG� NO� TZ� w{� JIB [[� ��� ��� ��t bb� ��� BJ� QQ� __� ||� ��� 9>� ��� ��� FE� ff� ��� 66� 00/ ��� �ٽ ��� //� �Ƿ \\[ �ö 99� ~~{ ww� ��� EF� NV� II� ��� QQ� ^^� ��� zz� 44� ��� ]d� SU� V]� ��� SS� ~~� �͸ 68� ��� ��� aa� ��� {{� ��� EJ� ��� ZZ� ^[� 8;� 8=� AK� ��� ��� ��� <;6 ��� 35� 55� 58� GG� YY� [^� bb� jj� rr� ��� ��� ��� VTK ))� ++� 01� yxn ��� ``� ��� ��� 6��6��/joo�/^R���M��M=+�7��ތ�ߦ��ш�z�+���!�_� �ӈ�pp�Ѥt�����i��V&��SSS��D���qiE7X?~�n�a�SS ����~����8���2��4�SS�������8E��J��;gn>Z SS�c�gW��9fL��*�ϊ~�n�{�(.�g㜥��P�Bh�����4\���e��|B$-ðh�إg��4�ԃ�e؊�Hu�$�׀��T��D�nnnn��؊��������=%�:�nnn����9�H�O����#k̭'�>�n��n�'���Ɂ
-�rP�__���>"5>�d`���CPr[,�]_������}�g��w��`�v�ry0�K�1���s��GN�y�[��<�K���������)q�[�	�<lxl���x�Q���)�������b�+lx�QQAm@��[�F���b+3U�F��[�ʹYIIY�ʚ[[E����[OnClickBAnnullaClick  TToolbarButton97SpeedButton6Left0Top`WidthHeight
Glyph.Data
�  �  BM�      6   (               t                  ��������������������������������������������������� ��������������������������������������������������� �����������޽�����������������{ikRQR��������������� ������������������ζ�޶��Ͻ���������901������������ ������������֞{�a1�qJ޶��a1�a1޶�������981��������� ����������yZ�a)�a)ƞ����ޖk�a1�Y)֞{���ƶ���������� ������ޮ��a)�a1�a1�i9ގc�a)�a1�a1�Y)�ǵ���RIB������ �������i9�i1�a1�a1Άc����qB�a1�a1�a1�yR������������ �������a1�i1�a1�a1�yZ���禄�a)�a1�i1�i1����Ͻ������ �������i1�i1�a1�a1�a1֮�����s�a)�i1�i1�����Ɯ����� �������qB�i9�a)�a)�a1�a)�ǽ����q9�i9�qB����ǽ������ ��������k�yB֎k����yR�a)ަ����ގc�q9�s������������ �����������c�c������ﾥ�������R�R������ƾ������� ���������������ﾔ��������������k������������������ ���������������������ץ�ל�ߵ��������������������� ��������������������������������������������������� ��������������������������������������������������� OnClickSpeedButton6Click   TDataSourceDsUsersDataSet
TUsers_oldLeftTopj  TDataSourceDsQSediDataSetQSediLeft(Top�   TADOLinkedQueryQ
ConnectionData.DB
Parameters 	UseFilterLeft� Top�   TADOLinkedQueryQSedi
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect * from Aziende 	UseFilterLeftTop  TADOLinkedTable
TUsers_old
ConnectionData.DB
CursorTypectStatic	AfterOpenTUsers_oldAfterOpen	TableNameUsersLeft|TopD TStringFieldTUsers_oldNominativo	FieldName
NominativoSize  TAutoIncFieldTUsers_oldID	FieldNameIDReadOnly	  TStringFieldTUsers_oldDescrizione	FieldNameDescrizioneSize2   	TADOQueryQStati
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect ID,StatoRicfrom EBC_StatiRic LeftTop� TAutoIncFieldQStatiID	FieldNameIDReadOnly	  TStringFieldQStatiStatoRic	FieldNameStatoRic   TDataSourceDsQStatiDataSetQStatiLeftTop�  	TADOQueryTUsers
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Strings'Select * from Users join UsersSoftwares#on Users.ID=UsersSoftwares.IDUtente3where (datarevoca is null and datascadenza is null)and UsersSoftwares.IDSoftware=1order by Nominativo Left TopP TStringFieldTUsersNominativo	FieldName
NominativoSize  TAutoIncFieldTUsersID	FieldNameIDReadOnly	  TStringFieldTUsersDescrizione	FieldNameDescrizioneSize2    