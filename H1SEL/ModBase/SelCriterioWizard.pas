unit SelCriterioWizard;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ComCtrls, ExtCtrls, dxTL, dxDBCtrl, dxDBGrid, dxCntner, Db, ADODB,
     StdCtrls, Buttons, DBCtrls, dxEditor, dxExEdtr, dxEdLib, TB97;

type
     TSelCriterioWizardForm = class(TForm)
          PanSituazione: TPanel;
          PanButtons: TPanel;
          PCUnico: TPageControl;
          TsTabella: TTabSheet;
          Panel10: TPanel;
          LTot: TLabel;
          Label5: TLabel;
          QTabelle: TADOQuery;
          DsQTabelle: TDataSource;
          DBGTabelle: TdxDBGrid;
          DBGTabelleDescTabella: TdxDBGridMaskColumn;
          Label1: TLabel;
          DBText1: TDBText;
          TSCampo: TTabSheet;
          DsQCampi: TDataSource;
          QCampi: TADOQuery;
          DBGCampi: TdxDBGrid;
          DBGCampiDescCampo: TdxDBGridMaskColumn;
          Label2: TLabel;
          DBText2: TDBText;
          TsOperatore: TTabSheet;
          QOperatori: TADOQuery;
          DsQOperatori: TDataSource;
          DBGOperatori: TdxDBGrid;
          DBGOperatoriDescOperatore: TdxDBGridMaskColumn;
          Label3: TLabel;
          DBText3: TDBText;
          Panel1: TPanel;
          Label4: TLabel;
          Label6: TLabel;
          Panel2: TPanel;
          Label7: TLabel;
          Label8: TLabel;
          TSValore: TTabSheet;
          Panel3: TPanel;
          Label9: TLabel;
          Label10: TLabel;
          dxPickEdit1: TdxPickEdit;
          dxDateEdit1: TdxDateEdit;
          dxEditStyleController1: TdxEditStyleController;
          dxEdit1: TdxEdit;
          dxSpinEdit1: TdxSpinEdit;
          QTable: TADOQuery;
          DsQTable: TDataSource;
          PanGrid: TPanel;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1Desc: TdxDBGridMaskColumn;
          Panel4: TPanel;
          Label11: TLabel;
          ECerca: TEdit;
          dxDBGrid1Desc2: TdxDBGridColumn;
          BFine: TToolbarButton97;
          BitBtn1: TToolbarButton97;
    BIndietro: TToolbarButton97;
    BAvanti: TToolbarButton97;
    Panel5: TPanel;
    SpeedButton1: TToolbarButton97;
          procedure FormShow(Sender: TObject);
          procedure BIndietroClick(Sender: TObject);
          procedure BAvantiClick(Sender: TObject);
          procedure PCUnicoChange(Sender: TObject);
          procedure QCampiBeforeOpen(DataSet: TDataSet);
          procedure QOperatoriBeforeOpen(DataSet: TDataSet);
          procedure FormCreate(Sender: TObject);
          procedure SpeedButton1Click(Sender: TObject);
          procedure QOperatoriAfterOpen(DataSet: TDataSet);
          procedure BFineClick(Sender: TObject);
          procedure BitBtn1Click(Sender: TObject);
     private
          procedure SistemaBottoni;
     public
          xPaginaAttiva: integer;
          xTabella, xCampo, xOperatore, xValore: string;
     end;

var
     SelCriterioWizardForm: TSelCriterioWizardForm;

implementation

uses ModuloDati, Main;

{$R *.DFM}

procedure TSelCriterioWizardForm.FormShow(Sender: TObject);
var i: integer;
begin
     //Grafica
     SelCriterioWizardForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel3.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel3.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel3.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel4.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel4.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel4.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel5.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel5.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel5.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel10.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel10.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel10.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     PanGrid.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     PanGrid.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     PanGrid.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     PanSituazione.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     PanSituazione.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     PanSituazione.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     PanButtons.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     PanButtons.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     PanButtons.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     for i := 0 to PCUnico.PageCount - 1 do PCUnico.Pages[i].TabVisible := False;
     PCUnico.ActivePageIndex := xPaginaAttiva;
     if xPaginaAttiva = 3 then PCUnicoChange(self);
     SistemaBottoni;
     ECerca.text := '';
end;

procedure TSelCriterioWizardForm.BIndietroClick(Sender: TObject);
begin
     PCUnico.ActivePageIndex := PCUnico.ActivePageIndex - 1;
     // aggiornamento situazione pulsanti avanti/indietro
     PCUnicoChange(self);
     SistemaBottoni;
end;

procedure TSelCriterioWizardForm.BAvantiClick(Sender: TObject);
begin
     PCUnico.ActivePageIndex := PCUnico.ActivePageIndex + 1;
     // se si cambiano dei valori...
     if (QCampi.active) and (QCampi.FieldByName('DescCampo').AsString <> xCampo) then begin
          xValore := '';
     end;

     // aggiornamento situazione pulsanti avanti/indietro
     PCUnicoChange(self);
     SistemaBottoni;
end;

procedure TSelCriterioWizardForm.PCUnicoChange(Sender: TObject);
var xCampo1, xCampo2: string;
begin
     if PCUnico.ActivePage = TsTabella then begin
          if xTabella <> '' then
               QTabelle.Locate('DescTabella', xTabella, []);
          DBGTabelle.SetFocus;
     end;
     if PCUnico.ActivePage = TSCampo then begin
          if QCampi.Active then QCampi.Close;
          QCampi.Open;
          if xCampo <> '' then
               QCampi.Locate('DescCampo', xCampo, []);
          DBGCampi.SetFocus;
     end;

     if PCUnico.ActivePage = TsOperatore then begin
          if QOperatori.Active then QOperatori.Close;
          QOperatori.Open;
          if xOperatore <> '' then
               QOperatori.Locate('DescOperatore', xOperatore, []);
          DBGOperatori.SetFocus;
     end;
     if PCUnico.ActivePage = TsValore then begin
          // se apri direttamente questa controlla che sia aperta QCampi
          if xValore <> '' then begin
               if not QCampi.Active then QCampi.Open;
               QCampi.Locate('DescCampo', xCampo, []);
          end;
          dxEdit1.visible := False;
          dxPickEdit1.visible := False;
          dxDateEdit1.visible := False;
          dxSpinEdit1.visible := False;
          PanGrid.visible := False;
          if QCampi.FieldbyName('TipoCampo').AsString = '' then begin
               dxEdit1.visible := True;
               if xValore <> '' then dxEdit1.text := xValore;
               dxEdit1.Top := 32;
               dxEdit1.SetFocus;
          end;
          if QCampi.FieldbyName('TipoCampo').AsString = 'boolean' then begin
               // carica valori
               dxPickEdit1.Items.Clear;
               dxPickEdit1.Items.Add(copy(QCampi.FieldbyName('ValoriPossibili').AsString, 1, pos(',', QCampi.FieldbyName('ValoriPossibili').AsString) - 1));
               dxPickEdit1.Items.Add(copy(QCampi.FieldbyName('ValoriPossibili').AsString, pos(',', QCampi.FieldbyName('ValoriPossibili').AsString) + 1, length(QCampi.FieldbyName('ValoriPossibili').AsString)));
               dxPickEdit1.Text := copy(QCampi.FieldbyName('ValoriPossibili').AsString, 1, pos(',', QCampi.FieldbyName('ValoriPossibili').AsString) - 1);
               if xValore <> '' then dxPickEdit1.text := xValore;
               dxPickEdit1.visible := True;
               dxPickEdit1.Top := 32;
               dxPickEdit1.SetFocus;
          end;
          if QCampi.FieldbyName('TipoCampo').AsString = 'date' then begin
               dxDateEdit1.visible := True;
               if xValore <> '' then dxDateEdit1.Date := StrToDate(xValore);
               dxDateEdit1.Top := 32;
               dxDateEdit1.SetFocus;
          end;
          if UpperCase(QCampi.FieldbyName('TipoCampo').AsString) = 'DATESP' then begin
               if UpperCase(QCampi.FieldbyName('Campo').AsString) = 'DATANASCITA' {compleanno} then begin
                    dxEdit1.visible := True;
                    dxEdit1.Text := copy(datetimetostr(Date), 1, 6) + 'xxxx';
                    dxEdit1.ReadOnly := True;
               end;
          end;
          if QCampi.FieldbyName('TipoCampo').AsString = 'integer' then begin
               if QCampi.FieldbyName('TabLookup').AsString <> '' then begin
                    if QCampi.FieldbyName('QueryLookup').AsString <> '' then begin
                         // selezione specificata da query (nuovi campi TabQuery)
                         // Campi della query (in questo ordine):
                         // - ID
                         // - Campo 1
                         // - Campo 2 (eventuale)
                         QTable.Close;
                         QTable.SQL.text := QCampi.FieldbyName('QueryLookup').AsString;
                         if QCampi.FieldbyName('SearchField').AsString <> '' then begin
                              QTable.SQL.Add(' and ' + QCampi.FieldbyName('SearchField').AsString + ' like :xDaCercare ');
                              QTable.Parameters[0].Value := '%' + ECerca.Text + '%';
                         end;
                         //Showmessage(QTable.SQL.GetText);
                         QTable.Open;
                         if xValore <> '' then begin
                              QTable.Locate('ID', xValore, []);
                         end;
                         dxDBGrid1.KeyField := QTable.Fields[0].FieldName;
                         // campo 1
                         xCampo1 := QTable.Fields[1].FieldName;
                         dxDBGrid1Desc.FieldName := xCampo1;
                         dxDBGrid1Desc.Caption := xCampo1;
                         // campo 2
                         if QTable.FieldCount = 3 then begin
                              xCampo2 := QTable.Fields[2].FieldName;
                              dxDBGrid1Desc2.FieldName := xCampo2;
                              dxDBGrid1Desc2.visible := True;
                              dxDBGrid1Desc2.Caption := xCampo2;
                              dxDBGrid1.ShowGroupPanel := True;
                         end else begin
                              dxDBGrid1Desc2.visible := False;
                              dxDBGrid1.ShowGroupPanel := False;
                         end;
                         PanGrid.Align := alClient;
                         PanGrid.visible := True;
                         ECerca.SetFocus;
                    end else begin
                         // --> tabella ad una colonna (vecchi campi TabQuery)
                         QTable.Close;
                         QTable.SQL.text := 'select * from ' + QCampi.FieldbyName('TabLookup').AsString;
                         QTable.Open;
                         if xValore <> '' then
                              QTable.Locate('ID', xValore, []);
                         dxDBGrid1Desc.FieldName := QCampi.FieldbyName('FieldLookup').AsString;
                         dxDBGrid1Desc.Caption := '';
                         PanGrid.Align := alClient;
                         PanGrid.visible := True;
                         dxDBGrid1.SetFocus;
                    end;
               end else begin
                    // intero normale
                    dxSpinEdit1.visible := True;
                    if xValore <> '' then dxSpinEdit1.Value := StrToFloat(xValore);
                    dxSpinEdit1.Top := 32;
                    dxSpinEdit1.SetFocus;
               end;
          end;
     end;
end;

procedure TSelCriterioWizardForm.QCampiBeforeOpen(DataSet: TDataSet);
begin
     QCampi.Parameters[0].Value := QTabelle.FieldByName('DescTabella').AsString;
end;

procedure TSelCriterioWizardForm.QOperatoriBeforeOpen(DataSet: TDataSet);
begin
     QOperatori.Parameters[0].Value := QCampi.FieldbyName('ID').asString;
end;

procedure TSelCriterioWizardForm.SistemaBottoni;
begin
     BIndietro.Enabled := PCUnico.ActivePageIndex > 0;
     BAvanti.Enabled := PCUnico.ActivePageIndex < PCUnico.PageCount - 1;
     //BFine.Enabled:=PCUnico.ActivePageIndex=PCUnico.PageCount-1;
end;

procedure TSelCriterioWizardForm.FormCreate(Sender: TObject);
begin
     xtabella := '';
     xCampo := '';
     xOperatore := '';
     xValore := '';
end;

procedure TSelCriterioWizardForm.SpeedButton1Click(Sender: TObject);
begin
     // cerca
     QTable.Close;
     if QCampi.FieldbyName('QueryLookup').AsString <> '' then begin
          QTable.SQL.text := QCampi.FieldbyName('QueryLookup').AsString;
          QTable.SQL.Add(' and ' + QCampi.FieldbyName('SearchField').AsString + ' like :xDaCercare ');
     end else begin
          QTable.SQL.text := 'select * from ' + QCampi.FieldbyName('TabLookup').AsString;
          QTable.SQL.Add(' where ' + QCampi.FieldbyName('FieldLookup').AsString + ' like :xDaCercare ');
     end;
     QTable.Parameters[0].Value := '%' + ECerca.Text + '%';
     QTable.Open;
end;

procedure TSelCriterioWizardForm.QOperatoriAfterOpen(DataSet: TDataSet);
begin
     QOperatori.Refresh
end;

procedure TSelCriterioWizardForm.BFineClick(Sender: TObject);
begin
     SelCriterioWizardForm.ModalResult := mrOK;
end;

procedure TSelCriterioWizardForm.BitBtn1Click(Sender: TObject);
begin
SelCriterioWizardForm.ModalREsult := mrCancel;
     PCUnico.ActivePageIndex := PCUnico.ActivePageIndex - 1;
     // aggiornamento situazione pulsanti avanti/indietro
     PCUnicoChange(self);
     SistemaBottoni;
end;

end.

