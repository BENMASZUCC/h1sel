unit Nazione;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, TB97, ExtCtrls;

type
     TNazioneForm = class(TForm)
          EAbbrev: TEdit;
          EDesc: TEdit;
          EArea: TEdit;
          Label1: TLabel;
          Label2: TLabel;
          Label3: TLabel;
          Label4: TLabel;
          EDescNaz: TEdit;
          ECodiceCF: TEdit;
          Label5: TLabel;
          Panel1: TPanel;
          BOk: TToolbarButton97;
          BAnnulla: TToolbarButton97;
          procedure BOkClick(Sender: TObject);
          procedure BAnnullaClick(Sender: TObject);
          procedure FormShow(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     NazioneForm: TNazioneForm;

implementation

uses Main;

{$R *.DFM}

procedure TNazioneForm.BOkClick(Sender: TObject);
begin
     NazioneForm.ModalResult := mrOk;
end;

procedure TNazioneForm.BAnnullaClick(Sender: TObject);
begin
     NazioneForm.ModalResult := mrCancel;
end;

procedure TNazioneForm.FormShow(Sender: TObject);
begin
//Grafica
     NazioneForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
end;

end.

