unit FrmMultiDBCheckBox;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ExtCtrls, RXCtrls, Db, DBTables, Buttons, ADODB, U_ADOLinkCl, TB97;

type
     TMultiDBCheckBoxFrame = class(TFrame)
          CLBox: TRxCheckListBox;
          PanTitolo: TPanel;
          Q_OLD: TQuery;
          Panel1: TPanel;
          Q: TADOLinkedQuery;
    SpeedButton3: TToolbarButton97;
          procedure SpeedButton3Click(Sender: TObject);
     private
          xArrayID: array[1..100] of integer;
     public
          xIDAnag: integer;
          xTabBase, xTabCorrelaz, xCampoVis, xForeignKey, xNomeCampo: string;
          procedure LoadCLBox;
          procedure SaveToDb;
     end;

     // Usage:
     // xIDAnag:  Anagrafica.ID
     // xTabBase: tabella di base (es. DispContratti)
     // xTabCorrelaz: tabella di correlazione (es. AnagDispContratti)
     // xCampoVis: quale campo della tabella di base si vuole visualizzare nella lista
     //            la lista verr� ordinata per questo campo
     // xForeignKey: foreign key della tabella di correlazione verso la tabella di base
     //              (es. IDDispContratto in AnagDispContratti)

implementation

uses uUtilsVarie, ModuloDati;

{$R *.DFM}

{ TMultiDBCheckBoxFrame }

procedure TMultiDBCheckBoxFrame.LoadCLBox;
var i: integer;
begin
     for i := 1 to 100 do xArrayID[i] := 0;
     Q.Close;
     Q.SQL.text := 'select ID,' + xCampoVis + ' from ' + xTabBase + ' order by ' + xCampoVis;
     Q.Open;
     if data.Global.fieldbyname('debughrms').AsBoolean then q.SaveToFile('TMultiDBCheckBoxFrame_1.sql');
     CLBox.Items.Clear;
     i := 1;
     while not Q.Eof do begin
          CLBox.Items.Add(Q.FieldByName(xCampoVis).asString);
          xArrayID[i] := Q.FieldByName('ID').asInteger;
          Q.Next;
          inc(i);
     end;
     // quali selezionati
     Q.Close;
     Q.SQL.text := 'select * from ' + xTabCorrelaz + ' where IDAnagrafica=' + IntToStr(xIDAnag);
     Q.Open;
     if data.Global.fieldbyname('debughrms').AsBoolean then q.SaveToFile('TMultiDBCheckBoxFrame_2.sql');
     while not Q.EOF do begin
          for i := 1 to 100 do begin
               if xArrayID[i] = Q.FieldByName(xForeignKey).asInteger then begin
                    CLBox.Checked[i - 1] := True;
                    break;
               end;
          end;
          Q.Next;
     end;
     Q.Close;
end;

procedure TMultiDBCheckBoxFrame.SaveToDb;
var i: integer;
begin
     Q.SQL.text := 'delete from ' + xTabCorrelaz + ' where IDAnagrafica=' + IntToStr(xIDAnag);
     Q.ExecSQL;
     //[/TONI20020929\]
     {     Q.SQL.text:='insert into '+xTabCorrelaz+' (IDAnagrafica,'+xForeignKey+') '+
               'values ('+IntToStr(xIDAnag)+',:xForeignKey)';}
     for i := 0 to CLBox.Items.Count - 1 do begin
          if CLBox.Checked[i] then begin
               Q.SQL.text := 'insert into ' + xTabCorrelaz + ' (IDAnagrafica,' + xForeignKey + ') ' +
                    'values (' + IntToStr(xIDAnag) + ',' + IntToStr(xArrayID[i + 1]) + ')';
               //[/TONI20020929\]FINE
               Q.ExecSQL;
          end;
     end;
end;

procedure TMultiDBCheckBoxFrame.SpeedButton3Click(Sender: TObject);
begin
     SaveToDb;
     //OpenTab('DispContratti', ['ID', 'DispContratto', 'DispContratto_ENG'], ['Tipologia di contratto', 'Tipologia di contratto (ENG)']);
     OpenTab(xTabBase, ['ID', xCampoVis, 'VisibileWeb'], [xNomeCampo, 'Visibile Web']);
     LoadCLBox;
end;

end.
