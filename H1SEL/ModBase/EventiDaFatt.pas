unit EventiDaFatt;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  TB97, Grids, DBGrids, ExtCtrls, Wall, DB;

type
  TEventiDaFattForm = class(TForm)
    Wallpaper8: TPanel;
    DBGrid1: TDBGrid;
    ToolbarButton971: TToolbarButton97;
    ToolbarButton972: TToolbarButton97;
    procedure ToolbarButton972Click(Sender: TObject);
    procedure ToolbarButton971Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EventiDaFattForm: TEventiDaFattForm;

implementation

uses DataModule;

{$R *.DFM}


procedure TEventiDaFattForm.ToolbarButton972Click(Sender: TObject);
begin
     close
end;

procedure TEventiDaFattForm.ToolbarButton971Click(Sender: TObject);
begin
     if Data.DsFattDett.State in [dsEdit,dsInsert] then begin
        Data.FattDettDescrizione.asString:='Gestione evento '+
             Data.EventiXClienteDescrizione.asString;
        Data.FattDettImporto.Value:=Data.EventiXClienteImportoLire.Value;
        Data.FattDett.Post;
     end else begin
        Data.FattDett.Insert;
        Data.FattDettDescrizione.asString:='Gestione evento '+
             Data.EventiXClienteDescrizione.asString;
        Data.FattDettImporto.Value:=Data.EventiXClienteImportoLire.Value;
        Data.FattDett.Post;
     end;
     Data.EventiXCliente.Edit;
     Data.EventiXClienteFatturata.Value:=True;
     Data.EventiXCliente.Post;
end;

end.
