object ContrattiAnnunciFrame: TContrattiAnnunciFrame
  Left = 0
  Top = 0
  Width = 632
  Height = 405
  TabOrder = 0
  object Panel93: TPanel
    Left = 0
    Top = 0
    Width = 632
    Height = 23
    Align = alTop
    Alignment = taLeftJustify
    Caption = '  Contratti in essere con testate - edizioni'
    Color = clGray
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
  end
  object Wallpaper34: TPanel
    Left = 0
    Top = 23
    Width = 632
    Height = 39
    Align = alTop
    TabOrder = 1
    object TbBContrNew: TToolbarButton97
      Left = 1
      Top = 1
      Width = 78
      Height = 37
      Caption = 'Nuovo contratto'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        33333333FF33333333FF333993333333300033377F3333333777333993333333
        300033F77FFF3333377739999993333333333777777F3333333F399999933333
        33003777777333333377333993333333330033377F3333333377333993333333
        3333333773333333333F333333333333330033333333F33333773333333C3333
        330033333337FF3333773333333CC333333333FFFFF77FFF3FF33CCCCCCCCCC3
        993337777777777F77F33CCCCCCCCCC3993337777777777377333333333CC333
        333333333337733333FF3333333C333330003333333733333777333333333333
        3000333333333333377733333333333333333333333333333333}
      NumGlyphs = 2
      Opaque = False
      WordWrap = True
      OnClick = TbBContrNewClick
    end
    object TbBContrDel: TToolbarButton97
      Left = 157
      Top = 1
      Width = 78
      Height = 37
      Caption = 'Elimina contratto'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333000000000
        3333333777777777F3333330F777777033333337F3F3F3F7F3333330F0808070
        33333337F7F7F7F7F3333330F080707033333337F7F7F7F7F3333330F0808070
        33333337F7F7F7F7F3333330F080707033333337F7F7F7F7F3333330F0808070
        333333F7F7F7F7F7F3F33030F080707030333737F7F7F7F7F7333300F0808070
        03333377F7F7F7F773333330F080707033333337F7F7F7F7F333333070707070
        33333337F7F7F7F7FF3333000000000003333377777777777F33330F88877777
        0333337FFFFFFFFF7F3333000000000003333377777777777333333330777033
        3333333337FFF7F3333333333000003333333333377777333333}
      NumGlyphs = 2
      Opaque = False
      WordWrap = True
      OnClick = TbBContrDelClick
    end
    object TbBContrMod: TToolbarButton97
      Left = 79
      Top = 1
      Width = 78
      Height = 37
      Caption = 'Modifica contratto'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
        000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
        00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
        F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
        0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
        FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
        FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
        0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
        00333377737FFFFF773333303300000003333337337777777333}
      NumGlyphs = 2
      Opaque = False
      WordWrap = True
      OnClick = TbBContrModClick
    end
  end
  object DBGrid48: TDBGrid
    Left = 0
    Top = 62
    Width = 632
    Height = 343
    Align = alClient
    DataSource = DsQAnnContratti
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'Testata'
        Width = 124
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Edizione'
        Width = 96
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TotaleModuli'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        Title.Caption = 'Tot.Moduli'
        Width = 57
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ModuliUtilizzati'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clPurple
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Title.Caption = 'Utilizzati'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clPurple
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 53
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DataStipula'
        Title.Caption = 'Data stipula'
        Width = 69
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DataScadenza'
        Title.Caption = 'Scadenza'
        Width = 68
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CostoSingoloLire'
        Title.Caption = 'Costo singolo (�.)'
        Width = 87
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CostoSingoloEuro'
        Title.Caption = 'Costo singolo (e)'
        Width = 84
        Visible = True
      end>
  end
  object QAnnContratti_OLD: TQuery
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select Ann_Contratti.* ,'
      
        '          Ann_Testate.Denominazione Testata, NomeEdizione Edizio' +
        'ne'
      'from Ann_Contratti,Ann_Edizioni,Ann_Testate'
      'where Ann_Contratti.IDEdizione=Ann_Edizioni.ID'
      '    and Ann_Edizioni.IDTestata=Ann_Testate.ID'
      'order by Ann_Testate.Denominazione, NomeEdizione'
      '')
    Left = 88
    Top = 272
    object QAnnContratti_OLDID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.Ann_Contratti.ID'
    end
    object QAnnContratti_OLDIDEdizione: TIntegerField
      FieldName = 'IDEdizione'
      Origin = 'EBCDB.Ann_Contratti.IDEdizione'
    end
    object QAnnContratti_OLDDataStipula: TDateTimeField
      FieldName = 'DataStipula'
      Origin = 'EBCDB.Ann_Contratti.DataStipula'
    end
    object QAnnContratti_OLDDataScadenza: TDateTimeField
      FieldName = 'DataScadenza'
      Origin = 'EBCDB.Ann_Contratti.DataScadenza'
    end
    object QAnnContratti_OLDTotaleModuli: TSmallintField
      FieldName = 'TotaleModuli'
      Origin = 'EBCDB.Ann_Contratti.TotaleModuli'
    end
    object QAnnContratti_OLDModuliUtilizzati: TSmallintField
      FieldName = 'ModuliUtilizzati'
      Origin = 'EBCDB.Ann_Contratti.ModuliUtilizzati'
    end
    object QAnnContratti_OLDCostoSingoloLire: TFloatField
      FieldName = 'CostoSingoloLire'
      Origin = 'EBCDB.Ann_Contratti.CostoSingoloLire'
      DisplayFormat = '#,###'
    end
    object QAnnContratti_OLDCostoSingoloEuro: TFloatField
      FieldName = 'CostoSingoloEuro'
      Origin = 'EBCDB.Ann_Contratti.CostoSingoloEuro'
      DisplayFormat = '#,###.##'
    end
    object QAnnContratti_OLDTestata: TStringField
      FieldName = 'Testata'
      Origin = 'EBCDB.Ann_Testate.Denominazione'
      FixedChar = True
      Size = 50
    end
    object QAnnContratti_OLDEdizione: TStringField
      FieldName = 'Edizione'
      Origin = 'EBCDB.Ann_Edizioni.NomeEdizione'
      FixedChar = True
      Size = 50
    end
  end
  object DsQAnnContratti: TDataSource
    DataSet = QAnnContratti
    Left = 88
    Top = 240
  end
  object QAnnContratti: TADOLinkedQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select Ann_Contratti.* ,'
      
        '          Ann_Testate.Denominazione Testata, NomeEdizione Edizio' +
        'ne'
      'from Ann_Contratti,Ann_Edizioni,Ann_Testate'
      'where Ann_Contratti.IDEdizione=Ann_Edizioni.ID'
      '    and Ann_Edizioni.IDTestata=Ann_Testate.ID'
      'order by Ann_Testate.Denominazione, NomeEdizione'
      '')
    UseFilter = False
    Left = 88
    Top = 208
  end
end
