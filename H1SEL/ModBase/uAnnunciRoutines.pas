unit uAnnunciRoutines;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Buttons, DB, DBClient, Grids, DBGrids, ADODB, Mask, DBCtrls;

function CreaFileHTMLAnnuncio(ConnString, LogPath, xIDTemplate, xIDAnnuncio, xIDAnnEdizData: string): string;
procedure RigeneraFeedWeb;

implementation

uses ModuloDati4, uUtilsVarie;

procedure RigeneraFeedWeb;
begin
     //Update Flag per rigenerare Feed
     DataAnnunci.QTemp.Close;
     DataAnnunci.QTemp.SQL.text := 'Update Global set FlagRigeneraFeed=1';
     DataAnnunci.QTemp.ExecSQL;
end;

function CreaFileHTMLAnnuncio(ConnString, LogPath, xIDTemplate, xIDAnnuncio, xIDAnnEdizData: string): string;
var QTemplate, QTemp, QInsAnnuncio: TADOQuery;
     DB: TADOConnection;
     xDebug, xTestoSL: TStringList;
     i, n, j: integer;
     xFileDebug, xOperation, xTestoTemplate, xQuery, xQueryResult: string;
     NewStream: TMemoryStream;

const TagInizio = '�'; //codice ascii alt+170 ,   prima c'era # ma faceva del casino quando si mettono i colori esadecimali nell'html
const TagFine = '�';

begin
     //Gestione Template
     DataAnnunci.QTemp.Close;
     DataAnnunci.QTemp.SQL.text := 'select Top 1 ID from Ann_TemplateHTML where principale=1';
     DataAnnunci.QTemp.Open;
     if DataAnnunci.QTemp.RecordCount > 0 then
          xIDTemplate := DataAnnunci.QTemp.FieldByName('ID').asstring
     else begin
          DataAnnunci.QTemp.Close;
          DataAnnunci.QTemp.SQL.text := 'select ID from Ann_TemplateHTML';
          DataAnnunci.QTemp.Open;
          if DataAnnunci.QTemp.RecordCount > 0 then begin
               if DataAnnunci.QTemp.RecordCount = 1 then
                    xIDTemplate := DataAnnunci.QTemp.FieldByName('ID').asstring
               else
                    xIDTemplate := OpenSelFromTab('Ann_TemplateHTML', 'ID', 'Descrizione', 'Template HTML', '', False);
          end;
     end;

     // crea l'HTML dell'annuncio, da template
     xDebug := TStringList.create;
     xFileDebug := LogPath + '\Debug_CreaFileAnnuncio.txt';
     xOperation := '0) inizio elaborazione ore ' + TimeToStr(now) + '. INPUT: ' + chr(13) +
          '   ConnString = ' + ConnString + chr(13) +
          '   LogPath = ' + LogPath + chr(13) +
          '   xIDTemplate = ' + xIDTemplate + chr(13) +
          '   xIDAnnuncio = ' + xIDAnnuncio + chr(13);
     xDebug.text := xDebug.text + xOperation + chr(13);
     if LogPath <> '' then xDebug.SaveToFile(xFileDebug);

     DB := TADOConnection.Create(nil);
     DB.ConnectionString := ConnString;
     DB.LoginPrompt := False;
     DB.Connected := True;

     QTemplate := TADOQuery.create(nil);
     QTemplate.Connection := DB;
     QTemplate.SQL.Text := 'select TemplateHTML from  Ann_TemplateHTML where ID=' + xIDTemplate;

     xOperation := '1) apertura tabella template';
     try
          QTemplate.Open;
          xTestoTemplate := QTemplate.FieldByName('TemplateHTML').asString;
          xDebug.text := xDebug.text + xOperation + ' = OK.' + chr(13); if LogPath <> '' then xDebug.SaveToFile(xFileDebug);
     except
          on e: Exception do begin
               xDebug.text := xDebug.text + xOperation + ' = ERRORE: ' + e.Message + chr(13); if LogPath <> '' then xDebug.SaveToFile(xFileDebug);
               Result := 'ERRORE' + chr(13) + xDebug.text;
          end;
     end;
     QTemplate.Close;

     // sostituzioni query con risultati
     QTemp := TADOQuery.create(nil);
     QTemp.Connection := DB;

     i := 1;
     while pos(TagInizio, xTestoTemplate) > 0 do begin
          xOperation := '2' + IntToStr(i) + ') sostituzione';
          //showmessage('#='+ inttostr(pos('#', xTestoTemplate)) + '   �=' + inttostr(pos('�', xTestoTemplate)));
          xQuery := copy(xTestoTemplate, pos(TagInizio, xTestoTemplate) + 1, pos(TagFine, xTestoTemplate) - pos(TagInizio, xTestoTemplate) - 1);

          try
               QTemp.Close;
               QTemp.SQL.clear;
               QTemp.SQL.Text := xQuery;
               xDebug.text := xDebug.text + xquery;
               // unico parametro: ID pubblicazione
               j := 0;
               while j <= QTemp.Parameters.Count - 1 do begin
                    if QTemp.Parameters[j].Name = 'x' then
                         QTemp.Parameters[j].Value := StrToIntDef(xIDAnnuncio, 0);
                    if QTemp.Parameters[j].Name = 'xIDAnnEdizData' then
                         QTemp.Parameters[j].Value := StrToIntDef(xIDAnnEdizData, 0);
                    j := j + 1;
               end;
               QTemp.Open;

               xQueryResult := '';
               if not QTemp.eof then
                    xQueryResult := QTemp.Fields[0].AsString; // sempre e solo il primo campo

               xTestoTemplate := StringReplace(xTestoTemplate, TagInizio + xQuery + TagFine, xQueryResult, [rfReplaceAll]);
               xDebug.text := xDebug.text + xOperation + ' = OK.' + chr(13); if LogPath <> '' then xDebug.SaveToFile(xFileDebug);
          except
               on e: Exception do begin
                    xDebug.text := xDebug.text + xOperation + ' = ERRORE: ' + e.Message + chr(13); if LogPath <> '' then xDebug.SaveToFile(xFileDebug);
                    Result := 'ERRORE' + chr(13) + xDebug.text;
                    showmessage(e.message);
               end;
          end;

          inc(i);
     end;

     xDebug.text := xDebug.text + '2-FINE) fine sostituzione. Risultato:' + chr(13) + xTestoTemplate + chr(13); if LogPath <> '' then xDebug.SaveToFile(xFileDebug);

     NewStream := TMemoryStream.Create();
     xTestoSL := TStringList.create;
     xTestoSL.text := xTestoTemplate;
     //showmessage(  xTestoSL.text );
     xTestoSL.SaveToStream(NewStream);

     xOperation := '3) salvataggio HTML nell''annuncio';
     QInsAnnuncio := TADOQuery.create(nil);
     QInsAnnuncio.Connection := DB;    
     QInsAnnuncio.close;
     QInsAnnuncio.SQL.Text := 'select ID,FileAnnuncioHTML,FileTypeHTML from Ann_Annunci where ID=' + xIDAnnuncio;
     try
          QInsAnnuncio.Open;
          QInsAnnuncio.Edit;
          TBlobField(QInsAnnuncio.FieldByName('FileAnnuncioHTML')).LoadFromStream(NewStream);
          QInsAnnuncio.FieldByName('FileTypeHTML').Value := 'text/html';
          QInsAnnuncio.Post;

          xDebug.text := xDebug.text + xOperation + ' = OK.' + chr(13); if LogPath <> '' then xDebug.SaveToFile(xFileDebug);
     except
          on e: Exception do begin
               xDebug.text := xDebug.text + xOperation + ' = ERRORE: ' + e.Message + chr(13); if LogPath <> '' then xDebug.SaveToFile(xFileDebug);
               Result := 'ERRORE' + chr(13) + xDebug.text;
          end;
     end;
     QInsAnnuncio.close;
     QInsAnnuncio.free;

     // fine
     DB.Connected := False;
     DB.Free;
end;

end.

