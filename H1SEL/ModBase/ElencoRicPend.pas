//[TONI20021003]DEBUGOK
unit ElencoRicPend;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, Grids, DBGrids, StdCtrls, Buttons, DBTables, ExtCtrls, dxDBTLCl,
     dxGrClms, dxTL, dxDBCtrl, dxDBGrid, dxCntner, ADODB, U_ADOLinkCl, TB97;

type
     TElencoRicPendForm = class(TForm)
          Panel1: TPanel;
          DsQRicAttive: TDataSource;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1Progressivo: TdxDBGridMaskColumn;
          dxDBGrid1Cliente: TdxDBGridMaskColumn;
          dxDBGrid1DataInizio: TdxDBGridDateColumn;
          dxDBGrid1NumRicercati: TdxDBGridMaskColumn;
          dxDBGrid1Ruolo: TdxDBGridMaskColumn;
          dxDBGrid1Stato: TdxDBGridMaskColumn;
          dxDBGrid1StipendioLordo: TdxDBGridMaskColumn;
          dxDBGrid1ID: TdxDBGridMaskColumn;
          dxDBGrid1IDCliente: TdxDBGridMaskColumn;
          QRicAttive_Old: TADOLinkedQuery;
          dxDBGrid1Column10: TdxDBGridColumn;
          CBElencoRicPend: TCheckBox;
          Panel2: TPanel;
          BOk: TToolbarButton97;
          BAnnulla: TToolbarButton97;
          QRicAttive: TADOQuery;
    txtRif: TEdit;
    txtCliente: TEdit;
    Label1: TLabel;
    Label2: TLabel;
          procedure FormShow(Sender: TObject);
          procedure CBElencoRicPendClick(Sender: TObject);
          procedure BOkClick(Sender: TObject);
          procedure BAnnullaClick(Sender: TObject);
          procedure dxDBGrid1KeyPress(Sender: TObject; var Key: Char);
          procedure dxDBGrid1DblClick(Sender: TObject);
          procedure dxDBGrid1Click(Sender: TObject);
          procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
     private
          { Private declarations }
     public
          cliente: string;
          xall: boolean;
          { Public declarations }
     end;

var
     ElencoRicPendForm: TElencoRicPendForm;

implementation
//[/TONI20020726\]
uses ModuloDati, ModuloDati4, Main;
//[/TONI20020726\]FINE

{$R *.DFM}

procedure TElencoRicPendForm.FormShow(Sender: TObject);
begin
     //Grafica
     ElencoRicPendForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     Caption := '[S/9] - ' + caption;
     //dxDBGrid1.Filter.Add(dxDBGrid1Stato,'attiva','attiva');
     if not QRicAttive.Active then QRicAttive.Open;

     if cliente <> '' then
          QRicAttive.Locate('Cliente', cliente, []);


        dxDBGrid1.FocusedNode.Selected := False;

end;

//[TONI20021003]AGGIUTNA

procedure TElencoRicPendForm.CBElencoRicPendClick(Sender: TObject);
var xsearch: Tadoquery;
     j: integer;
     xfilter: string;
begin
     if (CBElencoRicPend.Visible = True) then begin
          //Ricerca solo clienti attivi
          if (xall = true) then begin
               xsearch := TADOQuery.Create(nil);
               xsearch.Connection := Data.DB;
               xsearch.SQL.Text := 'select * from EBC_StatiRic where StatoRic=''attiva'' or IDTipoStatoRic=1';
               xsearch.Open;
               j := 0;
               xfilter := '';
               while not xsearch.eof do begin
                    if j > 0 then xfilter := xfilter + ' OR ';
                    xfilter := xfilter + 'Stato=''' + xsearch.fieldbyname('StatoRic').asstring + '''';
                    j := j + 1;
                    xsearch.Next;
               end;
               xsearch.Close;
               xsearch.Free;
               QRicAttive.filter := xfilter;
               QRicAttive.Filtered := True;
               CBElencoRicPend.Checked := True;
               QRicAttive.Close;
               QRicAttive.Open;
               xall := False;
               txtRif.text := '';
               txtCliente.text := '';
          end else begin
               QRicAttive.filter := '';
               QRicAttive.Filtered := False;
               CBElencoRicPend.Checked := False;
               xall := True;
               QRicAttive.Close;
               QRicAttive.Open;
               txtRif.text := '';
               txtCliente.text := '';
          end;
     end;
end;

procedure TElencoRicPendForm.BOkClick(Sender: TObject);

begin

     if txtRif.text = '' then
     begin
        ShowMessage('E'' necessario selezionare una commessa');
        Exit;
     end;
     if MessageDlg('Sei sicuro di voler inserire la commessa "' + dxDBGrid1.ColumnByFieldName('Progressivo').Field.DisplayText + '" ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then
     begin
        ElencoRicPendForm.ModalResult := mrOk;
     end;

end;

procedure TElencoRicPendForm.BAnnullaClick(Sender: TObject);
begin
     ElencoRicPendForm.ModalResult := mrCancel;
end;

procedure TElencoRicPendForm.dxDBGrid1KeyPress(Sender: TObject;
     var Key: Char);
begin
     //if key = chr(13) then BOkClick(self);
end;

procedure TElencoRicPendForm.dxDBGrid1DblClick(Sender: TObject);
begin
     BOkClick(self);
end;

procedure TElencoRicPendForm.dxDBGrid1Click(Sender: TObject);
begin

     txtRif.text := dxDBGrid1.ColumnByFieldName('Progressivo').Field.DisplayText;
     txtCliente.text := dxDBGrid1.ColumnByFieldName('Cliente').Field.DisplayText;

end;

procedure TElencoRicPendForm.FormCloseQuery(Sender: TObject;
     var CanClose: Boolean);
begin

  {   if ModalResult = mrOK then begin
          if dxDBGrid1.SelectedCount = 0 then begin
               ShowMessage('E'' necessario selezionare un progetto');
               CanClose := false;
          end;
     end;
     }
end;

end.

