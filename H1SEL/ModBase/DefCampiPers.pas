unit DefCampiPers;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, ExtCtrls, dxCntner, dxEditor, dxExEdtr, dxEdLib,
     TB97;

type
     TDefCampiPersForm = class(TForm)
          Label1: TLabel;
          Label2: TLabel;
          Label3: TLabel;
          Label4: TLabel;
          Label5: TLabel;
          ENomeCampo: TEdit;
          Label6: TLabel;
          ELabelCampo: TEdit;
          Label7: TLabel;
          CBTipoCampo: TComboBox;
          SELunghezza: TdxSpinEdit;
          MNote: TdxMemo;
          Bevel1: TBevel;
          CBRicerche: TCheckBox;
          Panel1: TPanel;
          BitBtn1: TToolbarButton97;
          BAnnulla: TToolbarButton97;
    Label8: TLabel;
    SEOrdine: TdxSpinEdit;
          procedure CBTipoCampoChange(Sender: TObject);
          procedure BitBtn1Click(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure ENomeCampoExit(Sender: TObject);
          procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
          procedure BAnnullaClick(Sender: TObject);
     private
          { Private declarations }
     public
          xTipo: string;
     end;

var
     DefCampiPersForm: TDefCampiPersForm;

implementation

uses ModuloDati, Main;

{$R *.DFM}

procedure TDefCampiPersForm.CBTipoCampoChange(Sender: TObject);
begin
     if CBTipoCampo.text = 'stringa' then begin
          SELunghezza.Enabled := True;
          SELunghezza.Color := clWindow;
     end else begin
          SELunghezza.Value := 0;
          SELunghezza.Enabled := False;
          SELunghezza.Color := clBtnFace;
     end;
end;

procedure TDefCampiPersForm.BitBtn1Click(Sender: TObject);
var xTabella, xDicTipoCampo, xTipocampo, xLunghcampo, xDescTabella: string;
begin
     DefCampiPersForm.ModalResult := mrOk;
     if (ENomeCampo.Text = '') or (ELabelCampo.text = '') or (CBTipoCampo.Text = '') then begin
          showmessage('E'' necessario definire tutti i parametri per la creazione del campo');
          DefCampiPersForm.ModalResult := mrRetry;
     end
     else
          if (SELunghezza.IntValue <= 0) and (SELunghezza.Enabled = true) then begin
               showmessage('Non � possibile creare un campo stringa con lunghezza zero');
               DefCampiPersForm.ModalResult := mrRetry;
          end

          else
          begin

     // ambiti
               if xTipo = 'Anagrafica' then begin xTabella := 'AnagCampiPers'; xDescTabella := 'Dati personali (anagrafici)' end;
               if xTipo = 'Formazione' then begin xTabella := 'Form_CampiPers'; end;
               if xTipo = 'Organigramma' then begin xTabella := 'OrgCampiPers'; end;
               if xTipo = 'AnagraficaAziendale' then begin xTabella := 'CliCampiPers'; end;
               if xTipo = 'Annunci' then begin xTabella := 'Ann_CampiPers'; end;
               if xTipo = 'Commesse' then begin xTabella := 'Comm_CampiPers'; end;

     // tipi campi
               if CBTipoCampo.text = 'stringa' then begin xDicTipoCampo := 'varchar'; xTipocampo := 'varchar'; xLunghcampo := '(' + FloatToStr(SELunghezza.value) + ')' end;
               if CBTipoCampo.text = 'data' then begin xDicTipoCampo := 'datetime'; xTipocampo := 'datetime'; xLunghcampo := '' end;
               if CBTipoCampo.text = 'booleano' then begin xDicTipoCampo := 'bit default 0'; xTipocampo := 'boolean'; xLunghcampo := '' end;
               if CBTipoCampo.text = 'intero' then begin xDicTipoCampo := 'int'; xTipocampo := 'integer'; xLunghcampo := '' end;
               if CBTipoCampo.text = 'decimale' then begin xDicTipoCampo := 'float'; xTipocampo := 'float'; xLunghcampo := '' end;
               if CBTipoCampo.text = 'testo' then begin xDicTipoCampo := 'text'; xTipocampo := 'text'; xLunghcampo := '' end;

     // inserimento campi
               with data.QTemp do begin
                    Close;
                    SQL.Clear;
                    SQL.Add('if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id ' +
                         'and sysobjects.id = object_id(''' + xTabella + ''') and syscolumns.name = ''' + ENomeCampo.Text + ''') BEGIN');
          // creazione campo nella tabella giusta
                    SQL.Add('alter table ' + xTabella + ' add ' + ENomeCampo.Text + ' ' + xDicTipoCampo + ' ' + xLunghcampo + ' NULL');
          // definizione
                    SQL.Add('insert into TabCampiPers (Ambito,NomeCampo,LabelCampo,TipoCampo,SizeCampo,Note,Ordine)');
                    SQL.Add('values (''' + xTipo + ''',''' + ENomeCampo.Text + ''',''' + ELabelCampo.text + ''',''' + xTipocampo + ''',''' + FloatToStr(SELunghezza.value) + ''',''' + MNote.text + ''','+FloatToStr(SEOrdine.value)+')');
          // ricerche
                    if xTipo = 'Anagrafica' then begin
                         if CBRicerche.Checked then begin
                              if xTipocampo = 'varchar' then begin
                                   SQL.Add('INSERT INTO TabQuery(Tabella, DescTabella, Campo, DescCampo)');
                                   SQL.Add('VALUES(''' + xTabella + ''',''' + xDescTabella + ''',''' + ENomeCampo.Text + ''',''' + ELabelCampo.text + ''')');
                                   SQL.Add('INSERT INTO TabQueryOperatori(IDTabQuery, Operatore, DescOperatore)');
                                   SQL.Add('SELECT @@IDENTITY,''like'',''contiene la parola''');
                              end;
                              if xTipocampo = 'datetime' then begin
                                   SQL.Add('DECLARE @IdTabQuery int');
                                   SQL.Add('INSERT INTO TabQuery(Tabella, DescTabella, Campo, DescCampo)');
                                   SQL.Add('VALUES(''' + xTabella + ''',''' + xDescTabella + ''',''' + ENomeCampo.Text + ''',''' + ELabelCampo.text + ''')');
                                   SQL.Add('select @IdTabQuery = @@IDENTITY');
                                   SQL.Add('INSERT INTO TabQueryOperatori(IDTabQuery, Operatore, DescOperatore)');
                                   SQL.Add('SELECT @IdTabQuery,''>='',''maggiore o uguale a''');
                                   SQL.Add('INSERT INTO TabQueryOperatori(IDTabQuery, Operatore, DescOperatore)');
                                   SQL.Add('SELECT @IdTabQuery,''<='',''minore o uguale a''');
                              end;
                              if xTipocampo = 'boolean' then begin
                                   SQL.Add('INSERT INTO TabQuery(Tabella, DescTabella, Campo, DescCampo, ValoriPossibili, TipoCampo)');
                                   SQL.Add('VALUES(''' + xTabella + ''',''' + xDescTabella + ''',''' + ENomeCampo.Text + ''',''' + ELabelCampo.text + ''',''s�,no'',''boolean'')');
                                   SQL.Add('INSERT INTO TabQueryOperatori(IDTabQuery, Operatore, DescOperatore)');
                                   SQL.Add('SELECT @@IDENTITY,''='',''=''');
                              end;
                              if (xTipocampo = 'integer') or (xTipocampo = 'float') then begin
                                   SQL.Add('DECLARE @IdTabQuery int');
                                   SQL.Add('INSERT INTO TabQuery(Tabella, DescTabella, Campo, DescCampo, TipoCampo)');
                                   SQL.Add('VALUES(''' + xTabella + ''',''' + xDescTabella + ''',''' + ENomeCampo.Text + ''',''' + ELabelCampo.text + ''',''integer'')');
                                   SQL.Add('select @IdTabQuery = @@IDENTITY');
                                   SQL.Add('INSERT INTO TabQueryOperatori(IDTabQuery, Operatore, DescOperatore)');
                                   SQL.Add('SELECT @IdTabQuery,''>='',''maggiore o uguale a''');
                                   SQL.Add('INSERT INTO TabQueryOperatori(IDTabQuery, Operatore, DescOperatore)');
                                   SQL.Add('SELECT @IdTabQuery,''<='',''minore o uguale a''');
                              end;
                         end;
                    end;
                    SQL.Add('END');

          // #### DEBUG #####
          //SQL.SaveToFile('c:\dev\h1\ado\QCreaCampo.txt');

                    try
                         ExecSQL;
                    finally
                         ShowMessage('campo regolarmente creato');
                    end;
               end;
          end;
end;

procedure TDefCampiPersForm.FormShow(Sender: TObject);
begin
//Grafica
     DefCampiPersForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     CBRicerche.visible := xTipo = 'Anagrafica';
end;

procedure TDefCampiPersForm.ENomeCampoExit(Sender: TObject);
begin
     ENomeCampo.Text := StringReplace(ENomeCampo.text, ' ', '', [rfReplaceAll]);
end;


procedure TDefCampiPersForm.FormCloseQuery(Sender: TObject;
     var CanClose: Boolean);
begin
     if ModalResult = mrRetry then
          canclose := false;
end;

procedure TDefCampiPersForm.BAnnullaClick(Sender: TObject);
begin
     DefCampiPersForm.ModalResult := mrCancel;
end;

end.

