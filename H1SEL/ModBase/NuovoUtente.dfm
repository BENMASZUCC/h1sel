object NuovoUtenteForm: TNuovoUtenteForm
  Left = 302
  Top = 221
  ActiveControl = ENominativo
  BorderStyle = bsDialog
  Caption = 'Informazioni nuovo utente'
  ClientHeight = 130
  ClientWidth = 280
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 280
    Height = 40
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object Label3: TLabel
      Left = 7
      Top = 5
      Width = 154
      Height = 39
      Caption = 'Nuovo utente per il sistema H1 abilitato al database'
      WordWrap = True
    end
    object BitBtn1: TBitBtn
      Left = 186
      Top = 4
      Width = 90
      Height = 32
      TabOrder = 0
      Kind = bkOK
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 40
    Width = 280
    Height = 90
    Align = alClient
    BevelOuter = bvLowered
    TabOrder = 1
    object Label1: TLabel
      Left = 8
      Top = 34
      Width = 96
      Height = 13
      Caption = 'Nominativo (breve):'
    end
    object Label2: TLabel
      Left = 8
      Top = 58
      Width = 58
      Height = 13
      Caption = 'Descrizione:'
    end
    object Label4: TLabel
      Left = 8
      Top = 8
      Width = 76
      Height = 13
      Caption = 'Utente dominio:'
    end
    object EWinUser: TLabel
      Left = 112
      Top = 7
      Width = 46
      Height = 13
      Caption = 'EWinUser'
    end
    object ENominativo: TEdit
      Left = 110
      Top = 30
      Width = 104
      Height = 21
      MaxLength = 30
      TabOrder = 0
    end
    object EDescrizione: TEdit
      Left = 110
      Top = 54
      Width = 163
      Height = 21
      MaxLength = 50
      TabOrder = 1
    end
  end
end
