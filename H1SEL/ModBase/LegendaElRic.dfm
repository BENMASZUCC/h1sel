object LegendaElRicForm: TLegendaElRicForm
  Left = 312
  Top = 150
  BorderStyle = bsDialog
  Caption = 'Legenda e parametri'
  ClientHeight = 313
  ClientWidth = 488
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label11: TLabel
    Left = 4
    Top = 2
    Width = 390
    Height = 28
    Caption = 
      'NOTA: i seguenti valori sono GLOBALI.  E'#39' possibile settare gli ' +
      'stessi valori'#13#10'a livello di ogni singola commessa (entrando in c' +
      'ommessa, pagina "parametri")'
    Font.Charset = ANSI_CHARSET
    Font.Color = clPurple
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsItalic]
    ParentFont = False
  end
  object SpeedButton1: TSpeedButton
    Left = 456
    Top = 40
    Width = 20
    Height = 20
    Glyph.Data = {
      AA030000424DAA03000000000000360000002800000011000000110000000100
      1800000000007403000000000000000000000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FF00FFFFFFFFFFFFFFFFFFDEDFDEBDAEA5EFDFD6F7EFE7F7EFE7F7EFE7E7D7CE
      7B696B525152DEDFDEFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFF7F7EF
      E7DEF7F7F7E7DFDECEB6A5DEB6A5E7CFBDEFEFEFF7F7F7DECFC6393031CECFCE
      FFFFFFFFFFFFFFFFFF00FFFFFFF7F7EFF7F7EFF7F7EFD69E7BC66131C6714ADE
      B6A5C66131C66131DEB69CF7F7F7EFDFD6393831FFFFFFFFFFFFFFFFFF00FFFF
      FFF7E7DEF7F7F7CE795AC66129CE6129C69E84FFFFFFDE966BCE6131C65929D6
      9E7BF7F7F7C6B6B5848684FFFFFFFFFFFF00F7EFE7FFFFFFDEAE8CCE6129CE61
      31CE6131CE6939DE8E63CE6129CE6131CE6131C65929E7C7B5F7EFEF524942FF
      FFFFFFFFFF00F7DFD6FFF7F7CE6939CE6931CE6131CE6131CE8663F7E7D6D671
      42CE6131CE6131CE6131CE7952F7FFFFA5968CADAEADFFFFFF00FFEFEFF7DFCE
      CE6131D66931CE6131CE6131C6795AFFFFFFE7A684CE6129CE6131CE6931CE69
      31F7F7EFDECFBD9C9E9CFFFFFF00FFEFEFF7D7C6D66931D66931CE6131CE6131
      CE6131D6AE9CFFFFFFE79E73CE6129CE6931CE6931F7EFEFDECFC69C9E9CFFFF
      FF00FFEFEFFFE7DEE77142DE6939CE6129CE6129CE6131CE6129E7C7BDFFFFF7
      D67139D66939D67142FFF7F7D6C7BDB5B6B5FFFFFF00F7E7DEFFFFFFF79E6BEF
      7942D68E6BEFDFCEDE7952CE6129DEA684FFFFFFDE8E63DE7139E79E73FFFFFF
      A5968CFFFFFFFFFFFF00F7EFE7FFFFFFFFE7CEFF9E63E78E63EFEFEFFFEFE7EF
      BEA5FFF7EFF7EFEFE78652EF8652FFEFDEFFF7EFC6BEBDFFFFFFFFFFFF00FFFF
      FFF7E7DEFFFFFFFFE7C6FFBE84EFBE94EFE7DEE7E7E7EFE7DEFFB68CFF9E6BFF
      DFC6FFFFFFDECFC6FFFFFFFFFFFFFFFFFF00FFFFFFF7F7EFF7E7E7FFFFFFFFFF
      EFFFF7CEFFE7B5FFD7A5FFD79CFFDFB5FFF7EFFFFFFFEFDFDEEFEFEFFFFFFFFF
      FFFFFFFFFF00FFFFFFFFFFFFF7F7EFF7E7D6FFFFF7FFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFF7F7F7EFE7F7F7EFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF
      FFFFFFFFFFFFF7EFE7F7E7DEFFEFE7FFEFEFFFEFE7EFE7DEFFF7F7FFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFF00}
    OnClick = SpeedButton1Click
  end
  object BitBtn1: TBitBtn
    Left = 399
    Top = 3
    Width = 88
    Height = 34
    Caption = 'Chiudi'
    TabOrder = 0
    Kind = bkOK
  end
  object Panel5: TPanel
    Left = 2
    Top = 33
    Width = 382
    Height = 41
    BevelInner = bvSpace
    BevelOuter = bvLowered
    TabOrder = 1
    object Label13: TLabel
      Left = 38
      Top = 8
      Width = 182
      Height = 26
      Caption = 'n� di giorni dalla data di prima apertura della ricerca'
      WordWrap = True
    end
    object Label14: TLabel
      Left = 8
      Top = 8
      Width = 20
      Height = 16
      Caption = 'Ap'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label15: TLabel
      Left = 233
      Top = 8
      Width = 87
      Height = 26
      Caption = 'carattere rosso se pi� di giorni '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      WordWrap = True
    end
    object RxSpinEdit1: TRxSpinEdit
      Left = 325
      Top = 11
      Width = 50
      Height = 21
      TabOrder = 0
    end
  end
  object Panel6: TPanel
    Left = 2
    Top = 78
    Width = 382
    Height = 41
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 2
    object Label16: TLabel
      Left = 38
      Top = 8
      Width = 179
      Height = 26
      Caption = 
        'n� di giorni dall'#39'ultimo CONTATTO con un soggetto associato alla' +
        ' ricerca'
      WordWrap = True
    end
    object Label17: TLabel
      Left = 7
      Top = 8
      Width = 20
      Height = 16
      Caption = 'Uc'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 233
      Top = 8
      Width = 87
      Height = 26
      Caption = 'carattere rosso se pi� di giorni '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      WordWrap = True
    end
    object RxSpinEdit2: TRxSpinEdit
      Left = 325
      Top = 11
      Width = 50
      Height = 21
      TabOrder = 0
    end
  end
  object Panel7: TPanel
    Left = 2
    Top = 123
    Width = 382
    Height = 41
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 3
    object Label19: TLabel
      Left = 7
      Top = 8
      Width = 24
      Height = 16
      Caption = 'Col'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label20: TLabel
      Left = 38
      Top = 8
      Width = 179
      Height = 26
      Caption = 
        'n� di giorni dall'#39'ultimo COLLOQUIO con un soggetto associato all' +
        'a ricerca'
      WordWrap = True
    end
    object Label2: TLabel
      Left = 233
      Top = 8
      Width = 87
      Height = 26
      Caption = 'carattere rosso se pi� di giorni '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      WordWrap = True
    end
    object RxSpinEdit3: TRxSpinEdit
      Left = 325
      Top = 11
      Width = 50
      Height = 21
      TabOrder = 0
    end
  end
  object Panel1: TPanel
    Left = 2
    Top = 168
    Width = 271
    Height = 93
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 4
    object Label3: TLabel
      Left = 8
      Top = 7
      Width = 162
      Height = 13
      Caption = 'legenda colori colonna stato'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 48
      Top = 72
      Width = 175
      Height = 13
      Caption = 'verde: effettuato almeno un colloquio'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 48
      Top = 40
      Width = 214
      Height = 13
      Caption = 'arancione: candidati inseriti ma senza contatti'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 48
      Top = 56
      Width = 169
      Height = 13
      Caption = 'giallo: effettuato almeno un contatto'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label7: TLabel
      Left = 48
      Top = 24
      Width = 151
      Height = 13
      Caption = 'rosso: nessun candidato inserito'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Shape1: TShape
      Left = 8
      Top = 25
      Width = 35
      Height = 14
      Brush.Color = clRed
    end
    object Shape2: TShape
      Left = 8
      Top = 41
      Width = 35
      Height = 14
      Brush.Color = 33023
    end
    object Shape3: TShape
      Left = 8
      Top = 57
      Width = 35
      Height = 14
      Brush.Color = clYellow
    end
    object Shape4: TShape
      Left = 8
      Top = 73
      Width = 35
      Height = 14
      Brush.Color = clLime
    end
  end
  object Panel2: TPanel
    Left = 2
    Top = 266
    Width = 271
    Height = 46
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 5
    object Label8: TLabel
      Left = 8
      Top = 7
      Width = 205
      Height = 13
      Caption = 'legenda colore carattere Data Inizio'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label18: TLabel
      Left = 8
      Top = 24
      Width = 251
      Height = 13
      Caption = 'Rosso: superata o raggiunta data prevista di chiusura'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
  end
  object Q_OLD: TQuery
    DatabaseName = 'EBCDB'
    Left = 392
    Top = 72
  end
  object Q: TADOLinkedQuery
    Connection = Data.DB
    Parameters = <>
    UseFilter = False
    Left = 392
    Top = 104
  end
end
