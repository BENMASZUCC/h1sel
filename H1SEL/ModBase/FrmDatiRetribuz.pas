unit FrmDatiRetribuz;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, DBTables, Grids, DBGrids, TB97, ExtCtrls, RXDBCtrl, Aligrid, StdCtrls,
     Mask, DBCtrls, ADODB, U_ADOLinkCl, dxTL, dxDBCtrl, dxDBGrid, dxCntner;

type
     TDatiRetribuzFrame = class(TFrame)
          DsQDatiRetribuz: TDataSource;
          Panel1: TPanel;
          Panel2: TPanel;
          Panel74: TPanel;
          ToolbarButton971: TToolbarButton97;
          BMod: TToolbarButton97;
          BStorico: TToolbarButton97;
          Panel3: TPanel;
          Label1: TLabel;
          DsQTot: TDataSource;
          DBEdit1: TDBEdit;
          QDatiRetribuz: TADOLinkedQuery;
          QTot: TADOLinkedQuery;
          QDatiRetribuzID: TAutoIncField;
          QDatiRetribuzIDColonna: TAutoIncField;
          QDatiRetribuzData: TDateTimeField;
          QDatiRetribuzConsideraTot: TBooleanField;
          QDatiRetribuzCampo: TStringField;
          QDatiRetribuzValore: TFloatField;
          RxDBGrid1: TdxDBGrid;
          RxDBGrid1Campo: TdxDBGridColumn;
          RxDBGrid1Data: TdxDBGridColumn;
          RxDBGrid1Valore: TdxDBGridColumn;
          RxDBGrid1ConsideraTot: TdxDBGridColumn;
          procedure ToolbarButton971Click(Sender: TObject);
          procedure BModClick(Sender: TObject);
          procedure BStoricoClick(Sender: TObject);
          procedure QDatiRetribuz_old1BeforeOpen(DataSet: TDataSet);
          procedure QTotBeforeOpen(DataSet: TDataSet);
     private
          { Private declarations }
     public
          xIDEspLav: integer;
     end;

implementation

uses uUtilsVarie, ModuloDati, SelData, ModuloDati2, uASACand;

{$R *.DFM}

procedure TDatiRetribuzFrame.ToolbarButton971Click(Sender: TObject);
var xIDColonna: string;
begin
     if QDatiRetribuz.IsEmpty then xIDColonna := ''
          //[/TONI20020729\]
          //     else xIDColonna:=QDatiRetribuzID.AsString;
     else xIDColonna := QDatiRetribuz.FieldByName('ID').AsString;
     //[/TONI20020729\]FINE
     OpenSelFromTab('EspLavRetribColonne', 'ID', 'NomeColonna', 'Nome campo', xIDColonna);
     QDatiRetribuz.Close;
     QDatiRetribuz.Open;
end;

procedure TDatiRetribuzFrame.BModClick(Sender: TObject);
var xVal: string;
     xID: integer;
begin
     //[/TONI20020729\]
     {     xVal:=QDatiRetribuzValore.AsString;
          xID:=QDatiRetribuzID.Value;
          if not InputQuery(QDatiRetribuzCampo.Value,'valore:',xVal) then exit;}
     xVal := QDatiRetribuz.FieldByName('Valore').AsString;
     //[/GIULIO20020928\]
     if QDatiRetribuz.FieldByName('ID').Value <> null then
          xID := QDatiRetribuz.FieldByName('ID').Value
     else xID := 0;
     if not InputQuery(QDatiRetribuz.FieldbyName('Campo').Value, 'valore:', xVal) then exit;

     SelDataForm := TSelDataForm.create(self);
     SelDataForm.DEData.Date := Date;
     SelDataForm.ShowModal;
     if SelDataForm.ModalResult = mrOK then begin
          with Data do begin
               Qtemp.Close;
               {               Qtemp.SQL.text:='select ID from EspLavRetribuzioni '+
                                   'where IDColonna=:xIDColonna and IDEspLav=:xIDEspLAv';
                              Qtemp.ParambyName('xIDColonna').asInteger:=QDatiRetribuzIDColonna.Value;
                              Qtemp.ParambyName('xIDEspLav').asInteger:=Data.TEspLavID.Value;}
               Qtemp.SQL.text := 'select ID from EspLavRetribuzioni ' +
                    'where IDColonna=:xIDColonna: and IDEspLav=:xIDEspLAv:';
               Qtemp.ParambyName['xIDColonna'] := QDatiRetribuz.FieldByName('IDColonna').Value;
               if (Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 6)) = 'ADVANT') or (pos('MCS', UpperCase(Data.Global.FieldByName('NomeAzienda').Value)) > 0) then
                    Qtemp.ParamByname['xIDEspLav'] := ASASchedaCandForm.QEspLavID.Value
               else
                    Qtemp.ParambyName['xIDEspLav'] := Data.TEspLav.FieldByName('ID').Value;

               Qtemp.Open;
               Q1.Close;
               if Qtemp.IsEmpty then begin
                    {                    Q1.SQL.text:='insert into EspLavRetribuzioni (IDespLav,IDColonna,Valore,Data,ConsideraTot) '+
                                             'values (:xIDEspLav,:xIDColonna,:xValore,:xData,:xConsideraTot)';
                                        Q1.ParamByName('xIDEspLav').asInteger:=Data.TEspLavID.Value;
                                        Q1.ParamByName('xIDColonna').asInteger:=QDatiRetribuzIDColonna.Value;
                                        Q1.ParamByName('xValore').asFloat:=StrToFloat(xVal);
                                        Q1.ParamByName('xData').asDateTime:=SelDataForm.DEData.Date;
                                        Q1.ParamByName('xConsideraTot').asBoolean:=True;}
                    Q1.SQL.text := 'insert into EspLavRetribuzioni (IDespLav,IDColonna,Valore,Data,ConsideraTot) ' +
                         'values (:xIDEspLav:,:xIDColonna:,:xValore:,:xData:,:xConsideraTot:)';
                    if (Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 6)) = 'ADVANT') or (pos('MCS', UpperCase(Data.Global.FieldByName('NomeAzienda').Value)) > 0) then
                         Q1.ParamByname['xIDEspLav'] := ASASchedaCandForm.QEspLavID.Value
                    else
                         Q1.ParamByname['xIDEspLav'] := Data.TEspLav.FieldByName('ID').Value;
                    Q1.ParamByname['xIDColonna'] := QDatiRetribuz.FieldByName('IDColonna').Value;
                    Q1.ParamByname['xValore'] := StrToFloat(xVal);
                    Q1.ParamByname['xData'] := SelDataForm.DEData.Date;
                    Q1.ParamByname['xConsideraTot'] := 1;

               end else begin
                    {                    Q1.SQL.text:='update EspLavRetribuzioni set Valore=:xValore, Data=:xData '+
                                             'where ID='+QTemp.FieldByName('ID').asString;
                                        Q1.ParamByName('xValore').asFloat:=StrToFloat(xVal);
                                        Q1.ParamByName('xData').asDateTime:=SelDataForm.DEData.Date;}
                    Q1.SQL.text := 'update EspLavRetribuzioni set Valore=:xValore:, Data=:xData: ' +
                         'where ID=' + QTemp.FieldByName('ID').asString;
                    Q1.ParamByName['xValore'] := StrToFloat(xVal);
                    Q1.ParamByName['xData'] := SelDataForm.DEData.Date;
               end;
               DB.BeginTrans;
               try
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    QDatiRetribuz.Close;
                    QDatiRetribuz.Open;
                    QDatiRetribuz.Locate('ID', xID, []);
                    QTot.Close;
                    QTot.Open;
                    //[/TONI20020729\]FINE
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               end;
          end;
     end;
     SelDataForm.Free;
end;

procedure TDatiRetribuzFrame.BStoricoClick(Sender: TObject);
var xVal: string;
     xID: integer;
begin
     //[/TONI20020729\]
     {     xVal:=QDatiRetribuzValore.AsString;
          xID:=QDatiRetribuzID.Value;}
     xVal := QDatiRetribuz.FieldByName('Valore').AsString;
     xID := QDatiRetribuz.FieldByName('ID').Value;
     with Data do begin
          Qtemp.Close;
          {          Qtemp.SQL.text:='select ID from EspLavRetribuzioni '+
                         'where IDColonna=:xIDColonna and IDEspLav=:xIDEspLAv';
                    Qtemp.ParambyName('xIDColonna').asInteger:=QDatiRetribuzIDColonna.Value;
                    Qtemp.ParambyName('xIDEspLav').asInteger:=Data.TEspLavID.Value;}
          Qtemp.SQL.text := 'select ID from EspLavRetribuzioni ' +
               'where IDColonna=:xIDColonna: and IDEspLav=:xIDEspLAv:';
          Qtemp.ParambyName['xIDColonna'] := QDatiRetribuz.FieldByName('IDColonna').Value;
          if (Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 6)) = 'ADVANT') or (pos('MCS', UpperCase(Data.Global.FieldByName('NomeAzienda').Value)) > 0) then
               Qtemp.ParamByname['xIDEspLav'] := ASASchedaCandForm.QEspLavID.Value
          else
               Qtemp.ParamByname['xIDEspLav'] := Data.TEspLav.FieldByName('ID').Value;

          Qtemp.Open;
          Q1.Close;
          if Qtemp.IsEmpty then exit;
          {          Q1.SQL.text:='update EspLavRetribuzioni set ConsideraTot=:xConsideraTot '+
                         'where ID='+QTemp.FieldByName('ID').asString;
                    Q1.ParamByName('xConsideraTot').AsBoolean:=not QDatiRetribuzConsideraTot.Value;}
          Q1.SQL.text := 'update EspLavRetribuzioni set ConsideraTot=:xConsideraTot: ' +
               'where ID=' + QTemp.FieldByName('ID').asString;
          Q1.ParamByName['xConsideraTot'] := not QDatiRetribuz.FieldByName('ConsideraTot').Value;
          DB.BeginTrans;
          try
               Q1.ExecSQL;
               DB.CommitTrans;
               QDatiRetribuz.Close;
               QDatiRetribuz.Open;
               QDatiRetribuz.Locate('ID', xID, []);
               QTot.Close;
               QTot.Open;
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
          end;
     end;
     //[/TONI20020729\]FINE
end;

procedure TDatiRetribuzFrame.QDatiRetribuz_old1BeforeOpen(DataSet: TDataSet);
begin
     if Data.TEspLav.RecordCount > 0 then begin
          if (Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 6)) = 'ADVANT') or (pos('MCS', UpperCase(Data.Global.FieldByName('NomeAzienda').Value)) > 0) then
               {QDatiRetribuz.SQL.text := 'select EspLavRetribuzioni.ID, EspLavRetribColonne.ID IDColonna, ' +
                    '       EspLavRetribuzioni.Data, ConsideraTot, ' +
                    '       NomeColonna Campo, Valore ' +
                    'from EspLavRetribuzioni, EspLavRetribColonne ' +
                    'where EspLavRetribColonne.ID *= EspLavRetribuzioni.IDColonna ' +
                    'and IDEspLav=' + ASASchedaCandForm.QEspLavID.AsString}
               //modifica query di Thomas
               QDatiRetribuz.SQL.text := 'select EspLavRetribuzioni.ID, EspLavRetribColonne.ID IDColonna, ' +
                    '       EspLavRetribuzioni.Data, ConsideraTot, ' +
                    '       NomeColonna Campo, Valore' +
                    ' from EspLavRetribColonne left join EspLavRetribuzioni' +
                    ' on EspLavRetribColonne.ID = EspLavRetribuzioni.IDColonna' +
                    ' where IDEspLav=' + ASASchedaCandForm.QEspLavID.AsString
          else
               {QDatiRetribuz.SQL.text := 'select EspLavRetribuzioni.ID, EspLavRetribColonne.ID IDColonna, ' +
                    '       EspLavRetribuzioni.Data, ConsideraTot, ' +
                    '       NomeColonna Campo, Valore ' +
                    'from EspLavRetribuzioni, EspLavRetribColonne ' +
                    'where EspLavRetribColonne.ID *= EspLavRetribuzioni.IDColonna ' +
                    'and IDEspLav=' + Data.TEspLav.FieldByName('ID').asString}
               //modifica query di Thomas
               QDatiRetribuz.SQL.text := 'select EspLavRetribuzioni.ID, EspLavRetribColonne.ID IDColonna, ' +
                    '       EspLavRetribuzioni.Data, ConsideraTot, ' +
                    '       NomeColonna Campo, Valore ' +
                    ' from EspLavRetribColonne left join EspLavRetribuzioni' +
                    ' on EspLavRetribColonne.ID = EspLavRetribuzioni.IDColonna' +
                    ' where IDEspLav=' + Data.TEspLav.FieldByName('ID').asString
     end else
          {QDatiRetribuz.SQL.text := 'select EspLavRetribuzioni.ID, EspLavRetribColonne.ID IDColonna, ' +
               '       EspLavRetribuzioni.Data, ConsideraTot, ' +
               '       NomeColonna Campo, Valore' +
               'from EspLavRetribuzioni, EspLavRetribColonne ' +
               'where EspLavRetribColonne.ID *= EspLavRetribuzioni.IDColonna ' +
               ' and IDEspLav= 0'}
          //modifica query di Thomas
          QDatiRetribuz.SQL.text := 'select EspLavRetribuzioni.ID, EspLavRetribColonne.ID IDColonna, ' +
               '       EspLavRetribuzioni.Data, ConsideraTot, ' +
               '       NomeColonna Campo, Valore' +
               ' from EspLavRetribColonne left join EspLavRetribuzioni' +
               ' on EspLavRetribColonne.ID = EspLavRetribuzioni.IDColonna' +
               ' and IDEspLav= 0'
end;

procedure TDatiRetribuzFrame.QTotBeforeOpen(DataSet: TDataSet);
begin
     if Data.TEspLav.RecordCount > 0 then begin
          if (Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 6)) = 'ADVANT') or (pos('MCS', UpperCase(Data.Global.FieldByName('NomeAzienda').Value)) > 0) then
               QTot.SQL.text := 'select sum(Valore) TotValore from EspLavRetribuzioni ' +
                    'where ConsideraTot=1 and IDEspLav=' + ASASchedaCandForm.QEspLavID.AsString
          else
               QTot.SQL.text := 'select sum(Valore) TotValore from EspLavRetribuzioni ' +
                    'where ConsideraTot=1 and IDEspLav=' + Data.TEspLav.FieldByName('ID').asString;
     end else begin
          QTot.SQL.text := 'select sum(Valore) TotValore from EspLavRetribuzioni ' +
               'where ConsideraTot=1 and IDEspLav=0';
     end;
end;

end.

