unit DateInserimento;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, DtEdit97, Buttons, dxCntner, dxEditor, dxExEdtr, dxEdLib, DtEdDB97;

type
     TDateInserimentoForm = class(TForm)
          BitBtn1: TBitBtn;
          BitBtn2: TBitBtn;
          Label1: TLabel;
          Label2: TLabel;
          CBProm: TCheckBox;
          DEDataInizio: TdxDateEdit;
          DEDataControllo: TdxDateEdit;
          procedure FormShow(Sender: TObject);
          procedure BitBtn1Click(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     DateInserimentoForm: TDateInserimentoForm;

implementation

uses uQM;

{$R *.DFM}

procedure TDateInserimentoForm.FormShow(Sender: TObject);
begin
     Caption := '[E/3] - ' + Caption;
end;

procedure TDateInserimentoForm.BitBtn1Click(Sender: TObject);
begin
     // richiedi controllo di qualit� sulle due date
     if not EffettuaControlli(1, [DEDataInizio.Date, DEDataControllo.date]) then begin
          ModalResult := mrNone;
          Abort;
     end;

     // DA TOGLIERE
     // ########
     {if (DEDataInizio.Date=0)or(DEDataControllo.date=0) then begin
          ModalResult:=mrNone;
          MessageDlg('� necessario valorizzare entrambe le date',mtError, [mbOK],0);
          Abort;
     end;
     if (DEDataInizio.Date<Date)or(DEDataControllo.date=Date) then begin
          ModalResult:=mrNone;
          MessageDlg('Entrambe le date non possono essere antecedenti alla data odierna',mtError, [mbOK],0);
          Abort;
     end;}
end;

end.
