object DatiRetribuzFrame: TDatiRetribuzFrame
  Left = 0
  Top = 0
  Width = 485
  Height = 190
  TabOrder = 0
  object Panel1: TPanel
    Left = 393
    Top = 21
    Width = 92
    Height = 169
    Align = alRight
    BevelOuter = bvLowered
    TabOrder = 0
    object ToolbarButton971: TToolbarButton97
      Left = 1
      Top = 99
      Width = 89
      Height = 37
      Caption = 'Personalizza tabella'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        33333FFFFFFFFFFFFFFF000000000000000077777777777777770FFFFFFFFFFF
        FFF07F3FF3FF3FF3FFF70F00F00F00F000F07F773773773777370FFFFFFFFFFF
        FFF07F3FF3FF3FF3FFF70F00F00F00F000F07F773773773777370FFFFFFFFFFF
        FFF07F3FF3FF3FF3FFF70F00F00F00F000F07F773773773777370FFFFFFFFFFF
        FFF07F3FF3FF3FF3FFF70F00F00F00F000F07F773773773777370FFFFFFFFFFF
        FFF07FFFFFFFFFFFFFF70CCCCCCCCCCCCCC07777777777777777088CCCCCCCCC
        C8807FF7777777777FF700000000000000007777777777777777333333333333
        3333333333333333333333333333333333333333333333333333}
      NumGlyphs = 2
      Opaque = False
      WordWrap = True
      OnClick = ToolbarButton971Click
    end
    object BMod: TToolbarButton97
      Left = 1
      Top = 1
      Width = 89
      Height = 36
      Caption = 'Modifica valore'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
        000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
        00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
        F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
        0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
        FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
        FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
        0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
        00333377737FFFFF773333303300000003333337337777777333}
      NumGlyphs = 2
      WordWrap = True
      OnClick = BModClick
    end
    object BStorico: TToolbarButton97
      Left = 1
      Top = 37
      Width = 89
      Height = 36
      Caption = 'inverti "considera"'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clPurple
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333FFFFF3333333333999993333333333F77777FFF333333999999999
        3333333777333777FF33339993707399933333773337F3777FF3399933000339
        9933377333777F3377F3399333707333993337733337333337FF993333333333
        399377F33333F333377F993333303333399377F33337FF333373993333707333
        333377F333777F333333993333101333333377F333777F3FFFFF993333000399
        999377FF33777F77777F3993330003399993373FF3777F37777F399933000333
        99933773FF777F3F777F339993707399999333773F373F77777F333999999999
        3393333777333777337333333999993333333333377777333333}
      NumGlyphs = 2
      ParentFont = False
      WordWrap = True
      OnClick = BStoricoClick
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 21
    Width = 393
    Height = 169
    Align = alClient
    BevelOuter = bvLowered
    TabOrder = 1
    object Panel3: TPanel
      Left = 1
      Top = 136
      Width = 391
      Height = 32
      Align = alBottom
      BevelOuter = bvLowered
      Enabled = False
      TabOrder = 0
      object Label1: TLabel
        Left = 175
        Top = 8
        Width = 45
        Height = 13
        Caption = 'TOTALE:'
      end
      object DBEdit1: TDBEdit
        Left = 224
        Top = 5
        Width = 72
        Height = 21
        DataField = 'TotValore'
        DataSource = DsQTot
        TabOrder = 0
      end
    end
    object RxDBGrid1: TdxDBGrid
      Left = 1
      Top = 1
      Width = 391
      Height = 135
      Bands = <
        item
        end>
      DefaultLayout = True
      HeaderPanelRowCount = 1
      KeyField = 'ID'
      SummaryGroups = <>
      SummarySeparator = ', '
      Align = alClient
      TabOrder = 1
      DataSource = DsQDatiRetribuz
      Filter.Criteria = {00000000}
      OptionsBehavior = [edgoAutoSort, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoTabThrough, edgoVertThrough]
      OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
      OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoIndicator, edgoUseBitmap]
      object RxDBGrid1Campo: TdxDBGridColumn
        Width = 119
        BandIndex = 0
        RowIndex = 0
        FieldName = 'Campo'
      end
      object RxDBGrid1Data: TdxDBGridColumn
        Width = 116
        BandIndex = 0
        RowIndex = 0
        FieldName = 'Data'
      end
      object RxDBGrid1Valore: TdxDBGridColumn
        Width = 66
        BandIndex = 0
        RowIndex = 0
        FieldName = 'Valore'
      end
      object RxDBGrid1ConsideraTot: TdxDBGridColumn
        Caption = 'Considera tot.'
        Width = 74
        BandIndex = 0
        RowIndex = 0
        FieldName = 'ConsideraTot'
      end
    end
  end
  object Panel74: TPanel
    Left = 0
    Top = 0
    Width = 485
    Height = 21
    Align = alTop
    Alignment = taLeftJustify
    Caption = '  Dati retributivi'
    Color = clGray
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
  end
  object DsQDatiRetribuz: TDataSource
    DataSet = QDatiRetribuz
    Left = 40
    Top = 88
  end
  object DsQTot: TDataSource
    DataSet = QTot
    Left = 120
    Top = 93
  end
  object QDatiRetribuz: TADOLinkedQuery
    Connection = Data.DB
    CursorType = ctStatic
    BeforeOpen = QDatiRetribuz_old1BeforeOpen
    Parameters = <
      item
        Name = '='
        Size = -1
        Value = Null
      end
      item
        Name = 'xid:'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      ''
      'select EspLavRetribuzioni.ID, EspLavRetribColonne.ID IDColonna, '
      '       EspLavRetribuzioni.Data, ConsideraTot,'
      '       NomeColonna Campo, Valore'
      'from EspLavRetribColonne  left join EspLavRetribuzioni'
      'on EspLavRetribColonne.ID = EspLavRetribuzioni.IDColonna'
      'where esplavretribuzioni.idesplav:= :xid:'
      '')
    UseFilter = False
    Left = 40
    Top = 53
    object QDatiRetribuzID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QDatiRetribuzIDColonna: TAutoIncField
      FieldName = 'IDColonna'
      ReadOnly = True
    end
    object QDatiRetribuzData: TDateTimeField
      FieldName = 'Data'
    end
    object QDatiRetribuzConsideraTot: TBooleanField
      FieldName = 'ConsideraTot'
      DisplayValues = 's�;no'
    end
    object QDatiRetribuzCampo: TStringField
      FieldName = 'Campo'
      FixedChar = True
      Size = 30
    end
    object QDatiRetribuzValore: TFloatField
      FieldName = 'Valore'
    end
  end
  object QTot: TADOLinkedQuery
    Connection = Data.DB
    CursorType = ctStatic
    BeforeOpen = QTotBeforeOpen
    Parameters = <>
    SQL.Strings = (
      'select sum(Valore) TotValore'
      'from EspLavRetribuzioni'
      'where ConsideraTot=1')
    MasterDataSet = Data.TEspLav
    LinkedMasterField = 'ID'
    LinkedDetailField = 'IDEspLav'
    UseFilter = False
    Left = 120
    Top = 61
  end
end
