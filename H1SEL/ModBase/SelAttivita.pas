//[/TONI20020920\]DEBUGOKFINE
unit SelAttivita;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Grids, DBGrids, Db, DBTables, StdCtrls, Buttons, ExtCtrls, dxTL,
     dxDBCtrl, dxDBGrid, dxCntner, ADODB, U_ADOLinkCl, TB97;

type
     TSelAttivitaForm = class(TForm)
          DsSettori: TDataSource;
          Timer1: TTimer;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1Attivita: TdxDBGridMaskColumn;
          dxDBGrid1AreaSettore: TdxDBGridMaskColumn;
          dxDBGrid1Column3: TdxDBGridColumn;
          TSettori: TADOLinkedQuery;
          Panel1: TPanel;
          BOK: TToolbarButton97;
          BAnnulla: TToolbarButton97;
          procedure Timer1Timer(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure BOKClick(Sender: TObject);
          procedure BAnnullaClick(Sender: TObject);
     private
          xStringa: string;
     public
          { Public declarations }
     end;

var
     SelAttivitaForm: TSelAttivitaForm;

implementation

uses ModuloDati, Main;
{$R *.DFM}


procedure TSelAttivitaForm.Timer1Timer(Sender: TObject);
begin
     xStringa := '';
end;

procedure TSelAttivitaForm.FormShow(Sender: TObject);
begin
     //Grafica
     SelAttivitaForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     Caption := '[S/6] - ' + caption;
     TSettori.Open;
end;

procedure TSelAttivitaForm.BOKClick(Sender: TObject);
begin
     SelAttivitaForm.ModalResult := mrOK;
end;

procedure TSelAttivitaForm.BAnnullaClick(Sender: TObject);
begin
     SelAttivitaForm.ModalResult := mrCancel;
end;

end.

