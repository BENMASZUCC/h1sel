object StoricoAnagForm: TStoricoAnagForm
  Left = 322
  Top = 314
  Width = 702
  Height = 285
  Caption = 'Storico Commessa'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object dxDBGrid1: TdxDBGrid
    Left = 8
    Top = 8
    Width = 577
    Height = 241
    Bands = <
      item
      end>
    DefaultLayout = True
    HeaderPanelRowCount = 1
    SummaryGroups = <>
    SummarySeparator = ', '
    TabOrder = 0
    DataSource = Data.DSstorComm
    Filter.Criteria = {00000000}
    OptionsBehavior = [edgoAutoSort, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoTabThrough, edgoVertThrough]
    object dxDBGrid1data: TdxDBGridDateColumn
      Width = 90
      BandIndex = 0
      RowIndex = 0
      FieldName = 'data'
    end
    object dxDBGrid1Esito: TdxDBGridMaskColumn
      Width = 150
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Esito'
    end
    object dxDBGrid1Evento: TdxDBGridMaskColumn
      Width = 150
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Evento'
    end
    object dxDBGrid1candidato: TdxDBGridMaskColumn
      Visible = False
      BandIndex = 0
      RowIndex = 0
      FieldName = 'candidato'
    end
    object dxDBGrid1utente: TdxDBGridMaskColumn
      Width = 120
      BandIndex = 0
      RowIndex = 0
      FieldName = 'utente'
    end
  end
  object BitBtn1: TBitBtn
    Left = 592
    Top = 16
    Width = 97
    Height = 33
    Caption = 'Esci'
    TabOrder = 1
    Kind = bkOK
  end
end
