unit MotiviSospensione;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ExtCtrls, TB97, Db, ADODB, dxDBTLCl, dxGrClms, dxTL, dxDBCtrl,
     dxDBGrid, dxCntner, StdCtrls, DtEdit97, DtEdDB97, Mask, ToolEdit,
     ComCtrls, JvExComCtrls, JvDateTimePicker;

type
     TMotiviSospensioneForm = class(TForm)
          Panel1: TPanel;
          Panel2: TPanel;
          BAnnulla: TToolbarButton97;
          BOk: TToolbarButton97;
          QMotiviSosp: TADOQuery;
          QMotiviSospID: TAutoIncField;
          QMotiviSospCodice: TStringField;
          QMotiviSospDescMotivoStatoSosp: TStringField;
          QMotiviSospPeriodoSospSTD: TIntegerField;
          QMotiviSospPeriodoPreavvisoFineSosp: TIntegerField;
          DSMotiviSosp: TDataSource;
          dxDBGrid32: TdxDBGrid;
          dxDBGrid32Column1: TdxDBGridColumn;
          dxDBGrid32Column2: TdxDBGridColumn;
          dxDBGrid32Column4: TdxDBGridSpinColumn;
          dxDBGrid32Column3: TdxDBGridSpinColumn;
          GroupBox1: TGroupBox;
          QMotiviSospDataTermineCalc: TDateTimeField;
          DEData: TDateEdit97;
          procedure BAnnullaClick(Sender: TObject);
          procedure BOkClick(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure QMotiviSospAfterScroll(DataSet: TDataSet);
     private
    { Private declarations }
     public
    { Public declarations }
     end;

var
     MotiviSospensioneForm: TMotiviSospensioneForm;

implementation

uses Main, ModuloDati;

{$R *.DFM}

procedure TMotiviSospensioneForm.BAnnullaClick(Sender: TObject);
begin
     MotiviSospensioneForm.ModalResult := mrCancel;
end;

procedure TMotiviSospensioneForm.BOkClick(Sender: TObject);
begin
     MotiviSospensioneForm.DEData.DoDate;      
     MotiviSospensioneForm.ModalResult := mrOk;

end;

procedure TMotiviSospensioneForm.FormShow(Sender: TObject);
begin
//grafica
     Panel1.Color := mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.Color := mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;
     MotiviSospensioneForm.Color := mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;
      
     DEData.date := now;
     QMotiviSosp.Open;

end;

procedure TMotiviSospensioneForm.QMotiviSospAfterScroll(DataSet: TDataSet);
begin
     dedata.Date := QMotiviSospDataTermineCalc.Value;
end;

end.

