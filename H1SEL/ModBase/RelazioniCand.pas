unit RelazioniCand;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ImgList, dxTLClms, dxTL, dxCntner, ExtCtrls, Db, ADODB, Menus,
     dxPSCore, dxPSdxTLLnk, Buttons, StdCtrls, ComCtrls, DBCtrls, TB97;

type
     TRelazioniCandFrame = class(TFrame)
          dxTreeList: TdxTreeList;
          dxDicitura: TdxTreeListColumn;
          dxID: TdxTreeListSpinColumn;
          dxItemsCount: TdxTreeListSpinColumn;
          ImageList1: TImageList;
          QSubItems: TADOQuery;
          PMMaster: TPopupMenu;
          Aprischeda1: TMenuItem;
          N1: TMenuItem;
          Personalizza1: TMenuItem;
          dxScheda: TdxTreeListColumn;
          Panel1: TPanel;
          Qnote: TADOQuery;
          DSNote: TDataSource;
          MemoNote: TDBRichEdit;
          QnoteID: TIntegerField;
          QnoteDicitura: TMemoField;
          ToolbarButton971: TToolbarButton97;
          BSintesi: TToolbarButton97;
          QItems: TADOQuery;
          QItemsID: TAutoIncField;
          QItemsDicNodoMaster: TStringField;
          QItemsImageIndex: TSmallintField;
          QItemsStringaSQL: TStringField;
          QItemsOrdine: TSmallintField;
          QItemsInvisibile: TBooleanField;
          QItemsScheda: TStringField;
          QItemsTipo: TStringField;
          QItemsApplicazione: TStringField;
          QItemsIDMenu_master: TSmallintField;
          QItemsIDMenu_detail: TSmallintField;
          QItemsCodLicenza: TStringField;
          dxPrinter1: TdxComponentPrinter;
          dxPrinter1Link1: TdxTreeListReportLink;
          ToolbarButton972: TToolbarButton97;
          procedure dxTreeListCustomDrawCell(Sender: TObject; ACanvas: TCanvas;
               ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
               ASelected, AFocused, ANewItemRow: Boolean; var AText: string;
               var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
               var ADone: Boolean);
          procedure Aprischeda1Click(Sender: TObject);
          procedure PMMasterPopup(Sender: TObject);
          procedure Personalizza1Click(Sender: TObject);
          procedure SpeedButton1Click(Sender: TObject);
          procedure QItemsBeforeOpen(DataSet: TDataSet);
          procedure BSintesiClick(Sender: TObject);
     private
          { Private declarations }
     public
          xTipo, xChiamante: string;
          procedure CreaAlbero;
          procedure FormattazioneNote(str: string; var p: TStringList);
     end;

implementation

uses ModuloDati, RelazCand_config, Main, SchedaCand, ModuloDati2, ModelliWord,
     SelRicCand;

{$R *.DFM}

{ TRelazioniCandFrame }

type
     TRighe = array[1..1000] of integer;


function ContaRighe(testo: string): Trighe;
var i, N, c: integer;
     x: TRighe;
begin
     //la cella 1 dell'array � il numero righe
     // le altre celle  contengono le posizione di quando devo andare a capo
     if testo <> '' then begin
          testo := testo + #13;
          n := 0;
          c := 2;
          for i := 0 to Length(testo) do begin
               if testo[i] = #13 then begin
                    x[c] := i;
                    c := c + 1;
                    if i >= Length(testo) then begin
                         n := n;
                         // showmessage(inttostr(n));
                    end else
                         N := N + 1;
               end;
          end;
          x[1] := n;
          result := x;
     end;
end;


function TogliInvio(testo: string): string;
var i: integer; //se lo faccio con una stringreplace non "replaccia" il #13
     r: string;
begin
     if testo <> '' then begin
          r := '';
          for i := 0 to Length(testo) do begin
               if (testo[i] <> #13) then
                    r := r + testo[i];
          end;
          result := trim(r);
     end else
          result := '';
end;

procedure TRelazioniCandFrame.FormattazioneNote(str: string; var p: TStringList);
var j, i, f: integer;
     r, s: string;
begin
     if str <> '' then begin
          r := '';
          j := 0;
          p.Clear;
          for i := 0 to Length(str) do begin
               if (str[i] <> #13) then begin
                    r := r + str[i];
                    if str[i] = ' ' then j := Length(r);
               end;
               if Length(r) = 150 then begin
                    if j = 0 then begin
                         p.Add(Trim(r) + '-');
                         r := '';
                    end else begin
                         s := Copy(r, 1, j);
                         p.Add(Trim(s));
                         s := Copy(r, j + 1, Length(r) - j);
                         r := s;
                         j := Length(r);
                    end;
                    j := 0;
               end;
          end;
          p.Add(Trim(r));
     end else p.Text := '';
end;

procedure TRelazioniCandFrame.CreaAlbero;
var Item, SubItem: TdxTreeListNode;
     xBitMap: TBitMap;
     xImage: TImage;
     i, s, c, j: integer;
     nrighe: TRighe;
     xnote: string;
     xnotelist: TStringList;

begin
     if xTipo = '' then exit;
     if xTipo = 'A' then
          if (Data.TAnagrafica.IsEmpty) or (not Data.TAnagrafica.active) then exit;
     if xTipo = 'C' then
          if (Data2.TEBCClienti.IsEmpty) or (not Data2.TEBCClienti.active) then exit;

     // creazione nuova dxTreeList
     dxTreeList.ClearNodes;
     dxTreeList.BeginUpdate;

     QItems.Close;
     QItems.Open;

     // caricamento nodo
     QItems.First;
     xnotelist := TStringList.Create;
     while not QItems.EOF do begin
          if MainForm.CheckProfile(QItems.FieldByName('CodLicenza').AsString, false) then begin
               // nodo master - POINTER = 0
               Item := dxTreeList.Add;
               Item.Strings[0] := QItems.FieldByName('DicNodoMaster').asString;
               Item.Values[1] := QItems.FieldByName('ID').asInteger;
               Item.Values[3] := QItems.FieldByName('Scheda').asString;

               // iconcina
               Item.ImageIndex := QItems.FieldByName('ImageIndex').asInteger;
               Item.SelectedIndex := QItems.FieldByName('ImageIndex').asInteger;
               Item.StateIndex := QItems.FieldByName('ImageIndex').asInteger;

               QSubItems.Close;
               QSubItems.SQL.text := QItems.FieldByName('StringaSQL').asString;
               // parametri: un solo parametro, eventualmente ripetuto = Anagrafica.ID o EBC_Clienti.ID
               for i := 0 to QSubItems.Parameters.Count - 1 do begin
                    if xTipo = 'A' then
                         QSubItems.Parameters[i].Value := Data.TAnagraficaID.Value;
                    if xTipo = 'C' then
                         QSubItems.Parameters[i].Value := Data2.TEBCClientiID.Value;
               end;

               QSubItems.Open;
               // numero (conta)
               Item.Values[2] := QSubItems.RecordCount;
               //formattazione campo note
               if UpperCase(Item.Strings[0]) = 'NOTE' then begin
                    Qnote.Close;
                    QNote.SQL.text := QItems.FieldByName('StringaSQL').asString;
                    QNote.Parameters[0].Value := Data.TAnagraficaID.Value;
                    Qnote.Open;
                    //per andare a capo nelle note creo pi� righe
                    nrighe := ContaRighe(MemoNote.text);
                    s := 0;
                    c := 2;
                    for i := 0 to nrighe[1] do begin
                         SubItem := Item.AddChild;
                         SubItem.ImageIndex := -1;
                         SubItem.SelectedIndex := -1;
                         SubItem.StateIndex := -1;
                         xnote := '';
                         if i = 0 then
                              xnote := copy(trim(MemoNote.Text), s, (nrighe[c] - s) - 1)
                         else
                              xnote := copy(trim(MemoNote.Text), s + 1, (nrighe[c] - s));

                         //showmessage(xnote + '   / ' + TogliInvio(xnote));
                         //SubItem.Strings[0] := TogliInvio(xnote);

                         FormattazioneNote(xnote, xnotelist);
                         for j := 0 to xnotelist.Count - 1 do begin
                              SubItem.Strings[0] := xnotelist.Strings[j];
                              if (j <> (xnotelist.Count - 1)) then begin
                                   SubItem := Item.AddChild;
                                   SubItem.ImageIndex := -1;
                                   SubItem.SelectedIndex := -1;
                                   SubItem.StateIndex := -1;
                              end;
                         end;
                         xnotelist.Clear;

                         s := nrighe[c];
                         c := c + 1;
                    end;

               end else begin

                    while not QSubItems.EOF do begin
                         {
                        // nodi figli - POINTER = ID dell'entit�
                        SubItem := Item.AddChild;
                        SubItem.Strings[0] := QSubItems.FieldByName('Dicitura').asString;
                        SubItem.Values[1] := 0;

                        // iconcina -> al momento non visibile
                        SubItem.ImageIndex := -1;
                        SubItem.SelectedIndex := -1;
                        SubItem.StateIndex := -1;
                        //SubItem.ImageIndex := QItems.FieldByName('ImageIndex').asInteger;
                        //SubItem.SelectedIndex := QItems.FieldByName('ImageIndex').asInteger;
                        //SubItem.StateIndex := QItems.FieldByName('ImageIndex').asInteger;
                        QSubItems.Next;
                        }

                         SubItem := Item.AddChild;
                         //SubItem.Strings[0] := QSubItems.FieldByName('Dicitura').asString;
                         SubItem.Values[1] := 0;
                         SubItem.ImageIndex := -1;
                         SubItem.SelectedIndex := -1;
                         SubItem.StateIndex := -1;
                         FormattazioneNote(QSubItems.FieldByName('Dicitura').asString, xnotelist);
                         for j := 0 to xnotelist.Count - 1 do begin
                              SubItem.Strings[0] := xnotelist.Strings[j];
                              SubItem.Values[1] := 0;
                              if (j <> (xnotelist.Count - 1)) then begin
                                   SubItem := Item.AddChild;
                                   SubItem.ImageIndex := -1;
                                   SubItem.SelectedIndex := -1;
                                   SubItem.StateIndex := -1;
                              end;
                         end;
                         xnotelist.Clear;
                         QSubItems.Next;

                    end;
               end;
               if QSubItems.RecordCount < 10 then Item.Expand(true);
          end;
          QItems.Next;
     end;
     xnotelist.Free;
     dxTreeList.EndUpdate;
     dxTreeList.FullExpand;
end;

procedure TRelazioniCandFrame.dxTreeListCustomDrawCell(Sender: TObject;
     ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
     AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
     var AText: string; var AColor: TColor; AFont: TFont;
     var AAlignment: TAlignment; var ADone: Boolean);
begin
     if ANode.Strings[2] <> '' then begin
          //if (ANode.Values[2] > 0) and (AColumn = dxDicitura) then
          if AColumn = dxDicitura then
               AFont.style := [fsBold]
     end;
end;

procedure TRelazioniCandFrame.Aprischeda1Click(Sender: TObject);
begin
     if xTipo = 'A' then begin
          if dxTreeList.FocusedNode.Values[3] = 'DatiPersonali' then begin
               if xChiamante = 'Main' then begin
                    MainForm.PageControl2.ActivePage := MainForm.TSAnagDatiAnag;
                    MainForm.PageControl2Change(self);
               end;
               if xChiamante = 'Scheda' then begin
                    SchedaCandForm.PageControl2.ActivePage := SchedaCandForm.TSAnagDatiAnag;
                    SchedaCandForm.PageControl2Change(self);
               end;
          end;
          if (dxTreeList.FocusedNode.Values[3] = 'EspLav') and (xChiamante = 'Main') then begin
               MainForm.xQualeSchedaCurr := 'EspLav';
               MainForm.BAnagCurriculumClick(self);
          end;
          if (dxTreeList.FocusedNode.Values[3] = 'Lingue') and (xChiamante = 'Main') then begin
               MainForm.xQualeSchedaCurr := 'Lingue';
               MainForm.BAnagCurriculumClick(self);
          end;
          if (dxTreeList.FocusedNode.Values[3] = 'TitStudio') and (xChiamante = 'Main') then begin
               MainForm.xQualeSchedaCurr := 'TitStudio';
               MainForm.BAnagCurriculumClick(self);
          end;
          if dxTreeList.FocusedNode.Values[3] = 'Commesse' then begin
               if xChiamante = 'Main' then begin
                    MainForm.PageControl2.ActivePage := MainForm.TSAnagRicerche;
                    MainForm.PageControl2Change(self);
               end;
               if xChiamante = 'Scheda' then begin
                    SchedaCandForm.PageControl2.ActivePage := SchedaCandForm.TSAnagRicerche;
                    SchedaCandForm.PageControl2Change(self);
               end;
          end;
          if dxTreeList.FocusedNode.Values[3] = 'Aree e ruoli' then begin
               if xChiamante = 'Main' then begin
                    MainForm.PageControl2.ActivePage := MainForm.TsAnagMans;
                    MainForm.PageControl2Change(self);
               end;
               if xChiamante = 'Scheda' then begin
                    SchedaCandForm.PageControl2.ActivePage := SchedaCandForm.TsAnagMans;
                    SchedaCandForm.PageControl2Change(self);
               end;
          end;
          if dxTreeList.FocusedNode.Values[3] = 'Storico' then begin
               if xChiamante = 'Main' then begin
                    MainForm.PageControl2.ActivePage := MainForm.TSAnagStorico;
                    MainForm.PageControl2Change(self);
               end;
               if xChiamante = 'Scheda' then begin
                    SchedaCandForm.PageControl2.ActivePage := SchedaCandForm.TSAnagStorico;
                    SchedaCandForm.PageControl2Change(self);
               end;
          end;

     end;
     if xTipo = 'C' then begin
          if dxTreeList.FocusedNode.Values[3] = 'DatiAzienda' then begin
               MainForm.PCClienti.ActivePage := MainForm.TSClientiDett;
               MainForm.PCClientiChange(self);
          end;
          if dxTreeList.FocusedNode.Values[3] = 'CommesseAzienda' then begin
               MainForm.PCClienti.ActivePage := MainForm.TSClientiRic;
               MainForm.PCClientiChange(self);
          end;
     end;
end;

procedure TRelazioniCandFrame.PMMasterPopup(Sender: TObject);
begin
     if dxTreeList.FocusedNode.Values[1] = 0 then begin
          abort;
     end;
end;

procedure TRelazioniCandFrame.Personalizza1Click(Sender: TObject);
begin
     MessageDlg('Attenzione: le modifiche apportate valgono per tutti gli utenti', mtWarning, [mbOK], 0);
     RelazCand_configForm := TRelazCand_configForm.create(self);
       RelazCand_configForm.xTipo := xTipo;
     RelazCand_configForm.ShowModal;
     RelazCand_configForm.Free;
     CreaAlbero;
end;

procedure TRelazioniCandFrame.SpeedButton1Click(Sender: TObject);
begin
     if xTipo = 'A' then
          dxPrinter1Link1.ReportTitle.text := Data.TAnagraficaCognome.Value + ' ' + Data.TAnagraficaNome.Value;
     if xTipo = 'C' then
          dxPrinter1Link1.ReportTitle.text := Data2.TEBCClientiDescrizione.Value;
     dxPrinter1.Preview(True, dxPrinter1Link1);
end;

procedure TRelazioniCandFrame.QItemsBeforeOpen(DataSet: TDataSet);
begin
     QItems.SQL.Clear;
     QItems.SQL.Text := 'select * from AnagSintesiModel ' +
          'where (Invisibile=0 or Invisibile is null) ' +
          ' and Applicazione = ''H1Sel''  and Tipo=:x order by Ordine';
     QItems.Parameters[0].Value := xTipo;

end;

procedure TRelazioniCandFrame.BSintesiClick(Sender: TObject);
begin
     // emergency
     if Pos('EMERGENCY', UpperCase(Data.Global.FieldByName('NomeAzienda').AsString)) > 0 then begin
          data.QTemp.close;
          data.qtemp.sql.text := 'SELECT ID FROM EBC_CandidatiRicerche where idanagrafica= ' + Data.TAnagrafica.FieldByName('ID').asString;
          data.qtemp.open;
          if data.qtemp.RecordCount = 1 then begin
               CreaModelloEmergency(Data.TAnagrafica.FieldByName('ID').asInteger, data.qtemp.fieldbyname('ID').asInteger, Data.DB.ConnectionString);
          end else begin
               SelRicCandForm := TSelRicCandForm.create(self);
               SelRicCandForm.QRicCand.SQL.Text := 'select EBC_Clienti.Descrizione Cliente, Mansioni.Descrizione Posizione,' +
                    ' EBC_CandidatiRicerche.ID IDCandRic, EBC_Clienti.ID IDCliente, ' +
                    ' IDMansione, TitoloCliente,ebc_ricerche.id idricerca ' +
                    ' from EBC_Ricerche,EBC_Clienti,Mansioni,EBC_CandidatiRicerche  ' +
                    ' where EBC_Ricerche.IDCliente=EBC_Clienti.ID ' +
                    ' and EBC_Ricerche.IDMansione=Mansioni.ID  ' +
                    ' and EBC_Ricerche.ID=EBC_CandidatiRicerche.IDRicerca' +
                    ' and EBC_CandidatiRicerche.IDAnagrafica=:xIDAnag:';
               SelRicCandForm.QRicCand.ParambyName['xIDAnag'] := Data.TAnagrafica.FieldByName('ID').AsINteger;
               SelRicCandForm.QRicCand.Open;
               if SelRicCandForm.QRicCand.RecordCount = 0 then begin
                    SelRicCandForm.Free;
                    if MessageDlg('Non risultano ricerche associate al soggetto. Vuoi procedere?', mtWarning, [mbYes, mbNo], 0) = mrNo then begin
                         SelRicCandForm.Free;
                         exit;
                    end else CreaModelloEmergency(Data.TAnagrafica.FieldByName('ID').asInteger, 0, Data.DB.ConnectionString);
               end else begin
                    SelRicCandForm.ShowModal;
                    if SelRicCandForm.ModalResult = mrOK then
                         CreaModelloEmergency(Data.TAnagrafica.FieldByName('ID').asInteger, SelRicCandForm.QRicCandIDCandRic.AsInteger, Data.DB.ConnectionString)
                    else exit;
               end;
          end;
          exit;
     end;
end;




end.

