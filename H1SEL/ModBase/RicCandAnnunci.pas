unit RicCandAnnunci;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     TB97, ExtCtrls, dxDBGrid, dxDBTLCl, dxGrClms, dxDBCtrl, dxTL, dxCntner,
     StdCtrls, Buttons, Db, ADODB, U_ADOLinkCl;

type
     TRicCandAnnunciForm = class(TForm)
          Panel1: TPanel;
          Panel2: TPanel;
          PanCriteriButtons: TPanel;
          BCriterioNew: TToolbarButton97;
          BCriterioMod: TToolbarButton97;
          BCriterioDel: TToolbarButton97;
          BLineeOK: TToolbarButton97;
          BLineeCan: TToolbarButton97;
          BCriterioDelTutti: TToolbarButton97;
          Panel3: TPanel;
          DBGrid3: TdxDBGrid;
          DBGrid3DescTabella: TdxDBGridButtonColumn;
          DBGrid3DescCampo: TdxDBGridButtonColumn;
          DBGrid3DescOperatore: TdxDBGridButtonColumn;
          DBGrid3Valore: TdxDBGridButtonColumn;
          DBGrid3Descrizione: TdxDBGridMaskColumn;
          DBGrid3LivLinguaParlato: TdxDBGridButtonColumn;
          DBGrid3LivLinguaScritto: TdxDBGridButtonColumn;
          DBGrid3OpNext: TdxDBGridPickColumn;
          DBGrid3ID: TdxDBGridColumn;
          DBGrid3QueryLookup: TdxDBGridColumn;
          TLineeAnn: TADOQuery;
          TLineeAnnID: TAutoIncField;
          TLineeAnnIDRicerca: TIntegerField;
          TLineeAnnIDAnnuncio: TIntegerField;
          TLineeAnnIDAnnEdizData: TIntegerField;
          TLineeAnnDescrizione: TStringField;
          TLineeAnnStringa: TStringField;
          TLineeAnnTabella: TStringField;
          TLineeAnnOpSucc: TStringField;
          TLineeAnnOpNext: TStringField;
          TLineeAnnLivLinguaParlato: TIntegerField;
          TLineeAnnLivLinguaScritto: TIntegerField;
          TLineeAnnDescTabella: TStringField;
          TLineeAnnDescCampo: TStringField;
          TLineeAnnDescOperatore: TStringField;
          TLineeAnnValore: TStringField;
          TLineeAnnQueryLookup: TStringField;
          TLineeAnnDescCompleta: TStringField;
          TLineeAnnIDTabQueryOp: TIntegerField;
          Query1: TADOQuery;
          DsLineeAnn: TDataSource;
          Q: TADOLinkedQuery;
          TLineeAnnIDUtente: TIntegerField;
          TLineeAnnPeso: TIntegerField;
          DBGrid3Column11: TdxDBGridColumn;
          Q1: TADOLinkedQuery;
          procedure BCriterioNewClick(Sender: TObject);
          procedure BCriterioModClick(Sender: TObject);
          procedure BCriterioDelClick(Sender: TObject);
          procedure BCriterioDelTuttiClick(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
          procedure TLineeAnnBeforePost(DataSet: TDataSet);
          procedure DsLineeAnnStateChange(Sender: TObject);
          procedure BLineeOKClick(Sender: TObject);
          procedure BLineeCanClick(Sender: TObject);
          procedure DBGrid3ValoreEditButtonClick(Sender: TObject);
          procedure DBGrid3CustomDrawCell(Sender: TObject; ACanvas: TCanvas;
               ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
               ASelected, AFocused, ANewItemRow: Boolean; var AText: string;
               var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
               var ADone: Boolean);
     private
    { Private declarations }
     public
    { Public declarations }
          xIDAnnuncio, xIDRicerca: integer;
          procedure SalvaDaWizardAnn(xIDUtente: integer = 0; xIDRicerca: integer = 0; xIDAnnuncio: integer = 0; xIDAnnEdizData: integer = 0);
          procedure PredisponiWizardAnn;
          procedure RicreaStringaSQLAnn;
     end;

var
     RicCandAnnunciForm: TRicCandAnnunciForm;

implementation

uses ModuloDati, SelCriterioWizard, Main, ElencoUtenti;

{$R *.DFM}

procedure TRicCandAnnunciForm.BCriterioNewClick(Sender: TObject);
var K: INTEGER;
     xPeso: string;
begin
     SelCriterioWizardForm.xPaginaAttiva := 0;
     Panel2.Caption := '  Criteri impostati';
     with SelCriterioWizardForm do begin
          xTabella := '';
          xCampo := '';
          xOperatore := '';
          xValore := '';
     end;
     SelCriterioWizardForm.QTAbelle.Close;
     SelCriterioWizardForm.QTAbelle.SQL.Text := 'select distinct Tabella,DescTabella from TabQuery where SubQueryAnn <> '''' order by desctabella';
     SelCriterioWizardForm.QTabelle.Open;

     SelCriterioWizardForm.QCampi.Close;
     SelCriterioWizardForm.QCampi.SQL.Text := 'select * from TabQuery where DescTabella=:xTabella and SubQueryAnn <> '''' order by DescCampo';
     //SelCriterioWizardForm.QCampi.Open;

     SelCriterioWizardForm.ShowModal;
     if SelCriterioWizardForm.ModalResult = mrOK then begin
          if SelCriterioWizardForm.PanGrid.Visible then begin
               for k := 0 to SelCriterioWizardForm.dxDBGrid1.SelectedCount - 1 do begin
                    SelCriterioWizardForm.QTable.BookMark := SelCriterioWizardForm.dxDBGrid1.SelectedRows[k];
                    TLineeAnn.Insert;
                    TLineeAnnIDUtente.Value := MainForm.xIDUtenteAttuale;
                    SalvaDaWizardAnn(MainForm.xIDUtenteAttuale, xIDRicerca, xidannuncio, 0);
                    if InputQuery('Peso', 'Peso', xPeso) then
                         TLineeAnnPeso.Value := StrToInt(xPeso);
                    TLineeAnn.Post;

               end;
          end else begin
               TLineeAnn.Insert;
               TLineeAnnIDUtente.Value := MainForm.xIDUtenteAttuale;
               SalvaDaWizardAnn(MainForm.xIDUtenteAttuale, xIDRicerca, xidannuncio, 0);
               if InputQuery('Peso', 'Peso', xPeso) then
                    TLineeAnnPeso.Value := StrToInt(xPeso);
               TLineeAnn.Post;
          end;
     end;

end;

procedure TRicCandAnnunciForm.PredisponiWizardAnn;
begin
     SelCriterioWizardForm.QTAbelle.Close;
     SelCriterioWizardForm.QTAbelle.SQL.Text := 'select distinct Tabella,DescTabella from TabQuery where SubQueryAnn <> '''' order by desctabella';
     SelCriterioWizardForm.QTabelle.Open;

     SelCriterioWizardForm.QCampi.Close;
     SelCriterioWizardForm.QCampi.SQL.Text := 'select * from TabQuery where DescTabella=:xTabella and SubQueryAnn <> '''' order by DescCampo';
     //SelCriterioWizardForm.QCampi.Open;
     with SelCriterioWizardForm do begin
          // TABELLA
          if not QTabelle.Active then QTabelle.Open;
          xTabella := TLineeAnnDescTabella.Value;
          if xTabella <> '' then QTabelle.Locate('DescTabella', xTabella, []);
          // CAMPO
          xCampo := TLineeAnnDescCampo.Value;
          if xCampo <> '' then begin
               if not QCampi.Active then QCampi.Open else begin QCampi.Close; QCampi.Open; end;
               QCampi.Locate('DescCampo', xCampo, []);
          end;
          // OPERATORE
          xOperatore := TLineeAnnDescOperatore.Value;
          if xOperatore <> '' then begin
               if not QOperatori.Active then QOperatori.Open else begin QOperatori.Close; QOperatori.Open; end;
               QOperatori.Locate('DescOperatore', xOperatore, []);
          end;
          // VALORE
          xValore := TLineeAnnValore.Value;
     end;
end;

procedure TRicCandAnnunciForm.SalvaDaWizardAnn(xIDUtente: integer = 0; xIDRicerca: integer = 0; xIDAnnuncio: integer = 0; xIDAnnEdizData: integer = 0);

var xdesc: string;
begin
     if not (DSLineeAnn.State = dsEdit) then TLineeAnn.edit;
     TLineeAnnDescTabella.Value := SelCriterioWizardForm.QTabelle.FieldByName('DescTabella').asString;

     Q.Close;
     Q.SqL.Text := 'select Tabella from TabQuery where id=' + SelCriterioWizardForm.QCampi.fieldbyname('ID').asstring;
     q.open;

     TLineeAnnTabella.Value := q.fieldbyname('Tabella').asstring; // SelCriterioWizardForm.QTabelle.FieldByName('Tabella').asString;


     if SelCriterioWizardForm.QCampi.Active then
          TLineeAnnDescCampo.Value := SelCriterioWizardForm.QCampi.FieldByName('DescCampo').asString;
     if SelCriterioWizardForm.QOperatori.Active then
          TLineeAnnDescOperatore.Value := SelCriterioWizardForm.QOperatori.FieldByName('DescOperatore').asString;

     TLineeAnnIDUtente.Value := xIDUtente;
     TLineeAnnIDRicerca.Value := xIDRicerca;
     TLineeAnnIDAnnuncio.Value := xIDAnnuncio;
     TLineeAnnIDAnnEdizData.Value := xIDAnnEdizData;

     with SelCriterioWizardForm do begin
          if dxEdit1.visible then begin
               TLineeAnnValore.Value := dxEdit1.Text;
               xDesc := dxEdit1.Text;
          end;
          if dxPickEdit1.visible then begin
               TLineeAnnValore.Value := dxPickEdit1.Text;
               xDesc := dxPickEdit1.Text;
          end;
          if dxDateEdit1.visible then begin
               TLineeAnnValore.Value := DateToStr(dxDateEdit1.Date);
               xDesc := DateToStr(dxDateEdit1.Date);
          end;
          if dxSpinEdit1.visible then begin
               TLineeAnnValore.Value := IntToStr(round(dxSpinEdit1.value));
               xDesc := IntToStr(round(dxSpinEdit1.value));
          end;
          if PanGrid.visible then begin
               TLineeAnnValore.Value := QTable.FieldByName('ID').AsString;
               xDesc := QTable.Fields[1].AsString;
          end;
     end;

     // descrizione completa (per ricerca semplice)
     TLineeAnnDescCompleta.Value := SelCriterioWizardForm.QCampi.FieldByName('DescCampo').asString + ' ' +
          SelCriterioWizardForm.QOperatori.FieldByName('DescOperatore').asString; //SelCriterioWizardForm.QOperatori.FieldByName('DescCompleta').asString;
     // puntatore alla riga in TabQueryOperatori
     TLineeAnnIDTabQueryOp.Value := SelCriterioWizardForm.QOperatori.FieldByName('ID').asInteger;
     TLineeAnnDescrizione.Value := SelCriterioWizardForm.QCampi.FieldByName('DescCampo').asString + ' ' +
          SelCriterioWizardForm.QOperatori.FieldByName('DescOperatore').asString + ' ' + xDesc;
     if TLineeAnnOpSucc.Value = '' then TLineeAnnOpSucc.Value := 'and';
     if TLineeAnnOpNext.Value = '' then TLineeAnnOpNext.Value := 'e';
end;


procedure TRicCandAnnunciForm.BCriterioModClick(Sender: TObject);
var xPeso: string;
begin
     if TLineeAnn.IsEmpty then exit;
     PredisponiWizardAnn;
     // setta variabile "pagina attiva"
     SelCriterioWizardForm.xPaginaAttiva := 0;
     SelCriterioWizardForm.ShowModal;
     if SelCriterioWizardForm.ModalResult = mrOK then begin
          SalvaDaWizardAnn(MainForm.xIDUtenteAttuale, xIDRicerca, xidannuncio, 0);
          if InputQuery('Peso', 'Peso', xPeso) then
               TLineeAnnPeso.Value := StrToInt(xPeso);
          TLineeAnn.Post;
     end;
end;

procedure TRicCandAnnunciForm.BCriterioDelClick(Sender: TObject);
begin
     if TLineeAnn.IsEmpty then exit;
     //dxDBGrid1.Visible := False;
     //LTot.Visible := False;
     TLineeAnn.Delete;
     TLineeAnn.Close;
     TLineeAnn.Open;
end;

procedure TRicCandAnnunciForm.BCriterioDelTuttiClick(Sender: TObject);
begin
     if TLineeAnn.IsEmpty then exit;
    // dxDBGrid1.Visible := False;
     Query1.Close;
     Query1.SQl.Clear;
     //Query1.Parameters.AddParameter;
     if xidricerca > 0 then
          Query1.SQl.add('delete from Ann_LineeQuery where IDUtente = :x and IDRicerca=' + IntToStr(xIDRicerca))
     else
          Query1.SQl.add('delete from Ann_LineeQuery where IDUtente = :x and IDAnnuncio=' + IntToStr(xIDAnnuncio));
     Query1.Parameters[0].Value := MainForm.xIDUtenteAttuale;
     Query1.ExecSQL;
     TLineeAnn.Close;
     TLineeAnn.Open;
end;

procedure TRicCandAnnunciForm.FormShow(Sender: TObject);
var i: integer;
begin
     //Grafica
     RicCandAnnunciForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel3.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel3.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel3.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     PanCriteriButtons.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     PanCriteriButtons.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     PanCriteriButtons.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     SelCriterioWizardForm := TSelCriterioWizardForm.create(self);
     if xIDRicerca > 0 then begin
          Q1.Close;
          Q1.SQL.Text := 'select * from RicLineeQuery rq join tabquery tq ' +
               'on rq.desctabella = tq.desctabella and rq.desccampo=tq.desccampo ' +
               'left join ann_lineequery aq ' +
               'on aq.desctabella = tq.desctabella and aq.desccampo=tq.desccampo and rq.descrizione = aq.descrizione ' +
               'where rq.idricerca = :x0: ' +
               'and tq.subqueryann is not null ' +
               'and rq.Descrizione <> isnull(aq.descrizione,''Vuota'') ';
          Q1.ParamByName['x0'] := xIDRicerca;
          Q1.Open;
          if data.Global.fieldbyname('debughrms').asboolean then Q1.SQL.SaveToFile('q1.sql');
          if (Q1.RecordCount > 0) and (MessageDlg('Vuoi usare i criteri gi� inseriti nella sezione "cerca" di questa commessa?', mtConfirmation, [mbYes, mbNo], 0) = mrYes) then begin
               {
               Data.Q1.Close;
               Data.Q1.SQL.Text := 'delete from ann_lineequery where idricerca = :x0: and idutente = :x1:';
               Data.Q1.ParamByName['x0'] := xIDRicerca;
               Data.Q1.ParamByName['x1'] := MainForm.xIDUtenteAttuale;
               Data.Q1.ExecSQL;    }

               TLineeAnn.Sql.text := 'select * from Ann_LineeQuery where idricerca = :x0 ' +
                    'and IDutente = :x1';
               TLineeAnn.Parameters[0].Value := xIDRicerca;
               TLineeAnn.Parameters[1].Value := MainForm.xIDUtenteAttuale;
               TLineeAnn.Open;
              // Q.First;
               while not Q1.Eof do begin
                    if data.Global.fieldbyname('debughrms').asboolean then showmessage(Q1.FieldByName('DescCampo').asString);
                    TLineeAnn.Insert;
                    TLineeAnnIDRicerca.Value := xIDRicerca;
                    TLineeAnnIDUtente.Value := MainForm.xIDUtenteAttuale;
                    TLineeAnnIDAnnuncio.Value := xIDAnnuncio;
                    TLineeAnnIDAnnEdizData.Value := 0;



                    TLineeAnnDescTabella.Value := Q1.FieldByName('DescTabella').asString;
                    TLineeAnnTabella.Value := Q1.FieldByName('Tabella').asString;

                    TLineeAnnDescCampo.Value := Q1.FieldByName('DescCampo').asString;

                    TLineeAnnDescOperatore.Value := Q1.FieldByName('DescOperatore').asString;
                    TLineeAnnValore.Value := Q1.FieldByName('VAlore').AsString;
                    TLineeAnnDescCompleta.Value := Q1.FieldByName('DescCampo').asString + ' ' +
                         Q1.FieldByName('DescOperatore').asString;

                    //TLineeAnnIDTabQueryOp.Value := Q.FieldByName('ID').asInteger;
                    TLineeAnnDescrizione.Value := Q1.FieldByName('Descrizione').asString;
                    if TLineeAnnOpSucc.Value = '' then TLineeAnnOpSucc.Value := 'and';
                    if TLineeAnnOpNext.Value = '' then TLineeAnnOpNext.Value := 'e';
                    TLineeAnn.Post;
                    Q1.Next;
               end;
          end;


     end;

     if xIDAnnuncio > 0 then begin
          TLineeAnn.Sql.text := 'select * from Ann_LineeQuery where idannuncio = :x0 ' +
               'and IDutente = :x1';
          TLineeAnn.Parameters[0].Value := xIDAnnuncio;
          TLineeAnn.Parameters[1].Value := MainForm.xIDUtenteAttuale;
     end else begin
          TLineeAnn.Sql.text := 'select * from Ann_LineeQuery where idricerca = :x0 ' +
               'and IDutente = :x1';
          TLineeAnn.Parameters[0].Value := xIDRicerca;
          TLineeAnn.Parameters[1].Value := MainForm.xIDUtenteAttuale;
     end;

     TLineeAnn.Open;

     if data.Global.fieldbyname('debughrms').asboolean then TLineeAnn.sql.SaveToFile('TLineeAnn.sql');
end;

procedure TRicCandAnnunciForm.FormClose(Sender: TObject;
     var Action: TCloseAction);
begin
     SelCriterioWizardForm.Free;
end;

procedure TRicCandAnnunciForm.RicreaStringaSQLAnn;
var xStringa, xTabella, xCampo, xTipoCampo, xValoriPossibili, xTabLookup, xOperatore, xValore: string;
     xVal1, xVal0, xQueryLookup, xSubQuery: string;
     xIDTabQuery: integer;
begin
     // ricrea la stringa SQL per la riga
     Q.Close;
     Q.SQL.text := 'select * from TabQuery where DescTabella=:xDescTabella: and DescCampo=:xDescCampo:';
     Q.ParamByName['xDescTabella'] := TLineeAnnDescTabella.Value;
     Q.ParamByName['xDescCampo'] := TLineeAnnDescCampo.Value;
     Q.Open;
     // tabella e campo
     xTabella := Q.FieldByName('Tabella').asString;
     xCampo := Q.FieldByName('Campo').asString;
     xTipoCampo := Q.FieldByName('TipoCampo').asString;
     xSubQuery := Q.FieldByName('SubQueryAnn').AsString;
     if (Q.FieldByName('TabLookup').asString <> '') {and (xSubQuery = '')} then
          xQueryLookup := 'select ' + Q.FieldByName('FieldLookup').asString + ' Dicitura from ' + Q.FieldByName('TabLookup').asString + ' where id=:xID:'
     else xQueryLookup := '';
     xValoriPossibili := Q.FieldByName('ValoriPossibili').asString;
     if xValoriPossibili <> '' then begin
          xVal1 := copy(xValoriPossibili, 1, pos(',', xValoriPossibili) - 1);
          xVal0 := copy(xValoriPossibili, pos(',', xValoriPossibili) + 1, length(xValoriPossibili));
     end;
     xTabLookup := Q.FieldByName('TabLookup').asString;
     xIDTabQuery := Q.FieldByName('ID').asInteger;

     // operatore
     Q.Close;
     Q.SQL.text := 'select * from TabQueryOperatori where IDTabQuery=:xIDTabQuery: and DescOperatore=:xDescOperatore:';
     Q.ParamByName['xIDTabQuery'] := xIDTabQuery;
     Q.ParamByName['xDescOperatore'] := TLineeAnnDescOperatore.Value;
     Q.Open;
     xOperatore := Q.FieldByName('Operatore').asString;
     // casistica valori
     if xTipoCampo = '' then begin
          if xOperatore = 'like' then xValore := '''%' + TLineeAnnValore.Value + '%'''
          else xValore := '''' + TLineeAnnValore.Value + '''';
     end;
     if xTipoCampo = 'boolean' then begin
          if TLineeAnnValore.Value = xVal1 then xValore := '1'
          else xValore := '0';
     end;
     if xTipoCampo = 'date' then begin
          xValore := '''' + TLineeAnnValore.Value + '''';
     end;
     if Uppercase(xTipoCampo) = 'DATESP' then begin
          if UpperCase(TLineeAnnDescCampo.Value) = 'COMPLEANNO' then begin
               // xStringa := 'month (' + TTabQueryCampo.AsString + ') =  Month(GetDate()) ' +
                 //    'and day (' + TTabQueryCampo.AsString + ') =  day(GetDate())';

          end;
     end;
     if xTipoCampo = 'integer' then begin
          xValore := TLineeAnnValore.Value;
     end;
     // composizione stringa
     if xSubQuery = '' then begin
          if UpperCase(xTipoCampo) = 'DATESP' then begin
               if UpperCase(TLineeAnnDescCampo.Value) = 'COMPLEANNO' then
                    xStringa := 'month (' + xCampo + ') =  Month(GetDate()) ' +
                         'and day (' + xCampo + ') =  day(GetDate())';
          end else
               xStringa := xTabella + '.' + xCampo + ' ' + xOperatore + ' ' + xValore
     end else begin
          xStringa := StringReplace(xSubQuery, ':xOperatore', xOperatore, [rfReplaceAll, rfIgnoreCase]);
          xStringa := StringReplace(xStringa, ':xValore', xValore, [rfReplaceAll, rfIgnoreCase]);
          {if UpperCase(copy(xSubQuery, 1, 3)) = 'IN ' then
               xStringa := 'Anagrafica.ID' + ' ' + xStringa; }
     end;


     // salvataggio
     TLineeAnnStringa.Value := xStringa;
     TLineeAnnQueryLookup.Value := xQueryLookup;
end;

procedure TRicCandAnnunciForm.TLineeAnnBeforePost(DataSet: TDataSet);
begin
     RicreaStringaSQLAnn;
end;

procedure TRicCandAnnunciForm.DsLineeAnnStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsLineeAnn.State in [dsEdit, dsInsert];
     BCriterioNew.Enabled := not b;
     BCriterioMod.Enabled := not b;
     BCriterioDel.Enabled := not b;
     BCriterioDelTutti.Enabled := not b;
     BLineeOK.Enabled := b;
     BLineeCan.Enabled := b;
end;

procedure TRicCandAnnunciForm.BLineeOKClick(Sender: TObject);
begin
     TLineeAnn.Post;
end;

procedure TRicCandAnnunciForm.BLineeCanClick(Sender: TObject);
begin
     TLineeAnn.Cancel;
end;

procedure TRicCandAnnunciForm.DBGrid3ValoreEditButtonClick(
     Sender: TObject);
begin
     PredisponiWizardAnn;
     // setta variabile "pagina attiva"
     SelCriterioWizardForm.xPaginaAttiva := 3;
     SelCriterioWizardForm.ShowModal;
     if SelCriterioWizardForm.ModalResult = mrOK then begin
          SalvaDaWizardAnn;
          TLineeAnn.Post;
     end;
end;

procedure TRicCandAnnunciForm.DBGrid3CustomDrawCell(Sender: TObject;
     ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
     AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
     var AText: string; var AColor: TColor; AFont: TFont;
     var AAlignment: TAlignment; var ADone: Boolean);
var xID: integer;
begin
     if AColumn = DBGrid3Valore then begin
          if ANode.Strings[DBGrid3QueryLookup.Index] <> '' then begin
               xID := ANode.Values[DBGrid3ID.index];
               Q.Close;
               Q.SQL.text := ANode.Strings[DBGrid3QueryLookup.Index];
               Q.ParamByName['xID'] := StrToIntDef(ANode.Strings[DBGrid3Valore.Index], 0);
               Q.Open;
               AText := Q.FieldByName('Dicitura').asString;
          end;
     end;
end;
end.

