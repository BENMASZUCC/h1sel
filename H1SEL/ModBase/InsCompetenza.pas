unit InsCompetenza;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ExtCtrls, Grids, DBGrids, Db, DBTables, StdCtrls, Buttons, Spin, AdvGrid,
     ADODB, U_ADOLinkCl, BaseGrid, TB97;

type
     TInsCompetenzaForm = class(TForm)
          DsCompArea: TDataSource;
          Panel1: TPanel;
          Panel2: TPanel;
          DsAreeComp: TDataSource;
          DBGrid1: TDBGrid;
          Timer1: TTimer;
          ASG1: TAdvStringGrid;
          Label2: TLabel;
          TAreeComp: TADOLinkedTable;
          TCompArea: TADOLinkedTable;
          TAreeComp1: TADOLinkedQuery;
          TCompArea1: TADOLinkedQuery;
          TAreeComp1ID: TAutoIncField;
          TAreeComp1Descrizione: TStringField;
          TAreeComp1IDAzienda: TIntegerField;
          Panel3: TPanel;
          BitBtn1: TToolbarButton97;
          BAnnulla: TToolbarButton97;
    SpeedButton1: TToolbarButton97;
          procedure BitBtn1Click(Sender: TObject);
          procedure DBGrid1KeyPress(Sender: TObject; var Key: Char);
          procedure Timer1Timer(Sender: TObject);
          procedure ASG1GetEditorType(Sender: TObject; aCol, aRow: Integer;
               var aEditor: TEditorType);
          procedure TAreeComp_oldAfterScroll(DataSet: TDataSet);
          procedure FormShow(Sender: TObject);
          procedure TAreeComp1AfterScroll(DataSet: TDataSet);
          procedure SpeedButton1Click(Sender: TObject);
          procedure TAreeCompAfterScroll(DataSet: TDataSet);
          procedure BAnnullaClick(Sender: TObject);
     private
          xStringa: string;
     public
          xArrayIDComp: array[1..100] of integer;
          xArrayCompVal: array[1..100] of integer;
          procedure RiempiGriglia;
     end;

var
     InsCompetenzaForm: TInsCompetenzaForm;

implementation

{$R *.DFM}
uses
     //[/TONI20020729\]
     uUtilsVarie, ModuloDati, Main;
//[/TONI20020729\]FINE

procedure TInsCompetenzaForm.BitBtn1Click(Sender: TObject);
var i, xTot: integer;
     Vero: boolean;
begin
     InsCompetenzaForm.ModalResult := mrOk;
     xTot := 0;
     for i := 1 to ASG1.RowCount - 1 do begin
          ASG1.GetCheckBoxState(0, i, Vero);
          if Vero then begin
               inc(xTot);
               //showmessage(ASG1.cells[1, i]);
               xArrayCompVal[i] := StrToIntDef(ASG1.cells[1, i], 0);
               //xArrayCompVal[i] := StrToInt(ASG1.cells[1, i]);
          end;
     end;
     if xtot = 0 then begin
          MessageDlg('Nessuna competenza selezionata !', mtError, [mbOK], 0);
          ModalResult := mrNone;
          Abort;
     end;
end;

procedure TInsCompetenzaForm.DBGrid1KeyPress(Sender: TObject;
     var Key: Char);
begin
     Timer1.Enabled := False;
     if Key = chr(13) then begin
          ASG1.SetFocus;
     end else begin
          xStringa := xStringa + key;
          TAreeComp.Locate('DEscrizione', VarArrayOF([xStringa]), []);
     end;
     Timer1.Enabled := True;
end;

procedure TInsCompetenzaForm.Timer1Timer(Sender: TObject);
begin
     xStringa := '';
end;

procedure TInsCompetenzaForm.RiempiGriglia;
var i: integer;
begin
     if TCompArea.EOF then
          ASG1.Visible := False
     else begin
          TCompArea.Sort := 'Descrizione';
          ASG1.Clear;
          ASG1.RowCount := TCompArea.RecordCount + 1;
          ASG1.FixedRows := 1;
          ASG1.Cells[0, 0] := 'competenza';
          ASG1.Cells[1, 0] := 'valore';
          ASG1.RowHeights[0] := 20;
          for i := 1 to ASG1.RowCount - 1 do begin
               ASG1.RowHeights[i] := 18;
               ASG1.addcheckbox(0, i, false, false);
          end;

          //          TCompArea.Close;
          //          TCompArea.Open;
          TCompArea.First;
          i := 1;
          while not TCompArea.EOF do begin

               //[/TONI20020729\]
               //          ASG1.Cells[0,i]:=TCompAreaDescrizione.Value;
               ASG1.Cells[0, i] := TCompArea.FieldByName('Descrizione').Value;
               ASG1.Cells[1, i] := '0';
               xArrayIDComp[i] := TCompArea.FieldByname('ID').Value;
               //[/TONI20020729\]FINE
               TCompArea.next;
               inc(i);
          end;
          ASG1.Visible := True;
     end;
end;

procedure TInsCompetenzaForm.ASG1GetEditorType(Sender: TObject; aCol,
     aRow: Integer; var aEditor: TEditorType);
begin
     //if acol = 1 then aEditor := edSpinEdit;
     if acol = 2 then aEditor := edSpinEdit;
end;

procedure TInsCompetenzaForm.TAreeComp_oldAfterScroll(DataSet: TDataSet);
begin
     if not TCompArea.Active then TCompArea.Active := TRUE;
     RiempiGriglia;
end;

procedure TInsCompetenzaForm.FormShow(Sender: TObject);
begin
//Grafica
     InsCompetenzaForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel3.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel3.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel3.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     Caption := '[M/020] - ' + Caption;

     TAreeComp.Open;
     TAreeComp.Sort := 'Descrizione';
end;

procedure TInsCompetenzaForm.TAreeComp1AfterScroll(DataSet: TDataSet);
begin
     //TCompArea.Close;
     //TCompArea.SQL.Text:='Select * from competenze where IDArea = ' + inttostr(TAreeCompID.Value);
     //TCompArea.Open;
end;

procedure TInsCompetenzaForm.SpeedButton1Click(Sender: TObject);
begin
     ApriHelp('020');
end;

procedure TInsCompetenzaForm.TAreeCompAfterScroll(DataSet: TDataSet);
begin
     if not TCompArea.Active then TCompArea.Active := TRUE;
     TCompArea.Sort := 'Descrizione';
     RiempiGriglia;
end;

procedure TInsCompetenzaForm.BAnnullaClick(Sender: TObject);
begin
     InsCompetenzaForm.ModalResult := mrCancel;
end;

end.

