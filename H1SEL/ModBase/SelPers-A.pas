unit SelPers;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     StdCtrls,Mask,DBCtrls,Grids,DBGrids,Db,DBTables,Buttons,TB97,
     ExtCtrls,ComCtrls,DtEdit97,DtEdDB97,ComObj,RXDBCtrl,CliCandidati,
     RXSpin,dxCntner,dxEditor,dxExEdtr,dxEdLib,dxDBELib,dxDBTLCl,
     dxGrClms,dxDBGrid,dxTL,dxDBCtrl,dxGridMenus,dxPSdxDBGrLnk,dxPSCore,
     dxPSdxTLLnk,Menus,ImgList,FrameDBRichEdit2,dxTLClms,dxGrClEx,
     dxLayout,ShellApi,dxPSdxDBCtrlLnk;

type
     TSelPersForm=class(TForm)
          Panel2: TPanel;
          GroupBox7: TGroupBox;
          DBEdit1: TDBEdit;
          GroupBox5: TGroupBox;
          DbDateEdit971: TDbDateEdit97;
          GroupBox3: TGroupBox;
          SpeedButton1: TSpeedButton;
          DBEdit3: TDBEdit;
          DBEdit4: TDBEdit;
          GroupBox2: TGroupBox;
          Label6: TLabel;
          DBEdit2: TDBEdit;
          GBCliente: TGroupBox;
          SpeedButton2: TSpeedButton;
          DBEdit5: TDBEdit;
          GroupBox4: TGroupBox;
          BitBtn1: TBitBtn;
          Ora: TTimer;
          TimerInattivo: TTimer;
          GroupBox1: TGroupBox;
          DBEdit6: TDBEdit;
          BStatoRic: TSpeedButton;
          BStoricoRic: TSpeedButton;
          DBEdit7: TDBEdit;
          Label4: TLabel;
          Q: TQuery;
          Panel4: TPanel;
          ToolbarButton975: TToolbarButton97;
          DBEdit8: TDBEdit;
          BUtenteMod: TSpeedButton;
          GroupBox6: TGroupBox;
          BTipo: TSpeedButton;
          DBEdit9: TDBEdit;
          PCSelPers: TPageControl;
          TSCandidati: TTabSheet;
          TSCandAnnunci: TTabSheet;
          Panel1: TPanel;
          Panel5: TPanel;
          ToolbarButton9712: TToolbarButton97;
          ToolbarButton9713: TToolbarButton97;
          BCurriculum: TToolbarButton97;
          BElimina: TToolbarButton97;
          ToolbarButton974: TToolbarButton97;
          ToolbarButton978: TToolbarButton97;
          TbBAltroNome: TToolbarButton97;
          BInfoCand: TToolbarButton97;
          ToolbarButton972: TToolbarButton97;
          ToolbarButton973: TToolbarButton97;
          Bevel1: TBevel;
          Bevel2: TBevel;
          Bevel3: TBevel;
          Bevel4: TBevel;
          BRecupera: TToolbarButton97;
          BStampaElenco: TToolbarButton97;
          Bevel5: TBevel;
          Panel7: TPanel;
          Panel8: TPanel;
          LTotCand: TLabel;
          CBOpzioneCand: TCheckBox;
          Panel3: TPanel;
          SpeedButton3: TSpeedButton;
          Panel6: TPanel;
          Panel9: TPanel;
          Panel10: TPanel;
          LTotCandAnn: TLabel;
          BCurriculum1: TToolbarButton97;
          BInfoCand2: TToolbarButton97;
          ToolbarButton977: TToolbarButton97;
          ToolbarButton979: TToolbarButton97;
          Bevel6: TBevel;
          Bevel7: TBevel;
          ToolbarButton971: TToolbarButton97;
          Bevel8: TBevel;
          Panel11: TPanel;
          Label5: TLabel;
          Shape1: TShape;
          Label7: TLabel;
          ToolbarButton976: TToolbarButton97;
          TSAltriDati: TTabSheet;
          Panel12: TPanel;
          Panel13: TPanel;
          Panel14: TPanel;
          Panel15: TPanel;
          GroupBox8: TGroupBox;
          SpeedButton4: TSpeedButton;
          DBEdit10: TDBEdit;
          GroupBox9: TGroupBox;
          SpeedButton5: TSpeedButton;
          DBEdit11: TDBEdit;
          GroupBox10: TGroupBox;
          BUtente2: TSpeedButton;
          TSCliCand: TTabSheet;
          CliCandidatiFrame1: TCliCandidatiFrame;
          PanTiming: TPanel;
          Label1: TLabel;
          Label2: TLabel;
          Label3: TLabel;
          LTiming: TLabel;
          SecondiAttivita: TEdit;
          SecondiTotali: TEdit;
          TSRicParams: TTabSheet;
          Panel16: TPanel;
          Panel17: TPanel;
          Label13: TLabel;
          Label14: TLabel;
          Label15: TLabel;
          Panel18: TPanel;
          Label16: TLabel;
          Label17: TLabel;
          Label9: TLabel;
          Panel19: TPanel;
          Label19: TLabel;
          Label20: TLabel;
          Label10: TLabel;
          Panel20: TPanel;
          Label11: TLabel;
          dxDBSpinEdit1: TdxDBSpinEdit;
          dxDBSpinEdit2: TdxDBSpinEdit;
          dxDBSpinEdit3: TdxDBSpinEdit;
          GroupBox11: TGroupBox;
          DBDataPrev: TDbDateEdit97;
          BPassword: TSpeedButton;
          Panel21: TPanel;
          Label12: TLabel;
          DBGrid1: TDBGrid;
          GroupBox12: TGroupBox;
          Label18: TLabel;
          ERifOfferta: TEdit;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1ID: TdxDBGridMaskColumn;
          dxDBGrid1IDRicerca: TdxDBGridMaskColumn;
          dxDBGrid1IDAnagrafica: TdxDBGridMaskColumn;
          dxDBGrid1Escluso: TdxDBGridCheckColumn;
          dxDBGrid1DataImpegno: TdxDBGridDateColumn;
          dxDBGrid1Codstato: TdxDBGridMaskColumn;
          dxDBGrid1Stato: TdxDBGridMaskColumn;
          dxDBGrid1Cognome: TdxDBGridMaskColumn;
          dxDBGrid1Nome: TdxDBGridMaskColumn;
          dxDBGrid1CVNumero: TdxDBGridMaskColumn;
          dxDBGrid1TipoStato: TdxDBGridMaskColumn;
          dxDBGrid1DataIns: TdxDBGridDateColumn;
          dxDBGrid1MiniVal: TdxDBGridMaskColumn;
          dxDBGrid1IDStato: TdxDBGridMaskColumn;
          dxDBGrid1DataUltimoContatto: TdxDBGridDateColumn;
          dxDBGrid1TelUffCell: TdxDBGridColumn;
          dxDBGrid1Cellulare: TdxDBGridMaskColumn;
          dxDBGrid1telUfficio: TdxDBGridMaskColumn;
          dxDBGrid1DataNascita: TdxDBGridDateColumn;
          dxDBGrid1Eta: TdxDBGridColumn;
          dxDBGrid1Azienda: TdxDBGridLookupColumn;
          PMStampa: TPopupMenu;
          Stampaelenco1: TMenuItem;
          EsportainExcel1: TMenuItem;
          esportainHTML1: TMenuItem;
          dxPrinter1: TdxComponentPrinter;
          dxPrinter1Link1: TdxDBGridReportLink;
          ImageList1: TImageList;
          BModCodCommessa: TSpeedButton;
          FrameDBRich1: TFrameDBRich;
          TSGestione: TTabSheet;
          Panel22: TPanel;
          GBProvvigione: TGroupBox;
          Label21: TLabel;
          Label22: TLabel;
          DBCheckBox1: TDBCheckBox;
          DBEdit12: TDBEdit;
          DBComboBox1: TDBComboBox;
          GBOreLav: TGroupBox;
          EUtenteLav1: TEdit;
          EUtenteLav2: TEdit;
          EUtenteLav3: TEdit;
          SEOreLav1: TdxDBSpinEdit;
          SEOreLav2: TdxDBSpinEdit;
          SEOreLav3: TdxDBSpinEdit;
          GBRiepTiming: TGroupBox;
          dxDBGrid2: TdxDBGrid;
          dxDBGrid2Nominativo: TdxDBGridMaskColumn;
          dxDBGrid2Tot: TdxDBGridMaskColumn;
          GBCalcolo: TGroupBox;
          CE1: TdxCurrencyEdit;
          Label26: TLabel;
          Label27: TLabel;
          CE2: TdxCurrencyEdit;
          Label28: TLabel;
          Label29: TLabel;
          CE3: TdxCurrencyEdit;
          Label30: TLabel;
          Label31: TLabel;
          Label32: TLabel;
          Label33: TLabel;
          Label34: TLabel;
          Label35: TLabel;
          Label36: TLabel;
          CE4: TdxCurrencyEdit;
          Panel23: TPanel;
          ToolbarButton9710: TToolbarButton97;
          Label37: TLabel;
          CETot: TdxCurrencyEdit;
          Bevel9: TBevel;
          Bevel10: TBevel;
          Bevel11: TBevel;
          Bevel12: TBevel;
          Bevel13: TBevel;
          Label38: TLabel;
          Label39: TLabel;
          Label40: TLabel;
          CE5: TdxCurrencyEdit;
          Bevel14: TBevel;
          Label23: TLabel;
          ETotOreLav1: TEdit;
          Label24: TLabel;
          Label25: TLabel;
          Label41: TLabel;
          Label42: TLabel;
          TSTimeSheet: TTabSheet;
          DBGrid2: TDBGrid;
          SpeedButton6: TSpeedButton;
          SpeedButton7: TSpeedButton;
          PanTimeSheet: TPanel;
          TSTargetList: TTabSheet;
          Panel26: TPanel;
          Panel27: TPanel;
          BRicClienti: TToolbarButton97;
          BTargetListStampa: TToolbarButton97;
          PMStampaTL: TPopupMenu;
          MenuItem1: TMenuItem;
          MenuItem2: TMenuItem;
          MenuItem3: TMenuItem;
          dxPrinter1Link2: TdxDBGridReportLink;
          BTLOrg: TToolbarButton97;
          Bevel15: TBevel;
          Bevel16: TBevel;
          Bevel17: TBevel;
          BTLDelete: TToolbarButton97;
          Bevel18: TBevel;
          Panel28: TPanel;
          dxDBGrid3: TdxDBGrid;
          dxDBGridMaskColumn1: TdxDBGridMaskColumn;
          dxDBGridMaskColumn2: TdxDBGridMaskColumn;
          dxDBGrid1Attivita: TdxDBGridMaskColumn;
          dxDBGrid1telefono: TdxDBGridMaskColumn;
          dxDBGrid1Comune: TdxDBGridMaskColumn;
          dxDBGrid1DataUltimaEsploraz: TdxDBGridDateColumn;
          dxDBGrid3Column7: TdxDBGridBlobColumn;
          Splitter1: TSplitter;
          Panel29: TPanel;
          dxDBGrid4: TdxDBGrid;
          BTLCandVai: TToolbarButton97;
          Bevel19: TBevel;
          Bevel20: TBevel;
          dxDBGrid4ID: TdxDBGridMaskColumn;
          dxDBGrid4IDAnagrafica: TdxDBGridMaskColumn;
          dxDBGrid4CVNumero: TdxDBGridMaskColumn;
          dxDBGrid4Cognome: TdxDBGridMaskColumn;
          dxDBGrid4Nome: TdxDBGridMaskColumn;
          dxDBGrid4IDStato: TdxDBGridMaskColumn;
          dxDBGrid4Cellulare: TdxDBGridMaskColumn;
          dxDBGrid4TelUfficio: TdxDBGridMaskColumn;
          dxDBGrid4DataNascita: TdxDBGridDateColumn;
          dxDBGrid4Eta: TdxDBGridColumn;
          BTLElencoCli: TToolbarButton97;
          BTLCandNew: TToolbarButton97;
          BTLCandInsNuovo: TToolbarButton97;
          ToolbarButton9715: TToolbarButton97;
          PMStampaTLCand: TPopupMenu;
          MenuItem4: TMenuItem;
          MenuItem5: TMenuItem;
          MenuItem6: TMenuItem;
          dxPrinter1Link3: TdxDBGridReportLink;
          Panel24: TPanel;
          BTimeSheetNew: TToolbarButton97;
          BTimeSheetDel: TToolbarButton97;
          BTimeSheetCan: TToolbarButton97;
          BTimeSheetOK: TToolbarButton97;
          dxDBGrid5: TdxDBGrid;
          dxDBGridLayoutList1: TdxDBGridLayoutList;
          dxDBGridLayoutList1Item1: TdxDBGridLayout;
          dxDBGrid5Data: TdxDBGridDateColumn;
          dxDBGrid5OreConsuntivo: TdxDBGridMaskColumn;
          dxDBGrid5Utente: TdxDBGridExtLookupColumn;
          dxDBGrid5Note: TdxDBGridBlobColumn;
          dxDBGridLayoutList1Item2: TdxDBGridLayout;
          dxDBGrid5Column5: TdxDBGridExtLookupColumn;
          PMTargetList: TPopupMenu;
          modificanumeroditelefono1: TMenuItem;
          Label43: TLabel;
          DBEdit13: TDBEdit;
          BTLNoteCliente: TToolbarButton97;
          Splitter2: TSplitter;
          Panel25: TPanel;
          Panel30: TPanel;
          Panel31: TPanel;
          BTRepDettNew: TToolbarButton97;
          BTRepDettDel: TToolbarButton97;
          BTRepDettCan: TToolbarButton97;
          BTRepDettOK: TToolbarButton97;
          dxDBGrid6: TdxDBGrid;
          dxDBGrid6Ore: TdxDBGridMaskColumn;
          dxDBGrid6Causale: TdxDBGridLookupColumn;
          BAggTotTRep: TToolbarButton97;
          dxDBGrid6Note: TdxDBGridBlobColumn;
          ToolbarButton9711: TToolbarButton97;
          dxDBGrid1TelAzienda: TdxDBGridColumn;
          PMCand: TPopupMenu;
          modificaaziendaattualecandidato1: TMenuItem;
          modificanumeroditelefonodufficio1: TMenuItem;
          modificanumeroditelefonocellulare1: TMenuItem;
          TSFile: TTabSheet;
          Panel123: TPanel;
          BAnagFileNew: TToolbarButton97;
          BAnagFileMod: TToolbarButton97;
          BAnagFileDel: TToolbarButton97;
          BFileCandOpen: TToolbarButton97;
          QRicFile: TQuery;
          DsQRicFile: TDataSource;
          dxDBGrid7: TdxDBGrid;
          dxDBGrid7Descrizione: TdxDBGridMaskColumn;
          dxDBGrid7NomeFile: TdxDBGridMaskColumn;
          dxDBGrid7DataCreazione: TdxDBGridDateColumn;
          QRicFileID: TAutoIncField;
          QRicFileIDRicerca: TIntegerField;
          QRicFileIDTipo: TIntegerField;
          QRicFileDescrizione: TStringField;
          QRicFileNomeFile: TStringField;
          QRicFileDataCreazione: TDateTimeField;
          dxDBGrid1Column24: TdxDBGridColumn;
          CBOpzioneCand2: TCheckBox;
          DBGAnnunciCand: TdxDBGrid;
          DBGAnnunciCandRif: TdxDBGridMaskColumn;
          DBGAnnunciCandTestata: TdxDBGridMaskColumn;
          DBGAnnunciCandNomeEdizione: TdxDBGridMaskColumn;
          DBGAnnunciCandData: TdxDBGridDateColumn;
          DBGAnnunciCandCVNumero: TdxDBGridMaskColumn;
          DBGAnnunciCandCognome: TdxDBGridMaskColumn;
          DBGAnnunciCandNome: TdxDBGridMaskColumn;
          DBGAnnunciCandCVInseritoIndata: TdxDBGridDateColumn;
          DBGAnnunciCandIDAnagrafica: TdxDBGridColumn;
          dxDBGrid4Ruolo: TdxDBGridColumn;
          BTLDelSogg: TToolbarButton97;
          CBColoriCandAnnunci: TCheckBox;
          dxDBGrid1Column25: TdxDBGridColumn;
          dxDBGrid1Note: TdxDBGridColumn;
    PanCausali: TPanel;
    Label8: TLabel;
    DBLookupComboBox1: TDBLookupComboBox;
    BTabCausali: TSpeedButton;
          procedure BitBtn4Click(Sender: TObject);
          procedure BitBtn7Click(Sender: TObject);
          procedure BCurriculumClick(Sender: TObject);
          procedure ToolbarButton978Click(Sender: TObject);
          procedure BEliminaClick(Sender: TObject);
          procedure TbBAltroNomeClick(Sender: TObject);
          procedure SpeedButton1Click(Sender: TObject);
          procedure SpeedButton2Click(Sender: TObject);
          procedure ToolbarButton974Click(Sender: TObject);
          procedure ToolbarButton9712Click(Sender: TObject);
          procedure CBOpzioneCandClick(Sender: TObject);
          procedure ToolbarButton9713Click(Sender: TObject);
          procedure BInfoCandClick(Sender: TObject);
          procedure ToolbarButton972Click(Sender: TObject);
          procedure ToolbarButton973Click(Sender: TObject);
          procedure BRecuperaClick(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure OraTimer(Sender: TObject);
          procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X,
               Y: Integer);
          procedure TimerInattivoTimer(Sender: TObject);
          procedure FormCreate(Sender: TObject);
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
          procedure BStatoRicClick(Sender: TObject);
          procedure BStoricoRicClick(Sender: TObject);
          procedure ToolbarButton975Click(Sender: TObject);
          procedure BUtenteModClick(Sender: TObject);
          procedure SpeedButton3Click(Sender: TObject);
          procedure BTipoClick(Sender: TObject);
          procedure PCSelPersChange(Sender: TObject);
          procedure BInfoCand2Click(Sender: TObject);
          procedure BCurriculum1Click(Sender: TObject);
          procedure ToolbarButton977Click(Sender: TObject);
          procedure ToolbarButton979Click(Sender: TObject);
          procedure ToolbarButton971Click(Sender: TObject);
          procedure ToolbarButton976Click(Sender: TObject);
          procedure SpeedButton4Click(Sender: TObject);
          procedure SpeedButton5Click(Sender: TObject);
          procedure BUtente2Click(Sender: TObject);
          procedure BPasswordClick(Sender: TObject);
          procedure DbDateEdit971Exit(Sender: TObject);
          procedure DbDateEdit971Enter(Sender: TObject);
          procedure dxDBGrid1MouseUp(Sender: TObject; Button: TMouseButton;
               Shift: TShiftState; X,Y: Integer);
          procedure dxDBGrid1Click(Sender: TObject);
          procedure dxDBGrid1DblClick(Sender: TObject);
          procedure dxDBGrid1CustomDrawCell(Sender: TObject; ACanvas: TCanvas;
               ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
               ASelected,AFocused,ANewItemRow: Boolean; var AText: string;
               var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
               var ADone: Boolean);
          procedure Stampaelenco1Click(Sender: TObject);
          procedure EsportainExcel1Click(Sender: TObject);
          procedure esportainHTML1Click(Sender: TObject);
          procedure BModCodCommessaClick(Sender: TObject);
          procedure ToolbarButton9710Click(Sender: TObject);
          procedure SEOreLav1Exit(Sender: TObject);
          procedure SpeedButton6Click(Sender: TObject);
          procedure SpeedButton7Click(Sender: TObject);
          procedure dxDBGrid3MouseUp(Sender: TObject; Button: TMouseButton;
               Shift: TShiftState; X,Y: Integer);
          procedure BRicClientiClick(Sender: TObject);
          procedure MenuItem1Click(Sender: TObject);
          procedure MenuItem2Click(Sender: TObject);
          procedure MenuItem3Click(Sender: TObject);
          procedure BTLOrgClick(Sender: TObject);
          procedure BTLDeleteClick(Sender: TObject);
          procedure BTLCandVaiClick(Sender: TObject);
          procedure BTLElencoCliClick(Sender: TObject);
          procedure BTLCandNewClick(Sender: TObject);
          procedure dxDBGrid4DblClick(Sender: TObject);
          procedure BTLCandInsNuovoClick(Sender: TObject);
          procedure dxDBGrid4CustomDrawCell(Sender: TObject; ACanvas: TCanvas;
               ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
               ASelected,AFocused,ANewItemRow: Boolean; var AText: string;
               var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
               var ADone: Boolean);
          procedure dxDBGrid4MouseUp(Sender: TObject; Button: TMouseButton;
               Shift: TShiftState; X,Y: Integer);
          procedure MenuItem4Click(Sender: TObject);
          procedure MenuItem5Click(Sender: TObject);
          procedure MenuItem6Click(Sender: TObject);
          procedure BTimeSheetNewClick(Sender: TObject);
          procedure BTimeSheetDelClick(Sender: TObject);
          procedure BTimeSheetOKClick(Sender: TObject);
          procedure BTimeSheetCanClick(Sender: TObject);
          procedure modificanumeroditelefono1Click(Sender: TObject);
          procedure BTLNoteClienteClick(Sender: TObject);
          procedure BTRepDettNewClick(Sender: TObject);
          procedure BTRepDettDelClick(Sender: TObject);
          procedure BTRepDettOKClick(Sender: TObject);
          procedure BTRepDettCanClick(Sender: TObject);
          procedure BAggTotTRepClick(Sender: TObject);
          procedure ToolbarButton9711Click(Sender: TObject);
          procedure modificaaziendaattualecandidato1Click(Sender: TObject);
          procedure modificanumeroditelefonodufficio1Click(Sender: TObject);
          procedure modificanumeroditelefonocellulare1Click(Sender: TObject);
          procedure BAnagFileNewClick(Sender: TObject);
          procedure BAnagFileModClick(Sender: TObject);
          procedure BAnagFileDelClick(Sender: TObject);
          procedure BFileCandOpenClick(Sender: TObject);
          procedure CBOpzioneCand2Click(Sender: TObject);
          procedure DBGAnnunciCandCustomDrawCell(Sender: TObject; ACanvas: TCanvas;
               ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
               ASelected,AFocused,ANewItemRow: Boolean; var AText: string;
               var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
               var ADone: Boolean);
          procedure BTLDelSoggClick(Sender: TObject);
          procedure CBColoriCandAnnunciClick(Sender: TObject);
          procedure dxDBGrid3ChangeColumn(Sender: TObject; Node: TdxTreeListNode;
               Column: Integer);
          procedure dxDBGrid3ContextPopup(Sender: TObject; MousePos: TPoint;
               var Handled: Boolean);
    procedure BTabCausaliClick(Sender: TObject);
     private
          xStringa: string;
          xAttivo,xCerca: boolean;
          xMinStart: integer;
          xColumnAttuale,xColumnAttualeAnn: TColumn;
          xggDiffDataIns,xggDiffDataUltimoContatto: Integer;
          xVecchiadataInizio: TDateTime;
          xDisabilitaTargetList: Boolean;
          xColonnaSel: Integer;
          function LogEsportazioniCand(xDaDove: integer): boolean;
     public
          xIDOrg: integer;
          xFromOrgTL,xCheckRicCandCV,xCheckRicCandTel: boolean;
          procedure EseguiQueryCandidati;
          procedure RiprendiAttivita;
          function CheckStoricoTL(xIDAzienda: integer): boolean;
     end;

var
     SelPersForm: TSelPersForm;

implementation

uses MDRicerche,Curriculum,ModuloDati,ContattoCand,
     StoriaContatti,ValutazDip,EsitoColloquio,ElencoDip,Main,SelData,
     InsRuolo,ModuloDati2,SelCliente,View,FaxPresentaz,CambiaStato2,
     uGestEventi,SelPersNew,SchedaCand,uUtilsVarie,RepElencoSel,
     ModDettCandRic,ModStatoRic,StoricoRic,OpzioniEliminaz,Specifiche,
     ElencoUtenti,LegendaGestRic,SchedaSintetica,SelTipoCommessa,
     DettAnnuncio,ElencoSedi,SelContatti,CheckPass,UnitPrintMemo,
     RicClienti,NuovoSoggetto,StoricoTargetList,NoteCliente,
     SceltaModelloMailCli,RicFile;

{$R *.DFM}


procedure TSelPersForm.BitBtn4Click(Sender: TObject);
begin
     close;
end;

procedure TSelPersForm.BitBtn7Click(Sender: TObject);
begin
     close
end;

//[ALBERTO 20020902]

procedure TSelPersForm.BCurriculumClick(Sender: TObject);
var x: string;
     xIDAnag,i: integer;
begin
     if not Mainform.CheckProfile('051') then Exit;
     if not DataRicerche.QCandRic.Eof then begin
          PosizionaAnag(DataRicerche.QCandRicFieldByName('IDAnagrafica').AsInteger);
          Data.TTitoliStudio.Open;
          Data.TCorsiExtra.Open;
          Data.TLingueConosc.Open;
          Data.TEspLav.Open;
          CurriculumForm.ShowModal;

          Data.TTitoliStudio.Close;
          Data.TCorsiExtra.Close;
          Data.TLingueConosc.Close;
          Data.TEspLav.Close;
          MainForm.Pagecontrol5.ActivePage:=MainForm.TSStatoTutti;
          RiprendiAttivita;
          xIDAnag:=DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger;
          DataRicerche.QCandRic.Close;
          DataRicerche.QCandRic.Open;
          DataRicerche.QCandRic.Locate('IDAnagrafica',xIDAnag, []);
     end;
end;

procedure TSelPersForm.ToolbarButton978Click(Sender: TObject);
begin
     if not MainForm.CheckProfile('216') then Exit;
     if not DataRicerche.QCandRic.Eof then begin
          StoriaContattiForm:=TStoriaContattiForm.create(self);
          {
          StoriaContattiForm.QStoriaCont.ParamByName['xIDRicerca']:=Dataricerche.TRicerchePendID.Value;
          StoriaContattiForm.QStoriaCont.ParamByName['xIDAnagrafica']:=DataRicerche.QCandRicIDAnagrafica.Value;
          }
          StoriaContattiForm.QStoriaCont.ParamByName['xIDRicerca']:=Dataricerche.TRicerchePend.FieldByName('ID').AsInteger;
          StoriaContattiForm.QStoriaCont.ParamByName['xIDAnagrafica']:=DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger;
          StoriaContattiForm.QStoriaCont.Open;
          StoriaContattiForm.ShowModal;
          StoriaContattiForm.Free;
          RiprendiAttivita;
     end;
end;

procedure TSelPersForm.BEliminaClick(Sender: TObject);
var xPerche: string;
begin
     if not Mainform.CheckProfile('211') then Exit;
     if DataRicerche.QCandRic.FieldByName('Escluso').AsBoolean then begin
          MessageDlg('Il candidato � gi� escluso',mtError, [mbOK],0);
          exit;
     end;
     if not DataRicerche.QCandRic.Eof then begin
          if InputQuery('Elimina dalla ricerca','Motivo:',xPerche) then begin
               Data.DB.BeginTrans;
               try
                    DataRicerche.QGen.SQL.Clear;
                    DataRicerche.QGen.SQL.Add('update EBC_CandidatiRicerche set Escluso=1,');
                    DataRicerche.QGen.SQL.Add('Stato="eliminato (motivo: '+xPerche+')" where IDAnagrafica='+DataRicerche.QCandRicIDAnagrafica.asString+' and IDRicerca='+DataRicerche.QCandRicIDRicerca.asString);
                    DataRicerche.QGen.execSQL;
                    // aggiornamento storico
                    if Q.Active then Q.Close;
                    Q.SQL.text:='insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) '+
                         'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xIDCliente:)';
                    Q.ParamByName['xIDAnagrafica']:=DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger;
                    Q.ParamByName['xIDEvento']:=17;
                    Q.ParamByName['xDataEvento']:=date;
                    Q.ParamByName['xAnnotazioni']:='ric.'+DataRicerche.TRicerchePendProgressivo.asString+' per '+DataRicerche.TRicerchePendCliente.asString+' (motivo: '+xPerche+')';
                    Q.ParamByName['xIDRicerca']:=DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
                    Q.ParamByName['xIDUtente'] :=DataRicerche.TRicerchePend.FieldByName('IDUtente').AsInteger;
                    Q.ParamByName['xIDCliente']:=DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger;
                    Q.ExecSQL;
                    // controllo che non sia associato a ricerche diverse da questa dove non � gi� escluso
                    DataRicerche.QGen.Close;
                    DataRicerche.QGen.SQL.Clear;
                    DataRicerche.QGen.SQL.Add('select IDRicerca from EBC_CandidatiRicerche where IDAnagrafica='+DataRicerche.QCandRicIDAnagrafica.asString+' and IDRicerca<>'+DataRicerche.QCandRicIDRicerca.asString+' and Escluso=0');
                    DataRicerche.QGen.Open;
                    if not DataRicerche.QGen.IsEmpty then begin
                         // lasciato in selezione
                         MessageDlg('Il candidato � associato ad altre ricerche dove non � gi� escluso - verr� quindi mantenuto in selezione',mtInformation, [mbOK],0);
                    end else begin
                         // archiviazione CV
                         if Q.Active then Q.Close;
                         Q.SQL.text:='update Anagrafica set IDStato=28,IDTipoStato=2 where ID='+DataRicerche.QCandRicIDAnagrafica.asString;
                         Q.ExecSQL;
                    end;
                    Data.DB.CommitTrans;
                    DataRicerche.QCandRic.Close;
                    DataRicerche.QCandRic.Open;
                    LTotCand.Caption:=IntToStr(DataRicerche.QCandRic.RecordCount);
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('Errore sul database:  inserimento non avvenuto',mtError, [mbOK],0);
               end;
          end;
          RiprendiAttivita;
     end;
end;

procedure TSelPersForm.TbBAltroNomeClick(Sender: TObject);
var xVai: boolean;
     xRic,xLivProtez,xDicMess,xPassword,xUtenteResp,xClienteBlocco: string;
     xIDEvento,xIDClienteBlocco: integer;
     QLocal: TQuery;
begin
     if not Mainform.CheckProfile('210') then Exit;
     ElencoDipForm:=TElencoDipForm.create(self);
     ElencoDipForm.ShowModal;
     if ElencoDipForm.ModalResult=mrOK then begin
          // controllo propriet� CV
          if (not((ElencoDipForm.TAnagDip.FieldByName('IDProprietaCV').asString='')or(ElencoDipForm.TAnagDip.FieldByName('IDProprietaCV').AsInteger=0)))and
               (ElencoDipForm.TAnagDip.FieldByName('IDProprietaCV').AsInteger<>DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger) then
               if MessageDlg('ATTENZIONE: il CV risulta di propriet� di un altro cliente .'+chr(13)+
                    'Vuoi proseguire lo stesso ?',mtWarning, [mbYes,mbNo],0)=mrNo then begin
                    ElencoDipForm.Free;
                    RiprendiAttivita;
                    exit;
               end;

          DataRicerche.TRicAnagIDX.Open;
          if not DataRicerche.TRicAnagIDX.FindKey([DataRicerche.TRicerchePend.FieldByName('ID').AsInteger,ElencoDipForm.TAnagDip.FieldByName('ID').AsInteger]) then begin
               xVai:=True;
               // controllo se � in selezione
               if ElencoDipForm.TAnagDip.FieldByName('IDTipoStato').AsInteger =1 then begin
                    if MessageDlg('Il soggetto risulta IN SELEZIONE: '+chr(13)+xRic+chr(13)+'PROCEDERE COMUNQUE ?',mtWarning, [mbYes,mbNo],0)=mrNo then xVai:=False;
               end;
               // controllo se � gi� associato ad un'altra ricerca per la STESSA azienda
               Q.Close;
               Q.SQL.clear;
               Q.SQl.Add('select EBC_Ricerche.Progressivo,EBC_StatiRic.StatoRic from EBC_CandidatiRicerche,EBC_Ricerche,EBC_StatiRic ');
               Q.SQl.Add('where EBC_CandidatiRicerche.IDRicerca=EBC_Ricerche.ID and EBC_Ricerche.IDStatoRic=EBC_StatiRic.ID');
               Q.SQl.Add('and EBC_CandidatiRicerche.IDAnagrafica=:xIDAnag: and EBC_Ricerche.IDCliente=:xIDCliente:');
               Q.Prepare;
               Q.ParamByName['xIDAnag']:=ElencoDipForm.TAnagDip.FieldByName('ID').AsInteger;
               Q.ParamByName['xIDCliente']:=DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger;
               Q.Open;
               if not Q.IsEmpty then begin
                    xRic:='';
                    while not Q.EOF do begin xRic:=xRic+'N�'+Q.FieldByName('Progressivo').asString+' ('+Q.FieldByName('StatoRic').asString+') '; Q.Next; end;
                    if MessageDlg('Il soggetto � associato gi� alle seguenti ricerche per lo stesso cliente: '+chr(13)+xRic+chr(13)+'PROCEDERE COMUNQUE ?',mtWarning, [mbYes,mbNo],0)=mrNo then xVai:=False;
               end;
               // controllo incompatibilit�
               if IncompAnagCli(ElencoDipForm.TAnagDip.FieldByName('ID').AsInteger,DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger) then
                    if MessageDlg('Il soggetto risulta INCOMPATIBILE con questo cliente o con un cliente associato'+chr(13)+
                         'PROCEDERE COMUNQUE ?',mtWarning, [mbYes,mbNo],0)=mrNo then xVai:=False;

               // controllo blocco livello 1 o 2
               xIDClienteBlocco:=CheckAnagInseritoBlocco1(ElencoDipForm.TAnagDip.FieldByName('ID').AsInteger);
               if xIDClienteBlocco>0 then begin
                    xLivProtez:=copy(IntToStr(xIDClienteBlocco),1,1);
                    if xLivProtez='1' then begin
                         xIDClienteBlocco:=xIDClienteBlocco-10000;
                         xClienteBlocco:=GetDescCliente(xIDClienteBlocco);
                         xDicMess:='Stai inserendo in commessa un candidato appartenente ad una azienda ('+xClienteBlocco+') con blocco di livello 1, ';
                    end else begin
                         xIDClienteBlocco:=xIDClienteBlocco-20000;
                         xClienteBlocco:=GetDescCliente(xIDClienteBlocco);
                         xDicMess:='Stai inserendo in commessa un candidato appartenente ad una azienda ATTIVA ('+xClienteBlocco+'), ';
                    end;
                    if MessageDlg('ATTENZIONE!! PROTEZIONE CLIENTI LIVELLO '+xLivProtez+chr(13)+xDicMess+
                         'protetta dalle normative interne, se sei autorizzato premi SI, altrimenti premi NO. '+
                         'Se si prosegue, l''operazione verr� registrata nello storico del soggetto come forzatura. '+
                         'Verr� inviato un messaggio di promemoria al responsabile diretto',mtWarning, [mbYes,mbNo],0)=mrNo then xVai:=False
                    else begin
                         // pwd
                         xPassword:='';
                         xUtenteResp:=GetDescUtenteResp(MainForm.xIDUtenteAttuale);
                         if not InputQuery('Forzatura blocco livello '+xLivProtez,'Password di '+xUtenteResp,xPassword) then exit;
                         if not CheckPassword(xPassword,GetPwdUtenteResp(MainForm.xIDUtenteAttuale)) then begin
                              MessageDlg('Password errata',mtError, [mbOK],0);
                              xVai:=False;
                         end;
                         if xVai then begin
                              // promemoria al resp.
                              QLocal:=CreateQueryFromString('insert into Promemoria (IDUtente,IDUtenteDa,DataIns,Testo,Evaso,DataDaLeggere) '+
                                   'values (:xIDUtente:,:xIDUtenteDa:,:xDataIns:,:xTesto:,:xEvaso:,:xDataDaLeggere:)');
                              QLocal.ParamByName['xIDUtente']:=GetIDUtenteResp(MainForm.xIDUtenteAttuale);
                              QLocal.ParamByName['xIDUtenteDa']:=MainForm.xIDUtenteAttuale;
                              QLocal.ParamByName['xDataIns']:=Date;
                              QLocal.ParamByName['xTesto']:='forzatura livello '+xLivProtez+' per CV n� '+ElencoDipForm.TAnagDipCVNumero.AsString;
                              QLocal.ParamByName['xEvaso']:=False;
                              QLocal.ParamByName['xDataDaLeggere']:=Date;
                              QLocal.ExecSQL;
                              // registra nello storico soggetto
                              xIDEvento:=GetIDEvento('forzatura Blocco livello '+xLivProtez);
                              if xIDEvento>0 then begin
                                   with Data.Q1 do begin
                                        close;
                                        SQL.text:='insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) '+
                                             'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xIDCliente:)';
                                        ParamByName['xIDAnagrafica']:=ElencoDipForm.TAnagDip.FieldByName('ID').AsInteger;
                                        ParamByName['xIDEvento']:=xIDevento;
                                        ParamByName['xDataEvento']:=Date;
                                        ParamByName['xAnnotazioni']:='cliente: '+xClienteBlocco;
                                        ParamByName['xIDRicerca']:=DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
                                        ParamByName['xIDUtente']:=MainForm.xIDUtenteAttuale;
                                        ParamByName['xIDCliente']:=xIDClienteBlocco;
                                        ExecSQL;
                                   end;
                              end;
                         end;
                    end;
               end;

               if xVai then begin
                    Data.DB.BeginTrans;
                    try
                         if Q.Active then Q.Close;
                         Q.SQL.text:='insert into EBC_CandidatiRicerche (IDRicerca,IDAnagrafica,Escluso,Stato,DataIns) '+
                              'values (:xIDRicerca:,:xIDAnagrafica:,:xEscluso:,:xStato:,:xDataIns:)';
                         Q.ParamByName['xIDRicerca']:=DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
                         Q.ParamByName['xIDAnagrafica']:=ElencoDipForm.TAnagDip.FieldByName('ID').AsInteger;
                         Q.ParamByName['xEscluso']:=False;
                         Q.ParamByName['xStato']:='inserito direttamente da elenco';
                         Q.ParamByName['xDataIns']:=Date;
                         Q.ExecSQL;
                         // aggiornamento stato anagrafica
                         if Q.Active then Q.Close;
                         Q.SQL.text:='update Anagrafica set IDStato=27,IDTipoStato=1 where ID='+ElencoDipForm.TAnagDipID.asString;
                         Q.ExecSQL;
                         // aggiornamento storico
                         if Q.Active then Q.Close;
                         Q.SQL.text:='insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) '+
                              'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xIDCliente:)';
                         Q.ParamByName['xIDAnagrafica']:=ElencoDipForm.TAnagDip.FieldByName('ID').AsInteger;
                         Q.ParamByName['xIDEvento']:=19;
                         Q.ParamByName['xDataEvento']:=date;
                         Q.ParamByName['xAnnotazioni']:='ric.n�'+DataRicerche.TRicerchePend.FieldByName('Progressivo').AsString+' ('+DataRicerche.TRicerchePend.FieldByName('Cliente').AsString+')';
                         Q.ParamByName['xIDRicerca']  :=DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
                         Q.ParamByName['xIDUtente']   :=DataRicerche.TRicerchePend.FieldByName('IDUtente').AsInteger;
                         Q.ParamByName['xIDCliente']  :=DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger;
                         Q.ExecSQL;
                         Data.DB.CommitTrans;
                         DataRicerche.QCandRic.Close;
                         DataRicerche.QCandRic.Open;
                         LTotCand.Caption:=IntToStr(DataRicerche.QCandRic.RecordCount);
                    except
                         Data.DB.RollbackTrans;
                         MessageDlg('Errore sul database:  inserimento non avvenuto',mtError, [mbOK],0);
                    end;
               end;
          end;
          DataRicerche.TRicAnagIDX.Close;
     end;
     ElencoDipForm.Free;
     RiprendiAttivita;
end;

procedure TSelPersForm.SpeedButton1Click(Sender: TObject);
begin
     InsRuoloForm:=TInsRuoloForm.create(self);
     InsRuoloForm.ShowModal;
     if InsRuoloForm.ModalResult=mrOK then begin
          if not(DataRicerche.DsRicerchePend.State in [dsInsert,dsEdit]) then
               DataRicerche.TRicerchePend.Edit;
          DataRicerche.TRicerchePend.FieldByName('IDMansione').AsInteger:=InsRuoloForm.TRuoli.FieldByName('ID').AsInteger;
          DataRicerche.TRicerchePend.FieldByName('IDArea').AsInteger:=InsRuoloForm.TAree.FieldByName('ID').AsInteger;
          with DataRicerche.TRicerchePend do begin
               Data.DB.BeginTrans;
               try
                    ApplyUpdates;
                    Data.DB.CommitTrans;
                    CaricaApriRicPend(DataRicerche.QRicAttive.FieldByName('ID').AsInteger);
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
                    raise;
               end;
               CommitUpdates;
          end;
          InsRuoloForm.Free;
     end else begin
          InsRuoloForm.Free;
          Abort;
     end;
end;

procedure TSelPersForm.SpeedButton2Click(Sender: TObject);
begin
     if MessageDlg('ATTENZIONE: stai cambiando il cliente !! - Continuare ?',mtWarning, [mbYes,mbNo],0)=mrYes then begin
          SelClienteForm:=TSelClienteForm.create(self);
          SelClienteForm.ShowModal;
          if SelClienteForm.ModalResult=mrOK then begin
               DataRicerche.TRicerchePend.Edit;
               DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger:=SelClienteForm.TClienti.FieldByName('ID').AsInteger;
               with DataRicerche.TRicerchePend do begin
                    Data.DB.BeginTrans;
                    try
                         ApplyUpdates;
                         Data.DB.CommitTrans;
                         CaricaApriRicPend(DataRicerche.QRicAttive.FieldByName('ID').AsInteger);
                    except
                         Data.DB.RollbackTrans;
                         MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
                         raise;
                    end;
                    CommitUpdates;
               end;
          end;
          SelClienteForm.Free;
     end;
end;

procedure TSelPersForm.ToolbarButton974Click(Sender: TObject);
var i: integer;
     SavePlace: TBookmark;
begin
     if not Mainform.CheckProfile('213') then Exit;
     if DataRicerche.QCandRic.RecordCount>0 then begin
          FaxPresentazForm:=TFaxPresentazForm.create(self);
          FaxPresentazForm.ASG1.RowCount:=DataRicerche.QCandRic.RecordCount+1;
          DataRicerche.QCandRic.First;
          i:=1;
          while not DataRicerche.QCandRic.EOF do begin
               FaxPresentazForm.ASG1.addcheckbox(0,i,false,false);
               FaxPresentazForm.ASG1.Cells[0,i]:=DataRicerche.QCandRic.FieldByName('Cognome').AsString+' '+DataRicerche.QCandRic.FieldByName('Nome').AsString;
               FaxPresentazForm.xArrayIDAnag[i]:=DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger;
               DataRicerche.QCandRic.Next;
               inc(i);
          end;
          FaxPresentazForm.QClienti.ParamByName['xIDCliente']:=DataRicerche.TRicerchePendFieldByName('IDCliente').AsInteger;
          FaxPresentazForm.QClienti.open;
          FaxPresentazForm.TContattiCli.Open;
          FaxPresentazForm.ShowModal;
          SavePlace:=DataRicerche.QCandRic.GetBookmark;
          DataRicerche.QCandRic.Close;
          DataRicerche.QCandRic.Open;
          DataRicerche.QCandRic.GotoBookmark(SavePlace);
          DataRicerche.QCandRic.FreeBookmark(SavePlace);
          FaxPresentazForm.Free;
          RiprendiAttivita;
     end;
end;

procedure TSelPersForm.ToolbarButton9712Click(Sender: TObject);
var i: integer;
     Vero,xInserito: boolean;
     xID: integer;
begin
     if not Mainform.CheckProfile('050') then Exit;
     if DataRicerche.QCandRic.RecordCount>0 then begin
          CambiaStato2Form:=TCambiaStato2Form.create(self);
          CambiaStato2Form.ECogn.Text:=DataRicerche.QCandRic.FieldByName('Cognome').AsString;
          CambiaStato2Form.ENome.Text:=DataRicerche.QCandRic.FieldByName('Nome').AsString;
          CambiaStato2Form.PanStato.Visible:=False;
          CambiaStato2Form.xIDDaStato:=DataRicerche.QCandRic.FieldByName('IDStato').AsInteger;
          CambiaStato2Form.DEData.Date:=Date;
          CambiaStato2Form.ShowModal;
          if CambiaStato2Form.ModalResult=mrOK then begin
               xInserito:=InsEvento(DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger,
                    CambiaStato2Form.QNextStati.FieldByName('IDProcedura').AsInteger,
                    CambiaStato2Form.QNextStati.FieldByName('ID').AsInteger,
                    CambiaStato2Form.QNextStati.FieldByName('IDaStato').AsInteger,
                    CambiaStato2Form.QNextStati.FieldByName('IDTipoStatoA').AsInteger,
                    CambiaStato2Form.DEData.Date,
                    DataRicerche.QCandRic.FieldByName('Cognome').AsString,
                    DataRicerche.QCandRic.FieldByName('Nome').AsString,
                    CambiaStato2Form.Edit1.Text,
                    DataRicerche.TRicerchePend.FieldByName('ID').AsInteger,
                    MainForm.xIDUtenteAttuale,
                    DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger);
               xID:=DataRicerche.QCandRic.FieldByName('ID').AsInteger;
               DataRicerche.QCandRic.Close;
               DataRicerche.QCandRic.Open;
               DataRicerche.QCandRic.locate('ID',xID, []);
               LTotCand.Caption:=IntToStr(DataRicerche.QCandRic.RecordCount);
          end;
          CambiaStato2Form.Free;
          RiprendiAttivita;
     end;
end;

procedure TSelPersForm.EseguiQueryCandidati;
begin
     // NOTA: il DISTINCT NON FUNZIONA:  d� l'errore:
     // "Impossibile selezionare il tipo di dati text, ntext o image come DISTINCT."
     DataRicerche.QCandRic.Close;
     DataRicerche.QCandRic.SQL.Clear;
     DataRicerche.QCandRic.SQL.Text:='select distinct EBC_CandidatiRicerche.ID,EBC_CandidatiRicerche.IDRicerca, '+
          'Anagrafica.Cognome,Anagrafica.Nome,Anagrafica.CVNumero,Anagrafica.IDStato,Cellulare,TelUfficio,DataNascita,RecapitiTelefonici, '+
          'EBC_CandidatiRicerche.IDAnagrafica,EBC_CandidatiRicerche.Escluso, '+
          'EBC_CandidatiRicerche.DataImpegno,EBC_CandidatiRicerche.Codstato, '+
          'EBC_CandidatiRicerche.Stato,EBC_CandidatiRicerche.Note2 '+
          'Note, EBC_TipiStato.TipoStato,EBC_CandidatiRicerche.DataIns, '+
          'EBC_CandidatiRicerche.MiniVal, EBC_ContattiCandidati.Data DataUltimoContatto, '+
          'min(TitoliStudio.ID) IDTitoloStudioMin '+
          'from EBC_CandidatiRicerche,Anagrafica,EBC_TipiStato,EBC_ContattiCandidati, TitoliStudio '+
          'where EBC_CandidatiRicerche.IDAnagrafica=Anagrafica.ID '+
          'and Anagrafica.IDTipoStato=EBC_TipiStato.ID '+
          'and EBC_CandidatiRicerche.IDRicerca=:ID '+
          'and EBC_CandidatiRicerche.IDAnagrafica *= EBC_ContattiCandidati.IDAnagrafica '+
          'and EBC_ContattiCandidati.Data= '+
          '  (select max(Data) from EBC_ContattiCandidati '+
          '   where EBC_CandidatiRicerche.IDAnagrafica *= EBC_ContattiCandidati.IDAnagrafica '+
          '   and IDRicerca=:ID) '+
          'and EBC_CandidatiRicerche.IDAnagrafica *= TitoliStudio.IDAnagrafica ';
     if CBOpzioneCand.Checked then
          DataRicerche.QCandRic.SQL.Add('and (Escluso=0 or Escluso is null)');

     DataRicerche.QCandRic.SQL.Add('group by '+
          'EBC_CandidatiRicerche.ID,EBC_CandidatiRicerche.IDRicerca, '+
          'Anagrafica.Cognome,Anagrafica.Nome,Anagrafica.CVNumero,Anagrafica.IDStato,Cellulare,TelUfficio,DataNascita,RecapitiTelefonici, '+
          'EBC_CandidatiRicerche.IDAnagrafica,EBC_CandidatiRicerche.Escluso, '+
          'EBC_CandidatiRicerche.DataImpegno,EBC_CandidatiRicerche.Codstato, '+
          'EBC_CandidatiRicerche.Stato,EBC_CandidatiRicerche.Note2, '+
          'EBC_TipiStato.TipoStato,EBC_CandidatiRicerche.DataIns, '+
          'EBC_CandidatiRicerche.MiniVal, EBC_ContattiCandidati.Data '+
          'order by Anagrafica.Cognome,Anagrafica.Nome');

     { --- VECCHIO CODICE senza EspLAv e TitoliStudio
     DataRicerche.QCandRic.SQL.Text:='select distinct EBC_CandidatiRicerche.ID,EBC_CandidatiRicerche.IDRicerca, '+
          'Anagrafica.Cognome,Anagrafica.Nome,Anagrafica.CVNumero,Anagrafica.IDStato,Cellulare,TelUfficio,DataNascita,RecapitiTelefonici, '+
          'EBC_CandidatiRicerche.IDAnagrafica,EBC_CandidatiRicerche.Escluso, '+
          'EBC_CandidatiRicerche.DataImpegno,EBC_CandidatiRicerche.Codstato, '+
          'EBC_CandidatiRicerche.Stato,EBC_CandidatiRicerche.Note2 Note, '+
          'EBC_TipiStato.TipoStato,EBC_CandidatiRicerche.DataIns, '+
          'EBC_CandidatiRicerche.MiniVal, '+
          'EBC_ContattiCandidati.Data DataUltimoContatto '+
          'from EBC_CandidatiRicerche,Anagrafica,EBC_TipiStato,EBC_ContattiCandidati '+
          'where EBC_CandidatiRicerche.IDAnagrafica=Anagrafica.ID '+
          'and Anagrafica.IDTipoStato=EBC_TipiStato.ID and EBC_CandidatiRicerche.IDRicerca=:ID '+
          'and EBC_CandidatiRicerche.IDAnagrafica *= EBC_ContattiCandidati.IDAnagrafica '+
          'and EBC_ContattiCandidati.Data= '+
          '    (select max(Data) from EBC_ContattiCandidati '+
          '     where EBC_CandidatiRicerche.IDAnagrafica *= EBC_ContattiCandidati.IDAnagrafica '+
          '     and IDRicerca='+DataRicerche.TRicerchePendID.asString+')';
     if CBOpzioneCand.Checked then
          DataRicerche.QCandRic.SQL.Add('and (Escluso=0 or Escluso is null)');
     DataRicerche.QCandRic.SQL.Add('order by Anagrafica.Cognome,Anagrafica.Nome');}

     DataRicerche.QCandRic.Prepare;
     ScriviRegistry('QCandRic',DataRicerche.QCandRic.SQl.text);
     DataRicerche.QCandRic.Open;
     LTotCand.Caption:=IntToStr(DataRicerche.QCandRic.RecordCount);
end;

procedure TSelPersForm.CBOpzioneCandClick(Sender: TObject);
begin
     BRecupera.Enabled:=not(CBOpzioneCand.Checked);
     EseguiQueryCandidati;
end;

procedure TSelPersForm.ToolbarButton9713Click(Sender: TObject);
var i: integer;
begin
     RiprendiAttivita;
     if not Mainform.CheckProfile('5') then Exit;
     xCerca:=True;
     SelPersNewForm:=TSelPersNewForm.create(self);
     // riempimento combobox1 con le tabelle
     SelPersNewForm.QTabelle.Close;
     SelPersNewForm.QTabelle.Open;
     SelPersNewForm.QTabelle.First;
     SelPersNewForm.ComboBox1.Items.Clear;
     while not SelPersNewForm.QTabelle.EOF do begin
          SelPersNewForm.ComboBox1.Items.Add(SelPersNewForm.QTabelleDescTabella.asString);
          SelPersNewForm.QTabelle.Next;
     end;
     SelPersNewForm.QRes1.Close;
     // oggetti visibili ed invisibili
     SelPersNewForm.BAggiungiRic.Visible:=True;
     // togliere nominativi gi� presenti in archivio
     DataRicerche.QCandRic.First;
     for i:=1 to DataRicerche.QCandRic.RecordCount do begin
          SelPersNewForm.xDaEscludere[i]:=DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger;
          DataRicerche.QCandRic.Next;
     end;
     DataRicerche.QCandRic.First;
     SelPersNewForm.xChiamante:=1;
     SelPersNewForm.ShowModal;
     SelPersNewForm.Free;
     RiprendiAttivita;
     xCerca:=False;
end;

procedure TSelPersForm.BInfoCandClick(Sender: TObject);
begin
     if not Mainform.CheckProfile('00') then Exit;
     if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString,1,5))='ERGON' then begin
          SchedaSinteticaForm:=TSchedaSinteticaForm.create(self);
          SchedaSinteticaForm.xIDAnag:=DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger;
          SchedaSinteticaForm.ShowModal;
          SchedaSinteticaForm.Free;
     end else begin
          SchedaCandForm:=TSchedaCandForm.create(self);
          if not PosizionaAnag(DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger) then exit;
          SchedaCandForm.ShowModal;
          SchedaCandForm.Free;
          RiprendiAttivita;
     end;
end;

procedure TSelPersForm.ToolbarButton972Click(Sender: TObject);
var xFile: string;
     xProcedi: boolean;
begin
     ApriCV(DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger);
end;

procedure TSelPersForm.ToolbarButton973Click(Sender: TObject);
begin
     if not Mainform.CheckProfile('054') then Exit;
     ApriFileWord(DataRicerche.QCandRicCVNumero.AsString);
     RiprendiAttivita;
end;

procedure TSelPersForm.BRecuperaClick(Sender: TObject);
var xPerche: string;
begin
     if not Mainform.CheckProfile('212') then Exit;
     if (not DataRicerche.QCandRic.Eof)and(DataRicerche.QCandRic.FieldByName('Escluso').AsBoolean) then begin
          if InputQuery('reinserimento nella ricerca','Motivo:',xPerche) then begin
               Data.DB.BeginTrans;
               try
                    DataRicerche.QGen.SQL.Clear;
                    DataRicerche.QGen.SQL.Add('update EBC_CandidatiRicerche set Escluso=0,');
                    DataRicerche.QGen.SQL.Add('Stato="reinserito (motivo: '+xPerche+')" where IDAnagrafica='+DataRicerche.QCandRicIDAnagrafica.asString+' and IDRicerca='+DataRicerche.QCandRicIDRicerca.asString);
                    DataRicerche.QGen.execSQL;
                    // aggiornamento stato anagrafica
                    if Data.TAnagABS.FieldByName('IDStato').AsInteger =28 then begin
                         if Q.Active then Q.Close;
                         Q.SQL.text:='update Anagrafica set IDStato=27,IDTipoStato=1 where ID='+DataRicerche.QCandRicIDAnagrafica.asString;
                         Q.ExecSQL;
                    end;
                    // aggiornamento storico
                    if Q.Active then Q.Close;
                    Q.SQL.text:='insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) '+
                         'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xIDCliente:)';
                    Q.ParamByName['xIDAnagrafica']:=DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger;
                    Q.ParamByName['xIDEvento']:=78;
                    Q.ParamByName['xDataEvento']:=date;
                    Q.ParamByName['xAnnotazioni']:='ric.'+DataRicerche.TRicerchePendProgressivo.asString+' per '+DataRicerche.TRicerchePendCliente.asString+' (motivo: '+xPerche+')';
                    Q.ParamByName['xIDRicerca'] :=DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
                    Q.ParamByName['xIDUtente']  :=DataRicerche.TRicerchePend.FieldByName('IDUtente').AsInteger;
                    Q.ParamByName['xIDCliente'] :=DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger;
                    Q.ExecSQL;
                    // aggiornamento stato anagrafica --> in selezione
                    if Q.Active then Q.Close;
                    Q.SQL.text:='update Anagrafica set IDStato=27,IDTipoStato=1 where ID='+DataRicerche.QCandRicIDAnagrafica.asString;
                    Q.ExecSQL;

                    Data.DB.CommitTrans;
                    DataRicerche.QCandRic.Close;
                    DataRicerche.QCandRic.Open;
                    LTotCand.Caption:=IntToStr(DataRicerche.QCandRic.RecordCount);
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('Errore sul database:  inserimento non avvenuto',mtError, [mbOK],0);
               end;
          end;
          RiprendiAttivita;
     end;
end;

procedure TSelPersForm.FormShow(Sender: TObject);
begin
     // CONTROLLO DI GESTIONE PER ORA NON VISIBILE
     //PCSelPers.Pages[4].TabVisible:=False;
     PCSelPers.Pages[5].TabVisible:=False;
     // TARGET LIST -- ABILITATA SOLO SE IN GLOBAL.CHECKBOXINDICI il 4� carattere � = 1
     if copy(Data.Global.FieldByName('CheckBoxIndici').AsString,4,1)='1' then
          xDisabilitaTargetList:=False
     else xDisabilitaTargetList:=True;

     if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString,1,6))='PROPOS' then
          SelPersForm.WindowState:=wsMaximized;

     Caption:='[M/21] - '+caption;
     if not DataRicerche.QCandRic.Active then begin
          DataRicerche.QCandRic.SQL.Clear;
          DataRicerche.QCandRic.SQL.Text:='select distinct EBC_CandidatiRicerche.ID,EBC_CandidatiRicerche.IDRicerca, '+
               'Anagrafica.Cognome,Anagrafica.Nome,Anagrafica.CVNumero,Anagrafica.IDStato,Cellulare,TelUfficio,DataNascita, '+
               'EBC_CandidatiRicerche.IDAnagrafica,EBC_CandidatiRicerche.Escluso, '+
               'EBC_CandidatiRicerche.DataImpegno,EBC_CandidatiRicerche.Codstato, '+
               'EBC_CandidatiRicerche.Stato, '+
               'EBC_TipiStato.TipoStato,EBC_CandidatiRicerche.DataIns, '+
               'EBC_CandidatiRicerche.MiniVal, '+
               'EBC_ContattiCandidati.Data DataUltimoContatto '+
               'from EBC_CandidatiRicerche,Anagrafica,EBC_TipiStato,EBC_ContattiCandidati '+
               'where EBC_CandidatiRicerche.IDAnagrafica=Anagrafica.ID '+
               'and Anagrafica.IDTipoStato=EBC_TipiStato.ID and EBC_CandidatiRicerche.IDRicerca=:ID '+
               'and EBC_CandidatiRicerche.IDAnagrafica *= EBC_ContattiCandidati.IDAnagrafica '+
               'and EBC_ContattiCandidati.Data= '+
               '    (select max(Data) from EBC_ContattiCandidati '+
               '     where EBC_CandidatiRicerche.IDAnagrafica *= EBC_ContattiCandidati.IDAnagrafica '+
               '     and IDRicerca='+DataRicerche.TRicerchePendID.asString+') '+
               'order by Anagrafica.CVNumero';
          DataRicerche.QCandRic.Open;
     end;
     xAttivo:=True;
     xCerca:=False;
     LTotCand.Caption:=IntToStr(DataRicerche.QCandRic.RecordCount);
     LTiming.Caption:='TIMING (riferito all''utente '+MainForm.xUtenteAttuale+'):';

     // parametri evidenziazione date
     Q.SQl.text:='select ggDiffDataIns,ggDiffDataUltimoContatto,CheckRicCandCV,CheckRicCandTel from global ';
     Q.Open;
     xggDiffDataIns:=Q.FieldByname('ggDiffDataIns').asInteger;
     xggDiffDataUltimoContatto:=Q.FieldByname('ggDiffDataUltimoContatto').asInteger;
     xCheckRicCandCV:=Q.FieldByname('CheckRicCandCV').asBoolean;
     xCheckRicCandTel:=Q.FieldByname('CheckRicCandTel').asBoolean;
     Q.Close;
     // personalizzazioni ERGON
     if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString,1,5))='ERGON' then begin
          BInfocand.Caption:='scheda cand.';
          BCurriculum.Visible:=False;
     end;
     // personalizzazioni EUREN
     if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString,1,5))='EUREN' then begin
          PanTiming.visible:=False;
     end;
     if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString,1,6))='DUCATI' then begin
          PanCausali.visible:=True;
     end;

     RiprendiAttivita;
     if not xFromOrgTL then
          PCSelPers.ActivePage:=TSCandidati;

     FrameDBRich1.Editor.DataSource:=DataRicerche.dsRicerchePend;
end;

procedure TSelPersForm.OraTimer(Sender: TObject);
var x,y: integer;
begin
     y:=StrToInt(SecondiTotali.Text);
     SecondiTotali.Text:=IntToStr(y+1);
     if (xAttivo)or(xCerca) then begin
          x:=StrToInt(SecondiAttivita.Text);
          SecondiAttivita.Text:=IntToStr(x+1);
     end;
end;

procedure TSelPersForm.FormMouseMove(Sender: TObject; Shift: TShiftState;
     X,Y: Integer);
begin
     RiprendiAttivita;
end;

procedure TSelPersForm.TimerInattivoTimer(Sender: TObject);
begin
     TimerInattivo.Enabled:=False;
     xAttivo:=False;
end;

procedure TSelPersForm.FormCreate(Sender: TObject);
begin
     Data2.Qtemp.SQL.text:='select TotSec from TimingRic '+
          ' where IDUtente='+IntToStr(MainForm.xIDUtenteAttuale)+
          ' and IDRicerca='+DataRicerche.TRicerchePendID.asString+
          ' and Data=:xData:';
     Data2.Qtemp.ParamByName['xData']:=Date;
     Data2.Qtemp.Open;
     if Data2.Qtemp.RecordCount=0 then begin
          xMinStart:=0;
          Data2.Qtemp.Close;
          Data2.Qtemp.SQL.text:='insert into TimingRic (IDUtente,IDRicerca,Data,TotSec)'+
               ' values (:xIDUtente:,:xIDRicerca:,:xData:,:xTotSec:)';
          Data2.Qtemp.ParamByName['xIDUtente']:=MainForm.xIDUtenteAttuale;
          Data2.Qtemp.ParamByName['xIDRicerca']:=DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
          Data2.Qtemp.ParamByName['xData']:=Date;
          Data2.Qtemp.ParamByName['xTotSec']:=0;
          Data2.Qtemp.ExecSQL;
     end else begin
          xMinStart:=Data2.Qtemp.FieldByName('TotSec').asInteger;
          Data2.Qtemp.Close;
     end;
end;

procedure TSelPersForm.FormClose(Sender: TObject;
     var Action: TCloseAction);
begin
     Data2.Qtemp.SQL.text:='update TimingRic set TotSec='+IntToStr(xMinStart+StrToInt(SecondiAttivita.Text))+
          ' where IDUtente='+IntToStr(MainForm.xIDUtenteAttuale)+
          ' and IDRicerca='+DataRicerche.TRicerchePendID.asString+
          ' and Data=:xData:';
     Data2.Qtemp.ParamByName['xData']:=Date;
     Data2.Qtemp.ExecSQL;
     //if DataRicerche.DsRicerchePend.state=dsEdit then begin
     //     DataRicerche.TRicerchePend.Post;
     //     DataRicerche.TRicerchePend.ApplyUpdates;
     //     DataRicerche.TRicerchePend.CommitUpdates;
     //end;
     DataRicerche.QCandRic.Close;
end;

procedure TSelPersForm.RiprendiAttivita;
begin
     // --> azzerare il contatore e ripartire
     TimerInattivo.Enabled:=False;
     TimerInattivo.Interval:=0;
     TimerInattivo.Enabled:=True;
     TimerInattivo.Enabled:=False;
     TimerInattivo.Interval:=120000;
     TimerInattivo.Enabled:=True;
     xAttivo:=True;
end;

procedure TSelPersForm.BStatoRicClick(Sender: TObject);
var SavePlace: TBookmark;
     xIDRic,xIDCandInserito: integer;
     i,k: integer;
     xNonInviati: string;
begin
     if not Mainform.CheckProfile('23') then Exit;
     xIDRic:=DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
     ModStatoRicForm:=TModStatoRicForm.create(self);
     ModStatoRicForm.DEDallaData.Date:=Date;
     if DataRicerche.TRicerchePendDataFine.AsString<>'' then
          ModStatoRicForm.DEDataFine.Date:=DataRicerche.TRicerchePend.FieldByName('DataFine').AsDateTime;
     ModStatoRicForm.ShowModal;
     if ModStatoRicForm.ModalResult=mrOK then begin
          Data.DB.BeginTrans;
          try
               Data.Q1.Close;
               Data.Q1.SQL.text:='update EBC_Ricerche set IDStatoRic=:xIDStatoRic:,DallaData=:xDallaData:';
               if ModStatoRicForm.DEDataFine.text<>'' then
                    Data.Q1.SQL.Add(',DataFine=:xDataFine:');
               Data.Q1.SQL.Add('where ID='+DataRicerche.TRicerchePendID.asString);
               Data.Q1.ParamByName['xIDStatoRic']:=ModStatoRicForm.TStatiRic.FieldByName('ID').AsInteger;
               Data.Q1.ParamByName['xDallaData']:=ModStatoRicForm.DEDallaData.Date;
               if ModStatoRicForm.DEDataFine.text<>'' then
                    Data.Q1.ParamByName['xDataFine']:=ModStatoRicForm.DEDataFine.Date;
               Data.Q1.ExecSQL;
               // nuova riga nello storico
               Data.Q1.Close;
               Data.Q1.SQL.text:='Insert into EBC_StoricoRic (IDRicerca,IDStatoRic,DallaData,Note) '+
                    'values (:xIDRicerca:,:xIDStatoRic:,:xDallaData:,:xNote:)';
               Data.Q1.ParamByName['xIDRicerca']:=DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
               Data.Q1.ParamByName['xIDStatoRic']:=ModStatoRicForm.TStatiRic.FieldByName('ID').AsInteger;
               Data.Q1.ParamByName['xDallaData']:=ModStatoRicForm.DEDallaData.Date;
               Data.Q1.ParamByName['xNote']:=ModStatoRicForm.ENote.Text;
               Data.Q1.ExecSQL;

               Data.DB.CommitTrans;

               CaricaApriRicPend(xIDRic);
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE:  operazione non completata',mtError, [mbOK],0);
          end;
     end;

     // sistema di "sgancio" se la ricerca � chiusa
     if ModStatoRicForm.TStatiRicStatoRic.Value='conclusa' then begin
          if MessageDlg('Vuoi procedere con il sistema automatico di sgancio ?',mtInformation, [mbYes,mbNo],0)=mrYes then begin
               Data.Q1.Close;
               Data.Q1.SQL.text:='select Anagrafica.ID, Anagrafica.IDTipoStato, Anagrafica.Email, EBC_CandidatiRicerche.* '+
                    'from EBC_CandidatiRicerche, Anagrafica '+
                    'where EBC_CandidatiRicerche.IDAnagrafica=Anagrafica.ID '+
                    'and IDRicerca='+DataRicerche.TRicerchePendID.AsString;
               Data.Q1.Open;
               if not Data.Q1.Locate('IDTipoStato',3, []) then begin
                    if MessageDlg('ATTENZIONE: non risultano candidati inseriti ! - Vuoi procedere ?',mtWarning, [mbYes,mbNo],0)=mrNo then begin
                         Data.Q1.Close;
                         Exit;
                    end;
               end else xIDCandInserito:=Data.Q1.FieldByName('ID').asInteger;

               // aggiornamento candidati
               Data.Q1.First;
               while not Data.Q1.EOF do begin
                    if (Data.Q1.FieldByName('IDTipoStato').AsInteger<>3)and(Data.Q1.FieldByName('Escluso').asBoolean=False) then begin
                         Data.QTemp.Close;
                         Data.QTemp.SQL.Clear;
                         Data.QTemp.SQL.Add('update EBC_CandidatiRicerche set Escluso=1,');
                         Data.QTemp.SQL.Add('Stato="eliminato (motivo: commessa conclusa)" where IDAnagrafica='+Data.Q1.FieldByName('IDAnagrafica').asString+' and IDRicerca='+DataRicerche.TRicerchePendID.asString);
                         Data.QTemp.execSQL;
                         // candidati in selezione messi come esterni
                         Data.QTemp.Close;
                         Data.QTemp.SQL.text:='update anagrafica set IDTipoStato=2,IDStato=28 '+
                              'where ID='+Data.Q1.FieldByName('IDAnagrafica').asString;
                         Data.QTemp.ExecSQL;
                         // aggiornamento storico
                         Data.QTemp.Close;
                         Data.QTemp.SQL.text:='insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) '+
                              'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xIDCliente:)';
                         Data.QTemp.ParamByName['xIDAnagrafica']:=Data.Q1.FieldByName('IDAnagrafica').asInteger;
                         Data.QTemp.ParamByName['xIDEvento']:=17;
                         Data.QTemp.ParamByName['xDataEvento']:=Date;
                         Data.QTemp.ParamByName['xAnnotazioni']:='ric.'+DataRicerche.TRicerchePendProgressivo.asString+' per '+DataRicerche.TRicerchePendCliente.asString+' (motivo: commessa conclusa)';
                         Data.QTemp.ParamByName['xIDRicerca']:=DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
                         Data.QTemp.ParamByName['xIDUtente']:=MainForm.xIDUtenteAttuale;
                         Data.QTemp.ParamByName['xIDCliente']:=DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger;
                         Data.QTemp.ExecSQL;
                    end;
                    Data.Q1.Next;
               end;

               // ### attenzione ###: si assume sempre un solo inserito !!
               FaxPresentazForm:=TFaxPresentazForm.create(self);
               FaxPresentazForm.ASG1.RowCount:=DataRicerche.QCandRic.RecordCount;
               CBOpzioneCand.Checked:=False;
               EseguiQueryCandidati;
               DataRicerche.QCandRic.First;
               i:=1;
               while not DataRicerche.QCandRic.EOF do begin
                    if DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger<>xIDCandInserito then begin
                         FaxPresentazForm.ASG1.addcheckbox(0,i,false,false);
                         FaxPresentazForm.ASG1.Cells[0,i]:=DataRicerche.QCandRic.FieldByName('Cognome').AsString+' '+DataRicerche.QCandRic.FieldByName('Nome').AsString;
                         FaxPresentazForm.xArrayIDAnag[i]:=DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger;
                         inc(i);
                    end;
                    DataRicerche.QCandRic.Next;
               end;
               FaxPresentazForm.QClienti.ParamByName['xIDCliente']:=DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger;
               FaxPresentazForm.QClienti.open;
               FaxPresentazForm.TContattiCli.Open;
               FaxPresentazForm.ShowModal;
               SavePlace:=DataRicerche.QCandRic.GetBookmark;
               DataRicerche.QCandRic.Close;
               DataRicerche.QCandRic.Open;
               DataRicerche.QCandRic.GotoBookmark(SavePlace);
               DataRicerche.QCandRic.FreeBookmark(SavePlace);
               FaxPresentazForm.Free;


               // procedi: per ogni soggetto non inserito in azienda, escludilo
               // VECCHIO CODICE per la procedura di scancio
               {SceltaModelloMailCliForm:=TSceltaModelloMailCliForm.create(self);
               SceltaModelloMailCliForm.CBStoricoInvio.Visible:=False;
               SceltaModelloMailCliForm.QTipiMailContattiCli.Close;
               SceltaModelloMailCliForm.QTipiMailContattiCli.SQL.text:='select * from TipiMailCand';
               SceltaModelloMailCliForm.QTipiMailContattiCli.Open;
               SceltaModelloMailCliForm.BModTesti.Visible:=True;
               SceltaModelloMailCliForm.ShowModal;
               Data.Q1.First;
               while not Data.Q1.EOF do begin
                    if (Data.Q1.FieldByName('IDTipoStato').AsInteger<>3)and(Data.Q1.FieldByName('Escluso').asBoolean=False) then begin
                         Data.QTemp.Close;
                         Data.QTemp.SQL.Clear;
                         Data.QTemp.SQL.Add('update EBC_CandidatiRicerche set Escluso=1,');
                         Data.QTemp.SQL.Add('Stato="eliminato (motivo: commessa conclusa)" where IDAnagrafica='+Data.Q1.FieldByName('IDAnagrafica').asString+' and IDRicerca='+DataRicerche.TRicerchePendID.asString);
                         Data.QTemp.execSQL;
                         // candidati in selezione messi come esterni
                         Data.QTemp.Close;
                         Data.QTemp.SQL.text:='update anagrafica set IDTipoStato=2,IDStato=28 '+
                              'where ID='+Data.Q1.FieldByName('IDAnagrafica').asString;
                         Data.QTemp.ExecSQL;

                         // aggiornamento storico
                         Data.QTemp.Close;
                         Data.QTemp.SQL.text:='insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) '+
                              'values (:xIDAnagrafica,:xIDEvento,:xDataEvento,:xAnnotazioni,:xIDRicerca,:xIDUtente,:xIDCliente)';
                         Data.QTemp.ParamByName['xIDAnagrafica').asInteger:=Data.Q1.FieldByName('IDAnagrafica').asInteger;
                         Data.QTemp.ParamByName['xIDEvento').asInteger:=17;
                         Data.QTemp.ParamByName['xDataEvento').asDateTime:=Date;
                         Data.QTemp.ParamByName['xAnnotazioni').asString:='ric.'+DataRicerche.TRicerchePendProgressivo.asString+' per '+DataRicerche.TRicerchePendCliente.asString+' (motivo: commessa conclusa)';
                         Data.QTemp.ParamByName['xIDRicerca').asInteger:=DataRicerche.TRicerchePendID.Value;
                         Data.QTemp.ParamByName['xIDUtente').asInteger:=MainForm.xIDUtenteAttuale;
                         Data.QTemp.ParamByName['xIDCliente').asInteger:=DataRicerche.TRicerchePendIDCliente.Value;
                         Data.QTemp.ExecSQL;
                         // invio mail (se ce l'hanno...)
                         if Data.Q1.FieldByName('Email').asString<>'' then begin
                              MainForm.SpedisciMail(Data.Q1.FieldByName('Email').asString);
                              // storicizza invio mail
                              Data.QTemp.Close;
                              Data.QTemp.SQL.text:='insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) '+
                                   'values (:xIDAnagrafica,:xIDEvento,:xDataEvento,:xAnnotazioni,:xIDRicerca,:xIDUtente,:xIDCliente)';
                              Data.QTemp.ParamByName['xIDAnagrafica').asInteger:=Data.Q1.FieldByName('IDAnagrafica').asInteger;
                              Data.QTemp.ParamByName['xIDEvento').asInteger:=71;
                              Data.QTemp.ParamByName['xDataEvento').asDateTime:=Date;
                              Data.QTemp.ParamByName['xAnnotazioni').asString:=SceltaModelloMailCliForm.QTipiMailContattiCliDescrizione.Value;
                              Data.QTemp.ParamByName['xIDRicerca').asInteger:=DataRicerche.TRicerchePendID.Value;
                              Data.QTemp.ParamByName['xIDUtente').asInteger:=MainForm.xIDUtenteAttuale;
                              Data.QTemp.ParamByName['xIDCliente').asInteger:=DataRicerche.TRicerchePendIDCliente.Value;
                              Data.QTemp.ExecSQL;
                         end;
                    end;
                    Data.Q1.Next;
               end;
               SceltaModelloMailCliForm.Free;}


               showMessage('Procedura di sgancio conclusa');
          end;
     end;
     ModStatoRicForm.Free;
     CaricaApriRicPend(xIDRic);
end;

procedure TSelPersForm.BStoricoRicClick(Sender: TObject);
begin
     StoricoRicForm:=TStoricoRicForm.create(self);
     DataRicerche.TStoricoRic.Open;
     StoricoRicForm.ShowModal;
     StoricoRicForm.Free;
     DataRicerche.TStoricoRic.Close;
end;

procedure TSelPersForm.ToolbarButton975Click(Sender: TObject);
begin
     if not Mainform.CheckProfile('22') then Exit;
     SpecificheForm:=TSpecificheForm.create(self);
     SpecificheForm.ShowModal;
     SpecificheForm.Free;
end;

procedure TSelPersForm.BUtenteModClick(Sender: TObject);
var xIDUtente: integer;
begin
     ElencoUtentiForm:=TElencoUtentiForm.create(self);
     ElencoUtentiForm.ShowModal;
     if ElencoUtentiForm.ModalResult=mrOK then begin
          // controllo associazione gi� esistente con la commessa
          Data.Q1.Close;
          Data.Q1.SQL.text:='select count(ID) Tot from EBC_RicercheUtenti where IDRicerca=:xIDRicerca: and IDUtente='+ElencoUtentiForm.QUsersID.asString;
          Data.Q1.ParamByName['xIDRicerca']:=DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
          Data.Q1.Open;
          if Data.Q1.FieldByname('Tot').asInteger>0 then begin
               Data.Q1.Close;
               MessageDlg('L''utente � gi� associato alla commessa: impossibile continuare',mtError, [mbOK],0);
               ElencoUtentiForm.Free;
               Exit;
          end;
          Data.Q1.Close;
          xIDUtente:=DataRicerche.TRicerchePend.FieldByName('IDUtente').AsInteger;
          // EBC_Ricerche.IDUtente
          DataRicerche.TRicerchePend.Edit;
          DataRicerche.TRicerchePend.FieldByName('IDUtente').AsInteger:=ElencoUtentiForm.QUsers.FieldByName('ID').AsInteger;
          with DataRicerche.TRicerchePend do begin
               Data.DB.BeginTrans;
               try
                    ApplyUpdates;
                    // inserimento in EBC_RicercheUtenti
                    Data.QTemp.Close;
                    Data.QTemp.SQL.text:='update EBC_RicercheUtenti set IDUtente=:xIDUtenteNew: '+
                         'where IDUtente=:xIDUtente: and IDRicerca=:xIDRicerca:';
                    Data.QTemp.ParamByName['xIDUtenteNew']:=ElencoUtentiForm.QUsers.FieldByName('ID').AsInteger;
                    Data.QTemp.ParamByName['xIDUtente']:=xIDUtente;
                    Data.QTemp.ParamByName['xIDRicerca']:=DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
                    Data.QTemp.ExecSQL;
                    Data.DB.CommitTrans;
                    CaricaApriRicPend(DataRicerche.QRicAttive.FieldByName('ID').AsInteger);
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
                    raise;
               end;
               CommitUpdates;
          end;
     end;
     ElencoUtentiForm.Free;
end;

procedure TSelPersForm.SpeedButton3Click(Sender: TObject);
begin
     if not Mainform.CheckProfile('214') then Exit;
     LegendaGestRicForm:=TLegendaGestRicForm.create(self);
     LegendaGestRicForm.ShowModal;
     LegendaGestRicForm.Free;
     Q.SQl.text:='select ggDiffDataIns,ggDiffDataUltimoContatto,CheckRicCandCV,CheckRicCandTel from global ';
     Q.Open;
     xggDiffDataIns:=Q.FieldByname('ggDiffDataIns').asInteger;
     xggDiffDataUltimoContatto:=Q.FieldByname('ggDiffDataUltimoContatto').asInteger;
     xCheckRicCandCV:=Q.FieldByname('CheckRicCandCV').AsBoolean;
     xCheckRicCandTel:=Q.FieldByname('CheckRicCandTel').AsBoolean;
     Q.Close;
     EseguiQueryCandidati;
end;

procedure TSelPersForm.BTipoClick(Sender: TObject);
begin
     Caption:='[S/13] - '+Caption;
     if MessageDlg('ATTENZIONE: stai cambiando il TIPO !! - Continuare ?',mtWarning, [mbYes,mbNo],0)=mrYes then begin
          SelTipoCommessaForm:=TSelTipoCommessaForm.create(self);
          if DataRicerche.TRicerchePend.FieldByName('Tipo').AsString<>'' then
               while (SelTipoCommessaForm.QTipo.FieldByName('TipoCommessa').AsString<>DataRicerche.TRicerchePend.FieldByName('Tipo').AsString)and
                    (not(SelTipoCommessaForm.QTipo.EOF)) do SelTipoCommessaForm.QTipo.Next;
          SelTipoCommessaForm.ShowModal;
          if SelTipoCommessaForm.ModalResult=mrOK then begin
               DataRicerche.TRicerchePend.Edit;
               DataRicerche.TRicerchePend.FieldByName('Tipo').AsString:=SelTipoCommessaForm.QTipo.FieldByName('TipoCommessa').AsString;
               with DataRicerche.TRicerchePend do begin
                    Data.DB.BeginTrans;
                    try
                         ApplyUpdates;
                         Data.DB.CommitTrans;
                         CaricaApriRicPend(DataRicerche.QRicAttive.FieldByName('ID').AsInteger);
                    except
                         Data.DB.RollbackTrans;
                         MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
                         raise;
                    end;
                    CommitUpdates;
               end;
          end;
          SelTipoCommessaForm.Free;
     end;
end;

procedure TSelPersForm.PCSelPersChange(Sender: TObject);
begin
     if DataRicerche.TRicerchePendggDiffApRic.AsString='' then begin
          if not(DataRicerche.dsRicerchePend.State=dsEdit) then DataRicerche.TRicerchePend.Edit;
          DataRicerche.TRicerchePendggDiff.FieldByName('ApRic').AsInteger:=0;
     end;
     if DataRicerche.TRicerchePendggDiffUcRic.AsString='' then begin
          if not(DataRicerche.dsRicerchePend.State=dsEdit) then DataRicerche.TRicerchePend.Edit;
          DataRicerche.TRicerchePendggDiff.FieldByName('AUcRic').AsInteger:=0;
     end;
     if DataRicerche.TRicerchePendggDiffColRic.AsString='' then begin
          if not(DataRicerche.dsRicerchePend.State=dsEdit) then DataRicerche.TRicerchePend.Edit;
          DataRicerche.TRicerchePendggDiff.FieldByName('ColRic').AsInteger:=0;
     end;

     if PCSelPers.ActivePage=TSCandAnnunci then begin
          DataRicerche.QAnnunciCand.Close;
          DataRicerche.QAnnunciCand.ParamByName['xIDRicerca']:=DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
          DataRicerche.QAnnunciCand.Open;
          LTotCandAnn.caption:=IntToStr(DataRicerche.QAnnunciCand.Recordcount);
     end else DataRicerche.QAnnunciCand.Close;
     if PCSelPers.ActivePage=TSCliCand then begin
          CliCandidatiFrame1.xIDCliente:=DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger;
          CliCandidatiFrame1.xIDRicerca:=DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
          CliCandidatiFrame1.CreaAlbero;
     end;
     // altri dati
     if PCSelPers.ActivePage=TSAltriDati then begin
          // utenti associati alla ricerca
          DataRicerche.QRicUtenti.Open;
          // rif. offerta
          if DataRicerche.TRicerchePendIDOfferta.AsString<>'' then begin
               Data.Q1.Close;
               Data.Q1.SQL.text:='select Rif from EBC_Offerte where ID='+DataRicerche.TRicerchePendIDOfferta.AsString;
               Data.Q1.Open;
               if not Data.Q1.IsEmpty then
                    ERifOfferta.Text:=Data.Q1.FieldByName('Rif').asString
               else ERifOfferta.Text:=' --- ';
               Data.Q1.Close;
          end;
     end else DataRicerche.QRicUtenti.Close;
     // parametri
     if PCSelPers.ActivePage=TSRicParams then begin
          DataRicerche.QRicLineeQuery.Open;
     end else begin
          DataRicerche.QRicLineeQuery.Close;
     end;

     // time-sheet
     if PCSelPers.ActivePage=TSTimeSheet then begin
          DataRicerche.QUsersLK.ParamByName['xOggi']:=date;
          DataRicerche.QUsersLK.Open;
          DataRicerche.QRicTimeSheet.Open;
          DataRicerche.QTimeRepDett.Open;
     end else begin
          DataRicerche.QRicTimeSheet.Close;
          DataRicerche.QUsersLK.Close;
          DataRicerche.QTimeRepDett.Close;
     end;

     // controllo di gestione
     if PCSelPers.ActivePage=TSGestione then begin
          EUtenteLav1.Text:=DataRicerche.TRicerchePend.FieldByName('Utente').AsString;
          if (DataRicerche.TRicerchePendIDUtente2.AsString<>'')and
               (DataRicerche.TRicerchePendID.FieldByName('Utente2').AsString>0) then begin
               Data.Q1.Close;
               Data.Q1.SQL.text:='select Nominativo from Users where ID='+DataRicerche.TRicerchePend.FieldByName('Utente2').AsString;
               Data.Q1.Open;
               EUtenteLav2.Text:=Data.Q1.FieldByName('Nominativo').asString;
          end else SEOreLav2.enabled:=False;
          if (DataRicerche.TRicerchePendIDUtente3.AsString<>'')and
               (DataRicerche.TRicerchePend.FieldByName('Utente3').AsString>0) then begin
               Data.Q1.Close;
               Data.Q1.SQL.text:='select Nominativo from Users where ID='+DataRicerche.TRicerchePendIDUtente3.AsString;
               Data.Q1.Open;
               EUtenteLav3.Text:=Data.Q1.FieldByName('Nominativo').asString;
               Data.Q1.Close;
          end else SEOreLav3.enabled:=False;
          DataRicerche.QRicTiming.Open;
          // calcolo ore lav. totale
          if DataRicerche.QRicTiming.Locate('IDUtente',DataRicerche.TRicerchePend.FieldByName('Utente').AsString, []) then
               ETotOreLav1.Text:=IntToStr(DataRicerche.TRicerchePend.FieldByName('OreLavUtente1').AsString+Round(DataRicerche.QRicTimingTot.Value/3600))
          else ETotOreLav1.Text:=IntToStr(DataRicerche.TRicerchePend.FieldByName('OreLavUtente1').AsString);
     end else DataRicerche.QRicTiming.Close;

     // target list
     if PCSelPers.ActivePage=TSTargetList then begin
          DataRicerche.QRicTargetList.Open;
     end else begin
          DataRicerche.QRicTargetList.Close;
     end;

     // file
     if PCSelPers.ActivePage=TSFile then begin
          QRicFile.Open;
     end else begin
          QRicFile.Close;
     end;
end;

procedure TSelPersForm.BInfoCand2Click(Sender: TObject);
begin
     if not Mainform.CheckProfile('00') then Exit;
     if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString,1,5))='ERGON' then begin
          SchedaSinteticaForm:=TSchedaSinteticaForm.create(self);
          SchedaSinteticaForm.xIDAnag:=DataRicerche.QAnnunciCand.FieldByName('IDAnagrafica').AsInteger;
          SchedaSinteticaForm.ShowModal;
          SchedaSinteticaForm.Free;
     end else begin
          SchedaCandForm:=TSchedaCandForm.create(self);
          if not PosizionaAnag(DataRicerche.QAnnunciCand.FieldByName('IDAnagrafica').AsInteger) then exit;
          SchedaCandForm.ShowModal;
          SchedaCandForm.Free;
          RiprendiAttivita;
     end;
end;

procedure TSelPersForm.BCurriculum1Click(Sender: TObject);
var x: string;
     i: integer;
begin
     if not Mainform.CheckProfile('051') then Exit;
     if not DataRicerche.QCandRic.Eof then begin
          PosizionaAnag(DataRicerche.QAnnunciCand.FieldByName('IDAnagrafica').AsInteger);
          Data.TTitoliStudio.Open;
          Data.TCorsiExtra.Open;
          Data.TLingueConosc.Open;
          Data.TEspLav.Open;
          CurriculumForm.ShowModal;

          Data.TTitoliStudio.Close;
          Data.TCorsiExtra.Close;
          Data.TLingueConosc.Close;
          Data.TEspLav.Close;
          MainForm.Pagecontrol5.ActivePage:=MainForm.TSStatoTutti;
          RiprendiAttivita;
     end;
end;

procedure TSelPersForm.ToolbarButton977Click(Sender: TObject);
var xFile: string;
     xProcedi: boolean;
begin
     ApriCV(DataRicerche.QAnnunciCand.FieldByName('IDAnagrafica').AsInteger);
end;

procedure TSelPersForm.ToolbarButton979Click(Sender: TObject);
begin
     if not Mainform.CheckProfile('054') then Exit;
     ApriFileWord(DataRicerche.QAnnunciCandCVNumero.asString);
     RiprendiAttivita;
end;

procedure TSelPersForm.ToolbarButton971Click(Sender: TObject);
var xMiniVal: string;
begin
     if DataRicerche.QAnnunciCand.IsEmpty then exit;
     // appartenenza a questa ricerca
     Data.QTemp.Close;
     Data.QTemp.SQL.text:='select count(*) Tot from EBC_CandidatiRicerche '+
          'where IDRicerca=:xIDRicerca: and IDAnagrafica=:xIDAnagrafica:';
     Data.QTemp.ParamByName['xIDRicerca']:=DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
     Data.QTemp.ParamByName['xIDAnagrafica']:=DataRicerche.QAnnunciCand.FieldByName('IDAnagrafica').AsInteger;
     Data.QTemp.Open;
     if Data.QTemp.FieldByName('Tot').asInteger>0 then begin
          MessageDlg('ATTENZIONE: il CV � gi� associato a questa ricerca.'+chr(13)+
               'IMPOSSIBILE proseguire',mtError, [mbOK],0);
          Data.QTemp.Close;
          exit;
     end;
     Data.QTemp.Close;

     // appartenenza ad un'altra ricerca
     Data.QTemp.Close;
     Data.QTemp.SQL.text:='select Progressivo,EBC_Clienti.descrizione Cliente '+
          'from EBC_CandidatiRicerche,EBC_Ricerche,EBC_Clienti '+
          'where IDRicerca<>:xIDRicerca: and IDAnagrafica=:xIDAnagrafica: '+
          'and EBC_CandidatiRicerche.IDRicerca=EBC_Ricerche.ID '+
          'and EBC_Ricerche.IDCliente=EBC_Clienti.ID';
     Data.QTemp.ParamByName['xIDRicerca']:=DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
     Data.QTemp.ParamByName['xIDAnagrafica']:=DataRicerche.QAnnunciCand.FieldByName('IDAnagrafica').AsInteger;
     Data.QTemp.Open;
     if not(Data.QTemp.IsEmpty) then
          if MessageDlg('ATTENZIONE: il CV � associato almeno alla ricerca n� '+
               Data.QTemp.FieldByName('Progressivo').asString+' ('+Data.QTemp.FieldByName('Cliente').asString+')'+chr(13)+
               'Vuoi proseguire lo stesso ?',mtWarning, [mbYes,mbNo],0)=mrNo then begin
               Data.QTemp.Close;
               RiprendiAttivita;
               exit;
          end;
     Data.QTemp.Close;
     // propriet� CV
     if (not((DataRicerche.QAnnunciCandIDProprietaCV.asString='')or(DataRicerche.QAnnunciCand.FieldByName('IDProprietaCV').AsInteger=0)))and
          (DataRicerche.QAnnunciCandIDProprietaCV.value<>DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger) then
          if MessageDlg('ATTENZIONE: il CV risulta di propriet� di un altro cliente .'+chr(13)+
               'Vuoi proseguire lo stesso ?',mtWarning, [mbYes,mbNo],0)=mrNo then begin
               RiprendiAttivita;
               exit;
          end;

     xMiniVal:='';
     if not InputQuery('Inserimento nella ricerca','Valutazione (1 lettera):',xMiniVal) then exit;

     Data.DB.BeginTrans;
     try
          if Q.Active then Q.Close;
          Q.SQL.text:='insert into EBC_CandidatiRicerche (IDRicerca,IDAnagrafica,Escluso,Stato,DataIns,MiniVal) '+
               'values (:xIDRicerca:,:xIDAnagrafica:,:xEscluso:,:xStato:,:xDataIns:,:xMiniVal:)';
          Q.ParamByName['xIDRicerca']:=DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
          Q.ParamByName['xIDAnagrafica']:=DataRicerche.QAnnunciCand.FieldByName('IDAnagrafica').AsInteger;
          Q.ParamByName['xEscluso']:=False;
          Q.ParamByName['xStato']:='inserito direttamente da annunci';
          Q.ParamByName['xDataIns']:=Date;
          Q.ParamByName['xMiniVal']:=xMiniVal;
          Q.ExecSQL;
          // aggiornamento stato anagrafica
          if Q.Active then Q.Close;
          Q.SQL.text:='update Anagrafica set IDStato=27,IDTipoStato=1 where ID='+DataRicerche.QAnnunciCandIDAnagrafica.asString;
          Q.ExecSQL;
          // aggiornamento storico
          if Q.Active then Q.Close;
          Q.SQL.text:='insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) '+
               'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xIDCliente:)';
          Q.ParamByName['xIDAnagrafica']:=DataRicerche.QAnnunciCand.FieldByName('IDAnagrafica').AsInteger;;
          Q.ParamByName['xIDEvento']:=19;
          Q.ParamByName['xDataEvento']:=date;
          Q.ParamByName['xAnnotazioni']:='ric.n�'+DataRicerche.TRicerchePend.FieldByName('Progressivo').AsString+' ('+DataRicerche.TRicerchePendCliente.Value+')';
          Q.ParamByName['xIDRicerca']:=DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
          Q.ParamByName['xIDUtente'] :=DataRicerche.TRicerchePend.FieldByName('IDUtente').AsInteger;
          Q.ParamByName['xIDCliente']:=DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger;
          Q.ExecSQL;
          Data.DB.CommitTrans;

          DataRicerche.QAnnunciCand.Close;
          DataRicerche.QAnnunciCand.Open;
     except
          Data.DB.RollbackTrans;
          MessageDlg('Errore sul database:  inserimento non avvenuto',mtError, [mbOK],0);
     end;
     DataRicerche.QCandRic.Close;
     DataRicerche.QCandRic.Open;
     LTotCand.Caption:=IntToStr(DataRicerche.QCandRic.RecordCount);
     RiprendiAttivita;
end;

procedure TSelPersForm.ToolbarButton976Click(Sender: TObject);
begin
     if DataRicerche.QAnnunciCand.RecordCount=0 then exit;
     DettAnnuncioForm:=TDettAnnuncioForm.create(self);
     DettAnnuncioForm.QAnnuncio.ParamByName['xID']:=DataRicerche.QAnnunciCand.FieldByName('IDAnnuncio').AsInteger;
     DettAnnuncioForm.QAnnuncio.Open;                                        
     DettAnnuncioForm.ShowModal;
     DettAnnuncioForm.Free;
end;

procedure TSelPersForm.SpeedButton4Click(Sender: TObject);
begin
     ElencoSediForm:=TElencoSediForm.create(self);
     ElencoSediForm.ShowModal;
     if ElencoSediForm.ModalResult=mrOK then begin
          DataRicerche.TRicerchePend.Edit;
          DataRicerche.TRicerchePend.FieldByName('IDSede').AsInteger:=ElencoSediForm.QSediID.Value;
          with DataRicerche.TRicerchePend do begin
               Data.DB.BeginTrans;
               try
                    ApplyUpdates;
                    Data.DB.CommitTrans;
                    CaricaApriRicPend(DataRicerche.QRicAttive.FieldByName('ID').AsInteger);
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
                    raise;
               end;
               CommitUpdates;
          end;
     end;
     ElencoSediForm.Free;
end;

procedure TSelPersForm.SpeedButton5Click(Sender: TObject);
begin
     SelContattiForm:=TSelContattiForm.create(self);
     SelContattiForm.QContatti.ParamByName['xIDCliente']:=DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger;
     SelContattiForm.QContatti.Open;
     SelContattiForm.ShowModal;
     if SelContattiForm.ModalResult=mrOK then begin
          DataRicerche.TRicerchePend.Edit;
          DataRicerche.TRicerchePend.FieldByName('IDContattoCli').AsInteger:=SelContattiForm.QContatti.FieldByName('ID').asInteger;
          with DataRicerche.TRicerchePend do begin
               Data.DB.BeginTrans;
               try
                    ApplyUpdates;
                    Data.DB.CommitTrans;
                    CaricaApriRicPend(DataRicerche.QRicAttive.FieldByName('ID').AsInteger);
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
                    raise;
               end;
               CommitUpdates;
          end;
     end;
     SelContattiForm.Free;
end;

procedure TSelPersForm.BUtente2Click(Sender: TObject);
var xIDRic: integer;
     xIDRuoloRic: string;
begin
     xIDRic:=DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
     ElencoUtentiForm:=TElencoUtentiForm.create(self);
     ElencoUtentiForm.ShowModal;
     if ElencoUtentiForm.ModalResult=mrOK then begin
          // controllo se � gi� associato a questa ricerca
          Data.Q1.Close;
          Data.Q1.SQL.text:='select count(ID) Tot from EBC_RicercheUtenti where IDRicerca=:xIDRicerca: and IDUtente='+ElencoUtentiForm.QUsersID.asString;
          Data.Q1.ParamByName['xIDRicerca']:=DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
          Data.Q1.Open;
          if Data.Q1.FieldByname('Tot').asInteger>0 then begin
               Data.Q1.Close;
               MessageDlg('L''utente � gi� associato alla commessa: impossibile continuare',mtError, [mbOK],0);
               ElencoUtentiForm.Free;
               Exit;
          end;
          Data.Q1.Close;
          xIDRuoloRic:=OpenSelFromTab('EBC_RicUtentiRuoli','ID','RuoloRic','Ruolo commessa','',False);
          if xIDRuoloRic='' then begin
               ElencoUtentiForm.Free;
               showmessage('operazione annullata');
               exit;
          end;
          if xIDRuoloRic='1' then begin
               MessageDlg('Impossibile inserire un altro capo-commessa: impossibile continuare',mtError, [mbOK],0);
               ElencoUtentiForm.Free;
               showmessage('operazione annullata');
               exit;
          end;
          // procedi
          with Data do begin
               DB.BeginTrans;
               try
                    // utenti
                    Q1.SQL.text:='insert into EBC_RicercheUtenti (IDRicerca,IDUtente,IDRuoloRic) '+
                         'values (:xIDRicerca:,:xIDUtente:,:xIDRuoloRic:)';
                    Q1.ParamByName['xIDRicerca']:=xIDRic;
                    Q1.ParamByName['xIDUtente']:=ElencoUtentiForm.QUsers.FieldByName('ID').AsInteger;
                    Q1.ParamByName['xIDRuoloRic']:=StrToInt(xIDRuoloRic);
                    Q1.ExecSQL;
                    DB.CommitTrans;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: operazione non effettuata',mtError, [mbOK],0);
               end;
          end;
     end;
     ElencoUtentiForm.Free;
     DataRicerche.QRicUtenti.Close;
     DataRicerche.QRicUtenti.Open;
end;

procedure TSelPersForm.BPasswordClick(Sender: TObject);
var xPassword: string;
begin
     xPassword:='';
     if not InputQuery('Modifica data prevista chiusura','Password di amministratore',xPassword) then exit;
     Q.Close;
     Q.SQL.text:='select Password from Users where Nominativo=''Administrator''';
     Q.Open;
     if CheckPassword(xPassword,Q.Fieldbyname('Password').asString) then begin
          DBDataPrev.enabled:=True;
          ShowMessage('� ora possibile modificare la data');
     end else begin
          MessageDlg('Password di amministratore errata',mtError, [mbOK],0);
     end;
     Q.Close;
end;

procedure TSelPersForm.DbDateEdit971Exit(Sender: TObject);
var xPassword: string;
begin
     if xVecchiadataInizio<>DbDateEdit971.Date then begin
          if DataRicerche.TRicerchePend.FieldByName('IDUtente').AsInteger;<>Mainform.xIDUtenteAttuale then
               MessageDlg('� necessario essere titolare della commessa per modificare la data di apertura',mtError, [mbOK],0)
          else begin
               xPassword:='';
               if not InputQuery('Modifica data inizio commessa','Password di '+GetDescUtenteResp(MainForm.xIDUtenteAttuale),xPassword) then begin
                    DbDateEdit971.Date:=xVecchiadataInizio;
                    exit;
               end;
               if not CheckPassword(xPassword,GetPwdUtenteResp(MainForm.xIDUtenteAttuale)) then begin
                    MessageDlg('Password errata',mtError, [mbOK],0);
                    DbDateEdit971.Date:=xVecchiadataInizio;
               end;
          end;
     end;
end;

procedure TSelPersForm.DbDateEdit971Enter(Sender: TObject);
begin
     xVecchiadataInizio:=DbDateEdit971.Date;
end;

procedure TSelPersForm.dxDBGrid1MouseUp(Sender: TObject;
     Button: TMouseButton; Shift: TShiftState; X,Y: Integer);
begin
     if (Button<>mbRight)or(Shift<> []) then Exit;
     TdxDBGridPopupMenuManager.Instance.ShowGridPopupMenu(Sender as TdxDBGrid);
end;

procedure TSelPersForm.dxDBGrid1Click(Sender: TObject);
begin
     RiprendiAttivita;
end;

procedure TSelPersForm.dxDBGrid1DblClick(Sender: TObject);
var xID: integer;
begin
     if not MainForm.CheckProfile('217') then Exit;
     ModDettCandRicForm:=TModDettCandRicForm.create(self);
     ModDettCandRicForm.DEDataIns.Date:=DataRicerche.QCandRic.FieldByName('DataIns').AsDateTime;
     ModDettCandRicForm.ESituazione.text:=DataRicerche.QCandRicStato.asString;
     ModDettCandRicForm.EMiniVal.text:=DataRicerche.QCandRicMiniVal.AsString;
     ModDettCandRicForm.ENote.text:=DataRicerche.QCandRicNote.AsString;
     ModDettCandRicForm.Showmodal;
     if ModDettCandRicForm.Modalresult=mrOK then begin
          Data.DB.BeginTrans;
          try
               Data.Q1.Close;
               Data.Q1.SQL.text:='update EBC_CandidatiRicerche set DataIns=:xDataIns:,Stato=:xStato:,MiniVal=:xMiniVal:,Note2=:xNote: where ID='+DataRicerche.QCandRicID.asString;
               if ModDettCandRicForm.DEDataIns.Text='' then
                    Data.Q1.ParamByName['xDataIns']:=null
               else Data.Q1.ParamByName['xDataIns']:=ModDettCandRicForm.DEDataIns.Date;
               Data.Q1.ParamByName['xStato']:=ModDettCandRicForm.ESituazione.text;
               Data.Q1.ParamByName['xMiniVal']:=ModDettCandRicForm.EMiniVal.text;
               Data.Q1.ParamByName['xNote']:=ModDettCandRicForm.ENote.text;
               Data.Q1.ExecSQL;
               Data.DB.CommitTrans;

               xID:=DataRicerche.QCandRic.FieldByName('ID').AsInteger;
               DataRicerche.QCandRic.Close;
               DataRicerche.QCandRic.Open;
               DataRicerche.QCandRic.locate('ID',xID, []);
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE:  operazione non completata',mtError, [mbOK],0);
          end;
     end;
     ModDettCandRicForm.Free;
end;

procedure TSelPersForm.dxDBGrid1CustomDrawCell(Sender: TObject;
     ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
     AColumn: TdxTreeListColumn; ASelected,AFocused,ANewItemRow: Boolean;
     var AText: string; var AColor: TColor; AFont: TFont;
     var AAlignment: TAlignment; var ADone: Boolean);
var xFile: string;
begin
     if (Acolumn=dxDBGrid1DataIns)and(ANode.Values[dxDBGrid1DataIns.index]<Date-xggDiffDataIns)
          and(ANode.Values[dxDBGrid1DataIns.index]>0) then Acolor:=clYellow;
     if (Acolumn=dxDBGrid1DataUltimoContatto)and(ANode.Values[dxDBGrid1DataUltimoContatto.index]<Date-xggDiffDataUltimoContatto)
          and(ANode.Values[dxDBGrid1DataUltimoContatto.index]>0) then Acolor:=clYellow;
     // controllo mancanza CV
     if xCheckRicCandCV then begin
          if (Acolumn=dxDBGrid1CVNumero) then begin
               xFile:=GetCVPath+'\i'+AText+'a.gif';
               if not FileExists(xFile) then AFont.Color:=clRed;
          end;
     end;
     // controllo mancanza telefoni
     // if xCheckRicCandTel then begin
     //     if (Acolumn=dxDBGrid1Cognome) then begin
     //          if ANode.Values[dxDBGrid1TelUffCell.index]='' then AFont.Color:=clYellow;
     //     end;
     //end;
end;

procedure TSelPersForm.Stampaelenco1Click(Sender: TObject);
begin
     if not LogEsportazioniCand(1) then exit;
     dxPrinter1.Preview(True,nil);
end;

procedure TSelPersForm.EsportainExcel1Click(Sender: TObject);
begin
     if not LogEsportazioniCand(1) then exit;
     Mainform.Save('xls','Microsoft Excel 4.0 Worksheet (*.xls)|*.xls','ExpGrid.xls',dxDBGrid1.SaveToXLS);
end;

procedure TSelPersForm.esportainHTML1Click(Sender: TObject);
begin
     if not LogEsportazioniCand(1) then exit;
     Mainform.Save('htm','HTML File (*.htm; *.html)|*.htm','ExpGrid.htm',dxDBGrid1.SaveToHTML);
end;

procedure TSelPersForm.BModCodCommessaClick(Sender: TObject);
var xRif: string;
     xID: integer;
begin
     if MessageDlg('ATTENZIONE: stai cambiando il NUMERO della commessa?'+chr(13)+
          'VUOI PROSEGUIRE ?',mtWarning, [mbYes,mbNo],0)=mrNo then exit;
     xRif:=DataRicerche.TRicerchePend.FieldByName('Progressivo').AsString;
     if InputQuery('Modifica rif./codice commessa','nuovo rif./codice:',xRif) then begin
          xID:=DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;;
          DataRicerche.TRicerchePend.Edit;
          DataRicerche.TRicerchePend.FieldByName('Progressivo').AsString:=xRif;
          with DataRicerche.TRicerchePend do begin
               Data.DB.BeginTrans;
               try
                    ApplyUpdates;
                    Data.DB.CommitTrans;
                    CaricaApriRicPend(xID);
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
                    raise;
               end;
               CommitUpdates;
          end;
     end;
end;

procedure TSelPersForm.ToolbarButton9710Click(Sender: TObject);
var xAnno,xMese,xGiorno: Word;
     xTotCosti: real;
     xCostoAzMedioOrario,xTotOreCommesse: integer;
begin
     if DataRicerche.DsRicerchePend.state=dsEdit then begin
          DataRicerche.TRicerchePend.Post;
          DataRicerche.TRicerchePend.ApplyUpdates;
          DataRicerche.TRicerchePend.CommitUpdates;
     end;
     // ricalcolo ore lav. totale (per sicurezza)
     if DataRicerche.QRicTiming.Locate('IDUtente',DataRicerche.TRicerchePend.FieldByName('IDUtente').AsInteger, []) then
          ETotOreLav1.Text:=IntToStr(DataRicerche.TRicerchePend.FieldByName('OreLavUtente1').AsInteger+Round(DataRicerche.QRicTimingTot.Value/3600))
     else ETotOreLav1.Text:=IntToStr(DataRicerche.TRicerchePend.FieldByName('OreLavUtente1').AsInteger);

     // (1) - Ricavi
     Q.Close;
     Q.SQL.text:='select sum(Importo) TotRicavi from EBC_CompensiRicerche where IDRicerca='+DataRicerche.TRicerchePendID.AsString;
     Q.Open;
     CE1.Value:=Q.FieldByName('TotRicavi').asInteger;

     // (2) - Costi annunci
     Q.Close;
     Q.SQL.text:='select sum(CostoANoiLire) TotAnnunci from Ann_AnnEdizData,Ann_AnnunciRicerche '+
          'where Ann_AnnEdizData.IDAnnuncio=Ann_AnnunciRicerche.IDAnnuncio '+
          'and Ann_AnnunciRicerche.IDRicerca='+DataRicerche.TRicerchePendID.AsString;
     Q.Open;
     CE2.Value:=Q.FieldByName('TotAnnunci').asInteger;

     // (3) - Costi fissi risorse umane
     Q.Close;
     Q.SQL.text:='SELECT CostoAzMedioOrario FROM AnagRetribuzHRMS Where IDAnagrafica='+IntToStr(GetIDAnagUtente(DataRicerche.TRicerchePend.FieldByName('IDUtente').AsInteger));
     Q.Open;
     xCostoAzMedioOrario:=Q.FieldByName('CostoAzMedioOrario').asInteger;
     CE3.Value:=StrToInt(ETotOreLav1.text)*xCostoAzMedioOrario;

     // (4) - Provvigione selezionatore
     if DataRicerche.TRicerchePend.FieldByName('Provvigione').AsBoolean then begin
          CE4.value:=DataRicerche.TRicerchePend.FieldByName('ProvvigionePerc').AsInteger/100*CE1.Value;
     end else CE4.value:=0;

     // (5) - Costi fissi di gestione proporzionati
     DecodeDate(DataRicerche.TRicerchePend.FieldByName('DataInizio').AsDateTime,xAnno,xMese,xGiorno);
     Q.Close;
     Q.SQL.text:='select Sum(ImportoLire) TotCosti from Costi where Anno='+IntToStr(xAnno);
     Q.Open;
     xTotCosti:=Q.FieldByName('TotCosti').asFloat;
     Q.Close; // attenzione: per ora solo per l'utente 1
     Q.SQL.text:='select Sum(OreLavUtente1) TotOreLavUtente1 from EBC_Ricerche '+
          'where datepart(year,DataInizio)='+IntToStr(xAnno);
     Q.Open;
     xTotOreCommesse:=Q.FieldByName('TotOreLavUtente1').asInteger;
     Q.Close;
     Q.SQL.text:='select sum(TotSec) TotSecRic from TimingRic,EBC_Ricerche '+
          'where TimingRic.IDRicerca=EBC_Ricerche.ID and datepart(year,DataInizio)='+IntToStr(xAnno);
     Q.Open;
     xTotOreCommesse:=xTotOreCommesse+Round(Q.FieldByName('TotSecRic').asInteger/3600);

     CE5.value:=(xTotCosti/xTotOreCommesse)*StrToInt(ETotOreLav1.text);

     // TOTALE
     Q.Close;
     CeTot.Value:=CE1.Value-(CE2.Value+CE3.Value+CE4.Value+CE5.Value);

     // messaggio per visualizzare costi usati per il calcolo
     ShowMessage('Costo Aziendale orario per '+DataRicerche.TRicerchePend.FieldByName('IDUtente').AsInteger+' = '+FormatFloat('#,###',xCostoAzMedioOrario)+chr(13)+
          'Totale costi nell''anno '+IntToStr(xAnno)+' = '+FormatFloat('#,###',xTotCosti)+chr(13)+
          'Totale ore commesse nell''anno '+IntToStr(xAnno)+' = '+IntToStr(xTotOreCommesse)+chr(13)+
          'Costo singola ora di lavoro nell''anno '+IntToStr(xAnno)+' = '+FormatFloat('#,###',Round(xTotCosti/xTotOreCommesse)));
end;

procedure TSelPersForm.SEOreLav1Exit(Sender: TObject);
begin
     // calcolo ore lav. totale
     if DataRicerche.QRicTiming.Locate('IDUtente',DataRicerche.TRicerchePend.FieldByName('IDUtente').AsInteger, []) then
          ETotOreLav1.Text:=IntToStr(DataRicerche.TRicerchePend.FieldByName('OreLavUtente1').AsInteger+Round(DataRicerche.QRicTimingTot.Value/3600))
     else ETotOreLav1.Text:=IntToStr(DataRicerche.TRicerchePend.FieldByName('OreLavUtente1').AsInteger);
end;

procedure TSelPersForm.SpeedButton6Click(Sender: TObject);
var xIDRuoloRic: string;
begin
     xIDRuoloRic:=DataRicerche.QRicUtentiIDRuoloRic.AsString;
     xIDRuoloRic:=OpenSelFromTab('EBC_RicUtentiRuoli','ID','RuoloRic','Ruolo commessa',xIDRuoloRic,False);
     if xIDRuoloRic='' then begin
          showmessage('operazione annullata');
          exit;
     end;
     if xIDRuoloRic='1' then begin
          MessageDlg('Impossibile inserire un altro capo-commessa: impossibile continuare',mtError, [mbOK],0);
          showmessage('operazione annullata');
          exit;
     end;
     with Data do begin
          DB.BeginTrans;
          try
               Q1.SQL.text:='update EBC_RicercheUtenti set IDRuoloRic=:xIDRuoloRic: '+
                    'where ID='+DataRicerche.QRicUtentiID.asString;
               Q1.ParamByName['xIDRuoloRic']:=StrToInt(xIDRuoloRic);
               Q1.ExecSQL;
               DB.CommitTrans;
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: operazione non effettuata',mtError, [mbOK],0);
          end;
     end;
     DataRicerche.QRicUtenti.Close;
     DataRicerche.QRicUtenti.Open;
end;

procedure TSelPersForm.SpeedButton7Click(Sender: TObject);
begin
     if DataRicerche.QRicUtenti.IsEmpty then exit;
     // controllo ore segnate per l'utente in questa commessa
     Data.QTemp.Close;
     Data.QTemp.SQL.Text:='select count(*) Tot from EBC_RicercheTimeSheet '+
          'where IDUtente=:xIDUtente: and IDRicerca=:xIDRicerca:';
     Data.QTemp.ParamByName['xIDUtente']:=DataRicerche.QRicUtenti.FieldByName('IDUtente').AsInteger;
     Data.QTemp.ParamByName['xIDRicerca']:=DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
     Data.QTemp.Open;
     if Data.QTemp.FieldByName('Tot').asInteger>0 then begin
          MessageDlg('Ci sono ore imputate all''utente nel time-sheet: impossibile cancellare.',mtError, [mbOK],0);
          Data.QTemp.Close;
          exit;
     end;
     Data.QTemp.Close;

     if MessageDlg('Sei sicuro di voler eliminare l''utente dalla commessa ?',mtWarning, [mbNo,mbYes],0)=mrYes then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.SQL.text:='delete from EBC_RicercheUtenti '+
                         'where ID='+DataRicerche.QRicUtentiID.asString;
                    Q1.ExecSQL;
                    DB.CommitTrans;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: operazione non effettuata',mtError, [mbOK],0);
               end;
          end;
          DataRicerche.QRicUtenti.Close;
          DataRicerche.QRicUtenti.Open;
     end;
end;

procedure TSelPersForm.dxDBGrid3MouseUp(Sender: TObject;
     Button: TMouseButton; Shift: TShiftState; X,Y: Integer);
begin
     if (Button<>mbRight)or(Shift<> []) then Exit;
     TdxDBGridPopupMenuManager.Instance.ShowGridPopupMenu(Sender as TdxDBGrid);
end;

procedure TSelPersForm.BRicClientiClick(Sender: TObject);
begin
     if xDisabilitaTargetList then begin
          showmessage('Funzione disabilitata');
          exit;
     end;
     if not MainForm.CheckProfile('171') then Exit;
     RicClientiForm:=TRicClientiForm.create(self);
     RicClientiForm.BAddTargetList.visible:=True;
     RicClientiForm.xIDRicerca:=DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
     RicClientiForm.xIDClienteRic:=DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger;
     // riempimento combobox1 con le tabelle
     with RicClientiForm do begin
          QTabelle.Close;
          QTabelle.Open;
          QTabelle.First;
          ComboBox1.Items.Clear;
          while not QTabelle.EOF do begin
               ComboBox1.Items.Add(QTabelleDescTabella.asString);
               QTabelle.Next;
          end;
          QRes1.Close;
     end;
     // togli multiselect
     RicClientiForm.DBGrid5.OptionsBehavior:= [edgoAutoSort,edgoDragScroll,edgoEnterShowEditor,edgoImmediateEditor,edgoStoreToIniFile,edgoTabThrough,edgoVertThrough];
     RicClientiForm.Showmodal;
     RicClientiForm.Free;
     DataRicerche.QRicTargetList.Close;
     DataRicerche.QRicTargetList.Open;
end;

procedure TSelPersForm.MenuItem1Click(Sender: TObject);
begin
     dxPrinter1.Preview(True,dxPrinter1Link2);
end;

procedure TSelPersForm.MenuItem2Click(Sender: TObject);
begin
     Mainform.Save('xls','Microsoft Excel 4.0 Worksheet (*.xls)|*.xls','ExpGrid.xls',dxDBGrid3.SaveToXLS);
end;

procedure TSelPersForm.MenuItem3Click(Sender: TObject);
begin
     Mainform.Save('htm','HTML File (*.htm; *.html)|*.htm','ExpGrid.htm',dxDBGrid3.SaveToHTML);
end;

procedure TSelPersForm.BTLOrgClick(Sender: TObject);
var i,k,j: integer;
     xArrayStd,xArrayNew: array[1..100] of integer;
begin
     if xDisabilitaTargetList then begin
          showmessage('Funzione disabilitata');
          exit;
     end;
     if DataRicerche.QRicTargetList.IsEmpty then exit;
     MainForm.LOBAziendeClick(self);
     xIDOrg:=DataRicerche.QRicTargetList.FieldByName('IDCliente').AsInteger;
     // carica organigramma
     with DataRicerche do begin
          if not((QAziende.active)and(QAziende.FieldByName('ID').AsInteger=xIDOrg)) then begin
               QOrgNew.Close;
               QAziende.close;
               QAziende.SQL.Text:='select EBC_Clienti.* ,EBC_attivita.Attivita from EBC_Clienti,EBC_attivita '+
                    'where EBC_Clienti.IDAttivita=EBC_attivita.ID and EBC_Clienti.ID='+IntToStr(xIDOrg);
               QAziende.Open;
               QOrgNew.Open;
               // se non c'� l'organigramma ==> proponi quello standard
               if QOrgNew.IsEmpty then begin
                    if MessageDlg('Per questa azienda non risulta impostato l''organigramma '+chr(13)+
                         'VUOI ASSOCIARE L''ORGANIGRAMMA STANDARD ?',mtWarning, [mbYes,mbNo],0)=mrYes then begin
                         Data.Qtemp.Close;
                         Data.Qtemp.SQL.text:='select * from OrgStandard order by PARENT';
                         Data.Qtemp.Open;
                         k:=1;
                         while not Data.Qtemp.EOF do begin
                              Data.DB.BeginTrans;
                              try
                                   Data.Q1.Close;
                                   Data.Q1.SQL.text:='insert into Organigramma (Tipo,Descrizione,DescBreve,Interim,InStaff,'+
                                        '                                       WIDTH,HEIGHT,TYPE,COLOR,IMAGE,IMAGEALIGN,ORDINE,ALIGN,IDCliente) '+
                                        'values (:xTipo:,:xDescrizione:,:xDescBreve:,:xInterim:,:xInStaff:,:xWIDTH:,:xHEIGHT:,:xTYPE:,'+
                                        '        :xCOLOR:,:xIMAGE:,:xIMAGEALIGN:,:xORDINE:,:xALIGN:,:xIDCliente:)';
                                   Data.Q1.ParamByName['xTipo']:=Data.Qtemp.FieldByName('Tipo').asString;
                                   Data.Q1.ParamByName['xDescrizione']:=Data.Qtemp.FieldByName('Descrizione').asString;
                                   Data.Q1.ParamByName['xDescBreve']:=Data.Qtemp.FieldByName('DescBreve').asString;
                                   Data.Q1.ParamByName['xInterim']:=Data.Qtemp.FieldByName('Interim').asBoolean;
                                   Data.Q1.ParamByName['xInStaff']:=Data.Qtemp.FieldByName('InStaff').asBoolean;
                                   Data.Q1.ParamByName['xWIDTH']:=Data.Qtemp.FieldByName('WIDTH').asInteger;
                                   Data.Q1.ParamByName['xHEIGHT']:=Data.Qtemp.FieldByName('HEIGHT').asInteger;
                                   Data.Q1.ParamByName['xTYPE']:=Data.Qtemp.FieldByName('TYPE').asstring;
                                   Data.Q1.ParamByName['xCOLOR']:=Data.Qtemp.FieldByName('COLOR').asInteger;
                                   Data.Q1.ParamByName['xIMAGE']:=Data.Qtemp.FieldByName('IMAGE').asInteger;
                                   Data.Q1.ParamByName['xIMAGEALIGN']:=Data.Qtemp.FieldByName('IMAGEALIGN').asstring;
                                   Data.Q1.ParamByName['xORDINE']:=Data.Qtemp.FieldByName('ORDINE').asInteger;
                                   Data.Q1.ParamByName['xALIGN']:=Data.Qtemp.FieldByName('ALIGN').asString;
                                   Data.Q1.ParamByName['xIDCliente']:=xIDOrg;
                                   Data.Q1.ExecSQL;
                                   //
                                   Data.Q2.SQL.text:='select @@IDENTITY as LastID';
                                   Data.Q2.Open;
                                   xArrayStd[k]:=Data.QTemp.FieldByName('ID').asInteger;
                                   xArrayNew[k]:=Data.Q2.FieldByName('LastID').asInteger;
                                   Data.Q2.Close;

                                   Data.DB.CommitTrans;
                              except
                                   Data.DB.RollbackTrans;
                                   MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
                              end;
                              inc(k);
                              Data.Qtemp.Next;
                         end;
                         // PARENT
                         Data.Q2.Close;
                         Data.Q2.SQL.text:='select ID from Organigramma where IDCliente='+IntToStr(xIDOrg)+' order by ID';
                         Data.Q2.Open;
                         Data.Qtemp.First;
                         Data.Q2.Next;
                         Data.Qtemp.Next;
                         k:=1;
                         while not Data.Qtemp.EOF do begin
                              // trova nuovo PARENT nell'xArrayNew
                              for i:=1 to 100 do
                                   if xArrayStd[i]=Data.QTemp.FieldByName('PARENT').asInteger then break;

                              Data.DB.BeginTrans;
                              try
                                   Data.Q1.Close;
                                   Data.Q1.SQL.text:='update Organigramma set PARENT=:xPARENT: '+
                                        'where ID='+IntToStr(Data.Q2.FieldByName('ID').asInteger);
                                   Data.Q1.ParamByName['xPARENT']:=xArrayNew[i];
                                   Data.Q1.ExecSQL;
                                   Data.DB.CommitTrans;
                              except
                                   Data.DB.RollbackTrans;
                                   MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
                              end;
                              inc(k);
                              Data.Qtemp.Next;
                              Data.Q2.Next;
                         end;
                         Data.Qtemp.Close;
                         Data.Q2.Close;
                    end;
                    QOrgNew.Close;
                    QOrgNew.Open;
               end;
               MainForm.PColor.Color:=DataRicerche.QOrgNew.FieldByName('COLOR').AsInteger;
          end;
     end;
     // se data diversa ==> log target list
     Data.QTemp.Close;
     Data.QTemp.SQL.text:='select Data from EBC_LogEsplorazioni '+
          'where IDTargetList='+DataRicerche.QRicTargetListID.AsString;
     Data.QTemp.Open;
     if (Data.QTemp.isEmpty)or((not Data.QTemp.isEmpty)and(Data.QTemp.FieldByName('Data').asDateTime<>Date)) then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text:='insert into EBC_LogEsplorazioni (IDTargetList,Data) '+
                         'values (:xIDTargetList:,:xData:)';
                    Q1.ParamByName['xIDTargetList']:=DataRicerche.QRicTargetList.FieldByName('ID').AsInteger;
                    Q1.ParamByName['xData']:=Date;
                    Q1.ExecSQL;
                    DB.CommitTrans;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: operazione non effettuata',mtError, [mbOK],0);
               end;
          end;
     end;
     Data.QTemp.Close;
     MainForm.DisegnaTVAziendaCand;
     MainForm.BTargetList.visible:=True;
     MainForm.xIDRicerca:=DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
     MainForm.xIDTargetList:=DataRicerche.QRicTargetList.FieldByName('ID').AsInteger;
     close;
end;

procedure TSelPersForm.BTLDeleteClick(Sender: TObject);
begin
     if DataRicerche.QRicTargetList.IsEmpty then exit;
     if MessageDlg('Sei sicuro di voler elminare l''azienda '+DataRicerche.QRicTargetListAzienda.Value+' dalla target list ?',mtwarning, [mbYes,mbNo],0)=mrNo then exit;
     with Data do begin
          DB.BeginTrans;
          try
               Q1.Close;
               Q1.SQL.text:='delete from EBC_RicercheTargetList '+
                    'where ID='+DataRicerche.QRicTargetListID.asString;
               Q1.ExecSQL;
               DB.CommitTrans;
               DataRicerche.QRicTargetList.Close;
               DataRicerche.QRicTargetList.Open;
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: operazione non effettuata',mtError, [mbOK],0);
          end;
     end;

end;

procedure TSelPersForm.BTLCandVaiClick(Sender: TObject);
begin
     if DataRicerche.QTLCandidati.IsEmpty then exit;
     DataRicerche.QCandRic.Locate('ID',DataRicerche.QTLCandidati.FieldByName('ID').AsInteger;, []);
     PCSelPers.ActivePage:=TSCandidati;
end;

procedure TSelPersForm.BTLElencoCliClick(Sender: TObject);
var xIDTargetList,xIDRicerca,xIDClienteRic: integer;
begin
     if xDisabilitaTargetList then begin
          showmessage('Funzione disabilitata');
          exit;
     end;
     xIDRicerca:=DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;;
     xIDClienteRic:=DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger;
     SelClienteForm:=TSelClienteForm.create(self);
     SelClienteForm.RGFiltro.ItemIndex:=0;
     SelClienteForm.ShowModal;
     if SelClienteForm.ModalResult=mrOK then begin
          // cliente attivo
          if SelClienteForm.TClienti.FieldByName('Stato').AsString='attivo' then begin
               if messageDlg('L''azienda � un cliente ATTIVO - Vuoi proseguire ?',mtWarning, [mbYes,mbNo],0)=mrNo then
                    exit;
          end;
          // stesso cliente della commessa
          if xIDClienteRic=SelClienteForm.TClienti.FieldByName('ID').AsInteger then begin
               messageDlg('L''azienda � il cliente della commessa !!',mtError, [mbOK],0);
               exit;
          end;
          // controllo se � gi� un cliente in target list
          Data.QTemp.Close;
          Data.QTemp.SQL.text:='select count(*) Tot from EBC_RicercheTargetList '+
               'where IDRicerca=:xIDRicerca: and IDCliente=:xIDCliente:';
          Data.QTemp.ParamByName['xIDRicerca']:=xIDRicerca;
          Data.QTemp.ParamByName['xIDCliente']:=SelClienteForm.TClienti.FieldByName('ID').AsInteger;;
          Data.QTemp.Open;
          if Data.QTemp.FieldByName('Tot').asInteger>0 then begin
               messageDlg('Azienda gi� presente nella target list !',mtError, [mbOK],0);
               Data.QTemp.Close;
               exit;
          end;
          // controllo blocco 1
          Data.QTemp.Close;
          Data.QTemp.SQL.text:='select count(*) Tot from EBC_Clienti where ID=:xID: and Blocco1=1';
          Data.QTemp.ParamByName['xID']:=SelClienteForm.TClienti.FieldByName('ID').AsInteger;;
          Data.QTemp.Open;
          if Data.QTemp.FieldByName('Tot').asInteger>0 then begin
               if messageDlg('ATTENZIONE:  l''azienda ha impostato il BLOCCO di livello 1 - VUOI PROSEGUIRE ? ',mtWarning, [mbYes,mbNo],0)=mrNo then begin
                    Data.QTemp.Close;
                    exit;
               end;
          end;
          // controllo storico target list
          if CheckStoricoTL(SelClienteForm.TClienti.FieldByName('ID').AsInteger;)=False then exit;

          if messageDlg('Sei sicuro di voler aggiungere l''azienda '+SelClienteForm.TClienti.FieldByName('Descrizione').AsString+' alla target List ?',
               mtWarning, [mbYes,mbNo],0)=mrNo then exit;
          Data.QTemp.Close;
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.SQL.text:='insert into EBC_RicercheTargetList (IDRicerca,IDCliente) '+
                         'values (:xIDRicerca:,:xIDCliente:)';
                    Q1.ParamByName['xIDRicerca']:=xIDRicerca;
                    Q1.ParamByName['xIDCliente']:=SelClienteForm.TClienti.FieldByName('ID').AsInteger;
                    Q1.ExecSQL;
                    Q1.SQL.text:='select @@IDENTITY as LastID';
                    Q1.Open;
                    xIDTargetList:=Q1.FieldByName('LastID').asInteger;
                    Q1.Close;
                    Q1.SQL.text:='insert into EBC_LogEsplorazioni (IDTargetList,Data) '+
                         'values (:xIDTargetList:,:xData:)';
                    Q1.ParamByName['xIDTargetList']:=xIDTargetList;
                    Q1.ParamByName['xData']:=Date;
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    DataRicerche.QRicTargetList.Close;
                    DataRicerche.QRicTargetList.Open;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: operazione non effettuata',mtError, [mbOK],0);
               end;
          end;
     end;
     SelClienteForm.Free;
end;

procedure TSelPersForm.BTLCandNewClick(Sender: TObject);
var xAttuale,xSettore: string;
     xVai: boolean;
     xRic,xLivProtez,xDicMess,xPassword,xUtenteResp,xClienteBlocco: string;
     xIDEvento,xIDClienteBlocco,xIDAnag,xIDRicerca,xIDTargetList: integer;
     QLocal: TQuery;
begin
     if DataRicerche.QRicTargetList.IsEmpty then exit;
     ElencoDipForm:=TElencoDipForm.create(self);
     ElencoDipForm.ShowModal;
     if ElencoDipForm.ModalResult=mrOK then begin
          // aggiunta soggetto alla commessa
          xIDAnag:=ElencoDipForm.TAnagDip.FieldByName('ID').AsInteger;
          xIDRicerca:=DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
          xIDTargetList:=DataRicerche.QRicTargetList.FieldByName('ID').AsInteger;
          Data.QTemp.Close;
          Data.QTemp.SQL.text:='select IDProprietaCV from Anagrafica where ID='+IntToStr(xIDAnag);
          Data.QTemp.Open;
          // controllo propriet� CV
          if (not((Data.QTemp.FieldByName('IDProprietaCV').asString='')or(Data.QTemp.FieldByName('IDProprietaCV').asInteger=0)))and
               (Data.QTemp.FieldByName('IDProprietaCV').asInteger<>DataRicerche.QRicTargetList.FieldByName('IDCliente').AsInteger) then
               if MessageDlg('ATTENZIONE: il CV risulta di propriet� di un altro cliente.'+chr(13)+
                    'Vuoi proseguire lo stesso ?',mtWarning, [mbYes,mbNo],0)=mrNo then begin
                    Data.QTemp.Close;
                    exit;
               end;
          Data.QTemp.Close;

          DataRicerche.TRicAnagIDX.Open;
          if not DataRicerche.TRicAnagIDX.FindKey([xIDRicerca,xIDAnag]) then begin
               xVai:=True;
               // controllo se � gi� associato ad un'altra ricerca per la STESSA azienda
               Data.Q1.Close;
               Data.Q1.SQL.clear;
               Data.Q1.SQl.Add('select EBC_Ricerche.Progressivo,EBC_StatiRic.StatoRic from EBC_CandidatiRicerche,EBC_Ricerche,EBC_StatiRic ');
               Data.Q1.SQl.Add('where EBC_CandidatiRicerche.IDRicerca=EBC_Ricerche.ID and EBC_Ricerche.IDStatoRic=EBC_StatiRic.ID');
               Data.Q1.SQl.Add('and EBC_CandidatiRicerche.IDAnagrafica=:xIDAnag: and EBC_Ricerche.IDCliente=:xIDCliente:');
               Data.Q1.Prepare;
               Data.Q1.ParamByName['xIDAnag']:=xIDAnag;
               Data.Q1.ParamByName['xIDCliente']:=DataRicerche.QRicTargetList.FieldByName('IDCliente').AsInteger;
               Data.Q1.Open;
               if not Data.Q1.IsEmpty then begin
                    xRic:='';
                    while not Data.Q1.EOF do begin xRic:=xRic+'N�'+Data.Q1.FieldByName('Progressivo').asString+' ('+Data.Q1.FieldByName('StatoRic').asString+') '; Data.Q1.Next; end;
                    if MessageDlg('Il soggetto � associato gi� alle seguenti ricerche per lo stesso cliente: '+chr(13)+xRic+chr(13)+'PROCEDERE COMUNQUE ?',mtWarning, [mbYes,mbNo],0)=mrNo then xVai:=False;
               end;
               // controllo incompatibilit�
               if IncompAnagCli(xIDAnag,DataRicerche.QRicTargetList.FieldByName('IDCliente').AsInteger) then
                    if MessageDlg('Il soggetto risulta INCOMPATIBILE con questo cliente o con un cliente associato'+chr(13)+
                         'PROCEDERE COMUNQUE ?',mtWarning, [mbYes,mbNo],0)=mrNo then xVai:=False;

               // controllo blocco livello 1 o 2
               xIDClienteBlocco:=CheckAnagInseritoBlocco1(xIDAnag);
               if xIDClienteBlocco>0 then begin
                    xLivProtez:=copy(IntToStr(xIDClienteBlocco),1,1);
                    if xLivProtez='1' then begin
                         xIDClienteBlocco:=xIDClienteBlocco-10000;
                         xClienteBlocco:=GetDescCliente(xIDClienteBlocco);
                         xDicMess:='Stai inserendo in commessa un candidato appartenente ad una azienda ('+xClienteBlocco+') con blocco di livello 1, ';
                    end else begin
                         xIDClienteBlocco:=xIDClienteBlocco-20000;
                         xClienteBlocco:=GetDescCliente(xIDClienteBlocco);
                         xDicMess:='Stai inserendo in commessa un candidato appartenente ad una azienda ATTIVA ('+xClienteBlocco+'), ';
                    end;
                    if MessageDlg('ATTENZIONE!! PROTEZIONE CLIENTI LIVELLO '+xLivProtez+chr(13)+xDicMess+
                         'protetta dalle normative interne, se sei autorizzato premi SI, altrimenti premi NO. '+
                         'Se si prosegue, l''operazione verr� registrata nello storico del soggetto come forzatura. '+
                         'Verr� inviato un messaggio di promemoria al responsabile diretto',mtWarning, [mbYes,mbNo],0)=mrNo then xVai:=False
                    else begin
                         // pwd
                         xPassword:='';
                         xUtenteResp:=GetDescUtenteResp(MainForm.xIDUtenteAttuale);
                         if not InputQuery('Forzatura blocco livello '+xLivProtez,'Password di '+xUtenteResp,xPassword) then exit;
                         if not CheckPassword(xPassword,GetPwdUtenteResp(MainForm.xIDUtenteAttuale)) then begin
                              MessageDlg('Password errata',mtError, [mbOK],0);
                              xVai:=False;
                         end;
                         if xVai then begin
                              // promemoria al resp.
                              QLocal:=CreateQueryFromString('insert into Promemoria (IDUtente,IDUtenteDa,DataIns,Testo,Evaso,DataDaLeggere) '+
                                   'values (:xIDUtente:,:xIDUtenteDa:,:xDataIns:,:xTesto:,:xEvaso:,:xDataDaLeggere:)');
                              QLocal.ParamByName['xIDUtente']:=GetIDUtenteResp(MainForm.xIDUtenteAttuale);
                              QLocal.ParamByName['xIDUtenteDa']:=MainForm.xIDUtenteAttuale;
                              QLocal.ParamByName['xDataIns']:=Date;
                              QLocal.ParamByName['xTesto']:='forzatura livello '+xLivProtez+' per CV n� '+ElencoDipForm.TAnagDipCVNumero.AsString;
                              QLocal.ParamByName['xEvaso']:=False;
                              QLocal.ParamByName['xDataDaLeggere']:=Date;
                              QLocal.ExecSQL;
                              // registra nello storico soggetto
                              xIDEvento:=GetIDEvento('forzatura Blocco livello '+xLivProtez);
                              if xIDEvento>0 then begin
                                   with Data.Q1 do begin
                                        close;
                                        SQL.text:='insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) '+
                                             'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xIDCliente:)';
                                        ParamByName['xIDAnagrafica']:=xIDAnag;
                                        ParamByName['xIDEvento']:=xIDevento;
                                        ParamByName['xDataEvento']:=Date;
                                        ParamByName['xAnnotazioni']:='cliente: '+xClienteBlocco;
                                        ParamByName['xIDRicerca']:=xIDRicerca;
                                        ParamByName['xIDUtente']:=MainForm.xIDUtenteAttuale;
                                        ParamByName['xIDCliente']:=xIDClienteBlocco;
                                        ExecSQL;
                                   end;
                              end;
                         end;
                    end;
               end;

               if xVai then begin
                    Data.DB.BeginTrans;
                    try
                         if Data.Q1.Active then Data.Q1.Close;
                         Data.Q1.SQL.text:='insert into EBC_CandidatiRicerche (IDRicerca,IDAnagrafica,Escluso,Stato,DataIns,IDTargetList) '+
                              'values (:xIDRicerca:,:xIDAnagrafica:,:xEscluso:,:xStato:,:xDataIns:,:xIDTargetList:)';
                         Data.Q1.ParamByName['xIDRicerca']:=xIDRicerca;
                         Data.Q1.ParamByName['xIDAnagrafica']:=xIDAnag;
                         Data.Q1.ParamByName['xEscluso']:=False;
                         Data.Q1.ParamByName['xStato']:='inserito da Target List';
                         Data.Q1.ParamByName['xDataIns']:=Date;
                         Data.Q1.ParamByName['xIDTargetList']:=xIDTargetList;
                         Data.Q1.ExecSQL;
                         // aggiornamento stato anagrafica
                         if Data.Q1.Active then Data.Q1.Close;
                         Data.Q1.SQL.text:='update Anagrafica set IDStato=27,IDTipoStato=1 where ID='+IntToStr(xIDAnag);
                         Data.Q1.ExecSQL;
                         // aggiornamento storico
                         if Data.Q1.Active then Data.Q1.Close;
                         Data.Q1.SQL.text:='insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) '+
                              'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xIDCliente:)';
                         Data.Q1.ParamByName['xIDAnagrafica']:=xIDAnag;
                         Data.Q1.ParamByName['xIDEvento']:=19;
                         Data.Q1.ParamByName['xDataEvento']:=date;
                         Data.Q1.ParamByName['xAnnotazioni']:='inserito da target list';
                         Data.Q1.ParamByName['xIDRicerca']:=xIDRicerca;
                         Data.Qtemp.Close;
                         Data.Qtemp.SQL.text:='select IDUtente,IDCliente from EBC_Ricerche where ID='+IntToStr(xIDRicerca);
                         Data.Qtemp.Open;
                         Data.Q1.ParamByName['xIDUtente']:=Data.Qtemp.FieldByName('IDUtente').asInteger;
                         Data.Q1.ParamByName['xIDCliente']:=Data.Qtemp.FieldByName('IDCliente').asInteger;
                         Data.Qtemp.Close;
                         Data.Q1.ExecSQL;
                         Data.DB.CommitTrans;
                         DataRicerche.QTLCandidati.Close;
                         DataRicerche.QTLCandidati.Open;
                         DataRicerche.QCandRic.Close;
                         DataRicerche.QCandRic.Open;
                    except
                         Data.DB.RollbackTrans;
                         MessageDlg('Errore sul database:  inserimento non avvenuto',mtError, [mbOK],0);
                    end;
               end;
          end;
          DataRicerche.TRicAnagIDX.Close;
     end;
     ElencoDipForm.Free;
end;

procedure TSelPersForm.dxDBGrid4DblClick(Sender: TObject);
begin
     BTLCandVaiClick(self);
end;

procedure TSelPersForm.BTLCandInsNuovoClick(Sender: TObject);
var xIDAnag: integer;
     xS,xSettore: string;
     xVai,xNumeroValido: boolean;
     xCVNumero: string;
begin
     if DataRicerche.QRicTargetList.IsEmpty then exit;
     with MainForm do begin
          if not CheckProfile('65') then Exit;
          if not CheckProfile('000') then Exit;
          // controllo numero massimo consentiti
          NuovoSoggettoForm:=TNuovoSoggettoForm.create(self);
          NuovoSoggettoForm.ShowModal;
          if NuovoSoggettoForm.ModalResult=mrOK then begin
               Data.DB.BeginTrans;
               try
                    // NUMERO CV
                    if not cCVNumAutomatico then begin
                         // manuale
                         xCVNumero:='0';
                         xNumeroValido:=False;
                         while not xNumeroValido do begin
                              if not InputQuery('Numero CV manuale','Numero CV:',xCVNumero) then begin
                                   xVai:=False;
                                   Break;
                              end else begin
                                   if xCVNumero='' then xCVNumero:='0';
                                   if xCVNumero='0' then begin
                                        if MessageDlg('ATTENZIONE: Non � possibile inserire un n� CV uguale a ZERO'+chr(13)+
                                             'Verr� associato il progressivo autoincrementante interno.'+chr(13)+
                                             'Vuoi procedere (Yes) o Annullare l''inserimento (no) ?',mtWarning, [mbYes,mbNo],0)=mrNo then begin
                                             xVai:=False;
                                             Break;
                                        end;
                                   end;
                                   // controllo esistenza n� CV
                                   Data.QTemp.SQL.text:='select Cognome,Nome from Anagrafica where CVNumero='+xCVNumero;
                                   Data.QTemp.Open;
                                   if Data.QTemp.RecordCount>0 then begin
                                        if MessageDlg('ATTENZIONE: esiste gi� il numero CV '+xCVNumero+' abbinato al soggetto:'+chr(13)+
                                             Data.QTemp.FieldByName('Cognome').asString+' '+Data.QTemp.FieldByName('Nome').asString+chr(13)+chr(13)+
                                             'Vuoi procedere (Yes) o Annullare l''inserimento (no) ?',mtWarning, [mbYes,mbNo],0)=mrNo then begin
                                             xVai:=False;
                                             break;
                                        end;
                                   end else xNumeroValido:=True;
                                   Data.QTemp.Close;
                              end;
                         end;
                    end else xCVNumero:='0'; // automatico

                    Data.QTemp.Close;
                    Data.QTemp.SQL.Text:='insert into Anagrafica (CVNumero,Cognome,Nome,IDStato,IDTipoStato,IDProprietaCV,Sesso) '+
                         '  values (:xCVNumero:,:xCognome:,:xNome:,:xIDStato:,:xIDTipoStato:,:xIDProprietaCV:,:xSesso:)';
                    Data.QTemp.ParamByName['xCVNumero']:=StrtoInt(xCVNumero);
                    Data.QTemp.ParamByName['xCognome']:=NuovoSoggettoForm.ECogn.Text;
                    Data.QTemp.ParamByName['xNome']:=NuovoSoggettoForm.ENome.Text;
                    Data.QTemp.ParamByName['xIDStato']:=27;
                    Data.QTemp.ParamByName['xIDTipoStato']:=1;
                    Data.QTemp.ParamByName['xIDProprietaCV']:=0;
                    Data.QTemp.ParamByName['xSesso']:=NuovoSoggettoForm.CBSesso.Text;
                    Data.QTemp.ExecSQL;

                    Data.QTemp.SQL.text:='select @@IDENTITY as LastID';
                    Data.QTemp.Open;
                    xIDAnag:=Data.QTemp.FieldByName('LastID').asInteger;
                    Data.QTemp.Close;

                    // log inserimento
                    Log_Operation(xIDUtenteAttuale,'Anagrafica',xIDAnag,'I');

                    // CV Numero automatico (o nullo)
                    if xCVNumero='0' then begin
                         Data.QTemp.Close;
                         Data.QTemp.SQL.text:='update Anagrafica set CVNumero='+IntToStr(xIDAnag)+' where ID='+IntToStr(xIDAnag);
                         xCVNumero:=IntToStr(xIDAnag);
                         Data.QTemp.ExecSQL;
                    end;

                    // ALTRE INFO
                    Data.QTemp.SQL.text:='insert into AnagAltreInfo (IDAnagrafica,Note) '+
                         ' values (:xIDAnagrafica:,:xNote:)';
                    Data.QTemp.ParamByName['xIDAnagrafica']:=xIDAnag;
                    Data.QTemp.ParamByName['xNote']:=NuovoSoggettoForm.ENote.text;
                    Data.QTemp.ExecSQL;

                    // inserimento in ESPERIENZE LAVORATIVE
                    if DataRicerche.QRicTargetListIDAttivita.asString='' then xSettore:='0'
                    else xSettore:=DataRicerche.QRicTargetListIDAttivita.AsString;
                    Data2.QTemp.SQL.clear;
                    Data2.QTemp.SQL.Add('insert into EsperienzeLavorative (IDAnagrafica,IDAzienda,IDSettore,Attuale,IDMansione,IDArea) '+
                         'values ('+IntToStr(xIDAnag)+','+
                         DataRicerche.QRicTargetListIDCliente.asString+','+
                         xSettore+',1,:xIDMansione:,:xIDArea:)');
                    Data2.QTemp.ParamByName['xIDMansione']:=NuovoSoggettoForm.xIDRuolo;
                    Data2.QTemp.ParamByName['xIDArea']:=NuovoSoggettoForm.xIDArea;
                    Data2.QTemp.ExecSQL;

                    // riga nello storico con evento specifico
                    Data.QTemp.SQL.text:='insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente) '+
                         'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:)';
                    Data.QTemp.ParamByName['xIDAnagrafica']:=xIDAnag;
                    Data.QTemp.ParamByName['xIDEvento']:=87;
                    Data.QTemp.ParamByName['xDataEvento']:=Date;
                    Data.QTemp.ParamByName['xAnnotazioni']:='azienda: '+DataRicerche.QRicTargetListAzienda.asString;
                    Data.QTemp.ParamByName['xIDRicerca']:=0;
                    Data.QTemp.ParamByName['xIDUtente']:=xIDUtenteAttuale;
                    Data.QTemp.ExecSQL;

                    // inserimento in target list
                    if Data.Q1.Active then Data.Q1.Close;
                    Data.Q1.SQL.text:='insert into EBC_CandidatiRicerche (IDRicerca,IDAnagrafica,Escluso,Stato,DataIns,IDTargetList) '+
                         'values (:xIDRicerca:,:xIDAnagrafica:,:xEscluso:,:xStato:,:xDataIns:,:xIDTargetList:)';
                    Data.Q1.ParamByName['xIDRicerca']:=DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
                    Data.Q1.ParamByName['xIDAnagrafica']:=xIDAnag;
                    Data.Q1.ParamByName['xEscluso']:=False;
                    Data.Q1.ParamByName['xStato']:='inserito da Target List';
                    Data.Q1.ParamByName['xDataIns']:=Date;
                    Data.Q1.ParamByName['xIDTargetList']:=DataRicerche.QRicTargetList.FieldByName('ID').AsInteger;
                    Data.Q1.ExecSQL;
                    // aggiornamento storico
                    if Data.Q1.Active then Data.Q1.Close;
                    Data.Q1.SQL.text:='insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) '+
                         'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xIDCliente:)';
                    Data.Q1.ParamByName['xIDAnagrafica']:=xIDAnag;
                    Data.Q1.ParamByName['xIDEvento']:=19;
                    Data.Q1.ParamByName['xDataEvento']:=date;
                    Data.Q1.ParamByName['xAnnotazioni']:='inserito da target list';
                    Data.Q1.ParamByName['xIDRicerca']:=DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
                    Data.Qtemp.Close;
                    Data.Qtemp.SQL.text:='select IDUtente,IDCliente from EBC_Ricerche where ID='+IntToStr(xIDRicerca);
                    Data.Qtemp.Open;
                    Data.Q1.ParamByName['xIDUtente']:=Data.Qtemp.FieldByName('IDUtente').asInteger;
                    Data.Q1.ParamByName['xIDCliente']:=Data.Qtemp.FieldByName('IDCliente').asInteger;
                    Data.Qtemp.Close;
                    Data.Q1.ExecSQL;
                    Data.DB.CommitTrans;
                    DataRicerche.QTLCandidati.Close;
                    DataRicerche.QTLCandidati.Open;
                    DataRicerche.QCandRic.Close;
                    DataRicerche.QCandRic.Open;
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('ATTENZIONE: errore sul database - Inserimento non avvenuto',mtError, [mbOK],0);
               end;
          end;
          NuovoSoggettoForm.Free;
     end;
end;

procedure TSelPersForm.dxDBGrid4CustomDrawCell(Sender: TObject;
     ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
     AColumn: TdxTreeListColumn; ASelected,AFocused,ANewItemRow: Boolean;
     var AText: string; var AColor: TColor; AFont: TFont;
     var AAlignment: TAlignment; var ADone: Boolean);
var xFile: string;
begin
     // controllo mancanza CV
     if xCheckRicCandCV then begin
          if (Acolumn=dxDBGrid4CVNumero) then begin
               xFile:=GetCVPath+'\i'+AText+'a.gif';
               if not FileExists(xFile) then AFont.Color:=clRed;
          end;
     end;
end;

procedure TSelPersForm.dxDBGrid4MouseUp(Sender: TObject;
     Button: TMouseButton; Shift: TShiftState; X,Y: Integer);
begin
     if (Button<>mbRight)or(Shift<> []) then Exit;
     TdxDBGridPopupMenuManager.Instance.ShowGridPopupMenu(Sender as TdxDBGrid);
end;

procedure TSelPersForm.MenuItem4Click(Sender: TObject);
begin
     if not LogEsportazioniCand(2) then exit;
     dxPrinter1.Preview(True,dxPrinter1Link3);
end;

procedure TSelPersForm.MenuItem5Click(Sender: TObject);
begin
     if not LogEsportazioniCand(2) then exit;
     Mainform.Save('xls','Microsoft Excel 4.0 Worksheet (*.xls)|*.xls','ExpGrid.xls',dxDBGrid4.SaveToXLS);
end;

procedure TSelPersForm.MenuItem6Click(Sender: TObject);
begin
     if not LogEsportazioniCand(2) then exit;
     Mainform.Save('htm','HTML File (*.htm; *.html)|*.htm','ExpGrid.htm',dxDBGrid4.SaveToHTML);
end;

function TSelPersForm.CheckStoricoTL(xIDAzienda: integer): boolean;
begin
     // restituisce TRUE se procedere con l'associazione, FALSE se fermarsi
     StoricoTargetListForm:=TStoricoTargetListForm.create(self);
     StoricoTargetListForm.QStoricoTL.ParamByName['xIDCLiente']:=xIDAzienda;
     StoricoTargetListForm.QStoricoTL.Open;
     if StoricoTargetListForm.QStoricoTL.IsEmpty then begin
          Result:=True;
          StoricoTargetListForm.Free;
          exit;
     end;
     StoricoTargetListForm.ShowModal;
     if StoricoTargetListForm.ModalResult=mrOk then result:=True
     else result:=False;
     StoricoTargetListForm.Free;
end;

procedure TSelPersForm.BTimeSheetNewClick(Sender: TObject);
begin
     DataRicerche.QRicTimeSheet.Insert;
end;

procedure TSelPersForm.BTimeSheetDelClick(Sender: TObject);
begin
     if not DataRicerche.QTimeRepDett.IsEmpty then begin
          ShowMessage('Ci sono causali di dettaglio: IMPOSSIBILE CANCELLARE');
          exit;
     end;
     if MessageDlg('Sei sicuro di voler eliminare la voce ?',mtWarning, [mbYes,mbNo],0)=mrYes then
          DataRicerche.QRicTimeSheet.delete;
end;

procedure TSelPersForm.BTimeSheetOKClick(Sender: TObject);
begin
     DataRicerche.QRicTimeSheet.Post;
end;

procedure TSelPersForm.BTimeSheetCanClick(Sender: TObject);
begin
     DataRicerche.QRicTimeSheet.Cancel;
end;

procedure TSelPersForm.modificanumeroditelefono1Click(Sender: TObject);
var xTel: string;
     xID: integer;
begin
     xTel:=DataRicerche.QRicTargetListtelefono.Value;
     if not InputQuery('Modifica telefono azienda','nuovo numero:',xTel) then exit;
     with Data do begin
          DB.BeginTrans;
          try
               Q1.SQL.text:='update EBC_Clienti set Telefono=:xTelefono:'+
                    'where ID='+DataRicerche.QRicTargetListIDCliente.asString;
               Q1.ParamByName['xTelefono']:=xTel;
               Q1.ExecSQL;
               DB.CommitTrans;
               xID:=DataRicerche.QRicTargetList.FieldByName('ID').AsInteger
               DataRicerche.QRicTargetList.Close;
               DataRicerche.QRicTargetList.Open;
               DataRicerche.QRicTargetList.Locate('ID',xID, []);
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: operazione non effettuata',mtError, [mbOK],0);
          end;
     end;

end;

procedure TSelPersForm.BTLNoteClienteClick(Sender: TObject);
begin
     NoteClienteForm:=TNoteClienteForm.create(self);
     NoteClienteForm.QNoteCliente.ParamByName['xIDCliente']:=DataRicerche.QRicTargetList.FieldByName('IDCliente').AsInteger;
     NoteClienteForm.QNoteCliente.Open;
     NoteClienteForm.ShowModal;
     NoteClienteForm.Free;
end;

procedure TSelPersForm.BTRepDettNewClick(Sender: TObject);
begin
     if DataRicerche.QRicTimeSheet.IsEmpty then exit;
     if DataRicerche.DsQRicTimeSheet.state in [dsInsert,dsEdit] then DataRicerche.QRicTimeSheet.Post;
     DataRicerche.QTimeRepDett.Insert;
end;

procedure TSelPersForm.BTRepDettDelClick(Sender: TObject);
begin
     if DataRicerche.QRicTimeSheet.IsEmpty then exit;
     if DataRicerche.QTimeRepDett.IsEmpty then exit;
     DataRicerche.QTimeRepDett.Delete;
end;

procedure TSelPersForm.BTRepDettOKClick(Sender: TObject);
begin
     DataRicerche.QTimeRepDett.Post;
end;

procedure TSelPersForm.BTRepDettCanClick(Sender: TObject);
begin
     DataRicerche.QTimeRepDett.Cancel;
end;

procedure TSelPersForm.BAggTotTRepClick(Sender: TObject);
var xTot: real;
begin
     xTot:=0;
     DataRicerche.QTimeRepDett.First;
     while not DataRicerche.QTimeRepDett.EOF do begin
          xTot:=xTot+DataRicerche.QTimeRepDett.FieldByName('Ore').AsFloat;
          DataRicerche.QTimeRepDett.Next;
     end;
     DataRicerche.QRicTimeSheet.Edit;
     DataRicerche.QRicTimeSheet.FieldByName('OreConsuntivo').AsFloat;:=xTot;
     DataRicerche.QRicTimeSheet.Post;
end;

procedure TSelPersForm.ToolbarButton9711Click(Sender: TObject);
begin
     OpenTab('TimeSheetCausali', ['ID','Causale'], ['Causale']);
     DataRicerche.QTRepCausali.Close;
     DataRicerche.QTRepCausali.Open;
end;

function TSelPersForm.LogEsportazioniCand(xDaDove: integer): boolean;
var xCriteri: string;
begin
     // restituisce FALSE se l'utente ha abortito l'operazione
     Result:=True;
     if MessageDlg('ATTENZIONE:  l''esportazione di dati � un''operazione soggetta a registrazione'+chr(13)+
          'secondo la normativa sulla privacy.  SEI SICURO DI VOLER PROSEGUIRE ?',mtWarning, [mbYes,mbNo],0)=mrNo then begin
          Result:=False;
          exit;
     end;
     with Data do begin
          DB.BeginTrans;
          try
               if xDaDove=1 then begin
                    DataRicerche.QCandRic.First;
                    xCriteri:='CANDIDATI NELLA COMMESSA '+DataRicerche.TRicerchePend.FieldByName('Progressivo').AsString+': '+chr(13);
                    while not DataRicerche.QCandRic.EOF do begin
                         xCriteri:=xCriteri+DataRicerche.QCandRicCVNumero.AsString+' '+DataRicerche.QCandRic.FieldByName('Cognome').AsString+' '+DataRicerche.QCandRic.FieldByName('Nome').AsString+chr(13);
                         DataRicerche.QCandRic.Next;
                    end;
               end else begin
                    DataRicerche.QTLCandidati.First;
                    xCriteri:='CANDIDATI NELLA TARGET LIST per il cliente '+DataRicerche.QRicTargetListAzienda.Value+' (commessa '+DataRicerche.TRicerchePend.FieldByName('Progressivo').AsString+'): '+chr(13);
                    while not DataRicerche.QTLCandidati.EOF do begin
                         xCriteri:=xCriteri+DataRicerche.QTLCandidatiCVNumero.AsString+' '+DataRicerche.QTLCandidati.FieldByName('Cognome').AsString+' '+DataRicerche.QTLCandidati.FieldByName('Nome').AsString+chr(13);
                         DataRicerche.QTLCandidati.Next;
                    end;
               end;
               Q1.SQL.text:='insert into LogEsportazCand (IDUtente,Data,Criteri,StringaSQL,ResultNum) '+
                    ' values (:xIDUtente:,:xData:,:xCriteri:,:xStringaSQL:,:xResultNum:)';
               Q1.ParamByName['xIDUtente']:=MainForm.xIDUtenteAttuale;
               Q1.ParamByName['xData']:=Date;
               Q1.ParamByName['xCriteri']:=xCriteri;
               Q1.ParamByName['xStringaSQL']:='';
               if xDaDove=1 then
                    Q1.ParamByName['xResultNum']:=DataRicerche.QCandRic.RecordCount
               else Q1.ParamByName['xResultNum']:=DataRicerche.QTLCandidati.RecordCount;
               Q1.ExecSQL;

               DB.CommitTrans;
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: operazione non effettuata',mtError, [mbOK],0);
          end;
     end;
end;

procedure TSelPersForm.modificaaziendaattualecandidato1Click(Sender: TObject);
var xID: integer;
begin
     SelClienteForm:=TSelClienteForm.create(self);
     SelClienteForm.ShowModal;
     if SelClienteForm.ModalResult=mrOK then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Qtemp.Close;
                    QTemp.SQL.text:='select count(*) Tot from EsperienzeLavorative '+
                         ' where IDAnagrafica=:xIDAnagrafica: and Attuale=1';
                    QTemp.ParamByName['xIDAnagrafica']:=DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger;
                    Qtemp.Open;
                    if Qtemp.FieldByName('Tot').asInteger=0 then
                         Q1.SQL.text:='insert into EsperienzeLavorative (IDAnagrafica,IDAzienda,IDSettore,Attuale) '+
                              ' values (:xIDAnagrafica:,:xIDAzienda:,:xIDSettore:,1)'
                    else
                         Q1.SQL.text:='update EsperienzeLavorative set IDAzienda=:xIDAzienda:,IDSettore=:xIDSettore: '+
                              ' where IDAnagrafica=:xIDAnagrafica: and Attuale=1';
                    Qtemp.Close;
                    QTemp.SQL.text:='select IDAttivita from EBC_Clienti '+
                         ' where ID='+SelClienteForm.TClientiID.asString;
                    Qtemp.Open;
                    Q1.ParamByName['xIDAnagrafica']:=DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger;
                    Q1.ParamByName['xIDAzienda']:=SelClienteForm.TClienti.FieldByName('ID').AsInteger;
                    Q1.ParamByName['xIDSettore']:=QTemp.FieldByName('IDAttivita').asInteger;
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    xID:=DataRicerche.QCandRic.FieldByName('ID').AsInteger;
                    DataRicerche.QCandRic.Close;
                    DataRicerche.QCandRic.Open;
                    DataRicerche.QCandRic.Locate('ID',xID, []);
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: operazione non effettuata',mtError, [mbOK],0);
               end;
          end;
     end;
     SelClienteForm.Free;
end;

procedure TSelPersForm.modificanumeroditelefonodufficio1Click(Sender: TObject);
var xTel: string;
     xID: integer;
begin
     xTel:=DataRicerche.QCandRictelUfficio.Value;
     if InputQuery('Modifica n� tel.ufficio','nuovo numero:',xTel) then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.SQL.text:='update Anagrafica set TelUfficio=:xTelUfficio: '+
                         ' where ID=:xID:';
                    Q1.ParamByName['xTelUfficio']:=xTel;
                    Q1.ParamByName['xID']:=DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger;
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    xID:=DataRicerche.QCandRic.FieldByName('ID').AsInteger;
                    DataRicerche.QCandRic.Close;
                    DataRicerche.QCandRic.Open;
                    DataRicerche.QCandRic.Locate('ID',xID, []);
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: operazione non effettuata',mtError, [mbOK],0);
               end;
          end;
     end;
end;

procedure TSelPersForm.modificanumeroditelefonocellulare1Click(Sender: TObject);
var xTel: string;
     xID: integer;
begin
     xTel:=DataRicerche.QCandRicCellulare.Value;
     if InputQuery('Modifica n� cellulare','nuovo numero:',xTel) then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.SQL.text:='update Anagrafica set Cellulare=:xCellulare: '+
                         ' where ID=:xID:';
                    Q1.ParamByName['xCellulare']:=xTel;
                    Q1.ParamByName['xID']:=DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger;
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    xID:=DataRicerche.QCandRic.FieldByName('ID').AsInteger;
                    DataRicerche.QCandRic.Close;
                    DataRicerche.QCandRic.Open;
                    DataRicerche.QCandRic.Locate('ID',xID, []);
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: operazione non effettuata',mtError, [mbOK],0);
               end;
          end;
     end;
end;

procedure TSelPersForm.BAnagFileNewClick(Sender: TObject);
begin
     RicFileForm:=TRicFileForm.create(self);
     RicFileForm.FEFile.InitialDir:=GetDocPath;
     RicFileForm.ShowModal;
     if RicFileForm.ModalResult=mrOK then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text:='insert into EBC_RicercheFile (IDRicerca,IDTipo,Descrizione,NomeFile,DataCreazione) '+
                         'values (:xIDRicerca:,:xIDTipo:,:xDescrizione:,:xNomeFile:,:xDataCreazione:)';
                    Q1.ParamByName['xIDRicerca']:=DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
                    Q1.ParamByName['xIDTipo']:=0;
                    Q1.ParamByName['xDescrizione']:=RicFileForm.EDesc.text;
                    Q1.ParamByName['xNomeFile']:=RicFileForm.FEFile.filename;
                    Q1.ParamByName['xDataCreazione']:=FileDateToDateTime(FileAge(RicFileForm.FEFile.filename));
                    Q1.ExecSQL;
                    DB.CommitTrans;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               end;
          end;
          QRicFile.Close;
          QRicFile.Open;
     end;
     RicFileForm.Free;
end;

procedure TSelPersForm.BAnagFileModClick(Sender: TObject);
begin
     RicFileForm:=TRicFileForm.create(self);
     RicFileForm.EDesc.text:=QRicFile.FieldByName('Descrizione').AsString;
     RicFileForm.FEFile.FileName:=QRicFile.FieldByName('NomeFile').AsString;
     RicFileForm.ShowModal;
     if RicFileForm.ModalResult=mrOK then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text:='update EBC_RicercheFile set IDTipo=:xIDTipo:,Descrizione=:xDescrizione:,NomeFile=:xNomeFile:,DataCreazione=:xDataCreazione: '+
                         'where ID='+QRicFileID.asString;
                    Q1.ParamByName['xIDTipo']:=0;
                    Q1.ParamByName['xDescrizione']:=RicFileForm.EDesc.text;
                    Q1.ParamByName['xNomeFile']:=RicFileForm.FEFile.filename;
                    Q1.ParamByName['xDataCreazione']:=FileDateToDateTime(FileAge(RicFileForm.FEFile.filename));
                    Q1.ExecSQL;
                    DB.CommitTrans;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               end;
          end;
          QRicFile.Close;
          QRicFile.Open;
     end;
     RicFileForm.Free;
end;

procedure TSelPersForm.BAnagFileDelClick(Sender: TObject);
begin
     if MessageDlg('Sei sicuro di voler eliminare l''associazione ?',mtWarning, [mbNo,mbYes],0)=mrNo then exit;
     if MessageDlg('Vuoi eliminare il file fisico ?',mtWarning, [mbNo,mbYes],0)=mrYes then begin
          DeleteFile(QRicFile.FieldByName('NomeFile').AsString);
     end;
     with Data do begin
          DB.BeginTrans;
          try
               Q1.Close;
               Q1.SQL.text:='delete from EBC_RicercheFile where ID='+QRicFileID.AsString;
               Q1.ExecSQL;
               DB.CommitTrans;
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
          end;
     end;
     QRicFile.Close;
     QRicFile.Open;
end;

procedure TSelPersForm.BFileCandOpenClick(Sender: TObject);
var xFileName: string;
begin
     xFileName:=QRicFileFieldByName('NomeFile').AsString;
     ShellExecute(0,'Open',pchar(xFileName),'','',SW_SHOW);
end;

procedure TSelPersForm.CBOpzioneCand2Click(Sender: TObject);
begin
     DataRicerche.QTLCandidati.Close;
     DataRicerche.QTLCandidati.SQL.text:='select EBC_CandidatiRicerche.ID, Anagrafica.ID IDAnagrafica,CVNumero,Cognome,Nome, Mansioni.Descrizione RuoloAttuale, '+
          'Anagrafica.IDStato,Cellulare,TelUfficio,DataNascita '+
          'from EBC_CandidatiRicerche, Anagrafica, EsperienzeLavorative, Mansioni '+
          'where EBC_CandidatiRicerche.IDAnagrafica = Anagrafica.ID '+
          'and Anagrafica.ID=EsperienzeLavorative.IDAnagrafica '+
          'and EsperienzeLavorative.IDMansione *= Mansioni.ID '+
          'and EBC_CandidatiRicerche.IDTargetList=:ID '+
          'and EsperienzeLavorative.IDAzienda=:IDCliente ';
     if CBOpzioneCand2.Checked then
          DataRicerche.QTLCandidati.SQL.add('and Escluso=0');
     DataRicerche.QTLCandidati.Open;
end;

procedure TSelPersForm.DBGAnnunciCandCustomDrawCell(Sender: TObject;
     ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
     AColumn: TdxTreeListColumn; ASelected,AFocused,ANewItemRow: Boolean;
     var AText: string; var AColor: TColor; AFont: TFont;
     var AAlignment: TAlignment; var ADone: Boolean);
begin
     if (AColumn=DBGAnnunciCandCVNumero)and(CBColoriCandAnnunci.Checked) then begin
          if DataRicerche.QCandRic.Locate('IDAnagrafica',ANode.Values[DBGAnnunciCandIDAnagrafica.index], []) then
               Afont.Color:=clRed
          else Afont.Color:=clBlack;
     end else Afont.Color:=clBlack;
end;

procedure TSelPersForm.BTLDelSoggClick(Sender: TObject);
var xTabFiglie: string;
     xCognome,xNome,xCVNumero,xNatoA,xNatoIl,xCVInseritoIndata,xCVDataAgg: string;
begin
     if not MainForm.CheckProfile('001') then Exit;
     if DataRicerche.QTLCandidati.IsEmpty then exit
     else begin
          if MessageDlg('ATTENZIONE: la procedura � IRREVERSIBILE e DEFINITIVA !!'+chr(13)+
               'E'' necessario verificare il rispetto della normativa sulla Privacy e sulla'+chr(13)+
               'conservazione dei relativi dati.'+chr(13)+chr(13)+
               'SEI DAVVERO SICURO DI VOLER PROCEDERE ?',mtWarning,
               [mbNo,mbYes],0)=mrNo then exit;

          if MessageDlg('Sei proprio sicuro di voler eliminare il soggetto selezionato ?',mtWarning,
               [mbNo,mbYes],0)=mrYes then begin

               xTabFiglie:='';
               Data.Q1.Close;
               Data.Q1.SQL.text:='select count(*) TOT from AnagCaratteristiche where IDAnagrafica='+DataRicerche.QTLCandidatiIDAnagrafica.asString;
               Data.Q1.Open;
               if Data.Q1.fieldByName('TOT').asInteger>0 then xTabFiglie:=xTabFiglie+'- Caratteristiche'+chr(13);
               Data.Q1.Close;
               Data.Q1.SQL.text:='select count(*) TOT from AnagFile where IDAnagrafica='+DataRicerche.QTLCandidatiIDAnagrafica.asString;
               Data.Q1.Open;
               if Data.Q1.fieldByName('TOT').asInteger>0 then xTabFiglie:=xTabFiglie+'- File collegati al soggetto'+chr(13);
               Data.Q1.Close;
               Data.Q1.SQL.text:='select count(*) TOT from AnagIncompClienti where IDAnagrafica='+DataRicerche.QTLCandidatiIDAnagrafica.asString;
               Data.Q1.Open;
               if Data.Q1.fieldByName('TOT').asInteger>0 then xTabFiglie:=xTabFiglie+'- Incompatibilit� con clienti'+chr(13);
               Data.Q1.Close;
               Data.Q1.SQL.text:='select count(*) TOT from AnagMansioni where IDAnagrafica='+DataRicerche.QTLCandidatiIDAnagrafica.asString;
               Data.Q1.Open;
               if Data.Q1.fieldByName('TOT').asInteger>0 then xTabFiglie:=xTabFiglie+'- Ruoli/mansioni'+chr(13);
               Data.Q1.Close;
               Data.Q1.SQL.text:='select count(*) TOT from AnagValutaz where IDAnagrafica='+DataRicerche.QTLCandidatiIDAnagrafica.asString;
               Data.Q1.Open;
               if Data.Q1.fieldByName('TOT').asInteger>0 then xTabFiglie:=xTabFiglie+'- Valutazione'+chr(13);
               Data.Q1.Close;
               Data.Q1.SQL.text:='select count(*) TOT from CompetenzeAnagrafica where IDAnagrafica='+DataRicerche.QTLCandidatiIDAnagrafica.asString;
               Data.Q1.Open;
               if Data.Q1.fieldByName('TOT').asInteger>0 then xTabFiglie:=xTabFiglie+'- Competenze'+chr(13);
               Data.Q1.Close;
               Data.Q1.SQL.text:='select count(*) TOT from EBC_CandidatiRicerche where IDAnagrafica='+DataRicerche.QTLCandidatiIDAnagrafica.asString;
               Data.Q1.Open;
               if Data.Q1.fieldByName('TOT').asInteger>0 then xTabFiglie:=xTabFiglie+'- Commesse/ricerche'+chr(13);
               Data.Q1.Close;
               Data.Q1.SQL.text:='select count(*) TOT from EBC_ContattiCandidati where IDAnagrafica='+DataRicerche.QTLCandidatiIDAnagrafica.asString;
               Data.Q1.Open;
               if Data.Q1.fieldByName('TOT').asInteger>0 then xTabFiglie:=xTabFiglie+'- Contatti relativi a commesse'+chr(13);
               Data.Q1.Close;
               Data.Q1.SQL.text:='select count(*) TOT from EsperienzeLavorative where IDAnagrafica='+DataRicerche.QTLCandidatiIDAnagrafica.asString;
               Data.Q1.Open;
               if Data.Q1.fieldByName('TOT').asInteger>0 then xTabFiglie:=xTabFiglie+'- Esperienze lavorative'+chr(13);
               Data.Q1.Close;
               Data.Q1.SQL.text:='select count(*) TOT from LingueConosciute where IDAnagrafica='+DataRicerche.QTLCandidatiIDAnagrafica.asString;
               Data.Q1.Open;
               if Data.Q1.fieldByName('TOT').asInteger>0 then xTabFiglie:=xTabFiglie+'- Lingue conosciute'+chr(13);
               Data.Q1.Close;
               Data.Q1.SQL.text:='select count(*) TOT from Organigramma where IDDipendente='+DataRicerche.QTLCandidatiIDAnagrafica.asString;
               Data.Q1.Open;
               if Data.Q1.fieldByName('TOT').asInteger>0 then xTabFiglie:=xTabFiglie+'- Organigramma'+chr(13);
               Data.Q1.Close;
               Data.Q1.SQL.text:='select count(*) TOT from Storico where IDAnagrafica='+DataRicerche.QTLCandidatiIDAnagrafica.asString;
               Data.Q1.Open;
               if Data.Q1.fieldByName('TOT').asInteger>0 then xTabFiglie:=xTabFiglie+'- Storico'+chr(13);
               Data.Q1.Close;
               Data.Q1.SQL.text:='select count(*) TOT from TitoliStudio where IDAnagrafica='+DataRicerche.QTLCandidatiIDAnagrafica.asString;
               Data.Q1.Open;
               if Data.Q1.fieldByName('TOT').asInteger>0 then xTabFiglie:=xTabFiglie+'- Titoli di studio'+chr(13);

               if xTabFiglie<>'' then begin
                    if MessageDlg('ATTENZIONE:  risultano collegati i seguenti dati:'+chr(13)+
                         xTabFiglie+'SEI SICURO DI VOLER PROSEGUIRE ?',mtWarning, [mbYes,mbNo],0)=mrNo then
                         exit;
               end;

               if MessageDlg('Verranno eliminati TUTTI i dati di TUTTE le tabelle collegate'+chr(13)+
                    'IMPORTANTE:  non sar� pi� possibile recuperarli - CONTINUARE ?',mtWarning,
                    [mbNo,mbYes],0)=mrYes then begin
                    Data.DB.BeginTrans;
                    try
                         // eliminare tabelle collegate da CONSTRAINT di integrit� referenziale
                         Data.Q1.Close;
                         Data.Q1.SQL.Clear;
                         Data.Q1.SQL.Add('delete from AnagAltreInfo where IDAnagrafica='+DataRicerche.QTLCandidatiIDAnagrafica.asString);
                         Data.Q1.ExecSQL;
                         Data.Q1.SQL.Clear;
                         Data.Q1.SQL.Add('delete from AnagCaratteristiche where IDAnagrafica='+DataRicerche.QTLCandidatiIDAnagrafica.asString);
                         Data.Q1.ExecSQL;
                         Data.Q1.SQL.Clear;
                         Data.Q1.SQL.Add('delete from AnagFile where IDAnagrafica='+DataRicerche.QTLCandidatiIDAnagrafica.asString);
                         Data.Q1.ExecSQL;
                         Data.Q1.SQL.Clear;
                         Data.Q1.SQL.Add('delete from AnagIncompClienti where IDAnagrafica='+DataRicerche.QTLCandidatiIDAnagrafica.asString);
                         Data.Q1.ExecSQL;
                         Data.Q1.SQL.Clear;
                         Data.Q1.SQL.Add('delete from AnagMansioni where IDAnagrafica='+DataRicerche.QTLCandidatiIDAnagrafica.asString);
                         Data.Q1.ExecSQL;
                         Data.Q1.SQL.Clear;
                         Data.Q1.SQL.Add('delete from AnagValutaz where IDAnagrafica='+DataRicerche.QTLCandidatiIDAnagrafica.asString);
                         Data.Q1.ExecSQL;
                         Data.Q1.SQL.Clear;
                         Data.Q1.SQL.Add('delete from CompetenzeAnagrafica where IDAnagrafica='+DataRicerche.QTLCandidatiIDAnagrafica.asString);
                         Data.Q1.ExecSQL;
                         Data.Q1.SQL.Clear;
                         Data.Q1.SQL.Add('delete from EBC_CandidatiRicerche where IDAnagrafica='+DataRicerche.QTLCandidatiIDAnagrafica.asString);
                         Data.Q1.ExecSQL;
                         Data.Q1.SQL.Clear;
                         Data.Q1.SQL.Add('delete from EBC_ContattiCandidati where IDAnagrafica='+DataRicerche.QTLCandidatiIDAnagrafica.asString);
                         Data.Q1.ExecSQL;
                         Data.Q1.SQL.Clear;
                         Data.Q1.SQL.Add('delete from EsperienzeLavorative where IDAnagrafica='+DataRicerche.QTLCandidatiIDAnagrafica.asString);
                         Data.Q1.ExecSQL;
                         Data.Q1.SQL.Clear;
                         Data.Q1.SQL.Add('delete from LingueConosciute where IDAnagrafica='+DataRicerche.QTLCandidatiIDAnagrafica.asString);
                         Data.Q1.ExecSQL;
                         Data.Q1.SQL.Clear;
                         Data.Q1.SQL.Add('delete from Organigramma where IDDipendente='+DataRicerche.QTLCandidatiIDAnagrafica.asString);
                         Data.Q1.ExecSQL;
                         Data.Q1.SQL.Clear;
                         Data.Q1.SQL.Add('delete from Storico where IDAnagrafica='+DataRicerche.QTLCandidatiIDAnagrafica.asString);
                         Data.Q1.ExecSQL;
                         Data.Q1.SQL.Clear;
                         Data.Q1.SQL.Add('delete from TitoliStudio where IDAnagrafica='+DataRicerche.QTLCandidatiIDAnagrafica.asString);
                         Data.Q1.ExecSQL;
                         Data.Q1.SQL.Clear;
                         Data.Q1.SQL.Add('delete from Anagrafica where ID='+DataRicerche.QTLCandidatiIDAnagrafica.asString);
                         Data.Q1.ExecSQL;

                         // log cancellazione
                         xCognome:=DataRicerche.QTLCandidatiCognome.Value;
                         xNome:=DataRicerche.QTLCandidatiNome.Value;
                         xCVNumero:=DataRicerche.QTLCandidatiCVNumero.AsString;
                         xNatoIl:=DataRicerche.QTLCandidatiDataNascita.AsString;
                         Log_Operation(MainForm.xIDUtenteAttuale,'Anagrafica',DataRicerche.QTLCandidati.FieldByName('IDAnagrafica').AsInteger,'D',xCVNumero+' '+xCognome+' '+xNome+', nato a '+xNatoIl);

                         Data.DB.CommitTrans;
                         DataRicerche.QTLCandidati.Close;
                         DataRicerche.QTLCandidati.Open;
                    except
                         Data.DB.RollbackTrans;
                         MessageDlg('Errore nel database: cancellazione non eseguita',mtError, [mbOK],0);
                         raise;
                    end;
               end;
          end;
     end;
end;

procedure TSelPersForm.CBColoriCandAnnunciClick(Sender: TObject);
begin
     DataRicerche.QCandRic.Close;
     DataRicerche.QCandRic.Open;
end;

procedure TSelPersForm.dxDBGrid3ChangeColumn(Sender: TObject;
     Node: TdxTreeListNode; Column: Integer);
begin
     xColonnaSel:=Column;
end;

procedure TSelPersForm.dxDBGrid3ContextPopup(Sender: TObject;
     MousePos: TPoint; var Handled: Boolean);
begin
     if dxDBGrid3.columns[xColonnaSel+1].Caption='telefono' then
          modificanumeroditelefono1.Enabled:=True
     else modificanumeroditelefono1.Enabled:=False;
end;

procedure TSelPersForm.BTabCausaliClick(Sender: TObject);
begin
     OpenTab('EBC_RicercheCausali',['ID','Causale'],['Causale']);
     DataRicerche.QCausaliLK.Close;
     DataRicerche.QCausaliLK.Open;
end;

//[ALBERTO 20020902]FINE

end.

