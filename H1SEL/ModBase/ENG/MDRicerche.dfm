�
 TDATARICERCHE 0{�  TPF0TDataRicercheDataRicercheOldCreateOrder	OnCreateDataRicercheCreateLeft� Top
Height�WidthW TDataSourceDsRicerchePendDataSetTRicerchePendLeft Top  TDataSource
DsQCandRicDataSetQCandRicLeft� Top  TDataSourceDsQCandTutteDataSet
QCandTutteLeftTop}  TDataSourceDsAnagColloquiDataSetTAnagColloquiLeftXTop~  TDataSourceDsQRicAttiveDataSet
QRicAttiveLeft�Top  TDataSourceDsStoricoRicDataSetTStoricoRicLeft� Top  TDataSourceDsQCandAziendaDataSetQCandAziendaLeft� Top�  TDataSource	DsQOrgNewDataSetQOrgNewOnStateChangeDsQOrgNewStateChangeLeft Top�  TDataSourceDsQTipoCommesseDataSetQTipoCommesseLeftgTop  TDataSourceDsQTipiCommDataSet	QTipiCommLeftTop}  TDataSourceDsQAnnunciCandDataSetQAnnunciCandLeft�Top�   TDataSource	DsAziendeDataSetQAziendeLeftTop�  TDataSourceDsQRicLineeQueryDataSetQRicLineeQueryLeftBTop  TDataSourceDsQAnagIncompClientiDataSetQAnagIncompClientiLeft� Top{  TDataSourceDsQAnagNoteRicDataSetQAnagNoteRicLefthTop  TDataSourceDsQRicTimingDataSet
QRicTimingLeft0Top  TDataSourcedsQOrgNodoRuoloDataSetQOrgNodoRuoloLeft�Top�  TDataSourceDsQRicUtentiDataSet
QRicUtentiLeft�Topv  TDataSourceDsQRicTargetListDataSetQRicTargetListLeft�Top�  TDataSourceDsQTLCandidatiDataSetQTLCandidatiLefthTop�  TDataSourceDsQRicTimeSheetDataSetQRicTimeSheetOnStateChangeDsQRicTimeSheetStateChangeLeft0Top�   TDataSource
DsQUsersLKDataSetQUsersLKLeft�TopN  TDataSourceDsQGruppiProfLKDataSetQGruppiProfLKLeft(TopR  TDataSourceDsQTimeRepDettDataSetQTimeRepDettOnStateChangeDsQTimeRepDettStateChangeLeftpTop�   TDataSourceDsQAnagTitoloMansAttDataSetQAnagTitoloMansAttLeft\Top}  TADOLinkedQueryQTipoCommesse
ConnectionData.DB
Parameters SQL.Stringsselect * from EBC_TipiCommesse 	UseFilterLefthTop@  TADOLinkedQueryTStoricoRic
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Strings	select * /from EBC_StoricoRic,EBC_StatiRic, EBC_EventiRic/where EBC_StoricoRic.IDStatoRic=EBC_StatiRic.ID2and EBC_StoricoRic.IDEventoRic *= EBC_EventiRic.ID MasterDataSetTRicerchePendLinkedMasterFieldIDLinkedDetailField	IDRicerca	UseFilterLeft� Top@  TADOLinkedQueryQRicLineeQuery
ConnectionData.DB
Parameters SQL.Stringsselect * from RicLineeQuery MasterDataSetTRicerchePendLinkedMasterFieldIDLinkedDetailField	IDRicerca	UseFilterLeft@Top@  TADOLinkedQueryQTotCandRic
ConnectionData.DB
Parameters SQL.Strings"select IDRicerca,count(ID) TotCandfrom EBC_CandidatiRicerche group by IDRicerca 	UseFilterLeft�TopH  TADOLinkedQuery
QRicAttive
ConnectionData.DB
CursorTypectStatic
BeforeOpenQRicAttive_OLDBeforeOpen	AfterOpenQRicAttive_OLDAfterOpen
AfterCloseQRicAttive_OLDAfterClose
Parameters SQL.StringsLselect distinct EBC_Ricerche.ID,Progressivo,EBC_Clienti.Descrizione Cliente,BDataInizio,NumRicercati,TitoloCliente, Mansioni.Descrizione Ruolo,QUsers.Nominativo Selezionatore,EBC_Ricerche.Stato,Conclusa,IDStatoRic, DataFIne, CEBC_StatiRic.StatoRic,EBC_Ricerche.Tipo, DataPrevChiusura,IDOffertaMfrom EBC_Ricerche,Mansioni,Users,EBC_Clienti,EBC_StatiRic,EBC_RicercheUtenti /where EBC_Ricerche.IDMansione *= Mansioni.ID   3and EBC_Ricerche.ID = EBC_RicercheUtenti.IDRicerca *and EBC_Ricerche.IDCliente=EBC_CLienti.ID #and EBC_Ricerche.IDUtente=Users.ID +and EBC_Ricerche.IDStatoRic=EBC_StatiRic.ID!and EBC_RicercheUtenti.IDUtente=1and EBC_Ricerche.IDStatoRic=1order by Progressivo desc 	UseFilterLeft�Top@ TAutoIncFieldQRicAttiveID	FieldNameIDReadOnly	  TStringFieldQRicAttiveProgressivo	FieldNameProgressivoSize  TStringFieldQRicAttiveCliente	FieldNameClienteSize2  TDateTimeFieldQRicAttiveDataInizio	FieldName
DataInizio  TSmallintFieldQRicAttiveNumRicercati	FieldNameNumRicercati  TStringFieldQRicAttiveTitoloCliente	FieldNameTitoloClienteSize2  TStringFieldQRicAttiveRuolo	FieldNameRuoloSize(  TStringFieldQRicAttiveSelezionatore	FieldNameSelezionatoreSize  TStringFieldQRicAttiveStato	FieldNameStatoSize  TBooleanFieldQRicAttiveConclusa	FieldNameConclusa  TIntegerFieldQRicAttiveIDStatoRic	FieldName
IDStatoRic  TStringFieldQRicAttiveStatoRic	FieldNameStatoRic  TStringFieldQRicAttiveTipo	FieldNameTipoSize  TDateTimeFieldQRicAttiveDataPrevChiusura	FieldNameDataPrevChiusura  TIntegerFieldQRicAttiveIDOfferta	FieldName	IDOfferta  TStringFieldQRicAttiveRifOfferta	FieldKindfkLookup	FieldName
RifOffertaLookupDataSet
QOfferteLKLookupKeyFieldsIDLookupResultFieldRif	KeyFields	IDOffertaSize
Lookup	  TFloatFieldQRicAttiveTotCompdaFatt	FieldKindfkLookup	FieldNameTotCompdaFattLookupDataSetQTotCompDaFattLKLookupKeyFields	IDRicercaLookupResultField	ImpDaFatt	KeyFieldsIDLookup	  TFloatFieldQRicAttiveTotCompFatturato	FieldKindfkLookup	FieldNameTotCompFatturatoLookupDataSetQTotCompFattLKLookupKeyFields	IDRicercaLookupResultFieldImpFatturato	KeyFieldsIDLookup	  TDateTimeFieldQRicAttiveDataFIne	FieldNameDataFIne  TFloatFieldQRicAttiveRimbSpese	FieldKindfkLookup	FieldName	RimbSpeseLookupDataSetqTotRimbSpeseLKLookupKeyFields	IDRicercaLookupResultField	RimbSpese	KeyFieldsIDLookup	  TStringFieldQRicAttiveSupporto	FieldKindfkLookup	FieldNameSupportoLookupDataSetqCollaboratoriRicLKLookupKeyFields	IDricercaLookupResultField
Nominativo	KeyFieldsIDSizeLookup	   TADOLinkedQuery
QRicTiming
ConnectionData.DB
Parameters MasterDataSetTRicerchePendLinkedMasterFieldIDLinkedDetailField	IDRicerca	UseFilterLeft0TopH  TADOLinkedQueryQAnagNoteRic
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Strings select EBC_CandidatiRicerche.ID,7          EBC_CandidatiRicerche.Note2 Note,Progressivo,'       EBC_Clienti.Descrizione Cliente,       IDAnagrafica 3from EBC_CandidatiRicerche,EBC_Ricerche,EBC_Clienti5where EBC_CandidatiRicerche.IDRicerca=EBC_Ricerche.ID+  and EBC_Ricerche.IDCliente=EBC_Clienti.ID MasterDataSetData.TAnagraficaLinkedMasterFieldIDLinkedDetailFieldIDAnagrafica	UseFilterLefthTopH  TADOLinkedQuery	QTipiComm
ConnectionData.DB
Parameters SQL.Stringsselect * from EBC_TipiCommesse 	UseFilterLeftTop�   TADOLinkedQueryQAnagTitoloMansAtt
ConnectionData.DB
CursorTypectStatic
ParametersNameID
AttributespaSigned
paNullable DataType	ftInteger	Precision
SizeValue   SQL.Stringsselect TitoloMansionefrom EsperienzeLavorativewhere IDAnagrafica=:IDand Attuale=1 	UseFilterLeftXTop�   TADOLinkedQueryQLastEspLav
ConnectionData.DB
CursorTypectStatic
BeforeOpenQLastEspLav_OLDBeforeOpen
AfterCloseQLastEspLav_OLDAfterClose
Parameters SQL.Stringsselect IDAnagrafica,ID MaxIDfrom EsperienzeLavorative where Attuale=1 	UseFilterLeft� Top�  TIntegerFieldQLastEspLavIDAnagrafica	FieldNameIDAnagrafica  TAutoIncFieldQLastEspLavMaxID	FieldNameMaxIDReadOnly	  TStringFieldQLastEspLavAzienda	FieldKindfkLookup	FieldNameAziendaLookupDataSet	QEspLavLKLookupKeyFieldsIDLookupResultFieldAzienda	KeyFieldsMaxIDSize2Lookup	  TStringFieldQLastEspLavTelefono	FieldKindfkLookup	FieldNameTelefonoLookupDataSet	QEspLavLKLookupKeyFieldsIDLookupResultFieldTelefono	KeyFieldsMaxIDSizeLookup	   TADOLinkedQueryQAnagIncompClienti
ConnectionData.DB
CursorTypectStatic
DataSourceData.DsAnagrafica
ParametersNameID
AttributespaSigned DataType	ftInteger	Precision
SizeValue   SQL.Strings:select AnagIncompClienti.*,EBC_Clienti.Descrizione Cliente"from AnagIncompClienti,EBC_Clienti0where AnagIncompClienti.IDCliente=EBC_Clienti.IDand IDAnagrafica=:ID 	UseFilterLeft� Top�   TADOLinkedQuery
QCandTutte
ConnectionData.DB
Parameters SQL.Strings@select EBC_CandidatiRicerche.ID,EBC_CandidatiRicerche.IDRicerca,K          EBC_CandidatiRicerche.IDAnagrafica,EBC_CandidatiRicerche.Escluso,K          EBC_CandidatiRicerche.DataImpegno,EBC_CandidatiRicerche.Codstato,&          EBC_CandidatiRicerche.Stato,,          Anagrafica.Cognome,Anagrafica.Nome2from EBC_CandidatiRicerche,Anagrafica,EBC_Ricerche6where EBC_CandidatiRicerche.IDAnagrafica=Anagrafica.ID7    and EBC_CandidatiRicerche.IDRicerca=EBC_Ricerche.ID    and EBC_Ricerche.Conclusa=05    and EBC_CandidatiRicerche.DataImpegno is not null*order by EBC_CandidatiRicerche.DataImpegno 	UseFilterLeft Top�   TADOLinkedQuery	QEspLavLK
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect Esperienzelavorative.ID,=       EBC_Clienti.Descrizione Azienda, EBC_Clienti.Telefono,"       Mansioni.Descrizione Ruolo /from Esperienzelavorative,EBC_Clienti,Mansioni 6where Esperienzelavorative.IDAzienda *= EBC_Clienti.ID4  and Esperienzelavorative.IDMansione *= Mansioni.ID  	UseFilterLeftXTop�  TAutoIncFieldQEspLavLKID	FieldNameIDReadOnly	  TStringFieldQEspLavLKAzienda	FieldNameAziendaSize2  TStringFieldQEspLavLKTelefono	FieldNameTelefonoSize  TStringFieldQEspLavLKRuolo	FieldNameRuoloSize(   TADOLinkedQuery
QRicUtenti
ConnectionData.DB
Parameters MasterDataSetTRicerchePendLinkedMasterFieldIDLinkedDetailField	IDRicercaOriginalSQL.Strings$select EBC_RicercheUtenti.IDRicerca,)          EBC_RicercheUtenti.IDRuoloRic, #          Users.Nominativo Utente, &          EBC_RicUtentiRuoli.RuoloRic,&          EBC_RicercheUtenti.IDUtente,          EBC_RicercheUtenti.ID0from EBC_RicercheUtenti,Users,EBC_RicUtentiRuoli*where EBC_RicercheUtenti.IDUtente=Users.ID9  and EBC_RicercheUtenti.IDRuoloRic=EBC_RicUtentiRuoli.ID,and EBC_RicercheUtenti.IDUtente<>:xIDUtente:    	UseFilterLeft�Top�   TADOLinkedQueryQTemp
ConnectionData.DB
Parameters 	UseFilterLeft�Top  TADOLinkedQueryQTotCompDaFattLKActive	
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Strings'select IDRicerca,sum(Importo) ImpDaFattfrom EBC_CompensiRicerche*where (IDFattura is null) or (IDFattura=0)group by IDRicerca 	UseFilterLeft
Topx TIntegerFieldQTotCompDaFattLKIDRicerca	FieldName	IDRicerca  TFloatFieldQTotCompDaFattLKImpDaFatt	FieldName	ImpDaFattReadOnly	   TADOLinkedQuery
QCausaliLK
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Strings!select * from EBC_RicercheCausaliorder by Causale 	UseFilterLeft�Top8  TADOLinkedQueryQAziende
ConnectionData.DB
Parameters SQL.Strings+select EBC_Clienti.* ,EBC_attivita.Attivitafrom EBC_Clienti,EBC_attivita,where EBC_Clienti.IDAttivita=EBC_attivita.IDand EBC_Clienti.ID=12 	UseFilterLeftTop� TAutoIncField
QAziendeID	FieldNameIDReadOnly	  TStringFieldQAziendeDescrizione	FieldNameDescrizioneSize2  TStringFieldQAziendeStato	FieldNameStatoSize  TStringFieldQAziendeIndirizzo	FieldName	IndirizzoSize(  TStringFieldQAziendeCap	FieldNameCapSize  TStringFieldQAziendeComune	FieldNameComune  TStringFieldQAziendeProvincia	FieldName	ProvinciaSize  TIntegerFieldQAziendeIDAttivita	FieldName
IDAttivita  TStringFieldQAziendeTelefono	FieldNameTelefonoSize  TStringFieldQAziendeFax	FieldNameFaxSize  TStringFieldQAziendePartitaIVA	FieldName
PartitaIVASize  TStringFieldQAziendeCodiceFiscale	FieldNameCodiceFiscaleSize  TStringFieldQAziendeBancaAppoggio	FieldNameBancaAppoggioSizeP  TStringFieldQAziendeSistemaPagamento	FieldNameSistemaPagamentoSize<  
TMemoFieldQAziendeNoteContratto	FieldNameNoteContrattoBlobTypeftMemo  TStringFieldQAziendeResponsabile	FieldNameResponsabileSize(  TDateTimeFieldQAziendeConosciutoInData	FieldNameConosciutoInData  TStringFieldQAziendeIndirizzoLegale	FieldNameIndirizzoLegaleSize(  TStringFieldQAziendeCapLegale	FieldName	CapLegaleSize  TStringFieldQAziendeComuneLegale	FieldNameComuneLegale  TStringFieldQAziendeProvinciaLegale	FieldNameProvinciaLegaleSize  TStringFieldQAziendeCartellaDoc	FieldNameCartellaDocSizeP  TStringFieldQAziendeTipoStrada	FieldName
TipoStradaSize
  TStringFieldQAziendeTipoStradaLegale	FieldNameTipoStradaLegaleSize
  TStringFieldQAziendeNumCivico	FieldName	NumCivicoSize
  TStringFieldQAziendeNumCivicoLegale	FieldNameNumCivicoLegaleSize
  TStringFieldQAziendeNazioneAbb	FieldName
NazioneAbbSize  TStringFieldQAziendeNazioneAbbLegale	FieldNameNazioneAbbLegaleSize  
TMemoFieldQAziendeNote	FieldNameNoteBlobTypeftMemo  TFloatFieldQAziendeFatturato	FieldName	Fatturato  TSmallintFieldQAziendeNumDipendenti	FieldNameNumDipendenti  TBooleanFieldQAziendeBlocco1	FieldNameBlocco1  TStringFieldQAziendeEnteCertificatore	FieldNameEnteCertificatoreSized  TBooleanFieldQAziendeLibPrivacy	FieldName
LibPrivacy  TIntegerFieldQAziendeIdAzienda	FieldName	IdAzienda  TStringFieldQAziendeSitoInternet	FieldNameSitoInternet	FixedChar	Size2  TStringFieldQAziendeDescAttivitaAzienda	FieldNameDescAttivitaAzienda	FixedChar	Size�   TIntegerFieldQAziendeDallAnno	FieldNameDallAnno  TStringFieldQAziendeParentCompany	FieldNameParentCompany	FixedChar	Size2  TStringFieldQAziendeCCIAA	FieldNameCCIAA	FixedChar	Size2  TStringFieldQAziendeAttivita	FieldNameAttivitaSize2   TADOLinkedQueryQGen
ConnectionData.DB
Parameters 	UseFilterLeft� Topx  TADOLinkedQueryQTitoliStudioLK
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Strings#select TitoliStudio.ID, Descrizionefrom TitoliStudio, Diplomi)where TitoliStudio.IDDiploma = Diplomi.ID 	UseFilterLefthTop� TAutoIncFieldQTitoliStudioLKID	FieldNameIDReadOnly	  TStringFieldQTitoliStudioLKDescrizione	FieldNameDescrizioneSize<   TADOLinkedQueryQCandAzienda
ConnectionData.DB
Parameters SQL.Strings<select Anagrafica.ID,Anagrafica.CVNumero,Anagrafica.Cognome,:          Anagrafica.Nome,EsperienzeLavorative.IDMansione,$          Mansioni.Descrizione Ruolo.from EsperienzeLavorative,Anagrafica, Mansioni6where EsperienzeLavorative.IDAnagrafica=Anagrafica.ID 6    and EsperienzeLavorative.IDMansione *= Mansioni.ID<    and EsperienzeLavorative.Attuale=1 order by Cognome,Nome OriginalSQL.Strings<select Anagrafica.ID,Anagrafica.CVNumero,Anagrafica.Cognome,:          Anagrafica.Nome,EsperienzeLavorative.IDMansione,$          Mansioni.Descrizione Ruolo.from EsperienzeLavorative,Anagrafica, Mansioni6where EsperienzeLavorative.IDAnagrafica=Anagrafica.ID 6    and EsperienzeLavorative.IDMansione *= Mansioni.ID+    and EsperienzeLavorative.IDAzienda=:ID:<    and EsperienzeLavorative.Attuale=1 order by Cognome,Nome 	UseFilterLeft� Top�  TADOLinkedQueryQOrgNew_OLD
ConnectionData.DB
CursorTypectStatic
BeforeOpenQOrgNew_OLDBeforeOpen	AfterOpenQOrgNew_OLDAfterOpenBeforeCloseQOrgNew_OLDBeforeClose
AfterCloseQOrgNew_OLDAfterCloseAfterInsertQOrgNew_OLDAfterInsertAfterScrollQOrgNew_OLDAfterScrollOnCalcFieldsQOrgNew_OLDCalcFields
ParametersNamexIDCliente:
AttributespaSigned
paNullable DataType	ftInteger	Precision
SizeValue   SQL.StringsCselect Organigramma.*, Anagrafica.Cognome,Anagrafica.Nome,CVNumero,          Reparti.Reparto %from Organigramma,Anagrafica, Reparti0where Organigramma.IDDipendente *= Anagrafica.ID,    and Organigramma.IDReparto *= Reparti.ID+    and Organigramma.IDCliente=:xIDCliente: OriginalSQL.StringsCselect Organigramma.*, Anagrafica.Cognome,Anagrafica.Nome,CVNumero,          Reparti.Reparto %from Organigramma,Anagrafica, Reparti0where Organigramma.IDDipendente *= Anagrafica.ID,    and Organigramma.IDReparto *= Reparti.ID  	UseFilterLeft Top� TAutoIncFieldQOrgNew_OLDID	FieldNameIDReadOnly	  TIntegerFieldQOrgNew_OLDIDLivello	FieldName	IDLivello  TStringFieldQOrgNew_OLDTipo	FieldNameTipo	FixedChar	Size  TStringFieldQOrgNew_OLDDescrizione	FieldNameDescrizione	FixedChar	Size<  TStringFieldQOrgNew_OLDDescBreve	FieldName	DescBreve	FixedChar	Size
  TDateTimeFieldQOrgNew_OLDDataAgg	FieldNameDataAgg  TStringFieldQOrgNew_OLDTipoSubordine	FieldNameTipoSubordine	FixedChar	Size  TIntegerFieldQOrgNew_OLDNodoPadre	FieldName	NodoPadre  TIntegerFieldQOrgNew_OLDIDMansione	FieldName
IDMansione  TIntegerFieldQOrgNew_OLDIDDipendente	FieldNameIDDipendente  TSmallintFieldQOrgNew_OLDPercentuale	FieldNamePercentuale  TIntegerFieldQOrgNew_OLDCentroCosto	FieldNameCentroCosto  TSmallintFieldQOrgNew_OLDMonteOreRichiesto	FieldNameMonteOreRichiesto  TSmallintFieldQOrgNew_OLDTassoAssenteismo	FieldNameTassoAssenteismo  TBooleanFieldQOrgNew_OLDInterim	FieldNameInterim  TBooleanFieldQOrgNew_OLDInStaff	FieldNameInStaff  TFloatFieldQOrgNew_OLDValutazGen	FieldName
ValutazGen  TFloatFieldQOrgNew_OLDIndiceSost	FieldName
IndiceSost  TFloatFieldQOrgNew_OLDIndiceImp	FieldName	IndiceImp  TDateTimeFieldQOrgNew_OLDUltimoAgg	FieldName	UltimoAgg  TIntegerFieldQOrgNew_OLDPARENT	FieldNamePARENT  TIntegerFieldQOrgNew_OLDWIDTH	FieldNameWIDTH  TIntegerFieldQOrgNew_OLDHEIGHT	FieldNameHEIGHT  TStringFieldQOrgNew_OLDTYPE	FieldNameTYPE	FixedChar	  TIntegerFieldQOrgNew_OLDCOLOR	FieldNameCOLOR  TIntegerFieldQOrgNew_OLDIMAGE	FieldNameIMAGE  TStringFieldQOrgNew_OLDIMAGEALIGN	FieldName
IMAGEALIGN	FixedChar	  TIntegerFieldQOrgNew_OLDORDINE	FieldNameORDINE  TStringFieldQOrgNew_OLDALIGN	FieldNameALIGN	FixedChar	  TStringFieldQOrgNew_OLDCognome	FieldNameCognome	FixedChar	Size  TStringFieldQOrgNew_OLDNome	FieldNameNome	FixedChar	Size  TIntegerFieldQOrgNew_OLDCVNumero	FieldNameCVNumero  TStringFieldQOrgNew_OLDMansione	FieldKindfkLookup	FieldNameMansioneLookupDataSetQMansioniLKLookupKeyFieldsIDLookupResultFieldDescrizione	KeyFields
IDMansioneSize(Lookup	  TStringFieldQOrgNew_OLDDicNodo	FieldKindfkCalculated	FieldNameDicNodoSize!
Calculated	  TIntegerFieldQOrgNew_OLDIDReparto	FieldName	IDReparto  TIntegerFieldQOrgNew_OLDIDCliente	FieldName	IDCliente  TStringFieldQOrgNew_OLDRepartoDisplayWidth2	FieldNameRepartoSize2   TADOLinkedQueryQTRepCausali
ConnectionData.DB
Parameters SQL.Stringsselect * from TimeSheetCausali 	UseFilterLeftHTop�  TADOLinkedQueryQRicTargetList
ConnectionData.DB	AfterOpenQRicTargetList_OLDAfterOpenBeforeCloseQRicTargetListBeforeClose
AfterCloseQRicTargetList_OLDAfterClose	AfterPostQRicTargetList_OLDAfterPostAfterDeleteQRicTargetList_OLDAfterPostAfterScrollQRicTargetListAfterScroll
Parameters MasterDataSetTRicerchePendLinkedMasterFieldIDLinkedDetailField	IDRicercaOriginalSQL.Strings!select EBC_RicercheTargetList.ID,#  EBC_RicercheTargetList.IDRicerca,#  EBC_RicercheTargetList.IDCliente,  EBC_RicercheTargetList.Note,1       Descrizione Azienda, Attivita, IDAttivita,       telefono, Comune,2       EBC_LogEsplorazioni.Data DataUltimaEsploraz7from EBC_RicercheTargetList, EBC_Clienti, EBC_Attivita,     EBC_LogEsplorazioni 7where EBC_RicercheTargetList.IDCliente = EBC_Clienti.ID/  and EBC_Clienti.IDAttivita *= EBC_Attivita.IDC  and EBC_RicercheTargetList.ID *= EBC_LogEsplorazioni.IDTargetListK  and EBC_LogEsplorazioni.Data = (select max(Data) from EBC_LogEsplorazionig                                  where EBC_RicercheTargetList.ID *= EBC_LogEsplorazioni.IDTargetList) order by Descrizione  	UseFilterLeft�Top�  TADOLinkedQueryQOrgNodoRuolo
ConnectionData.DB
CursorTypectStatic
BeforeOpenQOrgNodoRuolo_OLDBeforeOpen	AfterOpenQOrgNodoRuoloAfterOpen
ParametersNamexIDMansione:
AttributespaSigned
paNullable DataType	ftInteger	Precision
SizeValue   SQL.Strings8select Anagrafica.ID,CVNumero,Cognome,Nome,IDProprietaCVfrom AnagMansioni,Anagrafica/where AnagMansioni.IDAnagrafica = Anagrafica.ID*and AnagMansioni.IDMansione =:xIDMansione:'and AnagMansioni.IDMansione is not nullorder by cognome OriginalSQL.Strings8select Anagrafica.ID,CVNumero,Cognome,Nome,IDProprietaCVfrom AnagMansioni,Anagrafica/where AnagMansioni.IDAnagrafica = Anagrafica.ID*and AnagMansioni.IDMansione =:xIDMansione:'and AnagMansioni.IDMansione is not nullorder by cognome  	UseFilterLeft�Top� TAutoIncFieldQOrgNodoRuoloID	FieldNameIDReadOnly	  TIntegerFieldQOrgNodoRuoloCVNumero	FieldNameCVNumero  TStringFieldQOrgNodoRuoloCognome	FieldNameCognomeSize  TStringFieldQOrgNodoRuoloNome	FieldNameNomeSize  TIntegerFieldQOrgNodoRuoloIDProprietaCV	FieldNameIDProprietaCV   TADOLinkedQueryQMansioniLK
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect ID,Descrizionefrom Mansioni  	UseFilterLeft(Top�  TADOLinkedQueryQTLCandidati
ConnectionData.DB
CursorTypectStatic
BeforeOpenQTLCandidatiBeforeOpenOnCalcFieldsQTLCandidati_OLDCalcFields
Parameters SQL.StringsYselect EBC_CandidatiRicerche.ID, Anagrafica.ID IDAnagrafica, DescrizioneMansione,        bCVNumero,Cognome,Nome, Mansioni.Descrizione RuoloAttuale,EBC_CandidatiRicerche.Note2 Note,        GAnagrafica.IDStato,Cellulare,TelUfficio,DataNascita,RecapitiTelefonici Ffrom EBC_CandidatiRicerche, Anagrafica, EsperienzeLavorative,Mansioni 9where EBC_CandidatiRicerche.IDAnagrafica = Anagrafica.ID 4and Anagrafica.ID=EsperienzeLavorative.IDAnagrafica 3and EsperienzeLavorative.IDMansione *= Mansioni.ID *and EBC_CandidatiRicerche.IDTargetList=21 &and EsperienzeLavorative.IDAzienda=234 LinkedMasterFieldIDOriginalSQL.Strings<select EBC_CandidatiRicerche.ID, Anagrafica.ID IDAnagrafica,@       CVNumero,Cognome,Nome, Mansioni.Descrizione RuoloAttuale,;       Anagrafica.IDStato,Cellulare,TelUfficio,DataNascita Ffrom EBC_CandidatiRicerche, Anagrafica, EsperienzeLavorative, Mansioni8where EBC_CandidatiRicerche.IDAnagrafica = Anagrafica.ID3and Anagrafica.ID=EsperienzeLavorative.IDAnagrafica2and EsperienzeLavorative.IDMansione *= Mansioni.ID/and EsperienzeLavorative.IDAzienda=:xIDCliente:and Escluso=0  	UseFilterLefthTop�  TADOLinkedQuery
QOfferteLKActive	
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect ID,Rif from EBC_Offerte 	UseFilterLeft�Top�  TADOLinkedQueryQUsersLK
ConnectionData.DB
BeforeOpenQUsersLKBeforeOpen
Parameters SQL.Strings0select ID,Nominativo,Descrizione,IDGruppoProfessfrom Users  OriginalSQL.Strings0select ID,Nominativo,Descrizione,IDGruppoProfessfrom Users >where not ((DataScadenza is not null and DataScadenza<:xoggi:)5   or (DataRevoca is not null and DataRevoca<:xoggi:)   or DataCreazione is null   or Tipo=0)order by Nominativo  	UseFilterLeft�Topx TAutoIncField
QUsersLKID	FieldNameIDReadOnly	  TStringFieldQUsersLKNominativo	FieldName
NominativoSize  TStringFieldQUsersLKDescrizione	FieldNameDescrizioneSize2  TIntegerFieldQUsersLKIDGruppoProfess	FieldNameIDGruppoProfess   TADOLinkedQueryQGruppiProfLK
ConnectionData.DB
Parameters SQL.Stringsselect * from CG_GruppiProfess 	UseFilterLeft(Topx TAutoIncFieldQGruppiProfLKID	FieldNameIDReadOnly	  TStringField QGruppiProfLKGruppoProfessionale	FieldNameGruppoProfessionaleSize  TFloatFieldQGruppiProfLKRateOrarioPrev	FieldNameRateOrarioPrev  TFloatFieldQGruppiProfLKRateOrarioCons	FieldNameRateOrarioCons  TIntegerFieldQGruppiProfLKIDMansione	FieldName
IDMansione  TFloatField!QGruppiProfLKRateOrarioDirectPrev	FieldNameRateOrarioDirectPrev  TFloatField!QGruppiProfLKRateOrarioDirectCons	FieldNameRateOrarioDirectCons   TADOLinkedTable
TClientiLK
ConnectionData.DB	TableNamedbo.EBC_ClientiLeftPTop  TADOLinkedTableTRicAnagIDX
ConnectionData.DB	TableNamedbo.EBC_CandidatiRicercheLeftTopp TAutoIncFieldTRicAnagIDXID	FieldNameIDReadOnly	  TIntegerFieldTRicAnagIDXIDRicerca	FieldName	IDRicerca  TIntegerFieldTRicAnagIDXIDAnagrafica	FieldNameIDAnagrafica   TADOLinkedTableTRicercheLK
ConnectionData.DB	TableNamedbo.EBC_RicercheLeft� Top  TADOLinkedTableTStoricoRicABS
ConnectionData.DB	TableNamedbo.EBC_StoricoRicLeftTop   TADOLinkedTableTCandRicABS
ConnectionData.DB	TableNamedbo.EBC_CandidatiRicercheLeftHTop(  TADOLinkedTableTAnagColloqui
ConnectionData.DB	TableNamedbo.EBC_ColloquiMasterDataSetData.TAnagraficaLinkedMasterFieldIDLinkedDetailFieldIDAnagraficaLeftxTopp  TADOLinkedTableTRicercheIDX
ConnectionData.DB	TableNamedbo.EBC_RicercheLeftzTop(  TADOLinkedTableTColloquiABS
ConnectionData.DB	TableNamedbo.EBC_CompensiRicercheLeftTop  TADOLinkedTableTContattiABS
ConnectionData.DB	TableNamedbo.EBC_ContattiCandidatiLeft� Top   TADOLinkedQueryQAnnunciCand
ConnectionData.DB
Parameters OriginalSQL.StringsQselect distinct Ann_Annunci.ID IDAnnuncio,Rif,Ann_Testate.Denominazione Testata, a       Ann_Edizioni.NomeEdizione,Ann_AnnEdizData.ID IDAnnEdizData,Visionato,Ann_AnnEdizData.Data,W       CVNumero,Cognome,Nome,CVInseritoIndata,Anagrafica.ID IDAnagrafica,IDProprietaCV,L       EBC_CandidatiRicerche.ID IDCandRic, Ann_anagAnnEdizData.DataPervenuto)from Ann_AnagAnnEdizData,Ann_AnnEdizData,K     Anagrafica,Ann_Edizioni,Ann_Testate,Ann_Annunci, EBC_CandidatiRicerche:where Ann_AnagAnnEdizData.IDAnnEdizData=Ann_AnnEdizData.ID4  and Ann_AnagAnnEdizData.IDAnagrafica=Anagrafica.ID0  and Ann_AnnEdizData.IDEdizione=Ann_Edizioni.ID/  and Ann_AnnEdizData.IDAnnuncio=Ann_Annunci.ID+  and Ann_Edizioni.IDTestata=Ann_Testate.IDL  and Ann_AnagAnnEdizData.IDAnagrafica *= EBC_CandidatiRicerche.IDAnagrafica2  and EBC_CandidatiRicerche.IDRicerca=:xIDRicerca:.and Ann_AnagAnnEdizData.IDRicerca=:xIDRicerca:�group by Ann_Annunci.ID,Rif,Ann_Testate.Denominazione, Ann_Edizioni.NomeEdizione,Ann_AnnEdizData.ID,Visionato,Ann_AnnEdizData.Data,�         CVNumero,Cognome,Nome,CVInseritoIndata,Anagrafica.ID,IDProprietaCV,EBC_CandidatiRicerche.ID, Ann_anagAnnEdizData.DataPervenutoorder by Cognome,Nome 	UseFilterLeft�Topx  TADOLinkedQueryQRicTimeSheet
ConnectionData.DB
BeforeOpenQRicTimeSheet_OLDBeforeOpen
AfterCloseQRicTimeSheet_OLDAfterCloseAfterInsertQRicTimeSheet_OLDAfterInsert
BeforePostQRicTimeSheet_OLDBeforePost	AfterPostQRicTimeSheet_OLDAfterPostBeforeDeleteQRicTimeSheet_OLDBeforeDeleteAfterDeleteQRicTimeSheet_OLDAfterDeleteOnNewRecordQRicTimeSheet_OLDNewRecord
Parameters SQL.Strings#select * from EBC_RicercheTimeSheet 	UseFilterLeft8Topx TAutoIncFieldQRicTimeSheetID	FieldNameIDReadOnly	  TIntegerFieldQRicTimeSheetIDRicerca	FieldName	IDRicerca  TIntegerFieldQRicTimeSheetIDUtente	FieldNameIDUtente  TDateTimeFieldQRicTimeSheetData	FieldNameData  TIntegerFieldQRicTimeSheetIDGruppoProfess	FieldNameIDGruppoProfess  TFloatFieldQRicTimeSheetOreConsuntivo	FieldNameOreConsuntivo  
TMemoFieldQRicTimeSheetNote	FieldNameNoteBlobTypeftMemo  TIntegerFieldQRicTimeSheetOrePreventivo	FieldNameOrePreventivo  TStringFieldQRicTimeSheetUtente	FieldKindfkLookup	FieldNameUtenteLookupDataSetQUsersLKLookupKeyFieldsIDLookupResultField
Nominativo	KeyFieldsIDUtenteSizeLookup	  TStringFieldQRicTimeSheetGruppoProfess	FieldKindfkLookup	FieldNameGruppoProfessLookupDataSetQGruppiProfLKLookupKeyFieldsIDLookupResultFieldGruppoProfessionale	KeyFieldsIDGruppoProfessSizeLookup	   TADOLinkedQueryQTimeRepDett
ConnectionData.DB
CursorTypectStatic
BeforeOpenQTimeRepDett_OLDBeforeOpen
AfterCloseQTimeRepDett_OLDAfterCloseAfterInsertQTimeRepDett_OLDAfterInsert	AfterPostQTimeRepDett_OLDAfterPostAfterDeleteQTimeRepDett_OLDAfterPost
Parameters SQL.Stringsselect *from EBC_RicercheTimeSDettaglio MasterDataSetQRicTimeSheetLinkedMasterFieldIDLinkedDetailFieldIDRicTimeSheet	UseFilterLeft�Topx TAutoIncFieldQTimeRepDettID	FieldNameIDReadOnly	  TIntegerFieldQTimeRepDettIDRicTimeSheet	FieldNameIDRicTimeSheet  TIntegerFieldQTimeRepDettIDCausale	FieldName	IDCausale  TFloatFieldQTimeRepDettOre	FieldNameOre  
TMemoFieldQTimeRepDettNote	FieldNameNoteBlobTypeftMemo  TStringFieldQTimeRepDettCausaleDisplayWidth2	FieldKindfkLookup	FieldNameCausaleLookupDataSetQTRepCausaliLookupKeyFieldsIDLookupResultFieldCausale	KeyFields	IDCausaleSize2Lookup	   TADOLinkedQueryQCandRic
ConnectionData.DB
CursorTypectStatic
BeforeOpenQCandRic_OLDBeforeOpen
AfterCloseQCandRic_OLDAfterClose	AfterPostQCandRic_OLDAfterPostOnCalcFieldsQCandRic_OLDCalcFields
Parameters MasterDataSetTRicerchePendLinkedMasterFieldIDLinkedDetailField	IDRicerca	UseFilterLeft� Top@ TIntegerField
QCandRicID	FieldNameIDReadOnly	  TIntegerFieldQCandRicIDRicerca	FieldName	IDRicerca  TStringFieldQCandRicCognome	FieldNameCognomeSize  TStringFieldQCandRicNome	FieldNameNomeSize  TIntegerFieldQCandRicCVNumero	FieldNameCVNumero  TIntegerFieldQCandRicIDStato	FieldNameIDStato  TStringFieldQCandRicCellulare	FieldName	CellulareSize  TStringFieldQCandRicTelUfficio	FieldName
TelUfficioSize  TDateTimeFieldQCandRicDataNascita	FieldNameDataNascita  TStringFieldQCandRicRecapitiTelefonici	FieldNameRecapitiTelefoniciSize  TIntegerFieldQCandRicIDAnagrafica	FieldNameIDAnagrafica  TBooleanFieldQCandRicEscluso	FieldNameEscluso  TDateTimeFieldQCandRicDataImpegno	FieldNameDataImpegno  TSmallintFieldQCandRicCodstato	FieldNameCodstato  TStringFieldQCandRicNote	FieldNameNoteSize   TStringFieldQCandRicStato	FieldNameStatoSize   TStringFieldQCandRicTipoStato	FieldName	TipoStato  TDateTimeFieldQCandRicDataIns	FieldNameDataIns  TStringFieldQCandRicMiniVal	FieldNameMiniValSize  TDateTimeFieldQCandRicDataUltimoContatto	FieldNameDataUltimoContatto  TIntegerFieldQCandRicIDTitoloStudioMin	FieldNameIDTitoloStudioMinReadOnly	  TStringFieldQCandRicAzienda	FieldKindfkLookup	FieldNameAziendaLookupDataSetQLastEspLavLookupKeyFieldsIDAnagraficaLookupResultFieldAzienda	KeyFieldsIDAnagraficaSize2Lookup	  TStringFieldQCandRicTelAzienda	FieldKindfkLookup	FieldName
TelAziendaLookupDataSetQLastEspLavLookupKeyFieldsIDAnagraficaLookupResultFieldTelefono	KeyFieldsIDAnagraficaSizeLookup	  TStringFieldQCandRicDiploma	FieldKindfkLookup	FieldNameDiplomaLookupDataSetQTitoliStudioLKLookupKeyFieldsIDLookupResultFieldDescrizione	KeyFieldsIDTitoloStudioMinSize<Lookup	  TStringFieldQCandRicEta	FieldKindfkCalculated	FieldNameEtaSize
Calculated	  TStringFieldQCandRicTelUffCell	FieldKindfkCalculated	FieldName
TelUffCellSize2
Calculated	  TBooleanFieldQCandRicFlagAccPres	FieldNameFlagAccPres   TADOLinkedQueryTRicerchePend
ConnectionData.DB
BeforeEditTRicerchePend_OLDBeforeEdit	AfterPostTRicerchePend_OLDAfterPost
Parameters SQL.Strings0SELECT ebc_ricerche.ID, ebc_ricerche.IDCliente, @               ebc_ricerche.IDUtente, ebc_ricerche.Progressivo, ?               ebc_ricerche.DataInizio, ebc_ricerche.DataFine, :               ebc_ricerche.Stato, ebc_ricerche.Conclusa, @               ebc_ricerche.IDStatoRic, ebc_ricerche.DallaData, =               ebc_ricerche.IDArea, ebc_ricerche.IDMansione, ?               ebc_ricerche.IDSpec, ebc_ricerche.NumRicercati, :               ebc_ricerche.Note, ebc_ricerche.IDFattura, >               ebc_ricerche.Sospesa, ebc_ricerche.SospesaDal, I               ebc_ricerche.StipendioNetto, ebc_ricerche.StipendioLordo, ?               ebc_ricerche.Fatturata, ebc_ricerche.Specifiche,'               ebc_ricerche.SpecifEst, E               ggDiffApRic,ggDiffUcRic,ggDiffColRic,DataPrevChiusura,E               aree.Descrizione Area, mansioni.Descrizione Mansione, +               mansioni.IDArea IDAreaMans, 0               ebc_clienti.Descrizione Cliente, (               users.Nominativo Utente, .               ebc_statiric.StatoRic StatoRic,0               ebc_ricerche.Tipo, IDContrattoCG,1               ebc_ricerche.IDSede,TitoloCliente,A               ebc_ricerche.IDContattoCli,ebc_ricerche.IDOfferta,>               Aziende.Descrizione Sede, IDUtente2, IDUtente3,7               EBC_ContattiClienti.Contatto Rifinterno,=               Provvigione,ProvvigionePerc,QuandoPagamUtente,D               OreLavUtente1,OreLavUtente2,OreLavUtente3, IDCausale,              DataPresuntaPresDFROM EBC_Ricerche, Aree, Mansioni, EBC_Clienti, Users, EBC_StatiRic,)            EBC_ContattiClienti, Aziende )WHERE  (ebc_ricerche.IDArea *= aree.ID)  1   AND  (ebc_ricerche.IDMansione = mansioni.ID)  3   AND  (ebc_ricerche.IDCliente = ebc_clienti.ID)  ,   AND  (ebc_ricerche.IDUtente = users.ID)  5   AND  (ebc_ricerche.IDStatoRic = ebc_statiric.ID)  +   AND  (ebc_ricerche.IDSede *= Aziende.ID)>   AND  (ebc_ricerche.IDContattoCli *= EBC_ContattiClienti.ID)  	UseFilterLeftTop@ TAutoIncFieldTRicerchePendID	FieldNameIDReadOnly	  TIntegerFieldTRicerchePendIDCliente	FieldName	IDCliente  TIntegerFieldTRicerchePendIDUtente	FieldNameIDUtente  TStringFieldTRicerchePendProgressivo	FieldNameProgressivo	FixedChar	Size  TDateTimeFieldTRicerchePendDataInizio	FieldName
DataInizio  TDateTimeFieldTRicerchePendDataFine	FieldNameDataFine  TStringFieldTRicerchePendStato	FieldNameStato	FixedChar	Size  TBooleanFieldTRicerchePendConclusa	FieldNameConclusa  TIntegerFieldTRicerchePendIDStatoRic	FieldName
IDStatoRic  TDateTimeFieldTRicerchePendDallaData	FieldName	DallaData  TIntegerFieldTRicerchePendIDArea	FieldNameIDArea  TIntegerFieldTRicerchePendIDMansione	FieldName
IDMansione  TIntegerFieldTRicerchePendIDSpec	FieldNameIDSpec  TSmallintFieldTRicerchePendNumRicercati	FieldNameNumRicercati  
TMemoFieldTRicerchePendNote	FieldNameNoteBlobTypeftMemo  TIntegerFieldTRicerchePendIDFattura	FieldName	IDFattura  TBooleanFieldTRicerchePendSospesa	FieldNameSospesa  TDateTimeFieldTRicerchePendSospesaDal	FieldName
SospesaDal  TFloatFieldTRicerchePendStipendioNetto	FieldNameStipendioNetto  TFloatFieldTRicerchePendStipendioLordo	FieldNameStipendioLordo  TBooleanFieldTRicerchePendFatturata	FieldName	Fatturata  
TMemoFieldTRicerchePendSpecifiche	FieldName
SpecificheBlobTypeftMemo  
TMemoFieldTRicerchePendSpecifEst	FieldName	SpecifEstBlobTypeftMemo  TSmallintFieldTRicerchePendggDiffApRic	FieldNameggDiffApRic  TSmallintFieldTRicerchePendggDiffUcRic	FieldNameggDiffUcRic  TSmallintFieldTRicerchePendggDiffColRic	FieldNameggDiffColRic  TDateTimeFieldTRicerchePendDataPrevChiusura	FieldNameDataPrevChiusura  TStringFieldTRicerchePendArea	FieldNameArea	FixedChar	Size  TStringFieldTRicerchePendMansione	FieldNameMansione	FixedChar	Size(  TIntegerFieldTRicerchePendIDAreaMans	FieldName
IDAreaMans  TStringFieldTRicerchePendCliente	FieldNameCliente	FixedChar	Size2  TStringFieldTRicerchePendUtente	FieldNameUtente	FixedChar	Size  TStringFieldTRicerchePendStatoRic	FieldNameStatoRic	FixedChar	  TStringFieldTRicerchePendTipo	FieldNameTipo	FixedChar	Size  TIntegerFieldTRicerchePendIDContrattoCG	FieldNameIDContrattoCG  TIntegerFieldTRicerchePendIDSede	FieldNameIDSede  TStringFieldTRicerchePendTitoloCliente	FieldNameTitoloCliente	FixedChar	Size2  TIntegerFieldTRicerchePendIDContattoCli	FieldNameIDContattoCli  TIntegerFieldTRicerchePendIDOfferta	FieldName	IDOfferta  TStringFieldTRicerchePendSede	FieldNameSede	FixedChar	Size  TIntegerFieldTRicerchePendIDUtente2	FieldName	IDUtente2  TIntegerFieldTRicerchePendIDUtente3	FieldName	IDUtente3  TStringFieldTRicerchePendRifinterno	FieldName
Rifinterno	FixedChar	Size2  TBooleanFieldTRicerchePendProvvigione	FieldNameProvvigione  TIntegerFieldTRicerchePendProvvigionePerc	FieldNameProvvigionePerc  TStringFieldTRicerchePendQuandoPagamUtente	FieldNameQuandoPagamUtente	FixedChar	Size2  TIntegerFieldTRicerchePendOreLavUtente1	FieldNameOreLavUtente1  TIntegerFieldTRicerchePendOreLavUtente2	FieldNameOreLavUtente2  TIntegerFieldTRicerchePendOreLavUtente3	FieldNameOreLavUtente3  TIntegerFieldTRicerchePendIDCausale	FieldName	IDCausale  TStringFieldTRicerchePendCausaleDisplayWidth2	FieldKindfkLookup	FieldNameCausaleLookupDataSet
QCausaliLKLookupKeyFieldsIDLookupResultFieldCausale	KeyFields	IDCausaleSize2Lookup	  TDateTimeFieldTRicerchePendDataPresuntaPres	FieldNameDataPresuntaPres   TADOLinkedTableQOrgNew
ConnectionData.DB
CursorTypectStaticFiltered	
BeforeOpenQOrgNew_OLDBeforeOpen	AfterOpenQOrgNew_OLDAfterOpenBeforeCloseQOrgNew_OLDBeforeClose
AfterCloseQOrgNewAfterCloseAfterInsertQOrgNew_OLDAfterInsertAfterScrollQOrgNew_OLDAfterScrollOnCalcFieldsQOrgNew_OLDCalcFields	TableNameOrganigrammaLeft TopX TAutoIncField	QOrgNewID	FieldNameIDReadOnly	  TIntegerFieldQOrgNewIDLivello	FieldName	IDLivello  TStringFieldQOrgNewTipo	FieldNameTipoSize  TStringFieldQOrgNewDescrizione	FieldNameDescrizioneSize<  TStringFieldQOrgNewDescBreve	FieldName	DescBreveSize
  TDateTimeFieldQOrgNewDataAgg	FieldNameDataAgg  TStringFieldQOrgNewTipoSubordine	FieldNameTipoSubordineSize  TIntegerFieldQOrgNewNodoPadre	FieldName	NodoPadre  TIntegerFieldQOrgNewIDMansione	FieldName
IDMansione  TIntegerFieldQOrgNewIDDipendente	FieldNameIDDipendente  TSmallintFieldQOrgNewPercentuale	FieldNamePercentuale  TIntegerFieldQOrgNewCentroCosto	FieldNameCentroCosto  TSmallintFieldQOrgNewMonteOreRichiesto	FieldNameMonteOreRichiesto  TSmallintFieldQOrgNewTassoAssenteismo	FieldNameTassoAssenteismo  TBooleanFieldQOrgNewInterim	FieldNameInterim  TBooleanFieldQOrgNewInStaff	FieldNameInStaff  TFloatFieldQOrgNewValutazGen	FieldName
ValutazGen  TFloatFieldQOrgNewIndiceSost	FieldName
IndiceSost  TFloatFieldQOrgNewIndiceImp	FieldName	IndiceImp  TDateTimeFieldQOrgNewUltimoAgg	FieldName	UltimoAgg  TIntegerFieldQOrgNewPARENT	FieldNamePARENT  TIntegerFieldQOrgNewWIDTH	FieldNameWIDTH  TIntegerFieldQOrgNewHEIGHT	FieldNameHEIGHT  TStringFieldQOrgNewTYPE	FieldNameTYPE  TIntegerFieldQOrgNewCOLOR	FieldNameCOLOR  TIntegerFieldQOrgNewIMAGE	FieldNameIMAGE  TStringFieldQOrgNewIMAGEALIGN	FieldName
IMAGEALIGN  TIntegerFieldQOrgNewORDINE	FieldNameORDINE  TStringFieldQOrgNewALIGN	FieldNameALIGN  TIntegerFieldQOrgNewIDReparto	FieldName	IDReparto  TIntegerFieldQOrgNewIDCliente	FieldName	IDCliente  TIntegerFieldQOrgNewIDAzienda	FieldName	IDAzienda  TFloatFieldQOrgNewRALmin	FieldNameRALmin  TFloatFieldQOrgNewRALmax	FieldNameRALmax  TIntegerFieldQOrgNewMinMesiAccesso	FieldNameMinMesiAccesso  TStringFieldQOrgNewMansione	FieldKindfkLookup	FieldNameMansioneLookupDataSetQMansioniLKLookupKeyFieldsIDLookupResultFieldDescrizione	KeyFields
IDMansioneSize(Lookup	  TStringFieldQOrgNewDicNodo	FieldKindfkCalculated	FieldNameDicNodoSize!
Calculated	  TStringFieldQOrgNewCognome	FieldKindfkLookup	FieldNameCognomeLookupDataSetQAnagLKLookupKeyFieldsIDLookupResultFieldCognome	KeyFieldsIDDipendenteSizeLookup	  TStringFieldQOrgNewNome	FieldKindfkLookup	FieldNameNomeLookupDataSetQAnagLKLookupKeyFieldsIDLookupResultFieldNome	KeyFieldsIDDipendenteSizeLookup	  TStringFieldQOrgNewReparto	FieldKindfkLookup	FieldNameRepartoLookupDataSet
QRepartiLKLookupKeyFieldsIDLookupResultFieldReparto	KeyFields	IDRepartoSize2Lookup	   TADOLinkedQueryQAnagLK
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect ID,Cognome,Nomefrom Anagrafica 	UseFilterLeft8Topp TAutoIncField	QAnagLKID	FieldNameIDReadOnly	  TStringFieldQAnagLKCognome	FieldNameCognomeSize  TStringFieldQAnagLKNome	FieldNameNomeSize   TADOLinkedQuery
QRepartiLK
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect * from Reparti 	UseFilterLeft�Topp TAutoIncFieldQRepartiLKID	FieldNameIDReadOnly	  TStringFieldQRepartiLKTipo	FieldNameTipoSize  TStringFieldQRepartiLKReparto	FieldNameRepartoSize2  TStringFieldQRepartiLKCodice	FieldNameCodiceSize  TIntegerFieldQRepartiLKIDAzienda	FieldName	IDAzienda   	TADOQueryQCommessaCL
ConnectionData.DB
CursorTypectStatic
BeforeOpenQCommessaCLBeforeOpenBeforeCloseQCommessaCLBeforeCloseAfterInsertQCommessaCLAfterInsert
ParametersName
xIDRicerca
AttributespaSigned
paNullable DataType	ftInteger	Precision
SizeValue   SQL.Stringsselect * from CLCommessaVociwhere IDRicerca=:xIDRicerca Left0Top  TAutoIncFieldQCommessaCLID	FieldNameIDReadOnly	  TIntegerFieldQCommessaCLIDRicerca	FieldName	IDRicerca  TIntegerFieldQCommessaCLIDVoceStandard	FieldNameIDVoceStandard  TStringFieldQCommessaCLVoce	FieldNameVoceSized   TDataSourceDsQCommessaCLDataSetQCommessaCLOnStateChangeDsQCommessaCLStateChangeLeft0Top0  	TADOQueryQCLStat
ConnectionData.DB
CursorTypectStatic
BeforeOpenQCLStatBeforeOpen
ParametersName
xIDRicerca
AttributespaSigned
paNullable DataType	ftInteger	Precision
SizeValue   SQL.StringsKselect CLColloquio.ID,rtrim(Cognome)+' '+substring(nome,1,1)+'.' Candidato,2          Data,CLCommessaVoci.Voce, Flag, Risposta:from CLColloquio, CLCommessaVoci, EBC_Colloqui, Anagrafica2where CLColloquio.IDCommessaVoce=CLCommessaVoci.ID-  and CLColloquio.IDColloquio=EBC_Colloqui.ID-  and EBC_Colloqui.IDAnagrafica=Anagrafica.ID(and CLCommessaVoci.IDRicerca=:xIDRicerca Left� Top   TDataSource	DsQCLStatDataSetQCLStatLeft� Top0  TADOLinkedQueryQTotCompFattLKActive	
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Strings5select IDRicerca,sum(Importo) ImpFatturato, rimbspesefrom EBC_CompensiRicerche\where (IDFattura is not null) and (IDFattura>0) and ((RimbSpese = 0) or (RimbSpese is null))group by IDRicerca,rimbspese 	UseFilterLeft8Top TIntegerFieldQTotCompFattLKIDRicerca	FieldName	IDRicerca  TFloatFieldQTotCompFattLKImpFatturato	FieldNameImpFatturatoReadOnly	   	TADOQueryqTotRimbSpeseLKActive	
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Strings*select IDRicerca,sum(Importo) as RimbSpesefrom EBC_CompensiRicerchewhere RimbSpese = 1group by IDRicerca LeftPTopX TIntegerFieldqTotRimbSpeseLKIDRicerca	FieldName	IDRicerca  TFloatFieldqTotRimbSpeseLKRimbSpese	FieldName	RimbSpeseReadOnly	   	TADOQueryqCollaboratoriRicLK
ConnectionData.DB
CursorTypectStatic
DataSourceDsQRicAttive
Parameters SQL.Stringsselect Nominativo,IDricercafrom ebc_ricercheutenti,users+where users.id= ebc_ricercheutenti.idutenteand idruoloric <> 1 LeftTopH TStringFieldqCollaboratoriRicLKNominativo	FieldName
NominativoSize  TIntegerFieldqCollaboratoriRicLKIDricerca	FieldName	IDricerca    