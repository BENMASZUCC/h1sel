�
 TSELESPLAVFORM 0  TPF0TSelEspLavFormSelEspLavFormLeft TopjBorderStylebsDialogCaption'Selezione esperienza lavorativa attualeClientHeight� ClientWidthuColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterPixelsPerInch`
TextHeight TPanelPanel1Left Top WidthuHeight1AlignalTop
BevelOuter	bvLoweredTabOrder  TBitBtnBitBtn1Left�TopWidthfHeight'TabOrder KindbkOK  TBitBtnBitBtn2Left	TopWidthfHeight'CaptionAnnullaTabOrderKindbkCancel  TBitBtnBNuovaLeftTopWidthfHeight'CaptionInserisci nuovaFont.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderOnClickBNuovaClick   	TdxDBGrid	dxDBGrid1Left Top1WidthuHeight� Bands  DefaultLayout	HeaderPanelRowCountKeyFieldIDSummaryGroups SummarySeparator, AlignalClientTabOrder
DataSource	DsQEspLavFilter.Criteria
       LookAndFeellfFlatOptionsBehavioredgoAutoSearchedgoAutoSortedgoDragScrolledgoEnterShowEditoredgoImmediateEditoredgoTabThroughedgoVertThrough 	OptionsDBedgoCancelOnExitedgoCanDeleteedgoCanInsertedgoCanNavigationedgoConfirmDeleteedgoLoadAllRecordsedgoUseBookmarks OptionsViewedgoAutoWidthedgoBandHeaderWidthedgoIndicatoredgoUseBitmap  TdxDBGridMaskColumndxDBGrid1AziendaWidth� 	BandIndex RowIndex 	FieldNameAzienda  TdxDBGridMaskColumndxDBGrid1AttivitaWidth� 	BandIndex RowIndex 	FieldNameAttivita  TdxDBGridMaskColumndxDBGrid1AnnoDalCaption	Dall'annoSortedcsDownWidthH	BandIndex RowIndex 	FieldNameAnnoDal  TdxDBGridMaskColumndxDBGrid1MeseDalCaptionDal meseWidthF	BandIndex RowIndex 	FieldNameMeseDal  TdxDBGridMaskColumndxDBGrid1RuoloWidth� 	BandIndex RowIndex 	FieldNameRuolo   TDataSource	DsQEspLavDataSetQEspLavLeft0Topp  	TADOQueryQEspLav
ConnectionData.DB
CursorTypectStatic
ParametersNamexIDAnag
AttributespaSigned
paNullable DataType	ftInteger	Precision
SizeValue   SQL.Strings>select EBC_Clienti.Descrizione Azienda, EBC_Attivita.Attivita,?       EsperienzeLavorative.ID,EsperienzeLavorative.IDMansione,2       AnnoDal,MeseDal, Mansioni.Descrizione Ruolo>from EsperienzeLavorative, EBC_Clienti, EBC_Attivita, Mansioni5where EsperienzeLavorative.IDAzienda = EBC_Clienti.ID.  and EBC_Clienti.IDAttivita = EBC_Attivita.ID4  and EsperienzeLavorative.IDMansione *= Mansioni.IDand IDAnagrafica=:xIDAnag"and (attuale=0 or attuale is null) order by EsperienzeLavorative.ID  Left0TopP TStringFieldQEspLavAzienda	FieldNameAziendaSize2  TStringFieldQEspLavAttivita	FieldNameAttivitaSize2  TAutoIncField	QEspLavID	FieldNameIDReadOnly	  TIntegerFieldQEspLavIDMansione	FieldName
IDMansione  TSmallintFieldQEspLavAnnoDal	FieldNameAnnoDal  TSmallintFieldQEspLavMeseDal	FieldNameMeseDal  TStringFieldQEspLavRuolo	FieldNameRuoloSize(   TADOLinkedQueryQ
ConnectionData.DB
Parameters 	UseFilterLeft0Top�    