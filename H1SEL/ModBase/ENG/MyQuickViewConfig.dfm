�
 TMYQUICKVIEWCONFIGFORM 0�  TPF0TMyQuickViewConfigFormMyQuickViewConfigFormLeft TopjBorderStylebsDialogCaptionConfiguratore My Quick ViewClientHeightClientWidth_Color	clBtnFaceFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style OldCreateOrderPositionpoScreenCenterOnClose	FormCloseOnShowFormShowPixelsPerInch`
TextHeight 	TSplitter	Splitter1LeftTop-WidthHeight� CursorcrHSplit  TPanelPanel1Left Top Width_Height-AlignalTop
BevelOuter	bvLoweredTabOrder  TBitBtnBitBtn1LeftTopWidthZHeight&AnchorsakTopakRight CaptionEsciTabOrder KindbkClose   TPanelPanel2Left Top-WidthHeight� AlignalLeft
BevelOuter	bvLoweredTabOrder TPanelPanTitoloCompensiLeftTopWidthHeightAlignalTop	AlignmenttaLeftJustifyCaption  Voci disponibiliColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   	TdxDBGrid	dxDBGrid1LeftTopWidthHeight� Bands  DefaultLayout	HeaderPanelRowCountKeyFieldIDSummaryGroups SummarySeparator, AlignalClientTabOrder
DataSourceDsQDispFilter.Criteria
       LookAndFeellfFlatOptionsBehavioredgoAutoSearchedgoAutoSortedgoDragScrolledgoEditingedgoEnterShowEditoredgoImmediateEditoredgoTabThroughedgoVertThrough 	OptionsDBedgoCancelOnExitedgoCanDeleteedgoCanInsertedgoCanNavigationedgoConfirmDeleteedgoLoadAllRecordsedgoUseBookmarks OptionsViewedgoAutoWidthedgoBandHeaderWidthedgoIndicatoredgoUseBitmap  TdxDBGridMaskColumndxDBGrid1VoceDisableEditor	SortedcsUpWidth� 	BandIndex RowIndex 	FieldNameVoce  TdxDBGridGraphicColumndxDBGrid1Column2	AlignmenttaCenterCaptionIconaVisibleWidth3	BandIndex RowIndex 	FieldName	SmallIcon    TPanelPanel3LeftGTop-WidthHeight� AlignalClient
BevelOuter	bvLoweredTabOrder TPanelPanel4LeftTopWidthHeightAlignalTop	AlignmenttaLeftJustifyCaption.  Voci visibili per l'utente e in quale ordineColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   	TdxDBGrid	dxDBGrid2LeftTopWidthHeight� Bands  DefaultLayout	HeaderPanelRowCountKeyFieldIDSummaryGroups SummarySeparator, AlignalClientTabOrder
DataSourceDsQVisibiliFilter.Criteria
       LookAndFeellfFlat	OptionsDBedgoCancelOnExitedgoCanDeleteedgoCanInsertedgoCanNavigationedgoConfirmDeleteedgoLoadAllRecordsedgoUseBookmarks OptionsViewedgoAutoWidthedgoBandHeaderWidthedgoIndicatoredgoUseBitmap  TdxDBGridMaskColumndxDBGrid2DicNodoMasterCaptionVoceDisableEditor	Width� 	BandIndex RowIndex 	FieldNameDicNodoMaster  TdxDBGridMaskColumndxDBGrid2OrdineSortedcsUpWidth9	BandIndex RowIndex 	FieldNameOrdine    TPanelPanel5LeftTop-Width/Height� AlignalLeft
BevelOuter	bvLoweredTabOrder TToolbarButton97
BInserisciLeftTop)Width&Height Hint-Rendi visibili la voce selezionata all'utenteCaption>Font.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.StylefsBold 
ParentFontParentShowHintShowHint	OnClickBInserisciClick  TToolbarButton97BTogliLeftTopIWidth&Height Hint6Togli la visibilit� per la voce selezionata all'utenteCaption<Font.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.StylefsBold 
ParentFontParentShowHintShowHint	OnClickBTogliClick  TToolbarButton97BTutteLeftTopsWidth&Height Hint'Rendi visibili all'utente tutte le vociCaption>>Font.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.StylefsBold 
ParentFontParentShowHintShowHint	OnClickBTutteClick   	TADOQueryQDispActive	
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Strings'select ID,DicNodoMaster Voce, SmallIconfrom MyQuickViewModel Left� Top�   TDataSourceDsQDispDataSetQDispLeft� Top�   	TADOQuery	QVisibili
ConnectionData.DB
CursorTypectStatic
ParametersName	xIDUtente
AttributespaSigned
paNullable DataType	ftInteger	Precision
SizeValue   SQL.Strings:select MyQuickViewModel.DicNodoMaster , MyQuickViewUsers.*'from MyQuickViewModel, MyQuickViewUsers8where MyQuickViewModel.ID = MyQuickViewUsers.IDModelItemand IDUtente=:xIDUtente LeftgTop�   TDataSourceDsQVisibiliDataSet	QVisibiliLeftgTop�   	TADOQueryQ
ConnectionData.DB
Parameters Left Top�    