�
 TMODPREZZOFORM 0  TPF0TModPrezzoFormModPrezzoFormLeft� TopjActiveControl
PrezzoEuroBorderStylebsDialogCaptionModifica prezzoClientHeightVClientWidth(Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TLabelLabel3LeftTopCWidth#HeightCaptionPrezzo:  TLabelLabel6Left� Top+WidthHeightCaptionLireFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsItalic 
ParentFontVisible  TLabelLabel7LeftTop+WidthHeightCaptionEuroFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsItalic 
ParentFont  TBevelBevel1Left,Top9WidthzHeightShape	bsTopLineVisible  TBevelBevel2Left� Top9WidthzHeightShape	bsTopLine  TBitBtnBitBtn1LefthTopWidth_Height&TabOrderKindbkOK  TBitBtnBitBtn2Left� TopWidth_Height&CaptionAnnullaTabOrderKindbkCancel  TRxCalcEdit
PrezzoLireLeft,Top?Width{HeightAutoSizeDisplayFormat�'.' ,0.Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold FormatOnEditing		NumGlyphs
ParentFontTabOrder VisibleOnChangePrezzoLireChange  TRxCalcEdit
PrezzoEuroLeft� Top?Width{HeightAutoSizeDisplayFormat
�'.' ,0.00FormatOnEditing		NumGlyphsTabOrderOnChangePrezzoEuroChange   