�
 TSTORICOCOMPFORM 0�  TPF0TStoricoCompFormStoricoCompFormLeftTop� BorderIcons
biMinimize BorderStylebsDialogCaptionStorico competenzeClientHeightClientWidthHColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrder	PositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TPanelPanel1Left Top WidthHHeight=AlignalTopTabOrder  TBitBtnBitBtn1Left�TopWidthcHeight&CaptionOKDefault	ModalResultTabOrder 
Glyph.Data
�  �  BM�      v   (   $            h                      �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 333333333333333333  333333333333�33333  334C33333338�33333  33B$3333333�8�3333  34""C33333833�3333  3B""$33333�338�333  4"*""C3338�8�3�333  2"��"C3338�3�333  :*3:"$3338�38�8�33  3�33�"C333�33�3�33  3333:"$3333338�8�3  33333�"C333333�3�3  33333:"$3333338�8�  333333�"C333333�3�  333333:"C3333338�  3333333�#3333333��  3333333:3333333383  333333333333333333  	NumGlyphs  TPanelPanel2LeftTopWidth@Height;AlignalLeft
BevelOuterbvNoneEnabledTabOrder TLabelLabel1LeftTop#Width>HeightCaptionCompetenza:  TDBEditDBEdit12LeftTopWidth� HeightColorclYellow	DataFieldCognome
DataSourceData.DsAnagraficaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontReadOnly	TabOrder   TDBEditDBEdit14Left� TopWidth~HeightColorclYellow	DataFieldNome
DataSourceData.DsAnagraficaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontReadOnly	TabOrder  TDBEditDBEdit1LeftKTopWidth� Height	DataFieldDescCompetenza
DataSourceData2.DsCompDipendenteReadOnly	TabOrder    TDBChartDBChart1Left Top!WidthHHeight� BackWall.Brush.ColorclWhiteBackWall.Brush.StylebsClear
MarginLeftMarginRightTitle.Text.StringsCurva andamento valori BottomAxis.Increment� Legend.VisibleView3DAlignalClient
BevelOuter	bvLoweredTabOrder TAreaSeriesSeries1Marks.ArrowLengthMarks.StylesmsValueMarks.Visible	
DataSourceTStoricoSeriesColorclSilverXLabelsSource	DallaData	AreaBrushbsFDiagonalDrawArea	Pointer.InflateMargins	Pointer.StylepsRectanglePointer.VisibleXValues.DateTime	XValues.NameXXValues.MultiplierXValues.OrderloAscendingXValues.ValueSource	DallaDataYValues.DateTimeYValues.NameYYValues.MultiplierYValues.OrderloNoneYValues.ValueSourceValore   TPanelPanel3Left Top=WidthHHeight� AlignalTopCaptionPanel3TabOrder TToolbarButton97BDelLeft�Top#WidthNHeight!Captionelimina
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 30    3337wwww�330�wwp3337�����330���p3337�����330��pp3337�����330���p3337�����330��pp3337�����330���p333�������00��pp0377�����33 ���p33w����s330��pp3337�����330pppp3337�����33     33wwwww33��ww33����33     33wwwwws3330wp333337���33330  333337ww333	NumGlyphsOnClick	BDelClick  TToolbarButton97BNewLeft�TopWidthNHeight!Captionnuovo
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333�3333�39�3330 373337w39�3330 3��337w9���33337ww333?9���333 7wws333w39�3333 373333w39�3333337s3333?3333333 3333�33w333<333 3337�33w333<�3333����?�<����Ù37wwwww�<����Ù37wwwwsw3333<�3333337s33�333<330 3337337w3333330 3333337w3333333333333333	NumGlyphsOnClick	BNewClick  TToolbarButton97BOKLeft�TopDWidthNHeight!CaptionconfermaEnabled
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� UUUUUUUUUUUUUUUUUUUUUUUUUUU�UUUUUUYUUUUUUW�UUUUU��UUUUUUww�UUUUU��UUUUUUww�UUUUY��UUUUWwwUUUU���UUUUwww�UUW���UUUWwuww�UUyUY�UUUwuUWw�UUUUUY�UUUUUWwUUUUUU�UUUUUUw�UUUUUY�UUUUUUWw�UUUUUUyUUUUUUw�UUUUUW�UUUUUUWw�UUUUUUY�UUUUUUWwUUUUUUUUUUUUUUUU	NumGlyphsOnClickBOKClick  TToolbarButton97BCanLeft�TopeWidthNHeight!CaptionannullaEnabled
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� P  UUUWwwuU�U    UPUUwwwwU�UU U PUUUw�wWUUU UPUUUUw�W_�UUPP UUUW�uw�UUU PPUUUw�Ww�UUU	�UUU�uwww�UPU	��UW_wwwu�PP��0UWu�wwW_U PU	UwWUwuuuUUUUP��0UU_UWWWWUUUU3UUuUUuuUUUUUUP��UU�UUW_UUPUUUU�UWUUUUu�UUUUUUP�UUUUUUW_UUUUUUUUUUUUUUu	NumGlyphsOnClick	BCanClick  	TdxDBGrid	dxDBGrid1LeftTopWidth�Height� Bands  DefaultLayout	HeaderPanelRowCountKeyFieldIDShowGroupPanel	SummaryGroups SummarySeparator, AlignalLeftTabOrder 
DataSource	DsStoricoFilter.Criteria
       	OptionsDBedgoCancelOnExitedgoCanDeleteedgoCanInsertedgoCanNavigationedgoConfirmDeleteedgoLoadAllRecordsedgoUseBookmarks OptionsViewedgoAutoWidthedgoBandHeaderWidthedgoIndicatoredgoUseBitmap  TdxDBGridDateColumndxDBGrid1DallaDataCaption
Dalla dataSortedcsDownWidth\	BandIndex RowIndex 	FieldName	DallaData  TdxDBGridMaskColumndxDBGrid1MotivoAumentoCaptioneventuale motivo aumentoWidthL	BandIndex RowIndex 	FieldNameMotivoAumento  TdxDBGridSpinColumndxDBGrid1Valore	AlignmenttaCenterWidth?	BandIndex RowIndex 	FieldNameValore    TDataSource	DsStoricoDataSetTStoricoOnStateChangeDsStoricoStateChangeLeft0Top�   TADOLinkedQueryTStoricoActive	
ConnectionData.DB
CursorTypectStatic	AfterPostTStorico_OLDAfterPostOnCalcFieldsTStorico_OLDCalcFields
DataSourceData2.DsCompDipendente
ParametersNameIDAnagrafica
AttributespaSigned
paNullable DataType	ftInteger	Precision
SizeValue  NameIDCompetenza
AttributespaSigned
paNullable DataType	ftInteger	Precision
SizeValue   SQL.Stringsselect * from StoricoCompAnag where IDAnagrafica=:IDAnagraficaand IDCompetenza=:IDCompetenza 	UseFilterLeft0Top}   