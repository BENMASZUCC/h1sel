�
 TFRMANAGCONOSCINFO 0&  TPF0TFrmAnagConoscInfoFrmAnagConoscInfoLeft Top WidthHeight�TabOrder  TPanelPanel1Left Top WidthHeight,AlignalTopTabOrder  TLabelLabel34LeftTopWidthJHeight CaptionApplicativi conosciutiFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontWordWrap	  TToolbarButton97BVoceNewLeft� TopWidth>Height%Caption
Nuova voceOpaqueWordWrap	OnClickBVoceNewClick  TToolbarButton97BVoceDelLeftNTopWidth>Height%CaptionElimina voceOpaqueWordWrap	OnClickBVoceDelClick  TToolbarButton97BVoceModLeftTopWidth>Height%CaptionModifica applicativoOpaqueWordWrap	OnClickBVoceModClick  TToolbarButton97BVoceCanLeft�TopWidth>Height%CaptionAnnulla modificaEnabledOpaqueWordWrap	OnClickBVoceCanClick  TToolbarButton97BVoceOKLeft�TopWidth>Height%CaptionConferma modificaEnabledOpaqueWordWrap	OnClickBVoceOKClick   TDBCtrlGridDBCtrlGrid1Left Top,WidthHeighttAlignalClientColCount
DataSourceDsQAnagConoscInfoPanelHeight|
PanelWidth	TabOrderRowCountSelectedColorclInfoBk TLabelLabel1Left� TopWidthHeightCaptionAreaFocusControlDBEdit1  TLabelLabel2LeftUTopWidth.HeightCaption	SottoareaFocusControlDBEdit2  TLabelLabel3LeftTopWidth`HeightCaptionProprieta applicativoFocusControlDBEdit3  TLabelLabel4LeftTop+WidthiHeightCaptionProcedura/applicativoFocusControlDBEdit4  TLabelLabel5Left� Top+WidthdHeightCaptionPeriodo utilizzo dal/alFocusControlDBEdit5  TLabelLabel7LeftCTop+WidthHeightCaptionLivello  TLabelLabel8Left�Top+Width5HeightCaptionPiattaforme  TLabelLabel6LeftTopRWidthWHeightCaptionDescrizione / noteFocusControlDBEdit7  TDBEditDBEdit1Left� TopWidth� HeightColorclInfoBk	DataFieldArea
DataSourceDsQAnagConoscInfoReadOnly	TabOrder  TDBEditDBEdit2LeftUTopWidth� HeightColorclInfoBk	DataField	SottoArea
DataSourceDsQAnagConoscInfoReadOnly	TabOrder  TDBEditDBEdit3LeftTopWidth� HeightColorclInfoBk	DataField	Proprieta
DataSourceDsQAnagConoscInfoReadOnly	TabOrder   TDBEditDBEdit4LeftTop;Width� HeightColorclInfoBk	DataField	Procedura
DataSourceDsQAnagConoscInfoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontReadOnly	TabOrder  TDBEditDBEdit5Left� Top;Width.Height	DataField
PeriodoDal
DataSourceDsQAnagConoscInfoTabOrder  TDBEditDBEdit6LeftTop;Width/Height	DataField	PeriodoAl
DataSourceDsQAnagConoscInfoTabOrder  TDBComboBoxDBComboBox1LeftBTop;WidthSHeight	DataFieldLivello
DataSourceDsQAnagConoscInfo
ItemHeightItems.Stringsottimobuonodiscretosufficientescarso TabOrder  TDBComboBoxDBComboBox2Left�Top;WidthkHeight	DataFieldPiattaforme
DataSourceDsQAnagConoscInfo
ItemHeightItems.StringsAS/400PC MonoutenzaReteUnix TabOrder  TDBEditDBEdit7LeftTopbWidth�Height	DataFieldDescrizione
DataSourceDsQAnagConoscInfoTabOrder   TDataSourceDsQAnagConoscInfoDataSetQAnagConoscInfoOnStateChangeDsQAnagConoscInfoStateChangeLeft0Top  TADOLinkedQueryQAnagConoscInfo
ConnectionData.DB
CursorTypectStatic
BeforeOpenQAnagConoscInfoBeforeOpen	AfterPostQAnagConoscInfo_OLDAfterPostAfterDeleteQAnagConoscInfo_OLDAfterPost
Parameters SQL.Strings(select AnagConoscInfo.*,ConoscenzeInfo.*"from AnagConoscInfo,ConoscenzeInfo3where AnagConoscInfo.IDConoscInfo=ConoscenzeInfo.IDand idanagrafica=1 MasterDataSetData.TAnagraficaLinkedMasterFieldIDLinkedDetailFieldIDAnagrafica	UseFilterLeft0Top�    