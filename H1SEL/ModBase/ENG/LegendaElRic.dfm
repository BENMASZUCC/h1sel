�
 TLEGENDAELRICFORM 0]  TPF0TLegendaElRicFormLegendaElRicFormLeft8Top� BorderStylebsDialogCaptionLegenda e parametriClientHeight9ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnClose	FormCloseOnShowFormShowPixelsPerInch`
TextHeight TLabelLabel11LeftTopWidth�HeightCaption�NOTA: i seguenti valori sono GLOBALI.  E' possibile settare gli stessi valori
a livello di ogni singola commessa (entrando in commessa, pagina "parametri")Font.CharsetANSI_CHARSET
Font.ColorclPurpleFont.Height�	Font.NameArial
Font.StylefsItalic 
ParentFont  TBitBtnBitBtn1Left�TopWidthXHeight"CaptionChiudiTabOrder KindbkOK  TPanelPanel5LeftTop!Width~Height)
BevelInnerbvSpace
BevelOuter	bvLoweredTabOrder TLabelLabel13Left&TopWidth� HeightCaption7n� di giorni dalla data di prima apertura della ricercaWordWrap	  TLabelLabel14LeftTopWidthHeightCaptionApFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel15Left� TopWidthWHeightCaption!carattere rosso se pi� di giorni Font.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontWordWrap	  TRxSpinEditRxSpinEdit1LeftETopWidth2HeightTabOrder    TPanelPanel6LeftTopNWidth~Height)
BevelInnerbvRaised
BevelOuter	bvLoweredTabOrder TLabelLabel16Left&TopWidth� HeightCaptionHn� di giorni dall'ultimo CONTATTO con un soggetto associato alla ricercaWordWrap	  TLabelLabel17LeftTopWidthHeightCaptionUcFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel1Left� TopWidthWHeightCaption!carattere rosso se pi� di giorni Font.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontWordWrap	  TRxSpinEditRxSpinEdit2LeftETopWidth2HeightTabOrder    TPanelPanel7LeftTop{Width~Height)
BevelInnerbvRaised
BevelOuter	bvLoweredTabOrder TLabelLabel19LeftTopWidthHeightCaptionColFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel20Left&TopWidth� HeightCaptionIn� di giorni dall'ultimo COLLOQUIO con un soggetto associato alla ricercaWordWrap	  TLabelLabel2Left� TopWidthWHeightCaption!carattere rosso se pi� di giorni Font.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontWordWrap	  TRxSpinEditRxSpinEdit3LeftETopWidth2HeightTabOrder    TPanelPanel1LeftTop� WidthHeight]
BevelInnerbvRaised
BevelOuter	bvLoweredTabOrder TLabelLabel3LeftTopWidth� HeightCaptionlegenda colori colonna statoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel4Left0TopHWidth� HeightCaption%verde: effettuato almeno un colloquioFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel5Left0Top(Width� HeightCaption/arancione: candidati inseriti ma senza contattiFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel6Left0Top8Width� HeightCaption%giallo: effettuato almeno un contattoFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel7Left0TopWidth� HeightCaption rosso: nessun candidato inseritoFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TShapeShape1LeftTopWidth#HeightBrush.ColorclRed  TShapeShape2LeftTop)Width#HeightBrush.Color��    TShapeShape3LeftTop9Width#HeightBrush.ColorclYellow  TShapeShape4LeftTopIWidth#HeightBrush.ColorclLime   TPanelPanel2LeftTop
WidthHeight.
BevelInnerbvRaised
BevelOuter	bvLoweredTabOrder TLabelLabel8LeftTopWidth� HeightCaption$legenda colore carattere Data InizioFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel18LeftTopWidth� HeightCaption5Rosso: superata o raggiunta data prevista di chiusuraFont.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont   TQueryQ_OLDDatabaseNameEBCDBLeft�TopH  TADOLinkedQueryQ
ConnectionData.DB
Parameters Left�Toph   