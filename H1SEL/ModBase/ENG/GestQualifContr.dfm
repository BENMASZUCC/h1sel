�
 TGESTQUALIFCONTRFORM 0w  TPF0TGestQualifContrFormGestQualifContrFormLeftTop� Width�Height�CaptionFGestione qualifiche contrattuali, tipi contratti e contratti nazionaliColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnClose	FormCloseOnShowFormShowPixelsPerInch`
TextHeight TPanelPanel1Left Top Width�Height0AlignalTop
BevelOuter	bvLoweredTabOrder  TBitBtnBitBtn1Left$TopWidtheHeight&AnchorsakTopakRight CaptionEsciTabOrder KindbkClose   TPageControlPCUnicoLeft Top0Width�HeightJ
ActivePage	TSInsiemeAlignalClientTabOrder 	TTabSheet	TSInsiemeCaptionQualifiche contrattuali TPanelPanel2Left Top Width�Height(AlignalTop
BevelOuter	bvLoweredTabOrder  TToolbarButton97BNewLeftTopWidth;Height%Caption
Nuova rigaWordWrap	OnClick	BNewClick  TToolbarButton97BDelLeft<TopWidth;Height%CaptionCancella rigaWordWrap	OnClick	BDelClick  TToolbarButton97BOKLeftwTopWidth;Height%Captionconferma modificheEnabledWordWrap	OnClickBOKClick  TToolbarButton97BCanLeft� TopWidth;Height%Captionannulla modificheEnabledWordWrap	OnClick	BCanClick   	TdxDBGrid	dxDBGrid1Left Top(Width�HeightBands  DefaultLayout	HeaderPanelRowCountKeyFieldIDSummaryGroups SummarySeparator, AlignalClientTabOrder
DataSource	DsQQualifFilter.Active	Filter.Criteria
       	OptionsDBedgoCancelOnExitedgoCanDeleteedgoCanInsertedgoCanNavigationedgoConfirmDeleteedgoLoadAllRecordsedgoUseBookmarks OptionsViewedgoAutoWidthedgoBandHeaderWidthedgoIndicatoredgoUseBitmap  TdxDBGridMaskColumndxDBGrid1QualificaSortedcsUpWidth~	BandIndex RowIndex 	FieldName	Qualifica  TdxDBGridLookupColumndxDBGrid1TipoContrattoCaptionTipo di contrattoWidtht	BandIndex RowIndex 	FieldNameTipoContratto  TdxDBGridLookupColumndxDBGrid1ContrattoNazCaptionContratto nazionaleWidths	BandIndex RowIndex 	FieldNameContrattoNaz    	TTabSheetTSTipiContrattiCaptionTipi contratti
ImageIndex TPanelPanel3Left Top Width�Height(AlignalTop
BevelOuter	bvLoweredTabOrder  TToolbarButton97B2NewLeftTopWidth;Height%Caption
Nuova rigaWordWrap	OnClick
B2NewClick  TToolbarButton97B2DelLeft<TopWidth;Height%CaptionCancella rigaWordWrap	OnClick
B2DelClick  TToolbarButton97B2OKLeftwTopWidth;Height%Captionconferma modificheEnabledWordWrap	OnClick	B2OKClick  TToolbarButton97B2CanLeft� TopWidth;Height%Captionannulla modificheEnabledWordWrap	OnClick
B2CanClick   	TdxDBGrid	dxDBGrid2Left Top(Width�HeightBands  DefaultLayout	HeaderPanelRowCountKeyFieldIDSummaryGroups SummarySeparator, AlignalClientTabOrder
DataSourceDsQTipiContrattiFilter.Criteria
       	OptionsDBedgoCancelOnExitedgoCanDeleteedgoCanInsertedgoCanNavigationedgoConfirmDeleteedgoLoadAllRecordsedgoUseBookmarks OptionsViewedgoAutoWidthedgoBandHeaderWidthedgoIndicatoredgoUseBitmap  TdxDBGridMaskColumndxDBGrid2TipoContrattoCaptionTipo di contrattoSortedcsUp	BandIndex RowIndex 	FieldNameTipoContratto    	TTabSheetTSContrattiNazCaptionContratti nazionali
ImageIndex TPanelPanel4Left Top Width�Height(AlignalTop
BevelOuter	bvLoweredTabOrder  TToolbarButton97B3NewLeftTopWidth;Height%Caption
Nuova rigaWordWrap	OnClick
B3NewClick  TToolbarButton97B3DelLeft<TopWidth;Height%CaptionCancella rigaWordWrap	OnClick
B3DelClick  TToolbarButton97B3OKLeftwTopWidth;Height%Captionconferma modificheEnabledWordWrap	OnClick	B3OKClick  TToolbarButton97B3CanLeft� TopWidth;Height%Captionannulla modificheEnabledWordWrap	OnClick
B3CanClick   	TdxDBGrid	dxDBGrid3Left Top(Width�HeightBands  DefaultLayout	HeaderPanelRowCountKeyFieldIDSummaryGroups SummarySeparator, AlignalClientTabOrder
DataSourceDsQContrattiNazFilter.Criteria
       	OptionsDBedgoCancelOnExitedgoCanDeleteedgoCanInsertedgoCanNavigationedgoConfirmDeleteedgoLoadAllRecordsedgoUseBookmarks OptionsViewedgoAutoWidthedgoBandHeaderWidthedgoIndicatoredgoUseBitmap  TdxDBGridMaskColumndxDBGrid3DescrizioneCaptioncontratto nazionaleSortedcsUp	BandIndex RowIndex 	FieldNameDescrizione     	TADOQueryQQualifActive	
ConnectionData.DB
CursorTypectStaticBeforeDeleteQQualifBeforeDelete
Parameters SQL.Strings$select * from QualificheContrattuali Left,Top�  TAutoIncField	QQualifID	FieldNameIDReadOnly	  TIntegerFieldQQualifIDTipoContratto	FieldNameIDTipoContratto  TStringFieldQQualifQualifica	FieldName	QualificaSize(  TSmallintFieldQQualifOrdine	FieldNameOrdine  TIntegerFieldQQualifIDContrattoNaz	FieldNameIDContrattoNaz  TStringFieldQQualifTipoContratto	FieldKindfkLookup	FieldNameTipoContrattoLookupDataSetQTipiContrattiLKLookupKeyFieldsIDLookupResultFieldTipoContratto	KeyFieldsIDTipoContrattoLookup	  TStringFieldQQualifContrattoNaz	FieldKindfkLookup	FieldNameContrattoNazLookupDataSetQContrattiNazLKLookupKeyFieldsIDLookupResultFieldDescrizione	KeyFieldsIDContrattoNazSize(Lookup	   	TADOQueryQTipiContrattiActive	
ConnectionData.DB
CursorTypectStatic	AfterPostQTipiContrattiAfterPostBeforeDeleteQTipiContrattiBeforeDelete
Parameters SQL.Stringsselect * from TipiContratti Left� Top�  TAutoIncFieldQTipiContrattiID	FieldNameIDReadOnly	  TStringFieldQTipiContrattiTipoContratto	FieldNameTipoContratto  TIntegerFieldQTipiContrattiIDAzienda	FieldName	IDAzienda   	TADOQueryQContrattiNazActive	
ConnectionData.DB
CursorTypectStatic	AfterPostQContrattiNazAfterPostBeforeDeleteQContrattiNazBeforeDelete
Parameters SQL.Stringsselect * from ContrattiNaz Left� Top�  TAutoIncFieldQContrattiNazID	FieldNameIDReadOnly	  TStringFieldQContrattiNazDescrizione	FieldNameDescrizioneSize(   TDataSource	DsQQualifDataSetQQualifOnStateChangeDsQQualifStateChangeLeft,Top�   TDataSourceDsQTipiContrattiDataSetQTipiContrattiOnStateChangeDsQTipiContrattiStateChangeLeft� Top�   TDataSourceDsQContrattiNazDataSetQContrattiNazOnStateChangeDsQContrattiNazStateChangeLeft� Top�   	TADOQueryQTipiContrattiLKActive	
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect * from TipiContratti LeftTop�  TAutoIncFieldQTipiContrattiLKID	FieldNameIDReadOnly	  TStringFieldQTipiContrattiLKTipoContratto	FieldNameTipoContratto  TIntegerFieldQTipiContrattiLKIDAzienda	FieldName	IDAzienda   	TADOQueryQContrattiNazLKActive	
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect * from ContrattiNaz Left<Top�  TAutoIncFieldQContrattiNazLKID	FieldNameIDReadOnly	  TStringFieldQContrattiNazLKDescrizione	FieldNameDescrizioneSize(    