�
 TVALUTAZCOLLOQUIOFORM 0�  TPF0TValutazColloquioFormValutazColloquioFormLeft� TopwWidthHeightMCaption Griglia di valutazione candidatoColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnClose	FormCloseOnShowFormShowPixelsPerInch`
TextHeight TPanelPanel1LeftTopWidth�Height/AnchorsakLeftakTopakRight 
BevelOuter	bvLoweredEnabledTabOrder  TLabelLabel1LeftTopWidth0HeightCaption	Candidato  TEditECognomeLeftTopWidth� HeightColorclYellowFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder TextECognome  TEditENomeLeft� TopWidth� HeightColorclYellowTabOrderTextENome   TBitBtnBitBtn1Left�TopWidthVHeight!AnchorsakTopakRight TabOrderOnClickBitBtn1ClickKindbkOK  TAdvStringGridASG1LeftTop8Width�Height�AnchorsakLeftakTopakRightakBottom ColCountDefaultRowHeightOptionsgoFixedVertLinegoFixedHorzLine
goVertLine
goHorzLinegoRangeSelectgoColSizing	goEditing 
ScrollBarsssHorizontalTabOrderAutoNumAlignAutoSize
VAlignment	vtaCenterEnhTextSizeEnhRowColMoveSortFixedColsSizeWithFormMultilinecellsOnGetAlignmentASG1GetAlignmentOnRightClickCellASG1RightClickCellSortDirectionsdAscendingOnCheckBoxClickASG1CheckBoxClickSortFull	SortAutoFormat	SortShowEnableGraphics	
SortColumn 	HintColorclYellowSelectionColorclHighlightSelectionTextColorclHighlightTextSelectionRectangleSelectionRTFKeepHintShowCellsOleDropTargetOleDropSource
OleDropRTFPrintSettings.FooterSize PrintSettings.HeaderSize PrintSettings.TimeppNonePrintSettings.DateppNonePrintSettings.DateFormat
dd/mm/yyyyPrintSettings.PageNrppNonePrintSettings.TitleppNonePrintSettings.Font.CharsetDEFAULT_CHARSETPrintSettings.Font.ColorclWindowTextPrintSettings.Font.Height�PrintSettings.Font.NameMS Sans SerifPrintSettings.Font.Style  PrintSettings.HeaderFont.CharsetDEFAULT_CHARSETPrintSettings.HeaderFont.ColorclWindowTextPrintSettings.HeaderFont.Height�PrintSettings.HeaderFont.NameMS Sans SerifPrintSettings.HeaderFont.Style  PrintSettings.FooterFont.CharsetDEFAULT_CHARSETPrintSettings.FooterFont.ColorclWindowTextPrintSettings.FooterFont.Height�PrintSettings.FooterFont.NameMS Sans SerifPrintSettings.FooterFont.Style PrintSettings.Borders
pbNoborderPrintSettings.BorderStylepsSolidPrintSettings.CenteredPrintSettings.RepeatFixedRowsPrintSettings.RepeatFixedColsPrintSettings.LeftSize PrintSettings.RightSize PrintSettings.ColumnSpacing PrintSettings.RowSpacing PrintSettings.TitleSpacing PrintSettings.Orientation
poPortraitPrintSettings.PagePrefixpagePrintSettings.FixedWidth PrintSettings.FixedHeight PrintSettings.UseFixedHeightPrintSettings.UseFixedWidthPrintSettings.FitToPagefpNeverPrintSettings.PageNumSep/PrintSettings.NoAutoSizePrintSettings.PrintGraphicsHTMLSettings.WidthdNavigation.AllowInsertRowNavigation.AllowDeleteRowNavigation.AdvanceOnEnterNavigation.AdvanceInsertNavigation.AutoGotoWhenSortedNavigation.AutoGotoIncrementalNavigation.AutoComboDropSizeNavigation.AdvanceDirectionadLeftRight"Navigation.AllowClipboardShortCutsNavigation.AllowSmartClipboardNavigation.AllowRTFClipboardNavigation.AdvanceAutoNavigation.InsertPositionpInsertBeforeNavigation.CursorWalkEditorNavigation.MoveRowOnSortNavigation.ImproveMaskSelNavigation.AlwaysEditColumnSize.SaveColumnSize.StretchColumnSize.Location
clRegistryCellNode.ColorclSilverCellNode.NodeTypecnFlatCellNode.NodeColorclBlackSizeWhileTyping.HeightSizeWhileTyping.WidthMouseActions.AllSelectMouseActions.ColSelectMouseActions.RowSelectMouseActions.DirectEdit	MouseActions.DisjunctRowSelectMouseActions.AllColumnSizeMouseActions.CaretPositioning
IntelliPan
ipVerticalURLColorclBlackURLShowURLFullURLEdit
ScrollTypessNormalScrollColorclNoneScrollWidthScrollProportionalScrollHintsshNone
OemConvertFixedFooters FixedRightCols FixedColWidth� FixedRowHeightFixedFont.CharsetDEFAULT_CHARSETFixedFont.ColorclWindowTextFixedFont.Height�FixedFont.NameMS Sans SerifFixedFont.Style FixedAsButtonsFloatFormat%.2fWordWrapColumnHeaders.StringssoggettoEC LookupLookupCaseSensitiveLookupHistoryHideFocusRectBackGround.Top BackGround.Left BackGround.DisplaybdTileHoveringFilter FilterActive	ColWidths�  
RowHeights   TPanelPanel2LeftTopWidth�Height!AnchorsakLeftakRightakBottom 
BevelOuter	bvLoweredTabOrder TLabelLabel2LeftETop	Width� HeightAnchorsakTopakRight CaptionPunteggio accumulato:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel3LeftTop	Width�HeightCaptionYfare click con il pulsante destro per visualizzare la descrizione dettagliata (se esiste)Font.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TEditETotLeft�TopWidth)HeightAnchorsakTopakRight Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder    TBitBtn	BInfoCandLeftNTopWidthVHeight!Caption
Info cand.TabOrderVisibleOnClickBInfoCandClick
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 33333333?�������        wwwwwwww�������?��?�?� �  �w7swsw7�������?��3��� ��  �w7s7ww��������3337����������?��7�7�  �����ww�7�77  �����ww37337���������������������wwwwwwww�����Ȁs7wwwws7        wwwwwwww33333333333333333333333333333333	NumGlyphs  TBitBtnBResetLeft�TopWidth[Height!CaptionReset grigliaFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderOnClickBResetClick
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 30    3337wwww�330�wwp3337�����330���p3337�����330��pp3337�����330���p3337�����330��pp3337�����330���p333�������00��pp0377�����33 ���p33w����s330��pp3337�����330pppp3337�����33     33wwwww33��ww33����33     33wwwwws3330wp333337���33330  333337ww333	NumGlyphs  TADOLinkedQueryQGriglia
ConnectionData.DB
Parameters OriginalSQL.Strings(select ValutazModel.*,AnagValutaz.Valorefrom AnagValutaz,ValutazModel+where ValutazModel.ID *= AnagValutaz.IDVoceand IDAnagrafica=:xIDAnag:order by ValutazModel.ID 	UseFilterLeft� Top�   TADOLinkedQueryQ
ConnectionData.DB
Parameters 	UseFilterLeft� Top@  TADOLinkedQueryQUpd
ConnectionData.DB
Parameters 	UseFilterLeft� Top   