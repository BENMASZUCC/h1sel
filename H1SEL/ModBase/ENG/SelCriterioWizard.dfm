�
 TSELCRITERIOWIZARDFORM 0�-  TPF0TSelCriterioWizardFormSelCriterioWizardFormLeftMTop� BorderStylebsDialogCaption0Wizard di creazione/modifica criterio di ricercaClientHeight\ClientWidth_Color	clBtnFaceFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style OldCreateOrderPositionpoScreenCenterOnCreate
FormCreateOnShowFormShowPixelsPerInch`
TextHeight TPanelPanSituazioneLeft Top Width_Height<AlignalTop
BevelOuter	bvLoweredTabOrder  TLabelLabel1LeftTopWidth&HeightCaptionTabella:  TDBTextDBText1LeftFTopWidth� Height	DataFieldDescTabella
DataSource
DsQTabelleFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.StylefsBold 
ParentFont  TLabelLabel2LeftTopWidth%HeightCaptionCampo:  TDBTextDBText2LeftFTopWidth� Height	DataField	DescCampo
DataSourceDsQCampiFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.StylefsBold 
ParentFont  TLabelLabel3LeftTop&Width6HeightCaption
Operatore:  TDBTextDBText3LeftFTop&Width� Height	DataFieldDescOperatore
DataSourceDsQOperatoriFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.StylefsBold 
ParentFont   TPanel
PanButtonsLeft Top.Width_Height.AlignalBottom
BevelOuter	bvLoweredTabOrder TBitBtn	BIndietroLeftbTopWidthNHeight"Caption
< IndietroTabOrder OnClickBIndietroClick  TBitBtnBAvantiLeft� TopWidthNHeight"CaptionAvanti >TabOrderOnClickBAvantiClick  TBitBtnBitBtn1LeftTopWidthNHeight"CaptionAnnullaTabOrderOnClickBIndietroClickKindbkCancel  TBitBtnBFineLeft
TopWidthNHeight"CaptionFineTabOrderKindbkOK   TPageControlPCUnicoLeft Top<Width_Height� 
ActivePage	TsTabellaAlignalClientTabOrderOnChangePCUnicoChange 	TTabSheet	TsTabellaCaption	TsTabella TPanelPanel10Left Top WidthWHeightAlignalTop	AlignmenttaLeftJustifyCaption"  Seleziona la tabella dall'elencoColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TLabelLTotLeft�TopWidth%HeightAnchorsakTopakRight AutoSizeCaptionLTotFont.CharsetDEFAULT_CHARSET
Font.ColorclYellowFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontVisible  TLabelLabel5Left�TopWidthEHeight	AlignmenttaRightJustifyAnchorsakTopakRight AutoSizeCaptionTotale query:Font.CharsetDEFAULT_CHARSET
Font.ColorclYellowFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont   	TdxDBGrid
DBGTabelleLeft TopWidthWHeight� Bands  DefaultLayout	HeaderPanelRowCountKeyFieldDescTabellaSummaryGroups SummarySeparator, AlignalClientColorclInfoBkTabOrder
DataSource
DsQTabelleFilter.Criteria
       LookAndFeellfFlatOptionsBehavioredgoAutoSearchedgoAutoSortedgoDragScrolledgoEnterShowEditoredgoImmediateEditoredgoTabThroughedgoVertThrough 	OptionsDBedgoCancelOnExitedgoCanDeleteedgoCanInsertedgoCanNavigationedgoConfirmDeleteedgoLoadAllRecordsedgoUseBookmarks OptionsViewedgoAutoWidthedgoBandHeaderWidthedgoIndicatoredgoUseBitmap  TdxDBGridMaskColumnDBGTabelleDescTabellaCaptionTabella	BandIndex RowIndex 	FieldNameDescTabella    	TTabSheetTSCampoCaptionTSCampo
ImageIndex 	TdxDBGridDBGCampiLeft TopWidthWHeight� Bands  DefaultLayout	HeaderPanelRowCountKeyFieldIDSummaryGroups SummarySeparator, AlignalClientColorclInfoBkTabOrder 
DataSourceDsQCampiFilter.Criteria
       LookAndFeellfFlatOptionsBehavioredgoAutoSearchedgoAutoSortedgoDragScrolledgoEnterShowEditoredgoImmediateEditoredgoTabThroughedgoVertThrough 	OptionsDBedgoCancelOnExitedgoCanDeleteedgoCanInsertedgoCanNavigationedgoConfirmDeleteedgoLoadAllRecordsedgoUseBookmarks OptionsViewedgoAutoWidthedgoBandHeaderWidthedgoIndicatoredgoUseBitmap  TdxDBGridMaskColumnDBGCampiDescCampoCaptionCampo	BandIndex RowIndex 	FieldName	DescCampo   TPanelPanel1Left Top WidthWHeightAlignalTop	AlignmenttaLeftJustifyCaption.  Seleziona il campo della tabella dall'elencoColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder TLabelLabel4Left�TopWidth%HeightAnchorsakTopakRight AutoSizeCaptionLTotFont.CharsetDEFAULT_CHARSET
Font.ColorclYellowFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontVisible  TLabelLabel6Left�TopWidthEHeight	AlignmenttaRightJustifyAnchorsakTopakRight AutoSizeCaptionTotale query:Font.CharsetDEFAULT_CHARSET
Font.ColorclYellowFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont    	TTabSheetTsOperatoreCaptionTsOperatore
ImageIndex 	TdxDBGridDBGOperatoriLeft TopWidthWHeight� Bands  DefaultLayout	HeaderPanelRowCountKeyField
IDTabQuerySummaryGroups SummarySeparator, AlignalClientColorclInfoBkTabOrder 
DataSourceDsQOperatoriFilter.Criteria
       LookAndFeellfFlatOptionsBehavioredgoAutoSearchedgoAutoSortedgoDragScrolledgoEnterShowEditoredgoImmediateEditoredgoTabThroughedgoVertThrough 	OptionsDBedgoCancelOnExitedgoCanDeleteedgoCanInsertedgoCanNavigationedgoConfirmDeleteedgoLoadAllRecordsedgoUseBookmarks OptionsViewedgoAutoWidthedgoBandHeaderWidthedgoIndicatoredgoUseBitmap  TdxDBGridMaskColumnDBGOperatoriDescOperatoreCaption	OperatoreSortedcsUp	BandIndex RowIndex 	FieldNameDescOperatore   TPanelPanel2Left Top WidthWHeightAlignalTop	AlignmenttaLeftJustifyCaption0  Seleziona l'operatore da applicare dall'elencoColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder TLabelLabel7Left�TopWidth%HeightAnchorsakTopakRight AutoSizeCaptionLTotFont.CharsetDEFAULT_CHARSET
Font.ColorclYellowFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontVisible  TLabelLabel8Left�TopWidthEHeight	AlignmenttaRightJustifyAnchorsakTopakRight AutoSizeCaptionTotale query:Font.CharsetDEFAULT_CHARSET
Font.ColorclYellowFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont    	TTabSheetTSValoreCaptionTSValore
ImageIndex TPanelPanel3Left Top WidthWHeightAlignalTop	AlignmenttaLeftJustifyCaption  Valore da cercareColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TLabelLabel9Left�TopWidth%HeightAnchorsakTopakRight AutoSizeCaptionLTotFont.CharsetDEFAULT_CHARSET
Font.ColorclYellowFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontVisible  TLabelLabel10Left�TopWidthEHeight	AlignmenttaRightJustifyAnchorsakTopakRight AutoSizeCaptionTotale query:Font.CharsetDEFAULT_CHARSET
Font.ColorclYellowFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont   TdxPickEditdxPickEdit1Left
Top WidthIColorclInfoBkTabOrderStyleControllerdxEditStyleController1  TdxDateEditdxDateEdit1Left
Top9WidthkColorclInfoBkTabOrderStyleControllerdxEditStyleController1Date�Q��  TdxEditdxEdit1Left
TopRWidth� ColorclInfoBkTabOrderStyleControllerdxEditStyleController1  TdxSpinEditdxSpinEdit1Left
ToplWidthAColorclInfoBkTabOrderStyleControllerdxEditStyleController1  TPanelPanGridLeftqTop!Width� Height� 
BevelOuterbvNoneTabOrder 	TdxDBGrid	dxDBGrid1Left TopWidth� Height{Bands  DefaultLayout	HeaderPanelRowCountKeyFieldIDShowSummaryFooter	SummaryGroups SummarySeparator, AlignalClientColorclInfoBkTabOrder 
DataSourceDsQTableFilter.Active	Filter.Criteria
       LookAndFeellfFlatOptionsBehavioredgoAutoSearchedgoAutoSortedgoDragScrolledgoEnterShowEditoredgoImmediateEditoredgoMultiSelectedgoTabThroughedgoVertThrough 	OptionsDBedgoCancelOnExitedgoCanDeleteedgoCanInsertedgoCanNavigationedgoConfirmDeleteedgoLoadAllRecordsedgoUseBookmarks OptionsViewedgoAutoWidthedgoBandHeaderWidthedgoIndicatoredgoUseBitmap  TdxDBGridMaskColumndxDBGrid1DescSortedcsUp	BandIndex RowIndex 	FieldNameDescrizioneSummaryFooterTypecstCountSummaryFooterFieldID  TdxDBGridColumndxDBGrid1Desc2Visible	BandIndex RowIndex    TPanelPanel4Left Top Width� HeightAlignalTop
BevelOuter	bvLoweredTabOrder TLabelLabel11LeftTopWidthUHeightCaptionTesto da cercare:  TSpeedButtonSpeedButton1Left� TopWidthHeightAnchorsakTopakRight 
Glyph.Data
:  6  BM6      6  (                  �  �            ��� xu� ��� z� lfn ֘� ��� �@y �W� ׎� cab �Sm �r� �e} �k} �|� ȋ� ��� ��� �im �ff �jj �nn �rr �kk �qq �vv �oo �vv ��� �]] ��� ��� ��� xWW �ss �]] �`` ��� ��� ǔ� Ɠ� ˘� ɖ� ��� ��� aOO Ǥ� uaa l\\ ��� tss ��� �~ ��� �ur �ro �|y ��� ��{ �|w ��� ɚ� ̞� ��� Ѥ� � ��� Ԥ� Ę� Û� մ� ޯ� ʥ� ڳ� ợ Ь� ȧ� 緕 � �à �ɱ �Λ �ў �Ҡ �ѥ �Ħ �ԡ �բ �֣ �ӣ �ة �٪ �ӧ �ҩ �ͨ ��� �פ �٦ �ե �ګ �ݰ �ۨ �ܩ �ݪ �ݬ �� �ޫ �߬ �� �� �� �� �� �� �� �� �֮ �� �� �� �� ��� ��� �� �� �� �� ��� ��� �� �� �� �� �� �� ��� �� ��� ��� �� ��� �� ��� ��� �� �� ��� ��� �� �� ��� ��� ��� �� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ���     a�� Q�� U�� Y�� o�� e�� B�� O�� ��� 6�� @�� dhm gkp @}� dfj ��� Ij� Nj� Rm� _m� ��� ��� ��� ��� {{{ kkk ^^^                                                          /��            /��           ����          �����          ���-��          ����      �����FMRH4      ��-_���ő      M`������1�     H�x������G     M�i������M     H�u����öM     H�|o�����D�     K���ms�_$       H�����VA         HHJLK�   OnClickSpeedButton1Click  TEditECercaLeft_TopWidthXHeightAnchorsakLeftakTopakRight TabOrder       	TADOQueryQTabelle
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Strings#select distinct Tabella,DescTabellafrom TabQuery order by desctabella LeftTop�   TDataSource
DsQTabelleDataSetQTabelleLeftTop	  TDataSourceDsQCampiDataSetQCampiLeft<Top	  	TADOQueryQCampi
ConnectionData.DB
CursorTypectStatic
BeforeOpenQCampiBeforeOpen
ParametersNamexTabella
Attributes
paNullable DataTypeftStringNumericScale� 	Precision� SizeValue   SQL.Stringsselect * from TabQuerywhere DescTabella=:xTabellaorder by DescCampo Left<Top�   	TADOQuery
QOperatori
ConnectionData.DB
CursorTypectStatic
BeforeOpenQOperatoriBeforeOpen	AfterOpenQOperatoriAfterOpen
ParametersNamexID
AttributespaSigned DataType	ftInteger	Precision
SizeValue   SQL.Stringsselect * from TabQueryOperatoriwhere IDTabQuery=:xIDorder by DescOperatore LefttTop�   TDataSourceDsQOperatoriDataSet
QOperatoriLefttTop	  TdxEditStyleControllerdxEditStyleController1ButtonStylebtsFlatHotTrack	Shadow	Left� Top�   	TADOQueryQTable
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect * from Diplomi Left� Top�   TDataSourceDsQTableDataSetQTableLeft� Top   