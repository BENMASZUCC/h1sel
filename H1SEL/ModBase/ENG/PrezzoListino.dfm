�
 TPREZZOLISTINOFORM 0�  TPF0TPrezzoListinoFormPrezzoListinoFormLeft�Top� ActiveControl	GroupBox1BorderStylebsDialogCaptionPrezzo a listino per annuncioClientHeight� ClientWidth\Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TLabelLabel3Left
TopfWidthLHeightCaptionPrezzo di Listino  TLabelLabel4LeftTop~Width=HeightCaptionPrezzo a noi:  TLabelLabel5LeftTop� WidthPHeightCaptionPrezzo al cliente:  TLabelLabel6Left� TopNWidthHeightCaptionLireFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsItalic 
ParentFontVisible  TLabelLabel7LeftBTopNWidthHeightCaptionEuroFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsItalic 
ParentFont  TBevelBevel1Left`Top\WidthzHeightShape	bsTopLineVisible  TBevelBevel2Left� Top\WidthzHeightShape	bsTopLine  TLabelLabel8LeftTop=Width3HeightCaptionDalla data:  TLabelLabel9LeftTop� WidthHeightCaptionNote:  TBitBtnBitBtn1Left� TopWidth\Height$TabOrder	KindbkOK  TBitBtnBitBtn2Left� Top'Width\Height$CaptionAnnullaTabOrder
KindbkCancel  TRxCalcEditListinoLireLeft`TopbWidth{HeightAutoSizeDisplayFormat�'.' ,0.FormatOnEditing		NumGlyphsTabOrderVisibleOnChangeListinoLireChange  TRxCalcEditAnoiLireLeft`TopzWidth{HeightAutoSizeDisplayFormat�'.' ,0.FormatOnEditing		NumGlyphsTabOrderVisibleOnChangeAnoiLireChange  TRxCalcEditAlClienteLireLeft`Top� Width{HeightAutoSizeDisplayFormat�'.' ,0.FormatOnEditing		NumGlyphsTabOrderVisibleOnChangeAlClienteLireChange  TRxCalcEditListinoEuroLeft� TopbWidth{HeightAutoSizeDisplayFormat
�'.' ,0.00FormatOnEditing		NumGlyphsTabOrderOnChangeListinoEuroChange  TRxCalcEditAnoiEuroLeft� TopzWidth{HeightAutoSizeDisplayFormat
�'.' ,0.00FormatOnEditing		NumGlyphsTabOrderOnChangeAnoiEuroChange  TRxCalcEditAlClienteEuroLeft� Top� Width{HeightAutoSizeDisplayFormat
�'.' ,0.00FormatOnEditing		NumGlyphsTabOrderOnChangeAlClienteEuroChange  	TGroupBox	GroupBox1LeftTopWidth� Height+CaptionQuantit�TabOrder  TLabelLabel1LeftTopWidthHeightCaptionDa:  TLabelLabel2LeftUTopWidth
HeightCaptionA:  TRxSpinEditRxSpinEdit1LeftTopWidth/HeightTabOrder   TRxSpinEditRxSpinEdit2LeftdTopWidth/HeightTabOrder   TDateEdit97DEDallaDataLeft:Top9WidthcHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderColorCalendar.ColorValidclBlueDayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanPopup
doIsMaskeddoShowCanceldoShowToday   TEditENoteLeftTop� WidthVHeightTabOrder   