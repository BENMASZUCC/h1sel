�
 TSCELTAMODELLOMAILCLIFORM 0U  TPF0TSceltaModelloMailCliFormSceltaModelloMailCliFormLeftTopkBorderStylebsDialogCaption!Scelta modello di mail da inviareClientHeightPClientWidthColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TSpeedButtonSpeedButton1LeftTTop� WidthHeight
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 333     333wwwww333����?���??� 0  � �w�ww?sw7������ws3?33�࿿ ���w�3ws��7�������w�3?��7࿿  �w�3wwss7��������w�?���37�   ��w�wwws?� � �� �ws�w73w730 ���37wss3?�330���  33773�ww33��33s7s730���37�33s3	�� 33ws��w3303   3373wwws3	NumGlyphsVisibleOnClickSpeedButton1Click  TBitBtnBitBtn1Left�TopWidthaHeight$TabOrder KindbkOK  TBitBtnBitBtn2Left�Top(WidthaHeight$CaptionAnnullaTabOrderKindbkCancel  	TGroupBox	GroupBox1LeftTop� WidthCHeight}AnchorsakTop Caption"Parametri di configurazione e-mailEnabledFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderVisible TLabelLabel2LeftTopWidthcHeightCaptionHost (server SMTP): Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel3LeftTopWidthHeightCaptionPort: Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel10LeftTop-WidthnHeightCaptionUser ID (nome utente): Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel11Left,TopMWidthGHeightCaptionNome mittente:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TBevelBevel1LeftxTopWidthHeighteShape
bsLeftLine  TLabelLabel12Left!TopeWidthTHeightCaptionIndirizzo mittente: Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TEditEHostLeft� TopWidth~HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder Text192.168.0.155  TEditEPortLeftTopWidth!HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderText25  TEditEUserIDLeft� Top)Width� HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderTextsabattin#ebcconsulting.com  TEditENameLeftTopIWidth� HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderTextAndrea S  TEditEAddressLeftTopaWidth� HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderTextsabattini@ebcconsulting.com   TPanelPanel13LeftTopWidth�Height3
BevelInnerbvRaised
BevelOuter	bvLoweredTabOrder TSpeedButton	BModTestiLeft�TopWidthHeightHintmodifica testi
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 333     333wwwww333����?���??� 0  � �w�ww?sw7������ws3?33�࿿ ���w�3ws��7�������w�3?��7࿿  �w�3wwss7��������w�?���37�   ��w�wwws?� � �� �ws�w73w730 ���37wss3?�330���  33773�ww33��33s7s730���37�33s3	�� 33ws��w3303   3373wwws3	NumGlyphsParentShowHintShowHint	VisibleOnClickBModTestiClick  TDBGridDBGrid2LeftTopWidth~HeightAnchorsakLeftakTopakBottom 
DataSourceDsQTipiMailContattiCliOptionsdgTitlesdgIndicatordgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgConfirmDeletedgCancelOnExit ReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameDescrizioneWidth� Visible	 Expanded	FieldNameOggettoWidth� Visible	    TPanelPanel14LeftTopWidth~Height	AlignmenttaLeftJustifyCaption  Tipi di mail disponibiliColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   	TCheckBoxCBStoricoInvioLeftTop;Width.HeightCaption+Registra nello storico invii per il clienteChecked	State	cbCheckedTabOrder  TDataSourceDsQTipiMailContattiCliDataSetQTipiMailContattiCliLeft\Topt  TADOLinkedQueryQTipiMailContattiCliActive	
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Strings!select * from TipiMailContattiCli 	UseFilterLeft[TopT  TADOLinkedQueryQ
ConnectionData.DB
Parameters 	UseFilterLeft�Top�    