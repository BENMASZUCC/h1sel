�
 TRIFCLIENTEDATIFORM 0�  TPF0TRifClienteDatiFormRifClienteDatiFormLeft>TopiActiveControl
ETitStudioBorderStylebsDialogCaption(Riferimento interno cliente - altri datiClientHeight�ClientWidthDColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TLabelLabel4Left+Top5WidthGHeightCaptionTitolo di studio:  TLabelLabel5LeftTopvWidthQHeightCaptionCarica aziendale:  TLabelLabel6LeftTop� WidthPHeightCaptionNome segretaria:  TLabelLabel11LeftTop�WidthyHeightCaptionUtente che lo ha inserito :  TLabelLabel12LeftTop�WidthRHeightCaptionData inserimento:  TLabelLabel13LeftTop5WidthHeightCaptionSesso  TLabelLabel15LeftTop� Width^HeightCaptionDivisione aziendale:  TBitBtnBitBtn1Left� TopWidth]Height#TabOrderKindbkOK  TBitBtnBitBtn2Left� Top&Width]Height#CaptionAnnullaTabOrderKindbkCancel  TPanelPanel1LeftTopWidth� Height-
BevelOuter	bvLoweredEnabledTabOrder	 TLabelLabel1LeftTopWidth8HeightCaptionNominativo:  TEditENominativoLeftTopWidth� HeightTabStopColorclAquaTabOrder    	TGroupBox	GroupBox1LeftTop Width� Height-CaptionCompleanno:TabOrder TLabelLabel2LeftTopWidth"HeightCaptionGiorno:  TLabelLabel3Left_TopWidthHeightCaptionMese:  	TSpinEditSEGiornoLeft.TopWidth)HeightMaxValueMinValue TabOrder Value   	TSpinEditSEMeseLeft� TopWidth&HeightMaxValueMinValue TabOrderValue    TPanelPanel2LeftTop� Width� Height	AlignmenttaLeftJustifyCaption  Dati personaliColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder
  TPanelPanel3LeftTop_Width� Height	AlignmenttaLeftJustifyCaption  Dati aziendaliColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TEdit
ETitStudioLeft+TopDWidth� Height	MaxLengthTabOrder  TEdit	ECaricaAzLeftTop� Width� Height	MaxLength(TabOrder  TEditENomeSegLeftTop� Width� Height	MaxLength(TabOrder  	TGroupBox	GroupBox2LeftTop0Width� HeightlCaptionResidenza privataTabOrder TLabelLabel7LeftTopWidth&HeightCaption	Indirizzo  TLabelLabel8LeftTop<WidthHeightCaptionCap  TLabelLabel10LeftPTop;WidthHeightCaptionProv.  TLabelLabel9LeftTopTWidth-HeightCaption	Telefono:  TLabelLabel14LeftTopWidth5HeightCaptionTipo strada  TLabelLabel93Left?TopWidth&HeightCaption	Indirizzo  TLabelLabel99Left� TopWidthHeightCaptionn� civ.  TLabelLabel16Left� Top;Width'HeightCaptionNazione  TEdit
EIndirizzoLeft?Top WidthqHeight	MaxLength2TabOrder  TEditECapLeft!Top7Width)Height	MaxLengthTabOrder  TEditEProvLeftkTop8WidthHeightCharCaseecUpperCase	MaxLengthTabOrder  TEditETelAbLeft:TopPWidth� Height	MaxLength(TabOrder  	TComboBoxCBTipoStradaResLeftTop Width6Height
ItemHeightTabOrder 
OnDropDownCBTipoStradaResDropDown  TEdit
ENumCivicoLeft� Top Width$HeightTabOrder  TEditENazioneLeft� Top8WidthHeightCharCaseecUpperCase	MaxLengthTabOrder   TPanelPanel4LeftTop�Width� Height	AlignmenttaLeftJustifyCaption  Nostri dati:ColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TEditEUtenteLeft|Top�WidthfHeightTabStopEnabledTabOrder  	TComboBoxCBSessoLeftTopDWidth%Height
ItemHeightTabOrder Items.StringsMF   TEditEDivisioneAzLeftTop� Width� Height	MaxLength(TabOrder  TdxDateEdit	DEDataInsLeft|Top�WidthgTabOrderDate�Q��  TQueryQAltriDati_OLDDatabaseNameEBCDBSQL.Strings.select EBC_ContattiClienti.*, Users.Nominativo-from EBC_ContattiClienti,Users with (updlock).where EBC_ContattiClienti.IDUtente *= Users.ID!  and EBC_ContattiClienti.ID=:xID Left� Top	ParamDataDataType	ftUnknownNamexID	ParamType	ptUnknown    TADOLinkedQuery
QAltriDati
ConnectionData.DB
ParametersNamexID:
AttributespaSigned DataType	ftInteger	Precision
SizeValue   SQL.Strings.select EBC_ContattiClienti.*, Users.Nominativofrom EBC_ContattiClienti,Users.where EBC_ContattiClienti.IDUtente *= Users.ID"  and EBC_ContattiClienti.ID=:xID:  Left� Top,   