�
 TSCELTAANAGFILEFORM 0�  TPF0TSceltaAnagFileFormSceltaAnagFileFormLeft� Top� BorderStylebsDialogCaptionApertura file candidatoClientHeight� ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterPixelsPerInch`
TextHeight TLabelLabel1LeftTopWidth�HeightCaption�Per questo soggetto non � stato trovato il file GIF scannerizzato.
Scegliere tra gli altri tipi di file a lui associati e premere OK, oppure annulla per uscire.Font.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel2LeftTopWidthHeightCaption=L'elenco � in ordine descrescente rispetto alla data del file  TBitBtnBitBtn1Left�TopWidthbHeight&TabOrder KindbkOK  TBitBtnBitBtn2Left`TopWidthbHeight&CaptionAnnullaTabOrderKindbkCancel  	TRxDBGrid	RxDBGrid1LeftTop/Width�Height� 
DataSourceDsQAnagFileOptionsdgTitlesdgIndicatordgColumnResize
dgColLines
dgRowLinesdgRowSelectdgConfirmDeletedgCancelOnExit ReadOnly	TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameTipoWidth}Visible	 Expanded	FieldNameDescrizioneWidthiVisible	 Expanded	FieldNameNomeFileTitle.Caption	Nome fileWidthpVisible	 Expanded	FieldNameDataCreazioneTitle.CaptionData del fileWidthDVisible	    TDataSourceDsQAnagFileDataSet	QAnagFileLeft8Top�   TADOLinkedQuery	QAnagFile
ConnectionData.DB
ParametersNamexIDAnag:
AttributespaSigned
paNullable DataType	ftInteger	Precision
SizeValue   SQL.Strings#select TipiFileCand.Tipo,AnagFile.*from AnagFile,TipiFileCand(where AnagFile.IDTipo *= TipiFileCand.ID#and AnagFile.IDAnagrafica=:xIDAnag:order by DataCreazione desc OriginalSQL.Strings#select TipiFileCand.Tipo,AnagFile.*from AnagFile,TipiFileCand'where AnagFile.IDTipo = TipiFileCand.ID#and AnagFile.IDAnagrafica=:xIDAnag:order by DataCreazione desc 	UseFilterLeft8Top`   