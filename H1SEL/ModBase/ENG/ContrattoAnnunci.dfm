�
 TCONTRATTOANNUNCIFORM 0�  TPF0TContrattoAnnunciFormContrattoAnnunciFormLeft�ToplActiveControlDBGrid1BorderStylebsDialogCaption*Contratto con testata-edizione per annunciClientHeight[ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TLabelLabel1LeftTop� Width8HeightCaptionData stipula  TLabelLabel2LeftTop� WidthHHeightCaptionData scadenza  TLabelLabel3LeftTopWidth?HeightCaptionTotale moduli  TLabelLabel4LeftTopWidthFHeightCaptionModuli utilizzati  TLabelLabel5Left
TopEWidthAHeightCaptionCosto Singolo  TLabelLabel6Left� Top.WidthHeightCaptionLireFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsItalic 
ParentFont  TBevelBevel1LeftSTop<WidthzHeightShape	bsTopLine  TBevelBevel2Left� Top<WidthzHeightShape	bsTopLine  TLabelLabel7Left5Top.WidthHeightCaptionEuroFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsItalic 
ParentFont  TBitBtnBitBtn1LeftQTopWidth]Height&TabOrderKindbkOK  TBitBtnBitBtn2LeftQTop(Width]Height&CaptionAnnullaTabOrderKindbkCancel  TDBGridDBGrid1LeftTopWidthJHeight� 
DataSourceDsQTestEdizOptionsdgTitlesdgIndicatordgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgConfirmDeletedgCancelOnExit ReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameTestataWidth� Visible	 Expanded	FieldNameEdizioneWidth� Visible	    TDateEdit97DEDataStipulaLeftTTop� WidtheHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderColorCalendar.ColorValidclBlueDayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanPopup
doIsMaskeddoShowCanceldoShowToday   TDateEdit97DEDataScadenzaLeftTTop� WidthyHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderColorCalendar.ColorValidclBlueDayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanClear
doCanPopup
doIsMaskeddoShowCanceldoShowToday   TRxSpinEdit	TotModuliLeftTTopWidth>HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder  TRxSpinEditModuliUtilizzatiLeftTTopWidth>HeightTabOrder  TRxCalcEdit	CostoLireLeftTTopAWidthyHeightAutoSizeDisplayFormat�'.' ,0.	NumGlyphsTabOrderOnChangeCostoLireChange  TRxCalcEdit	CostoEuroLeft� TopAWidthyHeightTabStopAutoSizeDisplayFormat
�'.' ,0.00	NumGlyphsTabOrderOnChangeCostoEuroChange  TQueryQTestEdiz_OLDDatabaseNameEBCDBSQL.Strings,select IDTestata,Ann_Edizioni.ID IDEdizione,B          Ann_Testate.Denominazione Testata, NomeEdizione Edizionefrom Ann_Edizioni,Ann_Testate+where Ann_Edizioni.IDTestata=Ann_Testate.ID/order by Ann_Testate.Denominazione,NomeEdizione Left Top8 TIntegerFieldQTestEdiz_OLDIDTestata	FieldName	IDTestataOriginEBCDB.Ann_Edizioni.IDTestata  TAutoIncFieldQTestEdiz_OLDIDEdizione	FieldName
IDEdizioneOriginEBCDB.Ann_Edizioni.ID  TStringFieldQTestEdiz_OLDTestata	FieldNameTestataOriginEBCDB.Ann_Testate.Denominazione	FixedChar	Size2  TStringFieldQTestEdiz_OLDEdizione	FieldNameEdizioneOriginEBCDB.Ann_Edizioni.NomeEdizione	FixedChar	Size2   TDataSourceDsQTestEdizDataSet	QTestEdizLeft TopX  TADOLinkedQuery	QTestEdizActive	
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Strings,select IDTestata,Ann_Edizioni.ID IDEdizione,B          Ann_Testate.Denominazione Testata, NomeEdizione Edizionefrom Ann_Edizioni,Ann_Testate+where Ann_Edizioni.IDTestata=Ann_Testate.ID/order by Ann_Testate.Denominazione,NomeEdizione  Left@Top8   