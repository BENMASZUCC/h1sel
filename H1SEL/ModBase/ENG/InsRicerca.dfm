�
 TINSRICERCAFORM 0p  TPF0TInsRicercaFormInsRicercaFormLeftmTop� BorderStylebsDialogCaptionNuova commessa/ricercaClientHeighthClientWidthQColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnCreate
FormCreateOnShowFormShowPixelsPerInch`
TextHeight TLabelLabel1LeftTopWidth4HeightCaptionData inizio:  TLabelLabel2LeftTopWidth7HeightCaptionN� ricercati:  TLabelLabel10LeftTop:Width9HeightCaptionTipo offerta:  TLabelLabel4LeftTopOWidthuHeightCaptionData prevista di chiusuraFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel5LeftTop8Width� HeightCaptionData prevista di presentazioneFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TBitBtnBitBtn1Left� TopWidthYHeight"TabOrderOnClickBitBtn1ClickKindbkOK  TBitBtnBitBtn2Left� Top&WidthYHeight"CaptionAnnullaTabOrderKindbkCancel  TDateEdit97DEDataLeftLTopWidthaHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder ColorCalendar.ColorValidclBlueDayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanPopup
doIsMaskeddoShowCanceldoShowToday   	TSpinEditSENumRicLeftLTopWidth)HeightMaxValuedMinValue TabOrderValue   	TGroupBox	GroupBox3LeftTop� WidthHeight?CaptionRuolo richiestoTabOrder TSpeedButtonSpeedButton1Left� TopWidthHeightHint"inserisci o cambia ruolo richiesto
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333�3333�39�3330 373337w39�3330 3��337w9���33337ww333?9���333 7wws333w39�3333 373333w39�3333337s3333?3333333 3333�33w333<333 3337�33w333<�3333����?�<����Ù37wwwww�<����Ù37wwwwsw3333<�3333337s33�333<330 3337337w3333330 3333337w3333333333333333	NumGlyphsParentShowHintShowHint	OnClickSpeedButton1Click  TEditERuoloLeftTopWidth� HeightReadOnly	TabOrder   TEditEAreaLeftTop%Width� HeightColor	clBtnFaceReadOnly	TabOrder   	TGroupBox	GroupBox4LeftTop� Width� Height*CaptionSelez.princ. (capo commessa)TabOrder TDBGridDBGrid1LeftTopWidth� Height
DataSourceDsUsersOptionsdgColumnResizedgTabsdgConfirmDeletedgCancelOnExit TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldName
NominativoWidthiVisible	     	TGroupBox	GBClienteLeftTopNWidth%HeightDCaptionClienteTabOrder TSpeedButtonBTrovaClienteLeftTopWidthHeightHintseleziona cliente
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333333333333333333333333�3333330 333����w330  0 �337ww7w�33�� � 33�3w�w33 ��� 33ws37�w30����� 3���37�w0  ��� 7wws37�w������ s����7�w0   �� 7wwws7�w333�   333s�www3330 3333337w3333333333333333333333333333333333333333333333333333333333333333333	NumGlyphsParentShowHintShowHint	OnClickBTrovaClienteClick  TLabelLabel3LeftTop+Width[HeightCaptionRiferimento interno:  TSpeedButtonSpeedButton2LeftTop%WidthHeightHint,seleziona riferimento interno per il cliente
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333333333333333333333333�3333330 333����w330  0 �337ww7w�33�� � 33�3w�w33 ��� 33ws37�w30����� 3���37�w0  ��� 7wws37�w������ s����7�w0   �� 7wwws7�w333�   333s�www3330 3333337w3333333333333333333333333333333333333333333333333333333333333333333	NumGlyphsParentShowHintShowHint	OnClickSpeedButton2Click  TEditEClienteLeftTopWidth� HeightReadOnly	TabOrder   TEditERifInternoLefthTop(Width� HeightTabOrder   	TComboBoxCBTipoLeftKTop6Width� Height
ItemHeightTabOrderTextricerca direttaItems.Stringsricerca direttaricerca mistaricerca d'archiviopubblicazione su stamparicerca internazionalealtro   	TGroupBox	GroupBox1LeftTopWidth� Height*CaptionSede operativaTabOrder TDBGridDBGrid2LeftTopWidth� Height
DataSourceDsQSediOptionsdgColumnResizedgTabsdgConfirmDeletedgCancelOnExit TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameDescrizioneWidth� Visible	     TDateEdit97DEDataPrevChiusLeft� TopLWidthvHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder	ColorCalendar.ColorValidclBlueDayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanClear
doCanPopup
doIsMaskeddoShowCanceldoShowToday   TDateEdit97DEDataPrevPresLeft� Top5WidthvHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder
ColorCalendar.ColorValidclBlueDayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanClear
doCanPopup
doIsMaskeddoShowCanceldoShowToday   TDataSourceDsUsersDataSetTUsersLeft� Top�   TDataSourceDsQSediDataSetQSediLeft(Top�   TADOLinkedQueryQ
ConnectionData.DB
Parameters 	UseFilterLeft� Top�   TADOLinkedQueryQSediActive	
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect * from Aziende 	UseFilterLeftTop�   TADOLinkedTableTUsersActive	
ConnectionData.DB
CursorTypectStatic	TableNameUsersLeft|Top�    