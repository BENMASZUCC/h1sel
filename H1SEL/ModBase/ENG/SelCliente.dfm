�
 TSELCLIENTEFORM 0O  TPF0TSelClienteFormSelClienteFormLeftTop� ActiveControlEDescBorderStylebsDialogCaptionSelezione Cliente/aziendaClientHeight�ClientWidthGColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TBitBtnBitBtn1Left�TopWidth]Height$TabOrder KindbkOK  TBitBtnBitBtn2Left�Top*Width]Height$CaptionAnnullaTabOrderKindbkCancel  TPanelPanel2LeftTopWidth�Height	AlignmenttaLeftJustifyCaption  Lista Clienti/aziendeColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TPanelPanel1LeftTopWidth�HeightX
BevelOuter	bvLoweredTabOrder TLabelLabel1LeftTop	Width(HeightCaptionRicerca:  TEditEDescLeft5TopWidth� HeightTabOrder OnChangeEDescChange  TRadioGroupRGFiltroLeftTopWidth� Height8CaptionFiltroColumns	ItemIndexItems.StringsTUTTIattivi
non attivi
contattati	eliminati TabOrderOnClickRGFiltroClick  TBitBtnBitBtn3LeftqTopWidthhHeight%CaptionNuova aziendaTabOrderOnClickBitBtn3Click
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333�3333�39�3330 373337w39�3330 3��337w9���33337ww333?9���333 7wws333w39�3333 373333w39�3333337s3333?3333333 3333�33w333<333 3337�33w333<�3333����?�<����Ù37wwwww�<����Ù37wwwwsw3333<�3333337s33�333<330 3337337w3333330 3333337w3333333333333333	NumGlyphs  	TCheckBox
CBTestoConLeft� TopWidth� HeightCaptionricerca per testo contenutoTabOrderOnClickCBTestoConClick   	TdxDBGrid	dxDBGrid1LeftTopwWidth=HeightYBands  DefaultLayout	HeaderPanelRowCountKeyFieldIDSummaryGroups SummarySeparator, TabOrder
DataSource	DsClientiFilter.Criteria
       OptionsBehavioredgoAutoSearchedgoAutoSortedgoDragScrolledgoEnterShowEditoredgoImmediateEditoredgoTabThroughedgoVertThrough 	OptionsDBedgoCancelOnExitedgoCanDeleteedgoCanInsertedgoCanNavigationedgoConfirmDeleteedgoLoadAllRecordsedgoUseBookmarks OptionsViewedgoAutoWidthedgoBandHeaderWidthedgoIndicatoredgoUseBitmap  TdxDBGridMaskColumndxDBGrid1DescrizioneCaptionDenominazioneSortedcsUpWidth� 	BandIndex RowIndex 	FieldNameDescrizione  TdxDBGridMaskColumndxDBGrid1ComuneWidth� 	BandIndex RowIndex 	FieldNameComune  TdxDBGridMaskColumndxDBGrid1ProvinciaWidth2	BandIndex RowIndex 	FieldName	Provincia  TdxDBGridMaskColumndxDBGrid1TipoAziendaCaptionTipoWidthf	BandIndex RowIndex 	FieldNameTipoAzienda   TDataSource	DsClientiDataSetTClientiLeftxTop  TADOLinkedQueryTClienti
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Strings9select EBC_Clienti.ID,Descrizione,Stato,Comune,Provincia,/          IDTipoAzienda,TipoAzienda, IDAttivita!from EBC_Clienti, EBC_TipiAziende5where EBC_Clienti.IDTipoAzienda *= EBC_TipiAziende.IDorder by Descrizione 	UseFilterLeftxTop�  TAutoIncField
TClientiID	FieldNameIDReadOnly	  TStringFieldTClientiDescrizione	FieldNameDescrizioneSize2  TStringFieldTClientiStato	FieldNameStatoSize  TStringFieldTClientiComune	FieldNameComune  TStringFieldTClientiProvincia	FieldName	ProvinciaSize  TIntegerFieldTClientiIDTipoAzienda	FieldNameIDTipoAzienda  TStringFieldTClientiTipoAzienda	FieldNameTipoAziendaSize  TIntegerFieldTClientiIDAttivita	FieldName
IDAttivita    