�
 TCONTATTOCANDFORM 0E  TPF0TContattoCandFormContattoCandFormLeftTop� HorzScrollBar.VisibleVertScrollBar.VisibleActiveControlDEDataBorderIcons BorderStylebsDialogCaptionRegistrazione contattoClientHeightClientWidthsColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrder	PositionpoScreenCenterOnCreate
FormCreateOnShowFormShowPixelsPerInch`
TextHeight TLabelLabel9LefttTopWidthHeightCaptionFaxFocusControlDBEdit5  TLabelLabel6LeftTop�WidthHeightCaptionNote  TPanelPanel1LeftTopXWidth� Height!
BevelInner	bvLoweredTabOrder  TLabelLabel2Left	Top
WidthHeightCaptionData  TLabelLabel3Left� Top	WidthHeightCaptionOra  TDateEdit97DEDataLeft(TopWidth[HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder ColorCalendar.ColorValidclBlueDayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanPopup
doIsMaskeddoShowCanceldoShowToday   	TMaskEditMEOraLeft� TopWidth#HeightEditMask
!90:00;1;_	MaxLengthTabOrderText18.00   TBitBtnBitBtn1LeftTop�Width]Height TabOrderOnClickBitBtn1ClickKindbkOK  TBitBtnBitBtn2LeftTop�Width]Height CaptionAnnullaTabOrderKindbkCancel  TDBEditDBEdit5LefttTopWidth� Height	DataFieldFaxTabOrder  TPanelPanel3Left Top WidthsHeightQAlignalTop
BevelOuterbvNoneEnabledTabOrder TLabelLabel1LeftTopWidth3HeightCaption
Candidato:  TLabelLabel7LeftTop)WidthTHeightCaptionRecapiti telefonici  TLabelLabel8Left� Top)Width(HeightCaption	Cellulare  TEditECognLeftTopWidth� HeightColorclYellowFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder TextECogn  TEditENomeLeft� TopWidthyHeightColorclYellowFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderTextENome  TEditERecLeftTop8Width� HeightTabOrderTextERec  TEditECellLeft� Top8Width� HeightTabOrderTextECell   TRadioGroupRadioGroup2LeftTop`WidthtHeight� CaptionEsito contattoItems.Stringsda richiamaredeve richiamarefissato colloquionon accettaaltro esito TabOrderOnClickRadioGroup2Click  TPanelPanAltroEsitoLeft~Top�Width� Height.
BevelOuter	bvLoweredTabOrderVisible TLabelLabel10LeftTopWidthWHeightCaptionBreve descrizione:  TEditEDescLeftTopWidth� Height	MaxLengthTabOrder    TEditENoteLeftTop�WidthHeight	MaxLength(TabOrder  TPanelPanMotivoNonAccLeft~Top�Width� Height.
BevelOuter	bvLoweredTabOrderVisible TLabelLabel4LeftTopWidthuHeightCaptionMotivo non accettazione  TSpeedButtonSpeedButton1Left� TopWidthHeightHint#seleziona motivo 
non accettazione
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333333333333333333333333�3333330 333����w330  0 �337ww7w�33�� � 33�3w�w33 ��� 33ws37�w30����� 3���37�w0  ��� 7wws37�w������ s����7�w0   �� 7wwws7�w333�   333s�www3330 3333337w3333333333333333333333333333333333333333333333333333333333333333333	NumGlyphsParentShowHintShowHint	OnClickSpeedButton1Click  TEdit
EMotNonAccLeftTopWidth� Height	MaxLengthReadOnly	TabOrder    TDBGridDBGrid1LeftTopWidth� Height� 
DataSourceDsQTipiContattiReadOnly	TabOrder	TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameTipoContattoTitle.CaptionTipologia di contattoWidth� Visible	    TADOLinkedQueryQAnag
ConnectionData.DB
Parameters 	UseFilterLeft(Top  	TADOQueryQTipiContattiActive	
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect * from TipiContattiCand Left� Top�  TAutoIncFieldQTipiContattiID	FieldNameIDReadOnly	  TStringFieldQTipiContattiTipoContatto	FieldNameTipoContattoSize2  TIntegerFieldQTipiContattiIDEvento	FieldNameIDEvento   TDataSourceDsQTipiContattiDataSetQTipiContattiLeft� Top�    