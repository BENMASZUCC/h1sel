�
 TLEGENDARICFORM 0E  TPF0TLegendaRicFormLegendaRicFormLeft8Top� BorderStylebsDialogCaptionLegenda e parametriClientHeightqClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnClose	FormCloseOnShowFormShowPixelsPerInch`
TextHeight TBitBtnBitBtn1LeftpTopWidthVHeight!CaptionChiudiTabOrder KindbkOK  TPanelPanel1LeftTopWidtheHeight)
BevelInnerbvSpace
BevelOuter	bvLoweredTabOrder TLabelLabel6Left&TopWidth� HeightCaptionEn� di giorni dall'ultima data di inserimento in una ricerca qualsiasiWordWrap	  TLabelLabel1LeftTopWidthHeightCaptionDiFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel3Left� TopWidth4HeightCaptionrosso se meno diFont.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontWordWrap	  TRxSpinEditRxSpinEdit1Left+TopWidth2HeightTabOrder    TPanelPanel2LeftTop2WidtheHeight)
BevelInnerbvRaised
BevelOuter	bvLoweredTabOrder TLabelLabel7Left&TopWidthoHeightCaption1n� di giorni dall'ultimo contatto con il soggettoWordWrap	  TLabelLabel2LeftTopWidthHeightCaptionUcFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel4Left� TopWidth4HeightCaptionrosso se meno diFont.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontWordWrap	  TRxSpinEditRxSpinEdit2Left+TopWidth2HeightTabOrder    TPanelPanel3LeftTop_WidtheHeight)
BevelInnerbvRaised
BevelOuter	bvLoweredTabOrder TLabelLabel5Left&TopWidthNHeightCaptioncolloquio fissato nel futuroWordWrap	  TLabelLabel8LeftTopWidthHeightCaptionFcFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel9Left� TopWidth4HeightCaptiongiallo se fissatoFont.CharsetDEFAULT_CHARSET
Font.ColorclYellowFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontWordWrap	   TPanelPanel4LeftTop� WidtheHeight)
BevelInnerbvSpace
BevelOuter	bvLoweredTabOrder TLabelLabel10Left&TopWidth� HeightCaption;n� di giorni dall'ultimo colloquio in una ricerca qualsiasiWordWrap	  TLabelLabel11LeftTopWidthHeightCaptionColFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel12Left� TopWidth?HeightCaptionazzurro se meno diFont.CharsetDEFAULT_CHARSET
Font.ColorclAquaFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontWordWrap	  TRxSpinEditRxSpinEdit3Left+TopWidth2HeightTabOrder    TPanelPanel5LeftTop� Width� Heightj
BevelInnerbvRaised
BevelOuter	bvLoweredTabOrder TLabelLabel13LeftTopWidth� HeightCaption Colorazione carattere Numero CV:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel14LeftTopWidth� HeightCaptionverde: almeno 30 gg. di vitaFont.CharsetDEFAULT_CHARSET
Font.ColorclGreenFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel15LeftTop(Width� HeightCaption arancione: almeno 60 gg. di vitaFont.CharsetDEFAULT_CHARSET
Font.Color��  Font.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel16LeftTop8Width� HeightCaptiongiallo: almeno 90 gg. di vitaFont.CharsetDEFAULT_CHARSET
Font.ColorclYellowFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel17LeftTopHWidth� HeightCaptionrosso: almeno 120 gg. di vitaFont.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel18LeftTopWWidth� HeightCaptionmarrone: almeno 180 gg. di vitaFont.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont   	TGroupBox	GroupBox2LeftTop.WidtheHeight)Caption$Segnalazione candidati gi� visionatiTabOrder TLabelLabel19LeftTopWidth}HeightCaptionCognome e NomeFont.CharsetDEFAULT_CHARSET
Font.Colorl�� Font.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel20Left� TopWidth� HeightCaption): sfondo azzurro se gi� visti in giornata   TQueryQ_OLDDatabaseNameEBCDBLeft�TopH  TADOLinkedQueryQ
ConnectionData.DB
Parameters 	UseFilterLeft�Toph   