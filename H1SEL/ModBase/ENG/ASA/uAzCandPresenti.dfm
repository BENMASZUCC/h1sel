�
 TAZCANDPRESENTIFORM 0�
  TPF0TAzCandPresentiFormAzCandPresentiFormLeftSTop}Width�Height�CaptionCandidati presenti in aziendaColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TPanelPanel1Left Top Width�Height/AlignalTop
BevelOuter	bvLoweredTabOrder  TBitBtnBitBtn1Left%TopWidthjHeight'AnchorsakTopakRight CaptionEsciTabOrder KindbkOK   	TdxDBGrid	dxDBGrid1Left Top/Width�HeighttBands  DefaultLayout	HeaderPanelRowCountKeyFieldIDSummaryGroups SummarySeparator, AlignalClientTabOrder
DataSourceDsQCandAziendaFilter.Criteria
       	OptionsDBedgoCancelOnExitedgoCanDeleteedgoCanInsertedgoCanNavigationedgoConfirmDeleteedgoLoadAllRecordsedgoUseBookmarks OptionsViewedgoAutoWidthedgoBandHeaderWidthedgoUseBitmap  TdxDBGridMaskColumndxDBGrid1CVNumeroCaptionN� CVWidthB	BandIndex RowIndex 	FieldNameCVNumero  TdxDBGridMaskColumndxDBGrid1CognomeSortedcsUpWidth� 	BandIndex RowIndex 	FieldNameCognome  TdxDBGridMaskColumndxDBGrid1NomeWidth� 	BandIndex RowIndex 	FieldNameNome   TQueryQCandAzienda_oldDatabaseNameEBCDB
DataSourceASAAziendaForm.DsQAziendaSQL.Strings+select Anagrafica.ID, CVNumero,Cognome,Nome%from EsperienzeLavorative, Anagrafica7where EsperienzeLavorative.IDAnagrafica = Anagrafica.ID&and EsperienzeLavorative.IDAzienda=:IDand Attuale=1 Left Top� 	ParamDataDataType	ftUnknownNameID	ParamType	ptUnknown   TAutoIncFieldQCandAzienda_oldID	FieldNameIDOriginEBCDB.Anagrafica.ID  TIntegerFieldQCandAzienda_oldCVNumero	FieldNameCVNumeroOriginEBCDB.Anagrafica.CVNumero  TStringFieldQCandAzienda_oldCognome	FieldNameCognomeOriginEBCDB.Anagrafica.Cognome	FixedChar	Size  TStringFieldQCandAzienda_oldNome	FieldNameNomeOriginEBCDB.Anagrafica.Nome	FixedChar	Size   TDataSourceDsQCandAziendaDataSetQCandAziendaLeft Top�   	TADOQueryQCandAzienda
ConnectionData.DB
BeforeOpenQCandAziendaBeforeOpen
ParametersNamexID
AttributespaSigned
paNullable DataType	ftInteger	Precision
SizeValue   SQL.Strings+select Anagrafica.ID, CVNumero,Cognome,Nome%from EsperienzeLavorative, Anagrafica7where EsperienzeLavorative.IDAnagrafica = Anagrafica.ID(and EsperienzeLavorative.IDAzienda= :xIDand Attuale=1 Left Topx TAutoIncFieldQCandAziendaID	FieldNameIDReadOnly	  TIntegerFieldQCandAziendaCVNumero	FieldNameCVNumero  TStringFieldQCandAziendaCognome	FieldNameCognomeSize  TStringFieldQCandAziendaNome	FieldNameNomeSize    