�
 TTIPIMAILCANDFORM 0�  TPF0TTipiMailCandFormTipiMailCandFormLeftTop� BorderStylebsDialogCaptionGestione tipi di mailClientHeight�ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TDBGridDBGrid1LeftTopWidth� HeightzAnchorsakLeftakTopakRightakBottom 
DataSourceDsQTipiCandReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameDescrizioneVisible	    TPanelPanel1Left� TopWidthYHeightzAnchorsakTopakRightakBottom 
BevelInnerbvRaised
BevelOuter	bvLoweredTabOrder TLabelLabel1LeftTop2Width7HeightCaptionDescrizioneFocusControlDBEdit1  TLabelLabel2LeftTop� Width9HeightCaptionIntestazioneFocusControlDBMemo1  TLabelLabel3LeftTop� WidthHeightCaptionCorpoFocusControlDBMemo2  TLabelLabel4LeftTopZWidth&HeightCaptionOggettoFocusControlDBEdit2  TDBEditDBEdit1LeftTopBWidthIHeightAnchorsakLeftakTopakRight 	DataFieldDescrizione
DataSourceDsQTipiCandTabOrder   TDBMemoDBMemo1LeftTop� WidthIHeight9AnchorsakLeftakTopakRight 	DataFieldIntestazione
DataSourceDsQTipiCandTabOrder  TDBMemoDBMemo2LeftTopWidthIHeightZAnchorsakLeftakTopakRightakBottom 	DataFieldCorpo
DataSourceDsQTipiCandTabOrder  TDBEditDBEdit2LeftTopjWidth0HeightAnchorsakLeftakTopakRight 	DataFieldOggetto
DataSourceDsQTipiCandTabOrder  TDBCheckBoxDBCheckBox1LeftTop� Width� HeightCaptionriporta dati ricerca	DataFieldInfoRicerca
DataSourceDsQTipiCandTabOrderValueCheckedTrueValueUncheckedFalse  TPanelPanel2LeftTopWidthJHeight&AnchorsakLeftakTopakRight 
BevelOuter	bvLoweredTabOrder TToolbarButton97BNewLeftTopWidthCHeight$Caption
Nuovo tipoWordWrap	OnClick	BNewClick  TToolbarButton97BdelLeftDTopWidthCHeight$CaptionEliminaWordWrap	OnClick	BdelClick  TToolbarButton97BOKLeft� TopWidthCHeight$CaptionConferma modificheEnabledWordWrap	OnClickBOKClick  TToolbarButton97BCanLeft� TopWidthCHeight$CaptionAnnulla modificheEnabledWordWrap	OnClick	BCanClick   TDBCheckBoxDBCheckBox2LeftTop� Width� HeightCaptionAggiungi specifiche (esterne)	DataFieldAddSpecifiche
DataSourceDsQTipiCandTabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox3LeftTopcWidth� HeightCaptionAggiungi link a questionario	DataFieldAddQuest
DataSourceDsQTipiCandTabOrderValueCheckedTrueValueUncheckedFalse  TPanelPanel3LeftTop� WidthJHeight&AnchorsakLeftakTopakRight 
BevelOuter	bvLoweredTabOrder TToolbarButton97BCliNewLeftTopWidthCHeight$Caption
Nuovo tipoWordWrap	OnClickBCliNewClick  TToolbarButton97BCliDelLeftDTopWidthCHeight$CaptionEliminaWordWrap	OnClickBCliDelClick  TToolbarButton97BCliOKLeft� TopWidthCHeight$CaptionConferma modificheEnabledWordWrap	OnClickBCliOKClick  TToolbarButton97BCliCanLeft� TopWidthCHeight$CaptionAnnulla modificheEnabledWordWrap	OnClickBCliCanClick    TBitBtnBitBtn1LeftCTopWidthPHeightAnchorsakTopakRight CaptionEsciTabOrderKindbkOK  TDataSourceDsQTipiCandDataSet	QTipiCandOnStateChangeDsQTipiCandStateChangeLeft Top@  TADOLinkedQuery	QTipiCandActive	
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Strings)select * from TipiMailCand with (updlock) 	UseFilterLeft Top  TADOLinkedQueryQTipiCli
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Strings0select * from TipiMailContattiCli with (updlock) 	UseFilterLeftXTop  TDataSource
dsQTipiCliDataSetQTipiCliOnStateChangedsQTipiCliStateChangeLeftXTop@   