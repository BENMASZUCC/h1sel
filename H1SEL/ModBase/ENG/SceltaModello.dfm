�
 TSCELTAMODELLOFORM 0v  TPF0TSceltaModelloFormSceltaModelloFormLeft� TopmBorderStylebsDialogCaptionScelta modello WordClientHeightDClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnCreate
FormCreateOnShowFormShowPixelsPerInch`
TextHeight TBitBtnBitBtn1LeftiTopWidthaHeight#TabOrder OnClickBitBtn1ClickKindbkOK  TBitBtnBitBtn2LeftiTop(WidthaHeight#CaptionAnnullaTabOrderKindbkCancel  TDBGridDBGrid1LeftTopWidth`Height� 
DataSourceDsModelliWordReadOnly	TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameNomeModelloTitle.CaptionNome modelloWidth^Visible	 Expanded	FieldNameDescrizioneWidth� Visible	    TRadioGroup	RGOpzioneLeftTop� Width`HeightQCaptionOpzione	ItemIndexItems.Strings3Stampa per tutti i record selezionati e chiudi Word=Crea file per il solo record selezionato (lascia aperto Word)?Crea file e stampa per il solo record selezionato e chiudi Word TabOrder  	TCheckBoxCBSalvaLeftTopWidth� HeightCaption'Salva file Word associandolo al clienteTabOrder  	TCheckBoxCBStoricizzaLeftTop/Width]HeightCaptionMRegistra nello storico l'invio per il riferimento o i riferimenti selezionatiChecked	State	cbCheckedTabOrder  TBitBtnBitBtn3LeftiTopUWidthaHeight#CaptionModelliTabOrderOnClickBitBtn3Click
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� UUP�YPU_�_uW��W �UYUwu�UW�up��UY3W_�UW�� �UY3w_UW�� UUY5wUU�WUU���uUuu_�Up�uPUW�U�wwuU U3UUwu_u�UUUUP35UU_��_uUU U3UUUw�u�UUPPP35UUWW�_uUU 3UUUw�UUU   5UWUwwwUUUU  UWUUww�UUU0 UWUUww��uUU3P  UUUUwWwwUUU	NumGlyphs  TDataSourceDsModelliWordDataSetQModelliWordLeftHTopH  TADOLinkedQueryQModelliWord
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect * from ModelliWord  	UseFilterLeftHTop TAutoIncFieldQModelliWordID	FieldNameIDReadOnly	  TStringFieldQModelliWordNomeModello	FieldNameNomeModelloSize  TStringFieldQModelliWordDescrizione	FieldNameDescrizioneSize2  TStringFieldQModelliWordTabellaMaster	FieldNameTabellaMaster  TStringField QModelliWordInizialiFileGenerato	FieldNameInizialiFileGeneratoSize  TBooleanFieldQModelliWordH1HRMS	FieldNameH1HRMS  TBooleanFieldQModelliWordH1SEL	FieldNameH1SEL   	TADOQueryQModelliAvanzati
ConnectionData.DB
DataSourceDsModelliWord
ParametersNameID
AttributespaSigned DataType	ftInteger	Precision
SizeValue   SQL.Stringsselect *from modellidefinizioniwhere idmodello=:ID LeftpTop   