�
 TFRMSTATISTICHE 0  TPF0TFrmStatisticheFrmStatisticheLeft Top WidthHeight�TabOrder  	TSplitter	Splitter1LeftTop WidthHeight�CursorcrHSplitAlignalRight  TPanelPanel182LeftATop Width� Height�AlignalRightColorS�� TabOrder  TLabelLabel127LeftTopWidthwHeightCaptionAziende in Archivio oggi :Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel128LeftTopGWidth_HeightCaptionCV in Archivio oggi :Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel129LeftTopWidthHeight  TLabelLabel131LeftTop(Width� HeightCaption!Aziende attive in Archivio oggi :Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel132LeftTophWidthuHeightCaptionFatturato da inizio Anno :Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel133LeftTop� Width� HeightCaptionNumero Fatture da inizio Anno :Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TDBTextDBText16LeftTopWidthAHeight	DataFieldtot
DataSourceDSQAzTotFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TDBTextDBText17LeftTop6WidthAHeight	DataFieldtot
DataSourceDSQAzAttTotFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TDBTextDBText18LeftTopVWidthAHeight	DataFieldtot
DataSourceDSCVTotFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TDBTextDBText19LeftTopvWidthAHeight	DataFieldtot
DataSourceDSFattFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TDBTextDBText20LeftTop� WidthAHeight	DataFieldtot
DataSource	DSQNrFattFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel134LeftTop� Width~HeightCaption,Fatturato anno precedente
(stesso periodo):Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TDBTextDBText21LeftTop� WidthAHeight	DataFieldtot
DataSourceDSFattASFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel135LeftTop� Width� HeightCaption1Numero Fatture anno precedente
(stesso periodo):Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TDBTextDBText22LeftTop� WidthAHeight	DataFieldtot
DataSource
DSNrFattASFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel136LeftTopWidth� HeightCaptionNumero Colloqui ultimi 30 gg :Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TDBTextDBText23LeftTopWidthAHeight	DataFieldtot
DataSourceDSNrCollFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TDBTextDBText24LeftTop>WidthAHeight	DataFieldtot
DataSourceDSQNrCollASFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel144LeftTop"Width� HeightCaption<Numero Colloqui ultimi 30 gg 
Anno scorso, stesso periodo :Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel150LeftTopmWidth� HeightCaptionANumero Presentazioni ultimi 30 gg 
Anno scorso, stesso periodo :Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TDBTextDBText25LeftTop�WidthAHeight	DataFieldtot
DataSourceDSNrPressASFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TDBTextDBText26LeftTop\WidthAHeight	DataFieldtot
DataSourceDSNrPresFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel151LeftTopNWidth� HeightCaption#Numero Presentazioni ultimi 30 gg :Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel152LeftTop�Width� HeightCaptionNumero Offerte ultimi 30 gg :Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TDBTextDBText27LeftTop�WidthAHeight	DataFieldtot
DataSourceDSNROffFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel153LeftTop�Width� HeightCaption;Numero Offerte ultimi 30 gg 
Anno scorso, stesso periodo :Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TDBTextDBText28LeftTop�WidthAHeight	DataFieldtot
DataSource	DSNrOffASFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont   TPanelPanel181Left Top WidthAHeight�AlignalClientCaptionPanel221TabOrder TPageControlPageControl1LeftTopWidth?Height�
ActivePage	TabSheet2AlignalClientTabOrder  	TTabSheet	TabSheet1CaptionMaster Report
TabVisible 
TScrollBox
ScrollBox1Left Top Width7Height�VertScrollBar.Position�AlignalClientTabOrder  TDBChartDBChartFattLeft Top@Width#Height� Gradient.EndColor �� Gradient.Visible	Title.Font.Color�Title.Font.Height�Title.Font.StylefsBold Title.Text.StringsAndamento Fatturato ultimi anni BottomAxis.LabelsFont.Height�LeftAxis.LabelsFont.Height�LeftAxis.VisibleLegend.VisibleAlignalTopTabOrder 
OnDblClickDBChartCVInsClick TLineSeries
SeriesFattMarks.ArrowLengthMarks.Callout.Brush.ColorclBlackMarks.Callout.DistanceMarks.Callout.LengthMarks.Brush.ColorclWhiteMarks.Brush.StylebsClearMarks.Font.Height�Marks.Font.StylefsBold Marks.Frame.VisibleMarks.StylesmsValueMarks.Visible	
DataSourceFatturatoxAnnoSeriesColorclGreenValueFormat#,##0.00XLabelsSourceanno
LineHeightOutLine.Color��  OutLine.EndStyleesSquareOutLine.Visible	Pointer.InflateMargins	Pointer.StylepsRectanglePointer.VisibleXValues.NameXXValues.OrderloAscendingYValues.NameYYValues.ValueSourcenumero   TDBChartDBChartNrFattLeft Top� Width#Height� Gradient.EndColor �� Gradient.Visible	Title.Font.Color�Title.Font.Height�Title.Font.StylefsBold Title.Text.StringsNumero Fatture Emesse LeftAxis.VisibleLegend.VisibleAlignalTopTabOrder
OnDblClickDBChartCVInsClickPrintMargins  TButtonButton1Left�Top� WidthKHeightCaptionButton1TabOrder   TLineSeriesSeriesNrFattMarks.ArrowLengthMarks.Callout.Brush.ColorclBlackMarks.Callout.DistanceMarks.Callout.LengthMarks.Brush.ColorclWhiteMarks.Brush.StylebsClearMarks.Font.Height�Marks.Font.StylefsBold Marks.Frame.VisibleMarks.StylesmsValueMarks.Visible	
DataSourceNrFatturexAnnoSeriesColor �@ ValueFormat#,##0XLabelsSourceanno
LineHeightLinePen.Color�LinePen.EndStyleesFlatOutLine.Color �@ OutLine.EndStyleesFlatOutLine.Visible	Pointer.InflateMargins	Pointer.StylepsRectanglePointer.VisibleXValues.NameXXValues.OrderloAscendingYValues.NameYYValues.ValueSourcenumero   TPanelPanel1Left Top�Width#Height� AlignalTopCaptionPanel1TabOrder TDBChartDBChartCVInsLeftTopWidth�Height� Gradient.EndColor �� Gradient.Visible	Title.Font.ColorclBlackTitle.Font.Height�Title.Font.StylefsBold Title.Text.Strings-Andamento inserimento CV (rosso anno attuale) BottomAxis.LabelsFont.Height�LeftAxis.VisibleLegend.VisibleAlignalClientTabOrder 
OnDblClickDBChartCVInsClick 
TBarSeriesBarSeriesCVInseritiBarPen.StylepsDotMarks.ArrowLengthMarks.Callout.Brush.ColorclBlackMarks.Callout.DistanceMarks.Callout.LengthMarks.Brush.ColorclWhiteMarks.Brush.StylebsClearMarks.Font.ColorclRedMarks.Font.Height�Marks.Frame.VisibleMarks.StylesmsValueMarks.Visible	
DataSourceQCVInseritiSeriesColorclRedXLabelsSourcemeseBarStyle
bsCilinderBarWidthPercentGradient.DirectiongdTopBottomXValues.NameXXValues.OrderloAscendingYValues.NameBarYValues.ValueSourcenumero  
TBarSeriesBarSeriesCVInseritiASBarPen.StylepsDotMarks.ArrowLength	Marks.Callout.Brush.ColorclBlackMarks.Callout.Distance
Marks.Callout.Length	Marks.Brush.ColorclWhiteMarks.Brush.StylebsClearMarks.Font.CharsetANSI_CHARSETMarks.Font.Height�Marks.Font.OutLine.Color��@ Marks.Frame.VisibleMarks.StylesmsValueMarks.Visible	
DataSourceQCVInseritiASSeriesColor��  XLabelsSourcemeseAutoMarkPositionBarStyle
bsCilinderBarWidthPercentGradient.DirectiongdTopBottomXValues.NameXXValues.OrderloAscendingYValues.NameBarYValues.ValueSourcenumero   TDBChartDBChart4Left�TopWidth?Height� Gradient.EndColor �� Gradient.Visible	Title.Font.ColorclBlackTitle.Text.StringsTotale per Anno BottomAxis.LabelsAngle-BottomAxis.LabelsFont.Height�BottomAxis.MaximumOffset
BottomAxis.MinimumOffset
LeftAxis.VisibleLegend.VisibleAlignalRightTabOrder 
TBarSeriesBarSeriesTotCVPerAnnoBarPen.StylepsDotDepthMarks.ArrowLengthMarks.Callout.Brush.ColorclBlackMarks.Callout.Distance�Marks.Callout.LengthMarks.Brush.ColorclWhiteMarks.Brush.StylebsClearMarks.Font.Height�Marks.Frame.VisibleMarks.StylesmsValueMarks.Visible	
DataSourceQTotCVPerAnnoSeriesColorclRedXLabelsSourceannoBarStyle
bsCilinderBarWidthPercentGradient.DirectiongdTopBottomXValues.NameXXValues.OrderloAscendingYValues.NameBarYValues.ValueSourcenumero    TPanelPanelFattPrevTotLeft TopR�Width#Height� AlignalTopTabOrder TDBChartDBChart3LeftTopWidth�Height� Gradient.EndColor �� Gradient.Visible	Title.Font.ColorclBlackTitle.Font.Height�Title.Font.StylefsBold Title.Text.StringsFatturato previsto Totale BottomAxis.LabelsFont.Height�LeftAxis.VisibleLegend.VisibleAlignalClientTabOrder 
OnDblClickDBChartCVInsClick 
TBarSeriesBarSeries10BarPen.StylepsDotMarks.ArrowLengthMarks.Callout.Brush.ColorclBlackMarks.Callout.Distance�Marks.Callout.LengthMarks.Brush.ColorclWhiteMarks.Brush.StylebsClearMarks.Font.ColorclRedMarks.Font.Height�Marks.Frame.VisibleMarks.StylesmsValueMarks.Visible	
DataSourceQAndFattPrevistoTotSeriesColorclRedXLabelsSourcemeseBarStyle
bsCilinderBarWidthPercentGradient.DirectiongdTopBottomXValues.NameXXValues.OrderloAscendingYValues.NameBarYValues.ValueSourcenumero   TDBChartDBChart5Left�TopWidth@Height� Gradient.EndColor �� Gradient.Visible	Title.Font.ColorclBlackTitle.Text.StringsTotale per Anno BottomAxis.LabelsAngle-BottomAxis.LabelsFont.Height�BottomAxis.MaximumOffset
BottomAxis.MinimumOffset
LeftAxis.VisibleLegend.VisibleAlignalRightTabOrder 
TBarSeriesBarSeriesQAndFattPrevPerAnnoBarPen.StylepsDotDepthMarks.ArrowLengthMarks.Callout.Brush.ColorclBlackMarks.Callout.Distance�Marks.Callout.LengthMarks.Brush.ColorclWhiteMarks.Brush.StylebsClearMarks.Font.Height�Marks.Frame.VisibleMarks.StylesmsValueMarks.Visible	
DataSourceQAndFattPrevistoTotPerAnnoSeriesColorclRedXLabelsSourceannoBarStyle
bsCilinderBarWidthPercentGradient.DirectiongdTopBottomXValues.NameXXValues.OrderloAscendingYValues.NameBarYValues.ValueSourcenumero    TPanelPanel3Left Top��Width#Height� AlignalTopCaptionPanel3TabOrder TDBChartDBChartAzInsLeftTopWidth�Height� Gradient.EndColor �� Gradient.Visible	Title.Font.Color�Title.Font.Height�Title.Font.StylefsBold Title.Text.Strings.Andamento Clienti  Attivi (rosso anno attuale) BottomAxis.LabelsFont.Height�LeftAxis.VisibleLegend.VisibleAlignalClientTabOrder 
OnDblClickDBChartCVInsClick 
TBarSeriesBarSeriesCliAttBarPen.StylepsDotMarks.ArrowLengthMarks.Callout.Brush.ColorclBlackMarks.Callout.DistanceMarks.Callout.LengthMarks.Brush.ColorclWhiteMarks.Brush.StylebsClearMarks.Font.ColorclRedMarks.Font.Height�Marks.Frame.VisibleMarks.StylesmsValueMarks.Visible	
DataSource
ClientiAttSeriesColorclRedXLabelsSourcemeseBarStyle
bsCilinderBarWidthPercentGradient.DirectiongdTopBottomXValues.NameXXValues.OrderloAscendingYValues.NameBarYValues.ValueSourcenumero  
TBarSeriesBarSeriesCliAttaSBarPen.StylepsDotMarks.ArrowLength	Marks.Callout.Brush.ColorclBlackMarks.Callout.Distance
Marks.Callout.Length	Marks.Brush.ColorclWhiteMarks.Brush.StylebsClearMarks.Font.Height�Marks.Frame.VisibleMarks.StylesmsValueMarks.Visible	
DataSourceClientiATTASSeriesColor��  XLabelsSourcemeseBarStyle
bsCilinderBarWidthPercentGradient.DirectiongdTopBottomXValues.NameXXValues.OrderloAscendingYValues.NameBarYValues.ValueSourcenumero   TDBChartDBChart6Left�TopWidth@Height� Gradient.EndColor �� Gradient.Visible	Title.Font.ColorclBlackTitle.Text.StringsTotale per Anno BottomAxis.LabelsAngle-BottomAxis.LabelsFont.Height�BottomAxis.MaximumOffset
BottomAxis.MinimumOffset
LeftAxis.VisibleLegend.VisibleAlignalRightTabOrder 
TBarSeriesBarSeriesCliAttPerAnnoBarPen.StylepsDotDepthMarks.ArrowLengthMarks.Callout.Brush.ColorclBlackMarks.Callout.Distance�Marks.Callout.LengthMarks.Brush.ColorclWhiteMarks.Brush.StylebsClearMarks.Font.Height�Marks.Frame.VisibleMarks.StylesmsValueMarks.Visible	
DataSourceQTotCliAttPerAnnoSeriesColorclRedXLabelsSourceannoBarStyle
bsCilinderBarWidthPercentGradient.DirectiongdTopBottomXValues.NameXXValues.OrderloAscendingYValues.NameBarYValues.ValueSourcenumero    TPanelPanel4Left Top8�Width#Height� AlignalTopCaptionPanel4TabOrder TDBChartDBChartInsCliLeftTopWidth�Height� Gradient.EndColor �� Gradient.Visible	Title.Font.Color�Title.Font.Height�Title.Font.StylefsBold Title.Text.Strings3Andamento inserimento Clienti  (rosso anno attuale) BottomAxis.LabelsFont.Height�LeftAxis.VisibleLegend.VisibleAlignalClientTabOrder 
OnDblClickDBChartCVInsClick 
TBarSeries
BarSeries5BarPen.StylepsDotMarks.ArrowLengthMarks.Callout.Brush.ColorclBlackMarks.Callout.DistanceMarks.Callout.LengthMarks.Brush.ColorclWhiteMarks.Brush.StylebsClearMarks.Font.ColorclRedMarks.Font.Height�Marks.Frame.VisibleMarks.StylesmsValueMarks.Visible	
DataSourceQAzInsSeriesColorclRedXLabelsSourcemeseBarStyle
bsCilinderBarWidthPercentGradient.DirectiongdTopBottomXValues.NameXXValues.OrderloAscendingYValues.NameBarYValues.ValueSourcenumero  
TBarSeries
BarSeries6BarPen.StylepsDotMarks.ArrowLength	Marks.Callout.Brush.ColorclBlackMarks.Callout.Distance
Marks.Callout.Length	Marks.Brush.ColorclWhiteMarks.Brush.StylebsClearMarks.Font.Height�Marks.Frame.VisibleMarks.StylesmsValueMarks.Visible	
DataSourceQAzInsASSeriesColor��  XLabelsSourcemeseBarStyle
bsCilinderBarWidthPercentGradient.DirectiongdTopBottomXValues.NameXXValues.OrderloAscendingYValues.NameBarYValues.ValueSourcenumero   TDBChartDBChart7Left�TopWidth@Height� Gradient.EndColor �� Gradient.Visible	Title.Font.ColorclBlackTitle.Text.StringsTotale per Anno BottomAxis.LabelsAngle-BottomAxis.LabelsFont.Height�BottomAxis.MaximumOffset
BottomAxis.MinimumOffset
LeftAxis.VisibleLegend.VisibleAlignalRightTabOrder 
TBarSeriesBarSeries14BarPen.StylepsDotDepthMarks.ArrowLengthMarks.Callout.Brush.ColorclBlackMarks.Callout.Distance�Marks.Callout.LengthMarks.Brush.ColorclWhiteMarks.Brush.StylebsClearMarks.Font.Height�Marks.Frame.VisibleMarks.StylesmsValueMarks.Visible	
DataSourceQTotClientiPerAnnoSeriesColorclRedXLabelsSourceannoBarStyle
bsCilinderBarWidthPercentGradient.DirectiongdTopBottomXValues.NameXXValues.OrderloAscendingYValues.NameBarYValues.ValueSourcenumero    TPanelPanelFattPrevUCLeft Top��Width#Height� AlignalTopCaptionPanelFattPrevUCTabOrder TDBChartDBChart2LeftTopWidth�Height� Gradient.EndColor �� Gradient.Visible	Title.Font.ColorclBlackTitle.Font.Height�Title.Font.StylefsBold Title.Text.Strings"Fatturato previsto Utente connesso BottomAxis.LabelsFont.Height�LeftAxis.VisibleLegend.VisibleAlignalClientTabOrder 
OnDblClickDBChartCVInsClick 
TBarSeries
BarSeries9BarPen.StylepsDotMarks.ArrowLengthMarks.Callout.Brush.ColorclBlackMarks.Callout.Distance�Marks.Callout.LengthMarks.Brush.ColorclWhiteMarks.Brush.StylebsClearMarks.Font.ColorclRedMarks.Font.Height�Marks.Frame.VisibleMarks.StylesmsValueMarks.Visible	
DataSourceQAndFattPUtenteConSeriesColorclRedXLabelsSourcemeseBarStyle
bsCilinderBarWidthPercentGradient.DirectiongdTopBottomXValues.NameXXValues.OrderloAscendingYValues.NameBarYValues.ValueSourcenumero   TDBChartDBChart8Left�TopWidth@Height� Gradient.EndColor �� Gradient.Visible	Title.Font.ColorclBlackTitle.Text.StringsTotale per Anno BottomAxis.LabelsAngle-BottomAxis.LabelsFont.Height�BottomAxis.MaximumOffset
BottomAxis.MinimumOffset
LeftAxis.VisibleLegend.VisibleAlignalRightTabOrder 
TBarSeriesBarSeriesFattPrevUCPerAnnoBarPen.StylepsDotDepthMarks.ArrowLengthMarks.Callout.Brush.ColorclBlackMarks.Callout.Distance�Marks.Callout.LengthMarks.Brush.ColorclWhiteMarks.Brush.StylebsClearMarks.Font.Height�Marks.Frame.VisibleMarks.StylesmsValueMarks.Visible	
DataSourceQAndFattPUtenteConPerAnnoSeriesColorclRedXLabelsSourceannoBarStyle
bsCilinderBarWidthPercentGradient.DirectiongdTopBottomXValues.NameXXValues.OrderloAscendingYValues.NameBarYValues.ValueSourcenumero    TPanelPanelFattTotUtenteConnLeft TopWidth#Height� AlignalTopCaptionPanelFattTotUtenteConnTabOrder TDBChartDBChart1LeftTopWidth�Height� Gradient.EndColor �� Gradient.Visible	Title.Font.ColorclBlackTitle.Font.Height�Title.Font.StylefsBold Title.Text.Strings5Fatturato totale Utente connesso (rosso anno attuale) BottomAxis.LabelsFont.Height�LeftAxis.VisibleLegend.VisibleAlignalClientTabOrder 
OnDblClickDBChartCVInsClick 
TBarSeriesBarSeriesAndFattUCBarPen.StylepsDotMarks.ArrowLengthMarks.Callout.Brush.ColorclBlackMarks.Callout.DistanceMarks.Callout.LengthMarks.Brush.ColorclWhiteMarks.Brush.StylebsClearMarks.Font.ColorclRedMarks.Font.Height�Marks.Frame.VisibleMarks.StylesmsValueMarks.Visible	
DataSourceQAndFattSingoloUtSeriesColorclRedXLabelsSourcemeseBarStyle
bsCilinderBarWidthPercentGradient.DirectiongdTopBottomXValues.NameXXValues.OrderloAscendingYValues.NameBarYValues.ValueSourcenumero  
TBarSeriesBarSeriesAndFattUCASBarPen.StylepsDotMarks.ArrowLength	Marks.Callout.Brush.ColorclBlackMarks.Callout.Distance
Marks.Callout.Length	Marks.Brush.ColorclWhiteMarks.Brush.StylebsClearMarks.Font.Height�Marks.Frame.VisibleMarks.StylesmsValueMarks.Visible	
DataSourceQAndFattSingoloUtASSeriesColor��  XLabelsSourcemeseBarStyle
bsCilinderBarWidthPercentGradient.DirectiongdTopBottomXValues.NameXXValues.OrderloAscendingYValues.NameBarYValues.ValueSourcenumero   TDBChartDBChart9Left�TopWidth@Height� Gradient.EndColor �� Gradient.Visible	Title.Font.ColorclBlackTitle.Text.StringsTotale per Anno BottomAxis.LabelsAngle-BottomAxis.LabelsFont.Height�BottomAxis.MaximumOffset
BottomAxis.MinimumOffset
LeftAxis.VisibleLegend.VisibleAlignalRightTabOrder 
TBarSeriesBarSeriesFattUCPerAnnoBarPen.StylepsDotDepthMarks.ArrowLengthMarks.Callout.Brush.ColorclBlackMarks.Callout.Distance�Marks.Callout.LengthMarks.Brush.ColorclWhiteMarks.Brush.StylebsClearMarks.Font.Height�Marks.Frame.VisibleMarks.StylesmsValueMarks.Visible	
DataSourceQAndFattUCPerAnnoSeriesColorclRedXLabelsSourceannoBarStyle
bsCilinderBarWidthPercentGradient.DirectiongdTopBottomXValues.NameXXValues.OrderloAscendingYValues.NameBarYValues.ValueSourcenumero    TDBChartDBChartFattPrevLeft Top~�Width#Height� Gradient.EndColor �� Gradient.Visible	Title.Font.ColorclBlackTitle.Font.Height�Title.Font.StylefsBold Title.Text.StringsFatturato Totale BottomAxis.LabelsFont.Height�LeftAxis.VisibleLegend.VisibleAlignalTopTabOrder
OnDblClickDBChartCVInsClick 
TBarSeriesBarSeriesFattAzBarPen.StylepsDotMarks.ArrowLengthMarks.Callout.Brush.ColorclBlackMarks.Callout.DistanceMarks.Callout.LengthMarks.Brush.ColorclWhiteMarks.Brush.StylebsClearMarks.Font.ColorclRedMarks.Font.Height�Marks.Frame.VisibleMarks.StylesmsValueMarks.Visible	
DataSource
QAndFattAzSeriesColorclRedXLabelsSourcemeseBarStyle
bsCilinderBarWidthPercentGradient.DirectiongdTopBottomXValues.NameXXValues.OrderloAscendingYValues.NameBarYValues.ValueSourcenumero  
TBarSeriesBarSeriesFattAzASBarPen.StylepsDotMarks.ArrowLength	Marks.Callout.Brush.ColorclBlackMarks.Callout.Distance
Marks.Callout.Length	Marks.Brush.ColorclWhiteMarks.Brush.StylebsClearMarks.Font.Height�Marks.Frame.VisibleMarks.StylesmsValueMarks.Visible	
DataSourceQAndFattAZASSeriesColorclLimeXLabelsSourcemeseBarStyle
bsCilinderBarWidthPercentGradient.DirectiongdTopBottomXValues.NameXXValues.OrderloAscendingYValues.NameBarYValues.ValueSourcenumero   TPanelPanel8Left Top��Width#Height� AlignalTopCaptionPanel1TabOrder	 TDBChart	DBChart13LeftTopWidth�Height� Gradient.EndColor �� Gradient.Visible	Title.Font.ColorclBlackTitle.Font.Height�Title.Font.StylefsBold Title.Text.Strings2Presentazioni Utente connesso (rosso anno attuale) BottomAxis.LabelsFont.Height�LeftAxis.VisibleLegend.VisibleAlignalClientTabOrder 
OnDblClickDBChartCVInsClick 
TBarSeriesBarSeriesPresUCPerMeseBarPen.StylepsDotMarks.ArrowLengthMarks.Callout.Brush.ColorclBlackMarks.Callout.DistanceMarks.Callout.LengthMarks.Brush.ColorclWhiteMarks.Brush.StylebsClearMarks.Font.ColorclRedMarks.Font.Height�Marks.Frame.VisibleMarks.StylesmsValueMarks.Visible	
DataSourceQNrPresPerMeseUCSeriesColorclRedXLabelsSourcemeseBarStyle
bsCilinderBarWidthPercentGradient.DirectiongdTopBottomXValues.NameXXValues.OrderloAscendingYValues.NameBarYValues.ValueSourcenumero  
TBarSeriesSeriesNrPresUCPerMeseASBarPen.StylepsDotMarks.ArrowLength	Marks.Callout.Brush.ColorclBlackMarks.Callout.Distance
Marks.Callout.Length	Marks.Brush.ColorclWhiteMarks.Brush.StylebsClearMarks.Frame.VisibleMarks.Visible	
DataSourceQNrPresPerMeseUCASSeriesColorclLimeXLabelsSourcemeseBarStyle
bsCilinderBarWidthPercentGradient.DirectiongdTopBottomXValues.NameXXValues.OrderloAscendingYValues.NameBarYValues.ValueSourcenumero   TDBChart	DBChart14Left�TopWidth@Height� Gradient.EndColor �� Gradient.Visible	Title.Font.ColorclBlackTitle.Text.StringsTotale per Anno BottomAxis.LabelsAngle-BottomAxis.LabelsFont.Height�BottomAxis.MaximumOffset
BottomAxis.MinimumOffset
LeftAxis.VisibleLegend.VisibleAlignalRightTabOrder 
TBarSeriesBarSeriesNrPresPerAnnoUCBarPen.StylepsDotDepthMarks.ArrowLengthMarks.Callout.Brush.ColorclBlackMarks.Callout.Distance�Marks.Callout.LengthMarks.Brush.ColorclWhiteMarks.Brush.StylebsClearMarks.Font.Height�Marks.Frame.VisibleMarks.StylesmsValueMarks.Visible	
DataSourceQNrPresPerAnnoUCSeriesColorclRedXLabelsSourceannoBarStyle
bsCilinderBarWidthPercentGradient.DirectiongdTopBottomXValues.NameXXValues.OrderloAscendingYValues.NameBarYValues.ValueSourcenumero    TPanelPanel9Left Top&�Width#Height� AlignalTopCaptionPanel1TabOrder
 TDBChart	DBChart15LeftTopWidth�Height� Gradient.EndColor �� Gradient.Visible	Title.Font.ColorclBlackTitle.Font.Height�Title.Font.StylefsBold Title.Text.StringsCommesse Utente connesso  BottomAxis.LabelsFont.Height�LeftAxis.VisibleLegend.VisibleAlignalClientTabOrder 
OnDblClickDBChartCVInsClickPrintMargins''  
TBarSeriesBarSeriesCommUCBarPen.StylepsDotMarks.ArrowLengthMarks.Callout.Brush.ColorclBlackMarks.Callout.DistanceMarks.Callout.LengthMarks.Brush.ColorclWhiteMarks.Brush.StylebsClearMarks.Font.ColorclRedMarks.Font.Height�Marks.Frame.VisibleMarks.StylesmsValueMarks.Visible	
DataSourceQCommAperteUCSeriesColorclRedXLabelsSourcemeseBarStyle
bsCilinderBarWidthPercentGradient.DirectiongdTopBottomXValues.NameXXValues.OrderloAscendingYValues.NameBarYValues.ValueSourcenumero  
TBarSeriesSeries1BarPen.StylepsDotMarks.ArrowLength	Marks.Callout.Brush.ColorclBlackMarks.Callout.Distance
Marks.Callout.Length	Marks.Brush.ColorclWhiteMarks.Brush.StylebsClearMarks.Font.Height�Marks.Frame.VisibleMarks.StylesmsValueMarks.Visible	
DataSourceQCommAperteASUCSeriesColorclLimeXLabelsSourcemeseBarStyle
bsCilinderBarWidthPercentGradient.DirectiongdTopBottomXValues.NameXXValues.OrderloAscendingYValues.NameBarYValues.ValueSourcenumero   TDBChart	DBChart16Left�TopWidth@Height� Gradient.EndColor �� Gradient.Visible	Title.Font.ColorclBlackTitle.Text.StringsTotale per Anno BottomAxis.LabelsAngle-BottomAxis.LabelsFont.Height�BottomAxis.MaximumOffset
BottomAxis.MinimumOffset
LeftAxis.VisibleLegend.VisibleAlignalRightTabOrder 
TBarSeriesBarSeriesCommApUCPerAnnoBarPen.StylepsDotDepthMarks.ArrowLengthMarks.Callout.Brush.ColorclBlackMarks.Callout.Distance�Marks.Callout.LengthMarks.Brush.ColorclWhiteMarks.Brush.StylebsClearMarks.Font.Height�Marks.Frame.VisibleMarks.StylesmsValueMarks.Visible	
DataSourceQCommAperteUCPerAnnoSeriesColorclRedXLabelsSourceannoBarStyle
bsCilinderBarWidthPercentGradient.DirectiongdTopBottomXValues.NameXXValues.OrderloAscendingYValues.NameBarYValues.ValueSourcenumero    TPanelPanel10Left Top��Width#Height� AlignalTopCaptionPanel1TabOrder TDBChart	DBChart17LeftTopWidth�Height� Gradient.EndColor �� Gradient.Visible	Title.Font.ColorclBlackTitle.Font.Height�Title.Font.StylefsBold Title.Text.Strings-Colloqui Utente connesso (rosso anno attuale) BottomAxis.LabelsFont.Height�LeftAxis.VisibleLegend.VisibleAlignalClientTabOrder 
OnDblClickDBChartCVInsClick 
TBarSeriesBarSeriesNrCollUCBarPen.StylepsDotMarks.ArrowLengthMarks.Callout.Brush.ColorclBlackMarks.Callout.DistanceMarks.Callout.LengthMarks.Brush.ColorclWhiteMarks.Brush.StylebsClearMarks.Font.ColorclRedMarks.Font.Height�Marks.Frame.VisibleMarks.StylesmsValueMarks.Visible	
DataSource	QNrCollUCSeriesColorclRedXLabelsSourcemeseBarStyle
bsCilinderBarWidthPercentGradient.DirectiongdTopBottomXValues.NameXXValues.OrderloAscendingYValues.NameBarYValues.ValueSourcenumero  
TBarSeriesBarSeriesNrCollUCASBarPen.StylepsDotMarks.ArrowLength	Marks.Callout.Brush.ColorclBlackMarks.Callout.Distance
Marks.Callout.Length	Marks.Brush.ColorclWhiteMarks.Brush.StylebsClearMarks.Font.Height�Marks.Frame.VisibleMarks.StylesmsValueMarks.Visible	
DataSourceQNrCollUCASSeriesColorclLimeXLabelsSourcemeseBarStyle
bsCilinderBarWidthPercentGradient.DirectiongdTopBottomXValues.NameXXValues.OrderloAscendingYValues.NameBarYValues.ValueSourcenumero   TDBChart	DBChart18Left�TopWidth@Height� Gradient.EndColor �� Gradient.Visible	Title.Font.ColorclBlackTitle.Text.StringsTotale per Anno BottomAxis.LabelsAngle-BottomAxis.LabelsFont.Height�BottomAxis.MaximumOffset
BottomAxis.MinimumOffset
LeftAxis.VisibleLegend.VisibleAlignalRightTabOrder 
TBarSeriesSeriesCollUCPerAnnoBarPen.StylepsDotMarks.ArrowLengthMarks.Callout.Brush.ColorclBlackMarks.Callout.LengthMarks.Brush.ColorclWhiteMarks.Brush.StylebsClearMarks.Frame.VisibleMarks.StylesmsValueMarks.Visible	
DataSourceQNrCollUCPerAnnoSeriesColorclRedXLabelsSourceannoBarStyle
bsCilinderBarWidthPercentGradient.DirectiongdTopBottomXValues.NameXXValues.OrderloAscendingYValues.NameBarYValues.ValueSourcenumero    TPanelPanel7Left Topd�Width#Height� AlignalTopCaptionPanel1TabOrder TDBChartDBChartPresLeftTopWidth�Height� Gradient.EndColor �� Gradient.Visible	Title.Font.ColorclBlackTitle.Font.Height�Title.Font.StylefsBold Title.Text.Strings*Presentazioni Azienda (rosso anno attuale) BottomAxis.LabelsFont.Height�LeftAxis.VisibleLegend.VisibleAlignalClientTabOrder 
OnDblClickDBChartCVInsClick 
TBarSeriesBarSeriesPresPerMeseBarPen.StylepsDotMarks.ArrowLengthMarks.Callout.Brush.ColorclBlackMarks.Callout.DistanceMarks.Callout.LengthMarks.Brush.ColorclWhiteMarks.Brush.StylebsClearMarks.Font.ColorclRedMarks.Font.Height�Marks.Frame.VisibleMarks.StylesmsValueMarks.Visible	
DataSourceQNrPresPerMeseSeriesColorclRedXLabelsSourcemeseBarStyle
bsCilinderBarWidthPercentGradient.DirectiongdTopBottomXValues.NameXXValues.OrderloAscendingYValues.NameBarYValues.ValueSourcenumero  
TBarSeriesBarSeriesPresPerMeseASBarPen.StylepsDotMarks.ArrowLength	Marks.Callout.Brush.ColorclBlackMarks.Callout.Distance
Marks.Callout.Length	Marks.Brush.ColorclWhiteMarks.Brush.StylebsClearMarks.Font.Height�Marks.Frame.VisibleMarks.StylesmsValueMarks.Visible	
DataSourceQNrPresPerMeseASSeriesColorclLimeXLabelsSourcemeseBarStyle
bsCilinderBarWidthPercentGradient.DirectiongdTopBottomXValues.NameXXValues.OrderloAscendingYValues.NameBarYValues.ValueSourcenumero   TDBChart	DBChart12Left�TopWidth@Height� Gradient.EndColor �� Gradient.Visible	Title.Font.ColorclBlackTitle.Text.StringsTotale per Anno BottomAxis.LabelsAngle-BottomAxis.LabelsFont.Height�BottomAxis.MaximumOffset
BottomAxis.MinimumOffset
LeftAxis.VisibleLegend.VisibleAlignalRightTabOrder 
TBarSeriesBarSeriesNrPresPerAnnoAzBarPen.StylepsDotDepthMarks.ArrowLengthMarks.Callout.Brush.ColorclBlackMarks.Callout.Distance�Marks.Callout.LengthMarks.Brush.ColorclWhiteMarks.Brush.StylebsClearMarks.Font.Height�Marks.Frame.VisibleMarks.StylesmsValueMarks.Visible	
DataSourceQNrPresPerAnnoSeriesColorclRedXLabelsSourceannoBarStyle
bsCilinderBarWidthPercentGradient.DirectiongdTopBottomXValues.NameXXValues.OrderloAscendingYValues.NameBarYValues.ValueSourcenumero    TPanelPanel11Left Top��Width#Height� AlignalTopCaptionPanel1TabOrder TDBChart	DBChart19LeftTopWidth�Height� Gradient.EndColor �� Gradient.Visible	Title.Font.ColorclBlackTitle.Font.Height�Title.Font.StylefsBold Title.Text.StringsColloqui Azienda  BottomAxis.LabelsFont.Height�LeftAxis.VisibleLegend.VisibleAlignalClientTabOrder 
OnDblClickDBChartCVInsClick 
TBarSeriesBarSeriesCollAzBarPen.StylepsDotMarks.ArrowLengthMarks.Callout.Brush.ColorclBlackMarks.Callout.DistanceMarks.Callout.LengthMarks.Brush.ColorclWhiteMarks.Brush.StylebsClearMarks.Font.ColorclRedMarks.Font.Height�Marks.Frame.VisibleMarks.StylesmsValueMarks.Visible	
DataSourceQNrColloquiAzSeriesColorclRedXLabelsSourcemeseBarStyle
bsCilinderBarWidthPercentGradient.DirectiongdTopBottomXValues.NameXXValues.OrderloAscendingYValues.NameBarYValues.ValueSourcenumero  
TBarSeriesBarSeriesCollAzASBarPen.StylepsDotMarks.ArrowLength	Marks.Callout.Brush.ColorclBlackMarks.Callout.Distance
Marks.Callout.Length	Marks.Brush.ColorclWhiteMarks.Brush.StylebsClearMarks.Frame.VisibleMarks.StylesmsValueMarks.Visible	
DataSourceQNrColloquiAzASSeriesColorclLimeXLabelsSourcemeseBarStyle
bsCilinderBarWidthPercentGradient.DirectiongdTopBottomXValues.NameXXValues.OrderloAscendingYValues.NameBarYValues.ValueSourcenumero   TDBChart	DBChart20Left�TopWidth@Height� Gradient.EndColor �� Gradient.Visible	Title.Font.ColorclBlackTitle.Text.StringsTotale per Anno BottomAxis.LabelsAngle-BottomAxis.LabelsFont.Height�BottomAxis.MaximumOffset
BottomAxis.MinimumOffset
LeftAxis.VisibleLegend.VisibleAlignalRightTabOrder 
TBarSeriesBarSeriesNrColloquiAzPerAnnoBarPen.StylepsDotDepthMarks.ArrowLengthMarks.Callout.Brush.ColorclBlackMarks.Callout.Distance�Marks.Callout.LengthMarks.Brush.ColorclWhiteMarks.Brush.StylebsClearMarks.Font.Height�Marks.Frame.VisibleMarks.StylesmsValueMarks.Visible	
DataSourceQNrColloquiAzPerAnnoSeriesColorclRedXLabelsSourceannoBarStyle
bsCilinderBarWidthPercentGradient.DirectiongdTopBottomXValues.NameXXValues.OrderloAscendingYValues.NameBarYValues.ValueSourcenumero    TPanelPanel12Left Top��Width#Height� AlignalTopCaptionPanel1TabOrder TDBChart	DBChart21LeftTopWidth�Height� Gradient.EndColor �� Gradient.Visible	Title.Font.ColorclBlackTitle.Font.Height�Title.Font.StylefsBold Title.Text.StringsCommesse Azienda BottomAxis.LabelsFont.Height�LeftAxis.VisibleLegend.VisibleAlignalClientTabOrder 
OnDblClickDBChartCVInsClick 
TBarSeriesBarSeriesCommAzBarPen.StylepsDotMarks.ArrowLengthMarks.Callout.Brush.ColorclBlackMarks.Callout.DistanceMarks.Callout.LengthMarks.Brush.ColorclWhiteMarks.Brush.StylebsClearMarks.Font.ColorclRedMarks.Font.Height�Marks.Frame.VisibleMarks.StylesmsValueMarks.Visible	
DataSourceQCommAperteAzSeriesColorclRedXLabelsSourcemeseBarStyle
bsCilinderBarWidthPercentGradient.DirectiongdTopBottomXValues.NameXXValues.OrderloAscendingYValues.NameBarYValues.ValueSourcenumero  
TBarSeriesBarSeriesCommAperteAzASBarPen.StylepsDotMarks.ArrowLength	Marks.Callout.Brush.ColorclBlackMarks.Callout.Distance
Marks.Callout.Length	Marks.Brush.ColorclWhiteMarks.Brush.StylebsClearMarks.Frame.VisibleMarks.StylesmsValueMarks.Visible	
DataSourceQCommAperteAZASSeriesColorclLimeXLabelsSourcemeseBarStyle
bsCilinderBarWidthPercentGradient.DirectiongdTopBottomXValues.NameXXValues.OrderloAscendingYValues.NameBarYValues.ValueSourcenumero   TDBChart	DBChart22Left�TopWidth@Height� Gradient.EndColor �� Gradient.Visible	Title.Font.ColorclBlackTitle.Text.StringsTotale per Anno BottomAxis.LabelsAngle-BottomAxis.LabelsFont.Height�BottomAxis.MaximumOffset
BottomAxis.MinimumOffset
LeftAxis.VisibleLegend.VisibleAlignalRightTabOrder 
TBarSeriesBarSeries35BarPen.StylepsDotDepthMarks.ArrowLengthMarks.Callout.Brush.ColorclBlackMarks.Callout.Distance�Marks.Callout.LengthMarks.Brush.ColorclWhiteMarks.Brush.StylebsClearMarks.Font.Height�Marks.Frame.VisibleMarks.StylesmsValueMarks.Visible	
DataSourceQCommAperteAzPerAnnoSeriesColorclRedXLabelsSourceannoBarStyle
bsCilinderBarWidthPercentGradient.DirectiongdTopBottomXValues.NameXXValues.OrderloAscendingYValues.NameBarYValues.ValueSourcenumero      	TTabSheet	TabSheet2CaptionAnalisi Utenti
ImageIndexParentShowHintShowHint TPanelPanel2Left Top Width7HeightQAlignalTopTabOrder  TLabelLabel1LeftTopWidth HeightCaptionUtente  TLabelLabel2LeftTopWidthHeightCaptionDal  TLabelLabel3Left� TopWidth	HeightCaptionAl  TLabelLabel4LeftTop0WidthHeightCaptionuLa somma delle singole attivit� sono riferite ai singoli mesi
del periodo selezionato. Non si discriminano gli anni!Font.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TDBLookupComboBoxDBLookupComboBox1Left8TopWidth� HeightKeyFieldid	ListField
nominativo
ListSource	DSQUtentiTabOrder   TButtonButton2LeftTop0WidthKHeightCaptionAggiornaTabOrderOnClickButton2Click  TDateTimePickerDTDalLeft7TopWidthpHeightCalAlignmentdtaLeftDateۆ�6�L�@Timeۆ�6�L�@
DateFormatdfShortDateMode
dmComboBoxKinddtkDate
ParseInputTabOrder  TDateTimePickerDTAlLeft� TopWidthqHeightCalAlignmentdtaLeftDate��Y�s�L�@Time��Y�s�L�@
DateFormatdfShortDateMode
dmComboBoxKinddtkDate
ParseInputTabOrder   TPanelPanel5Left TopQWidth7Height�AlignalClientTabOrder TAdvStringGriddataGridLeftTopWidth5Height�AlignalClientColCountDefaultRowHeight	FixedCols RowCount	PopupMenu
PopupMenu1TabOrder AutoNumAlignAutoSize
VAlignmentvtaTopEnhTextSizeEnhRowColMoveSortFixedColsSizeWithFormMultilinecellsSortDirectionsdAscendingSortFull	SortAutoFormat	SortShowEnableGraphics
SortColumn 	HintColorclYellowSelectionColorclHighlightSelectionTextColorclHighlightTextSelectionRectangleSelectionRTFKeepHintShowCellsOleDropTargetOleDropSource
OleDropRTFPrintSettings.FooterSize PrintSettings.HeaderSize PrintSettings.TimeppNonePrintSettings.DateppNonePrintSettings.DateFormat
dd/mm/yyyyPrintSettings.PageNrppNonePrintSettings.TitleppNonePrintSettings.Font.CharsetDEFAULT_CHARSETPrintSettings.Font.ColorclWindowTextPrintSettings.Font.Height�PrintSettings.Font.NameMS Sans SerifPrintSettings.Font.Style  PrintSettings.HeaderFont.CharsetDEFAULT_CHARSETPrintSettings.HeaderFont.ColorclWindowTextPrintSettings.HeaderFont.Height�PrintSettings.HeaderFont.NameMS Sans SerifPrintSettings.HeaderFont.Style  PrintSettings.FooterFont.CharsetDEFAULT_CHARSETPrintSettings.FooterFont.ColorclWindowTextPrintSettings.FooterFont.Height�PrintSettings.FooterFont.NameMS Sans SerifPrintSettings.FooterFont.Style PrintSettings.Borders
pbNoborderPrintSettings.BorderStylepsSolidPrintSettings.CenteredPrintSettings.RepeatFixedRowsPrintSettings.RepeatFixedColsPrintSettings.LeftSize PrintSettings.RightSize PrintSettings.ColumnSpacing PrintSettings.RowSpacing PrintSettings.TitleSpacing PrintSettings.Orientation
poPortraitPrintSettings.FixedWidth PrintSettings.FixedHeight PrintSettings.UseFixedHeightPrintSettings.UseFixedWidthPrintSettings.FitToPagefpNeverPrintSettings.PageNumSep/PrintSettings.NoAutoSizePrintSettings.PrintGraphicsHTMLSettings.WidthdNavigation.AllowInsertRowNavigation.AllowDeleteRowNavigation.AdvanceOnEnterNavigation.AdvanceInsertNavigation.AutoGotoWhenSortedNavigation.AutoGotoIncrementalNavigation.AutoComboDropSizeNavigation.AdvanceDirectionadLeftRight"Navigation.AllowClipboardShortCutsNavigation.AllowSmartClipboardNavigation.AllowRTFClipboardNavigation.AdvanceAutoNavigation.InsertPositionpInsertBeforeNavigation.CursorWalkEditorNavigation.MoveRowOnSortNavigation.ImproveMaskSelNavigation.AlwaysEditColumnSize.SaveColumnSize.Stretch	ColumnSize.Location
clRegistryCellNode.ColorclSilverCellNode.NodeTypecnFlatCellNode.NodeColorclBlackSizeWhileTyping.HeightSizeWhileTyping.WidthMouseActions.AllSelectMouseActions.ColSelectMouseActions.RowSelectMouseActions.DirectEditMouseActions.DisjunctRowSelectMouseActions.AllColumnSizeMouseActions.CaretPositioning
IntelliPan
ipVerticalURLColorclBlueURLShowURLFullURLEdit
ScrollTypessNormalScrollColorclNoneScrollWidthScrollProportionalScrollHintsshNone
OemConvertFixedFooters FixedRightCols FixedColWidth@FixedRowHeightFixedFont.CharsetDEFAULT_CHARSETFixedFont.ColorclWindowTextFixedFont.Height�FixedFont.NameMS Sans SerifFixedFont.Style FixedAsButtonsFloatFormat%.2fWordWrapLookupLookupCaseSensitiveLookupHistoryHideFocusRectBackGround.Top BackGround.Left BackGround.DisplaybdTileHoveringFilter FilterActive	ColWidths@@@@@@@@@@@@�  
RowHeights       	TADOQueryQCVInseriti
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect SUM  (numero) numero,mese,anno,nrmesefrom (select count (id) numero,$datename (mm,cvinseritoindata) mese,YEAR (cvinseritoindata) anno,MONTH (cvinseritoindata) nrmesefrom anagrafica0where YEAR (cvinseritoindata) = YEAR (getdate())group by cvinseritoindata) aagroup by mese,nrmese,annoorder by anno,nrmese LeftTop` TIntegerFieldQCVInseritinumero	FieldNamenumeroReadOnly	  TWideStringFieldQCVInseritimese	FieldNamemeseSize   	TADOQueryQAzIns
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect SUM  (numero) numero,mese,anno,nrmesefrom(select count (id) numero,% datename (mm,conosciutoindata) mese,YEAR (conosciutoindata) anno,MONTH (conosciutoindata) nrmesefrom ebc_clienti0where YEAR (conosciutoindata) = YEAR (getdate())group by conosciutoindata) aagroup by mese,nrmese,annoorder by anno,nrmese  Left8Top`  	TADOQuery
ClientiAtt
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect sum (numero) numero, mese, nrmese, annofrom (select count (id) numero,&  datename (mm,conosciutoindata) mese,  YEAR (conosciutoindata) anno,  MONTH (conosciutoindata) nrmesefrom ebc_clientiwhere idtipoazienda=1group by conosciutoindata5having YEAR (conosciutoindata) = YEAR (getdate())) aagroup by mese,nrmese,annoorder by anno,nrmese Left`Top`  	TADOQueryFatturatoxAnno
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Strings select sum (ecr.importo) numero,  YEAR (data) anno,from fatture f join ebc_compensiricerche ecr  on ecr.idfattura=f.idgroup by YEAR (data)order by YEAR (data) Left� Top` TIntegerFieldFatturatoanno	FieldNameanno  TFloatFieldFatturatonumero	FieldNamenumeroReadOnly	   	TADOQueryNrFatturexAnno
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Strings"select count (progressivo) numero,  YEAR (data) annofrom fatturegroup by YEAR (data)order by YEAR(data) Left� Top| TIntegerFieldNrFatturenumero	FieldNamenumeroReadOnly	  TIntegerFieldNrFattureanno	FieldNameanno   	TADOQueryQCVTot
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Strings%select count (id) tot from anagrafica LeftTop�  TIntegerField	QCVTottotDefaultExpression0	FieldNametotReadOnly	   	TADOQueryQAzTot
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect count (id) totfrom ebc_Clienti Left8Top�  TIntegerField	QAzTottotDefaultExpression0	FieldNametotReadOnly	   	TADOQuery	QAzAttTot
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect count (id) totfrom ebc_Clientiwhere idtipoazienda=1 Left_Top�  TIntegerFieldQAzAttTottotDefaultExpression0	FieldNametotReadOnly	   	TADOQueryQFattAnnoAttuale
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect sum (ecr.importo) tot,from fatture f join ebc_compensiricerche ecr  on ecr.idfattura=f.id!where year (data)=year(GETDATE()) Left@Top TFloatFieldQFatttotDefaultExpression0	FieldNametotReadOnly	
EditFormat###0,00   	TADOQueryQNrFatt
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect count (progressivo) totfrom fatture!where year (data)=year(GETDATE()) Left`Top TIntegerField
QNrFatttotDefaultExpression0	FieldNametotReadOnly	   TDataSourceDSCVTotDataSetQCVTotLeftTop�   TDataSourceDSQAzTotDataSetQAzTotLeft8Top�   TDataSourceDSQAzAttTotDataSet	QAzAttTotLeft_Top�   TDataSourceDSFattDataSetQFattAnnoAttualeLeft@TopH  TDataSource	DSQNrFattDataSetQNrFattLeft`TopH  	TADOQueryQFattAnnoScorso
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect sum (ecr.importo) tot,from fatture f join ebc_compensiricerche ecr  on ecr.idfattura = f.idwhere data between2'01/01/'+ CAST (YEAR (GETDATE())-1 AS VARCHAR) andsCAST (DAY (GETDATE()) AS VARCHAR)+'/'+CAST (MONTH (GETDATE()) AS VARCHAR)+'/'+ CAST (YEAR (GETDATE())-1 AS VARCHAR)  Left@Top, TFloatFieldQFattAnnoScorsotot	FieldNametotReadOnly	   TDataSourceDSFattASDataSetQFattAnnoScorsoLeft@Topd  	TADOQuery	QNRFattAS
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect count (progressivo) totfrom fatture%where (year (data)=year(GETDATE())-1)#and (MONTH(data)<=MONTH(GETDATE()))and (DAY(data)<=DAY(GETDATE())) Left`Top, TIntegerFieldQNRFattAStot	FieldNametotReadOnly	   TDataSource
DSNrFattASDataSet	QNRFattASLeft`Topd  	TADOQueryQNrCollUltimoMese
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect count(id) totfrom ebc_Colloqui3where datediff (dd,data,getdate()) between 0 and 30 Left�Top�   TDataSourceDSNrCollDataSetQNrCollUltimoMeseLeft�Top�   	TADOQueryQNrCollUltimoMeseAS
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect count(id) totfrom ebc_Colloquiwhere datediff (dd,data,(CAST (DAY (getdate()) AS VARCHAR) + '/'+,  CAST (MONTH (getdate()) AS VARCHAR) + '/'+7  CAST (YEAR (getdate())-1 AS VARCHAR))between 0 and 30 Left�Top�   TDataSourceDSQNrCollASDataSetQNrCollUltimoMeseASLeft�Top  	TADOQueryQNrPresPerMese
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Strings+select count(distinct idanagrafica) numero,DATENAME (mm,dataevento) mesefrom storico7where ((idevento=76) or (idevento=83) or (idevento=20))(and YEAR (dataevento) = YEAR (getdate())3group by DATENAME (mm,dataevento),MONTH(dataevento)order by  MONTH (dataevento) LeftxTop`  TDataSourceDSNrPresDataSetQNrPresUltimoMeseLeftxTop�   	TADOQueryQNrPresPerMeseAS
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Strings+select count(distinct idanagrafica) numero,DATENAME (mm,dataevento) mesefrom storico7where ((idevento=76) or (idevento=83) or (idevento=20))*and YEAR (dataevento) = YEAR (getdate())-13group by DATENAME (mm,dataevento),MONTH(dataevento)order by  MONTH (dataevento) LeftxTop| TIntegerFieldQNrPresPerMeseASnumero	FieldNamenumeroReadOnly	  TWideStringFieldQNrPresPerMeseASmese	FieldNamemeseSize   TDataSourceDSNrPressASDataSetQNrPresUltimoMeseASLeftxTop  	TADOQueryQNROff
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect count(id) totfrom ebc_offerte4where datediff (dd,data,getdate()) between 0 and 30 group by YEAR (data) LeftPTop` TIntegerFieldIntegerField4	FieldNametotReadOnly	   TDataSourceDSNROffDataSetQNROffLeftPTop�   TDataSource	DSNrOffASDataSetQNrOffASLeftPTop�   	TADOQueryQNrOffAS
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect count(id) totfrom ebc_offerte�where datediff (dd,data,CAST (DAY (getdate()) AS VARCHAR) + '/'+  CAST (MONTH (getdate()) AS VARCHAR) + '/'+CAST (YEAR (getdate())-1 AS VARCHAR)) between 0 and 30 "and YEAR (data)= YEAR(GETDATE())-1 LeftPTop| TIntegerFieldIntegerField5	FieldNametotReadOnly	   	TADOQueryClientiATTAS
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect SUM (numero) numero,mese,anno,nrmesefrom (select count (id) numero,&  datename (mm,conosciutoindata) mese,  YEAR (conosciutoindata) anno,!  MONTH (conosciutoindata) nrmesefrom ebc_clientiwhere idtipoazienda=1group by conosciutoindata7having YEAR (conosciutoindata) = YEAR (getdate())-1) aagroup by mese,nrmese,annoorder by anno,nrmese Left_Top|  	TADOQueryQCVInseritiASAutoCalcFields
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect SUM  (numero) numero,mese,anno,nrmesefrom (select count (id) numero,$datename (mm,cvinseritoindata) mese,YEAR (cvinseritoindata) anno,MONTH (cvinseritoindata) nrmesefrom anagrafica2where YEAR (cvinseritoindata) = YEAR (getdate())-1group by cvinseritoindata) aagroup by mese,nrmese,annoorder by anno,nrmese LeftTop| TIntegerFieldIntegerField6	FieldNamenumeroReadOnly	  TWideStringFieldWideStringField1	FieldNamemeseSize   	TADOQueryQAzInsAS
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect SUM  (numero) numero,mese,anno,nrmesefrom(select count (id) numero,% datename (mm,conosciutoindata) mese,YEAR (conosciutoindata) anno,MONTH (conosciutoindata) nrmesefrom ebc_clienti2where YEAR (conosciutoindata) = YEAR (getdate())-1group by conosciutoindata) aagroup by mese,nrmese,annoorder by anno,nrmese  Left8Top|  	TADOQueryQAndFattSingoloUt
ConnectionData.DB
CursorTypectStatic
ParametersNameidUtenteDataType	ftIntegerSize�Value   SQL.Stringsselect SUM(numero) numero,anno,nrmese,mesefrom  (select sum(ecr.importo) numero,  MONTH (data) nrmese,  YEAR (data) anno,  DATENAME (mm,data) mesefrom )  fatture f join ebc_compensiricerche ecr    on f.id =ecr.idfattura  join ebc_ricerche er    on er.id=ecr.idricercawhere er.idutente=:idUtentegroup by  data)having YEAR (data) = YEAR (getdate())) aagroup by nrmese,mese,annoorder by anno,nrmese Left� Top_  	TADOQueryQAndFattSingoloUtAS
ConnectionData.DB
ParametersNameidUtenteDataType	ftIntegerSize�Value   SQL.Stringsselect SUM(numero) numero,anno,nrmese,mesefrom  (select sum(ecr.importo) numero,  MONTH (data) nrmese,  YEAR (data) anno,  DATENAME (mm,data) mese-from fatture f  join ebc_compensiricerche ecr    on f.id =ecr.idfattura  join ebc_ricerche er    on er.id=ecr.idricercawhere er.idutente=:idUtentegroup by  datahaving data is not null(and YEAR (data) = YEAR (getdate())-1) aagroup by nrmese,mese,annoorder by anno,nrmese Left� Top{  	TADOQueryQAndFattPUtenteCon
ConnectionData.DB
CursorTypectStatic
ParametersName	xidUtente
AttributespaSigned
paNullable DataType	ftInteger	Precision
SizeValue   SQL.Stringsselect sum(ecr.importo) numero,  MONTH (dataprevfatt) nrmese,  YEAR (dataprevfatt) anno,!  DATENAME (mm,dataprevfatt) mesefrom /  ebc_compensiricerche ecr join ebc_ricerche er    on er.id=ecr.idricercawhere er.idutente=:xidUtente9and datediff (mm,getdate(),dataprevfatt)between -2 and 11!group by    MONTH (dataprevfatt),    DATENAME (mm,dataprevfatt),  YEAR (dataprevfatt) order by anno,nrmese Left� Top_  	TADOQueryQAndFattPrevistoTot
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect sum(ecr.importo) numero,  MONTH (dataprevfatt) nrmese,"  DATENAME (mm,dataprevfatt) mese,  YEAR (dataprevfatt) annofrom /  ebc_compensiricerche ecr join ebc_ricerche er    on er.id=ecr.idricerca<where datediff (mm,getdate(),dataprevfatt)between -2  and 11Ngroup by  MONTH (dataprevfatt),DATENAME (mm,dataprevfatt), YEAR (dataprevfatt)order by anno,nrmese LeftTop_  	TADOQueryQTotCVPerAnno
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect count(id) numero,YEAR (cvinseritoindata) annofrom anagrafica2where datediff (yy,cvinseritoindata,getdate()) <=1 group by YEAR (cvinseritoindata) LeftTop�   	TADOQueryQTotClientiPerAnno
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect count(id) numero,YEAR (conosciutoindata) annofrom ebc_clienti2where datediff (yy,conosciutoindata,getdate()) <=1 group by YEAR (conosciutoindata) Left8Top�   	TADOQueryQTotCliAttPerAnno
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect count(id) numero,YEAR (conosciutoindata) annofrom ebc_clienti2where datediff (yy,conosciutoindata,getdate()) <=1and idtipoazienda=1 group by YEAR (conosciutoindata) Left_Top�   	TADOQueryQNrPresPerAnno
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Strings+select count(distinct idanagrafica) numero,YEAR (dataevento) annofrom storico7where ((idevento=76) or (idevento=83) or (idevento=20))+and DATEDIFF (yy,dataevento,getdate()) <=1 group by YEAR (dataevento)order by  YEAR (dataevento) LeftxTop�   	TADOQueryQNrPresPerMeseUC
ConnectionData.DB
CursorTypectStatic
ParametersNameidutenteDataType	ftIntegerSize�Value   SQL.Strings+select count(distinct idanagrafica) numero,DATENAME (mm,dataevento) mesefrom storico7where ((idevento=76) or (idevento=83) or (idevento=20))(and YEAR (dataevento) = YEAR (getdate())and idutente=:idutente3group by DATENAME (mm,dataevento),MONTH(dataevento)order by  MONTH (dataevento) Left(Top`  	TADOQueryQNrPresPerMeseUCAS
ConnectionData.DB
CursorTypectStatic
ParametersNameidutenteDataType	ftIntegerSize�Value   SQL.Strings+select count(distinct idanagrafica) numero,DATENAME (mm,dataevento) mesefrom storico7where ((idevento=76) or (idevento=83) or (idevento=20))*and YEAR (dataevento) = YEAR (getdate())-1and idutente=:idutente3group by DATENAME (mm,dataevento),MONTH(dataevento)order by  MONTH (dataevento) Left(Top| TIntegerFieldIntegerField1	FieldNamenumeroReadOnly	  TWideStringFieldWideStringField2	FieldNamemeseSize   	TADOQueryQNrPresPerAnnoUC
ConnectionData.DB
CursorTypectStatic
ParametersNameidutenteDataType	ftIntegerSize�Value   SQL.Strings+select count(distinct idanagrafica) numero,YEAR (dataevento) annofrom storico7where ((idevento=76) or (idevento=83) or (idevento=20))+and DATEDIFF (yy,dataevento,getdate()) <=1 and idutente=:idutentegroup by YEAR (dataevento)order by  YEAR (dataevento) Left(Top�   	TADOQueryQNrPresUltimoMese
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Strings'select count(distinct idanagrafica) totfrom storico7where ((idevento=76) or (idevento=83) or (idevento=20))7and DATEDIFF (dd,dataevento,getdate()) between 0 and 30 LeftxTop�  TIntegerFieldQNrPresUltimoMesetotDefaultExpression0	FieldNametotReadOnly	   	TADOQueryQNrPresUltimoMeseAS
ConnectionData.DB
Parameters SQL.Strings'select count(distinct idanagrafica) totfrom storico7where ((idevento=76) or (idevento=83) or (idevento=20))Eand DATEDIFF (dd,dataevento, CAST (DAY (getdate()) AS VARCHAR) + '/'++CAST (MONTH (getdate()) AS VARCHAR) + '/' +7CAST (YEAR (getdate()) -1 AS VARCHAR)) between 0 and 30 LeftxTop�  TIntegerFieldQNrPresUltimoMeseAStot	FieldNametotReadOnly	   	TADOQueryQNrColloquiAz
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Strings!select count(distinct id) numero,DATENAME (mm,data) mesefrom ebc_colloqui$where YEAR (data) = YEAR (getdate())'group by DATENAME (mm,data),MONTH(data)order by  MONTH (data) Left�Top`  	TADOQueryQNrColloquiAzAS
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Strings!select count(distinct id) numero,DATENAME (mm,data) mesefrom ebc_colloqui&where YEAR (data) = YEAR (getdate())-1'group by DATENAME (mm,data),MONTH(data)order by  MONTH (data) Left�Top| TIntegerFieldIntegerField2	FieldNamenumeroReadOnly	  TWideStringFieldWideStringField3	FieldNamemeseSize   	TADOQueryQNrColloquiAzPerAnno
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Strings!select count(distinct id) numero,YEAR (data) annofrom ebc_colloqui$where datediff(yy,data,getdate())<=1group by YEAR (data) Left�Top�   	TADOQuery	QNrCollUC
ConnectionData.DB
CursorTypectStatic
ParametersNameidutenteDataType	ftIntegerSize�Value   SQL.Strings!select count(distinct id) numero,DATENAME (mm,data) mesefrom ebc_colloqui$where YEAR (data) = YEAR (getdate())and idselezionatore=:idutente'group by DATENAME (mm,data),MONTH(data)order by  MONTH (data) Left�Top`  	TADOQueryQNrCollUCAS
ConnectionData.DB
CursorTypectStatic
ParametersNameidutenteDataType	ftIntegerSize�Value   SQL.Strings!select count(distinct id) numero,DATENAME (mm,data) mesefrom ebc_colloqui&where YEAR (data) = YEAR (getdate())-1and idselezionatore=:idutente'group by DATENAME (mm,data),MONTH(data)order by  MONTH (data) Left�Top| TIntegerFieldIntegerField3	FieldNamenumeroReadOnly	  TWideStringFieldWideStringField4	FieldNamemeseSize   	TADOQueryQNrCollUCPerAnno
ConnectionData.DB
CursorTypectStatic
ParametersNameidutenteDataType	ftIntegerSize�Value   SQL.Strings!select count(distinct id) numero,YEAR (data) annofrom ebc_colloqui$where datediff(yy,data,getdate())<=1and idselezionatore=:idutentegroup by YEAR (data) Left�Top�   	TADOQueryQCommAperteAz
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Strings!select count(distinct id) numero,DATENAME (mm,datainizio) mesefrom ebc_ricerche*where YEAR (datainizio) = YEAR (getdate())3group by DATENAME (mm,datainizio),MONTH(datainizio)order by  MONTH (datainizio) Left�Top`  	TADOQueryQCommAperteAZAS
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Strings!select count(distinct id) numero,DATENAME (mm,datainizio) mesefrom ebc_ricerche,where YEAR (datainizio) = YEAR (getdate())-13group by DATENAME (mm,datainizio),MONTH(datainizio)order by  MONTH (datainizio) Left�Top| TIntegerFieldIntegerField7	FieldNamenumeroReadOnly	  TWideStringFieldWideStringField5	FieldNamemeseSize   	TADOQueryQCommAperteAzPerAnno
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Strings!select count(distinct id) numero,YEAR (datainizio) annofrom ebc_ricerche*where datediff(yy,datainizio,getdate())<=1group by YEAR (datainizio) Left�Top�   	TADOQueryQCommAperteUC
ConnectionData.DB
CursorTypectStatic
ParametersNameidutenteDataType	ftIntegerSize�Value   SQL.Strings!select count(distinct id) numero,DATENAME (mm,datainizio) mesefrom ebc_ricerche*where YEAR (datainizio) = YEAR (getdate())and idutente=:idutente3group by DATENAME (mm,datainizio),MONTH(datainizio)order by  MONTH (datainizio) LeftTop`  	TADOQueryQCommAperteASUC
ConnectionData.DB
CursorTypectStatic
ParametersNameidutenteDataType	ftIntegerSize�Value   SQL.Strings!select count(distinct id) numero,DATENAME (mm,datainizio) mesefrom ebc_ricerche,where YEAR (datainizio) = YEAR (getdate())-1and idutente=:idutente3group by DATENAME (mm,datainizio),MONTH(datainizio)order by  MONTH (datainizio) LeftTop| TIntegerFieldIntegerField8	FieldNamenumeroReadOnly	  TWideStringFieldWideStringField6	FieldNamemeseSize   	TADOQueryQCommAperteUCPerAnno
ConnectionData.DB
CursorTypectStatic
ParametersNameidutenteDataType	ftIntegerSize�Value   SQL.Strings!select count(distinct id) numero,YEAR (datainizio) annofrom ebc_ricerche*where datediff(yy,datainizio,getdate())<=1and idutente=:idutentegroup by YEAR (datainizio) LeftTop�   	TADOQueryQAndFattPrevistoTotPerAnno
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect sum(ecr.importo) numero,  YEAR (dataprevfatt) annofrom /  ebc_compensiricerche ecr join ebc_ricerche er    on er.id=ecr.idricerca;where datediff (yy,dataprevfatt,getdate()) between -1 and 0group by YEAR (dataprevfatt)order by anno  LeftTop{  	TADOQueryQAndFattPUtenteConPerAnno
ConnectionData.DB
CursorTypectStatic
ParametersNameidutente
AttributespaSigned
paNullable DataType	ftInteger	Precision
SizeValue   SQL.Stringsselect sum(ecr.importo) numero,  YEAR (dataprevfatt) annofrom /  ebc_compensiricerche ecr join ebc_ricerche er    on er.id=ecr.idricercawhere er.idutente=:idutente8and datediff (yy,dataprevfatt,getdate())between -1 and 0group by YEAR (dataprevfatt) order by anno Left� Top{  	TADOQuery
QAndFattAz
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Strings!select sum (ecr.importo)  numero,  MONTH (data) nrmese,  DATENAME (mm,data) mesefrom *  fatture f join ebc_compensiricerche ecr     on f.id =ecr.idfatturawhere data is not null"and YEAR (data) = YEAR (getdate())*group by  MONTH (data),DATENAME (mm,data) order by nrmese Left� Top_  	TADOQueryQAndFattAZAS
ConnectionData.DB
Parameters SQL.Strings!select sum (ecr.importo)  numero,  MONTH (data) nrmese,  DATENAME (mm,data) mesefrom *  fatture f join ebc_compensiricerche ecr     on f.id =ecr.idfattura&where YEAR (data) = YEAR (getdate())-1*group by  month (data),DATENAME (mm,data) order by nrmese Left� Top{  	TADOQueryQAndFattUCPerAnno
ConnectionData.DB
CursorTypectStatic
ParametersNameidUtenteDataType	ftIntegerSize�Value   SQL.Stringsselect SUM(numero) numero,annofrom  (select sum(ecr.importo) numero,  YEAR (data) annofrom )  fatture f join ebc_compensiricerche ecr    on f.id =ecr.idfattura  join ebc_ricerche er    on er.id=ecr.idricercawhere er.idutente=:idUtentegroup by  datahaving data is not null5and datediff (yy,data,getdate()) between -2 and 0) aagroup by annoorder by anno Left� Top�   TChartEditorChartEditor1HideTabscetTools Left� Top�   TChartPreviewerChartPreviewer1Left� Top�   	TADOQueryQAppuntamenti
ConnectionData.DB
BeforeOpenQAppuntamentiBeforeOpen
DataSource	DSQUtenti
ParametersNameid
AttributespaSigned
paNullable DataType	ftInteger	Precision
SizeValue  NamealDataTypeftStringValue  NamedalDataTypeftStringValue  Nameid1Size�Value  Nameal1DataTypeftStringValue  Namedal1DataTypeftStringValue  Nameid2Size�Value  Nameal2DataTypeftStringValue  Namedal2DataTypeftStringSizeValue   SQL.StringsOselect datename (m,dataEVENTO) mese, MONTH (dataevento) nrmese,count (s.id) totfrom storico swhere idevento=27and idutente=:idIand Datediff (dd,dataevento,getdate()) >= Datediff (dd,:al,getdate()) and@datediff (dd,dataevento,getdate())<=Datediff (dd,:dal,getdate())4group by datename (m,dataevento), MONTH (dataevento)unionCselect datename (m,data) mese, MONTH (data) nrmese,count (a.id) totfrom agenda awhere tipo=1and idutente=:id1Dand Datediff (dd,data,getdate()) >= Datediff (dd,:al1,getdate()) and;datediff (dd,data,getdate())<=Datediff (dd,:dal1,getdate())and idcandric not in (select idanagraficafrom storicowhere idevento=27and idutente=:id2Jand Datediff (dd,dataevento,getdate()) >= Datediff (dd,:al2,getdate()) andBdatediff (dd,dataevento,getdate())<=Datediff (dd,:dal2,getdate()))(group by datename (m,data), MONTH (data) Left�Top TWideStringFieldQAppuntamentimese	FieldNamemeseSize  TIntegerFieldQAppuntamentinrmese	FieldNamenrmese  TIntegerFieldQAppuntamentitot	FieldNametotReadOnly	   	TADOQueryQUtenti
ConnectionData.DB
Parameters SQL.Stringsselect nominativo, id
from users Left�Top TStringFieldQUtentinominativo	FieldName
nominativoSize  TAutoIncField	QUtentiid	FieldNameidReadOnly	   TDataSource	DSQUtentiDataSetQUtentiLeft�Top  	TADOQueryQInserimenti
ConnectionData.DB
BeforeOpenQInserimentiBeforeOpen
DataSource	DSQUtenti
ParametersNameid
AttributespaSigned
paNullable DataType	ftInteger	Precision
SizeValue  NamealSize�Value  NamedalSize�Value   SQL.Stringsselect count (id) tot, Datename (m,dataora) mese, MONTH(dataora) nrmese from log_tableOpwhere tabella like 'Anagrafica'and idutente=:idiand Datediff (dd,dataora,getdate()) between Datediff (dd,:al,getdate()) and  Datediff (dd,:dal,getdate())and Operation='I'	group by  Datename (m,dataora), MONTH(dataora)order by nrmese Left�Top0  	TADOQueryQInserimentiRic
ConnectionData.DB
BeforeOpenQInserimentiBeforeOpen
DataSource	DSQUtenti
ParametersNameid
AttributespaSigned
paNullable DataType	ftInteger	Precision
SizeValue  NamealSize�Value  NamedalSize�Value   SQL.StringsOselect datename (m,dataEVENTO) mese, MONTH (dataevento) nrmese,count (s.id) totfrom storico swhere idevento in (19,63)and idutente=:idland Datediff (dd,dataevento,getdate()) between Datediff (dd,:al,getdate()) and  Datediff (dd,:dal,getdate())4group by datename (m,dataevento), MONTH (dataevento)order by nrmese Left�TopP  	TADOQueryQPresentazioni
ConnectionData.DB
BeforeOpenQInserimentiBeforeOpen
DataSource	DSQUtenti
ParametersNameid
AttributespaSigned
paNullable DataType	ftInteger	Precision
SizeValue  NamealSize�Value  NamedalSize�Value   SQL.StringsOselect datename (m,dataEVENTO) mese, MONTH (dataevento) nrmese,count (s.id) totfrom storico swhere idevento in (76,83,20)and idutente=:idland Datediff (dd,dataevento,getdate()) between Datediff (dd,:al,getdate()) and  Datediff (dd,:dal,getdate())4group by datename (m,dataevento), MONTH (dataevento)order by nrmese Left�Topp  	TADOQuery	QRicerche
ConnectionData.DB
BeforeOpenQRicercheBeforeOpen
DataSource	DSQUtenti
ParametersNameDal
Attributes
paNullable DataType
ftDateTime	PrecisionSizeValue  NameAl
Attributes
paNullable DataType
ftDateTime	PrecisionSizeValue  Nameid
AttributespaSigned
paNullable DataType	ftInteger	Precision
SizeValue   SQL.StringsKselect datename (m,datainizio) mese,MONTH (datainizio) nrmese,Count(ID) Totfrom EBC_Ricerche*where DataInizio>=:Dal and DataInizio<=:Aland idutente=:id5group by datename (m,datainizio) ,MONTH (datainizio)  Left�Top�  TWideStringFieldQRicerchemese	FieldNamemeseSize  TIntegerFieldQRicerchenrmese	FieldNamenrmese  TIntegerFieldQRicercheTot	FieldNameTotReadOnly	   	TADOQueryQRicercheChiuse
ConnectionData.DB
BeforeOpenQRicercheBeforeOpen
DataSource	DSQUtenti
ParametersNameAl
Attributes
paNullable DataType
ftDateTime	PrecisionSizeValue  NameDal
Attributes
paNullable DataType
ftDateTime	PrecisionSizeValue  Nameid
AttributespaSigned
paNullable DataType	ftInteger	Precision
SizeValue   SQL.StringsGselect datename (m,datafine) mese,MONTH (datafine) nrmese,Count(ID) Totfrom EBC_Ricercheowhere  (Datediff (dd,datafine,getdate()) between Datediff (dd,:al,getdate()) and  Datediff (dd,:dal,getdate()))and (idutente=:id)1group by datename (m,datafine) ,MONTH (datafine)  Left�Top�  TWideStringFieldQRicercheChiusemese	FieldNamemeseSize  TIntegerFieldQRicercheChiusenrmese	FieldNamenrmese  TIntegerFieldQRicercheChiuseTot	FieldNameTotReadOnly	   
TPopupMenu
PopupMenu1Left� Top�  	TMenuItemEsporta1CaptionEsporta 	TMenuItemFoglioExcel1CaptionFoglio ExcelOnClickFoglioExcel1Click  	TMenuItem
FoglioCSV1Caption
Foglio CSVOnClickFoglioCSV1Click   	TMenuItemCopia1CaptionCopia 	TMenuItemFormatoExcel1CaptionFormato ExcelOnClickFormatoExcel1Click    TSaveDialogSaveDialog1TitleEsporta Contenuto inLeft� Top�   	TADOQueryQFattUCPerMese
ConnectionData.DB
CursorTypectStatic
BeforeOpenQRicercheBeforeOpen
DataSource	DSQUtenti
ParametersNameidDataType	ftIntegerSize�Value  NamealDataTypeftStringSizeValue  NamedalDataTypeftStringSizeValue   SQL.Stringsselect SUM(numero) tot,nrmese,mesefrom  (select sum(ecr.importo) numero,  MONTH (data) nrmese,  DATENAME (mm,data) mesefrom )  fatture f join ebc_compensiricerche ecr    on f.id =ecr.idfattura  join ebc_ricerche er    on er.id=ecr.idricercawhere er.idutente=:idfand Datediff (dd,data,getdate()) between Datediff (dd,:al,getdate()) and  Datediff (dd,:dal,getdate())-group by  MONTH (data),DATENAME (mm,data) ) agroup by nrmese,meseorder by nrmese Left�Top�  TFloatFieldQFattUCPerMesetot	FieldNametotReadOnly	DisplayFormat###0.00  TIntegerFieldQFattUCPerMesenrmese	FieldNamenrmese  TWideStringFieldQFattUCPerMesemese	FieldNamemeseSize   	TADOQueryQOfferte
ConnectionData.DB
BeforeOpenQInserimentiBeforeOpen
DataSource	DSQUtenti
ParametersNameid
AttributespaSigned
paNullable DataType	ftInteger	Precision
SizeValue  NamealDataTypeftStringSizeValue  NamedalDataTypeftStringSizeValue   SQL.StringsCselect datename (m,data) mese, MONTH (data) nrmese,count (e.id) totAfrom ebc_offerte e join ebc_offerteutenti eu on eu.idofferta=e.idwhere eu.idutente=:idfand Datediff (dd,data,getdate()) between Datediff (dd,:al,getdate()) and  Datediff (dd,:dal,getdate())(group by datename (m,data), MONTH (data)order by nrmese Left�Top�  TWideStringFieldQOffertemese	FieldNamemeseSize  TIntegerFieldQOffertenrmese	FieldNamenrmese  TIntegerFieldQOffertetot	FieldNametotReadOnly	   	TADOQueryQEventiComm
ConnectionData.DB
BeforeOpenQRicercheBeforeOpen
DataSource	DSQUtenti
ParametersNameid
AttributespaSigned
paNullable DataType	ftInteger	Precision
SizeValue  NameAl
Attributes
paNullable DataType
ftDateTime	PrecisionSizeValue  NameDal
Attributes
paNullable DataType
ftDateTime	PrecisionSizeValue   SQL.StringsOselect datename (m,dataEVENTO) mese, MONTH (dataevento) nrmese,count (s.id) totfrom storico swhere idutente=:idand (idricerca is not null)land Datediff (dd,dataevento,getdate()) between Datediff (dd,:al,getdate()) and  Datediff (dd,:dal,getdate())4group by datename (m,dataevento), MONTH (dataevento)order by nrmese Left�Top TWideStringFieldWideStringField7	FieldNamemeseSize  TIntegerFieldIntegerField9	FieldNamenrmese  TIntegerFieldIntegerField10	FieldNameTotReadOnly	    