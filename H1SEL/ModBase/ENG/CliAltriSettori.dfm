�
 TCLIALTRISETTORIFORM 0�  TPF0TCliAltriSettoriFormCliAltriSettoriFormLeftaTopoBorderStylebsDialogCaptionAltri settori per l'aziendaClientHeight"ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterPixelsPerInch`
TextHeight TPanelPanel1Left Top Width�Height+AlignalTop
BevelOuter	bvLoweredTabOrder  TLabelLabel1LeftTopWidthSHeightCaptionAttivit� principale:  TDBTextDBText1LeftTopWidth� Height	DataFieldAttivita
DataSourceData2.DsEBCclientiFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TBitBtnBitBtn1LeftSTopWidthZHeight&CaptionEsciTabOrder KindbkOK  TBitBtnBitBtn2Left� TopWidthZHeight&CaptionAnnullaTabOrderVisibleKindbkCancel   TPanelPanel2LeftSTop+Width]Height� AlignalRight
BevelOuter	bvLoweredTabOrder TToolbarButton97BAnagFileNewLeftTopWidth[Height%Hint>cerca, associa e definisci il 
tipo per un file gi� esistenteCaptionAssocia settore
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333�3333�39�3330 373337w39�3330 3��337w9���33337ww333?9���333 7wws333w39�3333 373333w39�3333337s3333?3333333 3333�33w333<333 3337�33w333<�3333����?�<����Ù37wwwww�<����Ù37wwwwsw3333<�3333337s33�333<330 3337337w3333330 3333337w3333333333333333	NumGlyphsOpaqueParentShowHintShowHint	WordWrap	OnClickBAnagFileNewClick  TToolbarButton97BAnagFileDelLeftTop&Width[Height%CaptionElimina associaz.
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 30    3337wwww�330�wwp3337�����330���p3337�����330��pp3337�����330���p3337�����330��pp3337�����330���p333�������00��pp0377�����33 ���p33w����s330��pp3337�����330pppp3337�����33     33wwwww33��ww33����33     33wwwwws3330wp333337���33330  333337ww333	NumGlyphsOpaqueWordWrap	OnClickBAnagFileDelClick   	TdxDBGrid	dxDBGrid1Left Top+WidthSHeight� Bands  DefaultLayout	HeaderPanelRowCountKeyFieldIDSummaryGroups SummarySeparator, AlignalClientTabOrder
DataSourceDsQCliSettoriFilter.Criteria
       OptionsBehavioredgoAutoSortedgoDragScrolledgoEnterShowEditoredgoImmediateEditoredgoTabThroughedgoVertThrough 	OptionsDBedgoCancelOnExitedgoCanDeleteedgoCanInsertedgoCanNavigationedgoConfirmDeleteedgoLoadAllRecordsedgoUseBookmarks OptionsViewedgoAutoWidthedgoBandHeaderWidthedgoIndicatoredgoRowSelectedgoUseBitmap  TdxDBGridMaskColumndxDBGrid1IDClienteVisible	BandIndex RowIndex 	FieldName	IDCliente  TdxDBGridMaskColumndxDBGrid1AttivitaSortedcsUp	BandIndex RowIndex 	FieldNameAttivita  TdxDBGridMaskColumndxDBGrid1IDAttivitaVisible	BandIndex RowIndex 	FieldName
IDAttivita  TdxDBGridMaskColumndxDBGrid1IDVisible	BandIndex RowIndex 	FieldNameID  TdxDBGridMaskColumndxDBGrid1AreaSettoreCaptionArea	BandIndex RowIndex 	FieldNameAreaSettore   TDataSourceDsQCliSettoriDataSetQCliSettoriLeftxTop�   TADOLinkedQueryQCliSettoriActive	
ConnectionData.DB
CursorTypectStatic
DataSourceData2.DsEBCclienti
ParametersNameID
AttributespaSigned DataType	ftInteger	Precision
SizeValue   SQL.Strings@select EBC_ClientiAttivita.*, EBC_Attivita.Attivita, AreaSettore3from EBC_ClientiAttivita, EBC_Attivita, AreeSettori4where EBC_ClientiAttivita.IDAttivita=EBC_Attivita.ID=   and EBC_Attivita.IDAreaSettore *= AreeSettori.ID          and IDCliente=:ID  	UseFilterLeftxTop`   