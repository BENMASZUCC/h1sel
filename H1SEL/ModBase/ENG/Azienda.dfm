�
 TAZIENDAFORM 0�  TPF0TAziendaFormAziendaFormLeftTTopUBorderStylebsDialogCaptionAziendaClientHeight�ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TLabelLabel1LeftTopWidthGHeightCaptionNome azienda:  TLabelLabel2LeftTop Width)HeightCaption
Indirizzo:  TLabelLabel3LeftTop8WidthIHeightCaptionCap e Comune:  TLabelLabel5LeftTopPWidth/HeightCaption
Provincia:  TSpeedButtonSpeedButton1LeftYTop2WidthHeight
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333333333333333333333333�3333330 333����w330  0 �337ww7w�33�� � 33�3w�w33 ��� 33ws37�w30����� 3���37�w0  ��� 7wws37�w������ s����7�w0   �� 7wwws7�w333�   333s�www3330 3333337w3333333333333333333333333333333333333333333333333333333333333333333	NumGlyphsOnClickSpeedButton1Click  TLabelLabel8LeftTopPWidth*HeightCaptionNazione:  TSpeedButtonBNazLeft� TopKWidthHeightHintseleziona nazione
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� UUUUUUUUUUU���UUUUTLLUUUUU�ww_�UUT�D�DUUUWu�Uw_UULC4DDEUUuWUU�T��4��3UW�W�Uw�T��3DD3UWUWw�Uw_���3���EU�w�UUL�33��D�Www��U�333����www��UL�333<LEWwwwU��<�3<��u�u_w�u\��333LUW��www��T�3333�UWWwwww�UU33�<3�UUwwUuwuUUS<���UUUWu��wUUUU\L�UUUUUWwwUUU	NumGlyphsParentShowHintShowHint	OnClick	BNazClick  TLabelLabel4LeftTopiWidth-HeightCaption	Telefono:  TLabelLabel9Left� TopiWidthHeightCaptionFax:  TLabelLabel10LeftTop� Width;HeightCaptionSito internet:  TLabelLabel11LeftTop�Width� HeightCaptionDescrizione attivit� azienda:  TLabelLabel12LeftTop� WidthHeightCaptionStato  TLabelLabel13Left� Top� Width=HeightCaptionTipo azienda  TEditEDescrizioneLeft`TopWidthHeightCharCaseecUpperCaseFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 	MaxLength2
ParentFontTabOrder   TBitBtnBitBtn1LeftzTopWidth\Height"TabOrderKindbkOK  TBitBtnBitBtn2LeftzTop%Width\Height"CaptionAnnullaTabOrderKindbkCancel  TEditECapLeft`Top4Width)Height	MaxLengthTabOrder  TEditEComuneLeft� Top4Width� HeightCharCaseecUpperCase	MaxLengthTabOrder  TEditEProvLeft`TopLWidthHeightCharCaseecUpperCase	MaxLengthTabOrder  TDBGridDBGrid1LeftaTop� WidthHeight� 
DataSource	DsSettoriReadOnly	TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style 
OnKeyPressDBGrid1KeyPressColumnsExpanded	FieldNameAttivitaTitle.CaptionSettoreWidth� Visible	    TRadioGroupRG1LeftTop� WidthVHeight9CaptionSettore/attivit�:	ItemIndexItems.Stringsnessuno
da tabella TabOrder  	TGroupBox	GroupBox1LeftTop� WidthHeight+CaptionAltre informazioniTabOrder TLabelLabel6LeftTopWidth-HeightCaption
Fatturato:  TLabelLabel7Left� TopWidth@HeightCaptionN� dipendenti  
TFloatEditFENumDipLeft� TopWidth!HeightTabOrder Text0  TRxCalcEditFEFatturatoLeft=TopWidth`HeightTabStopAutoSizeDisplayFormat
�'.' ,0.00	NumGlyphsTabOrder   TEdit
EIndirizzoLeft� TopWidth� Height	MaxLength(TabOrder  	TComboBoxCBTipoStradaLeft`TopWidth@Height
ItemHeightTabOrder
OnDropDownCBTipoStradaDropDownItems.StringsViaVialePiazzaLargoCorso   TEdit
ENumCivicoLeftOTopWidth$Height	MaxLength
TabOrder  TEditENazioneLeft� TopLWidthHeightCharCaseecUpperCase	MaxLengthTabOrder  TEdit	ETelefonoLeft`TopfWidthyHeightTabOrder  TEditEFaxLeft� TopfWidthzHeightTabOrder	  TdxHyperLinkEditESitoInternetLeft`Top~WidthHint,fare doppio click per 
attivare l'hyperlinkParentShowHintShowHint	TabOrder
  TMemoEDescAttAziendaLeftTop�WidthkHeight4TabOrder  	TComboBoxcbStatoLeft`Top� WidthiHeight
ItemHeightTabOrderItems.Strings
contattatoattivo
non attivo	eliminato   	TComboBoxcbTipoAziendaLeftTop� WidthiHeight
ItemHeightTabOrderTextazienda esternaVisible
OnDropDowncbTipoAziendaDropDownItems.Strings
contattatoattivo
non attivo	eliminato   TDBGriddbgTipoAziendaLeftTop� WidthkHeight
DataSourcedsTipiAziendeOptionsdgColumnResize
dgColLines
dgRowLinesdgTabsdgConfirmDeletedgCancelOnExit TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameTipoAziendaWidthdVisible	    TDataSource	DsSettoriDataSetTSettoriLeft� Top  TADOLinkedQueryQ
ConnectionData.DB
Parameters 	UseFilterLeft(Top�   TADOLinkedQueryTSettoriActive	
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect * from EBC_Attivitaorder by Attivita 	UseFilterLeft� Top�  TAutoIncField
TSettoriID	FieldNameIDReadOnly	  TStringFieldTSettoriAttivita	FieldNameAttivita	FixedChar	Size2  TIntegerFieldTSettoriIDAreaSettore	FieldNameIDAreaSettore  TStringFieldTSettoriIDAzienda	FieldName	IDAzienda	FixedChar	Size
  TStringFieldTSettoriAttivita_ENG	FieldNameAttivita_ENG	FixedChar	Size2   	TADOQueryqTipiAziendaActive	
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect * from ebc_tipiaziendeorder by tipoazienda Left�Top�  TStringFieldqTipiAziendaTipoAzienda	FieldNameTipoAziendaSize  TAutoIncFieldqTipiAziendaID	FieldNameIDReadOnly	   TDataSourcedsTipiAziendeDataSetqTipiAziendaLeft�Top�    