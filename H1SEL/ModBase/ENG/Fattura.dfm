�
 TFATTURAFORM 0�D  TPF0TFatturaFormFatturaFormLeft� Top� BorderStylebsDialogCaptionFattura (o nota di accredito)ClientHeight%ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnClose	FormCloseOnShowFormShowPixelsPerInch`
TextHeight TPanelPanel1Left Top Width�Height,AlignalTopTabOrder  TBitBtnBitBtn1Left_TopWidthcHeight!CaptionOK-EsciTabOrder OnClickBitBtn1ClickKindbkOK  TBitBtnBitBtn2Left^TopWidth|Height!CaptionStampa con WordTabOrderOnClickBitBtn2Click
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 0      ?��������������wwwwwww�������wwwwwww        ���������������wwwwwww�������wwwwwww�������wwwwww        wwwwwww30����337���?330� 337�wss330����337��?�330�  337�swws330���3337��73330��3337�ss3330�� 33337��w33330  33337wws333	NumGlyphs  	TCheckBoxCBFronteLeft�TopWidthkHeightCaptionPagina copertinaTabOrder  	TCheckBoxCBNoteSpeseLeft�TopWidthyHeightCaptionallegato note speseTabOrder   TPanelPanel2Left TopBWidth�Height� AlignalTop
BevelOuter	bvLoweredTabOrder TLabelLabel1LeftTopWidth%HeightCaptionNumeroFocusControlDBEdit1  TLabelLabel2Left3TopWidthHeightCaptionTipo  TLabelLabel4LeftTopWidth HeightCaptionCliente  TLabelLabel5Left� TopWidthHeightCaptionData  TLabelLabel142LeftTop2WidthkHeightCaptionModalit� di pagamento  TLabelLabel6Left)Top2WidthZHeightCaptionAppoggio Bancario  TLabelLabel7LeftTop\Width7HeightCaption
Decorrenza  TLabelLabel8LeftdTop\Width0HeightCaptionScadenza  TLabelLabel9Left=Top\Width)HeightCaption	Pagata il  TLabelLabel10Left� Top\Width@HeightCaptionResponsabileFocusControlDBEdit4  TDBEditDBEdit1LeftTopWidth)Height	DataFieldProgressivo
DataSource	DsFatturaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder   TDBComboBoxDBComboBox1Left3TopWidth� Height	DataFieldTipo
DataSource	DsFattura
ItemHeightItems.StringsFatturaNota di accredito TabOrder  TDBEditDBEdit3LeftTopWidthLHeightTabStopColorclAqua	DataFieldDescrizione
DataSource	DsFatturaTabOrder  TDbDateEdit97DbDateEdit971Left� TopWidthXHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderColorCalendar.ColorValidclBlueDate��  DayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanPopup
doIsMaskeddoShowCanceldoShowToday 	DataFieldData
DataSource	DsFattura  TDBComboBoxDBComboBox3LeftTopBWidthHeightStylecsDropDownList	DataFieldModalitaPagamento
DataSource	DsFattura
ItemHeightItems.Strings$Rimessa Diretta - scadenza immediata!Rimessa Diretta - scadenza 30 gg.!Rimessa Diretta - scadenza 60 gg.!Rimessa Diretta - scadenza 90 gg.+Ricevuta Bancaria - scadenza 30 gg. DF f.m.+Ricevuta Bancaria - scadenza 60 gg. DF f.m.+Ricevuta Bancaria - scadenza 90 gg. DF f.m..Ricevuta Bancaria - scadenza 30/60 gg. DF f.m..Ricevuta Bancaria - scadenza 60/90 gg. DF f.m.come da offerta TabOrderOnChangeDBComboBox3Change  TDbDateEdit97DbDateEdit972LeftcToplWidthXHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderColorCalendar.ColorValidclBlueDate��  DayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanPopup
doIsMaskeddoShowCanceldoShowToday 	DataFieldScadenzaPagamento
DataSource	DsFattura  TDbDateEdit97DbDateEdit973LeftToplWidthXHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderColorCalendar.ColorValidclBlueDate��  DayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanPopup
doIsMaskeddoShowCanceldoShowToday 	DataField
Decorrenza
DataSource	DsFattura  TDbDateEdit97DbDateEdit974Left=ToplWidthiHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderColorCalendar.ColorValidclBlueDate��  DayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanClear
doCanPopup
doIsMaskeddoShowCanceldoShowToday 	DataFieldPagataInData
DataSource	DsFattura  	TGroupBox	GBRifProgLeftbTopWidthAHeight-CaptionRif.Fatt.N�TabOrder	 TDBEditDBEdit2LeftTopWidth1Height	DataFieldRifProg
DataSource	DsFatturaTabOrder    TDBComboBoxDBCBAppoggioLeft(TopBWidth|Height	DataFieldAppoggioBancario
DataSource	DsFattura
ItemHeightTabOrder
  TDBEditDBEdit4Left� ToplWidth!Height	DataFieldResponsabile
DataSource	DsFatturaTabOrder  TDBCheckBoxDBCBFlagIVALeftTop� WidthBHeightCaption=Fuori campo applicazione IVA (art.7, co.4, lett.D DPR 633/72)	DataFieldFlagFuoriAppIVA63372
DataSource	DsFatturaTabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCBRimbSpeseLeftTop� Width� HeightCaptionConsidera come "rimborso spese"	DataField	RimbSpese
DataSource	DsFatturaTabOrderValueCheckedTrueValueUncheckedFalse   TPanelPanel3Left Top,Width�HeightAlignalTop	AlignmenttaLeftJustifyCaption  Dati fatturaColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TPanelPanel4Left Top� Width�HeightAlignalTop	AlignmenttaLeftJustifyCaption  Dettaglio fatturaColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TPanelPanel5Left TopWidth�Height$AlignalBottomTabOrder TLabelLabel3Left�Top
Width:HeightCaptionTOTALI:FocusControlDBEdit5Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TDBEditDBEdit5Left�TopWidthPHeight	DataFieldImporto
DataSource	DsFatturaTabOrder   TDBEditDBEdit6LeftgTopWidthNHeight	DataFieldTotale
DataSource	DsFatturaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder   TPanelPanel6Left TopWidth�Height� AlignalClient
BevelOuter	bvLoweredTabOrder TDBGridDBGrid1LeftTop)Width�Height� AlignalClient
DataSource
DsFattDettReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameDescrizioneWidth�Visible	 Expanded	FieldName
ImponibileTitle.AlignmenttaRightJustifyWidthSVisible	 	AlignmenttaCenterExpanded	FieldNamePercentualeIVATitle.AlignmenttaCenterTitle.CaptionIVA %Width#Visible	 Expanded	FieldNameTotaleFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold Title.AlignmenttaRightJustifyTitle.Font.CharsetDEFAULT_CHARSETTitle.Font.ColorclWindowTextTitle.Font.Height�Title.Font.NameMS Sans SerifTitle.Font.StylefsBold WidthMVisible	    TPanelPanel7LeftTopWidth�Height(AlignalTopTabOrder TToolbarButton97TbBFattDettNewLeftTopWidthTHeight&CaptionNuovo dettaglio
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 33333333?�������        wwwwwwww�������?��?��� � � �w7sw7w7�������?��?��� � � �w7sw7w7�������?��?��� � � �w7sw7w7�������?��?��� � � �w7sw7w7���������������������wwwwwwww�����Ȁ�wwww�        wwwwwwww33333333333333333333333333333333	NumGlyphsWordWrap	OnClickTbBFattDettNewClick  TToolbarButton97TbBFattDettDelLeft� TopWidthTHeight&CaptionElimina dettaglio
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 30    3337wwww�330�wwp3337�����330���p3337�����330��pp3337�����330���p3337�����330��pp3337�����330���p333�������00��pp0377�����33 ���p33w����s330��pp3337�����330pppp3337�����33     33wwwww33��ww33����33     33wwwwws3330wp333337���33330  333337ww333	NumGlyphsWordWrap	OnClickTbBFattDettDelClick  TToolbarButton97TbBFattDettModLeft� TopWidthTHeight&CaptionModifica dettaglio
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 333     333wwwww333����?���??� 0  � �w�ww?sw7������ws3?33�࿿ ���w�3ws��7�������w�3?��7࿿  �w�3wwss7��������w�?���37�   ��w�wwws?� � �� �ws�w73w730 ���37wss3?�330���  33773�ww33��33s7s730���37�33s3	�� 33ws��w3303   3373wwws3	NumGlyphsWordWrap	OnClickTbBFattDettModClick  TToolbarButton97ToolbarButton971LeftUTopWidthTHeight&CaptionNuovo generico
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333�3333�39�3330 373337w39�3330 3��337w9���33337ww333?9���333 7wws333w39�3333 373333w39�3333337s3333?3333333 3333�33w333<333 3337�33w333<�3333����?�<����Ù37wwwww�<����Ù37wwwwsw3333<�3333337s33�333<330 3337337w3333330 3333337w3333333333333333	NumGlyphsWordWrap	OnClickToolbarButton971Click    TDataSource	DsFatturaDataSetTFatturaLeft�Top  TDataSource
DsFattDettDataSet	TFattDettLeft(Top}  TADOLinkedQuery	TFattDett
ConnectionData.DBOnCalcFieldsTFattDett_OLDCalcFields
Parameters SQL.Stringsselect * from FattDett MasterDataSetTFatturaLinkedMasterFieldIDLinkedDetailField	IDFatturaOriginalSQL.Stringsselect * from FattDettwhere IDFattura=:ID: 	UseFilterLeft(Top; TAutoIncFieldTFattDettID	FieldNameIDReadOnly	  TIntegerFieldTFattDettIDFattura	FieldName	IDFattura  TStringFieldTFattDettDescrizione	FieldNameDescrizioneSize�   TFloatFieldTFattDettImponibile	FieldName
ImponibileDisplayFormat#,###.00  TFloatFieldTFattDettPercentualeIVA	FieldNamePercentualeIVA  TIntegerFieldTFattDettIDCompenso	FieldName
IDCompenso  TIntegerFieldTFattDettIDAnnuncio	FieldName
IDAnnuncio  TFloatFieldTFattDettTotale	FieldKindfkCalculated	FieldNameTotaleDisplayFormat#,###.00
Calculated	   TADOLinkedQueryTFattura
ConnectionData.DB
ParametersNamexID:
AttributespaSigned DataType	ftInteger	Precision
SizeValue   SQL.Strings"select * from Fatture,EBC_Clienti &where Fatture.IDCliente=EBC_Clienti.IDand Fatture.ID=:xID: OriginalSQL.Strings"select * from Fatture,EBC_Clienti &where Fatture.IDCliente=EBC_Clienti.IDand Fatture.ID=:xID: 	UseFilterLeft�Topb TAutoIncField
TFatturaID	FieldNameIDReadOnly	  TIntegerFieldTFatturaProgressivo	FieldNameProgressivo  TStringFieldTFatturaTipo	FieldNameTipo  TIntegerFieldTFatturaRifProg	FieldNameRifProg  TIntegerFieldTFatturaIDCliente	FieldName	IDCliente  TDateTimeFieldTFatturaData	FieldNameData  TDateTimeFieldTFatturaDecorrenza	FieldName
Decorrenza  TFloatFieldTFatturaImporto	FieldNameImportoDisplayFormat#,###.00  TFloatFieldTFatturaIVA	FieldNameIVA  TFloatFieldTFatturaTotale	FieldNameTotaleDisplayFormat#,###.00  TStringFieldTFatturaModalitaPagamento	FieldNameModalitaPagamentoSize<  TStringFieldTFatturaAssegno	FieldNameAssegnoSize  TDateTimeFieldTFatturaScadenzaPagamento	FieldNameScadenzaPagamento  TBooleanFieldTFatturaPagata	FieldNamePagata  TDateTimeFieldTFatturaPagataInData	FieldNamePagataInData  TStringFieldTFatturaAppoggioBancario	FieldNameAppoggioBancarioSizeP  TIntegerFieldTFatturaIDRicerca	FieldName	IDRicerca  TStringFieldTFatturaResponsabile	FieldNameResponsabile	FixedChar	SizeP  TBooleanFieldTFatturaFlagFuoriAppIVA63372	FieldNameFlagFuoriAppIVA63372  TAutoIncFieldTFatturaID_1	FieldNameID_1ReadOnly	  TStringFieldTFatturaDescrizione	FieldNameDescrizioneSize2  TStringFieldTFatturaStato	FieldNameStatoSize  TStringFieldTFatturaIndirizzo	FieldName	IndirizzoSize(  TStringFieldTFatturaCap	FieldNameCapSize  TStringFieldTFatturaComune	FieldNameComune  TStringFieldTFatturaProvincia	FieldName	ProvinciaSize  TIntegerFieldTFatturaIDAttivita	FieldName
IDAttivita  TStringFieldTFatturaTelefono	FieldNameTelefonoSize  TStringFieldTFatturaFax	FieldNameFaxSize  TStringFieldTFatturaPartitaIVA	FieldName
PartitaIVASize  TStringFieldTFatturaCodiceFiscale	FieldNameCodiceFiscaleSize  TStringFieldTFatturaBancaAppoggio	FieldNameBancaAppoggioSizeP  TStringFieldTFatturaSistemaPagamento	FieldNameSistemaPagamentoSize<  
TMemoFieldTFatturaNoteContratto	FieldNameNoteContrattoBlobTypeftMemo  TStringFieldTFatturaResponsabile_1	FieldNameResponsabile_1Size(  TDateTimeFieldTFatturaConosciutoInData	FieldNameConosciutoInData  TStringFieldTFatturaIndirizzoLegale	FieldNameIndirizzoLegaleSize(  TStringFieldTFatturaCapLegale	FieldName	CapLegaleSize  TStringFieldTFatturaComuneLegale	FieldNameComuneLegale  TStringFieldTFatturaProvinciaLegale	FieldNameProvinciaLegaleSize  TStringFieldTFatturaCartellaDoc	FieldNameCartellaDocSizeP  TStringFieldTFatturaTipoStrada	FieldName
TipoStradaSize
  TStringFieldTFatturaTipoStradaLegale	FieldNameTipoStradaLegaleSize
  TStringFieldTFatturaNumCivico	FieldName	NumCivicoSize
  TStringFieldTFatturaNumCivicoLegale	FieldNameNumCivicoLegaleSize
  TStringFieldTFatturaNazioneAbb	FieldName
NazioneAbbSize  TStringFieldTFatturaNazioneAbbLegale	FieldNameNazioneAbbLegaleSize  
TMemoFieldTFatturaNote	FieldNameNoteBlobTypeftMemo  TFloatFieldTFatturaFatturato	FieldName	Fatturato  TSmallintFieldTFatturaNumDipendenti	FieldNameNumDipendenti  TBooleanFieldTFatturaBlocco1	FieldNameBlocco1  TStringFieldTFatturaEnteCertificatore	FieldNameEnteCertificatoreSized  TBooleanFieldTFatturaLibPrivacy	FieldName
LibPrivacy  TIntegerFieldTFatturaIdAzienda	FieldName	IdAzienda  TStringFieldTFatturaSitoInternet	FieldNameSitoInternet	FixedChar	Size2  TStringFieldTFatturaDescAttivitaAzienda	FieldNameDescAttivitaAzienda	FixedChar	Size�   TIntegerFieldTFatturaDallAnno	FieldNameDallAnno  TStringFieldTFatturaParentCompany	FieldNameParentCompany	FixedChar	Size2  TBooleanFieldTFatturaRimbSpese	FieldName	RimbSpese   TADOLinkedQueryQNoteSpeseFatt
ConnectionData.DB
Parameters SQL.Stringsselect * from RicNoteSpese MasterDataSetTFatturaLinkedMasterFieldIDLinkedDetailField	IDFatturaOriginalSQL.Stringsselect * from RicNoteSpesewhere IDFattura=:ID: 	UseFilterLeft�Top�   TADOLinkedQueryQTemp
ConnectionData.DB
Parameters 	UseFilterLeftTop�   TADOLinkedQuery	QAppoggio
ConnectionData.DB
Parameters SQL.Strings<select AppoggioBancario1,AppoggioBancario2,AppoggioBancario3from Global 	UseFilterLefthTop�  TStringFieldQAppoggioAppoggioBancario1	FieldNameAppoggioBancario1SizeP  TStringFieldQAppoggioAppoggioBancario2	FieldNameAppoggioBancario2SizeP  TStringFieldQAppoggioAppoggioBancario3	FieldNameAppoggioBancario3SizeP   TThreadADOLinkedTableTRicercheABS
ConnectionData.DB
CursorTypectStatic	TableNameEBC_RicercheLeft8Top�    