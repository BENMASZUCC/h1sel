�
 TCOLLOQUIOFORM 0�,  TPF0TColloquioFormColloquioFormLeft� Top� Width�Height0Caption!Scheda di valutazione (Colloquio)Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style PositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TPanelPanel1Left Top Width�HeightXAlignalTopTabOrder  TLabelLabel14LeftTop
WidthHeightCaptionEt�:  TBitBtnBitBtn1Left~TopWidth_HeightCaption
CurriculumFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder OnClickBitBtn1Click  	TGroupBox	GroupBox3LeftTopWidth�Height:AlignalBottomCaptionRicercaColor	clBtnFaceParentColorTabOrder TLabelLabel5LeftTopWidthHeightCaptionRif.FocusControlDBEdit2  TLabelLabel11Left0TopWidth HeightCaptionCliente  TLabelLabel12Left� TopWidthHeightCaptionStatoFocusControlDBEdit19  TLabelLabel13LeftbTopWidth@HeightCaptionResponsabile  TSpeedButtonSpeedButton1Left�TopWidth"HeightHintInformazioni sulla ricercaCaptionInfoParentShowHintShowHint	OnClickSpeedButton1Click  TDBEditDBEdit2LeftTopWidth$HeightColor	clBtnFace	DataFieldProgressivo
DataSourceDataSel_EBC.DsRicerchePendReadOnly	TabOrder   TDBEditDBEdit5Left/TopWidth� HeightColor	clBtnFace	DataFieldCliente
DataSourceDataSel_EBC.DsRicerchePendReadOnly	TabOrder  TDBEditDBEdit19Left� TopWidth� HeightColor	clBtnFace	DataFieldStato
DataSourceDataSel_EBC.DsRicerchePendReadOnly	TabOrder  TDBEditDBEdit20LeftbTopWidthQHeightColor	clBtnFace	DataFieldSelezionatore
DataSourceDataSel_EBC.DsRicerchePendReadOnly	TabOrder   TDBEditDBEdit12LeftTopWidth� HeightColorclYellow	DataFieldCognome
DataSourceData.DsAnagraficaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontReadOnly	TabOrder  TDBEditDBEdit14Left� TopWidthnHeightColorclYellow	DataFieldNome
DataSourceData.DsAnagraficaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontReadOnly	TabOrder  TDBEditDBEdit18Left'TopWidthHeightTabStop	DataFieldEta
DataSourceData.DsAnagraficaFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontReadOnly	TabOrder   TPageControlPageControl1Left TopXWidth�Heightp
ActivePageTsCaratteristicheAlignalClientTabOrder 	TTabSheet
TsDatiAnagCaptionDati Personali TLabelLabel38Left� Top� Width2HeightCaptionStatoCivileFocusControlDBLookupComboBox1  TLabelLabel49Left� Top� WidthBHeightCaptionCodice fiscaleFocusControlDBEdit17  TLabelLabel50Left� Top� Width2HeightCaptionPartita IVAFocusControlDBEdit23  	TGroupBox	GroupBox1LeftTop0Width� HeightdCaption	ResidenzaTabOrder  TLabelLabel6LeftTopWidth&HeightCaption	IndirizzoFocusControlDBEdit6  TLabelLabel7Left� TopWidthHeightCaptionCapFocusControlDBEdit7  TLabelLabel8LeftTop7Width'HeightCaptionComuneFocusControlDBEdit8  TLabelLabel9Left� Top7WidthHeightCaptionProv.FocusControlDBEdit9  TDBEditDBEdit6LeftTopWidth� Height	DataField	Indirizzo
DataSourceData.DsAnagraficaTabOrder   TDBEditDBEdit7Left� TopWidth(Height	DataFieldCap
DataSourceData.DsAnagraficaTabOrder  TDBEditDBEdit8LeftTopGWidth� Height	DataFieldComune
DataSourceData.DsAnagraficaTabOrder  TDBEditDBEdit9Left� TopFWidthHeight	DataField	Provincia
DataSourceData.DsAnagrafica	MaxLengthTabOrder   	TGroupBox	GroupBox2Left� Top0Width� HeightdCaption	DomicilioTabOrder TLabelLabel4LeftTopWidth&HeightCaption	IndirizzoFocusControlDBEdit3  TLabelLabel10Left� TopWidthHeightCaptionCapFocusControlDBEdit4  TLabelLabel32LeftTop7Width'HeightCaptionComuneFocusControlDBEdit10  TLabelLabel33Left� Top7WidthHeightCaptionProv.FocusControlDBEdit11  TDBEditDBEdit3LeftTopWidth� Height	DataFieldDomicilioIndirizzo
DataSourceData.DsAnagraficaTabOrder   TDBEditDBEdit4Left� TopWidth(Height	DataFieldDomicilioCap
DataSourceData.DsAnagraficaTabOrder  TDBEditDBEdit10LeftTopGWidth� Height	DataFieldDomicilioComune
DataSourceData.DsAnagraficaTabOrder  TDBEditDBEdit11Left� TopFWidthHeight	DataFieldDomicilioProvincia
DataSourceData.DsAnagraficaTabOrder  TBitBtnBitBtn12LeftITop
Width8HeightCaptioncopiaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   TDBLookupComboBoxDBLookupComboBox1Left� Top� WidthhHeight	DataFieldStatoCivile
DataSourceData.DsAnagraficaTabOrder  TDBEditDBEdit17Left� Top� WidthgHeight	DataFieldCodiceFiscale
DataSourceData.DsAnagraficaTabOrder  TDBEditDBEdit23Left� Top� WidthgHeight	DataField
PartitaIVA
DataSourceData.DsAnagraficaTabOrder  TPanelPanel9LeftTop� Width� Height� 
BevelInnerbvRaised
BevelOuter	bvLoweredTabOrder TLabelLabel46LeftTopWidthTHeightCaptionRecapiti telefoniciFocusControlDBEdit13  TLabelLabel47LeftTop/Width(HeightCaption	CellulareFocusControlDBEdit15  TLabelLabel48LeftTopWWidthHeightCaptionFaxFocusControlDBEdit16  TDBEditDBEdit13LeftTopWidth� Height	DataFieldRecapitiTelefonici
DataSourceData.DsAnagraficaTabOrder   TDBEditDBEdit15LeftTop?Width^Height	DataField	Cellulare
DataSourceData.DsAnagraficaTabOrder  TDBEditDBEdit16LeftTopgWidth^Height	DataFieldFax
DataSourceData.DsAnagraficaTabOrder   TPanel
Wallpaper3Left Top Width�Height'AlignalTopTabOrder TToolbarButton97	TbBAnagOKLeft�TopWidth2Height%CaptionOKEnabledOpaqueOnClickTbBAnagOKClick  TToolbarButton97
TbBAnagCanLeft1TopWidth2Height%CaptionAnnullaEnabledOpaqueOnClickTbBAnagCanClick    	TTabSheetTsCompetenzeCaption
Competenze TPanel
Wallpaper6Left Top Width�Height'AlignalTopTabOrder  TToolbarButton97TbBCompDipNewLeftTopWidth2Height%CaptionNuovaOpaqueOnClickTbBCompDipNewClick  TToolbarButton97TbBCompDipDelLeft3TopWidth2Height%CaptionEliminaOpaqueOnClickTbBCompDipDelClick  TToolbarButton97TbBCompDipOKLefteTopWidth2Height%CaptionOKEnabledOpaqueOnClickTbBCompDipOKClick  TToolbarButton97TbBCompDipCanLeft� TopWidth2Height%CaptionAnnullaEnabledOpaqueOnClickTbBCompDipCanClick   TDBGridDBGrid14Left Top'Width�Height-AlignalClient
DataSourceData2.DsCompDipendenteTabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style Columns	FieldNameDescCompetenzaTitle.CaptionDescriz.competenzaWidth�  	FieldName	TipologiaTitle.CaptionTipo 	AlignmenttaCenter	FieldNameValoreWidth-     	TTabSheetTsCaratteristicheCaptionCaratteristiche ricercate TDBGridDBGrid1Left Top'Width�HeightAlignalClient
DataSourceDataSel_EBC.DsCarattcandTabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style 
OnDblClickToolbarButton971ClickColumns	FieldNameProgressivoTitle.CaptionRicerca 	FieldNameDescrizione 	FieldName	PunteggioFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold Width5    TPanelPanel4Left Top+Width�Height)AlignalBottomTabOrder TLabelLabel1LeftTopWidthqHeightCaptionSomma punteggio:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TDBEditDBEdit1LeftxTop	Width2Height	DataFieldSUM OF Punteggio
DataSourceDataSel_EBC.DsSumPuntiCarattFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontReadOnly	TabOrder    TPanel
Wallpaper1Left Top Width�Height'AlignalTopTabOrder TToolbarButton97ToolbarButton971LeftTopWidthPHeight%CaptionModifica punteggioOpaqueWordWrap	OnClickToolbarButton971Click    	TTabSheet
TsTutteCarCaptionTutte le caratter. TDBGridDBGrid19Left Top'Width�Height-AlignalClient
DataSourceData2.DsCarattDipTabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style Columns	FieldNameCaratteristica 	FieldName	Punteggio    TPanel
Wallpaper7Left Top Width�Height'AlignalTopTabOrder TToolbarButton97TbBCarattDipNewLeftTopWidth2Height%CaptionNuovoOpaqueOnClickTbBCarattDipNewClick  TToolbarButton97TbBCarattDipDelLeft3TopWidth2Height%CaptionEliminaOpaqueOnClickTbBCarattDipDelClick  TToolbarButton97TbBCarattDipOKLefteTopWidth2Height%CaptionOKEnabledOpaqueOnClickTbBCarattDipOKClick  TToolbarButton97TbBCarattDipCanLeft� TopWidth2Height%CaptionAnnullaEnabledOpaqueOnClickTbBCarattDipCanClick     TPanelPanel2Left Top�Width�HeightMAlignalBottomTabOrder TLabelLabel2LeftTopWidth� HeightCaptionValutazione generale:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel3LeftTop-Width~HeightCaptionEsito finale colloquio:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TBitBtnBitBtn2LeftbTopWidthzHeightCaptionTermine colloquioDefault	ModalResultTabOrder OnClickBitBtn2Click
Glyph.Data
�  �  BM�      v   (   $            h                       �  �   �� �   � � ��  ��� ���   �  �   �� �   � � ��  ��� 333333333333333333  333333333333�33333  334C33333338�33333  33B$3333333�8�3333  34""C33333833�3333  3B""$33333�338�333  4"*""C3338�8�3�333  2"��"C3338�3�333  :*3:"$3338�38�8�33  3�33�"C333�33�3�33  3333:"$3333338�8�3  33333�"C333333�3�3  33333:"$3333338�8�  333333�"C333333�3�  333333:"C3333338�  3333333�#3333333��  3333333:3333333383  333333333333333333  	NumGlyphs  TBitBtnBitBtn3LeftbTop(WidthyHeightCaptionAnnullaTabOrderKindbkCancel  	TComboBox	ComboBox1Left� Top
Width� HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ItemHeightItems.Stringsottimodiscretosufficienteinsufficiente 
ParentFontTabOrder  	TComboBox	ComboBox2Left� Top)Width� HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ItemHeightItems.Stringsmantenere nella ricercaeliminare dalla ricercaaltro colloquio 
ParentFontTabOrderTextmantenere nella ricerca    