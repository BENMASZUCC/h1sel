unit AnagFile;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, ExtCtrls, Buttons, Mask, ToolEdit, Grids, DBGrids, Db, DBTables,
     ADODB, U_ADOLinkCl, DBCtrls, dxTL, dxDBCtrl, dxDBGrid, dxCntner,
     dxDBTLCl, dxGrClms, TB97;

type
     TAnagFileForm = class(TForm)
          Panel1: TPanel;
          Label1: TLabel;
          ECognome: TEdit;
          ENome: TEdit;
          DsQTipiFile: TDataSource;
          Label2: TLabel;
          EDesc: TEdit;
          Label3: TLabel;
          FEFile: TFilenameEdit;
          SpeedButton1: TSpeedButton;
          QTipiFile: TADOLinkedQuery;
          CBFlagInvio: TCheckBox;
          CBVisibleCliente: TCheckBox;
          QGlobal: TADOQuery;
          DsQGlobal: TDataSource;
          QGlobalAnagFileDentroDB: TBooleanField;
          QGlobalOpzione: TStringField;
          QTipiFileID: TAutoIncField;
          QTipiFileTipo: TStringField;
          QTipiFileOrdinamento: TIntegerField;
          QTipiFileDefaultVisibileWeb: TBooleanField;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1DefaultVisibileWeb: TdxDBGridCheckColumn;
          LInfo: TLabel;
          dxDBGrid1Tipo: TdxDBGridColumn;
          Panel2: TPanel;
          BitBtn1: TToolbarButton97;
          BAnnulla: TToolbarButton97;
          Label5: TLabel;
          DBText1: TDBText;
          BTipiFile: TToolbarButton97;
          SpeedButton2: TToolbarButton97;
          CBFlagEsportaCVGeneraPres: TCheckBox;
          procedure FormShow(Sender: TObject);
          procedure SpeedButton1Click(Sender: TObject);
          procedure QGlobalCalcFields(DataSet: TDataSet);
          procedure SpeedButton2Click(Sender: TObject);
          procedure FormCreate(Sender: TObject);
          procedure dxDBGrid1CustomDrawCell(Sender: TObject; ACanvas: TCanvas;
               ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
               ASelected, AFocused, ANewItemRow: Boolean; var AText: string;
               var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
               var ADone: Boolean);
          procedure BTipiFileClick(Sender: TObject);
          procedure BitBtn1Click(Sender: TObject);
          procedure BAnnullaClick(Sender: TObject);
          procedure dxDBGrid1KeyPress(Sender: TObject; var Key: Char);
          procedure EDescKeyPress(Sender: TObject; var Key: Char);
          procedure FEFileKeyPress(Sender: TObject; var Key: Char);
          procedure CBFlagInvioKeyPress(Sender: TObject; var Key: Char);
          procedure CBVisibleClienteKeyPress(Sender: TObject; var Key: Char);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     AnagFileForm: TAnagFileForm;

var f: boolean;

implementation

uses uUtilsVarie, uTipiFileCand, ModuloDati, Main;

{$R *.DFM}

procedure TAnagFileForm.FormShow(Sender: TObject);
begin
     //Grafica
     AnagFileForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     Caption := '[M/070] - ' + Caption;
     if pos('INTERMEDIA', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) > 0 then begin
          CBVisibleCliente.visible := false;
          CBFlagEsportaCVGeneraPres.Visible := true;
     end;
     f := false;
end;

procedure TAnagFileForm.SpeedButton1Click(Sender: TObject);
begin
     OpenTab('TipiFileCand', ['ID', 'Tipo', 'Ordinamento'], ['Tipo file', 'Ordine']);
     QTipiFile.Close;
     QTipiFile.SQL.Text := 'select * from TipiFileCand order by Ordinamento';
     QTipiFile.Open;
end;


procedure TAnagFileForm.QGlobalCalcFields(DataSet: TDataSet);
begin
     if QGlobalAnagFileDentroDB.value then
          QGlobalOpzione.value := 'File inserito nel database'
     else QGlobalOpzione.value := 'File salvato su file system';
end;

procedure TAnagFileForm.SpeedButton2Click(Sender: TObject);
begin
     ApriHelp('070');
end;

procedure TAnagFileForm.FormCreate(Sender: TObject);
begin
     QGlobal.Open;
     QTipiFile.Open;

     CBFlagInvio.checked := true;
     CBVisibleCliente.checked := true;
end;

procedure TAnagFileForm.dxDBGrid1CustomDrawCell(Sender: TObject;
     ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
     AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
     var AText: string; var AColor: TColor; AFont: TFont;
     var AAlignment: TAlignment; var ADone: Boolean);
begin
     if ANode.Values[dxDBGrid1DefaultVisibileWeb.index] = true then begin
          Afont.Color := clRed;
          f := true;
     end;

     LInfo.Visible := f;

end;

procedure TAnagFileForm.BTipiFileClick(Sender: TObject);
begin
     if not mainform.CheckProfile('074') then Exit;
     TipiFileCandForm := TTipiFileCandForm.Create(self);
     TipiFileCandForm.ShowModal;
     TipiFileCandForm.free;
     QTipiFile.Close;
     QTipiFile.Open;
end;

procedure TAnagFileForm.BitBtn1Click(Sender: TObject);
begin
     AnagFileForm.ModalResult := mrOK;
end;

procedure TAnagFileForm.BAnnullaClick(Sender: TObject);
begin
     AnagFileForm.ModalResult := mrCancel;
end;

procedure TAnagFileForm.dxDBGrid1KeyPress(Sender: TObject; var Key: Char);
begin
     //if key = chr(13) then BitBtn1Click(self);
end;

procedure TAnagFileForm.EDescKeyPress(Sender: TObject; var Key: Char);
begin
     if key = chr(13) then BitBtn1Click(self);
end;

procedure TAnagFileForm.FEFileKeyPress(Sender: TObject; var Key: Char);
begin
     if key = chr(13) then BitBtn1Click(self);
end;

procedure TAnagFileForm.CBFlagInvioKeyPress(Sender: TObject;
     var Key: Char);
begin
     if key = chr(13) then BitBtn1Click(self);
end;

procedure TAnagFileForm.CBVisibleClienteKeyPress(Sender: TObject;
     var Key: Char);
begin
     if key = chr(13) then BitBtn1Click(self);
end;

end.

