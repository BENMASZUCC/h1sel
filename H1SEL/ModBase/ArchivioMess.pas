unit ArchivioMess;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, DBTables, dxDBTLCl, dxGrClms, dxTL, dxDBCtrl, dxDBGrid, dxCntner,
     StdCtrls, Buttons, ExtCtrls, ADODB, U_ADOLinkCl;

type
     TArchivioMessForm = class(TForm)
          QMessLetti_OLD: TQuery;
          DsQMessLetti: TDataSource;
          Panel1: TPanel;
          BitBtn1: TBitBtn;
          dxDBGrid1: TdxDBGrid;
          QMessLetti_OLDID: TAutoIncField;
          QMessLetti_OLDIDUtente: TIntegerField;
          QMessLetti_OLDIDUtenteDa: TIntegerField;
          QMessLetti_OLDDataIns: TDateTimeField;
          QMessLetti_OLDDataDaLeggere: TDateTimeField;
          QMessLetti_OLDTesto: TStringField;
          QMessLetti_OLDDataLettura: TDateTimeField;
          QMessLetti_OLDEvaso: TBooleanField;
          QMessLetti_OLDIDCandUtente: TIntegerField;
          dxDBGrid1ID: TdxDBGridMaskColumn;
          dxDBGrid1IDUtente: TdxDBGridMaskColumn;
          dxDBGrid1IDUtenteDa: TdxDBGridMaskColumn;
          dxDBGrid1DataIns: TdxDBGridDateColumn;
          dxDBGrid1Testo: TdxDBGridMaskColumn;
          dxDBGrid1DataLettura: TdxDBGridDateColumn;
          dxDBGrid1IDCandUtente: TdxDBGridMaskColumn;
          BitBtn2: TBitBtn;
          QMessLetti: TADOLinkedQuery;
          procedure BitBtn2Click(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     ArchivioMessForm: TArchivioMessForm;

implementation

uses ModuloDati;

{$R *.DFM}

procedure TArchivioMessForm.BitBtn2Click(Sender: TObject);
begin
     if QMessLetti.IsEmpty then exit;
     if Messagedlg('Sei sicuro di voler eliminare DEFINITIVAMENTE il messaggio ?', mtWarning, [mbNo, mbYes], 0) = mrYes then begin
          with Data do begin
               //[/TONI20020725\]
               //               DB.BeginTrans;
               DB.BeginTrans;
               try
                    Q1.Close;
                    //                    Q1.SQL.text:='delete from Promemoria where ID='+QMessLettiID.AsString;
                    Q1.SQL.text := 'delete from Promemoria where ID=' + QMessLetti.FieldBYName('ID').AsString;
                    Q1.ExecSQL;
                    //                    DB.CommitTrans;
                    DB.CommitTrans;
               except
                    //                    DB.RollbackTrans;
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               end;
          end;
          QMessLetti.Close;
          QMessLetti.Open;
          //[/TONI20020725\]FINE
     end;
end;

end.
