unit UFunzioniEsterne;

interface

//uses Crypt2;

// richiamo DLL
function Cripta(a: PWideChar): PWideChar; stdcall; external 'ebccontrol.dll'; //ttwcript
function Decripta(a: PWideChar): PWideChar; stdcall; external 'ebccontrol.dll'; //ttwcript

function MD2Code(a: PWideChar): PWideChar; stdcall; external 'ebccontrol.dll'; //funzioni nella CheckPass.pas
function MD2Check(a, b: PWideChar): boolean; stdcall; external 'ebccontrol.dll'; //funzioni nella CheckPass.pas


 // 3DES////
function Cripta3DES(a: PWideChar): PWideChar; stdcall; external 'ebccontrol.dll';
function Decripta3DES(a: PWideChar): PWideChar; stdcall; external 'ebccontrol.dll';
 /////

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// funzioni che chiamano dll esterne

function CriptaStringa(xparola: string): string;
function DecriptaStringa(xparola: string): string;
function MD2Cripta(xparola: string): string;
function MD2Verifica(xparolaChiaro, xParolaCriptata: string): boolean;
 /// 3DES ////
function CriptaStringa3DES(xpsw: string): string;
function DecriptaStringa3DES(xpsw: string): string;
 ////


implementation

function CriptaStringa3DES(xpsw: string): string;
var myWideString: WideString;
     myWideCharPtr, r: PWideChar;
     xParolaCriptata: string;
begin

     myWideString := xpsw;
     myWideCharPtr := Addr(myWideString[1]);

     r := Cripta3DES(myWideCharPtr);
     xParolaCriptata := WideCharToString(r);
     result := xParolaCriptata;

end;


function DecriptaStringa3DES(xpsw: string): string;
var myWideString: WideString;
     myWideCharPtr, r: PWideChar;
     xParolaDecriptata: string;
begin

     myWideString := xpsw;
     myWideCharPtr := Addr(myWideString[1]);

     r := Decripta3DES(myWideCharPtr);
     xParolaDecriptata := WideCharToString(r);
     result := xParolaDecriptata;

end;




function MD2Cripta(xparola: string): string;
var myWideString: WideString;
     myWideCharPtr, r: PWideChar;
     xParolaCriptata: string;
begin
     myWideString := xparola;
     myWideCharPtr := Addr(myWideString[1]);
     r := MD2Code(myWideCharPtr);
     xParolaCriptata := WideCharToString(r);
     result := xParolaCriptata;
end;

function MD2Verifica(xparolaChiaro, xParolaCriptata: string): boolean;
var a1, a2: WideString;
     p1, p2: PWideChar;
begin
     //in chiaro
     a1 := xparolaChiaro;
     p1 := Addr(a1[1]);

     // gia criptata
     a2 := xParolaCriptata;
     p2 := Addr(a2[1]);

     if MD2Check(p1, p2) then
          result := true
     else
          result := false;
end;


function CriptaStringa(xparola: string): string;
var
     // xParolaDecriptata: string;
     r: PWideChar;
     myWideString: WideString;
     myWideCharPtr: PWideChar;
begin
     myWideString := xparola;
     myWideCharPtr := Addr(myWideString[1]);
     r := Cripta(myWideCharPtr);
     result := WideCharToString(r);
end;


function DecriptaStringa(xparola: string): string;
var
     // xParolaDecriptata: string;
     r: PWideChar;
     myWideString: WideString;
     myWideCharPtr: PWideChar;
begin
     myWideString := xparola;
     myWideCharPtr := Addr(myWideString[1]);
     r := Decripta(myWideCharPtr);
     result := WideCharToString(r);
end;

end.

