unit Compenso;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, FloatEdit, Buttons, Mask, DBCtrls, ExtCtrls, DtEdit97,
     ToolEdit, CurrEdit, Db, ADODB;

type
     TCompensoForm = class(TForm)
          Panel1: TPanel;
          Label1: TLabel;
          DBEdit1: TDBEdit;
          Label2: TLabel;
          DBEdit2: TDBEdit;
          Label3: TLabel;
          DBEdit3: TDBEdit;
          Label4: TLabel;
          DBEdit4: TDBEdit;
          BitBtn1: TBitBtn;
          BitBtn2: TBitBtn;
          CBTipo: TComboBox;
          Label5: TLabel;
          Label6: TLabel;
          Label7: TLabel;
          ENote: TEdit;
          DEDataFatt: TDateEdit97;
          Label8: TLabel;
          RxImporto: TRxCalcEdit;
          cbRimbSpese: TCheckBox;
    Q: TADOQuery;
    EDescrizione: TEdit;
    Label9: TLabel;
          procedure FormShow(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     CompensoForm: TCompensoForm;

implementation

uses ModuloDati2, ModuloDati;

{$R *.DFM}

procedure TCompensoForm.FormShow(Sender: TObject);
begin
     Caption := '[M/122] - ' + caption;
     if pos('BFK', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) > 0 then begin
          RxImporto.DisplayFormat := 'chf''.'' ,0.00';
     end;

     // caricamento tipi compensi
     Q.Close;
     Q.SQL.clear;
     Q.SQL.text := 'select * from EBC_OffertaDettaglioTipi';
     Q.Open;
     CBTipo.Items.Clear;
     while not Q.EOF do begin
          CBTipo.Items.Add(Q.FieldByName('Tipo').asString);
          Q.Next;
     end;
     Q.Close;

end;

end.
