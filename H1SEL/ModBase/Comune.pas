unit Comune;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, TB97, ExtCtrls;

type
     TComuneForm = class(TForm)
          EDesc: TEdit;
          EProv: TEdit;
          ECap: TEdit;
          EPrefisso: TEdit;
          Label1: TLabel;
          Label2: TLabel;
          Label3: TLabel;
          Label4: TLabel;
          Label5: TLabel;
          Label6: TLabel;
          EAreaNielsen: TEdit;
          eRegList: TComboBox;
          EDCodice: TEdit;
          Label7: TLabel;
          Panel1: TPanel;
          BOk: TToolbarButton97;
          BAnnulla: TToolbarButton97;
          procedure FormShow(Sender: TObject);
          procedure BOkClick(Sender: TObject);
          procedure BAnnullaClick(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     ComuneForm: TComuneForm;

implementation

uses Main;

{$R *.DFM}

procedure TComuneForm.FormShow(Sender: TObject);
begin
//Grafica
     ComuneForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Caption := '[S/18] - ' + Caption;
end;

procedure TComuneForm.BOkClick(Sender: TObject);
begin
     ComuneForm.ModalResult := mrOk;
end;

procedure TComuneForm.BAnnullaClick(Sender: TObject);
begin
     ComuneForm.ModalResult := mrCancel;
end;

end.

