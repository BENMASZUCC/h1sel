�
 TELENCORICCLIENTEFORM 0r  TPF0TElencoRicClienteFormElencoRicClienteFormLeft2Top� BorderStylebsDialogCaption+Elenco commesse per il cliente in questioneClientHeight�ClientWidth.Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterPixelsPerInch`
TextHeight TPanelPanel1Left Top Width.Height(AlignalTopTabOrder  TPanelPanel7LeftTopWidth Height&AlignalLeft
BevelOuterbvNoneEnabledTabOrder  TDBEditDBEdit12LeftTop	Width� HeightColorclAqua	DataFieldDescrizione
DataSourceData2.dsEBCClientiFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontReadOnly	TabOrder    TBitBtnBitBtn1Left�TopWidthRHeight TabOrderKindbkOK  TBitBtnBitBtn2Left�TopWidthNHeight CaptionAnnullaTabOrderKindbkCancel   	TdxDBGridDBGElencoCommLeft Top(Width.HeightwBands  DefaultLayout	HeaderPanelRowCountKeyFieldIDSummaryGroups SummarySeparator, AlignalClientTabOrder
DataSourceDsQRicClienteFilter.Active	Filter.Criteria
       	OptionsDBedgoCancelOnExitedgoCanDeleteedgoCanInsertedgoCanNavigationedgoConfirmDeleteedgoLoadAllRecordsedgoUseBookmarks OptionsViewedgoAutoWidthedgoBandHeaderWidthedgoIndicatoredgoUseBitmap  TdxDBGridColumnDBGElencoCommColumn1CaptionRif.DisableEditor	Width:	BandIndex RowIndex 	FieldNameProgressivo  TdxDBGridDateColumnDBGElencoCommColumn2CaptionData InizioDisableEditor	Width`	BandIndex RowIndex 	FieldName	DallaData  TdxDBGridColumnDBGElencoCommColumn3CaptionN� RicercatiDisableEditor	Widthl	BandIndex RowIndex 	FieldNameNumRicercati  TdxDBGridColumnDBGElencoCommColumn4DisableEditor	WidthQ	BandIndex RowIndex 	FieldNameRuolo  TdxDBGridColumnDBGElencoCommColumn5CaptionStato RicercaDisableEditor	WidthU	BandIndex RowIndex 	FieldNameStatoRic  TdxDBGridColumnDBGElencoCommColumn6DisableEditor	Width[	BandIndex RowIndex 	FieldNameUtente   TDataSourceDsQRicClienteDataSetQRicClienteLefthTop�   	TADOQueryQRicCliente
ConnectionData.DB
CursorTypectStatic
ParametersName
xIDCliente
AttributespaSigned
paNullable DataType	ftInteger	Precision
SizeValue�
  SQL.Strings)select EBC_Ricerche.ID,EBC_Ricerche.Tipo,>       Progressivo,DataInizio,DataFine,NumRicercati,DallaData,;       Mansioni.Descrizione Ruolo, Users.Nominativo Utente,X       EBC_StatiRic.StatoRic,EBC_Ricerche.StipendioLordo,ebc_clienti.descrizione Cliente9from EBC_Ricerche,Mansioni,EBC_StatiRic,Users,EBC_clienti)where EBC_Ricerche.IDMansione=Mansioni.ID-  and EBC_Ricerche.IDStatoRic=EBC_StatiRic.ID$  and EBC_Ricerche.IDUtente=Users.ID)and ebc_ricerche.idcliente=ebc_clienti.id&and ebc_ricerche.idcliente=:xIDCliente LefthTop�    