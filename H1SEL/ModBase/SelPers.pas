unit SelPers;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Mask, DBCtrls, Grids, DBGrids, Db, DBTables, Buttons, TB97,
     ExtCtrls, ComCtrls, DtEdit97, DtEdDB97, ComObj, RXDBCtrl, CliCandidati,
     RXSpin, dxCntner, dxEditor, dxExEdtr, dxEdLib, dxDBELib, dxDBTLCl,
     dxGrClms, dxDBGrid, dxTL, dxDBCtrl, dxGridMenus, dxPSdxDBGrLnk, dxPSCore,
     dxPSdxTLLnk, Menus, ImgList, FrameDBRichEdit2, dxTLClms, dxGrClEx,
     dxLayout, ShellApi, dxPSdxDBCtrlLnk, ADODB, U_ADOLinkCl, FrmCampiPers, legginote,
     OleCtrls, SHDocVw, uSendMailMAPI, uFunzioniEsterne,u_ElencoWorkFlow;

type
     TSelPersForm = class(TForm)
          //testmb
          Panel2: TPanel;
          GroupBox7: TGroupBox;
          DBEdit1: TDBEdit;
          GroupBox5: TGroupBox;
          DbDateEdit971: TDbDateEdit97;
          GroupBox3: TGroupBox;
          DBEdit3: TDBEdit;
          DBEdit4: TDBEdit;
          GBCliente: TGroupBox;
          DBEdit5: TDBEdit;
          GroupBox4: TGroupBox;
          Ora: TTimer;
          TimerInattivo: TTimer;
          GroupBox1: TGroupBox;
          DBEdit6: TDBEdit;
          DBEdit7: TDBEdit;
          Label4: TLabel;
          Panel4: TPanel;
          ToolbarButton975: TToolbarButton97;
          DBEdit8: TDBEdit;
          GroupBox6: TGroupBox;
          DBEdit9: TDBEdit;
          PCSelPers: TPageControl;
          TSCandidati: TTabSheet;
          TSCandAnnunci: TTabSheet;
          Panel1: TPanel;
          Panel5: TPanel;
          ToolbarButton9712: TToolbarButton97;
          ToolbarButton9713: TToolbarButton97;
          BCurriculum: TToolbarButton97;
          BElimina: TToolbarButton97;
          ToolbarButton974: TToolbarButton97;
          ToolbarButton978: TToolbarButton97;
          TbBAltroNome: TToolbarButton97;
          BInfoCand: TToolbarButton97;
          ToolbarButton972: TToolbarButton97;
          ToolbarButton973: TToolbarButton97;
          Bevel1: TBevel;
          Bevel2: TBevel;
          Bevel3: TBevel;
          Bevel4: TBevel;
          BRecupera: TToolbarButton97;
          BStampaElenco: TToolbarButton97;
          Bevel5: TBevel;
          Panel7: TPanel;
          Panel8: TPanel;
          LTotCand: TLabel;
          CBOpzioneCand: TCheckBox;
          Panel3: TPanel;
          Panel6: TPanel;
          Panel9: TPanel;
          Panel10: TPanel;
          LTotCandAnn: TLabel;
          BCurriculum1: TToolbarButton97;
          BInfoCand2: TToolbarButton97;
          ToolbarButton977: TToolbarButton97;
          ToolbarButton979: TToolbarButton97;
          Bevel6: TBevel;
          Bevel7: TBevel;
          ToolbarButton971: TToolbarButton97;
          Bevel8: TBevel;
          Panel11: TPanel;
          Label5: TLabel;
          Shape1: TShape;
          Label7: TLabel;
          ToolbarButton976: TToolbarButton97;
          TSAltriDati: TTabSheet;
          Panel12: TPanel;
          TSCliCand: TTabSheet;
          CliCandidatiFrame1: TCliCandidatiFrame;
          PanTiming: TPanel;
          Label1: TLabel;
          Label2: TLabel;
          Label3: TLabel;
          LTiming: TLabel;
          SecondiAttivita: TEdit;
          SecondiTotali: TEdit;
          TSRicParams: TTabSheet;
          Panel16: TPanel;
          Panel17: TPanel;
          Label13: TLabel;
          Label14: TLabel;
          Label15: TLabel;
          Panel18: TPanel;
          Label16: TLabel;
          Label17: TLabel;
          Label9: TLabel;
          Panel19: TPanel;
          Label19: TLabel;
          Label20: TLabel;
          Label10: TLabel;
          Panel20: TPanel;
          Label11: TLabel;
          dxDBSpinEdit1: TdxDBSpinEdit;
          dxDBSpinEdit2: TdxDBSpinEdit;
          dxDBSpinEdit3: TdxDBSpinEdit;
          Panel21: TPanel;
          Label12: TLabel;
          DBGrid1: TDBGrid;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1ID: TdxDBGridMaskColumn;
          dxDBGrid1IDRicerca: TdxDBGridMaskColumn;
          dxDBGrid1IDAnagrafica: TdxDBGridMaskColumn;
          dxDBGrid1Escluso: TdxDBGridCheckColumn;
          dxDBGrid1DataImpegno: TdxDBGridDateColumn;
          dxDBGrid1Codstato: TdxDBGridMaskColumn;
          dxDBGrid1Stato: TdxDBGridMaskColumn;
          dxDBGrid1Nome: TdxDBGridMaskColumn;
          dxDBGrid1CVNumero: TdxDBGridMaskColumn;
          dxDBGrid1TipoStato: TdxDBGridMaskColumn;
          dxDBGrid1DataIns: TdxDBGridDateColumn;
          dxDBGrid1MiniVal: TdxDBGridMaskColumn;
          dxDBGrid1IDStato: TdxDBGridMaskColumn;
          dxDBGrid1DataUltimoContatto: TdxDBGridDateColumn;
          dxDBGrid1TelUffCell: TdxDBGridColumn;
          dxDBGrid1Cellulare: TdxDBGridMaskColumn;
          dxDBGrid1telUfficio: TdxDBGridMaskColumn;
          dxDBGrid1DataNascita: TdxDBGridDateColumn;
          dxDBGrid1Eta: TdxDBGridColumn;
          dxDBGrid1Azienda: TdxDBGridLookupColumn;
          PMStampa: TPopupMenu;
          Stampaelenco1: TMenuItem;
          EsportainExcel1: TMenuItem;
          esportainHTML1: TMenuItem;
          dxPrinter1: TdxComponentPrinter;
          //dxPrinter1Link1: TdxDBGridReportLink;
          ImageList1: TImageList;
          TSGestQ: TTabSheet;
          TSTimeSheet: TTabSheet;
          PanTimeSheet: TPanel;
          TSTargetList: TTabSheet;
          Panel26: TPanel;
          Panel27: TPanel;
          BRicClienti: TToolbarButton97;
          BTargetListStampa: TToolbarButton97;
          PMStampaTL: TPopupMenu;
          MenuItem1: TMenuItem;
          MenuItem2: TMenuItem;
          MenuItem3: TMenuItem;
          //dxPrinter1Link2: TdxDBGridReportLink;
          BTLOrg: TToolbarButton97;
          Bevel16: TBevel;
          Bevel17: TBevel;
          BTLDelete: TToolbarButton97;
          Bevel18: TBevel;
          Panel28: TPanel;
          dxDbGrid3dxDBGridMaskColumn1: TdxDBGridMaskColumn;
          dxDbGrid3dxDBGridMaskColumn2: TdxDBGridMaskColumn;
          dxDbGrid3dxDBGrid1Attivita: TdxDBGridMaskColumn;
          dxDbGrid3dxDBGrid1telefono: TdxDBGridMaskColumn;
          dxDbGrid3dxDBGrid1Comune: TdxDBGridMaskColumn;
          dxDbGrid3dxDBGrid1DataUltimaEsploraz: TdxDBGridDateColumn;
          dxDbGrid3Column7: TdxDBGridBlobColumn;
          Splitter1: TSplitter;
          Panel29: TPanel;
          dxDBGrid4: TdxDBGrid;
          BTLCandVai: TToolbarButton97;
          Bevel20: TBevel;
          dxDBGrid4ID: TdxDBGridMaskColumn;
          dxDBGrid4IDAnagrafica: TdxDBGridMaskColumn;
          dxDBGrid4CVNumero: TdxDBGridMaskColumn;
          dxDBGrid4Cognome: TdxDBGridMaskColumn;
          dxDBGrid4Nome: TdxDBGridMaskColumn;
          dxDBGrid4IDStato: TdxDBGridMaskColumn;
          dxDBGrid4Cellulare: TdxDBGridMaskColumn;
          dxDBGrid4TelUfficio: TdxDBGridMaskColumn;
          dxDBGrid4DataNascita: TdxDBGridDateColumn;
          dxDBGrid4Eta: TdxDBGridColumn;
          BTLElencoCli: TToolbarButton97;
          BTLCandNew: TToolbarButton97;
          BTLCandInsNuovo: TToolbarButton97;
          ToolbarButton9715: TToolbarButton97;
          PMStampaTLCand: TPopupMenu;
          MenuItem4: TMenuItem;
          MenuItem5: TMenuItem;
          MenuItem6: TMenuItem;
          //dxPrinter1Link3: TdxDBGridReportLink;
          Panel24: TPanel;
          BTimeSheetNew: TToolbarButton97;
          BTimeSheetDel: TToolbarButton97;
          BTimeSheetCan: TToolbarButton97;
          BTimeSheetOK: TToolbarButton97;
          dxDBGrid5: TdxDBGrid;
          dxDBGridLayoutList1: TdxDBGridLayoutList;
          dxDBGridLayoutList1Item1: TdxDBGridLayout;
          dxDBGrid5Data: TdxDBGridDateColumn;
          dxDBGrid5OreConsuntivo: TdxDBGridMaskColumn;
          dxDBGrid5Utente: TdxDBGridExtLookupColumn;
          dxDBGrid5Note: TdxDBGridBlobColumn;
          dxDBGridLayoutList1Item2: TdxDBGridLayout;
          dxDBGrid5Column5: TdxDBGridExtLookupColumn;
          PMTargetList: TPopupMenu;
          modificanumeroditelefono1: TMenuItem;
          BTLNoteCliente: TToolbarButton97;
          Splitter2: TSplitter;
          Panel25: TPanel;
          Panel30: TPanel;
          Panel31: TPanel;
          BTRepDettNew: TToolbarButton97;
          BTRepDettDel: TToolbarButton97;
          BTRepDettCan: TToolbarButton97;
          BTRepDettOK: TToolbarButton97;
          dxDBGrid6: TdxDBGrid;
          dxDBGrid6Ore: TdxDBGridMaskColumn;
          dxDBGrid6Causale: TdxDBGridLookupColumn;
          BAggTotTRep: TToolbarButton97;
          dxDBGrid6Note: TdxDBGridBlobColumn;
          ToolbarButton9711: TToolbarButton97;
          dxDBGrid1TelAzienda: TdxDBGridColumn;
          PMCand: TPopupMenu;
          modificaaziendaattualecandidato1: TMenuItem;
          modificanumeroditelefonodufficio1: TMenuItem;
          modificanumeroditelefonocellulare1: TMenuItem;
          TSFile: TTabSheet;
          Panel123: TPanel;
          BAnagFileNew: TToolbarButton97;
          BAnagFileMod: TToolbarButton97;
          BAnagFileDel: TToolbarButton97;
          BFileCandOpen: TToolbarButton97;
          DsQRicFile: TDataSource;
          dxDBGrid7: TdxDBGrid;
          dxDBGrid7Descrizione: TdxDBGridMaskColumn;
          dxDBGrid7NomeFile: TdxDBGridMaskColumn;
          dxDBGrid7DataCreazione: TdxDBGridDateColumn;
          dxDBGrid1Column24: TdxDBGridColumn;
          CBOpzioneCand2: TCheckBox;
          DBGAnnunciCand: TdxDBGrid;
          DBGAnnunciCandRif: TdxDBGridMaskColumn;
          DBGAnnunciCandTestata: TdxDBGridMaskColumn;
          DBGAnnunciCandNomeEdizione: TdxDBGridMaskColumn;
          DBGAnnunciCandData: TdxDBGridDateColumn;
          DBGAnnunciCandCVNumero: TdxDBGridMaskColumn;
          DBGAnnunciCandCognome: TdxDBGridMaskColumn;
          DBGAnnunciCandNome: TdxDBGridMaskColumn;
          DBGAnnunciCandCVInseritoIndata: TdxDBGridDateColumn;
          dxDBGrid4Ruolo: TdxDBGridColumn;
          BTLDelSogg: TToolbarButton97;
          CBColoriCandAnnunci: TCheckBox;
          dxDBGrid1Column25: TdxDBGridColumn;
          Q: TADOLinkedQuery;
          QRicFile: TADOLinkedQuery;
          QRicFileID: TAutoIncField;
          QRicFileIDRicerca: TIntegerField;
          QRicFileIDTipo: TIntegerField;
          QRicFileDescrizione: TStringField;
          QRicFileNomeFile: TStringField;
          QRicFileDataCreazione: TDateTimeField;
          dxDBGrid4Column12: TdxDBGridColumn;
          dxDBGrid4Column13: TdxDBGridColumn;
          PMTLCand: TPopupMenu;
          modificatelefonocasa1: TMenuItem;
          modificanote1: TMenuItem;
          dxDBGrid1Note: TdxDBGridBlobColumn;
          modificadescrizposizione1: TMenuItem;
          dxDBGrid4Column14: TdxDBGridMemoColumn;
          PanCheckListCommessa: TPanel;
          Panel22: TPanel;
          Panel23: TPanel;
          dxDBGrid2: TdxDBGrid;
          dxDBGrid2Voce: TdxDBGridMaskColumn;
          BCLImportStd: TToolbarButton97;
          BCLVociStandard: TToolbarButton97;
          BCLNew: TToolbarButton97;
          BCLDel: TToolbarButton97;
          BCLCan: TToolbarButton97;
          BCLOK: TToolbarButton97;
          dxDBGrid2IDVoceStandard: TdxDBGridColumn;
          Splitter3: TSplitter;
          Panel32: TPanel;
          dxDBGrid8: TdxDBGrid;
          Panel33: TPanel;
          dxDBGrid8Candidato: TdxDBGridMaskColumn;
          dxDBGrid8Voce: TdxDBGridMaskColumn;
          dxDBGrid8Flag: TdxDBGridCheckColumn;
          dxDBGrid8Data: TdxDBGridColumn;
          dxDBGrid8Risposta: TdxDBGridBlobColumn;
          dxDBGrid1FlagAccPres: TdxDBGridCheckColumn;
          dxPrinter1Link1: TdxDBGridReportLink;
          dxPrinter1Link2: TdxDBGridReportLink;
          dxPrinter1Link3: TdxDBGridReportLink;
          BSaveToExcelTR: TToolbarButton97;
          DBGAnnunciCandVisionato: TdxDBGridCheckColumn;
          dfd1: TMenuItem;
          dxDBGrid4Column15: TdxDBGridBlobColumn;
          BTLCandInsRic: TToolbarButton97;
          DBGAnnunciCandDataPervenuto: TdxDBGridDateColumn;
          dxDBGrid1VisibleCliente: TdxDBGridCheckColumn;
          inverticampoVisibilealcliente1: TMenuItem;
          Gestionetabstatiperclienti1: TMenuItem;
          modificastatoperilcliente1: TMenuItem;
          dxDBGrid1StatoCliente: TdxDBGridExtLookupColumn;
          Impostazionecolorigriglia1: TMenuItem;
          dxDBGrid1IDEvento: TdxDBGridColumn;
          Colorariga1: TMenuItem;
          ColorDialog1: TColorDialog;
          dxDBGrid1Colore: TdxDBGridColumn;
          DBEdit13: TDBEdit;
          dxDBGrid1Tipo: TdxDBGridPickColumn;
          dxDBGrid1Inquadramento: TdxDBGridMaskColumn;
          dxDBGrid1RuoloAttuale: TdxDBGridMaskColumn;
          dxDBGrid1RetribTotale: TdxDBGridCurrencyColumn;
          dxDBGrid1Retribuzione: TdxDBGridCurrencyColumn;
          dxDBGrid1RetribVariabile: TdxDBGridCurrencyColumn;
          dxDBGrid1NoteCliente: TdxDBGridBlobColumn;
          dxDBGrid1JobTitle: TdxDBGridColumn;
          expModelloExcel: TMenuItem;
          TSComCampPers: TTabSheet;
          CampiPersFrame1: TCampiPersFrame;
          dxDBGrid1SettoreAz: TdxDBGridColumn;
          PMStorico: TPopupMenu;
          Storiacontatti1: TMenuItem;
          StoricoGeneraleCommesse1: TMenuItem;
          DBCheckBox1: TDBCheckBox;
          Label21: TLabel;
          Label6: TLabel;
          DBEdit2: TDBEdit;
          Label22: TLabel;
          DBEdit12: TDBEdit;
          Label23: TLabel;
          dxDBBlobEdit1: TdxDBBlobEdit;
          pmAnnRicerche: TPopupMenu;
          Colorariga2: TMenuItem;
          pmCandAnnunci: TPopupMenu;
          Segnacome1: TMenuItem;
          DBGAnnunciCandColumn15: TdxDBGridColumn;
          PMApriFileOfferta: TPopupMenu;
          Key2People1: TMenuItem;
          Intermedia1: TMenuItem;
          Ita1: TMenuItem;
          Eng1: TMenuItem;
          Ita2: TMenuItem;
          Eng2: TMenuItem;
          Modificanote2: TMenuItem;
          dxDbGrid3: TdxDBGrid;
          bImpostaPesi: TToolbarButton97;
          DBGAnnunciCandPeso: TdxDBGridColumn;
          TSValutaz: TTabSheet;
          Panel34: TPanel;
          Panel35: TPanel;
          Panel36: TPanel;
          dxDBGrid9: TdxDBGrid;
          dxDBGrid9ID: TdxDBGridMaskColumn;
          dxDBGrid9Candidato: TdxDBGridMaskColumn;
          dxDBGrid9Voce: TdxDBGridMaskColumn;
          dxDBGrid9Legenda: TdxDBGridMaskColumn;
          dxDBGrid9Selezionatore: TdxDBGridMaskColumn;
          dxDBGrid9Note: TdxDBGridBlobColumn;
          BTabellaVoci: TToolbarButton97;
          BValutazExport: TToolbarButton97;
          BValutazNew: TToolbarButton97;
          BValutazDel: TToolbarButton97;
          BValutazCan: TToolbarButton97;
          BValutazOK: TToolbarButton97;
          dxDBGrid9Valore: TdxDBGridSpinColumn;
          ToolbarButton9710: TToolbarButton97;
          ToolbarButton9714: TToolbarButton97;
          WebBrowser1: TWebBrowser;
          dxDBGrid1Distanza: TdxDBGridColumn;
          dxDBGrid1Tempo: TdxDBGridColumn;
          PMGoogleMaps: TPopupMenu;
          AnagraficaResidenza1: TMenuItem;
          AnagraficaDomicilio1: TMenuItem;
          BCalcolaDistanza: TToolbarButton97;
          bAnnEvento: TToolbarButton97;
          DBGAnnunciCandStato: TdxDBGridColumn;
          DBGAnnunciCandTotPunteggio: TdxDBGridColumn;
          BQuestRisposteDate: TToolbarButton97;
          TSQuestionari: TTabSheet;
          Panel38: TPanel;
          Splitter4: TSplitter;
          PCQuest: TPageControl;
          TSQuest_dett: TTabSheet;
          Panel39: TPanel;
          dxDBGrid10: TdxDBGrid;
          dxDBGrid10ID: TdxDBGridMaskColumn;
          dxDBGrid10IDRicerca: TdxDBGridMaskColumn;
          dxDBGrid10IDQuest: TdxDBGridMaskColumn;
          dxDBGrid10Questionario: TdxDBGridLookupColumn;
          dxDBGrid10Tipologia: TdxDBGridMaskColumn;
          dxDBGrid10GruppoQuestionario: TdxDBGridMaskColumn;
          Panel37: TPanel;
          DBGAnnunciCandProvinciaresidenza: TdxDBGridColumn;
          DBGAnnunciCandProvDomicilio: TdxDBGridColumn;
          DBGAnnunciCandNomeazienda: TdxDBGridColumn;
          DBGAnnunciCandretribuzioni: TdxDBGridColumn;
          DBGAnnunciCandIDCandRic: TdxDBGridColumn;
          DBGAnnunciCandJobTitle: TdxDBGridColumn;
          dxDBGrid1Cognome: TdxDBGridColumn;
          Panel40: TPanel;
          BRicQuestNew: TToolbarButton97;
          BRicQuestDel: TToolbarButton97;
          BRicQuest_H1Quest: TToolbarButton97;
          dxDBGrid11: TdxDBGrid;
          dxDBGrid11ID: TdxDBGridMaskColumn;
          dxDBGrid11Candidato: TdxDBGridMaskColumn;
          dxDBGrid11DataOraInvio: TdxDBGridDateColumn;
          dxDBGrid11IDAnagQuest: TdxDBGridMaskColumn;
          dxDBGrid11DataOraInizio: TdxDBGridDateColumn;
          dxDBGrid11DataOraFIne: TdxDBGridDateColumn;
          dxDBGrid10Codice: TdxDBGridColumn;
          dxDBGrid11Email: TdxDBGridColumn;
          BQuestInvia: TToolbarButton97;
          ToolbarButton9716: TToolbarButton97;
          BCliAssociaFile: TToolbarButton97;
          dxDBGrid11TotPunteggio: TdxDBGridColumn;
          TSQuest_risp: TTabSheet;
          dxDBGrid12: TdxDBGrid;
          dxDBGrid12ID: TdxDBGridMaskColumn;
          dxDBGrid12Candidato: TdxDBGridMaskColumn;
          dxDBGrid12Raggruppamento: TdxDBGridMaskColumn;
          dxDBGrid12TotPunti: TdxDBGridMaskColumn;
          dxDBGrid1Visionato: TdxDBGridCheckColumn;
          SpeedButton13: TSpeedButton;
          TUTTIAnagraficaResidenza1: TMenuItem;
          TUTTIAnagraficaDomicilio1: TMenuItem;
          dxDBGrid1StatoCand: TdxDBGridColumn;
          dxDBGrid1DaRichiamareEntro: TdxDBGridDateColumn;
          dxDBGrid1MotivoRichiamo: TdxDBGridColumn;
          dxDBGrid1SospesoFinoAl: TdxDBGridDateColumn;
          dxDBGrid1MotivoStatoSosp: TdxDBGridColumn;
          dxDBGrid1Cvinseritoindata: TdxDBGridDateColumn;
          dxDBGrid1ProprietaCV: TdxDBGridColumn;
          dxDBGrid1NazioneDomicilio: TdxDBGridColumn;
          dxDBGrid1DomicilioComune: TdxDBGridColumn;
          Esportadaticandidati1: TMenuItem;
          dxDbGrid3Provincia: TdxDBGridColumn;
          ToolbarButton9717: TToolbarButton97;
          ToolbarButton9718: TToolbarButton97;
          BModCodCommessa: TToolbarButton97;
          BTipo: TToolbarButton97;
          SpeedButton1: TToolbarButton97;
          BUtenteMod: TToolbarButton97;
          BStatoRic: TToolbarButton97;
          BStatoSecMod: TToolbarButton97;
          BStoricoRic: TToolbarButton97;
          Panel41: TPanel;
          BOk: TToolbarButton97;
          ShowMap: TMenuItem;
          dxDBGrid1Email: TdxDBGridColumn;
          dxDBGrid1DomicilioRegione: TdxDBGridColumn;
          dxDBGrid1Regione: TdxDBGridColumn;
          BStampaElencoAnnunci: TToolbarButton97;
          ToolbarButton9720: TToolbarButton97;
          PMStampaAnnunci: TPopupMenu;
          Gestionetabstatipercliente1: TMenuItem;
          eliminastatoperilcliente1: TMenuItem;
          DBGAnnunciCandStatoCliente: TdxDBGridLookupColumn;
          dxDBGrid1MansioneCommessa: TdxDBGridColumn;
          dxDBGrid1Classificazione: TdxDBGridColumn;
          dxDBGrid1domiciliocap: TdxDBGridColumn;
          dxDBGrid1domicilioprovincia: TdxDBGridColumn;
          dxDBGrid1Comune: TdxDBGridColumn;
          dxDBGrid1Provincia: TdxDBGridColumn;
          dxDBGrid1ResidenzaStato: TdxDBGridColumn;
          ModificaFlagAcc: TMenuItem;
          dxDBGrid1MansioneCand: TdxDBGridColumn;
          dxDBGrid1AreaCand: TdxDBGridColumn;
          expModelloExcelGenerale: TMenuItem;
          expModelloExcelIntermedia: TMenuItem;
          SpeedButton2: TToolbarButton97;
          TSAttivita: TTabSheet;
          Panel42: TPanel;
          Panel43: TPanel;
          BAttivitaNew: TToolbarButton97;
          BAttivitaDel: TToolbarButton97;
          BAttivitaCan: TToolbarButton97;
          BAttivitaOK: TToolbarButton97;
          dxDBGrid13: TdxDBGrid;
          dxDBGrid13ID: TdxDBGridMaskColumn;
          dxDBGrid13IDRicerca: TdxDBGridMaskColumn;
          dxDBGrid13Data: TdxDBGridDateColumn;
          dxDBGrid13IDEvento: TdxDBGridMaskColumn;
          dxDBGrid13IDAnagrafica: TdxDBGridMaskColumn;
          dxDBGrid13IDUtente: TdxDBGridMaskColumn;
          dxDBGrid13Evento: TdxDBGridLookupColumn;
          dxDBGrid13Candidato: TdxDBGridLookupColumn;
          dxDBGrid13Utente: TdxDBGridLookupColumn;
          dxDBGrid13Costo: TdxDBGridCurrencyColumn;
          dxDBGrid13Note: TdxDBGridBlobColumn;
          BTabEventiAtt: TToolbarButton97;
          dxDBGrid13Column13: TdxDBGridColumn;
          ToolbarButton9723: TToolbarButton97;
          dxDBGrid13Minuti: TdxDBGridSpinColumn;
          MiniProfilo1: TMenuItem;
          MiniProfiloanonimo1: TMenuItem;
          PanStatCommessa: TPanel;
          Label162: TLabel;
          DBText9: TDBText;
          Label163: TLabel;
          DBText10: TDBText;
          Label164: TLabel;
          DBText11: TDBText;
          Label165: TLabel;
          DBText12: TDBText;
          Bevel10: TBevel;
          Bevel11: TBevel;
          Bevel12: TBevel;
          Bevel13: TBevel;
          ToolbarButton9724: TToolbarButton97;
          Label149: TLabel;
          DBText8: TDBText;
          Bevel9: TBevel;
          Label167: TLabel;
          DBText13: TDBText;
          Bevel14: TBevel;
          Label168: TLabel;
          DBText14: TDBText;
          Bevel15: TBevel;
          dxDBGrid1AReaSEttore: TdxDBGridColumn;
          Panel14: TPanel;
          PAlto: TPanel;
          Panel15: TPanel;
          GroupBox8: TGroupBox;
          SpeedButton4: TToolbarButton97;
          ToolbarButton9719: TToolbarButton97;
          DBEdit10: TDBEdit;
          GroupBox9: TGroupBox;
          SpeedButton5: TToolbarButton97;
          ToolbarButton9721: TToolbarButton97;
          DBEdit11: TDBEdit;
          GroupBox10: TGroupBox;
          BUtente2: TToolbarButton97;
          SpeedButton6: TToolbarButton97;
          SpeedButton7: TToolbarButton97;
          SpeedButton8: TToolbarButton97;
          DBGrid2: TDBGrid;
          GroupBox11: TGroupBox;
          BPassword: TToolbarButton97;
          DBDataPrev: TDbDateEdit97;
          GroupBox12: TGroupBox;
          Label18: TLabel;
          bApriFileOfferta: TToolbarButton97;
          bSelOfferta: TToolbarButton97;
          ERifOfferta: TEdit;
          PanCausali: TPanel;
          Label8: TLabel;
          BTabCausali: TSpeedButton;
          DBLookupComboBox1: TDBLookupComboBox;
          GroupBox13: TGroupBox;
          DbDateEdit972: TDbDateEdit97;
          GroupBox2: TGroupBox;
          SpeedButton12: TToolbarButton97;
          ToolbarButton9722: TToolbarButton97;
          DBEdit16: TDBEdit;
          GroupBox14: TGroupBox;
          DBEdit14: TDBEdit;
          DBEdit15: TDBEdit;
          SpeedButton10: TToolbarButton97;
          SpeedButton11: TToolbarButton97;
          Splitter5: TSplitter;
          PBasso: TPanel;
          Panel13: TPanel;
          FrameDBRich1: TFrameDBRich;
          Committenti: TGroupBox;
          ToolbarButton9725: TToolbarButton97;
          ToolbarButton9726: TToolbarButton97;
          dxDBGrid14: TdxDBGrid;
          dxDBGrid14ID: TdxDBGridMaskColumn;
          dxDBGrid14IDRicerca: TdxDBGridMaskColumn;
          dxDBGrid14IDContattoCli: TdxDBGridMaskColumn;
          dxDBGrid14IDAnagrafica: TdxDBGridMaskColumn;
          dxDBGrid14Nominativo: TdxDBGridMaskColumn;
          WorkflowScadenze1: TMenuItem;
          Attivaworkflow1: TMenuItem;
          QTemp: TADOQuery;
    ElencoWorkFlow1: TMenuItem;
    ADOTable1: TADOTable;
    DataSource1: TDataSource;
          procedure BitBtn4Click(Sender: TObject);
          procedure BitBtn7Click(Sender: TObject);
          procedure BCurriculumClick(Sender: TObject);
          procedure ToolbarButton978Click(Sender: TObject);
          procedure BEliminaClick(Sender: TObject);
          procedure TbBAltroNomeClick(Sender: TObject);
          procedure SpeedButton1Click(Sender: TObject);
          procedure SpeedButton2Click(Sender: TObject);
          procedure ToolbarButton974Click(Sender: TObject);
          procedure ToolbarButton9712Click(Sender: TObject);
          procedure CBOpzioneCandClick(Sender: TObject);
          procedure ToolbarButton9713Click(Sender: TObject);
          procedure BInfoCandClick(Sender: TObject);
          procedure ToolbarButton972Click(Sender: TObject);
          procedure ToolbarButton973Click(Sender: TObject);
          procedure BRecuperaClick(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure OraTimer(Sender: TObject);
          procedure TimerInattivoTimer(Sender: TObject);
          procedure FormCreate(Sender: TObject);
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
          procedure BStatoRicClick(Sender: TObject);
          procedure BStoricoRicClick(Sender: TObject);
          procedure ToolbarButton975Click(Sender: TObject);
          procedure BUtenteModClick(Sender: TObject);
          procedure SpeedButton3Click(Sender: TObject);
          procedure BTipoClick(Sender: TObject);
          procedure PCSelPersChange(Sender: TObject);
          procedure BInfoCand2Click(Sender: TObject);
          procedure BCurriculum1Click(Sender: TObject);
          procedure ToolbarButton977Click(Sender: TObject);
          procedure ToolbarButton979Click(Sender: TObject);
          procedure ToolbarButton971Click(Sender: TObject);
          procedure ToolbarButton976Click(Sender: TObject);
          procedure SpeedButton4Click(Sender: TObject);
          procedure SpeedButton5Click(Sender: TObject);
          procedure BUtente2Click(Sender: TObject);
          procedure BPasswordClick(Sender: TObject);
          procedure DbDateEdit971Exit(Sender: TObject);
          procedure dxDBGrid1MouseUp(Sender: TObject; Button: TMouseButton;
               Shift: TShiftState; X, Y: Integer);
          procedure dxDBGrid1Click(Sender: TObject);
          procedure dxDBGrid1DblClick(Sender: TObject);
          procedure dxDBGrid1CustomDrawCell(Sender: TObject; ACanvas: TCanvas;
               ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
               ASelected, AFocused, ANewItemRow: Boolean; var AText: string;
               var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
               var ADone: Boolean);
          procedure Stampaelenco1Click(Sender: TObject);
          procedure EsportainExcel1Click(Sender: TObject);
          procedure esportainHTML1Click(Sender: TObject);
          procedure BModCodCommessaClick(Sender: TObject);
          procedure SpeedButton6Click(Sender: TObject);
          procedure SpeedButton7Click(Sender: TObject);
          procedure dxDBGrid3MouseUp(Sender: TObject; Button: TMouseButton;
               Shift: TShiftState; X, Y: Integer);
          procedure BRicClientiClick(Sender: TObject);
          procedure MenuItem1Click(Sender: TObject);
          procedure MenuItem2Click(Sender: TObject);
          procedure MenuItem3Click(Sender: TObject);
          procedure BTLOrgClick(Sender: TObject);
          procedure BTLDeleteClick(Sender: TObject);
          procedure BTLCandVaiClick(Sender: TObject);
          procedure BTLElencoCliClick(Sender: TObject);
          procedure BTLCandNewClick(Sender: TObject);
          procedure dxDBGrid4DblClick(Sender: TObject);
          procedure BTLCandInsNuovoClick(Sender: TObject);
          procedure dxDBGrid4CustomDrawCell(Sender: TObject; ACanvas: TCanvas;
               ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
               ASelected, AFocused, ANewItemRow: Boolean; var AText: string;
               var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
               var ADone: Boolean);
          procedure dxDBGrid4MouseUp(Sender: TObject; Button: TMouseButton;
               Shift: TShiftState; X, Y: Integer);
          procedure MenuItem4Click(Sender: TObject);
          procedure MenuItem5Click(Sender: TObject);
          procedure MenuItem6Click(Sender: TObject);
          procedure BTimeSheetNewClick(Sender: TObject);
          procedure BTimeSheetDelClick(Sender: TObject);
          procedure BTimeSheetOKClick(Sender: TObject);
          procedure BTimeSheetCanClick(Sender: TObject);
          procedure modificanumeroditelefono1Click(Sender: TObject);
          procedure BTLNoteClienteClick(Sender: TObject);
          procedure BTRepDettNewClick(Sender: TObject);
          procedure BTRepDettDelClick(Sender: TObject);
          procedure BTRepDettOKClick(Sender: TObject);
          procedure BTRepDettCanClick(Sender: TObject);
          procedure BAggTotTRepClick(Sender: TObject);
          procedure ToolbarButton9711Click(Sender: TObject);
          procedure modificaaziendaattualecandidato1Click(Sender: TObject);
          procedure modificanumeroditelefonodufficio1Click(Sender: TObject);
          procedure modificanumeroditelefonocellulare1Click(Sender: TObject);
          procedure BAnagFileNewClick(Sender: TObject);
          procedure BAnagFileModClick(Sender: TObject);
          procedure BAnagFileDelClick(Sender: TObject);
          procedure BFileCandOpenClick(Sender: TObject);
          procedure CBOpzioneCand2Click(Sender: TObject);
          procedure DBGAnnunciCandCustomDrawCell(Sender: TObject; ACanvas: TCanvas;
               ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
               ASelected, AFocused, ANewItemRow: Boolean; var AText: string;
               var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
               var ADone: Boolean);
          procedure BTLDelSoggClick(Sender: TObject);
          procedure CBColoriCandAnnunciClick(Sender: TObject);
          procedure dxDBGrid3ChangeColumn(Sender: TObject; Node: TdxTreeListNode;
               Column: Integer);
          procedure dxDBGrid3ContextPopup(Sender: TObject; MousePos: TPoint;
               var Handled: Boolean);
          procedure BTabCausaliClick(Sender: TObject);
          procedure QRicFileBeforeOpen(DataSet: TDataSet);
          procedure dxDBGrid4ContextPopup(Sender: TObject; MousePos: TPoint;
               var Handled: Boolean);
          procedure fa(Sender: TObject; Node: TdxTreeListNode;
               Column: Integer);
          procedure modificatelefonocasa1Click(Sender: TObject);
          procedure modificanote1Click(Sender: TObject);
          procedure modificadescrizposizione1Click(Sender: TObject);
          procedure BCLImportStdClick(Sender: TObject);
          procedure BCLNewClick(Sender: TObject);
          procedure BCLDelClick(Sender: TObject);
          procedure BCLOKClick(Sender: TObject);
          procedure BCLCanClick(Sender: TObject);
          procedure dxDBGrid2Editing(Sender: TObject; Node: TdxTreeListNode;
               var Allow: Boolean);
          procedure BCLVociStandardClick(Sender: TObject);
          procedure dxDBGrid2CustomDrawCell(Sender: TObject; ACanvas: TCanvas;
               ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
               ASelected, AFocused, ANewItemRow: Boolean; var AText: string;
               var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
               var ADone: Boolean);
          procedure BSaveToExcelTRClick(Sender: TObject);
          procedure dfd1Click(Sender: TObject);
          procedure dxDBGrid4Column15Validate(Sender: TObject;
               var ErrorText: string; var Accept: Boolean);
          procedure BTLCandInsRicClick(Sender: TObject);
          procedure dxDBGrid4Edited(Sender: TObject; Node: TdxTreeListNode);
          procedure dxDBGrid3Edited(Sender: TObject; Node: TdxTreeListNode);
          procedure inverticampoVisibilealcliente1Click(Sender: TObject);
          procedure Gestionetabstatiperclienti1Click(Sender: TObject);
          procedure modificastatoperilcliente1Click(Sender: TObject);
          procedure dxDBGrid1StatoClienteChange(Sender: TObject);
          procedure dxDBGrid1NoteEditButtonClick(Sender: TObject);
          procedure Impostazionecolorigriglia1Click(Sender: TObject);
          procedure Colorariga1Click(Sender: TObject);
          procedure SpeedButton8Click(Sender: TObject);
          procedure Panel7Exit(Sender: TObject);
          procedure expModelloExcelClick(Sender: TObject);
          procedure StoricoGeneraleCommesse1Click(Sender: TObject);
          procedure BStatoSecModClick(Sender: TObject);
          procedure SpeedButton9Click(Sender: TObject);
          procedure Colorariga2Click(Sender: TObject);
          procedure SpeedButton10Click(Sender: TObject);
          procedure SpeedButton11Click(Sender: TObject);
          procedure DBGAnnunciCandContextPopup(Sender: TObject; MousePos: TPoint;
               var Handled: Boolean);
          procedure Segnacome1Click(Sender: TObject);
          procedure bSelOffertaClick(Sender: TObject);
          procedure bApriFileOffertaClick(Sender: TObject);
          procedure Ita1Click(Sender: TObject);
          procedure Eng1Click(Sender: TObject);
          procedure Ita2Click(Sender: TObject);
          procedure Eng2Click(Sender: TObject);
          procedure Modificanote2Click(Sender: TObject);
          procedure bImpostaPesiClick(Sender: TObject);
          procedure BValutazExportClick(Sender: TObject);
          procedure BTabellaVociClick(Sender: TObject);
          procedure BValutazOKClick(Sender: TObject);
          procedure BValutazCanClick(Sender: TObject);
          procedure BValutazNewClick(Sender: TObject);
          procedure BValutazDelClick(Sender: TObject);
          procedure Button1Click(Sender: TObject);
          procedure ToolbarButton9710Click(Sender: TObject);
          procedure ToolbarButton9714Click(Sender: TObject);
          procedure WebBrowser1DocumentComplete(Sender: TObject;
               const pDisp: IDispatch; var URL: OleVariant);
          procedure BCalcolaDistanzaClick(Sender: TObject);
          procedure AnagraficaResidenza1Click(Sender: TObject);
          procedure AnagraficaDomicilio1Click(Sender: TObject);
          procedure bAnnEventoClick(Sender: TObject);
          procedure BQuestRisposteDateClick(Sender: TObject);
          procedure SpeedButton12Click(Sender: TObject);
          procedure BRicQuestNewClick(Sender: TObject);
          procedure BRicQuestDelClick(Sender: TObject);
          procedure DBGAnnunciCandMouseUp(Sender: TObject; Button: TMouseButton;
               Shift: TShiftState; X, Y: Integer);
          procedure BQuestInviaClick(Sender: TObject);
          procedure ToolbarButton9716Click(Sender: TObject);
          procedure BCliAssociaFileClick(Sender: TObject);
          procedure BRicQuest_H1QuestClick(Sender: TObject);
          procedure DbDateEdit971Enter(Sender: TObject);
          procedure SpeedButton13Click(Sender: TObject);
          procedure TUTTIAnagraficaResidenza1Click(Sender: TObject);
          procedure TUTTIAnagraficaDomicilio1Click(Sender: TObject);
          procedure Esportadaticandidati1Click(Sender: TObject);
          procedure ToolbarButton9717Click(Sender: TObject);
          procedure ToolbarButton9718Click(Sender: TObject);
          procedure BOkClick(Sender: TObject);
          procedure dxDbGrid3Click(Sender: TObject);
          procedure dxDbGrid3KeyUp(Sender: TObject; var Key: Word;
               Shift: TShiftState);
          procedure ShowMapClick(Sender: TObject);
          procedure CliCandidatiFrame1Infocandidato1Click(Sender: TObject);
          procedure CliCandidatiFrame1curriculum1Click(Sender: TObject);
          procedure CliCandidatiFrame1CancSituazCandClick(Sender: TObject);
          procedure Gestionetabstatipercliente1Click(Sender: TObject);
          procedure eliminastatoperilcliente1Click(Sender: TObject);
          procedure DBGAnnunciCandStatoClienteChange(Sender: TObject);
          procedure ToolbarButton9720Click(Sender: TObject);
          procedure ToolbarButton9719Click(Sender: TObject);
          procedure ToolbarButton9721Click(Sender: TObject);
          procedure ToolbarButton9722Click(Sender: TObject);
          procedure ModificaFlagAccClick(Sender: TObject);
          procedure expModelloExcelIntermediaClick(Sender: TObject);
          procedure BAttivitaNewClick(Sender: TObject);
          procedure BAttivitaDelClick(Sender: TObject);
          procedure BAttivitaOKClick(Sender: TObject);
          procedure BAttivitaCanClick(Sender: TObject);
          procedure BTabEventiAttClick(Sender: TObject);
          procedure ToolbarButton9723Click(Sender: TObject);
          procedure MiniProfilo1Click(Sender: TObject);
          procedure MiniProfiloanonimo1Click(Sender: TObject);
          procedure EsportamodelloExcel1Click(Sender: TObject);
          procedure ToolbarButton9724Click(Sender: TObject);
          procedure ToolbarButton9725Click(Sender: TObject);
          procedure ToolbarButton9726Click(Sender: TObject);
          procedure Attivaworkflow1Click(Sender: TObject);
    procedure ElencoWorkFlow1Click(Sender: TObject);
     private
          xStringa: string;
          xAttivo, xCerca: boolean;
          xMinStart: integer;
          xColumnAttuale, xColumnAttualeAnn: TColumn;
          xggDiffDataIns, xggDiffDataUltimoContatto: Integer;
          xVecchiadataInizio: TDateTime;
          xDisabilitaTargetList: Boolean;
          xColonnaSel, xColonnaSel2: Integer;
          function LogEsportazioniCand(xDaDove: integer): boolean;
          procedure NascondiFunzioni;
     public
          xIDOrg, xIDRicercaInt: integer;
          xFromOrgTL, xCheckRicCandCV, xCheckRicCandTel: boolean;
          xModificabile: boolean;
          xComeAdmin: boolean; //significa che sono entrato come administrator e posso modificare la commessa. tasto destro sul pulsante riprendi commessa
          xidoffertaTemp: string;
          IndirizzoCliente, IndirizzoAna: string;
          procedure EseguiQueryCandidati;
          procedure RiprendiAttivita;
          function CheckStoricoTL(xIDAzienda: integer): boolean;
          procedure ImpostaSoloLettura;
          procedure ExpMappaturainModello(xNomeModello: string; var xFileName: string);
          procedure ExpMappaturainModelloIntermedia(xIDRicerca: integer; xNomeModello: string; var xFileName, output: string;
               flagsave: boolean = false);
          //procedure WMHack(var message: TMessage); //message WM_USER + 1024;
     end;

var
     SelPersForm: TSelPersForm;

implementation

uses ModuloDati, MDRicerche, Curriculum, ContattoCand,
     StoriaContatti, ValutazDip, EsitoColloquio, ElencoDip, Main, SelData,
     InsRuolo, ModuloDati2, SelCliente, View, FaxPresentaz, CambiaStato2,
     uGestEventi, SelPersNew, SchedaCand, uUtilsVarie, RepElencoSel,
     ModDettCandRic, ModStatoRic, StoricoRic, OpzioniEliminaz, Specifiche,
     ElencoUtenti, LegendaGestRic, SchedaSintetica, SelTipoCommessa,
     DettAnnuncio, ElencoSedi, SelContatti, CheckPass, UnitPrintMemo,
     RicClienti, NuovoSoggetto, StoricoTargetList, NoteCliente,
     SceltaModelloMailCli, RicFile, SettaColoriGrigliaCand, InputPassword,
     InsNote, StoricoAnag, IndirizziCliente, RicCandAnnunci,
     TabVociValutazCand, NuovaValutazCand, TabVociValutazCandStd, googleapi,
     AnagQuestRisposteDate, DettAnagQuest, SelQuest, uTest, GoogleMap,
     ModelliWord, Splash3, u_AttivaWorkflow;

{$R *.DFM}

const
     PWDCaption = 'Forzatura blocco livello 2 ';

var TipoIndirizzoGoogleMaps: Integer; // se 1 prende anagrafica domicilio se 2 prende anagrafica residenza
     TipoIndirizzoGoogleMapsTutti: Integer;

procedure TSelPersForm.BitBtn4Click(Sender: TObject);
begin
     close;
end;

procedure TSelPersForm.BitBtn7Click(Sender: TObject);
begin
     close
end;

//[ALBERTO 20020902]

procedure TSelPersForm.BCurriculumClick(Sender: TObject);
var x: string;
     xIDAnag, i: integer;
begin
     if not Mainform.CheckProfile('051') then Exit;
     xIDAnag := DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger;
     if not DataRicerche.QCandRic.Eof then begin
          PosizionaAnag(DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger);
          CurriculumForm.ShowModal;

          MainForm.Pagecontrol5.ActivePage := MainForm.TSStatoTutti;
          RiprendiAttivita;
     end;
     DataRicerche.QCandRic.Close;
     DataRicerche.QCandRic.Open;
     if pos('EMERGENCY', UpperCase(data.Global.fieldbyname('NomeAzienda').asString)) > 0 then dxDBGrid1.FullCollapse
     else dxDBGrid1.FullExpand;
     DataRicerche.QCandRic.Locate('IDAnagrafica', xIDAnag, []);
     DataRicerche.QCandRic.FieldByName('IDAnagrafica').FocusControl;
     RegistraVisioneCand(MainForm.xIDUtenteAttuale, DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger);
end;

//[TONI200210DEBUGOK]

procedure TSelPersForm.ToolbarButton978Click(Sender: TObject);
var xIDAnag: integer;
begin
     if not MainForm.CheckProfile('216') then Exit;
     xIDAnag := DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger;
     if not DataRicerche.QCandRic.Eof then begin
          if pos('INTERMEDIA', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) > 0 then begin
               StoricoGeneraleCommesse1Click(self);
          end
          else begin
               StoriaContattiForm := TStoriaContattiForm.create(self);
               StoriaContattiForm.QStoriaCont.ReloadSQL;
               StoriaContattiForm.QStoriaCont.ParamByName['xIDRicerca'] := Dataricerche.TRicerchePend.FieldByName('ID').AsInteger;
               StoriaContattiForm.QStoriaCont.ParamByName['xIDAnagrafica'] := DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger;
               StoriaContattiForm.QStoriaCont.Open;
               StoriaContattiForm.ShowModal;
               StoriaContattiForm.Free;
               RiprendiAttivita;
          end;
          DataRicerche.QCandRic.Close;
          DataRicerche.QCandRic.Open;
          if pos('EMERGENCY', UpperCase(data.Global.fieldbyname('NomeAzienda').asString)) > 0 then dxDBGrid1.FullCollapse
          else dxDBGrid1.FullExpand;
          DataRicerche.QCandRic.Locate('IDAnagrafica', xIDAnag, []);
          DataRicerche.QCandRic.FieldByName('IDAnagrafica').FocusControl;
     end;
end;

//[TONI20021003]DEBUGOK

procedure TSelPersForm.BEliminaClick(Sender: TObject);
var xPerche: string;
begin
     if not Mainform.CheckProfile('211') then Exit;
     if DataRicerche.QCandRic.FieldByName('Escluso').Value = 1 then begin
          MessageDlg('Il candidato � gi� escluso', mtError, [mbOK], 0);
          exit;
     end;
     if not DataRicerche.QCandRic.Eof then begin
          if InputQuery('Elimina dalla ricerca', 'Motivo (max 230 caratteri):', xPerche) then begin
               Data.DB.BeginTrans;
               try
                    DataRicerche.QGen.Close;
                    DataRicerche.QGen.SQL.Clear;
                    DataRicerche.QGen.SQL.Add('update EBC_CandidatiRicerche set Escluso=1,');
                    DataRicerche.QGen.SQL.Add('Stato=''Escluso (motivo: ' + StringReplace(copy(xPerche, 1, 230), '''', '''''', [rfReplaceAll]) + ')'' where IDAnagrafica=' + DataRicerche.QCandRic.FieldByName('IDAnagrafica').asString + ' and IDRicerca=' + DataRicerche.QCandRic.FieldByName('IDRicerca').asString);
                    DataRicerche.QGen.execSQL;
                    // aggiornamento storico
                    if Q.Active then Q.Close;
                    Q.SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
                         'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xIDCliente:)';
                    Q.ParamByName['xIDAnagrafica'] := DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger;
                    Q.ParamByName['xIDEvento'] := 17;
                    Q.ParamByName['xDataEvento'] := date;
                    Q.ParamByName['xAnnotazioni'] := copy('ric.' + TrimRight(StringReplace(DataRicerche.TRicerchePend.FieldByName('Progressivo').AsString, '"', '''', [rfReplaceAll, rfIgnoreCase])) + ' per ' + TrimRight(DataRicerche.TRicerchePend.FieldByName('Cliente').asString) + ' (motivo: ' + xPerche + ')', 1, 256);
                    Q.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
                    Q.ParamByName['xIDUtente'] := Mainform.xIDUtenteAttuale; //DataRicerche.TRicerchePend.FieldByName('IDUtente').AsInteger;
                    Q.ParamByName['xIDCliente'] := DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger;
                    // try
                    Q.ExecSQL;
                    //except
                      //   on E: Exception do begin
                        // ShowMessage(E.Message);
                         //ShowMessage(q.SQL.Text);
                         //ShowMessage(q.SQL.GetText);
                         //end;
                    //end;
                    // controllo che non sia associato a ricerche diverse da questa dove non � gi� escluso
                    DataRicerche.QGen.Close;
                    DataRicerche.QGen.SQL.Clear;
                    DataRicerche.QGen.SQL.Add('select IDRicerca from EBC_CandidatiRicerche where IDAnagrafica=' + DataRicerche.QCandRic.FieldByName('IDAnagrafica').asString + ' and IDRicerca<>' + DataRicerche.QCandRic.FieldByName('IDRicerca').asString + ' and Escluso=0');
                    DataRicerche.QGen.Open;
                    if not DataRicerche.QGen.IsEmpty then begin
                         // lasciato in selezione
                         MessageDlg('Il candidato � associato ad altre ricerche dove non � gi� escluso - verr� quindi mantenuto in selezione', mtInformation, [mbOK], 0);
                    end else begin
                         // archiviazione CV
                         if Q.Active then Q.Close;
                         Q.SQL.text := 'update Anagrafica set IDStato=28,IDTipoStato=2 where ID=' + DataRicerche.QCandRic.FieldByName('IDAnagrafica').asString;
                         Q.ExecSQL;
                    end;
                    Data.DB.CommitTrans;
                    DataRicerche.QCandRic.Close;
                    DataRicerche.QCandRic.Open;
                    LTotCand.Caption := IntToStr(DataRicerche.QCandRic.RecordCount);
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('Errore sul database:  inserimento non avvenuto', mtError, [mbOK], 0);
                    raise;
               end;
          end;
          RiprendiAttivita;
          if pos('EMERGENCY', UpperCase(data.Global.fieldbyname('NomeAzienda').asString)) > 0 then dxDBGrid1.FullCollapse
          else dxDBGrid1.FullExpand;
     end;
end;

//[TONI20021002]DEBUGOK

procedure TSelPersForm.TbBAltroNomeClick(Sender: TObject);
var xVai: boolean;
     xRic, xLivProtez, xDicMess, xPassword, xUtenteResp, xClienteBlocco: string;
     xIDEvento, xIDClienteBlocco: integer;
     QLocal: TADOLinkedQuery;
begin
     if not Mainform.CheckProfile('210') then Exit;
     ElencoDipForm := TElencoDipForm.create(self);
     ElencoDipForm.ShowModal;
     if ElencoDipForm.ModalResult = mrOK then begin
          // controllo esistenza
         { if DataRicerche.QCandRic.Locate('IDAnagrafica', ElencoDipForm.TAnagDip.FieldByName('ID').value, []) then begin
               MessageDlg('Il candidato � gi� associato al progetto/commessa', mtError, [mbOK], 0);
               exit;
          end; }

          if pos('INTERMEDIA', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) > 0 then begin

               DataRicerche.QTemp.Close;
               DataRicerche.Qtemp.SQL.Text := 'select count(*) tot ' +
                    ' from esperienzelavorative el' +
                    ' where el.idanagrafica=' + ElencoDipForm.TAnagDip.FieldByName('ID').asString + ' and el.attuale=1 ' +
                    ' and el.AziendaNome is not NULL and el.AziendaNome <> '''' ' +
                    ' and el.IDMansione is not NULL and el.IDMansione <> 0 ';
               DataRicerche.Qtemp.Open;
               if DataRicerche.Qtemp.FieldByName('tot').asinteger <= 0 then begin
                    MessageDlg('Inserire l''esperienza Lavorativa Attuale del candidato per proseguire (con Azienda e Ruolo)', mtError, [mbOk], 0);
                    exit;
               end;

               DataRicerche.QCkRegoleClienti.Close;
               DataRicerche.QCkRegoleClienti.SQL.Text := 'select c.id idcliente,el.attuale,el.idanagrafica,el.aziendanome,c.descrizione,c.idregolacliente,trc.descrizione regola, trc.flag FlagRegola ' +
                    ' from esperienzelavorative el' +
                    ' join ebc_clienti c on c.id=el.idazienda join tabregolecliente trc on trc.id=c.idregolacliente' +
                    ' where el.idanagrafica=' + ElencoDipForm.TAnagDip.FieldByName('ID').asString + ' and el.attuale=1';
               DataRicerche.QCkRegoleClienti.Open;
               if DataRicerche.QCkRegoleClienti.RecordCount > 0 then
                    if DataRicerche.QCkRegoleClienti.FieldByName('FlagRegola').asboolean then
                         if MessageDlg('Limite per questo soggetto: ' + DataRicerche.QCkRegoleClienti.fieldByName('Regola').asstring + #10#13 + 'Vuoi Proseguire ?', mtWarning, [mbYes, mbNo], 0) = mrNO then
                              exit;
          end;

          Data.QTemp.close;
          Data.QTemp.sql.text := 'select * from ebc_candidatiricerche where idricerca = ' + DataRicerche.TRicerchePend.FieldByName('ID').AsString +
               ' and idanagrafica = ' + ElencoDipForm.TAnagDip.FieldByName('ID').AsString;
          Data.QTemp.Open;
          if Data.QTemp.RecordCount > 0 then begin
               MessageDlg('Il candidato � gi� associato al progetto/commessa', mtError, [mbOK], 0);
               exit;
          end;

          // controllo limite soggetti
         // if not OkIDAnag(ElencoDipForm.TAnagDip.FieldByName('ID').value) then exit; viene gi� fatta sull'ok del pulsante  di ElencoDipForm

          // controllo propriet� CV
          if (not ((ElencoDipForm.TAnagDip.FieldByName('IDProprietaCV').asString = '') or (ElencoDipForm.TAnagDip.FieldByName('IDProprietaCV').AsInteger = 0))) and
               (ElencoDipForm.TAnagDip.FieldByName('IDProprietaCV').AsInteger <> DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger) then
               if MessageDlg('ATTENZIONE: il CV risulta di propriet� di un altro cliente .' + chr(13) +
                    'Vuoi proseguire lo stesso ?', mtWarning, [mbYes, mbNo], 0) = mrNo then begin
                    ElencoDipForm.Free;
                    RiprendiAttivita;
                    exit;
               end;

          DataRicerche.TRicAnagIDX.Open;
          if not DataRicerche.TRicAnagIDX.FindKeyADO(VarArrayOf([DataRicerche.TRicerchePend.FieldByName('ID').AsInteger, ElencoDipForm.TAnagDip.FieldByName('ID').AsInteger])) then begin
               xVai := True;
               // controllo se � in selezione
               if ElencoDipForm.TAnagDip.FieldByName('IDTipoStato').AsInteger = 1 then begin
                    if MessageDlg('Il soggetto risulta IN SELEZIONE: ' + chr(13) + xRic + chr(13) + 'PROCEDERE COMUNQUE ?', mtWarning, [mbYes, mbNo], 0) = mrNo then xVai := False;
               end;
               // controllo se � gi� associato ad un'altra ricerca per la STESSA azienda
               Q.Close;
               Q.SQL.clear;
               Q.SQl.Add('select EBC_Ricerche.Progressivo,EBC_StatiRic.StatoRic from EBC_CandidatiRicerche,EBC_Ricerche,EBC_StatiRic ');
               Q.SQl.Add('where EBC_CandidatiRicerche.IDRicerca=EBC_Ricerche.ID and EBC_Ricerche.IDStatoRic=EBC_StatiRic.ID');
               Q.SQl.Add('and EBC_CandidatiRicerche.IDAnagrafica=:xIDAnag: and EBC_Ricerche.IDCliente=:xIDCliente:');
               Q.ParamByName['xIDAnag'] := ElencoDipForm.TAnagDip.FieldByName('ID').AsInteger;
               Q.ParamByName['xIDCliente'] := DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger;
               Q.Open;
               if not Q.IsEmpty then begin
                    xRic := '';
                    while not Q.EOF do begin xRic := xRic + 'N�' + Q.FieldByName('Progressivo').asString + ' (' + Q.FieldByName('StatoRic').asString + ') '; Q.Next; end;
                    if MessageDlg('Il soggetto � associato gi� alle seguenti ricerche per lo stesso cliente: ' + chr(13) +
                         xRic + chr(13) + 'PROCEDERE COMUNQUE ?', mtWarning, [mbYes, mbNo], 0) = mrNo then xVai := False;
               end;
               // controllo incompatibilit�
               if IncompAnagCli(ElencoDipForm.TAnagDip.FieldByName('ID').AsInteger, DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger) then
                    if MessageDlg('Il soggetto risulta INCOMPATIBILE con questo cliente o con un cliente associato' + chr(13) +
                         'PROCEDERE COMUNQUE ?', mtWarning, [mbYes, mbNo], 0) = mrNo then xVai := False;


               // controllo blocco livello 1 o 2
               xIDClienteBlocco := CheckAnagInseritoBlocco1(ElencoDipForm.TAnagDip.FieldByName('ID').AsInteger);
               if xIDClienteBlocco > 0 then begin
                    xLivProtez := copy(IntToStr(xIDClienteBlocco), 1, 1);
                    if xLivProtez = '1' then begin
                         xIDClienteBlocco := xIDClienteBlocco - 10000;
                         xClienteBlocco := GetDescCliente(xIDClienteBlocco);
                         xDicMess := 'Stai inserendo in commessa/progetto un candidato appartenente ad una azienda (' + xClienteBlocco + ') con blocco di livello 1, ';
                    end else begin
                         xIDClienteBlocco := xIDClienteBlocco - 20000;
                         xClienteBlocco := GetDescCliente(xIDClienteBlocco);
                         xDicMess := 'Stai inserendo in commessa/progetto un candidato appartenente ad una azienda ATTIVA (' + xClienteBlocco + '), ';
                    end;
                    if MessageDlg('ATTENZIONE!! PROTEZIONE CLIENTI LIVELLO ' + xLivProtez + chr(13) + xDicMess +
                         'protetta dalle normative interne, se sei autorizzato premi SI, altrimenti premi NO. ' +
                         'Se si prosegue, l''operazione verr� registrata nello storico del soggetto come forzatura.', mtWarning, [mbYes, mbNo], 0) = mrNo then xVai := False
                    else begin

                         if pos('INTERMEDIA', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) = 0 then begin
                              // pwd
                              xPassword := '';
                              xUtenteResp := GetDescUtenteResp(MainForm.xIDUtenteAttuale);
                              // richiesta password
                              InputPasswordForm.EPassword.text := 'EPassword';
                              InputPasswordForm.caption := 'Inserimento password utente';
                              InputPasswordForm.ShowModal;
                              if InputPasswordForm.ModalResult = mrOK then begin
                                   xPassword := InputPasswordForm.EPassword.Text;
                              end else exit;
                              if not CheckPassword(xPassword, GetPwdUtente(MainForm.xIDUtenteAttuale)) then begin
                                   MessageDlg('Password errata', mtError, [mbOK], 0);
                                   xVai := False;
                              end;
                         end;
                         if xVai then begin
                              // promemoria al resp.
                              QLocal := CreateQueryFromString('insert into Promemoria (IDUtente,IDUtenteDa,DataIns,Testo,Evaso,DataDaLeggere) ' +
                                   'values (:xIDUtente:,:xIDUtenteDa:,:xDataIns:,:xTesto:,:xEvaso:,:xDataDaLeggere:)');
                              QLocal.ParamByName['xIDUtente'] := GetIDUtenteResp(MainForm.xIDUtenteAttuale);
                              QLocal.ParamByName['xIDUtenteDa'] := MainForm.xIDUtenteAttuale;
                              QLocal.ParamByName['xDataIns'] := Date;
                              QLocal.ParamByName['xTesto'] := 'forzatura livello ' + xLivProtez + ' per CV n� ' + ElencoDipForm.TAnagDip.FieldByName('CVNumero').AsString;
                              QLocal.ParamByName['xEvaso'] := 0;
                              QLocal.ParamByName['xDataDaLeggere'] := Date;
                              QLocal.ExecSQL;
                              // registra nello storico soggetto
                              xIDEvento := GetIDEvento('forzatura Blocco livello ' + xLivProtez);
                              if xIDEvento > 0 then begin
                                   with Data.Q1 do begin
                                        close;
                                        SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
                                             'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xIDCliente:)';
                                        ParamByName['xIDAnagrafica'] := ElencoDipForm.TAnagDip.FieldByName('ID').AsInteger;
                                        ParamByName['xIDEvento'] := xIDevento;
                                        ParamByName['xDataEvento'] := Date;
                                        ParamByName['xAnnotazioni'] := 'cliente: ' + xClienteBlocco;
                                        ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
                                        ParamByName['xIDUtente'] := MainForm.xIDUtenteAttuale;
                                        ParamByName['xIDCliente'] := xIDClienteBlocco;
                                        ExecSQL;
                                   end;
                              end;
                         end;
                    end;
               end;

               if xVai then begin
                    Data.DB.BeginTrans;
                    try
                         if Q.Active then Q.Close;
                         Q.SQL.text := 'insert into EBC_CandidatiRicerche (IDRicerca,IDAnagrafica,Escluso,Stato,DataIns) ' +
                              'values (:xIDRicerca:,:xIDAnagrafica:,:xEscluso:,:xStato:,:xDataIns:)';
                         Q.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
                         Q.ParamByName['xIDAnagrafica'] := ElencoDipForm.TAnagDip.FieldByName('ID').AsInteger;
                         Q.ParamByName['xEscluso'] := 0;
                         if pos('EMERGENCY', UpperCase(data.Global.fieldbyname('NomeAzienda').asString)) <= 0 then
                              Q.ParamByName['xStato'] := 'inserito direttamente da elenco'
                         else Q.ParamByName['xStato'] := ' ';
                         Q.ParamByName['xDataIns'] := Date;
                         Q.ExecSQL;
                         // aggiornamento stato anagrafica
                         if (not (ElencoDipForm.TAnagDip.FieldByName('IDTipoStato').AsInteger in [6, 7, 8])) then begin
                              if Q.Active then Q.Close;
                              if pos('EMERGENCY', UpperCase(data.Global.fieldbyname('NomeAzienda').asString)) > 0 then
                                   Q.SQL.text := 'update Anagrafica set IDStato=28,IDTipoStato=2 where ID=' + ElencoDipForm.TAnagDip.FieldByName('ID').asString
                              else Q.SQL.text := 'update Anagrafica set IDStato=27,IDTipoStato=1 where ID=' + ElencoDipForm.TAnagDip.FieldByName('ID').asString;
                              Q.ExecSQL;
                         end;



                         // aggiornamento storico
                         if Q.Active then Q.Close;
                         Q.SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
                              'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xIDCliente:)';
                         Q.ParamByName['xIDAnagrafica'] := ElencoDipForm.TAnagDip.FieldByName('ID').AsInteger;
                         { if pos('EMERGENCY', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) > 0 then
                              Q.ParamByName['xIDEvento'] := 66
                         else }
                         Q.ParamByName['xIDEvento'] := 19;
                         Q.ParamByName['xDataEvento'] := date;
                         Q.ParamByName['xAnnotazioni'] := 'ric.n�' + TrimRight(DataRicerche.TRicerchePend.FieldByName('Progressivo').AsString) + ' (' + TrimRight(DataRicerche.TRicerchePend.FieldByName('Cliente').AsString) + ')';
                         Q.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
                         Q.ParamByName['xIDUtente'] := MainForm.xIDUtenteAttuale;
                         Q.ParamByName['xIDCliente'] := DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger;
                         Q.ExecSQL;
                         Data.DB.CommitTrans;
                         DataRicerche.QCandRic.Close;
                         DataRicerche.QCandRic.Open;
                         LTotCand.Caption := IntToStr(DataRicerche.QCandRic.RecordCount);
                    except
                         Data.DB.RollbackTrans;
                         MessageDlg('Errore sul database:  inserimento non avvenuto', mtError, [mbOK], 0);
                    end;
               end;
          end;
          DataRicerche.TRicAnagIDX.Close;
     end;
     ElencoDipForm.Free;
     RiprendiAttivita;
     if pos('EMERGENCY', UpperCase(data.Global.fieldbyname('NomeAzienda').asString)) > 0 then dxDBGrid1.FullCollapse
     else dxDBGrid1.FullExpand;
end;

//[TONI20021002]DEBUGOK

procedure TSelPersForm.SpeedButton1Click(Sender: TObject);
var xNote: string;
     xIDOldRuolo: integer;
begin
     xIDOldRuolo := DataRicerche.TRicerchePend.FieldByName('IDMansione').AsInteger;
     InsRuoloForm := TInsRuoloForm.create(self);
     InsRuoloForm.ShowModal;
     if InsRuoloForm.ModalResult = mrOK then begin
          if not (DataRicerche.DsRicerchePend.State in [dsInsert, dsEdit]) then
               DataRicerche.TRicerchePend.Edit;
          DataRicerche.TRicerchePend.FieldByName('IDMansione').AsInteger := InsRuoloForm.TRuoli.FieldByName('ID').AsInteger;
          DataRicerche.TRicerchePend.FieldByName('IDArea').AsInteger := InsRuoloForm.TRuoli.FieldByName('IDArea').AsInteger;
          with DataRicerche.TRicerchePend do begin
               Data.DB.BeginTrans;
               try
                    post;
                    Data.DB.CommitTrans;
                    CaricaApriRicPend(DataRicerche.QRicAttive.FieldByName('ID').AsInteger);

                    // storicizza modifica ruolo
                    InsNoteForm := TInsNoteForm.create(self);
                    InsNoteForm.Caption := 'Modifica del ruolo';
                    InsNoteForm.ShowModal;
                    if InsNoteForm.ModalResult = mrOK then
                         xNote := InsNoteForm.MNote.text
                    else xNote := '';

                    Data.QTemp.Close;
                    Data.QTemp.SQL.clear;
                    Data.QTemp.SQL.text := 'insert into EBC_Ricerche_StoricoRuolo (IDRicerca,IDMansione,Data,IDUtente,Note) ' +
                         'values (:xIDRicerca,:xIDMansione,:xData,:xIDUtente,:xNote)';
                    Data.QTemp.Parameters[0].value := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
                    Data.QTemp.Parameters[1].value := xIDOldRuolo;
                    Data.QTemp.Parameters[2].value := Date + Time;
                    Data.QTemp.Parameters[3].value := MainForm.xIDUtenteAttuale;
                    Data.QTemp.Parameters[4].value := xNote;
                    Data.QTemp.ExecSQL;

                    InsNoteForm.Free;
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                    raise;
               end;
          end;
          InsRuoloForm.Free;
     end else begin
          InsRuoloForm.Free;
          Abort;
     end;
end;

procedure TSelPersForm.SpeedButton2Click(Sender: TObject);
begin
     if MessageDlg('ATTENZIONE: stai cambiando il cliente !! - Continuare ?', mtWarning, [mbYes, mbNo], 0) = mrYes then begin
          SelClienteForm := TSelClienteForm.create(self);
          SelClienteForm.ShowModal;
          if SelClienteForm.ModalResult = mrOK then begin
               DataRicerche.TRicerchePend.Edit;
               DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger := SelClienteForm.TClienti.FieldByName('ID').AsInteger;
               with DataRicerche.TRicerchePend do begin
                    Data.DB.BeginTrans;
                    try
                         //                         ApplyUpdates;
                         POst;
                         Data.DB.CommitTrans;
                         CaricaApriRicPend(DataRicerche.QRicAttive.FieldByName('ID').AsInteger);
                    except
                         Data.DB.RollbackTrans;
                         MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                         raise;
                    end;
                    //                    CommitUpdates;
               end;
          end;
          SelClienteForm.Free;
     end;
end;

//[TONI20021003]DEBUGOK

procedure TSelPersForm.ToolbarButton974Click(Sender: TObject);
var i, xIDAnag: integer;
     SavePlace: TBookmark;
begin
     if not Mainform.CheckProfile('213') then Exit;
     xIDAnag := DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger;
     DataRicerche.QCandRic.Close;
     DataRicerche.QCandRic.Open;

     if DataRicerche.QCandRic.RecordCount > 0 then begin
          FaxPresentazForm := TFaxPresentazForm.create(self);
          if faxpresentazform.QStatiCliente.Active = false then faxpresentazform.QStatiCliente.open;
          FaxPresentazForm.ASG1.RowCount := DataRicerche.QCandRic.RecordCount + 1;
          DataRicerche.QCandRic.First;
          i := 1;
          while not DataRicerche.QCandRic.EOF do begin
               FaxPresentazForm.ASG1.addcheckbox(0, i, false, false);
               FaxPresentazForm.ASG1.Cells[0, i] := DataRicerche.QCandRic.FieldByName('Cognome').AsString + ' ' + DataRicerche.QCandRic.FieldByName('Nome').AsString;
               FaxPresentazForm.xArrayIDAnag[i] := DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger;
               // showmessage(DataRicerche.QCandRic.FieldByName('StatoCliente').AsString);
               FaxPresentazForm.ASG1.Cells[3, i] := DataRicerche.QCandRic.FieldByName('StatoCliente').AsString;
               DataRicerche.QCandRic.Next;
               inc(i);
          end;
          //FaxPresentazForm.QClienti.ReloadSQL;
          FaxPresentazForm.QClienti.Close;
          FaxPresentazForm.QClienti.ParamByName['xIDCliente'] := DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger;
          FaxPresentazForm.QClienti.open;
          FaxPresentazForm.TContattiCli.Open;

          // filtro per stato cliente
        { FaxPresentazForm.pfiltro.visible := true;
          FaxPresentazForm.qstaticliente.open;
          FaxPresentazForm.CBFiltroStatoCliente.Items.Add('TUTTI');
          while not FaxPresentazForm.qstaticliente.eof do begin
               FaxPresentazForm.CBFiltroStatoCliente.Items.Add(FaxPresentazForm.qstaticlientedescrizione.asstring);
               FaxPresentazForm.qstaticliente.next;
          end;
             FaxPresentazForm.CBFiltroStatoCliente.ItemIndex:=0;
             }

          FaxPresentazForm.xTotRighe := dataricerche.qcandric.RecordCount;
          FaxPresentazForm.ShowModal;

          // SetWindowPos(FaxPresentazForm.Handle,
          //                                        HWND_TOPMOST,
          //                                        0, 0, 0, 0,
          //                                        SWP_NOMOVE or
          //                                        SWP_NOACTIVATE or
          //                                        SWP_NOSIZE);

          SavePlace := DataRicerche.QCandRic.GetBookmark;
          DataRicerche.QCandRic.Close;
          DataRicerche.QCandRic.Open;
          DataRicerche.QCandRic.GotoBookmark(SavePlace);
          DataRicerche.QCandRic.FreeBookmark(SavePlace);
          FaxPresentazForm.Free;
     end;
     RiprendiAttivita;

     DataRicerche.QCandRic.Close;
     DataRicerche.QCandRic.Open;
     if pos('EMERGENCY', UpperCase(data.Global.fieldbyname('NomeAzienda').asString)) > 0 then dxDBGrid1.FullCollapse
     else dxDBGrid1.FullExpand;
     DataRicerche.QCandRic.Locate('IDAnagrafica', xIDAnag, []);
     DataRicerche.QCandRic.FieldByName('IDAnagrafica').FocusControl;
end;

procedure TSelPersForm.ToolbarButton9712Click(Sender: TObject);
var i: integer;
     Vero, xInserito: boolean;
     xIDAnag: integer;
begin
     if not Mainform.CheckProfile('050') then Exit;
     xIDAnag := DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger;
     if DataRicerche.QCandRic.RecordCount > 0 then begin
          CambiaStato2Form := TCambiaStato2Form.create(self);
          CambiaStato2Form.ECogn.Text := DataRicerche.QCandRic.FieldByName('Cognome').AsString;
          CambiaStato2Form.ENome.Text := DataRicerche.QCandRic.FieldByName('Nome').AsString;
          CambiaStato2Form.PanStato.Visible := False;
          CambiaStato2Form.xIDDaStato := DataRicerche.QCandRic.FieldByName('IDStato').AsInteger;
          CambiaStato2Form.DEData.Date := Date;
          CambiaStato2Form.ShowModal;
          if CambiaStato2Form.ModalResult = mrOK then begin
               xInserito := InsEvento(DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger,
                    CambiaStato2Form.QNextStati.FieldByName('IDProcedura').AsInteger,
                    CambiaStato2Form.QNextStati.FieldByName('ID').AsInteger,
                    CambiaStato2Form.QNextStati.FieldByName('IDaStato').AsInteger,
                    CambiaStato2Form.QNextStati.FieldByName('IDTipoStatoA').AsInteger,
                    CambiaStato2Form.DEData.Date,
                    DataRicerche.QCandRic.FieldByName('Cognome').AsString,
                    DataRicerche.QCandRic.FieldByName('Nome').AsString,
                    CambiaStato2Form.Edit1.Text,
                    DataRicerche.TRicerchePend.FieldByName('ID').AsInteger,
                    MainForm.xIDUtenteAttuale,
                    DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger);
               RiprendiAttivita;
          end;
          CambiaStato2Form.Free;
          DataRicerche.QCandRic.Close;
          DataRicerche.QCandRic.Open;
          if pos('EMERGENCY', UpperCase(data.Global.fieldbyname('NomeAzienda').asString)) > 0 then dxDBGrid1.FullCollapse
          else dxDBGrid1.FullExpand;
          DataRicerche.QCandRic.Locate('IDAnagrafica', xIDAnag, []);
          DataRicerche.QCandRic.FieldByName('IDAnagrafica').FocusControl;
          RegistraVisioneCand(MainForm.xIDUtenteAttuale, DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger);
          LTotCand.Caption := IntToStr(DataRicerche.QCandRic.RecordCount);
     end;
end;

procedure TSelPersForm.EseguiQueryCandidati;
begin
     if data.Global.fieldbyname('debughrms').AsBoolean then Showmessage('EseguiQueryCandidati 1');

    // showmessage(DataRicerche.TRicerchePend.FieldByname('ID').asString);


     DataRicerche.QCandRic.Close;
     DataRicerche.QCandRic.SQL.Clear;
     DataRicerche.QCandRic.SQL.Text :=
          'SELECT DISTINCT EBC_CandidatiRicerche.ID' + chr(13) +
          '	,EBC_CandidatiRicerche.IDRicerca' + chr(13) +
          '	,Anagrafica.Cognome' + chr(13) +
          '	,Anagrafica.Nome' + chr(13) +
          '	,Anagrafica.CVNumero' + chr(13) +
          '	,Anagrafica.IDStato' + chr(13) +
          '	,Cellulare' + chr(13) +
          '	,TelUfficio' + chr(13) +
          '	,DataNascita' + chr(13) +
          '	,RecapitiTelefonici' + chr(13) +
          '	,Anagrafica.DomicilioStato' + chr(13) +
          '	,EBC_Clienti.Descrizione ProprietaCV' + chr(13) +
          '	,Anagrafica.domicilioRegione' + chr(13) +
          '	,Anagrafica.domicilioprovincia' + chr(13) +
          '	,Anagrafica.domicilioindirizzo' + chr(13) +
          '	,Anagrafica.domiciliotipostrada' + chr(13) +
          '	,Anagrafica.domiciliocomune' + chr(13) +
          '	,Anagrafica.domiciliocap' + chr(13) +
          '	,Anagrafica.domicilionumcivico' + chr(13) +
          '	,Anagrafica.Regione' + chr(13) +
          '	,Anagrafica.Stato ResidenzaStato' + chr(13) +
          '	,Anagrafica.Provincia' + chr(13) +
          '	,Anagrafica.Comune' + chr(13) +
          '	,anagrafica.indirizzo' + chr(13) +
          '	,anagrafica.cap' + chr(13) +
          '	,anagrafica.numcivico' + chr(13) +
          '	,anagrafica.tipostrada' + chr(13) +
          '	,anagrafica.Email' + chr(13) +
          '	,EBC_CandidatiRicerche.IDAnagrafica' + chr(13) +
          '	,CAST(EBC_CandidatiRicerche.Escluso AS INT) Escluso' + chr(13) +
          '	,EBC_CandidatiRicerche.DataImpegno' + chr(13) +
          '	,EBC_CandidatiRicerche.Codstato' + chr(13) +
          '	,EBC_CandidatiRicerche.Stato' + chr(13) +
          '	,EBC_CandidatiRicerche.Note2 note' + chr(13) +
          '	,EBC_TipiStato.TipoStato' + chr(13) +
          '	,EBC_CandidatiRicerche.DataIns' + chr(13) +
          '	,EBC_CandidatiRicerche.MiniVal' + chr(13) +
          '	,EBC_ContattiCandidati.Data DataUltimoContatto' + chr(13) +
          '	,FlagAccPres' + chr(13) +
          '	,EBC_CandidatiRicerche.NoteCliente' + chr(13) +
          '	--,min(TitoliStudio.ID) IDTitoloStudioMin' + chr(13) +
          '	,EBC_CandidatiRicerche.IDStatoCliente' + chr(13) +
          '    ,ebc_ricerchestaticliente.Descrizione StatoCliente' + chr(13) +
          '	,VisibleCliente' + chr(13) +
          '	,Storico.IDEvento' + chr(13) +
          '	,Colore' + chr(13) +
          '	,AnagAltreInfo.Retribuzione' + chr(13) +
          '	,AnagAltreInfo.RetribVariabile' + chr(13) +
          '	,AnagAltreInfo.RetribTotale' + chr(13) +
          '	,EBC_CandidatiRicerche.Tipo' + chr(13) +
          '	,AnagAltreInfo.Inquadramento' + chr(13) +
          '	,Ruolo' + chr(13) +
          '	,EBC_CandidatiRicerche.Bloccato' + chr(13) +
          '	,EBC_CandidatiRicerche.IDIndicazione' + chr(13) +
          '	,EBC_CandidatiRicerche.Visionato' + chr(13) +
          '	,googledistanzaric.distanza' + chr(13) +
          '	,googledistanzaric.tempo' + chr(13) +
          //'	,googledistanzaric.distanzaDomicilio' + chr(13) +
//'	,googledistanzaric.tempoDomicilio' + chr(13) +
     '	,EBC_CandidatiRicerche.DataPrevRichiamo' + chr(13) +
          '	,MotiviStatoSospensione.DescMotivoStatoSosp' + chr(13) +
          '	,Anagrafica.DataFineStatoSOSPESO' + chr(13) +
          '	,MotiviRichiamoCandidati.DescMotivoRichiamoCandidato' + chr(13) +
          '	,EBC_Stati.Stato StatoCand' + chr(13) +
          '	,CVInseritoInData' + chr(13) +
          '	,EspLavAttuale.Azienda' + chr(13) +
          '     ,EspLavAttuale.AreaSettore' + chr(13) +
          '    ,EspLavAttuale.Settore SettoreAz' + chr(13) +
          '    ,EspLavAttuale.Telefono TelAzienda' + chr(13) +
          '    ,EspLavAttuale.JobTitle' + chr(13) +
          '    ,TitStudio.Diploma' + chr(13) +
          '    ,M.Descrizione Mansione' + chr(13) +
          '    ,AM.AreaCand' + chr(13) +
          '    ,AM.MansioneCand' + chr(13) +
          '    ,TC.Descrizione Classificazione' + chr(13) +
          'FROM EBC_CandidatiRicerche' + chr(13) +
          'JOIN Anagrafica ON EBC_CandidatiRicerche.IDAnagrafica = Anagrafica.ID' + chr(13) +
          'JOIN AnagAltreInfo ON EBC_CandidatiRicerche.IDAnagrafica = AnagAltreInfo.IDAnagrafica' + chr(13) +
          'JOIN EBC_TipiStato ON Anagrafica.IDTipoStato = EBC_TipiStato.ID' + chr(13) +
          'JOIN EBC_Stati ON Anagrafica.IDStato = EBC_Stati.ID' + chr(13) +
          'JOIN EBC_Ricerche ERic on EBC_CandidatiRicerche.IDRicerca = ERic.ID' + chr(13) +
          'LEFT JOIN MANSIONI M on ERic.IDMansione = M.ID' + chr(13) +
          'LEFT JOIN TabClassificazioni TC on Anagrafica.IDClassificazione = TC.ID' + chr(13) +
          'LEFT JOIN googledistanzaric ON googledistanzaric.idanagrafica = anagrafica.id' + chr(13) +
          '	AND googledistanzaric.idricerca = EBC_CandidatiRicerche.idricerca ' + chr(13) +
          'LEFT JOIN (' + chr(13) +
          '	SELECT *' + chr(13) +
          '	FROM anagcolorimoduli' + chr(13) +
          //'	WHERE idutente = 9' + chr(13) +

     //Data.QTempAdoStd1.sql.Text := ' select IDUtente from EBC_RicercheUtenti where IDRicerca=:xIDRicerca and IDUtente=:xIDUtente ';
     ////////////////////'	WHERE IDUtente=:xIDUtente ' + chr(13) +
     '	WHERE idutente = ' + IntToStr(MainForm.xIDUtenteAttuale) + ' ' + chr(13) +
          '	) anagcolorimoduli ON anagcolorimoduli.IDCandRicerche = ebc_candidatiricerche.id' + chr(13) +
          'LEFT JOIN EBC_ContattiCandidati ON (' + chr(13) +
          '		(EBC_CandidatiRicerche.IDAnagrafica = EBC_ContattiCandidati.IDAnagrafica)' + chr(13) +
          '		AND (' + chr(13) +
          '			EBC_ContattiCandidati.Data = (' + chr(13) +
          '				SELECT max(Data)' + chr(13) +
          '				FROM EBC_ContattiCandidati' + chr(13) +
          '				WHERE EBC_CandidatiRicerche.IDAnagrafica = EBC_ContattiCandidati.IDAnagrafica' + chr(13) +
          '				)' + chr(13) +
          '			)' + chr(13) +
          '		)' + chr(13) +
          'LEFT JOIN TitoliStudio ON EBC_CandidatiRicerche.IDAnagrafica = TitoliStudio.IDAnagrafica' + chr(13) +
          'LEFT JOIN EBC_RicercheStatiCliente ON EBC_CandidatiRicerche.IDStatoCliente = EBC_RicercheStatiCliente.ID' + chr(13) +
          'LEFT JOIN Storico ON (' + chr(13) +
          '		(EBC_CandidatiRicerche.IDAnagrafica = Storico.IDAnagrafica)' + chr(13) +
          '		AND (' + chr(13) +
          '			Storico.ID = (' + chr(13) +
          '				SELECT TOP 1 ID' + chr(13) +
          '				FROM Storico' + chr(13) +
          '				WHERE EBC_CandidatiRicerche.IDAnagrafica = Storico.IDAnagrafica' + chr(13) +
          '				ORDER BY DataEvento DESC' + chr(13) +
          '				)' + chr(13) +
          '			)' + chr(13) +
          '		)' + chr(13) +
          'LEFT JOIN MotiviStatoSospensione ON MotiviStatoSospensione.ID = Anagrafica.IDMotivoStatoSosp' + chr(13) +
          'LEFT JOIN MotiviRichiamoCandidati ON MotiviRichiamoCandidati.ID = EBC_candidatiRicerche.IDMotivoRichiamoCandidato' + chr(13) +
          'LEFT JOIN EBC_Clienti ON EBC_Clienti.id = Anagrafica.IDProprietaCV' + chr(13) +
          '-- esperienza lavorativa attuale (ultima, se pi� di una) ' + chr(13) +
          'LEFT OUTER JOIN (' + chr(13) +
          '	SELECT IDAnagrafica' + chr(13) +
          '		,max(ID) MaxID' + chr(13) +
          '	FROM EsperienzeLavorative' + chr(13) +
          '	WHERE Attuale = 1' + chr(13) +
          '	GROUP BY IDAnagrafica' + chr(13) +
          '	) EspLav_MaxID ON Anagrafica.ID = EspLav_MaxID.IDAnagrafica' + chr(13) +
          'LEFT OUTER JOIN (' + chr(13) +
          '	SELECT Esperienzelavorative.ID' + chr(13) +
          '		,Esperienzelavorative.IDAnagrafica' + chr(13) +
          '		,Azienda = CASE ' + chr(13) +
          '			WHEN Esperienzelavorative.IDAzienda IS NULL' + chr(13) +
          '				THEN Esperienzelavorative.AziendaNome' + chr(13) +
          '			ELSE EBC_Clienti.Descrizione' + chr(13) +
          '			END' + chr(13) +
          '		--,titolomansione' + chr(13) +
          '		,Mansioni.Descrizione Ruolo' + chr(13) +
          '		--,cast(descrizionemansione AS VARCHAR(255)) descrizionemansione' + chr(13) +
          '		,ruolopresentaz AS JobTitle' + chr(13) +
          '		,Aree.Descrizione Area' + chr(13) +
          '		,ebc_attivita.attivita Settore,AReeSEttori.AreaSEttore' + chr(13) +
          '	    ,ebc_clienti.Telefono' + chr(13) +
          '	FROM Esperienzelavorative' + chr(13) +
          '	LEFT JOIN EBC_Clienti ON Esperienzelavorative.IDAzienda = EBC_Clienti.ID' + chr(13) +
          '	LEFT JOIN Mansioni ON Esperienzelavorative.IDMansione = Mansioni.ID' + chr(13) +
          '	LEFT JOIN aree ON Mansioni.IDArea = aree.id' + chr(13) +
          '	LEFT JOIN ebc_attivita ON ebc_attivita.id = esperienzelavorative.idsettore' + chr(13) +
          '     LEFT JOIN AreeSettori on AReeSEttori.ID=EBC_Attivita.IDAreaSettore' + chr(13) +
          '	) EspLavAttuale ON EspLav_MaxID.MaxID = EspLavAttuale.ID' + chr(13) +
          '-- titolo di studio minimo' + chr(13) +
          'LEFT OUTER JOIN (' + chr(13) +
          '	SELECT IDAnagrafica' + chr(13) +
          '		,min(ID) MinID' + chr(13) +
          '	FROM TitoliStudio' + chr(13) +
          '	GROUP BY IDAnagrafica' + chr(13) +
          '	) TitoliStudio_MinID ON Anagrafica.ID = TitoliStudio_MinID.IDAnagrafica' + chr(13) +
          'LEFT OUTER JOIN (' + chr(13) +
          '	select TitoliStudio.ID, Descrizione Diploma' + chr(13) +
          '	from TitoliStudio, Diplomi' + chr(13) +
          '	where TitoliStudio.IDDiploma = Diplomi.ID' + chr(13) +
          '	) TitStudio ON TitoliStudio_MinID.MinID = TitStudio.ID' + chr(13) +
          'LEFT OUTER JOIN (' + chr(13) +
          '-- PRIMO RUOLO ' + chr(13) +
          '	select IDAnagrafica,MAX(ID) MaxID' + chr(13) +
          '	from anagmansioni' + chr(13) +
          '	Where anagmansioni.attuale=1' + chr(13) +
          '	group by idanagrafica' + chr(13) +
          '	) Ruoli_MaxID on Anagrafica.ID = Ruoli_MaxID.IDAnagrafica' + chr(13) +
          'LEFT OUTER JOIN (' + chr(13) +
          '	select AnagMansioni.IDAnagrafica,Mansioni.Descrizione MansioneCand,' + chr(13) +
          '     Aree.Descrizione AreaCand, AnagMansioni.ID' + chr(13) +
          '	from AnagMansioni JOIN Mansioni' + chr(13) +
          '	on Mansioni.ID = AnagMansioni.IDMansione' + chr(13) +
          '	JOIN aree ON Mansioni.IDArea = aree.id' + chr(13) +
          '	) AM ON AM.ID = Ruoli_MaxID.MaxID' + chr(13) +
          'where EBC_CandidatiRicerche.IDRicerca= ' + DataRicerche.TRicerchePend.FieldByname('ID').asString + ' ' + chr(13) +
        ///////////////  'where EBC_CandidatiRicerche.IDRicerca=:xIDRicerca ' + ' ' + chr(13) +

          //Data.QTempAdoStd1.sql.Text := ' select IDUtente from EBC_RicercheUtenti where IDRicerca=:xIDRicerca and IDUtente=:xIDUtente ';

       '	AND EBC_CandidatiRicerche.InsFromTL = 1';
    if CBOpzioneCand.Checked then
          DataRicerche.QCandRic.SQL.Add('and (Escluso=0 or Escluso is null)');




     if data.Global.fieldbyname('debughrms').AsBoolean then Showmessage('EseguiQueryCandidati 2');

     DataRicerche.QCandRic.SQL.Add('GROUP BY EBC_CandidatiRicerche.ID' + chr(13) +
          '	,EBC_CandidatiRicerche.IDRicerca' + chr(13) +
          '	,Anagrafica.DomicilioStato' + chr(13) +
          '	,EBC_Clienti.Descrizione' + chr(13) +
          '	,Anagrafica.domicilioRegione' + chr(13) +
          '	,Anagrafica.domicilioprovincia' + chr(13) +
          '	,Anagrafica.domicilioindirizzo' + chr(13) +
          '	,Anagrafica.domiciliotipostrada' + chr(13) +
          '	,Anagrafica.domiciliocomune' + chr(13) +
          '	,Anagrafica.domiciliocap' + chr(13) +
          '	,Anagrafica.domicilionumcivico' + chr(13) +
          '	,Anagrafica.Regione' + chr(13) +
          '	,Anagrafica.Stato' + chr(13) +
          '	,Anagrafica.Provincia' + chr(13) +
          '	,Anagrafica.Comune' + chr(13) +
          '	,anagrafica.indirizzo' + chr(13) +
          '	,anagrafica.cap' + chr(13) +
          '	,anagrafica.numcivico' + chr(13) +
          '	,anagrafica.tipostrada' + chr(13) +
          '	,anagrafica.Email' + chr(13) +
          '	,googledistanzaric.distanza' + chr(13) +
          '	,googledistanzaric.tempo' + chr(13) +
          //'	,googledistanzaric.distanzaDomicilio' + chr(13) +
     //'	,googledistanzaric.tempoDomicilio' + chr(13) +
          '	,Anagrafica.Cognome' + chr(13) +
          '	,Anagrafica.Nome' + chr(13) +
          '	,Anagrafica.CVNumero' + chr(13) +
          '	,Anagrafica.IDStato' + chr(13) +
          '	,Cellulare' + chr(13) +
          '	,TelUfficio' + chr(13) +
          '	,DataNascita' + chr(13) +
          '	,RecapitiTelefonici' + chr(13) +
          '	,EBC_CandidatiRicerche.IDAnagrafica' + chr(13) +
          '	,CAST(EBC_CandidatiRicerche.Escluso AS INT)' + chr(13) +
          '	,EBC_CandidatiRicerche.DataImpegno' + chr(13) +
          '	,EBC_CandidatiRicerche.Codstato' + chr(13) +
          '	,EBC_CandidatiRicerche.Stato' + chr(13) +
          '	,EBC_CandidatiRicerche.Note2' + chr(13) +
          '	,EBC_TipiStato.TipoStato' + chr(13) +
          '	,EBC_CandidatiRicerche.DataIns' + chr(13) +
          '	,EBC_CandidatiRicerche.MiniVal' + chr(13) +
          '	,EBC_ContattiCandidati.Data' + chr(13) +
          '	,FlagAccPres' + chr(13) +
          '	,EBC_CandidatiRicerche.NoteCliente' + chr(13) +
          '	,EBC_CandidatiRicerche.IDStatoCliente' + chr(13) +
          '     ,ebc_ricerchestaticliente.Descrizione' + chr(13) +
          '	,VisibleCliente' + chr(13) +
          '	,Storico.IDEvento' + chr(13) +
          '	,Colore' + chr(13) +
          '	,AnagAltreInfo.Retribuzione' + chr(13) +
          '	,AnagAltreInfo.RetribVariabile' + chr(13) +
          '	,AnagAltreInfo.RetribTotale' + chr(13) +
          '	,EBC_CandidatiRicerche.Tipo' + chr(13) +
          '	,AnagAltreInfo.Inquadramento' + chr(13) +
          '	,Ruolo' + chr(13) +
          '	,EBC_CandidatiRicerche.Bloccato' + chr(13) +
          '	,EBC_CandidatiRicerche.IDIndicazione' + chr(13) +
          '	,EBC_CandidatiRicerche.Visionato' + chr(13) +
          '	,EBC_CandidatiRicerche.DataPrevRichiamo' + chr(13) +
          '	,MotiviStatoSospensione.DescMotivoStatoSosp' + chr(13) +
          '	,Anagrafica.DataFineStatoSOSPESO' + chr(13) +
          '	,MotiviRichiamoCandidati.DescMotivoRichiamoCandidato' + chr(13) +
          '	,EBC_Stati.Stato' + chr(13) +
          '	,CVInseritoInData' + chr(13) +
          '	,EspLavAttuale.Azienda' + chr(13) +
          '     ,EspLavAttuale.AreaSettore' + chr(13) +
          '    ,EspLavAttuale.Settore' + chr(13) +
          '    ,EspLavAttuale.Telefono' + chr(13) +
          '    ,EspLavAttuale.JobTitle' + chr(13) +
          '	,TitStudio.Diploma' + chr(13) +
          '    ,M.Descrizione' + chr(13) +
          '    ,AM.AreaCand' + chr(13) +
          '    ,AM.MansioneCand' + chr(13) +
          '    ,TC.Descrizione' + chr(13) +
          'ORDER BY Anagrafica.Cognome' + chr(13) +
          '	,Anagrafica.Nome');


     if data.Global.fieldbyname('debughrms').AsBoolean then Showmessage('EseguiQueryCandidati 3');
     ScriviRegistry('QCandRic', DataRicerche.QCandRic.SQl.text);

     //showmessage(DataRicerche.QCandRic.SQl.text);
     
     if data.Global.fieldbyname('debughrms').AsBoolean then Showmessage('EseguiQueryCandidati 4');

     if data.Global.fieldbyname('debughrms').AsBoolean then DataRicerche.QCandRic.SQL.SaveToFile('EseguiQueryCandidati.sql');


     ///////////////////////////////////////////////
     //test mb 20200518

     //Data.QTemp.close;
    // Data.QTemp.sql.Text := ' select IDUtente from EBC_RicercheUtenti where IDRicerca=:xIDRicerca and IDUtente=:xIDUtente ';

     //Data.QTemp.sql.Text := DataRicerche.QCandRic.SQl.text;
    // Data.QTemp.Parameters.ParamByName('xIDRicerca').value := DataRicerche.TRicerchePend.FieldByname('ID').asString;
   //  Data.QTemp.Parameters.ParamByName('xIDUtente').value := IntToStr(MainForm.xIDUtenteAttuale);
   //  Data.QTemp.Open;
   //  showmessage('test sql');


     //DataRicerche.QCandRic.Close;
     //DataRicerche.QCandRic.SQL.Clear;
     //DataRicerche.QCandRic.SQL := Data.QTemp.SQL;

   //  showmessage(DataRicerche.TRicerchePend.FieldByname('ID').asString);

/////////////////////////////////////////////     DataRicerche.QCandRic.Parameters.ParamByName('xIDRicerca').value := strtoint(DataRicerche.TRicerchePend.FieldByname('ID').asString);
/////////////////////////////////////////////     DataRicerche.QCandRic.Parameters.ParamByName('xIDUtente').value := MainForm.xIDUtenteAttuale;
//     DataRicerche.QCandRic.Parameters.ParamByName('xIDRicerca').value := 1328;
//     DataRicerche.QCandRic.Parameters.ParamByName('xIDUtente').value := 36;

     ///DataRicerche.QCandRic.Open;
     //test mb 20200518
     ///////////////////////////////////////////////



     DataRicerche.QCandRic.Open;
     if data.Global.fieldbyname('debughrms').AsBoolean then Showmessage('EseguiQueryCandidati 5');

     LTotCand.Caption := IntToStr(DataRicerche.QCandRic.RecordCount);
     if data.Global.fieldbyname('debughrms').AsBoolean then Showmessage('EseguiQueryCandidati 6');

end;

//[TONI20021003]DEBUGOK

procedure TSelPersForm.CBOpzioneCandClick(Sender: TObject);
begin
     BRecupera.Enabled := not (CBOpzioneCand.Checked);
     EseguiQueryCandidati;
end;

//[TONI20021003]DEBUGOK

procedure TSelPersForm.ToolbarButton9713Click(Sender: TObject);
var i: integer;
begin
     RiprendiAttivita;
     if not Mainform.CheckProfile('5') then Exit;
     xCerca := True;
     SelPersNewForm := TSelPersNewForm.create(self);

     SelPersNewForm.xIDRicerca := xIDRicercaInt;

     // riempimento combobox1 con le tabelle
     SelPersNewForm.QTabelle.Close;
     SelPersNewForm.QTabelle.Open;
     SelPersNewForm.QTabelle.First;
     SelPersNewForm.QRes1.Close;
     // oggetti visibili ed invisibili
     SelPersNewForm.BAggiungiRic.Visible := True;
     SelPersNewForm.BAggiungiRic.Visible := True;
     // togliere nominativi gi� presenti in archivio
     DataRicerche.QCandRic.First;
     for i := 1 to DataRicerche.QCandRic.RecordCount do begin
          if Data.Global.fieldbyname('debughrms').AsBoolean = TRUE then showmessage('i=' + inttostr(i));
          if Data.Global.fieldbyname('debughrms').AsBoolean = TRUE then showmessage('idanag=' + DataRicerche.QCandRic.FieldByName('IDAnagrafica').asString);
          SelPersNewForm.xDaEscludere[i] := DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger;


          DataRicerche.QCandRic.Next;
     end;
     if DataRicerche.TRicerchePend.Active = false then DataRicerche.TRicerchePend.Open;
     SelPersNewForm.xidIndirizzoCliente := DataRicerche.TRicerchePendIDIndirizzoCliente.AsString;
     //per adex nn serve la prendo da dataricerche.QRicAttiveID.Value;    //SelPersNewForm.xidricerca:= DataRicerche.TRicerchePendID.AsString;
     DataRicerche.QCandRic.First;
     SelPersNewForm.xChiamante := 1;
     SelPersNewForm.ShowModal;
     SelPersNewForm.Free;
     RiprendiAttivita;
     if pos('EMERGENCY', UpperCase(data.Global.fieldbyname('NomeAzienda').asString)) > 0 then dxDBGrid1.FullCollapse
     else dxDBGrid1.FullExpand;
     xCerca := False;
end;

//[TONI20021003]DEBUGOK

procedure TSelPersForm.BInfoCandClick(Sender: TObject);
var xIDAnag: integer;
begin
     if not Mainform.CheckProfile('00') then Exit;
     xIDAnag := DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger;
     if DataRicerche.DsQCandRic.state = dsEdit then DataRicerche.QCandRic.Post;
     data.Global.open;
     if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 5)) = 'ERGON' then begin
          SchedaSinteticaForm := TSchedaSinteticaForm.create(self);
          SchedaSinteticaForm.xIDAnag := DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger;
          SchedaSinteticaForm.ShowModal;
          SchedaSinteticaForm.Free;
     end else begin
          SchedaCandForm := TSchedaCandForm.create(self);
          if not PosizionaAnag(DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger) then exit;
          SchedaCandForm.ShowModal;
          SchedaCandForm.Free;
          RiprendiAttivita;
     end;
     EseguiQueryCandidati;
     DataRicerche.QCandRic.Close;
     DataRicerche.QCandRic.Open;
     if pos('EMERGENCY', UpperCase(data.Global.fieldbyname('NomeAzienda').asString)) > 0 then dxDBGrid1.FullCollapse
     else dxDBGrid1.FullExpand;
     DataRicerche.QCandRic.Locate('IDAnagrafica', xIDAnag, []);
     DataRicerche.QCandRic.FieldByName('IDAnagrafica').FocusControl;
     RegistraVisioneCand(MainForm.xIDUtenteAttuale, DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger);
end;

//[TONI20021003]DEBUGOK

procedure TSelPersForm.ToolbarButton972Click(Sender: TObject);
var xFile: string;
     xProcedi: boolean;
     xIDAnag: integer;
begin
     xIDAnag := DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger;
     ApriCV(DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger);

     DataRicerche.QCandRic.Close;
     DataRicerche.QCandRic.Open;
     if pos('EMERGENCY', UpperCase(data.Global.fieldbyname('NomeAzienda').asString)) > 0 then dxDBGrid1.FullCollapse
     else dxDBGrid1.FullExpand;
     DataRicerche.QCandRic.Locate('IDAnagrafica', xIDAnag, []);
     DataRicerche.QCandRic.FieldByName('IDAnagrafica').FocusControl;
     RegistraVisioneCand(MainForm.xIDUtenteAttuale, DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger);
end;

//[TONI20021003]DEBUGOK

procedure TSelPersForm.ToolbarButton973Click(Sender: TObject);
var xIDAnag: integer;
begin
     if not Mainform.CheckProfile('054') then Exit;
     xIDAnag := DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger;
     ApriFileWord(DataRicerche.QCandRic.FieldByname('CVNumero').AsString);
     RiprendiAttivita;

     DataRicerche.QCandRic.Close;
     DataRicerche.QCandRic.Open;
     if pos('EMERGENCY', UpperCase(data.Global.fieldbyname('NomeAzienda').asString)) > 0 then dxDBGrid1.FullCollapse
     else dxDBGrid1.FullExpand;
     DataRicerche.QCandRic.Locate('IDAnagrafica', xIDAnag, []);
     DataRicerche.QCandRic.FieldByName('IDAnagrafica').FocusControl;
     RegistraVisioneCand(MainForm.xIDUtenteAttuale, DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger);
end;

//[TONI20021003]DEBUGOK

procedure TSelPersForm.BRecuperaClick(Sender: TObject);
var xPerche: string;
     xIDAnag: integer;
begin
     if not Mainform.CheckProfile('212') then Exit;
     xIDAnag := DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger;
     if (not DataRicerche.QCandRic.Eof) and (DataRicerche.QCandRic.FieldByName('Escluso').Value = 1) then begin
          if InputQuery('reinserimento nella ricerca', 'Motivo:', xPerche) then begin
               Data.DB.BeginTrans;
               try
                    DataRicerche.QGen.Close;
                    DataRicerche.QGen.SQL.Clear;
                    DataRicerche.QGen.SQL.Add('update EBC_CandidatiRicerche set Escluso=0,');
                    DataRicerche.QGen.SQL.Add('Stato=''reinserito (motivo: ' + StringReplace(copy(xPerche, 1, 230), '''', '''''', [rfReplaceAll]) + ')'' where IDAnagrafica=' + DataRicerche.QCandRic.FieldByName('IDAnagrafica').asString + ' and IDRicerca=' + DataRicerche.QCandRic.FieldByName('IDRicerca').asString);
                    //DataRicerche.QGen.SQL.Add('Stato=substr(reinserito (motivo: '+xPerche+'),1,80) where IDAnagrafica='+DataRicerche.QCandRic.FieldByName ('IDAnagrafica').asString+' and IDRicerca='+DataRicerche.QCandRic.FieldByName ('IDRicerca').asString);    Riga di codice sostituita con quella sopra
                    DataRicerche.QGen.execSQL;
                    // aggiornamento stato anagrafica
                    if not Data.TAnagABS.Active then Data.TAnagABS.Open;
                    if Data.TAnagABS.FieldByName('IDStato').AsInteger = 28 then begin
                         if Q.Active then Q.Close;
                         if pos('EMERGENCY', UpperCase(data.Global.fieldbyname('NomeAzienda').asString)) > 0 then
                              Q.SQL.text := 'update Anagrafica set IDStato=28,IDTipoStato=2 where ID=' + DataRicerche.QCandRic.FieldByName('IDAnagrafica').asString
                         else Q.SQL.text := 'update Anagrafica set IDStato=27,IDTipoStato=1 where ID=' + DataRicerche.QCandRic.FieldByName('IDAnagrafica').asString;
                         Q.ExecSQL;
                    end;
                    // aggiornamento storico
                    if Q.Active then Q.Close;
                    Q.SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
                         'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xIDCliente:)';
                    Q.ParamByName['xIDAnagrafica'] := DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger;
                    Q.ParamByName['xIDEvento'] := 78;
                    Q.ParamByName['xDataEvento'] := date;
                    Q.ParamByName['xAnnotazioni'] := copy('ric.' + TrimRight(StringReplace(DataRicerche.TRicerchePend.FieldByName('Progressivo').AsString, '"', '''', [rfReplaceAll, rfIgnoreCase])) + ' per ' + TrimRight(DataRicerche.TRicerchePend.FieldByName('Cliente').asString) + ' (motivo: ' + xPerche + ')', 1, 256);
                    Q.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
                    Q.ParamByName['xIDUtente'] := Mainform.xIDUtenteAttuale;
                    Q.ParamByName['xIDCliente'] := DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger;
                    Q.ExecSQL;
                    // aggiornamento stato anagrafica --> in selezione
                    if Q.Active then Q.Close;
                    if pos('EMERGENCY', UpperCase(data.Global.fieldbyname('NomeAzienda').asString)) > 0 then
                         Q.SQL.text := 'update Anagrafica set IDStato=28,IDTipoStato=2 where ID=' + DataRicerche.QCandRic.FieldByName('IDAnagrafica').asString
                    else Q.SQL.text := 'update Anagrafica set IDStato=27,IDTipoStato=1 where ID=' + DataRicerche.QCandRic.FieldByName('IDAnagrafica').asString;
                    Q.ExecSQL;

                    Data.DB.CommitTrans;
                    DataRicerche.QCandRic.Close;
                    DataRicerche.QCandRic.Open;
                    Data.TAnagABS.Close;
                    LTotCand.Caption := IntToStr(DataRicerche.QCandRic.RecordCount);
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('Errore sul database:  inserimento non avvenuto', mtError, [mbOK], 0);
                    raise;
               end;
          end;
          RiprendiAttivita;

          DataRicerche.QCandRic.Close;
          DataRicerche.QCandRic.Open;
          if pos('EMERGENCY', UpperCase(data.Global.fieldbyname('NomeAzienda').asString)) > 0 then dxDBGrid1.FullCollapse
          else dxDBGrid1.FullExpand;
          DataRicerche.QCandRic.Locate('IDAnagrafica', xIDAnag, []);
          DataRicerche.QCandRic.FieldByName('IDAnagrafica').FocusControl;
     end;
end;

procedure TSelPersForm.FormShow(Sender: TObject);
begin
     //Grafica

     SelPersForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel4.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel4.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel4.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel5.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel5.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel5.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel7.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel7.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel7.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel8.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel8.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel8.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel3.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel3.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel3.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     PanTiming.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     PanTiming.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     PanTiming.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel6.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel6.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel6.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel10.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel10.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel10.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel11.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel11.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel11.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel9.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel9.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel9.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel16.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel16.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel16.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel17.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel17.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel17.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel18.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel18.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel18.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel19.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel19.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel19.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel20.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel20.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel20.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel21.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel21.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel21.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     PanTimeSheet.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     PanTimeSheet.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     PanTimeSheet.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel24.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel24.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel24.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel25.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel25.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel25.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel30.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel30.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel30.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel31.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel31.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel31.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     PanCheckListCommessa.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     PanCheckListCommessa.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     PanCheckListCommessa.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel22.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel22.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel22.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel23.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel23.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel23.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel32.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel32.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel32.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel33.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel33.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel33.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel12.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel12.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel12.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel13.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel13.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel13.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel14.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel14.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel14.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel15.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel15.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel15.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     PanCausali.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     PanCausali.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     PanCausali.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel26.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel26.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel26.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel27.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel27.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel27.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel28.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel28.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel28.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel29.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel29.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel29.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel123.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel123.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel123.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel34.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel34.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel34.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel35.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel35.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel35.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel36.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel36.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel36.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel38.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel38.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel38.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel39.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel39.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel39.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel40.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel40.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel40.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel41.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel41.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel41.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel37.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel37.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel37.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel43.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel43.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel43.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     DBEdit1.color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     DBEdit3.color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     CampiPersFrame1.PanTop.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     CampiPersFrame1.Pan1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     CampiPersFrame1.Pan2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     CampiPersFrame1.Pan3.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     CampiPersFrame1.Pan4.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     CampiPersFrame1.Pan5.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     CampiPersFrame1.Pan6.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     CampiPersFrame1.Pan7.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     CampiPersFrame1.Pan8.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     CampiPersFrame1.Pan9.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     CampiPersFrame1.Pan10.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;

     if data.Global.fieldbyname('debughrms').AsBoolean then showmessage('selpers 3');

     xVecchiadataInizio := DbDateEdit971.Date;

     //viene eseguito un cotrollo simile sull'onchange della pcselpers per visualizzare il bottone e le due colonne nella griglia
    { if FileExists(ExtractFilePath(Application.ExeName) + 'CalcolaDistanza.html') then begin
          BCalcolaDistanza.Visible := true;
          dxDBGrid1Distanza.visible := true;
          dxDBGrid1Tempo.Visible := true;
          WebBrowser1.Navigate(ExtractFilePath(Application.ExeName) + 'CalcolaDistanza.html');
     end;
     }

     xidoffertaTemp := DataRicerche.TRicerchePend.FieldByName('IDOfferta').asString;
     xModificabile := true;

     // TARGET LIST -- ABILITATA SOLO SE IN GLOBAL.CHECKBOXINDICI il 4� carattere � = 1
     if copy(Data.Global.FieldByName('CheckBoxIndici').AsString, 4, 1) = '1' then
          xDisabilitaTargetList := False
     else xDisabilitaTargetList := True;

     if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 6)) = 'PROPOS' then
          SelPersForm.WindowState := wsMaximized;

     if (Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 3)) = 'RAY') or (Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 7)) = 'MISTRAL') then begin
          dxDBGrid4Column13.Visible := false;
          dxDBGrid4Column15.Visible := true;
          BTLCandVai.Height := 28;
          BTLCandNew.Height := 28;
          BTLCandInsNuovo.Height := 28;
          BTLDelSogg.Height := 28;
          BTLCandInsRic.Height := 28;
          BTLCandVai.Top := 240;
          BTLCandNew.Top := 268;
          BTLCandInsNuovo.Top := 296;
          BTLDelSogg.Top := 352;
          BTLCandInsRic.Top := 324;
          BTLCandInsRic.Visible := True;
     end;

     if Pos('BFK', UpperCase(Data.Global.FieldByName('NomeAzienda').asString)) > 0 then
          TSComCampPers.TabVisible := false;

     Caption := '[M/21] - ' + caption;

     { if not DataRicerche.QCandRic.Active then begin
          DataRicerche.QCandRic.SQL.Clear;
          DataRicerche.QCandRic.SQL.Text := 'select distinct EBC_CandidatiRicerche.ID,EBC_CandidatiRicerche.IDRicerca, ' +
               'Anagrafica.Cognome,Anagrafica.Nome,Anagrafica.CVNumero,Anagrafica.IDStato,Cellulare,TelUfficio,DataNascita, ' +
               ' Anagrafica.domicilioprovincia, Anagrafica.domicilioindirizzo,Anagrafica.domiciliotipostrada,Anagrafica.domiciliocomune,Anagrafica.domiciliocap,Anagrafica.domicilionumcivico, ' +
               ' Anagrafica.Provincia, Anagrafica.Comune,anagrafica.indirizzo,anagrafica.cap,anagrafica.numcivico,anagrafica.tipostrada, ' +
               'EBC_CandidatiRicerche.IDAnagrafica,EBC_CandidatiRicerche.Escluso, ' +
               'EBC_CandidatiRicerche.DataImpegno,EBC_CandidatiRicerche.Codstato, ' +
               '  EBC_CandidatiRicerche.Stato , Anagrafica.DomicilioStato,EBC_Clienti.Descrizione ProprietaCV, ' +
               'EBC_TipiStato.TipoStato,EBC_CandidatiRicerche.DataIns, ' +
               'EBC_CandidatiRicerche.MiniVal, ' +
               'EBC_ContattiCandidati.Data DataUltimoContatto, FlagAccPres, ' +
               'EBC_CandidatiRicerche.IDStatoCliente, VisibleCliente, IDIndicazione, ' +
               'googledistanzaric.distanza,googledistanzaric.tempo,cvinseritoindata ' +
               'from EBC_CandidatiRicerche,Anagrafica,EBC_TipiStato,EBC_ContattiCandidati,EBC_RicercheStatiCliente,EBC_Clienti ' +
               'where EBC_CandidatiRicerche.IDAnagrafica=Anagrafica.ID ' +
               'and Anagrafica.IDTipoStato=EBC_TipiStato.ID and EBC_CandidatiRicerche.IDRicerca=:ID ' +
               'and EBC_CandidatiRicerche.IDAnagrafica *= EBC_ContattiCandidati.IDAnagrafica ' +
               'and EBC_CandidatiRicerche.IDStatoCliente *= EBC_RicercheStatiCliente.ID ' +
               'and EBC_ContattiCandidati.Data= ' +
               '    (select max(Data) from EBC_ContattiCandidati ' +
               '     where EBC_CandidatiRicerche.IDAnagrafica *= EBC_ContattiCandidati.IDAnagrafica ' +
               '     and IDRicerca=' + DataRicerche.TRicerchePend.FieldByName('ID').asString + ') ' +
               ' and EBC_CandidatiRicerche.InsFromTL = 1 ' +
               ' and anagrafica.IDProprietaCV=EBC_Clienti.ID ' +
               ' order by Anagrafica.CVNumero';
          DataRicerche.QCandRic.Open;
     end; }


     if data.Global.fieldbyname('debughrms').AsBoolean then showmessage('selpers 4');

     EseguiQueryCandidati;

     if data.Global.fieldbyname('debughrms').AsBoolean then showmessage('selpers 5');

     xAttivo := True;
     xCerca := False;
     LTotCand.Caption := IntToStr(DataRicerche.QCandRic.RecordCount);
     LTiming.Caption := 'TIMING (riferito all''utente ' + MainForm.xUtenteAttuale + '):';

     // parametri evidenziazione date
     Q.SQl.text := 'select ggDiffDataIns,ggDiffDataUltimoContatto,CheckRicCandCV,CheckRicCandTel from global ';
     Q.Open;
     xggDiffDataIns := Q.FieldByname('ggDiffDataIns').asInteger;
     xggDiffDataUltimoContatto := Q.FieldByname('ggDiffDataUltimoContatto').asInteger;
     xCheckRicCandCV := Q.FieldByname('CheckRicCandCV').asBoolean;
     xCheckRicCandTel := Q.FieldByname('CheckRicCandTel').asBoolean;
     Q.Close;
     // personalizzazioni ERGON
     if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 5)) = 'ERGON' then begin
          BInfocand.Caption := 'scheda cand.';
          BCurriculum.Visible := False;
     end;
     // personalizzazioni EUREN
     if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 5)) = 'EUREN' then begin
          PanTiming.visible := False;
     end;
     if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 6)) = 'DUCATI' then begin
          PanCausali.visible := True;
          GroupBox10.Height := 128;
     end;

     // personalizzazioni Seltis
     if pos('SELTIS', Data.Global.FieldByName('NomeAzienda').AsString) > 0 then
          DBGAnnunciCandVisionato.Visible := true;

     if (pos('INTERMEDIA', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) > 0) then begin
          bApriFileOfferta.Visible := true;
          bAnnEvento.Visible := true;
          DBGAnnunciCandStato.Visible := true;
     end;

     if (pos('INTERMEDIA', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) > 0)
          or (pos('YOOX', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) > 0) then begin
          SelPersForm.Caption := 'Gestione dei progetti';
          Panel20.caption := '  parametri temporali specifici per il progetto';
          Panel21.caption := '  criteri di ricerca impostati per questo progetto';
          Panel22.caption := '  CheckList per questo progetto';
          Panel33.caption := '  Riepilogo CheckList per questo progetto';
          Panel14.caption := '  altri dati relativi a questo progetto';
          Panel13.caption := '  Note interne relative al progetto';
          Panel26.caption := '  Aziende appartenenti alla Target List per questo progetto';
          Panel29.caption := '  Cand. associati all''azienda sopra selezionata ed inseriti nel progetto';
          expModelloExcel.Visible := true;
          bSelOfferta.Visible := true;
     end;

     // se non si � un utente abilitato, non si possono modificare i dati
     if xComeAdmin = false then begin
          //if Data.Global.FieldByName('GestioneUtentiCommessa').AsBoolean = true then begin
          if (MainForm.xUtenteAttuale <> 'Administrator') then begin //and (Data.Global.FieldByName('UtentiCommessaSoloLettura').asString <> '') then begin
               if Data.Global.FieldByName('UtentiCommessaSoloLettura').asBoolean then begin
                    Q.Close;
                    Q.SQl.clear;
                    Q.SQl.text := 'select ID from EBC_RicercheUtenti where IDRicerca=:x0 and IDUtente=:x1';
                    Q.Parameters[0].Value := DataRicerche.TRicerchePend.FieldByName('ID').asInteger;
                    Q.Parameters[1].Value := MainForm.xIDUtenteAttuale;
                    Q.Open;
                    xModificabile := not Q.IsEmpty;
                    if not xModificabile then
                         ImpostaSoloLettura;
               end;
          end;
          //end;
     end;

     if xModificabile then
          if Pos('BFK', UpperCase(Data.Global.FieldByName('NomeAzienda').asString)) = 0 then begin
               InsertRicercheInUso(DataRicerche.TRicerchePend.FieldByName('ID').asInteger, MainForm.xIDUtenteAttuale);
          end;
     RiprendiAttivita;
     if not xFromOrgTL then
          PCSelPers.ActivePage := TSCandidati;

     FrameDBRich1.Editor.DataSource := DataRicerche.dsRicerchePend;

     ModificaFlagAcc.Checked := DataRicerche.qCandRicFlagAccPres.AsBoolean;

     // nascondi funzioni
     NascondiFunzioni;

     dxDBGrid1StatoCliente.DBGridLayout := MainForm.dxDBGridLayoutList1Item2;


     // Questionari
     TSQuestionari.TabVisible := False;
     PCQuest.ActivePage := TSQuest_dett;
     BRicQuest_H1Quest.visible := FileExists(ExtractFileDir(Application.ExeName) + '\H1Questionari.exe');
     if pos('EMERGENCY', UpperCase(data.Global.fieldbyname('NomeAzienda').asString)) > 0 then dxDBGrid1.FullCollapse
     else dxDBGrid1.FullExpand;

     //Gestione H1Sel Aziende
     if data.xSelAziende then begin
          GroupBox6.Visible := true;
          GBCliente.caption := 'Cliente Interno';
          GroupBox12.Visible := false;
          TSTimeSheet.TabVisible := false;
          TSTargetList.TabVisible := false;
          TSValutaz.caption := 'Killer Questions';
          GroupBox8.Caption := 'Sede operativa che gestisce il progetto';
          GroupBox9.Caption := 'Referente interno per il cliente interno (resp.aziendale)';
          Panel13.Caption := '  Note interne relative al progetto';
          Panel14.Caption := '    Altri dati relativi al progetto';
          Panel22.Caption := '  CheckList per il progetto';
          Panel10.Caption := '  Candidati associati agli annunci pervenuti per questo progetto';
          Panel33.Caption := '  Riepilogo CheckList per questo progetto';
          Panel20.Caption := '  Parametri temporali specifici per il progetto';
          Panel21.Caption := '  Criteri di ricerca impostati per questo progetto';
          Panel10.Caption := '  Candidati associati agli annunci pervenuti per questo progetto';
          selpersform.Caption := 'Gestione del Progetto';
          label7.caption := 'appartenenti a questo progetto';
          label13.caption := 'n� di giorni dalla data di prima apertura del progetto';
          label16.caption := 'n� di giorni dall''ultimo CONTATTO con un soggetto associato al progetto';
          label20.caption := 'n� di giorni dall''ultimo COLLOQUIO con un soggetto associato al progetto';
          label12.caption := 'Sono gli stessi che appaiono nel motore di ricerca ma salvati per questo progetto:' + chr(13) +
               'per modificarli � necessario utilizzare il motore di ricerca';
          label13.caption := 'n� di giorni dalla data di prima apertura del progetto';
          label16.caption := 'n� di giorni dall''ultimo CONTATTO con un soggetto associato al progetto';
          label20.caption := 'n� di giorni dall''ultimo COLLOQUIO con un soggetto associato al progetto';
          CliCandidatiFrame1.Label2.Caption := '  Situazione dei candidati per questo cliente interno';
          CliCandidatiFrame1.CBCandRic.Caption := 'Visualizza candidati solo per questo progetto';


          //09/02/2018 - sistemazione H1Sel Aziende AZ-FP-AH
          PanTiming.Visible := false;
          BQuestRisposteDate.visible := false;
          TSAttivita.TabVisible := false;
          TSQuestionari.TabVisible := false;
     end;

     // personalizzazioni SCR
     if Pos('S.C.R.', UpperCase(Data.Global.FieldByName('NomeAzienda').asString)) > 0 then begin
          MiniProfilo1.Visible := True;
          MiniProfiloanonimo1.Visible := True;
     end else begin
          MiniProfilo1.Visible := False;
          MiniProfiloanonimo1.Visible := False;
     end;

     // Statistiche commessa (Ricky 17/01)
     DataRicerche.QStatCommessa.Close;
     DataRicerche.QStatCommessa.Parameters[0].value := DataRicerche.TRicerchePendID.Value;
     DataRicerche.QStatCommessa.Open;

end;

procedure TSelPersForm.OraTimer(Sender: TObject);
var x, y: integer;
begin
     y := StrToInt(SecondiTotali.Text);
     SecondiTotali.Text := IntToStr(y + 1);
     if (xAttivo) or (xCerca) then begin
          x := StrToInt(SecondiAttivita.Text);
          SecondiAttivita.Text := IntToStr(x + 1);
     end;
end;

procedure TSelPersForm.TimerInattivoTimer(Sender: TObject);
begin
     TimerInattivo.Enabled := False;
     xAttivo := False;
end;

procedure TSelPersForm.FormCreate(Sender: TObject);
begin

     if data.Global.fieldbyname('debughrms').AsBoolean then showmessage('selpers.create');

     FrameDBRich1.Editor.DataSource := DataRicerche.dsRicerchePend;
     if data.Global.fieldbyname('debughrms').AsBoolean then showmessage('utente=' + inttostr(MainForm.xIDUtenteAttuale));
     if data.Global.fieldbyname('debughrms').AsBoolean then showmessage('idricerca=' + DataRicerche.TRicerchePend.FieldByName('ID').asString);

     Data2.Qtemp.SQL.text := 'select TotSec from TimingRic ' +
          ' where IDUtente=' + IntToStr(MainForm.xIDUtenteAttuale) +
          ' and IDRicerca=' + DataRicerche.TRicerchePend.FieldByName('ID').asString +
          ' and Data=:xData:';
     Data2.Qtemp.ParamByName['xData'] := Date;
     Data2.Qtemp.Open;
     if data.Global.fieldbyname('debughrms').AsBoolean then showmessage('selpers 1');
     if Data2.Qtemp.RecordCount = 0 then begin
          xMinStart := 0;
          Data2.Qtemp.Close;
          Data2.Qtemp.SQL.text := 'insert into TimingRic (IDUtente,IDRicerca,Data,TotSec)' +
               ' values (:xIDUtente:,:xIDRicerca:,:xData:,:xTotSec:)';
          Data2.Qtemp.ParamByName['xIDUtente'] := MainForm.xIDUtenteAttuale;
          Data2.Qtemp.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
          Data2.Qtemp.ParamByName['xData'] := Date;
          Data2.Qtemp.ParamByName['xTotSec'] := 0;
          Data2.Qtemp.ExecSQL;
          if data.Global.fieldbyname('debughrms').AsBoolean then showmessage('selpers 2');
     end else begin
          xMinStart := Data2.Qtemp.FieldByName('TotSec').asInteger;
          Data2.Qtemp.Close;
     end;

     xModificabile := true;

end;

procedure TSelPersForm.FormClose(Sender: TObject;
     var Action: TCloseAction);
begin
     Data2.Qtemp.SQL.text := 'update TimingRic set TotSec=' + IntToStr(xMinStart + StrToInt(SecondiAttivita.Text)) +
          ' where IDUtente=' + IntToStr(MainForm.xIDUtenteAttuale) +
          ' and IDRicerca=' + DataRicerche.TRicerchePend.FieldByName('ID').asString +
          ' and Data=:xData:';
     Data2.Qtemp.ParamByName['xData'] := Date;
     Data2.Qtemp.ExecSQL;
     //if DataRicerche.DsRicerchePend.state=dsEdit then begin
     //     DataRicerche.TRicerchePend.Post;
     //     DataRicerche.TRicerchePend.ApplyUpdates;
     //     DataRicerche.TRicerchePend.CommitUpdates;
     //end;
     if DataRicerche.DsQCandRic.state = dsEdit then DataRicerche.QCandRic.Post;
     DataRicerche.QCandRic.Close;
     if DataRicerche.QRicTargetList.active then DataRicerche.QRicTargetList.Close;
     if DataRicerche.QTLCandidati.active then DataRicerche.QTLCandidati.Close;

     if DataRicerche.DsQRicTargetList.State = dsEdit then DataRicerche.QRicTargetList.Post;

     if DataRicerche.QRicTimeSheet.Active then DataRicerche.QRicTimeSheet.Close;
     if DataRicerche.QUsersLK.Active then DataRicerche.QUsersLK.Close;
     if DataRicerche.QTimeRepDett.Active then DataRicerche.QTimeRepDett.Close;
     if DataRicerche.QTLCandidati.State = dsEdit then DataRicerche.QTLCandidati.Post;

     if DataRicerche.QValutazCand.Active then DataRicerche.QValutazCand.Close;

     DeleteRicercheInUso(DataRicerche.TRicerchePend.FieldByName('ID').AsInteger, MainForm.xIDUtenteAttuale);

     // sblocco note per tutti
     //Data.Q1.Close;
     //Data.Q1.SQL.Clear;
     //Data.Q1.SQL.Text := 'Update EBC_CandidatiRicerche set Bloccato = 0 where IDRicerca = ' + DataRicerche.TRicerchePend.FieldByName('ID').asString;
     //Data.Q1.ExecSQL;

     if DataRicerche.QValutazCand.Active then DataRicerche.QValutazCand.Close;

     if DataRicerche.QAttivita.Active then DataRicerche.QAttivita.Close;
     if DataRicerche.QCandidatiLK.Active then DataRicerche.QCandidatiLK.Close;
     if DataRicerche.QUtentiRicLK.Active then DataRicerche.QUtentiRicLK.Close;

end;

procedure TSelPersForm.RiprendiAttivita;
begin
     // --> azzerare il contatore e ripartire
     TimerInattivo.Enabled := False;
     TimerInattivo.Interval := 0;
     TimerInattivo.Enabled := True;
     TimerInattivo.Enabled := False;
     TimerInattivo.Interval := 120000;
     TimerInattivo.Enabled := True;
     xAttivo := True;
end;

//[TONI20021002]DEBUGOK

procedure TSelPersForm.BStatoRicClick(Sender: TObject);
var SavePlace: TBookmark;
     xIDRic, xIDCandInserito, xIDUltimoEvStorico: integer;
     i, k, xIDEventoRic: integer;
     xNonInviati, s: string;
     xDataUltimoEvento: TDateTime;

begin
     if not Mainform.CheckProfile('23') then Exit;
     xIDRic := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
     ModStatoRicForm := TModStatoRicForm.create(self);
     ModStatoRicForm.DEDallaData.Date := Date;
     if DataRicerche.TRicerchePend.FieldByName('DataFine').AsString <> '' then
          ModStatoRicForm.DEDataFine.Date := DataRicerche.TRicerchePend.FieldByName('DataFine').AsDateTime;
     ModStatoRicForm.ShowModal;
     if ModStatoRicForm.ModalResult = mrOK then begin
          Data.DB.BeginTrans;
          try
               if data.Global.FieldByName('debughrms').asBoolean = true then showmessage('a');
               Data.Q1.Close;
               Data.Q1.SQL.text := 'update EBC_Ricerche set IDStatoRic=:xIDStatoRic:,DallaData=:xDallaData:';
               if ModStatoRicForm.DEDataFine.text <> '' then
                    Data.Q1.SQL.Add(',DataFine=:xDataFine:');
               Data.Q1.SQL.Add('where ID=' + DataRicerche.TRicerchePend.FieldByName('ID').asString);
               Data.Q1.ParamByName['xIDStatoRic'] := ModStatoRicForm.TStatiRic.FieldByName('ID').AsInteger;
               if data.Global.FieldByName('debughrms').asBoolean = true then showmessage('b');
               Data.Q1.ParamByName['xDallaData'] := ModStatoRicForm.DEDallaData.Date;
               if data.Global.FieldByName('debughrms').asBoolean = true then showmessage('c');
               if ModStatoRicForm.DEDataFine.text <> '' then
                    Data.Q1.ParamByName['xDataFine'] := ModStatoRicForm.DEDataFine.Date;
               Data.Q1.ExecSQL;
               // data ultimo evento
               Data.Q1.Close;
               Data.Q1.SQL.text := 'select ID,DallaData from EBC_StoricoRic ' +
                    'where IDRicerca=:xIDRicerca: order by DallaData desc';
               Data.Q1.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
               Data.Q1.Open;
               if not Data.Q1.IsEmpty then begin
                    xIDUltimoEvStorico := Data.Q1.FieldByName('ID').asInteger;
                    if data.Global.FieldByName('debughrms').asBoolean = true then showmessage('d');
                    s := FormatDateTime('dd/mm/yyyy', Data.Q1.FieldByName('DallaData').asDateTime);
                    xDataUltimoEvento := StrToDate(s);
                    if data.Global.FieldByName('debughrms').asBoolean = true then showmessage('e');
               end else begin
                    xIDUltimoEvStorico := 0;
                    xDataUltimoEvento := 0;
               end;
               // casistica evento
               xIDEventoRic := 0;
               case ModStatoRicForm.TStatiRic.FieldByName('ID').AsInteger of
                    2: xIDEventoRic := 2;
                    5: xIDEventoRic := 3;
                    1: if DataRicerche.TRicerchePendIDStatoRic.Value <> 1 then xIDEventoRic := 4;
               end;
               // nuova riga nello storico
               if data.Global.FieldByName('debughrms').asBoolean = true then showmessage('f');
               Data.Q1.Close;
               Data.Q1.SQL.text := 'Insert into EBC_StoricoRic (IDRicerca,IDStatoRic,DallaData,Note,IDEventoRic,IDStatoSecondario) ' +
                    'values (:xIDRicerca:,:xIDStatoRic:,:xDallaData:,:xNote:,:xIDEventoRic:,:xIDStatoSecondario:)';
               Data.Q1.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
               Data.Q1.ParamByName['xIDStatoRic'] := ModStatoRicForm.TStatiRic.FieldByName('ID').AsInteger;
               Data.Q1.ParamByName['xDallaData'] := ModStatoRicForm.DEDallaData.Date;
               Data.Q1.ParamByName['xNote'] := ModStatoRicForm.ENote.Text;
               Data.Q1.ParamByName['xIDEventoRic'] := xIDEventoRic;
               Data.Q1.ParamByName['xIDStatoSecondario'] := DataRicerche.TRicerchePend.FieldByName('IDStatoSecondario').AsInteger;
               if data.Global.FieldByName('debughrms').asBoolean = true then showmessage('g');
               Data.Q1.ExecSQL;
               if data.Global.FieldByName('debughrms').asBoolean = true then showmessage('h');
               // aggiornamento penultimo (ex ultimo) evento
               if xIDUltimoEvStorico > 0 then begin
                    Data.Q1.Close;
                    Data.Q1.SQL.text := 'update EBC_StoricoRic set TotGiorni=:xTotGiorni: ' +
                         'where ID=' + IntToStr(xIDUltimoEvStorico);
                    Data.Q1.ParamByName['xTotGiorni'] := xDataUltimoEvento - ModStatoRicForm.DEDallaData.Date;
                    Data.Q1.ExecSQL;
               end;

               Data.DB.CommitTrans;
               if data.Global.FieldByName('debughrms').asBoolean = true then showmessage('i');

          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE:  operazione non completata', mtError, [mbOK], 0);
               raise;
          end;
     end;
     if data.Global.FieldByName('debughrms').asBoolean = true then showmessage('l');
     CaricaApriRicPend(xIDRic);
     if data.Global.FieldByName('debughrms').asBoolean = true then showmessage('m');
     // sistema di "sgancio" se la ricerca � chiusa
     if {(ModStatoRicForm.TStatiRic.FieldByName('StatoRic').AsString = 'conclusa') or
     (ModStatoRicForm.TStatiRic.FieldByName('StatoRic').AsString = 'sospesa') or
     (ModStatoRicForm.TStatiRic.FieldByName('StatoRic').AsString = 'chiusa') or
     (ModStatoRicForm.TStatiRic.FieldByName('StatoRic').AsString = 'cancellata') or
     (ModStatoRicForm.TStatiRic.FieldByName('StatoRic').AsString = 'abbandonata') or
     //(ModStatoRicForm.TStatiRic.FieldByName('StatoRic').AsString = 'in chiusura') or
(ModStatoRicForm.TStatiRic.FieldByName('StatoRic').AsString = 'eliminata') }ModStatoRicForm.TStatiRic.FieldByName('CheckSgancio').asBoolean = True then begin
          if MessageDlg('Vuoi procedere con il sistema automatico di sgancio ?', mtInformation, [mbYes, mbNo], 0) = mrYes then begin
               Data.Q4.Close;
               Data.Q4.SQL.text := 'select Anagrafica.ID, Anagrafica.IDTipoStato, Anagrafica.Email, EBC_CandidatiRicerche.* ' +
                    'from EBC_CandidatiRicerche, Anagrafica ' +
                    'where EBC_CandidatiRicerche.IDAnagrafica=Anagrafica.ID ' +
                    'and IDRicerca=' + DataRicerche.TRicerchePend.FieldByName('ID').AsString;
               Data.Q4.Open;
               if not Data.Q4.Locate('IDTipoStato', 3, []) then begin
                    if MessageDlg('ATTENZIONE: non risultano candidati inseriti ! - Vuoi procedere ?', mtWarning, [mbYes, mbNo], 0) = mrNo then begin
                         Data.Q4.Close;
                         Exit;
                    end;
               end else xIDCandInserito := Data.Q4.FieldByName('ID').asInteger;
               if data.Global.FieldByName('debughrms').asBoolean = true then showmessage('n');
               // aggiornamento candidati
               Data.Q4.First;
               while not Data.Q4.EOF do begin
                    if (Data.Q4.FieldByName('IDTipoStato').AsInteger <> 3) and (Data.Q4.FieldByName('Escluso').asBoolean = False) then begin
                         Data.QTemp.Close;
                         Data.QTemp.SQL.Clear;
                         Data.QTemp.SQL.Add('update EBC_CandidatiRicerche set Escluso=1,');
                         Data.QTemp.SQL.Add('Stato=''eliminato (commessa/progetto ' + ModStatoRicForm.TStatiRic.FieldByName('StatoRic').AsString + ')'' where IDAnagrafica=' + Data.Q4.FieldByName('IDAnagrafica').asString + ' and IDRicerca=' + DataRicerche.TRicerchePend.FieldByName('ID').asString);
                         Data.QTemp.execSQL;
                         // candidati in selezione messi come esterni
                         Data.Q3.Close;
                         Data.Q3.SQL.Text := 'select * ' +
                              'from ebc_candidatiricerche ' +
                              'where escluso = 0  ' +
                              'and idanagrafica = ' + Data.Q4.FieldByName('IDAnagrafica').asString;
                         Data.Q3.Open;
                         if (Data.Q3.RecordCount = 0) and (not (Data.Q4.FieldByName('IDTipoStato').AsInteger in [6, 7, 8])) then begin
                              Data.QTemp.Close;
                              Data.QTemp.SQL.text := 'update anagrafica set IDTipoStato=2,IDStato=28 ' +
                                   'where ID=' + Data.Q4.FieldByName('IDAnagrafica').asString;
                              Data.QTemp.ExecSQL;
                         end;
                         // aggiornamento storico
                         Data.QTemp.Close;
                         Data.QTemp.SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
                              'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xIDCliente:)';
                         Data.QTemp.ParamByName['xIDAnagrafica'] := Data.Q4.FieldByName('IDAnagrafica').asInteger;
                         Data.QTemp.ParamByName['xIDEvento'] := 17;
                         Data.QTemp.ParamByName['xDataEvento'] := Date;
                         Data.QTemp.ParamByName['xAnnotazioni'] := copy('ric.' + DataRicerche.TRicerchePend.FieldByName('Progressivo').asString + ' per ' + DataRicerche.TRicerchePend.FieldByName('Cliente').asString + ' (commessa/progetto ' + ModStatoRicForm.TStatiRic.FieldByName('StatoRic').AsString + ')', 1, 256);
                         Data.QTemp.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
                         Data.QTemp.ParamByName['xIDUtente'] := MainForm.xIDUtenteAttuale;
                         Data.QTemp.ParamByName['xIDCliente'] := DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger;
                         Data.QTemp.ExecSQL;
                    end;
                    Data.Q4.Next;
               end;

               // Sgancio dei soggetti inseriti in TL
               Data.Q2.Close;
               Data.Q2.SQL.Text := 'select distinct anagrafica.id, cognome, nome, IDTipoStato, Escluso ' +
                    'from EBC_CandidatiRicerche, Anagrafica, EsperienzeLavorative ' +
                    'where EBC_CandidatiRicerche.IDAnagrafica = Anagrafica.ID ' +
                    'and Anagrafica.ID=EsperienzeLavorative.IDAnagrafica ' +
                    'and EsperienzeLavorative.IDAzienda in (select idcliente ' +
                    'from ebc_ricerchetargetlist ' +
                    'where idricerca = :xIDRicerca1:) ' +
                    'and EBC_CandidatiRicerche.idricerca = :xIDRicerca2: ' +
                    'and Escluso=0 ';
               Data.Q2.ParamByName['xIDRicerca1'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
               Data.Q2.ParamByName['xIDRicerca2'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
               Data.Q2.Open;
               while not Data.Q2.EOF do begin
                    if (Data.Q2.FieldByName('IDTipoStato').AsInteger <> 3) and (Data.Q2.FieldByName('Escluso').asBoolean = False) then begin
                         // candidati in selezione messi come esterni
                         Data.Q3.Close;
                         Data.Q3.SQL.Text := 'select * ' +
                              'from ebc_candidatiricerche ' +
                              'where escluso = 0  ' +
                              'and idanagrafica = ' + Data.Q2.FieldByName('ID').asString;
                         Data.Q3.Open;
                         if (Data.Q3.RecordCount = 1) and (not (Data.Q4.FieldByName('IDTipoStato').AsInteger in [6, 7, 8])) then begin
                              Data.QTemp.Close;
                              Data.QTemp.SQL.text := 'update anagrafica set IDTipoStato=2,IDStato=28 ' +
                                   'where ID=' + Data.Q2.FieldByName('ID').asString;
                              Data.QTemp.ExecSQL;
                         end;




                         // aggiornamento storico
                         Data.QTemp.Close;
                         Data.QTemp.SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
                              'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xIDCliente:)';
                         Data.QTemp.ParamByName['xIDAnagrafica'] := Data.Q2.FieldByName('ID').asInteger;
                         Data.QTemp.ParamByName['xIDEvento'] := 17;
                         Data.QTemp.ParamByName['xDataEvento'] := Date;
                         Data.QTemp.ParamByName['xAnnotazioni'] := copy('Target List ric.' + DataRicerche.TRicerchePend.FieldByName('Progressivo').asString + ' per ' + DataRicerche.TRicerchePend.FieldByName('Cliente').asString + ' (commessa/progetto ' + ModStatoRicForm.TStatiRic.FieldByName('StatoRic').AsString + ')', 1, 256);
                         Data.QTemp.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
                         Data.QTemp.ParamByName['xIDUtente'] := MainForm.xIDUtenteAttuale;
                         Data.QTemp.ParamByName['xIDCliente'] := DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger;
                         Data.QTemp.ExecSQL;
                    end;
                    Data.Q2.Next;
               end;

               // ### attenzione ###: si assume sempre un solo inserito !!
               FaxPresentazForm := TFaxPresentazForm.create(self);
               CBOpzioneCand.Checked := False;
               EseguiQueryCandidati;
               FaxPresentazForm.ASG1.RowCount := DataRicerche.QCandRic.RecordCount + 1;
               DataRicerche.QCandRic.First;
               i := 1;
               while not DataRicerche.QCandRic.EOF do begin
                    if DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger <> xIDCandInserito then begin
                         FaxPresentazForm.ASG1.addcheckbox(0, i, false, false);
                         FaxPresentazForm.ASG1.Cells[0, i] := DataRicerche.QCandRic.FieldByName('Cognome').AsString + ' ' + DataRicerche.QCandRic.FieldByName('Nome').AsString;
                         FaxPresentazForm.xArrayIDAnag[i] := DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger;
                         FaxPresentazForm.ASG1.Cells[3, i] := DataRicerche.QCandRic.FieldByName('StatoCliente').AsString;
                         inc(i);
                    end;
                    DataRicerche.QCandRic.Next;
               end;
               FaxPresentazForm.QClienti.ParamByName['xIDCliente'] := DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger;
               FaxPresentazForm.QClienti.open;
               FaxPresentazForm.TContattiCli.Open;
               FaxPresentazForm.xTotRighe := dataricerche.qcandric.RecordCount;
               FaxPresentazForm.ShowModal;
               SavePlace := DataRicerche.QCandRic.GetBookmark;
               DataRicerche.QCandRic.Close;
               DataRicerche.QCandRic.Open;
               DataRicerche.QCandRic.GotoBookmark(SavePlace);
               DataRicerche.QCandRic.FreeBookmark(SavePlace);
               FaxPresentazForm.Free;
               CBOpzioneCand.Checked := True;
               // procedi: per ogni soggetto non inserito in azienda, escludilo
               // VECCHIO CODICE per la procedura di scancio
               {SceltaModelloMailCliForm:=TSceltaModelloMailCliForm.create(self);
               SceltaModelloMailCliForm.CBStoricoInvio.Visible:=False;
               SceltaModelloMailCliForm.QTipiMailContattiCli.Close;
               SceltaModelloMailCliForm.QTipiMailContattiCli.SQL.text:='select * from TipiMailCand';
               SceltaModelloMailCliForm.QTipiMailContattiCli.Open;
               SceltaModelloMailCliForm.BModTesti.Visible:=True;
               SceltaModelloMailCliForm.ShowModal;
               Data.Q1.First;
               while not Data.Q1.EOF do begin
                    if (Data.Q1.FieldByName('IDTipoStato').AsInteger<>3)and(Data.Q1.FieldByName('Escluso').asBoolean=False) then begin
                         Data.QTemp.Close;
                         Data.QTemp.SQL.Clear;
                         Data.QTemp.SQL.Add('update EBC_CandidatiRicerche set Escluso=1,');
                         Data.QTemp.SQL.Add('Stato="eliminato (motivo: commessa conclusa)" where IDAnagrafica='+Data.Q1.FieldByName('IDAnagrafica').asString+' and IDRicerca='+DataRicerche.TRicerchePend.FieldByName ('ID').asString);
                         Data.QTemp.execSQL;
                         // candidati in selezione messi come esterni
                         Data.QTemp.Close;
                         Data.QTemp.SQL.text:='update anagrafica set IDTipoStato=2,IDStato=28 '+
                              'where ID='+Data.Q1.FieldByName('IDAnagrafica').asString;
                         Data.QTemp.ExecSQL;

                         // aggiornamento storico
                         Data.QTemp.Close;
                         Data.QTemp.SQL.text:='insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) '+
                              'values (:xIDAnagrafica,:xIDEvento,:xDataEvento,:xAnnotazioni,:xIDRicerca,:xIDUtente,:xIDCliente)';
                         Data.QTemp.ParamByName['xIDAnagrafica').asInteger:=Data.Q1.FieldByName('IDAnagrafica').asInteger;
                         Data.QTemp.ParamByName['xIDEvento').asInteger:=17;
                         Data.QTemp.ParamByName['xDataEvento').asDateTime:=Date;
                         Data.QTemp.ParamByName['xAnnotazioni').asString:='ric.'+DataRicerche.TRicerchePendProgressivo.asString+' per '+DataRicerche.TRicerchePend.FieldByName ('Cliente').asString+' (motivo: commessa conclusa)';
                         Data.QTemp.ParamByName['xIDRicerca').asInteger:=DataRicerche.TRicerchePend.FieldByName ('ID').Value;
                         Data.QTemp.ParamByName['xIDUtente').asInteger:=MainForm.xIDUtenteAttuale;
                         Data.QTemp.ParamByName['xIDCliente').asInteger:=DataRicerche.TRicerchePend.FieldByName ('ID')Cliente.Value;
                         Data.QTemp.ExecSQL;
                         // invio mail (se ce l'hanno...)
                         if Data.Q1.FieldByName('Email').asString<>'' then begin
                              MainForm.SpedisciMail(Data.Q1.FieldByName('Email').asString);
                              // storicizza invio mail
                              Data.QTemp.Close;
                              Data.QTemp.SQL.text:='insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) '+
                                   'values (:xIDAnagrafica,:xIDEvento,:xDataEvento,:xAnnotazioni,:xIDRicerca,:xIDUtente,:xIDCliente)';
                              Data.QTemp.ParamByName['xIDAnagrafica').asInteger:=Data.Q1.FieldByName('IDAnagrafica').asInteger;
                              Data.QTemp.ParamByName['xIDEvento').asInteger:=71;
                              Data.QTemp.ParamByName['xDataEvento').asDateTime:=Date;
                              Data.QTemp.ParamByName['xAnnotazioni').asString:=SceltaModelloMailCliForm.QTipiMailContattiCliDescrizione.Value;
                              Data.QTemp.ParamByName['xIDRicerca').asInteger:=DataRicerche.TRicerchePend.FieldByName ('ID').Value;
                              Data.QTemp.ParamByName['xIDUtente').asInteger:=MainForm.xIDUtenteAttuale;
                              Data.QTemp.ParamByName['xIDCliente').asInteger:=DataRicerche.TRicerchePend.FieldByName ('ID')Cliente.Value;
                              Data.QTemp.ExecSQL;
                         end;
                    end;
                    Data.Q1.Next;
               end;
               SceltaModelloMailCliForm.Free;}

               showMessage('Procedura di sgancio conclusa');
          end;
     end;
     ModStatoRicForm.Free;
     CaricaApriRicPend(xIDRic);
     sleep(10);
end;

//[TONI20021003]DEBUGOK

procedure TSelPersForm.BStoricoRicClick(Sender: TObject);
begin
     StoricoRicForm := TStoricoRicForm.create(self);
     DataRicerche.TStoricoRic.Parameters[0].Value := Dataricerche.TRicerchePendID.Value;
     DataRicerche.TStoricoRic.Open;
     StoricoRicForm.ShowModal;
     StoricoRicForm.Free;
     DataRicerche.TStoricoRic.Close;
end;

//[TONI20021002]DEBUGOK

procedure TSelPersForm.ToolbarButton975Click(Sender: TObject);
begin
     if not Mainform.CheckProfile('22') then Exit;
     SpecificheForm := TSpecificheForm.create(self);
     SpecificheForm.ShowModal;
     SpecificheForm.Free;
end;

//[TONI20021002]DEBUGOK

procedure TSelPersForm.BUtenteModClick(Sender: TObject);
var xIDUtente: integer;
     xIDRuoloRic: string;
begin
     ElencoUtentiForm := TElencoUtentiForm.create(self);
     ElencoUtentiForm.ShowModal;
     if ElencoUtentiForm.ModalResult = mrOK then begin
          // controllo associazione gi� esistente con la commessa
          Data.Q1.Close;
          Data.Q1.SQL.text := 'select count(ID) Tot from EBC_RicercheUtenti where IDRicerca=:xIDRicerca: and IDUtente=' + ElencoUtentiForm.QUsers.fieldByName('ID').asString;
          Data.Q1.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
          Data.Q1.Open;
          if Data.Q1.FieldByname('Tot').asInteger > 0 then begin
               Data.Q1.Close;
               MessageDlg('L''utente � gi� associato alla commessa/progetto: impossibile continuare', mtError, [mbOK], 0);
               ElencoUtentiForm.Free;
               Exit;
          end;
          Data.Q1.Close;
          xIDUtente := DataRicerche.TRicerchePend.FieldByName('IDUtente').AsInteger;
          // EBC_Ricerche.IDUtente
          //dataricerche.tricerchepend.SQL.SaveToFile('TricerchePend.sql');
          DataRicerche.TRicerchePend.Edit;
          DataRicerche.TRicerchePend.FieldByName('IDUtente').AsInteger := ElencoUtentiForm.QUsers.FieldByName('ID').AsInteger;
          with DataRicerche.TRicerchePend do begin
               Data.DB.BeginTrans;
               try
                    //                    ApplyUpdates;
                    Post;
                    // inserimento in EBC_RicercheUtenti
                    Data.QTemp.Close;
                    Data.QTemp.SQL.text := 'Select * from ebc_ricercheutenti where idutente =' + ElencoUtentiForm.QUsers.FieldByName('ID').AsString +
                         ' and idricerca=' + DataRicerche.TRicerchePend.FieldByName('ID').asString;
                    Data.QTemp.Open;

                    if Data.Qtemp.RecordCount = 0 then begin

                         Data.QTemp.Close;
                         Data.QTemp.SQL.text := 'insert into ebc_ricercheutenti (IDUtente,IDRicerca,IDRuoloRic) ' +
                              'Values(:xIDUtente:,:xIDRicerca:,:xIDRuoloRic:)';
                         Data.QTemp.ParamByName['xIDUtente'] := ElencoUtentiForm.QUsers.FieldByName('ID').AsInteger;
                         Data.QTemp.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
                         Data.QTemp.ParamByName['xIDRuoloRic'] := 1;
                    end else begin
                         Data.QTemp.Close;
                         Data.QTemp.SQL.text := 'update EBC_RicercheUtenti set IDUtente=:xIDUtenteNew: ' +
                              'where and IDRicerca=:xIDRicerca: and IDRuoloRic=1';
                         Data.QTemp.ParamByName['xIDUtenteNew'] := ElencoUtentiForm.QUsers.FieldByName('ID').AsInteger;
                         // Data.QTemp.ParamByName['xIDUtente'] := xIDUtente;
                         Data.QTemp.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;

                    end;

                    Data.QTemp.ExecSQL;
                    if xIDUtente <> DataRicerche.TRicerchePend.FieldByName('IDUtente').AsInteger then begin
                         if MessageDlg('Vuoi mantenere l''utente all''interno della commessa?', mtInformation, [mbYes, mbNo], 0) = mrYes then begin

                              xIDRuoloRic := OpenSelFromQuery(' select * from EBC_RicUtentiRuoli where id <>1 ',
                                   'ID', 'RuoloRic', 'Ruolo commessa/progetto', '', False);

                              if xIDRuoloRic = '' then begin
                                   // ElencoUtentiForm.Free;
                                   showmessage('operazione annullata');
                                   exit;
                              end;
                              data.qtemp.close;
                              data.QTemp.sql.text := 'select * from ebc_ricercheutenti where idutente=' + inttostr(xIDUtente)
                                   + ' and idricerca=' + DataRicerche.TRicerchePend.FieldByName('ID').asString
                                   + ' and IDRuoloRic=' + xIDRuoloRic;
                              data.qtemp.Open;
                              if data.qtemp.RecordCount = 0 then begin
                                   Data.QTemp.Close;
                                   Data.QTemp.SQL.text := 'update ebc_ricercheutenti set IDRuoloRic=:xIDRuoloRic: ' +
                                        'Where idutente=:xIDUtente: and idricerca = :xIDRicerca:';
                                   Data.QTemp.ParamByName['xIDRuoloRic'] := strtoint(xidruoloric);
                                   Data.QTemp.ParamByName['xIDUtente'] := xIDUtente;
                                   Data.QTemp.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;

                                   Data.QTemp.ExecSQL;
                              end;

                              // ElencoUtentiForm.Free;
                         end else begin
                              Data.QTemp.Close;
                              Data.QTemp.SQL.text := 'delete from ebc_ricercheutenti ' +
                                   'Where idutente=:xIDUtente: and idricerca = :xIDRicerca:';
                              Data.QTemp.ParamByName['xIDUtente'] := xIDUtente;
                              Data.QTemp.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;

                              Data.QTemp.ExecSQL;
                         end;
                    end;
                    Data.DB.CommitTrans;
                    CaricaApriRicPend(DataRicerche.TRicerchePend.FieldByName('ID').AsInteger);
                    DataRicerche.QRicUtenti.Close;
                    DataRicerche.QRicUtenti.Open;

               except
                    Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                    raise;
               end;
               //               CommitUpdates;
          end;
     end;
     ElencoUtentiForm.Free;
end;

procedure TSelPersForm.SpeedButton3Click(Sender: TObject);
begin
     if not Mainform.CheckProfile('214') then Exit;
     LegendaGestRicForm := TLegendaGestRicForm.create(self);
     LegendaGestRicForm.ShowModal;
     LegendaGestRicForm.Free;
     Q.SQl.text := 'select ggDiffDataIns,ggDiffDataUltimoContatto,CheckRicCandCV,CheckRicCandTel from global ';
     Q.Open;
     xggDiffDataIns := Q.FieldByname('ggDiffDataIns').asInteger;
     xggDiffDataUltimoContatto := Q.FieldByname('ggDiffDataUltimoContatto').asInteger;
     xCheckRicCandCV := Q.FieldByname('CheckRicCandCV').AsBoolean;
     xCheckRicCandTel := Q.FieldByname('CheckRicCandTel').AsBoolean;
     Q.Close;
     EseguiQueryCandidati;
end;

//[TONI20020210]DEBUG

procedure TSelPersForm.BTipoClick(Sender: TObject);
begin
     Caption := '[S/13] - ' + Caption;
     if MessageDlg('ATTENZIONE: stai cambiando il TIPO !! - Continuare ?', mtWarning, [mbYes, mbNo], 0) = mrYes then begin
          SelTipoCommessaForm := TSelTipoCommessaForm.create(self);
          if DataRicerche.TRicerchePend.FieldByName('Tipo').AsString <> '' then
          begin
               if not SelTipoCommessaForm.QTipo.Active then SelTipoCommessaForm.QTipo.Open;
               SelTipoCommessaForm.QTipo.Locate('TipoCommessa', DataRicerche.TRicerchePend.FieldByName('Tipo').AsString, [locaseinsensitive]);
          end;
          SelTipoCommessaForm.ShowModal;
          if SelTipoCommessaForm.ModalResult = mrOK then begin
               DataRicerche.TRicerchePend.Edit;
               DataRicerche.TRicerchePend.FieldByName('Tipo').AsString := SelTipoCommessaForm.QTipo.FieldByName('TipoCommessa').AsString;
               with DataRicerche.TRicerchePend do begin
                    Data.DB.BeginTrans;
                    try
                         //                         ApplyUpdates;
                         post;
                         Data.DB.CommitTrans;
                         CaricaApriRicPend(DataRicerche.QRicAttive.FieldByName('ID').AsInteger);
                    except
                         Data.DB.RollbackTrans;
                         MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                         raise;
                    end;
                    //                    CommitUpdates;
               end;
          end;
          SelTipoCommessaForm.Free;
     end;
end;

//[TONI20021002]DEBUGOK

procedure TSelPersForm.PCSelPersChange(Sender: TObject);
begin
     try
          if DataRicerche.TRicerchePend.Active = FALSE then DataRicerche.TRicerchePend.Open;
          //if DataRicerche.TRicerchePend.Active = True then DataRicerche.TRicerchePend.Close;
          //DataRicerche.TRicerchePend.Open;
          if DataRicerche.QTLCandidati.State = dsEdit then DataRicerche.QTLCandidati.Post;
          if DataRicerche.TRicerchePend.FieldByName('ggDiffApRic').AsString = '' then begin
               if not (DataRicerche.dsRicerchePend.State = dsEdit) then DataRicerche.TRicerchePend.Edit;
               DataRicerche.TRicerchePend.FieldByName('ggDiffApRic').AsInteger := 0;
          end;
          if DataRicerche.TRicerchePend.FieldByName('ggDiffUcRic').AsString = '' then begin
               if not (DataRicerche.dsRicerchePend.State = dsEdit) then DataRicerche.TRicerchePend.Edit;
               DataRicerche.TRicerchePend.FieldByName('ggDiffUcRic').AsInteger := 0;
          end;
          if DataRicerche.TRicerchePend.FieldByName('ggDiffColRic').AsString = '' then begin
               if not (DataRicerche.dsRicerchePend.State = dsEdit) then DataRicerche.TRicerchePend.Edit;
               DataRicerche.TRicerchePend.FieldByName('ggDiffColRic').AsInteger := 0;
          end;

          // se  esiste il file calcoldadistanza.html
          //decide per quale tab scheet visualizzarlo
          if FileExists('CalcolaDistanza.html') then begin
               if PCSelPers.ActivePage = TSCandidati then
                    BCalcolaDistanza.visible := true else
                    BCalcolaDistanza.Visible := false;
          end else begin
               BCalcolaDistanza.Visible := false;
               dxDBGrid1Distanza.Visible := false;
               dxDBGrid1Tempo.Visible := false;
          end;

          if PCSelPers.ActivePage = TSCandAnnunci then begin

               AggiornaPesiAnnunci(xIDRicercaInt);
               DataRicerche.QAnnunciCand.Close;
               //DataRicerche.QAnnunciCand.ReloadSQL;
               //DataRicerche.QAnnunciCand.ParamByName['xIDRicerca1'] := DataRicerche.TRicerchePend.FieldByname('ID').asString;
               //DataRicerche.QAnnunciCand.ParamByName['xIDRicerca2'] := DataRicerche.TRicerchePend.FieldByname('ID').asString;

               {
               DataRicerche.QAnnunciCand.Parameters[0].Value := DataRicerche.QRicAttiveID.Value;
               DataRicerche.QAnnunciCand.Parameters[1].Value := DataRicerche.QRicAttiveID.Value;
               }
               DataRicerche.QAnnunciCand.Parameters[0].Value := MainForm.xIDUtenteAttuale;
               DataRicerche.QAnnunciCand.Parameters[1].Value := xIDRicercaInt;
               DataRicerche.QAnnunciCand.Open;

               {DataRicerche.QAnnunciCand.Close;
               DataRicerche.QAnnunciCand.Open;}

               LTotCandAnn.caption := IntToStr(DataRicerche.QAnnunciCand.Recordcount);
          end else DataRicerche.QAnnunciCand.Close;

          // Situazione candidati
          if PCSelPers.ActivePage = TSCliCand then begin
               CliCandidatiFrame1.xIDCliente := DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger;
               CliCandidatiFrame1.xIDRicerca := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
               CliCandidatiFrame1.CreaAlbero;
          end;

          // altri dati
          if PCSelPers.ActivePage = TSAltriDati then begin
               // utenti associati alla ricerca
               DataRicerche.QRicUtenti.Close;
               //          DataRicerche.QRicUtenti.ReloadSQL;
               DataRicerche.QRicUtenti.Open;
               DataRicerche.QCommittentiCommesse.Close;
               DataRicerche.QCommittentiCommesse.open;
               // rif. offerta
               if DataRicerche.TRicerchePend.fieldByName('IDOfferta').AsString <> '' then begin
                    Data.Q1.Close;
                    Data.Q1.SQL.text := 'select Rif from EBC_Offerte where ID=' + DataRicerche.TRicerchePend.FieldByName('IDOfferta').AsString;
                    Data.Q1.Open;
                    if not Data.Q1.IsEmpty then
                         ERifOfferta.Text := Data.Q1.FieldByName('Rif').asString
                    else ERifOfferta.Text := ' --- ';
                    Data.Q1.Close;
               end;
          end else DataRicerche.QRicUtenti.Close;
          // parametri
          if PCSelPers.ActivePage = TSRicParams then begin
               DataRicerche.QRicLineeQuery.Open;
          end else begin
               DataRicerche.QRicLineeQuery.Close;
          end;

          // time-sheet
          if PCSelPers.ActivePage = TSTimeSheet then begin
               DataRicerche.QUsersLK.Close;
               DataRicerche.QUsersLK.SQL.Text := 'select ID,Nominativo,Descrizione,IDGruppoProfess from Users';
               DataRicerche.QUsersLK.Open;
               DataRicerche.QRicTimeSheet.Close;
               DataRicerche.QRicTimeSheet.SQl.text := 'select * from EBC_RicercheTimeSheet where IDRicerca=' + DataRicerche.TRicerchePendID.AsString;
               DataRicerche.QRicTimeSheet.Open;
               DataRicerche.QTimeRepDett.Open;
          end else begin
               DataRicerche.QRicTimeSheet.Close;
               DataRicerche.QUsersLK.Close;
               DataRicerche.QTimeRepDett.Close;
          end;

          // Gestione Qualit�
          if PCSelPers.ActivePage = TSGestQ then begin
               DataRicerche.QCommessaCL.Open;
               DataRicerche.QCLStat.Open;
          end else begin
               DataRicerche.QCommessaCL.Close;
               DataRicerche.QCLStat.Close;
          end;

          //CAMPI PERSONALIZZATI
          if PCSelPers.ActivePage = TSComCampPers then begin
               if (not mainform.CheckProfile('1022', false)) then begin //ALE 03012003
                    PCSelPers.ActivePage := TSFile;
                    Exit;
               end;
               if assigned(SelPersForm) then begin
                    CampiPersFrame1.xTipo := 'Commesse';
                    CampiPersFrame1.CaricaValori;
                    CampiPersFrame1.ImpostaCampi;
               end;
          end else begin
               CampiPersFrame1.QCampiPers.Close;
          end;

          // target list
          if PCSelPers.ActivePage = TSTargetList then begin
               DataRicerche.QRicTargetList.Close;
               DataRicerche.QRicTargetList.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePendID.Value;
               DataRicerche.QRicTargetList.Open;
               //DataRicerche.QTLCandidati.Open;
               {DataRicerche.QTLCandidati.Close;
               DataRicerche.QTLCandidati.ParamByName['xIDTargetList'] := DataRicerche.QRicTargetListID.Value;
               DataRicerche.QTLCandidati.ParamByName['xIDAzienda'] := DataRicerche.QRicTargetListIDCliente.Value;
               DataRicerche.QTLCandidati.Open;}
          end else begin
               DataRicerche.QRicTargetList.Close;
               //DataRicerche.QTLCandidati.Close;
          end;

          // file
          if PCSelPers.ActivePage = TSFile then begin
               QRicFile.Open;

          end else begin
               QRicFile.Close;
          end;

          // valutazione candidati
          if PCSelPers.ActivePage = TSValutaz then begin
               DataRicerche.QValutazCand.Close;
               DataRicerche.QValutazCand.Open;
          end else begin
               DataRicerche.QValutazCand.Close;
          end;

          // Quetionari
          if PCSelPers.ActivePage = TSQuestionari then begin
               DataRicerche.QRicQuest.Open;
               DataRicerche.QRicQuest_Anag.Open;
               DataRicerche.QRicQuest_Risp.Open;
          end else begin
               DataRicerche.QRicQuest.Close;
               DataRicerche.QRicQuest_Risp.Close;
          end;

          // attivit�
          if PCSelPers.ActivePage = TSAttivita then begin
               DataRicerche.QAttivita.Open;
               DataRicerche.QCandidatiLK.Open;
               DataRicerche.QUtentiRicLK.Open;
          end else begin
               DataRicerche.QAttivita.Close;
               DataRicerche.QCandidatiLK.Close;
               DataRicerche.QUtentiRicLK.Close;
          end;

     except
          raise;
     end;
end;

procedure TSelPersForm.BInfoCand2Click(Sender: TObject);
var xpos: integer;
begin
     if not Mainform.CheckProfile('00') then Exit;
     if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 5)) = 'ERGON' then begin
          SchedaSinteticaForm := TSchedaSinteticaForm.create(self);
          SchedaSinteticaForm.xIDAnag := DataRicerche.QAnnunciCand.FieldByName('IDAnagrafica').AsInteger;
          SchedaSinteticaForm.ShowModal;
          SchedaSinteticaForm.Free;
     end else begin
          SchedaCandForm := TSchedaCandForm.create(self);
          if not PosizionaAnag(DataRicerche.QAnnunciCand.FieldByName('IDAnagrafica').AsInteger) then exit;
          SchedaCandForm.ShowModal;
          SchedaCandForm.Free;
          //if pos('SELTIS', Data.Global.FieldByName('NomeAzienda').AsString) > 0 then
        //  xpos := GetScrollPos(DBGAnnunciCand.Handle, SB_VERT);
          CheckVisionati(DataRicerche.QAnnunciCand.FieldByName('IDAnagrafica').AsInteger, DataRicerche.QAnnunciCand.FieldByName('IDAnnEdizData').AsInteger, true);
          ///  SetScrollPos(DBGAnnunciCand.Handle, SB_VERT, xpos, True);

          RiprendiAttivita;
     end;
end;

procedure TSelPersForm.BCurriculum1Click(Sender: TObject);
var x: string;
     i: integer;
begin
     if not Mainform.CheckProfile('051') then Exit;
     if not DataRicerche.QAnnunciCand.Eof then begin
          PosizionaAnag(DataRicerche.QAnnunciCand.FieldByName('IDAnagrafica').AsInteger);
          CurriculumForm.ShowModal;

          MainForm.Pagecontrol5.ActivePage := MainForm.TSStatoTutti;
          if pos('SELTIS', Data.Global.FieldByName('NomeAzienda').AsString) > 0 then
               CheckVisionati(DataRicerche.QAnnunciCand.FieldByName('IDAnagrafica').AsInteger, DataRicerche.QAnnunciCand.FieldByName('IDAnnEdizData').AsInteger, true);
          RiprendiAttivita;
     end;
end;

procedure TSelPersForm.ToolbarButton977Click(Sender: TObject);
var xFile: string;
     xProcedi: boolean;
begin
     ApriCV(DataRicerche.QAnnunciCand.FieldByName('IDAnagrafica').AsInteger);
     if (pos('SELTIS', Data.Global.FieldByName('NomeAzienda').AsString) > 0) and (xCheckVisionato = true) then
          CheckVisionati(DataRicerche.QAnnunciCand.FieldByName('IDAnagrafica').AsInteger, DataRicerche.QAnnunciCand.FieldByName('IDAnnEdizData').AsInteger, false);
end;

procedure TSelPersForm.ToolbarButton979Click(Sender: TObject);
begin
     if not Mainform.CheckProfile('054') then Exit;
     ApriFileWord(DataRicerche.QAnnunciCand.fieldByName('CVNumero').asString);
     RiprendiAttivita;
end;

//[TONI20021003]DEBUGOK

procedure TSelPersForm.ToolbarButton971Click(Sender: TObject);
var xMiniVal: string;
begin
     if DataRicerche.QAnnunciCand.IsEmpty then exit;
     if pos('INTERMEDIA', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) > 0 then begin

          DataRicerche.QTemp.Close;
          DataRicerche.Qtemp.SQL.Text := 'select count(*) tot ' +
               ' from esperienzelavorative el' +
               ' where el.idanagrafica=' + DataRicerche.QAnnunciCand.FieldByName('IDAnagrafica').asString + ' and el.attuale=1 ' +
               ' and el.AziendaNome is not NULL and el.AziendaNome <> '''' ' +
               ' and el.IDMansione is not NULL and el.IDMansione <> 0 ';
          DataRicerche.Qtemp.Open;
          if DataRicerche.Qtemp.FieldByName('tot').asinteger <= 0 then begin
               MessageDlg('Inserire l''esperienza Lavorativa Attuale del candidato per proseguire (con Azienda e Ruolo)', mtError, [mbOk], 0);
               exit;
          end;

          DataRicerche.QCkRegoleClienti.Close;
          DataRicerche.QCkRegoleClienti.SQL.Text := 'select c.id idcliente,el.attuale,el.idanagrafica,el.aziendanome,c.descrizione,c.idregolacliente,trc.descrizione regola, trc.flag FlagRegola ' +
               ' from esperienzelavorative el' +
               ' join ebc_clienti c on c.id=el.idazienda join tabregolecliente trc on trc.id=c.idregolacliente' +
               ' where el.idanagrafica=' + DataRicerche.QAnnunciCand.FieldByName('IDAnagrafica').asString + ' and el.attuale=1';
          DataRicerche.QCkRegoleClienti.Open;
          if DataRicerche.QCkRegoleClienti.RecordCount > 0 then
               if DataRicerche.QCkRegoleClienti.FieldByName('FlagRegola').asboolean then
                    if MessageDlg('Limite per questo soggetto: ' + DataRicerche.QCkRegoleClienti.fieldByName('Regola').asstring + #10#13 + 'Vuoi Proseguire ?', mtWarning, [mbYes, mbNo], 0) = mrNO then
                         exit;
     end;

     // appartenenza a questa ricerca
     Data.QTemp.Close;
     Data.QTemp.SQL.text := 'select count(*) Tot from EBC_CandidatiRicerche ' +
          'where IDRicerca=:xIDRicerca: and IDAnagrafica=:xIDAnagrafica:';
     Data.QTemp.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
     Data.QTemp.ParamByName['xIDAnagrafica'] := DataRicerche.QAnnunciCand.FieldByName('IDAnagrafica').AsInteger;
     Data.QTemp.Open;
     if Data.QTemp.FieldByName('Tot').asInteger > 0 then begin
          MessageDlg('ATTENZIONE: il CV � gi� associato a questa ricerca.' + chr(13) +
               'IMPOSSIBILE proseguire', mtError, [mbOK], 0);
          Data.QTemp.Close;
          exit;
     end;
     Data.QTemp.Close;

     // appartenenza ad un'altra ricerca
     Data.QTemp.Close;
     Data.QTemp.SQL.text := 'select Progressivo,EBC_Clienti.descrizione Cliente ' +
          'from EBC_CandidatiRicerche,EBC_Ricerche,EBC_Clienti ' +
          'where IDRicerca<>:xIDRicerca: and IDAnagrafica=:xIDAnagrafica: ' +
          'and EBC_CandidatiRicerche.IDRicerca=EBC_Ricerche.ID ' +
          'and EBC_Ricerche.IDCliente=EBC_Clienti.ID ' +
          'and (EBC_CandidatiRicerche.Escluso=0 or EBC_CandidatiRicerche.Escluso is null)';
     Data.QTemp.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
     Data.QTemp.ParamByName['xIDAnagrafica'] := DataRicerche.QAnnunciCand.FieldByName('IDAnagrafica').AsInteger;
     Data.QTemp.Open;
     if not (Data.QTemp.IsEmpty) then
          if MessageDlg('ATTENZIONE: il CV � associato almeno alla ricerca n� ' +
               Data.QTemp.FieldByName('Progressivo').asString + ' (' + Data.QTemp.FieldByName('Cliente').asString + ')' + chr(13) +
               'Vuoi proseguire lo stesso ?', mtWarning, [mbYes, mbNo], 0) = mrNo then begin
               Data.QTemp.Close;
               RiprendiAttivita;
               exit;
          end;
     Data.QTemp.Close;
     // propriet� CV
     if (not ((DataRicerche.QAnnunciCand.FieldByName('IDProprietaCV').asString = '') or (DataRicerche.QAnnunciCand.FieldByName('IDProprietaCV').AsInteger = 0))) and
          (DataRicerche.QAnnunciCand.FieldByName('IDProprietaCV').value <> DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger) then
          if MessageDlg('ATTENZIONE: il CV risulta di propriet� di un altro cliente .' + chr(13) +
               'Vuoi proseguire lo stesso ?', mtWarning, [mbYes, mbNo], 0) = mrNo then begin
               RiprendiAttivita;
               exit;
          end;

     xMiniVal := '';
     if not InputQuery('Inserimento nella ricerca', 'Valutazione (1 lettera):', xMiniVal) then exit;

     Data.DB.BeginTrans;
     try
          if Q.Active then Q.Close;
          Q.SQL.text := 'insert into EBC_CandidatiRicerche (IDRicerca,IDAnagrafica,Escluso,Stato,DataIns,MiniVal) ' +
               'values (:xIDRicerca:,:xIDAnagrafica:,:xEscluso:,:xStato:,:xDataIns:,:xMiniVal:)';
          Q.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
          Q.ParamByName['xIDAnagrafica'] := DataRicerche.QAnnunciCand.FieldByName('IDAnagrafica').AsInteger;
          Q.ParamByName['xEscluso'] := 0;
          Q.ParamByName['xStato'] := 'inserito direttamente da annunci';
          Q.ParamByName['xDataIns'] := Date;
          Q.ParamByName['xMiniVal'] := xMiniVal;
          Q.ExecSQL;
          // aggiornamento stato anagrafica
          if Q.Active then Q.Close;
          if pos('EMERGENCY', UpperCase(data.Global.fieldbyname('NomeAzienda').asString)) > 0 then
               Q.SQL.text := 'update Anagrafica set IDStato=28,IDTipoStato=2 where ID=''' + DataRicerche.QAnnunciCand.FieldByName('IDAnagrafica').asString + ''''
          else Q.SQL.text := 'update Anagrafica set IDStato=27,IDTipoStato=1 where ID=''' + DataRicerche.QAnnunciCand.FieldByName('IDAnagrafica').asString + '''';
          Q.ExecSQL;
          // aggiornamento storico
          if Q.Active then Q.Close;
          Q.SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
               'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xIDCliente:)';
          Q.ParamByName['xIDAnagrafica'] := DataRicerche.QAnnunciCand.FieldByName('IDAnagrafica').AsInteger; ;
          Q.ParamByName['xIDEvento'] := 19;
          Q.ParamByName['xDataEvento'] := date;
          Q.ParamByName['xAnnotazioni'] := 'ric.n�' + Trimright(DataRicerche.TRicerchePend.FieldByName('Progressivo').AsString) + ' (' + TrimRight(DataRicerche.TRicerchePend.FieldByName('Cliente').AsString) + ')';
          Q.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
          Q.ParamByName['xIDUtente'] := MainForm.xIDUtenteAttuale;
          Q.ParamByName['xIDCliente'] := DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger;
          Q.ExecSQL;
          Data.DB.CommitTrans;

          DataRicerche.QAnnunciCand.Close;
          DataRicerche.QAnnunciCand.Open;
     except
          Data.DB.RollbackTrans;
          MessageDlg('Errore sul database:  inserimento non avvenuto', mtError, [mbOK], 0);
     end;
     DataRicerche.QCandRic.Close;
     DataRicerche.QCandRic.Open;
     LTotCand.Caption := IntToStr(DataRicerche.QCandRic.RecordCount);
     RiprendiAttivita;
end;

procedure TSelPersForm.ToolbarButton976Click(Sender: TObject);
begin
     if DataRicerche.QAnnunciCand.RecordCount = 0 then exit;
     DettAnnuncioForm := TDettAnnuncioForm.create(self);
     DettAnnuncioForm.QAnnuncio.ReloadSQL;
     DettAnnuncioForm.QAnnuncio.ParamByName['xID'] := DataRicerche.QAnnunciCand.FieldByName('IDAnnuncio').AsInteger;
     DettAnnuncioForm.QAnnuncio.Open;
     DettAnnuncioForm.ShowModal;
     DettAnnuncioForm.Free;
end;

//[TONI20021004]DEBUGOK

procedure TSelPersForm.SpeedButton4Click(Sender: TObject);
begin
     ElencoSediForm := TElencoSediForm.create(self);
     ElencoSediForm.ShowModal;
     if ElencoSediForm.ModalResult = mrOK then begin
          DataRicerche.TRicerchePend.Edit;
          DataRicerche.TRicerchePend.FieldByName('IDSede').AsInteger := ElencoSediForm.QSedi.fieldByName('ID').Asinteger;
          with DataRicerche.TRicerchePend do begin
               Data.DB.BeginTrans;
               try
                    //                    ApplyUpdates;
                    post;
                    Data.DB.CommitTrans;
                    CaricaApriRicPend(DataRicerche.QRicAttive.FieldByName('ID').AsInteger);
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                    raise;
               end;
               //               CommitUpdates;
          end;
     end;
     ElencoSediForm.Free;
end;

//[TONI20021004]DEBUGOK

procedure TSelPersForm.SpeedButton5Click(Sender: TObject);
begin
     SelContattiForm := TSelContattiForm.create(self);
     SelContattiForm.QContatti.Close;
     SelContattiForm.QContatti.ParamByName['xIDCliente'] := DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger;
     SelContattiForm.xIDCliente := DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger;
     SelContattiForm.QContatti.Open;
     {SelContattiForm.QContatti1.close; // ALE 01/12/2002
     SelContattiForm.qcontatti1.sql.text:='select ID,contatto,telefono from ebc_contatticlienti where idcliente = '+DataRicerche.TRicerchePend.FieldByName('IDCliente').AsString;
     SelContattiForm.QContatti1.open; // ALE 01/12/2002 --- Fine}

     SelContattiForm.ShowModal;
     if SelContattiForm.ModalResult = mrOK then begin
          DataRicerche.TRicerchePend.Edit;
          DataRicerche.TRicerchePend.FieldByName('IDContattoCli').AsInteger := SelContattiForm.QContatti.FieldByName('ID').asInteger;
          with DataRicerche.TRicerchePend do begin
               Data.DB.BeginTrans;
               try
                    //                    ApplyUpdates;
                    Post;
                    Data.DB.CommitTrans;
                    CaricaApriRicPend(DataRicerche.TRicerchePendID.AsInteger); // DataRicerche.QRicAttive.FieldByName('ID').AsInteger);
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                    raise;
               end;
               //               CommitUpdates;
          end;
     end;
     SelContattiForm.Free;
end;

procedure TSelPersForm.BUtente2Click(Sender: TObject);
var xIDRic: integer;
     xIDRuoloRic: string;
begin
     xIDRic := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
     ElencoUtentiForm := TElencoUtentiForm.create(self);
     ElencoUtentiForm.ShowModal;
     if ElencoUtentiForm.ModalResult = mrOK then begin
          // controllo se � gi� associato a questa ricerca
          Data.Q1.Close;
          Data.Q1.SQL.text := 'select count(ID) Tot from EBC_RicercheUtenti where IDRicerca=:xIDRicerca: and IDUtente=' + ElencoUtentiForm.QUsers.FieldByName('ID').asString;
          Data.Q1.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
          Data.Q1.Open;
          if Data.Q1.FieldByname('Tot').asInteger > 0 then begin
               Data.Q1.Close;
               MessageDlg('L''utente � gi� associato alla commessa/progetto: impossibile continuare', mtError, [mbOK], 0);
               ElencoUtentiForm.Free;
               Exit;
          end;
          Data.Q1.Close;

          xIDRuoloRic := OpenSelFromQuery(' select * from EBC_RicUtentiRuoli where id <>1 ',
               'ID', 'RuoloRic', 'Ruolo commessa/progetto', '', False);
          if xIDRuoloRic = '' then begin
               ElencoUtentiForm.Free;
               showmessage('operazione annullata');
               exit;
          end;
          if xIDRuoloRic = '1' then begin
               MessageDlg('Impossibile inserire un altro capo-commessa/progetto: impossibile continuare', mtError, [mbOK], 0);
               ElencoUtentiForm.Free;
               showmessage('operazione annullata');
               exit;
          end;
          // procedi
          with Data do begin
               DB.BeginTrans;
               try
                    // utenti
                    Q1.SQL.text := 'insert into EBC_RicercheUtenti (IDRicerca,IDUtente,IDRuoloRic) ' +
                         'values (:xIDRicerca:,:xIDUtente:,:xIDRuoloRic:)';
                    Q1.ParamByName['xIDRicerca'] := xIDRic;
                    Q1.ParamByName['xIDUtente'] := ElencoUtentiForm.QUsers.FieldByName('ID').AsInteger;
                    Q1.ParamByName['xIDRuoloRic'] := StrToInt(xIDRuoloRic);
                    Q1.ExecSQL;
                    DB.CommitTrans;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: operazione non effettuata', mtError, [mbOK], 0);
               end;
          end;
          DataRicerche.QRicUtenti.Close;
          //       DataRicerche.QRicUtenti.ReloadSQL;
          //       DataRicerche.QRicUtenti.ParamByname ['xIDUtente']:=ElencoUtentiForm.QUsers.FieldByName('ID').AsInteger;
          DataRicerche.QRicUtenti.Open;
     end;
     ElencoUtentiForm.Free;
end;

procedure TSelPersForm.BPasswordClick(Sender: TObject);
var xPassword: string;
begin
     xPassword := '';

     // richiesta password
     InputPasswordForm.EPassword.text := 'EPassword';
     InputPasswordForm.caption := 'Inserimento password amministratore';
     InputPasswordForm.ShowModal;
     if InputPasswordForm.ModalResult = mrOK then begin
          xPassword := InputPasswordForm.EPassword.Text;
     end else exit;
     //if not InputQuery('Modifica data prevista chiusura', 'Password di amministratore', xPassword) then exit;

     Q.Close;
     Q.SQL.text := 'select Password from Users where Nominativo=''Administrator''';
     Q.Open;
     if CheckPassword(TrimRight(xPassword), TrimRight(Q.Fieldbyname('Password').asString)) then begin
          DBDataPrev.enabled := True;
          ShowMessage('� ora possibile modificare la data');
     end else begin
          MessageDlg('Password di amministratore errata', mtError, [mbOK], 0);
     end;
     Q.Close;
end;

procedure TSelPersForm.DbDateEdit971Exit(Sender: TObject);
var xPassword: string;
begin
     //showmessage('vecchia= ' + datetostr(xVecchiadataInizio) + '   nuova=' + datetostr(DbDateEdit971.Date));
    //  DbDateEdit971.PopupCalendar(self);


    //per emergency non deve fare il controllo della password devono cambiare la data della commessa senza alcun controllo
     if pos('EMERGENCY', UpperCase(data.Global.fieldbyname('NomeAzienda').asString)) < 0 then begin

          if xVecchiadataInizio <> DbDateEdit971.Date then begin
               if DataRicerche.TRicerchePend.FieldByName('IDUtente').AsInteger <> Mainform.xIDUtenteAttuale then
               begin
                    DbDateEdit971.Date := xVecchiadataInizio;
                    MessageDlg('� necessario essere titolare della commessa/progetto per modificare la data di apertura', mtError, [mbOK], 0)
               end
               else begin
                    xPassword := '';

                    // richiesta password
                    InputPasswordForm.EPassword.text := 'EPassword';
                    InputPasswordForm.caption := 'Password utente resp.';
                    InputPasswordForm.ShowModal;
                    if InputPasswordForm.ModalResult = mrOK then begin
                         xPassword := InputPasswordForm.EPassword.Text;
                    end else begin
                         DbDateEdit971.Date := xVecchiadataInizio;
                         exit;
                    end;
                    if not CheckPassword(xPassword, GetPwdUtenteResp(MainForm.xIDUtenteAttuale)) then begin
                         MessageDlg('Password errata', mtError, [mbOK], 0);
                         DbDateEdit971.Date := xVecchiadataInizio;
                    end else begin
                         dataricerche.TRicerchePend.Post;
                         xVecchiadataInizio := dataricerche.TRicerchePendDataInizio.Value;
                         SelPersForm.SetFocus;
                    end;
               end;
          end;
     end;
end;

procedure TSelPersForm.dxDBGrid1MouseUp(Sender: TObject;
     Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
     if (Button <> mbRight) or (Shift <> []) then Exit;
     TdxDBGridPopupMenuManager.Instance.ShowGridPopupMenu(Sender as TdxDBGrid);
end;

procedure TSelPersForm.dxDBGrid1Click(Sender: TObject);
begin
     RiprendiAttivita;
end;

procedure TSelPersForm.dxDBGrid1DblClick(Sender: TObject);
var xID, xIDAnag: integer;
begin
     if pos('SOLA LETTURA', UpperCase(SelPersForm.Caption)) > 0 then
          xModificabile := FALSE
     else
          xModificabile := TRUE;
     if not xModificabile then exit;

     // controllo se � bloccato
    { if DataRicerche.qCandRicBloccato.Value then begin
          MessageDlg('Il record � bloccato per una modifica da parte di un altro utente - impossibile modificare', mtError, [mbOK], 0);
          Exit;
     end else begin
          DataRicerche.QCandRic.edit;
          DataRicerche.QCandRicBloccato.Value := True;
     end;
     }
     if not MainForm.CheckProfile('217') then Exit;
     xIDAnag := DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger;
     ModDettCandRicForm := TModDettCandRicForm.create(self);
     ModDettCandRicForm.DEDataIns.Date := DataRicerche.QCandRic.FieldByName('DataIns').AsDateTime;
     ModDettCandRicForm.ESituazione.text := DataRicerche.QCandRic.FieldByName('Stato').asString;
     ModDettCandRicForm.EMiniVal.text := DataRicerche.QCandRic.FieldByName('MiniVal').AsString;
     ModDettCandRicForm.ENote.text := DataRicerche.QCandRic.FieldByName('Note').AsString;
     ModDettCandRicForm.EnoteCli.Text := DataRicerche.QCandRic.FieldByName('NoteCliente').AsString;
     ModDettCandRicForm.cbVisibileCliente.Checked := DataRicerche.QCandRic.FieldByName('VisibleCliente').AsBoolean;
     ModDettCandRicForm.CB_Visionato.Checked := DataRicerche.QCandRic.FieldByName('Visionato').AsBoolean;

     ModDettCandRicForm.xIDCandRic := DataRicerche.QCandRic.FieldByName('ID').AsInteger;
     // ### NON SI SA PERCHE', MA NON VA !!
     //ModDettCandRicForm.xIDIndicazione:=DataRicerche.QCandRic.FieldByName('IDIndicazione').AsInteger;

     if xModificabile = false then begin
          ModDettCandRicForm.DBEdit1.ReadOnly := True;
          ModDettCandRicForm.DBEdit2.ReadOnly := True;
          ModDettCandRicForm.DBEdit3.ReadOnly := True;
          ModDettCandRicForm.DEDatains.Enabled := False;
          ModDettCandRicForm.ESituazione.ReadOnly := True;
          ModDettCandRicForm.EMiniVal.ReadOnly := True;
          ModDettCandRicForm.Enote.ReadOnly := True;
          ModDettCandRicForm.EnoteCli.ReadOnly := True;
          ModDettCandRicForm.cbVisibileCliente.Enabled := false;
          ModDettCandRicForm.CB_Visionato.Enabled := false;
     end;

     ModDettCandRicForm.Showmodal;
     xID := DataRicerche.QCandRic.FieldByName('ID').AsInteger;
     if ModDettCandRicForm.Modalresult = mrOK then begin
          InsertStoricoNoteQCandRic(Mainform.xUtenteAttuale, DataRicerche.QCandRic.FieldByName('ID').asString, 'NoteBefore', Now);
          Data.DB.BeginTrans;
          try
               Data.Q1.Close;
               Data.Q1.SQL.text := 'update EBC_CandidatiRicerche set DataIns=:xDataIns:,Stato=:xStato:,MiniVal=:xMiniVal:,Note2=:xNote:,NoteCliente=:xNoteCliente:, VisibleCliente=:xVisibleCliente:, IDIndicazione=:xIDIndicazione:, Visionato=:xVisionato:  where ID=' + DataRicerche.QCandRic.FieldByName('ID').asString;
               if ModDettCandRicForm.DEDataIns.Text = '' then
                    Data.Q1.ParamByName['xDataIns'] := null
               else Data.Q1.ParamByName['xDataIns'] := ModDettCandRicForm.DEDataIns.Date;
               Data.Q1.ParamByName['xStato'] := ModDettCandRicForm.ESituazione.text;
               Data.Q1.ParamByName['xMiniVal'] := ModDettCandRicForm.EMiniVal.text;
               Data.Q1.ParamByName['xNote'] := ModDettCandRicForm.ENote.text;
               Data.Q1.ParamByName['xNoteCliente'] := ModDettCandRicForm.eNoteCli.text;
               Data.Q1.ParamByName['xVisibleCliente'] := ModDettCandRicForm.cbVisibileCliente.Checked;
               Data.Q1.ParamByName['xIDIndicazione'] := ModDettCandRicForm.QIndicazioniID.asInteger;
               Data.Q1.ParamByName['xVisionato'] := ModDettCandRicForm.CB_Visionato.Checked;
               Data.Q1.ExecSQL;
               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE:  operazione non completata', mtError, [mbOK], 0);
               raise;
          end;
          InsertStoricoNoteQCandRic(Mainform.xUtenteAttuale, DataRicerche.QCandRic.FieldByName('ID').asString, 'NoteAfter', Now);
     end else begin
          //DataRicerche.QCandRic.Post;
     end;
     //DataRicerche.QCandRic.Post;
     ModDettCandRicForm.Free;

     DataRicerche.QCandRic.Close;
     DataRicerche.QCandRic.Open;
     if pos('EMERGENCY', UpperCase(data.Global.fieldbyname('NomeAzienda').asString)) > 0 then dxDBGrid1.FullCollapse
     else dxDBGrid1.FullExpand;
     DataRicerche.QCandRic.Locate('IDAnagrafica', xIDAnag, []);
     DataRicerche.QCandRic.FieldByName('IDAnagrafica').FocusControl;
     RegistraVisioneCand(MainForm.xIDUtenteAttuale, DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger);

end;

procedure TSelPersForm.dxDBGrid1CustomDrawCell(Sender: TObject;
     ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
     AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
     var AText: string; var AColor: TColor; AFont: TFont;
     var AAlignment: TAlignment; var ADone: Boolean);
var xFile: string;
begin
     if Data.Global.fieldbyname('debughrms').VAlue <> TRue then begin
          //lasciare questo primo if, altrimenti va in errore "variant type conversion" quando si raggruppa utilizzando una colonna testuale
          if dxdbgrid1.GroupColumnCount = 0 then begin

               if (Acolumn = dxDBGrid1DataIns) and (ANode.Values[dxDBGrid1DataIns.index] < Date - xggDiffDataIns)
                    and (ANode.Values[dxDBGrid1DataIns.index] > 0) then Acolor := clYellow;
               if (Acolumn = dxDBGrid1DataUltimoContatto) and (ANode.Values[dxDBGrid1DataUltimoContatto.index] < Date - xggDiffDataUltimoContatto)
                    and (ANode.Values[dxDBGrid1DataUltimoContatto.index] > 0) then Acolor := clYellow;
               // controllo mancanza CV

               //*** Tolta in data 20/07/2010 poich� causa di un rallentamento e poich� ritenuta ormai inutile
                    {if xCheckRicCandCV then begin
                         if (Acolumn = dxDBGrid1CVNumero) then begin
                              xFile := GetCVPath + '\i' + AText + 'a.gif';
                              if not FileExists(xFile) then AFont.Color := clRed;
                         end;
                    end;                         }
               // controllo mancanza telefoni
               // if xCheckRicCandTel then begin
               //     if (Acolumn=dxDBGrid1Cognome) then begin
               //          if ANode.Values[dxDBGrid1TelUffCell.index]='' then AFont.Color:=clYellow;
               //     end;
               //end;

               if {((Acolumn = dxDBGrid1Cognome) or (Acolumn = dxDBGrid1Nome)) and }
                    (ANode.Values[dxDBGrid1Colore.index] > 0) then begin
                    if Acolumn <> dxDBGrid1Stato then
                         AColor := ANode.Values[dxDBGrid1Colore.index];
               end;

               if (pos('INTERMEDIA', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) = 0) and (pos('EMERGENCY', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) = 0) then begin

                    if ((Acolumn = dxDBGrid1Cognome) or (Acolumn = dxDBGrid1Nome)) and (CheckVisioneCand(MainForm.xIDUtenteAttuale, ANode.Values[dxDBGrid1IDAnagrafica.index]))
                         then begin
                         AColor := $00FFB66C; ;
                    end;
               end;

               {if (Acolumn = dxDBGrid1Cognome) then begin
                 if CheckVisioneCand(MainForm.xIDUtenteAttuale,ANode.Values[dxDBGrid1IDAnagrafica.index]) then begin
                   dxDBGrid1.Columns[3].Color := clGreen;
                 end;
               end;}

               // --- impostazione colore in base all'ultimo evento

               if (Acolumn = dxDBGrid1Stato) then begin
                    Q.Close;
                    Q.SQL.Text := 'select Colore from EBC_Ricerche_ColoriGriglia where IDEvento=:x and IDUtente=:y';
                    Q.Parameters[0].Value := ANode.Values[dxDBGrid1IDEvento.index];
                    Q.Parameters[1].Value := MainForm.xIDUtenteAttuale;
                    Q.Open;
                    if not Q.IsEmpty then begin
                         AFont.Color := Q.FieldByName('Colore').asInteger;

                    end;
               end;
               Q.Close;

          end;
     end;
end;

procedure TSelPersForm.Stampaelenco1Click(Sender: TObject);
begin
     if not LogEsportazioniCand(1) then exit;
     dxPrinter1.Preview(True, nil);
end;

procedure TSelPersForm.EsportainExcel1Click(Sender: TObject);
var xdata, xOra: string;
     xFileName: string;
begin
     if not LogEsportazioniCand(1) then exit;

     if POS('INTERMEDIA', Uppercase(Data.Global.FieldByName('NomeAzienda').Value)) > 0 then begin
          xData := DateToStr(Date);
          xOra := TimeToStr(Time);

          if Data.Global.FieldByName('CheckReNameFileAllegato').asBoolean = TRUE then begin
               xFileName := DbEdit1.Text + '_' + DbEdit5.Text + '_' + DbEdit4.Text + '_' + xData + '_' + xOra;


               xFileName := StringReplace(xFileName, '/', '', [rfReplaceAll]);
               xFileName := StringReplace(xFileName, '\', '', [rfReplaceAll]);
               xFileName := StringReplace(xFileName, '.', '', [rfReplaceAll]);

               xFileName := GetPathFromTable('Commessa') + xFileName + '.xls';

               if data.Global.FieldByName('DebugHrms').AsBoolean then showmessage('xFileName= ' + xFileName);

               try
                    dxDBGrid1.SaveToXLS(xFileName, True);

                    Data.Q1.Close;
                    Data.Q1.SQL.text := 'insert into EBC_RicercheFile (IDRicerca,IDTipo,Descrizione,NomeFile,DataCreazione) ' +
                         'values (:xIDRicerca:,:xIDTipo:,:xDescrizione:,:xNomeFile:,:xDataCreazione:)';
                    Data.Q1.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
                    Data.Q1.ParamByName['xIDTipo'] := 0;
                    Data.Q1.ParamByName['xDescrizione'] := 'Esportazione ricerca ' + DbEdit1.Text + ' (' + datetostr(Date) + ')';
                    Data.Q1.ParamByName['xNomeFile'] := xFileName;
                    Data.Q1.ParamByName['xDataCreazione'] := Date;
                    Data.Q1.ExecSQL;
               except
               end;

               if MainForm.xSendMail_Remote then begin
                    CopiaApriFileSulClient(xFileName, true);
               end else
                    ShellExecute(0, 'Open', pchar(xFileName), '', '', SW_SHOWDEFAULT);

          end else
               Mainform.Save('xls', 'Microsoft Excel 4.0 Worksheet (*.xls)|*.xls', 'ExpGrid.xls', dxDBGrid1.SaveToXLS);

     end else
          Mainform.Save('xls', 'Microsoft Excel 4.0 Worksheet (*.xls)|*.xls', 'ExpGrid.xls', dxDBGrid1.SaveToXLS);
end;

procedure TSelPersForm.esportainHTML1Click(Sender: TObject);
begin
     if not LogEsportazioniCand(1) then exit;
     Mainform.Save('htm', 'HTML File (*.htm; *.html)|*.htm', 'ExpGrid.htm', dxDBGrid1.SaveToHTML);
end;

//[TONI20021002]DEBUGOK

procedure TSelPersForm.BModCodCommessaClick(Sender: TObject);
var xRif: string;
     xID: integer;
begin
     if MessageDlg('ATTENZIONE: stai cambiando il NUMERO della commessa/progetto?' + chr(13) +
          'VUOI PROSEGUIRE ?', mtWarning, [mbYes, mbNo], 0) = mrNo then exit;
     xRif := DataRicerche.TRicerchePend.FieldByName('Progressivo').AsString;
     if InputQuery('Modifica rif./codice commessa/progetto', 'nuovo rif./codice:', xRif) then begin
          xID := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger; ;
          DataRicerche.TRicerchePend.Edit;
          DataRicerche.TRicerchePend.FieldByName('Progressivo').AsString := xRif;
          with DataRicerche.TRicerchePend do begin
               Data.DB.BeginTrans;
               try
                    //                    ApplyUpdates;
                    Post;
                    Data.DB.CommitTrans;
                    CaricaApriRicPend(xID);
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                    raise;
               end;
               //               CommitUpdates;
          end;
     end;

end;

procedure TSelPersForm.SpeedButton6Click(Sender: TObject);
var xIDRuoloRic: string;
begin
     if DataRicerche.QRicUtenti.IsEmpty then exit;
     xIDRuoloRic := DataRicerche.QRicUtenti.FieldByName('IDRuoloRic').AsString;

     xIDRuoloRic := OpenSelFromQuery(' select * from EBC_RicUtentiRuoli where id <>1 ',
          'ID', 'RuoloRic', 'Ruolo commessa/progetto', '', False);

     if xIDRuoloRic = '' then begin
          //showmessage('operazione annullata');
          exit;
     end;
     if xIDRuoloRic = '1' then begin
          MessageDlg('Impossibile inserire un altro capo-commessa/progetto: impossibile continuare', mtError, [mbOK], 0);
          //showmessage('operazione annullata');
          exit;
     end;
     with Data do begin
          DB.BeginTrans;
          try
               Q1.SQL.text := 'update EBC_RicercheUtenti set IDRuoloRic=:xIDRuoloRic: ' +
                    'where ID=' + DataRicerche.QRicUtenti.FieldByName('ID').asString;
               Q1.ParamByName['xIDRuoloRic'] := StrToInt(xIDRuoloRic);
               Q1.ExecSQL;
               DB.CommitTrans;
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: operazione non effettuata', mtError, [mbOK], 0);
          end;
     end;
     DataRicerche.QRicUtenti.Close;
     DataRicerche.QRicUtenti.Open;
end;

procedure TSelPersForm.SpeedButton7Click(Sender: TObject);
begin
     if DataRicerche.QRicUtenti.IsEmpty then exit;
     // controllo ore segnate per l'utente in questa commessa
     Data.QTemp.Close;
     Data.QTemp.SQL.Text := 'select count(*) Tot from EBC_RicercheTimeSheet ' +
          'where IDUtente=:xIDUtente: and IDRicerca=:xIDRicerca:';
     Data.QTemp.ParamByName['xIDUtente'] := DataRicerche.QRicUtenti.FieldByName('IDUtente').AsInteger;
     Data.QTemp.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
     Data.QTemp.Open;
     if Data.QTemp.FieldByName('Tot').asInteger > 0 then begin
          MessageDlg('Ci sono ore imputate all''utente nel time-sheet: impossibile cancellare.', mtError, [mbOK], 0);
          Data.QTemp.Close;
          exit;
     end;
     Data.QTemp.Close;

     if MessageDlg('Sei sicuro di voler eliminare l''utente dalla commessa/progetto ?', mtWarning, [mbNo, mbYes], 0) = mrYes then begin
          with Data do begin
               //DB.BeginTrans;
               try
                    Q1.SQL.text := 'delete from EBC_RicercheUtenti ' +
                         'where ID=' + DataRicerche.QRicUtenti.FieldByName('ID').asString;
                    Q1.ExecSQL;
                    //   DB.CommitTrans;
               except
                    // DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: operazione non effettuata', mtError, [mbOK], 0);
                    raise
               end;
          end;
          DataRicerche.QRicUtenti.Close;
          DataRicerche.QRicUtenti.Open;
     end;
end;

procedure TSelPersForm.dxDBGrid3MouseUp(Sender: TObject;
     Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
     if (Button <> mbRight) or (Shift <> []) then Exit;
     TdxDBGridPopupMenuManager.Instance.ShowGridPopupMenu(Sender as TdxDBGrid);
end;

//[TONI20021004]DEBUGOK

procedure TSelPersForm.BRicClientiClick(Sender: TObject);
begin
     if xDisabilitaTargetList then begin
          showmessage('Funzione disabilitata');
          exit;
     end;
     if not MainForm.CheckProfile('171') then Exit;
     RicClientiForm := TRicClientiForm.create(self);
     RicClientiForm.BAddTargetList.visible := True;
     RicClientiForm.xIDRicerca := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
     RicClientiForm.xIDClienteRic := DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger;
     // riempimento combobox1 con le tabelle
     with RicClientiForm do begin
          QTabelle.Close;
          QTabelle.Open;
          QTabelle.First;
          ComboBox1.Items.Clear;
          while not QTabelle.EOF do begin
               ComboBox1.Items.Add(QTabelle.FieldByName('DescTabella').asString);
               QTabelle.Next;
          end;
          QRes1.Close;
     end;
     // togli multiselect
     RicClientiForm.DBGrid5.OptionsBehavior := [edgoAutoSort, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoStoreToIniFile, edgoTabThrough, edgoVertThrough];
     RicClientiForm.Showmodal;
     RicClientiForm.Free;
     DataRicerche.QRicTargetList.Close;
     DataRicerche.QRicTargetList.Close;
     DataRicerche.QRicTargetList.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePendID.Value;
     DataRicerche.QRicTargetList.Open;
end;

procedure TSelPersForm.MenuItem1Click(Sender: TObject);
begin
     dxPrinter1.Preview(True, dxPrinter1Link2);
end;

procedure TSelPersForm.MenuItem2Click(Sender: TObject);
begin
     Mainform.Save('xls', 'Microsoft Excel 4.0 Worksheet (*.xls)|*.xls', 'ExpGrid.xls', dxDBGrid3.SaveToXLS);
end;

procedure TSelPersForm.MenuItem3Click(Sender: TObject);
begin
     Mainform.Save('htm', 'HTML File (*.htm; *.html)|*.htm', 'ExpGrid.htm', dxDBGrid3.SaveToHTML);
end;

procedure TSelPersForm.BTLOrgClick(Sender: TObject);
var i, k, j: integer;
     xArrayStd, xArrayNew: array[1..100] of integer;
begin
     if xDisabilitaTargetList then begin
          showmessage('Funzione disabilitata');
          exit;
     end;
     if DataRicerche.QRicTargetList.IsEmpty then exit;
     MainForm.LOBAziendeClick(self);
     xIDOrg := DataRicerche.QRicTargetList.FieldByName('IDCliente').AsInteger;
     // carica organigramma
     with DataRicerche do begin
          if not ((QAziende.active) and (QAziende.FieldByName('ID').AsInteger = xIDOrg)) then begin
               QOrgNew.Close;
               QAziende.close;
               QAziende.SQL.Text := 'select EBC_Clienti.* ,EBC_attivita.Attivita from EBC_Clienti,EBC_attivita ' +
                    'where EBC_Clienti.IDAttivita=EBC_attivita.ID and EBC_Clienti.ID=' + IntToStr(xIDOrg);
               QAziende.Open;
               QOrgNew.Open;
               // se non c'� l'organigramma ==> proponi quello standard
               if QOrgNew.IsEmpty then begin
                    if MessageDlg('Per questa azienda non risulta impostato l''organigramma ' + chr(13) +
                         'VUOI ASSOCIARE L''ORGANIGRAMMA STANDARD ?', mtWarning, [mbYes, mbNo], 0) = mrYes then begin
                         Data.Qtemp.Close;
                         Data.Qtemp.SQL.text := 'select * from OrgStandard where idsoftware=1 order by PARENT';
                         Data.Qtemp.Open;
                         k := 1;
                         while not Data.Qtemp.EOF do begin
                              Data.DB.BeginTrans;
                              try
                                   Data.Q1.Close;
                                   Data.Q1.SQL.text := 'insert into Organigramma (Tipo,Descrizione,DescBreve,Interim,InStaff,' +
                                        '                                       WIDTH,HEIGHT,TYPE,COLOR,IMAGE,IMAGEALIGN,ORDINE,ALIGN,IDCliente,IDSoftware) ' +
                                        'values (:xTipo:,:xDescrizione:,:xDescBreve:,:xInterim:,:xInStaff:,:xWIDTH:,:xHEIGHT:,:xTYPE:,' +
                                        '        :xCOLOR:,:xIMAGE:,:xIMAGEALIGN:,:xORDINE:,:xALIGN:,:xIDCliente:,:xIDSoftware:)';
                                   Data.Q1.ParamByName['xTipo'] := Data.Qtemp.FieldByName('Tipo').asString;
                                   Data.Q1.ParamByName['xDescrizione'] := Data.Qtemp.FieldByName('Descrizione').asString;
                                   Data.Q1.ParamByName['xDescBreve'] := Data.Qtemp.FieldByName('DescBreve').asString;
                                   Data.Q1.ParamByName['xInterim'] := Data.Qtemp.FieldByName('Interim').asBoolean;
                                   Data.Q1.ParamByName['xInStaff'] := Data.Qtemp.FieldByName('InStaff').asBoolean;
                                   Data.Q1.ParamByName['xWIDTH'] := Data.Qtemp.FieldByName('WIDTH').asInteger;
                                   Data.Q1.ParamByName['xHEIGHT'] := Data.Qtemp.FieldByName('HEIGHT').asInteger;
                                   Data.Q1.ParamByName['xTYPE'] := Data.Qtemp.FieldByName('TYPE').asstring;
                                   Data.Q1.ParamByName['xCOLOR'] := Data.Qtemp.FieldByName('COLOR').asInteger;
                                   Data.Q1.ParamByName['xIMAGE'] := Data.Qtemp.FieldByName('IMAGE').asInteger;
                                   Data.Q1.ParamByName['xIMAGEALIGN'] := Data.Qtemp.FieldByName('IMAGEALIGN').asstring;
                                   Data.Q1.ParamByName['xORDINE'] := Data.Qtemp.FieldByName('ORDINE').asInteger;
                                   Data.Q1.ParamByName['xALIGN'] := Data.Qtemp.FieldByName('ALIGN').asString;
                                   Data.Q1.ParamByName['xIDCliente'] := xIDOrg;
                                   Data.Q1.ParambyName['xIDSoftware'] := 1;
                                   Data.Q1.ExecSQL;
                                   //
                                   Data.Q2.SQL.text := 'select @@IDENTITY as LastID';
                                   Data.Q2.Open;
                                   xArrayStd[k] := Data.QTemp.FieldByName('ID').asInteger;
                                   xArrayNew[k] := Data.Q2.FieldByName('LastID').asInteger;
                                   Data.Q2.Close;

                                   Data.DB.CommitTrans;
                              except
                                   Data.DB.RollbackTrans;
                                   MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                              end;
                              inc(k);
                              Data.Qtemp.Next;
                         end;
                         // PARENT
                         Data.Q2.Close;
                         Data.Q2.SQL.text := 'select ID from Organigramma where IDCliente=' + IntToStr(xIDOrg) + ' order by ID';
                         Data.Q2.Open;
                         Data.Qtemp.First;
                         Data.Q2.Next;
                         Data.Qtemp.Next;
                         k := 1;
                         while not Data.Qtemp.EOF do begin
                              // trova nuovo PARENT nell'xArrayNew
                              for i := 1 to 100 do
                                   if xArrayStd[i] = Data.QTemp.FieldByName('PARENT').asInteger then break;

                              if xArrayNew[i] <> 0 then begin
                                   Data.DB.BeginTrans;
                                   try
                                        Data.Q1.Close;
                                        Data.Q1.SQL.text := 'update Organigramma set PARENT=:xPARENT: ' +
                                             'where ID=' + IntToStr(Data.Q2.FieldByName('ID').asInteger);
                                        Data.Q1.ParamByName['xPARENT'] := xArrayNew[i];
                                        Data.Q1.ExecSQL;
                                        Data.DB.CommitTrans;
                                   except
                                        Data.DB.RollbackTrans;
                                        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                                   end;
                              end;
                              inc(k);
                              Data.Qtemp.Next;
                              Data.Q2.Next;
                         end;
                         Data.Qtemp.Close;
                         Data.Q2.Close;
                    end;
                    QOrgNew.Close;
                    QOrgNew.Open;
               end;
               MainForm.PColor.Color := DataRicerche.QOrgNew.FieldByName('COLOR').AsInteger;
          end;
     end;
     // se data diversa ==> log target list


     Data.QTemp.Close;
     Data.QTemp.SQL.text := 'select Max(Data) as Data from EBC_LogEsplorazioni ' +
          'where IDTargetList=' + DataRicerche.QRicTargetList.FieldByName('ID').AsString;
     Data.QTemp.Open;
     if (Data.QTemp.RecordCount = 0) or ((Data.QTemp.RecordCount > 0) and (Data.QTemp.FieldByName('Data').asDateTime <> Date)) then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text := 'insert into EBC_LogEsplorazioni (IDTargetList,Data) ' +
                         'values (:xIDTargetList:,:xData:)';
                    Q1.ParamByName['xIDTargetList'] := DataRicerche.QRicTargetList.FieldByName('ID').AsInteger;
                    Q1.ParamByName['xData'] := Date;
                    Q1.ExecSQL;
                    DB.CommitTrans;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: operazione non effettuata', mtError, [mbOK], 0);
               end;
          end;
     end;
     Data.QTemp.Close;
     MainForm.DisegnaTVAziendaCand;
     MainForm.BTargetList.visible := True;
     MainForm.xIDRicerca := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
     MainForm.xIDTargetList := DataRicerche.QRicTargetList.FieldByName('ID').AsInteger;
     close;
end;

procedure TSelPersForm.BTLDeleteClick(Sender: TObject);
begin
     if DataRicerche.QRicTargetList.IsEmpty then exit;
     if MessageDlg('Sei sicuro di voler elminare l''azienda ' + DataRicerche.QRicTargetList.FieldByName('Azienda').AsString + ' dalla target list ?', mtwarning, [mbYes, mbNo], 0) = mrNo then exit;
     with Data do begin
          Q1.Close;
          Q1.SQL.text := 'delete from EBC_RicercheTargetList ' +
               'where ID=' + DataRicerche.QRicTargetList.FieldByName('ID').asString;
          Q1.ExecSQL;
          DataRicerche.QRicTargetList.Close;
          DataRicerche.QRicTargetList.Close;
          DataRicerche.QRicTargetList.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePendID.Value;
          DataRicerche.QRicTargetList.Open;
     end;

end;

procedure TSelPersForm.BTLCandVaiClick(Sender: TObject);
begin
     if (Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 3)) = 'RAY') or (Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 7)) = 'MISTRAL') then begin
          if not Mainform.CheckProfile('00') then Exit;
          data.Global.open;
          SchedaCandForm := TSchedaCandForm.create(self);
          if not PosizionaAnag(DataRicerche.QTLCandidati.FieldByName('IDAnagrafica').AsInteger) then exit;
          SchedaCandForm.ShowModal;
          SchedaCandForm.Free;
          RiprendiAttivita;

     end else begin

          if DataRicerche.QTLCandidati.IsEmpty then exit;
          DataRicerche.QCandRic.Locate('ID', DataRicerche.QTLCandidati.FieldByName('ID').AsInteger, []);
          PCSelPers.ActivePage := TSCandidati;
     end;
end;

procedure TSelPersForm.BTLElencoCliClick(Sender: TObject);
var xIDTargetList, xIDRicerca, xIDClienteRic: integer;
     xpassword: string;
begin
     if xDisabilitaTargetList then begin
          showmessage('Funzione disabilitata');
          exit;
     end;
     xIDRicerca := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger; ;
     xIDClienteRic := DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger;
     SelClienteForm := TSelClienteForm.create(self);
     SelClienteForm.RGFiltro.ItemIndex := 0;
     SelClienteForm.ShowModal;
     if SelClienteForm.ModalResult = mrOK then begin
          // cliente attivo
          {if SelClienteForm.TClienti.FieldByName('Stato').AsString = 'attivo' then begin
            if messageDlg('L''azienda � un cliente ATTIVO - Vuoi proseguire ?', mtWarning, [mbYes, mbNo], 0) = mrNo then
              exit;
          end;}
          // stesso cliente della commessa
          if xIDClienteRic = SelClienteForm.TClienti.FieldByName('ID').AsInteger then begin
               messageDlg('L''azienda � il cliente della commessa/progetto !!', mtError, [mbOK], 0);
               exit;
          end;
          // controllo se � gi� un cliente in target list
          Data.QTemp.Close;
          Data.QTemp.SQL.text := 'select count(*) Tot from EBC_RicercheTargetList ' +
               'where IDRicerca=:xIDRicerca: and IDCliente=:xIDCliente:';
          Data.QTemp.ParamByName['xIDRicerca'] := xIDRicerca;
          Data.QTemp.ParamByName['xIDCliente'] := SelClienteForm.TClienti.FieldByName('ID').AsInteger; ;
          Data.QTemp.Open;
          if Data.QTemp.FieldByName('Tot').asInteger > 0 then begin
               messageDlg('Azienda gi� presente nella target list !', mtError, [mbOK], 0);
               Data.QTemp.Close;
               exit;
          end;
          // controllo blocco 1
          Data.QTemp.Close;
          Data.QTemp.SQL.text := 'select count(*) Tot from EBC_Clienti where ID=:xID: and Blocco1=1';
          Data.QTemp.ParamByName['xID'] := SelClienteForm.TClienti.FieldByName('ID').AsInteger;
          Data.QTemp.Open;
          if Data.QTemp.FieldByName('Tot').asInteger > 0 then begin
               if messageDlg('ATTENZIONE:  l''azienda ha impostato il BLOCCO di livello 1 - VUOI PROSEGUIRE ? ', mtWarning, [mbYes, mbNo], 0) = mrNo then begin
                    Data.QTemp.Close;
                    exit;
               end;
          end;
          // controllo storico target list
          if CheckStoricoTL(SelClienteForm.TClienti.FieldByName('ID').AsInteger) = False then exit;

          if messageDlg('Sei sicuro di voler aggiungere l''azienda ' + SelClienteForm.TClienti.FieldByName('Descrizione').AsString + ' alla target List ?',
               mtWarning, [mbYes, mbNo], 0) = mrNo then exit;

          Data.QTemp.Close;
          Data.QTemp.SQL.text := 'select count(*) Tot from EBC_Clienti where stato= ''attivo'' and IDTipoAzienda = 1 and ID = :xID:';
          Data.QTemp.ParamByName['xID'] := SelClienteForm.TClienti.FieldByName('ID').AsInteger;
          Data.QTemp.Open;
          if Data.QTemp.FieldByName('Tot').asInteger > 0 then begin
               if MessageDlg('ATTENZIONE!! PROTEZIONE CLIENTI LIVELLO 2' + chr(13) + 'Stai inserendo in Target List un candidato appartenente ad una azienda cliente ATTIVA' + chr(13) +
                    'L''operazione � protetta dalle normative interne, se sei autorizzato premi SI, altrimenti premi NO. ' +
                    'Se si prosegue, l''operazione verr� registrata nello storico del cliente come forzatura. '
                    , mtWarning, [mbYes, mbNo], 0) = mrYes then begin
                    PostMessage(Handle, WM_USER + 1024, 0, 0);

                    if pos('INTERMEDIA', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) = 0 then begin
                         // richiesta password
                         InputPasswordForm.EPassword.text := 'EPassword';
                         InputPasswordForm.caption := 'Password utente resp.';
                         InputPasswordForm.ShowModal;
                         if InputPasswordForm.ModalResult = mrOK then begin
                              xPassword := InputPasswordForm.EPassword.Text;
                         end else exit;
                         if not CheckPassword(xPassword, GetPwdUtenteResp(9)) then begin
                              MessageDlg('Password errata', mtError, [mbOK], 0);
                              exit;
                         end;
                    end;
               end else exit;
          end;
          Data.QTemp.Close;
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.SQL.text := 'insert into EBC_RicercheTargetList (IDRicerca,IDCliente) ' +
                         'values (:xIDRicerca:,:xIDCliente:)';
                    Q1.ParamByName['xIDRicerca'] := xIDRicerca;
                    Q1.ParamByName['xIDCliente'] := SelClienteForm.TClienti.FieldByName('ID').AsInteger;
                    Q1.ExecSQL;
                    Q1.SQL.text := 'select @@IDENTITY as LastID';
                    Q1.Open;
                    xIDTargetList := Q1.FieldByName('LastID').asInteger;
                    Q1.Close;
                    Q1.SQL.text := 'insert into EBC_LogEsplorazioni (IDTargetList,Data) ' +
                         'values (:xIDTargetList:,:xData:)';
                    Q1.ParamByName['xIDTargetList'] := xIDTargetList;
                    Q1.ParamByName['xData'] := Date;
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    DataRicerche.QRicTargetList.Close;
                    DataRicerche.QRicTargetList.Close;
                    DataRicerche.QRicTargetList.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePendID.Value;
                    DataRicerche.QRicTargetList.Open;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: operazione non effettuata', mtError, [mbOK], 0);
               end;
          end;
     end;
     SelClienteForm.Free;
end;

//[TONI20021004]DEBUGOK

procedure TSelPersForm.BTLCandNewClick(Sender: TObject);
var xAttuale, xSettore: string;
     xVai: boolean;
     xRic, xLivProtez, xDicMess, xPassword, xUtenteResp, xClienteBlocco: string;
     xIDEvento, xIDClienteBlocco, xIDAnag, xIDRicerca, xIDTargetList: integer;
     QLocal: TADOLinkedQuery;
begin
     if DataRicerche.QRicTargetList.IsEmpty then exit;
     ElencoDipForm := TElencoDipForm.create(self);
     ElencoDipForm.ShowModal;
     if ElencoDipForm.ModalResult = mrOK then begin
          // aggiunta soggetto alla commessa
          xIDAnag := ElencoDipForm.TAnagDip.FieldByName('ID').AsInteger;
          xIDRicerca := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
          xIDTargetList := DataRicerche.QRicTargetList.FieldByName('ID').AsInteger;
          Data.QTemp.Close;
          Data.QTemp.SQL.text := 'select IDProprietaCV from Anagrafica where ID=' + IntToStr(xIDAnag);
          Data.QTemp.Open;
          // controllo propriet� CV
          if (not ((Data.QTemp.FieldByName('IDProprietaCV').asString = '') or (Data.QTemp.FieldByName('IDProprietaCV').asInteger = 0))) and
               (Data.QTemp.FieldByName('IDProprietaCV').asInteger <> DataRicerche.QRicTargetList.FieldByName('IDCliente').AsInteger) then
               if MessageDlg('ATTENZIONE: il CV risulta di propriet� di un altro cliente.' + chr(13) +
                    'Vuoi proseguire lo stesso ?', mtWarning, [mbYes, mbNo], 0) = mrNo then begin
                    Data.QTemp.Close;
                    exit;
               end;
          Data.QTemp.Close;

          DataRicerche.TRicAnagIDX.Open;
          //          if not DataRicerche.TRicAnagIDX.FindKey([xIDRicerca,xIDAnag]) then begin
          if not DataRicerche.TRicAnagIDX.FindKeyADO(VarArrayOf([xIDRicerca, xIDAnag])) then begin
               xVai := True;
               // controllo se � gi� associato ad un'altra ricerca per la STESSA azienda
               Data.Q1.Close;
               Data.Q1.SQL.clear;
               Data.Q1.SQl.Add('select EBC_Ricerche.Progressivo,EBC_StatiRic.StatoRic from EBC_CandidatiRicerche,EBC_Ricerche,EBC_StatiRic ');
               Data.Q1.SQl.Add('where EBC_CandidatiRicerche.IDRicerca=EBC_Ricerche.ID and EBC_Ricerche.IDStatoRic=EBC_StatiRic.ID');
               Data.Q1.SQl.Add('and EBC_CandidatiRicerche.IDAnagrafica=:xIDAnag: and EBC_Ricerche.IDCliente=:xIDCliente:');
               //               Data.Q1.Prepare;
               Data.Q1.ParamByName['xIDAnag'] := xIDAnag;
               Data.Q1.ParamByName['xIDCliente'] := DataRicerche.QRicTargetList.FieldByName('IDCliente').AsInteger;
               Data.Q1.Open;
               if not Data.Q1.IsEmpty then begin
                    xRic := '';
                    while not Data.Q1.EOF do begin xRic := xRic + 'N�' + Data.Q1.FieldByName('Progressivo').asString + ' (' + Data.Q1.FieldByName('StatoRic').asString + ') '; Data.Q1.Next; end;
                    if MessageDlg('Il soggetto � associato gi� alle seguenti ricerche per lo stesso cliente: ' + chr(13) + xRic + chr(13) + 'PROCEDERE COMUNQUE ?', mtWarning, [mbYes, mbNo], 0) = mrNo then xVai := False;
               end;
               // controllo incompatibilit�
               if IncompAnagCli(xIDAnag, DataRicerche.QRicTargetList.FieldByName('IDCliente').AsInteger) then
                    if MessageDlg('Il soggetto risulta INCOMPATIBILE con questo cliente o con un cliente associato' + chr(13) +
                         'PROCEDERE COMUNQUE ?', mtWarning, [mbYes, mbNo], 0) = mrNo then xVai := False;

               // controllo blocco livello 1 o 2
               xIDClienteBlocco := CheckAnagInseritoBlocco1(xIDAnag);
               if xIDClienteBlocco > 0 then begin
                    xLivProtez := copy(IntToStr(xIDClienteBlocco), 1, 1);
                    if xLivProtez = '1' then begin
                         xIDClienteBlocco := xIDClienteBlocco - 10000;
                         xClienteBlocco := GetDescCliente(xIDClienteBlocco);
                         xDicMess := 'Stai inserendo in commessa/progetto un candidato appartenente ad una azienda (' + xClienteBlocco + ') con blocco di livello 1, ';
                    end else begin
                         xIDClienteBlocco := xIDClienteBlocco - 20000;
                         xClienteBlocco := GetDescCliente(xIDClienteBlocco);
                         xDicMess := 'Stai inserendo in commessa/progetto un candidato appartenente ad una azienda ATTIVA (' + xClienteBlocco + '), ';
                    end;
                    if MessageDlg('ATTENZIONE!! PROTEZIONE CLIENTI LIVELLO ' + xLivProtez + chr(13) + xDicMess +
                         'protetta dalle normative interne, se sei autorizzato premi SI, altrimenti premi NO. ' +
                         'Se si prosegue, l''operazione verr� registrata nello storico del soggetto come forzatura.', mtWarning, [mbYes, mbNo], 0) = mrNo then xVai := False
                    else begin
                         // pwd
                         xPassword := '';
                         xUtenteResp := GetDescUtenteResp(MainForm.xIDUtenteAttuale);

                         if pos('INTERMEDIA', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) = 0 then begin
                              // richiesta password
                              InputPasswordForm.EPassword.text := 'EPassword';
                              InputPasswordForm.caption := 'Inserimento password utente';
                              InputPasswordForm.ShowModal;
                              if InputPasswordForm.ModalResult = mrOK then begin
                                   xPassword := InputPasswordForm.EPassword.Text;
                              end else exit;
                              if not CheckPassword(TrimRight(xPassword), GetPwdUtente(MainForm.xIDUtenteAttuale)) then begin
                                   MessageDlg('Password errata', mtError, [mbOK], 0);
                                   xVai := False;
                              end;
                         end;
                         if xVai then begin
                              // promemoria al resp.
                              QLocal := CreateQueryFromString('insert into Promemoria (IDUtente,IDUtenteDa,DataIns,Testo,Evaso,DataDaLeggere) ' +
                                   'values (:xIDUtente:,:xIDUtenteDa:,:xDataIns:,:xTesto:,:xEvaso:,:xDataDaLeggere:)');
                              QLocal.ParamByName['xIDUtente'] := GetIDUtenteResp(MainForm.xIDUtenteAttuale);
                              QLocal.ParamByName['xIDUtenteDa'] := MainForm.xIDUtenteAttuale;
                              QLocal.ParamByName['xDataIns'] := Date;
                              QLocal.ParamByName['xTesto'] := 'forzatura livello ' + xLivProtez + ' per CV n� ' + ElencoDipForm.TAnagDip.FieldByName('CVNumero').AsString;
                              QLocal.ParamByName['xEvaso'] := 0;
                              QLocal.ParamByName['xDataDaLeggere'] := Date;
                              QLocal.ExecSQL;
                              // registra nello storico soggetto
                              xIDEvento := GetIDEvento('forzatura Blocco livello ' + xLivProtez);
                              if xIDEvento > 0 then begin
                                   with Data.Q1 do begin
                                        close;
                                        SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
                                             'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xIDCliente:)';
                                        ParamByName['xIDAnagrafica'] := xIDAnag;
                                        ParamByName['xIDEvento'] := xIDevento;
                                        ParamByName['xDataEvento'] := Date;
                                        ParamByName['xAnnotazioni'] := 'cliente: ' + xClienteBlocco;
                                        ParamByName['xIDRicerca'] := xIDRicerca;
                                        ParamByName['xIDUtente'] := MainForm.xIDUtenteAttuale;
                                        ParamByName['xIDCliente'] := xIDClienteBlocco;
                                        ExecSQL;
                                   end;
                              end;
                         end;
                    end;
               end;

               if xVai then begin
                    Data.DB.BeginTrans;
                    try
                         if Data.Q1.Active then Data.Q1.Close;
                         Data.Q1.close;
                         Data.Q1.sql.text := 'select * from ebc_candidatiricerche where idricerca = ' + DataRicerche.TRicerchePend.FieldByName('ID').AsString +
                              ' and idanagrafica = ' + ElencoDipForm.TAnagDip.FieldByName('ID').AsString;
                         Data.Q1.Open;
                         if not Data.Q1.IsEmpty then begin
                              if Data.Q1.Active then Data.Q1.Close;
                              MessageDlg('Il candidato � gi� associato al progetto/commessa', mtWarning, [mbOK], 0);
                              if (Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 3)) = 'RAY') or (Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 7)) = 'MISTRAL') then begin
                                   Data.Q1.SQL.text := 'Update EBC_CandidatiRicerche Set Escluso = 0' +
                                        ', Stato = ''inserito da Target List''' +
                                        ', DataIns = ''' + DateToStr(Date) + '''' +
                                        ', IDTargetList = ' + IntToStr(xIDTargetList) +
                                        ', InsFromTL = 0' +
                                        ' where IDRicerca = ' + IntToStr(xIDRicerca) +
                                        ' and IDAnagrafica = ' + IntToStr(xIDAnag);
                              end else begin
                                   Data.Q1.SQL.text := 'Update EBC_CandidatiRicerche Set  Escluso = 0' +
                                        ', Stato = ''inserito da Target List''' +
                                        ', DataIns = ''' + DateToStr(Date) + '''' +
                                        ', IDTargetList = ' + IntToStr(xIDTargetList) +
                                        ' where IDRicerca = ' + IntToStr(xIDRicerca) +
                                        ' and IDAnagrafica = ' + IntToStr(xIDAnag);
                              end;
                         end else begin
                              if Data.Q1.Active then Data.Q1.Close;
                              if (Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 3)) = 'RAY') or (Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 7)) = 'MISTRAL') then begin
                                   Data.Q1.SQL.text := 'insert into EBC_CandidatiRicerche (IDRicerca,IDAnagrafica,Escluso,Stato,DataIns,IDTargetList, InsFromTL) ' +
                                        'values (:xIDRicerca:,:xIDAnagrafica:,:xEscluso:,:xStato:,:xDataIns:,:xIDTargetList:,:xInsFromTL:)';
                                   Data.Q1.ParamByName['xIDRicerca'] := xIDRicerca;
                                   Data.Q1.ParamByName['xIDAnagrafica'] := xIDAnag;
                                   Data.Q1.ParamByName['xEscluso'] := 0;
                                   Data.Q1.ParamByName['xStato'] := 'inserito da Target List';
                                   Data.Q1.ParamByName['xDataIns'] := Date;
                                   Data.Q1.ParamByName['xIDTargetList'] := xIDTargetList;
                                   Data.Q1.ParamByName['xInsFromTL'] := 0;
                              end else begin
                                   Data.Q1.SQL.text := 'insert into EBC_CandidatiRicerche (IDRicerca,IDAnagrafica,Escluso,Stato,DataIns,IDTargetList) ' +
                                        'values (:xIDRicerca:,:xIDAnagrafica:,:xEscluso:,:xStato:,:xDataIns:,:xIDTargetList:)';
                                   Data.Q1.ParamByName['xIDRicerca'] := xIDRicerca;
                                   Data.Q1.ParamByName['xIDAnagrafica'] := xIDAnag;
                                   Data.Q1.ParamByName['xEscluso'] := 0;
                                   Data.Q1.ParamByName['xStato'] := 'inserito da Target List';
                                   Data.Q1.ParamByName['xDataIns'] := Date;
                                   Data.Q1.ParamByName['xIDTargetList'] := xIDTargetList;
                              end;
                         end;
                         Data.Q1.ExecSQL;
                         // aggiornamento stato anagrafica
                         if Data.Q1.Active then Data.Q1.Close;
                         if pos('EMERGENCY', UpperCase(data.Global.fieldbyname('NomeAzienda').asString)) > 0 then
                              Data.Q1.SQL.text := 'update Anagrafica set IDStato=28,IDTipoStato=2 where ID=' + IntToStr(xIDAnag)
                         else Data.Q1.SQL.text := 'update Anagrafica set IDStato=27,IDTipoStato=1 where ID=' + IntToStr(xIDAnag);
                         Data.Q1.ExecSQL;
                         // aggiornamento storico
                         if Data.Q1.Active then Data.Q1.Close;
                         Data.Q1.SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
                              'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xIDCliente:)';
                         Data.Q1.ParamByName['xIDAnagrafica'] := xIDAnag;
                         Data.Q1.ParamByName['xIDEvento'] := 19;
                         Data.Q1.ParamByName['xDataEvento'] := date;
                         Data.Q1.ParamByName['xAnnotazioni'] := 'inserito da target list';
                         Data.Q1.ParamByName['xIDRicerca'] := xIDRicerca;
                         Data.Qtemp.Close;
                         Data.Qtemp.SQL.text := 'select IDUtente,IDCliente from EBC_Ricerche where ID=' + IntToStr(xIDRicerca);
                         Data.Qtemp.Open;
                         Data.Q1.ParamByName['xIDUtente'] := MainForm.xIDUtenteAttuale;
                         Data.Q1.ParamByName['xIDCliente'] := Data.Qtemp.FieldByName('IDCliente').asInteger;
                         Data.Qtemp.Close;
                         Data.Q1.ExecSQL;
                         Data.DB.CommitTrans;
                         DataRicerche.QTLCandidati.Close;
                         //DataRicerche.QTLCandidati.ParamByName['xIDTargetList'] := DataRicerche.QRicTargetListID.Value;
                         //DataRicerche.QTLCandidati.ParamByName['xIDAzienda'] := DataRicerche.QRicTargetListIDCliente.Value;
                         DataRicerche.QTLCandidati.Open;
                         DataRicerche.QCandRic.Close;
                         DataRicerche.QCandRic.Open;
                    except
                         Data.DB.RollbackTrans;
                         MessageDlg('Errore sul database:  inserimento non avvenuto', mtError, [mbOK], 0);
                         raise;
                    end;
               end;
          end;
          DataRicerche.TRicAnagIDX.Close;
     end;
     ElencoDipForm.Free;
end;

procedure TSelPersForm.dxDBGrid4DblClick(Sender: TObject);
begin
     BTLCandVaiClick(self);
end;

procedure TSelPersForm.BTLCandInsNuovoClick(Sender: TObject);
var xIDAnag: integer;
     xS, xSettore: string;
     xVai, xNumeroValido: boolean;
     xCVNumero: string;
begin
     xVai := true;
     if DataRicerche.QRicTargetList.IsEmpty then exit;
     with MainForm do begin
          if not CheckProfile('65') then Exit;
          if not CheckProfile('000') then Exit;
          // controllo numero massimo consentiti
          NuovoSoggettoForm := TNuovoSoggettoForm.create(self);
          NuovoSoggettoForm.ShowModal;
          if NuovoSoggettoForm.ModalResult = mrOK then begin
               {Data.DB.BeginTrans;
               try}
                    // NUMERO CV
               if not cCVNumAutomatico then begin
                    // manuale
                    xCVNumero := '0';
                    xNumeroValido := False;
                    while not xNumeroValido do begin
                         if not InputQuery('Numero CV manuale', 'Numero CV:', xCVNumero) then begin
                              xVai := False;
                              Break;
                         end else begin
                              if xCVNumero = '' then xCVNumero := '0';
                              if xCVNumero = '0' then begin
                                   if MessageDlg('ATTENZIONE: Non � possibile inserire un n� CV uguale a ZERO' + chr(13) +
                                        'Verr� associato il progressivo autoincrementante interno.' + chr(13) +
                                        'Vuoi procedere (Yes) o Annullare l''inserimento (no) ?', mtWarning, [mbYes, mbNo], 0) = mrNo then begin
                                        xVai := False;
                                        Break;
                                   end;
                              end;
                              // controllo esistenza n� CV
                              data.QTemp.Close;
                              Data.QTemp.SQL.text := 'select Cognome,Nome from Anagrafica where CVNumero=' + xCVNumero;
                              Data.QTemp.Open;
                              if Data.QTemp.RecordCount > 0 then begin
                                   if MessageDlg('ATTENZIONE: esiste gi� il numero CV ' + xCVNumero + ' abbinato al soggetto:' + chr(13) +
                                        Data.QTemp.FieldByName('Cognome').asString + ' ' + Data.QTemp.FieldByName('Nome').asString + chr(13) + chr(13) +
                                        'Vuoi procedere (Yes) o Annullare l''inserimento (no) ?', mtWarning, [mbYes, mbNo], 0) = mrNo then begin
                                        xVai := False;
                                        break;
                                   end;
                              end else xNumeroValido := True;
                              Data.QTemp.Close;
                         end;
                    end;
               end else xCVNumero := '0'; // automatico

               if xVai then begin
                    Data.DB.BeginTrans;
                    try
                         Data.QTemp.Close;
                         Data.QTemp.SQL.Text := 'insert into Anagrafica (CVNumero,Cognome,Nome,IDStato,IDTipoStato,IDProprietaCV,Sesso,CVInseritoInData,password,email,idwebprofile,username) ' +
                              '  values (:xCVNumero:,:xCognome:,:xNome:,:xIDStato:,:xIDTipoStato:,:xIDProprietaCV:,:xSesso:,:xCVInseritoInData:,:xpassword:,:xemail:,:xidwebprofile:,:xusername:)';
                         Data.QTemp.ParamByName['xCVNumero'] := StrtoInt(xCVNumero);
                         Data.QTemp.ParamByName['xCognome'] := NuovoSoggettoForm.ECogn.Text;
                         Data.QTemp.ParamByName['xNome'] := NuovoSoggettoForm.ENome.Text;
                         if pos('EMERGENCY', UpperCase(data.Global.fieldbyname('NomeAzienda').asString)) > 0 then
                              Data.QTemp.ParamByName['xIDStato'] := 28
                         else Data.QTemp.ParamByName['xIDStato'] := 27;
                         Data.QTemp.ParamByName['xIDTipoStato'] := 1;
                         Data.QTemp.ParamByName['xIDProprietaCV'] := 0;
                         Data.QTemp.ParamByName['xSesso'] := NuovoSoggettoForm.CBSesso.Text;
                         Data.QTemp.ParamByName['xCVInseritoInData'] := date;
                         if data.Global.FieldByName('GeneraPasswordAnag').asinteger = 1 then
                              Data.QTemp.ParamByName['xpassword'] := CriptaStringa3DES(copy(CreateGuid, 2, 8))
                         else
                              Data.QTemp.ParamByName['xpassword'] := copy(CreateGuid, 2, 8);
                         Data.QTemp.ParamByName['xemail'] := NuovoSoggettoForm.EDMail.text;
                         Data.QTemp.ParamByName['xidwebprofile'] := 5;
                         Data.QTemp.ParamByName['xusername'] := GeneraUsername(NuovoSoggettoForm.EDMail.text, NuovoSoggettoForm.ECogn.Text, NuovoSoggettoForm.ENome.Text, '');
                         Data.QTemp.ExecSQL;

                         Data.QTemp.SQL.text := 'select @@IDENTITY as LastID';
                         Data.QTemp.Open;
                         xIDAnag := Data.QTemp.FieldByName('LastID').asInteger;
                         Data.QTemp.Close;

                         // log inserimento
                         Log_Operation(xIDUtenteAttuale, 'Anagrafica', xIDAnag, 'I');

                         // CV Numero automatico (o nullo)
                         if xCVNumero = '0' then begin
                              Data.QTemp.Close;
                              Data.QTemp.SQL.text := 'update Anagrafica set CVNumero=' + IntToStr(xIDAnag) + ' where ID=' + IntToStr(xIDAnag);
                              if pos('Athena', data.Global.fieldbyname('NomeAzienda').AsString) > 0 then
                                   xCVNumero := IntToStr(xIDAnag - 6520)
                              else
                                   xCVNumero := IntToStr(xIDAnag);
                              Data.QTemp.ExecSQL;
                         end;

                         // ALTRE INFO
                         Data.QTemp.Close;
                         Data.QTemp.SQL.text := 'insert into AnagAltreInfo (IDAnagrafica,Note) ' +
                              ' values (:xIDAnagrafica:,:xNote:)';
                         Data.QTemp.ParamByName['xIDAnagrafica'] := xIDAnag;
                         Data.QTemp.ParamByName['xNote'] := NuovoSoggettoForm.ENote.text;
                         Data.QTemp.ExecSQL;

                         // inserimento in ESPERIENZE LAVORATIVE
                         if DataRicerche.QRicTargetList.FieldByName('IDAttivita').asString = '' then xSettore := '0'
                         else xSettore := DataRicerche.QRicTargetList.FieldByName('IDAttivita').AsString;
                         Data.QTemp.Close;
                         Data2.QTemp.SQL.clear;
                         Data2.QTemp.SQL.Add('insert into EsperienzeLavorative (IDAnagrafica,IDAzienda,IDSettore,Attuale,IDMansione,IDArea) ' +
                              'values (' + IntToStr(xIDAnag) + ',' +
                              DataRicerche.QRicTargetList.FieldByName('IDCliente').asString + ',' +
                              xSettore + ',1,:xIDMansione:,:xIDArea:)');
                         Data2.QTemp.ParamByName['xIDMansione'] := NuovoSoggettoForm.xIDRuolo;
                         Data2.QTemp.ParamByName['xIDArea'] := NuovoSoggettoForm.xIDArea;
                         Data2.QTemp.ExecSQL;

                         // riga nello storico con evento specifico
                         Data.QTemp.Close;
                         Data.QTemp.SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente) ' +
                              'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:)';
                         Data.QTemp.ParamByName['xIDAnagrafica'] := xIDAnag;
                         Data.QTemp.ParamByName['xIDEvento'] := 87;
                         Data.QTemp.ParamByName['xDataEvento'] := Date;
                         Data.QTemp.ParamByName['xAnnotazioni'] := 'azienda: ' + DataRicerche.QRicTargetList.FieldByName('Azienda').asString;
                         Data.QTemp.ParamByName['xIDRicerca'] := 0;
                         Data.QTemp.ParamByName['xIDUtente'] := xIDUtenteAttuale;
                         Data.QTemp.ExecSQL;

                         // inserimento in target list
                         if Data.Q1.Active then Data.Q1.Close;
                         if (Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 3)) = 'RAY') or (Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 7)) = 'MISTRAL') then begin
                              Data.Q1.SQL.text := 'insert into EBC_CandidatiRicerche (IDRicerca,IDAnagrafica,Escluso,Stato,DataIns,IDTargetList, InsFromTL) ' +
                                   'values (:xIDRicerca:,:xIDAnagrafica:,:xEscluso:,:xStato:,:xDataIns:,:xIDTargetList:,:xInsFromTL:)';
                              Data.Q1.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
                              Data.Q1.ParamByName['xIDAnagrafica'] := xIDAnag;
                              Data.Q1.ParamByName['xEscluso'] := 0;
                              Data.Q1.ParamByName['xStato'] := 'inserito da Target List';
                              Data.Q1.ParamByName['xDataIns'] := Date;
                              Data.Q1.ParamByName['xIDTargetList'] := DataRicerche.QRicTargetList.FieldByName('ID').AsInteger;
                              Data.Q1.ParamByName['xInsFromTL'] := 0;
                         end else begin
                              Data.Q1.SQL.text := 'insert into EBC_CandidatiRicerche (IDRicerca,IDAnagrafica,Escluso,Stato,DataIns,IDTargetList) ' +
                                   'values (:xIDRicerca:,:xIDAnagrafica:,:xEscluso:,:xStato:,:xDataIns:,:xIDTargetList:)';
                              Data.Q1.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
                              Data.Q1.ParamByName['xIDAnagrafica'] := xIDAnag;
                              Data.Q1.ParamByName['xEscluso'] := 0;
                              Data.Q1.ParamByName['xStato'] := 'inserito da Target List';
                              Data.Q1.ParamByName['xDataIns'] := Date;
                              Data.Q1.ParamByName['xIDTargetList'] := DataRicerche.QRicTargetList.FieldByName('ID').AsInteger;
                         end;
                         Data.Q1.ExecSQL;
                         // aggiornamento storico
                         if Data.Q1.Active then Data.Q1.Close;
                         Data.Q1.SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
                              'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xIDCliente:)';
                         Data.Q1.ParamByName['xIDAnagrafica'] := xIDAnag;
                         Data.Q1.ParamByName['xIDEvento'] := 19;
                         Data.Q1.ParamByName['xDataEvento'] := date;
                         Data.Q1.ParamByName['xAnnotazioni'] := 'inserito da target list';
                         Data.Q1.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
                         Data.Qtemp.Close;
                         Data.Qtemp.SQL.text := 'select IDUtente,IDCliente from EBC_Ricerche where ID=' + IntToStr(xIDRicerca);
                         Data.Qtemp.Open;
                         Data.Q1.ParamByName['xIDUtente'] := MainForm.xIDUtenteAttuale;
                         Data.Q1.ParamByName['xIDCliente'] := Data.Qtemp.FieldByName('IDCliente').asInteger;
                         Data.Qtemp.Close;
                         Data.Q1.ExecSQL;
                         Data.DB.CommitTrans;
                         DataRicerche.QTLCandidati.Close;
                         //DataRicerche.QTLCandidati.ParamByName['xIDTargetList'] := DataRicerche.QRicTargetListID.Value;
                         //DataRicerche.QTLCandidati.ParamByName['xIDAzienda'] := DataRicerche.QRicTargetListIDCliente.Value;
                         DataRicerche.QTLCandidati.Open;
                         DataRicerche.QCandRic.Close;
                         DataRicerche.QCandRic.Open;

                    except
                         Data.DB.RollbackTrans;
                         MessageDlg('ATTENZIONE: errore sul database - Inserimento non avvenuto', mtError, [mbOK], 0);
                         raise;
                    end;
               end;

          end;
          NuovoSoggettoForm.Free;
     end;
end;

procedure TSelPersForm.dxDBGrid4CustomDrawCell(Sender: TObject;
     ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
     AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
     var AText: string; var AColor: TColor; AFont: TFont;
     var AAlignment: TAlignment; var ADone: Boolean);
var xFile: string;
begin
     // controllo mancanza CV
     if xCheckRicCandCV then begin
          if (Acolumn = dxDBGrid4CVNumero) then begin
               xFile := GetCVPath + '\i' + AText + 'a.gif';
               if not FileExists(xFile) then AFont.Color := clRed;
          end;
     end;
end;

procedure TSelPersForm.dxDBGrid4MouseUp(Sender: TObject;
     Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
     if (Button <> mbRight) or (Shift <> []) then Exit;
     TdxDBGridPopupMenuManager.Instance.ShowGridPopupMenu(Sender as TdxDBGrid);
end;

procedure TSelPersForm.MenuItem4Click(Sender: TObject);
begin
     if not LogEsportazioniCand(2) then exit;
     dxPrinter1.Preview(True, dxPrinter1Link3);
end;

procedure TSelPersForm.MenuItem5Click(Sender: TObject);
begin
     if not LogEsportazioniCand(2) then exit;
     Mainform.Save('xls', 'Microsoft Excel 4.0 Worksheet (*.xls)|*.xls', 'ExpGrid.xls', dxDBGrid4.SaveToXLS);
end;

procedure TSelPersForm.MenuItem6Click(Sender: TObject);
begin
     if not LogEsportazioniCand(2) then exit;
     Mainform.Save('htm', 'HTML File (*.htm; *.html)|*.htm', 'ExpGrid.htm', dxDBGrid4.SaveToHTML);
end;

//[TONI20021004]DEBUGOK

function TSelPersForm.CheckStoricoTL(xIDAzienda: integer): boolean;
begin
     // restituisce TRUE se procedere con l'associazione, FALSE se fermarsi
     StoricoTargetListForm := TStoricoTargetListForm.create(self);
     StoricoTargetListForm.QStoricoTL.ReloadSQL;
     StoricoTargetListForm.QStoricoTL.ParamByName['xIDCLiente'] := xIDAzienda;
     StoricoTargetListForm.QStoricoTL.Open;
     if StoricoTargetListForm.QStoricoTL.IsEmpty then begin
          Result := True;
          StoricoTargetListForm.Free;
          exit;
     end;
     StoricoTargetListForm.ShowModal;
     if StoricoTargetListForm.ModalResult = mrOk then result := True
     else result := False;
     StoricoTargetListForm.Free;
end;

procedure TSelPersForm.BTimeSheetNewClick(Sender: TObject);
begin
     DataRicerche.QRicTimeSheet.Insert;
end;

procedure TSelPersForm.BTimeSheetDelClick(Sender: TObject);
begin
     if not DataRicerche.QTimeRepDett.IsEmpty then begin
          ShowMessage('Ci sono causali di dettaglio: IMPOSSIBILE CANCELLARE');
          exit;
     end;
     if DataRicerche.QRicTimeSheet.IsEmpty then exit;
     if MessageDlg('Sei sicuro di voler eliminare la voce ?', mtWarning, [mbYes, mbNo], 0) = mrYes then
          DataRicerche.QRicTimeSheet.delete;
end;

procedure TSelPersForm.BTimeSheetOKClick(Sender: TObject);
begin
     DataRicerche.QRicTimeSheet.Post;
end;

procedure TSelPersForm.BTimeSheetCanClick(Sender: TObject);
begin
     DataRicerche.QRicTimeSheet.Cancel;
end;

procedure TSelPersForm.modificanumeroditelefono1Click(Sender: TObject);
var xTel: string;
     xID: integer;
begin
     xTel := DataRicerche.QRicTargetList.FieldByName('telefono').AsString;
     if not InputQuery('Modifica telefono azienda', 'nuovo numero:', xTel) then exit;
     with Data do begin
          DB.BeginTrans;
          try
               Q1.SQL.text := 'update EBC_Clienti set Telefono=:xTelefono:' +
                    'where ID=' + DataRicerche.QRicTargetList.FieldByName('IDCliente').asString;
               Q1.ParamByName['xTelefono'] := xTel;
               Q1.ExecSQL;
               DB.CommitTrans;
               xID := DataRicerche.QRicTargetList.FieldByName('ID').AsInteger;
               DataRicerche.QRicTargetList.Close;
               DataRicerche.QRicTargetList.Close;
               DataRicerche.QRicTargetList.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePendID.Value;
               DataRicerche.QRicTargetList.Open;
               DataRicerche.QRicTargetList.Locate('ID', xID, []);
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: operazione non effettuata', mtError, [mbOK], 0);
          end;
     end;
end;

//[TONI20021004]DEBUGOK

procedure TSelPersForm.BTLNoteClienteClick(Sender: TObject);
begin
     if DataRicerche.QRicTargetList.IsEmpty then exit;
     NoteClienteForm := TNoteClienteForm.create(self);
     NoteClienteForm.QNoteCliente.SQL.text := 'select ID,Notecontratto as Note from EBC_Clienti where ID=' + DataRicerche.QRicTargetList.FieldByName('IDCliente').AsString; //ALE 27/11/2002  sostituito sql.text ==> vecchio testo = select ID,Note from EBC_Clienti where ID=
     NoteClienteForm.QNoteCliente.Open;
     NoteClienteForm.ShowModal;
     NoteClienteForm.Free;
end;

procedure TSelPersForm.BTRepDettNewClick(Sender: TObject);
begin
     if DataRicerche.QRicTimeSheet.IsEmpty then exit;
     if DataRicerche.DsQRicTimeSheet.state in [dsInsert, dsEdit] then DataRicerche.QRicTimeSheet.Post;
     DataRicerche.QTimeRepDett.Insert;
end;

procedure TSelPersForm.BTRepDettDelClick(Sender: TObject);
begin
     if DataRicerche.QRicTimeSheet.IsEmpty then exit;
     if DataRicerche.QTimeRepDett.IsEmpty then exit;
     DataRicerche.QTimeRepDett.Delete;
end;

procedure TSelPersForm.BTRepDettOKClick(Sender: TObject);
begin
     DataRicerche.QTimeRepDett.Post;
end;

procedure TSelPersForm.BTRepDettCanClick(Sender: TObject);
begin
     DataRicerche.QTimeRepDett.Cancel;
end;

procedure TSelPersForm.BAggTotTRepClick(Sender: TObject);
var xTot: real;
begin
     xTot := 0;
     DataRicerche.QTimeRepDett.First;
     while not DataRicerche.QTimeRepDett.EOF do begin
          xTot := xTot + DataRicerche.QTimeRepDett.FieldByName('Ore').AsFloat;
          DataRicerche.QTimeRepDett.Next;
     end;
     DataRicerche.QRicTimeSheet.Edit;
     DataRicerche.QRicTimeSheet.FieldByName('OreConsuntivo').AsFloat := xTot;
     DataRicerche.QRicTimeSheet.Post;
end;

procedure TSelPersForm.ToolbarButton9711Click(Sender: TObject);
begin
     OpenTab('TimeSheetCausali', ['ID', 'Causale'], ['Causale']);
     DataRicerche.QTRepCausali.Close;
     DataRicerche.QTRepCausali.Open;
end;

function TSelPersForm.LogEsportazioniCand(xDaDove: integer): boolean;
var xCriteri: string;
begin
     // restituisce FALSE se l'utente ha abortito l'operazione
     Result := True;
     if MessageDlg('ATTENZIONE:  l''esportazione di dati � un''operazione soggetta a registrazione' + chr(13) +
          'secondo la normativa sulla privacy.  SEI SICURO DI VOLER PROSEGUIRE ?', mtWarning, [mbYes, mbNo], 0) = mrNo then begin
          Result := False;
          exit;
     end;
     with Data do begin
          DB.BeginTrans;
          try
               if xDaDove = 1 then begin
                    DataRicerche.QCandRic.First;
                    xCriteri := 'CANDIDATI NELLA COMMESSA/PROGETTO ' + DataRicerche.TRicerchePend.FieldByName('Progressivo').AsString + ': ' + chr(13);
                    while not DataRicerche.QCandRic.EOF do begin
                         xCriteri := xCriteri + DataRicerche.QCandRic.FieldByName('CVNumero').AsString + ' ' + DataRicerche.QCandRic.FieldByName('Cognome').AsString + ' ' + DataRicerche.QCandRic.FieldByName('Nome').AsString + chr(13);
                         DataRicerche.QCandRic.Next;
                    end;
               end else begin
                    DataRicerche.QTLCandidati.First;
                    xCriteri := 'CANDIDATI NELLA TARGET LIST per il cliente ' + DataRicerche.QRicTargetList.FieldByName('Azienda').Value + ' (commessa/progetto ' + DataRicerche.TRicerchePend.FieldByName('Progressivo').AsString + '): ' + chr(13);
                    while not DataRicerche.QTLCandidati.EOF do begin
                         xCriteri := xCriteri + DataRicerche.QTLCandidati.FieldByName('CVNumero').AsString + ' ' + DataRicerche.QTLCandidati.FieldByName('Cognome').AsString + ' ' + DataRicerche.QTLCandidati.FieldByName('Nome').AsString + chr(13);
                         DataRicerche.QTLCandidati.Next;
                    end;
               end;
               Q1.SQL.text := 'insert into LogEsportazCand (IDUtente,Data,Criteri,StringaSQL,ResultNum) ' +
                    ' values (:xIDUtente:,:xData:,:xCriteri:,:xStringaSQL:,:xResultNum:)';
               Q1.ParamByName['xIDUtente'] := MainForm.xIDUtenteAttuale;
               Q1.ParamByName['xData'] := Date;
               Q1.ParamByName['xCriteri'] := xCriteri;
               Q1.ParamByName['xStringaSQL'] := '';
               if xDaDove = 1 then
                    Q1.ParamByName['xResultNum'] := DataRicerche.QCandRic.RecordCount
               else Q1.ParamByName['xResultNum'] := DataRicerche.QTLCandidati.RecordCount;
               Q1.ExecSQL;

               DB.CommitTrans;
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: operazione non effettuata', mtError, [mbOK], 0);
          end;
     end;
end;

procedure TSelPersForm.modificaaziendaattualecandidato1Click(Sender: TObject);
var xID: integer;
begin
     SelClienteForm := TSelClienteForm.create(self);
     SelClienteForm.ShowModal;
     if SelClienteForm.ModalResult = mrOK then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Qtemp.Close;
                    QTemp.SQL.text := 'select count(*) Tot from EsperienzeLavorative ' +
                         ' where IDAnagrafica=:xIDAnagrafica: and Attuale=1';
                    QTemp.ParamByName['xIDAnagrafica'] := DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger;
                    Qtemp.Open;
                    if Qtemp.FieldByName('Tot').asInteger = 0 then
                         Q1.SQL.text := 'insert into EsperienzeLavorative (IDAnagrafica,IDAzienda,IDSettore,Attuale) ' +
                              ' values (:xIDAnagrafica:,:xIDAzienda:,:xIDSettore:,1)'
                    else
                         Q1.SQL.text := 'update EsperienzeLavorative set IDAzienda=:xIDAzienda:,IDSettore=:xIDSettore: ' +
                              ' where IDAnagrafica=:xIDAnagrafica: and Attuale=1';
                    Qtemp.Close;
                    QTemp.SQL.text := 'select IDAttivita from EBC_Clienti ' +
                         ' where ID=' + SelClienteForm.TClienti.FieldByName('ID').asString;
                    Qtemp.Open;
                    Q1.ParamByName['xIDAnagrafica'] := DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger;
                    Q1.ParamByName['xIDAzienda'] := SelClienteForm.TClienti.FieldByName('ID').AsInteger;
                    Q1.ParamByName['xIDSettore'] := QTemp.FieldByName('IDAttivita').asInteger;
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    xID := DataRicerche.QCandRic.FieldByName('ID').AsInteger;
                    DataRicerche.QCandRic.Close;
                    DataRicerche.QCandRic.Open;
                    DataRicerche.QCandRic.Locate('ID', xID, []);
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: operazione non effettuata', mtError, [mbOK], 0);
               end;
          end;
     end;
     SelClienteForm.Free;
end;

procedure TSelPersForm.modificanumeroditelefonodufficio1Click(Sender: TObject);
var xTel: string;
     xID: integer;
begin
     xTel := DataRicerche.QCandRic.FieldByName('telUfficio').AsString;
     if InputQuery('Modifica n� tel.ufficio', 'nuovo numero:', xTel) then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.SQL.text := 'update Anagrafica set TelUfficio=:xTelUfficio: ' +
                         ' where ID=:xID:';
                    Q1.ParamByName['xTelUfficio'] := xTel;
                    Q1.ParamByName['xID'] := DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger;
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    xID := DataRicerche.QCandRic.FieldByName('ID').AsInteger;
                    DataRicerche.QCandRic.Close;
                    DataRicerche.QCandRic.Open;
                    DataRicerche.QCandRic.Locate('ID', xID, []);
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: operazione non effettuata', mtError, [mbOK], 0);
               end;
          end;
     end;
end;

procedure TSelPersForm.modificanumeroditelefonocellulare1Click(Sender: TObject);
var xTel: string;
     xID: integer;
begin
     xTel := DataRicerche.QCandRic.FieldByName('Cellulare').AsString;
     if InputQuery('Modifica n� cellulare', 'nuovo numero:', xTel) then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.SQL.text := 'update Anagrafica set Cellulare=:xCellulare: ' +
                         ' where ID=:xID:';
                    Q1.ParamByName['xCellulare'] := xTel;
                    Q1.ParamByName['xID'] := DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger;
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    xID := DataRicerche.QCandRic.FieldByName('ID').AsInteger;
                    DataRicerche.QCandRic.Close;
                    DataRicerche.QCandRic.Open;
                    DataRicerche.QCandRic.Locate('ID', xID, []);
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: operazione non effettuata', mtError, [mbOK], 0);
               end;
          end;
     end;
end;

//[TONI20021004]DEBUGOK

procedure TSelPersForm.BAnagFileNewClick(Sender: TObject);
var xIDRicFile: integer;
     xFileNameSave: string;
begin
     RicFileForm := TRicFileForm.create(self);
     RicFileForm.FEFile.InitialDir := GetDocPath;
     if Data.Global.FieldByName('CheckCopiaFileAllegato').asString <> '' then begin
          if Data.Global.FieldByName('CheckCopiaFileAllegato').value then
               RicFileForm.FEFile.InitialDir := '';
     end;
     RicFileForm.ShowModal;
     if RicFileForm.ModalResult = mrOK then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text := 'insert into EBC_RicercheFile (IDRicerca,IDTipo,Descrizione) ' +
                         'values (:xIDRicerca:,:xIDTipo:,:xDescrizione:)';
                    Q1.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
                    Q1.ParamByName['xIDTipo'] := 0;
                    Q1.ParamByName['xDescrizione'] := RicFileForm.EDesc.text;
                    Q1.ExecSQL;

                    data.QTemp.close;
                    Data.QTemp.SQL.text := 'select @@IDENTITY as LastID';
                    Data.QTemp.Open;
                    xIDRicFile := Data.QTemp.FieldByName('LastID').asInteger;
                    Data.QTemp.Close;


                    if Data.Global.FieldByName('CheckCopiaFileAllegato').AsBoolean then
                         xFileNameSave := CopiaFileAll(RicFileForm.FEFile.filename, Data.Global.FieldByName('CheckRenameFileAllegato').AsBoolean, 'Commessa', xIDRicFile)
                    else
                         xFileNameSave := RicFileForm.FEFile.filename;

                    Q1.Close;
                    Q1.SQL.Text := 'update EBC_RicercheFile set nomefile = :xnomefile:, datacreazione = :xdatacreazione: where ID = ' + inttostr(xIDRicFile);
                    Q1.ParamByName['xNomeFile'] := xFileNameSave;
                    Q1.ParamByName['xDataCreazione'] := FileDateToDateTime(FileAge(RicFileForm.FEFile.filename));
                    Q1.ExecSQL;

                    DB.CommitTrans;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               end;
          end;
          QRicFile.Close;
          QRicFile.Open;
     end;
     RicFileForm.Free;
end;

//[TONI20021004]DEBUGOK

procedure TSelPersForm.BAnagFileModClick(Sender: TObject);
begin
     if QRicFile.IsEmpty then exit;
     RicFileForm := TRicFileForm.create(self);
     RicFileForm.EDesc.text := QRicFile.FieldByName('Descrizione').AsString;

     if (QRicFileNomeFile.asString <> '') or (Data.Global.FieldByName('CheckCopiaFileAllegato').asBoolean = TRUE) then begin
          RicFileForm.FEFile.Enabled := False;
          RicFileForm.FEFile.Color := clBtnFace;
     end;


     // COmmentato in data 11/10/10 poich� inutile fino a quando i documenti collegati a commessa non saranno messi nel DB
     //if Data.Global.FieldByName('AnagFileDentroDB').asBoolean = FALSE then begin
     RicFileForm.FEFile.FileName := QRicFile.FieldByName('NomeFile').AsString;
     //end;

     if pos('INTERMEDIA', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) > 0 then begin
          RicFileForm.FEFile.Text := '(non disponibile)';
     end;

     RicFileForm.ShowModal;
     if RicFileForm.ModalResult = mrOK then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text := 'update EBC_RicercheFile set IDTipo=:xIDTipo:,Descrizione=:xDescrizione:,NomeFile=:xNomeFile:' +
                         'where ID=' + QRicFile.fieldByName('ID').asString;
                    Q1.ParamByName['xIDTipo'] := 0;
                    Q1.ParamByName['xDescrizione'] := RicFileForm.EDesc.text;
                    if RicFileForm.FEFile.Text = '(non disponibile)' then
                         Q1.ParambyName['xNomeFile'] := QRicFile.FieldByName('NomeFile').AsString
                    else Q1.ParamByName['xNomeFile'] := RicFileForm.FEFile.filename;
                    Q1.ExecSQL;
                    DB.CommitTrans;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                    raise;
               end;
          end;
          QRicFile.Close;
          QRicFile.Open;
     end;
     RicFileForm.Free;
end;

//[TONI20021004]DEBUGOK

procedure TSelPersForm.BAnagFileDelClick(Sender: TObject);
var Xtesto, xnometemp: string;
     xidtemp: integer;
begin
     if QRicFile.IsEmpty then exit;
     xidtemp := QRicFile.FieldByName('ID').value;
     xnometemp := trim(QRicFile.FieldByName('NomeFile').value);
     Xtesto := '';
     if MessageDlg('Sei sicuro di voler eliminare l''associazione ?', mtWarning, [mbNo, mbYes], 0) <> mrYes then exit;
     if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 5)) <> 'ERGON' then begin
          if MessageDlg('Vuoi eliminare il file fisico ?', mtWarning, [mbNo, mbYes], 0) = mrYes then begin
               xtesto := '� stato premuto SI per eliminare il file fisico';
               DeleteFile(QRicFile.FieldByName('NomeFile').AsString);
          end;
     end;
     with Data do begin
          DB.BeginTrans;
          try
               Q1.Close;
               Q1.SQL.text := 'delete from EBC_RicercheFile where ID=' + QRicFile.FieldByName('ID').AsString;
               Q1.ExecSQL;

               if Xtesto = '' then xtesto := 'eliminaz normale solo associazione';
               xtesto := xtesto + ' rif=' + DBEdit1.Text;
               Log_Operation(MainForm.xIDUtenteAttuale, 'EBC_RicercheFile', xidtemp, 'D', xnometemp + ' = ' + xtesto);

               DB.CommitTrans;
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
          end;
     end;
     QRicFile.Close;
     QRicFile.Open;
end;

//[TONI20021004]DEBUGOK

procedure TSelPersForm.BFileCandOpenClick(Sender: TObject);
var xFileName: string;
begin
     xFileName := QRicFile.FieldByName('NomeFile').AsString;

     if MainForm.xSendMail_Remote then begin
          CopiaApriFileSulClient(xFileName, true);
     end else
          ShellExecute(0, 'Open', pchar(xFileName), '', '', SW_SHOWDEFAULT);
end;

procedure TSelPersForm.CBOpzioneCand2Click(Sender: TObject);
begin
     DataRicerche.QTLCandidati.Close;
     //DataRicerche.QTLCandidati.ParamByName['xIDTargetList'] := DataRicerche.QRicTargetListID.Value;
     //DataRicerche.QTLCandidati.ParamByName['xIDAzienda'] := DataRicerche.QRicTargetListIDCliente.Value;
     DataRicerche.QTLCandidati.Open;
end;

procedure TSelPersForm.DBGAnnunciCandCustomDrawCell(Sender: TObject;
     ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
     AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
     var AText: string; var AColor: TColor; AFont: TFont;
     var AAlignment: TAlignment; var ADone: Boolean);
begin
     if Data.Global.fieldbyname('debughrms').VAlue <> TRue then begin
          if ((AColumn = DBGAnnunciCandCognome) or (AColumn = DBGAnnunciCandNome)) and (CBColoriCandAnnunci.Checked) then begin
               if ANode.Values[DBGAnnunciCandIDCandRic.index] > 0 then
                    Afont.Color := clRed
               else Afont.Color := clWindowText;
               if ANode.Values[DBGAnnunciCandVisionato.index] = FALSE then
                    AFont.Style := AFont.Style + [fsBold]
               else
                    AFont.Style := AFont.Style - [fsBold];
          end else begin
               if ANode.Values[DBGAnnunciCandVisionato.index] = FALSE then
                    AFont.Style := AFont.Style + [fsBold]
               else
                    AFont.Style := AFont.Style - [fsBold];
               Afont.Color := clWindowText;
          end;
          if ASelected then afont.Color := clWhite;
     end;
end;

procedure TSelPersForm.BTLDelSoggClick(Sender: TObject);
var xTabFiglie: string;
     xCognome, xNome, xCVNumero, xNatoA, xNatoIl, xCVInseritoIndata, xCVDataAgg: string;
begin
     if not MainForm.CheckProfile('001') then Exit;
     if DataRicerche.QTLCandidati.IsEmpty then exit
     else begin
          if MessageDlg('ATTENZIONE: la procedura � IRREVERSIBILE e DEFINITIVA !!' + chr(13) +
               'E'' necessario verificare il rispetto della normativa sulla Privacy e sulla' + chr(13) +
               'conservazione dei relativi dati.' + chr(13) + chr(13) +
               'SEI DAVVERO SICURO DI VOLER PROCEDERE ?', mtWarning,
               [mbNo, mbYes], 0) = mrNo then exit;

          if MessageDlg('Sei proprio sicuro di voler eliminare il soggetto selezionato ?', mtWarning,
               [mbNo, mbYes], 0) = mrYes then begin

               xTabFiglie := '';
               Data.Q1.Close;
               Data.Q1.SQL.text := 'select count(*) TOT from AnagCaratteristiche where IDAnagrafica=' + DataRicerche.QTLCandidati.FieldByName('IDAnagrafica').asString;
               Data.Q1.Open;
               if Data.Q1.fieldByName('TOT').asInteger > 0 then xTabFiglie := xTabFiglie + '- Caratteristiche' + chr(13);
               Data.Q1.Close;
               Data.Q1.SQL.text := 'select count(*) TOT from AnagFile where IDAnagrafica=' + DataRicerche.QTLCandidati.FieldByName('IDAnagrafica').asString;
               Data.Q1.Open;
               if Data.Q1.fieldByName('TOT').asInteger > 0 then xTabFiglie := xTabFiglie + '- File collegati al soggetto' + chr(13);
               Data.Q1.Close;
               Data.Q1.SQL.text := 'select count(*) TOT from AnagIncompClienti where IDAnagrafica=' + DataRicerche.QTLCandidati.FieldByName('IDAnagrafica').asString;
               Data.Q1.Open;
               if Data.Q1.fieldByName('TOT').asInteger > 0 then xTabFiglie := xTabFiglie + '- Incompatibilit� con clienti' + chr(13);
               Data.Q1.Close;
               Data.Q1.SQL.text := 'select count(*) TOT from AnagMansioni where IDAnagrafica=' + DataRicerche.QTLCandidati.FieldByName('IDAnagrafica').asString;
               Data.Q1.Open;
               if Data.Q1.fieldByName('TOT').asInteger > 0 then xTabFiglie := xTabFiglie + '- Ruoli/mansioni' + chr(13);
               Data.Q1.Close;
               Data.Q1.SQL.text := 'select count(*) TOT from AnagValutaz where IDAnagrafica=' + DataRicerche.QTLCandidati.FieldByName('IDAnagrafica').asString;
               Data.Q1.Open;
               if Data.Q1.fieldByName('TOT').asInteger > 0 then xTabFiglie := xTabFiglie + '- Valutazione' + chr(13);
               Data.Q1.Close;
               Data.Q1.SQL.text := 'select count(*) TOT from CompetenzeAnagrafica where IDAnagrafica=' + DataRicerche.QTLCandidati.FieldByName('IDAnagrafica').asString;
               Data.Q1.Open;
               if Data.Q1.fieldByName('TOT').asInteger > 0 then xTabFiglie := xTabFiglie + '- Competenze' + chr(13);
               Data.Q1.Close;
               Data.Q1.SQL.text := 'select count(*) TOT from EBC_CandidatiRicerche where IDAnagrafica=' + DataRicerche.QTLCandidati.FieldByName('IDAnagrafica').asString;
               Data.Q1.Open;
               if Data.Q1.fieldByName('TOT').asInteger > 0 then xTabFiglie := xTabFiglie + '- Commesse/progetti' + chr(13);
               Data.Q1.Close;
               Data.Q1.SQL.text := 'select count(*) TOT from EBC_ContattiCandidati where IDAnagrafica=' + DataRicerche.QTLCandidati.FieldByName('IDAnagrafica').asString;
               Data.Q1.Open;
               if Data.Q1.fieldByName('TOT').asInteger > 0 then xTabFiglie := xTabFiglie + '- Contatti relativi a commesse/progetti' + chr(13);
               Data.Q1.Close;
               Data.Q1.SQL.text := 'select count(*) TOT from EsperienzeLavorative where IDAnagrafica=' + DataRicerche.QTLCandidati.FieldByName('IDAnagrafica').asString;
               Data.Q1.Open;
               if Data.Q1.fieldByName('TOT').asInteger > 0 then xTabFiglie := xTabFiglie + '- Esperienze lavorative' + chr(13);
               Data.Q1.Close;
               Data.Q1.SQL.text := 'select count(*) TOT from LingueConosciute where IDAnagrafica=' + DataRicerche.QTLCandidati.FieldByName('IDAnagrafica').asString;
               Data.Q1.Open;
               if Data.Q1.fieldByName('TOT').asInteger > 0 then xTabFiglie := xTabFiglie + '- Lingue conosciute' + chr(13);
               Data.Q1.Close;
               Data.Q1.SQL.text := 'select count(*) TOT from Organigramma where IDDipendente=' + DataRicerche.QTLCandidati.FieldByName('IDAnagrafica').asString;
               Data.Q1.Open;
               if Data.Q1.fieldByName('TOT').asInteger > 0 then xTabFiglie := xTabFiglie + '- Organigramma' + chr(13);
               Data.Q1.Close;
               Data.Q1.SQL.text := 'select count(*) TOT from Storico where IDAnagrafica=' + DataRicerche.QTLCandidati.FieldByName('IDAnagrafica').asString;
               Data.Q1.Open;
               if Data.Q1.fieldByName('TOT').asInteger > 0 then xTabFiglie := xTabFiglie + '- Storico' + chr(13);
               Data.Q1.Close;
               Data.Q1.SQL.text := 'select count(*) TOT from TitoliStudio where IDAnagrafica=' + DataRicerche.QTLCandidati.FieldByName('IDAnagrafica').asString;
               Data.Q1.Open;
               if Data.Q1.fieldByName('TOT').asInteger > 0 then xTabFiglie := xTabFiglie + '- Titoli di studio' + chr(13);

               if xTabFiglie <> '' then begin
                    if MessageDlg('ATTENZIONE:  risultano collegati i seguenti dati:' + chr(13) +
                         xTabFiglie + 'SEI SICURO DI VOLER PROSEGUIRE ?', mtWarning, [mbYes, mbNo], 0) = mrNo then
                         exit;
               end;

               if MessageDlg('Verranno eliminati TUTTI i dati di TUTTE le tabelle collegate' + chr(13) +
                    'IMPORTANTE:  non sar� pi� possibile recuperarli - CONTINUARE ?', mtWarning,
                    [mbNo, mbYes], 0) = mrYes then begin
                    Data.DB.BeginTrans;
                    try
                         // eliminare tabelle collegate da CONSTRAINT di integrit� referenziale
                         Data.Q1.Close;
                         Data.Q1.SQL.Clear;
                         Data.Q1.SQL.Add('delete from AnagAltreInfo where IDAnagrafica=' + DataRicerche.QTLCandidati.FieldByName('IDAnagrafica').asString);
                         Data.Q1.ExecSQL;
                         Data.Q1.SQL.Clear;
                         Data.Q1.SQL.Add('delete from AnagCaratteristiche where IDAnagrafica=' + DataRicerche.QTLCandidati.FieldByName('IDAnagrafica').asString);
                         Data.Q1.ExecSQL;
                         Data.Q1.SQL.Clear;
                         Data.Q1.SQL.Add('delete from AnagFile where IDAnagrafica=' + DataRicerche.QTLCandidati.FieldByName('IDAnagrafica').asString);
                         Data.Q1.ExecSQL;
                         Data.Q1.SQL.Clear;
                         Data.Q1.SQL.Add('delete from AnagIncompClienti where IDAnagrafica=' + DataRicerche.QTLCandidati.FieldByName('IDAnagrafica').asString);
                         Data.Q1.ExecSQL;
                         Data.Q1.SQL.Clear;
                         Data.Q1.SQL.Add('delete from AnagMansioni where IDAnagrafica=' + DataRicerche.QTLCandidati.FieldByName('IDAnagrafica').asString);
                         Data.Q1.ExecSQL;
                         Data.Q1.SQL.Clear;
                         Data.Q1.SQL.Add('delete from AnagValutaz where IDAnagrafica=' + DataRicerche.QTLCandidati.FieldByName('IDAnagrafica').asString);
                         Data.Q1.ExecSQL;
                         Data.Q1.SQL.Clear;
                         Data.Q1.SQL.Add('delete from CompetenzeAnagrafica where IDAnagrafica=' + DataRicerche.QTLCandidati.FieldByName('IDAnagrafica').asString);
                         Data.Q1.ExecSQL;
                         Data.Q1.SQL.Clear;
                         Data.Q1.SQL.Add('delete from EBC_CandidatiRicerche where IDAnagrafica=' + DataRicerche.QTLCandidati.FieldByName('IDAnagrafica').asString);
                         Data.Q1.ExecSQL;
                         Data.Q1.SQL.Clear;
                         Data.Q1.SQL.Add('delete from EBC_ContattiCandidati where IDAnagrafica=' + DataRicerche.QTLCandidati.FieldByName('IDAnagrafica').asString);
                         Data.Q1.ExecSQL;
                         Data.Q1.SQL.Clear;
                         Data.Q1.SQL.Add('delete from EsperienzeLavorative where IDAnagrafica=' + DataRicerche.QTLCandidati.FieldByName('IDAnagrafica').asString);
                         Data.Q1.ExecSQL;
                         Data.Q1.SQL.Clear;
                         Data.Q1.SQL.Add('delete from LingueConosciute where IDAnagrafica=' + DataRicerche.QTLCandidati.FieldByName('IDAnagrafica').asString);
                         Data.Q1.ExecSQL;
                         Data.Q1.SQL.Clear;
                         Data.Q1.SQL.Add('delete from Organigramma where IDDipendente=' + DataRicerche.QTLCandidati.FieldByName('IDAnagrafica').asString);
                         Data.Q1.ExecSQL;
                         Data.Q1.SQL.Clear;
                         Data.Q1.SQL.Add('delete from Storico where IDAnagrafica=' + DataRicerche.QTLCandidati.FieldByName('IDAnagrafica').asString);
                         Data.Q1.ExecSQL;
                         Data.Q1.SQL.Clear;
                         Data.Q1.SQL.Add('delete from TitoliStudio where IDAnagrafica=' + DataRicerche.QTLCandidati.FieldByName('IDAnagrafica').asString);
                         Data.Q1.ExecSQL;
                         Data.Q1.SQL.Clear;
                         Data.Q1.SQL.Add('delete from Anagrafica where ID=' + DataRicerche.QTLCandidati.FieldByName('IDAnagrafica').asString);
                         Data.Q1.ExecSQL;

                         // log cancellazione
                         xCognome := DataRicerche.QTLCandidati.FieldByName('Cognome').AsString;
                         xNome := DataRicerche.QTLCandidati.FieldByName('Nome').AsString;
                         xCVNumero := DataRicerche.QTLCandidati.FieldByName('CVNumero').AsString;
                         xNatoIl := DataRicerche.QTLCandidati.FieldByName('DataNascita').AsString;
                         Log_Operation(MainForm.xIDUtenteAttuale, 'Anagrafica', DataRicerche.QTLCandidati.FieldByName('IDAnagrafica').AsInteger, 'D', xCVNumero + ' ' + xCognome + ' ' + xNome + ', nato a ' + xNatoIl);

                         Data.DB.CommitTrans;
                         DataRicerche.QTLCandidati.Close;
                         //DataRicerche.QTLCandidati.ParamByName['xIDTargetList'] := DataRicerche.QRicTargetListID.Value;
                         //DataRicerche.QTLCandidati.ParamByName['xIDAzienda'] := DataRicerche.QRicTargetListIDCliente.Value;
                         DataRicerche.QTLCandidati.Open;
                    except
                         Data.DB.RollbackTrans;
                         MessageDlg('Errore nel database: cancellazione non eseguita', mtError, [mbOK], 0);
                         raise;
                    end;
               end;
          end;
     end;
end;

procedure TSelPersForm.CBColoriCandAnnunciClick(Sender: TObject);
begin
     DataRicerche.QCandRic.Close;
     DataRicerche.QCandRic.Open;
end;

procedure TSelPersForm.dxDBGrid3ChangeColumn(Sender: TObject;
     Node: TdxTreeListNode; Column: Integer);
begin
     xColonnaSel := Column;
end;

procedure TSelPersForm.dxDBGrid3ContextPopup(Sender: TObject;
     MousePos: TPoint; var Handled: Boolean);
begin
     (*     if dxDBGrid3.columns[xColonnaSel + 1].Caption = 'telefono' then
               modificanumeroditelefono1.Enabled := True
          else modificanumeroditelefono1.Enabled := False;

          if dxDBGrid3.columns[xColonnaSel + 1].Caption = 'Note' then
               modificanote2.Enabled := True
          else modificanote2.Enabled := False;          *)
end;

procedure TSelPersForm.BTabCausaliClick(Sender: TObject);
begin
     OpenTab('EBC_RicercheCausali', ['ID', 'Causale'], ['Causale']);
     DataRicerche.QCausaliLK.Close;
     DataRicerche.QCausaliLK.Open;
end;

//[ALBERTO 20020902]FINE

procedure TSelPersForm.QRicFileBeforeOpen(DataSet: TDataSet);
begin
     QRicFile.SQL.text := 'select * from EBC_RicercheFile where IDRicerca=' + DataRicerche.TRicerchePendID.AsString;
end;

procedure TSelPersForm.dxDBGrid4ContextPopup(Sender: TObject;
     MousePos: TPoint; var Handled: Boolean);
begin
     if DataRicerche.QTLCandidati.IsEmpty then begin
          modificatelefonocasa1.Enabled := False;
          modificanote1.Enabled := False;
     end else begin
          modificatelefonocasa1.Enabled := True;
          modificanote1.Enabled := True;
     end;
     {if dxDBGrid4.columns[xColonnaSel2].Caption='Tel.casa' then
          modificatelefonocasa1.Enabled:=True
     else modificatelefonocasa1.Enabled:=False;
     if dxDBGrid4.columns[xColonnaSel2].Caption='Note' then
          modificanote1.Enabled:=True
     else modificanote1.Enabled:=False;}
end;

procedure TSelPersForm.fa(Sender: TObject;
     Node: TdxTreeListNode; Column: Integer);
begin
     xColonnaSel2 := Column;
end;

procedure TSelPersForm.modificatelefonocasa1Click(Sender: TObject);
var xTel: string;
     xID: integer;
begin
     xTel := DataRicerche.QTLCandidati.FieldByName('RecapitiTelefonici').AsString;
     if not InputQuery('Modifica telefono casa', 'nuovo numero:', xTel) then exit;
     with Data do begin
          DB.BeginTrans;
          try
               Q1.SQL.text := 'update Anagrafica set RecapitiTelefonici=:xTelefono:' +
                    'where ID=' + DataRicerche.QTLCandidati.FieldByName('IDAnagrafica').asString;
               Q1.ParamByName['xTelefono'] := xTel;
               Q1.ExecSQL;
               DB.CommitTrans;
               xID := DataRicerche.QTLCandidati.FieldByName('ID').AsInteger;
               DataRicerche.QTLCandidati.Close;
               //DataRicerche.QTLCandidati.ParamByName['xIDTargetList'] := DataRicerche.QRicTargetListID.Value;
               //DataRicerche.QTLCandidati.ParamByName['xIDAzienda'] := DataRicerche.QRicTargetListIDCliente.Value;
               DataRicerche.QTLCandidati.Open;
               DataRicerche.QTLCandidati.Locate('ID', xID, []);
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: operazione non effettuata', mtError, [mbOK], 0);
          end;
     end;
end;

procedure TSelPersForm.modificanote1Click(Sender: TObject);
var xNote: string;
     xID: integer;
begin
     xNote := DataRicerche.QTLCandidati.FieldByName('Note').AsString;
     //if not InputQuery('Modifica note (commessa/progetto)', 'nuove note:', xNote) then exit;
     LeggiNoteForm := TLeggiNoteForm.Create(self);
     LeggiNoteForm.mNote.Text := xNote;
     LeggiNoteForm.modificabile := true;
     LeggiNoteForm.ShowModal;
     if legginoteform.modalresult = mrOK then begin
          xNote := LeggiNoteForm.mNote.Text;
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.SQL.text := 'update EBC_CandidatiRicerche set Note2=:xNote: ' +
                         'where IDAnagrafica=' + DataRicerche.QTLCandidati.FieldByName('IDAnagrafica').asString + ' ' +
                         'and IDRicerca=' + DataRicerche.TRicerchePendID.AsString;
                    Q1.ParamByName['xNote'] := xNote;
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    xID := DataRicerche.QTLCandidati.FieldByName('ID').AsInteger;
                    DataRicerche.QTLCandidati.Close;
                    //DataRicerche.QTLCandidati.ParamByName['xIDTargetList'] := DataRicerche.QRicTargetListID.Value;
                    //DataRicerche.QTLCandidati.ParamByName['xIDAzienda'] := DataRicerche.QRicTargetListIDCliente.Value;
                    DataRicerche.QTLCandidati.Open;
                    DataRicerche.QTLCandidati.Locate('ID', xID, []);
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: operazione non effettuata', mtError, [mbOK], 0);
               end;
          end;
     end;
     LeggiNoteForm.Free;
end;

procedure TSelPersForm.modificadescrizposizione1Click(Sender: TObject);
var xNote: string;
     xID: integer;
begin
     //showmessage(    DataRicerche.QTLCandidati.SQL.text);
     xNote := DataRicerche.QTLCandidati.FieldByName('DescrizioneMansione').AsString;
     if not InputQuery('Modifica descrizione posizione', 'nuovo testo:', xNote) then exit;
     with Data do begin
          DB.BeginTrans;
          try
               Q1.SQL.text := 'update EsperienzeLavorative set DescrizioneMansione=:xDescrizioneMansione: ' +
                    'where ID=' + DataRicerche.QTLCandidati.FieldByName('IDEspLav').asString;
               Q1.ParamByName['xDescrizioneMansione'] := xNote;
               Q1.ExecSQL;
               DB.CommitTrans;
               xID := DataRicerche.QTLCandidati.FieldByName('ID').AsInteger;
               DataRicerche.QTLCandidati.Close;
               //DataRicerche.QTLCandidati.ParamByName['xIDTargetList'] := DataRicerche.QRicTargetListID.Value;
               //DataRicerche.QTLCandidati.ParamByName['xIDAzienda'] := DataRicerche.QRicTargetListIDCliente.Value;
               DataRicerche.QTLCandidati.Open;
               DataRicerche.QTLCandidati.Locate('ID', xID, []);
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: operazione non effettuata', mtError, [mbOK], 0);
          end;
     end;
end;

procedure TSelPersForm.BCLImportStdClick(Sender: TObject);
begin
     // importazioni voci standard della CheckList
     // --> lo fa per tutte quelle voci non ancora inserite
     Q.Close;
     Q.SQL.text := 'select * from CLCommessaVociStandard';
     Q.Open;
     Data.QTemp.Close;
     while not Q.EOF do begin
          if not DataRicerche.QCommessaCL.Locate('IDVoceStandard', Q.FieldByName('ID').asInteger, []) then begin
               Data.QTemp.SQL.text := 'insert into CLCommessaVoci (IDRicerca,IDVoceStandard,Voce) ' +
                    ' values (' + DataRicerche.TRicerchePendID.asString + ',' + Q.FieldByName('ID').asString + ',''' + Q.FieldByName('VoceStandard').asString + ''')';
               Data.QTemp.ExecSQL;
          end;
          Q.Next;
     end;
     Q.Close;
     DataRicerche.QCommessaCL.Close;
     DataRicerche.QCommessaCL.Open;
end;

procedure TSelPersForm.BCLNewClick(Sender: TObject);
begin
     DataRicerche.QCommessaCL.Insert;
end;

procedure TSelPersForm.BCLDelClick(Sender: TObject);
begin
     if MessageDlg('Sei sicuro di voler eliminare la voce selezionata ?', mtWarning, [mbNo, mbYes], 0) <> mrYes then exit;
     DataRicerche.QCommessaCL.Delete;
end;

procedure TSelPersForm.BCLOKClick(Sender: TObject);
begin
     DataRicerche.QCommessaCL.Post;
end;

procedure TSelPersForm.BCLCanClick(Sender: TObject);
begin
     DataRicerche.QCommessaCL.Cancel;
end;

procedure TSelPersForm.dxDBGrid2Editing(Sender: TObject;
     Node: TdxTreeListNode; var Allow: Boolean);
begin
     if Node.Values[dxDBGrid2IDVoceStandard.index] <> null then
          Allow := False;
end;

procedure TSelPersForm.BCLVociStandardClick(Sender: TObject);
begin
     // gestione voci standard
     OpenTab('CLCommessaVociStandard', ['ID', 'VoceStandard'], ['voce standard']);
end;

procedure TSelPersForm.dxDBGrid2CustomDrawCell(Sender: TObject;
     ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
     AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
     var AText: string; var AColor: TColor; AFont: TFont;
     var AAlignment: TAlignment; var ADone: Boolean);
begin
     if ANode.Values[dxDBGrid2IDVoceStandard.index] <> null then
          AFont.Color := clBlue;
end;

procedure TSelPersForm.BSaveToExcelTRClick(Sender: TObject);
begin
     if not LogEsportazioniCand(1) then exit;
     Mainform.Save('xls', 'Microsoft Excel 4.0 Worksheet (*.xls)|*.xls', 'ExpTimeReport.xls', dxDBGrid5.SaveToXLS);
end;

procedure TSelPersForm.dfd1Click(Sender: TObject);
const xQLingue = 'select l.lingua,livelloconoscenza ' +
     'from lingueconosciute lc ' +
          'left join lingue l ' +
          'on l.id=lc.idlingua ' +
          'left join livellolingue ll ' +
          'on ll.id=lc.livello  ' +
          'where lc.idanagrafica = :xidanagrafica';
const xQTitoliStudio = 'select d.descrizione ' +
     'from titolistudio ts ' +
          'join diplomi d  ' +
          'on d.id=ts.iddiploma ' +
          'where ts.idanagrafica = :xidanagrafica';
const xQEspLav = 'select aziendasede,aziendacomune, ' +
     'aziendaprovincia,descrizionemansione,livellocontrattuale as Livello, ' +
          'retribnettamensile as Ral, Attuale ' +
          'from esperienzelavorative el ' +
          'where el.idanagrafica = :xidanagrafica ' +
          'order by annodal desc ,meseal desc ';
const xQueryAltriDati = 'select Inquadramento,DispContratto ' +
     'from anagaltreinfo ai left join anagdispcontratti dc ' +
          'join dispcontratti c ' +
          'on c.id=dc.iddispcontratto ' +
          'on ai.idanagrafica = dc.idanagrafica ' +
          'where ai.idanagrafica = :xidanagrafica';

var MsExcel: variant;
     xRiga: integer;
     xQueryE, xQueryT, xQueryL, xQueryA: TADOQuery;
     xTitoliStudio, xLingue, xSede, xAltriDati: string;
begin

     try MsExcel := CreateOleObject('Excel.application');
     except
          ShowMessage('Non riesco ad aprire Microsoft Excel.');
          exit;
     end;
     xTitoliStudio := '';
     xLingue := '';
     xSede := '';
     xAltriDati := '';

     xQueryE := TADOQuery.Create(self);
     xQueryT := TADOQuery.Create(self);
     xQueryL := TADOQuery.Create(self);
     xQueryA := TADOQuery.Create(self);

     xQueryE.Connection := Data.DB;
     xQueryT.Connection := Data.DB;
     xQueryL.Connection := Data.DB;
     xQueryA.Connection := Data.DB;

     xQueryE.SQL.Text := xQEspLav;
     xQueryT.SQL.Text := xQTitoliStudio;
     xQueryL.SQL.Text := xQLingue;
     xQueryA.SQL.Text := xQueryAltriDati;

     MsExcel.visible := true;
     MsExcel.workbooks.Open(Data.Global.FieldByName('DirFileDoc').AsString + '\ExpTargetList.xls');
     DataRicerche.QRicTargetList.First;
     MsExcel.activeworkbook.activesheet.cells[1, 8] := DateToStr(Date);

     MsExcel.activeworkbook.activesheet.cells[2, 3] := dbedit5.Text;
     MsExcel.activeworkbook.activesheet.cells[2, 5] := dbedit13.Text;
     xRiga := 4;
     while not DataRicerche.QRicTargetList.Eof do begin
          DataRicerche.QTLCandidati.First;
          MsExcel.activeworkbook.activesheet.cells[xRiga, 1] := DataRicerche.QRicTargetList.FieldByName('Azienda').AsString; //'uno';
          MsExcel.activeworkbook.activesheet.cells[xRiga, 2] := DataRicerche.QRicTargetList.FieldByname('Telefono').AsString;
          MsExcel.activeworkbook.activesheet.cells[xRiga, 3] := DataRicerche.QRicTargetList.FieldByname('Note').AsString;
          MsExcel.activeworkbook.activesheet.cells[xRiga, 14] := DataRicerche.QRicTargetList.FieldByname('Comune').AsString;
          if DataRicerche.QTLCandidati.RecordCount > 0 then begin
               while not DataRicerche.QTLCandidati.Eof do begin
                    xTitoliStudio := '';
                    xLingue := '';
                    xSede := '';
                    xAltriDati := '';

                    xQueryE.Close;
                    xQueryT.Close;
                    xQueryL.Close;
                    xQueryA.Close;


                    xQueryE.Parameters[0].Value := DataRicerche.QTLCandidatiIDAnagrafica.Value;
                    xQueryT.Parameters[0].Value := DataRicerche.QTLCandidatiIDAnagrafica.Value;
                    xQueryL.Parameters[0].Value := DataRicerche.QTLCandidatiIDAnagrafica.Value;
                    xQueryA.Parameters[0].Value := DataRicerche.QTLCandidatiIDAnagrafica.Value;

                    xQueryE.Open;
                    xQueryT.Open;
                    xQueryL.Open;
                    xQueryA.Open;


                    if xQueryE.Locate('Attuale', 1, []) = false then begin
                         xQueryE.First;
                    end;


                    MsExcel.activeworkbook.activesheet.cells[xRiga, 1] := DataRicerche.QRicTargetList.FieldByName('Azienda').AsString; //RAY 1107
                    MsExcel.activeworkbook.activesheet.cells[xRiga, 4] := DataRicerche.QTLCandidati.FieldByname('Cognome').AsString + ' ' + DataRicerche.QTLCandidati.FieldByname('Nome').AsString; //'tre';
                    MsExcel.activeworkbook.activesheet.cells[xRiga, 5] := DataRicerche.QTLCandidati.FieldByname('RuoloAttuale').AsString;
                    MsExcel.activeworkbook.activesheet.cells[xRiga, 6] := xQueryE.FieldByname('descrizionemansione').AsString;
                    MsExcel.activeworkbook.activesheet.cells[xRiga, 7] := DataRicerche.QTLCandidati.FieldByname('Eta').AsString; //'cinque';
                    MsExcel.activeworkbook.activesheet.cells[xRiga, 8] := DataRicerche.QTLCandidati.FieldByname('Cellulare').AsString;
                    MsExcel.activeworkbook.activesheet.cells[xRiga, 9] := DataRicerche.QTLCandidati.FieldByname('Note').AsString; //'sei';

                    //titoli studio
                    if xQueryT.RecordCount > 0 then begin
                         while not xQueryT.Eof do begin
                              xTitoliStudio := xTitoliStudio + xQueryT.FieldByname('descrizione').AsString + #10;
                              xQueryT.Next;
                         end;
                         xTitoliStudio := copy(xTitoliStudio, 1, length(xTitoliStudio) - 1);
                    end else
                         xTitoliStudio := ' - ';

                    //Lingue
                    if xQueryL.RecordCount > 0 then begin
                         while not xQueryL.Eof do begin
                              if xQueryL.FieldByname('livelloconoscenza').AsString <> '' then
                                   xLingue := xLingue + xQueryL.FieldByname('lingua').AsString + ' - ' + xQueryL.FieldByname('livelloconoscenza').AsString + #10
                              else
                                   xLingue := xLingue + xQueryL.FieldByname('lingua').AsString + #10;

                              // MsExcel.activeworkbook.activesheet.cells[xRiga, 12] := ' - ' + MsExcel.activeworkbook.activesheet.cells[xRiga, 12].Text  + xQueryL.FieldByname('livelloconoscenza').AsString;

                              xQueryL.Next;
                         end;
                         xLingue := copy(xLingue, 1, length(xLingue) - 1);
                    end else
                         xLingue := ' - ';



                    //Altri dati

                    if xQueryA.RecordCount > 0 then begin

                         MsExcel.activeworkbook.activesheet.cells[xRiga, 10] := xQueryE.FieldByname('Livello').AsString + ' - ' + xQueryE.FieldByname('Ral').AsString;
                         MsExcel.activeworkbook.activesheet.cells[xRiga, 11] := xQueryA.FieldByname('Inquadramento').AsString + ' - ' + xQueryA.FieldByname('DispContratto').AsString;

                         //xAltriDati := copy(xAltriDati, 1, length(xAltriDati) - 1);
                    end else begin
                         MsExcel.activeworkbook.activesheet.cells[xRiga, 10] := ' - ';
                         MsExcel.activeworkbook.activesheet.cells[xRiga, 11] := ' - ';
                    end;

                    MsExcel.activeworkbook.activesheet.cells[xRiga, 12] := xTitoliStudio;
                    MsExcel.activeworkbook.activesheet.cells[xRiga, 13] := xLingue;

                    // sede azienda

                    //if  xQueryE.FieldByName('aziendasede').AsString <>

                    //MsExcel.activeworkbook.activesheet.cells[xRiga, 13] := xQueryE.FieldByName().AsString;
                    //MsExcel.activeworkbook.activesheet.cells[xRiga, 14] := DataRicerche.QRicTargetList.FieldByname('Comune').AsString;
                    DataRicerche.QTLCandidati.Next;
                    xRiga := xRiga + 1;
               end;
          end else
               xRiga := xRiga + 1;
          DataRicerche.QRicTargetList.Next;
          //xRiga := xRiga + 1;
     end;

     //MainForm.SaveDialog.FileName := Data.Global.FieldByName('DirFileDoc').AsString + 'ExpTargetList'+DataRicerche.TRicerchePendProgressivo.AsString+'.xls';
     //MainForm.SaveDialog.Options := [ofReadOnly];
     //MainForm.SaveDialog.InitialDir := Data.Global.FieldByName('DirFileDoc').AsString;
     //MainForm.SaveDialog.FileName := DataRicerche.TRicerchePendProgressivo.AsString+'.xls';
     //MainForm.SaveDialog.Execute;
     MsExcel.activeworkbook.SaveAs(Data.Global.FieldByName('DirFileDoc').AsString + '\ExpTargetList' + ' ' + DataRicerche.TRicerchePendProgressivo.AsString + '.xls');
     //ShowMessage(MainForm.SaveDialog.FileName);
     //MainForm.SaveDialog.Free;
     xQueryE.Close;
     xQueryT.Close;
     xQueryL.Close;
     xQueryA.Close;

     xQueryE.Free;
     xQueryT.Free;
     xQueryL.Free;
     xQueryA.Free;
end;

procedure TSelPersForm.dxDBGrid4Column15Validate(Sender: TObject;
     var ErrorText: string; var Accept: Boolean);
begin
     //if DataRicerche.QTLCandidati.State = dsEdit then DataRicerche.QTLCandidati.Post;
end;

procedure TSelPersForm.BTLCandInsRicClick(Sender: TObject);
begin
     Data.Q1.Close;
     Data.Q1.SQL.Text := 'select * from EBC_Candidatiricerche where InsFromTL = 1 ' +
          'and ID = ' + DataRicerche.QTLCandidati.FieldByName('ID').AsString;
     Data.Q1.Open;
     if Data.Q1.RecordCount = 0 then begin
          Data.Q1.Close;
          Data.Q1.SQL.Text := 'Update EBC_CandidatiRicerche set InsFromTL = 1 ' +
               'where ID = ' + DataRicerche.QTLCandidati.FieldByName('ID').AsString;
          Data.Q1.ExecSQL;
          ShowMessage('Inserito candidato in commessa/progetto');
          DataRicerche.QCandRic.Close;
          DataRicerche.QCandRic.Open;
     end else
          ShowMessage('Candidato gi� inserito all''interno della commessa/progetto');
end;

procedure TSelPersForm.NascondiFunzioni;
begin
     // Naaconde le funzioni:
     // --> funziona solo con alcune
     TSRicParams.TabVisible := MainForm.VerificaFunzLicenzeNew('307') = true;
     TSTimeSheet.TabVisible := MainForm.VerificaFunzLicenzeNew('308') = true;
     TSGestQ.TabVisible := MainForm.VerificaFunzLicenzeNew('309') = true;
     TSTargetList.TabVisible := MainForm.VerificaFunzLicenzeNew('310') = true;
     TSFile.TabVisible := MainForm.VerificaFunzLicenzeNew('311') = true;
     TSValutaz.TabVisible := MainForm.VerificaFunzLicenzeNew('316') = true;
end;

procedure TSelPersForm.dxDBGrid4Edited(Sender: TObject;
     Node: TdxTreeListNode);
begin
     if DataRicerche.DsQTLCandidati.State = dsEdit then DataRicerche.QTLCandidati.Post;
end;

procedure TSelPersForm.dxDBGrid3Edited(Sender: TObject;
     Node: TdxTreeListNode);
begin
     if DataRicerche.DsQRicTargetList.State = dsEdit then DataRicerche.QRicTargetList.Post;
end;

procedure TSelPersForm.inverticampoVisibilealcliente1Click(
     Sender: TObject);
var xID: integer;
begin
     with Data do begin
          DB.BeginTrans;
          try
               xID := DataRicerche.QCandRic.FieldByName('ID').AsInteger;
               Q1.SQL.text := 'update EBC_CandidatiRicerche set VisibleCliente=:xVisibleCliente: ' +
                    ' where ID=:xID:';
               if DataRicerche.QCandRicVisibleCliente.Value = true then
                    Q1.ParamByName['xVisibleCliente'] := False
               else
                    Q1.ParamByName['xVisibleCliente'] := True;
               Q1.ParamByName['xID'] := xID;
               Q1.ExecSQL;
               DB.CommitTrans;

               DataRicerche.QCandRic.Close;
               DataRicerche.QCandRic.Open;
               DataRicerche.QCandRic.Locate('ID', xID, []);
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: operazione non effettuata', mtError, [mbOK], 0);
          end;
     end;

end;


procedure TSelPersForm.Gestionetabstatiperclienti1Click(Sender: TObject);
begin
     OpenTab('EBC_RicercheStatiCliente', ['ID', 'Descrizione'], ['Stato candidato per il cliente']);
     DataRicerche.QStatiCliente.Close;
     DataRicerche.QStatiCliente.open;
end;

procedure TSelPersForm.modificastatoperilcliente1Click(Sender: TObject);
var xIDTemp: integer;
begin
     if MessageDlg('Vuoi procedere con l''eliminazione dello stato attuale?', mtWarning, [mbYes, mbNo], 0) = mrYes then begin
          Data.Q1.Close;
          Data.Q1.SQL.Text := 'Update EBC_CandidatiRicerche set IDStatoCliente = NULL ' +
               'where id = ' + DataRicerche.QCandRicID.AsString;
          Data.Q1.ExecSQL;

          xIDTemp := DataRicerche.QCandRicID.Value;

          DataRicerche.QCandRic.Close;
          DataRicerche.QCandRic.Open;
          DataRicerche.QCandRic.Locate('ID', xIDTemp, []);
     end;
end;

procedure TSelPersForm.dxDBGrid1StatoClienteChange(Sender: TObject);
begin
     if DataRicerche.DsQCandRic.State = dsEdit then DataRicerche.QCandRic.Post;
     if DataRicerche.DsQCandRic.State = dsEdit then DataRicerche.QCandRic.Post;
end;

procedure WMHack(var message: TMessage); //: message WM_USER + 1024;
var
     i: Integer;
     j: Integer;
begin
     {  for i := 0 to Screen.FormCount - 1 do
         showmessage(inttostr(Screen.FormCount));
       if Screen.Forms[i].Caption = 'Forzatura blocco livello 2' then
       begin
         for j := 0 to Screen.Forms[i].ControlCount - 1 do
           if Screen.Forms[i].Controls[j] is TEdit then
           begin
             TEdit(Screen.Forms[i].Controls[j]).PasswordChar := '*';
             TEdit(Screen.Forms[i].Controls[j]).SelectAll;
             Exit;
           end;
       end;   }

end;

procedure TSelPersForm.dxDBGrid1NoteEditButtonClick(Sender: TObject);
begin
     // controllo se � bloccato
    { if DataRicerche.qCandRicBloccato.Value then begin
          MessageDlg('Il record � bloccato per una modifica da parte di un altro utente - impossibile modificare', mtError, [mbOK], 0);
          Abort;
     end else begin
          DataRicerche.QCandRic.edit;
          DataRicerche.QCandRicBloccato.Value := True;
     end;  }
end;

procedure TSelPersForm.Impostazionecolorigriglia1Click(Sender: TObject);
begin
     SettaColoriGrigliaCandForm := TSettaColoriGrigliaCandForm.create(self);
     SettaColoriGrigliaCandForm.ShowModal;
     SettaColoriGrigliaCandForm.Free;
end;

procedure TSelPersForm.Colorariga1Click(Sender: TObject);
var xIDTemp: integer;
begin
     xIDTemp := DataRicerche.qCandRicID.Value;
     if ColorDialog1.Execute then begin
          Colorariga(Mainform.xIDUtenteAttuale, ColorDialog1.Color, xIDTemp, 'IDCandRicerche');
          DataRicerche.qCandric.Close;
          DataRicerche.qCandric.Open;

          DataRicerche.QCandRic.Locate('ID', xIDTemp, []);
     end;
end;

procedure TSelPersForm.SpeedButton8Click(Sender: TObject);
begin
     OpenTab('EBC_RicUtentiRuoli', ['ID', 'RuoloRic'], ['Ruolo']);
     DataRicerche.QRicUtenti.Close;
     DataRicerche.QRicUtenti.Open;
end;

procedure TSelPersForm.ImpostaSoloLettura;
begin
     dxDBGrid1.OptionsBehavior := [edgoAutoSort, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoStoreToRegistry, edgoTabThrough, edgoVertThrough];
     //Panel5.enabled := False;;
     ToolbarButton9713.enabled := False; ;
     TbBAltroNome.enabled := False; ;
     ToolbarButton9712.enabled := False; ;
     BElimina.enabled := False; ;
     BRecupera.enabled := False; ;
     ToolbarButton974.enabled := False; ;
     TSCandAnnunci.enabled := False; ;
     //TSCliCand.enabled := False; ;
     TSRicParams.enabled := False; ;
     TSTimeSheet.enabled := False; ;
     TSGestQ.enabled := False; ;
     TSAltriDati.enabled := False; ;
     TSTargetList.enabled := False; ;
     //TSFile.enabled := False; ;
     BAnagFileNew.enabled := False;
     BAnagFileDel.enabled := False;
     BAnagFileMod.enabled := False;
     GroupBox7.enabled := False; ;
     GroupBox5.enabled := False; ;
     GroupBox6.enabled := False; ;
     GBCliente.enabled := False; ;
     GroupBox4.enabled := False; ;
     GroupBox3.enabled := False; ;
     GroupBox1.enabled := False; ;
     ToolbarButton975.enabled := False;


     SelPersForm.Caption := 'Gestione del progetto/commessa - SOLA LETTURA';
end;

procedure TSelPersForm.Panel7Exit(Sender: TObject);
begin
     if DataRicerche.DsQCandRic.state = dsEdit then DataRicerche.QCandRic.Post;
end;

procedure TSelPersForm.expModelloExcelClick(Sender: TObject);
var

     xFileName, xdata, xora: string;
begin

     if not LogEsportazioniCand(1) then exit;
     //if POS('INTERMEDIA', Uppercase(Data.Global.FieldByName('NomeAzienda').Value)) > 0 then begin
     xData := DateToStr(Date);
     xOra := TimeToStr(Time);

     if Data.Global.FieldByName('CheckReNameFileAllegato').asBoolean = TRUE then begin
          xFileName := DbEdit1.Text + '_' + DbEdit5.Text + '_' + DbEdit4.Text + '_' + xData + '_' + xOra;


          xFileName := StringReplace(xFileName, '/', '', [rfReplaceAll]);
          xFileName := StringReplace(xFileName, '\', '', [rfReplaceAll]);
          xFileName := StringReplace(xFileName, '.', '', [rfReplaceAll]);
          xFileName := StringReplace(xFileName, ':', '', [rfReplaceAll]);
          xFileName := GetPathFromTable('Commessa') + xFileName + '.xls';
          //showmessage( xFileName);
           //xFileName := GetPathFromTable('Commessa') + 'FileAle.xls';

          try
               ExpMappaturainModello(GetDocPath + '\ExpMappModel.xls', xFileName);

               Data.Q1.Close;
               Data.Q1.SQL.text := 'insert into EBC_RicercheFile (IDRicerca,IDTipo,Descrizione,NomeFile,DataCreazione) ' +
                    'values (:xIDRicerca:,:xIDTipo:,:xDescrizione:,:xNomeFile:,:xDataCreazione:)';
               Data.Q1.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
               Data.Q1.ParamByName['xIDTipo'] := 0;
               Data.Q1.ParamByName['xDescrizione'] := 'Esportazione ricerca ' + DbEdit1.Text + ' (' + datetostr(Date) + ')';
               Data.Q1.ParamByName['xNomeFile'] := xFileName;
               Data.Q1.ParamByName['xDataCreazione'] := Date;
               Data.Q1.ExecSQL;
          except
          end;




     end else begin
          MainForm.SaveDialog.Execute;
          xFileName := MainForm.SaveDialog.FileName;
          ExpMappaturainModello(GetDocPath + '\ExpMappModel.xls', xFileName);


          try
               ExpMappaturainModello(GetDocPath + '\ExpMappModel.xls', xFileName);

               Data.Q1.Close;
               Data.Q1.SQL.text := 'insert into EBC_RicercheFile (IDRicerca,IDTipo,Descrizione,NomeFile,DataCreazione) ' +
                    'values (:xIDRicerca:,:xIDTipo:,:xDescrizione:,:xNomeFile:,:xDataCreazione:)';
               Data.Q1.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
               Data.Q1.ParamByName['xIDTipo'] := 0;
               Data.Q1.ParamByName['xDescrizione'] := 'Esportazione ricerca ' + DbEdit1.Text + ' (' + datetostr(Date) + ')';
               Data.Q1.ParamByName['xNomeFile'] := MainForm.SaveDialog.FileName;
               Data.Q1.ParamByName['xDataCreazione'] := Date;
               Data.Q1.ExecSQL;
          except
          end;

     end;


     try
          if MainForm.xSendMail_Remote then begin
               CopiaApriFileSulClient(xFileName, true);
          end else
               ShellExecute(0, 'Open', pchar(xFileName), '', '', SW_SHOWDEFAULT);
     except

     end;
     // end else
       //    Mainform.Save('xls', 'Microsoft Excel 4.0 Worksheet (*.xls)|*.xls', 'ExpGrid.xls', dxDBGrid1.SaveToXLS)

end;



procedure TSelPersForm.ExpMappaturainModello(xNomeModello: string; var xFileName: string);
var MsExcel: variant;
begin
     try
          MsExcel := CreateOleObject('Excel.application');
     except
          ShowMessage('Non riesco ad aprire Microsoft Excel.');
          exit;
     end;

     MsExcel.WorkBooks.Open(xNomeModello);
     DataRicerche.QCandRic.First;
     while not DataRicerche.QCandRic.Eof do begin
          MsExcel.Activeworkbook.activesheet.cells[DataRicerche.QCandRic.RecNo + 1, 1] := DataRicerche.qCandRicAzienda.Value;
          MsExcel.Activeworkbook.activesheet.cells[DataRicerche.QCandRic.RecNo + 1, 2] := DataRicerche.qCandRicCognome.Value;
          MsExcel.Activeworkbook.activesheet.cells[DataRicerche.QCandRic.RecNo + 1, 3] := DataRicerche.qCandRicNome.Value;

          MsExcel.Activeworkbook.activesheet.cells[DataRicerche.QCandRic.RecNo + 1, 4] := DataRicerche.qCandRicEta.Value;
          MsExcel.Activeworkbook.activesheet.cells[DataRicerche.QCandRic.RecNo + 1, 5] := DataRicerche.qCandRicJobTitle.Value;
          MsExcel.Activeworkbook.activesheet.cells[DataRicerche.QCandRic.RecNo + 1, 6] := DataRicerche.qCandRicnote.Value;

          MsExcel.Activeworkbook.activesheet.cells[DataRicerche.QCandRic.RecNo + 1, 7] := DataRicerche.qCandRicRetribTotale.value;
          MsExcel.Activeworkbook.activesheet.cells[DataRicerche.QCandRic.RecNo + 1, 8] := DataRicerche.qCandRicStato.Value;

          MsExcel.Activeworkbook.activesheet.cells[DataRicerche.QCandRic.RecNo + 1, 9] := DataRicerche.qCandRicCVNumero.Value;
          MsExcel.Activeworkbook.activesheet.cells[DataRicerche.QCandRic.RecNo + 1, 10] := DataRicerche.qCandRicCellulare.Value;
          MsExcel.Activeworkbook.activesheet.cells[DataRicerche.QCandRic.RecNo + 1, 11] := DataRicerche.qCandRicTelUfficio.Value;
          MsExcel.Activeworkbook.activesheet.cells[DataRicerche.QCandRic.RecNo + 1, 12] := DataRicerche.qCandRicRecapitiTelefonici.Value;
          MsExcel.Activeworkbook.activesheet.cells[DataRicerche.QCandRic.RecNo + 1, 13] := DataRicerche.qCandRicDataUltimoContatto.AsString;
          MsExcel.Activeworkbook.activesheet.cells[DataRicerche.QCandRic.RecNo + 1, 14] := DataRicerche.qCandRicStatoCliente.Value;
          MsExcel.Activeworkbook.activesheet.cells[DataRicerche.QCandRic.RecNo + 1, 15] := DataRicerche.qCandRicTelAzienda.Value;
          MsExcel.Activeworkbook.activesheet.cells[DataRicerche.QCandRic.RecNo + 1, 16] := DataRicerche.qCandRicTipo.Value;

          MsExcel.Activeworkbook.activesheet.cells[DataRicerche.QCandRic.RecNo + 1, 17] := DataRicerche.qCandRicInquadramento.Value;
          MsExcel.Activeworkbook.activesheet.cells[DataRicerche.QCandRic.RecNo + 1, 18] := DataRicerche.qCandRicRuolo.Value;
          MsExcel.Activeworkbook.activesheet.cells[DataRicerche.QCandRic.RecNo + 1, 19] := DataRicerche.qCandRicDiploma.Value;


          MsExcel.Activeworkbook.activesheet.cells[DataRicerche.QCandRic.RecNo + 1, 20] := DataRicerche.qCandRicDataNascita.asString;
          MsExcel.Activeworkbook.activesheet.cells[DataRicerche.QCandRic.RecNo + 1, 21] := DataRicerche.qCandRicDataImpegno.asString;
          MsExcel.Activeworkbook.activesheet.cells[DataRicerche.QCandRic.RecNo + 1, 22] := DataRicerche.qCandRicDatains.AsString;
          MsExcel.Activeworkbook.activesheet.cells[DataRicerche.QCandRic.RecNo + 1, 23] := DataRicerche.qCandRicNoteCliente.Value;
          MsExcel.Activeworkbook.activesheet.cells[DataRicerche.QCandRic.RecNo + 1, 24] := DataRicerche.qCandRicRetribuzione.Value;
          MsExcel.Activeworkbook.activesheet.cells[DataRicerche.QCandRic.RecNo + 1, 25] := DataRicerche.qCandRicRetribTotale.VAlue;


          MsExcel.Activeworkbook.activesheet.cells[DataRicerche.QCandRic.RecNo + 1, 26] := DataRicerche.qCandRicVisibleCliente.AsString;
          MsExcel.Activeworkbook.activesheet.cells[DataRicerche.QCandRic.RecNo + 1, 27] := DataRicerche.qCandRicFlagAccPres.AsString;


          DataRicerche.QCandRic.Next;
     end;
     MSExcel.ActiveWorkBook.SaveAs(xFileName);
     MSExcel.WorkBooks.Close;
end;

procedure TSelPersForm.StoricoGeneraleCommesse1Click(Sender: TObject);
begin
     data.QStorComm.close;
     data.QStorComm.Parameters.ParamByName('xIDRicerca').value := Dataricerche.TRicerchePend.FieldByName('ID').AsInteger;
     data.QStorComm.Parameters.ParamByName('xidanagrafica').value := DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger;
     data.QStorComm.open;
     StoricoAnagForm := TStoricoAnagForm.create(self);
     StoricoAnagForm.caption := 'Storico Commessa di ' + data.QStorComm.fieldbyname('candidato').asString;
     StoricoAnagForm.showModal;
end;

procedure TSelPersForm.BStatoSecModClick(Sender: TObject);
var xIDRic: integer;
     xID: string;
begin
     xIDRic := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;

     xID := OpenSelFromTab('EBC_StatiRicSecondari', 'ID', 'StatoSecondario', 'Stato secondario', DataRicerche.TRicerchePendIDStatoSecondario.asString);
     if xID = '' then exit;

     Data.Q1.Close;
     Data.Q1.SQL.text := 'update EBC_Ricerche set IDStatoSecondario=:x: where ID=' + DataRicerche.TRicerchePend.FieldByName('ID').asString;
     Data.Q1.ParamByName['x'] := StrToInt(xID);
     Data.Q1.ExecSQL;

     Data.Q1.Close;
     Data.Q1.SQL.text := 'Insert into EBC_StoricoRic (IDRicerca,IDStatoRic,DallaData,IDStatoSecondario) ' +
          'values (:xIDRicerca:,:xIDStatoRic:,:xDallaData:,:xIDStatoSecondario:)';
     Data.Q1.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
     Data.Q1.ParamByName['xIDStatoRic'] := DataRicerche.TRicerchePend.FieldByName('IDStatoRic').AsInteger;
     Data.Q1.ParamByName['xDallaData'] := Date;
     Data.Q1.ParamByName['xIDStatoSecondario'] := StrToInt(xID);
     Data.Q1.ExecSQL;


     CaricaApriRicPend(xIDRic);
end;

procedure TSelPersForm.SpeedButton9Click(Sender: TObject);
begin
     ApriHelp('21');
end;

procedure TSelPersForm.Colorariga2Click(Sender: TObject);
var xIDTemp: integer;
begin
     {xIDTemp := DataRicerche.QAnnunciCandid
     if ColorDialog1.Execute then begin
          Colorariga(Mainform.xIDUtenteAttuale, ColorDialog1.Color, xIDTemp, 'IDCandRicerche');
          DataRicerche.qCandric.Close;
          DataRicerche.qCandric.Open;

          DataRicerche.QCandRic.Locate('ID', xIDTemp, []);
     end;}
end;

procedure TSelPersForm.SpeedButton10Click(Sender: TObject);
begin
     IndirizziClienteForm := TIndirizziClienteForm.Create(self);
     IndirizziCLienteForm.xIdCliente := DataRicerche.TRicerchePendIDCliente.Value;
     IndirizziClienteForm.ShowModal;
     if IndirizziClienteForm.ModalResult = mrOK then begin
          DataRicerche.TRicerchePend.Edit;
          DataRicerche.TRicerchePend.FieldByName('IDIndirizzoCliente').AsInteger := IndirizziClienteForm.qClienteIndirizziID.value;
          with DataRicerche.TRicerchePend do begin
               Data.DB.BeginTrans;
               try
                    post;
                    Data.DB.CommitTrans;
                    CaricaApriRicPend(DataRicerche.QRicAttive.FieldByName('ID').AsInteger);
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                    raise;
               end;
          end;
     end;
     IndirizziClienteForm.Free;
end;

procedure TSelPersForm.SpeedButton11Click(Sender: TObject);
begin
     DataRicerche.TRicerchePend.Edit;
     DataRicerche.TRicerchePend.FieldByName('IDIndirizzoCliente').AsInteger := 0;
     with DataRicerche.TRicerchePend do begin
          Data.DB.BeginTrans;
          try
               post;
               Data.DB.CommitTrans;
               CaricaApriRicPend(DataRicerche.QRicAttive.FieldByName('ID').AsInteger);
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               raise;
          end;
     end;
end;

procedure TSelPersForm.DBGAnnunciCandContextPopup(Sender: TObject;
     MousePos: TPoint; var Handled: Boolean);
begin
     if DataRicerche.qAnnunciCandVisionato.Value = TRUE then
          Segnacome1.Caption := 'Segna come da rivedere'
     else
          Segnacome1.Caption := 'Segna come gi� visto';
end;

procedure TSelPersForm.Segnacome1Click(Sender: TObject);
var xIDLoc: integer;
begin

     if POS('RIVEDERE', UpperCase(Segnacome1.Caption)) > 0 then begin
          Data.QTemp.Close;
          Data.QTemp.SQL.Text := 'update ann_anagannedizdata set Visionato = 0 ' +
               'where idanagrafica = :xIDAnagrafica: and idannedizdata = :xIDAnnEdizData:';
          Data.QTemp.ParamByName['xIDAnagrafica'] := DataRicerche.qAnnunciCandIDAnagrafica.Value;
          Data.QTemp.ParamByName['xIDAnnEdizData'] := DataRicerche.qAnnunciCandIDAnnEdizData.Value;
          Data.QTemp.ExecSQL;

          xIDLoc := DataRicerche.QAnnunciCand.FieldByname('IDAnagrafica').AsInteger;
          DataRicerche.QAnnunciCand.Close;
          DataRicerche.QAnnunciCand.Open;
          DataRicerche.QAnnunciCand.Locate('IDAnagrafica', xIDLoc, []);
     end else begin
          Data.QTemp.Close;
          Data.QTemp.SQL.Text := 'update ann_anagannedizdata set Visionato = 1 ' +
               'where idanagrafica = :xIDAnagrafica: and idannedizdata = :xIDAnnEdizData:';
          Data.QTemp.ParamByName['xIDAnagrafica'] := DataRicerche.qAnnunciCandIDAnagrafica.Value;
          Data.QTemp.ParamByName['xIDAnnEdizData'] := DataRicerche.qAnnunciCandIDAnnEdizData.Value;
          Data.QTemp.ExecSQL;

          xIDLoc := DataRicerche.QAnnunciCand.FieldByname('IDAnagrafica').AsInteger;
          DataRicerche.QAnnunciCand.Close;
          DataRicerche.QAnnunciCand.Open;
          DataRicerche.QAnnunciCand.Locate('IDAnagrafica', xIDLoc, []);
     end;
end;

procedure TSelPersForm.bSelOffertaClick(Sender: TObject);

begin
     //xidofferta := OpenSelFromQuery('set dateformat dmy select ID,''Rif. ''+Rif+'' del '' + cast(Data as varchar (11) ) Rif from ebc_offerte where idcliente = ' + DataRicerche.TRicerchePend.FieldByName('IDCliente').AsString , 'ID', 'Rif', 'Riferimento', 'ID');
     xidoffertatemp := OpenSelFromQuery('set dateformat dmy select ID,Rif from ebc_offerte where idcliente = ' + DataRicerche.TRicerchePend.FieldByName('IDCliente').AsString, 'ID', 'Rif', 'Riferimento', 'ID', false);
     ErifOfferta.Text := Mainform.xgenericString;
end;

procedure TSelPersForm.bApriFileOffertaClick(Sender: TObject);
var xgetfilepath, xFileWord: string;
begin
     xgetfilepath := GetPathFromTable('Offerta');

     if xgetfilepath = '' then
          xFileWord := GetDocPath + '\o' + xidoffertatemp + '.doc'
     else
          xFileWord := xgetfilepath + xidoffertatemp + '.doc';


     if FileExists(xFileWord) then begin
          // file da aprire
          if MainForm.xSendMail_Remote then begin
               CopiaApriFileSulClient(xFileWord, true);
          end else
               ShellExecute(0, 'Open', pchar(xFileWord), '', '', SW_SHOWDEFAULT);
     end else begin
          MessageDlg('File non ancora generato per l''offerta!', mtInformation, [mbOk], 0);
     end;
end;


procedure TSelPersForm.Ita1Click(Sender: TObject);
var xgetfilepath, xFileWord: string;
begin
     xgetfilepath := GetPathFromTable('OffertaK2P');

     if xgetfilepath = '' then
          xFileWord := GetDocPath + '\oK2P' + xidoffertatemp + '_ita.doc'
     else
          xFileWord := xgetfilepath + xidoffertatemp + '_ita.doc';


     if FileExists(xFileWord) then begin
          // file da aprire
          if MainForm.xSendMail_Remote then begin
               CopiaApriFileSulClient(xFileWord, true);
          end else
               ShellExecute(0, 'Open', pchar(xFileWord), '', '', SW_SHOWDEFAULT);
     end else
          MessageDlg('Il file non esiste:' + chr(13) + xFileWord, mtError, [mbOK], 0);
end;

procedure TSelPersForm.Eng1Click(Sender: TObject);
var xgetfilepath, xFileWord: string;
begin
     xgetfilepath := GetPathFromTable('OffertaK2P');

     if xgetfilepath = '' then
          xFileWord := GetDocPath + '\oK2P' + xidoffertatemp + '_eng.doc'
     else
          xFileWord := xgetfilepath + xidoffertatemp + '_eng.doc';


     if FileExists(xFileWord) then
          // file da aprire
          if MainForm.xSendMail_Remote then begin
               CopiaApriFileSulClient(xFileWord, true);
          end else
               ShellExecute(0, 'Open', pchar(xFileWord), '', '', SW_SHOWDEFAULT)
     else
          MessageDlg('Il file non esiste:' + chr(13) + xFileWord, mtError, [mbOK], 0);

end;

procedure TSelPersForm.Ita2Click(Sender: TObject);
var xgetfilepath, xFileWord: string;
begin
     xgetfilepath := GetPathFromTable('OffertaIS');

     if xgetfilepath = '' then
          xFileWord := GetDocPath + '\oIS' + xidoffertatemp + '_ita.doc'
     else
          xFileWord := xgetfilepath + xidoffertatemp + '_ita.doc';


     if FileExists(xFileWord) then
          // file da aprire
          ShellExecute(0, 'Open', pchar(xFileWord), '', '', SW_SHOWDEFAULT)
     else
          MessageDlg('Il file non esiste:' + chr(13) + xFileWord, mtError, [mbOK], 0);

end;

procedure TSelPersForm.Eng2Click(Sender: TObject);
var xgetfilepath, xFileWord: string;
begin
     xgetfilepath := GetPathFromTable('OffertaIS');

     if xgetfilepath = '' then
          xFileWord := GetDocPath + '\oIS' + xidoffertatemp + '_eng.doc'
     else
          xFileWord := xgetfilepath + xidoffertatemp + '_eng.doc';


     if FileExists(xFileWord) then
          // file da aprire
          ShellExecute(0, 'Open', pchar(xFileWord), '', '', SW_SHOWDEFAULT)
     else
          MessageDlg('Il file non esiste:' + chr(13) + xFileWord, mtError, [mbOK], 0);
end;

procedure TSelPersForm.Modificanote2Click(Sender: TObject);
var xNote: string;
     xID: integer;
begin
     xNote := DataRicerche.QRicTargetList.fieldbyname('note').asstring;
     LeggiNoteForm := TLeggiNoteForm.Create(self);
     LeggiNoteForm.mNote.Text := xNote;
     LeggiNoteForm.modificabile := true;
     LeggiNoteForm.ShowModal;
     if legginoteform.modalresult = mrOK then begin
          xNote := LeggiNoteForm.mNote.Text;
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.SQL.text := 'update EBC_RicercheTargetList set Note=:xNote: ' +
                         ' where ID=' + dataricerche.QRicTargetList.fieldbyname('ID').asstring;
                    Q1.ParamByName['xNote'] := xNote;
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    xID := dataricerche.QRicTargetList.fieldbyname('ID').asInteger;
                    dataricerche.QRicTargetList.Close;
                    DataRicerche.QRicTargetList.Close;
                    DataRicerche.QRicTargetList.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePendID.Value;
                    DataRicerche.QRicTargetList.Open;
                    dataricerche.QRicTargetList.Locate('ID', xID, []);
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: operazione non effettuata', mtError, [mbOK], 0);
               end;
          end;
     end;
     LeggiNoteForm.Free;
end;

procedure TSelPersForm.bImpostaPesiClick(Sender: TObject);
begin
     RicCandAnnunciForm := TRicCandAnnunciForm.Create(self);
     if data.Global.fieldbyname('debughrms').asboolean then showmessage('IDRICERCA= ' + IntToStr(xidricercaint));
     RicCandAnnunciForm.xidRicerca := xIDRicercaInt;
     RicCandAnnunciForm.xidAnnuncio := 0;
     RicCandAnnunciForm.ShowModal;
     RicCandAnnunciForm.Free;

     PCSelPersChange(self);
end;

procedure TSelPersForm.BValutazExportClick(Sender: TObject);
begin
     Mainform.Save('xls', 'Microsoft Excel 4.0 Worksheet (*.xls)|*.xls', 'ValutazCand.xls', dxDBGrid9.SaveToXLS);
end;

procedure TSelPersForm.BTabellaVociClick(Sender: TObject);
begin
     TabVociValutazCandForm := TTabVociValutazCandForm.create(self);
     TabVociValutazCandForm.xIDRicerca := DataRicerche.TRicerchePendID.Value;
     TabVociValutazCandForm.ShowModal;
     if TabVociValutazCandForm.xNewVoce then begin
          if MessageDlg('Sono state create nuove voci. Vuoi rivedere la valutazione ?', mtInformation, [mbYes, mbNo], 0) = mrYes then begin
               BValutazNewClick(self);
          end;
     end;
     TabVociValutazCandForm.Free;
end;

procedure TSelPersForm.BValutazOKClick(Sender: TObject);
begin
     DataRicerche.QValutazCand.Post;
end;

procedure TSelPersForm.BValutazCanClick(Sender: TObject);
begin
     DataRicerche.QValutazCand.Cancel;
end;

procedure TSelPersForm.BValutazNewClick(Sender: TObject);
begin
     NuovaValutazCandForm := TNuovaValutazCandForm.create(self);
     NuovaValutazCandForm.xIDRicerca := DataRicerche.TRicerchePendID.Value;
     NuovaValutazCandForm.xIDUtente := MainForm.xIDUtenteAttuale;
     NuovaValutazCandForm.ShowModal;
     NuovaValutazCandForm.Free;

     DataRicerche.QValutazCand.Close;
     DataRicerche.QValutazCand.Open;
end;

procedure TSelPersForm.BValutazDelClick(Sender: TObject);
begin
     if DataRicerche.qvalutazcand.IsEmpty then exit;
     if MessageDlg('Sei sicuro di voler eliminare la riga selezionata ?', mtWarning, [mbNo, mbYes], 0) <> mrYes then exit;
     Data.Q1.close;
     Data.Q1.SQL.clear;
     Data.Q1.SQL.text := 'delete from EBC_CandRic_Valutaz where ID=' + DataRicerche.qvalutazcandiD.AsString;
     Data.Q1.ExecSQL;

     DataRicerche.QValutazCand.Close;
     DataRicerche.QValutazCand.Open;
end;

procedure TSelPersForm.Button1Click(Sender: TObject);
begin
     DataRicerche.QValutazCand.close;
     dataricerche.QValutazCand.Open;
end;

procedure TSelPersForm.ToolbarButton9710Click(Sender: TObject);
begin
     TabVociValutazCandStdForm := TTabVociValutazCandStdForm.create(self);
     TabVociValutazCandStdForm.ShowModal;
     TabVociValutazCandStdForm.Free;
end;

procedure TSelPersForm.ToolbarButton9714Click(Sender: TObject);
begin
     // importazioni voci standard della CheckList
     // --> lo fa per tutte quelle voci non ancora inserite
     Q.Close;
     Q.SQL.text := 'insert into EBC_Ricerche_valutaz_voci (IDRicerca,Voce,Legenda,IDVoceStandard) ' +
          'select ' + DataRicerche.TRicerchePendID.asString + ',Voce,Legenda,ID ' +
          'from EBC_Ricerche_valutaz_voci_std ' +
          'where ID not in ( ' +
          '	select IDVoceStandard ' +
          '	from EBC_Ricerche_valutaz_voci ' +
          '    where IDRicerca=' + DataRicerche.TRicerchePendID.asString +
          '    and IDVoceStandard is not null)';
     try
          Q.ExecSQL;
     finally
          ShowMessage('Voci standard importate');
     end;
     DataRicerche.QValutazCand.Close;
     DataRicerche.QValutazCand.Open;
end;

procedure TSelPersForm.WebBrowser1DocumentComplete(Sender: TObject;
     const pDisp: IDispatch; var URL: OleVariant);
begin
     BCalcolaDistanza.Enabled := true;
end;

function TogliWhiteSpace(str: string): string;
var
     i: integer;
begin
     for i := 0 to (length(str) - 1) do
          if (str[i] = ' ') then
               str[i] := '+';
     Result := str;
end;


procedure TSelPersForm.BCalcolaDistanzaClick(Sender: TObject);
var //IndirizzoCliente, IndirizzoAna: string;
     i, idtemp, xid: integer;
     km, tempo, a, xstatus: string;
     xStartLat, xStartLng, xEndLat, xEndLng: string;
     km1, tempo1: extended;
     xlogDistanza: TStringList;
begin
     //  ShowMessage(QRes1ID.asstring);
     //  for i := 0 to dxDBGrid1.SelectedCount - 1 do begin
     //       dataricerche.qCandRic.Bookmark := dxDBGrid1.SelectedRows[i];
     xlogDistanza := TStringList.Create;
     if DataRicerche.TRicerchePendIDIndirizzoCliente.AsString = '' then begin
          //MessageDlg('Attenzione, Nessun indirizzo cliente impostato nella commessa', MTWarning, [mbOk], 0);
          MessageDlg('    Attenzione, Impossibile Calcolare la Distanza' + chr(13) +
               'L''indirizzo del Cliente potrebbe non essere Impostato nella Commessa.', MTWarning, [mbOk], 0);
          exit;
     end;
     // Memo1.Lines.Add('idaangrafica= ' + dataricerche.qCandRicIDAnagrafica.AsString);
     DataRicerche.QDistanza.close;
     DataRicerche.QDistanza.SQL.Text := 'select tipostrada,comune,indirizzo,cap,provincia,numcivico from EBC_ClienteIndirizzi ' +
          ' where id=' + DataRicerche.TRicerchePendIDIndirizzoCliente.AsString;
     DataRicerche.QDistanza.Open;
     IndirizzoCliente := '';
     if not DataRicerche.QDistanza.Eof then begin
          if DataRicerche.QDistanza.fieldbyname('comune').asstring <> '' then begin
               IndirizzoCliente := DataRicerche.QDistanza.fieldbyname('comune').asstring;
               if (dataricerche.QDistanza.fieldbyname('tipostrada').asstring <> '') and
                    (dataricerche.QDistanza.fieldbyname('indirizzo').asstring <> '') then
                    IndirizzoCliente := trim(DataRicerche.QDistanza.fieldbyname('tipostrada').asstring) + '+' + trim(DataRicerche.QDistanza.fieldbyname('indirizzo').asstring) + '+' +
                         trim(DataRicerche.QDistanza.fieldbyname('numcivico').asstring) + '+' + //DataRicerche.QDistanza.fieldbyname('cap').asstring + ' ' +
                         trim(DataRicerche.QDistanza.fieldbyname('comune').asstring); // + '+' + trim(DataRicerche.QDistanza.fieldbyname('provincia').asstring);
          end;
          IndirizzoCliente := StringReplace(IndirizzoCliente, '''', '\''', [rfReplaceAll]);
          IndirizzoCliente := TogliWhiteSpace(IndirizzoCliente);
     end;
     if IndirizzoCliente = '' then begin
          //MessageDlg('Attenzione, Nessun indirizzo cliente impostato nella commessa', MTWarning, [mbOk], 0);
          MessageDlg('    Attenzione, Impossibile Calcolare la Distanza' + chr(13) +
               'L''indirizzo del Cliente potrebbe non essere Impostato nella Commessa.', MTWarning, [mbOk], 0);
          exit;
     end;

     case TipoIndirizzoGoogleMapsTutti of
          1: begin
                    for i := 0 to dxDBGrid1.SelectedCount - 1 do begin
                         DataRicerche.qCandRic.Bookmark := dxDBGrid1.SelectedRows[i];
                         case TipoIndirizzoGoogleMaps of
                              1: begin //domicilio
                                        IndirizzoAna := '';
                                        if dataricerche.qCandRic.fieldbyname('domiciliocomune').asstring <> '' then begin
                                             IndirizzoAna := dataricerche.qCandRic.fieldbyname('domiciliocomune').asstring;
                                             if (dataricerche.qCandRic.fieldbyname('domiciliotipostrada').asstring <> '') and
                                                  (dataricerche.qCandRic.fieldbyname('domicilioindirizzo').asstring <> '') then
                                                  IndirizzoAna := IndirizzoAna + '+' + trim(dataricerche.qCandRic.fieldbyname('domiciliotipostrada').asstring) + '+' +
                                                       trim(dataricerche.qCandRic.fieldbyname('domicilioindirizzo').asstring);
                                        end;
                                   end;
                              2: begin //residenza
                                        IndirizzoAna := '';
                                        if dataricerche.qCandRic.fieldbyname('comune').asstring <> '' then begin
                                             IndirizzoAna := dataricerche.qCandRic.fieldbyname('comune').asstring;
                                             if (dataricerche.qCandRic.fieldbyname('tipostrada').asstring <> '') and
                                                  (dataricerche.qCandRic.fieldbyname('domicilioindirizzo').asstring <> '') then
                                                  IndirizzoAna := IndirizzoAna + '+' + trim(dataricerche.qCandRic.fieldbyname('tipostrada').asstring) + '+' +
                                                       trim(dataricerche.qCandRic.fieldbyname('indirizzo').asstring);
                                        end;
                                   end;
                         end;
                         IndirizzoAna := StringReplace(IndirizzoAna, '''', '\''', [rfReplaceAll]);
                         IndirizzoAna := TogliWhiteSpace(IndirizzoAna);
                         if (IndirizzoAna <> '') then begin
                              Data.qtemp2.close;
                              Data.qtemp2.SQL.text := 'select IDKey from FormWeb_social where social = ''Google Distance'' ';
                              Data.qtemp2.Open;
                              if Data.Qtemp2.RecordCount > 0 then begin
                                   try

                                        CalcolaDistanceDurata(IndirizzoCliente, IndirizzoAna, 'Driving', km, tempo, xStartLat, xStartLng, xEndLat, xEndLng, xstatus);
                                        xlogDistanza.Add(UpperCase(dataricerche.qCandRic.fieldbyname('Cognome').asstring) + ' ' +
                                             dataricerche.qCandRic.fieldbyname('Nome').asstring + ' - ' + xstatus);
                                   except
                                        on E: Exception do ShowMessage(E.Message);
                                   end;
                                   try
                                        DataRicerche.QInsertDist.Close;
                                        DataRicerche.QInsertDist.Parameters.ParamByName('xIdRicerca').value := dataricerche.QRicAttiveID.Value;
                                        DataRicerche.QInsertDist.Parameters.ParamByName('xIdAnagrafica').value := dataricerche.qCandRicIDAnagrafica.value;
                                        case TipoIndirizzoGoogleMaps of
                                             1: begin //domicilio
                                                  //DataRicerche.QInsertDist.Parameters.ParamByName('xDistanzaDomicilio').value := km; //FormatFloat('0.00', km1);
                                                  //DataRicerche.QInsertDist.Parameters.ParamByName('xTempoDomicilio').value := tempo; //FormatFloat('0.00', tempo1);
                                                  //DataRicerche.QInsertDist.Parameters.ParamByName('xLatitudineCandDomicilio').value := xStartLat;
                                                  //DataRicerche.QInsertDist.Parameters.ParamByName('xLongitudineCandDomicilio').value := xStartLng;
                                                       DataRicerche.QInsertDist.Parameters.ParamByName('xDistanza').value := km; //FormatFloat('0.00', km1);
                                                       DataRicerche.QInsertDist.Parameters.ParamByName('xTempo').value := tempo; //FormatFloat('0.00', tempo1);
                                                       DataRicerche.QInsertDist.Parameters.ParamByName('xLatitudineCand').value := xStartLat;
                                                       DataRicerche.QInsertDist.Parameters.ParamByName('xLongitudineCand').value := xStartLng;
                                                  end;
                                             2: begin //residenza
                                                       DataRicerche.QInsertDist.Parameters.ParamByName('xDistanza').value := km; //FormatFloat('0.00', km1);
                                                       DataRicerche.QInsertDist.Parameters.ParamByName('xTempo').value := tempo; //FormatFloat('0.00', tempo1);
                                                       DataRicerche.QInsertDist.Parameters.ParamByName('xLatitudineCand').value := xStartLat;
                                                       DataRicerche.QInsertDist.Parameters.ParamByName('xLongitudineCand').value := xStartLng;
                                                  end;
                                        end;
                                        DataRicerche.QInsertDist.Parameters.ParamByName('xLatitudineCliente').value := xEndLat;
                                        DataRicerche.QInsertDist.Parameters.ParamByName('xLongitudineCliente').value := xEndLng;
                                        DataRicerche.QInsertDist.ExecSQL;
                                   except
                                        on E: Exception do ShowMessage(E.Message);
                                   end;
                              end; //mm
                         end;
                    end;
               end;
          2: begin
                    DataRicerche.qCandRic.First;
                    while not dataricerche.qCandRic.Eof do begin
                         case TipoIndirizzoGoogleMaps of
                              1: begin //domicilio
                                        IndirizzoAna := '';
                                        if dataricerche.qCandRic.fieldbyname('domiciliocomune').asstring <> '' then begin
                                             IndirizzoAna := dataricerche.qCandRic.fieldbyname('domiciliocomune').asstring;
                                             if (dataricerche.qCandRic.fieldbyname('domiciliotipostrada').asstring <> '') and
                                                  (dataricerche.qCandRic.fieldbyname('domicilioindirizzo').asstring <> '') then
                                                  IndirizzoAna := IndirizzoAna + '+' + trim(dataricerche.qCandRic.fieldbyname('domiciliotipostrada').asstring) + '+' +
                                                       trim(dataricerche.qCandRic.fieldbyname('domicilioindirizzo').asstring);
                                        end;

                                   end;
                              2: begin //residenza
                                        IndirizzoAna := '';
                                        if dataricerche.qCandRic.fieldbyname('comune').asstring <> '' then begin
                                             IndirizzoAna := dataricerche.qCandRic.fieldbyname('comune').asstring;
                                             if (dataricerche.qCandRic.fieldbyname('tipostrada').asstring <> '') and
                                                  (dataricerche.qCandRic.fieldbyname('domicilioindirizzo').asstring <> '') then
                                                  IndirizzoAna := IndirizzoAna + '+' + trim(dataricerche.qCandRic.fieldbyname('tipostrada').asstring) + '+' +
                                                       trim(dataricerche.qCandRic.fieldbyname('indirizzo').asstring);
                                        end;
                                   end;
                         end;
                         IndirizzoAna := StringReplace(IndirizzoAna, '''', '\''', [rfReplaceAll]);
                         IndirizzoAna := TogliWhiteSpace(IndirizzoAna);
                         if (IndirizzoAna <> '') then begin
                              Data.qtemp2.close;
                              Data.qtemp2.SQL.text := 'select IDKey from FormWeb_social where social = ''Google Distance'' ';
                              Data.qtemp2.Open;
                              if Data.Qtemp2.RecordCount > 0 then begin
                                   try

                                        CalcolaDistanceDurata(IndirizzoCliente, IndirizzoAna, 'Driving', km, tempo, xStartLat, xStartLng, xEndLat, xEndLng, xstatus);
                                        xlogDistanza.Add(UpperCase(dataricerche.qCandRic.fieldbyname('Cognome').asstring) + ' ' +
                                             dataricerche.qCandRic.fieldbyname('Nome').asstring + ' - ' + xstatus);
                                   except
                                        on E: Exception do ShowMessage(E.Message);
                                   end;
                                   try
                                        DataRicerche.QInsertDist.Close;
                                        DataRicerche.QInsertDist.Parameters.ParamByName('xIdRicerca').value := dataricerche.QRicAttiveID.Value;
                                        DataRicerche.QInsertDist.Parameters.ParamByName('xIdAnagrafica').value := dataricerche.qCandRicIDAnagrafica.value;
                                        case TipoIndirizzoGoogleMaps of
                                             1: begin //domicilio
                                                  //DataRicerche.QInsertDist.Parameters.ParamByName('xDistanzaDomicilio').value := km; //FormatFloat('0.00', km1);
                                                  //DataRicerche.QInsertDist.Parameters.ParamByName('xTempoDomicilio').value := tempo; //FormatFloat('0.00', tempo1);
                                                  //DataRicerche.QInsertDist.Parameters.ParamByName('xLatitudineCandDomicilio').value := xStartLat;
                                                  //DataRicerche.QInsertDist.Parameters.ParamByName('xLongitudineCandDomicilio').value := xStartLng;
                                                       DataRicerche.QInsertDist.Parameters.ParamByName('xDistanza').value := km; //FormatFloat('0.00', km1);
                                                       DataRicerche.QInsertDist.Parameters.ParamByName('xTempo').value := tempo; //FormatFloat('0.00', tempo1);
                                                       DataRicerche.QInsertDist.Parameters.ParamByName('xLatitudineCand').value := xStartLat;
                                                       DataRicerche.QInsertDist.Parameters.ParamByName('xLongitudineCand').value := xStartLng;
                                                  end;
                                             2: begin //residenza
                                                       DataRicerche.QInsertDist.Parameters.ParamByName('xDistanza').value := km; //FormatFloat('0.00', km1);
                                                       DataRicerche.QInsertDist.Parameters.ParamByName('xTempo').value := tempo; //FormatFloat('0.00', tempo1);
                                                       DataRicerche.QInsertDist.Parameters.ParamByName('xLatitudineCand').value := xStartLat;
                                                       DataRicerche.QInsertDist.Parameters.ParamByName('xLongitudineCand').value := xStartLng;
                                                  end;
                                        end;
                                        DataRicerche.QInsertDist.Parameters.ParamByName('xLatitudineCliente').value := xEndLat;
                                        DataRicerche.QInsertDist.Parameters.ParamByName('xLongitudineCliente').value := xEndLng;
                                        DataRicerche.QInsertDist.ExecSQL;
                                   except
                                        on E: Exception do ShowMessage(E.Message);
                                   end;
                              end //mm
                         end;
                         DataRicerche.qCandRic.Next;
                    end;
               end;
     end;
     //MessageDlg('Termine Calcolo delle Distanze' + chr(13) + xlogDistanza.Text, mtConfirmation, [mbOk], 0);
     MessageDlg('Termine Calcolo delle Distanze.', mtConfirmation, [mbOk], 0);
     {
     IndirizzoAna := StringReplace(IndirizzoAna, '''', '\''', [rfReplaceAll]);
     IndirizzoAna := TogliWhiteSpace(IndirizzoAna);
     if IndirizzoCliente = '' then begin
          MessageDlg('Attenzione, Impossibile Calcolare la Distanza' + chr(13) + ' -- Indirizzo del Cliente non � stato Impostato. -- ', MTWarning, [mbOk], 0);
          dataricerche.qCandRic.Locate('ID', xid, []);
          exit;
     end;

     if data.Global.fieldbyname('debughrms').asboolean = true then
          showmessage('indirizzoana= ' + IndirizzoAna + chr(13) + 'indirizzocliente= ' + IndirizzoCliente);

     try
          if (IndirizzoCliente <> '') and (IndirizzoAna <> '') then begin
               //xid := dataricerche.qCandRicID.AsInteger;
               try
                    CalcolaDistanceDurata(IndirizzoCliente, IndirizzoAna, 'Driving', km, tempo, xStartLat, xStartLng, xEndLat, xEndLng);
               except
                    on E: Exception do ShowMessage(E.Message);
               end;
               //dataricerche.qCandRic.Locate('ID', xid, []);
               if data.Global.fieldbyname('debughrms').asboolean = true then
                    showmessage('km=' + km + chr(13) + 'tempo=' + tempo);
               try
                    DataRicerche.QInsertDist.Close;
                    DataRicerche.QInsertDist.Parameters.ParamByName('xIdRicerca').value := dataricerche.QRicAttiveID.Value;
                    DataRicerche.QInsertDist.Parameters.ParamByName('xIdAnagrafica').value := dataricerche.qCandRicIDAnagrafica.value;
                    DataRicerche.QInsertDist.Parameters.ParamByName('xDistanza').value := km; //FormatFloat('0.00', km1);
                    DataRicerche.QInsertDist.Parameters.ParamByName('xTempo').value := tempo; //FormatFloat('0.00', tempo1);
                    DataRicerche.QInsertDist.Parameters.ParamByName('xLatitudineCand').value := xStartLat;
                    DataRicerche.QInsertDist.Parameters.ParamByName('xLongitudineCand').value := xStartLng;
                    DataRicerche.QInsertDist.Parameters.ParamByName('xLatitudineCliente').value := xEndLat;
                    DataRicerche.QInsertDist.Parameters.ParamByName('xLongitudineCliente').value := xEndLng;
                    DataRicerche.QInsertDist.ExecSQL;
               except
                    on E: Exception do ShowMessage(E.Message);
               end;
          end;
          }
          (*      //  Memo1.Lines.Add('iniz');
             ExecuteJavaScript(WebBrowser1, 'distanza(' + '''' + IndirizzoCliente
                  + '''' + ',' + '''' + IndirizzoAna + '''' + ');');
                  //Memo1.Lines.Add('2');
                  //Sleep(1000);
             Delay(1000);
                  //Memo1.Lines.Add('3');
                  //a indica l'errore restituito da google
             a := '';
             a := GetElementIdValue(SelPersForm.WebBrowser1, 'input', 'resulterrore', 'value');
             if a = 'vuoto' then begin
                   //    showmessage(GetElementIdValue(WebBrowser1, 'input', 'resultkm', 'value') + ' km');
                   //    showmessage(GetElementIdValue(WebBrowser1, 'input', 'resulttempo', 'value') + ' minuti');
                  km := GetElementIdValue(SelPersForm.WebBrowser1, 'input', 'resultkm', 'value');
                  tempo := GetElementIdValue(SelPersForm.WebBrowser1, 'input', 'resulttempo', 'value');
                  km1 := StrToFloat(StringReplace(km, '.', ',', [rfReplaceAll]));
                  tempo1 := StrToFloat(StringReplace(tempo, '.', ',', [rfReplaceAll]));


                  try
                       DataRicerche.QInsertDist.Close;
                       DataRicerche.QInsertDist.Parameters.ParamByName('xIdRicerca').value := dataricerche.QRicAttiveID.Value;
                       DataRicerche.QInsertDist.Parameters.ParamByName('xIdAnagrafica').value := dataricerche.qCandRicIDAnagrafica.value;
                       DataRicerche.QInsertDist.Parameters.ParamByName('xDistanza').value := FormatFloat('0.00', km1);
                       DataRicerche.QInsertDist.Parameters.ParamByName('xTempo').value := FormatFloat('0.00', tempo1);
                       DataRicerche.QInsertDist.ExecSQL;
                  except
                       on E: Exception do ShowMessage(E.Message);
                  end;

             end else
                  showmessage(a);

                 // Memo1.Lines.Add('------');

              *)
     {
     except
     on E: Exception do ShowMessage(E.Message);
     end;

     }

     //ShowMessage(xidcliente);

     //  end;
     //  Memo1.Lines.SaveToFile('LogGoogleDistanza.txt');
     {* idtemp := dataricerche.qCandRicID.AsInteger;
     dataricerche.qCandRic.close;
     dataricerche.qCandRic.Open;
     dataricerche.qCandRic.Locate('id', idtemp, []);  *}

end;

procedure TSelPersForm.AnagraficaResidenza1Click(Sender: TObject);
var i, xIDTEmp: integer;
     xLocate: Boolean;

begin
     TipoIndirizzoGoogleMapsTutti := 1;
     TipoIndirizzoGoogleMaps := 2;
     if dxDBGrid1.SelectedCount = 0 then begin
          MessageDlg('Attenzione non � stato selezionato nessun Candidato.', mtWarning, [mbOk], 0);
          exit;
     end;
     if dxDBGrid1.SelectedCount > 1 then begin
          DataRicerche.qCandRic.Bookmark := dxDBGrid1.SelectedRows[0];
          xIDTEmp := DataRicerche.qCandRicID.Value;
     end else xIDTEmp := DataRicerche.qCandRicID.Value;
     BCalcolaDistanzaClick(self);
     {
     for i := 0 to dxDBGrid1.SelectedCount - 1 do begin
          DataRicerche.qCandRic.Bookmark := dxDBGrid1.SelectedRows[i]
          BCalcolaDistanzaClick(self);;
     end;
     xLocate := false;
     if dxDBGrid1.SelectedCount = 1 then begin
          xIDTEmp := DataRicerche.qCandRicID.Value;
          xlocate := true;
     end;
     if xlocate then
          dataricerche.qCandRic.Locate('id', xidtemp, []);
     }
     dataricerche.qCandRic.close;
     dataricerche.qCandRic.Open;
     dataricerche.qCandRic.Locate('id', xidtemp, []);
end;

procedure TSelPersForm.AnagraficaDomicilio1Click(Sender: TObject);
var i, xidtemp: integer;
     xLocate: Boolean;
begin
     TipoIndirizzoGoogleMapsTutti := 1;
     TipoIndirizzoGoogleMaps := 1;
     if dxDBGrid1.SelectedCount = 0 then begin
          MessageDlg('Attenzione non � stato selezionato nessun Candidato.', mtWarning, [mbOk], 0);
          exit;
     end;
     if dxDBGrid1.SelectedCount > 1 then begin
          DataRicerche.qCandRic.Bookmark := dxDBGrid1.SelectedRows[0];
          xIDTEmp := DataRicerche.qCandRicID.Value;
     end else xIDTEmp := DataRicerche.qCandRicID.Value;
     BCalcolaDistanzaClick(self);
     {
     for i := dxDBGrid1.SelectedCount - 1 downto 0 do begin
          DataRicerche.qCandRic.Bookmark := dxDBGrid1.SelectedRows[i];
          BCalcolaDistanzaClick(self);
     end;
     xLocate := false;
     if dxDBGrid1.SelectedCount = 1 then begin
          xIDTEmp := DataRicerche.qCandRicID.Value;
          xlocate := true;
     end;
     dataricerche.qCandRic.close;
     dataricerche.qCandRic.Open;
     if xlocate then
          dataricerche.qCandRic.Locate('id', xidtemp, []);
     }
     dataricerche.qCandRic.close;
     dataricerche.qCandRic.Open;
     dataricerche.qCandRic.Locate('id', xidtemp, []);
end;

procedure TSelPersForm.bAnnEventoClick(Sender: TObject);
var i: integer;
     Vero, xInserito: boolean;
     xID: integer;
begin
     if not Mainform.CheckProfile('050') then Exit;
     if DataRicerche.QAnnunciCand.RecordCount > 0 then begin
          CambiaStato2Form := TCambiaStato2Form.create(self);
          CambiaStato2Form.ECogn.Text := DataRicerche.QAnnunciCand.FieldByName('Cognome').AsString;
          CambiaStato2Form.ENome.Text := DataRicerche.QAnnunciCand.FieldByName('Nome').AsString;
          CambiaStato2Form.PanStato.Visible := False;
          CambiaStato2Form.xIDDaStato := DataRicerche.QAnnunciCand.FieldByName('IDStato').AsInteger;
          CambiaStato2Form.DEData.Date := Date;
          CambiaStato2Form.ShowModal;
          if CambiaStato2Form.ModalResult = mrOK then begin
               xInserito := InsEvento(DataRicerche.QAnnunciCand.FieldByName('IDAnagrafica').AsInteger,
                    CambiaStato2Form.QNextStati.FieldByName('IDProcedura').AsInteger,
                    CambiaStato2Form.QNextStati.FieldByName('ID').AsInteger,
                    CambiaStato2Form.QNextStati.FieldByName('IDaStato').AsInteger,
                    CambiaStato2Form.QNextStati.FieldByName('IDTipoStatoA').AsInteger,
                    CambiaStato2Form.DEData.Date,
                    DataRicerche.QAnnunciCand.FieldByName('Cognome').AsString,
                    DataRicerche.QAnnunciCand.FieldByName('Nome').AsString,
                    CambiaStato2Form.Edit1.Text,
                    0,
                    MainForm.xIDUtenteAttuale,
                    0);
               xID := DataRicerche.QAnnunciCand.FieldByName('IDAnagrafica').AsInteger;
               DataRicerche.QAnnunciCand.Close;
               DataRicerche.QAnnunciCand.Open;
               DataRicerche.QAnnunciCand.locate('IDAnagrafica', xID, []);
               LTotCandAnn.Caption := IntToStr(DataRicerche.QAnnunciCand.RecordCount);
          end;
          CambiaStato2Form.Free;
          //RiprendiAttivita;
     end;
end;

procedure TSelPersForm.BQuestRisposteDateClick(Sender: TObject);
begin
     {
     if DataRicerche.qAnnunciCandIDAnagQuest.AsString = '' then exit;
     AnagQuestRisposteDateForm := TAnagQuestRisposteDateForm.create(self);
     AnagQuestRisposteDateForm.QRisposteDate.Parameters[0].Value := DataRicerche.qAnnunciCandIDAnagQuest.Value;
     AnagQuestRisposteDateForm.ShowModal;
     AnagQuestRisposteDateForm.Free;
     }

     if DataRicerche.qAnnunciCandIDAnagQuest.AsString = '' then exit;
     DettAnagQuestForm := TDettAnagQuestForm.create(self);
     DettAnagQuestForm.xIDAnagQuest := DataRicerche.qAnnunciCandIDAnagQuest.Value;
     DettAnagQuestForm.ShowModal;
     DettAnagQuestForm.Free;
end;

procedure TSelPersForm.SpeedButton12Click(Sender: TObject);
var xID: string;
begin
     xID := OpenSelFromQuery(' select * from TipologieContratti ', 'ID', 'TipologiaContratto', 'Tipologia contratto', DataRicerche.TRicerchePend.FieldByName('IDTipologiaContratto').asString, True);
     if xID <> '' then begin
          DataRicerche.TRicerchePend.Edit;
          DataRicerche.TRicerchePend.FieldByName('IDTipologiaContratto').AsInteger := StrToIntDef(xID, 0);
          DataRicerche.TRicerchePend.Post;
          CaricaApriRicPend(DataRicerche.QRicAttive.FieldByName('ID').AsInteger);
     end;
end;

procedure TSelPersForm.BRicQuestNewClick(Sender: TObject);
var xIDQuest: integer;
begin
     // selezione questionari
     SelQuestForm := TSelQuestForm.create(self);
     SelQuestForm.ShowModal;
     if SelQuestForm.ModalResult = mrOK then begin
          Data.Q1.close;
          Data.Q1.SQL.clear;
          Data.Q1.SQL.text := 'insert into QUEST_RicercheQuest (IDRicerca, IDQuest) values (:x0, :x1)';
          Data.Q1.Parameters[0].value := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
          Data.Q1.Parameters[1].value := SelQuestForm.QElencoQuestID.Value;
          Data.Q1.ExecSQL;

          Data.Q1.Close;
          Data.Q1.SQL.text := 'select @@IDENTITY as LastID';
          Data.Q1.Open;
          xIDQuest := Data.Q1.FieldByName('LastID').asInteger;

          DataRicerche.QRicQuest.Close;
          DataRicerche.QRicQuest.Open;
          DataRicerche.QRicQuest.Locate('ID', xIDQuest, []);
     end;
end;

procedure TSelPersForm.BRicQuestDelClick(Sender: TObject);
begin
     if DataRicerche.QRicQuest.IsEmpty then exit;
     if MessageDlg('Sei sicuro di voler eliminare il questionario selezionato ?', mtWarning, [mbNo, mbYes], 0) <> mrYes then exit;
     Data.Q1.close;
     Data.Q1.SQL.clear;
     Data.Q1.SQL.text := 'delete from QUEST_RicercheQuest where ID=' + DataRicerche.QRicQuestID.AsString;
     Data.Q1.ExecSQL;

     DataRicerche.QRicQuest.Close;
     DataRicerche.QRicQuest.Open;
end;

procedure TSelPersForm.DBGAnnunciCandMouseUp(Sender: TObject;
     Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
     if (Button <> mbRight) or (Shift <> []) then Exit;
     TdxDBGridPopupMenuManager.Instance.ShowGridPopupMenu(Sender as TdxDBGrid);
end;

procedure TSelPersForm.BQuestInviaClick(Sender: TObject);
var xURL, xID, email, LF, xBody: string;
     i, ID: integer;
begin
     if dxDBGrid11.SelectedCount = 0 then begin
          MessageDlg('Nessun candidato selezionato', MTError, [mbOk], 0);
          exit;
     end;

     Data.Q1.Close;
     Data.Q1.SQL.clear;
     Data.Q1.SQL.text := 'select URL_ASP from QUEST_Params';
     Data.Q1.Open;
     xURL := Data.Q1.FieldByName('URL_ASP').asString;
     Data.Q1.Close;

     for i := 0 to dxDBGrid11.SelectedCount - 1 do begin
          DataRicerche.QRicQuest_Anag.Bookmark := dxDBGrid11.SelectedRows[i];
          xID := DataRicerche.QRicQuest_AnagGUID.AsString;

          if DataRicerche.QRicQuest_AnagDataOraInvio.asString = '' then begin

               xURL := StringReplace(xURL, '#', DataRicerche.QRicQuestIDQuest.AsString, []) + xID + '&IDAnagCompilatore=' + xID;
               xURL := StringReplace(xURL, '&', '%26', [rfReplaceAll]);
               xURL := StringReplace(xURL, '"', '%22', [rfReplaceAll]);

               if DataRicerche.QRicQuest_AnagEmail.value = '' then
                    InputQuery('Candidato senza mail', 'Indirizzo da fornire al candidato:', xURL)
               else begin

                    LF := '%0D%0A';

                    xBody := 'La preghiamo di compilare il questionario che trova al seguente indirizzo:' + LF + xURL +
                         LF + LF + 'La ringraziamo della collaborazione.' +
                         LF + LF + Data.Global.FieldByName('NomeAzienda').asString;

                    email := 'mailto:' + DataRicerche.QRicQuest_AnagEmail.value;
                    email := email + '?subject=Richiesta compilazione questionario';
                    email := email + '&body=' + xBody;

                    //ShellExecute(0, 'Open', pchar(email), '', '', SW_SHOWNORMAL);
                    SendEMailMAPI('Richiesta compilazione questionario', xBody, DataRicerche.QRicQuest_AnagEmail.value, '', '', MainForm.xSendMail_Remote, MainForm.xDebug, True, False);

               end;

               Data.QTemp.Close;
               Data.QTemp.SQL.clear;
               Data.QTemp.SQL.text := 'insert into QUEST_AnagQuest_Invio (IDAnagrafica, IDQuest, DataOraInvio) ' +
                    'values (:x0, :x1, :x2)';
               Data.QTemp.Parameters[0].value := DataRicerche.QRicQuest_AnagIDAnagrafica.Value;
               Data.QTemp.Parameters[1].value := DataRicerche.QRicQuestIDQuest.value;
               Data.QTemp.Parameters[2].value := Date + Time;
               Data.QTemp.ExecSQL;
          end;
     end;

     ID := DataRicerche.QRicQuest_AnagID.Value;
     DataRicerche.QRicQuest_Anag.Close;
     DataRicerche.QRicQuest_Anag.Open;
     DataRicerche.QRicQuest_Anag.locate('ID', ID, []);

end;

procedure TSelPersForm.ToolbarButton9716Click(Sender: TObject);
var i, ID: integer;
begin
     if dxDBGrid11.SelectedCount = 0 then begin
          MessageDlg('Nessun candidato selezionato', MTError, [mbOk], 0);
          exit;
     end;

     if MessageDlg('Sei sicuro di voler annullare l''invio per i soggetti selezionati ?', mtWarning, [mbYes, mbNo], 0) = mrNo then exit;

     for i := 0 to dxDBGrid11.SelectedCount - 1 do begin
          DataRicerche.QRicQuest_Anag.Bookmark := dxDBGrid11.SelectedRows[i];

          Data.QTemp.Close;
          Data.QTemp.SQL.clear;
          Data.QTemp.SQL.text := 'delete from QUEST_AnagQuest_Invio where ID = :x';
          Data.QTemp.Parameters[0].value := DataRicerche.QRicQuest_AnagIDAnagInvio.Value;
          Data.QTemp.ExecSQL;
     end;

     ID := DataRicerche.QRicQuest_AnagID.Value;
     DataRicerche.QRicQuest_Anag.Close;
     DataRicerche.QRicQuest_Anag.Open;
     DataRicerche.QRicQuest_Anag.locate('ID', ID, []);

end;

procedure TSelPersForm.BCliAssociaFileClick(Sender: TObject);
begin
     if DataRicerche.QRicQuest_Anag.IsEmpty then exit;
     if DataRicerche.QRicQuest_AnagIDAnagQuest.asString = '' then exit;
     DettAnagQuestForm := TDettAnagQuestForm.create(self);
     DettAnagQuestForm.xIDAnagQuest := DataRicerche.QRicQuest_AnagIDAnagQuest.value;
     DettAnagQuestForm.ShowModal;
     DettAnagQuestForm.Free;
end;

procedure TSelPersForm.BRicQuest_H1QuestClick(Sender: TObject);
var xFileName, xParams: string;
begin
     if DataRicerche.QRicQuest.IsEmpty then exit;
     xFileName := ExtractFileDir(Application.ExeName) + '\H1Questionari.exe';
     xParams := DataRicerche.QRicQuestIDQuest.AsString;

     ShellExecute(0, 'Open', pchar(xFileName), pchar(xParams), '', 0);
end;

procedure TSelPersForm.DbDateEdit971Enter(Sender: TObject);
begin
     DataRicerche.TRicerchePend.Edit;
end;

procedure TSelPersForm.SpeedButton13Click(Sender: TObject);
begin
     DataRicerche.QAnnunciCand.sql.savetofile('QAnnunciCand.sql');
     {     TestForm := TTestForm.create(self);
          TestForm.ShowModal;
          TestForm.Free; }
end;

procedure TSelPersForm.TUTTIAnagraficaResidenza1Click(Sender: TObject);
begin
     TipoIndirizzoGoogleMapsTutti := 2;
     TipoIndirizzoGoogleMaps := 2;
     BCalcolaDistanzaClick(self);
     {
     DataRicerche.qCandRic.First;
     while not dataricerche.qCandRic.Eof do begin
          BCalcolaDistanzaClick(self);
          DataRicerche.qCandRic.Next;
     end;
     }
     DataRicerche.qCandRic.Close;
     DataRicerche.qCandRic.Open;
     DataRicerche.qCandRic.First;
end;

procedure TSelPersForm.TUTTIAnagraficaDomicilio1Click(Sender: TObject);
begin
     TipoIndirizzoGoogleMapsTutti := 2;
     TipoIndirizzoGoogleMaps := 1;
     BCalcolaDistanzaClick(self);
     {
     DataRicerche.qCandRic.First;
     while not dataricerche.qCandRic.Eof do begin
          BCalcolaDistanzaClick(self);
          DataRicerche.qCandRic.Next;
     end;
     }
     dataricerche.qCandRic.close;
     dataricerche.qCandRic.Open;
     DataRicerche.qCandRic.First;
end;

procedure TSelPersForm.Esportadaticandidati1Click(Sender: TObject);
var xidanag, xPassword: string;
     i: integer;
begin
     if MainForm.VerificaFunzLicenze('108') = true then begin
          MessageDlg('FUNZIONE O MODULO ' + GetFunctionName('108') + ' NON COMPRESA NELLA LICENZA' + #13 + 'Consulta l''About per maggiori informazioni', mtError, [mbOK], 0);
          Exit;
     end;
     xidanag := '';
     if dxDBGrid1.SelectedCount <= 0 then begin
          MessageDlg('Nessun Candidato Selezionato', mtInformation, [mbOK], 0);
          exit;
     end;
     for i := 0 to dxDBGrid1.SelectedCount - 1 do begin
          dataricerche.qCandRic.Bookmark := dxDBGrid1.SelectedRows[i];
          xidanag := xidanag + DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsString + ',';
     end;
     xidanag[Length(xidanag)] := ' ';
     if not Mainform.CheckProfile('90') then Exit;
     InputPasswordForm.EPassword.text := 'EPassword';
     InputPasswordForm.caption := 'Inserimento password di Esportazione';
     InputPasswordForm.ShowModal;
     if InputPasswordForm.ModalResult = mrOK then begin
          xPassword := InputPasswordForm.EPassword.Text;
     end else exit;
     if not CheckPassword(xPassword, LeggiPswProcedure(10)) then begin
          MessageDlg('Password errata', mtError, [mbOK], 0);
          exit;
     end;
     if xidanag <> '' then
          MainForm.CallMeExport(xidanag, data.Global.Fieldbyname('nomeazienda').AsString, ';', 4, 4, 4, 4, handle, 'commessa');
end;

procedure TSelPersForm.ToolbarButton9717Click(Sender: TObject);
begin
     if not Mainform.CheckProfile('214') then Exit;
     LegendaGestRicForm := TLegendaGestRicForm.create(self);
     LegendaGestRicForm.ShowModal;
     LegendaGestRicForm.Free;
     Q.SQl.text := 'select ggDiffDataIns,ggDiffDataUltimoContatto,CheckRicCandCV,CheckRicCandTel from global ';
     Q.Open;
     xggDiffDataIns := Q.FieldByname('ggDiffDataIns').asInteger;
     xggDiffDataUltimoContatto := Q.FieldByname('ggDiffDataUltimoContatto').asInteger;
     xCheckRicCandCV := Q.FieldByname('CheckRicCandCV').AsBoolean;
     xCheckRicCandTel := Q.FieldByname('CheckRicCandTel').AsBoolean;
     Q.Close;
     EseguiQueryCandidati;
end;

procedure TSelPersForm.ToolbarButton9718Click(Sender: TObject);
begin
     ApriHelp('21');
end;

procedure TSelPersForm.BOkClick(Sender: TObject);
begin
     SelPersForm.ModalResult := mrOk;
     if DataRicerche.DsQRicTargetList.State = dsEdit then DataRicerche.QRicTargetList.Post;
     if (ERifOfferta.Text <> '') and (xidoffertaTemp <> '') then begin
          Data.Q3.Close;
          Data.Q3.SQL.Text := 'update EBC_Ricerche set IDOfferta = ' + xidoffertaTemp + ' where id = ' + DataRicerche.TRicerchePend.FieldByName('ID').AsString;
          Data.Q3.ExecSQL;
     end;
end;

procedure TSelPersForm.dxDbGrid3Click(Sender: TObject);
begin
     DataRicerche.QTLCandidati.Close;
     //DataRicerche.QTLCandidati.ParamByName['xIDTargetList'] := DataRicerche.QRicTargetListID.Value;
     //DataRicerche.QTLCandidati.ParamByName['xIDAzienda'] := DataRicerche.QRicTargetListIDCliente.Value;
     DataRicerche.QTLCandidati.Open;
end;

procedure TSelPersForm.dxDbGrid3KeyUp(Sender: TObject; var Key: Word;
     Shift: TShiftState);
begin
     dxDbGrid3Click(Self);
end;

procedure TSelPersForm.ShowMapClick(Sender: TObject);
begin
     if (dxDBGrid1.SelectedCount <= 0) or (dxDBGrid1.SelectedCount > 1) then begin
          MessageDlg('Attenzione, Seleziona un solo soggetto!', mtWarning, [mbOK], 0);
          exit;
     end;
     DataRicerche.qCandRic.Bookmark := dxDBGrid1.SelectedRows[0];
     DataRicerche.QDistanza.close;
     DataRicerche.QDistanza.SQL.Text := 'select * from GoogleDistanzaRic ' +
          ' where idRicerca=' + DataRicerche.qCandRicIDRicerca.AsString +
          ' and idAnagrafica=' + DataRicerche.qCandRicIDAnagrafica.AsString;
     DataRicerche.QDistanza.Open;
     if DataRicerche.QDistanza.IsEmpty then begin
          MessageDlg('Attenzione, Non hai calcolato la Distanza tra ' +
               chr(13) + 'l''indirizzo del Cliente e l''indirizzo del Candidato!', mtWarning, [mbOK], 0);
          Exit;
     end;
     GoogleMapForm := TGoogleMapForm.create(self);
     GoogleMapForm.partenza := IndirizzoCliente;
     GoogleMapForm.arrivo := IndirizzoAna;
     GoogleMapForm.LatCand := DataRicerche.QDistanza.FieldByName('LatitudineCand').AsString;
     GoogleMapForm.LngCand := DataRicerche.QDistanza.FieldByName('LongitudineCand').AsString;
     GoogleMapForm.LatCliente := DataRicerche.QDistanza.FieldByName('LatitudineCliente').AsString;
     GoogleMapForm.LngCliente := DataRicerche.QDistanza.FieldByName('LongitudineCliente').AsString;
     GoogleMapForm.idRicerca := DataRicerche.qCandRicIDRicerca.AsString;
     GoogleMapForm.idAnagrafica := DataRicerche.qCandRicIDAnagrafica.AsString;
     GoogleMapForm.ShowModal;
     GoogleMapForm.Free;
end;

procedure TSelPersForm.CliCandidatiFrame1Infocandidato1Click(
     Sender: TObject);
begin
     CliCandidatiFrame1.Infocandidato1Click(Sender);
end;

procedure TSelPersForm.CliCandidatiFrame1curriculum1Click(Sender: TObject);
begin
     CliCandidatiFrame1.curriculum1Click(Sender);
end;

procedure TSelPersForm.CliCandidatiFrame1CancSituazCandClick(
     Sender: TObject);
begin
     CliCandidatiFrame1.CancSituazCandClick(Sender);

end;

procedure TSelPersForm.Gestionetabstatipercliente1Click(Sender: TObject);
begin
     OpenTab('Ann_AnnunciStatiCliente', ['ID', 'Descrizione'], ['Stato candidato per il cliente']);
     DataRicerche.QStatiClientiAnnunci.Close;
     DataRicerche.QStatiClientiAnnunci.open;
end;

procedure TSelPersForm.eliminastatoperilcliente1Click(Sender: TObject);
var xIDTemp: integer;
begin
     if MessageDlg('Vuoi procedere con l''eliminazione dello Stato Cliente Attuale?', mtWarning, [mbYes, mbNo], 0) = mrYes then begin
          Data.Q1.Close;
          Data.Q1.SQL.Text := 'Update Ann_AnagAnnEdizData set IDStatoCliente = NULL ' +
               'where id = ' + DataRicerche.qAnnunciCandIDAnagrafica.AsString;
          Data.Q1.ExecSQL;

          xIDTemp := DataRicerche.qAnnunciCandIDAnagrafica.Value;
          DataRicerche.QAnnunciCand.Parameters[0].Value := MainForm.xIDUtenteAttuale;
          DataRicerche.QAnnunciCand.Parameters[1].Value := xIDRicercaInt;
          DataRicerche.qAnnunciCand.Locate('IDAnagrafica', xIDTemp, []);
     end;
end;

procedure TSelPersForm.DBGAnnunciCandStatoClienteChange(Sender: TObject);
begin
     if DataRicerche.DsQAnnunciCand.State = dsEdit then DataRicerche.QAnnunciCand.Post;
     if DataRicerche.DsQAnnunciCand.State = dsEdit then DataRicerche.QAnnunciCand.Post;
end;

procedure TSelPersForm.ToolbarButton9720Click(Sender: TObject);
var i: integer;
     SavePlace: TBookmark;
begin
     if not Mainform.CheckProfile('2181') then Exit;
     DataRicerche.QAnnunciCand.Close;
     DataRicerche.QAnnunciCand.Open;

     if DataRicerche.QAnnunciCand.RecordCount > 0 then begin
          FaxPresentazForm := TFaxPresentazForm.create(self);
          faxpresentazform.QStatiCliente.Close;
          faxpresentazform.QStatiCliente.SQL.Text := 'select * from Ann_AnnunciStatiCliente';
          faxpresentazform.QStatiCliente.open;
          FaxPresentazForm.ASG1.RowCount := DataRicerche.QAnnunciCand.RecordCount + 1;
          DataRicerche.QAnnunciCand.First;
          i := 1;
          while not DataRicerche.QAnnunciCand.EOF do begin
               FaxPresentazForm.ASG1.addcheckbox(0, i, false, false);
               FaxPresentazForm.ASG1.Cells[0, i] := DataRicerche.QAnnunciCand.FieldByName('Cognome').AsString + ' ' + DataRicerche.QAnnunciCand.FieldByName('Nome').AsString;
               FaxPresentazForm.xArrayIDAnag[i] := DataRicerche.QAnnunciCand.FieldByName('IDAnagrafica').AsInteger;
               FaxPresentazForm.ASG1.Cells[3, i] := DataRicerche.QAnnunciCand.FieldByName('StatoCliente').AsString;
               DataRicerche.QAnnunciCand.Next;
               inc(i);
          end;
          FaxPresentazForm.QClienti.ReloadSQL;
          FaxPresentazForm.QClienti.ParamByName['xIDCliente'] := DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger;
          FaxPresentazForm.QClienti.open;
          FaxPresentazForm.TContattiCli.Open;
          FaxPresentazForm.xTotRighe := dataricerche.QAnnunciCand.RecordCount;
          FaxPresentazForm.ShowModal; SavePlace := DataRicerche.QAnnunciCand.GetBookmark;
          DataRicerche.QAnnunciCand.Close;
          DataRicerche.QAnnunciCand.Open;
          DataRicerche.QAnnunciCand.GotoBookmark(SavePlace);
          DataRicerche.QAnnunciCand.FreeBookmark(SavePlace);
          FaxPresentazForm.Free;
          RiprendiAttivita;
     end;
end;

procedure TSelPersForm.ToolbarButton9719Click(Sender: TObject);
begin
     DataRicerche.TRicerchePend.Edit;
     DataRicerche.TRicerchePend.FieldByName('IDSede').AsInteger := 0;
     with DataRicerche.TRicerchePend do begin
          Data.DB.BeginTrans;
          try
               post;
               Data.DB.CommitTrans;
               CaricaApriRicPend(DataRicerche.QRicAttive.FieldByName('ID').AsInteger);
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               raise;
          end;
     end;
end;

procedure TSelPersForm.ToolbarButton9721Click(Sender: TObject);
begin
     DataRicerche.TRicerchePend.Edit;
     DataRicerche.TRicerchePend.FieldByName('IDContattoCli').AsInteger := 0;
     with DataRicerche.TRicerchePend do begin
          Data.DB.BeginTrans;
          try
               post;
               Data.DB.CommitTrans;
               CaricaApriRicPend(DataRicerche.QRicAttive.FieldByName('ID').AsInteger);
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               raise;
          end;
     end;
end;

procedure TSelPersForm.ToolbarButton9722Click(Sender: TObject);
begin
     DataRicerche.TRicerchePend.Edit;
     DataRicerche.TRicerchePend.FieldByName('IDTipologiaContratto').AsInteger := 0;
     with DataRicerche.TRicerchePend do begin
          Data.DB.BeginTrans;
          try
               post;
               Data.DB.CommitTrans;
               CaricaApriRicPend(DataRicerche.QRicAttive.FieldByName('ID').AsInteger);
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               raise;
          end;
     end;
end;

procedure TSelPersForm.ModificaFlagAccClick(Sender: TObject);
var xIDAnag: integer;
begin
     Data.Q1.Close;
     if DataRicerche.qCandRicFlagAccPres.AsBoolean then
          Data.Q1.SQL.Text := 'Update EBC_CandidatiRicerche set flagaccpres =0' +
               'where id = ' + DataRicerche.qCandRicID.AsString
     else
          Data.Q1.SQL.Text := 'Update EBC_CandidatiRicerche set flagaccpres =1' +
               'where id = ' + DataRicerche.qCandRicID.AsString;
     Data.Q1.ExecSQL;
     ModificaFlagAcc.Checked := DataRicerche.qCandRicFlagAccPres.AsBoolean;
     xIDAnag := DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger;
     DataRicerche.QCandRic.Close;
     DataRicerche.QCandRic.Open;
     dxDBGrid1.FullCollapse;
     DataRicerche.QCandRic.Locate('IDAnagrafica', xIDAnag, []);
     RegistraVisioneCand(MainForm.xIDUtenteAttuale, DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger);
     DataRicerche.QCandRic.FieldByName('ID').FocusControl;
end;

procedure TSelPersForm.ExpMappaturainModelloIntermedia(xIDRicerca: integer; xNomeModello: string; var xFileName, output: string;
     flagsave: boolean = false);
var MsExcel, shape: Variant;
     myYear, myMonth, myDay: Word;
     nomefilename: string;
     qCandric: Tadoquery;
     xoutput: string;
     n: integer;
begin
     xoutput := '1';

     qCandRic := TADOQuery.Create(self);
     qCandRic.Connection := Data.DB;
     qCandRic.SQL.text := 'select Ric.ID IDRicerca,a.ID IDAnagrafica,Cognome,Nome,EspLavAttuale.Azienda,DataNascita,JobTitle from EBC_Ricerche Ric join ebc_candidatiricerche Candric ' +
          'on Ric.id=candric.idricerca ' +
          'join anagrafica a on a.id=candric.idanagrafica ' +
          'join ebc_clienti cli on cli.id=ric.idcliente ' +
          'LEFT OUTER JOIN ( ' +
          'SELECT Esperienzelavorative.ID ' +
          ',Esperienzelavorative.IDAnagrafica ' +
          ',ISNULL(EBC_Clienti.Descrizione,Esperienzelavorative.AziendaNome) Azienda ' +
          ',Mansioni.Descrizione Ruolo  ' +
          ',ruolopresentaz AS JobTitle  ' +
          ',Aree.Descrizione Area  ' +
          ',ebc_attivita.attivita Settore  ' +
          ',ebc_clienti.Telefono  from EsperienzeLavorative  join ( ' +
          'select IDAnagrafica,Max(ID) ID  ' +
          'from esperienzelavorative where attuale = 1 ' +
          'Group By IDAnagrafica) MaxEL ' +
          'on MaxEL.id = EsperienzeLavorative.id  ' +
          'LEFT JOIN EBC_Clienti ON Esperienzelavorative.IDAzienda = EBC_Clienti.ID  ' +
          'LEFT JOIN Mansioni ON Esperienzelavorative.IDMansione = Mansioni.ID  ' +
          'left JOIN aree ON Mansioni.IDArea = aree.id  ' +
          'LEFT JOIN ebc_attivita ON ebc_attivita.id = esperienzelavorative.idsettore) EspLavAttuale  ' +
          'ON EspLavAttuale.IDAnagrafica = a.ID  ';
     if CBOpzioneCand.Checked then begin
          qCandRic.SQL.text := qCandRic.SQL.text + ' where candric.escluso<>1 ' +
               ' and ric.id=:x0 order by cognome,nome';
     end else begin
          qCandRic.SQL.text := qCandRic.SQL.text + ' where ric.id=:x0 order by cognome,nome';
     end;
     qCandRic.Parameters[0].Value := xIDRicerca; // DataRicerche.QRicAttiveID.Value;
     qCandRic.Open;
     if QCandRic.RecordCount = 0 then begin
          xoutput := '0';
          //exit;
     end else begin

          try MsExcel := CreateOleObject('Excel.application');
          except
               ShowMessage('Non riesco ad aprire Microsoft Excel.');
               exit;
          end;

          MsExcel.WorkBooks.Open(xNomeModello);

    //showmessage(inttostr({DataRicerche.}QCandRic.recno));
     {DataRicerche.}QCandRic.First;
          Data.Q1.Close;
          Data.Q1.SQL.Text := 'Select ER.titolocliente, EC.Descrizione Cliente, ER.Progressivo Rif, getdate() as DataOdierna from EBC_Ricerche ER' +
               ' join EBC_Clienti EC on ER.idcliente = EC.id' +
               ' where ER.id=' + {DataRicerche.} qCandRic.Fieldbyname('IDRicerca').AsString;
          Data.Q1.Open;
          if Data.Q1.RecordCount = 0 then begin
               QCandRic.Close;
               qCandRic.Free;
               MSExcel.WorkBooks.Close;
               MSExcel.Quit;
               MSExcel := Unassigned;
               xoutput := '0';
               exit;
          end
          else begin

               nomefilename := StringReplace(StringReplace(StringReplace(StringReplace(Data.Q1.fieldbyname('Rif').AsString + '_' +
                    Data.Q1.fieldbyname('TitoloCliente').AsString, '&', '_', [rfReplaceAll, rfIgnoreCase])
                    , ',', '_', [rfReplaceAll, rfIgnoreCase]), '/', '_', [rfReplaceAll, rfIgnoreCase])
                    , ';', '_', [rfReplaceAll, rfIgnoreCase]);


               if not flagsave then begin
                    xFileName := xFileName + nomefilename + '.xlsx';

               //xFileName := ExtractFileDir(Application.exename) + '\' + nomefilename + '.xlsx';
               //showmessage(nomefilename);

                    if FileExists(xFileName) then begin
                         if MessageDlg(' Il file � gi� presente nella cartella. Vuoi sostituirlo ?', mtWarning, [mbYes, mbNo], 0) = mrYes then begin

                              DeleteFile(xFileName);
                         end else begin
                              QCandRic.Close;
                              qCandRic.Free;
                              xoutput := '0';
                              MSExcel.WorkBooks.Close;
                              MSExcel.Quit;
                              MSExcel := Unassigned;
                              //exit;
                         end;
                    end;
               end;

               if xoutput <> '0' then begin
                   // MSExcel.Visible := true;
                    //MsExcel.Activeworkbook.activesheet.cells[10, 6] := 'PROGRESS REPORT ' + Data.Q1.fieldbyname('TitoloCliente').AsString;
                    DecodeDate(Date, myYear, myMonth, myDay);
                    //MsExcel.Activeworkbook.activesheet.cells[11, 6] := VarToStr(myDay) + '/' + VarToStr(myMonth) + '/' + VarToStr(myYear);
                    MsExcel.Activeworkbook.activesheet.cells[36, 7] := VarToStr(myDay) + '/' + VarToStr(myMonth) + '/' + VarToStr(myYear);
                    //MsExcel.Activeworkbook.activesheet.cells[26, 2] := 'Il Presente report contiene delle informazioni strettamente confidenziali raccolte per l�esclusivo uso di ' + Data.Q1.fieldbyname('Cliente').AsString + '. Il nostro cliente si impegna a';
                    MsExcel.Activeworkbook.activesheet.cells[38, 1] := 'Il Presente report contiene delle informazioni strettamente confidenziali raccolte per l�esclusivo uso di ' + Data.Q1.fieldbyname('Cliente').AsString + '. Il nostro cliente si impegna a';

     {DataRicerche.}QCandRic.First;


                    while not {DataRicerche.} QCandRic.Eof do begin
                         Data.Qtemp.Close;
                         Data.Qtemp.SQL.Text := 'Select aa.benefits,aa.Inquadramento,cs.Settore,tr.TipoRetrib,aa.Retribuzione,aa.RetribVariabile' +
                              ' from AnagaltreInfo aa left join contrattisettori cs on aa.idContrattoSettore = cs.id' +
                              ' left join TipiRetribuzione tr on aa.idtiporetrib = tr.id' +
                              ' where IDAnagrafica=' + {DataRicerche.} QCandRic.Fieldbyname('IDAnagrafica').AsString;
                         Data.Qtemp.Open;
                         //MsExcel.Activeworkbook.activesheet.cells[{DataRicerche.}QCandRic.RecNo + 43, 1] := {DataRicerche.} QCandRic.RecNo;
                         MsExcel.Activeworkbook.activesheet.cells[{DataRicerche.}QCandRic.RecNo + 43, 1] := {DataRicerche.} QCandRic.Fieldbyname('Azienda').AsString;
                         MsExcel.Activeworkbook.activesheet.cells[{DataRicerche.}QCandRic.RecNo + 43, 2] := {DataRicerche.} QCandRic.Fieldbyname('Nome').AsString + ' ' + {DataRicerche.} QCandRic.Fieldbyname('Cognome').AsString;

                         MsExcel.Activeworkbook.activesheet.cells[{DataRicerche.}QCandRic.RecNo + 43, 3] := 'PROSPECT ' + IntToStr({DataRicerche.}QCandRic.RecNo);
                         if {DataRicerche.} QCandRic.Fieldbyname('DataNascita').asstring = '' then
                              MsExcel.Activeworkbook.activesheet.cells[{DataRicerche.}QCandRic.RecNo + 43, 4] := ''
                         else begin
                              DecodeDate({DataRicerche.}QCandRic.Fieldbyname('DataNascita').AsDateTime, myYear, myMonth, myDay);
                              MsExcel.Activeworkbook.activesheet.cells[{DataRicerche.}QCandRic.RecNo + 43, 4] := myYear;
                         end;
                         MsExcel.Activeworkbook.activesheet.cells[{DataRicerche.}QCandRic.RecNo + 43, 5] := {DataRicerche.} QCandRic.Fieldbyname('JobTitle').AsString;
                         MsExcel.Activeworkbook.activesheet.cells[{DataRicerche.}QCandRic.RecNo + 43, 6] :=
                              'Inquadramento: ' + Data.qtemp.FieldByName('Inquadramento').AsString + '  ' +
                              'CCNL: ' + Data.qtemp.FieldByName('Settore').AsString + '  ' +
                              'RAL: ' + Data.qtemp.FieldByName('Retribuzione').AsString + '  ' +
                              'Variabile: ' + Data.qtemp.FieldByName('RetribVariabile').AsString + '  ' +
                              'Benefit: ' + Data.qtemp.FieldByName('Benefits').AsString + '  ';

                         Data.Qtemp.Close;
                         Data.Qtemp.SQL.Text := 'Select ECR.MiniVal Valutazione,ECR.Stato Situazione,ECR.Note2 NoteCommessa,ECR.NoteCliente from EBC_Ricerche ER' +
                              ' join EBC_CandidatiRicerche ECR on ER.id = ECR.IDRicerca' +
                              ' where ER.id=' + {DataRicerche.} QCandRic.Fieldbyname('IDRicerca').AsString +
                              ' and IDAnagrafica=' + {DataRicerche.} QCandRic.Fieldbyname('IDAnagrafica').AsString;
                         Data.Qtemp.Open;
                         MsExcel.Activeworkbook.activesheet.cells[{DataRicerche.}QCandRic.RecNo + 43, 7] := Data.qtemp.FieldByName('Valutazione').AsString;
                         MsExcel.Activeworkbook.activesheet.cells[{DataRicerche.}QCandRic.RecNo + 43, 8] := Data.qtemp.FieldByName('Situazione').AsString;
                         MsExcel.Activeworkbook.activesheet.cells[{DataRicerche.}QCandRic.RecNo + 43, 9] := Data.qtemp.FieldByName('NoteCommessa').AsString;
                         MsExcel.Activeworkbook.activesheet.cells[{DataRicerche.}QCandRic.RecNo + 43, 10] := Data.qtemp.FieldByName('NoteCliente').AsString;

          //showmessage(inttostr({DataRicerche.}QCandRic.recno) + ' - ' + {DataRicerche.} QCandRic.Fieldbyname('Nome').AsString + ' ' + {DataRicerche.} QCandRic.Fieldbyname('Cognome').AsString);
          {DataRicerche.}QCandRic.Next;
                    end;

                    //TExtBox intestazioni
                    for n := 1 to MsExcel.Activeworkbook.activesheet.shapes.Count do begin

                         shape := MsExcel.Activeworkbook.activesheet.shapes.item(n);



                         if (shape.Type = 17) (*msoTextBox*) (*or (shape.Type = 1)*) then begin



                              if Data.Q1.fieldbyname('TitoloCliente').AsString <> '' then begin
                                   Shape.TextFrame.characters.text := 'Posizione: ' + Data.Q1.fieldbyname('TitoloCliente').AsString;
                                   Shape.TextFrame.Characters.Font.ColorIndex := 2;
                              end

                              else
                                   Shape.TextFrame.textframe.characters.text := '';
                         //Showmessage(inttostr(Shape.Type)+' '+ Shape.Name);
                         end;


                    end;
               end;
          end;
     end;
     if xoutput <> '0' then begin
          if Length(nomefilename) <= 31 then
               MSExcel.ActiveWorkBook.activesheet.name := nomefilename
          else MSExcel.ActiveWorkBook.activesheet.name := copy(nomefilename, 1, 31);

          MSExcel.ActiveWorkbook.SaveAs(xFileName);

          QCandRic.Close;
          qCandRic.Free;
          MSExcel.WorkBooks.Close;
          MSExcel.Quit;
          MSExcel := Unassigned;
     end;
     output := xoutput;
end;

procedure TSelPersForm.expModelloExcelIntermediaClick(Sender: TObject);
var
     xFileName, xdata, xora, xoutput: string;
begin
     if not LogEsportazioniCand(1) then exit;
     //if POS('INTERMEDIA', Uppercase(Data.Global.FieldByName('NomeAzienda').Value)) > 0 then begin
     xData := DateToStr(Date);
     xOra := TimeToStr(Time);

     xoutput := '1';

     Splash3Form := TSplash3Form.create(nil);
     Splash3Form.Show;

     if Data.Global.FieldByName('CheckReNameFileAllegato').asBoolean = TRUE then begin
          xFileName := DbEdit1.Text + '_' + DbEdit5.Text + '_' + DbEdit4.Text + '_' + xData + '_' + xOra;


          xFileName := StringReplace(xFileName, '/', '', [rfReplaceAll]);
          xFileName := StringReplace(xFileName, '\', '', [rfReplaceAll]);
          xFileName := StringReplace(xFileName, '.', '', [rfReplaceAll]);

          xFileName := GetPathFromTable('Commessa') + xFileName + '.xlsx';



          try
               xFileName := GetPathFromTable('Commessa');
               //ExpMappaturainModelloIntermedia(GetDocPath + '\ExpMappModelIntermedia.xls', xFileName);
               ExpMappaturainModelloIntermedia(DataRicerche.TRicerchePend.FieldByName('ID').asInteger, GetDocPath + '\ExpMappModelIntermedia_new.xlsx', xFileName, xoutput);
               Data.Q2.Close;
               Data.Q2.SQL.text := 'select Top 1 id from EBC_RicercheFile where idricerca=' + DataRicerche.TRicerchePend.FieldByName('ID').asstring +
                    ' and  NomeFile=''' + xFileName + '''';
               Data.Q2.Open;
               if Data.Q2.RecordCount > 0 then begin
                    Data.Q1.Close;
                    Data.Q1.SQL.text := 'update EBC_RicercheFile ' +
                         ' set NomeFile=''' + xFileName + '''' +
                         ' ,DataCreazione=''' + DateToStr(Date) + '''' +
                         ' where id=' + Data.Q2.FieldByName('ID').AsString;
                    Data.Q1.ExecSQL;
               end else begin
                    Data.Q1.Close;
                    Data.Q1.SQL.text := 'insert into EBC_RicercheFile (IDRicerca,IDTipo,Descrizione,NomeFile,DataCreazione) ' +
                         'values (:xIDRicerca:,:xIDTipo:,:xDescrizione:,:xNomeFile:,:xDataCreazione:)';
                    Data.Q1.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
                    Data.Q1.ParamByName['xIDTipo'] := 0;
                    Data.Q1.ParamByName['xDescrizione'] := 'Esportazione ricerca con il Modello Intermedia ' + DbEdit1.Text + ' (' + datetostr(Date) + ')';
                    Data.Q1.ParamByName['xNomeFile'] := xFileName;
                    Data.Q1.ParamByName['xDataCreazione'] := Date;
                    Data.Q1.ExecSQL;
               end;
          except
               on e: exception do showmessage(e.message);
          end;



     end else begin
          MainForm.SaveDialog.Execute;
          xFileName := MainForm.SaveDialog.FileName;
          //ExpMappaturainModello(GetDocPath + '\ExpMappModelIntermedia.xls', xFileName, true);


          try
               //ExpMappaturainModelloIntermedia(GetDocPath + '\ExpMappModelIntermedia.xls', xFileName, true);
               ExpMappaturainModelloIntermedia(DataRicerche.TRicerchePend.FieldByName('ID').asInteger, GetDocPath + '\ExpMappModelIntermedia_new.xlsx', xFileName, xoutput, true);
               Data.Q2.Close;
               Data.Q2.SQL.text := 'select Top 1 id from EBC_RicercheFile where idricerca=' + DataRicerche.TRicerchePend.FieldByName('ID').asstring +
                    ' and  NomeFile=''' + xFileName + '''';
               Data.Q2.Open;
               if Data.Q2.RecordCount > 0 then begin
                    Data.Q1.Close;
                    Data.Q1.SQL.text := 'update EBC_RicercheFile ' +
                         ' set NomeFile=''' + xFileName + '''' +
                         ' ,DataCreazione=''' + DateToStr(Date) + '''' +
                         ' where id=' + Data.Q2.FieldByName('ID').AsString;
                    Data.Q1.ExecSQL;
               end else begin


                    Data.Q1.Close;
                    Data.Q1.SQL.text := 'insert into EBC_RicercheFile (IDRicerca,IDTipo,Descrizione,NomeFile,DataCreazione) ' +
                         'values (:xIDRicerca:,:xIDTipo:,:xDescrizione:,:xNomeFile:,:xDataCreazione:)';
                    Data.Q1.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
                    Data.Q1.ParamByName['xIDTipo'] := 0;
                    Data.Q1.ParamByName['xDescrizione'] := 'Esportazione ricerca con il modello Intermedia ' + DbEdit1.Text + ' (' + datetostr(Date) + ')';
                    Data.Q1.ParamByName['xNomeFile'] := MainForm.SaveDialog.FileName;
                    Data.Q1.ParamByName['xDataCreazione'] := Date;
                    Data.Q1.ExecSQL;
               end;
          except
          end;

     end;

     Application.ProcessMessages;

     Splash3Form.Hide;
     Splash3Form.Free;
     if xoutput = '1' then begin
          try
               if MainForm.xSendMail_Remote then begin
                    CopiaApriFileSulClient(xFileName, true);
               end else
                    ShellExecute(0, 'Open', pchar(xFileName), '', '', SW_SHOWDEFAULT);
          except
          end;
     end;
      //end else
        //  Mainform.Save('xls', 'Microsoft Excel 4.0 Worksheet (*.xls)|*.xls', 'ExpGrid.xls', dxDBGrid1.SaveToXLS)
end;

procedure TSelPersForm.BAttivitaNewClick(Sender: TObject);
begin
     DataRicerche.QAttivita.Insert;
end;

procedure TSelPersForm.BAttivitaDelClick(Sender: TObject);
begin
     if MessageDlg('Sei sicuro di voler eliminare l''attivit� selezionata ?', mtWarning, [mbYes, mbNo], 0) = mrYes then
          DataRicerche.QAttivita.Delete;
end;

procedure TSelPersForm.BAttivitaOKClick(Sender: TObject);
begin
     DataRicerche.QAttivita.Post;
end;

procedure TSelPersForm.BAttivitaCanClick(Sender: TObject);
begin
     DataRicerche.QAttivita.Cancel;
end;

procedure TSelPersForm.BTabEventiAttClick(Sender: TObject);
begin
     OpenTab('TabEventiAttCommessa', ['ID', 'Evento'], ['Evento']);
     DataRicerche.QTabEventiAttCommLK.Close;
     DataRicerche.QTabEventiAttCommLK.Open;
end;

procedure TSelPersForm.ToolbarButton9723Click(Sender: TObject);
begin
     if MessageDlg('Sei sicuro di voler procedere ?', mtWarning, [mbYes, mbNo], 0) = mrNo then exit;

     Data.Qtemp.Close;
     Data.Qtemp.SQL.text := 'update EBC_Ricerche_attivita set IDFattura=null where ID=' + DataRicerche.QAttivitaID.AsString;
     Data.Qtemp.ExecSQL;
     DataRicerche.QAttivita.Close;
     DataRicerche.QAttivita.Open;
end;

procedure TSelPersForm.MiniProfilo1Click(Sender: TObject);
var xIDs: string;
     k: integer;
begin
     if dxDBGrid1.SelectedCount = 0 then begin
          MessageDlg('Nessun record selezionato. Impossibile proseguire', mtError, [mbOK], 0);
          exit;
     end;

     xIDs := '';
     for k := 0 to dxDBGrid1.SelectedCount - 1 do begin
          DataRicerche.qCandRic.BookMark := dxDBGrid1.SelectedRows[k];
          xIDs := xIDs + DataRicerche.qCandRicIDAnagrafica.AsString + ',';
     end;
     xIDs := copy(xIDS, 1, length(xIDs) - 1);

     CreaMiniProfiliSCR('Commessa', DataRicerche.TRicerchePendID.Value, xIDs, False, Data.DB.ConnectionString);
end;

procedure TSelPersForm.MiniProfiloanonimo1Click(Sender: TObject);
var xIDs: string;
     k: integer;
begin
     if dxDBGrid1.SelectedCount = 0 then begin
          MessageDlg('Nessun record selezionato. Impossibile proseguire', mtError, [mbOK], 0);
          exit;
     end;

     xIDs := '';
     for k := 0 to dxDBGrid1.SelectedCount - 1 do begin
          DataRicerche.qCandRic.BookMark := dxDBGrid1.SelectedRows[k];
          xIDs := xIDs + DataRicerche.qCandRicIDAnagrafica.AsString + ',';
     end;
     xIDs := copy(xIDS, 1, length(xIDs) - 1);

     CreaMiniProfiliSCR('Commessa', DataRicerche.TRicerchePendID.Value, xIDs, True, Data.DB.ConnectionString);
end;

procedure TSelPersForm.EsportamodelloExcel1Click(Sender: TObject);
var
     xFileName, xdata, xora: string;
begin




end;

procedure TSelPersForm.ToolbarButton9724Click(Sender: TObject);
begin
     PanStatCommessa.visible := False;
end;

procedure TSelPersForm.ToolbarButton9725Click(Sender: TObject);
begin
///15 febbraio 2018
     SelContattiForm := TSelContattiForm.create(self);
     SelContattiForm.QContatti.Close;
     SelContattiForm.QContatti.ParamByName['xIDCliente'] := DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger;
     SelContattiForm.xIDCliente := DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger;
     SelContattiForm.QContatti.Open;


     SelContattiForm.ShowModal;
     if SelContattiForm.ModalResult = mrOK then begin


          DataRicerche.QTemp2.close;
          DataRicerche.QTemp2.SQL.Text := 'select ID from EBC_RicercheContattiCli where IDRicerca=' + DataRicerche.TRicerchePendID.AsString +
               ' and IDContattoCli=' + SelContattiForm.QContatti.FieldByName('ID').AsString;
          DataRicerche.QTemp2.open;
          if (DataRicerche.QTemp2.RecordCount = 0) then begin

               try

                    DataRicerche.QTemp2.close;
                    DataRicerche.QTemp2.SQL.Text := ' insert into EBC_RicercheContattiCli ' +
                         ' (IDRicerca, IDContattoCli, IDAnagrafica) ' +
                         ' values ' +
                         ' (:xIDRicerca, :xIDContattoCli, :xIDAnagrafica) ';
                    DataRicerche.QTemp2.Parameters.ParamByName('xIDRicerca').value := DataRicerche.TRicerchePendID.AsInteger;
                    DataRicerche.QTemp2.Parameters.ParamByName('xIDContattoCli').value := SelContattiForm.QContatti.FieldByName('ID').AsInteger;
                    DataRicerche.QTemp2.Parameters.ParamByName('xIDAnagrafica').value := SelContattiForm.QContattiIDAnagrafica.asInteger;
                    DataRicerche.QTemp2.ExecSQL;

                    DataRicerche.QCommittentiCommesse.close;
                    DataRicerche.QCommittentiCommesse.Open;


               except

                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                    raise;
               end;

          end;

     end;
     SelContattiForm.Free;
end;

procedure TSelPersForm.ToolbarButton9726Click(Sender: TObject);
begin
     if not DataRicerche.QCommittentiCommesse.IsEmpty then begin

          if Messagedlg('Vuoi cancellare questa associazione? ', mtConfirmation, [mbyes, mbNo], 0) = mrYes then begin

               DataRicerche.QTemp2.close;
               DataRicerche.QTemp2.sql.Text := 'delete EBC_RicercheContattiCli where id=' + DataRicerche.QCommittentiCommesseID.asString;
               DataRicerche.QTemp2.ExecSQL;


               DataRicerche.QCommittentiCommesse.close;
               DataRicerche.QCommittentiCommesse.open;

          end;
     end;
end;

procedure TSelPersForm.Attivaworkflow1Click(Sender: TObject);
var xElencoWFAttivi: string;
begin
     AttivaworkflowForm := TAttivaworkflowForm.create(self);
     AttivaworkflowForm.xIDAnag := DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger;
     AttivaworkflowForm.xIDRicerca := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
     AttivaworkflowForm.Label1.Caption := 'Stai attivando un work flow per ' + DataRicerche.qCandRicCognome.AsString + ' ' + DataRicerche.qCandRicNome.AsString + #13 +
          'per il progetto Rif:' + DataRicerche.TRicerchePendProgressivo.AsString;


     QTemp.close;
     QTemp.SQL.Text := '  select  ' +
          '  sr.ID,   ' +
          '  r.Progressivo, ' +
          '  sc.ID Concluso ' +
          '   from WFScadenze_Ricerche sr ' +
          '  join EBC_Ricerche r on r.ID=sr.IDRicerca ' +
          '  left join WFScadenzeConclusioni sc on (sc.IDxx=sr.ID and sc.CodTipologia=''Sel'' ) ' +
          '   where sr.IDAnagrafica=:xIDAnagrafica ' +
          '   and  sc.ID is null ';
     QTemp.Parameters.ParamByName('xIDAnagrafica').value := DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger;
     QTemp.Open;
     xElencoWFAttivi := '';
     while not QTemp.eof do begin
          xElencoWFAttivi := xElencoWFAttivi + QTemp.fieldByName('Progressivo').asString + '; ';
          QTemp.next;
     end;
     xElencoWFAttivi := Copy(xElencoWFAttivi, 0, length(xElencoWFAttivi) - 1);

     if xElencoWFAttivi <> '' then begin
          AttivaworkflowForm.Label2.Caption := 'Per ' + DataRicerche.qCandRicCognome.AsString + ' ' + DataRicerche.qCandRicNome.AsString + ' ci sono gi� WF attivi sui seguenti progetti Rif:' + #13 + xElencoWFAttivi;
          AttivaworkflowForm.Label2.Visible := true;
     end else begin
         AttivaworkflowForm.Label2.Visible := false;
     end;



     AttivaworkflowForm.ShowModal;

     if AttivaworkflowForm.ModalResult = mrOK then begin
          QTemp.close;
          QTemp.SQL.Text := ' set dateformat dmy ' +
               ' insert into WFScadenze_Ricerche ' +
               ' (IDAnagrafica, IDRicerca, DataTermineWF, Note ) ' +
               ' values ' +
               ' (:xIDAnagrafica, :xIDRicerca, :xDataTermineWF, :xNote )';
          QTemp.Parameters.ParamByName('xIDAnagrafica').value := DataRicerche.QCandRic.FieldByName('IDAnagrafica').AsInteger;
          QTemp.Parameters.ParamByName('xIDRicerca').value := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
          QTemp.Parameters.ParamByName('xDataTermineWF').value := AttivaworkflowForm.DEData.Text;

          if AttivaworkflowForm.Memo1.text <>'' then begin
                QTemp.Parameters.ParamByName('xNote').value := AttivaworkflowForm.Memo1.text;
          end else
             QTemp.Parameters.ParamByName('xNote').value := ' ';

          QTemp.ExecSQL;

          MessageDlg('Workflow Attivato', mtConfirmation, [mbOk], 0);

     end;

     AttivaworkflowForm.free;

end;

procedure TSelPersForm.ElencoWorkFlow1Click(Sender: TObject);
begin
     ElencoWorkFlowForm := TElencoWorkFlowForm.create(self);
     ElencoWorkFlowForm.xIDRicerca := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;

     ElencoWorkFlowForm.Label1.caption:='Elenco Work Flow per il progetto Rif: '+DataRicerche.TRicerchePendProgressivo.asString;

     ElencoWorkFlowForm.ShowModal;

     ElencoWorkFlowForm.free;
end;

end.

