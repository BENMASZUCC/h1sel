unit TabCatProfess;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ExtCtrls, DBTables, Db, StdCtrls, Buttons, TB97, dxDBTLCl, dxGrClms,
     dxTL, dxDBCtrl, dxDBGrid, dxCntner, ADODB;

type
     TTabCatProfessForm = class(TForm)
          Panel3: TPanel;
          BNew: TToolbarButton97;
          BDel: TToolbarButton97;
          BCan: TToolbarButton97;
          BConferma: TToolbarButton97;
          Panel7: TPanel;
          QCatProfess: TADOQuery;
          QContrattiNazLK: TADOQuery;
          DSQCatprofess: TDataSource;
          QContrattiNazLKid: TAutoIncField;
          QContrattiNazLKdescrizione: TStringField;
          QCatProfessid: TAutoIncField;
          QCatProfesscategoria: TStringField;
          QCatProfessidcontrattonaz: TIntegerField;
          QCatProfessContrattoNaz: TStringField;
          bTabTipoCatProfess: TToolbarButton97;
          QTipoCatProfess: TADOQuery;
          QTipoCatProfessID: TAutoIncField;
          QTipoCatProfessDescrizione: TStringField;
          QTipoCatProfessCodice: TStringField;
          QCatProfessIDTipoCatProfess: TIntegerField;
          QCatProfessTipoCatProfessLK: TStringField;
          ToolbarButton971: TToolbarButton97;
          BOk: TToolbarButton97;
          Panel1: TPanel;
          dxDBGrid2: TdxDBGrid;
          dxDBGrid2Categoria: TdxDBGridColumn;
          dxDBGrid2ContrattoNaz: TdxDBGridColumn;
          dxDBGrid2Column3: TdxDBGridLookupColumn;
          procedure DsQCatProfessStateChange(Sender: TObject);
          procedure BNewClick(Sender: TObject);
          procedure BDelClick(Sender: TObject);
          procedure BConfermaClick(Sender: TObject);
          procedure BCanClick(Sender: TObject);
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
          procedure FormShow(Sender: TObject);
          procedure bTabTipoCatProfessClick(Sender: TObject);
          procedure QCatProfessBeforeOpen(DataSet: TDataSet);
          procedure ToolbarButton971Click(Sender: TObject);
          procedure BOkClick(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     TabCatProfessForm: TTabCatProfessForm;

implementation

uses MOdulodati, UUtilsVarie, Main;
{$R *.DFM}

procedure TTabCatProfessForm.DsQCatProfessStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsQCatProfess.State in [dsEdit, dsInsert];
     BNew.Enabled := not b;
     BDel.Enabled := not b;
     BConferma.Enabled := b;
     BCan.Enabled := b;
end;

procedure TTabCatProfessForm.BNewClick(Sender: TObject);
begin
     QCatProfess.Insert;
end;

procedure TTabCatProfessForm.BDelClick(Sender: TObject);
begin
     if QCatProfess.IsEmpty then exit;
     if MessageDlg('Sei sicuro di voler cancellare ?', mtWarning, [mbYes, mbNo], 0) = mrNo then exit;
     // dafare integrity check
     QCatProfess.Delete;
end;

procedure TTabCatProfessForm.BConfermaClick(Sender: TObject);
begin
     QCatProfess.Post;
end;

procedure TTabCatProfessForm.BCanClick(Sender: TObject);
begin
     QCatProfess.Cancel;
end;

procedure TTabCatProfessForm.FormClose(Sender: TObject;
     var Action: TCloseAction);
begin
     if DsQCatProfess.state in [dsinsert, dsedit] then QCatProfess.Post;
end;

procedure TTabCatProfessForm.FormShow(Sender: TObject);
begin
    //grafica
     Panel3.color := mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;
     QCatProfess.close;
     QContrattiNazLK.close;
     QTipoCatProfess.close;
     QContrattiNazLK.Open;
     QTipoCatProfess.Open;
     QCatProfess.Open;
end;

procedure TTabCatProfessForm.bTabTipoCatProfessClick(Sender: TObject);
begin
     OpenTab('TipoCatProfess', ['ID', 'Descrizione', 'Codice'], ['Descrizione', 'Codice'], true, true, 'Descrizione', '', 'Tipi Categorie Prof.', 21);
     QTipoCatProfess.close;
     QTipoCatProfess.Open;
end;

procedure TTabCatProfessForm.QCatProfessBeforeOpen(DataSet: TDataSet);
begin
     QContrattiNazLK.close;
     QTipoCatProfess.close;
     QContrattiNazLK.Open;
     QTipoCatProfess.Open;
end;

procedure TTabCatProfessForm.ToolbarButton971Click(Sender: TObject);
begin
     QCatProfess.Cancel;
     TabCatProfessForm.ModalResult := mrCancel;
end;

procedure TTabCatProfessForm.BOkClick(Sender: TObject);
begin
     TabCatProfessForm.ModalResult := mrOk;
end;

end.

