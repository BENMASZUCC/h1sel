unit SelContatti;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Grids, DBGrids, Db, DBTables, StdCtrls, Buttons, ADODB, U_ADOLinkCl,
     dxTL, dxDBCtrl, dxDBGrid, dxCntner, TB97, ExtCtrls;

type
     TSelContattiForm = class(TForm)
          DsQContatti: TDataSource;
          QContatti: TADOLinkedQuery;
          QContattiID: TAutoIncField;
          QContattiContatto: TStringField;
          QContattiTelefono: TStringField;
          QContatti1: TADOQuery;
          Q: TADOQuery;
          Panel1: TPanel;
          BOk: TToolbarButton97;
          BAnnulla: TToolbarButton97;
          SpeedButton1: TToolbarButton97;
          Panel2: TPanel;
          dxDBGrid23: TdxDBGrid;
          dxDBGrid23ID: TdxDBGridMaskColumn;
          dxDBGrid23Contatto: TdxDBGridMaskColumn;
          dxDBGrid23Telefono: TdxDBGridMaskColumn;
          QContattiIDAnagrafica: TIntegerField;
          procedure FormShow(Sender: TObject);
          procedure BOkClick(Sender: TObject);
          procedure BAnnullaClick(Sender: TObject);
          procedure SpeedButton1Click(Sender: TObject);
     private
          { Private declarations }
     public
          xIDCliente: integer;
     end;

var
     SelContattiForm: TSelContattiForm;

implementation
uses ModuloDati, ContattoCli, Main, uutilsvarie;
{$R *.DFM}

procedure TSelContattiForm.FormShow(Sender: TObject);
begin
     Caption := '[S/36] - ' + Caption;
     SelContattiForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
end;

procedure TSelContattiForm.SpeedButton1Click(Sender: TObject);
var xID, xIDAnag, xIDSTato: integer;
begin
     if not MainForm.CheckProfile('103') then Exit;
     ContattoCliForm := TContattoCliForm.create(self);
     ContattoCliForm.Panel1.visible := False;
     COntattoCLiForm.xInsDaRicerca := True;
     ContattoCliForm.ShowModal;
     if ContattoCliForm.ModalResult = mrOK then begin

          Q.Close;
          Q.SQL.clear;
          Q.SQL.text := 'select ID,Descrizione,Comune,Provincia from EBC_Clienti where ID=' + IntToStr(xIDCliente);
          Q.Open;

          with Data do begin
               try
                    Q1.Close;
                    Q1.SQL.text := 'insert into EBC_ContattiClienti (IDCliente,Contatto,Telefono,Fax,Email,Note,DataIns,IDUtente,Titolo,CellularePrivato,Uscito, IDAnagrafica ) ' +
                         'values (:xIDCliente:,:xContatto:,:xTelefono:,:xFax:,:xEmail:,:xNote:,:xDataIns:,:xIDUtente:,:xTitolo:,:xCellularePrivato:,:xUscito:, :xIDAnagrafica: )';
                    Q1.ParamByName['xIDCliente'] := xIDCliente;
                    Q1.ParamByName['xContatto'] := ContattoCliForm.EContattoCognome.text + ' ' + ContattoCliForm.EContattoNome.text;
                    Q1.ParamByName['xTelefono'] := ContattoCliForm.ETelefono.text;
                    Q1.ParamByName['xFax'] := ContattoCliForm.EFax.text;
                    Q1.ParamByName['xEmail'] := ContattoCliForm.EEmail.text;
                    Q1.ParamByName['xNote'] := ContattoCliForm.ENote.text;
                    Q1.ParamByName['xDataIns'] := Date;
                    Q1.ParamByName['xIDUtente'] := MainForm.xIDUtenteAttuale;
                    Q1.ParamByName['xTitolo'] := ContattoCliForm.ETitolo.text;
                    Q1.ParamByName['xCellularePrivato'] := ContattoCliForm.eCellulare.Text;
                    Q1.ParambyName['xUscito'] := ContattoCliForm.CBUscito.Checked;

                    if ContattoCliForm.xIDAnag <> -1 then
                         Q1.ParambyName['xIDanagrafica'] := ContattoCliForm.xIDAnag
                    else
                         Q1.ParambyName['xIDanagrafica'] := NULL;


                    Q1.ExecSQL;

                    Q1.SQL.text := 'select @@IDENTITY as LastID';
                    Q1.Open;
                    xID := Q1.FieldByName('LastID').asInteger;
                    Q1.Close;


                    //if (data.xSelAziende = false) then begin

                         //if MessageDlg('Vuoi inserire questo contatto anche come candidato ?', mtInformation, [mbYes, mbNo], 0) = mrYes then begin

                    if ContattoCliForm.xIDAnag = -1 then begin

                         Q1.Close;
                         Q1.SQL.text := 'select ID from EBC_STati where idtipostato = 17 ';
                         Q1.Open;

                         xIdStato := Q1.Fields[0].Value;


                         Q1.Close;
                         Q1.SQL.Clear;
                         Q1.SQL.text := 'insert into Anagrafica (Cognome,Nome,IDTipoStato,IDStato,RecapitiTelefonici,Fax,Email,Titolo,Cellulare,password,idwebprofile,username) ' +
                              'values (:xCognome:,:xNome:,:xIDTipoStato:,:xIDStato:,:xRecapitiTelefonici:,:xFax:,:xEmail:,:xTitolo:,:xCellulare:,:xpassword:,:xidwebprofile:,:xusername:)';
                         Q1.ParamByName['xCognome'] := ContattoCliForm.EContattoCognome.Text;
                         Q1.ParamByName['xNome'] := ContattoCliForm.EContattoNome.Text;
                         Q1.ParamByName['xIDTipoStato'] := 17; //2;
                         Q1.ParamByName['xIDStato'] := xIdStato; //28;
                         Q1.ParamByName['xRecapitiTelefonici'] := ContattoCliForm.ETelefono.text;
                         Q1.ParamByName['xFax'] := ContattoCliForm.EFax.text;
                         Q1.ParamByName['xEmail'] := ContattoCliForm.EEmail.text;
                         Q1.ParamByName['xTitolo'] := ContattoCliForm.ETitolo.text;
                         Q1.ParamByName['xCellulare'] := ContattoCliForm.eCellulare.Text;
                         Q1.ParamByName['xpassword'] := copy(CreateGuid, 2, 8);
                         Q1.ParamByName['xidwebprofile'] := 5;
                         Q1.ParamByName['xusername'] := GeneraUsername(ContattoCliForm.EEmail.text, ContattoCliForm.EContattoCognome.Text, ContattoCliForm.EContattoNome.Text, '');
                         Q1.ExecSQL;

                         Q1.SQL.text := 'select @@IDENTITY as LastID';
                         Q1.Open;
                         xIDAnag := Q1.FieldByName('LastID').asInteger;
                         Q1.Close;

                         Q1.SQL.Clear;
                         Q1.SQL.text := 'insert into AnagAltreInfo (IDAnagrafica) ' +
                              'values (:xIDAnagrafica:)';
                         Q1.ParamByName['xIDAnagrafica'] := xIDAnag;
                         Q1.ExecSQL;
                              {
                              Q1.SQL.Clear;
                              Q1.SQL.text := 'insert into EsperienzeLavorative (IDAnagrafica,AziendaNome,AziendaComune,AziendaProvincia,Attuale) ' +
                                   'values (:xIDAnagrafica:,:xAziendaNome:,:xAziendaComune:,:xAziendaProvincia:,:xAttuale:)';
                              Q1.ParamByName['xIDAnagrafica'] := xIDAnag;
                              Q1.ParamByName['xAziendaNome'] := Q.FieldByName('Descrizione').asString;
                              Q1.ParamByName['xAziendaComune'] := Q.FieldByName('Comune').asString;
                              Q1.ParamByName['xAziendaProvincia'] := Q.FieldByName('Provincia').asString;
                              Q1.ParamByName['xAttuale'] := True;
                              Q1.ExecSQL;
                                                 }
                         Q1.SQL.Clear;
                         Q1.SQL.text := 'Update EBC_ContattiClienti set IDAnagrafica=:xIDAnagrafica: where ID=' + IntToStr(xID);
                         Q1.ParamByName['xIDAnagrafica'] := xIDAnag;
                         Q1.ExecSQL;
                    end;

                         //end;
                    //end;

                    QContatti.Close;
                    QContatti.Open;
                    QContatti.Locate('ID', xID, []);


               except
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               end;
          end;
     end;
     ContattoCliForm.Free;
end;

procedure TSelContattiForm.BOkClick(Sender: TObject);
begin
     SelContattiForm.ModalResult := mrOk;
end;

procedure TSelContattiForm.BAnnullaClick(Sender: TObject);
begin
     SelContattiForm.ModalResult := mrCancel;
end;

end.

