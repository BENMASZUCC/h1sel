unit uAnnuncioLink2;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     TB97, ExtCtrls, Db, ADODB, dxCntner, dxTL, dxDBCtrl, dxDBGrid,
     StdCtrls,Clipbrd;

type
     TAnnuncioLinkForm2 = class(TForm)
          BitBtn1: TToolbarButton97;
          QLink: TADOQuery;
          DSLink: TDataSource;
          QLinkID: TAutoIncField;
          QLinkDescrizione: TStringField;
          QLinkLink: TStringField;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1ID: TdxDBGridMaskColumn;
          dxDBGrid1Descrizione: TdxDBGridMaskColumn;
          dxDBGrid1Link: TdxDBGridMaskColumn;
          PSotto: TPanel;
          procedure FormShow(Sender: TObject);
          procedure BitBtn1Click(Sender: TObject);
     private
    { Private declarations }
     public
    { Public declarations }
     end;

var
     AnnuncioLinkForm2: TAnnuncioLinkForm2;

implementation

uses main, Modulodati,ModuloDati4;


{$R *.DFM}


 procedure SetClipboardText(const Text: WideString);
var
  Count: Integer;
  Handle: HGLOBAL;
  Ptr: Pointer;
begin
  Count := (Length(Text)+1)*SizeOf(WideChar);
  Handle := GlobalAlloc(GMEM_MOVEABLE, Count);
  Try
    Win32Check(Handle<>0);
    Ptr := GlobalLock(Handle);
    Win32Check(Assigned(Ptr));
    Move(PWideChar(Text)^, Ptr^, Count);
    GlobalUnlock(Handle);
    Clipboard.SetAsHandle(CF_UNICODETEXT, Handle);
  Except
    GlobalFree(Handle);
    raise;
  End;
end;


procedure TAnnuncioLinkForm2.FormShow(Sender: TObject);
var q:TADOQuery;
begin
    //Grafica
     AnnuncioLinkForm2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     PSotto.color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;

     Q:= TADOQuery.Create(self);
     q.Connection := Data.DB;

     Q.SQL.Text := 'select ID,GUIDEdiz from Ann_AnnEdizData where id = :x0';
     Q.Parameters[0].Value := DataAnnunci.QAnnEdizDataXAnn.FieldByName('ID').AsString;
     Q.Open;

     QLink.close;
     QLink.Parameters.ParamByName('xIDAnnuncio').value:= DataAnnunci.QAnnEdizDataXAnn.FieldByName('IDAnnuncio').AsString;
     QLink.Parameters.ParamByName('xIDAnnEdizData').value:= Q.FieldByName('GuidEdiz').asString; //DataAnnunci.QAnnEdizDataXAnn.FieldByName('ID').AsString;
     QLink.open;

     q.Free;

end;

procedure TAnnuncioLinkForm2.BitBtn1Click(Sender: TObject);
begin

    // StrToClipbrd(QLinkLink.AsString);

    SetClipboardText(QLinkLink.asstring);
    Close;
end;

end.

