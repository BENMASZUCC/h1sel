unit Competenze;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  TB97, ExtCtrls, Wall, Grids, DBGrids;

type
  TCompetenzeForm = class(TForm)
    Wallpaper6: TPanel;
    TbBCompNew: TToolbarButton97;
    TbBCompDel: TToolbarButton97;
    TbBCompOK: TToolbarButton97;
    TbBCompCan: TToolbarButton97;
    DBGrid1: TDBGrid;
    ToolbarButton971: TToolbarButton97;
    procedure TbBCompNewClick(Sender: TObject);
    procedure TbBCompDelClick(Sender: TObject);
    procedure TbBCompOKClick(Sender: TObject);
    procedure TbBCompCanClick(Sender: TObject);
    procedure ToolbarButton971Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CompetenzeForm: TCompetenzeForm;

implementation

uses ModuloDati2;

{$R *.DFM}


procedure TCompetenzeForm.TbBCompNewClick(Sender: TObject);
begin
     Data2.TCompetenze.Insert;
end;

procedure TCompetenzeForm.TbBCompDelClick(Sender: TObject);
begin
     if MessageDlg('Sei sicuro di voler cancellare ?',mtWarning,
        [mbNo,mbYes],0)=mrYes then Data2.TCompetenze.Delete;
end;

procedure TCompetenzeForm.TbBCompOKClick(Sender: TObject);
begin
     Data2.TCompetenze.Post;
end;

procedure TCompetenzeForm.TbBCompCanClick(Sender: TObject);
begin
     Data2.TCompetenze.Cancel;
end;


procedure TCompetenzeForm.ToolbarButton971Click(Sender: TObject);
begin
     close
end;

end.
