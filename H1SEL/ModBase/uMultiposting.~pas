unit uMultiposting;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, Buttons, DB, DBClient, Grids, DBGrids,
     Provider, ADODB, Mask, DBCtrls, UCrypter, Registry, JvSimpleXml;

function AssemblaXML(ConnString, DefPath, LogPath: string; xIDAnnEdizData: integer): string;

implementation

function AssemblaXML(ConnString, DefPath, LogPath: string; xIDAnnEdizData: integer): string;
var xDebug, xStringaXML: TStringList;
     xFileDebug, xOperation, xTemplateXML, xQuery, xQueryResult, xObbNonValorizzati,xErrorMsg: string;
     i: integer;
     DB: TADOConnection;
     QPubb, Q: TADOQuery;
     xXML: TJvSimpleXml;
     xElement: TJvSimpleXmlElem;
     xObb: boolean;
begin
     // file di log
     xDebug:=TStringList.create;
     xFileDebug:=LogPath + '\Debug_AssemblaXML.txt';
     xDebug.text:= 'Parametri trasmessi: ' + chr(13) +
          'ConnString = ' + ConnString + chr(13) +
          'DefPath = ' + DefPath + chr(13) +
          'LogPath = ' + LogPath + chr(13) +
          'xIDAnnEdizData = ' + IntToStr(xIDAnnEdizData) + chr(13) + chr(13);

     // connessione al database
     xOperation:= 'Connessione al database';
     try
          DB:=TADOConnection.Create(nil);
          DB.ConnectionString:=ConnString;
          DB.LoginPrompt:=False;
          DB.Connected:=True;

          xDebug.text:=xDebug.text + xOperation + ': OK. ' + chr(13); if LogPath <> '' then xDebug.SaveToFile(xFileDebug);
     except
          on e: Exception do begin
               xDebug.text:=xDebug.text + xOperation + ': ERRORE: ' + e.Message + chr(13); if LogPath <> '' then xDebug.SaveToFile(xFileDebug);
               Result:= 'ERRORE' + chr(13) + xDebug.text;
          end;
     end;

     // recupero l'ID del sito dove pubblicare (obbligatorio)
     xOperation:= 'Apertura template sito';
     try
          QPubb:=TADOQuery.create(nil);
          QPubb.Connection:=DB;
          QPubb.SQL.Text:= 'select AED.ID, S.Template, S.NonSostituireCar ' +
               ' from Ann_AnnEdizData AED ' +
               ' join MP_Siti S on AED.IDMP_Sito = S.ID ' +
               ' where AED.ID=' + IntToStr(xIDAnnEdizData);
          QPubb.Open;
          if QPubb.IsEmpty then begin
               Result:= 'ERRORE: nessun sito impostato per la pubblicazione';
               exit;
          end;

          xTemplateXML:=QPubb.FieldByName('Template').asString;

          xDebug.text:=xDebug.text + xOperation + ': OK > Template = ' + xTemplateXML + '.' + chr(13); if LogPath <> '' then xDebug.SaveToFile(xFileDebug);
     except
          on e: Exception do begin
               xDebug.text:=xDebug.text + xOperation + ': ERRORE: ' + e.Message + chr(13); if LogPath <> '' then xDebug.SaveToFile(xFileDebug);
               Result:= 'ERRORE' + chr(13) + xDebug.text;
          end;
     end;

     Q:=TADOQuery.create(nil);
     Q.Connection:=DB;

     // Elaborazione template XML
     xOperation:= 'Apertura file template XML';
     try
          xStringaXML:=TStringList.create;
          xStringaXML.LoadFromFile(DefPath + '\' + xTemplateXML);
          xDebug.text:=xDebug.text + xOperation + ': OK.' + chr(13); if LogPath <> '' then xDebug.SaveToFile(xFileDebug);
     except
          on e: Exception do begin
               xDebug.text:=xDebug.text + xOperation + ': ERRORE: ' + e.Message + chr(13); if LogPath <> '' then xDebug.SaveToFile(xFileDebug);
               Result:= 'ERRORE' + chr(13) + xDebug.text;
          end;
     end;

     xObbNonValorizzati:= '';

     while pos('#', xStringaXML.text) > 0 do begin
          xOperation:= 'Esecuzione query';
          xQuery:=copy(xStringaXML.text, pos('#', xStringaXML.text) + 1, pos('�', xStringaXML.text) - pos('#', xStringaXML.text) - 1);

          if copy(xQuery, 1, 1) = '$' then begin
               xQuery:=copy(xQuery, 2, length(xQuery));
               xObb:=True;
          end else xObb:=false;

          try
               Q.SQL.clear;
               Q.SQL.Text:=xQuery;
               // parametri
               for i:=0 to Q.Parameters.Count - 1 do begin
                    Q.Parameters[i].Value:=QPubb.FieldByname(copy(Q.Parameters[i].Name, 2, length(Q.Parameters[i].Name))).Value;
               end;
               Q.Open;
               if ((Q.IsEmpty) or (Q.Fields[0].AsString='')) and xObb then begin
                    xErrorMsg:=copy(Q.SQL.Text,pos('ErrorMsg',Q.SQL.Text)+9,length(Q.SQL.Text));
                    xErrorMsg:=copy(xErrorMsg,1,pos('EndErrorMsg',xErrorMsg)-1);
                    xObbNonValorizzati:=xObbNonValorizzati + '- ' + Q.Fields[0].FieldName + ' '+ xErrorMsg + chr(13);
               end;

               xDebug.text:=xDebug.text + xOperation + ': OK > Query = ' + chr(13) + xQuery + chr(13) + 'Risultato query = ' + xQueryResult + '.' + chr(13); if LogPath <> '' then xDebug.SaveToFile(xFileDebug);
          except
               on e: Exception do begin
                    xDebug.text:=xDebug.text + xOperation + ': ERRORE: ' + e.Message + chr(13); if LogPath <> '' then xDebug.SaveToFile(xFileDebug);
                    Result:= 'ERRORE' + chr(13) + xDebug.text;
               end;
          end;

          xQueryResult:= '';
          while not Q.eof do begin
               xQueryResult:=xQueryResult + Q.Fields[0].AsString; // sempre e solo il primo campo
               Q.Next;
          end;
          // tolto su richiesta di Manzoni/MioJob
          //if xQueryResult = '' then xQueryResult:= '(non valorizzato)';

          // controllo tag HTML non autorizzati > per ora vale per tutti !
          if (pos('<HTML>', xQueryResult) > 0) or (pos('<FONT', xQueryResult) > 0) or (pos('<HEAD', xQueryResult) > 0) or (pos('<TITLE', xQueryResult) > 0) or (pos('<BODY', xQueryResult) > 0) then begin
               Result:= 'ERRORE: tag HTML non validi nel campo ' + Q.Fields[0].FieldName;
               exit;
          end;

          // caratteri HTML
          if copy(xStringaXML.text, pos('#', xStringaXML.text) - 6, 6) <> 'CDATA[' then begin
               if (QPubb.FieldByname('NonSostituireCar').asString='') or (QPubb.FieldByname('NonSostituireCar').asString='0') then begin
                    xQueryResult:=StringReplace(xQueryResult, '<', '&lt;', [rfReplaceAll]);
                    xQueryResult:=StringReplace(xQueryResult, '>', '&gt;', [rfReplaceAll]);
               end;
          end;

          if xObb then
               xStringaXML.text:=StringReplace(xStringaXML.text, '#$' + xQuery + '�', xQueryResult, [rfReplaceAll])
          else xStringaXML.text:=StringReplace(xStringaXML.text, '#' + xQuery + '�', xQueryResult, [rfReplaceAll]);
          sleep(10);
     end;

     //xXML:=TJvSimpleXml.Create(nil);
     //xXML.LoadFromFile(DefPath+'\'+xTemplateXML);
     //Result:=xXML.SaveToString;

     if pos('<?xml', xStringaXML.Text) <= 0 then
          xStringaXML.Text:= '<?xml version="1.0" encoding="ISO-8859-1" ?>' + chr(13) + xStringaXML.Text;

     if xObbNonValorizzati <> '' then
          Result:= 'ERRORE ! ci sono elementi obbligatori non valorizzati o non valorizzati correttamente:' + chr(13) + xObbNonValorizzati
     else begin
          // controllo correttezza XML
          xOperation:= 'controllo correttezza XML';

          xXML:=TJvSimpleXml.Create(nil);
          try
               xXML.LoadFromString(xStringaXML.Text);
               Result:=xStringaXML.Text;

          except
               on e: Exception do begin
                    xDebug.text:=xDebug.text + xOperation + ': ERRORE: ' + e.Message + chr(13); if LogPath <> '' then xDebug.SaveToFile(xFileDebug);
                    Result:= 'ERRORE' + chr(13) + xDebug.text;
               end;
          end;

     end;

     xDebug.text:=xDebug.text + 'FINE > Result = ' + chr(13) + Result; if LogPath <> '' then xDebug.SaveToFile(xFileDebug);

     // chiusura
     xStringaXML.Free;
     QPubb.Close;
     QPubb.Free;
     Q.Free;
     DB.Connected:=False;
     DB.Free;
end;

end.

