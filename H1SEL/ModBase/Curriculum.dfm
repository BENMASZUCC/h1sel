�
 TCURRICULUMFORM 0�i TPF0TCurriculumFormCurriculumFormLeftoTop� WidthHeight�HelpContext͆ CaptionCurriculum VitaeColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrder	PositionpoScreenCenterOnClose	FormCloseOnShowFormShowPixelsPerInch`
TextHeight TLabelLabel43Left� Top�WidthDHeightCaptionNomeEdizione  TLabelLabel68LeftiTopbWidth&HeightCaptionBenefits  TPageControlPageControl1Left Top+WidthHeightX
ActivePageTSCVTitoliStudioAlignalClientTabOrder OnChangePageControl1Change
OnChangingPageControl1Changing 	TTabSheetTSCVAltriDatiHelpContext͆ Caption
Altri dati TLabelLabel47LefthTop�Width� HeightCaption&Disponibilita movimento sul territorio  TLabelLabel45LeftTop^Width;HeightCaptionOrario ideale  TLabelLabel54LefthToprWidthoHeightCaptionProvincia/e di interesseFocusControlDBEdit40  TLabelLabel58LefthTop(WidthGHeightCaptionInquadramentoFocusControlDBEdit42  TLabelLabel52LefthTopWidth&HeightCaptionBenefitsFocusControlDBEdit31  TLabelLabel62LeftTop Width4HeightCaptionNazionalit�  TLabelLabel57LeftTop+WidthbHeightCaptionNumero passaporto:   TLabelLabel65LefthTopKWidthOHeightCaptionSettore contrattoFocusControlDBLookupComboBox4  TLabellNoteLefthTop� WidthHeightCaptionNoteVisible  TToolbarButton97SpeedButton2Left�TopYWidthHeightHint"Gestione Tabella tipi retribuzioni
Glyph.Data
:  6  BM6      6  (                                �   �   ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ���                              OnClickSpeedButton2Click  TToolbarButton97BAziendaLeftGTopWidthHeightHintVedi/seleziona Azienda cliente
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 33      33wwwwww33�����33?�??�33  �33wssw733�����33?��?�33 � �33w7sw733�����33�3?��33 �� ��33w?�w�730� ����373w77��3�����3s73s7770������7�373s370������7�3s73?� ����  w�33�ww࿿� ��w�37w?7���w�s?ss�࿿ � 3ww��w�w3��    3wwwwwws3	NumGlyphsOnClickBAziendaClick  	TGroupBox	GroupBox1Left� Top�Width� Height'CaptionMezziTabOrder TLabelLabel1Left� TopWidthHeightCaptionTipo:FocusControlDBEdit4  TDBCheckBoxDBCheckBox1LeftUTopWidth;HeightCaptionPatente	DataFieldPatente
DataSourcedsAnagAltriDatiTabOrderValueCheckedTrueValueUncheckedFalse  TDBEditDBEdit4Left� TopWidthHeight	DataFieldTipoPatente
DataSourcedsAnagAltriDatiTabOrder  TDBCheckBoxDBCheckBox2LeftTopWidthKHeightCaption
Automunito	DataFieldDispAuto
DataSourcedsAnagAltriDatiTabOrder ValueCheckedTrueValueUncheckedFalse   	TGroupBox	GroupBox2LeftTop=Width^HeightBCaptionServizio Militare di levaTabOrder TLabelLabel2Left
TopWidthHeightCaptionStato  TLabelLabel3Left� TopWidth#HeightCaptionPresso:FocusControlDBEdit7  TLabelLabel4Left	Top*Width'HeightCaptionPeriodo:FocusControlDBEdit8  TLabelLabel5Left� Top+Width HeightCaptionGrado:FocusControlDBEdit9  TDBEditDBEdit7Left� TopWidth� Height	DataFieldServizioMilitareDove
DataSourcedsAnagAltriDatiTabOrder  TDBEditDBEdit8Left7Top(WidthzHeight	DataFieldServizioMilitareQuando
DataSourcedsAnagAltriDatiTabOrder  TDBEditDBEdit9Left� Top(Width{Height	DataFieldServizioMilitareGrado
DataSourcedsAnagAltriDatiTabOrder  TDBComboBoxDBComboBox1Left7TopWidth^Height	DataFieldServizioMilitareStato
DataSourcedsAnagAltriDati
ItemHeightItems.Stringsassoltoda assolvereesentein svolgimento TabOrder    TPanelPanCatProtetteLeftTop� Width^Height-
BevelInnerbvRaised
BevelOuter	bvLoweredTabOrder TLabelLabel6LeftTopWidthTHeightCaptionCategorie protette  TLabelLabel7Left� TopWidthHeightCaptionTipoVisible  TLabelLabel8Left� TopWidthHeightCaption%FocusControlDBEdit11  TDBEditDBEdit11Left� TopWidth-Height	DataFieldPercentualeInvalidita
DataSourcedsAnagAltriDati	MaxLengthTabOrder  TDBComboBoxDBComboBox2LeftTopWidth� Height	DataFieldCateogorieProtette
DataSourcedsAnagAltriDati
ItemHeightItems.Stringsiscrittonon iscritto TabOrder   TDBComboBoxDBComboBox3Left� TopWidth~Height	DataField
Invalidita
DataSourcedsAnagAltriDati
ItemHeightItems.StringsDisabileInvalido del LavoroOrfano di lavoro o di guerra TabOrderVisible   TDBLookupComboBoxDBLookupComboBox2LefthTop�Width\Height	DataFieldDispMovimento
DataSourcedsAnagAltriDatiTabOrder
  TDBEditDBEdit38LeftITopZWidthHeight	DataFieldOrarioIdeale
DataSourcedsAnagAltriDatiTabOrder  	TGroupBox	GroupBox5LeftTopxWidth^Height� CaptionInformazioni C.V.TabOrder TLabelLabel51Left� TopWidthHHeightCaptionInserito in data:Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel53LeftTopWidth;HeightCaptionProvenienza  TLabelLDataRealeCVLeft� TopUWidth2HeightCaptiondata reale:Font.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel37LeftDTopTWidth`HeightCaptionData aggiornamento  TLabelLabel74LeftTop8Width:HeightCaptionDettaglio provenienzaWordWrap	  TToolbarButton97
BTabProvCVLeft� TopWidthHeightHinttabella provenienza
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 33      33wwwwww33�����33?�??�33  �33wssw733�����33?��?�33 � �33w7sw733�����33�3?��33 �� ��33w?�w�730� ����373w77��3�����3s73s7770������7�373s370������7�3s73?� ����  w�33�ww࿿� ��w�37w?7���w�s?ss�࿿ � 3ww��w�w3��    3wwwwwws3	NumGlyphsOnClickBTabProvCVClick  TDbDateEdit97DbDateEdit971Left� TopWidth� HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder ColorCalendar.ColorValidclBlueDate      ��@DayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanClear
doCanPopup
doIsMaskeddoShowCanceldoShowToday OnChangeDbDateEdit971Change	DataFieldCVinseritoInData
DataSourceData.DsAnagrafica  TDBComboBox
DBCBProvCVLeftTopWidth� Height	DataFieldCVProvenienza
DataSourcedsAnagAltriDati
ItemHeightTabOrder  TDbDateEdit97DbDEDataRealeCVLeft� TopdWidth� HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderColorCalendar.ColorValidclBlueDate      ��@DayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanClear
doCanPopup
doIsMaskeddoShowCanceldoShowToday 	DataFieldCVDataReale
DataSourceData.DsAnagrafica  TDBCheckBoxDbCbNoCVLeftTopfWidth7HeightCaptionNo CV	DataFieldNoCV
DataSourcedsAnagAltriDatiTabOrderValueCheckedTrueValueUncheckedFalseVisible  TDbDateEdit97DbDateEdit973LeftDTopeWidth� HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderColorCalendar.ColorValidclBlueDate      ��@DayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanClear
doCanPopup
doIsMaskeddoShowCanceldoShowToday 	DataField	CVDataAgg
DataSourceData.DsAnagrafica  TDBEditDBEdit57LeftQTop:WidthHeight	DataFieldSpecCVProvenienza
DataSourcedsAnagAltriDatiTabOrder   	TGroupBox	GroupBox6LefthTop�WidthHeight� TabOrderVisible TLabelLabel48LeftTop6WidthtHeightCaptionin Cigs da almeno 6 mesi  TLabelLabel49LeftToppWidth� HeightCaption con trattam.spec. di disoccupaz.  TDBCheckBoxDBCheckBox4LeftTop
WidthaHeightCaptionin mobilit�	DataFieldMobilita
DataSourcedsAnagAltriDatiTabOrder ValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox5LeftTopWidthyHeightCaptionin cassa integrazione	DataFieldCassaIntegrazione
DataSourcedsAnagAltriDatiTabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox6LeftTop~WidthaHeightCaptiondisp. Part-time	DataFieldDispPartTime
DataSourcedsAnagAltriDatiTabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox7LeftTop� WidthaHeightCaptiondisp. Interinale	DataFieldDispInterinale
DataSourcedsAnagAltriDatiTabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox11LeftTop'Width� HeightCaption CIGS da almeno 3 anni da imprese	DataFieldCassaIntegrStraord3
DataSourcedsAnagAltriDatiTabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox12LeftTopDWidth� HeightCaptionCIGS da almeno 24 mesi	DataFieldCassaIntegrStraord24
DataSourcedsAnagAltriDatiTabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox13LeftTopSWidth� HeightCaptionDisoccupato da almeno 24 mesi	DataFieldDisoccupato24
DataSourcedsAnagAltriDatiTabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox14LeftTopbWidth� HeightCaptionDisoccupato da almeno 12 mesi	DataFieldDisoccupato12
DataSourcedsAnagAltriDatiTabOrderValueCheckedTrueValueUncheckedFalse   TDBEditDBEdit40LefthTop�Width\Height	DataFieldProvInteresse
DataSourcedsAnagAltriDatiTabOrder  TDBEditDBEdit42LefthTop6Width\Height	DataFieldInquadramento
DataSourcedsAnagAltriDatiTabOrder  	TGroupBox	GroupBox7LeftTopWidth� Height6CaptionRiferimento annuncioTabOrder	 TToolbarButton97SpeedButton1LeftTopWidth� Height#HintTrova annuncioCaptionAnnunci associati
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 33      33wwwwww33�����33?�??�33  �33wssw733�����33?��?�33 � �33w7sw733�����33�3?��33 �� ��33w?�w�730� ����373w77��3�����3s73s7770������7�373s370������7�3s73?� ����  w�33�ww࿿� ��w�37w?7���w�s?ss�࿿ � 3ww��w�w3��    3wwwwwws3	NumGlyphsOnClickSpeedButton1Click   TRadioGroupRGProprietaLeft� TopWidth� Height6CaptionPropriet� CV	ItemIndex Items.Stringsnostroazienda cliente TabOrderVisibleOnClickRGProprietaClick  TDBEditDBEdit31LefthTopWidth\Height	DataFieldBenefits
DataSourcedsAnagAltriDatiTabOrder  TDBEditDBEdit46LeftTopWidth� Height	DataFieldNazionalita
DataSourcedsAnagAltriDatiTabOrder   �TMultiDBCheckBoxFrameMultiDBCheckBoxFrame1LeftTop� Width^Height� TabOrder �TRxCheckListBoxCLBoxWidthBHeight�   �TPanel	PanTitoloWidth^  �TPanelPanel1LeftBHeight�    TButtonButton1Left,Top�WidthKHeightCaptionButton1TabOrderVisible  TDBEditDBEdit50LeftsTop(Width� Height	DataFieldNumPassaporto
DataSourcedsAnagAltriDatiTabOrder  	TGroupBox	GroupBox8LefthTop� Width\HeightNCaptionRetribuzioneFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder TLabelLabel59LeftTopWidth)HeightCaption
Fissa a.l.FocusControlDBEdit43Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel61Left� TopWidthNHeightCaptionTipo retribuzioneFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel63LeftTop9Width9HeightCaptionVariabile a.l.Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel64Left� Top9WidthHeightCaptionTotaleFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TToolbarButton97SpeedButton3Left@TopWidthHeightHint"Gestione Tabella tipi retribuzioni
Glyph.Data
:  6  BM6      6  (                                �   �   ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ���                              OnClickSpeedButton3Click  TDBEditDBEdit43LeftTopWidth� Height	DataFieldRetribuzione
DataSourcedsAnagAltriDatiFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   TDBLookupComboBoxDBLookupComboBox1Left� TopWidthcHeight	DataField	TipoRetib
DataSourcedsAnagAltriDatiFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBEditDBEdit51LeftHTop5WidthiHeight	DataFieldRetribVariabile
DataSourcedsAnagAltriDatiFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBEditDBEdit52Left� Top5WidthwHeight	DataFieldRetribTotale
DataSourcedsAnagAltriDatiFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TdxDBCurrencyEditRetribIntermediaLeftTop(WidthsFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderVisible	DataFieldRetribuzione
DataSourcedsAnagAltriDatiDisplayFormat,0.00;-,0.00NullableStoredValues   TdxDBCurrencyEditRetribVarIntermediaLeftMTop@WidthbFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderVisible
DataSourcedsAnagAltriDatiNullable  TdxDBCurrencyEditRetribTotIntermediaLeft� Top@WidthCFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderVisible
DataSourcedsAnagAltriDatiNullable   TDBLookupComboBoxDBLookupComboBox4LefthTopZWidth9Height	DataFieldContrattoSettore
DataSourcedsAnagAltriDatiTabOrder  TDBMemoeNotecvLefthTop� Width\Height	DataFieldNoteAltriDati
DataSourcedsAnagAltriDati	MaxLength2TabOrderVisible  �TMultiDBCheckBoxFrameMultiDBCheckBoxFrame2LefthTopWidth,Height� TabOrder �TRxCheckListBoxCLBoxWidthHeight�   �TPanel	PanTitoloWidth,  �TPanelPanel1LeftHeight�    	TGroupBox	GroupBox9Left�Top�WidthIHeightQCaption	GroupBox9TabOrderVisible TDBCheckBoxDBCheckBox24LeftTop8Width� HeightCaption)Disponibilit� avvicinamento a Sede lavoro	DataFieldDispAvvicinSede
DataSourcedsAnagAltriDatiTabOrder ValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox15LeftTopWidth� HeightCaption%Disponibilit� a trasferte giornaliere	DataFieldDispTraferteGiorn
DataSourcedsAnagAltriDatiTabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox16LeftTop(Width� HeightCaptionDisponibilit� al pernottamento	DataFieldDispPernottamento
DataSourcedsAnagAltriDatiTabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox17LeftTopHWidth� HeightCaptionDisponibilit� al trasferimento	DataFieldDisptrasferimento
DataSourcedsAnagAltriDatiTabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox9LeftTopXWidth� HeightCaptionAbilitazione professione	DataFieldAbilitazProfessioni
DataSourcedsAnagAltriDatiTabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox10LeftTophWidthuHeightCaptionTemporary Manager	DataFieldTemporaryMG
DataSourcedsAnagAltriDatiTabOrderValueCheckedTrueValueUncheckedFalse   �TMultiDBCheckBoxFrameMultiDBCheckBoxFrame3LeftiTop�Width,HeightqTabOrder �TRxCheckListBoxCLBoxWidthHeight\  �TPanel	PanTitoloWidth,  �TPanelPanel1LeftHeight\    	TTabSheetTSCVTitoliStudioHelpContextΆ Caption
Accademici TDBCtrlGridDBCtrlGrid1Left TopWidthHeight�AlignalTopAllowInsertColCount
DataSourceData.DsTitoliStudioPanelHeight� 
PanelWidth�TabOrder RowCountSelectedColorclInfoBk	ShowFocus TLabelLabel9Left3Top2WidthyHeightCaptionIstituto/Scuola/Universit�FocusControlDBEdit6  TLabelLabel10LeftNTopWidth/HeightCaption	Votazione  TLabelLabel12LeftTop2WidthNHeightCaptionSpecializzazioneFocusControlDBEdit13  TLabelLabel16Left� Top2WidthxHeightCaptionData/periodo conseguim.FocusControlDBEdit15  TLabelLabel11LeftTopWidthHeightCaptionTitolo  TLabelLabel14LeftTop2WidthwHeightCaptionAbilitazione ProfessionaleFocusControlDBEdit14  TLabelLabel72Left
TopYWidth'HeightCaptionNazione  TLabelLabel73Left� TopjWidthHeightCaptionNote  TDBEditDBEdit6Left3TopAWidth� Height	DataFieldLuogoConseguimento
DataSourceData.DsTitoliStudioTabOrder  TDBCheckBoxDBCheckBox3Left�TopWidthTHeightCaptionLaurea breve	DataFieldLaureaBreve
DataSourceData.DsTitoliStudioTabOrderValueCheckedTrueValueUncheckedFalse  TDBEditDBEdit13LeftTopAWidth� Height	DataFieldSpecializzazione
DataSourceData.DsTitoliStudioTabOrder  TDBEditDBEdit15Left� TopAWidthuHeight	DataFieldDataConseguimento
DataSourceData.DsTitoliStudioTabOrder  TDBEditDBEdit14LeftTopAWidth� Height	DataFieldAbilitazProfessionale
DataSourceData.DsTitoliStudioTabOrder  TDBEditDBEdit44Left� TopWidth�Height	DataFieldDescrizione
DataSourceData.DsTitoliStudioFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontReadOnly	TabOrder  TDBEditDBEdit10LeftOTopWidthAHeight	DataField	Votazione
DataSourceData.DsTitoliStudioTabOrder  TDBEditDBEdit49Left	TopWidth� Height	DataFieldTipo
DataSourceData.DsTitoliStudioFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontReadOnly	TabOrder   TDBEditDBEdit56LeftTopiWidth� Height	DataFieldNazioneConseg
DataSourceData.DsTitoliStudioTabOrder  TDBMemoDBMemo2Left� Top[WidthHeight.	DataFieldNote
DataSourceData.DsTitoliStudioTabOrder	  TDBCheckBoxDBCheckBox26Left�TopWidthYHeight	AlignmenttaLeftJustifyCaptionMassimo titolo	DataFieldMassimoTitolo
DataSourceData.DsTitoliStudioFont.CharsetDEFAULT_CHARSET
Font.Color� @ Font.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder
ValueCheckedTrueValueUncheckedFalse  TDBEditDBEdit67LeftTopqWidth� Height	DataFieldNazioneConseg
DataSourceData.DsTitoliStudioTabOrder   TPanelPanel9Left Top WidthHeightAlignalTopTabOrder TToolbarButton97TbBTitoliNewLeftATopWidth2HeightCaptionNuovoOpaqueOnClickTbBTitoliNewClick  TToolbarButton97TbBTitoliDelLeftsTopWidth2HeightCaptionEliminaOpaqueOnClickTbBTitoliDelClick  TToolbarButton97TbBTitoliOKLeft�TopWidth2HeightCaptionOKEnabledOpaqueOnClickTbBTitoliOKClick  TToolbarButton97TbBTitoliCanLeft�TopWidth2HeightCaptionAnnullaEnabledOpaqueOnClickTbBTitoliCanClick  TToolbarButton97ToolbarButton972Left� TopWidthOHeightCaptionDettaglio tesiOnClickToolbarButton972Click  TLabelLabel15LeftTopWidthcHeightCaptionTitoli di studioFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TPanelPanel5Left� TopWidthVHeight
BevelOuterbvNoneTabOrder  TToolbarButton97
BTitoloITALeftTopWidth(HeightCaptionITAFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontOnClickBTitoloITAClick  TToolbarButton97
BTitoloENGLeft+TopWidth(HeightCaptionENGFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontOnClickBTitoloENGClick     	TTabSheetTSAlbiOrdiniCaptionAlbi/Ordini
ImageIndex
 TPanelPanel16Left Top WidthHeightAlignalTop	AlignmenttaLeftJustifyBorderStylebsSingleCaption  Albi / OrdiniColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder   TDBCtrlGridDBCtrlGrid5Left TopWidthHeight	AlignalClientColCount
DataSourceDSanagcatprofessPanelHeight� 
PanelWidth�TabOrderRowCountSelectedColor���  TLabelLabel77Left
TopWidth9HeightCaptionAlbo/Ordine  TLabelLabel78LeftTop%WidthEHeightCaptionData iscrizione  TLabelLabel79LeftTop@Width:HeightCaptionIstituto abilit.  TLabelLabel80LeftTopXWidth1HeightCaptionProv. abilit  TLabelLabel81Left� TopsWidth8HeightCaptionData Equip.  TLabelLabel82Left�TopWidthHeightCaptionTipo  TLabelLabel83Left[Top[WidthPHeightCaptionCitt� estera abilit.  TLabelLabel84LeftnTop%WidthDHeightCaptionN� Abilitazione  TLabelLabel85Left}Top@Width0HeightCaptionCitt� abilit.  TLabelLabel86Left�TopsWidth-HeightCaption	N� Equip.  TdxDBLookupEditdxDBLookupEdit1Left`TopWidth� TabOrder 	DataFieldCatProfessLK
DataSourceDSanagcatprofessLookupKeyValue   TdxDBDateEditdxDBDateEdit2Left`Top!WidthaTabOrder	DataField
DataConseg
DataSourceDSanagcatprofess  TDBEditDBEdit58Left`Top;Width� Height	DataFieldIstitutoConseg
DataSourceDSanagcatprofessTabOrder  TDBEditDBEdit59Left`TopUWidthYHeight	DataField
ProvConseg
DataSourceDSanagcatprofessTabOrder  TDBCheckBoxDBCheckBox25LeftToprWidthVHeightCaptionEquipollente	DataFieldEquipollente
DataSourceDSanagcatprofessTabOrderValueCheckedTrueValueUncheckedFalse  TdxDBDateEditdxDBDateEdit3Left� ToppWidthaTabOrder	DataField	DataEquip
DataSourceDSanagcatprofess  TDBEditDBEdit60Left�TopUWidth� Height	DataFieldCittaEsteraConseg
DataSourceDSanagcatprofessTabOrder  TDBEditDBEdit61Left�TopWidth� Height	DataFieldTipoLK
DataSourceDSanagcatprofessTabOrder  TDBEditDBEdit62Left�Top!Width� Height	DataFieldNumAbil
DataSourceDSanagcatprofessTabOrder  TDBEditDBEdit63Left�Top;Width� Height	DataFieldLuogoConseg
DataSourceDSanagcatprofessTabOrder	  TDBEditDBEdit64Left�TopoWidth� Height	DataFieldNumEquip
DataSourceDSanagcatprofessTabOrder
   TPanelPanel12Left TopWidthHeight)AlignalBottom
BevelOuterbvNoneTabOrder TToolbarButton97BAggiungiAlbiLeft	TopWidthQHeight!CaptionAggiungi
Glyph.Data
:  6  BM6      6  (                                     ��� � � Q�m �. �̟ �6 Q 8mF 2�P �> qÅ 5�X K�c �1 �4 a�w �9 �ƕ 	]  >�] ?�Y W�n E�d �F (�G �3 �0 �5 l�� <�^ ]�v E�` f�| K�f M�f Z�r v�� :�[ /�T X ?�a �1 3kB 
�1 �2 �? ?�Z e�{ �2 �7 U�p �5 ,�I Q 	X 
�/ 
�3 <�X �3 Z�o [ �5 
�0 6kE �0 �/ Y�o �3 �5 Y u�� �6 5lC �/ �1 L�h F�e 
Z ^�v �2 �/ �/ �/ 	�3 4�P �7 �7 �4 
�1 �1 	�2 Q Q [ �3 �. �. �/ �0 	Y 	] 5kC 4kB �5 �4 �0 �0 �/ �1 ?�Y                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     f]666@]STTS6]cEhc6]B4>l6]KW2m]f\\\]]kV]]6667nU5aYW98bA`Jd^OL).;XHDiP[Z=M1__-?*(# :'
,QQjRNg\\\\\/&]]]]]I\#3"	]\<!$\\0%\GC\+FeeFfOnClickBAggiungiAlbiClick  TToolbarButton97bEliminaAlbiLeftZTopWidthQHeight!CaptionElimina
Glyph.Data
:  6  BM6      6  (                                     ��� � � � }}� u JJ� ��� 		� ��� 

H ii� --� 11� � ii� � 00� JJ� � YY� ] xx� ;;� >>� � $$� � vv� � 77� ((� � g 

i @@� KK� aa� nn� ��� kk� � O QQ� ""� ���   | BB� 55� � ##� � pp� --� oo� 88� DD� vv� rr� 00� o � %%� !!� GG� V � tt� 		W c � VV� f nn� 11� m � DD� q � {{� � ZZ� � � MM� nn� x � rr� __� ff� �   c � � BB� � � ll� ##� TT�   r p ''� � !!� � � X d e j tt� tt� nn� ii� ii�   � 

� � � O pp� m u pp� h z s � � &&� � � 

� � {{� rr� nn� nn� ""� ��� U qq� g qq� qq� � � ,,� ''� � � � � 		H ss� f � � � rr� rr� rr� rr� p qq� nn� nn� nn� t nn�  � ii� � ''� � 

� � � � � � �                                                                                                                                                                                                                                                                                         �A{o��!�������V�]Q��WVP�_l1��*^S�xX��B=)wnr] ���L��jFT�~�p�O�d�v,���<��,�5�h3B&:fa���>����g7?b�ki�u��K$U��y�����HRe+8J\.�qt%ZG�8`2}c
(-C6[NI��@#0;�D'	"��|M��m9Es��/���4rzYOnClickbEliminaAlbiClick  TToolbarButton97bAnnullaAlbiLeftTopWidthPHeight!CaptionAnnullaEnabled
Glyph.Data
F  B  BMB      B   (                                   |  �     �II�8�<)U)UMI�D�@�4�0�0M�<��5�5�5�5�5�5�5�5�5�5�5�5�5�5�5��b�eJYJ]�n�r1j�e�ak])Y�DIfI��5�5�5�5�5�5�5�5�5�5�5�5�5�5�5���n�aQ�rssj�e�aJ])Q�@)Y�a�D���5�5�5�5�5�5�5�5�5�5�5�5�5�5���n�n)U�nZw�nf�aJ]Q�<�eI����5�5�5�5�5�5�5�5�5�5�5�5�5����s�n1jZw�r1j�aJY�D)QJ]�,�����5�5�5�5�5�5�5�5�5�5�5�5�����f9wRj9w�rRj�a)U�@Q�8������5�5�5�5�5�5�5�5�5�5�5������k1j�r�rRj�a)Q�4�<��������5�5�5�5�5�5�5�5�5���������1j�rRj�a�8)E�����������5�5�5�5�5�5�����������JYJ]IIE�������������5�5�5�5������������0�0�@I�D������������5�5�5�5�5����������)M�8)U)YJ]J]�@����������5�5�5�5�5�5�5���������M�a�ak]�a�aM����������5�5�5�5�5�5�5����������a�r�n�eJ])U�D����������5�5�5�5�5�5�5���������k]s9w1j)YI�(����������5�5�5�5�5�5�5�����������asn�a)UcA�����������5�5�5�5�5�5������������a)U�@�M�������������5�5�5�5�����	NumGlyphsOnClickbAnnullaAlbiClick  TToolbarButton97BConfermaAlbiLeft� TopWidthVHeight!CaptionConfermaEnabled
Glyph.Data
F  B  BMB      B   (                                   |  �     ��%�!�!:2�)�!o!0���)��5�5�5�5�5�5�5�5�5�5�5�5�5�5�5�yc�>�m6zWzW�J�>v>�)�%1o!�6S��5�5�5�5�5�5�5�5�5�5�5�5�5�5�5��S�!*�[ycO�Bv>�)�%/�%]*o!���5�5�5�5�5�5�5�5�5�5�5�5�5�5��WzW�)S�kW�Fv>42�!]*����5�5�5�5�5�5�5�5�5�5�5�5�5�����[zW?�k�[�Jv>2o!�!*������5�5�5�5�5�5�5�5�5�5�5�5����yc�[?�[yc�N�B./�%,������5�5�5�5�5�5�5�5�5�5�5�������k�:S�kW�F�-�!��������5�5�5�5�5�5�5�5�5����������F�gS_�F�!1F�����������5�5�5�5�5�5�����������m6L>�11F�������������5�5�5�5�����������+�3������������5�5�5�5�5����������06��%�%�!�3����������5�5�5�5�5�5�5���������P&?�>&�!�V����������5�5�5�5�5�5�5����������2�WzWv:�%�1����������5�5�5�5�5�5�5����������2�[�k�N2p!�����������5�5�5�5�5�5�5�����������:sg�V:��5�����������5�5�5�5�5�5�����������o>L>�1�V�������������5�5�5�5�����	NumGlyphsOnClickBConfermaAlbiClick  TToolbarButton97btabTipoCatprofessLeft�TopWidthiHeight!CaptionTab. Categorie Prof.
Glyph.Data
F  B  BMB      B   (                                  |  �     2F2B2B2B2B2B2B1B1B1BBBBB>>RF����2B����1B����>SF��w�w�2B��{�{�1B��{�{�BSF��w�w�2B��w�{�1B��{�{�BSF����2F����2B����BSFSFSFSFRFRF2F2B2B2B2B2B1B1B1B1BSF����RF����2B����1BSF��w�w�SF��w�w�2B��{�{�1BtF��w�w�SF��w�w�2B��w�{�2BtJ����SF����RF����2BtJtJtFtFSFSFSFSFSFSFRFRF2B2B2B2BtJ����SF����SF����2BuJ��w�s�tF��s�w�SF��w�w�2B�J��s�s�tF��s�s�SF��w�w�2F�J����tJ����SF����RF�J�J�J�JuJtJtJtJtFSFSFSFSFSFSFSFWordWrap	OnClickbtabTipoCatprofessClick    	TTabSheetTSCVCorsiStageHelpContextφ CaptionCorsi-Stage TDBCtrlGridDBCtrlGrid2Left TopWidthHeight�AlignalTopAllowInsertColCount
DataSourceData.DsCorsiExtraPanelHeight`
PanelWidth�TabOrder RowCountSelectedColorclInfoBk	ShowFocus TLabelLabel13LeftTopWidth+HeightCaption	Tipologia  TLabelLabel17Left� TopWidthHHeightCaptionOrganizzazioneFocusControlDBEdit17  TLabelLabel18LeftTop,Width� HeightCaptionTitolo/Descrizione/ArgomentiFocusControlDBMemo1  TLabelLabel20Left�Top7Width>HeightCaptionGiorni (stage)FocusControlDBEdit19  TLabelLabel22Left&TopWidth;HeightCaptionDurata (ore):  TLabelLabel21Left� Top7Width&HeightCaptionAziendaFocusControlDBEdit20  TLabelLabel32Left�Top8WidthbHeightCaptionConseguito nell'annoFocusControlDBEdit34  TDBComboBoxDBComboBox4LeftTopWidth� Height	DataField	Tipologia
DataSourceData.DsCorsiExtraFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ItemHeightItems.StringsFormazione/SpecializzazioneMasterDottorato di Ricerca/Ph DStage 
ParentFontTabOrder   TDBEditDBEdit17Left� TopWidthIHeight	DataFieldOrganizzazione
DataSourceData.DsCorsiExtraTabOrder  TDBMemoDBMemo1LeftTop;Width� Height 	DataField	Argomenti
DataSourceData.DsCorsiExtraTabOrder  TDBEditDBEdit18Left&TopWidth<Height	DataField	DurataOre
DataSourceData.DsCorsiExtraTabOrder  TDBEditDBEdit19Left�TopFWidth>Height	DataFieldDurataStageGiorni
DataSourceData.DsCorsiExtraTabOrder  TDBEditDBEdit20Left� TopFWidth� Height	DataFieldNomeAzienda
DataSourceData.DsCorsiExtraTabOrder  TDBEditDBEdit34Left�TopFWidthcHeight	DataFieldConseguitoAnno
DataSourceData.DsCorsiExtraTabOrder   TPanelPanel2Left Top WidthHeightAlignalTopTabOrder TToolbarButton97TbBCorsiExtraNewLeftATopWidth2HeightCaptionNuovoOpaqueOnClickTbBCorsiExtraNewClick  TToolbarButton97TbBCorsiExtraDelLeftsTopWidth2HeightCaptionEliminaOpaqueOnClickTbBCorsiExtraDelClick  TToolbarButton97TbBCorsiExtraOKLeft�TopWidth2HeightCaptionOKEnabledOpaqueOnClickTbBCorsiExtraOKClick  TToolbarButton97TbBCorsiExtraCanLeft�TopWidth2HeightCaptionAnnullaEnabledOpaqueOnClickTbBCorsiExtraCanClick  TLabelLabel23LeftTopWidth� HeightCaptionCorsi o stage c/o aziendeFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont    	TTabSheet
TSCVLingueHelpContextІ CaptionLingue TDBCtrlGridDBCtrlGrid3Left TopWidthHeight�AlignalTopAllowInsertColCount
DataSourceData.DsLingueConoscPanelHeight3
PanelWidth�TabOrder RowCountSelectedColorclInfoBk	ShowFocus TLabelLabel24LeftTopWidth HeightCaptionLinguaFocusControlDBEdit16  TLabelLabel25Left� TopWidthGHeightCaptionLivello (parlato)  TLabelLabel26Left@TopWidth0HeightCaption	SoggiornoFocusControlDBEdit22  TLabelLabel60LeftNTopWidthCHeightCaptionLivello (scritto)  TLabelLabel90Left�TopWidthiHeightCaptionLivello (comprensione)  TDBEditDBEdit16LeftTopWidth� Height	DataFieldLingua
DataSourceData.DsLingueConoscFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontReadOnly	TabOrder   TDBEditDBEdit22Left?TopWidth� Height	DataField	Soggiorno
DataSourceData.DsLingueConoscReadOnly	TabOrder  TDBEditDBEdit21Left� TopWidthhHeight	DataFieldDescLivello
DataSourceData.DsLingueConoscReadOnly	TabOrder  TDBEditDBEdit45LeftMTopWidthiHeight	DataFieldDescLivelloScritto
DataSourceData.DsLingueConoscReadOnly	TabOrder  TDBEditDBEdit66Left�TopWidthiHeight	DataField DescLivelloComprensione
DataSourceData.DsLingueConoscReadOnly	TabOrder   TPanelPanel1Left Top WidthHeightAlignalTopTabOrder TToolbarButton97TbBLinConNewLeftcTopWidth6HeightCaptionNuovoOpaqueOnClickTbBLinConNewClick  TToolbarButton97TbBLinConDelLeft�TopWidth6HeightCaptionEliminaOpaqueOnClickTbBLinConDelClick  TToolbarButton97TbBLinConModLeft�TopWidth6HeightCaptionModificaOpaqueOnClickTbBLinConModClick  TLabelLabel30LeftTopWidth~HeightCaptionLingue conosciuteFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TToolbarButton97
BLinguaENGLeft� TopWidth(HeightCaptionENGFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontOnClickBLinguaENGClick  TToolbarButton97
BLinguaITALeft� TopWidth(HeightCaptionITAFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontOnClickBLinguaITAClick    	TTabSheet
TSCVespLavHelpContextц CaptionEsperienze Lav. TPanelPanel3Left Top WidthHeight)AlignalTopTabOrder  TToolbarButton97TbBEspLavDelLeft� TopWidth5Height'CaptionEliminaOpaqueWordWrap	OnClickTbBEspLavDelClick  TToolbarButton97TbBEspLavModLeftTopWidthRHeight'Hintmodifica azienda e ruoloCaptionModifica azienda/settoreOpaqueWordWrap	OnClickTbBEspLavModClick  TToolbarButton97ToolbarButton9710Left�TopWidth2Height'Captionaltri dettagliWordWrap	OnClickToolbarButton9710Click  TLabelLabel34LeftTopWidthSHeight CaptionEsperienze lavorativeFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontWordWrap	  TToolbarButton97TbBEspLavNewLeft� TopWidth2Height'CaptionNuovaOpaqueWordWrap	OnClickTbBEspLavNewClick  TToolbarButton97ToolbarButton975LefteTopWidth7Height'Hintmodifica azienda e ruoloCaptionModifica ruoloOpaqueWordWrap	OnClickToolbarButton975Click  TToolbarButton97BEspLavRetribLeft�TopWidth9Height'Hint9voci retributive di dettaglio
per questa esperienza lav.Captionvoci retributiveFont.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontParentShowHintShowHint	WordWrap	OnClickBEspLavRetribClick  TToolbarButton97ToolbarButton976LeftTopWidth9Height'Hint/qualifica e livello
per questa esperienza lav.CaptionQualifica LivelloFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontParentShowHintShowHint	WordWrap	OnClickToolbarButton976Click  TToolbarButton97
BEspLavITALeftYTopWidth(HeightCaptionITAFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontOnClickBEspLavITAClick  TToolbarButton97
BEspLavENGLeft� TopWidth(HeightCaptionENGFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontOnClickBEspLavENGClick   TPanelPanEspProfMaturataLeft TopWidthHeight!AlignalBottom
BevelOuter	bvLoweredTabOrderVisible TLabelLabel41LeftTopWidth� HeightCaption"Esperienza professionale maturata:FocusControlDBLookupComboBox3Font.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TToolbarButton97BEspProfmatITALeft�TopWidth(HeightCaptionITAFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontOnClickBEspProfmatITAClick  TToolbarButton97BEspProfmatENGLeft�TopWidth(HeightCaptionENGFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TDBLookupComboBoxDBLookupComboBox3Left� TopWidth� Height	DataFieldEspProfMaturata
DataSourcedsAnagAltriDatiFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder    TDBCtrlGridDBCtrlGrid4Left Top)WidthHeight�AlignalClientAllowInsertColCount
DataSourceData.DsEspLavPanelHeight� 
PanelWidth�TabOrderRowCountSelectedColorclInfoBk	ShowFocus TLabelLabel38Left[TopWidth[HeightCaptionDal-Al (mese/anno)FocusControlDBEdit32Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLTipoContrattoLeft[TopgWidtheHeightCaptionQualifica ContrattualeFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLLivelloContrLeft[Top� WidthYHeightCaptionLivello contrattualeFocusControlDBELivelloContrFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel42Left[Top� Width;HeightCaptionRetribuzioneFocusControlDBEdit35Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel44LeftUTop� Width)HeightCaption	MensilitaFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontVisible  TLabel
LMensilitaLeft�Top� Width,HeightCaption
Mensilit�:Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel56Left�TopWidthHeightCaptionMesiFocusControlDBEdit41Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel19LeftTop� Width]HeightCaptionCodifica pers.unica:Font.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontVisible  TLabelLabel40LeftXTop� Width:HeightCaptionTel.azienda:Visible  TLabelLabel39Left�Top#WidthHeightCaption--  TLabelLabel69Left[Top� Width&HeightCaptionBenefitsFocusControlDBEdit35Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel70Left\TopCWidthpHeightCaptionCondizione Contrattuale  TLabelLabel87LeftiTopWidth)HeightCaptionAttualeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  	TGroupBox	GroupBox3LeftTopWidthJHeight� CaptionAziendaEnabledFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TLabelLabel27LeftTopWidth8HeightCaption
Denominaz.FocusControlDBEdit23Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel28LeftTop(Width&HeightCaption	IndirizzoFocusControlDBEdit24Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel29LeftTop>Width'HeightCaptionComuneFocusControlDBEdit25Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel31LeftTop(WidthHeightCaptionProv.FocusControlDBEdit26  TLabelLabel33Left�Top?Width-HeightCaption
Fatturato:FocusControlDBEdit28  TLabelLabel35LeftLTopWWidthHeightCaptionN�Dip.FocusControlDBEdit29Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel71LeftTopTWidthHeightCaptionStato  TDBEditDBEdit23LeftDTopWidth�Height	DataFieldNomeAzienda
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder   TDBEditDBEdit24Left6Top$Width�Height	DataFieldAzIndirizzo
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBEditDBEdit25Left5Top;WidthtHeight	DataFieldComuneAzienda
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBEditDBEdit26LeftTop$Width#Height	DataFieldProvinciaAzienda
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBEditDBEdit28Left�Top;WidthaHeight	DataField	Fatturato
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBEditDBEdit29LeftrTopRWidthHeight	DataFieldNumDipendenti
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBEditDBEdit55Left5TopQWidthHeight	DataFieldAziendaStato
DataSourceData.DsEspLavTabOrder  	TGroupBox
GroupBox10LeftTopiWidth?Height1CaptionSettore e Area settoreTabOrder TDBEditDBEdit27LeftTopWidthHeight	DataFieldSettoreCalc
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder   TDBEditDBEdit65Left&TopWidthHeight	DataFieldAreaSettore
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontReadOnly	TabOrder    TDBEditDBEdit32LeftqTop Width#Height	DataFieldAnnoDal
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBEditDBEdit33Left�Top Width#Height	DataFieldAnnoAl
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBComboBoxDBCTipoContratto_oldLeft\TopuWidth� Height	DataFieldTipoContratto
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ItemHeightItems.Strings
StagionaleApprendistaFormazione LavoroTempo IndeterminatoTempo Determinato&Collaborazione Coordinata ContinuativaPrestatore d'opera occasionale
Consulente 
ParentFontTabOrderVisible
OnDropDownDBCTipoContratto_oldDropDown  TDBEditDBELivelloContrLeft[Top� Width� Height	DataFieldLivelloContrattuale
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontReadOnly	TabOrder  TDBEditDBEdit35Left[Top� WidthqHeight	DataFieldRetribNettaMensile
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder	  	TGroupBox	GroupBox4LeftTop� WidthJHeightVCaptionRuolo e Area ruoloFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder TLabelLabel66LeftTop)Width'HeightCaption
Job title:FocusControlDBEdit53Font.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel67LeftTop+Width(HeightCaptionRegione  TDBEditDBEdit3Left)TopWidthHeight	DataFieldDescrizione_1
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontReadOnly	TabOrder  TDBEditDBEdit5LeftTopWidthHeight	DataFieldDescrizione
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontReadOnly	TabOrder   TDBEditDBEdit53Left4Top'Width� Height	DataFieldRuoloPresentaz
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBCheckBoxDBCheckBox20LeftTop@Width-HeightHintConsiglio di AmministrazioneCaptionCdA	DataFieldFlag_CdA
DataSourceData.DsEspLavParentShowHintShowHint	TabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox21Left6Top@Width\HeightHintCollegio sindacaleCaptionColl.sindacale	DataFieldFlag_CollegioSind
DataSourceData.DsEspLavParentShowHintShowHint	TabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox22Left� Top@Width]HeightHintConsiglio di gestioneCaptionCons.Gestione	DataFieldFlag_ConsiglioGest
DataSourceData.DsEspLavParentShowHintShowHint	TabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox23Left� Top@WidthNHeightHintConsiglio di sorveglianzaCaption
Cons.Sorv.	DataFieldFlag_ConsiglioSorv
DataSourceData.DsEspLavParentShowHintShowHint	TabOrderValueCheckedTrueValueUncheckedFalse  TDBComboBoxDBComboBox5Left>Top(WidthRHeightStylecsDropDownList	DataFieldRegione
DataSourceData.DsEspLav
ItemHeightTabOrderOnChangeDBComboBox5Change   TDBEditDBEMensilitaLeft�Top� WidthHeight	DataField	Mensilita
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder
  TDBEditDBEdit41Left�Top Width)Height	DataField
DurataMesi
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBCheckBoxDBCheckBox8Left�TopWidthHeight	AlignmenttaLeftJustifyCaptionAttuale	DataFieldAttuale
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderValueCheckedTrueValueUncheckedFalse  TDBEditDBEdit30Left�Top� WidthQHeight	DataFieldTitoloMansione
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderVisible  TDBEditDBEdit12Left|Top� Width� Height	DataFieldTelefono
DataSourceData.DsEspLavReadOnly	TabOrderVisible  TDBEditDBEdit36Left[Top WidthHeight	DataFieldMeseDal
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBEditDBEdit37Left�Top WidthHeight	DataFieldMeseAl
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBEditDBEdit54Left[Top� Width� Height	DataFieldBenefits
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBLookupComboBoxDBLookupComboBox5LeftZTopQWidth� Height	DataFieldCondizioneContrattuale
DataSourceData.DsEspLavTabOrder  TDBEditDBCTipoContrattoLeft\TopuWidth� Height	DataFieldTipoContratto
DataSourceData.DsEspLavReadOnly	TabOrder  TDBCheckBoxcbVisibileWebLeft�TopWidthaHeight	AlignmenttaLeftJustifyCaptionVisibile WEB	DataFieldVisibileWeb
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderValueCheckedTrueValueUncheckedFalseVisible    	TTabSheetTSAreeInteresseCaptionAree di Interesse
ImageIndex	 TAdvSplitterAdvSplitter1Left Top!WidthHeightCursorcrVSplitAlignalTopAppearance.BorderColorclNoneAppearance.BorderColorHotclNoneAppearance.ColorclWhiteAppearance.ColorToclSilverAppearance.ColorHotclWhiteAppearance.ColorHotToclGray	GripStylesgDots  TPanelPanel10Left Top WidthHeight)AlignalTopTabOrder  TLabelLabel75LeftTopWidthwHeightCaption!Aree Ruoli Aziendali di interesseFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontWordWrap	  TToolbarButton97NuovaAreaInteresseLeft� TopWidth`Height!Caption
Nuova area
Glyph.Data
:  6  BM6      6  (                                     ��� � � �y8 � {U9 ��� �yf ��� �` �i/ �{T �| �hL �Բ �F � ֌U �pM �~G �_= �c �l �r9 �ʧ �rY ׃E ܛk �P ��� �]A �V ��� ﰁ �x �mQ �ȟ �bE �ub �m5 �~> �Z9 �k- �ϯ ρD �G وK �q2 �w: �Ҷ ޅD ߍQ ߚe �v` �ֻ ��� �K ԈO ۑX �q �V6 �w �h �\< �v; �{= �{` ۋO �`B ؀? ��a �Ү �oU �Ŝ �M ߈G �k2 �z< á� �J ۃB ӀD Š� �H �D �Y: �M ~U6 �s\ ݛh ��Y �H ݎS ݏW �\: � �wd �^B �aC �nR ؂C ܆E �j/ �x �}` �{< �? ��� �q �v; �a �W7 �]= ��� ğ� �~b �}= كB U5 �_B �bD � �cE �i �|< ~V6 U6 �aD ��x �~c �~b                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     vRfWxz%bDwaN
}BF:9,0/*4QL~$-OST2PE(|'o�JV8[KeujtUsq!g;{nZCiY@<�G y_"l>	\.]m)hI+167k=3M^cHX5`&�A?rdp#WordWrap	OnClickNuovaAreaInteresseClick  TToolbarButton97BSalvaAreeInteresseLeftYTopWidthHHeight!CaptionSalva Enabled
Glyph.Data
z  v  BMv      6  (               @                     ��� � � 5�� ЗL NMM ��� m ��� �ȑ BV� ��� on� ��� &9� &&& >�� gw� 3o{ ��� ۯr ��� S�� Ke �� `cc ��s �پ ���  999 j�� Or 9�� #bm ��� uqm Igh ��� ע^ 庂 ��� !�� J�� 2�� $�� dcu �}q  -} WWW ��� CCC ��� ˠi  /// ��� ��� ?�� ��� ~�� ��� Sp ��� jjj +h�  K�� Ž� ɦt ~~~ ͛W 5eu �| ��� ��� P�� *ky ��� ��� ��� 8�� $�� TRP ]^\ ܫj KFE 444 >>> ��� !& FMO Ԧe ��� ��� ��� ̜_ ���  ~zw ��� yur ӠX Nn ��� ]l ��� ̔G �� ��� *** ��� ��� onn 羇 ��� ���      IIJ ��� �u ��� �¿ ^[W ��� M�� ʗT  ��� ћS ½� $'+ 4�� ٧c pli ��� ��� <�� YZ[ [ce G�� ggg ��� FFF �z OOO ��� # ٬o `__ qqq  }�� ̖P  ޮo ��� NII \_` ��� !$( ;;< 6�� '�� ��� �� UUV  $$$ ((( ,,, 111 666 @@@ 2mz ��� p >:= ��� ܰu ΜZ SSS ��� XVU \[[ ccd ��� ��� ��� ���  ��� �� !`l ��� ��� PQR LLJ $�� ��� ��� ��� ��� ��� 878 ��� ~�� BBA EDC ��� HHH ��� 㹁 NNN PPN ڭq XYY \]^ ��� ��� ��� ���   ��� ;:: _m ��� ,ly _`a ppn ��� ��� ��� ��� LLL RRP ���  ��� ���   $ %%% ''' --- ... 000 222 333 555 ��� === ��� ??? DDD ��<N&_����ce$V>gH������]3n��=4p�#A!*RM��ͫ������6=��v�:C,����u�n��6x���� ����i�X�����6mK��v�h�-�+Q�"��7����s��v��į-�~�l�А��&}3v�����-�L�����w��?��ó��{�h���Dq��w�b��򡄔b�BZ[��aOj���II�ؕ\�G`E%�2����	r(�y�U�'f�k�5/d
ж1�t���P.0;31�z�����8^^��Yږ��������)o)��޽�ͪ���������)o)��޽�w�nw��������8^^�ʒX��@����������7�@���������ʧ����F������8^^�ʋ�����ۼ�JJ��|���ٶ1ٹT�������S����9��ww�����X�WWWW���OnClickBSalvaAreeInteresseClick  TToolbarButton97BeliminaAreaLeft� TopWidth`Height!CaptionElimina Area
Glyph.Data
:  6  BM6      6  (                                     ��� � � ��y -.� ��� DC; mn� ��� ��� ^^� __^ ��� IR� ��� CC� ,,' xx� ��� ��� ��� ]h� VV� ��� ��� CC� ��� ��� ��� pm] kk� wup ��� ��� ��� ��� ��� 885 ��� ��� \a� ��w ��� 88� ��� ``� CG� MO� ��� ��� ��� nn� HH� ��� XY� �l yy� S_� ��� ��� <=� uu� ��� >>� hgd df� 46� //� ��� [ZU ss� ��� ||� ��� ��z ai� ��� ��� LN� ��� ��� ��� 33� ss� ��� ��� PV� }}� ��� ��� ��� ��� |{n ||� ``� CC� NN� ee� ��� ��� ��� 79� Xd� ��� ��� \\� 13� JJ� ��� ��� ��� ��v GK� ��� oo� CI� ]]� ��� CC� PZ� ��� hh� ^]W FO� V]� ��� ��� ��� � 66� ��� ��� ��� ��� ��� ��� /3� ��� ]][ nkZ ~}q ��� ah� ��� bh� ��� ��� 36� eec Wa� ��{ ��� @I� GH� ,/� xxq QY� ��� ��� ��� ��� ��� ��� oo� ��� ��� NT� ��� qq� ��� 22� BF� NO� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� 02� 22� �~m ��� ��� ��� ��� ��� 986 ��� ��� 35� 77� ��� ��� ��� ��� DC< //� ��� NN� ��w Yd� ��� ��� ��� ��� ��� ��� ||� ���                                                                                                                                                                 ,%��,�@��''� ʔ��z\bpV||����E��o�s���(AyJ}�:jL�f{$gB����CN��K9w1����R!;2��d0���P7)�kŹ�;lh	.e�+"΂`i<����/Ī�v�I��^t���T?�_c�[6�n��5�4�D�x�r��GH=�Z>���U�3]��Q�S-a
�#umWX��8F���ж�M*&~qMY������O��WordWrap	OnClickBeliminaAreaClick   	TdxDBGrid	dxDBGrid2Left TopMWidthHeight� Bands  DefaultLayout	HeaderPanelRowCountKeyFieldIDSummaryGroups SummarySeparator, AlignalClientTabOrder
DataSourceDSAnagInteressiSviluppoFilter.Criteria
       	OptionsDBedgoCancelOnExitedgoCanDeleteedgoCanInsertedgoCanNavigationedgoConfirmDeleteedgoLoadAllRecordsedgoUseBookmarks OptionsViewedgoAutoWidthedgoBandHeaderWidthedgoIndicatoredgoUseBitmap  TdxDBGridColumndxDBGrid2Column1DisableEditor	WidthT	BandIndex RowIndex 	FieldNameDescrizione  TdxDBGridBlobColumndxDBGrid2Column2Width
	BandIndex RowIndex 	FieldNameNote   TPanelPanel11Left Top$WidthHeight)AlignalTopTabOrder TLabelLabel76LeftTopWidth5HeightCaptionAmbiti di InteresseFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontWordWrap	  TToolbarButton97BNuovoInteressiSviluppoLeft� TopWidth`Height!Caption
Nuova area
Glyph.Data
:  6  BM6      6  (                                     ��� � � �y8 � {U9 ��� �yf ��� �` �i/ �{T �| �hL �Բ �F � ֌U �pM �~G �_= �c �l �r9 �ʧ �rY ׃E ܛk �P ��� �]A �V ��� ﰁ �x �mQ �ȟ �bE �ub �m5 �~> �Z9 �k- �ϯ ρD �G وK �q2 �w: �Ҷ ޅD ߍQ ߚe �v` �ֻ ��� �K ԈO ۑX �q �V6 �w �h �\< �v; �{= �{` ۋO �`B ؀? ��a �Ү �oU �Ŝ �M ߈G �k2 �z< á� �J ۃB ӀD Š� �H �D �Y: �M ~U6 �s\ ݛh ��Y �H ݎS ݏW �\: � �wd �^B �aC �nR ؂C ܆E �j/ �x �}` �{< �? ��� �q �v; �a �W7 �]= ��� ğ� �~b �}= كB U5 �_B �bD � �cE �i �|< ~V6 U6 �aD ��x �~c �~b                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     vRfWxz%bDwaN
}BF:9,0/*4QL~$-OST2PE(|'o�JV8[KeujtUsq!g;{nZCiY@<�G y_"l>	\.]m)hI+167k=3M^cHX5`&�A?rdp#WordWrap	OnClickBNuovoInteressiSviluppoClick  TToolbarButton97BSalvaInteressiSviluppoLeftYTopWidthHHeight!CaptionSalva Enabled
Glyph.Data
z  v  BMv      6  (               @                     ��� � � 5�� ЗL NMM ��� m ��� �ȑ BV� ��� on� ��� &9� &&& >�� gw� 3o{ ��� ۯr ��� S�� Ke �� `cc ��s �پ ���  999 j�� Or 9�� #bm ��� uqm Igh ��� ע^ 庂 ��� !�� J�� 2�� $�� dcu �}q  -} WWW ��� CCC ��� ˠi  /// ��� ��� ?�� ��� ~�� ��� Sp ��� jjj +h�  K�� Ž� ɦt ~~~ ͛W 5eu �| ��� ��� P�� *ky ��� ��� ��� 8�� $�� TRP ]^\ ܫj KFE 444 >>> ��� !& FMO Ԧe ��� ��� ��� ̜_ ���  ~zw ��� yur ӠX Nn ��� ]l ��� ̔G �� ��� *** ��� ��� onn 羇 ��� ���      IIJ ��� �u ��� �¿ ^[W ��� M�� ʗT  ��� ћS ½� $'+ 4�� ٧c pli ��� ��� <�� YZ[ [ce G�� ggg ��� FFF �z OOO ��� # ٬o `__ qqq  }�� ̖P  ޮo ��� NII \_` ��� !$( ;;< 6�� '�� ��� �� UUV  $$$ ((( ,,, 111 666 @@@ 2mz ��� p >:= ��� ܰu ΜZ SSS ��� XVU \[[ ccd ��� ��� ��� ���  ��� �� !`l ��� ��� PQR LLJ $�� ��� ��� ��� ��� ��� 878 ��� ~�� BBA EDC ��� HHH ��� 㹁 NNN PPN ڭq XYY \]^ ��� ��� ��� ���   ��� ;:: _m ��� ,ly _`a ppn ��� ��� ��� ��� LLL RRP ���  ��� ���   $ %%% ''' --- ... 000 222 333 555 ��� === ��� ??? DDD ��<N&_����ce$V>gH������]3n��=4p�#A!*RM��ͫ������6=��v�:C,����u�n��6x���� ����i�X�����6mK��v�h�-�+Q�"��7����s��v��į-�~�l�А��&}3v�����-�L�����w��?��ó��{�h���Dq��w�b��򡄔b�BZ[��aOj���II�ؕ\�G`E%�2����	r(�y�U�'f�k�5/d
ж1�t���P.0;31�z�����8^^��Yږ��������)o)��޽�ͪ���������)o)��޽�w�nw��������8^^�ʒX��@����������7�@���������ʧ����F������8^^�ʋ�����ۼ�JJ��|���ٶ1ٹT�������S����9��ww�����X�WWWW���OnClickBSalvaInteressiSviluppoClick  TToolbarButton97BEliminaInteressiSviluppoLeft� TopWidth`Height!CaptionElimina Area
Glyph.Data
:  6  BM6      6  (                                     ��� � � ��y -.� ��� DC; mn� ��� ��� ^^� __^ ��� IR� ��� CC� ,,' xx� ��� ��� ��� ]h� VV� ��� ��� CC� ��� ��� ��� pm] kk� wup ��� ��� ��� ��� ��� 885 ��� ��� \a� ��w ��� 88� ��� ``� CG� MO� ��� ��� ��� nn� HH� ��� XY� �l yy� S_� ��� ��� <=� uu� ��� >>� hgd df� 46� //� ��� [ZU ss� ��� ||� ��� ��z ai� ��� ��� LN� ��� ��� ��� 33� ss� ��� ��� PV� }}� ��� ��� ��� ��� |{n ||� ``� CC� NN� ee� ��� ��� ��� 79� Xd� ��� ��� \\� 13� JJ� ��� ��� ��� ��v GK� ��� oo� CI� ]]� ��� CC� PZ� ��� hh� ^]W FO� V]� ��� ��� ��� � 66� ��� ��� ��� ��� ��� ��� /3� ��� ]][ nkZ ~}q ��� ah� ��� bh� ��� ��� 36� eec Wa� ��{ ��� @I� GH� ,/� xxq QY� ��� ��� ��� ��� ��� ��� oo� ��� ��� NT� ��� qq� ��� 22� BF� NO� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� 02� 22� �~m ��� ��� ��� ��� ��� 986 ��� ��� 35� 77� ��� ��� ��� ��� DC< //� ��� NN� ��w Yd� ��� ��� ��� ��� ��� ��� ||� ���                                                                                                                                                                 ,%��,�@��''� ʔ��z\bpV||����E��o�s���(AyJ}�:jL�f{$gB����CN��K9w1����R!;2��d0���P7)�kŹ�;lh	.e�+"΂`i<����/Ī�v�I��^t���T?�_c�[6�n��5�4�D�x�r��GH=�Z>���U�3]��Q�S-a
�#umWX��8F���ж�M*&~qMY������O��WordWrap	OnClickBEliminaInteressiSviluppoClick  TToolbarButton97BTabAmbitiSviluppoLeft�TopWidth� Height!Caption!Tab. Ambiti di interesse/Sviluppo
Glyph.Data
z  v  BMv      6  (               @                     ��� � � ��= ǻ� [TH ��} �X �c  ʧw ��� vnb ��� �rG �8 ��� ��b ��^ �ɼ �w5 ��� iaS �}o ��M ��J �K ��� ��� Ğh �|/ �zS ��S �ð �@ ��� }ui �i) ��\ �p2 ��� phY ��t b[N ��� �S ��� �L �v< ��� �nC ��D ɤp �Ÿ �p@ �E ��� ��� �= ÷� ��� ��w rl_ ��Y ��I {td �X �p ��� ��� e^Q mdU �z7 ��N ��� �S �P �xl ��[ ȿ� Ƹ� ��� ��� ��� ��@ �³ �G �ǹ ��: �~s �qF ��� ��� sj\ �xP ��� mg[ oi^ �˾ �tJ ��� ˥t �= �Ƶ �K ��} ��� yqb ��u jcV ��� �q5 ɼ� ��� ��� ��� �z8 �ĳ ��[ ø� ̤q ��� ��� ��� tl^ vn_ �: �G �ķ �Z �T �n ��q ʿ� ��r Ƚ� ż� ��Q ƺ� ��� `YM �Ϳ �ȼ �ƹ ��� ��^ ��� c\O ngX qi[ �oD �; �zm �R �õ ��F ��� �}q ǿ� ��t ǹ� ��| ķ� ��� d^O kdT ��� ��� �qG �J �ʾ �E }vg �ɺ �Ǻ �Ʒ ��V ��F �Ķ �± Ƚ� ��` ��y ĺ� ��� ˦r ��� ��� �c" �˿ �}q �}n �p �µ �ĳ ��p ��u ��\ ɤr ̧w ɥt ��� ��� ��� ��� ��� ��� \UI ofY xpa �{7 �ʼ ��o ��q ��O �ð ��� ��r ��v ��� ɾ� ��v ƽ� ŝg şi ʦu ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� aZM d]P f^R f_Q haT piZ qj\ sl] sl_ �pF ��� ��� ��� �zT ��� �̾ �~o �ȼ �~p �Ȼ �q #\�(ϓFl��E��*������*_(t���v�OOO�o��o��o�� Δ�T���iN�p�y��������N��zz��ۍ4�T����4�۱NNT�����o�|�ffںiۢ�Np��韋��f���ۢI�p���O�j�a����f4��N�N�T��ډ݉�@�������ʳ��R���-P�������x�L�/uMnb]&�$Y����1�5_������KHg}2gU��}G9}G9`d�A,�J~Ѩ�.6s!e!��=�w�	��?�3W��S��>>%=�-mm�[r��Qx�Z8+:�����i�ͷ7ITCq��"{��
�
D^D�'���Ү�a�V��V�������BB�������))�ٵh@��:�ױ�����Ү��c�"X0�<<<<���k�)��)�ԃB�Ӡ;WordWrap	OnClickBTabAmbitiSviluppoClick   	TdxDBGrid	dxDBGrid3Left Top)WidthHeight� Bands  DefaultLayout	HeaderPanelRowCountKeyFieldIDSummaryGroups SummarySeparator, AlignalTopTabOrder
DataSourceDSAnagAreeInteresseFilter.Criteria
       	OptionsDBedgoCancelOnExitedgoCanDeleteedgoCanInsertedgoCanNavigationedgoConfirmDeleteedgoLoadAllRecordsedgoUseBookmarks OptionsViewedgoAutoWidthedgoBandHeaderWidthedgoIndicatoredgoUseBitmap AnchorsakLeftakTopakRightakBottom  TdxDBGridColumndxDBGrid3DescrizioneDisableEditor	WidthT	BandIndex RowIndex 	FieldNameArea  TdxDBGridBlobColumndxDBGrid3NoteWidth
	BandIndex RowIndex 	FieldNameNote    	TTabSheetTSCVConoscInfoCaptionApplicativi conosc.
ImageIndex �TFrmAnagConoscInfoFrmAnagConoscInfo1WidthHeight<AlignalClient �TPanelPanel1Width �TToolbarButton97BVoceNewOnClickFrmAnagConoscInfo1BVoceNewClick  �TToolbarButton97BVoceDelOnClickFrmAnagConoscInfo1BVoceDelClick   �TDBCtrlGridDBCtrlGrid1WidthHeight�PanelHeight� 
PanelWidth� �TLabelLabel1Left�   �TLabelLabel7Leftc  �TLabelLabel8Left�  �TDBEditDBEdit1Left� Width�   �TDBEditDBEdit5Width?  �TDBEditDBEdit6LeftQWidth@  �TDBEditDBEdit7Widthe  �TDBLookupComboBoxDBLKLivelliLeft[Width~  �TDBLookupComboBoxDBLKPiattaformeLeft�   �TPanel
PselezInfoWidth �TToolbarButton97BSelezSoftwareOnClick%FrmAnagConoscInfo1BSelezSoftwareClick  �TToolbarButton97
BSelezAreaOnClick!FrmAnagConoscInfo1BSelezAreaClick     	TTabSheetTSAltriInteressiCaptionAltri Interessi
ImageIndex 	TSplitter	Splitter1Left Top�WidthHeightCursorcrVSplitAlignalTop  	TSplitter	Splitter2Left TopWidthHeightCursorcrVSplitAlignalTop  	TSplitter	Splitter3Left Top� WidthHeightCursorcrVSplitAlignalTop  TPanelPanel6Left Top WidthHeight1AlignalTop
BevelOuterbvNoneTabOrder  TToolbarButton97BSalvaInteressiPersTagLeftTopWidthHHeight!CaptionSalva 
Glyph.Data
z  v  BMv      6  (               @                     ��� � � 5�� ЗL NMM ��� m ��� �ȑ BV� ��� on� ��� &9� &&& >�� gw� 3o{ ��� ۯr ��� S�� Ke �� `cc ��s �پ ���  999 j�� Or 9�� #bm ��� uqm Igh ��� ע^ 庂 ��� !�� J�� 2�� $�� dcu �}q  -} WWW ��� CCC ��� ˠi  /// ��� ��� ?�� ��� ~�� ��� Sp ��� jjj +h�  K�� Ž� ɦt ~~~ ͛W 5eu �| ��� ��� P�� *ky ��� ��� ��� 8�� $�� TRP ]^\ ܫj KFE 444 >>> ��� !& FMO Ԧe ��� ��� ��� ̜_ ���  ~zw ��� yur ӠX Nn ��� ]l ��� ̔G �� ��� *** ��� ��� onn 羇 ��� ���      IIJ ��� �u ��� �¿ ^[W ��� M�� ʗT  ��� ћS ½� $'+ 4�� ٧c pli ��� ��� <�� YZ[ [ce G�� ggg ��� FFF �z OOO ��� # ٬o `__ qqq  }�� ̖P  ޮo ��� NII \_` ��� !$( ;;< 6�� '�� ��� �� UUV  $$$ ((( ,,, 111 666 @@@ 2mz ��� p >:= ��� ܰu ΜZ SSS ��� XVU \[[ ccd ��� ��� ��� ���  ��� �� !`l ��� ��� PQR LLJ $�� ��� ��� ��� ��� ��� 878 ��� ~�� BBA EDC ��� HHH ��� 㹁 NNN PPN ڭq XYY \]^ ��� ��� ��� ���   ��� ;:: _m ��� ,ly _`a ppn ��� ��� ��� ��� LLL RRP ���  ��� ���   $ %%% ''' --- ... 000 222 333 555 ��� === ��� ??? DDD ��<N&_����ce$V>gH������]3n��=4p�#A!*RM��ͫ������6=��v�:C,����u�n��6x���� ����i�X�����6mK��v�h�-�+Q�"��7����s��v��į-�~�l�А��&}3v�����-�L�����w��?��ó��{�h���Dq��w�b��򡄔b�BZ[��aOj���II�ؕ\�G`E%�2����	r(�y�U�'f�k�5/d
ж1�t���P.0;31�z�����8^^��Yږ��������)o)��޽�ͪ���������)o)��޽�w�nw��������8^^�ʒX��@����������7�@���������ʧ����F������8^^�ʋ�����ۼ�JJ��|���ٶ1ٹT�������S����9��ww�����X�WWWW���OnClickBSalvaInteressiPersClick  TToolbarButton97BAnnullaAltriInteressiTagLeftUTopWidthGHeight!CaptionAnnulla modifica
Glyph.Data
:  6  BM6      6  (                                     ��� � � ��y -.� ��� DC; mn� ��� ��� ^^� __^ ��� IR� ��� CC� ,,' xx� ��� ��� ��� ]h� VV� ��� ��� CC� ��� ��� ��� pm] kk� wup ��� ��� ��� ��� ��� 885 ��� ��� \a� ��w ��� 88� ��� ``� CG� MO� ��� ��� ��� nn� HH� ��� XY� �l yy� S_� ��� ��� <=� uu� ��� >>� hgd df� 46� //� ��� [ZU ss� ��� ||� ��� ��z ai� ��� ��� LN� ��� ��� ��� 33� ss� ��� ��� PV� }}� ��� ��� ��� ��� |{n ||� ``� CC� NN� ee� ��� ��� ��� 79� Xd� ��� ��� \\� 13� JJ� ��� ��� ��� ��v GK� ��� oo� CI� ]]� ��� CC� PZ� ��� hh� ^]W FO� V]� ��� ��� ��� � 66� ��� ��� ��� ��� ��� ��� /3� ��� ]][ nkZ ~}q ��� ah� ��� bh� ��� ��� 36� eec Wa� ��{ ��� @I� GH� ,/� xxq QY� ��� ��� ��� ��� ��� ��� oo� ��� ��� NT� ��� qq� ��� 22� BF� NO� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� 02� 22� �~m ��� ��� ��� ��� ��� 986 ��� ��� 35� 77� ��� ��� ��� ��� DC< //� ��� NN� ��w Yd� ��� ��� ��� ��� ��� ��� ||� ���                                                                                                                                                                 ,%��,�@��''� ʔ��z\bpV||����E��o�s���(AyJ}�:jL�f{$gB����CN��K9w1����R!;2��d0���P7)�kŹ�;lh	.e�+"΂`i<����/Ī�v�I��^t���T?�_c�[6�n��5�4�D�x�r��GH=�Z>���U�3]��Q�S-a
�#umWX��8F���ж�M*&~qMY������O��OpaqueWordWrap	OnClickBAnnullaAltriInteressiClick   TPanelPanel8Left Top�WidthHeightpAlignalTop
BevelOuterbvNoneTabOrder TLabelLabel97LeftTopWidthEHeightCaptionNote Personali  TDBMemoDBMemo5LeftTopWidth�HeightR	DataFieldNotePersonali
DataSourceDSAnagInteressiPersTabOrder    TPanelPanel13Left TopWidthHeightpAlignalTop
BevelOuterbvNoneTabOrder TLabelLabel36LeftTopWidth2HeightCaptionAspirazioni  TDBMemoDBMemo3LeftTopWidth�HeightR	DataFieldAspirazioni
DataSourceDSAnagInteressiPersTabOrder    TPanelPanel14Left Top� WidthHeightpAlignalTop
BevelOuterbvNoneTabOrder TLabelLabel88LeftTopWidthHeightCaptionAbilit�  TDBMemoDBMemo4LeftTopWidth�HeightR	DataFieldAbilita
DataSourceDSAnagInteressiPersTabOrder    TPanelPanel15Left Top1WidthHeightpAlignalTop
BevelOuterbvNoneTabOrder TLabelLabel89LeftTopWidthDHeightCaptionHobby e Sport  TDBMemoDBMemo6LeftTopWidth�HeightR	DataField
HobbySport
DataSourceDSAnagInteressiPersTabOrder     	TTabSheetTSCVCampiPersCaptionForm Web - Pers.
ImageIndex TPanel
PanFerrariLeft Top WidthHeight� AlignalTop
BevelOuter	bvLoweredTabOrder  TLabelLabel46LeftTop2WidthGHeightCaptionSe s� quando ?FocusControlDBEdit39  TLabelLabel50Left� Top2WidthOHeightCaptionTipo di selezioneFocusControlDBEdit47  TLabelLabel55LeftTopaWidth� HeightCaptionParenti conosciuti in aziendaFocusControlDBEdit48  TDBCheckBoxDBCheckBox18LeftTopWidth)HeightCaption6Ha inviato precedenti domande presso la nostra societ�	DataFieldDomandePrec
DataSourcedsAnagAltriDatiTabOrder ValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox19LeftTop Width)HeightCaption7Ha gi� partecipato ad altre selezioni in questa azienda	DataFieldPartecSelezPrec
DataSourcedsAnagAltriDatiTabOrderValueCheckedTrueValueUncheckedFalse  TDBEditDBEdit39LeftTopBWidth� Height	DataFieldPartecSelezQuando
DataSourcedsAnagAltriDatiTabOrder  TDBEditDBEdit47Left� TopBWidth� Height	DataFieldTipoSelezione
DataSourcedsAnagAltriDatiTabOrder  TDBEditDBEdit48LeftTopqWidth0Height	DataFieldParentiInAzienda
DataSourcedsAnagAltriDatiTabOrder    	TTabSheetTSCampiPersCaptionCandidato - Pers
ImageIndex �TCampiPersFrameCampiPersFrame1WidthHeight<AlignalClient �TPanelPanTopWidth  �TPanelPan1Width �	TdxDBEdit	dxDBEdit1Width�  �	TdxDBMemo	dxDBMemo1Height   �TPanelPan2Width �	TdxDBEdit	dxDBEdit2Width�  �	TdxDBMemo	dxDBMemo2Height   �TPanelPan3Width �	TdxDBEdit	dxDBEdit3Width�  �	TdxDBMemo	dxDBMemo3Height   �TPanelPan4Width �	TdxDBEdit	dxDBEdit4Width�  �	TdxDBMemo	dxDBMemo4Height   �TPanelPan5Width �	TdxDBEdit	dxDBEdit5Width�  �	TdxDBMemo	dxDBMemo5Height   �TPanelPan6Width �	TdxDBEdit	dxDBEdit6Width�  �	TdxDBMemo	dxDBMemo6Height   �TPanelPan7Width �	TdxDBEdit	dxDBEdit7Width�  �	TdxDBMemo	dxDBMemo7Height   �TPanelPan8Width �	TdxDBEdit	dxDBEdit8Width�  �	TdxDBMemo	dxDBMemo8Height   �TPanelPan9Width �	TdxDBEdit	dxDBEdit9Width�  �	TdxDBMemo	dxDBMemo9Height   �TPanelPan10Width �	TdxDBEdit
dxDBEdit10Width�  �	TdxDBMemo
dxDBMemo10Height   �TPanelPan11Width� �	TdxDBMemo
dxDBMemo11Height   �TPanelPan12Width� �	TdxDBMemo
dxDBMemo12Height     	TTabSheetTSNoteCaptionNote
ImageIndex 	TSplitter	Splitter4Left TopWidthHeightCursorcrVSplitAlignalTop  TPanelPanel4Left Top WidthHeightAlignalTop
BevelOuterbvNoneTabOrder   TPanelPanel17Left TopWidthHeightCAlignalClient
BevelOuterbvNoneCaptionPanel17TabOrder TDBMemoDBNoteLeft TopWidthHeight.AlignalClient	DataFieldPresentazioneBreve
DataSourcedsAnagAltriDatiReadOnly	TabOrder   TPanelPanel18Left Top WidthHeightAlignalTop	AlignmenttaLeftJustifyBorderStylebsSingleCaption Presentazione PersonaleColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder   TPanelPanel19Left TopWidthHeight� AlignalTop
BevelOuterbvNoneCaptionPanel17TabOrder TDBMemoDBMemo7Left TopWidthHeight� AlignalClient	DataFieldNoteCurriculum
DataSourcedsAnagAltriDatiTabOrder   TPanelPanel20Left Top WidthHeightAlignalTop	AlignmenttaLeftJustifyBorderStylebsSingleCaption NoteColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder     TPanel
Wallpaper1Left Top WidthHeight+AlignalTopTabOrder TToolbarButton97ToolbarButton973Left�TopWidthPHeight CaptionEsciFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3     33wwwww33����33w?33333���33s�3333���337?3333��3333333��3333333��3333333��3333333��3333333��333�333���333w3333��3333333��33?3333���333333���33w��33     33wwwwws3	NumGlyphsOpaque
ParentFontOnClickToolbarButton973Click  TToolbarButton97ToolbarButton971Left�TopWidthNHeight CaptionCV ViewFont.CharsetDEFAULT_CHARSET
Font.ColorclBlueFont.Height�	Font.NameMS Sans Serif
Font.Style 
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 0       7wwwwwww������pw?�333?ww����w�w?���w��w     3wwwwww������3??�?��p � �?ww�w7p���w37�s7 ~�����w��7��7ww� ��www7w��0~� ��7s�sw�73p� �37s7ws733p� ��3373w�7333���333377333���33337s7333����333����333     333wwwww	NumGlyphsOpaque
ParentFontOnClickToolbarButton971Click  TToolbarButton97ToolbarButton974Left8TopWidthPHeightCaption	File Word
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 333333333333�333333<333333�w�3333<���33337w73333�3333ssw�33< ����33�w3773<����37w?33sw�������ss�377 �����w37?33sw������s�3s�3770������7?37?3?s3��� 33s�3s�w330��� 3337?37w3333�33333s�s333330 3333337w333333333333333333333333333333333333	NumGlyphsOnClickToolbarButton974Click  TToolbarButton97ToolbarButton977LeftHTopWidth)HeightCaptiongraficEnabledVisibleOnClickToolbarButton977Click  TPanelPanel7LeftTopWidth4Height)AlignalLeft
BevelOuterbvNoneEnabledTabOrder  TDBEditDBEdit1LeftTop
Width� HeightColorclYellow	DataFieldCognome
DataSourceData.DsAnagraficaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontReadOnly	TabOrder   TDBEditDBEdit2Left� Top
Width� HeightColorclYellow	DataFieldNome
DataSourceData.DsAnagraficaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontReadOnly	TabOrder    TDataSourcedsAnagAltriDatiDataSetTAnagAltriDatiLeft� Top	  TADOLinkedQueryQTipiRetribLK
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Strings*select ID,TipoRetrib from TipiRetribuzione 	UseFilterLeftTop  TADOLinkedQueryTAnagAltriDati
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect * from AnagAltreInfo  MasterDataSetData.TAnagraficaLinkedMasterFieldIDLinkedDetailFieldIDAnagrafica	UseFilterLeft� Top	 TStringFieldTAnagAltriDatiTipoRetib	FieldKindfkLookup	FieldName	TipoRetibLookupDataSetQTipiRetribLKLookupKeyFieldsIDLookupResultField
TipoRetrib	KeyFieldsIDTipoRetribLookup	  TStringFieldTAnagAltriDatiDispMovimento	FieldKindfkLookup	FieldNameDispMovimentoLookupDataSet
TDispMovLKLookupKeyFieldsIDLookupResultFieldDescrizione	KeyFields	IDDispMovSize(Lookup	  TIntegerFieldTAnagAltriDatiIDAnagrafica	FieldNameIDAnagrafica  TBooleanFieldTAnagAltriDatiFiguraChiave	FieldNameFiguraChiave  TBooleanField$TAnagAltriDatiAssunzioneObbligatoria	FieldNameAssunzioneObbligatoria  TBooleanField!TAnagAltriDatiIdoneoAffiancamento	FieldNameIdoneoAffiancamento  TStringField TAnagAltriDatiCateogorieProtette	FieldNameCateogorieProtette	FixedChar	Size  TStringFieldTAnagAltriDatiInvalidita	FieldName
Invalidita	FixedChar	Size
  TIntegerField#TAnagAltriDatiPercentualeInvalidita	FieldNamePercentualeInvalidita  TBooleanFieldTAnagAltriDatiPatente	FieldNamePatente  TStringFieldTAnagAltriDatiTipoPatente	FieldNameTipoPatente	FixedChar	Size  TBooleanFieldTAnagAltriDatiDispAuto	FieldNameDispAuto  TStringField#TAnagAltriDatiServizioMilitareStato	FieldNameServizioMilitareStato	FixedChar	Size  TStringField"TAnagAltriDatiServizioMilitareDove	FieldNameServizioMilitareDove	FixedChar	Size  TStringField$TAnagAltriDatiServizioMilitareQuando	FieldNameServizioMilitareQuando	FixedChar	  TStringField#TAnagAltriDatiServizioMilitareGrado	FieldNameServizioMilitareGrado	FixedChar	Size  TStringFieldTAnagAltriDatiTipoContratto	FieldNameTipoContratto	FixedChar	Size(  TStringField#TAnagAltriDatiQualificaContrattuale	FieldNameQualificaContrattuale	FixedChar	Size(  TDateTimeFieldTAnagAltriDatiDataAssunzione	FieldNameDataAssunzione  TDateTimeFieldTAnagAltriDatiDataScadContratto	FieldNameDataScadContratto  TDateTimeFieldTAnagAltriDatiDataUscita	FieldName
DataUscita  TDateTimeFieldTAnagAltriDatiDataFineProva	FieldNameDataFineProva  TDateTimeFieldTAnagAltriDatiDataProssScatto	FieldNameDataProssScatto  TDateTimeFieldTAnagAltriDatiDataPensione	FieldNameDataPensione  TIntegerFieldTAnagAltriDatiIDStabilimento	FieldNameIDStabilimento  TStringFieldTAnagAltriDatiPosizioneIdeale	FieldNamePosizioneIdeale	FixedChar	Size  TStringFieldTAnagAltriDatiOrarioIdeale	FieldNameOrarioIdeale	FixedChar	  TIntegerFieldTAnagAltriDatiIDDispMov	FieldName	IDDispMov  TStringFieldTAnagAltriDatiDisponibContratti	FieldNameDisponibContratti	FixedChar	  TStringFieldTAnagAltriDatiCVProvenienza	FieldNameCVProvenienza	FixedChar	Size  TStringFieldTAnagAltriDatiCVFile	FieldNameCVFile	FixedChar	Size(  TStringField!TAnagAltriDatiAbilitazProfessioni	FieldNameAbilitazProfessioni	FixedChar	Size  
TMemoFieldTAnagAltriDatiNote	FieldNameNoteBlobTypeftMemo  TBooleanFieldTAnagAltriDatiMobilita	FieldNameMobilita  TBooleanFieldTAnagAltriDatiCassaIntegrazione	FieldNameCassaIntegrazione  TBooleanFieldTAnagAltriDatiDispPartTime	FieldNameDispPartTime  TBooleanFieldTAnagAltriDatiDispInterinale	FieldNameDispInterinale  TStringFieldTAnagAltriDatiProvInteresse	FieldNameProvInteresse	FixedChar	SizeP  TStringFieldTAnagAltriDatiInquadramento	FieldNameInquadramentoSize2  TStringFieldTAnagAltriDatiRetribuzione	FieldNameRetribuzione	FixedChar	Sized  TStringFieldTAnagAltriDatiBenefits	FieldNameBenefits	FixedChar	Sized  TFloatFieldTAnagAltriDatiPercPartTime	FieldNamePercPartTime  TDateTimeFieldTAnagAltriDatiDataAssunzRicon	FieldNameDataAssunzRicon  TDateTimeField!TAnagAltriDatiDataIngressoAzienda	FieldNameDataIngressoAzienda  TStringFieldTAnagAltriDatiCodSindacato	FieldNameCodSindacato	FixedChar	Size
  TStringFieldTAnagAltriDatiCodTipoAssunzione	FieldNameCodTipoAssunzione	FixedChar	Size  TSmallintFieldTAnagAltriDatiPeriodoProvaGG	FieldNamePeriodoProvaGG  TIntegerFieldTAnagAltriDatiIDMotAssunz	FieldNameIDMotAssunz  TStringFieldTAnagAltriDatiNazionalita	FieldNameNazionalita	FixedChar	Size2  TIntegerFieldTAnagAltriDatiIDTipoRetrib	FieldNameIDTipoRetrib  TIntegerFieldTAnagAltriDatiIDSindacato	FieldNameIDSindacato  TStringFieldTAnagAltriDatiNoteInfoPos	FieldNameNoteInfoPos	FixedChar	Size  TBooleanFieldTAnagAltriDatiTemporaryMG	FieldNameTemporaryMG  TBooleanFieldTAnagAltriDatiNoCV	FieldNameNoCV  TBooleanField!TAnagAltriDatiCassaIntegrStraord3	FieldNameCassaIntegrStraord3  TBooleanField"TAnagAltriDatiCassaIntegrStraord24	FieldNameCassaIntegrStraord24  TBooleanFieldTAnagAltriDatiDisoccupato24	FieldNameDisoccupato24  TBooleanFieldTAnagAltriDatiDisoccupato12	FieldNameDisoccupato12  TBooleanFieldTAnagAltriDatiDispTraferteGiorn	FieldNameDispTraferteGiorn  TBooleanFieldTAnagAltriDatiDispPernottamento	FieldNameDispPernottamento  TBooleanFieldTAnagAltriDatiDisptrasferimento	FieldNameDisptrasferimento  TIntegerFieldTAnagAltriDatiIDContrattoNaz	FieldNameIDContrattoNaz  TIntegerFieldTAnagAltriDatiIDCatProfess	FieldNameIDCatProfess  TDateTimeFieldTAnagAltriDatiDataLicenziamento	FieldNameDataLicenziamento  TDateTimeFieldTAnagAltriDatiDataProroga	FieldNameDataProroga  TDateTimeFieldTAnagAltriDatiDataUltimaProm	FieldNameDataUltimaProm  TIntegerFieldTAnagAltriDatiIDEspProfMaturata	FieldNameIDEspProfMaturata  TBooleanFieldTAnagAltriDatiDomandePrec	FieldNameDomandePrec  TBooleanFieldTAnagAltriDatiPartecSelezPrec	FieldNamePartecSelezPrec  TStringFieldTAnagAltriDatiPartecSelezQuando	FieldNamePartecSelezQuando	FixedChar	Size(  TStringFieldTAnagAltriDatiTipoSelezione	FieldNameTipoSelezione	FixedChar	Size  TStringFieldTAnagAltriDatiParentiInAzienda	FieldNameParentiInAzienda	FixedChar	Size2  TStringFieldTAnagAltriDatiTipoPersona	FieldNameTipoPersona	FixedChar	Size  TStringFieldTAnagAltriDatiDirigenteTipo	FieldNameDirigenteTipo	FixedChar	Size  TStringFieldTAnagAltriDatiDirigenteStato	FieldNameDirigenteStato	FixedChar	Size  TIntegerField TAnagAltriDatiDirigenteIDCliente	FieldNameDirigenteIDCliente  TIntegerFieldTAnagAltriDatiIDTDgTipologia	FieldNameIDTDgTipologia  TStringFieldTAnagAltriDatiEspProfMaturata	FieldKindfkLookup	FieldNameEspProfMaturataLookupDataSetQEspProfMatLKLookupKeyFieldsIDLookupResultFieldEspProfMaturata	KeyFieldsIDEspProfMaturataSize(Lookup	  TStringFieldTAnagAltriDatiNumPassaporto	FieldNameNumPassaportoSize2  TStringFieldTAnagAltriDatiRetribVariabile	FieldNameRetribVariabileSize2  TStringFieldTAnagAltriDatiRetribTotale	FieldNameRetribTotaleSize2  TIntegerField TAnagAltriDatiIDContrattoSettore	FieldNameIDContrattoSettore  TStringFieldTAnagAltriDatiContrattoSettore	FieldKindfkLookup	FieldNameContrattoSettoreLookupDataSetQContrattiSettoriLKLookupKeyFieldsIDLookupResultFieldSettore	KeyFieldsIDContrattoSettoreSize2Lookup	  
TMemoFieldTAnagAltriDatiNoteCurriculum	FieldNameNoteCurriculumBlobTypeftMemo  
TMemoFieldTAnagAltriDatiNoteAltriDati	FieldNameNoteAltriDatiBlobTypeftMemo  TBooleanFieldTAnagAltriDatiDispAvvicinSede	FieldNameDispAvvicinSede  TStringFieldTAnagAltriDatiSpecCVProvenienza	FieldNameSpecCVProvenienzaSize�  
TMemoField TAnagAltriDatiPresentazioneBreve	FieldNamePresentazioneBreveReadOnly	BlobTypeftMemo   TADOLinkedQueryQEspProfMatLK
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect * from EspProfMaturata 	UseFilterLeft� Top  TADOLinkedTable
TDispMovLK
ConnectionData.DB
CursorTypectStatic	TableNameDispMovimentoLeft� Top	  TADOLinkedTableTAnagMansioni
ConnectionData.DB	TableNameAnagMansioniLeft�Top�   TADOLinkedTableTMansioniLK
ConnectionData.DB	TableNameMansioniLeft�Top�   	TADOQueryQContrattiSettoriLK
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect *from ContrattiSettoriorder by settore Left	TopI TAutoIncFieldQContrattiSettoriLKID	FieldNameIDReadOnly	  TStringFieldQContrattiSettoriLKSettore	FieldNameSettoreSize2   	TADOQueryQAnagAreeInteresse
ConnectionData.DB
DataSourceData.DsAnagrafica
ParametersNameID
AttributespaSigned DataType	ftInteger	Precision
SizeValue   SQL.Stringsselect ai.ID,a.Descrizione Area,ai.Note from AnagAreeInteresse aijoin aree a on a.id=ai.idareawhere idanagrafica=:ID Left� Top TAutoIncFieldQAnagAreeInteresseID	FieldNameIDReadOnly	  TStringFieldQAnagAreeInteresseArea	FieldNameAreaSized  
TMemoFieldQAnagAreeInteresseNote	FieldNameNoteBlobTypeftMemo   TDataSourceDSAnagAreeInteresseDataSetQAnagAreeInteresseOnStateChangeDSAnagAreeInteresseStateChangeLeft� Top  TDataSourceDSAnagInteressiSviluppoDataSetQAnagInteressiSviluppoOnStateChange"DSAnagInteressiSviluppoStateChangeLeft�Top�   	TADOQueryQAnagInteressiSviluppo
ConnectionData.DB
DataSourceData.DsAnagrafica
ParametersNameID
AttributespaSigned
paNullable DataType	ftInteger	Precision
SizeValue   SQL.Strings	  select ais.ID,ais.IDanagrafica,ais.Ordine,	ais.Note,3isnull(asv.Descrizione, AmbitoSviluppo) Descrizione   from AnagInteressiSviluppo ais;left join AmbitiSviluppo asv on ais.idambitoSviluppo=asv.idwhere  ais.idanagrafica=:ID Left�Top�  TAutoIncFieldQAnagInteressiSviluppoID	FieldNameIDReadOnly	  TIntegerField"QAnagInteressiSviluppoIDanagrafica	FieldNameIDanagrafica  TIntegerFieldQAnagInteressiSviluppoOrdine	FieldNameOrdine  
TMemoFieldQAnagInteressiSviluppoNote	FieldNameNoteBlobTypeftMemo  TStringField!QAnagInteressiSviluppoDescrizione	FieldNameDescrizioneSize    	TADOQueryQAnagCatProfess
ConnectionData.DB
DataSourceData.DsAnagrafica
ParametersNameID
AttributespaSigned
paNullable DataType	ftInteger	Precision
SizeValue   SQL.Stringsselect * from AnagCatProfesswhere idanagrafica=:ID Left�Top�  TAutoIncFieldQAnagCatProfessID	FieldNameIDReadOnly	  TIntegerFieldQAnagCatProfessIDAnagrafica	FieldNameIDAnagrafica  TIntegerFieldQAnagCatProfessIDCatProfess	FieldNameIDCatProfess  TStringFieldQAnagCatProfessIstitutoConseg	FieldNameIstitutoConsegSize�   TStringFieldQAnagCatProfessLuogoConseg	FieldNameLuogoConsegSize�   TStringFieldQAnagCatProfessProvConseg	FieldName
ProvConsegSize�   TStringField QAnagCatProfessCittaEsteraConseg	FieldNameCittaEsteraConsegSize�   TDateTimeFieldQAnagCatProfessDataConseg	FieldName
DataConseg  TStringFieldQAnagCatProfessNumAbil	FieldNameNumAbilSize2  TBooleanFieldQAnagCatProfessEquipollente	FieldNameEquipollente  TDateTimeFieldQAnagCatProfessDataEquip	FieldName	DataEquip  TStringFieldQAnagCatProfessNumEquip	FieldNameNumEquipSize2  TStringFieldQAnagCatProfessCatProfessLK	FieldKindfkLookup	FieldNameCatProfessLKLookupDataSetQCatProfessLKLookupKeyFieldsIDLookupResultFieldDescrizione	KeyFieldsIDCatProfessSize�Lookup	  TStringFieldQAnagCatProfessQTipoLK	FieldKindfkLookup	FieldNameTipoLKLookupDataSetQCatProfessLKLookupKeyFieldsIDLookupResultFieldTipo	KeyFieldsIDCatProfessSize�Lookup	   	TADOQueryQCatProfessLK
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect  TipoCatProfess.Descrizione Tipo,CatProfess.* from CatProfessleft join TipoCatProfess 0on CatProfess.IDTipoCatProfess=TipoCatProfess.ID Left�Top�  TAutoIncFieldQCatProfessLKID	FieldNameIDReadOnly	  TStringFieldQCatProfessLKDescrizione	FieldNameDescrizioneSize(  TStringFieldQCatProfessLKTipo	FieldNameTipoSize�   TDataSourceDSanagcatprofessDataSetQAnagCatProfessOnStateChangeDSanagcatprofessStateChangeLeft�Top�   	TADOQueryQAnagOnteressiPers
ConnectionData.DB
DataSourceData.DsAnagrafica
ParametersNameID
AttributespaSigned
paNullable DataType	ftInteger	Precision
SizeValue   SQL.Stringsselect * from AnagInteressiPers where idanagrafica=:ID Left�Top TAutoIncFieldQAnagOnteressiPersID	FieldNameIDReadOnly	  TIntegerFieldQAnagOnteressiPersIDAnagrafica	FieldNameIDAnagrafica  
TMemoFieldQAnagOnteressiPersHobbySport	FieldName
HobbySportBlobTypeftMemo  
TMemoFieldQAnagOnteressiPersAbilita	FieldNameAbilitaBlobTypeftMemo  
TMemoFieldQAnagOnteressiPersAspirazioni	FieldNameAspirazioniBlobTypeftMemo  
TMemoFieldQAnagOnteressiPersNotePersonali	FieldNameNotePersonaliBlobTypeftMemo  TStringFieldQAnagOnteressiPersParolaChiave1	FieldNameParolaChiave1Size�   TStringFieldQAnagOnteressiPersParolaChiave2	FieldNameParolaChiave2Size�   TStringFieldQAnagOnteressiPersParolaChiave3	FieldNameParolaChiave3Size�   TStringFieldQAnagOnteressiPersParolaChiave4	FieldNameParolaChiave4Size�   TStringFieldQAnagOnteressiPersParolaChiave5	FieldNameParolaChiave5Size�    TDataSourceDSAnagInteressiPersDataSetQAnagOnteressiPersOnStateChangeDSAnagInteressiPersStateChangeLeft�Top4  	TADOQueryQtemp
ConnectionData.DB
Parameters Left�Topz   