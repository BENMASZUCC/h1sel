unit ContattoCand;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, ComCtrls, DBCtrls, Mask, Buttons, ExtCtrls, DB,
     Grids, DBGrids, DtEdit97, DtEdDB97, DBTables, ADODB, U_ADOLinkCl;

type
     TContattoCandForm = class(TForm)
          Panel1: TPanel;
          BitBtn1: TBitBtn;
          BitBtn2: TBitBtn;
          Label2: TLabel;
          Label3: TLabel;
          Label9: TLabel;
          DBEdit5: TDBEdit;
          Panel3: TPanel;
          Label1: TLabel;
          DEData: TDateEdit97;
          MEOra: TMaskEdit;
          Label7: TLabel;
          Label8: TLabel;
          RadioGroup2: TRadioGroup;
          PanAltroEsito: TPanel;
          Label10: TLabel;
          EDesc: TEdit;
          Label6: TLabel;
          ECogn: TEdit;
          ENome: TEdit;
          ERec: TEdit;
          ECell: TEdit;
          PanMotivoNonAcc: TPanel;
          Label4: TLabel;
          EMotNonAcc: TEdit;
          SpeedButton1: TSpeedButton;
          QAnag: TADOLinkedQuery;
          QTipiContatti: TADOQuery;
          DsQTipiContatti: TDataSource;
          QTipiContattiID: TAutoIncField;
          QTipiContattiTipoContatto: TStringField;
          QTipiContattiIDEvento: TIntegerField;
          DBGrid1: TDBGrid;
          eMail: TEdit;
          Label5: TLabel;
    eNote: TMemo;
          procedure RadioGroup2Click(Sender: TObject);
          procedure BitBtn1Click(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure FormCreate(Sender: TObject);
          procedure SpeedButton1Click(Sender: TObject);
     private
          { Private declarations }
     public
          xIDAnag, xIDMotivoNonAcc: integer;
     end;

var
     ContattoCandForm: TContattoCandForm;

implementation

uses MDRicerche, ModuloDati, uUtilsVarie, Main;

{$R *.DFM}


procedure TContattoCandForm.RadioGroup2Click(Sender: TObject);
begin
     {     case Radiogroup2.ItemIndex of
          0: begin PanDataOra.visible:=True; DEData2.Setfocus end;
          1: begin PanDataOra.visible:=False end;
          2: begin PanDataOra.visible:=False end;
          end;
     }
     if Radiogroup2.ItemIndex = 3 then PanMotivoNonAcc.Visible := True
     else PanMotivoNonAcc.Visible := False;
     if Radiogroup2.ItemIndex = 4 then PanAltroEsito.Visible := True
     else PanAltroEsito.Visible := False;
end;

procedure TContattoCandForm.BitBtn1Click(Sender: TObject);
begin
     if radiogroup2.visible then begin
          if RadioGroup2.Itemindex < 0 then begin
               Modalresult := mrNone;
               MessageDlg('� necessario specificare almeno un esito del contatto', mtError, [mbOK], 0);
               Abort;
          end;
     end;
end;

procedure TContattoCandForm.FormShow(Sender: TObject);
begin
     Caption := '[E/4] - ' + Caption;
     QAnag.SQL.text := 'select Cognome,Nome,RecapitiTelefonici,Cellulare,email from ' +
          'Anagrafica where ID=' + IntToStr(xIDAnag);
     QAnag.Open;
     ECogn.Text := QAnag.FieldbyName('Cognome').asString;
     ENome.Text := QAnag.FieldbyName('Nome').asString;
     ERec.Text := QAnag.FieldbyName('RecapitiTelefonici').asString;
     ECell.Text := QAnag.FieldbyName('Cellulare').asString;
     eMail.Text := QAnag.FieldbyName('email').asString;
    // ContattoCandForm.BringToFront;

end;

procedure TContattoCandForm.FormCreate(Sender: TObject);
begin
     xIDMotivoNonAcc := 0;
     QTipiContatti.close;
     QTipiContatti.open;
   //  DoubleBuffered := True;

end;

procedure TContattoCandForm.SpeedButton1Click(Sender: TObject);
begin
     xIDMotivoNonAcc := StrToIntDef(OpenSelFromTab('MotiviNonAccettaz', 'ID', 'Motivo', 'Motivo', ''), 0);
     EMotNonAcc.Text := MainForm.xgenericString;
end;

end.

