�
 TSCHEDATESTATEFORM 0�  TPF0TSchedaTestateFormSchedaTestateFormLeft!Top� Width�Height'CaptionScheda Testate
ParentFont	PositionpoScreenCenterPixelsPerInch`
TextHeight TPanelPanel2Left Top'Width�Height� AlignalClient
BevelInner	bvLoweredBorderWidthCaptionPanel2TabOrder  
TScrollBox	ScrollBoxLeftTopWidthtHeight� HorzScrollBar.MarginHorzScrollBar.RangerVertScrollBar.MarginVertScrollBar.Range� AlignalClient
AutoScrollBorderStylebsNoneTabOrder  TLabelLabel1LeftTop	WidthWHeight	AlignmenttaRightJustifyAutoSizeCaptionDenominazioneFocusControlEditDenominazione  TLabelLabel2LeftTopWidthWHeight	AlignmenttaRightJustifyAutoSizeCaption	Tipologia  TLabelLabel3LeftTop5WidthWHeight	AlignmenttaRightJustifyAutoSizeCaptionAgenziaFocusControlEditAgenzia  TLabelLabel4LeftTopKWidthWHeight	AlignmenttaRightJustifyAutoSizeCaptionResponsabileFocusControlEditResponsabile  TLabelLabel5LeftTopaWidthWHeight	AlignmenttaRightJustifyAutoSizeCaptionTelefonoFocusControlEditTelefono  TLabelLabel6LeftTopwWidthWHeight	AlignmenttaRightJustifyAutoSizeCaptionFaxFocusControlEditFax  TLabelLabel7LeftTop� WidthWHeight	AlignmenttaRightJustifyAutoSizeCaptionEmailFocusControl	EditEmail  TLabelLabel8LeftTop� WidthWHeight	AlignmenttaRightJustifyAutoSizeCaptionTipoPagamento  TLabelLabel9LeftTop� WidthWHeight	AlignmenttaRightJustifyAutoSizeCaptionCostoFocusControl	EditCosto  TDBEditEditDenominazioneLeftcTopWidthHeight	DataFieldDenominazione
DataSourceDataSel_EBC.DsTestateFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder   TDBEditEditAgenziaLeftcTop2WidthZHeight	DataFieldAgenzia
DataSourceDataSel_EBC.DsTestateTabOrder  TDBEditEditResponsabileLeftcTopHWidth� Height	DataFieldResponsabile
DataSourceDataSel_EBC.DsTestateTabOrder  TDBEditEditTelefonoLeftcTop^WidthsHeight	DataFieldTelefono
DataSourceDataSel_EBC.DsTestateTabOrder  TDBEditEditFaxLeftcToptWidthsHeight	DataFieldFax
DataSourceDataSel_EBC.DsTestateTabOrder  TDBEdit	EditEmailLeftcTop� WidthsHeight	DataFieldEmail
DataSourceDataSel_EBC.DsTestateTabOrder  TDBEdit	EditCostoLeftcTop� WidthAHeight	DataFieldCosto
DataSourceDataSel_EBC.DsTestateTabOrder  TDBComboBoxDBComboBox1LeftcTopWidth\Height	DataField	Tipologia
DataSourceDataSel_EBC.DsTestate
ItemHeightItems.Strings	nazionale	regionalelocale TabOrder  TDBComboBoxDBComboBox2LeftdTop� WidthSHeight	DataFieldTipoPagamento
DataSourceDataSel_EBC.DsTestate
ItemHeightItems.Stringsmoduloparola TabOrder    TPanel
Wallpaper1Left Top Width�Height'AlignalTopTabOrder TToolbarButton97TbBTestataNewLeftTopWidth2Height%CaptionNuovoOpaqueOnClickTbBTestataNewClick  TToolbarButton97TbBTestataDelLeft3TopWidth2Height%CaptionEliminaOpaqueOnClickTbBTestataDelClick  TToolbarButton97TbBTestataCanLeft:TopWidthEHeight%CaptionEsciOpaqueOnClickTbBTestataCanClick    