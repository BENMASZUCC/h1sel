unit SelDiploma;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, Grids, DBGrids, Db, DBTables, ADODB, U_ADOLinkCl, dxTL,
     dxDBCtrl, dxDBGrid, dxCntner, dxDBTLCl, dxGrClms, TB97, ExtCtrls;

type
     TSelDiplomaForm = class(TForm)
          DsDiplomi: TDataSource;
          Diplomi: TADOLinkedQuery;
          DiplomiID: TAutoIncField;
          DiplomiIDArea: TIntegerField;
          DiplomiTipo: TStringField;
          DiplomiDescrizione: TStringField;
          DiplomiPunteggioMax: TSmallintField;
          DiplomiIDAzienda: TStringField;
          DiplomiTipo_ENG: TStringField;
          DiplomiDescrizione_ENG: TStringField;
          DiplomiIDGruppo: TIntegerField;
          QGruppiDiplomiLK: TADOQuery;
          QareeDiplomiLK: TADOQuery;
          QareeDiplomiLKID: TAutoIncField;
          QareeDiplomiLKArea: TStringField;
          QareeDiplomiLKIdAzienda: TIntegerField;
          QGruppiDiplomiLKID: TAutoIncField;
          QGruppiDiplomiLKGruppo: TStringField;
          DiplomiGruppo: TStringField;
          DiplomiArea: TStringField;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1ID: TdxDBGridMaskColumn;
          dxDBGrid1IDArea: TdxDBGridMaskColumn;
          dxDBGrid1Tipo: TdxDBGridMaskColumn;
          dxDBGrid1Descrizione: TdxDBGridMaskColumn;
          dxDBGrid1PunteggioMax: TdxDBGridMaskColumn;
          dxDBGrid1IDAzienda: TdxDBGridMaskColumn;
          dxDBGrid1Tipo_ENG: TdxDBGridMaskColumn;
          dxDBGrid1Descrizione_ENG: TdxDBGridMaskColumn;
          dxDBGrid1IDGruppo: TdxDBGridMaskColumn;
          dxDBGrid1Gruppo: TdxDBGridLookupColumn;
          dxDBGrid1Area: TdxDBGridLookupColumn;
          Panel1: TPanel;
          ToolbarButton971: TToolbarButton97;
          BAnnulla: TToolbarButton97;
          Panel3: TPanel;
          BITA: TToolbarButton97;
          BENG: TToolbarButton97;
          Panel2: TPanel;
          Label2: TLabel;
          ECerca: TEdit;
          procedure FormShow(Sender: TObject);
          procedure BITAClick(Sender: TObject);
          procedure BENGClick(Sender: TObject);
          procedure BAnnullaClick(Sender: TObject);
          procedure ToolbarButton971Click(Sender: TObject);
          procedure ECercaChange(Sender: TObject);
     private
          { Private declarations }
          xStringa: string;
     public
          { Public declarations }
     end;

var
     SelDiplomaForm: TSelDiplomaForm;

implementation

uses ModuloDati, Main;

{$R *.DFM}



//[ALBERTO 20020902]

procedure TSelDiplomaForm.FormShow(Sender: TObject);
begin
     //Grafica
     SelDiplomaForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel3.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel3.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel3.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     Caption := '[S/38] - ' + Caption;
     xStringa := '';
     if (Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 7)) = 'FERRARI') or
          (Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 3)) = 'EBC') then begin
          BITA.visible := True;
          BENG.visible := True;
     end;
     Diplomi.close;
     Diplomi.Open;
end;

procedure TSelDiplomaForm.BITAClick(Sender: TObject);
begin
     //QTipiDiplomi.Close;
     //QTipiDiplomi.SQL.text:='select Tipo from Diplomi where Tipo is not null group by Tipo';
     //QTipiDiplomi.Open;
     Diplomi.Close;
     Diplomi.LinkedDetailField := 'Tipo';
     Diplomi.SQL.text := 'select * from Diplomi order by Descrizione';
     Diplomi.Open;
     dxDBGrid1Descrizione.FieldName := 'Descrizione';
     dxDBGrid1Descrizione.Caption := 'Diploma';
end;

procedure TSelDiplomaForm.BENGClick(Sender: TObject);
begin
     //QTipiDiplomi.Close;
     //QTipiDiplomi.SQL.text:='select Tipo_ENG Tipo from Diplomi where Tipo_ENG is not null group by Tipo_ENG';
     //QTipiDiplomi.Open;
     Diplomi.Close;
     Diplomi.LinkedDetailField := 'Tipo_ENG';
     Diplomi.SQL.text := 'select * from Diplomi order by Descrizione_ENG';
     Diplomi.Open;
     dxDBGrid1Descrizione.FieldName := 'Descrizione_ENG';
     dxDBGrid1Descrizione.Caption := 'Diploma (ENG)';
end;

procedure TSelDiplomaForm.BAnnullaClick(Sender: TObject);
begin
     SelDiplomaForm.ModalResult := mrCancel;
end;

procedure TSelDiplomaForm.ToolbarButton971Click(Sender: TObject);
begin
     SelDiplomaForm.ModalResult := mrOK;
end;

procedure TSelDiplomaForm.ECercaChange(Sender: TObject);
begin
     Diplomi.Close;
     Diplomi.SQL.Text := 'select * from Diplomi' +
          ' where Descrizione like :xCerca:' +
          ' order by Descrizione';
     Diplomi.ParamByName['xCerca:'] := '%' + ECerca.text + '%';
     Diplomi.Open;
end;

end.

