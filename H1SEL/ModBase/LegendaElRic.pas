unit LegendaElRic;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, ExtCtrls, Buttons, RXSpin, Db, DBTables, ADODB, U_ADOLinkCl;

type
     TLegendaElRicForm = class(TForm)
          BitBtn1: TBitBtn;
          Q_OLD: TQuery;
          Panel5: TPanel;
          Label13: TLabel;
          Label14: TLabel;
          Label15: TLabel;
          RxSpinEdit1: TRxSpinEdit;
          Panel6: TPanel;
          Label16: TLabel;
          Label17: TLabel;
          RxSpinEdit2: TRxSpinEdit;
          Panel7: TPanel;
          Label19: TLabel;
          Label20: TLabel;
          RxSpinEdit3: TRxSpinEdit;
          Label1: TLabel;
          Label2: TLabel;
          Panel1: TPanel;
          Label3: TLabel;
          Label4: TLabel;
          Label5: TLabel;
          Label6: TLabel;
          Label7: TLabel;
          Shape1: TShape;
          Shape2: TShape;
          Shape3: TShape;
          Shape4: TShape;
          Label11: TLabel;
          Panel2: TPanel;
          Label8: TLabel;
          Label18: TLabel;
          Q: TADOLinkedQuery;
    SpeedButton1: TSpeedButton;
          procedure FormShow(Sender: TObject);
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SpeedButton1Click(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     LegendaElRicForm: TLegendaElRicForm;

implementation

uses ModuloDati,uUtilsVarie;

{$R *.DFM}

procedure TLegendaElRicForm.FormShow(Sender: TObject);
begin
     Caption := '[M/28] - ' + caption;
     Q.SQl.text := 'select ggDiffApRic,ggDiffUcRic,ggDiffColRic from global ';
     Q.Open;
     RxSpinEdit1.value := Q.FieldByname('ggDiffApRic').asInteger;
     RxSpinEdit2.value := Q.FieldByname('ggDiffUcRic').asInteger;
     RxSpinEdit3.value := Q.FieldByname('ggDiffColRic').asInteger;
     Q.Close;
end;

//[/TONI20020905\]

procedure TLegendaElRicForm.FormClose(Sender: TObject;
     var Action: TCloseAction);
begin
     with Data do begin
          DB.BeginTrans;
          try
               Q1.Close;
               {                Q1.SQL.text:='update global set ggDiffApRic=:xggDiffApRic,ggDiffUcRic=:xggDiffUcRic,ggDiffColRic=:xggDiffColRic';
                               Q1.ParamByName('xggDiffApRic').asInteger:=RxSpinEdit1.AsInteger;
                               Q1.ParamByName('xggDiffUcRic').asInteger:=RxSpinEdit2.AsInteger;
                               Q1.ParamByName('xggDiffColRic').asInteger:=RxSpinEdit3.AsInteger;}
               Q1.SQL.text := 'update global set ggDiffApRic=:xggDiffApRic:,ggDiffUcRic=:xggDiffUcRic:,ggDiffColRic=:xggDiffColRic:';
               Q1.ParamByName['xggDiffApRic'] := RxSpinEdit1.AsInteger;
               Q1.ParamByName['xggDiffUcRic'] := RxSpinEdit2.AsInteger;
               Q1.ParamByName['xggDiffColRic'] := RxSpinEdit3.AsInteger;
               Q1.ExecSQL;
               DB.CommitTrans;
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
          end;
     end;
end;
//[/TONI20020905\]FINE

procedure TLegendaElRicForm.SpeedButton1Click(Sender: TObject);
begin
ApriHelp('214');
end;

end.
