object OfferteForm: TOfferteForm
  Left = 353
  Top = 148
  Width = 1085
  Height = 696
  Caption = 'Visualizzazione offerte ai clienti'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel4: TPanel
    Left = 0
    Top = 0
    Width = 1077
    Height = 39
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object ToolbarButton9723: TToolbarButton97
      Left = 2
      Top = 1
      Width = 86
      Height = 37
      Caption = 'Dettaglio offerta'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
        555555FFFFFFFFFF5F5557777777777505555777777777757F55555555555555
        055555555555FF5575F555555550055030555555555775F7F7F55555550FB000
        005555555575577777F5555550FB0BF0F05555555755755757F555550FBFBF0F
        B05555557F55557557F555550BFBF0FB005555557F55575577F555500FBFBFB0
        305555577F555557F7F5550E0BFBFB003055557575F55577F7F550EEE0BFB0B0
        305557FF575F5757F7F5000EEE0BFBF03055777FF575FFF7F7F50000EEE00000
        30557777FF577777F7F500000E05555BB05577777F75555777F5500000555550
        3055577777555557F7F555000555555999555577755555577755}
      NumGlyphs = 2
      Opaque = False
      WordWrap = True
      OnClick = ToolbarButton9723Click
    end
    object ToolbarButton971: TToolbarButton97
      Left = 88
      Top = 1
      Width = 102
      Height = 37
      DropdownMenu = PMStampa
      Caption = 'Stampa/esporta'
    end
    object BitBtn1: TToolbarButton97
      Left = 987
      Top = 1
      Width = 85
      Height = 37
      Caption = 'Esci'
      Glyph.Data = {
        76060000424D7606000000000000360400002800000018000000180000000100
        080000000000400200000000000000000000000100000000000000000000FFFF
        FF00FF00FF0040D86800B1AEAC00545149001D973F0076967600C4F1D1002827
        270079D2910045A35E00716D5E00CFC7C00024C04D009B958D005FBA77005CE6
        8000E4EFE70090B69900B1D9BC005C8864003D3C390076B3870034934E003ABD
        5D007C7B730054D073005F5E5E00938F7800AAA695002CD358009BC6A60062A4
        73006AC98300E0D5C70049EC7500C1BBB7004FB96A00D5EEDB00697C6D008187
        820081C091002AAF4D00B5E9C300F1F7F2009D9D9C003432310044C666005397
        60009F9B7F0037CC5E004A49430039A0550072A67F007C9B83005F7763006E6E
        6D0055A66C006BB87F00A7A6A5008B8C880030C257004ADC700086B48800668B
        6C0060B06C0077C48B00259F470092C29C00C8C2B00054C27100E7FAEC0065B0
        790085827000C5C3C20056DC790071D08A005B584E005BCA79009A939900DCF3
        E3004DCC6E0086C4960080AC8C0032B054007572710064C17D003DA95B0041B4
        6000C9E7D100A39F9200B9F1C800F8FBF900D9D1C000BBB5B50046E67000B1AE
        A20048AB630080C793008DBB91006D9276006EAF7F0064D081004EE3750043CF
        680078BB890056B66F002ABD52007FB68D00CEECD6003FC262007E7E7B006F88
        7000329F500094908800D0CCB90031CC5A00C1B9AE00489B5F00A7A49E003BD1
        6300C4BBC100679F6F0077CD8E00CCC7C60055E57A004FA66700888886005DB5
        720072C0870095BD9700ACABA8005CA76F008CC09A006360610030BD56004DD1
        700046BF66005BC37700CBC8B600BBB8B90038B7590049A85B00AFAA98008482
        7500A09A8D0073C9890033D05D00B5B2A80059B16F00D9EEDF0087BB940033C6
        5A0096C3A1006DC4850069AB7A0040DE6A0051D775005B59590049D76D009B98
        80008F8D8500ECF6EF00E5F4E90028C35100B8AFB1003FCB63005AE07E009C97
        880065CA7F007B967A0062B47300D3EAD9004949480042BA610079767500A2A1
        9F006CCD8600A2A1990070B5820074B98600D4CFBC00C4BFB9002BC756003CA4
        58004AC66B008DB68E007CC08D00BFBDBE0068BD7E003CC6610049E773003131
        2F00E0F2E500C9C1BF0045E06E004FD472006B866D0082817D0094BE9F005FBF
        790065A97700C1EDCD003BD6640053E1790063786600B1AFA700ABA9A4007CCC
        9100C9BFB40037BC5A003DB45D00657C6A009BC1A600D1EFD90051B56C007DBA
        8E007A7870009B9B9B007BB88B00D9F2DF00B8ECC60026B04B002BB34F004C9A
        61008F8F8C00E8F5EB0059C3740093C69F0062B478008CB6970085BE9300A5A4
        A20055D476009D969A0053B96E005CBE76007CC78F00DEEEE300CAEDD300D3CB
        BD00BFB6B400B7B3B20056544B00DCD3C200CFC9BC00C6BDBD0037CF5F0041DA
        6A0042A45D00ADADAC0057E17C00A9A8A800A4A0950086868300020202020202
        022E39AEC10909C1AE392E020202020202020202020202B11C2F1C507ABDBD7A
        EB872F9F2E0202020202020202028016567DE5E488757588E4E57DB016FF0202
        0202020202C734A69A3E033FCDFCFCCD3FCC3E455F34700202020202E205F7AC
        94A03F69687E1111A84CA09481C3F43D0202023C4EF2AC7579CC30E31B60C068
        CDFC4CC57596254EE902021A7383B833796FC26EA3EA24C0C43F4C9E89B8640F
        DA023C0C0D2B993319C26E6E6EA31B60C4F9039EC5A7DF0D0CE973A9BB6C998E
        C26E6ED76ED7A3529D03797989526C4092A29176428855C2AD6EDD4851D727A3
        BACC7933A752198FD2914A23358AA4ADAD51081F5CC22727A38AF8339930AF06
        234A1DB6B98BD797C2087579945CA42727A3AF9999D3AF448C1DA174FAEC47D7
        CBA533333375DEA42797A3D46CE026724632A1F5776BEDB24FBF3E999933B8DE
        E39797A35859D8185EA1A9F67B961057224D6752BF3E3E0E2CE3EF122D966231
        F1A9B346ABE6E6BE9B937C0A0A0A4DB2AAF02D12A3ADFA0746FE841ECF21493B
        8243EED1D1D17C939BC95A5DAD0BE1D0908402FEF671B5B46ABC63636363EE43
        823B81147F3AC6F6FE0202FB5BB741D9DC2A535353532ABCB53B4985851525FE
        FB0202028461CF41869886868686E8D91766CA9C15D095D00202020202D0CF5F
        CEE7209A9A86986D17173638F395D00202020202020284787D3D283713D6C854
        65D53D7D7884020202020202020202FBB1044B8D2E2929DB8D4B04B1FB020202
        020202020202020202FBB1DBB1FDFD2EDBB18402020202020202}
      OnClick = BitBtn1Click
    end
  end
  object dxDBGrid1: TdxDBGrid
    Left = 0
    Top = 39
    Width = 1077
    Height = 630
    Bands = <
      item
      end>
    DefaultLayout = True
    HeaderPanelRowCount = 1
    KeyField = 'ID'
    ShowGroupPanel = True
    ShowSummaryFooter = True
    SummaryGroups = <
      item
        DefaultGroup = False
        SummaryItems = <
          item
            ColumnName = 'dxDBGrid1Stato'
            SummaryField = 'TotaleCalc'
            SummaryFormat = '#,###'
            SummaryType = cstSum
          end
          item
            ColumnName = 'dxDBGrid1Stato'
            SummaryField = 'SoloAccettati'
            SummaryFormat = '#,###'
            SummaryType = cstSum
          end>
        Name = 'Default'
      end>
    SummarySeparator = ', '
    Align = alClient
    TabOrder = 1
    OnMouseUp = dxDBGrid1MouseUp
    DataSource = DsQOfferte
    Filter.Active = True
    Filter.Criteria = {00000000}
    LookAndFeel = lfFlat
    OptionsBehavior = [edgoAutoSort, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoStoreToRegistry, edgoTabThrough, edgoVertThrough]
    OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
    OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoRowSelect, edgoUseBitmap]
    RegistryPath = '\Software\H1\DBGOfferte'
    OnCustomDrawCell = dxDBGrid1CustomDrawCell
    OnCustomDrawFooter = dxDBGrid1CustomDrawFooter
    object dxDBGrid1ID: TdxDBGridMaskColumn
      Alignment = taCenter
      Caption = 'Rif'
      Color = clBtnFace
      Visible = False
      Width = 46
      BandIndex = 0
      RowIndex = 0
      FieldName = 'ID'
      SummaryFooterType = cstCount
      DisableFilter = True
    end
    object dxDBGrid1Rif: TdxDBGridMaskColumn
      Color = clBtnFace
      Width = 21
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Rif'
      SummaryFooterType = cstCount
      DisableFilter = True
    end
    object dxDBGrid1Cliente: TdxDBGridMaskColumn
      Sorted = csUp
      Width = 68
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Cliente'
      SummaryType = cstCount
      SummaryField = 'ID'
      SummaryFormat = '  Totale offerte = #,###'
      SummaryGroupName = 'Default'
    end
    object dxDBGrid1Data: TdxDBGridDateColumn
      Width = 55
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Data'
    end
    object dxDBGrid1Tipo: TdxDBGridMaskColumn
      Width = 44
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Tipo'
      SummaryType = cstCount
      SummaryField = 'ID'
      SummaryFormat = '  Totale offerte = #,###'
      SummaryGroupName = 'Default'
    end
    object dxDBGrid1AreaRuolo: TdxDBGridColumn
      Caption = 'Area (ruolo)'
      Width = 92
      BandIndex = 0
      RowIndex = 0
      FieldName = 'AreaRuolo'
    end
    object dxDBGrid1Ruolo: TdxDBGridColumn
      Width = 66
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Ruolo'
    end
    object dxDBGrid1IDCliente: TdxDBGridMaskColumn
      Visible = False
      Width = 46
      BandIndex = 0
      RowIndex = 0
      FieldName = 'IDCliente'
    end
    object dxDBGrid1AMezzo: TdxDBGridMaskColumn
      Caption = 'a mezzo'
      Width = 51
      BandIndex = 0
      RowIndex = 0
      FieldName = 'AMezzo'
      SummaryType = cstCount
      SummaryField = 'ID'
      SummaryFormat = '  Totale offerte = #,###'
    end
    object dxDBGrid1AttenzioneDi: TdxDBGridMaskColumn
      Caption = 'all'#39'attenzione di'
      Width = 58
      BandIndex = 0
      RowIndex = 0
      FieldName = 'AttenzioneDi'
    end
    object dxDBGrid1IDContratto: TdxDBGridMaskColumn
      Visible = False
      Width = 46
      BandIndex = 0
      RowIndex = 0
      FieldName = 'IDContratto'
    end
    object dxDBGrid1Stato: TdxDBGridMaskColumn
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clPurple
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Width = 43
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Stato'
      SummaryType = cstCount
      SummaryField = 'ID'
      SummaryFormat = '  Totale offerte = #,###'
      SummaryGroupName = 'Default'
    end
    object dxDBGrid1Condizioni: TdxDBGridMaskColumn
      Visible = False
      Width = 85
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Condizioni'
    end
    object dxDBGrid1ImportoTotale: TdxDBGridCurrencyColumn
      Caption = 'Imp.Totale'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      Width = 45
      BandIndex = 0
      RowIndex = 0
      FieldName = 'TotaleCalc'
      SummaryFooterType = cstSum
      SummaryFooterField = 'TotaleCalc'
      SummaryFooterFormat = ',0.00'
      DisplayFormat = ',0.00'
      Nullable = False
      SummaryType = cstSum
    end
    object dxDBGrid1Esito: TdxDBGridMaskColumn
      Visible = False
      Width = 46
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Esito'
    end
    object dxDBGrid1Note: TdxDBGridMaskColumn
      Visible = False
      Width = 136
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Note'
    end
    object dxDBGrid1IDUtente: TdxDBGridMaskColumn
      Visible = False
      Width = 46
      BandIndex = 0
      RowIndex = 0
      FieldName = 'IDUtente'
    end
    object dxDBGrid1IDRicerca: TdxDBGridMaskColumn
      Visible = False
      Width = 46
      BandIndex = 0
      RowIndex = 0
      FieldName = 'IDRicerca'
    end
    object dxDBGrid1LineaProdotto: TdxDBGridColumn
      Caption = 'Linea di Prodotto'
      Visible = False
      Width = 66
      BandIndex = 0
      RowIndex = 0
      FieldName = 'LineaProdotto'
    end
    object dxDBGrid1Area: TdxDBGridColumn
      Caption = 'Area/Filone prof.'
      Visible = False
      Width = 46
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Area'
    end
    object dxDBGrid1SoloAccettati: TdxDBGridCurrencyColumn
      Caption = 'Importi accettati'
      Width = 57
      BandIndex = 0
      RowIndex = 0
      FieldName = 'SoloAccettati'
      SummaryFooterType = cstSum
      SummaryFooterField = 'SoloAccettati'
      SummaryFooterFormat = ',0.00'
      DisplayFormat = ',0.00'
      Nullable = False
      SummaryType = cstSum
    end
    object dxDBGrid1Column20: TdxDBGridColumn
      Caption = 'Sede Operativa'
      Width = 63
      BandIndex = 0
      RowIndex = 0
      FieldName = 'SedeOperativa'
    end
    object dxDBGrid1ColumnComune: TdxDBGridColumn
      Width = 68
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Comune'
    end
    object dxDBGrid1ColumnProvincia: TdxDBGridColumn
      Width = 49
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Provincia'
    end
    object dxDBGrid1ConoscTramite: TdxDBGridColumn
      Caption = 'Conosc.tramite'
      Width = 62
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Tramite'
    end
    object dxDBGrid1Column24: TdxDBGridColumn
      Caption = 'Data acc.'
      Width = 51
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Data_acc'
    end
    object dxDBGrid1Column25: TdxDBGridCheckColumn
      Caption = 'Prima vend.'
      Width = 38
      BandIndex = 0
      RowIndex = 0
      FieldName = 'PrimaVendita'
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object dxDBGrid1Column26: TdxDBGridCheckColumn
      Caption = 'UpSell.'
      Width = 64
      BandIndex = 0
      RowIndex = 0
      FieldName = 'UpSelling'
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object dxDBGrid1Condizione: TdxDBGridColumn
      Visible = False
      Width = 72
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Condizione'
    end
    object dxDBGrid1TotCalcPerc: TdxDBGridCurrencyColumn
      Caption = 'Totale cond.'
      Visible = False
      Width = 65
      BandIndex = 0
      RowIndex = 0
      FieldName = 'TotCalcPerc'
      SummaryFooterType = cstSum
      SummaryFooterField = 'TotCalcPerc'
      SummaryFooterFormat = ',0.00'
      Nullable = False
    end
    object dxDBGrid1Column31: TdxDBGridDateColumn
      Caption = 'Ult.contatto'
      BandIndex = 0
      RowIndex = 0
      FieldName = 'DataUltimoContatto'
    end
    object dxDBGrid1Column32: TdxDBGridColumn
      Width = 80
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Utente'
    end
    object dxDBGrid1Column33: TdxDBGridColumn
      Caption = 'Data prev.chius.'
      BandIndex = 0
      RowIndex = 0
      FieldName = 'DataPrevChiusura'
      SummaryFooterType = cstSum
      SummaryFooterField = 'TotCalcPerc_p'
      SummaryFooterFormat = ',0.00'
    end
    object dxDBGrid1Column34: TdxDBGridColumn
      Visible = False
      BandIndex = 0
      RowIndex = 0
      FieldName = 'TotCalcPerc_p'
      SummaryFooterType = cstSum
      SummaryFooterField = 'TotCalcPerc_p'
      SummaryFooterFormat = ',0.00'
    end
    object dxDBGrid1Column35: TdxDBGridColumn
      Caption = 'Titolo cliente'
      Width = 90
      BandIndex = 0
      RowIndex = 0
      FieldName = 'TitoloCliente'
    end
  end
  object QOfferte_OLD: TQuery
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      
        'select EBC_Offerte.*,EBC_Clienti.Descrizione Cliente, LineaProdo' +
        'tto, '
      '          Aree.Descrizione Area'
      
        'from EBC_Offerte join EBC_Clienti on EBC_Offerte.IDCliente=EBC_C' +
        'lienti.ID'
      
        'left join CG_LineeProdotto on EBC_Offerte.IDLineaProdotto = CG_L' +
        'ineeProdotto.ID'
      'left join Aree on EBC_Offerte.IDArea = Aree.ID')
    Left = 32
    Top = 416
    object QOfferte_OLDID: TAutoIncField
      FieldName = 'ID'
    end
    object QOfferte_OLDIDCliente: TIntegerField
      FieldName = 'IDCliente'
    end
    object QOfferte_OLDRif: TStringField
      FieldName = 'Rif'
      FixedChar = True
      Size = 10
    end
    object QOfferte_OLDData: TDateTimeField
      FieldName = 'Data'
    end
    object QOfferte_OLDAMezzo: TStringField
      FieldName = 'AMezzo'
      FixedChar = True
      Size = 15
    end
    object QOfferte_OLDAttenzioneDi: TStringField
      FieldName = 'AttenzioneDi'
      FixedChar = True
      Size = 40
    end
    object QOfferte_OLDAnticipoRichiesto: TFloatField
      FieldName = 'AnticipoRichiesto'
      DisplayFormat = '#,###'
    end
    object QOfferte_OLDCondizioni: TStringField
      FieldName = 'Condizioni'
      FixedChar = True
      Size = 50
    end
    object QOfferte_OLDEsito: TStringField
      FieldName = 'Esito'
      FixedChar = True
    end
    object QOfferte_OLDNote: TStringField
      FieldName = 'Note'
      FixedChar = True
      Size = 80
    end
    object QOfferte_OLDStato: TStringField
      FieldName = 'Stato'
      FixedChar = True
    end
    object QOfferte_OLDTipo: TStringField
      FieldName = 'Tipo'
      FixedChar = True
      Size = 30
    end
    object QOfferte_OLDIDUtente: TIntegerField
      FieldName = 'IDUtente'
    end
    object QOfferte_OLDImportoTotale: TFloatField
      FieldName = 'ImportoTotale'
      DisplayFormat = '#,###.##'
    end
    object QOfferte_OLDCliente: TStringField
      FieldName = 'Cliente'
      FixedChar = True
      Size = 30
    end
    object QOfferte_OLDIDRicerca: TIntegerField
      FieldName = 'IDRicerca'
    end
    object QOfferte_OLDIDContratto: TIntegerField
      FieldName = 'IDContratto'
    end
    object QOfferte_OLDIDLineaProdotto: TIntegerField
      FieldName = 'IDLineaProdotto'
    end
    object QOfferte_OLDLineaProdotto: TStringField
      FieldName = 'LineaProdotto'
      FixedChar = True
      Size = 50
    end
    object QOfferte_OLDIDArea: TIntegerField
      FieldName = 'IDArea'
    end
    object QOfferte_OLDArea: TStringField
      FieldName = 'Area'
      FixedChar = True
      Size = 30
    end
  end
  object DsQOfferte: TDataSource
    DataSet = QOfferte
    Left = 144
    Top = 256
  end
  object PMStampa: TPopupMenu
    Left = 144
    Top = 152
    object stampagriglia1: TMenuItem
      Caption = 'stampa griglia'
      ImageIndex = 18
      OnClick = stampagriglia1Click
    end
    object esportainExcel1: TMenuItem
      Caption = 'esporta in Excel'
      ImageIndex = 19
      OnClick = esportainExcel1Click
    end
    object esportainHTML1: TMenuItem
      Caption = 'esporta in HTML'
      ImageIndex = 20
      OnClick = esportainHTML1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Ripristinaimpostazionigriglia1: TMenuItem
      Caption = 'Ripristina impostazioni griglia'
      OnClick = Ripristinaimpostazionigriglia1Click
    end
  end
  object dxPrinter1: TdxComponentPrinter
    CurrentLink = dxPrinter1Link1
    Version = 0
    Left = 216
    Top = 176
    object dxPrinter1Link1: TdxDBGridReportLink
      Caption = 'dxPrinter1Link1'
      Component = dxDBGrid1
      DesignerHelpContext = 0
      PrinterPage.Background.Brush.Style = bsClear
      PrinterPage.Footer = 6350
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 25400
      PrinterPage.Margins.Left = 25400
      PrinterPage.Margins.Right = 25400
      PrinterPage.Margins.Top = 10000
      PrinterPage.PageFooter.Font.Charset = DEFAULT_CHARSET
      PrinterPage.PageFooter.Font.Color = clWindowText
      PrinterPage.PageFooter.Font.Height = -11
      PrinterPage.PageFooter.Font.Name = 'Tahoma'
      PrinterPage.PageFooter.Font.Style = []
      PrinterPage.PageHeader.Font.Charset = DEFAULT_CHARSET
      PrinterPage.PageHeader.Font.Color = clWindowText
      PrinterPage.PageHeader.Font.Height = -11
      PrinterPage.PageHeader.Font.Name = 'Tahoma'
      PrinterPage.PageHeader.Font.Style = []
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportTitle.Font.Charset = DEFAULT_CHARSET
      ReportTitle.Font.Color = clWindowText
      ReportTitle.Font.Height = -19
      ReportTitle.Font.Name = 'Times New Roman'
      ReportTitle.Font.Style = [fsBold]
      BandColor = clBtnFace
      BandFont.Charset = DEFAULT_CHARSET
      BandFont.Color = clWindowText
      BandFont.Height = -11
      BandFont.Name = 'MS Sans Serif'
      BandFont.Style = []
      Color = clWindow
      EvenFont.Charset = DEFAULT_CHARSET
      EvenFont.Color = clWindowText
      EvenFont.Height = -11
      EvenFont.Name = 'Times New Roman'
      EvenFont.Style = []
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      FooterFont.Charset = DEFAULT_CHARSET
      FooterFont.Color = clWindowText
      FooterFont.Height = -11
      FooterFont.Name = 'MS Sans Serif'
      FooterFont.Style = []
      GridLineColor = clBtnFace
      GroupNodeFont.Charset = DEFAULT_CHARSET
      GroupNodeFont.Color = clWindowText
      GroupNodeFont.Height = -11
      GroupNodeFont.Name = 'Times New Roman'
      GroupNodeFont.Style = []
      GroupNodeColor = clBtnFace
      HeaderColor = clBtnFace
      HeaderFont.Charset = DEFAULT_CHARSET
      HeaderFont.Color = clWindowText
      HeaderFont.Height = -11
      HeaderFont.Name = 'MS Sans Serif'
      HeaderFont.Style = []
      OddColor = clWindow
      OddFont.Charset = DEFAULT_CHARSET
      OddFont.Color = clWindowText
      OddFont.Height = -11
      OddFont.Name = 'Times New Roman'
      OddFont.Style = []
      Options = [tlpoBands, tlpoHeaders, tlpoFooters, tlpoRowFooters, tlpoPreview, tlpoPreviewGrid, tlpoGrid, tlpoFlatCheckMarks, tlpoImages, tlpoStateImages]
      PreviewFont.Charset = DEFAULT_CHARSET
      PreviewFont.Color = clBlue
      PreviewFont.Height = -11
      PreviewFont.Name = 'MS Sans Serif'
      PreviewFont.Style = []
      RowFooterColor = cl3DLight
      RowFooterFont.Charset = DEFAULT_CHARSET
      RowFooterFont.Color = clWindowText
      RowFooterFont.Height = -11
      RowFooterFont.Name = 'MS Sans Serif'
      RowFooterFont.Style = []
      BuiltInReportLink = True
    end
  end
  object QOfferte: TADOLinkedQuery
    Connection = Data.DB
    CursorType = ctStatic
    OnCalcFields = QOfferteCalcFields
    Parameters = <>
    SQL.Strings = (
      
        'select distinct Off_dett_MinID.MinID, Data_acc, Off_dett.Area Ar' +
        'eaRuolo, Off_dett.Ruolo,'
      #9#9'EBC_Offerte.*,EBC_Clienti.Descrizione Cliente, LineaProdotto, '
      
        '        Aree.Descrizione Area, Aziende.descrizione SedeOperativa' +
        ',('
      #9'select sum(Importo)'
      #9'from ebc_offertadettaglio'
      #9'where accettata = 1'
      #9'and ebc_offertadettaglio.idofferta=EBC_Offerte.ID'
      #9'group by idofferta'
      
        #9') as SoloAccettati, EBC_Clienti.Comune, EBC_Clienti.Provincia, ' +
        'TabConoscTramite.Tramite,'
      
        #9'OC.Condizione, OC.Perc PercCondizione, DataUltimoContatto, Data' +
        'PrevChiusura,'
      #9'TitoloCliente'
      'from EBC_Offerte'
      'join EBC_Clienti on EBC_Offerte.IDCliente = EBC_Clienti.ID'
      
        'left outer join CG_LineeProdotto on EBC_Offerte.IDLineaProdotto ' +
        '= CG_LineeProdotto.ID'
      'left outer join Aree on EBC_Offerte.IDArea = Aree.ID'
      
        'left outer join Aziende on EBC_offerte.IDSedeOperativa = aziende' +
        '.id'
      
        'left outer join TabConoscTramite on EBC_Clienti.IDConoscTramite ' +
        '= TabConoscTramite.ID'
      'left outer join ('
      #9'select IDOfferta, max(DallaData) Data_acc'
      #9'from EBC_OfferteStorico '
      #9'where Stato = '#39'accettata'#39
      #9'group by IDOfferta'
      ') Off_Storico on EBC_Offerte.ID = Off_Storico.IDOfferta'
      'left outer join ('
      #9'select IDOfferta, min(ID) MinID'
      #9'from EBC_OffertaDettaglio '
      #9'group by IDOfferta'
      #9') Off_dett_MinID on EBC_Offerte.ID = Off_dett_MinID.IDOfferta'
      'left outer join ('
      
        #9'select OD.ID, A.Descrizione Area, M.Descrizione Ruolo, OD.Titol' +
        'oCliente'
      #9'from EBC_OffertaDettaglio OD'
      #9'join Mansioni M on OD.IDMansione = M.ID'
      #9'join Aree A on M.IDArea = A.ID'
      ') Off_dett on Off_dett_MinID.MinID = Off_Dett.ID'
      
        'left outer join EBC_OfferteTabCondizioni OC on EBC_Offerte.IDCon' +
        'dizione = OC.ID'
      'left outer join ('
      #9'select IDCliente, max(ID) MaxID'
      #9'from EBC_ContattiFatti '
      #9'group by IDCliente'
      #9') Contatti_MaxID on EBC_Clienti.ID = Contatti_MaxID.IDCliente'
      'left outer join ('
      #9'select CF.ID, CF.Data DataUltimoContatto'
      #9'from EBC_ContattiFatti CF'
      ') ContattiFatti on Contatti_MaxID.MaxID = ContattiFatti.ID'
      'order by EBC_Offerte.ID desc')
    UseFilter = False
    Left = 144
    Top = 224
    object QOfferteID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QOfferteIDCliente: TIntegerField
      FieldName = 'IDCliente'
    end
    object QOfferteRif: TStringField
      FieldName = 'Rif'
      Size = 10
    end
    object QOfferteData: TDateTimeField
      FieldName = 'Data'
    end
    object QOfferteAMezzo: TStringField
      FieldName = 'AMezzo'
      Size = 15
    end
    object QOfferteAttenzioneDi: TStringField
      FieldName = 'AttenzioneDi'
      Size = 40
    end
    object QOfferteAnticipoRichiesto: TFloatField
      FieldName = 'AnticipoRichiesto'
    end
    object QOfferteCondizioni: TStringField
      FieldName = 'Condizioni'
      Size = 50
    end
    object QOfferteEsito: TStringField
      FieldName = 'Esito'
    end
    object QOfferteNote: TStringField
      FieldName = 'Note'
      Size = 80
    end
    object QOfferteStato: TStringField
      FieldName = 'Stato'
    end
    object QOfferteTipo: TStringField
      FieldName = 'Tipo'
      Size = 30
    end
    object QOfferteIDUtente: TIntegerField
      FieldName = 'IDUtente'
    end
    object QOfferteImportoTotale: TFloatField
      FieldName = 'ImportoTotale'
      DisplayFormat = '#,###.00'
    end
    object QOfferteIDRicerca: TIntegerField
      FieldName = 'IDRicerca'
    end
    object QOfferteIDContratto: TIntegerField
      FieldName = 'IDContratto'
    end
    object QOfferteIDContrattoCG: TIntegerField
      FieldName = 'IDContrattoCG'
    end
    object QOfferteIDArea: TIntegerField
      FieldName = 'IDArea'
    end
    object QOfferteIDLineaProdotto: TIntegerField
      FieldName = 'IDLineaProdotto'
    end
    object QOfferteCliente: TStringField
      FieldName = 'Cliente'
      Size = 50
    end
    object QOfferteLineaProdotto: TStringField
      FieldName = 'LineaProdotto'
      Size = 50
    end
    object QOfferteArea: TStringField
      FieldName = 'Area'
      Size = 30
    end
    object QOfferteSoloAccettati: TFloatField
      FieldName = 'SoloAccettati'
      ReadOnly = True
    end
    object QOfferteSedeOperativa: TStringField
      FieldName = 'SedeOperativa'
      FixedChar = True
      Size = 30
    end
    object QOfferteComune: TStringField
      FieldName = 'Comune'
      Size = 100
    end
    object QOfferteProvincia: TStringField
      FieldName = 'Provincia'
      Size = 2
    end
    object QOfferteTramite: TStringField
      FieldName = 'Tramite'
      Size = 50
    end
    object QOfferteData_acc: TDateTimeField
      FieldName = 'Data_acc'
      ReadOnly = True
    end
    object QOffertePrimaVendita: TBooleanField
      FieldName = 'PrimaVendita'
    end
    object QOfferteUpSelling: TBooleanField
      FieldName = 'UpSelling'
    end
    object QOfferteTotaleCalc: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TotaleCalc'
      Calculated = True
    end
    object QOfferteTotProdServ: TFloatField
      FieldName = 'TotProdServ'
    end
    object QOfferteAreaRuolo: TStringField
      FieldName = 'AreaRuolo'
      Size = 100
    end
    object QOfferteRuolo: TStringField
      FieldName = 'Ruolo'
      Size = 100
    end
    object QOfferteIDCondizione: TIntegerField
      FieldName = 'IDCondizione'
    end
    object QOfferteCondizione: TStringField
      FieldName = 'Condizione'
      Size = 50
    end
    object QOffertePercCondizione: TSmallintField
      FieldName = 'PercCondizione'
    end
    object QOfferteTotCalcPerc: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TotCalcPerc'
      Calculated = True
    end
    object QOfferteDataUltimoContatto: TDateTimeField
      FieldName = 'DataUltimoContatto'
    end
    object QOfferteUtente: TStringField
      FieldKind = fkLookup
      FieldName = 'Utente'
      LookupDataSet = QUsers
      LookupKeyFields = 'ID'
      LookupResultField = 'Nominativo'
      KeyFields = 'IDUtente'
      Size = 30
      Lookup = True
    end
    object QOfferteDataPrevChiusura: TDateField
      FieldName = 'DataPrevChiusura'
    end
    object QOfferteTotCalcPerc_p: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TotCalcPerc_p'
      Calculated = True
    end
    object QOfferteTitoloCliente: TStringField
      FieldName = 'TitoloCliente'
      Size = 50
    end
  end
  object QUsers: TADOLinkedQuery
    Connection = Data.DB
    Parameters = <>
    SQL.Strings = (
      'select ID,Nominativo,Descrizione from Users ')
    UseFilter = False
    Left = 96
    Top = 240
    object QUsersID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QUsersNominativo: TStringField
      FieldName = 'Nominativo'
      Size = 30
    end
  end
  object QTemp: TADOQuery
    Connection = Data.DB
    Parameters = <>
    Left = 32
    Top = 480
  end
end
