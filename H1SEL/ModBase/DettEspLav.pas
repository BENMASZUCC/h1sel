unit DettEspLav;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, DBCtrls, Mask, ExtCtrls, DtEdit97, DtEdDB97, dxCntner,
     dxEditor, dxExEdtr, dxEdLib, dxDBELib, TB97;

type
     TDettEspLavForm = class(TForm)
          Label44: TLabel;
          DBMemo2: TDBMemo;
          Label45: TLabel;
          DBMemo3: TDBMemo;
          Panel1: TPanel;
          Label1: TLabel;
          DBEdit23: TDBEdit;
          Label36: TLabel;
          DBEdit27: TDBEdit;
          Label2: TLabel;
          DBMemo1: TDBMemo;
          GroupBox2: TGroupBox;
          Label3: TLabel;
          DBCheckBox1: TDBCheckBox;
          DBCheckBox2: TDBCheckBox;
          Label4: TLabel;
          dxDBSpinEdit1: TdxDBSpinEdit;
          dxDBSpinEdit2: TdxDBSpinEdit;
          Label5: TLabel;
          DBEdit1: TDBEdit;
          Label6: TLabel;
          DBEdit2: TDBEdit;
          Label7: TLabel;
          DBMemo4: TDBMemo;
          Label8: TLabel;
          Panel2: TPanel;
          BOK: TToolbarButton97;
          BAnnulla: TToolbarButton97;
          procedure FormShow(Sender: TObject);
          procedure BOKClick(Sender: TObject);
          procedure BAnnullaClick(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     DettEspLavForm: TDettEspLavForm;

implementation

uses ModuloDati, Main;

{$R *.DFM}


procedure TDettEspLavForm.FormShow(Sender: TObject);
begin
     //Grafica
     DettEspLavForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     Caption := '[S/20] - ' + Caption;
end;

procedure TDettEspLavForm.BOKClick(Sender: TObject);
begin
     //close;
     DettEspLavForm.ModalResult := mrOk;
end;

procedure TDettEspLavForm.BAnnullaClick(Sender: TObject);
begin
     DettEspLavForm.ModalResult := mrCancel;
end;

end.

