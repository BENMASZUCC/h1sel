object RicCandAnnunciForm: TRicCandAnnunciForm
  Left = 568
  Top = 143
  BorderStyle = bsDialog
  Caption = 'Elenco filtri e pesi '
  ClientHeight = 276
  ClientWidth = 924
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 924
    Height = 196
    Align = alTop
    Caption = 'Panel1'
    TabOrder = 0
    object Panel2: TPanel
      Left = 855
      Top = 1
      Width = 68
      Height = 194
      Align = alRight
      TabOrder = 0
      object PanCriteriButtons: TPanel
        Left = 1
        Top = 1
        Width = 66
        Height = 192
        Align = alTop
        BevelOuter = bvLowered
        TabOrder = 0
        object BCriterioNew: TToolbarButton97
          Left = 1
          Top = 1
          Width = 32
          Height = 30
          Hint = 'nuovo criterio'
          Glyph.Data = {
            36050000424D3605000000000000360400002800000010000000100000000100
            08000000000000010000C40E0000C40E0000000100000001000000000000FFFF
            FF0000000000D6A78900EAD2C200FFFBF800EDBC9400F4D2B500E5C5AA00F7E0
            CD00F8E2D000F7C49600B09A8700B6A89C00FDF4EC00FCF3EB00FFCE9C00FBCA
            9A00FFCF9F00FFCFA000FFD0A200FFD1A300FFD2A400FFD3A700FFD4A900FFD5
            AB00FFD6AC00FFD7AF00FFD8B100FFD9B300FFDBB700FFDDBB00FFE1C300FFE5
            CB00FFECD900FFEEDD00FDEEDF00FBEDDF00FFF2E500FFF9F300FFFBF700FFFC
            F900FFD19E00F5CC9F00FFE9D200FFF6EC00FFBF7300FFD4A100FFDDB400FFF1
            E100FEB65500FFD9A600EECEA600FFDEAB00FFE0AD00FFECCF00FFE8B600FFEF
            BC00FFF5C200FFFFE900FFFFF200D4D5BC00BBBFAE00D7DED300315131006477
            64000DA61A00479050004461470014AD2900B7C7BA001FB83D0058B76B005BBB
            6E0025BA490025A24200278A3E0027C04D0029BF52002CC5570035CB66003DD6
            700045DD77004AE37D0057F08A0061BD7F002BAE590030B25D0047C07B00BEE3
            D400088A5C00B0DDD100EDF8F7008FF5EF0099FFFF00A5FFFF00A8FFFF00AAFE
            FF00ADFFFF00B3FFFF00BDFEFF00C3FFFF00D3FFFF00D6FFFF00DBFFFF00F4FF
            FF00EFF5F500FAFFFF0086ECEF0097FDFF0096F3F60099F5F700AAFCFF00CBFE
            FF00E4FEFF0093F9FF00AEFBFF0099F8FF0098F1F8007EB3B700CDF6F90073E6
            F2007EE6F2007FE5EF008BF2FE008EF4FF0091E4EE00B9F7FE00D7FBFF0085EE
            FC008CF2FF00A1F4FF0089EFFF00D2F9FF00DFF2F50050A9BC0075D9EC0083E9
            FF0085EAFF0086ECFF009AEEFF00B2F2FF00EDFBFE0080E6FF0099CFDB00C2F1
            FC003BB9DC0046C4E70069D8F50076DFFC007CE2FF007EE4FF007EE3FF0080E5
            FF0084E6FF0080DDF50083DFF9008AE4FE001093BE001386AB001BA1CA0023A3
            CC0026AFDC001B7B9A002EB4DC001F75910032B7E10031ACD40035BAE10032A0
            C40025728B0042BFE80051C9ED0042A2C1003C92AC004DB7DA0056B8D800336D
            800063D3F4006FD9FC0056A5BF0074DDFC0078DEFF00457F92007ADFFF007BE1
            FF0070C6E200426975008CD3EA009DD9EC00BFE6F200CFECF5000099CC000B8A
            B50016A5D5001593BE001BA7D9001CA6D5001DA8DA0021AADC0022A9D90024AB
            DD0026A9D6002CADDD002176930047B9DF0050BBE20051B9DC005CCDF40064D1
            F9005CBFE2005DBEDF0060BFDF0073D9FF0066C2E20076DBFF0077DDFF003864
            73007FCCE700B2EBFF00AFDFEF0055656B00D4EFF900DFF2F9002DAFE20030B1
            E30038B5E8003DB8EA0059C6F40062CEF9006DD3FE0070D6FF006BCAF200A2E4
            FF004E5D63004ABEF00052C1F30062C6F20066C8F2006A737700E2F4FC005AC6
            F90063CBFE0066CCFF0080D4FC0086D6FE008FD9FE0099DEFF00747474006C6C
            6C006B6B6B006767670064646400626262005E5E5E00FFFFFF00000000000000
            000000000000000000000000F8F8F8F80000000000000000000000CECEA79FDD
            FBF9F8F800000000000000CE91E7CECEA79FAABBFCF9F800000000CEBD8C9999
            999999CEA79FFDF8000000CECE788484848484848FCED9F8000000CECE916D6D
            6D6D6D6D8463CEFEF80000CED5CE6871656362628C66CED9F80000CE9696CECE
            CECE68718D8E8ECEF80000CE73737343879DCECECECDCECE000000CE5F5E4545
            43545F5ECEF8000000000000CE4A4A4A4543CDCECE0000000000000000005050
            4300000000000000000000000000005043000000000000000000000000000050
            5043000000000000000000000000000050504300000000000000}
          ParentShowHint = False
          ShowHint = True
          OnClick = BCriterioNewClick
        end
        object BCriterioMod: TToolbarButton97
          Left = 33
          Top = 1
          Width = 32
          Height = 30
          Hint = 'modifica criterio'
          Glyph.Data = {
            36050000424D3605000000000000360400002800000010000000100000000100
            08000000000000010000120B0000120B00000001000000010000000000009C63
            00009C633100FF9C3100525252009C6363009C9C6300FF9C6300A5A5A500F7CE
            A500FFCECE00FFEFCE009CFFCE00FFFFCE00F7FFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000F0F0F000000
            0F0F0F0F0F0F0F0F0F0F0F0F00060D000004040404040404040F0F0F00060700
            0600090909090909040F0F0F0F0000000D06000909090909040F0F0F0F04000F
            0A0D060009090909040F0F0F0F000F0E0F0A0D0600090909040F0F0F000F0E09
            0E0F0A0C07000909040F0F000F0D0A0E090E0F0A0F000909040F00060A000D01
            0E0A0A0F0F000A0A040F000206000006010D0B07000B0A0A040F0000000D0200
            06020700020B0B0B040F0F000F0F0D02000600070B0B0B0B040F0F0F000F0F0A
            06000B0B0B0B0808040F0F0F0F000F0F0A02000B0B0B0503040F0F0F0F04000F
            0F0D000B0B0B02040F0F0F0F0F040400000004040404040F0F0F}
          ParentShowHint = False
          ShowHint = True
          OnClick = BCriterioModClick
        end
        object BCriterioDel: TToolbarButton97
          Left = 1
          Top = 31
          Width = 32
          Height = 30
          Hint = 'elimina criterio selezionato'
          Glyph.Data = {
            36050000424D3605000000000000360400002800000010000000100000000100
            080000000000000100000000000000000000000100000000000000000000FFFF
            FF0000000000FFDFFF000E48E900134BE9002C5DE9007294F10098B1F600BACA
            F800DCE4FB000439E000053AE000083EE1000A43E8000D42E0000F43E1001547
            E0001848E0001C4BE0002352E6002456E8002C58E6002D5CE8003763E8006587
            EE006B8DF0006F8FEF007192F100B6C7F800DBE3FB00E2E8FB00EBEFFC000132
            D700022CC000022BBE00052EC100072FBE000A32C0000B33C1000D35C0001238
            C2001337BD00183CC0001F4CDF002450E400244FDF00254EDE00264EE2002951
            DC002A50DC002C55E4002C51DB004066E7004669E6006483EC00728FEE008097
            ED00B0BFF500BBC8F600E0E6FB000026B6000026B5001138D4000F30B5001941
            DE001636B8001739B9001839BB001939BA001F44D9001A39B8001A39B7001D3F
            BF002248DF001E3EBC002347D8002141BF002342BE002342BD002C52E2002A4D
            DA002C50E1002C51E1002D51DC002E51DB003054E2003053E1002F52DB003154
            E2003053DC002F51DA003558E2003759E300385AE3003A5BE3003A5CE3003B5D
            E3003F5FE4003F60E3004162E4004261E4004363E4004464E5004564E5004665
            E5004867E5004868E500496AE6004A69E6004B68E5004B69E5004E6CE6004E6D
            E6004F6CE600506DE600536FE7005470E7005673E7005A75E8005B76E8005D78
            E8005E79E800627CE900637DE800657EE9006781EA006A83E9006C86EB006B85
            EA006B83E9006C85EA006F88EB007089EB00728AEC00768DEC00778EEC007990
            EC007A91EC007E94ED008398EE00899DEE008CA0EF008B9EEE008DA1EF008596
            DC007F90D10095A8F0008A9BDE0097A9F10097A8F0009DAEF100A2B2F200A6B5
            F200B0BDF400B2BFF400BBC6F500E9EDFC004865E4004C67E500506CE600516C
            E6007086EA008093ED008195ED00B1BDF400B7C2F500EFF1FD00EEF0FC00FFFF
            FF00000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000101010101
            0101010101010101010001914042474844432A282625223D9201013F46515B55
            34312E13110F0C213E010141525D678B1F01010A1C050E0B2301014A5E669801
            3B381B090108040D24010153658F018C3635180607011A102701015C6E209C71
            6C010117151D1E12290101627401A4736B0101161419012C2B0101687701A4A0
            6A0101332D37012F490101727CA7A6A19E010150303A3C324D010174839501A3
            9F655F56800186544E01017A8A859901A5A2829A019357584F01018190398496
            A801019D8D64605A4B010188978E89857F7D7B79767064584501019B877E7875
            726D6F696361594C940100010101010101010101010101010100}
          ParentShowHint = False
          ShowHint = True
          OnClick = BCriterioDelClick
        end
        object BLineeOK: TToolbarButton97
          Left = 1
          Top = 61
          Width = 32
          Height = 30
          Caption = 'OK'
          Enabled = False
          OnClick = BLineeOKClick
        end
        object BLineeCan: TToolbarButton97
          Left = 33
          Top = 61
          Width = 32
          Height = 30
          Caption = 'Can'
          Enabled = False
          OnClick = BLineeCanClick
        end
        object BCriterioDelTutti: TToolbarButton97
          Left = 33
          Top = 31
          Width = 32
          Height = 30
          Hint = 'elimina tutti i criteri'
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333000000000
            3333333777777777F3333330F777777033333337F3F3F3F7F3333330F0808070
            33333337F7F7F7F7F3333330F080707033333337F7F7F7F7F3333330F0808070
            33333337F7F7F7F7F3333330F080707033333337F7F7F7F7F3333330F0808070
            333333F7F7F7F7F7F3F33030F080707030333737F7F7F7F7F7333300F0808070
            03333377F7F7F7F773333330F080707033333337F7F7F7F7F333333070707070
            33333337F7F7F7F7FF3333000000000003333377777777777F33330F88877777
            0333337FFFFFFFFF7F3333000000000003333377777777777333333330777033
            3333333337FFF7F3333333333000003333333333377777333333}
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          OnClick = BCriterioDelTuttiClick
        end
      end
    end
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 854
      Height = 19
      Alignment = taLeftJustify
      Anchors = [akLeft, akTop, akRight]
      Caption = '  Criteri impostati '
      Color = clGray
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
    object DBGrid3: TdxDBGrid
      Left = 1
      Top = 23
      Width = 648
      Height = 168
      Bands = <
        item
          Width = 556
        end
        item
          Caption = 'Livello lingua (>=)'
          Width = 100
        end>
      DefaultLayout = False
      HeaderPanelRowCount = 1
      KeyField = 'ID'
      ShowSummaryFooter = True
      SummaryGroups = <
        item
          DefaultGroup = False
          SummaryItems = <
            item
              ColumnName = 'DBGrid3Column11'
              SummaryField = 'Peso'
            end>
          Name = 'DBGrid3SummaryGroup2'
        end>
      SummarySeparator = ', '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      BandFont.Charset = DEFAULT_CHARSET
      BandFont.Color = clWindowText
      BandFont.Height = -11
      BandFont.Name = 'MS Sans Serif'
      BandFont.Style = []
      DataSource = DsLineeAnn
      Filter.Criteria = {00000000}
      HeaderFont.Charset = DEFAULT_CHARSET
      HeaderFont.Color = clWindowText
      HeaderFont.Height = -11
      HeaderFont.Name = 'MS Sans Serif'
      HeaderFont.Style = []
      OptionsBehavior = [edgoDragScroll, edgoEditing, edgoEnterShowEditor, edgoImmediateEditor, edgoTabThrough, edgoVertThrough]
      OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
      OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoUseBitmap]
      PreviewFont.Charset = DEFAULT_CHARSET
      PreviewFont.Color = clBlue
      PreviewFont.Height = -11
      PreviewFont.Name = 'MS Sans Serif'
      PreviewFont.Style = []
      ShowBands = True
      ShowRowFooter = True
      OnCustomDrawCell = DBGrid3CustomDrawCell
      object DBGrid3DescTabella: TdxDBGridButtonColumn
        Caption = 'Tabella'
        Color = clBtnFace
        DisableEditor = True
        Width = 123
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DescTabella'
        Buttons = <
          item
            Default = True
          end>
      end
      object DBGrid3DescCampo: TdxDBGridButtonColumn
        Caption = 'Campo'
        DisableEditor = True
        Width = 117
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DescCampo'
        Buttons = <
          item
            Default = True
          end>
      end
      object DBGrid3DescOperatore: TdxDBGridButtonColumn
        Caption = 'Operatore'
        DisableEditor = True
        Width = 87
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DescOperatore'
        Buttons = <
          item
            Default = True
          end>
      end
      object DBGrid3Valore: TdxDBGridButtonColumn
        Width = 186
        BandIndex = 0
        RowIndex = 0
        FieldName = 'Valore'
        OnEditButtonClick = DBGrid3ValoreEditButtonClick
        Buttons = <
          item
            Default = True
          end>
      end
      object DBGrid3Descrizione: TdxDBGridMaskColumn
        DisableEditor = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Visible = False
        Width = 80
        BandIndex = 0
        RowIndex = 0
        FieldName = 'Stringa'
      end
      object DBGrid3LivLinguaParlato: TdxDBGridButtonColumn
        Caption = 'Parlato'
        Width = 52
        BandIndex = 1
        RowIndex = 0
        FieldName = 'LivLinguaParlato'
        Buttons = <
          item
            Default = True
          end>
      end
      object DBGrid3LivLinguaScritto: TdxDBGridButtonColumn
        Caption = 'Scritto'
        Width = 48
        BandIndex = 1
        RowIndex = 0
        FieldName = 'LivLinguaScritto'
        Buttons = <
          item
            Default = True
          end>
      end
      object DBGrid3ID: TdxDBGridColumn
        Visible = False
        BandIndex = 0
        RowIndex = 0
        FieldName = 'ID'
      end
      object DBGrid3QueryLookup: TdxDBGridColumn
        Visible = False
        BandIndex = 0
        RowIndex = 0
        FieldName = 'QueryLookup'
      end
      object DBGrid3Column11: TdxDBGridColumn
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        BandIndex = 0
        RowIndex = 0
        FieldName = 'Peso'
        SummaryFooterType = cstSum
        SummaryFooterField = 'Peso'
        SummaryFooterFormat = '0'
        SummaryGroupName = 'DBGrid3SummaryGroup2'
      end
      object DBGrid3OpNext: TdxDBGridPickColumn
        Alignment = taCenter
        Caption = 'next'
        Color = clBtnFace
        Visible = False
        Width = 43
        BandIndex = 0
        RowIndex = 0
        FieldName = 'OpNext'
        Items.Strings = (
          'e'
          'o'
          'fine')
      end
    end
  end
  object TLineeAnn: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    BeforePost = TLineeAnnBeforePost
    Parameters = <>
    SQL.Strings = (
      'select * from Ann_LineeQuery')
    Left = 576
    Top = 136
    object TLineeAnnID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object TLineeAnnIDRicerca: TIntegerField
      FieldName = 'IDRicerca'
    end
    object TLineeAnnIDAnnuncio: TIntegerField
      FieldName = 'IDAnnuncio'
    end
    object TLineeAnnIDAnnEdizData: TIntegerField
      FieldName = 'IDAnnEdizData'
    end
    object TLineeAnnDescrizione: TStringField
      FieldName = 'Descrizione'
      Size = 200
    end
    object TLineeAnnStringa: TStringField
      FieldName = 'Stringa'
      Size = 3000
    end
    object TLineeAnnTabella: TStringField
      FieldName = 'Tabella'
      Size = 50
    end
    object TLineeAnnOpSucc: TStringField
      FieldName = 'OpSucc'
      Size = 50
    end
    object TLineeAnnOpNext: TStringField
      FieldName = 'OpNext'
      Size = 50
    end
    object TLineeAnnLivLinguaParlato: TIntegerField
      FieldName = 'LivLinguaParlato'
    end
    object TLineeAnnLivLinguaScritto: TIntegerField
      FieldName = 'LivLinguaScritto'
    end
    object TLineeAnnDescTabella: TStringField
      FieldName = 'DescTabella'
      Size = 100
    end
    object TLineeAnnDescCampo: TStringField
      FieldName = 'DescCampo'
      Size = 100
    end
    object TLineeAnnDescOperatore: TStringField
      FieldName = 'DescOperatore'
      Size = 100
    end
    object TLineeAnnValore: TStringField
      FieldName = 'Valore'
      Size = 100
    end
    object TLineeAnnQueryLookup: TStringField
      FieldName = 'QueryLookup'
      Size = 500
    end
    object TLineeAnnDescCompleta: TStringField
      FieldName = 'DescCompleta'
      Size = 500
    end
    object TLineeAnnIDTabQueryOp: TIntegerField
      FieldName = 'IDTabQueryOp'
    end
    object TLineeAnnIDUtente: TIntegerField
      FieldName = 'IDUtente'
    end
    object TLineeAnnPeso: TIntegerField
      FieldName = 'Peso'
    end
  end
  object Query1: TADOQuery
    Connection = Data.DB
    Parameters = <>
    Left = 544
    Top = 104
  end
  object DsLineeAnn: TDataSource
    DataSet = TLineeAnn
    OnStateChange = DsLineeAnnStateChange
    Left = 528
    Top = 136
  end
  object Q: TADOLinkedQuery
    Connection = Data.DB
    Parameters = <>
    UseFilter = False
    Left = 584
    Top = 96
  end
  object Q1: TADOLinkedQuery
    Connection = Data.DB
    Parameters = <>
    UseFilter = False
    Left = 616
    Top = 96
  end
end
