unit CodFiscale;

interface
uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, ExtCtrls, Db, ADODB, ModuloDati, comune, main, Nazione;


function calc_CF(cognome: string; nome: string;
     datanascita: string; sesso: char; comune, nazione: string): string;
var ch: boolean;


implementation



function calcCognome(cognome: string): string;
var ncar, i: integer;
     c: char;
     cod_cognome: string;
begin
     cognome := StringReplace(cognome, ' ', '', [rfReplaceAll, rfIgnoreCase]);
     cognome := StringReplace(cognome, '''', '', [rfReplaceAll, rfIgnoreCase]);
     ncar := Length(cognome);
     for i := 1 to ncar do begin
          c := cognome[i];
          if ((c <> 'A') and (c <> 'E') and (c <> 'I') and (c <> 'O') and (c <> 'U')) then
               cod_Cognome := cod_cognome + c;
     end;
     if (Length(cod_cognome) < 3) then
     begin
          for i := 1 to ncar do begin
               c := cognome[i];
               if ((c = 'A') or (c = 'E') or (c = 'I') or (c = 'O') or (c = 'U')) then
                    cod_Cognome := cod_cognome + c;
          end;
     end;
     if (Length(cod_cognome) < 3) then
          repeat
               cod_cognome := cod_cognome + 'x';
          until (Length(cod_cognome) = 3);
     SetLength(cod_cognome, 3);
     result := cod_cognome;
end;

function calcnome(nome: string): string;
var ncar, i: integer;
     c: char;
     cod_nome: string;
begin
     nome := StringReplace(nome, ' ', '', [rfReplaceAll, rfIgnoreCase]);
     nome := StringReplace(nome, '''', '', [rfReplaceAll, rfIgnoreCase]);
     ncar := Length(nome);
     for i := 1 to ncar do begin
          c := nome[i];
          if ((c <> 'A') and (c <> 'E') and (c <> 'I') and (c <> 'O') and (c <> 'U')) then
               cod_nome := cod_nome + c;
     end;
     if (Length(cod_nome) > 3) then begin
          cod_nome := cod_nome[1] + cod_nome[3] + cod_nome[4];
     end
     else begin
          for i := 1 to ncar do begin
               c := nome[i];
               if ((c = 'A') or (c = 'E') or (c = 'I') or (c = 'O') or (c = 'U')) then
                    cod_nome := cod_nome + c;
          end;
     end;
     if (Length(cod_nome) < 3) then
          repeat
               cod_nome := cod_nome + 'x';
          until (Length(cod_nome) = 3);

     SetLength(cod_nome, 3);
     result := cod_nome;
end;


function calcannonascita(xdata: string; sesso: char): string;
var mese, anno, cod_datanascita, gg: string;
     giorno: integer;
begin
     mese := copy(xdata, 4, 2);
     giorno := strtoint(copy(xdata, 1, 2));
     anno := copy(xdata, 9, 2);
     Data.QCF.close;
     data.QCF.Parameters[0].Value := mese;
     data.QCF.Open;


     if sesso = 'F' then
          giorno := giorno + 40;
     if (giorno < 10) then
     begin
          gg := '0' + inttostr(giorno);
     end
     else begin
          gg := inttostr(giorno);
     end;
     cod_datanascita := anno + data.QCF.fieldbyname('lettera_mese').asstring + gg;
     result := cod_datanascita;
end;

procedure salvacomune();
begin
     if comuneform.ModalResult = mrOK then
     begin
          if (ComuneForm.EDCodice.Text = '') then
          begin
               if MessageDlg('Ti sei dimenticato di inserire il codice!!!' + #13 + '(Impossibile calcolare C.F. senza codice)', mtWarning, [mbOK], 0) = mrOK then
               begin
                    ComuneForm.ShowModal;
                    salvacomune;
                    exit;
               end;
          end
          else
               with Data do begin
                    DB.BeginTrans;
                    try
                         Q1.Close;
                         Q1.SQL.text := 'insert into TabCom (Codice,Descrizione,Prov,Cap,Prefisso,Regione)' +
                              'values (:xCod,:xDescrizione,:xProv,:xCap,:xPrefisso,:xregione)';
                         Q1.Parameters.ParamByName('xCod').vAlue := ComuneForm.EDCodice.Text;
                         Q1.Parameters.ParamByName('xDescrizione').Value := ComuneForm.EDesc.Text;
                         Q1.Parameters.ParamByName('xProv').Value := ComuneForm.EProv.Text;
                         Q1.Parameters.ParamByName('xCap').VAlue := ComuneForm.ECap.Text;
                         Q1.Parameters.ParamByName('xPrefisso').vAlue := ComuneForm.EPrefisso.Text;
                         Q1.Parameters.ParamByName('xregione').vAlue := ComuneForm.eRegList.Text;
                         Q1.ExecSQL;
                         DB.CommitTrans;
                    except
                         DB.RollbackTrans;
                         MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                    end;
               end;
     end else begin
          ch := false;
          exit;
     end;
end;


function calccomune(comune, nazione: string): string;
var xCod, xDescrizione: string;
     y: integer;
begin

     data.Qcod_comuni.close;
     if ((nazione = 'IT') or (nazione = '') or (nazione = 'ITA')or (nazione = 'ITALIA')) then begin
          data.Qcod_comuni.close;
          data.Qcod_comuni.Parameters[0].Value := comune;
          data.Qcod_comuni.Open;
          xCod := data.Qcod_comuni.fieldbyname('codice').asstring;
          xDescrizione := data.Qcod_comuni.fieldbyname('Descrizione').asstring;
          y := 1;
     end else begin
          data.qtemp.close;
          data.qtemp.sql.text := 'select isnull(CodiceCF,'''') CodiceCF,DescNazione,Abbrev,Areageografica,ID from nazioni '+
          ' where UPPER(Abbrev)= UPPER(:xNaz)';
          data.qtemp.Parameters.ParamByName('xNaz').value := nazione;
          data.qtemp.open;
          if data.qtemp.RecordCount <= 0 then begin
               data.qtemp.close;
               data.qtemp.sql.text := 'select isnull(CodiceCF,'''') CodiceCF,DescNazione,Abbrev,Areageografica,ID from nazioni '+
               ' where UPPER(DescNazione)= UPPER(:xNaz)';
               data.qtemp.Parameters.ParamByName('xNaz').value := nazione;
               data.qtemp.open;
          end;
          xCod := data.qtemp.fieldbyname('CodiceCF').asString;
          xDescrizione := data.qtemp.fieldbyname('DescNazione').asstring;
          y := 2;
     end;

     if (xCod = '') then
     begin
          if (xDescrizione <> '') then
          begin
               if MessageDlg('Manca il Codice per questo/a comune/nazione - Modificarlo/a?', mtWarning, [mbYes, mbNo], 0) = mrYes then begin
                    //mainform.ECercaCom.text := data.Qcod_comuni.fieldbyname('descrizione').asstring;
                    //mainform.PageControl1.ActivePage := mainform.TSTabelle;
                    //mainform.pagecontrol3.ActivePage := mainform.TSTabCom;
                    //ch := false;
                    //exit;
                    if y = 1 then begin
                         mainform.ECercaCom.text := data.Qcod_comuni.fieldbyname('descrizione').asstring;
                         mainform.PageControl1.ActivePage := mainform.TSTabelle;
                         mainform.pagecontrol3.ActivePage := mainform.TSTabCom;
                         ch := false;
                         exit;
                    end;
                    if y = 2 then begin
                         nazioneform := TNazioneForm.Create(nil);
                         nazioneform.EDesc.text := xDescrizione;
                         nazioneform.EAbbrev.Text := data.qtemp.fieldbyname('Abbrev').asString;
                         nazioneform.EArea.Text := data.qtemp.fieldbyname('Areageografica').asString;
                         nazioneform.showmodal;
                         if nazioneform.ModalResult = mrOK then begin
                              data.q1.Close;
                              data.q1.SQL.Text := 'update Nazioni set Abbrev=:xAbbrev,DescNazione=:xDescNazione,AreaGeografica=:xAreaGeografica, CodiceCF=:xCodiceCF ' +
                                   'where ID=' + data.qtemp.fieldbyname('ID').asString;
                              data.Q1.Parameters.ParamByName('xAbbrev').value := NazioneForm.EAbbrev.text;
                              data.Q1.Parameters.ParamByName('xDescNazione').value := NazioneForm.EDesc.Text;
                              data.Q1.Parameters.ParamByName('xAreaGeografica').value := NazioneForm.EArea.Text;
                              data.Q1.Parameters.ParamByName('xCodiceCF').value := NazioneForm.ecodiceCF.text;
                              data.Q1.ExecSQL;
                              xCod := NazioneForm.ecodicecf.text;
                         end else begin
                              ch := false;
                              exit;
                         end;
                         nazioneform.free;
                    end;
               end
               else begin
                    ch := false;
                    exit;
               end;
          end
          else
               if ((data.Qcod_comuni.fieldbyname('descrizione').asstring = '') and (ch = true)) then
               begin
                    if MessageDlg('Comune non presente nel DataBase, Vuoi Inserirlo?', mtWarning, [mbYes, mbNo], 0) = mrYes then
                    begin
                         ComuneForm := TComuneForm.create(nil);
                         ComuneForm.EDesc.Text := MainForm.DBEdit65.text;
                         ComuneForm.Showmodal;
                         salvacomune;
                    end
                    else
                    begin
                         ch := false;
                         exit;
                    end;
               end;
          ComuneForm.Free;
          if ch = false then begin
               showmessage('Comune non inserito: impossibile calcolare il codice fiscale');
               exit;
          end;
          calccomune(comune, nazione);
     end;
     result := xCod;
end;



function calc_CF(cognome: string; nome: string;
     datanascita: string; sesso: char; comune, nazione: string): string;
var codice: string;
     c: char;
     i, smpari, smdispari, smtot: integer;
begin
     ch := true;
     codice := calccognome(cognome) + calcnome(nome) +
          calcannonascita(datanascita, sesso) +
          calccomune(comune, nazione);
     smdispari := 0;
     for i := 1 to Length(codice) do
     begin
          if (i mod 2 <> 0) then
          begin
               c := codice[i];
               data.Qcontrollo.close;
               data.Qcontrollo.Parameters[0].Value := c;
               data.Qcontrollo.Parameters[1].Value := 'D';
               data.Qcontrollo.Open;
               smdispari := smdispari + data.Qcontrollo.fieldbyname('codice').asinteger;
          end;
     end;
     smpari := 0;
     for i := 1 to Length(codice) do
     begin
          if (i mod 2 = 0) then
          begin
               c := codice[i];
               data.Qcontrollo.close;
               data.Qcontrollo.Parameters[0].Value := c;
               data.Qcontrollo.Parameters[1].Value := 'P';
               data.Qcontrollo.Open;
               smpari := smpari + data.Qcontrollo.fieldbyname('codice').asinteger;
          end;
     end;
     smtot := (smpari + smdispari) mod 26;
     data.qultcar.close;
     data.qultcar.Parameters[0].Value := smtot;
     data.qultcar.Open;
     result := codice + data.qultcar.fieldbyname('carattere').asstring;
end;
end.

