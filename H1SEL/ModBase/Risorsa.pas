unit Risorsa;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, TB97, ExtCtrls;

type
     TRisorsaForm = class(TForm)
          ERisorsa: TEdit;
          Label1: TLabel;
          CBColore: TComboBox;
          Label2: TLabel;
    Panel1: TPanel;
    BitBtn1: TToolbarButton97;
    BitBtn2: TToolbarButton97;
          procedure FormShow(Sender: TObject);
          procedure BitBtn1Click(Sender: TObject);
          procedure BitBtn2Click(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     RisorsaForm: TRisorsaForm;

implementation

uses Main;

{$R *.DFM}

procedure TRisorsaForm.FormShow(Sender: TObject);
begin
     //Grafica
     RisorsaForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     Caption := '[S/31] - ' + Caption;
end;

procedure TRisorsaForm.BitBtn1Click(Sender: TObject);
begin
     RisorsaForm.ModalResult:= mrOk;
end;

procedure TRisorsaForm.BitBtn2Click(Sender: TObject);
begin
     RisorsaForm.ModalResult := mrCancel;
end;

end.

