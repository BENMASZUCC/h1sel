unit TipoStrada;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, Grids, DBGrids, Db, DBTables, ADODB, U_ADOLinkCl,
  TB97, ExtCtrls;

type
     TTipoStradaForm = class(TForm)
          QNazioni_OLD: TQuery;
          DsQNazioni: TDataSource;
          DBGrid1: TDBGrid;
          Label1: TLabel;
          ETipo: TEdit;
          QNazioni_OLDID: TAutoIncField;
          QNazioni_OLDAbbrev: TStringField;
          QNazioni_OLDDescNazione: TStringField;
          QNazioni_OLDAreaGeografica: TStringField;
          QNazioni: TADOLinkedQuery;
          Panel1: TPanel;
    BitBtn1: TToolbarButton97;
    BitBtn2: TToolbarButton97;
          procedure FormShow(Sender: TObject);
          procedure BitBtn1Click(Sender: TObject);
          procedure BitBtn2Click(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     TipoStradaForm: TTipoStradaForm;

implementation
uses ModuloDati, Main;
{$R *.DFM}

procedure TTipoStradaForm.FormShow(Sender: TObject);
begin
     //Grafica
     TipoStradaForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     Caption := '[S/46] - ' + Caption;
end;

procedure TTipoStradaForm.BitBtn1Click(Sender: TObject);
begin
     TipoStradaForm.ModalResult := mrOk;
end;

procedure TTipoStradaForm.BitBtn2Click(Sender: TObject);
begin
     TipoStradaForm.ModalResult := mrCancel;
end;

end.

