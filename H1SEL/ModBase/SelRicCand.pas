unit SelRicCand;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Grids, DBGrids, Db, DBTables, ExtCtrls, StdCtrls, Buttons, ADODB,
     U_ADOLinkCl, TB97;

type
     TSelRicCandForm = class(TForm)
          Panel124: TPanel;
          DsQRicCand: TDataSource;
          DBGrid1: TDBGrid;
          QRicCand: TADOLinkedQuery;
          Panel1: TPanel;
          BOK: TToolbarButton97;
          BAnnulla: TToolbarButton97;
          SpeedButton1: TToolbarButton97;
          QRicCandCliente: TStringField;
          QRicCandPosizione: TStringField;
          QRicCandIDCandRic: TAutoIncField;
          QRicCandIDCliente: TAutoIncField;
          QRicCandIDMansione: TIntegerField;
          QRicCandTitoloCliente: TStringField;
          QRicCandidricerca: TAutoIncField;
    GBLingua: TGroupBox;
    CBLingua: TComboBox;
          procedure FormShow(Sender: TObject);
          procedure SpeedButton1Click(Sender: TObject);
          procedure BOKClick(Sender: TObject);
          procedure BAnnullaClick(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
          xchiamante: integer;
     end;

var
     SelRicCandForm: TSelRicCandForm;

implementation

uses ModuloDati, uUtilsVarie, Main;

{$R *.DFM}

procedure TSelRicCandForm.FormShow(Sender: TObject);
begin
     //Grafica
     SelRicCandForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel124.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel124.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel124.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     Caption := '[S/4] - ' + Caption;
     if xchiamante = 1 then begin
          DBGrid1.Columns[2].Visible := true;
          DBGrid1.Columns[3].Visible := true;
     end;
end;

procedure TSelRicCandForm.SpeedButton1Click(Sender: TObject);
begin
     Aprihelp('073');
end;

procedure TSelRicCandForm.BOKClick(Sender: TObject);
begin
     SelRicCandForm.ModalResult := mrOk;
end;

procedure TSelRicCandForm.BAnnullaClick(Sender: TObject);
begin
     SelRicCandForm.ModalResult := mrCancel;
end;

end.

