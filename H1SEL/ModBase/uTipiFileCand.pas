unit uTipiFileCand;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ExtCtrls, dxDBTLCl, dxGrClms, dxTL, dxDBCtrl, dxDBGrid, Db, ADODB,
     StdCtrls, Buttons, dxCntner, TB97;

type
     TTipiFileCandForm = class(TForm)
          Panel1: TPanel;
          Panel2: TPanel;
          dxDBGrid1: TdxDBGrid;
          Panel3: TPanel;
          DSTipiFileCand: TDataSource;
          QTipiFile: TADOQuery;
          QTipiFileID: TAutoIncField;
          QTipiFileTipo: TStringField;
          QTipiFileOrdinamento: TIntegerField;
          dxDBGrid1CTipo: TdxDBGridColumn;
          dxDBGrid1CVisibWeb: TdxDBGridCheckColumn;
          BNuovo: TToolbarButton97;
          BModifica: TToolbarButton97;
          Belimina: TToolbarButton97;
          QTipiFileDefaultVisibileWeb: TBooleanField;
          dxDBGrid1Ordinamento: TdxDBGridSpinColumn;
          BOk: TToolbarButton97;
          BAnnulla: TToolbarButton97;
          ToolbarButton971: TToolbarButton97;
          procedure FormShow(Sender: TObject);
          procedure BNuovoClick(Sender: TObject);
          procedure BModificaClick(Sender: TObject);
          procedure BeliminaClick(Sender: TObject);
          procedure dxDBGrid1ChangeNode(Sender: TObject; OldNode,
               Node: TdxTreeListNode);
          procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
          procedure BOkClick(Sender: TObject);
          procedure BAnnullaClick(Sender: TObject);
          procedure ToolbarButton971Click(Sender: TObject);
     private
    { Private declarations }
     public
    { Public declarations }
     end;

var
     TipiFileCandForm: TTipiFileCandForm;

implementation

uses modulodati, Main;

{$R *.DFM}

procedure TTipiFileCandForm.FormShow(Sender: TObject);
begin
//Grafica
     TipiFileCandForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel3.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel3.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel3.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     QTipiFile.Open;
     dxDBGrid1CTipo.DisableEditor := True;
     dxDBGrid1CVisibWeb.DisableEditor := True;
     dxDBGrid1Ordinamento.DisableEditor := True;
end;

procedure TTipiFileCandForm.BNuovoClick(Sender: TObject);
begin
     QTipiFile.Insert;
     BModificaClick(self);
end;

procedure TTipiFileCandForm.BModificaClick(Sender: TObject);
begin
     QTipiFile.edit;
     BNuovo.Enabled := False;
     Belimina.Enabled := False;
     BModifica.Enabled := false;
     BOk.Enabled := True;
     BAnnulla.Enabled := True;
     dxDBGrid1CTipo.DisableEditor := False;
     dxDBGrid1CVisibWeb.DisableEditor := False;
     dxDBGrid1Ordinamento.DisableEditor := False;
end;

procedure TTipiFileCandForm.BeliminaClick(Sender: TObject);
begin
     if MessageDlg('Sei sicuro di voler eliminare la riga selezionata?',
          mtConfirmation, [mbYes, mbNo], 0) = mrYes then
          QTipiFile.Delete;

end;

procedure TTipiFileCandForm.dxDBGrid1ChangeNode(Sender: TObject; OldNode,
     Node: TdxTreeListNode);
begin

     //dxDBGrid1CVisibWeb.DisableEditor := true;
     //dxDBGrid1CTipo.DisableEditor := true;
end;

procedure TTipiFileCandForm.FormCloseQuery(Sender: TObject;
     var CanClose: Boolean);
begin
     if DSTipiFileCand.state in [dsedit, dsinsert] then begin
          if MessageDlg('Alcune modifiche non sono state salvate, salvare ora?',
               mtConfirmation, [mbYes, mbNo], 0) = mrYes then
               QTipiFile.Post else
               QTipiFile.cancel;
     end;
end;

procedure TTipiFileCandForm.BOkClick(Sender: TObject);
begin
     Data.DB.BeginTrans;
     BNuovo.Enabled := True;
     Belimina.Enabled := True;
     BModifica.Enabled := True;
     BOk.Enabled := False;
     BAnnulla.Enabled := False;
     try
          QTipiFile.Post;
          Data.DB.CommitTrans;
     except
          Data.DB.RollbackTrans;
          MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
          raise;
     end;
     dxDBGrid1CTipo.DisableEditor := True;
     dxDBGrid1CVisibWeb.DisableEditor := True;
     dxDBGrid1Ordinamento.DisableEditor := True;
end;

procedure TTipiFileCandForm.BAnnullaClick(Sender: TObject);
begin
     QTipiFile.Cancel;
     BNuovo.Enabled := True;
     Belimina.Enabled := True;
     BModifica.Enabled := True;
     BOk.Enabled := False;
     BAnnulla.Enabled := False;
     dxDBGrid1CTipo.DisableEditor := True;
     dxDBGrid1CVisibWeb.DisableEditor := True;
     dxDBGrid1Ordinamento.DisableEditor := True;
end;

procedure TTipiFileCandForm.ToolbarButton971Click(Sender: TObject);
begin
     TipiFileCandForm.ModalResult := mrCancel;
end;

end.

