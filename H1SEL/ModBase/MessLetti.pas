unit MessLetti;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Grids, DBGrids, ExtCtrls, Wall, TB97;

type
     TMessLettiForm = class(TForm)
          Wallpaper1: TPanel;
          DBGrid1: TDBGrid;
          ToolbarButton971: TToolbarButton97;
          ToolbarButton972: TToolbarButton97;
          procedure ToolbarButton971Click(Sender: TObject);
          procedure ToolbarButton972Click(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     MessLettiForm: TMessLettiForm;

implementation

uses ModuloDati;

{$R *.DFM}

procedure TMessLettiForm.ToolbarButton971Click(Sender: TObject);
begin
     close
end;

procedure TMessLettiForm.ToolbarButton972Click(Sender: TObject);
begin
     if MessageDlg('Sei sicuro di voler eliminarlo ?', mtWarning,
          [mbNo, mbYes], 0) = mrYes then Data.TPromemoria.Delete;
end;

end.
