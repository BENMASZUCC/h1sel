unit QRStorico;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, DBTables, Qrctrls, QuickRpt, ExtCtrls;

type
     TQRStoricoForm = class(TForm)
          QuickRep1: TQuickRep;
          DetailBand1: TQRBand;
          PageFooterBand1: TQRBand;
          TitleBand1: TQRBand;
          ColumnHeaderBand1: TQRBand;
          QRLabel3: TQRLabel;
          QRDBText1: TQRDBText;
          QRDBText2: TQRDBText;
          QRLabel1: TQRLabel;
          QRLabel4: TQRLabel;
          QRLabel5: TQRLabel;
          QRDBText3: TQRDBText;
          QRDBText5: TQRDBText;
          QRDBText6: TQRDBText;
          QRShape1: TQRShape;
          QRSysData1: TQRSysData;
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     QRStoricoForm: TQRStoricoForm;

implementation

uses ModuloDati2, ModuloDati;

{$R *.DFM}

end.
