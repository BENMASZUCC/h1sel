unit InSelezione;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Grids, DBGrids, Db, DBTables, ExtCtrls, StdCtrls, Buttons, DtEdit97,
     Mask, DBCtrls, ADODB, U_ADOLinkCl;

type
     TInSelezioneForm = class(TForm)
          RGopzioni: TRadioGroup;
          Panel1: TPanel;
          Panel7: TPanel;
          DBEdit12: TDBEdit;
          DBEdit14: TDBEdit;
          DsAnag: TDataSource;
          BitBtn1: TBitBtn;
          BitBtn2: TBitBtn;
    TAnag: TADOLinkedTable;
    TAnagNew: TADOQuery;
    DSTAnagNew: TDataSource;
          procedure FormShow(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     InSelezioneForm: TInSelezioneForm;

implementation

uses ModuloDati;

{$R *.DFM}

procedure TInSelezioneForm.FormShow(Sender: TObject);
begin
     Caption := '[E/1] - ' + Caption;
end;

end.
