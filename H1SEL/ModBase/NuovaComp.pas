unit NuovaComp;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, ExtCtrls, DtEdit97, Mask, DBCtrls, Db, DBTables, Spin,
     TB97;

type
     TNuovaCompForm = class(TForm)
          Panel1: TPanel;
          Label1: TLabel;
          EMot: TEdit;
          DEData: TDateEdit97;
          Label2: TLabel;
          Panel2: TPanel;
          EComp: TEdit;
          Label6: TLabel;
          Label7: TLabel;
          Label8: TLabel;
          Panel3: TPanel;
          BOK: TToolbarButton97;
          BAnnulla: TToolbarButton97;
    SEVecchioVal: TEdit;
    SENuovoVal: TEdit;
          procedure FormShow(Sender: TObject);
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
          procedure BOKClick(Sender: TObject);
          procedure BAnnullaClick(Sender: TObject);
     private
          { Private declarations }
     public
          xMot, xComp: string;
     end;

var
     NuovaCompForm: TNuovaCompForm;

implementation

uses Main;


{$R *.DFM}

procedure TNuovaCompForm.FormShow(Sender: TObject);
begin
     //Grafica
     NuovaCompForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel3.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel3.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel3.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     EMot.Text := xMot;
     EComp.Text := xComp;
end;

procedure TNuovaCompForm.FormClose(Sender: TObject;
     var Action: TCloseAction);
begin
     xMot := EMot.Text;
end;

procedure TNuovaCompForm.BOKClick(Sender: TObject);
begin
     NuovaCompForm.ModalResult := mrOK;
end;

procedure TNuovaCompForm.BAnnullaClick(Sender: TObject);
begin
     NuovaCompForm.ModalResult := mrCancel;
end;

end.

