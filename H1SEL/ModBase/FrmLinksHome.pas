unit FrmLinksHome;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ExtCtrls, dxExEdtr, dxEdLib, dxDBELib, jpeg, Buttons, DBCtrls, dxDBTLCl,
     dxGrClms, dxTL, dxDBCtrl, dxDBGrid, dxCntner, Db, ADODB,
     StdCtrls, JvDBImage, JvExExtCtrls, JvBaseThumbnail, JvThumbImage,
     JvImageRotate, JvSpecialImage, AdvPicture, ThumbnailList, ShellAPI;

type
     TFrmLinkwebHome = class(TFrame)
          DSWebLink: TDataSource;
          QWebLink: TADOQuery;
          QWebLinksID: TAutoIncField;
          QWebLinksDescrizione: TStringField;
          QWebLinksLogo: TBlobField;
          AdvPicture2: TAdvPicture;
          AdvPicture1: TAdvPicture;
          AdvPicture3: TAdvPicture;
          AdvPicture4: TAdvPicture;
          AdvPicture5: TAdvPicture;
          AdvPicture6: TAdvPicture;
          AdvPicture7: TAdvPicture;
          AdvPicture8: TAdvPicture;
          AdvPicture9: TAdvPicture;
          QWebLinkVisibileHome: TBooleanField;
          QWebLinkLink: TStringField;
          procedure dxDBGrid1Column4GetGraphicClass(Sender: TObject;
               Node: TdxTreeListNode; var GraphicClass: TGraphicClass);
          procedure QWebLinkAfterOpen(DataSet: TDataSet);
          procedure AdvPicture1Click(Sender: TObject);
          procedure AdvPicture2Click(Sender: TObject);
          procedure AdvPicture3Click(Sender: TObject);
          procedure AdvPicture4Click(Sender: TObject);
          procedure AdvPicture5Click(Sender: TObject);
          procedure AdvPicture6Click(Sender: TObject);
          procedure AdvPicture7Click(Sender: TObject);
          procedure AdvPicture8Click(Sender: TObject);
          procedure AdvPicture9Click(Sender: TObject);
     private
    { Private declarations }
     public
    { Public declarations }
     end;

implementation

{$R *.DFM}

uses modulodati;

var Link1,
     Link2,
          Link3,
          Link4,
          Link5,
          Link6,
          Link7,
          Link8,
          Link9: string;

procedure TFrmLinkwebHome.dxDBGrid1Column4GetGraphicClass(Sender: TObject;
     Node: TdxTreeListNode; var GraphicClass: TGraphicClass);
begin
     GraphicClass := TJPEGImage;
      // GraphicClass := TBitmap;
end;

procedure TFrmLinkwebHome.QWebLinkAfterOpen(DataSet: TDataSet);
var i: integer;
     xTempStream: TMemoryStream;
begin

     QWebLink.first;
     while not QWebLink.Eof do begin
          xTempStream := TMemoryStream.Create();
          QWebLinksLogo.SaveToStream(xTempStream);

          case QWebLink.RecNo of
               1: begin
                         AdvPicture1.Picture.LoadFromStream(xTempStream);
                         AdvPicture1.Visible := true;
                         Link1 := QWebLinkLink.AsString;
                    end;
               2: begin
                         AdvPicture2.Picture.LoadFromStream(xTempStream);
                         AdvPicture2.Visible := true;
                         Link2 := QWebLinkLink.AsString;
                    end;
               3: begin
                         AdvPicture3.Picture.LoadFromStream(xTempStream);
                         AdvPicture3.Visible := true;
                         Link3 := QWebLinkLink.AsString;
                    end;
               4: begin
                         AdvPicture4.Picture.LoadFromStream(xTempStream);
                         AdvPicture4.Visible := true;
                         Link4 := QWebLinkLink.AsString;
                    end;
               5: begin
                         AdvPicture5.Picture.LoadFromStream(xTempStream);
                         AdvPicture5.Visible := true;
                         Link5 := QWebLinkLink.AsString;
                    end;
               6: begin
                         AdvPicture6.Picture.LoadFromStream(xTempStream);
                         AdvPicture6.Visible := true;
                         Link6 := QWebLinkLink.AsString;
                    end;
               7: begin
                         AdvPicture7.Picture.LoadFromStream(xTempStream);
                         AdvPicture7.Visible := true;
                         Link7 := QWebLinkLink.AsString;
                    end;
               8: begin
                         AdvPicture8.Picture.LoadFromStream(xTempStream);
                         AdvPicture8.Visible := true;
                         Link8 := QWebLinkLink.AsString;
                    end;
               9: begin
                         AdvPicture9.Picture.LoadFromStream(xTempStream);
                         AdvPicture9.Visible := true;
                         Link9 := QWebLinkLink.AsString;
                    end;
          end;
          xTempStream.free;
          QWebLink.Next;
     end;

end;

procedure TFrmLinkwebHome.AdvPicture1Click(Sender: TObject);
begin
     ShellExecute(Application.Handle, 'open',
          pchar(Link1), nil, nil, SW_SHOWNORMAL)
end;

procedure TFrmLinkwebHome.AdvPicture2Click(Sender: TObject);
begin
     ShellExecute(Application.Handle, 'open',
          pchar(Link2), nil, nil, SW_SHOWNORMAL)
end;

procedure TFrmLinkwebHome.AdvPicture3Click(Sender: TObject);
begin
     ShellExecute(Application.Handle, 'open',
          pchar(Link3), nil, nil, SW_SHOWNORMAL)
end;

procedure TFrmLinkwebHome.AdvPicture4Click(Sender: TObject);
begin
     ShellExecute(Application.Handle, 'open',
          pchar(Link4), nil, nil, SW_SHOWNORMAL)
end;

procedure TFrmLinkwebHome.AdvPicture5Click(Sender: TObject);
begin
     ShellExecute(Application.Handle, 'open',
          pchar(Link5), nil, nil, SW_SHOWNORMAL)
end;

procedure TFrmLinkwebHome.AdvPicture6Click(Sender: TObject);
begin
     ShellExecute(Application.Handle, 'open',
          pchar(Link6), nil, nil, SW_SHOWNORMAL)
end;

procedure TFrmLinkwebHome.AdvPicture7Click(Sender: TObject);
begin
     ShellExecute(Application.Handle, 'open',
          pchar(Link7), nil, nil, SW_SHOWNORMAL)
end;

procedure TFrmLinkwebHome.AdvPicture8Click(Sender: TObject);
begin
     ShellExecute(Application.Handle, 'open',
          pchar(Link8), nil, nil, SW_SHOWNORMAL)
end;

procedure TFrmLinkwebHome.AdvPicture9Click(Sender: TObject);
begin
     ShellExecute(Application.Handle, 'open',
          pchar(Link9), nil, nil, SW_SHOWNORMAL)
end;

end.

