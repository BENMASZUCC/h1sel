unit TabContratti;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     TB97, dxDBTLCl, dxGrClms, dxTL, dxDBCtrl, dxDBGrid, dxCntner, Db,
     DBTables, StdCtrls, Buttons, ExtCtrls, dxLayout, dxGrClEx, ADODB,
     U_ADOLinkCl;

type
     TTabContrattiForm = class(TForm)
          Panel1: TPanel;
          QQualifContr_OLD: TQuery;
          DsQQualifContr: TDataSource;
          QTipiContrattiLK_OLD: TQuery;
          QTipiContrattiLK_OLDID: TAutoIncField;
          QTipiContrattiLK_OLDTipoContratto: TStringField;
          QQualifContr_OLDID: TAutoIncField;
          QQualifContr_OLDIDTipoContratto: TIntegerField;
          QQualifContr_OLDQualifica: TStringField;
          QQualifContr_OLDOrdine: TSmallintField;
          QQualifContr_OLDtipoContratto: TStringField;
          Panel2: TPanel;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1ID: TdxDBGridMaskColumn;
          dxDBGrid1IDTipoContratto: TdxDBGridMaskColumn;
          dxDBGrid1tipoContratto: TdxDBGridLookupColumn;
          dxDBGrid1Qualifica: TdxDBGridMaskColumn;
          dxDBGrid1Ordine: TdxDBGridMaskColumn;
          Panel3: TPanel;
          BNew: TToolbarButton97;
          BDel: TToolbarButton97;
          BCan: TToolbarButton97;
          BOK: TToolbarButton97;
          Splitter1: TSplitter;
          QTipiContratti_OLD: TQuery;
          DsQTipiContratti: TDataSource;
          QTipiContratti_OLDID: TAutoIncField;
          QTipiContratti_OLDTipoContratto: TStringField;
          QQualifContr: TADOLinkedQuery;
          QTipiContrattiLK: TADOLinkedQuery;
          QTipiContratti: TADOLinkedQuery;
          QQualifContrID: TAutoIncField;
          QQualifContrIDTipoContratto: TIntegerField;
          QQualifContrQualifica: TStringField;
          QQualifContrOrdine: TSmallintField;
          QQualifContrIDContrattoNaz: TIntegerField;
          QQualifContrPrassiAziendale: TFloatField;
          QQualifContrPrimoDecile: TFloatField;
          QQualifContrPrimoQuartile: TFloatField;
          QQualifContrMedia: TFloatField;
          QQualifContrMediana: TFloatField;
          QQualifContrTerzoQuartile: TFloatField;
          QQualifContrNonoDecile: TFloatField;
          QQualifContrTipoContratto: TStringField;
          QTipiContrattiID: TAutoIncField;
          QTipiContrattiTipoContratto: TStringField;
          QTipiContrattiIDAzienda: TIntegerField;
          QTipiContrattiIDContrattoNaz: TIntegerField;
          QTipiContrattiCaricato: TStringField;
          QTipiContrattiTipoContratto_ENG: TStringField;
          qContrattiNazLK: TADOQuery;
          qContrattiNazLKID: TAutoIncField;
          qContrattiNazLKDescrizione: TStringField;
          qContrattiNazLKCodice: TStringField;
          QQualifContrContrattoNaz: TStringField;
          dxDBGrid1ContrattoNaz: TdxDBGridLookupColumn;
          Splitter2: TSplitter;
          Panel6: TPanel;
          Panel7: TPanel;
    bContrNew: TToolbarButton97;
    bcontrDel: TToolbarButton97;
    bContrCan: TToolbarButton97;
    bContrOK: TToolbarButton97;
          Panel8: TPanel;
          Panel4: TPanel;
          dxDBGrid2: TdxDBGrid;
          dxDBGrid2ID: TdxDBGridMaskColumn;
          dxDBGrid2TipoContratto: TdxDBGridMaskColumn;
          dxDBGrid2TipoContratto_ENG: TdxDBGridColumn;
          Panel5: TPanel;
          BTipoNew: TToolbarButton97;
          BTipoDel: TToolbarButton97;
          BTipoCan: TToolbarButton97;
          BTipoOK: TToolbarButton97;
          dxDBGrid3: TdxDBGrid;
          qContrattiNaz: TADOQuery;
          dsContrattiNaz: TDataSource;
          qContrattiNazID: TAutoIncField;
          qContrattiNazDescrizione: TStringField;
          qContrattiNazCodice: TStringField;
          dxDBGrid3ID: TdxDBGridMaskColumn;
          dxDBGrid3Descrizione: TdxDBGridMaskColumn;
          dxDBGrid3Codice: TdxDBGridMaskColumn;
    BitBtn1: TToolbarButton97;
          procedure DsQQualifContrStateChange(Sender: TObject);
          procedure QQualifContr_OLDAfterPost(DataSet: TDataSet);
          procedure BNewClick(Sender: TObject);
          procedure BDelClick(Sender: TObject);
          procedure BOKClick(Sender: TObject);
          procedure BCanClick(Sender: TObject);
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
          procedure QQualifContr_OLDBeforePost(DataSet: TDataSet);
          procedure DsQTipiContrattiStateChange(Sender: TObject);
          procedure BTipoNewClick(Sender: TObject);
          procedure BTipoDelClick(Sender: TObject);
          procedure BTipoOKClick(Sender: TObject);
          procedure BTipoCanClick(Sender: TObject);
          procedure QTipiContratti_OLDAfterPost(DataSet: TDataSet);
          procedure BitBtn1Click(Sender: TObject);
          procedure bContrNewClick(Sender: TObject);
          procedure bcontrDelClick(Sender: TObject);
          procedure bContrOKClick(Sender: TObject);
          procedure bContrCanClick(Sender: TObject);
          procedure dsContrattiNazStateChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
     private
          xID: integer;
     public
          { Public declarations }
     end;

var
     TabContrattiForm: TTabContrattiForm;

implementation

uses ModuloDati, uUtilsVarie, Main;

{$R *.DFM}

procedure TTabContrattiForm.DsQQualifContrStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsQQualifContr.State in [dsEdit, dsInsert];
     BNew.Enabled := not b;
     BDel.Enabled := not b;
     BOK.Enabled := b;
     BCan.Enabled := b;
end;

procedure TTabContrattiForm.QQualifContr_OLDAfterPost(DataSet: TDataSet);
begin
     QQualifContr.Close;
     QQualifContr.Open;
     QQualifContr.Locate('ID', xID, []);
end;

procedure TTabContrattiForm.BNewClick(Sender: TObject);
begin
     QQualifContr.Insert;
end;

procedure TTabContrattiForm.BDelClick(Sender: TObject);
begin
     if QQualifContr.IsEmpty then exit;
     if MessageDlg('Sei sicuro di voler cancellare ?', mtWarning, [mbYes, mbNo], 0) = mrNo then exit;
     QQualifContr.Delete;
end;

procedure TTabContrattiForm.BOKClick(Sender: TObject);
begin
     QQualifContr.Post;
end;

procedure TTabContrattiForm.BCanClick(Sender: TObject);
begin
     QQualifContr.cancel;
end;

procedure TTabContrattiForm.FormClose(Sender: TObject;
     var Action: TCloseAction);
begin
     if DsQQualifContr.state in [dsinsert, dsedit] then QQualifContr.Post;
end;

procedure TTabContrattiForm.QQualifContr_OLDBeforePost(DataSet: TDataSet);

//[ALBERTO 20020912]

begin
     xID := QQualifContr.FieldByName('ID').AsInteger;
end;

//[ALBERTO 20020912]FINE

procedure TTabContrattiForm.DsQTipiContrattiStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsQTipiContratti.State in [dsEdit, dsInsert];
     BTipoNew.Enabled := not b;
     BTipoDel.Enabled := not b;
     BTipoOK.Enabled := b;
     BTipoCan.Enabled := b;
end;

procedure TTabContrattiForm.BTipoNewClick(Sender: TObject);
begin
     QTipiContratti.Insert;
end;

procedure TTabContrattiForm.BTipoDelClick(Sender: TObject);
begin
     if QTipiContratti.IsEmpty then exit;
     if MessageDlg('Sei sicuro di voler cancellare ?', mtWarning, [mbYes, mbNo], 0) = mrNo then exit;
     if VerifIntegrRef('QualificheContrattuali', 'IDTipoContratto', QTipiContratti.FieldByName('ID').AsString) then
          QTipiContratti.Delete;
end;

procedure TTabContrattiForm.BTipoOKClick(Sender: TObject);
begin
     QTipiContratti.Post;
end;

procedure TTabContrattiForm.BTipoCanClick(Sender: TObject);
begin
     QTipiContratti.Cancel;
end;

procedure TTabContrattiForm.QTipiContratti_OLDAfterPost(DataSet: TDataSet);
begin
     QTipiContratti.Close;
     QTipiContratti.Open;
     QTipiContrattiLK.Close;
     QTipiContrattiLK.Open;
end;

procedure TTabContrattiForm.BitBtn1Click(Sender: TObject);
begin
     if DsQQualifContr.state in [dsInsert, dsEdit] then QQualifContr.Post;
     if DsQTipiContratti.state in [dsInsert, dsEdit] then QTipiContratti.Post;
     TabContrattiForm.ModalResult:=mrOk
end;

procedure TTabContrattiForm.bContrNewClick(Sender: TObject);
begin
     qContrattiNaz.Insert;
end;

procedure TTabContrattiForm.bcontrDelClick(Sender: TObject);
begin
     if qContrattiNaz.IsEmpty then exit;
     if MessageDlg('Sei sicuro di voler cancellare ?', mtWarning, [mbYes, mbNo], 0) = mrNo then exit;
     if VerifIntegrRef('QualificheContrattuali', 'IDContrattoNaz', QContrattinaz.FieldByName('ID').AsString) then
          qContrattiNaz.Delete;
end;

procedure TTabContrattiForm.bContrOKClick(Sender: TObject);
begin
     QContrattiNaz.Post;
end;

procedure TTabContrattiForm.bContrCanClick(Sender: TObject);
begin
     QContrattiNaz.Cancel;
end;

procedure TTabContrattiForm.dsContrattiNazStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsContrattiNaz.State in [dsEdit, dsInsert];
     bContrNew.Enabled := not b;
     bContrDel.Enabled := not b;
     bContrOK.Enabled := b;
     bContrcan.Enabled := b;
end;

procedure TTabContrattiForm.FormShow(Sender: TObject);
begin
     //Grafica
     TabContrattiForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel3.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel3.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel3.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel4.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel4.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel4.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel5.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel5.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel5.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel6.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel6.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel6.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel7.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel7.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel7.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel8.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel8.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel8.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
end;

end.

