unit TabVociValutazCand;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, ExtCtrls, dxTL, dxDBCtrl, dxDBGrid, dxCntner, Db,
     ADODB, TB97;

type
     TTabVociValutazCandForm = class(TForm)
          Panel1: TPanel;
          QTabVoci: TADOQuery;
          DsQTabVoci: TDataSource;
          QTabVociID: TAutoIncField;
          QTabVociIDRicerca: TIntegerField;
          QTabVociVoce: TStringField;
          QTabVociLegenda: TStringField;
          dxDBGrid7: TdxDBGrid;
          dxDBGrid7ID: TdxDBGridMaskColumn;
          dxDBGrid7IDRicerca: TdxDBGridMaskColumn;
          dxDBGrid7Voce: TdxDBGridMaskColumn;
          dxDBGrid7Legenda: TdxDBGridMaskColumn;
          BNew: TToolbarButton97;
          BDel: TToolbarButton97;
          BCan: TToolbarButton97;
          BOK: TToolbarButton97;
          BitBtn1: TToolbarButton97;
          procedure QTabVociBeforeOpen(DataSet: TDataSet);
          procedure QTabVociAfterInsert(DataSet: TDataSet);
          procedure DsQTabVociStateChange(Sender: TObject);
          procedure BNewClick(Sender: TObject);
          procedure BDelClick(Sender: TObject);
          procedure BOKClick(Sender: TObject);
          procedure BCanClick(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
          procedure BitBtn1Click(Sender: TObject);
     private
    { Private declarations }
     public
          xIDRicerca: integer;
          xNewVoce: boolean;
     end;

var
     TabVociValutazCandForm: TTabVociValutazCandForm;

implementation

uses ModuloDati, Main;

{$R *.DFM}

procedure TTabVociValutazCandForm.QTabVociBeforeOpen(DataSet: TDataSet);
begin
     QTabVoci.Parameters[0].Value := xIDRicerca;
end;

procedure TTabVociValutazCandForm.QTabVociAfterInsert(DataSet: TDataSet);
begin
     QTabVociIDRicerca.value := xIDRicerca;
     xNewVoce := true;
end;

procedure TTabVociValutazCandForm.DsQTabVociStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsQTabVoci.State in [dsEdit, dsInsert];
     BNew.Enabled := not b;
     BDel.Enabled := not b;
     BOK.Enabled := b;
     BCan.Enabled := b;
end;

procedure TTabVociValutazCandForm.BNewClick(Sender: TObject);
begin
     QTabVoci.Insert;
end;

procedure TTabVociValutazCandForm.BDelClick(Sender: TObject);
begin
     if MessageDlg('Sei sicuro di voler eliminare la voce selezionata ?', mtWarning, [mbNo, mbYes], 0) <> mrYes then exit;
     // controllo integritÓ referenziale
     Data.Q1.Close;
     Data.Q1.SQL.clear;
     Data.Q1.SQL.text := 'select count(*) Tot from EBC_CandRic_Valutaz where IDVoce=' + QTabVociID.asString;
     Data.Q1.Open;
     if Data.Q1.FieldByName('Tot').asInteger > 0 then begin
          MessageDlg('Ci sono valutazioni con questa voce - IMPOSSIBILE CANCELLARE', mtError, [mbOK], 0);
          exit;
     end;
     QTabVoci.Delete;
end;

procedure TTabVociValutazCandForm.BOKClick(Sender: TObject);
begin
     QTabVoci.Post;
end;

procedure TTabVociValutazCandForm.BCanClick(Sender: TObject);
begin
     QTabVoci.Cancel;
end;

procedure TTabVociValutazCandForm.FormShow(Sender: TObject);
begin
     //Grafica
     TabVociValutazCandForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     QTabVoci.Open;
     xNewVoce := False;
end;

procedure TTabVociValutazCandForm.FormClose(Sender: TObject;
     var Action: TCloseAction);
begin
     if DsQTabVoci.state in [dsInsert, dsEdit] then QTabVoci.Post;
end;

procedure TTabVociValutazCandForm.BitBtn1Click(Sender: TObject);
begin
     TabVociValutazCandForm.ModalResult := mrOK;
end;

end.

