unit DettaglioTesi;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, DBCtrls, Buttons, TB97, ExtCtrls;

type
     TDettaglioTesiForm = class(TForm)
          Label13: TLabel;
          DBMemo1: TDBMemo;
          Panel2: TPanel;
          BitBtn1: TToolbarButton97;
          BAnnulla: TToolbarButton97;
          procedure BitBtn1Click(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure BAnnullaClick(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     DettaglioTesiForm: TDettaglioTesiForm;

implementation

uses ModuloDati, Main;

{$R *.DFM}


procedure TDettaglioTesiForm.BitBtn1Click(Sender: TObject);
begin
     with Data.TTitoliStudio do begin
          Data.DB.BeginTrans;
          try
               Post;
               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               raise;
          end;
     end;
     DettaglioTesiForm.ModalResult := mrOK;
end;

procedure TDettaglioTesiForm.FormShow(Sender: TObject);
begin
     //Grafica
     DettaglioTesiForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     Caption := '[M/05112] - ' + Caption;
end;

procedure TDettaglioTesiForm.BAnnullaClick(Sender: TObject);
begin
     DettaglioTesiForm.ModalResult := mrCancel;
end;

end.

