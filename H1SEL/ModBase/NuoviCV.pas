//[TONI20021002]DEBUGOK
unit NuoviCV;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, Grids, DBGrids, DBTables, ExtCtrls, Buttons, ComCtrls, StdCtrls,
     DtEdit97, ImageWindow, ImgList, Menus, OleCtrls, Spin, ADODB,
     U_ADOLinkCl, TB97;

type
     TNuoviCVForm = class(TForm)
          DsQRicAttiveSel: TDataSource;
          dsSelez: TDataSource;
          DBGrid1: TDBGrid;
          ImageList1: TImageList;
          PopupMenu1: TPopupMenu;
          Eliminaassociazione1: TMenuItem;
          PopupMenu2: TPopupMenu;
          Infocandidato1: TMenuItem;
          curriculum1: TMenuItem;
          documenti1: TMenuItem;
          fileWord1: TMenuItem;
          Panel5: TPanel;
          Panel1: TPanel;
          Panel8: TPanel;
          LSel: TLabel;
          TVRic: TTreeView;
          Panel2: TPanel;
          Panel3: TPanel;
          TV: TTreeView;
          Panel4: TPanel;
          Label1: TLabel;
          Label2: TLabel;
          DEDataDal: TDateEdit97;
          DEDataAl: TDateEdit97;
          Splitter1: TSplitter;
          TSelez: TADOLinkedTable;
          TRicABS: TADOLinkedTable;
          QRicAttiveSel: TADOLinkedQuery;
          QAnagMans_old: TADOLinkedQuery;
          QPerRuolo: TADOLinkedQuery;
          QPerArea: TADOLinkedQuery;
          QAnagArea_old: TADOLinkedQuery;
          Q: TADOLinkedQuery;
          TSelezID: TAutoIncField;
          TSelezNominativo: TStringField;
          RGOpzioni: TRadioGroup;
          QRes1: TADOLinkedQuery;
          TLinee: TADOLinkedQuery;
          QTabelleLista: TADOLinkedQuery;
          QTabelleListaTabella: TStringField;
          TLineeID: TAutoIncField;
          TLineeIDRicerca: TIntegerField;
          TLineeDescrizione: TStringField;
          TLineeStringa: TStringField;
          TLineeTabella: TStringField;
          TLineeOpSucc: TStringField;
          TLineeNext: TStringField;
          QAnagArea: TADOQuery;
          QAnagMans: TADOQuery;
          Panel6: TPanel;
          BitBtn1: TToolbarButton97;
          Panel7: TPanel;
          SpeedButton2: TToolbarButton97;
          SpeedButton3: TToolbarButton97;
          SpeedButton1: TToolbarButton97;
          procedure SpeedButton1Click(Sender: TObject);
          procedure SpeedButton2Click(Sender: TObject);
          procedure TVRicDragOver(Sender, Source: TObject; X, Y: Integer;
               State: TDragState; var Accept: Boolean);
          procedure TVRicDragDrop(Sender, Source: TObject; X, Y: Integer);
          procedure BitBtn1Click(Sender: TObject);
          procedure TVRicClick(Sender: TObject);
          procedure Eliminaassociazione1Click(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure Infocandidato1Click(Sender: TObject);
          procedure curriculum1Click(Sender: TObject);
          procedure documenti1Click(Sender: TObject);
          procedure fileWord1Click(Sender: TObject);
          procedure FormCreate(Sender: TObject);
          procedure FormDestroy(Sender: TObject);
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
          procedure TSelezAfterScroll(DataSet: TDataSet);
          procedure SpeedButton3Click(Sender: TObject);
     private
          function ComponiQuery(xIDRic: integer): boolean;
          function ControlloBlocchi: boolean;
     public
          { Public declarations }
     end;

var
     NuoviCVForm: TNuoviCVForm;

implementation

uses ModuloDati, Main, Splash3, SchedaCand, Curriculum, View, uUtilsVarie,
     CheckPass;

{$R *.DFM}

//[/TONI20020911\]
//[TONI20021002]DEBUGOK

procedure TNuoviCVForm.SpeedButton1Click(Sender: TObject);
var N, N1: TTreeNode;
     x, x1: string;
begin
     case RGOpzioni.ItemIndex of
          0: begin
                    TV.Items.BeginUpdate;
                    TV.Items.Clear;
                    QPerArea.Close;
                    QPerArea.Open;

                    while not QPerArea.EOF do begin
                         QAnagArea.Close;
                         // QAnagArea.ReloadSQL;

                         {AnagArea.ParamByName['xIDArea'] := QPerArea.FieldByName('ID').asInteger;
                         QAnagArea.ParamByName['xDal'] := DEDataDal.Date;
                         QAnagArea.ParamByName['xAl'] := DEDataAl.Date;      }
                         QanagArea.Parameters.ParamByName('xIDArea').Value := QPerArea.FieldByName('ID').asInteger;
                         QanagArea.Parameters.ParamByName('xDal').Value := DEDataDal.Date;
                         QanagArea.Parameters.ParamByName('xAl').Value := DEDataAl.Date;
                         QAnagArea.Open;
                         if not QAnagArea.IsEmpty then begin
                              N := TV.Items.AddChildObject(nil, QPerArea.FieldByName('Area').asString, pointer(QPerArea.FieldByName('ID').asInteger));
                              N.ImageIndex := 3; N.SelectedIndex := 3; N.StateIndex := 3;
                              while not QAnagArea.EOF do begin
                                   N1 := TV.Items.AddChildObject(N, QAnagArea.FieldByName('Cognome').asString + ' ' + QAnagArea.FieldByName('Nome').asString, pointer(QAnagArea.FieldByName('ID').asInteger));
                                   N1.ImageIndex := 2; N1.SelectedIndex := 2; N1.StateIndex := 2;
                                   QAnagArea.Next;
                              end;
                         end;
                         QPerArea.Next;
                    end;
                    QPerArea.Close;
                    TV.Items.EndUpdate;
               end;
          1: begin
                    TV.Items.BeginUpdate;
                    TV.Items.Clear;
                    QPerRuolo.Close;
                    QPerRuolo.Open;
                    x := QPerRuolo.SQL.Text;
                    while not QPerRuolo.EOF do begin
                         QAnagMans.Close;
                         //QAnagMans.REloadSQL;
                         {               QAnagMans.ParamByName('xIDMans').asInteger:=QPerRuolo.FieldByName('ID').asInteger;
                                        QAnagMans.ParamByName('xDal').asDateTime:=DEDataDal.Date;
                                        QAnagMans.ParamByName('xAl').asDateTime:=DEDataAl.Date;}
                         {QAnagMans.ParamByName['xIDMans'] := QPerRuolo.FieldByName('ID').asInteger;
                         QAnagMans.ParamByName['xDal'] := DEDataDal.Date;
                         QAnagMans.ParamByName['xAl'] := DEDataAl.Date;  }
                         QAnagMans.Parameters.ParamByName('xIDMans').Value := QPerRuolo.FieldByName('ID').asInteger;
                         QAnagMans.Parameters.ParamByName('xDal').Value := DEDataDal.Date;
                         QAnagMans.Parameters.ParamByName('xAl').Value := DEDataAl.Date;
                         QAnagMans.Open;
                         x1 := QAnagMans.SQL.Text;
                         if not QAnagMans.IsEmpty then begin
                              N := TV.Items.AddChildObject(nil, QPerRuolo.FieldByName('Ruolo').asString, pointer(QPerRuolo.FieldByName('ID').asInteger));
                              N.ImageIndex := 3; N.SelectedIndex := 3; N.StateIndex := 3;
                              while not QAnagMans.EOF do begin
                                   N1 := TV.Items.AddChildObject(N, QAnagMans.FieldByName('Cognome').asString + ' ' + QAnagMans.FieldByName('Nome').asString, pointer(QAnagMans.FieldByName('ID').asInteger));
                                   N1.ImageIndex := 2; N1.SelectedIndex := 2; N1.StateIndex := 2;
                                   QAnagMans.Next;
                              end;
                         end;
                         QPerRuolo.Next;

                    end;
                    QPerRuolo.Close;
                    TV.Items.EndUpdate;
               end;
          2: begin
                    TV.Items.BeginUpdate;
                    TV.Items.Clear;
                    if not QRicAttiveSel.Active then QRicAttiveSel.Open
                    else QRicAttiveSel.First;
                    // ShowMessage('Numero record = ' + inttostr(QRicAttiveSel.recordcount));
                    while not QRicAttiveSel.EOF do begin
                         //ShowMessage('IDRicerca = ' + QRicAttiveSel.FieldByName('ID').asstring);
                         if ComponiQuery(QRicAttiveSel.FieldByName('ID').asInteger) then begin
                              N := TV.Items.AddChildObject(nil, QRicAttiveSel.FieldByName('Rif').asString + ' ' + QRicAttiveSel.FieldByName('Cliente').asString, pointer(QRicAttiveSel.FieldByName('ID').asInteger));
                              N.ImageIndex := 3; N.SelectedIndex := 3; N.StateIndex := 3;
                              while not QRes1.EOF do begin
                                   N1 := TV.Items.AddChildObject(N, QRes1.FieldByName('Cognome').asString + ' ' + QRes1.FieldByName('Nome').asString, pointer(QRes1.FieldByName('ID').asInteger));
                                   N1.ImageIndex := 2; N1.SelectedIndex := 2; N1.StateIndex := 2;
                                   QRes1.Next;
                              end;
                              TV.Items.EndUpdate;
                         end;
                         QRicAttiveSel.Next;
                    end;
               end;
     end;
end;
//[/TONI20020911\]FINE

//[TONI20021002]DEBUGOK

procedure TNuoviCVForm.SpeedButton2Click(Sender: TObject);
var N, N1: TTreeNode;
begin
     TVRic.Items.BeginUpdate;
     TVRic.Items.Clear;
     if not QRicAttiveSel.Active then begin
          QRicAttiveSel.SQL.text := 'select EBC_Ricerche.ID,Progressivo Rif,EBC_Clienti.Descrizione Cliente,Mansioni.Descrizione Ruolo, ' +
               '          Numricercati, Aree.Descrizione Area ' +
               'from EBC_Ricerche,EBC_Clienti,Mansioni,Aree, EBC_StatiRic ' +
               'where EBC_Ricerche.IDCliente=EBC_Clienti.ID ' +
               '    and EBC_Ricerche.IDMansione=Mansioni.ID ' +
               '    and EBC_Ricerche.IDArea=Aree.ID ' +
               'and EBC_Ricerche.IDUtente=' + TSelezID.asString + ' ' +
               'and EBC_Ricerche.IDstatoRic=EBC_StatiRic.ID ' +
               'and IDTipoStatoRic=1 ' +
               'order by Ruolo ';
          QRicAttiveSel.Open;
     end;

     while not QRicAttiveSel.EOF do begin
          N := TVRic.Items.AddChildObject(nil, QRicAttiveSel.FieldByName('Rif').asString + '-' + QRicAttiveSel.FieldByName('Cliente').asString + ' (' +
               QRicAttiveSel.FieldByName('Ruolo').asString + ' - ' + QRicAttiveSel.FieldByName('Area').asString + ' )',
               pointer(QRicAttiveSel.FieldByName('ID').asInteger));
          N.ImageIndex := 5; N.SelectedIndex := 5; N.StateIndex := 5;
          QRicAttiveSel.Next;
     end;
     QRicAttiveSel.Close;
     TVRic.Items.EndUpdate;
     if TSelez.active = false then TSelez.Open;
     Lsel.caption := TSelez.FieldByName('Nominativo').AsString;
end;

procedure TNuoviCVForm.TVRicDragOver(Sender, Source: TObject; X,
     Y: Integer; State: TDragState; var Accept: Boolean);
begin
     Accept := Sender <> Source;
end;

//[TONI20021002]DEBUGOK

procedure TNuoviCVForm.TVRicDragDrop(Sender, Source: TObject; X,
     Y: Integer);
var T, N: TTreeNode;
     i: integer;
     xEsisteGia: boolean;
begin
     if (TV.Selected.Level = 1) and (TVRic.DropTarget.Level = 0) then begin
          // controllo ruolo
          if RGOpzioni.ItemIndex = 1 then begin
               TRicABS.Open;
               //TRicABS.FindKeyADO(VarArrayOf([longint(TVRic.DropTarget.Data)]));
               TRicABS.Locate('ID', longint(TVRic.DropTarget.Data), []);
               if longint(TV.Selected.Parent.Data) <> TRicABS.FieldByName('IDMansione').AsInteger then
                    if MessageDlg('Attenzione: i ruoli non coincidono - proseguire ?', mtwarning, [mbYes, mbNo], 0) = mrNo then
                         Exit;
               TRicABS.Close;
          end;
          // controllo esistenza
          xEsisteGia := false;
          if TVRic.DropTarget.HasChildren then begin
               N := TVRic.DropTarget.getFirstChild;
               for i := 1 to TVRic.DropTarget.Count do begin
                    if longint(N.Data) = longint(TV.Selected.Data) then begin
                         xEsisteGia := True;
                         Break;
                    end;
                    N := N.GetNext;
               end;
          end;
          if (not xEsisteGia) and ControlloBlocchi then begin
               N := TVRic.Items.AddChildObject(TVRic.DropTarget, TV.Selected.text, pointer(longint(TV.Selected.Data)));
               N.Expand(false);
               N.ImageIndex := 2; N.SelectedIndex := 2; N.StateIndex := 2;
          end;
     end;
end;

//[TONI20021002]DEBUGOK

procedure TNuoviCVForm.BitBtn1Click(Sender: TObject);
var Root, N, N1: TTreeNode;
     i, k, xIDRicerca, xIDCliente, xIDUtente: integer;
     xProg, xCliente: string;
begin
     TRicABS.open;
     N := TVRic.TopItem;
     for i := 1 to TVRic.Items.count do begin
          if N.level = 0 then
               xIDRicerca := longint(N.Data)
          else begin
               // se � un candidato >> prova ad inserirlo
               if Q.Active then Q.Close;
               Q.SQL.text := 'select ID from EBC_CandidatiRicerche where IDRicerca=' + IntToStr(xIDRicerca) + ' and IDAnagrafica=' + IntToStr(longint(N.Data));
               Q.Open;
               if Q.RecordCount = 0 then begin
                    Q.Close;
                    Data.DB.BeginTrans;
                    try
                         Q.SQL.text := 'insert into EBC_CandidatiRicerche (IDRicerca,IDAnagrafica,Escluso,Stato,DataIns) ' +
                              'values (:xIDRicerca:,:xIDAnagrafica:,:xEscluso:,:xStato:,:xDataIns:)';
                         Q.ParamByName['xIDRicerca'] := xIDRicerca;
                         Q.ParamByName['xIDAnagrafica'] := longint(N.Data);
                         Q.ParamByName['xEscluso'] := 0;
                         Q.ParamByName['xStato'] := 'inserito da Nuovi CV';
                         Q.ParamByName['xDataIns'] := Date;
                         Q.ExecSQL;
                         // aggiornamento stato anagrafica
                         if Q.Active then Q.Close;
                         if pos('EMERGENCY', UpperCase(data.Global.fieldbyname('NomeAzienda').asString)) > 0 then
                              Q.SQL.text := 'update Anagrafica set IDStato=28,IDTipoStato=2 where ID=' + IntToStr(longint(N.Data))
                         else Q.SQL.text := 'update Anagrafica set IDStato=27,IDTipoStato=1 where ID=' + IntToStr(longint(N.Data));
                         Q.ExecSQL;
                         // aggiornamento storico
                         if Q.Active then Q.Close;
                         Q.SQL.text := 'select EBC_Ricerche.Progressivo,EBC_Clienti.Descrizione Cliente,EBC_Ricerche.IDCliente,EBC_Ricerche.IDUtente ' +
                              'from EBC_Ricerche,EBC_Clienti where EBC_Ricerche.IDCliente=EBC_Clienti.ID and EBC_Ricerche.ID=' + IntToStr(xIDRicerca);
                         Q.Open;
                         xProg := Q.FieldByName('Progressivo').asString;
                         xCliente := Q.FieldByName('Cliente').asString;
                         xIDUtente := Q.FieldByName('IDUtente').asInteger;
                         xIDCliente := Q.FieldByName('IDCliente').asInteger;
                         Q.Close;
                         Q.SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
                              'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xIDCliente:)';
                         Q.ParamByName['xIDAnagrafica'] := longint(N.Data);
                         Q.ParamByName['xIDEvento'] := 19;
                         Q.ParamByName['xDataEvento'] := date;
                         Q.ParamByName['xAnnotazioni'] := 'ric.n�' + xProg + ' (' + xCliente + ')';
                         Q.ParamByName['xIDRicerca'] := xIDRicerca;
                         Q.ParamByName['xIDUtente'] := xIDUtente;
                         Q.ParamByName['xIDCliente'] := xIDCliente;
                         Q.ExecSQL;
                         Data.DB.CommitTrans;
                    except
                         Data.DB.RollbackTrans;
                         MessageDlg('ERRORE SUL DATABASE:  operazione non completata', mtError, [mbOK], 0);
                    end;
                    N.ImageIndex := 6; N.SelectedIndex := 6; N.StateIndex := 6;
               end else begin N.ImageIndex := 7; N.SelectedIndex := 7; N.StateIndex := 7; end;
          end;
          N := N.GetNext;
     end;
     ShowMessage('Operazione completata:' + chr(13) +
          'i candidati con l''icona VERDE sono stati associati alla ricerca,' + chr(13) +
          'quelli con l''icona ROSSA erano gi� stati associati alla medesima ricerca');
     TRicABS.Close;
end;

procedure TNuoviCVForm.TVRicClick(Sender: TObject);
begin
     if TVRic.Selected.Level <> 1 then Eliminaassociazione1.Enabled := False
     else Eliminaassociazione1.Enabled := True;
end;

procedure TNuoviCVForm.Eliminaassociazione1Click(Sender: TObject);
begin
     TVRic.Selected.Delete;
end;

//[TONI20021002]DEBUGOK

procedure TNuoviCVForm.FormShow(Sender: TObject);
begin
     //Grafica
     NuoviCVForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel3.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel3.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel3.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel4.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel4.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel4.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel5.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel5.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel5.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel6.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel6.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel6.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel7.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel7.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel7.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel8.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel8.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel8.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     Caption := '[M/25] - ' + caption;
     Splash3Form := TSplash3Form.Create(nil);
     Splash3Form.Show;
     Splash3Form.Update;
     //     TSelez.FindKey([mainForm.xIDUtenteAttuale]);
     if Tselez.state = dsInactive then TSelez.Open;
     TSelez.Locate('ID', mainForm.xIDUtenteAttuale, []);
     DEDataDal.date := Date - 3;
     DEDataAl.date := Date;
     SpeedButton2Click(self);
     //SpeedButton1Click(self);
     Splash3Form.Hide;
     Splash3Form.Free;

     //Gestione H1Sel Aziende
     if data.xSelAziende then begin
          Panel8.Caption := '  Progetti attivi per il selezionatore';
          SpeedButton2.Caption := 'Aggiorna Progetti';
          RGOpzioni.Items.Text := 'per area' + chr(13) +
               'per ruolo' + chr(13) +
               'per query progetto';
          NuoviCVForm.Caption := '[M/25] - Gestione nuovi CV rispetto al progetto attivo'
     end;
end;

//[TONI20021002]DEBUGOK

procedure TNuoviCVForm.Infocandidato1Click(Sender: TObject);
begin
     if TV.Selected.level = 0 then exit;
     SchedaCandForm := TSchedaCandForm.create(self);
     if not PosizionaAnag(longint(TV.Selected.Data)) then exit;
     SchedaCandForm.ShowModal;
     SchedaCandForm.Free;
end;

//[TONI20021002]DEBUGOK

procedure TNuoviCVForm.curriculum1Click(Sender: TObject);
var x: string;
     i: integer;
begin
     if TV.Selected.level = 0 then exit;
     PosizionaAnag(longint(TV.Selected.Data));

     CurriculumForm.ShowModal;

     MainForm.Pagecontrol5.ActivePage := MainForm.TSStatoTutti;
end;

//[TONI20021002]DEBUGOK

procedure TNuoviCVForm.documenti1Click(Sender: TObject);
var xFile: string;
     xProcedi: boolean;
     xCVNumero: string;
begin
     if TV.Selected.level = 0 then exit;
     ApriCV(longint(TV.Selected.Data));
end;

//[TONI20021002]DEBUGOK

procedure TNuoviCVForm.fileWord1Click(Sender: TObject);
begin
     if not PosizionaAnag(longint(TV.Selected.Data)) then exit;
     ApriFileWord(Data.TAnagrafica.FieldByName('CVNumero').AsString);
end;

//[TONI20021002]DEBUGOK

procedure TNuoviCVForm.FormCreate(Sender: TObject);
begin
     MainForm.xNuoviCVFormCreated := true;
end;

//[TONI20021002]DEBUGOK

procedure TNuoviCVForm.FormDestroy(Sender: TObject);
begin
     MainForm.xNuoviCVFormCreated := False;
end;

//[TONI20021002]DEBUGOK

procedure TNuoviCVForm.FormClose(Sender: TObject;
     var Action: TCloseAction);
begin
     NuoviCVForm.Free;
end;

function TNuoviCVForm.ComponiQuery(xIDRic: integer): boolean;
var xTab: array[1..5] of string;
     i, j, k: integer;
     xS, xS1, xd, xLineeStringa: string;
     xApertaPar, xEspLav: boolean;
begin
     // tabelle implicate tranne Anagrafica
     QTabelleLista.Close;
     QTabelleLista.SQL.text := '';
     QTabelleLista.SQL.add('select distinct Tabella from RicLineeQuery ');
     QTabelleLista.SQL.add('where Tabella<>''Anagrafica'' and IDRicerca=' + IntToStr(xIDRic));
     QTabelleLista.Open;
     if QTabelleLista.IsEmpty then begin
          result := False;
          exit;
     end;
     QTabelleLista.First;
     for i := 1 to 5 do xTab[i] := '';
     i := 1;
     while not QTabelleLista.EOF do begin
          xTab[i] := QTabelleListaTabella.AsString;
          QTabelleLista.Next; inc(i);
     end;

     xEspLav := False;
     xS := 'from Anagrafica,AnagAltreInfo';
     k := 1;
     while k <= 5 do begin
          if (xTab[k] <> '') and (UpperCase(xTab[k]) <> 'ANAGALTREINFO') then
               xS := xs + ',' + xTab[k];
          if xTab[k] = 'AnagCampiPers' then
               xS := xS + ',CampiPers';
          inc(k);
     end;

     // linee query
     if TLinee.Active then TLinee.Close;
     TLinee.SQL.text := 'select * from RicLineeQuery where IDRicerca=' + IntToStr(xIDRic);
     TLinee.Open;

     QRes1.Close;
     QRes1.SQL.text := '';

     QRes1.SQL.Add('select distinct Anagrafica.ID,Anagrafica.Cognome,Anagrafica.Nome');
     QRes1.SQL.Add(xS);
     QRes1.SQL.Add('where Anagrafica.ID is not null and Anagrafica.ID=AnagAltreInfo.IDAnagrafica');


     xS1 := '';
     for i := 1 to 5 do begin
          //if (xTab[i]<>'')and(xTab[i]<>'EsperienzeLavorative') then
          if xTab[i] <> '' then
               if xTab[i] = '' then
                    xS1 := xS1 + ' and Anagrafica.ID=' + xTab[i] + '.IDAnagrafica and CampiPers.ID = AnagCampiPers.IDCampo'
               else xS1 := xS1 + ' and Anagrafica.ID=' + xTab[i] + '.IDAnagrafica';
     end;
     QRes1.SQL.Add(xS1);
     if TLinee.RecordCount > 0 then
          QRes1.SQL.Add('and');

     TLinee.First;
     xLineeStringa := '';
     xApertaPar := False;
     while not TLinee.EOF do begin
          xLineeStringa := TLineeStringa.asString;
          // per le LINGUE e per i settori --> apposita clausola
          if copy(TLineeDescrizione.Value, 1, 6) = 'lingua' then
               // ## OLD ## xLineeStringa:='Anagrafica.ID in (select IDAnagrafica from LingueConosciute where lingua='''+copy(TLineeDescrizione.Value,9,Length(TLineeDescrizione.Value))+''')';
               xLineeStringa := 'Anagrafica.ID in (select IDAnagrafica from LingueConosciute where ' + TLineeStringa.Value + ')';
          TLinee.Next;
          if TLinee.Eof then
               // ultimo record -> senza AND alla fine
               if xApertaPar then QRes1.SQL.Add(' ' + xLineeStringa + ')')
               else QRes1.SQL.Add(' ' + xLineeStringa)
          else begin
               TLinee.Prior;
               if TLineeOpSucc.AsString = 'and' then
                    if not xApertaPar then
                         QRes1.SQL.Add(' ' + xLineeStringa + ' and ')
                    else begin
                         QRes1.SQL.Add(' ' + xLineeStringa + ') and ');
                         xApertaPar := False;
                    end;
               if TLineeOpSucc.AsString = 'or' then
                    if not xApertaPar then begin
                         QRes1.SQL.Add(' (' + xLineeStringa + ' or ');
                         xApertaPar := True;
                    end else begin
                         QRes1.SQL.Add(' ' + xLineeStringa + ' or ');
                         xApertaPar := True;
                    end;
          end;
          TLinee.Next;
     end;

     QRes1.SQL.Add('and CVInseritoInData>=:xDal: and CVInseritoInData<=:xAl:');
     QRes1.ParamByName['xDal'] := DEDataDal.Date;
     QRes1.ParamByName['xAl'] := DEDataAl.Date;
     QRes1.SQL.Add('order by cognome');

     // cancellare
     //QRes1.SQL.SaveToFile('c:\QNuoviCV.txt');
     try
          QRes1.Open;
          if QRes1.IsEmpty then result := False
          else result := true;
     except
          result := False;
          //QRes1.SQL.SaveToFile('c:\QNuoviCV-ERROR-'+IntToStr(xIDRic)+'.txt');
     end;
end;

procedure TNuoviCVForm.TSelezAfterScroll(DataSet: TDataSet);
begin
     QRicAttiveSel.Close;
     SpeedButton2Click(self);
end;

function TNuoviCVForm.ControlloBlocchi: boolean;
var xIDAnag, xIDClienteBlocco, xIDEvento: integer;
     xLivProtez, xDicMess, xPassword, xUtenteResp, xClienteBlocco: string;
     xVai: boolean;
     QLocal: TADOLinkedQuery;
begin
     xVai := True;
     xIDAnag := longint(TV.Selected.Data);
     // controllo blocco livello 1 o 2
     xIDClienteBlocco := CheckAnagInseritoBlocco1(xIDAnag);
     if xIDClienteBlocco > 0 then begin
          xLivProtez := copy(IntToStr(xIDClienteBlocco), 1, 1);
          if xLivProtez = '1' then begin
               xIDClienteBlocco := xIDClienteBlocco - 10000;
               xClienteBlocco := GetDescCliente(xIDClienteBlocco);
               xDicMess := 'Stai inserendo in commessa un candidato appartenente ad una azienda (' + xClienteBlocco + ') con blocco di livello 1, ';
          end else begin
               xIDClienteBlocco := xIDClienteBlocco - 20000;
               xClienteBlocco := GetDescCliente(xIDClienteBlocco);
               xDicMess := 'Stai inserendo in commessa un candidato appartenente ad una azienda ATTIVA (' + xClienteBlocco + '), ';
          end;
          if MessageDlg('ATTENZIONE!! PROTEZIONE CLIENTI LIVELLO ' + xLivProtez + chr(13) + xDicMess +
               'protetta dalle normative interne, se sei autorizzato premi SI, altrimenti premi NO. ' +
               'Se si prosegue, l''operazione verr� registrata nello storico del soggetto come forzatura.', mtWarning, [mbYes, mbNo], 0) = mrNo then xVai := False
          else begin
               if pos('INTERMEDIA', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) = 0 then begin
                    // pwd
                    xPassword := '';
                    xUtenteResp := GetDescUtenteResp(MainForm.xIDUtenteAttuale);
                    if not InputQuery('Forzatura blocco livello ' + xLivProtez, 'Password di ' + xUtenteResp, xPassword) then exit;
                    if not CheckPassword(xPassword, GetPwdUtenteResp(MainForm.xIDUtenteAttuale)) then begin
                         MessageDlg('Password errata', mtError, [mbOK], 0);
                         xVai := False;
                    end;
               end;
               if xVai then begin
                    // promemoria al resp.
                    QLocal := CreateQueryFromString('insert into Promemoria (IDUtente,IDUtenteDa,DataIns,Testo,Evaso,DataDaLeggere) ' +
                         'values (:xIDUtente:,:xIDUtenteDa:,:xDataIns:,:xTesto:,:xEvaso:,:xDataDaLeggere:)');
                    QLocal.ParamByName['xIDUtente'] := GetIDUtenteResp(MainForm.xIDUtenteAttuale);
                    QLocal.ParamByName['xIDUtenteDa'] := MainForm.xIDUtenteAttuale;
                    QLocal.ParamByName['xDataIns'] := Date;
                    QLocal.ParamByName['xTesto'] := 'forzatura livello ' + xLivProtez + ' per codice soggetto ' + IntToStr(xIDAnag);
                    QLocal.ParamByName['xEvaso'] := 0;
                    QLocal.ParamByName['xDataDaLeggere'] := Date;
                    QLocal.ExecSQL;
                    // registra nello storico soggetto
                    xIDEvento := GetIDEvento('forzatura Blocco livello ' + xLivProtez);
                    if xIDEvento > 0 then begin
                         with Data.Q1 do begin
                              close;
                              SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
                                   'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xIDCliente:)';
                              ParamByName['xIDAnagrafica'] := xIDAnag;
                              ParamByName['xIDEvento'] := xIDevento;
                              ParamByName['xDataEvento'] := Date;
                              ParamByName['xAnnotazioni'] := 'cliente: ' + xClienteBlocco;
                              ParamByName['xIDRicerca'] := longint(TVRic.DropTarget.Data);
                              ParamByName['xIDUtente'] := MainForm.xIDUtenteAttuale;
                              ParamByName['xIDCliente'] := xIDClienteBlocco;
                              ExecSQL;
                         end;
                    end;
               end;
          end;
     end;
     Result := xVai;
end;

procedure TNuoviCVForm.SpeedButton3Click(Sender: TObject);
begin
     ApriHelp('25');
end;

end.

