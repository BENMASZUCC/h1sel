unit InsEvento;

// Giulio 22-11-2002 2.0.0.21
// - Allargato campo per annotazioni (nel DB Storico.Annotazioni � varchar(100)
// - sostituita DBGrid con QuantumGrid

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Grids, DBGrids, Db, DBTables, DtEdit97, Buttons, ExtCtrls,
     ADODB, U_ADOLinkCl, dxDBTLCl, dxGrClms, dxTL, dxDBCtrl, dxDBGrid,
     dxCntner, ComCtrls, JvExComCtrls, JvDateTimePicker, TB97;

type
     TInsEventoForm = class(TForm)
          DEData_old: TDateEdit97;
          Label1: TLabel;
          Label2: TLabel;
          DsEventi: TDataSource;
          Panel19: TPanel;
          EDesc: TEdit;
          DBGrid2: TDBGrid;
          Panel2: TPanel;
          DsUsers: TDataSource;
          TEventi: TADOLinkedTable;
          TStatiLK: TADOLinkedTable;
          QUsers: TADOLinkedQuery;
          TEventiID: TAutoIncField;
          TEventiIDdaStato: TIntegerField;
          TEventiEvento: TStringField;
          TEventiIDaStato: TIntegerField;
          TEventiNonCancellabile: TBooleanField;
          TEventiIDProcedura: TIntegerField;
          TStatiLKID: TAutoIncField;
          TStatiLKStato: TStringField;
          TStatiLKIDTipoStato: TIntegerField;
          TStatiLKPRman: TBooleanField;
          TStatiLKIDAzienda: TIntegerField;
          TEventiDaStato: TStringField;
          TEventiAStato: TStringField;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1Evento: TdxDBGridMaskColumn;
          dxDBGrid1DaStato: TdxDBGridLookupColumn;
          dxDBGrid1AStato: TdxDBGridLookupColumn;
          DEData: TJvDateTimePicker;
          Panel1: TPanel;
          BOk: TToolbarButton97;
          BAnnulla: TToolbarButton97;
          procedure FormShow(Sender: TObject);
          procedure FormCreate(Sender: TObject);
          procedure BOkClick(Sender: TObject);
          procedure BAnnullaClick(Sender: TObject);
     private
          { Private declarations }
     public
          xIDUtente: integer;
     end;

var
     InsEventoForm: TInsEventoForm;

implementation

//[/TONI20020929\]
uses ModuloDati, Main;
//[/TONI20020929\]FINE
{$R *.DFM}

procedure TInsEventoForm.FormShow(Sender: TObject);
begin
     //Grafica
     InsEventoForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel19.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel19.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel19.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;


     Caption := '[S/26] - ' + Caption;
     //[/TONI20020929\]
     //     QUsers.ParamByName('xOggi').asDateTime:=Date;
     //DEData.Date:=date;
     QUsers.Close;
     //[/TONI20020929\]FINE
     QUsers.Open;
     if xIDUtente <> 0 then
          QUsers.Locate('ID', xIDUtente, []);
end;

procedure TInsEventoForm.FormCreate(Sender: TObject);
begin
     xIDUtente := 0;
end;

procedure TInsEventoForm.BOkClick(Sender: TObject);
begin
     InsEventoForm.ModalResult := mrOk;
end;

procedure TInsEventoForm.BAnnullaClick(Sender: TObject);
begin
     InsEventoForm.ModalResult := mrCancel;
end;

end.

