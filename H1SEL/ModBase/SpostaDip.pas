unit SpostaDip;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ZoomArrow, StdCtrls, Buttons, Db, DBTables, Mask, DBCtrls, ExtCtrls,
  ImageWindow, ImgList;

type
  TSpostaDipForm = class(TForm)
    Panel1: TPanel;
    Panel17: TPanel;
    DBECogn: TDBEdit;
    DBENome: TDBEdit;
    TAnag: TTable;
    DsAnag: TDataSource;
    TAnagID: TAutoIncField;
    TAnagCognome: TStringField;
    TAnagNome: TStringField;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    PPosizNow: TPanel;
    PPosizNext: TPanel;
    RGTipo: TRadioGroup;
    Label1: TLabel;
    Label2: TLabel;
    ImageList1: TImageList;
    ImageBox1: TImageBox;
    procedure RGTipoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SpostaDipForm: TSpostaDipForm;

implementation

{$R *.DFM}

procedure TSpostaDipForm.RGTipoClick(Sender: TObject);
begin
     case RGTipo.ItemIndex of
     0: ImageBox1.ImageIndex:=0;
     1: ImageBox1.ImageIndex:=2;
     2: ImageBox1.ImageIndex:=1;
     end;
end;

end.
