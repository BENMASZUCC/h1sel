unit InsInserimento;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Grids, DBGrids, StdCtrls, Buttons, Mask, DBCtrls, ExtCtrls, Db, DBTables,
     DtEdit97;

type
     TInsInserimentoForm = class(TForm)
          Panel1: TPanel;
          Panel7: TPanel;
          DBEdit12: TDBEdit;
          BitBtn1: TBitBtn;
          DBGrid7: TDBGrid;
          BitBtn2: TBitBtn;
          Panel2: TPanel;
          Label1: TLabel;
          Label2: TLabel;
          ESoggetto: TEdit;
          PanTitoloCompensi: TPanel;
          DEDataDal: TDateEdit97;
          DEDataAl: TDateEdit97;
          EMotCess: TEdit;
          ENote: TEdit;
          Label3: TLabel;
          Label4: TLabel;
          Label5: TLabel;
          Label6: TLabel;
          Panel3: TPanel;
          procedure BitBtn1Click(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     InsInserimentoForm: TInsInserimentoForm;

implementation

uses ModuloDati, ModuloDati2, MDRicerche;

{$R *.DFM}

procedure TInsInserimentoForm.BitBtn1Click(Sender: TObject);
begin
     if DEDataDal.text = '' then begin
          ModalResult := mrNone;
          MessageDlg('campo "Dalla data" obbligatorio', mtError, [mbOK], 0);
          DEDataDal.SetFocus;
          Abort;
     end;
end;

end.
