unit SelNazione;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Grids, DBGrids, Db, DBTables, Buttons;

type
  TSelNazioneForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    QNazioni: TQuery;
    DsQNazioni: TDataSource;
    QNazioniID: TAutoIncField;
    QNazioniAbbrev: TStringField;
    QNazioniDescNazione: TStringField;
    QNazioniAreaGeografica: TStringField;
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    ECerca: TEdit;
    Label1: TLabel;
    procedure ECercaChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SelNazioneForm: TSelNazioneForm;

implementation

{$R *.DFM}

procedure TSelNazioneForm.ECercaChange(Sender: TObject);
begin
     QNazioni.Close;
     QNazioni.ParamByName['xDesc']:=ECerca.text+'%';
     QNazioni.Open;
end;

procedure TSelNazioneForm.FormShow(Sender: TObject);
begin
     Caption:='[S/2] - '+Caption;
end;

end.
