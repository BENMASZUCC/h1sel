unit InsRicerca;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, DBCtrls, Mask, Buttons, Spin, DtEdit97, Grids, DBGrids, Db,
     DBTables, ADODB, U_ADOLinkCl, TB97, ExtCtrls;

type
     TInsRicercaForm = class(TForm)
          DEData: TDateEdit97;
          Label1: TLabel;
          SENumRic: TSpinEdit;
          Label2: TLabel;
          GroupBox3: TGroupBox;
          GroupBox4: TGroupBox;
          GBCliente: TGroupBox;
          ECliente: TEdit;
          ERuolo: TEdit;
          EArea: TEdit;
          DsUsers: TDataSource;
          DBGrid1: TDBGrid;
          Label10: TLabel;
          CBTipo: TComboBox;
          GroupBox1: TGroupBox;
          DBGrid2: TDBGrid;
          DsQSedi: TDataSource;
          Label3: TLabel;
          ERifInterno: TEdit;
          Label4: TLabel;
          DEDataPrevChius: TDateEdit97;
          Q: TADOLinkedQuery;
          QSedi: TADOLinkedQuery;
          TUsers_old: TADOLinkedTable;
          Label5: TLabel;
          DEDataPrevPres: TDateEdit97;
          Label43: TLabel;
          ETitoloCLiente: TEdit;
          CBInBudget: TCheckBox;
          Label6: TLabel;
          DBGrid3: TDBGrid;
          QStati: TADOQuery;
          DsQStati: TDataSource;
          QStatiID: TAutoIncField;
          QStatiStatoRic: TStringField;
          TUsers_oldNominativo: TStringField;
          TUsers_oldID: TAutoIncField;
          TUsers_oldDescrizione: TStringField;
          TUsers: TADOQuery;
          TUsersID: TAutoIncField;
          TUsersNominativo: TStringField;
          TUsersDescrizione: TStringField;
          Panel1: TPanel;
          BitBtn1: TToolbarButton97;
          BAnnulla: TToolbarButton97;
          SpeedButton6: TToolbarButton97;
          BTrovaCliente: TToolbarButton97;
          SpeedButton2: TToolbarButton97;
          SpeedButton1: TToolbarButton97;
          cbUtente: TComboBox;
          procedure BTrovaClienteClick(Sender: TObject);
          procedure SpeedButton1Click(Sender: TObject);
          procedure BitBtn1Click(Sender: TObject);
          procedure FormCreate(Sender: TObject);
          procedure SpeedButton2Click(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure TUsers_oldAfterOpen(DataSet: TDataSet);
          procedure BAnnullaClick(Sender: TObject);
          procedure SpeedButton6Click(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
     private
          { Private declarations }
     public
          xIDCliente, xIDRuolo, xIDArea, xIDRifInterno: integer;
     end;

var
     InsRicercaForm: TInsRicercaForm;

implementation

uses SelCliente, InsRuolo, SelContatti,
     //[/TONI200209]
     ModuloDati, uQM, uASAInsRuolo, uUtilsVarie, Main;
//[/TONI200209]FINE

{$R *.DFM}

procedure TInsRicercaForm.BTrovaClienteClick(Sender: TObject);
begin
     SelClienteForm := TSelClienteForm.create(self);
     SelClienteForm.RGFiltro.Visible := False;
     SelClienteForm.ShowModal;
     if SelClienteForm.ModalResult = mrOK then begin
          //[/TONI200209]
          {        xIDCliente:=SelClienteForm.TClientiID.Value;
                  ECliente.Text:=SelClienteForm.TClientiDescrizione.Value;}
          xIDCliente := SelClienteForm.TClienti.FieldByName('ID').Value;
          ECliente.Text := SelClienteForm.TClienti.FieldByName('Descrizione').Value;
          //[/TONI200209]FINE
     end;
     SelClienteForm.Free;
end;

procedure TInsRicercaForm.SpeedButton1Click(Sender: TObject);
begin
     if (Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 6)) = 'ADVANT') or (pos('MCS', UpperCase(Data.Global.FieldByName('NomeAzienda').Value)) > 0) then begin
          InsRuoloAsaForm := TInsRuoloAsaForm.Create(self);
          InsRuoloAsaForm.ShowModal;
          if InsRuoloAsaForm.ModalResult = mrOK then begin
               xIDRuolo := InsRuoloAsaForm.TRuoli.FieldByName('ID').Asinteger;
               xIDArea := InsRuoloAsaForm.TAree.FieldByName('ID').Asinteger;
               ERuolo.Text := InsRuoloAsaForm.TRuoli.FieldByName('Descrizione').AsString;
               EArea.text := InsRuoloAsaForm.TAree.FieldByName('Descrizione').Value;
          end;
     end else begin
          InsRuoloForm := TInsRuoloForm.create(self);
          InsRuoloForm.Height := 425;
          InsRuoloForm.ShowModal;
          if InsRuoloForm.ModalResult = mrOK then begin
               xIDRuolo := InsRuoloForm.TRuoli.FieldByName('ID').Asinteger;
               xIDArea := InsRuoloForm.TRuoli.FieldByName('IDArea').Asinteger;
               ERuolo.Text := InsRuoloForm.TRuoli.FieldByName('Ruolo').AsString;
               EArea.text := InsRuoloForm.TRuoli.FieldByName('Area').Value;
          end;
          InsRuoloForm.Free;
     end;
end;

procedure TInsRicercaForm.BitBtn1Click(Sender: TObject);
begin
     InsRicercaForm.ModalResult := mrOK;
     if xIDCliente = 0 then begin
          ModalResult := mrNone;
          MessageDlg('Cliente mancante', mtError, [mbOK], 0);

     end;

    


     // richiedi controllo di qualit� sulle due date
     if not EffettuaControlli(2, [DEDataPrevPres.Date, DEDataPrevChius.Date]) then begin
          ModalResult := mrNone;
          Abort;
     end;
end;

procedure TInsRicercaForm.FormCreate(Sender: TObject);
begin
     xIDRuolo := 0;
     xIDRifInterno := 0;
     xIDCliente := 0;
     // riempimento combo tipo
     Q.SQL.text := 'select * from EBC_TipiCommesse';
     Q.Open;
     CBTipo.Items.Clear;
     while not Q.EOF do begin
          CBTipo.Items.Add(Q.FieldByName('TipoCommessa').asString);
          Q.next;
     end;
     Q.Close;
end;

procedure TInsRicercaForm.SpeedButton2Click(Sender: TObject);
begin
     if xIDCliente = 0 then begin
          Showmessage('selezionare prima un cliente');
          exit;
     end;
     SelContattiForm := TSelContattiForm.create(self);
     SelContattiForm.xIDCliente := xIDCliente;
     SelContattiForm.QContatti.Sql.text := 'select ID,Contatto,Telefono,IDAnagrafica from EBC_ContattiClienti where IDCliente=' + IntTostr(xIDCliente);
     SelContattiForm.QContatti.Open;
     SelContattiForm.ShowModal;
     if SelContattiForm.ModalResult = mrOK then begin
          xIDRifInterno := SelContattiForm.QContatti.FieldByName('ID').asInteger;
          ERifInterno.Text := SelContattiForm.QContatti.FieldByName('Contatto').asString;
     end;
     SelContattiForm.Free;
end;

procedure TInsRicercaForm.FormShow(Sender: TObject);
var xtemp: integer;
begin
     //Grafica
     InsRicercaForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     Caption := '[M/20] - ' + caption;
     QSedi.Open;
     QStati.Open;


     cbutente.Items.Clear;
     TUsers.First;
     while not tUsers.eof do begin
          cbutente.Items.Add(TUsers.FieldByName('Nominativo').AsString);
          {if TUsers.FieldByName('Nominativo').AsString = MainForm.xUtenteAttuale then
               xtemp := cbutente.ItemIndex; }
          tusers.next;
     end;
     //cbutente.ItemIndex := xtemp;

     cbutente.ItemIndex := cbUtente.Items.IndexOf(MainForm.xUtenteAttuale);

     //Gestione H1Sel Aziende
     if data.xSelAziende then begin
         // Label10.Visible := false;
         // CBTipo.Visible := false;
         // DBGrid3.Visible := false;
          //Label6.Visible := false;
          GBCliente.caption := 'Cliente Interno';
          GroupBox4.caption := 'Selez.princ. (capo progetto)';
          InsRicercaForm.Caption := '[M/20] - Nuovo Progetto';
          QStati.Locate('StatoRic', 'attiva', []);
         // GBCliente.top := 150;
         // GroupBox3.top := 230;
         // GroupBox4.top := 350;
         // GroupBox1.top := 400;
     end;
end;

procedure TInsRicercaForm.TUsers_oldAfterOpen(DataSet: TDataSet);
begin
     //tUsers.Sort := 'Nominativo';
end;

procedure TInsRicercaForm.BAnnullaClick(Sender: TObject);
begin
     InsRicercaForm.ModalResult := mrCancel;
end;

procedure TInsRicercaForm.SpeedButton6Click(Sender: TObject);
begin
     ApriHelp('20');
end;

procedure TInsRicercaForm.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
if ModalResult=mrOK then begin
     if (ETitoloCLiente.Text = '') and (eRuolo.Text = '') then begin

          MessageDlg('Attenzione: � necessario inserire il ruolo tabellare o il ruolo testuale', mtWarning, [mbOK],0);
          CanClose := false;

     end else begin
          CanClose := true;
     end;
     end else

           CanClose := true;
end;

end.

