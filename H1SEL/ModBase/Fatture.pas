unit Fatture;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Mask, DBCtrls, Db, RXDBCtrl, ExtCtrls, DBTables, Grids,
     DBGrids, Buttons, TB97, dxDBTLCl, dxGrClms, dxTL, dxDBCtrl, dxDBGrid,
     dxCntner, dxGridMenus, Menus, dxPSCore, dxPSdxTLLnk, dxPSdxDBGrLnk,
     dxPSdxDBCtrlLnk, ADODB, U_ADOLinkCl, Word2000, ComObj;

type
     TFattureForm = class(TForm)
          DsQFatture: TDataSource;
          Panel4: TPanel;
          dxDBGrid1: TdxDBGrid;
          ToolbarButton971: TToolbarButton97;
          PMStampa: TPopupMenu;
          stampagriglia1: TMenuItem;
          esportainExcel1: TMenuItem;
          esportainHTML1: TMenuItem;
          dxPrinter1: TdxComponentPrinter;
          dxPrinter1Link1: TdxDBGridReportLink;
          QFatture: TADOQuery;
          QFattureCliente: TStringField;
          QFattureID: TAutoIncField;
          QFattureProgressivo: TStringField;
          QFattureTipo: TStringField;
          QFattureIDCliente: TIntegerField;
          QFattureData: TDateTimeField;
          QFattureDecorrenza: TDateTimeField;
          QFattureImporto: TFloatField;
          QFattureIVA: TFloatField;
          QFattureTotale: TFloatField;
          QFattureModalitaPagamento: TStringField;
          QFattureAssegno: TStringField;
          QFattureScadenzaPagamento: TDateTimeField;
          QFatturePagata: TBooleanField;
          QFatturePagataInData: TDateTimeField;
          QFattureAppoggioBancario: TStringField;
          QFattureIDRicerca: TIntegerField;
          QFattureResponsabile: TStringField;
          QFattureFlagFuoriAppIVA63372: TBooleanField;
          QFattureRimbSpese: TBooleanField;
          dxDBGrid1Cliente: TdxDBGridMaskColumn;
          dxDBGrid1ID: TdxDBGridMaskColumn;
          dxDBGrid1Progressivo: TdxDBGridMaskColumn;
          dxDBGrid1Tipo: TdxDBGridMaskColumn;
          dxDBGrid1RifProg: TdxDBGridMaskColumn;
          dxDBGrid1IDCliente: TdxDBGridMaskColumn;
          dxDBGrid1Data: TdxDBGridDateColumn;
          dxDBGrid1Decorrenza: TdxDBGridDateColumn;
          dxDBGrid1IVA: TdxDBGridMaskColumn;
          dxDBGrid1ModalitaPagamento: TdxDBGridMaskColumn;
          dxDBGrid1Assegno: TdxDBGridMaskColumn;
          dxDBGrid1ScadenzaPagamento: TdxDBGridDateColumn;
          dxDBGrid1Pagata: TdxDBGridCheckColumn;
          dxDBGrid1PagataInData: TdxDBGridDateColumn;
          dxDBGrid1AppoggioBancario: TdxDBGridMaskColumn;
          dxDBGrid1IDRicerca: TdxDBGridMaskColumn;
          dxDBGrid1Responsabile: TdxDBGridMaskColumn;
          dxDBGrid1FlagFuoriAppIVA63372: TdxDBGridCheckColumn;
          dxDBGrid1RimbSpese: TdxDBGridCheckColumn;
          dxDBGrid1Importo: TdxDBGridCurrencyColumn;
          dxDBGrid1Totale: TdxDBGridCurrencyColumn;
          dxDBGrid1Column22: TdxDBGridColumn;
          QFattureFilialeOrigine: TStringField;
          QFattureFilialeChiusura: TStringField;
          dxDBGrid1Column23: TdxDBGridColumn;
          qAccordiFilialiLK: TADOQuery;
          qAccordiFilialiLKID: TAutoIncField;
          qAccordiFilialiLKDescrizione: TStringField;
          qAccordiFilialiLKPercOrigine: TFloatField;
          qAccordiFilialiLKPercChiusura: TFloatField;
          qAccordiFilialiLKAttivo: TBooleanField;
          QFatturePercOrigine: TFloatField;
          QFattureImportoOrigine: TFloatField;
          QFatturePercChiusura: TFloatField;
          QFattureImportoChiusura: TFloatField;
          dxDBGrid1Column24: TdxDBGridColumn;
          dxDBGrid1Column25: TdxDBGridColumn;
          dxDBGrid1Column26: TdxDBGridColumn;
          dxDBGrid1Column27: TdxDBGridColumn;
          QFattureRifProg: TStringField;
          BitBtn1: TToolbarButton97;
    QFattureComune: TStringField;
    QFattureProvincia: TStringField;
    QFattureregione: TStringField;
    dxDBGrid1Comune: TdxDBGridColumn;
    dxDBGrid1Provincia: TdxDBGridColumn;
    dxDBGrid1Regione: TdxDBGridColumn;
    QFattureAnno: TIntegerField;
    dxDBGrid1Anno: TdxDBGridColumn;
          procedure FormShow(Sender: TObject);
          procedure BitBtn1Click(Sender: TObject);
          procedure dxDBGrid1CustomDrawFooter(Sender: TObject; ACanvas: TCanvas;
               ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
               var AText: string; var AColor: TColor; AFont: TFont;
               var AAlignment: TAlignment; var ADone: Boolean);
          procedure stampagriglia1Click(Sender: TObject);
          procedure esportainExcel1Click(Sender: TObject);
          procedure esportainHTML1Click(Sender: TObject);
          procedure dxDBGrid1MouseUp(Sender: TObject; Button: TMouseButton;
               Shift: TShiftState; X, Y: Integer);
     private
          xColumn: TColumn;
     public
          xIDUtente: integer;
     end;

var
     FattureForm: TFattureForm;

implementation

uses Main, OffertaCliente, ModuloDati, SelCliente, CreaCompensi;

{$R *.DFM}

procedure TFattureForm.FormShow(Sender: TObject);
begin
     //Grafica
     FattureForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel4.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel4.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel4.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     if QFatture.Active = false then QFatture.Open;
     dxDBGrid1.Filter.Clear;
end;

procedure TFattureForm.BitBtn1Click(Sender: TObject);
begin
     //close;
     FattureForm.ModalResult := mrok;
end;

procedure TFattureForm.dxDBGrid1CustomDrawFooter(Sender: TObject;
     ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
     AColumn: TdxTreeListColumn; var AText: string; var AColor: TColor;
     AFont: TFont; var AAlignment: TAlignment; var ADone: Boolean);
begin
     AFont.Style := [fsBold];
end;

procedure TFattureForm.stampagriglia1Click(Sender: TObject);
begin
     dxPrinter1.Preview(True, dxPrinter1Link1);
end;

procedure TFattureForm.esportainExcel1Click(Sender: TObject);
var MSExcel: variant;
     xFileWord, DescrizioneFile: string;
begin
     Mainform.Save('xls', 'Microsoft Excel 11.0 (*.xls)|*.xls', 'ExpGrid.xls', dxDBGrid1.SaveToXLS);
end;

procedure TFattureForm.esportainHTML1Click(Sender: TObject);
begin
     Mainform.Save('htm', 'HTML File (*.htm; *.html)|*.htm', 'ExpGrid.htm', dxDBGrid1.SaveToHTML);
end;

procedure TFattureForm.dxDBGrid1MouseUp(Sender: TObject;
     Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
     if (Button <> mbRight) or (Shift <> []) then Exit;
     TdxDBGridPopupMenuManager.Instance.ShowGridPopupMenu(Sender as TdxDBGrid);
end;

end.

