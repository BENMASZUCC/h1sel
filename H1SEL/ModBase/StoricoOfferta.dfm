object StoricoOffertaForm: TStoricoOffertaForm
  Left = 474
  Top = 308
  BorderStyle = bsDialog
  Caption = 'Storico offerta'
  ClientHeight = 194
  ClientWidth = 349
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 4
    Top = 5
    Width = 253
    Height = 184
    DataSource = DsQStoricoOfferta
    ReadOnly = True
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'Stato'
        Width = 136
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DallaData'
        Title.Caption = 'Dalla data'
        Width = 82
        Visible = True
      end>
  end
  object BitBtn1: TBitBtn
    Left = 262
    Top = 5
    Width = 84
    Height = 32
    TabOrder = 1
    Kind = bkOK
  end
  object QStoricoOfferta_OLD: TQuery
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select * from EBC_OfferteStorico with (updlock)'
      'where IDOfferta=:xIDOfferta'
      'order by DallaData')
    Left = 96
    Top = 56
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'xIDOfferta'
        ParamType = ptUnknown
      end>
    object QStoricoOfferta_OLDID: TAutoIncField
      FieldName = 'ID'
    end
    object QStoricoOfferta_OLDIDOfferta: TIntegerField
      FieldName = 'IDOfferta'
    end
    object QStoricoOfferta_OLDStato: TStringField
      FieldName = 'Stato'
      FixedChar = True
    end
    object QStoricoOfferta_OLDDallaData: TDateTimeField
      FieldName = 'DallaData'
    end
    object QStoricoOfferta_OLDNote: TStringField
      FieldName = 'Note'
      FixedChar = True
      Size = 50
    end
  end
  object DsQStoricoOfferta: TDataSource
    DataSet = QStoricoOfferta
    Left = 96
    Top = 88
  end
  object QStoricoOfferta: TADOLinkedQuery
    Connection = Data.DB
    Parameters = <
      item
        Name = 'xIDOfferta:'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select * from EBC_OfferteStorico with (updlock)'
      'where IDOfferta=:xIDOfferta:'
      'order by DallaData')
    OriginalSQL.Strings = (
      'select * from EBC_OfferteStorico with (updlock)'
      'where IDOfferta=:xIDOfferta:'
      'order by DallaData')
    UseFilter = False
    Left = 96
    Top = 24
    object QStoricoOffertaID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QStoricoOffertaIDOfferta: TIntegerField
      FieldName = 'IDOfferta'
    end
    object QStoricoOffertaStato: TStringField
      FieldName = 'Stato'
    end
    object QStoricoOffertaDallaData: TDateTimeField
      FieldName = 'DallaData'
    end
    object QStoricoOffertaNote: TStringField
      FieldName = 'Note'
      Size = 50
    end
  end
end
