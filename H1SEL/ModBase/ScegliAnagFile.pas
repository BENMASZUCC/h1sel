unit ScegliAnagFile;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, Main,
     StdCtrls, TB97, ExtCtrls, dxTL, dxDBCtrl, dxDBGrid, dxCntner,
     dxDBTLCl, dxGrClms;

type
     TScegliAnagFileForm = class(TForm)
          Panel3: TPanel;
          ToolbarButton972: TToolbarButton97;
          ToolbarButton973: TToolbarButton97;
          CBNessunFile: TCheckBox;
          Panel2: TPanel;
          BAnnulla: TToolbarButton97;
          BOK: TToolbarButton97;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1Descrizione: TdxDBGridColumn;
          dxDBGrid1NomeFile: TdxDBGridColumn;
          dxDBGrid1Tipo: TdxDBGridColumn;
          dxDBGrid1Bloccato: TdxDBGridCheckColumn;
          procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
          procedure BAnnullaClick(Sender: TObject);
          procedure BOKClick(Sender: TObject);
          procedure ToolbarButton972Click(Sender: TObject);
          procedure CBNessunFileClick(Sender: TObject);
          procedure ToolbarButton973Click(Sender: TObject);
          procedure FormShow(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     ScegliAnagFileForm: TScegliAnagFileForm;

implementation

uses modulodati, uUtilsVarie;

{$R *.DFM}

procedure TScegliAnagFileForm.FormCloseQuery(Sender: TObject;
     var CanClose: Boolean);
begin
     MainForm.OnCloseQuery(self, canclose);
end;

procedure TScegliAnagFileForm.BAnnullaClick(Sender: TObject);
begin
     ModalResult := mrCancel;
end;

procedure TScegliAnagFileForm.BOKClick(Sender: TObject);
begin
     ModalResult := mrOk;
end;

procedure TScegliAnagFileForm.ToolbarButton972Click(Sender: TObject);
begin
     salvafileDue(data.QAnagFile.FieldByName('ID').asInteger, '');
end;

procedure TScegliAnagFileForm.CBNessunFileClick(Sender: TObject);
begin
     dxDBGrid1.Enabled := CBNessunFile.Enabled;
end;

procedure TScegliAnagFileForm.ToolbarButton973Click(Sender: TObject);
begin
     if data.QAnagFile.IsEmpty then exit;
     ApriFileDue(data.QAnagFile.FieldByName('ID').asInteger, '');
end;

procedure TScegliAnagFileForm.FormShow(Sender: TObject);
begin
     // grafica
     Panel3.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
end;

end.

