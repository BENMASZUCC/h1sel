�
 TDETTESPLAVFORM 0�  TPF0TDettEspLavFormDettEspLavFormLeftbTop� Width[HeightCaption*Esperienze lavorative - Dettagli ulterioriColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrder	PositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TLabelLabel44LeftTop3WidthfHeightCaptionDescrizione posizioneFocusControlDBMemo2  TLabelLabel45LeftTop� Width� HeightCaptionMotivazione cambio di lavoroFocusControlDBMemo3  TLabelLabel2LeftTop� Width� HeightCaptionDescrizione attivit� aziendaFocusControlDBMemo1  TLabelLabel5Left� Top� Width� HeightCaption,attenzione:  per ora � collegato al soggettoFont.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel6LeftTop�Width� HeightCaption*Ruolo temporaneo (indicato dal candidato):Font.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel7LeftTop�Width� HeightCaption&Ruolo indicato da form autocandidatura  TLabelLabel8LeftTopWidth?HeightCaptionNote generali  TDBMemoDBMemo2LeftTopBWidth�HeightO	DataFieldDescrizioneMansione
DataSourceData.DsEspLavTabOrder   TDBMemoDBMemo3LeftTop� Width�Height(	DataFieldMotivoCessazione
DataSourceData.DsEspLavTabOrder  TPanelPanel1LeftTopWidth�Height*
BevelOuter	bvLoweredEnabledTabOrder TLabelLabel1LeftTopWidth)HeightCaptionAzienda:  TLabelLabel36LeftTopWidth"HeightCaptionSettoreFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TDBEditDBEdit23LeftTopWidth HeightColorclLime	DataFieldAziendaNome
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder   TDBEditDBEdit27LeftTopWidth� Height	DataFieldSettore
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   TDBMemoDBMemo1LeftTop� Width�Height.	DataFieldDescrizioneAttivitaAz
DataSourceData.DsEspLavTabOrder  	TGroupBox	GroupBox2LeftTop[Width~HeightKCaption	PreavvisoTabOrder TLabelLabel3LeftTopWidthHeightCaptionGiorni  TLabelLabel4LeftTop2WidthbHeightCaptionTermine di preavviso  TDBCheckBoxDBCheckBox1LeftfTopWidthHeightCaption8di calendario (se non selezionato = di lavoro effettivo)	DataFieldPreavvisoCalendario
DataSourceData.DsEspLavTabOrder ValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox2Left� Top1Width� HeightCaptionQualsiasi giorno del mese	DataFieldPreavvisoTermQualsGiorno
DataSourceData.DsEspLavTabOrderValueCheckedTrueValueUncheckedFalse  TdxDBSpinEditdxDBSpinEdit1Left'TopWidth9TabOrder	DataFieldPreavvisoGiorni
DataSourceData.DsEspLav  TdxDBSpinEditdxDBSpinEdit2LeftoTop/Width9TabOrder	DataFieldPreavvisoTermGiorno
DataSourceData.DsEspLav   TDBEditDBEdit1Left� Top�Width� Height	DataFieldRuolo
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontReadOnly	TabOrder  TDBEditDBEdit2Left� Top�Width� Height	DataFieldTitoloMansione
DataSourceData.DsEspLavTabOrder  TDBMemoDBMemo4LeftTop Width�Height1	DataFieldNote
DataSourceData.DsEspLavTabOrder  TPanelPanel2Left�Top�WidthZHeightW
BevelOuterbvNoneTabOrder TToolbarButton97BOKLeftTopWidthUHeight#CaptionOK
Glyph.Data
z  v  BMv      6  (               @                     ��� � � @�h ��� TQI �? v�v ��� ('' yґ E�^ qm^ ��� $�M ��� _�w \� ��� ��� �ټ \�d =<9 v�� 4�N :�] |{s T�s _^^ ��x ��� ,�X �Ʀ b�s jɃ ��� I�u ��� O�j ��� i|m ��� ��� *�M ��� ��� ��� 421 D�f S�` �� 7�^ JIC 9�U r� |�� _wc nnm U�l k� ��� ��� 0�W J�p ��� f�l `�l wċ %�G � �° T�q ��� e�y ��p ��� V�y qЊ [XN [�y ��� ��� M�n �Ė ��� 2�T urq d�} =�[ A�` ��� ��� ��� ��� ��� ��� F�p ��� H�c �Ǔ ��� m�v n� dЁ N�u C�h x�� V�o *�R �� ��� ?�b ~~{ o�p 2�P ��� �̹ 1�Z ��� H�_ ��� ;�c Ļ� g�o w͎ ��� U�z O�g ��� ]�r r�� ��� ��� \�o ��� c`a 0�V M�p F�f [�w �ȶ ��� 8�Y I�[ ��� ��u ��� sɉ 3�] ��� Y�o ��� ��� 3�Z �á mą i�z @�j Q�u [YY I�m ��� ��� ��� ��� (�Q ��� ?�c Z�~ ��� e� {�z b�s ��� IIH B�a yvu ��� l͆ ��� p�� t�� �ϼ Ŀ� +�V <�X J�k ��� |�� ��� h�~ <�a I�s 11/ ��� ��� E�n O�r k�m ��} ��� _�y e�w ��� ;�d S�y cxf ��� ��� |̑ ɿ� 7�Z =�] e|j ��� ��� Q�l }�� zxp ��� {�� ��� ��� &�K +�O L�a ��� ��� Y�t �Ɵ b�x ��� ��� ��� U�v ��� S�n \�v |Ǐ ��� ��� �˽ ��� ��� VTK ��� �ɼ ƽ� 7�_ A�j B�] ��� W�| ��� ��� ��� .9��		��9.�/Pz��z�/�.�V}��uu���}���4��>?����?�>E_4p�����?ih~�L�����=<N�uy�0�`�h��L�u�%N�s��3yo�n��$��?L���d�<+�3�nnn�`���ŧ��s��l���nn�nףR�yy�Rl@���vB�U­n�HQ�'���y3�R�ґJ#5����Q\�''���3�0�#J���ח�uy�\�''����ӯD��t��G�˥333uޤ'���l�&rF2��wk��O�>��3��㗗�XY�^���{�W"MgR�>>,��-�b1�F��澛�|


M���-���F���!I;�C����|���Z]��А���q��j�cccc�C�;�:����[�A��*SSSS*��;I��%���a�A��������fʜЕ���_�� ����m68���x}=(7��Te�=}x���K�.))ۍK����۱��.۱�OnClickBOKClick  TToolbarButton97BAnnullaLeftTop+WidthUHeight#CaptionAnnulla
Glyph.Data
z  v  BMv      6  (               @                     ��� � � ��} � Ze� ��� =<: ��� ��� OO� ��� ba] 47� ��� ef� bb� ��~ �û xx� ~�� ((' ��� 76� NN� ��� ��� ��� SRI tt� mmm %%� LS� ��� ��� cb� ��w cc� CF� XX� ?G� VV� ��� ��� ��� �к ��� 330 ml� ��� ��� ||� ��� ��� HHH {zs pn] -.� (*� jl� aa� Z[� ��� ��� ��� oo� ��o sr� VZ� ��� ��� ��� CC� ��� ��� ||� ��� 38� MM� 00� �ŭ ss� ��� T^� QQ� ii� -2� GK� ��� ��� ��� ��� 4;� __� YZW ``� II� w|� ��� ��� ��� ;>� ��� ��� JJ� YWM ��� VV� ��� \\� ��� ��� dm� ��� ��� nn� )-� ##� ��� ��� ��� �˻ AF� QW� @@� qq� -/� utm ??� II� NN� ��� ��� 9:� ��� ��� Ya� ll� 33� ��� ��� ~~� �ջ ))� ��� ��� ��� ��� ��� ��� ��� ��� hg� xw� ��� ��� BD� ��� KT� ��� `h� nn� ��� �Ǽ DG� NO� TZ� w{� JIB [[� ��� ��� ��t bb� ��� BJ� QQ� __� ||� ��� 9>� ��� ��� FE� ff� ��� 66� 00/ ��� �ٽ ��� //� �Ƿ \\[ �ö 99� ~~{ ww� ��� EF� NV� II� ��� QQ� ^^� ��� zz� 44� ��� ]d� SU� V]� ��� SS� ~~� �͸ 68� ��� ��� aa� ��� {{� ��� EJ� ��� ZZ� ^[� 8;� 8=� AK� ��� ��� ��� <;6 ��� 35� 55� 58� GG� YY� [^� bb� jj� rr� ��� ��� ��� VTK ))� ++� 01� yxn ��� ``� ��� ��� 6��6��/joo�/^R���M��M=+�7��ތ�ߦ��ш�z�+���!�_� �ӈ�pp�Ѥt�����i��V&��SSS��D���qiE7X?~�n�a�SS ����~����8���2��4�SS�������8E��J��;gn>Z SS�c�gW��9fL��*�ϊ~�n�{�(.�g㜥��P�Bh�����4\���e��|B$-ðh�إg��4�ԃ�e؊�Hu�$�׀��T��D�nnnn��؊��������=%�:�nnn����9�H�O����#k̭'�>�n��n�'���Ɂ
-�rP�__���>"5>�d`���CPr[,�]_������}�g��w��`�v�ry0�K�1���s��GN�y�[��<�K���������)q�[�	�<lxl���x�Q���)�������b�+lx�QQAm@��[�F���b+3U�F��[�ʹYIIY�ʚ[[E����[OnClickBAnnullaClick    