unit InputPassword;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, TB97, ExtCtrls;

type
     TInputPasswordForm = class(TForm)
          EPassword: TEdit;
          Label1: TLabel;
          Panel1: TPanel;
          BOK: TToolbarButton97;
          BAnnulla: TToolbarButton97;
          procedure FormShow(Sender: TObject);
          procedure BOKClick(Sender: TObject);
          procedure BAnnullaClick(Sender: TObject);
          procedure EPasswordKeyPress(Sender: TObject; var Key: Char);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     InputPasswordForm: TInputPasswordForm;

implementation

uses Main;

{$R *.DFM}

procedure TInputPasswordForm.FormShow(Sender: TObject);
begin
     InputPasswordForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
end;

procedure TInputPasswordForm.BOKClick(Sender: TObject);
begin
     InputPasswordForm.ModalResult := mrOK;
end;

procedure TInputPasswordForm.BAnnullaClick(Sender: TObject);
begin
     InputPasswordForm.ModalResult := mrCancel;
end;

procedure TInputPasswordForm.EPasswordKeyPress(Sender: TObject;
     var Key: Char);
begin
     if key = chr(13) then BOKClick(self);
end;

end.

