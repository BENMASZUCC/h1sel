unit CliAltriSettori;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     dxTL, dxDBCtrl, dxDBGrid, dxCntner, Db, DBTables, TB97, StdCtrls,
     Buttons, ExtCtrls, DBCtrls, ADODB, U_ADOLinkCl;

type
     TCliAltriSettoriForm = class(TForm)
          Panel1: TPanel;
          BitBtn1: TBitBtn;
          Panel2: TPanel;
          BAnagFileNew: TToolbarButton97;
          BAnagFileDel: TToolbarButton97;
          DsQCliSettori: TDataSource;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1IDCliente: TdxDBGridMaskColumn;
          dxDBGrid1Attivita: TdxDBGridMaskColumn;
          dxDBGrid1IDAttivita: TdxDBGridMaskColumn;
          dxDBGrid1ID: TdxDBGridMaskColumn;
          dxDBGrid1AreaSettore: TdxDBGridMaskColumn;
          BitBtn2: TBitBtn;
          Label1: TLabel;
          DBText1: TDBText;
          QCliSettori: TADOLinkedQuery;
          procedure BAnagFileNewClick(Sender: TObject);
          procedure BAnagFileDelClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     CliAltriSettoriForm: TCliAltriSettoriForm;

implementation

uses ModuloDati2, ModuloDati, SelAttivita;

{$R *.DFM}

procedure TCliAltriSettoriForm.BAnagFileNewClick(Sender: TObject);
begin
     SelAttivitaForm := TSelAttivitaForm.create(self);
     SelAttivitaForm.ShowModal;
     if SelAttivitaForm.ModalResult = mrOK then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.SetSQLText(['insert into EBC_ClientiAttivita (IDCliente,IDAttivita) ' +
                         'values (:xIDCliente:,:xIDAttivita:)',
                              Data2.TEBCClienti.FieldByName('ID').AsInteger,
                              SelAttivitaForm.TSettori.FieldByName('ID').AsInteger]);
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    QCliSettori.Close;
                    QCliSettori.Open;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               end;
          end;
     end;
     SelAttivitaForm.Free;
end;

procedure TCliAltriSettoriForm.BAnagFileDelClick(Sender: TObject);
begin
     if MessageDlg('Sei sicuro di voler eliminare l''associazione ?', mtWarning, [mbNo, mbYes], 0) <> mrYes then exit;
     with Data do begin
          DB.BeginTrans;
          try
               Q1.SQL.text := 'delete from EBC_ClientiAttivita ' +
                    'where ID=' + QCliSettori.FieldByName('ID').AsString;
               Q1.ExecSQL;
               DB.CommitTrans;
               QCliSettori.Close;
               QCliSettori.Open;
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
          end;
     end;
end;

procedure TCliAltriSettoriForm.FormShow(Sender: TObject);
begin
     QCliSettori.Open;
end;

end.

