//[TONI20021003]DEBUGOK
unit StoricoRic;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Grids, DBGrids, StdCtrls, Buttons, dxDBTLCl, dxGrClms, dxTL, dxDBCtrl,
     dxDBGrid, dxCntner, TB97, ExtCtrls;

type
     TStoricoRicForm = class(TForm)
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1ID: TdxDBGridMaskColumn;
          dxDBGrid1IDRicerca: TdxDBGridMaskColumn;
          dxDBGrid1IDStatoRic: TdxDBGridMaskColumn;
          dxDBGrid1DallaData: TdxDBGridDateColumn;
          dxDBGrid1Note: TdxDBGridMaskColumn;
          dxDBGrid1TotGiorni: TdxDBGridMaskColumn;
          dxDBGrid1IDEventoRic: TdxDBGridMaskColumn;
          dxDBGrid1ID_1: TdxDBGridMaskColumn;
          dxDBGrid1StatoRic: TdxDBGridMaskColumn;
          dxDBGrid1IdAzienda: TdxDBGridMaskColumn;
          dxDBGrid1ID_2: TdxDBGridMaskColumn;
          dxDBGrid1EventoRic: TdxDBGridMaskColumn;
    dxDBGrid1StatoSEcondario: TdxDBGridColumn;
    Panel1: TPanel;
    ToolbarButton971: TToolbarButton97;
    BitBtn2: TToolbarButton97;
    BitBtn3: TToolbarButton97;
    BitBtn4: TToolbarButton97;
          procedure BitBtn2Click(Sender: TObject);
          procedure BitBtn3Click(Sender: TObject);
          procedure BitBtn4Click(Sender: TObject);
          procedure FormShow(Sender: TObject);
    procedure ToolbarButton971Click(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     StoricoRicForm: TStoricoRicForm;

implementation

uses MDRicerche, SelData, ModuloDati, Main;

{$R *.DFM}

procedure TStoricoRicForm.BitBtn2Click(Sender: TObject);
begin

     //[ALBERTO 20020912]

     SelDataForm := TSelDataForm.create(self);
     SelDataForm.DEData.Date := DataRicerche.TStoricoRic.FieldByName('DallaData').AsDateTime;
     SelDataForm.ShowModal;
     if SelDataForm.ModalResult = mrOK then begin
          Data.DB.BeginTrans;
          try
               Data.Q1.Close;
               Data.Q1.SQL.text := 'update EBC_StoricoRic set DallaData=:xDallaData: ' +
                    'where ID=' + DataRicerche.TStoricoRic.FieldByName('ID').AsString;
               Data.Q1.ParamByName['xDallaData'] := SelDataForm.DEData.Date;
               Data.Q1.ExecSQL;
               Data.DB.CommitTrans;
               DataRicerche.TStoricoRic.Close;
               DataRicerche.TStoricoRic.Open;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE:  operazione non completata', mtError, [mbOK], 0);
          end;
     end;
     SelDataForm.Free;
end;

procedure TStoricoRicForm.BitBtn3Click(Sender: TObject);
var xNote: string;
begin
     xNote := DataRicerche.TStoricoRic.FieldByName('Note').AsString;
     if not inputquery('Modifica storico ric.', 'Note:', xNote) then exit;
     Data.DB.BeginTrans;
     try
          Data.Q1.Close;
          Data.Q1.SQL.text := 'update EBC_StoricoRic set Note=:xNote: ' +
               'where ID=' + DataRicerche.TStoricoRic.FieldByName('ID').AsString;

          //[ALBERTO 20020912]FINE

          Data.Q1.ParamByName['xNote'] := xNote;
          Data.Q1.ExecSQL;
          Data.DB.CommitTrans;
          DataRicerche.TStoricoRic.Close;
          DataRicerche.TStoricoRic.Open;
     except
          Data.DB.RollbackTrans;
          MessageDlg('ERRORE SUL DATABASE:  operazione non completata', mtError, [mbOK], 0);
     end;
end;

procedure TStoricoRicForm.BitBtn4Click(Sender: TObject);
begin
     if DataRicerche.TStoricoRic.IsEmpty then exit;
     if MessageDlg('Sei sicuro di voler eliminare la riga di storico ?', mtWarning, [mbNo, mbYes], 0) = mrYes then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text := 'delete from EBC_StoricoRic ' +
                         'where ID=' + DataRicerche.TStoricoRic.FieldByName('ID').asString;
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    DataRicerche.TStoricoRic.Close;
                    DataRicerche.TStoricoRic.Open;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               end;
          end;
     end;
end;

procedure TStoricoRicForm.FormShow(Sender: TObject);
begin
     Caption := '[M/215] - ' + Caption; 
     StoricoRicForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
end;

procedure TStoricoRicForm.ToolbarButton971Click(Sender: TObject);
begin
     StoricoRicForm.ModalResult:=mrOK;
end;

end.
