�
 TINSNODOORGFORM 03  TPF0TInsNodoOrgFormInsNodoOrgFormLeft4Top� ActiveControlEDenomBreveBorderStylebsDialogCaptionInserimento nuovo nodoClientHeight� ClientWidthColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterPixelsPerInch`
TextHeight TLabelLabel97LeftTopwWidth� HeightCaptionDenominazione estesa posizione  TLabelLabel98LeftTop� WidthHeightCaptionLivello  TLabelLabel99LeftTopLWidthVHeightCaptionDenominaz. breve  TPanelPanel1Left Top WidthHeight1AlignalTop
BevelOuterbvNoneTabOrder TBitBtnBitBtn1LeftNTopWidth[Height$TabOrder KindbkOK  TBitBtnBitBtn2Left� TopWidth[Height$CaptionAnnullaTabOrderKindbkCancel   TPanelPanel18Left Top1WidthHeightAlignalTop	AlignmenttaLeftJustifyCaption*  Inserimento informazioni principali nodoColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TPanelPanel110LeftbTopZWidthUHeightTabOrder 	TCheckBox	CBInterimLeftTopWidthKHeightCaption
ad interimFont.CharsetDEFAULT_CHARSET
Font.ColorclYellowFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder    TPanelPanel111Left� TopZWidthPHeightTabOrder 	TCheckBoxCBStaffLeftTopWidthAHeightCaptionin staffFont.CharsetDEFAULT_CHARSET
Font.ColorclTealFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder    TEditEDenomBreveLeftTop\WidthXHeightCharCaseecUpperCaseTabOrder   TEditEDenomEstesaLeftTop� WidthHeightCharCaseecUpperCaseFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder  	TComboBox	CBLivelloLeftTop� Width� Height
ItemHeightItems.StringsDirezione Generale	DivisioneEnteFunzioneRepartoTurnoDiretto TabOrderText	Divisione   