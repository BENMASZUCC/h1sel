//[TONI20020923]DEBUGOK
unit SceltaAnagFile;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, Grids, DBGrids, RXDBCtrl, Db, DBTables, ExtCtrls,
     ADODB, U_ADOLinkCl, TB97, dxTL, dxDBCtrl, dxDBGrid, dxCntner;

type
     TSceltaAnagFileForm = class(TForm)
          Panel1: TPanel;
          TBSalva: TToolbarButton97;
          TBApri: TToolbarButton97;
          TBEsci: TToolbarButton97;
          Panel3: TPanel;
          Label1: TLabel;
          ToolbarButton972: TToolbarButton97;
          ToolbarButton973: TToolbarButton97;
          ToolbarButton974: TToolbarButton97;
    RxDBGrid1: TdxDBGrid;
    RxDBGrid1Tipo: TdxDBGridColumn;
    RxDBGrid1Descrizione: TdxDBGridColumn;
    RxDBGrid1NomeFile: TdxDBGridColumn;
    RxDBGrid1DataCreazione: TdxDBGridColumn;
    RxDBGrid1FileBloccato: TdxDBGridColumn;
          procedure BitBtn2Click(Sender: TObject);
          procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
          procedure TBApriClick(Sender: TObject);
          procedure TBEsciClick(Sender: TObject);
          procedure TBSalvaClick(Sender: TObject);
          procedure FormShow(Sender: TObject);
     private
          { Private declarations }
     public
          xModalitaApertura: integer;
          //se valorizzata a 2 il form viene usato per fare una selezione dei file ad esempio nel modello keystoen
               { Public declarations }
     end;

var
     SceltaAnagFileForm: TSceltaAnagFileForm;

implementation

uses ModuloDati, uUtilsVarie, main;

{$R *.DFM}

procedure TSceltaAnagFileForm.BitBtn2Click(Sender: TObject);
begin
     SceltaAnagFileForm.Close;

end;

procedure TSceltaAnagFileForm.FormCloseQuery(Sender: TObject;
     var CanClose: Boolean);
begin
     MainForm.OnCloseQuery(self, canclose);
end;

procedure TSceltaAnagFileForm.TBApriClick(Sender: TObject);
begin
     if data.QAnagFileApriCV.IsEmpty then exit;
     ApriFileDue(data.QAnagFileApriCV.FieldByName('IDFile').asInteger, 'sceltaanagfile');
end;

procedure TSceltaAnagFileForm.TBEsciClick(Sender: TObject);
begin
     SceltaAnagFileForm.Close;
end;

procedure TSceltaAnagFileForm.TBSalvaClick(Sender: TObject);
begin
     salvafileDue(data.QAnagFileApriCV.FieldByName('IDFile').asInteger, 'sceltaanagfile');
end;

procedure TSceltaAnagFileForm.FormShow(Sender: TObject);
begin
     //Grafica
     SceltaAnagFileForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name; 
     Panel3.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel3.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel3.font.name := MainForm.AdvToolBarOfficeStyler1.font.name; 
end;

end.

