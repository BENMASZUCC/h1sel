unit StoricoOfferta;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, Grids, DBGrids, Db, DBTables, ADODB, U_ADOLinkCl;

type
     TStoricoOffertaForm = class(TForm)
          QStoricoOfferta_OLD: TQuery;
          DsQStoricoOfferta: TDataSource;
          QStoricoOfferta_OLDID: TAutoIncField;
          QStoricoOfferta_OLDIDOfferta: TIntegerField;
          QStoricoOfferta_OLDStato: TStringField;
          QStoricoOfferta_OLDDallaData: TDateTimeField;
          QStoricoOfferta_OLDNote: TStringField;
          DBGrid1: TDBGrid;
          BitBtn1: TBitBtn;
          QStoricoOfferta: TADOLinkedQuery;
    QStoricoOffertaID: TAutoIncField;
    QStoricoOffertaIDOfferta: TIntegerField;
    QStoricoOffertaStato: TStringField;
    QStoricoOffertaDallaData: TDateTimeField;
    QStoricoOffertaNote: TStringField;
          procedure FormShow(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     StoricoOffertaForm: TStoricoOffertaForm;

implementation
uses ModuloDati;
{$R *.DFM}

procedure TStoricoOffertaForm.FormShow(Sender: TObject);
begin
     Caption := '[S/42] - ' + Caption;
end;

end.
