�
 TPROMEMORIAFORM 0�  TPF0TPromemoriaFormPromemoriaFormLeftTopvWidthHeight�BorderIcons
biMinimize
biMaximize Caption
PromemoriaColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrder	PositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TPanel
Wallpaper1Left Top WidthHeight!AlignalTopTabOrder  TDBTextDBText1Left6TopWidthAHeight	DataField
Nominativo
DataSourceData.DsUsersFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTransparent	  TLabelLabel1LeftTopWidth*HeightCaptionUtente:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsUnderline 
ParentFontTransparent	  TToolbarButton97ToolbarButton971Left�Top WidthSHeight CaptionProcediFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style Opaque
ParentFontOnClickToolbarButton971Click   TPanelPanel1Left Top!WidthHeightAlignalTop	AlignmenttaLeftJustifyCaption  CompleanniColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder  TPanelPanel2Left Top:WidthHeightAlignalTop
BevelOuter	bvLoweredCaptionPanel2TabOrder TDBGridDBGrid1Left TopWidthHeightd
DataSourceDataSource2ReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameCognomeWidthHVisible	 Expanded	FieldNameNomeWidthDVisible	 Expanded	FieldNameDataNascitaVisible	 Expanded	FieldNameEtaVisible	    TPanelPanel3LeftTopWidthHeightAlignalTopTabOrder TLabelLabel2LeftTopWidthHeightCaptionOggiFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsItalic 
ParentFont  TLabelLabel3LeftTopWidth*HeightCaptionDomaniFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsItalic 
ParentFont   TDBGridDBGrid2LeftTopWidth Heightd
DataSourceDataSource1ReadOnly	TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameCognomeWidthHVisible	 Expanded	FieldNameNomeWidth>Visible	 Expanded	FieldNameDataNascitaVisible	 Expanded	FieldNameEtaVisible	     TPanelPanel12Left Top� WidthHeight#AlignalClient
BevelInner	bvLoweredCaptionPanel12TabOrder 	TSplitter	Splitter4LeftTopWidthHeightCursorcrVSplitAlignalTop  TPanelPanel14LeftTopWidthHeightAlignalTop	AlignmenttaLeftJustify
BevelOuterbvNoneCaption  PromemoriaColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder   TDBGridDBGrid3LeftTopWidth�HeightAlignalLeft
DataSourceData.DsPromemoriaReadOnly	TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameTestoWidthZVisible	 Expanded	FieldNameDataInsTitle.Captioninserito ilVisible	    TPanel
Wallpaper2Left�TopWidthEHeightAlignalClientTabOrder TToolbarButton97ToolbarButton972Left TopWidthDHeight Captionmessaggio lettoOpaqueWordWrap	OnClickToolbarButton972Click    TDataSourceDataSource1DataSetAnag1Left� Top�   TTableAnag1OnCalcFieldsAnag1CalcFieldsDatabaseNameHRFilter"GiornoNascita=22 and MeseNascita=5Filtered		IndexName
IdxCognome	TableNamedbo.AnagraficaLeft� Topp TAutoIncFieldAnag1ID	FieldNameID  TStringFieldAnag1Cognome	FieldNameCognomeSize  TStringField	Anag1Nome	FieldNameNomeSize  TDateTimeFieldAnag1DataNascita	FieldNameDataNascita  TSmallintFieldAnag1GiornoNascita	FieldNameGiornoNascita  TSmallintFieldAnag1MeseNascita	FieldNameMeseNascita  TStringFieldAnag1Eta	FieldKindfkCalculated	FieldNameEtaSize
Calculated	   TTableAnag2OnCalcFieldsAnag2CalcFieldsDatabaseNameHRFilter"GiornoNascita=23 and MeseNascita=5Filtered		IndexName
IdxCognome	TableNamedbo.AnagraficaLeft�Topp TAutoIncFieldAnag2ID	FieldNameID  TStringFieldAnag2Cognome	FieldNameCognomeSize  TStringField	Anag2Nome	FieldNameNomeSize  TDateTimeFieldAnag2DataNascita	FieldNameDataNascita  TSmallintFieldAnag2GiornoNascita	FieldNameGiornoNascita  TSmallintFieldAnag2MeseNascita	FieldNameMeseNascita  TStringFieldAnag2Eta	FieldKindfkCalculated	FieldNameEtaSize
Calculated	   TDataSourceDataSource2DataSetAnag2Left�Top�    