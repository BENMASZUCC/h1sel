unit ArchivioAnn;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Grids, DBGrids, ExtCtrls;

type
  TArchivioAnnForm = class(TForm)
    Panel1: TPanel;
    DBGrid1: TDBGrid;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ArchivioAnnForm: TArchivioAnnForm;

implementation

uses ModuloDati4;

{$R *.DFM}

procedure TArchivioAnnForm.BitBtn2Click(Sender: TObject);
begin
     if MessageDlg('Sei sicuro di voler cancellarlo ?',mtWarning,
        [mbNo,mbYes],0)=mrYes then DataAnnunci.TAnnunci.Delete;
end;

end.
