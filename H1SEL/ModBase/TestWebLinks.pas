unit TestWebLinks;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FrmWebLinks;

type
  TTestWebLinksForm = class(TForm)
    FrameWebLinks1: TFrameWebLinks;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  TestWebLinksForm: TTestWebLinksForm;

implementation

uses ModuloDati;

{$R *.DFM}

procedure TTestWebLinksForm.FormShow(Sender: TObject);
begin
     FrameWebLinks1.xID:=Data.TAnagraficaID.Value;
     FrameWebLinks1.InsRecords;   
     FrameWebLinks1.QWebLinks.Open;
end;

end.
