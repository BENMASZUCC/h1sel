object Data: TData
  OldCreateOrder = False
  Left = 236
  Top = 106
  Height = 540
  Width = 783
  object DB: TDatabase
    AliasName = 'EBCDB'
    Connected = True
    DatabaseName = 'EBCDB'
    LoginPrompt = False
    Params.Strings = (
      'USER NAME=sa'
      'PASSWORD=sa')
    SessionName = 'Default'
    Left = 19
    Top = 25
  end
  object TAnagrafica: TQuery
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select ID,Cognome,Nome'
      'from Anagrafica'
      'where Cognome like :xTesto'
      'order by Cognome,Nome')
    Left = 24
    Top = 96
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'xTesto'
        ParamType = ptUnknown
      end>
    object TAnagraficaID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.Anagrafica.ID'
    end
    object TAnagraficaCognome: TStringField
      FieldName = 'Cognome'
      Origin = 'EBCDB.Anagrafica.Cognome'
      FixedChar = True
      Size = 30
    end
    object TAnagraficaNome: TStringField
      FieldName = 'Nome'
      Origin = 'EBCDB.Anagrafica.Nome'
      FixedChar = True
      Size = 30
    end
  end
  object DsAnagrafica: TDataSource
    DataSet = TAnagrafica
    Left = 24
    Top = 144
  end
  object QAziende: TQuery
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select ID,Descrizione'
      'from EBC_Clienti'
      'where Descrizione like :xTesto'
      'order by Descrizione')
    Left = 112
    Top = 96
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'xTesto'
        ParamType = ptUnknown
      end>
    object QAziendeID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.EBC_Clienti.ID'
    end
    object QAziendeDescrizione: TStringField
      FieldName = 'Descrizione'
      Origin = 'EBCDB.EBC_Clienti.Descrizione'
      FixedChar = True
      Size = 50
    end
  end
  object DsQAziende: TDataSource
    DataSet = QAziende
    Left = 112
    Top = 144
  end
end
