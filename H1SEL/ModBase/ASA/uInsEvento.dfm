object ASAInsEventoForm: TASAInsEventoForm
  Left = 392
  Top = 109
  BorderStyle = bsDialog
  Caption = 'Inserimento evento'
  ClientHeight = 438
  ClientWidth = 411
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 5
    Top = 79
    Width = 87
    Height = 13
    Caption = 'Scelta dell'#39'evento:'
  end
  object Label2: TLabel
    Left = 5
    Top = 261
    Width = 58
    Height = 13
    Caption = 'Descrizione:'
  end
  object Label3: TLabel
    Left = 5
    Top = 57
    Width = 62
    Height = 13
    Caption = 'Data evento:'
  end
  object Label4: TLabel
    Left = 6
    Top = 327
    Width = 340
    Height = 13
    Caption = 
      'Commesse attive ove � inserito il candidato (e dove non � stato ' +
      'escluso)'
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 411
    Height = 44
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 3
    object BitBtn1: TBitBtn
      Left = 219
      Top = 3
      Width = 94
      Height = 38
      TabOrder = 0
      Kind = bkOK
    end
    object BitBtn2: TBitBtn
      Left = 314
      Top = 3
      Width = 94
      Height = 38
      Caption = 'Annulla'
      TabOrder = 1
      Kind = bkCancel
    end
  end
  object dxDBGrid1: TdxDBGrid
    Left = 5
    Top = 94
    Width = 401
    Height = 142
    Bands = <
      item
      end>
    DefaultLayout = True
    HeaderPanelRowCount = 1
    KeyField = 'MaxID'
    SummaryGroups = <>
    SummarySeparator = ', '
    TabOrder = 1
    DataSource = DsQEventi
    Filter.Active = True
    Filter.Criteria = {00000000}
    OptionsBehavior = [edgoAutoSearch, edgoAutoSort, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoTabThrough, edgoVertThrough]
    OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
    OptionsView = [edgoBandHeaderWidth, edgoIndicator, edgoUseBitmap]
    object dxDBGrid1evento: TdxDBGridMaskColumn
      Sorted = csUp
      BandIndex = 0
      RowIndex = 0
      FieldName = 'evento'
    end
    object dxDBGrid1StatoSuccessivo: TdxDBGridMaskColumn
      Caption = 'Stato successivo'
      BandIndex = 0
      RowIndex = 0
      FieldName = 'StatoSuccessivo'
    end
  end
  object EDesc: TEdit
    Left = 5
    Top = 276
    Width = 400
    Height = 21
    TabOrder = 2
  end
  object DeDataEvento: TdxDateEdit
    Left = 74
    Top = 54
    Width = 102
    TabOrder = 0
    Date = -700000
  end
  object CBAssociaCommessa: TCheckBox
    Left = 6
    Top = 306
    Width = 179
    Height = 17
    Caption = 'Associa a commessa/cliente'
    TabOrder = 4
    OnClick = CBAssociaCommessaClick
  end
  object dxDBGrid2: TdxDBGrid
    Left = 5
    Top = 342
    Width = 402
    Height = 89
    Bands = <
      item
      end>
    DefaultLayout = True
    HeaderPanelRowCount = 1
    KeyField = 'ID'
    SummaryGroups = <>
    SummarySeparator = ', '
    Color = clBtnFace
    TabOrder = 5
    DataSource = DsQCommesse
    Filter.Criteria = {00000000}
    OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
    object dxDBGrid2RifCommessa: TdxDBGridMaskColumn
      Caption = 'Rif.commessa'
      Width = 94
      BandIndex = 0
      RowIndex = 0
      FieldName = 'RifCommessa'
    end
    object dxDBGrid2Cliente: TdxDBGridMaskColumn
      Sorted = csUp
      Width = 284
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Cliente'
    end
  end
  object CBModStato: TCheckBox
    Left = 6
    Top = 240
    Width = 259
    Height = 17
    Caption = 'Modifica stato candidato con stato successivo'
    Checked = True
    State = cbChecked
    TabOrder = 6
  end
  object QEventi_Old: TQuery
    SQL.Strings = (
      'select max(ebc_eventi.id) MaxID ,evento, '
      '       AStati.Stato StatoSuccessivo,IDAStato'
      'from ebc_eventi, EBC_Stati AStati'
      'where ebc_eventi.IDAStato = AStati.ID'
      '  and (idastato in (27,28,29,61) or idastato is null)'
      'group by evento, AStati.Stato ,IDAStato'
      'order by evento')
    Left = 88
    Top = 160
    object QEventi_OldMaxID: TIntegerField
      FieldName = 'MaxID'
    end
    object QEventi_Oldevento: TStringField
      FieldName = 'evento'
      FixedChar = True
      Size = 30
    end
    object QEventi_OldStatoSuccessivo: TStringField
      FieldName = 'StatoSuccessivo'
      FixedChar = True
      Size = 30
    end
    object QEventi_OldIDAStato: TIntegerField
      FieldName = 'IDAStato'
    end
  end
  object DsQEventi: TDataSource
    DataSet = QEventi
    Left = 88
    Top = 192
  end
  object QCommesse_Old: TQuery
    DataSource = ASASchedaCandForm.DsQAnag
    SQL.Strings = (
      'select distinct EBC_Ricerche.ID, Progressivo RifCommessa, '
      
        '           EBC_Clienti.ID IDcliente, EBC_Clienti.Descrizione Cli' +
        'ente'
      'from EBC_Ricerche, EBC_CandidatiRicerche, EBC_Clienti'
      'where EBC_Ricerche.IDCliente=EBC_Clienti.ID'
      '  and EBC_Ricerche.ID=EBC_CandidatiRicerche.IDRicerca'
      'and IDStatoRic=1 and escluso=0'
      'and EBC_CandidatiRicerche.IDAnagrafica=:ID')
    Left = 128
    Top = 344
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'ID'
        ParamType = ptUnknown
      end>
    object QCommesse_OldRifCommessa: TStringField
      FieldName = 'RifCommessa'
      Origin = 'EBCDB.EBC_Ricerche.Progressivo'
      FixedChar = True
      Size = 15
    end
    object QCommesse_OldCliente: TStringField
      FieldName = 'Cliente'
      Origin = 'EBCDB.EBC_Clienti.Descrizione'
      FixedChar = True
      Size = 50
    end
    object QCommesse_OldID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.EBC_Ricerche.ID'
    end
    object QCommesse_OldIDCliente: TIntegerField
      FieldName = 'IDCliente'
    end
  end
  object DsQCommesse: TDataSource
    DataSet = QCommesse
    Left = 128
    Top = 375
  end
  object QEventi: TADOLinkedQuery
    Active = True
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select max(ebc_eventi.id) MaxID ,evento, '
      '       AStati.Stato StatoSuccessivo,IDAStato'
      'from ebc_eventi, EBC_Stati AStati'
      'where ebc_eventi.IDAStato = AStati.ID'
      '  and (idastato in (27,28,29,61) or idastato is null)'
      'group by evento, AStati.Stato ,IDAStato'
      'order by evento')
    UseFilter = False
    Left = 168
    Top = 160
    object QEventiMaxID: TIntegerField
      FieldName = 'MaxID'
      ReadOnly = True
    end
    object QEventievento: TStringField
      FieldName = 'evento'
      Size = 30
    end
    object QEventiStatoSuccessivo: TStringField
      FieldName = 'StatoSuccessivo'
      Size = 30
    end
    object QEventiIDAStato: TIntegerField
      FieldName = 'IDAStato'
    end
  end
  object QCommesse: TADOLinkedQuery
    Connection = Data.DB
    DataSource = ASASchedaCandForm.DsQAnag
    Parameters = <
      item
        Name = 'ID'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select distinct EBC_Ricerche.ID, Progressivo RifCommessa, '
      
        '           EBC_Clienti.ID IDcliente, EBC_Clienti.Descrizione Cli' +
        'ente'
      'from EBC_Ricerche, EBC_CandidatiRicerche, EBC_Clienti'
      'where EBC_Ricerche.IDCliente=EBC_Clienti.ID'
      '  and EBC_Ricerche.ID=EBC_CandidatiRicerche.IDRicerca'
      'and IDStatoRic=1 and escluso=0'
      'and EBC_CandidatiRicerche.IDAnagrafica=:ID')
    UseFilter = False
    Left = 200
    Top = 384
    object QCommesseID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QCommesseRifCommessa: TStringField
      FieldName = 'RifCommessa'
      Size = 15
    end
    object QCommesseIDcliente: TAutoIncField
      FieldName = 'IDcliente'
      ReadOnly = True
    end
    object QCommesseCliente: TStringField
      FieldName = 'Cliente'
      Size = 50
    end
  end
end
