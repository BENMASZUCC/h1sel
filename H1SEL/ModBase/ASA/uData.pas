unit uData;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables;

type
  TData = class(TDataModule)
    DB: TDatabase;
    TAnagrafica: TQuery;
    DsAnagrafica: TDataSource;
    TAnagraficaID: TAutoIncField;
    TAnagraficaCognome: TStringField;
    TAnagraficaNome: TStringField;
    QAziende: TQuery;
    DsQAziende: TDataSource;
    QAziendeID: TAutoIncField;
    QAziendeDescrizione: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Data: TData;

implementation

{$R *.DFM}

end.
