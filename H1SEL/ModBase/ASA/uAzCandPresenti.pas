unit uAzCandPresenti;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     dxTL, dxDBCtrl, dxDBGrid, dxCntner, Db, StdCtrls, Buttons, ExtCtrls,
     DBTables, ADODB, U_ADOLinkCl, Menus;

type
     TAzCandPresentiForm = class(TForm)
          QCandAzienda_old: TQuery;
          DsQCandAzienda: TDataSource;
          Panel1: TPanel;
          BitBtn1: TBitBtn;
          QCandAzienda_oldID: TAutoIncField;
          QCandAzienda_oldCVNumero: TIntegerField;
          QCandAzienda_oldCognome: TStringField;
          QCandAzienda_oldNome: TStringField;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1CVNumero: TdxDBGridMaskColumn;
          dxDBGrid1Cognome: TdxDBGridMaskColumn;
          dxDBGrid1Nome: TdxDBGridMaskColumn;
          QCandAzienda: TADOQuery;
          QCandAziendaID: TAutoIncField;
          QCandAziendaCVNumero: TIntegerField;
          QCandAziendaCognome: TStringField;
          QCandAziendaNome: TStringField;
          pmApriScheda: TPopupMenu;
          Aprischedasoggetto1: TMenuItem;
          procedure FormShow(Sender: TObject);
          procedure QCandAziendaBeforeOpen(DataSet: TDataSet);
          procedure Aprischedasoggetto1Click(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     AzCandPresentiForm: TAzCandPresentiForm;

implementation

uses uASAAzienda, ModuloDati, ModuloDati2, uASACand, uUtilsVarie;

{$R *.DFM}

procedure TAzCandPresentiForm.FormShow(Sender: TObject);
begin
     QCandAzienda.Open;
end;

procedure TAzCandPresentiForm.QCandAziendaBeforeOpen(DataSet: TDataSet);
begin
     QCandAzienda.Parameters[0].Value := Data2.TEBCClientiID.Value;
end;

procedure TAzCandPresentiForm.Aprischedasoggetto1Click(Sender: TObject);
begin
     if QCandAzienda.IsEmpty then exit;
     CaricaApriAnag(QCandAziendaCognome.Value, QCandAziendaNome.Value, QCandAziendaID.AsInteger);
     //Data.TAnagrafica.locate('IDAzienda',QAziendaID.Value, []);
     ASASchedaCandForm := TASASchedaCandForm.create(self);
     ASASchedaCandForm.ShowModal;
     ASASchedaCandForm.Free;
end;

end.
