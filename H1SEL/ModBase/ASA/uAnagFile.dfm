object FileSoggettoForm: TFileSoggettoForm
  Left = 330
  Top = 215
  BorderStyle = bsDialog
  Caption = 'File associati al soggetto'
  ClientHeight = 373
  ClientWidth = 594
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel123: TPanel
    Left = 0
    Top = 0
    Width = 594
    Height = 39
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object BAnagFileNew: TToolbarButton97
      Left = 1
      Top = 1
      Width = 76
      Height = 37
      Hint = 'cerca, associa e definisci il '#13#10'tipo per un file gi� esistente'
      Caption = 'Associa nuovo file'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        33333333FF33333333FF333993333333300033377F3333333777333993333333
        300033F77FFF3333377739999993333333333777777F3333333F399999933333
        33003777777333333377333993333333330033377F3333333377333993333333
        3333333773333333333F333333333333330033333333F33333773333333C3333
        330033333337FF3333773333333CC333333333FFFFF77FFF3FF33CCCCCCCCCC3
        993337777777777F77F33CCCCCCCCCC3993337777777777377333333333CC333
        333333333337733333FF3333333C333330003333333733333777333333333333
        3000333333333333377733333333333333333333333333333333}
      NumGlyphs = 2
      Opaque = False
      ParentShowHint = False
      ShowHint = True
      WordWrap = True
      OnClick = BAnagFileNewClick
    end
    object BAnagFileMod: TToolbarButton97
      Left = 77
      Top = 1
      Width = 75
      Height = 37
      Caption = 'Modifica dati'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
        000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
        00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
        F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
        0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
        FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
        FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
        0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
        00333377737FFFFF773333303300000003333337337777777333}
      NumGlyphs = 2
      Opaque = False
      ParentShowHint = False
      ShowHint = True
      WordWrap = True
      OnClick = BAnagFileModClick
    end
    object BAnagFileDel: TToolbarButton97
      Left = 152
      Top = 1
      Width = 75
      Height = 37
      Caption = 'Elimina associaz.'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333000000000
        3333333777777777F3333330F777777033333337F3F3F3F7F3333330F0808070
        33333337F7F7F7F7F3333330F080707033333337F7F7F7F7F3333330F0808070
        33333337F7F7F7F7F3333330F080707033333337F7F7F7F7F3333330F0808070
        333333F7F7F7F7F7F3F33030F080707030333737F7F7F7F7F7333300F0808070
        03333377F7F7F7F773333330F080707033333337F7F7F7F7F333333070707070
        33333337F7F7F7F7FF3333000000000003333377777777777F33330F88877777
        0333337FFFFFFFFF7F3333000000000003333377777777777333333330777033
        3333333337FFF7F3333333333000003333333333377777333333}
      NumGlyphs = 2
      Opaque = False
      WordWrap = True
      OnClick = BAnagFileDelClick
    end
    object BFileCandOpen: TToolbarButton97
      Left = 229
      Top = 1
      Width = 75
      Height = 37
      Caption = 'Apri file'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
        333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
        0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
        07333337F3FF3FFF7F333330F00F000F07333337F77377737F333330FFFFFFFF
        07333FF7F3FFFF3F7FFFBBB0F0000F0F0BB37777F7777373777F3BB0FFFFFFFF
        0BBB3777F3FF3FFF77773330F00F000003333337F773777773333330FFFF0FF0
        33333337F3FF7F37F3333330F08F0F0B33333337F7737F77FF333330FFFF003B
        B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
        3BB33773333773333773B333333B3333333B7333333733333337}
      NumGlyphs = 2
      ParentFont = False
      WordWrap = True
      OnClick = BFileCandOpenClick
    end
    object BitBtn1: TBitBtn
      Left = 495
      Top = 3
      Width = 93
      Height = 34
      TabOrder = 0
      Kind = bkOK
    end
  end
  object dxDBGrid1: TdxDBGrid
    Left = 0
    Top = 39
    Width = 594
    Height = 334
    Bands = <
      item
      end>
    DefaultLayout = True
    HeaderPanelRowCount = 1
    KeyField = 'ID'
    ShowGroupPanel = True
    SummaryGroups = <>
    SummarySeparator = ', '
    Align = alClient
    TabOrder = 1
    DataSource = DsQAnagFile
    Filter.Active = True
    Filter.Criteria = {00000000}
    OptionsBehavior = [edgoAutoSort, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoTabThrough, edgoVertThrough]
    OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
    OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoUseBitmap]
    object dxDBGrid1Tipo: TdxDBGridMaskColumn
      Width = 125
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Tipo'
    end
    object dxDBGrid1Descrizione: TdxDBGridMaskColumn
      Width = 167
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Descrizione'
      DisableFilter = True
    end
    object dxDBGrid1NomeFile: TdxDBGridMaskColumn
      Caption = 'Nome file'
      Width = 275
      BandIndex = 0
      RowIndex = 0
      FieldName = 'NomeFile'
      DisableFilter = True
    end
    object dxDBGrid1DataCreazione: TdxDBGridDateColumn
      Caption = 'Data creazione'
      Width = 89
      BandIndex = 0
      RowIndex = 0
      FieldName = 'DataCreazione'
      DisableFilter = True
    end
  end
  object QAnagFile_Old: TQuery
    SQL.Strings = (
      'select * '
      'from AnagFile,TipiFileCand '
      'where AnagFile.IDTipo *= TipiFileCand.ID'
      'and IDAnagrafica=:ID')
    Left = 64
    Top = 104
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'ID'
        ParamType = ptUnknown
      end>
    object QAnagFile_OldID: TAutoIncField
      FieldName = 'ID'
    end
    object QAnagFile_OldIDAnagrafica: TIntegerField
      FieldName = 'IDAnagrafica'
    end
    object QAnagFile_OldDescrizione: TStringField
      FieldName = 'Descrizione'
      FixedChar = True
      Size = 100
    end
    object QAnagFile_OldNomeFile: TStringField
      FieldName = 'NomeFile'
      FixedChar = True
      Size = 200
    end
    object QAnagFile_OldIDTipo: TIntegerField
      FieldName = 'IDTipo'
    end
    object QAnagFile_OldTipo: TStringField
      FieldName = 'Tipo'
      FixedChar = True
      Size = 30
    end
    object QAnagFile_OldDataCreazione: TDateTimeField
      FieldName = 'DataCreazione'
    end
  end
  object DsQAnagFile: TDataSource
    DataSet = QAnagFile
    Left = 64
    Top = 136
  end
  object QAnagFile: TADOLinkedQuery
    Connection = Data.DB
    Parameters = <
      item
        Name = 'ID'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select * '
      'from AnagFile,TipiFileCand '
      'where AnagFile.IDTipo *= TipiFileCand.ID'
      'and IDAnagrafica=:ID')
    UseFilter = False
    Left = 96
    Top = 104
    object QAnagFileID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QAnagFileIDAnagrafica: TIntegerField
      FieldName = 'IDAnagrafica'
    end
    object QAnagFileDescrizione: TStringField
      FieldName = 'Descrizione'
      Size = 100
    end
    object QAnagFileNomeFile: TStringField
      FieldName = 'NomeFile'
      Size = 200
    end
    object QAnagFileIDTipo: TIntegerField
      FieldName = 'IDTipo'
    end
    object QAnagFileTipo: TStringField
      FieldName = 'Tipo'
      Size = 30
    end
    object QAnagFileDataCreazione: TDateTimeField
      FieldName = 'DataCreazione'
    end
  end
end
