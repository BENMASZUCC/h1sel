unit uASACand;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     dxExEdtr, dxEdLib, dxDBELib, dxCntner, dxEditor, dxLayoutControl,
     cxControls, StdCtrls, Buttons, ComCtrls, dxLayoutLookAndFeels, Db,
     DBTables, ExtCtrls, TB97, dxDBEdtr, dxTL, dxDBCtrl, dxDBGrid, dxDBTLCl,
     dxGrClms, dxLayout, Mask, DBCtrls, FR_DSet, FR_DBSet, FR_Class,
     FR_ADODB, FR_BDEDB, ADODB, U_ADOLinkCl, Menus, DtEdit97, DtEdDB97;

type
     TASASchedaCandForm = class(TForm)
          LcMainGroup_Root: TdxLayoutGroup;
          LcMain: TdxLayoutControl;
          Panel1: TPanel;
          LcMainDatiSoggetto: TdxLayoutGroup;
          dxLayoutLookAndFeelList1: TdxLayoutLookAndFeelList;
          dxLayoutStandardLookAndFeel1: TdxLayoutStandardLookAndFeel;
          QAnag_old: TQuery;
          DsQAnag: TDataSource;
          UpdAnag: TUpdateSQL;
          dxDBEdit1: TdxDBEdit;
          LcMainItem1: TdxLayoutItem;
          LcMainItem2: TdxLayoutItem; dxDBEdit2: TdxDBEdit;
          BCustomize: TToolbarButton97;
          dxLayoutOfficeLookAndFeel1: TdxLayoutOfficeLookAndFeel;
          dxLayoutWebLookAndFeel1: TdxLayoutWebLookAndFeel;
          dxDBEdit3: TdxDBEdit;
          dxDBPickEdit1: TdxDBPickEdit;
          dxDBDateEdit1old: TdxDBDateEdit;
          dxDBEdit5: TdxDBEdit;
          QAnag_oldID: TAutoIncField;
          QAnag_oldCognome: TStringField;
          QAnag_oldNome: TStringField;
          QAnag_oldTitolo: TStringField;
          QAnag_oldDataNascita: TDateTimeField;
          QAnag_oldGiornoNascita: TSmallintField;
          QAnag_oldMeseNascita: TSmallintField;
          QAnag_oldLuogoNascita: TStringField;
          QAnag_oldSesso: TStringField;
          QAnag_oldIndirizzo: TStringField;
          QAnag_oldCap: TStringField;
          QAnag_oldComune: TStringField;
          QAnag_oldIDComuneRes: TIntegerField;
          QAnag_oldIDZonaRes: TIntegerField;
          QAnag_oldProvincia: TStringField;
          QAnag_oldStato: TStringField;
          QAnag_oldDomicilioIndirizzo: TStringField;
          QAnag_oldDomicilioCap: TStringField;
          QAnag_oldDomicilioComune: TStringField;
          QAnag_oldIDComuneDom: TIntegerField;
          QAnag_oldIDZonaDom: TIntegerField;
          QAnag_oldDomicilioProvincia: TStringField;
          QAnag_oldDomicilioStato: TStringField;
          QAnag_oldRecapitiTelefonici: TStringField;
          QAnag_oldCellulare: TStringField;
          QAnag_oldFax: TStringField;
          QAnag_oldCodiceFiscale: TStringField;
          QAnag_oldPartitaIVA: TStringField;
          QAnag_oldEmail: TStringField;
          QAnag_oldIDStatoCivile: TIntegerField;
          QAnag_oldIDStato: TIntegerField;
          QAnag_oldIDTipoStato: TIntegerField;
          QAnag_oldCVNumero: TIntegerField;
          QAnag_oldCVIDAnnData: TIntegerField;
          QAnag_oldCVinseritoInData: TDateTimeField;
          QAnag_oldFoto: TBlobField;
          QAnag_oldIDUtenteMod: TIntegerField;
          QAnag_oldDubbiIns: TStringField;
          QAnag_oldIDProprietaCV: TIntegerField;
          QAnag_oldTelUfficio: TStringField;
          QAnag_oldOldCVNumero: TStringField;
          QAnag_oldTipoStrada: TStringField;
          QAnag_oldDomicilioTipoStrada: TStringField;
          QAnag_oldNumCivico: TStringField;
          QAnag_oldDomicilioNumCivico: TStringField;
          QAnag_oldIDUtente: TIntegerField;
          QAnag_oldIDQualifContr: TIntegerField;
          QAnag_oldTagliaMaglia: TStringField;
          QAnag_oldTagliaPantaloni: TStringField;
          QAnag_oldTagliaScarpe: TStringField;
          QAnag_oldIDAzienda: TIntegerField;
          QAnag_oldTotValutaz: TIntegerField;
          QAnag_oldLibPrivacy: TBooleanField;
          QAnag_oldCVDataReale: TDateTimeField;
          QAnag_oldCVDataAgg: TDateTimeField;
          QAnag_oldDomicilioRegione: TStringField;
          QAnag_oldDomicilioAreaNielsen: TSmallintField;
          QAnag_oldIdTipoStrada: TIntegerField;
          QAnag_oldIdDomicilioTipoStrada: TIntegerField;
          QAnag_oldAziendaPaghe: TIntegerField;
          QAnag_oldIDTipologia: TIntegerField;
          QAnag_oldEta: TStringField;
          LcMainItem3: TdxLayoutItem;
          LcMainItem4: TdxLayoutItem;
          dxDBEdit4: TdxDBEdit;
          LcMainItem5: TdxLayoutItem;
          LcMainGroup3: TdxLayoutGroup;
          LcMainGroup2: TdxLayoutGroup;
          LcMainItem7: TdxLayoutItem;
          QStatiCiviliLK_Old: TQuery;
          QStatiCiviliLK_OldID: TAutoIncField;
          QStatiCiviliLK_OldDescrizione: TStringField;
          QStatiCiviliLK_OldSesso: TStringField;
          QAnag_oldStatoCivile: TStringField;
          dxDBLookupEdit1: TdxDBLookupEdit;
          LcMainItem8: TdxLayoutItem;
          QAnag_oldNumFigli: TStringField;
          dxDBEdit6: TdxDBEdit;
          LcMainItem9: TdxLayoutItem;
          LcMainGroup1: TdxLayoutGroup;
          LcMainGroup4: TdxLayoutGroup;
          LcMainGroup6: TdxLayoutGroup;
          LcMainGroup5: TdxLayoutGroup;
          LcMainItem11: TdxLayoutItem;
          LcMainGroup8: TdxLayoutGroup;
          dxDBEdit7: TdxDBEdit;
          dxDBEdit8: TdxDBEdit;
          dxDBEdit9: TdxDBEdit;
          LcMainItem12: TdxLayoutItem;
          LcMainItem13: TdxLayoutItem;
          dxDBEdit10: TdxDBEdit;
          LcMainItem14: TdxLayoutItem;
          LcMainResidenza: TdxLayoutGroup;
          LcMainItem10: TdxLayoutItem;
          LcMainItem15: TdxLayoutItem;
          LcMainItem16: TdxLayoutItem;
          LcMainGroup7: TdxLayoutGroup;
          LcMainGroup10: TdxLayoutGroup;
          dxDBEdit11: TdxDBEdit;
          dxDBEdit12: TdxDBEdit;
          dxDBEdit13: TdxDBEdit;
          LcMainGroup12: TdxLayoutGroup;
          BDomicilio: TSpeedButton;
          BResidenza: TSpeedButton;
          LcMainItem18: TdxLayoutItem;
          LcMainItem19: TdxLayoutItem;
          LcMainDomicilio: TdxLayoutGroup;
          LcMainGroup14: TdxLayoutGroup;
          LcMainItem20: TdxLayoutItem;
          LcMainItem21: TdxLayoutItem;
          LcMainItem22: TdxLayoutItem;
          LcMainItem23: TdxLayoutItem;
          LcMainGroup15: TdxLayoutGroup;
          LcMainGroup16: TdxLayoutGroup;
          dxDBEdit15: TdxDBEdit;
          dxDBEdit16: TdxDBEdit;
          dxDBEdit17: TdxDBEdit;
          dxDBEdit18: TdxDBEdit;
          QNazioniLK_Old: TQuery;
          QNazioniLK_OldID: TAutoIncField;
          QNazioniLK_OldAbbrev: TStringField;
          QNazioniLK_OldDescNazione: TStringField;
          QNazioniLK_OldAreaGeografica: TStringField;
          QNazioniLK_OldDescNazionalita: TStringField;
          QAnag_oldNazione: TStringField;
          QAnag_oldDomicilioNazione: TStringField;
          dxDBLookupEdit2: TdxDBLookupEdit;
          dxDBLookupEdit3: TdxDBLookupEdit;
          LcMainItem17: TdxLayoutItem;
          LcMainItem24: TdxLayoutItem;
          LcMainGroup9: TdxLayoutGroup;
          LcMainGroup13: TdxLayoutGroup;
          QTipiStatiLK_Old: TQuery;
          QTipiStatiLK_OldID: TAutoIncField;
          QTipiStatiLK_OldTipoStato: TStringField;
          dxDBLookupEdit4: TdxDBLookupEdit;
          LCStatusGroup_Root: TdxLayoutGroup;
          LCStatus: TdxLayoutControl;
          LCStatusItem1: TdxLayoutItem;
          LCStatusItem2: TdxLayoutItem;
          BRelazioni: TSpeedButton;
          QAnag_oldTipoStato: TStringField;
          LcMainGroup11: TdxLayoutGroup;
          LcMainGroup17: TdxLayoutGroup;
          LcMainGroup18: TdxLayoutGroup;
          LcMainGroup19: TdxLayoutGroup;
          LcMainGroup20: TdxLayoutGroup;
          LcMainGroup21: TdxLayoutGroup;
          QAnagLingue_Old: TQuery;
          DsQAnagLingue: TDataSource;
          LcMainItem25: TdxLayoutItem;
          DBGLingue: TdxDBGrid;
          QLingueLK_Old: TQuery;
          QLingueLK_OldID: TAutoIncField;
          QLingueLK_OldLingua: TStringField;
          QAnagLingue_OldID: TAutoIncField;
          QAnagLingue_OldIDAnagrafica: TIntegerField;
          QAnagLingue_OldLingua: TStringField;
          QAnagLingue_OldLivello: TSmallintField;
          QAnagLingue_OldSoggiorno: TStringField;
          QAnagLingue_OldLivelloScritto: TSmallintField;
          QAnagLingue_OldIdLingua: TIntegerField;
          QLivConoscLingueLK_Old: TQuery;
          QLivConoscLingueLK_OldID: TAutoIncField;
          QLivConoscLingueLK_OldLivelloConoscenza: TStringField;
          QAnagLingue_OldLivelloConoscenza: TStringField;
          DBGLingueLinguaTab: TdxDBGridLookupColumn;
          DBGLingueLivelloConoscenza: TdxDBGridLookupColumn;
          LcMainGroup22: TdxLayoutGroup;
          LcMainGroup24: TdxLayoutGroup;
          LcMainGroup25: TdxLayoutGroup;
          LcMainGroup26: TdxLayoutGroup;
          QEspLav_Old: TQuery;
          DsQEspLav: TDataSource;
          QEspLav_OldAnnoDal: TSmallintField;
          QEspLav_OldAnnoAl: TSmallintField;
          QEspLav_OldRetribNettaMensile: TIntegerField;
          QEspLav_OldTitoloMansione: TStringField;
          QEspLav_OldCliente: TStringField;
          dxDBGridLayoutList1: TdxDBGridLayoutList;
          DBGEspLav: TdxDBGrid;
          QEspLav_OldID: TAutoIncField;
          DBGEspLavAnnoDal: TdxDBGridMaskColumn;
          DBGEspLavAnnoAl: TdxDBGridMaskColumn;
          DBGEspLavRetribNettaMensile: TdxDBGridMaskColumn;
          DBGEspLavTitoloMansione: TdxDBGridMaskColumn;
          DBGEspLavCliente: TdxDBGridMaskColumn;
          LcMainItem26: TdxLayoutItem;
          QEspLav_OldAttuale: TBooleanField;
          QAnagNoteRic_Old: TQuery;
          DsQAnagNoteRic: TDataSource;
          LcMainGroup23: TdxLayoutGroup;
          LcMainItem28: TdxLayoutItem;
          DBGContatti: TdxDBGrid;
          DBGContattiData: TdxDBGridDateColumn;
          DBGContattiNote: TdxDBGridMaskColumn;
          DBGContattiCliente: TdxDBGridMaskColumn;
          LcMainItem29: TdxLayoutItem;
          LcMainItem30: TdxLayoutItem;
          LcMainItem31: TdxLayoutItem;
          LcMainItem32: TdxLayoutItem;
          LcMainItem33: TdxLayoutItem;
          LcMainItem34: TdxLayoutItem;
          LcMainItem35: TdxLayoutItem;
          LcMainItem36: TdxLayoutItem;
          LcMainItem37: TdxLayoutItem;
          LcMainItem38: TdxLayoutItem;
          LcMainItem39: TdxLayoutItem;
          LcMainGroup28: TdxLayoutGroup;
          LcMainGroup29: TdxLayoutGroup;
          BNuovo: TToolbarButton97;
          BModCand: TToolbarButton97;
          BEliminaCand: TToolbarButton97;
          BVediCv: TToolbarButton97;
          BPrec: TToolbarButton97;
          BSucc: TToolbarButton97;
          BOK: TToolbarButton97;
          BCan: TToolbarButton97;
          QAziendaAttuale_Old: TQuery;
          DsQAziendaAttuale: TDataSource;
          QAziendaAttuale_OldID: TAutoIncField;
          QAziendaAttuale_OldAzienda: TStringField;
          QAziendaAttuale_OldIndirizzo: TStringField;
          QAziendaAttuale_OldCap: TStringField;
          QAziendaAttuale_OldComune: TStringField;
          QAziendaAttuale_OldProvincia: TStringField;
          dxDBEdit14: TdxDBEdit;
          dxDBEdit19: TdxDBEdit;
          dxDBEdit20: TdxDBEdit;
          dxDBEdit21: TdxDBEdit;
          dxDBEdit22: TdxDBEdit;
          dxDBEdit23: TdxDBEdit;
          dxDBEdit24: TdxDBEdit;
          QDiploma_Old: TQuery;
          DsQDiploma: TDataSource;
          QDiploma_OldID: TAutoIncField;
          QDiploma_OldIDDiploma: TIntegerField;
          QDiploma_OldTesiTitoloArgomento: TMemoField;
          QDiplomiLK_Old: TQuery;
          QDiplomiLK_OldID: TAutoIncField;
          QDiplomiLK_OldDescrizione: TStringField;
          QDiploma_OldDiploma: TStringField;
          dxDBLookupEdit5: TdxDBLookupEdit;
          LcMainItem40: TdxLayoutItem;
          dxDBMemo1: TdxDBMemo;
          LcMainItem41: TdxLayoutItem;
          UpdDiploma: TUpdateSQL;
          QLaurea_Old: TQuery;
          DsQLaurea: TDataSource;
          UpdLaurea: TUpdateSQL;
          QLaurea_OldID: TAutoIncField;
          QLaurea_OldIDDiploma: TIntegerField;
          QLaurea_OldTesiTitoloArgomento: TMemoField;
          QLaurea_OldDiploma: TStringField;
          dxDBLookupEdit6: TdxDBLookupEdit;
          LcMainItem42: TdxLayoutItem;
          dxDBMemo2: TdxDBMemo;
          LcMainItem43: TdxLayoutItem;
          QMaster_Old: TQuery;
          DsQMaster: TDataSource;
          UpdMaster: TUpdateSQL;
          QMaster_OldID: TAutoIncField;
          QMaster_OldArgomenti: TMemoField;
          dxDBMemo3: TdxDBMemo;
          LcMainItem44: TdxLayoutItem;
          QAnagAltreInfo_Old: TQuery;
          DsQAnagAltreInfo: TDataSource;
          dxDBEdit25: TdxDBEdit;
          LcMainItem45: TdxLayoutItem;
          UpdAltreInfo: TUpdateSQL;
          Panel2: TPanel;
          LcMainItem46: TdxLayoutItem;
          LcMainGroup30: TdxLayoutGroup;
          BDiplomaOK: TToolbarButton97;
          BDiplomaCan: TToolbarButton97;
          Panel3: TPanel;
          LcMainItem47: TdxLayoutItem;
          LcMainGroup31: TdxLayoutGroup;
          BLaureaOK: TToolbarButton97;
          BLaureaCan: TToolbarButton97;
          QDiploma_OldIDAnagrafica: TIntegerField;
          QLaurea_OldIDAnagrafica: TIntegerField;
          QDiploma_OldTipo: TStringField;
          QLaurea_OldTipo: TStringField;
          Panel4: TPanel;
          LcMainItem48: TdxLayoutItem;
          BAbilitazOK: TToolbarButton97;
          BAbilitazCan: TToolbarButton97;
          QLaureeLK_Old: TQuery;
          AutoIncField1: TAutoIncField;
          StringField1: TStringField;
          QAnag_oldCellulareUfficio: TStringField;
          QAnag_oldEmailUfficio: TStringField;
          QMaster_OldTipologia: TStringField;
          QMaster_OldIDAnagrafica: TIntegerField;
          Panel5: TPanel;
          LcMainItem49: TdxLayoutItem;
          BMasterOK: TToolbarButton97;
          BMasterCan: TToolbarButton97;
          BFile: TToolbarButton97;
          LcMainGroup32: TdxLayoutGroup;
          Panel6: TPanel;
          LcMainItem50: TdxLayoutItem;
          BLinguaOK: TToolbarButton97;
          BLinguaCan: TToolbarButton97;
          BLinguaNew: TToolbarButton97;
          BLinguaDel: TToolbarButton97;
          Panel7: TPanel;
          LcMainItem52: TdxLayoutItem;
          BEspLavDett: TSpeedButton;
          BEspLavNew: TSpeedButton;
          BEspLavDel: TSpeedButton;
          QTemp_Old: TQuery;
          QDatiMod_Old: TQuery;
          DsQDatiMod: TDataSource;
          DBEdit3: TDBEdit;
          DBEdit4: TDBEdit;
          DBEdit5: TDBEdit;
          DBEdit6: TDBEdit;
          QDatiIns_Old: TQuery;
          DsQDatiIns: TDataSource;
          QDatiIns_OldNominativo: TStringField;
          QDatiIns_OldDataOra: TDateTimeField;
          QDatiMod_OldNominativo: TStringField;
          QDatiMod_OldData: TDateTimeField;
          QEspLavTotValRetribLK_Old: TQuery;
          QEspLavTotValRetribLK_OldIDEspLav: TIntegerField;
          QEspLavTotValRetribLK_OldTotValore: TFloatField;
          QEspLav_OldTotValRetrib: TFloatField;
          DBGEspLavColumn6: TdxDBGridColumn;
          QAziendaAttuale_OldTelefono: TStringField;
          dxDBEdit26: TdxDBEdit;
          LcMainItem27: TdxLayoutItem;
          QAnagNoteRic_OldID: TAutoIncField;
          QAnagNoteRic_OldCliente: TStringField;
          QAnagNoteRic_OldDataUltimoEvento: TDateTimeField;
          Panel8: TPanel;
          LcMainItem51: TdxLayoutItem;
          BDettHistory: TSpeedButton;
          LcMainGroup27: TdxLayoutGroup;
          LcMainGroup34: TdxLayoutGroup;
          SpeedButton1: TSpeedButton;
          Q1_Ols: TQuery;
          QClientiLK_Old: TQuery;
          QClientiLK_OldID: TAutoIncField;
          QClientiLK_OldDescrizione: TStringField;
          dxDBLookupEdit7: TdxDBLookupEdit;
          LcMainItem53: TdxLayoutItem;
          QAnagLingue_OldLinguaTab: TStringField;
          bInsComm: TToolbarButton97;
          QReports_Old: TQuery;
          QCReport_Old: TQuery;
          QCReport_Oldid: TAutoIncField;
          QEspLavTotValRetribLK: TADOLinkedQuery;
          QEspLavTotValRetribLKIDEspLav: TIntegerField;
          QEspLavTotValRetribLKTotValore: TFloatField;
          QDatiIns: TADOLinkedQuery;
          QDatiInsNominativo: TStringField;
          QDatiInsDataOra: TDateTimeField;
          QMaster: TADOLinkedQuery;
          QMasterID: TAutoIncField;
          QMasterArgomenti: TMemoField;
          QMasterIDAnagrafica: TIntegerField;
          QMasterTipologia: TStringField;
          QAnagAltreInfo: TADOLinkedQuery;
          QClientiLK: TADOLinkedQuery;
          QClientiLKID: TAutoIncField;
          QClientiLKDescrizione: TStringField;
          Q1: TADOLinkedQuery;
          QReports: TADOLinkedQuery;
          QCReport: TADOLinkedQuery;
          QCReportid: TAutoIncField;
          QTemp: TADOLinkedQuery;
          QLivConoscLingueLK: TADOLinkedQuery;
          QLingueLK: TADOLinkedQuery;
          QLivConoscLingueLKID: TAutoIncField;
          QLivConoscLingueLKLivelloConoscenza: TStringField;
          QLingueLKID: TAutoIncField;
          QLingueLKLingua: TStringField;
          QAziendaAttuale: TADOLinkedQuery;
          QAziendaAttualeID: TAutoIncField;
          QAziendaAttualeAzienda: TStringField;
          QAziendaAttualeIndirizzo: TStringField;
          QAziendaAttualeCap: TStringField;
          QAziendaAttualeComune: TStringField;
          QAziendaAttualeProvincia: TStringField;
          QAziendaAttualeTelefono: TStringField;
          QTipiStatiLK: TADOLinkedQuery;
          QNazioniLK: TADOLinkedQuery;
          QStatiCiviliLK: TADOLinkedQuery;
          QTipiStatiLKID: TAutoIncField;
          QTipiStatiLKTipoStato: TStringField;
          QNazioniLKID: TAutoIncField;
          QNazioniLKAbbrev: TStringField;
          QNazioniLKDescNazione: TStringField;
          QNazioniLKAreaGeografica: TStringField;
          QNazioniLKDescNazionalita: TStringField;
          QStatiCiviliLKID: TAutoIncField;
          QStatiCiviliLKDescrizione: TStringField;
          QStatiCiviliLKSesso: TStringField;
          QAnagLinked: TADOLinkedQuery;
          QAnagLinkedID: TAutoIncField;
          QAnagLinkedCognome: TStringField;
          QAnagLinkedNome: TStringField;
          QAnagLinkedTitolo: TStringField;
          QAnagLinkedDataNascita: TDateTimeField;
          QAnagLinkedGiornoNascita: TSmallintField;
          QAnagLinkedMeseNascita: TSmallintField;
          QAnagLinkedLuogoNascita: TStringField;
          QAnagLinkedSesso: TStringField;
          QAnagLinkedIndirizzo: TStringField;
          QAnagLinkedCap: TStringField;
          QAnagLinkedComune: TStringField;
          QAnagLinkedIDComuneRes: TIntegerField;
          QAnagLinkedIDZonaRes: TIntegerField;
          QAnagLinkedProvincia: TStringField;
          QAnagLinkedStato: TStringField;
          QAnagLinkedDomicilioIndirizzo: TStringField;
          QAnagLinkedDomicilioCap: TStringField;
          QAnagLinkedDomicilioComune: TStringField;
          QAnagLinkedIDComuneDom: TIntegerField;
          QAnagLinkedIDZonaDom: TIntegerField;
          QAnagLinkedDomicilioProvincia: TStringField;
          QAnagLinkedDomicilioStato: TStringField;
          QAnagLinkedRecapitiTelefonici: TStringField;
          QAnagLinkedCellulare: TStringField;
          QAnagLinkedFax: TStringField;
          QAnagLinkedCodiceFiscale: TStringField;
          QAnagLinkedPartitaIVA: TStringField;
          QAnagLinkedEmail: TStringField;
          QAnagLinkedIDStatoCivile: TIntegerField;
          QAnagLinkedIDStato: TIntegerField;
          QAnagLinkedIDTipoStato: TIntegerField;
          QAnagLinkedCVNumero: TIntegerField;
          QAnagLinkedCVIDAnnData: TIntegerField;
          QAnagLinkedCVinseritoInData: TDateTimeField;
          QAnagLinkedFoto: TBlobField;
          QAnagLinkedIDUtenteMod: TIntegerField;
          QAnagLinkedDubbiIns: TStringField;
          QAnagLinkedIDProprietaCV: TIntegerField;
          QAnagLinkedTelUfficio: TStringField;
          QAnagLinkedOldCVNumero: TStringField;
          QAnagLinkedTipoStrada: TStringField;
          QAnagLinkedDomicilioTipoStrada: TStringField;
          QAnagLinkedNumCivico: TStringField;
          QAnagLinkedDomicilioNumCivico: TStringField;
          QAnagLinkedIDUtente: TIntegerField;
          QAnagLinkedIDQualifContr: TIntegerField;
          QAnagLinkedTagliaMaglia: TStringField;
          QAnagLinkedTagliaPantaloni: TStringField;
          QAnagLinkedTagliaScarpe: TStringField;
          QAnagLinkedIDAzienda: TIntegerField;
          QAnagLinkedTotValutaz: TIntegerField;
          QAnagLinkedLibPrivacy: TBooleanField;
          QAnagLinkedCVDataReale: TDateTimeField;
          QAnagLinkedCVDataAgg: TDateTimeField;
          QAnagLinkedDomicilioRegione: TStringField;
          QAnagLinkedDomicilioAreaNielsen: TSmallintField;
          QAnagLinkedIdTipoStrada: TIntegerField;
          QAnagLinkedIdDomicilioTipoStrada: TIntegerField;
          QAnagLinkedAziendaPaghe: TIntegerField;
          QAnagLinkedIDTipologia: TIntegerField;
          QAnagLinkedNumFigli: TStringField;
          QAnagLinkedCellulareUfficio: TStringField;
          QAnagLinkedEmailUfficio: TStringField;
          QAnagLinkedIDTransito: TIntegerField;
          QAnagLinkedCostoOrarioDipFormaz: TFloatField;
          QAnagLinkedPAGAM_Intestatario: TStringField;
          QAnagLinkedPAGAM_ABI: TStringField;
          QAnagLinkedPAGAM_CAB: TStringField;
          QAnagLinkedPAGAM_NumeroCC: TStringField;
          QAnagLinkedPAGAM_ModPagam: TStringField;
          QAnagLinkedPAGAM_IDBanca: TIntegerField;
          QAnagLinkedInteressanteInternet: TBooleanField;
          QAnagLinkedguid: TGuidField;
          QAnagLinkedidstabilimento: TIntegerField;
          QAnagLinkedDipDiretto: TBooleanField;
          QAnagLinkedVIPList: TBooleanField;
          QAnagLinkedEta: TStringField;
          QAnagLinkedStatoCivile: TStringField;
          QAnagLinkedNazione: TStringField;
          QAnagLinkedDomicilioNazione: TStringField;
          QAnagLinkedTipoStato: TStringField;
          QAnagLinkedCliente: TStringField;
          QDiploma: TADOQuery;
          QDiplomaID: TAutoIncField;
          QDiplomaIDDiploma: TIntegerField;
          QDiplomaTesiTitoloArgomento: TMemoField;
          QDiplomaIDAnagrafica: TIntegerField;
          QDiplomaTipo: TStringField;
          QDiplomaDiploma: TStringField;
          QLaurea: TADOQuery;
          QLaureaID: TAutoIncField;
          QLaureaIDDiploma: TIntegerField;
          QLaureaTesiTitoloArgomento: TMemoField;
          QLaureaIDAnagrafica: TIntegerField;
          QLaureaTipo: TStringField;
          QLaureaDiploma: TStringField;
          QAnagLingue: TADOQuery;
          QAnagLingueID: TAutoIncField;
          QAnagLingueIDAnagrafica: TIntegerField;
          QAnagLingueLingua: TStringField;
          QAnagLingueLivello: TSmallintField;
          QAnagLingueSoggiorno: TStringField;
          QAnagLingueLivelloScritto: TSmallintField;
          QAnagLingueIdLingua: TIntegerField;
          QAnagLingueLivelloConoscenza: TStringField;
          QAnagLingueLinguaTab: TStringField;
          QEspLav: TADOQuery;
          QEspLavID: TAutoIncField;
          QEspLavAnnoDal: TSmallintField;
          QEspLavAnnoAl: TSmallintField;
          QEspLavRetribNettaMensile: TIntegerField;
          QEspLavTitoloMansione: TStringField;
          QEspLavAttuale: TBooleanField;
          QEspLavCliente: TStringField;
          QEspLavTotValRetrib: TFloatField;
          qDatiMod: TADOQuery;
          qDatiModNominativo: TStringField;
          qDatiModData: TDateTimeField;
          qAnag: TADOQuery;
          qAnagID: TAutoIncField;
          qAnagCognome: TStringField;
          qAnagNome: TStringField;
          qAnagTitolo: TStringField;
          qAnagDataNascita: TDateTimeField;
          qAnagGiornoNascita: TSmallintField;
          qAnagMeseNascita: TSmallintField;
          qAnagLuogoNascita: TStringField;
          qAnagSesso: TStringField;
          qAnagIndirizzo: TStringField;
          qAnagCap: TStringField;
          qAnagComune: TStringField;
          qAnagIDComuneRes: TIntegerField;
          qAnagIDZonaRes: TIntegerField;
          qAnagProvincia: TStringField;
          qAnagStato: TStringField;
          qAnagDomicilioIndirizzo: TStringField;
          qAnagDomicilioCap: TStringField;
          qAnagDomicilioComune: TStringField;
          qAnagIDComuneDom: TIntegerField;
          qAnagIDZonaDom: TIntegerField;
          qAnagDomicilioProvincia: TStringField;
          qAnagDomicilioStato: TStringField;
          qAnagRecapitiTelefonici: TStringField;
          qAnagCellulare: TStringField;
          qAnagFax: TStringField;
          qAnagCodiceFiscale: TStringField;
          qAnagPartitaIVA: TStringField;
          qAnagEmail: TStringField;
          qAnagIDStatoCivile: TIntegerField;
          qAnagIDStato: TIntegerField;
          qAnagIDTipoStato: TIntegerField;
          qAnagCVNumero: TIntegerField;
          qAnagCVIDAnnData: TIntegerField;
          qAnagCVinseritoInData: TDateTimeField;
          qAnagFoto: TBlobField;
          qAnagIDUtenteMod: TIntegerField;
          qAnagDubbiIns: TStringField;
          qAnagIDProprietaCV: TIntegerField;
          qAnagTelUfficio: TStringField;
          qAnagOldCVNumero: TStringField;
          qAnagTipoStrada: TStringField;
          qAnagDomicilioTipoStrada: TStringField;
          qAnagNumCivico: TStringField;
          qAnagDomicilioNumCivico: TStringField;
          qAnagIDUtente: TIntegerField;
          qAnagIDQualifContr: TIntegerField;
          qAnagTagliaMaglia: TStringField;
          qAnagTagliaPantaloni: TStringField;
          qAnagTagliaScarpe: TStringField;
          qAnagIDAzienda: TIntegerField;
          qAnagTotValutaz: TIntegerField;
          qAnagLibPrivacy: TBooleanField;
          qAnagCVDataReale: TDateTimeField;
          qAnagCVDataAgg: TDateTimeField;
          qAnagDomicilioRegione: TStringField;
          qAnagDomicilioAreaNielsen: TSmallintField;
          qAnagIdTipoStrada: TIntegerField;
          qAnagIdDomicilioTipoStrada: TIntegerField;
          qAnagAziendaPaghe: TIntegerField;
          qAnagIDTipologia: TIntegerField;
          qAnagNumFigli: TStringField;
          qAnagCellulareUfficio: TStringField;
          qAnagEmailUfficio: TStringField;
          qAnagIDTransito: TIntegerField;
          qAnagCostoOrarioDipFormaz: TFloatField;
          qAnagInteressanteInternet: TBooleanField;
          qAnagguid: TGuidField;
          qAnagidstabilimento: TIntegerField;
          qAnagDipDiretto: TBooleanField;
          qAnagVIPList: TBooleanField;
          qAnagEta: TStringField;
          qAnagStatoCivile: TStringField;
          qAnagNazione: TStringField;
          qAnagDomicilioNazione: TStringField;
          qAnagTipoStato: TStringField;
          qAnagCliente: TStringField;
          QAnagNoteRic: TADOQuery;
          QAnagNoteRicID: TIntegerField;
          QAnagNoteRicCliente: TStringField;
          QAnagNoteRicDataUltimoEvento: TDateTimeField;
          QDiplomiLK: TADOQuery;
          QDiplomiLKID: TAutoIncField;
          QDiplomiLKDescrizione: TStringField;
          QLaureeLK: TADOQuery;
          QLaureeLKID: TAutoIncField;
          QLaureeLKDescrizione: TStringField;
          pmCancContattoCli: TPopupMenu;
          Eliminacontatto1: TMenuItem;
          dxDBDateEdit1: TDbDateEdit97;
          LcMainItem54: TdxLayoutItem;
          procedure BCustomizeClick(Sender: TObject);
          procedure QAnag_oldCalcFields(DataSet: TDataSet);
          procedure BResidenzaClick(Sender: TObject);
          procedure BDomicilioClick(Sender: TObject);
          procedure QAnag_oldBeforePost(DataSet: TDataSet);
          procedure BModCandClick(Sender: TObject);
          procedure DsQAnagStateChange(Sender: TObject);
          procedure BEliminaCandClick(Sender: TObject);
          procedure BOKClick(Sender: TObject);
          procedure BCanClick(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure BPrecClick(Sender: TObject);
          procedure BSuccClick(Sender: TObject);
          procedure QAnag_oldAfterOpen(DataSet: TDataSet);
          procedure QAnag_oldBeforeClose(DataSet: TDataSet);
          procedure BRelazioniClick(Sender: TObject);
          procedure QAnag_oldAfterPost(DataSet: TDataSet);
          procedure BEspLavDettClick(Sender: TObject);
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
          procedure QAnag_oldAfterScroll(DataSet: TDataSet);
          procedure QDiploma_OldAfterPost(DataSet: TDataSet);
          procedure DsQDiplomaStateChange(Sender: TObject);
          procedure BDiplomaOKClick(Sender: TObject);
          procedure BDiplomaCanClick(Sender: TObject);
          procedure BLaureaOKClick(Sender: TObject);
          procedure BLaureaCanClick(Sender: TObject);
          procedure DsQLaureaStateChange(Sender: TObject);
          procedure QLaurea_OldAfterPost(DataSet: TDataSet);
          procedure QDiploma_OldAfterInsert(DataSet: TDataSet);
          procedure QLaurea_OldAfterInsert(DataSet: TDataSet);
          procedure QMaster_OldAfterPost(DataSet: TDataSet);
          procedure BAbilitazOKClick(Sender: TObject);
          procedure BAbilitazCanClick(Sender: TObject);
          procedure QAnagAltreInfo_OldAfterPost(DataSet: TDataSet);
          procedure DsQAnagAltreInfoStateChange(Sender: TObject);
          procedure QMaster_OldAfterInsert(DataSet: TDataSet);
          procedure BMasterOKClick(Sender: TObject);
          procedure BMasterCanClick(Sender: TObject);
          procedure DsQMasterStateChange(Sender: TObject);
          procedure BNuovoClick(Sender: TObject);
          procedure BVediCvClick(Sender: TObject);
          procedure BFileClick(Sender: TObject);
          procedure DsQAnagLingueStateChange(Sender: TObject);
          procedure QAnagLingue_OldAfterPost(DataSet: TDataSet);
          procedure BLinguaNewClick(Sender: TObject);
          procedure BLinguaDelClick(Sender: TObject);
          procedure BLinguaOKClick(Sender: TObject);
          procedure BLinguaCanClick(Sender: TObject);
          procedure QAnagLingue_OldAfterInsert(DataSet: TDataSet);
          procedure BEspLavNewClick(Sender: TObject);
          procedure BEspLavDelClick(Sender: TObject);
          procedure QAnagLingue_OldAfterEdit(DataSet: TDataSet);
          procedure QDiploma_OldAfterEdit(DataSet: TDataSet);
          procedure QLaurea_OldAfterEdit(DataSet: TDataSet);
          procedure QMaster_OldAfterEdit(DataSet: TDataSet);
          procedure BDettHistoryClick(Sender: TObject);
          procedure SpeedButton1Click(Sender: TObject);
          procedure bInsCommClick(Sender: TObject);
          procedure Button1Click(Sender: TObject);
          procedure QDatiModBeforeOpen(DataSet: TDataSet);
          procedure QDiplomaBeforeOpen(DataSet: TDataSet);
          procedure QAnagLingueBeforeOpen(DataSet: TDataSet);
          procedure QLaureaBeforeOpen(DataSet: TDataSet);
          procedure QEspLavBeforeOpen(DataSet: TDataSet);
          procedure QAnagNoteRicBeforeOpen(DataSet: TDataSet);
          procedure Eliminacontatto1Click(Sender: TObject);
          procedure dxDBDateEdit1oldExit(Sender: TObject);
          procedure dxDBDateEdit1oldKeyPress(Sender: TObject; var Key: Char);
     private
          xAnagLingueIns, xEspLavIns, xDiplomaIns, xLaureaIns, xMasterIns: boolean;
          procedure SalvaAziendaAttuale;
     public
          { Public declarations }
     end;

var
     ASASchedaCandForm: TASASchedaCandForm;

implementation

uses ModuloDati, uRelazioni, uDettagliCand, Main, uUtilsVarie, uAnagFile,
     uDettHistory, uInsEvento, uAnagFile2, ElencoRicPend, MDRicerche,
     ModuloDati2, FrmDatiRetribuz;

{$R *.DFM}

procedure TASASchedaCandForm.BCustomizeClick(Sender: TObject);
begin
     //lcMain.Customization := True;
   //     QCReport.Close;
   //     QCReport.ParamByName('ID').AsInteger := Data.TAnagraficaID.AsInteger;
   //     QCReport.Open;
   //  frReport2.ShowReport;
end;

procedure TASASchedaCandForm.QAnag_oldCalcFields(DataSet: TDataSet);
var xAnno, xMese, xgiorno: Word;
begin
     if QAnagDataNascita.asString = '' then
          QAnagEta.asString := ''
     else begin
          DecodeDate((Date - QAnagDataNascita.Value), xAnno, xMese, xgiorno);
          // TAnagraficaEta.Value:=StrToInt(copy(xAnno,3,2));
          QAnagEta.Value := copy(IntToStr(xAnno), 3, 2);
     end;
end;

procedure TASASchedaCandForm.BResidenzaClick(Sender: TObject);
begin
     LcMainResidenza.Visible := True;
     LcMainDomicilio.Visible := False;
end;

procedure TASASchedaCandForm.BDomicilioClick(Sender: TObject);
begin
     LcMainResidenza.Visible := False;
     LcMainDomicilio.Visible := True;
end;

procedure TASASchedaCandForm.QAnag_oldBeforePost(DataSet: TDataSet);
begin
     if QAnagIDTipoStato.Value = 1 then QAnagIDStato.Value := 27;
     if QAnagIDTipoStato.Value = 2 then QAnagIDStato.Value := 28;
     if QAnagIDTipoStato.Value = 3 then QAnagIDStato.Value := 29;
     if QAnagIDTipoStato.Value = 5 then QAnagIDStato.Value := 30;
end;

procedure TASASchedaCandForm.BModCandClick(Sender: TObject);
begin
     QAnag.Edit;
end;

procedure TASASchedaCandForm.DsQAnagStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsQAnag.State in [dsEdit, dsInsert];
     BNuovo.Visible := not b;
     BEliminaCand.Visible := not b;
     //BModCand.Visible:=not b;
     //lcMain.enabled:=b;
     BOK.Visible := b;
     BCan.Visible := b;
end;

procedure TASASchedaCandForm.BEliminaCandClick(Sender: TObject);
begin
     MainForm.TbBAnagDelClick(nil);
     Close;
end;

procedure TASASchedaCandForm.BOKClick(Sender: TObject);
begin
     QAnag.Post;
end;

procedure TASASchedaCandForm.BCanClick(Sender: TObject);
begin
     QAnag.Cancel;
end;

procedure TASASchedaCandForm.FormShow(Sender: TObject);
begin
     // posizionati sul soggetto selezionato
     if Data.Tanagrafica.RecordCount > 0 then begin
          QAnag.Close;
          QAnag.Parameters.ParamByName('xIDAnag').Value := Data.TAnagraficaID.Value;
          QAnag.Open;
     end else begin
          BNuovoClick(self);
     end;
     QAziendaAttuale.Open;
     QDiploma.Open;
     QLaurea.Open;
     QMaster.Open;
     QAnagAltreInfo.Open;
     QAnagLingue.Open;
     QDatiMod.Open;
     // pulsanti prec. e succ.
     BPrec.Enabled := not Data.TAnagrafica.BOF;
     BSucc.Enabled := not Data.TAnagrafica.EOF;
end;

procedure TASASchedaCandForm.BPrecClick(Sender: TObject);
begin
     SalvaAziendaAttuale;
     if Data.TAnagrafica.BOF then exit;
     Data.TAnagrafica.Prior;
     QAnag.Close;
     QAnag.Parameters.ParamByName('xIDAnag').Value := Data.TAnagraficaID.Value;
     QAnag.Open;
     BPrec.Enabled := not Data.TAnagrafica.BOF;
     BSucc.Enabled := not Data.TAnagrafica.EOF;
end;

procedure TASASchedaCandForm.BSuccClick(Sender: TObject);
begin
     SalvaAziendaAttuale;
     if Data.TAnagrafica.EOF then exit;
     Data.TAnagrafica.Next;
     QAnag.Close;
     QAnag.Parameters.ParamByName('xIDAnag').Value := Data.TAnagraficaID.Value;
     QAnag.Open;
     BPrec.Enabled := not Data.TAnagrafica.BOF;
     BSucc.Enabled := not Data.TAnagrafica.EOF;
end;

procedure TASASchedaCandForm.QAnag_oldAfterOpen(DataSet: TDataSet);
begin
     // apri tabelle collegate
     QEspLav.Open;
     QAnagLingue.Open;
     QAnagNoteRic.Open;
     QDatiIns.Open;
     QDatiMod.Open;

     QDiploma.Close;
     QDiploma.Open;
     QLaurea.Close;
     QLaurea.Open;
end;

procedure TASASchedaCandForm.QAnag_oldBeforeClose(DataSet: TDataSet);
begin
     QEspLav.Close;
     QAnagLingue.Close;
     QAnagNoteRic.Close;
end;

procedure TASASchedaCandForm.BRelazioniClick(Sender: TObject);
begin
     RelazioniForm := TRelazioniForm.create(self);
     //RelazioniForm.Top:=ASASchedaCandForm.Top+80;
     //RelazioniForm.Left:=LcStatus.Left+BRelazioni.Left+100;
     RelazioniForm.ShowModal;
     RelazioniForm.Free;
end;

procedure TASASchedaCandForm.QAnag_oldAfterPost(DataSet: TDataSet);
begin
     //QAnag.ApplyUpdates;
     //QAnag.CommitUpdates;
     Log_Operation(MainForm.xIDUtenteAttuale, 'Anagrafica', Data.TAnagraficaID.Value, 'U');
     QDatiMod.Close;
     QDatiMod.Open;
end;

procedure TASASchedaCandForm.BEspLavDettClick(Sender: TObject);
var xID: integer;
begin
     if QEspLav.IsEmpty then exit;
     DettagliCandForm := TDettagliCandForm.create(self);
     // DettagliCandForm.QEspLav.Parameters[0].Value := QEspLavID.Value;
     DettagliCandForm.QEspLav.Open;
     DettagliCandForm.ShowModal;
     DettagliCandForm.Free;
     QEspLavTotValRetribLK.Close;
     QEspLavTotValRetribLK.Open;
     xID := QEspLavID.Value;
     QEspLav.Close;
     QEspLav.Open;
     QEspLav.Locate('ID', xID, []);
end;

procedure TASASchedaCandForm.SalvaAziendaAttuale;
begin
     // PER ORA NON SALVA: D� errore di "stack overflow"
     //if DsQAziendaAttuale.state=dsedit then
     //     QAziendaAttuale.Post;
end;

procedure TASASchedaCandForm.FormClose(Sender: TObject;
     var Action: TCloseAction);
begin
     SalvaAziendaAttuale;
end;

procedure TASASchedaCandForm.QAnag_oldAfterScroll(DataSet: TDataSet);
begin
     if DsQDiploma.state = dsEdit then QDiploma.Post;
end;

procedure TASASchedaCandForm.QDiploma_OldAfterPost(DataSet: TDataSet);
var xID: integer;
begin
     //QDiploma.ApplyUpdates;
     //QDiploma.CommitUpdates;
     xID := QDiplomaID.Value;
     QDiploma.Close;
     QDiploma.Open;
     if xDiplomaIns then begin
          QDiploma.Last;
          Log_Operation(MainForm.xIDUtenteAttuale, 'TitoliStudio', QDiplomaID.Value, 'I');
     end else begin
          QDiploma.Locate('ID', xID, []);
          Log_Operation(MainForm.xIDUtenteAttuale, 'TitoliStudio', QDiplomaID.Value, 'U');
     end;
     QDatiMod.Close;
     QDatiMod.Open;
end;

procedure TASASchedaCandForm.DsQDiplomaStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsQDiploma.State in [dsEdit, dsInsert];
     BDiplomaOK.Enabled := b;
     BDiplomaCan.Enabled := b;
end;

procedure TASASchedaCandForm.BDiplomaOKClick(Sender: TObject);
begin

     if DsQDiploma.State = dsInsert then begin
          Q1.Close;
          Q1.SQL.Text := 'Insert into titolistudio (idanagrafica,iddiploma,TesiTitoloArgomento) ' +
               'values (:xIDAnag:, :xiddiploma:, :xTesi:)';
          Q1.ParamByName['xIDAnag'] := qAnagID.Value;
          Q1.ParamByName['xIDdiploma'] := QDiplomiLKID.Value;
          Q1.ParamByName['xtesi'] := dxDBMemo1.Text;
          Q1.ExecSQL;
          QDiploma.Close;
          QDiploma.Open;
     end;

     if DsQDiploma.State = dsEdit then begin
          Q1.Close;
          Q1.SQL.Text := 'Update titolistudio set iddiploma=:xIDDiploma:, tesititoloargomento=:xtesititoloargomento: ' +
               'where id = ' + QDiplomaID.AsString;
          Q1.ParamByName['xIDDiploma'] := QDiplomaIDDiploma.Value;
          Q1.ParamByName['xtesititoloargomento'] := dxDBMemo1.Text;
          Q1.ExecSQL;
          QDiploma.Close;

          QDiploma.Open;
     end;
end;

procedure TASASchedaCandForm.BDiplomaCanClick(Sender: TObject);
begin
     QDiploma.Cancel;
end;

procedure TASASchedaCandForm.BLaureaOKClick(Sender: TObject);
begin



     if DsQLaurea.State = dsInsert then begin
          Q1.Close;
          Q1.SQL.Text := 'Insert into titolistudio (idanagrafica,iddiploma,TesiTitoloArgomento) ' +
               'values (:xIDAnag:, :xiddiploma:, :xTesi:)';
          Q1.ParamByName['xIDAnag'] := qAnagID.Value;
          Q1.ParamByName['xIDdiploma'] := QLaureeLKID.Value;
          Q1.ParamByName['xtesi'] := dxDBMemo2.Text;
          Q1.ExecSQL;
          QLaurea.Close;
          QLaurea.Open
     end;

     if DsQLaurea.State = dsEdit then begin
          Q1.Close;
          Q1.SQL.Text := 'Update titolistudio set iddiploma=:xIDDiploma:, tesititoloargomento=:xtesititoloargomento: ' +
               'where id = ' + QLaureaID.AsString;
          Q1.ParamByName['xIDDiploma'] := QLaureaIDDiploma.Value;
          Q1.ParamByName['xtesititoloargomento'] := dxDBMemo2.Text;
          Q1.ExecSQL;
          QLaurea.Close;
          QLaurea.Open;

          QLaurea.Post;
     end;
end;

procedure TASASchedaCandForm.BLaureaCanClick(Sender: TObject);
begin
     QLaurea.Cancel;
end;

procedure TASASchedaCandForm.DsQLaureaStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsQLaurea.State in [dsEdit, dsInsert];
     BLaureaOK.Enabled := b;
     BLaureaCan.Enabled := b;
end;

procedure TASASchedaCandForm.QLaurea_OldAfterPost(DataSet: TDataSet);
var xID: integer;
begin
     //QLaurea.ApplyUpdates;
     //QLaurea.CommitUpdates;
     xID := QLaureaID.Value;
     QLaurea.Close;
     QLaurea.Open;
     if xLaureaIns then begin
          QLaurea.Last;
          Log_Operation(MainForm.xIDUtenteAttuale, 'TitoliStudio', QLaureaID.Value, 'I');
     end else begin
          QLaurea.Locate('ID', xID, []);
          Log_Operation(MainForm.xIDUtenteAttuale, 'TitoliStudio', QLaureaID.Value, 'U');
     end;
     QDatiMod.Close;
     QDatiMod.Open;
end;

procedure TASASchedaCandForm.QDiploma_OldAfterInsert(DataSet: TDataSet);
begin
     QDiplomaIDAnagrafica.Value := QAnagID.Value;
     QDiplomaTipo.Value := 'Diploma';
     xDiplomaIns := True;
end;

procedure TASASchedaCandForm.QLaurea_OldAfterInsert(DataSet: TDataSet);
begin
     QLaureaIDAnagrafica.Value := QAnagID.Value;
     QLaureaTipo.Value := 'Laurea';
     xLaureaIns := True;
end;

procedure TASASchedaCandForm.QMaster_OldAfterPost(DataSet: TDataSet);
var xID: integer;
begin
     //QMaster.ApplyUpdates;
     //QMaster.CommitUpdates;
     xID := QMasterID.Value;
     QMaster.Close;
     QMaster.Open;
     if xMasterIns then begin
          QMaster.Last;
          Log_Operation(MainForm.xIDUtenteAttuale, 'CorsiExtra', QMasterID.Value, 'I');
     end else begin
          QMaster.Locate('ID', xID, []);
          Log_Operation(MainForm.xIDUtenteAttuale, 'CorsiExtra', QMasterID.Value, 'U');
     end;
     QDatiMod.Close;
     QDatiMod.Open;
end;

procedure TASASchedaCandForm.BAbilitazOKClick(Sender: TObject);
begin
     QAnagAltreInfo.Post;
end;

procedure TASASchedaCandForm.BAbilitazCanClick(Sender: TObject);
begin
     QAnagAltreInfo.Cancel;
end;

procedure TASASchedaCandForm.QAnagAltreInfo_OldAfterPost(DataSet: TDataSet);
begin
     //QAnagAltreInfo.ApplyUpdates;
     //QAnagAltreInfo.CommitUpdates;
     Log_Operation(MainForm.xIDUtenteAttuale, 'AnagAltreInfo', Data.TAnagraficaID.value, 'U');
     QDatiMod.Close;
     QDatiMod.Open;
end;

procedure TASASchedaCandForm.DsQAnagAltreInfoStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsQAnagAltreInfo.State in [dsEdit, dsInsert];
     BAbilitazOK.Enabled := b;
     BAbilitazCan.Enabled := b;
end;

procedure TASASchedaCandForm.QMaster_OldAfterInsert(DataSet: TDataSet);
begin
     QMasterIDAnagrafica.Value := QAnagID.Value;
     QMasterTipologia.Value := 'Master';
     xMasterIns := True;
end;

procedure TASASchedaCandForm.BMasterOKClick(Sender: TObject);
begin
     QMaster.Post;
end;

procedure TASASchedaCandForm.BMasterCanClick(Sender: TObject);
begin
     QMaster.Cancel;
end;

procedure TASASchedaCandForm.DsQMasterStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsQMaster.State in [dsEdit, dsInsert];
     BMasterOK.Enabled := b;
     BMasterCan.Enabled := b;
end;

procedure TASASchedaCandForm.BNuovoClick(Sender: TObject);
begin
     MainForm.TbBAnagNewClick(nil);
     QAnag.Close;
     QAnag.Parameters.ParamByName('xIDAnag').Value := Data.TAnagraficaID.Value;
     QAnag.Open;
     QAziendaAttuale.Open;
     QDiploma.Open;
     QLaurea.Open;
     QMaster.Open;
     QAnagAltreInfo.Open;
     // pulsanti prec. e succ.
     BPrec.Enabled := not Data.TAnagrafica.BOF;
     BSucc.Enabled := not Data.TAnagrafica.EOF;
end;

procedure TASASchedaCandForm.BVediCvClick(Sender: TObject);
begin
     //ApriCV(Data.TAnagraficaID.value);
     ApriFileWord(Data.TAnagraficaCVNumero.asString);
end;

procedure TASASchedaCandForm.BFileClick(Sender: TObject);
begin
     FileSoggetto2Form := TFileSoggetto2Form.create(self);
     FileSoggetto2Form.ShowModal;
     FileSoggetto2Form.Free
end;

procedure TASASchedaCandForm.DsQAnagLingueStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsQAnagLingue.State in [dsEdit, dsInsert];
     BLinguaNew.Enabled := not b;
     BLinguaDel.Enabled := not b;
     BLinguaOK.Enabled := b;
     BLinguaCan.Enabled := b;
end;

procedure TASASchedaCandForm.QAnagLingue_OldAfterPost(DataSet: TDataSet);
var xID: integer;
begin
     xID := QAnagLingueID.Value;
     QAnagLingue.Close;
     QAnagLingue.Open;
     if xAnagLingueIns then begin
          QAnagLingue.Last;
          Log_Operation(MainForm.xIDUtenteAttuale, 'LingueConosciute', QAnagLingueID.Value, 'I');
     end else begin
          QAnagLingue.Locate('ID', xID, []);
          Log_Operation(MainForm.xIDUtenteAttuale, 'LingueConosciute', QAnagLingueID.Value, 'U');
     end;
     QDatiMod.Close;
     QDatiMod.Open;
end;

procedure TASASchedaCandForm.BLinguaNewClick(Sender: TObject);
begin
     QAnagLingue.Insert;
end;

procedure TASASchedaCandForm.BLinguaDelClick(Sender: TObject);
begin
     if MessageDlg('Sei sicuro di voler cancellare la lingua selezionata ?', mtWarning, [mbYes, mbNo], 0) = mrNo then exit;
     QAnagLingue.Delete;
end;

procedure TASASchedaCandForm.BLinguaOKClick(Sender: TObject);
begin
     QAnagLingue.Post;
end;

procedure TASASchedaCandForm.BLinguaCanClick(Sender: TObject);
begin
     QAnagLingue.Cancel;
end;

procedure TASASchedaCandForm.QAnagLingue_OldAfterInsert(DataSet: TDataSet);
begin
     QAnagLingueIDAnagrafica.Value := QAnagID.Value;
     xAnagLingueIns := False;
end;

procedure TASASchedaCandForm.BEspLavNewClick(Sender: TObject);
begin
     DettagliCandForm := TDettagliCandForm.create(self);
     DettagliCandForm.QEspLav.Parameters[0].Value := QEspLavID.Value;
     DettagliCandForm.QEspLav.Open;
     // inserimento nuova
     //DettagliCandForm.QEspLav.InsertRecord([QAnagID.value]);
     DettagliCandForm.QEspLav.Insert;
     //DettagliCandForm.DatiRetribuzFrame1.QDatiRetribuz.Close;
     //DettagliCandForm.DatiRetribuzFrame1.QDatiRetribuz.Open;
     //DettagliCandForm.QEspLav.Post;
     DettagliCandForm.ShowModal;
     DettagliCandForm.Free;
     QEspLavTotValRetribLK.Close;
     QEspLavTotValRetribLK.Open;
     QEspLav.Close;
     QEspLav.Open;
end;

procedure TASASchedaCandForm.BEspLavDelClick(Sender: TObject);
begin
     if MessageDlg('Sei sicuro di voler cancellare l''esperienza lavorativa selezionata ?', mtWarning, [mbYes, mbNo], 0) = mrNo then exit;
     // DettagliCandForm := TDettagliCandForm.create(self);
      //DettagliCandForm.QEspLav.Parameters[0].Value := QEspLavID.Value;
      //DettagliCandForm.QEspLav.Open;
      // inserimento nuova
      //DettagliCandForm.QEspLav.Delete;
      ////DettagliCandForm.QEspLav.ApplyUpdates;
      ////DettagliCandForm.QEspLav.CommitUpdates;
      //DettagliCandForm.Free;
     Q1.Close;
     Q1.SQL.Text := 'delete from esperienzelavorative where id = ' + QEsplavID.AsString;
     Q1.ExecSQL;


     QEspLav.Close;
     QEspLav.Open;
     QAziendaAttuale.Close;
     QAziendaAttuale.Open;
end;

procedure TASASchedaCandForm.QAnagLingue_OldAfterEdit(DataSet: TDataSet);
begin
     xAnagLingueIns := False;
end;

procedure TASASchedaCandForm.QDiploma_OldAfterEdit(DataSet: TDataSet);
begin
     xDiplomaIns := False;
end;

procedure TASASchedaCandForm.QLaurea_OldAfterEdit(DataSet: TDataSet);
begin
     xLaureaIns := False;
end;

procedure TASASchedaCandForm.QMaster_OldAfterEdit(DataSet: TDataSet);
begin
     xMasterIns := False;
end;

procedure TASASchedaCandForm.BDettHistoryClick(Sender: TObject);
begin
     DettHistoryForm := TDettHistoryForm.create(self);
     with DettHistoryForm do begin
          if QAnagNoteRicID.Value > 0 then
               QDettHistory.SQL.text := 'select EBC_Eventi.Evento, Nominativo, Storico.* ' +
                    'from storico, EBC_Eventi, Users, EBC_Ricerche ' +
                    'where storico.IDEvento = EBC_Eventi.ID ' +
                    '  and storico.IDRicerca = EBC_Ricerche.ID ' +
                    '  and storico.IDUtente = Users.ID ' +
                    ' and EBC_Ricerche.IDCliente=' + QAnagNoteRicID.AsString
          else
               QDettHistory.SQL.text := 'select EBC_Eventi.Evento, Nominativo, Storico.* ' +
                    'from storico, EBC_Eventi, Users ' +
                    'where storico.IDEvento = EBC_Eventi.ID ' +
                    '  and storico.IDUtente = Users.ID ' +
                    '  and (storico.IDRicerca is null or storico.IDRicerca=0)';
          QDettHistory.SQL.Add('and Storico.IDAnagrafica=' + QAnagID.AsString);
          QDettHistory.Open;
     end;
     DettHistoryForm.Caption := 'Dettaglio history - Cliente: ' + QAnagNoteRicCliente.Value;
     DettHistoryForm.ShowModal;
     DettHistoryForm.Free;
     QAnagNoteRic.Close;
     QAnagNoteRic.Open;
end;

procedure TASASchedaCandForm.SpeedButton1Click(Sender: TObject);
var xIDRic, xIDCliente: integer;
begin
     ASAInsEventoForm := TASAInsEventoForm.create(self);
     ASAInsEventoForm.ShowModal;
     if ASAInsEventoForm.ModalResult = mrOK then begin
          if (ASAInsEventoForm.CBAssociaCommessa.Checked) and (not ASAInsEventoForm.QCommesse.IsEmpty) then begin
               xIDRic := ASAInsEventoForm.QCommesseID.Value;
               xIDCliente := ASAInsEventoForm.QCommesseIDCliente.value;
          end else begin
               xIDRic := 0;
               xIDCliente := 0;
          end;
          QTemp.Close;
          QTemp.SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
               'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xIDCliente:)';
          QTemp.ParamByName['xIDAnagrafica'] := QAnagID.Value;
          QTemp.ParamByName['xIDEvento'] := ASAInsEventoForm.QEventiMaxID.Value;
          QTemp.ParamByName['xDataEvento'] := ASAInsEventoForm.DeDataEvento.Date;
          QTemp.ParamByName['xAnnotazioni'] := ASAInsEventoForm.EDesc.text;
          QTemp.ParamByName['xIDRicerca'] := xIDRic;
          QTemp.ParamByName['xIDCliente'] := xIDCliente;
          QTemp.ParamByName['xIDUtente'] := MainForm.xIDUtenteAttuale;
          QTemp.ExecSQL;
          if ASAInsEventoForm.CBModStato.Checked then begin
               Q1.Close;
               Q1.SQL.text := 'select IDTipoStato from EBC_Stati where ID=' + ASAInsEventoForm.QEventiIDAStato.asString;
               Q1.Open;
               QTemp.Close;
               QTemp.SQL.text := 'update anagrafica set IDStato=:xIDStato:, IDTipoStato=:xIDTipoStato: ' +
                    ' where ID=' + QAnagID.AsString;
               Qtemp.ParamByName['xIDStato'] := ASAInsEventoForm.QEventiIDAStato.Value;
               Qtemp.ParamByName['xIDTipoStato'] := Q1.FieldByName('IDTipoStato').asInteger;
               Q1.Close;
               QTemp.ExecSQL;
               QAnag.Close;
               QAnag.Parameters.ParamByName('xIDAnag').Value := Data.TAnagraficaID.Value;
               QAnag.Open;
               QAziendaAttuale.Open;
               QDiploma.Open;
               QLaurea.Open;
               QMaster.Open;
               QAnagAltreInfo.Open;
          end;
          QAnagNoteRic.Close;
          QAnagNoteRic.Open;
     end;
     ASAInsEventoForm.Free;
end;

procedure TASASchedaCandForm.bInsCommClick(Sender: TObject);
var xIDEvento, xIDClienteBlocco, xIDAnagTemp: integer;
     xVai: boolean;
     xRic, xLivProtez, xDicMess, xPassword, xUtenteResp, xClienteBlocco: string;
     QLocal: TQuery;
begin
     ElencoRicPendForm := TElencoRicPendForm.create(self);
     ElencoRicpendform.xall := True;
     ElencoRicpendform.CBElencoRicPend.Visible := True;
     ElencoRicPendForm.CBElencoRicPendClick(nil);
     ElencoRicPendForm.ShowModal;
     DataRicerche.TRicAnagIDX.Open;
     if ElencoRicPendForm.ModalResult = mrOK then begin
          xVai := true;
          // controllo se � gi� associato ad un'altra ricerca per la STESSA azienda
          Data.Q1.Close;
          Data.Q1.SQL.clear;
          Data.Q1.SQl.Add('select EBC_Ricerche.Progressivo,EBC_StatiRic.StatoRic from EBC_CandidatiRicerche,EBC_Ricerche,EBC_StatiRic ');
          Data.Q1.SQl.Add('where EBC_CandidatiRicerche.IDRicerca=EBC_Ricerche.ID and EBC_Ricerche.IDStatoRic=EBC_StatiRic.ID');
          Data.Q1.SQl.Add('and EBC_CandidatiRicerche.IDAnagrafica=:xIDAnag: and EBC_Ricerche.IDCliente=:xIDCliente:');
          //Data.Q1.Prepare;
          Data.Q1.ParamByName['xIDAnag'] := data.TAnagraficaID.Value;
          Data.Q1.ParamByName['xIDCliente'] := ElencoRicPendForm.QRicAttive.FieldByname('IDCliente').Value;
          Data.Q1.Open;
          if not Data.Q1.IsEmpty then begin
               xRic := '';
               while not Data.Q1.EOF do begin xRic := xRic + 'N�' + Data.Q1.FieldByName('Progressivo').asString + ' (' + Data.Q1.FieldByName('StatoRic').asString + ') '; Data.Q1.Next; end;
               if MessageDlg('Il soggetto � associato gi� alle seguenti ricerche per lo stesso cliente: ' + chr(13) + xRic + chr(13) + 'PROCEDERE COMUNQUE ?', mtWarning, [mbYes, mbNo], 0) = mrNo then xVai := False;
          end;
          Data.Q1.Close;

          // controllo incompatibilit�
          if IncompAnagCli(Data.TAnagraficaID.Value, ElencoRicPendForm.QRicAttive.FieldByName('IDCliente').Value) then
               if MessageDlg('Il soggetto risulta INCOMPATIBILE con questo cliente o con un cliente associato' + chr(13) +
                    'PROCEDERE COMUNQUE ?', mtWarning, [mbYes, mbNo], 0) = mrNo then begin
                    DataRicerche.TRicAnagIDX.Close;
                    ElencoRicPendForm.Free;
                    exit;
               end;
          // controllo blocco livello 1 o 2
          {xIDClienteBlocco:=CheckAnagInseritoBlocco1(Data.TAnagraficaID.Value);
          if xIDClienteBlocco>0 then begin
               xLivProtez:=copy(IntToStr(xIDClienteBlocco),1,1);
               if xLivProtez='1' then begin
                    xIDClienteBlocco:=xIDClienteBlocco-10000;
                    xClienteBlocco:=GetDescCliente(xIDClienteBlocco);
                    xDicMess:='Stai inserendo in commessa un candidato appartenente ad una azienda ('+xClienteBlocco+') con blocco di livello 1, ';
               end else begin
                    xIDClienteBlocco:=xIDClienteBlocco-20000;
                    xClienteBlocco:=GetDescCliente(xIDClienteBlocco);
                    xDicMess:='Stai inserendo in commessa un candidato appartenente ad una azienda ATTIVA ('+xClienteBlocco+'), ';
               end;
               if MessageDlg('ATTENZIONE!! PROTEZIONE CLIENTI LIVELLO '+xLivProtez+chr(13)+xDicMess+
                    'protetta dalle normative interne, se sei autorizzato premi SI, altrimenti premi NO. '+
                    'Se si prosegue, l''operazione verr� registrata nello storico del soggetto come forzatura. '+
                    'Verr� inviato un messaggio di promemoria al responsabile diretto',mtWarning, [mbYes,mbNo],0)=mrNo then xVai:=False
               else begin
                    // pwd
                    xUtenteResp:=GetDescUtenteResp(MainForm.xIDUtenteAttuale);
                    if not InputQuery('Forzatura blocco livello '+xLivProtez,'Password di '+xUtenteResp,xPassword) then exit;
                    if not CheckPassword(xPassword,GetPwdUtenteResp(MainForm.xIDUtenteAttuale)) then begin
                         MessageDlg('Password errata',mtError, [mbOK],0);
                         xVai:=False;
                    end;
                    if xVai then begin
                         // promemoria al resp.
                         QLocal:=CreateQueryFromString('insert into Promemoria (IDUtente,IDUtenteDa,DataIns,Testo,Evaso,DataDaLeggere) '+
                              'values (:xIDUtente,:xIDUtenteDa,:xDataIns,:xTesto,:xEvaso,:xDataDaLeggere)');
                         QLocal.ParamByName('xIDUtente').asInteger:=GetIDUtenteResp(MainForm.xIDUtenteAttuale);
                         QLocal.ParamByName('xIDUtenteDa').asInteger:=MainForm.xIDUtenteAttuale;
                         QLocal.ParamByName('xDataIns').asDateTime:=Date;
                         QLocal.ParamByName('xTesto').asString:='forzatura livello '+xLivProtez+' per CV n� '+Data.TAnagraficaCVNumero.AsString;
                         QLocal.ParamByName('xEvaso').asBoolean:=False;
                         QLocal.ParamByName('xDataDaLeggere').asDateTime:=Date;
                         QLocal.ExecSQL;
                         // registra nello storico soggetto
                         xIDEvento:=GetIDEvento('forzatura Blocco livello '+xLivProtez);
                         if xIDEvento>0 then begin
                              with Data.Q1 do begin
                                   close;
                                   SQL.text:='insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) '+
                                        'values (:xIDAnagrafica,:xIDEvento,:xDataEvento,:xAnnotazioni,:xIDRicerca,:xIDUtente,:xIDCliente)';
                                   ParamByName('xIDAnagrafica').asInteger:=Data.TAnagraficaID.Value;
                                   ParamByName('xIDEvento').asInteger:=xIDevento;
                                   ParamByName('xDataEvento').asDateTime:=Date;
                                   ParamByName('xAnnotazioni').asString:='cliente: '+xClienteBlocco;
                                   ParamByName('xIDRicerca').asInteger:=DataRicerche.TRicerchePendID.Value;
                                   ParamByName('xIDUtente').asInteger:=MainForm.xIDUtenteAttuale;
                                   ParamByName('xIDCliente').asInteger:=xIDClienteBlocco;
                                   ExecSQL;
                              end;
                         end;
                    end;
               end;
          end;}
          if xvai then begin
               if not DataRicerche.TRicAnagIDX.Locate('IDRicerca;IDAnagrafica', varArrayof([ElencoRicPendForm.QRicAttive.FieldByName('ID').Value, Data.TAnagraficaID.Value]), []) then
                    Data.DB.BeginTrans;
               try


                    Data.Q1.Close;
                    Data.Q1.SQL.text := 'insert into EBC_CandidatiRicerche (IDRicerca,IDAnagrafica,Escluso,Stato,DataIns) ' +
                         'values (:xIDRicerca:,:xIDAnagrafica:,:xEscluso:,:xStato:,:xDataIns:)';
                    Data.Q1.ParambyName['xIDRicerca'] := ElencoRicPendForm.QRicAttive.FieldByName('ID').Value;
                    Data.Q1.ParambyName['xIDAnagrafica'] := Data.TAnagraficaID.Value;
                    Data.Q1.ParambyName['xEscluso'] := False;
                    Data.Q1.ParambyName['xStato'] := 'inserito direttam. in data ' + DateToStr(Date);
                    Data.Q1.ParambyName['xDataIns'] := Date;
                    Data.Q1.ExecSQL;
                    // storico
                    Data.Q1.SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
                         'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xIDCliente:)';
                    Data.Q1.ParambyName['xIDAnagrafica'] := Data.TAnagraficaID.Value;
                    Data.Q1.ParambyName['xIDEvento'] := 58;
                    Data.Q1.ParambyName['xDataEvento'] := Date;
                    Data.Q1.ParambyName['xAnnotazioni'] := 'inserito nella ric.' + ElencoRicPendForm.QRicAttive.FieldByname('Progressivo').Value + ' (' + ElencoRicPendForm.QRicAttive.FieldByName('Cliente').value + ')';
                    Data.Q1.ParambyName['xIDRicerca'] := ElencoRicPendForm.QRicAttive.FieldByName('ID').Value;
                    Data.Q1.ParambyName['xIDUtente'] := MainForm.xIDUtenteAttuale;
                    Data.Q1.ParambyName['xIDCliente'] := ElencoRicPendForm.QRicAttive.FieldByName('IDCliente').Value;
                    Data.Q1.ExecSQL;
                    // Anagrafica: cambio stato
                    Data.Q1.SQL.text := 'update Anagrafica set IDTipoStato=1,IDStato=27 where ID=' + Data.TAnagraficaID.asString;
                    Data.Q1.ExecSQL;
                    Data.DB.CommitTrans;
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE:  operazione non completata', mtError, [mbOK], 0);
                    raise;
               end;
          end;
     end;
     DataRicerche.TRicAnagIDX.Close;
     Data2.QAnagRicerche.Close;
     Data2.QAnagRicerche.Open;
     xIDAnagTemp := qAnagID.Value;
     qAnag.Close;
     qAnag.open;
     qAnag.Locate('ID', xIDAnagTemp, []);
     ElencoRicPendForm.Free;
end;

procedure TASASchedaCandForm.Button1Click(Sender: TObject);
var xFile: string;
begin
     //frReport1.FileName := Data.global.fieldbyname('DirFileReports').AsString + ' \ASACandidateReport.frf';
     //frreport1.DesignReport;

     {     begin
          //if TV.Selected.level=0 then exit;
               xFile := TrimRight(Data.Global.FieldByName('DirFileReports').AsString) + '\ASACandidateReport.frf';
               if not FileExists(xFile) then begin
                    MessageDlg('FILE NON TROVATO: ' + chr(13) + xFile + chr(13) +
                         'il percorso impostato nel supervisor ' + chr(13) +
                         'potrebbe non essere corretto, oppure il file non esiste', mtError, [mbOK], 0);
                    exit;
               end;
               frReport1.LoadFromFile(xFile);
          end;}
     QCReport.Close;
     QCReport.ParamByName['ID'] := Data.TAnagraficaID.AsInteger;
     QCReport.Open;
    // frReport2.ShowReport;
end;

procedure TASASchedaCandForm.QDatiModBeforeOpen(DataSet: TDataSet);
begin
     QDatiMod.SQL.Text := 'select Nominativo,max(DataOra) Data ' +
          'from Log_TableOp, Users ' +
          'where Log_TableOp.IDUtente = Users.ID ' +
          'and Tabella=''Anagrafica'' and Keyvalue=' + Data.TAnagraficaID.AsString +
          ' and Operation='' U '' ' +
          'group by Nominativo ' +
          'union ' +
          'select Nominativo,max(DataOra) Data ' +
          'from anagrafica,AnagAltreInfo,Log_TableOp, Users ' +
          'where Log_TableOp.IDUtente = Users.ID ' +
          '  and anagrafica.ID = AnagAltreInfo.IDAnagrafica ' +
          '  and Tabella=''AnagAltreInfo'' and KeyValue=AnagAltreInfo.IDAnagrafica ' +
          'and Anagrafica.ID=' + Data.TAnagraficaID.AsString +
          ' group by Nominativo ' +
          'union ' +
          'select Nominativo,max(DataOra) Data ' +
          'from anagrafica,EsperienzeLavorative,Log_TableOp, Users ' +
          'where Log_TableOp.IDUtente = Users.ID ' +
          '  and anagrafica.ID = EsperienzeLavorative.IDAnagrafica ' +
          '  and Tabella=''EsperienzeLavorative'' and KeyValue=EsperienzeLavorative.ID ' +
          'and Anagrafica.ID=' + Data.TAnagraficaID.AsString +
          ' group by Nominativo ' +
          'union ' +
          'select Nominativo,max(DataOra) Data ' +
          'from anagrafica,LingueConosciute,Log_TableOp, Users ' +
          'where Log_TableOp.IDUtente = Users.ID ' +
          '  and anagrafica.ID = LingueConosciute.IDAnagrafica ' +
          '  and Tabella=''LingueConosciute'' and KeyValue=LingueConosciute.ID ' +
          'and Anagrafica.ID=' + Data.TAnagraficaID.AsString +
          ' group by Nominativo ' +
          'union ' +
          'select Nominativo,max(DataOra) Data ' +
          'from anagrafica,AnagMansioni,Log_TableOp, Users ' +
          'where Log_TableOp.IDUtente = Users.ID ' +
          '  and anagrafica.ID = AnagMansioni.IDAnagrafica ' +
          '  and Tabella=''AnagMansioni'' and KeyValue=AnagMansioni.ID ' +
          'and Anagrafica.ID=' + Data.TAnagraficaID.AsString +
          ' group by Nominativo ' +
          'union ' +
          'select Nominativo,max(DataOra) Data ' +
          'from anagrafica,TitoliStudio,Log_TableOp, users ' +
          'where Log_TableOp.IDUtente = Users.ID ' +
          '  and anagrafica.ID = TitoliStudio.IDAnagrafica ' +
          '  and Tabella=''TitoliStudio'' and KeyValue=TitoliStudio.ID ' +
          'and Anagrafica.ID=' + Data.TAnagraficaID.AsString +
          ' group by Nominativo ' +
          'union ' +
          'select Nominativo,max(DataOra) Data ' +
          'from anagrafica,CorsiExtra,Log_TableOp, Users ' +
          'where Log_TableOp.IDUtente = Users.ID ' +
          'and anagrafica.ID = CorsiExtra.IDAnagrafica ' +
          'and Tabella=''CorsiExtra'' and KeyValue=CorsiExtra.ID ' +
          'and Anagrafica.ID=' + Data.TAnagraficaID.AsString +
          ' group by Nominativo ' +
          'order by Data desc';

end;

procedure TASASchedaCandForm.QDiplomaBeforeOpen(DataSet: TDataSet);
begin
     QDiploma.Parameters[0].value := DAta.TAnagraficaID.value;
end;

procedure TASASchedaCandForm.QAnagLingueBeforeOpen(DataSet: TDataSet);
begin
     QAnagLingue.Parameters[0].value := Data.Tanagraficaid.value;
end;

procedure TASASchedaCandForm.QLaureaBeforeOpen(DataSet: TDataSet);
begin
     QLaurea.Parameters[0].value := DAta.TAnagraficaID.value;
end;

procedure TASASchedaCandForm.QEspLavBeforeOpen(DataSet: TDataSet);
begin
     //  QEspLav.Parameters[0].value := Data.TAnagraficaID.value;
end;


procedure TASASchedaCandForm.QAnagNoteRicBeforeOpen(DataSet: TDataSet);
begin
     QAnagNoteRic.Parameters.ParamByname('xID').value := Data.TAnagraficaID.value;
     QAnagNoteRic.Parameters.ParamByname('xID2').value := Data.TAnagraficaID.value;
end;

procedure TASASchedaCandForm.Eliminacontatto1Click(Sender: TObject);
begin
     if MessageDlg('Eliminare l''associazione al cliente ' + dxDBLookupEdit7.Text + '?',
          mtConfirmation, [mbYes, mbNo], 0) = mrYes then begin
          Q1.Close;
          Q1.SQL.Text := 'update anagrafica set IDazienda = NULL where id = ' + qAnagid.AsString;
          Q1.ExecSQL;

          qAnag.Close;
          qAnag.Open;
     end;
end;

procedure TASASchedaCandForm.dxDBDateEdit1oldExit(Sender: TObject);
begin
     {if length(dxDBDateEdit1.Text) = 8 then
        dxDBDateEdit1.Text := copy(dxDBDateEdit1.Text,1,2)+'/'+copy(dxDBDateEdit1.Text,3,2)+'/'+copy(dxDBDateEdit1.Text,5,4);
        }
end;

procedure TASASchedaCandForm.dxDBDateEdit1oldKeyPress(Sender: TObject;
     var Key: Char);
var xdata: string;
begin
     if Length(dxDBDateEdit1.Text) = 7 then begin
          xdata := copy(dxDBDateEdit1.Text, 1, 2) + '/' + copy(dxDBDateEdit1.Text, 3, 2) + '/' + copy(dxDBDateEdit1.Text, 5, 4) + Key;
          dxDBDateEdit1.Text := xData;
          Key := #0;
     end;

end;

end.
