unit uASAInsRuolo;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ExtCtrls, Grids, DBGrids, Db, DBTables, StdCtrls, Buttons, ADODB;

type
     TInsRuoloAsaForm = class(TForm)
          DsAree: TDataSource;
          Panel73: TPanel;
          Panel1: TPanel;
          BitBtn1: TBitBtn;
          DsRuoli: TDataSource;
          DBGrid2: TDBGrid;
          DBGrid1: TDBGrid;
          CBInsComp: TCheckBox;
          BitBtn2: TBitBtn;
          Timer1: TTimer;
          TAree_old: TQuery;
          TAree_oldID: TAutoIncField;
          TAree_oldDescrizione: TStringField;
          TRuoli_old: TQuery;
          TRuoli_oldID: TAutoIncField;
          TRuoli_oldDescrizione: TStringField;
          TAree_oldDescrizione_ENG: TStringField;
          TRuoli_oldDescrizione_ENG: TStringField;
          BAreeRuoliITA: TSpeedButton;
          BAreeRuoliENG: TSpeedButton;
          TAree: TADOQuery;
          TRuoli: TADOQuery;
          TAreeID: TAutoIncField;
          TAreeDescrizione: TStringField;
          TAreeDescrizione_ENG: TStringField;
          TRuoliID: TAutoIncField;
          TRuoliDescrizione: TStringField;
          TRuoliDescrizione_ENG: TStringField;
          procedure DBGrid1KeyPress(Sender: TObject; var Key: Char);
          procedure BitBtn1Click(Sender: TObject);
          procedure Timer1Timer(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure BAreeRuoliITAClick(Sender: TObject);
          procedure BAreeRuoliENGClick(Sender: TObject);
     private
          xStringa: string;
          { Private declarations }
     public
          { Public declarations }
     end;

var
     InsRuoloAsaForm: TInsRuoloAsaForm;

implementation

uses ModuloDati;

{$R *.DFM}

procedure TInsRuoloAsaForm.DBGrid1KeyPress(Sender: TObject; var Key: Char);
begin
     Timer1.Enabled := False;
     if Key = chr(13) then begin
          BitBtn1.Click;
     end else begin
          xStringa := xStringa + key;
          TAree.Locate('Descrizione', xStringa, [loCaseInsensitive, loPartialKey]);
     end;
     Timer1.Enabled := true;
end;

procedure TInsRuoloAsaForm.BitBtn1Click(Sender: TObject);
begin
     if TRuoli.RecordCount = 0 then begin
          MessageDlg('Nessun ruolo selezionato !', mtError, [mbOK], 0);
          ModalResult := mrNone;
          Abort;
     end;
end;

procedure TInsRuoloAsaForm.Timer1Timer(Sender: TObject);
begin
     xStringa := '';
end;

procedure TInsRuoloAsaForm.FormShow(Sender: TObject);
begin
     Caption := '[S/12] - ' + caption;

end;

procedure TInsRuoloAsaForm.BAreeRuoliITAClick(Sender: TObject);
begin
     DBGrid1.Columns[0].FieldName := 'Descrizione';
     DBGrid2.Columns[0].FieldName := 'Descrizione';
end;

procedure TInsRuoloAsaForm.BAreeRuoliENGClick(Sender: TObject);
begin
     DBGrid1.Columns[0].FieldName := 'Descrizione_ENG';
     DBGrid2.Columns[0].FieldName := 'Descrizione_ENG';
end;

end.
