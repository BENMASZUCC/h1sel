unit uAnagFile2;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, dxDBTLCl, dxGrClms, dxTL, dxDBCtrl, dxDBGrid,
     dxCntner, Db, DBTables, TB97, ExtCtrls, ShellApi, ADODB, U_ADOLinkCl;

type
     TFileSoggetto2Form = class(TForm)
          Panel123: TPanel;
          BFileCandOpen: TToolbarButton97;
          DsQAziendaFile: TDataSource;
          dxDBGrid1: TdxDBGrid;
          BitBtn1: TBitBtn;
          BNew: TToolbarButton97;
          BDel: TToolbarButton97;
          BOK: TToolbarButton97;
          BCan: TToolbarButton97;
          QAziendaFile_Old: TQuery;
          dxDBGrid1Descrizione: TdxDBGridMaskColumn;
          UpdAnagFile: TUpdateSQL;
          dxDBGrid1NomeFile: TdxDBGridButtonColumn;
          OpenDialog1: TOpenDialog;
          QAziendaFile_OldID: TAutoIncField;
          QAziendaFile_OldIDAnagrafica: TIntegerField;
          QAziendaFile_OldIDTipo: TIntegerField;
          QAziendaFile_OldDescrizione: TStringField;
          QAziendaFile_OldNomeFile: TStringField;
          QAziendaFile_OldDataCreazione: TDateTimeField;
          QAziendaFile_OldID_1: TAutoIncField;
          QAziendaFile_OldTipo: TStringField;
          dxDBGrid1Column3: TdxDBGridButtonColumn;
          QAziendaFile_OldSoloNome: TStringField;
          QAziendaFile: TADOQuery;
          QAziendaFileID: TAutoIncField;
          QAziendaFileIDAnagrafica: TIntegerField;
          QAziendaFileIDTipo: TIntegerField;
          QAziendaFileDescrizione: TStringField;
          QAziendaFileNomeFile: TStringField;
          QAziendaFileDataCreazione: TDateTimeField;
          QAziendaFileFlagInvio: TBooleanField;
          QAziendaFileSoloNome: TStringField;
          QAziendaFileID_1: TAutoIncField;
          QAziendaFileTipo: TStringField;
          procedure BFileCandOpenClick(Sender: TObject);
          procedure BNewClick(Sender: TObject);
          procedure BDelClick(Sender: TObject);
          procedure BOKClick(Sender: TObject);
          procedure BCanClick(Sender: TObject);
          procedure DsQAziendaFileStateChange(Sender: TObject);
          procedure QAziendaFile_OldAfterPost(DataSet: TDataSet);
          procedure QAziendaFile_OldAfterInsert(DataSet: TDataSet);
          procedure dxDBGrid1NomeFileButtonClick(Sender: TObject;
               AbsoluteIndex: Integer);
          procedure dxDBGrid1Column3ButtonClick(Sender: TObject;
               AbsoluteIndex: Integer);
          procedure FormShow(Sender: TObject);
          procedure QAziendaFile_OldBeforePost(DataSet: TDataSet);
          procedure QAziendaFileBeforeOpen(DataSet: TDataSet);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     FileSoggetto2Form: TFileSoggetto2Form;

implementation

uses AnagFile, ModuloDati, uASACand, uUtilsVarie, Main, uASAAzienda;

{$R *.DFM}

procedure TFileSoggetto2Form.BFileCandOpenClick(Sender: TObject);
var xFileName: string;
begin
     xFileName := QAziendaFileNomeFile.Value;
     ShellExecute(0, 'Open', pchar(xFileName), '', '', SW_SHOW);
end;

procedure TFileSoggetto2Form.BNewClick(Sender: TObject);
begin
     QAziendaFile.Insert;
end;

procedure TFileSoggetto2Form.BDelClick(Sender: TObject);
begin
     if MessageDlg('Sei sicuro di voler cancellare la riga selezionata ?', mtWarning, [mbYes, mbNo], 0) = mrNo then exit;
     QAziendaFile.Delete;
end;

procedure TFileSoggetto2Form.BOKClick(Sender: TObject);
begin
     QAziendaFile.Post;
end;

procedure TFileSoggetto2Form.BCanClick(Sender: TObject);
begin
     QAziendaFile.Cancel;
end;

procedure TFileSoggetto2Form.DsQAziendaFileStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsQAziendaFile.State in [dsEdit, dsInsert];
     BNew.enabled := not b;
     BDel.enabled := not b;
     BOK.enabled := b;
     BCan.enabled := b;
end;

procedure TFileSoggetto2Form.QAziendaFile_OldAfterPost(DataSet: TDataSet);
begin
     //QAziendaFile.ApplyUpdates;
     //QAziendaFile.CommitUpdates;
     QAziendaFile.Close;
     QAziendaFile.Open;
end;

procedure TFileSoggetto2Form.QAziendaFile_OldAfterInsert(DataSet: TDataSet);
begin
     QAziendaFileIDAnagrafica.Value := ASASchedaCandForm.QAnagID.Value;

end;

procedure TFileSoggetto2Form.dxDBGrid1NomeFileButtonClick(Sender: TObject;
     AbsoluteIndex: Integer);
begin
     if QAziendaFileNomeFile.Value <> '' then
          OpenDialog1.Filename := QAziendaFileNomeFile.Value;
     if OpenDialog1.Execute then begin
          if not (dsQAziendaFile.state = dsEdit) then QAziendaFile.Edit;
          QAziendaFileNomeFile.Value := OpenDialog1.FileName;
     end;
end;

procedure TFileSoggetto2Form.dxDBGrid1Column3ButtonClick(Sender: TObject;
     AbsoluteIndex: Integer);
var xID: string;
begin
     xID := OpenSelFromTab('TipiFileCand', 'ID', 'Tipo', 'Tipo', QAziendaFileIDTipo.asString, True);
     if xID <> '' then begin
          if not (dsQAziendaFile.state = dsEdit) then QAziendaFile.Edit;
          QAziendaFileIDTipo.Value := StrToIntDef(xID, 0);
     end;
end;

procedure TFileSoggetto2Form.FormShow(Sender: TObject);
begin
     if QAziendaFile.Active = true then
          QAziendaFile.Close;
     QAziendaFile.Open;
end;

procedure TFileSoggetto2Form.QAziendaFile_OldBeforePost(DataSet: TDataSet);
begin
     QAziendaFileSoloNome.Value := ExtractFileName(QAziendaFileNomeFile.AsString);
     QAziendaFileDataCreazione.Value := Date;
end;

procedure TFileSoggetto2Form.QAziendaFileBeforeOpen(DataSet: TDataSet);
begin
     QAziendaFile.Parameters[0].Value := Data.TAnagraficaID.Value;
end;

end.
