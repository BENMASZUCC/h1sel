unit uDettHistory;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, ExtCtrls, dxDBTLCl, dxGrClms, dxTL, dxDBCtrl,
     dxDBGrid, dxCntner, Db, DBTables, TB97, ADODB, U_ADOLinkCl;

type
     TDettHistoryForm = class(TForm)
          Panel1: TPanel;
          BitBtn1: TBitBtn;
          QDettHistory_Old: TQuery;
          DsQDettHistory: TDataSource;
          QDettHistory_OldEvento: TStringField;
          QDettHistory_OldNominativo: TStringField;
          QDettHistory_OldID: TAutoIncField;
          QDettHistory_OldIDAnagrafica: TIntegerField;
          QDettHistory_OldIDEvento: TIntegerField;
          QDettHistory_OldDataEvento: TDateTimeField;
          QDettHistory_OldAnnotazioni: TStringField;
          QDettHistory_OldIDRicerca: TIntegerField;
          QDettHistory_OldIDUtente: TIntegerField;
          QDettHistory_OldIDCliente: TIntegerField;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1Evento: TdxDBGridMaskColumn;
          dxDBGrid1Nominativo: TdxDBGridMaskColumn;
          dxDBGrid1DataEvento: TdxDBGridDateColumn;
          dxDBGrid1Annotazioni: TdxDBGridMaskColumn;
          BEliminaRiga: TToolbarButton97;
          QDettHistory: TADOLinkedQuery;
          QDettHistoryEvento: TStringField;
          QDettHistoryNominativo: TStringField;
          QDettHistoryID: TAutoIncField;
          QDettHistoryIDAnagrafica: TIntegerField;
          QDettHistoryIDEvento: TIntegerField;
          QDettHistoryDataEvento: TDateTimeField;
          QDettHistoryAnnotazioni: TStringField;
          QDettHistoryIDRicerca: TIntegerField;
          QDettHistoryIDUtente: TIntegerField;
          QDettHistoryIDCliente: TIntegerField;
          procedure BEliminaRigaClick(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     DettHistoryForm: TDettHistoryForm;

implementation

uses ModuloDati;

{$R *.DFM}

procedure TDettHistoryForm.BEliminaRigaClick(Sender: TObject);
begin
     if MessageDlg('Sei sicuro di voler cancellare la riga selezionata ?', mtWarning, [mbYes, mbNo], 0) = mrNo then exit;
     Data.Q1.Close;
     Data.Q1.SQL.text := 'delete from storico where id=' + QDettHistoryID.AsString;
     Data.Q1.ExecSQL;
     QDettHistory.Close;
     QDettHistory.Open;
end;

end.
