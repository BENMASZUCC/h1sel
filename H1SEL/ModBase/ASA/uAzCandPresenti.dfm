object AzCandPresentiForm: TAzCandPresentiForm
  Left = 339
  Top = 125
  Width = 411
  Height = 446
  Caption = 'Candidati presenti in azienda'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 403
    Height = 47
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object BitBtn1: TBitBtn
      Left = 293
      Top = 4
      Width = 106
      Height = 39
      Anchors = [akTop, akRight]
      Caption = 'Esci'
      TabOrder = 0
      Kind = bkOK
    end
  end
  object dxDBGrid1: TdxDBGrid
    Left = 0
    Top = 47
    Width = 403
    Height = 372
    Bands = <
      item
      end>
    DefaultLayout = True
    HeaderPanelRowCount = 1
    KeyField = 'ID'
    SummaryGroups = <>
    SummarySeparator = ', '
    Align = alClient
    PopupMenu = pmApriScheda
    TabOrder = 1
    DataSource = DsQCandAzienda
    Filter.Criteria = {00000000}
    OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
    OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoUseBitmap]
    object dxDBGrid1CVNumero: TdxDBGridMaskColumn
      Caption = 'N� CV'
      DisableEditor = True
      Width = 66
      BandIndex = 0
      RowIndex = 0
      FieldName = 'CVNumero'
    end
    object dxDBGrid1Cognome: TdxDBGridMaskColumn
      DisableEditor = True
      Sorted = csUp
      Width = 147
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Cognome'
    end
    object dxDBGrid1Nome: TdxDBGridMaskColumn
      DisableEditor = True
      Width = 147
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Nome'
    end
  end
  object QCandAzienda_old: TQuery
    DatabaseName = 'EBCDB'
    DataSource = ASAAziendaForm.DsQAzienda
    SQL.Strings = (
      'select Anagrafica.ID, CVNumero,Cognome,Nome'
      'from EsperienzeLavorative, Anagrafica'
      'where EsperienzeLavorative.IDAnagrafica = Anagrafica.ID'
      'and EsperienzeLavorative.IDAzienda=:ID'
      'and Attuale=1')
    Left = 32
    Top = 168
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'ID'
        ParamType = ptUnknown
      end>
    object QCandAzienda_oldID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.Anagrafica.ID'
    end
    object QCandAzienda_oldCVNumero: TIntegerField
      FieldName = 'CVNumero'
      Origin = 'EBCDB.Anagrafica.CVNumero'
    end
    object QCandAzienda_oldCognome: TStringField
      FieldName = 'Cognome'
      Origin = 'EBCDB.Anagrafica.Cognome'
      FixedChar = True
      Size = 30
    end
    object QCandAzienda_oldNome: TStringField
      FieldName = 'Nome'
      Origin = 'EBCDB.Anagrafica.Nome'
      FixedChar = True
      Size = 30
    end
  end
  object DsQCandAzienda: TDataSource
    DataSet = QCandAzienda
    Left = 32
    Top = 200
  end
  object QCandAzienda: TADOQuery
    Connection = Data.DB
    BeforeOpen = QCandAziendaBeforeOpen
    Parameters = <
      item
        Name = 'xID'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select Anagrafica.ID, CVNumero,Cognome,Nome'
      'from EsperienzeLavorative, Anagrafica'
      'where EsperienzeLavorative.IDAnagrafica = Anagrafica.ID'
      'and EsperienzeLavorative.IDAzienda= :xID'
      'and Attuale=1')
    Left = 32
    Top = 120
    object QCandAziendaID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QCandAziendaCVNumero: TIntegerField
      FieldName = 'CVNumero'
    end
    object QCandAziendaCognome: TStringField
      FieldName = 'Cognome'
      Size = 30
    end
    object QCandAziendaNome: TStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object pmApriScheda: TPopupMenu
    Left = 88
    Top = 216
    object Aprischedasoggetto1: TMenuItem
      Caption = 'Apri scheda soggetto'
      OnClick = Aprischedasoggetto1Click
    end
  end
end
