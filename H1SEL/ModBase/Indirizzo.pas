//DEBUGOK
unit Indirizzo;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, TB97, ExtCtrls;

type
     TIndirizzoForm = class(TForm)
          CBTipoStrada: TComboBox;
          EIndirizzo: TEdit;
          ENumCivico: TEdit;
          Label1: TLabel;
          Label2: TLabel;
          Label3: TLabel;
          Label8: TLabel;
          ENazione: TEdit;
          SpeedButton1: TSpeedButton;
          Panel1: TPanel;
          BOk: TToolbarButton97;
          BAnnulla: TToolbarButton97;
          procedure CBTipoStradaDropDown(Sender: TObject);
          procedure SpeedButton1Click(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure BOkClick(Sender: TObject);
          procedure BAnnullaClick(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     IndirizzoForm: TIndirizzoForm;

implementation



uses RiepilogoCand, SelNazione, Main;

{$R *.DFM}

procedure TIndirizzoForm.CBTipoStradaDropDown(Sender: TObject);
var xNazioneAzienda: string;
begin
     // tipo di strada in funzione della Nazione - RESIDENZA
     with riepilogocandform do begin
          Q.Close;
          Q.SQL.text := 'select NazioneAbbAzienda from Global';
          Q.Open;
          xNazioneAzienda := Q.FieldByName('NazioneAbbAzienda').asString;
          Q.Close;
          Q.SQL.text := 'select TipoStrada from TipiStrade where NazioneAbb=:xNazioneAbb:';
          if ENazione.text <> '' then
               Q.ParamByName['xNazioneAbb'] := ENazione.text
          else Q.ParamByName['xNazioneAbb'] := xNazioneAzienda;
          Q.Open;
          CBTipoStrada.Items.Clear;
          while not Q.EOF do begin
               CBTipoStrada.Items.Add(Q.FieldByName('TipoStrada').asString);
               Q.next;
          end;
          Q.Close;
     end;
end;

procedure TIndirizzoForm.SpeedButton1Click(Sender: TObject);
begin
     SelNazioneForm := TSelNazioneForm.create(self);
     SelNazioneForm.ShowModal;
     if (SelNazioneForm.ModalResult = mrOK) and (SelNazioneForm.QNazioni.RecordCount > 0) then begin
          ENazione.text := SelNazioneForm.QNazioni.FieldByName('Abbrev').AsString;
     end else begin
          ENazione.text := '';
     end;
     SelNazioneForm.Free;
end;

procedure TIndirizzoForm.FormShow(Sender: TObject);
begin
     Caption := '[I/4] - ' + Caption;
     IndirizzoForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
end;

procedure TIndirizzoForm.BOkClick(Sender: TObject);
begin
     IndirizzoForm.ModalResult := mrOk;
end;

procedure TIndirizzoForm.BAnnullaClick(Sender: TObject);
begin
     IndirizzoForm.ModalResult := mrCancel;
end;

end.

