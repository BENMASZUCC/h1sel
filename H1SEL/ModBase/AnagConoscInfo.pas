unit AnagConoscInfo;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     DBTables, StdCtrls, DBCtrls, Mask, Db, DBCGrids, ExtCtrls, TB97, ADODB,
     U_ADOLinkCl, Menus, main;

type
     TFrmAnagConoscInfo = class(TFrame)
          Panel1: TPanel;
          Label34: TLabel;
          DsQAnagConoscInfo: TDataSource;
          DBCtrlGrid1: TDBCtrlGrid;
          Label1: TLabel;
          DBEdit1: TDBEdit;
          Label5: TLabel;
          DBEdit5: TDBEdit;
          DBEdit6: TDBEdit;
          Label7: TLabel;
          Label8: TLabel;
          BVoceNew: TToolbarButton97;
          BVoceDel: TToolbarButton97;
          BVoceCan: TToolbarButton97;
          BVoceOK: TToolbarButton97;
          Label6: TLabel;
          DBEdit7: TDBEdit;
          QLivelli: TADOQuery;
          QAnagConoscInfo: TADOQuery;
          DBLKLivelli: TDBLookupComboBox;
          BLivelli: TToolbarButton97;
          Label3: TLabel;
          DBEdit3: TDBEdit;
          Pmtab: TPopupMenu;
          Livelli1: TMenuItem;
          QAnagConoscInfoID: TAutoIncField;
          QAnagConoscInfoIDAnagrafica: TIntegerField;
          QAnagConoscInfoIDConoscInfo: TIntegerField;
          QAnagConoscInfoPeriodoDal: TSmallintField;
          QAnagConoscInfoPeriodoAl: TSmallintField;
          QAnagConoscInfoLivello: TStringField;
          QAnagConoscInfoPiattaforme: TStringField;
          QAnagConoscInfoDescrizione: TStringField;
          QAnagConoscInfoSoftware: TStringField;
          QAnagConoscInfoLivelloLK: TStringField;
          Piattaforme1: TMenuItem;
          QPiattaforme: TADOQuery;
          QPiattaformeID: TAutoIncField;
          QPiattaformeDescrizione: TStringField;
          QAnagConoscInfoIDPiattaforma: TIntegerField;
          QAnagConoscInfoIDArea: TIntegerField;
          QAnagConoscInfoPiattaformeLK: TStringField;
          DBLKPiattaforme: TDBLookupComboBox;
          QAree: TADOQuery;
          QAreeID: TAutoIncField;
          QAreeDescrizione: TStringField;
          QAnagConoscInfoAreaLK: TStringField;
          Aree1: TMenuItem;
          Software1: TMenuItem;
          PselezInfo: TPanel;
          BSelezSoftware: TToolbarButton97;
          BSelezArea: TToolbarButton97;
          procedure DsQAnagConoscInfoStateChange(Sender: TObject);
          procedure BVoceNewClick(Sender: TObject);
          procedure BVoceOKClick(Sender: TObject);
          procedure BVoceCanClick(Sender: TObject);
          procedure BVoceDelClick(Sender: TObject);
          procedure QAnagConoscInfoBeforeOpen(DataSet: TDataSet);
          procedure Livelli1Click(Sender: TObject);
          procedure Piattaforme1Click(Sender: TObject);
          procedure Aree1Click(Sender: TObject);
          procedure QAnagConoscInfoAfterOpen(DataSet: TDataSet);
          procedure BSelezSoftwareClick(Sender: TObject);
          procedure Software1Click(Sender: TObject);
          procedure BSelezAreaClick(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

implementation

uses ConoscInfo, ModuloDati, uUtilsVarie;

{$R *.DFM}

procedure TFrmAnagConoscInfo.DsQAnagConoscInfoStateChange(Sender: TObject);
var b: boolean;
begin

     b := DsQAnagConoscInfo.State in [dsEdit, dsInsert];
     BVoceNew.Enabled := not b;
     BVoceDel.Enabled := not b;
     BVoceOK.Enabled := b;
     BVoceCan.Enabled := b;
   //  PSelezInfo.Enabled := b;

end;

procedure TFrmAnagConoscInfo.BVoceNewClick(Sender: TObject);
var newid: integer;
begin
     with Data do begin
          q1.close;
          q1.sql.text := 'insert into AnagConoscInfo (IDAnagrafica) values (:xIDAnagrafica) ';
          q1.Parameters.ParamByName('xIDAnagrafica').value := Data.TAnagrafica.FieldByName('ID').AsINteger;
          Q1.ExecSQL;

          q1.close;
          q1.sql.text := 'select @@identity LastId';
          q1.open;

          newid := q1.fieldbyname('LastId').asinteger;
     end;

     QAnagConoscInfo.close;
     QAnagConoscInfo.open;
     //QAnagConoscInfo.edit;
    // QAnagConoscInfo.locate('ID', newid, []);
    // QAnagConoscInfo.edit;

{
     ConoscInfoForm := TConoscInfoForm.create(self);
     ConoscInfoForm.ShowModal;
     if ConoscInfoForm.ModalResult = mrOK then begin

          if (not ConoscInfoForm.QConoscInfo.IsEmpty) and (ConoscInfoForm.QConoscInfo.FieldByName('ID').Value > 0) then begin
               with Data do begin
                    DB.BeginTrans;
                    try
                         q1.close;
                         q1.sql.text := 'insert into AnagConoscInfo (IDAnagrafica,IDConoscInfo) values (:xIDAnagrafica,:xIDConoscInfo) ';
                         q1.Parameters.ParamByName('xIDAnagrafica').value := Data.TAnagrafica.FieldByName('ID').AsINteger;
                         q1.Parameters.ParamByName('xIDConoscInfo').value := ConoscInfoForm.QConoscInfo.FieldByName('ID').AsInteger;
                         Q1.ExecSQL;
                         DB.CommitTrans;
                    except
                            DB.RollbackTrans;
                         MessageDlg('ERRORE SUL DATABASE: operazione non effettuata', mtError, [mbOK], 0);
                         raise;
                    end;
               end;
               QAnagConoscInfo.Close;
               QAnagConoscInfo.Open;
          end;
     end;
     ConoscInfoForm.Free; }
end;

procedure TFrmAnagConoscInfo.BVoceOKClick(Sender: TObject);
begin
     QAnagConoscInfo.Post;
end;

procedure TFrmAnagConoscInfo.BVoceCanClick(Sender: TObject);
begin
     QAnagConoscInfo.Cancel;
end;

procedure TFrmAnagConoscInfo.BVoceDelClick(Sender: TObject);
begin
     if QAnagConoscInfo.IsEmpty then exit;
     if MessageDlg('Sei sicuro di voler eliminare la voce selezionata ?', mtWarning,
          [mbNo, mbYes], 0) = mrYes then begin
          Data.QTemp.CLose;
          Data.QTemp.SQL.text := 'delete from AnagConoscInfo where ID=' + QAnagConoscInfo.FieldByName('ID').asString;
          Data.QTemp.ExecSQL;
          QAnagConoscInfo.Close;
          QAnagConoscInfo.Open;
     end;
end;

procedure TFrmAnagConoscInfo.QAnagConoscInfoBeforeOpen(DataSet: TDataSet);
begin
//grafica frame
//(messa qua perch� non trovo l'equivalente dell'onshow)
     PselezInfo.color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     DBCtrlGrid1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     DBCtrlGrid1.SelectedColor := MainForm.AdvToolBarOfficeStyler1.BorderColor;


     QAnagConoscInfo.SQL.Text := 'select * ' +
          ' from AnagConoscInfo' +
          ' where IDAnagrafica=' + Data.TAnagrafica.FieldByName('ID').asString;

     QLivelli.Open;
     QPiattaforme.Open;
     QAree.Open;
end;

procedure TFrmAnagConoscInfo.Livelli1Click(Sender: TObject);
begin
     OpenSelFromTab('LivelliConoscInfo', 'ID', 'Livello', 'Livelli', '');
     QLivelli.Close;
     QLivelli.Open;
end;

procedure TFrmAnagConoscInfo.Piattaforme1Click(Sender: TObject);
begin
     OpenSelFromTab('PiattaformeInfo', 'ID', 'Descrizione', 'Piattaforme', '');
     QPiattaforme.Close;
     QPiattaforme.Open;
end;

procedure TFrmAnagConoscInfo.Aree1Click(Sender: TObject);
begin
     OpenSelFromTab('AreeInfo', 'ID', 'Descrizione', 'aree', '');
     QAree.Close;
     QAree.Open;
end;

procedure TFrmAnagConoscInfo.QAnagConoscInfoAfterOpen(DataSet: TDataSet);
begin
     if QAnagConoscInfo.RecordCount = 0 then
          DBCtrlGrid1.Enabled := false else
          DBCtrlGrid1.Enabled := true;
end;

procedure TFrmAnagConoscInfo.BSelezSoftwareClick(Sender: TObject);
begin
     ConoscInfoForm := TConoscInfoForm.create(self);
     ConoscInfoForm.ShowModal;
     if ConoscInfoForm.ModalResult = mrok then begin
          QAnagConoscInfo.edit;
          QAnagConoscInfoSoftware.Value := ConoscInfoForm.QConoscInfoSoftware.Value;
          QAnagConoscInfo.post;
     end;

     ConoscInfoForm.free;
end;

procedure TFrmAnagConoscInfo.Software1Click(Sender: TObject);
begin
     ConoscInfoForm := TConoscInfoForm.create(self);
     ConoscInfoForm.ShowModal;
     ConoscInfoForm.free;
end;

procedure TFrmAnagConoscInfo.BSelezAreaClick(Sender: TObject);
var xid, xidtemp: string;
begin
     xidtemp := QAnagConoscInfoID.AsString;
     xid := '';
     xid := OpenSelFromTab('AreeInfo', 'ID', 'Descrizione', 'aree', '');
     if xid <> '' then begin
          QAnagConoscInfo.edit;
          QAnagConoscInfoIDArea.Value := strtoint(xid);
          QAnagConoscInfo.post;
          QAnagConoscInfo.Locate('id', xidtemp, []);
     end;

end;

end.

