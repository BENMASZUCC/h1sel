unit frmGestFile;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Buttons, TB97, dxDBTLCl, dxGrClms, dxTL, dxDBCtrl, dxDBGrid, dxCntner,
     StdCtrls, DBCtrls, ExtCtrls, IdGlobal, JclFileUtils, ModelCompositionTVD5b, uModelliWord_remote;

type
     TGestFileFrame = class(TFrame)
          Panel177: TPanel;
          DbCbcandInteressante: TDBCheckBox;
          dxDBGrid14: TdxDBGrid;
          dxDBGrid14NomeFile: TdxDBGridMaskColumn;
          dxDBGrid14DataCreazione: TdxDBGridDateColumn;
          dxDBGrid14Descrizione: TdxDBGridMaskColumn;
          dxDBGrid14Tipo: TdxDBGridMaskColumn;
          dxDBGrid14VisibleCliente: TdxDBGridCheckColumn;
          dxDBGrid14FlagInvio: TdxDBGridCheckColumn;
          dxDBGrid14Column7: TdxDBGridCheckColumn;
          Panel123: TPanel;
          BAnagFileNew: TToolbarButton97;
          BAnagFileMod: TToolbarButton97;
          BAnagFileDel: TToolbarButton97;
          ToolbarButton9745: TToolbarButton97;
          BFileDaModello: TToolbarButton97;
          ToolbarButton9756: TToolbarButton97;
          BAnagFileSalva: TToolbarButton97;
          Panel124: TPanel;
          SpeedButton19: TSpeedButton;
          procedure BAnagFileNewClick(Sender: TObject);
          procedure BAnagFileModClick(Sender: TObject);
          procedure BAnagFileDelClick(Sender: TObject);
          procedure ToolbarButton9756Click(Sender: TObject);
          procedure BAnagFileSalvaClick(Sender: TObject);
          procedure BFileDaModelloClick(Sender: TObject);
          procedure ToolbarButton9745Click(Sender: TObject);
     private
    { Private declarations }
     public
    { Public declarations }
     end;

implementation

uses AnagFile, ModuloDati, Main, uUtilsVarie, SceltaModelloAnag, SelRicCand;

{$R *.DFM}

procedure TGestFileFrame.BAnagFileNewClick(Sender: TObject);
var xFileNameSave: string;
     xIDAnagFile: integer;
begin
     if not MAinform.CheckProfile('070') then Exit;
     AnagFileForm := TAnagFileForm.create(self);
     AnagFileForm.ECognome.text := TrimRight(Data.TAnagrafica.FieldByName('Cognome').AsString);
     AnagFileForm.ENome.text := TrimRight(Data.TAnagrafica.FieldByName('Nome').AsString);
     if AnagFileForm.QGlobalAnagFileDentroDB.value then
          AnagFileForm.FEFile.InitialDir := ''
     else AnagFileForm.FEFile.InitialDir := GetDocPath;
     if Data.Global.FieldByName('CheckCopiaFileAllegato').asString <> '' then begin
          if Data.Global.FieldByName('CheckCopiaFileAllegato').value then
               AnagFileForm.FEFile.InitialDir := '';
     end;
     AnagFileForm.ShowModal;
     if AnagFileForm.ModalResult = mrOK then begin

          if AnagFileForm.QTipiFileDefaultVisibileWeb.AsBoolean = true then begin
               if MessageDlg('Attenzione!!! ' + chr(13) + 'Utilizzando il tipo "' + AnagFileForm.QTipiFileTipo.AsString + '" questo file sar� visibile dal candidato nella sua area riservata'
                    + chr(13) + 'Continuare?', mtConfirmation, [mbYes, mbNo], 0) = mrNO then
                    exit;
          end;

          with Data do begin
              // DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text := 'insert into AnagFile (IDAnagrafica,IDTipo,Descrizione,DataCreazione,FlagInvio,VisibleCliente) ' +
                         'values (:xIDAnagrafica:,:xIDTipo:,:xDescrizione:,:xDataCreazione:,:xFlagInvio:,:xVisibleCliente:)';
                    Q1.ParambyName['xIDAnagrafica'] := Data.TAnagrafica.FieldByName('ID').AsINteger;
                    Q1.ParambyName['xIDTipo'] := AnagFileForm.QTipiFile.FieldByName('ID').AsINteger;
                    Q1.ParambyName['xDescrizione'] := AnagFileForm.EDesc.text;

                    if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 6)) = 'BOYDEN' then
                         Q1.ParambyName['xDataCreazione'] := FileDateToDateTime(FileAge(AnagFileForm.FEFile.filename))
                    else
                         Q1.ParambyName['xDataCreazione'] := Date;

                    Q1.ParambyName['xFlagInvio'] := AnagFileForm.CBFlagInvio.checked;
                    Q1.ParambyName['xVisibleCliente'] := AnagFileForm.CBVisibleCliente.checked;
                    Q1.ExecSQL;

                    Data.QTemp.Close;
                    Data.QTemp.SQL.text := 'select @@IDENTITY as LastID';
                    Data.QTemp.Open;
                    xIDAnagFile := Data.QTemp.FieldByName('LastID').asInteger;
                    Data.QTemp.Close;

                    if AnagFileForm.QGlobalAnagFileDentroDB.value then begin
                          // inserimento file nel DB
                         if Data.Global.FieldByName('CheckRenameFileAllegato').AsBoolean then
                              xFileNameSave := GetPathFromTable('FileCandidati') + inttostr(xIDAnagFile) + ExtractFileExt(AnagFileForm.FEFile.filename)
                         else
                              xFileNameSave := AnagFileForm.FEFile.filename;

                         Data.QAnagFileFile.Close;
                         Data.QAnagFileFile.Parameters[0].value := xIDAnagFile;
                         Data.QAnagFileFile.Open;
                         Data.QAnagFileFile.Edit;

                          //Data.QAnagFileFileFileContent.LoadFromFile(xFileNameSave);

                         try
                              {HOWMESSAGE(AnagFileForm.FEFile.filename);
                              LoginForm := TLoginForm.create(self);
                              LoginForm.edit1.Text := AnagFileForm.FEFile.filename;
                              LoginForm.ShowModal;  }

                              Data.QAnagFileFileDocFile.LoadFromFile(AnagFileForm.FEFile.filename);
                         except
                              if data.global.fieldbyname('debughrms').asboolean = true then showmessage('copy=');
                              CopyFileTo(AnagFileForm.FEFile.filename, PathGetTempPath + '\' + ExtractFileName(xFileNameSave));
                              if data.global.fieldbyname('debughrms').asboolean = true then showmessage('load');
                              Data.QAnagFileFileDocFile.LoadFromFile(PathGetTempPath + '\' + ExtractFileName(xFileNameSave));
                              if data.global.fieldbyname('debughrms').asboolean = true then showmessage('delete');
                              deletefile(PathGetTempPath + '\' + ExtractFileName(xFileNameSave));
                         end;

                         Data.QAnagFileFileDocExt.Value := Stringreplace(ExtractFileExt(xFileNameSave), '.', '', [rfReplaceAll]);
                         Data.QAnagFileFileFileBloccato.value := False;
                         data.QAnagFileFile.FieldByName('idutente').value := mainform.xIDUtenteAttuale;
                         Data.QAnagFileFile.Post;
                         Data.QAnagFileFile.Close;
                    end else begin
                         if Data.Global.FieldByName('CheckCopiaFileAllegato').AsBoolean then
                              xFileNameSave := CopiaFileAll(AnagFileForm.FEFile.filename, Data.Global.FieldByName('CheckRenameFileAllegato').AsBoolean, 'FileCandidati', xIDAnagFile)
                         else
                              xFileNameSave := AnagFileForm.FEFile.filename;
                    end;
                    if data.global.fieldbyname('debughrms').asboolean = true then showmessage('update');
                    if data.global.fieldbyname('debughrms').asboolean = true then showmessage('nomefile=' + xFileNameSave);
                    Q1.Close;
                    Q1.SQL.Text := 'update anagfile set nomefile = :xnomefile:, solonome = :xsolonome: , FileType = :xFileType: where ID = ' + inttostr(xIDAnagFile);
                    Q1.ParambyName['xNomeFile'] := xFileNameSave;
                    Q1.ParambyName['xSoloNome'] := ExtractFileName(xFileNameSave);
                    Q1.ParambyName['xFileType'] := GetMIMETypeFromFile(xFileNameSave);
                    Q1.ExecSQL;

                    {

                    Q1.Close;
                    Q1.SQL.text := 'insert into AnagFile (IDAnagrafica,IDTipo,Descrizione,NomeFile,DataCreazione,FlagInvio,SoloNome,VisibleCliente,FileType) ' +
                         'values (:xIDAnagrafica:,:xIDTipo:,:xDescrizione:,:xNomeFile:,:xDataCreazione:,:xFlagInvio:,:xSoloNome:,:xVisibleCliente:,:xFileType:)';
                    Q1.ParambyName['xIDAnagrafica'] := Data.TAnagrafica.FieldByName('ID').AsINteger;
                    Q1.ParambyName['xIDTipo'] := AnagFileForm.QTipiFile.FieldByName('ID').AsINteger;
                    Q1.ParambyName['xDescrizione'] := AnagFileForm.EDesc.text;
                    Q1.ParambyName['xNomeFile'] := xFileNameSave;
                    Q1.ParambyName['xSoloNome'] := ExtractFileName(xFileNameSave);
                    Q1.ParambyName['xDataCreazione'] := Date;
                    Q1.ParambyName['xFlagInvio'] := AnagFileForm.CBFlagInvio.checked;
                    Q1.ParambyName['xVisibleCliente'] := AnagFileForm.CBVisibleCliente.checked;
                    Q1.ParambyName['xFileType'] := GetMIMETypeFromFile(xFileNameSave);
                    Q1.ExecSQL;

                    Data.QTemp.SQL.text := 'select @@IDENTITY as LastID';
                    Data.QTemp.Open;
                    xIDAnagFile := Data.QTemp.FieldByName('LastID').asInteger;
                    Data.QTemp.Close; }



                   // DB.CommitTrans;
               except
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                    raise;
                   // DB.RollbackTrans;
               end;
          end;
          Data.QAnagFile.Close;
          Data.QAnagFile.Open;
     end;
     AnagFileForm.Free;
end;
//[TONI20020923]DEBUGFINE

procedure TGestFileFrame.BAnagFileModClick(Sender: TObject);
begin
     if not Mainform.CheckProfile('071') then Exit;
     AnagFileForm := TAnagFileForm.create(self);
     AnagFileForm.ECognome.text := Data.TAnagrafica.FieldByName('Cognome').AsString;
     AnagFileForm.ENome.text := Data.TAnagrafica.FieldByName('Nome').AsString;
     while (AnagFileForm.QTipiFile.FieldByName('ID').AsInteger <> Data.QAnagFile.FieldByName('IDTipo').AsInteger) and
          (not AnagFileForm.QTipiFile.EOF) do AnagFileForm.QTipiFile.Next;
     AnagFileForm.EDesc.text := Data.QAnagFile.FieldByName('Descrizione').AsString;

     if (Data.QAnagFileDocFile.asString <> '') or (Data.Global.FieldByName('CheckCopiaFileAllegato').asBoolean = TRUE) then begin
          AnagFileForm.FEFile.Enabled := False;
          AnagFileForm.FEFile.Color := clBtnFace;
     end;

     if Data.Global.FieldByName('AnagFileDentroDB').asBoolean = FALSE then begin
          AnagFileForm.FEFile.FileName := Data.QAnagFile.FieldByName('NomeFile').AsString;
     end;

     if pos('INTERMEDIA', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) > 0 then begin
          AnagFileForm.FEFile.Text := '(non disponibile)';
     end;

     AnagFileForm.CBFlagInvio.checked := Data.QAnagFile.FieldByName('FlagInvio').AsBoolean;
     AnagFileForm.CBVisibleCliente.checked := Data.QAnagFile.FieldByName('VisibleCliente').AsBoolean;
     AnagFileForm.ShowModal;
     if AnagFileForm.ModalResult = mrOK then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;

                    if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 6)) = 'BOYDEN' then begin
                         Q1.SQL.text := 'update AnagFile set IDTipo=:xIDTipo:,Descrizione=:xDescrizione:,NomeFile=:xNomeFile:,FlagInvio=:xFlagInvio:,SoloNome=:xSoloNome:,VisibleCliente=:xVisibleCliente:,FileType=:xFileType: , ' +
                              ' DataCreazione=:xDataCreazione: ' +
                              'where ID=' + Data.QAnagFile.FieldByName('ID').asString;
                         Q1.ParambyName['xDataCreazione'] := DataModificaFile(data.QAnagFileNomeFile.AsString);
                    end else
                         Q1.SQL.text := 'update AnagFile set IDTipo=:xIDTipo:,Descrizione=:xDescrizione:,NomeFile=:xNomeFile:,FlagInvio=:xFlagInvio:,SoloNome=:xSoloNome:,VisibleCliente=:xVisibleCliente:,FileType=:xFileType: ' +
                              'where ID=' + Data.QAnagFile.FieldByName('ID').asString;


                    Q1.ParambyName['xIDTipo'] := AnagFileForm.QTipiFile.FieldByname('ID').AsString;
                    Q1.ParambyName['xDescrizione'] := AnagFileForm.EDesc.text;
                    if AnagFileForm.FEFile.Text = '(non disponibile)' then
                         Q1.ParambyName['xNomeFile'] := Data.QAnagFile.FieldByName('NomeFile').AsString
                    else Q1.ParambyName['xNomeFile'] := AnagFileForm.FEFile.filename;
                    //if Data.QAnagFileFileContent.asString <> '' then begin
                    if Data.QAnagFileDocFile.asString <> '' then begin
                         Q1.ParambyName['xSoloNome'] := Data.QAnagFileSoloNome.Value;
                    end else begin
                         if AnagFileForm.FEFile.Text = '(non disponibile)' then
                              Q1.ParambyName['xSoloNome'] := ExtractFileName(Data.QAnagFile.FieldByName('NomeFile').AsString)
                         else Q1.ParambyName['xSoloNome'] := ExtractFileName(AnagFileForm.FEFile.filename);
                    end;
                    Q1.ParambyName['xFlagInvio'] := AnagFileForm.CBFlagInvio.checked;
                    Q1.ParambyName['xVisibleCliente'] := AnagFileForm.CBVisibleCliente.checked;
                    Q1.ParambyName['xFileType'] := GetMIMETypeFromFile(AnagFileForm.FEFile.filename);
                    Q1.ExecSQL;
                    DB.CommitTrans;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                    raise;
               end;
          end;
          //[/GIULIO20020929\]
          Data.QAnagFile.Close;
          Data.QAnagFile.Open;
     end;
     AnagFileForm.Free;
end;

procedure TGestFileFrame.BAnagFileDelClick(Sender: TObject);
var xFileName: string;
begin
     if not Mainform.CheckProfile('071') then Exit;
     if MessageDlg('Sei sicuro di voler eliminare l''associazione ?', mtWarning, [mbNo, mbYes], 0) <> mrYes then exit;
     if data.Global.FieldByName('AnagFiledentroDB').asboolean = false then
          if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 5)) <> 'ERGON' then begin
               xFileName := Data.QAnagFile.FieldByName('NomeFile').AsString;
               if FileExists(xFileName) then begin
                    if MessageDlg('Vuoi eliminare il file fisico ?', mtWarning, [mbNo, mbYes], 0) = mrYes then begin
                         DeleteFile(xFileName);
                    end;
               end;
          end;
     with Data do begin
         //  DB.BeginTrans;
          try
               Q1.Close;
               Q1.SQL.text := 'delete from AnagFile where ID=' + Data.QAnagFile.FieldByName('ID').asString;
               Q1.ExecSQL;
               Log_Operation(Mainform.xIDUtenteAttuale, 'AnagFile', Data.QAnagFile.FieldByName('ID').asinteger, 'D', 'Canc ' + Data.QAnagFileDescrizione.AsString);
              // DB.CommitTrans;
          except
              // DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
          end;
     end;
     //[/GIULIO20020929\]
     Data.QAnagFile.Close;
     Data.QAnagFile.Open;
end;

procedure TGestFileFrame.ToolbarButton9756Click(Sender: TObject);
begin
     aprifiledue(data.QAnagFile.fieldByname('id').asinteger, 'main');
end;

procedure TGestFileFrame.BAnagFileSalvaClick(Sender: TObject);
begin
     salvafileDue(data.QAnagFile.fieldByname('id').asinteger, 'main');
end;

procedure TGestFileFrame.BFileDaModelloClick(Sender: TObject);
var xFile: string;
     xSalva, xStampa: boolean;
     xIDAnagFile: Integer;
begin
     if not Mainform.CheckProfile('072') then Exit;
     SceltaModelloAnagForm := TSceltaModelloAnagForm.create(self);
     SceltaModelloAnagForm.Height := 274;
     SceltaModelloAnagForm.QModelliWord.SQL.text := 'select * from ModelliWord where H1Sel>0';
     SceltaModelloAnagForm.QModelliWord.Open;
     SceltaModelloAnagForm.ShowModal;
     if SceltaModelloAnagForm.ModalResult = mrOK then begin

          if data.global.fieldbyname('AnagFileDentroDB').AsBoolean then
               xsalva := true
          else
               xsalva := SceltaModelloAnagForm.CBSalva.Checked;

          xStampa := False;

          if MainForm.xSendMail_Remote then
               xFile := RiempiModelloWord_remoto(SceltaModelloAnagForm.QModelliWord.FieldByName('ID').AsInteger, Data.TAnagrafica.FieldByName('ID').AsInteger)
          else
               xFile := CreaFileWordAnag(Data.TAnagrafica.FieldByName('ID').AsInteger, SceltaModelloAnagForm.QModelliWord.FieldByName('ID').AsInteger,
                    SceltaModelloAnagForm.QModelliWord.FieldByName('NomeModello').AsString,
                    xSalva, xStampa, TModelCompositionTV.Create(self));
          if xsalva and (xfile <> '') then begin
               // associa al soggetto

               with Data do begin
                  //  try
                    Q1.Close;
                    Q1.SQL.text := 'insert into AnagFile (IDAnagrafica,IDTipo,Descrizione,DataCreazione,FlagInvio,VisibleCliente) ' +
                         'values (:xIDAnagrafica:,:xIDTipo:,:xDescrizione:,:xDataCreazione:,:xFlagInvio:,:xVisibleCliente:)';
                    Q1.ParambyName['xIDAnagrafica'] := Data.TAnagrafica.FieldByName('ID').AsInteger;
                    Q1.ParambyName['xIDTipo'] := SceltaModelloAnagForm.QModelliWord.FieldByName('IDTipo').value;
                    Q1.ParambyName['xDescrizione'] := SceltaModelloAnagForm.QModelliWord.FieldByName('Descrizione').AsString;
                    Q1.ParambyName['xDataCreazione'] := Date;
                    Q1.ParambyName['xFlagInvio'] := SceltaModelloAnagForm.CBFlagInvio.checked;
                    Q1.ParambyName['xVisibleCliente'] := SceltaModelloAnagForm.CBVisibleCliente.checked;
                    Q1.ExecSQL;

                    Data.QTemp.Close;
                    Data.QTemp.SQL.text := 'select @@IDENTITY as LastID';
                    Data.QTemp.Open;
                    xIDAnagFile := Data.QTemp.FieldByName('LastID').asInteger;
                    Data.QTemp.Close;


                              // inserimento file nel DB
                    if data.global.fieldbyname('AnagFileDentroDB').AsBoolean then begin
                         Data.QAnagFileFile.Close;
                         Data.QAnagFileFile.Parameters[0].value := xIDAnagFile;
                         Data.QAnagFileFile.Open;
                         Data.QAnagFileFile.Edit;

                         try
                              Data.QAnagFileFileDocFile.LoadFromFile(xFile);
                              deletefile(xFile);
                         except
                              on e: Exception do begin
                                   MessageDlg('Errore:' + e.Message, mtError, [mbOK], 0);
                              end;
                         end;

                         Data.QAnagFileFileDocExt.Value := Stringreplace(ExtractFileExt(xFile), '.', '', [rfReplaceAll]);
                         Data.QAnagFileFileFileBloccato.value := False;
                         data.QAnagFileFile.FieldByName('idutente').value := mainform.xIDUtenteAttuale;
                         Data.QAnagFileFile.Post;
                         Data.QAnagFileFile.Close;
                    end;
                    Q1.Close;
                    Q1.SQL.Text := 'update anagfile set nomefile = :xnomefile:, solonome = :xsolonome: , FileType = :xFileType: where ID = ' + inttostr(xIDAnagFile);
                    Q1.ParambyName['xNomeFile'] := xFile;
                    Q1.ParambyName['xSoloNome'] := ExtractFileName(xFile);
                    Q1.ParambyName['xFileType'] := GetMIMETypeFromFile(xFile);
                    Q1.ExecSQL;

                    Data.QAnagFile.Close;
                    Data.QAnagFile.Open;
               end;
          end;

     end;
     SceltaModelloAnagForm.Free;
end;

procedure TGestFileFrame.ToolbarButton9745Click(Sender: TObject);
begin
     MessageDlg('ATTENZIONE!! Prima di procedere con la presentazione di personale ' +
          'accertarsi che il/i candidato/i sia/siano consenziente/i. (Decreto Legislativo 30 Giugno 2003 N. 196)', mtInformation, [mbOK], 0);
     if not Mainform.CheckProfile('073') then Exit;

          // OpenJob
     if Pos('OPENJOB', UpperCase(Data.Global.FieldByName('NomeAzienda').AsString)) > 0 then
     begin
          SelRicCandForm := TSelRicCandForm.create(self);
          SelRicCandForm.QRicCand.ParambyName['xIDAnag'] := Data.TAnagrafica.FieldByName('ID').AsINteger;
          SelRicCandForm.QRicCand.Open;
          if SelRicCandForm.QRicCand.RecordCount = 0 then begin
               MessageDlg('Non risultano ricerche attive associate al soggetto', mtError, [mbOK], 0);
               SelRicCandForm.Free;
               exit;
          end else
          begin
               SelRicCandForm.ShowModal;
               if SelRicCandForm.ModalResult = mrOK then
               begin
                    Mainform.CompilaFilePresPosOpenJob(SelRicCandForm.QRicCand.FieldByName('Posizione').asString,
                         SelRicCandForm.QRicCand.FieldByName('IDCandRic').asinteger,
                         SelRicCandForm.QRicCand.FieldByName('IDCliente').asInteger,
                         SelRicCandForm.QRicCand.FieldByName('IDMansione').asInteger,
                         SelRicCandForm.QRicCand.FieldByName('IDricerca').asInteger);
                    exit;
               end else exit;
          end;
     end;

     //OPEN Executive Search & Selection
     if Pos('OPEN EXECUTIVE', UpperCase(Data.Global.FieldByName('NomeAzienda').AsString)) > 0 then begin
          SelRicCandForm := TSelRicCandForm.create(self);
          SelRicCandForm.QRicCand.ParambyName['xIDAnag'] := Data.TAnagrafica.FieldByName('ID').AsINteger;
          SelRicCandForm.QRicCand.Open;
          if SelRicCandForm.QRicCand.RecordCount = 0 then begin
               MessageDlg('Non risultano ricerche attive associate al soggetto', mtError, [mbOK], 0);
               SelRicCandForm.Free;
               exit;
          end else
          begin
               SelRicCandForm.ShowModal;
               if SelRicCandForm.ModalResult = mrOK then
               begin
                    Mainform.CompilaFilePresPosOpenExecutive(SelRicCandForm.QRicCand.FieldByName('TitoloCliente').asString,
                         SelRicCandForm.QRicCand.FieldByName('IDCandRic').asinteger,
                         SelRicCandForm.QRicCand.FieldByName('IDCliente').asInteger,
                         SelRicCandForm.QRicCand.FieldByName('IDMansione').asInteger);
                    exit;
               end else exit;
          end;
     end;

      // Sambonet
     if Pos('SAMBONET', UpperCase(Data.Global.FieldByName('NomeAzienda').AsString)) > 0 then
     begin
          Mainform.CompilaCVSambonet(data.TAnagraficaID.AsString);
          exit;
     end;

     // STV
     if Pos('STV', UpperCase(Data.Global.FieldByName('NomeAzienda').AsString)) > 0 then
     begin
          SelRicCandForm := TSelRicCandForm.create(self);
          SelRicCandForm.QRicCand.ParambyName['xIDAnag'] := Data.TAnagrafica.FieldByName('ID').AsINteger;
          SelRicCandForm.QRicCand.Open;
          if SelRicCandForm.QRicCand.RecordCount = 0 then begin
               MessageDlg('Non risultano ricerche attive associate al soggetto', mtError, [mbOK], 0);
               SelRicCandForm.Free;
               exit;
          end else
          begin
               SelRicCandForm.ShowModal;
               if SelRicCandForm.ModalResult = mrOK then
               begin
                    Mainform.CompilaFilePresPosSTV(SelRicCandForm.QRicCand.FieldByName('TitoloCliente').asString,
                         SelRicCandForm.QRicCand.FieldByName('IDCandRic').asinteger,
                         SelRicCandForm.QRicCand.FieldByName('IDCliente').asInteger,
                         SelRicCandForm.QRicCand.FieldByName('IDMansione').asInteger);
                    exit;
               end else exit;
          end;
     end;


     if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 7)) = 'FERRARI' then begin
          // FERRARI
          Mainform.CompilaFilePresPosFERRARI;
          exit;
     end;

          //BOYDEN
     if Pos('RM BOYDEN', UpperCase(Data.Global.FieldByName('NomeAzienda').AsString)) > 0 then begin
          SelRicCandForm := TSelRicCandForm.create(self);
          SelRicCandForm.QRicCand.ParambyName['xIDAnag'] := Data.TAnagrafica.FieldByName('ID').AsINteger;
          SelRicCandForm.QRicCand.Open;
          if SelRicCandForm.QRicCand.RecordCount = 0 then begin
               MessageDlg('Non risultano ricerche attive associate al soggetto', mtError, [mbOK], 0);
               SelRicCandForm.Free;
               exit;
          end else begin
               SelRicCandForm.ShowModal;
               if SelRicCandForm.ModalResult = mrOK then begin
                    Mainform.CompilaFilePresPosBoydenRoma(SelRicCandForm.QRicCand.FieldByName('TitoloCliente').asString,
                         SelRicCandForm.QRicCand.FieldByName('IDCandRic').asinteger,
                         SelRicCandForm.QRicCand.FieldByName('IDCliente').asInteger,
                         SelRicCandForm.QRicCand.FieldByName('IDMansione').asInteger);
               end;
          end;
          SelRicCandForm.Free;
          exit;
     end;


     SelRicCandForm := TSelRicCandForm.create(self);
     SelRicCandForm.QRicCand.ParambyName['xIDAnag'] := Data.TAnagrafica.FieldByName('ID').AsINteger;
     SelRicCandForm.QRicCand.Open;
     if SelRicCandForm.QRicCand.RecordCount = 0 then begin
          MessageDlg('Non risultano ricerche attive associate al soggetto', mtError, [mbOK], 0);
          SelRicCandForm.Free;
          exit;
     end else begin
          SelRicCandForm.ShowModal;
          if SelRicCandForm.ModalResult = mrOK then begin
               // GENERA PRESENTAZIONE
               {if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 6)) = 'LEDERM' then begin
                    // SOLO LEDERMANN
                    SelContattiForm := TSelContattiForm.create(self);
                    SelContattiForm.QContatti.SQL.text := 'select ID,Contatto,Telefono ' +
                         'from EBC_ContattiClienti where IDCliente=' + SelRicCandForm.QRicCand.FieldByName('IDCliente').asString;
                    SelContattiForm.QContatti.Open;
                    SelContattiForm.ShowModal;
                    if SelContattiForm.Modalresult = mrOK then
                         CompilaFilePresPosLWG(SelRicCandForm.QRicCand.FieldByName('Posizione').asString,
                              SelRicCandForm.QRicCand.FieldByName('IDCandRic').asinteger,
                              SelRicCandForm.QRicCand.FieldByName('IDCliente').asInteger,
                              SelContattiForm.QContatti.FieldByName('ID').AsINteger);
                    SelContattiForm.Free;      }
               //end else begin
               if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 6)) = 'PROPOS' then begin
                         // PROPOSTE
                    Mainform.CompilaFilePresPosProposte(SelRicCandForm.QRicCand.FieldByName('Posizione').asString,
                         SelRicCandForm.QRicCand.FieldByName('IDCandRic').asinteger,
                         SelRicCandForm.QRicCand.FieldByName('IDCliente').asInteger,
                         SelRicCandForm.QRicCand.FieldByName('IDMansione').asInteger);
               end else begin
                    if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 6)) = 'MCS' then begin
                              // MCS
                         Mainform.CompilaFilePresPosMCS(SelRicCandForm.QRicCand.FieldByName('Posizione').asString,
                              SelRicCandForm.QRicCand.FieldByName('IDCandRic').asinteger,
                              SelRicCandForm.QRicCand.FieldByName('IDCliente').asInteger,
                              SelRicCandForm.QRicCand.FieldByName('IDMansione').asInteger);
                    end else begin
                         if (Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 6)) = 'BOYDEN') or (Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 9)) = 'LAVOROPIU') or
                              (Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 7)) = 'CALENTI') or (Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 10)) = 'EXPERIENCE') then begin
                              if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 6)) = 'BOYDEN' then begin
                                   Mainform.CompilaFilePresPosBoyden(SelRicCandForm.QRicCand.FieldByName('TitoloCliente').asString,
                                        SelRicCandForm.QRicCand.FieldByName('IDCandRic').asinteger,
                                        SelRicCandForm.QRicCand.FieldByName('IDCliente').asInteger,
                                        SelRicCandForm.QRicCand.FieldByName('IDMansione').asInteger);
                              end;
                              if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 9)) = 'LAVOROPIU' then begin
                                   Mainform.CompilaFilePresPosLavoroPiu(SelRicCandForm.QRicCand.FieldByName('TitoloCliente').asString,
                                        SelRicCandForm.QRicCand.FieldByName('IDCandRic').asinteger,
                                        SelRicCandForm.QRicCand.FieldByName('IDCliente').asInteger,
                                        SelRicCandForm.QRicCand.FieldByName('IDMansione').asInteger);
                              end;
                              if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 7)) = 'CALENTI' then begin
                                   Mainform.CompilaFilePresPosCalenti(SelRicCandForm.QRicCand.FieldByName('Cliente').asString,
                                        SelRicCandForm.QRicCand.FieldByName('Posizione').asString,
                                        SelRicCandForm.QRicCand.FieldByName('IDCandRic').asinteger);
                              end;
                              if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 10)) = 'EXPERIENCE' then begin
                                   Mainform.CompilaFilePresPosExperience(SelRicCandForm.QRicCand.FieldByName('Cliente').asString,
                                        SelRicCandForm.QRicCand.FieldByName('Posizione').asString,
                                        SelRicCandForm.QRicCand.FieldByName('IDCandRic').asinteger);
                              end;

                         end else begin
                                   // TUTTI GLI ALTRI
                              Mainform.CompilaFilePresPos(SelRicCandForm.QRicCand.FieldByName('Cliente').asString,
                                   SelRicCandForm.QRicCand.FieldByName('Posizione').asString,
                                   SelRicCandForm.QRicCand.FieldByName('IDCandRic').asinteger);
                         end;
                    end;
               end;
               //end;
          end;
          SelRicCandForm.Free;
     end;
end;

end.

