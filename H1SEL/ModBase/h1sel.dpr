program h1sel;



uses
  Windows,
  Forms,
  Dialogs,
  ModuloDati in 'ModuloDati.pas' {Data: TDataModule},
  Splash2 in 'Splash2.pas' {Splash2Form},
  CambiaStato2 in 'CambiaStato2.pas' {CambiaStato2Form},
  ModuloDati2 in 'ModuloDati2.pas' {Data2: TDataModule},
  Curriculum in 'Curriculum.pas' {CurriculumForm},
  DettaglioTesi in 'DettaglioTesi.pas' {DettaglioTesiForm},
  Lingue in 'Lingue.pas' {LingueForm},
  DettEspLav in 'DettEspLav.pas' {DettEspLavForm},
  Splash in 'Splash.pas' {SplashNewForm},
  Login in 'Login.pas' {LoginForm},
  SelArea in 'SelArea.pas' {SelAreaForm},
  SceltaStato in 'SceltaStato.pas' {SceltaStatoForm},
  MessLetti in 'MessLetti.pas' {MessLettiForm},
  StoricoComp in 'StoricoComp.pas' {StoricoCompForm},
  Main in 'Main.pas' {MainForm},
  SelPers in 'SelPers.pas' {SelPersForm},
  SelDiploma in 'SelDiploma.pas' {SelDiplomaForm},
  SelRuolo in 'SelRuolo.pas' {SelRuoloForm},
  SelLingua in 'SelLingua.pas' {SelLinguaForm},
  CompInserite in 'CompInserite.pas' {CompInseriteForm},
  ValutazDip in 'ValutazDip.pas' {ValutazDipForm},
  ElencoDip in 'ElencoDip.pas' {ElencoDipForm},
  RepElencoSel in 'RepElencoSel.pas' {QRElencoSel: TQuickRep},
  RepSchedaVal in 'RepSchedaVal.pas' {QRSchedaVal: TQuickRep},
  RuoloRif in 'RuoloRif.pas' {RuoloRifForm},
  CercaAnnuncio in 'CercaAnnuncio.pas' {CercaAnnuncioForm},
  MessIniziale in 'MessIniziale.pas' {MessInizialeForm},
  Messaggio in 'Messaggio.pas' {MessaggioForm},
  SelCompetenza in 'SelCompetenza.pas' {SelCompetenzaForm},
  InsRuolo in 'InsRuolo.pas' {InsRuoloForm},
  InsCaratt in 'InsCaratt.pas' {InsCarattForm},
  ConfEvento in 'ConfEvento.pas' {ConfEventoForm},
  Splash3 in 'Splash3.pas' {Splash3Form},
  AgendaSett in 'AgendaSett.pas' {AgendaSettForm},
  InsMessaggio in 'InsMessaggio.pas' {InsMessaggioForm},
  ModMessaggio in 'ModMessaggio.pas' {ModMessaggioForm},
  NuovoApp in 'NuovoApp.pas' {NuovoImpForm},
  Sposta in 'Sposta.pas' {SpostaForm},
  SelData in 'SelData.pas' {SelDataForm},
  View in 'View.pas' {ViewForm},
  SceltaAcquisiz in 'SceltaAcquisiz.pas' {SceltaAcquisizForm},
  InsEvento in 'InsEvento.pas' {InsEventoForm},
  GestStorico in 'GestStorico.pas' {GestStoricoForm},
  NuovaComp in 'NuovaComp.pas' {NuovaCompForm},
  QRStorico in 'QRStorico.pas' {QRStoricoForm},
  MDRicerche in 'MDRicerche.pas' {DataRicerche: TDataModule},
  Specifiche in 'Specifiche.pas' {SpecificheForm},
  ElencoRicPend in 'ElencoRicPend.pas' {ElencoRicPendForm},
  ContattoCand in 'ContattoCand.pas' {ContattoCandForm},
  StoriaContatti in 'StoriaContatti.pas' {StoriaContattiForm},
  ModuloDati4 in 'ModuloDati4.pas' {DataAnnunci: TDataModule},
  SelCliente in 'SelCliente.pas' {SelClienteForm},
  EsitoColloquio in 'EsitoColloquio.pas' {EsitoColloquioForm},
  FissaColloquio in 'FissaColloquio.pas' {FissaColloquioForm},
  InSelezione in 'InSelezione.pas' {InSelezioneForm},
  Inserimento in 'Inserimento.pas' {InserimentoForm},
  InsInserimento in 'InsInserimento.pas' {InsInserimentoForm},
  RicClienti in 'RicClienti.pas' {RicClientiForm},
  EspLav in 'EspLav.pas' {EspLavForm},
  Azienda in 'Azienda.pas' {AziendaForm},
  OffertaCliente in 'OffertaCliente.pas' {OffertaClienteForm},
  ContattoCliFatto in 'ContattoCliFatto.pas' {ContattoCliFattoForm},
  FaxPresentaz in 'FaxPresentaz.pas' {FaxPresentazForm},
  uGestEventi in 'uGestEventi.pas',
  SchedaCand in 'SchedaCand.pas' {SchedaCandForm},
  uUtilsVarie in 'uUtilsVarie.pas',
  InsCompetenza in 'InsCompetenza.pas' {InsCompetenzaForm},
  SelEdiz in 'SelEdiz.pas' {SelEdizForm},
  InsAnnuncio in 'InsAnnuncio.pas' {InsAnnuncioForm},
  Comuni in 'Comuni.pas' {ComuniForm},
  ZoneComuni in 'ZoneComuni.pas' {ZoneComuniForm},
  Fattura in 'Fattura.pas' {FatturaForm},
  InsDettFatt in 'InsDettFatt.pas' {InsDettFattForm},
  Presentaz in 'Presentaz.pas' {PresentazForm},
  RiepTiming in 'RiepTiming.pas' {RiepTimingForm},
  RepElencoAnnCand in 'RepElencoAnnCand.pas' {QRElencoAnnCand},
  ModDettCandRic in 'ModDettCandRic.pas' {ModDettCandRicForm},
  ModStatoRic in 'ModStatoRic.pas' {ModStatoRicForm},
  StoricoRic in 'StoricoRic.pas' {StoricoRicForm},
  SelAttivita in 'SelAttivita.pas' {SelAttivitaForm},
  OpzioniEliminaz in 'OpzioniEliminaz.pas' {OpzioniEliminazForm},
  CheckPass in 'CheckPass.pas',
  NuoviCV in 'NuoviCV.pas' {NuoviCVForm},
  InsNodoOrg in 'InsNodoOrg.pas' {InsNodoOrgForm},
  ElencoAziende in 'ElencoAziende.pas' {ElencoAziendeForm},
  NotaSpese in 'NotaSpese.pas' {NotaSpeseForm},
  ElencoUtenti in 'ElencoUtenti.pas' {ElencoUtentiForm},
  ElencoFattCliente in 'ElencoFattCliente.pas' {ElencoFattClienteForm},
  DubbiIns in 'DubbiIns.pas' {DubbiInsForm},
  Compenso in 'Compenso.pas' {CompensoForm},
  Risorsa in 'Risorsa.pas' {RisorsaForm},
  ContattoCli in 'ContattoCli.pas' {ContattoCliForm},
  ModFatt in 'ModFatt.pas' {ModFattForm},
  InsCliente in 'InsCliente.pas' {InsClienteForm},
  Competenza in 'Competenza.pas' {CompetenzaForm},
  EmailConnectInfo in 'EmailConnectInfo.pas' {EmailConnectInfoForm},
  TipiMailCand in 'TipiMailCand.pas' {TipiMailCandForm},
  RepAgenda in 'RepAgenda.pas' {QRAgenda},
  EmailMessage in 'EmailMessage.pas' {EmailMessageForm},
  LegendaCandUtente in 'LegendaCandUtente.pas' {LegendaCandUtenteForm},
  LegendaGestRic in 'LegendaGestRic.pas' {LegendaGestRicForm},
  ValutazColloquio in 'ValutazColloquio.pas' {ValutazColloquioForm},
  ValutazCollDett in 'ValutazCollDett.pas' {ValutazCollDettForm},
  AnagFile in 'AnagFile.pas' {AnagFileForm},
  SchedaSintetica in 'SchedaSintetica.pas' {SchedaSinteticaForm},
  SelRicCand in 'SelRicCand.pas' {SelRicCandForm},
  RifClienteDati in 'RifClienteDati.pas' {RifClienteDatiForm},
  SelPersNew in 'SelPersNew.pas' {SelPersNewForm},
  LegendaRic in 'LegendaRic.pas' {LegendaRicForm},
  ElencoRicCliente in 'ElencoRicCliente.pas' {ElencoRicClienteForm},
  SceltaModelloAnag in 'SceltaModelloAnag.pas' {SceltaModelloAnagForm},
  SceltaModelloMailCli in 'SceltaModelloMailCli.pas' {SceltaModelloMailCliForm},
  StoricoOfferta in 'StoricoOfferta.pas' {StoricoOffertaForm},
  Fatture in 'Fatture.pas' {FattureForm},
  SelContatti in 'SelContatti.pas' {SelContattiForm},
  DettOfferta in 'DettOfferta.pas' {DettOffertaForm},
  TipoCommessa in 'TipoCommessa.pas' {TipoCommessaForm},
  SelTipoCommessa in 'SelTipoCommessa.pas' {SelTipoCommessaForm},
  CreaCompensi in 'CreaCompensi.pas' {CreaCompensiForm},
  DettAnnuncio in 'DettAnnuncio.pas' {DettAnnuncioForm},
  InsRicerca in 'InsRicerca.pas' {InsRicercaForm},
  DettSede in 'DettSede.pas' {DettSedeForm},
  ElencoSedi in 'ElencoSedi.pas' {ElencoSediForm},
  CandUtente in 'CandUtente.pas' {CandUtenteFrame: TFrame},
  LegendaElRic in 'LegendaElRic.pas' {LegendaElRicForm},
  DateInserimento in 'DateInserimento.pas' {DateInserimentoForm},
  Comune in 'Comune.pas' {ComuneForm},
  OrgStandard in 'OrgStandard.pas' {OrgStandardForm},
  NuovoSoggetto in 'NuovoSoggetto.pas' {NuovoSoggettoForm},
  Nazione in 'Nazione.pas' {NazioneForm},
  SelNazione in 'SelNazione.pas' {SelNazioneForm},
  ModRuolo in 'ModRuolo.pas' {ModRuoloForm},
  CliCandidati in 'CliCandidati.pas' {CliCandidatiFrame: TFrame},
  Uscita in 'Uscita.pas' {UscitaForm},
  QREtichette2 in 'QREtichette2.pas' {QREtichette2Form: TQuickRep},
  OpzioniEtichette in 'OpzioniEtichette.pas' {OpzioniEtichetteForm},
  EliminaDipOrg in 'EliminaDipOrg.pas' {EliminaDipOrgForm},
  FrmStoricoAnagPos in 'FrmStoricoAnagPos.pas' {StoricoAnagPosFrame: TFrame},
  ModStoricoAnagPos in 'ModStoricoAnagPos.pas' {ModStoricoAnagPosForm},
  StoricoAnagPos in 'StoricoAnagPos.pas' {StoricoAnagPosForm},
  FrmListinoAnnunci in 'FrmListinoAnnunci.pas' {ListinoAnnunciFrame: TFrame},
  PrezzoListino in 'PrezzoListino.pas' {PrezzoListinoForm},
  StoricoPrezziAnnunciCli in 'StoricoPrezziAnnunciCli.pas' {StoricoPrezziAnnunciCliForm},
  Testata in 'Testata.pas' {TestataForm},
  Edizione in 'Edizione.pas' {EdizioneForm},
  ElencoTipiAnnunci in 'ElencoTipiAnnunci.pas' {ElencoTipiAnnunciForm},
  TipiAnnunciFrame in 'TipiAnnunciFrame.pas' {FrmTipiAnnunci: TFrame},
  FrmClienteAnnunci in 'FrmClienteAnnunci.pas' {ClienteAnnunciFrame: TFrame},
  SelTestataEdizione in 'SelTestataEdizione.pas' {SelTestataEdizioneForm},
  ContrattoAnnunci in 'ContrattoAnnunci.pas' {ContrattoAnnunciForm},
  FrmContrattiAnnunci in 'FrmContrattiAnnunci.pas' {ContrattiAnnunciFrame: TFrame},
  TipoStrada in 'TipoStrada.pas' {TipoStradaForm},
  SceltaModello in 'SceltaModello.pas' {SceltaModelloForm},
  GestModelliWord in 'GestModelliWord.pas' {GestModelliWordForm},
  SelTabGen in 'SelTabGen.pas' {SelTabGenForm},
  Contratto in 'Contratto.pas' {ContrattoForm},
  SelContrattoCli in 'SelContrattoCli.pas' {SelContrattoCliForm},
  ModPrezzo in 'ModPrezzo.pas' {ModPrezzoForm},
  GridDB in 'GridDB.pas' {FrameDB: TFrame},
  FrmFrameDB in 'FrmFrameDB.pas' {FrameDBForm},
  UnitPrintMemo in 'UnitPrintMemo.pas',
  FrameDBRichEdit2 in 'FrameDBRichEdit2.pas' {FrameDBRich: TFrame},
  AnagAnnunci in 'AnagAnnunci.pas' {AnagAnnunciForm},
  SelFromQuery in 'SelFromQuery.pas' {SelFromQueryForm},
  Costo in 'Costo.pas' {CostoForm},
  ArchivioMess in 'ArchivioMess.pas' {ArchivioMessForm},
  StoricoTargetList in 'StoricoTargetList.pas' {StoricoTargetListForm},
  NoteCliente in 'NoteCliente.pas' {NoteClienteForm},
  SelQualifContr in 'SelQualifContr.pas' {SelQualifContrForm},
  TabContratti in 'TabContratti.pas' {TabContrattiForm},
  ConoscInfo in 'ConoscInfo.pas' {ConoscInfoForm},
  AnagConoscInfo in 'AnagConoscInfo.pas' {FrmAnagConoscInfo: TFrame},
  CliAltriSettori in 'CliAltriSettori.pas' {CliAltriSettoriForm},
  FrmDatiRetribuz in 'FrmDatiRetribuz.pas' {DatiRetribuzFrame: TFrame},
  EspLavRetrib in 'EspLavRetrib.pas' {EspLavRetribForm},
  LogOperazioniSogg in 'LogOperazioniSogg.pas' {LogOperazioniSoggForm},
  RicFile in 'RicFile.pas' {RicFileForm},
  FrmMultiDBCheckBox in 'FrmMultiDBCheckBox.pas' {MultiDBCheckBoxFrame: TFrame},
  uRelazioni in 'ASA\uRelazioni.pas' {RelazioniForm},
  uASACand in 'ASA\uASACand.pas' {ASASchedaCandForm},
  uAzCandPresenti in 'ASA\uAzCandPresenti.pas' {AzCandPresentiForm},
  uAzOfferte in 'ASA\uAzOfferte.pas' {AzOfferteForm},
  uDettagliCand in 'ASA\uDettagliCand.pas' {DettagliCandForm},
  uASAAzienda in 'ASA\uASAAzienda.pas' {ASAAziendaForm},
  uAnagFile in 'ASA\uAnagFile.pas' {FileSoggettoForm},
  uASAcommessa in 'ASA\uASAcommessa.pas' {ASAcommessaForm},
  SplashNew in 'SplashNew.pas' {SplashNewForm},
  StoricoInviiContatto in 'StoricoInviiContatto.pas' {StoricoInviiContattoForm},
  Indirizzo in 'Indirizzo.pas' {IndirizzoForm},
  RiepilogoCand in 'RiepilogoCand.pas' {RiepilogoCandForm},
  InfoColloquio in 'InfoColloquio.pas' {InfoColloquioForm},
  MyQuickView in 'MyQuickView.pas' {MyQuickViewForm},
  MyQuickViewConfig in 'MyQuickViewConfig.pas' {MyQuickViewConfigForm},
  SelEspLav in 'SelEspLav.pas' {SelEspLavForm},
  SelCriterioWizard in 'SelCriterioWizard.pas' {SelCriterioWizardForm},
  GestEspLavMotInsModel in 'GestEspLavMotInsModel.pas' {GestEspLavMotInsModelForm},
  GestQualifContr in 'GestQualifContr.pas' {GestQualifContrForm},
  QMmessage in 'QMmessage.pas' {QMmessageForm},
  uQM in 'uQM.pas',
  QMDettaglio in 'QMDettaglio.pas' {QMDettaglioForm},
  CaricaPriimiProgress in 'CaricaPriimiProgress.pas' {CaricaPrimiProgressForm},
  uAbout_old in 'uAbout_old.pas' {AboutForm_Old},
  Utente in 'Utente.pas' {UtenteForm},
  frmAnagQuest in 'frmAnagQuest.pas' {AnagQuestFrame: TFrame},
  StatisticheFrame in 'StatisticheFrame.pas' {FrmStatistiche: TFrame},
  NuovoUtente in 'NuovoUtente.pas' {NuovoUtenteForm},
  sysutils,
  uAziendaFile in 'ASA\uAziendaFile.pas' {FileAziendaForm},
  uDettHistory in 'ASA\uDettHistory.pas' {DettHistoryForm},
  uInsEvento in 'ASA\uInsEvento.pas' {ASAInsEventoForm},
  uAnagFile2 in 'ASA\uAnagFile2.pas' {FileSoggetto2Form},
  ModelloWord in 'ModelloWord.pas' {ModelloWordForm},
  uASAInsRuolo in 'ASA\uASAInsRuolo.pas' {InsRuoloAsaForm},
  SettaColoriGrigliaCand in 'SettaColoriGrigliaCand.pas' {SettaColoriGrigliaCandForm},
  AnnuncioLink in 'AnnuncioLink.pas' {AnnuncioLinkForm},
  IndirizziCliente in 'IndirizziCliente.pas' {IndirizziClienteForm},
  InsIndirizzo in 'InsIndirizzo.pas' {InsindirizzoForm},
  RelazioniCand in 'RelazioniCand.pas' {RelazioniCandFrame: TFrame},
  RelazCand in 'RelazCand.pas' {RelazCandForm},
  InputPassword in 'InputPassword.pas' {InputPasswordForm},
  ContattoConCliente in 'ContattoConCliente.pas' {ContattoConClienteForm},
  Help in 'Help.pas' {HelpForm},
  FileAperto in 'FileAperto.pas' {FileApertoForm},
  FrmCampiPers in 'FrmCampiPers.pas' {CampiPersFrame: TFrame},
  DefCampiPers in 'DefCampiPers.pas' {DefCampiPersForm},
  Offerte in 'Offerte.pas' {OfferteForm},
  InsNote in 'InsNote.pas' {InsNoteForm},
  StoricoAnag in 'StoricoAnag.pas' {StoricoAnagForm},
  motoriricerca in 'motoriricerca.pas' {formMotoriRicerca},
  LeggiNote in 'LeggiNote.pas' {LeggiNoteForm},
  RelazCand_config in 'RelazCand_config.pas' {RelazCand_configForm},
  FrmWebLinks in 'FrmWebLinks.pas' {FrameWebLinks: TFrame},
  GestSitiWeb in 'GestSitiWeb.pas' {GestSitiWebForm},
  RicCandAnnunci in 'RicCandAnnunci.pas' {RicCandAnnunciForm},
  FotoSegreta in 'FotoSegreta.pas' {FotoSegretaForm},
  DettaglioNote in 'DettaglioNote.pas' {DettaglioNoteForm},
  TabVociValutazCandStd in 'TabVociValutazCandStd.pas' {TabVociValutazCandStdForm},
  NuovaValutazCand in 'NuovaValutazCand.pas' {NuovaValutazCandForm},
  TabVociValutazCand in 'TabVociValutazCand.pas' {TabVociValutazCandForm},
  GoogleApi in 'GoogleApi.pas',
  CodFiscale in 'CodFiscale.pas',
  AccFiliali in 'AccFiliali.pas' {AccFilialiForm},
  uMultiposting in 'uMultiposting.pas',
  TLHelp32,
  uGestTab in 'uGestTab.pas' {GestTabForm},
  AnagQuestRisposteDate in 'AnagQuestRisposteDate.pas' {AnagQuestRisposteDateForm},
  DettAnagQuest in 'DettAnagQuest.pas' {DettAnagQuestForm},
  SelQuest in 'SelQuest.pas' {SelQuestForm},
  VideoSegreto in 'VideoSegreto.pas' {VideoSegretoForm},
  uAnnunciRoutines in 'uAnnunciRoutines.pas',
  GraficaH1 in 'GraficaH1.pas',
  Foto in 'Foto.pas' {FotoForm},
  FrmLinksHome in 'FrmLinksHome.pas' {FrmLinkwebHome: TFrame},
  uTipiFileCand in 'uTipiFileCand.pas' {TipiFileCandForm},
  uPirola in 'uPirola.pas' {PirolaForm},
  frmGestFile in 'frmGestFile.pas' {GestFileFrame: TFrame},
  GestFile in 'GestFile.pas' {GestFileForm},
  SelRegProv in 'SelRegProv.pas' {SelRegProvForm},
  AnagTestELearning in 'AnagTestELearning.pas' {AnagTestelearningForm},
  TestELearning in 'TestELearning.pas' {TestELearningForm},
  uSendMailMAPI in 'uSendMailMAPI.pas',
  uTest in 'uTest.pas' {TestForm},
  uOLEMessageFilter in 'uOLEMessageFilter.pas',
  MotiviSospensione in 'MotiviSospensione.pas' {MotiviSospensioneForm},
  MotiviRichiamoCand in 'MotiviRichiamoCand.pas' {MotiviRichiamoCandForm},
  TabCatProfess in 'TabCatProfess.pas' {TabCatProfessForm},
  InsAreaNew in 'InsAreaNew.pas' {InsAreaNewForm},
  ComunitaNazStato in 'ComunitaNazStato.pas' {ComunitaNazStatoForm},
  ModelliWord in 'ModelliWord.pas',
  uModelliWord_remote in 'uModelliWord_remote.pas',
  GoogleMap in 'GoogleMap.pas' {GoogleMapForm},
  uSocialApi in 'uSocialApi.pas',
  SceltaAnagFile in 'SceltaAnagFile.pas' {SceltaAnagFileForm},
  ScegliAnagFile in 'ScegliAnagFile.pas' {ScegliAnagFileForm},
  EliminaCampoPers in 'EliminaCampoPers.pas' {EliminaCampoPersForm},
  TabCondizioniOfferte in 'TabCondizioniOfferte.pas' {TabCondizioniOfferteForm},
  EBCLink in 'EBCLink.pas' {EBCLinkForm},
  uAbout in 'uAbout.pas' {AboutForm},
  uAnnuncioLink2 in 'uAnnuncioLink2.pas' {AnnuncioLinkForm2},
  StoricoAnagrafica in 'StoricoAnagrafica.pas' {StoricoAnagraficaForm},
  ModificaStoricoAnagrafica in 'ModificaStoricoAnagrafica.pas' {ModificaStoricoAnagraficaForm},
  u_AttivaWorkflow in 'u_AttivaWorkflow.pas' {AttivaworkflowForm},
  u_ElencoWorkFlow in 'u_ElencoWorkFlow.pas' {ElencoWorkFlowForm};

{$R *.RES}

var xResult, xVersione: string;


function ElencoProcessi(Processo: string): integer;
var
     pe: TProcessEntry32;
     hSnap: THandle;
     n: integer;
begin
     hSnap := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

     //  form1.Memo1.Lines.Add('//////////////////// Elenco Processi ////////////////////////////');
     pe.dwSize := sizeof(TProcessEntry32);
     //Prelevo informazioni sul primo processo nello snapshot di sistema
     Process32First(hSnap, pe);
     n := 0;
     repeat //loop sui processi
          with pe do
          begin
               if szExeFile = processo then begin
                    n := n + 1;
               end;
          end;
     until (not (Process32Next(hSnap, pe)));

     CloseHandle(hSnap);
     ElencoProcessi := n;
end;

procedure DisableProcessWindowsGhosting;
{var
     DisableProcessWindowsGhostingImp: procedure;
begin
     @DisableProcessWindowsGhostingImp :=
          GetProcAddress(GetModuleHandle('%windir%\SysWOW64\user32.dll'), 'DisableProcessWindowsGhosting');
     if (@DisableProcessWindowsGhostingImp <> nil) then
          DisableProcessWindowsGhostingImp;   }
var
     User32: HMODULE;
     DisableProcessWindowsGhosting: procedure; StdCall;
begin
     User32 := GetModuleHandle('USER32');
     if User32 <> 0 then begin
          DisableProcessWindowsGhosting := GetProcAddress(User32,
               'DisableProcessWindowsGhosting');
          if Assigned(DisableProcessWindowsGhosting) then
               DisableProcessWindowsGhosting;
     end;

end;




var x: integer;
begin
     //possibilit� di lanciare h1sel almeno due volte
     //chiesto da Vercellone intermedia perch� altrimenti se sono su una commessa e un candidato gli tel loro devono chiudere la commessa per andare in anagrafica
     x := ElencoProcessi(ExtractFileName(application.exename)); // attenzione a minucole e maiuscole
     //showmessage(inttostr(x));
   // if x > 2 then begin
   //       SendMessage(HWND_BROADCAST,
  //             RegisterWindowMessage('H1Sel'),
   //            0,
    //           0);
          {Lets quit}
    //      Halt(0);
    // end;   // cavato perch� i clienti che si collegano in rdp per lanciare h1 non possono usarlo pi� di due
    //era stato messo per intermedia perch� vogliono lanciare 2 h1 contemporanemanete

//questo codice evita la possibilit� di aprire pi� volte h1sel
       {Attempt to create a named mutex}
    /// CreateMutex(nil, false, 'H1Sel')
     {if it failed then there is another instance}
     ///     if GetLastError = ERROR_ALREADY_EXISTS then begin
          {Send all windows our custom message - only our other}
          {instance will recognise it, and restore itself}
      ///         SendMessage(HWND_BROADCAST,
     ///              RegisterWindowMessage('H1Sel'),
      ///              0,
          ///         0);
          {Lets quit}
       ///        Halt(0);
     ///     end;
    // end;


     SplashNewForm := TSplashNewForm.Create(nil);
     SplashNewForm.Prog.Position := 0;
     SplashNewForm.Show;
     SplashNewForm.Update;

     try
          xVersione := '5.12.1';
          //il terzo numero � stato inserito per gestire la variazione di versione per gli utenti che effettuano il download. l'ultimo numero continier�
          //a crescere per ogni versione metre il terzo cambier� solo prima di una nuova release per il download o a seguito della variazione della minor release



                  // Disable Window Ghosting for the entire application.
          DisableProcessWindowsGhosting;

          Application.Initialize;
          {Tell Delphi to un-hide it's hidden application window}
          {This allows our instance to have a icon on the task bar}

          //Application.ShowMainForm := true;
          //ShowWindow(Application.Handle, SW_RESTORE);

          Application.Title := 'H1sel - Modulo Gestione Ricerca e Selezione';
          Application.HelpFile := 'Manh1sel.hlp';
          SplashNewForm.Prog.Position := 1;
          Application.CreateForm(TData, Data);
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TElencoWorkFlowForm, ElencoWorkFlowForm);

  data.Global.Open;
          if data.Global.FieldByName('GraficaSel').asinteger = 2 then begin
               with mainform do begin
                    // BorderStyle := bsNone;
                    Height := 768;
                    width := 1024;
                    VisibCavalieri := false;
                    dxNavBar1.Visible := false;
                    StatusBar1.Visible := false;
                    AdvToolBarPager1.Visible := true;
                    AdvToolBarPager1.Collaps;
                    AdvOfficeStatusBar1.visible := true;
                    // Dock973.visible := false;
                    TbMain.Visible := false;
                    TbClienti.visible := false;
               end
          end else begin
               MainForm.Height := 630;
               MainForm.Width := 820;
               mainform.VisibCavalieri := true;
               MainForm.AdvToolBarPager1.Visible := false;
               MainForm.dxNavBar1.Visible := true;
               MainForm.dxNavBar1.Width:=104;
          end;

          if data.Global.FieldByName('GraficaSel').asinteger = 2 then begin
               with mainform do begin

                    //anagrafica
                    TSAnagSintesi.tabvisible := VisibCavalieri;
                    TSAnagDatiAnag.tabvisible := VisibCavalieri;
                    TSAnagRicerche.tabvisible := VisibCavalieri;
                    TSAnagComp.tabvisible := VisibCavalieri;
                    TsAnagMans.tabvisible := VisibCavalieri;
                    TSAnagStorico.tabvisible := VisibCavalieri;
                    TSAnagNoteInt.tabvisible := VisibCavalieri;
                    TSAnagFile.tabvisible := VisibCavalieri;
                    tsAnagQuest.tabvisible := VisibCavalieri;
                    TSCampPers.tabvisible := VisibCavalieri;
                    TSAnagWeb.tabvisible := VisibCavalieri;

                    // clienti
                    TSCliSintesi.tabvisible := VisibCavalieri;
                    TSClientiDett.tabvisible := VisibCavalieri;
                    TSClientiRic.tabvisible := VisibCavalieri;
                    TSClientiCV.tabvisible := VisibCavalieri;
                    TSClientiAnnunci.tabvisible := VisibCavalieri;
                    TSClientiNoteSpese.tabvisible := VisibCavalieri;
                    TSClientiFatt.tabvisible := VisibCavalieri;
                    TSContattiOfferte.tabvisible := VisibCavalieri;
                    TSClientiContratto.tabvisible := VisibCavalieri;
                    TSCliContatti.tabvisible := VisibCavalieri;
                    TSCliForn.tabvisible := VisibCavalieri;
                    TsCampiPers.tabvisible := VisibCavalieri;

                    //tabelle
                    TSTabFlussi.tabvisible := VisibCavalieri;
                    TsTabCompetenze.tabvisible := VisibCavalieri;
                    TSTabCaratt.tabvisible := VisibCavalieri;
                    TsTabMansioni.tabvisible := VisibCavalieri;
                    TSTabAltro.tabvisible := VisibCavalieri;
                    TSTabDiplomi.tabvisible := VisibCavalieri;
                    TSTabAttiv.tabvisible := VisibCavalieri;
                    TSTabAziende.tabvisible := VisibCavalieri;
                    TSTabLingue.tabvisible := VisibCavalieri;
                    TSTabCom.tabvisible := VisibCavalieri;
                    TSTabNaz.tabvisible := VisibCavalieri;
                    TSFiliali.tabvisible := VisibCavalieri;
                    TSMotiviSospensione.TabVisible := VisibCavalieri;

                    //annunci
                    TSAnnDett.tabvisible := VisibCavalieri;
                    TSAnnDate.tabvisible := VisibCavalieri;
                    TSAnnCVPerv.tabvisible := VisibCavalieri;
                    TSAnnTesto.tabvisible := VisibCavalieri;
                    TSAnnTimeSheet.tabvisible := VisibCavalieri;
                    TSJobPilot.tabvisible := VisibCavalieri;
                    TSAnnCampiPers.tabvisible := VisibCavalieri;
                    Wallpaper43.Visible := VisibCavalieri;


                    ToolbarButton9742.Visible := false;
                    AdvToolBarPagerOrganigramma.TabVisible := false;
                    //organigramma
                    {TSAziendeOrgCand.tabvisible := VisibCavalieri;
                    TSAziendaOrgDett.tabvisible := VisibCavalieri;
                    TSAziendaOrgDati.tabvisible := VisibCavalieri;
                    TSAziendaOrgRuolo.tabvisible := VisibCavalieri;
                    TsOrgCampPers.tabvisible := VisibCavalieri; }

               end;
          end;


          Application.CreateForm(TDettaglioNoteForm, DettaglioNoteForm);
          if not Data.CheckVersione(xVersione) then xVersione := xVersione + '*';
          MainForm.Caption := 'H1-Sel - Modulo per la Ricerca e la Selezione (versione ' + xVersione + ')';

          Application.CreateForm(TFrameDBForm, FrameDBForm);
          Application.CreateForm(TMyQuickViewForm, MyQuickViewForm);
          Application.CreateForm(TQMmessageForm, QMmessageForm);
          Application.CreateForm(TUtenteForm, UtenteForm);
          Application.CreateForm(TFileAziendaForm, FileAziendaForm);
          Application.CreateForm(TDettHistoryForm, DettHistoryForm);
          Application.CreateForm(TASAInsEventoForm, ASAInsEventoForm);
          Application.CreateForm(TFileSoggetto2Form, FileSoggetto2Form);
          Application.CreateForm(TModelloWordForm, ModelloWordForm);
          Application.CreateForm(TInputPasswordForm, InputPasswordForm);
          Application.CreateForm(THelpForm, HelpForm);
          SplashNewForm.Prog.Position := 2; //2;
          //Application.CreateForm(TData, Data);
          SplashNewForm.Prog.Position := 3;
          Application.CreateForm(TCurriculumForm, CurriculumForm);
          SplashNewForm.Prog.Position := 4;
          Application.CreateForm(TData2, Data2);
          SplashNewForm.Prog.Position := 5;
          SplashNewForm.Prog.Position := 6;
          SplashNewForm.Prog.Position := 7;
          Application.CreateForm(TDataRicerche, DataRicerche);
          SplashNewForm.Prog.Position := 8;
          Application.CreateForm(TDataAnnunci, DataAnnunci);
          SplashNewForm.Prog.Position := 9;
          Application.CreateForm(TLoginForm, LoginForm);


          SplashNewForm.Hide;
          SplashNewForm.Free;

          LoginForm.Accedi := 'N';
          if not Data.Global.Active then Data.Global.Open;

          LoginForm.Username.Text := Data.Global.FieldByname('LastLogin').AsString;

          xResult := LoginForm.AccessVerify;
          if data.Global.FieldByName('debughrms').asboolean = true then ShowMessage('H1Sel.dpr: LoginForm.AccessVerify con risultato = ' + xResult);

          if xResult <> '' then begin
               showMessage('PROBLEMI CON LA LICENZA:' + #13 + xResult);
               MainForm.Free;
               Application.Terminate;
          end else begin
               {LoginForm.ShowModal;
               if LoginForm.Accedi = 'S' then begin }
               if data.Global.FieldByName('debughrms').asboolean = true then ShowMessage('H1Sel.dpr: Prima di LoginForm.CheckUserName');
               if LoginForm.CheckUserName <> '' then begin
                    if data.Global.FieldByName('debughrms').asboolean = true then ShowMessage('H1Sel.dpr: Utente esistente');
                    Application.Run
               end else begin
                    if data.Global.FieldByName('debughrms').asboolean = true then ShowMessage('H1Sel.dpr: Utente NON esistente');
                    if LoginForm.xNomeUtente_file <> '' then begin
                         MessageDlg('ERRORE: L''utente "' + LoginForm.xNomeUtente_file + '" non esiste oppure � autorizzato ad accedere ad H1Sel', mtError, [mbOk], 0);
                    end else begin
                         Data.Q1.Close;
                         Data.Q1.SQL.Text := 'select system_user as utente';
                         Data.Q1.Open;
                         //MessageDlg('ERRORE: L''utente ' + Data.Q1.FieldByName('Utente').asString + ' non esiste oppure � autorizzato ad accedere ad H1Sel', mtError, [mbOk], 0);
                         MessageDlg('ERRORE: Accesso negato', mtError, [mbOk], 0);
                         Data.Q1.Close;
                    end;

                    MainForm.Free;
                    Application.Terminate;
               end;
          end;
     except
          on Exception do raise;
     end;
end.

