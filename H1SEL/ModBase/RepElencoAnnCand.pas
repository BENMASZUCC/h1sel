unit RepElencoAnnCand;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, DBTables, Qrctrls, QuickRpt, ExtCtrls;

type
     TQRElencoAnnCand = class(TForm)
          QuickRep1: TQuickRep;
          DetailBand1: TQRBand;
          PageFooterBand1: TQRBand;
          TitleBand1: TQRBand;
          ColumnHeaderBand1: TQRBand;
          QRLabel1: TQRLabel;
          QRLabel2: TQRLabel;
          QRDBText1: TQRDBText;
          QRDBText2: TQRDBText;
          QRDBText3: TQRDBText;
          QRDBText4: TQRDBText;
          QRDBText5: TQRDBText;
          QRDBText6: TQRDBText;
          QRShape1: TQRShape;
          QRLabel4: TQRLabel;
          QRLabel5: TQRLabel;
          QRLabel6: TQRLabel;
          QRLabel3: TQRLabel;
          QRSysData1: TQRSysData;
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     QRElencoAnnCand: TQRElencoAnnCand;

implementation

uses ModuloDati4;

{$R *.DFM}

end.
