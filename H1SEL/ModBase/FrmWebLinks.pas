unit FrmWebLinks;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     dxDBTLCl, dxGrClms, dxTL, dxDBCtrl, dxDBGrid, Db, dxCntner, ADODB,
     ExtCtrls, Buttons, DBCtrls, dxExEdtr, dxEdLib, dxDBELib, jpeg, TB97;

type
     TFrameWebLinks = class(TFrame)
          Panel1: TPanel;
          Panel2: TPanel;
          DsQWebLinks: TDataSource;
          QWebLinks: TADOQuery;
          dxDBGrid1: TdxDBGrid;
          QWebLinksID: TAutoIncField;
          QWebLinksDescrizione: TStringField;
          QWebLinksLogo: TBlobField;
          dxDBGrid1ID: TdxDBGridMaskColumn;
          dxDBGrid1Descrizione: TdxDBGridMaskColumn;
          OpenDialog1: TOpenDialog;
          dxDBGrid1Column4: TdxDBGridGraphicColumn;
          QWebLinksLink: TStringField;
          dxDBGrid1Column5: TdxDBGridHyperLinkColumn;
    BCan: TToolbarButton97;
    BOK: TToolbarButton97;
    SpeedButton1: TToolbarButton97;
          procedure dxDBGrid1Column4GetGraphicClass(Sender: TObject;
               Node: TdxTreeListNode; var GraphicClass: TGraphicClass);
          procedure QWebLinksBeforeOpen(DataSet: TDataSet);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BOKClick(Sender: TObject);
    procedure BCanClick(Sender: TObject);
    procedure DsQWebLinksStateChange(Sender: TObject);
     private
    { Private declarations }
     public
          xID: integer;
          procedure InsRecords;
     end;

implementation

uses ModuloDati, GestSitiWeb;

{$R *.DFM}

procedure TFrameWebLinks.dxDBGrid1Column4GetGraphicClass(Sender: TObject;
     Node: TdxTreeListNode; var GraphicClass: TGraphicClass);
begin
     GraphicClass := TJPEGImage;
end;

procedure TFrameWebLinks.QWebLinksBeforeOpen(DataSet: TDataSet);
begin
     QWebLinks.Parameters[0].value := xID;
end;

procedure TFrameWebLinks.InsRecords;
begin
     Data.QTemp.Close;
     Data.QTemp.SQL.clear;        
     Data.QTemp.SQL.text := 'insert into AnagWebLinks (IDAnagrafica,IDWebLink) ' +
          ' select :x0, WL.ID from WebLinks WL ' +
          ' where ID not in (select IDWebLink from AnagWebLinks where IDAnagrafica=:x1)';
     Data.QTemp.Parameters[0].value := xID;
     Data.QTemp.Parameters[1].value := xID;
     Data.QTemp.ExecSQL;
end;

procedure TFrameWebLinks.SpeedButton1Click(Sender: TObject);
begin
     GestSitiWebForm:=TGestSitiWebForm.create(self);
     GestSitiWebForm.ShowModal;
     if GestSitiWebForm.ModalResult=mrOK then begin
         QWebLinks.Close;
         InsRecords;
         QWebLinks.Open;
     end;
     GestSitiWebForm.Free;
end;

procedure TFrameWebLinks.BOKClick(Sender: TObject);
begin
     QWebLinks.Post;
end;

procedure TFrameWebLinks.BCanClick(Sender: TObject);
begin
     QWebLinks.Cancel;
end;

procedure TFrameWebLinks.DsQWebLinksStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsQWebLinks.State in [dsEdit, dsInsert];
     BOK.Enabled := b;
     BCan.Enabled := b;
end;

end.

