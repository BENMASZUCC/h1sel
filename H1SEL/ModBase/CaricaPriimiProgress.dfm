object CaricaPrimiProgressForm: TCaricaPrimiProgressForm
  Left = 276
  Top = 308
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'Caricamento anagrafica'
  ClientHeight = 45
  ClientWidth = 432
  Color = clInactiveBorder
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 107
    Top = 8
    Width = 155
    Height = 16
    Caption = 'Attendere elaborazione'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 280
    Top = 9
    Width = 3
    Height = 13
  end
  object ProgressBar1: TProgressBar
    Left = 8
    Top = 27
    Width = 417
    Height = 9
    Min = 0
    Max = 100
    Smooth = True
    TabOrder = 0
  end
end
