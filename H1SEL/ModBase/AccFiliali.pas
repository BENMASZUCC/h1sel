unit AccFiliali;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, ExtCtrls, dxDBTLCl, dxGrClms, dxTL, dxDBCtrl, dxDBGrid,
     dxCntner, Db, ADODB, TB97;

type
     TAccFilialiForm = class(TForm)
          Panel1: TPanel;
          Label1: TLabel;
          Panel2: TPanel;
          qAccFiliali: TADOQuery;
          dsAccFiliali: TDataSource;
          qAccFilialiID: TAutoIncField;
          qAccFilialiDescrizione: TStringField;
          qAccFilialiPercOrigine: TFloatField;
          qAccFilialiPercChiusura: TFloatField;
          qAccFilialiAttivo: TBooleanField;
          Panel3: TPanel;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1ID: TdxDBGridMaskColumn;
          dxDBGrid1Descrizione: TdxDBGridMaskColumn;
          dxDBGrid1PercOrigine: TdxDBGridMaskColumn;
          dxDBGrid1PercChiusura: TdxDBGridMaskColumn;
          dxDBGrid1Attivo: TdxDBGridCheckColumn;
          TbBAccFilialiNew: TToolbarButton97;
          TbBAccFilialiPost: TToolbarButton97;
          procedure TbBAccFilialiNewClick(Sender: TObject);
          procedure TbBAccFilialiPostClick(Sender: TObject);
          procedure Button1Click(Sender: TObject);
          procedure qAccFilialiBeforePost(DataSet: TDataSet);
          procedure dsAccFilialiStateChange(Sender: TObject);
         
          procedure FormShow(Sender: TObject);
     private
    { Private declarations }
     public
    { Public declarations }
     end;

var
     AccFilialiForm: TAccFilialiForm;

implementation

uses ModuloDati, Main;

{$R *.DFM}

procedure TAccFilialiForm.TbBAccFilialiNewClick(Sender: TObject);
begin
     qaccfiliali.Insert;
end;

procedure TAccFilialiForm.TbBAccFilialiPostClick(Sender: TObject);
begin
     qAccfiliali.Post;
end;

procedure TAccFilialiForm.Button1Click(Sender: TObject);
begin
     qAccFiliali.post;
end;


procedure TAccFilialiForm.qAccFilialiBeforePost(DataSet: TDataSet);
begin
     if qAccFilialiPercOrigine.Value + qAccFilialiPercChiusura.Value <> 100 then
          messagedlg('Attenzione: la somme delle 2 percentuali � diversa da 100!', mtWarning, [mbOk], 0);
end;

procedure TAccFilialiForm.dsAccFilialiStateChange(Sender: TObject);
var b: boolean;
begin
     b := dsAccFiliali.State in [dsEdit, dsInsert];
     TbBAccFilialiNew.Enabled := not b;
     TbBAccFilialiPost.Enabled := b;
end;



procedure TAccFilialiForm.FormShow(Sender: TObject);
begin
     //Grafica
     AccFilialiForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel3.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel3.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel3.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     if qAccFiliali.Active = false then qAccFiliali.Open;
end;

end.

