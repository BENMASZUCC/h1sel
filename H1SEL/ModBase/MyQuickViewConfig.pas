unit MyQuickViewConfig;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     TB97, dxTL, dxDBCtrl, dxDBGrid, dxCntner, ExtCtrls, Db, ADODB, StdCtrls,
     Buttons, dxDBTLCl, dxGrClms;

type
     TMyQuickViewConfigForm = class(TForm)
          Panel1: TPanel;
          QDisp: TADOQuery;
          DsQDisp: TDataSource;
          Panel2: TPanel;
          Splitter1: TSplitter;
          Panel3: TPanel;
          PanTitoloCompensi: TPanel;
          Panel4: TPanel;
          Panel5: TPanel;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1Voce: TdxDBGridMaskColumn;
          BInserisci: TToolbarButton97;
          BTogli: TToolbarButton97;
          dxDBGrid2: TdxDBGrid;
          QVisibili: TADOQuery;
          DsQVisibili: TDataSource;
          dxDBGrid2DicNodoMaster: TdxDBGridMaskColumn;
          dxDBGrid2Ordine: TdxDBGridMaskColumn;
          Q: TADOQuery;
          BTutte: TToolbarButton97;
          dxDBGrid1Column2: TdxDBGridGraphicColumn;
          BEsci: TToolbarButton97;
          procedure BInserisciClick(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure BTogliClick(Sender: TObject);
          procedure BTutteClick(Sender: TObject);
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
          procedure BEsciClick(Sender: TObject);
     private
          { Private declarations }
     public
          xIDUtente: integer;
     end;

var
     MyQuickViewConfigForm: TMyQuickViewConfigForm;

implementation

uses ModuloDati, Main;

{$R *.DFM}

procedure TMyQuickViewConfigForm.BInserisciClick(Sender: TObject);
begin
     // controllo se c'� gi�
     if not QVisibili.Locate('IDModelItem', QDisp.FieldByName('ID').asInteger, []) then begin
          // inserimento
          Q.SQL.text := 'insert into MyQuickViewUsers (IDUtente,IDModelItem) ' +
               'values (:xIDUtente,:xIDModelItem)';
          Q.Parameters[0].Value := xIDUtente;
          Q.Parameters[1].Value := QDisp.FieldByName('ID').asInteger;
          Q.ExecSQL;
     end;
     QVisibili.Close;
     QVisibili.Open;
end;

procedure TMyQuickViewConfigForm.FormShow(Sender: TObject);
begin
     QDisp.Open;
     QVisibili.Parameters[0].Value := xIDUtente;
     QVisibili.Open;

     //Grafici
     MyQuickViewConfigForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel3.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel3.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel3.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel4.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel4.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel4.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel5.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel5.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel5.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     PanTitoloCompensi.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     PanTitoloCompensi.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     PanTitoloCompensi.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
end;

procedure TMyQuickViewConfigForm.BTogliClick(Sender: TObject);
begin
     Q.SQL.text := 'delete from MyQuickViewUsers where ID=' + QVisibili.FieldByName('ID').asString;
     Q.ExecSQL;
     QVisibili.Close;
     QVisibili.Open;
end;

procedure TMyQuickViewConfigForm.BTutteClick(Sender: TObject);
begin
     // Tutte
     QDisp.First;
     QDisp.DisableControls;
     QVisibili.DisableControls;
     while not QDisp.EOF do begin
          BInserisciClick(self);
          QDisp.Next;
     end;
     QDisp.EnableControls;
     QVisibili.EnableControls;
end;

procedure TMyQuickViewConfigForm.FormClose(Sender: TObject;
     var Action: TCloseAction);
begin
     if DsQDisp.state = dsEdit then QDisp.Post;
end;

procedure TMyQuickViewConfigForm.BEsciClick(Sender: TObject);
begin
     ModalResult := mrOk;
end;

end.

