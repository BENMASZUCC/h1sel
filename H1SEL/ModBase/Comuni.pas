unit Comuni;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ExtCtrls, Grids, DBGrids, Db, DBTables, StdCtrls, Buttons, ADODB,
     U_ADOLinkCl, TB97;

type
     TComuniForm = class(TForm)
          DsComuni: TDataSource;
          DBGrid1: TDBGrid;
          RGOpzioni: TRadioGroup;
          PanProv: TPanel;
          Label1: TLabel;
          EProv: TEdit;
          Label2: TLabel;
          ECerca: TEdit;
          TZoneLK: TADOLinkedTable;
          QComuni: TADOLinkedQuery;
    Panel2: TPanel;
    BOK: TToolbarButton97;
    BAnnulla: TToolbarButton97;
    SpeedButton1: TToolbarButton97;
          procedure RGOpzioniClick(Sender: TObject);
          procedure SpeedButton1Click(Sender: TObject);
          procedure ECercaChange(Sender: TObject);
          procedure FormShow(Sender: TObject);
    procedure BOKClick(Sender: TObject);
    procedure BAnnullaClick(Sender: TObject);
     private
          xStringa: string;
     public
          { Public declarations }
     end;

var
     ComuniForm: TComuniForm;

implementation

{$R *.DFM}
//[/TONI20020726\]
uses ModuloDati, Main;
//[/TONI20020726\]FINE

procedure TComuniForm.RGOpzioniClick(Sender: TObject);
begin
     if RGOpzioni.ItemIndex = 0 then begin
          QComuni.Filtered := False;
          PanProv.Visible := False;
     end else PanProv.Visible := True;
end;

procedure TComuniForm.SpeedButton1Click(Sender: TObject);
begin
     QComuni.Filter := 'Prov=''' + EProv.Text + '''';
     QComuni.Filtered := True;
end;

procedure TComuniForm.ECercaChange(Sender: TObject);
begin
     QComuni.Close;
     QComuni.SQL.text := 'select T.*, R.CodRegioneH1 from tabcom T join TabRegioni R on T.IDRegione = R.ID ' +
          'where T.Descrizione like ''' + StringReplace(ECerca.Text, '''', '''''', [rfreplaceall]) + '%''';
     QComuni.Open;
end;

procedure TComuniForm.FormShow(Sender: TObject);
begin
//Grafica
     ComuniForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     RGOpzioni.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     PanProv.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     PanProv.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     PanProv.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     Caption := '[S/1] - ' + Caption;
end;

procedure TComuniForm.BOKClick(Sender: TObject);
begin
     ComuniForm.ModalResult := mrOK;
end;

procedure TComuniForm.BAnnullaClick(Sender: TObject);
begin
     ComuniForm.ModalResult := mrCancel;
end;

end.
