//[/TONI20021003]DEBUGOK
unit ElencoDip;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, Grids, DBGrids, Db, DBTables, ExtCtrls, ADODB,
     U_ADOLinkCl, dxDBTLCl, dxGrClms, dxTL, dxDBCtrl, dxDBGrid, dxCntner,
     TB97;

type
     TElencoDipForm = class(TForm)
          Panel1: TPanel;
          DsAnagDip: TDataSource;
          Panel2: TPanel;
          Label1: TLabel;
          ECerca: TEdit;
          TAnagDip: TADOLinkedQuery;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1Column1: TdxDBGridColumn;
          dxDBGrid1Column2: TdxDBGridColumn;
          dxDBGrid1Column3: TdxDBGridColumn;
          dxDBGrid1Column4: TdxDBGridColumn;
          dxDBGrid1Column5: TdxDBGridCheckColumn;
          BitBtn1: TToolbarButton97;
          BitBtn2: TToolbarButton97;
          SpeedButton1: TToolbarButton97;
    dxDBGrid1Column6: TdxDBGridColumn;
    dxDBGrid1Column7: TdxDBGridColumn;
          procedure DBGrid1KeyPress(Sender: TObject; var Key: Char);
          procedure DBGrid1DblClick(Sender: TObject);
          procedure BitBtn1Click(Sender: TObject);
          procedure Timer1Timer(Sender: TObject);
          procedure ECercaChange(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure SpeedButton1Click(Sender: TObject);
          procedure BitBtn2Click(Sender: TObject);
          procedure dxDBGrid1KeyPress(Sender: TObject; var Key: Char);
          procedure dxDBGrid1KeyDown(Sender: TObject; var Key: Word;
               Shift: TShiftState);
          procedure dxDBGrid1DblClick(Sender: TObject);
     private
          xStringa: string;
          { Private declarations }
     public
          { Public declarations }
     end;

var
     ElencoDipForm: TElencoDipForm;

implementation

uses uUtilsVarie,
     //[/TONI20020726\]
     ModuloDati, InsRuolo, Main;
//[/TONI20020726\]FINE

{$R *.DFM}

procedure TElencoDipForm.DBGrid1KeyPress(Sender: TObject; var Key: Char);
begin
     if Key = chr(13) then BitBtn1.Click;
end;

procedure TElencoDipForm.DBGrid1DblClick(Sender: TObject);
begin
     BitBtn1.Click;
end;

procedure TElencoDipForm.BitBtn1Click(Sender: TObject);
begin
     if TAnagDip.EOF then begin
          MessageDlg('Nessun soggetto selezionato !', mtError, [mbOK], 0);
          ModalResult := mrNone;
          Abort;
     end;
     //[/TONI20020726\]
     //     if not OkIDAnag(TAnagDipID.Value) then begin
     if not OkIDAnag(TAnagDip.FieldByName('ID').Value) then begin
          ModalResult := mrNone;
          Abort;
     end;
     //     if TAnagDipIDTipoStato.Value=3 then begin
     if TAnagDip.FieldByName('IDTipoStato').Value = 3 then begin
          //[/TONI20020726\]FINE
          if MessageDlg('Il candidato risulta INSERITO in azienda. Continuare?', mtWarning, [mbYes, mbNo], 0) = mrNo then begin
               ModalResult := mrNone;
               Abort;
          end;
     end;
     ElencoDipForm.ModalResult := mrOK;
end;

procedure TElencoDipForm.Timer1Timer(Sender: TObject);
begin
     xStringa := '';
end;

procedure TElencoDipForm.ECercaChange(Sender: TObject);
begin
     TAnagDip.Close;
     //[/TONI20020726\]
     //     TAnagDip.ParamByName('xCerca').asString:=ECerca.text+'%';
     {d
     TAnagDip.SetSQLText(['select Anagrafica.ID,Cognome,Nome,CVNumero,TipoStato,'+
          'IDTipoStato,IDProprietaCV,CVIDAnndata,CVInseritoInData from Anagrafica,EBC_TipiStato '+
               ' where Anagrafica.IDTipoStato=EBC_TipiStato.ID and Cognome like :xCerca: order by Cognome,Nome',
               ECerca.Text+'%']); }

     TAnagDip.SQL.Text := 'select Anagrafica.ID,anagrafica.dacontattocliente,anagrafica.datanascita,anagrafica.Titolo,Cognome,Nome,CVNumero,TipoStato,anagrafica.RecapitiTelefonici,anagrafica.Cellulare,' +
          ' anagrafica.Fax, Anagrafica.Email, Anagrafica.IDTipoStato,IDProprietaCV,CVIDAnndata,CVInseritoInData, s.Stato '+
           ' from Anagrafica ' +
           ' join EBC_TipiStato  on Anagrafica.IDTipoStato=EBC_TipiStato.ID ' +
           ' join EBC_Stati s on s.id=Anagrafica.IDStato '+
           ' where Cognome like :xCerca:  order by Cognome,Nome ';
     TAnagDip.ParamByName['xCerca:'] := '%' + ECerca.text + '%';
     //[/TONI20020726\]FINE
     TAnagDip.Open;
end;

procedure TElencoDipForm.FormShow(Sender: TObject);
begin
     Caption := '[S/14] - ' + Caption;
     ElencoDipForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
end;

procedure TElencoDipForm.SpeedButton1Click(Sender: TObject);
begin
     ApriHelp('353');
end;

procedure TElencoDipForm.BitBtn2Click(Sender: TObject);
begin
     ElencoDipForm.ModalResult := mrCancel;
end;

procedure TElencoDipForm.dxDBGrid1KeyPress(Sender: TObject; var Key: Char);
begin
     //if key = chr(13) then BitBtn1Click(self);
end;

procedure TElencoDipForm.dxDBGrid1KeyDown(Sender: TObject; var Key: Word;
     Shift: TShiftState);
begin
     //if key = chr(13) then BitBtn1Click(self);
end;

procedure TElencoDipForm.dxDBGrid1DblClick(Sender: TObject);
begin
     BitBtn1Click(self);
end;

end.

