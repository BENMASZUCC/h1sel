unit DubbiIns;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, StdCtrls, Buttons, Grids, DBGrids, DBTables, ADODB, U_ADOLinkCl,
     TB97, ExtCtrls;

type
     TDubbiInsForm = class(TForm)
          dsQDubbi: TDataSource;
          DBGrid1: TDBGrid;
          QDubbi: TADOLinkedQuery;
          Panel1: TPanel;
          BOk: TToolbarButton97;
    BEsci: TToolbarButton97;
          procedure FormShow(Sender: TObject);
          procedure BOkClick(Sender: TObject);
          procedure BEsciClick(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     DubbiInsForm: TDubbiInsForm;

implementation
//[/TONI20020726]
uses ModuloDati, Main;
//[/TONI20020726]FINE

{$R *.DFM}

procedure TDubbiInsForm.FormShow(Sender: TObject);
begin
     Caption := '[M/055] - ' + Caption;
     
     //Grafici
     DubbiInsForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
end;

procedure TDubbiInsForm.BOkClick(Sender: TObject);
begin
     ModalResult := mrOk;
end;

procedure TDubbiInsForm.BEsciClick(Sender: TObject);
begin
     ModalResult := mrCancel;
     close;
end;

end.

