//TONI OK 04/5
unit ModelloWord;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, ExtCtrls, Buttons, Db, ADODB, TB97;

type
     TModelloWordForm = class(TForm)
          EModello: TEdit;
          EDesc: TEdit;
          EIniziali: TEdit;
          Label1: TLabel;
          Label2: TLabel;
          Label4: TLabel;
          RGTabella: TRadioGroup;
          GroupBox1: TGroupBox;
          CBH1Sel: TCheckBox;
          CBHRMS: TCheckBox;
          CBTipoFile: TComboBox;
          Label5: TLabel;
          QtipiFile: TADOQuery;
          Panel1: TPanel;
          BitBtn1: TToolbarButton97;
          BitBtn2: TToolbarButton97;
          procedure FormShow(Sender: TObject);
          procedure CBTipoFileChange(Sender: TObject);
          procedure BitBtn1Click(Sender: TObject);
          procedure BitBtn2Click(Sender: TObject);
          procedure CBH1SelClick(Sender: TObject);
          procedure SceltaTabelle;
          procedure CBHRMSClick(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     ModelloWordForm: TModelloWordForm;

implementation

uses Main;

{$R *.DFM}

procedure TModelloWordForm.FormShow(Sender: TObject);
begin
     //Grafica
     ModelloWordForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     CBTipoFileChange(self);
     CBHRMS.Checked := false;
     SceltaTabelle;
end;

procedure TModelloWordForm.CBTipoFileChange(Sender: TObject);
begin
     ModelloWordForm.QtipiFile.Locate('TIPO', ModelloWordForm.CBTipoFile.Text, []);
end;

procedure TModelloWordForm.BitBtn1Click(Sender: TObject);
begin
     ModelloWordForm.ModalResult := mrOk;
end;

procedure TModelloWordForm.BitBtn2Click(Sender: TObject);
begin
     ModelloWordForm.ModalResult := mrCancel;
end;

procedure TModelloWordForm.SceltaTabelle;
begin
     RGTabella.Controls[0].Visible := false;
     RGTabella.Controls[1].Visible := false;
     RGTabella.Controls[2].Visible := false;
     if (CBHRMS.Checked) or (CBH1Sel.Checked) then begin
          RGTabella.Controls[0].Visible := true;
     end;
     if CBH1Sel.Checked then begin
          RGTabella.Controls[1].Visible := true;
          RGTabella.Controls[2].Visible := true;
     end;
end;

procedure TModelloWordForm.CBH1SelClick(Sender: TObject);
begin
     SceltaTabelle;
end;

procedure TModelloWordForm.CBHRMSClick(Sender: TObject);
begin
     SceltaTabelle;
end;

end.

