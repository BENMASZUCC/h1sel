unit uGestEventi;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, Grids, DBGrids, Mask, DBCtrls, ExtCtrls, Db, DBTables,
     ModuloDati, ModuloDati2, MDRicerche, Main, InsCompetenza, ContattoConCliente,
     InSelezione, ElencoRicPend, Inserimento, ContattoCand, FissaColloquio,
     Presentaz, uUtilsVarie, DateInserimento, InfoColloquio, CambiaStato2, Comuni, SelPers, Registry,
     MotiviSospensione, MotiviRichiamoCand, ADODB, dxDBGrid, dxGrFltr, dxFilter;

function InsEvento(xIDAnag, xIDProc, xIDevento, xIDaStato, xIDTipoStatoA: integer; xData: TDateTime; xCognome, xNome, xMot: string; xIDRic, xIDutente, xIDcliente: integer): boolean;

implementation

uses U_ADOLinkCl;

function InsEvento(xIDAnag, xIDProc, xIDevento, xIDaStato, xIDTipoStatoA: integer; xData: TDateTime; xCognome, xNome, xMot: string; xIDRic, xIDutente, xIDCliente: integer): boolean;
var xAnnulla, xAggiornaStato, xProcedi: boolean;
     xSec, xMSec: Word;
     xHour, xMin, xfilter: string;
     xYear, xMonth, xDay: Word;
     xIDCandRic, xIDColloquio: integer;
     Q: TADOlinkedQuery;
     xMatricola, xIDAzienda, xTipoRicerca, xseparatore, xImpostazOra: string;
     i, j: integer;
     vero: boolean;
     xReg: TRegistry;
     xsearch: TADOQuery;
begin
     xAnnulla := False;
     xAggiornaStato := True;
     Data.DB.BeginTrans;

     xSeparatore := '.';

     xReg := TRegistry.create;
     xReg.RootKey := HKEY_CURRENT_USER;
     xReg.OpenKey('\Control Panel\International', True);
     xImpostazOra := xReg.ReadString('sTimeFormat');
     xReg.CloseKey;
     xReg.Free;
     // showmessage('impostazioni= ' + xImpostazOra);
     if xImpostazOra = 'H.mm.ss' then xseparatore := '.';
     if xImpostazOra = 'HH.mm.ss' then xseparatore := '.';
     if xImpostazOra = 'HH:mm:ss' then xseparatore := ':';

     try
          case xIDProc of
               1: begin // immissione in selezione
                         if Pos('EMERGENCY', UpperCase(Data.Global.FieldByName('NomeAzienda').AsString)) = 0 then begin
                              if xIDRic > 0 then begin
                                   if MessageDlg('Vuoi associare il candidato a QUESTA ricerca ?', mtInformation, [mbYes, mbNO], 0) = mrYes then begin
                                        Q := CreateQueryFromString('update EBC_CandidatiRicerche set Escluso=0 ' +
                                             'where IDAnagrafica=' + IntToStr(xIDAnag) + ' and IDRicerca=' + IntToStr(xIDRic));
                                        Q.ExecSQL;
                                        xMot := 'associato alla ricerca ' + DataRicerche.TRicerchePend.FieldByName('Progressivo').asString;
                                   end;
                              end else begin
                                   Application.CreateForm(TInSelezioneForm, InSelezioneForm);
                                   //InSelezioneForm.TAnag.FindkeyADO(VarArrayOf([xIDAnag]));
                                   InSelezioneForm.TAnagNew.Close;
                                   InSelezioneForm.TAnagNew.Parameters.ParamByName('xid').Value := xIDAnag;
                                   InSelezioneForm.TAnagNew.Open;
                                   InSelezioneForm.ShowModal;
                                   if InSelezioneForm.ModalResult = mrOK then begin
                                        if InSelezioneForm.RGopzioni.ItemIndex = 1 then begin
                                             // selezione ricerca da abbinare
                                             Application.CreateForm(TElencoRicPendForm, ElencoRicPendForm);

                                             ElencoRicpendform.xall := True;
                                             ElencoRicpendform.CBElencoRicPend.Visible := True;
                                             ElencoRicPendForm.CBElencoRicPendClick(nil);

                                             //ElencoRicpendform.dxDBGrid1.Filter.Add(ElencoRicpendform.dxDBGrid1Stato, 'attiva', 'attiva');
                                             ElencoRicPendForm.ShowModal;
                                             if ElencoRicPendForm.ModalResult = mrOK then begin
                                                  // registrazione abbinamento nome-ricerca
                                                  DataRicerche.TRicAnagIDX.Open;
                                                  if not DataRicerche.TRicAnagIDX.FindKeyADO(VarArrayOf([ElencoRicPendForm.QRicAttive.FieldByName('ID').AsInteger, xIDAnag])) then begin
                                                       Q := CreateQueryFromString('insert into EBC_CandidatiRicerche (IDRicerca,IDAnagrafica,Escluso) ' +
                                                            'values (:xIDRicerca:,:xIDAnagrafica:,:xEscluso:)');
                                                       Q.ParamByName['xIDRicerca'] := ElencoRicPendForm.QRicAttive.FieldByName('ID').AsInteger;
                                                       Q.ParamByName['xIDAnagrafica'] := xIDAnag;
                                                       Q.ParamByName['xEscluso'] := 0;
                                                       Q.ExecSQL;
                                                  end;
                                                  xMot := 'associato alla ricerca ' + ElencoRicPendForm.QRicAttive.FieldByName('Progressivo').AsString;
                                                  DataRicerche.TRicAnagIDX.Close;
                                             end;
                                             ElencoRicPendForm.Free;
                                        end;
                                   end else xAnnulla := True;
                                   InSelezioneForm.Free;
                              end;
                         end else begin
                              if xIDRic > 0 then begin
                                   if MessageDlg('Vuoi associare il candidato a QUESTA missione ?', mtInformation, [mbYes, mbNO], 0) = mrYes then begin
                                        Q := CreateQueryFromString('update EBC_CandidatiRicerche set Escluso=0, ' +
                                             'where IDAnagrafica=' + IntToStr(xIDAnag) + ' and IDRicerca=' + IntToStr(xIDRic));
                                        Q.ExecSQL;
                                        xMot := 'associato alla missione ' + DataRicerche.TRicerchePend.FieldByName('Progressivo').asString;
                                   end;
                              end else begin
                                   Application.CreateForm(TInSelezioneForm, InSelezioneForm);
                                   //InSelezioneForm.TAnag.FindkeyADO(VarArrayOf([xIDAnag]));
                                   InSelezioneForm.TAnagNew.Close;
                                   InSelezioneForm.TAnagNew.Parameters.ParamByName('xid').Value := xIDAnag;
                                   InSelezioneForm.TAnagNew.Open;
                                   InSelezioneForm.ShowModal;
                                   if InSelezioneForm.ModalResult = mrOK then begin
                                        if InSelezioneForm.RGopzioni.ItemIndex = 1 then begin
                                             // selezione ricerca da abbinare
                                             Application.CreateForm(TElencoRicPendForm, ElencoRicPendForm);

                                             ElencoRicpendform.xall := True;
                                             ElencoRicpendform.CBElencoRicPend.Visible := True;
                                             ElencoRicPendForm.CBElencoRicPendClick(nil);

                                             //ElencoRicpendform.dxDBGrid1.Filter.Add(ElencoRicpendform.dxDBGrid1Stato, 'attiva', 'attiva');
                                             ElencoRicPendForm.ShowModal;
                                             if ElencoRicPendForm.ModalResult = mrOK then begin
                                                  // registrazione abbinamento nome-ricerca
                                                  DataRicerche.TRicAnagIDX.Open;
                                                  if not DataRicerche.TRicAnagIDX.FindKeyADO(VarArrayOf([ElencoRicPendForm.QRicAttive.FieldByName('ID').AsInteger, xIDAnag])) then begin
                                                       Q := CreateQueryFromString('insert into EBC_CandidatiRicerche (IDRicerca,IDAnagrafica,Escluso) ' +
                                                            'values (:xIDRicerca:,:xIDAnagrafica:,:xEscluso:)');
                                                       Q.ParamByName['xIDRicerca'] := ElencoRicPendForm.QRicAttive.FieldByName('ID').AsInteger;
                                                       Q.ParamByName['xIDAnagrafica'] := xIDAnag;
                                                       Q.ParamByName['xEscluso'] := 0;
                                                       Q.ExecSQL;
                                                  end;
                                                  xMot := 'associato alla missione ' + ElencoRicPendForm.QRicAttive.FieldByName('Progressivo').AsString;
                                                  DataRicerche.TRicAnagIDX.Close;
                                             end;
                                             ElencoRicPendForm.Free;
                                        end;
                                   end else xAnnulla := True;
                                   InSelezioneForm.Free;
                              end;
                         end;
                    end;
               2: begin // inserimento in azienda
                         if xIDcliente = 0 then begin
                              // non dalla ricerca
                              Application.CreateForm(TInserimentoForm, InserimentoForm);
                              InserimentoForm.TEBCclienti.Locate('ID', xIDcliente, []);
                              InserimentoForm.ShowModal;
                              if InserimentoForm.ModalResult = mrCancel then xAnnulla := True
                              else xIDCliente := InserimentoForm.TEBCclienti.FieldByName('ID').AsInteger;
                              InserimentoForm.Free;
                         end;
                         if not xAnnulla then begin
                              // richiesta date (data presunta inizio reale e data controllo
                              Application.CreateForm(TDateInserimentoForm, DateInserimentoForm);
                              //DateInserimentoForm.DEDataInizio.Date:=Date;
                              //DateInserimentoForm.DEDataControllo.Date:=Date;
                              DateInserimentoForm.ShowModal;
                              if DateInserimentoForm.ModalResult = mrCancel then xAnnulla := True
                              else begin
                                   // eventuale messaggio in promemoria
                                   if DateInserimentoForm.CBProm.Checked then begin
                                        // nome cliente
                                        Data2.Qtemp.Close;
                                        Data2.Qtemp.SQL.text := 'select Descrizione from EBC_Clienti where ID=' + IntToStr(xIDCliente);
                                        Data2.Qtemp.Open;
                                        Q := CreateQueryFromString('insert into Promemoria (IDUtente,IDUtenteDa,DataIns,DataDaLeggere,Testo,Evaso,IDCandUtente) ' +
                                             'values (:xIDUtente:,:xIDUtenteDa:,:xDataIns:,:xDataDaLeggere:,:xTesto:,:xEvaso:,:xIDCandUtente:)');
                                        Q.ParamByName['xIDUtente'] := xIDutente;
                                        Q.ParamByName['xIDUtenteDa'] := xIDutente;
                                        Q.ParamByName['xDataIns'] := Date;
                                        if DateInserimentoForm.DEDataControllo.Text = '' then
                                             Q.ParamByName['xDataDaLeggere'] := NULL
                                        else Q.ParamByName['xDataDaLeggere'] := DateInserimentoForm.DEDataControllo.Date;
                                        Q.ParamByName['xTesto'] := copy('verificare inserimento in azienda ' + Data2.Qtemp.FieldByName('Descrizione').asString +
                                             ' di ' + xCognome + ' ' + xNome, 1, 80);
                                        Q.ParamByName['xEvaso'] := 0;
                                        Q.ParamByName['xIDCandUtente'] := xIDAnag;
                                        Q.ExecSQL;
                                        Data2.Qtemp.Close;
                                   end;
                                   Data2.TClientiABS.Open;
                                   Data2.TClientiABS.Locate('ID', xIDCliente, []);
                                   xMot := xMot + ' ' + Data2.TClientiABS.FieldByName('Descrizione').AsString;
                                   Q := CreateQueryFromString('insert into EBC_Inserimenti (IDAnagrafica,IDCliente,DallaData,IDRicerca,DataControllo) ' +
                                        'values (:xIDAnagrafica:,:xIDCliente:,:xDallaData:,:xIDRicerca:,:xDataControllo:)');
                                   Q.ParamByName['xIDAnagrafica'] := xIDAnag;
                                   Q.ParamByName['xIDCliente'] := xIDCliente;
                                   if DateInserimentoForm.DEDataInizio.text = '' then
                                        Q.ParamByName['xDallaData'] := NULL
                                   else Q.ParamByName['xDallaData'] := DateInserimentoForm.DEDataInizio.Date;
                                   Q.ParamByName['xIDRicerca'] := xIDRic;
                                   if DateInserimentoForm.DEDataControllo.text = '' then
                                        Q.ParamByName['xDataControllo'] := NULL
                                   else Q.ParamByName['xDataControllo'] := DateInserimentoForm.DEDataControllo.Date;
                                   Q.ExecSQL;
                                   DateInserimentoForm.Free;
                                   // rimozione dalle ricerche dove risulta attivo (su richiesta)
                                   if MessageDlg('Escludere automaticamente il candidato dalle ricerche cui � associato ?', mtWarning, [mbYes, mbNo], 0) = mrYes then begin
                                        DataRicerche.QGen.Close;
                                        DataRicerche.QGen.SQL.Clear;
                                        DataRicerche.QGen.SQL.Add('update EBC_CandidatiRicerche set Escluso=1,');
                                        DataRicerche.QGen.SQL.Add('Stato=substring(''inserimento in azienda ' + TrimRight(StringReplace(Data2.TClientiABS.FieldByName('Descrizione').AsString, '''', '''''', [rfReplaceAll])) + ''',1,80) where IDAnagrafica=' + IntToStr(xIDAnag));
                                        DataRicerche.QGen.execSQL;
                                   end;
                                   if xIDRic > 0 then begin
                                        if MessageDlg('Inserire in esperienze lavorative (come attuale) ?', mtWarning, [mbYes, mbNo], 0) = mrYes then begin
                                             // inserimento in esperienze lavorative (prima tutte le esp.lav. NON ATTUALI del sogg.)
                                             Data.Q1.Close;
                                             Data.Q1.SQL.text := 'select IDArea,IDMansione from EBC_Ricerche where ID=' + IntToStr(xIDRic);
                                             Data.Q1.Open;
                                             Q := CreateQueryFromString('update EsperienzeLavorative set Attuale=0 ' +
                                                  'where IDAnagrafica=:xIDAnagrafica:');
                                             Q.ParamByName['xIDAnagrafica'] := xIDAnag;
                                             Q.ExecSQL;
                                             Q := CreateQueryFromString('insert into EsperienzeLavorative (IDAnagrafica,IDAzienda,Attuale,AnnoDal,IDArea,IDMansione,IDSettore) ' +
                                                  'values (:xIDAnagrafica:,:xIDAzienda:,:xAttuale:,:xAnnoDal:,:xIDArea:,:xIDMansione:,:xIDSettore:)');
                                             Q.ParamByName['xIDAnagrafica'] := xIDAnag;
                                             Q.ParamByName['xIDAzienda'] := xIDCliente;
                                             Q.ParamByName['xAttuale'] := 1;
                                             DecodeDate(Date, xYear, xMonth, xDay);
                                             Q.ParamByName['xAnnoDal'] := xYear;
                                             if Data.Q1.IsEmpty then begin
                                                  Q.ParamByName['xIDArea'] := 0;
                                                  Q.ParamByName['xIDMansione'] := 0;
                                             end else begin
                                                  Q.ParamByName['xIDArea'] := Data.Q1.FieldByName('IDArea').asInteger;
                                                  Q.ParamByName['xIDMansione'] := Data.Q1.FieldByName('IDMansione').asInteger;
                                             end;
                                             // settore dell'azienda
                                             Data.Q1.Close;
                                             Data.Q1.SQL.text := 'select IDAttivita from EBC_Clienti where ID=' + IntToStr(xIDCliente);
                                             Data.Q1.Open;
                                             Q.ParamByName['xIDSettore'] := Data.Q1.FieldByName('IDAttivita').asInteger;
                                             Data.Q1.Close;
                                             Q.ExecSQL;
                                             Data.Q1.Close;
                                        end;
                                        // aggiornamento dati candidato-ricerca
                                        Q := CreateQueryFromString('update EBC_CandidatiRicerche set Escluso=0,DataImpegno=null,Stato=:xStato: ' +
                                             'where IDAnagrafica=' + IntToStr(xIDAnag) + ' and IDRicerca=' + IntToStr(xIDRic));
                                        if pos('EMERGENCY', uppercase(Data.Global.fieldbyname('nomeazienda').AsString)) > 0 then
                                             Q.ParamByName['xStato'] := 'FIT'
                                        else
                                             Q.ParamByName['xStato'] := 'inserito in azienda';
                                        Q.ExecSQL;
                                        // richiesta di conclusione ricerca
                                        MessageDlg('ATTENZIONE: verificare se rendere conclusa la ricerca.', mtWarning, [mbOK], 0);
                                        Data2.TClientiABS.Close;
                                   end;
                              end;
                         end;
                    end;
               3: begin // archiviazione
                         // rimozione dalle ricerche dove risulta attivo (su richiesta)
                         if MessageDlg('Eliminare il candidato dalle ricerche cui � associato ?', mtWarning, [mbYes, mbNo], 0) = mrYes then begin
                              DataRicerche.QGen.Close;
                              DataRicerche.QGen.SQL.Clear;
                              DataRicerche.QGen.SQL.Add('update EBC_CandidatiRicerche set Escluso=1,');
                              DataRicerche.QGen.SQL.Add('Stato=''ELIMINATO ' + xMot + ''' where IDAnagrafica=' + IntToStr(xIDAnag));
                              DataRicerche.QGen.execSQL;
                         end;
                    end;
               4: begin // registrazione contatto
                         // Application.CreateForm(TContattoCandForm, ContattoCandForm);
                         ContattoCandForm := TContattoCandForm.create(nil);
                         if pos('EMERGENCY', UpperCase(data.Global.fieldbyname('NomeAzienda').asString)) > 0 then begin
                              ContattoCandForm.BitBtn1.top := 248;
                              ContattoCandForm.BitBtn1.left := 60;
                              ContattoCandForm.BitBtn2.top := 248;
                              ContattoCandForm.BitBtn2.left := 250;
                              ContattoCandForm.Height := 350; //250;
                              //  ContattoCandForm.Position:=poScreenCenter;
                              ContattoCandForm.RadioGroup2.Visible := false;
                              // ContattoCandForm.eNote.Visible := false;
                              ContattoCandForm.Label6.top := 152;
                              ContattoCandForm.eNote.top := 168;
                              ContattoCandForm.eNote.Width := 353;


                              ContattoCandForm.RadioGroup2.ItemIndex := -1;
                              ContattoCandForm.PanAltroEsito.Visible := false;
                              ContattoCandForm.PanMotivoNonAcc.Visible := false;
                              ContattoCandForm.DBGrid1.Visible := false;

                         end;
                         ContattoCandForm.DEData.Date := xData;
                         // DecodeTime(Now, xHour, xMin, xSec, xMSec);
                          // showmessage(IntToStr(xHour) + xseparatore + IntToStr(xMin));
                         data.QOrarioAttuale.close;
                         data.QOrarioAttuale.Open;
                         xHour := data.QOrarioAttualeOre.asString;
                         xMin := data.QOrarioAttualeMinuti.asString;

                         ContattoCandForm.MEOra.Text := xHour + xseparatore + xMin;
                         ContattoCandForm.xIDAnag := xIDAnag;
                         ContattoCandForm.ShowModal;
                         { SetWindowPos(
                               ContattoCandForm.Handle,
                               HWND_TOPMOST,
                               411,
                               281,
                               0,
                               0,
                               SWP_SHOWWINDOW);
                          }
                         if ContattoCandForm.Modalresult = mrCancel then xAnnulla := True
                         else begin
                              xIDEvento := ContattoCandForm.QTipiContattiIDEvento.value;
                              xMot := '';

                              if pos('EMERGENCY', Uppercase(Data.Global.fieldbyname('NomeAzienda').AsString)) > 0 then
                                   xMot := ContattoCandForm.eNote.text; //serve x inserire le note nello storico

                              if ContattoCandForm.RadioGroup2.Visible then
                                   xMot := ContattoCandForm.RadioGroup2.Items[ContattoCandForm.RadioGroup2.ItemIndex];

                              if ContattoCandForm.RadioGroup2.ItemIndex = 0 then begin
                                   {if pos('EMERGENCY', UpperCase(data.Global.fieldbyname('NomeAzienda').asString)) > 0 then begin
                                        MotiviRichiamoCandForm := TMotiviRichiamoCandForm.create(nil);
                                        MotiviRichiamoCandForm.ShowModal;
                                        if MotiviRichiamoCandForm.ModalResult = mrOK then begin
                                             Q := CreateQueryFromString('update EBC_CandidatiRicerche set DataPrevRichiamo=:xdataPrevRichiamo: , ' +
                                                  ' IDMotivorichiamoCandidato=' + MotiviRichiamoCandForm.QMotiviRichiamoID.AsString +
                                                  ' where IDAnagrafica=' + IntToStr(xIDAnag) + ' and IDRicerca=' + IntToStr(xIDRic));
                                             q.ParamByName['xdataPrevRichiamo'] := MotiviRichiamoCandForm.DateEdit1.Text;
                                             q.ExecSQL;
                                        end else begin
                                             xAnnulla := true;
                                        end;

                                        MotiviRichiamoCandForm.free;
                                   end;    }

                                   if xAnnulla = false then begin
                                        // da richiamare
                                        // aggiornamento dati contatti-candidato
                                        Q := CreateQueryFromString('Insert into EBC_ContattiCandidati (IDRicerca,IDAnagrafica,TipoContatto,Data,Ora,Esito,Evaso,Note,IDUtente) ' +
                                             'values (:xIDRicerca:,:xIDAnagrafica:,:xTipoContatto:,:xData:,:xOra:,:xEsito:,:xEvaso:,:xNote:,:xIDUtente:)');
                                        Q.ParamByName['xIDRicerca'] := xIDRic;
                                        Q.ParamByName['xIDAnagrafica'] := xIDAnag;
                                        Q.ParamByName['xTipoContatto'] := ContattoCandForm.QTipiContattiTipoContatto.value;
                                        Q.ParamByName['xData'] := ContattoCandForm.DEData.date;
                                        Q.ParamByName['xOra'] := StrToDateTime(DatetoStr(ContattoCandForm.DEData.date) + ' ' + ContattoCandForm.MEOra.Text);
                                        Q.ParamByName['xEsito'] := 'da richiamare';
                                        Q.ParamByName['xEvaso'] := 0;
                                        Q.ParamByName['xNote'] := ContattoCandForm.ENote.Text;
                                        Q.ParamByName['xIDUtente'] := MainForm.xIDUtenteAttuale;
                                        Q.ExecSQL;
                                        // aggiornamento dati candidato-ricerca
                                        Q := CreateQueryFromString('update EBC_CandidatiRicerche set Escluso=0,DataImpegno=null,Stato=:xStato: ' +
                                             'where IDAnagrafica=' + IntToStr(xIDAnag) + ' and IDRicerca=' + IntToStr(xIDRic));
                                        Q.ParamByName['xStato'] := 'da richiamare ' + ContattoCandForm.ENote.Text;
                                        Q.ExecSQL;
                                   end
                              end;
                              if ContattoCandForm.RadioGroup2.ItemIndex = 1 then begin

                                   // deve richiamare
                                   //xMot := 'deve richiamare ' + ContattoCandForm.ENote.Text;
                                        // aggiornamento dati contatti-candidato
                                   Q := CreateQueryFromString('Insert into EBC_ContattiCandidati (IDRicerca,IDAnagrafica,TipoContatto,Data,Ora,Esito,Evaso,Note,IDUtente) ' +
                                        'values (:xIDRicerca:,:xIDAnagrafica:,:xTipoContatto:,:xData:,:xOra:,:xEsito:,:xEvaso:,:xNote:,:xIDUtente:)');
                                   Q.ParamByName['xIDRicerca'] := xIDRic;
                                   Q.ParamByName['xIDAnagrafica'] := xIDAnag;
                                   Q.ParamByName['xTipoContatto'] := ContattoCandForm.QTipiContattiTipoContatto.value;
                                   Q.ParamByName['xData'] := ContattoCandForm.DEData.date;
                                   Q.ParamByName['xOra'] := StrToDateTime(DatetoStr(ContattoCandForm.DEData.date) + ' ' + ContattoCandForm.MEOra.Text);
                                   Q.ParamByName['xEsito'] := 'deve richiamare';
                                   Q.ParamByName['xEvaso'] := 0;
                                   Q.ParamByName['xNote'] := ContattoCandForm.ENote.Text;
                                   Q.ParamByName['xIDUtente'] := MainForm.xIDUtenteAttuale;
                                   Q.ExecSQL;
                                   // aggiornamento dati candidato-ricerca
                                   Q := CreateQueryFromString('update EBC_CandidatiRicerche set Escluso=0,DataImpegno=null,Stato=:xStato: , ' +
                                        ' DataPrevRichiamo=NULL,  IDMotivorichiamoCandidato=NULL ' +
                                        'where IDAnagrafica=' + IntToStr(xIDAnag) + ' and IDRicerca=' + IntToStr(xIDRic));
                                   Q.ParamByName['xStato'] := 'deve richiamare ' + ContattoCandForm.ENote.Text;
                                   Q.ExecSQL;
                              end;
                              if ContattoCandForm.RadioGroup2.ItemIndex = 2 then begin
                                   // fissare colloquio
                                   Application.CreateForm(TFissaColloquioForm, FissaColloquioForm);
                                   FissaColloquioForm.TSelezionatori.Locate('ID', MainForm.xIDUtenteAttuale, []);
                                   FissaColloquioForm.DEData.Date := Date;
                                   // DecodeTime(Now, xHour, xMin, xSec, xMSec);
                                   data.QOrarioAttuale.close;
                                   data.QOrarioAttuale.Open;
                                   xHour := data.QOrarioAttualeOre.asString;
                                   xMin := data.QOrarioAttualeMinuti.asString;

                                   //  showmessage('ora=' + inttostr(xHour) + '  min=' + inttostr(xMin)+'  sec='+inttostr(xSec));

                                   FissaColloquioForm.Ora.Text := xHour + xseparatore + xMin;
                                   {  if strtoint(xMin) + 30 >= 60 then begin
                                          xHour := inttostr(strtoint(xhour) + 1);
                                               xMin := xMin - 60;
                                     end; }
                                   FissaColloquioForm.MEAlleOre.Text := data.QOrarioAttualeOreMagg.AsString + xseparatore + data.QOrarioAttualeMinutiMagg.AsString;

                                   FissaColloquioForm.Showmodal;
                                   if FissaColloquioForm.ModalResult = mrCancel then xAnnulla := True else begin
                                        xMot := xMot + ' il ' + DateToStr(FissaColloquioForm.DEData.Date) + ' alle ' + FissaColloquioForm.Ora.Text;
                                        xIDCandRic := 0;
                                        if xIDRic > 0 then begin
                                             // aggiornamento dati candidato-ricerca
                                             if not xAnnulla then begin
                                                  Q := CreateQueryFromString('update EBC_CandidatiRicerche set DataPrevRichiamo=NULL,  IDMotivorichiamoCandidato=NULL, Escluso=0,DataImpegno=:xDataImpegno:,Stato=:xStato: ' +
                                                       'where IDAnagrafica=' + IntToStr(xIDAnag) + ' and IDRicerca=' + IntToStr(xIDRic));
                                                  Q.ParamByName['xDataImpegno'] := FissaColloquioForm.DEData.Date;
                                                  if pos('INTERMEDIA', UpperCase(DAta.Global.FieldByName('NomeAzienda').AsString)) > 0 then begin
                                                       if FissaColloquioForm.CBInterno.Checked then
                                                            if FissaColloquioForm.CBLuogo.Checked then

                                                                 Q.ParamByName['xStato'] := 'colloquio in data ' + FissaColloquioForm.DEData.Text //+' del '+ FissaColloquioForm.DEData.Text;
                                                            else Q.ParamByName['xStato'] := 'colloquio in data ' + FissaColloquioForm.DEData.Text + ' (' + FissaColloquioForm.TRisorse.FieldByName('Risorsa').AsString + ') ' + ContattoCandForm.ENote.Text
                                                       else Q.ParamByName['xStato'] := 'colloquio ESTERNO in data ' + FissaColloquioForm.DEData.Text + ' ' + ContattoCandForm.ENote.Text;
                                                  end else begin
                                                       if FissaColloquioForm.CBInterno.Checked then
                                                            if FissaColloquioForm.CBLuogo.Checked then
                                                                 Q.ParamByName['xStato'] := ' ore ' + FissaColloquioForm.Ora.Text + ' colloquio'
                                                            else Q.ParamByName['xStato'] := ' ore ' + FissaColloquioForm.Ora.Text + ' colloquio (' + FissaColloquioForm.TRisorse.FieldByName('Risorsa').AsString + ') ' + ContattoCandForm.ENote.Text
                                                       else Q.ParamByName['xStato'] := 'ore ' + FissaColloquioForm.Ora.Text + ' colloquio ESTERNO (' + ContattoCandForm.ENote.Text + ')';
                                                  end;
                                                  Q.ExecSQL;
                                             end;
                                             // aggiornamento dati contatti-candidato
                                             Q := CreateQueryFromString('Insert into EBC_ContattiCandidati (IDRicerca,IDAnagrafica,TipoContatto,Data,Ora,Esito,Evaso,Note,IDUtente) ' +
                                                  'values (:xIDRicerca:,:xIDAnagrafica:,:xTipoContatto:,:xData:,:xOra:,:xEsito:,:xEvaso:,:xNote:,:xIDUtente:)');
                                             Q.ParamByName['xIDRicerca'] := xIDRic;
                                             Q.ParamByName['xIDAnagrafica'] := xIDAnag;
                                             Q.ParamByName['xTipoContatto'] := ContattoCandForm.QTipiContattiTipoContatto.value;
                                             Q.ParamByName['xData'] := ContattoCandForm.DEData.date;
                                             Q.ParamByName['xOra'] := StrToDateTime(DatetoStr(ContattoCandForm.DEData.date) + ' ' + ContattoCandForm.MEOra.Text);
                                             Q.ParamByName['xEsito'] := 'fissato colloquio';
                                             Q.ParamByName['xEvaso'] := 0;
                                             Q.ParamByName['xNote'] := ContattoCandForm.ENote.Text;
                                             Q.ParamByName['xIDUtente'] := MainForm.xIDUtenteAttuale;
                                             Q.ExecSQL;
                                        end;
                                        if FissaColloquioForm.CBInterno.Checked then begin
                                             // agenda
                                             Q := CreateQueryFromString('select ID from EBC_CandidatiRicerche where IDAnagrafica=' + IntToStr(xIDAnag) + ' and IDRicerca=' + IntToStr(xIDRic));
                                             Q.Open;
                                             xIDCandRic := Q.FieldByName('ID').asInteger;
                                             Q.Close;
                                             Q := CreateQueryFromString('Insert into Agenda (Data,Ore,AlleOre,IDUtente,IDRisorsa,Tipo,IDCandRic,Descrizione) ' +
                                                  'values (:xData:,:xOre:,:xAlleOre:,:xIDUtente:,:xIDRisorsa:,:xTipo:,:xIDCandRic:,:xDescrizione:)');
                                             Q.ParamByName['xData'] := FissaColloquioForm.DEData.Date;
                                             Q.ParamByName['xOre'] := StrToDateTime(DatetoStr(FissaColloquioForm.DEData.Date) + ' ' + FissaColloquioForm.Ora.Text);
                                             Q.ParamByName['xAlleOre'] := StrToDateTime(DatetoStr(FissaColloquioForm.DEData.Date) + ' ' + FissaColloquioForm.MEAlleOre.Text);
                                             Q.ParamByName['xIDUtente'] := FissaColloquioForm.TSelezionatori.FieldByName('ID').AsInteger;
                                             if FissaColloquioForm.CBLuogo.Checked then
                                                  Q.ParamByName['xIDRisorsa'] := 0
                                             else Q.ParamByName['xIDRisorsa'] := FissaColloquioForm.TRisorse.FieldByName('ID').AsInteger;
                                             Q.ParamByName['xTipo'] := 1;
                                             Q.ParamByName['xIDCandRic'] := xIDCandRic;
                                             Q.ParamByName['xDescrizione'] := xCognome + ' ' + copy(xNome, 1, 1);
                                             Q.ExecSQL;
                                        end else xMot := xMot + ' ESTERNO';
                                   end;
                                   FissaColloquioForm.Free;
                              end;
                              if ContattoCandForm.RadioGroup2.ItemIndex = 3 then begin
                                   // NON ACCETTA
                                   xMot := ContattoCandForm.EDesc.Text + ' ' + ContattoCandForm.EMotNonAcc.Text;
                                   // aggiornamento dati contatti-candidato
                                   if xIDRic > 0 then begin
                                        Q := CreateQueryFromString('Insert into EBC_ContattiCandidati (IDRicerca,IDAnagrafica,TipoContatto,Data,Ora,Esito,Evaso,Note,IDMotivoNonAcc,IDUtente) ' +
                                             'values (:xIDRicerca:,:xIDAnagrafica:,:xTipoContatto:,:xData:,:xOra:,:xEsito:,:xEvaso:,:xNote:,:xIDMotivoNonAcc:,:xIDUtente:)');
                                        Q.ParamByName['xIDRicerca'] := xIDRic;
                                        Q.ParamByName['xIDAnagrafica'] := xIDAnag;
                                        Q.ParamByName['xTipoContatto'] := ContattoCandForm.QTipiContattiTipoContatto.value;
                                        Q.ParamByName['xData'] := ContattoCandForm.DEData.date;
                                        Q.ParamByName['xOra'] := StrToDateTime(DatetoStr(ContattoCandForm.DEData.date) + ' ' + ContattoCandForm.MEOra.Text);
                                        Q.ParamByName['xEsito'] := 'NON ACCETTA';
                                        Q.ParamByName['xEvaso'] := 0;
                                        Q.ParamByName['xNote'] := ContattoCandForm.ENote.Text;
                                        Q.ParamByName['xIDMotivoNonAcc'] := ContattoCandForm.xIDMotivoNonAcc;
                                        Q.ParamByName['xIDUtente'] := MainForm.xIDUtenteAttuale;
                                        Q.ExecSQL;
                                        // aggiornamento dati candidato-ricerca --> ESCLUSIONE
                                        Q := CreateQueryFromString('update EBC_CandidatiRicerche set DataPrevRichiamo=NULL,  IDMotivorichiamoCandidato=NULL, DataImpegno=null, Stato=:xStato:, Escluso=1 ' +
                                             'where IDAnagrafica=' + IntToStr(xIDAnag) + ' and IDRicerca=' + IntToStr(xIDRic));
                                        Q.ParamByName['xStato'] := 'NON ACCETTA ' + ContattoCandForm.EMotNonAcc.Text + ' ' + ContattoCandForm.ENote.Text;
                                        Q.ExecSQL;
                                        // aggiornamento storico
                                        Q.SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
                                             'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xIDCliente:)';
                                        Q.ParamByName['xIDAnagrafica'] := xIDAnag;
                                        Q.ParamByName['xIDEvento'] := 17;
                                        Q.ParamByName['xDataEvento'] := date;
                                        Q.ParamByName['xAnnotazioni'] := 'NON ACCETTA ' + ContattoCandForm.EMotNonAcc.Text + ' ' + ContattoCandForm.ENote.Text;
                                        Q.ParamByName['xIDRicerca'] := xIDRic;
                                        Q.ParamByName['xIDUtente'] := xIDutente;
                                        Q.ParamByName['xIDCliente'] := xIDCliente;
                                        Q.ExecSQL;
                                        // archiviazione CV
                                        Q.SQL.text := 'update Anagrafica set IDStato=28,IDTipoStato=2 where ID=' + IntToStr(xIDAnag);
                                        Q.ExecSQL;
                                        xAggiornaStato := False;
                                   end;
                              end;

                              if ContattoCandForm.RadioGroup2.ItemIndex = 4 then begin
                                   // altro esito -> semplice registrazione
                                   // aggiornamento dati contatti-candidato
                                   xMot := ContattoCandForm.EDesc.Text + ' ' + ContattoCandForm.ENote.Text;
                                   Q := CreateQueryFromString('Insert into EBC_ContattiCandidati (IDRicerca,IDAnagrafica,TipoContatto,Data,Ora,Esito,Evaso,Note,IDUtente) ' +
                                        'values (:xIDRicerca:,:xIDAnagrafica:,:xTipoContatto:,:xData:,:xOra:,:xEsito:,:xEvaso:,:xNote:,:xIDUtente:)');
                                   Q.ParamByName['xIDRicerca'] := xIDRic;
                                   Q.ParamByName['xIDAnagrafica'] := xIDAnag;
                                   Q.ParamByName['xTipoContatto'] := ContattoCandForm.QTipiContattiTipoContatto.value;
                                   Q.ParamByName['xData'] := ContattoCandForm.DEData.date;
                                   Q.ParamByName['xOra'] := StrToDateTime(DatetoStr(ContattoCandForm.DEData.date) + ' ' + ContattoCandForm.MEOra.Text);
                                   Q.ParamByName['xEsito'] := ContattoCandForm.EDesc.Text;
                                   Q.ParamByName['xEvaso'] := 0;
                                   Q.ParamByName['xNote'] := ContattoCandForm.ENote.Text;
                                   Q.ParamByName['xIDUtente'] := MainForm.xIDUtenteAttuale;
                                   Q.ExecSQL;
                                   // aggiornamento dati candidato-ricerca
                                   Q := CreateQueryFromString('update EBC_CandidatiRicerche set DataImpegno=null,Stato=:xStato: ' +
                                        ',DataPrevRichiamo=NULL,  IDMotivorichiamoCandidato=NULL ' +
                                        'where IDAnagrafica=' + IntToStr(xIDAnag) + ' and IDRicerca=' + IntToStr(xIDRic));
                                   Q.ParamByName['xStato'] := ContattoCandForm.EDesc.Text + ' ' + ContattoCandForm.ENote.Text;
                                   Q.ExecSQL;
                              end;
                         end;
                         ContattoCandForm.Free;
                    end;
               5: begin // avvenuto colloquio
                         Application.CreateForm(TInfoColloquioForm, InfoColloquioForm);
                         //InfoColloquioForm:=TInfoColloquioForm.Create(CAmbiastato2Form);
                         InfoColloquioForm.TSelezionatori.Open;
                         InfoColloquioForm.TSelezionatori.Locate('ID', mainForm.xIDUtenteAttuale, []);
                         //InfoColloquioForm.TSelezionatori.FindKeyADO(VarArrayOf([xIDutente]));
                         InfoColloquioForm.DataColloquio.Date := xData;
                         InfoColloquioForm.xIDAnagrafica := xIDAnag;
                         InfoColloquioForm.xIDRicerca := xIDRic;
                         if xIDRic = 0 then begin
                              //InfoColloquioForm.CBEsito.Color := clBtnFace;
                              //InfoColloquioForm.CBEsito.Enabled := False;
                              InfoColloquioForm.CBEsito.Clear;
                              InfoColloquioForm.CBEsito.Items.Add('colloquio conosciutivo');
                              InfoColloquioForm.CBEsito.Items.Add('altro colloquio');
                              InfoColloquioForm.CBEsito.ItemIndex := 0;
                         end else begin
                              // vai a vedere il flag di accettazione
                              Data.QTemp.Close;
                              Data.QTemp.SQL.text := 'select FlagAccPres from EBC_CandidatiRicerche ' +
                                   'where IDRicerca=' + IntToStr(xIDRic) + ' and IDAnagrafica=' + IntToStr(xIDAnag);
                              Data.QTemp.Open;
                              if Data.QTemp.FieldByName('FlagAccPres').AsString <> '' then
                                   InfoColloquioForm.CBFlagAccPres.Checked := Data.QTemp.FieldByName('FlagAccPres').AsBoolean;
                              Data.QTemp.Close;
                         end;
                         // esperienza lavorativa
                         with InfoColloquioForm do begin
                              // Apre i valori retributivi con l'ID dell'esp.lav. attuale
                              QEspLavAttuale.Close;
                              QEspLavAttuale.ParamByName['xIDAnag'] := xIDAnagrafica;
                              QEspLavAttuale.Open;
                              // se non c'� esp.lav.attuale ?
                              if QEspLavAttuale.IsEmpty then begin
                                   // xAnnulla := false;
                                    //QEspLavAttuale.Close;
                                    // richiedi l'inserimento di una nuova esp.lav.
                                   { if not NuovaEspLavAttuale then begin
                                         xAnnulla := True;
                                      //   tolto perch� non serve, chiesto a patrizia e andrea
                                      //   ha fatto del casino da intermedia
                               end;  }
                                    //QEspLavAttuale.Close;
                                    //QEspLavAttuale.Open;
                                   MessageDlg('Attenzione, Manca l''esperienza lavorativa attuale del candidato.', mtWarning, [mbOK], 0);
                                   //xAnnulla := true;
                              end;
                         end;
                         if not xAnnulla then begin
                              InfoColloquioForm.ShowModal;
                              if InfoColloquioForm.ModalResult = mrOK then begin
                                   xData := InfoColloquioForm.DataColloquio.Date;
                                   xIDUtente := InfoColloquioForm.TSelezionatori.FieldByName('ID').AsInteger;
                                   if InfoColloquioForm.CBColloquioCliente.checked then
                                        //xMot := 'presso cliente - ' + InfoColloquioForm.CBDettCollCliente.text + ' - Giudizio: ' + InfoColloquioForm.CBGiudizio.text
                                        xMot := 'presso cliente (Giudizio: ' + InfoColloquioForm.CBDettCollCliente.text + 'Puntegg.: ' + InfoColloquioForm.SEPunteggio.text + ')'
                                   else xMot := 'Giudizio: ' + InfoColloquioForm.CBGiudizio.text + 'Puntegg.: ' + InfoColloquioForm.SEPunteggio.text;
                                   Q := CreateQueryFromString('Insert into EBC_Colloqui (IDAnagrafica,IDRicerca,IDSelezionatore,data,ValutazioneGenerale,Punteggio,Esito,PressoCliente,ValutazioneCliente) ' +
                                        'values (:xIDAnagrafica:,:xIDRicerca:,:xIDSelezionatore:,:xdata:,:xValutazioneGenerale:,:xPunteggio:,:xEsito:,:xPressoCliente:,:xValutazioneCliente:)');
                                   Q.ParamByName['xIDAnagrafica'] := xIDAnag;
                                   Q.ParamByName['xIDRicerca'] := xIDRic;
                                   Q.ParamByName['xIDSelezionatore'] := InfoColloquioForm.TSelezionatori.FieldByName('ID').AsInteger;
                                   Q.ParamByName['xdata'] := InfoColloquioForm.DataColloquio.Date;
                                   Q.ParamByName['xValutazioneGenerale'] := copy(InfoColloquioForm.CBGiudizio.text, 1, 15);
                                   Q.ParamByName['xPunteggio'] := InfoColloquioForm.SEPunteggio.Value;
                                   Q.ParamByName['xEsito'] := copy(InfoColloquioForm.CBEsito.text, 1, 30);
                                   Q.ParamByName['xValutazioneCliente'] := InfoColloquioForm.CBDettCollCliente.text;
                                   if InfoColloquioForm.CBColloquioCliente.checked then
                                        Q.ParamByName['xPressoCliente'] := 1
                                   else Q.ParamByName['xPressoCliente'] := 0;
                                   Q.ExecSQL;
                                   // prendi l'ID del colloquio
                                   Q.SQL.text := 'select @@IDENTITY as LastID';
                                   Q.Open;
                                   xIDColloquio := Q.FieldByName('LastID').asInteger;
                                   Q.Close;
                                   if xIDRic > 0 then begin
                                        // salvataggio della CLColloquio
                                        InfoColloquioForm.SalvaCLColloquio(xIDColloquio);
                                   end;

                                   // gestione dei possibili esiti
                                   if (xIDRic > 0) and (InfoColloquioForm.CBEsito.Text = 'mantenere nella ricerca') then begin
                                        // aggiornamento dati candidato-ricerca
                                        Q := CreateQueryFromString('update EBC_CandidatiRicerche set DataImpegno=null,Escluso=0,Stato=:xStato:,FlagAccPres=:xFlagAccPres: ' +
                                             'where IDAnagrafica=' + IntToStr(xIDAnag) + ' and IDRicerca=' + IntToStr(xIDRic));

                                        if InfoColloquioForm.CBColloquioCliente.checked then begin
                                             Q.ParamByName['xStato'] := 'colloquio presso cliente ' + InfoColloquioForm.CBDettCollCliente.text + ' il ' + DateToStr(InfoColloquioForm.DataColloquio.Date) + ' (' + InfoColloquioForm.CBGiudizio.text + '-' + IntToStr(InfoColloquioForm.SEPunteggio.Value) + ')';
                                             xMot := 'presso cliente ' + InfoColloquioForm.CBDettCollCliente.text + ' il ' + DateToStr(InfoColloquioForm.DataColloquio.Date) + ' (' + InfoColloquioForm.CBGiudizio.text + '-' + IntToStr(InfoColloquioForm.SEPunteggio.Value) + ')';
                                        end else begin
                                             Q.ParamByName['xStato'] := 'colloquio sostenuto il ' + DateToStr(InfoColloquioForm.DataColloquio.Date) + ' (' + InfoColloquioForm.CBGiudizio.text + '-' + IntToStr(InfoColloquioForm.SEPunteggio.Value) + ')';
                                             xMot := 'il ' + DateToStr(InfoColloquioForm.DataColloquio.Date) + ' (' + InfoColloquioForm.CBGiudizio.text + '-' + IntToStr(InfoColloquioForm.SEPunteggio.Value) + ')';
                                        end;

                                        Q.ParamByName['xFlagAccPres'] := InfoColloquioForm.CBFlagAccPres.Checked;
                                        Q.ExecSQL;
                                   end;
                                   if (xIDRic > 0) and (InfoColloquioForm.CBEsito.Text = 'eliminare dalla ricerca') then begin
                                        try
                                             DataRicerche.QGen.Close;
                                             DataRicerche.QGen.SQL.Clear;
                                             DataRicerche.QGen.SQL.Add('update EBC_CandidatiRicerche set Escluso=1,');
                                             DataRicerche.QGen.SQL.Add('Stato=''eliminato (motivo: ' + StringReplace(copy(InfoColloquioForm.eMotivoElim.Text, 1, 230), '''', '''''', [rfReplaceAll]) + ')'' where IDAnagrafica=' + DataRicerche.QCandRic.FieldByName('IDAnagrafica').asString + ' and IDRicerca=' + DataRicerche.QCandRic.FieldByName('IDRicerca').asString);
                                             DataRicerche.QGen.execSQL;
                                             // aggiornamento storico
                                             if Q.Active then Q.Close;
                                             Q.SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
                                                  'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xIDCliente:)';
                                             Q.ParamByName['xIDAnagrafica'] := xIDAnag;
                                             Q.ParamByName['xIDEvento'] := 17;
                                             Q.ParamByName['xDataEvento'] := xData;
                                             Q.ParamByName['xAnnotazioni'] := copy('ric.' + TrimRight(StringReplace(DataRicerche.TRicerchePend.FieldByName('Progressivo').AsString, '"', '''', [rfReplaceAll, rfIgnoreCase])) + ' per ' + TrimRight(DataRicerche.TRicerchePend.FieldByName('Cliente').asString) + ' (motivo: ' + InfoColloquioForm.eMotivoElim.Text + ')', 1, 256);
                                             Q.ParamByName['xIDRicerca'] := xIDRic;
                                             Q.ParamByName['xIDUtente'] := xIDUtente;
                                             Q.ParamByName['xIDCliente'] := xIDCliente;
                                             // try
                                             Q.ExecSQL;
                                             //except
                                               //   on E: Exception do begin
                                                 // ShowMessage(E.Message);
                                                  //ShowMessage(q.SQL.Text);
                                                  //ShowMessage(q.SQL.GetText);
                                                  //end;
                                             //end;
                                             // controllo che non sia associato a ricerche diverse da questa dove non � gi� escluso
                                             DataRicerche.QGen.Close;
                                             DataRicerche.QGen.SQL.Clear;
                                             DataRicerche.QGen.SQL.Add('select IDRicerca from EBC_CandidatiRicerche where IDAnagrafica=' + DataRicerche.QCandRic.FieldByName('IDAnagrafica').asString + ' and IDRicerca<>' + DataRicerche.QCandRic.FieldByName('IDRicerca').asString + ' and Escluso=0');
                                             DataRicerche.QGen.Open;
                                             if not DataRicerche.QGen.IsEmpty then begin
                                                  // lasciato in selezione
                                                  MessageDlg('Il candidato � associato ad altre ricerche dove non � gi� escluso - verr� quindi mantenuto in selezione', mtInformation, [mbOK], 0);
                                             end else begin
                                                  // archiviazione CV
                                                  if Q.Active then Q.Close;
                                                  Q.SQL.text := 'update Anagrafica set IDStato=28,IDTipoStato=2 where ID=' + DataRicerche.QCandRic.FieldByName('IDAnagrafica').asString;
                                                  Q.ExecSQL;
                                             end;
                                             //   Data.DB.CommitTrans;

                                             DataRicerche.QCandRic.Close;
                                             DataRicerche.QCandRic.Open;
                                             SelPersForm.LTotCand.Caption := IntToStr(DataRicerche.QCandRic.RecordCount);
                                        except
                                             //     Data.DB.RollbackTrans;
                                             MessageDlg('Errore sul database:  inserimento non avvenuto', mtError, [mbOK], 0);
                                             raise;
                                        end;

                                   end;
                                   if InfoColloquioForm.CBEsito.Text = 'altro colloquio' then begin
                                        xMot := 'fissato colloquio';
                                        // fissazione colloquio
                                        Application.CreateForm(TFissaColloquioForm, FissaColloquioForm);
                                        FissaColloquioForm.DEData.Date := Date;
                                        FissaColloquioForm.Showmodal;
                                        if FissaColloquioForm.ModalResult = mrOK then begin
                                             xMot := xMot + ' il ' + DateToStr(FissaColloquioForm.DEData.Date) + ' alle ' + FissaColloquioForm.Ora.Text;
                                             // aggiornamento dati candidato-ricerca
                                             if xIDRic > 0 then begin
                                                  Q := CreateQueryFromString('update EBC_CandidatiRicerche set DataImpegno=:xData:,Escluso=0,Stato=:xStato:,FlagAccPres=:xFlagAccPres: ' +
                                                       'where IDAnagrafica=' + IntToStr(xIDAnag) + ' and IDRicerca=' + IntToStr(xIDRic));
                                                  Q.ParamByName['xData'] := FissaColloquioForm.DEData.Date;
                                                  Q.ParamByName['xStato'] := 'fissato colloquio alle ore ' + FissaColloquioForm.Ora.Text + ' (' + FissaColloquioForm.TRisorse.FieldByName('Risorsa').AsString + ')';
                                                  Q.ParamByName['xFlagAccPres'] := InfoColloquioForm.CBFlagAccPres.Checked;
                                                  Q.ExecSQL;
                                             end;
                                             // agenda
                                             Q := CreateQueryFromString('Insert into Agenda (Data,Ore,AlleOre,IDUtente,IDRisorsa,Tipo,IDCandRic,Descrizione) ' +
                                                  'values (:xData:,:xOre:,:xAlleOre:,:xIDUtente:,:xIDRisorsa:,:xTipo:,:xIDCandRic:,:xDescrizione:)');
                                             Q.ParamByName['xData'] := FissaColloquioForm.DEData.Date;
                                             Q.ParamByName['xOre'] := StrToDateTime(DatetoStr(FissaColloquioForm.DEData.Date) + ' ' + FissaColloquioForm.Ora.Text);
                                             Q.ParamByName['xAlleOre'] := StrToDateTime(DatetoStr(FissaColloquioForm.DEData.Date) + ' ' + FissaColloquioForm.MEAlleOre.Text);
                                             Q.ParamByName['xIDUtente'] := xIDUtente;
                                             Q.ParamByName['xIDRisorsa'] := FissaColloquioForm.TRisorse.FieldByName('ID').AsInteger;
                                             Q.ParamByName['xTipo'] := 2;
                                             Q.ParamByName['xIDCandRic'] := DataRicerche.TRicAnagIDX.FieldByName('ID').AsInteger;
                                             Q.ParamByName['xDescrizione'] := xCognome + ' ' + copy(xNome, 1, 1);
                                             Q.ExecSQL;
                                        end;
                                        FissaColloquioForm.Free;
                                   end;
                                   //end;

                                   if InfoColloquioForm.CBEsito.Text = 'colloquio conosciutivo' then begin
                                        xMot := 'colloquio conosciutivo';
                                        // fissazione colloquio
                                        if InfoColloquioForm.CBColloquioCliente.checked then
                                             xMot := xMot + ' presso cliente ' + InfoColloquioForm.CBDettCollCliente.text + ' il ' + DateToStr(InfoColloquioForm.DataColloquio.Date) + ' (Giudizio: ' + InfoColloquioForm.CBGiudizio.text + ' - Puntegg.: ' + IntToStr(InfoColloquioForm.SEPunteggio.Value) + ')'
                                        else
                                             xMot := xMot + ' il ' + DateToStr(InfoColloquioForm.DataColloquio.Date) + ' (Giudizio: ' + InfoColloquioForm.CBGiudizio.text + ' - Puntegg.: ' + IntToStr(InfoColloquioForm.SEPunteggio.Value) + ')';
                                        if Q.Active then Q.Close;
                                        {
                                        Q.SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
                                             ' select :xIDAnagrafica:,ID,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xIDCliente: from ebc_eventi ' +
                                             ' where evento like ''colloquio conosciutivo''';
                                             Q.ParamByName['xIDAnagrafica'] := xIDAnag;
                                        Q.ParamByName['xDataEvento'] := xData;
                                        Q.ParamByName['xAnnotazioni'] := xMot;
                                        Q.ParamByName['xIDRicerca'] := xIDRic;
                                        Q.ParamByName['xIDUtente'] := xIDUtente;
                                        Q.ParamByName['xIDCliente'] := xIDCliente;
                                        Q.ExecSQL;
                                        }
                                   end;
                              end else xAnnulla := True;

                              InfoColloquioForm.Free;

                         end;

                    end;
               6: begin // presentazione all'azienda
                         Application.CreateForm(TPresentazForm, PresentazForm);
                         PresentazForm.ShowModal;
                         if PresentazForm.ModalResult = mrCancel then xAnnulla := True
                         else begin
                              if xIDcliente = 0 then begin
                                   Application.CreateForm(TInserimentoForm, InserimentoForm);
                                   InserimentoForm.Caption := 'Selezione dell''azienda per presentazione';
                                   InserimentoForm.TEBCclienti.Locate('ID', xIDcliente, []);
                                   InserimentoForm.ShowModal;
                                   if InserimentoForm.ModalResult = mrCancel then xAnnulla := True
                                   else xIDCliente := InserimentoForm.TEBCclienti.FieldByName('ID').AsInteger;
                                   InserimentoForm.Free;
                              end;
                              // messaggio PRIVACY
                              MessageDlg('ATTENZIONE!! Prima di procedere con la presentazione di personale ' +
                                   'accertarsi che il/i candidato/i sia/siano consenziente/i. (Decreto Legislativo 30 Giugno 2003 N. 196)', mtInformation, [mbOK], 0);
                              if xIDRic > 0 then begin
                                   // aggiornamento dati candidato-ricerca
                                   Q := CreateQueryFromString('update EBC_CandidatiRicerche set DataImpegno=null,Escluso=0,Stato=:xStato: ' +
                                        'where IDAnagrafica=' + IntToStr(xIDAnag) + ' and IDRicerca=' + IntToStr(xIDRic));
                                   if PresentazForm.RGOpzioni.ItemIndex >= 0 then
                                        Q.ParamByName['xStato'] := 'presentato il ' + DateToStr(xData) + ' ' + xmot + ' ' + PresentazForm.RGOpzioni.Items[PresentazForm.RGOpzioni.ItemIndex]
                                   else Q.ParamByName['xStato'] := 'presentato il ' + DateToStr(xData) + ' ' + xmot;
                                   Q.ExecSQL;
                              end;
                              if PresentazForm.RGOpzioni.ItemIndex >= 0 then
                                   xMot := PresentazForm.RGOpzioni.Items[PresentazForm.RGOpzioni.ItemIndex] + ' ' + xMot;
                              if PresentazForm.RGOpzioni2.ItemIndex >= 0 then
                                   xMot := PresentazForm.RGOpzioni2.Items[PresentazForm.RGOpzioni2.ItemIndex] + ' ' + xMot;
                         end;
                         PresentazForm.Free;
                    end;
               7: begin // eliminazione dall'archivio
                         DataRicerche.QGen.Close;
                         DataRicerche.QGen.SQL.Clear;
                         DataRicerche.QGen.SQL.Add('select IDRicerca from EBC_CandidatiRicerche where IDAnagrafica=' + IntToStr(xIDAnag));
                         DataRicerche.QGen.Open;
                         if not DataRicerche.QGen.IsEmpty then begin
                              if MessageDlg('Il candidato � associato a una o pi� ricerche: eliminarlo da queste ?', mtWarning, [mbYes, mbNO], 0) = mrYes then begin
                                   DataRicerche.QGen.Close;
                                   DataRicerche.QGen.SQL.Clear;
                                   DataRicerche.QGen.SQL.Add('update EBC_CandidatiRicerche set Escluso=1,');
                                   DataRicerche.QGen.SQL.Add('Stato=''ELIMINATO ' + xMot + ''' where IDAnagrafica=' + IntToStr(xIDAnag));
                                   DataRicerche.QGen.execSQL;
                              end;
                         end;
                    end;
               8: begin //Inserimento in azienda

                         Data2.TClientiABS.Open;
                         Data2.TClientiABS.Locate('ID', xIDCliente, []);
                         xMot := xMot + ' ' + Data2.TClientiABS.FieldByName('Descrizione').AsString;
                         Q := CreateQueryFromString('insert into EBC_Inserimenti (IDAnagrafica,IDCliente,DallaData,IDRicerca,DataControllo) ' +
                              'values (:xIDAnagrafica:,:xIDCliente:,:xDallaData:,:xIDRicerca:,:xDataControllo:)');
                         Q.ParamByName['xIDAnagrafica'] := xIDAnag;
                         Q.ParamByName['xIDCliente'] := xIDCliente;
                         //Q.ParamByName['xDallaData'] := Date;
                         Q.ParamByName['xDallaData'] := xData;
                         Q.ParamByName['xIDRicerca'] := xIDRic;
                         Q.ParamByName['xDataControllo'] := NULL;
                         Q.ExecSQL;
                         DateInserimentoForm.Free;

                         // storico anagrafica
                         StoricizzaAnagrafica(xData, xIDAnag, false);

                         if MessageDlg('Escludere automaticamente il candidato dalle ricerche cui � associato ?', mtWarning, [mbYes, mbNo], 0) = mrYes then begin
                              DataRicerche.QGen.Close;
                              DataRicerche.QGen.SQL.Clear;
                              DataRicerche.QGen.SQL.Add('update EBC_CandidatiRicerche set Escluso=1,');
                              DataRicerche.QGen.SQL.Add('Stato=substring(''inserimento in azienda ' + TrimRight(StringReplace(Data2.TClientiABS.FieldByName('Descrizione').AsString, '''', '''''', [rfReplaceAll])) + ''',1,80) where IDAnagrafica=' + IntToStr(xIDAnag));
                              DataRicerche.QGen.execSQL;
                         end;
                         if xIDRic > 0 then begin
                              if MessageDlg('Inserire in esperienze lavorative (come attuale) ?', mtWarning, [mbYes, mbNo], 0) = mrYes then begin
                                   //inserimento in esperienze lavorative (prima tutte le esp.lav. NON ATTUALI del sogg.)
                                   Data.Q1.Close;
                                   Data.Q1.SQL.text := 'select IDArea,IDMansione from EBC_Ricerche where ID=' + IntToStr(xIDRic);
                                   Data.Q1.Open;
                                   Q := CreateQueryFromString('update EsperienzeLavorative set Attuale=0 ' +
                                        'where IDAnagrafica=:xIDAnagrafica:');
                                   Q.ParamByName['xIDAnagrafica'] := xIDAnag;
                                   Q.ExecSQL;
                                   Q := CreateQueryFromString('insert into EsperienzeLavorative (IDAnagrafica,IDAzienda,Attuale,AnnoDal,IDArea,IDMansione,IDSettore) ' +
                                        'values (:xIDAnagrafica:,:xIDAzienda:,:xAttuale:,:xAnnoDal:,:xIDArea:,:xIDMansione:,:xIDSettore:)');
                                   Q.ParamByName['xIDAnagrafica'] := xIDAnag;
                                   Q.ParamByName['xIDAzienda'] := xIDCliente;
                                   Q.ParamByName['xAttuale'] := 1;
                                   DecodeDate(Date, xYear, xMonth, xDay);
                                   Q.ParamByName['xAnnoDal'] := xYear;
                                   if Data.Q1.IsEmpty then begin
                                        Q.ParamByName['xIDArea'] := 0;
                                        Q.ParamByName['xIDMansione'] := 0;
                                   end else begin
                                        Q.ParamByName['xIDArea'] := Data.Q1.FieldByName('IDArea').asInteger;
                                        Q.ParamByName['xIDMansione'] := Data.Q1.FieldByName('IDMansione').asInteger;
                                   end;
                                   // settore dell'azienda
                                   Data.Q1.Close;
                                   Data.Q1.SQL.text := 'select IDAttivita from EBC_Clienti where ID=' + IntToStr(xIDCliente);
                                   Data.Q1.Open;
                                   Q.ParamByName['xIDSettore'] := Data.Q1.FieldByName('IDAttivita').asInteger;
                                   Data.Q1.Close;
                                   Q.ExecSQL;
                                   Data.Q1.Close;
                              end;
                              // aggiornamento dati candidato-ricerca
                              Q := CreateQueryFromString('update EBC_CandidatiRicerche set Escluso=0,DataImpegno=null,Stato=:xStato: ' +
                                   'where IDAnagrafica=' + IntToStr(xIDAnag) + ' and IDRicerca=' + IntToStr(xIDRic));
                              if pos('EMERGENCY', uppercase(Data.Global.fieldbyname('nomeazienda').AsString)) > 0 then
                                   Q.ParamByName['xStato'] := 'FIT'
                              else
                                   Q.ParamByName['xStato'] := 'inserito in azienda';
                              Q.ExecSQL;
                              // richiesta di conclusione ricerca
                              MessageDlg('ATTENZIONE: verificare se rendere conclusa la ricerca.', mtWarning, [mbOK], 0);
                              Data2.TClientiABS.Close;
                         end;
                    end;
               (*  xProcedi := FALSE;
                 xMatricola := '0';
                 // Q := CreateQueryFromString('Select max (Matricola) as Matricola from anagrafica where matricola is not null');
                  //Q.Open;
                  //if Q.RecordCount > 0 then xMatricola := inttostr(Q.FieldByName('Matricola').AsInteger + 1);
                 if InputQuery('Matricola nuovo assunto', 'Matricola', xMatricola) then begin
                      Q := CreateQueryFromString('Update Anagrafica set matricola = :xMatricola: where id = ' + Data.TAnagraficaID.AsString);
                      Q.ParamByName['xMatricola'] := xMatricola;
                      Q.ExecSQL;
                 end;
                 //                         OpenTab('Aziende',['ID','Descrizione'],['Azienda'],false);
                 xIDAzienda := '0';

                 xIDAzienda := OpenSelFromTab('Aziende', 'ID', 'Descrizione', 'Azienda', '', false);
                 if xIDAzienda <> '0' then begin
                      Q := CreateQueryFromString('Update Anagrafica set idazienda = :xIDAzienda: where id = ' + Data.TAnagraficaID.AsString);
                      Q.ParamByName['xIDAzienda'] := xIDAzienda;
                      Q.ExecSQL;
                 end;

                 // Controllo Comune
                 if Data.TAnagraficaIDComuneRes.AsString = '' then xProcedi := TRUE;
                 if xProcedi = FALSE then begin
                      Q.Close;
                      Q.SQL.Text := 'select * from tabcom where id = ' + Data.TAnagraficaIDComuneRes.AsString;
                      Q.Open;

                      if Q.RecordCount = 0 then xProcedi := TRUE;
                 end;
                 if xProcedi then begin

                      if MessageDlg('Attenzione: Il comune di residenza non � corretto a fini fiscali!', mtWarning, [mbOk], 0) = mrOk then begin

                           Application.CreateForm(TComuniForm, ComuniForm);
                           ComuniForm.ShowModal;
                           if ComuniForm.ModalResult = mrOk then begin
                                Q.Close;
                                Q.SQL.Text := 'update anagrafica set idcomuneres=:xIDComuneres:, provincia=:xProvincia:, ' +
                                     'comune = :xcomune:, CAP = :xCAP: where id = ' + Data.TAnagraficaID.AsString;
                                Q.ParamByName['xIDComuneRes'] := ComuniForm.QComuni.FieldByName('ID').Value;
                                Q.ParamByName['xProvincia'] := ComuniForm.QComuni.FieldByName('Prov').Value;
                                Q.ParamByName['xComune'] := ComuniForm.QComuni.FieldByName('Descrizione').Value;
                                Q.ParamByName['xCAP'] := ComuniForm.QComuni.FieldByName('CAP').Value;
                                Q.ExecSQL;
                           end;
                           ComuniForm.Free;
                      end;

                 end;
            end;   *)


               10: begin
                         if MessageDlg('Vuoi procedere con l''attribuzione delle competenze?', mtInformation, [mbYes, mbNo], 0) = mrYes
                              then begin
                              Application.CreateForm(TInsCompetenzaForm, InsCompetenzaForm);
                              //InsCompetenzaForm.TAreeComp.Close;
                              InsCompetenzaForm.TAreeComp.Filter := 'descrizione = ''Paese idoneit�''';
                              InsCompetenzaForm.TAreeComp.Filtered := true;
                              InsCompetenzaForm.ASG1.ColCount := 1;
                              InsCompetenzaForm.ShowModal;
                              if InsCompetenzaForm.ModalResult = mrOK then begin
                                   for i := 1 to InsCompetenzaForm.ASG1.RowCount - 1 do begin
                                        InsCompetenzaForm.ASG1.GetCheckBoxState(0, i, Vero);
                                        //if Vero then begin
                                        with Data do begin
                                             // DB.BeginTrans;
                                             try
                                                  if Vero then begin
                                                       Q1.Close;
                                                       Q1.SQL.text := 'insert into CompetenzeAnagrafica (IDAnagrafica,IDCompetenza,Valore) ' +
                                                            'values (:xIDAnagrafica:,:xIDCompetenza:,:xValore:)';
                                                       Q1.ParamByName['xIDAnagrafica'] := Data.TAnagrafica.FieldByName('ID').AsInteger;
                                                       Q1.ParamByName['xIDCompetenza'] := InsCompetenzaForm.xArrayIDComp[i];
                                                       Q1.ParamByName['xValore'] := 1;
                                                       Q1.ExecSQL;

                                                       Q1.SQL.text := 'insert into StoricoCompAnag (IDCompetenza,IDAnagrafica,DallaData,MotivoAumento,valore) ' +
                                                            'values (:xIDCompetenza:,:xIDAnagrafica:,:xDallaData:,:xMotivoAumento:,:xvalore:)';
                                                       Q1.ParamByName['xIDCompetenza'] := InsCompetenzaForm.xArrayIDComp[i];
                                                       Q1.ParamByName['xIDAnagrafica'] := Data.TAnagrafica.FieldByName('ID').AsInteger;
                                                       Q1.ParamByName['xDallaData'] := Date;
                                                       Q1.ParamByName['xMotivoAumento'] := 'valore iniziale';
                                                       Q1.ParamByName['xValore'] := 1;
                                                       Q1.ExecSQL;

                                                  end else begin
                                                       Q1.Close;
                                                       Q1.SQL.text := 'insert into CompetenzeAnagrafica (IDAnagrafica,IDCompetenza,Valore) ' +
                                                            'values (:xIDAnagrafica:,:xIDCompetenza:,:xValore:)';
                                                       Q1.ParamByName['xIDAnagrafica'] := Data.TAnagrafica.FieldByName('ID').AsInteger;
                                                       Q1.ParamByName['xIDCompetenza'] := InsCompetenzaForm.xArrayIDComp[i];
                                                       Q1.ParamByName['xValore'] := 0;
                                                       Q1.ExecSQL;

                                                       Q1.SQL.text := 'insert into StoricoCompAnag (IDCompetenza,IDAnagrafica,DallaData,MotivoAumento,valore) ' +
                                                            'values (:xIDCompetenza:,:xIDAnagrafica:,:xDallaData:,:xMotivoAumento:,:xvalore:)';
                                                       Q1.ParamByName['xIDCompetenza'] := InsCompetenzaForm.xArrayIDComp[i];
                                                       Q1.ParamByName['xIDAnagrafica'] := Data.TAnagrafica.FieldByName('ID').AsInteger;
                                                       Q1.ParamByName['xDallaData'] := Date;
                                                       Q1.ParamByName['xMotivoAumento'] := 'valore iniziale';
                                                       Q1.ParamByName['xValore'] := 0;
                                                       Q1.ExecSQL;
                                                  end;

                                                  Data2.TCompDipendente.Close;
                                                  Data2.TCompDipendente.Open;
                                             except
                                                  MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                                                  raise;
                                             end;
                                        end;
                                   end;
                              end;
                              InsCompetenzaForm.Free;
                         end
                    end;

               12: begin // Contatto con il cliente
                         Application.CreateForm(TContattoConClienteForm, ContattoConClienteForm);
                         ContattoConClienteForm.ShowModal;
                         if ContattoConClienteForm.ModalResult = mrCancel then xAnnulla := True
                         else begin
                              if xIDcliente = 0 then begin
                                   Application.CreateForm(TInserimentoForm, InserimentoForm);
                                   InserimentoForm.Caption := 'Selezione dell''azienda';
                                   InserimentoForm.TEBCclienti.Locate('ID', xIDcliente, []);
                                   InserimentoForm.ShowModal;
                                   if InserimentoForm.ModalResult = mrCancel then xAnnulla := True
                                   else xIDCliente := InserimentoForm.TEBCclienti.FieldByName('ID').AsInteger;
                                   InserimentoForm.Free;
                              end;
                              if xIDRic > 0 then begin
                                   // aggiornamento dati candidato-ricerca
                                   Q := CreateQueryFromString('update EBC_CandidatiRicerche set DataImpegno=null,Escluso=0,Stato=:xStato: ' +
                                        'where IDAnagrafica=' + IntToStr(xIDAnag) + ' and IDRicerca=' + IntToStr(xIDRic));
                                   Q.ParamByName['xStato'] := 'contatto con il cliente il ' + DateToStr(xData) + ': ' + ContattoConClienteForm.RGTipoFeedback.Items[ContattoConClienteForm.RGTipoFeedback.ItemIndex];
                                   Q.ExecSQL;
                              end;
                              Data2.TClientiABS.close;
                              Data2.TClientiABS.Open;
                              Data2.TClientiABS.Locate('ID', xIDCliente, []);
                              xMot := Data2.TClientiABS.FieldByName('Descrizione').AsString + ': ' + ContattoConClienteForm.RGTipoFeedback.Items[ContattoConClienteForm.RGTipoFeedback.ItemIndex] + ' ' + xMot;
                         end;
                         ContattoConClienteForm.Free;
                    end;

               //Stato Sospeso
               13: begin
                         MotiviSospensioneForm := TMotiviSospensioneForm.create(nil);
                         MotiviSospensioneForm.showmodal;
                         if MotiviSospensioneForm.modalresult = MrOK then begin
                              Data.QTemp.close;
                              data.qtemp.SQL.Text := ' set dateformat dmy ' +
                                   ' update anagrafica set IDMotivoStatoSosp=' + MotiviSospensioneForm.QMotiviSospID.AsString + ',' +
                                   ' DataFineStatoSospeso=:xData ' +
                                   ' where id= ' + IntToStr(xIDAnag);
                              data.qtemp.Parameters.ParamByName('xData').value := MotiviSospensioneForm.dedata.date;
                              data.qtemp.ExecSQL;
                              if pos('EMERGENCY', UpperCase(data.Global.fieldbyname('NomeAzienda').asString)) <= 0 then
                                   xMot := 'Sospensione: ' + MotiviSospensioneForm.QMotiviSospDescMotivoStatoSosp.AsString + ' - Fino al: ' + MotiviSospensioneForm.DEData.text + ''
                              else xMot := 'Suspended for ' + MotiviSospensioneForm.QMotiviSospDescMotivoStatoSosp.AsString + ' - Until ' + MotiviSospensioneForm.DEData.text + '';
                         end else
                              xAnnulla := true;
                         MotiviSospensioneForm.free;
                    end;

               //  Inserimento date e motivo richiamo
               // prima era.................reinsrimento in selezione da sospeso
               14: begin
                         {        Data.QTemp.close;
                                 data.qtemp.SQL.Text := 'update anagrafica set IDMotivoStatoSosp=NULL, ' +
                                      ' DataFineStatoSospeso=NULL  where id= ' + IntToStr(xIDAnag);
                                 data.qtemp.ExecSQL;

                      }

                         if pos('EMERGENCY', UpperCase(data.Global.fieldbyname('NomeAzienda').asString)) > 0 then begin
                              MotiviRichiamoCandForm := TMotiviRichiamoCandForm.create(nil);
                              MotiviRichiamoCandForm.ShowModal;
                              if MotiviRichiamoCandForm.ModalResult = mrOK then begin
                                   if pos('EMERGENCY', UpperCase(data.Global.fieldbyname('NomeAzienda').asString)) > 0 then begin
                                        Q := CreateQueryFromString('update EBC_CandidatiRicerche set DataPrevRichiamo=''' + DatetoStr(MotiviRichiamoCandForm.DateEdit1.Date) + ''', ' +
                                             ' IDMotivorichiamoCandidato=''' + MotiviRichiamoCandForm.QMotiviRichiamoID.AsString + ''',Stato=NULL' +
                                             ' where IDAnagrafica=' + IntToStr(xIDAnag) + ' and IDRicerca=' + IntToStr(xIDRic));
                                        //q.ParamByName['xdataPrevRichiamo'] := MotiviRichiamoCandForm.DateEdit1.Text;
                                   end else begin
                                        Q := CreateQueryFromString('update EBC_CandidatiRicerche set DataPrevRichiamo=:xdataPrevRichiamo: , ' +
                                             ' IDMotivorichiamoCandidato=''' + MotiviRichiamoCandForm.QMotiviRichiamoID.AsString + ''',Stato=:xStato:' +
                                             ' where IDAnagrafica=' + IntToStr(xIDAnag) + ' and IDRicerca=' + IntToStr(xIDRic));
                                        q.ParamByName['xdataPrevRichiamo'] := DatetoStr(MotiviRichiamoCandForm.DateEdit1.Date);
                                        //Q.ParamByName['xStato'] := MotiviRichiamoCandForm.QMotiviRichiamoDescMotivoRichiamoCandidato.AsString + ' - Data di Richiamo: ' + DatetoStr(MotiviRichiamoCandForm.QMotiviRichiamoDataCalc.AsDateTime);
                                        Q.ParamByName['xStato'] := MotiviRichiamoCandForm.QMotiviRichiamoDescMotivoRichiamoCandidato.AsString + ' - Data di Richiamo: ' + DatetoStr(MotiviRichiamoCandForm.DateEdit1.Date);
                                   end;
                                   q.ExecSQL;
                                   //xMot := 'Richiamo: ' + MotiviRichiamoCandForm.QMotiviRichiamoDescMotivoRichiamoCandidato.AsString + ' - (Data di Richiamo: ' + MotiviRichiamoCandForm.QMotiviRichiamoDataCalc.AsString + ') ';
                                   //xMot := 'To Recall ' + MotiviRichiamoCandForm.QMotiviRichiamoDescMotivoRichiamoCandidato.AsString + ' - by ' + DatetoStr(MotiviRichiamoCandForm.QMotiviRichiamoDataCalc.AsDateTime);
                                   if CambiaStato2Form.Edit1.Text = '' then
                                        xMot := 'To Recall ' + MotiviRichiamoCandForm.QMotiviRichiamoDescMotivoRichiamoCandidato.AsString + ' - by ' + DatetoStr(MotiviRichiamoCandForm.DateEdit1.Date)
                                   else xMot := 'To Recall ' + MotiviRichiamoCandForm.QMotiviRichiamoDescMotivoRichiamoCandidato.AsString + ' - by ' + DatetoStr(MotiviRichiamoCandForm.DateEdit1.Date) + ' - ' + CambiaStato2Form.Edit1.Text;
                              end else
                                   xAnnulla := true;
                              MotiviRichiamoCandForm.free;
                         end;
                    end;

               15: begin
                         // fissare colloquio
                         Application.CreateForm(TFissaColloquioForm, FissaColloquioForm);
                         FissaColloquioForm.TSelezionatori.Locate('ID', MainForm.xIDUtenteAttuale, []);
                         FissaColloquioForm.DEData.Date := Date;
                         // DecodeTime(Now, xHour, xMin, xSec, xMSec);
                         data.QOrarioAttuale.close;
                         data.QOrarioAttuale.Open;
                         xHour := data.QOrarioAttualeOre.asString;
                         xMin := data.QOrarioAttualeMinuti.asString;

                         //  showmessage('ora=' + inttostr(xHour) + '  min=' + inttostr(xMin)+'  sec='+inttostr(xSec));

                         FissaColloquioForm.Ora.Text := xHour + xseparatore + xMin;
                         {  if strtoint(xMin) + 30 >= 60 then begin
                                xHour := inttostr(strtoint(xhour) + 1);
                                     xMin := xMin - 60;
                           end; }
                         FissaColloquioForm.MEAlleOre.Text := data.QOrarioAttualeOreMagg.AsString + xseparatore + data.QOrarioAttualeMinutiMagg.AsString;

                         FissaColloquioForm.Showmodal;
                         if FissaColloquioForm.ModalResult = mrCancel then xAnnulla := True else begin
                              if pos('EMERGENCY', UpperCase(data.Global.fieldbyname('NomeAzienda').asString)) > 0 then begin
                                   if FissaColloquioForm.CBLuogo.Checked then
                                        xMot := xMot + ' il ' + DateToStr(FissaColloquioForm.DEData.Date) //+ ' alle ' + FissaColloquioForm.Ora.Text
                                   else
                                        xMot := xMot + ' Invited to ' + FissaColloquioForm.TRisorse.FieldByName('Risorsa').AsString + ' il ' + DateToStr(FissaColloquioForm.DEData.Date); // + ' alle ' + FissaColloquioForm.Ora.Text;
                              end else xMot := xMot + ' il ' + DateToStr(FissaColloquioForm.DEData.Date); // + ' alle ' + FissaColloquioForm.Ora.Text;

                              xIDCandRic := 0;
                              if xIDRic > 0 then begin
                                   // aggiornamento dati candidato-ricerca
                                   if not xAnnulla then begin
                                        Q := CreateQueryFromString('update EBC_CandidatiRicerche set DataPrevRichiamo=NULL,  IDMotivorichiamoCandidato=NULL, Escluso=0, ' +
                                             'DataImpegno=:xDataImpegno:, Stato=:xStato: where IDAnagrafica=' + IntToStr(xIDAnag) + ' and IDRicerca=' + IntToStr(xIDRic));
                                        Q.ParamByName['xDataImpegno'] := FissaColloquioForm.DEData.Date;
                                        if pos('INTERMEDIA', UpperCase(DAta.Global.FieldByName('NomeAzienda').AsString)) > 0 then begin
                                             if FissaColloquioForm.CBInterno.Checked then
                                                  if FissaColloquioForm.CBLuogo.Checked then
                                                       Q.ParamByName['xStato'] := 'colloquio in data ' + FissaColloquioForm.DEData.Text //+' del '+ FissaColloquioForm.DEData.Text;
                                                  else Q.ParamByName['xStato'] := FissaColloquioForm.TRisorse.FieldByName('Risorsa').AsString + ' - colloquio in data (' + FissaColloquioForm.DEData.Text + ') ' + ContattoCandForm.ENote.Text
                                             else Q.ParamByName['xStato'] := 'colloquio ESTERNO in data ' + FissaColloquioForm.DEData.Text + ' ' + ContattoCandForm.ENote.Text;
                                        end else if pos('EMERGENCY', UpperCase(data.Global.fieldbyname('NomeAzienda').asString)) > 0 then begin
                                             if FissaColloquioForm.CBInterno.Checked then begin
                                                  if FissaColloquioForm.CBLuogo.Checked then begin
                                                       Q.ParamByName['xStato'] := 'Invited to';
                                                  end else begin
                                                       // showmessage(FissaColloquioForm.TRisorse.FieldByName('Risorsa').AsString);
                                                       Q.ParamByName['xStato'] := 'Invited to ' + FissaColloquioForm.TRisorse.FieldByName('Risorsa').AsString;
                                                       xMot := ' Invited to ' + FissaColloquioForm.TRisorse.FieldByName('Risorsa').AsString + ' - ' + DatetoStr(FissaColloquioForm.DEData.Date);
                                                  end;
                                             end;
                                        end else Q.ParamByName['xStato'] := 'Colloquio ESTERNO ' + ContattoCandForm.ENote.Text;
                                   end else begin
                                        if FissaColloquioForm.CBInterno.Checked then
                                             if FissaColloquioForm.CBLuogo.Checked then begin
                                                  Q.ParamByName['xStato'] := ' ore ' + FissaColloquioForm.Ora.Text + ' colloquio';
                                             end else begin
                                                  // showmessage(FissaColloquioForm.TRisorse.FieldByName('Risorsa').AsString);
                                                  Q.ParamByName['xStato'] := ' ore ' + FissaColloquioForm.Ora.Text + ' colloquio (' + FissaColloquioForm.TRisorse.FieldByName('Risorsa').AsString + ') ';
                                                  xMot := xMot + ' colloquio (' + FissaColloquioForm.TRisorse.FieldByName('Risorsa').AsString + ') '
                                             end
                                        else Q.ParamByName['xStato'] := 'ore ' + FissaColloquioForm.Ora.Text + ' colloquio ESTERNO (' + ContattoCandForm.ENote.Text + ')';
                                   end;
                                   Q.ExecSQL;
                              end;
                              // aggiornamento dati contatti-candidato
                  {  Q := CreateQueryFromString('Insert into EBC_ContattiCandidati (IDRicerca,IDAnagrafica,TipoContatto,Data,Ora,Esito,Evaso,Note,IDUtente) ' +
                         'values (:xIDRicerca:,:xIDAnagrafica:,:xTipoContatto:,:xData:,:xOra:,:xEsito:,:xEvaso:,:xNote:,:xIDUtente:)');
                    Q.ParamByName['xIDRicerca'] := xIDRic;
                    Q.ParamByName['xIDAnagrafica'] := xIDAnag;
                    Q.ParamByName['xTipoContatto'] := ContattoCandForm.QTipiContattiTipoContatto.value;
                    Q.ParamByName['xData'] := ContattoCandForm.DEData.date;
                    Q.ParamByName['xOra'] := StrToDateTime(DatetoStr(ContattoCandForm.DEData.date) + ' ' + ContattoCandForm.MEOra.Text);
                    Q.ParamByName['xEsito'] := 'fissato colloquio';
                    Q.ParamByName['xEvaso'] := 0;
                    Q.ParamByName['xNote'] := ContattoCandForm.ENote.Text;
                    Q.ParamByName['xIDUtente'] := MainForm.xIDUtenteAttuale;
                    Q.ExecSQL; }
                         end;
                         if FissaColloquioForm.CBInterno.Checked then begin
                              if pos('EMERGENCY', UpperCase(data.Global.fieldbyname('NomeAzienda').asString)) = 0 then begin
                                   // agenda
                                   Q := CreateQueryFromString('select ID from EBC_CandidatiRicerche where IDAnagrafica=' + IntToStr(xIDAnag) + ' and IDRicerca=' + IntToStr(xIDRic));
                                   Q.Open;
                                   xIDCandRic := Q.FieldByName('ID').asInteger;
                                   Q.Close;
                                   Q := CreateQueryFromString('Insert into Agenda (Data,Ore,AlleOre,IDUtente,IDRisorsa,Tipo,IDCandRic,Descrizione) ' +
                                        'values (:xData:,:xOre:,:xAlleOre:,:xIDUtente:,:xIDRisorsa:,:xTipo:,:xIDCandRic:,:xDescrizione:)');
                                   Q.ParamByName['xData'] := FissaColloquioForm.DEData.Date;
                                   Q.ParamByName['xOre'] := StrToDateTime(DatetoStr(FissaColloquioForm.DEData.Date) + ' ' + FissaColloquioForm.Ora.Text);
                                   Q.ParamByName['xAlleOre'] := StrToDateTime(DatetoStr(FissaColloquioForm.DEData.Date) + ' ' + FissaColloquioForm.MEAlleOre.Text);
                                   Q.ParamByName['xIDUtente'] := FissaColloquioForm.TSelezionatori.FieldByName('ID').AsInteger;
                                   if FissaColloquioForm.CBLuogo.Checked then
                                        Q.ParamByName['xIDRisorsa'] := 0
                                   else Q.ParamByName['xIDRisorsa'] := FissaColloquioForm.TRisorse.FieldByName('ID').AsInteger;
                                   Q.ParamByName['xTipo'] := 1;
                                   Q.ParamByName['xIDCandRic'] := xIDCandRic;
                                   Q.ParamByName['xDescrizione'] := xCognome + ' ' + copy(xNome, 1, 1);
                                   Q.ExecSQL;
                              end;
                         end else xMot := xMot + ' ESTERNO';
                         FissaColloquioForm.Free;
                    end;

          end;
          if xAnnulla then begin
               if Data.DB.InTransaction then Data.DB.RollbackTrans;
               Result := False;
          end else begin
               // recupera tipo da ricerca
               if xIDRic > 0 then begin
                    Data.QTemp.Close;
                    Data.QTemp.SQL.clear;
                    Data.QTemp.SQL.text := 'select Tipo from EBC_CandidatiRicerche where IDAnagrafica=:x0 and IDRicerca=:x1';
                    Data.QTemp.Parameters[0].value := xIDAnag;
                    Data.QTemp.Parameters[1].value := xIDRic;
                    Data.QTemp.Open;
                    xTipoRicerca := Data.QTemp.FieldByName('Tipo').asString;
                    Data.QTemp.Close;
               end else xTipoRicerca := '';



               // aggiornam. storico
               if xIDCliente <> 0 then
                    Q := CreateQueryFromString('insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente,TipoRicerca) ' +
                         'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xIDCliente:,:xTipoRicerca:)')
               else
                    Q := CreateQueryFromString('insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,TipoRicerca) ' +
                         'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xTipoRicerca:)');
               Q.ParamByName['xIDAnagrafica'] := xIDAnag;
               Q.ParamByName['xIDEvento'] := xIDevento;
               Q.ParamByName['xDataEvento'] := xData;
               Q.ParamByName['xAnnotazioni'] := xMot;
               Q.ParamByName['xIDRicerca'] := xIDRic;
               Q.ParamByName['xIDUtente'] := xIDutente;
               if xIDCliente <> 0 then
                    Q.ParamByName['xIDCliente'] := xIDCliente;
               Q.ParamByName['xTipoRicerca'] := xTipoRicerca;
               Q.ExecSQL;
               // aggiornamento stato e tipo stato in anagrafica
               if xAggiornaStato then begin
                    Q := CreateQueryFromString('update Anagrafica set IDStato=:xIDStato:,IDTipoStato=:xIDTipoStato: ' +
                         'where ID=' + IntToStr(xIDAnag));
                    Q.ParamByName['xIDStato'] := xIDaStato;
                    Q.ParamByName['xIDTipoStato'] := xIDTipoStatoA;
                    Q.ExecSQL;
               end;

               //se IDDAStato = 76 (RIchiamare) allore cancello dataprevrichiamo e IDMotivoRichiamo
               if pos('EMERGENCY', Uppercase(Data.Global.fieldbyname('NomeAzienda').AsString)) > 0 then begin
                    if xIDaStato <> 76 then begin
                         Data.Q3.Close;
                         Data.Q3.SQL.Text := 'select iddastato from ebc_eventi where id = ' + inttostr(xidevento);
                         Data.Q3.Open;
                         if Data.Q3.fields[0].AsString = '76' then begin
                              Q := CreateQueryFromString('update EBC_CandidatiRicerche set DataPrevRichiamo=NULL, ' +
                                   ' IDMotivorichiamoCandidato=NULL where IDAnagrafica=' + IntToStr(xIDAnag) +
                                   ' and IDRicerca=' + IntToStr(xIDRic));
                              q.ExecSQL;
                         end;
                         Data.Q3.Close;
                    end;

                    if xIDaStato <> 70 then begin
                         Data.Q3.Close;
                         Data.Q3.SQL.Text := 'select iddastato from ebc_eventi where id = ' + inttostr(xidevento);
                         Data.Q3.Open;
                         if Data.Q3.fields[0].AsString = '70' then begin
                              Q := CreateQueryFromString('update anagrafica set DataFineStatoSospeso=NULL, ' +
                                   ' IDMotivoStatoSosp=NULL where ID=' + IntToStr(xIDAnag));
                              q.ExecSQL;
                         end;
                         Data.Q3.Close;
                    end;

               end;

               Data.DB.CommitTrans;
               //Data.TAnagrafica.Refresh;
               Result := True;
          end;
     except
          Data.DB.RollbackTrans;
          // showmessage('3');
          MessageDlg('Errore sul database: registrazione non avvenuta', mtError, [mbOK], 0);
          raise;
     end;
end;

end.

