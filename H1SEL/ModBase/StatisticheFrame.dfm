object FrmStatistiche: TFrmStatistiche
  Left = 0
  Top = 0
  Width = 775
  Height = 503
  TabOrder = 0
  object Splitter1: TSplitter
    Left = 772
    Top = 0
    Width = 3
    Height = 503
    Cursor = crHSplit
    Align = alRight
  end
  object Panel182: TPanel
    Left = 577
    Top = 0
    Width = 195
    Height = 503
    Align = alRight
    Color = 16762707
    TabOrder = 0
    object Label127: TLabel
      Left = 8
      Top = 8
      Width = 119
      Height = 13
      Caption = 'Aziende in Archivio oggi :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -7
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label128: TLabel
      Left = 8
      Top = 71
      Width = 95
      Height = 13
      Caption = 'CV in Archivio oggi :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -7
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label129: TLabel
      Left = 24
      Top = 27
      Width = 3
      Height = 13
    end
    object Label131: TLabel
      Left = 8
      Top = 40
      Width = 148
      Height = 13
      Caption = 'Aziende attive in Archivio oggi :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -7
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label132: TLabel
      Left = 8
      Top = 104
      Width = 117
      Height = 13
      Caption = 'Fatturato da inizio Anno :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -7
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label133: TLabel
      Left = 8
      Top = 181
      Width = 148
      Height = 13
      Caption = 'Numero Fatture da inizio Anno :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -7
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DBText16: TDBText
      Left = 17
      Top = 22
      Width = 65
      Height = 17
      DataField = 'tot'
      DataSource = DSQAzTot
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -7
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DBText17: TDBText
      Left = 17
      Top = 54
      Width = 65
      Height = 17
      DataField = 'tot'
      DataSource = DSQAzAttTot
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -7
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DBText18: TDBText
      Left = 17
      Top = 86
      Width = 65
      Height = 17
      DataField = 'tot'
      DataSource = DSCVTot
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -7
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DBText19: TDBText
      Left = 17
      Top = 118
      Width = 65
      Height = 17
      DataField = 'tot'
      DataSource = DSFatt
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -7
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DBText20: TDBText
      Left = 17
      Top = 195
      Width = 65
      Height = 17
      DataField = 'tot'
      DataSource = DSQNrFatt
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -7
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label134: TLabel
      Left = 7
      Top = 134
      Width = 126
      Height = 26
      Caption = 'Fatturato anno precedente'#13#10'(stesso periodo):'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -7
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DBText21: TDBText
      Left = 17
      Top = 163
      Width = 65
      Height = 17
      DataField = 'tot'
      DataSource = DSFattAS
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -7
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label135: TLabel
      Left = 8
      Top = 213
      Width = 157
      Height = 26
      Caption = 'Numero Fatture anno precedente'#13#10'(stesso periodo):'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -7
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DBText22: TDBText
      Left = 17
      Top = 241
      Width = 65
      Height = 17
      DataField = 'tot'
      DataSource = DSNrFattAS
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -7
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label136: TLabel
      Left = 8
      Top = 258
      Width = 139
      Height = 13
      Caption = 'Numero Colloqui ultimi 30 gg :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -7
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DBText23: TDBText
      Left = 17
      Top = 273
      Width = 65
      Height = 17
      DataField = 'tot'
      DataSource = DSNrColl
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -7
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DBText24: TDBText
      Left = 17
      Top = 318
      Width = 65
      Height = 17
      DataField = 'tot'
      DataSource = DSQNrCollAS
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -7
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label144: TLabel
      Left = 8
      Top = 290
      Width = 139
      Height = 26
      Caption = 'Numero Colloqui ultimi 30 gg '#13#10'Anno scorso, stesso periodo :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -7
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label150: TLabel
      Left = 8
      Top = 365
      Width = 162
      Height = 26
      Caption = 
        'Numero Presentazioni ultimi 30 gg '#13#10'Anno scorso, stesso periodo ' +
        ':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -7
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DBText25: TDBText
      Left = 17
      Top = 393
      Width = 65
      Height = 17
      DataField = 'tot'
      DataSource = DSNrPressAS
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -7
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DBText26: TDBText
      Left = 17
      Top = 348
      Width = 65
      Height = 17
      DataField = 'tot'
      DataSource = DSNrPres
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -7
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label151: TLabel
      Left = 8
      Top = 334
      Width = 165
      Height = 13
      Caption = 'Numero Presentazioni ultimi 30 gg :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -7
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label152: TLabel
      Left = 8
      Top = 409
      Width = 134
      Height = 13
      Caption = 'Numero Offerte ultimi 30 gg :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -7
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DBText27: TDBText
      Left = 17
      Top = 423
      Width = 65
      Height = 17
      DataField = 'tot'
      DataSource = DSNROff
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -7
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label153: TLabel
      Left = 8
      Top = 440
      Width = 139
      Height = 26
      Caption = 'Numero Offerte ultimi 30 gg '#13#10'Anno scorso, stesso periodo :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -7
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object DBText28: TDBText
      Left = 17
      Top = 468
      Width = 65
      Height = 17
      DataField = 'tot'
      DataSource = DSNrOffAS
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -7
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object Panel181: TPanel
    Left = 0
    Top = 0
    Width = 577
    Height = 503
    Align = alClient
    Caption = 'Panel221'
    TabOrder = 1
    object PageControl1: TPageControl
      Left = 1
      Top = 1
      Width = 575
      Height = 501
      ActivePage = TabSheet2
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = 'Master Report'
        TabVisible = False
        object ScrollBox1: TScrollBox
          Left = 0
          Top = 0
          Width = 567
          Height = 473
          VertScrollBar.Position = 1774
          Align = alClient
          TabOrder = 0
          object DBChartFatt: TDBChart
            Left = 0
            Top = 326
            Width = 547
            Height = 150
            Gradient.EndColor = 12615680
            Gradient.Visible = True
            Title.Font.Color = -1
            Title.Font.Height = -12
            Title.Font.Style = [fsBold]
            Title.Text.Strings = (
              'Andamento Fatturato ultimi anni')
            BottomAxis.LabelsFont.Height = -8
            LeftAxis.LabelsFont.Height = -8
            LeftAxis.Visible = False
            Legend.Visible = False
            Align = alTop
            TabOrder = 0
            OnDblClick = DBChartCVInsClick
            object SeriesFatt: TLineSeries
              Marks.ArrowLength = 8
              Marks.Callout.Brush.Color = clBlack
              Marks.Callout.Distance = 2
              Marks.Callout.Length = 8
              Marks.Brush.Color = clWhite
              Marks.Brush.Style = bsClear
              Marks.Font.Height = -12
              Marks.Font.Style = [fsBold]
              Marks.Frame.Visible = False
              Marks.Style = smsValue
              Marks.Visible = True
              DataSource = FatturatoxAnno
              SeriesColor = clGreen
              ValueFormat = '#,##0.00'
              XLabelsSource = 'anno'
              LineHeight = 1
              OutLine.Color = 33023
              OutLine.EndStyle = esSquare
              OutLine.Visible = True
              Pointer.InflateMargins = True
              Pointer.Style = psRectangle
              Pointer.Visible = False
              XValues.Name = 'X'
              XValues.Order = loAscending
              YValues.Name = 'Y'
              YValues.ValueSource = 'numero'
            end
          end
          object DBChartNrFatt: TDBChart
            Left = 0
            Top = 176
            Width = 547
            Height = 150
            Gradient.EndColor = 12615680
            Gradient.Visible = True
            Title.Font.Color = -1
            Title.Font.Height = -12
            Title.Font.Style = [fsBold]
            Title.Text.Strings = (
              'Numero Fatture Emesse')
            LeftAxis.Visible = False
            Legend.Visible = False
            Align = alTop
            TabOrder = 1
            OnDblClick = DBChartCVInsClick
            PrintMargins = (
              29
              15
              29
              15)
            object Button1: TButton
              Left = 488
              Top = 216
              Width = 75
              Height = 25
              Caption = 'Button1'
              TabOrder = 0
            end
            object SeriesNrFatt: TLineSeries
              Marks.ArrowLength = 8
              Marks.Callout.Brush.Color = clBlack
              Marks.Callout.Distance = 2
              Marks.Callout.Length = 8
              Marks.Brush.Color = clWhite
              Marks.Brush.Style = bsClear
              Marks.Font.Height = -12
              Marks.Font.Style = [fsBold]
              Marks.Frame.Visible = False
              Marks.Style = smsValue
              Marks.Visible = True
              DataSource = NrFatturexAnno
              SeriesColor = 4227072
              ValueFormat = '#,##0'
              XLabelsSource = 'anno'
              LineHeight = 1
              LinePen.Color = -1
              LinePen.EndStyle = esFlat
              OutLine.Color = 4227072
              OutLine.EndStyle = esFlat
              OutLine.Visible = True
              Pointer.InflateMargins = True
              Pointer.Style = psRectangle
              Pointer.Visible = False
              XValues.Name = 'X'
              XValues.Order = loAscending
              YValues.Name = 'Y'
              YValues.ValueSource = 'numero'
            end
          end
          object Panel1: TPanel
            Left = 0
            Top = -1774
            Width = 547
            Height = 150
            Align = alTop
            Caption = 'Panel1'
            TabOrder = 2
            object DBChartCVIns: TDBChart
              Left = 1
              Top = 1
              Width = 482
              Height = 148
              Gradient.EndColor = 12615680
              Gradient.Visible = True
              Title.Font.Color = clBlack
              Title.Font.Height = -12
              Title.Font.Style = [fsBold]
              Title.Text.Strings = (
                'Andamento inserimento CV (rosso anno attuale)')
              BottomAxis.LabelsFont.Height = -8
              LeftAxis.Visible = False
              Legend.Visible = False
              Align = alClient
              TabOrder = 0
              OnDblClick = DBChartCVInsClick
              object BarSeriesCVInseriti: TBarSeries
                BarPen.Style = psDot
                Marks.ArrowLength = 4
                Marks.Callout.Brush.Color = clBlack
                Marks.Callout.Distance = 3
                Marks.Callout.Length = 4
                Marks.Brush.Color = clWhite
                Marks.Brush.Style = bsClear
                Marks.Font.Color = clRed
                Marks.Font.Height = -9
                Marks.Frame.Visible = False
                Marks.Style = smsValue
                Marks.Visible = True
                DataSource = QCVInseriti
                SeriesColor = clRed
                XLabelsSource = 'mese'
                BarStyle = bsCilinder
                BarWidthPercent = 25
                Gradient.Direction = gdTopBottom
                XValues.Name = 'X'
                XValues.Order = loAscending
                YValues.Name = 'Bar'
                YValues.ValueSource = 'numero'
              end
              object BarSeriesCVInseritiAS: TBarSeries
                BarPen.Style = psDot
                Marks.ArrowLength = 9
                Marks.Callout.Brush.Color = clBlack
                Marks.Callout.Distance = 10
                Marks.Callout.Length = 9
                Marks.Brush.Color = clWhite
                Marks.Brush.Style = bsClear
                Marks.Font.Charset = ANSI_CHARSET
                Marks.Font.Height = -9
                Marks.Font.OutLine.Color = 4227327
                Marks.Frame.Visible = False
                Marks.Style = smsValue
                Marks.Visible = True
                DataSource = QCVInseritiAS
                SeriesColor = 65408
                XLabelsSource = 'mese'
                AutoMarkPosition = False
                BarStyle = bsCilinder
                BarWidthPercent = 25
                Gradient.Direction = gdTopBottom
                XValues.Name = 'X'
                XValues.Order = loAscending
                YValues.Name = 'Bar'
                YValues.ValueSource = 'numero'
              end
            end
            object DBChart4: TDBChart
              Left = 483
              Top = 1
              Width = 63
              Height = 148
              Gradient.EndColor = 12615680
              Gradient.Visible = True
              Title.Font.Color = clBlack
              Title.Text.Strings = (
                'Totale per '
                'Anno')
              BottomAxis.LabelsAngle = 45
              BottomAxis.LabelsFont.Height = -8
              BottomAxis.MaximumOffset = 10
              BottomAxis.MinimumOffset = 10
              LeftAxis.Visible = False
              Legend.Visible = False
              Align = alRight
              TabOrder = 1
              object BarSeriesTotCVPerAnno: TBarSeries
                BarPen.Style = psDot
                Depth = 5
                Marks.ArrowLength = 20
                Marks.Callout.Brush.Color = clBlack
                Marks.Callout.Distance = -15
                Marks.Callout.Length = 20
                Marks.Brush.Color = clWhite
                Marks.Brush.Style = bsClear
                Marks.Font.Height = -9
                Marks.Frame.Visible = False
                Marks.Style = smsValue
                Marks.Visible = True
                DataSource = QTotCVPerAnno
                SeriesColor = clRed
                XLabelsSource = 'anno'
                BarStyle = bsCilinder
                BarWidthPercent = 25
                Gradient.Direction = gdTopBottom
                XValues.Name = 'X'
                XValues.Order = loAscending
                YValues.Name = 'Bar'
                YValues.ValueSource = 'numero'
              end
            end
          end
          object PanelFattPrevTot: TPanel
            Left = 0
            Top = -424
            Width = 547
            Height = 150
            Align = alTop
            TabOrder = 3
            object DBChart3: TDBChart
              Left = 1
              Top = 1
              Width = 481
              Height = 148
              Gradient.EndColor = 12615680
              Gradient.Visible = True
              Title.Font.Color = clBlack
              Title.Font.Height = -12
              Title.Font.Style = [fsBold]
              Title.Text.Strings = (
                'Fatturato previsto Totale')
              BottomAxis.LabelsFont.Height = -8
              LeftAxis.Visible = False
              Legend.Visible = False
              Align = alClient
              TabOrder = 0
              OnDblClick = DBChartCVInsClick
              object BarSeries10: TBarSeries
                BarPen.Style = psDot
                Marks.ArrowLength = 20
                Marks.Callout.Brush.Color = clBlack
                Marks.Callout.Distance = -5
                Marks.Callout.Length = 20
                Marks.Brush.Color = clWhite
                Marks.Brush.Style = bsClear
                Marks.Font.Color = clRed
                Marks.Font.Height = -9
                Marks.Frame.Visible = False
                Marks.Style = smsValue
                Marks.Visible = True
                DataSource = QAndFattPrevistoTot
                SeriesColor = clRed
                XLabelsSource = 'mese'
                BarStyle = bsCilinder
                BarWidthPercent = 25
                Gradient.Direction = gdTopBottom
                XValues.Name = 'X'
                XValues.Order = loAscending
                YValues.Name = 'Bar'
                YValues.ValueSource = 'numero'
              end
            end
            object DBChart5: TDBChart
              Left = 482
              Top = 1
              Width = 64
              Height = 148
              Gradient.EndColor = 12615680
              Gradient.Visible = True
              Title.Font.Color = clBlack
              Title.Text.Strings = (
                'Totale per '
                'Anno')
              BottomAxis.LabelsAngle = 45
              BottomAxis.LabelsFont.Height = -8
              BottomAxis.MaximumOffset = 10
              BottomAxis.MinimumOffset = 10
              LeftAxis.Visible = False
              Legend.Visible = False
              Align = alRight
              TabOrder = 1
              object BarSeriesQAndFattPrevPerAnno: TBarSeries
                BarPen.Style = psDot
                Depth = 5
                Marks.ArrowLength = 20
                Marks.Callout.Brush.Color = clBlack
                Marks.Callout.Distance = -15
                Marks.Callout.Length = 20
                Marks.Brush.Color = clWhite
                Marks.Brush.Style = bsClear
                Marks.Font.Height = -9
                Marks.Frame.Visible = False
                Marks.Style = smsValue
                Marks.Visible = True
                DataSource = QAndFattPrevistoTotPerAnno
                SeriesColor = clRed
                XLabelsSource = 'anno'
                BarStyle = bsCilinder
                BarWidthPercent = 25
                Gradient.Direction = gdTopBottom
                XValues.Name = 'X'
                XValues.Order = loAscending
                YValues.Name = 'Bar'
                YValues.ValueSource = 'numero'
              end
            end
          end
          object Panel3: TPanel
            Left = 0
            Top = -1624
            Width = 547
            Height = 150
            Align = alTop
            Caption = 'Panel3'
            TabOrder = 4
            object DBChartAzIns: TDBChart
              Left = 1
              Top = 1
              Width = 481
              Height = 148
              Gradient.EndColor = 12615680
              Gradient.Visible = True
              Title.Font.Color = -1
              Title.Font.Height = -12
              Title.Font.Style = [fsBold]
              Title.Text.Strings = (
                'Andamento Clienti  Attivi (rosso anno attuale)')
              BottomAxis.LabelsFont.Height = -8
              LeftAxis.Visible = False
              Legend.Visible = False
              Align = alClient
              TabOrder = 0
              OnDblClick = DBChartCVInsClick
              object BarSeriesCliAtt: TBarSeries
                BarPen.Style = psDot
                Marks.ArrowLength = 4
                Marks.Callout.Brush.Color = clBlack
                Marks.Callout.Distance = 3
                Marks.Callout.Length = 4
                Marks.Brush.Color = clWhite
                Marks.Brush.Style = bsClear
                Marks.Font.Color = clRed
                Marks.Font.Height = -9
                Marks.Frame.Visible = False
                Marks.Style = smsValue
                Marks.Visible = True
                DataSource = ClientiAtt
                SeriesColor = clRed
                XLabelsSource = 'mese'
                BarStyle = bsCilinder
                BarWidthPercent = 25
                Gradient.Direction = gdTopBottom
                XValues.Name = 'X'
                XValues.Order = loAscending
                YValues.Name = 'Bar'
                YValues.ValueSource = 'numero'
              end
              object BarSeriesCliAttaS: TBarSeries
                BarPen.Style = psDot
                Marks.ArrowLength = 9
                Marks.Callout.Brush.Color = clBlack
                Marks.Callout.Distance = 10
                Marks.Callout.Length = 9
                Marks.Brush.Color = clWhite
                Marks.Brush.Style = bsClear
                Marks.Font.Height = -9
                Marks.Frame.Visible = False
                Marks.Style = smsValue
                Marks.Visible = True
                DataSource = ClientiATTAS
                SeriesColor = 65408
                XLabelsSource = 'mese'
                BarStyle = bsCilinder
                BarWidthPercent = 25
                Gradient.Direction = gdTopBottom
                XValues.Name = 'X'
                XValues.Order = loAscending
                YValues.Name = 'Bar'
                YValues.ValueSource = 'numero'
              end
            end
            object DBChart6: TDBChart
              Left = 482
              Top = 1
              Width = 64
              Height = 148
              Gradient.EndColor = 12615680
              Gradient.Visible = True
              Title.Font.Color = clBlack
              Title.Text.Strings = (
                'Totale per '
                'Anno')
              BottomAxis.LabelsAngle = 45
              BottomAxis.LabelsFont.Height = -8
              BottomAxis.MaximumOffset = 10
              BottomAxis.MinimumOffset = 10
              LeftAxis.Visible = False
              Legend.Visible = False
              Align = alRight
              TabOrder = 1
              object BarSeriesCliAttPerAnno: TBarSeries
                BarPen.Style = psDot
                Depth = 5
                Marks.ArrowLength = 20
                Marks.Callout.Brush.Color = clBlack
                Marks.Callout.Distance = -15
                Marks.Callout.Length = 20
                Marks.Brush.Color = clWhite
                Marks.Brush.Style = bsClear
                Marks.Font.Height = -9
                Marks.Frame.Visible = False
                Marks.Style = smsValue
                Marks.Visible = True
                DataSource = QTotCliAttPerAnno
                SeriesColor = clRed
                XLabelsSource = 'anno'
                BarStyle = bsCilinder
                BarWidthPercent = 25
                Gradient.Direction = gdTopBottom
                XValues.Name = 'X'
                XValues.Order = loAscending
                YValues.Name = 'Bar'
                YValues.ValueSource = 'numero'
              end
            end
          end
          object Panel4: TPanel
            Left = 0
            Top = -1474
            Width = 547
            Height = 150
            Align = alTop
            Caption = 'Panel4'
            TabOrder = 5
            object DBChartInsCli: TDBChart
              Left = 1
              Top = 1
              Width = 481
              Height = 148
              Gradient.EndColor = 12615680
              Gradient.Visible = True
              Title.Font.Color = -1
              Title.Font.Height = -12
              Title.Font.Style = [fsBold]
              Title.Text.Strings = (
                'Andamento inserimento Clienti  (rosso anno attuale)')
              BottomAxis.LabelsFont.Height = -8
              LeftAxis.Visible = False
              Legend.Visible = False
              Align = alClient
              TabOrder = 0
              OnDblClick = DBChartCVInsClick
              object BarSeries5: TBarSeries
                BarPen.Style = psDot
                Marks.ArrowLength = 4
                Marks.Callout.Brush.Color = clBlack
                Marks.Callout.Distance = 3
                Marks.Callout.Length = 4
                Marks.Brush.Color = clWhite
                Marks.Brush.Style = bsClear
                Marks.Font.Color = clRed
                Marks.Font.Height = -9
                Marks.Frame.Visible = False
                Marks.Style = smsValue
                Marks.Visible = True
                DataSource = QAzIns
                SeriesColor = clRed
                XLabelsSource = 'mese'
                BarStyle = bsCilinder
                BarWidthPercent = 25
                Gradient.Direction = gdTopBottom
                XValues.Name = 'X'
                XValues.Order = loAscending
                YValues.Name = 'Bar'
                YValues.ValueSource = 'numero'
              end
              object BarSeries6: TBarSeries
                BarPen.Style = psDot
                Marks.ArrowLength = 9
                Marks.Callout.Brush.Color = clBlack
                Marks.Callout.Distance = 10
                Marks.Callout.Length = 9
                Marks.Brush.Color = clWhite
                Marks.Brush.Style = bsClear
                Marks.Font.Height = -9
                Marks.Frame.Visible = False
                Marks.Style = smsValue
                Marks.Visible = True
                DataSource = QAzInsAS
                SeriesColor = 65408
                XLabelsSource = 'mese'
                BarStyle = bsCilinder
                BarWidthPercent = 25
                Gradient.Direction = gdTopBottom
                XValues.Name = 'X'
                XValues.Order = loAscending
                YValues.Name = 'Bar'
                YValues.ValueSource = 'numero'
              end
            end
            object DBChart7: TDBChart
              Left = 482
              Top = 1
              Width = 64
              Height = 148
              Gradient.EndColor = 12615680
              Gradient.Visible = True
              Title.Font.Color = clBlack
              Title.Text.Strings = (
                'Totale per '
                'Anno')
              BottomAxis.LabelsAngle = 45
              BottomAxis.LabelsFont.Height = -8
              BottomAxis.MaximumOffset = 10
              BottomAxis.MinimumOffset = 10
              LeftAxis.Visible = False
              Legend.Visible = False
              Align = alRight
              TabOrder = 1
              object BarSeries14: TBarSeries
                BarPen.Style = psDot
                Depth = 5
                Marks.ArrowLength = 20
                Marks.Callout.Brush.Color = clBlack
                Marks.Callout.Distance = -15
                Marks.Callout.Length = 20
                Marks.Brush.Color = clWhite
                Marks.Brush.Style = bsClear
                Marks.Font.Height = -9
                Marks.Frame.Visible = False
                Marks.Style = smsValue
                Marks.Visible = True
                DataSource = QTotClientiPerAnno
                SeriesColor = clRed
                XLabelsSource = 'anno'
                BarStyle = bsCilinder
                BarWidthPercent = 25
                Gradient.Direction = gdTopBottom
                XValues.Name = 'X'
                XValues.Order = loAscending
                YValues.Name = 'Bar'
                YValues.ValueSource = 'numero'
              end
            end
          end
          object PanelFattPrevUC: TPanel
            Left = 0
            Top = -274
            Width = 547
            Height = 150
            Align = alTop
            Caption = 'PanelFattPrevUC'
            TabOrder = 6
            object DBChart2: TDBChart
              Left = 1
              Top = 1
              Width = 481
              Height = 148
              Gradient.EndColor = 12615680
              Gradient.Visible = True
              Title.Font.Color = clBlack
              Title.Font.Height = -12
              Title.Font.Style = [fsBold]
              Title.Text.Strings = (
                'Fatturato previsto Utente connesso')
              BottomAxis.LabelsFont.Height = -8
              LeftAxis.Visible = False
              Legend.Visible = False
              Align = alClient
              TabOrder = 0
              OnDblClick = DBChartCVInsClick
              object BarSeries9: TBarSeries
                BarPen.Style = psDot
                Marks.ArrowLength = 20
                Marks.Callout.Brush.Color = clBlack
                Marks.Callout.Distance = -15
                Marks.Callout.Length = 20
                Marks.Brush.Color = clWhite
                Marks.Brush.Style = bsClear
                Marks.Font.Color = clRed
                Marks.Font.Height = -9
                Marks.Frame.Visible = False
                Marks.Style = smsValue
                Marks.Visible = True
                DataSource = QAndFattPUtenteCon
                SeriesColor = clRed
                XLabelsSource = 'mese'
                BarStyle = bsCilinder
                BarWidthPercent = 25
                Gradient.Direction = gdTopBottom
                XValues.Name = 'X'
                XValues.Order = loAscending
                YValues.Name = 'Bar'
                YValues.ValueSource = 'numero'
              end
            end
            object DBChart8: TDBChart
              Left = 482
              Top = 1
              Width = 64
              Height = 148
              Gradient.EndColor = 12615680
              Gradient.Visible = True
              Title.Font.Color = clBlack
              Title.Text.Strings = (
                'Totale per '
                'Anno')
              BottomAxis.LabelsAngle = 45
              BottomAxis.LabelsFont.Height = -8
              BottomAxis.MaximumOffset = 10
              BottomAxis.MinimumOffset = 10
              LeftAxis.Visible = False
              Legend.Visible = False
              Align = alRight
              TabOrder = 1
              object BarSeriesFattPrevUCPerAnno: TBarSeries
                BarPen.Style = psDot
                Depth = 5
                Marks.ArrowLength = 20
                Marks.Callout.Brush.Color = clBlack
                Marks.Callout.Distance = -15
                Marks.Callout.Length = 20
                Marks.Brush.Color = clWhite
                Marks.Brush.Style = bsClear
                Marks.Font.Height = -9
                Marks.Frame.Visible = False
                Marks.Style = smsValue
                Marks.Visible = True
                DataSource = QAndFattPUtenteConPerAnno
                SeriesColor = clRed
                XLabelsSource = 'anno'
                BarStyle = bsCilinder
                BarWidthPercent = 25
                Gradient.Direction = gdTopBottom
                XValues.Name = 'X'
                XValues.Order = loAscending
                YValues.Name = 'Bar'
                YValues.ValueSource = 'numero'
              end
            end
          end
          object PanelFattTotUtenteConn: TPanel
            Left = 0
            Top = 26
            Width = 547
            Height = 150
            Align = alTop
            Caption = 'PanelFattTotUtenteConn'
            TabOrder = 7
            object DBChart1: TDBChart
              Left = 1
              Top = 1
              Width = 481
              Height = 148
              Gradient.EndColor = 12615680
              Gradient.Visible = True
              Title.Font.Color = clBlack
              Title.Font.Height = -12
              Title.Font.Style = [fsBold]
              Title.Text.Strings = (
                'Fatturato totale Utente connesso (rosso anno attuale)')
              BottomAxis.LabelsFont.Height = -8
              LeftAxis.Visible = False
              Legend.Visible = False
              Align = alClient
              TabOrder = 0
              OnDblClick = DBChartCVInsClick
              object BarSeriesAndFattUC: TBarSeries
                BarPen.Style = psDot
                Marks.ArrowLength = 4
                Marks.Callout.Brush.Color = clBlack
                Marks.Callout.Distance = 3
                Marks.Callout.Length = 4
                Marks.Brush.Color = clWhite
                Marks.Brush.Style = bsClear
                Marks.Font.Color = clRed
                Marks.Font.Height = -9
                Marks.Frame.Visible = False
                Marks.Style = smsValue
                Marks.Visible = True
                DataSource = QAndFattSingoloUt
                SeriesColor = clRed
                XLabelsSource = 'mese'
                BarStyle = bsCilinder
                BarWidthPercent = 25
                Gradient.Direction = gdTopBottom
                XValues.Name = 'X'
                XValues.Order = loAscending
                YValues.Name = 'Bar'
                YValues.ValueSource = 'numero'
              end
              object BarSeriesAndFattUCAS: TBarSeries
                BarPen.Style = psDot
                Marks.ArrowLength = 9
                Marks.Callout.Brush.Color = clBlack
                Marks.Callout.Distance = 10
                Marks.Callout.Length = 9
                Marks.Brush.Color = clWhite
                Marks.Brush.Style = bsClear
                Marks.Font.Height = -9
                Marks.Frame.Visible = False
                Marks.Style = smsValue
                Marks.Visible = True
                DataSource = QAndFattSingoloUtAS
                SeriesColor = 65408
                XLabelsSource = 'mese'
                BarStyle = bsCilinder
                BarWidthPercent = 25
                Gradient.Direction = gdTopBottom
                XValues.Name = 'X'
                XValues.Order = loAscending
                YValues.Name = 'Bar'
                YValues.ValueSource = 'numero'
              end
            end
            object DBChart9: TDBChart
              Left = 482
              Top = 1
              Width = 64
              Height = 148
              Gradient.EndColor = 12615680
              Gradient.Visible = True
              Title.Font.Color = clBlack
              Title.Text.Strings = (
                'Totale per '
                'Anno')
              BottomAxis.LabelsAngle = 45
              BottomAxis.LabelsFont.Height = -8
              BottomAxis.MaximumOffset = 10
              BottomAxis.MinimumOffset = 10
              LeftAxis.Visible = False
              Legend.Visible = False
              Align = alRight
              TabOrder = 1
              object BarSeriesFattUCPerAnno: TBarSeries
                BarPen.Style = psDot
                Depth = 5
                Marks.ArrowLength = 20
                Marks.Callout.Brush.Color = clBlack
                Marks.Callout.Distance = -15
                Marks.Callout.Length = 20
                Marks.Brush.Color = clWhite
                Marks.Brush.Style = bsClear
                Marks.Font.Height = -9
                Marks.Frame.Visible = False
                Marks.Style = smsValue
                Marks.Visible = True
                DataSource = QAndFattUCPerAnno
                SeriesColor = clRed
                XLabelsSource = 'anno'
                BarStyle = bsCilinder
                BarWidthPercent = 25
                Gradient.Direction = gdTopBottom
                XValues.Name = 'X'
                XValues.Order = loAscending
                YValues.Name = 'Bar'
                YValues.ValueSource = 'numero'
              end
            end
          end
          object DBChartFattPrev: TDBChart
            Left = 0
            Top = -124
            Width = 547
            Height = 150
            Gradient.EndColor = 12615680
            Gradient.Visible = True
            Title.Font.Color = clBlack
            Title.Font.Height = -12
            Title.Font.Style = [fsBold]
            Title.Text.Strings = (
              'Fatturato Totale')
            BottomAxis.LabelsFont.Height = -8
            LeftAxis.Visible = False
            Legend.Visible = False
            Align = alTop
            TabOrder = 8
            OnDblClick = DBChartCVInsClick
            object BarSeriesFattAz: TBarSeries
              BarPen.Style = psDot
              Marks.ArrowLength = 4
              Marks.Callout.Brush.Color = clBlack
              Marks.Callout.Distance = 3
              Marks.Callout.Length = 4
              Marks.Brush.Color = clWhite
              Marks.Brush.Style = bsClear
              Marks.Font.Color = clRed
              Marks.Font.Height = -9
              Marks.Frame.Visible = False
              Marks.Style = smsValue
              Marks.Visible = True
              DataSource = QAndFattAz
              SeriesColor = clRed
              XLabelsSource = 'mese'
              BarStyle = bsCilinder
              BarWidthPercent = 25
              Gradient.Direction = gdTopBottom
              XValues.Name = 'X'
              XValues.Order = loAscending
              YValues.Name = 'Bar'
              YValues.ValueSource = 'numero'
            end
            object BarSeriesFattAzAS: TBarSeries
              BarPen.Style = psDot
              Marks.ArrowLength = 9
              Marks.Callout.Brush.Color = clBlack
              Marks.Callout.Distance = 10
              Marks.Callout.Length = 9
              Marks.Brush.Color = clWhite
              Marks.Brush.Style = bsClear
              Marks.Font.Height = -9
              Marks.Frame.Visible = False
              Marks.Style = smsValue
              Marks.Visible = True
              DataSource = QAndFattAZAS
              SeriesColor = clLime
              XLabelsSource = 'mese'
              BarStyle = bsCilinder
              BarWidthPercent = 25
              Gradient.Direction = gdTopBottom
              XValues.Name = 'X'
              XValues.Order = loAscending
              YValues.Name = 'Bar'
              YValues.ValueSource = 'numero'
            end
          end
          object Panel8: TPanel
            Left = 0
            Top = -1324
            Width = 547
            Height = 150
            Align = alTop
            Caption = 'Panel1'
            TabOrder = 9
            object DBChart13: TDBChart
              Left = 1
              Top = 1
              Width = 481
              Height = 148
              Gradient.EndColor = 12615680
              Gradient.Visible = True
              Title.Font.Color = clBlack
              Title.Font.Height = -12
              Title.Font.Style = [fsBold]
              Title.Text.Strings = (
                'Presentazioni Utente connesso (rosso anno attuale)')
              BottomAxis.LabelsFont.Height = -8
              LeftAxis.Visible = False
              Legend.Visible = False
              Align = alClient
              TabOrder = 0
              OnDblClick = DBChartCVInsClick
              object BarSeriesPresUCPerMese: TBarSeries
                BarPen.Style = psDot
                Marks.ArrowLength = 4
                Marks.Callout.Brush.Color = clBlack
                Marks.Callout.Distance = 3
                Marks.Callout.Length = 4
                Marks.Brush.Color = clWhite
                Marks.Brush.Style = bsClear
                Marks.Font.Color = clRed
                Marks.Font.Height = -9
                Marks.Frame.Visible = False
                Marks.Style = smsValue
                Marks.Visible = True
                DataSource = QNrPresPerMeseUC
                SeriesColor = clRed
                XLabelsSource = 'mese'
                BarStyle = bsCilinder
                BarWidthPercent = 25
                Gradient.Direction = gdTopBottom
                XValues.Name = 'X'
                XValues.Order = loAscending
                YValues.Name = 'Bar'
                YValues.ValueSource = 'numero'
              end
              object SeriesNrPresUCPerMeseAS: TBarSeries
                BarPen.Style = psDot
                Marks.ArrowLength = 9
                Marks.Callout.Brush.Color = clBlack
                Marks.Callout.Distance = 10
                Marks.Callout.Length = 9
                Marks.Brush.Color = clWhite
                Marks.Brush.Style = bsClear
                Marks.Frame.Visible = False
                Marks.Visible = True
                DataSource = QNrPresPerMeseUCAS
                SeriesColor = clLime
                XLabelsSource = 'mese'
                BarStyle = bsCilinder
                BarWidthPercent = 25
                Gradient.Direction = gdTopBottom
                XValues.Name = 'X'
                XValues.Order = loAscending
                YValues.Name = 'Bar'
                YValues.ValueSource = 'numero'
              end
            end
            object DBChart14: TDBChart
              Left = 482
              Top = 1
              Width = 64
              Height = 148
              Gradient.EndColor = 12615680
              Gradient.Visible = True
              Title.Font.Color = clBlack
              Title.Text.Strings = (
                'Totale per '
                'Anno')
              BottomAxis.LabelsAngle = 45
              BottomAxis.LabelsFont.Height = -8
              BottomAxis.MaximumOffset = 10
              BottomAxis.MinimumOffset = 10
              LeftAxis.Visible = False
              Legend.Visible = False
              Align = alRight
              TabOrder = 1
              object BarSeriesNrPresPerAnnoUC: TBarSeries
                BarPen.Style = psDot
                Depth = 5
                Marks.ArrowLength = 20
                Marks.Callout.Brush.Color = clBlack
                Marks.Callout.Distance = -15
                Marks.Callout.Length = 20
                Marks.Brush.Color = clWhite
                Marks.Brush.Style = bsClear
                Marks.Font.Height = -9
                Marks.Frame.Visible = False
                Marks.Style = smsValue
                Marks.Visible = True
                DataSource = QNrPresPerAnnoUC
                SeriesColor = clRed
                XLabelsSource = 'anno'
                BarStyle = bsCilinder
                BarWidthPercent = 25
                Gradient.Direction = gdTopBottom
                XValues.Name = 'X'
                XValues.Order = loAscending
                YValues.Name = 'Bar'
                YValues.ValueSource = 'numero'
              end
            end
          end
          object Panel9: TPanel
            Left = 0
            Top = -724
            Width = 547
            Height = 150
            Align = alTop
            Caption = 'Panel1'
            TabOrder = 10
            object DBChart15: TDBChart
              Left = 1
              Top = 1
              Width = 481
              Height = 148
              Gradient.EndColor = 12615680
              Gradient.Visible = True
              Title.Font.Color = clBlack
              Title.Font.Height = -12
              Title.Font.Style = [fsBold]
              Title.Text.Strings = (
                'Commesse Utente connesso'
                '')
              BottomAxis.LabelsFont.Height = -8
              LeftAxis.Visible = False
              Legend.Visible = False
              Align = alClient
              TabOrder = 0
              OnDblClick = DBChartCVInsClick
              PrintMargins = (
                15
                39
                15
                39)
              object BarSeriesCommUC: TBarSeries
                BarPen.Style = psDot
                Marks.ArrowLength = 4
                Marks.Callout.Brush.Color = clBlack
                Marks.Callout.Distance = 3
                Marks.Callout.Length = 4
                Marks.Brush.Color = clWhite
                Marks.Brush.Style = bsClear
                Marks.Font.Color = clRed
                Marks.Font.Height = -9
                Marks.Frame.Visible = False
                Marks.Style = smsValue
                Marks.Visible = True
                DataSource = QCommAperteUC
                SeriesColor = clRed
                XLabelsSource = 'mese'
                BarStyle = bsCilinder
                BarWidthPercent = 25
                Gradient.Direction = gdTopBottom
                XValues.Name = 'X'
                XValues.Order = loAscending
                YValues.Name = 'Bar'
                YValues.ValueSource = 'numero'
              end
              object Series1: TBarSeries
                BarPen.Style = psDot
                Marks.ArrowLength = 9
                Marks.Callout.Brush.Color = clBlack
                Marks.Callout.Distance = 10
                Marks.Callout.Length = 9
                Marks.Brush.Color = clWhite
                Marks.Brush.Style = bsClear
                Marks.Font.Height = -9
                Marks.Frame.Visible = False
                Marks.Style = smsValue
                Marks.Visible = True
                DataSource = QCommAperteASUC
                SeriesColor = clLime
                XLabelsSource = 'mese'
                BarStyle = bsCilinder
                BarWidthPercent = 25
                Gradient.Direction = gdTopBottom
                XValues.Name = 'X'
                XValues.Order = loAscending
                YValues.Name = 'Bar'
                YValues.ValueSource = 'numero'
              end
            end
            object DBChart16: TDBChart
              Left = 482
              Top = 1
              Width = 64
              Height = 148
              Gradient.EndColor = 12615680
              Gradient.Visible = True
              Title.Font.Color = clBlack
              Title.Text.Strings = (
                'Totale per '
                'Anno')
              BottomAxis.LabelsAngle = 45
              BottomAxis.LabelsFont.Height = -8
              BottomAxis.MaximumOffset = 10
              BottomAxis.MinimumOffset = 10
              LeftAxis.Visible = False
              Legend.Visible = False
              Align = alRight
              TabOrder = 1
              object BarSeriesCommApUCPerAnno: TBarSeries
                BarPen.Style = psDot
                Depth = 5
                Marks.ArrowLength = 20
                Marks.Callout.Brush.Color = clBlack
                Marks.Callout.Distance = -15
                Marks.Callout.Length = 20
                Marks.Brush.Color = clWhite
                Marks.Brush.Style = bsClear
                Marks.Font.Height = -9
                Marks.Frame.Visible = False
                Marks.Style = smsValue
                Marks.Visible = True
                DataSource = QCommAperteUCPerAnno
                SeriesColor = clRed
                XLabelsSource = 'anno'
                BarStyle = bsCilinder
                BarWidthPercent = 25
                Gradient.Direction = gdTopBottom
                XValues.Name = 'X'
                XValues.Order = loAscending
                YValues.Name = 'Bar'
                YValues.ValueSource = 'numero'
              end
            end
          end
          object Panel10: TPanel
            Left = 0
            Top = -1024
            Width = 547
            Height = 150
            Align = alTop
            Caption = 'Panel1'
            TabOrder = 11
            object DBChart17: TDBChart
              Left = 1
              Top = 1
              Width = 481
              Height = 148
              Gradient.EndColor = 12615680
              Gradient.Visible = True
              Title.Font.Color = clBlack
              Title.Font.Height = -12
              Title.Font.Style = [fsBold]
              Title.Text.Strings = (
                'Colloqui Utente connesso (rosso anno attuale)')
              BottomAxis.LabelsFont.Height = -8
              LeftAxis.Visible = False
              Legend.Visible = False
              Align = alClient
              TabOrder = 0
              OnDblClick = DBChartCVInsClick
              object BarSeriesNrCollUC: TBarSeries
                BarPen.Style = psDot
                Marks.ArrowLength = 4
                Marks.Callout.Brush.Color = clBlack
                Marks.Callout.Distance = 3
                Marks.Callout.Length = 4
                Marks.Brush.Color = clWhite
                Marks.Brush.Style = bsClear
                Marks.Font.Color = clRed
                Marks.Font.Height = -9
                Marks.Frame.Visible = False
                Marks.Style = smsValue
                Marks.Visible = True
                DataSource = QNrCollUC
                SeriesColor = clRed
                XLabelsSource = 'mese'
                BarStyle = bsCilinder
                BarWidthPercent = 25
                Gradient.Direction = gdTopBottom
                XValues.Name = 'X'
                XValues.Order = loAscending
                YValues.Name = 'Bar'
                YValues.ValueSource = 'numero'
              end
              object BarSeriesNrCollUCAS: TBarSeries
                BarPen.Style = psDot
                Marks.ArrowLength = 9
                Marks.Callout.Brush.Color = clBlack
                Marks.Callout.Distance = 10
                Marks.Callout.Length = 9
                Marks.Brush.Color = clWhite
                Marks.Brush.Style = bsClear
                Marks.Font.Height = -9
                Marks.Frame.Visible = False
                Marks.Style = smsValue
                Marks.Visible = True
                DataSource = QNrCollUCAS
                SeriesColor = clLime
                XLabelsSource = 'mese'
                BarStyle = bsCilinder
                BarWidthPercent = 25
                Gradient.Direction = gdTopBottom
                XValues.Name = 'X'
                XValues.Order = loAscending
                YValues.Name = 'Bar'
                YValues.ValueSource = 'numero'
              end
            end
            object DBChart18: TDBChart
              Left = 482
              Top = 1
              Width = 64
              Height = 148
              Gradient.EndColor = 12615680
              Gradient.Visible = True
              Title.Font.Color = clBlack
              Title.Text.Strings = (
                'Totale per '
                'Anno')
              BottomAxis.LabelsAngle = 45
              BottomAxis.LabelsFont.Height = -8
              BottomAxis.MaximumOffset = 10
              BottomAxis.MinimumOffset = 10
              LeftAxis.Visible = False
              Legend.Visible = False
              Align = alRight
              TabOrder = 1
              object SeriesCollUCPerAnno: TBarSeries
                BarPen.Style = psDot
                Marks.ArrowLength = 5
                Marks.Callout.Brush.Color = clBlack
                Marks.Callout.Length = 5
                Marks.Brush.Color = clWhite
                Marks.Brush.Style = bsClear
                Marks.Frame.Visible = False
                Marks.Style = smsValue
                Marks.Visible = True
                DataSource = QNrCollUCPerAnno
                SeriesColor = clRed
                XLabelsSource = 'anno'
                BarStyle = bsCilinder
                BarWidthPercent = 25
                Gradient.Direction = gdTopBottom
                XValues.Name = 'X'
                XValues.Order = loAscending
                YValues.Name = 'Bar'
                YValues.ValueSource = 'numero'
              end
            end
          end
          object Panel7: TPanel
            Left = 0
            Top = -1174
            Width = 547
            Height = 150
            Align = alTop
            Caption = 'Panel1'
            TabOrder = 12
            object DBChartPres: TDBChart
              Left = 1
              Top = 1
              Width = 481
              Height = 148
              Gradient.EndColor = 12615680
              Gradient.Visible = True
              Title.Font.Color = clBlack
              Title.Font.Height = -12
              Title.Font.Style = [fsBold]
              Title.Text.Strings = (
                'Presentazioni Azienda (rosso anno attuale)')
              BottomAxis.LabelsFont.Height = -8
              LeftAxis.Visible = False
              Legend.Visible = False
              Align = alClient
              TabOrder = 0
              OnDblClick = DBChartCVInsClick
              object BarSeriesPresPerMese: TBarSeries
                BarPen.Style = psDot
                Marks.ArrowLength = 4
                Marks.Callout.Brush.Color = clBlack
                Marks.Callout.Distance = 3
                Marks.Callout.Length = 4
                Marks.Brush.Color = clWhite
                Marks.Brush.Style = bsClear
                Marks.Font.Color = clRed
                Marks.Font.Height = -9
                Marks.Frame.Visible = False
                Marks.Style = smsValue
                Marks.Visible = True
                DataSource = QNrPresPerMese
                SeriesColor = clRed
                XLabelsSource = 'mese'
                BarStyle = bsCilinder
                BarWidthPercent = 25
                Gradient.Direction = gdTopBottom
                XValues.Name = 'X'
                XValues.Order = loAscending
                YValues.Name = 'Bar'
                YValues.ValueSource = 'numero'
              end
              object BarSeriesPresPerMeseAS: TBarSeries
                BarPen.Style = psDot
                Marks.ArrowLength = 9
                Marks.Callout.Brush.Color = clBlack
                Marks.Callout.Distance = 10
                Marks.Callout.Length = 9
                Marks.Brush.Color = clWhite
                Marks.Brush.Style = bsClear
                Marks.Font.Height = -9
                Marks.Frame.Visible = False
                Marks.Style = smsValue
                Marks.Visible = True
                DataSource = QNrPresPerMeseAS
                SeriesColor = clLime
                XLabelsSource = 'mese'
                BarStyle = bsCilinder
                BarWidthPercent = 25
                Gradient.Direction = gdTopBottom
                XValues.Name = 'X'
                XValues.Order = loAscending
                YValues.Name = 'Bar'
                YValues.ValueSource = 'numero'
              end
            end
            object DBChart12: TDBChart
              Left = 482
              Top = 1
              Width = 64
              Height = 148
              Gradient.EndColor = 12615680
              Gradient.Visible = True
              Title.Font.Color = clBlack
              Title.Text.Strings = (
                'Totale per '
                'Anno')
              BottomAxis.LabelsAngle = 45
              BottomAxis.LabelsFont.Height = -8
              BottomAxis.MaximumOffset = 10
              BottomAxis.MinimumOffset = 10
              LeftAxis.Visible = False
              Legend.Visible = False
              Align = alRight
              TabOrder = 1
              object BarSeriesNrPresPerAnnoAz: TBarSeries
                BarPen.Style = psDot
                Depth = 5
                Marks.ArrowLength = 20
                Marks.Callout.Brush.Color = clBlack
                Marks.Callout.Distance = -15
                Marks.Callout.Length = 20
                Marks.Brush.Color = clWhite
                Marks.Brush.Style = bsClear
                Marks.Font.Height = -9
                Marks.Frame.Visible = False
                Marks.Style = smsValue
                Marks.Visible = True
                DataSource = QNrPresPerAnno
                SeriesColor = clRed
                XLabelsSource = 'anno'
                BarStyle = bsCilinder
                BarWidthPercent = 25
                Gradient.Direction = gdTopBottom
                XValues.Name = 'X'
                XValues.Order = loAscending
                YValues.Name = 'Bar'
                YValues.ValueSource = 'numero'
              end
            end
          end
          object Panel11: TPanel
            Left = 0
            Top = -874
            Width = 547
            Height = 150
            Align = alTop
            Caption = 'Panel1'
            TabOrder = 13
            object DBChart19: TDBChart
              Left = 1
              Top = 1
              Width = 481
              Height = 148
              Gradient.EndColor = 12615680
              Gradient.Visible = True
              Title.Font.Color = clBlack
              Title.Font.Height = -12
              Title.Font.Style = [fsBold]
              Title.Text.Strings = (
                'Colloqui Azienda'
                '')
              BottomAxis.LabelsFont.Height = -8
              LeftAxis.Visible = False
              Legend.Visible = False
              Align = alClient
              TabOrder = 0
              OnDblClick = DBChartCVInsClick
              object BarSeriesCollAz: TBarSeries
                BarPen.Style = psDot
                Marks.ArrowLength = 4
                Marks.Callout.Brush.Color = clBlack
                Marks.Callout.Distance = 3
                Marks.Callout.Length = 4
                Marks.Brush.Color = clWhite
                Marks.Brush.Style = bsClear
                Marks.Font.Color = clRed
                Marks.Font.Height = -9
                Marks.Frame.Visible = False
                Marks.Style = smsValue
                Marks.Visible = True
                DataSource = QNrColloquiAz
                SeriesColor = clRed
                XLabelsSource = 'mese'
                BarStyle = bsCilinder
                BarWidthPercent = 25
                Gradient.Direction = gdTopBottom
                XValues.Name = 'X'
                XValues.Order = loAscending
                YValues.Name = 'Bar'
                YValues.ValueSource = 'numero'
              end
              object BarSeriesCollAzAS: TBarSeries
                BarPen.Style = psDot
                Marks.ArrowLength = 9
                Marks.Callout.Brush.Color = clBlack
                Marks.Callout.Distance = 10
                Marks.Callout.Length = 9
                Marks.Brush.Color = clWhite
                Marks.Brush.Style = bsClear
                Marks.Frame.Visible = False
                Marks.Style = smsValue
                Marks.Visible = True
                DataSource = QNrColloquiAzAS
                SeriesColor = clLime
                XLabelsSource = 'mese'
                BarStyle = bsCilinder
                BarWidthPercent = 25
                Gradient.Direction = gdTopBottom
                XValues.Name = 'X'
                XValues.Order = loAscending
                YValues.Name = 'Bar'
                YValues.ValueSource = 'numero'
              end
            end
            object DBChart20: TDBChart
              Left = 482
              Top = 1
              Width = 64
              Height = 148
              Gradient.EndColor = 12615680
              Gradient.Visible = True
              Title.Font.Color = clBlack
              Title.Text.Strings = (
                'Totale per '
                'Anno')
              BottomAxis.LabelsAngle = 45
              BottomAxis.LabelsFont.Height = -8
              BottomAxis.MaximumOffset = 10
              BottomAxis.MinimumOffset = 10
              LeftAxis.Visible = False
              Legend.Visible = False
              Align = alRight
              TabOrder = 1
              object BarSeriesNrColloquiAzPerAnno: TBarSeries
                BarPen.Style = psDot
                Depth = 5
                Marks.ArrowLength = 20
                Marks.Callout.Brush.Color = clBlack
                Marks.Callout.Distance = -15
                Marks.Callout.Length = 20
                Marks.Brush.Color = clWhite
                Marks.Brush.Style = bsClear
                Marks.Font.Height = -9
                Marks.Frame.Visible = False
                Marks.Style = smsValue
                Marks.Visible = True
                DataSource = QNrColloquiAzPerAnno
                SeriesColor = clRed
                XLabelsSource = 'anno'
                BarStyle = bsCilinder
                BarWidthPercent = 25
                Gradient.Direction = gdTopBottom
                XValues.Name = 'X'
                XValues.Order = loAscending
                YValues.Name = 'Bar'
                YValues.ValueSource = 'numero'
              end
            end
          end
          object Panel12: TPanel
            Left = 0
            Top = -574
            Width = 547
            Height = 150
            Align = alTop
            Caption = 'Panel1'
            TabOrder = 14
            object DBChart21: TDBChart
              Left = 1
              Top = 1
              Width = 481
              Height = 148
              Gradient.EndColor = 12615680
              Gradient.Visible = True
              Title.Font.Color = clBlack
              Title.Font.Height = -12
              Title.Font.Style = [fsBold]
              Title.Text.Strings = (
                'Commesse Azienda')
              BottomAxis.LabelsFont.Height = -8
              LeftAxis.Visible = False
              Legend.Visible = False
              Align = alClient
              TabOrder = 0
              OnDblClick = DBChartCVInsClick
              object BarSeriesCommAz: TBarSeries
                BarPen.Style = psDot
                Marks.ArrowLength = 4
                Marks.Callout.Brush.Color = clBlack
                Marks.Callout.Distance = 3
                Marks.Callout.Length = 4
                Marks.Brush.Color = clWhite
                Marks.Brush.Style = bsClear
                Marks.Font.Color = clRed
                Marks.Font.Height = -9
                Marks.Frame.Visible = False
                Marks.Style = smsValue
                Marks.Visible = True
                DataSource = QCommAperteAz
                SeriesColor = clRed
                XLabelsSource = 'mese'
                BarStyle = bsCilinder
                BarWidthPercent = 25
                Gradient.Direction = gdTopBottom
                XValues.Name = 'X'
                XValues.Order = loAscending
                YValues.Name = 'Bar'
                YValues.ValueSource = 'numero'
              end
              object BarSeriesCommAperteAzAS: TBarSeries
                BarPen.Style = psDot
                Marks.ArrowLength = 9
                Marks.Callout.Brush.Color = clBlack
                Marks.Callout.Distance = 10
                Marks.Callout.Length = 9
                Marks.Brush.Color = clWhite
                Marks.Brush.Style = bsClear
                Marks.Frame.Visible = False
                Marks.Style = smsValue
                Marks.Visible = True
                DataSource = QCommAperteAZAS
                SeriesColor = clLime
                XLabelsSource = 'mese'
                BarStyle = bsCilinder
                BarWidthPercent = 25
                Gradient.Direction = gdTopBottom
                XValues.Name = 'X'
                XValues.Order = loAscending
                YValues.Name = 'Bar'
                YValues.ValueSource = 'numero'
              end
            end
            object DBChart22: TDBChart
              Left = 482
              Top = 1
              Width = 64
              Height = 148
              Gradient.EndColor = 12615680
              Gradient.Visible = True
              Title.Font.Color = clBlack
              Title.Text.Strings = (
                'Totale per '
                'Anno')
              BottomAxis.LabelsAngle = 45
              BottomAxis.LabelsFont.Height = -8
              BottomAxis.MaximumOffset = 10
              BottomAxis.MinimumOffset = 10
              LeftAxis.Visible = False
              Legend.Visible = False
              Align = alRight
              TabOrder = 1
              object BarSeries35: TBarSeries
                BarPen.Style = psDot
                Depth = 5
                Marks.ArrowLength = 20
                Marks.Callout.Brush.Color = clBlack
                Marks.Callout.Distance = -15
                Marks.Callout.Length = 20
                Marks.Brush.Color = clWhite
                Marks.Brush.Style = bsClear
                Marks.Font.Height = -9
                Marks.Frame.Visible = False
                Marks.Style = smsValue
                Marks.Visible = True
                DataSource = QCommAperteAzPerAnno
                SeriesColor = clRed
                XLabelsSource = 'anno'
                BarStyle = bsCilinder
                BarWidthPercent = 25
                Gradient.Direction = gdTopBottom
                XValues.Name = 'X'
                XValues.Order = loAscending
                YValues.Name = 'Bar'
                YValues.ValueSource = 'numero'
              end
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Analisi Utenti'
        ImageIndex = 1
        ParentShowHint = False
        ShowHint = False
        object Panel2: TPanel
          Left = 0
          Top = 0
          Width = 567
          Height = 81
          Align = alTop
          TabOrder = 0
          object Label1: TLabel
            Left = 11
            Top = 6
            Width = 32
            Height = 13
            Caption = 'Utente'
          end
          object Label2: TLabel
            Left = 11
            Top = 30
            Width = 16
            Height = 13
            Caption = 'Dal'
          end
          object Label3: TLabel
            Left = 177
            Top = 30
            Width = 9
            Height = 13
            Caption = 'Al'
          end
          object Label4: TLabel
            Left = 272
            Top = 48
            Width = 267
            Height = 26
            Caption = 
              'La somma delle singole attivit� sono riferite ai singoli mesi'#13#10'd' +
              'el periodo selezionato. Non si discriminano gli anni!'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -5
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object DBLookupComboBox1: TDBLookupComboBox
            Left = 56
            Top = 3
            Width = 145
            Height = 21
            KeyField = 'id'
            ListField = 'nominativo'
            ListSource = DSQUtenti
            TabOrder = 0
          end
          object Button2: TButton
            Left = 8
            Top = 48
            Width = 75
            Height = 22
            Caption = 'Aggiorna'
            TabOrder = 1
            OnClick = Button2Click
          end
          object DTDal: TDateTimePicker
            Left = 55
            Top = 27
            Width = 112
            Height = 19
            CalAlignment = dtaLeft
            Date = 38476.5438004745
            Time = 38476.5438004745
            DateFormat = dfShort
            DateMode = dmComboBox
            Kind = dtkDate
            ParseInput = False
            TabOrder = 2
          end
          object DTAl: TDateTimePicker
            Left = 200
            Top = 27
            Width = 113
            Height = 19
            CalAlignment = dtaLeft
            Date = 38476.5447370023
            Time = 38476.5447370023
            DateFormat = dfShort
            DateMode = dmComboBox
            Kind = dtkDate
            ParseInput = False
            TabOrder = 3
          end
        end
        object Panel5: TPanel
          Left = 0
          Top = 81
          Width = 567
          Height = 392
          Align = alClient
          TabOrder = 1
          object dataGrid: TAdvStringGrid
            Left = 1
            Top = 1
            Width = 565
            Height = 390
            Cursor = crDefault
            Align = alClient
            ColCount = 13
            DefaultRowHeight = 21
            FixedCols = 0
            RowCount = 2
            PopupMenu = PopupMenu1
            ScrollBars = ssBoth
            TabOrder = 0
            HintColor = clYellow
            ActiveCellFont.Charset = DEFAULT_CHARSET
            ActiveCellFont.Color = clWindowText
            ActiveCellFont.Height = -11
            ActiveCellFont.Name = 'Tahoma'
            ActiveCellFont.Style = [fsBold]
            CellNode.NodeType = cnFlat
            ColumnSize.Stretch = True
            ControlLook.DropDownHeader.Font.Charset = DEFAULT_CHARSET
            ControlLook.DropDownHeader.Font.Color = clWindowText
            ControlLook.DropDownHeader.Font.Height = -11
            ControlLook.DropDownHeader.Font.Name = 'Tahoma'
            ControlLook.DropDownHeader.Font.Style = []
            ControlLook.DropDownHeader.Visible = True
            ControlLook.DropDownHeader.Buttons = <>
            ControlLook.DropDownFooter.Font.Charset = DEFAULT_CHARSET
            ControlLook.DropDownFooter.Font.Color = clWindowText
            ControlLook.DropDownFooter.Font.Height = -11
            ControlLook.DropDownFooter.Font.Name = 'MS Sans Serif'
            ControlLook.DropDownFooter.Font.Style = []
            ControlLook.DropDownFooter.Visible = True
            ControlLook.DropDownFooter.Buttons = <>
            EnhRowColMove = False
            Filter = <>
            FilterDropDown.Font.Charset = DEFAULT_CHARSET
            FilterDropDown.Font.Color = clWindowText
            FilterDropDown.Font.Height = -11
            FilterDropDown.Font.Name = 'MS Sans Serif'
            FilterDropDown.Font.Style = []
            FilterDropDownClear = '(All)'
            FixedFont.Charset = DEFAULT_CHARSET
            FixedFont.Color = clWindowText
            FixedFont.Height = -11
            FixedFont.Name = 'MS Sans Serif'
            FixedFont.Style = []
            FloatFormat = '%.2f'
            PrintSettings.DateFormat = 'dd/mm/yyyy'
            PrintSettings.Font.Charset = DEFAULT_CHARSET
            PrintSettings.Font.Color = clWindowText
            PrintSettings.Font.Height = -11
            PrintSettings.Font.Name = 'MS Sans Serif'
            PrintSettings.Font.Style = []
            PrintSettings.FixedFont.Charset = DEFAULT_CHARSET
            PrintSettings.FixedFont.Color = clWindowText
            PrintSettings.FixedFont.Height = -11
            PrintSettings.FixedFont.Name = 'MS Sans Serif'
            PrintSettings.FixedFont.Style = []
            PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
            PrintSettings.HeaderFont.Color = clWindowText
            PrintSettings.HeaderFont.Height = -11
            PrintSettings.HeaderFont.Name = 'MS Sans Serif'
            PrintSettings.HeaderFont.Style = []
            PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
            PrintSettings.FooterFont.Color = clWindowText
            PrintSettings.FooterFont.Height = -11
            PrintSettings.FooterFont.Name = 'MS Sans Serif'
            PrintSettings.FooterFont.Style = []
            PrintSettings.Borders = pbNoborder
            PrintSettings.Centered = False
            PrintSettings.PageNumSep = '/'
            ScrollWidth = 16
            SearchFooter.FindNextCaption = 'Find &next'
            SearchFooter.FindPrevCaption = 'Find &previous'
            SearchFooter.Font.Charset = DEFAULT_CHARSET
            SearchFooter.Font.Color = clWindowText
            SearchFooter.Font.Height = -11
            SearchFooter.Font.Name = 'MS Sans Serif'
            SearchFooter.Font.Style = []
            SearchFooter.HighLightCaption = 'Highlight'
            SearchFooter.HintClose = 'Close'
            SearchFooter.HintFindNext = 'Find next occurence'
            SearchFooter.HintFindPrev = 'Find previous occurence'
            SearchFooter.HintHighlight = 'Highlight occurences'
            SearchFooter.MatchCaseCaption = 'Match case'
            SelectionColor = clHighlight
            SelectionTextColor = clHighlightText
            Version = '5.0.0.3'
            WordWrap = False
            ColWidths = (
              64
              64
              64
              64
              64
              64
              64
              64
              64
              64
              64
              64
              224)
            RowHeights = (
              21
              21)
          end
        end
      end
    end
  end
  object QCVInseriti: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select SUM  (numero) numero,'
      'mese,'
      'anno,'
      'nrmese'
      'from '
      '(select count (id) numero,'
      'datename (mm,cvinseritoindata) mese,'
      'YEAR (cvinseritoindata) anno,'
      'MONTH (cvinseritoindata) nrmese'
      'from anagrafica'
      'where YEAR (cvinseritoindata) = YEAR (getdate())'
      'group by cvinseritoindata) aa'
      'group by mese,nrmese,anno'
      'order by anno,nrmese')
    Left = 16
    Top = 96
    object QCVInseritinumero: TIntegerField
      FieldName = 'numero'
      ReadOnly = True
    end
    object QCVInseritimese: TWideStringField
      FieldName = 'mese'
      Size = 30
    end
  end
  object QAzIns: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select SUM  (numero) numero,'
      'mese,'
      'anno,'
      'nrmese'
      'from'
      '(select count (id) numero,'
      ' datename (mm,conosciutoindata) mese,'
      'YEAR (conosciutoindata) anno,'
      'MONTH (conosciutoindata) nrmese'
      'from ebc_clienti'
      'where YEAR (conosciutoindata) = YEAR (getdate())'
      'group by conosciutoindata) aa'
      'group by mese,nrmese,anno'
      'order by anno,nrmese'
      '')
    Left = 56
    Top = 96
  end
  object ClientiAtt: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select sum (numero) numero,'
      ' mese,'
      ' nrmese,'
      ' anno'
      'from '
      '(select count (id) numero,'
      '  datename (mm,conosciutoindata) mese,'
      '  YEAR (conosciutoindata) anno,'
      ' MONTH (conosciutoindata) nrmese'
      'from ebc_clienti'
      'where idtipoazienda=1'
      'group by conosciutoindata'
      'having YEAR (conosciutoindata) = YEAR (getdate())) aa'
      'group by mese,nrmese,anno'
      'order by anno,nrmese')
    Left = 96
    Top = 96
  end
  object FatturatoxAnno: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select sum (ecr.importo) numero,'
      '  YEAR (data) anno'
      'from fatture f join ebc_compensiricerche ecr'
      '  on ecr.idfattura=f.id'
      'group by YEAR (data)'
      'order by YEAR (data)')
    Left = 129
    Top = 96
    object Fatturatoanno: TIntegerField
      FieldName = 'anno'
    end
    object Fatturatonumero: TFloatField
      FieldName = 'numero'
      ReadOnly = True
    end
  end
  object NrFatturexAnno: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select count (progressivo) numero,'
      '  YEAR (data) anno'
      'from fatture'
      'group by YEAR (data)'
      'order by YEAR(data)')
    Left = 128
    Top = 124
    object NrFatturenumero: TIntegerField
      FieldName = 'numero'
      ReadOnly = True
    end
    object NrFattureanno: TIntegerField
      FieldName = 'anno'
    end
  end
  object QCVTot: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select count (id) tot from anagrafica')
    Left = 16
    Top = 180
    object QCVTottot: TIntegerField
      DefaultExpression = '0'
      FieldName = 'tot'
      ReadOnly = True
    end
  end
  object QAzTot: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select count (id) tot'
      'from ebc_Clienti')
    Left = 56
    Top = 180
    object QAzTottot: TIntegerField
      DefaultExpression = '0'
      FieldName = 'tot'
      ReadOnly = True
    end
  end
  object QAzAttTot: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select count (id) tot'
      'from ebc_Clienti'
      'where idtipoazienda=1')
    Left = 95
    Top = 180
    object QAzAttTottot: TIntegerField
      DefaultExpression = '0'
      FieldName = 'tot'
      ReadOnly = True
    end
  end
  object QFattAnnoAttuale: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select sum (ecr.importo) tot'
      'from fatture f join ebc_compensiricerche ecr'
      '  on ecr.idfattura=f.id'
      'where year (data)=year(GETDATE())')
    Left = 576
    Top = 16
    object QFatttot: TFloatField
      DefaultExpression = '0'
      FieldName = 'tot'
      ReadOnly = True
      EditFormat = '###0,00'
    end
  end
  object QNrFatt: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select count (progressivo) tot'
      'from fatture'
      'where year (data)=year(GETDATE())')
    Left = 608
    Top = 16
    object QNrFatttot: TIntegerField
      DefaultExpression = '0'
      FieldName = 'tot'
      ReadOnly = True
    end
  end
  object DSCVTot: TDataSource
    DataSet = QCVTot
    Left = 16
    Top = 208
  end
  object DSQAzTot: TDataSource
    DataSet = QAzTot
    Left = 56
    Top = 208
  end
  object DSQAzAttTot: TDataSource
    DataSet = QAzAttTot
    Left = 95
    Top = 208
  end
  object DSFatt: TDataSource
    DataSet = QFattAnnoAttuale
    Left = 576
    Top = 72
  end
  object DSQNrFatt: TDataSource
    DataSet = QNrFatt
    Left = 608
    Top = 72
  end
  object QFattAnnoScorso: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select sum (ecr.importo) tot'
      'from fatture f join ebc_compensiricerche ecr'
      '  on ecr.idfattura = f.id'
      'where data between'
      #39'01/01/'#39'+ CAST (YEAR (GETDATE())-1 AS VARCHAR) and'
      
        'CAST (DAY (GETDATE()) AS VARCHAR)+'#39'/'#39'+CAST (MONTH (GETDATE()) AS' +
        ' VARCHAR)+'#39'/'#39'+ CAST (YEAR (GETDATE())-1 AS VARCHAR)'
      '')
    Left = 576
    Top = 44
    object QFattAnnoScorsotot: TFloatField
      FieldName = 'tot'
      ReadOnly = True
    end
  end
  object DSFattAS: TDataSource
    DataSet = QFattAnnoScorso
    Left = 576
    Top = 100
  end
  object QNRFattAS: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select count (progressivo) tot'
      'from fatture'
      'where (year (data)=year(GETDATE())-1)'
      'and (MONTH(data)<=MONTH(GETDATE()))'
      'and (DAY(data)<=DAY(GETDATE()))')
    Left = 608
    Top = 44
    object QNRFattAStot: TIntegerField
      FieldName = 'tot'
      ReadOnly = True
    end
  end
  object DSNrFattAS: TDataSource
    DataSet = QNRFattAS
    Left = 608
    Top = 100
  end
  object QNrCollUltimoMese: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select count(id) tot'
      'from ebc_Colloqui'
      'where datediff (dd,data,getdate()) between 0 and 30')
    Left = 421
    Top = 180
  end
  object DSNrColl: TDataSource
    DataSet = QNrCollUltimoMese
    Left = 421
    Top = 236
  end
  object QNrCollUltimoMeseAS: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select count(id) tot'
      'from ebc_Colloqui'
      'where datediff (dd,data,'
      'CAST (DAY (getdate()) AS VARCHAR) + '#39'/'#39'+'
      '  CAST (MONTH (getdate()) AS VARCHAR) + '#39'/'#39'+'
      '  CAST (YEAR (getdate())-1 AS VARCHAR))between 0 and 30')
    Left = 421
    Top = 208
  end
  object DSQNrCollAS: TDataSource
    DataSet = QNrCollUltimoMeseAS
    Left = 421
    Top = 264
  end
  object QNrPresPerMese: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select count(distinct idanagrafica) numero,'
      'DATENAME (mm,dataevento) mese'
      'from storico'
      'where ((idevento=76) or (idevento=83) or (idevento=20))'
      'and YEAR (dataevento) = YEAR (getdate())'
      'group by DATENAME (mm,dataevento),MONTH(dataevento)'
      'order by  MONTH (dataevento)')
    Left = 376
    Top = 96
  end
  object DSNrPres: TDataSource
    DataSet = QNrPresUltimoMese
    Left = 376
    Top = 236
  end
  object QNrPresPerMeseAS: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select count(distinct idanagrafica) numero,'
      'DATENAME (mm,dataevento) mese'
      'from storico'
      'where ((idevento=76) or (idevento=83) or (idevento=20))'
      'and YEAR (dataevento) = YEAR (getdate())-1'
      'group by DATENAME (mm,dataevento),MONTH(dataevento)'
      'order by  MONTH (dataevento)')
    Left = 376
    Top = 124
    object QNrPresPerMeseASnumero: TIntegerField
      FieldName = 'numero'
      ReadOnly = True
    end
    object QNrPresPerMeseASmese: TWideStringField
      FieldName = 'mese'
      Size = 30
    end
  end
  object DSNrPressAS: TDataSource
    DataSet = QNrPresUltimoMeseAS
    Left = 376
    Top = 264
  end
  object QNROff: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select count(id) tot'
      'from ebc_offerte'
      'where datediff (dd,data,getdate()) between 0 and 30 '
      'group by YEAR (data)')
    Left = 336
    Top = 96
    object IntegerField4: TIntegerField
      FieldName = 'tot'
      ReadOnly = True
    end
  end
  object DSNROff: TDataSource
    DataSet = QNROff
    Left = 336
    Top = 152
  end
  object DSNrOffAS: TDataSource
    DataSet = QNrOffAS
    Left = 336
    Top = 180
  end
  object QNrOffAS: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select count(id) tot'
      'from ebc_offerte'
      
        'where datediff (dd,data,CAST (DAY (getdate()) AS VARCHAR) + '#39'/'#39'+' +
        '  CAST (MONTH (getdate()) AS VARCHAR) + '#39'/'#39'+CAST (YEAR (getdate(' +
        '))-1 AS VARCHAR)) between 0 and 30 '
      'and YEAR (data)= YEAR(GETDATE())-1')
    Left = 336
    Top = 124
    object IntegerField5: TIntegerField
      FieldName = 'tot'
      ReadOnly = True
    end
  end
  object ClientiATTAS: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select SUM (numero) numero,'
      'mese,'
      'anno,'
      'nrmese'
      'from '
      '(select count (id) numero,'
      '  datename (mm,conosciutoindata) mese,'
      '  YEAR (conosciutoindata) anno,'
      '  MONTH (conosciutoindata) nrmese'
      'from ebc_clienti'
      'where idtipoazienda=1'
      'group by conosciutoindata'
      'having YEAR (conosciutoindata) = YEAR (getdate())-1) aa'
      'group by mese,nrmese,anno'
      'order by anno,nrmese')
    Left = 95
    Top = 124
  end
  object QCVInseritiAS: TADOQuery
    AutoCalcFields = False
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select SUM  (numero) numero,'
      'mese,'
      'anno,'
      'nrmese'
      'from '
      '(select count (id) numero,'
      'datename (mm,cvinseritoindata) mese,'
      'YEAR (cvinseritoindata) anno,'
      'MONTH (cvinseritoindata) nrmese'
      'from anagrafica'
      'where YEAR (cvinseritoindata) = YEAR (getdate())-1'
      'group by cvinseritoindata) aa'
      'group by mese,nrmese,anno'
      'order by anno,nrmese')
    Left = 16
    Top = 124
    object IntegerField6: TIntegerField
      FieldName = 'numero'
      ReadOnly = True
    end
    object WideStringField1: TWideStringField
      FieldName = 'mese'
      Size = 30
    end
  end
  object QAzInsAS: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select SUM  (numero) numero,'
      'mese,'
      'anno,'
      'nrmese'
      'from'
      '(select count (id) numero,'
      ' datename (mm,conosciutoindata) mese,'
      'YEAR (conosciutoindata) anno,'
      'MONTH (conosciutoindata) nrmese'
      'from ebc_clienti'
      'where YEAR (conosciutoindata) = YEAR (getdate())-1'
      'group by conosciutoindata) aa'
      'group by mese,nrmese,anno'
      'order by anno,nrmese'
      '')
    Left = 56
    Top = 124
  end
  object QAndFattSingoloUt: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'idUtente'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      'select SUM(numero) numero,'
      'anno,'
      'nrmese,'
      'mese'
      'from '
      '(select sum(ecr.importo) numero,'
      '  MONTH (data) nrmese,'
      '  YEAR (data) anno,'
      '  DATENAME (mm,data) mese'
      'from '
      '  fatture f join ebc_compensiricerche ecr'
      '    on f.id =ecr.idfattura'
      '  join ebc_ricerche er'
      '    on er.id=ecr.idricerca'
      'where er.idutente=:idUtente'
      'group by  data'
      'having YEAR (data) = YEAR (getdate())) aa'
      'group by nrmese,mese,anno'
      'order by anno,nrmese')
    Left = 193
    Top = 95
  end
  object QAndFattSingoloUtAS: TADOQuery
    Connection = Data.DB
    Parameters = <
      item
        Name = 'idUtente'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      'select SUM(numero) numero,'
      'anno,'
      'nrmese,'
      'mese'
      'from '
      '(select sum(ecr.importo) numero,'
      '  MONTH (data) nrmese,'
      '  YEAR (data) anno,'
      '  DATENAME (mm,data) mese'
      'from fatture f  join ebc_compensiricerche ecr'
      '    on f.id =ecr.idfattura'
      '  join ebc_ricerche er'
      '    on er.id=ecr.idricerca'
      'where er.idutente=:idUtente'
      'group by  data'
      'having data is not null'
      'and YEAR (data) = YEAR (getdate())-1) aa'
      'group by nrmese,mese,anno'
      'order by anno,nrmese')
    Left = 193
    Top = 123
  end
  object QAndFattPUtenteCon: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'xidUtente'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select sum(ecr.importo) numero,'
      '  MONTH (dataprevfatt) nrmese,'
      '  YEAR (dataprevfatt) anno,'
      '  DATENAME (mm,dataprevfatt) mese'
      'from '
      '  ebc_compensiricerche ecr join ebc_ricerche er'
      '    on er.id=ecr.idricerca'
      'where er.idutente=:xidUtente'
      'and datediff (mm,getdate(),dataprevfatt)between -2 and 11'
      'group by    MONTH (dataprevfatt),'
      '    DATENAME (mm,dataprevfatt),'
      '  YEAR (dataprevfatt) '
      'order by anno,nrmese')
    Left = 227
    Top = 95
  end
  object QAndFattPrevistoTot: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select sum(ecr.importo) numero,'
      '  MONTH (dataprevfatt) nrmese,'
      '  DATENAME (mm,dataprevfatt) mese,'
      '  YEAR (dataprevfatt) anno'
      'from '
      '  ebc_compensiricerche ecr join ebc_ricerche er'
      '    on er.id=ecr.idricerca'
      'where datediff (mm,getdate(),dataprevfatt)between -2  and 11'
      
        'group by  MONTH (dataprevfatt),DATENAME (mm,dataprevfatt), YEAR ' +
        '(dataprevfatt)'
      'order by anno,nrmese')
    Left = 259
    Top = 95
  end
  object QTotCVPerAnno: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select count(id) numero,'
      'YEAR (cvinseritoindata) anno'
      'from anagrafica'
      'where datediff (yy,cvinseritoindata,getdate()) <=1'
      'group by YEAR (cvinseritoindata)')
    Left = 16
    Top = 152
  end
  object QTotClientiPerAnno: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select count(id) numero,'
      'YEAR (conosciutoindata) anno'
      'from ebc_clienti'
      'where datediff (yy,conosciutoindata,getdate()) <=1'
      'group by YEAR (conosciutoindata)')
    Left = 56
    Top = 152
  end
  object QTotCliAttPerAnno: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select count(id) numero,'
      'YEAR (conosciutoindata) anno'
      'from ebc_clienti'
      'where datediff (yy,conosciutoindata,getdate()) <=1'
      'and idtipoazienda=1'
      'group by YEAR (conosciutoindata)')
    Left = 95
    Top = 152
  end
  object QNrPresPerAnno: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select count(distinct idanagrafica) numero,'
      'YEAR (dataevento) anno'
      'from storico'
      'where ((idevento=76) or (idevento=83) or (idevento=20))'
      'and DATEDIFF (yy,dataevento,getdate()) <=1 '
      'group by YEAR (dataevento)'
      'order by  YEAR (dataevento)')
    Left = 376
    Top = 152
  end
  object QNrPresPerMeseUC: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'idutente'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      'select count(distinct idanagrafica) numero,'
      'DATENAME (mm,dataevento) mese'
      'from storico'
      'where ((idevento=76) or (idevento=83) or (idevento=20))'
      'and YEAR (dataevento) = YEAR (getdate())'
      'and idutente=:idutente'
      'group by DATENAME (mm,dataevento),MONTH(dataevento)'
      'order by  MONTH (dataevento)')
    Left = 296
    Top = 96
  end
  object QNrPresPerMeseUCAS: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'idutente'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      'select count(distinct idanagrafica) numero,'
      'DATENAME (mm,dataevento) mese'
      'from storico'
      'where ((idevento=76) or (idevento=83) or (idevento=20))'
      'and YEAR (dataevento) = YEAR (getdate())-1'
      'and idutente=:idutente'
      'group by DATENAME (mm,dataevento),MONTH(dataevento)'
      'order by  MONTH (dataevento)')
    Left = 296
    Top = 124
    object IntegerField1: TIntegerField
      FieldName = 'numero'
      ReadOnly = True
    end
    object WideStringField2: TWideStringField
      FieldName = 'mese'
      Size = 30
    end
  end
  object QNrPresPerAnnoUC: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'idutente'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      'select count(distinct idanagrafica) numero,'
      'YEAR (dataevento) anno'
      'from storico'
      'where ((idevento=76) or (idevento=83) or (idevento=20))'
      'and DATEDIFF (yy,dataevento,getdate()) <=1 '
      'and idutente=:idutente'
      'group by YEAR (dataevento)'
      'order by  YEAR (dataevento)')
    Left = 296
    Top = 152
  end
  object QNrPresUltimoMese: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select count(distinct idanagrafica) tot'
      'from storico'
      'where ((idevento=76) or (idevento=83) or (idevento=20))'
      'and DATEDIFF (dd,dataevento,getdate()) between 0 and 30')
    Left = 376
    Top = 180
    object QNrPresUltimoMesetot: TIntegerField
      DefaultExpression = '0'
      FieldName = 'tot'
      ReadOnly = True
    end
  end
  object QNrPresUltimoMeseAS: TADOQuery
    Connection = Data.DB
    Parameters = <>
    SQL.Strings = (
      'select count(distinct idanagrafica) tot'
      'from storico'
      'where ((idevento=76) or (idevento=83) or (idevento=20))'
      
        'and DATEDIFF (dd,dataevento, CAST (DAY (getdate()) AS VARCHAR) +' +
        ' '#39'/'#39'+'
      'CAST (MONTH (getdate()) AS VARCHAR) + '#39'/'#39' +'
      'CAST (YEAR (getdate()) -1 AS VARCHAR)) between 0 and 30')
    Left = 376
    Top = 208
    object QNrPresUltimoMeseAStot: TIntegerField
      FieldName = 'tot'
      ReadOnly = True
    end
  end
  object QNrColloquiAz: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select count(distinct id) numero,'
      'DATENAME (mm,data) mese'
      'from ebc_colloqui'
      'where YEAR (data) = YEAR (getdate())'
      'group by DATENAME (mm,data),MONTH(data)'
      'order by  MONTH (data)')
    Left = 421
    Top = 96
  end
  object QNrColloquiAzAS: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select count(distinct id) numero,'
      'DATENAME (mm,data) mese'
      'from ebc_colloqui'
      'where YEAR (data) = YEAR (getdate())-1'
      'group by DATENAME (mm,data),MONTH(data)'
      'order by  MONTH (data)')
    Left = 421
    Top = 124
    object IntegerField2: TIntegerField
      FieldName = 'numero'
      ReadOnly = True
    end
    object WideStringField3: TWideStringField
      FieldName = 'mese'
      Size = 30
    end
  end
  object QNrColloquiAzPerAnno: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select count(distinct id) numero,'
      'YEAR (data) anno'
      'from ebc_colloqui'
      'where datediff(yy,data,getdate())<=1'
      'group by YEAR (data)')
    Left = 421
    Top = 152
  end
  object QNrCollUC: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'idutente'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      'select count(distinct id) numero,'
      'DATENAME (mm,data) mese'
      'from ebc_colloqui'
      'where YEAR (data) = YEAR (getdate())'
      'and idselezionatore=:idutente'
      'group by DATENAME (mm,data),MONTH(data)'
      'order by  MONTH (data)')
    Left = 456
    Top = 96
  end
  object QNrCollUCAS: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'idutente'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      'select count(distinct id) numero,'
      'DATENAME (mm,data) mese'
      'from ebc_colloqui'
      'where YEAR (data) = YEAR (getdate())-1'
      'and idselezionatore=:idutente'
      'group by DATENAME (mm,data),MONTH(data)'
      'order by  MONTH (data)')
    Left = 456
    Top = 124
    object IntegerField3: TIntegerField
      FieldName = 'numero'
      ReadOnly = True
    end
    object WideStringField4: TWideStringField
      FieldName = 'mese'
      Size = 30
    end
  end
  object QNrCollUCPerAnno: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'idutente'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      'select count(distinct id) numero,'
      'YEAR (data) anno'
      'from ebc_colloqui'
      'where datediff(yy,data,getdate())<=1'
      'and idselezionatore=:idutente'
      'group by YEAR (data)')
    Left = 456
    Top = 152
  end
  object QCommAperteAz: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select count(distinct id) numero,'
      'DATENAME (mm,datainizio) mese'
      'from ebc_ricerche'
      'where YEAR (datainizio) = YEAR (getdate())'
      'group by DATENAME (mm,datainizio),MONTH(datainizio)'
      'order by  MONTH (datainizio)')
    Left = 501
    Top = 96
  end
  object QCommAperteAZAS: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select count(distinct id) numero,'
      'DATENAME (mm,datainizio) mese'
      'from ebc_ricerche'
      'where YEAR (datainizio) = YEAR (getdate())-1'
      'group by DATENAME (mm,datainizio),MONTH(datainizio)'
      'order by  MONTH (datainizio)')
    Left = 501
    Top = 124
    object IntegerField7: TIntegerField
      FieldName = 'numero'
      ReadOnly = True
    end
    object WideStringField5: TWideStringField
      FieldName = 'mese'
      Size = 30
    end
  end
  object QCommAperteAzPerAnno: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select count(distinct id) numero,'
      'YEAR (datainizio) anno'
      'from ebc_ricerche'
      'where datediff(yy,datainizio,getdate())<=1'
      'group by YEAR (datainizio)')
    Left = 501
    Top = 152
  end
  object QCommAperteUC: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'idutente'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      'select count(distinct id) numero,'
      'DATENAME (mm,datainizio) mese'
      'from ebc_ricerche'
      'where YEAR (datainizio) = YEAR (getdate())'
      'and idutente=:idutente'
      'group by DATENAME (mm,datainizio),MONTH(datainizio)'
      'order by  MONTH (datainizio)')
    Left = 533
    Top = 96
  end
  object QCommAperteASUC: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'idutente'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      'select count(distinct id) numero,'
      'DATENAME (mm,datainizio) mese'
      'from ebc_ricerche'
      'where YEAR (datainizio) = YEAR (getdate())-1'
      'and idutente=:idutente'
      'group by DATENAME (mm,datainizio),MONTH(datainizio)'
      'order by  MONTH (datainizio)')
    Left = 533
    Top = 124
    object IntegerField8: TIntegerField
      FieldName = 'numero'
      ReadOnly = True
    end
    object WideStringField6: TWideStringField
      FieldName = 'mese'
      Size = 30
    end
  end
  object QCommAperteUCPerAnno: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'idutente'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      'select count(distinct id) numero,'
      'YEAR (datainizio) anno'
      'from ebc_ricerche'
      'where datediff(yy,datainizio,getdate())<=1'
      'and idutente=:idutente'
      'group by YEAR (datainizio)')
    Left = 533
    Top = 152
  end
  object QAndFattPrevistoTotPerAnno: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select sum(ecr.importo) numero,'
      '  YEAR (dataprevfatt) anno'
      'from '
      '  ebc_compensiricerche ecr join ebc_ricerche er'
      '    on er.id=ecr.idricerca'
      'where datediff (yy,dataprevfatt,getdate()) between -1 and 0'
      'group by YEAR (dataprevfatt)'
      'order by anno'
      '')
    Left = 259
    Top = 123
  end
  object QAndFattPUtenteConPerAnno: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'idutente'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'select sum(ecr.importo) numero,'
      '  YEAR (dataprevfatt) anno'
      'from '
      '  ebc_compensiricerche ecr join ebc_ricerche er'
      '    on er.id=ecr.idricerca'
      'where er.idutente=:idutente'
      'and datediff (yy,dataprevfatt,getdate())between -1 and 0'
      'group by YEAR (dataprevfatt) '
      'order by anno')
    Left = 227
    Top = 123
  end
  object QAndFattAz: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select sum (ecr.importo)  numero,'
      '  MONTH (data) nrmese,'
      '  DATENAME (mm,data) mese'
      'from '
      '  fatture f join ebc_compensiricerche ecr '
      '    on f.id =ecr.idfattura'
      'where data is not null'
      'and YEAR (data) = YEAR (getdate())'
      'group by  MONTH (data),DATENAME (mm,data) '
      'order by nrmese')
    Left = 163
    Top = 95
  end
  object QAndFattAZAS: TADOQuery
    Connection = Data.DB
    Parameters = <>
    SQL.Strings = (
      'select sum (ecr.importo)  numero,'
      '  MONTH (data) nrmese,'
      '  DATENAME (mm,data) mese'
      'from '
      '  fatture f join ebc_compensiricerche ecr '
      '    on f.id =ecr.idfattura'
      'where YEAR (data) = YEAR (getdate())-1'
      'group by  month (data),DATENAME (mm,data) '
      'order by nrmese')
    Left = 163
    Top = 123
  end
  object QAndFattUCPerAnno: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'idUtente'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      'select SUM(numero) numero,'
      'anno'
      'from '
      '(select sum(ecr.importo) numero,'
      '  YEAR (data) anno'
      'from '
      '  fatture f join ebc_compensiricerche ecr'
      '    on f.id =ecr.idfattura'
      '  join ebc_ricerche er'
      '    on er.id=ecr.idricerca'
      'where er.idutente=:idUtente'
      'group by  data'
      'having data is not null'
      'and datediff (yy,data,getdate()) between -2 and 0) aa'
      'group by anno'
      'order by anno')
    Left = 193
    Top = 151
  end
  object ChartEditor1: TChartEditor
    HideTabs = [cetTools]
    Left = 128
    Top = 152
  end
  object ChartPreviewer1: TChartPreviewer
    Left = 128
    Top = 184
  end
  object QAppuntamenti: TADOQuery
    Connection = Data.DB
    BeforeOpen = QAppuntamentiBeforeOpen
    DataSource = DSQUtenti
    Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'al'
        DataType = ftString
        Value = ''
      end
      item
        Name = 'dal'
        DataType = ftString
        Value = ''
      end
      item
        Name = 'id1'
        Size = -1
        Value = Null
      end
      item
        Name = 'al1'
        DataType = ftString
        Value = ''
      end
      item
        Name = 'dal1'
        DataType = ftString
        Value = ''
      end
      item
        Name = 'id2'
        Size = -1
        Value = Null
      end
      item
        Name = 'al2'
        DataType = ftString
        Value = ''
      end
      item
        Name = 'dal2'
        DataType = ftString
        Size = 16
        Value = ''
      end>
    SQL.Strings = (
      
        'select datename (m,dataEVENTO) mese, MONTH (dataevento) nrmese,c' +
        'ount (s.id) tot'
      'from storico s'
      'where idevento=27'
      'and idutente=:id'
      
        'and Datediff (dd,dataevento,getdate()) >= Datediff (dd,:al,getda' +
        'te()) and'
      'datediff (dd,dataevento,getdate())<=Datediff (dd,:dal,getdate())'
      'group by datename (m,dataevento), MONTH (dataevento)'
      'union'
      
        'select datename (m,data) mese, MONTH (data) nrmese,count (a.id) ' +
        'tot'
      'from agenda a'
      'where tipo=1'
      'and idutente=:id1'
      
        'and Datediff (dd,data,getdate()) >= Datediff (dd,:al1,getdate())' +
        ' and'
      'datediff (dd,data,getdate())<=Datediff (dd,:dal1,getdate())'
      'and idcandric not in '
      '(select idanagrafica'
      'from storico'
      'where idevento=27'
      'and idutente=:id2'
      
        'and Datediff (dd,dataevento,getdate()) >= Datediff (dd,:al2,getd' +
        'ate()) and'
      
        'datediff (dd,dataevento,getdate())<=Datediff (dd,:dal2,getdate()' +
        '))'
      'group by datename (m,data), MONTH (data)')
    Left = 648
    Top = 16
    object QAppuntamentimese: TWideStringField
      FieldName = 'mese'
      Size = 30
    end
    object QAppuntamentinrmese: TIntegerField
      FieldName = 'nrmese'
    end
    object QAppuntamentitot: TIntegerField
      FieldName = 'tot'
      ReadOnly = True
    end
  end
  object QUtenti: TADOQuery
    Connection = Data.DB
    Parameters = <>
    SQL.Strings = (
      'select nominativo, id'
      'from users')
    Left = 709
    Top = 15
    object QUtentinominativo: TStringField
      FieldName = 'nominativo'
      Size = 30
    end
    object QUtentiid: TAutoIncField
      FieldName = 'id'
      ReadOnly = True
    end
  end
  object DSQUtenti: TDataSource
    DataSet = QUtenti
    Left = 680
    Top = 16
  end
  object QInserimenti: TADOQuery
    Connection = Data.DB
    BeforeOpen = QInserimentiBeforeOpen
    DataSource = DSQUtenti
    Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'al'
        Size = -1
        Value = Null
      end
      item
        Name = 'dal'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'select count (id) tot,'
      ' Datename (m,dataora) mese,'
      ' MONTH(dataora) nrmese '
      'from log_tableOp'
      'where tabella like '#39'Anagrafica'#39
      'and idutente=:id'
      
        'and Datediff (dd,dataora,getdate()) between Datediff (dd,:al,get' +
        'date()) and  Datediff (dd,:dal,getdate())'
      'and Operation='#39'I'#39
      'group by '
      ' Datename (m,dataora),'
      ' MONTH(dataora)'
      'order by nrmese')
    Left = 648
    Top = 48
  end
  object QInserimentiRic: TADOQuery
    Connection = Data.DB
    BeforeOpen = QInserimentiBeforeOpen
    DataSource = DSQUtenti
    Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'al'
        Size = -1
        Value = Null
      end
      item
        Name = 'dal'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      
        'select datename (m,dataEVENTO) mese, MONTH (dataevento) nrmese,c' +
        'ount (s.id) tot'
      'from storico s'
      'where idevento in (19,63)'
      'and idutente=:id'
      
        'and Datediff (dd,dataevento,getdate()) between Datediff (dd,:al,' +
        'getdate()) and  Datediff (dd,:dal,getdate())'
      'group by datename (m,dataevento), MONTH (dataevento)'
      'order by nrmese')
    Left = 648
    Top = 80
  end
  object QPresentazioni: TADOQuery
    Connection = Data.DB
    BeforeOpen = QInserimentiBeforeOpen
    DataSource = DSQUtenti
    Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'al'
        Size = -1
        Value = Null
      end
      item
        Name = 'dal'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      
        'select datename (m,dataEVENTO) mese, MONTH (dataevento) nrmese,c' +
        'ount (s.id) tot'
      'from storico s'
      'where idevento in (76,83,20)'
      'and idutente=:id'
      
        'and Datediff (dd,dataevento,getdate()) between Datediff (dd,:al,' +
        'getdate()) and  Datediff (dd,:dal,getdate())'
      'group by datename (m,dataevento), MONTH (dataevento)'
      'order by nrmese')
    Left = 648
    Top = 112
  end
  object QRicerche: TADOQuery
    Connection = Data.DB
    BeforeOpen = QRicercheBeforeOpen
    DataSource = DSQUtenti
    Parameters = <
      item
        Name = 'Dal'
        Attributes = [paNullable]
        DataType = ftDateTime
        Precision = 16
        Size = 16
        Value = Null
      end
      item
        Name = 'Al'
        Attributes = [paNullable]
        DataType = ftDateTime
        Precision = 16
        Size = 16
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      
        'select datename (m,datainizio) mese,MONTH (datainizio) nrmese,Co' +
        'unt(ID) Tot'
      'from EBC_Ricerche'
      'where DataInizio>=:Dal and DataInizio<=:Al'
      'and idutente=:id'
      'group by datename (m,datainizio) ,MONTH (datainizio) ')
    Left = 648
    Top = 144
    object QRicerchemese: TWideStringField
      FieldName = 'mese'
      Size = 30
    end
    object QRicerchenrmese: TIntegerField
      FieldName = 'nrmese'
    end
    object QRicercheTot: TIntegerField
      FieldName = 'Tot'
      ReadOnly = True
    end
  end
  object QRicercheChiuse: TADOQuery
    Connection = Data.DB
    BeforeOpen = QRicercheBeforeOpen
    DataSource = DSQUtenti
    Parameters = <
      item
        Name = 'Al'
        Attributes = [paNullable]
        DataType = ftDateTime
        Precision = 16
        Size = 16
        Value = Null
      end
      item
        Name = 'Dal'
        Attributes = [paNullable]
        DataType = ftDateTime
        Precision = 16
        Size = 16
        Value = Null
      end
      item
        Name = 'id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'select datename (m,datafine) mese,MONTH (datafine) nrmese,Count(' +
        'ID) Tot'
      'from EBC_Ricerche'
      
        'where  (Datediff (dd,datafine,getdate()) between Datediff (dd,:a' +
        'l,getdate()) and  Datediff (dd,:dal,getdate()))'
      'and (idutente=:id)'
      'group by datename (m,datafine) ,MONTH (datafine) ')
    Left = 648
    Top = 176
    object QRicercheChiusemese: TWideStringField
      FieldName = 'mese'
      Size = 30
    end
    object QRicercheChiusenrmese: TIntegerField
      FieldName = 'nrmese'
    end
    object QRicercheChiuseTot: TIntegerField
      FieldName = 'Tot'
      ReadOnly = True
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 221
    Top = 218
    object Esporta1: TMenuItem
      Caption = 'Esporta'
      object FoglioExcel1: TMenuItem
        Caption = 'Foglio Excel'
        OnClick = FoglioExcel1Click
      end
      object FoglioCSV1: TMenuItem
        Caption = 'Foglio CSV'
        OnClick = FoglioCSV1Click
      end
    end
    object Copia1: TMenuItem
      Caption = 'Copia'
      object FormatoExcel1: TMenuItem
        Caption = 'Formato Excel'
        OnClick = FormatoExcel1Click
      end
    end
  end
  object SaveDialog1: TSaveDialog
    Title = 'Esporta Contenuto in'
    Left = 253
    Top = 218
  end
  object QFattUCPerMese: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    BeforeOpen = QRicercheBeforeOpen
    DataSource = DSQUtenti
    Parameters = <
      item
        Name = 'id'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'al'
        DataType = ftString
        Size = 16
        Value = Null
      end
      item
        Name = 'dal'
        DataType = ftString
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select SUM(numero) tot,'
      'nrmese,'
      'mese'
      'from '
      '(select sum(ecr.importo) numero,'
      '  MONTH (data) nrmese,'
      '  DATENAME (mm,data) mese'
      'from '
      '  fatture f join ebc_compensiricerche ecr'
      '    on f.id =ecr.idfattura'
      '  join ebc_ricerche er'
      '    on er.id=ecr.idricerca'
      'where er.idutente=:id'
      
        'and Datediff (dd,data,getdate()) between Datediff (dd,:al,getdat' +
        'e()) and  Datediff (dd,:dal,getdate())'
      'group by  MONTH (data),DATENAME (mm,data) ) a'
      'group by nrmese,mese'
      'order by nrmese')
    Left = 649
    Top = 207
    object QFattUCPerMesetot: TFloatField
      FieldName = 'tot'
      ReadOnly = True
      DisplayFormat = '###0.00'
    end
    object QFattUCPerMesenrmese: TIntegerField
      FieldName = 'nrmese'
    end
    object QFattUCPerMesemese: TWideStringField
      FieldName = 'mese'
      Size = 30
    end
  end
  object QOfferte: TADOQuery
    Connection = Data.DB
    BeforeOpen = QInserimentiBeforeOpen
    DataSource = DSQUtenti
    Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'al'
        DataType = ftString
        Size = 4
        Value = Null
      end
      item
        Name = 'dal'
        DataType = ftString
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      
        'select datename (m,data) mese, MONTH (data) nrmese,count (e.id) ' +
        'tot'
      
        'from ebc_offerte e join ebc_offerteutenti eu on eu.idofferta=e.i' +
        'd'
      'where eu.idutente=:id'
      
        'and Datediff (dd,data,getdate()) between Datediff (dd,:al,getdat' +
        'e()) and  Datediff (dd,:dal,getdate())'
      'group by datename (m,data), MONTH (data)'
      'order by nrmese')
    Left = 648
    Top = 240
    object QOffertemese: TWideStringField
      FieldName = 'mese'
      Size = 30
    end
    object QOffertenrmese: TIntegerField
      FieldName = 'nrmese'
    end
    object QOffertetot: TIntegerField
      FieldName = 'tot'
      ReadOnly = True
    end
  end
  object QEventiComm: TADOQuery
    Connection = Data.DB
    BeforeOpen = QRicercheBeforeOpen
    DataSource = DSQUtenti
    Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'Al'
        Attributes = [paNullable]
        DataType = ftDateTime
        Precision = 16
        Size = 16
        Value = Null
      end
      item
        Name = 'Dal'
        Attributes = [paNullable]
        DataType = ftDateTime
        Precision = 16
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      
        'select datename (m,dataEVENTO) mese, MONTH (dataevento) nrmese,c' +
        'ount (s.id) tot'
      'from storico s'
      'where idutente=:id'
      'and (idricerca is not null)'
      
        'and Datediff (dd,dataevento,getdate()) between Datediff (dd,:al,' +
        'getdate()) and  Datediff (dd,:dal,getdate())'
      'group by datename (m,dataevento), MONTH (dataevento)'
      'order by nrmese')
    Left = 648
    Top = 280
    object WideStringField7: TWideStringField
      FieldName = 'mese'
      Size = 30
    end
    object IntegerField9: TIntegerField
      FieldName = 'nrmese'
    end
    object IntegerField10: TIntegerField
      FieldName = 'Tot'
      ReadOnly = True
    end
  end
end
