unit uGestTab;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ExtCtrls, dxCntner, dxTL, dxDBCtrl, dxDBGrid, Db, ADODB, StdCtrls,
     Buttons, Grids, DBGrids;

type
     TGestTabForm = class(TForm)
          Panel1: TPanel;
          QTab: TADOQuery;
          DsQTab: TDataSource;
          dxDBGrid27: TdxDBGrid;
          BitBtn1: TBitBtn;
          BitBtn2: TBitBtn;
          QTabID: TIntegerField;
          QTabCodice: TStringField;
          QTabDescrizione: TStringField;
          dxDBGrid27ID: TdxDBGridMaskColumn;
          dxDBGrid27Codice: TdxDBGridMaskColumn;
          dxDBGrid27Descrizione: TdxDBGridMaskColumn;
          ECerca: TEdit;
          Label1: TLabel;
          procedure FormShow(Sender: TObject);
    procedure ECercaChange(Sender: TObject);
     private
          { Private declarations }
     public
          xTitles: array[1..50] of string;
          xQueryOrig: string;
     end;

var
     GestTabForm: TGestTabForm;

implementation

uses ModuloDati;

{$R *.DFM}

procedure TGestTabForm.FormShow(Sender: TObject);
var i: integer;
begin
     //
end;

procedure TGestTabForm.ECercaChange(Sender: TObject);
begin
     QTab.Close;
     QTab.SQL.clear;
     if ECerca.text <> '' then
          QTab.SQL.text := xQueryOrig + ' where Descrizione like ''%' + ECerca.text + '%'''
     else QTab.SQL.text := xQueryOrig;
     QTab.Open;
end;

end.

