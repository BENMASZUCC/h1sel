�
 TCLIENTIEBCFORM 0  TPF0TClientiEBCFormClientiEBCFormLeftTop� Width�Height�CaptionArchivio clienti EBCFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style PositionpoScreenCenterPixelsPerInch`
TextHeight TPanelPanel16Left Top Width� Height�AlignalLeft
BevelOuterbvNoneCaptionPanel16TabOrder  TPanelPanel18Left Top Width� Height#AlignalTop
BevelOuter	bvLoweredCaptionPanel18TabOrder  TEditEdit2LeftTopWidthyHeightTabOrder Textcliente da cercareOnChangeEdit2Change   TDBGridDBGrid18Left Top#Width� Height�AlignalClient
DataSourceDataSel_EBC.DsClientiEBCReadOnly	TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style Columns	FieldNameDescrizioneWidth�      TPageControlPageControl4Left� Top Width�Height�
ActivePageTSClientiDatiAlignalClientTabOrder 	TTabSheetTSClientiDatiCaptionDati cliente TPanel Left Top Width�Height�AlignalClient
BevelOuter	bvLoweredTabOrder  TLabelLabel21LeftTop/Width7HeightCaptionDescrizioneFocusControlDBEdit18  TLabelLabel23LeftTopWWidth&HeightCaption	IndirizzoFocusControlDBEdit19  TLabelLabel24LeftTopWidthHeightCaptionCapFocusControlDBEdit20  TLabelLabel26Left2TopWidth'HeightCaptionComuneFocusControlDBEdit21  TLabelLabel27Left� TopWidthHeightCaptionProv.FocusControlDBEdit22  TDBEditDBEdit18LeftTop?Width� Height	DataFieldDescrizione
DataSourceDataSel_EBC.DsClientiEBCFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder   TDBEditDBEdit19LeftTopgWidth� Height	DataField	Indirizzo
DataSourceDataSel_EBC.DsClientiEBCTabOrder  TDBEditDBEdit20LeftTop� Width)Height	DataFieldCap
DataSourceDataSel_EBC.DsClientiEBCTabOrder  TDBEditDBEdit21Left2Top� Width� Height	DataFieldComune
DataSourceDataSel_EBC.DsClientiEBCTabOrder  TDBEditDBEdit22Left� Top� WidthHeight	DataField	Provincia
DataSourceDataSel_EBC.DsClientiEBCTabOrder  TPanel
Wallpaper2LeftTopWidth�Height'AlignalTopTabOrder TToolbarButton97TbBClientiNewLeftTopWidth2Height%CaptionNuovoOpaqueOnClickTbBClientiNewClick  TToolbarButton97TbBClientiDelLeft5TopWidth-Height%CaptionEliminaOpaqueOnClickTbBClientiDelClick  TToolbarButton97TbBClientiOKLefteTopWidth2Height%CaptionOKEnabledOpaqueOnClickTbBClientiOKClick  TToolbarButton97TbBClientiCanLeft� TopWidth2Height%CaptionAnnullaEnabledOpaqueOnClickTbBClientiCanClick       