unit TabVociValutazCandStd;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, ExtCtrls, dxTL, dxDBCtrl, dxDBGrid, dxCntner, Db,
     ADODB, TB97;

type
     TTabVociValutazCandStdForm = class(TForm)
          Panel1: TPanel;
          QTabVociStd: TADOQuery;
          DsQTabVoci: TDataSource;
          QTabVociStdID: TAutoIncField;
          QTabVociStdVoce: TStringField;
          QTabVociStdLegenda: TStringField;
          dxDBGrid7: TdxDBGrid;
          dxDBGrid7ID: TdxDBGridMaskColumn;
          dxDBGrid7IDRicerca: TdxDBGridMaskColumn;
          dxDBGrid7Voce: TdxDBGridMaskColumn;
          dxDBGrid7Legenda: TdxDBGridMaskColumn;
          BNew: TToolbarButton97;
          BDel: TToolbarButton97;
          BCan: TToolbarButton97;
          BOK: TToolbarButton97;
    BitBtn1: TToolbarButton97;
          procedure DsQTabVociStateChange(Sender: TObject);
          procedure BNewClick(Sender: TObject);
          procedure BDelClick(Sender: TObject);
          procedure BOKClick(Sender: TObject);
          procedure BCanClick(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BitBtn1Click(Sender: TObject);
     private
          { Private declarations }
     public
     end;

var
     TabVociValutazCandStdForm: TTabVociValutazCandStdForm;

implementation

uses ModuloDati, Main;

{$R *.DFM}

procedure TTabVociValutazCandStdForm.DsQTabVociStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsQTabVoci.State in [dsEdit, dsInsert];
     BNew.Enabled := not b;
     BDel.Enabled := not b;
     BOK.Enabled := b;
     BCan.Enabled := b;
end;

procedure TTabVociValutazCandStdForm.BNewClick(Sender: TObject);
begin
     QTabVociStd.Insert;
end;

procedure TTabVociValutazCandStdForm.BDelClick(Sender: TObject);
begin
     if MessageDlg('Sei sicuro di voler eliminare la voce selezionata ?', mtWarning, [mbNo, mbYes], 0) <> mrYes then exit;
     QTabVociStd.Delete;
end;

procedure TTabVociValutazCandStdForm.BOKClick(Sender: TObject);
begin
     QTabVociStd.Post;
end;

procedure TTabVociValutazCandStdForm.BCanClick(Sender: TObject);
begin
     QTabVociStd.Cancel;
end;

procedure TTabVociValutazCandStdForm.FormShow(Sender: TObject);
begin
     //Grafica
     TabVociValutazCandStdForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     QTabVociStd.Open;
end;

procedure TTabVociValutazCandStdForm.FormClose(Sender: TObject;
     var Action: TCloseAction);
begin
     if DsQTabVoci.state in [dsInsert, dsEdit] then QTabVociStd.Post;
end;

procedure TTabVociValutazCandStdForm.BitBtn1Click(Sender: TObject);
begin
     TabVociValutazCandStdForm.ModalResult:=mrOK;
end;

end.

