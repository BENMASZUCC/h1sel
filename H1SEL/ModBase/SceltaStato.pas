{$A+,B-,C+,D+,E-,F-,G+,H+,I+,J+,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y-,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
unit SceltaStato;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, Grids, DBGrids, ComCtrls, Db, DBTables, ExtCtrls,
     DtEdit97, ADODB, U_ADOLinkCl, DBCtrls, TB97;

type
     TSceltaStatoForm = class(TForm)
          DBGrid1: TDBGrid;
          Panel1: TPanel;
          Panel73: TPanel;
          Panel2: TPanel;
          Label1: TLabel;
          Label3: TLabel;
          Label4: TLabel;
          ECogn: TEdit;
          ENome: TEdit;
          DsQStati: TDataSource;
          PanAnn: TPanel;
          Label2: TLabel;
          DataPervenuto: TDateEdit97;
          Label5: TLabel;
          EDEmail: TEdit;
          EProvCV: TDBComboBox;
          Panel3: TPanel;
          BOk: TToolbarButton97;
          BAnnulla: TToolbarButton97;
          SpeedButton1: TToolbarButton97;
          dsQ1: TDataSource;
          Q1: TADOQuery;
          QStati: TADOQuery;
          BImportLinkedIn: TToolbarButton97;
          procedure FormCreate(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure SpeedButton1Click(Sender: TObject);
          procedure BAnnullaClick(Sender: TObject);
          procedure BOkClick(Sender: TObject);
          procedure ECognKeyPress(Sender: TObject; var Key: Char);
          procedure ENomeKeyPress(Sender: TObject; var Key: Char);
          procedure EDEmailKeyPress(Sender: TObject; var Key: Char);
          procedure EProvCVKeyPress(Sender: TObject; var Key: Char);
          procedure DBGrid1KeyPress(Sender: TObject; var Key: Char);
          procedure DataPervenutoKeyPress(Sender: TObject; var Key: Char);
          procedure BImportLinkedInClick(Sender: TObject);
     private
          { Private declarations }
     public
          xIDAnnuncio: integer;
     end;

var
     SceltaStatoForm: TSceltaStatoForm;

implementation

uses ModuloDati, uUtilsVarie, Curriculum, Main;

{$R *.DFM}


procedure TSceltaStatoForm.FormCreate(Sender: TObject);
begin
     DataPervenuto.Date := Date;
end;

procedure TSceltaStatoForm.FormShow(Sender: TObject);
begin
     //Grafica
     SceltaStatoForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel3.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel3.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel3.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel73.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel73.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel73.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     PanAnn.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     PanAnn.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     PanAnn.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     Caption := '[M/000] - ' + Caption;
     QStati.Open;
     QStati.Locate('ID', 28, []);
     // caricamento EProvCV con le provenienze da tabella
     Q1.Close;
     Q1.SQL.text := 'select Provenienza from ProvenienzeCV';
     Q1.Open;
     if data.Global.fieldbyname('debughrms').AsBoolean then Showmessage('SceltaStatoForm.0');
     EProvCV.items.Clear;
     while not Q1.EOF do begin
          if data.Global.fieldbyname('debughrms').AsBoolean then Showmessage('SceltaStatoForm.1');
          EProvCV.items.Add(Q1.FieldByName('Provenienza').asString);
          Q1.Next;
     end;
     EProvCV.ItemIndex := 0;
     if Data.Global.FieldByName('AbilitaNomiMaiusc').Value then begin
          ECogn.CharCase := ecUpperCase;
          ENome.CharCase := ecUpperCase
     end else begin
          ECogn.CharCase := ecNormal;
          ENome.CharCase := ecNormal;
     end;
end;

procedure TSceltaStatoForm.SpeedButton1Click(Sender: TObject);
begin
     ApriHelp('000');
end;

procedure TSceltaStatoForm.BAnnullaClick(Sender: TObject);
begin
     SceltaStatoForm.ModalResult := mrCancel;
end;

procedure TSceltaStatoForm.BOkClick(Sender: TObject);
begin
     SceltaStatoForm.ModalResult := mrOk;
end;

procedure TSceltaStatoForm.ECognKeyPress(Sender: TObject; var Key: Char);
begin
     if key = chr(13) then BOkClick(self);
end;

procedure TSceltaStatoForm.ENomeKeyPress(Sender: TObject; var Key: Char);
begin
     if key = chr(13) then BOkClick(self);
end;

procedure TSceltaStatoForm.EDEmailKeyPress(Sender: TObject; var Key: Char);
begin
     if key = chr(13) then BOkClick(self);
end;

procedure TSceltaStatoForm.EProvCVKeyPress(Sender: TObject; var Key: Char);
begin
     if key = chr(13) then BOkClick(self);
end;

procedure TSceltaStatoForm.DBGrid1KeyPress(Sender: TObject; var Key: Char);
begin
     if key = chr(13) then BOkClick(self);
end;

procedure TSceltaStatoForm.DataPervenutoKeyPress(Sender: TObject;
     var Key: Char);
begin
     if key = chr(13) then BOkClick(self);
end;

procedure TSceltaStatoForm.BImportLinkedInClick(Sender: TObject);
begin
     MainForm.ImportaLinkedInClick(nil);
     SceltaStatoForm.ModalResult := mrCancel;
end;

end.

