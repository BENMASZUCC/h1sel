unit SelLingua;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Grids, DBGrids, StdCtrls, Buttons, Db, DBTables, ExtCtrls, ADODB,
     U_ADOLinkCl;

type
     TSelLinguaForm = class(TForm)
          DataSource1: TDataSource;
          BitBtn1: TBitBtn;
          BitBtn2: TBitBtn;
          DBGrid1: TDBGrid;
          Timer1: TTimer;
          Lingue: TADOLinkedTable;
          procedure DBGrid1KeyPress(Sender: TObject; var Key: Char);
          procedure FormShow(Sender: TObject);
          procedure Timer1Timer(Sender: TObject);
     private
          { Private declarations }
          xStringa: string;
     public
          { Public declarations }
     end;

var
     SelLinguaForm: TSelLinguaForm;

implementation
uses ModuloDati;
{$R *.DFM}

procedure TSelLinguaForm.DBGrid1KeyPress(Sender: TObject; var Key: Char);
begin
     Timer1.Enabled := False;
     if Key = chr(13) then begin
          BitBtn1.Click;
     end else begin
          xStringa := xStringa + key;
          //        Lingue.FindNearest([xStringa]);
          Lingue.FindNearestADO(VarArrayOf([xStringa]));
     end;
     Timer1.Enabled := True;
end;

procedure TSelLinguaForm.FormShow(Sender: TObject);
begin
     Caption := '[S/39] - ' + Caption;
     xStringa := '';
end;

procedure TSelLinguaForm.Timer1Timer(Sender: TObject);
begin
     xStringa := '';
end;

end.
