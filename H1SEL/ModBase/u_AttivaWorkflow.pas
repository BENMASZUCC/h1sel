unit u_AttivaWorkflow;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     TB97, ExtCtrls, StdCtrls, Mask, Db, ADODB;

type
     TAttivaworkflowForm = class(TForm)
          Panel2: TPanel;
          BitBtn1: TToolbarButton97;
          BAnnulla: TToolbarButton97;
          Panel1: TPanel;
          GroupBox1: TGroupBox;
          BDataOdierna: TToolbarButton97;
          DEData: TMaskEdit;
          GroupBox2: TGroupBox;
          Memo1: TMemo;
          QCK: TADOQuery;
    Label1: TLabel;
    Label2: TLabel;
    Panel3: TPanel;
          procedure FormShow(Sender: TObject);
          procedure BDataOdiernaClick(Sender: TObject);
          procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure BitBtn1Click(Sender: TObject);
    procedure BAnnullaClick(Sender: TObject);
     private
    { Private declarations }
     public
    { Public declarations }
          xIDAnag, xIDRicerca: integer;
     end;

var
     AttivaworkflowForm: TAttivaworkflowForm;

implementation

uses Main, ModuloDati;

{$R *.DFM}

procedure TAttivaworkflowForm.FormShow(Sender: TObject);
begin

    //grafica

     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel3.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;

end;

procedure TAttivaworkflowForm.BDataOdiernaClick(Sender: TObject);
begin
     DEData.Text := datetostr(Date);
end;

procedure TAttivaworkflowForm.FormCloseQuery(Sender: TObject;
     var CanClose: Boolean);
begin
if ModalResult = mrOK then begin

     QCK.Close;
     QCK.SQL.Text := 'Select ID from WFScadenze_Ricerche where IDAnagrafica=:xIDAnagrafica and IDRicerca=:xIDRicerca ';
     QCK.Parameters.ParamByName('xIDAnagrafica').value := xIDAnag;
     QCK.Parameters.ParamByName('xIDRicerca').value := xIDRicerca;
     QCK.Open;

     if QCK.RecordCount = 0 then begin
          CanClose := true;
     end else begin
          CanClose := false;
          MessageDlg('Esiste gi� un Workflow per questo candidato su questo progetto ', mtInformation, [mbOk], 0);

     end;

end;

end;

procedure TAttivaworkflowForm.BitBtn1Click(Sender: TObject);
begin
ModalResult := mrOK;
end;

procedure TAttivaworkflowForm.BAnnullaClick(Sender: TObject);
begin
ModalResult := mrCancel;
end;

end.

