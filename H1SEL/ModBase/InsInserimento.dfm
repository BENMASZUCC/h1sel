�
 TINSINSERIMENTOFORM 0�  TPF0TInsInserimentoFormInsInserimentoFormLeft2Top� BorderStylebsDialogCaption.Selezione ricerca e inserimento dati necessariClientHeight�ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterPixelsPerInch`
TextHeight TPanelPanel1Left Top Width�Height:AlignalTopTabOrder  TPanelPanel7LeftTopWidthHeight8AlignalLeft
BevelOuterbvNoneEnabledTabOrder  TLabelLabel1LeftTop	Width#HeightCaptionCliente:  TLabelLabel2LeftTopWidth.HeightCaption	Soggetto:  TDBEditDBEdit12Left:TopWidth� HeightColorclAqua	DataFieldDescrizione
DataSourceData2.DsEBCclientiFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontReadOnly	TabOrder   TEdit	ESoggettoLeft:TopWidth� HeightColorclYellowFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder   TBitBtnBitBtn1Left7TopWidthRHeight TabOrderOnClickBitBtn1ClickKindbkOK  TBitBtnBitBtn2Left�TopWidthNHeight CaptionAnnullaTabOrderKindbkCancel   TDBGridDBGrid7Left TopOWidth�Height� AlignalClient
DataSourceData2.DsRicClientiReadOnly	TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameProgressivoTitle.CaptionRif.Width2Visible	 Expanded	FieldName
DataInizioTitle.CaptionData inizioWidthMVisible	 	AlignmenttaCenterExpanded	FieldNameNumRicercatiTitle.AlignmenttaCenterTitle.CaptionN�ric.Width!Visible	 Expanded	FieldNameRuoloWidth� Visible	 Expanded	FieldNameUtenteWidthZVisible	    TPanelPanel2Left Top/Width�Height~AlignalBottom
BevelOuter	bvLoweredTabOrder TLabelLabel3LeftTopWidth0HeightCaption
Dalla data  TLabelLabel4LeftTop6Width)HeightCaption	Alla data  TLabelLabel5LeftTopNWidthXHeightCaptionMotivo cessazione  TLabelLabel6LeftTopfWidthHeightCaptionNote  TPanelPanTitoloCompensiLeftTopWidth�HeightAlignalTop	AlignmenttaLeftJustifyCaption  Altri datiColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   TDateEdit97	DEDataDalLeftfTopWidthyHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderColorCalendar.ColorValidclBlueDayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanClear
doCanPopup
doIsMaskeddoShowCanceldoShowToday   TDateEdit97DEDataAlLeftfTop3WidthyHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderColorCalendar.ColorValidclBlueDayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanClear
doCanPopup
doIsMaskeddoShowCanceldoShowToday   TEditEMotCessLeftfTopKWidthpHeightTabOrder  TEditENoteLeftfTopcWidthpHeightTabOrder   TPanelPanel3Left Top:Width�HeightAlignalTop	AlignmenttaLeftJustifyCaption&  Ricerche attive associate al clienteColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   