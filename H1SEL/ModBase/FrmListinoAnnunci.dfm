object ListinoAnnunciFrame: TListinoAnnunciFrame
  Left = 0
  Top = 0
  Width = 683
  Height = 417
  TabOrder = 0
  object Splitter1: TSplitter
    Left = 208
    Top = 0
    Width = 3
    Height = 417
    Cursor = crHSplit
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 208
    Height = 417
    Align = alLeft
    BevelOuter = bvLowered
    TabOrder = 0
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 206
      Height = 21
      Align = alTop
      Alignment = taLeftJustify
      Caption = '  Testata-Edizione'
      Color = clGray
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object DBGrid1: TDBGrid
      Left = 1
      Top = 22
      Width = 206
      Height = 394
      Align = alClient
      DataSource = DsQEdizioni
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
      ReadOnly = True
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Testata'
          Width = 95
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Edizione'
          Width = 75
          Visible = True
        end>
    end
  end
  object Panel4: TPanel
    Left = 211
    Top = 0
    Width = 472
    Height = 417
    Align = alClient
    BevelOuter = bvLowered
    TabOrder = 1
    object Panel5: TPanel
      Left = 1
      Top = 1
      Width = 470
      Height = 21
      Align = alTop
      Alignment = taLeftJustify
      Caption = '  Listino prezzi per:'
      Color = clGray
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object DBText1: TDBText
        Left = 96
        Top = 5
        Width = 289
        Height = 13
        DataField = 'TestataEdizione'
        DataSource = DsQEdizioni
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
    object Panel1: TPanel
      Left = 1
      Top = 22
      Width = 470
      Height = 44
      Align = alTop
      BevelOuter = bvLowered
      TabOrder = 1
      object ToolbarButton972: TToolbarButton97
        Left = 3
        Top = 1
        Width = 75
        Height = 42
        Caption = 'Nuovo prezzo'
        Glyph.Data = {
          36050000424D3605000000000000360400002800000010000000100000000100
          08000000000000010000120B0000120B00000001000000010000000000008484
          8400C642FF0000FFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
          FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00020202020000
          0000000000000000000002020202000404040404040404040400020202020004
          0404040404040404040002020202000400000000000000040400020202020004
          0404040404040404040002020202000400000000000000040400020202020004
          0404040404040404040002020202010400000000000000040400020102020103
          0404040404040404040002020202010404010000040404040400020101010403
          0304040404000000000002020301030401010101040004040002020201030103
          0103040404000400020202010302010202010304040000020202020302020103
          0202010000000202020202020202020202020202020202020202}
        WordWrap = True
        OnClick = ToolbarButton972Click
      end
      object ToolbarButton973: TToolbarButton97
        Left = 79
        Top = 1
        Width = 75
        Height = 42
        Caption = 'Modifica prezzo'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
          000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
          00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
          F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
          0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
          FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
          FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
          0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
          00333377737FFFFF773333303300000003333337337777777333}
        NumGlyphs = 2
        WordWrap = True
        OnClick = ToolbarButton973Click
      end
      object ToolbarButton974: TToolbarButton97
        Left = 155
        Top = 1
        Width = 75
        Height = 42
        Caption = 'Elimina prezzo'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333000000000
          3333333777777777F3333330F777777033333337F3F3F3F7F3333330F0808070
          33333337F7F7F7F7F3333330F080707033333337F7F7F7F7F3333330F0808070
          33333337F7F7F7F7F3333330F080707033333337F7F7F7F7F3333330F0808070
          333333F7F7F7F7F7F3F33030F080707030333737F7F7F7F7F7333300F0808070
          03333377F7F7F7F773333330F080707033333337F7F7F7F7F333333070707070
          33333337F7F7F7F7FF3333000000000003333377777777777F33330F88877777
          0333337FFFFFFFFF7F3333000000000003333377777777777333333330777033
          3333333337FFF7F3333333333000003333333333377777333333}
        NumGlyphs = 2
        WordWrap = True
        OnClick = ToolbarButton974Click
      end
      object BModPrezzoQuestoCli: TToolbarButton97
        Left = 365
        Top = 1
        Width = 98
        Height = 41
        Caption = 'Imposta prezzo cliente'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clPurple
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00500000000055
          555557777777775F55550FFFFFFFFF0555557F5555555F7FFF5F0FEEEEEE0000
          05007F555555777775770FFFFFF0BFBFB00E7F5F5557FFF557770F0EEEE000FB
          FB0E7F75FF57775555770FF00F0FBFBFBF0E7F57757FFFF555770FE0B00000FB
          FB0E7F575777775555770FFF0FBFBFBFBF0E7F5575FFFFFFF5770FEEE0000000
          FB0E7F555777777755770FFFFF0B00BFB0007F55557577FFF7770FEEEEE0B000
          05557F555557577775550FFFFFFF0B0555557FF5F5F57575F55500F0F0F0F0B0
          555577F7F7F7F7F75F5550707070700B055557F7F7F7F7757FF5507070707050
          9055575757575757775505050505055505557575757575557555}
        NumGlyphs = 2
        ParentFont = False
        WordWrap = True
        OnClick = BModPrezzoQuestoCliClick
      end
    end
    object PCValuta: TPageControl
      Left = 1
      Top = 89
      Width = 470
      Height = 327
      ActivePage = TSEuro
      Align = alClient
      TabOrder = 2
      object TSLire: TTabSheet
        Caption = 'Prezzi in Lire'
        TabVisible = False
        object Panel7: TPanel
          Left = 0
          Top = 214
          Width = 462
          Height = 85
          Align = alBottom
          BevelOuter = bvLowered
          Enabled = False
          TabOrder = 0
          object DBText2: TDBText
            Left = 6
            Top = 39
            Width = 117
            Height = 17
            DataField = 'Testata'
            DataSource = DsQEdizioni
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label1: TLabel
            Left = 5
            Top = 64
            Width = 71
            Height = 13
            Caption = 'Tutte le testate'
          end
          object Label3: TLabel
            Left = 158
            Top = 22
            Width = 33
            Height = 14
            Caption = 'Listino'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsItalic]
            ParentFont = False
          end
          object Label4: TLabel
            Left = 227
            Top = 22
            Width = 25
            Height = 14
            Caption = 'A noi'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsItalic]
            ParentFont = False
          end
          object Label5: TLabel
            Left = 267
            Top = 22
            Width = 46
            Height = 14
            Caption = 'Al cliente'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsItalic]
            ParentFont = False
          end
          object Panel8: TPanel
            Left = 1
            Top = 1
            Width = 460
            Height = 21
            Align = alTop
            Alignment = taLeftJustify
            Caption = '  Prezzi migliori praticati (in Lire)'
            Color = clGray
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
          end
          object DBEdit1: TDBEdit
            Left = 128
            Top = 36
            Width = 64
            Height = 21
            Color = 13695481
            DataField = 'minPrezzoListinoLire'
            DataSource = DsQMinPrezziTestata
            TabOrder = 1
          end
          object DBEdit2: TDBEdit
            Left = 191
            Top = 36
            Width = 63
            Height = 21
            DataField = 'minPrezzoANoiLire'
            DataSource = DsQMinPrezziTestata
            TabOrder = 2
          end
          object DBEdit3: TDBEdit
            Left = 253
            Top = 36
            Width = 61
            Height = 21
            DataField = 'minPrezzoAlClienteLire'
            DataSource = DsQMinPrezziTestata
            TabOrder = 3
          end
          object DBEdit4: TDBEdit
            Left = 128
            Top = 60
            Width = 64
            Height = 21
            Color = 13695481
            DataField = 'minPrezzoListinoLire'
            DataSource = DsQMinPrezziTot
            TabOrder = 4
          end
          object DBEdit5: TDBEdit
            Left = 191
            Top = 60
            Width = 63
            Height = 21
            DataField = 'minPrezzoANoiLire'
            DataSource = DsQMinPrezziTot
            TabOrder = 5
          end
          object DBEdit6: TDBEdit
            Left = 253
            Top = 60
            Width = 61
            Height = 21
            DataField = 'minPrezzoAlClienteLire'
            DataSource = DsQMinPrezziTot
            TabOrder = 6
          end
        end
        object RxDBGrid1: TdxDBGrid
          Left = 0
          Top = 0
          Width = 462
          Height = 214
          Bands = <
            item
            end>
          DefaultLayout = True
          HeaderPanelRowCount = 1
          KeyField = 'ID'
          SummaryGroups = <>
          SummarySeparator = ', '
          Align = alClient
          TabOrder = 1
          DataSource = DsQListinoEdiz
          Filter.Criteria = {00000000}
          OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
          OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoIndicator, edgoUseBitmap]
          object dxDBGridDallaData: TdxDBGridColumn
            Caption = 'Dal'
            Sorted = csUp
            Width = 66
            BandIndex = 0
            RowIndex = 0
            FieldName = 'DallaData'
          end
          object dxDBGridQtaDa: TdxDBGridColumn
            Caption = 'Da'
            Width = 53
            BandIndex = 0
            RowIndex = 0
            FieldName = 'QtaDa'
          end
          object dxDBGridQtaA: TdxDBGridColumn
            Caption = 'A'
            Width = 53
            BandIndex = 0
            RowIndex = 0
            FieldName = 'QtaA'
          end
          object dxDBGridPrezzoListinoEuro: TdxDBGridColumn
            Caption = 'Listino'
            Width = 53
            BandIndex = 0
            RowIndex = 0
            FieldName = 'PrezzoListinoEuro'
          end
          object dxDBGridPrezzoANoiEuro: TdxDBGridColumn
            Caption = 'A Noi'
            Width = 53
            BandIndex = 0
            RowIndex = 0
            FieldName = 'PrezzoANoiEuro'
          end
          object dxDBGridPrezzoAlClienteEuro: TdxDBGridColumn
            Caption = 'Al Cliente'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clMaroon
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 53
            BandIndex = 0
            RowIndex = 0
            FieldName = 'PrezzoAlClienteEuro'
          end
          object dxDBGridDiffEuro: TdxDBGridColumn
            Caption = 'Guadagno'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clPurple
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 53
            BandIndex = 0
            RowIndex = 0
            FieldName = 'DiffEuro'
          end
          object dxDBGridPrezzoQuestoClienteEuro: TdxDBGridColumn
            Caption = 'Prezzo Questo'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clPurple
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 62
            BandIndex = 0
            RowIndex = 0
            FieldName = 'PrezzoQuestoClienteEuro'
          end
        end
      end
      object TSEuro: TTabSheet
        Caption = 'Prezzi in Euro'
        ImageIndex = 1
        object Panel9: TPanel
          Left = 0
          Top = 213
          Width = 462
          Height = 86
          Align = alBottom
          BevelOuter = bvLowered
          Enabled = False
          TabOrder = 0
          object DBText3: TDBText
            Left = 6
            Top = 40
            Width = 117
            Height = 17
            DataField = 'Testata'
            DataSource = DsQEdizioni
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label2: TLabel
            Left = 6
            Top = 65
            Width = 71
            Height = 13
            Caption = 'Tutte le testate'
          end
          object Label6: TLabel
            Left = 158
            Top = 22
            Width = 33
            Height = 14
            Caption = 'Listino'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsItalic]
            ParentFont = False
          end
          object Label7: TLabel
            Left = 227
            Top = 22
            Width = 25
            Height = 14
            Caption = 'A noi'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsItalic]
            ParentFont = False
          end
          object Label8: TLabel
            Left = 267
            Top = 22
            Width = 46
            Height = 14
            Caption = 'Al cliente'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsItalic]
            ParentFont = False
          end
          object Panel10: TPanel
            Left = 1
            Top = 1
            Width = 460
            Height = 21
            Align = alTop
            Alignment = taLeftJustify
            Caption = '  Prezzi migliori praticati (in Euro)'
            Color = clGray
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
          end
          object DBEdit7: TDBEdit
            Left = 128
            Top = 37
            Width = 64
            Height = 21
            Color = 13695481
            DataField = 'minPrezzoListinoEuro'
            DataSource = DsQMinPrezziTestata
            TabOrder = 1
          end
          object DBEdit8: TDBEdit
            Left = 191
            Top = 37
            Width = 63
            Height = 21
            DataField = 'minPrezzoANoiLireEuro'
            DataSource = DsQMinPrezziTestata
            TabOrder = 2
          end
          object DBEdit9: TDBEdit
            Left = 253
            Top = 37
            Width = 61
            Height = 21
            DataField = 'minPrezzoAlClienteEuro'
            DataSource = DsQMinPrezziTestata
            TabOrder = 3
          end
          object DBEdit10: TDBEdit
            Left = 128
            Top = 61
            Width = 64
            Height = 21
            Color = 13695481
            DataField = 'minPrezzoListinoEuro'
            DataSource = DsQMinPrezziTot
            TabOrder = 4
          end
          object DBEdit11: TDBEdit
            Left = 191
            Top = 61
            Width = 63
            Height = 21
            DataField = 'minPrezzoANoiLireEuro'
            DataSource = DsQMinPrezziTot
            TabOrder = 5
          end
          object DBEdit12: TDBEdit
            Left = 253
            Top = 61
            Width = 61
            Height = 21
            DataField = 'minPrezzoAlClienteEuro'
            DataSource = DsQMinPrezziTot
            TabOrder = 6
          end
        end
        object RxDBGrid2: TdxDBGrid
          Left = 0
          Top = 0
          Width = 462
          Height = 213
          Bands = <
            item
            end>
          DefaultLayout = True
          HeaderPanelRowCount = 1
          KeyField = 'ID'
          SummaryGroups = <>
          SummarySeparator = ', '
          Align = alClient
          TabOrder = 1
          DataSource = DsQListinoEdiz
          Filter.Criteria = {00000000}
          OptionsBehavior = [edgoAutoSort, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoTabThrough, edgoVertThrough]
          OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
          OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoIndicator, edgoUseBitmap]
          object dxDBGrid1DallaData: TdxDBGridColumn
            Caption = 'Dal'
            Width = 66
            BandIndex = 0
            RowIndex = 0
            FieldName = 'DallaData'
          end
          object dxDBGrid1QtaDa: TdxDBGridColumn
            Caption = 'Da'
            Width = 53
            BandIndex = 0
            RowIndex = 0
            FieldName = 'QtaDa'
          end
          object dxDBGrid1QtaA: TdxDBGridColumn
            Caption = 'A'
            Width = 53
            BandIndex = 0
            RowIndex = 0
            FieldName = 'QtaA'
          end
          object dxDBGrid1PrezzoListinoEuro: TdxDBGridColumn
            Caption = 'Listino'
            Width = 53
            BandIndex = 0
            RowIndex = 0
            FieldName = 'PrezzoListinoEuro'
          end
          object dxDBGrid1PrezzoANoiEuro: TdxDBGridColumn
            Caption = 'A Noi'
            Width = 53
            BandIndex = 0
            RowIndex = 0
            FieldName = 'PrezzoANoiEuro'
          end
          object dxDBGrid1PrezzoAlClienteEuro: TdxDBGridColumn
            Caption = 'Al Cliente'
            Width = 53
            BandIndex = 0
            RowIndex = 0
            FieldName = 'PrezzoAlClienteEuro'
          end
          object dxDBGrid1DiffEuro: TdxDBGridColumn
            Caption = 'Guadagno'
            Width = 53
            BandIndex = 0
            RowIndex = 0
            FieldName = 'DiffEuro'
          end
          object dxDBGrid1PrezzoQuestoClienteEuro: TdxDBGridColumn
            Caption = 'Prezzo Questo'
            Width = 62
            BandIndex = 0
            RowIndex = 0
            FieldName = 'PrezzoQuestoClienteEuro'
          end
        end
      end
    end
    object Panel6: TPanel
      Left = 1
      Top = 66
      Width = 470
      Height = 23
      Align = alTop
      Alignment = taLeftJustify
      Color = clGray
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      object LCliente: TLabel
        Left = 7
        Top = 5
        Width = 347
        Height = 13
        AutoSize = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
  end
  object QEdizioni_OLD: TQuery
    OnCalcFields = QEdizioni_OLDCalcFields
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      
        'select distinct Ann_Edizioni.ID IDEdizione, Ann_Edizioni.IDTesta' +
        'ta,'
      '       Ann_Testate.Denominazione Testata, NomeEdizione Edizione'
      'from Ann_Testate,Ann_Edizioni'
      'where Ann_Testate.ID=Ann_Edizioni.IDTestata'
      'order by Ann_Testate.Denominazione,NomeEdizione')
    Left = 48
    Top = 64
    object QEdizioni_OLDIDEdizione: TAutoIncField
      FieldName = 'IDEdizione'
      Origin = 'EBCDB.Ann_Edizioni.ID'
    end
    object QEdizioni_OLDTestata: TStringField
      FieldName = 'Testata'
      Origin = 'EBCDB.Ann_Testate.Denominazione'
      FixedChar = True
      Size = 50
    end
    object QEdizioni_OLDEdizione: TStringField
      FieldName = 'Edizione'
      Origin = 'EBCDB.Ann_Edizioni.NomeEdizione'
      FixedChar = True
      Size = 50
    end
    object QEdizioni_OLDTestataEdizione: TStringField
      FieldKind = fkCalculated
      FieldName = 'TestataEdizione'
      Size = 50
      Calculated = True
    end
    object QEdizioni_OLDIDTestata: TIntegerField
      FieldName = 'IDTestata'
      Origin = 'EBCDB.Ann_Edizioni.IDTestata'
    end
  end
  object DsQEdizioni: TDataSource
    DataSet = QEdizioni
    Left = 48
    Top = 96
  end
  object QListinoEdiz_OLD: TQuery
    AfterOpen = QListinoEdiz_OLDAfterOpen
    AfterScroll = QListinoEdiz_OLDAfterScroll
    OnCalcFields = QListinoEdiz_OLDCalcFields
    DatabaseName = 'EBCDB'
    DataSource = DsQEdizioni
    SQL.Strings = (
      'select Ann_EdizListino.*,'
      '       Ann_ListinoClienti.* '
      
        'from Ann_EdizListino left join Ann_ListinoClienti on Ann_EdizLis' +
        'tino.ID = Ann_ListinoClienti.IDEdizListino'
      'and Ann_ListinoClienti.IDCliente=:xIDCliente'
      'where IDEdizione=:IDEdizione'
      'order by QtaDa,QtaA,dallaData desc')
    Left = 229
    Top = 184
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'xIDCliente'
        ParamType = ptUnknown
      end
      item
        DataType = ftAutoInc
        Name = 'IDEdizione'
        ParamType = ptUnknown
      end>
    object QListinoEdiz_OLDID: TAutoIncField
      FieldName = 'ID'
    end
    object QListinoEdiz_OLDIDEdizione: TIntegerField
      FieldName = 'IDEdizione'
    end
    object QListinoEdiz_OLDDallaData: TDateTimeField
      FieldName = 'DallaData'
    end
    object QListinoEdiz_OLDPrezzoListinoLire: TFloatField
      FieldName = 'PrezzoListinoLire'
      DisplayFormat = '#,###'
    end
    object QListinoEdiz_OLDPrezzoANoiLire: TFloatField
      FieldName = 'PrezzoANoiLire'
      DisplayFormat = '#,###'
    end
    object QListinoEdiz_OLDPrezzoAlClienteLire: TFloatField
      FieldName = 'PrezzoAlClienteLire'
      DisplayFormat = '#,###'
    end
    object QListinoEdiz_OLDPrezzoListinoEuro: TFloatField
      FieldName = 'PrezzoListinoEuro'
      DisplayFormat = '#,###.00'
    end
    object QListinoEdiz_OLDPrezzoANoiEuro: TFloatField
      FieldName = 'PrezzoANoiEuro'
      DisplayFormat = '#,###.00'
    end
    object QListinoEdiz_OLDPrezzoAlClienteEuro: TFloatField
      FieldName = 'PrezzoAlClienteEuro'
      DisplayFormat = '#,###.00'
    end
    object QListinoEdiz_OLDNote: TStringField
      FieldName = 'Note'
      FixedChar = True
      Size = 80
    end
    object QListinoEdiz_OLDQtaDa: TSmallintField
      FieldName = 'QtaDa'
    end
    object QListinoEdiz_OLDQtaA: TSmallintField
      FieldName = 'QtaA'
    end
    object QListinoEdiz_OLDDiffLire: TFloatField
      FieldKind = fkCalculated
      FieldName = 'DiffLire'
      DisplayFormat = '#,###'
      Calculated = True
    end
    object QListinoEdiz_OLDDiffEuro: TFloatField
      FieldKind = fkCalculated
      FieldName = 'DiffEuro'
      DisplayFormat = '#,###.00'
      Calculated = True
    end
    object QListinoEdiz_OLDID_1: TAutoIncField
      FieldName = 'ID_1'
    end
    object QListinoEdiz_OLDPrezzoQuestoClienteLire: TFloatField
      FieldName = 'PrezzoQuestoClienteLire'
      DisplayFormat = '#,###'
    end
    object QListinoEdiz_OLDPrezzoQuestoClienteEuro: TFloatField
      FieldName = 'PrezzoQuestoClienteEuro'
      DisplayFormat = '#,###.00'
    end
    object QListinoEdiz_OLDIDCliente_1: TIntegerField
      FieldName = 'IDCliente_1'
    end
  end
  object DsQListinoEdiz: TDataSource
    DataSet = QListinoEdiz
    Left = 229
    Top = 216
  end
  object QMinPrezziTestata_OLD: TQuery
    DatabaseName = 'EBCDB'
    DataSource = DsQEdizioni
    SQL.Strings = (
      'select min(PrezzoListinoLire) minPrezzoListinoLire,'
      '       min(PrezzoANoiLire) minPrezzoANoiLire,'
      '       min(PrezzoAlClienteLire) minPrezzoAlClienteLire,'
      '       min(PrezzoListinoEuro) minPrezzoListinoEuro,'
      '       min(PrezzoANoiEuro) minPrezzoANoiLireEuro,'
      '       min(PrezzoAlClienteEuro) minPrezzoAlClienteEuro'
      'from Ann_EdizListino, Ann_Edizioni'
      'where IDEdizione = Ann_Edizioni.ID'
      '  and Ann_Edizioni.IDTestata = :IDTestata')
    Left = 136
    Top = 328
    ParamData = <
      item
        DataType = ftInteger
        Name = 'IDTestata'
        ParamType = ptUnknown
      end>
    object QMinPrezziTestata_OLDminPrezzoListinoLire: TFloatField
      FieldName = 'minPrezzoListinoLire'
      Origin = 'EBCDB.Ann_EdizListino.PrezzoListinoLire'
      DisplayFormat = '#,###'
    end
    object QMinPrezziTestata_OLDminPrezzoANoiLire: TFloatField
      FieldName = 'minPrezzoANoiLire'
      Origin = 'EBCDB.Ann_EdizListino.PrezzoANoiLire'
      DisplayFormat = '#,###'
    end
    object QMinPrezziTestata_OLDminPrezzoAlClienteLire: TFloatField
      FieldName = 'minPrezzoAlClienteLire'
      Origin = 'EBCDB.Ann_EdizListino.PrezzoAlClienteLire'
      DisplayFormat = '#,###'
    end
    object QMinPrezziTestata_OLDminPrezzoListinoEuro: TFloatField
      FieldName = 'minPrezzoListinoEuro'
      Origin = 'EBCDB.Ann_EdizListino.PrezzoListinoEuro'
      DisplayFormat = '#,###.##'
    end
    object QMinPrezziTestata_OLDminPrezzoANoiLireEuro: TFloatField
      FieldName = 'minPrezzoANoiLireEuro'
      Origin = 'EBCDB.Ann_EdizListino.PrezzoANoiEuro'
      DisplayFormat = '#,###.##'
    end
    object QMinPrezziTestata_OLDminPrezzoAlClienteEuro: TFloatField
      FieldName = 'minPrezzoAlClienteEuro'
      Origin = 'EBCDB.Ann_EdizListino.PrezzoAlClienteEuro'
      DisplayFormat = '#,###.##'
    end
  end
  object DsQMinPrezziTestata: TDataSource
    DataSet = QMinPrezziTestata
    Left = 168
    Top = 328
  end
  object QMinPrezziTot_OLD: TQuery
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select min(PrezzoListinoLire) minPrezzoListinoLire,'
      '       min(PrezzoANoiLire) minPrezzoANoiLire,'
      '       min(PrezzoAlClienteLire) minPrezzoAlClienteLire,'
      '       min(PrezzoListinoEuro) minPrezzoListinoEuro,'
      '       min(PrezzoANoiEuro) minPrezzoANoiLireEuro,'
      '       min(PrezzoAlClienteEuro) minPrezzoAlClienteEuro'
      'from Ann_EdizListino')
    Left = 136
    Top = 360
    object QMinPrezziTot_OLDminPrezzoListinoLire: TFloatField
      FieldName = 'minPrezzoListinoLire'
      Origin = 'EBCDB.Ann_EdizListino.PrezzoListinoLire'
      DisplayFormat = '#,###'
    end
    object QMinPrezziTot_OLDminPrezzoANoiLire: TFloatField
      FieldName = 'minPrezzoANoiLire'
      Origin = 'EBCDB.Ann_EdizListino.PrezzoANoiLire'
      DisplayFormat = '#,###'
    end
    object QMinPrezziTot_OLDminPrezzoAlClienteLire: TFloatField
      FieldName = 'minPrezzoAlClienteLire'
      Origin = 'EBCDB.Ann_EdizListino.PrezzoAlClienteLire'
      DisplayFormat = '#,###'
    end
    object QMinPrezziTot_OLDminPrezzoListinoEuro: TFloatField
      FieldName = 'minPrezzoListinoEuro'
      Origin = 'EBCDB.Ann_EdizListino.PrezzoListinoEuro'
      DisplayFormat = '#,###.##'
    end
    object QMinPrezziTot_OLDminPrezzoANoiLireEuro: TFloatField
      FieldName = 'minPrezzoANoiLireEuro'
      Origin = 'EBCDB.Ann_EdizListino.PrezzoANoiEuro'
      DisplayFormat = '#,###.##'
    end
    object QMinPrezziTot_OLDminPrezzoAlClienteEuro: TFloatField
      FieldName = 'minPrezzoAlClienteEuro'
      Origin = 'EBCDB.Ann_EdizListino.PrezzoAlClienteEuro'
      DisplayFormat = '#,###.##'
    end
  end
  object DsQMinPrezziTot: TDataSource
    DataSet = QMinPrezziTot
    Left = 168
    Top = 360
  end
  object QListinoEdiz: TADOLinkedQuery
    Connection = Data.DB
    CursorType = ctStatic
    AfterOpen = QListinoEdiz_OLDAfterOpen
    AfterScroll = QListinoEdiz_OLDAfterScroll
    OnCalcFields = QListinoEdiz_OLDCalcFields
    Parameters = <
      item
        Name = 'IDEdizione:'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select Ann_EdizListino.*,'
      '       Ann_ListinoClienti.* '
      
        'from Ann_EdizListino left join Ann_ListinoClienti on Ann_EdizLis' +
        'tino.ID = Ann_ListinoClienti.IDEdizListino'
      'where IDEdizione=:IDEdizione:'
      'order by QtaDa,QtaA,dallaData desc')
    MasterDataSet = QEdizioni
    LinkedMasterField = 'ID'
    LinkedDetailField = 'IDEdizione'
    UseFilter = False
    Left = 231
    Top = 153
    object QListinoEdizID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QListinoEdizIDEdizione: TIntegerField
      FieldName = 'IDEdizione'
    end
    object QListinoEdizDallaData: TDateTimeField
      FieldName = 'DallaData'
    end
    object QListinoEdizPrezzoListinoLire: TFloatField
      FieldName = 'PrezzoListinoLire'
      DisplayFormat = '#,###'
    end
    object QListinoEdizPrezzoANoiLire: TFloatField
      FieldName = 'PrezzoANoiLire'
      DisplayFormat = '#,###'
    end
    object QListinoEdizPrezzoAlClienteLire: TFloatField
      FieldName = 'PrezzoAlClienteLire'
      DisplayFormat = '#,###'
    end
    object QListinoEdizPrezzoListinoEuro: TFloatField
      FieldName = 'PrezzoListinoEuro'
      DisplayFormat = '#,###.00'
    end
    object QListinoEdizPrezzoANoiEuro: TFloatField
      FieldName = 'PrezzoANoiEuro'
      DisplayFormat = '#,###.00'
    end
    object QListinoEdizPrezzoAlClienteEuro: TFloatField
      FieldName = 'PrezzoAlClienteEuro'
      DisplayFormat = '#,###.00'
    end
    object QListinoEdizNote: TStringField
      FieldName = 'Note'
      FixedChar = True
      Size = 80
    end
    object QListinoEdizQtaDa: TSmallintField
      FieldName = 'QtaDa'
    end
    object QListinoEdizQtaA: TSmallintField
      FieldName = 'QtaA'
    end
    object QListinoEdizDiffLire: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'DiffLire'
      DisplayFormat = '#,###'
      Calculated = True
    end
    object QListinoEdizDiffEuro: TFloatField
      FieldKind = fkCalculated
      FieldName = 'DiffEuro'
      DisplayFormat = '#,###.00'
      Calculated = True
    end
    object QListinoEdizID_1: TAutoIncField
      FieldName = 'ID_1'
      ReadOnly = True
    end
    object QListinoEdizPrezzoQuestoClienteLire: TFloatField
      FieldName = 'PrezzoQuestoClienteLire'
      DisplayFormat = '#,###'
    end
    object QListinoEdizPrezzoQuestoClienteEuro: TFloatField
      FieldName = 'PrezzoQuestoClienteEuro'
      DisplayFormat = '#,###.00'
    end
    object QListinoEdizIDCliente: TIntegerField
      FieldName = 'IDCliente'
    end
  end
  object QEdizioni: TADOLinkedQuery
    Connection = Data.DB
    CursorType = ctStatic
    AfterScroll = QEdizioniAfterScroll
    OnCalcFields = QEdizioni_OLDCalcFields
    Parameters = <>
    SQL.Strings = (
      
        'select distinct Ann_Edizioni.ID IDEdizione, Ann_Edizioni.IDTesta' +
        'ta,'
      '       Ann_Testate.Denominazione Testata, NomeEdizione Edizione'
      'from Ann_Testate,Ann_Edizioni'
      'where Ann_Testate.ID=Ann_Edizioni.IDTestata'
      'order by Ann_Testate.Denominazione,NomeEdizione')
    UseFilter = False
    Left = 48
    Top = 128
    object QEdizioniIDEdizione: TAutoIncField
      FieldName = 'IDEdizione'
      ReadOnly = True
    end
    object QEdizioniIDTestata: TIntegerField
      FieldName = 'IDTestata'
    end
    object QEdizioniTestata: TStringField
      FieldName = 'Testata'
      FixedChar = True
      Size = 50
    end
    object QEdizioniTestataEdizione: TStringField
      DisplayWidth = 50
      FieldKind = fkCalculated
      FieldName = 'TestataEdizione'
      Size = 50
      Calculated = True
    end
    object QEdizioniEdizione: TStringField
      FieldName = 'Edizione'
      FixedChar = True
      Size = 50
    end
  end
  object QMinPrezziTestata: TADOLinkedQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select min(PrezzoListinoLire) minPrezzoListinoLire,'
      '       min(PrezzoANoiLire) minPrezzoANoiLire,'
      '       min(PrezzoAlClienteLire) minPrezzoAlClienteLire,'
      '       min(PrezzoListinoEuro) minPrezzoListinoEuro,'
      '       min(PrezzoANoiEuro) minPrezzoANoiLireEuro,'
      '       min(PrezzoAlClienteEuro) minPrezzoAlClienteEuro'
      'from Ann_EdizListino, Ann_Edizioni'
      'where IDEdizione = Ann_Edizioni.ID')
    MasterDataSet = QEdizioni
    LinkedMasterField = 'QEdizioni.IDTestata'
    LinkedDetailField = 'IDTestata'
    OriginalSQL.Strings = (
      'select min(PrezzoListinoLire) minPrezzoListinoLire,'
      '       min(PrezzoANoiLire) minPrezzoANoiLire,'
      '       min(PrezzoAlClienteLire) minPrezzoAlClienteLire,'
      '       min(PrezzoListinoEuro) minPrezzoListinoEuro,'
      '       min(PrezzoANoiEuro) minPrezzoANoiLireEuro,'
      '       min(PrezzoAlClienteEuro) minPrezzoAlClienteEuro'
      'from Ann_EdizListino, Ann_Edizioni'
      'where IDEdizione = Ann_Edizioni.ID'
      '  and Ann_Edizioni.IDTestata = :IDTestata:')
    UseFilter = False
    Left = 104
    Top = 328
    object QMinPrezziTestataminPrezzoListinoLire: TFloatField
      FieldName = 'minPrezzoListinoLire'
      ReadOnly = True
    end
    object QMinPrezziTestataminPrezzoANoiLire: TFloatField
      FieldName = 'minPrezzoANoiLire'
      ReadOnly = True
    end
    object QMinPrezziTestataminPrezzoAlClienteLire: TFloatField
      FieldName = 'minPrezzoAlClienteLire'
      ReadOnly = True
    end
    object QMinPrezziTestataminPrezzoListinoEuro: TFloatField
      FieldName = 'minPrezzoListinoEuro'
      ReadOnly = True
    end
    object QMinPrezziTestataminPrezzoANoiLireEuro: TFloatField
      FieldName = 'minPrezzoANoiLireEuro'
      ReadOnly = True
    end
    object QMinPrezziTestataminPrezzoAlClienteEuro: TFloatField
      FieldName = 'minPrezzoAlClienteEuro'
      ReadOnly = True
    end
  end
  object QMinPrezziTot: TADOLinkedQuery
    Connection = Data.DB
    Parameters = <>
    SQL.Strings = (
      'select min(PrezzoListinoLire) minPrezzoListinoLire,'
      '       min(PrezzoANoiLire) minPrezzoANoiLire,'
      '       min(PrezzoAlClienteLire) minPrezzoAlClienteLire,'
      '       min(PrezzoListinoEuro) minPrezzoListinoEuro,'
      '       min(PrezzoANoiEuro) minPrezzoANoiLireEuro,'
      '       min(PrezzoAlClienteEuro) minPrezzoAlClienteEuro'
      'from Ann_EdizListino')
    UseFilter = False
    Left = 104
    Top = 360
    object QMinPrezziTotminPrezzoListinoLire: TFloatField
      FieldName = 'minPrezzoListinoLire'
      ReadOnly = True
    end
    object QMinPrezziTotminPrezzoANoiLire: TFloatField
      FieldName = 'minPrezzoANoiLire'
      ReadOnly = True
    end
    object QMinPrezziTotminPrezzoAlClienteLire: TFloatField
      FieldName = 'minPrezzoAlClienteLire'
      ReadOnly = True
    end
    object QMinPrezziTotminPrezzoListinoEuro: TFloatField
      FieldName = 'minPrezzoListinoEuro'
      ReadOnly = True
    end
    object QMinPrezziTotminPrezzoANoiLireEuro: TFloatField
      FieldName = 'minPrezzoANoiLireEuro'
      ReadOnly = True
    end
    object QMinPrezziTotminPrezzoAlClienteEuro: TFloatField
      FieldName = 'minPrezzoAlClienteEuro'
      ReadOnly = True
    end
  end
end
