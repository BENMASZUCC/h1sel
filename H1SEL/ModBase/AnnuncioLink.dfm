object AnnuncioLinkForm: TAnnuncioLinkForm
  Left = 244
  Top = 235
  BorderStyle = bsDialog
  Caption = 'Link per form autocandidatura'
  ClientHeight = 46
  ClientWidth = 671
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object eLink: TEdit
    Left = 2
    Top = 1
    Width = 667
    Height = 21
    ReadOnly = True
    TabOrder = 0
    Text = 'eLink'
  end
  object bCopia: TButton
    Left = 581
    Top = 24
    Width = 89
    Height = 21
    Caption = 'Copia e chiudi'
    TabOrder = 1
    OnClick = bCopiaClick
  end
end
