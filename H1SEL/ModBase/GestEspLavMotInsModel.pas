unit GestEspLavMotInsModel;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     dxDBTLCl, dxGrClms, dxTL, dxDBCtrl, dxDBGrid, dxCntner, Db, ADODB,
     StdCtrls, Buttons, ExtCtrls, TB97;

type
     TGestEspLavMotInsModelForm = class(TForm)
          Panel1: TPanel;
          QModel: TADOQuery;
          DsQModel: TDataSource;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1Voce: TdxDBGridMaskColumn;
          dxDBGrid1Val1Dic: TdxDBGridMaskColumn;
          dxDBGrid1Val2Dic: TdxDBGridMaskColumn;
          dxDBGrid1Val3Dic: TdxDBGridMaskColumn;
          dxDBGrid1Val4Dic: TdxDBGridMaskColumn;
          dxDBGrid1Val5Dic: TdxDBGridMaskColumn;
          dxDBGrid1Val1Desc: TdxDBGridBlobColumn;
          dxDBGrid1Val2Desc: TdxDBGridBlobColumn;
          dxDBGrid1Val3Desc: TdxDBGridBlobColumn;
          dxDBGrid1Val4Desc: TdxDBGridBlobColumn;
          dxDBGrid1Val5Desc: TdxDBGridBlobColumn;
          dxDBGrid1DescVoce: TdxDBGridBlobColumn;
          Label1: TLabel;
          Panel2: TPanel;
          BitBtn1: TToolbarButton97;
          ToolbarButton971: TToolbarButton97;
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
          procedure BitBtn1Click(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure ToolbarButton971Click(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     GestEspLavMotInsModelForm: TGestEspLavMotInsModelForm;

implementation

uses ModuloDati, GestQualifContr, Main;

{$R *.DFM}

procedure TGestEspLavMotInsModelForm.FormClose(Sender: TObject;
     var Action: TCloseAction);
begin
     //if DsQModel.state in [dsinsert, dsedit] then QModel.Post;
end;

procedure TGestEspLavMotInsModelForm.BitBtn1Click(Sender: TObject);
begin
     if DsQModel.state in [dsinsert, dsedit] then QModel.Post;
     GestEspLavMotInsModelForm.ModalResult := mrOK;
end;

procedure TGestEspLavMotInsModelForm.FormShow(Sender: TObject);
begin
     //Grafica
     GestEspLavMotInsModelForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.Font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
end;

procedure TGestEspLavMotInsModelForm.ToolbarButton971Click(
     Sender: TObject);
begin
     QModel.Cancel;
     GestEspLavMotInsModelForm.ModalResult := mrCancel;
end;

end.

