unit Offerte;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Mask, DBCtrls, Db, RXDBCtrl, ExtCtrls, DBTables, Grids,
     DBGrids, Buttons, TB97, dxDBTLCl, dxGrClms, dxTL, dxDBCtrl, dxDBGrid,
     dxCntner, dxGridMenus, Menus, dxPSCore, dxPSdxTLLnk, dxPSdxDBGrLnk,
     dxPSdxDBCtrlLnk, ADODB, U_ADOLinkCl, uUtilsVarie;

type
     TOfferteForm = class(TForm)
          QOfferte_OLD: TQuery;
          DsQOfferte: TDataSource;
          QOfferte_OLDID: TAutoIncField;
          QOfferte_OLDIDCliente: TIntegerField;
          QOfferte_OLDRif: TStringField;
          QOfferte_OLDData: TDateTimeField;
          QOfferte_OLDAMezzo: TStringField;
          QOfferte_OLDAttenzioneDi: TStringField;
          QOfferte_OLDAnticipoRichiesto: TFloatField;
          QOfferte_OLDCondizioni: TStringField;
          QOfferte_OLDEsito: TStringField;
          QOfferte_OLDNote: TStringField;
          QOfferte_OLDStato: TStringField;
          QOfferte_OLDTipo: TStringField;
          QOfferte_OLDIDUtente: TIntegerField;
          QOfferte_OLDImportoTotale: TFloatField;
          QOfferte_OLDCliente: TStringField;
          Panel4: TPanel;
          ToolbarButton9723: TToolbarButton97;
          QOfferte_OLDIDRicerca: TIntegerField;
          QOfferte_OLDIDContratto: TIntegerField;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1ID: TdxDBGridMaskColumn;
          dxDBGrid1IDCliente: TdxDBGridMaskColumn;
          dxDBGrid1Rif: TdxDBGridMaskColumn;
          dxDBGrid1Data: TdxDBGridDateColumn;
          dxDBGrid1AMezzo: TdxDBGridMaskColumn;
          dxDBGrid1AttenzioneDi: TdxDBGridMaskColumn;
          dxDBGrid1Condizioni: TdxDBGridMaskColumn;
          dxDBGrid1Esito: TdxDBGridMaskColumn;
          dxDBGrid1Note: TdxDBGridMaskColumn;
          dxDBGrid1Stato: TdxDBGridMaskColumn;
          dxDBGrid1Tipo: TdxDBGridMaskColumn;
          dxDBGrid1IDUtente: TdxDBGridMaskColumn;
          dxDBGrid1Cliente: TdxDBGridMaskColumn;
          dxDBGrid1IDRicerca: TdxDBGridMaskColumn;
          dxDBGrid1IDContratto: TdxDBGridMaskColumn;
          dxDBGrid1ImportoTotale: TdxDBGridCurrencyColumn;
          ToolbarButton971: TToolbarButton97;
          PMStampa: TPopupMenu;
          stampagriglia1: TMenuItem;
          esportainExcel1: TMenuItem;
          esportainHTML1: TMenuItem;
          dxPrinter1: TdxComponentPrinter;
          dxPrinter1Link1: TdxDBGridReportLink;
          dxDBGrid1LineaProdotto: TdxDBGridColumn;
          QOfferte_OLDIDLineaProdotto: TIntegerField;
          QOfferte_OLDLineaProdotto: TStringField;
          QOfferte_OLDIDArea: TIntegerField;
          QOfferte_OLDArea: TStringField;
          dxDBGrid1Area: TdxDBGridColumn;
          QOfferte: TADOLinkedQuery;
          QOfferteID: TAutoIncField;
          QOfferteIDCliente: TIntegerField;
          QOfferteRif: TStringField;
          QOfferteData: TDateTimeField;
          QOfferteAMezzo: TStringField;
          QOfferteAttenzioneDi: TStringField;
          QOfferteAnticipoRichiesto: TFloatField;
          QOfferteCondizioni: TStringField;
          QOfferteEsito: TStringField;
          QOfferteNote: TStringField;
          QOfferteStato: TStringField;
          QOfferteTipo: TStringField;
          QOfferteIDUtente: TIntegerField;
          QOfferteImportoTotale: TFloatField;
          QOfferteIDRicerca: TIntegerField;
          QOfferteIDContratto: TIntegerField;
          QOfferteIDContrattoCG: TIntegerField;
          QOfferteIDArea: TIntegerField;
          QOfferteIDLineaProdotto: TIntegerField;
          QOfferteCliente: TStringField;
          QOfferteLineaProdotto: TStringField;
          QOfferteArea: TStringField;
          QUsers: TADOLinkedQuery;
          QUsersID: TAutoIncField;
          QUsersNominativo: TStringField;
          QOfferteSoloAccettati: TFloatField;
          dxDBGrid1SoloAccettati: TdxDBGridCurrencyColumn;
          QOfferteSedeOperativa: TStringField;
          dxDBGrid1Column20: TdxDBGridColumn;
          QOfferteComune: TStringField;
          QOfferteProvincia: TStringField;
          dxDBGrid1ColumnComune: TdxDBGridColumn;
          dxDBGrid1ColumnProvincia: TdxDBGridColumn;
          QOfferteTramite: TStringField;
          dxDBGrid1ConoscTramite: TdxDBGridColumn;
          QOfferteData_acc: TDateTimeField;
          dxDBGrid1Column24: TdxDBGridColumn;
          QOffertePrimaVendita: TBooleanField;
          QOfferteUpSelling: TBooleanField;
          dxDBGrid1Column25: TdxDBGridCheckColumn;
          dxDBGrid1Column26: TdxDBGridCheckColumn;
          QOfferteTotaleCalc: TFloatField;
          QOfferteTotProdServ: TFloatField;
          BitBtn1: TToolbarButton97;
          QOfferteAreaRuolo: TStringField;
          QOfferteRuolo: TStringField;
          dxDBGrid1AreaRuolo: TdxDBGridColumn;
          dxDBGrid1Ruolo: TdxDBGridColumn;
          QOfferteIDCondizione: TIntegerField;
          QOfferteCondizione: TStringField;
          dxDBGrid1Condizione: TdxDBGridColumn;
          QOffertePercCondizione: TSmallintField;
          QOfferteTotCalcPerc: TFloatField;
          dxDBGrid1TotCalcPerc: TdxDBGridCurrencyColumn;
          N1: TMenuItem;
          Ripristinaimpostazionigriglia1: TMenuItem;
          QOfferteDataUltimoContatto: TDateTimeField;
          dxDBGrid1Column31: TdxDBGridDateColumn;
          QTemp: TADOQuery;
          QOfferteUtente: TStringField;
          dxDBGrid1Column32: TdxDBGridColumn;
          QOfferteDataPrevChiusura: TDateField;
          dxDBGrid1Column33: TdxDBGridColumn;
          QOfferteTotCalcPerc_p: TFloatField;
          dxDBGrid1Column34: TdxDBGridColumn;
    QOfferteTitoloCliente: TStringField;
    dxDBGrid1Column35: TdxDBGridColumn;
          procedure FormShow(Sender: TObject);
          procedure BitBtn1Click(Sender: TObject);
          procedure ToolbarButton9723Click(Sender: TObject);
          procedure dxDBGrid1MouseUp(Sender: TObject; Button: TMouseButton;
               Shift: TShiftState; X, Y: Integer);
          procedure dxDBGrid1CustomDrawCell(Sender: TObject; ACanvas: TCanvas;
               ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
               ASelected, AFocused, ANewItemRow: Boolean; var AText: string;
               var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
               var ADone: Boolean);
          procedure dxDBGrid1CustomDrawFooter(Sender: TObject; ACanvas: TCanvas;
               ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
               var AText: string; var AColor: TColor; AFont: TFont;
               var AAlignment: TAlignment; var ADone: Boolean);
          procedure stampagriglia1Click(Sender: TObject);
          procedure esportainExcel1Click(Sender: TObject);
          procedure esportainHTML1Click(Sender: TObject);
          procedure QOfferteCalcFields(DataSet: TDataSet);
          procedure Ripristinaimpostazionigriglia1Click(Sender: TObject);
     private
          xColumn: TColumn;
     public
          xIDUtente: integer;
     end;

var
     OfferteForm: TOfferteForm;

implementation

uses Main, OffertaCliente, ModuloDati, SelCliente, CreaCompensi;

{$R *.DFM}

procedure TOfferteForm.FormShow(Sender: TObject);
begin
     //Grafica
     OfferteForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel4.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel4.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel4.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     Caption := '[M/172] - ' + Caption;
     //QUsers.ParamByName('xOggi').asDateTime:=Date;
     QUsers.Open;
     QUsers.Locate('ID', xIDUtente, []);
     // filtro su presentate
     if qOfferte.Active = false then qOfferte.Open;
     dxDBGrid1.Filter.Add(dxDBGrid1Stato, 'presentata', 'presentata');

     if pos('EBC', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) > 0 then begin
          dxDBGrid1Condizione.visible := True;
          dxDBGrid1TotCalcPerc.visible := True;
     end;
end;

procedure TOfferteForm.BitBtn1Click(Sender: TObject);
begin
     //close;
     OfferteForm.ModalResult := mrok;
end;

procedure TOfferteForm.ToolbarButton9723Click(Sender: TObject);
var xStato, xVecchioStato: string;
     i, xIDOfferta, xID: integer;
     Vero: boolean;
begin
     if not mainForm.CheckProfile('144') then Exit;
     xVecchioStato := QOfferteStato.Value;
     OffertaClienteForm := TOffertaClienteForm.create(self);
     OffertaClienteForm.ERif.Text := QOfferteRif.AsString;
     OffertaClienteForm.DEData.Date := QOfferteData.Value;
     OffertaClienteForm.CBAMezzo.Text := QOfferteAMezzo.Value;
     OffertaClienteForm.EAttenzioneDi.Text := QOfferteAttenzioneDi.Value;
     OffertaClienteForm.ENote.text := QOfferteNote.Value;
     OffertaClienteForm.ECondizioni.Text := QOfferteCondizioni.Value;
     OffertaClienteForm.xIDUtente := QOfferteIDUtente.Value;
     OffertaClienteForm.CBStato.text := QOfferteStato.Value;
     OffertaClienteForm.Panel1.Visible := False;
     xStato := QOfferteStato.Value;
     OffertaClienteForm.xIDContratto := QOfferteIDContratto.Value;
     OffertaClienteForm.xIDCliente := QOfferteIDCliente.Value;
     if QOfferteIDRicerca.asString <> '' then
          OffertaClienteForm.xIDRicerca := QOfferteIDRicerca.Value;
     OffertaClienteForm.xIDOfferta := QOfferteID.Value;
     OffertaClienteForm.PanOfferta.enabled := False;
     if pos('EBC', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) > 0 then begin
          // Borsari 30/08: Andrea deve poter modificare la condizione ...
          if QOfferteIDCondizione.AsString <> '' then begin
               OffertaClienteForm.xIDCondizione := QOfferteIDCondizione.AsInteger;
               QTemp.Close;
               QTemp.SQL.text := 'select Condizione from EBC_OfferteTabCondizioni where ID=' + QOfferteIDCondizione.asString;
               QTemp.Open;
               OffertaClienteForm.ECondizione.text := QTemp.FieldByName('Condizione').asString;
          end;
          // Borsari 16/12/2013: anche la data prevista chiusura
          OffertaClienteForm.DEDataPrevChiusura.Enabled := True;
     end else begin
          OffertaClienteForm.BitBtn1.Visible := False;
          OffertaClienteForm.BitBtn2.Caption := 'Esci';
          OffertaClienteForm.DEDataPrevChiusura.Enabled := False;
     end;

     if QOfferteDataPrevChiusura.asString <> '' then
          OffertaClienteForm.DEDataPrevChiusura.Date := QOfferteDataPrevChiusura.Value;

     OffertaClienteForm.Showmodal;
     if OffertaClienteForm.ModalResult = mrOK then begin
          if pos('EBC', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) > 0 then begin
               // Borsari 30/08: Andrea deve poter modificare la condizione ...
               QTemp.Close;
               QTemp.SQL.clear;
               QTemp.SQL.text := 'update EBC_Offerte set IDCondizione=:x0, DataPrevChiusura=:x1 where ID=' + QOfferteID.AsString;
               QTemp.Parameters[0].value := OffertaClienteForm.xIDCondizione;
               if (OffertaClienteForm.DEDataPrevChiusura.Date = 0) or
                    (OffertaClienteForm.DEDataPrevChiusura.Text = '') then
                    QTemp.Parameters[1].value := null
               else
                    QTemp.Parameters[1].value := OffertaClienteForm.DEDataPrevChiusura.Date;
               QTemp.ExecSQL;

               xID := QOfferteID.Value;
               QOfferte.Close;
               QOfferte.Open;
               QOfferte.Locate('ID', xID, []);
          end;
     end;
     OffertaClienteForm.Free;
end;

procedure TOfferteForm.dxDBGrid1MouseUp(Sender: TObject;
     Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
     if (Button <> mbRight) or (Shift <> []) then Exit;
     TdxDBGridPopupMenuManager.Instance.ShowGridPopupMenu(Sender as TdxDBGrid);
end;

procedure TOfferteForm.dxDBGrid1CustomDrawCell(Sender: TObject;
     ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
     AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
     var AText: string; var AColor: TColor; AFont: TFont;
     var AAlignment: TAlignment; var ADone: Boolean);
begin
     if not MainForm.CheckProfile('147', false) then begin
          // l'utente non pu� visualizzare gli importi di TUTTE le offerte
          if AColumn = dxDBGrid1ImportoTotale then begin
               dxDBGrid1ImportoTotale.Visible := False;
               dxDBGrid1ImportoTotale.DisableCustomizing := False;
          end;
     end;
     if (not MainForm.CheckProfile('148', false)) and
          (QUsersID.Value <> QOfferteIDUtente.Value) then begin
          // l'utente non pu� visualizzare gli importi delle offerte degli ALTRI UTENTI
          if AColumn = dxDBGrid1ImportoTotale then begin
               dxDBGrid1ImportoTotale.Visible := False;
               dxDBGrid1ImportoTotale.DisableCustomizing := False;
          end;
     end;
end;

procedure TOfferteForm.dxDBGrid1CustomDrawFooter(Sender: TObject;
     ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
     AColumn: TdxTreeListColumn; var AText: string; var AColor: TColor;
     AFont: TFont; var AAlignment: TAlignment; var ADone: Boolean);
begin
     AFont.Style := [fsBold];
end;

procedure TOfferteForm.stampagriglia1Click(Sender: TObject);
begin
     dxPrinter1.Preview(True, dxPrinter1Link1);
end;

procedure TOfferteForm.esportainExcel1Click(Sender: TObject);
begin
     Mainform.Save('xls', 'Microsoft Excel 4.0 Worksheet (*.xls)|*.xls', 'ExpGrid.xls', dxDBGrid1.SaveToXLS);
end;

procedure TOfferteForm.esportainHTML1Click(Sender: TObject);
begin
     Mainform.Save('htm', 'HTML File (*.htm; *.html)|*.htm', 'ExpGrid.htm', dxDBGrid1.SaveToHTML);
end;

procedure TOfferteForm.QOfferteCalcFields(DataSet: TDataSet);
begin
     QOfferteTotaleCalc.Value := QOfferteImportoTotale.value + QOfferteTotProdServ.value;
     if QOffertePercCondizione.asString = '' then
          QOfferteTotCalcPerc.value := QOfferteTotaleCalc.Value
     else QOfferteTotCalcPerc.value := QOfferteTotaleCalc.Value * QOffertePercCondizione.value / 100;

     if QOfferteDataPrevChiusura.asString <> '' then
          QOfferteTotCalcPerc_p.value := QOfferteTotCalcPerc.Value
     else QOfferteTotCalcPerc_p.value := 0;

end;

procedure TOfferteForm.Ripristinaimpostazionigriglia1Click(Sender: TObject);
begin
     CancellaRegistry('DBGOfferte');
     dxDBGrid1.RegistryPath := '';
end;

end.

