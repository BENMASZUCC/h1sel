unit CompInserite;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, ExtCtrls, Grids, Aligrid, dxCntner, dxTL, dxDBCtrl,
     dxDBGrid, BaseGrid, AdvGrid;

type
     TCompInseriteForm = class(TForm)
          BitBtn1: TBitBtn;
          Panel1: TPanel;
          Label2: TLabel;
          ERuolo: TEdit;
          Panel73: TPanel;
          BitBtn2: TBitBtn;
          Label1: TLabel;
          SG1: TAdvStringGrid;
          procedure BitBtn2Click(Sender: TObject);
          procedure SG1GetEditMask(Sender: TObject; ACol, ARow: Integer;
               var Value: string);
          procedure SG1SelectCell(Sender: TObject; ACol, ARow: Integer;
               var CanSelect: Boolean);
          procedure SG1DblClick(Sender: TObject);
          procedure BitBtn1Click(Sender: TObject);
          procedure FormShow(Sender: TObject);
     private
          xSelRow, xSelCol: integer;
          { Private declarations }
     public
          { Public declarations }
     end;

var
     CompInseriteForm: TCompInseriteForm;

implementation

{$R *.DFM}


procedure TCompInseriteForm.BitBtn2Click(Sender: TObject);
begin
     if MessageDlg('Sei sicuro di voler annullare l''inserimento di tutte le competenze ?',
          mtWarning, [mbYes, mbNo], 0) = mrNo then begin
          ModalResult := mrNone;
          Abort;
     end;
end;

procedure TCompInseriteForm.SG1GetEditMask(Sender: TObject; ACol,
     ARow: Integer; var Value: string);
begin
     if (ACol = 1) and (ARow > 0) then Value := '##';
end;

procedure TCompInseriteForm.SG1SelectCell(Sender: TObject; ACol,
     ARow: Integer; var CanSelect: Boolean);
begin
     xSelRow := aRow;
     xSelCol := aCol;
end;

procedure TCompInseriteForm.SG1DblClick(Sender: TObject);
var k: integer;
begin
     if (xSelCol = 2) and (xSelRow > 0) then
          if SG1.Cells[xSelCol, xSelRow] = '�' then begin
               SG1.Cells[xSelCol, xSelRow] := 'x';
               SG1.RowColor[xSelRow] := clRed;
          end else begin
               SG1.Cells[xSelCol, xSelRow] := '�';
               SG1.RowColor[xSelRow] := clLime;
          end;
end;

procedure TCompInseriteForm.BitBtn1Click(Sender: TObject);
var k: integer;
     s: string;
begin
     for k := 1 to SG1.RowCount - 1 do begin
          //quando cambiato il compoinente in advstringgrid non va questo controllo perch� la propriet� non � leggibile ma solo in scrittura
          {if SG1.RowColor[k] = clLime then begin
               s := trim(SG1.Cells[1, k]);
               SG1.Cells[1, k] := s;
               if s = '' then s := '0';
               if StrToInt(s) > 10 then begin
                    MessageDlg('Valori delle competenze non corretti', mtError, [mbOK], 0);
                    ModalResult := mrNone;
                    Abort;
          end; }
          if SG1.Cells[2, k] = '�' then begin
               s := trim(SG1.Cells[1, k]);
               SG1.Cells[1, k] := s;
               if s = '' then s := '0';
               if StrToInt(s) > 10 then begin
                    MessageDlg('Valori delle competenze non corretti', mtError, [mbOK], 0);
                    ModalResult := mrNone;
                    Abort;
               end;
          end;
     end;
end;

procedure TCompInseriteForm.FormShow(Sender: TObject);
begin
     Caption := '[S/17] - ' + Caption;
end;

end.

