unit FrmCampiPers;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ExtCtrls, dxCntner, dxTL, dxDBCtrl, dxDBGrid, Db, ADODB, Grids, DBGrids,
     StdCtrls, Mask, DBCtrls, dxExEdtr, dxEdLib, dxDBELib, dxEditor, TB97,
     Buttons, CheckPass;

type
     TCampiPersFrame = class(TFrame)
          QCampiPers: TADOQuery;
          DsQCampiPers: TDataSource;
          Q: TADOQuery;
          PanTop: TPanel;
          Pan1: TPanel;
          Label1: TLabel;
          dxDBEdit1: TdxDBEdit;
          dxDBDateEdit1: TdxDBDateEdit;
          dxDBCheckEdit1: TdxDBCheckEdit;
          dxDBSpinEdit1: TdxDBSpinEdit;
          Pan2: TPanel;
          Label2: TLabel;
          dxDBEdit2: TdxDBEdit;
          dxDBDateEdit2: TdxDBDateEdit;
          dxDBCheckEdit2: TdxDBCheckEdit;
          dxDBSpinEdit2: TdxDBSpinEdit;
          Pan3: TPanel;
          Label3: TLabel;
          dxDBEdit3: TdxDBEdit;
          dxDBDateEdit3: TdxDBDateEdit;
          dxDBCheckEdit3: TdxDBCheckEdit;
          dxDBSpinEdit3: TdxDBSpinEdit;
          TCampiPers: TADOTable;
          BOK: TToolbarButton97;
          BCan: TToolbarButton97;
          Pan4: TPanel;
          Label4: TLabel;
          dxDBEdit4: TdxDBEdit;
          dxDBDateEdit4: TdxDBDateEdit;
          dxDBCheckEdit4: TdxDBCheckEdit;
          dxDBSpinEdit4: TdxDBSpinEdit;
          Pan5: TPanel;
          Label5: TLabel;
          dxDBEdit5: TdxDBEdit;
          dxDBDateEdit5: TdxDBDateEdit;
          dxDBCheckEdit5: TdxDBCheckEdit;
          dxDBSpinEdit5: TdxDBSpinEdit;
          Pan6: TPanel;
          Label6: TLabel;
          dxDBEdit6: TdxDBEdit;
          dxDBDateEdit6: TdxDBDateEdit;
          dxDBCheckEdit6: TdxDBCheckEdit;
          dxDBSpinEdit6: TdxDBSpinEdit;
          Pan7: TPanel;
          Label7: TLabel;
          dxDBEdit7: TdxDBEdit;
          dxDBDateEdit7: TdxDBDateEdit;
          dxDBCheckEdit7: TdxDBCheckEdit;
          dxDBSpinEdit7: TdxDBSpinEdit;
          Pan8: TPanel;
          Label8: TLabel;
          dxDBEdit8: TdxDBEdit;
          dxDBDateEdit8: TdxDBDateEdit;
          dxDBCheckEdit8: TdxDBCheckEdit;
          dxDBSpinEdit8: TdxDBSpinEdit;
          Pan9: TPanel;
          Label9: TLabel;
          dxDBEdit9: TdxDBEdit;
          dxDBDateEdit9: TdxDBDateEdit;
          dxDBCheckEdit9: TdxDBCheckEdit;
          dxDBSpinEdit9: TdxDBSpinEdit;
          Pan10: TPanel;
          Label10: TLabel;
          dxDBEdit10: TdxDBEdit;
          dxDBDateEdit10: TdxDBDateEdit;
          dxDBCheckEdit10: TdxDBCheckEdit;
          dxDBSpinEdit10: TdxDBSpinEdit;
          ToolbarButton971: TToolbarButton97;
          Qattivitaformaz: TADOQuery;
          QattivitaformazID: TAutoIncField;
          QattivitaformazIDProgettoForm: TIntegerField;
          QattivitaformazCodiceCorso: TStringField;
          QattivitaformazInterno: TBooleanField;
          QattivitaformazIDArea: TIntegerField;
          QattivitaformazIDResponsabile: TIntegerField;
          QattivitaformazIDEnteFormaz: TIntegerField;
          QattivitaformazIDTutor: TIntegerField;
          QattivitaformazCodiceArgomento: TStringField;
          QattivitaformazLivello: TStringField;
          QattivitaformazAttiva: TBooleanField;
          QattivitaformazStato: TStringField;
          QattivitaformazTipo: TStringField;
          QattivitaformazDataInizio: TDateTimeField;
          QattivitaformazDataFine: TDateTimeField;
          QattivitaformazPeriodoPrevisto: TStringField;
          QattivitaformazDurataPrevista: TStringField;
          QattivitaformazOrarioDalle: TDateTimeField;
          QattivitaformazOrarioAlle: TDateTimeField;
          QattivitaformazDescrizione: TStringField;
          QattivitaformazLuogoSvolgimento: TStringField;
          QattivitaformazProgramma: TMemoField;
          QattivitaformazNote: TMemoField;
          QattivitaformazNumMinPartec: TIntegerField;
          QattivitaformazNumMaxPartec: TIntegerField;
          QattivitaformazTotOreLavoro: TIntegerField;
          QattivitaformazTotOreExtra: TIntegerField;
          QattivitaformazImportoFinanziatoLire: TFloatField;
          QattivitaformazImportoFinanziatoEuro: TFloatField;
          QattivitaformazCostoOraLavorative: TFloatField;
          QattivitaformazCostoOraStraord: TFloatField;
          QattivitaformazCostoOraLavFestiva: TFloatField;
          QattivitaformazNumProgetto: TStringField;
          QattivitaformazFondoSociale: TBooleanField;
          QattivitaformazIDMacroGruppo: TIntegerField;
          QattivitaformazTotOrePrev: TIntegerField;
          QattivitaformazCrediti: TSmallintField;
          QattivitaformazIDQuest: TIntegerField;
          QattivitaformazIDFormTipiAtt: TIntegerField;
          QattivitaformazSoloAnagrafica: TBooleanField;
          QattivitaformazTutorCognome: TStringField;
          QattivitaformazTutorNome: TStringField;
          Qattivitaformazidcorso: TAutoIncField;
          ToolbarButton972: TToolbarButton97;
          dxDBMemo1: TdxDBMemo;
          dxDBMemo2: TdxDBMemo;
          dxDBMemo3: TdxDBMemo;
          dxDBMemo4: TdxDBMemo;
          dxDBMemo5: TdxDBMemo;
          dxDBMemo6: TdxDBMemo;
          dxDBMemo7: TdxDBMemo;
          dxDBMemo8: TdxDBMemo;
          dxDBMemo9: TdxDBMemo;
          dxDBMemo10: TdxDBMemo;
          Pan11: TPanel;
          Label11: TLabel;
          dxDBEdit11: TdxDBEdit;
          dxDBDateEdit11: TdxDBDateEdit;
          dxDBCheckEdit11: TdxDBCheckEdit;
          dxDBSpinEdit11: TdxDBSpinEdit;
          dxDBMemo11: TdxDBMemo;
    Pan12: TPanel;
    Label12: TLabel;
    dxDBEdit12: TdxDBEdit;
    dxDBDateEdit12: TdxDBDateEdit;
    dxDBCheckEdit12: TdxDBCheckEdit;
    dxDBSpinEdit12: TdxDBSpinEdit;
    dxDBMemo12: TdxDBMemo;
          procedure BOKClick(Sender: TObject);
          procedure BCanClick(Sender: TObject);
          procedure DsQCampiPersStateChange(Sender: TObject);
          procedure TCampiPersBeforeClose(DataSet: TDataSet);
          procedure ToolbarButton971Click(Sender: TObject);
          procedure ToolbarButton972Click(Sender: TObject);
     private
     public
          xTipo: string;
          procedure CaricaValori;
          procedure ImpostaCampi;
     end;

implementation

uses ModuloDati, Main, DefCampiPers {,modulodatiformaz}, MDRicerche,
     ModuloDati4, ModuloDati2, InputPassword, EliminaCampoPers, uutilsVarie;

{$R *.DFM}

{ TCampiPersFrame }


procedure TCampiPersFrame.CaricaValori;
begin
     // apri query
     Q.Close;
     if xTipo = 'Anagrafica' then begin
          Q.SQL.text := 'if not exists (select * from AnagCampiPers where IDAnagrafica=:x1) ' +
               'BEGIN insert into AnagCampiPers (IDAnagrafica) values (:x2) END';
          Q.Parameters[0].Value := Data.TAnagraficaID.AsInteger;
          Q.Parameters[1].Value := Data.TAnagraficaID.AsInteger;
          TCampiPers.Close;
          TCampiPers.TableName := 'AnagCampiPers';
          TCampiPers.Filter := 'IDAnagrafica=' + Data.TAnagraficaID.asString;
          TCampiPers.Filtered := True;
          //TCampiPers.Open;
     end;
     if xTipo = 'Formazione' then begin
          Q.SQL.text := 'if not exists (select * from Form_CampiPers where IDCorso=:x1) ' +
               'BEGIN insert into Form_CampiPers (IDCorso) values (:x2) END';
          Q.Parameters[0].Value := QattivitaFormazID.AsInteger;
          Q.Parameters[1].Value := QattivitaFormazID.AsInteger;
          TCampiPers.Close;
          TCampiPers.TableName := 'Form_CampiPers';
          TCampiPers.Filter := 'IDCorso=' + QattivitaFormazID.AsString;
          TCampiPers.Filtered := True;
          //TCampiPers.Open;
     end;
     if xTipo = 'Annunci' then begin
          Q.SQL.text := 'if not exists (select * from Ann_CampiPers where IDAnnunci=:x1) ' +
               'BEGIN insert into Ann_CampiPers (IDAnnunci) values (:x2) END';
          Q.Parameters[0].Value := DataAnnunci.TannunciID.AsInteger;
          Q.Parameters[1].Value := DataAnnunci.TannunciID.AsInteger;
          TCampiPers.Close;
          TCampiPers.TableName := 'Ann_CampiPers';
          TCampiPers.Filter := 'IDAnnunci=' + DataAnnunci.TannunciID.AsString;
          TCampiPers.Filtered := True;
          //TCampiPers.Open;
     end;
     if xTipo = 'Commesse' then begin
          Q.SQL.text := 'if not exists (select * from Comm_CampiPers where IDCommesse=:x1) ' +
               'BEGIN insert into Comm_CampiPers (IDCommesse) values (:x2) END';
          Q.Parameters[0].Value := DataRicerche.QRicAttiveID.AsInteger;
          Q.Parameters[1].Value := DataRicerche.QRicAttiveID.AsInteger;
          TCampiPers.Close;
          TCampiPers.TableName := 'Comm_CampiPers';
          TCampiPers.Filter := 'IDCommesse=' + DataRicerche.QRicAttiveID.AsString;
          TCampiPers.Filtered := True;
          //TCampiPers.Open;
     end;
     if xTipo = 'Organigramma' then begin
          Q.SQL.text := 'if not exists (select * from OrgCampiPers where IDOrganigramma=:x1) ' +
               'BEGIN insert into OrgCampiPers (IDOrganigramma) values (:x2) END';
          Q.Parameters[0].Value := DataRicerche.QOrgNewID.AsInteger;
          Q.Parameters[1].Value := DataRicerche.QOrgNewID.AsInteger;
          TCampiPers.Close;
          TCampiPers.TableName := 'OrgCampiPers';
          TCampiPers.Filter := 'IDOrganigramma=' + DataRicerche.QOrgNewID.AsString;
          TCampiPers.Filtered := True;
          //TCampiPers.Open;
     end;
     if xTipo = 'AnagraficaAziendale' then begin
          Q.SQL.text := 'if not exists (select * from CliCampiPers where IDCliente=:x1) ' +
               'BEGIN insert into CliCampiPers(IDCliente) values (:x2) END';
          Q.Parameters[0].Value := data2.TEBCClientiID.asinteger;
          Q.Parameters[1].Value := data2.TEBCClientiID.asinteger;
          TCampiPers.Close;
          TCampiPers.TableName := 'CliCampiPers';
          TCampiPers.Filter := 'IDCliente=' + data2.TEBCClientiID.AsString;
          TCampiPers.Filtered := True;
          //TCampiPers.Open;
     end;
     if (Q.SQL.Text <> '') and (TCampiPers.TableName <> '') then begin
          Q.ExecSQL;
          TCampiPers.Open;
     end;
end;

procedure TCampiPersFrame.ImpostaCampi;
var xCount, xTop: integer;
begin
     // creazione campi
     Q.Close;
     Q.SQL.text := 'select top 11 * from TabCampiPers where Ambito=:x order by ordine';
     Q.Parameters[0].Value := xTipo;
     Q.Open;
     xCount := 1;
     xTop := 40;
     while not Q.EOF do begin
          case xCount of
               1: begin
                         Pan1.Visible := True;
                         Pan1.Top := xTop;
                         xTop := xTop + 30;
                         Label1.Caption := Q.FieldbyName('LabelCampo').asString;
                         // stringa
                         if Q.FieldbyName('TipoCampo').asString = 'varchar' then begin
                              dxDBEdit1.Visible := True; dxDBDateEdit1.Visible := False; dxDBCheckEdit1.Visible := False; dxDBSpinEdit1.Visible := False; dxDBMemo1.Visible := False;
                              dxDBEdit1.DataField := Q.FieldbyName('NomeCampo').asString;
                              dxDBEdit1.MaxLength := Q.FieldbyName('SizeCampo').asInteger;
                         end;
                         // data
                         if Q.FieldbyName('TipoCampo').asString = 'datetime' then begin
                              dxDBEdit1.Visible := False; dxDBDateEdit1.Visible := True; dxDBCheckEdit1.Visible := False; dxDBSpinEdit1.Visible := False; dxDBMemo1.Visible := False;
                              dxDBDateEdit1.DataField := Q.FieldbyName('NomeCampo').asString;
                         end;
                         // boolean
                         if Q.FieldbyName('TipoCampo').asString = 'boolean' then begin
                              dxDBEdit1.Visible := False; dxDBDateEdit1.Visible := False; dxDBCheckEdit1.Visible := True; dxDBSpinEdit1.Visible := False; dxDBMemo1.Visible := False;
                              dxDBCheckEdit1.DataField := Q.FieldbyName('NomeCampo').asString;
                         end;
                         // intero
                         if Q.FieldbyName('TipoCampo').asString = 'integer' then begin
                              dxDBEdit1.Visible := False; dxDBDateEdit1.Visible := False; dxDBCheckEdit1.Visible := False; dxDBSpinEdit1.Visible := True; dxDBMemo1.Visible := False;
                              dxDBSpinEdit1.DataField := Q.FieldbyName('NomeCampo').asString;
                              dxDBSpinEdit1.ValueType := vtInt;
                         end;
                         // floating point
                         if Q.FieldbyName('TipoCampo').asString = 'float' then begin
                              dxDBEdit1.Visible := False; dxDBDateEdit1.Visible := False; dxDBCheckEdit1.Visible := False; dxDBSpinEdit1.Visible := True; dxDBMemo1.Visible := False;
                              dxDBSpinEdit1.DataField := Q.FieldbyName('NomeCampo').asString;
                              dxDBSpinEdit1.ValueType := vtFloat;
                         end;
                         // text

                         if Q.FieldbyName('TipoCampo').asString = 'text' then begin
                              dxDBEdit1.Visible := False; dxDBDateEdit1.Visible := False; dxDBCheckEdit1.Visible := False; dxDBSpinEdit1.Visible := False; dxDBMemo1.Visible := True;
                              dxDBMemo1.DataField := Q.FieldbyName('NomeCampo').asString;

                              Pan1.Height := 100;
                              xTop := xTop + 70;
                         end;
                    end;
               2: begin
                         Pan2.Visible := True;
                         Pan2.Top := xTop;
                         xTop := xTop + 30;
                         Label2.Caption := Q.FieldbyName('LabelCampo').asString;
                         // stringa
                         if Q.FieldbyName('TipoCampo').asString = 'varchar' then begin
                              dxDBEdit2.Visible := True; dxDBDateEdit2.Visible := False; dxDBCheckEdit2.Visible := False; dxDBSpinEdit2.Visible := False; dxDBMemo2.Visible := False;
                              dxDBEdit2.DataField := Q.FieldbyName('NomeCampo').asString;
                              dxDBEdit2.MaxLength := Q.FieldbyName('SizeCampo').asInteger;
                         end;
                         // data
                         if Q.FieldbyName('TipoCampo').asString = 'datetime' then begin
                              dxDBEdit2.Visible := False; dxDBDateEdit2.Visible := True; dxDBCheckEdit2.Visible := False; dxDBSpinEdit2.Visible := False; dxDBMemo2.Visible := False;
                              dxDBDateEdit2.DataField := Q.FieldbyName('NomeCampo').asString;
                         end;
                         // boolean
                         if Q.FieldbyName('TipoCampo').asString = 'boolean' then begin
                              dxDBEdit2.Visible := False; dxDBDateEdit2.Visible := False; dxDBCheckEdit2.Visible := True; dxDBSpinEdit2.Visible := False; dxDBMemo2.Visible := False;
                              dxDBCheckEdit2.DataField := Q.FieldbyName('NomeCampo').asString;
                         end;
                         // intero
                         if Q.FieldbyName('TipoCampo').asString = 'integer' then begin
                              dxDBEdit2.Visible := False; dxDBDateEdit2.Visible := False; dxDBCheckEdit2.Visible := False; dxDBSpinEdit2.Visible := True; dxDBMemo2.Visible := False;
                              dxDBSpinEdit2.DataField := Q.FieldbyName('NomeCampo').asString;
                              dxDBSpinEdit2.ValueType := vtInt;
                         end;
                         // floating point
                         if Q.FieldbyName('TipoCampo').asString = 'float' then begin
                              dxDBEdit2.Visible := False; dxDBDateEdit2.Visible := False; dxDBCheckEdit2.Visible := False; dxDBSpinEdit2.Visible := True; dxDBMemo2.Visible := False;
                              dxDBSpinEdit2.DataField := Q.FieldbyName('NomeCampo').asString;
                              dxDBSpinEdit2.ValueType := vtFloat;
                         end;
                         if Q.FieldbyName('TipoCampo').asString = 'text' then begin
                              dxDBEdit2.Visible := False; dxDBDateEdit2.Visible := False; dxDBCheckEdit2.Visible := False; dxDBSpinEdit2.Visible := False; dxDBMemo2.Visible := True;
                              dxDBMemo2.DataField := Q.FieldbyName('NomeCampo').asString;

                              Pan2.Height := 100;
                              xTop := xTop + 70;
                         end;
                    end;
               3: begin
                         Pan3.Visible := True;
                         Pan3.Top := xTop;
                         xTop := xTop + 30;
                         Label3.Caption := Q.FieldbyName('LabelCampo').asString;
                         // stringa
                         if Q.FieldbyName('TipoCampo').asString = 'varchar' then begin
                              dxDBEdit3.Visible := True; dxDBDateEdit3.Visible := False; dxDBCheckEdit3.Visible := False; dxDBSpinEdit3.Visible := False; dxDBMemo3.Visible := False;
                              dxDBEdit3.DataField := Q.FieldbyName('NomeCampo').asString;
                              dxDBEdit3.MaxLength := Q.FieldbyName('SizeCampo').asInteger;
                         end;
                         // data
                         if Q.FieldbyName('TipoCampo').asString = 'datetime' then begin
                              dxDBEdit3.Visible := False; dxDBDateEdit3.Visible := True; dxDBCheckEdit3.Visible := False; dxDBSpinEdit3.Visible := False; dxDBMemo3.Visible := False;
                              dxDBDateEdit3.DataField := Q.FieldbyName('NomeCampo').asString;
                         end;
                         // boolean
                         if Q.FieldbyName('TipoCampo').asString = 'boolean' then begin
                              dxDBEdit3.Visible := False; dxDBDateEdit3.Visible := False; dxDBCheckEdit3.Visible := True; dxDBSpinEdit3.Visible := False; dxDBMemo3.Visible := False;
                              dxDBCheckEdit3.DataField := Q.FieldbyName('NomeCampo').asString;
                         end;
                         // intero
                         if Q.FieldbyName('TipoCampo').asString = 'integer' then begin
                              dxDBEdit3.Visible := False; dxDBDateEdit3.Visible := False; dxDBCheckEdit3.Visible := False; dxDBSpinEdit3.Visible := True; dxDBMemo3.Visible := False;
                              dxDBSpinEdit3.DataField := Q.FieldbyName('NomeCampo').asString;
                              dxDBSpinEdit3.ValueType := vtInt;
                         end;
                         // floating point
                         if Q.FieldbyName('TipoCampo').asString = 'float' then begin
                              dxDBEdit3.Visible := False; dxDBDateEdit3.Visible := False; dxDBCheckEdit3.Visible := False; dxDBSpinEdit3.Visible := True; dxDBMemo3.Visible := False;
                              dxDBSpinEdit3.DataField := Q.FieldbyName('NomeCampo').asString;
                              dxDBSpinEdit3.ValueType := vtFloat;
                         end;
                         if Q.FieldbyName('TipoCampo').asString = 'text' then begin
                              dxDBEdit3.Visible := False; dxDBDateEdit3.Visible := False; dxDBCheckEdit3.Visible := False; dxDBSpinEdit3.Visible := False; dxDBMemo3.Visible := True;
                              dxDBMemo3.DataField := Q.FieldbyName('NomeCampo').asString;

                              Pan3.Height := 100;
                              xTop := xTop + 70;
                         end;
                    end;
               4: begin
                         Pan4.Visible := True;
                         Pan4.Top := xTop;
                         xTop := xTop + 30;
                         Label4.Caption := Q.FieldbyName('LabelCampo').asString;
                         // stringa
                         if Q.FieldbyName('TipoCampo').asString = 'varchar' then begin
                              dxDBEdit4.Visible := True; dxDBDateEdit4.Visible := False; dxDBCheckEdit4.Visible := False; dxDBSpinEdit4.Visible := False; dxDBMemo4.Visible := False;
                              dxDBEdit4.DataField := Q.FieldbyName('NomeCampo').asString;
                              dxDBEdit4.MaxLength := Q.FieldbyName('SizeCampo').asInteger;
                         end;
                         // data
                         if Q.FieldbyName('TipoCampo').asString = 'datetime' then begin
                              dxDBEdit4.Visible := False; dxDBDateEdit4.Visible := True; dxDBCheckEdit4.Visible := False; dxDBSpinEdit4.Visible := False; dxDBMemo4.Visible := False;
                              dxDBDateEdit4.DataField := Q.FieldbyName('NomeCampo').asString;
                         end;
                         // boolean
                         if Q.FieldbyName('TipoCampo').asString = 'boolean' then begin
                              dxDBEdit4.Visible := False; dxDBDateEdit4.Visible := False; dxDBCheckEdit4.Visible := True; dxDBSpinEdit4.Visible := False; dxDBMemo4.Visible := False;
                              dxDBCheckEdit4.DataField := Q.FieldbyName('NomeCampo').asString;
                         end;
                         // intero
                         if Q.FieldbyName('TipoCampo').asString = 'integer' then begin
                              dxDBEdit4.Visible := False; dxDBDateEdit4.Visible := False; dxDBCheckEdit4.Visible := False; dxDBSpinEdit4.Visible := True; dxDBMemo4.Visible := False;
                              dxDBSpinEdit4.DataField := Q.FieldbyName('NomeCampo').asString;
                              dxDBSpinEdit4.ValueType := vtInt;
                         end;
                         // floating point
                         if Q.FieldbyName('TipoCampo').asString = 'float' then begin
                              dxDBEdit4.Visible := False; dxDBDateEdit4.Visible := False; dxDBCheckEdit4.Visible := False; dxDBSpinEdit4.Visible := True; dxDBMemo4.Visible := False;
                              dxDBSpinEdit4.DataField := Q.FieldbyName('NomeCampo').asString;
                              dxDBSpinEdit4.ValueType := vtFloat;
                         end;
                         if Q.FieldbyName('TipoCampo').asString = 'text' then begin
                              dxDBEdit4.Visible := False; dxDBDateEdit4.Visible := False; dxDBCheckEdit4.Visible := False; dxDBSpinEdit4.Visible := False; dxDBMemo4.Visible := True;
                              dxDBMemo4.DataField := Q.FieldbyName('NomeCampo').asString;

                              Pan4.Height := 100;
                              xTop := xTop + 70;
                         end;
                    end;
               5: begin
                         Pan5.Visible := True;
                         Pan5.Top := xTop;
                         xTop := xTop + 30;
                         Label5.Caption := Q.FieldbyName('LabelCampo').asString;
                         // stringa
                         if Q.FieldbyName('TipoCampo').asString = 'varchar' then begin
                              dxDBEdit5.Visible := True; dxDBDateEdit5.Visible := False; dxDBCheckEdit5.Visible := False; dxDBSpinEdit5.Visible := False; dxDBMemo5.Visible := False;
                              dxDBEdit5.DataField := Q.FieldbyName('NomeCampo').asString;
                              dxDBEdit5.MaxLength := Q.FieldbyName('SizeCampo').asInteger;
                         end;
                         // data
                         if Q.FieldbyName('TipoCampo').asString = 'datetime' then begin
                              dxDBEdit5.Visible := False; dxDBDateEdit5.Visible := True; dxDBCheckEdit5.Visible := False; dxDBSpinEdit5.Visible := False; dxDBMemo5.Visible := False;
                              dxDBDateEdit5.DataField := Q.FieldbyName('NomeCampo').asString;
                         end;
                         // boolean
                         if Q.FieldbyName('TipoCampo').asString = 'boolean' then begin
                              dxDBEdit5.Visible := False; dxDBDateEdit5.Visible := False; dxDBCheckEdit5.Visible := True; dxDBSpinEdit5.Visible := False; dxDBMemo5.Visible := False;
                              dxDBCheckEdit5.DataField := Q.FieldbyName('NomeCampo').asString;
                         end;
                         // intero
                         if Q.FieldbyName('TipoCampo').asString = 'integer' then begin
                              dxDBEdit5.Visible := False; dxDBDateEdit5.Visible := False; dxDBCheckEdit5.Visible := False; dxDBSpinEdit5.Visible := True; dxDBMemo5.Visible := False;
                              dxDBSpinEdit5.DataField := Q.FieldbyName('NomeCampo').asString;
                              dxDBSpinEdit5.ValueType := vtInt;
                         end;
                         // floating point
                         if Q.FieldbyName('TipoCampo').asString = 'float' then begin
                              dxDBEdit5.Visible := False; dxDBDateEdit5.Visible := False; dxDBCheckEdit5.Visible := False; dxDBSpinEdit5.Visible := True; dxDBMemo5.Visible := False;
                              dxDBSpinEdit5.DataField := Q.FieldbyName('NomeCampo').asString;
                              dxDBSpinEdit5.ValueType := vtFloat;
                         end;
                         if Q.FieldbyName('TipoCampo').asString = 'text' then begin
                              dxDBEdit5.Visible := False; dxDBDateEdit5.Visible := False; dxDBCheckEdit5.Visible := False; dxDBSpinEdit5.Visible := False; dxDBMemo5.Visible := True;
                              dxDBMemo5.DataField := Q.FieldbyName('NomeCampo').asString;

                              Pan5.Height := 100;
                              xTop := xTop + 70;
                         end;
                    end;
               6: begin
                         Pan6.Visible := True;
                         Pan6.Top := xTop;
                         xTop := xTop + 30;
                         Label6.Caption := Q.FieldbyName('LabelCampo').asString;
                         // stringa
                         if Q.FieldbyName('TipoCampo').asString = 'varchar' then begin
                              dxDBEdit6.Visible := True; dxDBDateEdit6.Visible := False; dxDBCheckEdit6.Visible := False; dxDBSpinEdit6.Visible := False; dxDBMemo6.Visible := False;
                              dxDBEdit6.DataField := Q.FieldbyName('NomeCampo').asString;
                              dxDBEdit6.MaxLength := Q.FieldbyName('SizeCampo').asInteger;
                         end;
                         // data
                         if Q.FieldbyName('TipoCampo').asString = 'datetime' then begin
                              dxDBEdit6.Visible := False; dxDBDateEdit6.Visible := True; dxDBCheckEdit6.Visible := False; dxDBSpinEdit6.Visible := False; dxDBMemo6.Visible := False;
                              dxDBDateEdit6.DataField := Q.FieldbyName('NomeCampo').asString;
                         end;
                         // boolean
                         if Q.FieldbyName('TipoCampo').asString = 'boolean' then begin
                              dxDBEdit6.Visible := False; dxDBDateEdit6.Visible := False; dxDBCheckEdit6.Visible := True; dxDBSpinEdit6.Visible := False; dxDBMemo6.Visible := False;
                              dxDBCheckEdit6.DataField := Q.FieldbyName('NomeCampo').asString;
                         end;
                         // intero
                         if Q.FieldbyName('TipoCampo').asString = 'integer' then begin
                              dxDBEdit6.Visible := False; dxDBDateEdit6.Visible := False; dxDBCheckEdit6.Visible := False; dxDBSpinEdit6.Visible := True; dxDBMemo6.Visible := False;
                              dxDBSpinEdit6.DataField := Q.FieldbyName('NomeCampo').asString;
                              dxDBSpinEdit6.ValueType := vtInt;
                         end;
                         // floating point
                         if Q.FieldbyName('TipoCampo').asString = 'float' then begin
                              dxDBEdit6.Visible := False; dxDBDateEdit6.Visible := False; dxDBCheckEdit6.Visible := False; dxDBSpinEdit6.Visible := True; dxDBMemo6.Visible := False;
                              dxDBSpinEdit6.DataField := Q.FieldbyName('NomeCampo').asString;
                              dxDBSpinEdit6.ValueType := vtFloat;
                         end;
                         if Q.FieldbyName('TipoCampo').asString = 'text' then begin
                              dxDBEdit6.Visible := False; dxDBDateEdit6.Visible := False; dxDBCheckEdit6.Visible := False; dxDBSpinEdit6.Visible := False; dxDBMemo6.Visible := True;
                              dxDBMemo6.DataField := Q.FieldbyName('NomeCampo').asString;

                              Pan6.Height := 100;
                              xTop := xTop + 70;
                         end;
                    end;
               7: begin
                         Pan7.Visible := True;
                         Pan7.Top := xTop;
                         xTop := xTop + 30;
                         Label7.Caption := Q.FieldbyName('LabelCampo').asString;
                         // stringa
                         if Q.FieldbyName('TipoCampo').asString = 'varchar' then begin
                              dxDBEdit7.Visible := True; dxDBDateEdit7.Visible := False; dxDBCheckEdit7.Visible := False; dxDBSpinEdit7.Visible := False; dxDBMemo7.Visible := False;
                              dxDBEdit7.DataField := Q.FieldbyName('NomeCampo').asString;
                              dxDBEdit7.MaxLength := Q.FieldbyName('SizeCampo').asInteger;
                         end;
                         // data
                         if Q.FieldbyName('TipoCampo').asString = 'datetime' then begin
                              dxDBEdit7.Visible := False; dxDBDateEdit7.Visible := True; dxDBCheckEdit7.Visible := False; dxDBSpinEdit7.Visible := False; dxDBMemo7.Visible := False;
                              dxDBDateEdit7.DataField := Q.FieldbyName('NomeCampo').asString;
                         end;
                         // boolean
                         if Q.FieldbyName('TipoCampo').asString = 'boolean' then begin
                              dxDBEdit7.Visible := False; dxDBDateEdit7.Visible := False; dxDBCheckEdit7.Visible := True; dxDBSpinEdit7.Visible := False; dxDBMemo7.Visible := False;
                              dxDBCheckEdit7.DataField := Q.FieldbyName('NomeCampo').asString;
                         end;
                         // intero
                         if Q.FieldbyName('TipoCampo').asString = 'integer' then begin
                              dxDBEdit7.Visible := False; dxDBDateEdit7.Visible := False; dxDBCheckEdit7.Visible := False; dxDBSpinEdit7.Visible := True; dxDBMemo7.Visible := False;
                              dxDBSpinEdit7.DataField := Q.FieldbyName('NomeCampo').asString;
                              dxDBSpinEdit7.ValueType := vtInt;
                         end;
                         // floating point
                         if Q.FieldbyName('TipoCampo').asString = 'float' then begin
                              dxDBEdit7.Visible := False; dxDBDateEdit7.Visible := False; dxDBCheckEdit7.Visible := False; dxDBSpinEdit7.Visible := True; dxDBMemo7.Visible := False;
                              dxDBSpinEdit7.DataField := Q.FieldbyName('NomeCampo').asString;
                              dxDBSpinEdit7.ValueType := vtFloat;
                         end;
                         if Q.FieldbyName('TipoCampo').asString = 'text' then begin
                              dxDBEdit7.Visible := False; dxDBDateEdit7.Visible := False; dxDBCheckEdit7.Visible := False; dxDBSpinEdit7.Visible := False; dxDBMemo7.Visible := True;
                              dxDBMemo7.DataField := Q.FieldbyName('NomeCampo').asString;

                              Pan7.Height := 100;
                              xTop := xTop + 70;
                         end;
                    end;
               8: begin
                         Pan8.Visible := True;
                         Pan8.Top := xTop;
                         xTop := xTop + 30;
                         Label8.Caption := Q.FieldbyName('LabelCampo').asString;
                         // stringa
                         if Q.FieldbyName('TipoCampo').asString = 'varchar' then begin
                              dxDBEdit8.Visible := True; dxDBDateEdit8.Visible := False; dxDBCheckEdit8.Visible := False; dxDBSpinEdit8.Visible := False; dxDBMemo8.Visible := False;
                              dxDBEdit8.DataField := Q.FieldbyName('NomeCampo').asString;
                              dxDBEdit8.MaxLength := Q.FieldbyName('SizeCampo').asInteger;
                         end;
                         // data
                         if Q.FieldbyName('TipoCampo').asString = 'datetime' then begin
                              dxDBEdit8.Visible := False; dxDBDateEdit8.Visible := True; dxDBCheckEdit8.Visible := False; dxDBSpinEdit8.Visible := False; dxDBMemo8.Visible := False;
                              dxDBDateEdit8.DataField := Q.FieldbyName('NomeCampo').asString;
                         end;
                         // boolean
                         if Q.FieldbyName('TipoCampo').asString = 'boolean' then begin
                              dxDBEdit8.Visible := False; dxDBDateEdit8.Visible := False; dxDBCheckEdit8.Visible := True; dxDBSpinEdit8.Visible := False; dxDBMemo8.Visible := False;
                              dxDBCheckEdit8.DataField := Q.FieldbyName('NomeCampo').asString;
                         end;
                         // intero
                         if Q.FieldbyName('TipoCampo').asString = 'integer' then begin
                              dxDBEdit8.Visible := False; dxDBDateEdit8.Visible := False; dxDBCheckEdit8.Visible := False; dxDBSpinEdit8.Visible := True; dxDBMemo8.Visible := False;
                              dxDBSpinEdit8.DataField := Q.FieldbyName('NomeCampo').asString;
                              dxDBSpinEdit8.ValueType := vtInt;
                         end;
                         // floating point
                         if Q.FieldbyName('TipoCampo').asString = 'float' then begin
                              dxDBEdit8.Visible := False; dxDBDateEdit8.Visible := False; dxDBCheckEdit8.Visible := False; dxDBSpinEdit8.Visible := True; dxDBMemo8.Visible := False;
                              dxDBSpinEdit8.DataField := Q.FieldbyName('NomeCampo').asString;
                              dxDBSpinEdit8.ValueType := vtFloat;
                         end;
                         if Q.FieldbyName('TipoCampo').asString = 'text' then begin
                              dxDBEdit8.Visible := False; dxDBDateEdit8.Visible := False; dxDBCheckEdit8.Visible := False; dxDBSpinEdit8.Visible := False; dxDBMemo8.Visible := True;
                              dxDBMemo8.DataField := Q.FieldbyName('NomeCampo').asString;

                              Pan8.Height := 100;
                              xTop := xTop + 70;
                         end;
                    end;
               9: begin
                         Pan9.Visible := True;
                         Pan9.Top := xTop;
                         xTop := xTop + 30;
                         Label9.Caption := Q.FieldbyName('LabelCampo').asString;
                         // stringa
                         if Q.FieldbyName('TipoCampo').asString = 'varchar' then begin
                              dxDBEdit9.Visible := True; dxDBDateEdit9.Visible := False; dxDBCheckEdit9.Visible := False; dxDBSpinEdit9.Visible := False; dxDBMemo9.Visible := False;
                              dxDBEdit9.DataField := Q.FieldbyName('NomeCampo').asString;
                              dxDBEdit9.MaxLength := Q.FieldbyName('SizeCampo').asInteger;
                         end;
                         // data
                         if Q.FieldbyName('TipoCampo').asString = 'datetime' then begin
                              dxDBEdit9.Visible := False; dxDBDateEdit9.Visible := True; dxDBCheckEdit9.Visible := False; dxDBSpinEdit9.Visible := False; dxDBMemo9.Visible := False;
                              dxDBDateEdit9.DataField := Q.FieldbyName('NomeCampo').asString;
                         end;
                         // boolean
                         if Q.FieldbyName('TipoCampo').asString = 'boolean' then begin
                              dxDBEdit9.Visible := False; dxDBDateEdit9.Visible := False; dxDBCheckEdit9.Visible := True; dxDBSpinEdit9.Visible := False; dxDBMemo9.Visible := False;
                              dxDBCheckEdit9.DataField := Q.FieldbyName('NomeCampo').asString;
                         end;
                         // intero
                         if Q.FieldbyName('TipoCampo').asString = 'integer' then begin
                              dxDBEdit9.Visible := False; dxDBDateEdit9.Visible := False; dxDBCheckEdit9.Visible := False; dxDBSpinEdit9.Visible := True; dxDBMemo9.Visible := False;
                              dxDBSpinEdit9.DataField := Q.FieldbyName('NomeCampo').asString;
                              dxDBSpinEdit9.ValueType := vtInt;
                         end;
                         // floating point
                         if Q.FieldbyName('TipoCampo').asString = 'float' then begin
                              dxDBEdit9.Visible := False; dxDBDateEdit9.Visible := False; dxDBCheckEdit9.Visible := False; dxDBSpinEdit9.Visible := True; dxDBMemo9.Visible := False;
                              dxDBSpinEdit9.DataField := Q.FieldbyName('NomeCampo').asString;
                              dxDBSpinEdit9.ValueType := vtFloat;
                         end;
                         if Q.FieldbyName('TipoCampo').asString = 'text' then begin
                              dxDBEdit9.Visible := False; dxDBDateEdit9.Visible := False; dxDBCheckEdit9.Visible := False; dxDBSpinEdit9.Visible := False; dxDBMemo9.Visible := True;
                              dxDBMemo9.DataField := Q.FieldbyName('NomeCampo').asString;

                              Pan9.Height := 100;
                              xTop := xTop + 70;
                         end;
                    end;
               10: begin
                         Pan10.Visible := True;
                         Pan10.Top := xTop;
                         xTop := xTop + 30;
                         Label10.Caption := Q.FieldbyName('LabelCampo').asString;
                         // stringa
                         if Q.FieldbyName('TipoCampo').asString = 'varchar' then begin
                              dxDBEdit10.Visible := True; dxDBDateEdit10.Visible := False; dxDBCheckEdit10.Visible := False; dxDBSpinEdit10.Visible := False; dxDBMemo10.Visible := False;
                              dxDBEdit10.DataField := Q.FieldbyName('NomeCampo').asString;
                              dxDBEdit10.MaxLength := Q.FieldbyName('SizeCampo').asInteger;
                         end;
                         // data
                         if Q.FieldbyName('TipoCampo').asString = 'datetime' then begin
                              dxDBEdit10.Visible := False; dxDBDateEdit10.Visible := True; dxDBCheckEdit10.Visible := False; dxDBSpinEdit10.Visible := False; dxDBMemo10.Visible := False;
                              dxDBDateEdit10.DataField := Q.FieldbyName('NomeCampo').asString;
                         end;
                         // boolean
                         if Q.FieldbyName('TipoCampo').asString = 'boolean' then begin
                              dxDBEdit10.Visible := False; dxDBDateEdit10.Visible := False; dxDBCheckEdit10.Visible := True; dxDBSpinEdit10.Visible := False; dxDBMemo10.Visible := False;
                              dxDBCheckEdit10.DataField := Q.FieldbyName('NomeCampo').asString;
                         end;
                         // intero
                         if Q.FieldbyName('TipoCampo').asString = 'integer' then begin
                              dxDBEdit10.Visible := False; dxDBDateEdit10.Visible := False; dxDBCheckEdit10.Visible := False; dxDBSpinEdit10.Visible := True; dxDBMemo10.Visible := False;
                              dxDBSpinEdit10.DataField := Q.FieldbyName('NomeCampo').asString;
                              dxDBSpinEdit10.ValueType := vtInt;
                         end;
                         // floating point
                         if Q.FieldbyName('TipoCampo').asString = 'float' then begin
                              dxDBEdit10.Visible := False; dxDBDateEdit10.Visible := False; dxDBCheckEdit10.Visible := False; dxDBSpinEdit10.Visible := True; dxDBMemo10.Visible := False;
                              dxDBSpinEdit10.DataField := Q.FieldbyName('NomeCampo').asString;
                              dxDBSpinEdit10.ValueType := vtFloat;
                         end;
                         if Q.FieldbyName('TipoCampo').asString = 'text' then begin
                              dxDBEdit10.Visible := False; dxDBDateEdit10.Visible := False; dxDBCheckEdit10.Visible := False; dxDBSpinEdit10.Visible := False; dxDBMemo10.Visible := True;
                              dxDBMemo10.DataField := Q.FieldbyName('NomeCampo').asString;

                              Pan10.Height := 100;
                              xTop := xTop + 70;
                         end;
                    end;



               11: begin
                         Pan11.Visible := True;
                         Pan11.Top := xTop;
                         xTop := xTop + 30;
                         Label11.Caption := Q.FieldbyName('LabelCampo').asString;
                         // stringa
                         if Q.FieldbyName('TipoCampo').asString = 'varchar' then begin
                              dxDBEdit11.Visible := True; dxDBDateEdit11.Visible := False; dxDBCheckEdit11.Visible := False; dxDBSpinEdit11.Visible := False; dxDBMemo11.Visible := False;
                              dxDBEdit11.DataField := Q.FieldbyName('NomeCampo').asString;
                              dxDBEdit11.MaxLength := Q.FieldbyName('SizeCampo').asInteger;
                         end;
                         // data
                         if Q.FieldbyName('TipoCampo').asString = 'datetime' then begin
                              dxDBEdit11.Visible := False; dxDBDateEdit11.Visible := True; dxDBCheckEdit11.Visible := False; dxDBSpinEdit11.Visible := False; dxDBMemo11.Visible := False;
                              dxDBDateEdit11.DataField := Q.FieldbyName('NomeCampo').asString;
                         end;
                         // boolean
                         if Q.FieldbyName('TipoCampo').asString = 'boolean' then begin
                              dxDBEdit11.Visible := False; dxDBDateEdit11.Visible := False; dxDBCheckEdit11.Visible := True; dxDBSpinEdit11.Visible := False; dxDBMemo11.Visible := False;
                              dxDBCheckEdit11.DataField := Q.FieldbyName('NomeCampo').asString;
                         end;
                         // intero
                         if Q.FieldbyName('TipoCampo').asString = 'integer' then begin
                              dxDBEdit11.Visible := False; dxDBDateEdit11.Visible := False; dxDBCheckEdit11.Visible := False; dxDBSpinEdit11.Visible := True; dxDBMemo11.Visible := False;
                              dxDBSpinEdit11.DataField := Q.FieldbyName('NomeCampo').asString;
                              dxDBSpinEdit11.ValueType := vtInt;
                         end;
                         // floating point
                         if Q.FieldbyName('TipoCampo').asString = 'float' then begin
                              dxDBEdit11.Visible := False; dxDBDateEdit11.Visible := True; dxDBCheckEdit11.Visible := False; dxDBSpinEdit11.Visible := True; dxDBMemo11.Visible := False;
                              dxDBSpinEdit11.DataField := Q.FieldbyName('NomeCampo').asString;
                              dxDBSpinEdit11.ValueType := vtFloat;
                         end;
                         if Q.FieldbyName('TipoCampo').asString = 'text' then begin
                              dxDBEdit11.Visible := False; dxDBDateEdit11.Visible := False; dxDBCheckEdit11.Visible := False; dxDBSpinEdit11.Visible := False; dxDBMemo11.Visible := True;
                              dxDBMemo11.DataField := Q.FieldbyName('NomeCampo').asString;

                              Pan11.Height := 100;
                              xTop := xTop + 70;
                         end;
                    end;

                    12: begin
                         Pan12.Visible := True;
                         Pan12.Top := xTop;
                         xTop := xTop + 30;
                         Label12.Caption := Q.FieldbyName('LabelCampo').asString;
                         // stringa
                         if Q.FieldbyName('TipoCampo').asString = 'varchar' then begin
                              dxDBEdit12.Visible := True; dxDBDateEdit12.Visible := False; dxDBCheckEdit12.Visible := False; dxDBSpinEdit12.Visible := False; dxDBMemo12.Visible := False;
                              dxDBEdit12.DataField := Q.FieldbyName('NomeCampo').asString;
                              dxDBEdit12.MaxLength := Q.FieldbyName('SizeCampo').asInteger;
                         end;
                         // data
                         if Q.FieldbyName('TipoCampo').asString = 'datetime' then begin
                              dxDBEdit12.Visible := False; dxDBDateEdit12.Visible := True; dxDBCheckEdit12.Visible := False; dxDBSpinEdit12.Visible := False; dxDBMemo12.Visible := False;
                              dxDBDateEdit12.DataField := Q.FieldbyName('NomeCampo').asString;
                         end;
                         // boolean
                         if Q.FieldbyName('TipoCampo').asString = 'boolean' then begin
                              dxDBEdit12.Visible := False; dxDBDateEdit12.Visible := False; dxDBCheckEdit12.Visible := True; dxDBSpinEdit12.Visible := False; dxDBMemo12.Visible := False;
                              dxDBCheckEdit12.DataField := Q.FieldbyName('NomeCampo').asString;
                         end;
                         // intero
                         if Q.FieldbyName('TipoCampo').asString = 'integer' then begin
                              dxDBEdit12.Visible := False; dxDBDateEdit12.Visible := False; dxDBCheckEdit12.Visible := False; dxDBSpinEdit12.Visible := True; dxDBMemo12.Visible := False;
                              dxDBSpinEdit12.DataField := Q.FieldbyName('NomeCampo').asString;
                              dxDBSpinEdit12.ValueType := vtInt;
                         end;
                         // floating point
                         if Q.FieldbyName('TipoCampo').asString = 'float' then begin
                              dxDBEdit12.Visible := False; dxDBDateEdit12.Visible := True; dxDBCheckEdit12.Visible := False; dxDBSpinEdit12.Visible := True; dxDBMemo12.Visible := False;
                              dxDBSpinEdit12.DataField := Q.FieldbyName('NomeCampo').asString;
                              dxDBSpinEdit12.ValueType := vtFloat;
                         end;
                         if Q.FieldbyName('TipoCampo').asString = 'text' then begin
                              dxDBEdit12.Visible := False; dxDBDateEdit12.Visible := False; dxDBCheckEdit12.Visible := False; dxDBSpinEdit12.Visible := False; dxDBMemo12.Visible := True;
                              dxDBMemo12.DataField := Q.FieldbyName('NomeCampo').asString;

                              Pan12.Height := 100;
                              xTop := xTop + 70;
                         end;
                    end;

          end;

          Q.next;
          inc(xCount);
     end;
     Q.Close;
end;

procedure TCampiPersFrame.BOKClick(Sender: TObject);
begin
     //QCampiPers.Post;
     TCampiPers.Post;
end;

procedure TCampiPersFrame.BCanClick(Sender: TObject);
begin
     //QCampiPers.Cancel;
     TCampiPers.Cancel;
end;

procedure TCampiPersFrame.DsQCampiPersStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsQCampiPers.State in [dsEdit, dsInsert];
     BOK.Enabled := b;
     BCan.Enabled := b;
end;

procedure TCampiPersFrame.TCampiPersBeforeClose(DataSet: TDataSet);
begin
     if DsQCampiPers.State = dsEdit then TCampiPers.Post;
end;

procedure TCampiPersFrame.ToolbarButton971Click(Sender: TObject);
begin
     // controllo <=10
     Q.Close;
     Q.SQL.text := 'select count(*) Tot from TabCampiPers where Ambito=:x';
     Q.Parameters[0].Value := xTipo;
     Q.Open;
     if Q.FieldbyName('Tot').AsInteger = 10 then begin
          MessageDlg('Raggiunto il massimo numero di campi personalizzati consentito' + chr(10) +
               'Impossibile proseguire', mtError, [mbOK], 0);
          Q.Close;
          Exit;
     end;
     Q.Close;

     DefCampiPersForm := TDefCampiPersForm.create(self);
     DefCampiPersForm.xTipo := xTipo;
     DefCampiPersForm.Showmodal;
     if DefCampiPersForm.ModalResult = mrOK then begin
          TCampiPers.Close;
          TCampiPers.Open;
          ImpostaCampi;
     end;
     DefCampiPersForm.Free;
end;


procedure TCampiPersFrame.ToolbarButton972Click(Sender: TObject);
var xPassword: string;
begin
     Q.close;
     Q.SQL.Text := 'select id from tabcampipers where ambito=:x ';
     Q.Parameters[0].value := xTipo;
     Q.open;
     if Q.RecordCount = 0 then begin
          MessageDlg('Nessun campo personalizzato impostato', mtInformation, [mbOk], 0);
          exit;
     end;

     InputPasswordForm.EPassword.text := 'EPassword';
     InputPasswordForm.caption := 'Inserimento Password';
     //InputPasswordForm.EPassword.SetFocus;
     InputPasswordForm.ShowModal;
     if InputPasswordForm.ModalResult = mrOK then begin
          xPassword := InputPasswordForm.EPassword.Text;
     end else exit;
     if not CheckPassword(xPassword, LeggiPswProcedure(9)) then begin
          MessageDlg('Password errata', mtError, [mbOK], 0);
          exit;
     end;
     EliminaCampoPersForm := TEliminaCampoPersForm.Create(Self);
     EliminaCampoPersForm.xtipo := xTipo;
     EliminaCampoPersForm.showmodal;
     if EliminaCampoPersForm.ModalResult = mrok then begin
          //  showmessage(EliminaCampoPersForm.QCampiPersLabelCampo.AsString);
          if MessageDlg('Sei sicuro di voler cancellare "' + EliminaCampoPersForm.QCampiPersLabelCampo.AsString + '" ?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then begin

               Data.Q1.close;
               data.q1.SQL.Text := 'delete tabcampipers  where id= ' + EliminaCampoPersForm.QCampiPersID.AsString;
               data.q1.ExecSQL;
               Log_Operation(mainform.xIDUtenteAttuale, 'tabcampipers', EliminaCampoPersForm.QCampiPersID.asInteger, 'D',
                    'Ambito=' + xTipo + ' NomeCampo=' + EliminaCampoPersForm.QCampiPersNomeCampo.AsString + ' LabelCampo=' + EliminaCampoPersForm.QCampiPersLabelCampo.AsString);

               Pan1.Visible := false;
               Pan2.Visible := false;
               Pan3.Visible := false;
               Pan4.Visible := false;
               Pan5.Visible := false;
               Pan6.Visible := false;
               Pan7.Visible := false;
               Pan8.Visible := false;
               Pan9.Visible := false;
               Pan10.Visible := false;
               ImpostaCampi;
               CaricaValori;
          end;
     end;
     EliminaCampoPersForm.free;
end;

end.

