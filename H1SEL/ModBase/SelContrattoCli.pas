unit SelContrattoCli;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Grids, DBGrids, Db, DBTables, StdCtrls, Buttons, ADODB, U_ADOLinkCl;

type
     TSelContrattoCliForm = class(TForm)
          BitBtn1: TBitBtn;
          BitBtn2: TBitBtn;
          DsQContrattiCli: TDataSource;
          DBGrid1: TDBGrid;
          QContrattiCli: TADOLinkedQuery;
          procedure FormShow(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     SelContrattoCliForm: TSelContrattoCliForm;

implementation
uses ModuloDati;
{$R *.DFM}

procedure TSelContrattoCliForm.FormShow(Sender: TObject);
begin
     Caption := '[S/37] - ' + Caption;
end;

end.
