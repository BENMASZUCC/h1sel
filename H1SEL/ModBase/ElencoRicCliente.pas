unit ElencoRicCliente;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Grids, DBGrids, StdCtrls, Buttons, Mask, DBCtrls, ExtCtrls, Db, DBTables,
     ADODB, U_ADOLinkCl, dxDBTLCl, dxGrClms, dxTL, dxDBCtrl, dxDBGrid,
     dxCntner;

type
     TElencoRicClienteForm = class(TForm)
          Panel1: TPanel;
          Panel7: TPanel;
          DBEdit12: TDBEdit;
          BitBtn1: TBitBtn;
          BitBtn2: TBitBtn;
          DsQRicCliente: TDataSource;
          DBGElencoComm: TdxDBGrid;
          DBGElencoCommColumn1: TdxDBGridColumn;
          DBGElencoCommColumn2: TdxDBGridDateColumn;
          DBGElencoCommColumn3: TdxDBGridColumn;
          DBGElencoCommColumn4: TdxDBGridColumn;
          DBGElencoCommColumn5: TdxDBGridColumn;
          DBGElencoCommColumn6: TdxDBGridColumn;
    QRicCliente: TADOQuery;
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     ElencoRicClienteForm: TElencoRicClienteForm;

implementation

uses ModuloDati, ModuloDati2, MDRicerche, InsRicerca, Main;

{$R *.DFM}

end.

