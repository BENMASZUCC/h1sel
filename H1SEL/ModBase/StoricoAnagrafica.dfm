object StoricoAnagraficaForm: TStoricoAnagraficaForm
  Left = 180
  Top = 201
  Width = 834
  Height = 486
  Caption = 'Storico Anagrafica'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 826
    Height = 17
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
  end
  object Panel2: TPanel
    Left = 0
    Top = 418
    Width = 826
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object ToolbarButton971: TToolbarButton97
      Left = 696
      Top = 3
      Width = 81
      Height = 38
      Caption = 'ESCI'
      Glyph.Data = {
        76060000424D7606000000000000360400002800000018000000180000000100
        080000000000400200000000000000000000000100000000000000000000FFFF
        FF00FF00FF0082817D001A1AA9005A65E500B1B2EC003D3C3A00DFDCC100B1AE
        A2004F4FA3008787C20062615D003437D300D8D9F6006566BF00626284009F9B
        7E00C3C3BB007878A6007E81E300282827009F9FD6003736B1004E4EC500C4C4
        DE009D9C9C00ECECF800535249007474CE006D6D6D002525BD004C53DC008F8F
        8C009090E2006362A700938E77006363D1004346B4005858B8003F47CE005656
        9500C1BFA9009191CC00AAA59300D4D0BA00A9ACDC00333330006D6C8D008484
        D3009D9FE7007C7CBB00D0D2EB00BFBFED00484848007B7A7300706E5D002D2E
        C900282AAE006A6CDC00616197005A5BCA00E1E1F0009391BF00B6B6AE006F6F
        B70085816F0073729A00565AD700A4A3A100CDCCC400B8B8E2004343C3008080
        8A008B89B6007C7CC800A19D8A003338C5004D4DB5003030A700C9C5AD007373
        C10088878600545EE2005151D1006969A0002D32B500474BD60096938400BBBB
        B600898ED500ADACAA00343BBA005F5FB300595A57006060C0004949AB00777C
        D4009898CF00C7C8E700ABABD2003B3ED1008482B600E4E5F7004A4ACC005957
        4D009A9A92005656C0008A8ACB005C5C8000D6D7EE00BDBDBF00646DE300C1C0
        B200A3A096006E6ECB00292DBD002323B000AFAB9700DDDDEA008383C800CFCB
        BB004146D9005157C8004040C8007171D5002D2FCE0075746D003F3FB4004949
        BA004E4EBD00D3D3F5009C988500393ACA00BCBBAE00ABAAA6005961E0006C6C
        BD003333CD00DDDEF1009696CA007E7ED200D9D5BB002929C9008E8D87008D8D
        DC00898A8E008C8CC400A3A5D7009B978000BFC0E100A7A49C0068678D007877
        9A00A1A09E009494DE004244D300B5B3A7004B54C700B4B6EE006068E1006E6E
        C200F0F1F800C8C7BC004447C9004E4FD500545ADD00777BCD004A4942005B5B
        BF008B8ABE00E7E7F300838174006262CB00B5B5E300424AD3005151CA005F5F
        BC007C7CC300CAC8C500393ED5009397D400848483004645AF0066668500C6C5
        C0003636BB0030302F00E1E1F600DCD9BD00CECBC0002F2FB100CAC7B7005C5C
        5B00C5C3B6003939B5007E7E7B007777D600EBEBF3004546D0004E56DF004949
        BF00B1AFAB005151C2005E5EC7008686CE007A7AC7003434C700BBBDE2005D64
        E0005355D800565DDD00DADBF0005353C6007E7ECC00D0CDB8003638CE009D9D
        94009B9BD3006161B9008C8CCD007B7BD000B8B7AA00454ADA00B1AFA6005A5A
        CE005E5BB500383BD100383DCE00414BCB009494CE008484C100E9E9F8003C3B
        3600C4C5E7003335B2003535C3003538C8004747B6005959BB005B5ECA006262
        D5006A6AC1007272C500E2E3F200E0E0EE00D8D8F10056544B002929BC002B2B
        C0003031C90079786E00A4A09300606081009C998900A7A6A400020202020202
        021A1E36BB1515BB361E1A0202020202020202020202029AC12F0C6A126F6F12
        D90C2F5E1A02020202020202020252077FB3E6F04DE4E44D3D2BB337E9B60202
        020202020203A8DE8CEDDFA688D1D188C87AED2B86A8C40202020202211CC25F
        B420A6D388A07070A0D1A474A9A3F790020202FF6986B10D5626AFC853535388
        D14494960DEF716945020237583F7EA46EA261C85305055320A7ABA2D27EAAFE
        FB02FF38BEF88A328BD434AF530505539E8B8B9FA59C1FBE384590844A8F8A3B
        676E3E5A205353E5638B6757A5A539664C90AC2AE2CF8A7E06D46EF47BC8282E
        8B6714E39CA585B750AC42081768FAD8E30ED4D4345CB5F48B9FE365D8C77C04
        0842242DC3B0688AD8A567D4D434F4D483B465D88AED4875C02495D780D5E154
        8A8A448B6E6E6E6E9CE3D88AFAF918BFC21195BDB7D53D25F1853AD06E6E6EEA
        EBFA8A39EC48CB4F8E95FED7236BCCAD27E73E8B6EBCF66ED427BAB0D5C9810A
        2D847250995F5FA119C6F53E1B22351B3EC664608281EE4350725B2CE05D5FAE
        ABF5F4E891C57D9B67F4F57789EE6087768702727930F34B1B1BF6318D8DDD1D
        73AE1BAB474E9879FC02025BFCC23CCE4B16D6CDCDCDD6CEF30FF2DA0F2971FC
        5B0202028709E03C6C786CDCDCCD78CE5189DBDB29879D87020202020287E086
        FD0B62E62B6C78B25151416D409D87020202020202025B974692B81393622B33
        5510924697870202020202020202025B9ACAB9591A49491A59B9CA9A5B020202
        0202020202020202025B451A9A87871A1A9A5B02020202020202}
      OnClick = ToolbarButton971Click
    end
    object BNew: TToolbarButton97
      Left = 2
      Top = 3
      Width = 79
      Height = 38
      Caption = 'Nuovo storico'
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFF00FFFFFFFF85B7850F6F0F1674161A761A1A761A18781817
        7917137D130D7F0D0A7E0A077C07027B020070007FB07FFFFFFFFFFFFF118311
        1F8C1F2A912A2F932F2E942E2C962C299A29239E231CA31C15A4150DA40D059F
        05019101057E05FFFFFFFFFFFF198D192C962C379C373D9F3D3C9F3C39A139A3
        D6A3FFFFFF24AF241CB11C13B2130AAD0A049F04057E05FFFFFFFFFFFF229122
        389C3843A24348A44845A54542A642FFFFFFFFFFFFFFFFFF21B52118B6180EB1
        0E08A308057E05FFFFFFFFFFFF2C962C42A0424CA54C4FA74F4CA74C46A74640
        AA40FFFFFFFFFFFFFFFFFF1AB31A14AF140FA30F0B800BFFFFFFFFFFFF359A35
        4BA54B52A85253A9534EA84E49A74941A84138AA38FFFFFFFFFFFFFFFFFF19AC
        1918A218128212FFFFFFFFFFFF3F9F3F53A953FFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF1F9E1F188118FFFFFFFFFFFF45A245
        5AAC5AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF259A251D7F1DFFFFFFFFFFFF4FA74F63B16361AF6159AB5951A65148A2483F
        9F3F369C36FFFFFFFFFFFFFFFFFF2699262A972A217E21FFFFFFFFFFFF53A953
        6CB66C68B4685EAD5E54A8544CA34C429F42FFFFFFFFFFFFFFFFFF2997292B98
        2B2D952D237E23FFFFFFFFFFFF5EAF5E7ABD7A70B87063B0635AAB5A52A652FF
        FFFFFFFFFFFFFFFF3399333099303098302F942F237D23FFFFFFFFFFFF6BB56B
        8DC68D80C0806FB76F67B26760AE60B4D9B4FFFFFF4CA54C49A44941A1413A9D
        3A3095301E7A1EFFFFFFFFFFFF77BB779DCF9D8CC68C79BC7970B87069B46965
        B26562B0625DAE5D56AB564EA74E41A1412F942F197719FFFFFFFFFFFFB1D8B1
        76BB7667B3675BAD5B54A9544FA74F4AA44A4BA54B46A3463FA03F3B9E3B3198
        31238C238ABB8AFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FF}
      Margin = 5
      OldDisabledStyle = True
      WordWrap = True
      OnClick = BNewClick
    end
    object BMod: TToolbarButton97
      Left = 81
      Top = 3
      Width = 87
      Height = 38
      Caption = 'Modifica storico'
      Glyph.Data = {
        36050000424D3605000000000000360400002800000010000000100000000100
        080000000000000100000000000000000000000100000000000000000000FFFF
        FF0000000000FFDFFF000EB0E30013B0E3000AACE20014ADE2001AB1E4001CB0
        E30023B1E400049BD900059BD90008A0DA000DA1D9000FA1DA0015A2DA0019AA
        E10018A0D90024ADE2002CB0E300018ED0001CA1DA001DA5DF001DA2DE0023A4
        DF002EA8E0001F9CD800249DD7002CA5DF0037ABE200FBFDFE000279BA000276
        B800057BBA00077AB8000A7CBA000B7DBB000D7CBA00127FBB00187EB900229B
        DC002598D6002998D50034A3DE0036A3DF0040A8E000006EAF00006CAE00137A
        B6001777B3001D7DB8002396DA002696DA002995D9002B96DA002A94D5002C94
        D4002D93D400399FDD0040A2DE0046A5DF0055ACE2001180CB000F6DAE00198A
        D5001672B1001876B3001975B3001A74B1001A74B0001F89D000228ED7001E78
        B500238ACF00217CB700237CB700237BB6002B95DA002C94D9002A8FD2002E92
        D3003096DA002F93D4003196D9003093D4002F92D3002F91D2003397DA003598
        DA00369ADB003799DB003898DB00389ADB003A9BDB003B9BDC003C9DDC003D9D
        DC003F9CDC003F9DDC00419EDD00429EDD0045A0DE0046A1DE0048A3DF0048A0
        DD0049A1DE004AA2DE004BA3DE004CA3DF004CA5DF004EA5DF004FA5DF0050A5
        DF0053A6E00053A7E00054A7E00055A8E00056A9E00056AAE0005AAAE1005BAA
        E1005DADE2005EACE2005EABE10060ADE20061ADE20063AFE30062AEE20065B0
        E30067B1E40068B2E40067B1E3006BB4E5006AB2E4006CB4E5006FB6E50070B6
        E50077BAE70076B9E60079BBE7007ABCE70080BFE8008CC5EA008DC5EA0085B6
        D8007FAFCE008ABADA009DCDEE00A5D1EF00B2D7F100CBE4F500E7F3FB005AA9
        E10063AEE3008BC2EA00FFFFFF00000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000101010101
        01010101010101010100019140424546433231262423202F9201013F47505751
        392B1C16100E0C15300101414F5B61603B2C1A130905060B210101485D010101
        010101010101040D2201014F656C706E3D2E1E140A08070F250101596C010101
        01010101010111122701016373767571683C2D1D1917181B2801016678010101
        010101010101292A330101707F7E1F9B69625A3E013435384B01017387837C01
        977D95014E36373A4C01017B8D899A990198015C585252534D010185908E8884
        7D01726D6A645E554901018A948F8C898681807A776F6456440101968B827974
        706B6C67635F544A930100010101010101010101010101010100}
      Margin = 5
      OldDisabledStyle = True
      WordWrap = True
      OnClick = BModClick
    end
    object BDel: TToolbarButton97
      Left = 168
      Top = 3
      Width = 89
      Height = 38
      Caption = 'Elimina storico'
      Glyph.Data = {
        36050000424D3605000000000000360400002800000010000000100000000100
        080000000000000100000000000000000000000100000000000000000000FFFF
        FF0000000000FFDFFF000E48E900134BE9002C5DE9007294F10098B1F600BACA
        F800DCE4FB000439E000053AE000083EE1000A43E8000D42E0000F43E1001547
        E0001848E0001C4BE0002352E6002456E8002C58E6002D5CE8003763E8006587
        EE006B8DF0006F8FEF007192F100B6C7F800DBE3FB00E2E8FB00EBEFFC000132
        D700022CC000022BBE00052EC100072FBE000A32C0000B33C1000D35C0001238
        C2001337BD00183CC0001F4CDF002450E400244FDF00254EDE00264EE2002951
        DC002A50DC002C55E4002C51DB004066E7004669E6006483EC00728FEE008097
        ED00B0BFF500BBC8F600E0E6FB000026B6000026B5001138D4000F30B5001941
        DE001636B8001739B9001839BB001939BA001F44D9001A39B8001A39B7001D3F
        BF002248DF001E3EBC002347D8002141BF002342BE002342BD002C52E2002A4D
        DA002C50E1002C51E1002D51DC002E51DB003054E2003053E1002F52DB003154
        E2003053DC002F51DA003558E2003759E300385AE3003A5BE3003A5CE3003B5D
        E3003F5FE4003F60E3004162E4004261E4004363E4004464E5004564E5004665
        E5004867E5004868E500496AE6004A69E6004B68E5004B69E5004E6CE6004E6D
        E6004F6CE600506DE600536FE7005470E7005673E7005A75E8005B76E8005D78
        E8005E79E800627CE900637DE800657EE9006781EA006A83E9006C86EB006B85
        EA006B83E9006C85EA006F88EB007089EB00728AEC00768DEC00778EEC007990
        EC007A91EC007E94ED008398EE00899DEE008CA0EF008B9EEE008DA1EF008596
        DC007F90D10095A8F0008A9BDE0097A9F10097A8F0009DAEF100A2B2F200A6B5
        F200B0BDF400B2BFF400BBC6F500E9EDFC004865E4004C67E500506CE600516C
        E6007086EA008093ED008195ED00B1BDF400B7C2F500EFF1FD00EEF0FC00FFFF
        FF00000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000101010101
        0101010101010101010001914042474844432A282625223D9201013F46515B55
        34312E13110F0C213E010141525D678B1F01010A1C050E0B2301014A5E669801
        3B381B090108040D24010153658F018C3635180607011A102701015C6E209C71
        6C010117151D1E12290101627401A4736B0101161419012C2B0101687701A4A0
        6A0101332D37012F490101727CA7A6A19E010150303A3C324D010174839501A3
        9F655F56800186544E01017A8A859901A5A2829A019357584F01018190398496
        A801019D8D64605A4B010188978E89857F7D7B79767064584501019B877E7875
        726D6F696361594C940100010101010101010101010101010100}
      Margin = 5
      OldDisabledStyle = True
      WordWrap = True
      OnClick = BDelClick
    end
  end
  object dxDBGrid1: TdxDBGrid
    Left = 0
    Top = 17
    Width = 826
    Height = 401
    Bands = <
      item
        Caption = 'Dati Anagrafici'
      end
      item
        Caption = 'Residenza'
      end
      item
        Caption = 'Domicilio'
      end
      item
        Caption = 'Recapiti Telefonici'
      end
      item
        Caption = 'Carta d'#39'IdentitÓ'
      end
      item
        Caption = 'Passaporto'
      end>
    DefaultLayout = False
    HeaderPanelRowCount = 1
    SummaryGroups = <>
    SummarySeparator = ', '
    Align = alClient
    TabOrder = 2
    DataSource = DSStoricoAnag
    Filter.Criteria = {00000000}
    OptionsBehavior = [edgoAutoSort, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoTabThrough, edgoVertThrough]
    OptionsDB = [edgoCancelOnExit, edgoCanNavigation, edgoConfirmDelete, edgoUseBookmarks]
    OptionsView = [edgoBandHeaderWidth, edgoIndicator, edgoUseBitmap]
    ShowBands = True
    object dxDBGrid1DallaData: TdxDBGridDateColumn
      Caption = 'Dalla Data'
      Width = 75
      BandIndex = 0
      RowIndex = 0
      FieldName = 'DallaData'
    end
    object dxDBGrid1CID: TdxDBGridMaskColumn
      Width = 30
      BandIndex = 0
      RowIndex = 0
      FieldName = 'CID'
    end
    object dxDBGrid1LuogoNascita: TdxDBGridMaskColumn
      Caption = 'Luogo Nascita'
      Width = 85
      BandIndex = 0
      RowIndex = 0
      FieldName = 'LuogoNascita'
    end
    object dxDBGrid1ProvNascita: TdxDBGridMaskColumn
      Caption = 'Prov Nascita'
      Width = 43
      BandIndex = 0
      RowIndex = 0
      FieldName = 'ProvNascita'
    end
    object dxDBGrid1StatoNascita: TdxDBGridMaskColumn
      Caption = 'Stato Nascita'
      Width = 86
      BandIndex = 0
      RowIndex = 0
      FieldName = 'StatoNascita'
    end
    object dxDBGrid1Nazionalita: TdxDBGridMaskColumn
      Caption = 'NazionalitÓ'
      Width = 71
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Nazionalita'
    end
    object dxDBGrid1DataNascita: TdxDBGridDateColumn
      Caption = 'Data Nascita'
      Width = 80
      BandIndex = 0
      RowIndex = 0
      FieldName = 'DataNascita'
    end
    object dxDBGrid1StatoCivile: TdxDBGridMaskColumn
      Caption = 'Stato Civile'
      Width = 93
      BandIndex = 0
      RowIndex = 0
      FieldName = 'StatoCivile'
    end
    object dxDBGrid1TipoStrada: TdxDBGridMaskColumn
      Caption = 'Tipo Strada'
      Width = 62
      BandIndex = 1
      RowIndex = 0
      FieldName = 'TipoStrada'
    end
    object dxDBGrid1Indirizzo: TdxDBGridMaskColumn
      Width = 200
      BandIndex = 1
      RowIndex = 0
      FieldName = 'Indirizzo'
    end
    object dxDBGrid1NumCivico: TdxDBGridMaskColumn
      Caption = 'Num Civico'
      BandIndex = 1
      RowIndex = 0
      FieldName = 'NumCivico'
    end
    object dxDBGrid1Edificio: TdxDBGridMaskColumn
      Width = 100
      BandIndex = 1
      RowIndex = 0
      FieldName = 'Edificio'
    end
    object dxDBGrid1Presso: TdxDBGridMaskColumn
      Width = 100
      BandIndex = 1
      RowIndex = 0
      FieldName = 'Presso'
    end
    object dxDBGrid1Cap: TdxDBGridMaskColumn
      Width = 50
      BandIndex = 1
      RowIndex = 0
      FieldName = 'Cap'
    end
    object dxDBGrid1Comune: TdxDBGridMaskColumn
      Width = 200
      BandIndex = 1
      RowIndex = 0
      FieldName = 'Comune'
    end
    object dxDBGrid1Frazione: TdxDBGridMaskColumn
      Width = 100
      BandIndex = 1
      RowIndex = 0
      FieldName = 'Frazione'
    end
    object dxDBGrid1Provincia: TdxDBGridMaskColumn
      Width = 50
      BandIndex = 1
      RowIndex = 0
      FieldName = 'Provincia'
    end
    object dxDBGrid1Stato: TdxDBGridMaskColumn
      Caption = 'Nazione'
      Width = 100
      BandIndex = 1
      RowIndex = 0
      FieldName = 'Stato'
    end
    object dxDBGrid1DomicilioTipoStrada: TdxDBGridMaskColumn
      Caption = 'Tipo Strada'
      Width = 63
      BandIndex = 2
      RowIndex = 0
      FieldName = 'DomicilioTipoStrada'
    end
    object dxDBGrid1DomicilioIndirizzo: TdxDBGridMaskColumn
      Caption = 'Indirizzo'
      Width = 200
      BandIndex = 2
      RowIndex = 0
      FieldName = 'DomicilioIndirizzo'
    end
    object dxDBGrid1DomicilioNumCivico: TdxDBGridMaskColumn
      Caption = 'Num Civico'
      Width = 44
      BandIndex = 2
      RowIndex = 0
      FieldName = 'DomicilioNumCivico'
    end
    object dxDBGrid1DomicilioEdificio: TdxDBGridMaskColumn
      Caption = 'Edificio'
      Width = 80
      BandIndex = 2
      RowIndex = 0
      FieldName = 'DomicilioEdificio'
    end
    object dxDBGrid1DomicilioPresso: TdxDBGridMaskColumn
      Caption = 'Presso'
      Width = 80
      BandIndex = 2
      RowIndex = 0
      FieldName = 'DomicilioPresso'
    end
    object dxDBGrid1DomicilioCap: TdxDBGridMaskColumn
      Caption = 'Cap'
      Width = 50
      BandIndex = 2
      RowIndex = 0
      FieldName = 'DomicilioCap'
    end
    object dxDBGrid1DomicilioFrazione: TdxDBGridMaskColumn
      Caption = 'Frazione'
      Width = 100
      BandIndex = 2
      RowIndex = 0
      FieldName = 'DomicilioFrazione'
    end
    object dxDBGrid1DomicilioComune: TdxDBGridMaskColumn
      Caption = 'Comune'
      Width = 120
      BandIndex = 2
      RowIndex = 0
      FieldName = 'DomicilioComune'
    end
    object dxDBGrid1DomicilioProvincia: TdxDBGridMaskColumn
      Caption = 'Provincia'
      Width = 40
      BandIndex = 2
      RowIndex = 0
      FieldName = 'DomicilioProvincia'
    end
    object dxDBGrid1DomicilioStato: TdxDBGridMaskColumn
      Caption = 'Nazione'
      Width = 100
      BandIndex = 2
      RowIndex = 0
      FieldName = 'DomicilioStato'
    end
    object dxDBGrid1RecapitiTelefonici: TdxDBGridMaskColumn
      Caption = 'Recapiti Telefonici'
      Width = 136
      BandIndex = 3
      RowIndex = 0
      FieldName = 'RecapitiTelefonici'
    end
    object dxDBGrid1Cellulare: TdxDBGridMaskColumn
      Width = 136
      BandIndex = 3
      RowIndex = 0
      FieldName = 'Cellulare'
    end
    object dxDBGrid1TelUfficio: TdxDBGridMaskColumn
      Caption = 'Tel Ufficio'
      Width = 128
      BandIndex = 3
      RowIndex = 0
      FieldName = 'TelUfficio'
    end
    object dxDBGrid1Email: TdxDBGridMaskColumn
      Width = 149
      BandIndex = 3
      RowIndex = 0
      FieldName = 'Email'
    end
    object dxDBGrid1EmailUfficio: TdxDBGridMaskColumn
      Caption = 'Email Ufficio'
      Width = 149
      BandIndex = 3
      RowIndex = 0
      FieldName = 'EmailUfficio'
    end
    object dxDBGrid1CasellaPostale: TdxDBGridMaskColumn
      Caption = 'Casella Postale'
      Width = 100
      BandIndex = 3
      RowIndex = 0
      FieldName = 'CasellaPostale'
    end
    object dxDBGrid1CartaIdentitaNum: TdxDBGridMaskColumn
      Caption = 'Numero'
      Width = 73
      BandIndex = 4
      RowIndex = 0
      FieldName = 'CartaIdentitaNum'
    end
    object dxDBGrid1CartaIdentitaEnte: TdxDBGridMaskColumn
      Caption = 'Rilasc. da'
      Width = 133
      BandIndex = 4
      RowIndex = 0
      FieldName = 'CartaIdentitaEnte'
    end
    object dxDBGrid1CartaIdentitaDataRil: TdxDBGridDateColumn
      Caption = 'Data rilascio'
      Width = 80
      BandIndex = 4
      RowIndex = 0
      FieldName = 'CartaIdentitaDataRil'
    end
    object dxDBGrid1PassaportoNum: TdxDBGridMaskColumn
      Caption = 'Numero'
      Width = 77
      BandIndex = 5
      RowIndex = 0
      FieldName = 'PassaportoNum'
    end
    object dxDBGrid1PassaportoDataRil: TdxDBGridDateColumn
      Caption = 'Data rilascio'
      Width = 80
      BandIndex = 5
      RowIndex = 0
      FieldName = 'PassaportoDataRil'
    end
    object dxDBGrid1PassaportoEnte: TdxDBGridMaskColumn
      Caption = 'Rilasciato da'
      Width = 100
      BandIndex = 5
      RowIndex = 0
      FieldName = 'PassaportoEnte'
    end
    object dxDBGrid1PassaportoScad: TdxDBGridDateColumn
      Caption = 'Scadenza'
      Width = 80
      BandIndex = 5
      RowIndex = 0
      FieldName = 'PassaportoScad'
    end
  end
  object QStoricoAnag: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    BeforeOpen = QStoricoAnagBeforeOpen
    AfterInsert = QStoricoAnagAfterInsert
    BeforePost = QStoricoAnagBeforePost
    Parameters = <
      item
        Name = 'ID'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select * from Storicoanagrafica where idanagrafica=:ID')
    Left = 56
    Top = 128
    object QStoricoAnagID: TAutoIncField
      FieldName = 'ID'
    end
    object QStoricoAnagIDAnagrafica: TIntegerField
      FieldName = 'IDAnagrafica'
    end
    object QStoricoAnagidazienda: TIntegerField
      FieldName = 'idazienda'
    end
    object QStoricoAnagCodiceFiscale: TStringField
      FieldName = 'CodiceFiscale'
      Size = 200
    end
    object QStoricoAnagCognome: TStringField
      FieldName = 'Cognome'
      Size = 200
    end
    object QStoricoAnagNome: TStringField
      FieldName = 'Nome'
      Size = 200
    end
    object QStoricoAnagMatricola: TStringField
      FieldName = 'Matricola'
      Size = 200
    end
    object QStoricoAnagCID: TStringField
      FieldName = 'CID'
      Size = 200
    end
    object QStoricoAnagCodicePaghe: TStringField
      FieldName = 'CodicePaghe'
      Size = 200
    end
    object QStoricoAnagCodiceBadge: TStringField
      FieldName = 'CodiceBadge'
      Size = 200
    end
    object QStoricoAnagSesso: TStringField
      FieldName = 'Sesso'
      Size = 200
    end
    object QStoricoAnagDataNascita: TDateTimeField
      FieldName = 'DataNascita'
    end
    object QStoricoAnagLuogoNascita: TStringField
      FieldName = 'LuogoNascita'
      Size = 8000
    end
    object QStoricoAnagProvNascita: TStringField
      FieldName = 'ProvNascita'
      Size = 200
    end
    object QStoricoAnagStatoNascita: TStringField
      FieldName = 'StatoNascita'
      Size = 200
    end
    object QStoricoAnagIndirizzo: TStringField
      FieldName = 'Indirizzo'
      Size = 200
    end
    object QStoricoAnagEdificio: TStringField
      FieldName = 'Edificio'
      Size = 200
    end
    object QStoricoAnagPresso: TStringField
      FieldName = 'Presso'
      Size = 200
    end
    object QStoricoAnagCap: TStringField
      FieldName = 'Cap'
      Size = 200
    end
    object QStoricoAnagFrazione: TStringField
      FieldName = 'Frazione'
      Size = 200
    end
    object QStoricoAnagComune: TStringField
      FieldName = 'Comune'
      Size = 200
    end
    object QStoricoAnagProvincia: TStringField
      FieldName = 'Provincia'
      Size = 200
    end
    object QStoricoAnagStato: TStringField
      FieldName = 'Stato'
      Size = 200
    end
    object QStoricoAnagDomicilioIndirizzo: TStringField
      FieldName = 'DomicilioIndirizzo'
      Size = 200
    end
    object QStoricoAnagDomicilioEdificio: TStringField
      FieldName = 'DomicilioEdificio'
      Size = 200
    end
    object QStoricoAnagDomicilioPresso: TStringField
      FieldName = 'DomicilioPresso'
      Size = 200
    end
    object QStoricoAnagDomicilioCap: TStringField
      FieldName = 'DomicilioCap'
      Size = 200
    end
    object QStoricoAnagDomicilioFrazione: TStringField
      FieldName = 'DomicilioFrazione'
      Size = 200
    end
    object QStoricoAnagDomicilioComune: TStringField
      FieldName = 'DomicilioComune'
      Size = 200
    end
    object QStoricoAnagDomicilioProvincia: TStringField
      FieldName = 'DomicilioProvincia'
      Size = 200
    end
    object QStoricoAnagDomicilioStato: TStringField
      FieldName = 'DomicilioStato'
      Size = 200
    end
    object QStoricoAnagNazionalita: TStringField
      FieldName = 'Nazionalita'
      Size = 200
    end
    object QStoricoAnagIDStatoCivile: TIntegerField
      FieldName = 'IDStatoCivile'
    end
    object QStoricoAnagIDDIploma: TIntegerField
      FieldName = 'IDDIploma'
    end
    object QStoricoAnagRecapitiTelefonici: TStringField
      FieldName = 'RecapitiTelefonici'
      Size = 200
    end
    object QStoricoAnagCellulare: TStringField
      FieldName = 'Cellulare'
      Size = 200
    end
    object QStoricoAnagTelUfficio: TStringField
      FieldName = 'TelUfficio'
      Size = 200
    end
    object QStoricoAnagEmail: TStringField
      FieldName = 'Email'
      Size = 200
    end
    object QStoricoAnagDallaData: TDateTimeField
      FieldName = 'DallaData'
    end
    object QStoricoAnagIdStato: TIntegerField
      FieldName = 'IdStato'
    end
    object QStoricoAnagIDTipoStato: TIntegerField
      FieldName = 'IDTipoStato'
    end
    object QStoricoAnagAzienda: TStringField
      FieldName = 'Azienda'
      Size = 200
    end
    object QStoricoAnagStatoCivile: TStringField
      FieldName = 'StatoCivile'
      Size = 200
    end
    object QStoricoAnagDiploma: TStringField
      FieldName = 'Diploma'
      Size = 200
    end
    object QStoricoAnagCasellaPostale: TStringField
      FieldName = 'CasellaPostale'
      Size = 200
    end
    object QStoricoAnagEmailUfficio: TStringField
      FieldName = 'EmailUfficio'
      Size = 200
    end
    object QStoricoAnagFax: TStringField
      FieldName = 'Fax'
      Size = 200
    end
    object QStoricoAnagCartaIdentitaNum: TStringField
      FieldName = 'CartaIdentitaNum'
      Size = 200
    end
    object QStoricoAnagCartaIdentitaEnte: TStringField
      FieldName = 'CartaIdentitaEnte'
      Size = 200
    end
    object QStoricoAnagCartaIdentitaDataRil: TDateTimeField
      FieldName = 'CartaIdentitaDataRil'
    end
    object QStoricoAnagPassaportoNum: TStringField
      FieldName = 'PassaportoNum'
      Size = 200
    end
    object QStoricoAnagPassaportoDataRil: TDateTimeField
      FieldName = 'PassaportoDataRil'
    end
    object QStoricoAnagPassaportoEnte: TStringField
      FieldName = 'PassaportoEnte'
      Size = 200
    end
    object QStoricoAnagPassaportoScad: TDateTimeField
      FieldName = 'PassaportoScad'
    end
    object QStoricoAnagComunitaNazionale: TStringField
      FieldName = 'ComunitaNazionale'
      Size = 200
    end
    object QStoricoAnagTipoStrada: TStringField
      FieldName = 'TipoStrada'
      Size = 200
    end
    object QStoricoAnagNumCivico: TStringField
      FieldName = 'NumCivico'
      Size = 10
    end
    object QStoricoAnagDomicilioTipoStrada: TStringField
      FieldName = 'DomicilioTipoStrada'
      Size = 200
    end
    object QStoricoAnagDomicilioNumCivico: TStringField
      FieldName = 'DomicilioNumCivico'
      Size = 10
    end
    object QStoricoAnagCancellato: TBooleanField
      FieldName = 'Cancellato'
    end
    object QStoricoAnagStatoCivileLK: TStringField
      FieldKind = fkLookup
      FieldName = 'StatoCivileLK'
      LookupDataSet = QStatiCiviliLK
      LookupKeyFields = 'ID'
      LookupResultField = 'Descrizione'
      KeyFields = 'IDStatoCivile'
      Size = 100
      Lookup = True
    end
  end
  object DSStoricoAnag: TDataSource
    DataSet = QStoricoAnag
    Left = 96
    Top = 128
  end
  object QStatiCiviliLK: TADOQuery
    Active = True
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select *'
      'from StatiCivili')
    Left = 80
    Top = 176
    object QStatiCiviliLKID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QStatiCiviliLKDescrizione: TStringField
      FieldName = 'Descrizione'
      Size = 100
    end
  end
  object Q: TADOQuery
    Connection = Data.DB
    Parameters = <>
    Left = 40
    Top = 320
  end
  object Qtemp: TADOQuery
    Connection = Data.DB
    Parameters = <>
    Left = 272
    Top = 232
  end
end
