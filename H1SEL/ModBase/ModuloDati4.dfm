object DataAnnunci: TDataAnnunci
  OldCreateOrder = True
  OnCreate = DataAnnunciCreate
  Left = 469
  Top = 162
  Height = 642
  Width = 946
  object DsAnnunci: TDataSource
    DataSet = TAnnunci
    OnStateChange = DsAnnunciStateChange
    Left = 24
    Top = 72
  end
  object DsTestate: TDataSource
    DataSet = QTestate
    Left = 280
    Top = 72
  end
  object DsEdizioniTest: TDataSource
    DataSet = QEdizioniTest
    Left = 336
    Top = 72
  end
  object DsAnnunciRic: TDataSource
    DataSet = TAnnunciRic
    Left = 88
    Top = 69
  end
  object DsGiorniIndicati: TDataSource
    DataSet = TGiorniIndicati
    OnStateChange = DsGiorniIndicatiStateChange
    Left = 448
    Top = 232
  end
  object DsQualifiche: TDataSource
    DataSet = TQualifiche
    Left = 509
    Top = 236
  end
  object DsSconti: TDataSource
    DataSet = TSconti
    OnStateChange = DsScontiStateChange
    Left = 411
    Top = 69
  end
  object DsAnnEdizDataXann: TDataSource
    DataSet = QAnnEdizDataXAnn
    Left = 24
    Top = 240
  end
  object DsTestateLK: TDataSource
    DataSet = TTestateLK
    Left = 520
    Top = 68
  end
  object DsPenetraz: TDataSource
    DataSet = TPenetraz
    OnStateChange = DsPenetrazStateChange
    Left = 465
    Top = 69
  end
  object DsQAnnAnag: TDataSource
    DataSet = QAnnAnag
    Left = 216
    Top = 69
  end
  object DsQAnnRuoli: TDataSource
    DataSet = QAnnRuoli
    Left = 152
    Top = 72
  end
  object DsQAnnAnagData: TDataSource
    DataSet = QAnnAnagData
    Left = 96
    Top = 240
  end
  object DsQCentriCostoCGLK: TDataSource
    DataSet = QCentriCostoCGLK
    Left = 48
    Top = 456
  end
  object DsQAnnContrattiCG: TDataSource
    DataSet = QAnnContrattiCG
    Left = 150
    Top = 242
  end
  object DsQAnnTimeReport: TDataSource
    DataSet = QAnnTimeReport
    OnStateChange = DsQAnnTimeReportStateChange
    Left = 222
    Top = 242
  end
  object DsQUtentiLK: TDataSource
    DataSet = QUtentiLK
    Left = 279
    Top = 242
  end
  object DsQGruppiProfLK: TDataSource
    DataSet = QGruppiProfLK
    Left = 351
    Top = 241
  end
  object DsQContrattiCGLK: TDataSource
    DataSet = QContrattiCGLK
    Left = 122
    Top = 454
  end
  object TAnnunci: TADOLinkedQuery
    Connection = Data.DB
    CursorType = ctStatic
    AfterClose = TAnnunci_OLDAfterClose
    AfterPost = TAnnunci_OLDAfterPost
    AfterScroll = TAnnunciAfterScroll
    Parameters = <>
    SQL.Strings = (
      ''
      
        'select Ann_Annunci.ID,Rif,Data,Civetta,FileTesto,NumModuli,NumPa' +
        'role,'
      'TotaleLire,TotaleEuro,Ann_Annunci.Note,Archiviato,CVPervenuti,'
      'Ann_Annunci.RuoloWeb,'
      'CVIdonei,CVChiamati,IDFattura,Ann_Annunci.IDCliente,IDUtente,'
      'IDTipoAnnuncio,IDSede,IDUtenteProj,IDMansione,'
      'EmailHost,EmailPort,EmailUserID,EmailPassword,'
      'IDCentroCostoCG,IDContrattoCG,LKCliente,LKMansione,'
      'JobTitle,JobZone,JobDescription, fileannunciohtml, InEvidenza,'
      'Skills,IDQuestionario,  EBC_Clienti.Descrizione Cliente, '
      
        '          Users1.Nominativo Utente, Users2.Nominativo UtenteProj' +
        ','
      
        '          Mansioni.Descrizione RuoloAnnuncio,VisibilitaAnnunciWe' +
        'b,'
      
        '          Aziende.descrizione Sede, Ann_TipiAnnuncio.TipoAnnunci' +
        'o, QualeRuoloUsare,'
      
        '          IDIndirizzoCliente ,EBC_ClienteIndirizzi.Comune Indiri' +
        'zzoCliente, EBC_ClienteIndirizzi.Descrizione DescIndirizzo, View' +
        '_RegProv'
      
        'from Ann_Annunci left join EBC_Clienti on Ann_Annunci.IDCliente ' +
        '= EBC_Clienti.ID'
      'left join Users Users1 on Ann_Annunci.IDUtente = Users1.ID'
      'left join Users Users2 on Ann_Annunci.IDUtenteProj = Users2.ID'
      'left join Mansioni on Ann_Annunci.IDMansione = Mansioni.ID'
      'left join Aziende on Ann_Annunci.IDSede = Aziende.ID'
      
        'left join Ann_TipiAnnuncio on Ann_Annunci.IDtipoAnnuncio = Ann_T' +
        'ipiAnnuncio.ID'
      
        'left join EBC_ClienteIndirizzi on Ann_Annunci.IDIndirizzoCliente' +
        ' = EBC_ClienteIndirizzi.ID'
      '')
    OriginalSQL.Strings = (
      ''
      
        'select Ann_Annunci.ID,Rif,Data,Civetta,FileTesto,NumModuli,NumPa' +
        'role,'
      'TotaleLire,TotaleEuro,Ann_Annunci.Note,Archiviato,CVPervenuti,'
      'Ann_Annunci.RuoloWeb,'
      'CVIdonei,CVChiamati,IDFattura,Ann_Annunci.IDCliente,IDUtente,'
      'IDTipoAnnuncio,IDSede,IDUtenteProj,IDMansione,'
      'EmailHost,EmailPort,EmailUserID,EmailPassword,'
      'IDCentroCostoCG,IDContrattoCG,LKCliente,LKMansione,'
      'JobTitle,JobZone,JobDescription, fileannunciohtml, InEvidenza,'
      'Skills,IDQuestionario,  EBC_Clienti.Descrizione Cliente, '
      
        '          Users1.Nominativo Utente, Users2.Nominativo UtenteProj' +
        ','
      
        '          Mansioni.Descrizione RuoloAnnuncio,VisibilitaAnnunciWe' +
        'b,'
      
        '          Aziende.descrizione Sede, Ann_TipiAnnuncio.TipoAnnunci' +
        'o, QualeRuoloUsare,'
      
        '          IDIndirizzoCliente ,EBC_ClienteIndirizzi.Comune Indiri' +
        'zzoCliente, EBC_ClienteIndirizzi.Descrizione DescIndirizzo, View' +
        '_RegProv'
      
        'from Ann_Annunci left join EBC_Clienti on Ann_Annunci.IDCliente ' +
        '= EBC_Clienti.ID'
      'left join Users Users1 on Ann_Annunci.IDUtente = Users1.ID'
      'left join Users Users2 on Ann_Annunci.IDUtenteProj = Users2.ID'
      'left join Mansioni on Ann_Annunci.IDMansione = Mansioni.ID'
      'left join Aziende on Ann_Annunci.IDSede = Aziende.ID'
      
        'left join Ann_TipiAnnuncio on Ann_Annunci.IDtipoAnnuncio = Ann_T' +
        'ipiAnnuncio.ID'
      
        'left join EBC_ClienteIndirizzi on Ann_Annunci.IDIndirizzoCliente' +
        ' = EBC_ClienteIndirizzi.ID'
      ''
      '')
    UseFilter = False
    Left = 24
    Top = 120
    object TAnnunciID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object TAnnunciRif: TStringField
      FieldName = 'Rif'
      Size = 10
    end
    object TAnnunciData: TDateTimeField
      FieldName = 'Data'
    end
    object TAnnunciFileTesto: TStringField
      FieldName = 'FileTesto'
      Size = 100
    end
    object TAnnunciNumModuli: TSmallintField
      FieldName = 'NumModuli'
    end
    object TAnnunciNumParole: TSmallintField
      FieldName = 'NumParole'
    end
    object TAnnunciTotaleLire: TFloatField
      FieldName = 'TotaleLire'
    end
    object TAnnunciCivetta: TBooleanField
      FieldName = 'Civetta'
    end
    object TAnnunciTotaleEuro: TFloatField
      FieldName = 'TotaleEuro'
    end
    object TAnnunciNote: TMemoField
      FieldName = 'Note'
      BlobType = ftMemo
    end
    object TAnnunciArchiviato: TBooleanField
      FieldName = 'Archiviato'
    end
    object TAnnunciCVPervenuti: TSmallintField
      FieldName = 'CVPervenuti'
    end
    object TAnnunciCVIdonei: TSmallintField
      FieldName = 'CVIdonei'
    end
    object TAnnunciCVChiamati: TSmallintField
      FieldName = 'CVChiamati'
    end
    object TAnnunciIDFattura: TIntegerField
      FieldName = 'IDFattura'
    end
    object TAnnunciIDCliente: TIntegerField
      FieldName = 'IDCliente'
    end
    object TAnnunciIDUtente: TIntegerField
      FieldName = 'IDUtente'
    end
    object TAnnunciIDTipoAnnuncio: TIntegerField
      FieldName = 'IDTipoAnnuncio'
    end
    object TAnnunciIDSede: TIntegerField
      FieldName = 'IDSede'
    end
    object TAnnunciIDUtenteProj: TIntegerField
      FieldName = 'IDUtenteProj'
    end
    object TAnnunciIDMansione: TIntegerField
      FieldName = 'IDMansione'
    end
    object TAnnunciEmailHost: TStringField
      FieldName = 'EmailHost'
      Size = 50
    end
    object TAnnunciEmailPort: TStringField
      FieldName = 'EmailPort'
      Size = 10
    end
    object TAnnunciEmailUserID: TStringField
      FieldName = 'EmailUserID'
      Size = 40
    end
    object TAnnunciEmailPassword: TStringField
      FieldName = 'EmailPassword'
    end
    object TAnnunciLKCliente: TStringField
      FieldName = 'LKCliente'
      Size = 100
    end
    object TAnnunciLKMansione: TStringField
      FieldName = 'LKMansione'
      Size = 60
    end
    object TAnnunciCliente: TStringField
      FieldName = 'Cliente'
      Size = 50
    end
    object TAnnunciUtente: TStringField
      FieldName = 'Utente'
      Size = 30
    end
    object TAnnunciUtenteProj: TStringField
      FieldName = 'UtenteProj'
      Size = 30
    end
    object TAnnunciRuoloAnnuncio: TStringField
      FieldName = 'RuoloAnnuncio'
      Size = 40
    end
    object TAnnunciSede: TStringField
      FieldName = 'Sede'
      Size = 30
    end
    object TAnnunciTipoAnnuncio: TStringField
      FieldName = 'TipoAnnuncio'
      Size = 60
    end
    object TAnnunciIDCentroCostoCG: TIntegerField
      FieldName = 'IDCentroCostoCG'
    end
    object TAnnunciIDContrattoCG: TIntegerField
      FieldName = 'IDContrattoCG'
    end
    object TAnnunciCodiceContrattoCG: TStringField
      FieldKind = fkLookup
      FieldName = 'CodiceContrattoCG'
      LookupDataSet = QContrattiCGLK
      LookupKeyFields = 'ID'
      LookupResultField = 'CodiceContratto'
      KeyFields = 'IDContrattoCG'
      Lookup = True
    end
    object TAnnunciCentroCostoCG: TStringField
      FieldKind = fkLookup
      FieldName = 'CentroCostoCG'
      LookupDataSet = QCentriCostoCGLK
      LookupKeyFields = 'ID'
      LookupResultField = 'CentroCosto'
      KeyFields = 'IDCentroCostoCG'
      Size = 50
      Lookup = True
    end
    object TAnnunciProgFattura: TStringField
      FieldKind = fkLookup
      FieldName = 'ProgFattura'
      LookupDataSet = TFattureLK
      LookupKeyFields = 'ID'
      LookupResultField = 'Progressivo'
      KeyFields = 'IDFattura'
      Size = 10
      Lookup = True
    end
    object TAnnunciJobTitle: TStringField
      FieldName = 'JobTitle'
      Size = 50
    end
    object TAnnunciJobZone: TStringField
      FieldName = 'JobZone'
      Size = 100
    end
    object TAnnunciSkills: TStringField
      FieldName = 'Skills'
      Size = 512
    end
    object TAnnunciJobDescription: TStringField
      FieldName = 'JobDescription'
      Size = 1024
    end
    object TAnnunciQualeRuoloUsare: TSmallintField
      FieldName = 'QualeRuoloUsare'
    end
    object TAnnunciIndirizzoCliente: TStringField
      FieldName = 'IndirizzoCliente'
      Size = 100
    end
    object TAnnunciIDIndirizzoCliente: TIntegerField
      FieldName = 'IDIndirizzoCliente'
    end
    object TAnnunciDescIndirizzo: TStringField
      FieldName = 'DescIndirizzo'
      Size = 100
    end
    object TAnnunciIDQuestionario: TIntegerField
      FieldName = 'IDQuestionario'
    end
    object TAnnunciQuestionario: TStringField
      FieldKind = fkLookup
      FieldName = 'Questionario'
      LookupDataSet = QQuestLK
      LookupKeyFields = 'ID'
      LookupResultField = 'TipoDomanda'
      KeyFields = 'IDQuestionario'
      Size = 50
      Lookup = True
    end
    object TAnnuncifileannunciohtml: TMemoField
      FieldName = 'fileannunciohtml'
      BlobType = ftMemo
    end
    object TAnnunciView_RegProv: TStringField
      FieldName = 'View_RegProv'
      Size = 256
    end
    object TAnnunciInEvidenza: TBooleanField
      FieldName = 'InEvidenza'
    end
    object TAnnunciVisibilitaAnnunciWeb: TBooleanField
      FieldName = 'VisibilitaAnnunciWeb'
    end
    object TAnnunciRuoloWeb: TStringField
      FieldName = 'RuoloWeb'
      Size = 100
    end
  end
  object QAnnRuoli: TADOLinkedQuery
    Connection = Data.DB
    Filtered = True
    Parameters = <>
    SQL.Strings = (
      
        'select Ann_AnnunciRuoli.*, Mansioni.Descrizione Ruolo from Ann_A' +
        'nnunciRuoli,Mansioni where/*BEGIN_AUTO_ADD*/ ( /*END_AUTO_ADD*/ ' +
        'Ann_AnnunciRuoli.IDMansione=Mansioni.ID/*BEGIN_AUTO_ADD*/ ) /*EN' +
        'D_AUTO_ADD*//*BEGIN_AUTO_ADD*/ AND (IDAnnuncio=630)/*END_AUTO_AD' +
        'D*/')
    MasterDataSet = TAnnunci
    LinkedMasterField = 'ID'
    LinkedDetailField = 'IDAnnuncio'
    UseFilter = False
    Left = 152
    Top = 120
  end
  object QTestate: TADOLinkedQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from Ann_Testate'
      'order by Denominazione')
    UseFilter = False
    Left = 280
    Top = 120
  end
  object QEdizioniTest_old: TADOLinkedQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      ''
      'select Ann_Edizioni.*, EBC_Clienti.descrizione Concessionario '
      'from Ann_Edizioni left join EBC_Clienti '
      'on Ann_Edizioni.IDCliente = EBC_Clienti.ID'
      'where IDTestata = 7')
    MasterDataSet = QTestate
    LinkedMasterField = 'ID'
    LinkedDetailField = 'IDTestata'
    UseFilter = False
    Left = 336
    Top = 176
  end
  object QAnnEdizDataXAnn: TADOLinkedQuery
    Connection = Data.DB
    CursorType = ctStatic
    Filtered = True
    AfterOpen = QAnnEdizDataXAnnAfterOpen
    BeforeEdit = QAnnEdizDataXAnnBeforeEdit
    AfterPost = QAnnEdizDataXAnnAfterPost
    OnCalcFields = QAnnEdizDataXAnn_OLDCalcFields
    Parameters = <>
    SQL.Strings = (
      
        ' select Ann_AnnEdizData.*,IDAnnuncio,IDEdizione,IDMP_Sito,      ' +
        '    Ann_Testate.denominazione Testata,NomeEdizione Edizione,  Gi' +
        'ornoRPQ from  Ann_AnnEdizData join Ann_Edizioni on Ann_AnnEdizDa' +
        'ta.IDEdizione=Ann_Edizioni.ID    join Ann_Testate on  Ann_Edizio' +
        'ni.IDTestata=Ann_Testate.ID   /*BEGIN_AUTO_ADD*/ WHERE (IDAnnunc' +
        'io=630)/*END_AUTO_ADD*/order by Data')
    MasterDataSet = TAnnunci
    LinkedMasterField = 'ID'
    LinkedDetailField = 'IDAnnuncio'
    OriginalSQL.Strings = (
      'select Ann_AnnEdizData.*,IDAnnuncio,IDEdizione,Data, IDMP_Sito,'
      
        '       Ann_Testate.denominazione Testata,NomeEdizione Edizione, ' +
        'GiornoRPQ'
      'from Ann_AnnEdizData,Ann_Edizioni,Ann_Testate '
      'where Ann_AnnEdizData.IDEdizione=Ann_Edizioni.ID'
      '  and Ann_Edizioni.IDTestata=Ann_Testate.ID'
      'and Ann_AnnEdizData.IDAnnuncio=:ID:'
      'order by Data')
    UseFilter = False
    Left = 24
    Top = 296
    object QAnnEdizDataXAnnID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QAnnEdizDataXAnnIDAnnuncio: TIntegerField
      FieldName = 'IDAnnuncio'
    end
    object QAnnEdizDataXAnnIDEdizione: TIntegerField
      FieldName = 'IDEdizione'
    end
    object QAnnEdizDataXAnnData: TDateTimeField
      FieldName = 'Data'
    end
    object QAnnEdizDataXAnnCostoLire: TFloatField
      FieldName = 'CostoLire'
    end
    object QAnnEdizDataXAnnCostoEuro: TFloatField
      FieldName = 'CostoEuro'
    end
    object QAnnEdizDataXAnnCostoANoiLire: TFloatField
      FieldName = 'CostoANoiLire'
    end
    object QAnnEdizDataXAnnCostoANoiEuro: TFloatField
      FieldName = 'CostoANoiEuro'
    end
    object QAnnEdizDataXAnnScontoAlClienteLire: TFloatField
      FieldName = 'ScontoAlClienteLire'
    end
    object QAnnEdizDataXAnnScontoAlClienteEuro: TFloatField
      FieldName = 'ScontoAlClienteEuro'
    end
    object QAnnEdizDataXAnnScontoANoiLire: TFloatField
      FieldName = 'ScontoANoiLire'
    end
    object QAnnEdizDataXAnnScontoANoiEuro: TFloatField
      FieldName = 'ScontoANoiEuro'
    end
    object QAnnEdizDataXAnnNumModuli: TIntegerField
      FieldName = 'NumModuli'
    end
    object QAnnEdizDataXAnnCodice: TStringField
      FieldName = 'Codice'
      Size = 4
    end
    object QAnnEdizDataXAnnListinoLire: TFloatField
      FieldName = 'ListinoLire'
    end
    object QAnnEdizDataXAnnListinoEuro: TFloatField
      FieldName = 'ListinoEuro'
    end
    object QAnnEdizDataXAnnDataScadenza: TDateTimeField
      FieldName = 'DataScadenza'
    end
    object QAnnEdizDataXAnnDataInvioJP: TDateTimeField
      FieldName = 'DataInvioJP'
    end
    object QAnnEdizDataXAnnIDMP_Sito: TIntegerField
      FieldName = 'IDMP_Sito'
    end
    object QAnnEdizDataXAnnIDAnnuncio_1: TIntegerField
      FieldName = 'IDAnnuncio_1'
    end
    object QAnnEdizDataXAnnIDEdizione_1: TIntegerField
      FieldName = 'IDEdizione_1'
    end
    object QAnnEdizDataXAnnIDMP_Sito_1: TIntegerField
      FieldName = 'IDMP_Sito_1'
    end
    object QAnnEdizDataXAnnTestata: TStringField
      FieldName = 'Testata'
      Size = 50
    end
    object QAnnEdizDataXAnnEdizione: TStringField
      FieldName = 'Edizione'
      Size = 50
    end
    object QAnnEdizDataXAnnGiornoRPQ: TSmallintField
      FieldName = 'GiornoRPQ'
    end
    object QAnnEdizDataXAnnMPSito: TStringField
      FieldKind = fkLookup
      FieldName = 'MPSito'
      LookupDataSet = QMPSitiLK
      LookupKeyFields = 'ID'
      LookupResultField = 'Nome'
      KeyFields = 'IDMP_Sito'
      Size = 50
      Lookup = True
    end
    object QAnnEdizDataXAnnGiorno: TStringField
      FieldKind = fkCalculated
      FieldName = 'Giorno'
      Size = 3
      Calculated = True
    end
    object QAnnEdizDataXAnnTipoGiorno: TStringField
      FieldKind = fkCalculated
      FieldName = 'TipoGiorno'
      Size = 3
      Calculated = True
    end
    object QAnnEdizDataXAnnDataUltimoPrelievoStatici: TDateTimeField
      FieldName = 'DataUltimoPrelievoStatici'
    end
    object QAnnEdizDataXAnnFeed_Facebook: TBooleanField
      FieldName = 'Feed_Facebook'
    end
    object QAnnEdizDataXAnnFeed_Linkedin: TBooleanField
      FieldName = 'Feed_Linkedin'
    end
    object QAnnEdizDataXAnnFeed_RaccoltaEBC: TBooleanField
      FieldName = 'Feed_RaccoltaEBC'
    end
  end
  object QAnnAnagData: TADOLinkedQuery
    Connection = Data.DB
    Parameters = <>
    SQL.Strings = (
      
        'select Ann_AnagAnnEdizData.ID,IDAnnEdizData,DataPervenuto,      ' +
        '  CVNumero,Cognome,Nome from Ann_AnagAnnEdizData,Anagrafica wher' +
        'e/*BEGIN_AUTO_ADD*/ ( /*END_AUTO_ADD*/ Ann_AnagAnnEdizData.IDAna' +
        'grafica=Anagrafica.ID/*BEGIN_AUTO_ADD*/ ) /*END_AUTO_ADD*//*BEGI' +
        'N_AUTO_ADD*/ AND (IDAnnEdizData=1016)/*END_AUTO_ADD*/')
    MasterDataSet = QAnnEdizDataXAnn
    LinkedMasterField = 'ID'
    LinkedDetailField = 'IDAnnEdizData'
    OriginalSQL.Strings = (
      'select Ann_AnagAnnEdizData.ID,IDAnnEdizData,DataPervenuto,'
      '       CVNumero,Cognome,Nome'
      'from Ann_AnagAnnEdizData,Anagrafica'
      'where Ann_AnagAnnEdizData.IDAnagrafica=Anagrafica.ID'
      'and IDAnnEdizData=:ID:')
    UseFilter = False
    Left = 96
    Top = 280
  end
  object QAnnContrattiCG: TADOLinkedQuery
    Connection = Data.DB
    Filtered = True
    Parameters = <>
    SQL.Strings = (
      
        'select CG_ContrattiAnnunci.ID, CodiceContratto, CG_Contratti.Des' +
        'crizione from CG_ContrattiAnnunci, CG_Contratti where/*BEGIN_AUT' +
        'O_ADD*/ ( /*END_AUTO_ADD*/ CG_ContrattiAnnunci.IDContratto=CG_Co' +
        'ntratti.ID/*BEGIN_AUTO_ADD*/ ) /*END_AUTO_ADD*//*BEGIN_AUTO_ADD*' +
        '/ AND (IDAnnuncio=630)/*END_AUTO_ADD*/')
    MasterDataSet = TAnnunci
    LinkedMasterField = 'ID'
    LinkedDetailField = 'IDAnnuncio'
    OriginalSQL.Strings = (
      
        'select CG_ContrattiAnnunci.ID, CodiceContratto, CG_Contratti.Des' +
        'crizione'
      'from CG_ContrattiAnnunci, CG_Contratti'
      'where CG_ContrattiAnnunci.IDContratto=CG_Contratti.ID'
      'and CG_ContrattiAnnunci.IDAnnuncio=:ID')
    UseFilter = False
    Left = 152
    Top = 296
  end
  object QAnnTimeReport: TADOLinkedQuery
    Connection = Data.DB
    Filtered = True
    BeforeOpen = QAnnTimeReport_OLDBeforeOpen
    AfterClose = QAnnTimeReport_OLDAfterClose
    AfterInsert = QAnnTimeReport_OLDAfterInsert
    AfterPost = QAnnTimeReport_OLDAfterPost
    BeforeDelete = QAnnTimeReport_OLDBeforeDelete
    AfterDelete = QAnnTimeReport_OLDAfterDelete
    OnNewRecord = QAnnTimeReport_OLDNewRecord
    Parameters = <>
    SQL.Strings = (
      
        'select Ann_AnnunciTimeSheet.* from Ann_AnnunciTimeSheet/*BEGIN_A' +
        'UTO_ADD*/ WHERE (IDAnnuncio=630)/*END_AUTO_ADD*/')
    MasterDataSet = TAnnunci
    LinkedMasterField = 'ID'
    LinkedDetailField = 'IDAnnuncio'
    UseFilter = False
    Left = 224
    Top = 288
    object QAnnTimeReportID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QAnnTimeReportIDAnnuncio: TIntegerField
      FieldName = 'IDAnnuncio'
    end
    object QAnnTimeReportIDUtente: TIntegerField
      FieldName = 'IDUtente'
    end
    object QAnnTimeReportData: TDateTimeField
      FieldName = 'Data'
    end
    object QAnnTimeReportIDGruppoProfess: TIntegerField
      FieldName = 'IDGruppoProfess'
    end
    object QAnnTimeReportOreConsuntivo: TFloatField
      FieldName = 'OreConsuntivo'
    end
    object QAnnTimeReportNote: TMemoField
      FieldName = 'Note'
      BlobType = ftMemo
    end
    object QAnnTimeReportOrePreventivo: TIntegerField
      FieldName = 'OrePreventivo'
    end
    object QAnnTimeReportUtente: TStringField
      FieldKind = fkLookup
      FieldName = 'Utente'
      LookupDataSet = QUtentiLK
      LookupKeyFields = 'ID'
      LookupResultField = 'Nominativo'
      KeyFields = 'IDUtente'
      Size = 30
      Lookup = True
    end
    object QAnnTimeReportGruppoProfess: TStringField
      FieldKind = fkLookup
      FieldName = 'GruppoProfess'
      LookupDataSet = QGruppiProfLK
      LookupKeyFields = 'ID'
      LookupResultField = 'GruppoProfessionale'
      KeyFields = 'IDGruppoProfess'
      Size = 30
      Lookup = True
    end
  end
  object QUtentiLK: TADOLinkedQuery
    Connection = Data.DB
    BeforePost = QUtentiLKBeforePost
    Parameters = <
      item
        Name = 'xoggi'
        Attributes = [paNullable]
        DataType = ftDateTime
        Precision = 16
        Size = 16
        Value = Null
      end
      item
        Name = 'xoggi'
        Attributes = [paNullable]
        DataType = ftDateTime
        Precision = 16
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select ID,Nominativo,Descrizione,IDGruppoProfess'
      'from Users '
      'where not ((DataScadenza is not null and DataScadenza<:xoggi)'
      '   or (DataRevoca is not null and DataRevoca<:xoggi)'
      '   or DataCreazione is null'
      '   or Tipo=0)'
      'order by Nominativo')
    OriginalSQL.Strings = (
      'select ID,Nominativo,Descrizione,IDGruppoProfess'
      'from Users '
      'where not ((DataScadenza is not null and DataScadenza<:xoggi:)'
      '   or (DataRevoca is not null and DataRevoca<:xoggi:)'
      '   or DataCreazione is null'
      '   or Tipo=0)'
      'order by Nominativo')
    UseFilter = False
    Left = 288
    Top = 288
    object QUtentiLKID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QUtentiLKNominativo: TStringField
      FieldName = 'Nominativo'
      FixedChar = True
      Size = 30
    end
    object QUtentiLKDescrizione: TStringField
      FieldName = 'Descrizione'
      FixedChar = True
      Size = 50
    end
    object QUtentiLKIDGruppoProfess: TIntegerField
      FieldName = 'IDGruppoProfess'
    end
  end
  object QGruppiProfLK: TADOLinkedQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from CG_GruppiProfess')
    UseFilter = False
    Left = 352
    Top = 288
    object QGruppiProfLKID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QGruppiProfLKGruppoProfessionale: TStringField
      FieldName = 'GruppoProfessionale'
      FixedChar = True
      Size = 30
    end
    object QGruppiProfLKRateOrarioPrev: TFloatField
      FieldName = 'RateOrarioPrev'
    end
    object QGruppiProfLKRateOrarioCons: TFloatField
      FieldName = 'RateOrarioCons'
    end
    object QGruppiProfLKIDMansione: TIntegerField
      FieldName = 'IDMansione'
    end
    object QGruppiProfLKRateOrarioDirectPrev: TFloatField
      FieldName = 'RateOrarioDirectPrev'
    end
    object QGruppiProfLKRateOrarioDirectCons: TFloatField
      FieldName = 'RateOrarioDirectCons'
    end
  end
  object Q: TADOLinkedQuery
    Connection = Data.DB
    Parameters = <>
    SQL.Strings = (
      'select * from Ann_Edizioni ')
    UseFilter = False
    Left = 192
    Top = 455
  end
  object QTemp: TADOLinkedQuery
    Connection = Data.DB
    Parameters = <>
    UseFilter = False
    Left = 240
    Top = 399
  end
  object QCentriCostoCGLK: TADOLinkedQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from CG_CentriCosto'
      'order by CentroCosto')
    UseFilter = False
    Left = 40
    Top = 504
    object QCentriCostoCGLKID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QCentriCostoCGLKCodice: TStringField
      FieldName = 'Codice'
      Size = 10
    end
    object QCentriCostoCGLKCentroCosto: TStringField
      FieldName = 'CentroCosto'
      Size = 50
    end
    object QCentriCostoCGLKNote: TMemoField
      FieldName = 'Note'
      BlobType = ftMemo
    end
  end
  object QContrattiCGLK: TADOLinkedQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select ID,CodiceContratto,Descrizione'
      'from CG_Contratti'
      'order by CodiceContratto')
    UseFilter = False
    Left = 128
    Top = 504
    object QContrattiCGLKID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QContrattiCGLKCodiceContratto: TStringField
      FieldName = 'CodiceContratto'
    end
    object QContrattiCGLKDescrizione: TStringField
      FieldName = 'Descrizione'
      Size = 100
    end
  end
  object TSconti: TADOLinkedTable
    Connection = Data.DB
    TableName = 'dbo.Ann_Sconti'
    MasterDataSet = QEdizioniTest_old
    LinkedMasterField = 'ID'
    LinkedDetailField = 'IDEdizione'
    Left = 408
    Top = 120
  end
  object TPenetraz: TADOLinkedTable
    Connection = Data.DB
    TableName = 'dbo.Ann_Penetrazioni'
    MasterDataSet = QEdizioniTest_old
    LinkedMasterField = 'ID'
    LinkedDetailField = 'IDEdizione'
    Left = 464
    Top = 120
  end
  object TTestateLK: TADOLinkedTable
    Connection = Data.DB
    TableName = 'dbo.Ann_Testate'
    Left = 520
    Top = 120
  end
  object TGiorniIndicati: TADOLinkedTable
    Connection = Data.DB
    TableName = 'dbo.Ann_GiorniIndicati'
    MasterDataSet = QEdizioniTest_old
    LinkedMasterField = 'ID'
    LinkedDetailField = 'IDEdizione'
    Left = 448
    Top = 280
  end
  object TQualifiche: TADOLinkedTable
    Connection = Data.DB
    TableName = 'dbo.Ann_Qualifiche'
    Left = 512
    Top = 280
  end
  object TEdizioniLK: TADOLinkedTable
    Connection = Data.DB
    TableName = 'dbo.Ann_Edizioni'
    Left = 560
    Top = 232
  end
  object TAnagABS: TADOLinkedTable
    Connection = Data.DB
    TableName = 'dbo.Anagrafica'
    Left = 304
    Top = 400
  end
  object TFattureLK: TADOLinkedTable
    Connection = Data.DB
    CursorType = ctStatic
    TableName = 'dbo.Fatture'
    Left = 440
    Top = 384
    object TFattureLKID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object TFattureLKProgressivo: TStringField
      FieldName = 'Progressivo'
      Size = 50
    end
  end
  object TFestivi: TADOLinkedTable
    Connection = Data.DB
    TableName = 'dbo.GiorniFestivi'
    Left = 568
    Top = 384
  end
  object TAnnunciRicIDX: TADOLinkedTable
    Connection = Data.DB
    TableName = 'dbo.Ann_AnnunciRicerche'
    Left = 384
    Top = 400
  end
  object TGiorniSettLK: TADOLinkedTable
    Connection = Data.DB
    TableName = 'dbo.GiorniSettimana'
    Left = 440
    Top = 480
  end
  object TScontiIDX: TADOLinkedTable
    Connection = Data.DB
    TableName = 'dbo.Ann_Sconti'
    Left = 504
    Top = 480
  end
  object TAnnunciRic: TADOQuery
    Connection = Data.DB
    DataSource = DsAnnunci
    Parameters = <
      item
        Name = 'ID'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'select Ann_AnnunciRicerche.IDAnnuncio,Ann_AnnunciRicerche.IDRice' +
        'rca,Ann_AnnunciRicerche.Codice,'
      
        '       Ann_AnnunciRicerche.Note,Ann_AnnunciRicerche.ID,EBC_Ricer' +
        'che.IDUtente,'
      
        '       EBC_Ricerche.Progressivo,EBC_Ricerche.DataInizio,EBC_Rice' +
        'rche.NumRicercati,'
      
        '       coalesce(Mansioni.Descrizione,EBC_Ricerche.TitoloCliente)' +
        ' Mansione,EBC_Clienti.Descrizione Cliente'
      'from Ann_AnnunciRicerche join EBC_Ricerche'
      'on Ann_AnnunciRicerche.IDRicerca=EBC_Ricerche.ID'
      'left join Mansioni'
      'on  EBC_Ricerche.IDMansione=Mansioni.ID'
      'join EBC_Clienti'
      'on EBC_Ricerche.IDCliente=EBC_Clienti.ID'
      'where Ann_AnnunciRicerche.IDAnnuncio=:ID'
      ''
      '')
    Left = 88
    Top = 120
  end
  object QAnnFile_old: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    BeforeOpen = QAnnFile_oldBeforeOpen
    Parameters = <>
    SQL.Strings = (
      'select ID,FileAnnuncio'
      'from Ann_Annunci'
      'where ID=1')
    Left = 256
    Top = 480
    object QAnnFile_oldID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QAnnFile_oldFileAnnuncio: TMemoField
      FieldName = 'FileAnnuncio'
      BlobType = ftBlob
    end
  end
  object QAnnFile: TADOQuery
    Connection = Data.DB
    BeforeOpen = QAnnFileBeforeOpen
    Parameters = <>
    SQL.Strings = (
      'select ID,FileAnnuncio, FileNome, FileType, FileBloccato'
      'from Ann_Annunci'
      'where ID=1')
    Left = 328
    Top = 488
    object QAnnFileID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QAnnFileFileAnnuncio: TMemoField
      FieldName = 'FileAnnuncio'
      BlobType = ftMemo
    end
    object QAnnFileFileNome: TStringField
      FieldName = 'FileNome'
      Size = 30
    end
    object QAnnFileFileType: TStringField
      DisplayWidth = 30
      FieldName = 'FileType'
      Size = 30
    end
    object QAnnFileFileBloccato: TBooleanField
      FieldName = 'FileBloccato'
    end
  end
  object qTestoAnn: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    BeforePost = qTestoAnnBeforePost
    DataSource = DsAnnunci
    Parameters = <
      item
        Name = 'id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select testo,solotesto,testopulito'
      'from ann_annunci'
      'where id = :id')
    Left = 632
    Top = 280
    object qTestoAnntesto: TMemoField
      FieldName = 'testo'
      BlobType = ftMemo
    end
    object qTestoAnnsolotesto: TMemoField
      FieldName = 'solotesto'
      BlobType = ftMemo
    end
    object qTestoAnntestopulito: TMemoField
      FieldName = 'testopulito'
      BlobType = ftMemo
    end
  end
  object dsTestoAnn: TDataSource
    DataSet = qTestoAnn
    Left = 624
    Top = 224
  end
  object QMPSitiLK: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select ID,Nome'
      'from MP_Siti')
    Left = 24
    Top = 352
    object QMPSitiLKID: TIntegerField
      FieldName = 'ID'
    end
    object QMPSitiLKNome: TStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object QPubb_Spec: TADOQuery
    Connection = Data.DB
    DataSource = DsAnnEdizDataXann
    Parameters = <
      item
        Name = 'ID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select PS.*, cast(T.Note as varchar) Note'
      'from MP_Pubb_Spec PS'
      'join MP_Siti_Tabelle T on PS.IDTabSito = T.ID'
      'where PS.IDAnnEdizData=:ID')
    Left = 88
    Top = 352
    object QPubb_SpecID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QPubb_SpecIDAnnEdizData: TIntegerField
      FieldName = 'IDAnnEdizData'
    end
    object QPubb_SpecIDTabSito: TIntegerField
      FieldName = 'IDTabSito'
    end
    object QPubb_SpecValore: TStringField
      FieldName = 'Valore'
      Size = 100
    end
    object QPubb_SpecDescValore: TStringField
      FieldName = 'DescValore'
      Size = 256
    end
    object QPubb_SpecNote: TStringField
      FieldName = 'Note'
      ReadOnly = True
      Size = 30
    end
    object QPubb_SpecNomeTabella: TStringField
      FieldKind = fkLookup
      FieldName = 'NomeTabella'
      LookupDataSet = QSitiTabLK
      LookupKeyFields = 'ID'
      LookupResultField = 'NomeTabella'
      KeyFields = 'IDTabSito'
      Size = 50
      Lookup = True
    end
    object QPubb_SpecDescTabella: TStringField
      FieldKind = fkLookup
      FieldName = 'DescTabella'
      LookupDataSet = QSitiTabLK
      LookupKeyFields = 'ID'
      LookupResultField = 'Descrizione'
      KeyFields = 'IDTabSito'
      Size = 256
      Lookup = True
    end
    object QPubb_SpecQueryPickUp: TStringField
      FieldKind = fkLookup
      FieldName = 'QueryPickUp'
      LookupDataSet = QSitiTabLK
      LookupKeyFields = 'ID'
      LookupResultField = 'QueryPickUp'
      KeyFields = 'IDTabSito'
      Size = 512
      Lookup = True
    end
  end
  object DsQPubb_Spec: TDataSource
    DataSet = QPubb_Spec
    Left = 88
    Top = 400
  end
  object QSitiTabLK: TADOQuery
    Connection = Data.DB
    Parameters = <>
    SQL.Strings = (
      
        'select ID, IDSito, NomeTabella, Descrizione, QueryPickUp,cast(No' +
        'te as varchar(50)) Note'
      'from MP_Siti_Tabelle')
    Left = 144
    Top = 368
    object QSitiTabLKID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QSitiTabLKIDSito: TIntegerField
      FieldName = 'IDSito'
    end
    object QSitiTabLKNomeTabella: TStringField
      FieldName = 'NomeTabella'
      Size = 50
    end
    object QSitiTabLKDescrizione: TStringField
      FieldName = 'Descrizione'
      Size = 256
    end
    object QSitiTabLKQueryPickUp: TStringField
      FieldName = 'QueryPickUp'
      Size = 512
    end
    object QSitiTabLKNote: TStringField
      FieldName = 'Note'
      ReadOnly = True
      Size = 50
    end
  end
  object QSiti2: TADOQuery
    Connection = Data.DB
    AfterOpen = QSiti2AfterOpen
    Parameters = <
      item
        Name = 'x'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 1
      end>
    SQL.Strings = (
      'select ID,Nome,OutputFileName,OutputFileDir,'
      '     TipoConnessione,FTP_host,FTP_Username,FTP_password,FTP_dir'
      'from MP_Siti'
      'where ID=:x')
    Left = 584
    Top = 448
    object QSiti2ID: TIntegerField
      FieldName = 'ID'
    end
    object QSiti2Nome: TStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QSiti2OutputFileName: TStringField
      FieldName = 'OutputFileName'
      Size = 100
    end
    object QSiti2OutputFileDir: TStringField
      FieldName = 'OutputFileDir'
      Size = 256
    end
    object QSiti2TipoConnessione: TStringField
      FieldName = 'TipoConnessione'
      Size = 10
    end
    object QSiti2FTP_host: TStringField
      FieldName = 'FTP_host'
      Size = 50
    end
    object QSiti2FTP_Username: TStringField
      FieldName = 'FTP_Username'
      Size = 30
    end
    object QSiti2FTP_password: TStringField
      FieldName = 'FTP_password'
      Size = 30
    end
    object QSiti2FTP_dir: TStringField
      FieldName = 'FTP_dir'
      Size = 100
    end
  end
  object QMPConfig: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select *'
      'from MP_Config')
    Left = 592
    Top = 496
    object QMPConfigWS_FTP_URL: TStringField
      FieldName = 'WS_FTP_URL'
      Size = 256
    end
    object QMPConfigDirLog: TStringField
      FieldName = 'DirLog'
      Size = 256
    end
  end
  object QQuestLK: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select Q.ID, Q.Codice, Q.TipoDomanda'
      'from QUEST_TipiDomande Q'
      'union select NULL ID, '#39#39' Codice, '#39'(nessuno)'#39' TipoDomanda')
    Left = 632
    Top = 400
    object QQuestLKID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QQuestLKCodice: TStringField
      FieldName = 'Codice'
      Size = 5
    end
    object QQuestLKTipoDomanda: TStringField
      FieldName = 'TipoDomanda'
      Size = 50
    end
  end
  object QAnnAnag: TADOQuery
    Connection = Data.DB
    BeforeOpen = QAnnAnagBeforeOpen
    DataSource = DsAnnunci
    Parameters = <
      item
        Name = 'xidutente'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'ID'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'select distinct Ann_AnagAnnEdizData.ID,IDAnnEdizData,DataPervenu' +
        'to,'
      
        '       CVNumero,Cognome,Nome,Ann_AnagAnnEdizData.IDAnagrafica, I' +
        'DAnnuncio, PesoRaggiunto, TotPunteggio, AQ.ID IDAnagQuest'
      'from Ann_AnagAnnEdizData'
      
        'join Ann_AnnEdizData on Ann_AnagAnnEdizData.IDAnnEdizData = Ann_' +
        'AnnEdizData.ID'
      'join Ann_Annunci on Ann_AnnEdizData.IDAnnuncio = Ann_Annunci.ID'
      
        'join Anagrafica on Ann_AnagAnnEdizData.IDAnagrafica = Anagrafica' +
        '.ID'
      
        'left outer join ann_anagannedizdataquery on Ann_AnagAnnEdizData.' +
        'id = ann_anagannedizdataquery.idanagannedizdata   AND ann_anagan' +
        'nedizdataquery.idutente = :xidutente'
      
        'left outer join Quest_AnagQuest AQ on (Ann_Annunci.IDQuestionari' +
        'o = AQ.IDQuest and Ann_AnagAnnEdizData.IDAnagrafica = AQ.IDAnagr' +
        'afica)'
      'where IDAnnuncio=:ID'
      ''
      ' ')
    Left = 216
    Top = 120
    object QAnnAnagID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QAnnAnagIDAnnEdizData: TIntegerField
      FieldName = 'IDAnnEdizData'
    end
    object QAnnAnagDataPervenuto: TDateTimeField
      FieldName = 'DataPervenuto'
    end
    object QAnnAnagCVNumero: TIntegerField
      FieldName = 'CVNumero'
    end
    object QAnnAnagCognome: TStringField
      FieldName = 'Cognome'
      Size = 30
    end
    object QAnnAnagNome: TStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QAnnAnagIDAnagrafica: TIntegerField
      FieldName = 'IDAnagrafica'
    end
    object QAnnAnagIDAnnuncio: TIntegerField
      FieldName = 'IDAnnuncio'
    end
    object QAnnAnagPesoRaggiunto: TFloatField
      FieldName = 'PesoRaggiunto'
    end
    object QAnnAnagTotPunteggio: TFloatField
      FieldName = 'TotPunteggio'
    end
    object QAnnAnagIDAnagQuest: TAutoIncField
      FieldName = 'IDAnagQuest'
      ReadOnly = True
    end
  end
  object QTemplateHTML: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select *'
      'from Ann_TemplateHTML')
    Left = 504
    Top = 344
    object QTemplateHTMLID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QTemplateHTMLDescrizione: TStringField
      FieldName = 'Descrizione'
      Size = 100
    end
    object QTemplateHTMLTemplateHTML: TMemoField
      FieldName = 'TemplateHTML'
      BlobType = ftMemo
    end
    object QTemplateHTMLURLIndicizzazione: TStringField
      FieldName = 'URLIndicizzazione'
      Size = 2048
    end
    object QTemplateHTMLURLPrelievoFeed: TStringField
      FieldName = 'URLPrelievoFeed'
      Size = 2048
    end
    object QTemplateHTMLURLFeed: TStringField
      FieldName = 'URLFeed'
      Size = 1024
    end
    object QTemplateHTMLFeed_Facebook: TMemoField
      FieldName = 'Feed_Facebook'
      BlobType = ftMemo
    end
    object QTemplateHTMLFeed_Linkedin: TMemoField
      FieldName = 'Feed_Linkedin'
      BlobType = ftMemo
    end
    object QTemplateHTMLFeed_Generico: TMemoField
      FieldName = 'Feed_Generico'
      BlobType = ftMemo
    end
    object QTemplateHTMLFeed_SimplyHired: TMemoField
      FieldName = 'Feed_SimplyHired'
      BlobType = ftMemo
    end
    object QTemplateHTMLFeed_Indeed: TMemoField
      FieldName = 'Feed_Indeed'
      BlobType = ftMemo
    end
    object QTemplateHTMLFeed_Trovit: TMemoField
      FieldName = 'Feed_Trovit'
      BlobType = ftMemo
    end
    object QTemplateHTMLFeed_Cambiolavoro: TMemoField
      FieldName = 'Feed_Cambiolavoro'
      BlobType = ftMemo
    end
    object QTemplateHTMLFeed_RaccoltaEBC: TMemoField
      FieldName = 'Feed_RaccoltaEBC'
      BlobType = ftMemo
    end
  end
  object QApriAnnunciHtml: TADOQuery
    Connection = Data.DB
    DataSource = DsAnnunci
    Parameters = <
      item
        Name = 'ID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select fileannunciohtml from ann_annunci'
      'where id= :ID')
    Left = 120
    Top = 8
    object QApriAnnunciHtmlfileannunciohtml: TMemoField
      FieldName = 'fileannunciohtml'
      BlobType = ftMemo
    end
  end
  object QAnnSettori: TADOQuery
    Connection = Data.DB
    BeforeOpen = QAnnSettoriBeforeOpen
    DataSource = DsAnnunci
    Parameters = <
      item
        Name = 'ID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select *'
      'from Ann_AnnunciSettori'
      'where IDAnnuncio=:ID')
    Left = 598
    Top = 72
    object QAnnSettoriID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QAnnSettoriIDAnnuncio: TIntegerField
      FieldName = 'IDAnnuncio'
    end
    object QAnnSettoriIDSettore: TIntegerField
      FieldName = 'IDSettore'
    end
    object QAnnSettoriNote: TMemoField
      FieldName = 'Note'
      BlobType = ftMemo
    end
    object QAnnSettoriSettore: TStringField
      FieldKind = fkLookup
      FieldName = 'Settore'
      LookupDataSet = QSettoriLK
      LookupKeyFields = 'ID'
      LookupResultField = 'Attivita'
      KeyFields = 'IDSettore'
      Size = 100
      Lookup = True
    end
    object QAnnSettoriFlag_feed: TBooleanField
      FieldName = 'Flag_feed'
    end
  end
  object DsQAnnSettori: TDataSource
    DataSet = QAnnSettori
    Left = 598
    Top = 120
  end
  object QSettoriLK: TADOQuery
    Connection = Data.DB
    Parameters = <>
    SQL.Strings = (
      'select ID, Attivita'
      'from EBC_Attivita'
      'order by Attivita')
    Left = 600
    Top = 168
    object QSettoriLKID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QSettoriLKAttivita: TStringField
      FieldName = 'Attivita'
      Size = 100
    end
  end
  object QAnnProv: TADOQuery
    Connection = Data.DB
    DataSource = DsAnnunci
    Parameters = <
      item
        Name = 'ID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select *'
      'from Ann_AnnunciProvince'
      'where IDAnnuncio=:ID')
    Left = 672
    Top = 72
    object QAnnProvID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QAnnProvIDAnnuncio: TIntegerField
      FieldName = 'IDAnnuncio'
    end
    object QAnnProvRegione: TStringField
      FieldName = 'Regione'
      Size = 100
    end
    object QAnnProvProvincia: TStringField
      FieldName = 'Provincia'
      Size = 2
    end
    object QAnnProvFlag_feed: TBooleanField
      FieldName = 'Flag_feed'
    end
    object QAnnProvNote: TMemoField
      FieldName = 'Note'
      BlobType = ftMemo
    end
  end
  object DsQAnnProv: TDataSource
    DataSet = QAnnProv
    Left = 672
    Top = 120
  end
  object Q2: TADOQuery
    Connection = Data.DB
    Parameters = <>
    Left = 200
    Top = 504
  end
  object QAnnSediEstere: TADOQuery
    Connection = Data.DB
    DataSource = DsAnnunci
    Parameters = <
      item
        Name = 'ID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select *'
      'from Ann_AnnunciSediEstere'
      'where IDAnnuncio=:ID')
    Left = 752
    Top = 73
    object QAnnSediEstereID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QAnnSediEstereIDAnnuncio: TIntegerField
      FieldName = 'IDAnnuncio'
    end
    object QAnnSediEstereNazione: TStringField
      FieldName = 'Nazione'
      Size = 100
    end
    object QAnnSediEstereCitta: TStringField
      FieldName = 'Citta'
      Size = 100
    end
    object QAnnSediEstereFlag_feed: TBooleanField
      FieldName = 'Flag_feed'
    end
    object QAnnSediEstereNote: TMemoField
      FieldName = 'Note'
      BlobType = ftMemo
    end
  end
  object DsQAnnSediEstere: TDataSource
    DataSet = QAnnSediEstere
    Left = 753
    Top = 120
  end
  object QEdizioniTest: TADOQuery
    Connection = Data.DB
    Parameters = <
      item
        Name = 'xID'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select Ann_Edizioni.*, EBC_Clienti.descrizione Concessionario '
      'from Ann_Edizioni left join EBC_Clienti '
      'on Ann_Edizioni.IDCliente = EBC_Clienti.ID'
      'where IDTestata = :xID'
      '')
    Left = 336
    Top = 128
    object QEdizioniTestID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QEdizioniTestIDTestata: TIntegerField
      FieldName = 'IDTestata'
    end
    object QEdizioniTestNomeEdizione: TStringField
      FieldName = 'NomeEdizione'
      Size = 50
    end
    object QEdizioniTestContatto: TStringField
      FieldName = 'Contatto'
      Size = 30
    end
    object QEdizioniTestTelefoni: TStringField
      FieldName = 'Telefoni'
      Size = 40
    end
    object QEdizioniTestFax: TStringField
      FieldName = 'Fax'
    end
    object QEdizioniTestCostoParolaFerLire: TFloatField
      FieldName = 'CostoParolaFerLire'
    end
    object QEdizioniTestCostoParolaFerEuro: TFloatField
      FieldName = 'CostoParolaFerEuro'
    end
    object QEdizioniTestCostoParolaFestLire: TFloatField
      FieldName = 'CostoParolaFestLire'
    end
    object QEdizioniTestCostoParolaFestEuro: TFloatField
      FieldName = 'CostoParolaFestEuro'
    end
    object QEdizioniTestCostoParolaRPQLire: TFloatField
      FieldName = 'CostoParolaRPQLire'
    end
    object QEdizioniTestCostoParolaRPQEuro: TFloatField
      FieldName = 'CostoParolaRPQEuro'
    end
    object QEdizioniTestCostoModuloFerLire: TFloatField
      FieldName = 'CostoModuloFerLire'
    end
    object QEdizioniTestCostoModuloFerEuro: TFloatField
      FieldName = 'CostoModuloFerEuro'
    end
    object QEdizioniTestCostoModuloFestLire: TFloatField
      FieldName = 'CostoModuloFestLire'
    end
    object QEdizioniTestCostoModuloFestEuro: TFloatField
      FieldName = 'CostoModuloFestEuro'
    end
    object QEdizioniTestCostoModuloRPQLire: TFloatField
      FieldName = 'CostoModuloRPQLire'
    end
    object QEdizioniTestCostoModuloRPQEuro: TFloatField
      FieldName = 'CostoModuloRPQEuro'
    end
    object QEdizioniTestPenetrazioneProvincia: TStringField
      FieldName = 'PenetrazioneProvincia'
      Size = 2
    end
    object QEdizioniTestPenetrazioneComune: TStringField
      FieldName = 'PenetrazioneComune'
      Size = 30
    end
    object QEdizioniTestPenetrazioneZona: TStringField
      FieldName = 'PenetrazioneZona'
    end
    object QEdizioniTestGiornoRPQ: TSmallintField
      FieldName = 'GiornoRPQ'
    end
    object QEdizioniTestNote: TStringField
      FieldName = 'Note'
      Size = 100
    end
    object QEdizioniTestIDCliente: TIntegerField
      FieldName = 'IDCliente'
    end
    object QEdizioniTestVisibileWeb: TBooleanField
      FieldName = 'VisibileWeb'
    end
    object QEdizioniTestConcessionario: TStringField
      FieldName = 'Concessionario'
      Size = 500
    end
  end
end
