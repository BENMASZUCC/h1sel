unit ModFatt;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Spin, Mask, ToolEdit, CurrEdit, StdCtrls, Buttons, TB97, ExtCtrls;

type
     TModFattForm = class(TForm)
          EDescrizione: TEdit;
          Label1: TLabel;
          RxImponibile: TRxCalcEdit;
          Imponibile: TLabel;
          Label2: TLabel;
          SEPercIVA_old: TSpinEdit;
          SEPercIVA: TEdit;
          Panel1: TPanel;
          BOK: TToolbarButton97;
          BAnnulla: TToolbarButton97;
          procedure FormShow(Sender: TObject);
          procedure SEPercIVAExit(Sender: TObject);
          procedure BOKClick(Sender: TObject);
          procedure BAnnullaClick(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     ModFattForm: TModFattForm;

implementation

uses ModuloDati, Main;

{$R *.DFM}

procedure TModFattForm.FormShow(Sender: TObject);
begin
     if pos('BFK', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) > 0 then begin
          rxImponibile.DisplayFormat := 'chf''.'' ,0.00';
     end;
     Caption := '[S/28] - ' + Caption;

     //Grafici
     ModFattForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

end;

procedure TModFattForm.SEPercIVAExit(Sender: TObject);
begin
     if (pos('BFK', UpperCase(Data.Global.FieldByName('NomeAzienda').AsString)) > 0) or (pos('Lederm', UpperCase(Data.Global.FieldByName('NomeAzienda').AsString)) > 0) then
          SEPercIVA.Text := StringReplace(SEPercIVA.Text, ',', '.', [rfReplaceAll])
     else
          SEPercIVA.Text := StringReplace(SEPercIVA.Text, '.', ',', [rfReplaceAll]);
end;

procedure TModFattForm.BOKClick(Sender: TObject);
begin
     ModalResult := mrOk;
end;

procedure TModFattForm.BAnnullaClick(Sender: TObject);
begin
     ModalResult := mrCancel;
end;

end.

