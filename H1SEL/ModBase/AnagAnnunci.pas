unit AnagAnnunci;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     dxDBGrid, dxDBTLCl, dxGrClms, dxTL, dxDBCtrl, dxCntner, Db, DBTables,
     StdCtrls, Buttons, ExtCtrls, TB97, ADODB, U_ADOLinkCl;

type
     TAnagAnnunciForm = class(TForm)
          Panel1: TPanel;
          DsQAnagAnnunci: TDataSource;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1ID: TdxDBGridMaskColumn;
          dxDBGrid1IDAnagrafica: TdxDBGridMaskColumn;
          dxDBGrid1IDAnnEdizData: TdxDBGridMaskColumn;
          dxDBGrid1DataPervenuto: TdxDBGridDateColumn;
          dxDBGrid1IDMansione: TdxDBGridMaskColumn;
          dxDBGrid1IDRicerca: TdxDBGridMaskColumn;
          dxDBGrid1IDEmailAnnunciRic: TdxDBGridMaskColumn;
          dxDBGrid1Progressivo: TdxDBGridMaskColumn;
          dxDBGrid1Ruolo: TdxDBGridMaskColumn;
          dxDBGrid1Data: TdxDBGridDateColumn;
          dxDBGrid1Rif: TdxDBGridMaskColumn;
          dxDBGrid1NomeEdizione: TdxDBGridMaskColumn;
          dxDBGrid1Testata: TdxDBGridMaskColumn;
          dxDBGrid1CodPubb: TdxDBGridColumn;
          ToolbarButton971: TToolbarButton97;
          ToolbarButton972: TToolbarButton97;
          QAnagAnnunci: TADOLinkedQuery;
          BitBtn1: TToolbarButton97;
          procedure ToolbarButton971Click(Sender: TObject);
          procedure ToolbarButton972Click(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure QAnagAnnunciBeforeOpen(DataSet: TDataSet);
          procedure BitBtn1Click(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     AnagAnnunciForm: TAnagAnnunciForm;

implementation

uses ModuloDati, CercaAnnuncio, SelFromQuery, Main;

{$R *.DFM}

procedure TAnagAnnunciForm.ToolbarButton971Click(Sender: TObject);
var xIDRicerca, xIDMansione: integer;
begin
     CercaAnnuncioForm := TCercaAnnuncioForm.create(self);
     CercaAnnuncioForm.QAnnAttivi.Close;
     CercaAnnuncioForm.QAnnAttivi.Open;
     QAnagAnnunci.Filtered := False;
     CercaAnnuncioForm.showModal;
     if CercaAnnuncioForm.ModalResult = mrOK then begin
          // rif. ricerca
          xIDRicerca := 0;
          xIDMansione := 0;
          Data.QTemp.Close;
          Data.QTemp.SQL.text := 'select Ann_AnnunciRicerche.ID,Ann_AnnunciRicerche.IDRicerca,' +
               ' EBC_Ricerche.IDMansione,Ann_AnnunciRicerche.Codice,EBC_Ricerche.Progressivo Rif, ' +
               ' EBC_Ricerche.NumRicercati Num,Mansioni.Descrizione Ruolo,EBC_Clienti.Descrizione Cliente, ' +
               ' Ann_AnnEdizData.codice' +
               ' from Ann_AnnunciRicerche ' +
               ' join EBC_Ricerche on EBC_Ricerche.ID = Ann_AnnunciRicerche.IDRicerca ' +
               ' join Mansioni on EBC_Ricerche.IDMansione=Mansioni.ID ' +
               ' join EBC_Clienti on EBC_Ricerche.IDCliente=EBC_Clienti.ID ' +
               ' join Ann_AnnEdizData on Ann_AnnEdizData.IDAnnuncio=Ann_AnnunciRicerche.IDAnnuncio ' +
               ' where Ann_AnnunciRicerche.IDAnnuncio=' + CercaAnnuncioForm.QAnnAttivi.FieldByName('ID').asString;
          Data.QTemp.Open;
          if Data.QTemp.RecordCount > 1 then begin
               SelFromQueryForm := TSelFromQueryForm.create(self);
               SelFromQueryForm.QGen.SQL.text := Data.QTemp.SQL.text;
               SelFromQueryForm.QGen.Open;
               SelFromQueryForm.xNumInvisibleCols := 3;
               SelFromQueryForm.Caption := 'seleziona ricerca/commessa associata all''annuncio';
               SelFromQueryForm.ShowModal;
               if SelFromQueryForm.ModalResult = mrOK then begin
                    xIDRicerca := SelFromQueryForm.QGen.FieldByName('IDRicerca').asInteger;
                    xIDMansione := SelFromQueryForm.QGen.FieldByName('IDMansione').asInteger;
               end;
               SelFromQueryForm.Free;
          end else begin
               xIDRicerca := Data.QTemp.FieldByName('IDRicerca').asInteger;
               xIDMansione := Data.QTemp.FieldByName('IDMansione').asInteger;
          end;
          Data.QTemp.Close;
          // rif ruolo
          Data.QTemp.SQL.text := 'select Ann_AnnunciRuoli.ID,Ann_AnnunciRuoli.Codice,IDMansione, Mansioni.Descrizione Ruolo ' +
               'from Ann_AnnunciRuoli ' +
               'join Mansioni on Ann_AnnunciRuoli.IDMansione=Mansioni.ID ' +
               'where Ann_AnnunciRuoli.IDAnnuncio=' + CercaAnnuncioForm.QAnnAttivi.FieldBYName('ID').asString;
          Data.QTemp.Open;
          if Data.QTemp.RecordCount > 1 then begin
               SelFromQueryForm := TSelFromQueryForm.create(self);
               SelFromQueryForm.QGen.SQL.text := Data.QTemp.SQL.text;
               SelFromQueryForm.QGen.Open;
               SelFromQueryForm.xNumInvisibleCols := 1;
               SelFromQueryForm.Caption := 'seleziona ruolo associata all''annuncio';
               SelFromQueryForm.ShowModal;
               if SelFromQueryForm.ModalResult = mrOK then begin
                    xIDMansione := SelFromQueryForm.QGen.FieldByName('IDMansione').asInteger;
               end;
               SelFromQueryForm.Free;
          end else begin
               if Data.QTemp.RecordCount = 1 then
                    xIDMansione := Data.QTemp.FieldByName('IDMansione').asInteger;
          end;

          with Data do begin
               DB.BeginTrans;
               try
                    Q1.SetSQLText(['insert into Ann_AnagAnnEdizData (IDAnagrafica,IDAnnEdizData,IDMansione,IDRicerca,DataPervenuto) ' +
                         'values (:xIDAnagrafica:,:xIDAnnEdizData:,:xIDMansione:,:xIDRicerca:,:xDataPervenuto:)',
                              Data.TAnagrafica.FieldByName('ID').AsInteger,
                              CercaAnnuncioForm.QAnnAttivi.FieldByName('IDAnnEdizData').AsInteger,
                              xIDMansione,
                              xIDRicerca,
                              Date()]);
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    QAnagAnnunci.Close;
                    QAnagAnnunci.Open;
                    QAnagAnnunci.Filter := 'IDAnagrafica=' + Data.TAnagraficaID.AsString;
                    QAnagAnnunci.Filtered := True;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: operazione non effettuata', mtError, [mbOK], 0);
               end;
          end;
     end;
     CercaAnnuncioForm.Free;
end;

procedure TAnagAnnunciForm.ToolbarButton972Click(Sender: TObject);
begin
     if QAnagAnnunci.IsEmpty then exit;
     if MessageDlg('Sei sicuro di voler cancellare l''associazione ?', mtWarning, [mbNo, mbYes], 0) <> mrYes then exit;
     Data.DB.BeginTrans;
     try
          Data.Q1.SQL.text := 'delete from Ann_AnagAnnEdizData where ID=' + QAnagAnnunci.fieldbyname('id').asString;
          Data.Q1.ExecSQL;
          Data.DB.CommitTrans;
          QAnagAnnunci.Close;
          QAnagAnnunci.Open;
     except
          Data.DB.RollBackTrans;
          MessageDlg('ERRORE SUL DATABASE:  operazione non completata', mtError, [mbOK], 0);
     end;
end;

procedure TAnagAnnunciForm.FormShow(Sender: TObject);
begin
     //Grafica
     AnagAnnunciForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     QAnagAnnunci.Open;
end;

procedure TAnagAnnunciForm.QAnagAnnunciBeforeOpen(DataSet: TDataSet);
begin
     // Giulio 07/07/2012
     {
     QAnagAnnunci.SQL.Text := 'select distinct Ann_AnagAnnEdizData.*, EBC_Ricerche.Progressivo, ' +
          'Mansioni.Descrizione Ruolo,Ann_AnnEdizData.Data, ' +
          'Rif,Ann_AnnEdizData.Codice CodPubb, ' +
          'NomeEdizione,Denominazione Testata ' +
          'from Ann_AnagAnnEdizData,EBC_Ricerche,Mansioni, ' +
          'Ann_AnnEdizData,Ann_Annunci,Ann_Edizioni,Ann_Testate ' +
          'where Ann_AnagAnnEdizData.IDAnnEdizData = Ann_AnnEdizData.ID ' +
          '  and Ann_AnnEdizData.IDAnnuncio = Ann_Annunci.ID ' +
          '  and Ann_AnnEdizData.IDEdizione = Ann_Edizioni.ID ' +
          '  and Ann_Edizioni.IDTestata = Ann_Testate.ID ' +
          '  and Ann_AnagAnnEdizData.IDRicerca *= EBC_Ricerche.ID ' +
          '  and Ann_AnagAnnEdizData.IDMansione *= Mansioni.ID ' +
          ' and Ann_AnagAnnEdizData.IDAnagrafica = ' + Data.TAnagraficaID.AsString;
     }

     QAnagAnnunci.SQL.Text := 'select distinct Ann_AnagAnnEdizData.*, EBC_Ricerche.Progressivo, ' +
          '       Mansioni.Descrizione Ruolo,Ann_AnnEdizData.Data, ' +
          '       Rif,Ann_AnnEdizData.Codice CodPubb, ' +
          '       NomeEdizione,Denominazione Testata ' +
          'from Ann_AnagAnnEdizData ' +
          'left outer join EBC_Ricerche on Ann_AnagAnnEdizData.IDRicerca = EBC_Ricerche.ID ' +
          'left outer join Mansioni on Ann_AnagAnnEdizData.IDMansione = Mansioni.ID ' +
          'join Ann_AnnEdizData on Ann_AnagAnnEdizData.IDAnnEdizData = Ann_AnnEdizData.ID ' +
          'join Ann_Annunci on Ann_AnnEdizData.IDAnnuncio = Ann_Annunci.ID ' +
          'join Ann_Edizioni on Ann_AnnEdizData.IDEdizione = Ann_Edizioni.ID ' +
          'join Ann_Testate on Ann_Edizioni.IDTestata = Ann_Testate.ID ' +
          'where Ann_AnagAnnEdizData.IDAnagrafica = ' + Data.TAnagraficaID.AsString;

end;

procedure TAnagAnnunciForm.BitBtn1Click(Sender: TObject);
begin
     AnagAnnunciForm.ModalResult := mrOk;
end;

end.

