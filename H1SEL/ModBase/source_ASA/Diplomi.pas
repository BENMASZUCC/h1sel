unit Diplomi;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables, ExtCtrls, Wall, StdCtrls, Grids, DBGrids, TB97;

type
  TDiplomiForm = class(TForm)
    Wallpaper1: TWallpaper;
    Diplomi: TTable;
    DsDiplomi: TDataSource;
    DiplomiID: TAutoIncField;
    DiplomiDescrizione: TStringField;
    DiplomiAmbito: TStringField;
    DBGrid1: TDBGrid;
    Edit1: TEdit;
    ToolbarButton971: TToolbarButton97;
    ToolbarButton972: TToolbarButton97;
    procedure Edit1Change(Sender: TObject);
    procedure ToolbarButton972Click(Sender: TObject);
    procedure ToolbarButton971Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    xChiama:integer;
  end;

var
  DiplomiForm: TDiplomiForm;

implementation

uses ModuloDati, ModuloDati3;

{$R *.DFM}

procedure TDiplomiForm.Edit1Change(Sender: TObject);
begin
     Diplomi.FindNearest([edit1.text]);
end;

procedure TDiplomiForm.ToolbarButton972Click(Sender: TObject);
begin
     if xChiama=2 then DataSel_EBC.TTitoliStudiioRic.Cancel;
     close;
end;

procedure TDiplomiForm.ToolbarButton971Click(Sender: TObject);
begin
     case xChiama of
     1:Data.TTitoliStudioDescrizione.Value:=DiplomiDescrizione.Value+' '+DiplomiAmbito.value;
     2:DataSel_EBC.TTitoliStudiioRicDescrizione.Value:=DiplomiDescrizione.Value+' '+DiplomiAmbito.value;
     end;
     close;
end;

end.
