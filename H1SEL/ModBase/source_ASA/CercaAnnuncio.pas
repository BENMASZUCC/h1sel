unit CercaAnnuncio;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     StdCtrls,Buttons,Db,Grids,DBGrids,DBTables,ExtCtrls,dxDBTLCl,
     dxGrClms,dxTL,dxDBCtrl,dxDBGrid,dxCntner;

type
     TCercaAnnuncioForm=class(TForm)
          QAnnAttivi: TQuery;
          DsQAnnAttivi: TDataSource;
          Panel1: TPanel;
          BitBtn1: TBitBtn;
          BitBtn2: TBitBtn;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1Rif: TdxDBGridMaskColumn;
          dxDBGrid1Data: TdxDBGridDateColumn;
          dxDBGrid1NomeEdizione: TdxDBGridMaskColumn;
          dxDBGrid1Denominazione: TdxDBGridMaskColumn;
          dxDBGrid1Civetta: TdxDBGridCheckColumn;
          dxDBGrid1ID: TdxDBGridMaskColumn;
          dxDBGrid1Archiviato: TdxDBGridCheckColumn;
          dxDBGrid1IDAnnEdizData: TdxDBGridMaskColumn;
          dxDBGrid1Codice: TdxDBGridColumn;
          QAnnAttiviRif: TStringField;
          QAnnAttiviData: TDateTimeField;
          QAnnAttiviIDAnnEdizData: TAutoIncField;
          QAnnAttiviNomeEdizione: TStringField;
          QAnnAttiviDenominazione: TStringField;
          QAnnAttiviCivetta: TBooleanField;
          QAnnAttiviID: TAutoIncField;
          QAnnAttiviArchiviato: TBooleanField;
          QAnnAttivicodice: TStringField;
          procedure FormShow(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     CercaAnnuncioForm: TCercaAnnuncioForm;

implementation

uses SceltaStato;

{$R *.DFM}

procedure TCercaAnnuncioForm.FormShow(Sender: TObject);
begin
     Caption:='[S/16] - '+Caption;
     //dxDBGrid1.Filter.Add(dxDBGrid1Archiviato,'False','No');
end;

end.

