unit RuoloRif;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Grids, DBGrids, Db, DBTables, ExtCtrls;

type
  TRuoloRifForm = class(TForm)
    DBGrid1: TDBGrid;
    TRuoli: TTable;
    TRuoliID: TAutoIncField;
    TRuoliDescrizione: TStringField;
    TRuoliIDArea: TIntegerField;
    DsRuoli: TDataSource;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    procedure BitBtn1Click(Sender: TObject);
    procedure DBGrid1KeyPress(Sender: TObject; var Key: Char);
  private
    xStringa:string;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  RuoloRifForm: TRuoloRifForm;

implementation

uses ValutazDip;

{$R *.DFM}

procedure TRuoloRifForm.BitBtn1Click(Sender: TObject);
begin
     ValutazDipForm.TRuoli.Locate('ID',TRuoliID.Value,[]);
end;

procedure TRuoloRifForm.DBGrid1KeyPress(Sender: TObject; var Key: Char);
begin
     if Key=chr(13) then begin
        BitBtn1.Click;
     end else begin
        xStringa:=xStringa+key;
        TRuoli.FindNearest([xStringa]);
     end;
end;

end.
