unit FaxPresentaz;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,ComObj,
     StdCtrls,Buttons,Grids,AdvGrid,DBGrids,Db,DBTables,ExtCtrls,
     SHFileOp,ComCtrls,ImgList,LookOut,Psock,NMsmtp;

type
     TFaxPresentazForm=class(TForm)
          DsClienti: TDataSource;
          TContattiCli: TTable;
          DsContattiCli: TDataSource;
          TContattiCliID: TAutoIncField;
          TContattiCliIDCliente: TIntegerField;
          TContattiCliContatto: TStringField;
          TContattiCliTelefono: TStringField;
          TContattiCliFax: TStringField;
          TContattiCliNote: TMemoField;
          QTitoliStudio: TQuery;
          QTitoliStudioTitoloStudio: TStringField;
          TAnag: TTable;
          TAnagTitolo: TStringField;
          TAnagDataNascita: TDateTimeField;
          TAnagSesso: TStringField;
          TAnagDomicilioCap: TStringField;
          TAnagDomicilioComune: TStringField;
          TAnagIDComuneDom: TIntegerField;
          TAnagIDZonaDom: TIntegerField;
          TAnagCVNumero: TIntegerField;
          TAnagCVProvenienza: TStringField;
          TAnagCVinseritoInData: TDateTimeField;
          TAnagIDStato: TIntegerField;
          TAnagIDTipoStato: TIntegerField;
          TAnagMobilita: TBooleanField;
          TAnagCassaIntegrazione: TBooleanField;
          TAnagDispPartTime: TBooleanField;
          TAnagDispInterinale: TBooleanField;
          TAnagCateogorieProtette: TStringField;
          TAnagInvalidita: TStringField;
          TAnagPercentualeInvalidita: TIntegerField;
          TAnagID: TAutoIncField;
          TTitStudio: TTable;
          TTitStudioIDAnagrafica: TIntegerField;
          TTitStudioIDDiploma: TIntegerField;
          TTitStudioID: TAutoIncField;
          TAnagMansioni: TTable;
          TAnagMansioniIDAnagrafica: TIntegerField;
          TAnagMansioniIDMansione: TIntegerField;
          TAnagMansioniID: TAutoIncField;
          TEspLav: TTable;
          TEspLavIDAnagrafica: TIntegerField;
          TEspLavIDSettore: TIntegerField;
          TEspLavID: TAutoIncField;
          TLingue: TTable;
          TLingueIDAnagrafica: TIntegerField;
          TLingueLingua: TStringField;
          TLingueLivello: TSmallintField;
          TLingueID: TAutoIncField;
          TAnagComp: TTable;
          TAnagCompIDAnagrafica: TIntegerField;
          TAnagCompIDCompetenza: TIntegerField;
          TAnagCompValore: TIntegerField;
          TAnagCompID: TAutoIncField;
          Label4: TLabel;
          Label5: TLabel;
          Label6: TLabel;
          Label7: TLabel;
          Label8: TLabel;
          Label9: TLabel;
          QTitoliStudioIDDiploma: TIntegerField;
          Src: TTable;
          BatchMove1: TBatchMove;
          Dest: TTable;
          TStoricoIDX: TTable;
          TStoricoIDXDataEvento: TDateTimeField;
          TStoricoIDXID: TAutoIncField;
          TStoricoIDXIDAnagrafica: TIntegerField;
          TStoricoIDXIDEvento: TIntegerField;
          TStoricoIDXIDRicerca: TIntegerField;
          TStoricoIDXIDUtente: TIntegerField;
          TStoricoIDXIDCliente: TIntegerField;
          TStoricoIDXAnnotazioni: TStringField;
          TAnagCognome: TStringField;
          TAnagNome: TStringField;
          Q: TQuery;
          QAnagAltreInfo: TQuery;
          QAnagAltreInfoIDAnagrafica: TIntegerField;
          QAnagAltreInfoFiguraChiave: TBooleanField;
          QAnagAltreInfoAssunzioneObbligatoria: TBooleanField;
          QAnagAltreInfoIdoneoAffiancamento: TBooleanField;
          QAnagAltreInfoCateogorieProtette: TStringField;
          QAnagAltreInfoInvalidita: TStringField;
          QAnagAltreInfoPercentualeInvalidita: TIntegerField;
          QAnagAltreInfoPatente: TBooleanField;
          QAnagAltreInfoTipoPatente: TStringField;
          QAnagAltreInfoDispAuto: TBooleanField;
          QAnagAltreInfoServizioMilitareStato: TStringField;
          QAnagAltreInfoServizioMilitareDove: TStringField;
          QAnagAltreInfoServizioMilitareQuando: TStringField;
          QAnagAltreInfoServizioMilitareGrado: TStringField;
          QAnagAltreInfoTipoContratto: TStringField;
          QAnagAltreInfoQualificaContrattuale: TStringField;
          QAnagAltreInfoDataAssunzione: TDateTimeField;
          QAnagAltreInfoDataScadContratto: TDateTimeField;
          QAnagAltreInfoDataUscita: TDateTimeField;
          QAnagAltreInfoDataFineProva: TDateTimeField;
          QAnagAltreInfoDataProssScatto: TDateTimeField;
          QAnagAltreInfoDataPensione: TDateTimeField;
          QAnagAltreInfoIDStabilimento: TIntegerField;
          QAnagAltreInfoPosizioneIdeale: TStringField;
          QAnagAltreInfoOrarioIdeale: TStringField;
          QAnagAltreInfoIDDispMov: TIntegerField;
          QAnagAltreInfoDisponibContratti: TStringField;
          QAnagAltreInfoCVProvenienza: TStringField;
          QAnagAltreInfoCVFile: TStringField;
          QAnagAltreInfoAbilitazProfessioni: TStringField;
          QAnagAltreInfoNote: TMemoField;
          QAnagAltreInfoMobilita: TBooleanField;
          QAnagAltreInfoCassaIntegrazione: TBooleanField;
          QAnagAltreInfoDispPartTime: TBooleanField;
          QAnagAltreInfoDispInterinale: TBooleanField;
          QAnagAltreInfoProvInteresse: TStringField;
          QAnagAltreInfoInquadramento: TStringField;
          QAnagAltreInfoRetribuzione: TStringField;
          QClienti: TQuery;
          QClientiID: TAutoIncField;
          QClientiDescrizione: TStringField;
          QClientiStato: TStringField;
          QClientiIndirizzo: TStringField;
          QClientiCap: TStringField;
          QClientiComune: TStringField;
          QClientiProvincia: TStringField;
          QClientiIDAttivita: TIntegerField;
          QClientiTelefono: TStringField;
          QClientiFax: TStringField;
          QClientiPartitaIVA: TStringField;
          QClientiCodiceFiscale: TStringField;
          QClientiBancaAppoggio: TStringField;
          QClientiSistemaPagamento: TStringField;
          QClientiNoteContratto: TMemoField;
          QClientiResponsabile: TStringField;
          QClientiConosciutoInData: TDateTimeField;
          QClientiIndirizzoLegale: TStringField;
          QClientiCapLegale: TStringField;
          QClientiComuneLegale: TStringField;
          QClientiProvinciaLegale: TStringField;
          DsQAnag: TDataSource;
          SrcAnagMansioni: TQuery;
          SrcAnagEspLav: TQuery;
          SrcAnagMansioniIDAnagrafica: TIntegerField;
          SrcAnagMansioniIDMansione: TIntegerField;
          SrcAnagEspLavIDAnagrafica: TIntegerField;
          SrcAnagEspLavIDSettore: TIntegerField;
          TLingueConosc: TQuery;
          TLingueConoscID: TAutoIncField;
          TLingueConoscIDAnagrafica: TIntegerField;
          TLingueConoscLingua: TStringField;
          TLingueConoscLivello: TSmallintField;
          SrcAnagComp: TQuery;
          SrcAnagCompID: TAutoIncField;
          SrcAnagCompIDCompetenza: TIntegerField;
          SrcAnagCompIDAnagrafica: TIntegerField;
          SrcAnagCompValore: TIntegerField;
          QAnag: TQuery;
          QAnagID: TAutoIncField;
          QAnagMatricola: TIntegerField;
          QAnagCognome: TStringField;
          QAnagNome: TStringField;
          QAnagTitolo: TStringField;
          QAnagDataNascita: TDateTimeField;
          QAnagGiornoNascita: TSmallintField;
          QAnagMeseNascita: TSmallintField;
          QAnagLuogoNascita: TStringField;
          QAnagSesso: TStringField;
          QAnagIndirizzo: TStringField;
          QAnagCap: TStringField;
          QAnagComune: TStringField;
          QAnagIDComuneRes: TIntegerField;
          QAnagIDZonaRes: TIntegerField;
          QAnagProvincia: TStringField;
          QAnagStato: TStringField;
          QAnagDomicilioIndirizzo: TStringField;
          QAnagDomicilioCap: TStringField;
          QAnagDomicilioComune: TStringField;
          QAnagIDComuneDom: TIntegerField;
          QAnagIDZonaDom: TIntegerField;
          QAnagDomicilioProvincia: TStringField;
          QAnagDomicilioStato: TStringField;
          QAnagRecapitiTelefonici: TStringField;
          QAnagCellulare: TStringField;
          QAnagFax: TStringField;
          QAnagCodiceFiscale: TStringField;
          QAnagPartitaIVA: TStringField;
          QAnagEmail: TStringField;
          QAnagIDStatoCivile: TIntegerField;
          QAnagIDStato: TIntegerField;
          QAnagIDTipoStato: TIntegerField;
          QAnagCVNumero: TIntegerField;
          QAnagCVIDAnnData: TIntegerField;
          QAnagCVinseritoInData: TDateTimeField;
          QAnagFoto: TBlobField;
          QAnagIDUtenteMod: TIntegerField;
          QAnagDubbiIns: TStringField;
          QAnagIDProprietaCV: TIntegerField;
          QAnagEta: TStringField;
          Panel2: TPanel;
          ImageList1: TImageList;
          PCSel: TPageControl;
          TSMailCand: TTabSheet;
          Panel4: TPanel;
          BitBtn2: TBitBtn;
          TabSheet1: TTabSheet;
          Panel12: TPanel;
          CBNominativo: TCheckBox;
          RGOpzioneExp: TRadioGroup;
          RGOpzioneCV: TRadioGroup;
          Panel5: TPanel;
          TSFax: TTabSheet;
          Panel6: TPanel;
          CBFax: TCheckBox;
          PanContatto: TPanel;
          Label1: TLabel;
          DBGrid1: TDBGrid;
          BitBtn4: TBitBtn;
          Panel7: TPanel;
          BitBtn3: TBitBtn;
          Panel8: TPanel;
          BitBtn1: TBitBtn;
          Panel9: TPanel;
          ASG1: TAdvStringGrid;
          Panel1: TPanel;
          GroupBox1: TGroupBox;
          Label2: TLabel;
          Label3: TLabel;
          Label10: TLabel;
          EHost: TEdit;
          EPort: TEdit;
          EUserID: TEdit;
          Label11: TLabel;
          EName: TEdit;
          Bevel1: TBevel;
          Label12: TLabel;
          EAddress: TEdit;
          TSSms: TTabSheet;
          Panel10: TPanel;
          NMSMTP1: TNMSMTP;
          StatusBar1: TStatusBar;
          Panel11: TPanel;
          BitBtn5: TBitBtn;
          SpeedButton1: TSpeedButton;
          Panel13: TPanel;
          QTipiMailCand: TQuery;
          DsTipiMailCand: TDataSource;
          QTipiMailCandID: TAutoIncField;
          QTipiMailCandDescrizione: TStringField;
          QTipiMailCandIntestazione: TMemoField;
          QTipiMailCandCorpo: TMemoField;
          DBGrid2: TDBGrid;
          Panel14: TPanel;
          Panel3: TPanel;
          QTipiMailCandOggetto: TStringField;
          QTipiMailCandInfoRicerca: TBooleanField;
          SpeedButton2: TSpeedButton;
          TSClientiCVMail: TTabSheet;
          Panel15: TPanel;
          Panel16: TPanel;
          BitBtn6: TBitBtn;
          MMail: TMemo;
          Label15: TLabel;
          GroupBox2: TGroupBox;
          CBCliMailCognNome: TCheckBox;
          RGCliMailOpz: TRadioGroup;
          CBCliMailCand: TCheckBox;
          SpeedButton3: TSpeedButton;
          Label16: TLabel;
          EClimailOgg: TEdit;
          CBCliMailRifRic: TCheckBox;
          TContattiCliEmail: TStringField;
          Panel17: TPanel;
          Label17: TLabel;
          DBGrid3: TDBGrid;
          QAnag2: TQuery;
          Label18: TLabel;
          ETestoSMS: TEdit;
          Panel18: TPanel;
          CBcandMailSMS: TCheckBox;
          EavvisoSMS: TEdit;
          CBDispatcher1: TComboBox;
          CBDispatcher2: TComboBox;
          Label19: TLabel;
          Label20: TLabel;
          Label21: TLabel;
          PanCandidati: TPanel;
          ExpressButton1: TExpressButton;
          ExpressButton4: TExpressButton;
          Bevel2: TBevel;
          Label13: TLabel;
          PanClienti: TPanel;
          ExpressButton2: TExpressButton;
          ExpressButton3: TExpressButton;
          Bevel3: TBevel;
          Label14: TLabel;
          ExpressButton5: TExpressButton;
          QTipiMailCandAddSpecifiche: TBooleanField;
          ExpressButton6: TExpressButton;
          ExpressButton7: TExpressButton;
          QClientiTipoStrada: TStringField;
          QClientiTipoStradaLegale: TStringField;
          QClientiNumCivico: TStringField;
          QClientiNumCivicoLegale: TStringField;
          QClientiNazioneAbb: TStringField;
          QClientiNazioneAbbLegale: TStringField;
          procedure CBFaxClick(Sender: TObject);
          procedure BitBtn3Click(Sender: TObject);
          procedure BitBtn4Click(Sender: TObject);
          procedure QAnagCalcFields(DataSet: TDataSet);
          procedure FormShow(Sender: TObject);
          procedure ExpressButton1Click(Sender: TObject);
          procedure ExpressButton2Click(Sender: TObject);
          procedure ExpressButton3Click(Sender: TObject);
          procedure ExpressButton4Click(Sender: TObject);
          procedure BitBtn2Click(Sender: TObject);
          procedure NMSMTP1Connect(Sender: TObject);
          procedure NMSMTP1Disconnect(Sender: TObject);
          procedure NMSMTP1ConnectionFailed(Sender: TObject);
          procedure NMSMTP1Status(Sender: TComponent; Status: string);
          procedure NMSMTP1EncodeStart(Filename: string);
          procedure NMSMTP1EncodeEnd(Filename: string);
          procedure NMSMTP1Failure(Sender: TObject);
          procedure NMSMTP1HostResolved(Sender: TComponent);
          procedure NMSMTP1PacketSent(Sender: TObject);
          procedure NMSMTP1RecipientNotFound(Recipient: string);
          procedure NMSMTP1SendStart(Sender: TObject);
          procedure NMSMTP1Success(Sender: TObject);
          procedure NMSMTP1HeaderIncomplete(var handled: Boolean;
               hiType: Integer);
          procedure SpeedButton1Click(Sender: TObject);
          procedure SpeedButton2Click(Sender: TObject);
          procedure ExpressButton5Click(Sender: TObject);
          procedure BitBtn6Click(Sender: TObject);
          procedure CBCliMailCandClick(Sender: TObject);
          procedure CBcandMailSMSClick(Sender: TObject);
          procedure BitBtn5Click(Sender: TObject);
          procedure ASG1GetCellColor(Sender: TObject; ARow,ACol: Integer;
               AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
          procedure ASG1CanEditCell(Sender: TObject; Arow,Acol: Integer;
               var canedit: Boolean);
          procedure ExpressButton6Click(Sender: TObject);
          procedure ExpressButton7Click(Sender: TObject);
     private
          FileList: TStringList;
          BaseDir: string;
          procedure GetFileList;
          procedure DeleteAllFiles;
          function WinExecAndWait32(FileName: string; Visibility: integer=SW_SHOWNORMAL): cardinal;
          procedure CaricaInfoConnect;
     public
          xArrayIDAnag: array[1..1000] of integer
     end;

var
     FaxPresentazForm: TFaxPresentazForm;

implementation

uses ModuloDati,MDRicerche,Main,Splash3,EmailConnectInfo,TipiMailCand,
     uUtilsVarie,EmailMessage,SceltaModello,SceltaModelloAnag,SelContatti;

{$R *.DFM}

procedure TFaxPresentazForm.CBFaxClick(Sender: TObject);
begin
     PanContatto.Visible:=CBFax.Checked;
end;

procedure TFaxPresentazForm.BitBtn3Click(Sender: TObject);
var xFileWord,xGiaInviati: string;
     MSWord: Variant;
     i,xTot: integer;
     Vero,xProcedi,xRiempi: boolean;
begin
     // messaggio PRIVACY
     MessageDlg('ATTENZIONE!! Prima di procedere con la presentazione di personale '+
          'accertarsi che il/i candidato/i sia/siano consenziente/i. (legge sulla privaci 675)',mtInformation, [mbOK],0);
     xTot:=0;
     for i:=1 to ASG1.RowCount-1 do begin
          ASG1.GetCheckBoxState(0,i,Vero);
          if Vero then inc(xTot);
     end;
     if xtot=0 then begin
          MessageDlg('Nessun candidato selezionato !',mtError, [mbOK],0);
          Abort;
     end;

     xProcedi:=True;
     // controllo gi� inviati
     TStoricoIDX.Open;
     xGiaInviati:='';
     for i:=1 to ASG1.RowCount-1 do begin
          ASG1.GetCheckBoxState(0,i,Vero);
          if Vero then
               if TStoricoIDX.FindKey([xArrayIDAnag[i],20,DataRicerche.TRicerchePendIDCliente.Value]) then
                    xGiaInviati:=xGiaInviati+ASG1.Cells[0,i]+' il '+TStoricoIDXDataEvento.asString+chr(13);
     end;
     TStoricoIDX.Close;
     if xGiaInviati<>'' then
          if MessageDlg('ATTENZIONE - Gi� effettuati i seguenti invii: '+chr(13)+
               xGiaInviati+chr(13)+
               '-- VUOI CONTINUARE ? --',mtWarning, [mbYes,mbNo],0)=mrNo then xProcedi:=False;

     if xProcedi then begin
          try
               MsWord:=CreateOleObject('Word.Basic');
          except
               ShowMessage('Non riesco ad aprire Microsoft Word.');
               Exit;
          end;

          xFileWord:=GetDocPath+'\p'+DataRicerche.TRicerchePendID.asString+'.doc';
          xRiempi:=True;
          if FileExists(xFileWord) then begin
               if MessageDlg('Il file esiste gi� - Aprirlo (Yes) o crearne uno nuovo, riscrivendo il vecchio (No) ?',mtWarning, [mbYes,mbNo],0)=mrYes then begin
                    MsWord.AppShow;
                    MsWord.FileOpen(xFileWord); // file da aprire
                    xRiempi:=False; // non riempire i campi
               end else begin
                    MsWord.AppShow;
                    MsWord.FileOpen(GetDocPath+'\ModPresCand.doc');
               end;
          end else begin
               MsWord.AppShow;
               MsWord.FileOpen(GetDocPath+'\ModPresCand.doc');
          end;

          // riempimento campi
          if xRiempi then begin
               MsWord.EditFind('#data');
               MsWord.Insert(DateToStr(Date));
               MsWord.EditFind('#NomeAzienda');
               MsWord.Insert(QClientiDescrizione.Value);
               MsWord.EditFind('#IndirizzoAzienda');
               MsWord.Insert(QClientiTipoStrada.Value+' '+QClientiIndirizzo.Value+' '+QClientiNumCivico.Value);
               MsWord.EditFind('#CapLuogoProvAzienda');
               MsWord.Insert(QClientiCap.Value+' '+QClientiComune.Value+' ('+QClientiProvincia.Value+')');
               if (CBFax.Checked)and(TContattiCli.RecordCount>0) then begin
                    MsWord.EditFind('#AttenzioneDi');
                    MsWord.Insert(TContattiCliContatto.Value);
               end;
               MsWord.EditFind('#NumeroFax');
               if QClientiFax.Value<>'' then
                    MsWord.Insert(QClientiFax.Value)
               else MsWord.Insert('---');
               MsWord.EditFind('#Ruolo');
               MsWord.Insert(DataRicerche.TRicerchePendMansione.Value);
               MsWord.EditFind('#CV');
               for i:=1 to ASG1.RowCount-1 do begin
                    ASG1.GetCheckBoxState(0,i,Vero);
                    if Vero then begin
                         QAnag.Close;
                         QAnag.ParamByName('xID').asInteger:=xArrayIDAnag[i];
                         QAnag.Open;
                         // dati candidato
                         MsWord.Insert('C.V. n�'+QAnagCVNumero.asString+'  del '+QAnagCVinseritoInData.asString+chr(13));
                         MsWord.Insert('Et�: '+QAnagEta.value+chr(13));
                         MsWord.Insert('Comune di Residenza: '+QAnagComune.value+chr(13));
                         MsWord.Insert('Titolo di studio: ');
                         QTitoliStudio.Open;
                         while not QTitoliStudio.EOF do begin
                              MsWord.Insert(QTitoliStudioTitoloStudio.value);
                              QTitoliStudio.Next;
                              if not QTitoliStudio.EOF then MsWord.Insert(', ');
                         end;
                         QTitoliStudio.Close;
                         MsWord.Insert(chr(13));
                         MsWord.Insert('Lingue Conosciute: ');
                         TLingueConosc.Open;
                         while not TLingueConosc.EOF do begin
                              MsWord.Insert(TLingueConoscLingua.value);
                              TLingueConosc.Next;
                              if not TLingueConosc.EOF then MsWord.Insert(', ');
                         end;
                         TLingueConosc.Close;
                         MsWord.Insert(chr(13));
                         MsWord.Insert('----------------------------------------------------------------------------------------------');
                         MsWord.Insert(chr(13));

                         Data.DB.BeginTrans;
                         try
                              // aggiornamento dati candidato-ricerca
                              Data.Q1.Close;
                              Data.Q1.SQL.text:='update EBC_CandidatiRicerche set Escluso=:xEscluso,DataImpegno=null,Stato=:xStato '+
                                   'where IDRicerca='+DataRicerche.TRicerchePendID.asString+' and IDAnagrafica='+QAnagID.asString;
                              Data.Q1.ParambyName('xEscluso').asBoolean:=False;
                              Data.Q1.ParambyName('xStato').asString:='invio FAX di presentazione il '+DateToStr(Date);
                              Data.Q1.ExecSQL;

                              // registrazione nello storico
                              Data.Q1.SQL.text:='insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) '+
                                   'values (:xIDAnagrafica,:xIDEvento,:xDataEvento,:xAnnotazioni,:xIDRicerca,:xIDUtente,:xIDCliente)';
                              Data.Q1.ParambyName('xIDAnagrafica').asInteger:=QAnagID.Value;
                              Data.Q1.ParambyName('xIDEvento').asInteger:=20;
                              Data.Q1.ParambyName('xDataEvento').asDateTime:=Date;
                              Data.Q1.ParambyName('xAnnotazioni').asString:='';
                              Data.Q1.ParambyName('xIDRicerca').asInteger:=DataRicerche.TRicerchePendID.Value;
                              Data.Q1.ParambyName('xIDUtente').asInteger:=MainForm.xIDUtenteAttuale;
                              Data.Q1.ParambyName('xIDCliente').asInteger:=DataRicerche.TRicerchePendIDCliente.Value;
                              Data.Q1.ExecSQL;

                              Data.DB.CommitTrans;
                         except
                              Data.DB.RollbackTrans;
                              MessageDlg('ERRORE SUL DATABASE:  operazione non completata',mtError, [mbOK],0);
                         end;
                    end;
               end;
               MsWord.EditFind('#FirmaConsulente');
               MsWord.Insert(DataRicerche.TRicerchePendUtente.asString);
               MsWord.EditFind('#quanti');
               MsWord.Insert(IntToStr(xTot));
               MsWord.FileSaveAs(xFileWord);
          end;
     end;
end;

procedure TFaxPresentazForm.BitBtn4Click(Sender: TObject);
var MyStringList: TStringList;
     xPath,xGiaInviati,xCognome,xNome: string;
     Vero,xProcedi: boolean;
     xFileName: PChar;
     i: integer;
     xLettera,xCar: char;
     xstring: string;
begin
     // messaggio PRIVACY
     MessageDlg('ATTENZIONE!! Prima di procedere con la presentazione di personale '+
          'accertarsi che il/i candidato/i sia/siano consenziente/i. (legge sulla privaci 675)',mtInformation, [mbOK],0);
     // DISABILITAZIONE MISTRAL Milano -- ALIAS='MISTRAL'
     if Uppercase(copy(Data.GlobalNomeAzienda.Value,1,7))='MISTRAL' then begin
          ShowMessage('FIDELITY:  FUNZIONE NON ABILITATA');
          Exit;
     end;
     MyStringList:=TStringList.Create;
     Session.GetAliasParams('EBCtemp',MyStringList);
     xPath:=copy(MyStringList[0],6,Length(MyStringList[0]));
     if xPath[length(xPath)]<>'\' then xPath:=xPath+'\'; // mette la barra finale se non c'e'
     BaseDir:=xPath; // BaseDir corrisponde alla cartella dell'alias "EBCtemp"
     if not FileExists(BaseDir+'pkzip.exe') then begin
          MessageDlg('File PKZIP.EXE non trovato - impossibile proseguire',mtError, [mbOK],0);
          exit;
     end;

     if (not FileExists(xPath+'Anagrafica.db'))or
          (not FileExists(xPath+'TitoliStudio.db'))or
          (not FileExists(xPath+'AreeDiplomi.db'))or
          (not FileExists(xPath+'AnagMansioni.db')) then begin
          MessageDlg('Mancano file necessari delle tabelle in EBCtemp - impossibile proseguire',mtError, [mbOK],0);
          exit;
     end;
     xProcedi:=True;
     // controllo gi� inviati
     TStoricoIDX.Open;
     xGiaInviati:='';
     for i:=1 to ASG1.RowCount-1 do begin
          ASG1.GetCheckBoxState(0,i,Vero);
          if Vero then
               if TStoricoIDX.FindKey([xArrayIDAnag[i],21,DataRicerche.TRicerchePendIDCliente.Value]) then
                    xGiaInviati:=xGiaInviati+ASG1.Cells[0,i]+' il '+TStoricoIDXDataEvento.asString+chr(13);
     end;
     TStoricoIDX.Close;
     if xGiaInviati<>'' then
          if MessageDlg('ATTENZIONE - Gi� effettuati i seguenti invii: '+chr(13)+
               xGiaInviati+chr(13)+
               '-- VUOI CONTINUARE ? --',mtWarning, [mbYes,mbNo],0)=mrNo then xProcedi:=False;

     if xProcedi then begin
          Splash3Form:=TSplash3Form.Create(nil);
          Splash3Form.Show;
          Splash3Form.Update;

          // svuotamento tabelle
          TAnag.Close;
          TAnag.Exclusive:=True;
          TAnag.EmptyTable;
          TAnag.Exclusive:=False;
          TTitStudio.Close;
          TTitStudio.Exclusive:=True;
          TTitStudio.EmptyTable;
          TTitStudio.Exclusive:=False;
          TAnagMansioni.Close;
          TAnagMansioni.Exclusive:=True;
          TAnagMansioni.EmptyTable;
          TAnagMansioni.Exclusive:=False;
          TEspLav.Close;
          TEspLav.Exclusive:=True;
          TEspLav.EmptyTable;
          TEspLav.Exclusive:=False;
          TLingue.Close;
          TLingue.Exclusive:=True;
          TLingue.EmptyTable;
          TLingue.Exclusive:=False;
          //TAnagComp.Close;
          //TAnagComp.Exclusive:=True;
          //TAnagComp.EmptyTable;
          //TAnagComp.Exclusive:=False;

          TAnag.Open;
          TTitStudio.Open;
          TAnagMansioni.Open;
          TEspLav.Open;
          TLingue.Open;
          //TAnagComp.Open;

          // cancellazione vecchi CV --> tutti i file GIF
          FileList:=TStringList.Create;
          GetFileList;
          DeleteAllFiles;
          FileList.Free;
          MyStringList.Free;

          // Riempimento tabelle
          for i:=1 to ASG1.RowCount-1 do begin
               ASG1.GetCheckBoxState(0,i,Vero);
               if Vero then begin
                    QAnag.Close;
                    QAnag.ParamByName('xID').asInteger:=xArrayIDAnag[i];
                    QAnag.Open;
                    QAnagAltreInfo.Close;
                    QAnagAltreInfo.ParamByName('xID').asInteger:=xArrayIDAnag[i];
                    QAnagAltreInfo.Open;
                    if CBNominativo.Checked then begin
                         xCognome:=''; xNome:='';
                    end else begin
                         xCognome:=QAnagCognome.Value;
                         xNome:=QAnagNome.Value;
                    end;
                    TAnag.InsertRecord([xCognome,xNome,
                         QAnagTitolo.Value,
                              QAnagDataNascita.Value,
                              QAnagSesso.Value,
                              QAnagDomicilioCap.Value,
                              QAnagDomicilioComune.Value,
                              QAnagIDComuneDom.Value,
                              QAnagIDZonaDom.Value,
                              QAnagCVNumero.Value,
                              null,
                              QAnagCVinseritoInData.Value,
                              null,
                              null,
                              QAnagAltreInfoMobilita.Value,
                              QAnagAltreInfoCassaIntegrazione.Value,
                              QAnagAltreInfoDispPartTime.Value,
                              QAnagAltreInfoDispInterinale.Value,
                              QAnagAltreInfoCateogorieProtette.Value,
                              QAnagAltreInfoInvalidita.Value,
                              QAnagAltreInfoPercentualeInvalidita.Value]);
                    // titoli di studio
                    QTitoliStudio.Open;
                    while not QTitoliStudio.EOF do begin
                         TTitStudio.InsertRecord([TAnagID.value,QTitoliStudioIDDiploma.value]);
                         QTitoliStudio.Next;
                    end;
                    QTitoliStudio.Close;
                    // ruoli posseduti
                    SrcAnagMansioni.Open;
                    while not SrcAnagMansioni.EOF do begin
                         TAnagMansioni.InsertRecord([TAnagID.value,SrcAnagMansioniIDMansione.Value]);
                         SrcAnagMansioni.Next;
                    end;
                    SrcAnagMansioni.Close;
                    // esperienze lavorative
                    SrcAnagEspLav.Open;
                    while not SrcAnagEspLav.EOF do begin
                         TEspLav.InsertRecord([TAnagID.value,SrcAnagEspLavIDSettore.Value]);
                         SrcAnagEspLav.Next;
                    end;
                    SrcAnagEspLav.Close;
                    // lingue conosciute
                    TLingueConosc.Open;
                    while not TLingueConosc.EOF do begin
                         TLingue.InsertRecord([TAnagID.value,TLingueConoscLingua.value,TLingueConoscLivello.value]);
                         TLingueConosc.Next;
                    end;
                    TLingueConosc.close;
                    // competenze possedute --> PER ORA NO (TROPPO GRANDE LA TABELLA DI BASE)
                    {SrcAnagComp.Open;
                    while not SrcAnagComp.EOF do begin
                          TAnagComp.InsertRecord([TAnagID.value,SrcAnagCompIDCompetenza.value,SrcAnagCompValore.Value]);
                       SrcAnagComp.Next;
                    end;
                    SrcAnagComp.Close;}

                    // copia file GIF
                    if RGOpzioneCV.ItemIndex=0 then xCar:=char('e') else xCar:=char('i');
                    if FileExists(GetCVPath+'\'+xCar+QAnagCVNumero.AsString+'a.gif') then begin
                         CopyFile(Pointer(GetCVPath+'\'+xCar+QAnagCVNumero.AsString+'a.gif'),Pointer(xPath+'\'+xCar+QAnagCVNumero.AsString+'a.gif'),False);
                         xLettera:=char('b');
                         while FileExists(GetCVPath+'\'+xCar+QAnagCVNumero.AsString+xlettera+'.gif') do begin
                              CopyFile(Pointer(GetCVPath+'\'+xCar+QAnagCVNumero.AsString+'a.gif'),Pointer(xPath+'\'+xCar+QAnagCVNumero.AsString+xLettera+'.gif'),False);
                              xLettera:=char(Ord(xLettera)+1);
                         end;
                    end;

                    Data.DB.BeginTrans;
                    try
                         // aggiornamento storico
                         if Q.Active then Q.Close;
                         Q.SQL.text:='insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) '+
                              'values (:xIDAnagrafica,:xIDEvento,:xDataEvento,:xAnnotazioni,:xIDRicerca,:xIDUtente,:xIDCliente)';
                         Q.ParamByName('xIDAnagrafica').asInteger:=QAnagID.Value;
                         Q.ParamByName('xIDEvento').asInteger:=21;
                         Q.ParamByName('xDataEvento').asDateTime:=date;
                         Q.ParamByName('xAnnotazioni').asString:='';
                         Q.ParamByName('xIDRicerca').asInteger:=DataRicerche.TRicerchePendID.Value;
                         Q.ParamByName('xIDUtente').asInteger:=DataRicerche.TRicerchePendIDUtente.Value;
                         Q.ParamByName('xIDCliente').asInteger:=DataRicerche.TRicerchePendIDCliente.Value;
                         Q.ExecSQL;
                         // aggiornamento stato anagrafica
                         if Q.Active then Q.Close;
                         Q.SQL.text:='update EBC_CandidatiRicerche set Escluso=0,DataImpegno=null,Stato=''invio CV via e-mail il '+DateToStr(Date)+''''+
                              ' where IDRicerca='+DataRicerche.TRicerchePendID.asString+' and IDAnagrafica='+QAnagID.asString;
                         Q.ExecSQL;
                         Data.DB.CommitTrans;
                    except
                         Data.DB.RollbackTrans;
                         MessageDlg('ATTENZIONE: errore sul database'+chr(13)+
                              'aggiornamento scheda e storico non avvenuto per'+chr(13)+
                              xCognome+' '+xNome,mtError, [mbOK],0);
                    end;
               end;
          end;

          TAnag.Close;
          TTitStudio.Close;
          TAnagMansioni.Close;
          TEspLav.Close;
          TLingue.Close;
          //TAnagComp.Close;

          // Tabelle di base --> BATCHMOVE
          Src.Close; Src.TableName:='dbo.AreeDiplomi';
          Dest.Close; Dest.TableName:='AreeDiplomi.db';
          Src.Open; Dest.Open;
          BatchMove1.Execute;
          Src.Close; Src.TableName:='dbo.Diplomi';
          Dest.Close; Dest.TableName:='Diplomi.db';
          Src.Open; Dest.Open;
          BatchMove1.Execute;
          {Src.Close; Src.TableName:='dbo.CompetenzeMansioni';      // MOLTO GRANDE --> da evitare !!
          Dest.Close; Dest.TableName:='CompetenzeMansioni.db';
          Src.Open; Dest.Open;
          BatchMove1.Execute;}
          Src.Close; Src.TableName:='dbo.Mansioni';
          Dest.Close; Dest.TableName:='Mansioni.db';
          Src.Open; Dest.Open;
          BatchMove1.Execute;
          Src.Close; Src.TableName:='dbo.EBC_attivita';
          Dest.Close; Dest.TableName:='EBC_attivita.db';
          Src.Open; Dest.Open;
          BatchMove1.Execute;
          Src.Close; Src.TableName:='dbo.Lingue';
          Dest.Close; Dest.TableName:='Lingue.db';
          Src.Open; Dest.Open;
          BatchMove1.Execute;
          {Src.Close; Src.TableName:='dbo.Competenze';
          Dest.Close; Dest.TableName:='Competenze.db';
          Src.Open; Dest.Open;
          BatchMove1.Execute;}
          {Src.Close; Src.TableName:='dbo.AreeCompetenze';
          Dest.Close; Dest.TableName:='AreeCompetenze.db';
          Src.Open; Dest.Open;
          BatchMove1.Execute;}
          Src.Close; Src.TableName:='dbo.Aree';
          Dest.Close; Dest.TableName:='Aree.db';
          Src.Open; Dest.Open;
          BatchMove1.Execute;
          Dest.Close;

          // cancellazione file ZIP
          if FileExists(BaseDir+'EBCdb.zip ') then DeleteFile(BaseDir+'EBCdb.zip ');
          if FileExists(BaseDir+'EBCgif.zip ') then DeleteFile(BaseDir+'EBCgif.zip ');
          // compattazione
          if FileExists(BaseDir+'pkzip.exe') then begin
               if RGOpzioneExp.ItemIndex=0 then begin
                    //xstring:=BaseDir+'pkzip '+BaseDir+'EBCdb.zip '+BaseDir+'*.db '+BaseDir+'*.val '+BaseDir+'*.xg? '+BaseDir+'*.yg? '+BaseDir+'*.px '+BaseDir+'*.mb';
                    xstring:=BaseDir+'pkzip '+BaseDir+'EBCdb.zip '+BaseDir+'*.db '+BaseDir+'*.xg? '+BaseDir+'*.yg?';
                    WinExecAndWait32(xString);
                    xstring:=BaseDir+'pkzip '+BaseDir+'EBCdb.zip '+BaseDir+'*.px '+BaseDir+'*.mb '+BaseDir+'*.val';
                    WinExecAndWait32(xString);
               end;
               WinExecAndWait32(BaseDir+'pkzip '+BaseDir+'EBCgif.zip '+BaseDir+'*.gif');
               ShowMessage('Esportazione terminata: i file EBCdb.zip e EBCgif.zip sono nella directory '+BaseDir);
          end else
               MessageDlg('ATTENZIONE: non � stato trovato il file PKZIP.EXE in '+BaseDir+chr(13)+
                    'Compattazione non effettuata',mtError, [mbOK],0);
          Splash3Form.Hide;
          Splash3Form.Free;
     end;
end;

procedure TFaxPresentazForm.GetFileList;
var TSR: TSearchRec; Found: integer;
begin
     Found:=FindFirst(BaseDir+'*.gif',$21,TSR); // $21=readonly+archive
     while Found=0 do begin
          if TSR.Name[1]<>'.' then FileList.Add(BaseDir+TSR.Name);
          Found:=FindNext(TSR);
     end;
     FindClose(TSR);
end;

procedure TFaxPresentazForm.DeleteAllFiles;
var i: integer; s: string;
begin
     for i:=0 to FileList.Count-1 do begin
          s:=FileList[i];
          DeleteFile(s);
     end;
end;

function TFaxPresentazForm.WinExecAndWait32(FileName: string; Visibility: integer=SW_SHOWNORMAL): cardinal;
{ returns 0 if the Exec failed, otherwise returns the process' exit
  code when the process terminates }
var
     zAppName: array[0..512] of char;
     lpCommandLine: array[0..512] of char;
     zCurDir: array[0..255] of char;
     WorkDir: string;
     StartupInfo: TStartupInfo;
     ProcessInfo: TProcessInformation;
begin
     StrPCopy(zAppName,'');
     StrPCopy(lpCommandLine,FileName);
     GetDir(0,WorkDir);
     StrPCopy(zCurDir,WorkDir);
     FillChar(StartupInfo,Sizeof(StartupInfo),#0);
     StartupInfo.cb:=Sizeof(StartupInfo);

     StartupInfo.dwFlags:=STARTF_USESHOWWINDOW;
     StartupInfo.wShowWindow:=Visibility;
     if not CreateProcess(
          nil,{ pointer to command line string }
          lpCommandLine,
          nil,{ pointer to process security attributes }
          nil,{ pointer to thread security attributes}
          false,{ handle inheritance flag }
          CREATE_NEW_CONSOLE or{ creation flags }
          NORMAL_PRIORITY_CLASS,
          nil,{ pointer to new environment block}
          nil,{ pointer to current directory name }
          StartupInfo,{ pointer to STARTUPINFO }
          ProcessInfo) then Result:=0{ pointer to PROCESS_INF }
     else begin
          WaitforSingleObject(ProcessInfo.hProcess,INFINITE);
          GetExitCodeProcess(ProcessInfo.hProcess,Result);
     end;
end;

procedure TFaxPresentazForm.QAnagCalcFields(DataSet: TDataSet);
var xAnno,xMese,xgiorno: Word;
begin
     if QAnagDataNascita.asString='' then
          QAnagEta.asString:=''
     else begin
          DecodeDate((Date-QAnagDataNascita.Value),xAnno,xMese,xgiorno);
          // TAnagraficaEta.Value:=StrToInt(copy(xAnno,3,2));
          QAnagEta.Value:=copy(IntToStr(xAnno),3,2);
     end;
end;

procedure TFaxPresentazForm.FormShow(Sender: TObject);
var i: integer;
begin
     Caption:='[M/213] - '+Caption;
     PCSel.Visible:=False;
     for i:=0 to PCSel.PageCount-1 do
          PCSel.Pages[i].TabVisible:=False;
end;

procedure TFaxPresentazForm.ExpressButton1Click(Sender: TObject);
begin
     PCSel.ActivePageIndex:=0;
     PCSel.Visible:=true;
     ExpressButton1.Down:=True;
     ExpressButton2.Down:=False;
     ExpressButton3.Down:=False;
     ExpressButton4.Down:=False;
     ExpressButton5.Down:=False;
     QTipiMailCand.Open;
     CaricaInfoConnect;
end;

procedure TFaxPresentazForm.ExpressButton2Click(Sender: TObject);
begin
     PCSel.ActivePageIndex:=1;
     PCSel.Visible:=true;
     ExpressButton1.Down:=False;
     ExpressButton2.Down:=True;
     ExpressButton3.Down:=False;
     ExpressButton4.Down:=False;
     ExpressButton5.Down:=False;
     QTipiMailCand.Close;
end;

procedure TFaxPresentazForm.ExpressButton3Click(Sender: TObject);
begin
     PCSel.ActivePageIndex:=2;
     PCSel.Visible:=true;
     ExpressButton1.Down:=False;
     ExpressButton2.Down:=False;
     ExpressButton3.Down:=True;
     ExpressButton4.Down:=False;
     ExpressButton5.Down:=False;
     QTipiMailCand.Close;
end;

procedure TFaxPresentazForm.ExpressButton4Click(Sender: TObject);
begin
     PCSel.ActivePageIndex:=3;
     PCSel.Visible:=true;
     ExpressButton1.Down:=False;
     ExpressButton2.Down:=False;
     ExpressButton3.Down:=False;
     ExpressButton4.Down:=True;
     ExpressButton5.Down:=False;
     QTipiMailCand.Close;
end;

procedure TFaxPresentazForm.BitBtn2Click(Sender: TObject);
var i,xTot,xCRpos: integer;
     xMess,xSl: TstringList;
     Vero: boolean;
     xNonInviati,xNonInviatiSMS,xFirma,xS: string;
begin
     xTot:=0;
     for i:=1 to ASG1.RowCount-1 do begin
          ASG1.GetCheckBoxState(0,i,Vero);
          if Vero then inc(xTot);
     end;
     if xtot=0 then begin
          MessageDlg('Nessun candidato selezionato !',mtError, [mbOK],0);
          Exit;
     end;
     if CBcandMailSMS.Checked then // invio SMS avviso
          showMessage('Chiudere SMSDispatcher - se � aperto - e premere OK');

     // impostazione parametri
     NMSMTP1.Host:=EHost.Text;
     NMSMTP1.Port:=StrToInt(EPort.Text);
     NMSMTP1.UserID:=EUserID.Text;
     // oggetto (da tabella)
     NMSMTP1.PostMessage.Subject:=QTipiMailCandOggetto.Value;
     // COMPOSIZIONE MESSAGGIO
     xMess:=TStringList.create;
     // intestazione (da tabella)
     xMess.Add(QTipiMailCandIntestazione.Value);
     xMess.Add('');
     if DataRicerche.TRicerchePendIDUtente.asString<>'' then begin
          Q.Close;
          Q.SQL.Text:='select Descrizione,FirmaMail from Users where ID='+DataRicerche.TRicerchePendIDUtente.asString;
          Q.Open;
     end else begin
          Q.Close;
          Q.SQL.Text:='select Descrizione,FirmaMail from Users where ID='+IntToStr(MainForm.xIDUtenteAttuale);
          Q.Open;
     end;
     // dati ricerca (FISSI)
     if DataRicerche.TRicerchePendIDUtente.asString<>'' then begin
          if QTipiMailCandInfoRicerca.Value then begin
               xMess.Add('Codice Ricerca:  '+DataRicerche.TRicerchePendProgressivo.Value);
               xMess.Add('Posizione ricercata:  '+DataRicerche.TRicerchePendMansione.Value+' (area: '+DataRicerche.TRicerchePendArea.Value+')');
               xMess.Add('Selezionatore: '+Q.FieldByName('Descrizione').asString);
               xMess.Add('');
          end;
          // specifiche ricerca (ESTERNE)
          if QTipiMailCandAddSpecifiche.Value then begin
               xMess.Add('Specifiche ricerca:');
               xS:=DataRicerche.TRicerchePendSpecifEst.Value;
               while pos(chr(13),xS)>0 do begin
                    xCRpos:=pos(chr(13),xS);
                    xMess.Add(copy(xS,1,xCRpos-1));
                    xS:=copy(xS,xCRpos+1,length(xS));
               end;
               xMess.Add(xS);
               xMess.Add('');
          end;
     end;
     // corpo (da tabella)
     xMess.Add(QTipiMailCandCorpo.Value);
     // saluti e firma (da tabella Utenti) con ritorno a capo
     xMess.Add('');
     xS:=Q.FieldByName('FirmaMail').asString;
     while pos(chr(13),xS)>0 do begin
          xCRpos:=pos(chr(13),xS);
          xMess.Add(copy(xS,1,xCRpos-1));
          xS:=copy(xS,xCRpos+1,length(xS));
     end;
     xMess.Add(xS);

     // edit messaggio
     EmailMessageForm:=TEmailMessageForm.create(self);
     EmailMessageForm.Memo1.Lines:=xMess;
     EmailMessageForm.ShowModal;
     if EmailMessageForm.ModalResult=mrCancel then begin
          EmailMessageForm.Free;
          exit;
     end;
     xmess.Clear;
     for i:=0 to EmailMessageForm.Memo1.Lines.count-1 do
          xMess.Add(EmailMessageForm.Memo1.Lines[i]);
     ShowMessage('Predisporre connessione Internet e premere OK quando pronti');
     EmailMessageForm.Free;
     NMSMTP1.PostMessage.Body:=xMess;
     // connessione
     StatusBar1.Visible:=True;
     NMSMTP1.Connect;
     // header (mittente)
     NMSMTP1.PostMessage.FromAddress:=EAddress.Text;
     NMSMTP1.PostMessage.FromName:=EName.Text;
     // invio vero e proprio dei messaggi
     xNonInviati:='';
     xNonInviatiSMS:='';
     for i:=1 to ASG1.RowCount-1 do begin
          ASG1.GetCheckBoxState(0,i,Vero);
          if Vero then begin
               QAnag2.Close;
               QAnag2.SQL.Text:='select Email,Cognome,Nome from Anagrafica where ID='+IntToStr(xArrayIDAnag[i]);
               QAnag2.Open;
               if QAnag2.FieldByName('Email').asString='' then begin
                    xNonInviati:=xNonInviati+QAnag2.FieldByName('Cognome').asString+' '+QAnag2.FieldByName('Nome').asString+chr(13);
               end else begin
                    NMSMTP1.PostMessage.ToAddress.Add(QAnag2.FieldByName('Email').asString);
                    // invio
                    NMSMTP1.SendMail;
                    if CBcandMailSMS.Checked then begin
                         // invio SMS di avviso
                         if not AccodaSMSaDispatcher(xArrayIDAnag[i],DataRicerche.TRicerchePendID.value,
                              DataRicerche.TRicerchePendIDUtente.value,
                              DataRicerche.TRicerchePendIDCliente.Value,
                              EavvisoSMS.Text,
                              CBDispatcher1.text,
                              True) then begin
                              xNonInviatiSMS:=xNonInviatiSMS+QAnag2.FieldByName('Cognome').asString+' '+QAnag2.FieldByName('Nome').asString+chr(13);
                         end;
                    end;
                    // registrazione nello storico del candidato
                    with Data do begin
                         DB.BeginTrans;
                         try
                              Q1:=CreateQueryFromString('insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) '+
                                   'values (:xIDAnagrafica,:xIDEvento,:xDataEvento,:xAnnotazioni,:xIDRicerca,:xIDUtente,:xIDCliente)');
                              Q1.ParamByName('xIDAnagrafica').asInteger:=xArrayIDAnag[i];
                              Q1.ParamByName('xIDEvento').asInteger:=71; // 71=inviata via e-mail
                              Q1.ParamByName('xDataEvento').asDateTime:=Date;
                              Q1.ParamByName('xAnnotazioni').asString:='invio e-mail di '+QTipiMailCandDescrizione.Value;
                              Q1.ParamByName('xIDRicerca').asInteger:=DataRicerche.TRicerchePendID.Value;
                              Q1.ParamByName('xIDUtente').asInteger:=DataRicerche.TRicerchePendIDUtente.Value;
                              Q1.ParamByName('xIDCliente').asInteger:=DataRicerche.TRicerchePendIDCliente.Value;
                              Q1.ExecSQL;
                              DB.CommitTrans;
                         except
                              DB.RollbackTrans;
                              MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
                         end;
                    end;
               end;
               QAnag2.Close;
          end;
     end;
     // disconnessione
     Q.Close;
     NMSMTP1.Disconnect;
     xMess.Free;
     StatusBar1.Visible:=False;
     if xNonInviati<>'' then
          ShowMessage('Messaggi non inviati ai seguenti soggetti per mancanza indirizzo e-mail:'+chr(13)+
               xnonInviati)
     else ShowMessage('Messaggi inviati con successo');
     if CBcandMailSMS.checked then begin
          if xNonInviatiSMS<>'' then
               ShowMessage('SMS non inviati ai seguenti soggetti per mancanza o errato numero cellulare:'+chr(13)+
                    xNonInviatiSMS)
          else ShowMessage('SMS accodati con successo per tutti i destinatari. '+chr(13)+
                    'Usare SMSDispatcher per inviare i messaggi stessi.');
     end;
end;

procedure TFaxPresentazForm.NMSMTP1Connect(Sender: TObject);
begin
     StatusBar1.SimpleText:='Connesso all''Host '+EHost.Text;
end;

procedure TFaxPresentazForm.NMSMTP1Disconnect(Sender: TObject);
begin
     if StatusBar1<>nil then
          StatusBar1.SimpleText:='Disconnesso';
end;

procedure TFaxPresentazForm.NMSMTP1ConnectionFailed(Sender: TObject);
begin
     ShowMessage('Connessione fallita all''Host '+EHost.Text);
end;

procedure TFaxPresentazForm.NMSMTP1Status(Sender: TComponent;
     Status: string);
begin
     if StatusBar1<>nil then
          StatusBar1.SimpleText:=status;
end;

procedure TFaxPresentazForm.NMSMTP1EncodeStart(Filename: string);
begin
     StatusBar1.SimpleText:='Encoding '+Filename;
end;

procedure TFaxPresentazForm.NMSMTP1EncodeEnd(Filename: string);
begin
     StatusBar1.SimpleText:='Finished encoding '+Filename;
end;

procedure TFaxPresentazForm.NMSMTP1Failure(Sender: TObject);
begin
     StatusBar1.SimpleText:='Spedizione Fallita ';
end;

procedure TFaxPresentazForm.NMSMTP1HostResolved(Sender: TComponent);
begin
     StatusBar1.SimpleText:='Risoluzione Host avvenuta';
end;

procedure TFaxPresentazForm.NMSMTP1PacketSent(Sender: TObject);
begin
     StatusBar1.SimpleText:='inviati '+IntToStr(NMSMTP1.BytesSent)+
          ' bytes dei '+IntToStr(NMSMTP1.BytesTotal)+' totali da inviare';
end;

procedure TFaxPresentazForm.NMSMTP1RecipientNotFound(Recipient: string);
begin
     ShowMessage('ERRORE: Recipient "'+Recipient+'" not found');
end;

procedure TFaxPresentazForm.NMSMTP1SendStart(Sender: TObject);
begin
     StatusBar1.simpleText:='Messaggio in spedizione';
end;

procedure TFaxPresentazForm.NMSMTP1Success(Sender: TObject);
begin
     StatusBar1.SimpleText:='Messaggio spedito con successo';
end;

procedure TFaxPresentazForm.NMSMTP1HeaderIncomplete(var handled: Boolean;
     hiType: Integer);
begin
     ShowMessage('Header (intestazione) incompleto.');
end;

procedure TFaxPresentazForm.SpeedButton1Click(Sender: TObject);
var xInfo,xFirma: string;
     xCRpos,i: integer;
begin
     EmailConnectInfoForm:=TEmailConnectInfoForm.create(self);
     EmailConnectInfoForm.xIDUser:=MainForm.xIDUtenteAttuale;
     EmailConnectInfoForm.ShowModal;
     if EmailConnectInfoForm.ModalResult=mrOK then begin
          // aggiornamento database
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text:='update Users set EmailConnectInfo=:xEmailConnectInfo, FirmaMail=:xFirmaMail '+
                         'where ID='+IntToStr(MainForm.xIDUtenteAttuale);
                    Q1.ParamByName('xEmailConnectInfo').asString:=EmailConnectInfoForm.EHost.text+chr(13)+
                         EmailConnectInfoForm.EPort.text+chr(13)+
                         EmailConnectInfoForm.EUserID.text+chr(13)+
                         EmailConnectInfoForm.EName.text+chr(13)+
                         EmailConnectInfoForm.EAddress.text;
                    xFirma:='';
                    for i:=0 to EmailConnectInfoForm.Memo1.lines.count-1 do
                         xFirma:=xFirma+EmailConnectInfoForm.Memo1.lines[i]+chr(13);
                    Q1.ParamByName('xFirmaMail').asString:=xFirma;
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    // RIcaricamento info connessione SMTP
                    Q.Close;
                    Q.SQL.text:='select Nominativo,EmailConnectInfo '+
                         'from Users where ID='+IntToStr(MainForm.xIDUtenteAttuale);
                    Q.Open;
                    // host
                    xInfo:=Q.FieldByName('EmailConnectInfo').asString;
                    if xInfo='' then begin
                         EHost.text:='';
                         EPort.text:='';
                         EUserID.text:='';
                         EName.text:='';
                         EAddress.text:='';
                    end else begin
                         xCRpos:=pos(chr(13),xInfo);
                         EHost.text:=copy(xInfo,1,xCRpos-1);
                         // port
                         xinfo:=copy(xInfo,xCRpos+1,length(xInfo));
                         xCRpos:=pos(chr(13),xInfo);
                         EPort.text:=copy(xInfo,1,xCRpos-1);
                         // userID
                         xinfo:=copy(xInfo,xCRpos+1,length(xInfo));
                         xCRpos:=pos(chr(13),xInfo);
                         EUserID.text:=copy(xInfo,1,xCRpos-1);
                         // name
                         xinfo:=copy(xInfo,xCRpos+1,length(xInfo));
                         xCRpos:=pos(chr(13),xInfo);
                         EName.text:=copy(xInfo,1,xCRpos-1);
                         // address
                         xinfo:=copy(xInfo,xCRpos+1,length(xInfo));
                         EAddress.text:=xInfo;
                    end;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               end;
          end;
     end;
     EmailConnectInfoForm.Free;
end;

procedure TFaxPresentazForm.SpeedButton2Click(Sender: TObject);
begin
     TipiMailCandForm:=TTipiMailCandForm.Create(self);
     TipiMailCandForm.ShowModal;
     TipiMailCandForm.Free;
     QTipiMailCand.Close;
     QTipiMailCand.Open;
end;

procedure TFaxPresentazForm.ExpressButton5Click(Sender: TObject);
begin
     PCSel.ActivePageIndex:=4;
     PCSel.Visible:=true;
     ExpressButton1.Down:=False;
     ExpressButton2.Down:=False;
     ExpressButton3.Down:=False;
     ExpressButton4.Down:=False;
     ExpressButton5.Down:=True;
     QTipiMailCand.Close;
     CaricaInfoConnect;
end;

procedure TFaxPresentazForm.BitBtn6Click(Sender: TObject);
var i,xTot,xCRpos: integer;
     xMess: TstringList;
     xAttach: TStrings;
     Vero: boolean;
     xTipoCV,xS: string;
begin
     // messaggio PRIVACY
     MessageDlg('ATTENZIONE!! Prima di procedere con la presentazione di personale '+
          'accertarsi che il/i candidato/i sia/siano consenziente/i. (legge sulla privaci 675)',mtInformation, [mbOK],0);
     xTot:=0;
     for i:=1 to ASG1.RowCount-1 do begin
          ASG1.GetCheckBoxState(0,i,Vero);
          if Vero then inc(xTot);
     end;
     if (xtot=0)and(CBCliMailCand.Checked) then begin
          MessageDlg('Nessun candidato selezionato !',mtError, [mbOK],0);
          Exit;
     end;
     // controllo e-mail destinatario
     if TContattiCliEmail.Value='' then begin
          MessageDlg('nessun indirizzo di posta elettronica '+chr(13)+
               'disponibile per il destinario selezionato.',mtError, [mbOK],0);
          Exit;
     end;

     // impostazione parametri
     NMSMTP1.Host:=EHost.Text;
     NMSMTP1.Port:=StrToInt(EPort.Text);
     NMSMTP1.UserID:=EUserID.Text;
     // oggetto (da tabella)
     NMSMTP1.PostMessage.Subject:=EClimailOgg.Text;
     // COMPOSIZIONE MESSAGGIO
     xMess:=TStringList.create;
     // intestazione (da tabella)
     for i:=0 to MMail.lines.count-1 do
          xMess.Add(MMail.lines[i]);
     xMess.Add('');
     Q.Close;
     Q.SQL.Text:='select Descrizione,FirmaMail from Users where ID='+DataRicerche.TRicerchePendIDUtente.asString;
     Q.Open;
     // dati ricerca
     if CBCliMailRifRic.Checked then begin
          xMess.Add('Codice Ricerca:  '+DataRicerche.TRicerchePendProgressivo.Value);
          xMess.Add('Posizione ricercata:  '+DataRicerche.TRicerchePendMansione.Value+' (area: '+DataRicerche.TRicerchePendArea.Value+')');
          xMess.Add('Selezionatore: '+Q.FieldByName('Descrizione').asString);
          xMess.Add('');
     end;
     // elenco soggetti e CV allegati
     if CBCliMailCand.Checked then begin
          xAttach:=TStringList.Create;
          if RGCliMailOpz.ItemIndex=0 then xTipoCV:='i' else xTipoCV:='e';
          xMess.Add('Elenco CV inviati:');
          for i:=1 to ASG1.RowCount-1 do begin
               ASG1.GetCheckBoxState(0,i,Vero);
               if Vero then begin
                    QAnag2.Close;
                    QAnag2.SQL.Text:='select CVNumero,Cognome,Nome from Anagrafica where ID='+IntToStr(xArrayIDAnag[i]);
                    QAnag2.Open;
                    if CBCliMailCognNome.checked then
                         xMess.Add(QAnag2.FieldByName('CVNumero').asString+' - '+QAnag2.FieldByName('Cognome').asString+' '+QAnag2.FieldByName('Nome').asString)
                    else xMess.Add(QAnag2.FieldByName('CVNumero').asString);
                    // cerca e aggiungi pagina/e CV
                    if FileExists(GetCVPath+'\'+xTipoCV+QAnag2.FieldbyName('CVNumero').asString+'a.gif') then
                         xAttach.Add(GetCVPath+'\'+xTipoCV+QAnag2.FieldbyName('CVNumero').asString+'a.gif');
                    if FileExists(GetCVPath+'\'+xTipoCV+QAnag2.FieldbyName('CVNumero').asString+'b.gif') then
                         xAttach.Add(GetCVPath+'\'+xTipoCV+QAnag2.FieldbyName('CVNumero').asString+'b.gif');
                    if FileExists(GetCVPath+'\'+xTipoCV+QAnag2.FieldbyName('CVNumero').asString+'c.gif') then
                         xAttach.Add(GetCVPath+'\'+xTipoCV+QAnag2.FieldbyName('CVNumero').asString+'c.gif');

                    // registrazione nello storico del candidato e del cliente
                    with Data do begin
                         DB.BeginTrans;
                         try
                              // storico candidato
                              Q1:=CreateQueryFromString('insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) '+
                                   'values (:xIDAnagrafica,:xIDEvento,:xDataEvento,:xAnnotazioni,:xIDRicerca,:xIDUtente,:xIDCliente)');
                              Q1.ParamByName('xIDAnagrafica').asInteger:=xArrayIDAnag[i];
                              Q1.ParamByName('xIDEvento').asInteger:=83; // 83=presentato via e-mail...
                              Q1.ParamByName('xDataEvento').asDateTime:=Date;
                              Q1.ParamByName('xAnnotazioni').asString:=DataRicerche.TRicerchePendCliente.Value+' '+EClimailOgg.Text;
                              Q1.ParamByName('xIDRicerca').asInteger:=DataRicerche.TRicerchePendID.Value;
                              Q1.ParamByName('xIDUtente').asInteger:=DataRicerche.TRicerchePendIDUtente.Value;
                              Q1.ParamByName('xIDCliente').asInteger:=DataRicerche.TRicerchePendIDCliente.Value;
                              Q1.ExecSQL;
                              // storico invio cliente
                              Q1:=CreateQueryFromString('insert into StoricoInvioClienti (IDCliente,Tipo,Data,Descrizione) '+
                                   'values (:xIDCliente,:xTipo,:xData,:xDescrizione)');
                              Q1.ParamByName('xIDCliente').asInteger:=DataRicerche.TRicerchePendIDCliente.Value;
                              Q1.ParamByName('xTipo').asString:='e-mail';
                              Q1.ParamByName('xData').asDateTime:=Date;
                              Q1.ParamByName('xDescrizione').asString:='invio CV in allegato';
                              Q1.ExecSQL;
                              DB.CommitTrans;
                         except
                              DB.RollbackTrans;
                              MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
                         end;
                    end;
               end;
               QAnag2.Close;
          end;
          // aggiungi tutti gli attachment
          NMSMTP1.PostMessage.Attachments.AddStrings(xAttach);
          xAttach.Free;
     end;
     // saluti e firma (da tabella Utenti) con ritorno a capo
     xMess.Add('');
     xS:=Q.FieldByName('FirmaMail').asString;
     while pos(chr(13),xS)>0 do begin
          xCRpos:=pos(chr(13),xS);
          xMess.Add(copy(xS,1,xCRpos-1));
          xS:=copy(xS,xCRpos+1,length(xS));
     end;
     xMess.Add(xS);
     // messaggio CONFIDENTIAL
     xMess.Add('');
     xMess.Add('ATTENTION:  Privileged - Confidential information may be contained in this message.');
     xMess.Add(' If you are not the addressee indicated in this message, you may not copy or deliver this message to anyone.');
     // edit messaggio
     EmailMessageForm:=TEmailMessageForm.create(self);
     EmailMessageForm.Memo1.Lines:=xMess;
     EmailMessageForm.ShowModal;
     if EmailMessageForm.ModalResult=mrCancel then begin
          EmailMessageForm.Free;
          exit;
     end;
     xmess.Clear;
     for i:=0 to EmailMessageForm.Memo1.Lines.count-1 do
          xMess.Add(EmailMessageForm.Memo1.Lines[i]);
     ShowMessage('Predisporre connessione Internet e premere OK quando pronti');
     EmailMessageForm.Free;
     NMSMTP1.PostMessage.Body:=xMess;
     // connessione
     StatusBar1.Visible:=True;
     NMSMTP1.Connect;
     // header (mittente)
     NMSMTP1.PostMessage.FromAddress:=EAddress.Text;
     NMSMTP1.PostMessage.FromName:=EName.Text;
     // invio vero e proprio dei messaggi
     NMSMTP1.PostMessage.ToAddress.Add(TContattiCliEmail.Value);
     // invio
     NMSMTP1.SendMail;

     // disconnessione
     Q.Close;
     NMSMTP1.Disconnect;
     xMess.Free;
     StatusBar1.Visible:=False;
     ShowMessage('Messaggi inviati con successo');
end;

procedure TFaxPresentazForm.CBCliMailCandClick(Sender: TObject);
begin
     GroupBox2.Visible:=CBCliMailCand.Checked;
end;

procedure TFaxPresentazForm.CaricaInfoConnect;
var xInfo: string;
     xCRpos: integer;
begin
     // caricamento info connessione SMTP
     Q.Close;
     Q.SQL.text:='select Nominativo,EmailConnectInfo '+
          'from Users where ID='+IntToStr(MainForm.xIDUtenteAttuale);
     Q.Open;
     // host
     xInfo:=Q.FieldByName('EmailConnectInfo').asString;
     if xInfo='' then begin
          EHost.text:='';
          EPort.text:='';
          EUserID.text:='';
          EName.text:='';
          EAddress.text:='';
     end else begin
          xCRpos:=pos(chr(13),xInfo);
          EHost.text:=copy(xInfo,1,xCRpos-1);
          // port
          xinfo:=copy(xInfo,xCRpos+1,length(xInfo));
          xCRpos:=pos(chr(13),xInfo);
          EPort.text:=copy(xInfo,1,xCRpos-1);
          // userID
          xinfo:=copy(xInfo,xCRpos+1,length(xInfo));
          xCRpos:=pos(chr(13),xInfo);
          EUserID.text:=copy(xInfo,1,xCRpos-1);
          // name
          xinfo:=copy(xInfo,xCRpos+1,length(xInfo));
          xCRpos:=pos(chr(13),xInfo);
          EName.text:=copy(xInfo,1,xCRpos-1);
          // address
          xinfo:=copy(xInfo,xCRpos+1,length(xInfo));
          EAddress.text:=xInfo;
     end;
end;

procedure TFaxPresentazForm.CBcandMailSMSClick(Sender: TObject);
begin
     EavvisoSMS.Visible:=CBcandMailSMS.Checked;
     CBDispatcher1.Visible:=CBcandMailSMS.Checked;
     if CBcandMailSMS.Checked then
          EavvisoSMS.text:=EavvisoSMS.text+' '+Data.GlobalNomeAzienda.Value;
end;

procedure TFaxPresentazForm.BitBtn5Click(Sender: TObject);
var i: integer;
     vero: boolean;
     xNonInviati: string;
begin
     // invio SMS
     showMessage('Chiudere SMSDispatcher - se � aperto - e premere OK');
     xNonInviati:='';
     for i:=1 to ASG1.RowCount-1 do begin
          ASG1.GetCheckBoxState(0,i,Vero);
          if Vero then begin
               if not AccodaSMSaDispatcher(xArrayIDAnag[i],DataRicerche.TRicerchePendID.value,
                    DataRicerche.TRicerchePendIDUtente.value,
                    DataRicerche.TRicerchePendIDCliente.Value,
                    ETestoSMS.Text,
                    CBDispatcher2.text,
                    True) then begin
                    QAnag2.Close;
                    QAnag2.SQL.Text:='select CVNumero,Cognome,Nome from Anagrafica where ID='+IntToStr(xArrayIDAnag[i]);
                    QAnag2.Open;
                    xNonInviati:=xNonInviati+QAnag2.FieldByName('Cognome').asString+' '+QAnag2.FieldByName('Nome').asString+chr(13);
                    // registrazione nello storico del candidato
                    with Data do begin
                         DB.BeginTrans;
                         try
                              Q1:=CreateQueryFromString('insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) '+
                                   'values (:xIDAnagrafica,:xIDEvento,:xDataEvento,:xAnnotazioni,:xIDRicerca,:xIDUtente,:xIDCliente)');
                              Q1.ParamByName('xIDAnagrafica').asInteger:=xArrayIDAnag[i];
                              Q1.ParamByName('xIDEvento').asInteger:=72; // 71=inviato SMS
                              Q1.ParamByName('xDataEvento').asDateTime:=Date;
                              Q1.ParamByName('xAnnotazioni').asString:='Testo: '+ETestoSMS.Text;
                              Q1.ParamByName('xIDRicerca').asInteger:=DataRicerche.TRicerchePendID.Value;
                              Q1.ParamByName('xIDUtente').asInteger:=DataRicerche.TRicerchePendIDUtente.Value;
                              Q1.ParamByName('xIDCliente').asInteger:=DataRicerche.TRicerchePendIDCliente.Value;
                              Q1.ExecSQL;
                              DB.CommitTrans;
                         except
                              DB.RollbackTrans;
                              MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
                         end;
                    end;
               end;
          end;
          QAnag2.Close;
     end;
     if xNonInviati<>'' then
          ShowMessage('SMS non inviati ai seguenti soggetti per mancanza o errato numero cellulare:'+chr(13)+
               xnonInviati)
     else ShowMessage('SMS accodati con successo per tutti i destinatari. '+chr(13)+
               'Usare SMSDispatcher per inviare i messaggi stessi.');
end;

procedure TFaxPresentazForm.ASG1GetCellColor(Sender: TObject; ARow,
     ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
     // controllo esistenza indirizzo e-mail e cellulare
     if ARow>0 then begin
          QAnag2.Close;
          QAnag2.SQL.Text:='select email,cellulare from Anagrafica where ID='+IntToStr(xArrayIDAnag[aRow]);
          QAnag2.Open;
          if ACol=1 then begin// e-mail
               if QAnag2.FieldByName('email').asString='' then
                    ABrush.Color:=clRed
               else ABrush.Color:=clLime;
          end;
          if ACol=2 then begin// cellulare
               if (QAnag2.FieldByName('cellulare').asString='')or
                    ((pos('-',QAnag2.FieldByName('Cellulare').asString)=0)and
                    (pos('/',QAnag2.FieldByName('Cellulare').asString)=0)) then
                    ABrush.Color:=clRed
               else ABrush.Color:=clLime;
          end;
          QAnag2.Close;
     end;
end;

procedure TFaxPresentazForm.ASG1CanEditCell(Sender: TObject; Arow,
     Acol: Integer; var canedit: Boolean);
begin
     if (ARow>0)and(Acol>0) then canedit:=False
     else canedit:=True;
end;

procedure TFaxPresentazForm.ExpressButton6Click(Sender: TObject);
var xFile: string;
     xSalva,xStampa,Vero: boolean;
     i: integer;
begin
     SceltaModelloAnagForm:=TSceltaModelloAnagForm.create(self);
     SceltaModelloAnagForm.Height:=238;
     SceltaModelloAnagForm.QModelliWord.SQL.text:='select * from ModelliWord '+
          'where TabellaMaster=:xTabMaster';
     SceltaModelloAnagForm.QModelliWord.ParamByName('xTabMaster').asString:='Anagrafica';
     SceltaModelloAnagForm.QModelliWord.Open;
     SceltaModelloAnagForm.ShowModal;
     if SceltaModelloAnagForm.ModalResult=mrOK then begin
          xsalva:=SceltaModelloAnagForm.CBSalva.Checked;
          xStampa:=False;
          for i:=1 to ASG1.RowCount-1 do begin
               ASG1.GetCheckBoxState(0,i,Vero);
               if Vero then begin
                    PosizionaAnag(xArrayIDAnag[i]);
                    xFile:=CreaFileWordAnag(SceltaModelloAnagForm.QModelliWordID.Value,
                         SceltaModelloAnagForm.QModelliWordNomeModello.Value,
                         xSalva,xStampa);
                    if xsalva then begin
                         // associa al soggetto
                         with Data do begin
                              DB.BeginTrans;
                              try
                                   Q1.Close;
                                   Q1.SQL.text:='insert into AnagFile (IDAnagrafica,IDTipo,Descrizione,NomeFile,DataCreazione) '+
                                        'values (:xIDAnagrafica,:xIDTipo,:xDescrizione,:xNomeFile,:xDataCreazione)';
                                   Q1.ParamByName('xIDAnagrafica').asInteger:=xArrayIDAnag[i];
                                   Q1.ParamByName('xIDTipo').asInteger:=3;
                                   Q1.ParamByName('xDescrizione').asString:=SceltaModelloAnagForm.QModelliWordDescrizione.Value;
                                   Q1.ParamByName('xNomeFile').asString:=xFile;
                                   Q1.ParamByName('xDataCreazione').asDateTime:=Date;
                                   Q1.ExecSQL;
                                   DB.CommitTrans;
                              except
                                   DB.RollbackTrans;
                                   MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
                              end;
                         end;
                    end;
               end;
          end;
     end;
     SceltaModelloAnagForm.Free;
end;

procedure TFaxPresentazForm.ExpressButton7Click(Sender: TObject);
var xFile: string;
     Vero: boolean;
     i: integer;
begin
     ShowMessage('Utilizzare pulsante in anagrafica soggetti');
     Exit;

     // DISABILITATO !!
     // presentazione a seconda del cliente - sorgente in MAINFORM
     if Uppercase(copy(Data.GlobalNomeAzienda.Value,1,6))='LEDERM' then begin
          // seleziona contatto
          SelContattiForm:=TSelContattiForm.create(self);
          SelContattiForm.QContatti.ParamByName('xIDCliente').asInteger:=DataRicerche.TRicerchePendIDCliente.asInteger;
          SelContattiForm.QContatti.Open;
          SelContattiForm.ShowModal;
          if SelContattiForm.Modalresult=mrOK then
               for i:=1 to ASG1.RowCount-1 do begin
                    ASG1.GetCheckBoxState(0,i,Vero);
                    if Vero then begin
                         PosizionaAnag(xArrayIDAnag[i]);
                         MainForm.CompilaFilePresPosLWG(DataRicerche.TRicerchePendMansione.Value,
                              DataRicerche.QCandRicID.Value,
                              DataRicerche.TRicerchePendIDCliente.Value,
                              SelContattiForm.QContattiID.Value);
                    end;
                    SelContattiForm.Free;
               end;
     end else begin
          for i:=1 to ASG1.RowCount-1 do begin
               ASG1.GetCheckBoxState(0,i,Vero);
               if Vero then begin
                    PosizionaAnag(xArrayIDAnag[i]);
                    MainForm.CompilaFilePresPos(DataRicerche.TRicerchePendCliente.Value,
                         DataRicerche.TRicerchePendMansione.Value,
                         DataRicerche.QCandRicID.Value);
               end;
          end;
     end;
end;

end.

