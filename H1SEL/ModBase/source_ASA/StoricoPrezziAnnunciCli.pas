unit StoricoPrezziAnnunciCli;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     Db,Grids,DBGrids,RXDBCtrl,DBTables,StdCtrls,ExtCtrls,Buttons,
     ComCtrls,Mask,DBCtrls;

type
     TStoricoPrezziAnnunciCliForm=class(TForm)
          BitBtn1: TBitBtn;
          Panel1: TPanel;
          Label1: TLabel;
          ECliente: TEdit;
          QStoricoCli: TQuery;
          dsQStoricoCli: TDataSource;
          QStoricoCliData: TDateTimeField;
          QStoricoCliCostoLire: TFloatField;
          QStoricoCliCostoEuro: TFloatField;
          QStoricoCliCostoANoiLire: TFloatField;
          QStoricoCliCostoANoiEuro: TFloatField;
          QStoricoCliNomeEdizione: TStringField;
          QStoricoCliTestata: TStringField;
          QStoricoCliScontoAlClienteLire: TFloatField;
          QStoricoCliScontoAlClienteEuro: TFloatField;
          QStoricoCliScontoANoiLire: TFloatField;
          QStoricoCliScontoANoiEuro: TFloatField;
          PCValute: TPageControl;
          TSLire: TTabSheet;
          TSEuro: TTabSheet;
          RxDBGrid1: TRxDBGrid;
          QStoricoCliNumModuli: TIntegerField;
          QStoricoCliGuadagnoLire: TFloatField;
          QStoricoCliGuadagnoEuro: TFloatField;
          Panel2: TPanel;
          QTotali: TQuery;
          QTotalisumCostoLire: TFloatField;
          QTotalisumCostoEuro: TFloatField;
          QTotalisumCostoANoiLire: TFloatField;
          QTotalisumCostoANoiEuro: TFloatField;
          QTotalisumScontoAlClienteLire: TFloatField;
          QTotalisumScontoAlClienteEuro: TFloatField;
          QTotalisumScontoANoiLire: TFloatField;
          QTotalisumScontoANoiEuro: TFloatField;
          DBEdit1: TDBEdit;
          DsQTotali: TDataSource;
          DBEdit2: TDBEdit;
          DBEdit3: TDBEdit;
          DBEdit4: TDBEdit;
          QTotaliGuadagnoLire: TFloatField;
          QTotaliGuadagnoEuro: TFloatField;
          DBEdit5: TDBEdit;
          Label2: TLabel;
    QTotaliTotmoduli: TIntegerField;
    DBEdit6: TDBEdit;
    Panel3: TPanel;
    Label3: TLabel;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    RxDBGrid2: TRxDBGrid;
          procedure FormCreate(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure RxDBGrid1TitleClick(Column: TColumn);
          procedure QStoricoCliCalcFields(DataSet: TDataSet);
          procedure QTotaliCalcFields(DataSet: TDataSet);
     private
          { Private declarations }
     public
          xIDCliente: integer;
     end;

var
     StoricoPrezziAnnunciCliForm: TStoricoPrezziAnnunciCliForm;

implementation

{$R *.DFM}

procedure TStoricoPrezziAnnunciCliForm.FormCreate(Sender: TObject);
begin
     xIDCliente:=0;
end;

procedure TStoricoPrezziAnnunciCliForm.FormShow(Sender: TObject);
begin
     QStoricoCli.ParamByName('xIDCliente').asInteger:=xIDCliente;
     QStoricoCli.open;
     QTotali.ParamByName('xIDCliente').asInteger:=xIDCliente;
     QTotali.open;
     caption:='[M/180] - '+caption;
     PCValute.ActivePage:=TSEuro;
end;

procedure TStoricoPrezziAnnunciCliForm.RxDBGrid1TitleClick(Column: TColumn);
begin
     QStoricoCli.Close;
     QStoricoCli.SQL.text:='select Ann_AnnEdizData.Data,CostoLire,CostoEuro, '+
          '       CostoANoiLire,CostoANoiEuro,Ann_AnnEdizData.NumModuli, '+
          '       ScontoAlClienteLire,ScontoAlClienteEuro, '+
          '       ScontoANoiLire,ScontoANoiEuro, '+
          '       NomeEdizione, Ann_Testate.Denominazione Testata '+
          'from Ann_AnnEdizData, Ann_Edizioni, Ann_Testate, Ann_Annunci '+
          'where Ann_AnnEdizData.IDEdizione=Ann_Edizioni.ID '+
          '  and Ann_Edizioni.IDTestata=Ann_Testate.ID '+
          '  and Ann_AnnEdizData.IDAnnuncio=Ann_Annunci.ID '+
          '  and Ann_Annunci.IDCliente=:xIDCliente ';
     if Column=RxDBGrid1.Columns[0] then
          QStoricoCli.SQL.Add('order by Ann_AnnEdizData.Data desc');
     if Column=RxDBGrid1.Columns[1] then
          QStoricoCli.SQL.Add('order by Ann_Testate.Denominazione,NomeEdizione');
     if Column=RxDBGrid1.Columns[2] then
          QStoricoCli.SQL.Add('order by NomeEdizione');
     if Column=RxDBGrid1.Columns[3] then
          QStoricoCli.SQL.Add('order by NumModuli');
     if Column=RxDBGrid1.Columns[4] then
          QStoricoCli.SQL.Add('order by CostoLire');
     if Column=RxDBGrid1.Columns[5] then
          QStoricoCli.SQL.Add('order by ScontoAlClienteLire');
     if Column=RxDBGrid1.Columns[6] then
          QStoricoCli.SQL.Add('order by CostoANoiLire');
     if Column=RxDBGrid1.Columns[7] then
          QStoricoCli.SQL.Add('order by ScontoANoiLire');
     QStoricoCli.Open;
end;

procedure TStoricoPrezziAnnunciCliForm.QStoricoCliCalcFields(
     DataSet: TDataSet);
begin
     QStoricoCliGuadagnoLire.Value:=QStoricoCliCostoLire.Value-QStoricoCliCostoANoiLire.Value;
     QStoricoCliGuadagnoEuro.Value:=QStoricoCliCostoEuro.Value-QStoricoCliCostoANoiEuro.Value;
end;

procedure TStoricoPrezziAnnunciCliForm.QTotaliCalcFields(
     DataSet: TDataSet);
begin
     QTotaliGuadagnoLire.Value:=QTotalisumCostoLire.Value-QTotalisumCostoANoiLire.Value;
     QTotaliGuadagnoEuro.Value:=QTotalisumCostoEuro.Value-QTotalisumCostoANoiEuro.Value;
end;

end.

