object ModRuoloForm: TModRuoloForm
  Left = 219
  Top = 106
  ActiveControl = SEAnniEsp
  BorderStyle = bsDialog
  Caption = 'Modifica dati ruolo'
  ClientHeight = 77
  ClientWidth = 265
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 4
    Top = 7
    Width = 90
    Height = 13
    Caption = 'Mesi di esperienza:'
  end
  object BitBtn1: TBitBtn
    Left = 167
    Top = 2
    Width = 96
    Height = 36
    TabOrder = 3
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 167
    Top = 39
    Width = 96
    Height = 36
    Caption = 'Annulla'
    TabOrder = 4
    Kind = bkCancel
  end
  object CBAttuale: TCheckBox
    Left = 4
    Top = 41
    Width = 97
    Height = 17
    Caption = 'Ruolo attuale'
    TabOrder = 2
  end
  object SEAnniEsp: TRxSpinEdit
    Left = 100
    Top = 4
    Width = 57
    Height = 21
    TabOrder = 0
  end
  object CBDisponibile: TCheckBox
    Left = 4
    Top = 26
    Width = 153
    Height = 17
    Caption = 'Disponibile per questo ruolo'
    TabOrder = 1
  end
  object CBAspirazione: TCheckBox
    Left = 4
    Top = 57
    Width = 97
    Height = 17
    Caption = 'Aspirazione'
    TabOrder = 5
  end
end
