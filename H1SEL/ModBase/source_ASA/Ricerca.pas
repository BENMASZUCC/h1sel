unit Ricerca;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ExtCtrls, StdCtrls, Buttons, DBCtrls, Mask, Grids, DBGrids, DB,
  DBDateTimePicker;

type
  TRicercaForm = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel2: TPanel;
    Panel4: TPanel;
    BitBtn4: TBitBtn;
    BitBtn5: TBitBtn;
    BitBtn6: TBitBtn;
    Panel3: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    GroupBox1: TGroupBox;
    CBArea: TCheckBox;
    CBRuolo: TCheckBox;
    CBSpec: TCheckBox;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    CoBEta: TComboBox;
    MEeta: TMaskEdit;
    CBEta: TCheckBox;
    CBProvDom: TCheckBox;
    MEProvDom: TMaskEdit;
    DBGrid2: TDBGrid;
    TBCarattRicNew: TBitBtn;
    TBCarattRicDel: TBitBtn;
    TBCarattRicOK: TBitBtn;
    TBCarattRicCan: TBitBtn;
    DBLArea: TDBLookupComboBox;
    DBLRuolo: TDBLookupComboBox;
    DBLSpec: TDBLookupComboBox;
    GroupBox6: TGroupBox;
    DBGrid3: TDBGrid;
    CBSesso: TCheckBox;
    CBStatoCivile: TCheckBox;
    CBDispAuto: TCheckBox;
    CBServMil: TCheckBox;
    CBDispMov: TCheckBox;
    CBoSesso: TComboBox;
    CBoServMil: TComboBox;
    DBGStatoCivile: TDBGrid;
    DBGDispMov: TDBGrid;
    BitBtn7: TBitBtn;
    Image3: TImage;
    Image4: TImage;
    Image5: TImage;
    Label3: TLabel;
    Panel1: TPanel;
    Image7: TImage;
    Image8: TImage;
    Image9: TImage;
    Label4: TLabel;
    DBGrid4: TDBGrid;
    GroupBox7: TGroupBox;
    DBEdit1: TDBEdit;
    DBGrid5: TDBGrid;
    BitBtn8: TBitBtn;
    Label5: TLabel;
    Label6: TLabel;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    Label7: TLabel;
    TabSheet3: TTabSheet;
    Panel5: TPanel;
    Image1: TImage;
    Image2: TImage;
    Image11: TImage;
    SpeedButton3: TSpeedButton;
    Panel6: TPanel;
    Label8: TLabel;
    Label9: TLabel;
    BitBtn9: TBitBtn;
    BitBtn10: TBitBtn;
    BitBtn11: TBitBtn;
    BitBtn12: TBitBtn;
    BitBtn13: TBitBtn;
    BitBtn14: TBitBtn;
    BitBtn17: TBitBtn;
    DBGrid6: TDBGrid;
    GroupBox2: TGroupBox;
    Panel7: TPanel;
    DBGrid7: TDBGrid;
    DBGrid1: TDBGrid;
    Panel8: TPanel;
    Panel9: TPanel;
    Splitter1: TSplitter;
    BitBtn15: TBitBtn;
    BitBtn16: TBitBtn;
    GroupBox5: TGroupBox;
    DBDateTimePicker1: TDBDateTimePicker;
    DBGrid8: TDBGrid;
    TBTitoliRicNew: TBitBtn;
    TBTitoliRicDel: TBitBtn;
    TBTitoliRicOK: TBitBtn;
    TBTitoliRicCan: TBitBtn;
    Label10: TLabel;
    DBLookupComboBox1: TDBLookupComboBox;
    Label11: TLabel;
    procedure FormShow(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure CBAreaClick(Sender: TObject);
    procedure CBRuoloClick(Sender: TObject);
    procedure CBSpecClick(Sender: TObject);
    procedure CBEtaClick(Sender: TObject);
    procedure CBProvDomClick(Sender: TObject);
    procedure TBAltreCompNewClick(Sender: TObject);
    procedure TBAltreCompDelClick(Sender: TObject);
    procedure TBAltreCompOKClick(Sender: TObject);
    procedure TBAltreCompCanClick(Sender: TObject);
    procedure TBCarattRicNewClick(Sender: TObject);
    procedure TBCarattRicDelClick(Sender: TObject);
    procedure TBCarattRicOKClick(Sender: TObject);
    procedure TBCarattRicCanClick(Sender: TObject);
    procedure CBSessoClick(Sender: TObject);
    procedure CBServMilClick(Sender: TObject);
    procedure CBStatoCivileClick(Sender: TObject);
    procedure CBDispMovClick(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
    procedure BitBtn8Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure BitBtn10Click(Sender: TObject);
    procedure BitBtn12Click(Sender: TObject);
    procedure BitBtn13Click(Sender: TObject);
    procedure BitBtn14Click(Sender: TObject);
    procedure CBoSessoChange(Sender: TObject);
    procedure BitBtn15Click(Sender: TObject);
    procedure BitBtn16Click(Sender: TObject);
    procedure TBTitoliRicNewClick(Sender: TObject);
    procedure TBTitoliRicDelClick(Sender: TObject);
    procedure TBTitoliRicOKClick(Sender: TObject);
    procedure TBTitoliRicCanClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  RicercaForm: TRicercaForm;

implementation

uses ModuloDati3, ModuloDati, ModCompMans, CheckSQL, DecisionCube, Splash,
  Splash2, ContattoCand, StoriaContatti, Curriculum, Colloquio;

{$R *.DFM}

procedure TRicercaForm.FormShow(Sender: TObject);
begin
     if DataSel_EBC.TRicerchePendStato.value='sel.criteri ricerca' then
        PageControl1.ActivePage:=TabSheet1;
     if DataSel_EBC.TRicerchePendStato.value='sel.candidati in evidenza' then
        PageControl1.ActivePage:=TabSheet2;
     if DataSel_EBC.TRicerchePendStato.value='programmazione colloqui' then
        PageControl1.ActivePage:=TabSheet3;

     TabSheet1.TabVisible:=False;
     TabSheet2.TabVisible:=False;
     TabSheet3.TabVisible:=False;
end;

procedure TRicercaForm.BitBtn1Click(Sender: TObject);
begin
     close;
end;

procedure TRicercaForm.BitBtn2Click(Sender: TObject);
begin
     DataSel_EBC.QSel.Close;
     DataSel_EBC.TRicerchePend.Edit;
     DataSel_EBC.TRicerchePendStato.value:='sel.criteri ricerca';
     DataSel_EBC.TRicerchePend.Post;
     PageControl1.ActivePage:=TabSheet1;
end;

procedure TRicercaForm.BitBtn4Click(Sender: TObject);
begin
     // cancellazione ricerca
     if MessageDlg('Vuoi cancellare la ricerca ?',mtWarning,[mbNo,mbYes],0)=mrYes then
        DataSel_EBC.TRicerchePend.Delete;

     close;
end;

procedure TRicercaForm.BitBtn6Click(Sender: TObject);
var xAnno,xMese,xgiorno:Word;
    xDiff,xDaScartare,xdaInserire,xUltimo:Integer;
    xDallaData:TDateTime;
    xOp:string;
    xScarta,xEsci:boolean;
begin
  Splash2Form := TSplash2Form.Create(nil);
  Splash2Form.Prog.Position:=0;
  Splash2Form.Show;
  Splash2Form.Update;

     if DataSel_EBC.DsRicerchePend.State in [dsEdit,dsInsert] then begin
        if CBEta.Checked then DataSel_EBC.TRicerchePendC_Eta.Value:=COBEta.Text+MEEta.text
        else DataSel_EBC.TRicerchePendC_Eta.Value:='';
        if CBProvDom.Checked then DataSel_EBC.TRicerchePendC_DomicilioProv.Value:=MEProvDom.text
        else DataSel_EBC.TRicerchePendC_DomicilioProv.Value:='';
        if CBSesso.Checked then DataSel_EBC.TRicerchePendC_Sesso.Value:=CBOSesso.text
        else DataSel_EBC.TRicerchePendC_Sesso.Value:='';
        if CBStatoCivile.Checked then DataSel_EBC.TRicerchePendC_StatoCivile.Value:=Data.TStatiCiviliID.asString
        else DataSel_EBC.TRicerchePendC_StatoCivile.Value:='';
        if CBDispAuto.Checked then DataSel_EBC.TRicerchePendC_DispAuto.Value:='S'
        else DataSel_EBC.TRicerchePendC_DispAuto.asString:='';
        if CBServMil.Checked then DataSel_EBC.TRicerchePendC_ServMilitare.Value:=CBoServMil.Text
        else DataSel_EBC.TRicerchePendC_ServMilitare.Value:='';
        if CBDispMov.Checked then DataSel_EBC.TRicerchePendC_DispMov.Value:=Data.TDispMovID.asString
        else DataSel_EBC.TRicerchePendC_DispMov.Value:='';

        DataSel_EBC.TRicerchePend.Post;
     end;

   Splash2Form.Prog.Position:=2;

     // selezione candidati -> predisposiz. Query
     DataSel_EBC.QSel.Close;
     DataSel_EBC.QSel.SQL.clear;
     DataSel_EBC.QSel.SQL.add('select *');
     DataSel_EBC.QSel.SQL.add('from Anagrafica,AnagMansioni,Mansioni,Disponibilita');
     if CBSpec.Checked then
        DataSel_EBC.QSel.SQL.add(',Specializzazioni,AnagSpec');
     DataSel_EBC.QSel.SQL.add('where Anagrafica.ID=AnagMansioni.IDAnagrafica');
     DataSel_EBC.QSel.SQL.add('   and AnagMansioni.IDMansione=Mansioni.ID');
     DataSel_EBC.QSel.SQL.add('   and Mansioni.ID='+DataSel_EBC.TRicerchePendIDMansione.asString);
     DataSel_EBC.QSel.SQL.add('   and Anagrafica.ID=Disponibilita.IDAnagrafica');
     DataSel_EBC.QSel.SQL.add('   and Disponibilita.IDMansione=Mansioni.ID');
     // specializzazione
     if CBSpec.Checked then begin
        DataSel_EBC.QSel.SQL.add('   and Anagrafica.ID=AnagSpec.IDAnagrafica');
        DataSel_EBC.QSel.SQL.add('   and AnagSpec.IDSpec=Specializzazioni.ID');
        DataSel_EBC.QSel.SQL.add('   and Specializzazioni.ID='+DataSel_EBC.TRicerchePendIDSpec.asString);
     end;

     // sui dati personali di QSel
     if CBProvDom.Checked then
        DataSel_EBC.QSel.SQL.add('and Anagrafica.DomicilioProvincia='''+MEProvDom.text+'''');
     if CBsesso.Checked then
        DataSel_EBC.QSel.SQL.add('and Anagrafica.Sesso='''+CBOSesso.text+'''');
     if CBStatoCivile.Checked then
        DataSel_EBC.QSel.SQL.add('and Anagrafica.IDStatoCivile='+Data.TStatiCiviliID.asString);
     if CBDispAuto.Checked then
        DataSel_EBC.QSel.SQL.add('and Anagrafica.DispAuto=True');
     if CBServMil.Checked then
        DataSel_EBC.QSel.SQL.add('and Anagrafica.ServizioMilitareStato='''+CBoServMil.Text+'''');
     if CBDispMov.Checked then
        DataSel_EBC.QSel.SQL.add('and Anagrafica.IDDispMov='+Data.TDispMovID.asString);

  Splash2Form.Prog.Position:=4;

     if CBEta.Checked then begin
        DecodeDate(Date,xAnno,xMese,xgiorno);
        xDiff:=xAnno-StrToInt(MEEta.text);
        //xDallaData:=EncodeDate(xDiff,xMese,xGiorno);
        xOp:=COBEta.Text;
        if COBEta.Text='>' then xOp:='<';
        if COBEta.Text='>=' then xOp:='<=';
        if COBEta.Text='<' then xOp:='>';
        if COBEta.Text='<=' then xOp:='>=';
        DataSel_EBC.QSel.SQL.add('and DataNascita'+xOp+''''+IntToStr(xMese)+'/'+IntToStr(xGiorno)+'/'+IntToStr(xDiff)+'''');
     end;

     DataSel_EBC.QSel.SQL.SaveToFile('c:\SQLTrace1.txt');
     DataSel_EBC.QSel.Open;

     DataSel_EBC.QAnagComp.Close;
     DataSel_EBC.QAnagComp.SQL.Clear;
     DataSel_EBC.QAnagComp.SQL.LoadFromFile('c:\QAnagComp.txt');
     DataSel_EBC.QAnagComp.SQL.Add('AND CompMans.IDMansione='+DataSel_EBC.TRicerchePendIDMansione.AsString);

     // sui dati personali di QAnagComp
     if CBProvDom.Checked then
        DataSel_EBC.QAnagComp.SQL.add('and Anag.DomicilioProvincia='''+MEProvDom.text+'''');
     if CBsesso.Checked then
        DataSel_EBC.QAnagComp.SQL.add('and Anag.Sesso='''+CBOSesso.text+'''');
     if CBStatoCivile.Checked then
        DataSel_EBC.QAnagComp.SQL.add('and Anag.IDStatoCivile='+Data.TStatiCiviliID.asString);
     if CBDispAuto.Checked then
        DataSel_EBC.QAnagComp.SQL.add('and Anag.DispAuto=True');
     if CBServMil.Checked then
        DataSel_EBC.QAnagComp.SQL.add('and Anag.ServizioMilitareStato='''+CBoServMil.Text+'''');
     if CBDispMov.Checked then
        DataSel_EBC.QAnagComp.SQL.add('and Anag.IDDispMov='+Data.TDispMovID.asString);

  Splash2Form.Prog.Position:=6;

     if CBEta.Checked then begin
        DecodeDate(Date,xAnno,xMese,xgiorno);
        xDiff:=xAnno-StrToInt(MEEta.text);
        //xDallaData:=EncodeDate(xDiff,xMese,xGiorno);
        xOp:=COBEta.Text;
        if COBEta.Text='>' then xOp:='<';
        if COBEta.Text='>=' then xOp:='<=';
        if COBEta.Text='<' then xOp:='>';
        if COBEta.Text='<=' then xOp:='>=';
        DataSel_EBC.QAnagComp.SQL.add('and Anag.DataNascita'+xOp+''''+IntToStr(xMese)+'/'+IntToStr(xGiorno)+'/'+IntToStr(xDiff)+'''');
     end;

     DataSel_EBC.QAnagComp.SQL.SaveToFile('c:\SQLTrace2.txt');

  Splash2Form.Prog.Position:=8;

     // scarto dei candidati che non rispettano i valori
     DataSel_EBC.QAnagComp.Close;
      // cancellazione precedenti risultati
      DataSel_EBC.QDelCandRicComp.SQL.Clear;
      DataSel_EBC.QDelCandRicComp.SQL.Add('Delete from EBC_CandRicComp');
      DataSel_EBC.QDelCandRicComp.SQL.Add('where IDRicerca='+DataSel_EBC.TRicerchePendID.asString);
      DataSel_EBC.QDelCandRicComp.ExecSQL;
     DataSel_EBC.QAnagComp.Open;
     DataSel_EBC.QAnagComp.First;
     xDaScartare:=0;
     DataSel_EBC.TAnagCaratt.IndexName:='IdxAnagCaratt';
     while not DataSel_EBC.QAnagComp.EOF do begin

       xScarta:=False;
       // controllo per ognuna delle caratteristiche ricerate
       DataSel_EBC.TCarattRic.First;
       while not DataSel_EBC.TCarattRic.EOF do begin
             if not DataSel_EBC.TAnagCaratt.FindKey([DataSel_EBC.QAnagCompID.Value,
                      DataSel_EBC.TCarattRicIDCaratt.Value]) then
             begin xScarta:=True;
                   xDaScartare:=DataSel_EBC.QAnagCompID.Value; end;
          DataSel_EBC.TCarattRic.Next;
       end;

      if not xScarta then begin
       if xDaScartare<>DataSel_EBC.QAnagCompID.Value then begin
         DataSel_EBC.TCompMans.FindKey([DataSel_EBC.QAnagCompIDCompetenza.Value,
                                        DataSel_EBC.TRicerchePendIDMansione.Value]);
         if DataSel_EBC.TCompMansOperatore.asString='>=' then begin
            if DataSel_EBC.QAnagCompValore.Value>=DataSel_EBC.TCompMansValore.Value then
               DataSel_EBC.TCandRicComp.InsertRecord([DataSel_EBC.QAnagCompID.Value])
            else xDaScartare:=DataSel_EBC.QAnagCompID.Value;
         end;
         if DataSel_EBC.TCompMansOperatore.asString='>' then begin
            if DataSel_EBC.QAnagCompValore.Value>DataSel_EBC.TCompMansValore.Value then
               DataSel_EBC.TCandRicComp.InsertRecord([DataSel_EBC.QAnagCompID.Value])
            else xDaScartare:=DataSel_EBC.QAnagCompID.Value;
         end;
         if DataSel_EBC.TCompMansOperatore.asString='=' then begin
            if DataSel_EBC.QAnagCompValore.Value=DataSel_EBC.TCompMansValore.Value then
               DataSel_EBC.TCandRicComp.InsertRecord([DataSel_EBC.QAnagCompID.Value])
            else xDaScartare:=DataSel_EBC.QAnagCompID.Value;
         end;
         if DataSel_EBC.TCompMansOperatore.asString='<' then begin
            if DataSel_EBC.QAnagCompValore.Value<DataSel_EBC.TCompMansValore.Value then
               DataSel_EBC.TCandRicComp.InsertRecord([DataSel_EBC.QAnagCompID.Value])
            else xDaScartare:=DataSel_EBC.QAnagCompID.Value;
         end;
         if DataSel_EBC.TCompMansOperatore.asString='<=' then begin
            if DataSel_EBC.QAnagCompValore.Value<=DataSel_EBC.TCompMansValore.Value then
               DataSel_EBC.TCandRicComp.InsertRecord([DataSel_EBC.QAnagCompID.Value])
            else xDaScartare:=DataSel_EBC.QAnagCompID.Value;
         end;
       end;
      end;
      DataSel_EBC.QAnagComp.Next;
     end;
     DataSel_EBC.TAnagCaratt.IndexName:='';
     DataSel_EBC.TCandRicComp.Close;
     DataSel_EBC.TCandRicComp.Open;

     // eliminazione doppioni
     // --> da fare

  Splash2Form.Prog.Position:=9;

     DataSel_EBC.DAnagComp.Close;
     DataSel_EBC.DAnagComp.SQL.Clear;
     DataSel_EBC.DAnagComp.SQL.LoadFromFile('c:\DAnagComp.txt');
     DataSel_EBC.DAnagComp.SQL.add('WHERE  CompMans.IDMansione ='+DataSel_EBC.TRicerchePendIDMansione.AsString);
     // sui dati personali di DAnagComp
     if CBProvDom.Checked then
        DataSel_EBC.DAnagComp.SQL.add('and Anag.DomicilioProvincia='''+MEProvDom.text+'''');
     if CBsesso.Checked then
        DataSel_EBC.DAnagComp.SQL.add('and Anag.Sesso='''+CBOSesso.text+'''');
     if CBStatoCivile.Checked then
        DataSel_EBC.DAnagComp.SQL.add('and Anag.IDStatoCivile='+Data.TStatiCiviliID.asString);
     if CBDispAuto.Checked then
        DataSel_EBC.DAnagComp.SQL.add('and Anag.DispAuto=True');
     if CBServMil.Checked then
        DataSel_EBC.DAnagComp.SQL.add('and Anag.ServizioMilitareStato='''+CBoServMil.Text+'''');
     if CBDispMov.Checked then
        DataSel_EBC.DAnagComp.SQL.add('and Anag.IDDispMov='+Data.TDispMovID.asString);

     if CBEta.Checked then begin
        DecodeDate(Date,xAnno,xMese,xgiorno);
        xDiff:=xAnno-StrToInt(MEEta.text);
        xDallaData:=EncodeDate(xDiff,xMese,xGiorno);
        xOp:=COBEta.Text;
        if COBEta.Text='>' then xOp:='<';
        if COBEta.Text='>=' then xOp:='<=';
        if COBEta.Text='<' then xOp:='>';
        if COBEta.Text='<=' then xOp:='>=';
        DataSel_EBC.DAnagComp.SQL.add('and Anag.DataNascita'+xOp+''''+DateTimeToStr(xDallaData)+'''');
     end;
     // caratteristiche
//     if not DataSel_EBC.TCarattRic.EOF then begin
//        DataSel_EBC.QSel.SQL.add('   and Anagrafica.ID=AnagCaratteristiche.IDAnagrafica');
//        DataSel_EBC.QSel.SQL.add('   and AnagCaratteristiche.IDCaratt=EBC_CarattRicercate.IDCaratt');
//        DataSel_EBC.QSel.SQL.add('   and EBC_CarattRicercate.IDRicerca='+DataSel_EBC.TRicerchePendID.asString);
//     end;

     DataSel_EBC.DAnagComp.SQL.add('GROUP BY Anag.Cognome, Comp.Descrizione');

  Splash2Form.Prog.Position:=10;
  Splash2Form.Hide;
  Splash2Form.Free;

    DataSel_EBC.TRicerchePend.Edit;
    DataSel_EBC.TRicerchePendStato.value:='sel.candidati in evidenza';
    DataSel_EBC.TRicerchePend.Post;
    PageControl1.ActivePage:=TabSheet2;
end;

procedure TRicercaForm.CBAreaClick(Sender: TObject);
begin
     if CBArea.Checked then begin
        DBLArea.Enabled:=True;
        DBLArea.Color:=clWindow;
     end
     else begin
        DBLArea.Enabled:=False;
        DBLArea.Color:=clBtnFace;
     end;
end;

procedure TRicercaForm.CBRuoloClick(Sender: TObject);
begin
     if CBRuolo.Checked then begin
        DBLRuolo.Enabled:=True;
        DBLRuolo.Color:=clWindow;
     end
     else begin
        DBLRuolo.Enabled:=False;
        DBLRuolo.Color:=clBtnFace;
     end;
end;

procedure TRicercaForm.CBSpecClick(Sender: TObject);
begin
     if CBSpec.Checked then begin
        DBLSpec.Enabled:=True;
        DBLSpec.Color:=clWindow;
     end
     else begin
        DBLSpec.Enabled:=False;
        DBLSpec.Color:=clBtnFace;
     end;
end;

procedure TRicercaForm.CBEtaClick(Sender: TObject);
begin
     if CBEta.Checked then begin
        CoBEta.Enabled:=True;
        CoBEta.Color:=clWindow;
        MEeta.Enabled:=True;
        Meeta.Color:=clWindow;
     end
     else begin
        CoBEta.Enabled:=False;
        CoBEta.Color:=clBtnFace;
        MEeta.Enabled:=False;
        Meeta.Color:=clBtnFace;
     end;
end;

procedure TRicercaForm.CBProvDomClick(Sender: TObject);
begin
     if CBProvDom.Checked then begin
        MEProvDom.Enabled:=True;
        MEProvDom.Color:=clWindow;
     end
     else begin
        MEProvDom.Enabled:=False;
        MEProvDom.Color:=clBtnFace;
     end;
end;

procedure TRicercaForm.TBAltreCompNewClick(Sender: TObject);
begin
     DataSel_EBC.TCompUltRic.Insert;
end;

procedure TRicercaForm.TBAltreCompDelClick(Sender: TObject);
begin
     DataSel_EBC.TCompUltRic.Delete;
end;

procedure TRicercaForm.TBAltreCompOKClick(Sender: TObject);
begin
     DataSel_EBC.TCompUltRic.Post;
end;

procedure TRicercaForm.TBAltreCompCanClick(Sender: TObject);
begin
     DataSel_EBC.TCompUltRic.Cancel;
end;

procedure TRicercaForm.TBCarattRicNewClick(Sender: TObject);
begin
     DataSel_EBC.TCarattRic.Insert;
end;

procedure TRicercaForm.TBCarattRicDelClick(Sender: TObject);
begin
     DataSel_EBC.TCarattRic.Delete;
end;

procedure TRicercaForm.TBCarattRicOKClick(Sender: TObject);
begin
     DataSel_EBC.TCarattRic.Post;
end;

procedure TRicercaForm.TBCarattRicCanClick(Sender: TObject);
begin
     DataSel_EBC.TCarattRic.Cancel;
end;

procedure TRicercaForm.CBSessoClick(Sender: TObject);
begin
     if CBSesso.Checked then begin
        CBoSesso.Enabled:=True;
        CBoSesso.Color:=clWindow;
     end
     else begin
        CBoSesso.Enabled:=False;
        CBoSesso.Color:=clBtnFace;
     end;
end;

procedure TRicercaForm.CBServMilClick(Sender: TObject);
begin
     if CBServMil.Checked then begin
        CBoServMil.Enabled:=True;
        CBoServMil.Color:=clWindow;
     end
     else begin
        CBoServMil.Enabled:=False;
        CBoServMil.Color:=clBtnFace;
     end;
end;

procedure TRicercaForm.CBStatoCivileClick(Sender: TObject);
begin
     if CBStatoCivile.Checked then begin
        DBGStatoCivile.Enabled:=True;
        DBGStatoCivile.Color:=clWindow;
     end
     else begin
        DBGStatoCivile.Enabled:=False;
        DBGStatoCivile.Color:=clBtnFace;
     end;
end;

procedure TRicercaForm.CBDispMovClick(Sender: TObject);
begin
     if CBDispMov.Checked then begin
        DBGDispMov.Enabled:=True;
        DBGDispMov.Color:=clWindow;
     end
     else begin
        DBGDispMov.Enabled:=False;
        DBGDispMov.Color:=clBtnFace;
     end;
end;

procedure TRicercaForm.BitBtn7Click(Sender: TObject);
begin
     ModCompMansForm:=TModCompMansForm.Create(self);
     DataSel_EBC.TCompMansione.Edit;
     ModCompMansForm.ShowModal;
     ModCompMansForm.Free;
end;

procedure TRicercaForm.BitBtn8Click(Sender: TObject);
begin
     DecisionForm:=TDecisionForm.create(self);
     DataSel_EBC.DAnagComp.Open;
     DecisionForm.ShowModal;
     DecisionForm.Free;
end;

procedure TRicercaForm.SpeedButton1Click(Sender: TObject);
begin
     if not DataSel_EBC.TCandRicAnag.FindKey([DataSel_EBC.TRicerchePendID.Value,
                                              DataSel_EBC.QSelID.Value])
     then DataSel_EBC.TCandRicerca.InsertRecord([DataSel_EBC.QSelID.Value,'da contattare',0]);
end;

procedure TRicercaForm.SpeedButton2Click(Sender: TObject);
begin
     if not DataSel_EBC.TCandRicAnag.FindKey([DataSel_EBC.TRicerchePendID.Value,
                                              DataSel_EBC.TCandRicCompIDAnagrafica.Value])
     then DataSel_EBC.TCandRicerca.InsertRecord([DataSel_EBC.TCandRicCompIDAnagrafica.Value,'da contattare',0]);
end;

procedure TRicercaForm.BitBtn3Click(Sender: TObject);
begin
    DataSel_EBC.TRicerchePend.Edit;
    DataSel_EBC.TRicerchePendStato.value:='programmazione colloqui';
    DataSel_EBC.TRicerchePend.Post;
    PageControl1.ActivePage:=TabSheet3;
end;

procedure TRicercaForm.SpeedButton3Click(Sender: TObject);
begin
     DataSel_EBC.TCandRicerca.Delete;
end;

procedure TRicercaForm.BitBtn10Click(Sender: TObject);
begin
    DataSel_EBC.TRicerchePend.Edit;
    DataSel_EBC.TRicerchePendStato.value:='sel.candidati in evidenza';
    DataSel_EBC.TRicerchePend.Post;
    PageControl1.ActivePage:=TabSheet2;
end;

procedure TRicercaForm.BitBtn12Click(Sender: TObject);
begin
//     if DataSel_EBC.TCandRicercaStato.Value='da contattare' then

     ContattoCandForm:=TContattoCandForm.create(self);
     DataSel_EBC.TAnag.FindKey([DataSel_EBC.TCandRicercaIDAnagrafica.Value]);
     DataSel_EBC.TContattiCand.Insert;
     DataSel_EBC.TContattiCandEvaso.Value:=False;
     DataSel_EBC.TContattiCandData.Value:=Date;
     DataSel_EBC.TContattiCandOra.Value:=Time;
     ContattoCandForm.Height:=176;
     ContattoCandForm.Width:=373;

     ContattoCandForm.ShowModal;
     ContattoCandForm.Free;
end;

procedure TRicercaForm.BitBtn13Click(Sender: TObject);
begin
     StoriaContattiForm:=TStoriaContattiForm.create(self);
     StoriaContattiForm.ShowModal;
     StoriaContattiForm.Free;
end;

procedure TRicercaForm.BitBtn14Click(Sender: TObject);
begin
     Data.TAnagrafica.IndexName:='';
     Data.TAnagrafica.FindKey([DataSel_EBC.TCandRicercaIDAnagrafica.Value]);
     CurriculumForm.ShowModal;
     Data.TAnagrafica.IndexName:='IdxCognome';
end;

procedure TRicercaForm.CBoSessoChange(Sender: TObject);
begin
     if CBoSesso.text='M' then
        Data.TStatiCivili.Filter:='Sesso=''M''';
     if CBoSesso.text='F' then
        Data.TStatiCivili.Filter:='Sesso=''F''';
     if CBoSesso.text='' then
        Data.TStatiCivili.Filter:='';
end;

procedure TRicercaForm.BitBtn15Click(Sender: TObject);
begin
     Data.TAnagrafica.IndexName:='';
     Data.TAnagrafica.FindKey([DataSel_EBC.TColloquiRicIDAnagrafica.Value]);
     CurriculumForm.ShowModal;
     Data.TAnagrafica.IndexName:='IdxCognome';
end;

procedure TRicercaForm.BitBtn16Click(Sender: TObject);
begin
     ColloquioForm:=TColloquioForm.create(self);
     DataSel_EBC.TColloqui2.FindKey([DataSel_EBC.TColloquiRicID.Value]);
     Data.TAnagrafica.IndexName:='';
     Data.TAnagrafica.FindKey([DataSel_EBC.TColloquiRicIDAnagrafica.Value]);
     DataSel_EBC.QCarattCand.Filtered:=True;
     DataSel_EBC.QCarattCand.Filter:='ID='+DataSel_EBC.TRicerchePendID.asString;
     Data.TAnagrafica.IndexName:='IdxCognome';
     ColloquioForm.ShowModal;
     ColloquioForm.free;
end;


procedure TRicercaForm.TBTitoliRicNewClick(Sender: TObject);
begin
     DataSel_EBC.TTitoliStudiioRic.Insert;
end;

procedure TRicercaForm.TBTitoliRicDelClick(Sender: TObject);
begin
     DataSel_EBC.TTitoliStudiioRic.Delete;
end;

procedure TRicercaForm.TBTitoliRicOKClick(Sender: TObject);
begin
     DataSel_EBC.TTitoliStudiioRic.Post;
end;

procedure TRicercaForm.TBTitoliRicCanClick(Sender: TObject);
begin
     DataSel_EBC.TTitoliStudiioRic.Cancel;
end;

end.
