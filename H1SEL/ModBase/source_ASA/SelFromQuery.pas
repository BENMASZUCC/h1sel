unit SelFromQuery;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, Db, DBTables, StdCtrls, Buttons, ExtCtrls;

type
  TSelFromQueryForm = class(TForm)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    QGen: TQuery;
    DsQGen: TDataSource;
    DBGrid1: TDBGrid;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    xNumInvisibleCols:integer;
  end;

var
  SelFromQueryForm: TSelFromQueryForm;

implementation

{$R *.DFM}

procedure TSelFromQueryForm.FormCreate(Sender: TObject);
begin
     xNumInvisibleCols:=0;
end;

procedure TSelFromQueryForm.FormShow(Sender: TObject);
var i:integer;
begin
     for i:=0 to xNumInvisibleCols-1 do
        DBGrid1.Columns[i].Visible:=False;
end;

end.
