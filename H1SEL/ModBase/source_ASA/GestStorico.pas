unit GestStorico;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  TB97, StdCtrls, Mask, DBCtrls, ExtCtrls, DB, Grids, DBGrids, DBTables,
  DBCGrids;

type
  TGestStoricoForm = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    ToolbarButton971: TToolbarButton97;
    Panel3: TPanel;
    BEventoIns: TToolbarButton97;
    BEventoDel: TToolbarButton97;
    BEventoMod: TToolbarButton97;
    ToolbarButton972: TToolbarButton97;
    DBCtrlGrid1: TDBCtrlGrid;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    Label7: TLabel;
    DBEdit9: TDBEdit;
    Panel19: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    QStorico: TQuery;
    procedure ToolbarButton971Click(Sender: TObject);
    procedure BEventoDelClick(Sender: TObject);
    procedure BEventoInsClick(Sender: TObject);
    procedure BEventoModClick(Sender: TObject);
    procedure ToolbarButton972Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  GestStoricoForm: TGestStoricoForm;

implementation

uses ModuloDati, InsEvento, QRStorico, Main;

{$R *.DFM}

procedure TGestStoricoForm.ToolbarButton971Click(Sender: TObject);
begin
     close;
end;

procedure TGestStoricoForm.BEventoDelClick(Sender: TObject);
begin
     if not Data.QStorico.EOF then
        if MessageDlg('Sei sicuro di voler eliminare l''evento selezionato ?',mtWarning,
           [mbNo,mbYes],0)=mrYes then begin
           QStorico.Close;
           QStorico.SQL.text:='delete from Storico where ID='+Data.QStoricoID.asString;
           QStorico.ExecSQL;
           Data.QStorico.Close;
           Data.QStorico.Open;
        end;
end;

procedure TGestStoricoForm.BEventoInsClick(Sender: TObject);
begin
     InsEventoForm:=TInsEventoForm.create(self);
     InsEventoForm.xIDUtente:=MainForm.xIDUtenteAttuale;
     InsEventoForm.ShowModal;
     if InsEventoForm.ModalResult=mrOK then begin
        Data.DB.BeginTrans;
        try
           Data.Q1.SQL.text:='insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) '+
                             'values (:xIDAnagrafica,:xIDEvento,:xDataEvento,:xAnnotazioni,:xIDRicerca,:xIDUtente,:xIDCliente)';
           Data.Q1.ParambyName('xIDAnagrafica').asInteger:=Data.TAnagraficaID.Value;
           Data.Q1.ParambyName('xIDEvento').asInteger:=InsEventoForm.TEventiID.Value;
           Data.Q1.ParambyName('xDataEvento').asDateTime:=InsEventoForm.DEData.Date;
           Data.Q1.ParambyName('xAnnotazioni').asString:=InsEventoForm.EDesc.Text;
           Data.Q1.ParambyName('xIDRicerca').asInteger:=0;
           Data.Q1.ParambyName('xIDUtente').asInteger:=InsEventoForm.QUsersID.Value;
           Data.Q1.ParambyName('xIDCliente').asInteger:=0;
           Data.Q1.ExecSQL;

           Data.DB.CommitTrans;
           Data.QStorico.Close;
           Data.QStorico.Open;
        except
             Data.DB.RollbackTrans;
             MessageDlg('ERRORE SUL DATABASE:  operazione non completata',mtError,[mbOK],0);
        end;
     end;
     InsEventoForm.Free;
end;

procedure TGestStoricoForm.BEventoModClick(Sender: TObject);
begin
     QStorico.Close;
     QStorico.SQL.text:='Select ID,DataEvento,Annotazioni,IDEvento,IDUtente from Storico where ID='+Data.QStoricoID.asString;
     QStorico.Open;
     InsEventoForm:=TInsEventoForm.create(self);
     InsEventoForm.DEData.Date:=QStorico.FieldbyName('DataEvento').asDateTime;
     InsEventoForm.EDesc.Text:=QStorico.FieldbyName('Annotazioni').asString;
     InsEventoForm.TEventi.IndexName:='';
     InsEventoForm.TEventi.FindKey([QStorico.FieldbyName('IDEvento').asInteger]);
     InsEventoForm.TEventi.IndexName:='IdxDaStato';
     InsEventoForm.xIDUtente:=QStorico.FieldbyName('IDUtente').asInteger;
     InsEventoForm.ShowModal;
     if InsEventoForm.ModalResult=mrOK then begin
        Data.DB.BeginTrans;
        try
           Data.Q1.Close;
           Data.Q1.SQL.text:='update Storico set DataEvento=:xDataEvento,Annotazioni=:xAnnotazioni,IDEvento=:xIDEvento,IDUtente=:xIDUtente where ID='+QStorico.FieldbyName('ID').asString;
           Data.Q1.ParambyName('xDataEvento').asDateTime:=InsEventoForm.DEData.Date;
           Data.Q1.ParambyName('xAnnotazioni').asString:=InsEventoForm.EDesc.Text;
           Data.Q1.ParambyName('xIDEvento').asInteger:=InsEventoForm.TEventiID.Value;
           Data.Q1.ParambyName('xIDUtente').asInteger:=InsEventoForm.QUsersID.Value;
           Data.Q1.ExecSQL;
           Data.DB.CommitTrans;
           Data.QStorico.Close;
           Data.QStorico.Open;
        except
            Data.DB.RollbackTrans;
            MessageDlg('ERRORE SUL DATABASE:  operazione non completata',mtError,[mbOK],0);
        end;
     end;
     QStorico.Close;
     InsEventoForm.Free;
end;

procedure TGestStoricoForm.ToolbarButton972Click(Sender: TObject);
begin
     QRStoricoForm:=TQRStoricoForm.create(self);
     try QRStoricoForm.QuickRep1.Preview
     finally QRStoricoForm.Free
     end;
end;

procedure TGestStoricoForm.FormShow(Sender: TObject);
begin
     Caption:='[M/040] - '+Caption;
end;

end.
