unit InsCaratt;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Grids, DBGrids, Db, DBTables, ExtCtrls;

type
  TInsCarattForm = class(TForm)
    Panel1: TPanel;
    TCaratt: TTable;
    DsCaratt: TDataSource;
    TCarattID: TAutoIncField;
    TCarattDescrizione: TStringField;
    DBGrid1: TDBGrid;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    procedure DBGrid1KeyPress(Sender: TObject; var Key: Char);
    procedure BitBtn1Click(Sender: TObject);
  private
    xStringa:string;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  InsCarattForm: TInsCarattForm;

implementation

{$R *.DFM}

procedure TInsCarattForm.DBGrid1KeyPress(Sender: TObject; var Key: Char);
begin
     if Key=chr(13) then begin
        BitBtn1.Click;
     end else begin
        xStringa:=xStringa+key;
        TCaratt.FindNearest([xStringa]);
     end;
end;

procedure TInsCarattForm.BitBtn1Click(Sender: TObject);
begin
     if TCaratt.EOF then begin
        MessageDlg('Nessuna caratteristica selezionata !',mtError,[mbOK],0);
        ModalResult:=mrNone;
        Abort;
     end;
end;

end.
