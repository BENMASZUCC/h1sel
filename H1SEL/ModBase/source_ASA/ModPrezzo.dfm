object ModPrezzoForm: TModPrezzoForm
  Left = 218
  Top = 106
  ActiveControl = PrezzoEuro
  BorderStyle = bsDialog
  Caption = 'Modifica prezzo'
  ClientHeight = 86
  ClientWidth = 296
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel
    Left = 6
    Top = 67
    Width = 35
    Height = 13
    Caption = 'Prezzo:'
  end
  object Label6: TLabel
    Left = 148
    Top = 43
    Width = 19
    Height = 14
    Caption = 'Lire'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsItalic]
    ParentFont = False
  end
  object Label7: TLabel
    Left = 270
    Top = 43
    Width = 23
    Height = 14
    Caption = 'Euro'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsItalic]
    ParentFont = False
  end
  object Bevel1: TBevel
    Left = 44
    Top = 57
    Width = 122
    Height = 5
    Shape = bsTopLine
  end
  object Bevel2: TBevel
    Left = 172
    Top = 57
    Width = 122
    Height = 5
    Shape = bsTopLine
  end
  object BitBtn1: TBitBtn
    Left = 104
    Top = 2
    Width = 95
    Height = 38
    TabOrder = 1
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 200
    Top = 2
    Width = 95
    Height = 38
    Caption = 'Annulla'
    TabOrder = 2
    Kind = bkCancel
  end
  object PrezzoLire: TRxCalcEdit
    Left = 44
    Top = 63
    Width = 123
    Height = 21
    AutoSize = False
    DisplayFormat = '�'#39'.'#39' ,0.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    FormatOnEditing = True
    NumGlyphs = 2
    ParentFont = False
    TabOrder = 0
    OnChange = PrezzoLireChange
  end
  object PrezzoEuro: TRxCalcEdit
    Left = 171
    Top = 63
    Width = 123
    Height = 21
    AutoSize = False
    DisplayFormat = '�'#39'.'#39' ,0.00'
    FormatOnEditing = True
    NumGlyphs = 2
    TabOrder = 3
    OnChange = PrezzoEuroChange
  end
end
