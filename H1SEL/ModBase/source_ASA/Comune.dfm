object ComuneForm: TComuneForm
  Left = 219
  Top = 106
  ActiveControl = EDesc
  BorderStyle = bsDialog
  Caption = 'Dati Comune'
  ClientHeight = 81
  ClientWidth = 477
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 6
    Top = 9
    Width = 76
    Height = 13
    Caption = 'Denominazione:'
  end
  object Label2: TLabel
    Left = 38
    Top = 34
    Width = 44
    Height = 13
    Caption = 'Provincia'
  end
  object Label3: TLabel
    Left = 124
    Top = 34
    Width = 19
    Height = 13
    Caption = 'Cap'
  end
  object Label4: TLabel
    Left = 202
    Top = 34
    Width = 37
    Height = 13
    Caption = 'Prefisso'
  end
  object Label5: TLabel
    Left = 293
    Top = 34
    Width = 43
    Height = 13
    Caption = 'Regione:'
  end
  object Label6: TLabel
    Left = 287
    Top = 58
    Width = 60
    Height = 13
    Caption = 'Area Nielsen'
  end
  object BitBtn1: TBitBtn
    Left = 378
    Top = 3
    Width = 97
    Height = 36
    TabOrder = 6
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 378
    Top = 41
    Width = 97
    Height = 36
    Caption = 'Annulla'
    TabOrder = 7
    Kind = bkCancel
  end
  object EDesc: TEdit
    Left = 89
    Top = 5
    Width = 284
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 50
    TabOrder = 0
  end
  object EProv: TEdit
    Left = 89
    Top = 29
    Width = 25
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 2
    TabOrder = 1
  end
  object ECap: TEdit
    Left = 148
    Top = 29
    Width = 49
    Height = 21
    MaxLength = 5
    TabOrder = 2
  end
  object EPrefisso: TEdit
    Left = 244
    Top = 29
    Width = 41
    Height = 21
    MaxLength = 4
    TabOrder = 3
  end
  object EReg: TEdit
    Left = 341
    Top = 29
    Width = 31
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 3
    TabOrder = 4
  end
  object EAreaNielsen: TEdit
    Left = 352
    Top = 53
    Width = 20
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 1
    TabOrder = 5
  end
end
