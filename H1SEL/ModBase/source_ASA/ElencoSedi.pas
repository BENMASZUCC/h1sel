unit ElencoSedi;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, Db, DBTables, StdCtrls, Buttons;

type
  TElencoSediForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    QSedi: TQuery;
    DsQSedi: TDataSource;
    QSediID: TAutoIncField;
    QSediDescrizione: TStringField;
    QSediIndirizzo: TStringField;
    QSediCap: TStringField;
    QSediComune: TStringField;
    QSediProvincia: TStringField;
    DBGrid1: TDBGrid;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ElencoSediForm: TElencoSediForm;

implementation

{$R *.DFM}

procedure TElencoSediForm.FormShow(Sender: TObject);
begin
     Caption:='[S/10] - '+caption;
end;

end.
