unit SelRichiesta;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, Db, DBTables, StdCtrls, Mask, DBCtrls, Buttons, ExtCtrls;

type
  TSelRichiestaForm = class(TForm)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Panel2: TPanel;
    Label1: TLabel;
    DBEdit5: TDBEdit;
    Panel73: TPanel;
    TRichInt: TTable;
    DsRichInt: TDataSource;
    TRichIntID: TAutoIncField;
    TRichIntRichiesta: TStringField;
    TRichIntIDQuestione: TIntegerField;
    TRichIntProponiDimissioni: TBooleanField;
    DBGrid1: TDBGrid;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SelRichiestaForm: TSelRichiestaForm;

implementation

uses ModuloDati;

{$R *.DFM}

end.
