unit InsDettFatt;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     ExtCtrls,StdCtrls,Buttons,Db,Grids,DBGrids,DBTables,Mask,DBCtrls,
     ComCtrls,AdvGrid;

type
     TInsDettFattForm=class(TForm)
          BitBtn1: TBitBtn;
          QInsNonFatt: TQuery;
          DsQInsNonFatt: TDataSource;
          QInsNonFattID: TIntegerField;
          QInsNonFattCognome: TStringField;
          QInsNonFattNome: TStringField;
          QInsNonFattDallaData: TDateTimeField;
          QInsNonFattProgressivo: TStringField;
          QInsNonFattRuolo: TStringField;
          QInsNonFattDataInizio: TDateTimeField;
          TInserimentiABS: TTable;
          TInserimentiABSID: TAutoIncField;
          TInserimentiABSIDAnagrafica: TIntegerField;
          TInserimentiABSIDCliente: TIntegerField;
          TInserimentiABSDallaData: TDateTimeField;
          QInsNonFattSesso: TStringField;
          QInsNonFattStipendioLordo: TFloatField;
          TInserimentiABSProgFattura: TIntegerField;
          DsQCompensi: TDataSource;
          QCompensi: TQuery;
          QCompensiImporto: TFloatField;
          QCompensiIDFattura: TIntegerField;
          QCompensiRifRicerca: TStringField;
          QCompensiNote: TStringField;
          Panel3: TPanel;
          Label1: TLabel;
          DBEdit1: TDBEdit;
          QCompensiIDRicerca: TIntegerField;
          Q: TQuery;
          QCompensiID: TIntegerField;
          PC: TPageControl;
          TSCompensi: TTabSheet;
          TSNoteSpese: TTabSheet;
          TSAnnunci: TTabSheet;
          DBGrid2: TDBGrid;
          BitBtn4: TBitBtn;
          RGOpz: TRadioGroup;
          Panel2: TPanel;
          PCSoggetti: TPageControl;
          TSSoggInseriti: TTabSheet;
          TSSoggCandRic: TTabSheet;
          Panel1: TPanel;
          Panel4: TPanel;
          DBGrid1: TDBGrid;
          Panel5: TPanel;
          Panel6: TPanel;
          ASG1: TAdvStringGrid;
          QCandRicCliente: TQuery;
          QCandRicClienteID: TIntegerField;
          QCandRicClienteCognome: TStringField;
          QCandRicClienteNome: TStringField;
          QCandRicClienteRifRicerca: TStringField;
          QCandRicClienteIDRicerca: TIntegerField;
          TAnagABS: TTable;
          TAnagABSID: TAutoIncField;
          TAnagABSCognome: TStringField;
          TAnagABSNome: TStringField;
          TAnagABSSesso: TStringField;
          QNoteSpeseCli: TQuery;
          DsQNoteSpeseCli: TDataSource;
          DBGrid3: TDBGrid;
          BNotaSpeseIns: TBitBtn;
          QNoteSpeseCliID: TIntegerField;
          QNoteSpeseCliData: TDateTimeField;
          QNoteSpeseCliDescrizione: TStringField;
          QNoteSpeseCliRifRicerca: TStringField;
          QNoteSpeseCliTotSpese: TFloatField;
          QNoteSpeseCliAutomobile: TFloatField;
          QNoteSpeseCliAutostrada: TFloatField;
          QNoteSpeseCliAereotreno: TFloatField;
          QNoteSpeseCliTaxi: TFloatField;
          QNoteSpeseCliAlbergo: TFloatField;
          QNoteSpeseCliRistoranteBar: TFloatField;
          QNoteSpeseCliParking: TFloatField;
          QNoteSpeseCliAltroImporto: TFloatField;
          Panel7: TPanel;
          QAnnunciCli: TQuery;
          DsQAnnunciCli: TDataSource;
          DBGrid4: TDBGrid;
          BInsAnnuncio: TBitBtn;
          QAnnunciCliData: TDateTimeField;
          QAnnunciCliRif: TStringField;
          QAnnunciCliID: TIntegerField;
          QAnnunciCliNumModuli: TSmallintField;
          QAnnunciCliNumParole: TSmallintField;
          QAnnunciCliTotaleLire: TCurrencyField;
          QAnnunciCliRifRicerca: TStringField;
          QAnnunciCli2: TQuery;
          QAnnunciCli2Data: TDateTimeField;
          QAnnunciCli2Rif: TStringField;
          QAnnunciCli2ID: TAutoIncField;
          QAnnunciCli2NumModuli: TSmallintField;
          QAnnunciCli2NumParole: TSmallintField;
          QAnnunciCli2TotaleLire: TFloatField;
          DsQAnnunciCli2: TDataSource;
          Panel8: TPanel;
          DBGrid5: TDBGrid;
          BInsAnnuncio2: TBitBtn;
          QCompensiTipo: TStringField;
    QAnnunciCliTotaleEuro: TFloatField;
    QAnnunciCli2TotaleEuro: TFloatField;
          procedure RGOpzClick(Sender: TObject);
          procedure BitBtn4Click(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure BNotaSpeseInsClick(Sender: TObject);
          procedure QNoteSpeseCliCalcFields(DataSet: TDataSet);
          procedure BInsAnnuncioClick(Sender: TObject);
          procedure BInsAnnuncio2Click(Sender: TObject);
     private
          xArrayIDAnag: array[1..100] of integer;
          procedure RiempiASG1;
          procedure RicalcolaTotFattura;
     public
          { Public declarations }
     end;

var
     InsDettFattForm: TInsDettFattForm;

implementation

uses Fattura,ModuloDati2,ModuloDati;

{$R *.DFM}

procedure TInsDettFattForm.RGOpzClick(Sender: TObject);
begin
     case RGOpz.ItemIndex of
          0: PCSoggetti.visible:=False;
          1: begin PCSoggetti.visible:=True; PCSoggetti.ActivePage:=TSSoggInseriti end;
          2: begin
                    PCSoggetti.visible:=True;
                    PCSoggetti.ActivePage:=TSSoggCandRic;
                    if ASG1.Cells[0,1]='' then
                         RiempiASG1;
               end;
     end;
end;

procedure TInsDettFattForm.BitBtn4Click(Sender: TObject);
var xDic,xCand,xTipo,xProgr: string;
     i,k,xIDIns: integer;
     Vero: boolean;
     xTotImponibile: real;
begin
     if DBGrid2.SelectedRows.Count=0 then begin
        showMessage('nessuna riga selezionata');
        exit;
     end;
     if DBGrid2.SelectedRows.Count>1 then begin
          // controllo stesso tipo
          xtipo:=QCompensiTipo.value;
          with DBGrid2.SelectedRows do begin
               for K:=0 to Count-1 do begin
                    QCompensi.BookMark:=Items[K];
                    if xTipo<>QCompensiTipo.value then
                         if MessageDlg('ATTENZIONE: sono stati selezionati compensi di tipo diverso ! '+
                              'SEI SICURO DI VOLER PROCEDERE ?',mtWarning, [mbYes,mbNO],0)=mrNo then exit;
               end;
          end;
          // controllo stessa ricerca
          xProgr:=QCompensiRifRicerca.Value;
          with DBGrid2.SelectedRows do begin
               for K:=0 to Count-1 do begin
                    QCompensi.BookMark:=Items[K];
                    if xProgr<>QCompensiRifRicerca.Value then
                         if MessageDlg('ATTENZIONE: sono stati selezionati compensi associati a ricerche/commesse diverse ! '+
                              'SEI SICURO DI VOLER PROCEDERE ?',mtWarning, [mbYes,mbNO],0)=mrNo then exit;
               end;
          end;
     end;
     if not QCompensi.IsEmpty then begin
          // dettaglio fattura
          case RGOpz.ItemIndex of
               0: begin
                         with Data do begin
                              DB.BeginTrans;
                              try
                                   xTotImponibile:=0;
                                   with DBGrid2.SelectedRows do begin
                                        for K:=0 to Count-1 do begin
                                             QCompensi.BookMark:=Items[K];
                                             xTotImponibile:=xTotImponibile+QCompensiImporto.Value;
                                             // aggiornamento Compensi
                                             Q.SQL.text:='update EBC_CompensiRicerche set IDFattura='+FatturaForm.TFatturaID.asString+' where ID='+QCompensiID.asString;
                                             Q.ExecSQL;
                                        end;
                                   end;
                                   Q1.Close;
                                   Q1.SQL.text:='insert into FattDett (IDFattura,Descrizione,Imponibile,PercentualeIVA,IDCompenso) '+
                                        'values (:xIDFattura,:xDescrizione,:xImponibile,:xPercentualeIVA,:xIDCompenso)';
                                   Q1.ParamByName('xIDFattura').asInteger:=FatturaForm.TFatturaID.Value;
                                   Q1.ParamByName('xDescrizione').asString:='Per attivit� di consulenza in ambito risorse umane - '+
                                        QCompensiTipo.Value+' (rif.ric.n� '+QCompensiRifRicerca.Value+')';
                                   Q1.ParamByName('xImponibile').asFloat:=xTotImponibile;
                                   Q1.ParamByName('xPercentualeIVA').asFloat:=20;
                                   Q1.ParamByName('xIDCompenso').asInteger:=QCompensiID.value;
                                   Q1.ExecSQL;
                                   RicalcolaTotFattura;
                                   DB.CommitTrans;
                                   QCompensi.Close;
                                   QCompensi.Open;
                              except
                                   DB.RollbackTrans;
                                   MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
                              end;
                         end;
                    end;
               1: begin
                         if QInsNonFatt.RecordCount=0 then begin
                              MessageDlg('Non ci sono soggetti inseriti cui far riferimento',mtError, [mbOK],0);
                              exit;
                         end;
                         xIDIns:=QInsNonFattID.Value;
                         if QInsNonFattSesso.Value='F' then xDic:='Sig.ra' else xDic:='Sig.';

                         with Data do begin
                              DB.BeginTrans;
                              try
                                   xTotImponibile:=0;
                                   with DBGrid2.SelectedRows do begin
                                        for K:=0 to Count-1 do begin
                                             QCompensi.BookMark:=Items[K];
                                             xTotImponibile:=xTotImponibile+QCompensiImporto.Value;
                                             // aggiornamento Compensi
                                             Q.SQL.text:='update EBC_CompensiRicerche set IDFattura='+FatturaForm.TFatturaID.asString+' where ID='+QCompensiID.asString;
                                             Q.ExecSQL;
                                        end;
                                   end;
                                   Q1.Close;
                                   Q1.SQL.text:='insert into FattDett (IDFattura,Descrizione,Imponibile,PercentualeIVA,IDCompenso) '+
                                        'values (:xIDFattura,:xDescrizione,:xImponibile,:xPercentualeIVA,:xIDCompenso)';
                                   Q1.ParamByName('xIDFattura').asInteger:=FatturaForm.TFatturaID.Value;
                                   Q1.ParamByName('xDescrizione').asString:='Per attivit� di consulenza in ambito risorse umane - '+
                                        QCompensiTipo.Value+' (rif.ric.n� '+QCompensiRifRicerca.Value+
                                        ') '+xDic+' '+QInsNonFattCognome.Value+' '+
                                        QInsNonFattNome.value+'. Ruolo: '+QInsNonFattRuolo.Value;
                                   Q1.ParamByName('xImponibile').asFloat:=xTotImponibile;
                                   Q1.ParamByName('xPercentualeIVA').asFloat:=20;
                                   Q1.ParamByName('xIDCompenso').asInteger:=QCompensiID.value;
                                   Q1.ExecSQL;
                                   // agg. inserimenti
                                   Q.SQL.text:='update EBC_Inserimenti set ProgFattura='+FatturaForm.TFatturaProgressivo.asString+' where ID='+IntToStr(xIDIns);
                                   Q.ExecSQL;

                                   RicalcolaTotFattura;

                                   DB.CommitTrans;
                                   QCompensi.Close;
                                   QCompensi.Open;
                                   FatturaForm.xUltimoInserito:=xIDIns;
                                   QInsNonFatt.Close;
                                   QInsNonFatt.Open;
                              except
                                   DB.RollbackTrans;
                                   MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
                              end;
                         end;
                    end;
               2: begin
                         // elenco selezionati
                         TAnagABS.Open;
                         xCand:='';
                         for i:=1 to ASG1.RowCount-1 do begin
                              ASG1.GetCheckBoxState(0,i,Vero);
                              if Vero then begin
                                   TAnagABS.FindKey([xArrayIDAnag[i]]);
                                   if TAnagABSSesso.Value='F' then xDic:='Sig.ra' else xDic:='Sig.';
                                   xCand:=xCand+xDic+' '+TAnagABSCognome.Value+' '+TAnagABSNome.Value+', ';
                              end;
                         end;
                         TAnagABS.Close;
                         if xCand='' then begin
                              MessageDlg('Non ci sono soggetti cui far riferimento',mtError, [mbOK],0);
                              exit;
                         end;

                         with Data do begin
                              DB.BeginTrans;
                              try
                                   xTotImponibile:=0;
                                   with DBGrid2.SelectedRows do begin
                                        for K:=0 to Count-1 do begin
                                             QCompensi.BookMark:=Items[K];
                                             xTotImponibile:=xTotImponibile+QCompensiImporto.Value;
                                             // aggiornamento Compensi
                                             Q.SQL.text:='update EBC_CompensiRicerche set IDFattura='+FatturaForm.TFatturaID.asString+' where ID='+QCompensiID.asString;
                                             Q.ExecSQL;
                                        end;
                                   end;
                                   Q1.Close;
                                   Q1.SQL.text:='insert into FattDett (IDFattura,Descrizione,Imponibile,PercentualeIVA,IDCompenso) '+
                                        'values (:xIDFattura,:xDescrizione,:xImponibile,:xPercentualeIVA,:xIDCompenso)';
                                   Q1.ParamByName('xIDFattura').asInteger:=FatturaForm.TFatturaID.Value;
                                   Q1.ParamByName('xDescrizione').asString:='Per attivit� di consulenza in ambito risorse umane - '+
                                        QCompensiTipo.Value+' (rif.ric.n� '+QCompensiRifRicerca.Value+
                                        ') '+xCand;
                                   Q1.ParamByName('xImponibile').asFloat:=xTotImponibile;
                                   Q1.ParamByName('xPercentualeIVA').asFloat:=20;
                                   Q1.ParamByName('xIDCompenso').asInteger:=QCompensiID.value;
                                   Q1.ExecSQL;

                                   RicalcolaTotFattura;

                                   DB.CommitTrans;
                                   QCompensi.Close;
                                   QCompensi.Open;
                              except
                                   DB.RollbackTrans;
                                   MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
                              end;
                         end;
                    end;
          end;
     end;
end;

procedure TInsDettFattForm.FormShow(Sender: TObject);
var i: integer;
begin
     //     for i:=1 to PCSoggetti.PageCount-1 do
     //         PCSoggetti.Pages[i].TabVisible:=False;
     TSSoggInseriti.TabVisible:=False;
     TSSoggCandRic.TabVisible:=False;
     Caption:='[M/1300] - '+caption;
end;

procedure TInsDettFattForm.RiempiASG1;
var i: integer;
begin
     QCandRicCliente.Open;
     ASG1.RowCount:=QCandRicCliente.RecordCount+1;
     i:=1;
     while not QCandRicCliente.EOF do begin
          ASG1.addcheckbox(0,i,false,false);
          ASG1.Cells[0,i]:=' '+QCandRicClienteCognome.Value+'  '+QCandRicClienteNome.Value+'  (Rif.ric.n� '+QCandRicClienteRifRicerca.Value+')';
          xArrayIDAnag[i]:=QCandRicClienteID.Value;
          QCandRicCliente.Next;
          inc(i);
     end;
     QCandRicCliente.Close;
end;

procedure TInsDettFattForm.BNotaSpeseInsClick(Sender: TObject);
begin
     if QNoteSpeseCli.RecordCount=0 then exit;
     with Data do begin
          DB.BeginTrans;
          try
               Q1.Close;
               Q1.SQL.text:='insert into FattDett (IDFattura,Descrizione,Imponibile,PercentualeIVA) '+
                    'values (:xIDFattura,:xDescrizione,:xImponibile,:xPercentualeIVA)';
               Q1.ParamByName('xIDFattura').asInteger:=FatturaForm.TFatturaID.Value;
               Q1.ParamByName('xDescrizione').asString:='Per spese sostenute - (rif.ric.n� '+QNoteSpeseCliRifRicerca.value+') - vedi allegato';
               Q1.ParamByName('xImponibile').asFloat:=QNoteSpeseCliTotSpese.Value;
               Q1.ParamByName('xPercentualeIVA').asFloat:=20;
               Q1.ExecSQL;
               // aggiornamento Nota spese
               Q.SQL.text:='update RicNoteSpese set IDFattura='+FatturaForm.TFatturaID.asString+' where ID='+QNoteSpeseCliID.asString;
               Q.ExecSQL;
               RicalcolaTotFattura;
               DB.CommitTrans;
               QNoteSpeseCli.Close;
               QNoteSpeseCli.Open;
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
          end;
     end;

end;

procedure TInsDettFattForm.QNoteSpeseCliCalcFields(DataSet: TDataSet);
begin
     QNoteSpeseCliTotSpese.Value:=QNoteSpeseCliAutomobile.Value+
          QNoteSpeseCliAutostrada.Value+
          QNoteSpeseCliAereotreno.Value+
          QNoteSpeseCliTaxi.Value+
          QNoteSpeseCliAlbergo.Value+
          QNoteSpeseCliRistoranteBar.Value+
          QNoteSpeseCliParking.Value+
          QNoteSpeseCliAltroImporto.Value;
end;

procedure TInsDettFattForm.BInsAnnuncioClick(Sender: TObject);
begin
     if QAnnunciCli.RecordCount=0 then exit;
     with Data do begin
          DB.BeginTrans;
          try
               Q1.Close;
               Q1.SQL.text:='insert into FattDett (IDFattura,Descrizione,Imponibile,PercentualeIVA,IDAnnuncio) '+
                    'values (:xIDFattura,:xDescrizione,:xImponibile,:xPercentualeIVA,:xIDAnnuncio)';
               Q1.ParamByName('xIDFattura').asInteger:=FatturaForm.TFatturaID.Value;
               Q1.ParamByName('xDescrizione').asString:='Per pubblicazione annuncio - (rif.annuncio n� '+QAnnunciCliRif.value+' - rif.ric.n� '+QAnnunciCliRifRicerca.value+')';
               Q1.ParamByName('xImponibile').asFloat:=QAnnunciCliTotaleEuro.Value;
               Q1.ParamByName('xPercentualeIVA').asFloat:=20;
               Q1.ParamByName('xIDAnnuncio').asInteger:=QAnnunciCliID.Value;
               Q1.ExecSQL;
               // aggiornamento Annuncio
               Q.SQL.text:='update Ann_annunci set IDFattura='+FatturaForm.TFatturaID.asString+' where ID='+QAnnunciCliID.asString;
               Q.ExecSQL;
               RicalcolaTotFattura;
               DB.CommitTrans;
               QAnnunciCli.Close;
               QAnnunciCli.Open;
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
          end;
     end;
end;

procedure TInsDettFattForm.RicalcolaTotFattura;
var tImporto,tTotal: double;
     xIDFattura: integer;
begin
     with FatturaForm do begin
          // ricalcolo imponibile fattura
          TFattDett.DisableControls;
          TFattDett.Close;
          TFattDett.Open;
          tImporto:=0;
          tTotal:=0;
          while not TFattDett.EOF do begin
               tImporto:=tImporto+TFattDettImponibile.Value;
               tTotal:=tTotal+TFattDettTotale.Value;
               TFattDett.Next;
          end;
          Qtemp.SQL.text:='update Fatture set Importo=:xImporto,Totale=:xTotale '+
               'where ID='+TFatturaID.asString;
          Qtemp.ParamByName('xImporto').asFloat:=tImporto;
          Qtemp.ParamByName('xTotale').asFloat:=tTotal;
          Qtemp.ExecSQL;
          TFattDett.EnableControls;
          // refresh fattura e dettaglio
          TFattDett.Close;
          xIDfattura:=TFatturaID.Value;
          TFattura.Close;
          TFattura.ParamByName('xID').asInteger:=xIDfattura;
          TFattura.open;
          TFattDett.Open;
     end;
end;

procedure TInsDettFattForm.BInsAnnuncio2Click(Sender: TObject);
begin
     if QAnnunciCli2.RecordCount=0 then exit;
     with Data do begin
          DB.BeginTrans;
          try
               Q1.Close;
               Q1.SQL.text:='insert into FattDett (IDFattura,Descrizione,Imponibile,PercentualeIVA,IDAnnuncio) '+
                    'values (:xIDFattura,:xDescrizione,:xImponibile,:xPercentualeIVA,:xIDAnnuncio)';
               Q1.ParamByName('xIDFattura').asInteger:=FatturaForm.TFatturaID.Value;
               Q1.ParamByName('xDescrizione').asString:='Per pubblicazione annuncio - (rif.annuncio n� '+QAnnunciCli2Rif.value+')';
               Q1.ParamByName('xImponibile').asFloat:=QAnnunciCli2TotaleEuro.Value;
               Q1.ParamByName('xPercentualeIVA').asFloat:=20;
               Q1.ParamByName('xIDAnnuncio').asInteger:=QAnnunciCliID.Value;
               Q1.ExecSQL;
               // aggiornamento Annuncio
               Q.SQL.text:='update Ann_annunci set IDFattura='+FatturaForm.TFatturaID.asString+' where ID='+QAnnunciCli2ID.asString;
               Q.ExecSQL;
               RicalcolaTotFattura;
               DB.CommitTrans;
               QAnnunciCli2.Close;
               QAnnunciCli2.Open;
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
          end;
     end;
end;

end.

