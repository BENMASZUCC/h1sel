unit StoriaContatti;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     StdCtrls,Buttons,Grids,DBGrids,Db,DBTables;

type
     TStoriaContattiForm=class(TForm)
          DBGrid1: TDBGrid;
          BitBtn1: TBitBtn;
          BitBtn2: TBitBtn;
          QStoriaCont: TQuery;
          dsQStoriaCont: TDataSource;
          QStoriaContID: TAutoIncField;
          QStoriaContIDRicerca: TIntegerField;
          QStoriaContIDAnagrafica: TIntegerField;
          QStoriaContTipoContatto: TStringField;
          QStoriaContData: TDateTimeField;
          QStoriaContOra: TDateTimeField;
          QStoriaContEsito: TStringField;
          QStoriaContEvaso: TBooleanField;
          QStoriaContNote: TStringField;
          QStoriaContIDMotivoNonAcc: TIntegerField;
          QStoriaContMotivo: TStringField;
    QStoriaContIDUtente: TIntegerField;
    QStoriaContUtente: TStringField;
          procedure BitBtn1Click(Sender: TObject);
          procedure BitBtn2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     StoriaContattiForm: TStoriaContattiForm;

implementation

uses MDRicerche,ModuloDati;

{$R *.DFM}

procedure TStoriaContattiForm.BitBtn1Click(Sender: TObject);
begin
     close
end;

procedure TStoriaContattiForm.BitBtn2Click(Sender: TObject);
begin
     if QStoriaCont.IsEmpty then exit;
     if MessageDlg('Sei sicuro di voler eliminare la riga ?',mtWarning, [mbNo,mbYes],0)=mrNo then exit;
     with Data do begin
          DB.BeginTrans;
          try
               Q1.Close;
               Q1.SQL.text:='delete from EBC_ContattiCandidati '+
                    'where ID='+QStoriaContID.AsString;
               Q1.ExecSQL;
               DB.CommitTrans;
               QStoriaCont.Close;
               QStoriaCont.Open;
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
          end;
     end;

end;

procedure TStoriaContattiForm.FormShow(Sender: TObject);
begin
     Caption:='[M/216] - '+Caption;
end;

end.

