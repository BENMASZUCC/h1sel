object DateInserimentoForm: TDateInserimentoForm
  Left = 238
  Top = 119
  ActiveControl = DEDataInizio
  BorderStyle = bsDialog
  Caption = 'Inserimento in azienda:  date di riferimento'
  ClientHeight = 75
  ClientWidth = 414
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 5
    Top = 6
    Width = 179
    Height = 13
    Caption = 'Data (presunta) inizio reale in azienda:'
  end
  object Label2: TLabel
    Left = 5
    Top = 31
    Width = 183
    Height = 13
    Caption = 'Data di controllo del candidato inserito:'
  end
  object BitBtn1: TBitBtn
    Left = 321
    Top = 3
    Width = 90
    Height = 34
    TabOrder = 3
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 321
    Top = 38
    Width = 90
    Height = 34
    Caption = 'Annulla'
    TabOrder = 4
    Kind = bkCancel
  end
  object CBProm: TCheckBox
    Left = 5
    Top = 53
    Width = 257
    Height = 17
    Caption = 'messaggio in promemoria (alla data di controllo)'
    Checked = True
    State = cbChecked
    TabOrder = 2
  end
  object DEDataInizio: TDateEdit97
    Left = 197
    Top = 3
    Width = 118
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    ColorCalendar.ColorValid = clBlue
    DayNames.Monday = 'lu'
    DayNames.Tuesday = 'ma'
    DayNames.Wednesday = 'me'
    DayNames.Thursday = 'gi'
    DayNames.Friday = 've'
    DayNames.Saturday = 'sa'
    DayNames.Sunday = 'do'
    MonthNames.January = 'gennaio'
    MonthNames.February = 'febbraio'
    MonthNames.March = 'marzo'
    MonthNames.April = 'aprile'
    MonthNames.May = 'maggio'
    MonthNames.June = 'giugno'
    MonthNames.July = 'luglio'
    MonthNames.August = 'agosto'
    MonthNames.September = 'settembre'
    MonthNames.October = 'ottobre'
    MonthNames.November = 'novembre'
    MonthNames.December = 'dicembre'
    Options = [doButtonTabStop, doCanClear, doCanPopup, doIsMasked, doShowCancel, doShowToday]
  end
  object DEDataControllo: TDateEdit97
    Left = 197
    Top = 27
    Width = 118
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    ColorCalendar.ColorValid = clBlue
    DayNames.Monday = 'lu'
    DayNames.Tuesday = 'ma'
    DayNames.Wednesday = 'me'
    DayNames.Thursday = 'gi'
    DayNames.Friday = 've'
    DayNames.Saturday = 'sa'
    DayNames.Sunday = 'do'
    MonthNames.January = 'gennaio'
    MonthNames.February = 'febbraio'
    MonthNames.March = 'marzo'
    MonthNames.April = 'aprile'
    MonthNames.May = 'maggio'
    MonthNames.June = 'giugno'
    MonthNames.July = 'luglio'
    MonthNames.August = 'agosto'
    MonthNames.September = 'settembre'
    MonthNames.October = 'ottobre'
    MonthNames.November = 'novembre'
    MonthNames.December = 'dicembre'
    Options = [doButtonTabStop, doCanClear, doCanPopup, doIsMasked, doShowCancel, doShowToday]
  end
end
