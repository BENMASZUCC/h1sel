unit Annuncio;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, ComCtrls, checklst, DBCtrls, Mask, Grids, DB,
  DBGrids, TB97, DtEdit97, DtEdDB97;

type
  TAnnuncioForm = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet3: TTabSheet;
    Panel1: TPanel;
    Panel2: TPanel;
    Label4: TLabel;
    Label2: TLabel;
    TabSheet2: TTabSheet;
    BitBtn3: TBitBtn;
    BitBtn1: TBitBtn;
    Panel3: TPanel;
    Panel4: TPanel;
    Label1: TLabel;
    Label3: TLabel;
    BitBtn5: TBitBtn;
    BitBtn6: TBitBtn;
    Panel5: TPanel;
    Panel6: TPanel;
    Label5: TLabel;
    Label6: TLabel;
    BitBtn8: TBitBtn;
    BitBtn9: TBitBtn;
    GroupBox1: TGroupBox;
    Label8: TLabel;
    DBEdit1: TDBEdit;
    Label9: TLabel;
    Label10: TLabel;
    DBMemo1: TDBMemo;
    Label11: TLabel;
    DBEdit3: TDBEdit;
    Label12: TLabel;
    DBMemo2: TDBMemo;
    Label13: TLabel;
    DBEdit4: TDBEdit;
    Label14: TLabel;
    DBEdit5: TDBEdit;
    SpeedButton1: TSpeedButton;
    GroupBox2: TGroupBox;
    DBGrid1: TDBGrid;
    GroupBox3: TGroupBox;
    DBGrid2: TDBGrid;
    SpeedButton3: TSpeedButton;
    Label17: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    Image1: TImage;
    Image2: TImage;
    Image7: TImage;
    Image8: TImage;
    Image9: TImage;
    Image10: TImage;
    Image3: TImage;
    Image4: TImage;
    Image5: TImage;
    OpenDialog1: TOpenDialog;
    Panel92: TPanel;
    TbBPubbNew: TToolbarButton97;
    TbBPubbDel: TToolbarButton97;
    TbBPubbOK: TToolbarButton97;
    TbBPubbCan: TToolbarButton97;
    GroupBox4: TGroupBox;
    Label18: TLabel;
    CLBRicerche: TCheckListBox;
    ToolbarButton971: TToolbarButton97;
    Shape1: TShape;
    Label7: TLabel;
    Label15: TLabel;
    Shape2: TShape;
    DBCheckBox1: TDBCheckBox;
    DbDateEdit971: TDbDateEdit97;
    procedure FormShow(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn8Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure BitBtn9Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure ToolbarButton971Click(Sender: TObject);
    procedure TbBPubbDelClick(Sender: TObject);
    procedure TbBPubbNewClick(Sender: TObject);
    procedure TbBPubbOKClick(Sender: TObject);
    procedure TbBPubbCanClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BitBtn6Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    xRic:array[0..100] of Integer;
    xNumItems:integer;
  end;

var
  AnnuncioForm: TAnnuncioForm;

implementation

uses ModuloDati4, InfoEdizione, ModuloDati;

{$R *.DFM}


procedure TAnnuncioForm.FormShow(Sender: TObject);
begin
     AnnuncioForm.Top:=40;
     AnnuncioForm.Left:=45;

     TabSheet1.TabVisible:=False;
     TabSheet2.TabVisible:=False;
     TabSheet3.TabVisible:=False;
     PageControl1.ActivePage:=TabSheet1;

     DataAnnunci.TEdizioniTest.Close;
     DataAnnunci.TEdizioniTest.MasterSource:=nil;
     DataAnnunci.TEdizioniTest.MasterFields:='';
     DataAnnunci.TEdizioniTest.Open;
     DBGrid1.Columns[1].width:=64;
end;

procedure TAnnuncioForm.BitBtn1Click(Sender: TObject);
begin
     close
end;

procedure TAnnuncioForm.BitBtn3Click(Sender: TObject);
var i,xIDRic:integer;
    xAlmenoUno:boolean;
begin
     xAlmenoUno:=False;
     for i:=0 to xNumItems do begin
        if CLBRicerche.Checked[i] then begin
           DataAnnunci.TAnnunciRic.InsertRecord([DataAnnunci.TAnnunciID.Value,xRic[i]]);
           xIDRic:=xRic[i];
           xAlmenoUno:=True;
        end;
     end;
     if not xAlmenoUno then begin
        ShowMessage('l''annuncio � stato etichettato come civetta');
        DataAnnunci.TAnnunci.Edit;
        DataAnnunci.TAnnunciCivetta.Value:=True;
        DataAnnunci.TAnnunci.Post;
     end else begin
        Data.Q1.Close;
        Data.Q1.SQL.text:='select Progressivo from EBC_Ricerche with (updlock) where ID='+IntToStr(xIDRic);
        DataAnnunci.TAnnunci.Edit;
        DataAnnunci.TAnnunciRif.Value:=Data.Q1.fieldbyName('Progressivo').asString;
        DataAnnunci.TAnnunci.Post;
        Data.Q1.Close;
     end;
     PageControl1.ActivePage:=TabSheet2
end;

procedure TAnnuncioForm.BitBtn5Click(Sender: TObject);
begin
     if DataAnnunci.DsAnnunci.State=dsEdit then
        DataAnnunci.TAnnunci.Post;
     if (DataAnnunci.TAnnunciNumModuli.asString='') and
        (DataAnnunci.TAnnunciNumParole.asString='') then
        showMessage('Manca numero parole o numero moduli')
     else PageControl1.ActivePage:=TabSheet3;
end;

procedure TAnnuncioForm.BitBtn8Click(Sender: TObject);
begin
     PageControl1.ActivePage:=TabSheet2;
end;

procedure TAnnuncioForm.SpeedButton3Click(Sender: TObject);
begin
     InfoEdizioneForm:=TInfoEdizioneForm.create(self);
     InfoEdizioneForm.Caption:=DataAnnunci.TEdizioniTestTestata.asString+' - edizione '+
                               DataAnnunci.TEdizioniTestNomeEdizione.asString;
     InfoEdizioneForm.ShowModal;
     InfoEdizioneForm.Free;
end;

procedure TAnnuncioForm.BitBtn9Click(Sender: TObject);
begin
     if DataAnnunci.DsAnnEdizDataXann.State in [dsInsert,dsEdit] then
        DataAnnunci.QAnnEdizDataXann.Post;
     close;
end;

procedure TAnnuncioForm.SpeedButton1Click(Sender: TObject);
begin
     OpenDialog1.FileName:=DataAnnunci.TAnnunciFileTesto.asString;
     OpenDialog1.Execute;
     if not (DataAnnunci.DsAnnunci.State in [dsInsert,dsEdit]) then
        DataAnnunci.TAnnunci.Edit;
     DataAnnunci.TAnnunciFileTesto.asString:=OpenDialog1.FileName;
end;

procedure TAnnuncioForm.ToolbarButton971Click(Sender: TObject);
begin
     //DataAnnunci.TAnnEdizDataXann.Insert;
     //DataAnnunci.TAnnEdizDataXannIDEdizione.Value:=DataAnnunci.TEdizioniTestID.Value;
     //DataAnnunci.TAnnEdizDataXann.Post;;
end;

procedure TAnnuncioForm.TbBPubbDelClick(Sender: TObject);
begin
     //if MessageDlg('Sei sicuro di voler cancellare ?',mtWarning,
     //   [mbNo,mbYes],0)=mrYes then DataAnnunci.TAnnEdizDataXann.Delete;
end;

procedure TAnnuncioForm.TbBPubbNewClick(Sender: TObject);
begin
     //DataAnnunci.TAnnEdizDataXann.Insert;
end;

procedure TAnnuncioForm.TbBPubbOKClick(Sender: TObject);
begin
     //DataAnnunci.TAnnEdizDataXann.Post;
end;

procedure TAnnuncioForm.TbBPubbCanClick(Sender: TObject);
begin
     //DataAnnunci.TAnnEdizDataXann.Cancel;
end;

procedure TAnnuncioForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     DataAnnunci.TEdizioniTest.Close;
     DataAnnunci.TEdizioniTest.MasterSource:=DataAnnunci.DsTestate;
     DataAnnunci.TEdizioniTest.MasterFields:='ID';
     DataAnnunci.TEdizioniTest.Open;
end;

procedure TAnnuncioForm.BitBtn6Click(Sender: TObject);
begin
     close;
end;

end.
