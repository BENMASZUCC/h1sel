unit ConoscInfo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBTables, Db, ExtCtrls, dxTL, dxDBCtrl, dxDBGrid, dxCntner, dxDBTLCl,
  dxGrClms, TB97, StdCtrls, Buttons;

type
  TConoscInfoForm = class(TForm)
    Panel1: TPanel;
    QConoscInfo: TQuery;
    DsQConoscInfo: TDataSource;
    UpdConoscInfo: TUpdateSQL;
    dxDBGrid1: TdxDBGrid;
    dxDBGrid1ID: TdxDBGridMaskColumn;
    dxDBGrid1Proprieta: TdxDBGridMaskColumn;
    dxDBGrid1Procedura: TdxDBGridMaskColumn;
    dxDBGrid1Area: TdxDBGridPickColumn;
    dxDBGrid1SottoArea: TdxDBGridPickColumn;
    BVoceNew: TToolbarButton97;
    BVoceDel: TToolbarButton97;
    BVoceCan: TToolbarButton97;
    BVoceOK: TToolbarButton97;
    BitBtn1: TBitBtn;
    QConoscInfoID: TAutoIncField;
    QConoscInfoArea: TStringField;
    QConoscInfoSottoArea: TStringField;
    QConoscInfoProprieta: TStringField;
    QConoscInfoProcedura: TStringField;
    BitBtn2: TBitBtn;
    procedure DsQConoscInfoStateChange(Sender: TObject);
    procedure QConoscInfoAfterPost(DataSet: TDataSet);
    procedure BVoceNewClick(Sender: TObject);
    procedure BVoceDelClick(Sender: TObject);
    procedure BVoceOKClick(Sender: TObject);
    procedure BVoceCanClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ConoscInfoForm: TConoscInfoForm;

implementation

uses ModuloDati;

{$R *.DFM}

procedure TConoscInfoForm.DsQConoscInfoStateChange(Sender: TObject);
var b: boolean;
begin
     b:=DsQConoscInfo.State in [dsEdit,dsInsert];
     BVoceNew.Enabled:=not b;
     BVoceDel.Enabled:=not b;
     BVoceOK.Enabled:=b;
     BVoceCan.Enabled:=b;
end;

procedure TConoscInfoForm.QConoscInfoAfterPost(DataSet: TDataSet);
begin
     with QConoscInfo do begin
          Data.DB.BeginTrans;
          try
               ApplyUpdates;
               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               raise;
          end;
          CommitUpdates;
     end;
end;

procedure TConoscInfoForm.BVoceNewClick(Sender: TObject);
begin
     QConoscInfo.Insert;
end;

procedure TConoscInfoForm.BVoceDelClick(Sender: TObject);
begin
     // DA FARE:  controllo integritÓ referenziale
     QConoscInfo.Delete;
end;

procedure TConoscInfoForm.BVoceOKClick(Sender: TObject);
begin
     QConoscInfo.Post;
end;

procedure TConoscInfoForm.BVoceCanClick(Sender: TObject);
begin
     QConoscInfo.Cancel;
end;

procedure TConoscInfoForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     if DsQConoscInfo.State in [dsInsert,dsEdit] then QConoscInfo.Post;
end;

end.
