unit RiepTiming;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Grids, DBGrids, Db, DBTables, Mask, DBCtrls;

type
  TRiepTimingForm = class(TForm)
    QClassifica: TQuery;
    QClassificaProgressivo: TStringField;
    QClassificaDescrizione: TStringField;
    DsQClassifica: TDataSource;
    DBGrid1: TDBGrid;
    BitBtn1: TBitBtn;
    Label1: TLabel;
    QTotTempo: TQuery;
    DsQTotTempo: TDataSource;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    QClassificaTot: TIntegerField;
    QTotTempoTotale: TIntegerField;
    procedure DBGrid1TitleClick(Column: TColumn);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  RiepTimingForm: TRiepTimingForm;

implementation

{$R *.DFM}

procedure TRiepTimingForm.DBGrid1TitleClick(Column: TColumn);
begin
     QClassifica.Close;
     QClassifica.SQL.Text:='select Progressivo,Descrizione,sum(TotSec) Tot '+
                           'from TimingRic,EBC_Ricerche,EBC_Clienti '+
                           'where TimingRic.IDRicerca=EBC_Ricerche.ID '+
                           '	 and EBC_Ricerche.IDCliente=EBC_Clienti.ID '+
                           'and TimingRic.IDUtente=:xIDutente '+
                           'group by Progressivo,Descrizione ';
     if Column=DBGrid1.Columns[0] then
        QClassifica.SQL.Add('order by Descrizione');
     if Column=DBGrid1.Columns[1] then
        QClassifica.SQL.Add('order by Progressivo');
     if Column=DBGrid1.Columns[2] then
        QClassifica.SQL.Add('order by Tot desc');
     QClassifica.open;
end;

procedure TRiepTimingForm.FormShow(Sender: TObject);
begin
     Caption:='[M/056] - '+Caption;
end;

end.
