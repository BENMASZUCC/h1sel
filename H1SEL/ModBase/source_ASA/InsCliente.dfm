object InsClienteForm: TInsClienteForm
  Left = 363
  Top = 179
  ActiveControl = EDenominazione
  BorderStyle = bsDialog
  Caption = 'Nuovo Cliente - campi obbligatori'
  ClientHeight = 124
  ClientWidth = 405
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 4
    Top = 2
    Width = 76
    Height = 13
    Caption = 'Denominazione:'
  end
  object Label2: TLabel
    Left = 4
    Top = 40
    Width = 28
    Height = 13
    Caption = 'Stato:'
  end
  object SpeedButton1: TSpeedButton
    Left = 269
    Top = 53
    Width = 28
    Height = 25
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000120B0000120B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      33333333333333333333333333333333333333333333333333FF333333333333
      3000333333FFFFF3F77733333000003000B033333777773777F733330BFBFB00
      E00033337FFF3377F7773333000FBFB0E000333377733337F7773330FBFBFBF0
      E00033F7FFFF3337F7773000000FBFB0E000377777733337F7770BFBFBFBFBF0
      E00073FFFFFFFF37F777300000000FB0E000377777777337F7773333330BFB00
      000033333373FF77777733333330003333333333333777333333333333333333
      3333333333333333333333333333333333333333333333333333333333333333
      3333333333333333333333333333333333333333333333333333}
    NumGlyphs = 2
    OnClick = SpeedButton1Click
  end
  object Label3: TLabel
    Left = 104
    Top = 40
    Width = 69
    Height = 13
    Caption = 'Attivit�/settore'
  end
  object Label4: TLabel
    Left = 4
    Top = 82
    Width = 91
    Height = 13
    Caption = 'Conosciuto in data:'
  end
  object BitBtn1: TBitBtn
    Left = 307
    Top = 6
    Width = 93
    Height = 35
    TabOrder = 2
    OnClick = BitBtn1Click
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 307
    Top = 43
    Width = 93
    Height = 35
    Caption = 'Annulla'
    TabOrder = 3
    Kind = bkCancel
  end
  object EDenominazione: TEdit
    Left = 4
    Top = 17
    Width = 293
    Height = 21
    MaxLength = 50
    TabOrder = 0
  end
  object CBStato: TComboBox
    Left = 4
    Top = 55
    Width = 97
    Height = 21
    ItemHeight = 13
    TabOrder = 1
    Text = 'cliente'
    Items.Strings = (
      'cliente'
      'non attivo'
      'respect')
  end
  object EAttivita: TEdit
    Left = 104
    Top = 55
    Width = 162
    Height = 21
    ReadOnly = True
    TabOrder = 4
  end
  object DEDataCon: TDateEdit97
    Left = 4
    Top = 97
    Width = 120
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    ColorCalendar.ColorValid = clBlue
    DayNames.Monday = 'lu'
    DayNames.Tuesday = 'ma'
    DayNames.Wednesday = 'me'
    DayNames.Thursday = 'gi'
    DayNames.Friday = 've'
    DayNames.Saturday = 'sa'
    DayNames.Sunday = 'do'
    MonthNames.January = 'gennaio'
    MonthNames.February = 'febbraio'
    MonthNames.March = 'marzo'
    MonthNames.April = 'aprile'
    MonthNames.May = 'maggio'
    MonthNames.June = 'giugno'
    MonthNames.July = 'luglio'
    MonthNames.August = 'agosto'
    MonthNames.September = 'settembre'
    MonthNames.October = 'ottobre'
    MonthNames.November = 'novembre'
    MonthNames.December = 'dicembre'
    Options = [doButtonTabStop, doCanClear, doCanPopup, doIsMasked, doShowCancel, doShowToday]
  end
end
