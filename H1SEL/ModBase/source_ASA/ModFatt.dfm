object ModFattForm: TModFattForm
  Left = 237
  Top = 101
  ActiveControl = EDescrizione
  BorderStyle = bsDialog
  Caption = 'Modifica fattura'
  ClientHeight = 89
  ClientWidth = 553
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 5
    Top = 5
    Width = 55
    Height = 13
    Caption = 'Descrizione'
  end
  object Imponibile: TLabel
    Left = 5
    Top = 46
    Width = 47
    Height = 13
    Caption = 'Imponibile'
  end
  object Label2: TLabel
    Left = 128
    Top = 47
    Width = 42
    Height = 13
    Caption = 'Perc.IVA'
  end
  object BitBtn1: TBitBtn
    Left = 464
    Top = 3
    Width = 86
    Height = 35
    TabOrder = 3
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 464
    Top = 41
    Width = 86
    Height = 35
    Caption = 'Annulla'
    TabOrder = 4
    Kind = bkCancel
  end
  object EDescrizione: TEdit
    Left = 5
    Top = 21
    Width = 441
    Height = 21
    MaxLength = 200
    TabOrder = 0
  end
  object RxImponibile: TRxCalcEdit
    Left = 5
    Top = 62
    Width = 116
    Height = 21
    AutoSize = False
    DisplayFormat = '�'#39'.'#39' ,0.00'
    FormatOnEditing = True
    NumGlyphs = 2
    TabOrder = 1
  end
  object SEPercIVA: TSpinEdit
    Left = 128
    Top = 61
    Width = 49
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 2
    Value = 0
  end
end
