unit DettAnnuncio;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables, StdCtrls, DBCtrls, Mask, Grids, DBGrids, Buttons;

type
  TDettAnnuncioForm = class(TForm)
    QAnnuncio: TQuery;
    DsQAnnuncio: TDataSource;
    QAnnuncioID: TAutoIncField;
    QAnnuncioRif: TStringField;
    QAnnuncioData: TDateTimeField;
    QAnnuncioTesto: TMemoField;
    QAnnuncioCivetta: TBooleanField;
    QAnnuncioFileTesto: TStringField;
    QAnnuncioNumModuli: TSmallintField;
    QAnnuncioNumParole: TSmallintField;
    QAnnuncioTotaleLire: TFloatField;
    QAnnuncioTotaleEuro: TFloatField;
    QAnnuncioNote: TMemoField;
    QAnnuncioArchiviato: TBooleanField;
    QAnnuncioCVPervenuti: TSmallintField;
    QAnnuncioCVIdonei: TSmallintField;
    QAnnuncioCVChiamati: TSmallintField;
    QAnnuncioIDFattura: TIntegerField;
    QAnnuncioDataPubblicazione: TDateTimeField;
    QAnnuncioCostoLire: TFloatField;
    QAnnuncioNomeEdizione: TStringField;
    QAnnuncioTestata: TStringField;
    DBGrid1: TDBGrid;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBMemo1: TDBMemo;
    DBCheckBox1: TDBCheckBox;
    Label3: TLabel;
    DBEdit2: TDBEdit;
    Label4: TLabel;
    DBEdit3: TDBEdit;
    Label5: TLabel;
    DBEdit4: TDBEdit;
    Label6: TLabel;
    DBEdit5: TDBEdit;
    DBCheckBox2: TDBCheckBox;
    Label7: TLabel;
    BitBtn1: TBitBtn;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DettAnnuncioForm: TDettAnnuncioForm;

implementation

{$R *.DFM}

procedure TDettAnnuncioForm.FormShow(Sender: TObject);
begin
     Caption:='[S/19] - '+Caption;
end;

end.
