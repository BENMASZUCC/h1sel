object FrmAnagConoscInfo: TFrmAnagConoscInfo
  Left = 0
  Top = 0
  Width = 537
  Height = 416
  TabOrder = 0
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 537
    Height = 44
    Align = alTop
    TabOrder = 0
    object Label34: TLabel
      Left = 7
      Top = 5
      Width = 74
      Height = 32
      Caption = 'Applicativi conosciuti'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object BVoceNew: TToolbarButton97
      Left = 210
      Top = 4
      Width = 62
      Height = 37
      Caption = 'Nuova voce'
      Opaque = False
      WordWrap = True
      OnClick = BVoceNewClick
    end
    object BVoceDel: TToolbarButton97
      Left = 334
      Top = 4
      Width = 62
      Height = 37
      Caption = 'Elimina voce'
      Opaque = False
      WordWrap = True
      OnClick = BVoceDelClick
    end
    object BVoceMod: TToolbarButton97
      Left = 272
      Top = 4
      Width = 62
      Height = 37
      Caption = 'Modifica applicativo'
      Opaque = False
      WordWrap = True
      OnClick = BVoceModClick
    end
    object BVoceCan: TToolbarButton97
      Left = 458
      Top = 4
      Width = 62
      Height = 37
      Caption = 'Annulla modifica'
      Enabled = False
      Opaque = False
      WordWrap = True
      OnClick = BVoceCanClick
    end
    object BVoceOK: TToolbarButton97
      Left = 396
      Top = 4
      Width = 62
      Height = 37
      Caption = 'Conferma modifica'
      Enabled = False
      Opaque = False
      WordWrap = True
      OnClick = BVoceOKClick
    end
  end
  object DBCtrlGrid1: TDBCtrlGrid
    Left = 0
    Top = 44
    Width = 537
    Height = 372
    Align = alClient
    ColCount = 1
    DataSource = DsQAnagConoscInfo
    PanelHeight = 124
    PanelWidth = 521
    TabOrder = 1
    RowCount = 3
    SelectedColor = clInfoBk
    object Label1: TLabel
      Left = 162
      Top = 5
      Width = 22
      Height = 13
      Caption = 'Area'
      FocusControl = DBEdit1
    end
    object Label2: TLabel
      Left = 341
      Top = 5
      Width = 46
      Height = 13
      Caption = 'Sottoarea'
      FocusControl = DBEdit2
    end
    object Label3: TLabel
      Left = 4
      Top = 5
      Width = 96
      Height = 13
      Caption = 'Proprieta applicativo'
      FocusControl = DBEdit3
    end
    object Label4: TLabel
      Left = 4
      Top = 43
      Width = 105
      Height = 13
      Caption = 'Procedura/applicativo'
      FocusControl = DBEdit4
    end
    object Label5: TLabel
      Left = 218
      Top = 43
      Width = 100
      Height = 13
      Caption = 'Periodo utilizzo dal/al'
      FocusControl = DBEdit5
    end
    object Label7: TLabel
      Left = 323
      Top = 43
      Width = 30
      Height = 13
      Caption = 'Livello'
    end
    object Label8: TLabel
      Left = 407
      Top = 43
      Width = 53
      Height = 13
      Caption = 'Piattaforme'
    end
    object Label6: TLabel
      Left = 4
      Top = 82
      Width = 87
      Height = 13
      Caption = 'Descrizione / note'
      FocusControl = DBEdit7
    end
    object DBEdit1: TDBEdit
      Left = 162
      Top = 21
      Width = 177
      Height = 21
      Color = clInfoBk
      DataField = 'Area'
      DataSource = DsQAnagConoscInfo
      ReadOnly = True
      TabOrder = 1
    end
    object DBEdit2: TDBEdit
      Left = 341
      Top = 21
      Width = 173
      Height = 21
      Color = clInfoBk
      DataField = 'SottoArea'
      DataSource = DsQAnagConoscInfo
      ReadOnly = True
      TabOrder = 2
    end
    object DBEdit3: TDBEdit
      Left = 4
      Top = 21
      Width = 156
      Height = 21
      Color = clInfoBk
      DataField = 'Proprieta'
      DataSource = DsQAnagConoscInfo
      ReadOnly = True
      TabOrder = 0
    end
    object DBEdit4: TDBEdit
      Left = 4
      Top = 59
      Width = 220
      Height = 21
      Color = clInfoBk
      DataField = 'Procedura'
      DataSource = DsQAnagConoscInfo
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 3
    end
    object DBEdit5: TDBEdit
      Left = 226
      Top = 59
      Width = 46
      Height = 21
      DataField = 'PeriodoDal'
      DataSource = DsQAnagConoscInfo
      TabOrder = 4
    end
    object DBEdit6: TDBEdit
      Left = 273
      Top = 59
      Width = 47
      Height = 21
      DataField = 'PeriodoAl'
      DataSource = DsQAnagConoscInfo
      TabOrder = 5
    end
    object DBComboBox1: TDBComboBox
      Left = 322
      Top = 59
      Width = 83
      Height = 21
      DataField = 'Livello'
      DataSource = DsQAnagConoscInfo
      ItemHeight = 13
      Items.Strings = (
        'ottimo'
        'buono'
        'discreto'
        'sufficiente'
        'scarso')
      TabOrder = 6
    end
    object DBComboBox2: TDBComboBox
      Left = 407
      Top = 59
      Width = 107
      Height = 21
      DataField = 'Piattaforme'
      DataSource = DsQAnagConoscInfo
      ItemHeight = 13
      Items.Strings = (
        'AS/400'
        'PC Monoutenza'
        'Rete'
        'Unix')
      TabOrder = 7
    end
    object DBEdit7: TDBEdit
      Left = 4
      Top = 98
      Width = 400
      Height = 21
      DataField = 'Descrizione'
      DataSource = DsQAnagConoscInfo
      TabOrder = 8
    end
  end
  object QAnagConoscInfo: TQuery
    CachedUpdates = True
    AfterPost = QAnagConoscInfoAfterPost
    AfterDelete = QAnagConoscInfoAfterPost
    DatabaseName = 'EBCDB'
    DataSource = Data.DsAnagrafica
    SQL.Strings = (
      'select AnagConoscInfo.*,ConoscenzeInfo.*'
      'from AnagConoscInfo,ConoscenzeInfo'
      'where AnagConoscInfo.IDConoscInfo=ConoscenzeInfo.ID'
      '  and IDAnagrafica=:ID')
    UpdateObject = UpdConoscInfo
    Left = 48
    Top = 240
    ParamData = <
      item
        DataType = ftAutoInc
        Name = 'ID'
        ParamType = ptUnknown
      end>
    object QAnagConoscInfoID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.AnagConoscInfo.ID'
    end
    object QAnagConoscInfoIDAnagrafica: TIntegerField
      FieldName = 'IDAnagrafica'
      Origin = 'EBCDB.AnagConoscInfo.IDAnagrafica'
    end
    object QAnagConoscInfoIDConoscInfo: TIntegerField
      FieldName = 'IDConoscInfo'
      Origin = 'EBCDB.AnagConoscInfo.IDConoscInfo'
    end
    object QAnagConoscInfoPeriodoDal: TSmallintField
      FieldName = 'PeriodoDal'
      Origin = 'EBCDB.AnagConoscInfo.PeriodoDal'
    end
    object QAnagConoscInfoPeriodoAl: TSmallintField
      FieldName = 'PeriodoAl'
      Origin = 'EBCDB.AnagConoscInfo.PeriodoAl'
    end
    object QAnagConoscInfoLivello: TStringField
      FieldName = 'Livello'
      Origin = 'EBCDB.AnagConoscInfo.Livello'
      FixedChar = True
      Size = 15
    end
    object QAnagConoscInfoPiattaforme: TStringField
      FieldName = 'Piattaforme'
      Origin = 'EBCDB.AnagConoscInfo.Piattaforme'
      FixedChar = True
      Size = 30
    end
    object QAnagConoscInfoArea: TStringField
      FieldName = 'Area'
      Origin = 'EBCDB.ConoscenzeInfo.Area'
      FixedChar = True
      Size = 30
    end
    object QAnagConoscInfoSottoArea: TStringField
      FieldName = 'SottoArea'
      Origin = 'EBCDB.ConoscenzeInfo.SottoArea'
      FixedChar = True
      Size = 30
    end
    object QAnagConoscInfoProprieta: TStringField
      FieldName = 'Proprieta'
      Origin = 'EBCDB.ConoscenzeInfo.Proprieta'
      FixedChar = True
      Size = 30
    end
    object QAnagConoscInfoProcedura: TStringField
      FieldName = 'Procedura'
      Origin = 'EBCDB.ConoscenzeInfo.Procedura'
      FixedChar = True
      Size = 50
    end
    object QAnagConoscInfoDescrizione: TStringField
      FieldName = 'Descrizione'
      Size = 50
    end
  end
  object DsQAnagConoscInfo: TDataSource
    DataSet = QAnagConoscInfo
    OnStateChange = DsQAnagConoscInfoStateChange
    Left = 48
    Top = 280
  end
  object UpdConoscInfo: TUpdateSQL
    ModifySQL.Strings = (
      'update AnagConoscInfo'
      'set'
      '  IDAnagrafica = :IDAnagrafica,'
      '  IDConoscInfo = :IDConoscInfo,'
      '  PeriodoDal = :PeriodoDal,'
      '  PeriodoAl = :PeriodoAl,'
      '  Livello = :Livello,'
      '  Piattaforme = :Piattaforme,'
      '  Descrizione = :Descrizione'
      'where'
      '  ID = :OLD_ID')
    InsertSQL.Strings = (
      'insert into AnagConoscInfo'
      
        '  (IDAnagrafica, IDConoscInfo, PeriodoDal, PeriodoAl, Livello, P' +
        'iattaforme, '
      '   Descrizione)'
      'values'
      
        '  (:IDAnagrafica, :IDConoscInfo, :PeriodoDal, :PeriodoAl, :Livel' +
        'lo, :Piattaforme, '
      '   :Descrizione)')
    DeleteSQL.Strings = (
      'delete from AnagConoscInfo'
      'where'
      '  ID = :OLD_ID')
    Left = 88
    Top = 256
  end
end
