unit InsRicerca;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, DBCtrls, Mask, Buttons, Spin, DtEdit97, Grids, DBGrids, Db,
  DBTables;

type
  TInsRicercaForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    DEData: TDateEdit97;
    Label1: TLabel;
    SENumRic: TSpinEdit;
    Label2: TLabel;
    GroupBox3: TGroupBox;
    SpeedButton1: TSpeedButton;
    GroupBox4: TGroupBox;
    GBCliente: TGroupBox;
    BTrovaCliente: TSpeedButton;
    ECliente: TEdit;
    ERuolo: TEdit;
    EArea: TEdit;
    TUsers: TTable;
    DsUsers: TDataSource;
    TUsersID: TAutoIncField;
    TUsersNominativo: TStringField;
    DBGrid1: TDBGrid;
    Label10: TLabel;
    CBTipo: TComboBox;
    Q: TQuery;
    GroupBox1: TGroupBox;
    DBGrid2: TDBGrid;
    QSedi: TQuery;
    QSediID: TAutoIncField;
    QSediDescrizione: TStringField;
    DsQSedi: TDataSource;
    Label3: TLabel;
    ERifInterno: TEdit;
    SpeedButton2: TSpeedButton;
    Label4: TLabel;
    DEDataPrevChius: TDateEdit97;
    procedure BTrovaClienteClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    xIDCliente,xIDRuolo,xIDArea,xIDRifInterno:integer;
  end;

var
  InsRicercaForm: TInsRicercaForm;

implementation

uses SelCliente, InsRuolo, SelContatti;

{$R *.DFM}

procedure TInsRicercaForm.BTrovaClienteClick(Sender: TObject);
begin
     SelClienteForm:=TSelClienteForm.create(self);
     SelClienteForm.ShowModal;
     if SelClienteForm.ModalResult=mrOK then begin
        xIDCliente:=SelClienteForm.TClientiID.Value;
        ECliente.Text:=SelClienteForm.TClientiDescrizione.Value;
     end;
     SelClienteForm.Free;
end;

procedure TInsRicercaForm.SpeedButton1Click(Sender: TObject);
begin
      InsRuoloForm:=TInsRuoloForm.create(self);
      InsRuoloForm.Height:=425;
      InsRuoloForm.ShowModal;
      if InsRuoloForm.ModalResult=mrOK then begin
         xIDRuolo:=InsRuoloForm.TRuoliID.Value;
         xIDArea:=InsRuoloForm.TAreeID.Value;
         ERuolo.Text:=InsRuoloForm.TRuoliDescrizione.AsString;
         EArea.text:=InsRuoloForm.TAreeDescrizione.Value;
      end;
      InsRuoloForm.Free;
end;

procedure TInsRicercaForm.BitBtn1Click(Sender: TObject);
begin
     if xIDCliente=0 then begin
        ModalResult:=mrNone;
        MessageDlg('Cliente mancante',mtError,[mbOK],0);
        Abort;
     end;
end;

procedure TInsRicercaForm.FormCreate(Sender: TObject);
begin
     xIDRuolo:=0;
     xIDRifInterno:=0;
     xIDCliente:=0;
     // riempimento combo tipo
     Q.SQL.text:='select * from EBC_TipiCommesse';
     Q.Open;
     CBTipo.Items.Clear;
     while not Q.EOF do begin
           CBTipo.Items.Add(Q.FieldByName('TipoCommessa').asString);
         Q.next;
     end;
     Q.Close;
end;

procedure TInsRicercaForm.SpeedButton2Click(Sender: TObject);
begin
     if xIDCliente=0 then begin
        Showmessage('selezionare prima un cliente');
        exit;
     end;
     SelContattiForm:=TSelContattiForm.create(self);
     SelContattiForm.QContatti.ParamByName('xIDCliente').asInteger:=xIDCliente;
     SelContattiForm.QContatti.Open;
     SelContattiForm.ShowModal;
     if SelContattiForm.ModalResult=mrOK then begin
        xIDRifInterno:=SelContattiForm.QContatti.FieldByName('ID').asInteger;
        ERifInterno.Text:=SelContattiForm.QContatti.FieldByName('Contatto').asString;
     end;
     SelContattiForm.Free;
end;

procedure TInsRicercaForm.FormShow(Sender: TObject);
begin
     Caption:='[M/20] - '+caption;
end;

end.
