unit Login;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     StdCtrls,Grids,DBGrids,Buttons,Mask,Db,DBTables,ImgList,
     ImageWindow,ExtCtrls,Nb;

type
     TLoginForm=class(TForm)
          BitBtn1: TBitBtn;
          Q1: TQuery;
          Panel1: TPanel;
          Image1: TImage;
          QUserObj: TQuery;
          QUserObjCodOggetto: TStringField;
          QDubbi: TQuery;
          QDubbiTotDubbi: TIntegerField;
          QUsers: TQuery;
          QUsersID: TAutoIncField;
          QUsersNominativo: TStringField;
          QUsersDescrizione: TStringField;
          QUsersPassword: TStringField;
          QUsersDataCreazione: TDateTimeField;
          QUsersDataUltimoAccesso: TDateTimeField;
          QUsersDataScadenza: TDateTimeField;
          QUsersDataRevoca: TDateTimeField;
          QUsersTipo: TSmallintField;
          QUsersLana0: TStringField;
          BitBtn2: TBitBtn;
          Panel2: TPanel;
          Label20: TLabel;
          Label1: TLabel;
          MaskEdit1: TMaskEdit;
          Username: TEdit;
          procedure BitBtn1Click(Sender: TObject);
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
          procedure FormShow(Sender: TObject);
          procedure FormCreate(Sender: TObject);
          procedure BitBtn2Click(Sender: TObject);
     private
          xMACaddress: string;
          xEsci: boolean;
     public
          Accedi: string;
          { Public declarations }
     end;

var
     LoginForm: TLoginForm;

implementation

uses Main,ModuloDati,MessIniziale,Splash3,CheckPass, uUtilsVarie;

{$R *.DFM}


procedure TLoginForm.BitBtn1Click(Sender: TObject);
var xAnno,xMese,xGiorno: Word;
     xTot,xCompVicini: integer;
     xData: TDateTime;
     i,xggDiffCol: integer;
begin
     QUsers.Close;
     QUsers.SQL.Text:='select * from Users where nominativo=:x';
     QUsers.ParamByName('x').asString:=Username.text;
     QUsers.Open;
     if not xEsci then begin
          if not QUsers.isempty then begin
               if CheckPassword(MaskEdit1.Text,QUsers.Fieldbyname('Password').asString) then begin

                    // controllo scadenza e revoca
                    if ((QUsersDataScadenza.asString<>'')and(QUsersDataScadenza.Value<Date))or
                         ((QUsersDataRevoca.asString<>'')and(QUsersDataRevoca.Value<Date)) then begin
                         MessageDlg('ERRORE: L''utente � scaduto o � stato revocato !',mtError, [mbOK],0);
                         Exit;
                    end;
                    // controllo inutilizzo (se c'� la data di ultimo utilizzo)
                    if (QUsersDataUltimoAccesso.AsString<>'')and(QUsersDataUltimoAccesso.Value<Date-180) then begin
                         MessageDlg('ERRORE: L''utente � inutilizzato da pi� di sei mesi (180 giorni solari).'+
                              chr(13)+'A norma del DPR 318/99 verr� automaticamente revocato !',mtError, [mbOK],0);
                         Q1.Close;
                         Q1.SQL.Text:='update Users set DataRevoca=:xData where ID='+QUsersID.asString;
                         Q1.ParamByName('xData').asDateTime:=Date;
                         Q1.ExecSQL;
                         Exit;
                    end;
                    // controllo inutilizzo (se non � mai stato utilizzato ==> sulla data di creazione)
                    if (QUsersDataUltimoAccesso.AsString='')and(QUsersDataCreazione.Value<Date-180) then begin
                         MessageDlg('ERRORE: L''utente � stato creato pi� di sei mesi (180 giorni solari) fa'+
                              chr(13)+'ma non risulta aver mai utilizzato il programma.'+
                              chr(13)+'A norma del DPR 318/99 verr� automaticamente revocato !',mtError, [mbOK],0);
                         Q1.Close;
                         Q1.SQL.Text:='update Users set DataRevoca=:xData where ID='+QUsersID.asString;
                         Q1.ParamByName('xData').asDateTime:=Date;
                         Q1.ExecSQL;
                         Exit;
                    end;

                    // controllo utente gi� collegato con altro MAC ADDRESS
                    // NEW ver. 1.8.8 --> il controllo viene fatto soltanto se l'ottavo carattere di checkboxindici � 1
                    if (copy(Data.GlobalCheckBoxIndici.Value,8,1)='1')or(Data.GlobalCheckBoxIndici.Value='') then begin
                         if (QUsersLana0.Value<>'')and(QUsersLana0.Value<>xMACaddress) then begin
                              MessageDlg('ERRORE: L''utente risulta attualmente connesso da un altro PC (Lana0='+QUsersLana0.Value+')'+
                                   chr(13)+'E'' CONSENTITO ACCEDERE SOLTANTO DA QUELLA MACCHINA!',mtError, [mbOK],0);
                              Exit;
                         end;
                    end;

                    // modifica data ultimo accesso e MAC Address
                    Q1.Close;
                    Q1.SQL.Text:='update Users set DataUltimoAccesso=:xData, Lana0=:xLana0 where ID='+QUsersID.asString;
                    Q1.ParamByName('xData').asDateTime:=Date;
                    Q1.ParamByName('xLana0').asString:=xMACaddress;
                    Q1.ExecSQL;

                    Accedi:='S';
                    // aggiornamento Last Login
                    Q1.Close;
                    Q1.SQL.Text:='update Global set LastLogin=:xLastLogin,LastLoginID=:xLastLoginID';
                    Q1.ParamByName('xLastLogin').asString:=QUsersNominativo.Value;
                    Q1.ParamByName('xLastLoginID').asInteger:=QUsersID.Value;
                    Q1.ExecSQL;

                    // nominativo last login in locale
                    ScriviRegistry('LastLoginUsername',QUsersNominativo.Value);

                    // inserimento accesso nel log accessi
                    Q1.Close;
                    Q1.SQL.Text:='insert into Log_accessi (IDUtente,Data,OraEntrata,Modulo) '+
                         'values (:xIDUtente,:xData,:xOraEntrata,:xModulo)';
                    Q1.ParamByName('xIDUtente').asInteger:=QUsersID.Value;
                    Q1.ParamByName('xData').asDateTime:=Date;
                    Q1.ParamByName('xOraEntrata').asDateTime:=Now;
                    Q1.ParamByName('xModulo').asString:='MB';
                    Q1.ExecSQL;
                    Q1.Close;

                    Data.Global.Close;
                    Data.Global.Open;
                    MainForm.Statusbar1.Panels[0].text:='Utente attuale: '+Data.GlobalLastLogin.Value;
                    MainForm.Statusbar1.Panels[4].text:=Data.GlobalNomeAzienda.Value;
                    MainForm.xUtenteAttuale:=Data.GlobalLastLogin.Value;
                    MainForm.xIDUtenteAttuale:=QUsersID.Value;
                    MainForm.xUtenteAttualeTipo:=QUsersTipo.Value;
                    if QUsersTipo.Value>1 then
                         MainForm.DBEDubbiIns.ReadOnly:=True;

                    // cancellazione vecchi messaggi
                    Q1.SQL.Clear;
                    Q1.SQL.Add('delete from Promemoria where DataDaLeggere<=:xData');
                    Q1.Prepare;
                    Q1.Params[0].asDateTime:=Date-30;
                    Q1.ExecSQL;
                    // Filtri Promemorie
                    Data.TPromScaduti.Open;
                    Data.TPromScaduti.Filter:='(DataLettura=null) and (DataDaLeggere<'''+DateToStr(date)+''') and IDUtente='+Data.GlobalLastLoginID.asString;

                    // se ci sono candidati associati all'utente che hanno avuto un
                    // colloquio pi� di x giorni fa ==> messaggio in promemoria (se non c'� gi� !)
                    Data.Q1.Close;
                    Data.Q1.SQL.text:='select ggDiffCol from Users where ID='+Data.GlobalLastLoginID.asString;
                    Data.Q1.Open;
                    xggDiffCol:=Data.Q1.FieldByName('ggDiffCol').asInteger;
                    Data.Q1.Close;
                    Data.Q1.SQL.text:='select DATEDIFF(day, EBC_Colloqui.Data, getdate()) ggDataUltimoColloquio, '+
                         'Anagrafica.ID,CVNumero,Cognome,Nome '+
                         'from Anagrafica,EBC_Colloqui '+
                         'where Anagrafica.ID = EBC_Colloqui.IDAnagrafica '+
                         'and EBC_Colloqui.Data= '+
                         '    (select max(Data) from EBC_Colloqui '+
                         '     where Anagrafica.ID = EBC_Colloqui.IDAnagrafica) '+
                         'and IDUtente='+Data.GlobalLastLoginID.asString+' '+
                         'and DATEDIFF(day, EBC_Colloqui.Data, getdate())>'+IntToStr(xggDiffCol);
                    Data.Q1.Open;
                    while not Data.Q1.EOF do begin
                         // controllo esistenza messaggio
                         Q1.Close;
                         // NOTA: SOLO UTENTE E CANDIDATO, NO DATA INS., cos� non rimette lo stesso messaggio il giorno dopo e poi ancora...
                         //Q1.SQL.text:='select ID from Promemoria where DataIns=:xDataIns and IDUtente=:xIDUtente and IDCandUtente=:xIDCandUtente';
                         //Q1.ParamByName('xDataIns').asdateTime:=Date;
                         Q1.SQL.text:='select count(ID) Tot from Promemoria where IDUtente=:xIDUtente and IDCandUtente=:xIDCandUtente';
                         Q1.ParamByName('xIDUtente').asInteger:=Data.GlobalLastLoginID.Value;
                         Q1.ParamByName('xIDCandUtente').asInteger:=Data.Q1.FieldByName('ID').asInteger;
                         Q1.Open;
                         if Q1.FieldByName('Tot').asInteger=0 then begin
                              with Data do begin
                                   DB.BeginTrans;
                                   try
                                        QTemp.Close;
                                        QTemp.SQL.text:='insert into Promemoria (IDUtente,IDUtenteDa,DataIns,DataDaLeggere,Testo,Evaso,IDCandUtente) '+
                                             'values (:xIDUtente,:xIDUtenteDa,:xDataIns,:xDataDaLeggere,:xTesto,:xEvaso,:xIDCandUtente)';
                                        QTemp.ParamByName('xIDUtente').asInteger:=Data.GlobalLastLoginID.Value;
                                        QTemp.ParamByName('xIDUtenteDa').asInteger:=Data.GlobalLastLoginID.Value;
                                        QTemp.ParamByName('xDataIns').asDateTime:=Date;
                                        QTemp.ParamByName('xDataDaLeggere').asDateTime:=Date;
                                        QTemp.ParamByName('xTesto').asString:='l''ultimo colloquio a '+Data.Q1.FieldByName('Cognome').asString+' '+
                                             Data.Q1.FieldByName('Nome').asString+
                                             ' � stato fatto '+IntToStr(Data.Q1.FieldByName('ggDataUltimoColloquio').asInteger)+' fa';
                                        QTemp.ParamByName('xEvaso').asBoolean:=False;
                                        QTemp.ParamByName('xIDCandUtente').asInteger:=Data.Q1.FieldByName('ID').asInteger;
                                        QTemp.ExecSQL;
                                        DB.CommitTrans;
                                   except
                                        DB.RollbackTrans;
                                        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
                                   end;
                              end;
                         end;
                         Data.Q1.Next;
                    end;
                    Data.Q1.Close;

                    Data.QPromFuturi.Close;
                    Data.QPromFuturi.Prepare;
                    Data.QPromFuturi.Params[0].AsDateTime:=Date;
                    Data.QPromFuturi.Params[1].Value:=Data.GlobalLastLoginID.Value;
                    Data.QPromFuturi.Open;

                    Data.QPromOggi.Close;
                    Data.QPromOggi.Prepare;
                    Data.QPromOggi.Params[0].AsDateTime:=Date;
                    Data.QPromOggi.Params[1].Value:=Data.GlobalLastLoginID.Value;
                    Data.QPromOggi.Open;

                    MessInizialeForm:=TMessInizialeForm.create(self);
                    MessInizialeForm.LOggi.Caption:=IntToStr(Data.QPromOggi.RecordCount);
                    MessInizialeForm.LFuturi.Caption:=IntToStr(Data.QPromFuturi.RecordCount);
                    MessInizialeForm.LScaduti.Caption:=IntToStr(Data.TPromScaduti.RecordCount);

                    // dubbi inserimento
                    QDubbi.Open;
                    if QDubbiTotDubbi.value=1 then
                         MessInizialeForm.LDubbi.caption:='c''� un soggetto per il quale ci sono dubbi sull''attribuzione dei ruoli';
                    if QDubbiTotDubbi.value>1 then
                         MessInizialeForm.LDubbi.caption:='ci sono '+QDubbiTotDubbi.asString+' soggetti per i quali ci sono dubbi sull''attribuzione dei ruoli';
                    QDubbi.Close;

                    Hide;
                    MessInizialeForm.ShowModal;
                    MessInizialeForm.Free;

                    Data.TPromScaduti.Close;

                    Data.QPromOggi.Close;
                    Data.QPromOggi.Open;
                    case Data.QPromOggi.RecordCount of
                         0: MainForm.StatusBar1.Panels[2].Text:='nessun messaggio in scadenza oggi';
                         1: MainForm.StatusBar1.Panels[2].Text:='� presente un messaggio in scadenza oggi';
                    else MainForm.StatusBar1.Panels[2].Text:='Sono presenti '+IntToStr(Data.QPromOggi.RecordCount)+' messaggi in scadenza oggi';
                    end;
                    Data.QPromOggi.Close;
                    Data.QPromFuturi.Close;

                    // Obj rights
                    for i:=1 to 1000 do Mainform.xUserObj[i]:='';
                    QUserObj.Close;
                    QUserObj.Prepare;
                    QUserObj.Params[0].asInteger:=Data.GlobalLastLoginID.Value;
                    QUserObj.Open;
                    i:=1;
                    while not QUserObj.EOF do begin
                         Mainform.xUserObj[i]:=QUserObjCodOggetto.Value;
                         QUserObj.Next;
                         inc(i);
                    end;


                    close;
               end else
                    MessageDlg('ERRORE: La password non � corretta',mtError, [mbOK],0);
          end else begin
               MessageDlg('ERRORE: L''utente non esiste',mtError, [mbOK],0);
               Username.SetFocus;
          end;
     end else begin
          Accedi:='S';
          MainForm.xEsci:=true;
          Close;
     end;
end;

procedure TLoginForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     Data.QPromOggi.Close;
     Data.QPromFuturi.Close;
end;

procedure TLoginForm.FormShow(Sender: TObject);
var xValRegistry:string;
begin
     Caption:='[A/0] - Accesso al sistema';

     Username.Text:=LeggiRegistry('LastLoginUsername');
     {Q1.Close;
     Q1.SQL.Text:='select LastLogin from Global';
     Q1.Open;
     Username.text:=Q1.FieldByName('LastLogin').asString;
     Q1.Close;}
end;


{---------------------------------------------}
{ enumerate the lana's  - works only on WIN32 }
{---------------------------------------------}

function NbLanaEnum: TLana_Enum;
var
     NCB: TNCB;
     L_Enum: TLana_Enum;
     RetCode: Word;
begin
     {$IFDEF WIN32}
     FillChar(NCB,SizeOf(NCB),0);
     FillChar(L_Enum,SizeOf(TLana_Enum),0);
     NCB.Command:=NCB_ENUM;
     NCB.Buf:=@L_Enum;
     NCB.Length:=Sizeof(L_Enum);
     RetCode:=NetBiosCmd(NCB);
     if RetCode<>NRC_GOODRET then begin
          L_Enum.Length:=0;
          L_Enum.Lana[0]:=Byte(RetCode);
     end;
     {$ELSE}{ not supported for WIN16, fake LANA 0 }
     L_Enum.Length:=1;
     L_Enum.Lana[0]:=0;
     {$ENDIF}
     Result:=L_Enum;
end;

{----------------------------------------}
{ Reset the lana - don't for WIN16 !     }
{----------------------------------------}

function NbReset(l: Byte): Word;
var
     NCB: TNCB;
begin
     {$IFNDEF WIN32}{ will reset all your connections for WIN16 }
     Result:=NRC_GOODRET; { so just fake a reset for Win16            }
     {$ELSE}
     FillChar(NCB,SizeOf(NCB),0);
     NCB.Command:=NCB_RESET;
     NCB.Lana_Num:=l;
     Result:=NetBiosCmd(NCB);
     {$ENDIF}
end;
{----------------------------------------}
{ return the MAC address of an interface }
{ in the form of a string like :         }
{ 'xx:xx:xx:xx:xx:xx'                    }
{ using the definitions in nb.pas        }
{----------------------------------------}

function NbGetMacAddr(LanaNum: Integer): string;
var
     NCB: TNCB;
     AdpStat: TAdpStat;
     RetCode: Word;
begin
     FillChar(NCB,SizeOf(NCB),0);
     FillChar(AdpStat,SizeOf(AdpStat),0);
     NCB.Command:=NCB_ADPSTAT;
     NCB.Buf:=@AdpStat;
     NCB.Length:=Sizeof(AdpStat);
     FillChar(NCB.CallName,Sizeof(TNBName),$20);
     NCB.CallName[0]:=Byte('*');
     NCB.Lana_Num:=LanaNum;
     RetCode:=NetBiosCmd(NCB);
     if RetCode=NRC_GOODRET then begin
          Result:=Format('%2.2x:%2.2x:%2.2x:%2.2x:%2.2x:%2.2x',
               [AdpStat.ID[0],
               AdpStat.ID[1],
                    AdpStat.ID[2],
                    AdpStat.ID[3],
                    AdpStat.ID[4],
                    AdpStat.ID[5]
                    ]);
     end else begin
          Result:='??:??:??:??:??:??';
     end;
end;

procedure TLoginForm.FormCreate(Sender: TObject);
var
     L_Enum: TLana_Enum;
     RetCode: Word;
     i: Integer;
begin
     Accedi:='N';
     xEsci:=False;
     L_Enum:=NbLanaEnum; { enumerate lanas for WIN NT }
     if L_Enum.Length=0 then begin
          //Button1.Caption := Format('LanaEnum err=%2.2x', [L_Enum.Lana[0]]);
          exit;
     end;

     for i:=0 to(L_Enum.Length-1) do begin{ for every lana found       }

          RetCode:=NbReset(L_Enum.Lana[i]); { Reset lana for WIN NT      }
          if RetCode<>NRC_GOODRET then begin
               //Button1.Caption := Format('Reset Lana %d err=%2.2x',[i, RetCode]);
               exit;
          end;
          { Get MAC Address = SOLO PER LANA = 0}
          xMACaddress:=NbGetMacAddr(0);
     end;
end;

procedure TLoginForm.BitBtn2Click(Sender: TObject);
begin
     xEsci:=True;
     BitBtn1Click(self);
end;

end.

