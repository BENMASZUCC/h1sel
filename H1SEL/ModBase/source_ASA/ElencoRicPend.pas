unit ElencoRicPend;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Grids, DBGrids, StdCtrls, Buttons, DBTables, ExtCtrls, dxDBTLCl,
  dxGrClms, dxTL, dxDBCtrl, dxDBGrid, dxCntner;

type
  TElencoRicPendForm = class(TForm)
    Panel1: TPanel;
    QRicAttive: TQuery;
    DsQRicAttive: TDataSource;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    QRicAttiveProgressivo: TStringField;
    QRicAttiveCliente: TStringField;
    QRicAttiveDataInizio: TDateTimeField;
    QRicAttiveNumRicercati: TSmallintField;
    QRicAttiveRuolo: TStringField;
    QRicAttiveStato: TStringField;
    QRicAttiveStipendioLordo: TFloatField;
    QRicAttiveID: TIntegerField;
    QRicAttiveIDCliente: TIntegerField;
    dxDBGrid1: TdxDBGrid;
    dxDBGrid1Progressivo: TdxDBGridMaskColumn;
    dxDBGrid1Cliente: TdxDBGridMaskColumn;
    dxDBGrid1DataInizio: TdxDBGridDateColumn;
    dxDBGrid1NumRicercati: TdxDBGridMaskColumn;
    dxDBGrid1Ruolo: TdxDBGridMaskColumn;
    dxDBGrid1Stato: TdxDBGridMaskColumn;
    dxDBGrid1StipendioLordo: TdxDBGridMaskColumn;
    dxDBGrid1ID: TdxDBGridMaskColumn;
    dxDBGrid1IDCliente: TdxDBGridMaskColumn;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ElencoRicPendForm: TElencoRicPendForm;

implementation

{$R *.DFM}

procedure TElencoRicPendForm.FormShow(Sender: TObject);
begin
     Caption:='[S/9] - '+caption;
     dxDBGrid1.Filter.Add(dxDBGrid1Stato,'attiva','attiva');
end;

end.
