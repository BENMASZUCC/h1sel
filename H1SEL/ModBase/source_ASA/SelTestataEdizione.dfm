object SelTestataEdizioneForm: TSelTestataEdizioneForm
  Left = 380
  Top = 103
  ActiveControl = DBGrid1
  BorderStyle = bsDialog
  Caption = 'Selezione della testata e dell'#39'edizione'
  ClientHeight = 368
  ClientWidth = 404
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object BitBtn1: TBitBtn
    Left = 310
    Top = 2
    Width = 92
    Height = 35
    TabOrder = 0
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 310
    Top = 38
    Width = 92
    Height = 35
    Caption = 'Annulla'
    TabOrder = 1
    Kind = bkCancel
  end
  object DBGrid1: TDBGrid
    Left = 3
    Top = 3
    Width = 302
    Height = 358
    DataSource = DsQTestEdiz
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'Testata'
        Width = 133
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Edizione'
        Width = 133
        Visible = True
      end>
  end
  object QTestEdiz: TQuery
    Active = True
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select IDTestata,Ann_Edizioni.ID IDEdizione,'
      
        '          Ann_Testate.Denominazione Testata, NomeEdizione Edizio' +
        'ne'
      'from Ann_Edizioni,Ann_Testate'
      'where Ann_Edizioni.IDTestata=Ann_Testate.ID'
      'order by Ann_Testate.Denominazione,NomeEdizione')
    Left = 32
    Top = 56
    object QTestEdizIDTestata: TIntegerField
      FieldName = 'IDTestata'
      Origin = 'EBCDB.Ann_Edizioni.IDTestata'
    end
    object QTestEdizIDEdizione: TAutoIncField
      FieldName = 'IDEdizione'
      Origin = 'EBCDB.Ann_Edizioni.ID'
    end
    object QTestEdizTestata: TStringField
      FieldName = 'Testata'
      Origin = 'EBCDB.Ann_Testate.Denominazione'
      FixedChar = True
      Size = 50
    end
    object QTestEdizEdizione: TStringField
      FieldName = 'Edizione'
      Origin = 'EBCDB.Ann_Edizioni.NomeEdizione'
      FixedChar = True
      Size = 50
    end
  end
  object DsQTestEdiz: TDataSource
    DataSet = QTestEdiz
    Left = 32
    Top = 88
  end
end
