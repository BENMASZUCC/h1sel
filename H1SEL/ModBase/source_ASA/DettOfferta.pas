unit DettOfferta;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Mask, ToolEdit, CurrEdit, StdCtrls, Buttons;

type
  TDettOffertaForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    CBTipo: TComboBox;
    EDesc: TEdit;
    RxImporto: TRxCalcEdit;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DettOffertaForm: TDettOffertaForm;

implementation

{$R *.DFM}

procedure TDettOffertaForm.FormShow(Sender: TObject);
begin
     Caption:='[S/21] - '+Caption;
end;

end.
