unit FrmListinoAnnunci;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     ExtCtrls,Db,DBTables,Grids,DBGrids,StdCtrls,DBCtrls,RXDBCtrl,
     ComCtrls,TB97,Mask;

type
     TListinoAnnunciFrame=class(TFrame)
          Panel2: TPanel;
          Panel3: TPanel;
          Panel4: TPanel;
          Panel5: TPanel;
          Panel1: TPanel;
          QEdizioni: TQuery;
          DsQEdizioni: TDataSource;
          DBGrid1: TDBGrid;
          DBText1: TDBText;
          QEdizioniIDEdizione: TAutoIncField;
          QEdizioniTestata: TStringField;
          QEdizioniEdizione: TStringField;
          QEdizioniTestataEdizione: TStringField;
          QListinoEdiz: TQuery;
          DsQListinoEdiz: TDataSource;
          QListinoEdizID: TAutoIncField;
          QListinoEdizIDEdizione: TIntegerField;
          QListinoEdizDallaData: TDateTimeField;
          QListinoEdizPrezzoListinoLire: TFloatField;
          QListinoEdizPrezzoANoiLire: TFloatField;
          QListinoEdizPrezzoAlClienteLire: TFloatField;
          QListinoEdizPrezzoListinoEuro: TFloatField;
          QListinoEdizPrezzoANoiEuro: TFloatField;
          QListinoEdizPrezzoAlClienteEuro: TFloatField;
          QListinoEdizNote: TStringField;
          QListinoEdizQtaDa: TSmallintField;
          QListinoEdizQtaA: TSmallintField;
          QListinoEdizDiffLire: TFloatField;
          QListinoEdizDiffEuro: TFloatField;
          PCValuta: TPageControl;
          TSLire: TTabSheet;
          TSEuro: TTabSheet;
          RxDBGrid1: TRxDBGrid;
          RxDBGrid2: TRxDBGrid;
          ToolbarButton972: TToolbarButton97;
          ToolbarButton973: TToolbarButton97;
          ToolbarButton974: TToolbarButton97;
          Splitter1: TSplitter;
          QMinPrezziTestata: TQuery;
          DsQMinPrezziTestata: TDataSource;
          QEdizioniIDTestata: TIntegerField;
          QMinPrezziTestataminPrezzoListinoLire: TFloatField;
          QMinPrezziTestataminPrezzoANoiLire: TFloatField;
          QMinPrezziTestataminPrezzoAlClienteLire: TFloatField;
          QMinPrezziTot: TQuery;
          DsQMinPrezziTot: TDataSource;
          QMinPrezziTotminPrezzoListinoLire: TFloatField;
          QMinPrezziTotminPrezzoANoiLire: TFloatField;
          QMinPrezziTotminPrezzoAlClienteLire: TFloatField;
          QMinPrezziTestataminPrezzoListinoEuro: TFloatField;
          QMinPrezziTestataminPrezzoANoiLireEuro: TFloatField;
          QMinPrezziTestataminPrezzoAlClienteEuro: TFloatField;
          QMinPrezziTotminPrezzoListinoEuro: TFloatField;
          QMinPrezziTotminPrezzoANoiLireEuro: TFloatField;
          QMinPrezziTotminPrezzoAlClienteEuro: TFloatField;
          Panel7: TPanel;
          DBText2: TDBText;
          Label1: TLabel;
          Panel8: TPanel;
          DBEdit1: TDBEdit;
          DBEdit2: TDBEdit;
          DBEdit3: TDBEdit;
          DBEdit4: TDBEdit;
          DBEdit5: TDBEdit;
          DBEdit6: TDBEdit;
          Panel9: TPanel;
          DBText3: TDBText;
          Label2: TLabel;
          Panel10: TPanel;
          DBEdit7: TDBEdit;
          DBEdit8: TDBEdit;
          DBEdit9: TDBEdit;
          DBEdit10: TDBEdit;
          DBEdit11: TDBEdit;
          DBEdit12: TDBEdit;
          Label3: TLabel;
          Label4: TLabel;
          Label5: TLabel;
          Label6: TLabel;
          Label7: TLabel;
          Label8: TLabel;
          Panel6: TPanel;
          LCliente: TLabel;
          QListinoEdizID_1: TAutoIncField;
          QListinoEdizPrezzoQuestoClienteLire: TFloatField;
          QListinoEdizPrezzoQuestoClienteEuro: TFloatField;
          BModPrezzoQuestoCli: TToolbarButton97;
          QListinoEdizIDCliente_1: TIntegerField;
          procedure QEdizioniCalcFields(DataSet: TDataSet);
          procedure QListinoEdizCalcFields(DataSet: TDataSet);
          procedure ToolbarButton972Click(Sender: TObject);
          procedure ToolbarButton973Click(Sender: TObject);
          procedure QListinoEdizAfterOpen(DataSet: TDataSet);
          procedure QListinoEdizAfterScroll(DataSet: TDataSet);
          procedure RxDBGrid1GetCellProps(Sender: TObject; Field: TField;
               AFont: TFont; var Background: TColor);
          procedure RxDBGrid2GetCellProps(Sender: TObject; Field: TField;
               AFont: TFont; var Background: TColor);
          procedure ToolbarButton974Click(Sender: TObject);
          procedure BModPrezzoQuestoCliClick(Sender: TObject);
     private
          xQtadal,xQtaAl,xIDRow: integer;
     public
          xIDCliente: integer;
     end;

implementation

uses PrezzoListino,ModuloDati,ModuloDati4,ModPrezzo,Main;

{$R *.DFM}

procedure TListinoAnnunciFrame.QEdizioniCalcFields(DataSet: TDataSet);
begin
     QEdizioniTestataEdizione.Value:=QEdizioniTestata.Value+' - '+QEdizioniEdizione.Value;
end;

procedure TListinoAnnunciFrame.QListinoEdizCalcFields(DataSet: TDataSet);
begin
     QListinoEdizDiffLire.Value:=QListinoEdizPrezzoAlClienteLire.Value-QListinoEdizPrezzoANoiLire.Value;
     QListinoEdizDiffEuro.Value:=QListinoEdizPrezzoAlClienteEuro.Value-QListinoEdizPrezzoANoiEuro.Value;
end;

procedure TListinoAnnunciFrame.ToolbarButton972Click(Sender: TObject);
var xIDClienteListino: integer;
begin
     if not MainForm.CheckProfile('3500') then Exit;
     PrezzoListinoForm:=TPrezzoListinoForm.create(self);
     PrezzoListinoForm.ShowModal;
     if PrezzoListinoForm.ModalResult=mrOK then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text:='insert into Ann_EdizListino (IDEdizione,DallaData,PrezzoListinoLire,PrezzoANoiLire,PrezzoAlClienteLire,'+
                         '                             PrezzoListinoEuro,PrezzoANoiEuro,PrezzoAlClienteEuro,Note,QtaDa,QtaA,IDCliente) '+
                         'values (:xIDEdizione,:xDallaData,:xPrezzoListinoLire,:xPrezzoANoiLire,:xPrezzoAlClienteLire,'+
                         '        :xPrezzoListinoEuro,:xPrezzoANoiEuro,:xPrezzoAlClienteEuro,:xNote,:xQtaDa,:xQtaA,:xIDCliente)';
                    Q1.ParamByName('xIDEdizione').asInteger:=QEdizioniIDEdizione.Value;
                    Q1.ParamByName('xDallaData').asDateTime:=PrezzoListinoForm.DEDallaData.Date;
                    Q1.ParamByName('xPrezzoListinoLire').AsFloat:=PrezzoListinoForm.ListinoLire.Value;
                    Q1.ParamByName('xPrezzoANoiLire').AsFloat:=PrezzoListinoForm.AnoiLire.Value;
                    Q1.ParamByName('xPrezzoAlClienteLire').AsFloat:=PrezzoListinoForm.AlClienteLire.Value;
                    Q1.ParamByName('xPrezzoListinoEuro').AsFloat:=PrezzoListinoForm.ListinoEuro.Value;
                    Q1.ParamByName('xPrezzoANoiEuro').AsFloat:=PrezzoListinoForm.AnoiEuro.Value;
                    Q1.ParamByName('xPrezzoAlClienteEuro').AsFloat:=PrezzoListinoForm.AlClienteEuro.Value;
                    Q1.ParamByName('xNote').asString:=PrezzoListinoForm.ENote.text;
                    Q1.ParamByName('xQtaDa').asInteger:=Round(PrezzoListinoForm.RxSpinEdit1.value);
                    Q1.ParamByName('xQtaA').asInteger:=Round(PrezzoListinoForm.RxSpinEdit2.value);
                    Q1.ParamByName('xIDCliente').asInteger:=xIDClienteListino;
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    QListinoEdiz.Close;
                    QListinoEdiz.Open;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               end;
          end;
     end;
     PrezzoListinoForm.free;
end;

procedure TListinoAnnunciFrame.ToolbarButton973Click(Sender: TObject);
var xIDClienteListino: integer;
begin
     if not MainForm.CheckProfile('3501') then Exit;
     if QListinoEdiz.RecordCount=0 then exit;
     PrezzoListinoForm:=TPrezzoListinoForm.create(self);
     PrezzoListinoForm.DEDallaData.Date:=QListinoEdizDallaData.Value;
     PrezzoListinoForm.ListinoLire.Value:=QListinoEdizPrezzoListinoLire.Value;
     PrezzoListinoForm.AnoiLire.Value:=QListinoEdizPrezzoANoiLire.Value;
     PrezzoListinoForm.AlClienteLire.Value:=QListinoEdizPrezzoAlClienteLire.Value;
     PrezzoListinoForm.ListinoEuro.Value:=QListinoEdizPrezzoListinoEuro.value;
     PrezzoListinoForm.AnoiEuro.Value:=QListinoEdizPrezzoANoiEuro.Value;
     PrezzoListinoForm.AlClienteEuro.Value:=QListinoEdizPrezzoAlClienteEuro.Value;
     PrezzoListinoForm.ENote.text:=QListinoEdizNote.Value;
     PrezzoListinoForm.RxSpinEdit1.value:=QListinoEdizQtaDa.Value;
     PrezzoListinoForm.RxSpinEdit2.value:=QListinoEdizQtaA.Value;
     PrezzoListinoForm.ShowModal;
     if PrezzoListinoForm.ModalResult=mrOK then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text:='update Ann_EdizListino set DallaData=:xDallaData,PrezzoListinoLire=:xPrezzoListinoLire,PrezzoANoiLire=:xPrezzoANoiLire,'+
                         'PrezzoAlClienteLire=:xPrezzoAlClienteLire,PrezzoListinoEuro=:xPrezzoListinoEuro, '+
                         'PrezzoANoiEuro=:xPrezzoANoiEuro,PrezzoAlClienteEuro=:xPrezzoAlClienteEuro,Note=:xNote,QtaDa=:XQtaDa,QtaA=:xQtaA,IDCliente=:xIDCliente '+
                         'where ID='+QListinoEdizID.asString;
                    Q1.ParamByName('xDallaData').asDateTime:=PrezzoListinoForm.DEDallaData.Date;
                    Q1.ParamByName('xPrezzoListinoLire').AsFloat:=PrezzoListinoForm.ListinoLire.Value;
                    Q1.ParamByName('xPrezzoANoiLire').AsFloat:=PrezzoListinoForm.AnoiLire.Value;
                    Q1.ParamByName('xPrezzoAlClienteLire').AsFloat:=PrezzoListinoForm.AlClienteLire.Value;
                    Q1.ParamByName('xPrezzoListinoEuro').AsFloat:=PrezzoListinoForm.ListinoEuro.Value;
                    Q1.ParamByName('xPrezzoANoiEuro').AsFloat:=PrezzoListinoForm.AnoiEuro.Value;
                    Q1.ParamByName('xPrezzoAlClienteEuro').AsFloat:=PrezzoListinoForm.AlClienteEuro.Value;
                    Q1.ParamByName('xNote').asString:=PrezzoListinoForm.ENote.text;
                    Q1.ParamByName('xQtaDa').asInteger:=Round(PrezzoListinoForm.RxSpinEdit1.value);
                    Q1.ParamByName('xQtaA').asInteger:=Round(PrezzoListinoForm.RxSpinEdit2.value);
                    Q1.ParamByName('xIDCliente').asInteger:=xIDClienteListino;
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    QListinoEdiz.Close;
                    QListinoEdiz.Open;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               end;
          end;
     end;
     PrezzoListinoForm.free;
end;

procedure TListinoAnnunciFrame.QListinoEdizAfterOpen(DataSet: TDataSet);
begin
     xQtadal:=-1;
     xQtaAl:=-1;
end;

procedure TListinoAnnunciFrame.QListinoEdizAfterScroll(DataSet: TDataSet);
begin
     xQtadal:=-1;
     xQtaAl:=-1;
end;

procedure TListinoAnnunciFrame.RxDBGrid1GetCellProps(Sender: TObject;
     Field: TField; AFont: TFont; var Background: TColor);
begin
     if (QListinoEdizQtaDa.Value<>xQtadal)and(QListinoEdizQtaA.Value<>xQtaAl) then begin
          Background:=clYellow;
          xQtadal:=QListinoEdizQtaDa.Value;
          xQtaAl:=QListinoEdizQtaA.Value;
     end;
end;

procedure TListinoAnnunciFrame.RxDBGrid2GetCellProps(Sender: TObject;
     Field: TField; AFont: TFont; var Background: TColor);
begin
     if (QListinoEdizQtaDa.Value<>xQtadal)and(QListinoEdizQtaA.Value<>xQtaAl) then begin
          Background:=clYellow;
          xQtadal:=QListinoEdizQtaDa.Value;
          xQtaAl:=QListinoEdizQtaA.Value;
     end;
end;

procedure TListinoAnnunciFrame.ToolbarButton974Click(Sender: TObject);
begin
     if not MainForm.CheckProfile('3502') then Exit;
     if QListinoEdiz.RecordCount=0 then exit;
     if MessageDlg('Sei sicuro di voler eliminare questa riga di listino ?',mtWarning,
          [mbNo,mbYes],0)=mrYes then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text:='delete from Ann_EdizListino '+
                         'where ID='+QListinoEdizID.asString;
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    QListinoEdiz.Close;
                    QListinoEdiz.Open;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               end;
          end;
     end;
end;

procedure TListinoAnnunciFrame.BModPrezzoQuestoCliClick(Sender: TObject);
begin
     if not MainForm.CheckProfile('3503') then Exit;
     if not MainForm.CheckProfile('181') then Exit;
     if QListinoEdiz.RecordCount=0 then exit;
     ModPrezzoForm:=TModPrezzoForm.create(self);
     if QListinoEdizPrezzoQuestoClienteLire.asString<>'' then begin
          ModPrezzoForm.PrezzoLire.value:=QListinoEdizPrezzoQuestoClienteLire.value;
          ModPrezzoForm.PrezzoEuro.value:=QListinoEdizPrezzoQuestoClienteEuro.value;
     end;
     ModPrezzoForm.ShowModal;
     if ModPrezzoForm.ModalResult=mrOK then begin
          if QListinoEdizIDCliente_1.AsString='' then begin
               with Data do begin
                    DB.BeginTrans;
                    try
                         Q1.Close;
                         Q1.SQL.text:='insert into Ann_ListinoClienti (IDEdizListino,IDCliente,PrezzoQuestoClienteLire,PrezzoQuestoClienteEuro) '+
                              ' values (:xIDEdizListino,:xIDCliente,:xPrezzoQuestoClienteLire,:xPrezzoQuestoClienteEuro)';
                         Q1.ParamByName('xIDEdizListino').asInteger:=QListinoEdizID.Value;
                         Q1.ParamByName('xIDCliente').asInteger:=xIDCliente;
                         Q1.ParamByName('xPrezzoQuestoClienteLire').asInteger:=round(ModPrezzoForm.PrezzoLire.value);
                         Q1.ParamByName('xPrezzoQuestoClienteEuro').asFloat:=ModPrezzoForm.PrezzoEuro.value;
                         Q1.ExecSQL;
                         DB.CommitTrans;
                         QListinoEdiz.Close;
                         // ??
                         QListinoEdiz.Open;
                    except
                         DB.RollbackTrans;
                         MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
                    end;
               end;
          end else begin
               with Data do begin
                    DB.BeginTrans;
                    try
                         Q1.Close;
                         Q1.SQL.text:='update Ann_ListinoClienti set PrezzoQuestoClienteLire=:xPrezzoQuestoClienteLire, '+
                              ' PrezzoQuestoClienteEuro=:xPrezzoQuestoClienteEuro '+
                              ' where ID='+QListinoEdizID_1.asString;
                         Q1.ParamByName('xPrezzoQuestoClienteLire').asInteger:=round(ModPrezzoForm.PrezzoLire.value);
                         Q1.ParamByName('xPrezzoQuestoClienteEuro').AsFloat:=ModPrezzoForm.PrezzoEuro.value;
                         Q1.ExecSQL;
                         DB.CommitTrans;
                         QListinoEdiz.Close;
                         // ??
                         QListinoEdiz.Open;
                    except
                         DB.RollbackTrans;
                         MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
                    end;
               end;
          end;
     end;
     ModPrezzoForm.Free;
end;

end.

