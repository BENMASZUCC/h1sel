unit SelRicCand;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, Db, DBTables, ExtCtrls, StdCtrls, Buttons;

type
  TSelRicCandForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Panel124: TPanel;
    QRicCand: TQuery;
    DsQRicCand: TDataSource;
    DBGrid1: TDBGrid;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SelRicCandForm: TSelRicCandForm;

implementation

{$R *.DFM}

procedure TSelRicCandForm.FormShow(Sender: TObject);
begin
     Caption:='[S/4] - '+Caption;
end;

end.
