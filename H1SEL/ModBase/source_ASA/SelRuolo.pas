unit SelRuolo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Grids, DBGrids, Db, DBTables, ExtCtrls, DBCtrls;

type
  TSelRuoloForm = class(TForm)
    Ruoli: TTable;
    DataSource1: TDataSource;
    Aree: TTable;
    RuoliID: TAutoIncField;
    RuoliDescrizione: TStringField;
    RuoliIDArea: TIntegerField;
    AreeID: TAutoIncField;
    AreeDescrizione: TStringField;
    RuoliArea: TStringField;
    DBGrid1: TDBGrid;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    DBNavigator1: TDBNavigator;
    procedure DBGrid1KeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    xStringa:string;
  public
    { Public declarations }
    xAbilitato:boolean;
  end;

var
  SelRuoloForm: TSelRuoloForm;

implementation

{$R *.DFM}

procedure TSelRuoloForm.DBGrid1KeyPress(Sender: TObject; var Key: Char);
begin
   if xAbilitato then begin
     if Key=chr(13) then begin
        BitBtn1.Click;
     end else begin
        xStringa:=xStringa+key;
        Ruoli.FindNearest([xStringa]);
     end;
   end;
end;

procedure TSelRuoloForm.FormShow(Sender: TObject);
begin
     xStringa:='';
end;

procedure TSelRuoloForm.FormCreate(Sender: TObject);
begin
     xAbilitato:=True;
end;

end.
