object LegendaElRicForm: TLegendaElRicForm
  Left = 312
  Top = 150
  BorderStyle = bsDialog
  Caption = 'Legenda e parametri'
  ClientHeight = 313
  ClientWidth = 488
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label11: TLabel
    Left = 4
    Top = 2
    Width = 390
    Height = 28
    Caption = 
      'NOTA: i seguenti valori sono GLOBALI.  E'#39' possibile settare gli ' +
      'stessi valori'#13#10'a livello di ogni singola commessa (entrando in c' +
      'ommessa, pagina "parametri")'
    Font.Charset = ANSI_CHARSET
    Font.Color = clPurple
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsItalic]
    ParentFont = False
  end
  object BitBtn1: TBitBtn
    Left = 399
    Top = 3
    Width = 88
    Height = 34
    Caption = 'Chiudi'
    TabOrder = 0
    Kind = bkOK
  end
  object Panel5: TPanel
    Left = 2
    Top = 33
    Width = 382
    Height = 41
    BevelInner = bvSpace
    BevelOuter = bvLowered
    TabOrder = 1
    object Label13: TLabel
      Left = 38
      Top = 8
      Width = 182
      Height = 26
      Caption = 'n� di giorni dalla data di prima apertura della ricerca'
      WordWrap = True
    end
    object Label14: TLabel
      Left = 8
      Top = 8
      Width = 20
      Height = 16
      Caption = 'Ap'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label15: TLabel
      Left = 233
      Top = 8
      Width = 87
      Height = 26
      Caption = 'carattere rosso se pi� di giorni '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      WordWrap = True
    end
    object RxSpinEdit1: TRxSpinEdit
      Left = 325
      Top = 11
      Width = 50
      Height = 21
      TabOrder = 0
    end
  end
  object Panel6: TPanel
    Left = 2
    Top = 78
    Width = 382
    Height = 41
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 2
    object Label16: TLabel
      Left = 38
      Top = 8
      Width = 179
      Height = 26
      Caption = 
        'n� di giorni dall'#39'ultimo CONTATTO con un soggetto associato alla' +
        ' ricerca'
      WordWrap = True
    end
    object Label17: TLabel
      Left = 7
      Top = 8
      Width = 20
      Height = 16
      Caption = 'Uc'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 233
      Top = 8
      Width = 87
      Height = 26
      Caption = 'carattere rosso se pi� di giorni '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      WordWrap = True
    end
    object RxSpinEdit2: TRxSpinEdit
      Left = 325
      Top = 11
      Width = 50
      Height = 21
      TabOrder = 0
    end
  end
  object Panel7: TPanel
    Left = 2
    Top = 123
    Width = 382
    Height = 41
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 3
    object Label19: TLabel
      Left = 7
      Top = 8
      Width = 24
      Height = 16
      Caption = 'Col'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label20: TLabel
      Left = 38
      Top = 8
      Width = 179
      Height = 26
      Caption = 
        'n� di giorni dall'#39'ultimo COLLOQUIO con un soggetto associato all' +
        'a ricerca'
      WordWrap = True
    end
    object Label2: TLabel
      Left = 233
      Top = 8
      Width = 87
      Height = 26
      Caption = 'carattere rosso se pi� di giorni '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      WordWrap = True
    end
    object RxSpinEdit3: TRxSpinEdit
      Left = 325
      Top = 11
      Width = 50
      Height = 21
      TabOrder = 0
    end
  end
  object Panel1: TPanel
    Left = 2
    Top = 168
    Width = 271
    Height = 93
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 4
    object Label3: TLabel
      Left = 8
      Top = 7
      Width = 162
      Height = 13
      Caption = 'legenda colori colonna stato'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 48
      Top = 72
      Width = 175
      Height = 13
      Caption = 'verde: effettuato almeno un colloquio'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 48
      Top = 40
      Width = 214
      Height = 13
      Caption = 'arancione: candidati inseriti ma senza contatti'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 48
      Top = 56
      Width = 169
      Height = 13
      Caption = 'giallo: effettuato almeno un contatto'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label7: TLabel
      Left = 48
      Top = 24
      Width = 151
      Height = 13
      Caption = 'rosso: nessun candidato inserito'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Shape1: TShape
      Left = 8
      Top = 25
      Width = 35
      Height = 14
      Brush.Color = clRed
    end
    object Shape2: TShape
      Left = 8
      Top = 41
      Width = 35
      Height = 14
      Brush.Color = 33023
    end
    object Shape3: TShape
      Left = 8
      Top = 57
      Width = 35
      Height = 14
      Brush.Color = clYellow
    end
    object Shape4: TShape
      Left = 8
      Top = 73
      Width = 35
      Height = 14
      Brush.Color = clLime
    end
  end
  object Panel2: TPanel
    Left = 2
    Top = 266
    Width = 271
    Height = 46
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 5
    object Label8: TLabel
      Left = 8
      Top = 7
      Width = 205
      Height = 13
      Caption = 'legenda colore carattere Data Inizio'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label18: TLabel
      Left = 8
      Top = 24
      Width = 251
      Height = 13
      Caption = 'Rosso: superata o raggiunta data prevista di chiusura'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
  end
  object Q: TQuery
    DatabaseName = 'EBCDB'
    Left = 392
    Top = 72
  end
end
