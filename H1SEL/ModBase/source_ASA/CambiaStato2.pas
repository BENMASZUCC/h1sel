unit CambiaStato2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Grids, DBGrids, Mask, DBCtrls, ExtCtrls, Db, DBTables,
  DtEdit97;

type
  TCambiaStato2Form = class(TForm)
    Panel1: TPanel;
    PanStato: TPanel;
    Label2: TLabel;
    Panel3: TPanel;
    Panel4: TPanel;
    DBGrid1: TDBGrid;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Panel5: TPanel;
    Panel6: TPanel;
    Label1: TLabel;
    Edit1: TEdit;
    Label4: TLabel;
    DEData: TDateEdit97;
    Panel127: TPanel;
    QNextStati: TQuery;
    dsQNextStati: TDataSource;
    QNextStatiID: TAutoIncField;
    QNextStatiEvento: TStringField;
    QNextStatiAStato: TStringField;
    QNextStatiIDProcedura: TIntegerField;
    ECogn: TEdit;
    ENome: TEdit;
    QNextStatiIDaStato: TIntegerField;
    QNextStatiIDTipoStatoA: TIntegerField;
    EStato: TEdit;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    xIDDaStato:integer;
  end;

var
  CambiaStato2Form: TCambiaStato2Form;

implementation

uses ModuloDati, ModuloDati2, Main, InSelezione, Inserimento, SelData;

{$R *.DFM}

procedure TCambiaStato2Form.FormShow(Sender: TObject);
begin
     Caption:='[M/050] - '+Caption;
     QNextStati.ParamByName('xDaStato').asInteger:=xIDDaStato;
     QNextStati.Open;
end;

end.
