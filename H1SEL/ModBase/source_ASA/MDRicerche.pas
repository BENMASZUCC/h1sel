unit MDRicerche;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     Db,DBTables;

type
     TDataRicerche=class(TDataModule)
          DsRicerchePend: TDataSource;
          QCandRic: TQuery;
          DsQCandRic: TDataSource;
          QCandTutte: TQuery;
          DsQCandTutte: TDataSource;
          TAnagColloqui: TTable;
          DsAnagColloqui: TDataSource;
          TAnagColloquiID: TAutoIncField;
          TAnagColloquiIDAnagrafica: TIntegerField;
          TAnagColloquiIDRicerca: TIntegerField;
          TAnagColloquiIDSelezionatore: TIntegerField;
          TAnagColloquiData: TDateTimeField;
          TAnagColloquiOra: TDateTimeField;
          TAnagColloquiValutazioneGenerale: TStringField;
          TAnagColloquiPunteggio: TSmallintField;
          TAnagColloquiEsito: TStringField;
          TColloquiABS: TTable;
          TColloquiABSID: TAutoIncField;
          TColloquiABSIDAnagrafica: TIntegerField;
          TColloquiABSIDRicerca: TIntegerField;
          TColloquiABSIDSelezionatore: TIntegerField;
          TColloquiABSData: TDateTimeField;
          TColloquiABSOra: TDateTimeField;
          TColloquiABSValutazioneGenerale: TStringField;
          TColloquiABSPunteggio: TSmallintField;
          TColloquiABSEsito: TStringField;
          TRicercheIDX: TTable;
          TRicercheIDXID: TAutoIncField;
          TRicercheIDXProgressivo: TStringField;
          QRicAttive: TQuery;
          DsQRicAttive: TDataSource;
          TRicAnagIDX: TTable;
          TRicAnagIDXID: TAutoIncField;
          TRicAnagIDXIDRicerca: TIntegerField;
          TRicAnagIDXIDAnagrafica: TIntegerField;
          TRicAnagIDXEscluso: TBooleanField;
          TRicAnagIDXInEvidenza: TBooleanField;
          TRicAnagIDXDataImpegno: TDateTimeField;
          TRicAnagIDXCodStato: TSmallintField;
          TRicAnagIDXStato: TStringField;
          TRicAnagIDXNote: TMemoField;
          TRicAnagIDXDataAssunzione: TDateTimeField;
          TContattiABS: TTable;
          TContattiABSID: TAutoIncField;
          TContattiABSIDRicerca: TIntegerField;
          TContattiABSIDAnagrafica: TIntegerField;
          TContattiABSTipoContatto: TStringField;
          TContattiABSData: TDateTimeField;
          TContattiABSOra: TDateTimeField;
          TContattiABSEvaso: TBooleanField;
          TContattiABSNote: TStringField;
          TRicercheLK: TTable;
          TRicercheLKID: TAutoIncField;
          TRicercheLKProgressivo: TStringField;
          QGen: TQuery;
          TContattiABSEsito: TStringField;
          QRicAttiveID: TIntegerField;
          QRicAttiveProgressivo: TStringField;
          QRicAttiveCliente: TStringField;
          QRicAttiveDataInizio: TDateTimeField;
          QRicAttiveNumRicercati: TSmallintField;
          QRicAttiveRuolo: TStringField;
          QRicAttiveSelezionatore: TStringField;
          QRicAttiveStato: TStringField;
          QRicAttiveConclusa: TBooleanField;
          TRicercheLKIDCliente: TIntegerField;
          TRicercheLKCliente: TStringField;
          TCandRicABS: TTable;
          TCandRicABSID: TAutoIncField;
          TCandRicABSDataIns: TDateTimeField;
          TCandRicABSMiniVal: TStringField;
          TCandRicABSStato: TStringField;
          TRicAnagIDXDataIns: TDateTimeField;
          TRicAnagIDXMiniVal: TStringField;
          QRicAttiveIDStatoRic: TIntegerField;
          TStoricoRicABS: TTable;
          TStoricoRicABSIDRicerca: TIntegerField;
          TStoricoRicABSIDStatoRic: TIntegerField;
          TStoricoRicABSDallaData: TDateTimeField;
          TStoricoRicABSNote: TStringField;
          DsStoricoRic: TDataSource;
          QCandAzienda: TQuery;
          DsQCandAzienda: TDataSource;
          QCandAziendaID: TIntegerField;
          QCandAziendaCVNumero: TIntegerField;
          QCandAziendaCognome: TStringField;
          QCandAziendaNome: TStringField;
          QCandAziendaIDMansione: TIntegerField;
          QCandRicID: TAutoIncField;
          QCandRicIDRicerca: TIntegerField;
          QCandRicIDAnagrafica: TIntegerField;
          QCandRicEscluso: TBooleanField;
          QCandRicDataImpegno: TDateTimeField;
          QCandRicCodstato: TSmallintField;
          QCandRicStato: TStringField;
          QCandRicCognome: TStringField;
          QCandRicNome: TStringField;
          QCandRicCVNumero: TIntegerField;
          QCandRicTipoStato: TStringField;
          QCandRicDataIns: TDateTimeField;
          QCandRicMiniVal: TStringField;
          QCandTutteID: TAutoIncField;
          QCandTutteIDRicerca: TIntegerField;
          QCandTutteIDAnagrafica: TIntegerField;
          QCandTutteEscluso: TBooleanField;
          QCandTutteDataImpegno: TDateTimeField;
          QCandTutteCodstato: TSmallintField;
          QCandTutteStato: TStringField;
          QCandTutteCognome: TStringField;
          QCandTutteNome: TStringField;
          QCandRicIDStato: TIntegerField;
          TRicerchePend: TQuery;
          UpdRicPend: TUpdateSQL;
          TRicerchePendID: TAutoIncField;
          TRicerchePendIDCliente: TIntegerField;
          TRicerchePendIDUtente: TIntegerField;
          TRicerchePendDataInizio: TDateTimeField;
          TRicerchePendDataFine: TDateTimeField;
          TRicerchePendStato: TStringField;
          TRicerchePendConclusa: TBooleanField;
          TRicerchePendIDStatoRic: TIntegerField;
          TRicerchePendDallaData: TDateTimeField;
          TRicerchePendIDArea: TIntegerField;
          TRicerchePendIDMansione: TIntegerField;
          TRicerchePendIDSpec: TIntegerField;
          TRicerchePendNumRicercati: TSmallintField;
          TRicerchePendNote: TMemoField;
          TRicerchePendIDFattura: TIntegerField;
          TRicerchePendSospesa: TBooleanField;
          TRicerchePendSospesaDal: TDateTimeField;
          TRicerchePendStipendioNetto: TFloatField;
          TRicerchePendStipendioLordo: TFloatField;
          TRicerchePendFatturata: TBooleanField;
          TRicerchePendSpecifiche: TMemoField;
          TRicerchePendArea: TStringField;
          TRicerchePendMansione: TStringField;
          TRicerchePendIDAreaMans: TIntegerField;
          TRicerchePendCliente: TStringField;
          TRicerchePendUtente: TStringField;
          TRicerchePendStatoRic: TStringField;
          QRicAttiveStatoRic: TStringField;
          TClientiLK: TTable;
          TClientiLKID: TAutoIncField;
          TClientiLKDescrizione: TStringField;
          TStoricoRic: TQuery;
          TStoricoRicID: TAutoIncField;
          TStoricoRicIDRicerca: TIntegerField;
          TStoricoRicIDStatoRic: TIntegerField;
          TStoricoRicDallaData: TDateTimeField;
          TStoricoRicNote: TStringField;
          TStoricoRicStatoRic: TStringField;
          QOrgNew: TQuery;
          QOrgNewID: TAutoIncField;
          QOrgNewIDLivello: TIntegerField;
          QOrgNewTipo: TStringField;
          QOrgNewDescrizione: TStringField;
          QOrgNewDescBreve: TStringField;
          QOrgNewDataAgg: TDateTimeField;
          QOrgNewTipoSubordine: TStringField;
          QOrgNewNodoPadre: TIntegerField;
          QOrgNewIDMansione: TIntegerField;
          QOrgNewIDDipendente: TIntegerField;
          QOrgNewPercentuale: TSmallintField;
          QOrgNewCentroCosto: TIntegerField;
          QOrgNewMonteOreRichiesto: TSmallintField;
          QOrgNewTassoAssenteismo: TSmallintField;
          QOrgNewInterim: TBooleanField;
          QOrgNewInStaff: TBooleanField;
          QOrgNewValutazGen: TFloatField;
          QOrgNewIndiceSost: TFloatField;
          QOrgNewIndiceImp: TFloatField;
          QOrgNewUltimoAgg: TDateTimeField;
          QOrgNewPARENT: TIntegerField;
          QOrgNewWIDTH: TIntegerField;
          QOrgNewHEIGHT: TIntegerField;
          QOrgNewTYPE: TStringField;
          QOrgNewCOLOR: TIntegerField;
          QOrgNewIMAGE: TIntegerField;
          QOrgNewIMAGEALIGN: TStringField;
          QOrgNewORDINE: TIntegerField;
          QOrgNewALIGN: TStringField;
          DsQOrgNew: TDataSource;
          UpdOrgNew: TUpdateSQL;
          QOrgNewCognome: TStringField;
          QOrgNewNome: TStringField;
          QOrgNewCVNumero: TIntegerField;
          QMansioniLK: TQuery;
          QMansioniLKID: TAutoIncField;
          QMansioniLKDescrizione: TStringField;
          QOrgNewMansione: TStringField;
          QOrgNewDicNodo: TStringField;
          TRicerchePendSpecifEst: TMemoField;
          QCandRicDataUltimoContatto: TDateTimeField;
          QTotCandRic: TQuery;
          QTotCandRicIDRicerca: TIntegerField;
          QTotCandRicTotCand: TIntegerField;
          QTipoCommesse: TQuery;
          DsQTipoCommesse: TDataSource;
          QRicAttiveTipo: TStringField;
          TRicerchePendTipo: TStringField;
          QTipiComm: TQuery;
          DsQTipiComm: TDataSource;
          QTipiCommID: TAutoIncField;
          QTipiCommTipoCommessa: TStringField;
          QTipiCommColore: TStringField;
          QAnnunciCand: TQuery;
          DsQAnnunciCand: TDataSource;
          QAnnunciCandTestata: TStringField;
          QAnnunciCandNomeEdizione: TStringField;
          QAnnunciCandData: TDateTimeField;
          QAnnunciCandCVNumero: TIntegerField;
          QAnnunciCandCognome: TStringField;
          QAnnunciCandNome: TStringField;
          QAnnunciCandCVInseritoIndata: TDateTimeField;
          QAnnunciCandIDAnagrafica: TAutoIncField;
          QAnnunciCandIDProprietaCV: TIntegerField;
          QAnnunciCandIDAnnuncio: TIntegerField;
          TRicerchePendSede: TStringField;
          TRicerchePendRifinterno: TStringField;
          TRicerchePendIDSede: TIntegerField;
          TRicerchePendIDContattoCli: TIntegerField;
          TRicerchePendIDUtente2: TIntegerField;
          TRicerchePendIDUtente3: TIntegerField;
          DsAziende: TDataSource;
          QAziende: TQuery;
          QAziendeID: TAutoIncField;
          QAziendeDescrizione: TStringField;
          QAziendeStato: TStringField;
          QAziendeIndirizzo: TStringField;
          QAziendeCap: TStringField;
          QAziendeComune: TStringField;
          QAziendeProvincia: TStringField;
          QAziendeIDAttivita: TIntegerField;
          QAziendeTelefono: TStringField;
          QAziendeFax: TStringField;
          QAziendePartitaIVA: TStringField;
          QAziendeCodiceFiscale: TStringField;
          QAziendeBancaAppoggio: TStringField;
          QAziendeSistemaPagamento: TStringField;
          QAziendeNoteContratto: TMemoField;
          QAziendeResponsabile: TStringField;
          QAziendeConosciutoInData: TDateTimeField;
          QAziendeIndirizzoLegale: TStringField;
          QAziendeCapLegale: TStringField;
          QAziendeComuneLegale: TStringField;
          QAziendeProvinciaLegale: TStringField;
          QAziendeCartellaDoc: TStringField;
          QAziendeTipoStrada: TStringField;
          QAziendeTipoStradaLegale: TStringField;
          QAziendeNumCivico: TStringField;
          QAziendeNumCivicoLegale: TStringField;
          QAziendeNazioneAbb: TStringField;
          QAziendeNazioneAbbLegale: TStringField;
          QAziendeAttivita: TStringField;
          QAziendeNote: TMemoField;
          QAziendeFatturato: TFloatField;
          QAziendeNumDipendenti: TSmallintField;
          QOrgNewIDReparto: TIntegerField;
          QOrgNewReparto: TStringField;
          QCandAziendaRuolo: TStringField;
          TRicerchePendggDiffApRic: TSmallintField;
          TRicerchePendggDiffUcRic: TSmallintField;
          TRicerchePendggDiffColRic: TSmallintField;
          TRicerchePendDataPrevChiusura: TDateTimeField;
          QRicAttiveDataPrevChiusura: TDateTimeField;
          QRicLineeQuery: TQuery;
          DsQRicLineeQuery: TDataSource;
          QRicLineeQueryID: TAutoIncField;
          QRicLineeQueryIDRicerca: TIntegerField;
          QRicLineeQueryDescrizione: TStringField;
          QRicLineeQueryStringa: TStringField;
          QRicLineeQueryTabella: TStringField;
          QRicLineeQueryOpSucc: TStringField;
          QRicLineeQueryNext: TStringField;
          QAnagIncompClienti: TQuery;
          DsQAnagIncompClienti: TDataSource;
          QAnagIncompClientiID: TAutoIncField;
          QAnagIncompClientiIDAnagrafica: TIntegerField;
          QAnagIncompClientiIDCliente: TIntegerField;
          QAnagIncompClientiDescrizione: TStringField;
          QAnagIncompClientiCliente: TStringField;
          TRicerchePendIDOfferta: TIntegerField;
          QCandRicTelUffCell: TStringField;
          QCandRicCellulare: TStringField;
          QCandRictelUfficio: TStringField;
          QCandRicDataNascita: TDateTimeField;
          QCandRicEta: TStringField;
          QEspLavLK: TQuery;
          QEspLavLKID: TAutoIncField;
          QEspLavLKAzienda: TStringField;
          QEspLavLKRuolo: TStringField;
          QLastEspLav: TQuery;
          QLastEspLavIDAnagrafica: TIntegerField;
          QLastEspLavMaxID: TIntegerField;
          QLastEspLavAzienda: TStringField;
          QLastEspLavRuolo: TStringField;
          QCandRicAzienda: TStringField;
          QAnagNoteRic: TQuery;
          DsQAnagNoteRic: TDataSource;
          QAnagNoteRicID: TAutoIncField;
          QAnagNoteRicProgressivo: TStringField;
          QCandRicNote: TStringField;
          QOrgNewIDCliente: TIntegerField;
          TRicerchePendProvvigione: TBooleanField;
          TRicerchePendProvvigionePerc: TIntegerField;
          TRicerchePendQuandoPagamUtente: TStringField;
          TRicerchePendOreLavUtente1: TIntegerField;
          TRicerchePendOreLavUtente2: TIntegerField;
          TRicerchePendOreLavUtente3: TIntegerField;
          QRicTiming: TQuery;
          DsQRicTiming: TDataSource;
          QRicTimingNominativo: TStringField;
          QRicTimingTot: TIntegerField;
          QRicTimingIDUtente: TIntegerField;
          QRicTimingID: TAutoIncField;
          QOrgNodoRuolo: TQuery;
          dsQOrgNodoRuolo: TDataSource;
          QOrgNodoRuoloID: TAutoIncField;
          QOrgNodoRuoloCVNumero: TIntegerField;
          QOrgNodoRuoloCognome: TStringField;
          QOrgNodoRuoloNome: TStringField;
          QTemp: TQuery;
          QAnagNoteRicNote: TStringField;
          QRicUtenti: TQuery;
          DsQRicUtenti: TDataSource;
          QRicUtentiUtente: TStringField;
          QRicUtentiRuoloRic: TStringField;
          QRicUtentiID: TAutoIncField;
          QRicUtentiIDRicerca: TIntegerField;
          QRicUtentiIDUtente: TIntegerField;
          QRicUtentiIDRuoloRic: TIntegerField;
          QRicTargetList: TQuery;
          DsQRicTargetList: TDataSource;
          QRicTargetListID: TAutoIncField;
          QRicTargetListIDRicerca: TIntegerField;
          QRicTargetListIDCliente: TIntegerField;
          QRicTargetListNote: TMemoField;
          QRicTargetListAzienda: TStringField;
          QRicTargetListAttivita: TStringField;
          QRicTargetListtelefono: TStringField;
          QRicTargetListComune: TStringField;
          UpdRicTargetList: TUpdateSQL;
          QOrgNodoRuoloIDProprietaCV: TIntegerField;
          QRicTargetListDataUltimaEsploraz: TDateTimeField;
          QTLCandidati: TQuery;
          DsQTLCandidati: TDataSource;
          QTLCandidatiID: TAutoIncField;
          QTLCandidatiIDAnagrafica: TAutoIncField;
          QTLCandidatiCVNumero: TIntegerField;
          QTLCandidatiCognome: TStringField;
          QTLCandidatiNome: TStringField;
          QTLCandidatiIDStato: TIntegerField;
          QTLCandidatiCellulare: TStringField;
          QTLCandidatiTelUfficio: TStringField;
          QTLCandidatiDataNascita: TDateTimeField;
          QTLCandidatiEta: TStringField;
          QRicTargetListIDAttivita: TIntegerField;
          QRicTimeSheet: TQuery;
          DsQRicTimeSheet: TDataSource;
          UpdRicTimeSheet: TUpdateSQL;
          QRicTimeSheetID: TAutoIncField;
          QRicTimeSheetIDRicerca: TIntegerField;
          QRicTimeSheetIDUtente: TIntegerField;
          QRicTimeSheetData: TDateTimeField;
          QRicTimeSheetOreConsuntivo: TFloatField;
          QRicTimeSheetNote: TMemoField;
          QUsersLK: TQuery;
          QUsersLKID: TAutoIncField;
          QUsersLKNominativo: TStringField;
          QUsersLKDescrizione: TStringField;
          DsQUsersLK: TDataSource;
          QRicTimeSheetUtente: TStringField;
          QRicTimeSheetIDGruppoProfess: TIntegerField;
          QGruppiProfLK: TQuery;
          DsQGruppiProfLK: TDataSource;
          QGruppiProfLKID: TAutoIncField;
          QGruppiProfLKGruppoProfessionale: TStringField;
          QRicTimeSheetGruppiProfess: TStringField;
          TRicerchePendTitoloCliente: TStringField;
          QRicAttiveTitoloCliente: TStringField;
          QOfferteLK: TQuery;
          QOfferteLKID: TAutoIncField;
          QOfferteLKRif: TStringField;
          QRicAttiveIDOfferta: TIntegerField;
          QRicAttiveRifOfferta: TStringField;
          TRicerchePendIDContrattoCG: TIntegerField;
          QTimeRepDett: TQuery;
          DsQTimeRepDett: TDataSource;
          UpdQTimeRepDett: TUpdateSQL;
          QTimeRepDettID: TAutoIncField;
          QTimeRepDettIDRicTimeSheet: TIntegerField;
          QTimeRepDettIDCausale: TIntegerField;
          QTimeRepDettOre: TFloatField;
          QTimeRepDettNote: TMemoField;
          QTRepCausali: TQuery;
          QTRepCausaliID: TAutoIncField;
          QTRepCausaliCausale: TStringField;
          QTimeRepDettCausale: TStringField;
          QUsersLKIDGruppoProfess: TIntegerField;
          TRicerchePendProgressivo: TStringField;
          QTotCompDaFattLK: TQuery;
          QRicAttiveTotCompDaFatt: TFloatField;
          QAnagNoteRicCliente: TStringField;
          QAnagTitoloMansAtt: TQuery;
          DsQAnagTitoloMansAtt: TDataSource;
          QAnagTitoloMansAttTitoloMansione: TStringField;
          QEspLavLKTelefono: TStringField;
          QLastEspLavTelefono: TStringField;
          QCandRicTelAzienda: TStringField;
          QCandRicRecapitiTelefonici: TStringField;
          QAnnunciCandRif: TStringField;
          QTLCandidatiRuoloAttuale: TStringField;
          QCandRicIDTitoloStudioMin: TIntegerField;
          QTitoliStudioLK: TQuery;
          QTitoliStudioLKID: TAutoIncField;
          QTitoliStudioLKDescrizione: TStringField;
          QCandRicDiploma: TStringField;
          TRicerchePendIDCausale: TIntegerField;
          QCausaliLK: TQuery;
          QCausaliLKID: TAutoIncField;
          QCausaliLKCausale: TStringField;
          TRicerchePendCausale: TStringField;
          procedure DataRicercheCreate(Sender: TObject);
          procedure TCandRicAfterPost(DataSet: TDataSet);
          procedure TMansDipAfterScroll(DataSet: TDataSet);
          procedure QRicAttiveAfterOpen(DataSet: TDataSet);
          procedure QOrgNewAfterPost(DataSet: TDataSet);
          procedure QOrgNewAfterClose(DataSet: TDataSet);
          procedure QOrgNewBeforeOpen(DataSet: TDataSet);
          procedure DsQOrgNewStateChange(Sender: TObject);
          procedure QOrgNewCalcFields(DataSet: TDataSet);
          procedure QOrgNewAfterInsert(DataSet: TDataSet);
          procedure QOrgNewAfterScroll(DataSet: TDataSet);
          procedure QRicAttiveBeforeOpen(DataSet: TDataSet);
          procedure QRicAttiveAfterClose(DataSet: TDataSet);
          procedure QOrgNewAfterOpen(DataSet: TDataSet);
          procedure QOrgNewBeforeClose(DataSet: TDataSet);
          procedure QCandRicCalcFields(DataSet: TDataSet);
          procedure TRicerchePendBeforeEdit(DataSet: TDataSet);
          procedure TRicerchePendAfterPost(DataSet: TDataSet);
          procedure QCandRicAfterPost(DataSet: TDataSet);
          procedure QOrgNodoRuoloBeforeOpen(DataSet: TDataSet);
          procedure QRicTargetListAfterPost(DataSet: TDataSet);
          procedure QRicTargetListAfterOpen(DataSet: TDataSet);
          procedure QRicTargetListAfterClose(DataSet: TDataSet);
          procedure QTLCandidatiCalcFields(DataSet: TDataSet);
          procedure QCandRicAfterClose(DataSet: TDataSet);
          procedure QCandRicBeforeOpen(DataSet: TDataSet);
          procedure QLastEspLavBeforeOpen(DataSet: TDataSet);
          procedure QLastEspLavAfterClose(DataSet: TDataSet);
          procedure QRicTimeSheetAfterPost(DataSet: TDataSet);
          procedure QRicTimeSheetNewRecord(DataSet: TDataSet);
          procedure DsQRicTimeSheetStateChange(Sender: TObject);
          procedure QRicTimeSheetAfterInsert(DataSet: TDataSet);
          procedure QRicTimeSheetBeforeOpen(DataSet: TDataSet);
          procedure QRicTimeSheetAfterClose(DataSet: TDataSet);
          procedure QRicTimeSheetAfterDelete(DataSet: TDataSet);
          procedure QRicTimeSheetBeforeDelete(DataSet: TDataSet);
          procedure QRicTimeSheetBeforePost(DataSet: TDataSet);
          procedure DsQTimeRepDettStateChange(Sender: TObject);
          procedure QTimeRepDettBeforeOpen(DataSet: TDataSet);
          procedure QTimeRepDettAfterClose(DataSet: TDataSet);
          procedure QTimeRepDettAfterPost(DataSet: TDataSet);
          procedure QTimeRepDettAfterInsert(DataSet: TDataSet);
     private
          { Private declarations }
          xVecchioVal,xIDTimeSheet: integer;
          xVecchiadataInizio: TDateTime;
     public
          { Public declarations }
          xValutazDipForm: boolean;
     end;

var
     DataRicerche: TDataRicerche;

implementation

uses ModuloDati,SelArea,ValutazDip,Main,SelPers,ModuloDati2,
     uUtilsVarie,CheckPass;

{$R *.DFM}


procedure TDataRicerche.DataRicercheCreate(Sender: TObject);
begin
     xValutazDipForm:=False;
end;

procedure TDataRicerche.TCandRicAfterPost(DataSet: TDataSet);
begin
     if QCandRic.Active then begin
          QCandRic.Close;
          QCandRic.Open;
     end;
     QCandTutte.Close;
     QCandTutte.Open;
end;

procedure TDataRicerche.TMansDipAfterScroll(DataSet: TDataSet);
begin
     if xValutazDipForm then begin
          ValutazDipForm.DBChart3.RefreshData;
          ValutazDipForm.DBChart4.RefreshData;
     end;
end;

procedure TDataRicerche.QRicAttiveAfterOpen(DataSet: TDataSet);
begin
     MainForm.LTotRic.Caption:=IntToStr(QRicAttive.RecordCount);
end;

procedure TDataRicerche.QOrgNewAfterPost(DataSet: TDataSet);
begin
     QOrgNew.ApplyUpdates;
end;

procedure TDataRicerche.QOrgNewAfterClose(DataSet: TDataSet);
begin
     QMansioniLK.Close;
end;

procedure TDataRicerche.QOrgNewBeforeOpen(DataSet: TDataSet);
begin
     QMansioniLK.Open;
end;

procedure TDataRicerche.DsQOrgNewStateChange(Sender: TObject);
var b: boolean;
begin
     b:=DsQOrgNew.State in [dsEdit,dsInsert];
     MainForm.TbBOrganigMod.Enabled:=not b;
     MainForm.PanOrgDett.Enabled:=b;
     MainForm.TbBOrganigOK.Enabled:=b;
     MainForm.TbBOrganigCan.Enabled:=b;
end;

procedure TDataRicerche.QOrgNewCalcFields(DataSet: TDataSet);
begin
     QOrgNewDicNodo.value:=QOrgNewReparto.Value+chr(13)+
          QOrgNewDescBreve.value+chr(13)+
          copy(QOrgNewCognome.Value,1,1)+
          LowerCase(copy(QOrgNewCognome.Value,2,Length(QOrgNewCognome.Value)-1))+' '+
          copy(QOrgNewNome.value,1,1)+'.';
end;

procedure TDataRicerche.QOrgNewAfterInsert(DataSet: TDataSet);
begin
     with QOrgNew,MainForm.DBTree do begin
          QOrgNewInterim.Value:=False;
          QOrgNewInStaff.value:=False;
          QOrgNewIDCliente.value:=QAziendeID.Value;
          FindField('Height').AsInteger:=DefaultNodeHeight;
          FindField('Width').AsInteger:=DefaultNodeWidth;
          FindField('Type').AsString:='Rectangle';
          FindField('Color').AsInteger:=clWhite;
          FindField('Image').AsInteger:=-1;
          FindField('ImageAlign').AsString:='Left-Top';
          FindField('Align').AsString:='Center';
     end;
end;

procedure TDataRicerche.QOrgNewAfterScroll(DataSet: TDataSet);
begin
     MainForm.PColor.Color:=QOrgNewCOLOR.Value;
     if MainForm.PCAziendaOrg.activepage=MainForm.TSAziendaOrgRuolo then begin
          QOrgNodoRuolo.Close;
          QOrgNodoRuolo.Open;
     end;
end;

procedure TDataRicerche.QRicAttiveBeforeOpen(DataSet: TDataSet);
begin
     QTotCandRic.Open;
     QTotCompDaFattLK.Open;
end;

procedure TDataRicerche.QRicAttiveAfterClose(DataSet: TDataSet);
begin
     QTotCandRic.Close;
     QTotCompDaFattLK.Close;
end;

procedure TDataRicerche.QOrgNewAfterOpen(DataSet: TDataSet);
begin
     MainForm.PCAziendaOrg.Enabled:=True;
end;

procedure TDataRicerche.QOrgNewBeforeClose(DataSet: TDataSet);
begin
     MainForm.PCAziendaOrg.Enabled:=False;
end;

procedure TDataRicerche.QCandRicCalcFields(DataSet: TDataSet);
var xAnno,xMese,xgiorno: Word;
begin
     if QcandRicDataNascita.asString='' then
          QcandRicEta.asString:=''
     else begin
          DecodeDate((Date-QcandRicDataNascita.Value),xAnno,xMese,xgiorno);
          // TAnagraficaEta.Value:=StrToInt(copy(xAnno,3,2));
          QcandRicEta.Value:=copy(IntToStr(xAnno),3,2);
     end;
     QcandRicTelUffCell.Value:=QcandRicCellulare.Value+' '+QcandRicTelUfficio.Value;
end;

procedure TDataRicerche.TRicerchePendBeforeEdit(DataSet: TDataSet);
begin
     xVecchiadataInizio:=TRicerchePendDataInizio.Value;
end;

procedure TDataRicerche.TRicerchePendAfterPost(DataSet: TDataSet);
begin
     TRicerchePend.ApplyUpdates;
     TRicerchePend.CommitUpdates;
end;

procedure TDataRicerche.QCandRicAfterPost(DataSet: TDataSet);
begin
     QCandRic.ApplyUpdates;
     QCandRic.CommitUpdates;
end;

procedure TDataRicerche.QOrgNodoRuoloBeforeOpen(DataSet: TDataSet);
begin
     if (QOrgNewIDMansione.asString='')or(QOrgNewIDMansione.value=0) then
          MainForm.LTotOrgRuolo.caption:='0'
     else begin
          // conta soggetti
          QTemp.Close;
          QTemp.SQL.text:='select count(AnagMansioni.ID) Tot from AnagMansioni '+
               'where AnagMansioni.IDMansione='+QOrgNewIDMansione.asString;
          QTemp.Open;
          MainForm.LTotOrgRuolo.caption:=IntToStr(QTemp.FieldByName('Tot').asInteger);
          //if QTemp.FieldByName('Tot').asInteger>100 then begin
          //     if MessageDlg('ATTENZIONE:  il numero di soggetti supera i 100. '+chr(13)+
          //          'Vuoi visualizzare lo stesso la griglia ?',mtWarning, [mbYes,mbNo],0)=mrNo then Abort;
          //end;
     end;
end;

procedure TDataRicerche.QRicTargetListAfterPost(DataSet: TDataSet);
begin
     with QRicTargetList do begin
          Data.DB.BeginTrans;
          try
               ApplyUpdates;
               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               raise;
          end;
          CommitUpdates;
     end;
end;

procedure TDataRicerche.QRicTargetListAfterOpen(DataSet: TDataSet);
begin
     if not QRicTargetList.IsEmpty then
          QTLCandidati.Open;
end;

procedure TDataRicerche.QRicTargetListAfterClose(DataSet: TDataSet);
begin
     QTLCandidati.Close;
end;

procedure TDataRicerche.QTLCandidatiCalcFields(DataSet: TDataSet);
var xAnno,xMese,xgiorno: Word;
begin
     if QTLCandidatiDataNascita.asString='' then
          QTLCandidatiEta.asString:=''
     else begin
          DecodeDate((Date-QTLCandidatiDataNascita.Value),xAnno,xMese,xgiorno);
          // TAnagraficaEta.Value:=StrToInt(copy(xAnno,3,2));
          QTLCandidatiEta.Value:=copy(IntToStr(xAnno),3,2);
     end;
end;

procedure TDataRicerche.QCandRicAfterClose(DataSet: TDataSet);
begin
     QLastEspLav.Close;
     QTitoliStudioLK.Close;
end;

procedure TDataRicerche.QCandRicBeforeOpen(DataSet: TDataSet);
begin
     QLastEspLav.Open;
     QTitoliStudioLK.Open;
end;

procedure TDataRicerche.QLastEspLavBeforeOpen(DataSet: TDataSet);
begin
     QEspLavLK.Open;
end;

procedure TDataRicerche.QLastEspLavAfterClose(DataSet: TDataSet);
begin
     QEspLavLK.Close;
end;

procedure TDataRicerche.QRicTimeSheetAfterPost(DataSet: TDataSet);
var xID: integer;
begin
     with QRicTimeSheet do begin
          Data.DB.BeginTrans;
          try
               ApplyUpdates;
               // se c'� il controllo di gestione, allora inserisci o aggiorna time report
               if copy(Data.GlobalCheckBoxIndici.Value,6,1)='1' then begin
                    if TRicerchePendIDContrattoCG.AsString<>'' then begin
                         Data.QTemp.Close;
                         Data.QTemp.SQL.text:='select ID from CG_ContrattiTimeSheet '+
                              'where IDTimeRepH1Sel=:xIDTimeRepH1Sel';
                         Data.QTemp.ParamByName('xIDTimeRepH1Sel').asInteger:=QRicTimeSheetID.Value;
                         Data.QTemp.Open;
                         if Data.QTemp.IsEmpty then begin
                              // ID questo record inserito
                              QTemp.Close;
                              QTemp.SQL.text:='select @@IDENTITY as LastID';
                              QTemp.Open;
                              xID:=QTemp.FieldByName('LastID').asInteger;
                              QTemp.Close;
                              Data.Q1.Close;
                              Data.Q1.SQL.text:='insert into CG_ContrattiTimeSheet (IDContratto, IDUtente, Data, OreConsuntivo, Note, IDGruppoProfess, IDTimeRepH1Sel) '+
                                   'values (:xIDContratto, :xIDUtente, :xData, :xOreConsuntivo, :xNote, :xIDGruppoProfess, :xIDTimeRepH1Sel)';
                              Data.Q1.ParamByName('xIDContratto').asInteger:=TRicerchePendIDContrattoCG.Value;
                              Data.Q1.ParamByName('xIDUtente').asInteger:=QRicTimeSheetIDUtente.Value;
                              Data.Q1.ParamByName('xData').asDateTime:=QRicTimeSheetData.Value;
                              Data.Q1.ParamByName('xOreConsuntivo').asFloat:=QRicTimeSheetOreConsuntivo.Value;
                              Data.Q1.ParamByName('xNote').asString:=QRicTimeSheetNote.Value;
                              Data.Q1.ParamByName('xIDGruppoProfess').asInteger:=QRicTimeSheetIDGruppoProfess.Value;
                              Data.Q1.ParamByName('xIDTimeRepH1Sel').asInteger:=xID;
                              Data.Q1.ExecSQL;
                         end else begin
                              Data.Q1.Close;
                              Data.Q1.SQL.text:='update CG_ContrattiTimeSheet set IDUtente=:xIDUtente, Data=:xData,'+
                                   ' OreConsuntivo=:xOreConsuntivo, Note=:xNote, IDGruppoProfess=:xIDGruppoProfess '+
                                   'where ID=:xID';
                              Data.Q1.ParamByName('xIDUtente').asInteger:=QRicTimeSheetIDUtente.Value;
                              Data.Q1.ParamByName('xData').asDateTime:=QRicTimeSheetData.Value;
                              Data.Q1.ParamByName('xOreConsuntivo').asFloat:=QRicTimeSheetOreConsuntivo.Value;
                              Data.Q1.ParamByName('xNote').asString:=QRicTimeSheetNote.Value;
                              Data.Q1.ParamByName('xIDGruppoProfess').asInteger:=QRicTimeSheetIDGruppoProfess.Value;
                              Data.Q1.ParamByName('xID').asInteger:=Data.QTemp.FieldByName('ID').asInteger;
                              Data.Q1.ExecSQL;
                         end;
                         Data.QTemp.Close;
                    end;
               end;
               Data.DB.CommitTrans;
               xID:=QRicTimeSheetID.Value;
               QRicTimeSheet.Close;
               QRicTimeSheet.Open;
               QRicTimeSheet.Locate('ID',xID, []);
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               raise;
          end;
          CommitUpdates;
     end;
end;

procedure TDataRicerche.QRicTimeSheetNewRecord(DataSet: TDataSet);
begin
     QRicTimeSheetIDUtente.Value:=MainForm.xIDUtenteAttuale;
     QRicTimeSheetData.Value:=Date;
     QUsersLK.locate('ID',QRicTimeSheetIDUtente.Value, []);
     QRicTimeSheetIDGruppoProfess.Value:=QUsersLKIDGruppoProfess.Value;

end;

procedure TDataRicerche.DsQRicTimeSheetStateChange(Sender: TObject);
var b: boolean;
begin
     b:=DsQRicTimeSheet.State in [dsEdit,dsInsert];
     SelPersForm.BTimeSheetNew.Enabled:=not b;
     SelPersForm.BTimeSheetDel.Enabled:=not b;
     SelPersForm.BTimeSheetOK.Enabled:=b;
     SelPersForm.BTimeSheetCan.Enabled:=b;
end;

procedure TDataRicerche.QRicTimeSheetAfterInsert(DataSet: TDataSet);
begin
     QRicTimeSheetIDRicerca.Value:=TRicerchePendID.Value;
end;

procedure TDataRicerche.QRicTimeSheetBeforeOpen(DataSet: TDataSet);
begin
     QGruppiProfLK.Open;
end;

procedure TDataRicerche.QRicTimeSheetAfterClose(DataSet: TDataSet);
begin
     QGruppiProfLK.Close;
end;

procedure TDataRicerche.QRicTimeSheetAfterDelete(DataSet: TDataSet);
begin
     // se c'� il controllo di gestione, allora inserisci o aggiorna time report
     with QRicTimeSheet do begin
          Data.DB.BeginTrans;
          try
               ApplyUpdates;
               if copy(Data.GlobalCheckBoxIndici.Value,6,1)='1' then begin
                    QTemp.Close;
                    QTemp.SQL.text:='delete from CG_ContrattiTimeSheet '+
                         'where IDTimeRepH1Sel=:xIDTimeRepH1Sel';
                    QTemp.ParambyName('xIDTimeRepH1Sel').asInteger:=xIDTimeSheet;
                    QTemp.ExecSQL;
               end;
               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: operazione non effettuata',mtError, [mbOK],0);
          end;
     end;
end;

procedure TDataRicerche.QRicTimeSheetBeforeDelete(DataSet: TDataSet);
begin
     xIDTimeSheet:=QRicTimeSheetID.Value;
end;

procedure TDataRicerche.QRicTimeSheetBeforePost(DataSet: TDataSet);
begin
     // controllo utente-data
{     Data.QTemp.Close;
     Data.QTemp.SQL.text:='select count(*) Tot from EBC_RicercheTimeSheet '+
          'where IDRicerca=:xIDRicerca and IDUtente=:xIDUtente and Data=:xData';
     Data.QTemp.ParamByName('xIDRicerca').asInteger:=TRicerchePendID.Value;
     Data.QTemp.ParamByName('xIDUtente').asInteger:=QRicTimeSheetIDUtente.Value;
     Data.QTemp.ParamByName('xData').asDateTime:=QRicTimeSheetData.Value;
     Data.QTemp.Open;
     if Data.QTemp.FieldByName('Tot').asInteger>0 then begin
        ShowMessage('Duplicazione utente-data - IMPOSSIBILE REGISTRARE');
        Abort;
     end;}
end;

procedure TDataRicerche.DsQTimeRepDettStateChange(Sender: TObject);
var b: boolean;
begin
     b:=DsQTimeRepDett.State in [dsEdit,dsInsert];
     SelPersForm.BTRepDettNew.Enabled:=not b;
     SelPersForm.BTRepDettDel.Enabled:=not b;
     SelPersForm.BTRepDettOK.Enabled:=b;
     SelPersForm.BTRepDettCan.Enabled:=b;
end;

procedure TDataRicerche.QTimeRepDettBeforeOpen(DataSet: TDataSet);
begin
     QTRepCausali.Open;
end;

procedure TDataRicerche.QTimeRepDettAfterClose(DataSet: TDataSet);
begin
     QTRepCausali.Close;
end;

procedure TDataRicerche.QTimeRepDettAfterPost(DataSet: TDataSet);
begin
     with QTimeRepDett do begin
          Data.DB.BeginTrans;
          try
               ApplyUpdates;
               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               raise;
          end;
          CommitUpdates;
     end;
end;

procedure TDataRicerche.QTimeRepDettAfterInsert(DataSet: TDataSet);
begin
     QTimeRepDettIDRicTimeSheet.Value:=QRicTimeSheetID.Value;
end;

end.

