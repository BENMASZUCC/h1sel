unit StoricoTargetList;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  dxDBTLCl, dxGrClms, dxTL, dxDBCtrl, dxDBGrid, Db, DBTables, dxCntner,
  StdCtrls, Buttons, ExtCtrls;

type
  TStoricoTargetListForm = class(TForm)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    dxDBGrid1: TdxDBGrid;
    QStoricoTL: TQuery;
    DsQStoricoTL: TDataSource;
    QStoricoTLID: TAutoIncField;
    QStoricoTLIDRicerca: TIntegerField;
    QStoricoTLIDCliente: TIntegerField;
    QStoricoTLNote: TMemoField;
    QStoricoTLProgressivo: TStringField;
    QStoricoTLCliente: TStringField;
    QStoricoTLDataUltimaEsploraz: TDateTimeField;
    dxDBGrid1ID: TdxDBGridMaskColumn;
    dxDBGrid1IDRicerca: TdxDBGridMaskColumn;
    dxDBGrid1IDCliente: TdxDBGridMaskColumn;
    dxDBGrid1Progressivo: TdxDBGridMaskColumn;
    dxDBGrid1Cliente: TdxDBGridMaskColumn;
    dxDBGrid1DataUltimaEsploraz: TdxDBGridDateColumn;
    dxDBGrid1Note: TdxDBGridBlobColumn;
    Label1: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  StoricoTargetListForm: TStoricoTargetListForm;

implementation

{$R *.DFM}

end.
