�
 TINVALUTAZIONEFORM 0i  TPF0TInValutazioneFormInValutazioneFormLeft� Top� BorderIcons BorderStylebsDialogCaptionCandidato in valutazioneClientHeight�ClientWidth�Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style PositionpoScreenCenterPixelsPerInch`
TextHeight TPageControlPageControl1Left Top Width�Height�
ActivePage
TsColloquiAlignalClientTabOrder  	TTabSheet
TsColloquiCaptionProgrammaz.Colloqui TLabelLabel7Left	Top0Width� HeightCaption%Colloqui programmati per il candidatoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TDBCtrlGridDBCtrlGrid1Left	TopYWidthIHeightAllowInsertColCount
DataSourceDataAssunzione.DsColloquiCandPanelHeight
PanelWidth9TabOrder RowCount	SelectedColorclTeal TDBEditDBEdit1LeftTopWidth@Height	DataFieldData
DataSourceDataAssunzione.DsColloquiCandTabOrder   TDBEditDBEdit2LeftKTopWidth(Height	DataFieldOra
DataSourceDataAssunzione.DsColloquiCandTabOrder  TDBEditDBEdit3LeftTopWidth^Height	DataFieldValutazioneGenerale
DataSourceDataAssunzione.DsColloquiCandTabOrder  TDBEditDBEdit4Left� TopWidth!Height	DataField	Punteggio
DataSourceDataAssunzione.DsColloquiCandTabOrder  TDBEditDBEdit5LeftmTopWidth� Height	DataFieldEsito
DataSourceDataAssunzione.DsColloquiCandTabOrder  TDBLookupComboBoxDBLookupComboBox1LeftuTopWidthlHeight	DataFieldSelezionatore
DataSourceDataAssunzione.DsColloquiCandTabOrder   TPanelPanel2Left	TopBWidth9HeightColorclGrayTabOrder TLabelLabel1Left	TopWidthHeightCaptionDataFocusControlDBEdit1Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel2LeftLTopWidthHeightCaptionOraFocusControlDBEdit2Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel3LeftTopWidthOHeightCaptionValutaz.generaleFocusControlDBEdit3Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel4Left� TopWidth0HeightCaption	PunteggioFocusControlDBEdit4Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel5LeftnTopWidthHeightCaptionEsitoFocusControlDBEdit5Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel6LeftvTopWidth@HeightCaptionSelezionatoreFocusControlDBLookupComboBox1Font.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont   	TGroupBox	GroupBox1Left
TopWidth� Height*CaptionEventoTabOrder TLabelLabel8LeftTopWidthHeightCaptionData:  TDateTimePicker
DataEventoLeft+TopWidthMHeightCalAlignmentdtaLeftDate�-�.J���@Time�-�.J���@
DateFormatdfShortDateMode
dmComboBoxKinddtkDate
ParseInput     TPanelPanel1Left Top�Width�Height*AlignalBottomTabOrder TBitBtnBitBtn2Left�TopWidth]HeightCaptionConfermaTabOrder KindbkOK  TBitBtnBitBtn3Left]TopWidth]HeightCaptionAnnullaTabOrderKindbkCancel   TBitBtnBitBtn1LeftXTopqWidthbHeight!Hint fissazione di un nuovo colloquioCaptionNuovo colloquioParentShowHintShowHint	TabOrderOnClickBitBtn1Click   