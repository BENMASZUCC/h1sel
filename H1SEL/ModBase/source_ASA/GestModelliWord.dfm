object GestModelliWordForm: TGestModelliWordForm
  Left = 222
  Top = 104
  BorderStyle = bsDialog
  Caption = 'Gestione modelli Word'
  ClientHeight = 497
  ClientWidth = 562
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 309
    Top = 381
    Width = 222
    Height = 112
    Caption = 
      'IMPORTANTE:'#13#10'- il "Nome Campo" � quello che il programma'#13#10'  trov' +
      'a nel modello Word e sostituisce con il '#13#10'  valore contenuto in ' +
      '"campo " prelevato'#13#10'  dalla tabella o dalle tabelle del database' +
      '.'#13#10'- Perch� il tutto funzioni � necessario che'#13#10'  l'#39'ordine dei c' +
      'ampi sia identico a quello del'#13#10'  modello Word'
    Font.Charset = ANSI_CHARSET
    Font.Color = clPurple
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsItalic]
    ParentFont = False
  end
  object BitBtn1: TBitBtn
    Left = 470
    Top = 5
    Width = 89
    Height = 33
    Caption = 'Esci'
    TabOrder = 0
    Kind = bkOK
  end
  object Panel3: TPanel
    Left = 4
    Top = 251
    Width = 300
    Height = 241
    BevelOuter = bvLowered
    TabOrder = 1
    object RxDBGrid2: TRxDBGrid
      Left = 1
      Top = 20
      Width = 225
      Height = 220
      Align = alClient
      DataSource = DsQCampiModello
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
      ReadOnly = True
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'NomeCampo'
          Title.Caption = 'Nome Campo'
          Width = 79
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Campo'
          Width = 111
          Visible = True
        end>
    end
    object Panel2: TPanel
      Left = 226
      Top = 20
      Width = 73
      Height = 220
      Align = alRight
      BevelOuter = bvLowered
      TabOrder = 1
      object BCampoNew: TToolbarButton97
        Left = 1
        Top = 1
        Width = 71
        Height = 38
        Caption = 'nuovo campo'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          33333333FF33333333FF333993333333300033377F3333333777333993333333
          300033F77FFF3333377739999993333333333777777F3333333F399999933333
          33003777777333333377333993333333330033377F3333333377333993333333
          3333333773333333333F333333333333330033333333F33333773333333C3333
          330033333337FF3333773333333CC333333333FFFFF77FFF3FF33CCCCCCCCCC3
          993337777777777F77F33CCCCCCCCCC3993337777777777377333333333CC333
          333333333337733333FF3333333C333330003333333733333777333333333333
          3000333333333333377733333333333333333333333333333333}
        NumGlyphs = 2
        WordWrap = True
        OnClick = BCampoNewClick
      end
      object BCampoMod: TToolbarButton97
        Left = 1
        Top = 39
        Width = 71
        Height = 38
        Caption = 'modifica campo'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
          000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
          00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
          F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
          0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
          FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
          FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
          0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
          00333377737FFFFF773333303300000003333337337777777333}
        NumGlyphs = 2
        WordWrap = True
        OnClick = BCampoModClick
      end
      object BCampoDel: TToolbarButton97
        Left = 1
        Top = 77
        Width = 71
        Height = 38
        Caption = 'elimina campo'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333000000000
          3333333777777777F3333330F777777033333337F3F3F3F7F3333330F0808070
          33333337F7F7F7F7F3333330F080707033333337F7F7F7F7F3333330F0808070
          33333337F7F7F7F7F3333330F080707033333337F7F7F7F7F3333330F0808070
          333333F7F7F7F7F7F3F33030F080707030333737F7F7F7F7F7333300F0808070
          03333377F7F7F7F773333330F080707033333337F7F7F7F7F333333070707070
          33333337F7F7F7F7FF3333000000000003333377777777777F33330F88877777
          0333337FFFFFFFFF7F3333000000000003333377777777777333333330777033
          3333333337FFF7F3333333333000003333333333377777333333}
        NumGlyphs = 2
        WordWrap = True
        OnClick = BCampoDelClick
      end
    end
    object Panel4: TPanel
      Left = 1
      Top = 1
      Width = 298
      Height = 19
      Align = alTop
      Alignment = taLeftJustify
      Caption = '  Campi da riempire relativi al modello'
      Color = clGray
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      object DBText1: TDBText
        Left = 180
        Top = 3
        Width = 140
        Height = 17
        DataField = 'NomeModello'
        DataSource = DsQModelli
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
  end
  object Panel5: TPanel
    Left = 4
    Top = 4
    Width = 461
    Height = 241
    BevelOuter = bvLowered
    TabOrder = 2
    object Panel6: TPanel
      Left = 387
      Top = 20
      Width = 73
      Height = 220
      Align = alRight
      BevelOuter = bvLowered
      TabOrder = 0
      object ToolbarButton971: TToolbarButton97
        Left = 1
        Top = 1
        Width = 71
        Height = 38
        Caption = 'nuovo modello'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          33333333FF33333333FF333993333333300033377F3333333777333993333333
          300033F77FFF3333377739999993333333333777777F3333333F399999933333
          33003777777333333377333993333333330033377F3333333377333993333333
          3333333773333333333F333333333333330033333333F33333773333333C3333
          330033333337FF3333773333333CC333333333FFFFF77FFF3FF33CCCCCCCCCC3
          993337777777777F77F33CCCCCCCCCC3993337777777777377333333333CC333
          333333333337733333FF3333333C333330003333333733333777333333333333
          3000333333333333377733333333333333333333333333333333}
        NumGlyphs = 2
        WordWrap = True
        OnClick = ToolbarButton971Click
      end
      object ToolbarButton972: TToolbarButton97
        Left = 1
        Top = 39
        Width = 71
        Height = 38
        Caption = 'modifica modello'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
          000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
          00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
          F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
          0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
          FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
          FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
          0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
          00333377737FFFFF773333303300000003333337337777777333}
        NumGlyphs = 2
        WordWrap = True
        OnClick = ToolbarButton972Click
      end
      object ToolbarButton973: TToolbarButton97
        Left = 1
        Top = 77
        Width = 71
        Height = 38
        Caption = 'elimina modello'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333000000000
          3333333777777777F3333330F777777033333337F3F3F3F7F3333330F0808070
          33333337F7F7F7F7F3333330F080707033333337F7F7F7F7F3333330F0808070
          33333337F7F7F7F7F3333330F080707033333337F7F7F7F7F3333330F0808070
          333333F7F7F7F7F7F3F33030F080707030333737F7F7F7F7F7333300F0808070
          03333377F7F7F7F773333330F080707033333337F7F7F7F7F333333070707070
          33333337F7F7F7F7FF3333000000000003333377777777777F33330F88877777
          0333337FFFFFFFFF7F3333000000000003333377777777777333333330777033
          3333333337FFF7F3333333333000003333333333377777333333}
        NumGlyphs = 2
        WordWrap = True
        OnClick = ToolbarButton973Click
      end
      object ToolbarButton974: TToolbarButton97
        Left = 1
        Top = 128
        Width = 71
        Height = 38
        Caption = 'crea modello'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Glyph.Data = {
          36050000424D3605000000000000360400002800000010000000100000000100
          08000000000000010000120B0000120B00000001000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
          A6000020400000206000002080000020A0000020C0000020E000004000000040
          20000040400000406000004080000040A0000040C0000040E000006000000060
          20000060400000606000006080000060A0000060C0000060E000008000000080
          20000080400000806000008080000080A0000080C0000080E00000A0000000A0
          200000A0400000A0600000A0800000A0A00000A0C00000A0E00000C0000000C0
          200000C0400000C0600000C0800000C0A00000C0C00000C0E00000E0000000E0
          200000E0400000E0600000E0800000E0A00000E0C00000E0E000400000004000
          20004000400040006000400080004000A0004000C0004000E000402000004020
          20004020400040206000402080004020A0004020C0004020E000404000004040
          20004040400040406000404080004040A0004040C0004040E000406000004060
          20004060400040606000406080004060A0004060C0004060E000408000004080
          20004080400040806000408080004080A0004080C0004080E00040A0000040A0
          200040A0400040A0600040A0800040A0A00040A0C00040A0E00040C0000040C0
          200040C0400040C0600040C0800040C0A00040C0C00040C0E00040E0000040E0
          200040E0400040E0600040E0800040E0A00040E0C00040E0E000800000008000
          20008000400080006000800080008000A0008000C0008000E000802000008020
          20008020400080206000802080008020A0008020C0008020E000804000008040
          20008040400080406000804080008040A0008040C0008040E000806000008060
          20008060400080606000806080008060A0008060C0008060E000808000008080
          20008080400080806000808080008080A0008080C0008080E00080A0000080A0
          200080A0400080A0600080A0800080A0A00080A0C00080A0E00080C0000080C0
          200080C0400080C0600080C0800080C0A00080C0C00080C0E00080E0000080E0
          200080E0400080E0600080E0800080E0A00080E0C00080E0E000C0000000C000
          2000C0004000C0006000C0008000C000A000C000C000C000E000C0200000C020
          2000C0204000C0206000C0208000C020A000C020C000C020E000C0400000C040
          2000C0404000C0406000C0408000C040A000C040C000C040E000C0600000C060
          2000C0604000C0606000C0608000C060A000C060C000C060E000C0800000C080
          2000C0804000C0806000C0808000C080A000C080C000C080E000C0A00000C0A0
          2000C0A04000C0A06000C0A08000C0A0A000C0A0C000C0A0E000C0C00000C0C0
          2000C0C04000C0C06000C0C08000C0C0A000F0FBFF00A4A0A000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FCFCFCA40000
          00000000000000000000FCFCFCA4070707070707070707070700FCFCFCA4FBFB
          FBFBFBFBFBFBFBFB0700FCFCFCA4FBFBFBFBFB07070707FB0700FCFCFCA4FBFB
          FBFBFBFBFBFBFBFB0700FCFCFCA4FB0707070707070707FB0700FCFCFCA4FBFB
          FBFBFBFBFBFBFBFB0700FC000000A40000FBFB07070707FB0700FC00FEFE00FE
          FE00FB0707FB07FB0700FC00FEFE00FEFE00FB0707FB07FB0700FC00FE00FEFE
          00FE0007FBFB07FB0700FC00FE0000FE0000FE00070707FB070000FEFEFE00FE
          00FEFEFE00FB000000000000000000000000000000FBA4FB00FCFCFCFCA4FBFB
          FBFBFBFBFBFBA400FCFCFCFCFCA4A4A4A4A4A4A4A4A4A4FCFCFC}
        ParentFont = False
        WordWrap = True
        OnClick = ToolbarButton974Click
      end
      object BAprimodello: TToolbarButton97
        Left = 1
        Top = 166
        Width = 71
        Height = 38
        Caption = 'apri modello'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0033333333B333
          333B33FF33337F3333F73BB3777BB7777BB3377FFFF77FFFF77333B000000000
          0B3333777777777777333330FFFFFFFF07333337F33333337F333330FFFFFFFF
          07333337F3FF3FFF7F333330F00F000F07333337F77377737F333330FFFFFFFF
          07333FF7F3FFFF3F7FFFBBB0F0000F0F0BB37777F7777373777F3BB0FFFFFFFF
          0BBB3777F3FF3FFF77773330F00F000003333337F773777773333330FFFF0FF0
          33333337F3FF7F37F3333330F08F0F0B33333337F7737F77FF333330FFFF003B
          B3333337FFFF77377FF333B000000333BB33337777777F3377FF3BB3333BB333
          3BB33773333773333773B333333B3333333B7333333733333337}
        NumGlyphs = 2
        ParentFont = False
        WordWrap = True
        OnClick = BAprimodelloClick
      end
    end
    object Panel7: TPanel
      Left = 1
      Top = 1
      Width = 459
      Height = 19
      Align = alTop
      Alignment = taLeftJustify
      Caption = '  Elenco modelli'
      Color = clGray
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -12
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
    object RxDBGrid1: TRxDBGrid
      Left = 1
      Top = 20
      Width = 386
      Height = 220
      Align = alClient
      DataSource = DsQModelli
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
      ReadOnly = True
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'NomeModello'
          Title.Caption = 'Nome Modello'
          Width = 76
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descrizione'
          Title.Caption = 'Breve descrizione'
          Width = 153
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TabellaMaster'
          Title.Caption = 'Tabella db'
          Width = 73
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'InizialiFileGenerato'
          Title.Caption = 'iniziali file'
          Visible = True
        end>
    end
  end
  object QModelli: TQuery
    Active = True
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select * from ModelliWord')
    Left = 32
    Top = 40
    object QModelliID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.ModelliWord.ID'
    end
    object QModelliNomeModello: TStringField
      FieldName = 'NomeModello'
      Origin = 'EBCDB.ModelliWord.NomeModello'
      FixedChar = True
      Size = 30
    end
    object QModelliDescrizione: TStringField
      FieldName = 'Descrizione'
      Origin = 'EBCDB.ModelliWord.Descrizione'
      FixedChar = True
      Size = 50
    end
    object QModelliTabellaMaster: TStringField
      FieldName = 'TabellaMaster'
      Origin = 'EBCDB.ModelliWord.TabellaMaster'
      FixedChar = True
    end
    object QModelliInizialiFileGenerato: TStringField
      FieldName = 'InizialiFileGenerato'
      Origin = 'EBCDB.ModelliWord.InizialiFileGenerato'
      FixedChar = True
      Size = 3
    end
  end
  object DsQModelli: TDataSource
    DataSet = QModelli
    Left = 32
    Top = 72
  end
  object QCampiModello: TQuery
    Active = True
    DatabaseName = 'EBCDB'
    DataSource = DsQModelli
    SQL.Strings = (
      'select * from ModelliWordCampi'
      'where IDModello=:ID'
      'order by ID')
    Left = 64
    Top = 376
    ParamData = <
      item
        DataType = ftAutoInc
        Name = 'ID'
        ParamType = ptUnknown
      end>
    object QCampiModelloID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.ModelliWordCampi.ID'
    end
    object QCampiModelloIDModello: TIntegerField
      FieldName = 'IDModello'
      Origin = 'EBCDB.ModelliWordCampi.IDModello'
    end
    object QCampiModelloNomeCampo: TStringField
      FieldName = 'NomeCampo'
      Origin = 'EBCDB.ModelliWordCampi.NomeCampo'
      FixedChar = True
    end
    object QCampiModelloCampo: TStringField
      FieldName = 'Campo'
      Origin = 'EBCDB.ModelliWordCampi.Campo'
      FixedChar = True
    end
    object QCampiModelloTipoCampo: TStringField
      FieldName = 'TipoCampo'
      Origin = 'EBCDB.ModelliWordCampi.TipoCampo'
      FixedChar = True
      Size = 10
    end
  end
  object DsQCampiModello: TDataSource
    DataSet = QCampiModello
    Left = 64
    Top = 408
  end
end
