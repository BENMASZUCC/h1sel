unit StoricoRic;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     Grids,DBGrids,StdCtrls,Buttons;

type
     TStoricoRicForm=class(TForm)
          BitBtn1: TBitBtn;
          DBGrid1: TDBGrid;
          BitBtn2: TBitBtn;
          BitBtn3: TBitBtn;
          BitBtn4: TBitBtn;
          procedure BitBtn2Click(Sender: TObject);
          procedure BitBtn3Click(Sender: TObject);
          procedure BitBtn4Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     StoricoRicForm: TStoricoRicForm;

implementation

uses MDRicerche,SelData,ModuloDati;

{$R *.DFM}

procedure TStoricoRicForm.BitBtn2Click(Sender: TObject);
begin
     SelDataForm:=TSelDataForm.create(self);
     SelDataForm.DEData.Date:=DataRicerche.TStoricoRicDallaData.Value;
     SelDataForm.ShowModal;
     if SelDataForm.ModalResult=mrOK then begin
          Data.DB.BeginTrans;
          try
               Data.Q1.Close;
               Data.Q1.SQL.text:='update EBC_StoricoRic set DallaData=:xDallaData '+
                    'where ID='+DataRicerche.TStoricoRicID.AsString;
               Data.Q1.ParambyName('xDallaData').asDateTime:=SelDataForm.DEData.Date;
               Data.Q1.ExecSQL;
               Data.DB.CommitTrans;
               DataRicerche.TStoricoRic.Close;
               DataRicerche.TStoricoRic.Open;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE:  operazione non completata',mtError, [mbOK],0);
          end;
     end;
     SelDataForm.Free;
end;

procedure TStoricoRicForm.BitBtn3Click(Sender: TObject);
var xNote: string;
begin
     xNote:=DataRicerche.TStoricoRicNote.Value;
     if not inputquery('Modifica storico ric.','Note:',xNote) then exit;
     Data.DB.BeginTrans;
     try
          Data.Q1.Close;
          Data.Q1.SQL.text:='update EBC_StoricoRic set Note=:xNote '+
               'where ID='+DataRicerche.TStoricoRicID.AsString;
          Data.Q1.ParambyName('xNote').asString:=xNote;
          Data.Q1.ExecSQL;
          Data.DB.CommitTrans;
          DataRicerche.TStoricoRic.Close;
          DataRicerche.TStoricoRic.Open;
     except
          Data.DB.RollbackTrans;
          MessageDlg('ERRORE SUL DATABASE:  operazione non completata',mtError, [mbOK],0);
     end;
end;

procedure TStoricoRicForm.BitBtn4Click(Sender: TObject);
begin
     if DataRicerche.TStoricoRic.IsEmpty then exit;
     if MessageDlg('Sei sicuro di voler eliminare la riga di storico ?',mtWarning, [mbNo,mbYes],0)=mrYes then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text:='delete from EBC_StoricoRic '+
                         'where ID='+DataRicerche.TStoricoRicID.asString;
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    DataRicerche.TStoricoRic.Close;
                    DataRicerche.TStoricoRic.Open;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               end;
          end;
     end;
end;

procedure TStoricoRicForm.FormShow(Sender: TObject);
begin
     Caption:='[M/215] - '+Caption;
end;

end.

