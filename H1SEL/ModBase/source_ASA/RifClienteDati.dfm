object RifClienteDatiForm: TRifClienteDatiForm
  Left = 318
  Top = 105
  ActiveControl = ETitStudio
  BorderStyle = bsDialog
  Caption = 'Riferimento interno cliente - altri dati'
  ClientHeight = 493
  ClientWidth = 324
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label4: TLabel
    Left = 43
    Top = 53
    Width = 71
    Height = 13
    Caption = 'Titolo di studio:'
  end
  object Label5: TLabel
    Left = 3
    Top = 118
    Width = 81
    Height = 13
    Caption = 'Carica aziendale:'
  end
  object Label6: TLabel
    Left = 3
    Top = 193
    Width = 80
    Height = 13
    Caption = 'Nome segretaria:'
  end
  object Label11: TLabel
    Left = 3
    Top = 447
    Width = 121
    Height = 13
    Caption = 'Utente che lo ha inserito :'
  end
  object Label12: TLabel
    Left = 3
    Top = 471
    Width = 82
    Height = 13
    Caption = 'Data inserimento:'
  end
  object Label13: TLabel
    Left = 5
    Top = 53
    Width = 29
    Height = 13
    Caption = 'Sesso'
  end
  object Label15: TLabel
    Left = 3
    Top = 156
    Width = 94
    Height = 13
    Caption = 'Divisione aziendale:'
  end
  object BitBtn1: TBitBtn
    Left = 229
    Top = 2
    Width = 93
    Height = 35
    TabOrder = 7
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 229
    Top = 38
    Width = 93
    Height = 35
    Caption = 'Annulla'
    TabOrder = 8
    Kind = bkCancel
  end
  object Panel1: TPanel
    Left = 3
    Top = 4
    Width = 222
    Height = 45
    BevelOuter = bvLowered
    Enabled = False
    TabOrder = 9
    object Label1: TLabel
      Left = 6
      Top = 4
      Width = 56
      Height = 13
      Caption = 'Nominativo:'
    end
    object ENominativo: TEdit
      Left = 5
      Top = 19
      Width = 212
      Height = 21
      TabStop = False
      Color = clAqua
      TabOrder = 0
    end
  end
  object GroupBox1: TGroupBox
    Left = 3
    Top = 256
    Width = 178
    Height = 45
    Caption = 'Compleanno:'
    TabOrder = 5
    object Label2: TLabel
      Left = 8
      Top = 20
      Width = 34
      Height = 13
      Caption = 'Giorno:'
    end
    object Label3: TLabel
      Left = 95
      Top = 20
      Width = 29
      Height = 13
      Caption = 'Mese:'
    end
    object SEGiorno: TSpinEdit
      Left = 46
      Top = 17
      Width = 41
      Height = 22
      MaxValue = 31
      MinValue = 0
      TabOrder = 0
      Value = 0
    end
    object SEMese: TSpinEdit
      Left = 133
      Top = 17
      Width = 38
      Height = 22
      MaxValue = 12
      MinValue = 0
      TabOrder = 1
      Value = 0
    end
  end
  object Panel2: TPanel
    Left = 3
    Top = 234
    Width = 222
    Height = 21
    Alignment = taLeftJustify
    Caption = '  Dati personali'
    Color = clGray
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 10
  end
  object Panel3: TPanel
    Left = 3
    Top = 95
    Width = 222
    Height = 21
    Alignment = taLeftJustify
    Caption = '  Dati aziendali'
    Color = clGray
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 11
  end
  object ETitStudio: TEdit
    Left = 43
    Top = 68
    Width = 182
    Height = 21
    MaxLength = 30
    TabOrder = 1
  end
  object ECaricaAz: TEdit
    Left = 3
    Top = 133
    Width = 222
    Height = 21
    MaxLength = 40
    TabOrder = 2
  end
  object ENomeSeg: TEdit
    Left = 3
    Top = 208
    Width = 222
    Height = 21
    MaxLength = 40
    TabOrder = 4
  end
  object GroupBox2: TGroupBox
    Left = 3
    Top = 304
    Width = 222
    Height = 108
    Caption = 'Residenza privata'
    TabOrder = 6
    object Label7: TLabel
      Left = 8
      Top = 15
      Width = 38
      Height = 13
      Caption = 'Indirizzo'
    end
    object Label8: TLabel
      Left = 8
      Top = 60
      Width = 19
      Height = 13
      Caption = 'Cap'
    end
    object Label10: TLabel
      Left = 80
      Top = 59
      Width = 25
      Height = 13
      Caption = 'Prov.'
    end
    object Label9: TLabel
      Left = 8
      Top = 84
      Width = 45
      Height = 13
      Caption = 'Telefono:'
    end
    object Label14: TLabel
      Left = 6
      Top = 17
      Width = 53
      Height = 13
      Caption = 'Tipo strada'
    end
    object Label93: TLabel
      Left = 63
      Top = 17
      Width = 38
      Height = 13
      Caption = 'Indirizzo'
    end
    object Label99: TLabel
      Left = 178
      Top = 17
      Width = 30
      Height = 13
      Caption = 'n� civ.'
    end
    object Label16: TLabel
      Left = 140
      Top = 59
      Width = 39
      Height = 13
      Caption = 'Nazione'
    end
    object EIndirizzo: TEdit
      Left = 63
      Top = 32
      Width = 113
      Height = 21
      MaxLength = 50
      TabOrder = 1
    end
    object ECap: TEdit
      Left = 33
      Top = 55
      Width = 41
      Height = 21
      MaxLength = 5
      TabOrder = 3
    end
    object EProv: TEdit
      Left = 107
      Top = 56
      Width = 27
      Height = 21
      CharCase = ecUpperCase
      MaxLength = 2
      TabOrder = 4
    end
    object ETelAb: TEdit
      Left = 58
      Top = 80
      Width = 156
      Height = 21
      MaxLength = 40
      TabOrder = 6
    end
    object CBTipoStradaRes: TComboBox
      Left = 8
      Top = 32
      Width = 54
      Height = 21
      ItemHeight = 13
      TabOrder = 0
      OnDropDown = CBTipoStradaResDropDown
    end
    object ENumCivico: TEdit
      Left = 178
      Top = 32
      Width = 36
      Height = 21
      TabOrder = 2
    end
    object ENazione: TEdit
      Left = 183
      Top = 56
      Width = 31
      Height = 21
      CharCase = ecUpperCase
      MaxLength = 3
      TabOrder = 5
    end
  end
  object Panel4: TPanel
    Left = 3
    Top = 418
    Width = 222
    Height = 21
    Alignment = taLeftJustify
    Caption = '  Nostri dati:'
    Color = clGray
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 12
  end
  object EUtente: TEdit
    Left = 124
    Top = 444
    Width = 102
    Height = 21
    TabStop = False
    Enabled = False
    TabOrder = 13
  end
  object DEDataIns: TDateEdit97
    Left = 124
    Top = 467
    Width = 103
    Height = 21
    TabStop = False
    Enabled = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 14
    ColorCalendar.ColorValid = clBlue
    DayNames.Monday = 'lu'
    DayNames.Tuesday = 'ma'
    DayNames.Wednesday = 'me'
    DayNames.Thursday = 'gi'
    DayNames.Friday = 've'
    DayNames.Saturday = 'sa'
    DayNames.Sunday = 'do'
    MonthNames.January = 'gennaio'
    MonthNames.February = 'febbraio'
    MonthNames.March = 'marzo'
    MonthNames.April = 'aprile'
    MonthNames.May = 'maggio'
    MonthNames.June = 'giugno'
    MonthNames.July = 'luglio'
    MonthNames.August = 'agosto'
    MonthNames.September = 'settembre'
    MonthNames.October = 'ottobre'
    MonthNames.November = 'novembre'
    MonthNames.December = 'dicembre'
    Options = [doButtonTabStop, doCanPopup, doIsMasked, doShowCancel, doShowToday]
  end
  object CBSesso: TComboBox
    Left = 4
    Top = 68
    Width = 37
    Height = 21
    ItemHeight = 13
    TabOrder = 0
    Items.Strings = (
      'M'
      'F')
  end
  object EDivisioneAz: TEdit
    Left = 3
    Top = 171
    Width = 222
    Height = 21
    MaxLength = 40
    TabOrder = 3
  end
  object QAltriDati: TQuery
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select EBC_ContattiClienti.*, Users.Nominativo'
      'from EBC_ContattiClienti,Users with (updlock)'
      'where EBC_ContattiClienti.IDUtente *= Users.ID'
      '  and EBC_ContattiClienti.ID=:xID')
    Left = 195
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'xID'
        ParamType = ptUnknown
      end>
  end
end
