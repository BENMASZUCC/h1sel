unit SceltaModelloAnag;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, Db, DBTables, StdCtrls, Buttons, ExtCtrls;

type
  TSceltaModelloAnagForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    DsModelliWord: TDataSource;
    DBGrid1: TDBGrid;
    QModelliWord: TQuery;
    QModelliWordID: TAutoIncField;
    QModelliWordNomeModello: TStringField;
    QModelliWordDescrizione: TStringField;
    QModelliWordTabellaMaster: TStringField;
    QModelliWordInizialiFileGenerato: TStringField;
    CBSalva: TCheckBox;
    BitBtn3: TBitBtn;
    RGOpzione: TRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SceltaModelloAnagForm: TSceltaModelloAnagForm;

implementation

uses GestModelliWord;

{$R *.DFM}

procedure TSceltaModelloAnagForm.FormCreate(Sender: TObject);
begin
     Height:=330;
end;

procedure TSceltaModelloAnagForm.BitBtn3Click(Sender: TObject);
begin
     GestModelliWordForm:=TGestModelliWordForm.create(self);
     GestModelliWordForm.ShowModal;
     QModelliWord.Close;
     QModelliWord.Open;
     GestModelliWordForm.Free;
end;

procedure TSceltaModelloAnagForm.BitBtn1Click(Sender: TObject);
begin
     if QModelliWord.IsEmpty then begin
        MessageDlg('Nessun modello disponibile: IMPOSSIBILE PROSEGUIRE'+chr(13)+
                   'Premere "Annulla" o creare un modello usando il pulsante "Modelli"',mtError,[mbOK],0);
        Abort;
     end;
end;

procedure TSceltaModelloAnagForm.FormShow(Sender: TObject);
begin
     Caption:='[M/072] - '+Caption;
end;

end.
