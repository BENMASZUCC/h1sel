unit SelContatti;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, Db, DBTables, StdCtrls, Buttons;

type
  TSelContattiForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    QContatti: TQuery;
    DsQContatti: TDataSource;
    DBGrid1: TDBGrid;
    QContattiID: TAutoIncField;
    QContattiContatto: TStringField;
    QContattiTelefono: TStringField;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SelContattiForm: TSelContattiForm;

implementation

{$R *.DFM}

procedure TSelContattiForm.FormShow(Sender: TObject);
begin
     Caption:='[S/36] - '+Caption;
end;

end.
