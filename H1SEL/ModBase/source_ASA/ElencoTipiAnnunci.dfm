object ElencoTipiAnnunciForm: TElencoTipiAnnunciForm
  Left = 299
  Top = 103
  BorderStyle = bsDialog
  Caption = 'Tipi di annunci'
  ClientHeight = 282
  ClientWidth = 449
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 449
    Height = 40
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object BitBtn1: TBitBtn
      Left = 271
      Top = 3
      Width = 85
      Height = 33
      TabOrder = 0
      Kind = bkOK
    end
    object BitBtn2: TBitBtn
      Left = 359
      Top = 3
      Width = 85
      Height = 33
      Caption = 'Annulla'
      TabOrder = 1
      Kind = bkCancel
    end
  end
  inline FrmTipiAnnunci: TFrmTipiAnnunci
    Top = 40
    Width = 449
    Height = 242
    Align = alClient
    TabOrder = 1
    inherited Panel1: TPanel
      Left = 369
      Height = 242
    end
    inherited RxDBGrid1: TRxDBGrid
      Width = 369
      Height = 242
    end
  end
end
