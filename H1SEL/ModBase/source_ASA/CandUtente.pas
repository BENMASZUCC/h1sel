unit CandUtente;

interface

uses 
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables, Grids, DBGrids, ExtCtrls, RXDBCtrl, TB97, StdCtrls, Buttons;

type
  TCandUtenteFrame = class(TFrame)
    QCandUtente: TQuery;
    Panel12: TPanel;
    Panel1: TPanel;
    DsQCandUtente: TDataSource;
    QCandUtenteID: TAutoIncField;
    QCandUtenteCVNumero: TIntegerField;
    QCandUtenteCognome: TStringField;
    QCandUtenteNome: TStringField;
    QCandUtenteDataNascita: TDateTimeField;
    QCandUtenteRecapitiTelefonici: TStringField;
    QCandUtenteCellulare: TStringField;
    RxDBGrid1: TRxDBGrid;
    QCandUtenteggDataUltimoColloquio: TIntegerField;
    BLegenda: TBitBtn;
    QCandUtenteggDataUltimoContatto: TIntegerField;
    procedure RxDBGrid1TitleClick(Column: TColumn);
    procedure RxDBGrid1GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure BLegendaClick(Sender: TObject);
  private
    xggDiffCol,xggDiffUc:integer;
  public
    xIDUtente:integer;
    xColumnRic:TColumn;
  end;

implementation

uses ModuloDati, Main, LegendaCandUtente;

{$R *.DFM}

procedure TCandUtenteFrame.RxDBGrid1TitleClick(Column: TColumn);
begin
     QCandUtente.Close;
     QCandUtente.SQL.text:='select Anagrafica.ID,CVNumero,Cognome,Nome,DataNascita,RecapitiTelefonici,Cellulare, '+
                           'DATEDIFF(day, EBC_Colloqui.Data, getdate()) ggDataUltimoColloquio, '+
                           'DATEDIFF(day, EBC_ContattiCandidati.Data, getdate()) ggDataUltimoContatto '+
                           'from Anagrafica,EBC_Colloqui,EBC_ContattiCandidati '+
                           'where Anagrafica.ID *= EBC_Colloqui.IDAnagrafica '+
                           'and EBC_Colloqui.Data=(select max(Data) from EBC_Colloqui where Anagrafica.ID *= EBC_Colloqui.IDAnagrafica) '+
                           'and Anagrafica.ID *= EBC_ContattiCandidati.IDAnagrafica '+
                           'and EBC_ContattiCandidati.Data=(select max(Data) from EBC_ContattiCandidati '+
                           'where Anagrafica.ID *= EBC_ContattiCandidati.IDAnagrafica) '+
                           'and IDUtente=:xIDUtente';
     if Column=RxDBGrid1.Columns[0] then
        QCandUtente.SQL.Add('order by CVNumero');
     if Column=RxDBGrid1.Columns[1] then
        QCandUtente.SQL.Add('order by Cognome,Nome');
     if Column=RxDBGrid1.Columns[3] then
        QCandUtente.SQL.Add('order by DataNascita');
     QCandUtente.ParamByName('xIDUtente').asInteger:=xIDUtente;
     QCandUtente.Open;
end;

procedure TCandUtenteFrame.RxDBGrid1GetCellParams(Sender: TObject;
  Field: TField; AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
     // trova parametro utente
     Data.Q1.Close;
     Data.Q1.SQL.text:='select ggDiffCol,ggDiffUc from Users where ID='+IntToStr(MainForm.xIDUtenteAttuale);
     Data.Q1.Open;
     xggDiffCol:=Data.Q1.FieldByName('ggDiffCol').asInteger;
     xggDiffUc:=Data.Q1.FieldByName('ggDiffUc').asInteger;
     Data.Q1.Close;
     if (Field.FieldName='ggDataUltimoColloquio')and(QCandUtenteggDataUltimoColloquio.Value>=xggDiffCol)
          then begin Afont.Color:=clRed; Afont.Style:= [fsBold]; end; //Background:=clAqua;
     if (Field.FieldName='ggDataUltimoContatto')and(QCandUtenteggDataUltimoContatto.Value>=xggDiffUc)
          then begin Afont.Color:=clFuchsia; Afont.Style:= [fsBold]; end; //Background:=clAqua;
end;

procedure TCandUtenteFrame.BLegendaClick(Sender: TObject);
begin
     LegendaCandUtenteForm:=TLegendacandUtenteForm.create(self);
     LegendacandUtenteForm.ShowModal;
     LegendacandUtenteForm.Free;

     Data.Q1.Close;
     Data.Q1.SQL.text:='select ggDiffCol,ggDiffUc from Users where ID='+IntToStr(MainForm.xIDUtenteAttuale);
     Data.Q1.Open;
     xggDiffCol:=Data.Q1.FieldByName('ggDiffCol').asInteger;
     xggDiffUc:=Data.Q1.FieldByName('ggDiffUc').asInteger;
     Data.Q1.Close;

     // refresh griglia
     RxDBGrid1TitleClick(xColumnRic);
end;

end.
