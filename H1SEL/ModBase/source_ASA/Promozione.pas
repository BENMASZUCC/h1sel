unit Promozione;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, StdCtrls, Mask, DBCtrls, ExtCtrls, Db, DBTables, DtEdit97,
  Grids, DBGrids;

type
  TPromozioneForm = class(TForm)
    Panel1: TPanel;
    Panel17: TPanel;
    DBEdit12: TDBEdit;
    DBEdit14: TDBEdit;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    GroupBox1: TGroupBox;
    EPosizNext: TEdit;
    BPosizNext: TSpeedButton;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    SpeedButton3: TSpeedButton;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    TAnag: TTable;
    DsAnag: TDataSource;
    TAnagID: TAutoIncField;
    TAnagCognome: TStringField;
    TAnagNome: TStringField;
    DEData: TDateEdit97;
    Label1: TLabel;
    TPosizNow: TTable;
    TPosizNowID: TAutoIncField;
    TPosizNowDescrizione: TStringField;
    TPosizNowDescBreve: TStringField;
    TPosizNowNodoPadre: TIntegerField;
    DsPosizNow: TDataSource;
    TPosizNowIDDipendente: TIntegerField;
    DBGrid1: TDBGrid;
    TOrgABS: TTable;
    TOrgABSID: TAutoIncField;
    TOrgABSIDDipendente: TIntegerField;
    procedure BPosizNextClick(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    xIDPosizNext:integer;
  end;

var
  PromozioneForm: TPromozioneForm;

implementation

uses ModuloDati, VediOrg, ElencoDipAttrib;

{$R *.DFM}

procedure TPromozioneForm.BPosizNextClick(Sender: TObject);
begin
     VediOrgForm:=TVediOrgForm.create(self);
     VediOrgForm.TAnagABS.FindKey([Data.TAnagraficaID.Value]);
     VediOrgForm.BCancel.Visible:=True;
     VediOrgForm.AggiornaTV;
     VediOrgForm.TrovaNodo;
     VediOrgForm.ShowModal;
     if VediOrgForm.ModalResult=mrOK then begin
        if VediOrgForm.TAbsOrgIDDipendente.asString='' then
           xIDPosizNext:=VediOrgForm.TAbsOrgID.Value
        else MessageDlg('Posizione gi� occupata: operazione non consentita',mtError,[mbOK],0);
     end;
     VediOrgForm.Free;
end;

procedure TPromozioneForm.SpeedButton3Click(Sender: TObject);
begin
     ElencoDipAttribForm:=TElencoDipAttribForm.create(self);
     ElencoDipAttribForm.ShowModal;
     if ElencoDipAttribForm.ModalResult=mrOK then begin
        TAnag.Open;
        TAnag.FindKey([ElencoDipAttribForm.QAnagID.Value]);
     end;
     ElencoDipAttribForm.Free;
end;

procedure TPromozioneForm.FormShow(Sender: TObject);
begin
     DEData.Date:=Date;
end;

procedure TPromozioneForm.BitBtn1Click(Sender: TObject);
begin
     if xIDPosizNext=null then begin
        MessageDlg('� necessario indicare la posizione successiva',mtError,[mbOK],0);
        ModalResult:=mrNone;
        Abort;
     end;
end;

end.
