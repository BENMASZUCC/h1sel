unit ZoneComuni;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  TB97, Grids, DBGrids, Db, DBTables, StdCtrls, Buttons, ExtCtrls;

type
  TZoneComuniForm = class(TForm)
    TZone: TTable;
    TComuniZona: TTable;
    DsZone: TDataSource;
    DSComuniZona: TDataSource;
    TZoneID: TAutoIncField;
    TZoneZona: TStringField;
    DBGrid1: TDBGrid;
    TComuniZonaID: TAutoIncField;
    TComuniZonaCodice: TStringField;
    TComuniZonaDescrizione: TStringField;
    TComuniZonaProv: TStringField;
    TComuniZonaCap: TStringField;
    TComuniZonaPrefisso: TStringField;
    TComuniZonaIDZona: TIntegerField;
    DBGrid2: TDBGrid;
    ToolbarButton971: TToolbarButton97;
    Panel73: TPanel;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    procedure ToolbarButton971Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ZoneComuniForm: TZoneComuniForm;

implementation

uses ModuloDati;

{$R *.DFM}

procedure TZoneComuniForm.ToolbarButton971Click(Sender: TObject);
begin
     if MessageDlg('Sei sicuro di voler eliminare l''associazione ?',mtWarning,
           [mbNo,mbYes],0)=mrYes then begin
        TComuniZona.Edit;
        TComuniZonaIDZona.asString:='';
        TComuniZona.Post;
        // modifica Anagrafica per quel comune (sia residenza che domicilio)
        Data.QTemp.Close;
        Data.QTemp.SQL.Clear;
        Data.QTemp.SQL.Add('update Anagrafica set IDZonaDom=null');
        Data.QTemp.SQL.Add('where IDComuneDom='+TComuniZonaID.asString);
        Data.QTemp.ExecSQL;
        Data.QTemp.SQL.Clear;
        Data.QTemp.SQL.Add('update Anagrafica set IDZonaRes=null');
        Data.QTemp.SQL.Add('where IDComuneRes='+TComuniZonaID.asString);
        Data.QTemp.ExecSQL;
     end;
end;

procedure TZoneComuniForm.FormShow(Sender: TObject);
begin
     Caption:='[S/45] - '+Caption;
end;

end.
