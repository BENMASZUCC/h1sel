unit NuovoSoggetto;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     StdCtrls,Buttons,ExtCtrls;

type
     TNuovoSoggettoForm=class(TForm)
          BitBtn1: TBitBtn;
          BitBtn2: TBitBtn;
          Label1: TLabel;
          Label2: TLabel;
          Label3: TLabel;
          Label4: TLabel;
          ECogn: TEdit;
          ENome: TEdit;
          CBSesso: TComboBox;
          ENote: TEdit;
          Label5: TLabel;
          ERuolo: TEdit;
          SpeedButton1: TSpeedButton;
          procedure BitBtn1Click(Sender: TObject);
          procedure SpeedButton1Click(Sender: TObject);
          procedure FormShow(Sender: TObject);
     private
          { Private declarations }
     public
          xIDRuolo,xIDArea: integer;
     end;

var
     NuovoSoggettoForm: TNuovoSoggettoForm;

implementation

uses ModuloDati,InsRuolo;

{$R *.DFM}

procedure TNuovoSoggettoForm.BitBtn1Click(Sender: TObject);
begin
     // controllo omonimia
     with Data do begin
          Q1.Close;
          Q1.SQL.Text:='select ID,CVNumero from Anagrafica where Cognome=:x and Nome=:y';
          Q1.Params[0].asString:=trim(ECogn.Text);
          Q1.Params[1].asString:=trim(ENOme.Text);
          Q1.Open;
          if Q1.RecordCount>0 then begin
               Modalresult:=mrNone;
               if MessageDlg('CONTROLLARE POSSIBILE OMONIMIA con CV n� '+Q1.FieldByName('CVNumero').asString+chr(13)+
                    'Registrare comunque ?',mtError, [mbYes,mbNo],0)=mrNo then begin
                    Abort;
               end else Modalresult:=mrOK;
          end;
     end;
end;

procedure TNuovoSoggettoForm.SpeedButton1Click(Sender: TObject);
begin
     InsRuoloForm:=TInsRuoloForm.create(self);
     InsRuoloForm.CBInsComp.Visible:=False;
     InsRuoloForm.ShowModal;
     if InsRuoloForm.ModalResult=mrOK then begin
          xIDRuolo:=InsRuoloForm.TRuoliID.Value;
          xIDArea:=InsRuoloForm.TAreeID.Value;
          ERuolo.text:=InsRuoloForm.TRuoliDescrizione.Value;
     end else begin
          xIDRuolo:=0;
          xIDArea:=0;
          ERuolo.text:='';
     end;
     InsRuoloForm.Free;
end;

procedure TNuovoSoggettoForm.FormShow(Sender: TObject);
begin
     xIDRuolo:=0;
     xIDArea:=0;
end;

end.

