unit AnagAnnunci;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     dxDBGrid,dxDBTLCl,dxGrClms,dxTL,dxDBCtrl,dxCntner,Db,DBTables,
     StdCtrls,Buttons,ExtCtrls,TB97;

type
     TAnagAnnunciForm=class(TForm)
          Panel1: TPanel;
          BitBtn1: TBitBtn;
          QAnagAnnunci: TQuery;
          DsQAnagAnnunci: TDataSource;
          QAnagAnnunciID: TAutoIncField;
          QAnagAnnunciIDAnagrafica: TIntegerField;
          QAnagAnnunciIDAnnEdizData: TIntegerField;
          QAnagAnnunciDataPervenuto: TDateTimeField;
          QAnagAnnunciIDMansione: TIntegerField;
          QAnagAnnunciIDRicerca: TIntegerField;
          QAnagAnnunciIDEmailAnnunciRic: TIntegerField;
          QAnagAnnunciProgressivo: TStringField;
          QAnagAnnunciRuolo: TStringField;
          QAnagAnnunciData: TDateTimeField;
          QAnagAnnunciRif: TStringField;
          QAnagAnnunciNomeEdizione: TStringField;
          QAnagAnnunciTestata: TStringField;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1ID: TdxDBGridMaskColumn;
          dxDBGrid1IDAnagrafica: TdxDBGridMaskColumn;
          dxDBGrid1IDAnnEdizData: TdxDBGridMaskColumn;
          dxDBGrid1DataPervenuto: TdxDBGridDateColumn;
          dxDBGrid1IDMansione: TdxDBGridMaskColumn;
          dxDBGrid1IDRicerca: TdxDBGridMaskColumn;
          dxDBGrid1IDEmailAnnunciRic: TdxDBGridMaskColumn;
          dxDBGrid1Progressivo: TdxDBGridMaskColumn;
          dxDBGrid1Ruolo: TdxDBGridMaskColumn;
          dxDBGrid1Data: TdxDBGridDateColumn;
          dxDBGrid1Rif: TdxDBGridMaskColumn;
          dxDBGrid1NomeEdizione: TdxDBGridMaskColumn;
          dxDBGrid1Testata: TdxDBGridMaskColumn;
          QAnagAnnunciCodPubb: TStringField;
          dxDBGrid1CodPubb: TdxDBGridColumn;
          ToolbarButton971: TToolbarButton97;
          ToolbarButton972: TToolbarButton97;
          procedure ToolbarButton971Click(Sender: TObject);
          procedure ToolbarButton972Click(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     AnagAnnunciForm: TAnagAnnunciForm;

implementation

uses ModuloDati,CercaAnnuncio,SelFromQuery;

{$R *.DFM}

procedure TAnagAnnunciForm.ToolbarButton971Click(Sender: TObject);
var xIDRicerca,xIDMansione: integer;
begin
     CercaAnnuncioForm:=TCercaAnnuncioForm.create(self);
     CercaAnnuncioForm.showModal;
     if CercaAnnuncioForm.ModalResult=mrOK then begin
          // rif. ricerca
          xIDRicerca:=0;
          xIDMansione:=0;
          Data.QTemp.Close;
          Data.QTemp.SQL.text:='select Ann_AnnunciRicerche.ID,IDRicerca,EBC_Ricerche.IDMansione,Codice,EBC_Ricerche.Progressivo Rif, '+
               'EBC_Ricerche.NumRicercati Num,Mansioni.Descrizione Ruolo,EBC_Clienti.Descrizione Cliente '+
               'from Ann_AnnunciRicerche,EBC_Ricerche,Mansioni,EBC_Clienti '+
               'where Ann_AnnunciRicerche.IDRicerca=EBC_Ricerche.ID and EBC_Ricerche.IDMansione=Mansioni.ID '+
               'and EBC_Ricerche.IDCliente=EBC_Clienti.ID and Ann_AnnunciRicerche.IDAnnuncio='+CercaAnnuncioForm.QAnnAttiviID.asString;
          Data.QTemp.Open;
          if Data.QTemp.RecordCount>1 then begin
               SelFromQueryForm:=TSelFromQueryForm.create(self);
               SelFromQueryForm.QGen.SQL.text:=Data.QTemp.SQL.text;
               SelFromQueryForm.QGen.Open;
               SelFromQueryForm.xNumInvisibleCols:=3;
               SelFromQueryForm.Caption:='seleziona ricerca/commessa associata all''annuncio';
               SelFromQueryForm.ShowModal;
               if SelFromQueryForm.ModalResult=mrOK then begin
                    xIDRicerca:=SelFromQueryForm.QGen.FieldByName('IDRicerca').asInteger;
                    xIDMansione:=SelFromQueryForm.QGen.FieldByName('IDMansione').asInteger;
               end;
               SelFromQueryForm.Free;
          end else begin
               xIDRicerca:=Data.QTemp.FieldByName('IDRicerca').asInteger;
               xIDMansione:=Data.QTemp.FieldByName('IDMansione').asInteger;
          end;
          Data.QTemp.Close;
          // rif ruolo
          Data.QTemp.SQL.text:='select Ann_AnnunciRuoli.ID,Codice,IDMansione, Mansioni.Descrizione Ruolo '+
               'from Ann_AnnunciRuoli,Mansioni where Ann_AnnunciRuoli.IDMansione=Mansioni.ID '+
               'and Ann_AnnunciRuoli.IDAnnuncio='+CercaAnnuncioForm.QAnnAttiviID.asString;
          Data.QTemp.Open;
          if Data.QTemp.RecordCount>1 then begin
               SelFromQueryForm:=TSelFromQueryForm.create(self);
               SelFromQueryForm.QGen.SQL.text:=Data.QTemp.SQL.text;
               SelFromQueryForm.QGen.Open;
               SelFromQueryForm.xNumInvisibleCols:=1;
               SelFromQueryForm.Caption:='seleziona ruolo associata all''annuncio';
               SelFromQueryForm.ShowModal;
               if SelFromQueryForm.ModalResult=mrOK then begin
                    xIDMansione:=SelFromQueryForm.QGen.FieldByName('IDMansione').asInteger;
               end;
               SelFromQueryForm.Free;
          end else begin
              if Data.QTemp.RecordCount=1 then
                 xIDMansione:=Data.QTemp.FieldByName('IDMansione').asInteger;
          end;

          with Data do begin
               DB.BeginTrans;
               try
                    Q1.SQL.text:='insert into Ann_AnagAnnEdizData (IDAnagrafica,IDAnnEdizData,IDMansione,IDRicerca) '+
                         'values (:xIDAnagrafica,:xIDAnnEdizData,:xIDMansione,:xIDRicerca)';
                    Q1.ParambyName('xIDAnagrafica').asInteger:=Data.TAnagraficaID.Value;
                    Q1.ParambyName('xIDAnnEdizData').asInteger:=CercaAnnuncioForm.QAnnAttiviIDAnnEdizData.Value;
                    Q1.ParambyName('xIDMansione').asInteger:=xIDMansione;
                    Q1.ParambyName('xIDRicerca').asInteger:=xIDRicerca;
                    Q1.ExecSQL;
                    QAnagAnnunci.Close;
                    QAnagAnnunci.Open;
                    DB.CommitTrans;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: operazione non effettuata',mtError, [mbOK],0);
               end;
          end;
     end;
     CercaAnnuncioForm.Free;
end;

procedure TAnagAnnunciForm.ToolbarButton972Click(Sender: TObject);
begin
     if QAnagAnnunci.IsEmpty then exit;
     if MessageDlg('Sei sicuro di voler cancellare l''associazione ?',mtWarning, [mbNo,mbYes],0)=mrNo then exit;
     Data.DB.BeginTrans;
     try
          Data.Q1.SQL.text:='delete from Ann_AnagAnnEdizData where ID='+QAnagAnnunciID.asString;
          Data.Q1.ExecSQL;
          Data.DB.CommitTrans;
          QAnagAnnunci.Close;
          QAnagAnnunci.Open;
     except
          Data.DB.RollbackTrans;
          MessageDlg('ERRORE SUL DATABASE:  operazione non completata',mtError, [mbOK],0);
     end;
end;

end.

