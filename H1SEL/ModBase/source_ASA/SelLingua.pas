unit SelLingua;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, StdCtrls, Buttons, Db, DBTables, ExtCtrls;

type
  TSelLinguaForm = class(TForm)
    Lingue: TTable;
    DataSource1: TDataSource;
    LingueID: TAutoIncField;
    LingueLingua: TStringField;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    DBGrid1: TDBGrid;
    Timer1: TTimer;
    procedure DBGrid1KeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
    xStringa:string;
  public
    { Public declarations }
  end;

var
  SelLinguaForm: TSelLinguaForm;

implementation

{$R *.DFM}

procedure TSelLinguaForm.DBGrid1KeyPress(Sender: TObject; var Key: Char);
begin
     Timer1.Enabled:=False;
     if Key=chr(13) then begin
        BitBtn1.Click;
     end else begin
        xStringa:=xStringa+key;
        Lingue.FindNearest([xStringa]);
     end;
     Timer1.Enabled:=True;
end;

procedure TSelLinguaForm.FormShow(Sender: TObject);
begin
     Caption:='[S/39] - '+Caption;
     xStringa:='';
end;

procedure TSelLinguaForm.Timer1Timer(Sender: TObject);
begin
     xStringa:='';
end;

end.
