unit TipiMailCand;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     TB97, StdCtrls, Buttons, DBCtrls, Mask, Db, ExtCtrls, Grids, DBGrids,
     DBTables;

type
     TTipiMailCandForm=class(TForm)
          QTipiCand: TQuery;
          DsQTipiCand: TDataSource;
          UpdTipi: TUpdateSQL;
          DBGrid1: TDBGrid;
          Panel1: TPanel;
          QTipiCandID: TAutoIncField;
          QTipiCandDescrizione: TStringField;
          QTipiCandIntestazione: TMemoField;
          QTipiCandCorpo: TMemoField;
          QTipiCandOggetto: TStringField;
          QTipiCandInfoRicerca: TBooleanField;
          Label1: TLabel;
          DBEdit1: TDBEdit;
          Label2: TLabel;
          DBMemo1: TDBMemo;
          Label3: TLabel;
          DBMemo2: TDBMemo;
          Label4: TLabel;
          DBEdit2: TDBEdit;
          DBCheckBox1: TDBCheckBox;
          BitBtn1: TBitBtn;
          Panel2: TPanel;
          BNew: TToolbarButton97;
          Bdel: TToolbarButton97;
          BOK: TToolbarButton97;
          BCan: TToolbarButton97;
    QTipiCandAddSpecifiche: TBooleanField;
    DBCheckBox2: TDBCheckBox;
          procedure DsQTipiCandStateChange(Sender: TObject);
          procedure BOKClick(Sender: TObject);
          procedure BCanClick(Sender: TObject);
          procedure BNewClick(Sender: TObject);
          procedure BdelClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
     private
    { Private declarations }
     public
    { Public declarations }
     end;

var
     TipiMailCandForm: TTipiMailCandForm;

implementation

uses ModuloDati;

{$R *.DFM}

procedure TTipiMailCandForm.DsQTipiCandStateChange(Sender: TObject);
var b: boolean;
begin
     b:=DsQTipiCand.State in [dsEdit, dsInsert];
     BNew.Enabled:=not b;
     BDel.Enabled:=not b;
     BOK.Enabled:=b;
     BCan.Enabled:=b;
end;

procedure TTipiMailCandForm.BOKClick(Sender: TObject);
begin
     with QTipiCand do begin
          Data.DB.BeginTrans;
          try
               ApplyUpdates;
               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               raise;
          end;
          CommitUpdates;
     end;
end;

procedure TTipiMailCandForm.BCanClick(Sender: TObject);
begin
     QTipiCand.Cancel;
end;

procedure TTipiMailCandForm.BNewClick(Sender: TObject);
var xDesc: string;
begin
     if InputQuery('Nuovo tipo di mail', 'Descrizione:', xDesc) then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text:='insert into TipiMailCand (Descrizione) '+
                         'values (:xDescrizione)';
                    Q1.ParamByName('xDescrizione').asString:=xDesc;
                    Q1.ExecSQL;
                    DB.CommitTrans;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               end;
          end;
          QTipiCand.Close;
          QTipiCand.Open;
          QTipiCand.Last;
     end;
end;

procedure TTipiMailCandForm.BdelClick(Sender: TObject);
begin
     if MessageDlg('Sei sicuro di voler eliminare questo tipo ?', mtWarning, [mbYes, mbNo], 0)=mrNo then Exit;
     with Data do begin
          DB.BeginTrans;
          try
               Q1.Close;
               Q1.SQL.text:='delete from tipiMailCand where ID='+QTipiCandID.asString;
               Q1.ExecSQL;
               DB.CommitTrans;
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
          end;
     end;
     QTipiCand.Close;
     QTipiCand.Open;
end;

procedure TTipiMailCandForm.FormShow(Sender: TObject);
begin
     Caption:='[S/43] - '+Caption;
end;

end.

