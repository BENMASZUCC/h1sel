object SelNazioneForm: TSelNazioneForm
  Left = 260
  Top = 117
  ActiveControl = ECerca
  BorderStyle = bsDialog
  Caption = 'Selezione nazione'
  ClientHeight = 300
  ClientWidth = 493
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object BitBtn1: TBitBtn
    Left = 394
    Top = 3
    Width = 96
    Height = 35
    TabOrder = 2
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 394
    Top = 39
    Width = 96
    Height = 35
    Caption = 'Annulla'
    TabOrder = 3
    Kind = bkCancel
  end
  object DBGrid1: TDBGrid
    Left = 4
    Top = 40
    Width = 385
    Height = 257
    DataSource = DsQNazioni
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'Abbrev'
        Title.Caption = 'Abbrev.'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DescNazione'
        Title.Caption = 'Nazione'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AreaGeografica'
        Title.Caption = 'Area geografica'
        Visible = True
      end>
  end
  object Panel1: TPanel
    Left = 4
    Top = 4
    Width = 385
    Height = 32
    BevelOuter = bvLowered
    TabOrder = 0
    object Label1: TLabel
      Left = 7
      Top = 9
      Width = 37
      Height = 13
      Caption = 'Ricerca'
    end
    object ECerca: TEdit
      Left = 50
      Top = 5
      Width = 143
      Height = 21
      TabOrder = 0
      OnChange = ECercaChange
    end
  end
  object QNazioni: TQuery
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select * from Nazioni'
      'where DescNazione like :xDesc'
      'order by DescNazione')
    Left = 24
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'xDesc'
        ParamType = ptUnknown
      end>
    object QNazioniID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.Nazioni.ID'
    end
    object QNazioniAbbrev: TStringField
      FieldName = 'Abbrev'
      Origin = 'EBCDB.Nazioni.Abbrev'
      FixedChar = True
      Size = 3
    end
    object QNazioniDescNazione: TStringField
      FieldName = 'DescNazione'
      Origin = 'EBCDB.Nazioni.DescNazione'
      FixedChar = True
      Size = 30
    end
    object QNazioniAreaGeografica: TStringField
      FieldName = 'AreaGeografica'
      Origin = 'EBCDB.Nazioni.AreaGeografica'
      FixedChar = True
    end
  end
  object DsQNazioni: TDataSource
    DataSet = QNazioni
    Left = 24
    Top = 96
  end
end
