unit ValutazFormaz;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, StdCtrls, Buttons, Mask, DBCtrls, ExtCtrls, Db, DBTables,
  Aligrid;

type
  TValutazFormazForm = class(TForm)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    Panel2: TPanel;
    Panel3: TPanel;
    DBEdit2: TDBEdit;
    DBEdit1: TDBEdit;
    DBEdit3: TDBEdit;
    SG1: TStringAlignGrid;
    TAnagCompIDX: TTable;
    TAnagCompIDXID: TAutoIncField;
    TAnagCompIDXIDCompetenza: TIntegerField;
    TAnagCompIDXIDAnagrafica: TIntegerField;
    TAnagCompIDXValore: TIntegerField;
    TStoricoAnagComp: TTable;
    TStoricoAnagCompID: TAutoIncField;
    TStoricoAnagCompIDCompetenza: TIntegerField;
    TStoricoAnagCompIDAnagrafica: TIntegerField;
    TStoricoAnagCompDallaData: TDateField;
    TStoricoAnagCompMotivoAumento: TStringField;
    TStoricoAnagCompIDCorso: TIntegerField;
    TStoricoAnagCompValore: TIntegerField;
    TStoricoAnagCompVariazPerc: TSmallintField;
    BitBtn2: TBitBtn;
    BRiempi: TBitBtn;
    Panel101: TPanel;
    procedure BRiempiClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    xArrayComp:array[1..40] of integer;
    xArrayPartec:array[1..100] of integer;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ValutazFormazForm: TValutazFormazForm;


implementation

uses MOduloDati6;


{$R *.DFM}


procedure TValutazFormazForm.BRiempiClick(Sender: TObject);
var i,k:integer;
begin
     SG1.RowCount:=DataFormazione.TSuggComp.RecordCount+1;
     SG1.ColCount:=DataFormazione.TPartecFormaz.RecordCount+1;
     DataFormazione.TSuggComp.First;
     SG1.RowHeights[0]:=16;
     for i:=1 to DataFormazione.TSuggComp.RecordCount do begin
         SG1.Cells[0,i]:=DataFormazione.TSuggCompCompetenza.AsString;
         SG1.RowHeights[i]:=16;
         xArrayComp[i]:=DataFormazione.TSuggCompIDCompetenza.Value;
         DataFormazione.TSuggComp.Next;
     end;
     DataFormazione.TPartecFormaz.First;
     SG1.ColWidths[0]:=180;
     for i:=1 to DataFormazione.TPartecFormaz.RecordCount do begin
         SG1.Cells[i,0]:=DataFormazione.TPartecFormazCognome.AsString;
         SG1.ColWidths[i]:=80;
         SG1.AlignCol[i]:=alCenter;
         SG1.EditCol[i]:=True;
         xArrayPartec[i]:=DataFormazione.TPartecFormazIDAnagrafica.Value;
         DataFormazione.TPartecFormaz.Next;
     end;
     // riempimento valori
     for i:=1 to SG1.RowCount-1 do begin
         for k:=1 to SG1.ColCount-1 do begin
             if TAnagCompIDX.FindKey([xArrayPartec[k],xArrayComp[i]]) then
                SG1.Cells[k,i]:=TAnagCompIDXValore.asString
             else SG1.Cells[k,i]:='0';
         end;
     end;
end;

procedure TValutazFormazForm.BitBtn1Click(Sender: TObject);
var xCol,xRow,xVarPerc,xVecchioVal:integer;
begin
   if MessageDlg('Le competenze verranno aggiornate coi nuovi valori: proseguire ?',mtWarning,[MbYes,mbNo],0)=mrNo then
      exit
   else begin
     for xCol:=1 to DataFormazione.TPartecFormaz.RecordCount do begin
         for xRow:=1 to DataFormazione.TSuggComp.RecordCount do begin
           if StrToInt(SG1.Cells[xCol,xRow])>0 then begin
             if TAnagCompIDX.FindKey([xArrayPartec[xCol],xArrayComp[xRow]]) then begin
                TAnagCompIDX.Edit;
                TAnagCompIDXValore.Value:=StrToInt(SG1.Cells[xCol,xRow]);
                TAnagCompIDX.Post;
             end else
               TAnagCompIDX.InsertRecord([xArrayPartec[xCol],xArrayComp[xRow],SG1.Cells[xCol,xRow]]);
             // registraz. storico
             xVecchioVal:=0;
             if TStoricoAnagComp.FindKey([xArrayPartec[xCol],xArrayComp[xRow]]) then xVecchioVal:=TStoricoAnagCompValore.Value;
             if xVecchioVal>0 then
                xVarPerc:=Trunc(((StrToInt(SG1.Cells[xCol,xRow])-xVecchioVal)/xVecchioVal)*100)
             else xVarPerc:=StrToInt(SG1.Cells[xCol,xRow])*100;

             TStoricoAnagComp.InsertRecord([xArrayPartec[xCol],
                                      xArrayComp[xRow],
                                      Date,
                                      'attivit� formativa ('+DataFormazione.TFormazAttivaCodiceCorso.Value+')',
                                      DataFormazione.TFormazAttivaID.Value,
                                      SG1.Cells[xCol,xRow],
                                      xVarPerc]);
           end;
         end;
     end;
   end;
end;

end.
