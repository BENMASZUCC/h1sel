object SelContrattoCliForm: TSelContrattoCliForm
  Left = 218
  Top = 106
  ActiveControl = DBGrid1
  BorderStyle = bsDialog
  Caption = 'Selezione contratto'
  ClientHeight = 256
  ClientWidth = 455
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object BitBtn1: TBitBtn
    Left = 357
    Top = 3
    Width = 97
    Height = 38
    TabOrder = 0
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 357
    Top = 43
    Width = 97
    Height = 38
    Caption = 'Annulla'
    TabOrder = 1
    Kind = bkCancel
  end
  object DBGrid1: TDBGrid
    Left = 3
    Top = 3
    Width = 350
    Height = 250
    DataSource = DsQContrattiCli
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'Data'
        Width = 73
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Descrizione'
        Width = 149
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Stato'
        Width = 92
        Visible = True
      end>
  end
  object QContrattiCli: TQuery
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select *'
      'from Contratti'
      'where IDCliente=:xIDCliente')
    Left = 40
    Top = 80
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'xIDCliente'
        ParamType = ptUnknown
      end>
    object QContrattiCliID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.Contratti.ID'
    end
    object QContrattiCliIDCliente: TIntegerField
      FieldName = 'IDCliente'
      Origin = 'EBCDB.Contratti.IDCliente'
    end
    object QContrattiCliData: TDateTimeField
      FieldName = 'Data'
      Origin = 'EBCDB.Contratti.Data'
    end
    object QContrattiCliDescrizione: TStringField
      FieldName = 'Descrizione'
      Origin = 'EBCDB.Contratti.Descrizione'
      FixedChar = True
      Size = 100
    end
    object QContrattiCliStato: TStringField
      FieldName = 'Stato'
      Origin = 'EBCDB.Contratti.Stato'
      FixedChar = True
    end
    object QContrattiCliCodice: TStringField
      FieldName = 'Codice'
      Size = 10
    end
  end
  object DsQContrattiCli: TDataSource
    DataSet = QContrattiCli
    Left = 40
    Top = 112
  end
end
