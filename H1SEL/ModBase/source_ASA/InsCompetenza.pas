unit InsCompetenza;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Grids, DBGrids, Db, DBTables, StdCtrls, Buttons, Spin, AdvGrid;

type
  TInsCompetenzaForm = class(TForm)
    TCompArea: TTable;
    DsCompArea: TDataSource;
    TCompAreaID: TAutoIncField;
    TCompAreaIDArea: TIntegerField;
    TCompAreaDescrizione: TStringField;
    TCompAreaTipologia: TStringField;
    TCompAreaQuantiValori: TIntegerField;
    TCompAreaArrayDescrizione: TStringField;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Panel2: TPanel;
    TAreeComp: TTable;
    DsAreeComp: TDataSource;
    TAreeCompID: TAutoIncField;
    TAreeCompDescrizione: TStringField;
    DBGrid1: TDBGrid;
    Timer1: TTimer;
    ASG1: TAdvStringGrid;
    Label2: TLabel;
    procedure BitBtn1Click(Sender: TObject);
    procedure DBGrid1KeyPress(Sender: TObject; var Key: Char);
    procedure Timer1Timer(Sender: TObject);
    //procedure ASG1GetEditorType(Sender: TObject; aCol, aRow: Integer; var aEditor: TEditorType);
    procedure TAreeCompAfterScroll(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
  private
    xStringa:string;
  public
    xArrayIDComp: array [1..100] of integer;
    xArrayCompVal: array [1..100] of integer;
    procedure RiempiGriglia;
  end;

var
  InsCompetenzaForm: TInsCompetenzaForm;

implementation

{$R *.DFM}

procedure TInsCompetenzaForm.BitBtn1Click(Sender: TObject);
var i,xTot:integer;
    Vero:boolean;
begin
     xTot:=0;
     for i:=1 to ASG1.RowCount-1 do begin
         ASG1.GetCheckBoxState(0,i,Vero);
         if Vero then begin
            inc(xTot);
            xArrayCompVal[i]:=StrToIntDef(ASG1.cells[1,i],0);
         end;
     end;
     if xtot=0 then begin
        MessageDlg('Nessuna competenza selezionata !',mtError,[mbOK],0);
        ModalResult:=mrNone;
        Abort;
     end;
end;

procedure TInsCompetenzaForm.DBGrid1KeyPress(Sender: TObject;
  var Key: Char);
begin
     Timer1.Enabled:=False;
     if Key=chr(13) then begin
        ASG1.SetFocus;
     end else begin
        xStringa:=xStringa+key;
        TAreeComp.FindNearest([xStringa]);
     end;
     Timer1.Enabled:=True;
end;

procedure TInsCompetenzaForm.Timer1Timer(Sender: TObject);
begin
     xStringa:='';
end;

procedure TInsCompetenzaForm.RiempiGriglia;
var i:integer;
begin
   if TCompArea.EOF then
      ASG1.Visible:=False
   else begin
     ASG1.Clear;
     ASG1.RowCount:=TCompArea.RecordCount+1;
     ASG1.FixedRows:=1;
     ASG1.Cells[0,0]:='competenza';
     ASG1.Cells[1,0]:='valore';
     ASG1.RowHeights[0]:=20;
     for i:=1 to ASG1.RowCount-1 do begin
         ASG1.RowHeights[i]:=18;
         ASG1.addcheckbox(0,i,false,false);
     end;

     TCompArea.First;
     i:=1;
     while not TCompArea.EOF do begin
          ASG1.Cells[0,i]:=TCompAreaDescrizione.Value;
          ASG1.Cells[1,i]:='0';
          xArrayIDComp[i]:=TCompAreaID.Value;
         TCompArea.next;
         inc(i);
     end;
     ASG1.Visible:=True;
   end;
end;

procedure TInsCompetenzaForm.TAreeCompAfterScroll(DataSet: TDataSet);
begin
     RiempiGriglia;
end;

procedure TInsCompetenzaForm.FormShow(Sender: TObject);
begin
     Caption:='[M/020] - '+Caption;
end;

end.
