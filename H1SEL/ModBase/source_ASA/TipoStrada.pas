unit TipoStrada;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Grids, DBGrids, Db, DBTables;

type
  TTipoStradaForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    QNazioni: TQuery;
    DsQNazioni: TDataSource;
    DBGrid1: TDBGrid;
    Label1: TLabel;
    ETipo: TEdit;
    QNazioniID: TAutoIncField;
    QNazioniAbbrev: TStringField;
    QNazioniDescNazione: TStringField;
    QNazioniAreaGeografica: TStringField;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  TipoStradaForm: TTipoStradaForm;

implementation

{$R *.DFM}

procedure TTipoStradaForm.FormShow(Sender: TObject);
begin
     Caption:='[S/46] - '+Caption;
end;

end.
