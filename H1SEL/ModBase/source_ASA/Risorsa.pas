unit Risorsa;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons;

type
  TRisorsaForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    ERisorsa: TEdit;
    Label1: TLabel;
    CBColore: TComboBox;
    Label2: TLabel;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  RisorsaForm: TRisorsaForm;

implementation

{$R *.DFM}

procedure TRisorsaForm.FormShow(Sender: TObject);
begin
     Caption:='[S/31] - '+Caption;
end;

end.
