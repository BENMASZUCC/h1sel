unit ArchivioMess;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     Db,DBTables,dxDBTLCl,dxGrClms,dxTL,dxDBCtrl,dxDBGrid,dxCntner,
     StdCtrls,Buttons,ExtCtrls;

type
     TArchivioMessForm=class(TForm)
          QMessLetti: TQuery;
          DsQMessLetti: TDataSource;
          Panel1: TPanel;
          BitBtn1: TBitBtn;
          dxDBGrid1: TdxDBGrid;
          QMessLettiID: TAutoIncField;
          QMessLettiIDUtente: TIntegerField;
          QMessLettiIDUtenteDa: TIntegerField;
          QMessLettiDataIns: TDateTimeField;
          QMessLettiDataDaLeggere: TDateTimeField;
          QMessLettiTesto: TStringField;
          QMessLettiDataLettura: TDateTimeField;
          QMessLettiEvaso: TBooleanField;
          QMessLettiIDCandUtente: TIntegerField;
          dxDBGrid1ID: TdxDBGridMaskColumn;
          dxDBGrid1IDUtente: TdxDBGridMaskColumn;
          dxDBGrid1IDUtenteDa: TdxDBGridMaskColumn;
          dxDBGrid1DataIns: TdxDBGridDateColumn;
          dxDBGrid1Testo: TdxDBGridMaskColumn;
          dxDBGrid1DataLettura: TdxDBGridDateColumn;
          dxDBGrid1IDCandUtente: TdxDBGridMaskColumn;
          BitBtn2: TBitBtn;
          procedure BitBtn2Click(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     ArchivioMessForm: TArchivioMessForm;

implementation

uses ModuloDati;

{$R *.DFM}

procedure TArchivioMessForm.BitBtn2Click(Sender: TObject);
begin
     if QMessLetti.IsEmpty then exit;
     if Messagedlg('Sei sicuro di voler eliminare DEFINITIVAMENTE il messaggio ?',mtWarning, [mbNo,mbYes],0)=mrYes then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text:='delete from Promemoria where ID='+QMessLettiID.AsString;
                    Q1.ExecSQL;
                    DB.CommitTrans;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               end;
          end;
          QMessLetti.Close;
          QMessLetti.Open;
     end;
end;

end.

