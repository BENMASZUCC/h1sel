�
 TINSDETTFATTFORM 0YX  TPF0TInsDettFattFormInsDettFattFormLeftTop� BorderStylebsDialogCaptionInserimento dettaglio fatturaClientHeight ClientWidthxColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TBitBtnBitBtn1LeftTopWidth`Height!CaptionEsciTabOrder KindbkOK  TPanelPanel3LeftTopWidth6Height+
BevelOuter	bvLoweredEnabledTabOrder TLabelLabel1LeftTopWidth HeightCaptionClienteFocusControlDBEdit1  TDBEditDBEdit1LeftTopWidth,HeightColorclAqua	DataFieldDescrizione
DataSourceData2.DsEBCclientiTabOrder    TPageControlPCLeftTop2WidthrHeight�
ActivePageTSNoteSpeseTabOrder 	TTabSheet
TSCompensiCaptionCompensi TDBGridDBGrid2LeftTopWidth
Heightq
DataSourceDsQCompensiOptionsdgTitlesdgIndicatordgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgConfirmDeletedgCancelOnExitdgMultiSelect TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameTipoWidth� Visible	 Expanded	FieldNameImportoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold Title.Font.CharsetDEFAULT_CHARSETTitle.Font.ColorclWindowTextTitle.Font.Height�Title.Font.NameMS Sans SerifTitle.Font.StylefsBold WidthEVisible	 Expanded	FieldName
RifRicercaTitle.CaptionRic.n�Width'Visible	 Expanded	FieldNameNoteWidth� Visible	    TBitBtnBitBtn4LeftTopWidthWHeight!Caption	InserisciFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickBitBtn4Click
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333�3333�39�3330 373337w39�3330 3��337w9���33337ww333?9���333 7wws333w39�3333 373333w39�3333337s3333?3333333 3333�33w333<333 3337�33w333<�3333����?�<����Ù37wwwww�<����Ù37wwwwsw3333<�3333337s33�333<330 3337337w3333330 3333337w3333333333333333	NumGlyphs  TRadioGroupRGOpzLeftTop� Width!HeightGCaptionOpzione inserimento	ItemIndex Items.Strings%non far riferimento ad alcun soggetto'far riferimento ad un soggetto inserito/far riferimento a uno o pi� soggetti in ricerca TabOrderOnClick
RGOpzClick  TPanelPanel2LeftTopWidth
Height	AlignmenttaLeftJustifyCaptionD  Compensi non ancora fatturati al cliente (in ordine di n� ricerca)ColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TPageControl
PCSoggettiLeftTop� WidthHeight� 
ActivePageTSSoggInseritiTabOrderVisible 	TTabSheetTSSoggInseritiCaptionTSSoggInseriti TPanelPanel1LeftTopWidth�Height� 
BevelOuter	bvLoweredCaptionPanel1TabOrder  TPanelPanel4LeftTopWidth�HeightAlignalTop	AlignmenttaLeftJustifyCaptionP  Inserimenti presso il cliente (e non ancora fatturati) per la ricerca relativaColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   TDBGridDBGrid1LeftTopWidth�HeightAlignalClient
DataSourceDsQInsNonFattTabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameCognomeWidth}Visible	 Expanded	FieldNameNomeWidthwVisible	 Expanded	FieldNameRuoloWidth� Visible	      	TTabSheetTSSoggCandRicCaptionTSSoggCandRic
ImageIndex TPanelPanel6Left Top WidthHeightAlignalTop	AlignmenttaLeftJustifyCaptionR  Candidati associati alle ricerche del cliente (non esclusi) in ordine alfabeticoColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   TAdvStringGridASG1Left TopWidthHeight� AlignalClientColCountDefaultRowHeight	FixedCols OptionsgoFixedVertLinegoFixedHorzLine
goVertLine
goHorzLinegoRangeSelectgoColSizing	goEditing 
ScrollBars
ssVerticalTabOrderAutoNumAlignAutoSize
VAlignment	vtaCenterEnhTextSizeEnhRowColMoveSortFixedColsSizeWithFormMultilinecellsSortDirectionsdAscendingSortFull	SortAutoFormat	SortShowEnableGraphics	
SortColumn 	HintColorclYellowSelectionColorclHighlightSelectionTextColorclHighlightTextSelectionRectangleSelectionRTFKeepHintShowCellsOleDropTargetOleDropSource
OleDropRTFPrintSettings.FooterSize PrintSettings.HeaderSize PrintSettings.TimeppNonePrintSettings.DateppNonePrintSettings.DateFormat
dd/mm/yyyyPrintSettings.PageNrppNonePrintSettings.TitleppNonePrintSettings.Font.CharsetDEFAULT_CHARSETPrintSettings.Font.ColorclWindowTextPrintSettings.Font.Height�PrintSettings.Font.NameMS Sans SerifPrintSettings.Font.Style  PrintSettings.HeaderFont.CharsetDEFAULT_CHARSETPrintSettings.HeaderFont.ColorclWindowTextPrintSettings.HeaderFont.Height�PrintSettings.HeaderFont.NameMS Sans SerifPrintSettings.HeaderFont.Style  PrintSettings.FooterFont.CharsetDEFAULT_CHARSETPrintSettings.FooterFont.ColorclWindowTextPrintSettings.FooterFont.Height�PrintSettings.FooterFont.NameMS Sans SerifPrintSettings.FooterFont.Style PrintSettings.Borders
pbNoborderPrintSettings.BorderStylepsSolidPrintSettings.CenteredPrintSettings.RepeatFixedRowsPrintSettings.RepeatFixedColsPrintSettings.LeftSize PrintSettings.RightSize PrintSettings.ColumnSpacing PrintSettings.RowSpacing PrintSettings.TitleSpacing PrintSettings.Orientation
poPortraitPrintSettings.PagePrefixpagePrintSettings.FixedWidth PrintSettings.FixedHeight PrintSettings.UseFixedHeightPrintSettings.UseFixedWidthPrintSettings.FitToPagefpNeverPrintSettings.PageNumSep/PrintSettings.NoAutoSizePrintSettings.PrintGraphicsHTMLSettings.WidthdNavigation.AllowInsertRowNavigation.AllowDeleteRowNavigation.AdvanceOnEnterNavigation.AdvanceInsertNavigation.AutoGotoWhenSortedNavigation.AutoGotoIncrementalNavigation.AutoComboDropSizeNavigation.AdvanceDirectionadLeftRight"Navigation.AllowClipboardShortCutsNavigation.AllowSmartClipboardNavigation.AllowRTFClipboardNavigation.AdvanceAutoNavigation.InsertPositionpInsertBeforeNavigation.CursorWalkEditorNavigation.MoveRowOnSortNavigation.ImproveMaskSelNavigation.AlwaysEditColumnSize.SaveColumnSize.StretchColumnSize.Location
clRegistryCellNode.ColorclSilverCellNode.NodeTypecnFlatCellNode.NodeColorclBlackSizeWhileTyping.HeightSizeWhileTyping.WidthMouseActions.AllSelectMouseActions.ColSelectMouseActions.RowSelectMouseActions.DirectEdit	MouseActions.DisjunctRowSelectMouseActions.AllColumnSizeMouseActions.CaretPositioning
IntelliPan
ipVerticalURLColorclBlackURLShowURLFullURLEdit
ScrollTypessNormalScrollColorclNoneScrollWidthScrollProportionalScrollHintsshNone
OemConvertFixedFooters FixedRightCols FixedColWidth�FixedRowHeightFixedFont.CharsetDEFAULT_CHARSETFixedFont.ColorclWindowTextFixedFont.Height�FixedFont.NameMS Sans SerifFixedFont.Style FixedAsButtonsFloatFormat%.2fWordWrapLookupLookupCaseSensitiveLookupHistoryHideFocusRectBackGround.Top BackGround.Left BackGround.DisplaybdTileHoveringFilter FilterActive	ColWidths� 
RowHeights      	TTabSheetTSNoteSpeseCaption
Note Spese
ImageIndex TPanelPanel5LeftTopWidth
Height	AlignmenttaLeftJustifyCaption@  Note spese non ancora fatturate al cliente (in ordine di data)ColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   TDBGridDBGrid3LeftTopWidth	Height�
DataSourceDsQNoteSpeseCliTabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameDataVisible	 Expanded	FieldNameDescrizioneWidth� Visible	 Expanded	FieldName
RifRicercaTitle.Caption
Rif.ric.n�Visible	 Expanded	FieldNameTotSpeseFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold Title.Font.CharsetDEFAULT_CHARSETTitle.Font.ColorclWindowTextTitle.Font.Height�Title.Font.NameMS Sans SerifTitle.Font.StylefsBold Visible	    TBitBtnBNotaSpeseInsLeftTopWidthWHeight!Caption	InserisciFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickBNotaSpeseInsClick
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333�3333�39�3330 373337w39�3330 3��337w9���33337ww333?9���333 7wws333w39�3333 373333w39�3333337s3333?3333333 3333�33w333<333 3337�33w333<�3333����?�<����Ù37wwwww�<����Ù37wwwwsw3333<�3333337s33�333<330 3337337w3333330 3333337w3333333333333333	NumGlyphs   	TTabSheet	TSAnnunciCaptionAnnunci
ImageIndex TPanelPanel7LeftTopWidth
Height	AlignmenttaLeftJustifyCaption[  Annunci non ancora fatturati al cliente (in ordine di rif.annuncio) - riferiti a ricercheColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   TDBGridDBGrid4LeftTopWidth
Height� 
DataSourceDsQAnnunciCliTabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameDataWidthJVisible	 Expanded	FieldNameRifTitle.CaptionRif.annuncioVisible	 Expanded	FieldName	NumModuliTitle.Caption	N� ModuliVisible	 Expanded	FieldName	NumParoleTitle.Caption	N� ParoleVisible	 Expanded	FieldName
TotaleEuroFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold Title.CaptionTotale EuroTitle.Font.CharsetDEFAULT_CHARSETTitle.Font.ColorclWindowTextTitle.Font.Height�Title.Font.NameMS Sans SerifTitle.Font.StylefsBold WidthLVisible	 Expanded	FieldName
RifRicercaTitle.Caption
Rif.ric.n�Visible	    TBitBtnBInsAnnuncioLeftTopWidthWHeight!Caption	InserisciFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickBInsAnnuncioClick
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333�3333�39�3330 373337w39�3330 3��337w9���33337ww333?9���333 7wws333w39�3333 373333w39�3333337s3333?3333333 3333�33w333<333 3337�33w333<�3333����?�<����Ù37wwwww�<����Ù37wwwwsw3333<�3333337s33�333<330 3337337w3333330 3333337w3333333333333333	NumGlyphs  TPanelPanel8LeftTop� Width
Height	AlignmenttaLeftJustifyCaption_  Annunci non ancora fatturati al cliente (in ordine di rif.annuncio) - NON riferiti a ricercheColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBGridDBGrid5LeftTop� Width
Height� 
DataSourceDsQAnnunciCli2TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameDataWidthJVisible	 Expanded	FieldNameRifTitle.CaptionRif.annuncioVisible	 Expanded	FieldName	NumModuliTitle.Caption	N� ModuliVisible	 Expanded	FieldName	NumParoleTitle.Caption	N� ParoleVisible	 Expanded	FieldName
TotaleEuroFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold Title.CaptionTotale EuroTitle.Font.CharsetDEFAULT_CHARSETTitle.Font.ColorclWindowTextTitle.Font.Height�Title.Font.NameMS Sans SerifTitle.Font.StylefsBold WidthLVisible	    TBitBtnBInsAnnuncio2LeftTop� WidthWHeight!Caption	InserisciFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickBInsAnnuncio2Click
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333�3333�39�3330 373337w39�3330 3��337w9���33337ww333?9���333 7wws333w39�3333 373333w39�3333337s3333?3333333 3333�33w333<333 3337�33w333<�3333����?�<����Ù37wwwww�<����Ù37wwwwsw3333<�3333337s33�333<330 3337337w3333330 3333337w3333333333333333	NumGlyphs    TQueryQInsNonFattActive	DatabaseNameEBCDB
DataSourceDsQCompensiSQL.StringsGselect EBC_Inserimenti.ID,Cognome,Nome,Sesso,EBC_Inserimenti.DallaData,I          EBC_Ricerche.Progressivo,DataInizio,Mansioni.Descrizione Ruolo,%          EBC_Ricerche.StipendioLordo5from EBC_Inserimenti,Anagrafica,EBC_Ricerche,Mansioni0where EBC_Inserimenti.IDAnagrafica=Anagrafica.ID1    and EBC_Inserimenti.IDRicerca=EBC_Ricerche.ID+    and EBC_Ricerche.IDMansione=Mansioni.ID"    and EBC_Ricerche.ID=:IDRicerca    and ProgFattura is null Left� Toph	ParamDataDataType	ftIntegerName	IDRicerca	ParamType	ptUnknown   TIntegerFieldQInsNonFattID	FieldNameIDOrigin"EBC_Inserimenti".ID  TStringFieldQInsNonFattCognome	FieldNameCognomeOrigin"Anagrafica".CognomeSize  TStringFieldQInsNonFattNome	FieldNameNomeOrigin"Anagrafica".NomeSize  TDateTimeFieldQInsNonFattDallaData	FieldName	DallaDataOrigin"EBC_Inserimenti".DallaData  TStringFieldQInsNonFattProgressivo	FieldNameProgressivoOrigin"EBC_Ricerche".ProgressivoSize
  TDateTimeFieldQInsNonFattDataInizio	FieldName
DataInizioOrigin"EBC_Ricerche".DataInizio  TStringFieldQInsNonFattRuolo	FieldNameRuoloOrigin"Mansioni".DescrizioneSize(  TStringFieldQInsNonFattSesso	FieldNameSessoOrigin"Anagrafica".SessoSize  TFloatFieldQInsNonFattStipendioLordo	FieldNameStipendioLordoOrigin"EBC_Ricerche".StipendioLordo   TDataSourceDsQInsNonFattDataSetQInsNonFattLeft� Top�  TTableTInserimentiABSDatabaseNameEBCDB	TableNamedbo.EBC_InserimentiLeft� Topx TAutoIncFieldTInserimentiABSID	FieldNameIDReadOnly	  TIntegerFieldTInserimentiABSIDAnagrafica	FieldNameIDAnagraficaRequired	  TIntegerFieldTInserimentiABSIDCliente	FieldName	IDClienteRequired	  TDateTimeFieldTInserimentiABSDallaData	FieldName	DallaData  TIntegerFieldTInserimentiABSProgFattura	FieldNameProgFattura   TDataSourceDsQCompensiDataSet	QCompensiLeft(Top�   TQuery	QCompensiActive	DatabaseNameEBCDB
DataSourceData2.DsEBCclientiSQL.Strings>select EBC_CompensiRicerche.Tipo,EBC_CompensiRicerche.Importo,M          EBC_CompensiRicerche.IDFattura,EBC_Ricerche.Progressivo RifRicerca,)          EBC_CompensiRicerche.IDRicerca,;          EBC_CompensiRicerche.Note,EBC_CompensiRicerche.ID&from EBC_CompensiRicerche,EBC_Ricerche4where EBC_CompensiRicerche.IDRicerca=EBC_Ricerche.ID*and EBC_CompensiRicerche.IDFattura is nulland EBC_Ricerche.IDCliente=:ID!order by EBC_Ricerche.Progressivo Left(Top� 	ParamDataDataType	ftAutoIncNameID	ParamType	ptUnknown   TStringFieldQCompensiTipo	FieldNameTipoOrigin"EBC_CompensiRicerche".TipoSize  TFloatFieldQCompensiImporto	FieldNameImportoOrigin"EBC_CompensiRicerche".ImportoDisplayFormat#,###.##  TIntegerFieldQCompensiIDFattura	FieldName	IDFatturaOrigin "EBC_CompensiRicerche".IDFattura  TStringFieldQCompensiRifRicerca	FieldName
RifRicercaOrigin"EBC_Ricerche".ProgressivoSize
  TStringFieldQCompensiNote	FieldNameNoteOrigin"EBC_CompensiRicerche".NoteSizeP  TIntegerFieldQCompensiIDRicerca	FieldName	IDRicercaOrigin "EBC_CompensiRicerche".IDRicerca  TIntegerFieldQCompensiID	FieldNameIDOrigin"EBC_CompensiRicerche".ID   TQueryQDatabaseNameEBCDBLeft#Top�  TQueryQCandRicClienteDatabaseNameEBCDB
DataSourceData2.DsEBCclientiSQL.Strings8select Anagrafica.ID,Anagrafica.Cognome,Anagrafica.Nome,.          EBC_Ricerche.Progressivo RifRicerca,#          EBC_Ricerche.ID IDRicerca2from EBC_CandidatiRicerche,EBC_Ricerche,Anagrafica5where EBC_CandidatiRicerche.IDRicerca=EBC_Ricerche.ID6  and EBC_CandidatiRicerche.IDAnagrafica=Anagrafica.IDP  and (EBC_CandidatiRicerche.Escluso=0 or EBC_CandidatiRicerche.Escluso is null)   and EBC_Ricerche.IDCliente=:IDorder by Cognome,Nome LeftnTopj	ParamDataDataType	ftAutoIncNameID	ParamType	ptUnknown   TIntegerFieldQCandRicClienteID	FieldNameIDOrigin"Anagrafica".ID  TStringFieldQCandRicClienteCognome	FieldNameCognomeOrigin"Anagrafica".CognomeSize  TStringFieldQCandRicClienteNome	FieldNameNomeOrigin"Anagrafica".NomeSize  TStringFieldQCandRicClienteRifRicerca	FieldName
RifRicercaOrigin"EBC_Ricerche".ProgressivoSize
  TIntegerFieldQCandRicClienteIDRicerca	FieldName	IDRicercaOrigin"EBC_Ricerche".ID   TTableTAnagABSDatabaseNameEBCDB	TableNamedbo.AnagraficaLeft~Top� TAutoIncField
TAnagABSID	FieldNameIDReadOnly	  TStringFieldTAnagABSCognome	FieldNameCognomeSize  TStringFieldTAnagABSNome	FieldNameNomeSize  TStringFieldTAnagABSSesso	FieldNameSessoSize   TQueryQNoteSpeseCliActive	OnCalcFieldsQNoteSpeseCliCalcFieldsDatabaseNameEBCDB
DataSourceData2.DsEBCclientiSQL.StringsBselect RicNoteSpese.ID,RicNoteSpese.Data,RicNoteSpese.Descrizione,"          RicNoteSpese.Automobile,"          RicNoteSpese.Autostrada,"          RicNoteSpese.Aereotreno,          RicNoteSpese.Taxi,          RicNoteSpese.Albergo,%          RicNoteSpese.RistoranteBar,          RicNoteSpese.Parking,$          RicNoteSpese.AltroImporto,/          EBC_Ricerche.Progressivo RifRicerca  from RicNoteSpese,EBC_Ricerche,where RicNoteSpese.IDRicerca=EBC_Ricerche.IDB  and (RicNoteSpese.IDFattura is null or RicNoteSpese.IDFattura=0)   and RicNoteSpese.IDCliente=:IDorder by Data Left�Topj	ParamDataDataType	ftAutoIncNameID	ParamType	ptUnknown   TIntegerFieldQNoteSpeseCliID	FieldNameIDOrigin"RicNoteSpese".ID  TDateTimeFieldQNoteSpeseCliData	FieldNameDataOrigin"RicNoteSpese".Data  TStringFieldQNoteSpeseCliDescrizione	FieldNameDescrizioneOrigin"RicNoteSpese".DescrizioneSize2  TStringFieldQNoteSpeseCliRifRicerca	FieldName
RifRicercaOrigin"EBC_Ricerche".ProgressivoSize
  TFloatFieldQNoteSpeseCliTotSpese	FieldKindfkCalculated	FieldNameTotSpeseDisplayFormat#,###.##
Calculated	  TFloatFieldQNoteSpeseCliAutomobile	FieldName
AutomobileOrigin"RicNoteSpese".Automobile  TFloatFieldQNoteSpeseCliAutostrada	FieldName
AutostradaOrigin"RicNoteSpese".Autostrada  TFloatFieldQNoteSpeseCliAereotreno	FieldName
AereotrenoOrigin"RicNoteSpese".AereoTreno  TFloatFieldQNoteSpeseCliTaxi	FieldNameTaxiOrigin"RicNoteSpese".Taxi  TFloatFieldQNoteSpeseCliAlbergo	FieldNameAlbergoOrigin"RicNoteSpese".Albergo  TFloatFieldQNoteSpeseCliRistoranteBar	FieldNameRistoranteBarOrigin"RicNoteSpese".RistoranteBar  TFloatFieldQNoteSpeseCliParking	FieldNameParkingOrigin"RicNoteSpese".Parking  TFloatFieldQNoteSpeseCliAltroImporto	FieldNameAltroImportoOrigin"RicNoteSpese".AltroImporto   TDataSourceDsQNoteSpeseCliDataSetQNoteSpeseCliLeft�Top�  TQueryQAnnunciCliActive	DatabaseNameEBCDB
DataSourceData2.DsEBCclientiSQL.Stringsselect Ann_annunci.Data,          Ann_annunci.Rif,          Ann_annunci.ID,           Ann_annunci.NumModuli,           Ann_annunci.NumParole,!          Ann_annunci.TotaleLire,!          Ann_annunci.TotaleEuro,-          EBC_Ricerche.Progressivo RifRicerca1from Ann_annunci,Ann_annunciRicerche,EBC_Ricerche3where Ann_annunci.ID=Ann_annunciRicerche.IDAnnuncio3  and Ann_annunciRicerche.IDRicerca=EBC_Ricerche.ID@  and (Ann_annunci.IDFattura is null or Ann_annunci.IDFattura=0)   and EBC_Ricerche.IDcliente=:IDorder by Data LeftTopj	ParamDataDataType	ftAutoIncNameID	ParamType	ptUnknown   TDateTimeFieldQAnnunciCliData	FieldNameDataOrigin"Ann_Annunci".Data  TStringFieldQAnnunciCliRif	FieldNameRifOrigin"Ann_Annunci".RifSize  TIntegerFieldQAnnunciCliID	FieldNameIDOrigin"Ann_Annunci".ID  TSmallintFieldQAnnunciCliNumModuli	FieldName	NumModuliOrigin"Ann_Annunci".NumModuli  TSmallintFieldQAnnunciCliNumParole	FieldName	NumParoleOrigin"Ann_Annunci".NumParole  TCurrencyFieldQAnnunciCliTotaleLire	FieldName
TotaleLireOrigin"Ann_Annunci".TotaleLireDisplayFormat#,###  TStringFieldQAnnunciCliRifRicerca	FieldName
RifRicercaOrigin"EBC_Ricerche".ProgressivoSize
  TFloatFieldQAnnunciCliTotaleEuro	FieldName
TotaleEuroOriginEBCDB.Ann_annunci.TotaleEuroDisplayFormat#,###.##   TDataSourceDsQAnnunciCliDataSetQAnnunciCliLeftTop�  TQueryQAnnunciCli2Active	DatabaseNameEBCDB
DataSourceData2.DsEBCclientiSQL.Stringsselect Ann_annunci.Data,          Ann_annunci.Rif,          Ann_annunci.ID,           Ann_annunci.NumModuli,           Ann_annunci.NumParole,!          Ann_annunci.TotaleLire,           Ann_annunci.TotaleEurofrom Ann_annunciwhere Ann_annunci.IDcliente=:ID@  and (Ann_annunci.IDFattura is null or Ann_annunci.IDFattura=0)order by Data Left7Topj	ParamDataDataType	ftAutoIncNameID	ParamType	ptUnknown   TDateTimeFieldQAnnunciCli2Data	FieldNameDataOriginEBCDB.Ann_annunci.Data  TStringFieldQAnnunciCli2Rif	FieldNameRifOriginEBCDB.Ann_annunci.Rif	FixedChar	Size  TAutoIncFieldQAnnunciCli2ID	FieldNameIDOriginEBCDB.Ann_annunci.ID  TSmallintFieldQAnnunciCli2NumModuli	FieldName	NumModuliOriginEBCDB.Ann_annunci.NumModuli  TSmallintFieldQAnnunciCli2NumParole	FieldName	NumParoleOriginEBCDB.Ann_annunci.NumParole  TFloatFieldQAnnunciCli2TotaleLire	FieldName
TotaleLireOriginEBCDB.Ann_annunci.TotaleLireDisplayFormat#,###  TFloatFieldQAnnunciCli2TotaleEuro	FieldName
TotaleEuroOriginEBCDB.Ann_annunci.TotaleEuroDisplayFormat#,###.##   TDataSourceDsQAnnunciCli2DataSetQAnnunciCli2Left7Top�   