unit FrmDatiRetribuz;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     Db,DBTables,Grids,DBGrids,TB97,ExtCtrls,RXDBCtrl,Aligrid,StdCtrls,
     Mask,DBCtrls;

type
     TDatiRetribuzFrame=class(TFrame)
          QDatiRetribuz: TQuery;
          DsQDatiRetribuz: TDataSource;
          UpdDatiRetribuz: TUpdateSQL;
          QDatiRetribuzID: TAutoIncField;
          QDatiRetribuzCampo: TStringField;
          QDatiRetribuzValore: TFloatField;
          Panel1: TPanel;
          Panel2: TPanel;
          Panel74: TPanel;
          ToolbarButton971: TToolbarButton97;
          QDatiRetribuzIDColonna: TIntegerField;
          BMod: TToolbarButton97;
          RxDBGrid1: TRxDBGrid;
          BStorico: TToolbarButton97;
          QDatiRetribuzData: TDateTimeField;
          QDatiRetribuzConsideraTot: TBooleanField;
          Panel3: TPanel;
          Label1: TLabel;
          QTot: TQuery;
          DsQTot: TDataSource;
          QTotTotValore: TFloatField;
          DBEdit1: TDBEdit;
          procedure ToolbarButton971Click(Sender: TObject);
          procedure BModClick(Sender: TObject);
          procedure BStoricoClick(Sender: TObject);
     private
          { Private declarations }
     public
          xIDEspLav: integer;
     end;

implementation

uses uUtilsVarie,ModuloDati,SelData,ModuloDati2;

{$R *.DFM}

procedure TDatiRetribuzFrame.ToolbarButton971Click(Sender: TObject);
var xIDColonna: string;
begin
     if QDatiRetribuz.IsEmpty then xIDColonna:=''
     else xIDColonna:=QDatiRetribuzID.AsString;
     OpenSelFromTab('EspLavRetribColonne','ID','NomeColonna','Nome campo',xIDColonna);
     QDatiRetribuz.Close;
     QDatiRetribuz.Open;
end;

procedure TDatiRetribuzFrame.BModClick(Sender: TObject);
var xVal: string;
     xID: integer;
begin
     xVal:=QDatiRetribuzValore.AsString;
     xID:=QDatiRetribuzID.Value;
     if not InputQuery(QDatiRetribuzCampo.Value,'valore:',xVal) then exit;
     SelDataForm:=TSelDataForm.create(self);
     SelDataForm.DEData.Date:=Date;
     SelDataForm.ShowModal;
     if SelDataForm.ModalResult=mrOK then begin
          with Data do begin
               Qtemp.Close;
               Qtemp.SQL.text:='select ID from EspLavRetribuzioni '+
                    'where IDColonna=:xIDColonna and IDEspLav=:xIDEspLAv';
               Qtemp.ParambyName('xIDColonna').asInteger:=QDatiRetribuzIDColonna.Value;
               Qtemp.ParambyName('xIDEspLav').asInteger:=Data.TEspLavID.Value;
               Qtemp.Open;
               Q1.Close;
               if Qtemp.IsEmpty then begin
                    Q1.SQL.text:='insert into EspLavRetribuzioni (IDespLav,IDColonna,Valore,Data,ConsideraTot) '+
                         'values (:xIDEspLav,:xIDColonna,:xValore,:xData,:xConsideraTot)';
                    Q1.ParamByName('xIDEspLav').asInteger:=Data.TEspLavID.Value;
                    Q1.ParamByName('xIDColonna').asInteger:=QDatiRetribuzIDColonna.Value;
                    Q1.ParamByName('xValore').asFloat:=StrToFloat(xVal);
                    Q1.ParamByName('xData').asDateTime:=SelDataForm.DEData.Date;
                    Q1.ParamByName('xConsideraTot').asBoolean:=True;
               end else begin
                    Q1.SQL.text:='update EspLavRetribuzioni set Valore=:xValore, Data=:xData '+
                         'where ID='+QTemp.FieldByName('ID').asString;
                    Q1.ParamByName('xValore').asFloat:=StrToFloat(xVal);
                    Q1.ParamByName('xData').asDateTime:=SelDataForm.DEData.Date;
               end;
               DB.BeginTrans;
               try
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    QDatiRetribuz.Close;
                    QDatiRetribuz.Open;
                    QDatiRetribuz.Locate('ID',xID, []);
                    QTot.Close;
                    QTot.Open;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               end;
          end;
     end;
     SelDataForm.Free;
end;

procedure TDatiRetribuzFrame.BStoricoClick(Sender: TObject);
var xVal: string;
     xID: integer;
begin
     xVal:=QDatiRetribuzValore.AsString;
     xID:=QDatiRetribuzID.Value;
     with Data do begin
          Qtemp.Close;
          Qtemp.SQL.text:='select ID from EspLavRetribuzioni '+
               'where IDColonna=:xIDColonna and IDEspLav=:xIDEspLAv';
          Qtemp.ParambyName('xIDColonna').asInteger:=QDatiRetribuzIDColonna.Value;
          Qtemp.ParambyName('xIDEspLav').asInteger:=Data.TEspLavID.Value;
          Qtemp.Open;
          Q1.Close;
          if Qtemp.IsEmpty then exit;
          Q1.SQL.text:='update EspLavRetribuzioni set ConsideraTot=:xConsideraTot '+
               'where ID='+QTemp.FieldByName('ID').asString;
          Q1.ParamByName('xConsideraTot').AsBoolean:=not QDatiRetribuzConsideraTot.Value;
          DB.BeginTrans;
          try
               Q1.ExecSQL;
               DB.CommitTrans;
               QDatiRetribuz.Close;
               QDatiRetribuz.Open;
               QDatiRetribuz.Locate('ID',xID, []);
               QTot.Close;
               QTot.Open;
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
          end;
     end;
end;

end.

