unit ModCompMans;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, DBCtrls, Mask, ExtCtrls;

type
  TModCompMansForm = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    DBLookupComboBox1: TDBLookupComboBox;
    Label2: TLabel;
    Label3: TLabel;
    DBEdit2: TDBEdit;
    Label4: TLabel;
    DBCheckBox1: TDBCheckBox;
    DBEdit3: TDBEdit;
    DBComboBox1: TDBComboBox;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ModCompMansForm: TModCompMansForm;

implementation

uses ModuloDati3;

{$R *.DFM}

procedure TModCompMansForm.BitBtn1Click(Sender: TObject);
begin
     DataSel_EBC.TCompMansione.Post;
     close;
end;

procedure TModCompMansForm.BitBtn2Click(Sender: TObject);
begin
     DataSel_EBC.TCompMansione.Cancel;
     close;
end;

end.
