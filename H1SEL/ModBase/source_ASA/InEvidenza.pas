unit InEvidenza;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, Buttons, ExtCtrls, Grids, DBGrids, DBCtrls, Mask,
  DBCGrids, DB, DBTables;

type
  TInEvidenzaForm = class(TForm)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    PageControl1: TPageControl;
    TsRicerche: TTabSheet;
    Panel2: TPanel;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    DataEvento: TDateTimePicker;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    DBGrid1: TDBGrid;
    BitBtn3: TBitBtn;
    DBGrid2: TDBGrid;
    BitBtn4: TBitBtn;
    TsDatiPos: TTabSheet;
    TsVisioneCV: TTabSheet;
    GroupBox3: TGroupBox;
    GroupBox4: TGroupBox;
    GroupBox5: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    RadioGroup1: TRadioGroup;
    BbCVnew: TBitBtn;
    BbCVdel: TBitBtn;
    BitBtn7: TBitBtn;
    DBCtrlGrid1: TDBCtrlGrid;
    DBEdit1: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    DBLookupComboBox1: TDBLookupComboBox;
    DBCheckBox2: TDBCheckBox;
    Panel3: TPanel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    DBMemo1: TDBMemo;
    Q1: TQuery;
    BbCVok: TBitBtn;
    BbCVcan: TBitBtn;
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BbCVnewClick(Sender: TObject);
    procedure DBGrid3DblClick(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
    procedure BbCVdelClick(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BbCVokClick(Sender: TObject);
    procedure BbCVcanClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  InEvidenzaForm: TInEvidenzaForm;

implementation

uses ModuloDati, ModuloDati5;

{$R *.DFM}





procedure TInEvidenzaForm.BitBtn3Click(Sender: TObject);
begin
     with DataAssunzione do begin
          TAnagRicerche.Findkey([QAnagRicercheID.Value]);
          TAnagRicerche.Edit;
           TAnagRicercheInEvidenza.Value:=True;
          TAnagRicerche.Post;
          // refresh altra query
          QAnagRicEvid.Close; QAnagRicEvid.Open;
     end;
end;

procedure TInEvidenzaForm.BitBtn4Click(Sender: TObject);
begin
     with DataAssunzione do begin
          TAnagRicerche.Findkey([QAnagRicEvidID.Value]);
          TAnagRicerche.Edit;
           TAnagRicercheInEvidenza.Value:=False;
          TAnagRicerche.Post;
          // refresh query
          QAnagRicerche.Close; QAnagRicerche.Open;
          QAnagRicEvid.Close; QAnagRicEvid.Open;
     end;
end;

procedure TInEvidenzaForm.BbCVnewClick(Sender: TObject);
begin
     DataAssunzione.TCVinVisione.Insert;
     DataAssunzione.TCVinVisioneRestituito.Value:=False;
     DataAssunzione.TCVinVisioneInteressante.Value:=False;
     DataAssunzione.TCVinVisioneNuovo.Value:=True;
end;

procedure TInEvidenzaForm.DBGrid3DblClick(Sender: TObject);
begin
     DataAssunzione.TCVinVisione.Edit;
     if DataAssunzione.TCVinVisioneRestituito.Value=True then
        DataAssunzione.TCVinVisioneRestituito.Value:=False
     else DataAssunzione.TCVinVisioneRestituito.Value:=True;
     DataAssunzione.TCVinVisione.Post;
end;

procedure TInEvidenzaForm.RadioGroup1Click(Sender: TObject);
begin
     DataAssunzione.TCVinVisione.Filtered:=True;
     case RadioGroup1.ItemIndex of
     0: DataAssunzione.TCVinVisione.Filter:='';
     1: DataAssunzione.TCVinVisione.Filter:='Restituito=False';
     end;
end;

procedure TInEvidenzaForm.BbCVdelClick(Sender: TObject);
begin
    if MessageDlg('Sei sicuro di voler eminimarlo',mtWarning,
       [mbNo,mbYes],0)=mrYes then DataAssunzione.TCVinVisione.Delete;
end;

procedure TInEvidenzaForm.BitBtn7Click(Sender: TObject);
begin
     DataAssunzione.TCVinVisione.Edit;
     DataAssunzione.TCVinVisioneRestituito.Value:=True;
     DataAssunzione.TCVinVisione.Post;
end;

procedure TInEvidenzaForm.BitBtn1Click(Sender: TObject);
begin
     DataAssunzione.TCVinVisione.Filter:='Nuovo=True';
     DataAssunzione.TCVinVisione.First;
     while not DataAssunzione.TCVinVisione.EOF do begin
         // registrazione in promemoria
            Data.TPromemoria.InsertRecord([DataAssunzione.TCVinVisioneIDinVisioneA.Value,
                                           Data.GlobalLastLoginID.Value,
                                           DataAssunzione.TCVinVisioneRestituireEntro.Value,
                                           'In visione CV di '+
                                            DataAssunzione.TCVinVisioneCandidato.asString+
                                            ' - candidato in evidenza.',
                                            null,False,Date]);
         DataAssunzione.TCVinVisione.Next;
     end;
     Q1.SQL.Clear;
     Q1.SQL.Add('update CVinVisione set Nuovo=False where Nuovo=True');
     Q1.ExecSQL;
end;



procedure TInEvidenzaForm.BbCVokClick(Sender: TObject);
begin
     DataAssunzione.TCVinVisione.Post;
end;

procedure TInEvidenzaForm.BbCVcanClick(Sender: TObject);
begin
     DataAssunzione.TCVinVisione.Cancel;
end;

end.
