unit AnalisiComp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, ExtCtrls, TeEngine, Series, StdCtrls, TeeProcs, Chart,
  DBChart, Db, DBTables, Buttons, Mask, DBCtrls;

type
  TAnalisiCompForm = class(TForm)
    QTot: TQuery;
    QTotTot: TFloatField;
    TTot: TTable;
    TTotIDAnagrafica: TIntegerField;
    TTotIDMansione: TIntegerField;
    TTotCognome: TStringField;
    TTotNome: TStringField;
    TTotTot: TSmallintField;
    DsTot: TDataSource;
    DBChart1: TDBChart;
    Series1: THorizBarSeries;
    DsQTot: TDataSource;
    Query1: TQuery;
    Q1: TQuery;
    Q1ID: TIntegerField;
    Q1Cognome: TStringField;
    Q1Nome: TStringField;
    Q1IDMansione: TIntegerField;
    Panel1: TPanel;
    Panel2: TPanel;
    DBEdit1: TDBEdit;
    Label1: TLabel;
    DBEdit2: TDBEdit;
    BitBtn1: TBitBtn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  AnalisiCompForm: TAnalisiCompForm;

implementation


{$R *.DFM}




end.
