unit Promemoria;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Wall, TB97, StdCtrls, DBCtrls, Db, Grids, DBGrids, DBTables;

type
  TPromemoriaForm = class(TForm)
    Wallpaper1: TWallpaper;
    DBText1: TDBText;
    Label1: TLabel;
    ToolbarButton971: TToolbarButton97;
    Panel1: TPanel;
    Panel2: TPanel;
    DataSource1: TDataSource;
    Anag1: TTable;
    DBGrid1: TDBGrid;
    Anag1ID: TAutoIncField;
    Anag1Cognome: TStringField;
    Anag1Nome: TStringField;
    Anag1DataNascita: TDateField;
    Anag1GiornoNascita: TSmallintField;
    Anag1MeseNascita: TSmallintField;
    Anag1Eta: TStringField;
    Panel3: TPanel;
    DBGrid2: TDBGrid;
    Label2: TLabel;
    Label3: TLabel;
    Anag2: TTable;
    DataSource2: TDataSource;
    Panel12: TPanel;
    Splitter4: TSplitter;
    Panel14: TPanel;
    Anag2ID: TAutoIncField;
    Anag2Cognome: TStringField;
    Anag2Nome: TStringField;
    Anag2DataNascita: TDateField;
    Anag2GiornoNascita: TSmallintField;
    Anag2MeseNascita: TSmallintField;
    Anag2Eta: TStringField;
    DBGrid3: TDBGrid;
    Wallpaper2: TWallpaper;
    ToolbarButton972: TToolbarButton97;
    procedure ToolbarButton971Click(Sender: TObject);
    procedure Anag1CalcFields(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure Anag2CalcFields(DataSet: TDataSet);
    procedure ToolbarButton972Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    xID:integer;
  end;

var
  PromemoriaForm: TPromemoriaForm;

implementation

uses ModuloDati, Main;

{$R *.DFM}

procedure TPromemoriaForm.ToolbarButton971Click(Sender: TObject);
begin
     MainForm.Show;
     if Data.TPromemoria.RecordCount=0 then
        MainForm.StatusBar1.Panels[2].Text:='nessuna voce � presente nel promemoria';
     if Data.TPromemoria.RecordCount=1 then
        MainForm.StatusBar1.Panels[2].Text:='� presente una voce nel promemoria';
     if Data.TPromemoria.RecordCount>1 then
        MainForm.StatusBar1.Panels[2].Text:='Sono presenti '+IntToStr(Data.TPromemoria.RecordCount)+
                                            ' voci nel promemoria';

     Data.TPromemoria.Filter:='DataLettura=null';
     PromemoriaForm.Free;
end;

procedure TPromemoriaForm.Anag1CalcFields(DataSet: TDataSet);
var xAnno,xMese,xgiorno:Word;
begin
     DecodeDate((Date-Anag1DataNascita.Value),xAnno,xMese,xgiorno);
     Anag1Eta.Value:=copy(IntToStr(xAnno),3,2);
end;

procedure TPromemoriaForm.FormShow(Sender: TObject);
var xAnno,xMese,xgiorno:Word;
begin
     // compleanni di oggi
     DecodeDate(Date,xAnno,xMese,xgiorno);
     Anag1.Filter:='GiornoNascita='+IntToStr(xGiorno)+
                   ' and MeseNascita='+IntToStr(xMese);
     // compleanni di domani
     DecodeDate(Date+1,xAnno,xMese,xgiorno);
     Anag2.Filter:='GiornoNascita='+IntToStr(xGiorno)+
                   ' and MeseNascita='+IntToStr(xMese);
     // Filtri Promemorie
     //Data.TPromemoria.Filter:='IDUtente='+IntToStr(xID)+' and DataDaLeggere='''+DateToStr(Date)+'''';
     Data.TPromemoria.Filter:='(DataLettura=null) and (DataDaLeggere>='''+DateToStr(date)+''')';
     Data.TPromScaduti.Filter:='(DataLettura=null) and (DataDaLeggere<'''+DateToStr(date)+''')';
end;

procedure TPromemoriaForm.Anag2CalcFields(DataSet: TDataSet);
var xAnno,xMese,xgiorno:Word;
begin
     DecodeDate((Date-Anag2DataNascita.Value),xAnno,xMese,xgiorno);
     Anag2Eta.Value:=copy(IntToStr(xAnno),3,2);
end;

procedure TPromemoriaForm.ToolbarButton972Click(Sender: TObject);
begin
     Data.TPromemoria.Edit;
     Data.TPromemoriaDataLettura.Value:=Date;
     Data.TPromemoria.Post;
end;

end.
