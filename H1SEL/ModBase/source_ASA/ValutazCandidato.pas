unit ValutazCandidato;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, TB97, Wall, DBCtrls, StdCtrls, ComCtrls, Mask, Buttons,
  ExtCtrls, TeEngine, Series, TeeProcs, Chart, DBChart, Db, DBTables;

type
  TValutazCandForm = class(TForm)
    Panel1: TPanel;
    DBEdit12: TDBEdit;
    DBEdit14: TDBEdit;
    PageControl1: TPageControl;
    TsDatiAnag: TTabSheet;
    Label38: TLabel;
    Label49: TLabel;
    Label50: TLabel;
    GroupBox1: TGroupBox;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    Label10: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    BitBtn12: TBitBtn;
    DBLookupComboBox1: TDBLookupComboBox;
    DBEdit17: TDBEdit;
    DBEdit23: TDBEdit;
    Panel9: TPanel;
    Label46: TLabel;
    Label47: TLabel;
    Label48: TLabel;
    DBEdit13: TDBEdit;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    Wallpaper3: TWallpaper;
    TbBAnagOK: TToolbarButton97;
    TbBAnagCan: TToolbarButton97;
    TsCompetTutte: TTabSheet;
    Wallpaper6: TWallpaper;
    TbBCompDipNew: TToolbarButton97;
    TbBCompDipDel: TToolbarButton97;
    TbBCompDipOK: TToolbarButton97;
    TbBCompDipCan: TToolbarButton97;
    DBGrid14: TDBGrid;
    TsTutteCar: TTabSheet;
    DBGrid19: TDBGrid;
    Wallpaper7: TWallpaper;
    TbBCarattDipNew: TToolbarButton97;
    TbBCarattDipDel: TToolbarButton97;
    TbBCarattDipOK: TToolbarButton97;
    TbBCarattDipCan: TToolbarButton97;
    Q1: TQuery;
    Q1IDMansione: TIntegerField;
    Q1IDAnagrafica: TIntegerField;
    Q1Descrizione: TStringField;
    Q1Tipologia: TStringField;
    Q1Peso: TSmallintField;
    Q1ValRichiesto: TIntegerField;
    Q1ValNome: TIntegerField;
    Q1Punteggio: TFloatField;
    DsQ1: TDataSource;
    TSCompRuolo: TTabSheet;
    Wallpaper1: TWallpaper;
    ToolbarButton972: TToolbarButton97;
    Panel4: TPanel;
    DBChart3: TDBChart;
    BarSeries1: TBarSeries;
    Panel5: TPanel;
    DBGrid2: TDBGrid;
    DBChart4: TDBChart;
    BarSeries3: TBarSeries;
    BarSeries2: TLineSeries;
    QTot: TQuery;
    QTotTot: TFloatField;
    TCompAnagIDX: TTable;
    TCompAnagIDXID: TAutoIncField;
    TCompAnagIDXIDCompetenza: TIntegerField;
    TCompAnagIDXIDAnagrafica: TIntegerField;
    TCompAnagIDXValore: TIntegerField;
    Q1IDCompetenza: TIntegerField;
    BitBtn20: TBitBtn;
    BitBtn1: TBitBtn;
    ToolbarButton971: TToolbarButton97;
    PanRuolo: TPanel;
    BitBtn3: TBitBtn;
    ToolbarButton973: TToolbarButton97;
    procedure TbBCompDipNewClick(Sender: TObject);
    procedure TbBCompDipDelClick(Sender: TObject);
    procedure TbBCompDipOKClick(Sender: TObject);
    procedure TbBCompDipCanClick(Sender: TObject);
    procedure TbBAnagOKClick(Sender: TObject);
    procedure TbBAnagCanClick(Sender: TObject);
    procedure TbBCarattDipNewClick(Sender: TObject);
    procedure TbBCarattDipOKClick(Sender: TObject);
    procedure TbBCarattDipCanClick(Sender: TObject);
    procedure TbBCarattDipDelClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ToolbarButton972Click(Sender: TObject);
    procedure BitBtn20Click(Sender: TObject);
    procedure ToolbarButton971Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ToolbarButton973Click(Sender: TObject);
  private
    { Private declarations }
    xVecchioVal:integer;
  public
    { Public declarations }
  end;

var
  ValutazCandForm: TValutazCandForm;

implementation

uses Curriculum, ModuloDati, ModuloDati2, StoricoComp,
     NuovaComp, InsCompetenza;

{$R *.DFM}


procedure TValutazCandForm.TbBCompDipNewClick(Sender: TObject);
var i,xVal:integer;
    Vero:boolean;
begin
      InsCompetenzaForm:=TInsCompetenzaForm.create(self);
      InsCompetenzaForm.RiempiGriglia;
      InsCompetenzaForm.ShowModal;
      if InsCompetenzaForm.ModalResult=mrOK then begin
       for i:=1 to InsCompetenzaForm.ASG1.RowCount-1 do begin
         InsCompetenzaForm.ASG1.GetCheckBoxState(0,i,Vero);
         if Vero then begin
            Data2.TAnagCompIDX.Open;
            if not Data2.TAnagCompIDX.FindKey([Data.TAnagraficaID.value,InsCompetenzaForm.xArrayIDComp[i]]) then begin
               Data2.TAnagCompIDX.InsertRecord([Data.TAnagraficaID.value,InsCompetenzaForm.xArrayIDComp[i],InsCompetenzaForm.xArrayCompVal[i]]);
               Data2.TStoricoCompABS.Open;
               Data2.TStoricoCompABS.InsertRecord([InsCompetenzaForm.xArrayIDComp[i],Data.TAnagraficaID.value,Date,
                                                   'valore iniziale',null,InsCompetenzaForm.xArrayCompVal[i],0]);
               Data2.TStoricoCompABS.Close;
            end;
            Data2.TAnagCompIDX.Close;
         end;
       end;

      end;
      InsCompetenzaForm.Free;
      if Data2.TCompDipendente.Active then Data2.TCompDipendente.refresh;

     Q1.Close;
     Q1.Open;
     QTot.Close;
     QTot.Open;
end;

procedure TValutazCandForm.TbBCompDipDelClick(Sender: TObject);
begin
     if MessageDlg('Sei sicuro di voler cancellare ?',mtWarning,
        [mbNo,mbYes],0)=mrYes then Data2.TCompDipendente.delete;
     Q1.Close;
     Q1.Open;
     QTot.Close;
     QTot.Open;
end;

procedure TValutazCandForm.TbBCompDipOKClick(Sender: TObject);
begin
     Data2.TCompDipendente.Post;
     Q1.Close;
     Q1.Open;
     QTot.Close;
     QTot.Open;
end;

procedure TValutazCandForm.TbBCompDipCanClick(Sender: TObject);
begin
     Data2.TCompDipendente.Cancel;
end;

procedure TValutazCandForm.TbBAnagOKClick(Sender: TObject);
begin
     Data.TAnagrafica.Post;
end;

procedure TValutazCandForm.TbBAnagCanClick(Sender: TObject);
begin
     Data.TAnagrafica.Cancel;
end;

procedure TValutazCandForm.TbBCarattDipNewClick(Sender: TObject);
begin
     Data2.TCarattDip.Insert;
end;

procedure TValutazCandForm.TbBCarattDipOKClick(Sender: TObject);
begin
     Data2.TCarattDip.Post;
end;

procedure TValutazCandForm.TbBCarattDipCanClick(Sender: TObject);
begin
     Data2.TCarattDip.Cancel;
end;

procedure TValutazCandForm.TbBCarattDipDelClick(Sender: TObject);
begin
     if MessageDlg('Sei sicuro di voler cancellare ?',mtWarning,
        [mbNo,mbYes],0)=mrYes then Data2.TCarattDip.delete;
end;

procedure TValutazCandForm.FormShow(Sender: TObject);
begin
     Data2.TCompDipendente.Open;
     Data.TAnagMansioni.Open;
     PageControl1.ActivePage:=TSCompRuolo;
     Data2.TCarattDip.Open;
end;

procedure TValutazCandForm.ToolbarButton972Click(Sender: TObject);
var xVal:string;
    xVecchioVal,xVarPerc:integer;
begin
   if not Q1.EOF then begin
     TCompAnagIDX.FindKey([Data.TAnagraficaID.Value,Q1IDCompetenza.Value]);
      NuovaCompForm:=TNuovaCompForm.create(self);
      NuovaCompForm.xComp:=Q1Descrizione.Value;
      NuovaCompForm.SEVecchioVal.Value:=TCompAnagIDXValore.asInteger;
      NuovaCompForm.SENuovoVal.Value:=TCompAnagIDXValore.asInteger;
      NuovaCompForm.xMot:='a seguito di valutazione';
      NuovaCompForm.DEData.Date:=Date;
      NuovaCompForm.ShowModal;
      if NuovaCompForm.ModalResult=mrOK then begin
          TCompAnagIDX.Edit;
          TCompAnagIDXValore.Value:=NuovaCompForm.SENuovoVal.Value;
          TCompAnagIDX.Post;
          xVecchioVal:=NuovaCompForm.SEVecchioVal.Value;
          if xVecchioVal>0 then
             xVarPerc:=Trunc(((NuovaCompForm.SENuovoVal.Value-xVecchioVal)/xVecchioVal)*100)
          else xVarPerc:=NuovaCompForm.SENuovoVal.Value*100;
             Data2.TStoricoCompABS.Open;
             Data2.TStoricoCompABS.InsertRecord([Q1IDCompetenza.Value,
                                      Q1IDAnagrafica.Value,
                                      NuovaCompForm.DEData.Date,
                                      NuovaCompForm.xMot,
                                      null,
                                      TCompAnagIDXValore.Value,
                                      xVarPerc]);
             Data2.TStoricoCompABS.Close;
          Q1.Close;
          Q1.Open;
          QTot.Close;
          QTot.Open;
          Data2.TCompDipendente.Refresh;
      end;
      NuovaCompForm.Free;

     Q1.close;
     Q1.SQL.Clear;
     Q1.SQL.LoadFromFile(ExtractFilePath(Application.ExeName)+'QCompRuolo.txt');
     Q1.SQL.Add('AND CompetenzeMansioni.IDMansione='+Data.TAnagMansioniIDMansione.asString);
     Q1.SQL.Add('AND CompetenzeAnagrafica.IDAnagrafica='+Data.TAnagraficaID.asString);
     Q1.Open;
     QTot.close;
     QTot.SQL.Clear;
     QTot.SQL.LoadFromFile(ExtractFilePath(Application.ExeName)+'QCompRuoloTot.txt');
     QTot.SQL.Add('AND CompetenzeMansioni.IDMansione='+Data.TAnagMansioniIDMansione.asString);
     QTot.SQL.Add('AND CompetenzeAnagrafica.IDAnagrafica='+Data.TAnagraficaID.asString);
     QTot.Open;
     Data2.TCompDipendente.Refresh;
   end;
end;

procedure TValutazCandForm.BitBtn20Click(Sender: TObject);
var x:string;
    i:integer;
begin
     Data.TTitoliStudio.Open;
     Data.TCorsiExtra.Open;
     Data.TLingueConosc.Open;
     Data.TEspLav.Open;
     // azzeramento checkbox
     for i:=0 to 6 do CurriculumForm.CheckListBox1.Checked[i]:=False;
     // costruzione checklistbox
     x:=Data.TAnagraficaDisponibContratti.AsString;
     i:=1;
     while copy(x,i,1)<>'' do begin
         CurriculumForm.CheckListBox1.Checked[StrToInt(copy(x,i,1))]:=true;
         inc(i);
     end;
     CurriculumForm.ShowModal;
     // risistemazione campo disp.contratto
     if CurriculumForm.xModificata then begin
        x:='';
        for i:=0 to 6 do
          if CurriculumForm.CheckListBox1.Checked[i] then
             x:=x+IntToStr(i);
        Data.TAnagrafica.Edit;
        Data.TAnagraficaDisponibContratti.AsString:=x;
        Data.TAnagrafica.Post;
     end;

     Data.TTitoliStudio.Close;
     Data.TCorsiExtra.Close;
     Data.TLingueConosc.Close;
     Data.TEspLav.Close;
end;

procedure TValutazCandForm.ToolbarButton971Click(Sender: TObject);
begin
     if Data2.TCompDipendente.RecordCount>0 then begin
        StoricoCompForm:=TStoricoCompForm.create(self);
        StoricoCompForm.ShowModal;
        StoricoCompForm.Free;
     end;
end;

procedure TValutazCandForm.BitBtn2Click(Sender: TObject);
begin
    if Data.TAnagMansioni.RecordCount>0 then begin
     QRSchedaValCand:=TQRSchedaValCand.create(self);
     QRSchedaValCand.QRLRuolo.Caption:=PanRuolo.Caption;
     try QRSchedaValCand.Preview except
         ShowMessage('problemi con il Report') end;
     QRSchedaValCand.Free;
    end;
end;

procedure TValutazCandForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     Data2.TCarattDip.Close;
     Data2.TCompDipendente.Close;
     Data.TAnagMansioni.Close;
end;

procedure TValutazCandForm.ToolbarButton973Click(Sender: TObject);
begin
     Data2.TCompDipendente.Edit;
     Data2.TCompDipendente.Post;
end;

end.
