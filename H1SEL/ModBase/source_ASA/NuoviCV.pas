unit NuoviCV;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     Db,Grids,DBGrids,DBTables,ExtCtrls,Buttons,ComCtrls,StdCtrls,
     DtEdit97,ImageWindow,ImgList,Menus,OleCtrls,Spin;

type
     TNuoviCVForm=class(TForm)
          QRicAttiveSel: TQuery;
          DsQRicAttiveSel: TDataSource;
          TSelez: TTable;
          dsSelez: TDataSource;
          DBGrid1: TDBGrid;
          TSelezNominativo: TStringField;
          TSelezID: TAutoIncField;
          QPerRuolo: TQuery;
          QAnagMans: TQuery;
          ImageList1: TImageList;
          SpeedButton2: TSpeedButton;
          BitBtn1: TBitBtn;
          PopupMenu1: TPopupMenu;
          Eliminaassociazione1: TMenuItem;
          TRicABS: TTable;
          TRicABSID: TAutoIncField;
          TRicABSIDMansione: TIntegerField;
          PopupMenu2: TPopupMenu;
          Infocandidato1: TMenuItem;
          curriculum1: TMenuItem;
          documenti1: TMenuItem;
          fileWord1: TMenuItem;
          Q: TQuery;
          QPerArea: TQuery;
          QAnagArea: TQuery;
          Panel5: TPanel;
          Panel1: TPanel;
          Panel8: TPanel;
          LSel: TLabel;
          TVRic: TTreeView;
          Panel2: TPanel;
          Panel3: TPanel;
          TV: TTreeView;
          Panel4: TPanel;
          SpeedButton1: TSpeedButton;
          Label1: TLabel;
          Label2: TLabel;
          DEDataDal: TDateEdit97;
          DEDataAl: TDateEdit97;
          Splitter1: TSplitter;
          QTabelleLista: TQuery;
          QRes1: TQuery;
          QTabelleListaTabella: TStringField;
          TLinee: TQuery;
          TLineeID: TAutoIncField;
          TLineeIDRicerca: TIntegerField;
          TLineeDescrizione: TStringField;
          TLineeStringa: TStringField;
          TLineeTabella: TStringField;
          TLineeOpSucc: TStringField;
          TLineeNext: TStringField;
          RGOpzioni: TRadioGroup;
          procedure SpeedButton1Click(Sender: TObject);
          procedure SpeedButton2Click(Sender: TObject);
          procedure TVRicDragOver(Sender,Source: TObject; X,Y: Integer;
               State: TDragState; var Accept: Boolean);
          procedure TVRicDragDrop(Sender,Source: TObject; X,Y: Integer);
          procedure BitBtn1Click(Sender: TObject);
          procedure TVRicClick(Sender: TObject);
          procedure Eliminaassociazione1Click(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure Infocandidato1Click(Sender: TObject);
          procedure curriculum1Click(Sender: TObject);
          procedure documenti1Click(Sender: TObject);
          procedure fileWord1Click(Sender: TObject);
          procedure FormCreate(Sender: TObject);
          procedure FormDestroy(Sender: TObject);
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
     private
          function ComponiQuery(xIDRic: integer): boolean;
     public
          { Public declarations }
     end;

var
     NuoviCVForm: TNuoviCVForm;

implementation

uses ModuloDati,Main,Splash3,SchedaCand,Curriculum,View,uUtilsVarie;

{$R *.DFM}

procedure TNuoviCVForm.SpeedButton1Click(Sender: TObject);
var N,N1: TTreeNode;
begin
     case RGOpzioni.ItemIndex of
          0: begin
                    TV.Items.BeginUpdate;
                    TV.Items.Clear;
                    QPerArea.Open;
                    while not QPerArea.EOF do begin
                         QAnagArea.Close;
                         QAnagArea.ParamByName('xIDArea').asInteger:=QPerArea.FieldByName('ID').asInteger;
                         QAnagArea.ParamByName('xDal').asDateTime:=DEDataDal.Date;
                         QAnagArea.ParamByName('xAl').asDateTime:=DEDataAl.Date;
                         QAnagArea.Open;
                         if not QAnagArea.IsEmpty then begin
                              N:=TV.Items.AddChildObject(nil,QPerArea.FieldByName('Area').asString,pointer(QPerArea.FieldByName('ID').asInteger));
                              N.ImageIndex:=3; N.SelectedIndex:=3; N.StateIndex:=3;
                              while not QAnagArea.EOF do begin
                                   N1:=TV.Items.AddChildObject(N,QAnagArea.FieldByName('Cognome').asString+' '+QAnagArea.FieldByName('Nome').asString,pointer(QAnagArea.FieldByName('ID').asInteger));
                                   N1.ImageIndex:=2; N1.SelectedIndex:=2; N1.StateIndex:=2;
                                   QAnagArea.Next;
                              end;
                         end;
                         QPerArea.Next;
                    end;
                    QPerArea.Close;
                    TV.Items.EndUpdate;
               end;
          1: begin
                    TV.Items.BeginUpdate;
                    TV.Items.Clear;
                    QPerRuolo.Open;
                    while not QPerRuolo.EOF do begin
                         QAnagMans.Close;
                         QAnagMans.ParamByName('xIDMans').asInteger:=QPerRuolo.FieldByName('ID').asInteger;
                         QAnagMans.ParamByName('xDal').asDateTime:=DEDataDal.Date;
                         QAnagMans.ParamByName('xAl').asDateTime:=DEDataAl.Date;
                         QAnagMans.Open;
                         if not QAnagMans.IsEmpty then begin
                              N:=TV.Items.AddChildObject(nil,QPerRuolo.FieldByName('Ruolo').asString,pointer(QPerRuolo.FieldByName('ID').asInteger));
                              N.ImageIndex:=3; N.SelectedIndex:=3; N.StateIndex:=3;
                              while not QAnagMans.EOF do begin
                                   N1:=TV.Items.AddChildObject(N,QAnagMans.FieldByName('Cognome').asString+' '+QAnagMans.FieldByName('Nome').asString,pointer(QAnagMans.FieldByName('ID').asInteger));
                                   N1.ImageIndex:=2; N1.SelectedIndex:=2; N1.StateIndex:=2;
                                   QAnagMans.Next;
                              end;
                         end;
                         QPerRuolo.Next;
                    end;
                    QPerRuolo.Close;
                    TV.Items.EndUpdate;
               end;
          2: begin
                    TV.Items.BeginUpdate;
                    TV.Items.Clear;
                    if not QRicAttiveSel.Active then QRicAttiveSel.Open
                    else QRicAttiveSel.First;
                    while not QRicAttiveSel.EOF do begin
                         if ComponiQuery(QRicAttiveSel.FieldByName('ID').asInteger) then begin
                              N:=TV.Items.AddChildObject(nil,QRicAttiveSel.FieldByName('Rif').asString+' '+QRicAttiveSel.FieldByName('Cliente').asString,pointer(QRicAttiveSel.FieldByName('ID').asInteger));
                              N.ImageIndex:=3; N.SelectedIndex:=3; N.StateIndex:=3;
                              while not QRes1.EOF do begin
                                   N1:=TV.Items.AddChildObject(N,QRes1.FieldByName('Cognome').asString+' '+QRes1.FieldByName('Nome').asString,pointer(QRes1.FieldByName('ID').asInteger));
                                   N1.ImageIndex:=2; N1.SelectedIndex:=2; N1.StateIndex:=2;
                                   QRes1.Next;
                              end;
                              TV.Items.EndUpdate;
                         end;
                         QRicAttiveSel.Next;
                    end;
               end;
     end;
end;

procedure TNuoviCVForm.SpeedButton2Click(Sender: TObject);
var N,N1: TTreeNode;
begin
     TVRic.Items.BeginUpdate;
     TVRic.Items.Clear;
     if not QRicAttiveSel.Active then QRicAttiveSel.Open;

     while not QRicAttiveSel.EOF do begin
          N:=TVRic.Items.AddChildObject(nil,QRicAttiveSel.FieldByName('Rif').asString+'-'+QRicAttiveSel.FieldByName('Cliente').asString+' ('+
               QRicAttiveSel.FieldByName('Ruolo').asString+' - '+QRicAttiveSel.FieldByName('Area').asString+' )',
               pointer(QRicAttiveSel.FieldByName('ID').asInteger));
          N.ImageIndex:=5; N.SelectedIndex:=5; N.StateIndex:=5;
          QRicAttiveSel.Next;
     end;
     QRicAttiveSel.Close;
     TVRic.Items.EndUpdate;
     Lsel.caption:=TSelezNominativo.Value;
end;

procedure TNuoviCVForm.TVRicDragOver(Sender,Source: TObject; X,
     Y: Integer; State: TDragState; var Accept: Boolean);
begin
     Accept:=Sender<>Source;
end;

procedure TNuoviCVForm.TVRicDragDrop(Sender,Source: TObject; X,
     Y: Integer);
var T,N: TTreeNode;
     i: integer;
     xEsisteGia: boolean;
begin
     if (TV.Selected.Level=1)and(TVRic.DropTarget.Level=0) then begin
          // controllo ruolo
          if RGOpzioni.ItemIndex=1 then begin
               TRicABS.Open;
               TRicABS.FindKey([longint(TVRic.DropTarget.Data)]);
               if longint(TV.Selected.Parent.Data)<>TRicABSIDMansione.Value then
                    if MessageDlg('Attenzione: i ruoli non coincidono - proseguire ?',mtwarning, [mbYes,mbNo],0)=mrNo then
                         Exit;
               TRicABS.Close;
          end;
          // controllo esistenza
          xEsisteGia:=false;
          if TVRic.DropTarget.HasChildren then begin
               N:=TVRic.DropTarget.getFirstChild;
               for i:=1 to TVRic.DropTarget.Count do begin
                    if longint(N.Data)=longint(TV.Selected.Data) then begin
                         xEsisteGia:=True;
                         Break;
                    end;
                    N:=N.GetNext;
               end;
          end;
          if not xEsisteGia then begin
               N:=TVRic.Items.AddChildObject(TVRic.DropTarget,TV.Selected.text,pointer(longint(TV.Selected.Data)));
               N.Expand(false);
               N.ImageIndex:=2; N.SelectedIndex:=2; N.StateIndex:=2;
          end;
     end;
end;

procedure TNuoviCVForm.BitBtn1Click(Sender: TObject);
var Root,N,N1: TTreeNode;
     i,k,xIDRicerca,xIDCliente,xIDUtente: integer;
     xProg,xCliente: string;
begin
     TRicABS.open;
     N:=TVRic.TopItem;
     for i:=1 to TVRic.Items.count do begin
          if N.level=0 then
               xIDRicerca:=longint(N.Data)
          else begin
               // se � un candidato >> prova ad inserirlo
               if Q.Active then Q.Close;
               Q.SQL.text:='select ID from EBC_CandidatiRicerche where IDRicerca='+IntToStr(xIDRicerca)+' and IDAnagrafica='+IntToStr(longint(N.Data));
               Q.Open;
               if Q.RecordCount=0 then begin
                    Q.Close;
                    Data.DB.BeginTrans;
                    try
                         Q.SQL.text:='insert into EBC_CandidatiRicerche (IDRicerca,IDAnagrafica,Escluso,Stato,DataIns) '+
                              'values (:xIDRicerca,:xIDAnagrafica,:xEscluso,:xStato,:xDataIns)';
                         Q.ParamByName('xIDRicerca').asInteger:=xIDRicerca;
                         Q.ParamByName('xIDAnagrafica').asInteger:=longint(N.Data);
                         Q.ParamByName('xEscluso').asBoolean:=False;
                         Q.ParamByName('xStato').asString:='inserito da Nuovi CV';
                         Q.ParamByName('xDataIns').asDateTime:=Date;
                         Q.ExecSQL;
                         // aggiornamento stato anagrafica
                         if Q.Active then Q.Close;
                         Q.SQL.text:='update Anagrafica set IDStato=27,IDTipoStato=1 where ID='+IntToStr(longint(N.Data));
                         Q.ExecSQL;
                         // aggiornamento storico
                         if Q.Active then Q.Close;
                         Q.SQL.text:='select EBC_Ricerche.Progressivo,EBC_Clienti.Descrizione Cliente,EBC_Ricerche.IDCliente,EBC_Ricerche.IDUtente '+
                              'from EBC_Ricerche,EBC_Clienti where EBC_Ricerche.IDCliente=EBC_Clienti.ID and EBC_Ricerche.ID='+IntToStr(xIDRicerca);
                         Q.Open;
                         xProg:=Q.FieldByName('Progressivo').asString;
                         xCliente:=Q.FieldByName('Cliente').asString;
                         xIDUtente:=Q.FieldByName('IDUtente').asInteger;
                         xIDCliente:=Q.FieldByName('IDCliente').asInteger;
                         Q.Close;
                         Q.SQL.text:='insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) '+
                              'values (:xIDAnagrafica,:xIDEvento,:xDataEvento,:xAnnotazioni,:xIDRicerca,:xIDUtente,:xIDCliente)';
                         Q.ParamByName('xIDAnagrafica').asInteger:=longint(N.Data);
                         Q.ParamByName('xIDEvento').asInteger:=19;
                         Q.ParamByName('xDataEvento').asDateTime:=date;
                         Q.ParamByName('xAnnotazioni').asString:='ric.n�'+xProg+' ('+xCliente+')';
                         Q.ParamByName('xIDRicerca').asInteger:=xIDRicerca;
                         Q.ParamByName('xIDUtente').asInteger:=xIDUtente;
                         Q.ParamByName('xIDCliente').asInteger:=xIDCliente;
                         Q.ExecSQL;
                         Data.DB.CommitTrans;
                    except
                         Data.DB.RollbackTrans;
                         MessageDlg('ERRORE SUL DATABASE:  operazione non completata',mtError, [mbOK],0);
                    end;
                    N.ImageIndex:=6; N.SelectedIndex:=6; N.StateIndex:=6;
               end else begin N.ImageIndex:=7; N.SelectedIndex:=7; N.StateIndex:=7; end;
          end;
          N:=N.GetNext;
     end;
     ShowMessage('Operazione completata:'+chr(13)+
          'i candidati con l''icona VERDE sono stati associati alla ricerca,'+chr(13)+
          'quelli con l''icona ROSSA erano gi� stati associati alla medesima ricerca');
     TRicABS.Close;
end;

procedure TNuoviCVForm.TVRicClick(Sender: TObject);
begin
     if TVRic.Selected.Level<>1 then Eliminaassociazione1.Enabled:=False
     else Eliminaassociazione1.Enabled:=True;
end;

procedure TNuoviCVForm.Eliminaassociazione1Click(Sender: TObject);
begin
     TVRic.Selected.Delete;
end;

procedure TNuoviCVForm.FormShow(Sender: TObject);
begin
     Caption:='[M/25] - '+caption;
     Splash3Form:=TSplash3Form.Create(nil);
     Splash3Form.Show;
     Splash3Form.Update;
     TSelez.FindKey([mainForm.xIDUtenteAttuale]);
     DEDataDal.date:=Date-3;
     DEDataAl.date:=Date;
     SpeedButton2Click(self);
     //SpeedButton1Click(self);
     Splash3Form.Hide;
     Splash3Form.Free;
end;

procedure TNuoviCVForm.Infocandidato1Click(Sender: TObject);
begin
     if TV.Selected.level=0 then exit;
     SchedaCandForm:=TSchedaCandForm.create(self);
     if not PosizionaAnag(longint(TV.Selected.Data)) then exit;
     SchedaCandForm.ShowModal;
     SchedaCandForm.Free;
end;

procedure TNuoviCVForm.curriculum1Click(Sender: TObject);
var x: string;
     i: integer;
begin
     if TV.Selected.level=0 then exit;
     PosizionaAnag(longint(TV.Selected.Data));

     Data.TTitoliStudio.Open;
     Data.TCorsiExtra.Open;
     Data.TLingueConosc.Open;
     Data.TEspLav.Open;
     CurriculumForm.ShowModal;

     Data.TTitoliStudio.Close;
     Data.TCorsiExtra.Close;
     Data.TLingueConosc.Close;
     Data.TEspLav.Close;
     MainForm.Pagecontrol5.ActivePage:=MainForm.TSStatoTutti;
end;

procedure TNuoviCVForm.documenti1Click(Sender: TObject);
var xFile: string;
     xProcedi: boolean;
     xCVNumero: string;
begin
     if TV.Selected.level=0 then exit;
     ApriCV(longint(TV.Selected.Data));
end;

procedure TNuoviCVForm.fileWord1Click(Sender: TObject);
begin
     if not PosizionaAnag(longint(TV.Selected.Data)) then exit;
     ApriFileWord(Data.TAnagraficaCVNumero.AsString);
end;

procedure TNuoviCVForm.FormCreate(Sender: TObject);
begin
     MainForm.xNuoviCVFormCreated:=true;
end;

procedure TNuoviCVForm.FormDestroy(Sender: TObject);
begin
     MainForm.xNuoviCVFormCreated:=False;
end;

procedure TNuoviCVForm.FormClose(Sender: TObject;
     var Action: TCloseAction);
begin
     NuoviCVForm.Free;
end;

function TNuoviCVForm.ComponiQuery(xIDRic: integer): boolean;
var xTab: array[1..5] of string;
     i,j,k: integer;
     xS,xS1,xd,xLineeStringa: string;
     xApertaPar,xEspLav: boolean;
begin
     // tabelle implicate tranne Anagrafica
     QTabelleLista.Close;
     QTabelleLista.SQL.clear;
     QTabelleLista.SQL.add('select distinct Tabella from RicLineeQuery ');
     QTabelleLista.SQL.add('where Tabella<>''Anagrafica'' and IDRicerca='+IntToStr(xIDRic));
     QTabelleLista.Open;
     if QTabelleLista.IsEmpty then begin
          result:=False;
          exit;
     end;
     QTabelleLista.First;
     for i:=1 to 5 do xTab[i]:='';
     i:=1;
     while not QTabelleLista.EOF do begin
          xTab[i]:=QTabelleListaTabella.AsString;
          QTabelleLista.Next; inc(i);
     end;

     xEspLav:=False;
     xS:='from Anagrafica,AnagAltreInfo';
     k:=1;
     while k<=5 do begin
          if (xTab[k]<>'')and(UpperCase(xTab[k])<>'ANAGALTREINFO') then
               xS:=xs+','+xTab[k];
          if xTab[k]='AnagCampiPers' then
               xS:=xS+',CampiPers';
          inc(k);
     end;

     // linee query
     if TLinee.Active then TLinee.Close;
     TLinee.ParamByName('xIDRIc').asInteger:=xIDRic;
     TLinee.Open;

     QRes1.Close;
     QRes1.SQL.Clear;

     QRes1.SQL.Add('select distinct Anagrafica.ID,Anagrafica.Cognome,Anagrafica.Nome');
     QRes1.SQL.Add(xS);
     QRes1.SQL.Add('where Anagrafica.ID is not null and Anagrafica.ID=AnagAltreInfo.IDAnagrafica');


     xS1:='';
     for i:=1 to 5 do begin
          //if (xTab[i]<>'')and(xTab[i]<>'EsperienzeLavorative') then
          if xTab[i]<>'' then
               if xTab[i]='' then
                    xS1:=xS1+' and Anagrafica.ID='+xTab[i]+'.IDAnagrafica and CampiPers.ID = AnagCampiPers.IDCampo'
               else xS1:=xS1+' and Anagrafica.ID='+xTab[i]+'.IDAnagrafica';
     end;
     QRes1.SQL.Add(xS1);
     if TLinee.RecordCount>0 then
          QRes1.SQL.Add('and');

     TLinee.First;
     xLineeStringa:='';
     xApertaPar:=False;
     while not TLinee.EOF do begin
          xLineeStringa:=TLineeStringa.asString;
          // per le LINGUE --> apposita clausola
          if copy(TLineeDescrizione.Value,1,6)='lingua' then
               // ## OLD ## xLineeStringa:='Anagrafica.ID in (select IDAnagrafica from LingueConosciute where lingua='''+copy(TLineeDescrizione.Value,9,Length(TLineeDescrizione.Value))+''')';
               xLineeStringa:='Anagrafica.ID in (select IDAnagrafica from LingueConosciute where '+TLineeStringa.Value+')';
          TLinee.Next;
          if TLinee.Eof then
               // ultimo record -> senza AND alla fine
               if xApertaPar then QRes1.SQL.Add(' '+xLineeStringa+')')
               else QRes1.SQL.Add(' '+xLineeStringa)
          else begin
               TLinee.Prior;
               if TLineeOpSucc.AsString='and' then
                    if not xApertaPar then
                         QRes1.SQL.Add(' '+xLineeStringa+' and ')
                    else begin
                         QRes1.SQL.Add(' '+xLineeStringa+') and ');
                         xApertaPar:=False;
                    end;
               if TLineeOpSucc.AsString='or' then
                    if not xApertaPar then begin
                         QRes1.SQL.Add(' ('+xLineeStringa+' or ');
                         xApertaPar:=True;
                    end else begin
                         QRes1.SQL.Add(' '+xLineeStringa+' or ');
                         xApertaPar:=True;
                    end;
          end;
          TLinee.Next;
     end;

     QRes1.SQL.Add('and CVInseritoInData>=:xDal and CVInseritoInData<=:xAl');
     QRes1.ParamByName('xDal').asDateTime:=DEDataDal.Date;
     QRes1.ParamByName('xAl').asDateTime:=DEDataAl.Date;
     QRes1.SQL.Add('order by cognome');

     // cancellare
     QRes1.SQL.SaveToFile('c:\QNuoviCV.txt');
     try
          QRes1.Open;
          if QRes1.IsEmpty then result:=False
          else result:=true;
     except
          result:=False;
          QRes1.SQL.SaveToFile('c:\QNuoviCV-ERROR-'+IntToStr(xIDRic)+'.txt');
     end;
end;

end.

