�
 TINSEVENTOFORM 0E  TPF0TInsEventoFormInsEventoFormLeftHToplBorderStylebsDialogCaptionInserimento EventoClientHeight�ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnCreate
FormCreateOnShowFormShowPixelsPerInch`
TextHeight TLabelLabel1LeftTop7Width>HeightCaptionData evento:  TLabelLabel2LeftTopLWidthsHeightCaptionDescrizione/Annotazioni  TPanelPanel1Left Top Width�Height*AlignalTop
BevelOuterbvNoneTabOrder  TBitBtnBitBtn1Left6TopWidthVHeight!TabOrder KindbkOK  TBitBtnBitBtn2Left�TopWidthVHeight!CaptionAnnullaTabOrderKindbkCancel   TDateEdit97DEDataLeftITop3WidthqHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderColorCalendar.ColorValidclBlueDayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanClear
doCanPopup
doIsMaskeddoShowCanceldoShowToday   TDBGridDBGrid1LeftTop� Width�Height� 
DataSourceDsEventiOptionsdgTitlesdgIndicatordgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgConfirmDeletedgCancelOnExit TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameDaStatoTitle.CaptionDallo statoWidth� Visible	 Expanded	FieldNameEventoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold Width� Visible	 Expanded	FieldNameAStatoTitle.Caption
Allo statoWidth� Visible	    TPanelPanel19LeftTopzWidth�Height	AlignmenttaLeftJustifyCaption  Elenco eventiColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TEditEDescLeftTop\WidthaHeightTabOrder  TDBGridDBGrid2LeftTopvWidthHeight� 
DataSourceDsUsersOptionsdgTitlesdgIndicatordgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgConfirmDeletedgCancelOnExit TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldName
NominativoWidth^Visible	 Expanded	FieldNameDescrizioneWidth� Visible	    TPanelPanel2LeftTop`WidthHeight	AlignmenttaLeftJustifyCaption-  Utente (tra quelli abilitati e non scaduti)ColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TTableTEventiActive	DatabaseNameEBCDB	IndexName
IdxDaStatoReadOnly		TableNamedbo.EBC_EventiLeftTop�  TAutoIncField	TEventiID	FieldNameIDReadOnly	  TIntegerFieldTEventiIDdaStato	FieldName	IDdaStato  TStringFieldTEventiEvento	FieldNameEventoSize  TIntegerFieldTEventiIDaStato	FieldNameIDaStato  TBooleanFieldTEventiNonCancellabile	FieldNameNonCancellabile  TIntegerFieldTEventiIDProcedura	FieldNameIDProcedura  TStringFieldTEventiDaStato	FieldKindfkLookup	FieldNameDaStatoLookupDataSetTStatiLKLookupKeyFieldsIDLookupResultFieldStato	KeyFields	IDdaStatoSizeLookup	  TStringFieldTEventiAStato	FieldKindfkLookup	FieldNameAStatoLookupDataSetTStatiLKLookupKeyFieldsIDLookupResultFieldStato	KeyFieldsIDaStatoSizeLookup	   TDataSourceDsEventiDataSetTEventiLeft4Top�   TTableTStatiLKActive	DatabaseNameEBCDB	TableNamedbo.EBC_StatiLefttTop�  TAutoIncField
TStatiLKID	FieldNameIDReadOnly	  TStringFieldTStatiLKStato	FieldNameStatoSize   TDataSourceDsUsersDataSetQUsersLeft?Top�  TQueryQUsersDatabaseNameEBCDBSQL.Strings select ID,Nominativo,Descrizionefrom Users =where not ((DataScadenza is not null and DataScadenza<:xoggi)4   or (DataRevoca is not null and DataRevoca<:xoggi)   or DataCreazione is null   or Tipo=0)order by Nominativo  Left?Top�	ParamDataDataType	ftUnknownNamexOggi	ParamType	ptUnknown DataType	ftUnknownNamexoggi	ParamType	ptUnknown   TIntegerFieldQUsersID	FieldNameIDOrigin
"Users".ID  TStringFieldQUsersNominativo	FieldName
NominativoOrigin"Users".NominativoSize  TStringFieldQUsersDescrizione	FieldNameDescrizioneOrigin"Users".DescrizioneSize2    