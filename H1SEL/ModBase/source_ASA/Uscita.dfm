object UscitaForm: TUscitaForm
  Left = 256
  Top = 106
  ActiveControl = BitBtn1
  BorderStyle = bsDialog
  Caption = 'Uscita dal programma'
  ClientHeight = 204
  ClientWidth = 355
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Image1: TImage
    Left = 2
    Top = 3
    Width = 33
    Height = 34
    Picture.Data = {
      055449636F6E0000010001002020100000000000E80200001600000028000000
      2000000040000000010004000000000080020000000000000000000000000000
      0000000000000000000080000080000000808000800000008000800080800000
      C0C0C000808080000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000
      FFFFFF0000000000000000000000000000000000000000000007070404040000
      0000000000000000007060000040400000000000000000000066022222040000
      0000000000000000007022222220400000000000000000000460222222200400
      0000000000000000607022222220404000000000000004040460222222200404
      0400000000007767607002222200704047700000000000000466000000064400
      0000000000000000007060404040700000000000000000000066060404040000
      0000000000000000007060000040400000000000000000000066033333040000
      0000000000000000007033333330400000000000000000000460333333300400
      0000000000000000607033333330404000000000000004040460333333300404
      0400000000007767607003333300704047700000000000000466000000064400
      0000000000000000007060404040700000000000000000000066060404040000
      000000000000000000706000004040000000000000000000006609F9F9040000
      000000000000000000709F9B9F9040000000000000000000046099B9B9900400
      000000000000000060709F9B9F9040400000000000000404046099F9F9900404
      0400000000007767607009999900704047700000000000000466000000046400
      0000000000000000007676767676700000000000000000000000000000000000
      00000000FFC007FFFF8003FFFF8003FFFF8003FFFF0001FFFE0000FFF800003F
      F000001FE000000FE000000FFF8003FFFF8003FFFF8003FFFF8003FFFF0001FF
      FE0000FFF800003FF000001FE000000FE000000FFF8003FFFF8003FFFF8003FF
      FF8003FFFF0001FFFE0000FFF800003FF000001FE000000FE000000FFF8003FF
      FFC007FF}
  end
  object BitBtn1: TBitBtn
    Left = 3
    Top = 166
    Width = 91
    Height = 36
    Caption = 'Sì'
    TabOrder = 0
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 261
    Top = 166
    Width = 91
    Height = 36
    Caption = 'Annulla'
    TabOrder = 1
    Kind = bkCancel
  end
  object Panel1: TPanel
    Left = 36
    Top = 3
    Width = 316
    Height = 33
    BevelOuter = bvLowered
    Caption = 'Avviso importante'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
  end
  object Panel2: TPanel
    Left = 3
    Top = 39
    Width = 349
    Height = 93
    BevelOuter = bvLowered
    TabOrder = 3
    object Label1: TLabel
      Left = 6
      Top = 5
      Width = 339
      Height = 85
      Caption = 
        'Le operazioni di salvataggio debbono essere eseguite quotidianam' +
        'ente '#13#10'e si intendono a  completa cura e responsabilità del clie' +
        'nte.'#13#10'EBC Consulting non si assume alcuna responsabilità per la ' +
        'perdita,'#13#10'il parziale o il totale danneggiamento degli archivi d' +
        'i H1Sel, qualsiasi ne'#13#10'sia la causa.  Per ulteriori informazioni' +
        ' vedere la sezione "Condizioni '#13#10'Generali" del contratto di forn' +
        'itura in licenza d'#39'uso del sistema H1Sel.'
      WordWrap = True
    end
  end
  object Panel3: TPanel
    Left = 3
    Top = 136
    Width = 349
    Height = 25
    BevelOuter = bvLowered
    Caption = 'Confermare uscita dal programma ?'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
  end
end
