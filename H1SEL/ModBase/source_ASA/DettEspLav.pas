unit DettEspLav;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, DBCtrls, Mask, ExtCtrls, DtEdit97, DtEdDB97, dxCntner,
  dxEditor, dxExEdtr, dxEdLib, dxDBELib;

type
  TDettEspLavForm = class(TForm)
    Label44: TLabel;
    DBMemo2: TDBMemo;
    Label45: TLabel;
    DBMemo3: TDBMemo;
    BitBtn1: TBitBtn;
    Panel1: TPanel;
    Label1: TLabel;
    DBEdit23: TDBEdit;
    Label36: TLabel;
    DBEdit27: TDBEdit;
    BitBtn2: TBitBtn;
    Label2: TLabel;
    DBMemo1: TDBMemo;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    Label4: TLabel;
    dxDBSpinEdit1: TdxDBSpinEdit;
    dxDBSpinEdit2: TdxDBSpinEdit;
    Label5: TLabel;
    DBEdit1: TDBEdit;
    Label6: TLabel;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DettEspLavForm: TDettEspLavForm;

implementation

uses ModuloDati;

{$R *.DFM}


procedure TDettEspLavForm.BitBtn1Click(Sender: TObject);
begin
     close
end;

procedure TDettEspLavForm.FormShow(Sender: TObject);
begin
     Caption:='[S/20] - '+Caption;
end;

end.
