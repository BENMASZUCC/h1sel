unit StoricoComp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, TeEngine, Series, TeeProcs, Chart, DBChart, Grids, DBGrids, Db,
  DBTables, Buttons, Mask, DBCtrls, ExtCtrls, TB97;

type
  TStoricoCompForm = class(TForm)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    TStorico: TTable;
    TStoricoID: TAutoIncField;
    TStoricoIDCompetenza: TIntegerField;
    TStoricoIDAnagrafica: TIntegerField;
    TStoricoDallaData: TDateTimeField;
    TStoricoMotivoAumento: TStringField;
    TStoricoIDCorso: TIntegerField;
    TStoricoValore: TIntegerField;
    DsStorico: TDataSource;
    DBChart1: TDBChart;
    Series1: TAreaSeries;
    TStoricoVariazPerc: TSmallintField;
    Panel2: TPanel;
    Label1: TLabel;
    DBEdit12: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit1: TDBEdit;
    Panel3: TPanel;
    DBGrid1: TDBGrid;
    BDel: TToolbarButton97;
    BNew: TToolbarButton97;
    BOK: TToolbarButton97;
    BCan: TToolbarButton97;
    TStoricoCollFormaz: TBooleanField;
    procedure TStoricoAfterPost(DataSet: TDataSet);
    procedure BDelClick(Sender: TObject);
    procedure DsStoricoStateChange(Sender: TObject);
    procedure BNewClick(Sender: TObject);
    procedure BOKClick(Sender: TObject);
    procedure BCanClick(Sender: TObject);
    procedure TStoricoCalcFields(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  StoricoCompForm: TStoricoCompForm;

implementation

uses ModuloDati, ModuloDati2;

{$R *.DFM}


procedure TStoricoCompForm.TStoricoAfterPost(DataSet: TDataSet);
begin
     DBChart1.RefreshData;
end;

procedure TStoricoCompForm.BDelClick(Sender: TObject);
begin
     if not TStorico.EOF then
        if MessageDlg('Sei sicuro di voler eliminare questa riga ?',mtWarning,
           [mbNo,mbYes],0)=mrYes then TStorico.Delete;
end;

procedure TStoricoCompForm.DsStoricoStateChange(Sender: TObject);
var b:boolean;
begin
     b:=DsStorico.State in [dsEdit,dsInsert];
     BNew.Enabled:=not b;
     BDel.Enabled:=not b;
     BOK.Enabled:=b;
     BCan.Enabled:=b;
end;

procedure TStoricoCompForm.BNewClick(Sender: TObject);
begin
     TStorico.Insert;
end;

procedure TStoricoCompForm.BOKClick(Sender: TObject);
begin
     TStorico.Post;
end;

procedure TStoricoCompForm.BCanClick(Sender: TObject);
begin
     TStorico.Cancel;
end;

procedure TStoricoCompForm.TStoricoCalcFields(DataSet: TDataSet);
begin
     if TStoricoIDCorso.asString<>'' then TStoricoCollFormaz.Value:=True
     else TStoricoCollFormaz.Value:=False;
end;

procedure TStoricoCompForm.FormShow(Sender: TObject);
begin
     Caption:='[M/023] - '+Caption;
end;

end.
