�
 TELENCOFATTCLIENTEFORM 0]
  TPF0TElencoFattClienteFormElencoFattClienteFormLeft� Top� BorderStylebsDialogCaptionFatture clienteClientHeight�ClientWidthkColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TLabelLabel2LeftTopFWidthfHeightCaptionFatture fatte al cliente  TBitBtnBitBtn1LeftTopWidth[Height"TabOrder KindbkOK  TBitBtnBitBtn2LeftTop%Width[Height"CaptionAnnullaTabOrderKindbkCancel  TPanelPanel7LeftTopWidthHeight1
BevelOuter	bvLoweredEnabledTabOrder TLabelLabel1LeftTopWidth HeightCaptionCliente  TDBEditDBEdit12LeftTopWidth� HeightColorclAqua	DataFieldDescrizione
DataSourceData2.DsEBCclientiFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontReadOnly	TabOrder    TDBGridDBGrid1LeftTopUWidthaHeight4
DataSourceDsQFattClienteTabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameProgressivoTitle.CaptionFatt.n�Width4Visible	 Expanded	FieldNameDataWidthKVisible	 Expanded	FieldNameTotaleFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold Title.AlignmenttaRightJustifyTitle.Font.CharsetDEFAULT_CHARSETTitle.Font.ColorclBlackTitle.Font.Height�Title.Font.NameMS Sans SerifTitle.Font.StylefsBold WidthIVisible	 	AlignmenttaCenterExpanded	FieldNamePagataTitle.AlignmenttaCenterVisible	 Expanded	FieldNamePagataInDataTitle.CaptionPagata in dataWidthMVisible	    TQueryQFattClienteActive	DatabaseNameEBCDB
DataSourceData2.DsEBCclientiSQL.Stringsselect *from Fatture where IDCliente=:ID Left(Top� 	ParamDataDataType	ftAutoIncNameID	ParamType	ptUnknown   TIntegerFieldQFattClienteID	FieldNameIDOrigin"Fatture".ID  TIntegerFieldQFattClienteProgressivo	FieldNameProgressivoOrigin"Fatture".Progressivo  TDateTimeFieldQFattClienteData	FieldNameDataOrigin"Fatture".Data  TFloatFieldQFattClienteTotale	FieldNameTotaleOrigin"Fatture".Totale  TDateTimeFieldQFattClientePagataInData	FieldNamePagataInDataOrigin"Fatture".PagataInData  TBooleanFieldQFattClientePagata	FieldNamePagataOrigin"Fatture".PagataDisplayValuess�;no   TDataSourceDsQFattClienteDataSetQFattClienteLeft(Top�    