unit CliCandidati;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     ComCtrls,ExtCtrls,Db,DBTables,ImgList,Menus,StdCtrls;

type
     TCliCandidatiFrame=class(TFrame)
          Panel1: TPanel;
          Panel104: TPanel;
          Q1: TQuery;
          TV: TTreeView;
          ImageList1: TImageList;
          ImageList3: TImageList;
          PopupMenu3: TPopupMenu;
          Infocandidato1: TMenuItem;
          curriculum1: TMenuItem;
          documenti1: TMenuItem;
          fileWord1: TMenuItem;
          Label1: TLabel;
          procedure Infocandidato1Click(Sender: TObject);
          procedure curriculum1Click(Sender: TObject);
          procedure documenti1Click(Sender: TObject);
          procedure fileWord1Click(Sender: TObject);
     private
     public
          xIDCliente,xIDRicerca: integer;
          procedure CreaAlbero;
     end;

implementation

uses SchedaCand,uUtilsVarie,ModuloDati,Curriculum,Main,View,ElencoDip,
     ModuloDati2,InsInserimento;

{$R *.DFM}

procedure TCliCandidatiFrame.CreaAlbero;
var N,N1: TTreeNode;
     xDicNodo: string;
begin
     TV.Items.BeginUpdate;
     TV.Items.Clear;
     // primo nodo: INSERITI
     N:=TV.Items.AddChildObject(nil,'CANDIDATI INSERITI IN AZIENDA',pointer(0));
     N.ImageIndex:=0;
     N.SelectedIndex:=0;
     Q1.Close;
     Q1.SQL.clear;
     Q1.SQL.Text:='select EBC_Inserimenti.IDAnagrafica,Anagrafica.Cognome,Anagrafica.Nome,Anagrafica.CVNumero, '+
          'EBC_Ricerche.Progressivo RifRicerca, '+
          'EBC_Inserimenti.DallaData,EBC_Inserimenti.AllaData,ProgFattura, '+
          'EBC_Inserimenti.MotivoCessazione '+
          'from EBC_Inserimenti,EBC_Ricerche,Anagrafica '+
          'where EBC_Inserimenti.IDRicerca=EBC_Ricerche.ID '+
          '  and EBC_Inserimenti.IDAnagrafica=Anagrafica.ID '+
          '  and EBC_Inserimenti.IDCliente='+IntToStr(xIDCliente)+' '+
          'order by Cognome,Nome';
     Q1.Open;
     while not Q1.EOF do begin
          xDicNodo:=Q1.FieldByName('Cognome').asString+' '+Q1.FieldByName('Nome').asString+'  dal '+Q1.FieldByName('DallaData').asString+' (Ricerca n� '+Q1.FieldByName('RifRicerca').asString+')';
          N1:=TV.Items.AddChildObject(N,xDicNodo,pointer(Q1.FieldByName('IDAnagrafica').asInteger));
          N1.ImageIndex:=1;
          N1.SelectedIndex:=1;
          Q1.Next;
     end;
     // secondo nodo: PRESENTATI
     if xIDRicerca>0 then
          N:=TV.Items.AddChildObject(nil,'CANDIDATI PRESENTATI (per questa ricerca)',pointer(1))
     else N:=TV.Items.AddChildObject(nil,'CANDIDATI PRESENTATI',pointer(1));
     N.ImageIndex:=0;
     N.SelectedIndex:=0;
     Q1.Close;
     Q1.SQL.clear;
     Q1.SQL.Text:='select Storico.IDAnagrafica,Anagrafica.Cognome,Anagrafica.Nome,Anagrafica.CVNumero, '+
          '       DataEvento,Evento,Nominativo Utente '+
          'from Storico,Anagrafica,EBC_Eventi,Users '+
          'where Storico.IDAnagrafica=Anagrafica.ID '+
          '  and Storico.IDEvento=EBC_Eventi.ID '+
          '  and Storico.IDUtente=Users.ID and IDEvento in (20,76,21) '+
          'and Storico.IDCliente='+IntToStr(xIDCliente);
     if xIDRicerca>0 then
          Q1.SQL.add('and IDRicerca='+IntToStr(xIDRicerca));
     Q1.SQL.add('order by Cognome,Nome');
     Q1.Open;
     while not Q1.EOF do begin
          xDicNodo:=Q1.FieldByName('Cognome').asString+' '+Q1.FieldByName('Nome').asString+' '+Q1.FieldByName('Evento').asString+' '+
               ' il '+Q1.FieldByName('DataEvento').asString+' (utente: '+Q1.FieldByName('utente').asString+')';
          N1:=TV.Items.AddChildObject(N,xDicNodo,pointer(Q1.FieldByName('IDAnagrafica').asInteger));
          N1.ImageIndex:=1;
          N1.SelectedIndex:=1;
          Q1.Next;
     end;
     // terzo nodo: candidati visti a colloquio
     N:=TV.Items.AddChildObject(nil,'CANDIDATI VISTI A COLLOQUIO',pointer(2));
     N.ImageIndex:=0;
     N.SelectedIndex:=0;
     Q1.Close;
     Q1.SQL.clear;
     Q1.SQL.Text:='select Anagrafica.ID IDAnagrafica,Anagrafica.Cognome,Anagrafica.Nome,Anagrafica.CVNumero, '+
          '       Data,ValutazioneGenerale,Nominativo Utente '+
          'from EBC_Colloqui,EBC_Ricerche,Anagrafica,Users '+
          'where EBC_Colloqui.IDAnagrafica=Anagrafica.ID '+
          '  and EBC_Colloqui.IDRicerca=EBC_Ricerche.ID '+
          '  and EBC_Colloqui.IDSelezionatore=Users.ID '+
          '  and EBC_Ricerche.IDCliente='+IntToStr(xIDCliente)+' '+
          'order by Cognome,Nome ';
     Q1.Open;
     while not Q1.EOF do begin
          xDicNodo:=Q1.FieldByName('Cognome').asString+' '+Q1.FieldByName('Nome').asString+'  colloquio del '+
               Q1.FieldByName('Data').asString+' - '+Q1.FieldByName('ValutazioneGenerale').asString+
               ' ('+Q1.FieldByName('utente').asString+')';
          N1:=TV.Items.AddChildObject(N,xDicNodo,pointer(Q1.FieldByName('IDAnagrafica').asInteger));
          N1.ImageIndex:=1;
          N1.SelectedIndex:=1;
          Q1.Next;
     end;
     // quarto nodo: CV PROPRI
     N:=TV.Items.AddChildObject(nil,'CANDIDATI "PROPRI"',pointer(3));
     N.ImageIndex:=0;
     N.SelectedIndex:=0;
     Q1.Close;
     Q1.SQL.clear;
     Q1.SQL.Text:='select Anagrafica.ID IDAnagrafica,Anagrafica.Cognome,Anagrafica.Nome,Anagrafica.CVNumero, '+
          '       Anagrafica.CVInseritoInData '+
          'from Anagrafica '+
          'where IDProprietaCV='+IntToStr(xIDCliente)+' '+
          'order by Cognome,Nome ';
     Q1.Open;
     while not Q1.EOF do begin
          xDicNodo:=Q1.FieldByName('Cognome').asString+' '+Q1.FieldByName('Nome').asString+'  inserito in data '+Q1.FieldByName('CVInseritoInData').asString;
          N1:=TV.Items.AddChildObject(N,xDicNodo,pointer(Q1.FieldByName('IDAnagrafica').asInteger));
          N1.ImageIndex:=1;
          N1.SelectedIndex:=1;
          Q1.Next;
     end;
     // quinto nodo: soggetti incompatibili
     N:=TV.Items.AddChildObject(nil,'CANDIDATI INCOMPATIBILI',pointer(4));
     N.ImageIndex:=0;
     N.SelectedIndex:=0;
     Q1.Close;
     Q1.SQL.clear;
     Q1.SQL.Text:='select Anagrafica.ID IDAnagrafica,Anagrafica.Cognome,Anagrafica.Nome,Anagrafica.CVNumero, '+
          '       AnagIncompClienti.Descrizione '+
          'from AnagIncompClienti,Anagrafica '+
          'where AnagIncompClienti.IDAnagrafica=Anagrafica.ID and IDCliente='+IntToStr(xIDCliente)+' '+
          'order by Cognome,Nome ';
     Q1.Open;
     while not Q1.EOF do begin
          xDicNodo:=Q1.FieldByName('Cognome').asString+' '+Q1.FieldByName('Nome').asString+' - '+Q1.FieldByName('Descrizione').asString;
          N1:=TV.Items.AddChildObject(N,xDicNodo,pointer(Q1.FieldByName('IDAnagrafica').asInteger));
          N1.ImageIndex:=1;
          N1.SelectedIndex:=1;
          Q1.Next;
     end;

     Q1.Close;
     TV.Items.EndUpdate;
     TV.FullExpand;
     TV.Selected:=TV.Items[0];
end;

procedure TCliCandidatiFrame.Infocandidato1Click(Sender: TObject);
begin
     if TV.Selected.level=0 then exit;
     SchedaCandForm:=TSchedaCandForm.create(self);
     if not PosizionaAnag(longint(TV.Selected.Data)) then exit;
     SchedaCandForm.ShowModal;
     SchedaCandForm.Free;
end;

procedure TCliCandidatiFrame.curriculum1Click(Sender: TObject);
begin
     if TV.Selected.level=0 then exit;
     if not PosizionaAnag(longint(TV.Selected.Data)) then exit;
     Data.TTitoliStudio.Open;
     Data.TCorsiExtra.Open;
     Data.TLingueConosc.Open;
     Data.TEspLav.Open;
     CurriculumForm.ShowModal;
     Data.TTitoliStudio.Close;
     Data.TCorsiExtra.Close;
     Data.TLingueConosc.Close;
     Data.TEspLav.Close;
     MainForm.Pagecontrol5.ActivePage:=MainForm.TSStatoTutti;
end;

procedure TCliCandidatiFrame.documenti1Click(Sender: TObject);
var xFile: string;
     xProcedi: boolean;
begin
     if TV.Selected.level=0 then exit;
     ApriCV(longint(TV.Selected.Data));
end;

procedure TCliCandidatiFrame.fileWord1Click(Sender: TObject);
begin
     if TV.Selected.level=0 then exit;
     if not PosizionaAnag(longint(TV.Selected.Data)) then exit;
     ApriFileWord(Data.TAnagraficaCVNumero.AsString);
end;

end.

