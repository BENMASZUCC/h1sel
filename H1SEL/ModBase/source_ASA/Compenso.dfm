�
 TCOMPENSOFORM 0a  TPF0TCompensoFormCompensoFormLeftTop� ActiveControlCBTipoBorderStylebsDialogCaptionCompenso ricercaClientHeight� ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TLabelLabel5LeftTop;WidthIHeightCaptionTipo compenso  TLabelLabel6LeftTopTWidth+HeightCaptionImportoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel7LeftTop� WidthHeightCaptionNote  TLabelLabel8LeftTopgWidth>HeightCaptionData Fattura (prevista)Font.CharsetDEFAULT_CHARSET
Font.ColorclBlueFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontWordWrap	  TPanelPanel1LeftTopWidth_Height-
BevelOuter	bvLoweredEnabledTabOrder TLabelLabel1LeftTopWidth0HeightCaptionRif.ricercaFocusControlDBEdit1  TLabelLabel2Left8TopWidth2HeightCaptionData InizioFocusControlDBEdit2  TLabelLabel3LeftzTopWidth
HeightCaptionn�FocusControlDBEdit3  TLabelLabel4Left� TopWidthHeightCaptionRuolo  TDBEditDBEdit1LeftTopWidth0Height	DataFieldProgressivo
DataSourceData2.DsRicClientiTabOrder   TDBEditDBEdit2Left8TopWidth@Height	DataField
DataInizio
DataSourceData2.DsRicClientiTabOrder  TDBEditDBEdit3LeftzTopWidthHeight	DataFieldNumRicercati
DataSourceData2.DsRicClientiTabOrder  TDBEditDBEdit4Left� TopWidth� Height	DataFieldRuolo
DataSourceData2.DsRicClientiTabOrder   TBitBtnBitBtn1LeftgTopWidthVHeight!TabOrderKindbkOK  TBitBtnBitBtn2LeftgTop%WidthVHeight!CaptionAnnullaTabOrderKindbkCancel  	TComboBoxCBTipoLeftWTop7Width� Height
ItemHeight	MaxLengthTabOrder Items.Stringsanticipo ricercapresentazione candidatoinserimento candidatoaltro   TEditENoteLeftWTop� Width	Height	MaxLengthPTabOrder  TDateEdit97
DEDataFattLeftWToplWidthwHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlueFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderColorCalendar.ColorValidclBlueDayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanClear
doCanPopup
doIsMaskeddoShowCanceldoShowToday   TRxCalcEdit	RxImportoLeftWTopQWidthvHeightAutoSizeDisplayFormat
�'.' ,0.00Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold FormatOnEditing		NumGlyphs
ParentFontTabOrder   