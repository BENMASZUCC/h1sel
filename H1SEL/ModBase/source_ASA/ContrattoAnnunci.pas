unit ContrattoAnnunci;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     StdCtrls,Buttons,ExtCtrls,Mask,ToolEdit,CurrEdit,RXSpin,DtEdit97,
     Db,DBTables,Grids,DBGrids;

type
     TContrattoAnnunciForm=class(TForm)
          BitBtn1: TBitBtn;
          BitBtn2: TBitBtn;
          DBGrid1: TDBGrid;
          QTestEdiz: TQuery;
          QTestEdizIDTestata: TIntegerField;
          QTestEdizIDEdizione: TAutoIncField;
          QTestEdizTestata: TStringField;
          QTestEdizEdizione: TStringField;
          DsQTestEdiz: TDataSource;
          DEDataStipula: TDateEdit97;
          DEDataScadenza: TDateEdit97;
          TotModuli: TRxSpinEdit;
          ModuliUtilizzati: TRxSpinEdit;
          CostoLire: TRxCalcEdit;
          CostoEuro: TRxCalcEdit;
          Label1: TLabel;
          Label2: TLabel;
          Label3: TLabel;
          Label4: TLabel;
          Label5: TLabel;
          Label6: TLabel;
          Bevel1: TBevel;
          Bevel2: TBevel;
          Label7: TLabel;
          procedure CostoLireChange(Sender: TObject);
          procedure FormShow(Sender: TObject);
    procedure CostoEuroChange(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     ContrattoAnnunciForm: TContrattoAnnunciForm;

implementation

{$R *.DFM}

procedure TContrattoAnnunciForm.CostoLireChange(Sender: TObject);
begin
     if CostoLire.Focused then
          CostoEuro.Value:=CostoLire.value/1936.27;
end;

procedure TContrattoAnnunciForm.FormShow(Sender: TObject);
begin
     Caption:='[M/3504] - '+caption;
end;

procedure TContrattoAnnunciForm.CostoEuroChange(Sender: TObject);
begin
     if CostoEuro.Focused then
          CostoLire.Value:=CostoEuro.value*1936.27;
end;

end.

