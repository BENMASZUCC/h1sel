unit AnagFile;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Buttons, Mask, ToolEdit, Grids, DBGrids, Db, DBTables;

type
  TAnagFileForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Panel1: TPanel;
    Label1: TLabel;
    ECognome: TEdit;
    ENome: TEdit;
    QTipiFile: TQuery;
    DsQTipiFile: TDataSource;
    QTipiFileID: TAutoIncField;
    QTipiFileTipo: TStringField;
    DBGrid1: TDBGrid;
    Label2: TLabel;
    EDesc: TEdit;
    Label3: TLabel;
    FEFile: TFilenameEdit;
    SpeedButton1: TSpeedButton;
    procedure FormShow(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  AnagFileForm: TAnagFileForm;

implementation

uses uUtilsVarie;

{$R *.DFM}

procedure TAnagFileForm.FormShow(Sender: TObject);
begin
     Caption:='[M/070] - '+Caption;
end;

procedure TAnagFileForm.SpeedButton1Click(Sender: TObject);
begin
     OpenTab('TipiFileCand',['ID','Tipo'],['Tipo file']);
     QTipiFile.Close;
     QTipiFile.Open;
end;

end.
