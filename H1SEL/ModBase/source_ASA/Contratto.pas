unit Contratto;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, DtEdit97, Buttons;

type
  TContrattoForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    DEData: TDateEdit97;
    EDesc: TEdit;
    EStato: TEdit;
    Label4: TLabel;
    ECodice: TEdit;
    SpeedButton1: TSpeedButton;
    procedure FormShow(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ContrattoForm: TContrattoForm;

implementation

uses uUtilsVarie, Main, ModuloDati;

{$R *.DFM}

procedure TContrattoForm.FormShow(Sender: TObject);
begin
     Caption:='[M/140] - '+caption;
     if Uppercase(copy(Data.GlobalNomeAzienda.Value,1,7))='ERREMME' then begin
        ContrattoForm.caption:='Dati contratto';
     end;
end;

procedure TContrattoForm.SpeedButton1Click(Sender: TObject);
begin
     if OpenSelFromTab('StatiContratti','ID','StatoContratto','Stato','')<>'' then begin
        EStato.text:=MainForm.xgenericString;
     end;
end;

end.
