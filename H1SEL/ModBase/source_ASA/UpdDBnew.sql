/* Script unico per la modifica al Database EBC */
/* Ultima modifica: 13/07/2002 */

/* attenzione a chi ha il db scritto in altro modo (vedi PROPOSTE) */
USE EBC
GO

UPDATE Global set Versione='1.9.3'
GO

/* allargamento campo Rif. annuncio */
if (select length from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('Ann_Annunci') and syscolumns.name = 'Rif')=6
BEGIN
   if exists (select * from dbo.sysindexes where name = 'IdxRif' and id = object_id('dbo.Ann_Annunci'))
   drop index dbo.Ann_Annunci.IdxRif
   Alter table Ann_Annunci
   alter column Rif char(10) NULL
   CREATE  INDEX IdxRif 
   ON Ann_Annunci(Rif)
END
GO

/* allargamento campo cellulare */
if (select length from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('Anagrafica') and syscolumns.name = 'Cellulare')=15
BEGIN
   Alter table Anagrafica
   alter column Cellulare char(30) NULL
END
GO

/* allargamento campo titolo in anagrafica */
if (select length from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('Anagrafica') and syscolumns.name = 'Titolo')=10
BEGIN
   Alter table Anagrafica
   alter column Titolo char(20) NULL
END
GO

/* allargamento campo titolo in rif. cliente */
if (select length from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('EBC_ContattiClienti') and syscolumns.name = 'Titolo')=10
BEGIN
   Alter table EBC_ContattiClienti
   alter column Titolo char(20) NULL
END
GO

/* ricerca per zona di interesse */
if not exists (select * from TabQuery 
               where Tabella='anagaltreinfo' and campo='ProvInteresse')
BEGIN
INSERT INTO TabQuery(Tabella, DescTabella, Campo, DescCampo)
VALUES('anagaltreinfo','Dati personali (anagrafici)','ProvInteresse','Zona/Prov. interesse')
INSERT INTO TabQueryOperatori(IDTabQuery, Operatore, DescOperatore)
SELECT @@IDENTITY,'like','contiene la parola'
END
GO


/* recupero eventuali commesse senza utenti */
insert into EBC_Ricercheutenti (IDRicerca,IDUtente,IDRuoloRic)
select ID,IDUtente,1 from EBC_Ricerche 
where IDUtente not in 
 (select IDUtente from EBC_Ricercheutenti where IDRicerca=EBC_Ricerche.ID)
and IDUtente is not null


/* Area settore "replicato" in esp.lav., trigger e ricerca */
update EsperienzeLavorative set IDAreaSettore =
 (select IDAreaSettore from EBC_Attivita where ID=EsperienzeLavorative.IDSettore)
where EsperienzeLavorative.IDSettore is not null
GO

CREATE TRIGGER trEspLavIDAreaSettore ON EsperienzeLavorative
  FOR INSERT, UPDATE AS
  DECLARE @IdInserito int 
  IF UPDATE(IDSettore)
  BEGIN
    SELECT @IdInserito = [ID] FROM inserted
    UPDATE EsperienzeLavorative SET
      EsperienzeLavorative.IDAreaSettore=
        (SELECT DISTINCT EBC_Attivita.IDAreaSettore FROM EBC_Attivita,inserted 
         WHERE EBC_Attivita.ID=inserted.IDSettore)
        WHERE (EsperienzeLavorative.ID=@IDInserito)
  END
GO

if not exists (select * from TabQuery 
               where Tabella='EsperienzeLavorative' and campo='IDAreaSettore')
BEGIN
INSERT INTO TabQuery(Tabella, DescTabella, Campo, DescCampo, TipoCampo,TabLookup,FieldLookup,FieldLookupSize)
VALUES('EsperienzeLavorative','Esperienze lavorative','IDAreaSettore','Area relativa al settore','integer','AreeSettori','AreaSettore',50)
INSERT INTO TabQueryOperatori(IDTabQuery, Operatore, DescOperatore)
SELECT @@IDENTITY,'=','='
END
GO


if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('Anagrafica') and syscolumns.name = 'NumFigli')
BEGIN
alter TABLE Anagrafica
add NumFigli char(10) NULL 
END
GO

if not exists (select * from sysobjects where id = object_id('AnagRelazioniUtenti'))
BEGIN
CREATE TABLE dbo.AnagRelazioniUtenti (
  ID int IDENTITY (1, 1) NOT NULL,
  IDAnagrafica int NOT NULL,
  IDUtente int NOT NULL,
  IDTipoRelazione int NULL,
  Note text NULL)
ALTER TABLE dbo.AnagRelazioniUtenti
  ADD CONSTRAINT PK_AnagRelazioniUtenti
  PRIMARY KEY (ID)
END
GO 

if not exists (select * from sysobjects where id = object_id('TipiRelazioniUtenti'))
BEGIN
CREATE TABLE dbo.TipiRelazioniUtenti (
  ID int IDENTITY (1, 1) NOT NULL,
  TipoRelazione char(20) NOT NULL)
ALTER TABLE dbo.TipiRelazioniUtenti
  ADD CONSTRAINT PK_TipiRelazioniUtenti
  PRIMARY KEY (ID)
END
GO 

if not exists (select * from EBC_TipiStato where ID=9)
BEGIN
SET IDENTITY_INSERT EBC_TipiStato ON
insert into EBC_TipiStato (ID,TipoStato)
values (9,'inserito da internet')
SET IDENTITY_INSERT EBC_TipiStato OFF
END
GO

if not exists (select * from EBC_Stati where ID=61)
BEGIN
SET IDENTITY_INSERT EBC_Stati ON
insert into EBC_Stati (ID,Stato,IDTipoStato)
values (61,'inserito da internet',9)
SET IDENTITY_INSERT EBC_Stati OFF
insert into EBC_Eventi (IDDaStato,Evento,IDAStato,NonCancellabile)
values (61,'in selezione',27,1)
insert into EBC_Eventi (IDDaStato,Evento,IDAStato,NonCancellabile)
values (61,'inserimento come esterno',31,1)
insert into EBC_Eventi (IDDaStato,Evento,IDAStato,NonCancellabile)
values (61,'eliminazione dall''archivio',30,1)
SET IDENTITY_INSERT EBC_Eventi ON
insert into EBC_Eventi (ID,Evento,NonCancellabile)
values (991,'inserito da internet',1)
SET IDENTITY_INSERT EBC_Eventi OFF
END
GO

if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('Anagrafica') and syscolumns.name = 'CellulareUfficio')
BEGIN
alter TABLE Anagrafica
add CellulareUfficio char(30) NULL, EmailUfficio char(40) NULL
END
GO

if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('EsperienzeLavorative') and syscolumns.name = 'IDMansioneTemp')
BEGIN
alter TABLE EsperienzeLavorative
add IDMansioneTemp int NULL 
END
GO

if not exists (select * from sysobjects where id = object_id('MansioniTemp'))
BEGIN
CREATE TABLE dbo.MansioniTemp (
  ID int IDENTITY (1, 1) NOT NULL,
  Ruolo char(40) NOT NULL,
  IDArea int NULL)
ALTER TABLE dbo.MansioniTemp
  ADD CONSTRAINT PK_MansioniTemp
  PRIMARY KEY (ID)
END
GO

/* BILINGUE e altro (FERRARI) */ 

if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('Diplomi') and syscolumns.name = 'Tipo_ENG')
BEGIN
alter TABLE Diplomi
add Tipo_ENG char(20) NULL, Descrizione_ENG char(60) NULL
END
GO

if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('Lingue') and syscolumns.name = 'Lingua_ENG')
BEGIN
alter TABLE Lingue
add Lingua_ENG char(15) NULL
END
GO

if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('LivelloLingue') and syscolumns.name = 'LivelloConoscenza_ENG')
BEGIN
alter TABLE LivelloLingue
add LivelloConoscenza_ENG char(15) NULL
END
GO

if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('LingueConosciute') and syscolumns.name = 'IDLingua')
BEGIN
alter TABLE LingueConosciute
add IDLingua int NULL
END
GO

update LingueConosciute 
set IDLingua=(select ID from Lingue 
            where Lingua=LingueConosciute.Lingua)
GO
update TabQuery set Campo='IDLingua', TipoCampo='integer'
where campo='Lingua'
GO

/* Corsi Extra: campo "Conseguito nell'anno" */
if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('CorsiExtra') and syscolumns.name = 'ConseguitoAnno')
BEGIN
alter TABLE CorsiExtra
add ConseguitoAnno int NULL
/* ricerca per "Conseguito nell'anno" */
DECLARE @IdTabQuery int 
INSERT INTO TabQuery(Tabella, DescTabella, Campo, DescCampo, TipoCampo)
VALUES('CorsiExtra','Corsi o stage presso aziende','ConseguitoAnno','Conseguito nell''anno','integer')
select @IdTabQuery = @@IDENTITY
INSERT INTO TabQueryOperatori(IDTabQuery, Operatore, DescOperatore)
SELECT @IdTabQuery,'=','='
INSERT INTO TabQueryOperatori(IDTabQuery, Operatore, DescOperatore)
SELECT @IdTabQuery,'>=','maggiore o uguale a'
INSERT INTO TabQueryOperatori(IDTabQuery, Operatore, DescOperatore)
SELECT @IdTabQuery,'<=','minore o uguale a'
END
GO

/* Conoscenze informatiche: campo descrittivo */
if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('AnagConoscInfo') and syscolumns.name = 'Descrizione')
BEGIN
alter TABLE AnagConoscInfo
add Descrizione char(50) NULL
SET IDENTITY_INSERT ConoscenzeInfo ON
insert into ConoscenzeInfo (ID,Procedura)
values (0,'altro')
SET IDENTITY_INSERT ConoscenzeInfo OFF
INSERT INTO TabQuery(Tabella, DescTabella, Campo, DescCampo, TipoCampo, TabLookup, FieldLookup, FieldLookupSize)
VALUES('AnagConoscInfo','Conoscenze Informatiche','IDConoscInfo','Applicativo/procedura','integer','ConoscenzeInfo','Procedura',50)
INSERT INTO TabQueryOperatori(IDTabQuery, Operatore, DescOperatore)
SELECT @@IDENTITY,'=','='
INSERT INTO TabQuery(Tabella, DescTabella, Campo, DescCampo)
VALUES('AnagConoscInfo','Conoscenze Informatiche','Livello','Livello di conoscenza')
INSERT INTO TabQueryOperatori(IDTabQuery, Operatore, DescOperatore)
SELECT @@IDENTITY,'=','='
INSERT INTO TabQuery(Tabella, DescTabella, Campo, DescCampo)
VALUES('AnagConoscInfo','Conoscenze Informatiche','Descrizione','Descrizione/note')
INSERT INTO TabQueryOperatori(IDTabQuery, Operatore, DescOperatore)
SELECT @@IDENTITY,'like','contiene la parola'
END
GO

/* Esperienza professionale maturata */
if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('AnagAltreInfo') and syscolumns.name = 'IDEspProfMaturata')
BEGIN
alter table AnagAltreInfo
add IDEspProfMaturata int NULL
CREATE TABLE dbo.EspProfMaturata (
  ID int IDENTITY (1, 1) NOT NULL,
  EspProfMaturata char(40) NULL,
  EspProfMaturata_ENG char(40) NULL)
ALTER TABLE dbo.EspProfMaturata
  ADD CONSTRAINT PK_EspProfMaturata
  PRIMARY KEY (ID)
INSERT INTO TabQuery(Tabella, DescTabella, Campo, DescCampo, TipoCampo, TabLookup, FieldLookup, FieldLookupSize)
VALUES('AnagAltreInfo','Dati personali (anagrafici)','IDEspProfMaturata','Esp.Profess.Maturata','integer','EspProfMaturata','EspProfMaturata',40)
INSERT INTO TabQueryOperatori(IDTabQuery, Operatore, DescOperatore)
SELECT @@IDENTITY,'=','='
END
GO

/* Settori */
if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('EBC_Attivita') and syscolumns.name = 'Attivita_ENG')
BEGIN
alter TABLE EBC_Attivita
add Attivita_ENG char(50) NULL
END
GO

/* Aree */
if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('Aree') and syscolumns.name = 'Descrizione_ENG')
BEGIN
alter TABLE Aree
add Descrizione_ENG char(30) NULL
END
GO

/* Ruoli (Mansioni) */
if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('Mansioni') and syscolumns.name = 'Descrizione_ENG')
BEGIN
alter TABLE Mansioni
add Descrizione_ENG char(30) NULL
END
GO

/* Esp.lav.: mese dal e al */
if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('EsperienzeLavorative') and syscolumns.name = 'MeseDal')
BEGIN
alter TABLE EsperienzeLavorative
add MeseDal smallint NULL, MeseAl smallint NULL
END
GO

/* Altri campi in AnagAltreInfo */
if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('AnagAltreInfo') and syscolumns.name = 'DomandePrec')
BEGIN
alter TABLE AnagAltreInfo
add DomandePrec bit DEFAULT 0,
    PartecSelezPrec bit DEFAULT 0,
    PartecSelezQuando char(40) NULL,
    TipoSelezione char(30) NULL,
    ParentiInAzienda char(50) NULL
INSERT INTO TabQuery(Tabella, DescTabella, Campo, DescCampo, ValoriPossibili, TipoCampo)
VALUES('AnagAltreInfo','Dati personali (anagrafici)','DomandePrec','inviato precedenti domande ?','s�,no','Boolean')
INSERT INTO TabQueryOperatori(IDTabQuery, Operatore, DescOperatore)
SELECT @@IDENTITY,'=','='
INSERT INTO TabQuery(Tabella, DescTabella, Campo, DescCampo, ValoriPossibili, TipoCampo)
VALUES('AnagAltreInfo','Dati personali (anagrafici)','PartecSelezPrec','Altre selezioni ?','s�,no','Boolean')
INSERT INTO TabQueryOperatori(IDTabQuery, Operatore, DescOperatore)
SELECT @@IDENTITY,'=','='
INSERT INTO TabQuery(Tabella, DescTabella, Campo, DescCampo)
VALUES('AnagAltreInfo','Dati personali (anagrafici)','PartecSelezQuando','Altre selezioni quando')
INSERT INTO TabQueryOperatori(IDTabQuery, Operatore, DescOperatore)
SELECT @@IDENTITY,'like','contiene la parola'
INSERT INTO TabQuery(Tabella, DescTabella, Campo, DescCampo)
VALUES('AnagAltreInfo','Dati personali (anagrafici)','TipoSelezione','Tipo di selezione')
INSERT INTO TabQueryOperatori(IDTabQuery, Operatore, DescOperatore)
SELECT @@IDENTITY,'like','contiene la parola'
INSERT INTO TabQuery(Tabella, DescTabella, Campo, DescCampo)
VALUES('AnagAltreInfo','Dati personali (anagrafici)','ParentiInAzienda','Parenti in azienda')
INSERT INTO TabQueryOperatori(IDTabQuery, Operatore, DescOperatore)
SELECT @@IDENTITY,'like','contiene la parola'
END
GO

/* campo CAUSALE e tabella di base */
if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('EBC_Ricerche') and syscolumns.name = 'IDCausale')
BEGIN
   Alter table EBC_Ricerche
   add IDCausale int NULL
END
GO

if not exists (select * from sysobjects where id = object_id('EBC_RicercheCausali'))
BEGIN
CREATE TABLE dbo.EBC_RicercheCausali (
  ID int IDENTITY (1, 1) NOT NULL,
  Causale char (50) NULL)
ALTER TABLE dbo.EBC_RicercheCausali
  ADD CONSTRAINT PK_EBC_RicercheCausali
  PRIMARY KEY (ID)
END
GO


/* modifiche controllo di gestione */
if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('CG_ContrattiCosti') and syscolumns.name = 'IDUtente')
BEGIN
   Alter table CG_ContrattiCosti
   add IDUtente int NULL
END
GO

if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('CG_ContrattiCosti') and syscolumns.name = 'Pagato')
BEGIN
   Alter table CG_ContrattiCosti
   add Pagato bit Default 0
END
GO

if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('CG_ContrattiRicavi') and syscolumns.name = 'Incassato')
BEGIN
   Alter table CG_ContrattiRicavi
   add Incassato bit Default 0
END
GO



/* ##################################################################################### */

/* PRMAN/H1hrms */

if (select syscolumns.xtype from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('Form_PartecPres') and syscolumns.name = 'OrePres')=56
BEGIN
   Alter table Form_PartecPres
   alter column OrePres float NULL
END
GO

if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('AnagStatoFamiglia') and syscolumns.name = 'Acarico')
BEGIN
   Alter table AnagStatoFamiglia
   add Acarico bit default 0
END
GO

if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('AnagStatoFamiglia') and syscolumns.name = 'Percentuale')
BEGIN
   Alter table AnagStatoFamiglia
   add Percentuale int NULL
END
GO

if not exists (select * from sysobjects where id = object_id('Form_AttivitaFile'))
BEGIN
CREATE TABLE dbo.Form_AttivitaFile (
  ID int IDENTITY (1, 1) NOT NULL,
  IDCorso int NOT NULL,
  IDTipo int NULL ,
  Descrizione char (100) NULL ,
  NomeFile char (200) NULL ,
  DataCreazione smalldatetime NULL)
ALTER TABLE dbo.Form_AttivitaFile
  ADD CONSTRAINT PK_Form_AttivitaFile
  PRIMARY KEY (ID)
END
GO

if (select length from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('TabQuery') and syscolumns.name = 'Tablookup')=20
BEGIN
   Alter table TabQuery
   alter column Tablookup char(30) NULL
END
GO

if not exists (select * from sysobjects where id = object_id('AnagPianoCarriera'))
BEGIN
CREATE TABLE dbo.AnagPianoCarriera (
  ID int IDENTITY (1, 1) NOT NULL,
  IDAnagrafica int NOT NULL,
  IDOrganigramma int NOT NULL,
  [PARENT] [int] NULL ,
  [WIDTH] [int] NULL ,
  [HEIGHT] [int] NULL ,
  [TYPE] [char] (20) NULL ,
  [COLOR] [int] NULL ,
  [IMAGE] [int] NULL ,
  [IMAGEALIGN] [char] (20) NULL ,
  [ORDINE] [int] NULL ,
  [ALIGN] [char] (20) NULL,
  Note char(80) NULL)
ALTER TABLE dbo.AnagPianoCarriera
  ADD CONSTRAINT PK_AnagPianoCarriera
  PRIMARY KEY (ID)
END
GO

if not exists (select * from sysobjects where id = object_id('AnagAreeInteresse'))
BEGIN
CREATE TABLE dbo.AnagAreeInteresse (
  ID int IDENTITY (1, 1) NOT NULL,
  IDAnagrafica int NOT NULL,
  IDArea int NOT NULL,
  Ordine smallint NULL,
  Note text NULL)
ALTER TABLE dbo.AnagAreeInteresse
  ADD CONSTRAINT PK_AnagAreeInteresse
  PRIMARY KEY (ID)
END
GO

if not exists (select * from sysobjects where id = object_id('AnagLineInteresse'))
BEGIN
CREATE TABLE dbo.AnagLineInteresse (
  ID int IDENTITY (1, 1) NOT NULL,
  IDAnagrafica int NOT NULL,
  IDReparto int NOT NULL,
  Ordine smallint NULL,
  Note text NULL)
ALTER TABLE dbo.AnagLineInteresse
  ADD CONSTRAINT PK_AnagLineInteresse
  PRIMARY KEY (ID)
END
GO

if (select syscolumns.xtype from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('Form_AttivitaDate') and syscolumns.name = 'OreLav')=56
BEGIN
   Alter table Form_AttivitaDate
   alter column OreLav float NULL
END
GO

if (select syscolumns.xtype from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('Form_AttivitaDate') and syscolumns.name = 'OreExtra')=56
BEGIN
   Alter table Form_AttivitaDate
   alter column OreExtra float NULL
END
GO

if (select syscolumns.xtype from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('Form_AttivitaDate') and syscolumns.name = 'OreStraord')=56
BEGIN
   Alter table Form_AttivitaDate
   alter column OreStraord float NULL
END
GO

if (select syscolumns.xtype from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('Form_AnagAttivita') and syscolumns.name = 'TotOre')=56
BEGIN
   Alter table Form_AnagAttivita
   alter column TotOre float NULL
END
GO

if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('Form_Prerequisiti') and syscolumns.name = 'IDCompetenza')
BEGIN
   Alter table Form_Prerequisiti
   add IDCompetenza int NULL
END
GO

if not exists (select * from sysobjects where id = object_id('ContrattiNaz'))
BEGIN
CREATE TABLE dbo.ContrattiNaz (
	[ID] [int] IDENTITY (1, 1) NOT NULL ,
	[Descrizione] [char] (40) NULL)
END
GO

if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('AnagAltreInfo') and syscolumns.name = 'IDContrattoNaz')
BEGIN
   Alter table AnagAltreInfo
   add IDContrattoNaz int NULL
END
GO

if not exists (select * from sysobjects where id = object_id('CatProfess'))
BEGIN
CREATE TABLE dbo.CatProfess (
	[ID] [int] IDENTITY (1, 1) NOT NULL ,
	[Descrizione] [char] (40) NULL ,
	[idcontrattonaz] [int] NULL )
END
GO

if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('AnagAltreInfo') and syscolumns.name = 'IDCatProfess')
BEGIN
   Alter table AnagAltreInfo
   add IDCatProfess int NULL
END
GO

if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('AnagAltreInfo') and syscolumns.name = 'DataLicenziamento')
BEGIN
   Alter table AnagAltreInfo
   add DataLicenziamento smalldatetime NULL
END
GO

if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('AnagAltreInfo') and syscolumns.name = 'DataProroga')
BEGIN
   Alter table AnagAltreInfo
   add DataProroga smalldatetime NULL
END
GO

if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('AnagAltreInfo') and syscolumns.name = 'DataUltimaProm')
BEGIN
   Alter table AnagAltreInfo
   add DataUltimaProm smalldatetime NULL
END
GO

if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('QualificheContrattuali') and syscolumns.name = 'IDContrattoNaz')
BEGIN
   Alter table QualificheContrattuali
   add IDContrattoNaz int NULL
END
GO

if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('StoricoContratto') and syscolumns.name = 'IDContrattoNaz')
BEGIN
   Alter table StoricoContratto
   add IDContrattoNaz int NULL
END
GO

if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('StoricoContratto') and syscolumns.name = 'IDCatProfess')
BEGIN
   Alter table StoricoContratto
   add IDCatProfess int NULL
END
GO

if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('RetribColonne') and syscolumns.name = 'IDContrattoNaz')
BEGIN
   Alter table RetribColonne
   add IDContrattoNaz int NULL
END
GO

if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('PRMAN_Pos1') and syscolumns.name = 'IDAzienda')
BEGIN
alter TABLE PRMAN_Pos1 
add IDAzienda int NULL 
END
GO

if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('PRMAN_Pos2') and syscolumns.name = 'IDAzienda')
BEGIN
alter TABLE PRMAN_Pos2 
add IDAzienda int NULL 
END
GO

if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('PRMAN_Pos3') and syscolumns.name = 'IDAzienda')
BEGIN
alter TABLE PRMAN_Pos3 
add IDAzienda int NULL 
END
GO

if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('PRMAN_Pos4') and syscolumns.name = 'IDAzienda')
BEGIN
alter TABLE PRMAN_Pos4 
add IDAzienda int NULL 
END
GO

if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('PRMAN_Pos5') and syscolumns.name = 'IDAzienda')
BEGIN
alter TABLE PRMAN_Pos5 
add IDAzienda int NULL 
END
GO

if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('PRMAN_Pos6') and syscolumns.name = 'IDAzienda')
BEGIN
alter TABLE PRMAN_Pos6 
add IDAzienda int NULL 
END
GO

if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('PRMAN_Pos7') and syscolumns.name = 'IDAzienda')
BEGIN
alter TABLE PRMAN_Pos7
add IDAzienda int NULL 
END
GO

if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('PRMAN_Pos8') and syscolumns.name = 'IDAzienda')
BEGIN
alter TABLE PRMAN_Pos8
add IDAzienda int NULL 
END
GO

if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('PRMAN_Pos9') and syscolumns.name = 'IDAzienda')
BEGIN
alter TABLE PRMAN_Pos9
add IDAzienda int NULL 
END
GO

if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('PRMAN_Pos10') and syscolumns.name = 'IDAzienda')
BEGIN
alter TABLE PRMAN_Pos10 
add IDAzienda int NULL 
END
GO

if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('CentriCosto') and syscolumns.name = 'IDAzienda')
BEGIN
alter TABLE CentriCosto
add IDAzienda int NULL 
END
GO

if not exists (select * from sysobjects where id = object_id('Giove'))
BEGIN
CREATE TABLE dbo.Giove (
  ID int IDENTITY (1, 1) NOT NULL,
  IDAnagrafica int NOT NULL,
  AnzCon2000 float NULL,
  AnzContrat float NULL,
  AnzConvenz float NULL,
  KCin int NULL,
  KPosInail int NULL,
  KPosINPS int NULL,
  Status int NULL,
  KTipoPagam int NULL,
  NumeroScat float NULL,
  DataRatFer smalldatetime NULL, 
  DataRatMen smalldatetime NULL,
  DataAnzLiv smalldatetime NULL)
ALTER TABLE dbo.Giove
  ADD CONSTRAINT PK_Giove
  PRIMARY KEY (ID)
END
GO

if not exists (select * from sysobjects where id = object_id('TabBanche'))
BEGIN
CREATE TABLE dbo.TabBanche (
	[id] [int] IDENTITY (1, 1) NOT NULL ,
	[descbanca] [char] (40) NULL ,
	[descagenzia] [char] (40) NULL ,
	[cab] [int] NULL ,
	[abi] [int] NULL )
ALTER TABLE dbo.TabBanche
  ADD CONSTRAINT PK_TabBanche
  PRIMARY KEY (ID)
END
GO

if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('AnagBanche') and syscolumns.name = 'IDBanca')
BEGIN
alter TABLE AnagBanche 
add IDBanca int NULL 
END
GO

if not exists (select * from sysobjects where id = object_id('GIOVEConfig'))
BEGIN
CREATE TABLE dbo.GIOVEConfig (
	[k1] [bit] NULL ,
	[k2] [bit] NULL ,
	[k3] [bit] NULL ,
	[k4] [bit] NULL ,
	[k5] [bit] NULL ,
	[k6] [bit] NULL ,
	[k7] [bit] NULL ,
	[k8] [bit] NULL ,
	[k9] [bit] NULL ,
	[k10] [bit] NULL ,
	[k11] [bit] NULL ,
	[k12] [bit] NULL ,
	[k13] [bit] NULL ,
	[k14] [bit] NULL ,
	[k15] [bit] NULL ,
	[k16] [bit] NULL ,
	[k17] [bit] NULL ,
	[k18] [bit] NULL ,
	[k19] [bit] NULL ,
	[k20] [bit] NULL ,
	[PercorsoFile] [char] (40) NULL )
END
GO

if not exists (select * from sysobjects where id = object_id('PianoCarrieraTemp'))
BEGIN
CREATE TABLE dbo.PianoCarrieraTemp (
     Nodo char(30) NULL,
     DataPrev smalldatetime NULL,
     TPMP int NULL,
     TPS int NULL)
END
GO

if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('AnagPianoCarriera') and syscolumns.name = 'DataPrev')
BEGIN
alter TABLE AnagPianoCarriera
add DataPrev smalldatetime NULL 
END
GO

if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('AnagPianoCarriera') and syscolumns.name = 'TPMP')
BEGIN
alter TABLE AnagPianoCarriera
add TPMP int NULL 
END
GO

if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('AnagPianoCarriera') and syscolumns.name = 'TPS')
BEGIN
alter TABLE AnagPianoCarriera
add TPS int NULL 
END
GO

if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('QualificheContrattuali') and syscolumns.name = 'PrassiAziendale')
BEGIN
   Alter table QualificheContrattuali
   add PrassiAziendale float NULL,
       PrimoDecile float NULL,
       PrimoQuartile float NULL,
       Media float NULL,
       Mediana float NULL,
       TerzoQuartile float NULL,
       NonoDecile float NULL
END
GO

if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('TipiContratti') and syscolumns.name = 'IDAzienda')
BEGIN
alter TABLE TipiContratti
add IDAzienda int NULL 
END
GO


if not exists (select * from sysobjects where id = object_id('Giove_Cin'))
BEGIN
CREATE TABLE dbo.Giove_Cin (
  ID int IDENTITY (1, 1) NOT NULL,
  Descrizione char(30) NOT NULL)
ALTER TABLE dbo.Giove_Cin
  ADD CONSTRAINT PK_Giove_Cin
  PRIMARY KEY (ID)
END
GO 

if not exists (select * from sysobjects where id = object_id('Giove_PosInail'))
BEGIN
CREATE TABLE dbo.Giove_PosInail (
  ID int IDENTITY (1, 1) NOT NULL,
  Descrizione char(30) NOT NULL)
ALTER TABLE dbo.Giove_PosInail
  ADD CONSTRAINT PK_Giove_PosInail
  PRIMARY KEY (ID)
END
GO 

if not exists (select * from sysobjects where id = object_id('Giove_PosInps'))
BEGIN
CREATE TABLE dbo.Giove_PosInps (
  ID int IDENTITY (1, 1) NOT NULL,
  Descrizione char(30) NOT NULL)
ALTER TABLE dbo.Giove_PosInps
  ADD CONSTRAINT PK_Giove_PosInps
  PRIMARY KEY (ID)
END
GO 

if not exists (select * from sysobjects where id = object_id('Giove_Status'))
BEGIN
CREATE TABLE dbo.Giove_Status (
  ID int IDENTITY (1, 1) NOT NULL,
  Descrizione char(30) NOT NULL)
ALTER TABLE dbo.Giove_Status
  ADD CONSTRAINT PK_Giove_Status
  PRIMARY KEY (ID)
END
GO 

if not exists (select * from sysobjects where id = object_id('Giove_TipoPagam'))
BEGIN
CREATE TABLE dbo.Giove_TipoPagam (
  ID int IDENTITY (1, 1) NOT NULL,
  Descrizione char(30) NOT NULL)
ALTER TABLE dbo.Giove_TipoPagam
  ADD CONSTRAINT PK_Giove_TipoPagam
  PRIMARY KEY (ID)
END
GO 


/* IDENTIY su CentroCosto */
/* ATTENZIONE AI DATI PREGRESSI !!! */
alter table CentriCosto
drop constraint PK_CentriCosto
GO
alter table CentriCosto
drop column CentroCosto
GO
alter table CentriCosto
Add CentroCosto int IDENTITY (1, 1) NOT NULL
GO
ALTER TABLE CentriCosto
  ADD CONSTRAINT PK_CentriCosto
  PRIMARY KEY (CentroCosto)
GO

if not exists (select * from sysobjects,syscolumns where sysobjects.id=syscolumns.id
    and sysobjects.id = object_id('Form_EntiFormaz') and syscolumns.name = 'IDAzienda')
BEGIN
alter TABLE Form_EntiFormaz
add IDAzienda int NULL,
    IDTipoEnteFormaz char(10) NULL,
    PuntoRosso bit default 0,
    AziendaAppart int NULL,
    ProvinciaComp char(2) NULL,
    TipoResp1 char(30) NULL,
    TipoResp2 char(30) NULL,
    Responsabile2 char(40) NULL, 
    TipoResp3 char(30) NULL,
    Responsabile3 char(40) NULL,
    Titolare1 char(40) NULL,
    Titolare2 char(40) NULL,
    Titolare3 char(40) NULL
END
GO

if not exists (select * from sysobjects where id = object_id('Form_TipiEntiFormaz'))
BEGIN
CREATE TABLE dbo.Form_TipiEntiFormaz (
  ID int IDENTITY (1, 1) NOT NULL,
  TipoEnteFormaz char(30) NOT NULL)
ALTER TABLE dbo.Form_TipiEntiFormaz
  ADD CONSTRAINT PK_Form_TipiEntiFormaz
  PRIMARY KEY (ID)
END
GO 

if not exists (select * from sysobjects where id = object_id('Form_Manualistica'))
BEGIN
CREATE TABLE dbo.Form_Manualistica (
  ID int IDENTITY (1, 1) NOT NULL,
  Descrizione char(30) NOT NULL)
ALTER TABLE dbo.Form_Manualistica
  ADD CONSTRAINT PK_Form_Manualistica
  PRIMARY KEY (ID)
END
GO 

if not exists (select * from sysobjects where id = object_id('Form_AttivitaMan'))
BEGIN
CREATE TABLE dbo.Form_AttivitaMan (
  ID int IDENTITY (1, 1) NOT NULL,
  IDCorso int NOT NULL,
  IDManuale int NOT NULL,
  Note char(80) NULL)
ALTER TABLE dbo.Form_AttivitaMan
  ADD CONSTRAINT PK_Form_AttivitaMan
  PRIMARY KEY (ID)
END
GO 

if not exists (select * from sysobjects where id = object_id('Form_MezziPedagogici'))
BEGIN
CREATE TABLE dbo.Form_MezziPedagogici (
  ID int IDENTITY (1, 1) NOT NULL,
  MezzoPedagogico char(30) NOT NULL)
ALTER TABLE dbo.Form_MezziPedagogici
  ADD CONSTRAINT PK_Form_MezziPedagogici
  PRIMARY KEY (ID)
END
GO 

if not exists (select * from sysobjects where id = object_id('Form_AttivitaMezzi'))
BEGIN
CREATE TABLE dbo.Form_AttivitaMezzi (
  ID int IDENTITY (1, 1) NOT NULL,
  IDCorso int NOT NULL,
  IDMezzo int NOT NULL,
  Note char(80) NULL)
ALTER TABLE dbo.Form_AttivitaMezzi
  ADD CONSTRAINT PK_Form_AttivitaMezzi
  PRIMARY KEY (ID)
END
GO 

if not exists (select * from sysobjects where id = object_id('EventiModelliWord'))
BEGIN
CREATE TABLE dbo.EventiModelliWord (
  ID int IDENTITY (1, 1) NOT NULL,
  IDEvento int NOT NULL,
  IDModello int NOT NULL,
  Note char(80) NULL)
ALTER TABLE dbo.EventiModelliWord
  ADD CONSTRAINT PK_EventiModelliWord
  PRIMARY KEY (ID)
END
GO 

/* valori crittati nelle retribuzioni */
alter table AnagRetribuzioni
add ValoreCrypt char(20) NULL
GO

alter table StoricoAnagRetrib
add ValoreCrypt char(20) NULL
GO

