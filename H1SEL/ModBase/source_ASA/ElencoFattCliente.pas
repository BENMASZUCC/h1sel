unit ElencoFattCliente;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, Grids, DBGrids, DBTables, StdCtrls, Mask, DBCtrls, ExtCtrls, Buttons;

type
  TElencoFattClienteForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Panel7: TPanel;
    DBEdit12: TDBEdit;
    Label1: TLabel;
    QFattCliente: TQuery;
    DsQFattCliente: TDataSource;
    DBGrid1: TDBGrid;
    Label2: TLabel;
    QFattClienteID: TIntegerField;
    QFattClienteProgressivo: TIntegerField;
    QFattClienteData: TDateTimeField;
    QFattClienteTotale: TFloatField;
    QFattClientePagataInData: TDateTimeField;
    QFattClientePagata: TBooleanField;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ElencoFattClienteForm: TElencoFattClienteForm;

implementation

uses ModuloDati2;

{$R *.DFM}

procedure TElencoFattClienteForm.FormShow(Sender: TObject);
begin
     Caption:='[S/23] - '+Caption;
end;

end.
