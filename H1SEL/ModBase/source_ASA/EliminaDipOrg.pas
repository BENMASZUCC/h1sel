unit EliminaDipOrg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, DtEdit97;

type
  TEliminaDipOrgForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Label2: TLabel;
    Label3: TLabel;
    ENote: TEdit;
    DEData: TDateEdit97;
    GroupBox1: TGroupBox;
    ECogn: TEdit;
    ENome: TEdit;
    SpeedButton1: TSpeedButton;
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    xIDAutorizzatoDa:integer;
  end;

var
  EliminaDipOrgForm: TEliminaDipOrgForm;

implementation

uses ElencoDip;

{$R *.DFM}

procedure TEliminaDipOrgForm.SpeedButton1Click(Sender: TObject);
begin
     ElencoDipForm:=TElencoDipForm.create(self);
     ElencoDipForm.ShowModal;
     if ElencoDipForm.ModalResult=mrOK then begin
        xIDAutorizzatoDa:=ElencoDipForm.TAnagDipID.Value;
        ECogn.Text:=ElencoDipForm.TAnagDipCognome.Value;
        ENome.Text:=ElencoDipForm.TAnagDipNome.Value;
     end;
     ElencoDipForm.Free;
end;

procedure TEliminaDipOrgForm.FormCreate(Sender: TObject);
begin
     xIDAutorizzatoDa:=0;
     DEData.date:=date;
end;

procedure TEliminaDipOrgForm.FormShow(Sender: TObject);
begin
     Caption:='[M/640] - '+Caption;
end;

end.
