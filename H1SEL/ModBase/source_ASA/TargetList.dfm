object TargetListForm: TTargetListForm
  Left = 257
  Top = 129
  Width = 734
  Height = 495
  Caption = 'Target List'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 726
    Height = 49
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object BStampaElenco: TToolbarButton97
      Left = 204
      Top = 4
      Width = 96
      Height = 40
      DropdownMenu = PMStampa
      Caption = 'stampa/esporta'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00300000000000
        00033FFFFFFFFFFFFFFF0888888888888880777777777777777F088888888888
        8880777777777777777F0000000000000000FFFFFFFFFFFFFFFF0F8F8F8F8F8F
        8F80777777777777777F08F8F8F8F8F8F9F0777777777777777F0F8F8F8F8F8F
        8F807777777777777F7F0000000000000000777777777777777F3330FFFFFFFF
        03333337F3FFFF3F7F333330F0000F0F03333337F77773737F333330FFFFFFFF
        03333337F3FF3FFF7F333330F00F000003333337F773777773333330FFFF0FF0
        33333337F3FF7F3733333330F08F0F0333333337F7737F7333333330FFFF0033
        33333337FFFF7733333333300000033333333337777773333333}
      Layout = blGlyphTop
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      Spacing = 0
      WordWrap = True
    end
    object ToolbarButton971: TToolbarButton97
      Left = 5
      Top = 4
      Width = 96
      Height = 40
      Caption = 'Ricerca clienti'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
        300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
        330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
        333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
        339977FF777777773377000BFB03333333337773FF733333333F333000333333
        3300333777333333337733333333333333003333333333333377333333333333
        333333333333333333FF33333333333330003333333333333777333333333333
        3000333333333333377733333333333333333333333333333333}
      NumGlyphs = 2
      WordWrap = True
      OnClick = ToolbarButton971Click
    end
    object ToolbarButton972: TToolbarButton97
      Left = 103
      Top = 4
      Width = 99
      Height = 40
      Hint = 'si posiziona sull'#39'organigramma'
      Caption = 'Organigramma'
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        8888800000888800000808777708808777700AAAA708809999700AAAA8088099
        9980800000888800000888808888888808888880000000000888888888880888
        888888888800000888888888808777708888888880BBBB708888888880BBBB80
        8888888888000008888888888888888888888888888888888888}
      ParentShowHint = False
      ShowHint = True
      WordWrap = True
      OnClick = ToolbarButton972Click
    end
    object BitBtn1: TBitBtn
      Left = 618
      Top = 5
      Width = 104
      Height = 39
      Anchors = [akTop, akRight]
      Caption = 'Esci'
      TabOrder = 0
      OnClick = BitBtn1Click
      Kind = bkOK
    end
  end
  object dxDBGrid1: TdxDBGrid
    Left = 0
    Top = 49
    Width = 726
    Height = 419
    Bands = <
      item
      end>
    DefaultLayout = True
    HeaderPanelRowCount = 1
    KeyField = 'ID'
    ShowGroupPanel = True
    SummaryGroups = <>
    SummarySeparator = ', '
    Align = alClient
    TabOrder = 1
    OnMouseUp = dxDBGrid1MouseUp
    DataSource = DsQTargetList
    Filter.Active = True
    IniFileName = 'c:\DbgridTargetList.ini'
    OptionsBehavior = [edgoAutoSort, edgoDragScroll, edgoEditing, edgoEnterShowEditor, edgoImmediateEditor, edgoStoreToIniFile, edgoTabThrough, edgoVertThrough]
    OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
    OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoIndicator, edgoRowSelect, edgoUseBitmap]
    object dxDBGrid1ID: TdxDBGridMaskColumn
      Visible = False
      Width = 78
      BandIndex = 0
      RowIndex = 0
      FieldName = 'ID'
    end
    object dxDBGrid1Azienda: TdxDBGridMaskColumn
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      Width = 173
      BandIndex = 0
      RowIndex = 0
      DisableGrouping = True
      FieldName = 'Azienda'
    end
    object dxDBGrid1Attivita: TdxDBGridMaskColumn
      Width = 197
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Attivita'
    end
    object dxDBGrid1telefono: TdxDBGridMaskColumn
      Width = 109
      BandIndex = 0
      RowIndex = 0
      DisableGrouping = True
      FieldName = 'telefono'
      DisableFilter = True
    end
    object dxDBGrid1Comune: TdxDBGridMaskColumn
      Width = 121
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Comune'
    end
    object dxDBGrid1DataUltimaEsploraz: TdxDBGridDateColumn
      Caption = 'Data ultima esploraz.'
      Width = 110
      BandIndex = 0
      RowIndex = 0
      FieldName = 'DataUltimaEsploraz'
    end
  end
  object QTargetList: TQuery
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select EBC_Clienti.ID, Descrizione Azienda, Attivita, '
      '          telefono, Comune, DataUltimaEsploraz'
      'from EBC_Clienti, EBC_Attivita'
      'where EBC_Clienti.IDAttivita *= EBC_Attivita.ID'
      'and TargetList=1'
      'order by Descrizione')
    Left = 112
    Top = 160
    object QTargetListID: TAutoIncField
      FieldName = 'ID'
    end
    object QTargetListAzienda: TStringField
      FieldName = 'Azienda'
      FixedChar = True
      Size = 30
    end
    object QTargetListAttivita: TStringField
      FieldName = 'Attivita'
      FixedChar = True
      Size = 50
    end
    object QTargetListtelefono: TStringField
      FieldName = 'telefono'
      FixedChar = True
      Size = 30
    end
    object QTargetListComune: TStringField
      FieldName = 'Comune'
      FixedChar = True
    end
    object QTargetListDataUltimaEsploraz: TDateTimeField
      FieldName = 'DataUltimaEsploraz'
    end
  end
  object DsQTargetList: TDataSource
    DataSet = QTargetList
    Left = 112
    Top = 192
  end
  object dxPrinter1: TdxComponentPrinter
    Left = 480
    Top = 280
    object dxPrinter1Link1: TdxDBGridReportLink
      Component = dxDBGrid1
      PrinterPage.Background.Brush.Style = bsClear
      PrinterPage.Margins.Bottom = 25400
      PrinterPage.Margins.Left = 25400
      PrinterPage.Margins.Right = 25400
      PrinterPage.Margins.Top = 10000
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxDMPaper_ = 9
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportTitle.Font.Charset = DEFAULT_CHARSET
      ReportTitle.Font.Color = clWindowText
      ReportTitle.Font.Height = -19
      ReportTitle.Font.Name = 'Times New Roman'
      ReportTitle.Font.Style = [fsBold]
      BandColor = clBtnFace
      BandFont.Charset = DEFAULT_CHARSET
      BandFont.Color = clWindowText
      BandFont.Height = -11
      BandFont.Name = 'MS Sans Serif'
      BandFont.Style = []
      Color = clWindow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      FooterFont.Charset = DEFAULT_CHARSET
      FooterFont.Color = clWindowText
      FooterFont.Height = -11
      FooterFont.Name = 'MS Sans Serif'
      FooterFont.Style = []
      GroupNodeColor = clBtnFace
      HeaderColor = clBtnFace
      HeaderFont.Charset = DEFAULT_CHARSET
      HeaderFont.Color = clWindowText
      HeaderFont.Height = -11
      HeaderFont.Name = 'MS Sans Serif'
      HeaderFont.Style = []
      GridLineColor = clBtnFace
      Options = [tlpoBands, tlpoHeaders, tlpoFooters, tlpoRowFooters, tlpoPreview, tlpoPreviewGrid, tlpoGrid, tlpoFlatCheckmarks, tlpoImages, tlpoStateImages]
      PreviewFont.Charset = DEFAULT_CHARSET
      PreviewFont.Color = clBlue
      PreviewFont.Height = -11
      PreviewFont.Name = 'MS Sans Serif'
      PreviewFont.Style = []
      RowFooterColor = cl3DLight
      RowFooterFont.Charset = DEFAULT_CHARSET
      RowFooterFont.Color = clWindowText
      RowFooterFont.Height = -11
      RowFooterFont.Name = 'MS Sans Serif'
      RowFooterFont.Style = []
    end
  end
  object PMStampa: TPopupMenu
    Left = 480
    Top = 248
    object Stampaelenco1: TMenuItem
      Caption = 'Stampa elenco'
      ImageIndex = 18
      OnClick = Stampaelenco1Click
    end
    object EsportainExcel1: TMenuItem
      Caption = 'Esporta in Excel'
      ImageIndex = 19
      OnClick = EsportainExcel1Click
    end
    object esportainHTML1: TMenuItem
      Caption = 'Esporta in HTML'
      ImageIndex = 20
      OnClick = esportainHTML1Click
    end
  end
end
