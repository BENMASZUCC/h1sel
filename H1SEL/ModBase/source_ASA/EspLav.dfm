�
 TESPLAVFORM 0,  TPF0TEspLavForm
EspLavFormLeft� Top� BorderStylebsDialogCaptionEsperienza lavorativaClientHeight�ClientWidth9Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TSpeedButtonBAreeRuoliITALeftTop� Width%Height
GroupIndexDown	CaptionITAFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontVisibleOnClickBAreeRuoliITAClick  TSpeedButtonBAreeRuoliENGLeft2Top� Width%Height
GroupIndexCaptionENGFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontVisibleOnClickBAreeRuoliENGClick  TBitBtnBitBtn1LeftTopWidthYHeight"TabOrder KindbkOK  TBitBtnBitBtn2Left�TopWidthYHeight"CaptionAnnullaTabOrderKindbkCancel  TPanelPanel73LeftTop� WidthHeight	AlignmenttaLeftJustifyCaption  AreeColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBGridDBGrid2LeftTopWidthHeight� 
DataSourceDsAreeReadOnly	TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameDescrizioneWidth� Visible	    TDBGridDBGrid3LeftTop)Width'Height� 
DataSourceDsRuoliReadOnly	TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameDescrizioneWidthVisible	    TPanelPanel2LeftTopWidth)Height	AlignmenttaLeftJustifyCaption*  Ruoli disponibili per l'area selezionataColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  	TCheckBox	CBInsCompLeftTop�Width)HeightCaption;attribuisci il ruolo e le relative competenze al nominativoChecked	Font.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontState	cbCheckedTabOrder  TPanel
PanAziendeLeftTopOWidth2Height� 
BevelOuterbvNoneTabOrderVisible TDBGridDBGrid1Left Top3Width�HeightuAlignalLeft
DataSourceDsElencoAziendeReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameDescrizioneTitle.CaptionNome aziendaWidth� Visible	 Expanded	FieldNameAttivitaWidth� Visible	 Expanded	FieldNameComuneWidthWVisible	 	AlignmenttaCenterExpanded	FieldName	ProvinciaTitle.CaptionProv.Visible	    TPanelPanel1Left�Top3WidthLHeightuAlignalClient
BevelOuter	bvLoweredCaptionPanel1TabOrder TToolbarButton97BAziendaNewLeftTopWidthJHeight#CaptionNuova azienda
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333�3333�39�3330 373337w39�3330 3��337w9���33337ww333?9���333 7wws333w39�3333 373333w39�3333337s3333?3333333 3333�33w333<333 3337�33w333<�3333����?�<����Ù37wwwww�<����Ù37wwwwsw3333<�3333337s33�333<330 3337337w3333330 3333337w3333333333333333	NumGlyphsWordWrap	OnClickBAziendaNewClick  TToolbarButton97BAziendaDelLeftTopGWidthJHeight#CaptionElimina
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 30    3337wwww�330�wwp3337�����330���p3337�����330��pp3337�����330���p3337�����330��pp3337�����330���p333�������00��pp0377�����33 ���p33w����s330��pp3337�����330pppp3337�����33     33wwwww33��ww33����33     33wwwwws3330wp333337���33330  333337ww333	NumGlyphsWordWrap	OnClickBAziendaDelClick  TToolbarButton97BAziendaModLeftTop$WidthJHeight#CaptionModifica
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 333     333wwwww333����?���??� 0  � �w�ww?sw7������ws3?33�࿿ ���w�3ws��7�������w�3?��7࿿  �w�3wwss7��������w�?���37�   ��w�wwws?� � �� �ws�w73w730 ���37wss3?�330���  33773�ww33��33s7s730���37�33s3	�� 33ws��w3303   3373wwws3	NumGlyphsWordWrap	OnClickBAziendaModClick   TPanelPanel3Left Top Width2HeightAlignalTop	AlignmenttaLeftJustifyCaption	  AziendeColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TPanelPanel6Left TopWidth2HeightAlignalTop
BevelOuter	bvLoweredTabOrder TLabelLabel1LeftTopWidthHeightCaptionCerca:  TEditEAziendaLeft)TopWidth� HeightTabOrder OnChangeEAziendaChange  	TCheckBoxCBTestoContLeft� TopWidth� HeightCaptionRicerca per testo contenutoTabOrderOnClickCBTestoContClick    TRadioGroup	RGOpzioneLeftTopWidth� HeightGCaptionOpzione	ItemIndex Items.Stringssolo settoreazienda (e suo settore)nessuno dei due TabOrderOnClickRGOpzioneClick  TPanel
PanSettoriLeftTopOWidth�Height� 
BevelOuterbvNoneTabOrder	 TPanelPanel4Left Top Width�HeightAlignalTop	AlignmenttaLeftJustifyCaption  Settori aziendaliColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   TDBGridDBGrid4Left TopWidthRHeight� AlignalLeft
DataSource	DsSettoriReadOnly	TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameAttivitaVisible	    TPanelPanel5LeftRTopWidthMHeight� AlignalClient
BevelOuter	bvLoweredTabOrder TToolbarButton97ToolbarButton971LeftTopWidthJHeight#CaptionNuovo settore
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333�3333�39�3330 373337w39�3330 3��337w9���33337ww333?9���333 7wws333w39�3333 373333w39�3333337s3333?3333333 3333�33w333<333 3337�33w333<�3333����?�<����Ù37wwwww�<����Ù37wwwwsw3333<�3333337s33�333<330 3337337w3333330 3333337w3333333333333333	NumGlyphsWordWrap	OnClickToolbarButton971Click  TSpeedButtonBSettoriITALeftTop*Width%Height
GroupIndexDown	CaptionITAFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontVisibleOnClickBSettoriITAClick  TSpeedButtonBSettoriENGLeftTop@Width%Height
GroupIndexCaptionENGFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontVisibleOnClickBSettoriENGClick    TDataSourceDsElencoAziendeDataSetTElencoAziendeLeftTop�   TTimerTimer1OnTimerTimer1TimerLeftoTopu  TTable
TSettoriLKActive	DatabaseNameEBCDB	TableNamedbo.EBC_AttivitaLeft?Top�  TAutoIncFieldTSettoriLKID	FieldNameIDReadOnly	  TStringFieldTSettoriLKAttivita	FieldNameAttivitaRequired	Size2   TDataSourceDsAreeDataSetTAreeLeftTope  TDataSourceDsRuoliDataSetTRuoliLeft?Top]  TTimerTimer2OnTimerTimer2TimerLeftWTopE  TDataSource	DsSettoriDataSetTSettoriLeftTop�   TQueryTElencoAziendeDatabaseNameEBCDBSQL.Stringsselect * from EBC_Clienti where Descrizione like :xDesc  LeftTop� 	ParamDataDataType	ftUnknownNamexDesc	ParamType	ptUnknown   TAutoIncFieldTElencoAziendeID	FieldNameIDOriginEBCDB.ElencoAziende.ID  TStringFieldTElencoAziendeDescrizione	FieldNameDescrizioneOriginEBCDB.ElencoAziende.Descrizione	FixedChar	Size  TStringFieldTElencoAziendeComune	FieldNameComuneOriginEBCDB.ElencoAziende.Comune	FixedChar	  TStringFieldTElencoAziendeProvincia	FieldName	ProvinciaOriginEBCDB.ElencoAziende.Provincia	FixedChar	Size  TIntegerFieldTElencoAziendeIDAttivita	FieldName
IDAttivitaOriginEBCDB.ElencoAziende.IDAttivita  TStringFieldTElencoAziendeAttivita	FieldKindfkLookup	FieldNameAttivitaLookupDataSet
TSettoriLKLookupKeyFieldsIDLookupResultFieldAttivita	KeyFields
IDAttivitaSize2Lookup	  TStringFieldTElencoAziendeIndirizzo	FieldName	Indirizzo	FixedChar	Size(  TStringFieldTElencoAziendeCap	FieldNameCap	FixedChar	Size  TFloatFieldTElencoAziendeFatturato	FieldName	Fatturato  TSmallintFieldTElencoAziendeNumDipendenti	FieldNameNumDipendenti  TStringFieldTElencoAziendeTipoStrada	FieldName
TipoStradaOriginEBCDB.EBC_Clienti.TipoStrada	FixedChar	Size
  TStringFieldTElencoAziendeNumCivico	FieldName	NumCivicoOriginEBCDB.EBC_Clienti.NumCivico	FixedChar	Size
  TStringFieldTElencoAziendeNazioneAbb	FieldName
NazioneAbbOriginEBCDB.EBC_Clienti.NazioneAbb	FixedChar	Size  TStringFieldTElencoAziendeTelefono	FieldNameTelefonoOriginEBCDB.EBC_Clienti.Telefono	FixedChar	Size  TStringFieldTElencoAziendeFax	FieldNameFaxOriginEBCDB.EBC_Clienti.Fax	FixedChar	Size  TStringFieldTElencoAziendeSitoInternet	FieldNameSitoInternetOriginEBCDB.EBC_Clienti.SitoInternet	FixedChar	Size2  TStringField!TElencoAziendeDescAttivitaAzienda	FieldNameDescAttivitaAziendaOrigin%EBCDB.EBC_Clienti.DescAttivitaAzienda	FixedChar	Size�    TQueryQDatabaseNameEBCDBLeftTop\  TQueryTSettoriActive	DatabaseNameEBCDBSQL.Stringsselect * from EBC_Attivitaorder by Attivita LeftTop�  TAutoIncField
TSettoriID	FieldNameIDOriginEBCDB.EBC_Attivita.ID  TStringFieldTSettoriAttivita	FieldNameAttivitaOriginEBCDB.EBC_Attivita.Attivita	FixedChar	Size2  TIntegerFieldTSettoriIDAreaSettore	FieldNameIDAreaSettoreOrigin EBCDB.EBC_Attivita.IDAreaSettore  TStringFieldTSettoriIDAzienda	FieldName	IDAziendaOriginEBCDB.EBC_Attivita.IDAzienda	FixedChar	Size
  TStringFieldTSettoriAttivita_ENG	FieldNameAttivita_ENGOriginEBCDB.EBC_Attivita.Attivita_ENG	FixedChar	Size2   TQueryTAreeActive	DatabaseNameEBCDBSQL.Stringsselect * from Areeorder by Descrizione Left TopH TAutoIncFieldTAreeID	FieldNameIDOriginEBCDB.Aree.ID  TStringFieldTAreeDescrizione	FieldNameDescrizioneOriginEBCDB.Aree.Descrizione	FixedChar	Size  TStringFieldTAreeDescrizione_ENG	FieldNameDescrizione_ENGOriginEBCDB.Aree.Descrizione_ENG	FixedChar	Size   TQueryTRuoliActive	DatabaseNameEBCDB
DataSourceDsAreeSQL.Strings-select ID,IDArea,Descrizione, Descrizione_ENGfrom Mansioniwhere IDArea=:IDorder by Descrizione Left@Top@	ParamDataDataType	ftAutoIncNameID	ParamType	ptUnknown   TAutoIncFieldTRuoliID	FieldNameIDOriginEBCDB.Mansioni.ID  TIntegerFieldTRuoliIDArea	FieldNameIDAreaOriginEBCDB.Mansioni.IDArea  TStringFieldTRuoliDescrizione	FieldNameDescrizioneOriginEBCDB.Mansioni.Descrizione	FixedChar	Size(  TStringFieldTRuoliDescrizione_ENG	FieldNameDescrizione_ENGOriginEBCDB.Mansioni.Descrizione_ENG	FixedChar	Size    