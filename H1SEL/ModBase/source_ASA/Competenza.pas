unit Competenza;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, Db, DBTables, StdCtrls, Buttons;

type
  TCompetenzaForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    EComp: TEdit;
    Label1: TLabel;
    DsAreeComp: TDataSource;
    DBGrid1: TDBGrid;
    Label2: TLabel;
    TAreeComp: TQuery;
    TAreeCompID: TAutoIncField;
    TAreeCompDescrizione: TStringField;
    Label3: TLabel;
    ETipo: TEdit;
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CompetenzaForm: TCompetenzaForm;

implementation

uses ModuloDati2;

{$R *.DFM}

procedure TCompetenzaForm.BitBtn1Click(Sender: TObject);
begin
     if trim(EComp.Text)='' then begin
        MessageDlg('La competenza non pu� avere valore nullo',mtError,[mbOK],0);
        ModalResult:=mrNone;
        Abort;
     end;
end;

end.
