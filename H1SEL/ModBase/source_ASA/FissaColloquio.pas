unit FissaColloquio;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     StdCtrls,Buttons,Grids,DBGrids,Mask,ComCtrls,Db,DBTables,DtEdit97;

type
     TFissaColloquioForm=class(TForm)
          GroupBox1: TGroupBox;
          Label4: TLabel;
          Label5: TLabel;
          Ora: TMaskEdit;
          DBGrid1: TDBGrid;
          BitBtn1: TBitBtn;
          TSelezionatori: TTable;
          TSelezionatoriID: TAutoIncField;
          TSelezionatoriNominativo: TStringField;
          DsSelez: TDataSource;
          DEData: TDateEdit97;
          BitBtn2: TBitBtn;
          CBInterno: TCheckBox;
          BitBtn3: TBitBtn;
          TRisorse: TTable;
          DsRisorse: TDataSource;
          TRisorseID: TAutoIncField;
          TRisorseRisorsa: TStringField;
          TRisorseColor: TStringField;
          DBGrid2: TDBGrid;
          Label1: TLabel;
          MEAlleOre: TMaskEdit;
    CBLuogo: TCheckBox;
          procedure BitBtn2Click(Sender: TObject);
          procedure CBInternoClick(Sender: TObject);
          procedure BitBtn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
     private
          { Private declarations }
     public
     end;

var
     FissaColloquioForm: TFissaColloquioForm;

implementation

uses AgendaSett,ModuloDati,ModuloDati2;

{$R *.DFM}

procedure TFissaColloquioForm.BitBtn2Click(Sender: TObject);
var xMin,xHour: integer;
begin
     // DISABILITAZIONE PROPOSTE Torino -- ALIAS='PROPOS' - per ora abilitata
     //if Uppercase(copy(Data.GlobalNomeAzienda.Value,1,6))='PROPOS' then begin
     //     ShowMessage('FUNZIONE NON ABILITATA');
     //     Exit;
     //end;
     AgendaSettForm:=TAgendaSettForm.create(self);
     AgendaSettForm.xIDSel:=TSelezionatoriID.Value;
     AgendaSettForm.Panel3.Visible:=False;
     AgendaSettForm.Panel4.Visible:=False;
     AgendaSettForm.TSCandidati.TabVisible:=False;
     AgendaSettForm.ASG1.PopupMenu:=nil;
     AgendaSettForm.ShowModal;
     if AgendaSettForm.PageControl1.ActivePage=AgendaSettForm.TSAgenda then begin
          DEData.Date:=AgendaSettForm.xPrimaData+AgendaSettForm.xSelCol-1;
          Ora.Text:=IntToStr(AgendaSettForm.xOre[AgendaSettForm.xSelRow])+'.00';
          MEAlleOre.Text:=IntToStr(AgendaSettForm.xOre[AgendaSettForm.xSelRow]+1)+'.00';
     end else begin
          DEData.Date:=AgendaSettForm.xPrimaData+AgendaSettForm.xSelColRis-1;
          Ora.Text:=IntToStr(AgendaSettForm.xOre[AgendaSettForm.xSelRowRis])+'.00';
          MEAlleOre.Text:=IntToStr(AgendaSettForm.xOre[AgendaSettForm.xSelRowRis]+1)+'.00';
          TRisorse.FindKey([AgendaSettForm.TRisorseID.Value]);
     end;
     AgendaSettForm.Free;
end;

procedure TFissaColloquioForm.CBInternoClick(Sender: TObject);
begin
     DBGrid2.Visible:=CBInterno.Checked;
end;

procedure TFissaColloquioForm.BitBtn1Click(Sender: TObject);
var xHour,xMin,xSec,xMSec,xHour2,xMin2,xSec2,xMSec2:Word;
begin
     // controllo orari
     if MEAlleOre.Text<>'  .  ' then
          if StrToDateTime(DatetoStr(DEData.Date)+' '+Ora.Text)>
               StrToDateTime(DatetoStr(DEData.Date)+' '+MEAlleOre.Text) then begin
               ModalResult:=mrNone;
               ShowMessage('Orari non corretti');
               Abort;
          end;
     // controllo concomitanza
     with Data2 do begin
          QCheck.Close;
          QCheck.Prepare;
          QCheck.ParamByName('xDalle').asDateTime:=StrToDateTime(DatetoStr(FissaColloquioForm.DEData.Date)+' '+FissaColloquioForm.Ora.Text);
          QCheck.ParamByName('xAlle').asDateTime:=StrToDateTime(DatetoStr(FissaColloquioForm.DEData.Date)+' '+FissaColloquioForm.MEAlleOre.Text);
          QCheck.ParamByName('xIDRis').asInteger:=FissaColloquioForm.TRisorseID.Value;
          QCheck.Open;
          if not QCheck.IsEmpty then begin
               ModalResult:=mrNone;
               DecodeTime(QCheck.FieldByName('Ore').asDateTime,xHour,xMin,xSec,xMSec);
               DecodeTime(QCheck.FieldByName('AlleOre').asDateTime,xHour2,xMin2,xSec2,xMSec2);
               MessageDlg('Risulta gi� presente il seguente impegno:'+chr(13)+
                    QCheck.FieldByName('Descrizione').asString+chr(13)+
                    'dalle '+IntToStr(xHour)+'.'+IntToStr(xMin)+
                    '  alle '+IntToStr(xHour2)+'.'+IntToStr(xMin2)+chr(13)+
                    'Luogo: '+TRisorseRisorsa.Value,mtError, [mbOK],0);
               Abort;
          end;
     end;
end;

procedure TFissaColloquioForm.FormShow(Sender: TObject);
begin
     Caption:='[E/5] - '+Caption;
end;

end.

