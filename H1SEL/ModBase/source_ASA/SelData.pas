unit SelData;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, DtEdit97, ImageWindow, ImgList, ExtCtrls;

type
  TSelDataForm = class(TForm)
    DEData: TDateEdit97;
    Label1: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    ImageList1: TImageList;
    ImageBox1: TImageBox;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    xData:TDateTime;
    xImageIndex:integer;
  end;

var
  SelDataForm: TSelDataForm;

implementation

{$R *.DFM}

procedure TSelDataForm.FormShow(Sender: TObject);
begin
     if xData=null then DEData.Date:=Date;
     if xImageIndex=null then ImageBox1.ImageIndex:=0
     else ImageBox1.ImageIndex:=xImageIndex;
     Caption:='[S/7] - '+caption;
end;

end.
