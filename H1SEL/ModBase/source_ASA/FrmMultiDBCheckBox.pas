unit FrmMultiDBCheckBox;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     ExtCtrls,RXCtrls,Db,DBTables, Buttons;

type
     TMultiDBCheckBoxFrame=class(TFrame)
          CLBox: TRxCheckListBox;
    PanTitolo: TPanel;
          Q: TQuery;
    Panel1: TPanel;
    SpeedButton3: TSpeedButton;
    procedure SpeedButton3Click(Sender: TObject);
     private
          xArrayID: array[1..100] of integer;
     public
          xIDAnag: integer;
          xTabBase,xTabCorrelaz,xCampoVis,xForeignKey: string;
          procedure LoadCLBox;
          procedure SaveToDb;
     end;

     // Usage:
     // xIDAnag:  Anagrafica.ID
     // xTabBase: tabella di base (es. DispContratti)
     // xTabCorrelaz: tabella di correlazione (es. AnagDispContratti)
     // xCampoVis: quale campo della tabella di base si vuole visualizzare nella lista
     //            la lista verr� ordinata per questo campo
     // xForeignKey: foreign key della tabella di correlazione verso la tabella di base
     //              (es. IDDispContratto in AnagDispContratti)

implementation

uses uUtilsVarie;

{$R *.DFM}

{ TMultiDBCheckBoxFrame }

procedure TMultiDBCheckBoxFrame.LoadCLBox;
var i: integer;
begin
     for i:=1 to 100 do xArrayID[i]:=0;
     Q.Close;
     Q.SQL.text:='select ID,'+xCampoVis+' from '+xTabBase+' order by '+xCampoVis;
     Q.Open;
     CLBox.Items.Clear;
     i:=1;
     while not Q.Eof do begin
          CLBox.Items.Add(Q.FieldByName(xCampoVis).asString);
          xArrayID[i]:=Q.FieldByName('ID').asInteger;
          Q.Next;
          inc(i);
     end;
     // quali selezionati
     Q.Close;
     Q.SQL.text:='select * from '+xTabCorrelaz+' where IDAnagrafica='+IntToStr(xIDAnag);
     Q.Open;
     while not Q.EOF do begin
          for i:=1 to 100 do begin
               if xArrayID[i]=Q.FieldByName(xForeignKey).asInteger then begin
                    CLBox.Checked[i-1]:=True;
                    break;
               end;
          end;
          Q.Next;
     end;
     Q.Close;
end;

procedure TMultiDBCheckBoxFrame.SaveToDb;
var i: integer;
begin
     Q.SQL.text:='delete from '+xTabCorrelaz+' where IDAnagrafica='+IntToStr(xIDAnag);
     Q.ExecSQL;
     Q.SQL.text:='insert into '+xTabCorrelaz+' (IDAnagrafica,'+xForeignKey+') '+
          'values ('+IntToStr(xIDAnag)+',:xForeignKey)';
     for i:=0 to CLBox.Items.Count-1 do begin
          if CLBox.Checked[i] then begin
             Q.ParamByName('xForeignKey').asInteger:=xArrayID[i+1];
             Q.ExecSQL;
          end;
     end;
end;

procedure TMultiDBCheckBoxFrame.SpeedButton3Click(Sender: TObject);
begin
     SaveToDb;
     OpenTab('DispContratti', ['ID','DispContratto'], ['Tipologia di contratto']);
     LoadCLBox;
end;

end.

