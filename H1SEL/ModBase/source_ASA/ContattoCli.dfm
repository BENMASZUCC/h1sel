object ContattoCliForm: TContattoCliForm
  Left = 261
  Top = 104
  ActiveControl = EContatto
  BorderStyle = bsDialog
  Caption = 'Riferimento interno'
  ClientHeight = 206
  ClientWidth = 385
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 9
    Top = 87
    Width = 40
    Height = 13
    Caption = 'Contatto'
  end
  object Label4: TLabel
    Left = 32
    Top = 135
    Width = 17
    Height = 13
    Caption = 'Fax'
  end
  object Label5: TLabel
    Left = 26
    Top = 183
    Width = 23
    Height = 13
    Caption = 'Note'
  end
  object Label3: TLabel
    Left = 7
    Top = 111
    Width = 42
    Height = 13
    Caption = 'Telefono'
  end
  object Label6: TLabel
    Left = 21
    Top = 159
    Width = 28
    Height = 13
    Caption = 'E-mail'
  end
  object Label7: TLabel
    Left = 20
    Top = 64
    Width = 29
    Height = 13
    Caption = 'Titolo:'
  end
  object BitBtn1: TBitBtn
    Left = 286
    Top = 3
    Width = 96
    Height = 35
    TabOrder = 6
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 286
    Top = 41
    Width = 96
    Height = 35
    Caption = 'Annulla'
    TabOrder = 7
    Kind = bkCancel
  end
  object Panel1: TPanel
    Left = 4
    Top = 5
    Width = 277
    Height = 48
    BevelOuter = bvLowered
    Enabled = False
    TabOrder = 8
    object Label1: TLabel
      Left = 6
      Top = 5
      Width = 35
      Height = 13
      Caption = 'Cliente:'
    end
    object DBEDenomCliente: TDBEdit
      Left = 6
      Top = 20
      Width = 265
      Height = 21
      Color = clAqua
      DataField = 'Descrizione'
      DataSource = Data2.DsEBCclienti
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
  end
  object EContatto: TEdit
    Left = 60
    Top = 83
    Width = 221
    Height = 21
    MaxLength = 50
    TabOrder = 1
  end
  object EFax: TEdit
    Left = 60
    Top = 131
    Width = 121
    Height = 21
    MaxLength = 30
    TabOrder = 3
  end
  object ENote: TEdit
    Left = 60
    Top = 179
    Width = 320
    Height = 21
    MaxLength = 200
    TabOrder = 5
  end
  object ETelefono: TEdit
    Left = 60
    Top = 107
    Width = 165
    Height = 21
    MaxLength = 30
    TabOrder = 2
  end
  object EEmail: TEdit
    Left = 60
    Top = 155
    Width = 165
    Height = 21
    MaxLength = 40
    TabOrder = 4
  end
  object ETitolo: TEdit
    Left = 60
    Top = 59
    Width = 101
    Height = 21
    MaxLength = 20
    TabOrder = 0
  end
end
