unit MessIniziale;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ImageWindow, ExtCtrls, TB97, Grids, DBGrids, Db,
  TeEngine, Series, GanttCh, DBTables, TeeProcs, Chart, DBChart, ImgList;

type
  TMessInizialeForm = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Image1: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Panel4: TPanel;
    Label3: TLabel;
    LOggi: TLabel;
    ImageBox1: TImageBox;
    ImageList2: TImageList;
    BitBtn1: TBitBtn;
    Panel5: TPanel;
    Label5: TLabel;
    LFuturi: TLabel;
    ImageBox2: TImageBox;
    Panel6: TPanel;
    Label4: TLabel;
    LScaduti: TLabel;
    ImageBox3: TImageBox;
    DBGrid5: TDBGrid;
    ToolbarButton978: TToolbarButton97;
    LDubbi: TLabel;
    procedure ToolbarButton978Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  MessInizialeForm: TMessInizialeForm;

implementation

uses ModuloDati;

{$R *.DFM}

procedure TMessInizialeForm.ToolbarButton978Click(Sender: TObject);
begin
     Data.DB.BeginTrans;
     try
        Data.Q1.Close;
        Data.Q1.SQL.text:='update Promemoria set DataLettura=:xDataLettura where ID='+Data.QPromOggiID.asString;
        Data.Q1.ParambyName('xDataLettura').asDateTime:=Date;
        Data.Q1.ExecSQL;
        Data.DB.CommitTrans;
        Data.QPromOggi.Close;
        Data.QPromOggi.Open;
     except
         Data.DB.RollbackTrans;
         MessageDlg('ERRORE SUL DATABASE:  operazione non completata',mtError,[mbOK],0);
     end;
end;

procedure TMessInizialeForm.FormShow(Sender: TObject);
begin
     Caption:='[A/1] - '+Caption;
end;

end.
