unit GestModelliWord;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     TB97,Grids,DBGrids,RXDBCtrl,ExtCtrls,StdCtrls,Buttons,Db,DBTables,
     DBCtrls,ComObj;

type
     TGestModelliWordForm=class(TForm)
          QModelli: TQuery;
          DsQModelli: TDataSource;
          QModelliID: TAutoIncField;
          QModelliNomeModello: TStringField;
          QModelliDescrizione: TStringField;
          QModelliTabellaMaster: TStringField;
          QModelliInizialiFileGenerato: TStringField;
          BitBtn1: TBitBtn;
          QCampiModello: TQuery;
          DsQCampiModello: TDataSource;
          QCampiModelloID: TAutoIncField;
          QCampiModelloIDModello: TIntegerField;
          QCampiModelloNomeCampo: TStringField;
          QCampiModelloCampo: TStringField;
          QCampiModelloTipoCampo: TStringField;
          Panel3: TPanel;
          RxDBGrid2: TRxDBGrid;
          Panel2: TPanel;
          Panel4: TPanel;
          BCampoNew: TToolbarButton97;
          BCampoMod: TToolbarButton97;
          BCampoDel: TToolbarButton97;
          Panel5: TPanel;
          Panel6: TPanel;
          ToolbarButton971: TToolbarButton97;
          ToolbarButton972: TToolbarButton97;
          ToolbarButton973: TToolbarButton97;
          Panel7: TPanel;
          RxDBGrid1: TRxDBGrid;
          DBText1: TDBText;
          Label1: TLabel;
          ToolbarButton974: TToolbarButton97;
          BAprimodello: TToolbarButton97;
          procedure ToolbarButton971Click(Sender: TObject);
          procedure ToolbarButton972Click(Sender: TObject);
          procedure ToolbarButton973Click(Sender: TObject);
          procedure BCampoNewClick(Sender: TObject);
          procedure BCampoModClick(Sender: TObject);
          procedure BCampoDelClick(Sender: TObject);
          procedure ToolbarButton974Click(Sender: TObject);
          procedure BAprimodelloClick(Sender: TObject);
          procedure FormShow(Sender: TObject);
     private
          function GetDocPath: string;
     public
          { Public declarations }
     end;

var
     GestModelliWordForm: TGestModelliWordForm;

implementation

uses ModelloWord,ModuloDati,CampiModello,uUtilsVarie;

{$R *.DFM}

procedure TGestModelliWordForm.ToolbarButton971Click(Sender: TObject);
begin
     ModelloWordForm:=TModelloWordForm.create(self);
     ModelloWordForm.showModal;
     if ModelloWordForm.Modalresult=mrOK then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text:='insert into ModelliWord (NomeModello,Descrizione,TabellaMaster,InizialiFileGenerato) '+
                         'values (:xNomeModello,:xDescrizione,:xTabellaMaster,:xInizialiFileGenerato)';
                    Q1.ParamByName('xNomeModello').asString:=ModelloWordForm.EModello.text;
                    Q1.ParamByName('xDescrizione').asString:=ModelloWordForm.EDesc.text;
                    case ModelloWordForm.RGTabella.ItemIndex of
                         0: Q1.ParamByName('xTabellaMaster').asString:='Anagrafica';
                         1: Q1.ParamByName('xTabellaMaster').asString:='EBC_Clienti';
                         2: Q1.ParamByName('xTabellaMaster').asString:='*RicClienti';
                    end;
                    Q1.ParamByName('xInizialiFileGenerato').asString:=ModelloWordForm.EIniziali.text;
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    QModelli.Close;
                    QModelli.Open;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               end;
          end;
     end;
     ModelloWordForm.Free;
end;

procedure TGestModelliWordForm.ToolbarButton972Click(Sender: TObject);
begin
     if QModelli.RecordCount=0 then exit;
     ModelloWordForm:=TModelloWordForm.create(self);
     ModelloWordForm.EModello.text:=QModelliNomeModello.Value;
     ModelloWordForm.EDesc.text:=QModelliDescrizione.Value;
     if QModelliTabellaMaster.Value='Anagrafica' then
          ModelloWordForm.RGTabella.ItemIndex:=0;
     if QModelliTabellaMaster.Value='EBC_Clienti' then
          ModelloWordForm.RGTabella.ItemIndex:=1;
     if QModelliTabellaMaster.Value='*RicClienti' then
          ModelloWordForm.RGTabella.ItemIndex:=2;
     ModelloWordForm.EIniziali.text:=QModelliInizialiFileGenerato.Value;
     ModelloWordForm.showModal;
     if ModelloWordForm.Modalresult=mrOK then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text:='update ModelliWord set NomeModello=:xNomeModello,Descrizione=:xDescrizione,'+
                         'TabellaMaster=:xTabellaMaster,InizialiFileGenerato=:xInizialiFileGenerato '+
                         'where ID='+QModelliID.AsString;
                    Q1.ParamByName('xNomeModello').asString:=ModelloWordForm.EModello.text;
                    Q1.ParamByName('xDescrizione').asString:=ModelloWordForm.EDesc.text;
                    case ModelloWordForm.RGTabella.ItemIndex of
                         0: Q1.ParamByName('xTabellaMaster').asString:='Anagrafica';
                         1: Q1.ParamByName('xTabellaMaster').asString:='EBC_Clienti';
                         2: Q1.ParamByName('xTabellaMaster').asString:='*RicClienti';
                    end;
                    Q1.ParamByName('xInizialiFileGenerato').asString:=ModelloWordForm.EIniziali.text;
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    QModelli.Close;
                    QModelli.Open;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               end;
          end;
     end;
     ModelloWordForm.Free;
end;

procedure TGestModelliWordForm.ToolbarButton973Click(Sender: TObject);
begin
     if QModelli.isEmpty then exit;
     if QCampiModello.RecordCount>0 then begin
          ShowMessage('IMPOSSIBILE CANCELLARE: ci sono campi che referenziano questo modello');
          exit;
     end;
     if MessageDlg('Sei sicuro di voler cancellare il modello selezionato ?',mtWarning, [mbYes,mbNo],0)=mrYes then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text:='delete from ModelliWord '+
                         'where ID='+QModelliID.asString;
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    QModelli.Close;
                    QModelli.Open;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               end;
          end;
     end;
end;

procedure TGestModelliWordForm.BCampoNewClick(Sender: TObject);
begin
     CampiModelloForm:=TCampiModelloForm.create(self);
     // elenco campi in funzione della tabella
     // ogni tabella ha un file di testo con tale elenco
     // ogni riga ha la sintassi:  <campo> (<sua descrizione>)
     CampiModelloForm.CBCampo.items.Clear;
     if QModelliTabellaMaster.value='Anagrafica' then // PER ORA NO AnagAltreInfo
          CampiModelloForm.CBCampo.items.LoadFromFile(ExtractFilePath(Application.ExeName)+'\MWAnagrafica.txt');
     if QModelliTabellaMaster.value='EBC_Clienti' then
          CampiModelloForm.CBCampo.items.LoadFromFile('');
     if QModelliTabellaMaster.value='*RicClienti' then
          CampiModelloForm.CBCampo.items.LoadFromFile(ExtractFilePath(Application.ExeName)+'\MWRicClienti.txt');
     CampiModelloForm.ShowModal;
     if CampiModelloForm.ModalResult=mrOK then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text:='insert into ModelliWordCampi (IDModello,NomeCampo,Campo) '+
                         'values (:xIDModello,:xNomeCampo,:xCampo)';
                    Q1.ParamByName('xIDModello').asInteger:=QModelliID.value;
                    Q1.ParamByName('xNomeCampo').asString:=CampiModelloForm.EDescWord.text;
                    Q1.ParamByName('xCampo').asString:=copy(CampiModelloForm.CBCampo.text,1,pos('(',CampiModelloForm.CBCampo.text)-1);
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    QCampiModello.Close;
                    QCampiModello.Open;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               end;
          end;
     end;
     CampiModelloForm.Free;
end;

procedure TGestModelliWordForm.BCampoModClick(Sender: TObject);
begin
     CampiModelloForm:=TCampiModelloForm.create(self);
     // elenco campi in funzione della tabella
     // ogni tabella ha un file di testo con tale elenco
     // ogni riga ha la sintassi:  <campo> (<sua descrizione>)
     CampiModelloForm.CBCampo.items.Clear;
     if QModelliTabellaMaster.value='Anagrafica' then // PER ORA NO AnagAltreInfo
          CampiModelloForm.CBCampo.items.LoadFromFile('MWAnagrafica.txt');
     if QModelliTabellaMaster.value='EBC_Clienti' then
          CampiModelloForm.CBCampo.items.LoadFromFile('');
     if QModelliTabellaMaster.value='*RicClienti' then
          CampiModelloForm.CBCampo.items.LoadFromFile('MWRicClienti.txt');
     CampiModelloForm.CBCampo.text:=QCampiModelloCampo.Value;
     CampiModelloForm.EDescWord.text:=QCampiModelloNomeCampo.Value;
     CampiModelloForm.ShowModal;
     if CampiModelloForm.ModalResult=mrOK then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text:='update ModelliWordCampi set NomeCampo=:xNomeCampo,Campo=:xCampo '+
                         'where ID='+QCampiModelloID.asString;
                    Q1.ParamByName('xNomeCampo').asString:=CampiModelloForm.EDescWord.text;
                    Q1.ParamByName('xCampo').asString:=copy(CampiModelloForm.CBCampo.text,1,pos('(',CampiModelloForm.CBCampo.text)-1);
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    QCampiModello.Close;
                    QCampiModello.Open;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               end;
          end;
     end;
     CampiModelloForm.Free;
end;

procedure TGestModelliWordForm.BCampoDelClick(Sender: TObject);
begin
     if QCampiModello.isEmpty then exit;
     if MessageDlg('Sei sicuro di voler cancellare il campo selezionato ?',mtWarning, [mbYes,mbNo],0)=mrYes then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text:='delete from ModelliWordCampi '+
                         'where ID='+QCampiModelloID.asString;
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    QCampiModello.Close;
                    QCampiModello.Open;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               end;
          end;
     end;
end;

procedure TGestModelliWordForm.ToolbarButton974Click(Sender: TObject);
var xModelloWord: string;
     MSWord: Variant;
begin
     if QModelli.IsEmpty then exit;
     if QCampiModello.IsEmpty then begin
          ShowMessage('Inserire prima i campi del modello');
          exit;
     end;
     xModelloWord:=QModelliNomeModello.Value;
     if FileExists(GetDocPath+'\'+xModelloWord+'.doc') then
          if MessageDlg('ATTENZIONE: il modello esiste gi�. Vuoi ricrearlo ?',mtWarning, [mbYes,mbNo],0)=mrNo then exit
          else if not DeleteFile(GetDocPath+'\'+xModelloWord+'.doc') then begin
               ShowMessage('PROBLEMI NELLA CANCELLAZIONE DEL FILE');
               exit;
          end;

     // Apri Word
     try MsWord:=CreateOleObject('Word.Basic');
     except
          ShowMessage('Non riesco ad aprire Microsoft Word.');
          exit;
     end;
     MsWord.AppShow;
     MsWord.FileNew;
     // intestazione
     MsWord.Insert('MODELLO PERSONALIZZATO '+QModelliDescrizione.Value+chr(13)+chr(13));
     // riempimento con i campi
     QCampiModello.First;
     while not QCampiModello.EOF do begin
          MsWord.Insert(QCampiModelloCampo.Value+': '+QCampiModelloNomeCampo.Value+chr(13));
          QCampiModello.Next;
     end;
     MsWord.FileSaveAs(GetDocPath+'\'+xModelloWord+'.doc');
     //MsWord.FileClose;
     //MsWord.AppClose;
end;

procedure TGestModelliWordForm.BAprimodelloClick(Sender: TObject);
var xModelloWord: string;
     MSWord: Variant;
begin
     if QModelli.IsEmpty then exit;
     xModelloWord:=QModelliNomeModello.Value;
     if not FileExists(GetDocPath+'\'+xModelloWord+'.doc') then begin
          MessageDlg('Il file non esiste',mtError, [mbOK],0);
          exit;
     end;
     // Apri Word
     try MsWord:=CreateOleObject('Word.Basic');
     except
          ShowMessage('Non riesco ad aprire Microsoft Word.');
          exit;
     end;
     MsWord.AppShow;
     MsWord.FileOpen(GetDocPath+'\'+xModelloWord+'.doc');
end;

procedure TGestModelliWordForm.FormShow(Sender: TObject);
begin
     Caption:='[M/792] - '+Caption;
end;

function TGestModelliWordForm.GetDocPath: string;
var xH1DocPath: string;
begin
     // restituisce il path della cartella documenti
     xH1DocPath:=LeggiRegistry('H1DocPath');
     if xH1DocPath<>'' then begin
          Result:=xH1DocPath;
     end else begin
          with Data.Q2 do begin
               Close;
               SQL.text:='select DirFileDoc from Global';
               Open;
               if IsEmpty then Result:=''
               else Result:=FieldByName('DirFileDoc').asString;
               Close;
          end;
     end;
end;

end.

