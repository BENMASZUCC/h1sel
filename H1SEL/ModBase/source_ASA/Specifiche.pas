unit Specifiche;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     StdCtrls,Buttons,ExtCtrls,ComCtrls,DBCtrls,TB97,ImgList,LookOut,
     Mask,DB,FrameDBRichEdit2;

type
     TSpecificheForm=class(TForm)
          Panel1: TPanel;
          Panel2: TPanel;
          Label1: TLabel;
          DBEdit1: TDBEdit;
          Label2: TLabel;
          DBEdit2: TDBEdit;
          Label3: TLabel;
          DBEdit3: TDBEdit;
          Label4: TLabel;
          Label5: TLabel;
          DBEdit4: TDBEdit;
          DBEdit5: TDBEdit;
          Panel3: TPanel;
          ImageList1: TImageList;
          ToolbarButton971: TToolbarButton97;
          PCSpec: TPageControl;
          TSInterne: TTabSheet;
          TSEsterne: TTabSheet;
          Timer1: TTimer;
          PanMess: TPanel;
          PanMess2: TPanel;
          FrameDBRichInt: TFrameDBRich;
          FrameDBRichEst: TFrameDBRich;
          procedure ToolbarButton971Click(Sender: TObject);
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
          procedure Timer1Timer(Sender: TObject);
    procedure FormShow(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     SpecificheForm: TSpecificheForm;

implementation

uses ModuloDati,ModuloDati2,REMain,MDRicerche,Main,UnitPrintMemo,
  uUtilsVarie;

{$R *.DFM}

procedure TSpecificheForm.ToolbarButton971Click(Sender: TObject);
begin
     close
end;

procedure TSpecificheForm.FormClose(Sender: TObject;
     var Action: TCloseAction);
begin
     //if DataRicerche.DsRicerchePend.State=dsEdit then begin
     with DataRicerche.TRicerchePend do begin
          Data.DB.BeginTrans;
          try
               ApplyUpdates;
               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               raise;
          end;
          CommitUpdates;
     end;
     //end;
     DataRicerche.QRicAttive.Close;
     Mainform.EseguiQRicAttive;
end;

procedure TSpecificheForm.Timer1Timer(Sender: TObject);
begin
     if FrameDBRichInt.Editor.Lines.Count>0 then begin
          ScriviRegistry('H1SpecifInt',FrameDBRichInt.Editor.Lines.Text);
          PanMess2.caption:='  Ultimo salvataggio specifiche INTERNE nella chiave del Registry "HKEY_CURRENT_USER\Software\H1\H1SpecifInt.txt" alle ore '+TimeToStr(Now);
     end;
     if FrameDBRichEst.Editor.Lines.Count>0 then begin
          ScriviRegistry('H1SpecifEst',FrameDBRichEst.Editor.Lines.Text);
          PanMess.caption:='  Ultimo salvataggio specifiche INTERNE nella chiave del Registry "HKEY_CURRENT_USER\Software\H1\H1SpecifEst.txt" alle ore '+TimeToStr(Now);
     end;
end;

procedure TSpecificheForm.FormShow(Sender: TObject);
begin
     Caption:='[M/22] - '+caption;
end;

end.

