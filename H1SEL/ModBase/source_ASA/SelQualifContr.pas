unit SelQualifContr;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Grids, DBGrids, Db, DBTables;

type
  TSelQualifContrForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    QQualif: TQuery;
    dsQQualif: TDataSource;
    DBGrid1: TDBGrid;
    QQualifID: TAutoIncField;
    QQualifTipoContratto: TStringField;
    QQualifQualifica: TStringField;
    BitBtn3: TBitBtn;
    procedure BitBtn3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SelQualifContrForm: TSelQualifContrForm;

implementation

uses TabContratti;

{$R *.DFM}

procedure TSelQualifContrForm.BitBtn3Click(Sender: TObject);
begin
     TabContrattiForm:=TTabContrattiForm.create(self);
     TabContrattiForm.ShowModal;
     TabContrattiForm.Free;
     QQualif.Close;
     QQualif.Open;
end;

end.
