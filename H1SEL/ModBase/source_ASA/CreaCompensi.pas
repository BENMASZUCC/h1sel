unit CreaCompensi;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Grids, AdvGrid, DtEdit97;

type
  TCreaCompensiForm = class(TForm)
    BitBtn1: TBitBtn;
    ASG1: TAdvStringGrid;
    Label1: TLabel;
    BitBtn2: TBitBtn;
    procedure ASG1CanEditCell(Sender: TObject; Arow, Acol: Integer;
      var canedit: Boolean);
  private
    { Private declarations }
  public
    xArrayID:array[1..50] of integer;
  end;

var
  CreaCompensiForm: TCreaCompensiForm;

implementation

{$R *.DFM}

procedure TCreaCompensiForm.ASG1CanEditCell(Sender: TObject; Arow,
  Acol: Integer; var canedit: Boolean);
begin
     if (ARow>0)and(Acol>0) then canedit:=False
     else canedit:=True;
end;

end.
