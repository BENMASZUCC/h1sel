unit SelPersNew;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, DBCtrls, Grids, DBGrids, Db, DBTables, Buttons, TB97,
  ExtCtrls, ComCtrls, ComObj, RXDBCtrl, dxDBGrid, dxDBTLCl, dxGrClms, dxTL,
  dxDBCtrl, dxCntner, dxGridMenus, ImgList, Menus, dxPSCore, dxPSdxTLLnk,
  dxPSdxDBGrLnk, Spin, dxPSdxDBCtrlLnk, ADODB, UCrypter;

type
  TSaveMethod = procedure(const FileName: string; ASaveAll: Boolean) of object;
  TSelPersNewForm = class(TForm)
    DsTabQuery: TDataSource;
    TTabOp: TTable;
    DsTabOp: TDataSource;
    TTabOpIDTabQuery: TIntegerField;
    TTabOpOperatore: TStringField;
    TTabOpDescOperatore: TStringField;
    DsLinee: TDataSource;
    Query1: TQuery;
    TTabLookup: TTable;
    DsTabLookup: TDataSource;
    TTabLookupID: TAutoIncField;
    TTabLookupLingua: TStringField;
    QRes1: TQuery;
    DsQRes1: TDataSource;
    QRes1ID: TIntegerField;
    QRes1Cognome: TStringField;
    QRes1Nome: TStringField;
    DsQTabLista: TDataSource;
    QTabelle: TQuery;
    QTabelleDescTabella: TStringField;
    QRes1DataNascita: TDateTimeField;
    QRes1IDStato: TIntegerField;
    QRes1IDTipoStato: TIntegerField;
    TStatiLK: TTable;
    TStatiLKID: TAutoIncField;
    TStatiLKStato: TStringField;
    TStatiLKIDTipoStato: TIntegerField;
    QRes1Stato: TStringField;
    TTipiStatiLK: TTable;
    TTipiStatiLKID: TAutoIncField;
    TTipiStatiLKTipoStato: TStringField;
    QRes1TipoStato: TStringField;
    PanRic: TPanel;
    BitBtn3: TBitBtn;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    DBGrid4: TDBGrid;
    ComboBox1: TComboBox;
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
    Edit1: TEdit;
    BitBtn1: TBitBtn;
    ComboBox2: TComboBox;
    BitBtn6: TBitBtn;
    BitBtn2: TBitBtn;
    Panel1: TPanel;
    BitBtn4: TBitBtn;
    BitBtn8: TBitBtn;
    QRes1CVNumero: TIntegerField;
    BAggiungiRic: TBitBtn;
    QRicLineeQuery: TQuery;
    QRicLineeQueryID: TIntegerField;
    QRicLineeQueryIDRicerca: TIntegerField;
    QRicLineeQueryDescrizione: TStringField;
    QRicLineeQueryStringa: TStringField;
    QRicLineeQueryTabella: TStringField;
    QRicLineeQueryOpSucc: TStringField;
    QRicLineeQueryNext: TStringField;
    TRicLineeQuery: TTable;
    TRicLineeQueryID: TAutoIncField;
    TRicLineeQueryIDRicerca: TIntegerField;
    TRicLineeQueryDescrizione: TStringField;
    TRicLineeQueryStringa: TStringField;
    TRicLineeQueryTabella: TStringField;
    TRicLineeQueryOpSucc: TStringField;
    TRicLineeQueryNext: TStringField;
    BApriForm: TBitBtn;
    BCurriculum: TBitBtn;
    BitBtn9: TBitBtn;
    BitBtn10: TBitBtn;
    Q: TQuery;
    QRes1Eta: TStringField;
    QRes1DomicilioComune: TStringField;
    QRes1OldCVNumero: TStringField;
    QRes1TelUfficio: TStringField;
    QRes1Cellulare: TStringField;
    QRes1TelUffCell: TStringField;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel10: TPanel;
    LTot: TLabel;
    GroupBox2: TGroupBox;
    CBColloquiati: TCheckBox;
    CBPropri: TCheckBox;
    QRes1ggUltimaDataIns: TIntegerField;
    QRes1ggDataUltimoContatto: TIntegerField;
    QRes1DataProssimoColloquio: TDateTimeField;
    QRes1ggDataUltimoColloquio: TIntegerField;
    QLastEspLav: TQuery;
    QLastEspLavIDAnagrafica: TIntegerField;
    QLastEspLavMaxID: TIntegerField;
    QEspLavLK: TQuery;
    QEspLavLKID: TAutoIncField;
    QEspLavLKAzienda: TStringField;
    QEspLavLKRuolo: TStringField;
    QLastEspLavAzienda: TStringField;
    QLastEspLavRuolo: TStringField;
    QRes1Azienda: TStringField;
    QRes1Ruolo: TStringField;
    QRes1ggDataInsCV: TIntegerField;
    dxDBGrid1: TdxDBGrid;
    dxDBGrid1Cognome: TdxDBGridMaskColumn;
    dxDBGrid1Nome: TdxDBGridMaskColumn;
    dxDBGrid1DataNascita: TdxDBGridDateColumn;
    dxDBGrid1TipoStato: TdxDBGridLookupColumn;
    dxDBGrid1CVNumero: TdxDBGridMaskColumn;
    dxDBGrid1Eta: TdxDBGridColumn;
    dxDBGrid1DomicilioComune: TdxDBGridMaskColumn;
    dxDBGrid1OldCVNumero: TdxDBGridMaskColumn;
    dxDBGrid1Azienda: TdxDBGridLookupColumn;
    dxDBGrid1Ruolo: TdxDBGridLookupColumn;
    dxDBGrid1TelUfficio: TdxDBGridMaskColumn;
    dxDBGrid1Cellulare: TdxDBGridMaskColumn;
    dxDBGrid1TelUffCell: TdxDBGridColumn;
    dxDBGrid1ggUltimaDataIns: TdxDBGridMaskColumn;
    dxDBGrid1ggDataUltimoContatto: TdxDBGridMaskColumn;
    dxDBGrid1DataProssimoColloquio: TdxDBGridDateColumn;
    dxDBGrid1ggDataUltimoColloquio: TdxDBGridMaskColumn;
    dxDBGrid1ggDataInsCV: TdxDBGridMaskColumn;
    dxDBGrid1IDStato: TdxDBGridColumn;
    PMExport: TPopupMenu;
    Stampaelenco1: TMenuItem;
    EsportainExcel1: TMenuItem;
    EsportainHTML1: TMenuItem;
    ImageList1: TImageList;
    SaveDialog: TSaveDialog;
    BLegenda: TToolbarButton97;
    ToolbarButton972: TToolbarButton97;
    BEmail: TToolbarButton97;
    Stampagriglia1: TMenuItem;
    dxPrinter1: TdxComponentPrinter;
    dxPrinter1Link1: TdxDBGridReportLink;
    GroupBox3: TGroupBox;
    CBAnzCV: TCheckBox;
    SEAnzCV: TSpinEdit;
    TTabQuery: TQuery;
    TTabQueryID: TAutoIncField;
    TTabQueryTabella: TStringField;
    TTabQueryDescTabella: TStringField;
    TTabQueryCampo: TStringField;
    TTabQueryDescCampo: TStringField;
    TTabQueryValoriPossibili: TStringField;
    TTabQueryTipoCampo: TStringField;
    TTabQueryTabLookup: TStringField;
    TTabQueryTabLookupIdx: TStringField;
    TTabQueryFormLookupID: TIntegerField;
    TTabQueryFieldLookup: TStringField;
    TTabQueryFieldLookupSize: TSmallintField;
    QTabelleLista: TQuery;
    QRes1CVInseritoInData: TDateTimeField;
    QRes1CVDataReale: TDateTimeField;
    dxDBGrid1DataRealeCV: TdxDBGridDateColumn;
    Label5: TLabel;
    TLinee: TQuery;
    TLineeID: TAutoIncField;
    TLineeIDAnagrafica: TIntegerField;
    TLineeDescrizione: TStringField;
    TLineeTabella: TStringField;
    TLineeOpSucc: TStringField;
    TLineeOpNext: TStringField;
    QTabelleListaTabella: TStringField;
    QRes1CVDataAgg: TDateTimeField;
    cancellasoggetti1: TMenuItem;
    QRes1RecapitiTelefonici: TStringField;
    dxDBGrid1Column24: TdxDBGridColumn;
    QRes1NoCV: TBooleanField;
    dxDBGrid1NoCv: TdxDBGridCheckColumn;
    PMStati: TPopupMenu;
    PMStati1: TMenuItem;
    PMStati2: TMenuItem;
    PMStati3: TMenuItem;
    PMStati4: TMenuItem;
    PMStati5: TMenuItem;
    cancellatuttiisoggettiselezionati1: TMenuItem;
    mettiinstatoeliminato1: TMenuItem;
    TLineeLivLinguaParlato: TIntegerField;
    TLineeLivLinguaScritto: TIntegerField;
    DBGrid3: TdxDBGrid;
    DBGrid3Descrizione: TdxDBGridMaskColumn;
    DBGrid3LivLinguaParlato: TdxDBGridButtonColumn;
    DBGrid3LivLinguaScritto: TdxDBGridButtonColumn;
    DBGrid3OpNext: TdxDBGridPickColumn;
    BFiltroStati: TToolbarButton97;
    QRes1ContattoCliente: TStringField;
    dxDBGrid1ContattoCli: TdxDBGridColumn;
    QLastEspLavCodPersUnica: TStringField;
    QRes1CodPers: TStringField;
    dxDBGrid1Column26: TdxDBGridColumn;
    QEspLavLKTitoloMansione: TStringField;
    QEspLavTotValRetribLK: TQuery;
    QEspLavTotValRetribLKIDEspLav: TIntegerField;
    QEspLavTotValRetribLKTotValore: TFloatField;
    QEspLavLKRetribuzione: TFloatField;
    QLastEspLavRetribuzione: TFloatField;
    QRes1Retribuzione: TFloatField;
    dxDBGrid1Column27: TdxDBGridColumn;
    QDatiIns: TQuery;
    QDatiInsNominativo: TStringField;
    QDatiInsDataOra: TDateTimeField;
    QDatiMod: TQuery;
    QDatiModNominativo: TStringField;
    QDatiModKeyValue: TIntegerField;
    QDatiModDataOra: TDateTimeField;
    QDatiInsKeyValue: TIntegerField;
    QRes1DataIns: TDateTimeField;
    dxDBGrid1Column28: TdxDBGridDateColumn;
    QRes1DataAgg: TDateTimeField;
    dxDBGrid1Column29: TdxDBGridDateColumn;
    TTabQuerySubQuery: TStringField;
    TLineeStringa: TStringField;
    DBGrid41: TdxDBGrid;
    DBGrid41ID: TdxDBGridMaskColumn;
    DBGrid41Lingua: TdxDBGridMaskColumn;
    Panel4: TPanel;
    PanInfoRet: TPanel;
    Label6: TLabel;
    CBIRnote: TCheckBox;
    EIRNote: TEdit;
    CBIRNoteInflectional: TCheckBox;
    CBIRFileSystem: TCheckBox;
    CBIRaltreRegole: TCheckBox;
    qRes1Ado_old: TADOQuery;
    qRes1Ado_oldID: TAutoIncField;
    qRes1Ado_oldCognome: TStringField;
    qRes1Ado_oldNome: TStringField;
    qRes1Ado_oldCVNumero: TIntegerField;
    qRes1Ado_oldOldCVNumero: TStringField;
    qRes1Ado_oldTelUfficio: TStringField;
    qRes1Ado_oldCellulare: TStringField;
    qRes1Ado_oldDataNascita: TDateTimeField;
    qRes1Ado_oldIDStato: TIntegerField;
    qRes1Ado_oldIDTipoStato: TIntegerField;
    qRes1Ado_oldDomicilioComune: TStringField;
    qRes1Ado_oldggUltimaDataIns: TIntegerField;
    qRes1Ado_oldggDataUltimoContatto: TIntegerField;
    qRes1Ado_oldDataProssimoColloquio: TDateTimeField;
    qRes1Ado_oldggDataUltimoColloquio: TIntegerField;
    qRes1Ado_oldggDataInsCV: TIntegerField;
    qRes1Ado_oldContattoCliente: TStringField;
    TwCrypter1: TTwCrypter;
    QRes1ADO: TADODataSet;
    QRes1ADOID: TAutoIncField;
    QRes1ADOCognome: TStringField;
    QRes1ADONome: TStringField;
    QRes1ADOCVNumero: TIntegerField;
    QRes1ADOOldCVNumero: TStringField;
    QRes1ADOTelUfficio: TStringField;
    QRes1ADOCellulare: TStringField;
    QRes1ADODataNascita: TDateTimeField;
    QRes1ADOIDStato: TIntegerField;
    QRes1ADOIDTipoStato: TIntegerField;
    QRes1ADODomicilioComune: TStringField;
    QRes1ADOggUltimaDataIns: TIntegerField;
    QRes1ADOggDataUltimoContatto: TIntegerField;
    QRes1ADODataProssimoColloquio: TDateTimeField;
    QRes1ADOggDataUltimoColloquio: TIntegerField;
    QRes1ADOggDataInsCV: TIntegerField;
    QRes1ADOContattoCliente: TStringField;
    QRes1ADOStato: TStringField;
    QRes1ADOTipoStato: TStringField;
    QRes1ADOEta: TStringField;
    QRes1ADOAzienda: TStringField;
    QRes1ADOTelUffCell: TStringField;
    QRes1ADOCVInseritoInData: TDateTimeField;
    QRes1ADOCVDataReale: TDateTimeField;
    QRes1ADOCVDataAgg: TDateTimeField;
    QRes1ADORecapitiTelefonici: TStringField;
    QRes1ADONoCV: TBooleanField;
    QRes1ADORuolo: TStringField;
    QRes1ADOCodPers: TStringField;
    QRes1ADORetribuzione: TFloatField;
    QRes1ADODataIns: TDateTimeField;
    QRes1ADODataAgg: TDateTimeField;
    procedure ComboBox1Change(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure TTabQueryAfterScroll(DataSet: TDataSet);
    procedure BitBtn3Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure DBGrid4KeyPress(Sender: TObject; var Key: Char);
    procedure BitBtn8Click(Sender: TObject);
    procedure BAggiungiRicClick(Sender: TObject);
    procedure TLineeBeforePost(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BApriFormClick(Sender: TObject);
    procedure BCurriculumClick(Sender: TObject);
    procedure BitBtn9Click(Sender: TObject);
    procedure BitBtn10Click(Sender: TObject);
    procedure QRes1CalcFields(DataSet: TDataSet);
    procedure CBColloquiatiClick(Sender: TObject);
    procedure QRes1BeforeOpen(DataSet: TDataSet);
    procedure QRes1AfterClose(DataSet: TDataSet);
    procedure dxDBGrid1Click(Sender: TObject);
    procedure dxDBGrid1CustomDrawCell(Sender: TObject; ACanvas: TCanvas;
      ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
      ASelected, AFocused, ANewItemRow: Boolean; var AText: string;
      var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
      var ADone: Boolean);
    procedure dxDBGrid1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Stampaelenco1Click(Sender: TObject);
    procedure EsportainHTML1Click(Sender: TObject);
    procedure EsportainExcel1Click(Sender: TObject);
    procedure ToolbarButton971Click(Sender: TObject);
    procedure ToolbarButton973Click(Sender: TObject);
    procedure Stampagriglia1Click(Sender: TObject);
    procedure TTabQueryAfterOpen(DataSet: TDataSet);
    procedure cancellasoggetti1Click(Sender: TObject);
    procedure PMStati1Click(Sender: TObject);
    procedure PMStati2Click(Sender: TObject);
    procedure PMStati3Click(Sender: TObject);
    procedure PMStati4Click(Sender: TObject);
    procedure PMStati5Click(Sender: TObject);
    procedure cancellatuttiisoggettiselezionati1Click(Sender: TObject);
    procedure mettiinstatoeliminato1Click(Sender: TObject);
    procedure DBGrid3LivLinguaParlatoButtonClick(Sender: TObject;
      AbsoluteIndex: Integer);
    procedure DBGrid3LivLinguaScrittoButtonClick(Sender: TObject;
      AbsoluteIndex: Integer);
    procedure qRes1Ado_oldBeforeOpen(DataSet: TDataSet);
    procedure QRes1ADOCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    xStringa, xIndexName, xOrdine: string;
    xFormID: integer;
    xggDiffDi, xggDiffUc, xggDiffCol: integer;
    xCheckRicCandCV: boolean;
    procedure Save(ADefaultExt, AFilter, AFileName: string; AMethod: TSaveMethod);
    function LogEsportazioniCand: boolean;
  public
    xDaEscludere: array[1..500] of integer;
    xChiamante: integer;
    xIsFulltextInstalled, xAnagAltreInfoOK, xFileSystemOK, xRegoleCollImpostate: boolean;
    xIRFileSystemScope: string;
  end;

var
  SelPersNewForm: TSelPersNewForm;

implementation

uses ModuloDati, RepElencoSel, View, SelPers, MDRicerche, InsRuolo,
  SelCompetenza, Main, Curriculum, uUtilsVarie, Comuni, SchedaCand,
  FaxPresentaz, LegendaRic, SchedaSintetica, CheckPass, SelDiploma, uASACand;

{$R *.DFM}


procedure TSelPersNewForm.ComboBox1Change(Sender: TObject);
var MyList: TStrings;
  i: integer;
  xTrovato: boolean;
begin
  TTabQuery.Close;
  TTabQuery.ParamByName('xDescTabella').asString := ComboBox1.Text;
  TTabQuery.Open;
  //TTabQuery.Filter:='DescTabella='''+ComboBox1.Text+'''';
  BApriForm.Visible := False;
  if TTabQueryTabLookup.AsString <> '' then begin
    if TTabQueryFormLookupID.asString <> '' then begin
      BApriForm.Visible := True;
      xFormID := TTabQueryFormLookupID.Value;
      xIndexName := TTabQueryTabLookupIdx.Value;
    end;
    ComboBox2.Visible := False;
    ComboBox2.Enabled := False;
    ComboBox2.Color := clBtnFace;
    Edit1.Visible := False;
    Edit1.Enabled := False;
    Edit1.Color := clBtnFace;

    TTabLookup.Close;
    if TTabQueryTabLookupIdx.Value <> '' then
      TTabLookup.IndexName := TTabQueryTabLookupIdx.Value
    else TTabLookup.IndexName := '';

    TTabLookup.TableName := 'dbo.' + TTabQueryTabLookup.asString;
    TTabLookup.Fields[1].FieldName := 'Pippo';
    TTabLookup.Fields[1].FieldName := TTabQueryFieldLookup.asString;
    TTabLookup.Fields[1].Size := TTabQueryFieldLookupSize.VAlue;
    DBGrid4.Columns[0].FieldName := TTabQueryFieldLookup.asString;
    DBGrid41.Columns[1].FieldName := TTabQueryFieldLookup.asString;
    TTabLookup.Open;
    DBGrid4.Visible := True;
    DBGrid4.Enabled := True;
    DBGrid4.Color := clWindow;
    DBGrid41.Visible := True;
    DBGrid41.Enabled := True;
    DBGrid41.Color := clWindow;
    xStringa := '';
  end else begin
    DBGrid4.Visible := False;
    DBGrid4.Enabled := False;
    DBGrid4.Color := clBtnFace;
    DBGrid41.Visible := False;
    DBGrid41.Enabled := False;
    DBGrid41.Color := clWindow;
    BApriForm.Visible := False;
    Edit1.Visible := True;
    Edit1.Enabled := true;
    Edit1.Color := clWindow;
  end;
  DBGrid1.Enabled := True;
  DBGrid1.Color := clWindow;
  DBGrid2.Enabled := True;
  DBGrid2.Color := clWindow;
end;

procedure TSelPersNewForm.BitBtn1Click(Sender: TObject);
var xVal, xValDesc, xOp, xStringa, xSubQuery: string;
  xTipoStringa: boolean;
begin
  // prosegui timing
  if xChiamante = 1 then SelPersForm.RiprendiAttivita;
  dxDBGrid1.Visible := False;
  LTot.Visible := False;

  if TTabQueryTipoCampo.asString = 'boolean' then begin
    if ComboBox2.text = 's�' then xVal := '1';
    if ComboBox2.text = 'no' then xVal := '0';
    xValDesc := ComboBox2.text;
  end else begin
    xVal := UpperCase(Edit1.text);
    xValDesc := Edit1.Text;
  end;

  xTipoStringa := False;
  if TTabOpOperatore.asString = 'like' then
    xVal := '''%' + xVal + '%'''
  else
    if not ((TTabQueryTipoCampo.asString = 'boolean') or
      (TTabQueryTipoCampo.asString = 'integer')) then begin
      xVal := '''' + xVal + '''';
      xTipoStringa := True;
    end;

  if TTabQueryTabLookup.AsString <> '' then begin
    xVal := TTabLookupID.asString;
    xValDesc := TTabLookup.Fields[1].asString;
  end;
  if TTabQuerySubQuery.asString <> '' then xTipoStringa := true;

  if xTipoStringa then begin
    if TTabQuerySubQuery.asString <> '' then begin
      xStringa := TTabQuerySubQuery.AsString;
      xStringa := StringReplace(xStringa, ':xOperatore', TTabOpOperatore.asString, [rfReplaceAll]);
      xStringa := StringReplace(xStringa, ':xValore', xVal, [rfReplaceAll]);
      xStringa := 'Anagrafica.ID' + ' ' + xStringa;

      TLinee.InsertRecord([TTabQueryDescCampo.asString + ' ' +
        TTabOpDescOperatore.asString + ' ' +
          xValDesc,
          xStringa,
          TTabQueryTabella.asString,
          'and', 'e',
          MainForm.xIDUtenteAttuale]);
    end else begin

      if TTabQueryTipoCampo.asString = 'date' then begin
        xStringa := TTabQueryTabella.asString + '.' + TTabQueryCampo.asString +
          TTabOpOperatore.asString + ' ' +
          xVal;
        TLinee.InsertRecord([TTabQueryDescCampo.asString + ' ' +
          TTabOpDescOperatore.asString + ' ' +
            xValDesc,
            xStringa,
            TTabQueryTabella.asString,
            'and', 'e',
            MainForm.xIDUtenteAttuale]);
      end else begin
        xstringa := 'UPPER(' + TTabQueryTabella.asString + '.' + TTabQueryCampo.asString + ') ' +
          TTabOpOperatore.asString + ' ' +
          xVal;
        // query specifica per i campi personalizzati
        if TTabQueryTabella.Value = 'AnagCampiPers' then
          xStringa := 'NomeCampo=''' + TTabQueryCampo.Value + ''' and valore ' +
            TTabOpOperatore.asString + '' + xVal + '';

        TLinee.InsertRecord([TTabQueryDescCampo.asString + ' ' +
          TTabOpDescOperatore.asString + ' ' +
            xValDesc,
            xStringa,
            TTabQueryTabella.asString,
            'and', 'e',
            MainForm.xIDUtenteAttuale]);
      end;
    end;
  end else begin

    xStringa := TTabQueryTabella.asString + '.' + TTabQueryCampo.asString + ' ' +
      TTabOpOperatore.asString + ' ' +
      xVal;
    // query specifica per l'et�
    if copy(TTabQueryDescCampo.Value, 1, 3) = 'et�' then begin
      xStringa := 'DATEDIFF(year, Anagrafica.DataNascita, getdate()) ' +
        TTabOpOperatore.asString + ' ' + xVal;
      //if TTabOpOperatore.AsString = '>=' then begin
           //dxDBGrid1.Filter.FilterText := 'eta >= ''' + xVal +'''';
           //dxDBGrid1.Filter.Add(dxDBGrid1Eta, xVal, '');
      //end;
    end;
    // query specifica per i campi personalizzati
    if TTabQueryTabella.Value = 'AnagCampiPers' then

      xStringa := 'NomeCampo=''' + TTabQueryCampo.Value + ''' and valore ' +
        TTabOpOperatore.asString + '' + xVal + '';

    TLinee.InsertRecord([TTabQueryDescCampo.asString + ' ' +
      TTabOpDescOperatore.asString + ' ' +
        xValDesc,
        xStringa,
        TTabQueryTabella.asString,
        'and', 'e',
        MainForm.xIDUtenteAttuale]);
  end;
  TLinee.Close;
  TLinee.Open;
end;

procedure TSelPersNewForm.TTabQueryAfterScroll(DataSet: TDataSet);
var MyList: TStrings;
  i: integer;
  xTrovato: boolean;
begin
  xFormID := 0;
  BApriForm.Visible := False;
  if (TTabQueryTipoCampo.asString = 'boolean') and
    (TTabQueryTabLookup.AsString = '') then begin
    ComboBox2.Visible := True;
    ComboBox2.Enabled := True;
    ComboBox2.Color := clWindow;
    DBGrid4.Visible := False;
    DBGrid41.Visible := False;
    BApriForm.Visible := False;
    Edit1.Visible := False;
    Edit1.Enabled := False;
    Edit1.Color := clBtnFace;
  end else begin
    if TTabQueryTabLookup.AsString <> '' then begin
      if TTabQueryFormLookupID.asString <> '' then begin
        BApriForm.Visible := True;
        xFormID := TTabQueryFormLookupID.Value;
        xIndexName := TTabQueryTabLookupIdx.Value;
      end;
      ComboBox2.Visible := False;
      ComboBox2.Enabled := False;
      ComboBox2.Color := clBtnFace;
      Edit1.Visible := False;
      Edit1.Enabled := False;
      Edit1.Color := clBtnFace;

      TTabLookup.Close;
      TTabLookup.TableName := 'dbo.' + TTabQueryTabLookup.asString;
      if TTabQueryTabLookupIdx.Value <> '' then
        TTabLookup.IndexName := TTabQueryTabLookupIdx.Value
      else TTabLookup.IndexName := '';

      TTabLookup.Fields[1].FieldName := 'Pippo';
      TTabLookup.Fields[1].FieldName := TTabQueryFieldLookup.asString;
      TTabLookup.Fields[1].Size := TTabQueryFieldLookupSize.VAlue;
      DBGrid4.Columns[0].FieldName := TTabQueryFieldLookup.asString;
      DBGrid41.Columns[1].FieldName := TTabQueryFieldLookup.asString;
      TTabLookup.Open;
      DBGrid4.Visible := True;
      DBGrid4.Enabled := True;
      xStringa := '';
      DBGrid4.Color := clWindow;
      DBGrid41.Visible := True;
      DBGrid41.Enabled := True;
      DBGrid41.Color := clWindow;
    end else begin
      DBGrid4.Visible := False;
      DBGrid4.Enabled := False;
      DBGrid4.Color := clBtnFace;
      DBGrid41.Visible := False;
      DBGrid41.Enabled := False;
      DBGrid41.Color := clBtnFace;
      BApriForm.Visible := False;
      Edit1.Visible := True;
      Edit1.Enabled := true;
      Edit1.Color := clWindow;
    end;

    ComboBox2.Visible := False;
    ComboBox2.Enabled := False;
    ComboBox2.Color := clBtnFace;
    {        Edit1.Visible:=True;
            Edit1.Enabled:=true;
            Edit1.Color:=clWindow;
            Edit1.text:='';
    }
  end;
end;

procedure TSelPersNewForm.BitBtn3Click(Sender: TObject);
begin
  // prosegui timing
  if xChiamante = 1 then SelPersForm.RiprendiAttivita;
  dxDBGrid1.Visible := False;
  LTot.Visible := False;
  TLinee.Delete;
  TLinee.Close;
  TLinee.Open;
end;

procedure TSelPersNewForm.FormShow(Sender: TObject);
var i: integer;
begin
  xOrdine := 'order by Anagrafica.Cognome,Anagrafica.Nome';
  Edit1.Enabled := False;
  Edit1.Color := clBtnFace;
  TLinee.ParamByName('xIDAnag').asInteger := MainForm.xIDUtenteAttuale;
  TLinee.Open;
  // se viene da Ricerca -> caricamento criteri salvati
  if xChiamante = 1 then begin
    Query1.SQL.Clear;
    Query1.SQL.Add('delete from UserLineeQuery where IDAnagrafica=' + IntToStr(MainForm.xIDUtenteAttuale));
    Query1.ExecSQL;
    TLinee.Close;
    TLinee.Open;
    QRicLineeQuery.Close;
    QRicLineeQuery.Prepare;
    QRicLineeQuery.Params[0].asInteger := DataRicerche.TRicerchePendID.Value;
    QRicLineeQuery.Open;
    while not QRicLineeQuery.EOF do begin
      TLinee.InsertRecord([QRicLineeQueryDescrizione.Value,
        QRicLineeQueryStringa.Value,
          QRicLineeQueryTabella.Value,
          QRicLineeQueryOpSucc.Value,
          QRicLineeQueryNext.Value,
          MainForm.xIDUtenteAttuale]);
      QRicLineeQuery.Next;
    end;
    TLinee.Close;
    TLinee.Open;
  end;
  // INFORMATION RETRIEVAL
  // -- 1) verifica corretta installazione componente full-text
  xIsFulltextInstalled := MainForm.xIsFulltextInstalled;
  if not xIsFulltextInstalled then begin
    PanInfoRet.Visible := False;
  end else begin
    // -- 3) verifica esistenza indice full-text sulla tabella AnagAltreInfo
    Q.Close;
    Q.SQL.text := 'select OBJECTPROPERTY ( object_id(''AnagAltreInfo''),''TableHasActiveFulltextIndex'') AnagAltreInfoOK';
    Q.Open;
    xAnagAltreInfoOK := Q.FieldByName('AnagAltreInfoOK').asInteger = 1;
    // -- VERIFICA SERVIZIO DI INDICIZZAZIONE a livello di File-System
    Q.Close;
    Q.SQL.text := 'select count(*) FileSystemOK from master.dbo.sysservers where srvname=''FileSystem''';
    Q.Open;
    xFileSystemOK := Q.FieldByName('FileSystemOK').asInteger = 1;
    // abilitazione checkbox
    CBIRnote.Enabled := xAnagAltreInfoOK;
    CBIRnote.Checked := xAnagAltreInfoOK;
    CBIRFileSystem.Enabled := xFileSystemOK;
    CBIRFileSystem.Checked := xFileSystemOK;
  end;


  // parametri sulle date da GLOBAL
  Q.Close;
  Q.SQl.text := 'select ggDiffDi,ggDiffUc,ggDiffCol,CheckRicCandCV from global ';
  Q.Open;
  xggDiffDi := Q.FieldByname('ggDiffDi').asInteger;
  xggDiffUc := Q.FieldByname('ggDiffUc').asInteger;
  xggDiffCol := Q.FieldByname('ggDiffCol').asInteger;
  xCheckRicCandCV := Q.FieldByname('CheckRicCandCV').asBoolean;
  Q.Close;
  // personalizzazione Ergon:
  if Uppercase(copy(Data.GlobalNomeAzienda.Value, 1, 5)) = 'ERGON' then begin
    BitBtn10.caption := 'scheda cand.';
    BitBtn10.Font.Style := [];
    BCurriculum.Visible := False;
  end;
  if Uppercase(copy(Data.GlobalNomeAzienda.Value, 1, 6)) = 'BOYDEN' then
    dxDBGrid1DataRealeCV.DisableCustomizing := True;

  if Uppercase(copy(Data.GlobalNomeAzienda.Value, 1, 7)) = 'FERRARI' then begin
    CBPropri.Checked := True;
    PMStati5.Checked := True;
  end;
  if Uppercase(copy(Data.GlobalNomeAzienda.Value, 1, 6)) = 'ADVANT' then begin
    BCurriculum.enabled := False;
    BitBtn8.enabled := False;
    CBIRaltreRegole.Checked := true;
    CBIRnote.Checked := false;
    CBIRnote.Enabled := false;
    // CBIRFileSystem.Checked := false;
  end;
  Caption := '[M/5] - ' + Caption;
end;

procedure TSelPersNewForm.BitBtn6Click(Sender: TObject);
begin
  // prosegui timing
  if xChiamante = 1 then SelPersForm.RiprendiAttivita;
  dxDBGrid1.Visible := False;
  LTot.Visible := False;
  Query1.Close;
  Query1.SQl.Clear;
  Query1.SQl.add('delete from UserLineeQuery where IDAnagrafica=' + IntToStr(MainForm.xIDUtenteAttuale));
  Query1.ExecSQL;
  TTabQuery.Close;
  TTabQuery.ParamByName('xDescTabella').asString := ComboBox1.Text;
  TTabQuery.Open;
  TLinee.Close;
  TLinee.Open;
end;

procedure TSelPersNewForm.BitBtn2Click(Sender: TObject);
var xTab: array[1..5] of string;
  i, j, k: integer;
  xS, xS1, xd, xLineeStringa, xStringaSQL: string;
  xApertaPar, xEspLav, xIRNote, xIRFileSystem: boolean;
begin
  // prosegui timing
  if xChiamante = 1 then SelPersForm.RiprendiAttivita;

  if DsLinee.State in [dsInsert, dsEdit] then begin
    TLinee.Post;
    TLinee.Close;
    TLinee.Open;
  end;
  TLinee.Last;
  if TLineeOpSucc.Value <> '' then begin
    TLinee.Edit;
    TLineeOpNext.Value := 'fine';
    TLinee.Post;
  end;
  TLinee.Close;
  TLinee.Open;
  // tabelle implicate tranne Anagrafica
  QTabelleLista.Close;
  QTabelleLista.SQL.clear;
  QTabelleLista.SQL.add('select distinct Tabella from UserLineeQuery ');
  QTabelleLista.SQL.add('where Tabella<>''Anagrafica'' and IDAnagrafica=' + IntToStr(MAinForm.xIDUtenteAttuale));
  QTabelleLista.Open;
  QTabelleLista.First;
  for i := 1 to 5 do xTab[i] := '';
  i := 1;
  while not QTabelleLista.EOF do begin
    xTab[i] := QTabelleListaTabella.AsString;
    QTabelleLista.Next; inc(i);
  end;

  xEspLav := False;
  xS := 'from Anagrafica,AnagAltreInfo';
  k := 1;
  while k <= 5 do begin
    //if xTab[i]='EsperienzeLavorative' then xEspLav:=True;
    //if (xTab[i]<>'')and(xTab[i]<>'EsperienzeLavorative') then begin
    if (xTab[k] <> '') and (UpperCase(xTab[k]) <> 'ANAGALTREINFO') then
      xS := xs + ',' + xTab[k];
    if xTab[k] = 'AnagCampiPers' then
      xS := xS + ',CampiPers';
    inc(k);
  end;

  QRes1.Close;
  QRes1.SQL.Clear;

  //QRes1.SQL.Add('SET DATEFORMAT dmy');
  QRes1.SQL.Add('select distinct Anagrafica.ID,Anagrafica.Cognome,Anagrafica.Nome,Anagrafica.CVNumero,Anagrafica.OldCVNumero,');
  QRes1.SQL.Add('                Anagrafica.TelUfficio,Anagrafica.Cellulare,CVInseritoInData,CVDataReale,CVDataAgg,RecapitiTelefonici,');
  QRes1.SQL.Add('                Anagrafica.DataNascita,Anagrafica.IDStato,Anagrafica.IDTipoStato,Anagrafica.DomicilioComune,');
  QRes1.SQL.Add('                DATEDIFF(day, EBC_CandidatiRicerche.DataIns, getdate()) ggUltimaDataIns,');
  QRes1.SQL.Add('                DATEDIFF(day, EBC_ContattiCandidati.Data, getdate()) ggDataUltimoContatto,');
  QRes1.SQL.Add('                EBC_CandidatiRicerche.DataImpegno DataProssimoColloquio,');
  QRes1.SQL.Add('                DATEDIFF(day, EBC_Colloqui.Data, getdate()) ggDataUltimoColloquio,');
  QRes1.SQL.Add('                DATEDIFF(day, Anagrafica.CVInseritoInData, getdate()) ggDataInsCV, NoCv, EBC_Clienti.Descrizione as ContattoCliente');
  // colonne visibili o meno a seconda del cliente
  if Uppercase(copy(Data.GlobalNomeAzienda.Value, 1, 5)) = 'ERGON' then begin
    dxDBGrid1CVNumero.FieldName := 'OldCVNumero';
    dxDBGrid1CVNumero.Caption := 'Old n�CV';
    dxDBGrid1TipoStato.Visible := False;
    dxDBGrid1DomicilioComune.Visible := False;
  end;
  //if Uppercase(copy(Data.GlobalNomeAzienda.Value,1,6))='BOYDEN' then begin
  //     QRes1.SQL.Add(',NoCV');
  //end;
  QRes1.SQL.Add(xS + ',EBC_CandidatiRicerche,EBC_ContattiCandidati,EBC_Colloqui,EBC_Clienti ');
  QRes1.SQL.Add('where Anagrafica.ID is not null and Anagrafica.ID=AnagAltreInfo.IDAnagrafica');
  QRes1.SQL.Add(' and anagrafica.idazienda *= ebc_clienti.id ');
  if not xEspLav then begin
    //QRes1.SQL.Add('and Anagrafica.ID *= EsperienzeLavorative.IDAnagrafica');
    //QRes1.SQL.Add('and EsperienzeLavorative.ID = (select max(ID) from EsperienzeLavorative');
    //QRes1.SQL.Add('                               where Anagrafica.ID *= EsperienzeLavorative.IDAnagrafica)');
  end else begin
    // *** Questo codice non viene pi� considerato (vedi riga 458 e 459 commentate)
    QRes1.SQL.Add('and Anagrafica.ID = EsperienzeLavorative.IDAnagrafica');
    QRes1.SQL.Add('and EsperienzeLavorative.ID = (select max(ID) from EsperienzeLavorative');
    QRes1.SQL.Add('                               where Anagrafica.ID = EsperienzeLavorative.IDAnagrafica and');
    // oondizioni su EspLav
    TLinee.First;
    xLineeStringa := '';
    xApertaPar := False;
    while not TLinee.EOF do begin
      //if TLinee
      xLineeStringa := TLineeStringa.asString;
      if copy(TLineeStringa.Value, 1, 13) = 'EsperienzeLav' then begin
        TLinee.Next;
        if TLinee.EOF then
          // ultimo record -> senza AND alla fine
          if xApertaPar then QRes1.SQL.Add(' ' + xLineeStringa + ')')
          else QRes1.SQL.Add(' ' + xLineeStringa)
        else begin
          TLinee.Prior;
          if TLineeOpSucc.AsString = 'and' then
            if not xApertaPar then
              QRes1.SQL.Add(' ' + xLineeStringa + ' and ')
            else begin
              QRes1.SQL.Add(' ' + xLineeStringa + ') and ');
              xApertaPar := False;
            end;
          if TLineeOpSucc.AsString = 'or' then
            if not xApertaPar then begin
              QRes1.SQL.Add(' (' + xLineeStringa + ' or ');
              xApertaPar := True;
            end else begin
              QRes1.SQL.Add(' ' + xLineeStringa + ' or ');
              xApertaPar := True;
            end;
        end;
      end;
      TLinee.Next;
    end;
    // verifica ultime tre lettere sono "and" o "or" --> allora toglile
    xd := QRes1.SQL[QRes1.SQL.count - 1];
    if copy(xd, Length(xd) - 3, 3) = 'and' then
      QRes1.SQL[QRes1.SQL.count - 1] := copy(xd, 1, Length(xd) - 4);
    if copy(xd, Length(xd) - 2, 2) = 'or' then
      QRes1.SQL[QRes1.SQL.count - 1] := copy(xd, 1, Length(xd) - 3);
    QRes1.SQL.Add(')');
  end;

  // anzianit� CV
  if CBAnzCV.Checked then
    QRes1.SQL.Add('and Anagrafica.CVInseritoInData>=:xCVInseritoInData');

  // togliere gli esclusi (cio� gi� presenti in ricerca)
  if xChiamante = 1 then begin
    for j := 1 to DataRicerche.QCandRic.RecordCount do
      QRes1.SQL.Add('and Anagrafica.ID<>' + IntToStr(xDaEscludere[j]));
  end;

  xS1 := '';
  for i := 1 to 5 do begin
    //if (xTab[i]<>'')and(xTab[i]<>'EsperienzeLavorative') then
    if xTab[i] <> '' then
      if xTab[i] = '' then
        xS1 := xS1 + ' and Anagrafica.ID=' + xTab[i] + '.IDAnagrafica and CampiPers.ID = AnagCampiPers.IDCampo'
      else xS1 := xS1 + ' and Anagrafica.ID=' + xTab[i] + '.IDAnagrafica';
  end;
  QRes1.SQL.Add(xS1);
  if TLinee.RecordCount > 0 then
    QRes1.SQL.Add('and');

  TLinee.First;
  xLineeStringa := '';
  xApertaPar := False;
  while not TLinee.EOF do begin
    xLineeStringa := TLineeStringa.asString;
    // per le LINGUE --> apposita clausola
    if copy(TLineeDescrizione.Value, 1, 6) = 'lingua' then begin
      // ## OLD ## xLineeStringa:='Anagrafica.ID in (select IDAnagrafica from LingueConosciute where lingua='''+copy(TLineeDescrizione.Value,9,Length(TLineeDescrizione.Value))+''')';
      xLineeStringa := 'Anagrafica.ID in (select IDAnagrafica from LingueConosciute where ' + TLineeStringa.Value;
      if TrimRight(TLineeLivLinguaParlato.asString) <> '' then
        xLineeStringa := xLineeStringa + ' and livello>=' + TrimRight(TLineeLivLinguaParlato.asString);
      if TrimRight(TLineeLivLinguaScritto.asString) <> '' then
        xLineeStringa := xLineeStringa + ' and livelloScritto>=' + TrimRight(TLineeLivLinguaScritto.asString);
      xLineeStringa := xLineeStringa + ')';
    end;
    TLinee.Next;
    if TLinee.Eof then
      // ultimo record -> senza AND alla fine
      if xApertaPar then QRes1.SQL.Add(' ' + xLineeStringa + ')')
      else QRes1.SQL.Add(' ' + xLineeStringa)
    else begin
      TLinee.Prior;
      if TLineeOpSucc.AsString = 'and' then
        if not xApertaPar then
          QRes1.SQL.Add(' ' + xLineeStringa + ' and ')
        else begin
          QRes1.SQL.Add(' ' + xLineeStringa + ') and ');
          xApertaPar := False;
        end;
      if TLineeOpSucc.AsString = 'or' then
        if not xApertaPar then begin
          QRes1.SQL.Add(' (' + xLineeStringa + ' or ');
          xApertaPar := True;
        end else begin
          QRes1.SQL.Add(' ' + xLineeStringa + ' or ');
          xApertaPar := True;
        end;
    end;
    TLinee.Next;
  end;

  // data ultimo inserimento in una ricerca
  QRes1.SQL.Add('and  Anagrafica.ID *= EBC_CandidatiRicerche.IDAnagrafica');
  QRes1.SQL.Add('     and EBC_CandidatiRicerche.DataIns =');
  QRes1.SQL.Add('     (select max(DataIns) from EBC_CandidatiRicerche');
  QRes1.SQL.Add('      where Anagrafica.ID *= EBC_CandidatiRicerche.IDAnagrafica)');
  // data ultimo contatto con il soggetto
  QRes1.SQL.Add('and Anagrafica.ID *= EBC_ContattiCandidati.IDAnagrafica');
  QRes1.SQL.Add('    and EBC_ContattiCandidati.Data=');
  QRes1.SQL.Add('    (select max(Data) from EBC_ContattiCandidati');
  QRes1.SQL.Add('     where Anagrafica.ID *= EBC_ContattiCandidati.IDAnagrafica)');
  // data ultimo colloquio
  QRes1.SQL.Add('and Anagrafica.ID *= EBC_Colloqui.IDAnagrafica');
  QRes1.SQL.Add('and EBC_Colloqui.Data=');
  QRes1.SQL.Add('    (select max(Data) from EBC_Colloqui');
  QRes1.SQL.Add('     where Anagrafica.ID *= EBC_Colloqui.IDAnagrafica)');

  // stati secondo la selezione
  if not PMStati1.Checked then
    QRes1.SQL.Add('and Anagrafica.IDTipoStato<>1');
  if not PMStati2.Checked then
    QRes1.SQL.Add('and Anagrafica.IDTipoStato<>2');
  if not PMStati3.Checked then
    QRes1.SQL.Add('and Anagrafica.IDTipoStato<>3');
  if not PMStati4.Checked then
    QRes1.SQL.Add('and Anagrafica.IDTipoStato<>5');
  if not PMStati5.Checked then
    QRes1.SQL.Add('and Anagrafica.IDTipoStato<>9');

  if CBColloquiati.Checked then
    QRes1.SQL.Add('and Anagrafica.ID in (select IDAnagrafica from EBC_Colloqui)');

  if not CBPropri.Checked then
    QRes1.SQL.Add('and (Anagrafica.IDProprietaCV is null or Anagrafica.IDProprietaCV=0)');


  xStringaSQL := QRes1.SQL.Text;
  QRes1.SQL.Clear;
  QRes1.SQL.Add('SET DATEFORMAT dmy');
  QRes1.SQL.Add(xStringaSQL);


  //INFORMATON RETRIEVAL
  if EIRNote.text <> '' then begin
    // SU FILE SYSTEM
    // sapere se � abilitata sul CAMPO NOTE (AnagAltreInfo.Note)
    if (xIsFulltextInstalled) and (xAnagAltreInfoOK) and (CBIRnote.Checked) then
      xIRNote := True
    else xIRNote := False;
    // sotto-query file-system
    if (xIsFulltextInstalled) and (xFileSystemOK) and (CBIRFileSystem.Checked) then begin
      xIRFileSystem := True;
      Q.Close;
      Q.SQL.text := 'select IRFileSystemScope from global';
      Q.Open;
      xIRFileSystemScope := Q.FieldByName('IRFileSystemScope').asString;
      // sotto-query AnagFile
      QRes1.SQL.Add('and (Anagrafica.ID in ( SELECT distinct IDAnagrafica FROM ' +
        'OPENQUERY(FileSystem, ''SELECT FileName,Characterization,DocAuthor,DocComments,DocSubject,DocTitle ' +
        ' FROM SCOPE ('''' "' + xIRFileSystemScope + '" '''') ' +
        ' WHERE CONTAINS( Contents, ''''' + EIRNote.text + ''''' ) '') AS Q, AnagFile ' +
        ' WHERE Q.FileName = AnagFile.SoloNome COLLATE SQL_Latin1_General_CP1_CI_AS ');
      // altre regole
      if CBIRaltreRegole.Checked then begin
        Q.Close;
        Q.SQL.text := 'select Regola from IR_LinkRules';
        Q.Open;
        while not Q.EOF do begin
          QRes1.SQL.Add('UNION');
          QRes1.SQL.Add('SELECT distinct Anagrafica.ID IDAnagrafica FROM ' +
            'OPENQUERY(FileSystem, ''SELECT FileName,Characterization,DocAuthor,DocComments,DocSubject,DocTitle ' +
            ' FROM SCOPE ('''' "' + xIRFileSystemScope + '" '''') ' +
            ' WHERE CONTAINS( Contents, ''''' + EIRNote.text + ''''' ) '') AS Q, Anagrafica ' +
            ' WHERE ' + Q.FieldByName('Regola').asString);
          Q.Next;
        end;
      end;
      QRes1.SQL.Add(')');
      Q.Close;
      QRes1.SQL.Add(')');

    end else xIRFileSystem := False;

    // INFORMATION RETRIEVAL SUL CAMPO NOTE (AnagAltreInfo.Note)
    if xIRNote then begin
      if not xIRFileSystem then begin
        QRes1.SQL.Add('and CONTAINS(AnagAltreInfo.Note, ''' + EIRNote.text + ''') ');
      end else begin
        QRes1.SQL.Add('UNION');
        QRes1.SQL.add(xStringaSQL); // ancora tutta la stringa
        QRes1.SQL.Add('and CONTAINS(AnagAltreInfo.Note, ''' + EIRNote.text + ''') ');
      end;
    end;
  end;

  QRes1.SQL.Add(xOrdine);

  // anzianit� CV
  if CBAnzCV.Checked then
    QRes1.ParamByName('xCVInseritoInData').asDateTime := Date - (SEAnzCV.Value * 30);

  ScriviRegistry('QRes1', QRes1.SQL.text);

  //QRes1.ExecSQL;
  //if EIRNote.Text <> '' then begin
    QRes1Ado.Close;
    dsQres1.DataSet := qRes1Ado;
    ////////QRes1Ado.SQL.Text := QRes1.SQL.text;
    QRes1Ado.CommandText := QRes1.SQL.text;
    QRes1Ado.Open;
    LTot.Caption := IntToStr(QRes1Ado.RecordCount);
  //end else begin

    //dsQres1.DataSet := qRes1;
    //QRes1.Open;
    //LTot.Caption := IntToStr(QRes1.RecordCount);
  //end;
  TLinee.Close;
  TLinee.Open;

  dxDBGrid1.Visible := True;
  //LTot.Caption := IntToStr(QRes1.RecordCount);
  LTot.Visible := True;
end;

procedure TSelPersNewForm.DBGrid4KeyPress(Sender: TObject; var Key: Char);
begin
  if TTabLookup.IndexName <> '' then begin
    if Key <> chr(13) then begin
      xStringa := xStringa + key;
      TTabLookup.FindNearest([xStringa]);
    end;
  end;
end;

procedure TSelPersNewForm.BitBtn8Click(Sender: TObject);
var xFile: string;
  xProcedi: boolean;
begin
  // prosegui timing
  if xChiamante = 1 then SelPersForm.RiprendiAttivita;
  if not OkIDAnag(QRes1ID.Value) then exit;
  ApriCV(QRes1ID.Value);
  // prosegui timing
  if xChiamante = 1 then SelPersForm.RiprendiAttivita;
end;

procedure TSelPersNewForm.BAggiungiRicClick(Sender: TObject);
var i: integer;
  xMiniVal, xRic: string;
  xVai: boolean;
  SavePlace: TBookmark;
  xIDEvento, xIDClienteBlocco: integer;
  xLivProtez, xDicMess, xPassword, xUtenteResp, xClienteBlocco: string;
  QLocal: TQuery;
begin
  if not Mainform.CheckProfile('210') then Exit;
  if not OkIDAnag(QRes1ID.Value) then exit;
  if QRes1IDTipoStato.Value = 3 then
    if MessageDlg('Il candidato risulta INSERITO in azienda. Continuare?', mtWarning, [mbYes, mbNo], 0) = mrNo then exit;
  xMiniVal := '';
  if not InputQuery('Inserimento nella ricerca', 'Valutazione (1 lettera):', xMiniVal) then exit;
  Q.Close;
  Q.SQL.Text := 'select ID from EBC_CandidatiRicerche where IDRicerca=' + Dataricerche.TRicerchePendID.asString + ' and IDAnagrafica=' + QRes1ID.asString;
  Q.Open;
  if Q.IsEmpty then begin
    xVai := True;
    // controllo se � in selezione
    if QRes1IDTipoStato.Value = 1 then begin
      if MessageDlg('Il soggetto risulta IN SELEZIONE: ' + chr(13) + xRic + chr(13) + 'PROCEDERE COMUNQUE ?', mtWarning, [mbYes, mbNo], 0) = mrNo then xVai := False;
    end;
    // controllo se � gi� associato ad un'altra ricerca per la STESSA azienda
    Q.Close; Q.SQL.clear;
    Q.SQl.Add('select EBC_Ricerche.Progressivo,EBC_StatiRic.StatoRic from EBC_CandidatiRicerche,EBC_Ricerche,EBC_StatiRic ');
    Q.SQl.Add('where EBC_CandidatiRicerche.IDRicerca=EBC_Ricerche.ID and EBC_Ricerche.IDStatoRic=EBC_StatiRic.ID');
    Q.SQl.Add('and EBC_CandidatiRicerche.IDAnagrafica=:xIDAnag and EBC_Ricerche.IDCliente=:xIDCliente');
    Q.Prepare;
    Q.ParamByName('xIDAnag').asInteger := QRes1ID.value;
    Q.ParamByName('xIDCliente').asInteger := DataRicerche.TRicerchePendIDCliente.Value;
    Q.Open;
    if not Q.IsEmpty then begin
      xRic := '';
      while not Q.EOF do begin xRic := xRic + 'N�' + Q.FieldByName('Progressivo').asString + ' (' + Q.FieldByName('StatoRic').asString + ') '; Q.Next; end;
      if MessageDlg('Il soggetto � associato gi� alle seguenti ricerche per lo stesso cliente: ' + chr(13) + xRic + chr(13) + 'PROCEDERE COMUNQUE ?', mtWarning, [mbYes, mbNo], 0) = mrNo then xVai := False;
    end;
    // controllo incompatibilit� (per questa azienda e per le aziende a questa correlate)
    if IncompAnagCli(QRes1ID.value, DataRicerche.TRicerchePendIDCliente.Value) then
      if MessageDlg('Il soggetto risulta INCOMPATIBILE con questo cliente o con un cliente associato' + chr(13) +
        'PROCEDERE COMUNQUE ?', mtWarning, [mbYes, mbNo], 0) = mrNo then xVai := False;
    // controllo appartenenza CV
    Q.Close;
    Q.SQL.Text := 'select IDProprietaCV from Anagrafica where ID=' + QRes1ID.asString;
    Q.Open;
    if (not ((Q.FieldByName('IDProprietaCV').asString = '') or (Q.FieldByName('IDProprietaCV').asInteger = 0))) and
      (Q.FieldByName('IDProprietaCV').asInteger <> DataRicerche.TRicerchePendIDCliente.value) then
      if MessageDlg('ATTENZIONE: il CV risulta di propriet� di un altro cliente.' + chr(13) +
        'Vuoi proseguire lo stesso ?', mtWarning, [mbYes, mbNo], 0) = mrNo then
        xVai := False;
    // controllo blocco livello 1 o 2
    xIDClienteBlocco := CheckAnagInseritoBlocco1(QRes1ID.Value);
    if xIDClienteBlocco > 0 then begin
      xLivProtez := copy(IntToStr(xIDClienteBlocco), 1, 1);
      if xLivProtez = '1' then begin
        xIDClienteBlocco := xIDClienteBlocco - 10000;
        xClienteBlocco := GetDescCliente(xIDClienteBlocco);
        xDicMess := 'Stai inserendo in commessa un candidato appartenente ad una azienda (' + xClienteBlocco + ') con blocco di livello 1, ';
      end else begin
        xIDClienteBlocco := xIDClienteBlocco - 20000;
        xClienteBlocco := GetDescCliente(xIDClienteBlocco);
        xDicMess := 'Stai inserendo in commessa un candidato appartenente ad una azienda ATTIVA (' + xClienteBlocco + '), ';
      end;
      if MessageDlg('ATTENZIONE!! PROTEZIONE CLIENTI LIVELLO ' + xLivProtez + chr(13) + xDicMess +
        'protetta dalle normative interne, se sei autorizzato premi SI, altrimenti premi NO. ' +
        'Se si prosegue, l''operazione verr� registrata nello storico del soggetto come forzatura. ' +
        'Verr� inviato un messaggio di promemoria al responsabile diretto', mtWarning, [mbYes, mbNo], 0) = mrNo then xVai := False
      else begin
        // pwd
        xUtenteResp := GetDescUtenteResp(MainForm.xIDUtenteAttuale);
        if not InputQuery('Forzatura blocco livello ' + xLivProtez, 'Password di ' + xUtenteResp, xPassword) then exit;
        if not CheckPassword(xPassword, GetPwdUtenteResp(MainForm.xIDUtenteAttuale)) then begin
          MessageDlg('Password errata', mtError, [mbOK], 0);
          xVai := False;
        end;
        if xVai then begin
          // promemoria al resp.
          QLocal := CreateQueryFromString('insert into Promemoria (IDUtente,IDUtenteDa,DataIns,Testo,Evaso,DataDaLeggere) ' +
            'values (:xIDUtente,:xIDUtenteDa,:xDataIns,:xTesto,:xEvaso,:xDataDaLeggere)');
          QLocal.ParamByName('xIDUtente').asInteger := GetIDUtenteResp(MainForm.xIDUtenteAttuale);
          QLocal.ParamByName('xIDUtenteDa').asInteger := MainForm.xIDUtenteAttuale;
          QLocal.ParamByName('xDataIns').asDateTime := Date;
          QLocal.ParamByName('xTesto').asString := 'forzatura livello ' + xLivProtez + ' per CV n� ' + QRes1CVNumero.AsString;
          QLocal.ParamByName('xEvaso').asBoolean := False;
          QLocal.ParamByName('xDataDaLeggere').asDateTime := Date;
          QLocal.ExecSQL;
          // registra nello storico soggetto
          xIDEvento := GetIDEvento('forzatura Blocco livello ' + xLivProtez);
          if xIDEvento > 0 then begin
            with Data.Q1 do begin
              close;
              SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
                'values (:xIDAnagrafica,:xIDEvento,:xDataEvento,:xAnnotazioni,:xIDRicerca,:xIDUtente,:xIDCliente)';
              ParamByName('xIDAnagrafica').asInteger := QRes1ID.Value;
              ParamByName('xIDEvento').asInteger := xIDevento;
              ParamByName('xDataEvento').asDateTime := Date;
              ParamByName('xAnnotazioni').asString := 'cliente: ' + xClienteBlocco;
              ParamByName('xIDRicerca').asInteger := DataRicerche.TRicerchePendID.Value;
              ParamByName('xIDUtente').asInteger := MainForm.xIDUtenteAttuale;
              ParamByName('xIDCliente').asInteger := xIDClienteBlocco;
              ExecSQL;
            end;
          end;
        end;
      end;
    end;

    if xVai then begin
      Data.DB.BeginTrans;
      try
        if Q.Active then Q.Close;
        Q.SQL.text := 'insert into EBC_CandidatiRicerche (IDRicerca,IDAnagrafica,Escluso,Stato,DataIns,Minival) ' +
          'values (:xIDRicerca,:xIDAnagrafica,:xEscluso,:xStato,:xDataIns,:xMinival)';
        Q.ParamByName('xIDRicerca').asInteger := DataRicerche.TRicerchePendID.Value;
        Q.ParamByName('xIDAnagrafica').asInteger := QRes1ID.value;
        Q.ParamByName('xEscluso').asBoolean := False;
        Q.ParamByName('xStato').asString := 'inserito da ricerca nomi';
        Q.ParamByName('xDataIns').asDateTime := Date;
        Q.ParamByName('xMiniVal').asString := xMiniVal;
        Q.ExecSQL;
        // aggiornamento stato anagrafica
        if QRes1IDStato.Value <> 27 then begin
          if Q.Active then Q.Close;
          Q.SQL.text := 'update Anagrafica set IDStato=27,IDTipoStato=1 where ID=' + QRes1ID.asString;
          Q.ExecSQL;
        end;
        // aggiornamento storico
        if Q.Active then Q.Close;
        Q.SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
          'values (:xIDAnagrafica,:xIDEvento,:xDataEvento,:xAnnotazioni,:xIDRicerca,:xIDUtente,:xIDCliente)';
        Q.ParamByName('xIDAnagrafica').asInteger := QRes1ID.value;
        Q.ParamByName('xIDEvento').asInteger := 19;
        Q.ParamByName('xDataEvento').asDateTime := date;
        Q.ParamByName('xAnnotazioni').asString := 'ric.n� ' + DataRicerche.TRicerchePendProgressivo.Value + ' (' + DataRicerche.TRicerchePendCliente.Value + ')';
        Q.ParamByName('xIDRicerca').asInteger := DataRicerche.TRicerchePendID.Value;
        Q.ParamByName('xIDUtente').asInteger := DataRicerche.TRicerchePendIDUtente.Value;
        Q.ParamByName('xIDCliente').asInteger := DataRicerche.TRicerchePendIDCliente.Value;
        Q.ExecSQL;
        Data.DB.CommitTrans;
        //DataRicerche.QCandRic.Next;
        //SavePlace:=DataRicerche.QCandRic.GetBookmark;
        DataRicerche.QCandRic.Close;
        DataRicerche.QCandRic.Open;
        //DataRicerche.QCandRic.GotoBookmark(SavePlace);
        //DataRicerche.QCandRic.FreeBookmark(SavePlace);
  // metti anche questo in quelli da escludere per le prossime ricerche
        for i := 1 to 500 do if xDaEscludere[i] = 0 then break;
        xDaEscludere[i] := QRes1ID.value;
        BitBtn2Click(self);
      except
        Data.DB.RollbackTrans;
        MessageDlg('Errore sul database:  inserimento non avvenuto', mtError, [mbOK], 0);
      end;
    end;
  end;
  // prosegui timing
  if xChiamante = 1 then SelPersForm.RiprendiAttivita;
end;

procedure TSelPersNewForm.TLineeBeforePost(DataSet: TDataSet);
begin
  if TLineeOpNext.Value = 'e' then TLineeOpSucc.Value := 'and';
  if TLineeOpNext.Value = 'o' then TLineeOpSucc.Value := 'or';
  if TLineeOpNext.Value = 'fine' then TLineeOpSucc.Value := '';
end;

procedure TSelPersNewForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if xChiamante = 1 then begin
    // cancellazione vecchie linee query
    Query1.DatabaseName := 'EBCDB';
    Query1.SQL.Clear;
    Query1.SQL.Add('delete from RicLineeQuery where IDRicerca=' + DataRicerche.TRicerchePendID.asString);
    Query1.ExecSQL;
    TLinee.First;
    // salvataggio linee query
    TRicLineeQuery.Open;
    while not TLinee.EOF do begin
      Data.DB.BeginTrans;
      try
        Q.Close;
        Q.SQL.text := 'Insert Into RicLineeQuery (IDRicerca,Descrizione,Stringa,Tabella,OpSucc,Next) ' +
          'values (:xIDRicerca,:xDescrizione,:xStringa,:xTabella,:xOpSucc,:xNext)';
        Q.ParambyName('xIDRicerca').asInteger := DataRicerche.TRicerchePendID.Value;
        Q.ParambyName('xDescrizione').asString := TLineeDescrizione.Value;
        Q.ParambyName('xStringa').asString := TLineeStringa.Value;
        Q.ParambyName('xTabella').asString := TLineeTabella.Value;
        Q.ParambyName('xOpSucc').asString := TLineeOpSucc.Value;
        Q.ParambyName('xNext').asString := TLineeOpNext.Value;
        Q.ExecSQL;
        Data.DB.CommitTrans;
      except
        Data.DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE:  operazione non completata', mtError, [mbOK], 0);
      end;
      TLinee.Next;
    end;
    TRicLineeQuery.Close;
  end;
end;

procedure TSelPersNewForm.BApriFormClick(Sender: TObject);
begin
  // prosegui timing
  if xChiamante = 1 then SelPersForm.RiprendiAttivita;
  case xFormID of
    1: begin
        InsRuoloForm := TInsRuoloForm.create(self);
        InsRuoloForm.CBInsComp.Visible := False;
        InsRuoloForm.ShowModal;
        if InsRuoloForm.ModalResult = mrOK then begin
          //TTabLookup.IndexName:='';
          TTabLookup.Locate('ID', InsRuoloForm.TRuoliID.Value, []);
          //TTabLookup.IndexName:=xIndexName;
        end;
        InsRuoloForm.Free;
      end;
    2: begin
        SelCompetenzaForm := TSelCompetenzaForm.create(self);
        SelCompetenzaForm.ShowModal;
        if SelCompetenzaForm.ModalResult = mrOK then begin
          //TTabLookup.IndexName:='';
          TTabLookup.Locate('ID', SelCompetenzaForm.TCompAreaID.Value, []);
          //TTabLookup.IndexName:=xIndexName;
        end;
        SelCompetenzaForm.Free;
      end;
    3: begin
        ComuniForm := TComuniForm.create(self);
        ComuniForm.EProv.text := MainForm.xUltimaProv;
        ComuniForm.ShowModal;
        if ComuniForm.ModalResult = mrOK then begin
          //TTabLookup.IndexName:='';
          TTabLookup.Locate('ID', ComuniForm.QComuniID.Value, []);
          //TTabLookup.IndexName:=xIndexName;
        end;
        MainForm.xUltimaProv := ComuniForm.EProv.text;
        ComuniForm.Free;
      end;
    4: begin
        SelDiplomaForm := TSelDiplomaForm.create(self);
        SelDiplomaForm.ShowModal;
        if SelDiplomaForm.ModalResult = mrOK then begin
          //TTabLookup.IndexName:='';
          TTabLookup.Locate('ID', SelDiplomaForm.DiplomiID.Value, []);
          //TTabLookup.IndexName:=xIndexName;
        end;
        SelDiplomaForm.Free;
      end;
  end;
end;

procedure TSelPersNewForm.BCurriculumClick(Sender: TObject);
var x: string;
  i: integer;
begin
  // prosegui timing
  if xChiamante = 1 then SelPersForm.RiprendiAttivita;
  if not OkIDAnag(QRes1ID.Value) then exit;
  if not QRes1.Eof then begin
    PosizionaAnag(QRes1ID.Value);

    Data.TTitoliStudio.Open;
    Data.TCorsiExtra.Open;
    Data.TLingueConosc.Open;
    Data.TEspLav.Open;
    CurriculumForm.ShowModal;

    Data.TTitoliStudio.Close;
    Data.TCorsiExtra.Close;
    Data.TLingueConosc.Close;
    Data.TEspLav.Close;
    MainForm.Pagecontrol5.ActivePage := MainForm.TSStatoTutti;
  end;
  // prosegui timing
  if xChiamante = 1 then SelPersForm.RiprendiAttivita;
end;

procedure TSelPersNewForm.BitBtn9Click(Sender: TObject);
begin
  // prosegui timing
  if xChiamante = 1 then SelPersForm.RiprendiAttivita;
  if not OkIDAnag(QRes1ID.Value) then exit;
  ApriFileWord(QRes1CVNumero.asString);
  // prosegui timing
  if xChiamante = 1 then SelPersForm.RiprendiAttivita;
end;

procedure TSelPersNewForm.BitBtn10Click(Sender: TObject);
var i, xID: integer;
  xSQL: string;
begin
  // prosegui timing
  if xChiamante = 1 then SelPersForm.RiprendiAttivita;
  if not OkIDAnag(QRes1ID.Value) then exit;
  if Uppercase(copy(Data.GlobalNomeAzienda.Value, 1, 6)) = 'ADVANT' then begin
    // QRes1 dalla 10a riga in poi
   { xSQL := 'select distinct Anagrafica.ID';
    for i := 9 to QRes1.SQL.Count - 2 do begin
      xSQL := xSQL + chr(13) + QRes1.SQL.Strings[i];
    end;    }
    // predisponi Data.TAnagrafica
    Data.TAnagrafica.SQL.text := 'SELECT Anagrafica.ID, Matricola, Cognome, Nome, Titolo,' + chr(13) +
      'DataNascita, GiornoNascita, MeseNascita, LuogoNascita,' + chr(13) +
      'Sesso, Indirizzo, Cap, Comune, IDComuneRes, IDZonaRes,' + chr(13) +
      'Provincia, DomicilioIndirizzo, DomicilioCap, DomicilioComune,' + chr(13) +
      'IDComuneDom, IDZonaDom, DomicilioProvincia, DomicilioStato,' + chr(13) +
      'RecapitiTelefonici, Cellulare, Fax, CodiceFiscale, PartitaIVA,' + chr(13) +
      'Email, IDStatoCivile, Anagrafica.IDStato, Anagrafica.IDTipoStato,' + chr(13) +
      'CVNumero, CVIDAnnData, CVinseritoInData, Foto, IDTipologia, ' + chr(13) +
      'IDUtenteMod, DubbiIns, IDProprietaCV, TelUfficio, OldCVNumero, ' + chr(13) +
      'EBC_Stati.Stato DescStato, TipoStrada, DomicilioTipoStrada, NumCivico, DomicilioNumCivico, ' + chr(13) +
      'IDUtente, Anagrafica.Stato, LibPrivacy, CVDataReale, CVDataAgg ' + chr(13) +
      'FROM Anagrafica,EBC_Stati ' + chr(13) +
      'WHERE Anagrafica.IDStato=EBC_Stati.ID ' + chr(13) +
      'and anagrafica.id = ' + QRes1ADOId.AsString;
      //'and Anagrafica.ID in (' + chr(13) + xSQL + ')';
    Data.TAnagrafica.SQL.SaveToFile('AnagQRes1.txt');
    Data.TAnagrafica.Open;
    xID := QRes1ID.Value;
    Data.TAnagrafica.Locate('ID', xID, []);
    ASASchedaCandForm := TASASchedaCandForm.create(self);
    ASASchedaCandForm.ShowModal;
    ASASchedaCandForm.Free;
    QRes1.Close;
    QRes1.Open;
    if Data.TAnagrafica.active then xID := Data.TAnagraficaID.value;
    QRes1.Locate('ID', xID, []);
  end else begin
    if QRes1.RecordCount > 0 then begin
      SchedaCandForm := TSchedaCandForm.create(self);
      if not PosizionaAnag(QRes1ID.Value) then exit;
      SchedaCandForm.ShowModal;
      SchedaCandForm.Free;
    end;
  end;
  // prosegui timing
  if xChiamante = 1 then SelPersForm.RiprendiAttivita;
end;

procedure TSelPersNewForm.QRes1CalcFields(DataSet: TDataSet);
var xAnno, xMese, xgiorno: Word;
begin
  if QRes1DataNascita.asString = '' then
    QRes1Eta.asString := ''
  else begin
    DecodeDate((Date - QRes1DataNascita.Value), xAnno, xMese, xgiorno);
    // TAnagraficaEta.Value:=StrToInt(copy(xAnno,3,2));
    QRes1Eta.Value := copy(IntToStr(xAnno), 3, 2);
  end;
  QRes1TelUffCell.Value := QRes1Cellulare.Value + ' ' + QRes1TelUfficio.Value;
end;

procedure TSelPersNewForm.CBColloquiatiClick(Sender: TObject);
var i: integer;
begin
  xOrdine := 'order by Anagrafica.CVNumero';
  if QRes1.Active then BitBtn2Click(self);
end;

procedure TSelPersNewForm.QRes1BeforeOpen(DataSet: TDataSet);
begin
  QEspLavLK.Open;
  QLastEspLav.Open;
end;

procedure TSelPersNewForm.QRes1AfterClose(DataSet: TDataSet);
begin
  QEspLavLK.Close;
  QLastEspLav.Close;
end;

procedure TSelPersNewForm.dxDBGrid1Click(Sender: TObject);
begin
  //if xChiamante=1 then SelPersForm.RiprendiAttivita;
end;

procedure TSelPersNewForm.dxDBGrid1CustomDrawCell(Sender: TObject;
  ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
  AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
  var AText: string; var AColor: TColor; AFont: TFont;
  var AAlignment: TAlignment; var ADone: Boolean);
var Value: variant;
  xFile: string;
begin
  if ANode.HasChildren then Exit;
  if Uppercase(copy(Data.GlobalNomeAzienda.Value, 1, 6)) <> 'BOYDEN' then begin
    if AColumn = dxDBGrid1CVNumero then begin
      Value := ANode.Values[dxDBGrid1ggDataInsCV.Index];
      if not VarIsNull(Value) then begin
        if Value > 30 then AFont.Color := clGreen; // verde: almeno 30 gg. di vita
        if Value > 60 then AFont.Color := $000080FF; // arancione: almeno 60 gg. di vita
        if Value > 90 then AFont.Color := clYellow; // giallo: almeno 90 gg. di vita
        if Value > 120 then AFont.Color := clRed; // rosso: almeno 120 gg. di vita
        if Value > 180 then AFont.Color := clMaroon; // marrone: almeno 180 gg. di vita
      end;
    end;
  end;
  if (Acolumn = dxDBGrid1ggUltimaDataIns) and (ANode.Values[dxDBGrid1ggUltimaDataIns.index] < xggDiffDi)
    and (ANode.Values[dxDBGrid1ggUltimaDataIns.index] > 0) then Acolor := clRed;
  if (Acolumn = dxDBGrid1ggDataUltimoContatto) and (ANode.Values[dxDBGrid1ggDataUltimoContatto.index] < xggDiffUc)
    and (ANode.Values[dxDBGrid1ggDataUltimoContatto.Index] > 0) then Acolor := clRed;
  if (Acolumn = dxDBGrid1DataProssimoColloquio) and (ANode.Values[dxDBGrid1DataProssimoColloquio.index] > 0)
    and (ANode.Values[dxDBGrid1DataProssimoColloquio.index] >= date) then Acolor := clYellow;
  if (Acolumn = dxDBGrid1ggDataUltimoColloquio) and (ANode.Values[dxDBGrid1ggDataUltimoColloquio.index] < xggDiffCol)
    and (ANode.Values[dxDBGrid1ggDataUltimoColloquio.index] > 0) then Acolor := clAqua;

  // controllo mancanza CV
  if Uppercase(copy(Data.GlobalNomeAzienda.Value, 1, 6)) = 'BOYDEN' then begin
    // Solo per la Boyden guarda il campo "No CV"
    if (Acolumn = dxDBGrid1CVNumero) then
      if ANode.Values[dxDBGrid1NoCv.Index] = True then AFont.Color := clRed;
  end else begin
    // per tutti gli altri guarda l'esistenza del file
    if xCheckRicCandCV then begin
      if (Acolumn = dxDBGrid1CVNumero) then begin
        xFile := GetCVPath + '\i' + AText + 'a.gif';
        if not FileExists(xFile) then AFont.Color := clRed;
      end;
    end;
  end;
end;

procedure TSelPersNewForm.dxDBGrid1MouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if (Button <> mbRight) or (Shift <> []) then Exit;
  TdxDBGridPopupMenuManager.Instance.ShowGridPopupMenu(Sender as TdxDBGrid);
end;

procedure TSelPersNewForm.Stampaelenco1Click(Sender: TObject);
begin
  if not MainForm.CheckProfile('51') then Exit;
  if not LogEsportazioniCand then exit;
  QRElencoSel := TQRElencoSel.create(self);
  QRElencoSel.QRMemo1.Lines.Clear;
  TLinee.First;
  while not TLinee.EOF do begin
    QRElencoSel.QRMemo1.Lines.Add(TLineeDescrizione.Value); ;
    TLinee.Next;
  end;
  if Uppercase(copy(Data.GlobalNomeAzienda.Value, 1, 5)) = 'ERGON' then begin
    QRElencoSel.QRLabel1.Caption := 'vecchio n�';
    QRElencoSel.QRDBText1.DataField := 'OLDCVNumero';
    // non funziona: QRElencoSel.QRLabel6.Visible:=False;
    //               QRElencoSel.QRDBText4.Visible:=False;
  end;
  QRElencoSel.Preview;
  QRElencoSel.Free;
end;

procedure TSelPersNewForm.Save(ADefaultExt, AFilter, AFileName: string; AMethod: TSaveMethod);
begin
  with SaveDialog do
  begin
    DefaultExt := ADefaultExt;
    Filter := AFilter;
    FileName := AFileName;
    if Execute then AMethod(FileName, True);
  end;
end;

procedure TSelPersNewForm.EsportainHTML1Click(Sender: TObject);
begin
  if not MainForm.CheckProfile('53') then Exit;
  if not LogEsportazioniCand then exit;
  Save('htm', 'HTML File (*.htm; *.html)|*.htm', 'ExpGrid.htm', dxDBGrid1.SaveToHTML);
end;

procedure TSelPersNewForm.EsportainExcel1Click(Sender: TObject);
begin
  if not MainForm.CheckProfile('53') then Exit;
  if not LogEsportazioniCand then exit;
  Save('xls', 'Microsoft Excel 4.0 Worksheet (*.xls)|*.xls', 'ExpGrid.xls', dxDBGrid1.SaveToXLS);
end;

procedure TSelPersNewForm.ToolbarButton971Click(Sender: TObject);
begin
  if not MainForm.CheckProfile('50') then Exit;
  LegendaRicForm := TLegendaRicForm.create(self);
  LegendaRicForm.ShowModal;
  LegendaRicForm.Free;
  Q.Close;
  Q.SQl.text := 'select ggDiffDi,ggDiffUc,ggDiffCol,CheckRicCandCV from global ';
  Q.Open;
  xggDiffDi := Q.FieldByname('ggDiffDi').asInteger;
  xggDiffUc := Q.FieldByname('ggDiffUc').asInteger;
  xggDiffCol := Q.FieldByname('ggDiffCol').asInteger;
  xCheckRicCandCV := Q.FieldByname('CheckRicCandCV').asBoolean;
  Q.Close;
  BitBtn2Click(self);
end;

procedure TSelPersNewForm.ToolbarButton973Click(Sender: TObject);
var i: integer;
begin
  if not MainForm.CheckProfile('52') then Exit;
  if QRes1.isEmpty then exit;
  FaxPresentazForm := TFaxPresentazForm.create(self);
  FaxPresentazForm.ASG1.RowCount := QRes1.RecordCount + 1;
  QRes1.First;
  QRes1.DisableControls;
  i := 1;
  while not QRes1.EOF do begin
    FaxPresentazForm.ASG1.addcheckbox(0, i, false, false);
    FaxPresentazForm.ASG1.Cells[0, i] := QRes1Cognome.Value + ' ' + QRes1Nome.Value;
    FaxPresentazForm.xArrayIDAnag[i] := QRes1ID.Value;
    QRes1.Next;
    inc(i);
  end;
  QRes1.EnableControls;
  FaxPresentazForm.PanClienti.Visible := False;
  FaxPresentazForm.ShowModal;
  FaxPresentazForm.Free;
end;

procedure TSelPersNewForm.Stampagriglia1Click(Sender: TObject);
begin
  if not MainForm.CheckProfile('53') then Exit;
  if not LogEsportazioniCand then exit;
  dxPrinter1.Preview(True, nil);
end;

procedure TSelPersNewForm.TTabQueryAfterOpen(DataSet: TDataSet);
begin
  TTabOp.Open;
end;

function TSelPersNewForm.LogEsportazioniCand: boolean;
var xCriteri: string;
begin
  // restituisce FALSE se l'utente ha abortito l'operazione
  Result := True;
  if MessageDlg('ATTENZIONE:  l''esportazione di dati � un''operazione soggetta a registrazione' + chr(13) +
    'secondo la normativa sulla privacy.  SEI SICURO DI VOLER PROSEGUIRE ?', mtWarning, [mbYes, mbNo], 0) = mrNo then begin
    Result := False;
    exit;
  end;
  with Data do begin
    DB.BeginTrans;
    try
      TLinee.First;
      xCriteri := '';
      while not TLinee.EOF do begin
        xCriteri := xCriteri + TLineeDescrizione.Value + chr(13);
        TLinee.Next;
      end;
      Q1.Close;
      Q1.SQL.text := 'insert into LogEsportazCand (IDUtente,Data,Criteri,StringaSQL,ResultNum) ' +
        ' values (:xIDUtente,:xData,:xCriteri,:xStringaSQL,:xResultNum)';
      Q1.ParambyName('xIDUtente').asInteger := MainForm.xIDUtenteAttuale;
      Q1.ParambyName('xData').asDateTime := Date;
      Q1.ParambyName('xCriteri').asString := xCriteri;
      Q1.ParambyName('xStringaSQL').asString := QRes1ADO.CommandText;
      Q1.ParambyName('xResultNum').asInteger := QRes1ADO.RecordCount;
      Q1.ExecSQL;

      DB.CommitTrans;
    except
      DB.RollbackTrans;
      MessageDlg('ERRORE SUL DATABASE: operazione non effettuata', mtError, [mbOK], 0);
    end;
  end;
end;

procedure TSelPersNewForm.cancellasoggetti1Click(Sender: TObject);
var xPassword: string;
begin
  if QRes1.IsEmpty then exit;
  // cancellazione soggetti selezionati
  if MessageDlg('ATTENZIONE: la procedura � IRREVERSIBILE e DEFINITIVA !!' + chr(13) +
    'E'' necessario verificare il rispetto della normativa sulla Privacy e sulla' + chr(13) +
    'conservazione dei relativi dati.' + chr(13) + chr(13) +
    'SEI DAVVERO SICURO DI VOLER PROCEDERE ?', mtWarning,
    [mbNo, mbYes], 0) = mrNo then exit;

  // richiesta password amministratore
  xPassword := '';
  if not InputQuery('cancellazione soggetti', 'Password di amministratore', xPassword) then exit;
  Q.Close;
  Q.SQL.text := 'select Password from Users where Nominativo=''Administrator''';
  Q.Open;
  if not CheckPassword(xPassword, Q.Fieldbyname('Password').asString) then begin
    MessageDlg('Password di amministratore errata', mtError, [mbOK], 0);
    Q.Close;
    exit;
  end;
  Q.Close;
  if MessageDlg('Verranno eliminati TUTTI i dati di TUTTE le tabelle collegate per TUTTI i SOGGETTI !' + chr(13) +
    'IMPORTANTE: L''OPERAZIONE E'' IRREVERSIBILE e  non sar� pi� possibile recuperare i dati !!!' + chr(13) + chr(13) +
    'SEI DAVVERO SICURO DI VOLER PROCEDERE ?', mtWarning,
    [mbNo, mbYes], 0) = mrYes then begin
    // vai - DEFINITIVA per tutti i soggetti !!
    QRes1.First;
    while not QRes1.EOF do begin
      CancellaSogg(QRes1ID.Value);
      QRes1.Next;
    end;
    BitBtn2Click(self);
  end;
end;

procedure TSelPersNewForm.PMStati1Click(Sender: TObject);
begin
  PMStati1.Checked := not PMStati1.Checked;
end;

procedure TSelPersNewForm.PMStati2Click(Sender: TObject);
begin
  PMStati2.Checked := not PMStati2.Checked;
end;

procedure TSelPersNewForm.PMStati3Click(Sender: TObject);
begin
  PMStati3.Checked := not PMStati3.Checked;
end;

procedure TSelPersNewForm.PMStati4Click(Sender: TObject);
begin
  PMStati4.Checked := not PMStati4.Checked;
end;

procedure TSelPersNewForm.PMStati5Click(Sender: TObject);
begin
  PMStati5.Checked := not PMStati5.Checked;
end;

procedure TSelPersNewForm.cancellatuttiisoggettiselezionati1Click(Sender: TObject);
var xPassword: string;
  k: integer;
begin
  if QRes1.IsEmpty then exit;
  // cancellazione soggetti selezionati
  if MessageDlg('ATTENZIONE: la procedura � IRREVERSIBILE e DEFINITIVA !!' + chr(13) +
    'E'' necessario verificare il rispetto della normativa sulla Privacy e sulla' + chr(13) +
    'conservazione dei relativi dati.' + chr(13) + chr(13) +
    'SEI DAVVERO SICURO DI VOLER PROCEDERE ?', mtWarning,
    [mbNo, mbYes], 0) = mrNo then exit;

  // richiesta password amministratore
  xPassword := '';
  if not InputQuery('cancellazione soggetti', 'Password di amministratore', xPassword) then exit;
  Q.Close;
  Q.SQL.text := 'select Password from Users where Nominativo=''Administrator''';
  Q.Open;
  if not CheckPassword(xPassword, Q.Fieldbyname('Password').asString) then begin
    MessageDlg('Password di amministratore errata', mtError, [mbOK], 0);
    Q.Close;
    exit;
  end;
  Q.Close;
  if MessageDlg('Verranno eliminati TUTTI i dati di TUTTE le tabelle collegate per TUTTI i SOGGETTI SELEZIONATI !' + chr(13) +
    'IMPORTANTE: L''OPERAZIONE E'' IRREVERSIBILE e  non sar� pi� possibile recuperare i dati !!!' + chr(13) + chr(13) +
    'SEI DAVVERO SICURO DI VOLER PROCEDERE ?', mtWarning,
    [mbNo, mbYes], 0) = mrYes then begin
    // vai - DEFINITIVA per tutti i soggetti SELEZIONATI !!
    for k := 0 to dxDBGrid1.SelectedCount - 1 do begin
      QRes1.BookMark := dxDBGrid1.SelectedRows[k];
      CancellaSogg(QRes1ID.Value);
    end;
    BitBtn2Click(self);
  end;
end;

procedure TSelPersNewForm.mettiinstatoeliminato1Click(Sender: TObject);
var k: integer;
begin
  if MessageDlg('TUTTI i SOGGETTI SELEZIONATI verranno messi nello stato "eliminato".' + chr(13) +
    'SEI DAVVERO SICURO DI VOLER PROCEDERE ?', mtWarning,
    [mbNo, mbYes], 0) = mrYes then begin
    // metti in stato ELIMINATO
    with Data do begin
      DB.BeginTrans;
      try
        for k := 0 to dxDBGrid1.SelectedCount - 1 do begin
          QRes1.BookMark := dxDBGrid1.SelectedRows[k];
          // modifica stato e tipostato
          Q1.SQL.text := 'update Anagrafica set IDStato=:xIDStato,IDTipoStato=:xIDTipoStato where ID=' + QRes1ID.AsString;
          Q1.ParambyName('xIDStato').asInteger := 30;
          Q1.ParambyName('xIDTipoStato').asInteger := 5;
          Q1.ExecSQL;
          // evento in storico
          Q1.SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
            'values (:xIDAnagrafica,:xIDEvento,:xDataEvento,:xAnnotazioni,:xIDRicerca,:xIDUtente,:xIDCliente)';
          Q1.ParamByName('xIDAnagrafica').asInteger := QRes1ID.Value;
          Q1.ParamByName('xIDEvento').asInteger := 80;
          Q1.ParamByName('xDataEvento').asDateTime := Date;
          Q1.ParamByName('xAnnotazioni').asString := 'eliminazione da motore di ricerca';
          Q1.ParamByName('xIDRicerca').asInteger := 0;
          Q1.ParamByName('xIDUtente').asInteger := MainForm.xIDUtenteAttuale;
          Q1.ParamByName('xIDCliente').asInteger := 0;
          Q1.ExecSQL;
        end;
        DB.CommitTrans;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: operazione non effettuata', mtError, [mbOK], 0);
      end;
    end;
    BitBtn2Click(self);
  end;
end;

procedure TSelPersNewForm.DBGrid3LivLinguaParlatoButtonClick(
  Sender: TObject; AbsoluteIndex: Integer);
var xID: string;
begin
  if TLineeTabella.Value = 'LingueConosciute' then begin
    xID := OpenSelFromTab('LivelloLingue', 'ID', 'LivelloConoscenza', 'Livello conoscenza', TLineeLivLinguaParlato.asString, False);
    if xID <> '' then begin
      if not (dsLinee.state = dsEdit) then TLinee.Edit;
      TLineeLivLinguaParlato.Value := StrToIntDef(xID, 0);
      TLinee.Post;
    end;
  end;
end;

procedure TSelPersNewForm.DBGrid3LivLinguaScrittoButtonClick(
  Sender: TObject; AbsoluteIndex: Integer);
var xID: string;
begin
  if TLineeTabella.Value = 'LingueConosciute' then begin
    xID := OpenSelFromTab('LivelloLingue', 'ID', 'LivelloConoscenza', 'Livello conoscenza', TLineeLivLinguaScritto.asString, False);
    if xID <> '' then begin
      if not (dsLinee.state = dsEdit) then TLinee.Edit;
      TLineeLivLinguaScritto.Value := StrToIntDef(xID, 0);
      TLinee.Post;
    end;
  end;
end;

procedure TSelPersNewForm.qRes1Ado_oldBeforeOpen(DataSet: TDataSet);
var xs: TStringList;
  xStringa: string;
begin
  QEspLavLK.Open;
  QLastEspLav.Open;

  xs := TStringList.Create;
  if FileExists(extractfiledir(Application.ExeName) + '\H1Connstring.txt') then begin
    xs.LoadFromFile(extractfiledir(Application.ExeName) + '\H1Connstring.txt');
    xStringa := xs[0];
    qRes1Ado.ConnectionString := TwCrypter1.DecryptString(xStringa);
  end else
    Messagedlg('ERRORE: File H1ConnString non trovato', mtError, [mbOk], 0);

  xs.Free
end;

procedure TSelPersNewForm.QRes1ADOCalcFields(DataSet: TDataSet);
var xAnno, xMese, xgiorno: Word;
begin
  if QRes1adoDataNascita.asString = '' then
    QRes1aDOEta.asString := ''
  else begin
    DecodeDate((Date - QRes1adODataNascita.Value), xAnno, xMese, xgiorno);
    // TAnagraficaEta.Value:=StrToInt(copy(xAnno,3,2));
    QRes1adoEta.Value := copy(IntToStr(xAnno), 3, 2);
  end;
  QRes1adoTelUffCell.Value := QRes1AdoCellulare.Value + ' ' +QRes1AdoTelUfficio.Value;
end;

end.

