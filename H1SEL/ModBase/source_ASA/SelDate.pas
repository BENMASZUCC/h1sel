unit SelDate;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, DtEdit97, Buttons, Mask;

type
  TSelDateForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    DataDal: TDateEdit97;
    DataAl: TDateEdit97;
    Label1: TLabel;
    Label2: TLabel;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    MEDalleOre: TMaskEdit;
    MEAlleOre: TMaskEdit;
    GroupBox2: TGroupBox;
    CBLun: TCheckBox;
    CBMar: TCheckBox;
    CBMer: TCheckBox;
    CBGio: TCheckBox;
    CBVen: TCheckBox;
    CBSab: TCheckBox;
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    xDataDal,xDataAl:TDateTime;
  end;

var
  SelDateForm: TSelDateForm;

implementation

uses Main;

{$R *.DFM}

procedure TSelDateForm.BitBtn1Click(Sender: TObject);
var xError:boolean;
begin
     if (trim(MEDalleOre.Text)='') or (trim(MEAlleOre.Text)='') then begin
        MessageDlg('Errore nell''inserimento degli orari',mtError,[mbOK],0);
        ModalResult:=mrNone;
        Abort;
     end;

     xError:=False;
     if (xDataDal<>null) and (MainForm.RGCerte.ItemIndex=0) then
        if DataDal.Date<xDataDal then xError:=True;
     if (xDataDal<>null) and (MainForm.RGCerte.ItemIndex=0) then
        if DataAl.Date>xDataAl then xError:=True;
     if xError then begin
        MessageDlg('Errore nell''inserimento delle date rispetto a quelle certe',mtError,[mbOK],0);
        ModalResult:=mrNone;
        Abort;
     end;
end;

end.
