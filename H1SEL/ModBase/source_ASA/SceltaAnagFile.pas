unit SceltaAnagFile;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Grids, DBGrids, RXDBCtrl, Db, DBTables, ExtCtrls;

type
  TSceltaAnagFileForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Label1: TLabel;
    QAnagFile: TQuery;
    DsQAnagFile: TDataSource;
    RxDBGrid1: TRxDBGrid;
    QAnagFileTipo: TStringField;
    QAnagFileID: TAutoIncField;
    QAnagFileIDAnagrafica: TIntegerField;
    QAnagFileIDTipo: TIntegerField;
    QAnagFileDescrizione: TStringField;
    QAnagFileNomeFile: TStringField;
    QAnagFileDataCreazione: TDateTimeField;
    Label2: TLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SceltaAnagFileForm: TSceltaAnagFileForm;

implementation

{$R *.DFM}

end.
