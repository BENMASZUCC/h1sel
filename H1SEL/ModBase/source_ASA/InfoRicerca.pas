unit InfoRicerca;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, DBCtrls, Mask, Grids, DBGrids;

type
  TInfoRicercaForm = class(TForm)
    GroupBox3: TGroupBox;
    DBGrid2: TDBGrid;
    GroupBox4: TGroupBox;
    GroupBox1: TGroupBox;
    CBArea: TCheckBox;
    CBRuolo: TCheckBox;
    CBSpec: TCheckBox;
    DBLArea: TDBLookupComboBox;
    DBLRuolo: TDBLookupComboBox;
    DBLSpec: TDBLookupComboBox;
    GroupBox6: TGroupBox;
    DBGrid3: TDBGrid;
    GroupBox2: TGroupBox;
    BitBtn1: TBitBtn;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  InfoRicercaForm: TInfoRicercaForm;

implementation

uses ModuloDati3;

{$R *.DFM}


procedure TInfoRicercaForm.FormShow(Sender: TObject);
begin
     InfoRicercaForm.Top:=30;
     InfoRicercaForm.Left:=60;
end;

end.
