unit InsAnnuncio;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, DBCtrls, Mask, Buttons, Spin, DtEdit97, Grids, DBGrids, Db,
  DBTables, ToolEdit, CurrEdit, ExtCtrls, TB97;

type
  TInsAnnuncioForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    DEData: TDateEdit97;
    Label1: TLabel;
    GroupBox4: TGroupBox;
    GBCliente: TGroupBox;
    BTrovaCliente: TSpeedButton;
    ECliente: TEdit;
    TUsers: TTable;
    DsUsers: TDataSource;
    TUsersID: TAutoIncField;
    TUsersNominativo: TStringField;
    DBGrid1: TDBGrid;
    Label2: TLabel;
    ERif: TEdit;
    procedure BTrovaClienteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    xIDCliente,xIDRuolo,xIDArea:integer;
  end;

var
  InsAnnuncioForm: TInsAnnuncioForm;

implementation

uses SelCliente;

{$R *.DFM}

procedure TInsAnnuncioForm.BTrovaClienteClick(Sender: TObject);
begin
     SelClienteForm:=TSelClienteForm.create(self);
     SelClienteForm.ShowModal;
     if SelClienteForm.ModalResult=mrOK then begin
        xIDCliente:=SelClienteForm.TClientiID.Value;
        ECliente.Text:=SelClienteForm.TClientiDescrizione.Value;
     end;
     SelClienteForm.Free;
end;

procedure TInsAnnuncioForm.FormCreate(Sender: TObject);
begin
     xIDCliente:=0;
end;

procedure TInsAnnuncioForm.FormShow(Sender: TObject);
begin
     Caption:='[M/30] - '+caption;
end;

end.
