unit Edizione;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Grids, DBGrids, Db, DBTables;

type
  TEdizioneForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Label1: TLabel;
    ENomeEdizione: TEdit;
    Label5: TLabel;
    ENote: TEdit;
    QConcess: TQuery;
    DsQConcess: TDataSource;
    DBGrid1: TDBGrid;
    QConcessID: TAutoIncField;
    QConcessDescrizione: TStringField;
    QConcessComune: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EdizioneForm: TEdizioneForm;

implementation

{$R *.DFM}

end.
