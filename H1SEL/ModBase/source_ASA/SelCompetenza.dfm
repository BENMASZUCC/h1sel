�
 TSELCOMPETENZAFORM 0+
  TPF0TSelCompetenzaFormSelCompetenzaFormLeftTopnActiveControlDBGrid1BorderStylebsDialogCaptionScelta competenzaClientHeightiClientWidth!Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrder	PositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TPanelPanel1Left� Top-WidthDHeight	AlignmenttaLeftJustifyCaption,  Elenco competenze (per l'area selezionata)ColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderTabStop	  TBitBtnBitBtn1LeftoTopWidthRHeight"TabOrder KindbkOK  TBitBtnBitBtn2Left�TopWidthRHeight"CaptionAnnullaTabOrderKindbkCancel  TPanelPanel2LeftTopWidth� Height	AlignmenttaLeftJustifyCaption  Aree competenzeColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderTabStop	  TDBGridDBGrid1LeftTop-Width� Height5
DataSource
DsAreeCompTabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style 
OnKeyPressDBGrid1KeyPressColumnsExpanded	FieldNameDescrizioneTitle.CaptionAreaWidth� Visible	    TDBGridDBGrid2Left� TopCWidthDHeight
DataSource
DsCompAreaTabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameDescrizioneTitle.Caption
CompetenzaWidth#Visible	    TTable	TCompAreaDatabaseNameEBCDB	IndexNameIdxAreaMasterFieldsIDMasterSource
DsAreeCompReadOnly		TableNamedbo.CompetenzeLeftTop�  TAutoIncFieldTCompAreaID	FieldNameID  TIntegerFieldTCompAreaIDArea	FieldNameIDArea  TStringFieldTCompAreaDescrizione	FieldNameDescrizioneSize(  TStringFieldTCompAreaTipologia	FieldName	TipologiaSize  TIntegerFieldTCompAreaQuantiValori	FieldNameQuantiValori  TStringFieldTCompAreaArrayDescrizione	FieldNameArrayDescrizioneSized   TDataSource
DsCompAreaDataSet	TCompAreaLeftTop�   TTable	TAreeCompDatabaseNameEBCDB	IndexNameIdxDescrizAreaReadOnly		TableNamedbo.AreeCompetenzeLeft0Top�  TAutoIncFieldTAreeCompID	FieldNameIDReadOnly	  TStringFieldTAreeCompDescrizione	FieldNameDescrizioneSize   TDataSource
DsAreeCompDataSet	TAreeCompLeft0Top�   TTimerTimer1OnTimerTimer1TimerLeftTop@   