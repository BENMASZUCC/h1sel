object CreaCompensiForm: TCreaCompensiForm
  Left = 223
  Top = 108
  BorderStyle = bsDialog
  Caption = 'Riporto dettaglio offerta in compensi commessa'
  ClientHeight = 195
  ClientWidth = 442
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 6
    Top = 5
    Width = 298
    Height = 13
    Caption = 'Selezionare i dettagli che diventano compensi per la commessa'
  end
  object BitBtn1: TBitBtn
    Left = 342
    Top = 4
    Width = 97
    Height = 33
    TabOrder = 0
    Kind = bkOK
  end
  object ASG1: TAdvStringGrid
    Left = 4
    Top = 21
    Width = 333
    Height = 169
    ColCount = 1
    DefaultRowHeight = 16
    FixedCols = 0
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goEditing]
    ScrollBars = ssVertical
    TabOrder = 1
    AutoNumAlign = False
    AutoSize = False
    VAlignment = vtaCenter
    EnhTextSize = False
    EnhRowColMove = False
    SortFixedCols = False
    SizeWithForm = False
    Multilinecells = False
    OnCanEditCell = ASG1CanEditCell
    SortDirection = sdAscending
    SortFull = True
    SortAutoFormat = True
    SortShow = False
    EnableGraphics = True
    SortColumn = 0
    HintColor = clYellow
    SelectionColor = clHighlight
    SelectionTextColor = clHighlightText
    SelectionRectangle = False
    SelectionRTFKeep = False
    HintShowCells = False
    OleDropTarget = False
    OleDropSource = False
    OleDropRTF = False
    PrintSettings.FooterSize = 0
    PrintSettings.HeaderSize = 0
    PrintSettings.Time = ppNone
    PrintSettings.Date = ppNone
    PrintSettings.DateFormat = 'dd/mm/yyyy'
    PrintSettings.PageNr = ppNone
    PrintSettings.Title = ppNone
    PrintSettings.Font.Charset = DEFAULT_CHARSET
    PrintSettings.Font.Color = clWindowText
    PrintSettings.Font.Height = -11
    PrintSettings.Font.Name = 'MS Sans Serif'
    PrintSettings.Font.Style = []
    PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
    PrintSettings.HeaderFont.Color = clWindowText
    PrintSettings.HeaderFont.Height = -11
    PrintSettings.HeaderFont.Name = 'MS Sans Serif'
    PrintSettings.HeaderFont.Style = []
    PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
    PrintSettings.FooterFont.Color = clWindowText
    PrintSettings.FooterFont.Height = -11
    PrintSettings.FooterFont.Name = 'MS Sans Serif'
    PrintSettings.FooterFont.Style = []
    PrintSettings.Borders = pbNoborder
    PrintSettings.BorderStyle = psSolid
    PrintSettings.Centered = False
    PrintSettings.RepeatFixedRows = False
    PrintSettings.RepeatFixedCols = False
    PrintSettings.LeftSize = 0
    PrintSettings.RightSize = 0
    PrintSettings.ColumnSpacing = 0
    PrintSettings.RowSpacing = 0
    PrintSettings.TitleSpacing = 0
    PrintSettings.Orientation = poPortrait
    PrintSettings.PagePrefix = 'page'
    PrintSettings.FixedWidth = 0
    PrintSettings.FixedHeight = 0
    PrintSettings.UseFixedHeight = False
    PrintSettings.UseFixedWidth = False
    PrintSettings.FitToPage = fpNever
    PrintSettings.PageNumSep = '/'
    PrintSettings.NoAutoSize = False
    PrintSettings.PrintGraphics = False
    HTMLSettings.Width = 100
    Navigation.AllowInsertRow = False
    Navigation.AllowDeleteRow = False
    Navigation.AdvanceOnEnter = False
    Navigation.AdvanceInsert = False
    Navigation.AutoGotoWhenSorted = False
    Navigation.AutoGotoIncremental = False
    Navigation.AutoComboDropSize = False
    Navigation.AdvanceDirection = adLeftRight
    Navigation.AllowClipboardShortCuts = False
    Navigation.AllowSmartClipboard = False
    Navigation.AllowRTFClipboard = False
    Navigation.AdvanceAuto = False
    Navigation.InsertPosition = pInsertBefore
    Navigation.CursorWalkEditor = False
    Navigation.MoveRowOnSort = False
    Navigation.ImproveMaskSel = False
    Navigation.AlwaysEdit = False
    ColumnSize.Save = False
    ColumnSize.Stretch = False
    ColumnSize.Location = clRegistry
    CellNode.Color = clSilver
    CellNode.NodeType = cnFlat
    CellNode.NodeColor = clBlack
    SizeWhileTyping.Height = False
    SizeWhileTyping.Width = False
    MouseActions.AllSelect = False
    MouseActions.ColSelect = False
    MouseActions.RowSelect = False
    MouseActions.DirectEdit = True
    MouseActions.DisjunctRowSelect = False
    MouseActions.AllColumnSize = False
    MouseActions.CaretPositioning = False
    IntelliPan = ipVertical
    URLColor = clBlack
    URLShow = False
    URLFull = False
    URLEdit = False
    ScrollType = ssNormal
    ScrollColor = clNone
    ScrollWidth = 16
    ScrollProportional = False
    ScrollHints = shNone
    OemConvert = False
    FixedFooters = 0
    FixedRightCols = 0
    FixedColWidth = 310
    FixedRowHeight = 16
    FixedFont.Charset = DEFAULT_CHARSET
    FixedFont.Color = clWindowText
    FixedFont.Height = -11
    FixedFont.Name = 'MS Sans Serif'
    FixedFont.Style = []
    FixedAsButtons = False
    FloatFormat = '%.2f'
    WordWrap = False
    ColumnHeaders.Strings = (
      'soggetto'
      'E'
      'C')
    Lookup = False
    LookupCaseSensitive = False
    LookupHistory = False
    HideFocusRect = False
    BackGround.Top = 0
    BackGround.Left = 0
    BackGround.Display = bdTile
    Hovering = False
    Filter = <>
    FilterActive = False
    ColWidths = (
      310)
    RowHeights = (
      16
      16
      16
      16
      16)
  end
  object BitBtn2: TBitBtn
    Left = 342
    Top = 39
    Width = 97
    Height = 33
    Caption = 'Annulla'
    TabOrder = 2
    Kind = bkCancel
  end
end
