program H1sel;

uses
  Windows,
  Forms,
  Dialogs,
  ModuloDati in 'ModuloDati.pas' {Data: TDataModule},
  Splash2 in 'Splash2.pas' {Splash2Form},
  CambiaStato2 in 'CambiaStato2.pas' {CambiaStato2Form},
  ModuloDati2 in 'ModuloDati2.pas' {Data2: TDataModule},
  Curriculum in 'Curriculum.pas' {CurriculumForm},
  DettaglioTesi in 'DettaglioTesi.pas' {DettaglioTesiForm},
  Lingue in 'Lingue.pas' {LingueForm},
  DettEspLav in 'DettEspLav.pas' {DettEspLavForm},
  Splash in 'Splash.pas' {SplashForm},
  Login in 'Login.pas' {LoginForm},
  SelArea in 'SelArea.pas' {SelAreaForm},
  SceltaStato in 'SceltaStato.pas' {SceltaStatoForm},
  MessLetti in 'MessLetti.pas' {MessLettiForm},
  StoricoComp in 'StoricoComp.pas' {StoricoCompForm},
  Main in 'Main.pas' {MainForm},
  SelPers in 'SelPers.pas' {SelPersForm},
  SelDiploma in 'SelDiploma.pas' {SelDiplomaForm},
  SelRuolo in 'SelRuolo.pas' {SelRuoloForm},
  SelLingua in 'SelLingua.pas' {SelLinguaForm},
  CompInserite in 'CompInserite.pas' {CompInseriteForm},
  ValutazDip in 'ValutazDip.pas' {ValutazDipForm},
  ElencoDip in 'ElencoDip.pas' {ElencoDipForm},
  RepElencoSel in 'RepElencoSel.pas' {QRElencoSel: TQuickRep},
  RepSchedaVal in 'RepSchedaVal.pas' {QRSchedaVal: TQuickRep},
  RuoloRif in 'RuoloRif.pas' {RuoloRifForm},
  CercaAnnuncio in 'CercaAnnuncio.pas' {CercaAnnuncioForm},
  MessIniziale in 'MessIniziale.pas' {MessInizialeForm},
  Messaggio in 'Messaggio.pas' {MessaggioForm},
  SelCompetenza in 'SelCompetenza.pas' {SelCompetenzaForm},
  InsRuolo in 'InsRuolo.pas' {InsRuoloForm},
  InsCaratt in 'InsCaratt.pas' {InsCarattForm},
  ConfEvento in 'ConfEvento.pas' {ConfEventoForm},
  Splash3 in 'Splash3.pas' {Splash3Form},
  AgendaSett in 'AgendaSett.pas' {AgendaSettForm},
  InsMessaggio in 'InsMessaggio.pas' {InsMessaggioForm},
  ModMessaggio in 'ModMessaggio.pas' {ModMessaggioForm},
  NuovoApp in 'NuovoApp.pas' {NuovoImpForm},
  Sposta in 'Sposta.pas' {SpostaForm},
  SelData in 'SelData.pas' {SelDataForm},
  View in 'View.pas' {ViewForm},
  SceltaAcquisiz in 'SceltaAcquisiz.pas' {SceltaAcquisizForm},
  InsEvento in 'InsEvento.pas' {InsEventoForm},
  GestStorico in 'GestStorico.pas' {GestStoricoForm},
  NuovaComp in 'NuovaComp.pas' {NuovaCompForm},
  QRStorico in 'QRStorico.pas' {QRStoricoForm},
  MDRicerche in 'MDRicerche.pas' {DataRicerche: TDataModule},
  Specifiche in 'Specifiche.pas' {SpecificheForm},
  ElencoRicPend in 'ElencoRicPend.pas' {ElencoRicPendForm},
  ContattoCand in 'ContattoCand.pas' {ContattoCandForm},
  StoriaContatti in 'StoriaContatti.pas' {StoriaContattiForm},
  ModuloDati4 in 'ModuloDati4.pas' {DataAnnunci: TDataModule},
  SelCliente in 'SelCliente.pas' {SelClienteForm},
  EsitoColloquio in 'EsitoColloquio.pas' {EsitoColloquioForm},
  FissaColloquio in 'FissaColloquio.pas' {FissaColloquioForm},
  InSelezione in 'InSelezione.pas' {InSelezioneForm},
  Inserimento in 'Inserimento.pas' {InserimentoForm},
  InsInserimento in 'InsInserimento.pas' {InsInserimentoForm},
  RicClienti in 'RicClienti.pas' {RicClientiForm},
  EspLav in 'EspLav.pas' {EspLavForm},
  Azienda in 'Azienda.pas' {AziendaForm},
  OffertaCliente in 'OffertaCliente.pas' {OffertaClienteForm},
  ContattoCliFatto in 'ContattoCliFatto.pas' {ContattoCliFattoForm},
  FaxPresentaz in 'FaxPresentaz.pas' {FaxPresentazForm},
  uGestEventi in 'uGestEventi.pas',
  SchedaCand in 'SchedaCand.pas' {SchedaCandForm},
  uUtilsVarie in 'uUtilsVarie.pas',
  InsCompetenza in 'InsCompetenza.pas' {InsCompetenzaForm},
  SelEdiz in 'SelEdiz.pas' {SelEdizForm},
  InsAnnuncio in 'InsAnnuncio.pas' {InsAnnuncioForm},
  Comuni in 'Comuni.pas' {ComuniForm},
  ZoneComuni in 'ZoneComuni.pas' {ZoneComuniForm},
  Fattura in 'Fattura.pas' {FatturaForm},
  InsDettFatt in 'InsDettFatt.pas' {InsDettFattForm},
  Presentaz in 'Presentaz.pas' {PresentazForm},
  RiepTiming in 'RiepTiming.pas' {RiepTimingForm},
  RepElencoAnnCand in 'RepElencoAnnCand.pas' {QRElencoAnnCand},
  ModDettCandRic in 'ModDettCandRic.pas' {ModDettCandRicForm},
  ModStatoRic in 'ModStatoRic.pas' {ModStatoRicForm},
  StoricoRic in 'StoricoRic.pas' {StoricoRicForm},
  SelAttivita in 'SelAttivita.pas' {SelAttivitaForm},
  OpzioniEliminaz in 'OpzioniEliminaz.pas' {OpzioniEliminazForm},
  CheckPass in 'CheckPass.pas',
  NuoviCV in 'NuoviCV.pas' {NuoviCVForm},
  InsNodoOrg in 'InsNodoOrg.pas' {InsNodoOrgForm},
  ElencoAziende in 'ElencoAziende.pas' {ElencoAziendeForm},
  NotaSpese in 'NotaSpese.pas' {NotaSpeseForm},
  ElencoUtenti in 'ElencoUtenti.pas' {ElencoUtentiForm},
  ElencoFattCliente in 'ElencoFattCliente.pas' {ElencoFattClienteForm},
  DubbiIns in 'DubbiIns.pas' {DubbiInsForm},
  Compenso in 'Compenso.pas' {CompensoForm},
  Risorsa in 'Risorsa.pas' {RisorsaForm},
  ContattoCli in 'ContattoCli.pas' {ContattoCliForm},
  ModFatt in 'ModFatt.pas' {ModFattForm},
  InsCliente in 'InsCliente.pas' {InsClienteForm},
  Competenza in 'Competenza.pas' {CompetenzaForm},
  EmailConnectInfo in 'EmailConnectInfo.pas' {EmailConnectInfoForm},
  TipiMailCand in 'TipiMailCand.pas' {TipiMailCandForm},
  RepAgenda in 'RepAgenda.pas' {QRAgenda},
  EmailMessage in 'EmailMessage.pas' {EmailMessageForm},
  LegendaCandUtente in 'LegendaCandUtente.pas' {LegendaCandUtenteForm},
  LegendaGestRic in 'LegendaGestRic.pas' {LegendaGestRicForm},
  ValutazColloquio in 'ValutazColloquio.pas' {ValutazColloquioForm},
  ValutazCollDett in 'ValutazCollDett.pas' {ValutazCollDettForm},
  AnagFile in 'AnagFile.pas' {AnagFileForm},
  SchedaSintetica in 'SchedaSintetica.pas' {SchedaSinteticaForm},
  SelRicCand in 'SelRicCand.pas' {SelRicCandForm},
  RifClienteDati in 'RifClienteDati.pas' {RifClienteDatiForm},
  SelPersNew in 'SelPersNew.pas' {SelPersNewForm},
  LegendaRic in 'LegendaRic.pas' {LegendaRicForm},
  ElencoRicCliente in 'ElencoRicCliente.pas' {ElencoRicClienteForm},
  SceltaModelloAnag in 'SceltaModelloAnag.pas' {SceltaModelloAnagForm},
  SceltaModelloMailCli in 'SceltaModelloMailCli.pas' {SceltaModelloMailCliForm},
  StoricoOfferta in 'StoricoOfferta.pas' {StoricoOffertaForm},
  Offerte in 'Offerte.pas' {OfferteForm},
  SelContatti in 'SelContatti.pas' {SelContattiForm},
  DettOfferta in 'DettOfferta.pas' {DettOffertaForm},
  TipoCommessa in 'TipoCommessa.pas' {TipoCommessaForm},
  SelTipoCommessa in 'SelTipoCommessa.pas' {SelTipoCommessaForm},
  CreaCompensi in 'CreaCompensi.pas' {CreaCompensiForm},
  DettAnnuncio in 'DettAnnuncio.pas' {DettAnnuncioForm},
  InsRicerca in 'InsRicerca.pas' {InsRicercaForm},
  DettSede in 'DettSede.pas' {DettSedeForm},
  ElencoSedi in 'ElencoSedi.pas' {ElencoSediForm},
  CandUtente in 'CandUtente.pas' {CandUtenteFrame: TFrame},
  LegendaElRic in 'LegendaElRic.pas' {LegendaElRicForm},
  DateInserimento in 'DateInserimento.pas' {DateInserimentoForm},
  Comune in 'Comune.pas' {ComuneForm},
  OrgStandard in 'OrgStandard.pas' {OrgStandardForm},
  NuovoSoggetto in 'NuovoSoggetto.pas' {NuovoSoggettoForm},
  Nazione in 'Nazione.pas' {NazioneForm},
  SelNazione in 'SelNazione.pas' {SelNazioneForm},
  ModRuolo in 'ModRuolo.pas' {ModRuoloForm},
  CliCandidati in 'CliCandidati.pas' {CliCandidatiFrame: TFrame},
  Uscita in 'Uscita.pas' {UscitaForm},
  QREtichette1 in 'QREtichette1.pas' {QREtichette1Form: TQuickRep},
  OpzioniEtichette in 'OpzioniEtichette.pas' {OpzioniEtichetteForm},
  EliminaDipOrg in 'EliminaDipOrg.pas' {EliminaDipOrgForm},
  FrmStoricoAnagPos in 'FrmStoricoAnagPos.pas' {StoricoAnagPosFrame: TFrame},
  ModStoricoAnagPos in 'ModStoricoAnagPos.pas' {ModStoricoAnagPosForm},
  StoricoAnagPos in 'StoricoAnagPos.pas' {StoricoAnagPosForm},
  FrmListinoAnnunci in 'FrmListinoAnnunci.pas' {ListinoAnnunciFrame: TFrame},
  PrezzoListino in 'PrezzoListino.pas' {PrezzoListinoForm},
  StoricoPrezziAnnunciCli in 'StoricoPrezziAnnunciCli.pas' {StoricoPrezziAnnunciCliForm},
  Testata in 'Testata.pas' {TestataForm},
  Edizione in 'Edizione.pas' {EdizioneForm},
  ElencoTipiAnnunci in 'ElencoTipiAnnunci.pas' {ElencoTipiAnnunciForm},
  TipiAnnunciFrame in 'TipiAnnunciFrame.pas' {FrmTipiAnnunci: TFrame},
  FrmClienteAnnunci in 'FrmClienteAnnunci.pas' {ClienteAnnunciFrame: TFrame},
  SelTestataEdizione in 'SelTestataEdizione.pas' {SelTestataEdizioneForm},
  ContrattoAnnunci in 'ContrattoAnnunci.pas' {ContrattoAnnunciForm},
  FrmContrattiAnnunci in 'FrmContrattiAnnunci.pas' {ContrattiAnnunciFrame: TFrame},
  TipoStrada in 'TipoStrada.pas' {TipoStradaForm},
  SceltaModello in 'SceltaModello.pas' {SceltaModelloForm},
  GestModelliWord in 'GestModelliWord.pas' {GestModelliWordForm},
  SelTabGen in 'SelTabGen.pas' {SelTabGenForm},
  SceltaAnagFile in 'SceltaAnagFile.pas' {SceltaAnagFileForm},
  Contratto in 'Contratto.pas' {ContrattoForm},
  SelContrattoCli in 'SelContrattoCli.pas' {SelContrattoCliForm},
  ModPrezzo in 'ModPrezzo.pas' {ModPrezzoForm},
  GridDB in 'GridDB.pas' {FrameDB: TFrame},
  FrmFrameDB in 'FrmFrameDB.pas' {FrameDBForm},
  UnitPrintMemo in 'UnitPrintMemo.pas',
  FrameDBRichEdit2 in 'FrameDBRichEdit2.pas' {FrameDBRich: TFrame},
  AnagAnnunci in 'AnagAnnunci.pas' {AnagAnnunciForm},
  SelFromQuery in 'SelFromQuery.pas' {SelFromQueryForm},
  Costo in 'Costo.pas' {CostoForm},
  ArchivioMess in 'ArchivioMess.pas' {ArchivioMessForm},
  StoricoTargetList in 'StoricoTargetList.pas' {StoricoTargetListForm},
  NoteCliente in 'NoteCliente.pas' {NoteClienteForm},
  SelQualifContr in 'SelQualifContr.pas' {SelQualifContrForm},
  TabContratti in 'TabContratti.pas' {TabContrattiForm},
  ConoscInfo in 'ConoscInfo.pas' {ConoscInfoForm},
  AnagConoscInfo in 'AnagConoscInfo.pas' {FrmAnagConoscInfo: TFrame},
  CliAltriSettori in 'CliAltriSettori.pas' {CliAltriSettoriForm},
  FrmDatiRetribuz in 'FrmDatiRetribuz.pas' {DatiRetribuzFrame: TFrame},
  EspLavRetrib in 'EspLavRetrib.pas' {EspLavRetribForm},
  LogOperazioniSogg in 'LogOperazioniSogg.pas' {LogOperazioniSoggForm},
  RicFile in 'RicFile.pas' {RicFileForm},
  FrmMultiDBCheckBox in 'FrmMultiDBCheckBox.pas' {MultiDBCheckBoxFrame: TFrame},
  uRelazioni in 'ASA\uRelazioni.pas' {RelazioniForm},
  uASACand in 'ASA\uASACand.pas' {ASASchedaCandForm},
  uAzCandPresenti in 'ASA\uAzCandPresenti.pas' {AzCandPresentiForm},
  uAzOfferte in 'ASA\uAzOfferte.pas' {AzOfferteForm},
  uDettagliCand in 'ASA\uDettagliCand.pas' {DettagliCandForm},
  uASAAzienda in 'ASA\uASAAzienda.pas' {ASAAziendaForm},
  uAnagFile2 in 'ASA\uAnagFile2.pas' {FileSoggetto2Form},
  uASAcommessa in 'ASA\uASAcommessa.pas' {ASAcommessaForm},
  uDettHistory in 'ASA\uDettHistory.pas' {DettHistoryForm},
  uInsEvento in 'ASA\uInsEvento.pas' {ASAInsEventoForm},
  AnagraficaExt in 'Suitex\AnagraficaExt.pas' {FrmSUITEXAnagraficaExt: TFrame},
  ClientiExt in 'Suitex\ClientiExt.pas' {FrmSUITEXClientiExt: TFrame},
  uAnagFile in 'ASA\uAnagFile.pas' {FileSoggettoForm},
  uAziendaFile in 'ASA\uAziendaFile.pas' {FileAziendaForm};

{$R *.RES}

begin
  {Attempt to create a named mutex}
  CreateMutex(nil, false, 'H1Sel');
  {if it failed then there is another instance}
  if GetLastError = ERROR_ALREADY_EXISTS then begin
    {Send all windows our custom message - only our other}
    {instance will recognise it, and restore itself}
    SendMessage(HWND_BROADCAST,
      RegisterWindowMessage('H1Sel'),
      0,
      0);
    {Lets quit}
    Halt(0);
  end;

  SplashForm := TSplashForm.Create(nil);
  SplashForm.Prog.Position := 0;
  SplashForm.Show;
  SplashForm.Update;

  Application.Initialize;
  {Tell Delphi to un-hide it's hidden application window}
  {This allows our instance to have a icon on the task bar}
  Application.ShowMainForm := true;
  ShowWindow(Application.Handle, SW_RESTORE);

  Application.Title := 'H1sel - Modulo Gestione Ricerca e Selezione';
  Application.HelpFile := 'Manh1sel.hlp';
  SplashForm.Prog.Position := 1;
  Application.CreateForm(TData, Data);
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TFrameDBForm, FrameDBForm);

  SplashForm.Prog.Position := 2;
  //Application.CreateForm(TData, Data);
  SplashForm.Prog.Position := 3;
  Application.CreateForm(TCurriculumForm, CurriculumForm);
  SplashForm.Prog.Position := 4;
  //ShowMessage('1');
  Application.CreateForm(TData2, Data2);
  //ShowMessage('2');
  SplashForm.Prog.Position := 5;
  SplashForm.Prog.Position := 6;
  //Application.CreateForm(TData2, Data2);
  SplashForm.Prog.Position := 7;
  Application.CreateForm(TDataRicerche, DataRicerche);
  SplashForm.Prog.Position := 8;
  Application.CreateForm(TDataAnnunci, DataAnnunci);
  SplashForm.Prog.Position := 9;
  Application.CreateForm(TLoginForm, LoginForm);
  SplashForm.Hide;
  SplashForm.Free;

  LoginForm.Accedi := 'N';
  LoginForm.Username.Text := Data.GlobalLastLogin.Value;
  LoginForm.ShowModal;
  if LoginForm.Accedi = 'S' then begin
    Application.Run
  end else Application.Terminate;
end.

