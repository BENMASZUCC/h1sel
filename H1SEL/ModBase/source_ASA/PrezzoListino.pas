unit PrezzoListino;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     StdCtrls,Buttons,RXSpin,Mask,ToolEdit,CurrEdit,DtEdit97,ExtCtrls;

type
     TPrezzoListinoForm=class(TForm)
          BitBtn1: TBitBtn;
          BitBtn2: TBitBtn;
          Label3: TLabel;
          Label4: TLabel;
          Label5: TLabel;
          ListinoLire: TRxCalcEdit;
          AnoiLire: TRxCalcEdit;
          AlClienteLire: TRxCalcEdit;
          ListinoEuro: TRxCalcEdit;
          AnoiEuro: TRxCalcEdit;
          AlClienteEuro: TRxCalcEdit;
          GroupBox1: TGroupBox;
          Label1: TLabel;
          Label2: TLabel;
          RxSpinEdit1: TRxSpinEdit;
          RxSpinEdit2: TRxSpinEdit;
          Label6: TLabel;
          Label7: TLabel;
          Bevel1: TBevel;
          Bevel2: TBevel;
          Label8: TLabel;
          DEDallaData: TDateEdit97;
          Label9: TLabel;
          ENote: TEdit;
          procedure ListinoLireChange(Sender: TObject);
          procedure AnoiLireChange(Sender: TObject);
          procedure AlClienteLireChange(Sender: TObject);
          procedure FormShow(Sender: TObject);
    procedure ListinoEuroChange(Sender: TObject);
    procedure AnoiEuroChange(Sender: TObject);
    procedure AlClienteEuroChange(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     PrezzoListinoForm: TPrezzoListinoForm;

implementation

{$R *.DFM}

procedure TPrezzoListinoForm.ListinoLireChange(Sender: TObject);
begin
     if ListinoLire.Focused then
          ListinoEuro.Value:=ListinoLire.value/1936.27;
end;

procedure TPrezzoListinoForm.AnoiLireChange(Sender: TObject);
begin
     if AnoiLire.Focused then
          AnoiEuro.Value:=AnoiLire.value/1936.27;
end;

procedure TPrezzoListinoForm.AlClienteLireChange(Sender: TObject);
begin
     if AlClienteLire.Focused then
          AlClienteEuro.Value:=AlClienteLire.value/1936.27;
end;

procedure TPrezzoListinoForm.FormShow(Sender: TObject);
begin
     Caption:='[M/3500] - '+caption;
end;

procedure TPrezzoListinoForm.ListinoEuroChange(Sender: TObject);
begin
     if ListinoEuro.Focused then
          ListinoLire.Value:=ListinoEuro.value*1936.27;
end;

procedure TPrezzoListinoForm.AnoiEuroChange(Sender: TObject);
begin
     if AnoiEuro.Focused then
          AnoiLire.Value:=AnoiEuro.value*1936.27;
end;

procedure TPrezzoListinoForm.AlClienteEuroChange(Sender: TObject);
begin
     if AlClienteEuro.Focused then
          AlClienteLire.Value:=AlClienteEuro.value*1936.27;
end;

end.

