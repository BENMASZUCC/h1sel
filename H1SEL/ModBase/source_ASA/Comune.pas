unit Comune;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons;

type
  TComuneForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    EDesc: TEdit;
    EProv: TEdit;
    ECap: TEdit;
    EPrefisso: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EReg: TEdit;
    Label6: TLabel;
    EAreaNielsen: TEdit;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ComuneForm: TComuneForm;

implementation

{$R *.DFM}

procedure TComuneForm.FormShow(Sender: TObject);
begin
     Caption:='[S/18] - '+Caption;
end;

end.
