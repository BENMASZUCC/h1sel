object EliminaDipOrgForm: TEliminaDipOrgForm
  Left = 328
  Top = 106
  ActiveControl = GroupBox1
  BorderStyle = bsDialog
  Caption = 'Eliminazione soggetto dall'#39'organigramma'
  ClientHeight = 149
  ClientWidth = 418
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 4
    Top = 70
    Width = 26
    Height = 13
    Caption = 'Data:'
  end
  object Label3: TLabel
    Left = 4
    Top = 109
    Width = 55
    Height = 13
    Caption = 'Annotazioni'
  end
  object BitBtn1: TBitBtn
    Left = 320
    Top = 2
    Width = 95
    Height = 39
    TabOrder = 3
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 320
    Top = 43
    Width = 95
    Height = 39
    Caption = 'Annulla'
    TabOrder = 4
    Kind = bkCancel
  end
  object ENote: TEdit
    Left = 4
    Top = 124
    Width = 305
    Height = 21
    MaxLength = 80
    TabOrder = 2
  end
  object DEData: TDateEdit97
    Left = 4
    Top = 85
    Width = 108
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    ColorCalendar.ColorValid = clBlue
    DayNames.Monday = 'lu'
    DayNames.Tuesday = 'ma'
    DayNames.Wednesday = 'me'
    DayNames.Thursday = 'gi'
    DayNames.Friday = 've'
    DayNames.Saturday = 'sa'
    DayNames.Sunday = 'do'
    MonthNames.January = 'gennaio'
    MonthNames.February = 'febbraio'
    MonthNames.March = 'marzo'
    MonthNames.April = 'aprile'
    MonthNames.May = 'maggio'
    MonthNames.June = 'giugno'
    MonthNames.July = 'luglio'
    MonthNames.August = 'agosto'
    MonthNames.September = 'settembre'
    MonthNames.October = 'ottobre'
    MonthNames.November = 'novembre'
    MonthNames.December = 'dicembre'
    Options = [doButtonTabStop, doCanPopup, doIsMasked, doShowCancel, doShowToday]
  end
  object GroupBox1: TGroupBox
    Left = 4
    Top = 2
    Width = 205
    Height = 64
    Caption = 'Autorizzato da:'
    TabOrder = 0
    object SpeedButton1: TSpeedButton
      Left = 171
      Top = 14
      Width = 28
      Height = 26
      Hint = 'seleziona soggetto'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        33333333333333333333333333333333333333333333333333FF333333333333
        3000333333FFFFF3F77733333000003000B033333777773777F733330BFBFB00
        E00033337FFF3377F7773333000FBFB0E000333377733337F7773330FBFBFBF0
        E00033F7FFFF3337F7773000000FBFB0E000377777733337F7770BFBFBFBFBF0
        E00073FFFFFFFF37F777300000000FB0E000377777777337F7773333330BFB00
        000033333373FF77777733333330003333333333333777333333333333333333
        3333333333333333333333333333333333333333333333333333333333333333
        3333333333333333333333333333333333333333333333333333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = SpeedButton1Click
    end
    object ECogn: TEdit
      Left = 7
      Top = 15
      Width = 161
      Height = 21
      ReadOnly = True
      TabOrder = 0
    end
    object ENome: TEdit
      Left = 7
      Top = 37
      Width = 161
      Height = 21
      TabOrder = 1
    end
  end
end
