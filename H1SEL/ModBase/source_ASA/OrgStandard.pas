unit OrgStandard;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     DBTables,Db,ExtCtrls,dxorgchr,dxdborgc, StdCtrls, Buttons, TB97,
  DBCtrls, Mask, ImgList, dxPSCore, dxPSdxOCLnk, dxPSdxDBOCLnk,
  dxPSGraphicLnk;

type
     TOrgStandardForm=class(TForm)
          Panel1: TPanel;
          QOrgStandard: TQuery;
          DsQOrgStandard: TDataSource;
          UpdOrgStandard: TUpdateSQL;
          DBTree: TdxDbOrgChart;
          QOrgStandardID: TAutoIncField;
          QOrgStandardTipo: TStringField;
          QOrgStandardDescrizione: TStringField;
          QOrgStandardDescBreve: TStringField;
          QOrgStandardNodoPadre: TIntegerField;
          QOrgStandardInterim: TBooleanField;
          QOrgStandardInStaff: TBooleanField;
          QOrgStandardPARENT: TIntegerField;
          QOrgStandardWIDTH: TIntegerField;
          QOrgStandardHEIGHT: TIntegerField;
          QOrgStandardTYPE: TStringField;
          QOrgStandardCOLOR: TIntegerField;
          QOrgStandardIMAGE: TIntegerField;
          QOrgStandardIMAGEALIGN: TStringField;
          QOrgStandardORDINE: TIntegerField;
          QOrgStandardALIGN: TStringField;
    BitBtn1: TBitBtn;
    BOrgNewNodo: TToolbarButton97;
    BOrgNewNewChildNodo: TToolbarButton97;
    BOrgNewDelNodo: TToolbarButton97;
    BOrgNewStampa: TToolbarButton97;
    Panel2: TPanel;
    Splitter1: TSplitter;
    Panel109: TPanel;
    Panel94: TPanel;
    TbBOrganigOK: TToolbarButton97;
    TbBOrganigCan: TToolbarButton97;
    TbBOrganigMod: TToolbarButton97;
    PanOrgDett: TPanel;
    Label56: TLabel;
    Label57: TLabel;
    Label58: TLabel;
    DBEdit45: TDBEdit;
    DBEdit46: TDBEdit;
    DBComboBox4: TDBComboBox;
    Panel98: TPanel;
    DBCheckBox6: TDBCheckBox;
    Panel99: TPanel;
    DBCheckBox5: TDBCheckBox;
    GroupBox12: TGroupBox;
    Label75: TLabel;
    Label76: TLabel;
    Label77: TLabel;
    Label78: TLabel;
    Label79: TLabel;
    Label80: TLabel;
    DBEdit60: TDBEdit;
    DBEdit61: TDBEdit;
    DBComboBox5: TDBComboBox;
    PColor: TPanel;
    DBComboBox6: TDBComboBox;
    DBEdit62: TDBEdit;
    ImageList2: TImageList;
    ColorDialog: TColorDialog;
    dxPrinter1: TdxComponentPrinter;
    dxPrinter1Link1: TdxDBOrgChartReportLink;
          procedure QOrgStandardAfterInsert(DataSet: TDataSet);
          procedure QOrgStandardAfterPost(DataSet: TDataSet);
          procedure DBTreeCreateNode(Sender: TObject; Node: TdxOcNode);
    procedure BOrgNewNodoClick(Sender: TObject);
    procedure BOrgNewNewChildNodoClick(Sender: TObject);
    procedure BOrgNewDelNodoClick(Sender: TObject);
    procedure TbBOrganigModClick(Sender: TObject);
    procedure TbBOrganigOKClick(Sender: TObject);
    procedure TbBOrganigCanClick(Sender: TObject);
    procedure DsQOrgStandardStateChange(Sender: TObject);
    procedure PColorClick(Sender: TObject);
    procedure QOrgStandardAfterScroll(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure BOrgNewStampaClick(Sender: TObject);
     private
          function GetShape(ShapeName: string): TdxOcShape;
          function GetNodeAlign(AlignName: string): TdxOcNodeAlign;
          function GetImageAlign(AlignName: string): TdxOcImageAlign;
     public
          { Public declarations }
     end;

var
     OrgStandardForm: TOrgStandardForm;

implementation

uses ModuloDati;

{$R *.DFM}

procedure TOrgStandardForm.QOrgStandardAfterInsert(DataSet: TDataSet);
begin
     with QOrgStandard,DBTree do begin
          QOrgStandardInterim.Value:=False;
          QOrgStandardInStaff.value:=False;
          FindField('Height').AsInteger:=DefaultNodeHeight;
          FindField('Width').AsInteger:=DefaultNodeWidth;
          FindField('Type').AsString:='Rectangle';
          FindField('Color').AsInteger:=clWhite;
          FindField('Image').AsInteger:=-1;
          FindField('ImageAlign').AsString:='Left-Top';
          FindField('Align').AsString:='Center';
     end;
end;

procedure TOrgStandardForm.QOrgStandardAfterPost(DataSet: TDataSet);
begin
     QOrgStandard.ApplyUpdates;
end;

procedure TOrgStandardForm.DBTreeCreateNode(Sender: TObject; Node: TdxOcNode);
begin
     with Node,QOrgStandard do
     begin
          if FindField('width').AsInteger>50 then
               Width:=FindField('width').AsInteger;
          if FindField('height').AsInteger>50 then
               Height:=FindField('height').AsInteger;
          Shape:=GetShape(FindField('type').AsString);
          Color:=FindField('color').AsInteger;
          Node.ChildAlign:=GetNodeAlign(FindField('Align').AsString);
          Node.ImageAlign:=GetImageAlign(FindField('ImageAlign').AsString);
     end;
end;

function TOrgStandardForm.GetShape(ShapeName: string): TdxOcShape;
const ShapeArray: array[0..3] of string=('Rectange','Round Rect','Ellipse','Diamond');
var i: integer;
begin
     Result:=TdxOcShape(0);
     for i:=0 to 3 do
          if AnsiUpperCase(ShapeArray[i])=AnsiUpperCase(ShapeName) then begin
               Result:=TdxOcShape(i);
               break;
          end;
end;

function TOrgStandardForm.GetNodeAlign(AlignName: string): TdxOcNodeAlign;
const AlignArray: array[0..2] of string=('Left','Center','Right');
var i: integer;
begin
     Result:=TdxOcNodeAlign(0);
     for i:=0 to 3 do
          if AnsiUpperCase(AlignArray[i])=AnsiUpperCase(AlignName) then begin
               Result:=TdxOcNodeAlign(i);
               break;
          end;
end;

function TOrgStandardForm.GetImageAlign(AlignName: string): TdxOcImageAlign;
const AlignArray: array[0..12] of string=(
          'None',
          'Left-Top','Left-Center','Left-Bottom',
          'Right-Top','Right-Center','Right-Bottom',
          'Top-Left','Top-Center','Top-Right',
          'Bottom-Left','Bottom-Center','Bottom-Right'
          );
var i: integer;
begin
     Result:=TdxOcImageAlign(0);
     for i:=0 to 12 do
          if AnsiUpperCase(AlignArray[i])=AnsiUpperCase(AlignName) then begin
               Result:=TdxOcImageAlign(i);
               break;
          end;
end;

procedure TOrgStandardForm.BOrgNewNodoClick(Sender: TObject);
var Node: TdxOcNode;
     xIDAzienda: integer;
begin
     Node:=DBTree.AddFirst(nil,nil);
     QOrgStandard.Close;
     QOrgStandard.Open;
     DBTree.FullExpand;
end;

procedure TOrgStandardForm.BOrgNewNewChildNodoClick(Sender: TObject);
var Node: TdxOcNode;
     I: integer;
begin
     QOrgStandard.DisableControls;
     if (DBTree.Selected<>nil) then
          Node:=DBTree.AddChild(DBTree.Selected,nil)
     else Node:=DBTree.Add(nil,nil);
     QOrgStandard.Close;
     QOrgStandard.Open;
     QOrgStandard.EnableControls;
     DBTree.FullExpand;
end;

procedure TOrgStandardForm.BOrgNewDelNodoClick(Sender: TObject);
begin
     if QOrgStandard.RecordCount=0 then exit;
     // controllo esistenza sotto-nodi
     if Dbtree.Selected.HasChildren then begin
          MessageDlg('Il nodo selezionato possiede sotto-nodi: impossibile cancellarlo',mtError, [mbOK],0);
          exit;
     end;
     if MessageDlg('Sei sicuro di voler eliminare il nodo selezionato ?',mtWarning, [mbNo,mbYes],0)=mrNo then exit;
     QOrgStandard.DisableControls;
     with Data do begin
          DB.BeginTrans;
          try
               Q1.Close;
               Q1.SQL.text:='delete from OrgStandard where ID='+QOrgStandardID.AsString;
               Q1.ExecSQL;
               DB.CommitTrans;
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
          end;
     end;
     QOrgStandard.Close;
     QOrgStandard.Open;
     QOrgStandard.EnableControls;
     DBTree.FullExpand;
end;

procedure TOrgStandardForm.TbBOrganigModClick(Sender: TObject);
begin
     QOrgStandard.Edit;
end;

procedure TOrgStandardForm.TbBOrganigOKClick(Sender: TObject);
begin
     with QOrgStandard do begin
          Data.DB.BeginTrans;
          try
               ApplyUpdates;
               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               raise;
          end;
          CommitUpdates;
          Close;
          Open;
          DBTree.FullExpand;
     end;
end;

procedure TOrgStandardForm.TbBOrganigCanClick(Sender: TObject);
begin
     QOrgstandard.Cancel;
end;

procedure TOrgStandardForm.DsQOrgStandardStateChange(Sender: TObject);
var b:boolean;
begin
     b:=DsQOrgStandard.State in [dsEdit,dsInsert];
     TbBOrganigMod.Enabled:=not b;
     PanOrgDett.Enabled:=b;
     TbBOrganigOK.Enabled:=b;
     TbBOrganigCan.Enabled:=b;
end;

procedure TOrgStandardForm.PColorClick(Sender: TObject);
begin
     if ColorDialog.Execute then begin
          PColor.Color:=ColorDialog.Color;
          QOrgStandard.Edit;
          QOrgStandardCOLOR.Value:=PColor.Color;
          DBTree.Selected.Color:=PColor.Color;
     end;
end;

procedure TOrgStandardForm.QOrgStandardAfterScroll(DataSet: TDataSet);
begin
     PColor.Color:=QOrgStandardCOLOR.Value;
end;

procedure TOrgStandardForm.FormShow(Sender: TObject);
begin
     Caption:='[S/47] - '+Caption;
     PColor.Color:=QOrgStandardCOLOR.Value;
end;

procedure TOrgStandardForm.BOrgNewStampaClick(Sender: TObject);
begin
     dxPrinter1.Preview(True,nil);
end;

end.

