unit SceltaModelloMailCli;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     StdCtrls,Buttons,ExtCtrls,Db,DBTables,Grids,DBGrids;

type
     TSceltaModelloMailCliForm=class(TForm)
          BitBtn1: TBitBtn;
          BitBtn2: TBitBtn;
          Q: TQuery;
          GroupBox1: TGroupBox;
          Label2: TLabel;
          Label3: TLabel;
          Label10: TLabel;
          Label11: TLabel;
          Bevel1: TBevel;
          Label12: TLabel;
          EHost: TEdit;
          EPort: TEdit;
          EUserID: TEdit;
          EName: TEdit;
          EAddress: TEdit;
          Panel13: TPanel;
          DBGrid2: TDBGrid;
          Panel14: TPanel;
          QTipiMailContattiCli: TQuery;
          DsQTipiMailContattiCli: TDataSource;
          QTipiMailContattiCliID: TAutoIncField;
          QTipiMailContattiCliDescrizione: TStringField;
          QTipiMailContattiCliOggetto: TStringField;
          QTipiMailContattiCliIntestazione: TMemoField;
          QTipiMailContattiCliCorpo: TMemoField;
          CBStoricoInvio: TCheckBox;
    BModTesti: TSpeedButton;
    SpeedButton1: TSpeedButton;
          procedure FormShow(Sender: TObject);
    procedure BModTestiClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
     private
          procedure CaricaInfoConnect;
     public
          { Public declarations }
     end;

var
     SceltaModelloMailCliForm: TSceltaModelloMailCliForm;

implementation

uses Main, TipiMailCand, EmailConnectInfo, ModuloDati;

{$R *.DFM}


procedure TSceltaModelloMailCliForm.CaricaInfoConnect;
var xInfo: string;
     xCRpos: integer;
begin
     // caricamento info connessione SMTP
     Q.Close;
     Q.SQL.text:='select Nominativo,EmailConnectInfo '+
          'from Users where ID='+IntToStr(MainForm.xIDUtenteAttuale);
     Q.Open;
     // host
     xInfo:=Q.FieldByName('EmailConnectInfo').asString;
     if xInfo='' then begin
          EHost.text:='';
          EPort.text:='';
          EUserID.text:='';
          EName.text:='';
          EAddress.text:='';
     end else begin
          xCRpos:=pos(chr(13),xInfo);
          EHost.text:=copy(xInfo,1,xCRpos-1);
          // port
          xinfo:=copy(xInfo,xCRpos+1,length(xInfo));
          xCRpos:=pos(chr(13),xInfo);
          EPort.text:=copy(xInfo,1,xCRpos-1);
          // userID
          xinfo:=copy(xInfo,xCRpos+1,length(xInfo));
          xCRpos:=pos(chr(13),xInfo);
          EUserID.text:=copy(xInfo,1,xCRpos-1);
          // name
          xinfo:=copy(xInfo,xCRpos+1,length(xInfo));
          xCRpos:=pos(chr(13),xInfo);
          EName.text:=copy(xInfo,1,xCRpos-1);
          // address
          xinfo:=copy(xInfo,xCRpos+1,length(xInfo));
          EAddress.text:=xInfo;
     end;
end;

procedure TSceltaModelloMailCliForm.FormShow(Sender: TObject);
begin
     CaricaInfoConnect;
     Caption:='[S/5] - '+caption;
end;

procedure TSceltaModelloMailCliForm.BModTestiClick(Sender: TObject);
begin
     TipiMailCandForm:=TTipiMailCandForm.Create(self);
     TipiMailCandForm.ShowModal;
     TipiMailCandForm.Free;
     QTipiMailContattiCli.Close;
     QTipiMailContattiCli.Open;
end;

procedure TSceltaModelloMailCliForm.SpeedButton1Click(Sender: TObject);
var xInfo,xFirma: string;
     xCRpos,i: integer;
begin
     EmailConnectInfoForm:=TEmailConnectInfoForm.create(self);
     EmailConnectInfoForm.xIDUser:=MainForm.xIDUtenteAttuale;
     EmailConnectInfoForm.ShowModal;
     if EmailConnectInfoForm.ModalResult=mrOK then begin
          // aggiornamento database
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text:='update Users set EmailConnectInfo=:xEmailConnectInfo, FirmaMail=:xFirmaMail '+
                         'where ID='+IntToStr(MainForm.xIDUtenteAttuale);
                    Q1.ParamByName('xEmailConnectInfo').asString:=EmailConnectInfoForm.EHost.text+chr(13)+
                         EmailConnectInfoForm.EPort.text+chr(13)+
                         EmailConnectInfoForm.EUserID.text+chr(13)+
                         EmailConnectInfoForm.EName.text+chr(13)+
                         EmailConnectInfoForm.EAddress.text;
                    xFirma:='';
                    for i:=0 to EmailConnectInfoForm.Memo1.lines.count-1 do
                         xFirma:=xFirma+EmailConnectInfoForm.Memo1.lines[i]+chr(13);
                    Q1.ParamByName('xFirmaMail').asString:=xFirma;
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    // RIcaricamento info connessione SMTP
                    Q.Close;
                    Q.SQL.text:='select Nominativo,EmailConnectInfo '+
                         'from Users where ID='+IntToStr(MainForm.xIDUtenteAttuale);
                    Q.Open;
                    // host
                    xInfo:=Q.FieldByName('EmailConnectInfo').asString;
                    if xInfo='' then begin
                         EHost.text:='';
                         EPort.text:='';
                         EUserID.text:='';
                         EName.text:='';
                         EAddress.text:='';
                    end else begin
                         xCRpos:=pos(chr(13),xInfo);
                         EHost.text:=copy(xInfo,1,xCRpos-1);
                         // port
                         xinfo:=copy(xInfo,xCRpos+1,length(xInfo));
                         xCRpos:=pos(chr(13),xInfo);
                         EPort.text:=copy(xInfo,1,xCRpos-1);
                         // userID
                         xinfo:=copy(xInfo,xCRpos+1,length(xInfo));
                         xCRpos:=pos(chr(13),xInfo);
                         EUserID.text:=copy(xInfo,1,xCRpos-1);
                         // name
                         xinfo:=copy(xInfo,xCRpos+1,length(xInfo));
                         xCRpos:=pos(chr(13),xInfo);
                         EName.text:=copy(xInfo,1,xCRpos-1);
                         // address
                         xinfo:=copy(xInfo,xCRpos+1,length(xInfo));
                         EAddress.text:=xInfo;
                    end;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               end;
          end;
     end;
     EmailConnectInfoForm.Free;
end;

end.

