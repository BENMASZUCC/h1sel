�
 TINSERIMENTOFORM 0w  TPF0TInserimentoFormInserimentoFormLeftTop� Width�Height�ActiveControlDBGrid1CaptionInserimento in aziendaColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderOnShowFormShowPixelsPerInch`
TextHeight TPanelPanel1Left Top Width�Height+AlignalTopTabOrder  TPanelPanel7LeftTopWidth� Height)AlignalLeft
BevelOuterbvNoneEnabledTabOrder  TDBEditDBEdit12Left	Top
WidthtHeightColorclYellow	DataFieldCognome
DataSourceData.DsAnagraficaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontReadOnly	TabOrder   TDBEditDBEdit14LeftTop
WidthcHeightColorclYellow	DataFieldNome
DataSourceData.DsAnagraficaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontReadOnly	TabOrder   TBitBtnBitBtn1Left� TopWidthWHeightTabOrderKindbkOK  TBitBtnBitBtn2LeftCTopWidthWHeightCaptionAnnullaTabOrderKindbkCancel   TPanel
PanClientiLeft Top+Width�Height�AlignalClient
BevelInner	bvLoweredTabOrder TPanelPanel12LeftTopWidth�HeightAlignalTop	AlignmenttaLeftJustifyCaption  Selezionare il clienteColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   TDBGridDBGrid1LeftTopWidth�Height�AlignalClient
DataSourceDsEBCclientiReadOnly	TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style 
OnKeyPressDBGrid1KeyPressColumnsExpanded	FieldNameDescrizioneTitle.CaptionDenominazione clienteWidth� Visible	 Expanded	FieldNameComuneTitle.CaptionComune/Localit�Width� Visible	     TTableTEBCclientiActive	DatabaseNameEBCDB	IndexNameIdxDescrizioneReadOnly		TableNamedbo.EBC_ClientiLeftTop�  TAutoIncFieldTEBCclientiID	FieldNameIDReadOnly	  TStringFieldTEBCclientiDescrizione	FieldNameDescrizioneSize  TStringFieldTEBCclientiIndirizzo	FieldName	IndirizzoSize(  TStringFieldTEBCclientiCap	FieldNameCapSize  TStringFieldTEBCclientiComune	FieldNameComune  TStringFieldTEBCclientiProvincia	FieldName	ProvinciaSize  TIntegerFieldTEBCclientiIDAttivita	FieldName
IDAttivita   TDataSourceDsEBCclientiDataSetTEBCclientiLeftTop   TTableTAnagInserimDatabaseNameEBCDB	TableNamedbo.EBC_InserimentiLeft8Top TIntegerFieldTAnagInserimIDAnagrafica	FieldNameIDAnagraficaRequired	  TIntegerFieldTAnagInserimIDCliente	FieldName	IDClienteRequired	  TDateTimeFieldTAnagInserimDallaData	FieldName	DallaData  TDateTimeFieldTAnagInserimAllaData	FieldNameAllaData  TIntegerFieldTAnagInserimIDRicerca	FieldName	IDRicerca  TStringFieldTAnagInserimMotivoCessazione	FieldNameMotivoCessazioneSize2  
TMemoFieldTAnagInserimNote	FieldNameNoteBlobTypeftMemoSize  TAutoIncFieldTAnagInserimID	FieldNameIDReadOnly	    