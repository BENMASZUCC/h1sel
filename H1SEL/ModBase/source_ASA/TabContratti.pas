unit TabContratti;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     TB97,dxDBTLCl,dxGrClms,dxTL,dxDBCtrl,dxDBGrid,dxCntner,Db,
     DBTables,StdCtrls,Buttons,ExtCtrls,dxLayout,dxGrClEx;

type
     TTabContrattiForm=class(TForm)
          Panel1: TPanel;
          BitBtn1: TBitBtn;
          QQualifContr: TQuery;
          DsQQualifContr: TDataSource;
          QTipiContrattiLK: TQuery;
          QTipiContrattiLKID: TAutoIncField;
          QTipiContrattiLKTipoContratto: TStringField;
          QQualifContrID: TAutoIncField;
          QQualifContrIDTipoContratto: TIntegerField;
          QQualifContrQualifica: TStringField;
          QQualifContrOrdine: TSmallintField;
          QQualifContrtipoContratto: TStringField;
          Panel2: TPanel;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1ID: TdxDBGridMaskColumn;
          dxDBGrid1IDTipoContratto: TdxDBGridMaskColumn;
          dxDBGrid1tipoContratto: TdxDBGridLookupColumn;
          dxDBGrid1Qualifica: TdxDBGridMaskColumn;
          dxDBGrid1Ordine: TdxDBGridMaskColumn;
          Panel3: TPanel;
          BNew: TToolbarButton97;
          BDel: TToolbarButton97;
          BCan: TToolbarButton97;
          BOK: TToolbarButton97;
          Splitter1: TSplitter;
          Panel4: TPanel;
          QTipiContratti: TQuery;
          DsQTipiContratti: TDataSource;
          Panel5: TPanel;
          BTipoNew: TToolbarButton97;
          BTipoDel: TToolbarButton97;
          BTipoCan: TToolbarButton97;
          BTipoOK: TToolbarButton97;
          dxDBGrid2: TdxDBGrid;
          QTipiContrattiID: TAutoIncField;
          QTipiContrattiTipoContratto: TStringField;
          dxDBGrid2ID: TdxDBGridMaskColumn;
          dxDBGrid2TipoContratto: TdxDBGridMaskColumn;
          procedure DsQQualifContrStateChange(Sender: TObject);
          procedure QQualifContrAfterPost(DataSet: TDataSet);
          procedure BNewClick(Sender: TObject);
          procedure BDelClick(Sender: TObject);
          procedure BOKClick(Sender: TObject);
          procedure BCanClick(Sender: TObject);
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
          procedure QQualifContrBeforePost(DataSet: TDataSet);
          procedure DsQTipiContrattiStateChange(Sender: TObject);
          procedure BTipoNewClick(Sender: TObject);
          procedure BTipoDelClick(Sender: TObject);
          procedure BTipoOKClick(Sender: TObject);
          procedure BTipoCanClick(Sender: TObject);
          procedure QTipiContrattiAfterPost(DataSet: TDataSet);
    procedure BitBtn1Click(Sender: TObject);
     private
          xID: integer;
     public
          { Public declarations }
     end;

var
     TabContrattiForm: TTabContrattiForm;

implementation

uses uUtilsVarie;

{$R *.DFM}

procedure TTabContrattiForm.DsQQualifContrStateChange(Sender: TObject);
var b: boolean;
begin
     b:=DsQQualifContr.State in [dsEdit,dsInsert];
     BNew.Enabled:=not b;
     BDel.Enabled:=not b;
     BOK.Enabled:=b;
     BCan.Enabled:=b;
end;

procedure TTabContrattiForm.QQualifContrAfterPost(DataSet: TDataSet);
begin
     QQualifContr.Close;
     QQualifContr.Open;
     QQualifContr.Locate('ID',xID, []);
end;

procedure TTabContrattiForm.BNewClick(Sender: TObject);
begin
     QQualifContr.Insert;
end;

procedure TTabContrattiForm.BDelClick(Sender: TObject);
begin
     if QQualifContr.IsEmpty then exit;
     if MessageDlg('Sei sicuro di voler cancellare ?',mtWarning, [mbYes,mbNo],0)=mrNo then exit;
     QQualifContr.Delete;
end;

procedure TTabContrattiForm.BOKClick(Sender: TObject);
begin
     QQualifContr.Post;
end;

procedure TTabContrattiForm.BCanClick(Sender: TObject);
begin
     QQualifContr.cancel;
end;

procedure TTabContrattiForm.FormClose(Sender: TObject;
     var Action: TCloseAction);
begin
     if DsQQualifContr.state in [dsinsert,dsedit] then QQualifContr.Post;
end;

procedure TTabContrattiForm.QQualifContrBeforePost(DataSet: TDataSet);
begin
     xID:=QQualifContrID.Value;
end;

procedure TTabContrattiForm.DsQTipiContrattiStateChange(Sender: TObject);
var b: boolean;
begin
     b:=DsQTipiContratti.State in [dsEdit,dsInsert];
     BTipoNew.Enabled:=not b;
     BTipoDel.Enabled:=not b;
     BTipoOK.Enabled:=b;
     BTipoCan.Enabled:=b;
end;

procedure TTabContrattiForm.BTipoNewClick(Sender: TObject);
begin
     QTipiContratti.Insert;
end;

procedure TTabContrattiForm.BTipoDelClick(Sender: TObject);
begin
     if QTipiContratti.IsEmpty then exit;
     if MessageDlg('Sei sicuro di voler cancellare ?',mtWarning, [mbYes,mbNo],0)=mrNo then exit;
     if VerifIntegrRef('QualificheContrattuali','IDTipoContratto',QTipiContrattiID.AsString) then
          QTipiContratti.Delete;
end;

procedure TTabContrattiForm.BTipoOKClick(Sender: TObject);
begin
     QTipiContratti.Post;
end;

procedure TTabContrattiForm.BTipoCanClick(Sender: TObject);
begin
     QTipiContratti.Cancel;
end;

procedure TTabContrattiForm.QTipiContrattiAfterPost(DataSet: TDataSet);
begin
     QTipiContratti.Close;
     QTipiContratti.Open;
     QTipiContrattiLK.Close;
     QTipiContrattiLK.Open;
end;

procedure TTabContrattiForm.BitBtn1Click(Sender: TObject);
begin
     if DsQQualifContr.state in [dsInsert,dsEdit] then QQualifContr.Post;
     if DsQTipiContratti.state in [dsInsert,dsEdit] then QTipiContratti.Post;
end;

end.

