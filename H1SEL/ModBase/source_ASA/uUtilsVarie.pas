unit uUtilsVarie;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,Registry,
     StdCtrls,Buttons,Grids,DBGrids,Mask,DBCtrls,ExtCtrls,Db,DBTables,FrmFrameDB,
     ComObj,ModuloDati,ModuloDati2,Main,MDRicerche,SelTabGen,SceltaAnagFile,View,ShellApi;

const xMax=99999;
const xAnagSQL='SELECT Anagrafica.ID, Matricola, Cognome, Nome, Titolo,'+chr(13)+
     'DataNascita, GiornoNascita, MeseNascita, LuogoNascita,'+chr(13)+
          'Sesso, Indirizzo, Cap, Comune, IDComuneRes, IDZonaRes,'+chr(13)+
          'Provincia, DomicilioIndirizzo, DomicilioCap, DomicilioComune,'+chr(13)+
          'IDComuneDom, IDZonaDom, DomicilioProvincia, DomicilioStato,'+chr(13)+
          'RecapitiTelefonici, Cellulare, Fax, CodiceFiscale, PartitaIVA,'+chr(13)+
          'Email, IDStatoCivile, Anagrafica.IDStato, Anagrafica.IDTipoStato,'+chr(13)+
          'CVNumero, CVIDAnnData, CVinseritoInData, Foto, IDTipologia, '+chr(13)+
          'IDUtenteMod, DubbiIns, IDProprietaCV, TelUfficio, OldCVNumero, '+chr(13)+
          'EBC_Stati.Stato DescStato, TipoStrada, DomicilioTipoStrada, NumCivico, DomicilioNumCivico, '+chr(13)+
          'IDUtente, Anagrafica.Stato, LibPrivacy, CVDataReale, CVDataAgg '+chr(13)+
          'FROM Anagrafica,EBC_Stati '+chr(13)+
          'WHERE Anagrafica.IDStato=EBC_Stati.ID';
const xClientiSQL='SELECT EBC_Clienti.ID, EBC_Clienti.Descrizione, '+
     'EBC_Clienti.Stato, EBC_Clienti.Indirizzo, EBC_Clienti.Cap, '+
          'EBC_Clienti.Comune, EBC_Clienti.Provincia, EBC_Clienti.IDAttivita, '+
          'EBC_Clienti.Telefono, EBC_Clienti.Fax, EBC_Clienti.PartitaIVA, '+
          'EBC_Clienti.CodiceFiscale, EBC_Clienti.BancaAppoggio, '+
          'EBC_Clienti.SistemaPagamento, EBC_Clienti.NoteContratto, '+
          'EBC_Clienti.Responsabile, EBC_Clienti.ConosciutoInData, '+
          'EBC_Clienti.IndirizzoLegale, EBC_Clienti.CapLegale, '+
          'EBC_Clienti.ComuneLegale, EBC_Clienti.ProvinciaLegale, '+
          'Ebc_attivita.Attivita,EBC_Clienti.CartellaDoc, '+
          'EBC_Clienti.TipoStrada, EBC_Clienti.NumCivico, NumDipendenti, Fatturato, SitoInternet, '+
          'EBC_Clienti.TipoStradaLegale, EBC_Clienti.NumCivicoLegale, '+
          'EBC_Clienti.NazioneAbb, EBC_Clienti.NazioneAbbLegale, Blocco1, EnteCertificatore, LibPrivacy, CCIAA '+
          'FROM EBC_Clienti,EBC_Attivita '+
          'WHERE  (EBC_Clienti.IDAttivita *= Ebc_attivita.ID)';
const xRicPendSQL='SELECT ebc_ricerche.ID, ebc_ricerche.IDCliente, '+
     'ebc_ricerche.IDUtente, ebc_ricerche.Progressivo, TitoloCliente, '+
          'ebc_ricerche.DataInizio, ebc_ricerche.DataFine, '+
          'ebc_ricerche.Stato, ebc_ricerche.Conclusa, '+
          'ebc_ricerche.IDStatoRic, ebc_ricerche.DallaData, '+
          'ebc_ricerche.IDArea, ebc_ricerche.IDMansione, '+
          'ebc_ricerche.IDSpec, ebc_ricerche.NumRicercati, '+
          'ebc_ricerche.Note, ebc_ricerche.IDFattura, '+
          'ebc_ricerche.Sospesa, ebc_ricerche.SospesaDal, '+
          'ebc_ricerche.StipendioNetto, ebc_ricerche.StipendioLordo, '+
          'ebc_ricerche.Fatturata, ebc_ricerche.Specifiche, ebc_ricerche.SpecifEst, '+
          'aree.Descrizione Area, mansioni.Descrizione Mansione, '+
          'ggDiffApRic,ggDiffUcRic,ggDiffColRic,DataPrevChiusura, '+
          'mansioni.IDArea IDAreaMans, ebc_ricerche.IDOfferta, '+
          'ebc_clienti.Descrizione Cliente, IDContrattoCG, '+
          'users.Nominativo Utente, IDUtente2, IDUtente3, '+
          'ebc_statiric.StatoRic StatoRic, ebc_ricerche.Tipo, '+
          'EBC_Ricerche.IDSede, EBC_Ricerche.IDContattoCli, '+
          'Aziende.Descrizione Sede, EBC_ContattiClienti.Contatto RifInterno, '+
          'Provvigione,ProvvigionePerc,QuandoPagamUtente, '+
          'OreLavUtente1,OreLavUtente2,OreLavUtente3, IDCausale '+
          'FROM EBC_Ricerche,Aree, '+
          'Mansioni,EBC_Clienti, '+
          'Users,EBC_StatiRic, Aziende, EBC_ContattiClienti  '+
          'WHERE (ebc_ricerche.IDArea *= aree.ID) '+
          'AND   (ebc_ricerche.IDMansione *= mansioni.ID) '+
          'AND   (ebc_ricerche.IDCliente = ebc_clienti.ID) '+
          'AND   (ebc_ricerche.IDUtente = users.ID) '+
          'AND   (ebc_ricerche.IDStatoRic = ebc_statiric.ID) '+
          'AND  (ebc_ricerche.IDSede *= Aziende.ID) '+
          'AND  (ebc_ricerche.IDContattoCli *= EBC_ContattiClienti.ID)';
var xArrayIDAnag: array[1..xMax] of integer;
var xReg: TRegistry;
procedure ApriFileWord(xNumCv: string);
procedure ApriFileWordFromFile(xNomeFile: string);
procedure CaricaPrimi;
procedure SettaDiritti(xID: integer);
function OkIDAnag(xID: integer): boolean;
function CreateQueryFromString(s: string): TQuery;
function PosizionaAnag(xIDAnag: integer): boolean;
function DammiCVNumero(xIDAnag: integer): integer;
procedure CaricaApriAnag(xStringa,xNome: string);
procedure CaricaApriClienti(xStringa,xFiltro: string; xTestoCont: boolean=False);
procedure CaricaApriRicPend(xIDRicerca: Integer);
function SpreadString(s: string): TStringList; // trasforma stringa con CR in StringList
function AccodaSMSaDispatcher(xIDAnag,xIDRic,xIDUtente,xIDCliente: integer; xMessaggio,xDispatcher: string; xStorico: boolean=True): boolean;
function CreaFileWord(xModelloWord,xTabellaMaster: string; xIDAnag: integer): string;
procedure Log_Operation(xIDUtente: integer; xTabella: string; xKeyValue: integer; xOperation: string; xNote: string='');
function CreaFileWordAnag(xIDModello: integer; xModelloWord: string; xSalva,xStampa: boolean): string;
function OpenSelFromTab(xTable,xKeyField,xFieldName,xFieldTitle,xID: string; xModify: boolean=True): string;
function VerifIntegrRef(xTable,xFKField,xID: string; xMessStop: boolean=True): boolean;
procedure ApriCV(xIDAnag: integer);
function IncompAnagCli(xIDAnag,xIDCliente: integer): boolean;
procedure OpenTab(xTable: string; xFields,xFieldtitles: array of string; xModify: boolean=True);
function CheckAnagInseritoBlocco1(xIDAnag: integer): integer;
function GetIDEvento(xDic: string): integer;
function GetPwdUtenteResp(xIDUtente: integer): string;
function GetIDUtenteResp(xIDUtente: integer): integer;
function GetDescUtenteResp(xIDUtente: integer): string;
function GetDescCliente(xIDCliente: integer): string;
function GetCodRicerca: string;
function GetCodOfferta: string;
function GetDocPath: string;
function GetWinTempPath: string;
function GetCVPath: string;
function GetIDAnagUtente(xIDUtente: integer): integer;
procedure CancellaSogg(xIDAnag: integer);
procedure ScriviRegistry(xChiave,xValore: string);
function LeggiRegistry(xChiave: string): string;

implementation

procedure CaricaPrimi;
var i: integer;
begin
     if xMax=99999 then exit;
     for i:=1 to xMax do xArrayIDAnag[i]:=0;
     Data.TAnagABS.Open;
     for i:=1 to xMax do begin
          xArrayIDAnag[i]:=Data.TAnagABSID.Value;
          Data.TAnagABS.Next;
     end;
     Data.TAnagABS.Close;
end;

procedure SettaDiritti(xID: integer);
var i: integer;
     xTrovato: boolean;
begin
     if xMax=99999 then
          xTrovato:=True
     else begin
          xTrovato:=False;
          for i:=1 to xMax do begin
               if xArrayIDAnag[i]=xID then begin
                    xTrovato:=True;
                    break;
               end;
          end;
     end;
     if xTrovato then begin
          MainForm.PageControl2.Visible:=True;
          MainForm.TbAnag.Visible:=True;
          if Uppercase(copy(Data.GlobalNomeAzienda.Value,1,6))='ADVANT' then
               MainForm.TbAnag.Visible:=False;
     end else begin
          MainForm.PageControl2.Visible:=False;
          MainForm.TbAnag.Visible:=False;
     end;
end;

function OkIDAnag(xID: integer): boolean;
var i: integer;
     xTrovato: boolean;
begin
     if xMax=99999 then
          xTrovato:=True
     else begin
          xTrovato:=False;
          for i:=1 to xMax do begin
               if xArrayIDAnag[i]=xID then begin
                    xTrovato:=True;
                    break;
               end;
          end;
     end;
     if xTrovato then Result:=True else Result:=False;
end;

procedure ApriFileWord(xNumCv: string);
var xFileWord: string;
     MSWord: Variant;
     xApriWord,xNuovoFile: boolean;
begin
     if xNumCv='' then MessageDlg('Numero CV mancante - � necessario averlo',mtError, [mbOK],0)
     else begin
          xApriWord:=True;
          xNuovoFile:=False;
          xFileWord:=GetDocPath+'\w'+xNumCv+'.doc';
          if not FileExists(xFileWord) then
               if MessageDlg('Il file non esiste - Vuoi crearlo ?',mtWarning, [mbYes,mbNo],0)=mrYes then begin
                    xApriWord:=True;
                    xNuovoFile:=True;
               end else xApriWord:=False;

          if xApriWord then begin
               try MsWord:=CreateOleObject('Word.Basic');
               except
                    ShowMessage('Non riesco ad aprire Microsoft Word.');
                    exit;
               end;
               MsWord.AppShow;
               if xNuovoFile then MsWord.FileNew
               else MsWord.FileOpen(xFileWord);
               MsWord.FileSaveAs(xFileWord);
          end;
     end;
end;

procedure ApriFileWordFromFile(xNomeFile: string);
var xFileWord: string;
     MSWord: Variant;
     xApriWord,xNuovoFile: boolean;
begin
     xFileWord:=xNomeFile;

     if not FileExists(xFileWord) then begin
          MessageDlg('Il file non esiste',mtError, [mbOK],0);
          Exit;
     end;
     try MsWord:=CreateOleObject('Word.Basic');
     except
          ShowMessage('Non riesco ad aprire Microsoft Word.');
          exit;
     end;
     MsWord.AppShow;
     MsWord.FileOpen(xFileWord);
end;

function CreateQueryFromString(s: string): TQuery;
begin
     if Data.QTemp.Active then Data.QTemp.close;
     Data.QTemp.SQL.text:=s;
     Result:=Data.QTemp;
end;

function PosizionaAnag(xIDAnag: integer): boolean;
begin
     if Data.TAnagrafica.Active then Data.TAnagrafica.Open;
     Data.TAnagrafica.SQL.clear;
     Data.TAnagrafica.SQL.text:=xAnagSQL+' '+'AND Anagrafica.ID=:xID';
     Data.TAnagrafica.Prepare;
     Data.TAnagrafica.ParamByName('xID').asInteger:=xIDAnag;
     Data.TAnagrafica.Open;
     Result:=not Data.TAnagrafica.isEmpty;
end;

function DammiCVNumero(xIDAnag: integer): integer;
begin
     Data.Q1.Close;
     Data.Q1.SQl.Text:='select CVNumero from Anagrafica where ID='+IntToStr(xIDAnag);
     Data.Q1.Open;
     if Data.Q1.RecordCount=0 then Result:=0
     else Result:=Data.Q1.FieldByName('CVNumero').asInteger;
     Data.Q1.Close;
end;

procedure CaricaApriAnag(xStringa,xNome: string);
begin
     if Data.TAnagrafica.Active then Data.TAnagrafica.Close;
     Data.TAnagrafica.SQL.clear;
     Data.TAnagrafica.SQL.text:=xAnagSQL;
     if xNome='' then
          Data.TAnagrafica.SQL.Add('AND Cognome LIKE :xCerca ORDER BY Cognome,Nome')
     else Data.TAnagrafica.SQL.Add('AND Cognome LIKE :xCerca AND Nome LIKE :xNome ORDER BY Cognome,Nome');
     Data.TAnagrafica.Prepare;
     Data.TAnagrafica.ParamByName('xCerca').asString:=xStringa+'%';
     if xNome<>'' then
          Data.TAnagrafica.ParamByName('xNome').asString:=xNome+'%';
     Data.TAnagrafica.Open;
     if not Data.QAnagNote.active then Data.QAnagNote.Open;
end;

procedure CaricaApriClienti(xStringa,xFiltro: string; xTestoCont: boolean=False);
begin
     if Data2.TEBCClienti.Active then Data2.TEBCClienti.Close;
     Data2.TEBCClienti.SQL.clear;
     Data2.TEBCClienti.SQL.text:=xClientiSQL;
     if xFiltro<>'' then
          Data2.TEBCClienti.SQL.Add('AND EBC_Clienti.Stato=:xFiltro');
     Data2.TEBCClienti.SQL.Add('AND EBC_Clienti.Descrizione LIKE :xCerca '+
          'ORDER BY EBC_Clienti.Descrizione');
     Data2.TEBCClienti.Prepare;
     if xTestoCont then
          Data2.TEBCClienti.ParamByName('xCerca').asString:='%'+xStringa+'%'
     else Data2.TEBCClienti.ParamByName('xCerca').asString:=xStringa+'%';
     if xFiltro<>'' then
          Data2.TEBCClienti.ParamByName('xFiltro').asString:=xFiltro;
     Data2.TEBCClienti.Open;
     MainForm.LTotAz.Caption:=IntToStr(Data2.TEBCclienti.RecordCount);
end;

procedure CaricaApriRicPend(xIDRicerca: Integer);
begin
     if DataRicerche.TRicerchePend.Active then DataRicerche.TRicerchePend.Close;
     DataRicerche.TRicerchePend.SQL.clear;
     DataRicerche.TRicerchePend.SQL.text:=xRicPendSQL;
     DataRicerche.TRicerchePend.SQL.Add('AND EBC_Ricerche.ID=:xCerca ');
     DataRicerche.TRicerchePend.Prepare;
     DataRicerche.TRicerchePend.ParamByName('xCerca').asInteger:=xIDRicerca;
     DataRicerche.TRicerchePend.Open;
end;

function SpreadString(s: string): TStringList;
var xSl: TStringList;
     xS: string;
     xCRpos: integer;
begin
     xS:=s;
     xSl:=TStringList.create;
     while pos(chr(13),xS)>0 do begin
          xCRpos:=pos(chr(13),xS);
          xSl.Add(copy(xS,1,xCRpos-1));
          xS:=copy(xS,xCRpos+1,length(xS));
     end;
     Result:=xSl;
     xsl.Free;
end;

function AccodaSMSaDispatcher(xIDAnag,xIDRic,xIDUtente,xIDCliente: integer; xMessaggio,xDispatcher: string; xStorico: boolean): boolean;
var MyStringList: TStrings;
     i: integer;
     xAliasExists: boolean;
     xCellulare,xPrefissoSZ,xNumero: string;
begin
     ShowMessage('funzione disabilitata');
     Result:=False;
     exit;

     // controllo esistenza alias e tabella di SMSDispatcher
     {MyStringList:=TStringList.create;
     Session.GetAliasNames(MyStringList);
     xAliasExists:=False;
     for I:=0 to MyStringList.Count-1 do
          if MyStringList[I]='EBCSMS' then begin
               xAliasExists:=true;
               break;
          end;
     MyStringList.Free;
     if not xAliasExists then begin
          MessageDlg('Alias EBCSMS non configurato su questa macchina: '+chr(13)+
               'Impossibile inviare SMS',mtError, [mbOK],0);
          exit;
     end else begin
          // controllo esistenza SMSDispatcher
          //
          //  DA FARE
          //
     end;

     Data.Q1.Close;
     Data.Q1.SQl.Text:='select Cognome,Nome,Cellulare from Anagrafica where ID='+IntToStr(xIDAnag);
     Data.Q1.Open;
     // controllo sul n� di cellulare
     if (Data.Q1.IsEmpty)or
          (Data.Q1.FieldByName('Cellulare').asString='')or
          ((pos('-',Data.Q1.FieldByName('Cellulare').asString)=0)and
          (pos('/',Data.Q1.FieldByName('Cellulare').asString)=0)) then begin
          Result:=False;
          Exit;
     end;

     // prefisso e numero
     xCellulare:=Data.Q1.FieldByName('Cellulare').asString;
     if pos('-',xCellulare)>0 then begin
          xPrefissoSZ:=copy(trim(xCellulare),2,pos('-',xCellulare)-2);
          xNumero:=copy(trim(xCellulare),pos('-',xCellulare)+1,length(trim(xCellulare)));
     end;
     if pos('/',xCellulare)>0 then begin
          xPrefissoSZ:=copy(trim(xCellulare),2,pos('/',xCellulare)-2);
          xNumero:=copy(trim(xCellulare),pos('/',xCellulare)+1,length(trim(xCellulare)));
     end;
     Data.TMessaggiSMS.DatabaseName:='EBCSMS';
     Data.TMessaggiSMS.Open;
     Data.TMessaggiSMS.InsertRecord([Data.Q1.FieldByName('Cognome').asString+' '+Data.Q1.FieldByName('Nome').asString,
          StrToInt(xNumero),
               xMessaggio,
               '0',
               xDispatcher,
               xPrefissoSZ,
               39]);
     Data.TMessaggiSMS.Close;
     // storico
     if xStorico then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.SQL.text:='insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) '+
                         'values (:xIDAnagrafica,:xIDEvento,:xDataEvento,:xAnnotazioni,:xIDRicerca,:xIDUtente,:xIDCliente)';
                    Q1.ParambyName('xIDAnagrafica').asInteger:=xIDAnag;
                    Q1.ParambyName('xIDEvento').asInteger:=72;
                    Q1.ParambyName('xDataEvento').asDateTime:=Date;
                    Q1.ParambyName('xAnnotazioni').asString:=copy(xMessaggio,1,50);
                    Q1.ParambyName('xIDRicerca').asInteger:=xIDRic;
                    Q1.ParambyName('xIDUtente').asInteger:=xIDUtente;
                    Q1.ParambyName('xIDCliente').asInteger:=xIDCliente;
                    Q1.ExecSQL;
                    DB.CommitTrans;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: storico SMS non inserito',mtError, [mbOK],0);
               end;
          end;
     end;
     Result:=True;
     Data.Q1.Close;}
end;

function CreaFileWord(xModelloWord,xTabellaMaster: string; xIDAnag: integer): string;
var xFileModello,xFileWord: string;
     xAnno,xMese,xGiorno: Word;
     MSWord: Variant;
     xApriWord,xNuovoFile: boolean;
     xIDModello: integer;
begin
     if not FileExists(GetDocPath+'\'+xModelloWord+'.doc') then begin
          MessageDlg('Il modello non esiste o la cartella non � impostata correttamente'+chr(13)+
               'IMPOSSIBILE PROSEGUIRE',mtError, [mbOK],0);
          Result:='';
          exit;
     end;
     with Data.QTemp do begin
          Close;
          SQL.text:='select * from ModelliWord where NomeModello=:xMod and TabellaMaster=:xTabMaster';
          ParamByName('xMod').asString:=xModelloWord;
          ParamByName('xTabMaster').asString:=xTabellaMaster;
          Open;
          DecodeDate(Date,xAnno,xMese,xGiorno);
          xFileWord:=GetDocPath+'\'+
               FieldbyName('InizialiFileGenerato').asString+IntToStr(xIDAnag)+
               '-'+IntToStr(xGiorno)+IntToStr(xMese)+'.doc';
          xIDModello:=FieldbyName('ID').asInteger;
          // controllo esistenza file
          if FileExists(xFileWord) then
               if MessageDlg('Il file esiste gi�: vuoi ricrearlo ?',mtWarning, [mbYes,mbNo],0)=mrNo then begin
                    Result:='';
                    exit;
               end;
          // Apri Word
          try MsWord:=CreateOleObject('Word.Basic');
          except
               ShowMessage('Non riesco ad aprire Microsoft Word.');
               exit;
          end;
          MsWord.AppShow;
          // apri modello
          MsWord.FileOpen(GetDocPath+'\'+xModelloWord+'.doc');
          // riempimento campi
          Close;
          SQL.text:='select NomeCampo,Campo,TipoCampo from ModelliWordCampi where IDModello=:xIDMod';
          ParamByName('xIDMod').asInteger:=xIDModello;
          Open;
          while not EOF do begin
               Data2.QTemp.Close;
               Data2.QTemp.SQL.text:='select '+FieldByName('Campo').asString+' ValoreCampo from '+xTabellaMaster+' where ID='+IntToStr(xIDAnag);
               Data2.QTemp.Open;
               MsWord.EditFind(FieldbyName('NomeCampo').asString);
               if FieldbyName('TipoCampo').asString='string' then
                    MsWord.Insert(Data2.QTemp.FieldbyName('ValoreCampo').asString);
               if FieldbyName('TipoCampo').asString='datetime' then
                    MsWord.Insert(DateToStr(Data2.QTemp.FieldbyName('ValoreCampo').asDateTime));
               if FieldbyName('TipoCampo').asString='integer' then
                    MsWord.Insert(IntToStr(Data2.QTemp.FieldbyName('ValoreCampo').asInteger));
               Next;
          end;
          Close;
          Data2.QTemp.Close;
          // salvataggio
          MsWord.FileSaveAs(xFileWord);
          Result:=xFileWord;
     end;
end;

procedure Log_Operation(xIDUtente: integer; xTabella: string; xKeyValue: integer; xOperation: string; xNote: string='');
begin
     // inserisci l'operazione nel log delle operazioni
     // ATTENZIONE: LA TRANSAZIONE DOVREBBE GIA' ESSERE IN CORSO !!
     if xKeyValue=0 then exit;
     with Data do begin
          Q1.Close;
          Q1.SQL.text:='insert into Log_TableOp (IDUtente,DataOra,Tabella,KeyValue,Operation,Annotazioni) '+
               'values (:xIDUtente,:xDataOra,:xTabella,:xKeyValue,:xOperation,:xAnnotazioni)';
          Q1.ParamByName('xIDUtente').asInteger:=xIDUtente;
          Q1.ParamByName('xDataOra').asDateTime:=Now;
          Q1.ParamByName('xTabella').asString:=xTabella;
          Q1.ParamByName('xKeyValue').asInteger:=xKeyValue;
          Q1.ParamByName('xOperation').asString:=xOperation;
          Q1.ParamByName('xAnnotazioni').asString:=xNote;
          Q1.ExecSQL;
     end;
end;

function CreaFileWordAnag(xIDModello: integer; xModelloWord: string; xSalva,xStampa: boolean): string;
var xFileModello,xFileWord: string;
     xAnno,xMese,xGiorno: Word;
     MSWord: Variant;
     xApriWord,xNuovoFile: boolean;
begin
     if not FileExists(GetDocPath+'\'+xModelloWord+'.doc') then begin
          MessageDlg('Il modello non esiste o la cartella non � impostata correttamente'+chr(13)+
               'IMPOSSIBILE PROSEGUIRE',mtError, [mbOK],0);
          Result:='';
          exit;
     end;
     with Data.QTemp do begin
          Close;
          SQL.text:='select * from ModelliWord where ID='+IntToStr(xIDModello);
          Open;
          DecodeDate(Date,xAnno,xMese,xGiorno);
          xFileWord:=GetDocPath+'\'+
               FieldbyName('InizialiFileGenerato').asString+IntToStr(Data.TAnagraficaID.Value)+
               '-'+IntToStr(xGiorno)+IntToStr(xMese)+IntToStr(xAnno)+'.doc';
          // Apri Word
          try MsWord:=CreateOleObject('Word.Basic');
          except
               ShowMessage('Non riesco ad aprire Microsoft Word.');
               exit;
          end;
          MsWord.AppShow;
          // apri modello
          MsWord.FileOpen(GetDocPath+'\'+xModelloWord+'.doc');
          // riempimento campi
          Close;
          SQL.text:='select NomeCampo,Campo,TipoCampo from ModelliWordCampi where IDModello=:xIDMod order by ID';
          ParamByName('xIDMod').asInteger:=xIDModello;
          Open;
          while not EOF do begin
               MsWord.EditFind(FieldbyName('NomeCampo').asString);
               if Data.TAnagrafica.FieldbyName(FieldByName('Campo').asString).asString<>'' then
                    MsWord.Insert(Data.TAnagrafica.FieldbyName(FieldByName('Campo').asString).asString)
               else MsWord.Insert('---');
               Next;
          end;
          Close;
          Data.QTemp.Close;

          if xSalva then
               // salvataggio
               MsWord.FileSaveAs(xFileWord);
          Result:=xFileWord;
          if xStampa then begin
               MsWord.FilePrint;
               MsWord.FileClose;
               MsWord.AppClose;
          end;
     end;
end;

function OpenSelFromTab(xTable,xKeyField,xFieldName,xFieldTitle,xID: string; xModify: boolean=True): string;
begin
     // apre una form di selezione con una tabella che visualizza UN campo solo
     // Usage:  xTable = tabella da cui selezionare
     //         xKeyField = campo chiave della tabella
     //         xFieldName = campo da visualizzare
     //         xFieldTitle = titolo per la griglia
     //         xID = valore del campo chiave (per la modifica: si posiziona sull'ID)
     //               � stringa perch� cos� va bene per gli interi e per le stringhe.
     // --> restituisce il campo chiave come stringa (cos� va bene per gli interi e per le stringhe)
     // --> imposta inoltre la variabile globale "MainForm.xgenericString" con il valore di xFieldName
     Application.CreateForm(TSelTabGenForm,SelTabGenForm);
     SelTabGenForm.FrameDB1.serviceform:=FrameDBForm;
     SelTabGenForm.xFieldName:=xFieldName;
     SelTabGenForm.xFieldTitle:=xFieldTitle;
     SelTabGenForm.QTab.SQL.text:='select '+xKeyField+','+xFieldName+' from '+xTable;
     SelTabGenForm.QTab.Open;
     if xID<>'' then
          while (SelTabGenForm.QTab.fieldbyName(xKeyField).asString<>xID)and
               (not SelTabGenForm.QTab.EOF) do SelTabGenForm.QTab.Next;
     SelTabGenForm.xModify:=xModify;
     SelTabGenForm.showmodal;
     MainForm.xgenericString:='';
     if SelTabGenForm.Modalresult=mrOK then begin
          Result:=SelTabGenForm.QTab.fieldbyName(xKeyField).asstring;
          MainForm.xgenericString:=SelTabGenForm.QTab.fieldbyName(xFieldName).asstring;
     end else Result:='';
     SelTabGenForm.Free;
end;

procedure ApriCV(xIDAnag: integer);
var xFile,xFileName: string;
     xViewCV,xProcedi: boolean;
begin
     if not MainForm.CheckProfile('053') then Exit;
     Data.Q1.Close;
     Data.Q1.SQL.text:='select CVNumero,Cognome,Nome from Anagrafica '+
          'where Anagrafica.ID='+IntToStr(xIDAnag);
     Data.Q1.Open;
     xFile:=GetCVPath+'\i'+Data.Q1.FieldByName('CVNumero').asString+'a.gif';
     if FileExists(xFile) then xViewCV:=True
     else begin
          // se esiste in altri formati (TIFF, BMP e PDF)
          xFile:=GetCVPath+'\i'+Data.Q1.FieldByName('CVNumero').asString+'a.tif';
          if FileExists(xFile) then begin
               ShellExecute(0,'Open',pchar(xFile),'','',SW_SHOW);
               exit;
          end;
          xFile:=GetCVPath+'\i'+Data.Q1.FieldByName('CVNumero').asString+'.tif';
          if FileExists(xFile) then begin
               ShellExecute(0,'Open',pchar(xFile),'','',SW_SHOW);
               exit;
          end;
          xFile:=GetCVPath+'\i'+Data.Q1.FieldByName('CVNumero').asString+'a.bmp';
          if FileExists(xFile) then begin
               ShellExecute(0,'Open',pchar(xFile),'','',SW_SHOW);
               exit;
          end;
          xFile:=GetCVPath+'\i'+Data.Q1.FieldByName('CVNumero').asString+'a.pdf';
          if FileExists(xFile) then begin
               ShellExecute(0,'Open',pchar(xFile),'','',SW_SHOW);
               exit;
          end;
          xViewCV:=False;
          Application.CreateForm(TSceltaAnagFileForm,SceltaAnagFileForm);
          SceltaAnagFileForm.QAnagFile.ParamByName('xIDAnag').asinteger:=xIDAnag;
          SceltaAnagFileForm.QAnagFile.Open;
          if SceltaAnagFileForm.QAnagFile.IsEmpty then begin
               if MessageDlg('Nessuna immagine e nessun file associato '+chr(13)+
                    'Aprire comunque per la scannerizzazione ?',mtWarning, [mbYES,mbNO],0)=mrYes then
                    xViewCV:=True;
          end else begin
               if SceltaAnagFileForm.QAnagFile.RecordCount=1 then begin
                    // se ce n'� uno solo, apri direttamente questo
                    xFileName:=SceltaAnagFileForm.QAnagFile.FieldByName('NomeFile').asString;
                    ShellExecute(0,'Open',pchar(xFileName),'','',SW_SHOW);
               end else begin
                    // altrimenti scegli tra quelli nell'elenco
                    SceltaAnagFileForm.ShowModal;
                    if SceltaAnagFileForm.ModalResult=mrOK then begin
                         // apertura file
                         xFileName:=SceltaAnagFileForm.QAnagFile.FieldByName('NomeFile').asString;
                         ShellExecute(0,'Open',pchar(xFileName),'','',SW_SHOW);
                    end;
               end;
          end;
          SceltaAnagFileForm.Free;
     end;
     if xViewCV then begin
          Application.CreateForm(TViewForm,ViewForm);
          if FileExists(xFile) then ViewForm.xFileName:=xFile
          else ViewForm.xFileName:='';
          ViewForm.xCVNumero:=Data.Q1.FieldByName('CVNumero').asString;
          ViewForm.Caption:=ViewForm.Caption+' '+Data.Q1.FieldByName('Cognome').asString+' '+Data.Q1.FieldByName('Nome').asString;
          ViewForm.ShowModal;
          ViewForm.Free;
     end;
     Data.Q1.Close;
end;

function VerifIntegrRef(xTable,xFKField,xID: string; xMessStop: boolean=True): boolean;
begin
     // verifica l'integrit� referenziale
     // Usage:  xTable = tabella (figlia) su cui operare la verifica
     //         xFKField = campo di questa tabella (foreign key)
     //         xID = valore della foreign key dalla tabella che si vuole cancellare.
     // --> restituisce TRUE se NON ci sono problemi (ovvero nessun record nella tab.figlia)
     //     restituisce FALSE se ci sono record.
     Data.Q1.Close;
     Data.Q1.SQL.text:='select count(*) Tot from '+xTable+' where '+xFKField+'='+xID;
     Data.Q1.Open;
     if Data.Q1.FieldByname('Tot').asInteger>0 then begin
          if xMessStop then
               MessageDlg('Violazione di integrit� referenziale con la tabella collegata:'+chr(13)+
                    xTable+' (campo: '+xFKField+')'+chr(13)+
                    'IMPOSSIBILE CANCELLARE',mtError, [mbOK],0);
          Result:=False;
     end else Result:=True;
     Data.Q1.Close;
end;

function IncompAnagCli(xIDAnag,xIDCliente: integer): boolean;
begin
     // incompatibilit� Anagrafica - cliente: tabella "AnagIncompClienti"
     // restituisce TRUE se c'� incompatibilit�
     with Data.Q1 do begin
          Close;
          SQL.Text:='select count(*) Tot from AnagIncompClienti where IDAnagrafica=:xIDAnagrafica and IDCliente=:xIDCliente';
          ParamByName('xIDAnagrafica').asInteger:=xIDAnag;
          ParamByName('xIDCliente').asInteger:=xIDCliente;
          Open;
          if FieldByName('Tot').asInteger>0 then Result:=True
          else begin
               // controllo incompatibilit� con clienti collegati, se ce ne sono
               Close;
               SQL.Text:='select IDClienteRef from ClienteLinkCliente where IDCliente=:xIDCliente and CheckCand=1';
               ParamByName('xIDCliente').asInteger:=xIDCliente;
               Open;
               Result:=False;
               while not EOF do begin
                    with Data.QTemp do begin
                         Close;
                         SQL.Text:='select count(*) Tot from AnagIncompClienti where IDAnagrafica=:xIDAnagrafica and IDCliente=:xIDCliente';
                         ParamByName('xIDAnagrafica').asInteger:=xIDAnag;
                         ParamByName('xIDCliente').asInteger:=Data.Q1.FieldByName('IDClienteRef').asInteger;
                         Open;
                         if FieldByName('Tot').asInteger>0 then Result:=True;
                    end;
                    Next;
               end;
          end;
          Close;
     end;
end;

procedure OpenTab(xTable: string; xFields,xFieldtitles: array of string; xModify: boolean=True);
var i: integer;
     xCampi: string;
begin
     // apre una form per la gestione la tabella (che visualizza tutti i campi)
     // Usage:  xTable = tabella da gestire
     //         xFields = array dei campi (stringhe); il primo � la CHIAVE PRIMARIA
     //         xFieldtitles = array dei titoli dei campi (tolta la CHIAVE PRIMARIA)
     //
     Application.CreateForm(TSelTabGenForm,SelTabGenForm);
     SelTabGenForm.FrameDB1.serviceform:=FrameDBForm;
     SelTabGenForm.xModify:=xModify;
     SelTabGenForm.xFieldName:='';
     SelTabGenForm.xFieldTitle:='';
     SelTabGenForm.Caption:='Gestione Tabella '+xTable;
     for i:=0 to Length(xFieldtitles)-1 do begin
          SelTabGenForm.xTitles[i+1]:=xFieldtitles[i];
     end;
     xcampi:='';
     for i:=0 to Length(xFields)-1 do begin
          xCampi:=xCampi+xFields[i];
          if i+1<=Length(xFields)-1 then xCampi:=xcampi+',';
     end;
     SelTabGenForm.Width:=400;
     SelTabGenForm.Height:=300;
     SelTabGenForm.QTab.SQL.text:='select '+xCampi+' from '+xTable;
     SelTabGenForm.QTab.Open;
     SelTabGenForm.BitBtn1.Visible:=False;
     SelTabGenForm.BitBtn2.caption:='Esci';
     SelTabGenForm.showmodal;
     SelTabGenForm.Free;
end;

function CheckAnagInseritoBlocco1(xIDAnag: integer): integer;
begin
     // controlla se il soggetto ha Esp.Lav. ATTUALI in un'azienda per cui c'� il BLOCCO1 attivo
     // o comunque se � un cliente attivo
     // ritorna ZERO se NON c'� blocco, altrimenti ritorna l'ID del Cliente
     // + 10.000 se di livello 1 e + 20.000 se di livello 2
     with Data.Q1 do begin
          Close;
          SQL.text:='select EsperienzeLavorative.IDAzienda,stato from EsperienzeLavorative,EBC_Clienti '+
               'where EsperienzeLavorative.IDAzienda=EBC_Clienti.ID '+
               'and (Blocco1=1 or stato=:xStato) '+
               'and Attuale=1 and IDAnagrafica='+IntToStr(xIDAnag);
          ParamByName('xStato').asString:='attivo';
          Open;
          if IsEmpty then Result:=0
          else begin
               if FieldByName('stato').asString='attivo' then
                    Result:=20000+FieldByName('IDAzienda').AsInteger
               else Result:=10000+FieldByName('IDAzienda').AsInteger;
          end;
          Close;
     end;
end;

function GetIDEvento(xDic: string): integer;
begin
     // restituisce l'ID dell'evento data la sua dicitura
     with Data.Q1 do begin
          Close;
          SQL.text:='select ID from EBC_Eventi '+
               'where Evento=:xEvento ';
          ParamByName('xEvento').asString:=xDic;
          Open;
          if IsEmpty then Result:=0
          else Result:=FieldByName('ID').asInteger;
          Close;
     end;
end;

function GetPwdUtenteResp(xIDUtente: integer): string;
var xIDUtenteResp: integer;
begin
     // restituisce la PASSWORD dell'utente "sopra" (IDUtenteResp)
     // altrimenti (se non c'� tale utente) quella di Administrator
     with Data.Q1 do begin
          Close;
          SQL.text:='select IDUtenteResp from Users where ID='+IntToStr(xIDUtente);
          Open;
          if (IsEmpty)or(FieldByName('IDUtenteResp').asInteger=0) then begin
               Close;
               SQL.text:='select Password from Users where Nominativo=''Administrator''';
          end else begin
               xIDUtenteResp:=FieldByName('IDUtenteResp').asInteger;
               Close;
               SQL.text:='select Password from Users where ID='+IntToStr(xIDUtenteResp);
          end;
          Open;
          Result:=FieldByName('Password').asString;
          Close;
     end;
end;

function GetDescUtenteResp(xIDUtente: integer): string;
var xIDUtenteResp: integer;
begin
     // restituisce la DESCRIZIONE dell'utente "sopra" (IDUtenteResp)
     // altrimenti (se non c'� tale utente) quella di Administrator
     with Data.Q1 do begin
          Close;
          SQL.text:='select IDUtenteResp from Users where ID='+IntToStr(xIDUtente);
          Open;
          if (IsEmpty)or(FieldByName('IDUtenteResp').asInteger=0) then begin
               Close;
               SQL.text:='select Descrizione from Users where Nominativo=''Administrator''';
          end else begin
               xIDUtenteResp:=FieldByName('IDUtenteResp').asInteger;
               Close;
               SQL.text:='select Descrizione from Users where ID='+IntToStr(xIDUtenteResp);
          end;
          Open;
          Result:=FieldByName('Descrizione').asString;
          Close;
     end;
end;

function GetIDUtenteResp(xIDUtente: integer): integer;
var xIDUtenteResp: integer;
begin
     // restituisce l'ID dell'utente "sopra" (IDUtenteResp)
     // oppure quello dell'amministratore se non c'�
     with Data.Q1 do begin
          Close;
          SQL.text:='select IDUtenteResp from Users where ID='+IntToStr(xIDUtente);
          Open;
          if (IsEmpty)or(FieldByName('IDUtenteResp').asInteger=0) then Result:=9// amministratore
          else Result:=FieldByName('IDUtenteResp').asInteger;
          Close;
     end;
end;

function GetDescCliente(xIDCliente: integer): string;
begin
     // restituisce la descrizione del cliente
     with Data.Q1 do begin
          Close;
          SQL.text:='select Descrizione from EBC_Clienti where ID='+IntToStr(xIDCliente);
          Open;
          if IsEmpty then Result:=''
          else Result:=FieldByName('Descrizione').asString;
          Close;
     end;
end;

function GetCodRicerca: string;
var xNewTrans: boolean;
     xAnno,xMese,xgiorno: Word;
begin
     // restituisce il codice per una nuova commessa
     // prendendolo dall'ultimo codice in GLOBAL
     // e incrementando il relativo contatore
     Result:='';
     xNewTrans:=False;
     with Data do begin
          if not DB.InTransaction then begin
               DB.BeginTrans;
               xNewTrans:=True;
          end;
          try
               QTemp.Close;
               QTemp.SQL.Text:='select UltimoProgRicerca,AddAnnoRic from global';
               QTemp.Open;
               if QTemp.fieldByName('AddAnnoRic').AsBoolean=true then begin
                    DecodeDate(Date,xAnno,xMese,xgiorno);
                    Result:=IntToStr(QTemp.fieldByName('UltimoProgRicerca').asInteger+1)+'/'+copy(IntToStr(xAnno),3,2);
               end else Result:=IntToStr(QTemp.fieldByName('UltimoProgRicerca').asInteger+1);
               QTemp.Close;
               Q1.SQL.text:='update global set UltimoProgRicerca=UltimoProgRicerca+1 ';
               Q1.ExecSQL;
               if xNewTrans then DB.CommitTrans;
          except
               if xNewTrans then DB.RollbackTrans;
               Result:='';
               MessageDlg('ERRORE SUL DATABASE: progressivo ricerca non impostato',mtError, [mbOK],0);
               raise;
          end;
     end;
end;

function GetCodOfferta: string;
var xNewTrans: boolean;
     xAnno,xMese,xgiorno: Word;
begin
     // restituisce il codice per una nuova offerta
     // prendendolo dall'ultimo codice in GLOBAL
     // e incrementando il relativo contatore
     Result:='';
     xNewTrans:=False;
     with Data do begin
          if not DB.InTransaction then begin
               DB.BeginTrans;
               xNewTrans:=True;
          end;
          try
               QTemp.Close;
               QTemp.SQL.Text:='select UltimoProgOfferta,AddAnnoRic from global';
               QTemp.Open;
               if QTemp.fieldByName('AddAnnoRic').AsBoolean=true then begin
                    DecodeDate(Date,xAnno,xMese,xgiorno);
                    Result:=IntToStr(QTemp.fieldByName('UltimoProgOfferta').asInteger+1)+'/'+copy(IntToStr(xAnno),3,2);
               end else Result:=IntToStr(QTemp.fieldByName('UltimoProgOfferta').asInteger+1);
               QTemp.Close;
               Q1.SQL.text:='update global set UltimoProgOfferta=UltimoProgOfferta+1 ';
               Q1.ExecSQL;
               if xNewTrans then DB.CommitTrans;
          except
               if xNewTrans then DB.RollbackTrans;
               Result:='';
               MessageDlg('ERRORE SUL DATABASE: progressivo offerta non impostato',mtError, [mbOK],0);
               raise;
          end;
     end;
end;

function GetDocPath: string;
var xH1DocPath: string;
begin
     // restituisce il path della cartella documenti
     xH1DocPath:=LeggiRegistry('H1DocPath');
     if xH1DocPath<>'' then begin
          Result:=xH1DocPath;
     end else begin
          with Data.Q2 do begin
               Close;
               SQL.text:='select DirFileDoc from Global';
               Open;
               if IsEmpty then Result:=''
               else Result:=FieldByName('DirFileDoc').asString;
               Close;
          end;
     end;
end;

function GetCVPath: string;
var xH1CVPath: string;
begin
     // restituisce il path della cartella cv
     xH1CVPath:=LeggiRegistry('H1DocPath');
     if xH1CVPath<>'' then begin
          Result:=xH1CVPath;
     end else begin
          with Data.QTemp do begin
               Close;
               SQL.text:='select DirFileCV from Global';
               Open;
               if IsEmpty then Result:=''
               else Result:=FieldByName('DirFileCV').asString;
               Close;
          end;
     end;
end;

function GetIDAnagUtente(xIDUtente: integer): integer;
begin
     with Data.QTemp do begin
          Close;
          SQL.text:='select IDAnagrafica from Users where ID='+IntToStr(xIDUtente);
          Open;
          if IsEmpty then Result:=0
          else Result:=FieldByName('IDAnagrafica').asInteger;
          Close;
     end;
end;

procedure CancellaSogg(xIDAnag: integer);
var xCognome,xNome,xCVNumero,xNatoA,xNatoIl,xCVInseritoIndata,xCVDataAgg: string;
begin
     xNatoA:='--';
     xNatoIl:='--';
     xCVInseritoIndata:='--';
     xCVDataAgg:='--';
     with Data.QTemp do begin
          Close;
          SQL.text:='select Cognome,Nome,CVNumero,LuogoNascita,DataNascita,CVInseritoIndata,CVDataAgg from Anagrafica where ID='+IntToStr(xIDAnag);
          Open;
          xCognome:=FieldByName('Cognome').asString;
          xNome:=FieldByName('Nome').asString;
          xCVNumero:=FieldByName('CVNumero').asString;
          xNatoA:=FieldByName('LuogoNascita').asString;
          xNatoIl:=FieldByName('DataNascita').asString;
          xCVInseritoIndata:=FieldByName('CVInseritoIndata').asString;
          xCVDataAgg:=FieldByName('CVDataAgg').asString;
          Close;
     end;
     Data.DB.BeginTrans;
     try
          // eliminare tabelle collegate da CONSTRAINT di integrit� referenziale                         Data.Q1.Close;
          Data.Q1.Close;
          Data.Q1.SQL.Clear;
          Data.Q1.SQL.Add('delete from AnagAltreInfo where IDAnagrafica='+IntToStr(xIDAnag));
          Data.Q1.ExecSQL;
          Data.Q1.SQL.Clear;
          Data.Q1.SQL.Add('delete from AnagCaratteristiche where IDAnagrafica='+IntToStr(xIDAnag));
          Data.Q1.ExecSQL;
          Data.Q1.SQL.Clear;
          Data.Q1.SQL.Add('delete from AnagFile where IDAnagrafica='+IntToStr(xIDAnag));
          Data.Q1.ExecSQL;
          Data.Q1.SQL.Clear;
          Data.Q1.SQL.Add('delete from AnagIncompClienti where IDAnagrafica='+IntToStr(xIDAnag));
          Data.Q1.ExecSQL;
          Data.Q1.SQL.Clear;
          Data.Q1.SQL.Add('delete from AnagMansioni where IDAnagrafica='+IntToStr(xIDAnag));
          Data.Q1.ExecSQL;
          Data.Q1.SQL.Clear;
          Data.Q1.SQL.Add('delete from AnagValutaz where IDAnagrafica='+IntToStr(xIDAnag));
          Data.Q1.ExecSQL;
          Data.Q1.SQL.Clear;
          Data.Q1.SQL.Add('delete from CompetenzeAnagrafica where IDAnagrafica='+IntToStr(xIDAnag));
          Data.Q1.ExecSQL;
          Data.Q1.SQL.Clear;
          Data.Q1.SQL.Add('delete from EBC_CandidatiRicerche where IDAnagrafica='+IntToStr(xIDAnag));
          Data.Q1.ExecSQL;
          Data.Q1.SQL.Clear;
          Data.Q1.SQL.Add('delete from EBC_ContattiCandidati where IDAnagrafica='+IntToStr(xIDAnag));
          Data.Q1.ExecSQL;
          Data.Q1.SQL.Clear;
          Data.Q1.SQL.Add('delete from EsperienzeLavorative where IDAnagrafica='+IntToStr(xIDAnag));
          Data.Q1.ExecSQL;
          Data.Q1.SQL.Clear;
          Data.Q1.SQL.Add('delete from LingueConosciute where IDAnagrafica='+IntToStr(xIDAnag));
          Data.Q1.ExecSQL;
          Data.Q1.SQL.Clear;
          Data.Q1.SQL.Add('delete from Organigramma where IDDipendente='+IntToStr(xIDAnag));
          Data.Q1.ExecSQL;
          Data.Q1.SQL.Clear;
          Data.Q1.SQL.Add('delete from Storico where IDAnagrafica='+IntToStr(xIDAnag));
          Data.Q1.ExecSQL;
          Data.Q1.SQL.Clear;
          Data.Q1.SQL.Add('delete from TitoliStudio where IDAnagrafica='+IntToStr(xIDAnag));
          Data.Q1.ExecSQL;
          Data.Q1.SQL.Clear;
          Data.Q1.SQL.Add('delete from Anagrafica where ID='+IntToStr(xIDAnag));
          Data.Q1.ExecSQL;

          // log cancellazione
          Log_Operation(MainForm.xIDUtenteAttuale,'Anagrafica',xIDAnag,'D',xCVNumero+' '+xCognome+' '+xNome+', nato a '+xNatoA+' il '+xNatoIl+' CV ins.il '+xCVInseritoIndata+' ult.agg.il '+xCVDataAgg);

          Data.DB.CommitTrans;
     except
          Data.DB.RollbackTrans;
          MessageDlg('Errore nel database: cancellazione non eseguita',mtError, [mbOK],0);
          raise;
     end;
end;

function GetWinTempPath: string;
var s: pchar;
begin
     s:=stralloc(255);
     gettemppath(255,s);
     Result:=s;
     strdispose(s);
end;

procedure ScriviRegistry(xChiave,xValore: string);
begin
     xReg:=TRegistry.create;
     xReg.RootKey:=HKEY_CURRENT_USER;
     xReg.OpenKey('\Software\H1',True);
     xReg.WriteString(xChiave,xValore);
     xReg.CloseKey;
     xReg.Free;
end;

function LeggiRegistry(xChiave: string): string;
begin
     Result:='';
     xReg:=TRegistry.create;
     xReg.RootKey:=HKEY_CURRENT_USER;
     xReg.OpenKey('\Software\H1',True);
     Result:=xReg.ReadString(xChiave);
     xReg.CloseKey;
     xReg.Free;
end;

end.

