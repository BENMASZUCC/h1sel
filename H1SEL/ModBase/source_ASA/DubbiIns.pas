unit DubbiIns;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, StdCtrls, Buttons, Grids, DBGrids, DBTables;

type
  TDubbiInsForm = class(TForm)
    QDubbi: TQuery;
    dsQDubbi: TDataSource;
    QDubbiID: TIntegerField;
    QDubbiCognome: TStringField;
    QDubbiNome: TStringField;
    QDubbiDubbiIns: TStringField;
    DBGrid1: TDBGrid;
    BitBtn1: TBitBtn;
    QDubbiCVInseritoIndata: TDateTimeField;
    BitBtn2: TBitBtn;
    procedure BitBtn2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DubbiInsForm: TDubbiInsForm;

implementation

{$R *.DFM}

procedure TDubbiInsForm.BitBtn2Click(Sender: TObject);
begin
     close
end;

procedure TDubbiInsForm.FormShow(Sender: TObject);
begin
     Caption:='[M/055] - '+Caption;
end;

end.
