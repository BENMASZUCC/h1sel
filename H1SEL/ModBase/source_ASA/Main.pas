unit Main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBCGrids, StdCtrls, DBCtrls, Mask, Grids, DBGrids, ExtCtrls, TB97, Wall,
  ComCtrls, LookOut, Menus, ImgList, TimeLine, Series, Spin, ComObj,
  TeEngine, TeeFunci, TeeProcs, Chart, DBChart, ImageWindow, DtEdit97,
  DtEdDB97, Buttons, DB, FileCtrl, CaptionButton, AdvGrid, dxorgchr,
  dxdborgc, ArrowBtn, RXDBCtrl, Psock, NMsmtp, CliCandidati, dxPSCore,
  dxPSdxOCLnk, dxPSdxDBOCLnk, FrmListinoAnnunci, TipiAnnunciFrame,
  FrmClienteAnnunci, FrmContrattiAnnunci, dxCntner, dxExEdtr, dxEdLib, dxPSdxDBGrLnk,
  dxDBELib, dxTL, dxDBCtrl, dxDBGrid, DBTables, dxDBTLCl, dxGrClms, dxGridMenus,
  dxPSdxTLLnk, FrameDBRichEdit2, dxTLClms, dxLayout, dxGrClEx, ShellApi,
  dxEditor, dxExGrEd, dxExELib, dxPSdxDBCtrlLnk, dxPSGraphicLnk, ClientiExt,
  FR_ADODB, FR_BDEDB, FR_Class, FR_DSet, FR_DBSet;

type
  TSaveMethod = procedure(const FileName: string; ASaveAll: Boolean) of object;

  TMainForm = class(TForm)
    PageControl1: TPageControl;
    ImageList1: TImageList;
    TSAnag: TTabSheet;
    Panel1: TPanel;
    Splitter2: TSplitter;
    PanPCSoggetti: TPanel;
    Panel4: TPanel;
    TSTabelle: TTabSheet;
    PageControl3: TPageControl;
    TSTabFlussi: TTabSheet;
    Panel11: TPanel;
    Edit1: TEdit;
    DBGrid6: TDBGrid;
    TSInizio: TTabSheet;
    Panel22: TPanel;
    Image1: TImage;
    TsTabCompetenze: TTabSheet;
    Wallpaper8: TWallpaper;
    TbBCompNew: TToolbarButton97;
    TbBCompDel: TToolbarButton97;
    TbBCompOK: TToolbarButton97;
    TSTabCaratt: TTabSheet;
    Wallpaper9: TWallpaper;
    TbBCarattNew: TToolbarButton97;
    TbBCarattDel: TToolbarButton97;
    TbBCarattOK: TToolbarButton97;
    TbBCarattCan: TToolbarButton97;
    DBGrid21: TDBGrid;
    TsTabMansioni: TTabSheet;
    Panel20: TPanel;
    Panel21: TPanel;
    Splitter7: TSplitter;
    Wallpaper10: TWallpaper;
    TbBMansNew: TToolbarButton97;
    TbBMansDel: TToolbarButton97;
    TbBMansMod: TToolbarButton97;
    Panel25: TPanel;
    Wallpaper11: TWallpaper;
    TbBCompMansNew: TToolbarButton97;
    TbBCompMansDel: TToolbarButton97;
    TbBCompMansOK: TToolbarButton97;
    TbBCompMansCan: TToolbarButton97;
    DBGrid23: TDBGrid;
    TSTabAltro: TTabSheet;
    Panel32: TPanel;
    Panel33: TPanel;
    Wallpaper15: TWallpaper;
    TbBAreeNew: TToolbarButton97;
    TbBAreeDel: TToolbarButton97;
    TbBAreeMod: TToolbarButton97;
    DBGrid24: TDBGrid;
    Splitter9: TSplitter;
    Panel34: TPanel;
    Label20: TLabel;
    DBEdit42: TDBEdit;
    Panel61: TPanel;
    Splitter14: TSplitter;
    Panel62: TPanel;
    Wallpaper1: TWallpaper;
    TbBEventiNew: TToolbarButton97;
    TbBEventiDel: TToolbarButton97;
    TbBEventiOK: TToolbarButton97;
    TbBEventiCan: TToolbarButton97;
    Panel64: TPanel;
    Panel65: TPanel;
    Wallpaper22: TWallpaper;
    TbBStatiNew: TToolbarButton97;
    TbBStatiDel: TToolbarButton97;
    TbBStatiOK: TToolbarButton97;
    TbBStatiCan: TToolbarButton97;
    DBGrid38: TDBGrid;
    Panel67: TPanel;
    TSTabDiplomi: TTabSheet;
    Panel68: TPanel;
    DBGrid39: TDBGrid;
    Panel69: TPanel;
    Wallpaper23: TWallpaper;
    TbBDiplomiNew: TToolbarButton97;
    TbBDiplomiDel: TToolbarButton97;
    TbBDiplomiOK: TToolbarButton97;
    TbBDiplomiCan: TToolbarButton97;
    Panel71: TPanel;
    Panel70: TPanel;
    Wallpaper24: TWallpaper;
    TbBAreeDiplNew: TToolbarButton97;
    TbBAreeDiplDel: TToolbarButton97;
    TbBAreeDiplOK: TToolbarButton97;
    TbBAreeDiplCan: TToolbarButton97;
    DBGrid40: TDBGrid;
    Label13: TLabel;
    OpenDialog1: TOpenDialog;
    TbBCompBack: TToolbarButton97;
    TbBCarattBack: TToolbarButton97;
    Panel88: TPanel;
    StatusBar1: TStatusBar;
    PopupMenu1: TPopupMenu;
    CopiadaAppunti1: TMenuItem;
    EliminaFoto1: TMenuItem;
    adattastretch1: TMenuItem;
    ImageList2: TImageList;
    PopupMenu2: TPopupMenu;
    Nuovosottonodo1: TMenuItem;
    Eliminanodo1: TMenuItem;
    TbBEventiMod: TToolbarButton97;
    Panel37: TPanel;
    LookOut1: TLookOut;
    LOPDipendenti: TLookOutPage;
    LOBSchede: TLookOutButton;
    LOBRicerche: TLookOutButton;
    LOPTabelle: TLookOutPage;
    LOBTab1: TLookOutButton;
    LOBTab2: TLookOutButton;
    LOBTab3: TLookOutButton;
    LOBTab4: TLookOutButton;
    LOBTab11: TLookOutButton;
    LOBTab7: TLookOutButton;
    TSPromemoria: TTabSheet;
    Dock973: TDock97;
    TbAnag: TToolbar97;
    BAnagEvento: TToolbarButton97;
    BAnagCurriculum: TToolbarButton97;
    BbValutaz: TToolbarButton97;
    TbMain: TToolbar97;
    BEsci: TToolbarButton97;
    BVediCV: TToolbarButton97;
    Panel91: TPanel;
    Panel92: TPanel;
    DBGrid2: TDBGrid;
    ToolbarButton978: TToolbarButton97;
    Panel113: TPanel;
    DBGrid20: TDBGrid;
    Panel114: TPanel;
    Panel115: TPanel;
    Panel116: TPanel;
    TbBAreeCompNew: TToolbarButton97;
    TbBAreeCompDel: TToolbarButton97;
    TbBAreeCompMod: TToolbarButton97;
    DBGrid12: TDBGrid;
    PageControl5: TPageControl;
    TsStatoDip: TTabSheet;
    Panel80: TPanel;
    TsStatoEsterno: TTabSheet;
    Panel81: TPanel;
    TsStatoUscito: TTabSheet;
    Panel82: TPanel;
    Label45: TLabel;
    Label67: TLabel;
    TSClientiEBC: TTabSheet;
    Splitter3: TSplitter;
    Panel5: TPanel;
    Panel8: TPanel;
    Label21: TLabel;
    EClienti: TEdit;
    DBGClienti: TDBGrid;
    Panel16: TPanel;
    LOBClientiEBC: TLookOutButton;
    TSRicerche: TTabSheet;
    Panel58: TPanel;
    Wallpaper43: TWallpaper;
    BNuovaRicerca: TToolbarButton97;
    BRiprendiRic: TToolbarButton97;
    ToolbarButton9719: TToolbarButton97;
    BSpecRic: TToolbarButton97;
    Panel56: TPanel;
    LOBPromemoria: TLookOutButton;
    LOBArchMess: TLookOutButton;
    TSAnnunci: TTabSheet;
    Panel13: TPanel;
    PanAnnunci: TPanel;
    Splitter1: TSplitter;
    Panel14: TPanel;
    PCAnnunci: TPageControl;
    TSAnnDett: TTabSheet;
    Panel38: TPanel;
    Wallpaper21: TWallpaper;
    TbBAnnNew: TToolbarButton97;
    TbBAnnDel: TToolbarButton97;
    TbBAnnOK: TToolbarButton97;
    TbBAnnCan: TToolbarButton97;
    TSAnnDate: TTabSheet;
    Splitter6: TSplitter;
    Panel45: TPanel;
    Wallpaper27: TWallpaper;
    TbBPubbNew: TToolbarButton97;
    TbBPubbDel: TToolbarButton97;
    Panel50: TPanel;
    TSAnnCVPerv: TTabSheet;
    Panel51: TPanel;
    LOBAnnunci: TLookOutButton;
    TSTabAnn: TTabSheet;
    PageControl8: TPageControl;
    TSTabTestate: TTabSheet;
    Splitter11: TSplitter;
    Panel63: TPanel;
    DBGrid36: TDBGrid;
    Panel72: TPanel;
    Wallpaper29: TWallpaper;
    TbBTestNew: TToolbarButton97;
    TbBTestDel: TToolbarButton97;
    TbBTestOK: TToolbarButton97;
    Panel76: TPanel;
    Panel77: TPanel;
    Wallpaper31: TWallpaper;
    TbBEdizTestNew: TToolbarButton97;
    TbBEdizTestDel: TToolbarButton97;
    TbBEdizTestOK: TToolbarButton97;
    Panel78: TPanel;
    Panel79: TPanel;
    PCInfoEdizione: TPageControl;
    TSEdizPenetr: TTabSheet;
    DBGrid43: TDBGrid;
    Panel83: TPanel;
    TbBPenetrazNew: TToolbarButton97;
    TbBPenetrazDel: TToolbarButton97;
    TbBPenetrazOK: TToolbarButton97;
    TbBPenetrazCan: TToolbarButton97;
    TSEdizSconti: TTabSheet;
    DBGrid44: TDBGrid;
    Panel84: TPanel;
    TbBScontiNew: TToolbarButton97;
    TbBScontiDel: TToolbarButton97;
    TbBScontiOK: TToolbarButton97;
    TbBScontiCan: TToolbarButton97;
    TSEdizGiorni: TTabSheet;
    Label130: TLabel;
    DBGrid45: TDBGrid;
    DBLookupComboBox6: TDBLookupComboBox;
    Panel85: TPanel;
    TbBGiorniNew: TToolbarButton97;
    TbBGiorniDel: TToolbarButton97;
    TbBGiorniOK: TToolbarButton97;
    TbBGiorniCan: TToolbarButton97;
    TsTabContrattiAnn: TTabSheet;
    LOBTabAnn: TLookOutButton;
    LOBTabAttiv: TLookOutButton;
    TSTabAttiv: TTabSheet;
    PageControl4: TPageControl;
    TSCliAttivi: TTabSheet;
    Panel23: TPanel;
    TSCliNonAttivi: TTabSheet;
    Panel26: TPanel;
    TSCliEliminati: TTabSheet;
    Panel27: TPanel;
    TSCliTutti: TTabSheet;
    Panel28: TPanel;
    LOBSelPers: TLookOutButton;
    BAnagFileWord: TToolbarButton97;
    TSTabAziende: TTabSheet;
    LOBTabAziende: TLookOutButton;
    TSStatoTutti: TTabSheet;
    Panel41: TPanel;
    TSTabLingue: TTabSheet;
    LOBTab8: TLookOutButton;
    Label18: TLabel;
    DBText2: TDBText;
    Panel46: TPanel;
    RGAnnFiltro: TRadioGroup;
    TbBPubbModData: TToolbarButton97;
    Image2: TImage;
    TSTabCom: TTabSheet;
    Panel49: TPanel;
    DBGrid11: TDBGrid;
    Panel52: TPanel;
    Wallpaper18: TWallpaper;
    ToolbarButton971: TToolbarButton97;
    Label23: TLabel;
    ECercaCom: TEdit;
    Panel53: TPanel;
    Panel54: TPanel;
    Wallpaper25: TWallpaper;
    ToolbarButton9711: TToolbarButton97;
    ToolbarButton9714: TToolbarButton97;
    ToolbarButton9731: TToolbarButton97;
    DBGrid16: TDBGrid;
    LOBTabComuni: TLookOutButton;
    ToolbarButton9732: TToolbarButton97;
    ToolbarButton9733: TToolbarButton97;
    ToolbarButton9734: TToolbarButton97;
    LTotCand: TLabel;
    LTotAz: TLabel;
    LTotRic: TLabel;
    ToolbarButton9737: TToolbarButton97;
    ToolbarButton9738: TToolbarButton97;
    BCambiaUtente: TToolbarButton97;
    RiepTiming: TToolbarButton97;
    Panel57: TPanel;
    ToolbarButton9736: TToolbarButton97;
    ToolbarButton9739: TToolbarButton97;
    ToolbarButton9740: TToolbarButton97;
    TsStatoEl: TTabSheet;
    ToolbarButton9743: TToolbarButton97;
    LOBAziende: TLookOutButton;
    TSAziende: TTabSheet;
    Panel59: TPanel;
    DBEdit43: TDBEdit;
    Panel60: TPanel;
    Panel95: TPanel;
    Splitter10: TSplitter;
    PCAziendaOrg: TPageControl;
    TSAziendeOrgCand: TTabSheet;
    Panel102: TPanel;
    ToolbarButton9744: TToolbarButton97;
    TSAziendaOrgDett: TTabSheet;
    PanOrg: TPanel;
    PanOrgDett: TPanel;
    Label57: TLabel;
    GroupBox14: TGroupBox;
    DBEdit69: TDBEdit;
    DBEdit47: TDBEdit;
    GroupBox9: TGroupBox;
    SpeedButton9: TSpeedButton;
    SpeedButton10: TSpeedButton;
    DBEdit48: TDBEdit;
    Panel98: TPanel;
    DBCheckBox6: TDBCheckBox;
    Panel99: TPanel;
    DBCheckBox5: TDBCheckBox;
    TSAziendaOrgDati: TTabSheet;
    PanAzienda: TPanel;
    Label33: TLabel;
    Label36: TLabel;
    Label40: TLabel;
    Label37: TLabel;
    Label43: TLabel;
    Label55: TLabel;
    DBEDenomAzienda: TDBEdit;
    GroupBox6: TGroupBox;
    Label52: TLabel;
    Label53: TLabel;
    Label54: TLabel;
    DBEdit34: TDBEdit;
    DBEdit35: TDBEdit;
    DBEdit36: TDBEdit;
    DbDateEdit974: TDbDateEdit97;
    DBEdit30: TDBEdit;
    DBMemo1: TDBMemo;
    DBEdit37: TDBEdit;
    DBEdit38: TDBEdit;
    Panel94: TPanel;
    TbBOrganigOK: TToolbarButton97;
    TbBOrganigCan: TToolbarButton97;
    TbBOrganigMod: TToolbarButton97;
    PopOrg: TPopupMenu;
    eliminasoggetto1: TMenuItem;
    eliminanodo2: TMenuItem;
    TVAziendaCand: TTreeView;
    PopupMenu3: TPopupMenu;
    Infocandidato1: TMenuItem;
    curriculum1: TMenuItem;
    documenti1: TMenuItem;
    fileWord1: TMenuItem;
    ImageList3: TImageList;
    Label32: TLabel;
    modificaattuale1: TMenuItem;
    BAzSearch: TToolbarButton97;
    eliminasoggetto2: TMenuItem;
    GBDatiAzienda: TGroupBox;
    Label59: TLabel;
    DBEdit52: TDBEdit;
    Label62: TLabel;
    DBEdit53: TDBEdit;
    Label64: TLabel;
    DBEdit55: TDBEdit;
    GroupBox11: TGroupBox;
    DBEdit56: TDBEdit;
    DBEdit57: TDBEdit;
    DBEdit58: TDBEdit;
    bDubbiIns: TToolbarButton97;
    Panel101: TPanel;
    Panel103: TPanel;
    Wallpaper30: TWallpaper;
    DBGrid26: TDBGrid;
    BRisNew: TToolbarButton97;
    BRisMod: TToolbarButton97;
    BRisDel: TToolbarButton97;
    LPropCV: TLabel;
    BAnagSearch: TSpeedButton;
    Panel106: TPanel;
    Panel109: TPanel;
    BOrgNewNodo: TToolbarButton97;
    BOrgNewNewChildNodo: TToolbarButton97;
    BOrgNewDelNodo: TToolbarButton97;
    GroupBox12: TGroupBox;
    Label75: TLabel;
    DBEdit60: TDBEdit;
    Label76: TLabel;
    DBEdit61: TDBEdit;
    DBComboBox5: TDBComboBox;
    Label77: TLabel;
    PColor: TPanel;
    Label78: TLabel;
    DBComboBox6: TDBComboBox;
    Label79: TLabel;
    Label80: TLabel;
    DBEdit62: TDBEdit;
    ColorDialog: TColorDialog;
    EPropCV: TEdit;
    BPropCV: TSpeedButton;
    DBTree: TdxDbOrgChart;
    associaanodo1: TMenuItem;
    BOrgCandDel: TSpeedButton;
    ToolbarButton973: TToolbarButton97;
    TbClienti: TToolbar97;
    BRicClienti: TToolbarButton97;
    TSCliContattati: TTabSheet;
    Panel129: TPanel;
    CBRicUtentiTutti: TCheckBox;
    GroupBox5: TGroupBox;
    Label24: TLabel;
    DBEdit23: TDBEdit;
    Panel130: TPanel;
    BGlobalOK: TToolbarButton97;
    BGlobalCan: TToolbarButton97;
    BCliSearch: TSpeedButton;
    TbBMansModArea: TToolbarButton97;
    NMSMTP1: TNMSMTP;
    BOfferte: TToolbarButton97;
    Panel131: TPanel;
    Panel132: TPanel;
    Wallpaper20: TWallpaper;
    BTipoCommNew: TToolbarButton97;
    BTipoCommMod: TToolbarButton97;
    BTipoCommDel: TToolbarButton97;
    RxDBGrid2: TRxDBGrid;
    DBGrid30: TDBGrid;
    Panel133: TPanel;
    Panel134: TPanel;
    BSedeNew: TToolbarButton97;
    ToolbarButton9748: TToolbarButton97;
    ToolbarButton9749: TToolbarButton97;
    Panel135: TPanel;
    Panel136: TPanel;
    Wallpaper32: TWallpaper;
    BProvNew: TToolbarButton97;
    BProvMod: TToolbarButton97;
    BProvDel: TToolbarButton97;
    DBGrid31: TDBGrid;
    BOrgNewStampa: TToolbarButton97;
    BOrgStandard: TToolbarButton97;
    BAssociaNew: TToolbarButton97;
    TSTabNaz: TTabSheet;
    Panel137: TPanel;
    Panel138: TPanel;
    Wallpaper33: TWallpaper;
    Label112: TLabel;
    ECercaNazione: TEdit;
    LOBTabNazioni: TLookOutButton;
    DBGrid33: TDBGrid;
    BNazNew: TToolbarButton97;
    BNazMod: TToolbarButton97;
    BNazDel: TToolbarButton97;
    Label118: TLabel;
    DBEdit110: TDBEdit;
    Label44: TLabel;
    Label119: TLabel;
    Label120: TLabel;
    DBEdit32: TDBEdit;
    DBComboBox11: TDBComboBox;
    DBEdit111: TDBEdit;
    BStoricoAnagPos: TToolbarButton97;
    StoricoPosizioni91: TMenuItem;
    PopupOrg: TPopupMenu;
    Storicoposizione1: TMenuItem;
    Eliminasoggettoassociato1: TMenuItem;
    TSListini: TTabSheet;
    ListinoAnnunciFrame1: TListinoAnnunciFrame;
    DBGrid28: TDBGrid;
    PCAnnunciDett: TPageControl;
    TSAnnDettRic: TTabSheet;
    Panel40: TPanel;
    Panel44: TPanel;
    DBGrid18: TDBGrid;
    Panel43: TPanel;
    ToolbarButton9712: TToolbarButton97;
    ToolbarButton9726: TToolbarButton97;
    TSAnnDettSpec: TTabSheet;
    Panel104: TPanel;
    GroupBox4: TGroupBox;
    SpeedButton13: TSpeedButton;
    DBEdit19: TDBEdit;
    GroupBox15: TGroupBox;
    BAnnSelSede: TSpeedButton;
    DBEdit73: TDBEdit;
    GroupBox19: TGroupBox;
    BAnnTipo: TSpeedButton;
    DBEdit75: TDBEdit;
    TSTabAnnTipi: TTabSheet;
    Panel105: TPanel;
    FrmTipiAnnunci: TFrmTipiAnnunci;
    TSEdizNote: TTabSheet;
    DBMemo4: TDBMemo;
    Panel139: TPanel;
    ToolbarButton9718: TToolbarButton97;
    ContrattiAnnunciFrame: TContrattiAnnunciFrame;
    Panel10: TPanel;
    Panel15: TPanel;
    Wallpaper19: TWallpaper;
    BTipiStradaNew: TToolbarButton97;
    BTipiStradaDel: TToolbarButton97;
    BTipiStradaMod: TToolbarButton97;
    DBGrid1: TDBGrid;
    DBEdit24: TDBEdit;
    Label25: TLabel;
    LOBTabModelliWord: TLookOutButton;
    TSCliAltre: TTabSheet;
    Panel29: TPanel;
    Label50: TLabel;
    GroupBox18: TGroupBox;
    Label56: TLabel;
    Label58: TLabel;
    DBEdit45: TDBEdit;
    DBEdit46: TDBEdit;
    DBEdit25: TDBEdit;
    SpeedButton7: TSpeedButton;
    TSCliconcess: TTabSheet;
    Panel100: TPanel;
    Label69: TLabel;
    ERifAnnuncio: TEdit;
    BRifAnnuncio: TSpeedButton;
    Panel117: TPanel;
    Panel140: TPanel;
    DBMemo5: TDBMemo;
    Panel141: TPanel;
    BJobDescOK: TToolbarButton97;
    BJobDescCan: TToolbarButton97;
    Splitter12: TSplitter;
    SaveDialog: TSaveDialog;
    Panel145: TPanel;
    Panel146: TPanel;
    Wallpaper28: TWallpaper;
    BMotNonAcc: TToolbarButton97;
    TSAnnEmail: TTabSheet;
    Panel147: TPanel;
    Label94: TLabel;
    DBEdit40: TDBEdit;
    Label95: TLabel;
    DBEdit41: TDBEdit;
    Label96: TLabel;
    DBEdit76: TDBEdit;
    Label102: TLabel;
    DBEdit83: TDBEdit;
    dxDBGrid1: TdxDBGrid;
    dxDBGrid1Progressivo: TdxDBGridMaskColumn;
    dxDBGrid1Cliente: TdxDBGridMaskColumn;
    dxDBGrid1DataInizio: TdxDBGridDateColumn;
    dxDBGrid1NumRicercati: TdxDBGridMaskColumn;
    dxDBGrid1Ruolo: TdxDBGridMaskColumn;
    dxDBGrid1Selezionatore: TdxDBGridMaskColumn;
    dxDBGrid1StatoRic: TdxDBGridMaskColumn;
    dxDBGrid1Tipo: TdxDBGridMaskColumn;
    dxDBGrid1DataPrevChiusura: TdxDBGridDateColumn;
    BLegenda: TToolbarButton97;
    BRicAttiveExp: TToolbarButton97;
    PMRicAttive: TPopupMenu;
    EsportainExcel1: TMenuItem;
    dxPrinter1: TdxComponentPrinter;
    dxPrinter1Link1: TdxDBOrgChartReportLink;
    EsportainHTML1: TMenuItem;
    dxPrinter1Link2: TdxDBGridReportLink;
    Stampagriglia1: TMenuItem;
    PanDatiAnnuncio: TPanel;
    Splitter17: TSplitter;
    Label39: TLabel;
    Label15: TLabel;
    Label104: TLabel;
    Label74: TLabel;
    SpeedButton11: TSpeedButton;
    DBMemo11: TDBMemo;
    DBEdit26: TDBEdit;
    DBCheckBox2: TDBCheckBox;
    DbDateEdit975: TDbDateEdit97;
    DBCheckBox1: TDBCheckBox;
    DBEdit59: TDBEdit;
    GBCliente: TGroupBox;
    BTrovaCliente: TSpeedButton;
    DBEdit72: TDBEdit;
    dxDBGrid2: TdxDBGrid;
    dxDBGrid2Rif: TdxDBGridMaskColumn;
    dxDBGrid2Data: TdxDBGridDateColumn;
    dxDBGrid2Civetta: TdxDBGridCheckColumn;
    TSAnnTesto: TTabSheet;
    Wallpaper34: TWallpaper;
    ToolbarButton9751: TToolbarButton97;
    ToolbarButton9752: TToolbarButton97;
    FrameDBRich1: TFrameDBRich;
    Panel149: TPanel;
    Label34: TLabel;
    SpeedButton1: TSpeedButton;
    SpeedButton14: TSpeedButton;
    DBEdit16: TDBEdit;
    BAnnSedeDel: TSpeedButton;
    BAnnCommModCodice: TToolbarButton97;
    TSAnnRuoli: TTabSheet;
    Panel150: TPanel;
    Panel151: TPanel;
    Panel152: TPanel;
    BAnnRuoloAdd: TToolbarButton97;
    BAnnRuoloDel: TToolbarButton97;
    BAnnRuoloCod: TToolbarButton97;
    dxDBGrid3: TdxDBGrid;
    dxDBGrid3ID: TdxDBGridMaskColumn;
    dxDBGrid3IDAnnuncio: TdxDBGridMaskColumn;
    dxDBGrid3IDMansione: TdxDBGridMaskColumn;
    dxDBGrid3Codice: TdxDBGridMaskColumn;
    dxDBGrid3Ruolo: TdxDBGridMaskColumn;
    dxDBGrid4: TdxDBGrid;
    dxDBGrid4ID: TdxDBGridMaskColumn;
    dxDBGrid4IDAnnuncio: TdxDBGridMaskColumn;
    dxDBGrid4IDEdizione: TdxDBGridMaskColumn;
    dxDBGrid4Data: TdxDBGridDateColumn;
    dxDBGrid4CostoEuro: TdxDBGridMaskColumn;
    dxDBGrid4Testata: TdxDBGridMaskColumn;
    dxDBGrid4Edizione: TdxDBGridMaskColumn;
    dxDBGrid4TipoGiorno: TdxDBGridColumn;
    dxDBGrid4Giorno: TdxDBGridColumn;
    dxDBGrid4NumModuli: TdxDBGridMaskColumn;
    dxDBGrid4Codice: TdxDBGridMaskColumn;
    BannDataModCod: TToolbarButton97;
    GroupBox10: TGroupBox;
    Label31: TLabel;
    DBEdit74: TDBEdit;
    dxDBGrid6: TdxDBGrid;
    dxDBGrid6ID: TdxDBGridMaskColumn;
    dxDBGrid6IDAnnEdizData: TdxDBGridMaskColumn;
    dxDBGrid6DataPervenuto: TdxDBGridDateColumn;
    dxDBGrid6CVNumero: TdxDBGridMaskColumn;
    dxDBGrid6Cognome: TdxDBGridMaskColumn;
    dxDBGrid6Nome: TdxDBGridMaskColumn;
    dxDBGrid7: TdxDBGrid;
    dxDBGrid7ID: TdxDBGridMaskColumn;
    dxDBGrid7IDAnnEdizData: TdxDBGridMaskColumn;
    dxDBGrid7DataPervenuto: TdxDBGridDateColumn;
    dxDBGrid7CVNumero: TdxDBGridMaskColumn;
    dxDBGrid7Cognome: TdxDBGridMaskColumn;
    dxDBGrid7Nome: TdxDBGridMaskColumn;
    Panel47: TPanel;
    BAnnEdizSoggAdd: TToolbarButton97;
    BAnnEdizSoggDel: TToolbarButton97;
    DBGEventi: TdxDBGrid;
    DBGEventiID: TdxDBGridMaskColumn;
    DBGEventiIDdaStato: TdxDBGridMaskColumn;
    DBGEventiDaStato: TdxDBGridLookupColumn;
    DBGEventiEvento: TdxDBGridMaskColumn;
    DBGEventiAStato: TdxDBGridLookupColumn;
    DBGEventiIDaStato: TdxDBGridMaskColumn;
    DBGEventiIDProcedura: TdxDBGridMaskColumn;
    dxPrinter1Link3: TdxDBGridReportLink;
    dxDBGRuoli: TdxDBGrid;
    dxDBGRuoliColumn1: TdxDBGridColumn;
    ToolbarButton9727: TToolbarButton97;
    dxPrinter1Link4: TdxDBGridReportLink;
    TSCliFornitori: TTabSheet;
    Panel118: TPanel;
    TSAziendaOrgRuolo: TTabSheet;
    Panel155: TPanel;
    DBEdit85: TDBEdit;
    Label99: TLabel;
    dxDBGOrgRuolo: TdxDBGrid;
    dxDBGOrgRuoloID: TdxDBGridMaskColumn;
    dxDBGOrgRuoloCVNumero: TdxDBGridMaskColumn;
    dxDBGOrgRuoloCognome: TdxDBGridMaskColumn;
    dxDBGOrgRuoloNome: TdxDBGridMaskColumn;
    Panel156: TPanel;
    Label114: TLabel;
    LTotOrgRuolo: TLabel;
    PopupMenu4: TPopupMenu;
    infocandidato2: TMenuItem;
    curriculum2: TMenuItem;
    documenti2: TMenuItem;
    fileWord2: TMenuItem;
    TSAnnTimeSheet: TTabSheet;
    Panel162: TPanel;
    Panel163: TPanel;
    BTargetList: TToolbarButton97;
    Aggiungiacommessa1: TMenuItem;
    Aggiungiacommessa2: TMenuItem;
    SpeedButton3: TSpeedButton;
    DBCheckBox8: TDBCheckBox;
    GroupBox13: TGroupBox;
    Label115: TLabel;
    DBEdit77: TDBEdit;
    GroupBox16: TGroupBox;
    Label117: TLabel;
    DBEdit86: TDBEdit;
    dxDBGrid1Column17: TdxDBGridColumn;
    dxDBGrid1Column18: TdxDBGridColumn;
    Panel165: TPanel;
    Panel18: TPanel;
    Wallpaper4: TWallpaper;
    TbBAttivitaNew: TToolbarButton97;
    TbBAttivitaDel: TToolbarButton97;
    BSaveToExcel: TToolbarButton97;
    ToolbarButton9715: TToolbarButton97;
    dxDBGAttivita: TdxDBGrid;
    dxDBGAttivitaID: TdxDBGridMaskColumn;
    dxDBGAttivitaAttivita: TdxDBGridMaskColumn;
    Splitter19: TSplitter;
    Panel166: TPanel;
    Panel167: TPanel;
    Wallpaper36: TWallpaper;
    ToolbarButton972: TToolbarButton97;
    ToolbarButton974: TToolbarButton97;
    ToolbarButton976: TToolbarButton97;
    dxDBGrid8: TdxDBGrid;
    dxDBGrid8AreaSettore: TdxDBGridMaskColumn;
    TbBAttivitaOK: TToolbarButton97;
    TbBAttivitaCan: TToolbarButton97;
    dxDBGridLayoutList1: TdxDBGridLayoutList;
    dxDBGridLayoutList1Item1: TdxDBGridLayout;
    dxDBGAttivitaAttivita_ENG: TdxDBGridExtLookupColumn;
    ToolbarButton9750: TToolbarButton97;
    TSAnnCG: TTabSheet;
    PAnnContrattiCG: TPanel;
    Panel168: TPanel;
    Panel169: TPanel;
    Panel170: TPanel;
    GroupBox17: TGroupBox;
    dxDBGridLayoutList1Item2: TdxDBGridLayout;
    dxDBExtLookupEdit1: TdxDBExtLookupEdit;
    Panel171: TPanel;
    ToolbarButton9753: TToolbarButton97;
    ToolbarButton9754: TToolbarButton97;
    dxDBGrid9: TdxDBGrid;
    dxDBGrid9CodiceContratto: TdxDBGridMaskColumn;
    dxDBGrid9Descrizione: TdxDBGridMaskColumn;
    Panel160: TPanel;
    BTimeSheetNew: TToolbarButton97;
    BTimeSheetDel: TToolbarButton97;
    BTimeSheetCan: TToolbarButton97;
    BTimeSheetOK: TToolbarButton97;
    dxDBGrid10: TdxDBGrid;
    dxDBGrid1Utente: TdxDBGridExtLookupColumn;
    dxDBGrid1Column5: TdxDBGridExtLookupColumn;
    dxDBGrid1Data: TdxDBGridDateColumn;
    dxDBGrid1OreConsuntivo: TdxDBGridMaskColumn;
    dxDBGrid1Note: TdxDBGridBlobColumn;
    dxDBGLLUtenti: TdxDBGridLayout;
    dxDBGLLGruppiProf: TdxDBGridLayout;
    Label121: TLabel;
    GroupBox22: TGroupBox;
    dxDBExtLookupEdit2: TdxDBExtLookupEdit;
    dxDBGLLContrattiCG: TdxDBGridLayout;
    dxDBGrid1TotCompDaFatt: TdxDBGridCurrencyColumn;
    Panel161: TPanel;
    ENome: TEdit;
    Label3: TLabel;
    BAnagModifiche: TToolbarButton97;
    TSStatoInternet: TTabSheet;
    Panel164: TPanel;
    CBOrgZoom: TCheckBox;
    CBOrgRotate: TCheckBox;
    BTabDiplomiITA: TSpeedButton;
    BTabDiplomiENG: TSpeedButton;
    PanTabLingue: TPanel;
    Panel42: TPanel;
    Wallpaper17: TWallpaper;
    TbBLingueNew: TToolbarButton97;
    TbBLingueDel: TToolbarButton97;
    TbBLingueOK: TToolbarButton97;
    TbBLingueCan: TToolbarButton97;
    BTabLivLingue: TToolbarButton97;
    DBGrid10: TDBGrid;
    dxDBGAttivitaColumn4: TdxDBGridColumn;
    BAreeRuoliITA: TSpeedButton;
    BAreeRuoliENG: TSpeedButton;
    PageControl2: TPageControl;
    TSAnagDatiAnag: TTabSheet;
    Label28: TLabel;
    PanAnag: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label5: TLabel;
    Label38: TLabel;
    Label100: TLabel;
    Label101: TLabel;
    Label35: TLabel;
    Label17: TLabel;
    Label11: TLabel;
    BStatoCivile: TSpeedButton;
    DBECognome: TDBEdit;
    DBEdit2: TDBEdit;
    DBComboBox1: TDBComboBox;
    GroupBox1: TGroupBox;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    SpeedButton2: TSpeedButton;
    LTipoStradaRes: TLabel;
    LIndirizzoRes: TLabel;
    Label110: TLabel;
    BAnagSelNazione1: TSpeedButton;
    BCopiaDaDom: TSpeedButton;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBCBTipoStradaRes: TDBComboBox;
    DBEdit54: TDBEdit;
    DBEdit82: TDBEdit;
    DBEIndirizzoRes: TDBEdit;
    DBEdit65: TDBEdit;
    DBEdit12: TDBEdit;
    DbDateEdit971: TDbDateEdit97;
    DBEdit1: TDBEdit;
    DBEdit14: TDBEdit;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    Label19: TLabel;
    Label22: TLabel;
    SpeedButton4: TSpeedButton;
    BCopiadaRes: TSpeedButton;
    LTipoStradaDom: TLabel;
    LIndirizzoDom: TLabel;
    Label111: TLabel;
    BAnagSelNazione2: TSpeedButton;
    DBEdit4: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBCBTipoStradaDom: TDBComboBox;
    DBEdit79: TDBEdit;
    DBEdit93: TDBEdit;
    DBEIndirizzoDom: TDBEdit;
    DBLookupComboBox1: TDBLookupComboBox;
    Wallpaper3: TWallpaper;
    TbBAnagNew: TToolbarButton97;
    TbBAnagDel: TToolbarButton97;
    TbBAnagMod: TToolbarButton97;
    TbBAnagCan: TToolbarButton97;
    TbBAnagOK: TToolbarButton97;
    Label109: TLabel;
    BCandUtente: TSpeedButton;
    ECandUtente: TEdit;
    PanTipologia: TPanel;
    Label6: TLabel;
    DBText6: TDBText;
    BAnagTipologia: TSpeedButton;
    Panel9: TPanel;
    Label42: TLabel;
    PanAnag2: TPanel;
    Panel24: TPanel;
    Label46: TLabel;
    Label47: TLabel;
    Label51: TLabel;
    Label85: TLabel;
    DBEdit13: TDBEdit;
    DBEdit15: TDBEdit;
    DBEdit33: TDBEdit;
    DBEdit70: TDBEdit;
    PanCF: TPanel;
    Label49: TLabel;
    LabPartitaIVA: TLabel;
    SpeedButton5: TSpeedButton;
    DBEdit17: TDBEdit;
    DBEPartitaIVA: TDBEdit;
    PanOldCV: TPanel;
    Label86: TLabel;
    DBEdit71: TDBEdit;
    DBCheckBox4: TDBCheckBox;
    DbAnagFoto: TdxDBGraphicEdit;
    TSAnagRicerche: TTabSheet;
    Panel126: TPanel;
    Label88: TLabel;
    Label90: TLabel;
    Label10: TLabel;
    Label89: TLabel;
    Panel127: TPanel;
    DBEdit21: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit22: TDBEdit;
    DBGrid7: TDBGrid;
    Wallpaper13: TWallpaper;
    ToolbarButton977: TToolbarButton97;
    BInsRicerca: TToolbarButton97;
    ToolbarButton9729: TToolbarButton97;
    ToolbarButton9730: TToolbarButton97;
    Panel12: TPanel;
    Panel48: TPanel;
    CBFiltroAnagRic: TCheckBox;
    TSAnagComp: TTabSheet;
    DBGrid14: TDBGrid;
    Wallpaper6: TWallpaper;
    TbBCompDipNew: TToolbarButton97;
    TbBCompDipDel: TToolbarButton97;
    ToolbarButton979: TToolbarButton97;
    ToolbarButton9710: TToolbarButton97;
    ToolbarButton9716: TToolbarButton97;
    Panel73: TPanel;
    TsAnagMans: TTabSheet;
    Splitter4: TSplitter;
    Panel75: TPanel;
    Wallpaper26: TWallpaper;
    TbBAnagMansNew: TToolbarButton97;
    TbBAnagMansDel: TToolbarButton97;
    Label65: TLabel;
    SpeedButton8: TSpeedButton;
    TbBAnagMansMod: TToolbarButton97;
    BAnagRuoliITA: TSpeedButton;
    BAnagRuoliENG: TSpeedButton;
    DBEDubbiIns: TDBEdit;
    DBGrid41: TDBGrid;
    Panel17: TPanel;
    Panel74: TPanel;
    Wallpaper7: TWallpaper;
    TbBCarattDipNew: TToolbarButton97;
    TbBCarattDipDel: TToolbarButton97;
    ToolbarButton975: TToolbarButton97;
    TbBCarattDipMod: TToolbarButton97;
    DBGrid19: TDBGrid;
    TSAnagStorico: TTabSheet;
    Panel7: TPanel;
    Splitter15: TSplitter;
    DBGrid3: TDBGrid;
    Panel55: TPanel;
    TbBGestStorico: TToolbarButton97;
    PanAnagIncompCli: TPanel;
    Panel30: TPanel;
    Panel142: TPanel;
    BAnagIncompNew: TToolbarButton97;
    BAnagIncompDel: TToolbarButton97;
    BAnagIncompModDesc: TToolbarButton97;
    DBGrid5: TDBGrid;
    Panel19: TPanel;
    TSAnagNoteInt: TTabSheet;
    Splitter18: TSplitter;
    MemoNoteCand: TMemo;
    Panel128: TPanel;
    FrameDBRichAnagNote: TFrameDBRich;
    dxDBGrid5: TdxDBGrid;
    dxDBGrid5ID: TdxDBGridMaskColumn;
    dxDBGrid5Progressivo: TdxDBGridMaskColumn;
    dxDBGrid5Cliente: TdxDBGridColumn;
    dxDBGrid5Note: TdxDBGridBlobColumn;
    Panel153: TPanel;
    PanTitoloMansione: TPanel;
    DBText5: TDBText;
    TSAnagFile: TTabSheet;
    Panel123: TPanel;
    BAnagFileNew: TToolbarButton97;
    BAnagFileMod: TToolbarButton97;
    BAnagFileDel: TToolbarButton97;
    BFileCandOpen: TToolbarButton97;
    ToolbarButton9745: TToolbarButton97;
    BFileDaModello: TToolbarButton97;
    Panel124: TPanel;
    RxDBGrid1: TRxDBGrid;
    PanPCClienti: TPanel;
    PCClienti: TPageControl;
    TSClientiDett: TTabSheet;
    Panel6: TPanel;
    Wallpaper2: TWallpaper;
    TbBClientiNew: TToolbarButton97;
    TbBClientiDel: TToolbarButton97;
    TbBClientiMod: TToolbarButton97;
    TbBClientiCan: TToolbarButton97;
    TbBClientiOK: TToolbarButton97;
    ToolbarButton9742: TToolbarButton97;
    BCliAltriSettori: TToolbarButton97;
    PanCliente: TPanel;
    Label29: TLabel;
    Label70: TLabel;
    Label143: TLabel;
    Label148: TLabel;
    Label16: TLabel;
    SpeedButton6: TSpeedButton;
    DBEDenomCliente: TDBEdit;
    GroupBox20: TGroupBox;
    Label137: TLabel;
    Label138: TLabel;
    DBEdit98: TDBEdit;
    DBEdit99: TDBEdit;
    DBEdit104: TDBEdit;
    DbDateEdit976: TDbDateEdit97;
    DBComboBox2: TDBComboBox;
    DBEdit28: TDBEdit;
    GroupBox8: TGroupBox;
    LTipoStradaCli: TLabel;
    Label63: TLabel;
    Label71: TLabel;
    Label72: TLabel;
    LIndirizzoCli: TLabel;
    Label113: TLabel;
    BCliSelNaz: TSpeedButton;
    SpeedButton12: TSpeedButton;
    DBEdit64: TDBEdit;
    DBEdit66: TDBEdit;
    DBEdit67: TDBEdit;
    DBCBTipoStradaCli: TDBComboBox;
    DBEdit78: TDBEdit;
    DBEdit103: TDBEdit;
    DBEIndirizzoCli: TDBEdit;
    DBCheckBox7: TDBCheckBox;
    PanCliContatti: TPanel;
    Panel97: TPanel;
    Label48: TLabel;
    Label14: TLabel;
    Label149: TLabel;
    Label41: TLabel;
    Wallpaper14: TWallpaper;
    TbBContattiCliNew: TToolbarButton97;
    TbBContattiCliDel: TToolbarButton97;
    TbBContattiCliMod: TToolbarButton97;
    TbBContattiCliAltriDati: TToolbarButton97;
    BCliInviaMail: TToolbarButton97;
    Panel2: TPanel;
    DBCtrlGrid1: TDBCtrlGrid;
    Label68: TLabel;
    Label82: TLabel;
    DBENomeContatto: TDBEdit;
    DBEdit39: TDBEdit;
    DBMemo7: TDBMemo;
    DBEdit109: TDBEdit;
    DBMemo2: TDBMemo;
    DBEdit18: TDBEdit;
    TSClientiRic: TTabSheet;
    Splitter8: TSplitter;
    Label26: TLabel;
    DBGrid13: TDBGrid;
    PanDettRicerca: TPanel;
    Label97: TLabel;
    Label98: TLabel;
    Label103: TLabel;
    Panel31: TPanel;
    DbDateEdit973: TDbDateEdit97;
    DBEdit68: TDBEdit;
    DBEdit29: TDBEdit;
    GroupBox3: TGroupBox;
    DBText3: TDBText;
    Label27: TLabel;
    DBEdit31: TDBEdit;
    Panel112: TPanel;
    RGFiltroRicCli: TRadioGroup;
    PanCliCompensi: TPanel;
    PanTitoloCompensi: TPanel;
    Panel96: TPanel;
    BCompensoNew: TToolbarButton97;
    BCompensoMod: TToolbarButton97;
    BCompensoDel: TToolbarButton97;
    BAssFattDel: TToolbarButton97;
    Panel121: TPanel;
    Label154: TLabel;
    DBText1: TDBText;
    DBText4: TDBText;
    Label66: TLabel;
    Label73: TLabel;
    DBGrid25: TDBGrid;
    TSClientiCV: TTabSheet;
    Splitter16: TSplitter;
    CliCandidatiFrame1: TCliCandidatiFrame;
    Panel143: TPanel;
    Panel144: TPanel;
    BCliRefCliNew: TToolbarButton97;
    BCliRefCliDEl: TToolbarButton97;
    BCliRefCliModDesc: TToolbarButton97;
    Label81: TLabel;
    DBGrid34: TDBGrid;
    Panel148: TPanel;
    DBCheckBox3: TDBCheckBox;
    TSClientiAnnunci: TTabSheet;
    ClienteAnnunciFrame: TClienteAnnunciFrame;
    TSClientiNoteSpese: TTabSheet;
    Panel66: TPanel;
    BNotaSpeseNew: TToolbarButton97;
    BNotaSpeseMod: TToolbarButton97;
    BNotaSpeseDel: TToolbarButton97;
    Panel86: TPanel;
    DBGrid17: TDBGrid;
    Panel87: TPanel;
    Label60: TLabel;
    Label61: TLabel;
    Panel89: TPanel;
    DBEdit44: TDBEdit;
    DBEdit49: TDBEdit;
    GroupBox7: TGroupBox;
    DBEdit50: TDBEdit;
    DBEdit51: TDBEdit;
    TSClientiFatt: TTabSheet;
    PanCliPag: TPanel;
    Label139: TLabel;
    Label140: TLabel;
    Label141: TLabel;
    Label142: TLabel;
    Panel107: TPanel;
    DBEdit100: TDBEdit;
    DBEdit101: TDBEdit;
    DBEdit102: TDBEdit;
    GroupBox21: TGroupBox;
    Label145: TLabel;
    Label146: TLabel;
    Label147: TLabel;
    Label106: TLabel;
    Label107: TLabel;
    Label108: TLabel;
    Label116: TLabel;
    DBEdit106: TDBEdit;
    DBEdit107: TDBEdit;
    DBEdit108: TDBEdit;
    BitBtn3: TBitBtn;
    DBEdit80: TDBEdit;
    DBComboBox10: TDBComboBox;
    DBEdit81: TDBEdit;
    DBEdit105: TDBEdit;
    DBComboBox3: TDBComboBox;
    Panel108: TPanel;
    Panel110: TPanel;
    Wallpaper35: TWallpaper;
    BFattClienteNew: TToolbarButton97;
    BModFattCli: TToolbarButton97;
    ToolbarButton9720: TToolbarButton97;
    ToolbarButton9735: TToolbarButton97;
    Panel125: TPanel;
    RGFiltroFatture: TRadioGroup;
    dxDBGrid12: TdxDBGrid;
    dxDBGrid12Progressivo: TdxDBGridMaskColumn;
    dxDBGrid12Tipo: TdxDBGridMaskColumn;
    dxDBGrid12Data: TdxDBGridDateColumn;
    dxDBGrid12ScadenzaPagamento: TdxDBGridDateColumn;
    dxDBGrid12PagataInData: TdxDBGridDateColumn;
    dxDBGrid12Totale: TdxDBGridMaskColumn;
    TSContattiOfferte: TTabSheet;
    Splitter5: TSplitter;
    Panel39: TPanel;
    Wallpaper16: TWallpaper;
    ToolbarButton9722: TToolbarButton97;
    ToolbarButton9723: TToolbarButton97;
    ToolbarButton9724: TToolbarButton97;
    ToolbarButton9725: TToolbarButton97;
    Panel35: TPanel;
    Panel93: TPanel;
    Wallpaper5: TWallpaper;
    BGruppiOffNew: TToolbarButton97;
    BGruppiOffMod: TToolbarButton97;
    BGruppiOffDel: TToolbarButton97;
    RxDBGrid3: TRxDBGrid;
    dxDBGrid11: TdxDBGrid;
    dxDBGrid11ID: TdxDBGridMaskColumn;
    dxDBGrid11IDCliente: TdxDBGridMaskColumn;
    dxDBGrid11Rif: TdxDBGridMaskColumn;
    dxDBGrid11Data: TdxDBGridDateColumn;
    dxDBGrid11AMezzo: TdxDBGridMaskColumn;
    dxDBGrid11AttenzioneDi: TdxDBGridMaskColumn;
    dxDBGrid11Stato: TdxDBGridMaskColumn;
    TSClientiContratto: TTabSheet;
    Splitter13: TSplitter;
    Panel111: TPanel;
    FrameDBRich2: TFrameDBRich;
    PCFileClienti: TPageControl;
    TSFileClienti1: TTabSheet;
    Panel90: TPanel;
    BCliAssociaFile: TToolbarButton97;
    BModAssociaFile: TToolbarButton97;
    BDelAssociaFile: TToolbarButton97;
    BCliApriFile: TToolbarButton97;
    dxDBGrid13: TdxDBGrid;
    dxDBGrid7Descrizione: TdxDBGridMaskColumn;
    dxDBGrid7NomeFile: TdxDBGridMaskColumn;
    dxDBGrid7DataCreazione: TdxDBGridDateColumn;
    TSFileClienti2: TTabSheet;
    Panel120: TPanel;
    Label91: TLabel;
    Label92: TLabel;
    DirCartellaCli: TDirectoryListBox;
    DriveComboBox3: TDriveComboBox;
    FileCartellaCli: TFileListBox;
    Panel119: TPanel;
    BFileClienteOpen: TToolbarButton97;
    TSCliContatti: TTabSheet;
    PanContattiCli: TPanel;
    Panel36: TPanel;
    Wallpaper12: TWallpaper;
    ToolbarButton9713: TToolbarButton97;
    ToolbarButton9717: TToolbarButton97;
    ToolbarButton9721: TToolbarButton97;
    DBGrid8: TDBGrid;
    PanCliAltriDati: TPanel;
    Label30: TLabel;
    Label83: TLabel;
    Label84: TLabel;
    Panel172: TPanel;
    DBEdit3: TDBEdit;
    dxDBHyperLinkEdit1: TdxDBHyperLinkEdit;
    dxDBCurrencyEdit1: TdxDBCurrencyEdit;
    TSCliForn: TTabSheet;
    Panel154: TPanel;
    Panel122: TPanel;
    Panel157: TPanel;
    BFornCCNew: TToolbarButton97;
    BFornCCMod1: TToolbarButton97;
    BFornCCDel: TToolbarButton97;
    DBGrid4: TDBGrid;
    Panel158: TPanel;
    Label87: TLabel;
    Panel159: TPanel;
    DBEdit84: TDBEdit;
    Label93: TLabel;
    DBEdit6: TDBEdit;
    Splitter20: TSplitter;
    ToolbarButton9728: TToolbarButton97;
    frReport1: TfrReport;
    procedure TbBAnagNewClick(Sender: TObject);
    procedure TbBAnagDelClick(Sender: TObject);
    procedure TbBAnagModClick(Sender: TObject);
    procedure TbBAnagCanClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TbBFlussiNewClick(Sender: TObject);
    procedure TbBFlussiDelClick(Sender: TObject);
    procedure TbBFlussiOKClick(Sender: TObject);
    procedure TbBFlussiCanClick(Sender: TObject);
    procedure TabelleClick(Sender: TObject);
    procedure TbBCompDipNewClick(Sender: TObject);
    procedure TbBCompDipDelClick(Sender: TObject);
    procedure ToolbarButton979Click(Sender: TObject);
    procedure TbBCarattDipNewClick(Sender: TObject);
    procedure TbBCarattDipDelClick(Sender: TObject);
    procedure TbBCompNewClick(Sender: TObject);
    procedure ToolbarButton975Click(Sender: TObject);
    procedure TbBCompDelClick(Sender: TObject);
    procedure TbBCompOKClick(Sender: TObject);
    procedure TbBCarattNewClick(Sender: TObject);
    procedure TbBCarattDelClick(Sender: TObject);
    procedure TbBCarattOKClick(Sender: TObject);
    procedure TbBCarattCanClick(Sender: TObject);
    procedure TbBMansNewClick(Sender: TObject);
    procedure TbBMansDelClick(Sender: TObject);
    procedure TbBMansModClick(Sender: TObject);
    procedure TbBCompMansNewClick(Sender: TObject);
    procedure TbBCompMansDelClick(Sender: TObject);
    procedure TbBCompMansOKClick(Sender: TObject);
    procedure TbBCompMansCanClick(Sender: TObject);
    procedure Esci1Click(Sender: TObject);
    procedure accessoconaltroutente1Click(Sender: TObject);
    procedure TbBAreeNewClick(Sender: TObject);
    procedure TbBAreeModClick(Sender: TObject);
    procedure TbBAreeDelClick(Sender: TObject);
    procedure TbBUtentiDelClick(Sender: TObject);
    procedure PageControl5Change(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure TbBStatiNewClick(Sender: TObject);
    procedure TbBStatiDelClick(Sender: TObject);
    procedure TbBStatiOKClick(Sender: TObject);
    procedure TbBStatiCanClick(Sender: TObject);
    procedure TbBDiplomiNewClick(Sender: TObject);
    procedure TbBDiplomiDelClick(Sender: TObject);
    procedure TbBDiplomiOKClick(Sender: TObject);
    procedure TbBDiplomiCanClick(Sender: TObject);
    procedure TbBAreeDiplNewClick(Sender: TObject);
    procedure TbBAreeDiplDelClick(Sender: TObject);
    procedure TbBAreeDiplOKClick(Sender: TObject);
    procedure TbBAreeDiplCanClick(Sender: TObject);
    procedure TbBEventiNewClick(Sender: TObject);
    procedure TbBEventiDelClick(Sender: TObject);
    procedure TbBEventiOKClick(Sender: TObject);
    procedure TbBEventiCanClick(Sender: TObject);
    procedure PageControl2Change(Sender: TObject);
    procedure TbBAnagMansNewClick(Sender: TObject);
    procedure TbBAnagMansDelClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure TbBCompBackClick(Sender: TObject);
    procedure TbBCarattBackClick(Sender: TObject);
    procedure ToolbarButton9710Click(Sender: TObject);
    procedure CopiadaAppunti1Click(Sender: TObject);
    procedure EliminaFoto1Click(Sender: TObject);
    procedure adattastretch1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TbBAnagOKClick(Sender: TObject);
    procedure TbBEventiModClick(Sender: TObject);
    procedure LOBPromemoriaClick(Sender: TObject);
    procedure LOBArchMessClick(Sender: TObject);
    procedure LOBRicercheClick(Sender: TObject);
    procedure LOBSchedeClick(Sender: TObject);
    procedure LOBTab1Click(Sender: TObject);
    procedure LOBTab2Click(Sender: TObject);
    procedure LOBTab3Click(Sender: TObject);
    procedure LOBTab4Click(Sender: TObject);
    procedure LOBTab5Click(Sender: TObject);
    procedure LOBTab11Click(Sender: TObject);
    procedure LOBTab7Click(Sender: TObject);
    procedure LOPTabelleClick(Sender: TObject);
    procedure LOPDipendentiClick(Sender: TObject);
    procedure BbValutazClick(Sender: TObject);
    procedure BAnagEventoClick(Sender: TObject);
    procedure BAnagCurriculumClick(Sender: TObject);
    procedure BEsciClick(Sender: TObject);
    procedure BVediCVClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure TbBGestStoricoClick(Sender: TObject);
    procedure ToolbarButton978Click(Sender: TObject);
    procedure ToolbarButton9716Click(Sender: TObject);
    procedure TbBAreeCompNewClick(Sender: TObject);
    procedure TbBAreeCompModClick(Sender: TObject);
    procedure TbBAreeCompDelClick(Sender: TObject);
    procedure DriveComboBox1Change(Sender: TObject);
    procedure TbBClientiNewClick(Sender: TObject);
    procedure TbBClientiDelClick(Sender: TObject);
    procedure TbBClientiModClick(Sender: TObject);
    procedure TbBClientiOKClick(Sender: TObject);
    procedure TbBClientiCanClick(Sender: TObject);
    procedure TbBContattiCliNewClick(Sender: TObject);
    procedure TbBContattiCliDelClick(Sender: TObject);
    procedure PCClientiChange(Sender: TObject);
    procedure RGFiltroRicCliClick(Sender: TObject);
    procedure LOBClientiEBCClick(Sender: TObject);
    procedure BInsRicercaClick(Sender: TObject);
    procedure BNuovaRicercaClick(Sender: TObject);
    procedure BRiprendiRicClick(Sender: TObject);
    procedure BSpecRicClick(Sender: TObject);
    procedure ToolbarButton9719Click(Sender: TObject);
    procedure ToolbarButton977Click(Sender: TObject);
    procedure BRicercheNewClick(Sender: TObject);
    procedure LOBAnnunciClick(Sender: TObject);
    procedure TbBAnnNewClick(Sender: TObject);
    procedure TbBAnnDelClick(Sender: TObject);
    procedure TbBAnnOKClick(Sender: TObject);
    procedure TbBAnnCanClick(Sender: TObject);
    procedure LOBTabAnnClick(Sender: TObject);
    procedure TbBTestNewClick(Sender: TObject);
    procedure TbBTestDelClick(Sender: TObject);
    procedure TbBTestOKClick(Sender: TObject);
    procedure TbBEdizTestNewClick(Sender: TObject);
    procedure TbBEdizTestDelClick(Sender: TObject);
    procedure TbBScontiNewClick(Sender: TObject);
    procedure TbBPenetrazNewClick(Sender: TObject);
    procedure TbBScontiDelClick(Sender: TObject);
    procedure TbBScontiOKClick(Sender: TObject);
    procedure TbBScontiCanClick(Sender: TObject);
    procedure TbBGiorniNewClick(Sender: TObject);
    procedure TbBGiorniDelClick(Sender: TObject);
    procedure TbBGiorniOKClick(Sender: TObject);
    procedure TbBGiorniCanClick(Sender: TObject);
    procedure TbBPenetrazDelClick(Sender: TObject);
    procedure TbBPenetrazOKClick(Sender: TObject);
    procedure TbBPenetrazCanClick(Sender: TObject);
    procedure TbBPubbNewClick(Sender: TObject);
    procedure TbBPubbDelClick(Sender: TObject);
    procedure LOBTabAttivClick(Sender: TObject);
    procedure TbBAttivitaNewClick(Sender: TObject);
    procedure TbBAttivitaDelClick(Sender: TObject);
    procedure PageControl4Change(Sender: TObject);
    procedure LOBSelPersClick(Sender: TObject);
    procedure BAnagFileWordClick(Sender: TObject);
    procedure LOBTabAziendeClick(Sender: TObject);
    procedure ToolbarButton9713Click(Sender: TObject);
    procedure ToolbarButton9717Click(Sender: TObject);
    procedure ToolbarButton9721Click(Sender: TObject);
    procedure ToolbarButton9722Click(Sender: TObject);
    procedure ToolbarButton9723Click(Sender: TObject);
    procedure ToolbarButton9724Click(Sender: TObject);
    procedure ToolbarButton9725Click(Sender: TObject);
    procedure LOBTab8Click(Sender: TObject);
    procedure TbBLingueNewClick(Sender: TObject);
    procedure TbBLingueDelClick(Sender: TObject);
    procedure TbBLingueOKClick(Sender: TObject);
    procedure TbBLingueCanClick(Sender: TObject);
    procedure ToolbarButton9712Click(Sender: TObject);
    procedure ToolbarButton9726Click(Sender: TObject);
    procedure RGAnnFiltroClick(Sender: TObject);
    procedure PCAnnunciChange(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure DBGrid24KeyPress(Sender: TObject; var Key: Char);
    procedure ToolbarButton9729Click(Sender: TObject);
    procedure CBFiltroAnagRicClick(Sender: TObject);
    procedure ToolbarButton9730Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure BCopiadaResClick(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure ECercaComChange(Sender: TObject);
    procedure LOBTabComuniClick(Sender: TObject);
    procedure ToolbarButton9711Click(Sender: TObject);
    procedure ToolbarButton9731Click(Sender: TObject);
    procedure ToolbarButton9714Click(Sender: TObject);
    procedure ToolbarButton971Click(Sender: TObject);
    procedure ToolbarButton9732Click(Sender: TObject);
    procedure ToolbarButton9733Click(Sender: TObject);
    procedure ToolbarButton9734Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BFattClienteNewClick(Sender: TObject);
    procedure BModFattCliClick(Sender: TObject);
    procedure ToolbarButton9735Click(Sender: TObject);
    procedure RGFiltroFattureClick(Sender: TObject);
    procedure ToolbarButton9720Click(Sender: TObject);
    procedure ToolbarButton9737Click(Sender: TObject);
    procedure BCambiaUtenteClick(Sender: TObject);
    procedure RiepTimingClick(Sender: TObject);
    procedure ToolbarButton9736Click(Sender: TObject);
    procedure ToolbarButton9739Click(Sender: TObject);
    procedure ToolbarButton9740Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure ToolbarButton9743Click(Sender: TObject);
    procedure PageControl2Changing(Sender: TObject;
      var AllowChange: Boolean);
    procedure LOBAziendeClick(Sender: TObject);
    procedure TbBOrganigModClick(Sender: TObject);
    procedure TbBOrganigOKClick(Sender: TObject);
    procedure TbBOrganigCanClick(Sender: TObject);
    procedure ToolbarButton9744Click(Sender: TObject);
    procedure SpeedButton9Click(Sender: TObject);
    procedure SpeedButton10Click(Sender: TObject);
    procedure Infocandidato1Click(Sender: TObject);
    procedure curriculum1Click(Sender: TObject);
    procedure documenti1Click(Sender: TObject);
    procedure fileWord1Click(Sender: TObject);
    procedure modificaattuale1Click(Sender: TObject);
    procedure BAzSearchClick(Sender: TObject);
    procedure eliminasoggetto2Click(Sender: TObject);
    procedure BNotaSpeseNewClick(Sender: TObject);
    procedure BNotaSpeseModClick(Sender: TObject);
    procedure BNotaSpeseDelClick(Sender: TObject);
    procedure PCClientiChanging(Sender: TObject; var AllowChange: Boolean);
    procedure PCAnnunciChanging(Sender: TObject; var AllowChange: Boolean);
    procedure SpeedButton8Click(Sender: TObject);
    procedure bDubbiInsClick(Sender: TObject);
    procedure BCompensoNewClick(Sender: TObject);
    procedure BCompensoModClick(Sender: TObject);
    procedure BCompensoDelClick(Sender: TObject);
    procedure BAssFattDelClick(Sender: TObject);
    procedure SpeedButton11Click(Sender: TObject);
    procedure BRisNewClick(Sender: TObject);
    procedure BRisModClick(Sender: TObject);
    procedure BRisDelClick(Sender: TObject);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
    procedure BAnagSearchClick(Sender: TObject);
    procedure TbBContattiCliModClick(Sender: TObject);
    procedure BOrgNewNodoClick(Sender: TObject);
    procedure BOrgNewNewChildNodoClick(Sender: TObject);
    procedure BOrgNewDelNodoClick(Sender: TObject);
    procedure PColorClick(Sender: TObject);
    procedure DBTreeCreateNode(Sender: TObject; Node: TdxOcNode);
    procedure DBGrid20TitleClick(Column: TColumn);
    procedure BPropCVClick(Sender: TObject);
    procedure associaanodo1Click(Sender: TObject);
    procedure BOrgCandDelClick(Sender: TObject);
    procedure DBEdit15Exit(Sender: TObject);
    procedure DBGrid13DblClick(Sender: TObject);
    procedure ToolbarButton973Click(Sender: TObject);
    procedure BFileClienteOpenClick(Sender: TObject);
    procedure DirCartellaCliChange(Sender: TObject);
    procedure BAnagFileNewClick(Sender: TObject);
    procedure BAnagFileModClick(Sender: TObject);
    procedure BAnagFileDelClick(Sender: TObject);
    procedure BFileCandOpenClick(Sender: TObject);
    procedure RxDBGrid1DblClick(Sender: TObject);
    procedure ToolbarButton9745Click(Sender: TObject);
    procedure TbBContattiCliAltriDatiClick(Sender: TObject);
    procedure BRicClientiClick(Sender: TObject);
    procedure CBRicUtentiTuttiClick(Sender: TObject);
    procedure BLegendaClick(Sender: TObject);
    procedure DBGrid7DblClick(Sender: TObject);
    procedure BGlobalOKClick(Sender: TObject);
    procedure BGlobalCanClick(Sender: TObject);
    procedure EClientiKeyPress(Sender: TObject; var Key: Char);
    procedure BCliSearchClick(Sender: TObject);
    procedure TbBMansModAreaClick(Sender: TObject);
    procedure BFornCCModClick(Sender: TObject);
    procedure BOfferteClick(Sender: TObject);
    procedure BTipoCommNewClick(Sender: TObject);
    procedure RxDBGrid2GetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure BTipoCommModClick(Sender: TObject);
    procedure BTipoCommDelClick(Sender: TObject);
    procedure RxDbTipiCommGetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure BTrovaClienteClick(Sender: TObject);
    procedure BSedeNewClick(Sender: TObject);
    procedure ToolbarButton9748Click(Sender: TObject);
    procedure ToolbarButton9749Click(Sender: TObject);
    procedure BProvNewClick(Sender: TObject);
    procedure BProvModClick(Sender: TObject);
    procedure BProvDelClick(Sender: TObject);
    procedure BCandUtenteClick(Sender: TObject);
    procedure ToolbarButton9738Click(Sender: TObject);
    procedure TSAnagNoteIntExit(Sender: TObject);
    procedure BOrgStandardClick(Sender: TObject);
    procedure BAssociaNewClick(Sender: TObject);
    procedure LOBTabNazioniClick(Sender: TObject);
    procedure ECercaNazioneChange(Sender: TObject);
    procedure BNazNewClick(Sender: TObject);
    procedure BNazModClick(Sender: TObject);
    procedure BNazDelClick(Sender: TObject);
    procedure BAnagSelNazione1Click(Sender: TObject);
    procedure BAnagSelNazione2Click(Sender: TObject);
    procedure BCliSelNazClick(Sender: TObject);
    procedure TbBAnagMansModClick(Sender: TObject);
    procedure DBGrid41DblClick(Sender: TObject);
    procedure TbBCarattDipModClick(Sender: TObject);
    procedure BOrgNewStampaClick(Sender: TObject);
    procedure BStoricoAnagPosClick(Sender: TObject);
    procedure StoricoPosizioni91Click(Sender: TObject);
    procedure Storicoposizione1Click(Sender: TObject);
    procedure Eliminasoggettoassociato1Click(Sender: TObject);
    procedure PopupOrgPopup(Sender: TObject);
    procedure PageControl8Change(Sender: TObject);
    procedure TbBEdizTestOKClick(Sender: TObject);
    procedure SpeedButton12Click(Sender: TObject);
    procedure SpeedButton13Click(Sender: TObject);
    procedure BAnnSelSedeClick(Sender: TObject);
    procedure BAnnTipoClick(Sender: TObject);
    procedure ListinoAnnunciFrame1ToolbarButton972Click(Sender: TObject);
    procedure ClienteAnnunciFrameToolbarButton971Click(Sender: TObject);
    procedure BTipiStradaNewClick(Sender: TObject);
    procedure BTipiStradaModClick(Sender: TObject);
    procedure BTipiStradaDelClick(Sender: TObject);
    procedure TbBPubbModDataClick(Sender: TObject);
    procedure DBCBTipoStradaCliDropDown(Sender: TObject);
    procedure DBCBTipoStradaResDropDown(Sender: TObject);
    procedure DBCBTipoStradaDomDropDown(Sender: TObject);
    procedure BFileDaModelloClick(Sender: TObject);
    procedure LOBTabModelliWordClick(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure ToolbarButton9742Click(Sender: TObject);
    procedure BGruppiOffNewClick(Sender: TObject);
    procedure BGruppiOffModClick(Sender: TObject);
    procedure BGruppiOffDelClick(Sender: TObject);
    procedure SpeedButton14Click(Sender: TObject);
    procedure BRifAnnuncioClick(Sender: TObject);
    procedure ERifAnnuncioKeyPress(Sender: TObject; var Key: Char);
    procedure BStatoCivileClick(Sender: TObject);
    procedure BJobDescOKClick(Sender: TObject);
    procedure BJobDescCanClick(Sender: TObject);
    procedure BNoteStampaClick(Sender: TObject);
    procedure BAnagIncompNewClick(Sender: TObject);
    procedure BAnagIncompDelClick(Sender: TObject);
    procedure BAnagIncompModDescClick(Sender: TObject);
    procedure BCliRefCliNewClick(Sender: TObject);
    procedure BCliRefCliDElClick(Sender: TObject);
    procedure BCliRefCliModDescClick(Sender: TObject);
    procedure DBGrid34DblClick(Sender: TObject);
    procedure BSaveToExcelClick(Sender: TObject);
    procedure BMotNonAccClick(Sender: TObject);
    procedure BCopiaDaDomClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure BTabLivLingueClick(Sender: TObject);
    procedure DBCheckBox3Exit(Sender: TObject);
    procedure dxDBGrid1DblClick(Sender: TObject);
    procedure dxDBGrid1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure dxDBGrid1CustomDrawCell(Sender: TObject; ACanvas: TCanvas;
      ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
      ASelected, AFocused, ANewItemRow: Boolean; var AText: string;
      var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
      var ADone: Boolean);
    procedure EsportainExcel1Click(Sender: TObject);
    procedure EsportainHTML1Click(Sender: TObject);
    procedure Stampagriglia1Click(Sender: TObject);
    procedure BAnnSedeDelClick(Sender: TObject);
    procedure BAnnCommModCodiceClick(Sender: TObject);
    procedure BAnnRuoloAddClick(Sender: TObject);
    procedure PCAnnunciDettChange(Sender: TObject);
    procedure BAnnRuoloDelClick(Sender: TObject);
    procedure BAnnRuoloCodClick(Sender: TObject);
    procedure BannDataModCodClick(Sender: TObject);
    procedure BAnnEdizSoggAddClick(Sender: TObject);
    procedure BAnnEdizSoggDelClick(Sender: TObject);
    procedure ToolbarButton9715Click(Sender: TObject);
    procedure ToolbarButton9727Click(Sender: TObject);
    procedure BFornCCNewClick(Sender: TObject);
    procedure BFornCCDelClick(Sender: TObject);
    procedure BFornCCMod1Click(Sender: TObject);
    procedure PCAziendaOrgChange(Sender: TObject);
    procedure infocandidato2Click(Sender: TObject);
    procedure curriculum2Click(Sender: TObject);
    procedure documenti2Click(Sender: TObject);
    procedure fileWord2Click(Sender: TObject);
    procedure BTargetListClick(Sender: TObject);
    procedure Aggiungiacommessa1Click(Sender: TObject);
    procedure PopupMenu4Popup(Sender: TObject);
    procedure PopupMenu3Popup(Sender: TObject);
    procedure Aggiungiacommessa2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure ToolbarButton972Click(Sender: TObject);
    procedure ToolbarButton976Click(Sender: TObject);
    procedure ToolbarButton974Click(Sender: TObject);
    procedure TbBAttivitaOKClick(Sender: TObject);
    procedure TbBAttivitaCanClick(Sender: TObject);
    procedure ToolbarButton9750Click(Sender: TObject);
    procedure ToolbarButton9753Click(Sender: TObject);
    procedure ToolbarButton9754Click(Sender: TObject);
    procedure BTimeSheetNewClick(Sender: TObject);
    procedure BTimeSheetDelClick(Sender: TObject);
    procedure BTimeSheetOKClick(Sender: TObject);
    procedure BTimeSheetCanClick(Sender: TObject);
    procedure dxDBGrid11DblClick(Sender: TObject);
    procedure ENomeKeyPress(Sender: TObject; var Key: Char);
    procedure BCliAltriSettoriClick(Sender: TObject);
    procedure BAnagTipologiaClick(Sender: TObject);
    procedure BAnagModificheClick(Sender: TObject);
    procedure BCliAssociaFileClick(Sender: TObject);
    procedure BModAssociaFileClick(Sender: TObject);
    procedure BDelAssociaFileClick(Sender: TObject);
    procedure BCliApriFileClick(Sender: TObject);
    procedure CBOrgZoomClick(Sender: TObject);
    procedure CBOrgRotateClick(Sender: TObject);
    procedure BTabDiplomiITAClick(Sender: TObject);
    procedure BTabDiplomiENGClick(Sender: TObject);
    procedure BAreeRuoliITAClick(Sender: TObject);
    procedure BAreeRuoliENGClick(Sender: TObject);
    procedure BAnagRuoliITAClick(Sender: TObject);
    procedure BAnagRuoliENGClick(Sender: TObject);
    procedure DBGrid6DblClick(Sender: TObject);
    procedure DBGClientiDblClick(Sender: TObject);
    procedure ToolbarButton9728Click(Sender: TObject);
  private
    xCont, xggDiffApRic, xggDiffUcRic, xggDiffColRic: integer;
    xStringaArea, xcaption: string;
    xPaginaPrec, xPaginaCliPrec, xPaginaAnnunciPrec: TTabsheet;
    procedure DisabilitaTabAnag;
    procedure DisabilitaTabGen;
    procedure DisabilitaTabAziende;
    procedure AggTotAnnuncio(xIDAnnuncio: integer);
    procedure RicalcoloCostoPubb(xIDAnnEdizData: integer);
  public
    xColumnRic: TColumn;
    cCVNumAutomatico, xIsFulltextInstalled: boolean;
    xNuoviCVFormCreated, xEsci: boolean;
    xTop, xIDUtenteAttuale: integer;
    xUserObj: array[1..1000] of string;
    xArrayAnagID: array[1..20] of integer;
    xASG1RowSel, xUtenteAttualeTipo: integer;
    xUtenteAttuale, xUltimaProv: string;
    xgenericString: string;
    xIDOrg, xIDRicerca, xIDTargetList, xIDRicercaPres: integer;
    function CheckProfile(CodObj: string; mess: boolean = True): boolean;
    function GetShape(ShapeName: string): TdxOcShape;
    function GetNodeAlign(AlignName: string): TdxOcNodeAlign;
    function GetImageAlign(AlignName: string): TdxOcImageAlign;
    procedure CompilaFilePresPos(xCliente, xPosizione: string; xIDCandRic: integer);
    procedure CompilaFilePresPosLWG(xPosizione: string; xIDCandRic, xIDCliente, xIDContatto: integer);
    procedure CompilaFilePresPosProposte(xPosizione: string; xIDCandRic, xIDCliente, xIDMansione: integer);
    procedure CompilaFilePresPosMCS(xPosizione: string; xIDCandRic, xIDCliente, xIDMansione: integer);
    procedure CompilaFilePresPosBoyden(xPosizione: string; xIDCandRic, xIDCliente, xIDMansione: integer);
    procedure Save(ADefaultExt, AFilter, AFileName: string; AMethod: TSaveMethod);
    procedure EseguiQRicAttive;
    procedure DisegnaTVAziendaCand;
    function SpedisciMail(xAddress: string): string;
  end;

var
  MainForm: TMainForm;

implementation

uses ModuloDati, ModuloDati2, MansComp, Curriculum,
  Login, SceltaStato, MessLetti, ValutazDip, StoricoComp,
  SelPers, ElencoDip, AnalisiComp, Messaggio, InsRuolo,
  AgendaSett, View, GestStorico, Competenza, InsCompetenza, MDRicerche,
  Specifiche, ElencoRicPend, SelData, ModuloDati4, SelPersNew, Azienda,
  ContattoCliFatto, OffertaCliente, CambiaStato2, uGestEventi, uUtilsVarie,
  CercaAnnuncio, SelEdiz, InsRicerca, Comuni, ZoneComuni, Fattura,
  RiepTiming, SchedaCand, RepElencoAnnCand, SelAttivita, OpzioniEliminaz,
  ModStatoRic, NuoviCV, InsNodoOrg, ElencoCandAzienda, ElencoAziende,
  NotaSpese, DubbiIns, Compenso, Risorsa, ContattoCli, InsCliente,
  SelCliente, ValutazColloquio, AnagFile, SchedaSintetica, SelRicCand,
  RifClienteDati, RicClienti, LegendaElRic, InsInserimento, SelArea,
  SceltaModelloMailCli, EmailMessage, Offerte, TipoCommessa,
  CreaCompensi, InsAnnuncio, ElencoUtenti, DettSede, CheckPass, Comune,
  OrgStandard, NuovoSoggetto, Nazione, SelNazione, CompInserite, ModRuolo,
  InsCaratt, NuovaComp, Uscita, EliminaDipOrg, StoricoAnagPos, Testata,
  Edizione, ElencoSedi, ElencoTipiAnnunci, ContrattoAnnunci, TipoStrada,
  SceltaModelloAnag, GestModelliWord, SelContatti, Contratto, UnitPrintMemo,
  SelFromQuery, Costo, CliAltriSettori, LogOperazioniSogg, RicFile, uASACand,
  uASAAzienda, uASAcommessa;

{$R *.DFM}

const xMaxCons = 99999;

var
  OldWindowProc: Pointer; {Variable for the old windows proc}
  MyMsg: DWord; {custom systemwide message}

function NewWindowProc(WindowHandle: hWnd;
  TheMessage: LongInt;
  ParamW: LongInt;
  ParamL: LongInt): LongInt stdcall;
begin
  if TheMessage = MyMsg then begin
    {Tell the application to restore, let it restore the form}
    SendMessage(Application.handle, WM_SYSCOMMAND, SC_RESTORE, 0);
    SetForegroundWindow(Application.Handle);
    {We handled the message - we are done}
    Result := 0;
    exit;
  end;
  {Call the original winproc}
  Result := CallWindowProc(OldWindowProc,
    WindowHandle,
    TheMessage,
    ParamW,
    ParamL);
end;

procedure TMainForm.TbBAnagNewClick(Sender: TObject);
var xStato, xTipoStato, xIDAnnData, i, xIDAnag: integer;
  xVai, xNumeroValido: boolean;
  xCVNumero: string;
begin
  if not CheckProfile('000') then Exit;
  // controllo numero massimo consentiti
  Data.QTemp.Close;
  Data.QTemp.SQL.Text := 'select count(*) TotAnag from Anagrafica';
  Data.QTemp.Open;
  if Data.QTemp.FieldbyName('TotAnag').asInteger >= xMaxCons then begin
    MessageDlg('Gi� raggiunto il limite massimo di soggetti consentito', mtError, [mbOK], 0);
    Data.QTemp.Close;
  end else begin
    Data.QTemp.Close;

    xStato := 1;
    xTipoStato := 2;

    // scelta STATO (prende da solo MACROSTATO)
    SceltaStatoForm := TSceltaStatoForm.create(self);
    SceltaStatoForm.DataPervenuto.Date := Date;
    SceltaStatoForm.ShowModal;
    if SceltaStatoForm.ModalResult = mrOK then begin

      // - controllo nome e cognomi nulli
      if (SceltaStatoForm.ECogn.Text = '') or (SceltaStatoForm.ENome.Text = '') then begin
        MessageDlg('Il soggetto non pu� avere cognome o nome vuoti', mtError, [mbOK], 0);
        exit;
      end;
      // - controllo omonimia
      xVai := True;
      with Data do begin
        Q1.Close;
        Q1.SQL.Text := 'select ID,CVNumero from Anagrafica where Cognome=:x and Nome=:y';
        Q1.Params[0].asString := trim(SceltaStatoForm.ECogn.Text);
        Q1.Params[1].asString := trim(SceltaStatoForm.ENOme.Text);
        Q1.Open;
        if Q1.RecordCount > 0 then begin
          if MessageDlg('CONTROLLARE POSSIBILE OMONIMIA con CV n� ' + Q1.FieldByName('CVNumero').asString + chr(13) +
            'Registrare comunque ?', mtError, [mbYes, mbNo], 0) = mrYes then
            xVai := True
          else xVai := False;
        end;
      end;
      if not xVai then exit;

      // NUMERO CV
      if not cCVNumAutomatico then begin
        // manuale
        xCVNumero := '0';
        xNumeroValido := False;
        while not xNumeroValido do begin
          if not InputQuery('Numero CV manuale', 'Numero CV:', xCVNumero) then begin
            xVai := False;
            Break;
          end else begin
            if xCVNumero = '' then xCVNumero := '0';
            if xCVNumero = '0' then begin
              if MessageDlg('ATTENZIONE: Non � possibile inserire un n� CV uguale a ZERO' + chr(13) +
                'Verr� associato il progressivo autoincrementante interno.' + chr(13) +
                'Vuoi procedere (Yes) o Annullare l''inserimento (no) ?', mtWarning, [mbYes, mbNo], 0) = mrNo then begin
                xVai := False;
                SceltaStatoForm.Free;
                Exit;
              end;
            end;
            // controllo esistenza n� CV
            Data.QTemp.SQL.text := 'select Cognome,Nome from Anagrafica where CVNumero=' + xCVNumero;
            Data.QTemp.Open;
            if not Data.QTemp.IsEmpty then begin
              if MessageDlg('ATTENZIONE: esiste gi� il numero CV ' + xCVNumero + ' abbinato al soggetto:' + chr(13) +
                Data.QTemp.FieldByName('Cognome').asString + ' ' + Data.QTemp.FieldByName('Nome').asString + chr(13) + chr(13) +
                'Vuoi procedere (Yes) o Annullare l''inserimento (no) ?', mtWarning, [mbYes, mbNo], 0) = mrNo then begin
                xVai := False;
                SceltaStatoForm.Free;
                Exit;
              end else xNumeroValido := True;
            end else xNumeroValido := True;
            Data.QTemp.Close;
          end;
        end;
      end else xCVNumero := '0'; // automatico

      Data.DB.BeginTrans;
      try
        Data.QTemp.Close;
        Data.QTemp.SQL.Text := 'insert into Anagrafica (CVNumero,Cognome,Nome,IDStato,IDTipoStato,' +
          'CVInseritoInData,IDProprietaCV) ' +
          '  values (:xCVNumero,:xCognome,:xNome,:xIDStato,:xIDTipoStato,' +
          ':xCVInseritoInData,:xIDProprietaCV)';
        Data.QTemp.ParamByName('xCVNumero').asInteger := StrtoInt(xCVNumero);
        Data.QTemp.ParamByName('xCognome').asString := SceltaStatoForm.ECogn.Text;
        Data.QTemp.ParamByName('xNome').asString := SceltaStatoForm.ENome.Text;
        Data.QTemp.ParamByName('xIDStato').asInteger := SceltaStatoForm.QStatiID.Value;
        Data.QTemp.ParamByName('xIDTipoStato').asInteger := SceltaStatoForm.QStatiIDTipoStato.Value;
        Data.QTemp.ParamByName('xCVInseritoInData').asDateTime := SceltaStatoForm.DataPervenuto.Date;
        Data.QTemp.ParamByName('xIDProprietaCV').asInteger := 0;
        Data.QTemp.ExecSQL;

        Data.QTemp.SQL.text := 'select @@IDENTITY as LastID';
        Data.QTemp.Open;
        xIDAnag := Data.QTemp.FieldByName('LastID').asInteger;
        Data.QTemp.Close;

        // log inserimento
        Log_Operation(MainForm.xIDUtenteAttuale, 'Anagrafica', xIDAnag, 'I');

        // CV Numero automatico (o nullo)
        if xCVNumero = '0' then begin
          Data.QTemp.Close;
          Data.QTemp.SQL.text := 'update Anagrafica set CVNumero=' + IntToStr(xIDAnag) + ' where ID=' + IntToStr(xIDAnag);
          xCVNumero := IntToStr(xIDAnag);
          Data.QTemp.ExecSQL;
        end;

        // ALTRE INFO
        Data.QTemp.SQL.text := 'insert into AnagAltreInfo (IDAnagrafica,CVProvenienza) ' +
          ' values (:xIDAnagrafica,:xCVProvenienza)';
        Data.QTemp.ParamByName('xIDAnagrafica').asInteger := xIDAnag;
        Data.QTemp.ParamByName('xCVProvenienza').asString := SceltaStatoForm.EProvCV.Text;
        Data.QTemp.ExecSQL;

        // storico
        Data.QTemp.SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDUtente) ' +
          ' values (:xIDAnagrafica,:xIDEvento,:xDataEvento,:xAnnotazioni,:xIDUtente)';
        Data.QTemp.ParamByName('xIDAnagrafica').asInteger := xIDAnag;
        Data.QTemp.ParamByName('xIDEvento').asInteger := 82;
        Data.QTemp.ParamByName('xDataEvento').asDateTime := SceltaStatoForm.DataPervenuto.Date;
        Data.QTemp.ParamByName('xAnnotazioni').asString := 'dal modulo H1Sel (nuovo soggetto)';
        Data.QTemp.ParamByName('xIDUtente').asInteger := xIDUtenteAttuale;
        Data.QTemp.ExecSQL;

        Data.DB.CommitTrans;
        // posizionati sul soggetto inserito
        Edit1.text := SceltaStatoForm.ECogn.Text;
        BAnagSearchClick(self);
        Data.TAnagrafica.Locate('ID', xIDAnag, []);
      except
        Data.DB.RollbackTrans;
        MessageDlg('ATTENZIONE: errore sul database - Inserimento non avvenuto', mtError, [mbOK], 0);
      end;
    end;
    SceltaStatoForm.Free;
    // inserisci nell'array
    CaricaPrimi;
  end;
end;

procedure TMainForm.TbBAnagDelClick(Sender: TObject);
var xTabFiglie: string;
  xCognome, xNome, xCVNumero, xNatoA, xNatoIl, xCVInseritoIndata, xCVDataAgg: string;
begin
  if not CheckProfile('001') then Exit;
  if Data.TAnagrafica.IsEmpty then
    ShowMessage('questo archivio � archivio vuoto: nessuna scheda da eliminare')
  else begin
    if MessageDlg('ATTENZIONE: la procedura � IRREVERSIBILE e DEFINITIVA !!' + chr(13) +
      'E'' necessario verificare il rispetto della normativa sulla Privacy e sulla' + chr(13) +
      'conservazione dei relativi dati.' + chr(13) + chr(13) +
      'SEI DAVVERO SICURO DI VOLER PROCEDERE ?', mtWarning,
      [mbNo, mbYes], 0) = mrNo then exit;

    if MessageDlg('Sei proprio sicuro di voler eliminare il soggetto selezionato ?', mtWarning,
      [mbNo, mbYes], 100103) = mrYes then begin

      xTabFiglie := '';
      Data.Q1.Close;
      Data.Q1.SQL.text := 'select count(*) TOT from AnagCaratteristiche where IDAnagrafica=' + Data.TAnagraficaID.asString;
      Data.Q1.Open;
      if Data.Q1.fieldByName('TOT').asInteger > 0 then xTabFiglie := xTabFiglie + '- Caratteristiche' + chr(13);
      Data.Q1.Close;
      Data.Q1.SQL.text := 'select count(*) TOT from AnagFile where IDAnagrafica=' + Data.TAnagraficaID.asString;
      Data.Q1.Open;
      if Data.Q1.fieldByName('TOT').asInteger > 0 then xTabFiglie := xTabFiglie + '- File collegati al soggetto' + chr(13);
      Data.Q1.Close;
      Data.Q1.SQL.text := 'select count(*) TOT from AnagIncompClienti where IDAnagrafica=' + Data.TAnagraficaID.asString;
      Data.Q1.Open;
      if Data.Q1.fieldByName('TOT').asInteger > 0 then xTabFiglie := xTabFiglie + '- Incompatibilit� con clienti' + chr(13);
      Data.Q1.Close;
      Data.Q1.SQL.text := 'select count(*) TOT from AnagMansioni where IDAnagrafica=' + Data.TAnagraficaID.asString;
      Data.Q1.Open;
      if Data.Q1.fieldByName('TOT').asInteger > 0 then xTabFiglie := xTabFiglie + '- Ruoli/mansioni' + chr(13);
      Data.Q1.Close;
      Data.Q1.SQL.text := 'select count(*) TOT from AnagValutaz where IDAnagrafica=' + Data.TAnagraficaID.asString;
      Data.Q1.Open;
      if Data.Q1.fieldByName('TOT').asInteger > 0 then xTabFiglie := xTabFiglie + '- Valutazione' + chr(13);
      Data.Q1.Close;
      Data.Q1.SQL.text := 'select count(*) TOT from CompetenzeAnagrafica where IDAnagrafica=' + Data.TAnagraficaID.asString;
      Data.Q1.Open;
      if Data.Q1.fieldByName('TOT').asInteger > 0 then xTabFiglie := xTabFiglie + '- Competenze' + chr(13);
      Data.Q1.Close;
      Data.Q1.SQL.text := 'select count(*) TOT from EBC_CandidatiRicerche where IDAnagrafica=' + Data.TAnagraficaID.asString;
      Data.Q1.Open;
      if Data.Q1.fieldByName('TOT').asInteger > 0 then xTabFiglie := xTabFiglie + '- Commesse/ricerche' + chr(13);
      Data.Q1.Close;
      Data.Q1.SQL.text := 'select count(*) TOT from EBC_ContattiCandidati where IDAnagrafica=' + Data.TAnagraficaID.asString;
      Data.Q1.Open;
      if Data.Q1.fieldByName('TOT').asInteger > 0 then xTabFiglie := xTabFiglie + '- Contatti relativi a commesse' + chr(13);
      Data.Q1.Close;
      Data.Q1.SQL.text := 'select count(*) TOT from EsperienzeLavorative where IDAnagrafica=' + Data.TAnagraficaID.asString;
      Data.Q1.Open;
      if Data.Q1.fieldByName('TOT').asInteger > 0 then xTabFiglie := xTabFiglie + '- Esperienze lavorative' + chr(13);
      Data.Q1.Close;
      Data.Q1.SQL.text := 'select count(*) TOT from LingueConosciute where IDAnagrafica=' + Data.TAnagraficaID.asString;
      Data.Q1.Open;
      if Data.Q1.fieldByName('TOT').asInteger > 0 then xTabFiglie := xTabFiglie + '- Lingue conosciute' + chr(13);
      Data.Q1.Close;
      Data.Q1.SQL.text := 'select count(*) TOT from Organigramma where IDDipendente=' + Data.TAnagraficaID.asString;
      Data.Q1.Open;
      if Data.Q1.fieldByName('TOT').asInteger > 0 then xTabFiglie := xTabFiglie + '- Organigramma' + chr(13);
      Data.Q1.Close;
      Data.Q1.SQL.text := 'select count(*) TOT from Storico where IDAnagrafica=' + Data.TAnagraficaID.asString;
      Data.Q1.Open;
      if Data.Q1.fieldByName('TOT').asInteger > 0 then xTabFiglie := xTabFiglie + '- Storico' + chr(13);
      Data.Q1.Close;
      Data.Q1.SQL.text := 'select count(*) TOT from TitoliStudio where IDAnagrafica=' + Data.TAnagraficaID.asString;
      Data.Q1.Open;
      if Data.Q1.fieldByName('TOT').asInteger > 0 then xTabFiglie := xTabFiglie + '- Titoli di studio' + chr(13);

      if xTabFiglie <> '' then begin
        if MessageDlg('ATTENZIONE:  risultano collegati i seguenti dati:' + chr(13) +
          xTabFiglie + 'SEI SICURO DI VOLER PROSEGUIRE ?', mtWarning, [mbYes, mbNo], 0) = mrNo then
          exit;
      end;

      if MessageDlg('Verranno eliminati TUTTI i dati di TUTTE le tabelle collegate' + chr(13) +
        'IMPORTANTE:  non sar� pi� possibile recuperarli - CONTINUARE ?', mtWarning,
        [mbNo, mbYes], 0) = mrYes then begin
        Data.DB.BeginTrans;
        try
          // eliminare tabelle collegate da CONSTRAINT di integrit� referenziale                         Data.Q1.Close;
          Data.Q1.Close;
          Data.Q1.SQL.Clear;
          Data.Q1.SQL.Add('delete from AnagAltreInfo where IDAnagrafica=' + Data.TAnagraficaID.asString);
          Data.Q1.ExecSQL;
          Data.Q1.SQL.Clear;
          Data.Q1.SQL.Add('delete from AnagCaratteristiche where IDAnagrafica=' + Data.TAnagraficaID.asString);
          Data.Q1.ExecSQL;
          Data.Q1.SQL.Clear;
          Data.Q1.SQL.Add('delete from AnagFile where IDAnagrafica=' + Data.TAnagraficaID.asString);
          Data.Q1.ExecSQL;
          Data.Q1.SQL.Clear;
          Data.Q1.SQL.Add('delete from AnagIncompClienti where IDAnagrafica=' + Data.TAnagraficaID.asString);
          Data.Q1.ExecSQL;
          Data.Q1.SQL.Clear;
          Data.Q1.SQL.Add('delete from AnagMansioni where IDAnagrafica=' + Data.TAnagraficaID.asString);
          Data.Q1.ExecSQL;
          Data.Q1.SQL.Clear;
          Data.Q1.SQL.Add('delete from AnagValutaz where IDAnagrafica=' + Data.TAnagraficaID.asString);
          Data.Q1.ExecSQL;
          Data.Q1.SQL.Clear;
          Data.Q1.SQL.Add('delete from CompetenzeAnagrafica where IDAnagrafica=' + Data.TAnagraficaID.asString);
          Data.Q1.ExecSQL;
          Data.Q1.SQL.Clear;
          Data.Q1.SQL.Add('delete from EBC_CandidatiRicerche where IDAnagrafica=' + Data.TAnagraficaID.asString);
          Data.Q1.ExecSQL;
          Data.Q1.SQL.Clear;
          Data.Q1.SQL.Add('delete from EBC_ContattiCandidati where IDAnagrafica=' + Data.TAnagraficaID.asString);
          Data.Q1.ExecSQL;
          Data.Q1.SQL.Clear;
          Data.Q1.SQL.Add('delete from EsperienzeLavorative where IDAnagrafica=' + Data.TAnagraficaID.asString);
          Data.Q1.ExecSQL;
          Data.Q1.SQL.Clear;
          Data.Q1.SQL.Add('delete from LingueConosciute where IDAnagrafica=' + Data.TAnagraficaID.asString);
          Data.Q1.ExecSQL;
          Data.Q1.SQL.Clear;
          Data.Q1.SQL.Add('delete from Organigramma where IDDipendente=' + Data.TAnagraficaID.asString);
          Data.Q1.ExecSQL;
          Data.Q1.SQL.Clear;
          Data.Q1.SQL.Add('delete from Storico where IDAnagrafica=' + Data.TAnagraficaID.asString);
          Data.Q1.ExecSQL;
          Data.Q1.SQL.Clear;
          Data.Q1.SQL.Add('delete from TitoliStudio where IDAnagrafica=' + Data.TAnagraficaID.asString);
          Data.Q1.ExecSQL;
          Data.Q1.SQL.Clear;
          Data.Q1.SQL.Add('delete from Anagrafica where ID=' + Data.TAnagraficaID.asString);
          Data.Q1.ExecSQL;

          // log cancellazione
          xCognome := Data.TAnagraficaCognome.Value;
          xNome := Data.TAnagraficaNome.Value;
          xCVNumero := Data.TAnagraficaCVNumero.AsString;
          xNatoA := Data.TAnagraficaLuogoNascita.Value;
          xNatoIl := Data.TAnagraficaDataNascita.asString;
          xCVInseritoIndata := Data.TAnagraficaCVinseritoInData.asString;
          xCVDataAgg := Data.TAnagraficaCVDataAgg.asString;
          Log_Operation(MainForm.xIDUtenteAttuale, 'Anagrafica', Data.TAnagraficaID.Value, 'D', xCVNumero + ' ' + xCognome + ' ' + xNome + ', nato a ' + xNatoA + ' il ' + xNatoIl + ' CV ins.il ' + xCVInseritoIndata + ' ult.agg.il ' + xCVDataAgg);

          Data.DB.CommitTrans;
          Data.TAnagrafica.Close;
        except
          Data.DB.RollbackTrans;
          MessageDlg('Errore nel database: cancellazione non eseguita', mtError, [mbOK], 0);
          raise;
        end;
      end;
    end;
  end;
end;

procedure TMainForm.TbBAnagModClick(Sender: TObject);
begin
  if not CheckProfile('002') then Exit;
  if Data.TAnagrafica.RecordCount = 0 then
    ShowMessage('questo archivio � archivio vuoto: nessuna scheda da modificare')
  else Data.TAnagrafica.Edit;
end;

procedure TMainForm.TbBAnagCanClick(Sender: TObject);
begin
  Data.TAnagrafica.Cancel;
end;

procedure TMainForm.FormCreate(Sender: TObject);
var i: integer;
  xAnno, xMese, xGiorno: Word;
begin
  xEsci := False;
  {Register a custom windows message}
  MyMsg := RegisterWindowMessage('H1Sel');
  {Set form1's windows proc to ours and remember the old window proc}
  OldWindowProc := Pointer(SetWindowLong(MainForm.Handle,
    GWL_WNDPROC,
    LongInt(@NewWindowProc)));

  DateSeparator := '/';
  ShortDateFormat := 'dd/mm/yyyy';

  Lookout1.ActivePage := LOPDipendenti;

  for i := 0 to PageControl1.PageCount - 1 do
    PageControl1.Pages[i].TabVisible := False;
  for i := 0 to PageControl3.PageCount - 1 do
    PageControl3.Pages[i].TabVisible := False;

  PageControl1.ActivePage := TSInizio;
  PageControl2.ActivePage := TSAnagDatiAnag;
  PageControl3.ActivePage := TsTabMansioni;
  PageControl8.ActivePage := TSTabTestate;

  TSAnagStorico.TabVisible := True;
  BbValutaz.Visible := True;
  xNuoviCVFormCreated := False;
end;

procedure TMainForm.TbBFlussiNewClick(Sender: TObject);
begin
  Data.TEventi.Insert;
end;

procedure TMainForm.TbBFlussiDelClick(Sender: TObject);
begin
  if MessageDlg('Sei sicuro di voler eliminarlo ?', mtWarning,
    [mbNo, mbYes], 0) = mrYes then
    if Data.TEventiIDProcedura.asString <> '' then
      messagedlg('Evento predefinito non modificabile', mtError, [mbOK], 0)
    else Data.TEventi.Delete;
end;

procedure TMainForm.TbBFlussiOKClick(Sender: TObject);
begin
  Data.TEventi.Post;
end;

procedure TMainForm.TbBFlussiCanClick(Sender: TObject);
begin
  Data.TEventi.Cancel;
end;

procedure TMainForm.TabelleClick(Sender: TObject);
begin
  PageControl1.ActivePage := TSTabelle;
end;

procedure TMainForm.TbBCompDipNewClick(Sender: TObject);
var i, xVal: integer;
  Vero: boolean;
begin
  if not CheckProfile('020') then Exit;
  InsCompetenzaForm := TInsCompetenzaForm.create(self);
  InsCompetenzaForm.RiempiGriglia;
  InsCompetenzaForm.ShowModal;
  if InsCompetenzaForm.ModalResult = mrOK then begin
    for i := 1 to InsCompetenzaForm.ASG1.RowCount - 1 do begin
      InsCompetenzaForm.ASG1.GetCheckBoxState(0, i, Vero);
      if Vero then begin
        with Data do begin
          DB.BeginTrans;
          try
            Q1.Close;
            Q1.SQL.text := 'insert into CompetenzeAnagrafica (IDAnagrafica,IDCompetenza,Valore) ' +
              'values (:xIDAnagrafica,:xIDCompetenza,:xValore)';
            Q1.ParamByName('xIDAnagrafica').asInteger := Data.TAnagraficaID.value;
            Q1.ParamByName('xIDCompetenza').asInteger := InsCompetenzaForm.xArrayIDComp[i];
            Q1.ParamByName('xValore').asInteger := InsCompetenzaForm.xArrayCompVal[i];
            Q1.ExecSQL;

            Q1.SQL.text := 'insert into StoricoCompAnag (IDCompetenza,IDAnagrafica,DallaData,MotivoAumento,valore) ' +
              'values (:xIDCompetenza,:xIDAnagrafica,:xDallaData,:xMotivoAumento,:xvalore)';
            Q1.ParamByName('xIDCompetenza').asInteger := InsCompetenzaForm.xArrayIDComp[i];
            Q1.ParamByName('xIDAnagrafica').asInteger := Data.TAnagraficaID.value;
            Q1.ParamByName('xDallaData').asDateTime := Date;
            Q1.ParamByName('xMotivoAumento').asString := 'valore iniziale';
            Q1.ParamByName('xValore').asInteger := InsCompetenzaForm.xArrayCompVal[i];
            Q1.ExecSQL;

            DB.CommitTrans;
            Data2.TCompDipendente.Close;
            Data2.TCompDipendente.Open;
          except
            DB.RollbackTrans;
            MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
          end;
        end;
      end;
    end;

  end;
  InsCompetenzaForm.Free;
end;

procedure TMainForm.TbBCompDipDelClick(Sender: TObject);
begin
  if not CheckProfile('021') then Exit;
  if Data2.TCompDipendente.RecordCount > 0 then
    if MessageDlg('Sei sicuro di voler cancellare questa competenza ?', mtWarning,
      [mbNo, mbYes], 0) = mrYes then begin
      with Data do begin
        DB.BeginTrans;
        try
          Q1.Close;
          Q1.SQL.text := 'delete from CompetenzeAnagrafica ' +
            'where ID=' + Data2.TCompDipendenteID.asString;
          Q1.ExecSQL;
          DB.CommitTrans;
        except
          DB.RollbackTrans;
          MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
        end;
      end;
      Data2.TCompDipendente.Close;
      Data2.TCompDipendente.Open;
    end;
end;

procedure TMainForm.ToolbarButton979Click(Sender: TObject);
begin
  if not CheckProfile('61') then Exit;
  TbBCompBack.Visible := True;
  PageControl1.ActivePage := TSTabelle;
  LOBTab2Click(self);
end;

procedure TMainForm.TbBCarattDipNewClick(Sender: TObject);
begin
  if not CheckProfile('033') then Exit;
  InsCarattForm := TInsCarattForm.create(self);
  InsCarattForm.ShowModal;
  if InsCarattForm.Modalresult = mrOK then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'insert into AnagCaratteristiche (IDAnagrafica,IDCaratt) ' +
          'values (:xIDAnagrafica,:xIDCaratt)';
        Q1.ParamByName('xIDAnagrafica').asInteger := Data.TAnagraficaID.Value;
        Q1.ParamByName('xIDCaratt').asInteger := InsCarattForm.TCarattID.Value;
        Q1.ExecSQL;
        DB.CommitTrans;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
    Data2.TCarattDip.Close;
    Data2.TCarattDip.Open;
  end;
  InsCarattForm.Free;
end;

procedure TMainForm.TbBCarattDipDelClick(Sender: TObject);
begin
  if not CheckProfile('034') then Exit;
  if Data2.TCarattDip.RecordCount = 0 then exit;
  if MessageDlg('Sei sicuro di voler cancellare ?', mtWarning,
    [mbNo, mbYes], 0) = mrYes then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'delete from AnagCaratteristiche ' +
          'where ID=' + Data2.TCarattDipID.asString;
        Q1.ExecSQL;
        DB.CommitTrans;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
    Data2.TCarattDip.Close;
    Data2.TCarattDip.Open;
  end;
end;

procedure TMainForm.TbBCompNewClick(Sender: TObject);
begin
  CompetenzaForm := TCompetenzaForm.create(self);
  CompetenzaForm.ShowModal;
  if CompetenzaForm.ModalResult = mrOK then begin
    with Data do begin
      Q1.Close;
      Q1.SQL.text := 'select ID from Competenze where Descrizione=:xDesc';
      Q1.ParamByName('xDesc').asString := CompetenzaForm.EComp.Text;
      Q1.open;
      if Q1.RecordCount > 0 then begin
        MessageDlg('Competenza gi� esistente', mtError, [mbOK], 0);
        exit;
      end;
    end;
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'insert into Competenze (Descrizione,IDArea,Tipologia) ' +
          'values (:xDescrizione,:xIDArea,:xTipologia)';
        Q1.ParamByName('xDescrizione').asString := CompetenzaForm.EComp.Text;
        Q1.ParamByName('xIDArea').asInteger := CompetenzaForm.TAreeCompID.Value;
        Q1.ParamByName('xTipologia').asString := CompetenzaForm.ETipo.text;
        Q1.ExecSQL;
        DB.CommitTrans;
        Data2.TCompetenze.Close;
        Data2.TCompetenze.Open;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
  end;
  CompetenzaForm.Free;
end;

procedure TMainForm.ToolbarButton975Click(Sender: TObject);
begin
  TbBCarattBack.Visible := True;
  PageControl1.ActivePage := TSTabelle;
  LOBTab3Click(self);
end;

procedure TMainForm.TbBCompDelClick(Sender: TObject);
begin
  if not Data2.TCompetenze.EOF then
    if MessageDlg('Sei sicuro di voler cancellare ?', mtWarning,
      [mbNo, mbYes], 0) = mrYes then begin
      with Data do begin
        DB.BeginTrans;
        try
          Q1.Close;
          Q1.SQL.text := 'delete from Competenze ' +
            'where ID=' + Data2.TCompetenzeID.asString;
          Q1.ExecSQL;
          DB.CommitTrans;
          Data2.TCompetenze.Close;
          Data2.TCompetenze.Open;
        except
          DB.RollbackTrans;
          MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
        end;
      end;
    end;
end;

procedure TMainForm.TbBCompOKClick(Sender: TObject);
begin
  if not Data2.TCompetenze.EOF then begin
    CompetenzaForm := TCompetenzaForm.create(self);
    CompetenzaForm.EComp.Text := Data2.TCompetenzeDescrizione.Value;
    CompetenzaForm.ETipo.text := Data2.TCompetenzeTipologia.Value;
    while (not CompetenzaForm.TAreeComp.EOF) and (CompetenzaForm.TAreeCompID.Value <> Data2.TCompetenzeIDArea.Value) do
      CompetenzaForm.TAreeComp.Next;
    CompetenzaForm.ShowModal;
    if CompetenzaForm.ModalResult = mrOK then begin
      with Data do begin
        DB.BeginTrans;
        try
          Q1.Close;
          Q1.SQL.text := 'update Competenze set Descrizione=:xDescrizione,IDArea=:xIDArea,Tipologia=:xTipologia ' +
            'where ID=' + Data2.TCompetenzeID.asString;
          Q1.ParamByName('xDescrizione').asString := CompetenzaForm.EComp.Text;
          Q1.ParamByName('xIDArea').asInteger := CompetenzaForm.TAreeCompID.Value;
          Q1.ParamByName('xTipologia').asString := CompetenzaForm.ETipo.text;
          Q1.ExecSQL;
          DB.CommitTrans;
          Data2.TCompetenze.Close;
          Data2.TCompetenze.Open;
        except
          DB.RollbackTrans;
          MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
        end;
      end;
    end;
    CompetenzaForm.Free;
  end;
end;

procedure TMainForm.TbBCarattNewClick(Sender: TObject);
begin
  Data2.TCaratteristiche.Insert;
end;

procedure TMainForm.TbBCarattDelClick(Sender: TObject);
begin
  if MessageDlg('Sei sicuro di voler cancellare ?', mtWarning,
    [mbNo, mbYes], 0) = mrYes then Data2.TCaratteristiche.Delete;
end;

procedure TMainForm.TbBCarattOKClick(Sender: TObject);
begin
  Data2.TCaratteristiche.Post;
end;

procedure TMainForm.TbBCarattCanClick(Sender: TObject);
begin
  Data2.TCaratteristiche.Cancel;
end;

procedure TMainForm.TbBMansNewClick(Sender: TObject);
var xDic: string;
begin
  if InputQuery('Nuovo ruolo', 'Denominazione:', xDic) then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'insert into Mansioni (Descrizione,IDArea) ' +
          'values (:xDescrizione,:xIDArea)';
        Q1.ParamByName('xDescrizione').asString := xDic;
        Q1.ParamByName('xIDArea').asInteger := Data2.TAreeID.Value;
        Q1.ExecSQL;
        DB.CommitTrans;
        Data2.TMansioni.Close;
        Data2.TMansioni.Open;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
    Data2.TMansioni.Refresh;
  end;
end;

procedure TMainForm.TbBMansDelClick(Sender: TObject);
begin
  // controllo esistenza competenze legate al ruolo
  if not data2.TCompMansioni.IsEmpty then
    if MessageDlg('Ci sono competenze legate a questo ruolo:' + chr(13) + 'VUOI PROSEGUIRE ?', mtWarning, [mbYes, mbNo], 0) = mrNo then Exit;

  // ALTRI CONTROLLI DI INTEGRITA' REFERENZIALE
  // - ruoli associati ai candidati
  Data.Q1.Close;
  Data.Q1.SQL.text := 'select count(*) Tot from AnagMansioni where IDMansione=' + Data2.TMansioniID.asString;
  Data.Q1.Open;
  if Data.Q1.FieldByname('Tot').asInteger > 0 then begin
    MessageDlg('Ci sono SOGGETTI associati a questo ruolo:' + chr(13) + 'IMPOSSIBILE CANCELLARE', mtError, [mbOK], 0);
    Data.Q1.Close;
    Exit;
  end;
  Data.Q1.Close;
  // - ricerche con associato il ruolo
  Data.Q1.Close;
  Data.Q1.SQL.text := 'select count(*) Tot from EBC_Ricerche where IDMansione=' + Data2.TMansioniID.asString;
  Data.Q1.Open;
  if Data.Q1.FieldByname('Tot').asInteger > 0 then begin
    MessageDlg('Ci sono RICERCHE associati a questo ruolo:' + chr(13) + 'IMPOSSIBILE CANCELLARE', mtError, [mbOK], 0);
    Data.Q1.Close;
    Exit;
  end;
  Data.Q1.Close;
  // - Annunci con associato il ruolo
  Data.Q1.Close;
  Data.Q1.SQL.text := 'select count(*) Tot from Ann_Annunci where IDMansione=' + Data2.TMansioniID.asString;
  Data.Q1.Open;
  if Data.Q1.FieldByname('Tot').asInteger > 0 then begin
    MessageDlg('Ci sono ANNUNCI associati a questo ruolo:' + chr(13) + 'IMPOSSIBILE CANCELLARE', mtError, [mbOK], 0);
    Data.Q1.Close;
    Exit;
  end;
  Data.Q1.Close;

  if MessageDlg('Sei sicuro di voler cancellare il ruolo selezionato ?', mtWarning,
    [mbNo, mbYes], 0) = mrYes then begin
    Data2.TMansioniABS.Open;
    Data2.TMansioniABS.Locate('ID', Data2.TMansioniID.Value, []);
    Data2.TMansioniABS.Delete;
    Data2.TMansioniABS.Close;
    Data2.TMansioni.Close;
    Data2.TMansioni.Open;
  end;
end;

procedure TMainForm.TbBMansModClick(Sender: TObject);
var xDic: string;
  xID: integer;
begin
  if not Data2.TMansioni.EOF then begin
    if BAreeRuoliITA.Down then begin
      xDic := Data2.TMansioniDescrizione.Value;
      if InputQuery('Modifica ruolo', 'Nuova denominazione (ITA):', xDic) then begin
        Data2.TMansioniABS.Open;
        Data2.TMansioniABS.Locate('ID', Data2.TMansioniID.Value, []);
        Data2.TMansioniABS.Edit;
        Data2.TMansioniABSDescrizione.Value := xDic;
        Data2.TMansioniABS.Post;
        Data2.TMansioniABS.Close;
      end;
    end else begin
      xDic := Data2.TMansioniDescrizione_ENG.Value;
      if InputQuery('Modifica ruolo', 'Nuova denominazione (ENG):', xDic) then begin
        Data2.TMansioniABS.Open;
        Data2.TMansioniABS.Locate('ID', Data2.TMansioniID.Value, []);
        Data2.TMansioniABS.Edit;
        Data2.TMansioniABSDescrizione_ENG.Value := xDic;
        Data2.TMansioniABS.Post;
        Data2.TMansioniABS.Close;
      end;
    end;
    xID := Data2.TMansioniID.Value;
    Data2.TMansioni.Close;
    Data2.TMansioni.Open;
    Data2.TMansioni.Locate('ID', xID, []);
  end;
end;

procedure TMainForm.TbBCompMansNewClick(Sender: TObject);
var i, xVal: integer;
  Vero: boolean;
begin
  vero := True;
  InsCompetenzaForm := TInsCompetenzaForm.create(self);
  InsCompetenzaForm.RiempiGriglia;
  InsCompetenzaForm.ShowModal;
  if InsCompetenzaForm.ModalResult = mrOK then begin
    // inserimento di tutte le competenze
    Data2.TCompMansIDX.Open;
    for i := 1 to InsCompetenzaForm.ASG1.RowCount - 1 do begin
      InsCompetenzaForm.ASG1.GetCheckBoxState(0, i, Vero);
      if Vero then begin
        if not Data2.TCompMansIDX.FindKey([InsCompetenzaForm.xArrayIDComp[i], Data2.TMansioniID.Value]) then begin
          with Data do begin
            DB.BeginTrans;
            try
              Q1.Close;
              Q1.SQL.text := 'insert into CompetenzeMansioni (IDCompetenza,IDMansione,valore) ' +
                'values (:xIDCompetenza,:xIDMansione,:xValore)';
              Q1.ParamByName('xIDCompetenza').asInteger := InsCompetenzaForm.xArrayIDComp[i];
              Q1.ParamByName('xIDMansione').asInteger := Data2.TMansioniID.Value;
              Q1.ParamByName('xValore').asInteger := InsCompetenzaForm.xArrayCompVal[i];
              Q1.ExecSQL;
              DB.CommitTrans;
            except
              DB.RollbackTrans;
              MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
            end;
          end;
        end;
      end;
    end;
    Data2.TCompMansIDX.close;
  end;
  InsCompetenzaForm.Free;
  if Data2.TCompMansioni.Active then Data2.TCompMansioni.refresh;
end;

procedure TMainForm.TbBCompMansDelClick(Sender: TObject);
begin
  if not Data2.TCompMansioni.EOF then
    if MessageDlg('Sei sicuro di voler cancellare ?', mtWarning,
      [mbNo, mbYes], 0) = mrYes then Data2.TCompMansioni.delete;
end;

procedure TMainForm.TbBCompMansOKClick(Sender: TObject);
begin
  Data2.TCompMansioni.Post;
end;

procedure TMainForm.TbBCompMansCanClick(Sender: TObject);
begin
  Data2.TCompMansioni.cancel;
end;

procedure TMainForm.Esci1Click(Sender: TObject);
begin
  close;
end;

procedure TMainForm.accessoconaltroutente1Click(Sender: TObject);
var xUtente: string;
begin
  xUtente := data.GlobalLastLogin.Value;
  if MessageDlg('terminare la sessione di ' + xUtente + ' ?',
    mtWarning, [mbYes, mbNo], 0) = mrYes then
  begin
    LoginForm.Accedi := 'N';
    LoginForm.Username.Text := Data.GlobalLastLogin.Value;
    LoginForm.ShowModal;
    showMessage('Utente attuale: ' + Data.GlobalLastLogin.Value);
  end;
end;

procedure TMainForm.TbBAreeNewClick(Sender: TObject);
var xDic: string;
begin
  xDic := '';
  if InputQuery('Nuova area', 'Denominazione (ITA):', xDic) then begin
    Data2.TAreeABS.Open;
    Data2.TAreeABS.InsertRecord([xDic]);
    Data2.TAree.Close;
    Data2.TAree.Open;
    xStringaArea := xDic;
    Data2.TAree.Locate('Descrizione', xStringaArea, []);
    xStringaArea := '';
    Data2.TAreeABS.Close;
  end;
end;

procedure TMainForm.TbBAreeModClick(Sender: TObject);
var xDic: string;
begin
  if not Data2.TAree.EOF then begin
    if BAreeRuoliITA.Down then begin
      xDic := Data2.TAreeDescrizione.Value;
      if InputQuery('Modifica area', 'Nuova denominazione (ITA):', xDic) then begin
        Data2.TAreeABS.Open;
        Data2.TAreeABS.Locate('ID', Data2.TAreeID.Value, []);
        Data2.TAreeABS.Edit;
        Data2.TAreeABSDescrizione.Value := xDic;
        Data2.TAreeABS.Post;
        Data2.TAreeABS.Close;
        Data2.TAree.Close;
        Data2.TAree.Open;
      end;
    end else begin
      xDic := Data2.TAreeDescrizione_ENG.Value;
      if InputQuery('Modifica area', 'Nuova denominazione (ENG):', xDic) then begin
        Data2.TAreeABS.Open;
        Data2.TAreeABS.Locate('ID', Data2.TAreeID.Value, []);
        Data2.TAreeABS.Edit;
        Data2.TAreeABSDescrizione_ENG.Value := xDic;
        Data2.TAreeABS.Post;
        Data2.TAreeABS.Close;
        Data2.TAree.Close;
        Data2.TAree.Open;
      end;
    end;
  end;
end;

procedure TMainForm.TbBAreeDelClick(Sender: TObject);
begin
  // controllo ruoli legati all'area
  if not Data2.TMansioni.IsEmpty then begin
    MessageDlg('Ci sono ruoli dipendenti: impossibile cancellare l''area', mtError, [mbOK], 0);
    Exit;
  end;
  if MessageDlg('Sei sicuro di voler cancellare l''area selezionata ?', mtWarning,
    [mbNo, mbYes], 0) = mrYes then begin
    Data2.TAreeABS.Open;
    Data2.TAreeABS.Locate('ID', Data2.TAreeID.Value, []);
    Data2.TAreeABS.Delete;
    Data2.TAreeABS.Close;
    Data2.TAree.Close;
    Data2.TAree.Open;
  end;
end;

procedure TMainForm.TbBUtentiDelClick(Sender: TObject);
begin
  if MessageDlg('Sei sicuro di voler cancellare ?', mtWarning,
    [mbNo, mbYes], 0) = mrYes then Data.Users.Delete;
end;

procedure TMainForm.PageControl5Change(Sender: TObject);
var i: integer;
  xLineeSQL: TStrings;
begin
  for i := 1 to PageControl2.PageCount - 1 do
    PageControl2.Pages[i].TabVisible := True;
  if Pagecontrol5.ActivePage = TSStatoTutti then begin
    Data.TAnagrafica.Filtered := False;
    if Data.TAnagrafica.EOF then
      for i := 1 to PageControl2.PageCount - 1 do
        PageControl2.Pages[i].TabVisible := False;
    BbValutaz.Visible := True;
    PageControl2.ActivePage := TSAnagDatiAnag;
    LabPartitaIVA.Visible := False;
    DBEPartitaIVA.Visible := False;
  end;
  if Pagecontrol5.ActivePage = TsStatoDip then begin
    Data.TAnagrafica.Filter := 'IDTipoStato=1';
    Data.TAnagrafica.Filtered := True;
    if Data.TAnagrafica.EOF then
      for i := 1 to PageControl2.PageCount - 1 do
        PageControl2.Pages[i].TabVisible := False;
    BbValutaz.Visible := True;
    PageControl2.ActivePage := TSAnagDatiAnag;
    LabPartitaIVA.Visible := False;
    DBEPartitaIVA.Visible := False;
  end;
  if Pagecontrol5.ActivePage = TsStatoEsterno then begin
    Data.TAnagrafica.Filter := 'IDTipoStato=2';
    Data.TAnagrafica.Filtered := True;
    if Data.TAnagrafica.EOF then
      for i := 1 to PageControl2.PageCount - 1 do
        PageControl2.Pages[i].TabVisible := False;
    BbValutaz.Visible := True;
    PageControl2.ActivePage := TSAnagDatiAnag;
    LabPartitaIVA.Visible := False;
    DBEPartitaIVA.Visible := False;
  end;
  if Pagecontrol5.ActivePage = TsStatoUscito then begin
    Data.TAnagrafica.Filter := 'IDTipoStato=3';
    Data.TAnagrafica.Filtered := True;
    if Data.TAnagrafica.EOF then
      for i := 1 to PageControl2.PageCount - 1 do
        PageControl2.Pages[i].TabVisible := False;
    BbValutaz.Visible := True;
    PageControl2.ActivePage := TSAnagDatiAnag;
    LabPartitaIVA.Visible := False;
    DBEPartitaIVA.Visible := False;
  end;
  if Pagecontrol5.ActivePage = TsStatoEl then begin
    Data.TAnagrafica.Filter := 'IDTipoStato=5';
    Data.TAnagrafica.Filtered := True;
    if Data.TAnagrafica.EOF then
      for i := 1 to PageControl2.PageCount - 1 do
        PageControl2.Pages[i].TabVisible := False;
    BbValutaz.Visible := True;
    PageControl2.ActivePage := TSAnagDatiAnag;
    LabPartitaIVA.Visible := False;
    DBEPartitaIVA.Visible := False;
  end;
  if Pagecontrol5.ActivePage = TSStatoInternet then begin
    Data.TAnagrafica.Filter := 'IDTipoStato=9';
    Data.TAnagrafica.Filtered := True;
    if Data.TAnagrafica.EOF then
      for i := 1 to PageControl2.PageCount - 1 do
        PageControl2.Pages[i].TabVisible := False;
    BbValutaz.Visible := True;
    PageControl2.ActivePage := TSAnagDatiAnag;
    LabPartitaIVA.Visible := False;
    DBEPartitaIVA.Visible := False;
  end;
  Edit1.Text := '';
  SettaDiritti(Data.TAnagraficaID.Value);
end;

procedure TMainForm.FormHide(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TMainForm.TbBStatiNewClick(Sender: TObject);
begin
  Data.TStati.Insert;
end;

procedure TMainForm.TbBStatiDelClick(Sender: TObject);
begin
  // controlli di INTEGRITA' REFERENZIALE
  // - eventi (tabella di base)
  Data.Q1.Close;
  Data.Q1.SQL.text := 'select count(*) Tot from EBC_Eventi where IDdaStato=' + Data.TStatiID.asString +
    ' or IDaStato=' + Data.TStatiID.asString;
  Data.Q1.Open;
  if Data.Q1.FieldByname('Tot').asInteger > 0 then begin
    MessageDlg('Ci sono EVENTI associati a questo stato:' + chr(13) + 'IMPOSSIBILE CANCELLARE', mtError, [mbOK], 0);
    Data.Q1.Close;
    Exit;
  end;
  Data.Q1.Close;
  // - candidati
  Data.Q1.Close;
  Data.Q1.SQL.text := 'select count(*) Tot from Anagrafica where IDStato=' + Data.TStatiID.asString;
  Data.Q1.Open;
  if Data.Q1.FieldByname('Tot').asInteger > 0 then begin
    MessageDlg('Ci sono CANDIDATI associati a questo stato:' + chr(13) + 'IMPOSSIBILE CANCELLARE', mtError, [mbOK], 0);
    Data.Q1.Close;
    Exit;
  end;
  Data.Q1.Close;

  if MessageDlg('Sei sicuro di voler cancellare ?', mtWarning,
    [mbNo, mbYes], 0) = mrYes then Data.TStati.Delete;
end;

procedure TMainForm.TbBStatiOKClick(Sender: TObject);
begin
  Data.TStati.Post;
end;

procedure TMainForm.TbBStatiCanClick(Sender: TObject);
begin
  Data.TStati.Cancel;
end;

procedure TMainForm.TbBDiplomiNewClick(Sender: TObject);
begin
  Data.TDiplomi.Insert;
end;

procedure TMainForm.TbBDiplomiDelClick(Sender: TObject);
begin
  if MessageDlg('Sei sicuro di voler cancellare ?', mtWarning,
    [mbNo, mbYes], 0) = mrYes then Data.TDiplomi.Delete;
end;

procedure TMainForm.TbBDiplomiOKClick(Sender: TObject);
begin
  Data.TDiplomi.Post;
end;

procedure TMainForm.TbBDiplomiCanClick(Sender: TObject);
begin
  Data.TDiplomi.Cancel;
end;

procedure TMainForm.TbBAreeDiplNewClick(Sender: TObject);
begin
  Data.TAreeDiplomi.Insert;
end;

procedure TMainForm.TbBAreeDiplDelClick(Sender: TObject);
begin
  if MessageDlg('Sei sicuro di voler cancellare ?', mtWarning,
    [mbNo, mbYes], 0) = mrYes then Data.TAreeDiplomi.Delete;
end;

procedure TMainForm.TbBAreeDiplOKClick(Sender: TObject);
begin
  Data.TAreeDiplomi.Post;
end;

procedure TMainForm.TbBAreeDiplCanClick(Sender: TObject);
begin
  Data.TAreeDiplomi.Cancel;
end;

procedure TMainForm.TbBEventiNewClick(Sender: TObject);
begin
  Data.TEventi.Insert;
end;

procedure TMainForm.TbBEventiDelClick(Sender: TObject);
begin
  if MessageDlg('Sei sicuro di voler cancellare ?', mtWarning,
    [mbNo, mbYes], 0) = mrYes then Data.TEventi.Delete;
end;

procedure TMainForm.TbBEventiOKClick(Sender: TObject);
begin
  Data.TEventi.post;
end;

procedure TMainForm.TbBEventiCanClick(Sender: TObject);
begin
  Data.TEventi.Cancel;
end;

procedure TMainForm.PageControl2Change(Sender: TObject);
begin
  // aggiornamento note candidato se cambio pagina
  if (Data.QAnagNote.active) and (Data.DsQAnagNote.State = dsEdit) then begin
    with Data.QAnagNote do begin
      Data.DB.BeginTrans;
      try
        ApplyUpdates;
        Data.DB.CommitTrans;
      except
        Data.DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
        raise;
      end;
      CommitUpdates;
    end;
  end;

  if (PageControl2.ActivePage = TSAnagDatiAnag) and
    (not CheckProfile('00', false)) then begin
    TSAnagDatiAnag.Visible := False;
    Exit;
  end;

  if not CheckProfile('00', false) then PanAnag.Visible := False
  else PanAnag.Visible := True;

  // RICERCHE
  if PageControl2.ActivePage = TSAnagRicerche then begin
    if not CheckProfile('01') then begin
      PageControl2.ActivePage := xPaginaPrec;
      Exit;
    end;
    Data2.QAnagRicerche.Open;
  end else begin
    Data2.QAnagRicerche.Close;
  end;

  // COMPETENZE
  if PageControl2.ActivePage = TSAnagComp then begin
    if not CheckProfile('02') then begin
      PageControl2.ActivePage := xPaginaPrec;
      Exit;
    end;
    Data2.TCompDipendente.Open;
  end else begin
    Data2.TCompDipendente.Close;
  end;

  // RUOLI e CARATTERISTICHE
  if PageControl2.ActivePage = TsAnagMans then begin
    if not CheckProfile('03') then begin
      PageControl2.ActivePage := xPaginaPrec;
      Exit;
    end;
    Data.TAnagMansioni.Open;
    Data2.TCarattDip.Open;
  end else begin
    Data.TAnagMansioni.Close;
    Data2.TCarattDip.Close;
  end;

  // STORICO
  if PageControl2.ActivePage = TSAnagStorico then begin
    if not CheckProfile('04') then begin
      PageControl2.ActivePage := xPaginaPrec;
      Exit;
    end;
    //Data.TStorico.Open;
    Data.QStorico.Open;
    DataRicerche.QAnagIncompClienti.Open;
  end else begin
    //Data.TStorico.Close;
    Data.QStorico.Close;
    DataRicerche.QAnagIncompClienti.Close;
  end;

  // NOTE INTERNE
  if PageControl2.ActivePage = TSAnagNoteInt then begin
    if not CheckProfile('06') then begin
      PageControl2.ActivePage := xPaginaPrec;
      Exit;
    end;
    if FrameDBRichAnagNote.Editor.DataSource = nil then
      FrameDBRichAnagNote.Editor.DataSource := Data.DsQAnagNote;
    if not Data.QAnagNote.active then Data.QAnagNote.Open;
    DataRicerche.QAnagTitoloMansAtt.Open;
    DataRicerche.QAnagNoteRic.Open;
  end else begin
    Data.QAnagNote.Close;
    DataRicerche.QAnagNoteRic.Close;
    DataRicerche.QAnagTitoloMansAtt.Close;
  end;

  // FILE ASSOCIATI AL CANDIDATO
  if PageControl2.ActivePage = TSAnagFile then begin
    if not CheckProfile('07') then begin
      PageControl2.ActivePage := xPaginaPrec;
      Exit;
    end;
    Data.QAnagFile.Open;
  end else begin
    Data.QAnagFile.Close;
  end;
end;

procedure TMainForm.TbBAnagMansNewClick(Sender: TObject);
var xIDAnag, xIDMans, xTotComp, i, xID: integer;
  xComp: array[1..50] of integer;
begin
  if not CheckProfile('030') then Exit;
  InsRuoloForm := TInsRuoloForm.create(self);
  InsRuoloForm.ShowModal;
  if InsRuoloForm.ModalResult = mrOK then begin
    // controllo esistenza ruolo
    Data.QTemp.Close;
    Data.QTemp.SQL.text := 'select count(ID) Tot from AnagMansioni where IDAnagrafica=:xIDAnag and IDMansione=:xIDMans';
    Data.QTemp.ParamByName('xIDAnag').asInteger := Data.TAnagraficaID.Value;
    Data.QTemp.ParamByName('xIDMans').asInteger := InsRuoloForm.TRuoliID.Value;
    Data.QTemp.Open;
    if Data.QTemp.FieldByName('Tot').asInteger > 0 then begin
      MessageDlg('Ruolo gi� associato al candidato. ' + chr(13) +
        'IMPOSSIBILE INSERIRE.', mtError, [mbOK], 0);
      exit;
    end;
    Data.QTemp.Close;

    with Data do begin
      DB.BeginTrans;
      try
        // inserimento competenze (se � stato richiesto)
        if InsRuoloForm.CBInsComp.Checked then begin
          CompInseriteForm := TCompInseriteForm.create(self);
          CompInseriteForm.ERuolo.Text := InsRuoloForm.TRuoliDescrizione.Value;
          Data2.QCompRuolo.Close;
          Data2.QCompRuolo.Prepare;
          Data2.QCompRuolo.Params[0].asInteger := InsRuoloForm.TRuoliID.Value;
          Data2.QCompRuolo.Open;
          Data2.QCompRuolo.First;
          xTotComp := 0;
          Data2.TAnagCompIDX.Open;
          CompInseriteForm.SG1.RowCount := 2;
          CompInseriteForm.SG1.RowHeights[0] := 16;
          while not Data2.QCompRuolo.EOF do begin
            if not Data2.TAnagCompIDX.FindKey([xIDAnag, Data2.QCompRuoloIDCompetenza.Value]) then begin
              CompInseriteForm.SG1.RowHeights[xTotComp + 1] := 16;
              CompInseriteForm.SG1.Cells[0, xTotComp + 1] := Data2.QCompRuoloDescCompetenza.Value;
              CompInseriteForm.SG1.Cells[1, xTotComp + 1] := '0';
              if TAnagraficaIDTipoStato.Value <> 2 then begin
                CompInseriteForm.SG1.Cells[2, xTotComp + 1] := '�';
                CompInseriteForm.SG1.ColorRow[xTotComp + 1] := clLime;
              end else begin
                CompInseriteForm.SG1.Cells[2, xTotComp + 1] := 'x';
                CompInseriteForm.SG1.ColorRow[xTotComp + 1] := clRed;
              end;
              xComp[xTotComp + 1] := Data2.QCompRuoloIDCompetenza.Value;
              Inc(xTotComp);
              CompInseriteForm.SG1.RowCount := CompInseriteForm.SG1.RowCount + 1;
            end;
            Data2.QCompRuolo.Next;
          end;
          CompInseriteForm.SG1.RowCount := CompInseriteForm.SG1.RowCount - 1;
          if xTotComp > 0 then CompInseriteForm.ShowModal;
          if CompInseriteForm.ModalResult = mrOK then begin
            for i := 1 to CompInseriteForm.SG1.RowCount - 1 do begin
              if CompInseriteForm.SG1.ColorRow[i] = clLime then begin

                Q1.Close;
                Q1.SQL.text := 'insert into CompetenzeAnagrafica (IDAnagrafica,IDCompetenza,Valore) ' +
                  'values (:xIDAnagrafica,:xIDCompetenza,:xValore)';
                Q1.ParamByName('xIDAnagrafica').asInteger := Data.TAnagraficaID.Value;
                Q1.ParamByName('xIDCompetenza').asInteger := xComp[i];
                Q1.ParamByName('xValore').asInteger := StrToInt(CompInseriteForm.SG1.Cells[1, i]);
                Q1.ExecSQL;

                Q1.SQL.text := 'insert into StoricoCompAnag (IDCompetenza,IDAnagrafica,DallaData,MotivoAumento,valore) ' +
                  'values (:xIDCompetenza,:xIDAnagrafica,:xDallaData,:xMotivoAumento,:xvalore)';
                Q1.ParamByName('xIDCompetenza').asInteger := xComp[i];
                Q1.ParamByName('xIDAnagrafica').asInteger := Data.TAnagraficaID.Value;
                Q1.ParamByName('xDallaData').asDateTime := Date;
                Q1.ParamByName('xMotivoAumento').asString := 'valore iniziale';
                Q1.ParamByName('xValore').asInteger := StrToInt(CompInseriteForm.SG1.Cells[1, i]);
                Q1.ExecSQL;
              end;
            end;
          end;
        end;
        // inserimento ruolo
        Q1.Close;
        Q1.SQL.text := 'insert into AnagMansioni (IDAnagrafica,IDMansione,IDArea,Attuale,AnniEsperienza,Disponibile) ' +
          'values (:xIDAnagrafica,:xIDMansione,:xIDArea,:xAttuale,:xAnniEsperienza,:xDisponibile)';
        Q1.ParamByName('xIDAnagrafica').asInteger := Data.TAnagraficaID.Value;
        Q1.ParamByName('xIDMansione').asInteger := InsRuoloForm.TRuoliID.Value;
        Q1.ParamByName('xIDArea').asInteger := InsRuoloForm.TAreeID.Value;
        Q1.ParamByName('xAttuale').asBoolean := False;
        Q1.ParamByName('xAnniEsperienza').asInteger := 0;
        Q1.ParamByName('xDisponibile').asBoolean := True;
        Q1.ExecSQL;
        Data.QTemp.SQL.text := 'select @@IDENTITY as LastID';
        Data.QTemp.Open;
        xID := Data.QTemp.FieldByName('LastID').asInteger;
        Data.QTemp.Close;
        DB.CommitTrans;
        Data.TAnagMansioni.Close;
        Data.TAnagMansioni.Open;
        Log_Operation(xIDUtenteAttuale, 'AnagMansioni', xID, 'I');
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;

    end;
    InsRuoloForm.Free;
  end;
end;

procedure TMainForm.TbBAnagMansDelClick(Sender: TObject);
begin
  if not CheckProfile('031') then Exit;
  if Data.TAnagMansioni.RecordCount > 0 then
    if MessageDlg('Sei sicuro di voler cancellare ?', mtWarning,
      [mbNo, mbYes], 0) = mrYes then begin
      with Data do begin
        DB.BeginTrans;
        try
          Q1.Close;
          Q1.SQL.text := 'delete from AnagMansioni ' +
            'where ID=' + Data.TAnagMansioniID.AsString;
          Q1.ExecSQL;
          DB.CommitTrans;
          Log_Operation(xIDUtenteAttuale, 'AnagMansioni', Data.TAnagMansioniID.Value, 'D');
          Data.TAnagMansioni.Close;
          Data.TAnagMansioni.Open;
        except
          DB.RollbackTrans;
          MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
        end;
      end;
    end;
end;

procedure TMainForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if not xEsci then begin
    UscitaForm := TUscitaForm.create(self);
    UscitaForm.ShowModal;
    if UscitaForm.ModalResult = mrCancel then begin
      UscitaForm.Free;
      Abort;
    end else begin
      UscitaForm.Free;
      // aggiornamento log accessi
      with Data do begin
        DB.BeginTrans;
        try
          Q1.Close;
          Q1.SQL.Text := 'update Log_accessi set OraUscita=:xOraUscita ' +
            'where ID = (select max(ID) from Log_accessi where IDUtente=:xIDUtente and Data=:xData and Modulo=:xModulo)';
          Q1.ParamByName('xOraUscita').asDateTime := Now;
          Q1.ParamByName('xIDUtente').asInteger := xIDUtenteAttuale;
          Q1.ParamByName('xData').asDateTime := Date;
          Q1.ParamByName('xModulo').asString := 'MB';
          Q1.ExecSQL;
          // azzeramento MAC address
          Q1.Close;
          Q1.SQL.Text := 'update Users set Lana0=NULL ' +
            'where ID=:xIDUtente';
          Q1.ParamByName('xIDUtente').asInteger := xIDUtenteAttuale;
          Q1.ExecSQL;

          DB.CommitTrans;
        except
          DB.RollbackTrans;
          MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
        end;
      end;
    end;
  end;
end;

procedure TMainForm.TbBCompBackClick(Sender: TObject);
begin
  TbBCompBack.Visible := False;
  Data2.TCompetenze.Close;
  PageControl1.ActivePage := TSAnag;
  PageControl2.ActivePage := TSAnagComp;
end;

procedure TMainForm.TbBCarattBackClick(Sender: TObject);
begin
  TbBCarattBack.Visible := False;
  Data2.TCaratteristiche.Close;
  PageControl1.ActivePage := TSAnag;
  PageControl2.ActivePage := TSAnagComp;
end;

procedure TMainForm.ToolbarButton9710Click(Sender: TObject);
begin
  if not CheckProfile('023') then Exit;
  if Data2.TCompDipendente.RecordCount > 0 then begin
    StoricoCompForm := TStoricoCompForm.create(self);
    StoricoCompForm.ShowModal;
    StoricoCompForm.Free;
  end;
end;

procedure TMainForm.CopiadaAppunti1Click(Sender: TObject);
begin
  if not CheckProfile('002') then Exit;
  if Data.TAnagrafica.RecordCount = 0 then Exit;
  if Data.DsAnagrafica.state <> dsEdit then Data.TAnagrafica.Edit;
  OpenDialog1.Filter := 'Immagine bitmap|*.bmp';
  if OpenDialog1.Execute then begin
    DbAnagFoto.Picture.LoadFromFile(OpenDialog1.FileName);
    TbBAnagOKClick(self);
  end;
end;

procedure TMainForm.EliminaFoto1Click(Sender: TObject);
begin
  if not CheckProfile('002') then Exit;
  if Data.TAnagrafica.RecordCount = 0 then Exit;
  if MessageDlg('sei sicuro di voler cancellare la foto per questo soggetto', mtWarning, [mbYes, mbNo], 0) = mrNo then exit;
  if Data.DsAnagrafica.state <> dsEdit then Data.TAnagrafica.Edit;
  Data.TAnagraficaFoto.Value := '';
  TbBAnagOKClick(self);
end;

procedure TMainForm.adattastretch1Click(Sender: TObject);
begin
  if adattastretch1.Checked then begin
    adattastretch1.Checked := False;
    DbAnagFoto.Stretch := False;
  end else begin
    adattastretch1.Checked := True;
    DbAnagFoto.Stretch := True;
  end;
end;

procedure TMainForm.FormShow(Sender: TObject);
var BI: TBitmap;
  C: TComponent;
  xp: string;
  i, l: integer;
begin
  if xMaxCons = 99999 then
    MainForm.Caption := MainForm.Caption
  else MainForm.Caption := MainForm.Caption + ' - LIMITE SOGGETTI = ' + IntToStr(xMaxCons);
  Pagecontrol5.ActivePage := TsStatoTutti;
  PCAziendaOrg.ActivePage := TSAziendeOrgCand;
  PCAnnunci.ActivePage := TSAnnDett;
  PCClienti.ActivePage := TSClientiDett;
  PCAnnunciDett.ActivePage := TSAnnDettRic;
  PanTitoloMansione.visible := False;

  // Numero CV automatico se non espressamente dichiarato per il cliente
  cCVNumAutomatico := True;

  // ABILITAZIONI per CONTROLLO DI GESTIONE
  if copy(Data.GlobalCheckBoxIndici.Value, 6, 1) = '1' then begin
    PCAnnunciDett.Pages[4].TabVisible := True;
  end else begin
    PCAnnunciDett.Pages[4].TabVisible := False;
  end;

  // IMPOSTAZIONI A SECONDA DEL CLIENTE (da GLOBAL)
  if Uppercase(copy(Data.GlobalNomeAzienda.Value, 1, 3)) = 'EBC' then begin
    cCVNumAutomatico := False;
    PanCF.Visible := True;
    PageControl2.Pages[2].TabVisible := True;
    BTabDiplomiITA.Visible := True;
    BTabDiplomiENG.Visible := True;
    BAreeRuoliITA.Visible := True;
    BAreeRuoliENG.Visible := True;
    BAnagRuoliITA.Visible := True;
    BAnagRuoliENG.Visible := True;
  end;
  if Uppercase(copy(Data.GlobalNomeAzienda.Value, 1, 5)) = 'ERGON' then begin
    cCVNumAutomatico := True;
    PanCF.Visible := False;
    PageControl2.Pages[2].TabVisible := False;
    PanTitoloCompensi.Caption := '  Fees associati alla ricerca selezionata';
    BCompensoNew.Caption := 'Nuovo fee';
    BCompensoMod.Caption := 'Modifica fee';
    BCompensoDel.Caption := 'Elimina fee';
    BbValutaz.Visible := False;
    LOBTab2.visible := False;
    PanOldCV.visible := True;
    TSAnagMans.Caption := 'Posizioni';
    LOBTab4.Caption := 'Posizioni';
  end;
  if Uppercase(copy(Data.GlobalNomeAzienda.Value, 1, 7)) = 'ERREMME' then begin
    cCVNumAutomatico := True;
    PanCF.Visible := False;
    PanOldCV.visible := True;
    // CONTRATTI E NON "GRUPPI DI OFFERTE"
    Panel93.Caption := '  Contratti con il cliente';
    BGruppiOffNew.Caption := 'Nuovo contratto';
    BGruppiOffMod.Caption := 'Modifica contratto';
    BGruppiOffDel.Caption := 'Elimina contratto';
  end;
  if Uppercase(copy(Data.GlobalNomeAzienda.Value, 1, 3)) = 'MCS' then begin
    cCVNumAutomatico := True;
  end;
  if Uppercase(copy(Data.GlobalNomeAzienda.Value, 1, 7)) = 'MISTRAL' then begin
    cCVNumAutomatico := True;
  end;

  if Uppercase(copy(Data.GlobalNomeAzienda.Value, 1, 6)) = 'BOYDEN' then begin
    cCVNumAutomatico := True;
    PanCF.Visible := True;
    PanCF.Width := 154;
    PanCF.Left := 87;
    PanOldCV.visible := True;
    // tutti i wallpaper senza il marmo
    WallPaper1.Wallpaper := nil; WallPaper1.caption := '';
    WallPaper2.Wallpaper := nil; WallPaper2.caption := '';
    WallPaper3.Wallpaper := nil; WallPaper3.caption := '';
    WallPaper4.Wallpaper := nil; WallPaper4.caption := '';
    WallPaper5.Wallpaper := nil; WallPaper5.caption := '';
    WallPaper6.Wallpaper := nil; WallPaper6.caption := '';
    WallPaper7.Wallpaper := nil; WallPaper7.caption := '';
    WallPaper8.Wallpaper := nil; WallPaper8.caption := '';
    WallPaper9.Wallpaper := nil; WallPaper9.caption := '';
    WallPaper10.Wallpaper := nil; WallPaper10.caption := '';
    WallPaper11.Wallpaper := nil; WallPaper11.caption := '';
    WallPaper12.Wallpaper := nil; WallPaper12.caption := '';
    WallPaper13.Wallpaper := nil; WallPaper13.caption := '';
    WallPaper14.Wallpaper := nil; WallPaper14.caption := '';
    WallPaper15.Wallpaper := nil; WallPaper15.caption := '';
    WallPaper16.Wallpaper := nil; WallPaper16.caption := '';
    WallPaper17.Wallpaper := nil; WallPaper17.caption := '';
    WallPaper18.Wallpaper := nil; WallPaper18.caption := '';
    WallPaper19.Wallpaper := nil; WallPaper19.caption := '';
    WallPaper20.Wallpaper := nil; WallPaper20.caption := '';
    WallPaper21.Wallpaper := nil; WallPaper21.caption := '';
    WallPaper22.Wallpaper := nil; WallPaper22.caption := '';
    WallPaper23.Wallpaper := nil; WallPaper23.caption := '';
    WallPaper24.Wallpaper := nil; WallPaper24.caption := '';
    WallPaper25.Wallpaper := nil; WallPaper25.caption := '';
    WallPaper26.Wallpaper := nil; WallPaper26.caption := '';
    WallPaper27.Wallpaper := nil; WallPaper27.caption := '';
    WallPaper28.Wallpaper := nil; WallPaper28.caption := '';
    WallPaper29.Wallpaper := nil; WallPaper29.caption := '';
    WallPaper30.Wallpaper := nil; WallPaper30.caption := '';
    WallPaper31.Wallpaper := nil; WallPaper31.caption := '';
    WallPaper32.Wallpaper := nil; WallPaper32.caption := '';
    WallPaper33.Wallpaper := nil; WallPaper33.caption := '';
    WallPaper34.Wallpaper := nil; WallPaper34.caption := '';
    WallPaper35.Wallpaper := nil; WallPaper35.caption := '';
    WallPaper36.Wallpaper := nil; WallPaper36.caption := '';
    WallPaper43.Wallpaper := nil; WallPaper43.caption := '';
  end;

  if Uppercase(copy(Data.GlobalNomeAzienda.Value, 1, 6)) = 'ADVANT' then begin
    cCVNumAutomatico := True;
    PanTitoloMansione.visible := True;
    PanAnagIncompCli.Visible := False;
    DBEIndirizzoRes.Left := 8; DBEIndirizzoRes.Width := 205;
    DBEIndirizzoDom.Left := 8; DBEIndirizzoDom.Width := 205;
    DBCBTipoStradaRes.Visible := False; LTipoStradaRes.Visible := False;
    DBCBTipoStradaDom.Visible := False; LTipoStradaDom.Visible := False;
    LIndirizzoDom.Left := 8; LIndirizzoRes.Left := 8;
    DBEIndirizzoCli.Left := 8; DBEIndirizzoCli.Width := 208;
    DBCBTipoStradaCli.Visible := False; LTipoStradaCli.Visible := False;
    LIndirizzoCli.Left := 8;
    // titoli stati soggetti
    TSStatoDip.Caption := 'attivi';
    TsStatoEsterno.Caption := 'non attivi';
    TsStatoUscito.Caption := 'piazzati';
    // Tipologia
    PanTipologia.Visible := True;
    // soggetti non visibili
    PanPCSoggetti.Visible := False;
    PanPCClienti.Visible := False;
    // bottoni lookout
    LOBAnnunci.Visible := False;
    LOBPromemoria.Visible := False;
    LOBAziende.Visible := False;
    LOBRicerche.caption := 'Ricerche';
  end;

  if Uppercase(copy(Data.GlobalNomeAzienda.Value, 1, 9)) = 'ZUCCHETTI' then begin
    TSAnagComp.Caption := 'Competenze / Conoscenze Informatiche';
  end;

  if Uppercase(copy(Data.GlobalNomeAzienda.Value, 1, 6)) = 'SUITEX' then begin
    //FrmSUITEXClientiExt1.Visible:=True;
  end;

  if Uppercase(copy(Data.GlobalNomeAzienda.Value, 1, 7)) = 'FERRARI' then begin
    cCVNumAutomatico := True;
    PanCF.Visible := True;
    BTabDiplomiITA.Visible := True;
    BTabDiplomiENG.Visible := True;
    BAreeRuoliITA.Visible := True;
    BAreeRuoliENG.Visible := True;
    BAnagRuoliITA.Visible := True;
    BAnagRuoliENG.Visible := True;
  end;

  //if not Data.TAnagrafica.Active then LTotCand.Caption:=''
  //else LTotCand.Caption:=IntToStr(Data.TAnagrafica.RecordCount);
  LTotAz.Caption := '0';

  // parametri griglia ricerca
  Data.Q1.Close;
  Data.Q1.Sql.text := 'select ggDiffApRic,ggDiffUcRic,ggDiffColRic from global ';
  Data.Q1.Open;
  xggDiffApRic := Data.Q1.FieldByname('ggDiffApRic').asInteger;
  xggDiffUcRic := Data.Q1.FieldByname('ggDiffUcRic').asInteger;
  xggDiffColRic := Data.Q1.FieldByname('ggDiffColRic').asInteger;
  Data.Q1.Close;

  // VERIFICA INFORMATION RETRIEVAL DISPONIBILE
  Data.QTemp.Close;
  Data.QTemp.SQL.text := 'SELECT fulltextserviceproperty(''IsFulltextInstalled'') IsFulltextInstalled';
  Data.QTemp.Open;
  xIsFulltextInstalled := Data.QTemp.FieldByName('IsFulltextInstalled').asInteger = 1;
  Data.QTemp.Close;
  Data.QTemp.SQL.text := 'SELECT fulltextcatalogproperty(''EBC1'', ''ItemCount'') ItemCount';
  Data.QTemp.Open;
  xIsFulltextInstalled := Data.QTemp.FieldByName('ItemCount').asString <> '';
  Data.QTemp.Close;

  // abilitazione checkbox per vedere le ricerche degli altri utenti
  CBRicUtentiTutti.Enabled := CheckProfile('26', False);
  xUltimaProv := upperCase(copy(Data.GlobalComuneAzienda.value, 1, 2));
  xcaption := MainForm.Caption;

  FrameDBRichAnagNote.Editor.DataSource := Data.DsQAnagNote;
  FrameDBRich2.Editor.DataSource := Data2.DsEBCclienti;
  FrameDBRich1.Editor.DataSource := DataAnnunci.DsAnnunci;

  xIDRicerca := 0;
  xIDRicercaPres := 0;
  xIDOrg := 0;
  // ESCI SUBITO
  if xEsci then begin
    BEsciClick(self);
  end;
end;

procedure TMainForm.TbBAnagOKClick(Sender: TObject);
begin
  with Data.TAnagrafica do begin
    Data.DB.BeginTrans;
    try
      ApplyUpdates;
      Data.DB.CommitTrans;
    except
      Data.DB.RollbackTrans;
      MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      raise;
    end;
    CommitUpdates;
  end;
end;

procedure TMainForm.TbBEventiModClick(Sender: TObject);
begin
  //if Data.TEventiIDProcedura.asString<>'' then
  //     ShowMessage('Evento predefinito non modificabile')
  //else
  Data.TEventi.Edit;
end;

procedure TMainForm.LOBPromemoriaClick(Sender: TObject);
begin
  // DISABILITAZIONE PROPOSTE Torino -- ALIAS='PROPOS' -- Per ora abilitata
  //if Uppercase(copy(Data.GlobalNomeAzienda.Value,1,6))='PROPOS' then begin
  //     ShowMessage('FUNZIONE NON ABILITATA');
  //     Exit;
  //end;

  if not CheckProfile('4') then Exit;
  AgendaSettForm := TAgendaSettForm.create(self);
  AgendaSettForm.xIDSel := xIDUtenteAttuale;
  AgendaSettForm.ShowModal;
  AgendaSettForm.Free;

  Data.QPromOggi.Close;
  Data.QPromOggi.Prepare;
  Data.QPromOggi.Params[0].AsDateTime := Date;
  Data.QPromOggi.Params[1].Value := xIDUtenteAttuale;
  Data.QPromOggi.Open;

  case Data.QPromOggi.RecordCount of
    0: MainForm.StatusBar1.Panels[2].Text := 'nessun messaggio in scadenza oggi';
    1: MainForm.StatusBar1.Panels[2].Text := '� presente un messaggio in scadenza oggi';
  else MainForm.StatusBar1.Panels[2].Text := 'Sono presenti ' + IntToStr(Data.QPromOggi.RecordCount) + ' messaggi in scadenza oggi';
  end;
end;

procedure TMainForm.LOBArchMessClick(Sender: TObject);
begin
  MessLettiForm := TMessLettiForm.create(self);
  Data.TPromemoria.Open;
  Data.TPromemoria.Filter := 'DataLettura<>null';
  MessLettiForm.ShowModal;
  MessLettiForm.Free;
  Data.TPromemoria.Close;
end;

procedure TMainForm.LOBRicercheClick(Sender: TObject);
begin
  if not CheckProfile('2') then Exit;
  if not DataRicerche.QRicAttive.Active then begin
    if Uppercase(copy(Data.GlobalNomeAzienda.Value, 1, 6)) = 'ADVANT' then
      CBRicUtentiTutti.Checked := True;
    EseguiQRicAttive;
    dxDBGrid1.Filter.Add(dxDBGrid1StatoRic, 'attiva', 'attiva');
  end;
  PageControl1.ActivePage := TSRicerche;
  LOBSchede.Down := False;
  LOBRicerche.Down := True;
  LOBClientiEBC.Down := False;
  LOBAnnunci.Down := False;
  LOBAziende.Down := False;
  TbAnag.Visible := False;
  TbClienti.Visible := False;
  DataAnnunci.TAnnunci.Close;
  DataAnnunci.TAnnunciRic.Close;
  //DisabilitaTabAziende;
  Caption := '[M/2] - ' + xCaption;
end;

procedure TMainForm.LOBSchedeClick(Sender: TObject);
begin
  PanAnag.Visible := False;
  if not CheckProfile('0') then Exit;
  Data.xControlla := True;
  if Data.TAnagrafica.active then
    SettaDiritti(Data.TAnagraficaID.Value);
  PageControl2Change(self);
  TbAnag.Visible := True;
  if Uppercase(copy(Data.GlobalNomeAzienda.Value, 1, 6)) = 'ADVANT' then
    TbAnag.Visible := False;
  TbClienti.Visible := False;

  PageControl1.ActivePage := TSAnag;
  LOBSchede.Down := True;
  LOBRicerche.Down := False;
  LOBClientiEBC.Down := False;
  LOBAnnunci.Down := False;
  LOBAziende.Down := False;
  //DisabilitaTabAziende;
  //DataAnnunci.TAnnunci.open;
  //DataAnnunci.TAnnunciRic.open;

  Caption := '[M/0] - ' + xCaption;
end;

procedure TMainForm.LOBTab1Click(Sender: TObject);
begin
  if not CheckProfile('70') then Exit;
  Data.TStati.Open;
  Data.TEventi.Open;
  PageControl1.ActivePage := TSTabelle;
  PageControl3.ActivePage := TSTabFlussi;
  LOBTab1.Down := True;
  LOBTab2.Down := False;
  LOBTab3.Down := False;
  LOBTab4.Down := False;
  LOBTab7.Down := False;
  LOBTab8.Down := False;
  LOBTabAnn.Down := False;
  LOBTabAttiv.Down := False;
  LOBTabAziende.Down := False;
  LOBTab11.Down := False;
  LOBTabComuni.Down := False;
  LOBTabNazioni.Down := False;
  LOBTabModelliWord.Down := False;
  Caption := '[M/70] - ' + xCaption;
end;

procedure TMainForm.LOBTab2Click(Sender: TObject);
begin
  if not CheckProfile('71') then Exit;
  Data2.TCompetenze.Open;
  Data2.TAreeComp.Open;
  PageControl1.ActivePage := TSTabelle;
  PageControl3.ActivePage := TsTabCompetenze;
  LOBTab1.Down := False;
  LOBTab2.Down := True;
  LOBTab3.Down := False;
  LOBTab4.Down := False;
  LOBTab7.Down := False;
  LOBTab8.Down := False;
  LOBTabAnn.Down := False;
  LOBTabAttiv.Down := False;
  LOBTabAziende.Down := False;
  LOBTab11.Down := False;
  LOBTabComuni.Down := False;
  LOBTabNazioni.Down := False;
  LOBTabModelliWord.Down := False;
  Caption := '[M/71] - ' + xCaption;
end;

procedure TMainForm.LOBTab3Click(Sender: TObject);
begin
  if not CheckProfile('72') then Exit;
  Data2.TCaratteristiche.Open;
  PageControl1.ActivePage := TSTabelle;
  PageControl3.ActivePage := TSTabCaratt;
  LOBTab1.Down := False;
  LOBTab2.Down := False;
  LOBTab3.Down := True;
  LOBTab4.Down := False;
  LOBTab7.Down := False;
  LOBTab8.Down := False;
  LOBTabAnn.Down := False;
  LOBTabAttiv.Down := False;
  LOBTabAziende.Down := False;
  LOBTab11.Down := False;
  LOBTabComuni.Down := False;
  LOBTabNazioni.Down := False;
  LOBTabModelliWord.Down := False;
  Caption := '[M/72] - ' + xCaption;
end;

procedure TMainForm.LOBTab4Click(Sender: TObject);
begin
  if not CheckProfile('73') then Exit;
  Data2.TAree.Open;
  Data2.TMansioni.Open;
  Data2.TCompMansioni.Open;
  Data2.QJobDesc.Open;
  PageControl1.ActivePage := TSTabelle;
  PageControl3.ActivePage := TSTabMansioni;
  LOBTab1.Down := False;
  LOBTab2.Down := False;
  LOBTab3.Down := False;
  LOBTab4.Down := True;
  LOBTab7.Down := False;
  LOBTab8.Down := False;
  LOBTabAnn.Down := False;
  LOBTabAttiv.Down := False;
  LOBTabAziende.Down := False;
  LOBTab11.Down := False;
  LOBTabComuni.Down := False;
  LOBTabNazioni.Down := False;
  LOBTabModelliWord.Down := False;
  Caption := '[M/73] - ' + xCaption;
end;

procedure TMainForm.LOBTab5Click(Sender: TObject);
begin
  Data2.TSpec.Open;
  LOBTab1.Down := False;
  LOBTab2.Down := False;
  LOBTab3.Down := False;
  LOBTab4.Down := False;
  LOBTab7.Down := False;
  LOBTab11.Down := False;
end;

procedure TMainForm.LOBTab11Click(Sender: TObject);
begin
  if not CheckProfile('790') then Exit;
  if not CheckProfile('7904', False) then GBDatiAzienda.Enabled := False;
  if Data.Global.Active then begin
    Data.Global.Close;
    Data.Global.Open;
  end;
  PageControl1.ActivePage := TSTabelle;
  PageControl3.ActivePage := TSTabAltro;
  Data.TRisorse.Open;
  Data.QSedi.Open;
  Data.QProvCV.Open;
  DataRicerche.QTipoCommesse.Open;
  LOBTab1.Down := False;
  LOBTab2.Down := False;
  LOBTab3.Down := False;
  LOBTab4.Down := False;
  LOBTab7.Down := False;
  LOBTab8.Down := False;
  LOBTabAnn.Down := False;
  LOBTabAttiv.Down := False;
  LOBTabAziende.Down := False;
  LOBTab11.Down := True;
  LOBTabComuni.Down := False;
  LOBTabNazioni.Down := False;
  LOBTabModelliWord.Down := False;
  Caption := '[M/790] - ' + xCaption;
end;

procedure TMainForm.LOBTab7Click(Sender: TObject);
begin
  if not CheckProfile('74') then Exit;
  Data.TDiplomi.Open;
  Data.TAreeDiplomi.Open;
  PageControl1.ActivePage := TSTabelle;
  PageControl3.ActivePage := TSTabDiplomi;
  LOBTab1.Down := False;
  LOBTab2.Down := False;
  LOBTab3.Down := False;
  LOBTab4.Down := False;
  LOBTab7.Down := True;
  LOBTab8.Down := False;
  LOBTabAnn.Down := False;
  LOBTabAttiv.Down := False;
  LOBTabAziende.Down := False;
  LOBTab11.Down := False;
  LOBTabComuni.Down := False;
  LOBTabNazioni.Down := False;
  LOBTabModelliWord.Down := False;
  Caption := '[M/74] - ' + xCaption;
end;

procedure TMainForm.LOPTabelleClick(Sender: TObject);
begin
  if not CheckProfile('7') then begin
    LOBTab1.Enabled := False;
    LOBTab2.Enabled := False;
    LOBTab3.Enabled := False;
    LOBTab4.Enabled := False;
    LOBTab7.Enabled := False;
    LOBTab8.Enabled := False;
    LOBTabAnn.Enabled := False;
    LOBTabAttiv.Enabled := False;
    LOBTabAziende.Enabled := False;
    LOBTabComuni.Enabled := False;
    LOBTab11.Enabled := False;
    Caption := '[M/7] - ' + xCaption;
  end else begin
    DisabilitaTabAnag;
    Data.QPromOggi.Close;
    TbAnag.Visible := False;
    TbClienti.Visible := False;
    PageControl1.ActivePage := TSTabelle;
  end;
end;

procedure TMainForm.LOPDipendentiClick(Sender: TObject);
begin
  if Data.DsGlobal.state = dsEdit then
    Data.Global.Post;
  DisabilitaTabGen;
  Data.QPromOggi.Close;
  if LOBSchede.Down then begin
    PageControl1.ActivePage := TSAnag;
    PageControl2Change(self);
    TbAnag.Visible := True;
    if Uppercase(copy(Data.GlobalNomeAzienda.Value, 1, 6)) = 'ADVANT' then
      TbAnag.Visible := False;
    TbClienti.Visible := False;
  end;
  if LOBClientiEBC.Down then begin
    PageControl1.ActivePage := TSClientiEBC;
    TbClienti.Visible := true;
  end;
end;

procedure TMainForm.BbValutazClick(Sender: TObject);
var xIDRuolo: integer;
begin
  if not CheckProfile('052') then Exit;
  if not Data.TAnagrafica.EOF then begin
    ValutazDipForm := TValutazDipForm.create(self);
    Data2.xValutazDipForm := True;
    Data.xValutazForm := True;
    Data.Q1.Close;
    Data.Q1.SQL.Clear;
    if Data.TAnagMansioni.active then xIDRuolo := Data.TAnagMansioniIDMansione.Value
    else begin
      Data.Q1.SQL.text := 'select IDMansione from AnagMansioni where IDAnagrafica=' + Data.TAnagraficaID.AsString;
      Data.Q1.Open;
      if Data.Q1.RecordCount > 0 then xIDRuolo := Data.Q1.FieldByName('IDMansione').asInteger;
      Data.Q1.Close;
    end;
    ValutazDipForm.TRuoli.Locate('ID', xIDRuolo, []);
    ValutazDipForm.Q1.Close;
    ValutazDipForm.Q1.SQL.Clear;
    ValutazDipForm.Q1.SQL.Add('SELECT distinct CompetenzeMansioni.IDMansione ,Competenze.ID IDCompetenza ,');
    ValutazDipForm.Q1.SQL.Add('Competenze.Descrizione ,Competenze.Tipologia , CompetenzeMansioni.Peso ,');
    ValutazDipForm.Q1.SQL.Add('CompetenzeMansioni.Valore ValRichiesto , CompetenzeAnagrafica.IDAnagrafica ,');
    ValutazDipForm.Q1.SQL.Add(' CompetenzeAnagrafica.Valore ValNome');
    ValutazDipForm.Q1.SQL.Add('FROM CompetenzeMansioni,Competenze,CompetenzeAnagrafica');
    ValutazDipForm.Q1.SQL.Add('WHERE ( CompetenzeMansioni.IDCompetenza = Competenze.ID )');
    ValutazDipForm.Q1.SQL.Add('AND ( CompetenzeMansioni.IDCompetenza = CompetenzeAnagrafica.IDCompetenza )');
    ValutazDipForm.Q1.SQL.Add('AND CompetenzeMansioni.IDMansione=:ID');
    ValutazDipForm.Q1.SQL.Add('AND CompetenzeAnagrafica.IDAnagrafica=' + Data.TAnagraficaID.asString);
    ValutazDipForm.Q1.Prepare;
    ValutazDipForm.Q1.Open;

    ValutazDipForm.QTot.Close;
    ValutazDipForm.QTot.SQL.Clear;
    ValutazDipForm.QTot.SQL.Add('SELECT ( sum(CompetenzeMansioni.Peso * CompetenzeAnagrafica.Valore ) /100 ) /');
    ValutazDipForm.QTot.SQL.Add('( sum(CompetenzeMansioni.Peso * CompetenzeMansioni.Valore ) /100 ) * 100 Tot');
    ValutazDipForm.QTot.SQL.Add('FROM CompetenzeMansioni,Competenze,CompetenzeAnagrafica');
    ValutazDipForm.QTot.SQL.Add('WHERE ( CompetenzeMansioni.IDCompetenza = Competenze.ID )');
    ValutazDipForm.QTot.SQL.Add('AND ( CompetenzeMansioni.IDCompetenza = CompetenzeAnagrafica.IDCompetenza )');
    ValutazDipForm.QTot.SQL.Add('AND CompetenzeMansioni.IDMansione=:ID');
    ValutazDipForm.QTot.SQL.Add('AND CompetenzeAnagrafica.IDAnagrafica=' + Data.TAnagraficaID.asString);
    ValutazDipForm.QTot.Prepare;
    ValutazDipForm.QTot.Open;

    ValutazDipForm.ShowModal;
    Data2.xValutazDipForm := False;
    Data.xValutazForm := False;
    ValutazDipForm.Free;
  end;
end;

procedure TMainForm.DisabilitaTabAnag;
begin
  // chiusura tabelle anagrafica
  Data.TAnagMansioni.Close;
  Data2.TCompDipendente.close;
  Data.TStorico.Close;
  Data2.TCarattDip.Close;
end;

procedure TMainForm.DisabilitaTabGen;
begin
  Data.TStati.Close;
  Data.TEventi.Close;
  Data.TDiplomi.Close;
  Data.TAreeDiplomi.Close;
  Data2.TCompetenze.Close;
  Data2.TAreeComp.Close;
  Data2.TCaratteristiche.Close;
  Data2.TAree.Close;
  Data2.TMansioni.Close;
  Data2.TCompMansioni.Close;
  if Data2.DsQJobDesc.state = dsEdit then BJobDescOKClick(self);
  Data2.QJobDesc.Close;
  Data2.TSpec.Close;
  Data.TLingue.Close;
  Data.QComuni.Close;
  Data.TZone.Close;
  Data2.QAttivita.Close;
  Data2.QAreeAttivita.Close;
  Data.TAziende.Close;
  Data.TRisorse.Close;
  DataAnnunci.QTestate.Close;
  DataAnnunci.QEdizioniTest.Close;
  DataAnnunci.TSconti.Close;
  DataAnnunci.TPenetraz.Close;
  DataAnnunci.TQualifiche.Close;
  DataAnnunci.TGiorniIndicati.Close;
  DataAnnunci.TEdizioniLK.Close;
  DataAnnunci.TTestateLK.Close;
  DataRicerche.QTipoCommesse.Close;
  Data.QSedi.Close;
  Data.QProvCV.Close;
  Data.QTipiStrada.Close;
end;

procedure TMainForm.BAnagEventoClick(Sender: TObject);
var xInserito: boolean;
begin
  if not CheckProfile('050') then Exit;
  if Data.TAnagrafica.RecordCount > 0 then begin
    CambiaStato2Form := TCambiaStato2Form.create(self);
    CambiaStato2Form.ECogn.Text := Data.TAnagraficaCognome.Value;
    CambiaStato2Form.ENome.Text := Data.TAnagraficaNome.Value;
    CambiaStato2Form.EStato.text := Data.TAnagraficaDescStato.Value;
    CambiaStato2Form.xIDDaStato := Data.TAnagraficaIDStato.Value;
    CambiaStato2Form.DEData.Date := Date;
    CambiaStato2Form.ShowModal;
    if CambiaStato2Form.ModalResult = mrOK then begin
      xInserito := InsEvento(Data.TAnagraficaID.Value,
        CambiaStato2Form.QNextStatiIDProcedura.Value,
        CambiaStato2Form.QNextStatiID.Value,
        CambiaStato2Form.QNextStatiIDaStato.Value,
        CambiaStato2Form.QNextStatiIDTipoStatoA.Value,
        CambiaStato2Form.DEData.Date,
        Data.TAnagraficaCognome.Value,
        Data.TAnagraficaNome.Value,
        CambiaStato2Form.Edit1.Text,
        0,
        xIDUtenteAttuale,
        0);
      BAnagSearchClick(self);
    end;
    CambiaStato2Form.Free;
  end;
end;

procedure TMainForm.BAnagCurriculumClick(Sender: TObject);
var x: string;
  i: integer;
begin
  if not CheckProfile('051') then Exit;
  if not Data.TAnagrafica.IsEmpty then begin
    Data.TTitoliStudio.Open;
    Data.TCorsiExtra.Open;
    Data.TLingueConosc.Open;
    Data.TEspLav.Open;

    CurriculumForm.ShowModal;

    Data.TTitoliStudio.Close;
    Data.TCorsiExtra.Close;
    Data.TLingueConosc.Close;
    Data.TEspLav.Close;
    // refresh anagrafica
    if Data.dsAnagrafica.State = dsEdit then begin
      with Data.TAnagrafica do begin
        Data.DB.BeginTrans;
        try
          ApplyUpdates;
          Data.DB.CommitTrans;
        except
          Data.DB.RollbackTrans;
          MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
          raise;
        end;
        CommitUpdates;
      end;
    end;
  end;
end;

procedure TMainForm.BEsciClick(Sender: TObject);
begin
  close
end;

procedure TMainForm.BVediCVClick(Sender: TObject);
var xFile: string;
  xProcedi: boolean;
begin
  ApriCV(Data.TAnagraficaID.value);
end;

procedure TMainForm.FormResize(Sender: TObject);
begin
  LookOut1.Refresh;
end;

procedure TMainForm.SpeedButton5Click(Sender: TObject);
begin
  {     if not (Data.DsAnagrafica.State=dsEdit) then
          Data.DsAnagrafica.Edit;
          Data.TAnagraficaCodiceFiscale.Value:='';
          Data.DsAnagrafica.Post;
  }
  DBEdit17.Clear
end;

procedure TMainForm.TbBGestStoricoClick(Sender: TObject);
begin
  if not CheckProfile('040') then Exit;
  Data.QStorico.First;
  GestStoricoForm := TGestStoricoForm.create(self);
  GestStoricoForm.ShowModal;
  GestStoricoForm.Free;
end;

procedure TMainForm.ToolbarButton978Click(Sender: TObject);
begin
  if not Data.QPromOggi.EOF then begin
    Data.TPromemABS.Open;
    Data.TPromemABS.FindKey([Data.QPromOggiID.value]);
    Data.TPromemABS.Edit;
    Data.TPromemABSDataLettura.Value := Date;
    Data.TPromemABS.Post;
    Data.TPromemABS.Close;
    Data.QPromOggi.Close;
    Data.QPromOggi.Open;
  end;
end;

procedure TMainForm.ToolbarButton9716Click(Sender: TObject);
var xData: string;
  xVecchioVal, xVarPerc: integer;
begin
  if not CheckProfile('022') then Exit;
  if Data2.TCompDipendente.RecordCount = 0 then exit;
  NuovaCompForm := TNuovaCompForm.create(self);
  NuovaCompForm.xComp := Data2.TCompDipendenteDescCompetenza.Value;
  NuovaCompForm.SEVecchioVal.Value := Data2.TCompDipendenteValore.asInteger;
  NuovaCompForm.SENuovoVal.Value := Data2.TCompDipendenteValore.asInteger;
  NuovaCompForm.xMot := 'a seguito di valutazione';
  NuovaCompForm.DEData.Date := Date;
  NuovaCompForm.ShowModal;
  if NuovaCompForm.ModalResult = mrOK then begin
    Data.DB.BeginTrans;
    try
      // se il valore � cambiato ==> aggiorna storico
      if NuovaCompForm.SENuovoVal.Value <> NuovaCompForm.SEVecchioVal.Value then begin
        xVecchioVal := NuovaCompForm.SEVecchioVal.Value;
        if xVecchioVal > 0 then
          xVarPerc := Trunc(((Data2.TCompDipendenteValore.Value - xVecchioVal) / xVecchioVal) * 100)
        else xVarPerc := Data2.TCompDipendenteValore.Value * 100;

        Data.Q1.SQL.text := 'insert into StoricoCompAnag (IDCompetenza,IDAnagrafica,DallaData,MotivoAumento,valore,VariazPerc) ' +
          'values (:xIDCompetenza,:xIDAnagrafica,:xDallaData,:xMotivoAumento,:xvalore,:xVariazPerc)';
        Data.Q1.ParamByName('xIDCompetenza').asInteger := Data2.TCompDipendenteIDCompetenza.Value;
        Data.Q1.ParamByName('xIDAnagrafica').asInteger := Data2.TCompDipendenteIDAnagrafica.Value;
        Data.Q1.ParamByName('xDallaData').asDateTime := NuovaCompForm.DEData.Date;
        Data.Q1.ParamByName('xMotivoAumento').asString := NuovaCompForm.xMot;
        Data.Q1.ParamByName('xValore').asInteger := NuovaCompForm.SENuovoVal.Value;
        // VECCHIO
        //Data.Q1.ParamByName('xValore').asInteger:=Data2.TCompDipendenteValore.Value;
        Data.Q1.ParamByName('xVariazPerc').asInteger := xVarPerc;
        Data.Q1.ExecSQL;
      end;
      // aggiornamento CompetenzeAnagrafica
      Data.Q1.SQL.text := 'update CompetenzeAnagrafica set Valore=:xValore ' +
        'where ID=' + Data2.TCompDipendenteID.asString;
      Data.Q1.ParamByName('xValore').asInteger := NuovaCompForm.SENuovoVal.Value;
      Data.Q1.ExecSQL;

      Data.DB.CommitTrans;
    except
      Data.DB.RollbackTrans;
      MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
    end;

    Data2.TCompDipendente.Close;
    Data2.TCompDipendente.Open;
  end;
  NuovaCompForm.Free;
end;

procedure TMainForm.TbBAreeCompNewClick(Sender: TObject);
var xAreaComp: string;
begin
  xAreaComp := '';
  if InputQuery('Nuova area competenza', 'nome:', xAreaComp) then begin
    if trim(xAreaComp) = '' then begin
      MessageDlg('L''area non pu� avere descrizione nulla', mtError, [mbOK], 0);
      Exit;
    end;
    Data2.TAreaCompIDX.Open;
    if Data2.TAreaCompIDX.FindKey([xAreaComp]) then begin
      MessageDlg('Area competenza gi� esistente', mtError, [mbOK], 0);
      Exit;
    end;
    Data2.TAreaCompIDX.Close;
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'insert into AreeCompetenze (Descrizione) ' +
          'values (:xDescrizione)';
        Q1.ParamByName('xDescrizione').asString := xAreaComp;
        Q1.ExecSQL;
        DB.CommitTrans;
        Data2.TAreecomp.Close;
        Data2.TAreecomp.open;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
  end;
end;

procedure TMainForm.TbBAreeCompModClick(Sender: TObject);
var xAreaComp: string;
begin
  if not Data2.TAreeComp.Eof then begin
    xAreaComp := Data2.TAreeCompDescrizione.Value;
    if InputQuery('Modifica area competenza', 'denominazione:', xAreaComp) then begin
      if trim(xAreaComp) = '' then begin
        MessageDlg('L''area competenza non pu� avere descrizione nulla', mtError, [mbOK], 0);
        Abort;
        Exit;
      end;
      with Data do begin
        DB.BeginTrans;
        try
          Q1.Close;
          Q1.SQL.text := 'update AreeCompetenze set Descrizione=:xDescrizione ' +
            'where ID=' + Data2.TAreeCompID.asString;
          Q1.ParamByName('xDescrizione').asString := xAreaComp;
          Q1.ExecSQL;
          DB.CommitTrans;
          Data2.TAreecomp.Close;
          Data2.TAreecomp.open;
        except
          DB.RollbackTrans;
          MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
        end;
      end;
    end;
  end;
end;

procedure TMainForm.TbBAreeCompDelClick(Sender: TObject);
begin
  // controllo competenze legati all'area
  Data2.QTemp.Close;
  Data2.QTemp.SQL.clear;
  Data2.QTemp.SQL.text := 'select count(*) Tot from Competenze where IDArea=' + Data2.TAreeCompID.asString;
  Data2.QTemp.Open;
  if Data2.QTemp.FieldByName('Tot').asInteger > 0 then begin
    MessageDlg('Ci sono competenze legate: impossibile cancellare l''area', mtError, [mbOK], 0);
    Exit;
    Data2.QTemp.Close;
  end;
  Data2.QTemp.Close;
  if Data2.TAreeComp.RecordCount > 0 then
    if MessageDlg('Sei sicuro di voler cancellare ?', mtWarning,
      [mbNo, mbYes], 0) = mrYes then begin
      with Data do begin
        DB.BeginTrans;
        try
          Q1.Close;
          Q1.SQL.text := 'delete from AreeCompetenze ' +
            'where ID=' + Data2.TAreeCompID.asString;
          Q1.ExecSQL;
          DB.CommitTrans;
          Data2.TAreecomp.Close;
          Data2.TAreecomp.open;
        except
          DB.RollbackTrans;
          MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
        end;
      end;
    end;
end;

procedure TMainForm.DriveComboBox1Change(Sender: TObject);
begin
  //DriveComboBox1.Drive;
end;

procedure TMainForm.TbBClientiNewClick(Sender: TObject);
var xDenom: string;
begin
  if not CheckProfile('100') then Exit;
  //     xDenom:='';
  //     InputQuery('Nuovo cliente','Nome:',xDenom);
  //     Data2.TEBCclienti.InsertRecord([xDenom]);

  InsClienteForm := TInsClienteForm.create(self);
  InsClienteForm.DEDataCon.date := Date;
  InsClienteForm.ShowModal;
  if InsClienteForm.ModalResult = mrOK then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'insert into EBC_Clienti (Descrizione,Stato,IDAttivita,ConosciutoInData) ' +
          'values (:xDescrizione,:xStato,:xIDAttivita,:xConosciutoInData)';
        Q1.ParamByName('xDescrizione').asString := InsClienteForm.EDenominazione.text;
        Q1.ParamByName('xStato').asString := InsClienteForm.CBStato.text;
        Q1.ParamByName('xIDAttivita').asInteger := InsClienteForm.xIDAttivita;
        Q1.ParamByName('xConosciutoInData').asDateTime := InsClienteForm.DEDataCon.Date;
        Q1.ExecSQL;
        DB.CommitTrans;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
    Data2.TEBCclienti.Close;
    Data2.TEBCclienti.Open;
  end;
  // posizionati su questo nuovo cliente
  EClienti.text := copy(InsClienteForm.EDenominazione.text, 1, 14);
  InsClienteForm.Free;
  BCliSearchClick(self);
end;

procedure TMainForm.TbBClientiDelClick(Sender: TObject);
var xTabFiglie: string;
begin
  if not CheckProfile('101') then Exit;
  if not Data2.TEBCclienti.IsEmpty then
    if MessageDlg('Sei proprio sicuro di voler cancellare l''azienda selezionata ?', mtWarning,
      [mbNo, mbYes], 0) = mrYes then begin

      Data.Q1.Close;
      Data.Q1.SQL.text := 'select count(*) Tot from EBC_candidatiRicerche ' +
        'where IDRicerca in (select ID from EBC_Ricerche where IDCliente=' + Data2.TEBCClientiID.asString + ')';
      Data.Q1.Open;
      if Data.Q1.fieldByName('TOT').asInteger > 0 then begin
        MessageDlg('Risultano candidati associati a commesse per questa azienda' + chr(13) +
          'IMPOSSIBILE CANCELLARE direttamente:  cancellare prima le commesse', mtError, [mbOK], 0);
        exit;
      end;
      Data.Q1.Close;

      xTabFiglie := '';
      Data.Q1.SQL.text := 'select count(*) TOT from AnagIncompClienti where IDCliente=' + Data2.TEBCClientiID.asString;
      Data.Q1.Open;
      if Data.Q1.fieldByName('TOT').asInteger > 0 then xTabFiglie := xTabFiglie + '- Incompatibilit� con soggetti' + chr(13);
      Data.Q1.Close;
      Data.Q1.SQL.text := 'select count(*) TOT from Organigramma where IDAzienda=' + Data2.TEBCClientiID.asString;
      Data.Q1.Open;
      if Data.Q1.fieldByName('TOT').asInteger > 0 then xTabFiglie := xTabFiglie + '- Organigramma' + chr(13);
      Data.Q1.Close;
      Data.Q1.SQL.text := 'select count(*) TOT from EsperienzeLavorative where IDAzienda=' + Data2.TEBCClientiID.asString;
      Data.Q1.Open;
      if Data.Q1.fieldByName('TOT').asInteger > 0 then xTabFiglie := xTabFiglie + '- Esperienze Lavorative soggetti' + chr(13);
      Data.Q1.Close;
      Data.Q1.SQL.text := 'select count(*) TOT from Storico where IDCliente=' + Data2.TEBCClientiID.asString;
      Data.Q1.Open;
      if Data.Q1.fieldByName('TOT').asInteger > 0 then xTabFiglie := xTabFiglie + '- Storico soggetti' + chr(13);
      Data.Q1.Close;
      Data.Q1.SQL.text := 'select count(*) TOT from EBC_Ricerche where IDCliente=' + Data2.TEBCClientiID.asString;
      Data.Q1.Open;
      if Data.Q1.fieldByName('TOT').asInteger > 0 then xTabFiglie := xTabFiglie + '- Commesse/ricerche' + chr(13);

      if xTabFiglie <> '' then begin
        if MessageDlg('ATTENZIONE:  risultano collegati i seguenti dati:' + chr(13) +
          xTabFiglie + 'SEI SICURO DI VOLER PROSEGUIRE ?', mtWarning, [mbYes, mbNo], 0) = mrNo then
          exit;
      end;

      if Data2.TEBCclientiStato.Value = 'eliminato' then
        Data2.TEBCclienti.Delete
      else
        if MessageDlg('Cancellare completamente (Yes) o mettere tra gli eliminati (No) ?', mtWarning,
          [mbNo, mbYes], 0) = mrYes then begin
          with Data do begin
            DB.BeginTrans;
            try
              // eliminare tabelle collegate da CONSTRAINT di integrit� referenziale
              Q1.Close;
              Q1.SQL.text := 'delete from AnagIncompClienti where IDCliente=' + Data2.TEBCClientiID.asString;
              Q1.ExecSQL;
              Q1.SQL.text := 'delete from Organigramma where IDAzienda=' + Data2.TEBCClientiID.asString;
              Q1.ExecSQL;
              Q1.SQL.text := 'delete from EsperienzeLavorative where IDAzienda=' + Data2.TEBCClientiID.asString;
              Q1.ExecSQL;
              Q1.SQL.text := 'delete from Storico where IDCliente=' + Data2.TEBCClientiID.asString;
              Q1.ExecSQL;
              Q1.SQL.text := 'delete from EBC_Ricerche where IDCliente=' + Data2.TEBCClientiID.asString;
              Q1.ExecSQL;

              Q1.SQL.text := 'delete from EBC_Clienti ' +
                'where ID=' + Data2.TEBCClientiID.asString;
              Q1.ExecSQL;
              DB.CommitTrans;
              BCliSearchClick(self);
            except
              DB.RollbackTrans;
              MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
              raise;
            end;
          end;
        end else begin
          Data2.TEBCclienti.Edit;
          Data2.TEBCclientiStato.Value := 'eliminato';
          Data2.TEBCclienti.Post;
        end;
    end;
end;

procedure TMainForm.TbBClientiModClick(Sender: TObject);
begin
  if not CheckProfile('102') then Exit;
  if not Data2.TEBCclienti.IsEmpty then begin
    PanCliente.Enabled := True;
    Data2.TEBCclienti.Edit;
  end;
end;

procedure TMainForm.TbBClientiOKClick(Sender: TObject);
begin
  Data2.TEBCClienti.Post;
  PanCliente.Enabled := False;
end;

procedure TMainForm.TbBClientiCanClick(Sender: TObject);
begin
  Data2.TEBCclienti.Cancel;
  PanCliente.Enabled := False;
end;

procedure TMainForm.TbBContattiCliNewClick(Sender: TObject);
begin
  if not CheckProfile('103') then Exit;
  ContattoCliForm := TContattoCliForm.create(self);
  ContattoCliForm.ShowModal;
  if ContattoCliForm.ModalResult = mrOK then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'insert into EBC_ContattiClienti (IDCliente,Contatto,Telefono,Fax,Email,Note,DataIns,IDUtente,Titolo) ' +
          'values (:xIDCliente,:xContatto,:xTelefono,:xFax,:xEmail,:xNote,:xDataIns,:xIDUtente,:xTitolo)';
        Q1.ParamByName('xIDCliente').asInteger := Data2.TEBCClientiID.Value;
        Q1.ParamByName('xContatto').asString := ContattoCliForm.EContatto.text;
        Q1.ParamByName('xTelefono').asString := ContattoCliForm.ETelefono.text;
        Q1.ParamByName('xFax').asString := ContattoCliForm.EFax.text;
        Q1.ParamByName('xEmail').asString := ContattoCliForm.EEmail.text;
        Q1.ParamByName('xNote').asString := ContattoCliForm.ENote.text;
        Q1.ParamByName('xDataIns').asDateTime := Date;
        Q1.ParamByName('xIDUtente').asInteger := xIDUtenteAttuale;
        Q1.ParamByName('xTitolo').asString := ContattoCliForm.ETitolo.text;
        Q1.ExecSQL;
        DB.CommitTrans;
        Data2.TContattiClienti.Close;
        Data2.TContattiClienti.open;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
    if MessageDlg('Vuoi inserire gli altri dati ?', mtInformation, [mbYes, mbNo], 0) = mrYes then begin
      if not CheckProfile('103') then begin
        ShowMessage('Accesso non consentito');
        Exit;
      end else begin
        Data2.TContattiClienti.Last;
        TbBContattiCliAltriDatiClick(self);
      end;
    end;
  end;
  ContattoCliForm.Free;
end;

procedure TMainForm.TbBContattiCliDelClick(Sender: TObject);
begin
  if Data2.TContattiClienti.IsEmpty then exit;
  if not CheckProfile('103') then Exit;
  if MessageDlg('Sei sicuro di voler cancellare ?', mtWarning,
    [mbNo, mbYes], 0) = mrYes then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'Delete from EBC_ContattiClienti ' +
          'where ID=' + Data2.TContattiClientiID.asString;
        Q1.ExecSQL;
        DB.CommitTrans;
        Data2.TContattiClienti.Close;
        Data2.TContattiClienti.open;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
  end;
end;

procedure TMainForm.PCClientiChange(Sender: TObject);
begin
  if (PageControl2.ActivePage = TSClientiDett) and
    (not CheckProfile('10', false)) then begin
    TSClientiDett.Visible := False;
    Exit;
  end;

  if not CheckProfile('10', false) then begin
    PanCliente.Visible := False;
    PanCliContatti.Visible := False;
    PanCliPag.Visible := False;
  end else begin
    PanCliente.Visible := True;
    PanCliContatti.Visible := True;
    PanCliPag.Visible := True;
  end;

  if PCClienti.ActivePage = TSClientiRic then begin
    // commesse e compensi
    if not CheckProfile('12') then begin
      PCClienti.ActivePage := xPaginaCliPrec;
      Exit;
    end;
    Data2.QRicClienti.Open;
    Data2.TCompensiRic.Open;
    Data2.QTotCompenso.Open;
    Data2.QTotComp2.Open;
    if not CheckProfile('121', False) then
      PanCliCompensi.visible := False;
    if (not CheckProfile('126', False)) and (Data2.QRicClientiIDUtente.Value <> xIDUtenteAttuale) then
      PanCliCompensi.visible := False
    else PanCliCompensi.visible := True;
  end else begin
    Data2.QRicClienti.Close;
    Data2.TCompensiRic.Close;
    Data2.QTotCompenso.Close;
    Data2.QTotComp2.Close;
  end;

  if PCClienti.ActivePage = TSClientiFatt then begin
    if not CheckProfile('13') then begin
      PCClienti.ActivePage := xPaginaCliPrec;
      Exit;
    end;
    Data2.TFattClienti.Open;
    Data2.QTotFattCli.Open;
  end else begin
    Data2.TFattClienti.Close;
    Data2.QTotFattCli.Close;
  end;

  if PCClienti.ActivePage = TSClientiAnnunci then begin
    if not CheckProfile('18') then begin
      PCClienti.ActivePage := xPaginaCliPrec;
      Exit;
    end;
    ClienteAnnunciFrame.xIDCliente := Data2.TEBCClientiID.Value;
    ClienteAnnunciFrame.xCliente := Data2.TEBCClientiDescrizione.Value;
    ClienteAnnunciFrame.QAnnunciCli.ParambyName('xIDCliente').asInteger := Data2.TEBCClientiID.Value;
    ClienteAnnunciFrame.QListinoCliente.ParambyName('xIDCliente').asInteger := Data2.TEBCClientiID.Value;
    ClienteAnnunciFrame.QAnnunciCli.Open;
    ClienteAnnunciFrame.QListinoCliente.Open;
    if not CheckProfile('182', False) then
      ClienteAnnunciFrame.PanListini.Visible := False;
  end else begin
    ClienteAnnunciFrame.QAnnunciCli.Close;
    ClienteAnnunciFrame.QListinoCliente.Close;
  end;

  // contratti e offerte
  if PCClienti.ActivePage = TSContattiOfferte then begin
    if not CheckProfile('14') then begin
      PCClienti.ActivePage := xPaginaCliPrec;
      Exit;
    end;
    Data2.QCliContratti.Open;
    Data2.TCliOfferte.Open;
  end else begin
    Data2.TCliOfferte.Close;
    Data2.QCliContratti.Close;
  end;

  if PCClienti.ActivePage = TSClientiNoteSpese then begin
    if not CheckProfile('15') then begin
      PCClienti.ActivePage := xPaginaCliPrec;
      Exit;
    end;
    Data2.TNoteSpeseCli.Open;
  end else begin
    Data2.TNoteSpeseCli.Close;
  end;

  // note e file collegati
  if PCClienti.ActivePage = TSClientiContratto then begin
    if not CheckProfile('16') then begin
      PCClienti.ActivePage := xPaginaCliPrec;
      Exit;
    end;
    Data2.QClienteFile.Open;
    if Data2.TEBCClientiCartellaDoc.Value <> '' then begin
      if DirectoryExists(Data2.TEBCClientiCartellaDoc.Value) then
        DirCartellaCli.Directory := Data2.TEBCClientiCartellaDoc.Value;
    end else DirCartellaCli.Directory := 'c:\';
  end else Data2.QClienteFile.Close;

  // candidati e aziende collegate
  if PCClienti.ActivePage = TSClientiCV then begin
    if not CheckProfile('11') then begin
      PCClienti.ActivePage := xPaginaCliPrec;
      Exit;
    end;
    if CheckProfile('110', False) then begin
      CliCandidatiFrame1.xIDCliente := Data2.TEBCClientiID.Value;
      CliCandidatiFrame1.xIDRicerca := 0;
      CliCandidatiFrame1.CreaAlbero;
    end;
    Data2.QCliRefCli.Open;
  end else Data2.QCliRefCli.Close;

  // contatti avuti con il cliente
  if PCClienti.ActivePage = TSCliContatti then begin
    if not CheckProfile('19') then begin
      PCClienti.ActivePage := xPaginaCliPrec;
      Exit;
    end;
    Data2.TCliContattiFatti.Open;
    if Uppercase(copy(Data.GlobalNomeAzienda.Value, 1, 6)) = 'SUITEX' then
      //FrmSUITEXClientiExt1.QDitteExt.Open;
  end else begin
    Data2.TCliContattiFatti.Close;
    if Uppercase(copy(Data.GlobalNomeAzienda.Value, 1, 6)) = 'SUITEX' then
      //FrmSUITEXClientiExt1.QDitteExt.Close;
  end;

  // fornitori
  if PCClienti.ActivePage = TSCliForn then begin
    Data2.QFornContiCosto.Open;
    Data2.QFornCosti.Open;
  end else begin
    Data2.QFornContiCosto.Close;
    Data2.QFornCosti.Close;
  end;

end;

procedure TMainForm.RGFiltroRicCliClick(Sender: TObject);
begin
  Data2.QTotCompenso.Close;
  Data2.QTotCompenso.SQL.Clear;
  Data2.QTotCompenso.SQL.Add('select sum(EBC_CompensiRicerche.Importo) TotCompenso from EBC_CompensiRicerche,EBC_Ricerche');
  Data2.QTotCompenso.SQL.Add('where EBC_CompensiRicerche.IDRicerca=EBC_Ricerche.ID and ' +
    '(EBC_CompensiRicerche.IDFattura is null or EBC_CompensiRicerche.IDFattura=0) and EBC_Ricerche.IDCliente=:ID');
  Data2.QTotComp2.Close;
  Data2.QTotComp2.SQL.Clear;
  Data2.QTotComp2.SQL.Add('select sum(EBC_CompensiRicerche.Importo) TotCompenso from EBC_CompensiRicerche,EBC_Ricerche');
  Data2.QTotComp2.SQL.Add('where EBC_CompensiRicerche.IDRicerca=EBC_Ricerche.ID and ' +
    '(EBC_CompensiRicerche.IDFattura is not null and EBC_CompensiRicerche.IDFattura>0) and EBC_Ricerche.IDCliente=:ID');

  Data2.QRicClienti.Close;
  Data2.QRicClienti.SQL.Text := 'select EBC_Ricerche.ID,IDUtente,Progressivo,DataInizio,DataFine,NumRicercati,DallaData, ' +
    'Mansioni.Descrizione Ruolo, Users.Nominativo Utente,EBC_StatiRic.StatoRic,EBC_Ricerche.StipendioLordo ' +
    'from EBC_Ricerche,Mansioni,EBC_StatiRic,Users where EBC_Ricerche.IDMansione=Mansioni.ID ' +
    'and EBC_Ricerche.IDStatoRic=EBC_StatiRic.ID and EBC_Ricerche.IDUtente=Users.ID and EBC_Ricerche.IDCliente=:ID ';

  case RGFiltroRicCli.ItemIndex of
    1: begin
        Data2.QRicClienti.SQL.Add('and EBC_Ricerche.IDStatoRic=1');
        Data2.QTotCompenso.SQL.Add('and EBC_Ricerche.IDStatoRic=1');
        Data2.QTotComp2.SQL.Add('and EBC_Ricerche.IDStatoRic=1');
      end;
    2: begin
        Data2.QRicClienti.SQL.Add('and EBC_Ricerche.IDStatoRic<>1');
        Data2.QTotCompenso.SQL.Add('and EBC_Ricerche.IDStatoRic<>1');
        Data2.QTotComp2.SQL.Add('and EBC_Ricerche.IDStatoRic<>1');
      end;
  end;
  Data2.QRicClienti.open;
  Data2.QTotCompenso.Open;
  Data2.QTotComp2.Open;
end;

procedure TMainForm.LOBClientiEBCClick(Sender: TObject);
begin
  if not CheckProfile('10', false) then begin
    PanCliente.Visible := False;
    PanCliContatti.Visible := False;
    PanCliPag.Visible := False;
  end else begin
    PanCliente.Visible := True;
    PanCliContatti.Visible := True;
    PanCliPag.Visible := True;
  end;
  if not CheckProfile('1') then Exit;
  PageControl1.ActivePage := TSClientiEBC;
  LOBSchede.Down := False;
  LOBRicerche.Down := False;
  LOBClientiEBC.Down := True;
  LOBAnnunci.Down := False;
  LOBAziende.Down := False;
  TbAnag.Visible := False;
  TbClienti.Visible := True;
  DataAnnunci.TAnnunci.Close;
  DataAnnunci.TAnnunciRic.Close;
  //DisabilitaTabAziende;
  Caption := '[M/1] - ' + xCaption;
end;

procedure TMainForm.BInsRicercaClick(Sender: TObject);
var xIDEvento, xIDClienteBlocco: integer;
  xVai: boolean;
  xRic, xLivProtez, xDicMess, xPassword, xUtenteResp, xClienteBlocco: string;
  QLocal: TQuery;
begin
  if not CheckProfile('010') then Exit;
  // controllo propriet� CV
  if (Data.TAnagraficaIDProprietaCV.asString <> '') and (Data.TAnagraficaIDProprietaCV.Value > 0) then
    if MessageDlg('ATTENZIONE: il CV risulta di propriet� del cliente ' + data.TAnagraficaProprietaCV.asString + chr(13) +
      'Vuoi proseguire lo stesso ?', mtWarning, [mbYes, mbNo], 0) = mrNo then exit;
  ElencoRicPendForm := TElencoRicPendForm.create(self);
  ElencoRicPendForm.ShowModal;
  DataRicerche.TRicAnagIDX.Open;
  if ElencoRicPendForm.ModalResult = mrOK then begin
    xVai := true;
    // controllo se � gi� associato ad un'altra ricerca per la STESSA azienda
    Data.Q1.Close;
    Data.Q1.SQL.clear;
    Data.Q1.SQl.Add('select EBC_Ricerche.Progressivo,EBC_StatiRic.StatoRic from EBC_CandidatiRicerche,EBC_Ricerche,EBC_StatiRic ');
    Data.Q1.SQl.Add('where EBC_CandidatiRicerche.IDRicerca=EBC_Ricerche.ID and EBC_Ricerche.IDStatoRic=EBC_StatiRic.ID');
    Data.Q1.SQl.Add('and EBC_CandidatiRicerche.IDAnagrafica=:xIDAnag and EBC_Ricerche.IDCliente=:xIDCliente');
    Data.Q1.Prepare;
    Data.Q1.ParamByName('xIDAnag').asInteger := data.TAnagraficaID.Value;
    Data.Q1.ParamByName('xIDCliente').asInteger := ElencoRicPendForm.QRicAttiveIDCliente.Value;
    Data.Q1.Open;
    if not Data.Q1.IsEmpty then begin
      xRic := '';
      while not Data.Q1.EOF do begin xRic := xRic + 'N�' + Data.Q1.FieldByName('Progressivo').asString + ' (' + Data.Q1.FieldByName('StatoRic').asString + ') '; Data.Q1.Next; end;
      if MessageDlg('Il soggetto � associato gi� alle seguenti ricerche per lo stesso cliente: ' + chr(13) + xRic + chr(13) + 'PROCEDERE COMUNQUE ?', mtWarning, [mbYes, mbNo], 0) = mrNo then xVai := False;
    end;
    Data.Q1.Close;

    // controllo incompatibilit�
    if IncompAnagCli(Data.TAnagraficaID.Value, ElencoRicPendForm.QRicAttiveIDCliente.Value) then
      if MessageDlg('Il soggetto risulta INCOMPATIBILE con questo cliente o con un cliente associato' + chr(13) +
        'PROCEDERE COMUNQUE ?', mtWarning, [mbYes, mbNo], 0) = mrNo then begin
        DataRicerche.TRicAnagIDX.Close;
        ElencoRicPendForm.Free;
        exit;
      end;
    // controllo blocco livello 1 o 2
    xIDClienteBlocco := CheckAnagInseritoBlocco1(Data.TAnagraficaID.Value);
    if xIDClienteBlocco > 0 then begin
      xLivProtez := copy(IntToStr(xIDClienteBlocco), 1, 1);
      if xLivProtez = '1' then begin
        xIDClienteBlocco := xIDClienteBlocco - 10000;
        xClienteBlocco := GetDescCliente(xIDClienteBlocco);
        xDicMess := 'Stai inserendo in commessa un candidato appartenente ad una azienda (' + xClienteBlocco + ') con blocco di livello 1, ';
      end else begin
        xIDClienteBlocco := xIDClienteBlocco - 20000;
        xClienteBlocco := GetDescCliente(xIDClienteBlocco);
        xDicMess := 'Stai inserendo in commessa un candidato appartenente ad una azienda ATTIVA (' + xClienteBlocco + '), ';
      end;
      if MessageDlg('ATTENZIONE!! PROTEZIONE CLIENTI LIVELLO ' + xLivProtez + chr(13) + xDicMess +
        'protetta dalle normative interne, se sei autorizzato premi SI, altrimenti premi NO. ' +
        'Se si prosegue, l''operazione verr� registrata nello storico del soggetto come forzatura. ' +
        'Verr� inviato un messaggio di promemoria al responsabile diretto', mtWarning, [mbYes, mbNo], 0) = mrNo then xVai := False
      else begin
        // pwd
        xUtenteResp := GetDescUtenteResp(MainForm.xIDUtenteAttuale);
        if not InputQuery('Forzatura blocco livello ' + xLivProtez, 'Password di ' + xUtenteResp, xPassword) then exit;
        if not CheckPassword(xPassword, GetPwdUtenteResp(MainForm.xIDUtenteAttuale)) then begin
          MessageDlg('Password errata', mtError, [mbOK], 0);
          xVai := False;
        end;
        if xVai then begin
          // promemoria al resp.
          QLocal := CreateQueryFromString('insert into Promemoria (IDUtente,IDUtenteDa,DataIns,Testo,Evaso,DataDaLeggere) ' +
            'values (:xIDUtente,:xIDUtenteDa,:xDataIns,:xTesto,:xEvaso,:xDataDaLeggere)');
          QLocal.ParamByName('xIDUtente').asInteger := GetIDUtenteResp(MainForm.xIDUtenteAttuale);
          QLocal.ParamByName('xIDUtenteDa').asInteger := MainForm.xIDUtenteAttuale;
          QLocal.ParamByName('xDataIns').asDateTime := Date;
          QLocal.ParamByName('xTesto').asString := 'forzatura livello ' + xLivProtez + ' per CV n� ' + Data.TAnagraficaCVNumero.AsString;
          QLocal.ParamByName('xEvaso').asBoolean := False;
          QLocal.ParamByName('xDataDaLeggere').asDateTime := Date;
          QLocal.ExecSQL;
          // registra nello storico soggetto
          xIDEvento := GetIDEvento('forzatura Blocco livello ' + xLivProtez);
          if xIDEvento > 0 then begin
            with Data.Q1 do begin
              close;
              SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
                'values (:xIDAnagrafica,:xIDEvento,:xDataEvento,:xAnnotazioni,:xIDRicerca,:xIDUtente,:xIDCliente)';
              ParamByName('xIDAnagrafica').asInteger := Data.TAnagraficaID.Value;
              ParamByName('xIDEvento').asInteger := xIDevento;
              ParamByName('xDataEvento').asDateTime := Date;
              ParamByName('xAnnotazioni').asString := 'cliente: ' + xClienteBlocco;
              ParamByName('xIDRicerca').asInteger := DataRicerche.TRicerchePendID.Value;
              ParamByName('xIDUtente').asInteger := MainForm.xIDUtenteAttuale;
              ParamByName('xIDCliente').asInteger := xIDClienteBlocco;
              ExecSQL;
            end;
          end;
        end;
      end;
    end;
    if xvai then begin
      if not DataRicerche.TRicAnagIDX.FindKey([ElencoRicPendForm.QRicAttiveID.Value, Data.TAnagraficaID.Value]) then
        Data.DB.BeginTrans;
      try
        Data.Q1.Close;
        Data.Q1.SQL.text := 'insert into EBC_CandidatiRicerche (IDRicerca,IDAnagrafica,Escluso,Stato,DataIns) ' +
          'values (:xIDRicerca,:xIDAnagrafica,:xEscluso,:xStato,:xDataIns)';
        Data.Q1.ParambyName('xIDRicerca').asInteger := ElencoRicPendForm.QRicAttiveID.Value;
        Data.Q1.ParambyName('xIDAnagrafica').asInteger := Data.TAnagraficaID.Value;
        Data.Q1.ParambyName('xEscluso').asBoolean := False;
        Data.Q1.ParambyName('xStato').asString := 'inserito direttam. in data ' + DateToStr(Date);
        Data.Q1.ParambyName('xDataIns').asDateTime := Date;
        Data.Q1.ExecSQL;
        // storico
        Data.Q1.SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
          'values (:xIDAnagrafica,:xIDEvento,:xDataEvento,:xAnnotazioni,:xIDRicerca,:xIDUtente,:xIDCliente)';
        Data.Q1.ParambyName('xIDAnagrafica').asInteger := Data.TAnagraficaID.Value;
        Data.Q1.ParambyName('xIDEvento').asInteger := 58;
        Data.Q1.ParambyName('xDataEvento').asDateTime := Date;
        Data.Q1.ParambyName('xAnnotazioni').asString := 'inserito nella ric.' + ElencoRicPendForm.QRicAttiveProgressivo.Value + ' (' + ElencoRicPendForm.QRicAttiveCliente.value + ')';
        Data.Q1.ParambyName('xIDRicerca').asInteger := ElencoRicPendForm.QRicAttiveID.Value;
        Data.Q1.ParambyName('xIDUtente').asInteger := xIDUtenteAttuale;
        Data.Q1.ParambyName('xIDCliente').asInteger := ElencoRicPendForm.QRicAttiveIDCliente.Value;
        Data.Q1.ExecSQL;
        // Anagrafica: cambio stato
        Data.Q1.SQL.text := 'update Anagrafica set IDTipoStato=1,IDStato=27 where ID=' + Data.TAnagraficaID.asString;
        Data.Q1.ExecSQL;
        Data.DB.CommitTrans;
      except
        Data.DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE:  operazione non completata', mtError, [mbOK], 0);
      end;
    end;
  end;
  DataRicerche.TRicAnagIDX.Close;
  Data2.QAnagRicerche.Close;
  Data2.QAnagRicerche.Open;
  ElencoRicPendForm.Free;
end;

procedure TMainForm.BNuovaRicercaClick(Sender: TObject);
var xAnno, xMese, xGiorno: Word;
  xIDRic, xIDAnagUtente, xProvvDef: integer;
  xCodRic: string;
  xProvv: boolean;
begin
  if not CheckProfile('20') then Exit;
  InsRicercaForm := TInsRicercaForm.create(self);
  InsRicercaForm.DEData.Date := Date;
  InsRicercaForm.SENumRic.Value := 1;
  InsRicercaForm.TUsers.FindKey([xIDUtenteAttuale]);
  InsRicercaForm.ShowModal;
  if InsRicercaForm.ModalResult = mrOK then begin
    // dati sulla provvigione
    Data.Qtemp.Close;
    Data.Qtemp.SQL.Text := 'select IDAnagrafica from Users where ID=' + InsRicercaForm.TUsersID.asString;
    Data.Qtemp.Open;
    if Data.Qtemp.FieldByName('IDAnagrafica').AsInteger > 0 then begin
      xIDAnagUtente := Data.Qtemp.FieldByName('IDAnagrafica').AsInteger;
      Data.Qtemp.Close;
      Data.Qtemp.SQL.Text := 'select * from AnagRetribuzHRMS where IDAnagrafica=' + IntToStr(xIDAnagUtente);
      Data.Qtemp.Open;
      if not Data.Qtemp.IsEmpty then begin
        xProvv := Data.Qtemp.FieldByName('Provvigione').asBoolean;
        xProvvDef := Data.Qtemp.FieldByName('ProvvigioneDefault').asInteger; ;
      end else begin
        xProvv := False;
        xProvvDef := 0;
      end;
    end;
    Data.Qtemp.Close;

    Data.DB.BeginTrans;
    try
      Data.Qtemp.Close;
      Data.Qtemp.SQL.Text := 'insert into EBC_Ricerche (IDCliente,IDUtente,DataInizio,' +
        'IDStatoRic,DallaData,IDMansione,IDArea,NumRicercati,Tipo,IDContattoCli,IDSede,' +
        'Provvigione,ProvvigionePerc,QuandoPagamUtente) ' +
        '  values (:xIDCliente,:xIDUtente,:xDataInizio,' +
        ':xIDStatoRic,:xDallaData,:xIDMansione,:xIDArea,:xNumRicercati,:xTipo,:xIDContattoCli,:xIDSede,' +
        ':xProvvigione,:xProvvigionePerc,:xQuandoPagamUtente)';
      Data.Qtemp.ParamByName('xIDCliente').asInteger := InsRicercaForm.xIDCliente;
      Data.Qtemp.ParamByName('xIDUtente').asInteger := InsRicercaForm.TUsersID.Value;
      Data.Qtemp.ParamByName('xDataInizio').asDateTime := InsRicercaForm.DEData.Date;
      Data.Qtemp.ParamByName('xIDStatoRic').asInteger := 1;
      Data.Qtemp.ParamByName('xDallaData').asDateTime := InsRicercaForm.DEData.Date;
      Data.Qtemp.ParamByName('xIDMansione').asInteger := InsRicercaForm.xIDRuolo;
      Data.Qtemp.ParamByName('xIDArea').asInteger := InsRicercaForm.xIDArea;
      Data.Qtemp.ParamByName('xNumRicercati').asInteger := InsRicercaForm.SENumRic.Value;
      Data.Qtemp.ParamByName('xTipo').asString := InsRicercaForm.CBTipo.Text;
      Data.Qtemp.ParamByName('xIDContattoCli').asInteger := InsRicercaForm.xIDRifInterno;
      Data.Qtemp.ParamByName('xIDSede').asInteger := InsRicercaForm.QSediID.Value;
      Data.Qtemp.ParamByName('xProvvigione').asBoolean := xProvv;
      Data.Qtemp.ParamByName('xProvvigionePerc').asInteger := xProvvDef;
      Data.Qtemp.ParamByName('xQuandoPagamUtente').asString := '1'; // a default TIPO=1
      Data.Qtemp.ExecSQL;

      Data.Qtemp.SQL.text := 'select @@IDENTITY as LastID';
      Data.Qtemp.Open;
      xIDRic := Data.Qtemp.FieldByName('LastID').asInteger;
      Data.Qtemp.Close;

      // progressivo dal nuovo contatore (non pi� dall'ID)
      xCodRic := GetCodRicerca;
      Data.Qtemp.Close;
      Data.Qtemp.SQL.text := 'update EBC_Ricerche set Progressivo=:xProg where ID=' + IntToStr(xIDRic);
      Data.Qtemp.ParamByName('xProg').asString := xCodRic;
      Data.Qtemp.ExecSQL;

      // se � impostata la data prevista di chiusura
      if InsRicercaForm.DEDataPrevChius.Text <> '' then begin
        Data.Qtemp.SQL.text := 'update EBC_Ricerche set DataPrevChiusura=:xDataPrevChiusura where ID=' + IntToStr(xIDRic);
        Data.Qtemp.ParamByName('xDataPrevChiusura').asDateTime := InsRicercaForm.DEDataPrevChius.Date;
        Data.Qtemp.ExecSQL;
      end;

      // se � impostato il ruolo: inserire la job description in Specifiche
      if InsRicercaForm.xIDRuolo > 0 then begin
        Data.Q1.Close;
        Data.Q1.SQL.text := 'select Mansioni from Mansioni where ID=' + IntToStr(InsRicercaForm.xIDRuolo);
        Data.Q1.Open;
        Data.Qtemp.SQL.text := 'update EBC_Ricerche set Specifiche=:xSpecifiche where ID=' + IntToStr(xIDRic);
        Data.Qtemp.ParamByName('xSpecifiche').asString := 'JOB DESCRIPTION:' + chr(13) + Data.Q1.FieldByName('Mansioni').asString;
        Data.Qtemp.ExecSQL;
      end;

      DecodeDate(Date, xAnno, xMese, xGiorno);

      // riga in storico ricerca
      Data.Q1.Close;
      Data.Q1.SQL.text := 'insert into EBC_StoricoRic (IDRicerca,IDStatoRic,DallaData,Note) ' +
        'values (:xIDRicerca,:xIDStatoRic,:xDallaData,:xNote)';
      Data.Q1.ParamByName('xIDRicerca').asInteger := xIDRic;
      Data.Q1.ParamByName('xIDStatoRic').asInteger := 1;
      Data.Q1.ParamByName('xDallaData').asDateTime := Date;
      Data.Q1.ParamByName('xNote').asString := 'inizio ricerca';
      Data.Q1.ExecSQL;

      // riga in ricerche-utenti con ruolo "capo commessa" (o comunque il primo)
      Data.Q1.Close;
      Data.Q1.SQL.text := 'insert into EBC_RicercheUtenti (IDRicerca,IDUtente,IDRuoloRic) ' +
        'values (:xIDRicerca,:xIDUtente,:xIDRuoloRic)';
      Data.Q1.ParamByName('xIDRicerca').asInteger := xIDRic;
      Data.Q1.ParamByName('xIDUtente').asInteger := InsRicercaForm.TUsersID.Value;
      Data.Q1.ParamByName('xIDRuoloRic').asInteger := 1;
      Data.Q1.ExecSQL;

      Data.DB.CommitTrans;
    except
      Data.DB.RollbackTrans;
      MessageDlg('ATTENZIONE: errore sul database - Inserimento non avvenuto', mtError, [mbOK], 0);
      raise;
      Exit;
    end;

    // visualizza ricerca
    CaricaApriRicPend(xIDRic);
    SelPersForm := TSelPersForm.create(self);
    SelPersForm.CBOpzioneCand.Checked := True;
    SelPersForm.EseguiQueryCandidati;
    SelPersForm.xFromOrgTL := False;
    SelPersForm.ShowModal;
    try SelPersForm.Free
    except
    end;
    DataRicerche.QRicAttive.Close;
    EseguiQRicAttive;
    DataRicerche.TRicerchePend.Close;
    if Data2.QRicClienti.Active then begin
      Data2.QRicClienti.Close;
      Data2.QRicClienti.Open;
    end;
    if Data2.QAnagRicerche.Active then begin
      Data2.QAnagRicerche.Close;
      Data2.QAnagRicerche.Open;
    end;
  end;
  InsRicercaForm.Free;
end;

procedure TMainForm.BRiprendiRicClick(Sender: TObject);
var SavePlace: TBookmark;
  xIDRic: integer;
begin
  if not CheckProfile('21') then Exit;
  CaricaApriRicPend(DataRicerche.QRicAttiveID.Value);
  // solo per ASA
  if Uppercase(copy(Data.GlobalNomeAzienda.Value, 1, 6)) = 'ADVANT' then begin
    ASAcommessaForm := TASAcommessaForm.create(self);
    ASAcommessaForm.ShowModal;
    ASAcommessaForm.Free;
    exit;
  end;

  // controllo selezionatore:  gli aggregati possono entrare come il titolare !
  Data.QTemp.Close;
  Data.QTemp.SQL.text := 'select IDUtente from EBC_RicercheUtenti where IDRicerca=' + DataRicerche.QRicAttiveID.AsString;
  Data.QTemp.Open;
  if not Data.QTemp.Locate('IDUtente', xIDUtenteAttuale, []) then begin
    if not CheckProfile('27', False) then begin
      MessageDlg('L''utente connesso non ha diritti di accesso a commesse di altri utenti' + chr(13) +
        'IMPOSSIBILE ACCEDERE ALLA COMMESSA', mtError, [mbOK], 0);
      Data.QTemp.Close;
      Exit;
    end else
      MessageDlg('ATTENZIONE: l''utente connesso � diverso dal titolare della commessa e dagli associati', mtWarning, [mbOK], 0);
  end;
  Data.QTemp.Close;
  SelPersForm := TSelPersForm.create(self);
  SelPersForm.CBOpzioneCand.Checked := True;
  SelPersForm.EseguiQueryCandidati;
  SelPersForm.xFromOrgTL := False;
  SelPersForm.ShowModal;
  if DataRicerche.DsRicerchePend.State = dsEdit then begin
    with DataRicerche.TRicerchePend do begin
      Data.DB.BeginTrans;
      try
        Post;
        ApplyUpdates;
        Data.DB.CommitTrans;
      except
        Data.DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
        raise;
      end;
      CommitUpdates;
    end;
    // aggiornamento griglia solo se ci sono state modifiche ai dati di ricerca
    xIDRic := DataRicerche.QRicAttiveID.Value;
    DataRicerche.QRicAttive.Close;
    EseguiQRicAttive;
    DataRicerche.QRicAttive.Locate('ID', xIDRic, []);
  end;

  try SelPersForm.Free
  except
  end;
  if Data2.QRicClienti.Active then begin
    Data2.QRicClienti.Close;
    Data2.QRicClienti.Open;
  end;
  if Data2.QAnagRicerche.Active then begin
    Data2.QAnagRicerche.Close;
    Data2.QAnagRicerche.Open;
  end;
  DataRicerche.TRicerchePend.Close;
end;

procedure TMainForm.BSpecRicClick(Sender: TObject);
begin
  if not CheckProfile('22') then Exit;
  CaricaApriRicPend(DataRicerche.QRicAttiveID.Value);
  SpecificheForm := TSpecificheForm.create(self);
  SpecificheForm.ShowModal;
  SpecificheForm.Free;
  DataRicerche.TRicerchePend.Close;
end;

procedure TMainForm.ToolbarButton9719Click(Sender: TObject);
var xTabFiglie: string;
begin
  if not CheckProfile('24') then Exit;
  CaricaApriRicPend(DataRicerche.QRicAttiveID.Value);
  if MessageDlg('Sei sicuro di voler eliminare definitivamente la Commessa selezionata ?', mtWarning, [mbNo, mbYes], 0) = mrYes then begin

    xTabFiglie := '';
    Data.Q1.Close;
    Data.Q1.SQL.text := 'select count(*) TOT from EBC_CandidatiRicerche where IDRicerca=' + DataRicerche.TRicerchePendID.asString;
    Data.Q1.Open;
    if Data.Q1.fieldByName('TOT').asInteger > 0 then xTabFiglie := xTabFiglie + '- Candidati associati alla commessa' + chr(13);
    Data.Q1.Close;
    Data.Q1.SQL.text := 'select count(*) TOT from EBC_CompensiRicerche where IDRicerca=' + DataRicerche.TRicerchePendID.asString;
    Data.Q1.Open;
    if Data.Q1.fieldByName('TOT').asInteger > 0 then xTabFiglie := xTabFiglie + '- Compensi' + chr(13);

    if xTabFiglie <> '' then begin
      if MessageDlg('ATTENZIONE:  risultano collegati i seguenti dati:' + chr(13) +
        xTabFiglie + 'SEI SICURO DI VOLER PROSEGUIRE ?', mtWarning, [mbYes, mbNo], 0) = mrNo then
        exit;
    end;

    if MessageDlg('ATTENZIONE: ' + chr(13) +
      'ELIMINARE DEFINITIVAMENTE LA RICERCA E TUTTI I DATI CORRELATI ?' + chr(13) + chr(13) +
      'TUTTI I DATI VERRANNO CANCELLATI SENZA POTERLI RECUPERARE !!', mtWarning, [mbNo, mbYes], 0) = mrYes then begin
      with Data do begin
        DB.BeginTrans;
        try
          // eliminare tabelle collegate da CONSTRAINT di integrit� referenziale
          Q1.Close;
          Q1.SQL.text := 'delete from EBC_CandidatiRicerche where IDRicerca=' + DataRicerche.TRicerchePendID.asString;
          Q1.ExecSQL;
          Q1.Close;
          Q1.SQL.text := 'delete from EBC_ContattiCandidati where IDRicerca=' + DataRicerche.TRicerchePendID.asString;
          Q1.ExecSQL;
          Q1.Close;
          Q1.SQL.text := 'delete from EBC_CompensiRicerche where IDRicerca=' + DataRicerche.TRicerchePendID.asString;
          Q1.ExecSQL;

          Q1.SQL.text := 'delete from EBC_Ricerche where ID=' + DataRicerche.TRicerchePendID.asString;
          Q1.ExecSQL;
          DB.CommitTrans;
        except
          DB.RollbackTrans;
          MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
          raise;
        end;
      end;
      DataRicerche.QRicAttive.Close;
      EseguiQRicAttive;
    end;
  end;
  DataRicerche.TRicerchePend.Close;
  if Data2.QRicClienti.Active then begin
    Data2.QRicClienti.Close;
    Data2.QRicClienti.Open;
  end;
  if Data2.QAnagRicerche.Active then begin
    Data2.QAnagRicerche.Close;
    Data2.QAnagRicerche.Open;
  end;
end;

procedure TMainForm.ToolbarButton977Click(Sender: TObject);
begin
  if not CheckProfile('011') then Exit;
  if Data2.QAnagRicerche.RecordCount > 0 then begin
    CaricaApriRicPend(Data2.QAnagRicercheIDRicerca.Value);
    // controllo selezionatore:  gli aggregati possono entrare come il titolare !
    if (DataRicerche.TRicerchePendIDUtente.Value <> xIDUtenteAttuale) and
      (DataRicerche.TRicerchePendIDUtente2.Value <> xIDUtenteAttuale) and
      (DataRicerche.TRicerchePendIDUtente3.Value <> xIDUtenteAttuale) then begin
      if not CheckProfile('27', False) then begin
        MessageDlg('L''utente connesso non ha diritti di accesso a commesse di altri utenti' + chr(13) +
          'IMPOSSIBILE ACCEDERE ALLA COMMESSA', mtError, [mbOK], 0);
        Exit;
      end else
        MessageDlg('ATTENZIONE: l''utente connesso � diverso dal titolare della commessa e dagli associati', mtWarning, [mbOK], 0);
    end;
    SelPersForm := TSelPersForm.create(self);
    SelPersForm.CBOpzioneCand.Checked := True;
    SelPersForm.EseguiQueryCandidati;
    SelPersForm.xFromOrgTL := False;
    SelPersForm.ShowModal;
    try SelPersForm.Free
    except
    end;
    DataRicerche.QRicAttive.Close;
    EseguiQRicAttive;
    Data2.QAnagRicerche.Close;
    Data2.QAnagRicerche.Open;
    DataRicerche.TRicerchePend.Close;
  end;
end;

procedure TMainForm.BRicercheNewClick(Sender: TObject);
var xAnno, xMese, xGiorno: Word;
begin
  if not CheckProfile('120') then Exit;
  ShowMessage('Utilizzare il pulsante nella gestione delle ricerche');
end;

procedure TMainForm.LOBAnnunciClick(Sender: TObject);
begin
  // DISABILITAZIONE PROPOSTE Torino -- ALIAS='PROPOS'
  // DISABILITAZIONE MISTRAL Milano -- ALIAS='MISTRAL'
  // DISABILITAZIONE VALTEC -- ALIAS='VALTEC'
  // DISABILITAZIONE ERREMME -- ALIAS='ERREMME'
  if (Uppercase(copy(Data.GlobalNomeAzienda.Value, 1, 6)) = 'PROPOS') or
    (Uppercase(copy(Data.GlobalNomeAzienda.Value, 1, 7)) = 'MISTRAL') or
    (Uppercase(copy(Data.GlobalNomeAzienda.Value, 1, 6)) = 'VALTEC') or
    (Uppercase(copy(Data.GlobalNomeAzienda.Value, 1, 7)) = 'ERREMME') then begin
    ShowMessage('FUNZIONE NON ABILITATA');
    Exit;
  end;
  if not CheckProfile('3') then Exit;
  PageControl1.ActivePage := TSAnnunci;
  LOBSchede.Down := False;
  LOBRicerche.Down := False;
  LOBClientiEBC.Down := False;
  LOBAnnunci.Down := True;
  LOBAziende.Down := False;
  TbAnag.Visible := False;
  TbClienti.Visible := False;
  //DisabilitaTabAziende;
  // apertura tabelle
  if not DataAnnunci.TAnnunci.active then begin
    DataAnnunci.TAnnunci.open;
    DataAnnunci.TAnnunciRic.open;
  end;
  Caption := '[M/3] - ' + xCaption;
end;

procedure TMainForm.TbBAnnNewClick(Sender: TObject);
begin
  if not CheckProfile('30') then Exit;
  InsAnnuncioForm := TInsAnnuncioForm.create(self);
  while InsAnnuncioForm.TUsersID.Value <> xIDUtenteAttuale do InsAnnuncioForm.TUsers.Next;
  InsAnnuncioForm.ShowModal;
  if InsAnnuncioForm.ModalResult = mrOK then begin
    with Data do begin
      if not DB.InTransaction then DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'insert into Ann_Annunci (Rif,Data,IDCliente,IDUtente,Archiviato,Civetta) ' +
          'values (:xRif,:xData,:xIDCliente,:xIDUtente,:xArchiviato,:xCivetta)';
        Q1.ParamByName('xRif').asString := InsAnnuncioForm.ERif.text;
        Q1.ParamByName('xData').AsDateTime := InsAnnuncioForm.DEData.date;
        Q1.ParamByName('xIDCliente').asInteger := InsAnnuncioForm.xIDCliente;
        Q1.ParamByName('xIDUtente').asInteger := InsAnnuncioForm.TUsersID.Value;
        Q1.ParamByName('xArchiviato').AsBoolean := False;
        Q1.ParamByName('xCivetta').AsBoolean := False;
        Q1.ExecSQL;
        DB.CommitTrans;
        ERifAnnuncio.text := InsAnnuncioForm.ERif.text;
        DataAnnunci.Tannunci.Close;
        DataAnnunci.Tannunci.Open;
        BRifAnnuncioClick(self);
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
        raise;
      end;
    end;
  end;
  InsAnnuncioForm.Free;
end;

procedure TMainForm.TbBAnnDelClick(Sender: TObject);
begin
  if not CheckProfile('32') then Exit;
  if DataAnnunci.TAnnunci.RecordCount > 0 then
    if MessageDlg('Sei sicuro di voler cancellare l''annuncio selezionato ?', mtWarning,
      [mbNo, mbYes], 0) = mrYes then begin
      with Data do begin
        DB.BeginTrans;
        try
          Q1.Close;
          Q1.SQL.text := 'delete from Ann_Annunci ' +
            'where ID=' + DataAnnunci.TAnnunciID.asString;
          Q1.ExecSQL;
          DB.CommitTrans;
          DataAnnunci.TAnnunci.Close;
          DataAnnunci.TAnnunci.Open;
        except
          DB.RollbackTrans;
          MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
        end;
      end;
    end;
end;

procedure TMainForm.TbBAnnOKClick(Sender: TObject);
begin
  if not CheckProfile('31') then Exit;
  DataAnnunci.TAnnunci.Post;
end;

procedure TMainForm.TbBAnnCanClick(Sender: TObject);
begin
  DataAnnunci.TAnnunci.Cancel;
end;

procedure TMainForm.LOBTabAnnClick(Sender: TObject);
begin
  // PAGINE PER ORA DISABILITATE
  PCInfoEdizione.Pages[1].TabVisible := False;
  PCInfoEdizione.Pages[2].TabVisible := False;
  PCInfoEdizione.Pages[3].TabVisible := False;

  // DISABILITAZIONE PROPOSTE Torino -- ALIAS='PROPOS'
  // DISABILITAZIONE MISTRAL Milano -- ALIAS='MISTRAL'
  if (Uppercase(copy(Data.GlobalNomeAzienda.Value, 1, 6)) = 'PROPOS') or
    (Uppercase(copy(Data.GlobalNomeAzienda.Value, 1, 7)) = 'MISTRAL') then begin
    ShowMessage('FUNZIONE NON ABILITATA');
    Exit;
  end;

  if not CheckProfile('76') then Exit;
  PageControl1.ActivePage := TSTabAnn;
  //DataAnnunci.TEdizioniLK.Open;
  //DataAnnunci.TTestateLK.Open;
  DataAnnunci.QTestate.Open;
  DataAnnunci.QEdizioniTest.Open;
  //DataAnnunci.TSconti.Open;
  //DataAnnunci.TPenetraz.Open;
  //DataAnnunci.TContratti.Open;
  //DataAnnunci.TQualifiche.Open;
  //DataAnnunci.TGiorniIndicati.Open;

  LOBTab1.Down := False;
  LOBTab2.Down := False;
  LOBTab3.Down := False;
  LOBTab4.Down := False;
  LOBTab7.Down := False;
  LOBTab8.Down := False;
  LOBTabAnn.Down := True;
  LOBTabAttiv.Down := False;
  LOBTabAziende.Down := False;
  LOBTab11.Down := False;
  LOBTabComuni.Down := False;
  LOBTabNazioni.Down := False;
  LOBTabModelliWord.Down := False;
  Caption := '[M/76] - ' + xCaption;
end;

procedure TMainForm.TbBTestNewClick(Sender: TObject);
begin
  TestataForm := TTestataForm.create(self);
  TestataForm.ShowModal;
  if TestataForm.ModalResult = mrOK then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'insert into Ann_Testate (Denominazione,Tipologia,Note) ' +
          'values (:xDenominazione,:xTipologia,:xNote)';
        Q1.ParamByName('xDenominazione').asString := TestataForm.EDenom.Text;
        Q1.ParamByName('xTipologia').asString := TestataForm.ETipo.Text;
        Q1.ParamByName('xNote').asString := TestataForm.ENote.Text;
        Q1.ExecSQL;
        DB.CommitTrans;
        DataAnnunci.QTestate.Close;
        DataAnnunci.QTestate.Open;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;

  end;
  TestataForm.Free;
end;

procedure TMainForm.TbBTestDelClick(Sender: TObject);
begin
  if DataAnnunci.QEdizioniTest.RecordCount > 0 then begin
    MessageDlg('Ci sono edizioni associate - Impossibile cancellare', mtError, [mbOK], 0);
    exit;
  end;
  if MessageDlg('Sei sicuro di voler cancellare ?', mtWarning, [mbNo, mbYes], 0) = mrYes then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'delete from Ann_Testate ' +
          'where ID=' + Dataannunci.QTestateID.asString;
        Q1.ExecSQL;
        DB.CommitTrans;
        DataAnnunci.QTestate.Close;
        DataAnnunci.QTestate.Open;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
  end;
end;

procedure TMainForm.TbBTestOKClick(Sender: TObject);
begin
  TestataForm := TTestataForm.create(self);
  TestataForm.EDenom.Text := DataAnnunci.QTestateDenominazione.Value;
  TestataForm.ETipo.Text := DataAnnunci.QTestateTipologia.Value;
  TestataForm.ENote.Text := DataAnnunci.QTestateNote.Value;
  TestataForm.ShowModal;
  if TestataForm.ModalResult = mrOK then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'update Ann_Testate set Denominazione=:xDenominazione,Tipologia=:xTipologia,Note=:xNote ' +
          'where ID=' + Dataannunci.QTestateID.asString;
        Q1.ParamByName('xDenominazione').asString := TestataForm.EDenom.Text;
        Q1.ParamByName('xTipologia').asString := TestataForm.ETipo.Text;
        Q1.ParamByName('xNote').asString := TestataForm.ENote.Text;
        Q1.ExecSQL;
        DB.CommitTrans;
        DataAnnunci.QTestate.Close;
        DataAnnunci.QTestate.Open;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;

  end;
  TestataForm.Free;
end;

procedure TMainForm.TbBEdizTestNewClick(Sender: TObject);
var xS: string;
begin
  EdizioneForm := TEdizioneForm.create(self);
  EdizioneForm.ShowModal;
  if EdizioneForm.ModalResult = mrOK then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'insert into Ann_Edizioni (IDTestata,NomeEdizione,IDCliente,Note) ' +
          'values (:xIDTestata,:xNomeEdizione,:xIDCliente,:xNote)';
        Q1.ParamByName('xIDTestata').asInteger := DataAnnunci.QTestateID.Value;
        Q1.ParamByName('xNomeEdizione').asString := EdizioneForm.ENomeEdizione.Text;
        Q1.ParamByName('xIDCliente').asInteger := EdizioneForm.QConcessID.Value;
        Q1.ParamByName('xNote').asString := EdizioneForm.ENote.text;
        Q1.ExecSQL;
        DB.CommitTrans;
        DataAnnunci.QEdizioniTest.Close;
        DataAnnunci.QEdizioniTest.Open;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;

  end;
  EdizioneForm.Free;
end;

procedure TMainForm.TbBEdizTestDelClick(Sender: TObject);
begin
  Data.Q1.Close;
  Data.Q1.SQL.text := 'select count(*) Tot from Ann_AnnEdizData where IDEdizione=' + DataAnnunci.QEdizioniTestID.asString;
  Data.Q1.Open;
  if Data.Q1.FieldByName('Tot').asInteger > 0 then begin
    MessageDlg('Ci sono pubblicazioni collegate a questa testata - IMPOSSIBILE CANCELLARE', mtError, [mbOK], 0);
    Data.Q1.Close;
    exit;
  end;
  Data.Q1.Close;
  if MessageDlg('Sei sicuro di voler cancellare ?', mtWarning, [mbNo, mbYes], 0) = mrNo then exit;
  with Data do begin
    DB.BeginTrans;
    try
      Q1.Close;
      Q1.SQL.text := 'delete from Ann_Edizioni ' +
        'where ID=' + DataAnnunci.QEdizioniTestID.asString;
      Q1.ExecSQL;
      DB.CommitTrans;
      DataAnnunci.QEdizioniTest.Close;
      DataAnnunci.QEdizioniTest.Open;
    except
      DB.RollbackTrans;
      MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
    end;
  end;
end;

procedure TMainForm.TbBScontiNewClick(Sender: TObject);
begin
  DataAnnunci.TSconti.Insert;
end;

procedure TMainForm.TbBPenetrazNewClick(Sender: TObject);
begin
  DataAnnunci.TPenetraz.Insert;
end;

procedure TMainForm.TbBScontiDelClick(Sender: TObject);
begin
  DataAnnunci.TSconti.Delete;
end;

procedure TMainForm.TbBScontiOKClick(Sender: TObject);
begin
  DataAnnunci.TSconti.Post;
end;

procedure TMainForm.TbBScontiCanClick(Sender: TObject);
begin
  DataAnnunci.TSconti.Cancel;
end;

procedure TMainForm.TbBGiorniNewClick(Sender: TObject);
begin
  DataAnnunci.TGiorniIndicati.Insert;
end;

procedure TMainForm.TbBGiorniDelClick(Sender: TObject);
begin
  DataAnnunci.TGiorniIndicati.Delete;
end;

procedure TMainForm.TbBGiorniOKClick(Sender: TObject);
begin
  DataAnnunci.TGiorniIndicati.Post;
end;

procedure TMainForm.TbBGiorniCanClick(Sender: TObject);
begin
  DataAnnunci.TGiorniIndicati.Cancel;
end;

procedure TMainForm.TbBPenetrazDelClick(Sender: TObject);
begin
  DataAnnunci.TPenetraz.Delete;
end;

procedure TMainForm.TbBPenetrazOKClick(Sender: TObject);
begin
  DataAnnunci.TPenetraz.Post;
end;

procedure TMainForm.TbBPenetrazCanClick(Sender: TObject);
begin
  DataAnnunci.TPenetraz.Cancel;
end;

procedure TMainForm.TbBPubbNewClick(Sender: TObject);
var xIDAnnEdizData: integer;
begin
  if not CheckProfile('350') then Exit;
  SelEdizForm := TSelEdizForm.create(self);
  SelEdizForm.xIDCliente := DataAnnunci.TAnnunciIDCliente.Value;
  SelEdizForm.xCliente := DataAnnunci.TAnnunciCliente.Value;
  SelEdizForm.DEData.Date := Date;
  // Listino riservato al cliente
  SelEdizForm.ListinoAnnunciFrame1.QListinoEdiz.ParamByName('xIDCliente').asInteger := DataAnnunci.TAnnunciIDCliente.Value;
  SelEdizForm.ListinoAnnunciFrame1.LCliente.Caption := 'Cliente: ' + DataAnnunci.TAnnunciCliente.Value;

  SelEdizForm.ShowModal;
  if SelEdizForm.ModalResult = mrOK then begin
    Data.DB.BeginTrans;
    try
      Data.Q1.Close;
      Data.Q1.SQL.text := 'insert into Ann_AnnEdizData (IDAnnuncio,IDEdizione,Data,NumModuli,' +
        '       CostoLire,CostoEuro,CostoANoiLire,CostoANoiEuro,' +
        '       ScontoAlClienteLire,ScontoAlClienteEuro,ScontoANoiLire,ScontoANoiEuro,ListinoLire,ListinoEuro) ' +
        'values (:xIDAnnuncio,:xIDEdizione,:xData,:xNumModuli,:xCostoLire,:xCostoEuro,:xCostoANoiLire,:xCostoANoiEuro,' +
        ':xScontoAlClienteLire,:xScontoAlClienteEuro,:xScontoANoiLire,:xScontoANoiEuro,:xListinoLire,:xListinoEuro)';
      Data.Q1.ParambyName('xIDAnnuncio').asInteger := DataAnnunci.TAnnunciID.Value;
      if SelEdizForm.PCListini.ActivePageIndex = 0 then
        Data.Q1.ParambyName('xIDEdizione').asInteger := SelEdizForm.ListinoAnnunciFrame1.QEdizioniIDEdizione.value
      else Data.Q1.ParambyName('xIDEdizione').asInteger := SelEdizForm.ContrattiAnnunciFrame1.QAnnContrattiIDEdizione.value;
      Data.Q1.ParambyName('xData').asDateTime := SelEdizForm.DEData.Date;
      Data.Q1.ParambyName('xNumModuli').asInteger := Round(SelEdizForm.SENumModuli.value);
      Data.Q1.ParambyName('xCostoLire').asFloat := SelEdizForm.AlClienteLire.Value;
      Data.Q1.ParambyName('xCostoEuro').asFloat := SelEdizForm.AlClienteEuro.Value;
      Data.Q1.ParambyName('xCostoANoiLire').asFloat := SelEdizForm.AnoiLire.Value;
      Data.Q1.ParambyName('xCostoANoiEuro').asFloat := SelEdizForm.AnoiEuro.Value;
      Data.Q1.ParambyName('xScontoAlClienteLire').asFloat := SelEdizForm.ScontoAlClienteLire.Value;
      Data.Q1.ParambyName('xScontoAlClienteEuro').asFloat := SelEdizForm.ScontoAlClienteEuro.Value;
      Data.Q1.ParambyName('xScontoANoiLire').asFloat := SelEdizForm.ScontoAnoiLire.Value;
      Data.Q1.ParambyName('xScontoANoiEuro').asFloat := SelEdizForm.ScontoAnoiEuro.Value;
      Data.Q1.ParambyName('xListinoLire').asFloat := SelEdizForm.ListinoLire.Value;
      Data.Q1.ParambyName('xListinoEuro').asFloat := SelEdizForm.ListinoEuro.Value;
      Data.Q1.ExecSQL;
      Data.QTemp.SQL.text := 'select @@IDENTITY as LastID';
      Data.QTemp.Open;
      xIDAnnEdizData := Data.QTemp.FieldByName('LastID').asInteger;
      Data.QTemp.Close;

      // se esiste un carnet di moduli per quella edizione --> CONTRATTI
      if SelEdizForm.PCListini.ActivePageIndex = 1 then begin
        // incrementa numero moduli utilizzati
        Data.Q1.Close;
        Data.Q1.SQL.text := 'update Ann_Contratti set ModuliUtilizzati=:xTot ' +
          'where ID=' + SelEdizForm.ContrattiAnnunciFrame1.QAnnContrattiID.AsString;
        Data.Q1.ParambyName('xTot').asInteger := SelEdizForm.ContrattiAnnunciFrame1.QAnnContrattiModuliUtilizzati.Value + SelEdizForm.SENumModuli.AsInteger;
        Data.Q1.ExecSQL;
      end;
      // codice pubblicazione
      Data.Q1.Close;
      Data.Q1.SQL.text := 'update Ann_AnnEdizData set Codice=:xCodice ' +
        'where ID=' + intToStr(xIDAnnEdizData);
      Data.Q1.ParambyName('xCodice').asString := intToStr(xIDAnnEdizData);
      Data.Q1.ExecSQL;

      // aggiornamento CONTROLLO DI GESTIONE (se esiste il file...)
      if copy(Data.GlobalCheckBoxIndici.Value, 6, 1) = '1' then begin
        // crea la voce di costo
        Data2.QTemp.SQL.Text := 'insert into CG_ContrattiCosti (IDContratto,Tipo,ImportoPrev,Note,IDAnnEdizData) ' +
          '  values (:xIDContratto,:xTipo,:xImportoPrev,:xNote,:xIDAnnEdizData)';
        Data2.QTemp.ParamByName('xIDContratto').asInteger := DataAnnunci.TAnnunciIDContrattoCG.Value;
        Data2.QTemp.ParamByName('xTipo').asString := 'Annuncio ' + DataAnnunci.TAnnunciRif.AsString + ' (' + intToStr(xIDAnnEdizData) + ')';
        Data2.QTemp.ParamByName('xImportoPrev').asFloat := SelEdizForm.AnoiEuro.Value;
        Data2.QTemp.ParamByName('xNote').asString := '';
        Data2.QTemp.ParamByName('xIDAnnEdizData').asInteger := xIDAnnEdizData;
        Data2.QTemp.execSQL;
        // crea la voce di ricavo
        Data2.QTemp.SQL.Text := 'insert into CG_ContrattiRicavi (IDContratto,Tipo,ImportoPrev,Note,IDAnnEdizData) ' +
          '  values (:xIDContratto,:xTipo,:xImportoPrev,:xNote,:xIDAnnEdizData)';
        Data2.QTemp.ParamByName('xIDContratto').asInteger := DataAnnunci.TAnnunciIDContrattoCG.Value;
        Data2.QTemp.ParamByName('xTipo').asString := 'Annuncio ' + DataAnnunci.TAnnunciRif.AsString + ' (' + intToStr(xIDAnnEdizData) + ')';
        Data2.QTemp.ParamByName('xImportoPrev').asFloat := SelEdizForm.AlClienteEuro.Value;
        Data2.QTemp.ParamByName('xNote').asString := '';
        Data2.QTemp.ParamByName('xIDAnnEdizData').asInteger := xIDAnnEdizData;
        Data2.QTemp.execSQL;
        Data.Q1.Close;
      end;

      Data.DB.CommitTrans;

      AggTotAnnuncio(DataAnnunci.TAnnunciID.Value);
      DataAnnunci.QAnnEdizDataXann.Close;
      DataAnnunci.QAnnEdizDataXann.Open;
      if not DataAnnunci.QAnnAnagData.active then
        DataAnnunci.QAnnAnagData.Open;
    except
      Data.DB.RollbackTrans;
      MessageDlg('ERRORE SUL DATABASE:  operazione non completata', mtError, [mbOK], 0);
    end;
  end;
  SelEdizForm.Free;
end;

procedure TMainForm.TbBPubbDelClick(Sender: TObject);
begin
  if not CheckProfile('351') then Exit;
  if DataAnnunci.QAnnEdizDataXann.RecordCount > 0 then
    if MessageDlg('Sei sicuro di voler cancellare ?', mtWarning, [mbNo, mbYes], 0) = mrYes then begin
      Data.DB.BeginTrans;
      try
        Data.Q1.Close;
        Data.Q1.SQL.text := 'delete from Ann_AnnEdizData where ID=' + DataAnnunci.QAnnEdizDataXannID.asString;
        Data.Q1.ExecSQL;

        // aggiornamento CONTROLLO DI GESTIONE (se esiste il file...)
        if copy(Data.GlobalCheckBoxIndici.Value, 6, 1) = '1' then begin
          // crea la voce di costo
          Data2.QTemp.SQL.Text := 'delete from CG_ContrattiCosti ' +
            ' where IDAnnEdizData=:xIDAnnEdizData';
          Data2.QTemp.ParamByName('xIDAnnEdizData').asInteger := DataAnnunci.QAnnEdizDataXAnnID.Value;
          Data2.QTemp.execSQL;
          // crea la voce di ricavo
          Data2.QTemp.SQL.Text := 'delete from CG_ContrattiRicavi ' +
            ' where IDAnnEdizData=:xIDAnnEdizData';
          Data2.QTemp.ParamByName('xIDAnnEdizData').asInteger := DataAnnunci.QAnnEdizDataXAnnID.Value;
          Data2.QTemp.execSQL;
          Data.Q1.Close;
        end;

        Data.DB.CommitTrans;
        DataAnnunci.QAnnEdizDataXann.Close;
        DataAnnunci.QAnnEdizDataXann.Open;
        AggTotAnnuncio(DataAnnunci.TAnnunciID.Value);
        if DataAnnunci.QAnnEdizDataXann.RecordCount = 0 then
          DataAnnunci.QAnnAnagData.Close;
      except
        Data.DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE:  operazione non completata', mtError, [mbOK], 0);
      end;
    end;
end;

procedure TMainForm.LOBTabAttivClick(Sender: TObject);
begin
  if not CheckProfile('77') then Exit;
  Data2.QAttivita.Open;
  Data2.QAreeAttivita.Open;
  PageControl1.ActivePage := TSTabelle;
  PageControl3.ActivePage := TSTabAttiv;
  LOBTab1.Down := False;
  LOBTab2.Down := False;
  LOBTab3.Down := False;
  LOBTab4.Down := False;
  LOBTab7.Down := False;
  LOBTab8.Down := False;
  LOBTabAnn.Down := False;
  LOBTabAttiv.Down := True;
  LOBTabAziende.Down := False;
  LOBTab11.Down := False;
  LOBTabComuni.Down := False;
  LOBTabNazioni.Down := False;
  LOBTabModelliWord.Down := False;
  Caption := '[M/77] - ' + xCaption;
end;

procedure TMainForm.TbBAttivitaNewClick(Sender: TObject);
var xDesc: string;
begin
  xDesc := '';
  if not InputQuery('Nuovo settore', 'nome settore:', xDesc) then exit;
  with Data do begin
    DB.BeginTrans;
    try
      Q1.Close;
      Q1.SQL.text := 'insert into EBC_Attivita (Attivita) ' +
        'values (:xAttivita)';
      Q1.ParamByName('xAttivita').asString := xDesc;
      Q1.ExecSQL;
      DB.CommitTrans;
    except
      DB.RollbackTrans;
      MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
    end;
  end;
  Data2.QAttivita.Close;
  Data2.QAttivita.Open;
end;

procedure TMainForm.TbBAttivitaDelClick(Sender: TObject);
begin
  // verifica integrit� referenziale
  if not VerifIntegrRef('EBC_Clienti', 'IDAttivita', Data2.QAttivitaID.AsString) then exit;
  if not VerifIntegrRef('EsperienzeLavorative', 'IDSettore', Data2.QAttivitaID.AsString) then exit;
  if MessageDlg('Sei sicuro di voler eliminare l''attivit� ?', mtWarning,
    [mbNo, mbYes], 0) = mrYes then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'delete from EBC_Attivita ' +
          'where ID=' + Data2.QAttivitaID.AsString;
        Q1.ExecSQL;
        DB.CommitTrans;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
    Data2.QAttivita.Close;
    Data2.QAttivita.Open;
  end;
end;

procedure TMainForm.PageControl4Change(Sender: TObject);
begin
  {     if Pagecontrol4.ActivePage=TSCliTutti then
            Data2.TEBCclienti.Filtered:=False
       else Data2.TEBCclienti.Filtered:=true;
       if Pagecontrol4.ActivePage=TSCliAttivi then
            Data2.TEBCclienti.Filter:='Stato=''attivo''';
       if Pagecontrol4.ActivePage=TSCliNonAttivi then
            Data2.TEBCclienti.Filter:='Stato=''non attivo''';
       if Pagecontrol4.ActivePage=TSCliEliminati then
            Data2.TEBCclienti.Filter:='Stato=''Eliminato''';
       if Pagecontrol4.ActivePage=TSCliContattati then
            Data2.TEBCclienti.Filter:='Stato=''contattato''';
  }
  BCliSearchClick(self);
  LTotAz.Caption := IntToStr(Data2.TEBCclienti.RecordCount);
end;

procedure TMainForm.LOBSelPersClick(Sender: TObject);
begin
  if not CheckProfile('5') then Exit;
  SelPersNewForm := TSelPersNewForm.create(self);
  // riempimento combobox1 con le tabelle
  SelPersNewForm.QTabelle.Close;
  SelPersNewForm.QTabelle.Open;
  SelPersNewForm.QTabelle.First;
  SelPersNewForm.ComboBox1.Items.Clear;
  while not SelPersNewForm.QTabelle.EOF do begin
    SelPersNewForm.ComboBox1.Items.Add(SelPersNewForm.QTabelleDescTabella.asString);
    SelPersNewForm.QTabelle.Next;
  end;
  SelPersNewForm.QRes1.Close;
  // rendere invisibili gli oggetti attinenti alla ricerca
  SelPersNewForm.BAggiungiRic.Visible := False;
  SelPersNewForm.xChiamante := 0;

  SelPersNewForm.ShowModal;
  SelPersNewForm.Free;
end;

procedure TMainForm.BAnagFileWordClick(Sender: TObject);
begin
  if not CheckProfile('054') then Exit;
  ApriFileWord(Data.TAnagraficaCVNumero.asString);
end;

procedure TMainForm.LOBTabAziendeClick(Sender: TObject);
begin
  if not CheckProfile('78') then Exit;
  PageControl1.ActivePage := TSTabelle;
  PageControl3.ActivePage := TSTabAziende;
  LOBTab1.Down := False;
  LOBTab2.Down := False;
  LOBTab3.Down := False;
  LOBTab4.Down := False;
  LOBTab7.Down := False;
  LOBTab8.Down := False;
  LOBTabAnn.Down := False;
  LOBTabAttiv.Down := False;
  LOBTabAziende.Down := True;
  LOBTab11.Down := False;
  LOBTabComuni.Down := False;
  LOBTabNazioni.Down := False;
  LOBTabModelliWord.Down := False;
end;

procedure TMainForm.ToolbarButton9713Click(Sender: TObject);
begin
  if not CheckProfile('190') then Exit;
  ContattoCliFattoForm := TContattoCliFattoForm.create(self);
  ContattoCliFattoForm.DEData.Date := Date;
  ContattoCliFattoForm.MEOre.Text := TimeToStr(Now);
  ContattoCliFattoForm.xIDContattoCli := 0;
  ContattoCliFattoForm.xIDOfferta := 0;
  ContattoCliFattoForm.Showmodal;
  if ContattoCliFattoForm.ModalResult = mrOK then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'insert into EBC_ContattiFatti (IDCliente,TipoContatto,Data,Ore,ParlatoCon,Descrizione,IDOfferta,IDContattoCli) ' +
          'values (:xIDCliente,:xTipoContatto,:xData,:xOre,:xParlatoCon,:xDescrizione,:xIDOfferta,:xIDContattoCli)';
        Q1.ParamByName('xIDCliente').asInteger := Data2.TEBCClientiID.Value;
        Q1.ParamByName('xTipoContatto').asString := ContattoCliFattoForm.CBTipoContatto.Text;
        Q1.ParamByName('xData').asDateTime := ContattoCliFattoForm.DEData.Date;
        Q1.ParamByName('xOre').asDateTime := StrToDateTime(DatetoStr(ContattoCliFattoForm.DEData.Date) + ' ' + ContattoCliFattoForm.MEOre.Text);
        Q1.ParamByName('xParlatoCon').asString := ContattoCliFattoForm.EParlatoCon.Text;
        Q1.ParamByName('xDescrizione').asString := ContattoCliFattoForm.EDescrizione.Text;
        Q1.ParamByName('xIDOfferta').asInteger := ContattoCliFattoForm.xIDOfferta;
        Q1.ParamByName('xIDContattoCli').asInteger := ContattoCliFattoForm.xIDContattoCli;
        Q1.ExecSQL;
        DB.CommitTrans;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
    Data2.TCliContattiFatti.Close;
    Data2.TCliContattiFatti.Open;
  end;
  ContattoCliFattoForm.Free;
end;

procedure TMainForm.ToolbarButton9717Click(Sender: TObject);
begin
  if not CheckProfile('191') then Exit;
  ContattoCliFattoForm := TContattoCliFattoForm.create(self);
  ContattoCliFattoForm.CBTipoContatto.Text := Data2.TCliContattiFattiTipoContatto.Value;
  ContattoCliFattoForm.DEData.Date := Data2.TCliContattiFattiData.Value;
  ContattoCliFattoForm.MEOre.Text := Data2.TCliContattiFattiOre.asString;
  ContattoCliFattoForm.EParlatoCon.Text := Data2.TCliContattiFattiParlatoCon.Value;
  ContattoCliFattoForm.EDescrizione.Text := Data2.TCliContattiFattiDescrizione.Value;
  ContattoCliFattoForm.xIDContattoCli := Data2.TCliContattiFattiIDContattoCli.Value;
  ContattoCliFattoForm.xIDOfferta := Data2.TCliContattiFattiIDOfferta.value;
  if (Data2.TCliContattiFattiIDOfferta.asString = '') and (Data2.TCliContattiFattiIDOfferta.value = 0) then begin
    ContattoCliFattoForm.CBRifOfferta.Checked := False;
    ContattoCliFattoForm.DBGrid2.color := clBtnFace;
  end else begin
    ContattoCliFattoForm.CBRifOfferta.Checked := True;
    ContattoCliFattoForm.DBGrid2.color := clWindow;
    // cerca l'offerta
    if not Data2.TCliOfferte.active then Data2.TCliOfferte.Open;
    Data2.TCliOfferte.First;
    while (Data2.TCliOfferteID.Value <> Data2.TCliContattiFattiIDOfferta.Value) and (not (Data2.TCliOfferte.EOF)) do
      Data2.TCliOfferte.Next;
  end;
  ContattoCliFattoForm.Showmodal;
  if ContattoCliFattoForm.ModalResult = mrOK then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'update EBC_ContattiFatti set TipoContatto=:xTipoContatto,Data=:xData,Ore=:xOre,ParlatoCon=:xParlatoCon, ' +
          'Descrizione=:xDescrizione,IDOfferta=:xIDOfferta,IDContattoCli=:xIDContattoCli ' +
          'where ID=' + Data2.TCliContattiFattiID.asString;
        Q1.ParamByName('xTipoContatto').asString := ContattoCliFattoForm.CBTipoContatto.Text;
        Q1.ParamByName('xData').asDateTime := ContattoCliFattoForm.DEData.Date;
        Q1.ParamByName('xOre').asDateTime := StrToDateTime(DatetoStr(ContattoCliFattoForm.DEData.Date) + ' ' + ContattoCliFattoForm.MEOre.Text);
        Q1.ParamByName('xParlatoCon').asString := ContattoCliFattoForm.EParlatoCon.Text;
        Q1.ParamByName('xDescrizione').asString := ContattoCliFattoForm.EDescrizione.Text;
        Q1.ParamByName('xIDOfferta').asInteger := ContattoCliFattoForm.xIDOfferta;
        Q1.ParamByName('xIDContattoCli').asInteger := ContattoCliFattoForm.xIDContattoCli;
        Q1.ExecSQL;
        DB.CommitTrans;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
    Data2.TCliContattiFatti.Close;
    Data2.TCliContattiFatti.Open;
  end;
  ContattoCliFattoForm.Free;
end;

procedure TMainForm.ToolbarButton9721Click(Sender: TObject);
begin
  if not CheckProfile('192') then Exit;
  if Data2.TCliContattiFatti.RecordCount > 0 then
    if MessageDlg('Sei sicuro di voler eliminare il contatto ?', mtWarning, [mbNo, mbYes], 0) = mrYes then begin
      with Data do begin
        DB.BeginTrans;
        try
          Q1.Close;
          Q1.SQL.text := 'delete from EBC_ContattiFatti where ID=' + Data2.TCliContattiFattiID.asString;
          Q1.ExecSQL;
          DB.CommitTrans;
        except
          DB.RollbackTrans;
          MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
        end;
      end;
    end;
  Data2.TCliContattiFatti.Close;
  Data2.TCliContattiFatti.open;
end;

procedure TMainForm.ToolbarButton9722Click(Sender: TObject);
var xIDOfferta, i: integer;
  Vero: boolean;
  xCodOfferta: string;
begin
  if not CheckProfile('143') then Exit;
  OffertaClienteForm := TOffertaClienteForm.create(self);
  OffertaClienteForm.DEData.Date := Date;
  OffertaClienteForm.xIDCliente := Data2.TEBCClientiID.Value;
  OffertaClienteForm.xIDContratto := 0;
  OffertaClienteForm.xCodContratto := '';
  OffertaClienteForm.xIDArea := 0;
  OffertaClienteForm.EArea.Text := '';
  // creazione offerta (necessario per gestione dettagli)
  with Data do begin
    DB.BeginTrans;
    try
      Q1.Close;
      Q1.SQL.text := 'insert into EBC_Offerte (IDCliente,Stato) ' +
        'values (:xIDCliente,:xStato)';
      Q1.ParamByName('xIDCliente').asInteger := Data2.TEBCClientiID.Value;
      Q1.ParamByName('xStato').asString := '';
      Q1.ExecSQL;
      // ID offerta
      Data.QTemp.Close;
      Data.QTemp.SQL.text := 'select @@IDENTITY as LastID';
      Data.QTemp.Open;
      xIDOfferta := Data.QTemp.FieldByName('LastID').asInteger;

      // progressivo dal nuovo contatore (non pi� dall'ID)
      xCodOfferta := GetCodOfferta;
      Q1.SQL.text := 'update EBC_Offerte set Rif=:xRif ' +
        'where ID=' + IntToStr(xIDOfferta);
      Q1.ParamByName('xRif').asString := xCodOfferta;
      Q1.ExecSQL;

      DB.CommitTrans;
    except
      DB.RollbackTrans;
      MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
    end;
  end;
  OffertaClienteForm.ERif.Text := xCodOfferta;
  OffertaClienteForm.xIDOfferta := xIDOfferta;
  OffertaClienteForm.xStato := 'presentata';
  OffertaClienteForm.xIDUtente := xIDUtenteAttuale;
  OffertaClienteForm.Showmodal;
  if OffertaClienteForm.ModalResult = mrOK then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'update EBC_Offerte set Data=:xData,AMezzo=:xAMezzo,AttenzioneDi=:xAttenzioneDi,ImportoTotale=:xImportoTotale,Rif=:xRif,' +
          '        Condizioni=:xCondizioni,Note=:xNote,Stato=:xStato,IDRicerca=:xIDRicerca,IDContratto=:xIDContratto,IDUtente=:xIDUtente,IDArea=:xIDArea,IDLineaProdotto=:xIDLineaProdotto ' +
          ' where ID=' + IntToStr(xIDOfferta);
        Q1.ParamByName('xData').asDateTime := OffertaClienteForm.DEData.Date;
        Q1.ParamByName('xAMezzo').asString := OffertaClienteForm.CBAMezzo.Text;
        Q1.ParamByName('xAttenzioneDi').asString := OffertaClienteForm.EAttenzioneDi.Text;
        Q1.ParamByName('xImportoTotale').asFloat := OffertaClienteForm.xImportoTotale;
        Q1.ParamByName('xRif').asString := OffertaClienteForm.ERif.Text;
        Q1.ParamByName('xCondizioni').asString := OffertaClienteForm.ECondizioni.Text;
        Q1.ParamByName('xNote').asString := OffertaClienteForm.ENote.text;
        Q1.ParamByName('xStato').asString := OffertaClienteForm.CBStato.text;
        Q1.ParamByName('xIDRicerca').asInteger := OffertaClienteForm.xIDRicerca;
        Q1.ParamByName('xIDContratto').asInteger := OffertaClienteForm.xIDContratto;
        Q1.ParamByName('xIDUtente').asInteger := xIDUtenteAttuale;
        Q1.ParamByName('xIDArea').asInteger := OffertaClienteForm.xIDArea;
        Q1.ParamByName('xIDLineaProdotto').asInteger := OffertaClienteForm.QLineeProdottoID.Value;
        Q1.ExecSQL;
        // prima riga storico offerte
        Data.QTemp.Close;
        Q1.Close;
        Q1.SQL.text := 'insert into EBC_OfferteStorico (IDOfferta,Stato,DallaData) ' +
          'values (:xIDOfferta,:xStato,:xDallaData)';
        Q1.ParamByName('xIDOfferta').asInteger := xIDOfferta;
        Q1.ParamByName('xStato').asString := OffertaClienteForm.CBStato.text;
        Q1.ParamByName('xDallaData').asDateTime := OffertaClienteForm.DEData.Date;
        Q1.ExecSQL;
        DB.CommitTrans;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
      Data2.TCliOfferte.Close;
      Data2.TCliOfferte.Open;
    end;
  end else begin
    // cancellazione offerta
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'delete from EBC_Offerte ' +
          ' where ID=' + IntToStr(xIDOfferta);
        Q1.ExecSQL;
        DB.CommitTrans;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
  end;
  OffertaClienteForm.Free;
end;

procedure TMainForm.ToolbarButton9723Click(Sender: TObject);
var xStato, xVecchioStato: string;
  xIDOfferta, i: integer;
  Vero: boolean;
begin
  if not CheckProfile('144') then Exit;
  if Data2.TCliOfferte.RecordCount = 0 then exit;
  xVecchioStato := Data2.TCliOfferteStato.Value;
  OffertaClienteForm := TOffertaClienteForm.create(self);
  OffertaClienteForm.ERif.Text := Data2.TCliOfferteRif.asString;
  OffertaClienteForm.DEData.Date := Data2.TCliOfferteData.Value;
  OffertaClienteForm.CBAMezzo.Text := Data2.TCliOfferteAMezzo.Value;
  OffertaClienteForm.EAttenzioneDi.Text := Data2.TCliOfferteAttenzioneDi.Value;
  OffertaClienteForm.ENote.text := Data2.TCliOfferteNote.Value;
  OffertaClienteForm.ECondizioni.Text := Data2.TCliOfferteCondizioni.Value;
  OffertaClienteForm.xIDUtente := Data2.TCliOfferteIDUtente.Value;
  OffertaClienteForm.CBStato.text := Data2.TCliOfferteStato.Value;
  if not CheckProfile('147', false) then
    // l'utente non pu� visualizzare gli importi di TUTTE le offerte
    OffertaClienteForm.DBTreeListImporto.Visible := False;
  if (not CheckProfile('148', false)) and (xIDUtenteAttuale <> Data2.TCliOfferteIDUtente.Value) then
    // l'utente non pu� visualizzare gli importi delle offerte degli ALTRI UTENTI
    OffertaClienteForm.DBTreeListImporto.Visible := False;
  xStato := Data2.TCliOfferteStato.Value;
  OffertaClienteForm.xIDCliente := Data2.TEBCClientiID.Value;
  OffertaClienteForm.xIDContratto := Data2.TCliOfferteIDContratto.Value;
  OffertaClienteForm.xCodContratto := Data2.TCliOfferteCodContratto.value;
  if Data2.TCliOfferteIDRicerca.asString <> '' then
    OffertaClienteForm.xIDRicerca := Data2.TCliOfferteIDRicerca.Value;
  OffertaClienteForm.xIDOfferta := Data2.TCliOfferteID.Value;
  OffertaClienteForm.xStato := Data2.TCliOfferteStato.value;
  if Data2.TCliOfferteIDArea.asString <> '' then begin
    OffertaClienteForm.xIDArea := Data2.TCliOfferteIDArea.Value;
    Data.QTemp.Close;
    Data.QTemp.SQL.text := 'select Descrizione Area from Aree where ID=' + Data2.TCliOfferteIDArea.asString;
    Data.QTemp.Open;
    OffertaClienteForm.EArea.text := Data.QTemp.FieldByName('Area').asString;
    Data.QTemp.Close;
  end;
  OffertaClienteForm.QLineeProdotto.Locate('ID', Data2.TCliOfferteIDLineaProdotto.value, []);
  OffertaClienteForm.Showmodal;
  if OffertaClienteForm.ModalResult = mrOK then begin
    xIDOfferta := Data2.TCliOfferteID.Value;
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'update EBC_Offerte set Data=:xData,AMezzo=:xAMezzo,AttenzioneDi=:xAttenzioneDi,ImportoTotale=:xImportoTotale,Rif=:xRif,' +
          '        Condizioni=:xCondizioni,Note=:xNote,Stato=:xStato,IDRicerca=:xIDRicerca,IDContratto=:xIDContratto,IDArea=:xIDArea,IDLineaProdotto=:xIDLineaProdotto ' +
          ' where ID=' + Data2.TCliOfferteID.AsString;
        Q1.ParamByName('xData').asDateTime := OffertaClienteForm.DEData.Date;
        Q1.ParamByName('xAMezzo').asString := OffertaClienteForm.CBAMezzo.Text;
        Q1.ParamByName('xAttenzioneDi').asString := OffertaClienteForm.EAttenzioneDi.Text;
        Q1.ParamByName('xImportoTotale').asFloat := OffertaClienteForm.xImportoTotale;
        Q1.ParamByName('xRif').asString := OffertaClienteForm.ERif.Text;
        Q1.ParamByName('xCondizioni').asString := OffertaClienteForm.ECondizioni.Text;
        Q1.ParamByName('xNote').asString := OffertaClienteForm.ENote.text;
        Q1.ParamByName('xStato').asString := OffertaClienteForm.CBStato.text;
        Q1.ParamByName('xIDRicerca').asInteger := OffertaClienteForm.xIDRicerca;
        Q1.ParamByName('xIDContratto').asInteger := OffertaClienteForm.xIDContratto;
        Q1.ParamByName('xIDArea').asInteger := OffertaClienteForm.xIDArea;
        Q1.ParamByName('xIDLineaProdotto').asInteger := OffertaClienteForm.QLineeProdottoID.Value;
        Q1.ExecSQL;
        // se lo stato � cambiato, storicizza stato
        if xStato <> OffertaClienteForm.CBStato.text then begin
          Q1.Close;
          Q1.SQL.text := 'insert into EBC_OfferteStorico (IDOfferta,Stato,DallaData) ' +
            'values (:xIDOfferta,:xStato,:xDallaData)';
          Q1.ParamByName('xIDOfferta').asInteger := xIDOfferta;
          Q1.ParamByName('xStato').asString := OffertaClienteForm.CBStato.text;
          Q1.ParamByName('xDallaData').asDateTime := Date;
          Q1.ExecSQL;
        end;
        DB.CommitTrans;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
    Data2.TCliOfferte.Close;
    Data2.TCliOfferte.Open;
    Data2.TCliOfferte.Locate('ID', OffertaClienteForm.xIDOfferta, []);
  end;
  OffertaClienteForm.Free;
end;

procedure TMainForm.ToolbarButton9724Click(Sender: TObject);
begin
  if not CheckProfile('145') then Exit;
  if not Data2.TCliOfferte.EOF then
    if MessageDlg('Sei sicuro di voler eliminare l''offerta ?', mtWarning, [mbNo, mbYes], 0) = mrYes then begin
      with Data do begin
        DB.BeginTrans;
        try
          Q1.Close;
          Q1.SQL.text := 'delete from EBC_Offerte ' +
            'where ID=' + Data2.TCliOfferteID.AsString;
          Q1.ExecSQL;
          DB.CommitTrans;
          Data2.TCliOfferte.Close;
          Data2.TCliOfferte.Open;
        except
          DB.RollbackTrans;
          MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
        end;
      end;
    end;
end;

procedure TMainForm.ToolbarButton9725Click(Sender: TObject);
var xFileWord: string;
  MSWord: Variant;
begin
  if not CheckProfile('146') then Exit;
  if not FileExists(GetDocPath + '\ModOfferta.doc') then begin
    ShowMessage('Modello inesistente o percorso errato.');
    exit;
  end;
  if not Data2.TCliOfferte.EOF then begin
    try
      MsWord := CreateOleObject('Word.Basic');
    except
      ShowMessage('Non riesco ad aprire Microsoft Word.');
      Exit;
    end;
    MsWord.AppShow;
    xFileWord := GetDocPath + '\o' + Data2.TCliOfferteID.asString + '.doc';
    if FileExists(xFileWord) then begin
      // file da aprire
      MsWord.FileOpen(xFileWord);
    end else begin
      // file da creare
      MsWord.FileOpen(GetDocPath + '\ModOfferta.doc');
      // riempimento campi
      MsWord.EditFind('#data');
      MsWord.Insert(DateToStr(Date));
      MsWord.EditFind('#NomeAzienda');
      MsWord.Insert(Data2.TEBCclientiDescrizione.Value);
      MsWord.EditFind('#IndirizzoAzienda');
      MsWord.Insert(Data2.TEBCclientiIndirizzo.Value);
      MsWord.EditFind('#CapLuogoProvAzienda');
      MsWord.Insert(Data2.TEBCclientiCap.Value + ' ' + Data2.TEBCclientiComune.Value + ' (' + Data2.TEBCclientiProvincia.Value + ')');
      MsWord.EditFind('#AttenzioneDi');
      MsWord.Insert(Data2.TCliOfferteAttenzioneDi.Value);

      MsWord.EditFind('#FirmaConsulente');
      MsWord.Insert(DataRicerche.TRicerchePendUtente.asString);

      MsWord.FileSaveAs(xFileWord);
    end;
  end;
end;

procedure TMainForm.LOBTab8Click(Sender: TObject);
begin
  if not CheckProfile('75') then Exit;
  Data.TLingue.Open;
  PageControl1.ActivePage := TSTabelle;
  PageControl3.ActivePage := TSTabLingue;
  LOBTab1.Down := False;
  LOBTab2.Down := False;
  LOBTab3.Down := False;
  LOBTab4.Down := False;
  LOBTab7.Down := False;
  LOBTab8.Down := True;
  LOBTabAnn.Down := False;
  LOBTabAttiv.Down := False;
  LOBTabAziende.Down := False;
  LOBTab11.Down := False;
  LOBTabComuni.Down := False;
  LOBTabNazioni.Down := False;
  LOBTabModelliWord.Down := False;
  Caption := '[M/75] - ' + xCaption;
end;

procedure TMainForm.TbBLingueNewClick(Sender: TObject);
begin
  Data.TLingue.Insert;
end;

procedure TMainForm.TbBLingueDelClick(Sender: TObject);
begin
  if MessageDlg('Sei sicuro di voler eliminare la lingua selezionata ?', mtWarning,
    [mbNo, mbYes], 0) = mrYes then Data.TLingue.Delete; ;
end;

procedure TMainForm.TbBLingueOKClick(Sender: TObject);
begin
  Data.TLingue.Post;
end;

procedure TMainForm.TbBLingueCanClick(Sender: TObject);
begin
  Data.TLingue.Cancel;
end;

procedure TMainForm.ToolbarButton9712Click(Sender: TObject);
var xTot: integer;
begin
  if not CheckProfile('33') then Exit;
  if DataAnnunci.DsAnnunci.State in [dsInsert, dsEdit] then begin
    with DataAnnunci.TAnnunci do begin
      Data.DB.BeginTrans;
      try
        ApplyUpdates;
        Data.DB.CommitTrans;
      except
        Data.DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate in Ann_Annunci', mtError, [mbOK], 0);
        raise;
      end;
      CommitUpdates;
      //Close;
      //Open;
    end;
  end;
  ElencoRicPendForm := TElencoRicPendForm.create(self);
  if DataAnnunci.TAnnunciCliente.Value <> '' then
    ElencoRicPendForm.dxDBGrid1.Filter.Add(ElencoRicPendForm.dxDBGrid1Cliente, DataAnnunci.TAnnunciCliente.Value, DataAnnunci.TAnnunciCliente.Value);
  ElencoRicPendForm.ShowModal;
  if ElencoRicPendForm.ModalResult = mrOK then begin
    with Data do begin
      Q1.close;
      Q1.SQL.text := 'select count(*) Tot from Ann_AnnunciRicerche ' +
        'where IDAnnuncio=' + DataAnnunci.TAnnunciID.asString + ' and IDRicerca=' + ElencoRicPendForm.QRicAttiveID.asString;
      Q1.Open;
      xTot := Q1.FieldByName('Tot').asInteger;
      Q1.Close;
      if xTot = 0 then begin
        Data.DB.BeginTrans;
        try
          Q1.SQL.text := 'insert into Ann_AnnunciRicerche (IDAnnuncio,IDRicerca,Codice) ' +
            'values (:xIDAnnuncio,:xIDRicerca,:xCodice)';
          Q1.ParamByName('xIDAnnuncio').asInteger := DataAnnunci.TAnnunciID.Value;
          Q1.ParamByName('xIDRicerca').asInteger := ElencoRicPendForm.QRicAttiveID.Value;
          Q1.ParamByName('xCodice').asString := Uppercase(char(DataAnnunci.TAnnunciRic.RecordCount + Ord('a')));
          Q1.ExecSQL;
          Data.DB.CommitTrans;
          DataAnnunci.TAnnunciRic.Close;
          DataAnnunci.TAnnunciRic.Open;
        except
          Data.DB.RollbackTrans;
          MessageDlg('ERRORE SUL DATABASE:  operazione non completata', mtError, [mbOK], 0);
        end;
      end;
    end;
  end;
  ElencoRicPendForm.Free;
end;

procedure TMainForm.ToolbarButton9726Click(Sender: TObject);
begin
  if not CheckProfile('34') then Exit;
  if DataAnnunci.TAnnunciRic.RecordCount > 0 then
    if MessageDlg('Sei sicuro di voler cancellare il riferimento ?', mtWarning,
      [mbNo, mbYes], 0) = mrYes then begin
      Data.DB.BeginTrans;
      try
        Data.Q1.SQL.text := 'delete from Ann_AnnunciRicerche where ID=' + DataAnnunci.TAnnunciRicID.asString;
        Data.Q1.ExecSQL;
        Data.DB.CommitTrans;
        DataAnnunci.TAnnunciRic.Close;
        DataAnnunci.TAnnunciRic.Open;
      except
        Data.DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE:  operazione non completata', mtError, [mbOK], 0);
      end;
    end;
end;

procedure TMainForm.RGAnnFiltroClick(Sender: TObject);
begin
  case RGAnnFiltro.ItemIndex of
    0: DataAnnunci.TAnnunci.Filtered := False;
    1: begin
        DataAnnunci.TAnnunci.Filtered := True;
        DataAnnunci.TAnnunci.Filter := 'Archiviato=False';
      end;
    2: begin
        DataAnnunci.TAnnunci.Filtered := True;
        DataAnnunci.TAnnunci.Filter := 'Archiviato=True';
      end;
  end;
end;

procedure TMainForm.PCAnnunciChange(Sender: TObject);
begin
  if DataAnnunci.DsAnnunci.State in [dsInsert, dsEdit] then DataAnnunci.TAnnunci.Post;

  if PCAnnunci.activePage = TSAnnDate then begin
    if not CheckProfile('35') then begin
      PCAnnunci.ActivePage := xPaginaAnnunciPrec;
      Exit;
    end;
    DataAnnunci.QAnnEdizDataXann.Open;
    DataAnnunci.QAnnAnagData.Open;
  end else begin
    DataAnnunci.QAnnEdizDataXann.Close;
    DataAnnunci.QAnnAnagData.Close;
  end;

  if PCAnnunci.activePage = TSAnnCVPerv then begin
    if not CheckProfile('36') then begin
      PCAnnunci.ActivePage := xPaginaAnnunciPrec;
      Exit;
    end;
    DataAnnunci.QAnnAnag.Open;
  end else begin
    DataAnnunci.QAnnAnag.Close;
  end;
  // Time-report
  if PCAnnunci.activePage = TSAnnTimeSheet then begin
    DataAnnunci.QAnnTimeReport.Open;
  end else begin
    DataAnnunci.QAnnTimeReport.Close;
  end;
end;

procedure TMainForm.SpeedButton1Click(Sender: TObject);
begin
  if DataAnnunci.TAnnunciFileTesto.Value <> '' then
    OpenDialog1.FileName := DataAnnunci.TAnnunciFileTesto.Value;
  if OpenDialog1.Execute then begin
    DataAnnunci.TAnnunci.Edit;
    DataAnnunci.TAnnunciFileTesto.Value := OpenDialog1.FileName;
    DataAnnunci.TAnnunci.Post;
  end;
end;

procedure TMainForm.DBGrid24KeyPress(Sender: TObject; var Key: Char);
begin
  if Key <> chr(13) then begin
    xStringaArea := xStringaArea + key;
    Data2.TAree.Locate('ID', xStringaArea, []);
  end;
end;

procedure TMainForm.ToolbarButton9729Click(Sender: TObject);
var xPerche: string;
begin
  if not CheckProfile('012') then Exit;
  if Data2.QAnagRicerche.RecordCount > 0 then begin
    if InputQuery('Elimina dalla ricerca', 'Motivo:', xPerche) then begin
      Data.DB.BeginTrans;
      try
        DataRicerche.QGen.SQL.Clear;
        DataRicerche.QGen.SQL.Add('update EBC_CandidatiRicerche set Escluso=1,');
        DataRicerche.QGen.SQL.Add('Stato="eliminato (motivo: ' + xPerche + ')" where IDAnagrafica=' + Data.TAnagraficaID.asString + ' and IDRicerca=' + Data2.QAnagRicercheIDRicerca.asString);
        DataRicerche.QGen.execSQL;
        // aggiornamento storico
        Data.Q1.SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente) ' +
          'values (:xIDAnagrafica,:xIDEvento,:xDataEvento,:xAnnotazioni,:xIDRicerca,:xIDUtente)';
        Data.Q1.ParambyName('xIDAnagrafica').asInteger := Data.TAnagraficaID.Value;
        Data.Q1.ParambyName('xIDEvento').asInteger := 17;
        Data.Q1.ParambyName('xDataEvento').asDateTime := Date;
        Data.Q1.ParambyName('xAnnotazioni').asString := 'ric.' + Data2.QAnagRicercheProgressivo.asString + ' per ' + Data2.QAnagRicercheCliente.asString + ' (motivo: ' + xPerche + ')';
        Data.Q1.ParambyName('xIDRicerca').asInteger := Data2.QAnagRicercheIDRicerca.value;
        Data.Q1.ParambyName('xIDUtente').asInteger := xIDUtenteAttuale;
        Data.Q1.ExecSQL;
        // opzioni cambiamento stato
        OpzioniEliminazForm := TOpzioniEliminazForm.create(self);
        OpzioniEliminazForm.ShowModal;
        Data.Q1.SQL.text := 'update Anagrafica set IDTipoStato=:xTipo,IDStato=:xStato where ID=' + Data.TAnagraficaID.asString;
        if OpzioniEliminazForm.RGOpzioni.Itemindex = 1 then begin
          Data.Q1.ParamByName('xTipo').asInteger := 2;
          Data.Q1.ParamByName('xStato').asInteger := 28;
          Data.Q1.ExecSQL;
        end;
        if OpzioniEliminazForm.RGOpzioni.Itemindex = 2 then begin
          Data.Q1.ParamByName('xTipo').asInteger := 5;
          Data.Q1.ParamByName('xStato').asInteger := 30;
          Data.Q1.ExecSQL;
        end;
        Data.DB.CommitTrans;
        OpzioniEliminazForm.Free;
        Data2.QAnagRicerche.Close;
        Data2.QAnagRicerche.Open;
      except
        Data.DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE:  operazione non completata', mtError, [mbOK], 0);
      end;
    end;
  end;
end;

procedure TMainForm.CBFiltroAnagRicClick(Sender: TObject);
begin
  Data2.QAnagRicerche.Close;
  Data2.QAnagRicerche.SQL.text := 'select EBC_Ricerche.Progressivo, EBC_Clienti.Descrizione Cliente, ' +
    'Mansioni.Descrizione Ruolo,Escluso,EBC_StatiRic.StatoRic,EBC_Ricerche.DataInizio,EBC_Ricerche.DataFine, ' +
    'Users.Nominativo Utente, EBC_CandidatiRicerche.ID,EBC_CandidatiRicerche.IDRicerca from EBC_CandidatiRicerche,EBC_Ricerche,Mansioni, ' +
    'Users,EBC_StatiRic,EBC_Clienti where EBC_CandidatiRicerche.IDRicerca=EBC_Ricerche.ID ' +
    'and EBC_Ricerche.IDMansione=Mansioni.ID and EBC_Ricerche.IDUtente=Users.ID ' +
    'and EBC_Ricerche.IDStatoRic=EBC_StatiRic.ID and EBC_Ricerche.IDCliente=EBC_Clienti.ID ' +
    'and EBC_CandidatiRicerche.IDAnagrafica=:ID ';
  if CBFiltroAnagRic.Checked then
    Data2.QAnagRicerche.SQL.Add('and Escluso=0');
  Data2.QAnagRicerche.Open;
end;

procedure TMainForm.ToolbarButton9730Click(Sender: TObject);
begin
  if not CheckProfile('013') then Exit;
  if Data2.QAnagRicerche.RecordCount > 0 then
    if MessageDlg('Sei sicuro di voler eliminare COMPLETAMENTE ogni riferimento soggetto-ricerca ?', mtWarning,
      [mbNo, mbYes], 0) = mrYes then begin
      with Data do begin
        DB.BeginTrans;
        try
          Q1.Close;
          Q1.SQL.text := 'delete from EBC_CandidatiRicerche where ID=' + Data2.QAnagRicercheID.AsString;
          Q1.ExecSQL;
          DB.CommitTrans;
        except
          DB.RollbackTrans;
          MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
        end;
      end;

      Data2.QAnagRicerche.Close;
      Data2.QAnagRicerche.Open;
    end;
end;

procedure TMainForm.SpeedButton2Click(Sender: TObject);
begin
  ComuniForm := TComuniForm.create(self);
  ComuniForm.EProv.text := xUltimaProv;
  ComuniForm.ShowModal;
  if not (Data.DsAnagrafica.State in [dsEdit, dsInsert]) then Data.TAnagrafica.Edit;
  if ComuniForm.ModalResult = mrOK then begin
    Data.TAnagraficaComune.Value := ComuniForm.QComuniDescrizione.Value;
    Data.TAnagraficaCap.Value := ComuniForm.QComuniCap.Value;
    Data.TAnagraficaProvincia.Value := ComuniForm.QComuniProv.Value;
    Data.TAnagraficaIDComuneRes.Value := ComuniForm.QComuniID.Value;
    Data.TAnagraficaIDZonaRes.Value := ComuniForm.QComuniIDZona.Value;
  end else begin
    Data.TAnagraficaComune.Value := '';
    Data.TAnagraficaCap.Value := '';
    Data.TAnagraficaProvincia.Value := '';
    Data.TAnagraficaIDComuneRes.Value := 0;
    Data.TAnagraficaIDZonaRes.Value := 0;
  end;
  with Data.TAnagrafica do begin
    Data.DB.BeginTrans;
    try
      ApplyUpdates;
      Data.DB.CommitTrans;
    except
      Data.DB.RollbackTrans;
      MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      raise;
    end;
    CommitUpdates;
  end;
  xUltimaProv := ComuniForm.EProv.text;
  ComuniForm.Free;
end;

procedure TMainForm.BCopiadaResClick(Sender: TObject);
begin
  if not (Data.DsAnagrafica.State in [dsEdit, dsInsert]) then Data.TAnagrafica.Edit;
  Data.TAnagraficaDomicilioIndirizzo.Value := Data.TAnagraficaIndirizzo.Value;
  Data.TAnagraficaDomicilioCap.Value := Data.TAnagraficaCap.Value;
  Data.TAnagraficaDomicilioComune.Value := Data.TAnagraficaComune.Value;
  Data.TAnagraficaDomicilioProvincia.Value := Data.TAnagraficaProvincia.Value;
  Data.TAnagraficaIDComuneDom.Value := Data.TAnagraficaIDComuneRes.Value;
  Data.TAnagraficaIDZonaDom.Value := Data.TAnagraficaIDZonaRes.Value;
  Data.TAnagraficaDomicilioTipoStrada.Value := Data.TAnagraficaTipoStrada.Value;
  Data.TAnagraficaDomicilioNumCivico.Value := Data.TAnagraficaNumCivico.Value;
  Data.TAnagraficaDomicilioStato.Value := Data.TAnagraficaStato.Value;
end;

procedure TMainForm.SpeedButton4Click(Sender: TObject);
begin
  ComuniForm := TComuniForm.create(self);
  ComuniForm.EProv.text := xUltimaProv;
  ComuniForm.ShowModal;
  if not (Data.DsAnagrafica.State in [dsEdit, dsInsert]) then Data.TAnagrafica.Edit;
  if ComuniForm.ModalResult = mrOK then begin
    Data.TAnagraficaDomicilioComune.Value := ComuniForm.QComuniDescrizione.Value;
    Data.TAnagraficaDomicilioCap.Value := ComuniForm.QComuniCap.Value;
    Data.TAnagraficaDomicilioProvincia.Value := ComuniForm.QComuniProv.Value;
    Data.TAnagraficaIDComuneDom.Value := ComuniForm.QComuniID.Value;
    Data.TAnagraficaIDZonaDom.Value := ComuniForm.QComuniIDZona.Value;
  end else begin
    Data.TAnagraficaDomicilioComune.Value := '';
    Data.TAnagraficaDomicilioCap.Value := '';
    Data.TAnagraficaDomicilioProvincia.Value := '';
    Data.TAnagraficaIDComuneDom.Value := 0;
    Data.TAnagraficaIDZonaDom.Value := 0;
  end;
  with Data.TAnagrafica do begin
    Data.DB.BeginTrans;
    try
      ApplyUpdates;
      Data.DB.CommitTrans;
    except
      Data.DB.RollbackTrans;
      MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      raise;
    end;
    CommitUpdates;
  end;
  xUltimaProv := ComuniForm.EProv.text;
  ComuniForm.Free;
end;

procedure TMainForm.ECercaComChange(Sender: TObject);
begin
  Data.QComuni.Close;
  Data.QComuni.ParamByName('xDesc').asString := ECercaCom.text + '%';
  Data.QComuni.Open;
end;

procedure TMainForm.LOBTabComuniClick(Sender: TObject);
begin
  if not CheckProfile('791') then Exit;
  Data.TZone.Open;
  PageControl1.ActivePage := TSTabelle;
  PageControl3.ActivePage := TSTabCom;
  LOBTab1.Down := False;
  LOBTab2.Down := False;
  LOBTab3.Down := False;
  LOBTab4.Down := False;
  LOBTab7.Down := False;
  LOBTab8.Down := False;
  LOBTabAnn.Down := False;
  LOBTabAttiv.Down := False;
  LOBTabAziende.Down := False;
  LOBTab11.Down := False;
  LOBTabComuni.Down := True;
  LOBTabNazioni.Down := False;
  LOBTabModelliWord.Down := False;
  Caption := '[M/791] - ' + xCaption;
end;

procedure TMainForm.ToolbarButton9711Click(Sender: TObject);
var xZona: string;
begin
  xZona := '';
  if InputQuery('Nuova zona', 'nome:', xZona) then begin
    if Data.TZone.FindKey([xZona]) then
      MessageDlg('La zona esiste gi�', mtError, [mbOK], 0)
    else begin
      Data.TZone.Insert;
      Data.TZoneZona.Value := xZona;
      Data.TZone.Post;
    end;
  end;
end;

procedure TMainForm.ToolbarButton9731Click(Sender: TObject);
var xZona: string;
begin
  if Data.TZone.RecordCount > 0 then begin
    xZona := Data.TZoneZona.Value;
    if InputQuery('Modifica nome zona', 'nuovo nome:', xZona) then begin
      Data.TZone.Edit;
      Data.TZoneZona.Value := xZona;
      Data.TZone.Post;
    end;
  end;
end;

procedure TMainForm.ToolbarButton9714Click(Sender: TObject);
begin
  if not Data.TZone.Eof then
    if MessageDlg('Sei sicuro di voler eliminare la ZONA selezionata ?', mtWarning,
      [mbNo, mbYes], 0) = mrYes then Data.TZone.Delete;
end;

procedure TMainForm.ToolbarButton971Click(Sender: TObject);
begin
  if MessageDlg('Associare al comune di ' + Data.QComuniDescrizione.Value + ' la zona ' + Data.TZoneZona.Value + ' ?', mtWarning, [mbYes, mbNo], 0) = mrYes then begin
    Data.xControllaComune := False;
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'update TabCom set IDZona=:xIDZona ' +
          'where ID=' + QComuniID.asString;
        Q1.ParamByName('xIDZona').asInteger := Data.TZoneID.Value;
        Q1.ExecSQL;
        DB.CommitTrans;
        ECercaComChange(self);
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;

    Data.xControllaComune := True;
    // modifica Anagrafica per quel comune (sia residenza che domicilio)
    Data.QTemp.Close;
    Data.QTemp.SQL.Clear;
    Data.QTemp.SQL.Add('update Anagrafica set IDZonaDom=' + Data.TZoneID.asString);
    Data.QTemp.SQL.Add('where IDComuneDom=' + Data.QComuniID.asString);
    Data.QTemp.ExecSQL;
    Data.QTemp.SQL.Clear;
    Data.QTemp.SQL.Add('update Anagrafica set IDZonaRes=' + Data.TZoneID.asString);
    Data.QTemp.SQL.Add('where IDComuneRes=' + Data.QComuniID.asString);
    Data.QTemp.ExecSQL;
  end;
end;

procedure TMainForm.ToolbarButton9732Click(Sender: TObject);
begin
  if MessageDlg('Sei sicuro di voler eliminare l''associazione ?', mtWarning,
    [mbNo, mbYes], 0) = mrYes then begin
    Data.xControllaComune := False;
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'update TabCom set IDZona=null ' +
          'where ID=' + QComuniID.asString;
        Q1.ExecSQL;
        DB.CommitTrans;
        ECercaComChange(self);
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
    Data.xControllaComune := True;
    // modifica Anagrafica per quel comune (sia residenza che domicilio)
    Data.QTemp.Close;
    Data.QTemp.SQL.Clear;
    Data.QTemp.SQL.Add('update Anagrafica set IDZonaDom=null');
    Data.QTemp.SQL.Add('where IDComuneDom=' + Data.QComuniID.asString);
    Data.QTemp.ExecSQL;
    Data.QTemp.SQL.Clear;
    Data.QTemp.SQL.Add('update Anagrafica set IDZonaRes=null');
    Data.QTemp.SQL.Add('where IDComuneRes=' + Data.QComuniID.asString);
    Data.QTemp.ExecSQL;
  end;
end;

procedure TMainForm.ToolbarButton9733Click(Sender: TObject);
begin
  ZoneComuniForm := TZoneComuniForm.create(self);
  ZoneComuniForm.ShowModal;
  ZoneComuniForm.Free;
end;

procedure TMainForm.ToolbarButton9734Click(Sender: TObject);
begin
  CaricaPrimi;
  //if not Data.TAnagrafica.Active then LTotCand.Caption:=''
  //else LTotCand.Caption:=IntToStr(Data.TAnagrafica.RecordCount);
  LTotAz.Caption := IntToStr(Data2.TEBCclienti.RecordCount);
end;

procedure TMainForm.BitBtn3Click(Sender: TObject);
begin
  if not (Data2.DsEBCclienti.State = dsEdit) then Data2.TEBCclienti.Edit;
  Data2.TEBCclientiIndirizzoLegale.Value := Data2.TEBCclientiIndirizzo.Value;
  Data2.TEBCclientiCapLegale.Value := Data2.TEBCclientiCap.Value;
  Data2.TEBCclientiComuneLegale.Value := Data2.TEBCclientiComune.Value;
  Data2.TEBCclientiProvinciaLegale.Value := Data2.TEBCclientiProvincia.Value;
  Data2.TEBCClientiTipoStradaLegale.Value := Data2.TEBCClientiTipoStrada.Value;
  Data2.TEBCClientiNumCivicoLegale.Value := Data2.TEBCClientiNumCivico.Value;
  Data2.TEBCClientiNazioneAbbLegale.Value := Data2.TEBCClientiNazioneAbb.Value;
  Data2.TEBCclienti.Post;
end;

procedure TMainForm.BFattClienteNewClick(Sender: TObject);
var xDataScadenza: TDateTime;
  xAnno, xMese, xGiorno: Word;
  xAppoggio: string;
  xProgfatt: integer;
begin
  if not CheckProfile('130') then Exit;
  if Data2.dsEBCClienti.State in [dsInsert, dsEdit] then
    Data2.TEBCclienti.Post;
  // scelta data decorrenza fattura
  SelDataForm := TSelDataForm.create(self);
  SelDataForm.Caption := 'Data di decorrenza';
  SelDataForm.DEData.Date := Date;
  SelDataForm.ShowModal;
  if SelDataForm.ModalResult = mrOK then begin
    // nuovo progressivo fattura
    if Data.Q1.Active then Data.Q1.Close;
    Data.Q1.SQL.Text := 'Update Global set UltimoProgFatt=UltimoProgFatt+1';
    Data.DB.BeginTrans;
    try
      Data.Q1.ExecSQL;
      Data.DB.CommitTrans;
      Data.Q1.SQL.Text := 'select UltimoProgFatt from Global';
      Data.Q1.Open;
      xProgfatt := Data.Q1.FieldByName('UltimoProgFatt').asInteger;
      Data.Q1.Close;
    except
      Data.DB.RollbackTrans;
      MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
    end;
    FatturaForm := TFatturaForm.create(self);
    // casistica scadenza pagamento
    xDataScadenza := Date;
    if Data2.TEBCclientiSistemaPagamento.Value = 'Rimessa Diretta - scadenza 30 gg.' then
      xDataScadenza := SelDataForm.DEData.Date + 30;
    if Data2.TEBCclientiSistemaPagamento.Value = 'Rimessa Diretta - scadenza 60 gg.' then
      xDataScadenza := SelDataForm.DEData.Date + 60;
    if Data2.TEBCclientiSistemaPagamento.Value = 'Rimessa Diretta - scadenza 90 gg.' then
      xDataScadenza := SelDataForm.DEData.Date + 90;
    if Data2.TEBCclientiSistemaPagamento.Value = 'Ricevuta Bancaria - scadenza 30 gg. DF f.m.' then begin
      xDataScadenza := SelDataForm.DEData.Date + 30;
      DecoDeDate(xDataScadenza, xAnno, xMese, xGiorno);
      // fine mese
      if xMese = 12 then xDataScadenza := EncodeDate(xAnno, 12, 31)
      else xDataScadenza := EncodeDate(xAnno, xMese + 1, 1) - 1;
    end;
    if Data2.TEBCclientiSistemaPagamento.Value = 'Ricevuta Bancaria - scadenza 60 gg. DF f.m.' then begin
      xDataScadenza := SelDataForm.DEData.Date + 60;
      DecoDeDate(xDataScadenza, xAnno, xMese, xGiorno);
      // fine mese
      if xMese = 12 then xDataScadenza := EncodeDate(xAnno, 12, 31)
      else xDataScadenza := EncodeDate(xAnno, xMese + 1, 1) - 1;
    end;
    if Data2.TEBCclientiSistemaPagamento.Value = 'Ricevuta Bancaria - scadenza 90 gg. DF f.m.' then begin
      xDataScadenza := SelDataForm.DEData.Date + 90;
      DecoDeDate(xDataScadenza, xAnno, xMese, xGiorno);
      // fine mese
      if xMese = 12 then xDataScadenza := EncodeDate(xAnno, 12, 31)
      else xDataScadenza := EncodeDate(xAnno, xMese + 1, 1) - 1;
    end;
    if Data2.TEBCclientiSistemaPagamento.Value = 'Ricevuta Bancaria - scadenza 90 gg. DF f.m.' then begin
      xDataScadenza := SelDataForm.DEData.Date + 90;
      DecoDeDate(xDataScadenza, xAnno, xMese, xGiorno);
      // fine mese
      if xMese = 12 then xDataScadenza := EncodeDate(xAnno, 12, 31)
      else xDataScadenza := EncodeDate(xAnno, xMese + 1, 1) - 1;
    end;
    if copy(Data2.TEBCclientiSistemaPagamento.Value, 1, 3) = 'Rim' then begin
      // riempo la combo con le nostre banche da Global
      FatturaForm.QAppoggio.Open;
      FatturaForm.DBCBAppoggio.Items.Clear;
      FatturaForm.DBCBAppoggio.Items.Add(FatturaForm.QAppoggioAppoggioBancario1.Value);
      if FatturaForm.QAppoggioAppoggioBancario2.Value <> '' then
        FatturaForm.DBCBAppoggio.Items.Add(FatturaForm.QAppoggioAppoggioBancario2.Value);
      if FatturaForm.QAppoggioAppoggioBancario3.Value <> '' then
        FatturaForm.DBCBAppoggio.Items.Add(FatturaForm.QAppoggioAppoggioBancario3.Value);
      FatturaForm.QAppoggio.Close;
    end else xAppoggio := Data2.TEBCclientiBancaAppoggio.Value;
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'insert into Fatture ' +
          '(Progressivo,Tipo,IDCliente,Data,ModalitaPagamento,' +
          ' AppoggioBancario,Decorrenza,ScadenzaPagamento) ' +
          'values (:xProgressivo,:xTipo,:xIDCliente,:xData,:xModalitaPagamento,' +
          ' :xAppoggioBancario,:xDecorrenza,:xScadenzaPagamento)';
        Q1.ParamByName('xProgressivo').asInteger := xProgfatt;
        Q1.ParamByName('xTipo').asString := 'Fattura';
        Q1.ParamByName('xIDCliente').asInteger := Data2.TEBCclientiID.Value;
        Q1.ParamByName('xData').asDateTime := SelDataForm.DEData.Date;
        Q1.ParamByName('xModalitaPagamento').asString := Data2.TEBCclientiSistemaPagamento.Value;
        Q1.ParamByName('xAppoggioBancario').asString := xAppoggio;
        Q1.ParamByName('xDecorrenza').asDateTime := SelDataForm.DEData.Date;
        Q1.ParamByName('xScadenzaPagamento').asDateTime := xDataScadenza;
        Q1.ExecSQL;
        DB.CommitTrans;
        Data.QTemp.SQL.text := 'select max(ID) NewID from Fatture';
        Data.QTemp.Open;
        FatturaForm.TFattura.ParamByName('xID').asInteger := Data.QTemp.FieldByName('NewID').asInteger;
        Data.QTemp.Close;
        FatturaForm.TFattura.Open;
        FatturaForm.TFattDett.Open;
        FatturaForm.ShowModal;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
    FatturaForm.Free;
    Data2.TFattClienti.Close;
    Data2.TFattClienti.Open;
  end;
  SelDataForm.Free;
end;

procedure TMainForm.BModFattCliClick(Sender: TObject);
begin
  if not CheckProfile('131') then Exit;
  if Data2.TFattClienti.RecordCount > 0 then begin
    FatturaForm := TFatturaForm.create(self);
    FatturaForm.TFattura.ParamByName('xID').asInteger := Data2.TFattClientiID.Value;
    FatturaForm.TFattura.Open;
    FatturaForm.TFattDett.Open;

    if copy(FatturaForm.TFatturaModalitaPagamento.Value, 1, 3) = 'Rim' then begin
      // riempo la combo con le nostre banche da Global
      FatturaForm.QAppoggio.Open;
      FatturaForm.DBCBAppoggio.Items.Clear;
      FatturaForm.DBCBAppoggio.Items.Add(FatturaForm.QAppoggioAppoggioBancario1.Value);
      if FatturaForm.QAppoggioAppoggioBancario2.Value <> '' then
        FatturaForm.DBCBAppoggio.Items.Add(FatturaForm.QAppoggioAppoggioBancario2.Value);
      if FatturaForm.QAppoggioAppoggioBancario3.Value <> '' then
        FatturaForm.DBCBAppoggio.Items.Add(FatturaForm.QAppoggioAppoggioBancario3.Value);
      FatturaForm.QAppoggio.Close;
    end;
    FatturaForm.ShowModal;
    FatturaForm.Free;
    Data2.TFattClienti.Close;
    Data2.TFattClienti.Open;
  end;
end;

procedure TMainForm.ToolbarButton9735Click(Sender: TObject);
begin
  if not CheckProfile('132') then Exit;
  if Data2.TFattClienti.RecordCount > 0 then
    if MessageDlg('Sei sicuro di voler cancellare la fattura ?', mtWarning,
      [mbNo, mbYes], 0) = mrYes then begin
      with Data do begin
        DB.BeginTrans;
        try
          Q1.Close;
          Q1.SQL.text := 'Delete from Fatture where ID=' + Data2.TFattClientiID.asString;
          Q1.ExecSQL;
          DB.CommitTrans;
          Data2.TFattClienti.Close;
          Data2.TFattClienti.Open;
        except
          DB.RollbackTrans;
          MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
        end;
      end;
      ShowMessage('Se necessario, correggere a mano il numero dell''ultima fattura in TABELLE - PARAMETRI');
    end;
end;

procedure TMainForm.RGFiltroFattureClick(Sender: TObject);
begin
  Data2.QTotFattCli.Close;
  Data2.QTotFattCli.SQL.Clear;
  Data2.QTotFattCli.SQL.Add('select sum(Totale) TotCliente from Fatture where IDCliente=:ID');
  case RGFiltroFatture.ItemIndex of
    0: begin
        Data2.TFattClienti.Filter := ''; Data2.TFattClienti.Filtered := False;
      end;
    1: begin
        Data2.TFattClienti.Filter := 'Tipo=''Fattura'''; Data2.TFattClienti.Filtered := True;
        Data2.QTotFattCli.SQL.Add('and Tipo="Fattura"');
      end;
    2: begin
        Data2.TFattClienti.Filter := 'Tipo=''Nota di accredito'''; Data2.TFattClienti.Filtered := True;
        Data2.QTotFattCli.SQL.Add('and Tipo="Nota di accredito"');
      end;
  end;
  Data2.QTotFattCli.Open;
end;

procedure TMainForm.ToolbarButton9720Click(Sender: TObject);
begin
  if not CheckProfile('131') then Exit;
  if Data2.TFattClienti.RecordCount > 0 then begin
    SelDataForm := TSelDataForm.create(self);
    SelDataForm.Caption := 'Data pagamento fattura';
    SelDataForm.DEData.Date := Date;
    SelDataForm.ShowModal;
    if SelDataForm.ModalResult = mrOK then begin
      Data2.TFattClienti.Edit;
      Data2.TFattClientiPagata.Value := True;
      Data2.TFattClientiPagataInData.Value := SelDataForm.DEData.Date;
      Data2.TFattClienti.Post;
    end else begin
      if MessageDlg('Vuoi togliere il "PAGATO" alla fattura ?', mtInformation, [mbYes, mbNo], 0) = mrYes then begin
        Data2.TFattClienti.Edit;
        Data2.TFattClientiPagata.Value := False;
        Data2.TFattClientiPagataInData.asString := '';
        Data2.TFattClienti.Post;
      end;
    end;
  end;
end;

procedure TMainForm.ToolbarButton9737Click(Sender: TObject);
begin
  // nuovo comune
  ComuneForm := TComuneForm.create(self);
  ComuneForm.ShowModal;
  if ComuneForm.ModalResult = mrOK then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'insert into TabCom (Descrizione,Prov,Cap,Prefisso,Regione,AreaNielsen) ' +
          'values (:xDescrizione,:xProv,:xCap,:xPrefisso,:xRegione,:xAreaNielsen)';
        Q1.ParamByName('xDescrizione').asString := ComuneForm.EDesc.Text;
        Q1.ParamByName('xProv').asString := ComuneForm.EProv.Text;
        Q1.ParamByName('xCap').asString := ComuneForm.ECap.Text;
        Q1.ParamByName('xPrefisso').asString := ComuneForm.EPrefisso.Text;
        Q1.ParamByName('xRegione').asString := ComuneForm.EReg.text;
        Q1.ParamByName('xAreaNielsen').AsInteger := StrToInt(ComuneForm.EAreaNielsen.text);
        Q1.ExecSQL;
        DB.CommitTrans;
        ECercaComChange(self);
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
  end;
  ComuneForm.Free;
end;

procedure TMainForm.BCambiaUtenteClick(Sender: TObject);
begin
  // aggiornamento log accessi per l'attuale utente
  with Data do begin
    DB.BeginTrans;
    try
      Q1.Close;
      Q1.SQL.Text := 'update Log_accessi set OraUscita=:xOraUscita ' +
        'where ID = (select max(ID) from Log_accessi where IDUtente=:xIDUtente and Data=:xData and Modulo=:xModulo)';
      Q1.ParamByName('xOraUscita').asDateTime := Now;
      Q1.ParamByName('xIDUtente').asInteger := xIDUtenteAttuale;
      Q1.ParamByName('xData').asDateTime := Date;
      Q1.ParamByName('xModulo').asString := 'MB';
      Q1.ExecSQL;

      // azzeramento MAC address
      Q1.Close;
      Q1.SQL.Text := 'update Users set Lana0=NULL ' +
        'where ID=:xIDUtente';
      Q1.ParamByName('xIDUtente').asInteger := xIDUtenteAttuale;
      Q1.ExecSQL;

      DB.CommitTrans;
    except
      DB.RollbackTrans;
      MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
    end;
  end;
  LoginForm.Accedi := 'N';
  LoginForm.Username.Text := Data.GlobalLastLogin.Value;
  LoginForm.ShowModal;
end;

procedure TMainForm.RiepTimingClick(Sender: TObject);
begin
  if not CheckProfile('056') then Exit;
  RiepTimingForm := TRiepTimingForm.create(self);
  RiepTimingForm.QClassifica.Prepare;
  RiepTimingForm.QClassifica.Params[0].asInteger := xIDUtenteAttuale;
  RiepTimingForm.QClassifica.Open;
  RiepTimingForm.QTotTempo.Prepare;
  RiepTimingForm.QTotTempo.Params[0].asInteger := xIDUtenteAttuale;
  RiepTimingForm.QTotTempo.Open;
  RiepTimingForm.ShowModal;
  RiepTimingForm.Free;
end;

procedure TMainForm.ToolbarButton9736Click(Sender: TObject);
begin
  if DataAnnunci.QAnnAnag.IsEmpty then exit;
  SchedaCandForm := TSchedaCandForm.create(self);
  if not PosizionaAnag(DataAnnunci.QAnnAnagIDAnagrafica.Value) then exit;
  SchedaCandForm.ShowModal;
  SchedaCandForm.Free;
end;

procedure TMainForm.ToolbarButton9739Click(Sender: TObject);
var xProcedi: boolean;
  xFile: string;
begin
  if DataAnnunci.QAnnAnag.IsEmpty then exit;
  ApriCV(DataAnnunci.QAnnAnagIDAnagrafica.Value);
end;

procedure TMainForm.ToolbarButton9740Click(Sender: TObject);
begin
  if DataAnnunci.QAnnAnag.IsEmpty then exit;
  QRElencoAnnCand := TQRElencoAnnCand.create(self);
  try QRElencoAnnCand.QuickRep1.Preview
  finally QRElencoAnnCand.Free
  end;
end;

procedure TMainForm.SpeedButton6Click(Sender: TObject);
var SavePlace: TBookmark;
  xCliente: string;
begin
  SelAttivitaForm := TSelAttivitaForm.create(self);
  if Data2.TEBCclientiIDAttivita.AsString <> '' then begin
    SelAttivitaForm.TSettori.locate('ID', Data2.TEBCclientiIDAttivita.Value, []);
  end;
  SelAttivitaForm.ShowModal;
  if SelAttivitaForm.ModalResult = mrOK then
    Data2.TEBCClientiIDAttivita.Value := SelAttivitaForm.TSettoriID.Value
  else
    if MessageDlg('Vuoi cancellare il campo attivit� ?', mtInformation, [mbYes, mbNo], 0) = mrYes then
      Data2.TEBCClientiIDAttivita.Value := 0;
  TbBClientiOKClick(self);
  // refresh con "saveplace"
  xCliente := Data2.TEBCClientiDescrizione.Value;
  Data2.TEBCClienti.Close;
  Data2.TEBCClienti.Open;
  EClienti.Text := xCliente;
  BCliSearchClick(self);
  SelAttivitaForm.Free;
end;

procedure TMainForm.ToolbarButton9743Click(Sender: TObject);
begin
  if not CheckProfile('25') then Exit;
  if not xNuoviCVFormCreated then
    NuoviCVForm := TNuoviCVForm.create(self);
  NuoviCVForm.Show;
  //NuoviCVForm.Free;
end;

function TMainForm.CheckProfile(CodObj: string; Mess: boolean = True): boolean;
var i: integer;
  xTrovato: boolean;
begin
  xTrovato := False;
  for i := 1 to 1000 do
    if xUserObj[i] = CodObj then begin
      xTrovato := True;
      Break;
    end;
  if xTrovato then begin
    if Mess then
      MessageDlg('L''utente ' + Data.GlobalLastLogin.Value + ' non ha diritto di accesso a questa funzione o parte del programma', mtError, [mbOK], 0);
    Result := False;
  end else Result := True;
end;

procedure TMainForm.PageControl2Changing(Sender: TObject; var AllowChange: Boolean);
begin
  if ((not Data.TAnagrafica.Active) or (Data.TAnagrafica.RecordCount = 0))
    and (PageControl2.ActivePage = TSAnagDatiAnag) then
    AllowChange := False
  else begin
    PanAnag.Visible := False;
    xPaginaPrec := PageControl2.ActivePage;
  end;
end;

procedure TMainForm.LOBAziendeClick(Sender: TObject);
begin
  if not CheckProfile('6') then Exit;
  PageControl1.ActivePage := TSAziende;
  LOBSchede.Down := False;
  LOBRicerche.Down := False;
  LOBClientiEBC.Down := False;
  LOBAnnunci.Down := False;
  LOBAziende.Down := true;
  TbAnag.Visible := False;
  TbClienti.Visible := False;
  DataAnnunci.TAnnunci.open;
  DataAnnunci.TAnnunciRic.open;
  Caption := '[M/6] - ' + xCaption;
end;

procedure TMainForm.TbBOrganigModClick(Sender: TObject);
begin
  if not CheckProfile('66') then Exit;
  DataRicerche.QOrgNew.Edit;
end;

procedure TMainForm.TbBOrganigOKClick(Sender: TObject);
begin
  with DataRicerche.QOrgNew do begin
    Data.DB.BeginTrans;
    try
      ApplyUpdates;
      Data.DB.CommitTrans;
    except
      Data.DB.RollbackTrans;
      MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      raise;
    end;
    CommitUpdates;
    Close;
    Open;
    DBTree.FullExpand;
  end;
end;

procedure TMainForm.TbBOrganigCanClick(Sender: TObject);
begin
  DataRicerche.QOrgNew.Cancel;
end;

procedure TMainForm.ToolbarButton9744Click(Sender: TObject);
var xAttuale, xSettore: string;
begin
  if not CheckProfile('64') then Exit;
  if (not DataRicerche.QOrgNew.Active) or (DataRicerche.QOrgNew.RecordCount = 0) then exit;
  ElencoDipForm := TElencoDipForm.create(self);
  ElencoDipForm.ShowModal;
  if ElencoDipForm.ModalResult = mrOK then begin
    // controllo se � gi� associato a questa azienda
    Data.QTemp.Close;
    Data.QTemp.SQL.text := 'select count(ID) Tot from EsperienzeLavorative ' +
      'where IDAnagrafica=:xIDAnagrafica and IDAzienda=:xIDAzienda';
    Data.QTemp.ParamByName('xIDAnagrafica').asInteger := ElencoDipForm.TAnagDipID.Value;
    Data.QTemp.ParamByName('xIDAzienda').asInteger := DataRicerche.QAziendeID.Value;
    Data.QTemp.open;
    if Data.QTemp.FieldByName('Tot').asInteger > 0 then begin
      MessageDlg('Il soggetto � gi� associato a questa azienda', mtError, [mbOK], 0);
      Data.QTemp.Close;
      exit;
    end;
    // controllo se � gi� associato ad un'altra azienda dove risulta "attuale"
    Data.QTemp.Close;
    Data.QTemp.SQL.text := 'select count(ID) Tot from EsperienzeLavorative ' +
      'where IDAnagrafica=:xIDAnagrafica and IDAzienda<>:xIDAzienda and Attuale=1';
    Data.QTemp.ParamByName('xIDAnagrafica').asInteger := ElencoDipForm.TAnagDipID.Value;
    Data.QTemp.ParamByName('xIDAzienda').asInteger := DataRicerche.QAziendeID.Value;
    Data.QTemp.open;
    if Data.QTemp.FieldByName('Tot').asInteger > 0 then begin
      if MessageDlg('Il soggetto � associato ad almeno un''altra azienda che risulta quella attuale' + chr(13) +
        'VUOI PROSEGUIRE', mtWarning, [mbYes, mbNO], 0) = mrNo then begin
        Data.QTemp.Close;
        exit;
      end;
    end;
    // inserimento vero e proprio
    if MessageDlg('� l''azienda attuale ?', mtInformation, [mbYes, mbNo], 0) = mrYes then
      xAttuale := '1' else xAttuale := '0';
    if DataRicerche.QAziendeIDAttivita.asString = '' then xSettore := '0'
    else xSettore := DataRicerche.QAziendeIDAttivita.asString;
    Data2.QTemp.SQL.clear;
    Data2.QTemp.SQL.Add('insert into EsperienzeLavorative (IDAnagrafica,IDAzienda,IDSettore,Attuale) ' +
      'values (' + ElencoDipForm.TAnagDipID.AsString + ',' +
      DataRicerche.QAziendeID.asString + ',' +
      xSettore + ',' + xAttuale + ')');
    Data2.QTemp.ExecSQL;
    DataRicerche.QCandAzienda.Close;
    DataRicerche.QCandAzienda.Open;
  end;
  ElencoDipForm.Free;
  DisegnaTVAziendaCand;
end;

procedure TMainForm.SpeedButton9Click(Sender: TObject);
begin
  InsRuoloForm := TInsRuoloForm.create(self);
  InsRuoloForm.CBInsComp.Visible := False;
  InsRuoloForm.ShowModal;
  if InsRuoloForm.ModalResult = mrOK then begin
    DataRicerche.QOrgNewIDMansione.Value := InsRuoloForm.TRuoliID.Value;
    //        Data2.TAbsOrg.Post;
  end;
  InsRuoloForm.Free;
end;

procedure TMainForm.SpeedButton10Click(Sender: TObject);
begin
  DataRicerche.QOrgNewIDMansione.asString := '';
end;

procedure TMainForm.DisabilitaTabAziende;
begin
  DataRicerche.QAziende.Close;
  DataRicerche.QCandAzienda.Close;
  DataRicerche.QOrgNew.Close;
end;

procedure TMainForm.DisegnaTVAziendaCand;
var N, N1: TTreeNode;
begin
  TVAziendaCand.Items.BeginUpdate;
  TVAziendaCand.Items.Clear;
  // primo nodo: azienda attuale
  N := TVAziendaCand.Items.AddChildObject(nil, 'Azienda attuale', pointer(0));
  N.ImageIndex := 13;
  N.SelectedIndex := 13;
  DataRicerche.QCandAzienda.Close;
  DataRicerche.QCandAzienda.SQL.clear;
  DataRicerche.QCandAzienda.SQL.Text := 'select Anagrafica.ID,Anagrafica.CVNumero,Anagrafica.Cognome,Anagrafica.Nome,' +
    'EsperienzeLavorative.IDMansione, Mansioni.Descrizione Ruolo ' +
    'from EsperienzeLavorative,Anagrafica, Mansioni  ' +
    'where EsperienzeLavorative.IDAnagrafica=Anagrafica.ID ' +
    'and EsperienzeLavorative.IDmansione *= Mansioni.ID ' +
    'and EsperienzeLavorative.IDAzienda=:ID ' +
    'and EsperienzeLavorative.Attuale=1 order by Cognome,Nome';
  DataRicerche.QCandAzienda.Open;
  while not DataRicerche.QCandAzienda.EOF do begin
    N1 := TVAziendaCand.Items.AddChildObject(N, DataRicerche.QCandAziendaCognome.Value + ' ' +
      DataRicerche.QCandAziendaNome.Value +
      '  ' + DataRicerche.QCandAziendaRuolo.Value, pointer(DataRicerche.QCandAziendaID.value));
    N1.ImageIndex := 11;
    N1.SelectedIndex := 11;
    DataRicerche.QCandAzienda.Next;
  end;
  // secondo nodo: azienda NON attuale
  N := TVAziendaCand.Items.AddChildObject(nil, 'Altri che ci hanno lavorato', pointer(1));
  N.ImageIndex := 14;
  N.SelectedIndex := 14;
  DataRicerche.QCandAzienda.Close;
  DataRicerche.QCandAzienda.SQL.clear;
  DataRicerche.QCandAzienda.SQL.Text := 'select Anagrafica.ID,Anagrafica.CVNumero,Anagrafica.Cognome,Anagrafica.Nome,' +
    'EsperienzeLavorative.IDMansione, Mansioni.Descrizione Ruolo ' +
    'from EsperienzeLavorative,Anagrafica,Mansioni ' +
    'where EsperienzeLavorative.IDAnagrafica=Anagrafica.ID ' +
    'and EsperienzeLavorative.IDMansione *= Mansioni.ID ' +
    'and EsperienzeLavorative.IDAzienda=:ID ' +
    'and EsperienzeLavorative.Attuale=0 order by Cognome,Nome';
  DataRicerche.QCandAzienda.Open;
  while not DataRicerche.QCandAzienda.EOF do begin
    N1 := TVAziendaCand.Items.AddChildObject(N, DataRicerche.QCandAziendaCognome.Value + ' ' +
      DataRicerche.QCandAziendaNome.Value +
      '  ' + DataRicerche.QCandAziendaRuolo.Value, pointer(DataRicerche.QCandAziendaID.value));
    N1.ImageIndex := 12;
    N1.SelectedIndex := 12;
    DataRicerche.QCandAzienda.Next;
  end;

  TVAziendaCand.Items.EndUpdate;
  TVAziendaCand.FullExpand;
  TVAziendaCand.Selected := TVAziendaCand.Items[0];
end;

procedure TMainForm.Infocandidato1Click(Sender: TObject);
begin
  if TVAziendaCand.Selected.level = 0 then exit;
  SchedaCandForm := TSchedaCandForm.create(self);
  if not PosizionaAnag(longint(TVAziendaCand.Selected.Data)) then exit;
  SchedaCandForm.ShowModal;
  SchedaCandForm.Free;
  DisegnaTVAziendaCand;
end;

procedure TMainForm.curriculum1Click(Sender: TObject);
var x: string;
  i: integer;
begin
  if TVAziendaCand.Selected.level = 0 then exit;
  if not PosizionaAnag(longint(TVAziendaCand.Selected.Data)) then exit;
  Data.TTitoliStudio.Open;
  Data.TCorsiExtra.Open;
  Data.TLingueConosc.Open;
  Data.TEspLav.Open;
  CurriculumForm.ShowModal;
  Data.TTitoliStudio.Close;
  Data.TCorsiExtra.Close;
  Data.TLingueConosc.Close;
  Data.TEspLav.Close;
  MainForm.Pagecontrol5.ActivePage := MainForm.TSStatoTutti;
  DisegnaTVAziendaCand;
end;

procedure TMainForm.documenti1Click(Sender: TObject);
var xFile: string;
  xProcedi: boolean;
begin
  if TVAziendaCand.Selected.level = 0 then exit;
  ApriCV(longint(TVAziendaCand.Selected.Data));
end;

procedure TMainForm.fileWord1Click(Sender: TObject);
begin
  ApriFileWord(IntToStr(DammiCVNumero(longint(TVAziendaCand.Selected.Data))));
end;

procedure TMainForm.modificaattuale1Click(Sender: TObject);
begin
  if TVAziendaCand.Selected.Level > 0 then begin
    if TVAziendaCand.Selected.ImageIndex = 8 then begin
      Data2.QTemp.SQL.clear;
      Data2.QTemp.SQL.Add('update EsperienzeLavorative set Attuale=0 where IDAnagrafica=' + IntToStr(longint(TVAziendaCand.Selected.Data)) + ' and IDAzienda=' + DataRicerche.QAziendeID.asString);
      Data2.QTemp.ExecSQL;
    end else begin
      Data2.QTemp.SQL.clear;
      Data2.QTemp.SQL.Add('update EsperienzeLavorative set Attuale=1 where IDAnagrafica=' + IntToStr(longint(TVAziendaCand.Selected.Data)) + ' and IDAzienda=' + DataRicerche.QAziendeID.asString);
      Data2.QTemp.ExecSQL;
    end;
    DisegnaTVAziendaCand;
  end;
end;

procedure TMainForm.BAzSearchClick(Sender: TObject);
var xArrayStd, xArrayNew: array[1..100] of integer;
  i, k, j: integer;
begin
  SelClienteForm := TSelClienteForm.create(self);
  SelClienteForm.ShowModal;
  if SelClienteForm.ModalResult = mrOK then begin
    if (SelClienteForm.TClienti.Active) and (not SelClienteForm.TClienti.IsEmpty) then begin
      with DataRicerche do begin
        QOrgNew.Close;
        QAziende.close;
        QAziende.SQL.Text := 'select EBC_Clienti.* ,EBC_attivita.Attivita from EBC_Clienti,EBC_attivita ' +
          'where EBC_Clienti.IDAttivita=EBC_attivita.ID and EBC_Clienti.ID=' + SelClienteForm.TClientiID.asString;
        QAziende.Open;
        QOrgNew.Open;
        // se non c'� l'organigramma ==> proponi quello standard
        if QOrgNew.IsEmpty then begin
          if MessageDlg('Per questa azienda non risulta impostato l''organigramma ' + chr(13) +
            'VUOI ASSOCIARE L''ORGANIGRAMMA STANDARD ?', mtWarning, [mbYes, mbNo], 0) = mrYes then begin
            Data.Qtemp.Close;
            Data.Qtemp.SQL.text := 'select * from OrgStandard order by PARENT';
            Data.Qtemp.Open;
            k := 1;
            while not Data.Qtemp.EOF do begin
              Data.DB.BeginTrans;
              try
                Data.Q1.Close;
                Data.Q1.SQL.text := 'insert into Organigramma (Tipo,Descrizione,DescBreve,Interim,InStaff,' +
                  '                                       WIDTH,HEIGHT,TYPE,COLOR,IMAGE,IMAGEALIGN,ORDINE,ALIGN,IDCliente) ' +
                  'values (:xTipo,:xDescrizione,:xDescBreve,:xInterim,:xInStaff,:xWIDTH,:xHEIGHT,:xTYPE,' +
                  '        :xCOLOR,:xIMAGE,:xIMAGEALIGN,:xORDINE,:xALIGN,:xIDCliente)';
                Data.Q1.ParamByName('xTipo').asString := Data.Qtemp.FieldByName('Tipo').asString;
                Data.Q1.ParamByName('xDescrizione').asString := Data.Qtemp.FieldByName('Descrizione').asString;
                Data.Q1.ParamByName('xDescBreve').asString := Data.Qtemp.FieldByName('DescBreve').asString;
                Data.Q1.ParamByName('xInterim').asBoolean := Data.Qtemp.FieldByName('Interim').asBoolean;
                Data.Q1.ParamByName('xInStaff').asBoolean := Data.Qtemp.FieldByName('InStaff').asBoolean;
                Data.Q1.ParamByName('xWIDTH').asInteger := Data.Qtemp.FieldByName('WIDTH').asInteger;
                Data.Q1.ParamByName('xHEIGHT').asInteger := Data.Qtemp.FieldByName('HEIGHT').asInteger;
                Data.Q1.ParamByName('xTYPE').asString := Data.Qtemp.FieldByName('TYPE').asstring;
                Data.Q1.ParamByName('xCOLOR').asInteger := Data.Qtemp.FieldByName('COLOR').asInteger;
                Data.Q1.ParamByName('xIMAGE').asInteger := Data.Qtemp.FieldByName('IMAGE').asInteger;
                Data.Q1.ParamByName('xIMAGEALIGN').asString := Data.Qtemp.FieldByName('IMAGEALIGN').asstring;
                Data.Q1.ParamByName('xORDINE').asInteger := Data.Qtemp.FieldByName('ORDINE').asInteger;
                Data.Q1.ParamByName('xALIGN').asString := Data.Qtemp.FieldByName('ALIGN').asString;
                Data.Q1.ParamByName('xIDCliente').asInteger := SelClienteForm.TClientiID.Value;
                Data.Q1.ExecSQL;
                //
                Data.Q2.SQL.text := 'select @@IDENTITY as LastID';
                Data.Q2.Open;
                xArrayStd[k] := Data.QTemp.FieldByName('ID').asInteger;
                xArrayNew[k] := Data.Q2.FieldByName('LastID').asInteger;
                Data.Q2.Close;

                Data.DB.CommitTrans;
              except
                Data.DB.RollbackTrans;
                MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
              end;
              inc(k);
              Data.Qtemp.Next;
            end;
            // PARENT
            Data.Q2.Close;
            Data.Q2.SQL.text := 'select ID from Organigramma where IDCliente=' + SelClienteForm.TClientiID.asString + ' order by ID';
            Data.Q2.Open;
            Data.Qtemp.First;
            Data.Q2.Next;
            Data.Qtemp.Next;
            k := 1;
            while not Data.Qtemp.EOF do begin
              // trova nuovo PARENT nell'xArrayNew
              for i := 1 to 100 do
                if xArrayStd[i] = Data.QTemp.FieldByName('PARENT').asInteger then break;

              Data.DB.BeginTrans;
              try
                Data.Q1.Close;
                Data.Q1.SQL.text := 'update Organigramma set PARENT=:xPARENT ' +
                  'where ID=' + IntToStr(Data.Q2.FieldByName('ID').asInteger);
                Data.Q1.ParamByName('xPARENT').asInteger := xArrayNew[i];
                Data.Q1.ExecSQL;
                Data.DB.CommitTrans;
              except
                Data.DB.RollbackTrans;
                MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
              end;
              inc(k);
              Data.Qtemp.Next;
              Data.Q2.Next;
            end;
            Data.Qtemp.Close;
            Data.Q2.Close;
          end;
          QOrgNew.Close;
          QOrgNew.Open;
        end;
        PColor.Color := DataRicerche.QOrgNewCOLOR.Value;
      end;
      DisegnaTVAziendaCand;
    end;
  end;
  SelClienteForm.Free;
end;

procedure TMainForm.eliminasoggetto2Click(Sender: TObject);
var Node: TTreeNode;
begin
  Node := TVAziendaCand.Selected;
  if Node <> nil then begin
    if longint(TVAziendaCand.Selected.Data) > 0 then begin
      if MessageDlg('Sei sicuro di voler eliminare l''associazione ?', mtWarning, [mbNo, mbYes], 0) = mrNo then exit;
      Data2.QTemp.SQL.clear;
      Data2.QTemp.SQL.Add('delete from EsperienzeLavorative where IDAnagrafica=' + IntToStr(longint(TVAziendaCand.Selected.Data)) + ' and IDAzienda=' + DataRicerche.QAziendeID.asString);
      Data2.QTemp.ExecSQL;
      DataRicerche.QCandAzienda.Close;
      DataRicerche.QCandAzienda.Open;
      DisegnaTVAziendaCand;
    end;
  end;
end;

procedure TMainForm.BNotaSpeseNewClick(Sender: TObject);
begin
  if not CheckProfile('150') then Exit;
  NotaSpeseForm := TNotaSpeseForm.create(self);
  NotaSpeseForm.DEData.Date := Date;
  NotaSpeseForm.ShowModal;
  if NotaSpeseForm.ModalResult = mrOK then begin
    Data2.Qtemp.SQL.clear;
    Data2.Qtemp.SQL.Add('insert into RicNoteSpese (IDCliente,IDRicerca,IDUtente,IDAnagrafica,Data,Descrizione,');
    Data2.Qtemp.SQL.Add('Automobile,Autostrada,Aereotreno,Taxi,Albergo,Ristorantebar,Parking,AltroDesc,AltroImporto,IDFattura)');
    if NotaSpeseForm.RGTipoSogg.ItemIndex = 0 then
      Data2.Qtemp.SQL.Add('values (' + Data2.TEBCclientiID.AsString + ',' +
        IntToStr(NotaSpeseForm.xIDRicerca) + ',' +
        '0' + ',' +
        IntToStr(NotaSpeseForm.xIDSoggetto) + ',' +
        ':xData,' +
        '''' + NotaSpeseForm.EDescrizione.text + ''',' +
        FloatToStr(NotaSpeseForm.RxCalcEdit1.Value) + ',' +
        FloatToStr(NotaSpeseForm.RxCalcEdit2.Value) + ',' +
        FloatToStr(NotaSpeseForm.RxCalcEdit3.Value) + ',' +
        FloatToStr(NotaSpeseForm.RxCalcEdit4.Value) + ',' +
        FloatToStr(NotaSpeseForm.RxCalcEdit5.Value) + ',' +
        FloatToStr(NotaSpeseForm.RxCalcEdit6.Value) + ',' +
        FloatToStr(NotaSpeseForm.RxCalcEdit7.Value) + ',' +
        '''' + NotaSpeseForm.EAltroDesc.text + ''',' +
        FloatToStr(NotaSpeseForm.RxCalcEdit8.Value) + ',' +
        IntToStr(NotaSpeseForm.xIDFattura) + ')')
    else
      Data2.Qtemp.SQL.Add('values (' + Data2.TEBCclientiID.AsString + ',' +
        IntToStr(NotaSpeseForm.xIDRicerca) + ',' +
        IntToStr(NotaSpeseForm.xIDSoggetto) + ',' +
        '0,' +
        ':xData,' +
        '''' + NotaSpeseForm.EDescrizione.text + ''',' +
        FloatToStr(NotaSpeseForm.RxCalcEdit1.Value) + ',' +
        FloatToStr(NotaSpeseForm.RxCalcEdit2.Value) + ',' +
        FloatToStr(NotaSpeseForm.RxCalcEdit3.Value) + ',' +
        FloatToStr(NotaSpeseForm.RxCalcEdit4.Value) + ',' +
        FloatToStr(NotaSpeseForm.RxCalcEdit5.Value) + ',' +
        FloatToStr(NotaSpeseForm.RxCalcEdit6.Value) + ',' +
        FloatToStr(NotaSpeseForm.RxCalcEdit7.Value) + ',' +
        '''' + NotaSpeseForm.EAltroDesc.text + ''',' +
        FloatToStr(NotaSpeseForm.RxCalcEdit8.Value) + ',' +
        IntToStr(NotaSpeseForm.xIDFattura) + ')');
    Data2.Qtemp.ParamByName('xData').asdateTime := NotaSpeseForm.DEData.Date;
    Data2.Qtemp.execSQL;
    Data2.TNoteSpeseCli.Close;
    Data2.TNoteSpeseCli.open;
  end;
  NotaSpeseForm.Free;
end;

procedure TMainForm.BNotaSpeseModClick(Sender: TObject);
begin
  if not CheckProfile('151') then Exit;
  NotaSpeseForm := TNotaSpeseForm.create(self);
  // riempimento campi
  if Data2.TNoteSpeseCliIDRicerca.asString <> '' then begin
    NotaSpeseForm.xIDRicerca := Data2.TNoteSpeseCliIDRicerca.Value;
    Data2.QTemp.SQL.text := 'select Mansioni.Descrizione Ruolo from EBC_Ricerche,Mansioni ' +
      'where EBC_Ricerche.IDMansione=Mansioni.ID and EBC_Ricerche.ID=' + Data2.TNoteSpeseCliIDRicerca.asString;
    Data2.QTemp.Open;
    NotaSpeseForm.ERicRif.text := Data2.TNoteSpeseCliRifRicerca.asString;
    NotaSpeseForm.ERicRuolo.text := Data2.QTemp.FieldByName('Ruolo').asString;
    Data2.QTemp.Close;
  end else begin
    NotaSpeseForm.xIDRicerca := 0;
    NotaSpeseForm.ERicRif.text := '';
    NotaSpeseForm.ERicRuolo.text := '';
  end;
  if Data2.TNoteSpeseCliIDAnagrafica.asString <> '0' then begin
    NotaSpeseForm.RGTipoSogg.ItemIndex := 0;
    NotaSpeseForm.xIDSoggetto := Data2.TNoteSpeseCliIDAnagrafica.Value;
    NotaSpeseForm.ESoggCogn.text := Data2.TNoteSpeseCliCognome.Value;
    NotaSpeseForm.ESoggNome.text := Data2.TNoteSpeseCliNome.Value;
  end else begin
    NotaSpeseForm.RGTipoSogg.ItemIndex := 1;
    NotaSpeseForm.xIDSoggetto := Data2.TNoteSpeseCliIDUtente.Value;
    NotaSpeseForm.ESoggCogn.text := Data2.TNoteSpeseCliUtente.Value;
    NotaSpeseForm.ESoggNome.text := '';
  end;
  NotaSpeseForm.DEData.Date := Data2.TNoteSpeseCliData.Value;
  NotaSpeseForm.EDescrizione.text := Data2.TNoteSpeseCliDescrizione.Value;
  NotaSpeseForm.RxCalcEdit1.Value := Data2.TNoteSpeseCliAutomobile.Value;
  NotaSpeseForm.RxCalcEdit2.Value := Data2.TNoteSpeseCliAutostrada.Value;
  NotaSpeseForm.RxCalcEdit3.Value := Data2.TNoteSpeseCliAereoTreno.Value;
  NotaSpeseForm.RxCalcEdit4.Value := Data2.TNoteSpeseCliTaxi.Value;
  NotaSpeseForm.RxCalcEdit5.Value := Data2.TNoteSpeseCliAlbergo.Value;
  NotaSpeseForm.RxCalcEdit6.Value := Data2.TNoteSpeseCliRistoranteBar.Value;
  NotaSpeseForm.RxCalcEdit7.Value := Data2.TNoteSpeseCliParking.Value;
  NotaSpeseForm.EAltroDesc.text := Data2.TNoteSpeseCliAltroDesc.asString;
  NotaSpeseForm.RxCalcEdit8.Value := Data2.TNoteSpeseCliAltroImporto.Value;
  if Data2.TNoteSpeseCliIDFattura.asstring <> '' then begin
    NotaSpeseForm.xIDFattura := Data2.TNoteSpeseCliIDFattura.Value;
    Data2.QTemp.SQL.text := 'select Progressivo,Data from Fatture ' +
      'where Fatture.ID=' + Data2.TNoteSpeseCliIDFattura.asString;
    Data2.QTemp.Open;
    NotaSpeseForm.EFattNum.text := Data2.QTemp.FieldByName('Progressivo').asString;
    NotaSpeseForm.EFattData.text := Data2.QTemp.FieldByName('Data').asString;
    Data2.QTemp.Close;
  end else begin
    NotaSpeseForm.xIDFattura := 0;
    NotaSpeseForm.EFattNum.Text := '';
    NotaSpeseForm.EFattData.text := '';
  end;
  NotaSpeseForm.ShowModal;
  if NotaSpeseForm.ModalResult = mrOK then begin
    Data2.Qtemp.SQL.clear;
    Data2.Qtemp.SQL.Add('update RicNoteSpese set IDRicerca=:xIDRicerca,IDUtente=:xIDUtente,IDAnagrafica=:xIDAnagrafica,Data=:xData,Descrizione=:xDescrizione,');
    Data2.Qtemp.SQL.Add('Automobile=:xAutomobile,Autostrada=:xAutostrada,Aereotreno=:xAereotreno,Taxi=:xTaxi,Albergo=:xAlbergo,Ristorantebar=:xRistorantebar,' +
      'Parking=:xParking,AltroDesc=:xAltroDesc,AltroImporto=:xAltroImporto,IDFattura=:xIDFattura where RicNoteSpese.ID=' + Data2.TNoteSpeseCliID.asString);
    Data2.Qtemp.ParamByName('xIDRicerca').asInteger := NotaSpeseForm.xIDRicerca;
    if NotaSpeseForm.RGTipoSogg.ItemIndex = 0 then begin
      Data2.Qtemp.ParamByName('xIDUtente').asInteger := 0;
      Data2.Qtemp.ParamByName('xIDAnagrafica').asInteger := NotaSpeseForm.xIDSoggetto;
    end else begin
      Data2.Qtemp.ParamByName('xIDUtente').asInteger := NotaSpeseForm.xIDSoggetto;
      Data2.Qtemp.ParamByName('xIDAnagrafica').asInteger := 0;
    end;
    Data2.Qtemp.ParamByName('xData').asDateTime := NotaSpeseForm.DEData.Date;
    Data2.Qtemp.ParamByName('xDescrizione').asString := NotaSpeseForm.EDescrizione.text;
    Data2.Qtemp.ParamByName('xAutomobile').AsFloat := NotaSpeseForm.RxCalcEdit1.Value;
    Data2.Qtemp.ParamByName('xAutostrada').asFloat := NotaSpeseForm.RxCalcEdit2.Value;
    Data2.Qtemp.ParamByName('xAereotreno').asFloat := NotaSpeseForm.RxCalcEdit3.Value;
    Data2.Qtemp.ParamByName('xTaxi').asFloat := NotaSpeseForm.RxCalcEdit4.Value;
    Data2.Qtemp.ParamByName('xAlbergo').asFloat := NotaSpeseForm.RxCalcEdit5.Value;
    Data2.Qtemp.ParamByName('xRistorantebar').asFloat := NotaSpeseForm.RxCalcEdit6.Value;
    Data2.Qtemp.ParamByName('xParking').asFloat := NotaSpeseForm.RxCalcEdit7.Value;
    Data2.Qtemp.ParamByName('xAltroDesc').asString := NotaSpeseForm.EAltroDesc.text;
    Data2.Qtemp.ParamByName('xAltroImporto').asFloat := NotaSpeseForm.RxCalcEdit8.Value;
    Data2.Qtemp.ParamByName('xIDFattura').asInteger := NotaSpeseForm.xIDFattura;
    Data2.Qtemp.execSQL;
    Data2.TNoteSpeseCli.Close;
    Data2.TNoteSpeseCli.Open;
  end;
  NotaSpeseForm.Free;
end;

procedure TMainForm.BNotaSpeseDelClick(Sender: TObject);
begin
  if not CheckProfile('152') then Exit;
  if Data2.TNoteSpeseCli.RecordCount > 0 then
    if MessageDlg('Sei sicuro di voler eliminare la nota spesa selezionata ?', mtWarning,
      [mbNo, mbYes], 0) = mrYes then begin
      with Data do begin
        DB.BeginTrans;
        try
          Q1.Close;
          Q1.SQL.text := 'delete from RicNoteSpese where ID=' + Data2.TNoteSpeseCliID.asString;
          Q1.ExecSQL;
          DB.CommitTrans;
        except
          DB.RollbackTrans;
          MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
        end;
      end;
    end;
  Data2.TNoteSpeseCli.Close;
  Data2.TNoteSpeseCli.Open;
end;

procedure TMainForm.PCClientiChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
  if ((not Data2.TEBCClienti.Active) or (Data2.TEBCClienti.RecordCount = 0))
    and (PCClienti.ActivePage = TSClientiDett) then
    AllowChange := False
  else begin
    PanCliente.Visible := False;
    PanCliContatti.Visible := False;
    PanCliPag.Visible := False;
    xPaginaCliPrec := PCClienti.ActivePage;
  end;
end;

procedure TMainForm.PCAnnunciChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
  xPaginaAnnunciPrec := PCAnnunci.ActivePage;
end;

procedure TMainForm.SpeedButton8Click(Sender: TObject);
var SavePlace: TBookmark;
begin
  if Data.TAnagraficaDubbiIns.value = '' then exit;
  if xUtenteAttualeTipo > 1 then begin
    MessageDlg('Utente non abilitato per togliere la segnalazione di dubbio sui ruoli', mtError, [mbOK], 0);
    Exit;
  end;
  if MessageDlg('Togliere segnalazione di dubbio sui ruoli ?', mtWarning, [mbYes, mbNo], 0) = mrYes then begin
    Data.xOKcheck := False;

    Data.TAnagrafica.Edit;
    Data.TAnagraficaDubbiIns.Value := '';
    Data.TAnagraficaIDUtenteMod.Value := xIDUtenteAttuale;
    Data.DB.BeginTrans;
    try
      Data.TAnagrafica.ApplyUpdates;
      Data.DB.CommitTrans;
    except
      Data.DB.RollbackTrans;
      MessageDlg('ERRORE SUL DATABASE:  operazione non completata', mtError, [mbOK], 0);
    end;
    Data.TAnagrafica.CommitUpdates;
    Data.xOKcheck := True;
  end;
end;

procedure TMainForm.bDubbiInsClick(Sender: TObject);
begin
  if not CheckProfile('055') then Exit;
  DubbiInsForm := TDubbiInsForm.create(self);
  DubbiInsForm.ShowModal;
  if (DubbiInsForm.ModalResult = mrOK) and (DubbiInsForm.QDubbi.RecordCount > 0) then begin
    PosizionaAnag(DubbiInsForm.QDubbiID.Value);
    PageControl5.ActivePage := TSStatoTutti;
    PageControl1.ActivePage := TSAnag;
    Pagecontrol2.ActivePage := TsAnagMans;
    LOBSchede.Down := True;
    LOBRicerche.Down := False;
    LOBClientiEBC.Down := False;
    LOBAnnunci.Down := False;
    TbAnag.Visible := True;
    TbClienti.Visible := False;
    xPaginaPrec := PageControl2.ActivePage;
    PageControl2Change(self);
  end;
  DubbiInsForm.Free;
end;

procedure TMainForm.BCompensoNewClick(Sender: TObject);
var xIDCompenso: integer;
begin
  if not CheckProfile('122') then Exit;
  CompensoForm := TCompensoForm.create(self);
  CompensoForm.ShowModal;
  if CompensoForm.ModalResult = mrOK then begin
    with Data do begin
      DB.BeginTrans;      
      try
        if CompensoForm.DEDataFatt.date = 0 then
          Data2.Qtemp.SQL.Text := 'insert into EBC_CompensiRicerche (IDRicerca,Tipo,Importo,Note) ' +
            ' values (:xIDRicerca,:xTipo,:xImporto,:xNote)'
        else
          Data2.Qtemp.SQL.Text := 'insert into EBC_CompensiRicerche (IDRicerca,Tipo,Importo,DataPrevFatt,Note) ' +
            ' values (:xIDRicerca,:xTipo,:xImporto,:xDataPrevFatt,:xNote)';
        Data2.Qtemp.ParamByName('xIDRicerca').asInteger := Data2.QRicClientiID.Value;
        Data2.Qtemp.ParamByName('xTipo').asString := CompensoForm.CBTipo.Text;
        Data2.Qtemp.ParamByName('xImporto').AsFloat := CompensoForm.RxImporto.Value;
        if CompensoForm.DEDataFatt.Date <> 0 then
          Data2.Qtemp.ParamByName('xDataPrevFatt').AsDateTime := CompensoForm.DEDataFatt.Date;
        Data2.Qtemp.ParamByName('xNote').asString := CompensoForm.ENote.Text;
        Data2.Qtemp.execSQL;

        // aggiornamento CONTROLLO DI GESTIONE (se c'�)
        if copy(Data.GlobalCheckBoxIndici.Value, 6, 1) = '1' then begin
          // ID compenso appena inserito
          Data.QTemp.Close;
          Data.QTemp.SQL.text := 'select @@IDENTITY as LastID';
          Data.QTemp.Open;
          xIDCompenso := Data.QTemp.FieldByName('LastID').asInteger;
          Data.QTemp.Close;
          // cerca contratto
          Data.Q1.Close;
          Data.Q1.SQL.text := 'select IDContrattoCG from EBC_Ricerche ' +
            'where ID=' + Data2.QRicClientiID.asString;
          Data.Q1.Open;
          // crea la voce di ricavo
          Data2.QTemp.SQL.Text := 'insert into CG_ContrattiRicavi (IDContratto,Tipo,ImportoPrev,Note,IDCompenso) ' +
            '  values (:xIDContratto,:xTipo,:xImportoPrev,:xNote,:xIDCompenso)';
          Data2.QTemp.ParamByName('xIDContratto').asInteger := Data.Q1.FieldByName('IDContrattoCG').AsInteger;
          Data2.QTemp.ParamByName('xTipo').asString := CompensoForm.CBTipo.Text;
          Data2.QTemp.ParamByName('xImportoPrev').asFloat := CompensoForm.RxImporto.Value;
          Data2.QTemp.ParamByName('xNote').asString := CompensoForm.ENote.Text;
          Data2.QTemp.ParamByName('xIDCompenso').asInteger := xIDCompenso;
          Data2.QTemp.execSQL;
          Data.Q1.Close;
        end;
        DB.CommitTrans;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
    Data2.TCompensiRic.Close;
    Data2.TCompensiRic.Open;
    Data2.QTotCompenso.Close;
    Data2.QTotCompenso.Open;
    Data2.QTotComp2.Close;
    Data2.QTotComp2.Open;
  end;
  CompensoForm.Free;
end;

procedure TMainForm.BCompensoModClick(Sender: TObject);
begin
  if not CheckProfile('123') then Exit;
  if Data2.TCompensiRic.RecordCount = 0 then exit;
  CompensoForm := TCompensoForm.create(self);
  CompensoForm.CBTipo.Text := Data2.TCompensiRicTipo.Value;
  CompensoForm.RxImporto.Value := Data2.TCompensiRicImporto.Value;
  CompensoForm.DEDataFatt.DAte := Data2.TCompensiRicDataPrevFatt.Value;
  CompensoForm.ENote.text := Data2.TCompensiRicNote.Value;
  CompensoForm.ShowModal;
  if CompensoForm.ModalResult = mrOK then begin
    with Data do begin
      DB.BeginTrans;
      try
        if CompensoForm.DEDataFatt.Date = 0 then
          Data2.Qtemp.SQL.Text := 'update EBC_CompensiRicerche ' +
            ' set Tipo=:xTipo,Importo=:xImporto,Note=:xNote ' +
            ' where ID=' + Data2.TCompensiRicID.asString
        else
          Data2.Qtemp.SQL.Text := 'update EBC_CompensiRicerche ' +
            ' set Tipo=:xTipo,Importo=:xImporto,DataPrevFatt=:xDataPrevFatt,Note=:xNote ' +
            ' where ID=' + Data2.TCompensiRicID.asString;
        Data2.Qtemp.ParamByName('xTipo').asString := CompensoForm.CBTipo.Text;
        Data2.Qtemp.ParamByName('xImporto').AsFloat := CompensoForm.RxImporto.Value;
        if CompensoForm.DEDataFatt.date <> 0 then
          Data2.Qtemp.ParamByName('xDataPrevFatt').AsDateTime := CompensoForm.DEDataFatt.Date;
        Data2.Qtemp.ParamByName('xNote').asString := CompensoForm.ENote.Text;
        Data2.Qtemp.execSQL;

        // aggiornamento CONTROLLO DI GESTIONE (se c'�)
        if copy(Data.GlobalCheckBoxIndici.Value, 6, 1) = '1' then begin
          // aggiorna la voce di ricavo
          Data2.QTemp.SQL.Text := 'update CG_ContrattiRicavi SET Tipo=:xTipo, ImportoPrev=:xImportoPrev, Note=:xNote ' +
            ' where IDCompenso=:xIDCompenso';
          Data2.QTemp.ParamByName('xTipo').asString := CompensoForm.CBTipo.Text;
          Data2.QTemp.ParamByName('xImportoPrev').asFloat := CompensoForm.RxImporto.Value;
          Data2.QTemp.ParamByName('xNote').asString := CompensoForm.ENote.Text;
          Data2.QTemp.ParamByName('xIDCompenso').asInteger := Data2.TCompensiRicID.Value;
          Data2.QTemp.execSQL;
        end;

        DB.CommitTrans;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
    Data2.TCompensiRic.Close;
    Data2.TCompensiRic.open;
    Data2.QTotCompenso.Close;
    Data2.QTotCompenso.Open;
    Data2.QTotComp2.Close;
    Data2.QTotComp2.Open;
  end;
  CompensoForm.Free;
end;

procedure TMainForm.BCompensoDelClick(Sender: TObject);
begin
  if not CheckProfile('124') then Exit;
  if Data2.TCompensiRic.RecordCount = 0 then exit;
  if MessageDlg('Sei sicuro di voler eliminare il compenso selezionato ?', mtWarning,
    [mbNo, mbYes], 0) = mrYes then begin
    with Data do begin
      DB.BeginTrans;
      try
        Data2.Qtemp.Close;
        Data2.Qtemp.SQL.Text := 'delete from EBC_CompensiRicerche ' +
          ' where ID=' + Data2.TCompensiRicID.asString;
        Data2.Qtemp.execSQL;

        // aggiornamento CONTROLLO DI GESTIONE (se c'�)
        if copy(Data.GlobalCheckBoxIndici.Value, 6, 1) = '1' then begin
          // aggiorna la voce di ricavo
          Data2.QTemp.SQL.Text := 'delete from CG_ContrattiRicavi ' +
            ' where IDCompenso=:xIDCompenso';
          Data2.QTemp.ParamByName('xIDCompenso').asInteger := Data2.TCompensiRicID.Value;
          Data2.QTemp.execSQL;
        end;

        DB.CommitTrans;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
    Data2.TCompensiRic.Close;
    Data2.TCompensiRic.Open;
    Data2.QTotCompenso.Close;
    Data2.QTotCompenso.Open;
    Data2.QTotComp2.Close;
    Data2.QTotComp2.Open;
  end;
end;

procedure TMainForm.BAssFattDelClick(Sender: TObject);
begin
  if not CheckProfile('125') then Exit;
  if (Data2.TCompensiRic.RecordCount = 0) or
    (Data2.TCompensiRicIDFattura.asString = '') then exit;
  if MessageDlg('Sei sicuro di voler eliminare l''associazione ?', mtWarning, [mbOK], 0) = mrNo then exit;
  with Data do begin
    DB.BeginTrans;
    try
      Data2.Qtemp.Close;
      Data2.Qtemp.SQL.Text := 'update EBC_CompensiRicerche ' +
        ' set IDFattura=null where ID=' + Data2.TCompensiRicID.asString;
      Data2.Qtemp.ExecSQL;

      DB.CommitTrans;
    except
      DB.RollbackTrans;
      MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
    end;
  end;
  Data2.TCompensiRic.Close;
  Data2.TCompensiRic.Open;
end;

procedure TMainForm.SpeedButton11Click(Sender: TObject);
begin
  if not (DataAnnunci.DsAnnunci.state = dsEdit) then DataAnnunci.TAnnunci.Edit;
  DataAnnunci.TAnnunciIDFattura.asString := '';
end;

procedure TMainForm.BRisNewClick(Sender: TObject);
begin
  if not CheckProfile('7900') then Exit;
  RisorsaForm := TRisorsaForm.create(self);
  RisorsaForm.ShowModal;
  if RisorsaForm.ModalResult = mrOK then begin
    Data.TRisorse.Insert;
    Data.TRisorseRisorsa.value := RisorsaForm.ERisorsa.text;
    if RisorsaForm.CBColore.text = 'verde' then
      Data.TRisorseColor.value := 'clLime';
    if RisorsaForm.CBColore.text = 'rosso' then
      Data.TRisorseColor.value := 'clRed';
    if RisorsaForm.CBColore.text = 'giallo' then
      Data.TRisorseColor.value := 'clYellow';
    if RisorsaForm.CBColore.text = 'azzurro' then
      Data.TRisorseColor.value := 'clAqua';
    Data.TRisorse.Post;
  end;
  RisorsaForm.Free;
end;

procedure TMainForm.BRisModClick(Sender: TObject);
begin
  if not CheckProfile('7900') then Exit;
  RisorsaForm := TRisorsaForm.create(self);
  RisorsaForm.ERisorsa.text := Data.TRisorseRisorsa.value;
  if Data.TRisorseColor.value = 'clLime' then RisorsaForm.CBColore.text := 'verde';
  if Data.TRisorseColor.value = 'clRed' then RisorsaForm.CBColore.text := 'rosso';
  if Data.TRisorseColor.value = 'clYellow' then RisorsaForm.CBColore.text := 'giallo';
  if Data.TRisorseColor.value = 'clAqua' then RisorsaForm.CBColore.text := 'azzurro';
  RisorsaForm.ShowModal;
  if RisorsaForm.ModalResult = mrOK then begin
    Data.TRisorse.Edit;
    Data.TRisorseRisorsa.value := RisorsaForm.ERisorsa.text;
    if RisorsaForm.CBColore.text = 'verde' then
      Data.TRisorseColor.value := 'clLime';
    if RisorsaForm.CBColore.text = 'rosso' then
      Data.TRisorseColor.value := 'clRed';
    if RisorsaForm.CBColore.text = 'giallo' then
      Data.TRisorseColor.value := 'clYellow';
    if RisorsaForm.CBColore.text = 'azzurro' then
      Data.TRisorseColor.value := 'clAqua';
    Data.TRisorse.Post;
  end;
  RisorsaForm.Free;
end;

procedure TMainForm.BRisDelClick(Sender: TObject);
begin
  if not CheckProfile('7900') then Exit;
  if not VerifIntegrRef('Agenda', 'IDRisorsa', Data.TRisorseID.AsString) then exit;
  if Data.TRisorse.RecordCount > 0 then
    if MessageDlg('Sei sicuro di voler eliminarlo ?', mtWarning,
      [mbNo, mbYes], 0) = mrYes then Data.TRisorse.Delete;
end;

procedure TMainForm.Edit1KeyPress(Sender: TObject; var Key: Char);
begin
  if key = chr(13) then BAnagSearchClick(self);
end;

procedure TMainForm.BAnagSearchClick(Sender: TObject);
var i: integer;
begin
  if Length(Edit1.text) < 2 then begin
    ShowMessage('Inserire almeno due iniziali per il cognome');
    exit;
  end;
  CaricaApriAnag(Edit1.Text, ENome.text);
  if not Data.TAnagrafica.IsEmpty then
    for i := 1 to PageControl2.PageCount - 1 do
      PageControl2.Pages[i].TabVisible := True;
  Data.xControlla := True;
  SettaDiritti(Data.TAnagraficaID.Value);
  if not CheckProfile('00', false) then PanAnag.Visible := False
  else PanAnag.Visible := True;
  // Utente associato al candidato
  if (Data.TAnagraficaIDUtente.asString <> '') and (Data.TAnagraficaIDUtente.Value > 0) then begin
    Data.Q1.Close;
    Data.Q1.SQL.text := 'select Nominativo from Users where ID=' + Data.TAnagraficaIDUtente.asString;
    Data.Q1.Open;
    ECandUtente.Text := Data.Q1.FieldbyName('Nominativo').asString;
    Data.Q1.Close;
  end else ECandUtente.Text := '';
  // proprietaCV
  if (Data.TAnagraficaIDProprietaCV.asString = '') or (Data.TAnagraficaIDProprietaCV.Value = 0) then
    EPropCV.Text := Data.GlobalNomeAzienda.Value
  else begin
    Data.Q1.Close;
    Data.Q1.SQL.text := 'select Descrizione from EBC_Clienti where ID=' + Data.TAnagraficaIDProprietaCV.asString;
    Data.Q1.Open;
    EPropCV.text := Data.Q1.FieldbyName('Descrizione').asString;
    Data.Q1.Close;
  end;
  //LTotCand.Caption:=IntToStr(Data.TAnagrafica.RecordCount);
end;

procedure TMainForm.TbBContattiCliModClick(Sender: TObject);
var xID: integer;
begin
  if Data2.TContattiClienti.IsEmpty then exit;
  if not CheckProfile('103') then Exit;
  ContattoCliForm := TContattoCliForm.create(self);
  ContattoCliForm.ETitolo.text := Data2.TContattiClientiTitolo.value;
  ContattoCliForm.EContatto.text := Data2.TContattiClientiContatto.value;
  ContattoCliForm.ETelefono.text := Data2.TContattiClientiTelefono.value;
  ContattoCliForm.EFax.text := Data2.TContattiClientiFax.value;
  ContattoCliForm.EEmail.text := Data2.TContattiClientiEmail.value;
  ContattoCliForm.ENote.text := Data2.TContattiClientiNote.Value;
  ContattoCliForm.ShowModal;
  if ContattoCliForm.ModalResult = mrOK then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'update EBC_ContattiClienti set Contatto=:xContatto,Titolo=:xTitolo,' +
          'Telefono=:xTelefono,Fax=:xFax,Email=:xEmail,Note=:xNote,DataIns=:xDataIns,IDUtente=:xIDUtente ' +
          'where ID=' + Data2.TContattiClientiID.asString;
        Q1.ParamByName('xTitolo').asString := ContattoCliForm.ETitolo.text;
        Q1.ParamByName('xContatto').asString := ContattoCliForm.EContatto.text;
        Q1.ParamByName('xTelefono').asString := ContattoCliForm.ETelefono.text;
        Q1.ParamByName('xFax').asString := ContattoCliForm.EFax.text;
        Q1.ParamByName('xEmail').asString := ContattoCliForm.EEmail.text;
        Q1.ParamByName('xNote').asString := ContattoCliForm.ENote.text;
        Q1.ParamByName('xDataIns').asDateTime := Date;
        Q1.ParamByName('xIDUtente').asInteger := xIDUtenteAttuale;
        Q1.ExecSQL;
        DB.CommitTrans;
        xID := Data2.TContattiClientiID.Value;
        Data2.TContattiClienti.Close;
        Data2.TContattiClienti.Open;
        Data2.TContattiClienti.Locate('ID', xID, []);
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
  end;
  ContattoCliForm.Free;
end;

procedure TMainForm.AggTotAnnuncio(xIDAnnuncio: integer);
var TempTotal: integer;
begin
  {aggiornamento TOTALE ANNUNCIO}
  with DataAnnunci do begin
    QAnnEdizDataXann.DisableControls;
    QAnnEdizDataXann.First;
    TempTotal := 0;
    while not QAnnEdizDataXann.EOF do
    begin
      TempTotal := TempTotal + QAnnEdizDataXannCostoLire.asInteger;
      QAnnEdizDataXann.Next;
    end;
    QAnnEdizDataXann.EnableControls;
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'update Ann_Annunci set TotaleLire=:xTotaleLire ' +
          'where ID=' + IntToStr(xIDAnnuncio);
        Q1.ParamByName('xTotaleLire').asInteger := Round(TempTotal);
        Q1.ExecSQL;
        DB.CommitTrans;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
  end;
end;

procedure TMainForm.RicalcoloCostoPubb(xIDAnnEdizData: integer);
var xGiorno: string;
  xPercScontoMod, xGiornoRPQ, xNumModuli, xIDEdizione, xNumParole: Integer;
  xData: TDateTime;
  xCostoLire: Real;
begin
  // (ri)calcolo costi per pubblicazione
  // Ann_AnnEdizData
  Data.Q1.Close;
  Data.Q1.SQL.Text := 'select Ann_AnnEdizData.Data,GiornoRPQ,NumModuli,NumParole,IDEdizione ' +
    'from Ann_AnnEdizData,Ann_Annunci,Ann_Edizioni ' +
    'where Ann_AnnEdizData.IDAnnuncio=Ann_Annunci.ID ' +
    'and Ann_AnnEdizData.IDEdizione=Ann_Edizioni.ID ' +
    'and Ann_AnnEdizData.ID=' + IntToStr(xIDAnnEdizData);
  Data.Q1.Open;
  xData := Data.Q1.FieldbyName('Data').asDateTime;
  xGiornoRPQ := Data.Q1.FieldbyName('GiornoRPQ').asInteger;
  xNumModuli := Data.Q1.FieldbyName('NumModuli').asInteger;
  xNumParole := Data.Q1.FieldbyName('NumParole').asInteger;
  xIDEdizione := Data.Q1.FieldbyName('IDEdizione').asInteger;
  Data.Q1.Close;

  xGiorno := 'Fer';
  if dayofweek(xData) = 1 then xGiorno := 'Fes';
  if DataAnnunci.TFestivi.FindKey([xData]) then xGiorno := 'Fes';
  if dayofweek(xData) = xGiornoRPQ then xGiorno := 'RPQ';
  // sconto moduli
  xPercScontoMod := 0;
  if xNumModuli > 0 then begin
    DataAnnunci.TScontiIDX.Open;
    DataAnnunci.TScontiIDX.FindNearest([xIDEdizione, xNumModuli]);
    if not DataAnnunci.TScontiIDX.EOF then xPercScontoMod := DataAnnunci.TScontiIDXPercSconto.Value;
    DataAnnunci.TScontiIDX.Close;
  end;

  // costi unitari
  with DataAnnunci do begin
    Q.close;
    Q.SQL.Clear;
    Q.SQL.text := 'select * from Ann_Edizioni where ID=' + IntToStr(xIDEdizione);
    Q.Open;
  end;
  Data.DB.BeginTrans;
  try
    xCostoLire := 0;
    if xGiorno = 'Fer' then
      xCostoLire := (DataAnnunci.QCostoParolaFerLire.Value * xNumParole) +
        ((DataAnnunci.QCostoModuloFerLire.Value * xNumModuli) -
        (DataAnnunci.QCostoModuloFerLire.Value * xNumModuli) *
        StrToFloat('0,' + IntToStr(xPercScontoMod)));
    if xGiorno = 'Fes' then
      xCostoLire := (DataAnnunci.QCostoParolaFestLire.Value * xNumParole) +
        ((DataAnnunci.QCostoModuloFestLire.Value * xNumModuli) -
        (DataAnnunci.QCostoModuloFestLire.Value * xNumModuli) *
        StrToFloat('0,' + IntToStr(xPercScontoMod)));
    if xGiorno = 'RPQ' then
      xCostoLire := (DataAnnunci.QCostoParolaRPQLire.Value * xNumParole) +
        ((DataAnnunci.QCostoModuloRPQLire.Value * xNumModuli) -
        (DataAnnunci.QCostoModuloRPQLire.Value * xNumModuli) *
        StrToFloat('0,' + IntToStr(xPercScontoMod)));
    Data.Q1.Close;
    Data.Q1.SQL.text := 'update Ann_AnnEdizData set CostoLire=:xCostoLire where ID=' + IntToStr(xIDAnnEdizData);
    Data.Q1.ParambyName('xCostoLire').AsFloat := xCostoLire;
    Data.Q1.ExecSQL;
    Data.DB.CommitTrans;
  except
    Data.DB.RollbackTrans;
    MessageDlg('ERRORE SUL DATABASE:  operazione non completata', mtError, [mbOK], 0);
  end;

end;

procedure TMainForm.BOrgNewNodoClick(Sender: TObject);
var Node: TdxOcNode;
  xIDAzienda: integer;
begin
  if not CheckProfile('60') then Exit;
  if not DataRicerche.QOrgNew.Active then exit;
  Node := DBTree.AddFirst(nil, nil);
  DataRicerche.QOrgNew.Close;
  DataRicerche.QOrgNew.Open;
  DBTree.FullExpand;
end;

procedure TMainForm.BOrgNewNewChildNodoClick(Sender: TObject);
var Node: TdxOcNode;
  I: integer;
begin
  if not CheckProfile('61') then Exit;
  if not DataRicerche.QOrgNew.Active then exit;
  DataRicerche.QOrgNew.DisableControls;
  if (DBTree.Selected <> nil) then
    Node := DBTree.AddChild(DBTree.Selected, nil)
  else Node := DBTree.Add(nil, nil);
  DataRicerche.QOrgNew.Close;
  DataRicerche.QOrgNew.Open;
  DataRicerche.QOrgNew.EnableControls;
  DBTree.FullExpand;
end;

procedure TMainForm.BOrgNewDelNodoClick(Sender: TObject);
begin
  if not CheckProfile('62') then Exit;
  if (not DataRicerche.QOrgNew.Active) or
    (DataRicerche.QOrgNew.RecordCount = 0) then exit;
  // controllo esistenza sotto-nodi
  if Dbtree.Selected.HasChildren then begin
    MessageDlg('Il nodo selezionato possiede sotto-nodi: impossibile cancellarlo', mtError, [mbOK], 0);
    exit;
  end;
  if MessageDlg('Sei sicuro di voler eliminare il nodo selezionato ?', mtWarning, [mbNo, mbYes], 0) = mrNo then exit;
  DataRicerche.QOrgNew.DisableControls;
  with Data do begin
    DB.BeginTrans;
    try
      Q1.Close;
      Q1.SQL.text := 'delete from Organigramma where ID=' + DataRicerche.QOrgNewID.asString;
      Q1.ExecSQL;
      DB.CommitTrans;
    except
      DB.RollbackTrans;
      MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
    end;
  end;
  DataRicerche.QOrgNew.Close;
  DataRicerche.QOrgNew.Open;
  DataRicerche.QOrgNew.EnableControls;
  DBTree.FullExpand;
end;

procedure TMainForm.PColorClick(Sender: TObject);
begin
  if ColorDialog.Execute then begin
    PColor.Color := ColorDialog.Color;
    DataRicerche.QOrgNew.Edit;
    DataRicerche.QOrgNewCOLOR.Value := PColor.Color;
    DBTree.Selected.Color := PColor.Color;
  end;
end;

procedure TMainForm.DBTreeCreateNode(Sender: TObject; Node: TdxOcNode);
begin
  with Node, DataRicerche.QOrgNew do
  begin
    if FindField('width').AsInteger > 50 then
      Width := FindField('width').AsInteger;
    if FindField('height').AsInteger > 50 then
      Height := FindField('height').AsInteger;
    Shape := GetShape(FindField('type').AsString);
    Color := FindField('color').AsInteger;
    Node.ChildAlign := GetNodeAlign(FindField('Align').AsString);
    Node.ImageAlign := GetImageAlign(FindField('ImageAlign').AsString);
  end;
end;

function TMainForm.GetShape(ShapeName: string): TdxOcShape;
const ShapeArray: array[0..3] of string = ('Rectange', 'Round Rect', 'Ellipse', 'Diamond');
var i: integer;
begin
  Result := TdxOcShape(0);
  for i := 0 to 3 do
    if AnsiUpperCase(ShapeArray[i]) = AnsiUpperCase(ShapeName) then begin
      Result := TdxOcShape(i);
      break;
    end;
end;

function TMainForm.GetNodeAlign(AlignName: string): TdxOcNodeAlign;
const AlignArray: array[0..2] of string = ('Left', 'Center', 'Right');
var i: integer;
begin
  Result := TdxOcNodeAlign(0);
  for i := 0 to 3 do
    if AnsiUpperCase(AlignArray[i]) = AnsiUpperCase(AlignName) then begin
      Result := TdxOcNodeAlign(i);
      break;
    end;
end;

function TMainForm.GetImageAlign(AlignName: string): TdxOcImageAlign;
const AlignArray: array[0..12] of string = (
    'None',
    'Left-Top', 'Left-Center', 'Left-Bottom',
    'Right-Top', 'Right-Center', 'Right-Bottom',
    'Top-Left', 'Top-Center', 'Top-Right',
    'Bottom-Left', 'Bottom-Center', 'Bottom-Right'
    );
var i: integer;
begin
  Result := TdxOcImageAlign(0);
  for i := 0 to 12 do
    if AnsiUpperCase(AlignArray[i]) = AnsiUpperCase(AlignName) then begin
      Result := TdxOcImageAlign(i);
      break;
    end;
end;

procedure TMainForm.DBGrid20TitleClick(Column: TColumn);
begin
  with Data2.TCompetenze do begin
    Close;
    SQL.Text := 'select Competenze.*,AreeCompetenze.Descrizione Area ' +
      'from Competenze,AreeCompetenze  ' +
      'where Competenze.IDArea=AreeCompetenze.ID ';
    if Column = DBGrid20.Columns[0] then
      SQL.Add('order by Competenze.Descrizione');
    if Column = DBGrid20.Columns[1] then
      SQL.Add('order by Competenze.Tipologia');
    if Column = DBGrid20.Columns[2] then
      SQL.Add('order by AreeCompetenze.descrizione');
    Open;
  end;
end;

procedure TMainForm.BPropCVClick(Sender: TObject);
begin
  SelClienteForm := TSelClienteForm.create(self);
  SelClienteForm.ShowModal;
  if SelClienteForm.ModalResult = mrOK then begin
    if not (Data.DsAnagrafica.State = dsEdit) then Data.TAnagrafica.Edit;
    Data.TAnagraficaIDProprietaCV.Value := SelClienteForm.TClientiID.Value;
    TbBAnagOKClick(Self);
    EPropCV.text := SelClienteForm.TClientiDescrizione.Value;
  end else begin
    if MessageDlg('Associare la propriet� del CV a ' + Data.GlobalNomeAzienda.Value + ' ?', mtWarning, [mbYes, mbNo], 0) = mrYes then begin
      if not (Data.DsAnagrafica.State = dsEdit) then Data.TAnagrafica.Edit;
      Data.TAnagraficaIDProprietaCV.Value := 0;
      TbBAnagOKClick(Self);
      EPropCV.text := Data.GlobalNomeAzienda.Value;
    end;
  end;
  SelClienteForm.Free;
end;

procedure TMainForm.associaanodo1Click(Sender: TObject);
begin
  if TVAziendaCand.Selected.level = 0 then exit;
  if DataRicerche.QOrgNew.RecordCount = 0 then exit;
  // controllo associazione gi� presente
  if (DataRicerche.QOrgNewIDDipendente.asString <> '') and (DataRicerche.QOrgNewIDDipendente.value > 0) then
    if MessageDlg('Il nodo ' + DataRicerche.QOrgNewDescBreve.value + ' � gi� occupato - VUOI PROSEGUIRE ?', mtWarning, [mbYes, mbNo], 0) = mrNo then exit;
  // controllo candidato non attivo in azienda
  if LongInt(TVAziendaCand.Selected.Parent.Data) = 1 then
    if MessageDlg('ATTENZIONE:  Soggetto non attualmente in quest''azienda.' + chr(13) +
      'VUOI PROSEGUIRE (rendendo l''azienda quella attuale) ?', mtWarning, [mbYes, mbNo], 0) = mrNo then exit;

  if MessageDlg('Associare il soggetto ' + TVAziendaCand.Selected.text + chr(13) +
    'al nodo dell''organigramma ' + DataRicerche.QOrgNewDescBreve.value + ' ?', mtInformation, [mbYes, mbNo], 0) = mrYes then begin
    EliminaDipOrgForm := TEliminaDipOrgForm.create(self);
    EliminaDipOrgForm.Caption := TVAziendaCand.Selected.text + ' --> ' + DataRicerche.QOrgNewDescBreve.value;
    EliminaDipOrgForm.Label2.Caption := 'Dalla data:';
    EliminaDipOrgForm.Showmodal;
    if EliminaDipOrgForm.ModalResult = mrOK then begin
      DataRicerche.QOrgNew.DisableControls;
      with Data do begin
        DB.BeginTrans;
        try
          Q1.Close;
          Q1.SQL.text := 'update Organigramma set IDDipendente=' + IntToStr(longint(TVAziendaCand.Selected.Data)) +
            ' where ID=' + DataRicerche.QOrgNewID.asString;
          Q1.ExecSQL;
          // inserisci in storico posizioni
          Q1.Close;
          Q1.SQL.text := 'insert into StoricoAnagPosOrg (IDAnagrafica,IDOrganigramma,DallaData,IDAutorizzatoDa,Note) ' +
            ' values (:xIDAnagrafica,:xIDOrganigramma,:xDallaData,:xIDAutorizzatoDa,:xNote)';
          Q1.ParamByName('xIDAnagrafica').asInteger := longint(TVAziendaCand.Selected.Data);
          Q1.ParamByName('xIDOrganigramma').asInteger := DataRicerche.QOrgNewID.Value;
          Q1.ParamByName('xDallaData').asDateTime := EliminaDipOrgForm.DEData.Date;
          Q1.ParamByName('xIDAutorizzatoDa').asInteger := EliminaDipOrgForm.xIDAutorizzatoDa;
          Q1.ParamByName('xNote').asString := EliminaDipOrgForm.ENote.Text;
          Q1.ExecSQL;

          // associa (se c'�) il ruolo all'esperienza lavorativa
          if DataRicerche.QOrgNewIDMansione.asString <> '' then begin
            QTemp.Close;
            QTemp.SQL.text := 'select IDArea from Mansioni where ID=' + DataRicerche.QOrgNewIDMansione.asString;
            QTemp.Open;
            Q1.Close;
            Q1.SQL.text := 'update EsperienzeLavorative set IDMansione=:xIDMansione,IDArea=:xIDArea ' +
              ' where IDAnagrafica=:xIDAnagrafica and IDAzienda=:xIDAzienda';
            Q1.ParamByName('xIDMansione').asInteger := DataRicerche.QOrgNewIDMansione.Value;
            Q1.ParamByName('xIDArea').asInteger := QTemp.FieldByName('IDArea').asInteger;
            Q1.ParamByName('xIDAnagrafica').asInteger := longint(TVAziendaCand.Selected.Data);
            Q1.ParamByName('xIDAzienda').asInteger := DataRicerche.QAziendeID.Value;
            Q1.ExecSQL;
            QTemp.Close;
          end;
          // se non era la sua attuale --> setta come attuale
          if LongInt(TVAziendaCand.Selected.Parent.Data) = 1 then begin
            // prima tutte le altre esp.lav. NON attuali
            Q1.Close;
            Q1.SQL.text := 'update EsperienzeLavorative set attuale=0 where IDAnagrafica=:xIDAnagrafica';
            Q1.ParamByName('xIDAnagrafica').asInteger := longint(TVAziendaCand.Selected.Data);
            Q1.ExecSQL;
            Q1.Close;
            Q1.SQL.text := 'update EsperienzeLavorative set Attuale=1 ' +
              ' where IDAnagrafica=:xIDAnagrafica and IDAzienda=:xIDAzienda';
            Q1.ParamByName('xIDAnagrafica').asInteger := longint(TVAziendaCand.Selected.Data);
            Q1.ParamByName('xIDAzienda').asInteger := DataRicerche.QAziendeID.Value;
            Q1.ExecSQL;
          end;
          DB.CommitTrans;
          DisegnaTVAziendaCand;
        except
          DB.RollbackTrans;
          MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
        end;
      end;
      DataRicerche.QOrgNew.Close;
      DataRicerche.QOrgNew.Open;
      DataRicerche.QOrgNew.EnableControls;
      DBTree.FullExpand;
    end;
    EliminaDipOrgForm.Free;
  end;
end;

procedure TMainForm.BOrgCandDelClick(Sender: TObject);
begin
  if not CheckProfile('640') then Exit;
  if DataRicerche.QOrgNew.RecordCount = 0 then exit;
  if MessageDlg('Eliminare il soggetto dal nodo dell''organigramma ' + DataRicerche.QOrgNewDescBreve.value + ' ?', mtInformation, [mbYes, mbNo], 0) = mrYes then begin
    EliminaDipOrgForm := TEliminaDipOrgForm.create(self);
    EliminaDipOrgForm.Showmodal;
    if EliminaDipOrgForm.ModalResult = mrOK then begin
      DataRicerche.QOrgNew.DisableControls;
      with Data do begin
        DB.BeginTrans;
        try
          Q1.Close;
          Q1.SQL.text := 'update Organigramma set IDDipendente=null where ID=' + DataRicerche.QOrgNewID.asString;
          Q1.ExecSQL;
          // aggiornamento storico posizione in org.
          Q1.Close;
          Q1.SQL.text := 'update StoricoAnagPosOrg set Alladata=:xAlladata,IDAutorizzatoDa=:xIDAutorizzatoDa,Note=:xNote ' +
            ' where IDOrganigramma=:xIDOrganigramma and IDAnagrafica=:xIDAnagrafica';
          Q1.ParamByName('xAlladata').asdateTime := EliminaDipOrgForm.DEData.Date;
          Q1.ParamByName('xIDAutorizzatoDa').asInteger := EliminaDipOrgForm.xIDAutorizzatoDa;
          Q1.ParamByName('xNote').asString := EliminaDipOrgForm.ENote.Text;
          Q1.ParamByName('xIDAnagrafica').asInteger := DataRicerche.QOrgNewIDDipendente.Value;
          Q1.ParamByName('xIDOrganigramma').asInteger := DataRicerche.QOrgNewID.Value;
          Q1.ExecSQL;

          DB.CommitTrans;
        except
          DB.RollbackTrans;
          MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
        end;
      end;
      DataRicerche.QOrgNew.Close;
      DataRicerche.QOrgNew.Open;
      DataRicerche.QOrgNew.EnableControls;
      DBTree.FullExpand;
    end;
    EliminaDipOrgForm.Free;
  end;
end;

procedure TMainForm.DBEdit15Exit(Sender: TObject);
begin
  if (DBEdit15.Text <> '') then
    if (pos('-', DBEdit15.Text) = 0) and (pos('/', DBEdit15.Text) = 0) then begin
      Showmessage('ATTENZIONE: Numero di cellulare non correttamente formattato.' + chr(13) +
        'Utilizzare la linea (-) o la barra (/) per separare il prefisso dal numero');
      DBEdit15.SetFocus;
    end;
end;

procedure TMainForm.DBGrid13DblClick(Sender: TObject);
var xID: integer;
begin
  if not CheckProfile('120') then Exit;
  if Data2.QRicClienti.RecordCount > 0 then begin
    CaricaApriRicPend(Data2.QRicClientiID.Value);
    // controllo selezionatore:  gli aggregati possono entrare come il titolare !
    if (DataRicerche.TRicerchePendIDUtente.Value <> xIDUtenteAttuale) and
      (DataRicerche.TRicerchePendIDUtente2.Value <> xIDUtenteAttuale) and
      (DataRicerche.TRicerchePendIDUtente3.Value <> xIDUtenteAttuale) then begin
      if not CheckProfile('27', False) then begin
        MessageDlg('L''utente connesso non ha diritti di accesso a commesse di altri utenti' + chr(13) +
          'IMPOSSIBILE ACCEDERE ALLA COMMESSA', mtError, [mbOK], 0);
        Exit;
      end else
        MessageDlg('ATTENZIONE: l''utente connesso � diverso dal titolare della commessa e dagli associati', mtWarning, [mbOK], 0);
    end;
    SelPersForm := TSelPersForm.create(self);
    SelPersForm.CBOpzioneCand.Checked := True;
    SelPersForm.EseguiQueryCandidati;
    SelPersForm.xFromOrgTL := False;
    SelPersForm.ShowModal;
    if DataRicerche.DsRicerchePend.State = dsEdit then begin
      with DataRicerche.TRicerchePend do begin
        Data.DB.BeginTrans;
        try
          Post;
          ApplyUpdates;
          Data.DB.CommitTrans;
        except
          Data.DB.RollbackTrans;
          MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
          raise;
        end;
        CommitUpdates;
      end;
      // aggiornamento griglia solo se ci sono state modifiche ai dati di ricerca
      xID := Data2.QRicClientiID.Value;
      Data2.QRicClienti.Close;
      Data2.QRicClienti.Open;
      Data2.QRicClienti.Locate('ID', xID, []);
    end;
    SelPersForm.Free;
    DataRicerche.TRicerchePend.Close;
  end;
end;

procedure TMainForm.ToolbarButton973Click(Sender: TObject);
begin
  if not CheckProfile('057') then Exit;
  if Data.TAnagrafica.IsEmpty then exit;
  ValutazColloquioForm := TValutazColloquioForm.create(self);
  ValutazColloquioForm.xIDAnag := Data.TAnagraficaID.Value;
  ValutazColloquioForm.ShowModal;
  ValutazColloquioForm.Free;
end;

procedure TMainForm.BFileClienteOpenClick(Sender: TObject);
var xFileName: string;
begin
  xFileName := FileCartellaCli.FileName;
  if length(ExtractFileName(xFileName)) > 8 then // nomi lunghi
    xFileName := ExtractShortPathName(xFileName);
  ShellExecute(0, 'Open', pchar(xFileName), '', '', SW_SHOW);

  {if Uppercase(ExtractFileExt(xFileName))='.DOC' then
       ApriFileWordFromFile(xFileName);
  //  WinExec(pointer('winword.exe '+xFileName),SW_SHOW);
  if Uppercase(ExtractFileExt(xFileName))='.XLS' then
       WinExec(pointer('Excel.exe '+xFileName),SW_SHOW);
  if Uppercase(ExtractFileExt(xFileName))='.MDB' then
       WinExec(pointer('Msaccess.exe '+xFileName),SW_SHOW);
  if Uppercase(ExtractFileExt(xFileName))='.TXT' then
       WinExec(pointer('notepad.exe '+xFileName),SW_SHOW);
  if (Uppercase(ExtractFileExt(xFileName))='.GIF')or
       (Uppercase(ExtractFileExt(xFileName))='.JPG')or
       (Uppercase(ExtractFileExt(xFileName))='.BMP') then
       WinExec(pointer('acdsee32.exe '+xFileName),SW_SHOW);}
end;

procedure TMainForm.DirCartellaCliChange(Sender: TObject);
begin
  if Data2.dsEBCclienti.State <> dsEdit then Data2.TEBCclienti.Edit;
  Data2.TEBCClientiCartellaDoc.Value := DirCartellaCli.Directory;
  Data2.TEBCclienti.Post;
end;

procedure TMainForm.BAnagFileNewClick(Sender: TObject);
begin
  if not CheckProfile('070') then Exit;
  AnagFileForm := TAnagFileForm.create(self);
  AnagFileForm.ECognome.text := Data.TAnagraficaCognome.Value;
  AnagFileForm.ENome.text := Data.TAnagraficaNome.Value;
  AnagFileForm.FEFile.InitialDir := GetDocPath;
  AnagFileForm.ShowModal;
  if AnagFileForm.ModalResult = mrOK then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'insert into AnagFile (IDAnagrafica,IDTipo,Descrizione,NomeFile,DataCreazione) ' +
          'values (:xIDAnagrafica,:xIDTipo,:xDescrizione,:xNomeFile,:xDataCreazione)';
        Q1.ParamByName('xIDAnagrafica').asInteger := Data.TAnagraficaID.Value;
        Q1.ParamByName('xIDTipo').asInteger := AnagFileForm.QTipiFileID.Value;
        Q1.ParamByName('xDescrizione').asString := AnagFileForm.EDesc.text;
        Q1.ParamByName('xNomeFile').asString := AnagFileForm.FEFile.filename;
        Q1.ParamByName('xDataCreazione').asDateTime := FileDateToDateTime(FileAge(AnagFileForm.FEFile.filename));
        Q1.ExecSQL;
        DB.CommitTrans;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
    Data.QAnagFile.Close;
    Data.QAnagFile.Open;
  end;
  AnagFileForm.Free;
end;

procedure TMainForm.BAnagFileModClick(Sender: TObject);
begin
  if not CheckProfile('071') then Exit;
  AnagFileForm := TAnagFileForm.create(self);
  AnagFileForm.ECognome.text := Data.TAnagraficaCognome.Value;
  AnagFileForm.ENome.text := Data.TAnagraficaNome.Value;
  while (AnagFileForm.QTipiFileID.value <> Data.QAnagFileIDTipo.Value) and
    (not AnagFileForm.QTipiFile.EOF) do AnagFileForm.QTipiFile.Next;
  AnagFileForm.EDesc.text := Data.QAnagFileDescrizione.Value;
  AnagFileForm.FEFile.FileName := Data.QAnagFileNomeFile.Value;
  AnagFileForm.ShowModal;
  if AnagFileForm.ModalResult = mrOK then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'update AnagFile set IDTipo=:xIDTipo,Descrizione=:xDescrizione,NomeFile=:xNomeFile,DataCreazione=:xDataCreazione ' +
          'where ID=' + Data.QAnagFileID.asString;
        Q1.ParamByName('xIDTipo').asInteger := AnagFileForm.QTipiFileID.Value;
        Q1.ParamByName('xDescrizione').asString := AnagFileForm.EDesc.text;
        Q1.ParamByName('xNomeFile').asString := AnagFileForm.FEFile.filename;
        Q1.ParamByName('xDataCreazione').asDateTime := FileDateToDateTime(FileAge(AnagFileForm.FEFile.filename));
        Q1.ExecSQL;
        DB.CommitTrans;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
    Data.QAnagFile.Close;
    Data.QAnagFile.Open;
  end;
  AnagFileForm.Free;
end;

procedure TMainForm.BAnagFileDelClick(Sender: TObject);
begin
  if not CheckProfile('071') then Exit;
  if MessageDlg('Sei sicuro di voler eliminare l''associazione ?', mtWarning, [mbNo, mbYes], 0) = mrNo then exit;
  if MessageDlg('Vuoi eliminare il file fisico ?', mtWarning, [mbNo, mbYes], 0) = mrYes then begin
    DeleteFile(Data.QAnagFileNomeFile.Value);
  end;
  with Data do begin
    DB.BeginTrans;
    try
      Q1.Close;
      Q1.SQL.text := 'delete from AnagFile where ID=' + Data.QAnagFileID.asString;
      Q1.ExecSQL;
      DB.CommitTrans;
    except
      DB.RollbackTrans;
      MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
    end;
  end;
  Data.QAnagFile.Close;
  Data.QAnagFile.Open;
end;

procedure TMainForm.BFileCandOpenClick(Sender: TObject);
var xFileName: string;
begin
  xFileName := Data.QAnagFileNomeFile.Value;
  ShellExecute(0, 'Open', pchar(xFileName), '', '', SW_SHOW);

  {if Uppercase(ExtractFileExt(xFileName))='.DOC' then
       ApriFileWordFromFile(xFileName);
  if length(ExtractFileName(xFileName))>8 then // nomi lunghi
       xFileName:=ExtractShortPathName(xFileName);
  //  WinExec(pointer('winword.exe '+),SW_SHOW);
  if Uppercase(ExtractFileExt(xFileName))='.XLS' then
       WinExec(pointer('Excel.exe '+xFileName),SW_SHOW);
  if Uppercase(ExtractFileExt(xFileName))='.MDB' then
       WinExec(pointer('Msaccess.exe '+xFileName),SW_SHOW);
  if Uppercase(ExtractFileExt(xFileName))='.TXT' then
       WinExec(pointer('notepad.exe '+xFileName),SW_SHOW);
  if (Uppercase(ExtractFileExt(xFileName))='.GIF')or
       (Uppercase(ExtractFileExt(xFileName))='.JPG')or
       (Uppercase(ExtractFileExt(xFileName))='.BMP') then
       WinExec(pointer('acdsee32.exe '+xFileName),SW_SHOW);
  if Uppercase(ExtractFileExt(xFileName))='.PDF' then
       WinExec(pointer('AcroRd32.exe '+xFileName),SW_SHOW);
  if (Uppercase(ExtractFileExt(xFileName))='.HTM')or
       (Uppercase(ExtractFileExt(xFileName))='.HTML') then
       WinExec(pointer('IEXPLORE.EXE '+xFileName),SW_SHOW);}
end;

procedure TMainForm.RxDBGrid1DblClick(Sender: TObject);
begin
  BFileCandOpenClick(self);
end;

procedure TMainForm.ToolbarButton9745Click(Sender: TObject);
begin
  MessageDlg('ATTENZIONE!! Prima di procedere con la presentazione di personale ' +
    'accertarsi che il/i candidato/i sia/siano consenziente/i. (legge sulla privaci 675)', mtInformation, [mbOK], 0);
  if not CheckProfile('073') then Exit;

  SelRicCandForm := TSelRicCandForm.create(self);
  SelRicCandForm.QRicCand.ParamByName('xIDAnag').asInteger := Data.TAnagraficaID.Value;
  SelRicCandForm.QRicCand.Open;
  if SelRicCandForm.QRicCand.RecordCount = 0 then begin
    MessageDlg('Non risultano ricerche attive associate al soggetto', mtError, [mbOK], 0);
    SelRicCandForm.Free;
    exit;
  end else begin
    SelRicCandForm.ShowModal;
    if SelRicCandForm.ModalResult = mrOK then begin
      // GENERA PRESENTAZIONE
      if Uppercase(copy(Data.GlobalNomeAzienda.Value, 1, 6)) = 'LEDERM' then begin
        // SOLO LEDERMANN
        SelContattiForm := TSelContattiForm.create(self);
        SelContattiForm.QContatti.ParamByName('xIDCliente').asInteger := SelRicCandForm.QRicCand.FieldByName('IDCliente').asInteger;
        SelContattiForm.QContatti.Open;
        SelContattiForm.ShowModal;
        if SelContattiForm.Modalresult = mrOK then
          CompilaFilePresPosLWG(SelRicCandForm.QRicCand.FieldByName('Posizione').asString,
            SelRicCandForm.QRicCand.FieldByName('IDCandRic').asinteger,
            SelRicCandForm.QRicCand.FieldByName('IDCliente').asInteger,
            SelContattiForm.QContattiID.Value);
        SelContattiForm.Free;
      end else begin
        if Uppercase(copy(Data.GlobalNomeAzienda.Value, 1, 6)) = 'PROPOS' then begin
          // PROPOSTE
          CompilaFilePresPosProposte(SelRicCandForm.QRicCand.FieldByName('Posizione').asString,
            SelRicCandForm.QRicCand.FieldByName('IDCandRic').asinteger,
            SelRicCandForm.QRicCand.FieldByName('IDCliente').asInteger,
            SelRicCandForm.QRicCand.FieldByName('IDMansione').asInteger);
        end else begin
          if Uppercase(copy(Data.GlobalNomeAzienda.Value, 1, 6)) = 'MCS' then begin
            // MCS
            CompilaFilePresPosMCS(SelRicCandForm.QRicCand.FieldByName('Posizione').asString,
              SelRicCandForm.QRicCand.FieldByName('IDCandRic').asinteger,
              SelRicCandForm.QRicCand.FieldByName('IDCliente').asInteger,
              SelRicCandForm.QRicCand.FieldByName('IDMansione').asInteger);
          end else begin
            if Uppercase(copy(Data.GlobalNomeAzienda.Value, 1, 6)) = 'BOYDEN' then begin
              // BOYDEN
              CompilaFilePresPosBoyden(SelRicCandForm.QRicCand.FieldByName('TitoloCliente').asString,
                SelRicCandForm.QRicCand.FieldByName('IDCandRic').asinteger,
                SelRicCandForm.QRicCand.FieldByName('IDCliente').asInteger,
                SelRicCandForm.QRicCand.FieldByName('IDMansione').asInteger);
            end else begin
              // TUTTI GLI ALTRI
              CompilaFilePresPos(SelRicCandForm.QRicCand.FieldByName('Cliente').asString,
                SelRicCandForm.QRicCand.FieldByName('Posizione').asString,
                SelRicCandForm.QRicCand.FieldByName('IDCandRic').asinteger);
            end;
          end;
        end;
      end;
    end;
    SelRicCandForm.Free;
  end;
end;

procedure TMainForm.CompilaFilePresPos(xCliente, xPosizione: string; xIDCandRic: integer);
var xFileWord, xFileBase, xGiaInviati, xS, xFileWordAll, xInquadramento, xRetribuzione: string;
  MSWord: Variant;
  i, xTot, xCRpos: integer;
  xProcedi, xRiempi, xAllegaFile: boolean;
begin
  // modello generico
  xFileBase := GetDocPath + '\ModPresCandUno.doc';
  // modello x ERGON
  if Uppercase(copy(Data.GlobalNomeAzienda.Value, 1, 5)) = 'ERGON' then
    xFileBase := GetDocPath + '\ModPresERGON.doc';

  if not FileExists(xFileBase) then begin
    MessageDlg('Modello di presentazione non trovato - IMPOSSIBILE CONTINUARE', mtError, [mbOK], 0);
    exit;
  end;
  // Inserisci file
  xAllegaFile := False;
  xFileWordAll := GetDocPath + '\w' + intToStr(Data.TAnagraficaCVNumero.Value) + '.doc';
  if FileExists(xFileWordAll) then
    if messageDlg('Vuoi inserire il file Word associato al candidato ?', mtInformation, [mbYes, mbNO], 0) = mrYes then
      xAllegaFile := True;

  try
    MsWord := CreateOleObject('Word.Basic');
  except
    ShowMessage('Non riesco ad aprire Microsoft Word.');
    Exit;
  end;
  // nome file = pc<IDCandRic>.doc (non CVNumero)
  xFileWord := GetDocPath + '\pc' + intToStr(xIDCandRic) + '.doc';
  xRiempi := True;
  if FileExists(xFileWord) then begin
    if MessageDlg('Il file esiste gi� - Aprirlo (Yes) o crearne uno nuovo, riscrivendo il vecchio (No) ?', mtWarning, [mbYes, mbNo], 0) = mrYes then begin
      MsWord.AppShow;
      MsWord.FileOpen(xFileWord); // file da aprire
      xRiempi := False; // non riempire i campi
    end else begin
      MsWord.AppShow;
      MsWord.FileOpen(xFileBase);
    end;
  end else begin
    MsWord.AppShow;
    MsWord.FileOpen(xFileBase);
  end;

  // riempimento campi
  if xRiempi then begin
    MsWord.EditFind('#cliente');
    MsWord.Insert(xCliente);
    MsWord.EditFind('#Posizione');
    MsWord.Insert(xPosizione);
    MsWord.EditFind('#soggetto');
    MsWord.Insert(Data.TAnagraficaCognome.Value + ' ' + Data.TAnagraficaNome.Value);
    MsWord.EditFind('#cliente2');
    MsWord.Insert(xCliente);
    MsWord.EditFind('#DataOggi');
    MsWord.Insert(DateToStr(Date));
    MsWord.EditFind('#LuogoDataNasc');
    MsWord.Insert(Data.TAnagraficaLuogoNascita.Value + ', ' + Data.TAnagraficaDataNascita.asString);
    if Data.TAnagraficaStatoCivile.Value <> '' then begin
      MsWord.EditFind('#statocivile');
      MsWord.Insert(Data.TAnagraficaStatoCivile.Value);
    end;
    if Data.TAnagraficaIndirizzo.Value <> '' then begin
      MsWord.EditFind('#indirizzo');
      MsWord.Insert(Data.TAnagraficaIndirizzo.Value);
    end;
    if Data.TAnagraficaRecapitiTelefonici.Value <> '' then begin
      MsWord.EditFind('#telefonoab');
      MsWord.Insert(Data.TAnagraficaRecapitiTelefonici.Value);
    end;
    if Data.TAnagraficaTelUfficio.Value <> '' then begin
      MsWord.EditFind('#telufficio');
      MsWord.Insert(Data.TAnagraficaTelUfficio.Value);
    end;
    if Data.TAnagraficaCellulare.Value <> '' then begin
      MsWord.EditFind('#cellulare');
      MsWord.Insert(Data.TAnagraficaCellulare.Value);
    end;

    // altri dati
    Data.Q1.close;
    Data.Q1.SQL.text := 'select ServizioMilitareStato,Inquadramento,Retribuzione ' +
      'from AnagAltreInfo where IDAnagrafica=' + Data.TAnagraficaID.asString;
    Data.Q1.Open;
    if Data.Q1.FieldByName('ServizioMilitareStato').asString <> '' then begin
      MsWord.EditFind('#ServMilStato');
      MsWord.Insert(Data.Q1.FieldByName('ServizioMilitareStato').asString);
    end;
    xInquadramento := Data.Q1.FieldByName('Inquadramento').asString;
    xRetribuzione := Data.Q1.FieldByName('Retribuzione').asString;

    // FORMAZIONE
    Data.Q1.close;
    Data.Q1.SQL.text := 'select tipo, Descrizione Titolo,LuogoConseguimento,DataConseguimento ' +
      'from TitoliStudio,Diplomi where TitoliStudio.IDDiploma=Diplomi.ID ' +
      'and IDAnagrafica=' + Data.TAnagraficaID.asString;
    Data.Q1.Open;
    if Data.Q1.RecordCount > 0 then begin
      MsWord.EditFind('#titoliStudio');
      while not Data.Q1.EOF do begin
        MsWord.Insert(Data.Q1.FieldByName('Titolo').asString + ' conseguita presso: ' +
          Data.Q1.FieldByName('LuogoConseguimento').asString + ' nel ' +
          Data.Q1.FieldByName('DataConseguimento').asString);
        Data.Q1.Next;
        if not Data.Q1.EOF then MsWord.Insert(chr(13));
      end;
    end;

    // LINGUE
    Data.Q1.close;
    Data.Q1.SQL.text := 'select Lingua, livelloConoscenza Livello from LingueConosciute,LivelloLingue ' +
      'where LingueConosciute.Livello *= LivelloLingue.ID ' +
      'and IDAnagrafica=' + Data.TAnagraficaID.asString;
    Data.Q1.Open;
    if Data.Q1.RecordCount > 0 then begin
      MsWord.EditFind('#lingue');
      while not Data.Q1.EOF do begin
        MsWord.Insert(Data.Q1.FieldByName('Lingua').asString + ': ' +
          Data.Q1.FieldByName('Livello').asString);
        Data.Q1.Next;
        if not Data.Q1.EOF then MsWord.Insert(chr(13));
      end;
    end;

    // PERCORSO PROFESSIONALE
    Data.Q1.close;
    Data.Q1.SQL.text := 'select EBC_Clienti.Descrizione Azienda, EBC_Clienti.Comune, EBC_Clienti.Provincia, ' +
      'EBC_Clienti.NumDipendenti, Attivita, Mansioni.Descrizione Posizione, ' +
      'AnnoDal,AnnoAl,DescrizioneMansione ' +
      'from EsperienzeLavorative,EBC_Clienti,Mansioni,EBC_Attivita ' +
      'where EsperienzeLavorative.IDAzienda=EBC_Clienti.ID ' +
      'and EsperienzeLavorative.IDMansione=Mansioni.ID ' +
      'and EsperienzeLavorative.IDSettore=EBC_Attivita.ID and IDAnagrafica=' + Data.TAnagraficaID.asString;
    Data.Q1.Open;
    MsWord.EditFind('#EspLav');
    while not Data.Q1.EOF do begin
      MsWord.Insert(Data.Q1.FieldByName('Azienda').asString + ' - ' + Data.Q1.FieldByName('Comune').asString + ' (' + Data.Q1.FieldByName('Provincia').asString + ')   ' +
        Data.Q1.FieldByName('AnnoDal').asString + '-' + Data.Q1.FieldByName('AnnoAl').asString + chr(13));
      MsWord.Insert(Data.Q1.FieldByName('Posizione').asString + chr(13));
      MsWord.Insert('Dip.: ' + Data.Q1.FieldByName('NumDipendenti').asString + chr(13));
      MsWord.Insert('Settore: ' + Data.Q1.FieldByName('Attivita').asString + chr(13));
      MsWord.Insert(chr(13));
      MsWord.Insert('Attivit� principali' + chr(13));

      xS := Data.Q1.FieldByName('DescrizioneMansione').asString;
      if xs <> '' then begin
        while pos(chr(13), xS) > 0 do begin
          xCRpos := pos(chr(13), xS);
          MsWord.Insert(copy(xS, 1, xCRpos - 1));
          xS := copy(xS, xCRpos + 1, length(xS));
        end;
      end;
      MsWord.Insert(chr(13));
      MsWord.Insert(chr(13));
      Data.Q1.Next;
      if not Data.Q1.EOF then MsWord.Insert(chr(13));
    end;

    // condizioni di inserimento attuali
    if xInquadramento <> '' then begin
      MsWord.EditFind('#livello');
      MsWord.Insert(xInquadramento);
    end;
    if xRetribuzione <> '' then begin
      MsWord.EditFind('#retribuzione');
      MsWord.Insert(xRetribuzione);
    end;

    // allega file
    if xAllegaFile then begin
      MsWord.EditFind('#AllegaFile');
      MsWord.InsertFile(xFileWordAll);
    end;

    Data.Q1.close;
    MsWord.FileSaveAs(xFileWord);
    // aggiornamento AnagFile
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'insert into AnagFile (IDAnagrafica,IDTipo,Descrizione,NomeFile,DataCreazione) ' +
          'values (:xIDAnagrafica,:xIDTipo,:xDescrizione,:xNomeFile,:xDataCreazione)';
        Q1.ParamByName('xIDAnagrafica').asInteger := Data.TAnagraficaID.Value;
        Q1.ParamByName('xIDTipo').asInteger := 1;
        Q1.ParamByName('xDescrizione').asString := '';
        Q1.ParamByName('xNomeFile').asString := xFileWord;
        Q1.ParamByName('xDataCreazione').asDateTime := Date;
        Q1.ExecSQL;
        DB.CommitTrans;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
    if Data.QAnagFile.Active then begin
      Data.QAnagFile.Close;
      Data.QAnagFile.Open;
    end;
  end;
end;

procedure TMainForm.CompilaFilePresPosLWG(xPosizione: string; xIDCandRic, xIDCliente, xIDContatto: integer);
var xFileWord, xFileBase, xGiaInviati, xS, xFileWordAll, xTitolo, xInquadramento, xRetribuzione, xNaz: string;
  MSWord: Variant;
  i, xTot, xCRpos: integer;
  xProcedi, xRiempi, xAllegaFile: boolean;
  xcliente, xRifInterno, xIndirizzoCliente, xCAPComuneProv, xTipoRetrib, xTipoRetribEng: string;
  xAnno, xMese, xGiorno: Word;
  xMeseDic: string;
begin
  // modello LWG
  xFileBase := GetDocPath + '\ModPresCandLWG.doc';

  if not FileExists(xFileBase) then begin
    MessageDlg('Modello di presentazione "ModPresCandLWG.doc" non trovato - IMPOSSIBILE CONTINUARE', mtError, [mbOK], 0);
    exit;
  end;
  // Inserisci file --> NO PER ORA
  xAllegaFile := False;
  {xFileWordAll:=GetDocPath+'\w'+intToStr(Data.TAnagraficaCVNumero.Value)+'.doc';
  if FileExists(xFileWordAll) then
       if messageDlg('Vuoi inserire il file Word associato al candidato ?',mtInformation, [mbYes,mbNO],0)=mrYes then
            xAllegaFile:=True;}

  try
    MsWord := CreateOleObject('Word.Basic');
  except
    ShowMessage('Non riesco ad aprire Microsoft Word.');
    Exit;
  end;
  // nome file = pc<IDCandRic>.doc (non CVNumero)
  xFileWord := GetDocPath + '\pc' + intToStr(xIDCandRic) + '.doc';
  xretribuzione := '   ';
  Data.QAnagNote.Open;
  xRetribuzione := Data.QAnagNoteRetribuzione.Value + '          ';
  xNaz := Data.QAnagNoteNazionalita.Value + '    ';
  xTipoRetrib := Data.QAnagNoteTipoRetrib.Value;
  if xTipoRetrib <> '' then xTipoRetrib := '(' + xTipoRetrib + ')'
  else xTipoRetrib := '   ';
  xTipoRetribEng := Data.QAnagNoteTipoRetribEng.Value;
  if xTipoRetribEng <> '' then xTipoRetribEng := '(' + xTipoRetribEng + ')'
  else xTipoRetribEng := '   ';
  Data.QAnagNote.Close;

  xcliente := '       ';
  xRifInterno := '       ';
  xIndirizzoCliente := '       ';
  xCAPComuneProv := '       ';
  Data.Q1.Close;
  Data.Q1.SQL.text := 'select EBC_Clienti.Descrizione Cliente,EBC_Clienti.Indirizzo,EBC_Clienti.Cap,' +
    'EBC_Clienti.Comune,EBC_Clienti.Provincia,Contatto,EBC_COntattiClienti.Titolo, ' +
    'EBC_COntattiClienti.Sesso ' +
    'from EBC_Clienti,EBC_ContattiClienti ' +
    'where EBC_Clienti.ID=EBC_ContattiClienti.IDCliente ' +
    'and EBC_ContattiClienti.ID=' + IntToStr(xIDContatto);
  Data.Q1.Open;
  xcliente := Data.Q1.FieldByName('Cliente').asString + '    ';
  if Data.Q1.FieldByName('Titolo').asString <> '' then
    xRifInterno := Data.Q1.FieldByName('Titolo').asString + ' ' + Data.Q1.FieldByName('Contatto').asString + '    '
  else
    if Data.Q1.FieldByName('Sesso').value = 'F' then
      xRifInterno := 'Sig.ra ' + Data.Q1.FieldByName('Contatto').asString + '    '
    else xRifInterno := 'Sig. ' + Data.Q1.FieldByName('Contatto').asString + '    ';
  xIndirizzoCliente := Data.Q1.FieldByName('Indirizzo').asString + '    ';
  xCAPComuneProv := Data.Q1.FieldByName('Cap').asString + ' ' +
    Data.Q1.FieldByName('Comune').asString + ' ' +
    Data.Q1.FieldByName('Provincia').asString + '   ';
  Data.Q1.Close;

  xRiempi := True;
  if FileExists(xFileWord) then begin
    if MessageDlg('Il file esiste gi� - Aprirlo (Yes) o crearne uno nuovo, riscrivendo il vecchio (No) ?', mtWarning, [mbYes, mbNo], 0) = mrYes then begin
      MsWord.AppShow;
      MsWord.FileOpen(xFileWord); // file da aprire
      xRiempi := False; // non riempire i campi
    end else begin
      MsWord.AppShow;
      MsWord.FileOpen(xFileBase);
    end;
  end else begin
    MsWord.AppShow;
    MsWord.FileOpen(xFileBase);
  end;

  // riempimento campi
  if xRiempi then begin
    // pagina-lettera di inoltro
    MsWord.EditFind('#cliente');
    MsWord.Insert(xCliente);
    MsWord.EditFind('#RifInterno');
    MsWord.Insert(xRifInterno);
    MsWord.EditFind('#IndirizzoCliente');
    MsWord.Insert(xIndirizzoCliente);
    MsWord.EditFind('#CAPComuneProv');
    MsWord.Insert(xCAPComuneProv);
    MsWord.EditFind('#data');
    DecodeDate(Date, xAnno, xMese, xGiorno);
    case xMese of
      1: xMeseDic := 'gennaio';
      2: xMeseDic := 'febbraio';
      3: xMeseDic := 'marzo';
      4: xMeseDic := 'aprile';
      5: xMeseDic := 'maggio';
      6: xMeseDic := 'giugno';
      7: xMeseDic := 'luglio';
      8: xMeseDic := 'agosto';
      9: xMeseDic := 'settembre';
      10: xMeseDic := 'ottobre';
      11: xMeseDic := 'novembre';
      12: xMeseDic := 'dicembre';
    end;
    MsWord.Insert(IntToStr(xGiorno) + ' ' + xMeseDic + ' ' + IntToStr(xAnno));
    MsWord.EditFind('#RifInterno2');
    MsWord.Insert(xRifInterno);
    MsWord.EditFind('#DicSessoCandidato');
    if Data.TAnagraficaSesso.value = 'F' then
      MsWord.Insert('della nostra candidata')
    else MsWord.Insert('del nostro candidato');
    MsWord.EditFind('#candidato');
    MsWord.Insert(Data.TAnagraficaNome.Value + ' ' + Data.TAnagraficaCognome.Value);
    MsWord.EditFind('#DicSessoCandidato2');
    if Data.TAnagraficaSesso.value = 'F' then
      MsWord.Insert('La nostra candidata')
    else MsWord.Insert('Il nostro candidato');
    MsWord.EditFind('#candidato2');
    MsWord.Insert(Data.TAnagraficaNome.Value + ' ' + Data.TAnagraficaCognome.Value);

    // CV vero e proprio (2a pagina)
    MsWord.EditFind('#cognome');
    MsWord.Insert(Data.TAnagraficaCognome.Value);
    MsWord.EditFind('#nome');
    MsWord.Insert(Data.TAnagraficaNome.Value);
    MsWord.EditFind('#DataNascita');
    MsWord.Insert(Data.TAnagraficaDataNascita.asString);
    MsWord.EditFind('#nazionalita');
    MsWord.Insert(xNaz);
    //if Data.TAnagraficaStatoCivile.Value<>'' then
    MsWord.EditFind('#StatoCivile');
    //else MsWord.Insert('--');
    MsWord.Insert(Data.TAnagraficaStatoCivile.Value + '  ');
    MsWord.EditFind('#Indirizzo');
    // comune di residenza (MODIFICATO tramite Ruben)
    if Data.TAnagraficaComune.value <> '' then
      MsWord.Insert(Data.TAnagraficaComune.value)
    else MsWord.Insert('--');
    MsWord.EditFind('#Professione');
    MsWord.Insert(uppercase(xPosizione));
    MsWord.EditFind('#TipoRetrib');
    MsWord.Insert(xTipoRetrib);
    MsWord.EditFind('#RetribLorda');
    MsWord.Insert(xretribuzione);
    MsWord.EditFind('#TipoRetribEng');
    MsWord.Insert(xTipoRetribEng);

    // FORMAZIONE
    Data.Q1.close;
    Data.Q1.SQL.text := 'select Tipo,Descrizione Titolo,LuogoConseguimento,DataConseguimento ' +
      'from TitoliStudio,Diplomi where TitoliStudio.IDDiploma=Diplomi.ID ' +
      'and IDAnagrafica=' + Data.TAnagraficaID.asString + '  ' +
      'order by DataConseguimento';
    Data.Q1.Open;
    if Data.Q1.RecordCount > 0 then begin
      MsWord.EditFind('#FormazScolastica');
      while not Data.Q1.EOF do begin
        xTitolo := '';
        xTitolo := Data.Q1.FieldByName('Titolo').asString;
        if Data.Q1.FieldByName('LuogoConseguimento').asString <> '' then
          xTitolo := xTitolo + '  conseguita presso: ' + Data.Q1.FieldByName('LuogoConseguimento').asString;
        if Data.Q1.FieldByName('DataConseguimento').asString <> '' then
          xTitolo := xTitolo + '  nel ' + Data.Q1.FieldByName('DataConseguimento').asString;
        MsWord.Insert(xTitolo);
        Data.Q1.Next;
        if not Data.Q1.EOF then MsWord.Insert(chr(13));
      end;
    end;

    // LINGUE
    Data.Q1.close;
    Data.Q1.SQL.text := 'select Lingua, LivParlato.livelloConoscenza LivelloParlato, LivScritto.livelloConoscenza LivelloScritto ' +
      'from LingueConosciute,LivelloLingue LivParlato, LivelloLingue LivScritto ' +
      'where LingueConosciute.Livello *= LivParlato.ID ' +
      '  and LingueConosciute.LivelloScritto *= LivScritto.ID ' +
      'and IDAnagrafica=' + Data.TAnagraficaID.asString;
    Data.Q1.Open;
    if Data.Q1.RecordCount > 0 then begin
      MsWord.EditFind('#lingue');
      while not Data.Q1.EOF do begin
        // prima colonna: la lingua
        MsWord.Insert(uppercase(Data.Q1.FieldByName('Lingua').asString));
        MsWord.NextCell;
        // seconda colonna: conoscenza orale
        MsWord.Insert(Data.Q1.FieldByName('LivelloParlato').asString);
        MsWord.NextCell;
        // terza colonna: conoscenza scritta
        MsWord.Insert(Data.Q1.FieldByName('LivelloScritto').asString);
        Data.Q1.Next;
        if not Data.Q1.EOF then MsWord.NextCell; //
      end;
    end;

    // PERCORSO PROFESSIONALE
    Data.Q1.close;
    Data.Q1.SQL.text := 'select EBC_Clienti.Descrizione Azienda, EBC_Clienti.Comune, EBC_Clienti.Provincia, ' +
      'EBC_Clienti.NumDipendenti, Attivita, Mansioni.Descrizione Posizione, ' +
      'AnnoDal,AnnoAl,DescrizioneMansione ' +
      'from EsperienzeLavorative,EBC_Clienti,Mansioni,EBC_Attivita ' +
      'where EsperienzeLavorative.IDAzienda = EBC_Clienti.ID ' +
      'and EsperienzeLavorative.IDMansione = Mansioni.ID ' +
      'and EsperienzeLavorative.IDSettore *= EBC_Attivita.ID and IDAnagrafica=' + Data.TAnagraficaID.asString +
      'order by AnnoAl desc';
    Data.Q1.Open;
    MsWord.EditFind('#EspLav');
    while not Data.Q1.EOF do begin
      // prima colonna
      MsWord.Insert(Data.Q1.FieldByName('AnnoDal').asString + ' - ' + Data.Q1.FieldByName('AnnoAl').asString);
      // seconda colonna
      MsWord.NextCell;
      MsWord.Insert(Data.Q1.FieldByName('Azienda').asString + ' - ' + Data.Q1.FieldByName('Comune').asString + ' (' + Data.Q1.FieldByName('Provincia').asString + ') ' + chr(13));
      MsWord.Insert('(' + LowerCase(Data.Q1.FieldByName('Attivita').asString) + ')' + chr(13) + chr(13));
      MsWord.Insert(uppercase(Data.Q1.FieldByName('Posizione').asString) + chr(13) + chr(13));
      xS := Data.Q1.FieldByName('DescrizioneMansione').asString;
      if xs <> '' then begin
        while pos(chr(13), xS) > 0 do begin
          xCRpos := pos(chr(13), xS);
          MsWord.Insert(copy(xS, 1, xCRpos - 1));
          xS := copy(xS, xCRpos + 1, length(xS));
        end;
      end;
      MsWord.Insert(xS);
      MsWord.Insert(chr(13));
      Data.Q1.Next;
      if not Data.Q1.EOF then MsWord.NextCell; //MsWord.Insert(chr(13));
    end;
    Data.Q1.close;
    MsWord.FileSaveAs(xFileWord);
    // aggiornamento AnagFile
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'insert into AnagFile (IDAnagrafica,IDTipo,Descrizione,NomeFile,DataCreazione) ' +
          'values (:xIDAnagrafica,:xIDTipo,:xDescrizione,:xNomeFile,:xDataCreazione)';
        Q1.ParamByName('xIDAnagrafica').asInteger := Data.TAnagraficaID.Value;
        Q1.ParamByName('xIDTipo').asInteger := 1;
        Q1.ParamByName('xDescrizione').asString := '';
        Q1.ParamByName('xNomeFile').asString := xFileWord;
        Q1.ParamByName('xDataCreazione').asDateTime := Date;
        Q1.ExecSQL;
        DB.CommitTrans;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
    if Data.QAnagFile.Active then begin
      Data.QAnagFile.Close;
      Data.QAnagFile.Open;
    end;
  end;
end;

procedure TMainForm.TbBContattiCliAltriDatiClick(Sender: TObject);
var xID: integer;
begin
  if Data2.TContattiClienti.IsEmpty then exit;
  if not CheckProfile('104') then Exit;
  RifClienteDatiForm := TRifClienteDatiForm.create(self);
  RifClienteDatiForm.ENominativo.text := Data2.TContattiClientiContatto.Value;
  RifClienteDatiForm.CBSesso.text := Data2.TContattiClientiSesso.Value;
  RifClienteDatiForm.xIDContatto := Data2.TContattiClientiID.Value;
  RifClienteDatiForm.ShowModal;
  if RifClienteDatiForm.ModalResult = mrOK then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'update EBC_ContattiClienti set CompleannoGiorno=:xCompleannoGiorno, ' +
          'CompleannoMese=:xCompleannoMese, TitoloStudio=:xTitoloStudio, CaricaAziendale=:xCaricaAziendale, ' +
          'NomeSegretaria=:xNomeSegretaria,IndirizzoPrivato=:xIndirizzoPrivato,CapPrivato=:xCapPrivato, ' +
          'ProvinciaPrivato=:xProvinciaPrivato,TelefonoAbitazione=:xTelefonoAbitazione,Sesso=:xSesso,Stato=:xStato, ' +
          'TipoStradaPrivato=:xTipoStradaPrivato,NumCivicoPrivato=:xNumCivicoPrivato,DivisioneAziendale=:xDivisioneAziendale, ' +
          'DataIns=:xDataIns,IDUtente=:xIDUtente ' +
          'where ID=' + Data2.TContattiClientiID.asString;
        Q1.ParamByName('xCompleannoGiorno').asInteger := RifClienteDatiForm.SEGiorno.value;
        Q1.ParamByName('xCompleannoMese').asInteger := RifClienteDatiForm.SEMese.value;
        Q1.ParamByName('xTitoloStudio').asString := RifClienteDatiForm.ETitStudio.text;
        Q1.ParamByName('xCaricaAziendale').asString := RifClienteDatiForm.ECaricaAz.text;
        Q1.ParamByName('xNomeSegretaria').asString := RifClienteDatiForm.ENomeSeg.text;
        Q1.ParamByName('xIndirizzoPrivato').asString := RifClienteDatiForm.EIndirizzo.text;
        Q1.ParamByName('xCapPrivato').asString := RifClienteDatiForm.ECap.Text;
        Q1.ParamByName('xProvinciaPrivato').asString := RifClienteDatiForm.EProv.text;
        Q1.ParamByName('xTelefonoAbitazione').asString := RifClienteDatiForm.EtelAb.text;
        Q1.ParamByName('xSesso').asString := RifClienteDatiForm.CBSesso.Text;
        Q1.ParamByName('xTipoStradaPrivato').asString := RifClienteDatiForm.CBTipoStradaRes.Text;
        Q1.ParamByName('xNumCivicoPrivato').asString := RifClienteDatiForm.ENumCivico.text;
        Q1.ParamByName('xDivisioneAziendale').asString := RifClienteDatiForm.EDivisioneAz.text;
        Q1.ParamByName('xStato').asString := RifClienteDatiForm.ENazione.Text;
        Q1.ParamByName('xDataIns').asDateTime := Date;
        Q1.ParamByName('xIDUtente').asInteger := xIDUtenteAttuale;
        Q1.ExecSQL;
        DB.CommitTrans;
        xID := Data2.TContattiClientiID.Value;
        Data2.TContattiClienti.Close;
        Data2.TContattiClienti.Open;
        Data2.TContattiClienti.Locate('ID', xID, []);
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
  end;
  RifClienteDatiForm.Free;
end;

procedure TMainForm.BRicClientiClick(Sender: TObject);
begin
  if not CheckProfile('171') then Exit;
  RicClientiForm := TRicClientiForm.create(self);
  // riempimento combobox1 con le tabelle
  with RicClientiForm do begin
    QTabelle.Close;
    QTabelle.Open;
    QTabelle.First;
    ComboBox1.Items.Clear;
    while not QTabelle.EOF do begin
      ComboBox1.Items.Add(QTabelleDescTabella.asString);
      QTabelle.Next;
    end;
    QRes1.Close;
  end;
  RicClientiForm.Showmodal;
  RicClientiForm.Free;
end;

procedure TMainForm.CBRicUtentiTuttiClick(Sender: TObject);
begin
  EseguiQRicAttive;
end;

procedure TMainForm.BLegendaClick(Sender: TObject);
begin
  LegendaElRicForm := TLegendaElRicForm.create(self);
  LegendaElRicForm.ShowModal;
  LegendaElRicForm.Free;

  Data.Q1.Close;
  Data.Q1.Sql.text := 'select ggDiffApRic,ggDiffUcRic,ggDiffColRic from global ';
  Data.Q1.Open;
  xggDiffApRic := Data.Q1.FieldByname('ggDiffApRic').asInteger;
  xggDiffUcRic := Data.Q1.FieldByname('ggDiffUcRic').asInteger;
  xggDiffColRic := Data.Q1.FieldByname('ggDiffColRic').asInteger;
  Data.Q1.Close;

  // refresh griglia
  EseguiQRicAttive;
end;

procedure TMainForm.DBGrid7DblClick(Sender: TObject);
begin
  ToolbarButton977Click(self);
end;

procedure TMainForm.BGlobalOKClick(Sender: TObject);
begin
  Data.Global.Post;
end;

procedure TMainForm.BGlobalCanClick(Sender: TObject);
begin
  Data.Global.Cancel;
end;

procedure TMainForm.EClientiKeyPress(Sender: TObject; var Key: Char);
begin
  if key = chr(13) then BCliSearchClick(self);
end;

procedure TMainForm.BCliSearchClick(Sender: TObject);
var xFiltro: string;
begin
  if Length(EClienti.text) < 2 then begin
    ShowMessage('Inserire almeno due iniziali');
    exit;
  end;
  xFiltro := '';
  TSCliForn.TabVisible := False;
  if Pagecontrol4.ActivePage = TSCliAttivi then xFiltro := 'attivo';
  if Pagecontrol4.ActivePage = TSCliNonAttivi then xFiltro := 'non attivo';
  if Pagecontrol4.ActivePage = TSCliEliminati then xFiltro := 'Eliminato';
  if Pagecontrol4.ActivePage = TSCliContattati then xFiltro := 'contattato';
  if Pagecontrol4.ActivePage = TSCliAltre then xFiltro := 'azienda esterna';
  if Pagecontrol4.ActivePage = TSCliconcess then xFiltro := 'concessionario';
  if Pagecontrol4.ActivePage = TSCliFornitori then xFiltro := 'fornitore';
  CaricaApriClienti(EClienti.text, xFiltro);
  if Data2.TEBCClientiStato.value = 'fornitore' then TSCliForn.TabVisible := True;
  if PCClienti.ActivePage = TSClientiCV then begin
    CliCandidatiFrame1.xIDCliente := Data2.TEBCClientiID.Value;
    CliCandidatiFrame1.xIDRicerca := 0;
    CliCandidatiFrame1.CreaAlbero;
  end;
end;

procedure TMainForm.TbBMansModAreaClick(Sender: TObject);
begin
  SelAreaForm := TSelAreaForm.create(self);
  SelAreaForm.caption := 'selezione nuova area';
  SelAreaForm.ShowModal;
  if SelAreaForm.ModalResult = mrOK then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'update EsperienzeLavorative set IDArea=:xIDArea where IDMansione=' + Data2.TMansioniID.asString;
        Q1.ParamByName('xIDArea').asInteger := SelAreaForm.QAreeID.Value;
        Q1.ExecSQL;
        Q1.Close;
        Q1.SQL.text := 'update AnagMansioni set IDArea=:xIDArea where IDMansione=' + Data2.TMansioniID.asString;
        Q1.ParamByName('xIDArea').asInteger := SelAreaForm.QAreeID.Value;
        Q1.ExecSQL;
        Q1.Close;
        Q1.SQL.text := 'update EBC_Ricerche set IDArea=:xIDArea where IDMansione=' + Data2.TMansioniID.asString;
        Q1.ParamByName('xIDArea').asInteger := SelAreaForm.QAreeID.Value;
        Q1.ExecSQL;
        Q1.Close;
        Q1.SQL.text := 'update Mansioni set IDArea=:xIDArea where ID=' + Data2.TMansioniID.asString;
        Q1.ParamByName('xIDArea').asInteger := SelAreaForm.QAreeID.Value;
        Q1.ExecSQL;

        DB.CommitTrans;
        Data2.TMansioni.Close;
        Data2.TMansioni.Open;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;

  end;
  SelAreaForm.Free;
end;

procedure TMainForm.BFornCCModClick(Sender: TObject);
var k: integer;
  xNonInviati: string;
begin
  if Data2.TContattiClienti.IsEmpty then exit;
  if not CheckProfile('105') then Exit;
  if (not (Data2.TContattiClienti.active)) or (Data2.TContattiClienti.RecordCount = 0) then exit;
  SceltaModelloMailCliForm := TSceltaModelloMailCliForm.create(self);
  SceltaModelloMailCliForm.ShowModal;
  if SceltaModelloMailCliForm.ModalResult = mrOK then begin
    xNonInviati := '';
    xNonInviati := SpedisciMail(Data2.TContattiClientiemail.Value);
    if xNonInviati <> '' then
      ShowMessage('Messaggio non inviato ai seguenti soggetti per mancanza indirizzo e-mail:' + chr(13) +
        xnonInviati)
    else begin
      // se ha spedito ==> STORICO
      if SceltaModelloMailCliForm.CBStoricoInvio.checked then begin
        with Data do begin
          DB.BeginTrans;
          try
            // storico invio cliente
            Q1 := CreateQueryFromString('insert into StoricoInvioClienti (IDCliente,IDContattoCli,IDTipoMailContattoCli,Tipo,Data,Descrizione) ' +
              'values (:xIDCliente,:xIDContattoCli,:xIDTipoMailContattoCli,:xTipo,:xData,:xDescrizione)');
            Q1.ParamByName('xIDCliente').asInteger := Data2.TEBCClientiID.Value;
            Q1.ParamByName('xIDContattoCli').asInteger := Data2.TContattiClientiID.Value;
            Q1.ParamByName('xIDTipoMailContattoCli').asInteger := SceltaModelloMailCliForm.QTipiMailContattiCliID.Value;
            Q1.ParamByName('xTipo').asString := 'e-mail';
            Q1.ParamByName('xData').asDateTime := Date;
            Q1.ParamByName('xDescrizione').asString := SceltaModelloMailCliForm.QTipiMailContattiCliDescrizione.Value;
            Q1.ExecSQL;
            DB.CommitTrans;
          except
            DB.RollbackTrans;
            MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
          end;
        end;
      end;
      ShowMessage('Messaggi inviati con successo');
    end;
  end;
  SceltaModelloMailCliForm.Free;
end;

function TMainForm.SpedisciMail(xAddress: string): string;
var i, xTot, xCRpos: integer;
  xMess, xSl: TstringList;
  Vero: boolean;
  xNonInviati, xNonInviatiSMS, xFirma, xS: string;
begin
  // impostazione parametri
  NMSMTP1.Host := SceltaModelloMailCliForm.EHost.Text;
  NMSMTP1.Port := StrToInt(SceltaModelloMailCliForm.EPort.Text);
  NMSMTP1.UserID := SceltaModelloMailCliForm.EUserID.Text;
  // oggetto (da tabella)
  NMSMTP1.PostMessage.Subject := SceltaModelloMailCliForm.QTipiMailContattiCliOggetto.Value;
  // COMPOSIZIONE MESSAGGIO
  xMess := TStringList.create;
  // intestazione (da tabella)
  xMess.Add(SceltaModelloMailCliForm.QTipiMailContattiCliIntestazione.Value);
  xMess.Add('');
  with Data do begin
    Q2.Close;
    Q2.SQL.Text := 'select Descrizione,FirmaMail from Users where ID=' + IntToStr(MainForm.xIDUtenteAttuale);
    Q2.Open;
    // corpo (da tabella)
    xMess.Add(SceltaModelloMailCliForm.QTipiMailContattiCliCorpo.Value);
    // saluti e firma (da tabella Utenti) con ritorno a capo
    xMess.Add('');
    xS := Q2.FieldByName('FirmaMail').asString;
    while pos(chr(13), xS) > 0 do begin
      xCRpos := pos(chr(13), xS);
      xMess.Add(copy(xS, 1, xCRpos - 1));
      xS := copy(xS, xCRpos + 1, length(xS));
    end;
    xMess.Add(xS);

    // edit messaggio
    EmailMessageForm := TEmailMessageForm.create(self);
    EmailMessageForm.Memo1.Lines := xMess;
    EmailMessageForm.ShowModal;
    if EmailMessageForm.ModalResult = mrCancel then begin
      EmailMessageForm.Free;
      exit;
    end;
    xmess.Clear;
    for i := 0 to EmailMessageForm.Memo1.Lines.count - 1 do
      xMess.Add(EmailMessageForm.Memo1.Lines[i]);
    ShowMessage('Predisporre connessione Internet e premere OK quando pronti');
    EmailMessageForm.Free;
    NMSMTP1.PostMessage.Body := xMess;
    // connessione
    NMSMTP1.Connect;
    // header (mittente)
    NMSMTP1.PostMessage.FromAddress := SceltaModelloMailCliForm.EAddress.Text;
    NMSMTP1.PostMessage.FromName := SceltaModelloMailCliForm.EName.Text;
    // invio vero e proprio dei messaggi
    xNonInviati := '';
    xNonInviatiSMS := '';

    if xAddress <> '' then begin
      NMSMTP1.PostMessage.ToAddress.Add(xAddress);
      // invio
      NMSMTP1.SendMail;
      Result := '';
    end;
    Q2.Close;
  end;
  NMSMTP1.Disconnect;
  xMess.Free;
end;

procedure TMainForm.BOfferteClick(Sender: TObject);
begin
  if not CheckProfile('172') then Exit;
  OfferteForm := TOfferteForm.create(self);
  OfferteForm.xIDUtente := xIDUtenteAttuale;
  OfferteForm.ShowModal;
  OfferteForm.Free;
end;

procedure TMainForm.BTipoCommNewClick(Sender: TObject);
begin
  if not CheckProfile('7902') then Exit;
  TipoCommessaForm := TTipoCommessaForm.create(self);
  TipoCommessaForm.ShowModal;
  if TipoCommessaForm.ModalResult = mrOK then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'insert into EBC_TipiCommesse (TipoCommessa,Colore) ' +
          'values (:xTipoCommessa,:xColore)';
        Q1.ParamByName('xTipoCommessa').asString := TipoCommessaForm.ETipo.Text;
        Q1.ParamByName('xColore').asString := TipoCommessaForm.CBColore.text;
        Q1.ExecSQL;
        DB.CommitTrans;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
    DataRicerche.QTipoCommesse.Close;
    DataRicerche.QTipoCommesse.Open;
  end;
  TipoCommessaForm.Free;
end;

procedure TMainForm.RxDBGrid2GetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if Field.FieldName = 'Colore' then begin
    if DataRicerche.QTipoCommesse.FieldByName('Colore').asString = 'rosso' then
      Background := clRed;
    if DataRicerche.QTipoCommesse.FieldByName('Colore').asString = 'verde' then
      Background := clLime;
    if DataRicerche.QTipoCommesse.FieldByName('Colore').asString = 'giallo' then
      Background := clYellow;
    if DataRicerche.QTipoCommesse.FieldByName('Colore').asString = 'azzurro' then
      Background := clAqua;
    if DataRicerche.QTipoCommesse.FieldByName('Colore').asString = 'fucsia' then
      Background := clFuchsia;
    if DataRicerche.QTipoCommesse.FieldByName('Colore').asString = 'bianco' then
      Background := clWhite;
    if DataRicerche.QTipoCommesse.FieldByName('Colore').asString = 'oliva' then
      Background := clOlive;
    if DataRicerche.QTipoCommesse.FieldByName('Colore').asString = 'grigio' then
      Background := clSilver;
    if DataRicerche.QTipoCommesse.FieldByName('Colore').asString = 'marrone' then
      Background := clMaroon;
  end;
end;

procedure TMainForm.BTipoCommModClick(Sender: TObject);
begin
  if not CheckProfile('7902') then Exit;
  TipoCommessaForm := TTipoCommessaForm.create(self);
  TipoCommessaForm.ETipo.text := DataRicerche.QTipoCommesse.FieldByName('TipoCommessa').asString;
  TipoCommessaForm.CBColore.text := DataRicerche.QTipoCommesse.FieldByName('Colore').asString;
  TipoCommessaForm.ShowModal;
  if TipoCommessaForm.ModalResult = mrOK then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'update EBC_TipiCommesse  set TipoCommessa=:xTipoCommessa,Colore=:xColore ' +
          'where ID=' + DataRicerche.QTipoCommesse.FieldByName('ID').asString;
        Q1.ParamByName('xTipoCommessa').asString := TipoCommessaForm.ETipo.Text;
        Q1.ParamByName('xColore').asString := TipoCommessaForm.CBColore.text;
        Q1.ExecSQL;
        DB.CommitTrans;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
    DataRicerche.QTipoCommesse.Close;
    DataRicerche.QTipoCommesse.Open;
  end;
  TipoCommessaForm.Free;
end;

procedure TMainForm.BTipoCommDelClick(Sender: TObject);
begin
  if not CheckProfile('7902') then Exit;
  if DataRicerche.QTipoCommesse.RecordCount = 0 then exit;
  Data.Q1.Close;
  Data.Q1.SQL.text := 'select count (*) Tot from EBC_Ricerche where Tipo=:xTipo';
  Data.Q1.ParamByName('xTipo').asString := DataRicerche.QTipoCommesse.FieldByName('TipoCommessa').asString;
  Data.Q1.Open;
  if Data.Q1.FieldByName('Tot').asInteger > 0 then begin
    MessageDlg('Esistono ricerche con questo tipo associato - IMPOSSIBILE CANCELLARE', mtError, [mbOK], 0);
    Exit;
  end;
  Data.Q1.Close;

  if MessageDlg('Sei sicuro di voler cancellare il tipo di commessa selezionato', mtWarning, [mbYes, mbNo], 0) = mrYes then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'delete from EBC_TipiCommesse ' +
          'where ID=' + DataRicerche.QTipoCommesse.FieldByName('ID').asString;
        Q1.ExecSQL;
        DB.CommitTrans;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
    DataRicerche.QTipoCommesse.Close;
    DataRicerche.QTipoCommesse.Open;
  end;
end;

procedure TMainForm.RxDbTipiCommGetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if Field.FieldName = 'TipoCommessa' then begin
    if DataRicerche.QTipiCommColore.Value = 'rosso' then AFont.Color := clRed;
    if DataRicerche.QTipiCommColore.Value = 'verde' then AFont.Color := clLime;
    if DataRicerche.QTipiCommColore.Value = 'giallo' then AFont.Color := clYellow;
    if DataRicerche.QTipiCommColore.Value = 'azzurro' then AFont.Color := clAqua;
    if DataRicerche.QTipiCommColore.Value = 'fucsia' then AFont.Color := clFuchsia;
    if DataRicerche.QTipiCommColore.Value = 'bianco' then AFont.Color := clWhite;
    if DataRicerche.QTipiCommColore.Value = 'oliva' then AFont.Color := clOlive;
    if DataRicerche.QTipiCommColore.Value = 'grigio' then AFont.Color := clSilver;
    if DataRicerche.QTipiCommColore.Value = 'marrone' then AFont.Color := clMaroon;
  end;
end;

procedure TMainForm.BTrovaClienteClick(Sender: TObject);
var xState: TDataSetState;
  xIDAnnuncio: integer;
  xRifAnnuncio: string;
begin
  xRifAnnuncio := DataAnnunci.TAnnunciRif.Value;
  xState := DataAnnunci.DsAnnunci.State;
  xIDAnnuncio := DataAnnunci.TAnnunciID.Value;
  SelClienteForm := TSelClienteForm.create(self);
  SelClienteForm.caption := 'Selezione Cliente/azienda';
  SelClienteForm.ShowModal;
  if SelClienteForm.ModalResult = mrOK then begin
    if xState = dsEdit then TbBAnnOKClick(self);
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'update Ann_Annunci set IDCliente=:xIDCliente ' +
          'where ID=' + DataAnnunci.TAnnunciID.asString;
        Q1.ParamByName('xIDCliente').asInteger := SelClienteForm.TClientiID.Value;
        Q1.ExecSQL;
        DB.CommitTrans;
        ERifAnnuncio.text := xRifAnnuncio;
        DataAnnunci.Tannunci.Close;
        DataAnnunci.Tannunci.Open;
        BRifAnnuncioClick(self);
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
    if xState in [dsEdit, dsinsert] then begin
      DataAnnunci.TAnnunci.Edit;
      //DBMemo10.SetFocus;
    end;
  end;
  SelClienteForm.Free;
end;

procedure TMainForm.BSedeNewClick(Sender: TObject);
begin
  if not CheckProfile('7905') then Exit;
  DettSedeForm := TDettSedeForm.create(self);
  DettSedeForm.ShowModal;
  if DettSedeForm.ModalResult = mrOK then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'insert into Aziende (Descrizione,TipoStrada,Indirizzo,NumCivico,Cap,Comune,Provincia) ' +
          'values (:xDescrizione,:xTipoStrada,:xIndirizzo,:xNumCivico,:xCap,:xComune,:xProvincia)';
        Q1.ParamByName('xDescrizione').asString := DettSedeForm.EDesc.Text;
        Q1.ParamByName('xTipoStrada').asString := DettSedeForm.CBTipoStrada.text;
        Q1.ParamByName('xIndirizzo').asString := DettSedeForm.EIndirizzo.text;
        Q1.ParamByName('xNumCivico').asString := DettSedeForm.ENumCivico.text;
        Q1.ParamByName('xCap').asString := DettSedeForm.ECap.text;
        Q1.ParamByName('xComune').asString := DettSedeForm.EComune.text;
        Q1.ParamByName('xProvincia').asString := DettSedeForm.EProv.Text;
        Q1.ExecSQL;
        DB.CommitTrans;
        Data.QSedi.Close;
        Data.QSedi.Open;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
  end;
  DettSedeForm.Free;
end;

procedure TMainForm.ToolbarButton9748Click(Sender: TObject);
begin
  if not CheckProfile('7905') then Exit;
  DettSedeForm := TDettSedeForm.create(self);
  DettSedeForm.EDesc.Text := Data.QSediDescrizione.Value;
  DettSedeForm.CBTipoStrada.Text := Data.QSediTipoStrada.Value;
  DettSedeForm.EIndirizzo.text := Data.QSediIndirizzo.Value;
  DettSedeForm.ENumCivico.Text := Data.QSediNumCivico.Value;
  DettSedeForm.ECap.text := Data.QSediCap.Value;
  DettSedeForm.EComune.text := Data.QSediComune.Value;
  DettSedeForm.EProv.Text := Data.QSediProvincia.Value;
  DettSedeForm.ShowModal;
  if DettSedeForm.ModalResult = mrOK then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'update Aziende set Descrizione=:xDescrizione,TipoStrada=:xTipoStrada,Indirizzo=:xIndirizzo,NumCivico=:xNumCivico, ' +
          'Cap=:xCap,Comune=:xComune,Provincia=:xProvincia where ID=' + Data.QSediID.asString;
        Q1.ParamByName('xDescrizione').asString := DettSedeForm.EDesc.Text;
        Q1.ParamByName('xTipoStrada').asString := DettSedeForm.CBTipoStrada.text;
        Q1.ParamByName('xIndirizzo').asString := DettSedeForm.EIndirizzo.text;
        Q1.ParamByName('xNumCivico').asString := DettSedeForm.ENumCivico.text;
        Q1.ParamByName('xCap').asString := DettSedeForm.ECap.text;
        Q1.ParamByName('xComune').asString := DettSedeForm.EComune.text;
        Q1.ParamByName('xProvincia').asString := DettSedeForm.EProv.Text;
        Q1.ExecSQL;
        DB.CommitTrans;
        Data.QSedi.Close;
        Data.QSedi.Open;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
  end;
  DettSedeForm.Free;
end;

procedure TMainForm.ToolbarButton9749Click(Sender: TObject);
begin
  if not CheckProfile('7905') then Exit;
  // controllo esistenza ricerche (commesse) collegate
  Data.Q1.Close;
  Data.Q1.SQL.text := 'select count(ID) Tot from EBC_Ricerche where IDSede=' + Data.QSediID.asString;
  Data.Q1.Open;
  if Data.Q1.FieldByName('Tot').asInteger > 0 then begin
    messageDlg('Esistono commesse/ricerche collegate a questa sede - IMPOSSIBILE CANCELLARE', mtError, [mbOK], 0);
    Data.Q1.Close;
    Exit;
  end;
  Data.Q1.Close;
  if Data.QSedi.RecordCount > 0 then
    if MessageDlg('Sei sicuro di voler cancellare questa sede ?', mtWarning,
      [mbNo, mbYes], 0) = mrYes then begin
      with Data do begin
        DB.BeginTrans;
        try
          Q1.Close;
          Q1.SQL.text := 'delete from Aziende ' +
            'where ID=' + Data.QSediID.asString;
          Q1.ExecSQL;
          DB.CommitTrans;
          Data.QSedi.Close;
          Data.QSedi.Open;
        except
          DB.RollbackTrans;
          MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
        end;
      end;
    end;
end;

procedure TMainForm.BProvNewClick(Sender: TObject);
var xProvCV: string;
begin
  if not CheckProfile('7901') then Exit;
  xProvCV := '';
  if InputQuery('Nuova provenienza', 'testo:', xProvCV) then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'insert into ProvenienzeCV (Provenienza) ' +
          'values (:xProvenienza)';
        Q1.ParamByName('xProvenienza').asString := xProvCV;
        Q1.ExecSQL;
        DB.CommitTrans;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
    Data.QProvCV.Close;
    Data.QProvCV.Open;
  end;
end;

procedure TMainForm.BProvModClick(Sender: TObject);
var xProvCV: string;
begin
  if not CheckProfile('7901') then Exit;
  if Data.QProvCV.RecordCount = 0 then exit;
  xProvCV := Data.QProvCVProvenienza.Value;
  if InputQuery('Modifica provenienza', 'testo:', xProvCV) then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'update ProvenienzeCV set Provenienza=:xProvenienza ' +
          'where ID=' + Data.QProvCVID.asString;
        Q1.ParamByName('xProvenienza').asString := xProvCV;
        Q1.ExecSQL;
        DB.CommitTrans;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
    Data.QProvCV.Close;
    Data.QProvCV.Open;
  end;
end;

procedure TMainForm.BProvDelClick(Sender: TObject);
begin
  if not CheckProfile('7901') then Exit;
  if Data.QProvCV.RecordCount = 0 then exit;
  if MessageDlg('Sei sicuro di voler cancellare questa provenienza ?', mtWarning,
    [mbNo, mbYes], 0) = mrYes then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'delete from ProvenienzeCV  ' +
          'where ID=' + Data.QProvCVID.asString;
        Q1.ExecSQL;
        DB.CommitTrans;
        Data.QProvCV.Close;
        Data.QProvCV.Open;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
  end;
end;

procedure TMainForm.BCandUtenteClick(Sender: TObject);
var xPasswd, xdesc: string;
  xIDEvento: integer;
  xStoricizza: boolean;
begin
  xIDEvento := 84;
  xStoricizza := True;
  xdesc := '';
  // controllo appartenenza: se � gi� di un altro utente ==> chiedi la sua password
  if (Data.TAnagraficaIDUtente.asString <> '') and (Data.TAnagraficaIDUtente.Value > 0)
    and (Data.TAnagraficaIDUtente.Value <> xIDUtenteAttuale) then begin
    // prendi il suo nominativo e la sua password
    Data.Q1.Close;
    Data.Q1.SQL.text := 'select Nominativo,Password from Users where ID=' + Data.TAnagraficaIDUtente.asString;
    Data.Q1.Open;
    // avvertimento
    if MessageDlg('ATTENZIONE: il CV � associato all''utente ' + Data.Q1.FieldByName('Nominativo').asString + chr(13) +
      'Vuoi procedere (verr� richiesta la sua password) ?', mtWarning, [mbYes, mbNo], 0) = mrNo then begin
      xStoricizza := False;
      exit;
    end;
    xPasswd := '';
    if not InputQuery('Inserire la password per l''utente ' + Data.Q1.FieldByName('Nominativo').asString, 'Password:', xPasswd) then begin
      xStoricizza := False;
      exit;
    end;
    if not CheckPassword(xPasswd, Data.Q1.Fieldbyname('Password').asString) then begin
      MessageDlg('ERRORE: La password non � corretta', mtError, [mbOK], 0);
      xStoricizza := False;
      exit;
    end;
    xIDEvento := 85;
    xdesc := 'da utente ' + Data.Q1.FieldByName('Nominativo').asString + ' ';
    Data.Q1.Close;
  end;

  ElencoUtentiForm := TElencoUtentiForm.create(self);
  ElencoUtentiForm.xIDUtente := xIDUtenteattuale;
  ElencoUtentiForm.ShowModal;
  if ElencoUtentiForm.ModalResult = mrOK then begin
    if not (Data.DsAnagrafica.State = dsEdit) then Data.TAnagrafica.Edit;
    Data.TAnagraficaIDUtente.Value := ElencoUtentiForm.QUsersID.Value;
    TbBAnagOKClick(Self);
    ECandUtente.text := ElencoUtentiForm.QUsersNominativo.Value;
    xdesc := xdesc + 'a utente ' + ElencoUtentiForm.QUsersNominativo.Value;
  end else begin
    if MessageDlg('Eliminare il riferimento all''utente ?', mtWarning, [mbYes, mbNo], 0) = mrYes then begin
      if not (Data.DsAnagrafica.State = dsEdit) then Data.TAnagrafica.Edit;
      Data.TAnagraficaIDUtente.Value := 0;
      TbBAnagOKClick(Self);
      ECandUtente.text := '';
      xIDEvento := 86;
    end else xStoricizza := False;
  end;
  ElencoUtentiForm.Free;

  if xStoricizza then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDUtente) ' +
          'values (:xIDAnagrafica,:xIDEvento,:xDataEvento,:xAnnotazioni,:xIDUtente)';
        Q1.ParamByName('xIDAnagrafica').asInteger := Data.TAnagraficaID.Value;
        Q1.ParamByName('xIDEvento').asInteger := xIDEvento;
        Q1.ParamByName('xDataEvento').asDateTime := date;
        Q1.ParamByName('xAnnotazioni').asString := xdesc;
        Q1.ParamByName('xIDUtente').asInteger := xIDUtenteAttuale;
        Q1.ExecSQL;
        DB.CommitTrans;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
  end;
end;

procedure TMainForm.ToolbarButton9738Click(Sender: TObject);
begin
  if Data.QComuni.IsEmpty then exit;
  if Data.QComuniCodice.Value <> '' then begin
    MessageDlg('Comune predefinito - Impossibile Cancellare', mtError, [mbOK], 0);
    exit;
  end else
    // controllo associazione anag-comune
    // da fare...
    if MessageDlg('Sei sicuro di voler eliminare il comune ?', mtWarning,
      [mbNo, mbYes], 0) = mrYes then begin
      with Data do begin
        DB.BeginTrans;
        try
          Q1.Close;
          Q1.SQL.text := 'delete from TabCom ' +
            'where ID=' + QComuniID.asString;
          Q1.ExecSQL;
          DB.CommitTrans;
        except
          DB.RollbackTrans;
          MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
        end;
      end;
      ECercaComChange(self);
    end;
end;

procedure TMainForm.TSAnagNoteIntExit(Sender: TObject);
begin
  if Data.DsQAnagNote.State = dsEdit then begin
    with Data.QAnagNote do begin
      Data.DB.BeginTrans;
      try
        ApplyUpdates;
        Data.DB.CommitTrans;
      except
        Data.DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
        raise;
      end;
      CommitUpdates;
    end;
  end;
end;

procedure TMainForm.BOrgStandardClick(Sender: TObject);
begin
  if not CheckProfile('68') then Exit;
  OrgStandardForm := TOrgStandardForm.create(self);
  OrgStandardForm.ShowModal;
  OrgStandardForm.Free;
end;

procedure TMainForm.BAssociaNewClick(Sender: TObject);
var xIDAnag: integer;
  xS, xSettore: string;
  xVai, xNumeroValido: boolean;
  xCVNumero: string;
begin
  if not CheckProfile('65') then Exit;
  if (not DataRicerche.QOrgNew.Active) or (DataRicerche.QOrgNew.RecordCount = 0) then exit;
  if not CheckProfile('000') then Exit;
  // controllo numero massimo consentiti
  Data.QTemp.Close;
  Data.QTemp.SQL.Text := 'select count(*) TotAnag from Anagrafica';
  Data.QTemp.Open;
  if Data.QTemp.FieldbyName('TotAnag').asInteger >= xMaxCons then begin
    MessageDlg('Gi� raggiunto il limite massimo di soggetti consentito', mtError, [mbOK], 0);
    Data.QTemp.Close;
  end else begin
    NuovoSoggettoForm := TNuovoSoggettoForm.create(self);
    NuovoSoggettoForm.ShowModal;
    if NuovoSoggettoForm.ModalResult = mrOK then begin
      Data.DB.BeginTrans;
      try
        // NUMERO CV
        if not cCVNumAutomatico then begin
          // manuale
          xCVNumero := '0';
          xNumeroValido := False;
          while not xNumeroValido do begin
            if not InputQuery('Numero CV manuale', 'Numero CV:', xCVNumero) then begin
              xVai := False;
              Break;
            end else begin
              if xCVNumero = '' then xCVNumero := '0';
              if xCVNumero = '0' then begin
                if MessageDlg('ATTENZIONE: Non � possibile inserire un n� CV uguale a ZERO' + chr(13) +
                  'Verr� associato il progressivo autoincrementante interno.' + chr(13) +
                  'Vuoi procedere (Yes) o Annullare l''inserimento (no) ?', mtWarning, [mbYes, mbNo], 0) = mrNo then begin
                  xVai := False;
                  Break;
                end;
              end;
              // controllo esistenza n� CV
              Data.QTemp.SQL.text := 'select Cognome,Nome from Anagrafica where CVNumero=' + xCVNumero;
              Data.QTemp.Open;
              if Data.QTemp.RecordCount > 0 then begin
                if MessageDlg('ATTENZIONE: esiste gi� il numero CV ' + xCVNumero + ' abbinato al soggetto:' + chr(13) +
                  Data.QTemp.FieldByName('Cognome').asString + ' ' + Data.QTemp.FieldByName('Nome').asString + chr(13) + chr(13) +
                  'Vuoi procedere (Yes) o Annullare l''inserimento (no) ?', mtWarning, [mbYes, mbNo], 0) = mrNo then begin
                  xVai := False;
                  break;
                end;
              end else xNumeroValido := True;
              Data.QTemp.Close;
            end;
          end;
        end else xCVNumero := '0'; // automatico

        Data.QTemp.Close;
        Data.QTemp.SQL.Text := 'insert into Anagrafica (CVNumero,Cognome,Nome,IDStato,IDTipoStato,IDProprietaCV) ' +
          '  values (:xCVNumero,:xCognome,:xNome,:xIDStato,:xIDTipoStato,:xIDProprietaCV)';
        Data.QTemp.ParamByName('xCVNumero').asInteger := StrtoInt(xCVNumero);
        Data.QTemp.ParamByName('xCognome').asString := NuovoSoggettoForm.ECogn.Text;
        Data.QTemp.ParamByName('xNome').asString := NuovoSoggettoForm.ENome.Text;
        Data.QTemp.ParamByName('xIDStato').asInteger := 28;
        Data.QTemp.ParamByName('xIDTipoStato').asInteger := 2;
        Data.QTemp.ParamByName('xIDProprietaCV').asInteger := 0;
        Data.QTemp.ExecSQL;

        Data.QTemp.SQL.text := 'select @@IDENTITY as LastID';
        Data.QTemp.Open;
        xIDAnag := Data.QTemp.FieldByName('LastID').asInteger;
        Data.QTemp.Close;

        // log inserimento
        Log_Operation(xIDUtenteAttuale, 'Anagrafica', xIDAnag, 'I');

        // ALTRE INFO
        Data.QTemp.SQL.text := 'insert into AnagAltreInfo (IDAnagrafica,Note) ' +
          ' values (:xIDAnagrafica,:xNote)';
        Data.QTemp.ParamByName('xIDAnagrafica').AsInteger := xIDAnag;
        Data.QTemp.ParamByName('xNote').AsString := NuovoSoggettoForm.ENote.text;
        Data.QTemp.ExecSQL;

        // inserimento vero e proprio
        if DataRicerche.QAziendeIDAttivita.asString = '' then xSettore := '0'
        else xSettore := DataRicerche.QAziendeIDAttivita.asString;
        Data2.QTemp.SQL.clear;
        Data2.QTemp.SQL.Add('insert into EsperienzeLavorative (IDAnagrafica,IDAzienda,IDSettore,Attuale,IDMansione,IDArea) ' +
          'values (' + IntToStr(xIDAnag) + ',' +
          DataRicerche.QAziendeID.asString + ',' +
          xSettore + ',1,:xIDMansione,:xIDArea)');
        Data2.QTemp.ParamByName('xIDMansione').asInteger := NuovoSoggettoForm.xIDRuolo;
        Data2.QTemp.ParamByName('xIDArea').asInteger := NuovoSoggettoForm.xIDArea;
        Data2.QTemp.ExecSQL;
        DataRicerche.QCandAzienda.Close;
        DataRicerche.QCandAzienda.Open;

        // riga nello storico con evento specifico
        Data.QTemp.SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente) ' +
          'values (:xIDAnagrafica,:xIDEvento,:xDataEvento,:xAnnotazioni,:xIDRicerca,:xIDUtente)';
        Data.QTemp.ParambyName('xIDAnagrafica').asInteger := xIDAnag;
        Data.QTemp.ParambyName('xIDEvento').asInteger := 87;
        Data.QTemp.ParambyName('xDataEvento').asDateTime := Date;
        Data.QTemp.ParambyName('xAnnotazioni').asString := 'azienda: ' + DataRicerche.QAziendeDescrizione.asString;
        Data.QTemp.ParambyName('xIDRicerca').asInteger := 0;
        Data.QTemp.ParambyName('xIDUtente').asInteger := xIDUtenteAttuale;
        Data.QTemp.ExecSQL;

        Data.DB.CommitTrans;
        DataRicerche.QCandAzienda.Close;
        DataRicerche.QCandAzienda.Open;
        DisegnaTVAziendaCand;
      except
        Data.DB.RollbackTrans;
        MessageDlg('ATTENZIONE: errore sul database - Inserimento non avvenuto', mtError, [mbOK], 0);
      end;
    end;
    NuovoSoggettoForm.Free;
  end;
end;

procedure TMainForm.LOBTabNazioniClick(Sender: TObject);
begin
  if not CheckProfile('791') then Exit;
  PageControl1.ActivePage := TSTabelle;
  PageControl3.ActivePage := TSTabNaz;
  Data.QTipiStrada.Open;
  LOBTab1.Down := False;
  LOBTab2.Down := False;
  LOBTab3.Down := False;
  LOBTab4.Down := False;
  LOBTab7.Down := False;
  LOBTab8.Down := False;
  LOBTabAnn.Down := False;
  LOBTabAttiv.Down := False;
  LOBTabAziende.Down := False;
  LOBTab11.Down := False;
  LOBTabComuni.Down := False;
  LOBTabNazioni.Down := True;
  Caption := '[M/791] - ' + xCaption;
end;

procedure TMainForm.ECercaNazioneChange(Sender: TObject);
begin
  Data.QNazioni.Close;
  Data.QNazioni.ParamByName('xDesc').asString := ECercaNazione.text + '%';
  Data.QNazioni.Open;
end;

procedure TMainForm.BNazNewClick(Sender: TObject);
begin
  NazioneForm := TNazioneForm.create(self);
  NazioneForm.ShowModal;
  if NazioneForm.ModalResult = mrOK then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'insert into Nazioni (Abbrev,DescNazione,AreaGeografica,DescNazionalita) ' +
          'values (:xAbbrev,:xDescNazione,:xAreaGeografica,:xDescNazionalita)';
        Q1.ParamByName('xAbbrev').asString := NazioneForm.EAbbrev.text;
        Q1.ParamByName('xDescNazione').asString := NazioneForm.EDesc.Text;
        Q1.ParamByName('xAreaGeografica').asString := NazioneForm.EArea.Text;
        Q1.ParamByName('xDescNazionalita').asString := NazioneForm.EdescNaz.Text;
        Q1.ExecSQL;
        DB.CommitTrans;
        ECercaNazioneChange(self);
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
  end;
  NazioneForm.Free;
end;

procedure TMainForm.BNazModClick(Sender: TObject);
begin
  NazioneForm := TNazioneForm.create(self);
  NazioneForm.EAbbrev.text := Data.QNazioniAbbrev.Value;
  NazioneForm.EDesc.text := Data.QNazioniDescNazione.Value;
  NazioneForm.EArea.text := Data.QNazioniAreaGeografica.Value;
  NazioneForm.EDescNaz.text := Data.QNazioniDescNazionalita.Value;
  NazioneForm.ShowModal;
  if NazioneForm.ModalResult = mrOK then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'update Nazioni set Abbrev=:xAbbrev,DescNazione=:xDescNazione,AreaGeografica=:xAreaGeografica,DescNazionalita=:xDescNazionalita ' +
          'where ID=' + Data.QNazioniID.asString;
        Q1.ParamByName('xAbbrev').asString := NazioneForm.EAbbrev.text;
        Q1.ParamByName('xDescNazione').asString := NazioneForm.EDesc.Text;
        Q1.ParamByName('xAreaGeografica').asString := NazioneForm.EArea.Text;
        Q1.ParamByName('xDescNazionalita').asString := NazioneForm.EdescNaz.Text;
        Q1.ExecSQL;
        DB.CommitTrans;
        ECercaNazioneChange(self);
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
  end;
  NazioneForm.Free;
end;

procedure TMainForm.BNazDelClick(Sender: TObject);
begin
  if not Data.QNazioni.Active then exit;
  // controllo associazione con le tabelle
  Data.QTemp.Close;
  Data.QTemp.SQL.text := 'select count(ID) Tot from Anagrafica where Stato=:xStato or DomicilioStato=:xDomicilioStato';
  Data.QTemp.ParamByName('xStato').asString := Data.QNazioniAbbrev.Value;
  Data.QTemp.ParamByName('xDomicilioStato').asString := Data.QNazioniAbbrev.Value;
  Data.QTemp.Open;
  if Data.QTemp.FieldByName('Tot').asInteger > 0 then begin
    MessageDlg('Nazione associata ad almeno un candidato ' + chr(13) +
      'IMPOSSIBILE CANCELLARE', mtError, [mbOK], 0);
    exit;
  end;
  Data.QTemp.Close;
  Data.QTemp.SQL.text := 'select count(ID) Tot from EBC_Clienti where NazioneAbb=:xNazioneAbb or NazioneAbbLegale=:xNazioneAbbLegale';
  Data.QTemp.ParamByName('xNazioneAbb').asString := Data.QNazioniAbbrev.Value;
  Data.QTemp.ParamByName('xNazioneAbbLegale').asString := Data.QNazioniAbbrev.Value;
  Data.QTemp.Open;
  if Data.QTemp.FieldByName('Tot').asInteger > 0 then begin
    MessageDlg('Nazione associata ad almeno un cliente ' + chr(13) +
      'IMPOSSIBILE CANCELLARE', mtError, [mbOK], 0);
    exit;
  end;
  Data.QTemp.Close;
  Data.QTemp.SQL.text := 'select count(ID) Tot from EBC_Clienti where NazioneAbb=:xNazioneAbb';
  Data.QTemp.ParamByName('xNazioneAbb').asString := Data.QNazioniAbbrev.Value;
  Data.QTemp.Open;
  if Data.QTemp.FieldByName('Tot').asInteger > 0 then begin
    MessageDlg('Nazione associata ad almeno un''azienda in archivio ' + chr(13) +
      'IMPOSSIBILE CANCELLARE', mtError, [mbOK], 0);
    exit;
  end;

  Data.QTemp.Close;

  if MessageDlg('Sei sicuro di voler eliminare la Nazione ?', mtWarning,
    [mbNo, mbYes], 0) = mrYes then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'delete from Nazioni ' +
          'where ID=' + QNazioniID.AsString;
        Q1.ExecSQL;
        DB.CommitTrans;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
    ECercaNazioneChange(self);
  end;
end;

procedure TMainForm.BAnagSelNazione1Click(Sender: TObject);
begin
  SelNazioneForm := TSelNazioneForm.create(self);
  SelNazioneForm.ShowModal;
  if (SelNazioneForm.ModalResult = mrOK) and (SelNazioneForm.QNazioni.RecordCount > 0) then begin
    Data.TAnagraficaStato.Value := SelNazioneForm.QNazioniDescNazione.Value;
  end else begin
    Data.TAnagraficaStato.Value := '';
  end;
  with Data.TAnagrafica do begin
    Data.DB.BeginTrans;
    try
      ApplyUpdates;
      Data.DB.CommitTrans;
    except
      Data.DB.RollbackTrans;
      MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      raise;
    end;
    CommitUpdates;
  end;
  SelNazioneForm.Free;
end;

procedure TMainForm.BAnagSelNazione2Click(Sender: TObject);
begin
  SelNazioneForm := TSelNazioneForm.create(self);
  SelNazioneForm.ShowModal;
  if (SelNazioneForm.ModalResult = mrOK) and (SelNazioneForm.QNazioni.RecordCount > 0) then begin
    Data.TAnagraficaDomicilioStato.Value := SelNazioneForm.QNazioniDescNazione.Value;
  end else begin
    Data.TAnagraficaDomicilioStato.Value := '';
  end;
  with Data.TAnagrafica do begin
    Data.DB.BeginTrans;
    try
      ApplyUpdates;
      Data.DB.CommitTrans;
    except
      Data.DB.RollbackTrans;
      MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      raise;
    end;
    CommitUpdates;
  end;
  SelNazioneForm.Free;
end;

procedure TMainForm.BCliSelNazClick(Sender: TObject);
begin
  SelNazioneForm := TSelNazioneForm.create(self);
  SelNazioneForm.ShowModal;
  if (SelNazioneForm.ModalResult = mrOK) and (SelNazioneForm.QNazioni.RecordCount > 0) then begin
    Data2.TEBCClientiNazioneAbb.Value := SelNazioneForm.QNazioniAbbrev.Value;
  end else begin
    Data2.TEBCClientiNazioneAbb.Value := '';
  end;
  SelNazioneForm.Free;
end;

procedure TMainForm.TbBAnagMansModClick(Sender: TObject);
begin
  if not CheckProfile('032') then Exit;
  if Data.TAnagMansioni.RecordCount = 0 then exit;
  ModRuoloForm := TModRuoloForm.create(self);
  ModRuoloForm.SEAnniEsp.Value := Data.TAnagMansioniAnniEsperienza.Value;
  ModRuoloForm.CBAttuale.checked := Data.TAnagMansioniAttuale.Value;
  ModRuoloForm.CBDisponibile.checked := Data.TAnagMansioniDisponibile.Value;
  ModRuoloForm.CBAspirazione.checked := Data.TAnagMansioniAspirazione.Value;
  ModRuoloForm.Caption := 'Ruolo: ' + Data.TAnagMansioniMansione.Value;
  ModRuoloForm.ShowModal;
  if ModRuoloForm.Modalresult = mrOK then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'update AnagMansioni set AnniEsperienza=:xAnniEsperienza,Attuale=:xAttuale,Disponibile=:xDisponibile,' +
          'Aspirazione=:xAspirazione where ID=' + Data.TAnagMansioniID.AsString;
        Q1.ParamByName('xAnniEsperienza').asInteger := Round(ModRuoloForm.SEAnniEsp.Value);
        Q1.ParamByName('xAttuale').asBoolean := ModRuoloForm.CBAttuale.checked;
        Q1.ParamByName('xDisponibile').asBoolean := ModRuoloForm.CBDisponibile.checked;
        Q1.ParamByName('xAspirazione').asBoolean := ModRuoloForm.CBAspirazione.checked;
        Q1.ExecSQL;
        DB.CommitTrans;
        Log_Operation(xIDUtenteAttuale, 'AnagMansioni', Data.TAnagMansioniID.Value, 'U');
        Data.TAnagMansioni.Close;
        Data.TAnagMansioni.Open;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
  end;
end;

procedure TMainForm.DBGrid41DblClick(Sender: TObject);
begin
  TbBAnagMansModClick(self);
end;

procedure TMainForm.TbBCarattDipModClick(Sender: TObject);
var xPunti: string;
begin
  if not CheckProfile('035') then Exit;
  if Data2.TCarattDip.RecordCount = 0 then exit;
  xPunti := Data2.TCarattDipPunteggio.asString;
  if inputquery('Modifica punteggio caratteristica', 'Nuovo punteggio:', xPunti) then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'update AnagCaratteristiche set Punteggio=:xPunteggio ' +
          'where ID=' + Data2.TCarattDipID.asString;
        Q1.ParamByName('xPunteggio').asInteger := StrToIntDef(xPunti, 0);
        Q1.ExecSQL;
        DB.CommitTrans;
        Data2.TCarattDip.Close;
        Data2.TCarattDip.Open;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
  end;
end;

procedure TMainForm.BOrgNewStampaClick(Sender: TObject);
begin
  if not CheckProfile('63') then Exit;
  dxPrinter1.Preview(True, dxPrinter1Link1);
end;

procedure TMainForm.BStoricoAnagPosClick(Sender: TObject);
begin
  if not CheckProfile('67') then Exit;
  StoricoAnagPosForm := TStoricoAnagPosForm.create(self);
  StoricoAnagPosForm.StoricoAnagPosFrame1.xIDOrganigramma := DataRicerche.QOrgNewID.Value;
  StoricoAnagPosForm.ShowModal;
  StoricoAnagPosForm.Free;
end;

procedure TMainForm.StoricoPosizioni91Click(Sender: TObject);
begin
  StoricoAnagPosForm := TStoricoAnagPosForm.create(self);
  StoricoAnagPosForm.StoricoAnagPosFrame1.xIDAnagrafica := longint(TVAziendaCand.Selected.Data);
  StoricoAnagPosForm.StoricoAnagPosFrame1.PanButtons.Visible := False;
  StoricoAnagPosForm.ShowModal;
  StoricoAnagPosForm.Free;
end;

procedure TMainForm.Storicoposizione1Click(Sender: TObject);
begin
  BStoricoAnagPosClick(self);
end;

procedure TMainForm.Eliminasoggettoassociato1Click(Sender: TObject);
begin
  if DataRicerche.QOrgNewIDDipendente.AsString <> '' then
    BOrgCandDelClick(self);
end;

procedure TMainForm.PopupOrgPopup(Sender: TObject);
begin
  if (not DataRicerche.QOrgNew.Active) or (DataRicerche.QOrgNew.RecordCount = 0) then begin
    Eliminasoggettoassociato1.Enabled := False;
    Storicoposizione1.Enabled := False;
  end else begin
    Eliminasoggettoassociato1.Enabled := True;
    Storicoposizione1.Enabled := True;
  end;
end;

procedure TMainForm.PageControl8Change(Sender: TObject);
begin
  if PageControl8.ActivePage = TSListini then begin
    ListinoAnnunciFrame1.QEdizioni.Open;
    ListinoAnnunciFrame1.QListinoEdiz.ParamByName('xIDCliente').asInteger := 0;
    ListinoAnnunciFrame1.QListinoEdiz.Open;
    ListinoAnnunciFrame1.xIDCliente := 0;
    ListinoAnnunciFrame1.RxDBGrid1.Columns[7].Visible := False;
    ListinoAnnunciFrame1.RxDBGrid2.Columns[7].Visible := False;
    ListinoAnnunciFrame1.BModPrezzoQuestoCli.visible := False;
    ListinoAnnunciFrame1.QMinPrezziTestata.Open;
    ListinoAnnunciFrame1.QMinPrezziTot.Open;
  end else begin
    ListinoAnnunciFrame1.QListinoEdiz.Close;
    ListinoAnnunciFrame1.QEdizioni.Close;
    ListinoAnnunciFrame1.QMinPrezziTestata.Close;
    ListinoAnnunciFrame1.QMinPrezziTot.Close;
  end;
  if PageControl8.ActivePage = TsTabContrattiAnn then begin
    ContrattiAnnunciFrame.QAnnContratti.Open;
  end else begin
    ContrattiAnnunciFrame.QAnnContratti.Close;
  end;
  if PageControl8.ActivePage = TSTabAnnTipi then begin
    FrmTipiAnnunci.QTipiAnnunci.Open;
  end else begin
    FrmTipiAnnunci.QTipiAnnunci.Close;
  end;
end;

procedure TMainForm.TbBEdizTestOKClick(Sender: TObject);
begin
  EdizioneForm := TEdizioneForm.create(self);
  EdizioneForm.ENomeEdizione.Text := DataAnnunci.QEdizioniTestNomeEdizione.Value;
  EdizioneForm.ENote.text := DataAnnunci.QEdizioniTestNote.Value;
  while (EdizioneForm.QConcessID.Value <> DataAnnunci.QEdizioniTestIDCliente.Value) and (not EdizioneForm.QConcess.EOF) do
    EdizioneForm.QConcess.Next;
  EdizioneForm.ShowModal;
  if EdizioneForm.ModalResult = mrOK then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'update Ann_Edizioni set NomeEdizione=:xNomeEdizione,IDCLiente=:xIDCliente,Note=:xNote ' +
          'where ID=' + DataAnnunci.QEdizioniTestID.asString;
        Q1.ParamByName('xNomeEdizione').asString := EdizioneForm.ENomeEdizione.Text;
        Q1.ParamByName('xIDCliente').asInteger := EdizioneForm.QConcessID.Value;
        Q1.ParamByName('xNote').asString := EdizioneForm.ENote.text;
        Q1.ExecSQL;
        DB.CommitTrans;
        DataAnnunci.QEdizioniTest.Close;
        DataAnnunci.QEdizioniTest.Open;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
  end;
  EdizioneForm.Free;
end;

procedure TMainForm.SpeedButton12Click(Sender: TObject);
begin
  ComuniForm := TComuniForm.create(self);
  ComuniForm.EProv.text := xUltimaProv;
  ComuniForm.ShowModal;
  if ComuniForm.ModalResult = mrOK then begin
    Data2.TEBCClientiComune.Value := ComuniForm.QComuniDescrizione.Value;
    Data2.TEBCClientiCap.Value := ComuniForm.QComuniCap.Value;
    Data2.TEBCClientiProvincia.Value := ComuniForm.QComuniProv.Value;
  end else begin
    Data2.TEBCClientiComune.Value := '';
    Data2.TEBCClientiCap.Value := '';
    Data2.TEBCClientiProvincia.Value := '';
  end;
  Data2.TEBCClienti.Post;
  xUltimaProv := ComuniForm.EProv.text;
  ComuniForm.Free;
end;

procedure TMainForm.SpeedButton13Click(Sender: TObject);
var xIDAnnuncio: integer;
  xRifAnnuncio: string;
begin
  xRifAnnuncio := DataAnnunci.TAnnunciRif.Value;
  xIDAnnuncio := DataAnnunci.TAnnunciID.Value;
  ElencoUtentiForm := TElencoUtentiForm.create(self);
  ElencoUtentiForm.xIDUtente := xIDUtenteAttuale;
  ElencoUtentiForm.ShowModal;
  if ElencoUtentiForm.ModalResult = mrOK then begin
    DataAnnunci.TAnnunci.Edit;
    DataAnnunci.TAnnunciIDUtenteProj.Value := ElencoUtentiForm.QUsersID.Value;
    DataAnnunci.TAnnunci.Post;
  end;
  ElencoUtentiForm.Free;
end;

procedure TMainForm.BAnnSelSedeClick(Sender: TObject);
var xIDAnnuncio: integer;
  xRifAnnuncio: string;
begin
  xRifAnnuncio := DataAnnunci.TAnnunciRif.Value;
  xIDAnnuncio := DataAnnunci.TAnnunciID.Value;
  ElencoSediForm := TElencoSediForm.create(self);
  ElencoSediForm.ShowModal;
  if ElencoSediForm.ModalResult = mrOK then begin
    DataAnnunci.TAnnunci.Edit;
    DataAnnunci.TAnnunciIDSede.Value := ElencoSediForm.QSediID.Value;
    DataAnnunci.TAnnunci.Post;
  end;
  ElencoSediForm.Free;
end;

procedure TMainForm.BAnnTipoClick(Sender: TObject);
var xIDAnnuncio: integer;
  xRifAnnuncio: string;
begin
  xRifAnnuncio := DataAnnunci.TAnnunciRif.Value;
  xIDAnnuncio := DataAnnunci.TAnnunciID.Value;
  ElencoTipiAnnunciForm := TElencoTipiAnnunciForm.create(self);
  ElencoTipiAnnunciForm.FrmTipiAnnunci.QTipiAnnunci.Open;
  if DataAnnunci.TAnnunciIDTipoAnnuncio.AsString <> '' then
    while (ElencoTipiAnnunciForm.FrmTipiAnnunci.QTipiAnnunciID.value <> DataAnnunci.TAnnunciIDTipoAnnuncio.Value) and
      (not ElencoTipiAnnunciForm.FrmTipiAnnunci.QTipiAnnunci.EOF) do
      ElencoTipiAnnunciForm.FrmTipiAnnunci.QTipiAnnunci.Next;
  ElencoTipiAnnunciForm.ShowModal;
  if ElencoTipiAnnunciForm.ModalResult = mrOK then begin
    DataAnnunci.TAnnunci.Edit;
    DataAnnunci.TAnnunciIDTipoAnnuncio.Value := ElencoTipiAnnunciForm.FrmTipiAnnunci.QTipiAnnunciID.Value;
    DataAnnunci.TAnnunci.Post;
  end;
  ElencoTipiAnnunciForm.Free;
end;

procedure TMainForm.ListinoAnnunciFrame1ToolbarButton972Click(
  Sender: TObject);
begin
  ListinoAnnunciFrame1.ToolbarButton972Click(Sender);

end;

procedure TMainForm.ClienteAnnunciFrameToolbarButton971Click(Sender: TObject);
begin
  if not CheckProfile('3') then Exit;
  LOBAnnunciClick(self);

  if not DataAnnunci.TAnnunci.locate('ID', ClienteAnnunciFrame.QAnnunciCliIDAnnuncio.value, []) then begin
    showMessage('Annuncio non trovato');
    exit;
  end;

  PCAnnunci.activepage := TSAnnDate;
  DataAnnunci.QAnnEdizDataXann.Open;
  if DataAnnunci.QAnnEdizDataXann.RecordCount > 0 then
    DataAnnunci.QAnnAnagData.Open;
  // posizionati sulla pubblicazione
  if not DataAnnunci.QAnnEdizDataXAnn.Locate('ID', ClienteAnnunciFrame.QAnnunciCliID.Value, []) then
    showMessage('Pubblicazione non trovato');
end;

procedure TMainForm.BTipiStradaNewClick(Sender: TObject);
begin
  TipoStradaForm := TTipoStradaForm.create(self);
  TipoStradaForm.ShowModal;
  if TipoStradaForm.ModalResult = mrOK then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'insert into TipiStrade (TipoStrada,NazioneAbb) ' +
          'values (:xTipoStrada,:xNazioneAbb)';
        Q1.ParamByName('xTipoStrada').asString := TipoStradaForm.ETipo.Text;
        Q1.ParamByName('xNazioneAbb').asString := TipoStradaForm.QNazioniAbbrev.Value;
        Q1.ExecSQL;
        DB.CommitTrans;
        Data.QTipiStrada.Close;
        Data.QTipiStrada.Open;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
  end;
  TipoStradaForm.Free;
end;

procedure TMainForm.BTipiStradaModClick(Sender: TObject);
begin
  TipoStradaForm := TTipoStradaForm.create(self);
  TipoStradaForm.ETipo.text := Data.QTipiStradaTipoStrada.Value;
  while (TipoStradaForm.QNazioniAbbrev.Value <> Data.QTipiStradaNazioneAbb.Value) and (not TipoStradaForm.QNazioni.EOF) do
    TipoStradaForm.QNazioni.Next;
  TipoStradaForm.ShowModal;
  if TipoStradaForm.ModalResult = mrOK then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'update TipiStrade set TipoStrada=:xTipoStrada,NazioneAbb=:xNazioneAbb ' +
          'where ID=' + Data.QTipiStradaID.asString;
        Q1.ParamByName('xTipoStrada').asString := TipoStradaForm.ETipo.Text;
        Q1.ParamByName('xNazioneAbb').asString := TipoStradaForm.QNazioniAbbrev.Value;
        Q1.ExecSQL;
        DB.CommitTrans;
        Data.QTipiStrada.Close;
        Data.QTipiStrada.Open;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
  end;
  TipoStradaForm.Free;
end;

procedure TMainForm.BTipiStradaDelClick(Sender: TObject);
begin
  if Data.QTipiStrada.IsEmpty then exit;
  if MessageDlg('Sei sicuro di voler eliminare il tipo di strada ?' + chr(13) + chr(13) +
    'NOTA: NON verranno cancellati i tipi gi� inseriti nelle varie anagrafiche', mtWarning, [mbNo, mbYes], 0) = mrYes then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'delete from TipiStrade ' +
          'where ID=' + Data.QTipiStradaID.asString;
        Q1.ExecSQL;
        DB.CommitTrans;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
    ECercaNazioneChange(self);
  end;
end;

procedure TMainForm.TbBPubbModDataClick(Sender: TObject);
var xIDAnnEdizData: integer;
begin
  if not CheckProfile('350') then Exit;
  SelEdizForm := TSelEdizForm.create(self);
  SelEdizForm.xIDCliente := DataAnnunci.TAnnunciIDCliente.Value;
  SelEdizForm.xCliente := DataAnnunci.TAnnunciCliente.Value;
  // riempimento campi
  SelEdizForm.DEData.Date := DataAnnunci.QAnnEdizDataXAnnData.Value;
  SelEdizForm.SENumModuli.Value := DataAnnunci.QAnnEdizDataXAnnNumModuli.Value;
  SelEdizForm.ListinoLire.Value := DataAnnunci.QAnnEdizDataXAnnListinoLire.Value;
  SelEdizForm.ListinoEuro.Value := DataAnnunci.QAnnEdizDataXAnnListinoEuro.Value;
  SelEdizForm.AlClienteLire.Value := DataAnnunci.QAnnEdizDataXAnnCostoLire.Value;
  SelEdizForm.AlClienteEuro.Value := DataAnnunci.QAnnEdizDataXAnnCostoEuro.Value;
  SelEdizForm.AnoiLire.Value := DataAnnunci.QAnnEdizDataXAnnCostoANoiLire.Value;
  SelEdizForm.AnoiEuro.Value := DataAnnunci.QAnnEdizDataXAnnCostoANoiEuro.Value;
  SelEdizForm.ScontoAlClienteLire.Value := DataAnnunci.QAnnEdizDataXAnnScontoAlClienteLire.Value;
  SelEdizForm.ScontoAlClienteEuro.Value := DataAnnunci.QAnnEdizDataXAnnScontoAlClienteEuro.Value;
  SelEdizForm.ScontoAnoiLire.Value := DataAnnunci.QAnnEdizDataXAnnScontoANoiLire.Value;
  SelEdizForm.ScontoAnoiEuro.Value := DataAnnunci.QAnnEdizDataXAnnScontoANoiEuro.Value;
  SelEdizForm.PCListini.visible := False;
  SelEdizForm.BCopiaValori.Enabled := False;
  SelEdizForm.RGOpzioneListino.visible := False;
  // Listino riservato al cliente
  SelEdizForm.ListinoAnnunciFrame1.QListinoEdiz.ParamByName('xIDCliente').asInteger := DataAnnunci.TAnnunciIDCliente.Value;

  SelEdizForm.ShowModal;
  if SelEdizForm.ModalResult = mrOK then begin
    Data.DB.BeginTrans;
    try
      Data.Q1.Close;
      Data.Q1.SQL.text := 'update Ann_AnnEdizData set Data=:xData,NumModuli=:xNumModuli,' +
        '       CostoLire=:xCostoLire,CostoEuro=:xCostoEuro,CostoANoiLire=:xCostoANoiLire,CostoANoiEuro=:xCostoANoiEuro,ListinoLire=:xListinoLire,ListinoEuro=:xListinoEuro, ' +
        '       ScontoAlClienteLire=:xScontoAlClienteLire,ScontoAlClienteEuro=:xScontoAlClienteEuro,ScontoANoiLire=:xScontoANoiLire,ScontoANoiEuro=:xScontoANoiEuro ' +
        ' where ID=' + DataAnnunci.QAnnEdizDataXAnnID.AsString;
      Data.Q1.ParambyName('xData').asDateTime := SelEdizForm.DEData.Date;
      Data.Q1.ParambyName('xNumModuli').asInteger := Round(SelEdizForm.SENumModuli.value);
      Data.Q1.ParambyName('xCostoLire').asFloat := SelEdizForm.AlClienteLire.Value;
      Data.Q1.ParambyName('xCostoEuro').asFloat := SelEdizForm.AlClienteEuro.Value;
      Data.Q1.ParambyName('xCostoANoiLire').asFloat := SelEdizForm.AnoiLire.Value;
      Data.Q1.ParambyName('xCostoANoiEuro').asFloat := SelEdizForm.AnoiEuro.Value;
      Data.Q1.ParambyName('xListinoLire').asFloat := SelEdizForm.ListinoLire.Value;
      Data.Q1.ParambyName('xListinoEuro').asFloat := SelEdizForm.ListinoEuro.Value;
      Data.Q1.ParambyName('xScontoAlClienteLire').asFloat := SelEdizForm.ScontoAlClienteLire.Value;
      Data.Q1.ParambyName('xScontoAlClienteEuro').asFloat := SelEdizForm.ScontoAlClienteEuro.Value;
      Data.Q1.ParambyName('xScontoANoiLire').asFloat := SelEdizForm.ScontoAnoiLire.Value;
      Data.Q1.ParambyName('xScontoANoiEuro').asFloat := SelEdizForm.ScontoAnoiEuro.Value;
      Data.Q1.ExecSQL;

      // aggiornamento CONTROLLO DI GESTIONE (se esiste il file...)
      if copy(Data.GlobalCheckBoxIndici.Value, 6, 1) = '1' then begin
        // crea la voce di costo
        Data2.QTemp.SQL.Text := 'update CG_ContrattiCosti set ImportoPrev=:xImportoPrev ' +
          ' where IDAnnEdizData=:xIDAnnEdizData';
        Data2.QTemp.ParamByName('xImportoPrev').asFloat := SelEdizForm.AnoiEuro.Value;
        Data2.QTemp.ParamByName('xIDAnnEdizData').asInteger := DataAnnunci.QAnnEdizDataXAnnID.Value;
        Data2.QTemp.execSQL;
        // crea la voce di ricavo
        Data2.QTemp.SQL.Text := 'update CG_ContrattiRicavi set ImportoPrev=:xImportoPrev ' +
          ' where IDAnnEdizData=:xIDAnnEdizData';
        Data2.QTemp.ParamByName('xImportoPrev').asFloat := SelEdizForm.AlClienteEuro.Value;
        Data2.QTemp.ParamByName('xIDAnnEdizData').asInteger := DataAnnunci.QAnnEdizDataXAnnID.Value;
        Data2.QTemp.execSQL;
        Data.Q1.Close;
      end;

      Data.DB.CommitTrans;

      AggTotAnnuncio(DataAnnunci.TAnnunciID.Value);
      DataAnnunci.QAnnEdizDataXann.Close;
      DataAnnunci.QAnnEdizDataXann.Open;
      if not DataAnnunci.QAnnAnagData.active then
        DataAnnunci.QAnnAnagData.Open;
    except
      Data.DB.RollbackTrans;
      MessageDlg('ERRORE SUL DATABASE:  operazione non completata', mtError, [mbOK], 0);
    end;
  end;
  SelEdizForm.Free;
end;

procedure TMainForm.DBCBTipoStradaCliDropDown(Sender: TObject);
begin
  // tipo di strada in funzione della Nazione
  with Data do begin
    Q1.Close;
    Q1.SQL.text := 'select TipoStrada from TipiStrade where NazioneAbb=:xNazioneAbb';
    if Data2.TEBCClientiNazioneAbb.Value <> '' then
      Q1.ParamByName('xNazioneAbb').asString := Data2.TEBCClientiNazioneAbb.value
    else Q1.ParamByName('xNazioneAbb').asString := GlobalNazioneAbbAzienda.Value;
    Q1.Open;
    DBCBTipoStradaCli.Items.Clear;
    while not Q1.EOF do begin
      DBCBTipoStradaCli.Items.Add(Q1.FieldByName('TipoStrada').asString);
      Q1.next;
    end;
    Q1.Close;
  end;
end;

procedure TMainForm.DBCBTipoStradaResDropDown(Sender: TObject);
begin
  // tipo di strada in funzione della Nazione - RESIDENZA
  with Data do begin
    Q1.Close;
    Q1.SQL.text := 'select TipoStrada from TipiStrade where NazioneAbb=:xNazioneAbb';
    if TAnagraficaStato.Value <> '' then
      Q1.ParamByName('xNazioneAbb').asString := TAnagraficaStato.Value
    else Q1.ParamByName('xNazioneAbb').asString := GlobalNazioneAbbAzienda.Value;
    Q1.Open;
    DBCBTipoStradaRes.Items.Clear;
    while not Q1.EOF do begin
      DBCBTipoStradaRes.Items.Add(Q1.FieldByName('TipoStrada').asString);
      Q1.next;
    end;
    Q1.Close;
  end;
end;

procedure TMainForm.DBCBTipoStradaDomDropDown(Sender: TObject);
begin
  // tipo di strada in funzione della Nazione - DOMICILIO
  with Data do begin
    Q1.Close;
    Q1.SQL.text := 'select TipoStrada from TipiStrade where NazioneAbb=:xNazioneAbb';
    if TAnagraficaDomicilioStato.Value <> '' then
      Q1.ParamByName('xNazioneAbb').asString := TAnagraficaDomicilioStato.Value
    else Q1.ParamByName('xNazioneAbb').asString := GlobalNazioneAbbAzienda.Value;
    Q1.Open;
    DBCBTipoStradaDom.Items.Clear;
    while not Q1.EOF do begin
      DBCBTipoStradaDom.Items.Add(Q1.FieldByName('TipoStrada').asString);
      Q1.next;
    end;
    Q1.Close;
  end;
end;

procedure TMainForm.BFileDaModelloClick(Sender: TObject);
var xFile: string;
  xSalva, xStampa: boolean;
begin
  if not CheckProfile('072') then Exit;
  SceltaModelloAnagForm := TSceltaModelloAnagForm.create(self);
  SceltaModelloAnagForm.Height := 238;
  SceltaModelloAnagForm.QModelliWord.SQL.text := 'select * from ModelliWord ' +
    'where TabellaMaster=:xTabMaster';
  SceltaModelloAnagForm.QModelliWord.ParamByName('xTabMaster').asString := 'Anagrafica';
  SceltaModelloAnagForm.QModelliWord.Open;
  SceltaModelloAnagForm.ShowModal;
  if SceltaModelloAnagForm.ModalResult = mrOK then begin
    xsalva := SceltaModelloAnagForm.CBSalva.Checked;
    xStampa := False;
    xFile := CreaFileWordAnag(SceltaModelloAnagForm.QModelliWordID.Value,
      SceltaModelloAnagForm.QModelliWordNomeModello.Value,
      xSalva, xStampa);
    if xsalva then begin
      // associa al soggetto
      with Data do begin
        DB.BeginTrans;
        try
          Q1.Close;
          Q1.SQL.text := 'insert into AnagFile (IDAnagrafica,IDTipo,Descrizione,NomeFile,DataCreazione) ' +
            'values (:xIDAnagrafica,:xIDTipo,:xDescrizione,:xNomeFile,:xDataCreazione)';
          Q1.ParamByName('xIDAnagrafica').asInteger := Data.TAnagraficaID.Value;
          Q1.ParamByName('xIDTipo').asInteger := 3;
          Q1.ParamByName('xDescrizione').asString := SceltaModelloAnagForm.QModelliWordDescrizione.Value;
          Q1.ParamByName('xNomeFile').asString := xFile;
          Q1.ParamByName('xDataCreazione').asDateTime := Date;
          Q1.ExecSQL;
          DB.CommitTrans;
        except
          DB.RollbackTrans;
          MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
        end;
      end;
      Data.QAnagFile.Close;
      Data.QAnagFile.Open;
    end;
  end;
  SceltaModelloAnagForm.Free;
end;

procedure TMainForm.LOBTabModelliWordClick(Sender: TObject);
begin
  if not CheckProfile('792') then Exit;
  GestModelliWordForm := TGestModelliWordForm.create(self);
  GestModelliWordForm.ShowModal;
  GestModelliWordForm.Free;
end;

procedure TMainForm.SpeedButton7Click(Sender: TObject);
var xID, xIDReparto: string;
begin
  if DataRicerche.QOrgNewIDReparto.asString = '' then xID := ''
  else xID := DataRicerche.QOrgNewIDReparto.asString;
  xIDReparto := OpenSelFromTab('Reparti', 'ID', 'Reparto', 'Nome reparto', xID);
  if xIDReparto = '' then exit;
  with Data do begin
    DB.BeginTrans;
    try
      Q1.Close;
      Q1.SQL.text := 'update Organigramma set IDReparto=:xIDReparto ' +
        'where ID=' + DataRicerche.QOrgNewID.asString;
      Q1.ParamByName('xIDReparto').asInteger := StrToInt(xIDReparto);
      Q1.ExecSQL;
      DB.CommitTrans;
      DataRicerche.QOrgNew.Close;
      DataRicerche.QOrgNew.Open;
    except
      DB.RollbackTrans;
      MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
    end;
  end;
end;

procedure TMainForm.ToolbarButton9742Click(Sender: TObject);
var xArrayStd, xArrayNew: array[1..100] of integer;
  i, k, j: integer;
begin
  if not CheckProfile('6') then Exit;
  if Data2.TEBCclienti.IsEmpty then exit;
  with DataRicerche do begin
    QOrgNew.Close;
    QAziende.close;
    QAziende.SQL.Text := 'select EBC_Clienti.* ,EBC_attivita.Attivita from EBC_Clienti,EBC_attivita ' +
      'where EBC_Clienti.IDAttivita=EBC_attivita.ID and EBC_Clienti.ID=' + Data2.TEBCClientiID.asString;
    QAziende.Open;
    QOrgNew.Open;
    // se non c'� l'organigramma ==> proponi quello standard
    if QOrgNew.IsEmpty then begin
      if MessageDlg('Per questa azienda non risulta impostato l''organigramma ' + chr(13) +
        'VUOI ASSOCIARE L''ORGANIGRAMMA STANDARD ?', mtWarning, [mbYes, mbNo], 0) = mrYes then begin
        Data.Qtemp.Close;
        Data.Qtemp.SQL.text := 'select * from OrgStandard order by PARENT';
        Data.Qtemp.Open;
        k := 1;
        while not Data.Qtemp.EOF do begin
          Data.DB.BeginTrans;
          try
            Data.Q1.Close;
            Data.Q1.SQL.text := 'insert into Organigramma (Tipo,Descrizione,DescBreve,Interim,InStaff,' +
              '                                       WIDTH,HEIGHT,TYPE,COLOR,IMAGE,IMAGEALIGN,ORDINE,ALIGN,IDAzienda) ' +
              'values (:xTipo,:xDescrizione,:xDescBreve,:xInterim,:xInStaff,:xWIDTH,:xHEIGHT,:xTYPE,' +
              '        :xCOLOR,:xIMAGE,:xIMAGEALIGN,:xORDINE,:xALIGN,:xIDAzienda)';
            Data.Q1.ParamByName('xTipo').asString := Data.Qtemp.FieldByName('Tipo').asString;
            Data.Q1.ParamByName('xDescrizione').asString := Data.Qtemp.FieldByName('Descrizione').asString;
            Data.Q1.ParamByName('xDescBreve').asString := Data.Qtemp.FieldByName('DescBreve').asString;
            Data.Q1.ParamByName('xInterim').asBoolean := Data.Qtemp.FieldByName('Interim').asBoolean;
            Data.Q1.ParamByName('xInStaff').asBoolean := Data.Qtemp.FieldByName('InStaff').asBoolean;
            Data.Q1.ParamByName('xWIDTH').asInteger := Data.Qtemp.FieldByName('WIDTH').asInteger;
            Data.Q1.ParamByName('xHEIGHT').asInteger := Data.Qtemp.FieldByName('HEIGHT').asInteger;
            Data.Q1.ParamByName('xTYPE').asString := Data.Qtemp.FieldByName('TYPE').asstring;
            Data.Q1.ParamByName('xCOLOR').asInteger := Data.Qtemp.FieldByName('COLOR').asInteger;
            Data.Q1.ParamByName('xIMAGE').asInteger := Data.Qtemp.FieldByName('IMAGE').asInteger;
            Data.Q1.ParamByName('xIMAGEALIGN').asString := Data.Qtemp.FieldByName('IMAGEALIGN').asstring;
            Data.Q1.ParamByName('xORDINE').asInteger := Data.Qtemp.FieldByName('ORDINE').asInteger;
            Data.Q1.ParamByName('xALIGN').asString := Data.Qtemp.FieldByName('ALIGN').asString;
            Data.Q1.ParamByName('xIDAzienda').asInteger := Data2.TEBCClientiID.Value;
            Data.Q1.ExecSQL;
            //
            Data.Q2.SQL.text := 'select @@IDENTITY as LastID';
            Data.Q2.Open;
            xArrayStd[k] := Data.QTemp.FieldByName('ID').asInteger;
            xArrayNew[k] := Data.Q2.FieldByName('LastID').asInteger;
            Data.Q2.Close;

            Data.DB.CommitTrans;
          except
            Data.DB.RollbackTrans;
            MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
          end;
          inc(k);
          Data.Qtemp.Next;
        end;
        // PARENT
        Data.Q2.Close;
        Data.Q2.SQL.text := 'select ID from Organigramma where IDAzienda=' + Data2.TEBCClientiID.asString + ' order by ID';
        Data.Q2.Open;
        Data.Qtemp.First;
        Data.Q2.Next;
        Data.Qtemp.Next;
        k := 1;
        while not Data.Qtemp.EOF do begin
          // trova nuovo PARENT nell'xArrayNew
          for i := 1 to 100 do
            if xArrayStd[i] = Data.QTemp.FieldByName('PARENT').asInteger then break;

          Data.DB.BeginTrans;
          try
            Data.Q1.Close;
            Data.Q1.SQL.text := 'update Organigramma set PARENT=:xPARENT ' +
              'where ID=' + IntToStr(Data.Q2.FieldByName('ID').asInteger);
            Data.Q1.ParamByName('xPARENT').asInteger := xArrayNew[i];
            Data.Q1.ExecSQL;
            Data.DB.CommitTrans;
          except
            Data.DB.RollbackTrans;
            MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
          end;
          inc(k);
          Data.Qtemp.Next;
          Data.Q2.Next;
        end;
        Data.Qtemp.Close;
        Data.Q2.Close;
      end;
      QOrgNew.Close;
      QOrgNew.Open;
    end;
    PColor.Color := DataRicerche.QOrgNewCOLOR.Value;
  end;
  DisegnaTVAziendaCand;
  PageControl1.ActivePage := TSAziende;
end;

procedure TMainForm.BGruppiOffNewClick(Sender: TObject);
begin
  if not CheckProfile('140') then exit;
  ContrattoForm := TContrattoForm.create(self);
  ContrattoForm.DEData.date := Date;
  ContrattoForm.ShowModal;
  if ContrattoForm.ModalResult = mrOK then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'insert into Contratti (Codice,IDCliente,Data,Descrizione,Stato) ' +
          'values (:xCodice,:xIDCliente,:xData,:xDescrizione,:xStato)';
        Q1.ParamByName('xCodice').asString := ContrattoForm.ECodice.text;
        Q1.ParamByName('xIDCliente').asInteger := Data2.TEBCClientiID.Value;
        Q1.ParamByName('xData').asDateTime := ContrattoForm.DEData.Date;
        Q1.ParamByName('xDescrizione').asString := ContrattoForm.EDesc.text;
        Q1.ParamByName('xStato').asString := ContrattoForm.EStato.text;
        Q1.ExecSQL;
        DB.CommitTrans;
        Data2.QCliContratti.Close;
        Data2.QCliContratti.Open;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
  end;
  ContrattoForm.Free;
end;

procedure TMainForm.BGruppiOffModClick(Sender: TObject);
begin
  if not CheckProfile('141') then exit;
  if Data2.QCliContratti.IsEmpty then exit;
  ContrattoForm := TContrattoForm.create(self);
  ContrattoForm.ECodice.text := Data2.QCliContrattiCodice.Value;
  ContrattoForm.DEData.Date := Data2.QCliContrattiData.Value;
  ContrattoForm.EDesc.text := Data2.QCliContrattiDescrizione.Value;
  ContrattoForm.EStato.text := Data2.QCliContrattiStato.Value;
  ContrattoForm.ShowModal;
  if ContrattoForm.ModalResult = mrOK then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'update Contratti set Codice=:xCodice,Data=:xData,Descrizione=:xDescrizione,Stato=:xStato ' +
          'where ID=' + Data2.QCliContrattiID.AsString;
        Q1.ParamByName('xCodice').asString := ContrattoForm.ECodice.text;
        Q1.ParamByName('xData').asDateTime := ContrattoForm.DEData.Date;
        Q1.ParamByName('xDescrizione').asString := ContrattoForm.EDesc.text;
        Q1.ParamByName('xStato').asString := ContrattoForm.EStato.text;
        Q1.ExecSQL;
        DB.CommitTrans;
        Data2.QCliContratti.Close;
        Data2.QCliContratti.Open;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
  end;
  ContrattoForm.Free;
end;

procedure TMainForm.BGruppiOffDelClick(Sender: TObject);
begin
  if not CheckProfile('142') then exit;
  // integrit� referenziale con le offerte
  if not VerifIntegrRef('EBC_Offerte', 'IDContratto', Data2.QCliContrattiID.asString) then exit;
  if Data2.QCliContratti.IsEmpty then exit;
  if MessageDlg('Sei sicuro di voler eliminare il contratto selezionato ?', mtWarning, [mbNo, mbYes], 0) = mrYes then
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'delete from Contratti ' +
          'where ID=' + Data2.QCliContrattiID.AsString;
        Q1.ExecSQL;
        DB.CommitTrans;
        Data2.QCliContratti.Close;
        Data2.QCliContratti.Open;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
end;

procedure TMainForm.SpeedButton14Click(Sender: TObject);
var xFileName: string;
begin
  if DataAnnunci.TAnnunciFileTesto.Value = '' then exit;
  xFileName := DataAnnunci.TAnnunciFileTesto.Value;
  if length(ExtractFileName(xFileName)) > 8 then // nomi lunghi
    xFileName := ExtractShortPathName(xFileName);
  ShellExecute(0, 'Open', pchar(xFileName), '', '', SW_SHOW);


  {if Uppercase(ExtractFileExt(xFileName))='.DOC' then
       ApriFileWordFromFile(xFileName);
  //  WinExec(pointer('winword.exe '+),SW_SHOW);
  if Uppercase(ExtractFileExt(xFileName))='.XLS' then
       WinExec(pointer('Excel.exe '+xFileName),SW_SHOW);
  if Uppercase(ExtractFileExt(xFileName))='.MDB' then
       WinExec(pointer('Msaccess.exe '+xFileName),SW_SHOW);
  if Uppercase(ExtractFileExt(xFileName))='.TXT' then
       WinExec(pointer('notepad.exe '+xFileName),SW_SHOW);
  if (Uppercase(ExtractFileExt(xFileName))='.GIF')or
       (Uppercase(ExtractFileExt(xFileName))='.JPG')or
       (Uppercase(ExtractFileExt(xFileName))='.BMP') then
       WinExec(pointer('acdsee32.exe '+xFileName),SW_SHOW);
  if Uppercase(ExtractFileExt(xFileName))='.PDF' then
       WinExec(pointer('AcroRd32.exe '+xFileName),SW_SHOW);}
end;

procedure TMainForm.BRifAnnuncioClick(Sender: TObject);
begin
  if DataAnnunci.TAnnunci.IsEmpty then exit;
  DataAnnunci.TAnnunci.locate('Rif', ERifAnnuncio.text, [loPartialKey]);
end;

procedure TMainForm.ERifAnnuncioKeyPress(Sender: TObject; var Key: Char);
begin
  if key = chr(13) then BRifAnnuncioClick(self);
end;

procedure TMainForm.BStatoCivileClick(Sender: TObject);
begin
  OpenSelFromTab('StatiCivili', 'ID', 'Descrizione', 'Stato civile', '');
  Data.TStatiCivili.Close;
  Data.TStatiCivili.Open;
end;

procedure TMainForm.BJobDescOKClick(Sender: TObject);
begin
  Data2.QJobDesc.Post;
end;

procedure TMainForm.BJobDescCanClick(Sender: TObject);
begin
  Data2.QJobDesc.Cancel;
end;

procedure TMainForm.BNoteStampaClick(Sender: TObject);
begin
  {     MemoNoteCand.Lines.clear;
       MemoNoteCand.Lines:=DBMemo3.Lines;
       PrintMemoConIntestazione(MemoNoteCand,'Note interne di '+
            data.TAnagraficaCognome.value+' '+data.TAnagraficaNome.value);
  }
end;

procedure TMainForm.BAnagIncompNewClick(Sender: TObject);
var xDesc: string;
begin
  SelClienteForm := TSelClienteForm.create(self);
  SelClienteForm.ShowModal;
  if SelClienteForm.ModalResult = mrOK then begin
    xDesc := '';
    if InputQuery('Incompatibilit� con cliente', 'descrizione:', xDesc) then begin
      with Data do begin
        DB.BeginTrans;
        try
          Q1.Close;
          Q1.SQL.text := 'insert into AnagIncompClienti (IDAnagrafica,IDCliente,Descrizione) ' +
            'values (:xIDAnagrafica,:xIDCliente,:xDescrizione)';
          Q1.ParamByName('xIDAnagrafica').asInteger := Data.TAnagraficaID.Value;
          Q1.ParamByName('xIDCliente').asInteger := SelClienteForm.TClientiID.Value;
          Q1.ParamByName('xDescrizione').asString := xDesc;
          Q1.ExecSQL;
          DB.CommitTrans;
          DataRicerche.QAnagIncompClienti.Close;
          DataRicerche.QAnagIncompClienti.Open;
        except
          DB.RollbackTrans;
          MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
        end;
      end;
    end;
  end;
  SelClienteForm.Free;
end;

procedure TMainForm.BAnagIncompDelClick(Sender: TObject);
begin
  if DataRicerche.QAnagIncompClienti.IsEmpty then exit;
  if MessageDlg('Sei sicuro di voler eliminare l''associazione di incompatibilit� con il cliente ?', mtWarning, [mbNo, mbYes], 0) = mrYes then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'delete from AnagIncompClienti ' +
          'where ID=' + DataRicerche.QAnagIncompClientiID.AsString;
        Q1.ExecSQL;
        DB.CommitTrans;
        DataRicerche.QAnagIncompClienti.Close;
        DataRicerche.QAnagIncompClienti.Open;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
  end;
end;

procedure TMainForm.BAnagIncompModDescClick(Sender: TObject);
var xDesc: string;
  xID: integer;
begin
  if DataRicerche.QAnagIncompClienti.IsEmpty then exit;
  xDesc := DataRicerche.QAnagIncompClientiDescrizione.Value;
  xID := DataRicerche.QAnagIncompClientiID.Value;
  if InputQuery('Incompatibilit� con cliente', 'descrizione', xDesc) then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'update AnagIncompClienti set Descrizione=:xDescrizione ' +
          'where ID=' + DataRicerche.QAnagIncompClientiID.AsString;
        Q1.ParamByName('xDescrizione').asString := xDesc;
        Q1.ExecSQL;
        DB.CommitTrans;
        DataRicerche.QAnagIncompClienti.Close;
        DataRicerche.QAnagIncompClienti.Open;
        DataRicerche.QAnagIncompClienti.Locate('ID', xID, []);
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
  end;
end;

procedure TMainForm.BCliRefCliNewClick(Sender: TObject);
var xDesc: string;
begin
  if not CheckProfile('111') then exit;
  SelClienteForm := TSelClienteForm.create(self);
  SelClienteForm.ShowModal;
  if SelClienteForm.ModalResult = mrOK then begin
    xDesc := '';
    if InputQuery('Relazione con il cliente', 'descrizione:', xDesc) then begin
      with Data do begin
        DB.BeginTrans;
        try
          Q1.Close;
          Q1.SQL.text := 'insert into ClienteLinkCliente (IDCliente,IDClienteRef,Descrizione,CheckCand) ' +
            'values (:xIDCliente,:xIDClienteRef,:xDescrizione,:xCheckCand)';
          Q1.ParamByName('xIDCliente').asInteger := Data2.TEBCClientiID.Value;
          Q1.ParamByName('xIDClienteRef').asInteger := SelClienteForm.TClientiID.Value;
          Q1.ParamByName('xDescrizione').asString := xDesc;
          Q1.ParamByName('xCheckCand').asBoolean := True;
          Q1.ExecSQL;
          DB.CommitTrans;
          Data2.QCliRefCli.Close;
          Data2.QCliRefCli.Open;
        except
          DB.RollbackTrans;
          MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
        end;
      end;
    end;
  end;
  SelClienteForm.Free;
end;

procedure TMainForm.BCliRefCliDElClick(Sender: TObject);
begin
  if not CheckProfile('112') then exit;
  if Data2.QCliRefCli.IsEmpty then exit;
  if MessageDlg('Sei sicuro di voler eliminare l''associazione con il cliente selezionato ?', mtWarning, [mbNo, mbYes], 0) = mrYes then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'delete from ClienteLinkCliente ' +
          'where ID=' + Data2.QCliRefCliID.AsString;
        Q1.ExecSQL;
        DB.CommitTrans;
        Data2.QCliRefCli.Close;
        Data2.QCliRefCli.Open;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
  end;
end;

procedure TMainForm.BCliRefCliModDescClick(Sender: TObject);
var xDesc: string;
  xID: integer;
begin
  if not CheckProfile('113') then exit;
  if Data2.QCliRefCli.IsEmpty then exit;
  xDesc := Data2.QCliRefCliDescrizione.Value;
  xID := Data2.QCliRefCliID.value;
  if InputQuery('Relazione con cliente', 'descrizione', xDesc) then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'update ClienteLinkCliente set Descrizione=:xDescrizione ' +
          'where ID=' + Data2.QCliRefCliID.AsString;
        Q1.ParamByName('xDescrizione').asString := xDesc;
        Q1.ExecSQL;
        DB.CommitTrans;
        Data2.QCliRefCli.Close;
        Data2.QCliRefCli.Open;
        Data2.QCliRefCli.Locate('ID', xID, []);
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
  end;
end;

procedure TMainForm.DBGrid34DblClick(Sender: TObject);
var xID: integer;
begin
  if Data2.QCliRefCli.IsEmpty then exit;
  xID := Data2.QCliRefCliID.value;
  with Data do begin
    DB.BeginTrans;
    try
      Q1.Close;
      Q1.SQL.text := 'update ClienteLinkCliente set CheckCand=:xCheckCand ' +
        'where ID=' + Data2.QCliRefCliID.AsString;
      Q1.ParamByName('xCheckCand').asboolean := not Data2.QCliRefCliCheckCand.Value;
      Q1.ExecSQL;
      DB.CommitTrans;
      Data2.QCliRefCli.Close;
      Data2.QCliRefCli.Open;
      Data2.QCliRefCli.Locate('ID', xID, []);
    except
      DB.RollbackTrans;
      MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
    end;
  end;
end;

procedure TMainForm.Save(ADefaultExt, AFilter, AFileName: string; AMethod: TSaveMethod);
begin
  with SaveDialog do begin
    DefaultExt := ADefaultExt;
    Filter := AFilter;
    FileName := AFileName;
    if Execute then
      AMethod(FileName, True);
  end;
end;

procedure TMainForm.BSaveToExcelClick(Sender: TObject);
begin
  Save('xls', 'Microsoft Excel 4.0 Worksheet (*.xls)|*.xls', 'EBCAttivita.xls', dxDBGAttivita.SaveToXLS);
end;

procedure TMainForm.BMotNonAccClick(Sender: TObject);
begin
  if not CheckProfile('7903') then Exit;
  OpenSelFromTab('MotiviNonAccettaz', 'ID', 'Motivo', 'Motivo', '');
end;

procedure TMainForm.BCopiaDaDomClick(Sender: TObject);
begin
  if not (Data.DsAnagrafica.State in [dsEdit, dsInsert]) then Data.TAnagrafica.Edit;
  Data.TAnagraficaIndirizzo.Value := Data.TAnagraficaDomicilioIndirizzo.Value;
  Data.TAnagraficaCap.Value := Data.TAnagraficaDomicilioCap.Value;
  Data.TAnagraficaComune.Value := Data.TAnagraficaDomicilioComune.Value;
  Data.TAnagraficaProvincia.Value := Data.TAnagraficaDomicilioProvincia.Value;
  Data.TAnagraficaIDComuneRes.Value := Data.TAnagraficaIDComuneDom.Value;
  Data.TAnagraficaIDZonaRes.Value := Data.TAnagraficaIDZonaDom.Value;
  Data.TAnagraficaTipoStrada.Value := Data.TAnagraficaDomicilioTipoStrada.Value;
  Data.TAnagraficaNumCivico.Value := Data.TAnagraficaDomicilioNumCivico.Value;
  Data.TAnagraficaStato.Value := Data.TAnagraficaDomicilioStato.Value;
end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
  {Set form1's window proc back to it's original procedure}
  //sleep(10);
  SetWindowLong(MainForm.Handle,
    GWL_WNDPROC,
    LongInt(OldWindowProc));
end;

procedure TMainForm.BTabLivLingueClick(Sender: TObject);
begin
  OpenTab('LivelloLingue', ['ID', 'LivelloConoscenza', 'LivelloConoscenza_ENG'], ['Livello', 'Livello (ENG)']);
  //OpenSelFromTab('LivelloLingue','ID','LivelloConoscenza','Livello conoscenza','');
end;

procedure TMainForm.DBCheckBox3Exit(Sender: TObject);
var xPassword, xUtenteResp: string;
begin
  if not DBCheckBox3.Checked then begin
    // se non � abilitato ==> blocco attivo
    // --> per sbloccare � necessaria la password di amministratore
    xPassword := '';
    xUtenteResp := GetDescUtenteResp(xIDUtenteAttuale);
    if not InputQuery('Modifica flag "Protezione 1"', 'Password di ' + xUtenteResp, xPassword) then begin
      MessageDlg('OPERAZIONE ANNULLATA', mtError, [mbOK], 0);
      Data2.TEBCclienti.Cancel;
      exit;
    end;
    if not CheckPassword(xPassword, GetPwdUtenteResp(xIDUtenteAttuale)) then begin
      MessageDlg('Password errata', mtError, [mbOK], 0);
      Data2.TEBCclienti.Cancel;
      exit;
    end;
  end;
  TbBClientiOKClick(self);
end;

procedure TMainForm.EseguiQRicAttive;
begin
  DataRicerche.QRicAttive.Close;
  DataRicerche.QRicAttive.SQL.Clear;
  {DataRicerche.QRicAttive.SQL.Text:='select distinct EBC_Ricerche.ID,Progressivo,EBC_Clienti.Descrizione Cliente,DataInizio,NumRicercati,TitoloCliente, '+
       'Mansioni.Descrizione Ruolo,Users.Nominativo Selezionatore,EBC_Ricerche.Stato,Conclusa,IDStatoRic,EBC_StatiRic.StatoRic,EBC_Ricerche.Tipo, '+
       'DataPrevChiusura,IDOfferta '+
       'from EBC_Ricerche,Mansioni,Users,EBC_Clienti,EBC_StatiRic '+
       'where EBC_Ricerche.IDMansione *= Mansioni.ID '+
       'and EBC_Ricerche.IDCliente=EBC_CLienti.ID '+
       'and EBC_Ricerche.IDUtente=Users.ID '+
       'and EBC_Ricerche.IDStatoRic=EBC_StatiRic.ID';
  if not CBRicUtentiTutti.Checked then
       DataRicerche.QRicAttive.SQL.Add('and EBC_Ricerche.IDUtente=:xIDUtente ');}
  DataRicerche.QRicAttive.SQL.text := 'select distinct EBC_Ricerche.ID,Progressivo,EBC_Clienti.Descrizione Cliente, ' +
    'DataInizio,NumRicercati,TitoloCliente, Mansioni.Descrizione Ruolo,Users.Nominativo Selezionatore, ' +
    'EBC_Ricerche.Stato,Conclusa,IDStatoRic,EBC_StatiRic.StatoRic,EBC_Ricerche.Tipo, DataPrevChiusura,IDOfferta ' +
    'from EBC_Ricerche,Mansioni,Users,EBC_Clienti,EBC_StatiRic,EBC_RicercheUtenti ' +
    'where EBC_Ricerche.IDMansione *= Mansioni.ID ' +
    'and EBC_Ricerche.ID = EBC_RicercheUtenti.IDRicerca ' +
    'and EBC_Ricerche.IDCliente=EBC_CLienti.ID ' +
    'and EBC_Ricerche.IDUtente=Users.ID ' +
    'and EBC_Ricerche.IDStatoRic=EBC_StatiRic.ID ';
  if not CBRicUtentiTutti.Checked then begin
    DataRicerche.QRicAttive.SQL.Add('and EBC_RicercheUtenti.IDUtente=:xIDUtente ');
    DataRicerche.QRicAttive.ParamByName('xIDUtente').asInteger := xIDUtenteAttuale;
  end;
  // ordinamento
  DataRicerche.QRicAttive.SQL.Add('order by Progressivo desc');
  // parametro per filtro utente
  ScriviRegistry('QRicAttive', DataRicerche.QRicAttive.SQL.text);
  DataRicerche.QRicAttive.Open;
  LTotRic.Caption := IntToStr(DataRicerche.QRicAttive.RecordCount);
end;

procedure TMainForm.dxDBGrid1DblClick(Sender: TObject);
begin
  BRiprendiRicClick(self);
end;

procedure TMainForm.dxDBGrid1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if (Button <> mbRight) or (Shift <> []) then Exit;
  TdxDBGridPopupMenuManager.Instance.ShowGridPopupMenu(Sender as TdxDBGrid);
end;

procedure TMainForm.dxDBGrid1CustomDrawCell(Sender: TObject;
  ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
  AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
  var AText: string; var AColor: TColor; AFont: TFont;
  var AAlignment: TAlignment; var ADone: Boolean);
begin
  if ANode.Values[dxDBGrid1Tipo.index] <> null then begin
    if Acolumn = dxDBGrid1Progressivo then begin
      Data.Q1.Close;
      Data.Q1.SQL.text := 'select Colore from EBC_TipiCommesse where TipoCommessa=:xTipoCommessa';
      Data.Q1.ParamByName('xTipoCommessa').asString := trim(ANode.Values[dxDBGrid1Tipo.index]);
      Data.Q1.Open;
      if Data.Q1.FieldByName('Colore').asString = 'rosso' then AColor := clRed;
      if Data.Q1.FieldByName('Colore').asString = 'verde' then AColor := clLime;
      if Data.Q1.FieldByName('Colore').asString = 'giallo' then AColor := clYellow;
      if Data.Q1.FieldByName('Colore').asString = 'azzurro' then AColor := clAqua;
      if Data.Q1.FieldByName('Colore').asString = 'fucsia' then AColor := clFuchsia;
      if Data.Q1.FieldByName('Colore').asString = 'bianco' then AColor := clWhite;
      if Data.Q1.FieldByName('Colore').asString = 'oliva' then AColor := clOlive;
      if Data.Q1.FieldByName('Colore').asString = 'grigio' then AColor := clSilver;
      if Data.Q1.FieldByName('Colore').asString = 'marrone' then AColor := clMaroon;
      Data.Q1.Close;
    end;
  end;

  // data prevista chiusura
  if AColumn = dxDBGrid1DataInizio then begin
    if (ANode.Values[dxDBGrid1dataPrevChiusura.index] <= Date) and
      (ANode.Values[dxDBGrid1dataPrevChiusura.index] > 0) then
      Afont.Color := clRed
    else Afont.Color := clBlack;
  end;
end;

procedure TMainForm.EsportainExcel1Click(Sender: TObject);
begin
  Save('xls', 'Microsoft Excel 4.0 Worksheet (*.xls)|*.xls', 'ExpGrid.xls', dxDBGrid1.SaveToXLS);
end;

procedure TMainForm.EsportainHTML1Click(Sender: TObject);
begin
  Save('htm', 'HTML File (*.htm; *.html)|*.htm', 'ExpGrid.htm', dxDBGrid1.SaveToHTML);
end;

procedure TMainForm.Stampagriglia1Click(Sender: TObject);
begin
  dxPrinter1.Preview(True, dxPrinter1Link2);
end;

procedure TMainForm.BAnnSedeDelClick(Sender: TObject);
begin
  DataAnnunci.TAnnunci.Edit;
  DataAnnunci.TAnnunciIDSede.Value := 0;
  DBEdit73.text := '';
  DataAnnunci.TAnnunci.Post;
end;

procedure TMainForm.BAnnCommModCodiceClick(Sender: TObject);
var xCod: string;
  xID: integer;
begin
  xCod := DataAnnunci.TAnnunciRicCodice.Value;
  if not InputQuery('Modifica codice commessa', 'Codice', xCod) then exit;
  with Data do begin
    DB.BeginTrans;
    try
      Q1.SQL.text := 'update Ann_AnnunciRicerche set Codice=:xCodice where ID=' + DataAnnunci.TAnnunciRicID.AsString;
      Q1.ParambyName('xCodice').asString := xCod;
      Q1.ExecSQL;
      xID := DataAnnunci.TAnnunciRicID.Value;
      DataAnnunci.TAnnunciRic.Close;
      DataAnnunci.TAnnunciRic.Open;
      DataAnnunci.TAnnunciRic.Locate('ID', xID, []);
      DB.CommitTrans;
    except
      DB.RollbackTrans;
      MessageDlg('ERRORE SUL DATABASE: operazione non effettuata', mtError, [mbOK], 0);
    end;
  end;
end;

procedure TMainForm.BAnnRuoloAddClick(Sender: TObject);
begin
  InsRuoloForm := TInsRuoloForm.create(self);
  InsRuoloForm.CBInsComp.Visible := False;
  InsRuoloForm.ShowModal;
  if InsRuoloForm.ModalResult = mrOK then begin
    DataAnnunci.TAnnunci.Edit;
    DataAnnunci.TAnnunciIDMansione.Value := InsRuoloForm.TRuoliID.Value;
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'INSERT INTO Ann_AnnunciRuoli (IDAnnuncio,IDMansione,Codice) ' +
          'values (:xIDAnnuncio,:xIDMansione,:xCodice)';
        Q1.ParamByName('xIDAnnuncio').asInteger := DataAnnunci.TannunciID.Value;
        Q1.ParamByName('xIDMansione').asInteger := InsRuoloForm.TRuoliID.Value;
        Q1.ParamByName('xCodice').asString := Uppercase(char(DataAnnunci.QAnnRuoli.RecordCount + Ord('a')));
        Q1.ExecSQL;
        DB.CommitTrans;
        DataAnnunci.QAnnRuoli.Close;
        DataAnnunci.QAnnRuoli.Open;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
        raise;
      end;
    end;
  end;
  InsRuoloForm.Free;
end;

procedure TMainForm.PCAnnunciDettChange(Sender: TObject);
begin
  if PCAnnunciDett.ActivePage = TSAnnRuoli then
    DataAnnunci.QAnnRuoli.Open
  else DataAnnunci.QAnnRuoli.Close;
end;

procedure TMainForm.BAnnRuoloDelClick(Sender: TObject);
begin
  //if not CheckProfile('34') then Exit;
  if DataAnnunci.QAnnRuoli.IsEmpty then exit;
  if MessageDlg('Sei sicuro di voler cancellare il riferimento ?', mtWarning, [mbNo, mbYes], 0) = mrNo then exit;
  Data.DB.BeginTrans;
  try
    Data.Q1.SQL.text := 'delete from Ann_AnnunciRuoli where ID=' + DataAnnunci.QAnnRuoliID.asString;
    Data.Q1.ExecSQL;
    Data.DB.CommitTrans;
    DataAnnunci.QAnnRuoli.Close;
    DataAnnunci.QAnnRuoli.Open;
  except
    Data.DB.RollbackTrans;
    MessageDlg('ERRORE SUL DATABASE:  operazione non completata', mtError, [mbOK], 0);
  end;
end;

procedure TMainForm.BAnnRuoloCodClick(Sender: TObject);
var xCod: string;
  xID: integer;
begin
  xCod := DataAnnunci.QAnnRuoliCodice.Value;
  if not InputQuery('Modifica codice ruolo', 'Codice', xCod) then exit;
  with Data do begin
    DB.BeginTrans;
    try
      Q1.SQL.text := 'update Ann_AnnunciRuoli set Codice=:xCodice where ID=' + DataAnnunci.QAnnRuoliID.AsString;
      Q1.ParambyName('xCodice').asString := xCod;
      Q1.ExecSQL;
      xID := DataAnnunci.QAnnRuoliID.Value;
      DataAnnunci.QAnnRuoli.Close;
      DataAnnunci.QAnnRuoli.Open;
      DataAnnunci.QAnnRuoli.Locate('ID', xID, []);
      DB.CommitTrans;
    except
      DB.RollbackTrans;
      MessageDlg('ERRORE SUL DATABASE: operazione non effettuata', mtError, [mbOK], 0);
    end;
  end;
end;

procedure TMainForm.BannDataModCodClick(Sender: TObject);
var xCod: string;
  xID: integer;
begin
  xCod := DataAnnunci.QAnnEdizDataXAnnCodice.Value;
  if not InputQuery('Modifica codice pubblicazione', 'Codice', xCod) then exit;
  with Data do begin
    DB.BeginTrans;
    try
      Q1.SQL.text := 'update Ann_AnnEdizData set Codice=:xCodice where ID=' + DataAnnunci.QAnnEdizDataXAnnID.AsString;
      Q1.ParambyName('xCodice').asString := xCod;
      Q1.ExecSQL;
      xID := DataAnnunci.QAnnEdizDataXAnnID.Value;
      DataAnnunci.QAnnEdizDataXAnn.Close;
      DataAnnunci.QAnnEdizDataXAnn.Open;
      DataAnnunci.QAnnEdizDataXAnn.Locate('ID', xID, []);
      DB.CommitTrans;
    except
      DB.RollbackTrans;
      MessageDlg('ERRORE SUL DATABASE: operazione non effettuata', mtError, [mbOK], 0);
    end;
  end;
end;

procedure TMainForm.BAnnEdizSoggAddClick(Sender: TObject);
var xIDRicerca, xIDMansione: integer;
begin
  if DataAnnunci.QAnnEdizDataXAnn.IsEmpty then exit;
  ElencoDipForm := TElencoDipForm.create(self);
  ElencoDipForm.ShowModal;
  if ElencoDipForm.ModalResult = mrOK then begin
    // controllo associazione
    Data.Q1.Close;
    Data.Q1.SQL.text := 'select count(*) Tot from Ann_AnagAnnEdizData where IDAnagrafica=:xIDAnagrafica and IDAnnEdizData=:xIDAnnEdizData ';
    Data.Q1.ParambyName('xIDAnagrafica').asInteger := ElencoDipForm.TAnagDipID.Value; ;
    Data.Q1.ParambyName('xIDAnnEdizData').asInteger := DataAnnunci.QAnnEdizDataXAnnID.Value;
    Data.Q1.Open;
    if Data.Q1.FieldByName('Tot').asInteger > 0 then begin
      MessageDlg('Pubblicazione gi� associata al soggetto - IMPOSSIBILE CONTINUARE', mtError, [mbOK], 0);
      Data.Q1.Close;
      ElencoDipForm.Free;
      exit;
    end;
    Data.Q1.Close;
    // rif. ricerca
    xIDRicerca := 0;
    xIDMansione := 0;
    Data.QTemp.Close;
    Data.QTemp.SQL.text := 'select Ann_AnnunciRicerche.ID,IDRicerca,EBC_Ricerche.IDMansione,Codice,EBC_Ricerche.Progressivo Rif, ' +
      'EBC_Ricerche.NumRicercati Num,Mansioni.Descrizione Ruolo,EBC_Clienti.Descrizione Cliente ' +
      'from Ann_AnnunciRicerche,EBC_Ricerche,Mansioni,EBC_Clienti ' +
      'where Ann_AnnunciRicerche.IDRicerca=EBC_Ricerche.ID and EBC_Ricerche.IDMansione=Mansioni.ID ' +
      'and EBC_Ricerche.IDCliente=EBC_Clienti.ID and Ann_AnnunciRicerche.IDAnnuncio=' + DataAnnunci.QAnnEdizDataXAnnIDAnnuncio.asString;
    Data.QTemp.Open;
    if not Data.QTemp.IsEmpty then begin
      SelFromQueryForm := TSelFromQueryForm.create(self);
      SelFromQueryForm.QGen.SQL.text := Data.QTemp.SQL.text;
      SelFromQueryForm.QGen.Open;
      SelFromQueryForm.xNumInvisibleCols := 3;
      SelFromQueryForm.Caption := 'seleziona ricerca/commessa associata all''annuncio';
      SelFromQueryForm.ShowModal;
      if SelFromQueryForm.ModalResult = mrOK then begin
        xIDRicerca := SelFromQueryForm.QGen.FieldByName('IDRicerca').asInteger;
        xIDMansione := SelFromQueryForm.QGen.FieldByName('IDMansione').asInteger;
      end;
      SelFromQueryForm.Free;
    end else begin
      xIDRicerca := Data.QTemp.FieldByName('IDRicerca').asInteger;
      xIDMansione := Data.QTemp.FieldByName('IDMansione').asInteger;
    end;
    Data.QTemp.Close;
    // rif ruolo
    Data.QTemp.SQL.text := 'select Ann_AnnunciRuoli.ID,Codice,IDMansione, Mansioni.Descrizione Ruolo ' +
      'from Ann_AnnunciRuoli,Mansioni where Ann_AnnunciRuoli.IDMansione=Mansioni.ID ' +
      'and Ann_AnnunciRuoli.IDAnnuncio=' + DataAnnunci.QAnnEdizDataXAnnIDAnnuncio.asString;
    Data.QTemp.Open;
    if Data.QTemp.RecordCount > 1 then begin
      SelFromQueryForm := TSelFromQueryForm.create(self);
      SelFromQueryForm.QGen.SQL.text := Data.QTemp.SQL.text;
      SelFromQueryForm.QGen.Open;
      SelFromQueryForm.xNumInvisibleCols := 1;
      SelFromQueryForm.Caption := 'seleziona ruolo associata all''annuncio';
      SelFromQueryForm.ShowModal;
      if SelFromQueryForm.ModalResult = mrOK then begin
        xIDMansione := SelFromQueryForm.QGen.FieldByName('IDMansione').asInteger;
      end;
      SelFromQueryForm.Free;
    end else begin
      if Data.QTemp.RecordCount = 1 then
        xIDMansione := Data.QTemp.FieldByName('IDMansione').asInteger;
    end;
    SelDataForm := TSelDataForm.create(self);
    SelDataForm.Caption := 'Data pervenuto';
    SelDataForm.DEData.Date := Date;
    SelDataForm.ShowModal;
    if SelDataForm.ModalResult = mrOK then begin
      with Data do begin
        DB.BeginTrans;
        try
          Q1.Close;
          Q1.SQL.text := 'insert into Ann_AnagAnnEdizData (IDAnagrafica,IDAnnEdizData,IDMansione,IDRicerca,DataPervenuto) ' +
            'values (:xIDAnagrafica,:xIDAnnEdizData,:xIDMansione,:xIDRicerca,:xDataPervenuto)';
          Q1.ParambyName('xIDAnagrafica').asInteger := ElencoDipForm.TAnagDipID.Value; ;
          Q1.ParambyName('xIDAnnEdizData').asInteger := DataAnnunci.QAnnEdizDataXAnnID.Value;
          Q1.ParambyName('xIDMansione').asInteger := xIDMansione;
          Q1.ParambyName('xIDRicerca').asInteger := xIDRicerca;
          Q1.ParambyName('xDataPervenuto').asDateTime := SelDataForm.DEData.Date;
          Q1.ExecSQL;
          DataAnnunci.QAnnAnagData.Close;
          DataAnnunci.QAnnAnagData.Open;
          DB.CommitTrans;
        except
          DB.RollbackTrans;
          MessageDlg('ERRORE SUL DATABASE: operazione non effettuata', mtError, [mbOK], 0);
        end;
      end;
    end;
  end;
  ElencoDipForm.Free;
end;

procedure TMainForm.BAnnEdizSoggDelClick(Sender: TObject);
begin
  if MessageDlg('Sei sicuro di voler eliminare l''associazione tra soggetto e pubblicazione ?', mtWarning,
    [mbNo, mbYes], 0) = mrYes then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'delete from Ann_AnagAnnEdizData where ID=' + DataAnnunci.QAnnAnagDataID.asString;
        Q1.ExecSQL;
        DataAnnunci.QAnnAnagData.Close;
        DataAnnunci.QAnnAnagData.Open;
        DB.CommitTrans;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
  end;
end;

procedure TMainForm.CompilaFilePresPosProposte(xPosizione: string; xIDCandRic, xIDCliente, xIDMansione: integer);
var xFileWord, xFileBase, xGiaInviati, xS, xFileWordAll, xTitolo, xInquadramento, xRetribuzione, xNaz: string;
  MSWord: Variant;
  i, xTot, xCRpos: integer;
  xProcedi, xRiempi, xAllegaFile: boolean;
  xcliente, xRifInterno, xIndirizzoCliente, xCAPComuneProv, xTipoRetrib, xTipoRetribEng, xAreaRuolo, xLingue, xBenefits: string;
begin
  // modello PROPOSTE
  xFileBase := GetDocPath + '\ModPresCandProposte.doc';

  if not FileExists(xFileBase) then begin
    MessageDlg('Modello di presentazione "ModPresCandProposte.doc" non trovato - IMPOSSIBILE CONTINUARE', mtError, [mbOK], 0);
    exit;
  end;

  try
    MsWord := CreateOleObject('Word.Basic');
  except
    ShowMessage('Non riesco ad aprire Microsoft Word.');
    Exit;
  end;
  xFileWord := GetDocPath + '\pc' + intToStr(xIDCandRic) + '.doc';

  xretribuzione := '   ';
  xInquadramento := '   ';
  xBenefits := '   ';
  Data.QAnagNote.Open;
  xRetribuzione := Data.QAnagNoteRetribuzione.Value + '          ';
  xInquadramento := Data.QAnagNoteInquadramento.Value + '          ';
  xBenefits := Data.QAnagNoteBenefits.Value + '          '; ;

  xNaz := Data.QAnagNoteNazionalita.Value + '    ';
  xTipoRetrib := Data.QAnagNoteTipoRetrib.Value;
  if xTipoRetrib <> '' then xTipoRetrib := '(' + xTipoRetrib + ')'
  else xTipoRetrib := '   ';
  xTipoRetribEng := Data.QAnagNoteTipoRetribEng.Value;
  if xTipoRetribEng <> '' then xTipoRetribEng := '(' + xTipoRetribEng + ')'
  else xTipoRetribEng := '   ';
  Data.QAnagNote.Close;

  xcliente := '       ';
  xRifInterno := '       ';
  xIndirizzoCliente := '       ';
  xCAPComuneProv := '       ';
  Data.Q1.Close;
  Data.Q1.SQL.text := 'select EBC_Clienti.Descrizione Cliente from EBC_Clienti ' +
    'where ID=' + IntToStr(xIDCliente);
  Data.Q1.Open;
  xcliente := Data.Q1.FieldByName('Cliente').asString + '    ';
  Data.Q1.Close;
  Data.Q1.SQL.text := 'select Aree.Descrizione area from Aree,Mansioni ' +
    'where Aree.ID=Mansioni.IDArea and Mansioni.ID=' + IntToStr(xIDMansione);
  Data.Q1.Open;
  xAreaRuolo := Data.Q1.FieldByName('Area').asString + ' - ' + xPosizione;
  Data.Q1.Close;

  xRiempi := True;
  if FileExists(xFileWord) then begin
    if MessageDlg('Il file esiste gi� - Aprirlo (Yes) o crearne uno nuovo, riscrivendo il vecchio (No) ?', mtWarning, [mbYes, mbNo], 0) = mrYes then begin
      MsWord.AppShow;
      MsWord.FileOpen(xFileWord); // file da aprire
      xRiempi := False; // non riempire i campi
    end else begin
      MsWord.AppShow;
      MsWord.FileOpen(xFileBase);
    end;
  end else begin
    MsWord.AppShow;
    MsWord.FileOpen(xFileBase);
  end;

  // riempimento campi
  if xRiempi then begin
    // pagina copertina
    MsWord.EditFind('#cliente');
    MsWord.Insert(xCliente);
    MsWord.EditFind('#arearuolo');
    MsWord.Insert(xAreaRuolo);
    MsWord.EditFind('#cognomeNome');
    MsWord.Insert(Data.TAnagraficaCognome.Value + ' ' + Data.TAnagraficaNome.Value);
    MsWord.EditFind('#data');
    MsWord.Insert(DateToStr(Date));
    // pagina 1
    MsWord.EditFind('#cognomeNome');
    MsWord.Insert(Data.TAnagraficaCognome.Value + ' ' + Data.TAnagraficaNome.Value);
    MsWord.EditFind('#LuogoDataNascita');
    MsWord.Insert(Data.TAnagraficaLuogoNascita.asString + ' ' + Data.TAnagraficaDataNascita.asString);
    MsWord.EditFind('#Residenza');
    MsWord.Insert(Data.TAnagraficaComune.Value + ' ' + Data.TAnagraficaProvincia.Value);
    MsWord.EditFind('#StatoCivile');
    MsWord.Insert(Data.TAnagraficaStatoCivile.Value + '  ');
    // FORMAZIONE
    Data.Q1.close;
    Data.Q1.SQL.text := 'select Tipo,Descrizione Titolo,LuogoConseguimento,DataConseguimento ' +
      'from TitoliStudio,Diplomi where TitoliStudio.IDDiploma=Diplomi.ID ' +
      'and IDAnagrafica=' + Data.TAnagraficaID.asString;
    Data.Q1.Open;
    if Data.Q1.RecordCount > 0 then begin
      MsWord.EditFind('#TitoliStudio');
      while not Data.Q1.EOF do begin
        xTitolo := '';
        xTitolo := Data.Q1.FieldByName('Titolo').asString;
        if Data.Q1.FieldByName('LuogoConseguimento').asString <> '' then
          xTitolo := xTitolo + '  conseguita presso: ' + Data.Q1.FieldByName('LuogoConseguimento').asString;
        if Data.Q1.FieldByName('DataConseguimento').asString <> '' then
          xTitolo := xTitolo + '  nel ' + Data.Q1.FieldByName('DataConseguimento').asString;
        MsWord.Insert(xTitolo);
        Data.Q1.Next;
        if not Data.Q1.EOF then MsWord.Insert(chr(13));
      end;
    end;
    // LINGUE
    Data.Q1.close;
    Data.Q1.SQL.text := 'select Lingua, LivParlato.livelloConoscenza LivelloParlato, LivScritto.livelloConoscenza LivelloScritto ' +
      'from LingueConosciute,LivelloLingue LivParlato, LivelloLingue LivScritto ' +
      'where LingueConosciute.Livello *= LivParlato.ID ' +
      '  and LingueConosciute.LivelloScritto *= LivScritto.ID ' +
      'and IDAnagrafica=' + Data.TAnagraficaID.asString;
    Data.Q1.Open;
    if Data.Q1.RecordCount > 0 then begin
      MsWord.EditFind('#lingue');
      while not Data.Q1.EOF do begin
        // prima colonna: la lingua
        xLingue := '';
        xLingue := Data.Q1.FieldByName('Lingua').asString;
        // solo conoscenza orale
        xLingue := xLingue + ' ' + Data.Q1.FieldByName('LivelloParlato').asString;
        MsWord.Insert(xLingue);
        Data.Q1.Next;
        if not Data.Q1.EOF then MsWord.Insert(chr(13));
      end;
    end;
    // PERCORSO PROFESSIONALE
    Data.Q1.close;
    Data.Q1.SQL.text := 'select EBC_Clienti.Descrizione Azienda, EBC_Clienti.Comune, EBC_Clienti.Provincia, ' +
      'EBC_Clienti.NumDipendenti, Attivita, Mansioni.Descrizione Posizione, ' +
      'AnnoDal,AnnoAl,DescrizioneMansione ' +
      'from EsperienzeLavorative,EBC_Clienti,Mansioni,EBC_Attivita ' +
      'where EsperienzeLavorative.IDAzienda=EBC_Clienti.ID ' +
      'and EsperienzeLavorative.IDMansione=Mansioni.ID ' +
      'and EsperienzeLavorative.IDSettore=EBC_Attivita.ID and IDAnagrafica=' + Data.TAnagraficaID.asString +
      'order by AnnoAl desc';
    Data.Q1.Open;
    MsWord.EditFind('#EspLav');
    while not Data.Q1.EOF do begin
      // prima colonna
      MsWord.Insert(Data.Q1.FieldByName('AnnoDal').asString + ' - ' + Data.Q1.FieldByName('AnnoAl').asString);
      // seconda colonna
      MsWord.NextCell;
      MsWord.Insert(Data.Q1.FieldByName('Azienda').asString + ' - ' + Data.Q1.FieldByName('Comune').asString + ' (' + Data.Q1.FieldByName('Provincia').asString + ') ' + chr(13));
      MsWord.Insert('(' + LowerCase(Data.Q1.FieldByName('Attivita').asString) + ')' + chr(13) + chr(13));
      MsWord.Insert(uppercase(Data.Q1.FieldByName('Posizione').asString) + chr(13) + chr(13));
      xS := Data.Q1.FieldByName('DescrizioneMansione').asString;
      if xs <> '' then begin
        while pos(chr(13), xS) > 0 do begin
          xCRpos := pos(chr(13), xS);
          MsWord.Insert(copy(xS, 1, xCRpos - 1));
          xS := copy(xS, xCRpos + 1, length(xS));
        end;
      end;
      MsWord.Insert(chr(13));
      Data.Q1.Next;
      if not Data.Q1.EOF then MsWord.NextCell; //MsWord.Insert(chr(13));
    end;
    Data.Q1.close;
    MsWord.EditFind('#inquadramento');
    MsWord.Insert(xInquadramento);
    MsWord.EditFind('#retribuzione');
    MsWord.Insert(xRetribuzione);
    MsWord.EditFind('#benefits');
    MsWord.Insert(xBenefits);


    MsWord.FileSaveAs(xFileWord);
    // aggiornamento AnagFile
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'insert into AnagFile (IDAnagrafica,IDTipo,Descrizione,NomeFile,DataCreazione) ' +
          'values (:xIDAnagrafica,:xIDTipo,:xDescrizione,:xNomeFile,:xDataCreazione)';
        Q1.ParamByName('xIDAnagrafica').asInteger := Data.TAnagraficaID.Value;
        Q1.ParamByName('xIDTipo').asInteger := 1;
        Q1.ParamByName('xDescrizione').asString := '';
        Q1.ParamByName('xNomeFile').asString := xFileWord;
        Q1.ParamByName('xDataCreazione').asDateTime := Date;
        Q1.ExecSQL;
        DB.CommitTrans;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
    if Data.QAnagFile.Active then begin
      Data.QAnagFile.Close;
      Data.QAnagFile.Open;
    end;
  end;
end;

procedure TMainForm.CompilaFilePresPosMCS(xPosizione: string; xIDCandRic, xIDCliente, xIDMansione: integer);
var xFileWord, xFileBase, xGiaInviati, xS, xFileWordAll, xTitolo, xInquadramento, xRetribuzione, xNaz: string;
  MSWord: Variant;
  i, xTot, xCRpos: integer;
  xProcedi, xRiempi, xAllegaFile: boolean;
  xcliente, xRifInterno, xIndirizzoCliente, xCAPComuneProv, xTipoRetrib, xTipoRetribEng, xAreaRuolo, xLingue, xBenefits: string;
begin
  // modello MCS
  xFileBase := GetDocPath + '\ModPresCandMCS.doc';

  if not FileExists(xFileBase) then begin
    MessageDlg('Modello di presentazione "ModPresCandMCS.doc" non trovato - IMPOSSIBILE CONTINUARE', mtError, [mbOK], 0);
    exit;
  end;

  try
    MsWord := CreateOleObject('Word.Basic');
  except
    ShowMessage('Non riesco ad aprire Microsoft Word.');
    Exit;
  end;
  xFileWord := GetDocPath + '\pc' + intToStr(xIDCandRic) + '.doc';

  xretribuzione := '   ';
  xInquadramento := '   ';
  xBenefits := '   ';
  Data.QAnagNote.Open;
  xRetribuzione := Data.QAnagNoteRetribuzione.Value + '          ';
  if Data.QAnagNoteBenefits.Value <> '' then
    xRetribuzione := xRetribuzione + ' + ' + Data.QAnagNoteBenefits.Value;
  xInquadramento := Data.QAnagNoteInquadramento.Value + '          ';
  xBenefits := Data.QAnagNoteBenefits.Value + '          '; ;

  xNaz := Data.QAnagNoteNazionalita.Value + '    ';
  xTipoRetrib := Data.QAnagNoteTipoRetrib.Value;
  if xTipoRetrib <> '' then xTipoRetrib := '(' + xTipoRetrib + ')'
  else xTipoRetrib := '   ';
  xTipoRetribEng := Data.QAnagNoteTipoRetribEng.Value;
  if xTipoRetribEng <> '' then xTipoRetribEng := '(' + xTipoRetribEng + ')'
  else xTipoRetribEng := '   ';
  Data.QAnagNote.Close;

  xcliente := '       ';
  xRifInterno := '       ';
  xIndirizzoCliente := '       ';
  xCAPComuneProv := '       ';
  Data.Q1.Close;
  Data.Q1.SQL.text := 'select EBC_Clienti.Descrizione Cliente from EBC_Clienti ' +
    'where ID=' + IntToStr(xIDCliente);
  Data.Q1.Open;
  xcliente := Data.Q1.FieldByName('Cliente').asString + '    ';
  Data.Q1.Close;
  Data.Q1.SQL.text := 'select Aree.Descrizione area from Aree,Mansioni ' +
    'where Aree.ID=Mansioni.IDArea and Mansioni.ID=' + IntToStr(xIDMansione);
  Data.Q1.Open;
  xAreaRuolo := Data.Q1.FieldByName('Area').asString + ' - ' + xPosizione;
  Data.Q1.Close;

  xRiempi := True;
  if FileExists(xFileWord) then begin
    if MessageDlg('Il file esiste gi� - Aprirlo (Yes) o crearne uno nuovo, riscrivendo il vecchio (No) ?', mtWarning, [mbYes, mbNo], 0) = mrYes then begin
      MsWord.AppShow;
      MsWord.FileOpen(xFileWord); // file da aprire
      xRiempi := False; // non riempire i campi
    end else begin
      MsWord.AppShow;
      MsWord.FileOpen(xFileBase);
    end;
  end else begin
    MsWord.AppShow;
    MsWord.FileOpen(xFileBase);
  end;

  // riempimento campi
  if xRiempi then begin
    // pagina copertina
    MsWord.EditFind('#posizione');
    MsWord.Insert(xPosizione);
    MsWord.EditFind('#NomeCognome');
    MsWord.Insert(Data.TAnagraficaNome.Value + ' ' + Data.TAnagraficaCognome.Value);
    MsWord.EditFind('#cliente');
    MsWord.Insert(xCliente);
    MsWord.EditFind('#data');
    MsWord.Insert(DateToStr(Date));
    // pagina 1
    MsWord.EditFind('#NomeCognome');
    MsWord.Insert(Data.TAnagraficaNome.Value + ' ' + Data.TAnagraficaCognome.Value);
    MsWord.EditFind('#Residenza');
    MsWord.Insert(Data.TAnagraficaTipoStrada.Value + ' ' + Data.TAnagraficaIndirizzo.Value + ' ' + Data.TAnagraficaNumCivico.Value + ' - ' +
      Data.TAnagraficaComune.Value + ' ' + Data.TAnagraficaProvincia.Value);
    MsWord.EditFind('#Telefono');
    if Data.TAnagraficaRecapitiTelefonici.Value <> '' then
      MsWord.Insert(Data.TAnagraficaRecapitiTelefonici.Value)
    else MsWord.Insert('---');
    MsWord.EditFind('#TelUfficio');
    if Data.TAnagraficaTelUfficio.Value <> '' then
      MsWord.Insert(Data.TAnagraficaTelUfficio.Value)
    else MsWord.Insert('---');
    MsWord.EditFind('#Cellulare');
    if Data.TAnagraficaCellulare.Value <> '' then
      MsWord.Insert(Data.TAnagraficaCellulare.Value)
    else MsWord.Insert('---');
    MsWord.EditFind('#Nascita');
    MsWord.Insert(Data.TAnagraficaLuogoNascita.asString + ' ' + Data.TAnagraficaDataNascita.asString);
    MsWord.EditFind('#StatoCivile');
    MsWord.Insert(Data.TAnagraficaStatoCivile.Value + '  ');

    // FORMAZIONE
    Data.Q1.close;
    Data.Q1.SQL.text := 'select Tipo,Descrizione Titolo,LuogoConseguimento,DataConseguimento,Votazione ' +
      'from TitoliStudio,Diplomi where TitoliStudio.IDDiploma=Diplomi.ID ' +
      'and IDAnagrafica=' + Data.TAnagraficaID.asString;
    Data.Q1.Open;
    if Data.Q1.RecordCount > 0 then begin
      MsWord.EditFind('#TitoliStudio');
      while not Data.Q1.EOF do begin
        xTitolo := '';
        xTitolo := Data.Q1.FieldByName('Titolo').asString;
        if Data.Q1.FieldByName('LuogoConseguimento').asString <> '' then
          xTitolo := xTitolo + ', ' + Data.Q1.FieldByName('LuogoConseguimento').asString;
        if Data.Q1.FieldByName('DataConseguimento').asString <> '' then
          xTitolo := xTitolo + ', ' + Data.Q1.FieldByName('DataConseguimento').asString;
        if Data.Q1.FieldByName('Votazione').asInteger > 0 then
          xTitolo := xTitolo + ', votazione: ' + Data.Q1.FieldByName('Votazione').asString;
        MsWord.Insert(xTitolo + '.');
        Data.Q1.Next;
        if not Data.Q1.EOF then MsWord.Insert(chr(13) + chr(13));
      end;
    end;
    // LINGUE
    Data.Q1.close;
    Data.Q1.SQL.text := 'select Lingua, livelloConoscenza ' +
      'from LingueConosciute,LivelloLingue ' +
      'where LingueConosciute.Livello *= LivelloLingue.ID ' +
      'and IDAnagrafica=' + Data.TAnagraficaID.asString;
    Data.Q1.Open;
    if Data.Q1.RecordCount > 0 then begin
      MsWord.EditFind('#lingue');
      while not Data.Q1.EOF do begin
        // prima colonna: la lingua
        xLingue := '';
        xLingue := Data.Q1.FieldByName('Lingua').asString + ': ' + Data.Q1.FieldByName('LivelloConoscenza').asString;
        MsWord.Insert(xLingue);
        Data.Q1.Next;
        if not Data.Q1.EOF then MsWord.Insert(chr(13));
      end;
    end;

    // pagina 2
    MsWord.EditFind('#NomeCognome');
    MsWord.Insert(Data.TAnagraficaNome.Value + ' ' + Data.TAnagraficaCognome.Value);
    // PERCORSO PROFESSIONALE
    Data.Q1.close;
    Data.Q1.SQL.text := 'select EBC_Clienti.Descrizione Azienda, EBC_Clienti.Comune, EBC_Clienti.Provincia, ' +
      'EBC_Clienti.NumDipendenti, Attivita, Mansioni.Descrizione Posizione, ' +
      'AnnoDal,AnnoAl,DescrizioneMansione,AziendaFatturato,AziendaNumDipendenti ' +
      'from EsperienzeLavorative,EBC_Clienti,Mansioni,EBC_Attivita ' +
      'where EsperienzeLavorative.IDAzienda=EBC_Clienti.ID ' +
      'and EsperienzeLavorative.IDMansione=Mansioni.ID ' +
      'and EsperienzeLavorative.IDSettore=EBC_Attivita.ID and IDAnagrafica=' + Data.TAnagraficaID.asString +
      'order by AnnoAl desc';
    Data.Q1.Open;
    MsWord.EditFind('#EspLav');
    while not Data.Q1.EOF do begin
      // prima colonna
      MsWord.Insert(Data.Q1.FieldByName('AnnoDal').asString + '/' + Data.Q1.FieldByName('AnnoAl').asString);
      // seconda colonna
      MsWord.NextCell;
      MsWord.Insert(Data.Q1.FieldByName('Azienda').asString + ' - ' + Data.Q1.FieldByName('Comune').asString + ' (' + Data.Q1.FieldByName('Provincia').asString + ') ' + chr(13));
      MsWord.Insert('Societ� operante nel settore: ' + LowerCase(Data.Q1.FieldByName('Attivita').asString) +
        ', con un fatturato annuo di ' + Data.Q1.FieldByName('AziendaFatturato').asString + ' miliardi, ed un ' +
        'organico pari a ' + Data.Q1.FieldByName('AziendaNumDipendenti').asString + ' dipendenti.' + chr(13) + chr(13));
      MsWord.Insert('In Qualit� di ' + Data.Q1.FieldByName('Posizione').asString + ':' + chr(13));
      xS := Data.Q1.FieldByName('DescrizioneMansione').asString;
      if xs <> '' then begin
        while pos(chr(13), xS) > 0 do begin
          xCRpos := pos(chr(13), xS);
          MsWord.Insert(copy(xS, 1, xCRpos - 1));
          xS := copy(xS, xCRpos + 1, length(xS));
        end;
      end;
      MsWord.Insert(chr(13));
      Data.Q1.Next;
      if not Data.Q1.EOF then MsWord.NextCell; //MsWord.Insert(chr(13));
    end;
    Data.Q1.close;

    // pagina 3
    MsWord.EditFind('#NomeCognome');
    MsWord.Insert(Data.TAnagraficaNome.Value + ' ' + Data.TAnagraficaCognome.Value);
    // inquadramento e retribuzione
    MsWord.EditFind('#retribuzione');
    MsWord.Insert(xRetribuzione);
    MsWord.EditFind('#inquadramento');
    MsWord.Insert(xInquadramento);


    MsWord.FileSaveAs(xFileWord);
    // aggiornamento AnagFile
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'insert into AnagFile (IDAnagrafica,IDTipo,Descrizione,NomeFile,DataCreazione) ' +
          'values (:xIDAnagrafica,:xIDTipo,:xDescrizione,:xNomeFile,:xDataCreazione)';
        Q1.ParamByName('xIDAnagrafica').asInteger := Data.TAnagraficaID.Value;
        Q1.ParamByName('xIDTipo').asInteger := 1;
        Q1.ParamByName('xDescrizione').asString := '';
        Q1.ParamByName('xNomeFile').asString := xFileWord;
        Q1.ParamByName('xDataCreazione').asDateTime := Date;
        Q1.ExecSQL;
        DB.CommitTrans;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
    if Data.QAnagFile.Active then begin
      Data.QAnagFile.Close;
      Data.QAnagFile.Open;
    end;
  end;
end;

procedure TMainForm.CompilaFilePresPosBoyden(xPosizione: string; xIDCandRic, xIDCliente, xIDMansione: integer);
var xFileWord, xFileBase, xGiaInviati, xS, xFileWordAll, xTitolo, xInquadramento, xRetribuzione, xNaz: string;
  MSWord: Variant;
  i, xTot, xCRpos: integer;
  xProcedi, xRiempi, xAllegaFile: boolean;
  xcliente, xRifInterno, xIndirizzoCliente, xCAPComuneProv, xTipoRetrib, xTipoRetribEng, xAreaRuolo, xLingue, xBenefits: string;
begin
  // modello MCS
  xFileBase := GetDocPath + '\ModPresCandBOYDEN.doc';

  if not FileExists(xFileBase) then begin
    MessageDlg('Modello di presentazione "ModPresCandBOYDEN.doc" non trovato - IMPOSSIBILE CONTINUARE', mtError, [mbOK], 0);
    exit;
  end;

  try
    MsWord := CreateOleObject('Word.Basic');
  except
    ShowMessage('Non riesco ad aprire Microsoft Word.');
    Exit;
  end;
  xFileWord := GetDocPath + '\pc' + intToStr(xIDCandRic) + '.doc';

  xretribuzione := '   ';
  xInquadramento := '   ';
  xBenefits := '   ';
  Data.QAnagNote.Open;
  xRetribuzione := Data.QAnagNoteRetribuzione.Value + '          ';
  if Data.QAnagNoteBenefits.Value <> '' then
    xRetribuzione := xRetribuzione + ' + ' + Data.QAnagNoteBenefits.Value;
  xInquadramento := Data.QAnagNoteInquadramento.Value + '          ';
  xBenefits := Data.QAnagNoteBenefits.Value + '          '; ;

  xNaz := Data.QAnagNoteNazionalita.Value + '    ';
  xTipoRetrib := Data.QAnagNoteTipoRetrib.Value;
  if xTipoRetrib <> '' then xTipoRetrib := '(' + xTipoRetrib + ')'
  else xTipoRetrib := '   ';
  xTipoRetribEng := Data.QAnagNoteTipoRetribEng.Value;
  if xTipoRetribEng <> '' then xTipoRetribEng := '(' + xTipoRetribEng + ')'
  else xTipoRetribEng := '   ';
  Data.QAnagNote.Close;

  xcliente := '       ';
  xRifInterno := '       ';
  xIndirizzoCliente := '       ';
  xCAPComuneProv := '       ';
  Data.Q1.Close;
  Data.Q1.SQL.text := 'select EBC_Clienti.Descrizione Cliente from EBC_Clienti ' +
    'where ID=' + IntToStr(xIDCliente);
  Data.Q1.Open;
  xcliente := Data.Q1.FieldByName('Cliente').asString + '    ';
  Data.Q1.Close;
  Data.Q1.SQL.text := 'select Aree.Descrizione area from Aree,Mansioni ' +
    'where Aree.ID=Mansioni.IDArea and Mansioni.ID=' + IntToStr(xIDMansione);
  Data.Q1.Open;
  xAreaRuolo := Data.Q1.FieldByName('Area').asString + ' - ' + xPosizione;
  Data.Q1.Close;

  xRiempi := True;
  if FileExists(xFileWord) then begin
    if MessageDlg('Il file esiste gi� - Aprirlo (Yes) o crearne uno nuovo, riscrivendo il vecchio (No) ?', mtWarning, [mbYes, mbNo], 0) = mrYes then begin
      MsWord.AppShow;
      MsWord.FileOpen(xFileWord); // file da aprire
      xRiempi := False; // non riempire i campi
    end else begin
      MsWord.AppShow;
      MsWord.FileOpen(xFileBase);
    end;
  end else begin
    MsWord.AppShow;
    MsWord.FileOpen(xFileBase);
  end;

  // riempimento campi
  if xRiempi then begin
    // pagina copertina
    MsWord.EditFind('#Data');
    MsWord.Insert(DateToStr(Date));
    MsWord.EditFind('#Cliente');
    if xCliente <> '' then
      MsWord.Insert(xCliente)
    else MsWord.Insert('---');
    MsWord.EditFind('#posizione');
    if xPosizione <> '' then
      MsWord.Insert(xPosizione)
    else MsWord.Insert('---');
    MsWord.EditFind('#NomeCognome');
    MsWord.Insert(Data.TAnagraficaNome.Value + ' ' + Data.TAnagraficaCognome.Value + '  ');
    // seconda pagina
    MsWord.EditFind('#NomeCognome');
    MsWord.Insert(Data.TAnagraficaNome.Value + ' ' + Data.TAnagraficaCognome.Value + '  ');
    MsWord.EditFind('#DomicilioIndirizzo');
    MsWord.Insert(Data.TAnagraficaDomicilioTipoStrada.Value + ' ' + Data.TAnagraficaDomicilioIndirizzo.Value + ' ' + Data.TAnagraficaDomicilioNumCivico.Value + '  ');
    MsWord.EditFind('#CapComune');
    MsWord.Insert(Data.TAnagraficaDomicilioCap.Value + ' ' + Data.TAnagraficaDomicilioComune.Value + '  ');
    MsWord.EditFind('#Telefono');
    MsWord.Insert(Data.TAnagraficaRecapitiTelefonici.Value + '  ');
    MsWord.EditFind('#Nazionalita');
    if xNaz <> '' then
      MsWord.Insert(xNaz)
    else MsWord.Insert('---');
    MsWord.EditFind('#Anni');
    MsWord.Insert(Data.TAnagraficaEta.Value + '   ');
    MsWord.EditFind('#DataNascita');
    MsWord.Insert(Data.TAnagraficaDataNascita.asString + '   ');
    MsWord.EditFind('#StatoCivile');
    MsWord.Insert(Data.TAnagraficaStatoCivile.Value + '  ');

    // FORMAZIONE
    Data.Q1.close;
    Data.Q1.SQL.text := 'select Tipo,Descrizione Titolo,LuogoConseguimento,DataConseguimento,Votazione ' +
      'from TitoliStudio,Diplomi where TitoliStudio.IDDiploma=Diplomi.ID ' +
      'and IDAnagrafica=' + Data.TAnagraficaID.asString;
    Data.Q1.Open;
    if Data.Q1.RecordCount > 0 then begin
      MsWord.EditFind('#TitoliStudio');
      while not Data.Q1.EOF do begin
        MsWord.Insert(Data.Q1.FieldByName('DataConseguimento').asString + '   ');
        MsWord.NextCell;
        MsWord.Insert(Data.Q1.FieldByName('Titolo').asString + '   ');
        Data.Q1.Next;
        if not Data.Q1.EOF then begin
          MsWord.NextCell;
        end;
      end;
    end;

    // Corsi Extra
    Data.Q1.close;
    Data.Q1.SQL.text := 'select Tipologia ' +
      'from CorsiExtra ' +
      'where IDAnagrafica=' + Data.TAnagraficaID.asString;
    Data.Q1.Open;
    if Data.Q1.RecordCount > 0 then begin
      MsWord.EditFind('#CorsiExtra');
      while not Data.Q1.EOF do begin
        MsWord.Insert(Data.Q1.FieldByName('Tipologia').asString + '   ');
        Data.Q1.Next;
        if not Data.Q1.EOF then begin
          MsWord.Insert(chr(13));
        end;
      end;
    end;

    // LINGUE
    Data.Q1.close;
    Data.Q1.SQL.text := 'select Lingua, livelloConoscenza ' +
      'from LingueConosciute,LivelloLingue ' +
      'where LingueConosciute.Livello *= LivelloLingue.ID ' +
      'and IDAnagrafica=' + Data.TAnagraficaID.asString;
    Data.Q1.Open;
    if Data.Q1.RecordCount > 0 then begin
      MsWord.EditFind('#lingua');
      while not Data.Q1.EOF do begin
        MsWord.Insert(Data.Q1.FieldByName('Lingua').asString + '   ');
        MsWord.NextCell;
        MsWord.Insert(Data.Q1.FieldByName('LivelloConoscenza').asString + '   ');
        Data.Q1.Next;
        if not Data.Q1.EOF then begin
          MsWord.NextCell;
        end;
      end;
    end;

    // pagina 3
    MsWord.EditFind('#NomeCognome');
    MsWord.Insert(Data.TAnagraficaNome.Value + ' ' + Data.TAnagraficaCognome.Value);
    // DATI PROFESSIONALI
    Data.Q1.close;
    Data.Q1.SQL.text := 'select EBC_Clienti.Descrizione Azienda, EBC_Clienti.Comune, EBC_Clienti.Provincia, ' +
      'EBC_Clienti.NumDipendenti, Attivita, Mansioni.Descrizione Posizione, ' +
      'AnnoDal,AnnoAl,DescrizioneMansione,AziendaFatturato,AziendaNumDipendenti,Attuale ' +
      'from EsperienzeLavorative,EBC_Clienti,Mansioni,EBC_Attivita ' +
      'where EsperienzeLavorative.IDAzienda=EBC_Clienti.ID ' +
      'and EsperienzeLavorative.IDMansione=Mansioni.ID ' +
      'and EsperienzeLavorative.IDSettore=EBC_Attivita.ID and IDAnagrafica=' + Data.TAnagraficaID.asString +
      'order by AnnoAl desc';
    Data.Q1.Open;
    MsWord.EditFind('#EspLav');
    while not Data.Q1.EOF do begin
      // prima colonna
      MsWord.Insert(Data.Q1.FieldByName('AnnoDal').asString + ' - ' + Data.Q1.FieldByName('AnnoAl').asString);
      // seconda colonna
      MsWord.NextCell;
      if Data.Q1.FieldByName('Attuale').asBoolean then begin
        MsWord.Insert(Data.Q1.FieldByName('Azienda').asString + ' (Attuale) ' + chr(13));
        MsWord.Insert(Data.Q1.FieldByName('Posizione').asString + chr(13));
        xS := Data.Q1.FieldByName('DescrizioneMansione').asString;
        if xs <> '' then begin
          while pos(chr(13), xS) > 0 do begin
            xCRpos := pos(chr(13), xS);
            MsWord.Insert(copy(xS, 1, xCRpos - 1));
            xS := copy(xS, xCRpos + 1, length(xS));
          end;
          MsWord.Insert(xS);
        end;
      end else begin
        MsWord.Insert(Data.Q1.FieldByName('Azienda').asString + chr(13));
        MsWord.Insert(Data.Q1.FieldByName('Posizione').asString + chr(13));
      end;
      Data.Q1.Next;
      if not Data.Q1.EOF then MsWord.NextCell;
    end;
    Data.Q1.close;

    MsWord.EditFind('#inquadramento');
    MsWord.Insert(xInquadramento + '   ');
    MsWord.EditFind('#retribuzione');
    MsWord.Insert(xRetribuzione + '   ');

    // pagina 4
    MsWord.EditFind('#NomeCognome');
    MsWord.Insert(Data.TAnagraficaNome.Value + ' ' + Data.TAnagraficaCognome.Value + '   ');
    // DATI PROFESSIONALI
    Data.Q1.close;
    Data.Q1.SQL.text := 'select EBC_Clienti.Descrizione Azienda, EBC_Clienti.Comune, EBC_Clienti.Provincia, ' +
      'EBC_Clienti.NumDipendenti, Attivita, Mansioni.Descrizione Posizione, ' +
      'AnnoDal,AnnoAl,DescrizioneMansione,AziendaFatturato,AziendaNumDipendenti,Attuale ' +
      'from EsperienzeLavorative,EBC_Clienti,Mansioni,EBC_Attivita ' +
      'where EsperienzeLavorative.IDAzienda=EBC_Clienti.ID ' +
      'and EsperienzeLavorative.IDMansione=Mansioni.ID ' +
      'and EsperienzeLavorative.IDSettore=EBC_Attivita.ID and IDAnagrafica=' + Data.TAnagraficaID.asString +
      'order by AnnoAl desc';
    Data.Q1.Open;
    MsWord.EditFind('#EspLav');
    while not Data.Q1.EOF do begin
      // prima colonna
      MsWord.Insert(Data.Q1.FieldByName('AnnoDal').asString + ' - ' + Data.Q1.FieldByName('AnnoAl').asString);
      // seconda colonna
      MsWord.NextCell;
      if Data.Q1.FieldByName('Attuale').asBoolean then begin
        MsWord.Insert(Data.Q1.FieldByName('Azienda').asString + ' (Attuale) ' + chr(13));
        MsWord.Insert(Data.Q1.FieldByName('Posizione').asString + chr(13));
        xS := Data.Q1.FieldByName('DescrizioneMansione').asString;
        if xs <> '' then begin
          while pos(chr(13), xS) > 0 do begin
            xCRpos := pos(chr(13), xS);
            MsWord.Insert(copy(xS, 1, xCRpos - 1));
            xS := copy(xS, xCRpos + 1, length(xS));
          end;
          MsWord.Insert(xS);
        end;
      end else begin
        MsWord.Insert(Data.Q1.FieldByName('Azienda').asString + chr(13));
        MsWord.Insert(Data.Q1.FieldByName('Posizione').asString + chr(13));
      end;
      Data.Q1.Next;
      if not Data.Q1.EOF then MsWord.NextCell;
    end;
    Data.Q1.close;

    // pagina 5
    MsWord.EditFind('#NomeCognome');
    MsWord.Insert(Data.TAnagraficaNome.Value + ' ' + Data.TAnagraficaCognome.Value);

    // FINE --> SALVATAGGIO
    MsWord.FileSaveAs(xFileWord);

    // aggiornamento AnagFile
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'insert into AnagFile (IDAnagrafica,IDTipo,Descrizione,NomeFile,DataCreazione) ' +
          'values (:xIDAnagrafica,:xIDTipo,:xDescrizione,:xNomeFile,:xDataCreazione)';
        Q1.ParamByName('xIDAnagrafica').asInteger := Data.TAnagraficaID.Value;
        Q1.ParamByName('xIDTipo').asInteger := 1;
        Q1.ParamByName('xDescrizione').asString := '';
        Q1.ParamByName('xNomeFile').asString := xFileWord;
        Q1.ParamByName('xDataCreazione').asDateTime := Date;
        Q1.ExecSQL;
        DB.CommitTrans;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
    if Data.QAnagFile.Active then begin
      Data.QAnagFile.Close;
      Data.QAnagFile.Open;
    end;
  end;
end;

procedure TMainForm.ToolbarButton9715Click(Sender: TObject);
begin
  dxPrinter1.Preview(True, dxPrinter1Link3);
end;

procedure TMainForm.ToolbarButton9727Click(Sender: TObject);
begin
  dxPrinter1.Preview(True, dxPrinter1Link4);
end;

procedure TMainForm.BFornCCNewClick(Sender: TObject);
var xID: string;
begin
  xID := OpenSelFromTab('ContiCosto', 'ID', 'Conto', 'Conto di costo', '');
  if xID = '' then exit;
  with Data do begin
    DB.BeginTrans;
    try
      Q1.Close;
      Q1.SQL.text := 'insert into FornContiCosto (IDCliente,IDContoCosto) ' +
        'values (:xIDCliente,:xIDContoCosto)';
      Q1.ParamByName('xIDCliente').asInteger := Data2.TEBCClientiID.Value;
      Q1.ParamByName('xIDContoCosto').asInteger := StrToInt(xID);
      Q1.ExecSQL;
      DB.CommitTrans;
    except
      DB.RollbackTrans;
      MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
    end;
  end;
  Data2.QFornContiCosto.Close;
  Data2.QFornContiCosto.Open;
end;

procedure TMainForm.BFornCCDelClick(Sender: TObject);
begin
  if Data2.QFornContiCosto.IsEmpty then exit;
  if MessageDlg('Sei sicuro di voler eliminare l''associazione tra fornitore e conto di costo ?', mtWarning,
    [mbNo, mbYes], 0) = mrYes then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'delete from FornContiCosto where ID=' + Data2.QFornContiCostoID.AsString;
        Q1.ExecSQL;
        Data2.QFornContiCosto.Close;
        Data2.QFornContiCosto.Open;
        DB.CommitTrans;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
  end;
end;

procedure TMainForm.BFornCCMod1Click(Sender: TObject);
var xID: string;
begin
  if Data2.QFornContiCosto.IsEmpty then exit;
  xID := OpenSelFromTab('ContiCosto', 'ID', 'Conto', 'Conto di costo', Data2.QFornContiCostoIDContoCosto.asString);
  if xID = '' then exit;
  with Data do begin
    DB.BeginTrans;
    try
      Q1.Close;
      Q1.SQL.text := 'update FornContiCosto set IDContoCosto=:xIDContoCosto ' +
        'where ID=' + Data2.QFornContiCostoID.AsString;
      Q1.ParamByName('xIDContoCosto').asInteger := StrToInt(xID);
      Q1.ExecSQL;
      DB.CommitTrans;
    except
      DB.RollbackTrans;
      MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
    end;
  end;
  Data2.QFornContiCosto.Close;
  Data2.QFornContiCosto.Open;
end;

procedure TMainForm.PCAziendaOrgChange(Sender: TObject);
begin
  if PCAziendaOrg.activepage = TSAziendaOrgRuolo then begin
    DataRicerche.QOrgNodoRuolo.Open;
  end else DataRicerche.QOrgNodoRuolo.Close;
end;

procedure TMainForm.infocandidato2Click(Sender: TObject);
begin
  if (DataRicerche.QOrgNodoRuolo.IsEmpty) or (not DataRicerche.QOrgNodoRuolo.active) then exit;
  SchedaCandForm := TSchedaCandForm.create(self);
  if not PosizionaAnag(DataRicerche.QOrgNodoRuoloID.Value) then exit;
  SchedaCandForm.ShowModal;
  SchedaCandForm.Free;
  DataRicerche.QOrgNodoRuolo.Close;
  DataRicerche.QOrgNodoRuolo.Open;
end;

procedure TMainForm.curriculum2Click(Sender: TObject);
var x: string;
  i: integer;
begin
  if (DataRicerche.QOrgNodoRuolo.IsEmpty) or (not DataRicerche.QOrgNodoRuolo.active) then exit;
  if not PosizionaAnag(DataRicerche.QOrgNodoRuoloID.Value) then exit;
  Data.TTitoliStudio.Open;
  Data.TCorsiExtra.Open;
  Data.TLingueConosc.Open;
  Data.TEspLav.Open;
  CurriculumForm.ShowModal;
  Data.TTitoliStudio.Close;
  Data.TCorsiExtra.Close;
  Data.TLingueConosc.Close;
  Data.TEspLav.Close;
  MainForm.Pagecontrol5.ActivePage := MainForm.TSStatoTutti;
end;

procedure TMainForm.documenti2Click(Sender: TObject);
begin
  if (DataRicerche.QOrgNodoRuolo.IsEmpty) or (not DataRicerche.QOrgNodoRuolo.active) then exit;
  ApriCV(DataRicerche.QOrgNodoRuoloID.Value);
end;

procedure TMainForm.fileWord2Click(Sender: TObject);
begin
  ApriFileWord(IntToStr(DammiCVNumero(DataRicerche.QOrgNodoRuoloID.Value)));
end;

procedure TMainForm.BTargetListClick(Sender: TObject);
begin
  BTargetList.visible := False;
  CaricaApriRicPend(xIDRicerca);
  SelPersForm := TSelPersForm.create(self);
  SelPersForm.CBOpzioneCand.Checked := True;
  SelPersForm.EseguiQueryCandidati;
  SelPersForm.PCSelPers.ActivePage := SelPersForm.TSTargetList;
  DataRicerche.QRicTargetList.Open;
  DataRicerche.QRicTargetList.Locate('IDCliente', DataRicerche.QAziendeID.Value, []);
  SelPersForm.xFromOrgTL := True;
  SelPersForm.ShowModal;
  try SelPersForm.Free
  except
  end;
end;

procedure TMainForm.PopupMenu4Popup(Sender: TObject);
begin
  if (xIDRicerca > 0) and (BTargetList.visible = true) then
    Aggiungiacommessa1.Enabled := True
  else Aggiungiacommessa1.Enabled := False;
end;

procedure TMainForm.Aggiungiacommessa1Click(Sender: TObject);
var xVai: boolean;
  xRic, xLivProtez, xDicMess, xPassword, xUtenteResp, xClienteBlocco: string;
  xIDEvento, xIDClienteBlocco: integer;
  QLocal: TQuery;
begin
  // aggiunta soggetto alla commessa
  // controllo propriet� CV
  if (not ((DataRicerche.QOrgNodoRuoloIDProprietaCV.asString = '') or (DataRicerche.QOrgNodoRuoloIDProprietaCV.Value = 0))) and
    (DataRicerche.QOrgNodoRuoloIDProprietaCV.value <> DataRicerche.QAziendeID.Value) then
    if MessageDlg('ATTENZIONE: il CV risulta di propriet� di un altro cliente .' + chr(13) +
      'Vuoi proseguire lo stesso ?', mtWarning, [mbYes, mbNo], 0) = mrNo then begin
      exit;
    end;

  DataRicerche.TRicAnagIDX.Open;
  if not DataRicerche.TRicAnagIDX.FindKey([xIDRicerca, DataRicerche.QOrgNodoRuoloID.Value]) then begin
    xVai := True;
    // controllo se � gi� associato ad un'altra ricerca per la STESSA azienda
    Data.Q1.Close;
    Data.Q1.SQL.clear;
    Data.Q1.SQl.Add('select EBC_Ricerche.Progressivo,EBC_StatiRic.StatoRic from EBC_CandidatiRicerche,EBC_Ricerche,EBC_StatiRic ');
    Data.Q1.SQl.Add('where EBC_CandidatiRicerche.IDRicerca=EBC_Ricerche.ID and EBC_Ricerche.IDStatoRic=EBC_StatiRic.ID');
    Data.Q1.SQl.Add('and EBC_CandidatiRicerche.IDAnagrafica=:xIDAnag and EBC_Ricerche.IDCliente=:xIDCliente');
    Data.Q1.Prepare;
    Data.Q1.ParamByName('xIDAnag').asInteger := DataRicerche.QOrgNodoRuoloID.Value;
    Data.Q1.ParamByName('xIDCliente').asInteger := DataRicerche.QAziendeID.Value;
    Data.Q1.Open;
    if not Data.Q1.IsEmpty then begin
      xRic := '';
      while not Data.Q1.EOF do begin xRic := xRic + 'N�' + Data.Q1.FieldByName('Progressivo').asString + ' (' + Data.Q1.FieldByName('StatoRic').asString + ') '; Data.Q1.Next; end;
      if MessageDlg('Il soggetto � associato gi� alle seguenti ricerche per lo stesso cliente: ' + chr(13) + xRic + chr(13) + 'PROCEDERE COMUNQUE ?', mtWarning, [mbYes, mbNo], 0) = mrNo then xVai := False;
    end;
    // controllo incompatibilit�
    if IncompAnagCli(DataRicerche.QOrgNodoRuoloID.Value, DataRicerche.QAziendeID.Value) then
      if MessageDlg('Il soggetto risulta INCOMPATIBILE con questo cliente o con un cliente associato' + chr(13) +
        'PROCEDERE COMUNQUE ?', mtWarning, [mbYes, mbNo], 0) = mrNo then xVai := False;

    // controllo blocco livello 1 o 2
    xIDClienteBlocco := CheckAnagInseritoBlocco1(DataRicerche.QOrgNodoRuoloID.Value);
    if xIDClienteBlocco > 0 then begin
      xLivProtez := copy(IntToStr(xIDClienteBlocco), 1, 1);
      if xLivProtez = '1' then begin
        xIDClienteBlocco := xIDClienteBlocco - 10000;
        xClienteBlocco := GetDescCliente(xIDClienteBlocco);
        xDicMess := 'Stai inserendo in commessa un candidato appartenente ad una azienda (' + xClienteBlocco + ') con blocco di livello 1, ';
      end else begin
        xIDClienteBlocco := xIDClienteBlocco - 20000;
        xClienteBlocco := GetDescCliente(xIDClienteBlocco);
        xDicMess := 'Stai inserendo in commessa un candidato appartenente ad una azienda ATTIVA (' + xClienteBlocco + '), ';
      end;
      if MessageDlg('ATTENZIONE!! PROTEZIONE CLIENTI LIVELLO ' + xLivProtez + chr(13) + xDicMess +
        'protetta dalle normative interne, se sei autorizzato premi SI, altrimenti premi NO. ' +
        'Se si prosegue, l''operazione verr� registrata nello storico del soggetto come forzatura. ' +
        'Verr� inviato un messaggio di promemoria al responsabile diretto', mtWarning, [mbYes, mbNo], 0) = mrNo then xVai := False
      else begin
        // pwd
        xPassword := '';
        xUtenteResp := GetDescUtenteResp(MainForm.xIDUtenteAttuale);
        if not InputQuery('Forzatura blocco livello ' + xLivProtez, 'Password di ' + xUtenteResp, xPassword) then exit;
        if not CheckPassword(xPassword, GetPwdUtenteResp(MainForm.xIDUtenteAttuale)) then begin
          MessageDlg('Password errata', mtError, [mbOK], 0);
          xVai := False;
        end;
        if xVai then begin
          // promemoria al resp.
          QLocal := CreateQueryFromString('insert into Promemoria (IDUtente,IDUtenteDa,DataIns,Testo,Evaso,DataDaLeggere) ' +
            'values (:xIDUtente,:xIDUtenteDa,:xDataIns,:xTesto,:xEvaso,:xDataDaLeggere)');
          QLocal.ParamByName('xIDUtente').asInteger := GetIDUtenteResp(MainForm.xIDUtenteAttuale);
          QLocal.ParamByName('xIDUtenteDa').asInteger := MainForm.xIDUtenteAttuale;
          QLocal.ParamByName('xDataIns').asDateTime := Date;
          QLocal.ParamByName('xTesto').asString := 'forzatura livello ' + xLivProtez + ' per CV n� ' + ElencoDipForm.TAnagDipCVNumero.AsString;
          QLocal.ParamByName('xEvaso').asBoolean := False;
          QLocal.ParamByName('xDataDaLeggere').asDateTime := Date;
          QLocal.ExecSQL;
          // registra nello storico soggetto
          xIDEvento := GetIDEvento('forzatura Blocco livello ' + xLivProtez);
          if xIDEvento > 0 then begin
            with Data.Q1 do begin
              close;
              SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
                'values (:xIDAnagrafica,:xIDEvento,:xDataEvento,:xAnnotazioni,:xIDRicerca,:xIDUtente,:xIDCliente)';
              ParamByName('xIDAnagrafica').asInteger := DataRicerche.QOrgNodoRuoloID.Value;
              ParamByName('xIDEvento').asInteger := xIDevento;
              ParamByName('xDataEvento').asDateTime := Date;
              ParamByName('xAnnotazioni').asString := 'cliente: ' + xClienteBlocco;
              ParamByName('xIDRicerca').asInteger := xIDRicerca;
              ParamByName('xIDUtente').asInteger := MainForm.xIDUtenteAttuale;
              ParamByName('xIDCliente').asInteger := xIDClienteBlocco;
              ExecSQL;
            end;
          end;
        end;
      end;
    end;

    if xVai then begin
      Data.DB.BeginTrans;
      try
        if Data.Q1.Active then Data.Q1.Close;
        Data.Q1.SQL.text := 'insert into EBC_CandidatiRicerche (IDRicerca,IDAnagrafica,Escluso,Stato,DataIns,IDTargetList) ' +
          'values (:xIDRicerca,:xIDAnagrafica,:xEscluso,:xStato,:xDataIns,:xIDTargetList)';
        Data.Q1.ParamByName('xIDRicerca').asInteger := xIDRicerca;
        Data.Q1.ParamByName('xIDAnagrafica').asInteger := DataRicerche.QOrgNodoRuoloID.Value;
        Data.Q1.ParamByName('xEscluso').asBoolean := False;
        Data.Q1.ParamByName('xStato').asString := 'inserito da Target List';
        Data.Q1.ParamByName('xDataIns').asDateTime := Date;
        Data.Q1.ParamByName('xIDTargetList').asInteger := xIDTargetList;
        Data.Q1.ExecSQL;
        // aggiornamento stato anagrafica
        if Data.Q1.Active then Data.Q1.Close;
        Data.Q1.SQL.text := 'update Anagrafica set IDStato=27,IDTipoStato=1 where ID=' + DataRicerche.QOrgNodoRuoloID.asString;
        Data.Q1.ExecSQL;
        // aggiornamento storico
        if Data.Q1.Active then Data.Q1.Close;
        Data.Q1.SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
          'values (:xIDAnagrafica,:xIDEvento,:xDataEvento,:xAnnotazioni,:xIDRicerca,:xIDUtente,:xIDCliente)';
        Data.Q1.ParamByName('xIDAnagrafica').asInteger := DataRicerche.QOrgNodoRuoloID.Value;
        Data.Q1.ParamByName('xIDEvento').asInteger := 19;
        Data.Q1.ParamByName('xDataEvento').asDateTime := date;
        Data.Q1.ParamByName('xAnnotazioni').asString := 'inserito da target list';
        Data.Q1.ParamByName('xIDRicerca').asInteger := xIDRicerca;
        Data.Qtemp.Close;
        Data.Qtemp.SQL.text := 'select IDUtente,IDCliente from EBC_Ricerche where ID=' + IntToStr(xIDRicerca);
        Data.Qtemp.Open;
        Data.Q1.ParamByName('xIDUtente').asInteger := Data.Qtemp.FieldByName('IDUtente').asInteger;
        Data.Q1.ParamByName('xIDCliente').asInteger := Data.Qtemp.FieldByName('IDCliente').asInteger;
        Data.Qtemp.Close;
        Data.Q1.ExecSQL;
        Data.DB.CommitTrans;
        ShowMessage('Inserimento in commessa avvenuto');
      except
        Data.DB.RollbackTrans;
        MessageDlg('Errore sul database:  inserimento non avvenuto', mtError, [mbOK], 0);
      end;
    end;
  end;
  DataRicerche.TRicAnagIDX.Close;
end;

procedure TMainForm.PopupMenu3Popup(Sender: TObject);
begin
  if (xIDRicerca > 0) and (BTargetList.visible = true) then
    Aggiungiacommessa2.Enabled := True
  else Aggiungiacommessa2.Enabled := False;
end;

procedure TMainForm.Aggiungiacommessa2Click(Sender: TObject);
var xVai: boolean;
  xRic, xLivProtez, xDicMess, xPassword, xUtenteResp, xClienteBlocco: string;
  xIDEvento, xIDClienteBlocco, xIDAnag: integer;
  QLocal: TQuery;
begin
  // aggiunta soggetto alla commessa
  xIDAnag := longint(TVAziendaCand.Selected.Data);
  Data.QTemp.Close;
  Data.QTemp.SQL.text := 'select IDProprietaCV from Anagrafica where ID=' + IntToStr(xIDAnag);
  Data.QTemp.Open;
  // controllo propriet� CV
  if (not ((Data.QTemp.FieldByName('IDProprietaCV').asString = '') or (Data.QTemp.FieldByName('IDProprietaCV').asInteger = 0))) and
    (Data.QTemp.FieldByName('IDProprietaCV').asInteger <> DataRicerche.QAziendeID.Value) then
    if MessageDlg('ATTENZIONE: il CV risulta di propriet� di un altro cliente .' + chr(13) +
      'Vuoi proseguire lo stesso ?', mtWarning, [mbYes, mbNo], 0) = mrNo then begin
      Data.QTemp.Close;
      exit;
    end;
  Data.QTemp.Close;

  DataRicerche.TRicAnagIDX.Open;
  if not DataRicerche.TRicAnagIDX.FindKey([xIDRicerca, xIDAnag]) then begin
    xVai := True;
    // controllo se � gi� associato ad un'altra ricerca per la STESSA azienda
    Data.Q1.Close;
    Data.Q1.SQL.clear;
    Data.Q1.SQl.Add('select EBC_Ricerche.Progressivo,EBC_StatiRic.StatoRic from EBC_CandidatiRicerche,EBC_Ricerche,EBC_StatiRic ');
    Data.Q1.SQl.Add('where EBC_CandidatiRicerche.IDRicerca=EBC_Ricerche.ID and EBC_Ricerche.IDStatoRic=EBC_StatiRic.ID');
    Data.Q1.SQl.Add('and EBC_CandidatiRicerche.IDAnagrafica=:xIDAnag and EBC_Ricerche.IDCliente=:xIDCliente');
    Data.Q1.Prepare;
    Data.Q1.ParamByName('xIDAnag').asInteger := xIDAnag;
    Data.Q1.ParamByName('xIDCliente').asInteger := DataRicerche.QAziendeID.Value;
    Data.Q1.Open;
    if not Data.Q1.IsEmpty then begin
      xRic := '';
      while not Data.Q1.EOF do begin xRic := xRic + 'N�' + Data.Q1.FieldByName('Progressivo').asString + ' (' + Data.Q1.FieldByName('StatoRic').asString + ') '; Data.Q1.Next; end;
      if MessageDlg('Il soggetto � associato gi� alle seguenti ricerche per lo stesso cliente: ' + chr(13) + xRic + chr(13) + 'PROCEDERE COMUNQUE ?', mtWarning, [mbYes, mbNo], 0) = mrNo then xVai := False;
    end;
    // controllo incompatibilit�
    if IncompAnagCli(xIDAnag, DataRicerche.QAziendeID.Value) then
      if MessageDlg('Il soggetto risulta INCOMPATIBILE con questo cliente o con un cliente associato' + chr(13) +
        'PROCEDERE COMUNQUE ?', mtWarning, [mbYes, mbNo], 0) = mrNo then xVai := False;

    // controllo blocco livello 1 o 2
    xIDClienteBlocco := CheckAnagInseritoBlocco1(xIDAnag);
    xClienteBlocco := GetDescCliente(xIDClienteBlocco);
    if xIDClienteBlocco > 0 then begin
      xLivProtez := copy(IntToStr(xIDClienteBlocco), 1, 1);
      if xLivProtez = '1' then begin
        xIDClienteBlocco := xIDClienteBlocco - 10000;
        xDicMess := 'Stai inserendo in commessa un candidato appartenente ad una azienda (' + xClienteBlocco + ') con blocco di livello 1, ';
      end else begin
        xIDClienteBlocco := xIDClienteBlocco - 20000;
        xDicMess := 'Stai inserendo in commessa un candidato appartenente ad una azienda ATTIVA (' + xClienteBlocco + '), ';
      end;
      if MessageDlg('ATTENZIONE!! PROTEZIONE CLIENTI LIVELLO ' + xLivProtez + chr(13) + xDicMess +
        'protetta dalle normative interne, se sei autorizzato premi SI, altrimenti premi NO. ' +
        'Se si prosegue, l''operazione verr� registrata nello storico del soggetto come forzatura. ' +
        'Verr� inviato un messaggio di promemoria al responsabile diretto', mtWarning, [mbYes, mbNo], 0) = mrNo then xVai := False
      else begin
        // pwd
        xPassword := '';
        xUtenteResp := GetDescUtenteResp(MainForm.xIDUtenteAttuale);
        if not InputQuery('Forzatura blocco livello ' + xLivProtez, 'Password di ' + xUtenteResp, xPassword) then exit;
        if not CheckPassword(xPassword, GetPwdUtenteResp(MainForm.xIDUtenteAttuale)) then begin
          MessageDlg('Password errata', mtError, [mbOK], 0);
          xVai := False;
        end;
        if xVai then begin
          // promemoria al resp.
          QLocal := CreateQueryFromString('insert into Promemoria (IDUtente,IDUtenteDa,DataIns,Testo,Evaso,DataDaLeggere) ' +
            'values (:xIDUtente,:xIDUtenteDa,:xDataIns,:xTesto,:xEvaso,:xDataDaLeggere)');
          QLocal.ParamByName('xIDUtente').asInteger := GetIDUtenteResp(MainForm.xIDUtenteAttuale);
          QLocal.ParamByName('xIDUtenteDa').asInteger := MainForm.xIDUtenteAttuale;
          QLocal.ParamByName('xDataIns').asDateTime := Date;
          QLocal.ParamByName('xTesto').asString := 'forzatura livello ' + xLivProtez + ' per CV n� ' + ElencoDipForm.TAnagDipCVNumero.AsString;
          QLocal.ParamByName('xEvaso').asBoolean := False;
          QLocal.ParamByName('xDataDaLeggere').asDateTime := Date;
          QLocal.ExecSQL;
          // registra nello storico soggetto
          xIDEvento := GetIDEvento('forzatura Blocco livello ' + xLivProtez);
          if xIDEvento > 0 then begin
            with Data.Q1 do begin
              close;
              SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
                'values (:xIDAnagrafica,:xIDEvento,:xDataEvento,:xAnnotazioni,:xIDRicerca,:xIDUtente,:xIDCliente)';
              ParamByName('xIDAnagrafica').asInteger := xIDAnag;
              ParamByName('xIDEvento').asInteger := xIDevento;
              ParamByName('xDataEvento').asDateTime := Date;
              ParamByName('xAnnotazioni').asString := 'cliente: ' + xClienteBlocco;
              ParamByName('xIDRicerca').asInteger := xIDRicerca;
              ParamByName('xIDUtente').asInteger := MainForm.xIDUtenteAttuale;
              ParamByName('xIDCliente').asInteger := xIDClienteBlocco;
              ExecSQL;
            end;
          end;
        end;
      end;
    end;

    if xVai then begin
      Data.DB.BeginTrans;
      try
        if Data.Q1.Active then Data.Q1.Close;
        Data.Q1.SQL.text := 'insert into EBC_CandidatiRicerche (IDRicerca,IDAnagrafica,Escluso,Stato,DataIns,IDTargetList) ' +
          'values (:xIDRicerca,:xIDAnagrafica,:xEscluso,:xStato,:xDataIns,:xIDTargetList)';
        Data.Q1.ParamByName('xIDRicerca').asInteger := xIDRicerca;
        Data.Q1.ParamByName('xIDAnagrafica').asInteger := xIDAnag;
        Data.Q1.ParamByName('xEscluso').asBoolean := False;
        Data.Q1.ParamByName('xStato').asString := 'inserito da Target List';
        Data.Q1.ParamByName('xDataIns').asDateTime := Date;
        Data.Q1.ParamByName('xIDTargetList').asInteger := xIDTargetList;
        Data.Q1.ExecSQL;
        // aggiornamento stato anagrafica
        if Data.Q1.Active then Data.Q1.Close;
        Data.Q1.SQL.text := 'update Anagrafica set IDStato=27,IDTipoStato=1 where ID=' + IntToStr(xIDAnag);
        Data.Q1.ExecSQL;
        // aggiornamento storico
        if Data.Q1.Active then Data.Q1.Close;
        Data.Q1.SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
          'values (:xIDAnagrafica,:xIDEvento,:xDataEvento,:xAnnotazioni,:xIDRicerca,:xIDUtente,:xIDCliente)';
        Data.Q1.ParamByName('xIDAnagrafica').asInteger := xIDAnag;
        Data.Q1.ParamByName('xIDEvento').asInteger := 19;
        Data.Q1.ParamByName('xDataEvento').asDateTime := date;
        Data.Q1.ParamByName('xAnnotazioni').asString := 'inserito da target list';
        Data.Q1.ParamByName('xIDRicerca').asInteger := xIDRicerca;
        Data.Qtemp.Close;
        Data.Qtemp.SQL.text := 'select IDUtente,IDCliente from EBC_Ricerche where ID=' + IntToStr(xIDRicerca);
        Data.Qtemp.Open;
        Data.Q1.ParamByName('xIDUtente').asInteger := Data.Qtemp.FieldByName('IDUtente').asInteger;
        Data.Q1.ParamByName('xIDCliente').asInteger := Data.Qtemp.FieldByName('IDCliente').asInteger;
        Data.Qtemp.Close;
        Data.Q1.ExecSQL;
        Data.DB.CommitTrans;
        ShowMessage('Inserimento in commessa avvenuto');
      except
        Data.DB.RollbackTrans;
        MessageDlg('Errore sul database:  inserimento non avvenuto', mtError, [mbOK], 0);
      end;
    end;
  end;
  DataRicerche.TRicAnagIDX.Close;
end;

procedure TMainForm.SpeedButton3Click(Sender: TObject);
var xFiltro: string;
begin
  if Length(EClienti.text) < 2 then begin
    ShowMessage('Inserire almeno due iniziali');
    exit;
  end;
  xFiltro := '';
  TSCliForn.TabVisible := False;
  if Pagecontrol4.ActivePage = TSCliAttivi then xFiltro := 'attivo';
  if Pagecontrol4.ActivePage = TSCliNonAttivi then xFiltro := 'non attivo';
  if Pagecontrol4.ActivePage = TSCliEliminati then xFiltro := 'Eliminato';
  if Pagecontrol4.ActivePage = TSCliContattati then xFiltro := 'contattato';
  if Pagecontrol4.ActivePage = TSCliAltre then xFiltro := 'azienda esterna';
  if Pagecontrol4.ActivePage = TSCliconcess then xFiltro := 'concessionario';
  if Pagecontrol4.ActivePage = TSCliFornitori then xFiltro := 'fornitore';
  CaricaApriClienti(EClienti.text, xFiltro, True);
  if Data2.TEBCClientiStato.value = 'fornitore' then TSCliForn.TabVisible := True;
  if PCClienti.ActivePage = TSClientiCV then begin
    CliCandidatiFrame1.xIDCliente := Data2.TEBCClientiID.Value;
    CliCandidatiFrame1.xIDRicerca := 0;
    CliCandidatiFrame1.CreaAlbero;
  end;
end;

procedure TMainForm.ToolbarButton972Click(Sender: TObject);
var xDesc: string;
begin
  xDesc := '';
  if not InputQuery('Nuova area', 'nome area:', xDesc) then exit;
  with Data do begin
    DB.BeginTrans;
    try
      Q1.Close;
      Q1.SQL.text := 'insert into AreeSettori (AreaSettore) ' +
        'values (:xAreaSettore)';
      Q1.ParamByName('xAreaSettore').asString := xDesc;
      Q1.ExecSQL;
      DB.CommitTrans;
    except
      DB.RollbackTrans;
      MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
    end;
  end;
  Data2.QAreeAttivita.Close;
  Data2.QAreeAttivita.Open;
end;

procedure TMainForm.ToolbarButton976Click(Sender: TObject);
var xDesc: string;
begin
  xDesc := Data2.QAreeAttivitaAreaSettore.Value;
  if not InputQuery('Modifica area', 'nuovo nome:', xDesc) then exit;
  with Data do begin
    DB.BeginTrans;
    try
      Q1.Close;
      Q1.SQL.text := 'update AreeSettori set AreaSettore=:xAreaSettore ' +
        'where ID=' + Data2.QAreeAttivitaID.AsString;
      Q1.ParamByName('xAreaSettore').asString := xDesc;
      Q1.ExecSQL;
      DB.CommitTrans;
    except
      DB.RollbackTrans;
      MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
    end;
  end;
  Data2.QAreeAttivita.Close;
  Data2.QAreeAttivita.Open;
end;

procedure TMainForm.ToolbarButton974Click(Sender: TObject);
begin
  // verifica integrit� referenziale
  if not VerifIntegrRef('EBC_Attivita', 'IDAreaSettore', Data2.QAreeAttivitaID.AsString) then exit;
  if MessageDlg('Sei sicuro di voler eliminare l''area selezionata ?', mtWarning,
    [mbNo, mbYes], 0) = mrYes then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'delete from AreeSettori ' +
          'where ID=' + Data2.QAreeAttivitaID.AsString;
        Q1.ExecSQL;
        DB.CommitTrans;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
    Data2.QAreeAttivita.Close;
    Data2.QAreeAttivita.Open;
  end;
end;

procedure TMainForm.TbBAttivitaOKClick(Sender: TObject);
begin
  Data2.QAttivita.Post;
end;

procedure TMainForm.TbBAttivitaCanClick(Sender: TObject);
begin
  Data2.QAttivita.Cancel;
end;

procedure TMainForm.ToolbarButton9750Click(Sender: TObject);
var xReg: string;
  xID: integer;
begin
  // modifica comune
  if Data.QComuni.IsEmpty then exit;
  ComuneForm := TComuneForm.create(self);
  ComuneForm.EDesc.Text := Data.QComuniDescrizione.Value;
  ComuneForm.EProv.Text := Data.QComuniProv.Value;
  ComuneForm.ECap.Text := Data.QComuniCap.Value;
  ComuneForm.EPrefisso.Text := Data.QComuniPrefisso.Value;
  ComuneForm.EReg.text := Data.QComuniRegione.Value;
  ComuneForm.EAreaNielsen.text := Data.QComuniAreaNielsen.AsString;
  ComuneForm.ShowModal;
  if ComuneForm.ModalResult = mrOK then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'update TabCom set Descrizione=:xDescrizione,Prov=:xProv,Cap=:xCap,' +
          'Prefisso=:xPrefisso,Regione=:xRegione,AreaNielsen=:xAreaNielsen ' +
          'where ID=' + Data.QComuniID.AsString;
        Q1.ParamByName('xDescrizione').asString := ComuneForm.EDesc.Text;
        Q1.ParamByName('xProv').asString := ComuneForm.EProv.Text;
        Q1.ParamByName('xCap').asString := ComuneForm.ECap.Text;
        Q1.ParamByName('xPrefisso').asString := ComuneForm.EPrefisso.Text;
        Q1.ParamByName('xRegione').asString := ComuneForm.EReg.text;
        Q1.ParamByName('xAreaNielsen').AsInteger := StrToInt(ComuneForm.EAreaNielsen.text);
        Q1.ExecSQL;
        DB.CommitTrans;
        ECercaComChange(self);
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
  end;
  ComuneForm.Free;
end;

procedure TMainForm.ToolbarButton9753Click(Sender: TObject);
begin
  // associa contratto

end;

procedure TMainForm.ToolbarButton9754Click(Sender: TObject);
begin
  // elimina associazione

end;

procedure TMainForm.BTimeSheetNewClick(Sender: TObject);
begin
  DataAnnunci.QAnnTimeReport.Insert;
end;

procedure TMainForm.BTimeSheetDelClick(Sender: TObject);
begin
  if MessageDlg('Sei sicuro di voler eliminare la voce ?', mtWarning, [mbYes, mbNo], 0) = mrYes then
    DataAnnunci.QAnnTimeReport.Delete;
end;

procedure TMainForm.BTimeSheetOKClick(Sender: TObject);
begin
  DataAnnunci.QAnnTimeReport.Post;
end;

procedure TMainForm.BTimeSheetCanClick(Sender: TObject);
begin
  DataAnnunci.QAnnTimeReport.Cancel;
end;

procedure TMainForm.dxDBGrid11DblClick(Sender: TObject);
begin
  ToolbarButton9723Click(self);
end;

procedure TMainForm.ENomeKeyPress(Sender: TObject; var Key: Char);
begin
  if key = chr(13) then BAnagSearchClick(self);
end;

procedure TMainForm.BCliAltriSettoriClick(Sender: TObject);
begin
  // altri settori per l'azienda
  CliAltriSettoriForm := TCliAltriSettoriForm.create(self);
  CliAltriSettoriForm.ShowModal;
  CliAltriSettoriForm.Free;
end;

procedure TMainForm.BAnagTipologiaClick(Sender: TObject);
var xID: string;
begin
  // cambia tipologia utente
  xID := '';
  if Data.TAnagraficaIDTipologia.asString <> '' then xID := Data.TAnagraficaIDTipologia.asString;
  xID := OpenSelFromTab('TipologieSogg', 'ID', 'Tipologia', 'Tipologia', xID);
  if xID <> '' then begin
    if not (Data.DsAnagrafica.State = dsEdit) then Data.TAnagrafica.Edit;
    Data.TAnagraficaIDTipologia.Value := StrToIntDef(xID, 0);
    Data.QTipologieSoggLK.Close;
    Data.QTipologieSoggLK.Open;
    TbBAnagOKClick(self);
  end;
end;

procedure TMainForm.BAnagModificheClick(Sender: TObject);
begin
  // log modifiche
  LogOperazioniSoggForm := TLogOperazioniSoggForm.create(self);
  LogOperazioniSoggForm.QLog.ParamByName('xIDAnag').asInteger := Data.TanagraficaID.Value;
  LogOperazioniSoggForm.QLog.Open;
  LogOperazioniSoggForm.ShowModal;
  LogOperazioniSoggForm.Free;
end;

procedure TMainForm.BCliAssociaFileClick(Sender: TObject);
begin
  RicFileForm := TRicFileForm.create(self);
  RicFileForm.FEFile.InitialDir := GetDocPath;
  RicFileForm.ShowModal;
  if RicFileForm.ModalResult = mrOK then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'insert into EBC_ClientiFile (IDCliente,IDTipo,Descrizione,NomeFile,DataCreazione) ' +
          'values (:xIDCliente,:xIDTipo,:xDescrizione,:xNomeFile,:xDataCreazione)';
        Q1.ParamByName('xIDCliente').asInteger := Data2.TEBCclientiID.Value;
        Q1.ParamByName('xIDTipo').asInteger := 0;
        Q1.ParamByName('xDescrizione').asString := RicFileForm.EDesc.text;
        Q1.ParamByName('xNomeFile').asString := RicFileForm.FEFile.filename;
        Q1.ParamByName('xDataCreazione').asDateTime := FileDateToDateTime(FileAge(RicFileForm.FEFile.filename));
        Q1.ExecSQL;
        DB.CommitTrans;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
    Data2.QClienteFile.Close;
    Data2.QClienteFile.Open;
  end;
  RicFileForm.Free;
end;

procedure TMainForm.BModAssociaFileClick(Sender: TObject);
begin
  RicFileForm := TRicFileForm.create(self);
  RicFileForm.EDesc.text := Data2.QClienteFileDescrizione.Value;
  RicFileForm.FEFile.FileName := Data2.QClienteFileNomeFile.Value;
  RicFileForm.ShowModal;
  if RicFileForm.ModalResult = mrOK then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'update EBC_ClientiFile set IDTipo=:xIDTipo,Descrizione=:xDescrizione,NomeFile=:xNomeFile,DataCreazione=:xDataCreazione ' +
          'where ID=' + Data2.QClienteFileID.AsString;
        Q1.ParamByName('xIDTipo').asInteger := 0;
        Q1.ParamByName('xDescrizione').asString := RicFileForm.EDesc.text;
        Q1.ParamByName('xNomeFile').asString := RicFileForm.FEFile.filename;
        Q1.ParamByName('xDataCreazione').asDateTime := FileDateToDateTime(FileAge(RicFileForm.FEFile.filename));
        Q1.ExecSQL;
        DB.CommitTrans;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
    Data2.QClienteFile.Close;
    Data2.QClienteFile.Open;
  end;
  RicFileForm.Free;
end;

procedure TMainForm.BDelAssociaFileClick(Sender: TObject);
begin
  if MessageDlg('Sei sicuro di voler eliminare l''associazione ?', mtWarning, [mbNo, mbYes], 0) = mrNo then exit;
  if MessageDlg('Vuoi eliminare il file fisico ?', mtWarning, [mbNo, mbYes], 0) = mrYes then begin
    DeleteFile(Data2.QClienteFileNomeFile.Value);
  end;
  with Data do begin
    DB.BeginTrans;
    try
      Q1.Close;
      Q1.SQL.text := 'delete from EBC_ClientiFile where ID=' + Data2.QClienteFileID.AsString;
      Q1.ExecSQL;
      DB.CommitTrans;
    except
      DB.RollbackTrans;
      MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
    end;
  end;
  Data2.QClienteFile.Close;
  Data2.QClienteFile.Open;
end;

procedure TMainForm.BCliApriFileClick(Sender: TObject);
var xFileName: string;
begin
  xFileName := Data2.QClienteFileNomeFile.Value;
  ShellExecute(0, 'Open', pchar(xFileName), '', '', SW_SHOW);
end;

procedure TMainForm.CBOrgZoomClick(Sender: TObject);
begin
  DBTree.Zoom := CBOrgZoom.Checked;
end;

procedure TMainForm.CBOrgRotateClick(Sender: TObject);
begin
  DBTree.Rotated := CBOrgRotate.Checked;
end;

procedure TMainForm.BTabDiplomiITAClick(Sender: TObject);
begin
  DBGrid39.Columns[0].FieldName := 'Tipo';
  DBGrid39.Columns[0].Title.Caption := 'Tipo';
  DBGrid39.Columns[1].FieldName := 'Descrizione';
  DBGrid39.Columns[1].Title.Caption := 'Descrizione';
end;

procedure TMainForm.BTabDiplomiENGClick(Sender: TObject);
begin
  DBGrid39.Columns[0].FieldName := 'Tipo_ENG';
  DBGrid39.Columns[0].Title.Caption := 'Tipo (ENG)';
  DBGrid39.Columns[1].FieldName := 'Descrizione_ENG';
  DBGrid39.Columns[1].Title.Caption := 'Descrizione (ENG)';
end;

procedure TMainForm.BAreeRuoliITAClick(Sender: TObject);
begin
  DBGrid24.columns[0].FieldName := 'Descrizione';
  dxDBGRuoliColumn1.FieldName := 'Descrizione';
end;

procedure TMainForm.BAreeRuoliENGClick(Sender: TObject);
begin
  DBGrid24.columns[0].FieldName := 'Descrizione_ENG';
  dxDBGRuoliColumn1.FieldName := 'Descrizione_ENG';
end;

procedure TMainForm.BAnagRuoliITAClick(Sender: TObject);
begin
  DBGrid41.Columns[0].FieldName := 'Area';
  DBGrid41.Columns[1].FieldName := 'Mansione';
end;

procedure TMainForm.BAnagRuoliENGClick(Sender: TObject);
begin
  DBGrid41.Columns[0].FieldName := 'Area_ENG';
  DBGrid41.Columns[1].FieldName := 'Mansione_ENG';
end;

procedure TMainForm.DBGrid6DblClick(Sender: TObject);
begin
  if (Uppercase(copy(Data.GlobalNomeAzienda.Value, 1, 6)) = 'ADVANT') or
    (Uppercase(copy(Data.GlobalNomeAzienda.Value, 1, 3)) = 'EBC') then begin
    if Data.TAnagrafica.IsEmpty then TbBAnagNewClick(nil);
    ASASchedaCandForm := TASASchedaCandForm.create(self);
    ASASchedaCandForm.ShowModal;
    ASASchedaCandForm.Free;
  end;
end;

procedure TMainForm.DBGClientiDblClick(Sender: TObject);
begin
  if (Uppercase(copy(Data.GlobalNomeAzienda.Value, 1, 6)) = 'ADVANT') or
    (Uppercase(copy(Data.GlobalNomeAzienda.Value, 1, 3)) = 'EBC') then begin
    ASAAziendaForm := TASAAziendaForm.create(self);
    ASAAziendaForm.ShowModal;
    ASAAziendaForm.Free;
  end;
end;

procedure TMainForm.ToolbarButton9728Click(Sender: TObject);
begin
  frReport1.ShowReport;
end;

begin
  {Tell Delphi to hide it's hidden application window for now to avoid}
  {a "flash" on the taskbar if we halt due to another instance}
  ShowWindow(Application.Handle, SW_HIDE);
end.

