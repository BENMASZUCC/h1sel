unit AgendaSett;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     StdCtrls,Buttons,Spin,Grids,AdvGrid,ImgList,ExtCtrls,Db,DBTables,asPrev,
     DBGrids,TB97,Menus,ComCtrls,CandUtente,dxPSCore,dxPSdxTLLnk,
     dxPSdxDBGrLnk,dxDBTLCl,dxGrClms,dxTL,dxDBCtrl,dxDBGrid,dxCntner,
     dxPSdxDBCtrlLnk;

type
     TAgendaSettForm=class(TForm)
          ImageList1: TImageList;
          QAgenda: TQuery;
          QAgendaTipo: TSmallintField;
          QAgendaData: TDateTimeField;
          QAgendaOre: TDateTimeField;
          QAgendaIDCandRic: TIntegerField;
          QAgendaDescrizione: TStringField;
          DsUsers: TDataSource;
          QPromemoria: TQuery;
          QPromemoriaID: TIntegerField;
          QPromemoriaIDUtente: TIntegerField;
          QPromemoriaIDUtenteDa: TIntegerField;
          QPromemoriaDataIns: TDateTimeField;
          QPromemoriaDataDaLeggere: TDateTimeField;
          QPromemoriaTesto: TStringField;
          QPromemoriaDataLettura: TDateTimeField;
          QPromemoriaEvaso: TBooleanField;
          PM1: TPopupMenu;
          modificaimpegno1: TMenuItem;
          nuovoimpegno1: TMenuItem;
          eliminaimpegno1: TMenuItem;
          Panel5: TPanel;
          BEsci: TToolbarButton97;
          PM2: TPopupMenu;
          vedispostamessaggio1: TMenuItem;
          nuovomessaggio1: TMenuItem;
          eliminamessaggio1: TMenuItem;
          TPromABS: TTable;
          TPromABSID: TAutoIncField;
          TPromABSIDUtente: TIntegerField;
          TPromABSIDUtenteDa: TIntegerField;
          TPromABSDataIns: TDateTimeField;
          TPromABSDataDaLeggere: TDateTimeField;
          TPromABSTesto: TStringField;
          TPromABSDataLettura: TDateTimeField;
          TPromABSEvaso: TBooleanField;
          TUsersLK: TTable;
          TUsersLKID: TAutoIncField;
          TUsersLKNominativo: TStringField;
          TPromABSUtente: TStringField;
          PageControl1: TPageControl;
          TSAgenda: TTabSheet;
          TSCandidati: TTabSheet;
          Panel1: TPanel;
          Panel2: TPanel;
          Panel3: TPanel;
          BNuovoImpegno: TToolbarButton97;
          BCancella: TToolbarButton97;
          BSpostaImpegno: TToolbarButton97;
          BGestisciRic: TToolbarButton97;
          ASG1: TAdvStringGrid;
          Panel4: TPanel;
          Panel6: TPanel;
          ASG2: TAdvStringGrid;
          Panel7: TPanel;
          BNuovoProm: TToolbarButton97;
          BEliminaProm: TToolbarButton97;
          BModProm: TToolbarButton97;
          BSettPrec: TToolbarButton97;
          BSettSucc: TToolbarButton97;
          Splitter1: TSplitter;
          Panel8: TPanel;
          QCandidati: TQuery;
          DsQCandidati: TDataSource;
          QCandidatiRif: TStringField;
          QCandidatiCliente: TStringField;
          QCandidatiRuolo: TStringField;
          QCandidatiStato: TStringField;
          Label2: TLabel;
          ToolbarButton971: TToolbarButton97;
          QCandidatiIDRicerca: TIntegerField;
          ToolbarButton972: TToolbarButton97;
          QCandidatiID: TIntegerField;
          QCandidatiDataIns: TDateTimeField;
          QCandidatiNominativo: TStringField;
          QAgendaIDRisorsa: TIntegerField;
          TRisorseABS: TTable;
          TRisorseABSID: TAutoIncField;
          TRisorseABSRisorsa: TStringField;
          TRisorseABSColor: TStringField;
          TSAgendaRis: TTabSheet;
          Panel9: TPanel;
          BSettPrec1: TToolbarButton97;
          BSettSucc1: TToolbarButton97;
          Panel10: TPanel;
          Panel11: TPanel;
          ASGRis: TAdvStringGrid;
          PanUtenti: TPanel;
          Label1: TLabel;
          DBGrid1: TDBGrid;
          PanRisorse: TPanel;
          Label3: TLabel;
          DsRisorse: TDataSource;
          DBGrid2: TDBGrid;
          QRisorseDisp: TQuery;
          QRisorseDispData: TDateTimeField;
          QRisorseDispOre: TDateTimeField;
          QRisorseDispIDUtente: TIntegerField;
          QRisorseDispTipo: TSmallintField;
          QRisorseDispIDCandRic: TIntegerField;
          QRisorseDispDescrizione: TStringField;
          QRisorseDispIDRisorsa: TIntegerField;
          QRisorseDispUtente: TStringField;
          QUsers: TQuery;
          QUsersID: TAutoIncField;
          QUsersNominativo: TStringField;
          QUsersDescrizione: TStringField;
          BAgendaStampa: TToolbarButton97;
          QCheck: TQuery;
          QAgendaAlleOre: TDateTimeField;
          Q: TQuery;
          QRisorseDispAlleOre: TDateTimeField;
          QAgendaID: TIntegerField;
          QRisorseDispID: TIntegerField;
          QAgendaABS: TQuery;
          QAgendaABSID: TAutoIncField;
          QAgendaABSData: TDateTimeField;
          QAgendaABSOre: TDateTimeField;
          QAgendaABSIDUtente: TIntegerField;
          QAgendaABSTipo: TSmallintField;
          QAgendaABSIDCandRic: TIntegerField;
          QAgendaABSDescrizione: TStringField;
          QAgendaABSIDRisorsa: TIntegerField;
          QAgendaABSAlleOre: TDateTimeField;
          BSpostaImpegnoRis: TToolbarButton97;
          ToolbarButton973: TToolbarButton97;
          ToolbarButton974: TToolbarButton97;
          ToolbarButton975: TToolbarButton97;
          TSCandUtente: TTabSheet;
          CandUtenteFrame1: TCandUtenteFrame;
          TRisorse: TQuery;
          TRisorseID: TAutoIncField;
          TRisorseRisorsa: TStringField;
          TRisorseColor: TStringField;
          BArchiviaProm: TToolbarButton97;
          BArchivioProm: TToolbarButton97;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1Rif: TdxDBGridMaskColumn;
          dxDBGrid1Cliente: TdxDBGridMaskColumn;
          dxDBGrid1Ruolo: TdxDBGridMaskColumn;
          dxDBGrid1Nominativo: TdxDBGridMaskColumn;
          dxDBGrid1Stato: TdxDBGridMaskColumn;
          dxDBGrid1DataIns: TdxDBGridDateColumn;
          BSituazCandStampa: TToolbarButton97;
          dxPrinter1: TdxComponentPrinter;
          dxPrinter1Link1: TdxDBGridReportLink;
          procedure ASG1ClickCell(Sender: TObject; Arow,Acol: Integer);
          procedure ASG1GetAlignment(Sender: TObject; ARow,ACol: Integer;
               var AAlignment: TAlignment);
          procedure ASG1GetCellColor(Sender: TObject; ARow,ACol: Integer;
               AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
          procedure FormShow(Sender: TObject);
          procedure TUsersAfterScroll(DataSet: TDataSet);
          procedure ASG2GetAlignment(Sender: TObject; ARow,ACol: Integer;
               var AAlignment: TAlignment);
          procedure ASG2GetCellColor(Sender: TObject; ARow,ACol: Integer;
               AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
          procedure BSettPrecClick(Sender: TObject);
          procedure BSettSuccClick(Sender: TObject);
          procedure BNuovoImpegnoClick(Sender: TObject);
          procedure BCancellaClick(Sender: TObject);
          procedure BSpostaImpegnoClick(Sender: TObject);
          procedure BEsciClick(Sender: TObject);
          procedure modificaimpegno1Click(Sender: TObject);
          procedure nuovoimpegno1Click(Sender: TObject);
          procedure eliminaimpegno1Click(Sender: TObject);
          procedure ASG1DblClick(Sender: TObject);
          procedure BModPromClick(Sender: TObject);
          procedure vedispostamessaggio1Click(Sender: TObject);
          procedure nuovomessaggio1Click(Sender: TObject);
          procedure eliminamessaggio1Click(Sender: TObject);
          procedure ASG2ClickCell(Sender: TObject; Arow,Acol: Integer);
          procedure ASG2DblClick(Sender: TObject);
          procedure BNuovoPromClick(Sender: TObject);
          procedure BEliminaPromClick(Sender: TObject);
          procedure BGestisciRicClick(Sender: TObject);
          procedure PageControl1Change(Sender: TObject);
          procedure ToolbarButton971Click(Sender: TObject);
          procedure ToolbarButton972Click(Sender: TObject);
          procedure ASGRisClickCell(Sender: TObject; Arow,Acol: Integer);
          procedure ASGRisGetAlignment(Sender: TObject; ARow,ACol: Integer;
               var AAlignment: TAlignment);
          procedure TRisorseAfterScroll(DataSet: TDataSet);
          procedure BSettPrec1Click(Sender: TObject);
          procedure BSettSucc1Click(Sender: TObject);
          procedure ASGRisGetCellColor(Sender: TObject; ARow,ACol: Integer;
               AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
          procedure FormCreate(Sender: TObject);
          procedure QUsersAfterScroll(DataSet: TDataSet);
          procedure BAgendaStampaClick(Sender: TObject);
          procedure BSpostaImpegnoRisClick(Sender: TObject);
          procedure ToolbarButton974Click(Sender: TObject);
          procedure ToolbarButton975Click(Sender: TObject);
          procedure BArchiviaPromClick(Sender: TObject);
          procedure BArchivioPromClick(Sender: TObject);
          procedure BSituazCandStampaClick(Sender: TObject);
     private
          { Private declarations }
          xColonnaSel: TColumn;
          procedure InsMessaggi;
          procedure InsImpegniAgenda;
          procedure InsImpegniAgendaRis;
          procedure ColonneOre;
          procedure ColonneOreRis;
          function LogEsportazioniCand: boolean;
     public
          xSelRow,xSelCol,xSelRow2,xSelCol2,xSelRowRis,xSelColRis,xIDSel: integer;
          xPrimaData: TDateTime;
          xGiorni: array[1..6] of integer;
          xOre: array[1..50] of integer;
          xArrayCol: array[1..6,1..50] of string;
          xArrayIDAgenda: array[1..6,1..50] of integer;
          xArrayIDProm: array[1..6,1..20] of integer;
          procedure ComponiGriglia;
          procedure ComponiGrigliaRis;
     end;

var
     AgendaSettForm: TAgendaSettForm;

implementation

uses NuovoApp,Sposta,ModMessaggio,InsMessaggio,SelPers,MDRicerche,
     ModuloDati2,ModuloDati,uUtilsVarie,RepAgenda,Main,ArchivioMess;

{$R *.DFM}

procedure TAgendaSettForm.ASG1ClickCell(Sender: TObject; Arow,
     Acol: Integer);
begin
     xSelRow:=ARow;
     xSelCol:=Acol;
end;

procedure TAgendaSettForm.ASG1GetAlignment(Sender: TObject; ARow,
     ACol: Integer; var AAlignment: TAlignment);
begin
     if (ACol=0)or(ARow=0) then AAlignment:=taCenter;
end;

procedure TAgendaSettForm.ASG1GetCellColor(Sender: TObject; ARow,
     ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
var i,k: integer;
begin
     if (ARow>0)and(ACol>0) then
          ABrush.Color:=clWindow;
     if ARow=0 then
          afont.style:= [fsBold];
     for i:=1 to ASG1.ColCount do begin
          for k:=1 to ASG1.RowCount do begin
               if xArrayCol[i,k]<>'' then begin
                    if (ACol=i)and(ARow=k) then
                         if xArrayCol[i,k]='clLime' then ABrush.Color:=clLime
                         else
                              if xArrayCol[i,k]='clRed' then ABrush.Color:=clRed
                              else
                                   if xArrayCol[i,k]='clYellow' then ABrush.Color:=clYellow
                                   else
                                        if xArrayCol[i,k]='clAqua' then ABrush.Color:=clAqua;
               end;
          end;
     end;
end;

procedure TAgendaSettForm.InsImpegniAgenda;
var xAnno,xMese,xGiorno,xOra,xMin,xSec,xMSec,xOra2,xMin2,xSec2,xMSec2: Word;
     k,i,h,j,xRiga,xColonna: integer;
     xMinStr,xMinStr2,xDesc: string;
begin
     ShortTimeFormat:='hh.mm';
     QAgenda.Close;
     QAgenda.Prepare;
     QAgenda.Params[0].AsDateTime:=xPrimaData;
     QAgenda.Params[1].AsDateTime:=xPrimaData+5;
     QAgenda.Params[2].Value:=QUsersID.Value;
     QAgenda.Open;
     for i:=1 to 6 do
          for k:=1 to ASG1.RowCount do xArrayCol[i,k]:='';
     // repulisti array IDagenda
     for i:=1 to 6 do
          for k:=1 to 50 do xArrayIDAgenda[i,k]:=0;

     while not QAgenda.EOF do begin
          DecodeDate(QAgendaData.Value,xAnno,xMese,xGiorno);
          DecodeTime(QAgendaOre.Value,xOra,xMin,xSec,xMSec);
          DecodeTime(QAgendaAlleOre.Value,xOra2,xMin2,xSec2,xMSec2);
          if xMin<10 then xMinStr:='0'+IntToStr(xMin)
          else xMinStr:=IntToStr(xMin);
          if xMin2<10 then xMinStr2:='0'+IntToStr(xMin2)
          else xMinStr2:=IntToStr(xMin2);
          // cerca colonna giusta
          for k:=1 to 6 do if xGiorni[k]=xGiorno then begin xColonna:=k; break; end;
          // cerca riga giusta
          for h:=1 to 50 do if xore[h]=xOra then begin xRiga:=h; break; end;

          while ASG1.Cells[xColonna,xRiga]<>'' do begin
               if xOre[xRiga+1]<>xOra then
                    ASG1.InsertRows(xRiga+1,1);
               for i:=0 to ASG1.RowCount-1 do ASG1.RowHeights[i]:=16;
               inc(xRiga);
          end;

          ASG1.AddImageIdx(xColonna,xRiga,QAgendaTipo.Value,habeforetext,vaCenter);
          if QAgendaIDCandRic.AsString='' then begin
               if xOra2>0 then
                    ASG1.Cells[xColonna,xRiga]:=IntToStr(xOra)+'.'+xMinStr+'-'+IntToStr(xOra2)+'.'+xMinStr2+' '+LowerCase(QAgendaDescrizione.Value)
               else ASG1.Cells[xColonna,xRiga]:=IntToStr(xOra)+'.'+xMinStr+' '+LowerCase(QAgendaDescrizione.Value);
               xArrayIDAgenda[xColonna,xRiga]:=QAgendaID.Value
          end else begin
               Q.Close;
               Q.SQL.text:='select EBC_Ricerche.Progressivo,EBC_Clienti.Descrizione Cliente '+
                    'from EBC_CandidatiRicerche,EBC_Ricerche,EBC_Clienti '+
                    'where EBC_CandidatiRicerche.IDRicerca=EBC_Ricerche.ID '+
                    'and EBC_Ricerche.IDCliente=EBC_Clienti.ID and EBC_CandidatiRicerche.ID='+QAgendaIDCandRic.asString;
               Q.Open;

               xDesc:=LowerCase(QAgendaDescrizione.Value)+' (Rif.'+Q.FieldByName('Progressivo').asString+') '+
                    Q.FieldByName('Cliente').asString;
               if xOra2>0 then
                    ASG1.Cells[xColonna,xRiga]:=IntToStr(xOra)+'.'+xMinStr+'-'+IntToStr(xOra2)+'.'+xMinStr2+' '+xDesc
               else ASG1.Cells[xColonna,xRiga]:=IntToStr(xOra)+'.'+xMinStr+' '+xDesc;
               xArrayIDAgenda[xColonna,xRiga]:=QAgendaID.Value;
          end;
          // colore a seconda della risorsa
          TRisorseABS.Open;
          if QAgendaIDRisorsa.asString<>'' then begin
               if TRisorseABS.FindKey([QAgendaIDRisorsa.value]) then
                    xArrayCol[xColonna,xRiga]:=TRisorseABSColor.Value;
          end;
          TRisorseABS.Close;

          xSelRow:=xRiga;
          xSelCol:=xColonna;
          ColonneOre;
          QAgenda.Next;
     end;
end;

procedure TAgendaSettForm.InsImpegniAgendaRis;
var xAnno,xMese,xGiorno,xOra,xMin,xSec,xMSec,xOra2,xMin2,xSec2,xMSec2: Word;
     k,i,h,j,xRiga,xColonna: integer;
     xMinStr,xMinStr2,xDesc: string;
begin
     ShortTimeFormat:='hh.mm';
     QRisorseDisp.Close;
     QRisorseDisp.Prepare;
     QRisorseDisp.Params[0].AsDateTime:=xPrimaData;
     QRisorseDisp.Params[1].AsDateTime:=xPrimaData+5;
     QRisorseDisp.Params[2].Value:=TRisorseID.Value;
     QRisorseDisp.Open;
     while not QRisorseDisp.EOF do begin
          DecodeDate(QRisorseDispData.Value,xAnno,xMese,xGiorno);
          DecodeTime(QRisorseDispOre.Value,xOra,xMin,xSec,xMSec);
          DecodeTime(QRisorseDispAlleOre.Value,xOra2,xMin2,xSec2,xMSec2);
          if xMin<10 then xMinStr:='0'+IntToStr(xMin)
          else xMinStr:=IntToStr(xMin);
          if xMin2<10 then xMinStr2:='0'+IntToStr(xMin2)
          else xMinStr2:=IntToStr(xMin2);
          // cerca colonna giusta
          for k:=1 to 6 do if xGiorni[k]=xGiorno then begin xColonna:=k; break; end;
          // cerca riga giusta
          for h:=1 to 50 do if xore[h]=xOra then begin xRiga:=h; break; end;

          while ASGRis.Cells[xColonna,xRiga]<>'' do begin
               if xOre[xRiga+1]<>xOra then
                    ASGRis.InsertRows(xRiga+1,1);
               for i:=0 to ASGRis.RowCount-1 do ASGRis.RowHeights[i]:=16;
               inc(xRiga);
          end;

          ASGRis.AddImageIdx(xColonna,xRiga,QRisorseDispTipo.Value,habeforetext,vaCenter);
          if xOra2>0 then
               ASGRis.Cells[xColonna,xRiga]:=IntToStr(xOra)+'.'+xMinStr+'-'+IntToStr(xOra2)+'.'+xMinStr2+' '+LowerCase(QRisorseDispUtente.Value)
          else ASGRis.Cells[xColonna,xRiga]:=IntToStr(xOra)+'.'+xMinStr+' '+LowerCase(QRisorseDispUtente.Value);
          xArrayIDAgenda[xColonna,xRiga]:=QRisorseDispID.Value;

          xSelRowRis:=xRiga;
          xSelColRis:=xColonna;
          ColonneOreRis;
          QRisorseDisp.Next;
     end;
end;

procedure TAgendaSettForm.ColonneOre;
var k,x: integer;
begin
     x:=8;
     for k:=1 to ASG1.RowCount-1 do begin
          if ASG1.Cells[0,k]<>'' then inc(x);
          xOre[k]:=x;
     end;
end;

procedure TAgendaSettForm.ColonneOreRis;
var k,x: integer;
begin
     x:=8;
     for k:=1 to ASGRis.RowCount-1 do begin
          if ASGRis.Cells[0,k]<>'' then inc(x);
          xOre[k]:=x;
     end;
end;

procedure TAgendaSettForm.FormShow(Sender: TObject);
var xData: TDateTime;
begin
     QUsers.ParamByName('xOggi').asDateTime:=Date;
     QUsers.Open;
     if xIDSel>0 then begin
          while (not QUsers.EOF)and(QUsersID.Value<>xIDSel) do
               QUsers.Next;
     end;
     if DayOfWeek(Date)=2 then xPrimaData:=Date
     else begin
          xData:=Date;
          while DayOfWeek(xData)<>2 do xData:=xData-1;
          xPrimaData:=xData;
     end;
     PageControl1.ActivePage:=TSAgenda;
     ComponiGriglia;
     ComponiGrigliaRis;
     Caption:='[M/4] - '+Caption;
end;

procedure TAgendaSettForm.ComponiGriglia;
const GiorniSett: array[1..7] of string=('Lu','Ma','Me','Gi','Ve','Sa','Do');
var i,k: integer;
     xAnno,xMese,xGiorno,xAnnoOggi,xMeseOggi,xGiornoOggi: Word;
begin
     ASG1.Clear;
     ASG1.RowCount:=13;
     ASG1.ColCount:=7;
     for i:=0 to ASG1.RowCount-1 do begin
          ASG1.RowHeights[i]:=16;
          if i=0 then ASG1.Cells[0,i]:='orario'
          else if i<13 then ASG1.Cells[0,i]:=IntToStr(i+8)+'.00';
     end;
     for i:=0 to ASG1.ColCount-1 do begin
          if i=0 then ASG1.ColWidths[i]:=40
          else ASG1.ColWidths[i]:=120;
          if i>0 then begin
               ASG1.Cells[i,0]:=GiorniSett[i]+' '+DateToStr(xPrimaData+i-1);
               DecodeDate(xPrimaData+i-1,xAnno,xMese,xGiorno);
               xGiorni[i]:=xGiorno;
          end;
     end;
     ColonneOre;
     InsImpegniAgenda;

     // griglia promemoria
     ASG2.Clear;
     ASG2.RowCount:=5;
     ASG2.ColCount:=7;
     for i:=0 to ASG2.RowCount-1 do begin
          ASG2.RowHeights[i]:=16;
          if i=0 then ASG2.Cells[0,i]:=''
          else if i<5 then ASG2.Cells[0,i]:='';
     end;
     for i:=0 to ASG2.ColCount-1 do begin
          if i=0 then ASG2.ColWidths[i]:=40
          else ASG2.ColWidths[i]:=120;
          if i>0 then begin
               ASG2.Cells[i,0]:=GiorniSett[i]+' '+DateToStr(xPrimaData+i-1);
          end;
     end;
     InsMessaggi;
end;

procedure TAgendaSettForm.ComponiGrigliaRis;
const GiorniSett: array[1..7] of string=('Lu','Ma','Me','Gi','Ve','Sa','Do');
var i,k: integer;
     xAnno,xMese,xGiorno,xAnnoOggi,xMeseOggi,xGiornoOggi: Word;
begin
     ASGRis.Clear;
     ASGRis.RowCount:=13;
     ASGRis.ColCount:=7;
     for i:=0 to ASGRis.RowCount-1 do begin
          ASGRis.RowHeights[i]:=16;
          if i=0 then ASGRis.Cells[0,i]:='orario'
          else if i<13 then ASGRis.Cells[0,i]:=IntToStr(i+8)+'.00';
     end;
     for i:=0 to ASGRis.ColCount-1 do begin
          if i=0 then ASGRis.ColWidths[i]:=40
          else ASGRis.ColWidths[i]:=120;
          if i>0 then begin
               ASGRis.Cells[i,0]:=GiorniSett[i]+' '+DateToStr(xPrimaData+i-1);
               DecodeDate(xPrimaData+i-1,xAnno,xMese,xGiorno);
               xGiorni[i]:=xGiorno;
          end;
     end;
     ColonneOreRis;
     InsImpegniAgendaRis;
end;

procedure TAgendaSettForm.TUsersAfterScroll(DataSet: TDataSet);
begin
     ComponiGriglia;
end;

procedure TAgendaSettForm.ASG2GetAlignment(Sender: TObject; ARow,
     ACol: Integer; var AAlignment: TAlignment);
begin
     if (ACol=0)or(ARow=0) then AAlignment:=taCenter;
end;

procedure TAgendaSettForm.ASG2GetCellColor(Sender: TObject; ARow,
     ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
     if ARow=0 then
          afont.style:= [fsBold];
end;

procedure TAgendaSettForm.InsMessaggi;
var xAnno,xMese,xGiorno,xOra,xMin,xSec,xMSec: Word;
     k,i,h,xRiga,xColonna: integer;
     xMinStr,xDesc: string;
begin
     QPromemoria.Close;
     QPromemoria.Prepare;
     QPromemoria.Params[0].AsDateTime:=xPrimaData;
     QPromemoria.Params[1].AsDateTime:=xPrimaData+5;
     QPromemoria.Params[2].Value:=QUsersID.Value;
     QPromemoria.Open;
     while not QPromemoria.EOF do begin
          DecodeDate(QPromemoriaDataDaLeggere.Value,xAnno,xMese,xGiorno);
          // cerca colonna giusta
          for k:=1 to 6 do
               if xGiorni[k]=xGiorno then begin xColonna:=k; break; end;
          xRiga:=1;
          while ASG2.Cells[xColonna,xRiga]<>'' do begin
               inc(xRiga);
          end;
          if xRiga>=5 then ASG2.InsertRows(xRiga,1);

          ASG2.Cells[xColonna,xRiga]:=QPromemoriaTesto.Value;
          xArrayIDProm[xColonna,xRiga]:=QPromemoriaID.Value;

          QPromemoria.Next;
     end;
     for i:=0 to ASG2.RowCount-1 do ASG2.RowHeights[i]:=16;
end;

procedure TAgendaSettForm.BSettPrecClick(Sender: TObject);
begin
     xPrimaData:=xPrimaData-7;
     ComponiGriglia;
end;

procedure TAgendaSettForm.BSettSuccClick(Sender: TObject);
begin
     xPrimaData:=xPrimaData+7;
     ComponiGriglia;
end;

procedure TAgendaSettForm.BNuovoImpegnoClick(Sender: TObject);
var xHour,xMin,xSec,xMSec,xHour2,xMin2,xSec2,xMSec2: Word;
     xVai: boolean;
begin
     NuovoImpForm:=TNuovoImpForm.create(self);
     if PageControl1.ActivePageIndex=0 then begin
          NuovoImpForm.DEData.Date:=xPrimaData+xSelCol-1;
          NuovoImpForm.MEOre.Text:=IntToStr(xOre[xSelRow])+'.00';
     end else begin
          NuovoImpForm.DEData.Date:=xPrimaData+xSelColRis-1;
          NuovoImpForm.MEOre.Text:=IntToStr(xOre[xSelRowRis])+'.00';
          while (not NuovoImpForm.TRisorse.EOF)and(TRisorseID.Value<>NuovoImpForm.TRisorseID.Value) do
               NuovoImpForm.TRisorse.Next;
          NuovoImpForm.ActiveControl:=NuovoImpForm.MEAlleOre;
     end;
     //NuovoImpForm.MEAlleOre.Text:=IntToStr(xOre[xSelRow])+'.30';
     NuovoImpForm.ShowModal;
     xVai:=True;
     if NuovoImpForm.ModalResult=mrOK then begin
          Data.DB.BeginTrans;
          try
               Data.Q1.Close;
               if NuovoImpForm.MEAlleOre.Text<>'  .  ' then
                    Data.Q1.SQL.text:='insert into Agenda (Data,Ore,AlleOre,Descrizione,IDUtente,Tipo,IDRisorsa) '+
                         'values (:xData,:xOre,:xAlleOre,:xDescrizione,:xIDUtente,:xTipo,:xIDRisorsa)'
               else
                    Data.Q1.SQL.text:='insert into Agenda (Data,Ore,Descrizione,IDUtente,Tipo,IDRisorsa) '+
                         'values (:xData,:xOre,:xDescrizione,:xIDUtente,:xTipo,:xIDRisorsa)';
               Data.Q1.ParambyName('xData').asDateTime:=NuovoImpForm.DEData.Date;
               Data.Q1.ParambyName('xOre').asDateTime:=StrToDateTime(DatetoStr(NuovoImpForm.DEData.Date)+' '+NuovoImpForm.MEOre.Text);
               if NuovoImpForm.MEAlleOre.Text<>'  .  ' then
                    Data.Q1.ParambyName('xAlleOre').asDateTime:=StrToDateTime(DatetoStr(NuovoImpForm.DEData.Date)+' '+NuovoImpForm.MEAlleOre.Text);
               Data.Q1.ParambyName('xDescrizione').asString:=NuovoImpForm.EDescriz.Text;
               Data.Q1.ParambyName('xTipo').asInteger:=NuovoImpForm.RGTipo.Itemindex+1;
               Data.Q1.ParambyName('xIDUtente').asInteger:=QUsersID.Value;
               if (NuovoImpForm.RGTipo.ItemIndex=0)and(not NuovoImpForm.CBLuogo.checked) then
                    Data.Q1.ParambyName('xIDRisorsa').asInteger:=NuovoImpForm.TRisorseID.Value
               else Data.Q1.ParambyName('xIDRisorsa').asInteger:=0;
               Data.Q1.ExecSQL;

               Data.DB.CommitTrans;
               QAgenda.Close;
               QAgenda.Open;
               if PageControl1.ActivePageIndex=0 then
                    ComponiGriglia
               else ComponiGrigliaRis;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE:  operazione non completata',mtError, [mbOK],0);
          end;
     end;
     NuovoImpForm.Free;
end;

procedure TAgendaSettForm.BCancellaClick(Sender: TObject);
begin
     if ASG1.Cells[xSelCol,xSelRow]<>'' then begin
          if Messagedlg('Sei sicuro di eliminare l''appuntamento ?',mtWarning, [mbNo,mbYes],0)=mrYes then begin
               with Data do begin
                    DB.BeginTrans;
                    try
                         Q1.Close;
                         Q1.SQL.text:='delete from Agenda where ID='+IntToStr(xArrayIDAgenda[xSelCol,xSelRow]);
                         Q1.ExecSQL;
                         DB.CommitTrans;
                    except
                         DB.RollbackTrans;
                         MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
                    end;
               end;
               QAgenda.Close;
               QAgenda.Open;
               ComponiGriglia;
          end;
     end;
end;

procedure TAgendaSettForm.BSpostaImpegnoClick(Sender: TObject);
var xOra,xMin,xSec,xMSec,xOra2,xMin2,xSec2,xMSec2: Word;
     xIDAgenda: integer;
     xMinStr,xMinStr2: string;
begin
     xIDAgenda:=xArrayIDAgenda[xSelCol,xSelRow];
     if xIDAgenda>0 then begin
          QAgendaABS.Close;
          QAgendaABS.Params[0].asInteger:=xIDAgenda;
          QAgendaABS.Open;

          SpostaForm:=TSpostaForm.create(self);
          SpostaForm.DEData.Date:=QAgendaABSData.Value;
          while (not SpostaForm.TRisorse.EOF)and(QAgendaABSIDRisorsa.Value<>SpostaForm.TRisorseID.Value) do
               SpostaForm.TRisorse.Next;
          // dalle ore
          DecodeTime(QAgendaABSOre.Value,xOra,xMin,xSec,xMSec);
          if xMin<10 then xMinStr:='0'+IntToStr(xMin)
          else xMinStr:=IntToStr(xMin);
          SpostaForm.MEOre.Text:=IntToStr(xOra)+'.'+xMinStr;
          // alle ore
          DecodeTime(QAgendaABSAlleOre.Value,xOra2,xMin2,xSec2,xMSec2);
          if xMin2<10 then xMinStr2:='0'+IntToStr(xMin2)
          else xMinStr2:=IntToStr(xMin2);
          if xOra2>0 then SpostaForm.MEAlleOre.Text:=IntToStr(xOra2)+'.'+xMinStr2;
          SpostaForm.EDescriz.Text:=QAgendaABSDescrizione.Value;
          SpostaForm.RGTipo.Itemindex:=QAgendaABSTipo.Value-1;
          if SpostaForm.RGTipo.ItemIndex=0 then
               SpostaForm.PanLuogo.Visible:=true
          else SpostaForm.PanLuogo.Visible:=False;
          SpostaForm.ShowModal;
          if SpostaForm.ModalResult=mrOK then begin
               // controllo concomitanza in SpostaForm
               Data.DB.BeginTrans;
               try
                    Data.Q1.Close;
                    if SpostaForm.MEAlleOre.Text<>'  .  ' then
                         Data.Q1.SQL.text:='update Agenda set Data=:xData,Ore=:xOre,AlleOre=:xAlleOre,Descrizione=:xDescrizione,IDRisorsa=:xIDRisorsa,Tipo=:xTipo '+
                              'where ID='+IntToStr(xIDAgenda)
                    else
                         Data.Q1.SQL.text:='update Agenda set Data=:xData,Ore=:xOre,Descrizione=:xDescrizione,IDRisorsa=:xIDRisorsa,Tipo=:xTipo '+
                              'where ID='+IntToStr(xIDAgenda);
                    Data.Q1.ParambyName('xData').asDateTime:=SpostaForm.DEData.Date;
                    Data.Q1.ParambyName('xOre').asDateTime:=StrToDateTime(DatetoStr(SpostaForm.DEData.Date)+' '+SpostaForm.MEOre.Text);
                    if SpostaForm.MEAlleOre.Text<>'  .  ' then
                         Data.Q1.ParambyName('xAlleOre').asDateTime:=StrToDateTime(DatetoStr(SpostaForm.DEData.Date)+' '+SpostaForm.MEAlleOre.Text);
                    Data.Q1.ParambyName('xDescrizione').asString:=SpostaForm.EDescriz.Text;
                    if (SpostaForm.RGTipo.ItemIndex=0)and(not SpostaForm.CBLuogo.checked) then
                         Data.Q1.ParambyName('xIDRisorsa').asInteger:=SpostaForm.TRisorseID.Value
                    else Data.Q1.ParambyName('xIDRisorsa').asInteger:=0;
                    Data.Q1.ParambyName('xTipo').asInteger:=SpostaForm.RGTipo.Itemindex+1;
                    Data.Q1.ExecSQL;

                    Data.DB.CommitTrans;
                    QAgenda.Close;
                    QAgenda.Open;
                    ComponiGriglia;
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE:  operazione non completata',mtError, [mbOK],0);
               end;
          end;
          SpostaForm.Free;
          QAgendaABS.Close;
     end;
end;

procedure TAgendaSettForm.BEsciClick(Sender: TObject);
begin
     close
end;

procedure TAgendaSettForm.modificaimpegno1Click(Sender: TObject);
begin
     BSpostaImpegnoClick(self);
end;

procedure TAgendaSettForm.nuovoimpegno1Click(Sender: TObject);
begin
     BNuovoImpegnoClick(self);
end;

procedure TAgendaSettForm.eliminaimpegno1Click(Sender: TObject);
begin
     BCancellaClick(self);
end;

procedure TAgendaSettForm.ASG1DblClick(Sender: TObject);
begin
     BSpostaImpegnoClick(self);
end;

procedure TAgendaSettForm.BModPromClick(Sender: TObject);
var xOra,xMin,xSec,xMSec: Word;
     xMinStr,xCella: string;
     xTent: integer;
begin
     if ASG2.Cells[xSelCol2,xSelRow2]<>'' then begin
          Q.Close;
          Q.SQL.text:='select * from Promemoria where ID='+IntToStr(xArrayIDProm[xSelCol2,xSelRow2]);
          Q.Open;
          ModMessaggioForm:=TModMessaggioForm.create(self);
          ModMessaggioForm.DEData.Date:=Q.FieldbyName('DataDaLeggere').asDateTime;
          ModMessaggioForm.EDescriz.Text:=Q.FieldbyName('Testo').asString;
          ModMessaggioForm.ShowModal;
          if ModMessaggioForm.ModalResult=mrOK then begin
               with Data do begin
                    DB.BeginTrans;
                    try
                         Q1.Close;
                         Q1.SQL.text:='update Promemoria set DataDaLeggere=:xDataDaLeggere,Testo=:xTesto '+
                              'where ID='+Q.FieldbyName('ID').asString;
                         Q1.ParamByName('xDataDaLeggere').asDateTime:=ModMessaggioForm.DEData.Date;
                         Q1.ParamByName('xTesto').asString:=ModMessaggioForm.EDescriz.Text;
                         Q1.ExecSQL;
                         DB.CommitTrans;
                    except
                         DB.RollbackTrans;
                         MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
                    end;
               end;
               QPromemoria.Close;
               QPromemoria.Open;
               ComponiGriglia;
          end;
          Q.Close;
          ModMessaggioForm.Free;
     end;
end;

procedure TAgendaSettForm.vedispostamessaggio1Click(Sender: TObject);
begin
     BModPromClick(self);
end;

procedure TAgendaSettForm.nuovomessaggio1Click(Sender: TObject);
begin
     BNuovoPromClick(self);
end;

procedure TAgendaSettForm.eliminamessaggio1Click(Sender: TObject);
begin
     BEliminaPromClick(self);
end;

procedure TAgendaSettForm.ASG2ClickCell(Sender: TObject; Arow,
     Acol: Integer);
begin
     xSelRow2:=ARow;
     xSelCol2:=Acol;
end;

procedure TAgendaSettForm.ASG2DblClick(Sender: TObject);
begin
     BModPromClick(self);
end;

procedure TAgendaSettForm.BNuovoPromClick(Sender: TObject);
begin
     TPromABS.open;
     TPromABS.Insert;
     TPromABSIDUtente.Value:=QUsersID.Value;
     TPromABSDataIns.Value:=Date;
     TPromABSEvaso.Value:=False;
     TPromABSDataDaLeggere.Value:=xPrimaData+xSelCol2-1;
     InsMessaggioForm:=TInsMessaggioForm.create(self);
     InsMessaggioForm.ShowModal;
     if InsMessaggioForm.ModalResult=MrOK then begin
          TPromABS.Post;
          ComponiGriglia;
     end else TPromABS.Cancel;
     InsMessaggioForm.Free;
     TPromABS.Open;
end;

procedure TAgendaSettForm.BEliminaPromClick(Sender: TObject);
var xCella: string;
begin
     if ASG2.Cells[xSelCol2,xSelRow2]<>'' then begin
          if Messagedlg('Sei sicuro di voler eliminare DEFINITIVAMENTE il messaggio ?',mtWarning, [mbNo,mbYes],0)=mrYes then begin
               with Data do begin
                    DB.BeginTrans;
                    try
                         Q1.Close;
                         Q1.SQL.text:='delete from Promemoria where ID='+IntToStr(xArrayIDProm[xSelCol2,xSelRow2]);
                         Q1.ExecSQL;
                         DB.CommitTrans;
                    except
                         DB.RollbackTrans;
                         MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
                    end;
               end;
               QPromemoria.Close;
               QPromemoria.Open;
               ComponiGriglia;
          end;
     end;
end;

procedure TAgendaSettForm.BGestisciRicClick(Sender: TObject);
begin
     if ASG1.Cells[xSelCol,xSelRow]<>'' then begin
          QAgendaABS.Close;
          QAgendaABS.Params[0].asInteger:=xArrayIDAgenda[xSelCol,xSelRow];
          QAgendaABS.Open;
          if QAgendaABSIDCandRic.asString<>'' then begin
               Q.Close;
               Q.SQL.text:='select IDRicerca from EBC_CandidatiRicerche where ID='+QAgendaABSIDCandRic.asString;
               Q.Open;
               CaricaApriRicPend(Q.FieldByName('IDRicerca').asinteger);

               SelPersForm:=TSelPersForm.create(self);
               SelPersForm.EseguiQueryCandidati;
               SelPersForm.xFromOrgTL:=False;
               SelPersForm.ShowModal;
               try SelPersForm.Free
               except
               end;
               DataRicerche.QRicAttive.Close;
               Mainform.EseguiQRicAttive;
               if Data2.QRicClienti.Active then begin
                    Data2.QRicClienti.Close;
                    Data2.QRicClienti.open;
               end;
               if Data2.QAnagRicerche.Active then begin
                    Data2.QAnagRicerche.Close;
                    Data2.QAnagRicerche.Open;
               end;
               DataRicerche.TRicerchePend.Close;
          end;
          QAgenda.Close;
          QAgenda.Open;
          ComponiGriglia;
     end;
end;

procedure TAgendaSettForm.PageControl1Change(Sender: TObject);
begin
     if PageControl1.ActivePage=TSAgenda then begin
          PanUtenti.Visible:=True;
          PanRisorse.Visible:=False;
          ComponiGriglia;
     end;
     //
     if PageControl1.ActivePage=TSAgendaRis then begin
          PanUtenti.Visible:=False;
          PanRisorse.Visible:=True;
          if TRisorseColor.Value='clLime' then DBGrid2.Color:=clLime
          else
               if TRisorseColor.Value='clRed' then DBGrid2.Color:=clRed
               else
                    if TRisorseColor.Value='clYellow' then DBGrid2.Color:=clYellow
                    else
                         if TRisorseColor.Value='clAqua' then DBGrid2.Color:=clAqua;
          ComponiGrigliaRis;
     end;
     //
     if PageControl1.ActivePage=TSCandidati then begin
          QCandidati.Open;
          PanUtenti.Visible:=true;
     end else QCandidati.Close;
     //
     if PageControl1.ActivePage=TSCandUtente then begin
          CandUtenteFrame1.QCandUtente.Close;
          CandUtenteFrame1.QCandUtente.ParamByName('xIDUtente').asInteger:=QUsersID.Value;
          CandUtenteFrame1.QCandUtente.Open;
          CandUtenteFrame1.xIDUtente:=QUsersID.Value;
          CandUtenteFrame1.xColumnRic:=CandUtenteFrame1.RxDBGrid1.Columns[1];
          PanUtenti.Visible:=true;
          PanRisorse.Visible:=False;
     end;
end;

procedure TAgendaSettForm.ToolbarButton971Click(Sender: TObject);
begin
     CaricaApriRicPend(QCandidatiIDRicerca.Value);
     SelPersForm:=TSelPersForm.create(self);
     SelPersForm.CBOpzioneCand.Checked:=True;
     SelPersForm.EseguiQueryCandidati;
     SelPersForm.xFromOrgTL:=False;
     SelPersForm.ShowModal;
     try SelPersForm.Free
     except
     end;
     DataRicerche.TRicerchePend.Close;
end;

procedure TAgendaSettForm.ToolbarButton972Click(Sender: TObject);
var xDic: string;
     SavePlace: TBookmark;
begin
     if not QCandidati.EOF then begin
          xDic:=QCandidatiStato.Value;
          if InputQuery('Modifica stato','Nuova descrizione:',xDic) then begin
               SavePlace:=QCandidati.GetBookmark;
               Data.DB.BeginTrans;
               try
                    Data.Q1.Close;
                    Data.Q1.SQL.text:='update EBC_CandidatiRicerche set Stato=:xStato where ID='+QCandidatiID.asString;
                    Data.Q1.ParambyName('xStato').asString:=xDic;
                    Data.Q1.ExecSQL;
                    Data.DB.CommitTrans;
                    QCandidati.Close;
                    QCandidati.Open;
                    QCandidati.GotoBookmark(SavePlace);
                    QCandidati.FreeBookmark(SavePlace);
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE:  operazione non completata',mtError, [mbOK],0);
               end;
          end;
     end;
end;

procedure TAgendaSettForm.ASGRisClickCell(Sender: TObject; Arow,
     Acol: Integer);
begin
     xSelRowRis:=ARow;
     xSelColRis:=Acol;
end;

procedure TAgendaSettForm.ASGRisGetAlignment(Sender: TObject; ARow,
     ACol: Integer; var AAlignment: TAlignment);
begin
     if (ACol=0)or(ARow=0) then AAlignment:=taCenter;
end;

procedure TAgendaSettForm.TRisorseAfterScroll(DataSet: TDataSet);
begin
     if TRisorseColor.Value='clLime' then DBGrid2.Color:=clLime
     else
          if TRisorseColor.Value='clRed' then DBGrid2.Color:=clRed
          else
               if TRisorseColor.Value='clYellow' then DBGrid2.Color:=clYellow
               else
                    if TRisorseColor.Value='clAqua' then DBGrid2.Color:=clAqua;
     ComponiGrigliaRis;
end;

procedure TAgendaSettForm.BSettPrec1Click(Sender: TObject);
begin
     xPrimaData:=xPrimaData-7;
     ComponiGrigliaRis;
end;

procedure TAgendaSettForm.BSettSucc1Click(Sender: TObject);
begin
     xPrimaData:=xPrimaData+7;
     ComponiGrigliaRis;
end;

procedure TAgendaSettForm.ASGRisGetCellColor(Sender: TObject; ARow,
     ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
     if ARow=0 then
          afont.style:= [fsBold];
end;

procedure TAgendaSettForm.FormCreate(Sender: TObject);
begin
     xIDSel:=0;
end;

procedure TAgendaSettForm.QUsersAfterScroll(DataSet: TDataSet);
begin
     ComponiGriglia;
     if PageControl1.ActivePage=TSCandUtente then begin
          CandUtenteFrame1.QCandUtente.Close;
          CandUtenteFrame1.QCandUtente.ParamByName('xIDUtente').asInteger:=QUsersID.Value;
          CandUtenteFrame1.QCandUtente.Open;
          CandUtenteFrame1.xIDUtente:=QUsersID.Value;
     end;
end;

procedure TAgendaSettForm.BAgendaStampaClick(Sender: TObject);
begin
     QRAgenda:=TQRAgenda.create(self);
     QRAgenda.QRDate.caption:='Settimana dal '+DateToStr(xPrimaData)+' al '+DateToStr(xPrimaData+5);
     QRAgenda.QDate.SQL.text:='select distinct Data from agenda '+
          'where Data>=:xDal and Data<=:xAl ';
     QRAgenda.QDate.Prepare;
     QRAgenda.QDate.Params[0].AsDateTime:=xPrimaData;
     QRAgenda.QDate.Params[1].AsDateTime:=xPrimaData+5;

     QRAgenda.QAgenda.SQL.text:='select * from agenda '+
          'where Data=:Data '+
          'and IDUtente=:xIDUtente '+
          'order by Ore ';
     QRAgenda.QAgenda.Prepare;
     QRAgenda.QAgenda.Params[1].Value:=QUsersID.Value;
     QRAgenda.QDate.open;
     QRAgenda.QAgenda.open;
     QRAgenda.QR1.Preview;
     QRAgenda.Free;
end;

procedure TAgendaSettForm.BSpostaImpegnoRisClick(Sender: TObject);
var xOra,xMin,xSec,xMSec,xOra2,xMin2,xSec2,xMSec2: Word;
     xMinStr,xMinStr2: string;
begin
     if ASGRis.Cells[xSelColRis,xSelRowRis]<>'' then begin
          QAgendaABS.Close;
          QAgendaABS.Params[0].asInteger:=xArrayIDAgenda[xSelColRis,xSelRowRis];
          QAgendaABS.Open;

          SpostaForm:=TSpostaForm.create(self);
          SpostaForm.DEData.Date:=QAgendaABSData.Value;
          while (not SpostaForm.TRisorse.EOF)and(QAgendaABSIDRisorsa.Value<>SpostaForm.TRisorseID.Value) do
               SpostaForm.TRisorse.Next;
          // dalle ore
          DecodeTime(QAgendaABSOre.Value,xOra,xMin,xSec,xMSec);
          if xMin<10 then xMinStr:='0'+IntToStr(xMin)
          else xMinStr:=IntToStr(xMin);
          SpostaForm.MEOre.Text:=IntToStr(xOra)+'.'+xMinStr;
          // alle ore
          DecodeTime(QAgendaABSAlleOre.Value,xOra2,xMin2,xSec2,xMSec2);
          if xMin2<10 then xMinStr2:='0'+IntToStr(xMin2)
          else xMinStr2:=IntToStr(xMin2);
          if xOra2>0 then SpostaForm.MEAlleOre.Text:=IntToStr(xOra2)+'.'+xMinStr2;
          SpostaForm.EDescriz.Text:=QAgendaABSDescrizione.Value;
          SpostaForm.RGTipo.Itemindex:=QAgendaABSTipo.Value-1;
          if SpostaForm.RGTipo.ItemIndex=0 then
               SpostaForm.PanLuogo.Visible:=true
          else SpostaForm.PanLuogo.Visible:=False;
          SpostaForm.ShowModal;
          if SpostaForm.ModalResult=mrOK then begin
               // controllo concomitanza in SpostaForm
               Data.DB.BeginTrans;
               try
                    Data.Q1.Close;
                    if SpostaForm.MEAlleOre.Text<>'  .  ' then
                         Data.Q1.SQL.text:='update Agenda set Data=:xData,Ore=:xOre,AlleOre=:xAlleOre,Descrizione=:xDescrizione,IDRisorsa=:xIDRisorsa,Tipo=:xTipo '+
                              'where Data=:kData and Ore=:kOre and IDUtente=:xIDUtente'
                    else
                         Data.Q1.SQL.text:='update Agenda set Data=:xData,Ore=:xOre,Descrizione=:xDescrizione,IDRisorsa=:xIDRisorsa,Tipo=:xTipo '+
                              'where Data=:kData and Ore=:kOre and IDUtente=:xIDUtente';
                    Data.Q1.ParambyName('xData').asDateTime:=SpostaForm.DEData.Date;
                    Data.Q1.ParambyName('xOre').asDateTime:=StrToDateTime(DatetoStr(SpostaForm.DEData.Date)+' '+SpostaForm.MEOre.Text);
                    if SpostaForm.MEAlleOre.Text<>'  .  ' then
                         Data.Q1.ParambyName('xAlleOre').asDateTime:=StrToDateTime(DatetoStr(SpostaForm.DEData.Date)+' '+SpostaForm.MEAlleOre.Text);
                    Data.Q1.ParambyName('xDescrizione').asString:=SpostaForm.EDescriz.Text;
                    Data.Q1.ParambyName('kData').asDateTime:=QAgendaABSData.Value;
                    Data.Q1.ParambyName('kOre').asDateTime:=QAgendaABSOre.Value;
                    Data.Q1.ParambyName('xIDUtente').asInteger:=QUsersID.Value;
                    if (SpostaForm.RGTipo.ItemIndex=0)and(not SpostaForm.CBLuogo.checked) then
                         Data.Q1.ParambyName('xIDRisorsa').asInteger:=SpostaForm.TRisorseID.Value
                    else Data.Q1.ParambyName('xIDRisorsa').asInteger:=0;
                    Data.Q1.ParambyName('xTipo').asInteger:=SpostaForm.RGTipo.Itemindex+1;
                    Data.Q1.ExecSQL;

                    Data.DB.CommitTrans;
                    QAgenda.Close;
                    QAgenda.Open;
                    ComponiGrigliaRis;
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE:  operazione non completata',mtError, [mbOK],0);
               end;
          end;
          SpostaForm.Free;
          QAgendaABS.Close;
     end;
end;

procedure TAgendaSettForm.ToolbarButton974Click(Sender: TObject);
begin
     if ASG1.Cells[xSelColRis,xSelRowRis]<>'' then begin
          if Messagedlg('Sei sicuro di eliminare l''appuntamento ?',mtWarning, [mbNo,mbYes],0)=mrYes then begin
               with Data do begin
                    DB.BeginTrans;
                    try
                         Q1.Close;
                         Q1.SQL.text:='delete from Agenda where ID='+IntToStr(xArrayIDAgenda[xSelColRis,xSelRowRis]);
                         Q1.ExecSQL;
                         DB.CommitTrans;
                    except
                         DB.RollbackTrans;
                         MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
                    end;
               end;
               QAgenda.Close;
               QAgenda.Open;
               ComponiGrigliaRis;
          end;
     end;
end;

procedure TAgendaSettForm.ToolbarButton975Click(Sender: TObject);
begin
     if ASG1.Cells[xSelColRis,xSelRowRis]<>'' then begin
          QAgendaABS.Close;
          QAgendaABS.Params[0].asInteger:=xArrayIDAgenda[xSelColRis,xSelRowRis];
          QAgendaABS.Open;
          if QAgendaABSIDCandRic.asString<>'' then begin
               Q.Close;
               Q.SQL.text:='select IDRicerca from EBC_CandidatiRicerche where ID='+QAgendaABSIDCandRic.asString;
               Q.Open;
               CaricaApriRicPend(Q.FieldByName('IDRicerca').asinteger);

               SelPersForm:=TSelPersForm.create(self);
               SelPersForm.xFromOrgTL:=False;
               SelPersForm.ShowModal;
               try SelPersForm.Free
               except
               end;
               DataRicerche.QRicAttive.Close;
               Mainform.EseguiQRicAttive;
               if Data2.QRicClienti.Active then begin
                    Data2.QRicClienti.Close;
                    Data2.QRicClienti.Open;
               end;
               if Data2.QAnagRicerche.Active then begin
                    Data2.QAnagRicerche.Close;
                    Data2.QAnagRicerche.Open;
               end;
               DataRicerche.TRicerchePend.Close;
          end;
          QAgenda.Close;
          QAgenda.Open;
          ComponiGrigliaRis;
     end;
end;

procedure TAgendaSettForm.BArchiviaPromClick(Sender: TObject);
begin
     if ASG2.Cells[xSelCol2,xSelRow2]<>'' then begin
          if Messagedlg('Sei sicuro di voler ARCHIVIARE il messaggio ?',mtWarning, [mbNo,mbYes],0)=mrYes then begin
               with Data do begin
                    DB.BeginTrans;
                    try
                         Q1.Close;
                         Q1.SQL.text:='update Promemoria set DataLettura=:xDataLettura '+
                              'where ID='+IntToStr(xArrayIDProm[xSelCol2,xSelRow2]);
                         Q1.ParamByName('xDataLettura').asDateTime:=Date;
                         Q1.ExecSQL;
                         DB.CommitTrans;
                    except
                         DB.RollbackTrans;
                         MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
                    end;
               end;
               QPromemoria.Close;
               QPromemoria.Open;
               ComponiGriglia;
          end;
     end;
end;

procedure TAgendaSettForm.BArchivioPromClick(Sender: TObject);
begin
     ArchivioMessForm:=TArchivioMessForm.create(self);
     ArchivioMessForm.QMessLetti.ParamByName('xIDUtente').asInteger:=QUsersID.Value;
     ArchivioMessForm.QMessLetti.Open;
     ArchivioMessForm.ShowModal;
     ArchivioMessForm.Free;
     ComponiGriglia;
end;

procedure TAgendaSettForm.BSituazCandStampaClick(Sender: TObject);
begin
     if not MainForm.CheckProfile('40') then Exit;
     if not LogEsportazioniCand then exit;
     dxPrinter1.Preview(True,nil);
end;

function TAgendaSettForm.LogEsportazioniCand: boolean;
var xCriteri: string;
begin
     // restituisce FALSE se l'utente ha abortito l'operazione
     Result:=True;
     if MessageDlg('ATTENZIONE:  la stampa o l''esportazione di dati � un''operazione soggetta a registrazione'+chr(13)+
          'secondo la normativa sulla privacy.  SEI SICURO DI VOLER PROSEGUIRE ?',mtWarning, [mbYes,mbNo],0)=mrNo then begin
          Result:=False;
          exit;
     end;
     with Data do begin
          DB.BeginTrans;
          try
               xCriteri:='da Agenda-Situaz.cand.';
               Q1.SQL.text:='insert into LogEsportazCand (IDUtente,Data,Criteri,StringaSQL,ResultNum) '+
                    ' values (:xIDUtente,:xData,:xCriteri,:xStringaSQL,:xResultNum)';
               Q1.ParambyName('xIDUtente').asInteger:=MainForm.xIDUtenteAttuale;
               Q1.ParambyName('xData').asDateTime:=Date;
               Q1.ParambyName('xCriteri').asString:=xCriteri;
               Q1.ParambyName('xStringaSQL').asString:=QCandidati.SQL.text;
               Q1.ParambyName('xResultNum').asInteger:=QCandidati.RecordCount;
               Q1.ExecSQL;

               DB.CommitTrans;
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: operazione non effettuata',mtError, [mbOK],0);
          end;
     end;
end;

end.

