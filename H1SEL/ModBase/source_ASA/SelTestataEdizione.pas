unit SelTestataEdizione;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Grids, DBGrids, Db, DBTables;

type
  TSelTestataEdizioneForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    QTestEdiz: TQuery;
    DsQTestEdiz: TDataSource;
    DBGrid1: TDBGrid;
    QTestEdizIDTestata: TIntegerField;
    QTestEdizIDEdizione: TAutoIncField;
    QTestEdizTestata: TStringField;
    QTestEdizEdizione: TStringField;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SelTestataEdizioneForm: TSelTestataEdizioneForm;

implementation

{$R *.DFM}

procedure TSelTestataEdizioneForm.FormShow(Sender: TObject);
begin
     Caption:='[S/40] - '+Caption;
end;

end.
