unit Scheda;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, TB97, ExtCtrls, ComCtrls, Wall, Mask, DBCtrls, StdCtrls,
  DBCGrids;

type
  TSchedaForm = class(TForm)
    Wallpaper1: TWallpaper;
    Panel1: TPanel;
    Splitter1: TSplitter;
    TbBAnagCan: TToolbarButton97;
    TbBAnagOK: TToolbarButton97;
    TbBAnagDel: TToolbarButton97;
    TbBAnagNew: TToolbarButton97;
    Panel2: TPanel;
    DBGrid1: TDBGrid;
    Edit1: TEdit;
    Panel6: TPanel;
    Panel3: TPanel;
    PageControl1: TPageControl;
    TSAnagDatiAnag: TTabSheet;
    Panel4: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label11: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit14: TDBEdit;
    DBComboBox1: TDBComboBox;
    TSAnagDatiAccad: TTabSheet;
    Panel5: TPanel;
    Label10: TLabel;
    DBCtrlGrid1: TDBCtrlGrid;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    TSAnagStatoServ: TTabSheet;
    Panel7: TPanel;
    Label12: TLabel;
    Label13: TLabel;
    DBEdit5: TDBEdit;
    TSAnagRetrib: TTabSheet;
    Panel8: TPanel;
    Label14: TLabel;
    DBGrid2: TDBGrid;
    procedure TbBAnagNewClick(Sender: TObject);
    procedure TbBAnagDelClick(Sender: TObject);
    procedure TbBAnagOKClick(Sender: TObject);
    procedure TbBAnagCanClick(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SchedaForm: TSchedaForm;

implementation

uses ModuloDati;

{$R *.DFM}

procedure TSchedaForm.TbBAnagNewClick(Sender: TObject);
begin
     Data.TAnagrafica.Insert;
     PageControl1.ActivePage:=TSAnagDatiAnag;
     DBEdit14.SetFocus;
end;

procedure TSchedaForm.TbBAnagDelClick(Sender: TObject);
begin
     if MessageDlg('Sei sicuro di voler eminimarlo',mtWarning,
        [mbNo,mbYes],0)=mrYes then Data.TAnagrafica.Delete;
end;

procedure TSchedaForm.TbBAnagOKClick(Sender: TObject);
begin
     Data.TAnagrafica.Post;
end;

procedure TSchedaForm.TbBAnagCanClick(Sender: TObject);
begin
     Data.TAnagrafica.Cancel;
end;


procedure TSchedaForm.Edit1Change(Sender: TObject);
begin
     Data.TAnagrafica.FindNearest([Edit1.text]);
end;



end.
