unit AnagConoscInfo;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     DBTables,StdCtrls,DBCtrls,Mask,Db,DBCGrids,ExtCtrls,TB97;

type
     TFrmAnagConoscInfo=class(TFrame)
          Panel1: TPanel;
          Label34: TLabel;
          QAnagConoscInfo: TQuery;
          DsQAnagConoscInfo: TDataSource;
          DBCtrlGrid1: TDBCtrlGrid;
          QAnagConoscInfoID: TAutoIncField;
          QAnagConoscInfoIDAnagrafica: TIntegerField;
          QAnagConoscInfoIDConoscInfo: TIntegerField;
          QAnagConoscInfoPeriodoDal: TSmallintField;
          QAnagConoscInfoPeriodoAl: TSmallintField;
          QAnagConoscInfoLivello: TStringField;
          QAnagConoscInfoPiattaforme: TStringField;
          QAnagConoscInfoArea: TStringField;
          QAnagConoscInfoSottoArea: TStringField;
          QAnagConoscInfoProprieta: TStringField;
          QAnagConoscInfoProcedura: TStringField;
          Label1: TLabel;
          DBEdit1: TDBEdit;
          Label2: TLabel;
          DBEdit2: TDBEdit;
          Label3: TLabel;
          DBEdit3: TDBEdit;
          Label4: TLabel;
          DBEdit4: TDBEdit;
          Label5: TLabel;
          DBEdit5: TDBEdit;
          DBEdit6: TDBEdit;
          Label7: TLabel;
          Label8: TLabel;
          DBComboBox1: TDBComboBox;
          DBComboBox2: TDBComboBox;
          UpdConoscInfo: TUpdateSQL;
          BVoceNew: TToolbarButton97;
          BVoceDel: TToolbarButton97;
          BVoceMod: TToolbarButton97;
          BVoceCan: TToolbarButton97;
          BVoceOK: TToolbarButton97;
    Label6: TLabel;
    DBEdit7: TDBEdit;
    QAnagConoscInfoDescrizione: TStringField;
          procedure DsQAnagConoscInfoStateChange(Sender: TObject);
          procedure BVoceNewClick(Sender: TObject);
          procedure BVoceOKClick(Sender: TObject);
          procedure BVoceCanClick(Sender: TObject);
          procedure BVoceDelClick(Sender: TObject);
          procedure QAnagConoscInfoAfterPost(DataSet: TDataSet);
          procedure BVoceModClick(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

implementation

uses ConoscInfo,ModuloDati;

{$R *.DFM}

procedure TFrmAnagConoscInfo.DsQAnagConoscInfoStateChange(Sender: TObject);
var b: boolean;
begin
     b:=DsQAnagConoscInfo.State in [dsEdit,dsInsert];
     BVoceNew.Enabled:=not b;
     BVoceDel.Enabled:=not b;
     BVoceMod.Enabled:=not b;
     BVoceOK.Enabled:=b;
     BVoceCan.Enabled:=b;
end;

procedure TFrmAnagConoscInfo.BVoceNewClick(Sender: TObject);
begin
     // nuova voce
     ConoscInfoForm:=TConoscInfoForm.create(self);
     ConoscInfoForm.ShowModal;
     if ConoscInfoForm.ModalResult=mrOK then begin
          if (not ConoscInfoForm.QConoscInfo.IsEmpty) and (ConoscInfoForm.QConoscInfoID.Value>0) then begin
               with Data do begin
                    DB.BeginTrans;
                    try
                         Q1.SQL.text:='insert into AnagConoscInfo (IDAnagrafica,IDConoscInfo) '+
                              'values (:xIDAnagrafica,:xIDConoscInfo)';
                         Q1.ParambyName('xIDAnagrafica').asInteger:=Data.TAnagraficaID.Value;
                         Q1.ParambyName('xIDConoscInfo').asInteger:=ConoscInfoForm.QConoscInfoID.value;
                         Q1.ExecSQL;
                         DB.CommitTrans;
                    except
                         DB.RollbackTrans;
                         MessageDlg('ERRORE SUL DATABASE: operazione non effettuata',mtError, [mbOK],0);
                         raise;
                    end;
               end;
               QAnagConoscInfo.Close;
               QAnagConoscInfo.Open;
          end;
     end;
     ConoscInfoForm.Free;
end;

procedure TFrmAnagConoscInfo.BVoceOKClick(Sender: TObject);
begin
     QAnagConoscInfo.Post;
end;

procedure TFrmAnagConoscInfo.BVoceCanClick(Sender: TObject);
begin
     QAnagConoscInfo.Cancel;
end;

procedure TFrmAnagConoscInfo.BVoceDelClick(Sender: TObject);
begin
     if QAnagConoscInfo.IsEmpty then exit;
     if MessageDlg('Sei sicuro di voler eliminare la voce selezionata ?',mtWarning,
          [mbNo,mbYes],0)=mrYes then QAnagConoscInfo.Delete;
end;

procedure TFrmAnagConoscInfo.QAnagConoscInfoAfterPost(DataSet: TDataSet);
begin
     with QAnagConoscInfo do begin
          Data.DB.BeginTrans;
          try
               ApplyUpdates;
               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               raise;
          end;
          CommitUpdates;
     end;
end;

procedure TFrmAnagConoscInfo.BVoceModClick(Sender: TObject);
var xID:integer;
begin
     // modifica voce
     ConoscInfoForm:=TConoscInfoForm.create(self);
     ConoscInfoForm.QConoscInfo.Locate('ID',QAnagConoscInfoIDConoscInfo.Value, []);
     ConoscInfoForm.ShowModal;
     if ConoscInfoForm.ModalResult=mrOK then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.SQL.text:='update AnagConoscInfo set IDConoscInfo=:xIDConoscInfo '+
                         'where ID='+QAnagConoscInfoID.AsString;
                    Q1.ParambyName('xIDConoscInfo').asInteger:=ConoscInfoForm.QConoscInfoID.value;
                    Q1.ExecSQL;
                    DB.CommitTrans;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: operazione non effettuata',mtError, [mbOK],0);
                    raise;
               end;
          end;
          xID:=QAnagConoscInfoID.Value;
          QAnagConoscInfo.Close;
          QAnagConoscInfo.Open;
          QAnagConoscInfo.Locate('ID',xID,[]);
     end;
     ConoscInfoForm.Free;
end;

end.

