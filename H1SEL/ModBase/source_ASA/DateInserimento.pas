unit DateInserimento;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, DtEdit97, Buttons;

type
  TDateInserimentoForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    CBProm: TCheckBox;
    DEDataInizio: TDateEdit97;
    DEDataControllo: TDateEdit97;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DateInserimentoForm: TDateInserimentoForm;

implementation

{$R *.DFM}

procedure TDateInserimentoForm.FormShow(Sender: TObject);
begin
     Caption:='[E/3] - '+Caption;
end;

end.
