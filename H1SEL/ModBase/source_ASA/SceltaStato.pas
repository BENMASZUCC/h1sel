unit SceltaStato;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Grids, DBGrids, ComCtrls, Db, DBTables, ExtCtrls,
  DtEdit97;

type
  TSceltaStatoForm = class(TForm)
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Panel73: TPanel;
    Panel2: TPanel;
    PanAnn: TPanel;
    Label2: TLabel;
    DataPervenuto: TDateEdit97;
    Label1: TLabel;
    EProvCV: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    ECogn: TEdit;
    ENome: TEdit;
    QStati: TQuery;
    DsQStati: TDataSource;
    QStatiID: TAutoIncField;
    QStatiStato: TStringField;
    QStatiIDTipoStato: TIntegerField;
    QStatiPRman: TBooleanField;
    QStatiTipoStato: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    xIDAnnuncio:integer;
  end;

var
  SceltaStatoForm: TSceltaStatoForm;

implementation

uses ModuloDati;

{$R *.DFM}


procedure TSceltaStatoForm.FormCreate(Sender: TObject);
begin
     DataPervenuto.Date:=Date;
end;

procedure TSceltaStatoForm.FormShow(Sender: TObject);
begin
     Caption:='[M/000] - '+Caption;
end;

end.
