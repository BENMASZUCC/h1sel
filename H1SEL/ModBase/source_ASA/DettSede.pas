unit DettSede;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons;

type
  TDettSedeForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    EIndirizzo: TEdit;
    ECap: TEdit;
    EComune: TEdit;
    EProv: TEdit;
    Label5: TLabel;
    EDesc: TEdit;
    CBTipoStrada: TComboBox;
    ENumCivico: TEdit;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DettSedeForm: TDettSedeForm;

implementation

{$R *.DFM}

procedure TDettSedeForm.FormShow(Sender: TObject);
begin
     Caption:='[S/22] - '+Caption;
end;

end.
