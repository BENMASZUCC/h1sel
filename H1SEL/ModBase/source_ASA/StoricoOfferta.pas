unit StoricoOfferta;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Grids, DBGrids, Db, DBTables;

type
  TStoricoOffertaForm = class(TForm)
    QStoricoOfferta: TQuery;
    DsQStoricoOfferta: TDataSource;
    QStoricoOffertaID: TAutoIncField;
    QStoricoOffertaIDOfferta: TIntegerField;
    QStoricoOffertaStato: TStringField;
    QStoricoOffertaDallaData: TDateTimeField;
    QStoricoOffertaNote: TStringField;
    DBGrid1: TDBGrid;
    BitBtn1: TBitBtn;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  StoricoOffertaForm: TStoricoOffertaForm;

implementation

{$R *.DFM}

procedure TStoricoOffertaForm.FormShow(Sender: TObject);
begin
     Caption:='[S/42] - '+Caption;
end;

end.
