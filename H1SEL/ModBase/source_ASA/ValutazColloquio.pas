unit ValutazColloquio;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     Db,DBTables,StdCtrls,Grids,AdvGrid,Buttons,ExtCtrls,Aligrid;

type
     TValutazColloquioForm=class(TForm)
          Panel1: TPanel;
          BitBtn1: TBitBtn;
          Label1: TLabel;
          ECognome: TEdit;
          ENome: TEdit;
          QGriglia: TQuery;
          Q: TQuery;
          QGrigliaID: TAutoIncField;
          QGrigliaVoce: TStringField;
          QGrigliaDescVoce: TStringField;
          QGrigliaVal1Dic: TStringField;
          QGrigliaVal1Desc: TStringField;
          QGrigliaVal2Dic: TStringField;
          QGrigliaVal2Desc: TStringField;
          QGrigliaVal3Dic: TStringField;
          QGrigliaVal3Desc: TStringField;
          QGrigliaVal4Dic: TStringField;
          QGrigliaVal4Desc: TStringField;
          QGrigliaVal5Dic: TStringField;
          QGrigliaVal5Desc: TStringField;
          QGrigliaValore: TSmallintField;
          ASG1: TAdvStringGrid;
          Panel2: TPanel;
          Label2: TLabel;
          ETot: TEdit;
          QUpd: TQuery;
          Label3: TLabel;
          BInfoCand: TBitBtn;
          procedure BitBtn1Click(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure ASG1GetAlignment(Sender: TObject; ARow,ACol: Integer;
               var AAlignment: TAlignment);
          procedure ASG1CheckBoxClick(Sender: TObject; aCol,aRow: Integer;
               state: Boolean);
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
          procedure ASG1RightClickCell(Sender: TObject; Arow,Acol: Integer);
          procedure BInfoCandClick(Sender: TObject);
     private
          procedure ComponiGriglia;
          procedure SalvaGriglia;
          procedure CalcolaAccumulo;
     public
          xIDAnag: integer;
     end;

var
     ValutazColloquioForm: TValutazColloquioForm;

implementation

uses ModuloDati,ValutazCollDett,uUtilsVarie,SchedaCand;

{$R *.DFM}

procedure TValutazColloquioForm.BitBtn1Click(Sender: TObject);
begin
     Close
end;

procedure TValutazColloquioForm.ComponiGriglia;
var i,k: integer;
begin
     // composizione grilia
     QGriglia.Close;
     QGriglia.ParamByName('xIDAnag').asInteger:=xIDAnag;
     QGriglia.Open;
     ASG1.RowCount:=QGriglia.RecordCount+1;
     ASG1.ColCount:=6;
     // altezze righe
     for k:=1 to ASG1.RowCount-1 do
          ASG1.RowHeights[k]:=20;
     // prima riga: intestazioni
     ASG1.cells[0,0]:='voce';
     ASG1.cells[1,0]:='1';
     ASG1.cells[2,0]:='2';
     ASG1.cells[3,0]:='3';
     ASG1.cells[4,0]:='4';
     ASG1.cells[5,0]:='5';
     i:=1;
     while not QGriglia.EOF do begin
          // prima colonna: voce
          ASG1.ColWidths[0]:=150;
          ASG1.Cells[0,i]:=QGrigliaVoce.Value;
          // seconda colonna -> PRIMA VOCE
          ASG1.ColWidths[1]:=120;
          ASG1.Cells[1,i]:=QGrigliaVal1Dic.Value;
          if QGrigliaValore.Value=1 then
               ASG1.AddCheckBox(1,i,True,False)
          else ASG1.AddCheckBox(1,i,False,false);
          // terza colonna -> SECONDA VOCE
          ASG1.ColWidths[2]:=120;
          ASG1.Cells[2,i]:=QGrigliaVal2Dic.Value;
          if QGrigliaValore.Value=2 then
               ASG1.AddCheckBox(2,i,True,false)
          else ASG1.AddCheckBox(2,i,False,false);
          // Quarta colonna -> TERZA VOCE
          ASG1.ColWidths[3]:=120;
          ASG1.Cells[3,i]:=QGrigliaVal3Dic.Value;
          if QGrigliaValore.Value=3 then
               ASG1.AddCheckBox(3,i,True,false)
          else ASG1.AddCheckBox(3,i,False,false);
          // Quinta colonna -> QUARTA VOCE
          ASG1.ColWidths[4]:=120;
          ASG1.Cells[4,i]:=QGrigliaVal4Dic.Value;
          if QGrigliaValore.Value=4 then
               ASG1.AddCheckBox(4,i,True,false)
          else ASG1.AddCheckBox(4,i,False,false);
          // Sesta colonna -> QUINTA VOCE
          ASG1.ColWidths[5]:=120;
          ASG1.Cells[5,i]:=QGrigliaVal5Dic.Value;
          if QGrigliaValore.Value=5 then
               ASG1.AddCheckBox(5,i,True,false)
          else ASG1.AddCheckBox(5,i,False,false);

          QGriglia.Next;
          inc(i);
     end;
     QGriglia.Close;
end;

procedure TValutazColloquioForm.FormShow(Sender: TObject);
begin
     Caption:='[M/057] - '+Caption;
     Q.SQL.text:='select Cognome,Nome from Anagrafica where ID='+IntToStr(xIDAnag);
     Q.Open;
     ECognome.text:=Q.FieldByNAme('Cognome').asString;
     ENome.text:=Q.FieldByNAme('Nome').asString;
     Q.Close;
     ComponiGriglia;
     CalcolaAccumulo;
     ValutazCollDettForm:=TValutazCollDettForm.create(self);
end;

procedure TValutazColloquioForm.ASG1GetAlignment(Sender: TObject; ARow,
     ACol: Integer; var AAlignment: TAlignment);
begin
     if (ACol>0)and(Arow=0) then AAlignment:=taCenter;
end;

procedure TValutazColloquioForm.SalvaGriglia;
var i,k,xTot: integer;
     Vero,xTrans: boolean;
begin
     // salvataggio valori sul DB
     xtrans:=Data.DB.InTransaction;
     Q.Close;
     Q.SQL.text:='select count(*) Tot from AnagValutaz '+
          'where IDAnagrafica=:xIDAnagrafica and IDVoce=:xIDVoce';
     Q.ParamByName('xIDAnagrafica').asInteger:=xIDAnag;
     xTot:=0;
     for i:=1 to ASG1.RowCount-1 do begin
          for k:=1 to ASG1.ColCount-1 do begin
               ASG1.GetCheckBoxState(k,i,Vero);
               if Vero then begin
                    Q.Close;
                    Q.ParamByName('xIDVoce').asinteger:=i;
                    Q.Prepare;
                    Q.Open;
                    QUpd.Close;
                    xTot:=xTot+k;
                    if Q.FieldByName('Tot').asInteger>0 then begin
                         // aggiorna
                         QUpd.SQL.text:='update AnagValutaz set Valore=:xVal '+
                              'where IDAnagrafica=:xIDAnagrafica and IDVoce=:xIDVoce';
                         QUpd.ParamByName('xVal').asInteger:=k;
                         QUpd.ParamByName('xIDAnagrafica').asInteger:=xIDAnag;
                         QUpd.ParamByName('xIDVoce').asinteger:=i;
                         with Data do begin
                              if not xtrans then DB.BeginTrans;
                              try
                                   QUpd.ExecSQL;
                                   if not xtrans then DB.CommitTrans;
                              except
                                   if not xtrans then DB.RollbackTrans;
                                   MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
                              end;
                         end;
                    end else begin
                         // inserisci
                         QUpd.SQL.text:='insert into AnagValutaz (IDAnagrafica,IDVoce,Valore) '+
                              'values (:xIDAnagrafica,:xIDVoce,:xValore)';
                         QUpd.ParamByName('xIDAnagrafica').asInteger:=xIDAnag;
                         QUpd.ParamByName('xIDVoce').asinteger:=i;
                         QUpd.ParamByName('xValore').asInteger:=k;
                         with Data do begin
                              if not xtrans then DB.BeginTrans;
                              try
                                   QUpd.ExecSQL;
                                   if not xtrans then DB.CommitTrans;
                              except
                                   if not xtrans then DB.RollbackTrans;
                                   MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
                              end;
                         end;
                    end;
               end;
          end;
     end;
     // aggiornamento totale valutazione in Anagrafica
     QUpd.SQL.text:='update Anagrafica set TotValutaz=:xTotValutaz '+
          'where ID='+IntToStr(xIDAnag);
     QUpd.ParamByName('xTotValutaz').asinteger:=xTot;
     with Data do begin
          if not xtrans then DB.BeginTrans;
          try
               QUpd.ExecSQL;
               if not xtrans then DB.CommitTrans;
          except
               if not xtrans then DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
          end;
     end;
     Q.Close;
end;

procedure TValutazColloquioForm.ASG1CheckBoxClick(Sender: TObject; aCol,
     aRow: Integer; state: Boolean);
var i: integer;
begin
     if State=True then
          // solo uno pu� essere selezionato
          for i:=1 to ASG1.ColCount-1 do
               if ACol<>i then
                    ASG1.AddCheckBox(i,Arow,False,False);
     CalcolaAccumulo;
end;

procedure TValutazColloquioForm.FormClose(Sender: TObject;
     var Action: TCloseAction);
begin
     SalvaGriglia;
     ValutazCollDettForm.Free;
end;

procedure TValutazColloquioForm.CalcolaAccumulo;
var i,k,xTot: integer;
     Vero: boolean;
begin
     xTot:=0;
     for i:=1 to ASG1.RowCount-1 do
          for k:=1 to ASG1.ColCount-1 do begin
               ASG1.GetCheckBoxState(k,i,Vero);
               if Vero then xTot:=xTot+k;
          end;
     ETot.text:=IntToStr(xTot);
end;

procedure TValutazColloquioForm.ASG1RightClickCell(Sender: TObject; Arow,
     Acol: Integer);
begin
     // descrizione significato cella
     ValutazCollDettForm.Top:=ValutazColloquioForm.top+80+((Arow+1)*20);
     ValutazCollDettForm.Left:=ValutazColloquioForm.left+(ACol*120);
     Q.Close;
     Q.SQL.text:='select * from ValutazModel where ID=:xID';
     Q.ParamByName('xID').asInteger:=Arow;
     Q.Open;
     case Acol of
          0: ValutazCollDettForm.Label1.caption:=Q.Fieldbyname('DescVoce').asString;
          1: ValutazCollDettForm.Label1.caption:=Q.Fieldbyname('Val1Desc').asString;
          2: ValutazCollDettForm.Label1.caption:=Q.Fieldbyname('Val2Desc').asString;
          3: ValutazCollDettForm.Label1.caption:=Q.Fieldbyname('Val3Desc').asString;
          4: ValutazCollDettForm.Label1.caption:=Q.Fieldbyname('Val4Desc').asString;
          5: ValutazCollDettForm.Label1.caption:=Q.Fieldbyname('Val5Desc').asString;
     end;
     if ValutazCollDettForm.Label1.caption<>'' then
          ValutazCollDettForm.Show;
end;

procedure TValutazColloquioForm.BInfoCandClick(Sender: TObject);
begin
     SchedaCandForm:=TSchedaCandForm.create(self);
     if not PosizionaAnag(xIDAnag) then exit;
     SchedaCandForm.BValutaz.visible:=False;
     SchedaCandForm.ShowModal;
     SchedaCandForm.Free;
end;

end.

