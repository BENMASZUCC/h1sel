unit ClientiEBC;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  TB97, Wall, Mask, DBCtrls, StdCtrls, ComCtrls, Grids, DBGrids, ExtCtrls;

type
  TClientiEBCForm = class(TForm)
    Panel16: TPanel;
    Panel18: TPanel;
    Edit2: TEdit;
    DBGrid18: TDBGrid;
    PageControl4: TPageControl;
    TSClientiDati: TTabSheet;
    Label21: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    Wallpaper2: TWallpaper;
    TbBClientiNew: TToolbarButton97;
    TbBClientiDel: TToolbarButton97;
    TbBClientiOK: TToolbarButton97;
    TbBClientiCan: TToolbarButton97;
    procedure Edit2Change(Sender: TObject);
    procedure TbBClientiNewClick(Sender: TObject);
    procedure TbBClientiDelClick(Sender: TObject);
    procedure TbBClientiOKClick(Sender: TObject);
    procedure TbBClientiCanClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ClientiEBCForm: TClientiEBCForm;

implementation

uses ModuloDati3;

{$R *.DFM}


procedure TClientiEBCForm.Edit2Change(Sender: TObject);
begin
     DataSel_EBC.TClientiEBC.FindNearest([Edit2.Text]);
end;

procedure TClientiEBCForm.TbBClientiNewClick(Sender: TObject);
begin
     DataSel_EBC.TClientiEBC.Insert;
end;

procedure TClientiEBCForm.TbBClientiDelClick(Sender: TObject);
begin
     if MessageDlg('Sei sicuro di voler cancellare ?',mtWarning,
        [mbNo,mbYes],0)=mrYes then DataSel_EBC.TClientiEBC.delete;
end;

procedure TClientiEBCForm.TbBClientiOKClick(Sender: TObject);
begin
     DataSel_EBC.TClientiEBC.Post;
end;

procedure TClientiEBCForm.TbBClientiCanClick(Sender: TObject);
begin
     DataSel_EBC.TClientiEBC.cancel;
end;

end.
