unit ModRuolo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, RXSpin, Buttons;

type
  TModRuoloForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    CBAttuale: TCheckBox;
    SEAnniEsp: TRxSpinEdit;
    Label1: TLabel;
    CBDisponibile: TCheckBox;
    CBAspirazione: TCheckBox;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ModRuoloForm: TModRuoloForm;

implementation

{$R *.DFM}

procedure TModRuoloForm.FormShow(Sender: TObject);
begin
     Caption:='[S/29] - '+Caption;
end;

end.
