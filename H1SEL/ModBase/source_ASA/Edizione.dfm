object EdizioneForm: TEdizioneForm
  Left = 487
  Top = 95
  ActiveControl = ENomeEdizione
  BorderStyle = bsDialog
  Caption = 'Edizione'
  ClientHeight = 253
  ClientWidth = 422
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 4
    Top = 8
    Width = 70
    Height = 13
    Caption = 'Nome edizione'
  end
  object Label5: TLabel
    Left = 3
    Top = 232
    Width = 23
    Height = 13
    Caption = 'Note'
  end
  object BitBtn1: TBitBtn
    Left = 326
    Top = 3
    Width = 93
    Height = 34
    TabOrder = 2
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 326
    Top = 39
    Width = 93
    Height = 34
    Caption = 'Annulla'
    TabOrder = 3
    Kind = bkCancel
  end
  object ENomeEdizione: TEdit
    Left = 80
    Top = 4
    Width = 241
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    MaxLength = 50
    ParentFont = False
    TabOrder = 0
  end
  object ENote: TEdit
    Left = 32
    Top = 229
    Width = 387
    Height = 21
    TabOrder = 1
  end
  object DBGrid1: TDBGrid
    Left = 4
    Top = 30
    Width = 317
    Height = 195
    DataSource = DsQConcess
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'Descrizione'
        Title.Caption = 'Nome concessionario'
        Width = 174
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Comune'
        Width = 109
        Visible = True
      end>
  end
  object QConcess: TQuery
    Active = True
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select ID,Descrizione, Comune'
      'from EBC_Clienti'
      'where stato='#39'concessionario'#39
      'order by Descrizione')
    Left = 24
    Top = 88
    object QConcessID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.EBC_Clienti.ID'
    end
    object QConcessDescrizione: TStringField
      FieldName = 'Descrizione'
      Origin = 'EBCDB.EBC_Clienti.Descrizione'
      FixedChar = True
      Size = 30
    end
    object QConcessComune: TStringField
      FieldName = 'Comune'
      Origin = 'EBCDB.EBC_Clienti.Comune'
      FixedChar = True
    end
  end
  object DsQConcess: TDataSource
    DataSet = QConcess
    Left = 32
    Top = 136
  end
end
