€
 TCURRICULUMFORM 0∆Ћ  TPF0TCurriculumFormCurriculumFormLeft\Topа Width8HeightIHelpContextЌЖ CaptionCurriculum VitaeColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.Style OldCreateOrder	PositionpoScreenCenterOnClose	FormCloseOnShowFormShowPixelsPerInch`
TextHeight TLabelLabel43Leftј TopЄWidthDHeightCaptionNomeEdizione  TPageControlPageControl1Left Top+Width0Height
ActivePageTSCVCampiPersAlignalClientTabOrder OnChangePageControl1Change
OnChangingPageControl1Changing 	TTabSheetTSCVAltriDatiHelpContextЌЖ Caption
Altri dati TLabelLabel47Left)Top<Width¶ HeightCaption&Disponibilita movimento sul territorio  TLabelLabel45Left)TopЧWidth;HeightCaptionOrario ideale  TLabelLabel54Left)Top¶ WidthoHeightCaptionProvincia/e di interesseFocusControlDBEdit40  TLabelLabel58Left)TopЋ WidthGHeightCaptionInquadramentoFocusControlDBEdit42  TLabelLabel59Left)Topт Width;HeightCaptionRetribuzioneFocusControlDBEdit43  TSpeedButtonBAziendaLeftTopµWidthHeightHintVedi/seleziona Azienda cliente
Glyph.Data
z  v  BMv      v   (                                    А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 33      33wwwwww33€€€€р33?€??ч33  р33wssw733€€€€р33?у€?ч33 р р33w7sw733€€€€р33у3?уч33 €€ рр33w?уwч730ы рр€р373w77уч3∞њ∞рр3s73s7770ыыры€р7у373s370њњ∞€€р7у3s73?ч ыыы€  wу33уwwањњ∞ €рw€37w?7оыыwуs?ssоањњ € 3ww€€w€w3оо    3wwwwwws3	NumGlyphsParentShowHintShowHint	OnClickBAziendaClick  TLabelLabel52Left)TopWidth&HeightCaptionBenefitsFocusControlDBEdit31  TLabelLabel61LeftЂTopт WidthNHeightCaptionTipo retribuzione  TLabelLabel62LeftTop Width4HeightCaptionNazionalitа  TSpeedButtonSpeedButton3LeftTopю WidthHeightHint"Gestione Tabella tipi retribuzioni
Glyph.Data
:  6  BM6      6  (                                Д   €   ДДД ∆∆∆ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€                              ParentShowHintShowHint	OnClickSpeedButton3Click  	TGroupBox	GroupBox1LeftЭ Top€Width≈ Height'CaptionMezziTabOrder TLabelLabel1LeftП TopWidthHeightCaptionTipo:FocusControlDBEdit4  TDBCheckBoxDBCheckBox1LeftUTopWidth;HeightCaptionPatente	DataFieldPatente
DataSourcedsAnagAltriDatiTabOrderValueCheckedTrueValueUncheckedFalse  TDBEditDBEdit4Left™ TopWidthHeight	DataFieldTipoPatente
DataSourcedsAnagAltriDatiTabOrder  TDBCheckBoxDBCheckBox2LeftTopWidthKHeightCaption
Automunito	DataFieldDispAuto
DataSourcedsAnagAltriDatiTabOrder ValueCheckedTrueValueUncheckedFalse   	TGroupBox	GroupBox2LeftTop&Width^HeightFCaptionServizio Militare di levaTabOrder TLabelLabel2Left
TopWidthHeightCaptionStato  TLabelLabel3LeftЬ TopWidth#HeightCaptionPresso:FocusControlDBEdit7  TLabelLabel4Left	Top,Width'HeightCaptionPeriodo:FocusControlDBEdit8  TLabelLabel5Leftґ Top-Width HeightCaptionGrado:FocusControlDBEdit9  TDBEditDBEdit7Left≈ TopWidthТ Height	DataFieldServizioMilitareDove
DataSourcedsAnagAltriDatiTabOrder  TDBEditDBEdit8Left7Top*WidthzHeight	DataFieldServizioMilitareQuando
DataSourcedsAnagAltriDatiTabOrder  TDBEditDBEdit9Left№ Top*Width{Height	DataFieldServizioMilitareGrado
DataSourcedsAnagAltriDatiTabOrder  TDBComboBoxDBComboBox1Left7TopWidth^Height	DataFieldServizioMilitareStato
DataSourcedsAnagAltriDati
ItemHeightItems.Stringsassoltoda assolvereesentein svolgimento TabOrder    TPanelPanCatProtetteLeftToppWidth^Height1
BevelInnerbvRaised
BevelOuter	bvLoweredTabOrder TLabelLabel6LeftTopWidthTHeightCaptionCategorie protette  TLabelLabel7Left™ TopWidthHeightCaptionTipo  TLabelLabel8Left#TopWidth5HeightCaption% InvaliditаFocusControlDBEdit11  TDBEditDBEdit11Left*TopWidth-Height	DataFieldPercentualeInvalidita
DataSourcedsAnagAltriDatiTabOrder  TDBComboBoxDBComboBox2LeftTopWidthЭ Height	DataFieldCateogorieProtette
DataSourcedsAnagAltriDati
ItemHeightItems.Stringsiscrittonon iscritto TabOrder   TDBComboBoxDBComboBox3Left© TopWidth~Height	DataField
Invalidita
DataSourcedsAnagAltriDati
ItemHeightItems.StringsDisabileInvalido del LavoroOrfano di lavoro o di guerra TabOrder   TDBLookupComboBoxDBLookupComboBox2Left)TopKWidthь Height	DataFieldDispMovimento
DataSourcedsAnagAltriDatiTabOrder  TDBEditDBEdit38Left)Top¶Widthь Height	DataFieldOrarioIdeale
DataSourcedsAnagAltriDatiTabOrder  	TGroupBox	GroupBox5LeftTop0Width!HeightaCaptionInformazioni C.V.TabOrder TLabelLabel51Left≤ TopWidthHHeightCaptionInserito in data:Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel53LeftTopWidth;HeightCaptionProvenienza  TSpeedButton
BTabProvCVLeftФ TopWidthHeightHinttabella provenienza
Glyph.Data
z  v  BMv      v   (                                       А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 33      33wwwwww33€€€€р33?€??ч33  р33wssw733€€€€р33?у€?ч33 р р33w7sw733€€€€р33у3?уч33 €€ рр33w?уwч730ы рр€р373w77уч3∞њ∞рр3s73s7770ыыры€р7у373s370њњ∞€€р7у3s73?ч ыыы€  wу33уwwањњ∞ €рw€37w?7оыыwуs?ssоањњ € 3ww€€w€w3оо    3wwwwwws3	NumGlyphsParentShowHintShowHint	OnClickBTabProvCVClick  TLabelLDataRealeCVLeft± Top8Width2HeightCaptiondata reale:Font.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel37LeftDTop7Width`HeightCaptionData aggiornamento  TDbDateEdit97DbDateEdit971Left± TopWidthjHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder ColorCalendar.ColorValidclBlueDate      єО@DayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanClear
doCanPopup
doIsMaskeddoShowCanceldoShowToday 	DataFieldCVinseritoInData
DataSourceData.DsAnagrafica  TDBComboBox
DBCBProvCVLeftTopWidthК Height	DataFieldCVProvenienza
DataSourcedsAnagAltriDati
ItemHeightTabOrder  TDbDateEdit97DbDEDataRealeCVLeft± TopFWidthjHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderColorCalendar.ColorValidclBlueDate      єО@DayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanClear
doCanPopup
doIsMaskeddoShowCanceldoShowToday 	DataFieldCVDataReale
DataSourceData.DsAnagrafica  TDBCheckBoxDbCbNoCVLeftTopFWidth7HeightCaptionNo CV	DataFieldNoCV
DataSourcedsAnagAltriDatiTabOrderValueCheckedTrueValueUncheckedFalseVisible  TDbDateEdit97DbDateEdit973LeftDTopFWidthjHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderColorCalendar.ColorValidclBlueDate      єО@DayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanClear
doCanPopup
doIsMaskeddoShowCanceldoShowToday 	DataField	CVDataAgg
DataSourceData.DsAnagrafica   	TGroupBox	GroupBox6LeftfTop€Widthњ HeightҐ TabOrder TLabelLabel48LeftTop6WidthtHeightCaptionin Cigs da almeno 6 mesi  TLabelLabel49LeftToppWidthЩ HeightCaption con trattam.spec. di disoccupaz.  TDBCheckBoxDBCheckBox4LeftTop
WidthaHeightCaptionin mobilitа	DataFieldMobilita
DataSourcedsAnagAltriDatiTabOrder ValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox5LeftTopWidthyHeightCaptionin cassa integrazione	DataFieldCassaIntegrazione
DataSourcedsAnagAltriDatiTabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox6LeftTop~WidthaHeightCaptiondisp. Part-time	DataFieldDispPartTime
DataSourcedsAnagAltriDatiTabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox7LeftTopН WidthaHeightCaptiondisp. Interinale	DataFieldDispInterinale
DataSourcedsAnagAltriDatiTabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox11LeftTop'Widthґ HeightCaption CIGS da almeno 3 anni da imprese	DataFieldCassaIntegrStraord3
DataSourcedsAnagAltriDatiTabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox12LeftTopDWidthЬ HeightCaptionCIGS da almeno 24 mesi	DataFieldCassaIntegrStraord24
DataSourcedsAnagAltriDatiTabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox13LeftTopSWidth± HeightCaptionDisoccupato da almeno 24 mesi	DataFieldDisoccupato24
DataSourcedsAnagAltriDatiTabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox14LeftTopaWidthі HeightCaptionDisoccupato da almeno 12 mesi	DataFieldDisoccupato12
DataSourcedsAnagAltriDatiTabOrderValueCheckedTrueValueUncheckedFalse   TDBCheckBoxDBCheckBox9Left)TopјWidthД HeightCaptionAbilitazione professione	DataFieldAbilitazProfessioni
DataSourcedsAnagAltriDatiTabOrderValueCheckedTrueValueUncheckedFalse  TDBEditDBEdit40Left)Topі Widthь Height	DataFieldProvInteresse
DataSourcedsAnagAltriDatiTabOrder  TDBEditDBEdit42Left)Topў Widthь Height	DataFieldInquadramento
DataSourcedsAnagAltriDatiTabOrder  TDBEditDBEdit43Left)Top WidthВ Height	DataFieldRetribuzione
DataSourcedsAnagAltriDatiTabOrder	  	TGroupBox	GroupBox7LeftTopШWidthД Height6CaptionRiferimento annuncioTabOrder TSpeedButtonSpeedButton1LeftTopWidthwHeightHintTrova annuncioCaptionAnnunci associati
Glyph.Data
z  v  BMv      v   (                                       А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 33      33wwwwww33€€€€р33?€??ч33  р33wssw733€€€€р33?у€?ч33 р р33w7sw733€€€€р33у3?уч33 €€ рр33w?уwч730ы рр€р373w77уч3∞њ∞рр3s73s7770ыыры€р7у373s370њњ∞€€р7у3s73?ч ыыы€  wу33уwwањњ∞ €рw€37w?7оыыwуs?ssоањњ € 3ww€€w€w3оо    3wwwwwws3	NumGlyphsParentShowHintShowHint	OnClickSpeedButton1Click   TRadioGroupRGProprietaLeftМ TopШWidthxHeight6CaptionProprietа CV	ItemIndex Items.Stringsnostroazienda cliente TabOrderVisibleOnClickRGProprietaClick  TDBEditDBEdit31Left)Top%Widthь Height	DataFieldBenefits
DataSourcedsAnagAltriDatiTabOrder  TDBEditDBEdit46LeftTopWidthХ Height	DataFieldNazionalita
DataSourcedsAnagAltriDatiTabOrder   TDBLookupComboBoxDBLookupComboBox1LeftђTop WidthaHeight	DataField
TipoRetrib
DataSourcedsAnagAltriDatiTabOrder
  TDBCheckBoxDBCheckBox10Left∞TopјWidthuHeightCaptionTemporary Manager	DataFieldTemporaryMG
DataSourcedsAnagAltriDatiTabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox15Left)TopcWidthб HeightCaption%Disponibilitа a trasferte giornaliere	DataFieldDispTraferteGiorn
DataSourcedsAnagAltriDatiTabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox16Left)ToptWidthў HeightCaptionDisponibilitа al pernottamento	DataFieldDispPernottamento
DataSourcedsAnagAltriDatiTabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox17Left)TopЕWidthў HeightCaptionDisponibilitа al trasferimento	DataFieldDisptrasferimento
DataSourcedsAnagAltriDatiTabOrderValueCheckedTrueValueUncheckedFalse  фTMultiDBCheckBoxFrameMultiDBCheckBoxFrame1LeftTopІ WidthHeightЕ TabOrder сTRxCheckListBoxCLBoxWidthHeightp  сTPanel	PanTitoloWidth  сTPanelPanel1LeftHeightp    	TTabSheetTSCVTitoliStudioHelpContextќЖ Caption
Accademici TDBCtrlGridDBCtrlGrid1LeftTop!WidthHeightЪAllowInsertColCount
DataSourceData.DsTitoliStudioPanelHeightR
PanelWidth	TabOrder RowCountSelectedColorclInfoBk	ShowFocus TLabelLabel9LeftXTopWidthyHeightCaptionIstituto/Scuola/UniversitаFocusControlDBEdit6  TLabelLabel10LeftTopWidth/HeightCaption	Votazione  TLabelLabel12LeftTop)WidthNHeightCaptionSpecializzazioneFocusControlDBEdit13  TLabelLabel16Left± Top)WidthxHeightCaptionData/periodo conseguim.FocusControlDBEdit15  TLabelLabel11LeftTopWidthHeightCaptionTitolo  TLabelLabel14Left,Top)WidthwHeightCaptionAbilitazione ProfessionaleFocusControlDBEdit14  TDBEditDBEdit6LeftXTopWidth© Height	DataFieldLuogoConseguimento
DataSourceData.DsTitoliStudioTabOrder  TDBCheckBoxDBCheckBox3LeftѓTop'WidthTHeightCaptionLaurea breve	DataFieldLaureaBreve
DataSourceData.DsTitoliStudioTabOrderValueCheckedTrueValueUncheckedFalse  TDBEditDBEdit13LeftTop8WidthЂ Height	DataFieldSpecializzazione
DataSourceData.DsTitoliStudioTabOrder  TDBEditDBEdit15Left≤ Top8WidthvHeight	DataFieldDataConseguimento
DataSourceData.DsTitoliStudioTabOrder  TDBEditDBEdit14Left*Top8WidthЎ Height	DataFieldAbilitazProfessionale
DataSourceData.DsTitoliStudioTabOrder  TDBEditDBEdit44LeftxTopWidthЬ Height	DataFieldDiploma
DataSourceData.DsTitoliStudioFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder  TDBEditDBEdit10LeftTopWidthAHeight	DataField	Votazione
DataSourceData.DsTitoliStudioTabOrder  TDBEditDBEdit49LeftTopWidthqHeight	DataFieldTipoDiploma
DataSourceData.DsTitoliStudioFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontReadOnly	TabOrder    TPanelPanel9LeftTopWidthHeightTabOrder TToolbarButton97TbBTitoliNewLeftATopWidth2HeightCaptionNuovoOpaqueOnClickTbBTitoliNewClick  TToolbarButton97TbBTitoliDelLeftsTopWidth2HeightCaptionEliminaOpaqueOnClickTbBTitoliDelClick  TToolbarButton97TbBTitoliOKLeft•TopWidth2HeightCaptionOKEnabledOpaqueOnClickTbBTitoliOKClick  TToolbarButton97TbBTitoliCanLeft„TopWidth2HeightCaptionAnnullaEnabledOpaqueOnClickTbBTitoliCanClick  TToolbarButton97ToolbarButton972Leftк TopWidthOHeightCaptionDettaglio tesiOnClickToolbarButton972Click  TLabelLabel15LeftTopWidthcHeightCaptionTitoli di studioFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Heightу	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TSpeedButton
BTitoloITALeftФ TopWidth%Height
GroupIndexDown	CaptionITAFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontVisibleOnClickBTitoloITAClick  TSpeedButton
BTitoloENGLeftє TopWidth%Height
GroupIndexCaptionENGFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontVisibleOnClickBTitoloENGClick    	TTabSheetTSCVCorsiStageHelpContextѕЖ CaptionCorsi-Stage TDBCtrlGridDBCtrlGrid2LeftTop!WidthHeightАAllowInsertColCount
DataSourceData.DsCorsiExtraPanelHeight`
PanelWidth	TabOrder RowCountSelectedColorclInfoBk	ShowFocus TLabelLabel13LeftTopWidth+HeightCaption	Tipologia  TLabelLabel17Left» TopWidthHHeightCaptionOrganizzazioneFocusControlDBEdit17  TLabelLabel18LeftTop,WidthК HeightCaptionTitolo/Descrizione/ArgomentiFocusControlDBMemo1  TLabelLabel20Left\Top7Width>HeightCaptionGiorni (stage)FocusControlDBEdit19  TLabelLabel22LeftЖTopWidth#HeightCaptionDurata:  TLabelLabel21Leftµ Top7Width&HeightCaptionAziendaFocusControlDBEdit20  TLabelLabel32LeftЮTop8WidthbHeightCaptionConseguito nell'annoFocusControlDBEdit34  TDBComboBoxDBComboBox4LeftTopWidthЅ Height	DataField	Tipologia
DataSourceData.DsCorsiExtraFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold 
ItemHeightItems.StringsFormazione/SpecializzazioneMasterDottorato di Ricerca/Ph DStage 
ParentFontTabOrder   TDBEditDBEdit17Left» TopWidthє Height	DataFieldOrganizzazione
DataSourceData.DsCorsiExtraTabOrder  TDBMemoDBMemo1LeftTop;Widthђ Height 	DataField	Argomenti
DataSourceData.DsCorsiExtraTabOrder  TDBEditDBEdit18LeftЖTopWidth8Height	DataField	DurataOre
DataSourceData.DsCorsiExtraTabOrder  TDBEditDBEdit19Left\TopFWidth>Height	DataFieldDurataStageGiorni
DataSourceData.DsCorsiExtraTabOrder  TDBEditDBEdit20Leftі TopFWidth• Height	DataFieldNomeAzienda
DataSourceData.DsCorsiExtraTabOrder  TDBEditDBEdit34LeftЮTopFWidthcHeight	DataFieldConseguitoAnno
DataSourceData.DsCorsiExtraTabOrder   TPanelPanel2LeftTopWidth	HeightTabOrder TToolbarButton97TbBCorsiExtraNewLeftATopWidth2HeightCaptionNuovoOpaqueOnClickTbBCorsiExtraNewClick  TToolbarButton97TbBCorsiExtraDelLeftsTopWidth2HeightCaptionEliminaOpaqueOnClickTbBCorsiExtraDelClick  TToolbarButton97TbBCorsiExtraOKLeft•TopWidth2HeightCaptionOKEnabledOpaqueOnClickTbBCorsiExtraOKClick  TToolbarButton97TbBCorsiExtraCanLeft„TopWidth2HeightCaptionAnnullaEnabledOpaqueOnClickTbBCorsiExtraCanClick  TLabelLabel23LeftTopWidth≤ HeightCaptionCorsi o stage c/o aziendeFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Heightу	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont    	TTabSheet
TSCVLingueHelpContext–Ж CaptionLingue TDBCtrlGridDBCtrlGrid3LeftTop!WidthHeightШAllowInsertColCount
DataSourceData.DsLingueConoscPanelHeight3
PanelWidth	TabOrder RowCountSelectedColorclInfoBk	ShowFocus TLabelLabel24LeftTopWidth HeightCaptionLinguaFocusControlDBEdit16  TLabelLabel25LeftМ TopWidthGHeightCaptionLivello (parlato)  TLabelLabel26Left`TopWidth0HeightCaption	SoggiornoFocusControlDBEdit22  TLabelLabel60Leftц TopWidthCHeightCaptionLivello (scritto)  TDBEditDBEdit16LeftTopWidthГ Height	DataFieldLingua
DataSourceData.DsLingueConoscFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontReadOnly	TabOrder   TDBEditDBEdit22Left`TopWidth° Height	DataField	Soggiorno
DataSourceData.DsLingueConoscReadOnly	TabOrder  TDBEditDBEdit21LeftЛ TopWidthhHeight	DataFieldDescLivello
DataSourceData.DsLingueConoscTabOrder  TDBEditDBEdit45Leftх TopWidthiHeight	DataFieldDescLivelloScritto
DataSourceData.DsLingueConoscTabOrder   TPanelPanel1LeftTopWidthHeightTabOrder TToolbarButton97TbBLinConNewLeftcTopWidth6HeightCaptionNuovoOpaqueOnClickTbBLinConNewClick  TToolbarButton97TbBLinConDelLeftѕTopWidth6HeightCaptionEliminaOpaqueOnClickTbBLinConDelClick  TToolbarButton97TbBLinConModLeftЩTopWidth6HeightCaptionModificaOpaqueOnClickTbBLinConModClick  TLabelLabel30LeftTopWidth~HeightCaptionLingue conosciuteFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Heightу	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TSpeedButton
BLinguaITALeftTopWidth%Height
GroupIndexDown	CaptionITAFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontVisibleOnClickBLinguaITAClick  TSpeedButton
BLinguaENGLeft1TopWidth%Height
GroupIndexCaptionENGFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontVisibleOnClickBLinguaENGClick    	TTabSheet
TSCVespLavHelpContext—Ж CaptionEsperienze Lav. TDBCtrlGridDBCtrlGrid4LeftTop/WidthHeightКAllowInsertColCount
DataSourceData.DsEspLavPanelHeight≈ 
PanelWidth	TabOrder RowCountSelectedColorclInfoBk	ShowFocus TLabelLabel38LeftRTopWidth[HeightCaptionDal-Al (mese/anno)FocusControlDBEdit32Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLTipoContrattoLeftRTop6WidthEHeightCaptionTipo contratto:Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLLivelloContrLeftRTopZWidthYHeightCaptionLivello contrattualeFocusControlDBELivelloContrFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel42LeftRTopWidth;HeightCaptionRetribuzioneFocusControlDBEdit35Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel44LeftRTopТ Width)HeightCaption	MensilitaFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabel
LMensilitaLeftµTopWidth,HeightCaption
Mensilitа:Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel56LeftЏTopWidthHeightCaptionMesiFocusControlDBEdit41Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFont  TDBTextDBText1Left±TopWidth?Height	AlignmenttaRightJustify	DataFieldAttuale
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel19LeftTopђ Width]HeightCaptionCodifica pers.unica:Font.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel40Left=Topђ Width:HeightCaptionTel.azienda:  TLabelLabel39LeftМTop#WidthHeightCaption--  	TGroupBox	GroupBox3LeftTopWidthFHeightnCaptionAziendaEnabledFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TLabelLabel27LeftTopWidth8HeightCaption
Denominaz.FocusControlDBEdit23Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel28LeftTop(Width&HeightCaption	IndirizzoFocusControlDBEdit24Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel29LeftTop>Width'HeightCaptionComuneFocusControlDBEdit25Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel31Left
Top(WidthHeightCaptionProv.FocusControlDBEdit26  TLabelLabel33LeftЊ Top?Width-HeightCaption
Fatturato:FocusControlDBEdit28  TLabelLabel35LeftTopWWidthHeightCaptionN∞Dip.FocusControlDBEdit29Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel36LeftYTopVWidth"HeightCaptionSettore  TDBEditDBEdit23LeftDTopWidthь Height	DataFieldAzDenominazione
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder   TDBEditDBEdit24Left6Top$Width– Height	DataFieldAzIndirizzo
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBEditDBEdit25Left5Top;WidthЖ Height	DataFieldAzComune
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBEditDBEdit26Left&Top$WidthHeight	DataFieldAzProv
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBEditDBEdit28Leftр Top;WidthPHeight	DataField	Fatturato
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBEditDBEdit29Left5TopRWidthHeight	DataFieldNumDipendenti
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBEditDBEdit27LeftА TopRWidthј Height	DataFieldSettore
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder   TDBEditDBEdit32LefthTop Width#Height	DataFieldAnnoDal
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBEditDBEdit33Left©Top Width#Height	DataFieldAnnoAl
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBComboBoxDBCTipoContrattoLeftRTopDWidth± Height	DataFieldTipoContratto
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ItemHeightItems.Strings
StagionaleApprendistaFormazione LavoroTempo IndeterminatoTempo Determinato&Collaborazione Coordinata ContinuativaPrestatore d'opera occasionale
Consulente 
ParentFontTabOrder  TDBEditDBELivelloContrLeftRTophWidth∞ Height	DataFieldLivelloContrattuale
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBEditDBEdit35LeftRTopН WidthqHeight	DataFieldRetribNettaMensile
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder	  	TGroupBox	GroupBox4LeftToprWidthFHeight0CaptionRuolo e Area ruoloEnabledFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder TDBEditDBEdit3LeftЇ TopWidthЗ Height	DataFieldDescrizione_1
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontReadOnly	TabOrder  TDBEditDBEdit5LeftTopWidth± Height	DataFieldDescrizione
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontReadOnly	TabOrder    TDBEditDBEMensilitaLeft≈TopН WidthHeight	DataField	Mensilita
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder
  TDBEditDBEdit41LeftЏTop Width)Height	DataField
DurataMesi
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBCheckBoxDBCheckBox8LeftуTopWidthHeight	AlignmenttaLeftJustify	DataFieldAttuale
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderValueCheckedTrueValueUncheckedFalse  TDBEditDBEdit30LefthTop© Width– Height	DataFieldTitoloMansione
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBEditDBEdit12Left{Top© WidthЕ Height	DataField
AzTelefono
DataSourceData.DsEspLavReadOnly	TabOrder  TDBEditDBEdit36LeftRTop WidthHeight	DataFieldMeseDal
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBEditDBEdit37LeftФTop WidthHeight	DataFieldMeseAl
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   TPanelPanel3LeftTopWidthHeight)TabOrder TToolbarButton97TbBEspLavDelLeftё TopWidth5Height'CaptionEliminaOpaqueWordWrap	OnClickTbBEspLavDelClick  TToolbarButton97TbBEspLavModLeftTopWidthRHeight'Hintmodifica azienda e ruoloCaptionModifica azienda/settoreOpaqueWordWrap	OnClickTbBEspLavModClick  TToolbarButton97ToolbarButton9710LeftЬTopWidth2Height'Captionaltri dettagliWordWrap	OnClickToolbarButton9710Click  TLabelLabel34LeftTopWidthSHeight CaptionEsperienze lavorativeFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Heightу	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontWordWrap	  TToolbarButton97TbBEspLavNewLeftђ TopWidth2Height'CaptionNuovaOpaqueWordWrap	OnClickTbBEspLavNewClick  TToolbarButton97ToolbarButton975LefteTopWidth7Height'Hintmodifica azienda e ruoloCaptionModifica ruoloOpaqueWordWrap	OnClickToolbarButton975Click  TToolbarButton97BEspLavRetribLeftќTopWidth9Height'Hint9voci retributive di dettaglio
per questa esperienza lav.Captionvoci retributiveFont.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontParentShowHintShowHint	WordWrap	OnClickBEspLavRetribClick  TSpeedButton
BEspLavITALeft]TopWidth%Height
GroupIndexDown	CaptionITAFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontVisibleOnClickBEspLavITAClick  TSpeedButton
BEspLavENGLeftВ TopWidth%Height
GroupIndexCaptionENGFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontVisibleOnClickBEspLavENGClick   TPanelPanEspProfMaturataLeft Top∆Width(Height!AlignalBottom
BevelOuter	bvLoweredTabOrderVisible TLabelLabel41LeftTopWidth§ HeightCaption"Esperienza professionale maturata:FocusControlDBLookupComboBox3Font.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFont  TSpeedButtonBEspProfmatITALeftОTopWidth%Height
GroupIndexDown	CaptionITAFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontOnClickBEspProfmatITAClick  TSpeedButtonBEspProfmatENGLeft≥TopWidth%Height
GroupIndexCaptionENGFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontOnClickBEspProfmatENGClick  TDBLookupComboBoxDBLookupComboBox3Left∞ TopWidthў Height	DataFieldEspProfMaturata
DataSourcedsAnagAltriDatiFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder     	TTabSheetTSCVConoscInfoCaptionApplicativi conosc.
ImageIndex фTFrmAnagConoscInfoFrmAnagConoscInfo1LeftTopHeight§ сTPanelPanel1 сTToolbarButton97BVoceModOnClick   сTDBCtrlGridDBCtrlGrid1HeightxPanelHeight} сTLabelLabel3WidthJCaptionSoftware house  сTLabelLabel7WidthGCaptionLivello conosc.  сTDBEditDBEdit1ColorclWindow  сTDBEditDBEdit2ColorclWindow  сTDBEditDBEdit3ColorclWindow  сTDBEditDBEdit4ColorclWindow     	TTabSheetTSCVCampiPersCaptionCampi personalizzati
ImageIndex TPanel
PanFerrariLeft Top Width(HeightП AlignalTop
BevelOuter	bvLoweredTabOrder Visible TLabelLabel46LeftTop2WidthGHeightCaptionSe sм quando ?FocusControlDBEdit39  TLabelLabel50Leftƒ Top2WidthOHeightCaptionTipo di selezioneFocusControlDBEdit47  TLabelLabel55LeftTopaWidthЗ HeightCaptionParenti conosciuti in aziendaFocusControlDBEdit48  TDBCheckBoxDBCheckBox18LeftTopWidth)HeightCaption6Ha inviato precedenti domande presso la nostra societа	DataFieldDomandePrec
DataSourcedsAnagAltriDatiTabOrder ValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox19LeftTop Width)HeightCaption7Ha giа partecipato ad altre selezioni in questa azienda	DataFieldPartecSelezPrec
DataSourcedsAnagAltriDatiTabOrderValueCheckedTrueValueUncheckedFalse  TDBEditDBEdit39LeftTopBWidthє Height	DataFieldPartecSelezQuando
DataSourcedsAnagAltriDatiTabOrder  TDBEditDBEdit47Leftƒ TopBWidthЄ Height	DataFieldTipoSelezione
DataSourcedsAnagAltriDatiTabOrder  TDBEditDBEdit48LeftTopqWidth0Height	DataFieldParentiInAzienda
DataSourcedsAnagAltriDatiTabOrder   фTFrmSUITEXAnagraficaExtFrmSUITEXAnagraficaExtTopП Width(HeightXAlignalClientTabOrderVisible сTPanel	PanTitoloWidth(  сTPageControlPCSuitexWidth(HeightC с	TTabSheet	TabSheet1 сTPanelPanel1 с	TdxDBGrid	dxDBGrid1Filter.Criteria
        сTdxDBGridLookupColumndxDBGrid1TipoDistStoredRowIndex  сTdxDBGridLookupColumndxDBGrid1SettoreStoredRowIndex  сTdxDBGridLookupColumndxDBGrid1ClienteStoredRowIndex  сTdxDBGridDateColumndxDBGrid1DataDalStoredRowIndex  сTdxDBGridDateColumndxDBGrid1DataAlStoredRowIndex  сTdxDBGridCheckColumndxDBGrid1AttualeStoredRowIndex     с	TTabSheet	TabSheet4 сTPanelPanel5 с	TdxDBGrid	dxDBGrid2Filter.Criteria
           с	TTabSheet	TabSheet3 сTPanelPanel8 с	TdxDBGrid	dxDBGrid3Filter.Criteria
        сTdxDBGridLookupColumndxDBGrid3SettoreStoredRowIndex  сTdxDBGridLookupColumndxDBGrid3TipoDistStoredRowIndex  сTdxDBGridMaskColumndxDBGrid3NazioneStoredRowIndex  сTdxDBGridMaskColumndxDBGrid3RegioneStoredRowIndex         
TWallpaper
Wallpaper1Left Top Width0Height+AlignalTopTabOrder TToolbarButton97ToolbarButton973LeftџTopWidthPHeight CaptionEsciFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.Style 
Glyph.Data
z  v  BMv      v   (                                       А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 3     33wwwww33їїїї33w?33333їїї33sу3333їїї337?3333її3333333її3333333її3333333її3333333її3333333її333у333∞її333w3333її3333333її33?3333бїї333333оїї33w€€33     33wwwwws3	NumGlyphsOpaque
ParentFontOnClickToolbarButton973Click  TToolbarButton97ToolbarButton971LeftЛTopWidthNHeight CaptionCV ViewFont.CharsetDEFAULT_CHARSET
Font.ColorclBlueFont.Heightх	Font.NameMS Sans Serif
Font.Style 
Glyph.Data
z  v  BMv      v   (                                       А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 0       7wwwwwww€€€€€€pw?у333?ww€€€€wрw?€€€wч€w     3wwwwww€€їїї∞3??у?ч€p ї ∞?wwуw7p∞∞∞w37чs7 ~о∞∞ї∞w€€7чу7wwа оаwww7wуч0~о аа7s€swч73pа а37s7ws733pо оа3373w€7333оаа333377333оаа33337s7333оооа333€€€ч333     333wwwww	NumGlyphsOpaque
ParentFontOnClickToolbarButton971Click  TToolbarButton97ToolbarButton974Left8TopWidthPHeightCaption	File Word
Glyph.Data
z  v  BMv      v   (                                       А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 333333333333€333333<333333чwу3333<јр√33337w73333√3333sswу33< €рр√33чw3773<ј€€€37w?33swу√€€рр√ssу377 €р€€€w37?33sw€€€€ррsу3sу3770€€р€€€7?37?3?s3€€€ 33sу3s€w330€€р 3337?37w3333€33333s€s333330 3333337w333333333333333333333333333333333333	NumGlyphsOnClickToolbarButton974Click  TPanelPanel7LeftTopWidth4Height)AlignalLeft
BevelOuterbvNoneEnabledTabOrder  TDBEditDBEdit1LeftTop
WidthЛ HeightColorclYellow	DataFieldCognome
DataSourceData.DsAnagraficaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontReadOnly	TabOrder   TDBEditDBEdit2LeftЦ Top
WidthТ HeightColorclYellow	DataFieldNome
DataSourceData.DsAnagraficaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontReadOnly	TabOrder    TTableTAnagMansioni
BeforeOpenTAnagMansioniBeforeOpen
AfterCloseTAnagMansioniAfterCloseDatabaseNameEBCDB	TableNamedbo.AnagMansioniLeftTop@ TIntegerFieldTAnagMansioniIDAnagrafica	FieldNameIDAnagrafica  TIntegerFieldTAnagMansioniIDMansione	FieldName
IDMansione  TBooleanFieldTAnagMansioniDisponibile	FieldNameDisponibile  TIntegerFieldTAnagMansioniIDArea	FieldNameIDArea  
TMemoFieldTAnagMansioniNote	FieldNameNoteBlobTypeftMemo  TSmallintFieldTAnagMansioniAnniEsperienza	FieldNameAnniEsperienza  TAutoIncFieldTAnagMansioniID	FieldNameIDReadOnly	  TStringFieldTAnagMansioniMansione	FieldKindfkLookup	FieldNameMansioneLookupDataSetTMansioniLKLookupKeyFieldsIDLookupResultFieldDescrizione	KeyFields
IDMansioneSize(Lookup	   TTableTMansioniLKDatabaseNameEBCDB	TableNamedbo.MansioniLeft(Top@ TAutoIncFieldTMansioniLKID	FieldNameIDReadOnly	  TStringFieldTMansioniLKDescrizione	FieldNameDescrizioneSize(   TDataSourcedsAnagAltriDatiDataSetTAnagAltriDatiLeft© Top  TTable
TDispMovLKActive	DatabaseNameEBCDB	TableNamedbo.DispMovimentoLeft— TopA TAutoIncFieldTDispMovLKID	FieldNameID  TStringFieldTDispMovLKDescrizione	FieldNameDescrizione	FixedChar	Size(   TQueryTAnagAltriDatiCachedUpdates		AfterPostTAnagAltriDatiAfterPostDatabaseNameEBCDB
DataSourceData.DsAnagraficaRequestLive	SQL.Stringsselect * from AnagAltreInfo where IDAnagrafica=:ID UpdateObjectUpdAnagAltreInfoLeftЙ Top	ParamDataDataType	ftAutoIncNameID	ParamType	ptUnknown   TIntegerFieldTAnagAltriDatiIDAnagrafica	FieldNameIDAnagrafica  TBooleanFieldTAnagAltriDatiFiguraChiave	FieldNameFiguraChiave  TBooleanField$TAnagAltriDatiAssunzioneObbligatoria	FieldNameAssunzioneObbligatoria  TBooleanField!TAnagAltriDatiIdoneoAffiancamento	FieldNameIdoneoAffiancamento  TStringField TAnagAltriDatiCateogorieProtette	FieldNameCateogorieProtette	FixedChar	Size  TStringFieldTAnagAltriDatiInvalidita	FieldName
Invalidita	FixedChar	Size
  TIntegerField#TAnagAltriDatiPercentualeInvalidita	FieldNamePercentualeInvalidita  TBooleanFieldTAnagAltriDatiPatente	FieldNamePatente  TStringFieldTAnagAltriDatiTipoPatente	FieldNameTipoPatente	FixedChar	Size  TBooleanFieldTAnagAltriDatiDispAuto	FieldNameDispAuto  TStringField#TAnagAltriDatiServizioMilitareStato	FieldNameServizioMilitareStato	FixedChar	Size  TStringField"TAnagAltriDatiServizioMilitareDove	FieldNameServizioMilitareDove	FixedChar	Size  TStringField$TAnagAltriDatiServizioMilitareQuando	FieldNameServizioMilitareQuando	FixedChar	  TStringField#TAnagAltriDatiServizioMilitareGrado	FieldNameServizioMilitareGrado	FixedChar	Size  TStringFieldTAnagAltriDatiTipoContratto	FieldNameTipoContratto	FixedChar	Size(  TStringField#TAnagAltriDatiQualificaContrattuale	FieldNameQualificaContrattuale	FixedChar	Size(  TDateTimeFieldTAnagAltriDatiDataAssunzione	FieldNameDataAssunzione  TDateTimeFieldTAnagAltriDatiDataScadContratto	FieldNameDataScadContratto  TDateTimeFieldTAnagAltriDatiDataUscita	FieldName
DataUscita  TDateTimeFieldTAnagAltriDatiDataFineProva	FieldNameDataFineProva  TDateTimeFieldTAnagAltriDatiDataProssScatto	FieldNameDataProssScatto  TDateTimeFieldTAnagAltriDatiDataPensione	FieldNameDataPensione  TIntegerFieldTAnagAltriDatiIDStabilimento	FieldNameIDStabilimento  TStringFieldTAnagAltriDatiPosizioneIdeale	FieldNamePosizioneIdeale	FixedChar	Size  TStringFieldTAnagAltriDatiOrarioIdeale	FieldNameOrarioIdeale	FixedChar	  TIntegerFieldTAnagAltriDatiIDDispMov	FieldName	IDDispMov  TStringFieldTAnagAltriDatiDisponibContratti	FieldNameDisponibContratti	FixedChar	  TStringFieldTAnagAltriDatiCVProvenienza	FieldNameCVProvenienza	FixedChar	Size  TStringFieldTAnagAltriDatiCVFile	FieldNameCVFile	FixedChar	Size(  TStringField!TAnagAltriDatiAbilitazProfessioni	FieldNameAbilitazProfessioni	FixedChar	Size  
TMemoFieldTAnagAltriDatiNote	FieldNameNoteBlobTypeftMemo  TBooleanFieldTAnagAltriDatiMobilita	FieldNameMobilita  TBooleanFieldTAnagAltriDatiCassaIntegrazione	FieldNameCassaIntegrazione  TBooleanFieldTAnagAltriDatiDispPartTime	FieldNameDispPartTime  TBooleanFieldTAnagAltriDatiDispInterinale	FieldNameDispInterinale  TStringFieldTAnagAltriDatiProvInteresse	FieldNameProvInteresse	FixedChar	SizeP  TStringFieldTAnagAltriDatiInquadramento	FieldNameInquadramento	FixedChar	Size2  TStringFieldTAnagAltriDatiRetribuzione	FieldNameRetribuzione	FixedChar	Sized  TStringFieldTAnagAltriDatiDispMovimento	FieldKindfkLookup	FieldNameDispMovimentoLookupDataSet
TDispMovLKLookupKeyFieldsIDLookupResultFieldDescrizione	KeyFields	IDDispMovSize(Lookup	  TStringFieldTAnagAltriDatiBenefits	FieldNameBenefitsOriginEBCDB.AnagAltreInfo.Benefits	FixedChar	Sized  TStringFieldTAnagAltriDatiNazionalita	FieldNameNazionalitaOriginEBCDB.AnagAltreInfo.Nazionalita	FixedChar	Size2  TIntegerFieldTAnagAltriDatiIDTipoRetrib	FieldNameIDTipoRetribOrigin EBCDB.AnagAltreInfo.IDTipoRetrib  TStringFieldTAnagAltriDatiTipoRetrib	FieldKindfkLookup	FieldName
TipoRetribLookupDataSetQTipiRetribLKLookupKeyFieldsIDLookupResultField
TipoRetrib	KeyFieldsIDTipoRetribLookup	  TBooleanFieldTAnagAltriDatiTemporaryMG	FieldNameTemporaryMGOriginEBCDB.AnagAltreInfo.TemporaryMG  TBooleanFieldTAnagAltriDatiNoCV	FieldNameNoCVOriginEBCDB.AnagAltreInfo.NoCV  TBooleanField!TAnagAltriDatiCassaIntegrStraord3	FieldNameCassaIntegrStraord3Origin'EBCDB.AnagAltreInfo.CassaIntegrStraord3  TBooleanField"TAnagAltriDatiCassaIntegrStraord24	FieldNameCassaIntegrStraord24Origin(EBCDB.AnagAltreInfo.CassaIntegrStraord24  TBooleanFieldTAnagAltriDatiDisoccupato24	FieldNameDisoccupato24Origin!EBCDB.AnagAltreInfo.Disoccupato24  TBooleanFieldTAnagAltriDatiDisoccupato12	FieldNameDisoccupato12Origin!EBCDB.AnagAltreInfo.Disoccupato12  TBooleanFieldTAnagAltriDatiDispTraferteGiorn	FieldNameDispTraferteGiornOrigin%EBCDB.AnagAltreInfo.DispTraferteGiorn  TBooleanFieldTAnagAltriDatiDispPernottamento	FieldNameDispPernottamentoOrigin%EBCDB.AnagAltreInfo.DispPernottamento  TBooleanFieldTAnagAltriDatiDisptrasferimento	FieldNameDisptrasferimentoOrigin%EBCDB.AnagAltreInfo.Disptrasferimento  TIntegerFieldTAnagAltriDatiIDEspProfMaturata	FieldNameIDEspProfMaturata  TStringFieldTAnagAltriDatiEspProfMaturata	FieldKindfkLookup	FieldNameEspProfMaturataLookupDataSetQEspProfMatLKLookupKeyFieldsIDLookupResultFieldEspProfMaturata	KeyFieldsIDEspProfMaturataSize(Lookup	  TBooleanFieldTAnagAltriDatiDomandePrec	FieldNameDomandePrecOriginEBCDB.AnagAltreInfo.DomandePrec  TBooleanFieldTAnagAltriDatiPartecSelezPrec	FieldNamePartecSelezPrecOrigin#EBCDB.AnagAltreInfo.PartecSelezPrec  TStringFieldTAnagAltriDatiPartecSelezQuando	FieldNamePartecSelezQuandoOrigin%EBCDB.AnagAltreInfo.PartecSelezQuando	FixedChar	Size(  TStringFieldTAnagAltriDatiTipoSelezione	FieldNameTipoSelezioneOrigin!EBCDB.AnagAltreInfo.TipoSelezione	FixedChar	Size  TStringFieldTAnagAltriDatiParentiInAzienda	FieldNameParentiInAziendaOrigin$EBCDB.AnagAltreInfo.ParentiInAzienda	FixedChar	Size2   
TUpdateSQLUpdAnagAltreInfoModifySQL.Stringsupdate AnagAltreInfoset  FiguraChiave = :FiguraChiave,3  AssunzioneObbligatoria = :AssunzioneObbligatoria,-  IdoneoAffiancamento = :IdoneoAffiancamento,+  CateogorieProtette = :CateogorieProtette,  Invalidita = :Invalidita,1  PercentualeInvalidita = :PercentualeInvalidita,  Patente = :Patente,  TipoPatente = :TipoPatente,  DispAuto = :DispAuto,1  ServizioMilitareStato = :ServizioMilitareStato,/  ServizioMilitareDove = :ServizioMilitareDove,3  ServizioMilitareQuando = :ServizioMilitareQuando,1  ServizioMilitareGrado = :ServizioMilitareGrado,!  TipoContratto = :TipoContratto,1  QualificaContrattuale = :QualificaContrattuale,#  DataAssunzione = :DataAssunzione,)  DataScadContratto = :DataScadContratto,  DataUscita = :DataUscita,!  DataFineProva = :DataFineProva,%  DataProssScatto = :DataProssScatto,  DataPensione = :DataPensione,#  IDStabilimento = :IDStabilimento,%  PosizioneIdeale = :PosizioneIdeale,  OrarioIdeale = :OrarioIdeale,  IDDispMov = :IDDispMov,)  DisponibContratti = :DisponibContratti,!  CVProvenienza = :CVProvenienza,  CVFile = :CVFile,-  AbilitazProfessioni = :AbilitazProfessioni,  Note = :Note,  Mobilita = :Mobilita,)  CassaIntegrazione = :CassaIntegrazione,  DispPartTime = :DispPartTime,#  DispInterinale = :DispInterinale,!  ProvInteresse = :ProvInteresse,!  Inquadramento = :Inquadramento,  Retribuzione = :Retribuzione,  Benefits = :Benefits,  Nazionalita = :Nazionalita,  IDTipoRetrib = :IDTipoRetrib,  TemporaryMG = :TemporaryMG,  NoCV = :NoCV,-  CassaIntegrStraord3 = :CassaIntegrStraord3,/  CassaIntegrStraord24 = :CassaIntegrStraord24,!  Disoccupato24 = :Disoccupato24,!  Disoccupato12 = :Disoccupato12,)  DispTraferteGiorn = :DispTraferteGiorn,)  DispPernottamento = :DispPernottamento,)  Disptrasferimento = :Disptrasferimento,)  IDEspProfMaturata = :IDEspProfMaturata,  DomandePrec = :DomandePrec,%  PartecSelezPrec = :PartecSelezPrec,)  PartecSelezQuando = :PartecSelezQuando,!  TipoSelezione = :TipoSelezione,&  ParentiInAzienda = :ParentiInAziendawhere"  IDAnagrafica = :OLD_IDAnagrafica InsertSQL.Stringsinsert into AnagAltreInfoR  (FiguraChiave, AssunzioneObbligatoria, IdoneoAffiancamento, CateogorieProtette, ]   Invalidita, PercentualeInvalidita, Patente, TipoPatente, DispAuto, ServizioMilitareStato, H   ServizioMilitareDove, ServizioMilitareQuando, ServizioMilitareGrado, L   TipoContratto, QualificaContrattuale, DataAssunzione, DataScadContratto, M   DataUscita, DataFineProva, DataProssScatto, DataPensione, IDStabilimento, O   PosizioneIdeale, OrarioIdeale, IDDispMov, DisponibContratti, CVProvenienza, Q   CVFile, AbilitazProfessioni, Note, Mobilita, CassaIntegrazione, DispPartTime, I   DispInterinale, ProvInteresse, Inquadramento, Retribuzione, Benefits, \   Nazionalita, IDTipoRetrib, TemporaryMG, NoCV, CassaIntegrStraord3, CassaIntegrStraord24, G   Disoccupato24, Disoccupato12, DispTraferteGiorn, DispPernottamento, G   Disptrasferimento, IDEspProfMaturata, DomandePrec, PartecSelezPrec, 6   PartecSelezQuando, TipoSelezione, ParentiInAzienda)valuesV  (:FiguraChiave, :AssunzioneObbligatoria, :IdoneoAffiancamento, :CateogorieProtette, K   :Invalidita, :PercentualeInvalidita, :Patente, :TipoPatente, :DispAuto, K   :ServizioMilitareStato, :ServizioMilitareDove, :ServizioMilitareQuando, T   :ServizioMilitareGrado, :TipoContratto, :QualificaContrattuale, :DataAssunzione, U   :DataScadContratto, :DataUscita, :DataFineProva, :DataProssScatto, :DataPensione, U   :IDStabilimento, :PosizioneIdeale, :OrarioIdeale, :IDDispMov, :DisponibContratti, X   :CVProvenienza, :CVFile, :AbilitazProfessioni, :Note, :Mobilita, :CassaIntegrazione, R   :DispPartTime, :DispInterinale, :ProvInteresse, :Inquadramento, :Retribuzione, V   :Benefits, :Nazionalita, :IDTipoRetrib, :TemporaryMG, :NoCV, :CassaIntegrStraord3, N   :CassaIntegrStraord24, :Disoccupato24, :Disoccupato12, :DispTraferteGiorn, M   :DispPernottamento, :Disptrasferimento, :IDEspProfMaturata, :DomandePrec, K   :PartecSelezPrec, :PartecSelezQuando, :TipoSelezione, :ParentiInAzienda) DeleteSQL.Stringsdelete from AnagAltreInfowhere"  IDAnagrafica = :OLD_IDAnagrafica Left… Top  TQueryQTipiRetribLKActive	DatabaseNameEBCDBSQL.Strings*select ID,TipoRetrib from TipiRetribuzione LeftcTop
 TStringFieldQTipiRetribLKTipoRetrib	FieldName
TipoRetribOrigin!EBCDB.TipiRetribuzione.TipoRetrib	FixedChar	  TAutoIncFieldQTipiRetribLKID	FieldNameIDOriginEBCDB.TipiRetribuzione.ID   TQueryQEspProfMatLKActive	DatabaseNameEBCDBSQL.Stringsselect * from EspProfMaturata Leftс Top TAutoIncFieldQEspProfMatLKID	FieldNameIDOriginEBCDB.EspProfMaturata.ID  TStringFieldQEspProfMatLKEspProfMaturata	FieldNameEspProfMaturataOrigin%EBCDB.EspProfMaturata.EspProfMaturata	FixedChar	Size(  TStringField QEspProfMatLKEspProfMaturata_ENG	FieldNameEspProfMaturata_ENGOrigin)EBCDB.EspProfMaturata.EspProfMaturata_ENG	FixedChar	Size(    