unit ContattoCliFatto;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, Buttons, StdCtrls, Mask, DtEdit97, DBCtrls, ExtCtrls;

type
  TContattoCliFattoForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Panel1: TPanel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    DEData: TDateEdit97;
    MEOre: TMaskEdit;
    CBTipoContatto: TComboBox;
    EParlatoCon: TEdit;
    EDescrizione: TEdit;
    SpeedButton1: TSpeedButton;
    DBGrid1: TDBGrid;
    CBRifOfferta: TCheckBox;
    DBGrid2: TDBGrid;
    procedure SpeedButton1Click(Sender: TObject);
    procedure CBRifOffertaClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    xIDContattoCli,xIDOfferta:integer;
  end;

var
  ContattoCliFattoForm: TContattoCliFattoForm;

implementation

uses ModuloDati2;

{$R *.DFM}

procedure TContattoCliFattoForm.SpeedButton1Click(Sender: TObject);
begin
     if Data2.TContattiClienti.RecordCount>0 then begin
        EParlatoCon.text:=Data2.TContattiClientiContatto.Value;
        xIDContattoCli:=Data2.TContattiClientiID.Value;
     end;
end;

procedure TContattoCliFattoForm.CBRifOffertaClick(Sender: TObject);
begin
     if CBRifOfferta.Checked then
        DBGrid2.color:=clWindow
     else DBGrid2.color:=clBtnFace;
end;

procedure TContattoCliFattoForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     if (CBRifOfferta.Checked) and (Data2.TCliOfferte.RecordCount>0) then
        xIDOfferta:=Data2.TCliOfferteid.Value;
end;

procedure TContattoCliFattoForm.FormShow(Sender: TObject);
begin
     if xIDContattoCli>0 then
        while (Data2.TContattiClientiID.Value<>xIDContattoCli) and (not Data2.TContattiClienti.EOF) do
              Data2.TContattiClienti.Next;
     Caption:='[M/190] - '+caption;
end;

end.
