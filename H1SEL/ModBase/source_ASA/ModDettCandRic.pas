unit ModDettCandRic;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, DtEdit97, Mask, DBCtrls, ExtCtrls, Buttons;

type
  TModDettCandRicForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Panel1: TPanel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    DEDataIns: TDateEdit97;
    ESituazione: TEdit;
    EMiniVal: TEdit;
    Label4: TLabel;
    ENote: TEdit;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ModDettCandRicForm: TModDettCandRicForm;

implementation

uses SelPers, MDRicerche;

{$R *.DFM}

procedure TModDettCandRicForm.FormShow(Sender: TObject);
begin
     Caption:='[M/217] - '+Caption;
end;

end.
