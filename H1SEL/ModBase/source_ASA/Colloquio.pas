unit Colloquio;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DBCtrls, StdCtrls, ExtCtrls, Buttons, Mask, ComCtrls, TB97, Wall, Grids,
  DBGrids;

type
  TColloquioForm = class(TForm)
    Panel1: TPanel;
    PageControl1: TPageControl;
    Panel2: TPanel;
    TsDatiAnag: TTabSheet;
    Label38: TLabel;
    Label49: TLabel;
    Label50: TLabel;
    GroupBox1: TGroupBox;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    Label10: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    BitBtn12: TBitBtn;
    DBLookupComboBox1: TDBLookupComboBox;
    DBEdit17: TDBEdit;
    DBEdit23: TDBEdit;
    Panel9: TPanel;
    Label46: TLabel;
    Label47: TLabel;
    Label48: TLabel;
    DBEdit13: TDBEdit;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    TsCompetenze: TTabSheet;
    Wallpaper6: TWallpaper;
    TbBCompDipNew: TToolbarButton97;
    TbBCompDipDel: TToolbarButton97;
    TbBCompDipOK: TToolbarButton97;
    TbBCompDipCan: TToolbarButton97;
    DBGrid14: TDBGrid;
    BitBtn1: TBitBtn;
    TsCaratteristiche: TTabSheet;
    DBGrid1: TDBGrid;
    GroupBox3: TGroupBox;
    DBEdit12: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit18: TDBEdit;
    Panel4: TPanel;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Wallpaper3: TWallpaper;
    TbBAnagOK: TToolbarButton97;
    TbBAnagCan: TToolbarButton97;
    Wallpaper1: TWallpaper;
    ToolbarButton971: TToolbarButton97;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    Label2: TLabel;
    ComboBox1: TComboBox;
    Label3: TLabel;
    ComboBox2: TComboBox;
    TsTutteCar: TTabSheet;
    DBGrid19: TDBGrid;
    Wallpaper7: TWallpaper;
    TbBCarattDipNew: TToolbarButton97;
    TbBCarattDipDel: TToolbarButton97;
    TbBCarattDipOK: TToolbarButton97;
    TbBCarattDipCan: TToolbarButton97;
    Label5: TLabel;
    DBEdit2: TDBEdit;
    Label11: TLabel;
    DBEdit5: TDBEdit;
    Label12: TLabel;
    DBEdit19: TDBEdit;
    Label13: TLabel;
    DBEdit20: TDBEdit;
    Label14: TLabel;
    SpeedButton1: TSpeedButton;
    procedure BitBtn1Click(Sender: TObject);
    procedure TbBCompDipNewClick(Sender: TObject);
    procedure TbBCompDipDelClick(Sender: TObject);
    procedure TbBCompDipOKClick(Sender: TObject);
    procedure TbBCompDipCanClick(Sender: TObject);
    procedure TbBAnagOKClick(Sender: TObject);
    procedure TbBAnagCanClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure ToolbarButton971Click(Sender: TObject);
    procedure TbBCarattDipNewClick(Sender: TObject);
    procedure TbBCarattDipDelClick(Sender: TObject);
    procedure TbBCarattDipOKClick(Sender: TObject);
    procedure TbBCarattDipCanClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ColloquioForm: TColloquioForm;

implementation

uses ModuloDati3, Curriculum, ModuloDati, ModuloDati2, FissaColloquio,
  InfoRicerca;

{$R *.DFM}


procedure TColloquioForm.BitBtn1Click(Sender: TObject);
begin
     CurriculumForm.ShowModal;
end;

procedure TColloquioForm.TbBCompDipNewClick(Sender: TObject);
begin
     Data2.TCompDipendente.Insert;
end;

procedure TColloquioForm.TbBCompDipDelClick(Sender: TObject);
begin
     if MessageDlg('Sei sicuro di voler cancellare ?',mtWarning,
        [mbNo,mbYes],0)=mrYes then Data2.TCompDipendente.delete;
end;

procedure TColloquioForm.TbBCompDipOKClick(Sender: TObject);
begin
     Data2.TCompDipendente.Post;
end;

procedure TColloquioForm.TbBCompDipCanClick(Sender: TObject);
begin
     Data2.TCompDipendente.Cancel;
end;

procedure TColloquioForm.TbBAnagOKClick(Sender: TObject);
begin
     Data.TAnagrafica.Post;
end;

procedure TColloquioForm.TbBAnagCanClick(Sender: TObject);
begin
     Data.TAnagrafica.Cancel;
end;


procedure TColloquioForm.BitBtn2Click(Sender: TObject);
var xDavantia:string;
begin
     // aggiornamento colloquio
     DataSel_EBC.TColloqui2.Edit;
      DataSel_EBC.TColloqui2ValutazioneGenerale.Value:=ComboBox1.Text;
      DataSel_EBC.TColloqui2Punteggio.Value:=DataSel_EBC.QSumPuntiCarattSUMOFPunteggio.asInteger;
      DataSel_EBC.TColloqui2Esito.Value:=ComboBox2.Text;
     DataSel_EBC.TColloqui2.Post;

     if ComboBox2.Text='mantenere nella ricerca' then begin
        DataSel_EBC.TCandRicAnag.FindKey([DataSel_EBC.TRicerchePendID.Value,
                                          Data.TAnagraficaID.Value]);
        DataSel_EBC.TCandRicAnag.Edit;
        DataSel_EBC.TCandRicAnagStato.Value:='colloquio con esito positivo in data '+
                                             DateToStr(Date)+' ('+DataSel_EBC.TSelezionatoriNominativo.Value+')';
        DataSel_EBC.TCandRicAnag.Post;
     end;

     if ComboBox2.Text='eliminare dalla ricerca' then begin
        DataSel_EBC.TCandRicAnag.FindKey([DataSel_EBC.QColloquiSelIDRicerca.Value,
                                          DataSel_EBC.QColloquiSelIDAnagrafica.Value]);
        DataSel_EBC.TCandRicAnag.Delete;
     end;

     if ComboBox2.Text='altro colloquio' then begin
        FissaColloquioForm:=TFissaColloquioForm.create(self);
        FissaColloquioForm.Data.Date:=Date;
        FissaColloquioForm.ShowModal;
        xDavantia:=DataSel_EBC.TSelezionatoriNominativo.asString;
        DataSel_EBC.TColloqui.InsertRecord([DataSel_EBC.TSelezionatoriID.Value,
                                            FissaColloquioForm.Data.Date,
                                            StrToTime(FissaColloquioForm.Ora.Text)]);

        DataSel_EBC.TCandRicerca.Edit;
        DataSel_EBC.TCandRicercaStato.Value:='fissato colloquio per il giorno '+
                    DateToStr(FissaColloquioForm.Data.Date)+' alle ore '+
                    FissaColloquioForm.Ora.Text+' ('+xDavantiA+')';
        DataSel_EBC.TCandRicerca.Post;
        DataSel_EBC.TColloquiRic.Close;
        DataSel_EBC.TColloquiRic.Open;
        // aggiornamento promemoria generale
        DataSel_EBC.TPromemoria.InsertRecord([DataSel_EBC.TRicerchePendIDUtente.Value,
                                              null,
                                              Date,
                                              FissaColloquioForm.Data.Date,
                                              'colloquio con '+DataSel_EBC.TAnagCognome.asString+' alle ore '+
                                               FissaColloquioForm.Ora.Text+
                                               ' per ricerca '+DataSel_EBC.TRicerchePendProgressivo.asString+' ('+
                                               DataSel_EBC.TRicerchePendMansione.asString+')',
                                              null,False]);
        FissaColloquioForm.Free;
     end;
     DataSel_EBC.TCandRicerca.Refresh;
     DataSel_EBC.QColloquiSel.Close;
     DataSel_EBC.QColloquiSel.Open;
end;

procedure TColloquioForm.ToolbarButton971Click(Sender: TObject);
var xNew:string;
begin
     InputQuery('Modifica punteggio','Nuovo valore: ',xNew);
     DataSel_EBC.TAnagCaratt.FindKey([DataSel_EBC.QCarattCandIDAC.Value]);
     DataSel_EBC.TAnagCaratt.Edit;
      DataSel_EBC.TAnagCarattPunteggio.asInteger:=StrToInt(xNew);
     DataSel_EBC.TAnagCaratt.Post;

     DataSel_EBC.QCarattCand.Close;
     DataSel_EBC.QCarattCand.Open;
     DataSel_EBC.QSumPuntiCaratt.Close;
     DataSel_EBC.QSumPuntiCaratt.Open;
end;



procedure TColloquioForm.TbBCarattDipNewClick(Sender: TObject);
begin
     Data2.TCarattDip.Insert;
end;

procedure TColloquioForm.TbBCarattDipDelClick(Sender: TObject);
begin
     if MessageDlg('Sei sicuro di voler cancellare ?',mtWarning,
        [mbNo,mbYes],0)=mrYes then Data2.TCarattDip.delete;
end;

procedure TColloquioForm.TbBCarattDipOKClick(Sender: TObject);
begin
     Data2.TCarattDip.Post;
end;

procedure TColloquioForm.TbBCarattDipCanClick(Sender: TObject);
begin
     Data2.TCarattDip.Cancel;
end;


procedure TColloquioForm.FormShow(Sender: TObject);
begin
     ColloquioForm.Top:=20;
     ColloquioForm.Left:=50;
end;

procedure TColloquioForm.SpeedButton1Click(Sender: TObject);
begin
     InfoRicercaForm:=TInfoRicercaForm.Create(self);
     InfoRicercaForm.ShowModal;
     InfoRicercaForm.Free;
end;

end.
