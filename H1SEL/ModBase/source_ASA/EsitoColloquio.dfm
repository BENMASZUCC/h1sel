�
 TESITOCOLLOQUIOFORM 0  TPF0TEsitoColloquioFormEsitoColloquioFormLeft� Top� BorderIcons BorderStylebsDialogCaptionEsito colloquioClientHeight� ClientWidth$Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrder	PositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TLabelLabel5LeftTopeWidthGHeightCaptionData colloquio:  TLabelLabel4LeftTop5Width^HeightCaptionColloquio tenuto da:  TBitBtnBitBtn1Left� Top� WidthZHeight!Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder KindbkOK  TBitBtnBitBtn2Left� Top� WidthZHeight!CaptionAnnullaTabOrderKindbkCancel  TPanelPanel1LeftTop{Width� Height[
BevelInner	bvLoweredTabOrder TLabelLabel1LeftTopWidthHeightCaptionEsito  TLabelLabel2LeftTop.Width%HeightCaptionGiudizio  TLabelLabel3Left� Top.Width0HeightCaption	Punteggio  	TComboBoxCBEsitoLeftTopWidth� Height
ItemHeightTabOrder Textmantenere nella ricercaItems.Stringsmantenere nella ricercaaltro colloquio   	TComboBox
CBGiudizioLeftTop=Width{Height
ItemHeightTabOrderItems.StringsottimobuonodiscretosufficienteinsufficienteSOTTODIMESIONATO	ALLINEATOSOVRADIMENSIONATO   	TSpinEditSEPunteggioLeft� Top=Width1HeightMaxValuedMinValue TabOrderValue    TDBGridDBGrid1LeftTopEWidth� Height
DataSourceDsSelezionatoriOptionsdgColumnResizedgTabsdgConfirmDeletedgCancelOnExit TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldName
NominativoWidthiVisible	    TPanelPanel2Left Top Width$Height1AlignalTop
BevelOuterbvNoneEnabledTabOrder TLabelLabel6LeftTopWidth3HeightCaption
Candidato:  TEditECognLeftTopWidth� HeightColorclYellowFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder   TEditENomeLeft� TopWidth� HeightColorclYellowTabOrder   TDateEdit97DataColloquioLeftXTop`WidthhHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderColorCalendar.ColorValidclBlueDayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanPopup
doIsMaskeddoShowCanceldoShowToday   TBitBtnBitBtn3Left� Top7WidthZHeight!CaptionValutazioneTabOrderOnClickBitBtn3Click
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 33333333?�������        wwwwwwww�������?��?��� � � �w7sw7w7�������?��?��� � � �w7sw7w7�������?��?��� � � �w7sw7w7�������?��?��� � � �w7sw7w7���������������������wwwwwwww�����Ȁ�wwww�        wwwwwwww33333333333333333333333333333333	NumGlyphs  TDataSourceDsSelezionatoriDataSetTSelezionatoriLeft� Top0  TTableTSelezionatoriActive	DatabaseNameEBCDB	TableName	dbo.UsersLeftxTop0 TAutoIncFieldTSelezionatoriID	FieldNameID  TStringFieldTSelezionatoriNominativo	FieldName
Nominativo	FixedChar	Size    