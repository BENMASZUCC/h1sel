unit InsRuolo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Grids, DBGrids, Db, DBTables, StdCtrls, Buttons;

type
  TInsRuoloForm = class(TForm)
    DsAree: TDataSource;
    Panel73: TPanel;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    DsRuoli: TDataSource;
    DBGrid2: TDBGrid;
    DBGrid1: TDBGrid;
    CBInsComp: TCheckBox;
    BitBtn2: TBitBtn;
    Timer1: TTimer;
    TAree: TQuery;
    TAreeID: TAutoIncField;
    TAreeDescrizione: TStringField;
    TRuoli: TQuery;
    TRuoliID: TAutoIncField;
    TRuoliDescrizione: TStringField;
    TAreeDescrizione_ENG: TStringField;
    TRuoliDescrizione_ENG: TStringField;
    BAreeRuoliITA: TSpeedButton;
    BAreeRuoliENG: TSpeedButton;
    procedure DBGrid1KeyPress(Sender: TObject; var Key: Char);
    procedure BitBtn1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BAreeRuoliITAClick(Sender: TObject);
    procedure BAreeRuoliENGClick(Sender: TObject);
  private
    xStringa:string;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  InsRuoloForm: TInsRuoloForm;

implementation

uses ModuloDati;

{$R *.DFM}

procedure TInsRuoloForm.DBGrid1KeyPress(Sender: TObject; var Key: Char);
begin
     Timer1.Enabled:=False;
     if Key=chr(13) then begin
        BitBtn1.Click;
     end else begin
        xStringa:=xStringa+key;
        TAree.Locate('Descrizione',xStringa,[loCaseInsensitive,loPartialKey]);
     end;
     Timer1.Enabled:=true;
end;

procedure TInsRuoloForm.BitBtn1Click(Sender: TObject);
begin
     if TRuoli.RecordCount=0 then begin
        MessageDlg('Nessun ruolo selezionato !',mtError,[mbOK],0);
        ModalResult:=mrNone;
        Abort;
     end;
end;

procedure TInsRuoloForm.Timer1Timer(Sender: TObject);
begin
     xStringa:='';
end;

procedure TInsRuoloForm.FormShow(Sender: TObject);
begin
     Caption:='[S/12] - '+caption;
     if (Uppercase(copy(Data.GlobalNomeAzienda.Value,1,7))='FERRARI') or
        (Uppercase(copy(Data.GlobalNomeAzienda.Value,1,3))='EBC') then begin
        BAreeRuoliITA.visible:=True;
        BAreeRuoliENG.visible:=True;
     end;
end;

procedure TInsRuoloForm.BAreeRuoliITAClick(Sender: TObject);
begin
     DBGrid1.Columns[0].FieldName:='Descrizione';
     DBGrid2.Columns[0].FieldName:='Descrizione';
end;

procedure TInsRuoloForm.BAreeRuoliENGClick(Sender: TObject);
begin
     DBGrid1.Columns[0].FieldName:='Descrizione_ENG';
     DBGrid2.Columns[0].FieldName:='Descrizione_ENG';
end;

end.
