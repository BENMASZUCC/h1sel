�
 TINSRUOLOFORM 0�  TPF0TInsRuoloFormInsRuoloFormLeft)Top� HelpContext� ActiveControlDBGrid1BorderStylebsDialogCaptionNuovo RuoloClientHeight�ClientWidth Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TSpeedButtonBAreeRuoliITALeft� TopWidth%Height
GroupIndexDown	CaptionITAFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontVisibleOnClickBAreeRuoliITAClick  TSpeedButtonBAreeRuoliENGLeftTopWidth%Height
GroupIndexCaptionENGFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontVisibleOnClickBAreeRuoliENGClick  TPanelPanel73LeftTopWidth� Height	AlignmenttaLeftJustifyCaption  AreeColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   TPanelPanel1Left� Top.WidthHeight	AlignmenttaLeftJustifyCaption*  Ruoli disponibili per l'area selezionataColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TBitBtnBitBtn1Left:TopWidth]Height"TabOrderOnClickBitBtn1ClickKindbkOK  TDBGridDBGrid2Left� TopCWidthHeightF
DataSourceDsRuoliReadOnly	TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameDescrizioneWidth� Visible	    TDBGridDBGrid1LeftTopWidth� Heightm
DataSourceDsAreeReadOnly	TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style 
OnKeyPressDBGrid1KeyPressColumnsExpanded	FieldNameDescrizioneWidth� Visible	    	TCheckBox	CBInsCompLeftTop�Width)HeightCaption;attribuisci il ruolo e le relative competenze al nominativoChecked	Font.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontState	cbCheckedTabOrder  TBitBtnBitBtn2Left�TopWidth]Height"CaptionAnnullaTabOrderKindbkCancel  TDataSourceDsAreeDataSetTAreeLeft Top�   TDataSourceDsRuoliDataSetTRuoliLeft� Top�   TTimerTimer1OnTimerTimer1TimerLeft Top`  TQueryTAreeActive	DatabaseNameEBCDBSQL.Strings&select ID,Descrizione, Descrizione_ENG	from Areeorder by Descrizione Left Top�  TAutoIncFieldTAreeID	FieldNameIDOriginEBCDB.Aree.ID  TStringFieldTAreeDescrizione	FieldNameDescrizioneOriginEBCDB.Aree.Descrizione	FixedChar	Size  TStringFieldTAreeDescrizione_ENG	FieldNameDescrizione_ENGOriginEBCDB.Aree.Descrizione_ENG	FixedChar	Size   TQueryTRuoliActive	DatabaseNameEBCDB
DataSourceDsAreeSQL.Strings&select ID,Descrizione, Descrizione_ENGfrom Mansioniwhere IDArea=:IDorder by Descrizione Left� Top� 	ParamDataDataType	ftAutoIncNameID	ParamType	ptUnknown   TAutoIncFieldTRuoliID	FieldNameIDOriginEBCDB.Mansioni.ID  TStringFieldTRuoliDescrizione	FieldNameDescrizioneOriginEBCDB.Mansioni.Descrizione	FixedChar	Size(  TStringFieldTRuoliDescrizione_ENG	FieldNameDescrizione_ENGOriginEBCDB.Mansioni.Descrizione_ENG	FixedChar	Size    