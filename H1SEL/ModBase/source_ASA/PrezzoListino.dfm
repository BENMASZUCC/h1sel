object PrezzoListinoForm: TPrezzoListinoForm
  Left = 392
  Top = 132
  ActiveControl = GroupBox1
  BorderStyle = bsDialog
  Caption = 'Prezzo a listino per annuncio'
  ClientHeight = 207
  ClientWidth = 348
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel
    Left = 10
    Top = 102
    Width = 76
    Height = 13
    Caption = 'Prezzo di Listino'
  end
  object Label4: TLabel
    Left = 26
    Top = 126
    Width = 61
    Height = 13
    Caption = 'Prezzo a noi:'
  end
  object Label5: TLabel
    Left = 6
    Top = 149
    Width = 80
    Height = 13
    Caption = 'Prezzo al cliente:'
  end
  object Label6: TLabel
    Left = 200
    Top = 78
    Width = 19
    Height = 14
    Caption = 'Lire'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsItalic]
    ParentFont = False
  end
  object Label7: TLabel
    Left = 322
    Top = 78
    Width = 23
    Height = 14
    Caption = 'Euro'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsItalic]
    ParentFont = False
  end
  object Bevel1: TBevel
    Left = 96
    Top = 92
    Width = 122
    Height = 5
    Shape = bsTopLine
  end
  object Bevel2: TBevel
    Left = 224
    Top = 92
    Width = 122
    Height = 5
    Shape = bsTopLine
  end
  object Label8: TLabel
    Left = 3
    Top = 61
    Width = 51
    Height = 13
    Caption = 'Dalla data:'
  end
  object Label9: TLabel
    Left = 3
    Top = 170
    Width = 26
    Height = 13
    Caption = 'Note:'
  end
  object BitBtn1: TBitBtn
    Left = 254
    Top = 2
    Width = 92
    Height = 36
    TabOrder = 9
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 254
    Top = 39
    Width = 92
    Height = 36
    Caption = 'Annulla'
    TabOrder = 10
    Kind = bkCancel
  end
  object ListinoLire: TRxCalcEdit
    Left = 96
    Top = 98
    Width = 123
    Height = 21
    AutoSize = False
    DisplayFormat = '£'#39'.'#39' ,0.'
    FormatOnEditing = True
    NumGlyphs = 2
    TabOrder = 2
    OnChange = ListinoLireChange
  end
  object AnoiLire: TRxCalcEdit
    Left = 96
    Top = 122
    Width = 123
    Height = 21
    AutoSize = False
    DisplayFormat = '£'#39'.'#39' ,0.'
    FormatOnEditing = True
    NumGlyphs = 2
    TabOrder = 3
    OnChange = AnoiLireChange
  end
  object AlClienteLire: TRxCalcEdit
    Left = 96
    Top = 146
    Width = 123
    Height = 21
    AutoSize = False
    DisplayFormat = '£'#39'.'#39' ,0.'
    FormatOnEditing = True
    NumGlyphs = 2
    TabOrder = 4
    OnChange = AlClienteLireChange
  end
  object ListinoEuro: TRxCalcEdit
    Left = 223
    Top = 98
    Width = 123
    Height = 21
    AutoSize = False
    DisplayFormat = '€'#39'.'#39' ,0.00'
    FormatOnEditing = True
    NumGlyphs = 2
    TabOrder = 5
    OnChange = ListinoEuroChange
  end
  object AnoiEuro: TRxCalcEdit
    Left = 223
    Top = 122
    Width = 123
    Height = 21
    AutoSize = False
    DisplayFormat = '€'#39'.'#39' ,0.00'
    FormatOnEditing = True
    NumGlyphs = 2
    TabOrder = 6
    OnChange = AnoiEuroChange
  end
  object AlClienteEuro: TRxCalcEdit
    Left = 223
    Top = 146
    Width = 123
    Height = 21
    AutoSize = False
    DisplayFormat = '€'#39'.'#39' ,0.00'
    FormatOnEditing = True
    NumGlyphs = 2
    TabOrder = 7
    OnChange = AlClienteEuroChange
  end
  object GroupBox1: TGroupBox
    Left = 3
    Top = 4
    Width = 154
    Height = 43
    Caption = 'Quantità'
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 20
      Width = 17
      Height = 13
      Caption = 'Da:'
    end
    object Label2: TLabel
      Left = 85
      Top = 19
      Width = 10
      Height = 13
      Caption = 'A:'
    end
    object RxSpinEdit1: TRxSpinEdit
      Left = 30
      Top = 16
      Width = 47
      Height = 21
      TabOrder = 0
    end
    object RxSpinEdit2: TRxSpinEdit
      Left = 100
      Top = 16
      Width = 47
      Height = 21
      TabOrder = 1
    end
  end
  object DEDallaData: TDateEdit97
    Left = 58
    Top = 57
    Width = 99
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    ColorCalendar.ColorValid = clBlue
    DayNames.Monday = 'lu'
    DayNames.Tuesday = 'ma'
    DayNames.Wednesday = 'me'
    DayNames.Thursday = 'gi'
    DayNames.Friday = 've'
    DayNames.Saturday = 'sa'
    DayNames.Sunday = 'do'
    MonthNames.January = 'gennaio'
    MonthNames.February = 'febbraio'
    MonthNames.March = 'marzo'
    MonthNames.April = 'aprile'
    MonthNames.May = 'maggio'
    MonthNames.June = 'giugno'
    MonthNames.July = 'luglio'
    MonthNames.August = 'agosto'
    MonthNames.September = 'settembre'
    MonthNames.October = 'ottobre'
    MonthNames.November = 'novembre'
    MonthNames.December = 'dicembre'
    Options = [doButtonTabStop, doCanPopup, doIsMasked, doShowCancel, doShowToday]
  end
  object ENote: TEdit
    Left = 3
    Top = 184
    Width = 342
    Height = 21
    TabOrder = 8
  end
end
