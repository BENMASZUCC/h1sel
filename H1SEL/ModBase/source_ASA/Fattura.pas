unit Fattura;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     Grids,DBGrids,DtEdit97,DtEdDB97,StdCtrls,DBCtrls,Mask,Db,
     DBTables,ExtCtrls,TB97,Buttons,ComObj;

type
     TFatturaForm=class(TForm)
          Panel1: TPanel;
          Panel2: TPanel;
          Panel3: TPanel;
          Panel4: TPanel;
          Panel5: TPanel;
          Panel6: TPanel;
          DsFattura: TDataSource;
          DsFattDett: TDataSource;
          Label1: TLabel;
          DBEdit1: TDBEdit;
          Label2: TLabel;
          DBComboBox1: TDBComboBox;
          Label4: TLabel;
          DBEdit3: TDBEdit;
          DbDateEdit971: TDbDateEdit97;
          Label5: TLabel;
          DBComboBox3: TDBComboBox;
          Label142: TLabel;
          Label6: TLabel;
          Label7: TLabel;
          Label8: TLabel;
          DbDateEdit972: TDbDateEdit97;
          DbDateEdit973: TDbDateEdit97;
          Label9: TLabel;
          DbDateEdit974: TDbDateEdit97;
          DBGrid1: TDBGrid;
          Panel7: TPanel;
          GBRifProg: TGroupBox;
          DBEdit2: TDBEdit;
          BitBtn1: TBitBtn;
          TbBFattDettNew: TToolbarButton97;
          TbBFattDettDel: TToolbarButton97;
          TbBFattDettMod: TToolbarButton97;
          Label3: TLabel;
          DBEdit5: TDBEdit;
          DBEdit6: TDBEdit;
          BitBtn2: TBitBtn;
          TRicercheABS: TTable;
          TRicercheABSProgressivo: TStringField;
          TRicercheABSDataInizio: TDateTimeField;
          TRicercheABSStipendioLordo: TFloatField;
          TRicercheABSID: TAutoIncField;
          CBFronte: TCheckBox;
          CBNoteSpese: TCheckBox;
          DBCBAppoggio: TDBComboBox;
          QAppoggio: TQuery;
          QAppoggioAppoggioBancario1: TStringField;
          QAppoggioAppoggioBancario2: TStringField;
          QAppoggioAppoggioBancario3: TStringField;
          QNoteSpeseFatt: TQuery;
          QNoteSpeseFattID: TIntegerField;
          QNoteSpeseFattIDCliente: TIntegerField;
          QNoteSpeseFattIDRicerca: TIntegerField;
          QNoteSpeseFattIDUtente: TIntegerField;
          QNoteSpeseFattIDAnagrafica: TIntegerField;
          QNoteSpeseFattData: TDateTimeField;
          QNoteSpeseFattDescrizione: TStringField;
          QNoteSpeseFattAutomobile: TFloatField;
          QNoteSpeseFattAutostrada: TFloatField;
          QNoteSpeseFattAereoTreno: TFloatField;
          QNoteSpeseFattTaxi: TFloatField;
          QNoteSpeseFattAlbergo: TFloatField;
          QNoteSpeseFattRistoranteBar: TFloatField;
          QNoteSpeseFattParking: TFloatField;
          QNoteSpeseFattAltroDesc: TStringField;
          QNoteSpeseFattAltroImporto: TFloatField;
          QNoteSpeseFattIDFattura: TIntegerField;
          QTemp: TQuery;
          QNoteSpeseFattTotale: TFloatField;
          TFattura: TQuery;
          UpdFatture: TUpdateSQL;
          TFatturaID: TAutoIncField;
          TFatturaProgressivo: TIntegerField;
          TFatturaTipo: TStringField;
          TFatturaRifProg: TIntegerField;
          TFatturaIDCliente: TIntegerField;
          TFatturaData: TDateTimeField;
          TFatturaDecorrenza: TDateTimeField;
          TFatturaImporto: TFloatField;
          TFatturaIVA: TFloatField;
          TFatturaTotale: TFloatField;
          TFatturaModalitaPagamento: TStringField;
          TFatturaAssegno: TStringField;
          TFatturaScadenzaPagamento: TDateTimeField;
          TFatturaPagata: TBooleanField;
          TFatturaPagataInData: TDateTimeField;
          TFatturaAppoggioBancario: TStringField;
          TFatturaDescrizione: TStringField;
          TFattDett: TQuery;
          TFattDettID: TAutoIncField;
          TFattDettIDFattura: TIntegerField;
          TFattDettDescrizione: TStringField;
          TFattDettImponibile: TFloatField;
          TFattDettPercentualeIVA: TFloatField;
          TFattDettIDCompenso: TIntegerField;
          TFattDettTotale: TFloatField;
          ToolbarButton971: TToolbarButton97;
          TFatturaResponsabile: TStringField;
          Label10: TLabel;
          DBEdit4: TDBEdit;
    TFatturaFlagFuoriAppIVA63372: TBooleanField;
    DBCBFlagIVA: TDBCheckBox;
          procedure TbBFattDettNewClick(Sender: TObject);
          procedure TFattDettCalcFields(DataSet: TDataSet);
          procedure BitBtn1Click(Sender: TObject);
          procedure TbBFattDettDelClick(Sender: TObject);
          procedure BitBtn2Click(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure DBComboBox3Change(Sender: TObject);
          procedure QNoteSpeseFattCalcFields(DataSet: TDataSet);
          procedure TbBFattDettModClick(Sender: TObject);
          procedure ToolbarButton971Click(Sender: TObject);
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
     private
          { Private declarations }
     public
          xUltimoInserito: integer;
     end;

var
     FatturaForm: TFatturaForm;

implementation

uses InsDettFatt,ModuloDati,ModuloDati2,ElencoRicCliente,ModFatt,
     uUtilsVarie;

{$R *.DFM}

procedure TFatturaForm.TbBFattDettNewClick(Sender: TObject);
begin
     if DsFattura.State in [dsInsert,dsEdit] then begin
          with TFattura do begin
               Data.DB.BeginTrans;
               try
                    ApplyUpdates;
                    Data.DB.CommitTrans;
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               end;
               CommitUpdates;
          end;
     end;
     InsDettFattForm:=TInsDettFattForm.create(self);
     InsDettFattForm.ShowModal;
     InsDettFattForm.Free;
     TFattDett.Close;
     TFattDett.Open;
end;

procedure TFatturaForm.TFattDettCalcFields(DataSet: TDataSet);
begin
     TFattDettTotale.Value:=TFattDettImponibile.Value+(TFattDettImponibile.Value*TFattDettPercentualeIVA.Value/100);
end;

procedure TFatturaForm.BitBtn1Click(Sender: TObject);
begin
     if DsFattura.State in [dsInsert,dsEdit] then begin
          with TFattura do begin
               Data.DB.BeginTrans;
               try
                    ApplyUpdates;
                    Data.DB.CommitTrans;
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               end;
               CommitUpdates;
          end;
     end;
end;

procedure TFatturaForm.TbBFattDettDelClick(Sender: TObject);
var tImporto,tTotal: double;
     xIDFattura: integer;
begin
     if TFattDett.RecordCount>0 then
          if MessageDlg('Sei sicuro di voler cancellare la riga ?',mtWarning,
               [mbNo,mbYes],0)=mrYes then begin
               with Data do begin
                    DB.BeginTrans;
                    try
                         Q1.Close;
                         Q1.SQL.text:='delete FattDett where ID='+TFattDettID.AsString;
                         Q1.ExecSQL;
                         // ricalcolo imponibile fattura
                         TFattDett.DisableControls;
                         TFattDett.Close;
                         TFattDett.Open;
                         tImporto:=0;
                         tTotal:=0;
                         while not TFattDett.EOF do begin
                              tImporto:=tImporto+TFattDettImponibile.Value;
                              tTotal:=tTotal+TFattDettTotale.Value;
                              TFattDett.Next;
                         end;
                         Q1.SQL.text:='update Fatture set Importo=:xImporto,Totale=:xTotale '+
                              'where ID='+TFatturaID.asString;
                         Q1.ParamByName('xImporto').asFloat:=tImporto;
                         Q1.ParamByName('xTotale').asFloat:=tTotal;
                         Q1.ExecSQL;
                         TFattDett.EnableControls;
                         DB.CommitTrans;
                         // refresh fattura e dettaglio
                         TFattDett.Close;
                         xIDfattura:=TFatturaID.Value;
                         TFattura.Close;
                         TFattura.ParamByName('xID').asInteger:=xIDfattura;
                         TFattura.open;
                         TFattDett.Open;
                    except
                         DB.RollbackTrans;
                         MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
                    end;
               end;
          end;
end;

procedure TFatturaForm.BitBtn2Click(Sender: TObject);
var xFileWord,xFattNum,xErrore: string;
     MSWord: Variant;
     xApriWord,xNuovoFile,xApriModello: boolean;
     x: integer;
     xTotIva: Real;
begin
     QNoteSpeseFatt.Open;
     if (QNoteSpeseFatt.RecordCount>0)and(CBNoteSpese.Checked=False) then
          if MessageDlg('ATTENZIONE: risultano note spese collegate a questa fattura ma non � stato richiesto l''allegato:'+chr(13)+
               'si vuole stampare la fattura con l''allegato (Yes) o senza (No) ?',mtWarning, [mbYes,mbNo],0)=mrYes then
               CBNoteSpese.Checked:=true;
     QNoteSpeseFatt.Close;
     xErrore:='';
     if Data2.TEBCclientiIndirizzoLegale.Value='' then xErrore:=chr(13)+'Indirizzo legale mancante';
     if Data2.TEBCclientiComuneLegale.Value='' then xErrore:=xErrore+chr(13)+'Comune (legale) mancante';
     if Data2.TEBCclientiProvinciaLegale.Value='' then xErrore:=xErrore+chr(13)+'Provincia (legale) mancante';
     if Data2.TEBCclientiPartitaIVA.Value='' then xErrore:=xErrore+chr(13)+'Partita IVA mancante';
     if (Data2.TEBCclientiIndirizzoLegale.Value='')or
          (Data2.TEBCclientiComuneLegale.Value='')or
          (Data2.TEBCclientiProvinciaLegale.Value='')or
          (Data2.TEBCclientiPartitaIVA.Value='') then begin
          MessageDlg('Dati sull''azienda INCOMPLETI:'+xErrore+chr(13)+'IMPOSSIBILE PROSEGUIRE',mtError, [mbOK],0);
          Exit;
     end else begin
          xFattNum:=TFatturaProgressivo.asString;
          xFileWord:=GetDocPath+'\Fatt'+xFattNum+'.doc';
          xApriModello:=True;
          if FileExists(xFileWord) then
               if MessageDlg('Il file esiste gi� - Aprirlo (Yes) o crearne uno nuovo, riscrivendo il vecchio (No) ?',mtWarning, [mbYes,mbNo],0)=mrYes then
                    xApriModello:=False;

          if (xApriModello)and(not(FileExists(GetDocPath+'\ModFattura.doc'))) then begin
               MessageDlg('File ModFattura.doc non trovato'+chr(13)+'controllare la directory:'+chr(13)+GetDocPath,mtError, [mbOK],0);
               exit;
          end;

          try
               MsWord:=CreateOleObject('Word.Basic');
          except
               ShowMessage('Non riesco ad aprire Microsoft Word.');
               Exit;
          end;

          MsWord.AppShow;
          if xApriModello then begin
               if not CBNoteSpese.Checked then
                    MsWord.FileOpen(GetDocPath+'\ModFattura.doc')
               else MsWord.FileOpen(GetDocPath+'\ModFatturaNS.doc');

               if CBFronte.checked then begin
                    // ATTENZIONE: il file deve avere la copertina
                    MsWord.EditFind('#Data');
                    MsWord.Insert(DateToStr(Date));
                    MsWord.EditFind('#NomeAzienda');
                    MsWord.Insert(Data2.TEBCclientiDescrizione.Value);
                    MsWord.EditFind('#IndirizzoAzienda');
                    MsWord.Insert(Data2.TEBCclientiIndirizzoLegale.Value);
                    MsWord.EditFind('#CapComuneProvAzienda');
                    MsWord.Insert(Data2.TEBCclientiCapLegale.Value+' '+Data2.TEBCclientiComuneLegale.Value+' ('+Data2.TEBCclientiProvinciaLegale.Value+')');
               end;

               // riempimento campi
               MsWord.EditFind('#Tipo');
               MsWord.Insert(TFatturaTipo.Value);
               MsWord.EditFind('#Numero');
               MsWord.Insert(TFatturaProgressivo.asString);
               MsWord.EditFind('#DataFattura');
               MsWord.Insert(TFatturaData.asString);
               MsWord.EditFind('#NomeAzienda');
               MsWord.Insert(Data2.TEBCclientiDescrizione.Value);
               MsWord.EditFind('#Responsabile');
               if TFatturaResponsabile.Value<>'' then
                    MsWord.Insert('c.a. '+TFatturaResponsabile.asString)
               else MsWord.Insert('');
               MsWord.EditFind('#IndirizzoAzienda');
               MsWord.Insert(Data2.TEBCClientiTipoStradaLegale.Value+' '+Data2.TEBCclientiIndirizzoLegale.Value+' '+Data2.TEBCClientiNumCivicoLegale.Value);
               MsWord.EditFind('#CapComuneProvAzienda');
               MsWord.Insert(Data2.TEBCclientiCapLegale.Value+' '+Data2.TEBCclientiComuneLegale.Value+' ('+Data2.TEBCclientiProvinciaLegale.Value+')');
               MsWord.EditFind('#PartitaIVA');
               MsWord.Insert(Data2.TEBCclientiPartitaIVA.Value);
               {     if TFatturaIDRicerca.asString<>'' then begin
                       TRicercheABS.Open;
                       TRicercheABS.FindKey([TFatturaIDRicerca.Value]);
                       MsWord.EditFind('#Ordine');
                       MsWord.Insert(TRicercheABSProgressivo.Value+' del '+TRicercheABSDataInizio.asString);
                       TRicercheABS.Close;
                    end else begin}
               MsWord.EditFind('#Ordine');
               if TFatturaRifProg.asString<>'' then
                    MsWord.Insert(TFatturaRifProg.asString)
               else MsWord.Insert('---');
               //     end;

                    //riempimento colonne dettagli
               MsWord.EditFind('#dett');
               TFattDett.First;
               x:=0;
               xTotIva:=0;
               while x<10 do begin
                    // descrizione
                    if not TFattDett.EOF then
                         MsWord.Insert(TFattDettDescrizione.asString);
                    MsWord.NextCell;
                    // Imponibile (ORA IN EURO)
                    if not TFattDett.EOF then
                         MsWord.Insert(FormatFloat('###,###,###.##',TFattDettImponibile.Value));
                    MsWord.NextCell;
                    // Imponibile (calcolato in LIRE)
                    if not TFattDett.EOF then
                         MsWord.Insert(FormatFloat('###,###,###',TFattDettImponibile.Value*1936.27));
                    MsWord.NextCell;
                    // IVA
                    if not TFattDett.EOF then
                         MsWord.Insert(TFattDettPercentualeIVA.asString+' %');
                    xTotIVA:=xTotIVA+(TFattDettImponibile.Value*TFattDettPercentualeIVA.Value/100);
                    MsWord.NextCell;
                    // Totale (ORA IN EURO)
                    if not TFattDett.EOF then
                         MsWord.Insert(FormatFloat('###,###,###.##',TFattDettTotale.Value));
                    MsWord.NextCell;
                    // Totale (calcolato in LIRE)
                    if not TFattDett.EOF then
                         MsWord.Insert(FormatFloat('###,###,###',TFattDettTotale.Value*1936.27));

                    TFattDett.Next;
                    MsWord.NextCell;
                    x:=x+1;
               end;

               // TOTALI
               MsWord.EditFind('#TotImponibile');
               MsWord.Insert(FormatFloat('###,###,###.##',TFatturaImporto.Value));
               MsWord.NextCell;
               MsWord.Insert(FormatFloat('###,###,###',TFatturaImporto.Value*1936.27));
               MsWord.EditFind('#TotIva');
               MsWord.Insert(FormatFloat('###,###,###.##',TFatturaTotale.Value-TFatturaImporto.Value));
               MsWord.NextCell;
               MsWord.Insert(FormatFloat('###,###,###',(TFatturaTotale.Value-TFatturaImporto.Value)*1936.27));
               MsWord.EditFind('#TotFattura');
               MsWord.Insert(FormatFloat('###,###,###.##',TFatturaTotale.Value));
               MsWord.NextCell;
               MsWord.Insert(FormatFloat('###,###,###',TFatturaTotale.Value*1936.27));

               MsWord.EditFind('#ModalitaPagamento');
               MsWord.Insert(TFatturaModalitaPagamento.asString);
               if Uppercase(copy(Data.GlobalNomeAzienda.Value,1,6))<>'PROPOS' then begin
                    MsWord.EditFind('#Decorrenza');
                    MsWord.Insert(TFatturaDecorrenza.asString);
                    MsWord.EditFind('#Scadenza');
                    MsWord.Insert(TFatturaScadenzaPagamento.asString);
               end;
               MsWord.EditFind('#AppoggioBancario');
               if TFatturaAppoggioBAncario.asString<>'' then
                    MsWord.Insert(TFatturaAppoggioBAncario.asString)
               else MsWord.Insert('---');

               if TFatturaFlagFuoriAppIVA63372.Value then begin
                  MsWord.Insert(chr(13));
                  MsWord.Insert(chr(13));
                  MsWord.Insert(DBCBFlagIVA.Caption);
               end;

               // allegato note spese
               if CBNoteSpese.Checked then begin
                    MsWord.EditFind('#NumFattura');
                    MsWord.Insert(TFatturaProgressivo.asString);
                    MsWord.EditFind('#DataFattura');
                    MsWord.Insert(TFatturaData.asString);
                    MsWord.EditFind('#dett');
                    QNoteSpeseFatt.Open;
                    if QNoteSpeseFatt.RecordCount>0 then begin
                         while not QNoteSpeseFatt.EOF do begin
                              QTemp.Close;
                              if QNoteSpeseFattIDUtente.value<>0 then begin
                                   QTemp.SQL.text:='select Nominativo from Users where ID='+QNoteSpeseFattIDUtente.asString;
                                   QTemp.Open;
                                   MsWord.Insert(QNoteSpeseFattDescrizione.Value+' di '+QTemp.FieldByName('Nominativo').asString);
                              end else begin
                                   QTemp.SQL.text:='select Cognome,Nome from Anagrafica where ID='+QNoteSpeseFattIDAnagrafica.asString;
                                   QTemp.Open;
                                   MsWord.Insert(QNoteSpeseFattDescrizione.Value+' di '+QTemp.FieldByName('Cognome').asString+' '+QTemp.FieldByName('Nome').asString);
                              end;
                              MsWord.NextCell;
                              MsWord.Insert(FormatFloat('###,###,###.##',QNoteSpeseFattAutomobile.Value));
                              MsWord.NextCell;
                              MsWord.Insert(FormatFloat('###,###,###.##',QNoteSpeseFattAutostrada.Value));
                              MsWord.NextCell;
                              MsWord.Insert(FormatFloat('###,###,###.##',QNoteSpeseFattAereoTreno.Value));
                              MsWord.NextCell;
                              MsWord.Insert(FormatFloat('###,###,###.##',QNoteSpeseFattTaxi.Value));
                              MsWord.NextCell;
                              MsWord.Insert(FormatFloat('###,###,###.##',QNoteSpeseFattAlbergo.Value));
                              MsWord.NextCell;
                              MsWord.Insert(FormatFloat('###,###,###.##',QNoteSpeseFattRistoranteBar.Value));
                              MsWord.NextCell;
                              MsWord.Insert(FormatFloat('###,###,###.##',QNoteSpeseFattParking.Value));
                              MsWord.NextCell;
                              MsWord.Insert(QNoteSpeseFattAltroDesc.value+'  ');
                              if QNoteSpeseFattAltroDesc.value<>'' then
                                   MsWord.Insert(FormatFloat('###,###,###.##',QNoteSpeseFattAltroImporto.Value));
                              MsWord.NextCell;
                              MsWord.Insert(FormatFloat('###,###,###.##',QNoteSpeseFattTotale.Value));

                              QNoteSpeseFatt.Next;
                              if not QNoteSpeseFatt.EOF then
                                   MsWord.NextCell;
                         end;
                    end;
                    QNoteSpeseFatt.Close;
               end;

               MsWord.FileSaveAs(xFileWord);
          end else MsWord.FileOpen(xFileWord);
     end;
end;

procedure TFatturaForm.FormShow(Sender: TObject);
begin
     xUltimoInserito:=0;
     if Data2.DsEBCclienti.state=dsEdit then Data2.TEBCclienti.Post;
     Caption:='[M/130] - '+caption;
end;

procedure TFatturaForm.DBComboBox3Change(Sender: TObject);
begin
     if copy(Data2.TEBCclientiSistemaPagamento.Value,1,3)='Rim' then begin
          // riempo la combo con le nostre banche da Global
          QAppoggio.Open;
          DBCBAppoggio.Items.Clear;
          DBCBAppoggio.Items.Add(QAppoggioAppoggioBancario1.Value);
          if QAppoggioAppoggioBancario2.Value<>'' then
               DBCBAppoggio.Items.Add(QAppoggioAppoggioBancario2.Value);
          if QAppoggioAppoggioBancario3.Value<>'' then
               DBCBAppoggio.Items.Add(QAppoggioAppoggioBancario3.Value);
          QAppoggio.Close;
     end else TFatturaAppoggioBancario.value:=Data2.TEBCclientiBancaAppoggio.Value;
end;

procedure TFatturaForm.QNoteSpeseFattCalcFields(DataSet: TDataSet);
begin
     QNoteSpeseFattTotale.Value:=QNoteSpeseFattAutomobile.Value+
          QNoteSpeseFattAutostrada.Value+
          QNoteSpeseFattAereoTreno.Value+
          QNoteSpeseFattTaxi.Value+
          QNoteSpeseFattAlbergo.Value+
          QNoteSpeseFattRistoranteBar.Value+
          QNoteSpeseFattParking.Value+
          QNoteSpeseFattAltroImporto.Value;
end;

procedure TFatturaForm.TbBFattDettModClick(Sender: TObject);
var tImporto,tTotal: double;
     xIDFattura: integer;
begin
     ModFattForm:=TModFattForm.create(self);
     ModFattForm.EDescrizione.text:=TFattDettDescrizione.Value;
     ModFattForm.RxImponibile.Value:=TFattDettImponibile.Value;
     ModFattForm.SEPercIVA.Value:=TFattDettPercentualeIVA.AsInteger;
     ModFattForm.ShowModal;
     if ModFattForm.ModalResult=mrOK then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text:='update FattDett set Descrizione=:xDescrizione,Imponibile=:xImponibile,PercentualeIVA=:xPercentualeIVA '+
                         'where ID='+TFattDettID.asString;
                    Q1.ParamByName('xDescrizione').asString:=ModFattForm.EDescrizione.text;
                    Q1.ParamByName('xImponibile').asFloat:=ModFattForm.RxImponibile.Value;
                    Q1.ParamByName('xPercentualeIVA').asInteger:=ModFattForm.SEPercIVA.Value;
                    Q1.ExecSQL;
                    // ricalcolo imponibile fattura
                    TFattDett.DisableControls;
                    TFattDett.Close;
                    TFattDett.Open;
                    tImporto:=0;
                    tTotal:=0;
                    while not TFattDett.EOF do begin
                         tImporto:=tImporto+TFattDettImponibile.Value;
                         tTotal:=tTotal+TFattDettTotale.Value;
                         TFattDett.Next;
                    end;
                    Q1.SQL.text:='update Fatture set Importo=:xImporto,Totale=:xTotale '+
                         'where ID='+TFatturaID.asString;
                    Q1.ParamByName('xImporto').asFloat:=tImporto;
                    Q1.ParamByName('xTotale').asFloat:=tTotal;
                    Q1.ExecSQL;
                    TFattDett.EnableControls;

                    DB.CommitTrans;
                    // refresh fattura e dettaglio
                    TFattDett.Close;
                    xIDfattura:=TFatturaID.Value;
                    TFattura.Close;
                    TFattura.ParamByName('xID').asInteger:=xIDfattura;
                    TFattura.open;
                    TFattDett.Open;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               end;
          end;

     end;
     ModFattForm.Free;
end;

procedure TFatturaForm.ToolbarButton971Click(Sender: TObject);
var tImporto,tTotal: double;
     xIDfattura: integer;
begin
     ModFattForm:=TModFattForm.create(self);
     ModFattForm.Caption:='Inserimento nuovo dettaglio fattura';
     ModFattForm.ShowModal;
     if ModFattForm.ModalResult=mrOK then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text:='insert into FattDett (IDFattura,Descrizione,Imponibile,PercentualeIVA) '+
                         'values (:xIDFattura,:xDescrizione,:xImponibile,:xPercentualeIVA)';
                    Q1.ParamByName('xIDFattura').asInteger:=TFatturaID.Value;
                    Q1.ParamByName('xDescrizione').asString:=ModFattForm.EDescrizione.text;
                    Q1.ParamByName('xImponibile').asFloat:=ModFattForm.RxImponibile.Value;
                    Q1.ParamByName('xPercentualeIVA').asInteger:=ModFattForm.SEPercIVA.Value;
                    Q1.ExecSQL;
                    // ricalcolo imponibile fattura
                    TFattDett.DisableControls;
                    TFattDett.Close;
                    TFattDett.Open;
                    tImporto:=0;
                    tTotal:=0;
                    while not TFattDett.EOF do begin
                         tImporto:=tImporto+TFattDettImponibile.Value;
                         tTotal:=tTotal+TFattDettTotale.Value;
                         TFattDett.Next;
                    end;
                    Q1.SQL.text:='update Fatture set Importo=:xImporto,Totale=:xTotale '+
                         'where ID='+TFatturaID.asString;
                    Q1.ParamByName('xImporto').asFloat:=tImporto;
                    Q1.ParamByName('xTotale').asFloat:=tTotal;
                    Q1.ExecSQL;
                    TFattDett.EnableControls;

                    DB.CommitTrans;
                    // refresh fattura e dettaglio
                    TFattDett.Close;
                    xIDfattura:=TFatturaID.Value;
                    TFattura.Close;
                    TFattura.ParamByName('xID').asInteger:=xIDfattura;
                    TFattura.open;
                    TFattDett.Open;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               end;
          end;

     end;
     ModFattForm.Free;
end;

procedure TFatturaForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     // SE C'E' il CONTROLLO DI GESTIONE:
     // per ogni dettaglio di fattura legato a compenso
     // ==> aggiorna il valore A CONSUNTIVO in H1SelCG
     if copy(Data.GlobalCheckBoxIndici.Value,6,1)='1' then begin
          TFattDett.First;
          while not TFattDett.EOF do begin
               if (TFattDettIDCompenso.AsString<>'')and(TFattDettIDCompenso.Value>0) then begin
                    with Data do begin
                         DB.BeginTrans;
                         try
                              // compensi ricerche
                              Q1.SQL.text:='update CG_ContrattiRicavi set Importo=:xImporto '+
                                   'where IDCompenso=:xIDCompenso';
                              Q1.ParambyName('xImporto').asFloat:=TFattDettImponibile.Value;
                              Q1.ParambyName('xIDCompenso').asInteger:=TFattDettIDCompenso.Value;
                              Q1.ExecSQL;
                              // annunci
                              {Q1.SQL.text:='update CG_ContrattiRicavi set Importo=:xImporto '+
                                   'where IDCompenso=:xIDCompenso';
                              Q1.ParambyName('xImporto').asFloat:=TFattDettImponibile.Value;
                              Q1.ParambyName('xIDCompenso').asInteger:=TFattDettIDCompenso.Value;
                              Q1.ExecSQL;}

                              DB.CommitTrans;
                         except
                              DB.RollbackTrans;
                              MessageDlg('ERRORE SUL DATABASE: operazione non effettuata',mtError, [mbOK],0);
                         end;
                    end;
               end;
               TFattDett.Next;
          end;
     end;
end;

end.

