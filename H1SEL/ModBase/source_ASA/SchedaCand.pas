unit SchedaCand;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     StdCtrls,DBCtrls,ExtCtrls,Grids,DBGrids,DtEdit97,DtEdDB97,
     ComCtrls,Buttons,Mask,TB97,DB,DBTables,dxDBTLCl,dxGrClms,dxTL,
     dxDBCtrl,dxDBGrid,dxCntner,FrameDBRichEdit2;

type
     TSchedaCandForm=class(TForm)
          Panel1: TPanel;
          Panel2: TPanel;
          Label1: TLabel;
          Label2: TLabel;
          Label11: TLabel;
          DBECognome: TDBEdit;
          DBEdit2: TDBEdit;
          DBEdit14: TDBEdit;
          BitBtn1: TBitBtn;
          PageControl2: TPageControl;
          TSAnagDatiAnag: TTabSheet;
          PanAnag: TPanel;
          Label38: TLabel;
          Label100: TLabel;
          Label101: TLabel;
          Label35: TLabel;
          DBLookupComboBox1: TDBLookupComboBox;
          DBEdit65: TDBEdit;
          DBEdit16: TDBEdit;
          DbDateEdit971: TDbDateEdit97;
          TSAnagRicerche: TTabSheet;
          DBGrid7: TDBGrid;
          Panel12: TPanel;
          TSAnagComp: TTabSheet;
          DBGrid14: TDBGrid;
          Panel73: TPanel;
          TsAnagMans: TTabSheet;
          Splitter4: TSplitter;
          Panel75: TPanel;
          DBGrid41: TDBGrid;
          Panel17: TPanel;
          Panel74: TPanel;
          DBGrid19: TDBGrid;
          TSAnagStorico: TTabSheet;
          Panel7: TPanel;
          DBGrid3: TDBGrid;
          Panel19: TPanel;
          Label15: TLabel;
          DBEdit42: TDBEdit;
          Panel126: TPanel;
          Panel127: TPanel;
          GroupBox1: TGroupBox;
          Label7: TLabel;
          Label8: TLabel;
          Label9: TLabel;
          SpeedButton2: TSpeedButton;
          DBEdit7: TDBEdit;
          DBEdit8: TDBEdit;
          DBEdit9: TDBEdit;
          GroupBox2: TGroupBox;
          Label4: TLabel;
          Label19: TLabel;
          Label22: TLabel;
          SpeedButton4: TSpeedButton;
          SpeedButton3: TSpeedButton;
          DBEdit4: TDBEdit;
          DBEdit10: TDBEdit;
          DBEdit11: TDBEdit;
          Panel3: TPanel;
          BModifica: TToolbarButton97;
          Panel4: TPanel;
          Label88: TLabel;
          Label89: TLabel;
          Label90: TLabel;
          Label10: TLabel;
          DBEdit21: TDBEdit;
          DBEdit22: TDBEdit;
          DBEdit5: TDBEdit;
          DBEdit20: TDBEdit;
          Panel5: TPanel;
          Label5: TLabel;
          Label12: TLabel;
          Label13: TLabel;
          Label85: TLabel;
          DBEdit1: TDBEdit;
          DBEdit12: TDBEdit;
          DBEdit18: TDBEdit;
          DBEdit70: TDBEdit;
          PanCF: TPanel;
          Label49: TLabel;
          LabPartitaIVA: TLabel;
          DBEdit17: TDBEdit;
          DBEPartitaIVA: TDBEdit;
          PanOldCV: TPanel;
          Label86: TLabel;
          DBEdit71: TDBEdit;
          BValutaz: TSpeedButton;
          TSAnagNoteInt: TTabSheet;
          QAnagNote: TQuery;
          QAnagNoteNote: TMemoField;
          QAnagNoteIDAnagrafica: TIntegerField;
          DsQAnagNote: TDataSource;
          UpdAnagNote: TUpdateSQL;
          DBEdit6: TDBEdit;
          DBComboBox7: TDBComboBox;
          DBEdit54: TDBEdit;
          Label6: TLabel;
          Label93: TLabel;
          Label99: TLabel;
          DBEdit3: TDBEdit;
          DBComboBox8: TDBComboBox;
          DBEdit79: TDBEdit;
          Label3: TLabel;
          Label105: TLabel;
          TbBAnagMansNew: TToolbarButton97;
          TbBAnagMansDel: TToolbarButton97;
          TbBAnagMansMod: TToolbarButton97;
          FrameDBRich1: TFrameDBRich;
          Splitter1: TSplitter;
          Panel153: TPanel;
          dxDBGrid5: TdxDBGrid;
          dxDBGrid5ID: TdxDBGridMaskColumn;
          dxDBGrid5Progressivo: TdxDBGridMaskColumn;
          dxDBGrid5Note: TdxDBGridBlobColumn;
          procedure PageControl2Change(Sender: TObject);
          procedure SpeedButton2Click(Sender: TObject);
          procedure SpeedButton4Click(Sender: TObject);
          procedure SpeedButton3Click(Sender: TObject);
          procedure BModificaClick(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
          procedure DBEdit15Exit(Sender: TObject);
          procedure BValutazClick(Sender: TObject);
          procedure DsQAnagNoteStateChange(Sender: TObject);
          procedure TbBAnagMansNewClick(Sender: TObject);
          procedure TbBAnagMansModClick(Sender: TObject);
          procedure TbBAnagMansDelClick(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     SchedaCandForm: TSchedaCandForm;

implementation

uses ModuloDati2,ModuloDati,Comuni,ValutazColloquio,InsRuolo,CompInserite,
     Main,ModRuolo,MDRicerche;

{$R *.DFM}

procedure TSchedaCandForm.PageControl2Change(Sender: TObject);
begin
     if Data.DsAnagrafica.State=dsEdit then begin
          with Data.TAnagrafica do begin
               Post;
               Data.DB.BeginTrans;
               try
                    ApplyUpdates;
                    Data.DB.CommitTrans;
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
                    raise;
               end;
               CommitUpdates;
          end;
          PanAnag.Enabled:=False;
          BModifica.Enabled:=True;
     end;
     if DsQAnagNote.State=dsEdit then begin
          with QAnagNote do begin
               Post;
               Data.DB.BeginTrans;
               try
                    ApplyUpdates;
                    Data.DB.CommitTrans;
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
                    raise;
               end;
               CommitUpdates;
          end;
     end;

     // RICERCHE
     if PageControl2.ActivePage=TSAnagRicerche then begin
          Data2.QAnagRicerche.Open;
     end else begin
          Data2.QAnagRicerche.Close;
     end;

     // COMPETENZE
     if PageControl2.ActivePage=TSAnagComp then begin
          Data2.TCompDipendente.Open;
     end else begin
          Data2.TCompDipendente.Close;
     end;

     // RUOLI e CARATTERISTICHE
     if PageControl2.ActivePage=TsAnagMans then begin
          Data.TAnagMansioni.Open;
          Data2.TCarattDip.Open;
     end else begin
          Data.TAnagMansioni.Close;
          Data2.TCarattDip.Close;
     end;

     // STORICO
     if PageControl2.ActivePage=TSAnagStorico then begin
          Data.QStorico.Open;
     end else begin
          Data.QStorico.Close;
     end;

     // NOTE INTERNE
     if PageControl2.ActivePage=TSAnagNoteInt then begin
          QAnagNote.Open;
          DataRicerche.QAnagNoteRic.Open;
     end else begin
          QAnagNote.Close;
          DataRicerche.QAnagNoteRic.Close;
     end;
end;

procedure TSchedaCandForm.SpeedButton2Click(Sender: TObject);
begin
     ComuniForm:=TComuniForm.create(self);
     ComuniForm.EProv.text:=MainForm.xUltimaProv;
     ComuniForm.ShowModal;
     if not(Data.DsAnagrafica.State in [dsEdit,dsInsert]) then Data.TAnagrafica.Edit;
     if ComuniForm.ModalResult=mrOK then begin
          Data.TAnagraficaComune.Value:=ComuniForm.QComuniDescrizione.Value;
          Data.TAnagraficaCap.Value:=ComuniForm.QComuniCap.Value;
          Data.TAnagraficaProvincia.Value:=ComuniForm.QComuniProv.Value;
          Data.TAnagraficaIDComuneRes.Value:=ComuniForm.QComuniID.Value;
          Data.TAnagraficaIDZonaRes.Value:=ComuniForm.QComuniIDZona.Value;
     end else begin
          Data.TAnagraficaComune.Value:='';
          Data.TAnagraficaCap.Value:='';
          Data.TAnagraficaProvincia.Value:='';
          Data.TAnagraficaIDComuneRes.Value:=0;
          Data.TAnagraficaIDZonaRes.Value:=0;
     end;
     with Data.TAnagrafica do begin
          Data.DB.BeginTrans;
          try
               ApplyUpdates;
               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               raise;
          end;
          CommitUpdates;
     end;
     BModifica.Enabled:=True;
     PanAnag.Enabled:=False;
     MainForm.xUltimaProv:=ComuniForm.EProv.text;
     ComuniForm.Free;
end;

procedure TSchedaCandForm.SpeedButton4Click(Sender: TObject);
begin
     ComuniForm:=TComuniForm.create(self);
     ComuniForm.EProv.text:=MainForm.xUltimaProv;
     ComuniForm.ShowModal;
     if not(Data.DsAnagrafica.State in [dsEdit,dsInsert]) then Data.TAnagrafica.Edit;
     if ComuniForm.ModalResult=mrOK then begin
          Data.TAnagraficaDomicilioComune.Value:=ComuniForm.QComuniDescrizione.Value;
          Data.TAnagraficaDomicilioCap.Value:=ComuniForm.QComuniCap.Value;
          Data.TAnagraficaDomicilioProvincia.Value:=ComuniForm.QComuniProv.Value;
          Data.TAnagraficaIDComuneDom.Value:=ComuniForm.QComuniID.Value;
          Data.TAnagraficaIDZonaDom.Value:=ComuniForm.QComuniIDZona.Value;
     end else begin
          Data.TAnagraficaDomicilioComune.Value:='';
          Data.TAnagraficaDomicilioCap.Value:='';
          Data.TAnagraficaDomicilioProvincia.Value:='';
          Data.TAnagraficaIDComuneDom.Value:=0;
          Data.TAnagraficaIDZonaDom.Value:=0;
     end;
     with Data.TAnagrafica do begin
          Data.DB.BeginTrans;
          try
               ApplyUpdates;
               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               raise;
          end;
          CommitUpdates;
     end;
     BModifica.Enabled:=True;
     PanAnag.Enabled:=False;
     MainForm.xUltimaProv:=ComuniForm.EProv.text;
     ComuniForm.Free;
end;

procedure TSchedaCandForm.SpeedButton3Click(Sender: TObject);
begin
     if not(Data.DsAnagrafica.State in [dsEdit,dsInsert]) then Data.TAnagrafica.Edit;
     Data.TAnagraficaDomicilioIndirizzo.Value:=Data.TAnagraficaIndirizzo.Value;
     Data.TAnagraficaDomicilioCap.Value:=Data.TAnagraficaCap.Value;
     Data.TAnagraficaDomicilioComune.Value:=Data.TAnagraficaComune.Value;
     Data.TAnagraficaDomicilioProvincia.Value:=Data.TAnagraficaProvincia.Value;
     Data.TAnagraficaIDComuneDom.Value:=Data.TAnagraficaIDComuneRes.Value;
     Data.TAnagraficaIDZonaDom.Value:=Data.TAnagraficaIDZonaRes.Value;
end;

procedure TSchedaCandForm.BModificaClick(Sender: TObject);
begin
     if Data.TAnagrafica.EOF then
          ShowMessage('questo archivio � archivio vuoto: nessuna scheda da modificare')
     else begin
          Data.TAnagrafica.Edit;
          PanAnag.Enabled:=True;
          BModifica.Enabled:=False;
     end;
end;

procedure TSchedaCandForm.FormShow(Sender: TObject);
begin
     Caption:='[M/00] - '+Caption;
     Data.xSchedaCand:=True;
     //QAnagNote.Open;
     DataRicerche.QAnagNoteRic.Open;
     PageControl2.ActivePage:=TSAnagNoteInt;
     FrameDBRich1.ToolButton1.Visible:=False;
     // IMPOSTAZIONI A SECONDA DEL CLIENTE (da GLOBAL)
     if Uppercase(copy(Data.GlobalNomeAzienda.Value,1,5))='ERGON' then begin
          PanCF.Visible:=False;
          PageControl2.Pages[2].TabVisible:=False;
     end;
end;

procedure TSchedaCandForm.FormClose(Sender: TObject;
     var Action: TCloseAction);
begin
     Data.xSchedaCand:=False;
     if Data.DsAnagrafica.State=dsEdit then begin
          with Data.TAnagrafica do begin
               Post;
               Data.DB.BeginTrans;
               try
                    ApplyUpdates;
                    Data.DB.CommitTrans;
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
                    raise;
               end;
               CommitUpdates;
          end;
     end;
     if DsQAnagNote.State=dsEdit then begin
          with QAnagNote do begin
               Post;
               Data.DB.BeginTrans;
               try
                    ApplyUpdates;
                    Data.DB.CommitTrans;
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
                    raise;
               end;
               CommitUpdates;
          end;
     end;
end;

procedure TSchedaCandForm.DBEdit15Exit(Sender: TObject);
begin
     if (DBEdit12.Text<>'') then
          if (pos('-',DBEdit12.Text)=0)and(pos('/',DBEdit12.Text)=0) then begin
               Showmessage('ATTENZIONE: Numero di cellulare non correttamente formattato.'+chr(13)+
                    'Utilizzare la linea (-) o la barra (/) per separare il prefisso dal numero');
               DBEdit12.SetFocus;
          end;
end;

procedure TSchedaCandForm.BValutazClick(Sender: TObject);
begin
     ValutazColloquioForm:=TValutazColloquioForm.create(self);
     ValutazColloquioForm.xIDAnag:=Data.TAnagraficaID.Value;
     ValutazColloquioForm.ShowModal;
     ValutazColloquioForm.Free;
end;

procedure TSchedaCandForm.DsQAnagNoteStateChange(Sender: TObject);
var b: boolean;
begin
     b:=DsQAnagNote.State in [dsEdit,dsInsert];
end;

procedure TSchedaCandForm.TbBAnagMansNewClick(Sender: TObject);
var xIDAnag,xIDMans,xTotComp,i: integer;
     xComp: array[1..50] of integer;
begin
     if not MainForm.CheckProfile('030') then Exit;
     InsRuoloForm:=TInsRuoloForm.create(self);
     InsRuoloForm.ShowModal;
     if InsRuoloForm.ModalResult=mrOK then begin
          // controllo esistenza ruolo
          Data.QTemp.Close;
          Data.QTemp.SQL.text:='select count(ID) Tot from AnagMansioni where IDAnagrafica=:xIDAnag and IDMansione=:xIDMans';
          Data.QTemp.ParamByName('xIDAnag').asInteger:=Data.TAnagraficaID.Value;
          Data.QTemp.ParamByName('xIDMans').asInteger:=InsRuoloForm.TRuoliID.Value;
          Data.QTemp.Open;
          if Data.QTemp.FieldByName('Tot').asInteger>0 then begin
               MessageDlg('Ruolo gi� associato al candidato. '+chr(13)+
                    'IMPOSSIBILE INSERIRE.',mtError, [mbOK],0);
               exit;
          end;
          Data.QTemp.Close;

          with Data do begin
               DB.BeginTrans;
               try
                    // inserimento competenze (se � stato richiesto)
                    if InsRuoloForm.CBInsComp.Checked then begin
                         CompInseriteForm:=TCompInseriteForm.create(self);
                         CompInseriteForm.ERuolo.Text:=InsRuoloForm.TRuoliDescrizione.Value;
                         Data2.QCompRuolo.Close;
                         Data2.QCompRuolo.Prepare;
                         Data2.QCompRuolo.Params[0].asInteger:=InsRuoloForm.TRuoliID.Value;
                         Data2.QCompRuolo.Open;
                         Data2.QCompRuolo.First;
                         xTotComp:=0;
                         Data2.TAnagCompIDX.Open;
                         CompInseriteForm.SG1.RowCount:=2;
                         CompInseriteForm.SG1.RowHeights[0]:=16;
                         while not Data2.QCompRuolo.EOF do begin
                              if not Data2.TAnagCompIDX.FindKey([xIDAnag,Data2.QCompRuoloIDCompetenza.Value]) then begin
                                   CompInseriteForm.SG1.RowHeights[xTotComp+1]:=16;
                                   CompInseriteForm.SG1.Cells[0,xTotComp+1]:=Data2.QCompRuoloDescCompetenza.Value;
                                   CompInseriteForm.SG1.Cells[1,xTotComp+1]:='0';
                                   if TAnagraficaIDTipoStato.Value<>2 then begin
                                        CompInseriteForm.SG1.Cells[2,xTotComp+1]:='�';
                                        CompInseriteForm.SG1.ColorRow[xTotComp+1]:=clLime;
                                   end else begin
                                        CompInseriteForm.SG1.Cells[2,xTotComp+1]:='x';
                                        CompInseriteForm.SG1.ColorRow[xTotComp+1]:=clRed;
                                   end;
                                   xComp[xTotComp+1]:=Data2.QCompRuoloIDCompetenza.Value;
                                   Inc(xTotComp);
                                   CompInseriteForm.SG1.RowCount:=CompInseriteForm.SG1.RowCount+1;
                              end;
                              Data2.QCompRuolo.Next;
                         end;
                         CompInseriteForm.SG1.RowCount:=CompInseriteForm.SG1.RowCount-1;
                         if xTotComp>0 then CompInseriteForm.ShowModal;
                         if CompInseriteForm.ModalResult=mrOK then begin
                              for i:=1 to CompInseriteForm.SG1.RowCount-1 do begin
                                   if CompInseriteForm.SG1.ColorRow[i]=clLime then begin

                                        Q1.Close;
                                        Q1.SQL.text:='insert into CompetenzeAnagrafica (IDAnagrafica,IDCompetenza,Valore) '+
                                             'values (:xIDAnagrafica,:xIDCompetenza,:xValore)';
                                        Q1.ParamByName('xIDAnagrafica').asInteger:=Data.TAnagraficaID.Value;
                                        Q1.ParamByName('xIDCompetenza').asInteger:=xComp[i];
                                        Q1.ParamByName('xValore').asInteger:=StrToInt(CompInseriteForm.SG1.Cells[1,i]);
                                        Q1.ExecSQL;

                                        Q1.SQL.text:='insert into StoricoCompAnag (IDCompetenza,IDAnagrafica,DallaData,MotivoAumento,valore) '+
                                             'values (:xIDCompetenza,:xIDAnagrafica,:xDallaData,:xMotivoAumento,:xvalore)';
                                        Q1.ParamByName('xIDCompetenza').asInteger:=xComp[i];
                                        Q1.ParamByName('xIDAnagrafica').asInteger:=Data.TAnagraficaID.Value;
                                        Q1.ParamByName('xDallaData').asDateTime:=Date;
                                        Q1.ParamByName('xMotivoAumento').asString:='valore iniziale';
                                        Q1.ParamByName('xValore').asInteger:=StrToInt(CompInseriteForm.SG1.Cells[1,i]);
                                        Q1.ExecSQL;
                                   end;
                              end;
                         end;
                    end;
                    // inserimento ruolo
                    Q1.Close;
                    Q1.SQL.text:='insert into AnagMansioni (IDAnagrafica,IDMansione,IDArea,Attuale,AnniEsperienza,Disponibile) '+
                         'values (:xIDAnagrafica,:xIDMansione,:xIDArea,:xAttuale,:xAnniEsperienza,:xDisponibile)';
                    Q1.ParamByName('xIDAnagrafica').asInteger:=Data.TAnagraficaID.Value;
                    Q1.ParamByName('xIDMansione').asInteger:=InsRuoloForm.TRuoliID.Value;
                    Q1.ParamByName('xIDArea').asInteger:=InsRuoloForm.TAreeID.Value;
                    Q1.ParamByName('xAttuale').asBoolean:=False;
                    Q1.ParamByName('xAnniEsperienza').asInteger:=0;
                    Q1.ParamByName('xDisponibile').asBoolean:=True;
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    Data.TAnagMansioni.Close;
                    Data.TAnagMansioni.Open;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               end;

          end;
          InsRuoloForm.Free;
     end;
end;

procedure TSchedaCandForm.TbBAnagMansModClick(Sender: TObject);
begin
     if not Mainform.CheckProfile('032') then Exit;
     if Data.TAnagMansioni.RecordCount=0 then exit;
     ModRuoloForm:=TModRuoloForm.create(self);
     ModRuoloForm.SEAnniEsp.Value:=Data.TAnagMansioniAnniEsperienza.Value;
     ModRuoloForm.CBAttuale.checked:=Data.TAnagMansioniAttuale.Value;
     ModRuoloForm.CBDisponibile.checked:=Data.TAnagMansioniDisponibile.Value;
     ModRuoloForm.Caption:='Ruolo: '+Data.TAnagMansioniMansione.Value;
     ModRuoloForm.ShowModal;
     if ModRuoloForm.Modalresult=mrOK then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text:='update AnagMansioni set AnniEsperienza=:xAnniEsperienza,Attuale=:xAttuale,Disponibile=:xDisponibile '+
                         'where ID='+Data.TAnagMansioniID.AsString;
                    Q1.ParamByName('xAnniEsperienza').asInteger:=Round(ModRuoloForm.SEAnniEsp.Value);
                    Q1.ParamByName('xAttuale').asBoolean:=ModRuoloForm.CBAttuale.checked;
                    Q1.ParamByName('xDisponibile').asBoolean:=ModRuoloForm.CBDisponibile.checked;
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    Data.TAnagMansioni.Close;
                    Data.TAnagMansioni.Open;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               end;
          end;
     end;
end;

procedure TSchedaCandForm.TbBAnagMansDelClick(Sender: TObject);
begin
     if not Mainform.CheckProfile('031') then Exit;
     if Data.TAnagMansioni.RecordCount>0 then
          if MessageDlg('Sei sicuro di voler cancellare ?',mtWarning,
               [mbNo,mbYes],0)=mrYes then begin
               with Data do begin
                    DB.BeginTrans;
                    try
                         Q1.Close;
                         Q1.SQL.text:='delete from AnagMansioni '+
                              'where ID='+Data.TAnagMansioniID.AsString;
                         Q1.ExecSQL;
                         DB.CommitTrans;
                         Data.TAnagMansioni.Close;
                         Data.TAnagMansioni.Open;
                    except
                         DB.RollbackTrans;
                         MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
                    end;
               end;
          end;
end;

end.

