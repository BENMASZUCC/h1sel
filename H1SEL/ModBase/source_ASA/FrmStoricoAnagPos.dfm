object StoricoAnagPosFrame: TStoricoAnagPosFrame
  Left = 0
  Top = 0
  Width = 723
  Height = 297
  TabOrder = 0
  object PanButtons: TPanel
    Left = 0
    Top = 0
    Width = 723
    Height = 39
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object ToolbarButton971: TToolbarButton97
      Left = 82
      Top = 2
      Width = 78
      Height = 35
      Caption = 'Modifica dati'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000000
        000033333377777777773333330FFFFFFFF03FF3FF7FF33F3FF700300000FF0F
        00F077F777773F737737E00BFBFB0FFFFFF07773333F7F3333F7E0BFBF000FFF
        F0F077F3337773F3F737E0FBFBFBF0F00FF077F3333FF7F77F37E0BFBF00000B
        0FF077F3337777737337E0FBFBFBFBF0FFF077F33FFFFFF73337E0BF0000000F
        FFF077FF777777733FF7000BFB00B0FF00F07773FF77373377373330000B0FFF
        FFF03337777373333FF7333330B0FFFF00003333373733FF777733330B0FF00F
        0FF03333737F37737F373330B00FFFFF0F033337F77F33337F733309030FFFFF
        00333377737FFFFF773333303300000003333337337777777333}
      NumGlyphs = 2
      WordWrap = True
      OnClick = ToolbarButton971Click
    end
    object ToolbarButton972: TToolbarButton97
      Left = 161
      Top = 2
      Width = 78
      Height = 35
      Caption = 'Elimina riga'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333000000000
        3333333777777777F3333330F777777033333337F3F3F3F7F3333330F0808070
        33333337F7F7F7F7F3333330F080707033333337F7F7F7F7F3333330F0808070
        33333337F7F7F7F7F3333330F080707033333337F7F7F7F7F3333330F0808070
        333333F7F7F7F7F7F3F33030F080707030333737F7F7F7F7F7333300F0808070
        03333377F7F7F7F773333330F080707033333337F7F7F7F7F333333070707070
        33333337F7F7F7F7FF3333000000000003333377777777777F33330F88877777
        0333337FFFFFFFFF7F3333000000000003333377777777777333333330777033
        3333333337FFF7F3333333333000003333333333377777333333}
      NumGlyphs = 2
      WordWrap = True
      OnClick = ToolbarButton972Click
    end
    object ToolbarButton973: TToolbarButton97
      Left = 3
      Top = 2
      Width = 78
      Height = 35
      Caption = 'Inserisci riga'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        33333333FF33333333FF333993333333300033377F3333333777333993333333
        300033F77FFF3333377739999993333333333777777F3333333F399999933333
        33003777777333333377333993333333330033377F3333333377333993333333
        3333333773333333333F333333333333330033333333F33333773333333C3333
        330033333337FF3333773333333CC333333333FFFFF77FFF3FF33CCCCCCCCCC3
        993337777777777F77F33CCCCCCCCCC3993337777777777377333333333CC333
        333333333337733333FF3333333C333330003333333733333777333333333333
        3000333333333333377733333333333333333333333333333333}
      NumGlyphs = 2
      WordWrap = True
      OnClick = ToolbarButton973Click
    end
  end
  object RxDBGrid1: TRxDBGrid
    Left = 0
    Top = 39
    Width = 723
    Height = 258
    Align = alClient
    DataSource = dsQStoricoPos
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'Descrizione'
        Title.Caption = 'Nome nodo'
        Width = 147
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Cognome'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        Width = 110
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Nome'
        Width = 105
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DallaData'
        Title.Caption = 'Dalla data'
        Width = 63
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Alladata'
        Title.Caption = 'Alla data'
        Width = 63
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'AutorizzatoDa'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clPurple
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Title.Caption = 'Autorizzato da'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clPurple
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 99
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Note'
        Width = 96
        Visible = True
      end>
  end
  object QStoricoPos: TQuery
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select Anag1.Cognome,Anag1.Nome,StoricoAnagPosOrg.*,'
      
        '          Anag2.Cognome AutorizzatoDa, Anag2.Nome AutorizzatoDaN' +
        'ome,'
      '           Organigramma.Descrizione'
      
        'from StoricoAnagPosOrg, Anagrafica Anag1, Anagrafica Anag2, Orga' +
        'nigramma'
      'where StoricoAnagPosOrg.IDAnagrafica = Anag1.ID'
      '   and StoricoAnagPosOrg.IDAutorizzatoDa *= Anag2.ID'
      '   and StoricoAnagPosOrg.IDOrganigramma = Organigramma.ID'
      'order by DallaData')
    Left = 40
    Top = 88
    object QStoricoPosCognome: TStringField
      FieldName = 'Cognome'
      Origin = 'EBCDB.StoricoAnagPosOrg.ID'
      FixedChar = True
      Size = 30
    end
    object QStoricoPosNome: TStringField
      FieldName = 'Nome'
      Origin = 'EBCDB.StoricoAnagPosOrg.IDAnagrafica'
      FixedChar = True
      Size = 30
    end
    object QStoricoPosID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.StoricoAnagPosOrg.IDOrganigramma'
    end
    object QStoricoPosIDAnagrafica: TIntegerField
      FieldName = 'IDAnagrafica'
      Origin = 'EBCDB.StoricoAnagPosOrg.DallaData'
    end
    object QStoricoPosIDOrganigramma: TIntegerField
      FieldName = 'IDOrganigramma'
      Origin = 'EBCDB.StoricoAnagPosOrg.Alladata'
    end
    object QStoricoPosDallaData: TDateTimeField
      FieldName = 'DallaData'
      Origin = 'EBCDB.StoricoAnagPosOrg.Note'
    end
    object QStoricoPosAlladata: TDateTimeField
      FieldName = 'Alladata'
      Origin = 'EBCDB.StoricoAnagPosOrg.IDAutorizzatoDa'
    end
    object QStoricoPosNote: TStringField
      FieldName = 'Note'
      Origin = 'EBCDB.Anagrafica.Cognome'
      FixedChar = True
      Size = 80
    end
    object QStoricoPosIDAutorizzatoDa: TIntegerField
      FieldName = 'IDAutorizzatoDa'
      Origin = 'EBCDB.Anagrafica.Nome'
    end
    object QStoricoPosAutorizzatoDa: TStringField
      FieldName = 'AutorizzatoDa'
      Origin = 'EBCDB.Anagrafica.Cognome'
      FixedChar = True
      Size = 30
    end
    object QStoricoPosDescrizione: TStringField
      FieldName = 'Descrizione'
      Origin = 'EBCDB.Organigramma.Descrizione'
      FixedChar = True
      Size = 60
    end
    object QStoricoPosAutorizzatoDaNome: TStringField
      FieldName = 'AutorizzatoDaNome'
      Origin = 'EBCDB.Anagrafica.Nome'
      FixedChar = True
      Size = 30
    end
  end
  object dsQStoricoPos: TDataSource
    DataSet = QStoricoPos
    Left = 40
    Top = 120
  end
end
