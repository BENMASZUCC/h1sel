unit InsCliente;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, DtEdit97;

type
  TInsClienteForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Label1: TLabel;
    EDenominazione: TEdit;
    Label2: TLabel;
    CBStato: TComboBox;
    SpeedButton1: TSpeedButton;
    EAttivita: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    DEDataCon: TDateEdit97;
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    xIDAttivita:integer;
  end;

var
  InsClienteForm: TInsClienteForm;

implementation

uses SelAttivita;

{$R *.DFM}

procedure TInsClienteForm.SpeedButton1Click(Sender: TObject);
begin
     SelAttivitaForm:=TSelAttivitaForm.create(self);
     SelAttivitaForm.ShowModal;
     if SelAttivitaForm.ModalResult=mrOK then begin
        xIDAttivita:=SelAttivitaForm.TSettoriID.Value;
        EAttivita.text:=SelAttivitaForm.TSettoriAttivita.Value;
     end else
         if MessageDlg('Vuoi cancellare il campo attivit� ?',mtInformation,[mbYes,mbNo],0)=mrYes then begin
            xIDAttivita:=0;
            EAttivita.text:='';
         end;
     SelAttivitaForm.Free;
end;

procedure TInsClienteForm.FormShow(Sender: TObject);
begin
     xIDAttivita:=0;   
     Caption:='[M/100] - '+Caption;
end;

procedure TInsClienteForm.BitBtn1Click(Sender: TObject);
begin
     if xIDAttivita=0 then begin
        ModalResult:=mrNone;
        MessageDlg('� necessario indicare l''attivit�',mtError,[mbOK],0);
        Abort;
     end;
end;

end.
