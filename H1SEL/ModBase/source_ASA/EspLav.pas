unit EspLav;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     ExtCtrls,Grids,DBGrids,Db,DBTables,StdCtrls,Buttons,TB97;

type
     TEspLavForm=class(TForm)
          BitBtn1: TBitBtn;
          BitBtn2: TBitBtn;
          DsElencoAziende: TDataSource;
          Timer1: TTimer;
          TSettoriLK: TTable;
          TSettoriLKID: TAutoIncField;
          TSettoriLKAttivita: TStringField;
          DsAree: TDataSource;
          Panel73: TPanel;
          DBGrid2: TDBGrid;
          DsRuoli: TDataSource;
          DBGrid3: TDBGrid;
          Panel2: TPanel;
          Timer2: TTimer;
          CBInsComp: TCheckBox;
          PanAziende: TPanel;
          DBGrid1: TDBGrid;
          Panel1: TPanel;
          BAziendaNew: TToolbarButton97;
          BAziendaDel: TToolbarButton97;
          BAziendaMod: TToolbarButton97;
          Panel3: TPanel;
          RGOpzione: TRadioGroup;
          PanSettori: TPanel;
          Panel4: TPanel;
          DsSettori: TDataSource;
          DBGrid4: TDBGrid;
          Panel5: TPanel;
          ToolbarButton971: TToolbarButton97;
          Panel6: TPanel;
          Label1: TLabel;
          EAzienda: TEdit;
          TElencoAziende: TQuery;
          TElencoAziendeID: TAutoIncField;
          TElencoAziendeDescrizione: TStringField;
          TElencoAziendeComune: TStringField;
          TElencoAziendeProvincia: TStringField;
          TElencoAziendeIDAttivita: TIntegerField;
          TElencoAziendeAttivita: TStringField;
          TElencoAziendeIndirizzo: TStringField;
          TElencoAziendeCap: TStringField;
          TElencoAziendeFatturato: TFloatField;
          TElencoAziendeNumDipendenti: TSmallintField;
          Q: TQuery;
          TElencoAziendeTipoStrada: TStringField;
          TElencoAziendeNumCivico: TStringField;
          TElencoAziendeNazioneAbb: TStringField;
          TElencoAziendeTelefono: TStringField;
          TElencoAziendeFax: TStringField;
          TElencoAziendeSitoInternet: TStringField;
          TElencoAziendeDescAttivitaAzienda: TStringField;
          CBTestoCont: TCheckBox;
          TSettori: TQuery;
          TSettoriID: TAutoIncField;
          TSettoriAttivita: TStringField;
          TSettoriIDAreaSettore: TIntegerField;
          TSettoriIDAzienda: TStringField;
          TSettoriAttivita_ENG: TStringField;
          BSettoriITA: TSpeedButton;
          BSettoriENG: TSpeedButton;
          BAreeRuoliITA: TSpeedButton;
          BAreeRuoliENG: TSpeedButton;
          TAree: TQuery;
          TAreeID: TAutoIncField;
          TAreeDescrizione: TStringField;
          TAreeDescrizione_ENG: TStringField;
          TRuoli: TQuery;
          TRuoliID: TAutoIncField;
          TRuoliIDArea: TIntegerField;
          TRuoliDescrizione: TStringField;
          TRuoliDescrizione_ENG: TStringField;
          procedure Timer1Timer(Sender: TObject);
          procedure BAziendaDelClick(Sender: TObject);
          procedure BAziendaOKClick(Sender: TObject);
          procedure BAziendaCanClick(Sender: TObject);
          procedure BAziendaNewClick(Sender: TObject);
          procedure BAziendaModClick(Sender: TObject);
          procedure Timer2Timer(Sender: TObject);
          procedure RGOpzioneClick(Sender: TObject);
          procedure ToolbarButton971Click(Sender: TObject);
          procedure EAziendaChange(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure CBTestoContClick(Sender: TObject);
          procedure BSettoriITAClick(Sender: TObject);
          procedure BSettoriENGClick(Sender: TObject);
          procedure BAreeRuoliITAClick(Sender: TObject);
          procedure BAreeRuoliENGClick(Sender: TObject);
     private
          xStringa,xStringa2: string;
     public
          { Public declarations }
     end;

var
     EspLavForm: TEspLavForm;

implementation

uses Azienda,ModuloDati,Main;

{$R *.DFM}

procedure TEspLavForm.Timer1Timer(Sender: TObject);
begin
     xStringa:='';
end;

procedure TEspLavForm.BAziendaDelClick(Sender: TObject);
begin
     if not TElencoAziende.IsEmpty then begin
          // controllo eventuali associazioni
          // SOGGETTI
          Q.Close;
          Q.SQL.text:='select count(*) Tot from EsperienzeLavorative '+
               'where IDAzienda='+TElencoAziendeID.asString;
          Q.Open;
          if Q.FieldByName('Tot').asInteger>0 then begin
               MessageDlg('Esistono candidati associati all''azienda.'+chr(13)+
                    'IMPOSSIBILE CANCELLARE',mtError, [mbOK],0);
               Exit;
          end;
          // COMMESSE
          Q.Close;
          Q.SQL.text:='select count(*) Tot from EBC_Ricerche '+
               'where IDCliente='+TElencoAziendeID.asString;
          Q.Open;
          if Q.FieldByName('Tot').asInteger>0 then begin
               MessageDlg('Esistono commesse/ricerche associate all''azienda.'+chr(13)+
                    'IMPOSSIBILE CANCELLARE',mtError, [mbOK],0);
               Exit;
          end;
          // OFFERTE
          Q.Close;
          Q.SQL.text:='select count(*) Tot from EBC_Offerte '+
               'where IDCliente='+TElencoAziendeID.asString;
          Q.Open;
          if Q.FieldByName('Tot').asInteger>0 then begin
               MessageDlg('Esistono offerte associate all''azienda.'+chr(13)+
                    'IMPOSSIBILE CANCELLARE',mtError, [mbOK],0);
               Exit;
          end;
          // Contatti fatti
          Q.Close;
          Q.SQL.text:='select count(*) Tot from EBC_ContattiFatti '+
               'where IDCliente='+TElencoAziendeID.asString;
          Q.Open;
          if Q.FieldByName('Tot').asInteger>0 then begin
               MessageDlg('Esistono contatti avuti con l''azienda.'+chr(13)+
                    'IMPOSSIBILE CANCELLARE',mtError, [mbOK],0);
               Exit;
          end;

          if MessageDlg('Sei DAVVERO sicuro di voler cancellare l''Azienda ?',mtWarning, [mbYes,mbNo],0)=mrYes then begin
               with Data do begin
                    DB.BeginTrans;
                    try
                         Q.Close;
                         Q.SQL.text:='delete from EBC_Clienti where ID='+TElencoAziendeID.asString;
                         Q.ExecSQL;
                         DB.CommitTrans;
                    except
                         DB.RollbackTrans;
                         MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
                    end;
               end;
               EAziendaChange(self);
          end;
     end;
end;

procedure TEspLavForm.BAziendaOKClick(Sender: TObject);
begin
     TElencoAziende.Post;
end;

procedure TEspLavForm.BAziendaCanClick(Sender: TObject);
begin
     TElencoAziende.Cancel;
end;

procedure TEspLavForm.BAziendaNewClick(Sender: TObject);
begin
     AziendaForm:=TAziendaForm.create(self);
     AziendaForm.xUltimaProv:=MainForm.xUltimaProv;
     AziendaForm.ShowModal;
     if AziendaForm.ModalResult=mrOK then begin
          Data.DB.BeginTrans;
          try
               Q.SQL.text:='insert into EBC_Clienti (Descrizione,TipoStrada,Indirizzo,NumCivico,Comune,Cap,Provincia,Telefono,Fax,SitoInternet,DescAttivitaAzienda,Fatturato,NumDipendenti,NazioneAbb,Stato,IDAttivita) '+
                    'values (:xDescrizione,:xTipoStrada,:xIndirizzo,:xNumCivico,:xComune,:xCap,:xProvincia,:xTelefono,:xFax,:xSitoInternet,:xDescAttivitaAzienda,:xFatturato,:xNumDipendenti,:xNazioneAbb,:xStato,:xIDAttivita)';
               Q.ParamByName('xDescrizione').asString:=AziendaForm.EDescrizione.Text;
               Q.ParamByName('xTipoStrada').asString:=AziendaForm.CBTipoStrada.Text;
               Q.ParamByName('xIndirizzo').asString:=AziendaForm.EIndirizzo.text;
               Q.ParamByName('xNumCivico').asString:=AziendaForm.ENumCivico.text;
               Q.ParamByName('xComune').asString:=AziendaForm.EComune.Text;
               Q.ParamByName('xCap').asString:=AziendaForm.ECap.Text;
               Q.ParamByName('xProvincia').asString:=AziendaForm.EProv.Text;
               Q.ParamByName('xTelefono').asString:=AziendaForm.ETelefono.Text;
               Q.ParamByName('xFax').asString:=AziendaForm.EFax.Text;
               Q.ParamByName('xSitoInternet').asString:=AziendaForm.ESitoInternet.Text;
               Q.ParamByName('xDescAttivitaAzienda').asString:=AziendaForm.EDescAttAzienda.Text;
               Q.ParamByName('xFatturato').asFloat:=AziendaForm.FEFatturato.Value;
               Q.ParamByName('xNumDipendenti').asInteger:=StrToIntDef(AziendaForm.FENumDip.Text,0);
               Q.ParamByName('xNazioneAbb').asString:=AziendaForm.ENazione.text;
               Q.ParamByName('xStato').asString:='azienda esterna';
               if AziendaForm.RG1.ItemIndex=0 then
                    Q.ParamByName('xIDAttivita').asInteger:=0
               else Q.ParamByName('xIDAttivita').asInteger:=AziendaForm.TSettoriID.Value;

               Q.ExecSQL;
               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
          end;
          EAzienda.Text:=AziendaForm.EDescrizione.Text;
          EAziendaChange(self);
     end;
     AziendaForm.Free;
end;

procedure TEspLavForm.BAziendaModClick(Sender: TObject);
begin
     AziendaForm:=TAziendaForm.create(self);
     AziendaForm.EDescrizione.Text:=TElencoAziendeDescrizione.Value;
     AziendaForm.CBTipoStrada.text:=TElencoAziendeTipoStrada.Value;
     AziendaForm.EIndirizzo.Text:=TElencoAziendeIndirizzo.Value;
     AziendaForm.ENumCivico.text:=TElencoAziendeNumCivico.Value;
     AziendaForm.ECap.Text:=TElencoAziendeCap.Value;
     AziendaForm.EComune.Text:=TElencoAziendeComune.Value;
     AziendaForm.EProv.Text:=TElencoAziendeProvincia.Value;
     AziendaForm.ENazione.Text:=TElencoAziendeNazioneAbb.Value;
     AziendaForm.ETelefono.Text:=TElencoAziendeTelefono.Value;
     AziendaForm.ESitoInternet.Text:=TElencoAziendeSitoInternet.Value;
     AziendaForm.EFax.Text:=TElencoAziendeFax.Value;
     AziendaForm.FEFatturato.Text:=TElencoAziendeFatturato.asString;
     AziendaForm.EDescAttAzienda.Text:=TElencoAziendeDescAttivitaAzienda.asString;
     AziendaForm.FENumDip.Text:=TElencoAziendeNumDipendenti.asString;
     if TElencoAziendeIDAttivita.asString<>'' then begin
          AziendaForm.TSettori.IndexName:='';
          AziendaForm.TSettori.FindKey([TElencoAziendeIDAttivita.Value]);
          AziendaForm.TSettori.IndexName:='IdxAttivita';
     end else AziendaForm.RG1.Itemindex:=0;
     AziendaForm.ShowModal;
     if AziendaForm.ModalResult=mrOK then begin

          with Data do begin
               DB.BeginTrans;
               try
                    Q.Close;
                    Q.SQL.text:='update EBC_Clienti set Descrizione=:xDescrizione,TipoStrada=:xTipoStrada,Indirizzo=:xIndirizzo,NumCivico=:xNumCivico, '+
                         'Comune=:xComune,Cap=:xCap,Provincia=:xProvincia,Fatturato=:xFatturato,Telefono=:xTelefono,Fax=:xFax,SitoInternet=:xSitoInternet, '+
                         'NumDipendenti=:xNumDipendenti,IDAttivita=:xIDAttivita,NazioneAbb=:xNazioneAbb,DescAttivitaAzienda=:xDescAttivitaAzienda '+
                         'where ID='+TElencoAziendeID.asString;
                    Q.ParamByName('xDescrizione').asString:=AziendaForm.EDescrizione.Text;
                    Q.ParamByName('xTipoStrada').asString:=AziendaForm.CBTipoStrada.Text;
                    Q.ParamByName('xIndirizzo').asString:=AziendaForm.EIndirizzo.text;
                    Q.ParamByName('xNumCivico').asString:=AziendaForm.ENumCivico.text;
                    Q.ParamByName('xComune').asString:=AziendaForm.EComune.Text;
                    Q.ParamByName('xCap').asString:=AziendaForm.ECap.Text;
                    Q.ParamByName('xProvincia').asString:=AziendaForm.EProv.Text;
                    Q.ParamByName('xTelefono').asString:=AziendaForm.ETelefono.Text;
                    Q.ParamByName('xFax').asString:=AziendaForm.EFax.Text;
                    Q.ParamByName('xSitoInternet').asString:=AziendaForm.ESitoInternet.Text;
                    Q.ParamByName('xDescAttivitaAzienda').asString:=AziendaForm.EDescAttAzienda.Text;
                    if AziendaForm.FEFatturato.Text<>'' then
                         Q.ParamByName('xFatturato').asFloat:=StrToFloat(AziendaForm.FEFatturato.Text)
                    else Q.ParamByName('xFatturato').asFloat:=0;
                    Q.ParamByName('xNumDipendenti').asInteger:=StrToIntDef(AziendaForm.FENumDip.Text,0);
                    if AziendaForm.RG1.ItemIndex=0 then
                         Q.ParamByName('xIDAttivita').asInteger:=0
                    else Q.ParamByName('xIDAttivita').asInteger:=AziendaForm.TSettoriID.Value;
                    Q.ParamByName('xNazioneAbb').asString:=AziendaForm.ENazione.Text;
                    Q.ExecSQL;
                    DB.CommitTrans;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               end;
          end;
          EAziendaChange(self);
     end;
     AziendaForm.Free;
end;

procedure TEspLavForm.Timer2Timer(Sender: TObject);
begin
     xStringa2:='';
end;

procedure TEspLavForm.RGOpzioneClick(Sender: TObject);
begin
     case RGOpzione.ItemIndex of
          0: begin
                    PanAziende.Visible:=False;
                    PanSettori.Visible:=True;
               end;
          1: begin
                    PanAziende.Visible:=True;
                    PanSettori.Visible:=False;
               end;
     end;
end;

procedure TEspLavForm.ToolbarButton971Click(Sender: TObject);
var xSettore: string;
begin
     if not MainForm.CheckProfile('77') then Exit;
     if InputQuery('Nuovo settore','nome settore:',xSettore) then
          TSettori.InsertRecord([xSettore]);
end;

procedure TEspLavForm.EAziendaChange(Sender: TObject);
begin
     TElencoAziende.Close;
     if CBTestoCont.Checked then
          TElencoAziende.ParamByName('xDesc').asString:='%'+EAzienda.text+'%'
     else TElencoAziende.ParamByName('xDesc').asString:=EAzienda.text+'%';
     TElencoAziende.Open;
end;

procedure TEspLavForm.FormShow(Sender: TObject);
begin
     Caption:='[M/05140] - '+Caption;
     if (Uppercase(copy(Data.GlobalNomeAzienda.Value,1,7))='FERRARI') or
        (Uppercase(copy(Data.GlobalNomeAzienda.Value,1,3))='EBC') then begin
        BSettoriITA.visible:=True;
        BSettoriENG.visible:=True;
        BAreeRuoliITA.visible:=True;
        BAreeRuoliENG.visible:=True;
     end;
end;

procedure TEspLavForm.CBTestoContClick(Sender: TObject);
begin
     if CBTestoCont.Checked then begin
          if EAzienda.text<>'' then begin
               TElencoAziende.Close;
               TElencoAziende.ParamByName('xDesc').asString:='%'+EAzienda.text+'%';
               TElencoAziende.Open;
          end;
     end else begin
          if EAzienda.text<>'' then
               EAziendaChange(nil);
     end;
end;

procedure TEspLavForm.BSettoriITAClick(Sender: TObject);
begin
     TSettori.Close;
     TSettori.SQL.text:='select * from EBC_Attivita order by Attivita';
     DBGrid4.Columns[0].FieldName:='Attivita';
     TSettori.Open;
end;

procedure TEspLavForm.BSettoriENGClick(Sender: TObject);
begin
     TSettori.Close;
     TSettori.SQL.text:='select * from EBC_Attivita order by Attivita_ENG';
     DBGrid4.Columns[0].FieldName:='Attivita_ENG';
     TSettori.Open;
end;

procedure TEspLavForm.BAreeRuoliITAClick(Sender: TObject);
begin
     DBGrid2.Columns[0].FieldName:='Descrizione';
     DBGrid3.Columns[0].FieldName:='Descrizione';
end;

procedure TEspLavForm.BAreeRuoliENGClick(Sender: TObject);
begin
     DBGrid2.Columns[0].FieldName:='Descrizione_ENG';
     DBGrid3.Columns[0].FieldName:='Descrizione_ENG';
end;

end.

