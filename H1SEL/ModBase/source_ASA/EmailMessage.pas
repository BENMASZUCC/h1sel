unit EmailMessage;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls;

type
  TEmailMessageForm = class(TForm)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Panel15: TPanel;
    Memo1: TMemo;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EmailMessageForm: TEmailMessageForm;

implementation

{$R *.DFM}

procedure TEmailMessageForm.FormShow(Sender: TObject);
begin
     Caption:='[S/25] - '+Caption;
end;

end.
