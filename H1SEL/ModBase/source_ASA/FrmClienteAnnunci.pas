unit FrmClienteAnnunci;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     TB97,ExtCtrls,Grids,DBGrids,RXDBCtrl,Db,DBTables;

type
     TClienteAnnunciFrame=class(TFrame)
          PanTitoloCompensi: TPanel;
          RxDBGrid1: TRxDBGrid;
          Splitter1: TSplitter;
          Panel2: TPanel;
          ToolbarButton972: TToolbarButton97;
          QAnnunciCli: TQuery;
          DsQAnnunciCli: TDataSource;
          QListinoCliente: TQuery;
          DsQListinoCliente: TDataSource;
          QAnnunciCliData: TDateTimeField;
          QAnnunciCliNumModuli: TIntegerField;
          QAnnunciCliNomeEdizione: TStringField;
          QAnnunciCliTestata: TStringField;
          QAnnunciCliNominativo: TStringField;
          ToolbarButton971: TToolbarButton97;
          QAnnunciCliID: TAutoIncField;
          QListinoClienteID: TAutoIncField;
          QListinoClienteIDEdizione: TIntegerField;
          QListinoClienteDallaData: TDateTimeField;
          QListinoClientePrezzoListinoLire: TFloatField;
          QListinoClientePrezzoANoiLire: TFloatField;
          QListinoClientePrezzoAlClienteLire: TFloatField;
          QListinoClientePrezzoListinoEuro: TFloatField;
          QListinoClientePrezzoANoiEuro: TFloatField;
          QListinoClientePrezzoAlClienteEuro: TFloatField;
          QListinoClienteNote: TStringField;
          QListinoClienteQtaDa: TSmallintField;
          QListinoClienteQtaA: TSmallintField;
          QListinoClienteIDCliente: TIntegerField;
          QListinoClienteNomeEdizione: TStringField;
          QListinoClienteTestata: TStringField;
          QListinoClienteTestataEdizione: TStringField;
          QAnnunciCliIDAnnuncio: TIntegerField;
          QListinoClientePrezzoQuestoClienteLire: TFloatField;
          QListinoClientePrezzoQuestoClienteEuro: TFloatField;
          QListinoClienteIDCliente_1: TIntegerField;
          QListinoClienteID_1: TAutoIncField;
          PanListini: TPanel;
          Panel1: TPanel;
          Panel3: TPanel;
          ToolbarButton974: TToolbarButton97;
          RxDBGrid2: TRxDBGrid;
          procedure ToolbarButton972Click(Sender: TObject);
          procedure QListinoClienteCalcFields(DataSet: TDataSet);
          procedure ToolbarButton974Click(Sender: TObject);
     private
          { Private declarations }
     public
          xIDCliente: integer;
          xCliente: string;
     end;

implementation

uses StoricoPrezziAnnunciCli,PrezzoListino,ModuloDati,SelTestataEdizione,
     ModPrezzo;

{$R *.DFM}

procedure TClienteAnnunciFrame.ToolbarButton972Click(Sender: TObject);
begin
     StoricoPrezziAnnunciCliForm:=TStoricoPrezziAnnunciCliForm.create(self);
     StoricoPrezziAnnunciCliForm.xIDCliente:=xIDCliente;
     StoricoPrezziAnnunciCliForm.ECliente.text:=xCliente;
     StoricoPrezziAnnunciCliForm.ShowModal;
     StoricoPrezziAnnunciCliForm.Free;
end;

procedure TClienteAnnunciFrame.QListinoClienteCalcFields(DataSet: TDataSet);
begin
     QListinoClienteTestataEdizione.value:=QListinoClienteTestata.value+' '+QListinoClienteNomeEdizione.value;
end;

procedure TClienteAnnunciFrame.ToolbarButton974Click(Sender: TObject);
var xIDClienteListino: integer;
begin
     if QListinoCliente.RecordCount=0 then exit;
     ModPrezzoForm:=TModPrezzoForm.create(self);
     if QListinoClientePrezzoQuestoClienteLire.asString<>'' then begin
          ModPrezzoForm.PrezzoLire.value:=QListinoClientePrezzoQuestoClienteLire.value;
          ModPrezzoForm.PrezzoEuro.value:=QListinoClientePrezzoQuestoClienteEuro.value;
     end;
     ModPrezzoForm.ShowModal;
     if ModPrezzoForm.ModalResult=mrOK then begin
          if QListinoClienteIDCliente_1.asString='' then begin
               with Data do begin
                    DB.BeginTrans;
                    try
                         Q1.Close;
                         Q1.SQL.text:='insert into Ann_ListinoClienti (IDEdizListino,IDCliente,PrezzoQuestoClienteLire,PrezzoQuestoClienteEuro) '+
                              ' values (:xIDEdizListino,:xIDCliente,:xPrezzoQuestoClienteLire,:xPrezzoQuestoClienteEuro)';
                         Q1.ParamByName('xIDEdizListino').asInteger:=QListinoClienteID.Value;
                         Q1.ParamByName('xIDCliente').asInteger:=xIDCliente;
                         Q1.ParamByName('xPrezzoQuestoClienteLire').asInteger:=round(ModPrezzoForm.PrezzoLire.value);
                         Q1.ParamByName('xPrezzoQuestoClienteEuro').asFloat:=ModPrezzoForm.PrezzoEuro.value;
                         Q1.ExecSQL;
                         DB.CommitTrans;
                         QListinoCliente.Close;
                         QListinoCliente.Open;
                    except
                         DB.RollbackTrans;
                         MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
                    end;
               end;
          end else begin
               with Data do begin
                    DB.BeginTrans;
                    try
                         Q1.Close;
                         Q1.SQL.text:='update Ann_ListinoClienti set PrezzoQuestoClienteLire=:xPrezzoQuestoClienteLire, '+
                              ' PrezzoQuestoClienteEuro=:xPrezzoQuestoClienteEuro '+
                              ' where ID='+QListinoClienteID_1.asString;
                         Q1.ParamByName('xPrezzoQuestoClienteLire').asInteger:=round(ModPrezzoForm.PrezzoLire.value);
                         Q1.ParamByName('xPrezzoQuestoClienteEuro').asFloat:=ModPrezzoForm.PrezzoEuro.value;
                         Q1.ExecSQL;
                         DB.CommitTrans;
                         QListinoCliente.Close;
                         QListinoCliente.Open;
                    except
                         DB.RollbackTrans;
                         MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
                    end;
               end;
          end;
     end;
     ModPrezzoForm.Free;

     // record da inserire

  // VECCHIO CODICE
  {if QListinoCliente.RecordCount=0 then exit;
  PrezzoListinoForm:=TPrezzoListinoForm.create(self);
  PrezzoListinoForm.DEDallaData.Date:=QListinoClienteDallaData.Value;
  PrezzoListinoForm.ListinoLire.Value:=QListinoClientePrezzoListinoLire.Value;
  PrezzoListinoForm.AnoiLire.Value:=QListinoClientePrezzoANoiLire.Value;
  PrezzoListinoForm.AlClienteLire.Value:=QListinoClientePrezzoAlClienteLire.Value;
  PrezzoListinoForm.ListinoEuro.Value:=QListinoClientePrezzoListinoEuro.value;
  PrezzoListinoForm.AnoiEuro.Value:=QListinoClientePrezzoANoiEuro.Value;
  PrezzoListinoForm.AlClienteEuro.Value:=QListinoClientePrezzoAlClienteEuro.Value;
  PrezzoListinoForm.ENote.text:=QListinoClienteNote.Value;
  PrezzoListinoForm.RxSpinEdit1.value:=QListinoClienteQtaDa.Value;
  PrezzoListinoForm.RxSpinEdit2.value:=QListinoClienteQtaA.Value;
  xIDClienteListino:=xIDCliente;
  PrezzoListinoForm.ShowModal;
  if PrezzoListinoForm.ModalResult=mrOK then begin
       with Data do begin
            DB.BeginTrans;
            try
                 Q1.Close;
                 Q1.SQL.text:='update Ann_EdizListino set DallaData=:xDallaData,PrezzoListinoLire=:xPrezzoListinoLire,PrezzoANoiLire=:xPrezzoANoiLire,'+
                      'PrezzoAlClienteLire=:xPrezzoAlClienteLire,PrezzoListinoEuro=:xPrezzoListinoEuro, '+
                      'PrezzoANoiEuro=:xPrezzoANoiEuro,PrezzoAlClienteEuro=:xPrezzoAlClienteEuro,Note=:xNote,QtaDa=:XQtaDa,QtaA=:xQtaA,IDCliente=:xIDCliente '+
                      'where ID='+QListinoClienteID.asString;
                 Q1.ParamByName('xDallaData').asDateTime:=PrezzoListinoForm.DEDallaData.Date;
                 Q1.ParamByName('xPrezzoListinoLire').AsFloat:=PrezzoListinoForm.ListinoLire.Value;
                 Q1.ParamByName('xPrezzoANoiLire').AsFloat:=PrezzoListinoForm.AnoiLire.Value;
                 Q1.ParamByName('xPrezzoAlClienteLire').AsFloat:=PrezzoListinoForm.AlClienteLire.Value;
                 Q1.ParamByName('xPrezzoListinoEuro').AsFloat:=PrezzoListinoForm.ListinoEuro.Value;
                 Q1.ParamByName('xPrezzoANoiEuro').AsFloat:=PrezzoListinoForm.AnoiEuro.Value;
                 Q1.ParamByName('xPrezzoAlClienteEuro').AsFloat:=PrezzoListinoForm.AlClienteEuro.Value;
                 Q1.ParamByName('xNote').asString:=PrezzoListinoForm.ENote.text;
                 Q1.ParamByName('xQtaDa').asInteger:=Round(PrezzoListinoForm.RxSpinEdit1.value);
                 Q1.ParamByName('xQtaA').asInteger:=Round(PrezzoListinoForm.RxSpinEdit2.value);
                 Q1.ParamByName('xIDCliente').asInteger:=xIDClienteListino;
                 Q1.ExecSQL;
                 DB.CommitTrans;
                 QListinoCliente.Close;
                 QListinoCliente.Open;
            except
                 DB.RollbackTrans;
                 MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
            end;
       end;
  end;
  PrezzoListinoForm.free;}
end;

end.

