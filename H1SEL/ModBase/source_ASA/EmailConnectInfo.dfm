object EmailConnectInfoForm: TEmailConnectInfoForm
  Left = 562
  Top = 222
  ActiveControl = EHost
  BorderStyle = bsDialog
  Caption = 'Parametri di configurazione e-mail'
  ClientHeight = 268
  ClientWidth = 428
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 15
    Top = 47
    Width = 99
    Height = 13
    Caption = 'Host (server SMTP): '
  end
  object Label2: TLabel
    Left = 260
    Top = 47
    Width = 25
    Height = 13
    Caption = 'Port: '
  end
  object Label4: TLabel
    Left = 4
    Top = 71
    Width = 110
    Height = 13
    Caption = 'User ID (nome utente): '
  end
  object Label3: TLabel
    Left = 40
    Top = 103
    Width = 71
    Height = 13
    Caption = 'Nome mittente:'
  end
  object Bevel1: TBevel
    Left = 116
    Top = 43
    Width = 4
    Height = 220
    Shape = bsLeftLine
  end
  object Label6: TLabel
    Left = 29
    Top = 127
    Width = 84
    Height = 13
    Caption = 'Indirizzo mittente: '
  end
  object Label7: TLabel
    Left = 32
    Top = 157
    Width = 77
    Height = 13
    Caption = 'Firma personale:'
  end
  object BitBtn1: TBitBtn
    Left = 332
    Top = 3
    Width = 93
    Height = 32
    Anchors = [akTop, akRight]
    TabOrder = 5
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 332
    Top = 37
    Width = 93
    Height = 32
    Anchors = [akTop, akRight]
    Caption = 'Annulla'
    TabOrder = 6
    Kind = bkCancel
  end
  object EHost: TEdit
    Left = 124
    Top = 43
    Width = 130
    Height = 21
    TabOrder = 0
    Text = '192.168.0.155'
  end
  object EPort: TEdit
    Left = 288
    Top = 43
    Width = 36
    Height = 21
    TabOrder = 1
    Text = '25'
  end
  object EUserID: TEdit
    Left = 124
    Top = 67
    Width = 200
    Height = 21
    TabOrder = 2
    Text = 'sabattin#ebcconsulting.com'
  end
  object EName: TEdit
    Left = 123
    Top = 99
    Width = 126
    Height = 21
    TabOrder = 3
    Text = 'Andrea S'
  end
  object EAddress: TEdit
    Left = 123
    Top = 123
    Width = 199
    Height = 21
    TabOrder = 4
    Text = 'sabattini@ebcconsulting.com'
  end
  object Panel1: TPanel
    Left = 3
    Top = 4
    Width = 323
    Height = 31
    Anchors = [akLeft, akTop, akRight]
    BevelOuter = bvLowered
    Enabled = False
    TabOrder = 7
    object Label5: TLabel
      Left = 8
      Top = 9
      Width = 35
      Height = 13
      Caption = 'Utente:'
    end
    object EUtente: TEdit
      Left = 48
      Top = 5
      Width = 266
      Height = 21
      Color = clAqua
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      Text = 'EUtente'
    end
  end
  object Memo1: TMemo
    Left = 123
    Top = 155
    Width = 199
    Height = 109
    Lines.Strings = (
      'Memo1')
    TabOrder = 8
  end
  object Q: TQuery
    DatabaseName = 'EBCDB'
    Left = 283
    Top = 12
  end
end
