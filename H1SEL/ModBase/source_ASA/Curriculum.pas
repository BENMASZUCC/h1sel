 unit Curriculum;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     StdCtrls,DBCtrls,Buttons,Mask,ComCtrls,ExtCtrls,TB97,DBCGrids,DB,
     Wall,checklst,DtEdit97,DtEdDB97,DBTables,Grids,DBGrids,ComObj,RXCtrls,
     AnagConoscInfo,FrmMultiDBCheckBox,AnagraficaExt;

type
     TCurriculumForm=class(TForm)
          PageControl1: TPageControl;
          TSCVAltriDati: TTabSheet;
          TSCVTitoliStudio: TTabSheet;
          TSCVCorsiStage: TTabSheet;
          TSCVLingue: TTabSheet;
          TSCVespLav: TTabSheet;
          GroupBox1: TGroupBox;
          DBCheckBox1: TDBCheckBox;
          Label1: TLabel;
          DBEdit4: TDBEdit;
          DBCheckBox2: TDBCheckBox;
          GroupBox2: TGroupBox;
          Label2: TLabel;
          Label3: TLabel;
          DBEdit7: TDBEdit;
          Label4: TLabel;
          DBEdit8: TDBEdit;
          Label5: TLabel;
          DBEdit9: TDBEdit;
          DBComboBox1: TDBComboBox;
          DBCtrlGrid1: TDBCtrlGrid;
          Panel9: TPanel;
          TbBTitoliNew: TToolbarButton97;
          TbBTitoliDel: TToolbarButton97;
          TbBTitoliOK: TToolbarButton97;
          TbBTitoliCan: TToolbarButton97;
          Label9: TLabel;
          DBEdit6: TDBEdit;
          Label10: TLabel;
          DBCheckBox3: TDBCheckBox;
          Label12: TLabel;
          DBEdit13: TDBEdit;
          Label16: TLabel;
          DBEdit15: TDBEdit;
          DBCtrlGrid2: TDBCtrlGrid;
          Label13: TLabel;
          DBComboBox4: TDBComboBox;
          Label17: TLabel;
          DBEdit17: TDBEdit;
          Label18: TLabel;
          DBMemo1: TDBMemo;
          DBEdit18: TDBEdit;
          Label20: TLabel;
          DBEdit19: TDBEdit;
          Label22: TLabel;
          Label21: TLabel;
          DBEdit20: TDBEdit;
          Panel2: TPanel;
          TbBCorsiExtraNew: TToolbarButton97;
          TbBCorsiExtraDel: TToolbarButton97;
          TbBCorsiExtraOK: TToolbarButton97;
          TbBCorsiExtraCan: TToolbarButton97;
          ToolbarButton972: TToolbarButton97;
          Wallpaper1: TWallpaper;
          ToolbarButton973: TToolbarButton97;
          Label15: TLabel;
          Label23: TLabel;
          DBCtrlGrid3: TDBCtrlGrid;
          Panel1: TPanel;
          TbBLinConNew: TToolbarButton97;
          TbBLinConDel: TToolbarButton97;
          TbBLinConMod: TToolbarButton97;
          Label30: TLabel;
          Label24: TLabel;
          DBEdit16: TDBEdit;
          Label25: TLabel;
          Label26: TLabel;
          DBEdit22: TDBEdit;
          DBCtrlGrid4: TDBCtrlGrid;
          Panel3: TPanel;
          TbBEspLavDel: TToolbarButton97;
          TbBEspLavMod: TToolbarButton97;
          ToolbarButton9710: TToolbarButton97;
          Label34: TLabel;
          GroupBox3: TGroupBox;
          Label27: TLabel;
          DBEdit23: TDBEdit;
          Label28: TLabel;
          DBEdit24: TDBEdit;
          Label29: TLabel;
          DBEdit25: TDBEdit;
          Label31: TLabel;
          DBEdit26: TDBEdit;
          Label33: TLabel;
          DBEdit28: TDBEdit;
          Label35: TLabel;
          DBEdit29: TDBEdit;
          Label36: TLabel;
          Label38: TLabel;
          DBEdit32: TDBEdit;
          DBEdit33: TDBEdit;
          DBCTipoContratto: TDBComboBox;
          LTipoContratto: TLabel;
          LLivelloContr: TLabel;
          DBELivelloContr: TDBEdit;
          Label42: TLabel;
          DBEdit35: TDBEdit;
          PanCatProtette: TPanel;
          Label6: TLabel;
          Label7: TLabel;
          Label8: TLabel;
          DBEdit11: TDBEdit;
          DBComboBox2: TDBComboBox;
          DBComboBox3: TDBComboBox;
          Label47: TLabel;
          DBLookupComboBox2: TDBLookupComboBox;
          Label11: TLabel;
          GroupBox4: TGroupBox;
          Label45: TLabel;
          DBEdit38: TDBEdit;
          Label44: TLabel;
          DBEMensilita: TDBEdit;
          LMensilita: TLabel;
          GroupBox5: TGroupBox;
          Label51: TLabel;
          Label53: TLabel;
          Label56: TLabel;
          DBEdit41: TDBEdit;
          ToolbarButton971: TToolbarButton97;
          Label14: TLabel;
          DBEdit14: TDBEdit;
          DbDateEdit971: TDbDateEdit97;
          Panel7: TPanel;
          DBEdit1: TDBEdit;
          DBEdit2: TDBEdit;
          DBEdit3: TDBEdit;
          DBEdit5: TDBEdit;
          TbBEspLavNew: TToolbarButton97;
          TAnagMansioni: TTable;
          TAnagMansioniID: TAutoIncField;
          TAnagMansioniIDAnagrafica: TIntegerField;
          TAnagMansioniIDMansione: TIntegerField;
          TAnagMansioniNote: TMemoField;
          TAnagMansioniAnniEsperienza: TSmallintField;
          TAnagMansioniDisponibile: TBooleanField;
          TMansioniLK: TTable;
          TMansioniLKID: TAutoIncField;
          TMansioniLKDescrizione: TStringField;
          TAnagMansioniMansione: TStringField;
          DBEdit27: TDBEdit;
          GroupBox6: TGroupBox;
          DBCheckBox4: TDBCheckBox;
          DBCheckBox5: TDBCheckBox;
          DBCheckBox6: TDBCheckBox;
          DBCheckBox7: TDBCheckBox;
          ToolbarButton974: TToolbarButton97;
          Label43: TLabel;
          DBCheckBox8: TDBCheckBox;
          ToolbarButton975: TToolbarButton97;
          TAnagMansioniIDArea: TIntegerField;
          dsAnagAltriDati: TDataSource;
          TDispMovLK: TTable;
          TDispMovLKID: TAutoIncField;
          TDispMovLKDescrizione: TStringField;
          DBCheckBox9: TDBCheckBox;
          Label54: TLabel;
          DBEdit40: TDBEdit;
          Label58: TLabel;
          DBEdit42: TDBEdit;
          Label59: TLabel;
          DBEdit43: TDBEdit;
          GroupBox7: TGroupBox;
          SpeedButton1: TSpeedButton;
          RGProprieta: TRadioGroup;
          BAzienda: TSpeedButton;
          TAnagAltriDati: TQuery;
          TAnagAltriDatiIDAnagrafica: TIntegerField;
          TAnagAltriDatiFiguraChiave: TBooleanField;
          TAnagAltriDatiAssunzioneObbligatoria: TBooleanField;
          TAnagAltriDatiIdoneoAffiancamento: TBooleanField;
          TAnagAltriDatiCateogorieProtette: TStringField;
          TAnagAltriDatiInvalidita: TStringField;
          TAnagAltriDatiPercentualeInvalidita: TIntegerField;
          TAnagAltriDatiPatente: TBooleanField;
          TAnagAltriDatiTipoPatente: TStringField;
          TAnagAltriDatiDispAuto: TBooleanField;
          TAnagAltriDatiServizioMilitareStato: TStringField;
          TAnagAltriDatiServizioMilitareDove: TStringField;
          TAnagAltriDatiServizioMilitareQuando: TStringField;
          TAnagAltriDatiServizioMilitareGrado: TStringField;
          TAnagAltriDatiTipoContratto: TStringField;
          TAnagAltriDatiQualificaContrattuale: TStringField;
          TAnagAltriDatiDataAssunzione: TDateTimeField;
          TAnagAltriDatiDataScadContratto: TDateTimeField;
          TAnagAltriDatiDataUscita: TDateTimeField;
          TAnagAltriDatiDataFineProva: TDateTimeField;
          TAnagAltriDatiDataProssScatto: TDateTimeField;
          TAnagAltriDatiDataPensione: TDateTimeField;
          TAnagAltriDatiIDStabilimento: TIntegerField;
          TAnagAltriDatiPosizioneIdeale: TStringField;
          TAnagAltriDatiOrarioIdeale: TStringField;
          TAnagAltriDatiIDDispMov: TIntegerField;
          TAnagAltriDatiDisponibContratti: TStringField;
          TAnagAltriDatiCVProvenienza: TStringField;
          TAnagAltriDatiCVFile: TStringField;
          TAnagAltriDatiAbilitazProfessioni: TStringField;
          TAnagAltriDatiNote: TMemoField;
          TAnagAltriDatiMobilita: TBooleanField;
          TAnagAltriDatiCassaIntegrazione: TBooleanField;
          TAnagAltriDatiDispPartTime: TBooleanField;
          TAnagAltriDatiDispInterinale: TBooleanField;
          TAnagAltriDatiProvInteresse: TStringField;
          TAnagAltriDatiInquadramento: TStringField;
          TAnagAltriDatiRetribuzione: TStringField;
          TAnagAltriDatiDispMovimento: TStringField;
          UpdAnagAltreInfo: TUpdateSQL;
          DBEdit21: TDBEdit;
          DBEdit44: TDBEdit;
          DBCBProvCV: TDBComboBox;
          TAnagAltriDatiBenefits: TStringField;
          Label52: TLabel;
          DBEdit31: TDBEdit;
          BTabProvCV: TSpeedButton;
          Label60: TLabel;
          DBEdit45: TDBEdit;
          Label61: TLabel;
          TAnagAltriDatiNazionalita: TStringField;
          Label62: TLabel;
          DBEdit46: TDBEdit;
          TAnagAltriDatiIDTipoRetrib: TIntegerField;
          QTipiRetribLK: TQuery;
          QTipiRetribLKTipoRetrib: TStringField;
          QTipiRetribLKID: TAutoIncField;
          TAnagAltriDatiTipoRetrib: TStringField;
          DBLookupComboBox1: TDBLookupComboBox;
          SpeedButton3: TSpeedButton;
          LDataRealeCV: TLabel;
          DbDEDataRealeCV: TDbDateEdit97;
          DBCheckBox10: TDBCheckBox;
          TAnagAltriDatiTemporaryMG: TBooleanField;
          TAnagAltriDatiNoCV: TBooleanField;
          DbCbNoCV: TDBCheckBox;
          Label37: TLabel;
          DbDateEdit973: TDbDateEdit97;
          TAnagAltriDatiCassaIntegrStraord3: TBooleanField;
          TAnagAltriDatiCassaIntegrStraord24: TBooleanField;
          TAnagAltriDatiDisoccupato24: TBooleanField;
          TAnagAltriDatiDisoccupato12: TBooleanField;
          DBCheckBox11: TDBCheckBox;
          DBCheckBox12: TDBCheckBox;
          DBCheckBox13: TDBCheckBox;
          DBCheckBox14: TDBCheckBox;
          Label48: TLabel;
          Label49: TLabel;
          TAnagAltriDatiDispTraferteGiorn: TBooleanField;
          TAnagAltriDatiDispPernottamento: TBooleanField;
          TAnagAltriDatiDisptrasferimento: TBooleanField;
          DBCheckBox15: TDBCheckBox;
          DBCheckBox16: TDBCheckBox;
          DBCheckBox17: TDBCheckBox;
          DBEdit10: TDBEdit;
          DBText1: TDBText;
          TSCVConoscInfo: TTabSheet;
          FrmAnagConoscInfo1: TFrmAnagConoscInfo;
          DBEdit30: TDBEdit;
          Label19: TLabel;
          Label40: TLabel;
          DBEdit12: TDBEdit;
          BEspLavRetrib: TToolbarButton97;
          MultiDBCheckBoxFrame1: TMultiDBCheckBoxFrame;
          BTitoloITA: TSpeedButton;
          BTitoloENG: TSpeedButton;
          BLinguaITA: TSpeedButton;
          BLinguaENG: TSpeedButton;
          Label32: TLabel;
          DBEdit34: TDBEdit;
          PanEspProfMaturata: TPanel;
          TAnagAltriDatiIDEspProfMaturata: TIntegerField;
          QEspProfMatLK: TQuery;
          QEspProfMatLKID: TAutoIncField;
          QEspProfMatLKEspProfMaturata: TStringField;
          QEspProfMatLKEspProfMaturata_ENG: TStringField;
          TAnagAltriDatiEspProfMaturata: TStringField;
          Label41: TLabel;
          DBLookupComboBox3: TDBLookupComboBox;
          BEspProfmatITA: TSpeedButton;
          BEspProfmatENG: TSpeedButton;
          BEspLavITA: TSpeedButton;
          BEspLavENG: TSpeedButton;
          DBEdit36: TDBEdit;
          DBEdit37: TDBEdit;
          Label39: TLabel;
          TSCVCampiPers: TTabSheet;
          PanFerrari: TPanel;
          TAnagAltriDatiDomandePrec: TBooleanField;
          TAnagAltriDatiPartecSelezPrec: TBooleanField;
          TAnagAltriDatiPartecSelezQuando: TStringField;
          TAnagAltriDatiTipoSelezione: TStringField;
          TAnagAltriDatiParentiInAzienda: TStringField;
          DBCheckBox18: TDBCheckBox;
          DBCheckBox19: TDBCheckBox;
          Label46: TLabel;
          DBEdit39: TDBEdit;
          Label50: TLabel;
          DBEdit47: TDBEdit;
          Label55: TLabel;
          DBEdit48: TDBEdit;
          DBEdit49: TDBEdit;
          FrmSUITEXAnagraficaExt: TFrmSUITEXAnagraficaExt;
          procedure TbBTitoliNewClick(Sender: TObject);
          procedure TbBTitoliDelClick(Sender: TObject);
          procedure TbBTitoliOKClick(Sender: TObject);
          procedure TbBTitoliCanClick(Sender: TObject);
          procedure ToolbarButton972Click(Sender: TObject);
          procedure TbBCorsiExtraNewClick(Sender: TObject);
          procedure TbBCorsiExtraDelClick(Sender: TObject);
          procedure TbBCorsiExtraOKClick(Sender: TObject);
          procedure TbBCorsiExtraCanClick(Sender: TObject);
          procedure ToolbarButton973Click(Sender: TObject);
          procedure TbBLinConNewClick(Sender: TObject);
          procedure TbBLinConDelClick(Sender: TObject);
          procedure TbBLinConModClick(Sender: TObject);
          procedure TbBLinConCanClick(Sender: TObject);
          procedure TbBEspLavNewClick(Sender: TObject);
          procedure TbBEspLavDelClick(Sender: TObject);
          procedure TbBEspLavModClick(Sender: TObject);
          procedure ToolbarButton9710Click(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure DBLookupComboBox4Click(Sender: TObject);
          procedure ToolbarButton971Click(Sender: TObject);
          procedure CheckListBox1ClickCheck(Sender: TObject);
          procedure ToolbarButton974Click(Sender: TObject);
          procedure SpeedButton1Click(Sender: TObject);
          procedure ToolbarButton975Click(Sender: TObject);
          procedure PageControl1Changing(Sender: TObject;
               var AllowChange: Boolean);
          procedure PageControl1Change(Sender: TObject);
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
          procedure TAnagMansioniBeforeOpen(DataSet: TDataSet);
          procedure TAnagMansioniAfterClose(DataSet: TDataSet);
          procedure RGProprietaClick(Sender: TObject);
          procedure BAziendaClick(Sender: TObject);
          procedure TAnagAltriDatiAfterPost(DataSet: TDataSet);
          procedure BTabProvCVClick(Sender: TObject);
          procedure SpeedButton3Click(Sender: TObject);
          procedure BEspLavRetribClick(Sender: TObject);
          procedure BTitoloITAClick(Sender: TObject);
          procedure BTitoloENGClick(Sender: TObject);
          procedure BLinguaITAClick(Sender: TObject);
          procedure BLinguaENGClick(Sender: TObject);
          procedure BEspProfmatITAClick(Sender: TObject);
          procedure BEspProfmatENGClick(Sender: TObject);
          procedure BEspLavITAClick(Sender: TObject);
          procedure BEspLavENGClick(Sender: TObject);
     private
          xPaginaPrec: TTabsheet;
          { Private declarations }
     public
          { Public declarations }
          xModificata,xModPropCV: boolean;
          xIDAziendaProp: integer;
     end;

var
     CurriculumForm: TCurriculumForm;

implementation

uses ModuloDati,DettaglioTesi,Lingue,DettEspLav,SelArea,ModuloDati2,
     SelRuolo,SelDiploma,InsRuolo,View,CompInserite,EspLav,uUtilsVarie,
     CercaAnnuncio,Main,ElencoAziende,AnagAnnunci,CliAltriSettori,
     EspLavRetrib;

{$R *.DFM}

procedure TCurriculumForm.TbBTitoliNewClick(Sender: TObject);
begin
     if not MainForm.CheckProfile('05110') then Exit;
     SelDiplomaForm:=TSelDiplomaForm.create(self);
     SelDiplomaForm.ShowModal;
     if SelDiplomaForm.ModalResult=mrcancel then begin SelDiplomaForm.Free; exit; end;
     with Data.TTitoliStudio do begin
          Data.DB.BeginTrans;
          try
               Insert;
               Data.TTitoliStudioIDAnagrafica.value:=data.TAnagraficaID.value;
               Data.TTitoliStudioIDDiploma.value:=SelDiplomaForm.DiplomiID.value;
               //Data.TTitoliStudioTipoDiploma.value:=SelDiplomaForm.DiplomiTipo.Value;
               ApplyUpdates;
               //Data.Q1.Close;
               //Data.Q1.SQL.Text:='update TitoliStudio set TipoDiploma= '+
               //     '(select Tipo from Diplomi where ID=TitoliStudio.IDDiploma)';
               //Data.Q1.ExecSQL;

               Data.QTemp.SQL.text:='select @@IDENTITY as LastID';
               Data.QTemp.Open;
               Log_Operation(MainForm.xIDUtenteAttuale,'TitoliStudio',Data.QTemp.FieldByName('LastID').asInteger,'I');
               Data.QTemp.Close;

               Data.DB.CommitTrans;
               Data.TTitoliStudio.close;
               Data.TTitoliStudio.Open;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
          end;
          CommitUpdates;
     end;
     SelDiplomaForm.Free;
end;

procedure TCurriculumForm.TbBTitoliDelClick(Sender: TObject);
begin
     if not MainForm.CheckProfile('05111') then Exit;
     if not Data.TTitoliStudio.IsEmpty then
          if MessageDlg('Sei sicuro di voler eminimarlo',mtWarning,
               [mbNo,mbYes],0)=mrYes then Data.TTitoliStudio.Delete;
end;

procedure TCurriculumForm.TbBTitoliOKClick(Sender: TObject);
begin
     with Data.TTitoliStudio do begin
          Data.DB.BeginTrans;
          try
               ApplyUpdates;
               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
          end;
          CommitUpdates;
     end;
end;

procedure TCurriculumForm.TbBTitoliCanClick(Sender: TObject);
begin
     with Data.TTitoliStudio do begin
          Data.DB.BeginTrans;
          try
               CancelUpdates;
               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
          end;
          CommitUpdates;
     end;
end;

procedure TCurriculumForm.ToolbarButton972Click(Sender: TObject);
begin
     DettaglioTesiForm:=TDettaglioTesiForm.Create(self);
     DettaglioTesiForm.ShowModal;
     DettaglioTesiForm.Free;
end;

procedure TCurriculumForm.TbBCorsiExtraNewClick(Sender: TObject);
begin
     if not MainForm.CheckProfile('05120') then Exit;
     Data.TCorsiExtra.Insert;
     Data.TCorsiExtraIDAnagrafica.Value:=Data.TAnagraficaID.Value;
end;

procedure TCurriculumForm.TbBCorsiExtraDelClick(Sender: TObject);
begin
     if not MainForm.CheckProfile('05121') then Exit;
     if not Data.TCorsiExtra.EOF then
          if MessageDlg('Sei sicuro di voler eminimarlo',mtWarning,
               [mbNo,mbYes],0)=mrYes then Data.TCorsiExtra.Delete;
end;

procedure TCurriculumForm.TbBCorsiExtraOKClick(Sender: TObject);
begin
     with Data.TCorsiExtra do begin
          Data.DB.BeginTrans;
          try
               ApplyUpdates;
               Data.DB.CommitTrans;
               Close;
               Open;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
          end;
          CommitUpdates;
     end;
end;

procedure TCurriculumForm.TbBCorsiExtraCanClick(Sender: TObject);
begin
     with Data.TCorsiExtra do begin
          Data.DB.BeginTrans;
          try
               CancelUpdates;
               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
          end;
          CommitUpdates;
     end;
end;

procedure TCurriculumForm.ToolbarButton973Click(Sender: TObject);
begin
     close
end;

procedure TCurriculumForm.TbBLinConNewClick(Sender: TObject);
begin
     if not MainForm.CheckProfile('05130') then Exit;
     LingueForm:=TLingueForm.Create(self);
     LingueForm.ShowModal;
     if LingueForm.ModalResult=mrOK then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text:='insert into LingueConosciute (IDAnagrafica,IDLingua,Lingua,Livello,Soggiorno,LivelloScritto) '+
                         'values (:xIDAnagrafica,:xIDLingua,:xLingua,:xLivello,:xSoggiorno,:xLivelloScritto)';
                    Q1.ParamByName('xIDAnagrafica').asInteger:=Data.TAnagraficaID.Value;
                    Q1.ParamByName('xIDLingua').asInteger:=LingueForm.LingueID.Value;
                    Q1.ParamByName('xLingua').asString:=LingueForm.LingueLingua.Value;
                    Q1.ParamByName('xLivello').asInteger:=LingueForm.QLivLingueID.Value;
                    Q1.ParamByName('xSoggiorno').asString:=LingueForm.ESoggiorno.text;
                    Q1.ParamByName('xLivelloScritto').asInteger:=LingueForm.QLivLingue2ID.Value;
                    Q1.ExecSQL;

                    Data.QTemp.SQL.text:='select @@IDENTITY as LastID';
                    Data.QTemp.Open;
                    Log_Operation(MainForm.xIDUtenteAttuale,'LingueConosciute',Data.QTemp.FieldByName('LastID').asInteger,'I');
                    Data.QTemp.Close;

                    DB.CommitTrans;
                    TLingueConosc.close;
                    TLingueConosc.Open;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               end;
          end;
     end;
     LingueForm.Free;
end;

procedure TCurriculumForm.TbBLinConDelClick(Sender: TObject);
begin
     if not MainForm.CheckProfile('05131') then Exit;
     if Data.TLingueConosc.RecordCount>0 then
          if MessageDlg('Sei sicuro di voler eliminarlo ?',mtWarning,
               [mbNo,mbYes],0)=mrYes then begin
               with Data do begin
                    DB.BeginTrans;
                    try
                         Q1.Close;
                         Q1.SQL.text:='delete from LingueConosciute '+
                              'where ID='+TLingueConoscID.asString;
                         Q1.ExecSQL;

                         Log_Operation(MainForm.xIDUtenteAttuale,'LingueConosciute',Data.TLingueConoscID.value,'D');

                         DB.CommitTrans;
                         TLingueConosc.close;
                         TLingueConosc.Open;
                    except
                         DB.RollbackTrans;
                         MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
                    end;
               end;
          end;
end;

procedure TCurriculumForm.TbBLinConModClick(Sender: TObject);
begin
     if not MainForm.CheckProfile('05130') then Exit;
     LingueForm:=TLingueForm.Create(self);
     LingueForm.Lingue.FindKey([Data.TLingueConoscLingua.value]);
     LingueForm.ESoggiorno.text:=Data.TLingueConoscSoggiorno.Value;
     LingueForm.QLivLingue.locate('ID',Data.TLingueConoscLivello.Value, []);
     LingueForm.QLivLingue2.locate('ID',Data.TLingueConoscLivelloScritto.Value, []);
     LingueForm.ShowModal;
     if LingueForm.ModalResult=mrOK then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text:='update LingueConosciute set IDLingua=:xIDLingua, Lingua=:xLingua,Livello=:xLivello,LivelloScritto=:xLivelloScritto,Soggiorno=:xSoggiorno '+
                         'where ID='+Data.TLingueConoscID.asString;
                    Q1.ParamByName('xIDLingua').asInteger:=LingueForm.LingueID.Value;
                    Q1.ParamByName('xLingua').asString:=LingueForm.LingueLingua.Value;
                    Q1.ParamByName('xLivello').asInteger:=LingueForm.QLivLingueID.Value;
                    Q1.ParamByName('xLivelloScritto').asInteger:=LingueForm.QLivLingue2ID.Value;
                    Q1.ParamByName('xSoggiorno').asString:=LingueForm.ESoggiorno.text;
                    Q1.ExecSQL;

                    Log_Operation(MainForm.xIDUtenteAttuale,'LingueConosciute',Data.TLingueConoscID.value,'U');

                    DB.CommitTrans;
                    TLingueConosc.close;
                    TLingueConosc.Open;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               end;
          end;
     end;
     LingueForm.Free;
end;

procedure TCurriculumForm.TbBLinConCanClick(Sender: TObject);
begin
     with Data.TLingueConosc do begin
          Data.DB.BeginTrans;
          try
               CancelUpdates;
               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
          end;
          CommitUpdates;
     end;
end;

procedure TCurriculumForm.TbBEspLavNewClick(Sender: TObject);
var xIDAnag,xTotComp,i: integer;
     xComp: array[1..50] of integer;
begin
     if not MainForm.CheckProfile('05140') then Exit;
     xIDAnag:=Data.TanagraficaID.Value;
     EspLavForm:=TEspLavForm.create(self);
     EspLavForm.ShowModal;
     if EspLavForm.ModalResult=mrOK then begin
          Data.TEspLav.Insert;
          Data.TEspLavIDAnagrafica.Value:=Data.TAnagraficaID.Value;
          if EspLavForm.RGOpzione.Itemindex=0 then begin
               Data.TEspLavIDAzienda.Value:=0;
               Data.TEspLavAziendaNome.Value:='';
               Data.TEspLavIDSettore.Value:=EspLavForm.TSettoriID.Value;
          end;
          if EspLavForm.RGOpzione.Itemindex=1 then begin
               Data.TEspLavIDAzienda.Value:=EspLavForm.TElencoAziendeID.Value;
               Data.TEspLavAziendaNome.Value:=EspLavForm.TElencoAziendeDescrizione.Value;
               Data.TEspLavIDSettore.Value:=EspLavForm.TElencoAziendeIDAttivita.Value;
               // altri settori per l'azienda
               Data.QTemp.Close;
               Data.QTemp.SQL.text:='select count(*) Tot from EBC_ClientiAttivita where IDCliente='+EspLavForm.TElencoAziendeID.asString;
               Data.QTemp.Open;
               if Data.QTemp.FieldByName('Tot').asInteger>0 then begin
                    CliAltriSettoriForm:=TCliAltriSettoriForm.create(self);
                    CliAltriSettoriForm.BitBtn2.Visible:=True;
                    CliAltriSettoriForm.BitBtn1.Caption:='OK';
                    CaricaApriClienti(EspLavForm.TElencoAziendeDescrizione.Value,'');
                    CliAltriSettoriForm.Caption:='Altre attivit� (Annulla per tenere quella princ.)';
                    CliAltriSettoriForm.ShowModal;
                    if CliAltriSettoriForm.ModalResult=mrOK then
                         Data.TEspLavIDSettore.Value:=CliAltriSettoriForm.QCliSettoriIDAttivita.Value;
                    CliAltriSettoriForm.Free;
               end;
               Data.QTemp.Close;
          end;
          if EspLavForm.RGOpzione.Itemindex=2 then begin
               Data.TEspLavIDAzienda.Value:=0;
               Data.TEspLavAziendaNome.Value:='';
               Data.TEspLavIDSettore.Value:=0;
          end;
          Data.TEspLavIDMansione.Value:=EspLavForm.TRuoliID.Value;
          Data.TEspLavIDArea.Value:=EspLavForm.TAreeID.Value;
          Data.TEspLavAttuale.Value:=False;

          Data.DB.BeginTrans;
          try
               Data.TEspLav.ApplyUpdates;

               Data.QTemp.SQL.text:='select @@IDENTITY as LastID';
               Data.QTemp.Open;
               Log_Operation(MainForm.xIDUtenteAttuale,'EsperienzeLavorative',Data.QTemp.FieldByName('LastID').asInteger,'I');
               Data.QTemp.Close;

               Data2.TAnagMansIDX.Open;
               if not Data2.TAnagMansIDX.FindKey([Data.TAnagraficaID.value,EspLavForm.TRuoliID.Value]) then begin
                    // attribuisci il ruolo e le competenze
                    Data.Q1.SQL.text:='insert into AnagMansioni (IDAnagrafica,IDMansione,Disponibile,IDArea) '+
                         'values (:xIDAnagrafica,:xIDMansione,:xDisponibile,:xIDArea)';
                    Data.Q1.ParamByName('xIDAnagrafica').asInteger:=Data.TAnagraficaID.Value;
                    Data.Q1.ParamByName('xIDMansione').asInteger:=EspLavForm.TRuoliID.Value;
                    Data.Q1.ParamByName('xDisponibile').asBoolean:=True;
                    Data.Q1.ParamByName('xIDArea').asInteger:=EspLavForm.TAreeID.Value;
                    Data.Q1.ExecSQL;

                    if EspLavForm.CBInsComp.Checked then begin
                         // inserisci le competenze se mancanti
                         CompInseriteForm:=TCompInseriteForm.create(self);
                         CompInseriteForm.ERuolo.Text:=EspLavForm.TRuoliDescrizione.Value;
                         Data2.QCompRuolo.Close;
                         Data2.QCompRuolo.Prepare;
                         Data2.QCompRuolo.Params[0].asInteger:=Data.TEspLavIDMansione.Value;
                         Data2.QCompRuolo.Open;
                         Data2.QCompRuolo.First;
                         xTotComp:=0;
                         Data2.TAnagCompIDX.Open;
                         CompInseriteForm.SG1.RowCount:=2;
                         CompInseriteForm.SG1.RowHeights[0]:=16;
                         while not Data2.QCompRuolo.EOF do begin
                              if not Data2.TAnagCompIDX.FindKey([xIDAnag,Data2.QCompRuoloIDCompetenza.Value]) then begin
                                   CompInseriteForm.SG1.RowHeights[xTotComp+1]:=16;
                                   CompInseriteForm.SG1.Cells[0,xTotComp+1]:=Data2.QCompRuoloDescCompetenza.Value;
                                   CompInseriteForm.SG1.Cells[1,xTotComp+1]:='0';
                                   if Data.TAnagraficaIDTipoStato.Value<>2 then begin
                                        CompInseriteForm.SG1.Cells[2,xTotComp+1]:='�';
                                        CompInseriteForm.SG1.ColorRow[xTotComp+1]:=clLime;
                                   end else begin
                                        CompInseriteForm.SG1.Cells[2,xTotComp+1]:='x';
                                        CompInseriteForm.SG1.ColorRow[xTotComp+1]:=clRed;
                                   end;
                                   xComp[xTotComp+1]:=Data2.QCompRuoloIDCompetenza.Value;
                                   Inc(xTotComp);
                                   CompInseriteForm.SG1.RowCount:=CompInseriteForm.SG1.RowCount+1;
                              end;
                              Data2.QCompRuolo.Next;
                         end;
                         CompInseriteForm.SG1.RowCount:=CompInseriteForm.SG1.RowCount-1;
                         if xTotComp>0 then CompInseriteForm.ShowModal;
                         if CompInseriteForm.ModalResult=mrOK then begin
                              for i:=1 to CompInseriteForm.SG1.RowCount-1 do begin
                                   if CompInseriteForm.SG1.ColorRow[i]=clLime then begin
                                        Data.Q1.SQL.text:='insert into CompetenzeAnagrafica (IDAnagrafica,IDCompetenza,Valore) '+
                                             'values (:xIDAnagrafica,:xIDCompetenza,:xValore)';
                                        Data.Q1.ParamByName('xIDAnagrafica').asInteger:=xIDAnag;
                                        Data.Q1.ParamByName('xIDCompetenza').asInteger:=xComp[i];
                                        Data.Q1.ParamByName('xValore').asInteger:=StrToInt(CompInseriteForm.SG1.Cells[1,i]);
                                        Data.Q1.ExecSQL;
                                        // storico competenza
                                        Data.Q1.SQL.text:='insert into StoricoCompAnag (IDCompetenza,IDAnagrafica,DallaData,MotivoAumento,valore) '+
                                             'values (:xIDCompetenza,:xIDAnagrafica,:xDallaData,:xMotivoAumento,:xvalore)';
                                        Data.Q1.ParamByName('xIDCompetenza').asInteger:=xComp[i];
                                        Data.Q1.ParamByName('xIDAnagrafica').asInteger:=xIDAnag;
                                        Data.Q1.ParamByName('xDallaData').asDateTime:=Date;
                                        Data.Q1.ParamByName('xMotivoAumento').asString:='valore iniziale';
                                        Data.Q1.ParamByName('xValore').asInteger:=StrToInt(CompInseriteForm.SG1.Cells[1,i]);
                                        Data.Q1.ExecSQL;
                                   end;
                              end;
                         end;
                         Data2.TAnagCompIDX.Close;
                         CompInseriteForm.Free;
                    end;
               end;
               Data2.TAnagMansIDX.Close;

               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ATTENZIONE: errore sul database - Inserimento non avvenuto',mtError, [mbOK],0);
          end;
          Data.TEspLav.CommitUpdates;
          Data.TEspLav.Close;
          Data.TEspLav.Open;
     end;
     EspLavForm.Free;
end;

procedure TCurriculumForm.TbBEspLavDelClick(Sender: TObject);
begin
     if not MainForm.CheckProfile('05141') then Exit;
     if not Data.TEspLav.IsEmpty then
          if MessageDlg('Sei sicuro di voler eminimarlo',mtWarning,
               [mbNo,mbYes],0)=mrYes then begin
               with Data do begin
                    DB.BeginTrans;
                    try
                         Q1.Close;
                         Q1.SQL.text:='delete from EsperienzeLavorative '+
                              'where ID='+TEspLavID.asString;
                         Q1.ExecSQL;

                         Log_Operation(MainForm.xIDUtenteAttuale,'EsperienzeLavorative',TEspLavID.value,'D');

                         DB.CommitTrans;
                         TEspLav.Close;
                         TEspLav.Open;
                    except
                         DB.RollbackTrans;
                         MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
                    end;
               end;

          end;
end;

procedure TCurriculumForm.TbBEspLavModClick(Sender: TObject);
var xIDVecchioRuolo: integer;
     xIDAnag,xTotComp,i: integer;
     xComp: array[1..50] of integer;
begin
     if not MainForm.CheckProfile('05142') then Exit;
     if Data.TEspLav.RecordCount>0 then begin
          EspLavForm:=TEspLavForm.create(self);
          if Data.TEspLavIDAzienda.asString<>'' then begin
               EspLavForm.RGOpzione.ItemIndex:=1;
               EspLavForm.PanAziende.Visible:=True;
               EspLavForm.PanSettori.Visible:=False;
               EspLavForm.EAzienda.text:=Data.TEspLavAzDenominazione.Value;
               EspLavForm.EAziendaChange(self);
          end else begin
               EspLavForm.TSettori.Locate('ID',Data.TEspLavIDSettore.Value, []);
               EspLavForm.RGOpzione.ItemIndex:=0;
               EspLavForm.PanAziende.Visible:=False;
               EspLavForm.PanSettori.Visible:=True;
          end;
          EspLavForm.Height:=277;
          EspLavForm.ShowModal;
          if EspLavForm.ModalResult=mrOK then begin
               Data.TEspLav.Edit;
               if EspLavForm.RGOpzione.ItemIndex=0 then begin
                    Data.TEspLavIDSettore.Value:=EspLavForm.TSettoriID.Value;
                    Data.TEspLavAziendaNome.Value:='';
               end else begin
                    Data.TEspLavIDSettore.Value:=EspLavForm.TElencoAziendeIDAttivita.Value;
                    Data.TEspLavIDAzienda.Value:=EspLavForm.TElencoAziendeID.Value;
                    Data.TEspLavAziendaNome.Value:=EspLavForm.TElencoAziendeDescrizione.Value;
                    // altri settori per l'azienda
                    Data.QTemp.Close;
                    Data.QTemp.SQL.text:='select count(*) Tot from EBC_ClientiAttivita where IDCliente='+EspLavForm.TElencoAziendeID.asString;
                    Data.QTemp.Open;
                    if Data.QTemp.FieldByName('Tot').asInteger>0 then begin
                         CliAltriSettoriForm:=TCliAltriSettoriForm.create(self);
                         CliAltriSettoriForm.BitBtn2.Visible:=True;
                         CliAltriSettoriForm.BitBtn1.Caption:='OK';
                         CaricaApriClienti(EspLavForm.TElencoAziendeDescrizione.Value,'');
                         CliAltriSettoriForm.Caption:='Altre attivit� (Annulla per tenere quella princ.)';
                         CliAltriSettoriForm.ShowModal;
                         if CliAltriSettoriForm.ModalResult=mrOK then
                              Data.TEspLavIDSettore.Value:=CliAltriSettoriForm.QCliSettoriIDAttivita.Value;
                         CliAltriSettoriForm.Free;
                    end;
                    Data.QTemp.Close;
               end;
               Data.DB.BeginTrans;
               try
                    Data.TEspLav.ApplyUpdates;
                    Data.DB.CommitTrans;
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('ATTENZIONE: errore sul database - Inserimento non avvenuto',mtError, [mbOK],0);
               end;
               Data.TEspLav.CommitUpdates;
          end else begin
               if MessageDlg('Vuoi azzerare i dati sull''azienda ?',mtInformation, [mbYes,mbNo],0)=mrYes then begin
                    Data.TEspLav.Edit;
                    Data.TEspLavIDSettore.asString:='';
                    Data.TEspLavIDAzienda.asString:='';
                    Data.TEspLavAziendaNome.asString:='';
                    Data.DB.BeginTrans;
                    try
                         Data.TEspLav.ApplyUpdates;
                         Data.DB.CommitTrans;
                    except
                         Data.DB.RollbackTrans;
                         MessageDlg('ATTENZIONE: errore sul database - Inserimento non avvenuto',mtError, [mbOK],0);
                    end;
                    Data.TEspLav.CommitUpdates;
               end;
          end;
          Data.TEspLav.Close;
          Data.TEspLav.Open;
          EspLavForm.Free;
     end;
end;

procedure TCurriculumForm.ToolbarButton9710Click(Sender: TObject);
begin
     DettEspLavForm:=TDettEspLavForm.Create(self);
     DettEspLavForm.ShowModal;
     if DettEspLavForm.ModalResult=mrOK then begin
          Data.DB.BeginTrans;
          try
               Data.TEspLav.ApplyUpdates;
               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ATTENZIONE: errore sul database - Inserimento non avvenuto',mtError, [mbOK],0);
          end;
          Data.TEspLav.CommitUpdates;
     end;
     DettEspLavForm.Free;
end;

procedure TCurriculumForm.FormShow(Sender: TObject);
var i: integer;
     x: string;
begin
     Caption:='[M/051] - curriculum vitae';
     PageControl1.ActivePage:=TSCVAltriDati;
     if not mainForm.CheckProfile('0510') then TSCVAltriDati.Visible:=False
     else TSCVAltriDati.Visible:=true;
     xModificata:=False;
     TAnagAltriDati.Open;
     DBCtrlGrid4.Height:=335;
     if Data.TAnagraficaSesso.Value='F' then CurriculumForm.GroupBox2.Visible:=False
     else CurriculumForm.GroupBox2.Visible:=True;
     if (Data.TAnagraficaIDProprietaCV.value=0)or(Data.TAnagraficaIDProprietaCV.asString='') then begin
          RGProprieta.ItemIndex:=0;
          BAzienda.Visible:=False;
          xIDAziendaProp:=0;
     end else begin
          RGProprieta.ItemIndex:=1;
          BAzienda.Visible:=True;
          xIDAziendaProp:=Data.TAnagraficaIDProprietaCV.value;
     end;
     xModPropCV:=False;
     // caricamento DBCBProvCV con le provenienze da tabella
     Data.Q1.Close;
     Data.Q1.SQL.text:='select Provenienza from ProvenienzeCV';
     Data.Q1.Open;
     DBCBProvCV.items.Clear;
     while not Data.Q1.EOF do begin
          DBCBProvCV.items.Add(Data.Q1.FieldByName('Provenienza').asString);
          Data.Q1.Next;
     end;
     Data.Q1.Close;

     // impostazioni generalizzate
     PageControl1.Pages[5].TabVisible:=False;
     PageControl1.Pages[6].TabVisible:=False;
     PanCatProtette.Visible:=False;

     // Categorie protette invisibile per TUTTI tranne che per ZUCCHETTI
     if Uppercase(copy(Data.GlobalNomeAzienda.Value,1,9))='ZUCCHETTI' then begin
          PageControl1.Pages[5].TabVisible:=True;
          PanCatProtette.Visible:=True;
     end;

     if Uppercase(copy(Data.GlobalNomeAzienda.Value,1,6))='LEDERM' then begin
          GroupBox1.Visible:=False;
          GroupBox6.Visible:=False;
          GroupBox2.Visible:=False;
          DBCheckBox9.Visible:=False;
          MultiDBCheckBoxFrame1.Visible:=False;
          Label54.Visible:=False;
          DBEdit40.Visible:=False;
          Label58.Visible:=False;
          DBEdit42.Visible:=False;
          Label45.Visible:=False;
          DBEdit38.Visible:=False;
     end;
     if Uppercase(copy(Data.GlobalNomeAzienda.Value,1,6))='BOYDEN' then begin
          DbCbNoCV.Visible:=True;
          DbDEDataRealeCV.Visible:=False;
          LDataRealeCV.Visible:=False;
     end;
     if Uppercase(copy(Data.GlobalNomeAzienda.Value,1,6))='ADVANT' then begin
          DBCtrlGrid4.Height:=395;
          LTipoContratto.Visible:=False;
          DBCTipoContratto.Visible:=False;
          LLivelloContr.Visible:=False;
          DBELivelloContr.Visible:=False;
          LMensilita.Visible:=False;
          DBEMensilita.Visible:=False;
     end;

     if Uppercase(copy(Data.GlobalNomeAzienda.Value,1,7))='FERRARI' then begin
          PageControl1.Pages[5].TabVisible:=True;
          PageControl1.Pages[6].TabVisible:=True;
          PanEspProfMaturata.Visible:=True;
          PanFerrari.Visible:=True;
          BTitoloITA.Visible:=True;
          BTitoloENG.Visible:=True;
          BLinguaITA.Visible:=True;
          BLinguaENG.Visible:=True;
          BEspLavITA.Visible:=True;
          BEspLavENG.Visible:=True;
          DBEdit23.DataField:='AziendaNome';
          DBEdit25.DataField:='AziendaSede';
          Label29.caption:='Sede';
     end;
     // ### PER TEST: le impostazioni per FERRARI anche per EBC
     if Uppercase(copy(Data.GlobalNomeAzienda.Value,1,3))='EBC' then begin
          PageControl1.Pages[5].TabVisible:=True;
          PageControl1.Pages[6].TabVisible:=True;
          PanEspProfMaturata.Visible:=True;
          BTitoloITA.Visible:=True;
          BTitoloENG.Visible:=True;
          BLinguaITA.Visible:=True;
          BLinguaENG.Visible:=True;
          BEspLavITA.Visible:=True;
          BEspLavENG.Visible:=True;
     end;

     if Uppercase(copy(Data.GlobalNomeAzienda.Value,1,6))='SUITEX' then begin
          PageControl1.Pages[6].TabVisible:=True;
          FrmSUITEXAnagraficaExt.Visible:=True;
     end;

     // nuovo MultiDBCheckbox
     with MultiDBCheckBoxFrame1 do begin
          Pantitolo.Caption:=' Disponibilit� nei confronti delle tipologie di contratto';
          xIDAnag:=Data.TAnagraficaID.Value;
          xTabBase:='DispContratti';
          xTabCorrelaz:='AnagDispContratti';
          xCampoVis:='DispContratto';
          xForeignKey:='IDDispContratto';
          LoadCLBox;
     end;
end;

procedure TCurriculumForm.DBLookupComboBox4Click(Sender: TObject);
begin
     // selezione area e filtro
     SelAreaForm:=TSelAreaForm.create(self);
     SelAreaForm.ShowModal;
     if SelAreaForm.ModalResult=mrOK then begin
          Data.TMansioniLK.Filtered:=True;
          Data.TMansioniLK.Filter:='IDArea='+Data2.TAreeID.asString;
     end;
     SelAreaForm.Free;

     Data.DB.BeginTrans;
     try
          if Data.Q1.Active then Data.Q1.Close;
          Data.Q1.SQL.text:='update EsperienzeLavorative set IDArea='+Data2.TAreeID.asString+
               ' where ID='+Data.TEspLavID.asString;
          Data.Q1.ExecSQL;
          Data.DB.CommitTrans;
     except
          Data.DB.RollbackTrans;
          MessageDlg('ATTENZIONE: errore sul database - Inserimento non avvenuto',mtError, [mbOK],0);
     end;
end;

procedure TCurriculumForm.ToolbarButton971Click(Sender: TObject);
var xFile: string;
     xProcedi: boolean;
begin
     ApriCV(Data.TAnagraficaID.value);
end;

procedure TCurriculumForm.CheckListBox1ClickCheck(Sender: TObject);
begin
     xModificata:=True;
end;

procedure TCurriculumForm.ToolbarButton974Click(Sender: TObject);
begin
     ApriFileWord(Data.TAnagraficaCVNumero.asString);
end;

procedure TCurriculumForm.SpeedButton1Click(Sender: TObject);
begin
     AnagAnnunciForm:=TAnagAnnunciForm.create(self);
     AnagAnnunciForm.ShowModal;
     AnagAnnunciForm.Free;
end;

procedure TCurriculumForm.ToolbarButton975Click(Sender: TObject);
var xIDVecchioRuolo: integer;
     xIDAnag,xTotComp,i: integer;
     xComp: array[1..50] of integer;
begin
     if not MainForm.CheckProfile('05143') then Exit;
     if Data.TEspLav.RecordCount>0 then begin
          xIDVecchioRuolo:=Data.TEspLavIDMansione.Value;
          InsRuoloForm:=TInsRuoloForm.create(self);
          if Data.TEspLavIDArea.asString<>'' then begin
               InsRuoloForm.TAree.Locate('ID',Data.TEspLavIDArea.Value, []);
          end;
          InsRuoloForm.ShowModal;
          if InsRuoloForm.ModalResult=mrOK then begin
               Data.TEspLav.edit;
               Data.TEspLavIDMansione.Value:=InsRuoloForm.TRuoliID.Value;
               Data.TEspLavIDArea.Value:=InsRuoloForm.TAreeID.Value;

               Data.DB.BeginTrans;
               try
                    Data.TEspLav.ApplyUpdates;
                    // se il ruolo � nuovo inseriscilo e inserisci le competenze
                    if xIDVecchioRuolo<>Data.TEspLavIDMansione.Value then begin
                         Data2.TAnagMansIDX.Open;
                         if not Data2.TAnagMansIDX.FindKey([Data.TAnagraficaID.value,InsRuoloForm.TRuoliID.Value]) then begin
                              // attribuisci il ruolo e le competenze
                              Data.Q1.SQL.text:='insert into AnagMansioni (IDAnagrafica,IDMansione,Disponibile,IDArea) '+
                                   'values (:xIDAnagrafica,:xIDMansione,:xDisponibile,:xIDArea)';
                              Data.Q1.ParamByName('xIDAnagrafica').asInteger:=Data.TAnagraficaID.Value;
                              Data.Q1.ParamByName('xIDMansione').asInteger:=InsRuoloForm.TRuoliID.Value;
                              Data.Q1.ParamByName('xDisponibile').asBoolean:=True;
                              Data.Q1.ParamByName('xIDArea').asInteger:=InsRuoloForm.TAreeID.Value;
                              Data.Q1.ExecSQL;

                              if InsRuoloForm.CBInsComp.Checked then begin
                                   // inserisci le competenze se mancanti
                                   CompInseriteForm:=TCompInseriteForm.create(self);
                                   CompInseriteForm.ERuolo.Text:=InsRuoloForm.TRuoliDescrizione.Value;
                                   Data2.QCompRuolo.Close;
                                   Data2.QCompRuolo.Prepare;
                                   Data2.QCompRuolo.Params[0].asInteger:=Data.TEspLavIDMansione.Value;
                                   Data2.QCompRuolo.Open;
                                   Data2.QCompRuolo.First;
                                   xTotComp:=0;
                                   Data2.TAnagCompIDX.Open;
                                   CompInseriteForm.SG1.RowCount:=2;
                                   CompInseriteForm.SG1.RowHeights[0]:=16;
                                   while not Data2.QCompRuolo.EOF do begin
                                        if not Data2.TAnagCompIDX.FindKey([xIDAnag,Data2.QCompRuoloIDCompetenza.Value]) then begin
                                             CompInseriteForm.SG1.RowHeights[xTotComp+1]:=16;
                                             CompInseriteForm.SG1.Cells[0,xTotComp+1]:=Data2.QCompRuoloDescCompetenza.Value;
                                             CompInseriteForm.SG1.Cells[1,xTotComp+1]:='0';
                                             if Data.TAnagraficaIDTipoStato.Value<>2 then begin
                                                  CompInseriteForm.SG1.Cells[2,xTotComp+1]:='�';
                                                  CompInseriteForm.SG1.ColorRow[xTotComp+1]:=clLime;
                                             end else begin
                                                  CompInseriteForm.SG1.Cells[2,xTotComp+1]:='x';
                                                  CompInseriteForm.SG1.ColorRow[xTotComp+1]:=clRed;
                                             end;
                                             xComp[xTotComp+1]:=Data2.QCompRuoloIDCompetenza.Value;
                                             Inc(xTotComp);
                                             CompInseriteForm.SG1.RowCount:=CompInseriteForm.SG1.RowCount+1;
                                        end;
                                        Data2.QCompRuolo.Next;
                                   end;
                                   CompInseriteForm.SG1.RowCount:=CompInseriteForm.SG1.RowCount-1;
                                   if xTotComp>0 then CompInseriteForm.ShowModal;
                                   if CompInseriteForm.ModalResult=mrOK then begin
                                        for i:=1 to CompInseriteForm.SG1.RowCount-1 do begin
                                             if CompInseriteForm.SG1.ColorRow[i]=clLime then begin
                                                  Data.Q1.SQL.text:='insert into CompetenzeAnagrafica (IDAnagrafica,IDCompetenza,Valore) '+
                                                       'values (:xIDAnagrafica,:xIDCompetenza,:xValore)';
                                                  Data.Q1.ParamByName('xIDAnagrafica').asInteger:=xIDAnag;
                                                  Data.Q1.ParamByName('xIDCompetenza').asInteger:=xComp[i];
                                                  Data.Q1.ParamByName('xValore').asInteger:=StrToInt(CompInseriteForm.SG1.Cells[1,i]);
                                                  Data.Q1.ExecSQL;
                                                  // storico competenza
                                                  Data.Q1.SQL.text:='insert into StoricoCompAnag (IDCompetenza,IDAnagrafica,DallaData,MotivoAumento,valore) '+
                                                       'values (:xIDCompetenza,:xIDAnagrafica,:xDallaData,:xMotivoAumento,:xvalore)';
                                                  Data.Q1.ParamByName('xIDCompetenza').asInteger:=xComp[i];
                                                  Data.Q1.ParamByName('xIDAnagrafica').asInteger:=xIDAnag;
                                                  Data.Q1.ParamByName('xDallaData').asDateTime:=Date;
                                                  Data.Q1.ParamByName('xMotivoAumento').asString:='valore iniziale';
                                                  Data.Q1.ParamByName('xValore').asInteger:=StrToInt(CompInseriteForm.SG1.Cells[1,i]);
                                                  Data.Q1.ExecSQL;
                                             end;
                                        end;
                                   end;
                                   Data2.TAnagCompIDX.Close;
                                   CompInseriteForm.Free;
                              end;
                         end;
                         Data2.TAnagMansIDX.Close;
                    end;

                    Data.DB.CommitTrans;
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('ATTENZIONE: errore sul database - Inserimento non avvenuto',mtError, [mbOK],0);
               end;
               Data.TEspLav.CommitUpdates;
          end else begin
               if MessageDlg('Vuoi azzerare i dati relativi al ruolo/mansione ?',mtInformation, [mbYes,mbNo],0)=mrYes then begin
                    Data.TEspLav.Edit;
                    Data.TEspLavIDMansione.asString:='';
                    Data.TEspLavIDArea.asString:='';
                    Data.DB.BeginTrans;
                    try
                         Data.TEspLav.ApplyUpdates;
                         Data.DB.CommitTrans;
                    except
                         Data.DB.RollbackTrans;
                         MessageDlg('ATTENZIONE: errore sul database - Inserimento non avvenuto',mtError, [mbOK],0);
                    end;
                    Data.TEspLav.CommitUpdates;
               end;
          end;
          InsRuoloForm.Free;
          Data.TEspLav.Close;
          Data.TEspLav.Open;
     end;
end;

procedure TCurriculumForm.PageControl1Changing(Sender: TObject;
     var AllowChange: Boolean);
begin
     xPaginaPrec:=PageControl1.ActivePage;
end;

procedure TCurriculumForm.PageControl1Change(Sender: TObject);
begin
     if (PageControl1.ActivePage=TSCVAltriDati)and
          (not mainForm.CheckProfile('0510')) then begin
          PageControl1.ActivePage:=xPaginaPrec;
          Exit;
     end;
     if (PageControl1.ActivePage=TSCVTitoliStudio)and
          (not mainForm.CheckProfile('0511')) then begin
          PageControl1.ActivePage:=xPaginaPrec;
          Exit;
     end;
     if (PageControl1.ActivePage=TSCVCorsiStage)and
          (not mainForm.CheckProfile('0512')) then begin
          PageControl1.ActivePage:=xPaginaPrec;
          Exit;
     end;
     if (PageControl1.ActivePage=TSCVLingue)and
          (not mainForm.CheckProfile('0513')) then begin
          PageControl1.ActivePage:=xPaginaPrec;
          Exit;
     end;
     if (PageControl1.ActivePage=TSCVespLav)and
          (not mainForm.CheckProfile('0514')) then begin
          PageControl1.ActivePage:=xPaginaPrec;
          Exit;
     end;
     //CONOSCENZE INFORMATICHE
     if PageControl1.ActivePage=TSCVConoscInfo then begin
          //FrmAnagConoscInfo1.QAnagConoscInfo.ParamByName('xIDAnag').asInteger:=Data.TAnagraficaID.Value;
          FrmAnagConoscInfo1.QAnagConoscInfo.Open;
     end else begin
          if FrmAnagConoscInfo1.DsQAnagConoscInfo.state in [dsedit,dsInsert] then
               FrmAnagConoscInfo1.QAnagConoscInfo.Post;
          FrmAnagConoscInfo1.QAnagConoscInfo.Close;
     end;
     // CAMPI PERSONALIZZATI
     if (Uppercase(copy(Data.GlobalNomeAzienda.Value,1,6))='SUITEX') and
        (PageControl1.ActivePage=TSCVCampiPers) then begin
          FrmSUITEXAnagraficaExt.QSUITEXAnagExt.Open;
     end else FrmSUITEXAnagraficaExt.QSUITEXAnagExt.Close;
end;

procedure TCurriculumForm.FormClose(Sender: TObject;
     var Action: TCloseAction);
var x: string;
     i: integer;
begin
     if Data.DsTitoliStudio.State in [dsInsert,dsEdit] then begin
          with Data.TTitoliStudio do begin
               Data.DB.BeginTrans;
               try ApplyUpdates; Data.DB.CommitTrans;
               except Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               end;
               CommitUpdates;
          end;
     end;
     if Data.DsCorsiExtra.State in [dsInsert,dsEdit] then begin
          with Data.TCorsiExtra do begin
               Data.DB.BeginTrans;
               try ApplyUpdates; Data.DB.CommitTrans;
               except Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate in CorsiExtra',mtError, [mbOK],0);
               end;
               CommitUpdates;
          end;
     end;
     if Data.DsLingueConosc.State in [dsInsert,dsEdit] then begin
          with Data.TLingueConosc do begin
               Data.DB.BeginTrans;
               try ApplyUpdates; Data.DB.CommitTrans;
               except Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate in LingueConosc',mtError, [mbOK],0);
               end;
               CommitUpdates;
          end;
     end;
     if Data.DsEspLav.State in [dsInsert,dsEdit] then begin
          with Data.TEspLav do begin
               Data.DB.BeginTrans;
               try ApplyUpdates; Data.DB.CommitTrans;
               except Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate in EspLav',mtError, [mbOK],0);
               end;
               CommitUpdates;
          end;
     end;
     // risistemazione campo disp.contratto
     if dsAnagAltriDati.State in [dsInsert,dsEdit] then begin
          with TAnagAltriDati do begin
               Data.DB.BeginTrans;
               try
                    ApplyUpdates;
                    Data.DB.CommitTrans;
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate in AnagAltriDati',mtError, [mbOK],0);
               end;
               CommitUpdates;
          end;
     end;
     if xModPropCV then begin
          // *********************************************
          // ATTENZIONE:  NON FUNZIONA --> blocca tutto !!
          // *********************************************
          Data.QTemp.Close;
          Data.QTemp.SQL.Text:='update Anagrafica set IDProprietaCV='+IntToStr(xIDAziendaProp)+
               ' where ID='+Data.TAnagraficaID.asString;
          Data.QTemp.ExecSQL;
     end;
     TAnagAltriDati.Close;

     if FrmAnagConoscInfo1.DsQAnagConoscInfo.state in [dsedit,dsInsert] then
          FrmAnagConoscInfo1.QAnagConoscInfo.Post;

     MultiDBCheckBoxFrame1.SaveToDb;
end;

procedure TCurriculumForm.TAnagMansioniBeforeOpen(DataSet: TDataSet);
begin
     TMansioniLK.Open;
end;

procedure TCurriculumForm.TAnagMansioniAfterClose(DataSet: TDataSet);
begin
     TMansioniLK.Close;
end;

procedure TCurriculumForm.RGProprietaClick(Sender: TObject);
begin
     if RGProprieta.ItemIndex=0 then begin
          BAzienda.Visible:=False;
          xIDAziendaProp:=0;
     end else BAzienda.Visible:=True;
     xModPropCV:=True;
end;

procedure TCurriculumForm.BAziendaClick(Sender: TObject);
begin
     ElencoAziendeForm:=TElencoAziendeForm.create(self);
     if not((Data.TAnagraficaIDProprietaCV.value=0)or(Data.TAnagraficaIDProprietaCV.asString='')) then begin
          ElencoAziendeForm.TElencoAziende.Close;
          ElencoAziendeForm.TElencoAziende.SQl.text:='select * from EBC_Clienti '+
               'where ID='+Data.TAnagraficaIDProprietaCV.asString;
          ElencoAziendeForm.TElencoAziende.open;
     end;
     ElencoAziendeForm.Showmodal;
     if ElencoAziendeForm.ModalResult=mrOK then begin
          xIDAziendaProp:=ElencoAziendeForm.TElencoAziendeID.Value;
     end;
     ElencoAziendeForm.Free;
end;

procedure TCurriculumForm.TAnagAltriDatiAfterPost(DataSet: TDataSet);
begin
     Log_Operation(MainForm.xIDUtenteAttuale,'AnagAltreInfo',Data.TAnagraficaID.value,'U');
end;

procedure TCurriculumForm.BTabProvCVClick(Sender: TObject);
begin
     OpenSelFromTab('ProvenienzeCV','ID','Provenienza','Provenienza','');
     // caricamento DBCBProvCV con le provenienze da tabella
     Data.Q1.Close;
     Data.Q1.SQL.text:='select Provenienza from ProvenienzeCV';
     Data.Q1.Open;
     DBCBProvCV.items.Clear;
     while not Data.Q1.EOF do begin
          DBCBProvCV.items.Add(Data.Q1.FieldByName('Provenienza').asString);
          Data.Q1.Next;
     end;
     Data.Q1.Close;
end;

procedure TCurriculumForm.SpeedButton3Click(Sender: TObject);
begin
     OpenTab('TipiRetribuzione', ['ID','TipoRetrib','TipoRetribEng'], ['Dicitura italiano','Dicitura Inglese']);
     QTipiRetribLK.Close;
     QTipiRetribLK.Open;
end;

procedure TCurriculumForm.BEspLavRetribClick(Sender: TObject);
begin
     if Data.TEspLav.IsEmpty then exit;
     EspLavRetribForm:=TEspLavRetribForm.create(self);
     EspLavRetribForm.showModal;
     EspLavRetribForm.Free;
end;

procedure TCurriculumForm.BTitoloITAClick(Sender: TObject);
begin
     Data.TTitoliStudio.Close;
     Data.TTitoliStudioDiploma.LookupResultField:='Descrizione';
     Data.TTitoliStudioTipoDiploma.LookupResultField:='Tipo';
     Data.TTitoliStudio.Open;
end;

procedure TCurriculumForm.BTitoloENGClick(Sender: TObject);
begin
     Data.TTitoliStudio.Close;
     Data.TTitoliStudioDiploma.LookupResultField:='Descrizione_ENG';
     Data.TTitoliStudioTipoDiploma.LookupResultField:='Tipo_ENG';
     Data.TTitoliStudio.Open;
end;

procedure TCurriculumForm.BLinguaITAClick(Sender: TObject);
begin
     Data.TLingueConosc.Close;
     Data.TLingueConoscLingua.LookupResultField:='Lingua';
     Data.TLingueConoscDescLivello.LookupResultField:='LivelloConoscenza';
     Data.TLingueConoscDescLivelloScritto.LookupResultField:='LivelloConoscenza';
     Data.TLingueConosc.Open;
end;

procedure TCurriculumForm.BLinguaENGClick(Sender: TObject);
begin
     Data.TLingueConosc.Close;
     Data.TLingueConoscLingua.LookupResultField:='Lingua_ENG';
     Data.TLingueConoscDescLivello.LookupResultField:='LivelloConoscenza_ENG';
     Data.TLingueConoscDescLivelloScritto.LookupResultField:='LivelloConoscenza_ENG';
     Data.TLingueConosc.Open;
end;

procedure TCurriculumForm.BEspProfmatITAClick(Sender: TObject);
begin
     TAnagAltriDati.Close;
     TAnagAltriDatiEspProfMaturata.LookupResultField:='EspProfMaturata';
     TAnagAltriDati.Open;
end;

procedure TCurriculumForm.BEspProfmatENGClick(Sender: TObject);
begin
     TAnagAltriDati.Close;
     TAnagAltriDatiEspProfMaturata.LookupResultField:='EspProfMaturata_ENG';
     TAnagAltriDati.Open;
end;

procedure TCurriculumForm.BEspLavITAClick(Sender: TObject);
begin
     Data.TEspLav.Close;
     Data.TEspLavSettore.LookupResultField:='Attivita';
     Data.TEspLav.Open;
     DBEdit5.DataField:='Descrizione';
     DBEdit3.DataField:='Descrizione_1';
end;

procedure TCurriculumForm.BEspLavENGClick(Sender: TObject);
begin
     Data.TEspLav.Close;
     Data.TEspLavSettore.LookupResultField:='Attivita_ENG';
     Data.TEspLav.Open;
     DBEdit5.DataField:='Descrizione_ENG';
     DBEdit3.DataField:='Descrizione_ENG_1';
end;

end.

