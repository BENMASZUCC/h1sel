�
 TQRELENCOSEL 0%  TPF0TQRElencoSelQRElencoSelLeft Top WidthHeightcFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightDataSetSelPersNewForm.QRes1Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style Functions.Strings
PAGENUMBERCOLUMNNUMBERREPORTTITLE Functions.DATA00'' Options Page.ColumnsPage.Orientation
poPortraitPage.PaperSizeA4Page.Values       �@      ��
@       �@      @�
@       �@       �@           PrinterSettings.CopiesPrinterSettings.DuplexPrinterSettings.FirstPage PrinterSettings.LastPage PrinterSettings.OutputBinFirstPrintIfEmpty
SnapToGrid	UnitsMMZoomd TQRBandDetailBand1Left&Top� Width�Height#Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.ValuesUUUUUU5�@������v�	@ BandTyperbDetail 	TQRDBText	QRDBText1Left Top WidthDHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values2Ъ����@                    ~�����@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetSelPersNewForm.QRes1	DataFieldCVNumeroTransparentWordWrap	FontSize
  	TQRDBText	QRDBText2LeftHTop Width>HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      ��@          ������
�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetSelPersNewForm.QRes1	DataFieldCognomeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize
  	TQRDBText	QRDBText3LeftHTopWidth#HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      ��@UUUUUUU�@UUUUUU5�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetSelPersNewForm.QRes1	DataFieldNomeTransparentWordWrap	FontSize
  	TQRDBText	QRDBText4LeftxTop WidthQHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values2Ъ����@��UUUU�	@                P�@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetSelPersNewForm.QRes1	DataField	TipoStatoTransparentWordWrap	FontSize
  	TQRDBText	QRDBText5Left� Top WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      Ț@                @�@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchColorclWhiteDataSetSelPersNewForm.QRes1	DataFieldEtaTransparentWordWrap	FontSize
  	TQRDBText	QRDBText6Left	Top Width7HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUI�@          UUUUUU��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetSelPersNewForm.QRes1	DataField
TelUffCellTransparentWordWrap	FontSize
  	TQRDBText	QRDBText7Left�Top Width"HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      �	@          �������@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetSelPersNewForm.QRes1	DataFieldRuoloTransparentWordWrap	FontSize
  	TQRDBText	QRDBText8Left�TopWidth0HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      �	@UUUUUUU�@       �@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetSelPersNewForm.QRes1	DataFieldAziendaTransparentWordWrap	FontSize
  TQRShapeQRShape2Left TopWidth�Height	Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@          hUUUUu�@c5����t�	@ Shape
qrsHorLine   TQRBandPageFooterBand1Left&Top� Width�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values       �@������v�	@ BandTyperbPageFooter  TQRBand
TitleBand1Left&Top&Width�Height{Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values      ��@������v�	@ BandTyperbTitle TQRLabelQRLabel3Left TopWidth�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values痪���J�@                ��@��UUUՉ�	@ 	AlignmenttaCenterAlignToBandAutoSizeAutoStretchCaption!Elenco dei nominativi selezionatiColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBoldfsUnderline 
ParentFontTransparentWordWrap	FontSize  
TQRSysData
QRSysData1Left�Top"WidthIHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�/UUUU5�@��UUUU~�	@      ��@~����ҿ@ 	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataqrsDateFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparentFontSize  TQRLabelQRLabel4LeftTop*WidthpHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUU�@      @�@������*�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionCriteri di selezione:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsUnderline 
ParentFontTransparentWordWrap	FontSize
  TQRMemoQRMemo1LeftTop?Width?HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUU�@      ��@      ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretch	ColorclWhiteTransparentWordWrap	FontSize
   TQRBandColumnHeaderBand1Left&Top� Width�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values      @�@������v�	@ BandTyperbColumnHeader TQRShapeQRShape1Left TopWidth�Height	Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@                ��@c5����v�	@ Shape
qrsHorLine  TQRLabelQRLabel1Left TopWidth%HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@          UUUUUUU�@��������@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionN� CVColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsItalic 
ParentFontTransparentWordWrap	FontSize
  TQRLabelQRLabel2LeftHTopWidthAHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      ��@UUUUUUU�@��������@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption
NominativoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsItalic 
ParentFontTransparentWordWrap	FontSize
  TQRLabelQRLabel5Left� TopWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      Ț@UUUUUUU�@      @�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionEt�ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsItalic 
ParentFontTransparentWordWrap	FontSize
  TQRLabelQRLabel6LeftxTopWidth HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUU�	@UUUUUUU�@UUUUUUU�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionStatoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsItalic 
ParentFontTransparentWordWrap	FontSize
  TQRLabelQRLabel7Left
TopWidthzHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@�������@UUUUUUU�@UUUUUUe�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionCellulare - Tel.UfficioColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsItalic 
ParentFontTransparentWordWrap	FontSize
  TQRLabelQRLabel8Left�TopWidthvHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@������>�	@UUUUUUU�@�������@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionPosizione e aziendaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsItalic OnPrintQRLabel8Print
ParentFontTransparentWordWrap	FontSize
    