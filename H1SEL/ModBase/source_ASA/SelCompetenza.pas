unit SelCompetenza;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Grids, DBGrids, Db, DBTables, StdCtrls, Buttons, Spin, AdvGrid;

type
  TSelCompetenzaForm = class(TForm)
    TCompArea: TTable;
    DsCompArea: TDataSource;
    TCompAreaID: TAutoIncField;
    TCompAreaIDArea: TIntegerField;
    TCompAreaDescrizione: TStringField;
    TCompAreaTipologia: TStringField;
    TCompAreaQuantiValori: TIntegerField;
    TCompAreaArrayDescrizione: TStringField;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Panel2: TPanel;
    TAreeComp: TTable;
    DsAreeComp: TDataSource;
    TAreeCompID: TAutoIncField;
    TAreeCompDescrizione: TStringField;
    DBGrid1: TDBGrid;
    Timer1: TTimer;
    DBGrid2: TDBGrid;
    procedure DBGrid1KeyPress(Sender: TObject; var Key: Char);
    procedure Timer1Timer(Sender: TObject);
    //procedure ASG1GetEditorType(Sender: TObject; aCol, aRow: Integer; var aEditor: TEditorType);
    procedure FormShow(Sender: TObject);
  private
    xStringa:string;
  public
    xArrayIDComp: array [1..100] of integer;
    xArrayCompVal: array [1..100] of integer;
  end;

var
  SelCompetenzaForm: TSelCompetenzaForm;

implementation

{$R *.DFM}

procedure TSelCompetenzaForm.DBGrid1KeyPress(Sender: TObject;
  var Key: Char);
begin
     Timer1.Enabled:=False;
     if Key=chr(13) then begin
//        ASG1.SetFocus;
     end else begin
        xStringa:=xStringa+key;
        TAreeComp.FindNearest([xStringa]);
     end;
     Timer1.Enabled:=True;
end;

procedure TSelCompetenzaForm.Timer1Timer(Sender: TObject);
begin
     xStringa:='';
end;

procedure TSelCompetenzaForm.FormShow(Sender: TObject);
begin
     Caption:='[S/35] - '+Caption;
end;

end.
