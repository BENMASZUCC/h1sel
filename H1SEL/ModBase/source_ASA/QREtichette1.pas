{ QuickReport master detail template }

unit QREtichette1;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     Qrctrls,quickrpt,DB,DBTables,ExtCtrls;

type
     TQREtichette1Form=class(TQuickRep)
          DetailBand1: TQRBand;
          QRLabel5: TQRLabel;
          QRLabel6: TQRLabel;
          QRLabel7: TQRLabel;
          QRLabel8: TQRLabel;
          QRDBText1: TQRDBText;
          QRShape1: TQRShape;
          QRShape2: TQRShape;
          QRShape3: TQRShape;
          QRShape4: TQRShape;
          QRDBText2: TQRDBText;
          QRLabel1: TQRLabel;
          QRLabel2: TQRLabel;
          QRLabel3: TQRLabel;
          procedure QRLabel6Print(sender: TObject; var Value: string);
          procedure QRLabel7Print(sender: TObject; var Value: string);
          procedure QRLabel5Print(sender: TObject; var Value: string);
          procedure QREtichette1FormNeedData(Sender: TObject;
               var MoreData: Boolean);
          procedure QRLabel8Print(sender: TObject; var Value: string);
          procedure QREtichette1FormBeforePrint(Sender: TCustomQuickRep;
               var PrintReport: Boolean);
          procedure DetailBand1BeforePrint(Sender: TQRCustomBand;
               var PrintBand: Boolean);
          procedure QRDBText2Print(sender: TObject; var Value: string);
          procedure QRLabel1Print(sender: TObject; var Value: string);
          procedure QRLabel2Print(sender: TObject; var Value: string);
          procedure QRLabel3Print(sender: TObject; var Value: string);
     private
          CurLbl,NumCopie: integer;
     public
          { Public declarations }
     end;

var
     QREtichette1Form: TQREtichette1Form;

implementation

uses RicClienti,ModuloDati;

{$R *.DFM}

procedure TQREtichette1Form.QRLabel6Print(sender: TObject;
     var Value: string);
begin
     if RicClientiForm.QRes1Contatto.Value='' then
          Value:=RicClientiForm.QRes1Cliente.Value
     else Value:=RicClientiForm.QRes1Contatto.Value;
end;

procedure TQREtichette1Form.QRLabel7Print(sender: TObject;
     var Value: string);
begin
     with RicClientiForm do begin
          if QRes1EtichettaAbitazione.Value then
               Value:=QRes1CapPrivato.value+' '+QRes1ComunePrivato.Value+' ('+QRes1ProvinciaPrivato.Value+')'
          else Value:=QRes1Cap.value+' '+QRes1Comune.Value+' ('+QRes1Provincia.Value+')';
     end;
end;


procedure TQREtichette1Form.QRLabel5Print(sender: TObject; var Value: string);
begin
     if RicClientiForm.QRes1Contatto.Value='' then begin
          Value:='Spett.le';
     end else begin
          if RicClientiForm.QRes1Titolo.Value='' then begin
               if RicClientiForm.QRes1Sesso.value='F' then
                    Value:='Gent.ma Sig.ra'
               else Value:='Egr.Sig.';
          end else Value:='Egr. '+RicClientiForm.QRes1Titolo.Value;
     end;
end;

procedure TQREtichette1Form.QREtichette1FormNeedData(Sender: TObject;
     var MoreData: Boolean);
begin
     MoreData:=true;
     //inc(CurLbl);
     //if CurLbl>NumCopie then begin
     RicClientiForm.Qres1.Next;
     if RicClientiForm.Qres1.EOF then begin
          MoreData:=false;
     end
          //else begin
          //   NumCopie:=Data.QCliCatNonAbbNumCopie.Value; /// numerocopie
          //          CurLbl:=1;
          //     end;
          //end;
end;

procedure TQREtichette1Form.QRLabel8Print(sender: TObject;
     var Value: string);
begin
     if RicClientiForm.QRes1Contatto.Value<>'' then begin
          if not RicClientiForm.QRes1EtichettaAbitazione.Value then
               value:='c/o '+RicClientiForm.QRes1Cliente.Value
          else value:='';
     end else value:='';
end;

procedure TQREtichette1Form.QREtichette1FormBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
     RicClientiForm.Qres1.First;
     PrintReport:=RicClientiForm.Qres1.RecordCount>0;
     //if PrintReport then begin
     //     CurLbl:=0;
     //     NumCopie:=Data.QCliCatNonAbbNumCopie.Value; /// numerocopie
     //end;
end;

procedure TQREtichette1Form.DetailBand1BeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
var i,PDS: integer;
begin
     {for i:=1 to 1024 do
          if MainForm.xCatScelte[i]=Data.QCliCatNonAbbIDCategoria.Value then break;
     PDS:=MainForm.xCatPercRandom[i];
     PrintBand:=Random(100)<PDS;
     if PrintBand then inc(MainForm.xTotAbbStampati);}
end;

procedure TQREtichette1Form.QRDBText2Print(sender: TObject; var Value: string);
begin
     if RicClientiForm.QRes1EtichettaAbitazione.Value then
          // attenzione:  per ora vale solo per l'ITALIA !!
          Value:=RicClientiForm.QRes1TipoStradaPrivato.Value+' '+
               RicClientiForm.QRes1IndirizzoPrivato.Value+' '+
               RicClientiForm.QRes1NumCivicoPrivato.Value
     else Value:=RicClientiForm.QRes1TipoStrada.Value+' '+
          RicClientiForm.QRes1Indirizzo.Value+' '+
               RicClientiForm.QRes1NumCivico.Value;
end;

procedure TQREtichette1Form.QRLabel1Print(sender: TObject; var Value: string);
begin
     if RicClientiForm.QRes1EtichettaAbitazione.Value then
          value:=RicClientiForm.QRes1Paese.Value
     else begin
          Data.Q1.Close;
          Data.Q1.SQL.text:='select DescNazione from Nazioni where Abbrev='''+RicClientiForm.QRes1NazioneAbb.value+'''';
          Data.Q1.Open;
          value:=Data.Q1.FieldByName('DescNazione').asString;
          Data.Q1.Close;
     end;
end;

procedure TQREtichette1Form.QRLabel2Print(sender: TObject; var Value: string);
begin
     if not RicClientiForm.QRes1EtichettaAbitazione.Value then
          value:=RicClientiForm.QRes1DivisioneAziendale.Value
     else value:='';
end;

procedure TQREtichette1Form.QRLabel3Print(sender: TObject; var Value: string);
begin
     if not RicClientiForm.QRes1EtichettaAbitazione.Value then
          value:=RicClientiForm.QRes1CaricaAziendale.Value
     else value:='';
end;

end.

