�
 TRIEPTIMINGFORM 0�	  TPF0TRiepTimingFormRiepTimingFormLeft� TopsBorderStylebsDialogCaption!Riepilogo tempi Utente - RicercheClientHeight�ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TLabelLabel1LeftTopWidth$HeightCaptionCClassifica clienti-ricerche in ordine di numero secondi di attivit�  TLabelLabel2Left� Top�WidthYHeightCaptionTotale secondi:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TDBGridDBGrid1LeftTopWidthZHeightw
DataSourceDsQClassificaTabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style OnTitleClickDBGrid1TitleClickColumnsExpanded	FieldNameDescrizioneTitle.CaptionClienteVisible	 Expanded	FieldNameProgressivoTitle.CaptionRif.RicercaWidth=Visible	 Expanded	FieldNameTotTitle.AlignmenttaRightJustifyTitle.Caption
N� secondiVisible	    TBitBtnBitBtn1LefthTopWidthYHeight!TabOrderKindbkOK  TDBEditDBEdit1Left
Top�WidthAHeight	DataFieldTotale
DataSourceDsQTotTempoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder  TQueryQClassificaDatabaseNameEBCDBSQL.Strings.select Progressivo,Descrizione,sum(TotSec) Tot(from TimingRic,EBC_Ricerche,EBC_Clienti )where TimingRic.IDRicerca=EBC_Ricerche.ID-    and EBC_Ricerche.IDCliente=EBC_Clienti.ID%    and TimingRic.IDUtente=:xIDutente group by Progressivo,Descrizioneorder by Tot desc Left(Topp	ParamDataDataType	ftUnknownName	xIDutente	ParamType	ptUnknown   TStringFieldQClassificaProgressivo	FieldNameProgressivoOrigin"EBC_Ricerche".ProgressivoSize
  TStringFieldQClassificaDescrizione	FieldNameDescrizioneOrigin"EBC_Clienti".DescrizioneSize  TIntegerFieldQClassificaTot	FieldNameTot   TDataSourceDsQClassificaDataSetQClassificaLeft(Top�   TQuery	QTotTempoDatabaseNameEBCDBSQL.Stringsselect sum(TotSec) Totalefrom TimingRic where IDUtente=:xIDUtente LeftxTop� 	ParamDataDataType	ftUnknownName	xIDUtente	ParamType	ptUnknown   TIntegerFieldQTotTempoTotale	FieldNameTotale   TDataSourceDsQTotTempoDataSet	QTotTempoLeftxTop�    