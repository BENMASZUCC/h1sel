�
 TINEVIDENZAFORM 0q  TPF0TInEvidenzaFormInEvidenzaFormLeft� Top� BorderIcons BorderStylebsDialogCaptionCandidato in evidenzaClientHeight�ClientWidth�Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style PositionpoScreenCenterPixelsPerInch`
TextHeight TPanelPanel1Left Top�Width�Height1AlignalBottomTabOrder  TBitBtnBitBtn1LeftTop	Width_Height!CaptionConfermaTabOrder OnClickBitBtn1ClickKindbkOK  TBitBtnBitBtn2Left}Top	Width_Height!CaptionAnnullaTabOrderKindbkCancel   TPageControlPageControl1Left Top Width�Height�
ActivePageTsVisioneCVAlignalClientTabOrder 	TTabSheet
TsRicercheCaptionRicerche-Evidenza TPanelPanel2Left Top Width�HeightnAlignalClient
BevelOuterbvNoneCaptionPanel2TabOrder  	TGroupBox	GroupBox1LeftTop Width� Height*CaptionEventoTabOrder  TLabelLabel3LeftTopWidthHeightCaptionData:  TDateTimePicker
DataEventoLeft+TopWidthMHeightCalAlignmentdtaLeftDate�-�.J���@Time�-�.J���@
DateFormatdfShortDateMode
dmComboBoxKinddtkDate
ParseInput   	TGroupBox	GroupBox2LeftTop0Width�Height9Hint.registra l'evidenza per la ricerca selezionataCaptionRicerche interneTabOrder TLabelLabel1LeftTopWidthKHeightCaption;Elenco ricerche non concluse in cui � inserito il candidatoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel2Left	Top� WidthHeightCaption0Ricerche per le quali il candidato � in evidenzaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TDBGridDBGrid1LeftTop"Width�Height{
DataSourceDataAssunzione.DsQAnagRicReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style Columns	FieldNameProgressivoTitle.CaptionRif.Width/ 	FieldNameMansioneTitle.CaptionRuolo/mansioniWidthr 	FieldName
DataInizioTitle.CaptionData inizioWidth9 	FieldNameStatoTitle.CaptionStato ricercaWidth�  	FieldNameStato_1Title.Captionsituazione candidatoWidth;    TBitBtnBitBtn3Left�Top� WidthxHeightCaptionRegistra evidenzaParentShowHintShowHint	TabOrderOnClickBitBtn3Click
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333337?3333330�3333333333330�333333�333330�33333w?�33330��3333w?33330���33333333 ���3337ss3330����333333330����333s333330����333s�33s3333��3337����3333    3337www3333���3337����333    337wwww�333   337www��333    337wwww33	NumGlyphs  TDBGridDBGrid2LeftTop� Width�Heightr
DataSourceDataAssunzione.DsQAnagRicEvidReadOnly	TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style Columns	FieldNameProgressivoTitle.CaptionRif.Width/ 	FieldNameMansioneTitle.CaptionRuolo/mansioniWidthr 	FieldName
DataInizioTitle.CaptionData inizioWidth9 	FieldNameStatoTitle.CaptionStato ricercaWidth�  	FieldNameStato_1Title.Captionsituazione candidatoWidth;    TBitBtnBitBtn4LeftATop� WidthxHeightCaptionTogli evidenzaTabOrderOnClickBitBtn4Click
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� P  UUUWwwuU�U    UPUUwwwwU�UU U PUUUw�wWUUU UPUUUUw�W_�UUPP UUUW�uw�UUU PPUUUw�Ww�UUU	�UUU�uwww�UPU	��UW_wwwu�PP��0UWu�wwW_U PU	UwWUwuuuUUUUP��0UU_UWWWWUUUU3UUuUUuuUUUUUUP��UU�UUW_UUPUUUU�UWUUUUu�UUUUUUP�UUUUUUW_UUUUUUUUUUUUUUu	NumGlyphs     	TTabSheet	TsDatiPosCaptionPosizione prospettata 	TGroupBox	GroupBox3LeftTopWidthHeightICaptionQualifica previstaTabOrder   	TGroupBox	GroupBox4LeftTop`WidthHeightICaptionRetribuzione previstaTabOrder  	TGroupBox	GroupBox5LeftTop� WidthHeightiCaptionPosizione organigramma previstaTabOrder TLabelLabel4LefthTop(Width(HeightCaption???Font.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont    	TTabSheetTsVisioneCVCaption
Visione CV TLabelLabel5Left	TopWidth� HeightCaption Invio del Curriculum in visione:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TRadioGroupRadioGroup1LeftLTop6Width� Height;CaptionFiltro	ItemIndex Items.Stringstuttisolo quelli da restituire TabOrder OnClickRadioGroup1Click  TBitBtnBbCVnewLeftETop
Width=HeightCaptionNuovoTabOrderOnClickBbCVnewClick  TBitBtnBbCVdelLeft�Top
Width=HeightCaptionEliminaTabOrderOnClickBbCVdelClick  TBitBtnBitBtn7LeftNTopzWidth`HeightCaptionRestituzioneTabOrderOnClickBitBtn7Click
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333�33;3�3333�;�w{�w{�7����s3�    33wwwwww330����337�333330����337��?�330� 337�sws330����3?����?���� �ww�wssw;������7w��?�ww30�  337�swws330���3337��7�330��3337�sw�330�� ;�337��w7�3�  3�33www3w�;�3;�3;�7s37s37s�33;333;s3373337	NumGlyphs  TDBCtrlGridDBCtrlGrid1LeftTop=Width?HeightAllowInsertColCount
DataSourceDataAssunzione.DsCVinVisionePanelHeight#
PanelWidth/TabOrderRowCountSelectedColorclTeal TDBEditDBEdit1LeftwTopWidth@Height	DataFieldRestituireEntro
DataSourceDataAssunzione.DsCVinVisioneTabOrder   TDBCheckBoxDBCheckBox1Left� Top
WidthIHeightCaption
Restituito	DataField
Restituito
DataSourceDataAssunzione.DsCVinVisioneTabOrderValueCheckedTrueValueUncheckedFalse  TDBLookupComboBoxDBLookupComboBox1LeftTopWidthjHeight	DataField
InVisioneA
DataSourceDataAssunzione.DsCVinVisioneTabOrder  TDBCheckBoxDBCheckBox2LeftTop
WidthVHeightCaptionInteressante	DataFieldInteressante
DataSourceDataAssunzione.DsCVinVisioneTabOrderValueCheckedTrueValueUncheckedFalse  TDBMemoDBMemo1LeftYTopWidth� Height	DataFieldNote
DataSourceDataAssunzione.DsCVinVisioneTabOrder   TPanelPanel3LeftTop)Width.HeightColorclGrayTabOrder TLabelLabel6LeftTopWidth6HeightCaptionIn visione aColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFont  TLabelLabel7LeftwTopWidthBHeightCaptionrestituire entroColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFont  TLabelLabel8LeftWTopWidth6HeightCaptionannotazioniColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFont   TBitBtnBbCVokLeft�Top
Width=HeightCaptionOKEnabledTabOrderOnClickBbCVokClick  TBitBtnBbCVcanLeft�Top
Width=HeightCaptionAnnullaEnabledTabOrderOnClickBbCVcanClick    TQueryQ1DatabaseNameHRLeftdTop8   