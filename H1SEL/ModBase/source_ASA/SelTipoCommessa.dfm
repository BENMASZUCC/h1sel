object SelTipoCommessaForm: TSelTipoCommessaForm
  Left = 207
  Top = 106
  BorderStyle = bsDialog
  Caption = 'Selezione tipo commessa'
  ClientHeight = 193
  ClientWidth = 267
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 3
    Top = 4
    Width = 169
    Height = 185
    DataSource = DsQTipo
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'TipoCommessa'
        Title.Caption = 'Tipo commessa'
        Width = 133
        Visible = True
      end>
  end
  object BitBtn1: TBitBtn
    Left = 178
    Top = 3
    Width = 86
    Height = 32
    TabOrder = 1
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 178
    Top = 37
    Width = 86
    Height = 32
    Caption = 'Annulla'
    TabOrder = 2
    Kind = bkCancel
  end
  object QTipo: TQuery
    Active = True
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select * from EBC_TipiCommesse')
    Left = 72
    Top = 48
    object QTipoID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.EBC_TipiCommesse.ID'
    end
    object QTipoTipoCommessa: TStringField
      FieldName = 'TipoCommessa'
      Origin = 'EBCDB.EBC_TipiCommesse.TipoCommessa'
      FixedChar = True
      Size = 30
    end
    object QTipoColore: TStringField
      FieldName = 'Colore'
      Origin = 'EBCDB.EBC_TipiCommesse.Colore'
      FixedChar = True
      Size = 10
    end
  end
  object DsQTipo: TDataSource
    DataSet = QTipo
    Left = 72
    Top = 80
  end
end
