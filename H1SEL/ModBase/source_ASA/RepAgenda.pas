unit RepAgenda;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables, QuickRpt, Qrctrls, ExtCtrls;

type
  TQRAgenda = class(TForm)
    QR1: TQuickRep;
    DetailBand1: TQRBand;
    PageFooterBand1: TQRBand;
    TitleBand1: TQRBand;
    ColumnHeaderBand1: TQRBand;
    QRSubDetail1: TQRSubDetail;
    QDate: TQuery;
    QAgenda: TQuery;
    DsQDate: TDataSource;
    QDateData: TDateTimeField;
    QAgendaData: TDateTimeField;
    QAgendaOre: TDateTimeField;
    QAgendaIDUtente: TIntegerField;
    QAgendaTipo: TSmallintField;
    QAgendaIDCandRic: TIntegerField;
    QAgendaDescrizione: TStringField;
    QAgendaIDRisorsa: TIntegerField;
    QRDBText1: TQRDBText;
    QRDBText3: TQRDBText;
    QRLabel1: TQRLabel;
    QRDate: TQRLabel;
    QRShape1: TQRShape;
    QRDBText2: TQRDBText;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    Q: TQuery;
    QRLabel9: TQRLabel;
    procedure QRLabel2Print(sender: TObject; var Value: String);
    procedure QRLabel7Print(sender: TObject; var Value: String);
    procedure QRLabel8Print(sender: TObject; var Value: String);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  QRAgenda: TQRAgenda;

implementation

{$R *.DFM}

procedure TQRAgenda.QRLabel2Print(sender: TObject; var Value: String);
begin
     case QAgendaTipo.Value of
     1: Value:='colloquio';
     2: Value:='telefonata';
     3: Value:='varie';
     end;
end;

procedure TQRAgenda.QRLabel7Print(sender: TObject; var Value: String);
begin
     case DayOfWeek(QDateData.Value) of
     1: value:='Domenica';
     2: value:='Luned�';
     3: value:='Marted�';
     4: value:='Mercoled�';
     5: value:='Gioved�';
     6: value:='Venerd�';
     7: value:='Sabato';
     end;
end;

procedure TQRAgenda.QRLabel8Print(sender: TObject; var Value: String);
begin
     if QAgendaIDCandRic.AsString='' then
        value:=''
     else begin
          Q.SQL.Text:='select EBC_Ricerche.Progressivo Rif,EBC_Clienti.Descrizione Cliente '+
                      'from EBC_CandidatiRicerche,EBC_Ricerche,EBC_Clienti '+
                      'where EBC_CandidatiRicerche.IDRicerca=EBC_Ricerche.ID '+
                      'and EBC_Ricerche.IDCliente=EBC_Clienti.ID '+
                      'and EBC_CandidatiRicerche.ID='+QAgendaIDCandRic.asString;
          Q.Open;
          Value:='N� '+Q.FieldByName('Rif').asString+' per '+Q.FieldByName('Cliente').asString;
          Q.Close;
     end;
end;

end.
