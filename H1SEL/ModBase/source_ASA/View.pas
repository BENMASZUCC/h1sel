unit View;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ImageEnView, ImageEn, ExtCtrls, StdCtrls, Buttons, ComCtrls, Mask, ComObj,
  DBCtrls, Db, DBTables, ImageEnIO, Spin, TB97, GIFLZW, ImgList, LookOut,
  ImageEnProc, Menus;

type
  TViewForm = class(TForm)
    DsAnagABS: TDataSource;
    Panel4: TPanel;
    SBPrec: TToolbarButton97;
    SBSucc: TToolbarButton97;
    BAcquisisci: TToolbarButton97;
    Label1: TLabel;
    BitBtn2: TToolbarButton97;
    BitBtn1: TToolbarButton97;
    TrackBar1: TTrackBar;
    ZoomLbl: TEdit;
    LPagCorr: TLabel;
    ImageList1: TImageList;
    BCut: TToolbarButton97;
    BSalvaConNome: TToolbarButton97;
    ToolbarButton972: TToolbarButton97;
    LInterno: TLabel;
    BUndo: TToolbarButton97;
    BStampa: TToolbarButton97;
    MainMenu1: TMainMenu;
    File1: TMenuItem;
    Edit1: TMenuItem;
    Cancella1: TMenuItem;
    Zoom1: TMenuItem;
    N1001: TMenuItem;
    adatta1: TMenuItem;
    Pagina1: TMenuItem;
    indietro1: TMenuItem;
    avanti1: TMenuItem;
    acquisiscidascanner1: TMenuItem;
    caricailfileesternointerno1: TMenuItem;
    salvailfilecomeesterno1: TMenuItem;
    Annullacancellazione1: TMenuItem;
    Stampa1: TMenuItem;
    StampaImmagine1: TMenuItem;
    ImageEn1: TImageEn;
    ToolbarButton971: TToolbarButton97;
    procedure TrackBar1Change(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SBPrecClick(Sender: TObject);
    procedure SBSuccClick(Sender: TObject);
    procedure BAcquisisciClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BCutClick(Sender: TObject);
    procedure ImageEnView1ImageChange(Sender: TObject);
    procedure BSalvaConNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ToolbarButton972Click(Sender: TObject);
    procedure BUndoClick(Sender: TObject);
    procedure BStampaClick(Sender: TObject);
    procedure Cancella1Click(Sender: TObject);
    procedure acquisiscidascanner1Click(Sender: TObject);
    procedure caricailfileesternointerno1Click(Sender: TObject);
    procedure salvailfilecomeesterno1Click(Sender: TObject);
    procedure Annullacancellazione1Click(Sender: TObject);
    procedure N1001Click(Sender: TObject);
    procedure adatta1Click(Sender: TObject);
    procedure indietro1Click(Sender: TObject);
    procedure avanti1Click(Sender: TObject);
    procedure StampaImmagine1Click(Sender: TObject);
    procedure ImageEn1ImageChange(Sender: TObject);
    procedure ToolbarButton971Click(Sender: TObject);
  private
    xLetteraCorr:char;
    Changed:boolean;
    procedure SalvaComeEsterno;
  public
    { Public declarations }
    xInterno:boolean;
    xFileName,xCVNumero,xCodCV:string;
    procedure CaricaFile;
  end;

var
  ViewForm: TViewForm;

implementation

uses ModuloDati, uUtilsVarie;

{$R *.DFM}

procedure TViewForm.TrackBar1Change(Sender: TObject);
begin
     ImageEn1.Zoom:=TrackBar1.Position;
     ZoomLbl.Text:=IntToStr(TrackBar1.Position);
end;

procedure TViewForm.BitBtn1Click(Sender: TObject);
begin
     ImageEn1.Fit;
     ZoomLbl.Text:=IntToStr(ImageEn1.Zoom);
end;

procedure TViewForm.BitBtn2Click(Sender: TObject);
begin
     ImageEn1.Zoom:=100;
     ZoomLbl.Text:=IntToStr(100);
end;

procedure TViewForm.CaricaFile;
begin
   if FileExists(xFileName) then begin
        ImageEn1.Deselect;
        ImageEn1.LoadFromFile(xFileName);
        ImageEn1.Zoom:=100;
        ZoomLbl.Text:='100';
        ImageEn1.ClearUndo;
        ImageEn1.Background:=clWhite;
        ImageEn1.MouseInteract:=[miSelect];
        changed:=False;
        if xInterno then LInterno.caption:='(interno)'
        else LInterno.caption:='(esterno)';

        //ImageEn1.Cursor:=1785;
   end else MessageDlg('ERRORE: file non trovato !',mtError,[mbOK],0);
end;

procedure TViewForm.FormShow(Sender: TObject);
begin
     Caption:='[M/053] - '+Caption;
     if xFileName='' then begin
        SBPrec.Enabled:=False;
        SBSucc.Enabled:=False;
        xLetteraCorr:='0';
        LPagCorr.Caption:='(nessuna pagina)';
        LInterno.caption:='---';
     end else begin
        SBPrec.Enabled:=False;
        if not FileExists(GetCVPath+'\i'+xCVNumero+'b.gif') then SBSucc.Enabled:=False
        else SBSucc.Enabled:=True;
        xLetteraCorr:='a';
        LPagCorr.Caption:='Pagina '+xLetteraCorr;
        LInterno.caption:='(interno)';
        CaricaFile;
     end;
end;

procedure TViewForm.SBPrecClick(Sender: TObject);
begin
   xLetteraCorr:=char(Ord(xLetteraCorr)-1);
   if xInterno then xFileName:=GetCVPath+'\i'+xCVNumero+xLetteraCorr+'.gif'
   else xFileName:=GetCVPath+'\e'+xCVNumero+xLetteraCorr+'.gif';
   if xInterno then begin
     if not FileExists(GetCVPath+'\i'+xCVNumero+char(Ord(xLetteraCorr)-1)+'.gif') then SBPrec.Enabled:=False
     else SBPrec.Enabled:=True;
     if not FileExists(GetCVPath+'\i'+xCVNumero+char(Ord(xLetteraCorr)+1)+'.gif') then SBSucc.Enabled:=False
     else SBSucc.Enabled:=True;
   end else begin
     if not FileExists(GetCVPath+'\e'+xCVNumero+char(Ord(xLetteraCorr)-1)+'.gif') then SBPrec.Enabled:=False
     else SBPrec.Enabled:=True;
     if not FileExists(GetCVPath+'\e'+xCVNumero+char(Ord(xLetteraCorr)+1)+'.gif') then SBSucc.Enabled:=False
     else SBSucc.Enabled:=True;
   end;
   CaricaFile;
   LPagCorr.Caption:='Pagina '+xLetteraCorr;
end;

procedure TViewForm.SBSuccClick(Sender: TObject);
begin
   xLetteraCorr:=char(Ord(xLetteraCorr)+1);
   if xInterno then
      xFileName:=GetCVPath+'\i'+xCVNumero+xLetteraCorr+'.gif'
   else xFileName:=GetCVPath+'\e'+xCVNumero+xLetteraCorr+'.gif';
   if xInterno then begin
     if not FileExists(GetCVPath+'\i'+xCVNumero+char(Ord(xLetteraCorr)-1)+'.gif') then SBPrec.Enabled:=False
     else SBPrec.Enabled:=True;
     if not FileExists(GetCVPath+'\i'+xCVNumero+char(Ord(xLetteraCorr)+1)+'.gif') then SBSucc.Enabled:=False
     else SBSucc.Enabled:=True;
   end else begin
     if not FileExists(GetCVPath+'\e'+xCVNumero+char(Ord(xLetteraCorr)-1)+'.gif') then SBPrec.Enabled:=False
     else SBPrec.Enabled:=True;
     if not FileExists(GetCVPath+'\e'+xCVNumero+char(Ord(xLetteraCorr)+1)+'.gif') then SBSucc.Enabled:=False
     else SBSucc.Enabled:=True;
   end;

   CaricaFile;
   LPagCorr.Caption:='Pagina '+xLetteraCorr;
end;

procedure TViewForm.BAcquisisciClick(Sender: TObject);
var xOK:boolean;
    xLettera:char;
begin
     ImageEn1.Acquire;
     //ImageEnIO1.DoPreviews([ppGIF]);
     if xLetteraCorr='0' then xLettera:='a'
     else xLettera:=char(Ord(xLetteraCorr)+1);;
     xInterno:=True;
     ImageEn1.ConvertToBWOrdered;
     if FileExists(GetCVPath+'\i'+xCVNumero+char(Ord(xLettera))+'.gif') then
        if MessageDlg('Attenzione: il file '+'\i'+xCVNumero+char(Ord(xLettera))+'.gif'+chr(13)+
             'esiste gi�. VUOI SOVRASCRIVERLO ?',mtWarning,[mbYes,mbNo],0)=mrNo then exit;
     ImageEn1.SaveToFileGIF(GetCVPath+'\i'+xCVNumero+char(Ord(xLettera))+'.gif');
     xLetteraCorr:=xLettera;

     xFileName:=GetCVPath+'\i'+xCVNumero+xLetteraCorr+'.gif';
     if not FileExists(GetCVPath+'\i'+xCVNumero+char(Ord(xLetteraCorr)-1)+'.gif') then SBPrec.Enabled:=False
     else SBPrec.Enabled:=True;
     if not FileExists(GetCVPath+'\i'+xCVNumero+char(Ord(xLetteraCorr)+1)+'.gif') then SBSucc.Enabled:=False
     else SBSucc.Enabled:=True;
     LPagCorr.Caption:='Pagina '+xLetteraCorr;
end;

procedure TViewForm.FormCreate(Sender: TObject);
begin
     xLetteraCorr:='0';
     xInterno:=True;
     DefGIF_LZWDECOMPFUNC:=GIFLZWDecompress;
     DefGIF_LZWCOMPFUNC:=GIFLZWCompress;
end;

procedure TViewForm.BCutClick(Sender: TObject);
begin
     ImageEn1.ClearSel;
     //ImageEn1.Deselect;
end;

procedure TViewForm.ImageEnView1ImageChange(Sender: TObject);
begin
     Changed:=true;
end;

procedure TViewForm.BSalvaConNomeClick(Sender: TObject);
begin
     SalvaComeEsterno;
end;

procedure TViewForm.SalvaComeEsterno;
var xNomeFile:string;
    xVai:boolean;
begin
     xVai:=True;
     xNomeFile:=GetCVPath+'\e'+xCVNumero+char(Ord(xLetteraCorr))+'.gif';
     if FileExists(xNomeFile) then
        if MessageDlg('Il file esiste gi� - Sovrascriverlo ?',mtWarning,[mbYes,mbNo],0)=mrNo then xVai:=False;
     if xVai then begin
        ImageEn1.SaveToFile(xNomeFile);
        Changed:=False;
        xInterno:=False;
        LInterno.caption:='(esterno)';
        ToolbarButton972.caption:='carica interno';
        if not FileExists(GetCVPath+'\e'+xCVNumero+char(Ord(xLetteraCorr)-1)+'.gif') then SBPrec.Enabled:=False
        else SBPrec.Enabled:=True;
        if not FileExists(GetCVPath+'\e'+xCVNumero+char(Ord(xLetteraCorr)+1)+'.gif') then SBSucc.Enabled:=False
        else SBSucc.Enabled:=True;
        ShowMessage('File salvato con nome "'+'e'+xCVNumero+char(Ord(xLetteraCorr))+'.gif'+'"');
     end;
end;

procedure TViewForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     if Changed then
        if MessageDlg('MODIFICHE APPORTATE: Il file non � salvato - Salvare ?',mtWarning,[mbYes,mbNO],0)=mrYes then
           SalvaComeEsterno;
end;

procedure TViewForm.ToolbarButton972Click(Sender: TObject);
begin
     if xInterno then begin
        xInterno:=False;
        xFileName:=GetCVPath+'\e'+xCVNumero+'a.gif';
        if FileExists(xFileName) then begin
           xLetteraCorr:='a';
           CaricaFile;
           ToolbarButton972.caption:='carica interno';
           if not FileExists(GetCVPath+'\e'+xCVNumero+char(Ord(xLetteraCorr)-1)+'.gif') then SBPrec.Enabled:=False
           else SBPrec.Enabled:=True;
           if not FileExists(GetCVPath+'\e'+xCVNumero+char(Ord(xLetteraCorr)+1)+'.gif') then SBSucc.Enabled:=False
           else SBSucc.Enabled:=True;
        end else MessageDlg('Non esiste alcun file esterno',mtError,[mbOK],0);
     end else begin
        xInterno:=True;
        xFileName:=GetCVPath+'\i'+xCVNumero+'a.gif';
        if FileExists(xFileName) then begin
           xLetteraCorr:='a';
           CaricaFile;
           ToolbarButton972.caption:='carica esterno';
           if not FileExists(GetCVPath+'\i'+xCVNumero+char(Ord(xLetteraCorr)-1)+'.gif') then SBPrec.Enabled:=False
           else SBPrec.Enabled:=True;
           if not FileExists(GetCVPath+'\i'+xCVNumero+char(Ord(xLetteraCorr)+1)+'.gif') then SBSucc.Enabled:=False
           else SBSucc.Enabled:=True;
        end else MessageDlg('Non esiste alcun file interno',mtError,[mbOK],0);
     end;
end;

procedure TViewForm.BUndoClick(Sender: TObject);
begin
     ImageEn1.Undo;
     ImageEn1.Deselect;
end;

procedure TViewForm.BStampaClick(Sender: TObject);
begin
     if xFileName<>'' then begin
        if FileExists(ExtractFilePath(Application.EXEName)+'ACDSee32.exe') then
           WinExec(pointer(ExtractFilePath(Application.EXEName)+'ACDSee32 /p '+xFileName),SW_SHOW)
        else MessageDlg('Il file "ACDSee32.exe" deve trovarsi nella stessa directory del programma - IMPOSSIBILE STAMPARE IL FILE',mtError,[mbOK],0);
     end;
end;

procedure TViewForm.Cancella1Click(Sender: TObject);
begin
     ImageEn1.ClearSel;
end;

procedure TViewForm.acquisiscidascanner1Click(Sender: TObject);
begin
     BAcquisisciClick(self);
end;

procedure TViewForm.caricailfileesternointerno1Click(Sender: TObject);
begin
     ToolbarButton972Click(self);
end;

procedure TViewForm.salvailfilecomeesterno1Click(Sender: TObject);
begin
     SalvaComeEsterno;
end;

procedure TViewForm.Annullacancellazione1Click(Sender: TObject);
begin
     BUndoClick(self);
end;

procedure TViewForm.N1001Click(Sender: TObject);
begin
     BitBtn2Click(self);
end;

procedure TViewForm.adatta1Click(Sender: TObject);
begin
     BitBtn1Click(self);
end;

procedure TViewForm.indietro1Click(Sender: TObject);
begin
     SBPrecClick(self);
end;

procedure TViewForm.avanti1Click(Sender: TObject);
begin
     SBSuccClick(self);
end;

procedure TViewForm.StampaImmagine1Click(Sender: TObject);
begin
     BStampaClick(self);
end;

procedure TViewForm.ImageEn1ImageChange(Sender: TObject);
begin
     Changed:=true;
end;

procedure TViewForm.ToolbarButton971Click(Sender: TObject);
begin
     Close;   
end;

end.
