€
 TSELPERSFORM 0Б} TPF0TSelPersFormSelPersFormLeftш TopЕ WidthHeightNActiveControl	PCSelPersBorderIconsbiSystemMenu
biMaximize CaptionGestione della commessaColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.Style OldCreateOrder	PositionpoScreenCenterOnClose	FormCloseOnCreate
FormCreateOnMouseMoveFormMouseMoveOnShowFormShowPixelsPerInch`
TextHeight TPanelPanel2Left Top Width€HeightYAlignalTopTabOrder OnMouseMoveFormMouseMove 	TGroupBox	GroupBox7LeftTopWidthzHeight*CaptionRif./CodiceTabOrder OnMouseMoveFormMouseMove TSpeedButtonBModCodCommessaLeft\TopWidthHeight
Glyph.Data
z  v  BMv      v   (                                       А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 3333333333333333333333333333333€3333330 333€€учw330  0 ∞337ww7wч33ыы а 33€3wчw33 њ∞а 33ws37чw30ыыыра 3ч€€37чw0  њ∞а 7wws37чwыыыыра s€€€€7чw0   ∞а 7wwws7чw333ы   333s€www3330 3333337w3333333333333333333333333333333333333333333333333333333333333333333	NumGlyphsOnClickBModCodCommessaClick  TDBEditDBEdit1LeftTopWidthTHeightTabStopColor	clBtnFace	DataFieldProgressivo
DataSourceDataRicerche.DsRicerchePendEnabledTabOrder    	TGroupBox	GroupBox5LeftВ TopWidthcHeight*CaptionData inizioTabOrderOnMouseMoveFormMouseMove TDbDateEdit97DbDateEdit971LeftTopWidthWHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder ColorCalendar.ColorValidclBlueDate      DН@DayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanPopup
doIsMaskeddoShowCanceldoShowToday OnEnterDbDateEdit971EnterOnExitDbDateEdit971Exit	DataField
DataInizio
DataSourceDataRicerche.DsRicerchePend   	TGroupBox	GroupBox3LeftTop+Width|Height*CaptionRuolo richiestoTabOrderOnMouseMoveFormMouseMove TSpeedButtonSpeedButton1LeftZTopWidthHeightHint"inserisci o cambia ruolo richiesto
Glyph.Data
z  v  BMv      v   (                                       А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 3333333333€3333€39У3330 373337w39У3330 3ч€337w9ЩЩУ33337ww333?9ЩЩУ333 7wws333w39У3333 373333w39У3333337s3333?3333333 3333у33w333<333 3337€33w333<√3333€€ч€?у<ћћћћ√Щ37wwwwwу<ћћћћ√Щ37wwwwsw3333<√3333337s33€333<330 3337337w3333330 3333337w3333333333333333	NumGlyphsParentShowHintShowHint	OnClickSpeedButton1Click  TDBEditDBEdit3Leftќ TopWidthЙ HeightColor	clBtnFace	DataFieldArea
DataSourceDataRicerche.DsRicerchePendEnabledReadOnly	TabOrder   TDBEditDBEdit4LeftTopWidth≈ Height	DataFieldMansione
DataSourceDataRicerche.DsRicerchePendReadOnly	TabOrder   	TGroupBox	GroupBox2LeftДTop+Width5Height*Caption	richiestiTabOrderOnMouseMoveFormMouseMove TLabelLabel6LeftTopWidthHeightCaptionN∞  TDBEditDBEdit2LeftTopWidthHeight	DataFieldNumRicercati
DataSourceDataRicerche.DsRicerchePendFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder    	TGroupBox	GBClienteLeftdTopWidth» Height*CaptionClienteTabOrderOnMouseMoveFormMouseMove TSpeedButtonSpeedButton2Left© TopWidthHeight
Glyph.Data
z  v  BMv      v   (                                       А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 3333333333333333333333333333333€3333330 333€€учw330  0 ∞337ww7wч33ыы а 33€3wчw33 њ∞а 33ws37чw30ыыыра 3ч€€37чw0  њ∞а 7wws37чwыыыыра s€€€€7чw0   ∞а 7wwws7чw333ы   333s€www3330 3333337w3333333333333333333333333333333333333333333333333333333333333333333	NumGlyphsOnClickSpeedButton2Click  TDBEditDBEdit5LeftTopWidth° HeightColorclAqua	DataFieldCliente
DataSourceDataRicerche.DsRicerchePendReadOnly	TabOrder    	TGroupBox	GroupBox4Left/TopWidthxHeight*CaptionCapo commessaTabOrderOnMouseMoveFormMouseMove TSpeedButton
BUtenteModLeftYTopWidthHeightHintmodifica selezionatore
Glyph.Data
z  v  BMv      v   (                                    А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 3333333333333333333333333333333€3333330 333€€учw330  0 ∞337ww7wч33ыы а 33€3wчw33 њ∞а 33ws37чw30ыыыра 3ч€€37чw0  њ∞а 7wws37чwыыыыра s€€€€7чw0   ∞а 7wwws7чw333ы   333s€www3330 3333337w3333333333333333333333333333333333333333333333333333333333333333333	NumGlyphsParentShowHintShowHint	OnClickBUtenteModClick  TDBEditDBEdit8LeftTopWidthOHeight	DataFieldUtente
DataSourceDataRicerche.DsRicerchePendReadOnly	TabOrder    TBitBtnBitBtn1LeftЂTopWidthPHeight'CaptionOK-EsciTabOrderKindbkOK  	TGroupBox	GroupBox1LeftЉTop+Widthо Height*CaptionStato ricercaTabOrder TSpeedButton	BStatoRicLeft∞ TopWidthHeightHintmodifica lo stato della ricerca
Glyph.Data
z  v  BMv      v   (                                       А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ P     UWwwwwwuхИИИИИАU_€€€€ч_     Иuwwwww_uPА€€€АWч€€€uuч     ррАwwwwwWWч€€€€ Аuх€_хuwчPр €ррАWчuwUWччP€€€€ррАW€UUUWWч €€€€  wх€€хww	 р  р РwWwwWwч	€€€€ РUUUUwW €€€€ w_€_хwU  €UwuwUU€€€€ UU_€€wuU€   UUu€wwwuUUP UUUUUUWwUUUUU	NumGlyphsParentShowHintShowHint	OnClickBStatoRicClick  TSpeedButtonBStoricoRicLeftЌ TopWidthHeightHint7visualizza lo storico degli stati relativi alla ricerca
Glyph.Data
z  v  BMv      v   (                                       А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 3333333333333333333√3333333333333<3333337wу33333јр√33333w73333<€33337wуwу333ћј€р√333s7733<ћћ€33733wуwу3Lћћј€р√333773ƒћћћ€3wу3уwуwу<Lјћј€р√773ч7s3ƒћјћ33wу7уww33<L ћј3337w37у333ƒћћћ3333wу3ч3333<Lћ√333377s33333ƒ√333333ws3333333333333333333	NumGlyphsParentShowHintShowHint	OnClickBStoricoRicClick  TLabelLabel4LeftXTopWidthHeightCaptiondal  TDBEditDBEdit6LeftTopWidthNHeight	DataFieldStatoRic
DataSourceDataRicerche.DsRicerchePendFont.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontReadOnly	TabOrder   TDBEditDBEdit7LeftjTopWidthBHeight	DataField	DallaData
DataSourceDataRicerche.DsRicerchePendReadOnly	TabOrder   TPanelPanel4LeftЂTop0WidthRHeight%
BevelOuter	bvLoweredTabOrder TToolbarButton97ToolbarButton975LeftTopWidthPHeight#Caption
SpecificheFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.Style 
Glyph.Data
z  v  BMv      v   (                                       А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 333     333wwwww333€€€р?у€у??ч 0  € рwчww?sw7аыы€€рws3?33чањњ €ррwу3wsуч7аыыырррwу3?чч7ањњ  рwу3wwss7аыыыыр€рwу?€€ч37ањ   €рw€wwws?ч ы ∞€ рws€w73w730 €€р37wss3?ч330∞€€  33773€ww33рр33s7s730∞€€37ч33s3	€€ 33ws€€w3303   3373wwws3Layout
blGlyphTop	NumGlyphs
ParentFontSpacing OnClickToolbarButton975ClickOnMouseMoveFormMouseMove   	TGroupBox	GroupBox6Leftи TopWidthyHeight*CaptionTipo commessaTabOrder	OnMouseMoveFormMouseMove TSpeedButtonBTipoLeft[TopWidthHeightHintmodifica tipo commessa
Glyph.Data
z  v  BMv      v   (                                    А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 3333333333333333333333333333333€3333330 333€€учw330  0 ∞337ww7wч33ыы а 33€3wчw33 њ∞а 33ws37чw30ыыыра 3ч€€37чw0  њ∞а 7wws37чwыыыыра s€€€€7чw0   ∞а 7wwws7чw333ы   333s€www3330 3333337w3333333333333333333333333333333333333333333333333333333333333333333	NumGlyphsParentShowHintShowHint	OnClick
BTipoClick  TDBEditDBEdit9LeftTopWidthTHeight	DataFieldTipo
DataSourceDataRicerche.DsRicerchePendReadOnly	TabOrder     TPageControl	PCSelPersLeft TopYWidth€HeightЏ
ActivePageTSCandidatiAlignalClientFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.Style Images
ImageList1	MultiLine	
ParentFontTabOrderOnChangePCSelPersChange 	TTabSheetTSCandidatiCaption	Candidati TPanelPanel1Left Top WidthчHeightљAlignalClient
BevelOuter	bvLoweredTabOrder OnMouseMoveFormMouseMove TPanelPanel5LeftҐTopWidthTHeightїAlignalRight
BevelOuter	bvLoweredTabOrder OnMouseMoveFormMouseMove TToolbarButton97ToolbarButton9712LeftTopHWidthRHeight#Captionevento
Glyph.Data
z  v  BMv      v   (                                       А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 333333333€€€€€€у4DDDDDD37wwwwwwу4ћћћћћƒ37wwwwwwу4DИДD37wуss7w330ИБ€p3337у737у330ИЛчp3337?3у73333Бч3333sч?s33330Лp333337?w333333333333wу33330333337ww€3333±с3333w7wу330Иыp3337уswу330ИИp333чу3ww€у4DИИИДD37w€€€чwу4ћћћћћƒ37wwwwwwу4DDDDDD37wwwwww3Layout
blGlyphTop	NumGlyphsSpacing OnClickToolbarButton9712ClickOnMouseMoveFormMouseMove  TToolbarButton97ToolbarButton9713LeftTop WidthRHeight#CaptionCerca...Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold 
Glyph.Data
z  v  BMv      v   (                                       А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 3333333333333?3333330 333333чw333333 33333?ws333330 333333чw333333 333?€?ws337 ;p333?wsчw333ww ≥333w37ws330xшшp3337у337у33wПППЗs3373337?33шшшч333€€у33ЙЩЩЗ337ww333шшшч33sу333s33wПППЗs337у337у330xшшp3337?у3ч3333ww3333w?чs33337 333333ws3333Layout
blGlyphTop	NumGlyphs
ParentFontSpacing OnClickToolbarButton9713ClickOnMouseMoveFormMouseMove  TToolbarButton97BCurriculumLeftTopР WidthRHeight#Hintvisualizza curriculumCaption
Curriculum
Glyph.Data
z  v  BMv      v   (                                       А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 3333333333333?3333330 333333чw333333 33333?ws333330 333333чw333333 333?€?ws337 ;p333?wsчw333ww ≥333w37ws330ППp3337у3у7у33wшщшчs33737у7?33ПЙПЗ333ч€у33щЩЩч337ww333ПЙПЗ33sу7у3s33wшщшчs337у737у330ППp3337?у3ч3333ww3333w?чs33337 333333ws3333Layout
blGlyphTop	NumGlyphsParentShowHintShowHint	Spacing OnClickBCurriculumClickOnMouseMoveFormMouseMove  TToolbarButton97BEliminaLeftTopWidthRHeight#Hint,togli dai candidati selezionati  in evidenzaCaptionElimina
Glyph.Data
z  v  BMv      v   (                                       А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 30    3337wwwwу330чwwp3337ууучу330рААp3337ччччу330рАpp3337ччччу330рААp3337ччччу330рАpp3337ччччу330рААp333чччччуу00рАpp0377ччччч33 рААp33wччччs330рАpp3337ччччу330pppp3337чччч€33     33wwwww33ИЗww33€€€€33     33wwwwws3330wp333337€чу33330  333337ww333Layout
blGlyphTop	NumGlyphsParentShowHintShowHint	Spacing WordWrap	OnClickBEliminaClickOnMouseMoveFormMouseMove  TToolbarButton97ToolbarButton974LeftTophWidthRHeight#Hint)presentazione all'azienda via fax o EmailCaptioninvio file/Email
Glyph.Data
z  v  BMv      v   (                                       А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 333333333333у333333Р3333333w€333333Щ3?€€€€wу   ЩЩЩР3wwwwwww€€рЩЩЩЩ37wwww€рЩЩЩЩР?чwwwww рЩЩЩЩw7wwwws€рЩЩЩР3?чwwww3 €€рЩ3w337ws3€€€рР33?у€чw33 р  33w7wws33€р€333?чуs333рр3333w7ч3333€р3333€чs3333   33333www333333333333333333333Layout
blGlyphTop	NumGlyphsParentShowHintShowHint	Spacing WordWrap	OnClickToolbarButton974ClickOnMouseMoveFormMouseMove  TToolbarButton97ToolbarButton978LeftTopь WidthRHeight#Hint!visualizza la storia dei contattiCaptionStoria contatti
Glyph.Data
z  v  BMv      v   (                                       А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ UUU     UU_wwwwwUU €€€рU_w_€хWU   €р_wwwUW €€€рw_€€ч   рwwwW€€€р_€€ч   рwwwW€€€р_€UW €рwu_ч€€  _хww €wUu€€ U€€wU   UwwwuU   UUwwwuUU   UUUwwwuUUU   UUUUwwwuUUUULayout
blGlyphTop	NumGlyphsParentShowHintShowHint	Spacing OnClickToolbarButton978ClickOnMouseMoveFormMouseMove  TToolbarButton97TbBAltroNomeLeftTop#WidthRHeight#Hintseleziona dall'elenco generaleCaptionins. da elenco
Glyph.Data
z  v  BMv      v   (                                       А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ U    UUUwwwwхUU    АUUUwwwww€UUPїї∞UUWх€чwхUP∞ ∞0АUUччwччwхP   ∞3 UWwwwчхwUPїї∞ 3UWх€чwхUP∞ ∞А3UWчwWчхUPїї∞А3UWхUWчхUPїї∞ 3UW€€чwхUP   p3UWwwwWхUUP€чp3UUW€€ч€UU    UUwwwwUUwww UUu€UUwUUP≥≥∞UUWu€UWхUUP;; UUUWu€€wхUUUp   UUUUWwwwULayout
blGlyphTop	NumGlyphsParentShowHintShowHint	Spacing OnClickTbBAltroNomeClickOnMouseMoveFormMouseMove  TToolbarButton97	BInfoCandLeftTopmWidthRHeight#Hintscheda sintetica candidatoCaption
Info cand.Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold 
Glyph.Data
z  v  BMv      v   (                                    А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 33333333?€€€€€€€        wwwwwwww€€€€€€р?у€?€?ч р  рw7swsw7€€€€€€р?у€3€€ч рр  рw7s7wwчоопрщьр3337ччч€€€рщьр?€у7ч7ч  €р€ьрwwу7у77  €р€€рww37337€€€€€€р€€€€€€чћћћћћћјwwwwwwwwМћћћћ»Аs7wwwws7        wwwwwwww33333333333333333333333333333333Layout
blGlyphTop	NumGlyphs
ParentFontParentShowHintShowHint	Spacing OnClickBInfoCandClickOnMouseMoveFormMouseMove  TToolbarButton97ToolbarButton972LeftTop≥ WidthRHeight#HintBvisualizza curriculum scannerizzato 
o file collegati al soggettoCaption	DocumentiFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold 
Glyph.Data
z  v  BMv      v   (                                       А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 3їїїїїї33wwwwww3ї їїїї33w333у3їїї∞ї33sу37€3ї∞її 337€3ws3ї∞∞ї3377s33її її333w333її її333w333оо оо33?w€33оА о33sчws33оаАоо3377333оо оо333w333ооаоо3337у33ооо оо3333wу33ооо оо3333w333оооооо33€€€€€3оооооо33wwwwwwsLayout
blGlyphTop	NumGlyphs
ParentFontParentShowHintShowHint	Spacing OnClickToolbarButton972ClickOnMouseMoveFormMouseMove  TToolbarButton97ToolbarButton973LeftTop÷ WidthRHeight#Hint/visualizza file di Word 
collegato al soggettoCaptionAppunti WordFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold 
Glyph.Data
z  v  BMv      v   (                                       А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 333333333333€333333<333333чwу3333<јр√33337w73333√3333sswу33< €рр√33чw3773<ј€€€37w?33swу√€€рр√ssу377 €р€€€w37?33sw€€€€ррsу3sу3770€€р€€€7?37?3?s3€€€ 33sу3s€w330€€р 3337?37w3333€33333s€s333330 3333337w333333333333333333333333333333333333Layout
blGlyphTop	NumGlyphs
ParentFontParentShowHintShowHint	Spacing OnClickToolbarButton973ClickOnMouseMoveFormMouseMove  TBevelBevel1LeftTopFWidthRHeightStylebsRaised  TBevelBevel2LeftTopkWidthRHeightStylebsRaised  TBevelBevel3LeftTopщ WidthRHeightStylebsRaised  TBevelBevel4LeftTopeWidthRHeightStylebsRaised  TToolbarButton97	BRecuperaLeftTopBWidthRHeight#Hint,togli dai candidati selezionati  in evidenzaCaptionReinserisciEnabled
Glyph.Data
z  v  BMv      v   (                                       А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 33333333333€€у33339ЩЩ33333чww€у339ЩЩЩЩ3337w37w€33ЩУpsЩУ33w37уwу9Щ3 9Щ37s3w3wу9У3ps3Щ37s37337€Щ333339Уwу33у37Щ330339Уwу37€33sЩ33ps333wу3w333Щ33333wу3w?€€Щ33 ЩЩУw€3www9У3 9ЩУ7?уw7w9Щ3 3ЩУ7s€w?w3ЩУpsЩЩУ3w?7?ww39ЩЩЩЩ3У37w37w3s339ЩЩ333337ww333Layout
blGlyphTop	NumGlyphsParentShowHintShowHint	Spacing WordWrap	OnClickBRecuperaClickOnMouseMoveFormMouseMove  TToolbarButton97BStampaElencoLeftTopПWidthRHeight#Hint)presentazione all'azienda via fax o EmailDropdownMenuPMStampaCaptionstampa/esp.
Glyph.Data
z  v  BMv      v   (                                       А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 0      ?€€€€€€€ИИИИИИАwwwwwwwИИИИИИАwwwwwww        €€€€€€€€ППППППАwwwwwwwшшшшшщрwwwwwwwППППППАwwwwww        wwwwwww30€€€€337у€€?330р 337чwss330€€€€337у€?€330р  337чswws330€€р3337у€73330рП3337чss3330€€ 33337€€w33330  33337wws333Layout
blGlyphTop	NumGlyphsParentShowHintShowHint	Spacing WordWrap	OnMouseMoveFormMouseMove  TBevelBevel5Left TopМWidthRHeightStylebsRaised   TPanelPanel7LeftTopWidth°HeightїAlignalClient
BevelOuterbvNoneTabOrder TPanelPanel8Left Top Width°HeightAlignalTop	AlignmenttaLeftJustifyCaption  Situazione contatti/colloquiColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Heightф	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder OnMouseMoveFormMouseMove TLabelLTotCandLeftDTopWidthDHeight	AlignmenttaRightJustifyCaptionLTotCandFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Heightу	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  	TCheckBoxCBOpzioneCandLeftС TopWidthЅ HeightCaption%non visualizzare i candidati esclusi Checked	Font.CharsetDEFAULT_CHARSET
Font.ColorclYellowFont.Heightф	Font.NameMS Sans Serif
Font.Style 
ParentFontState	cbCheckedTabOrder OnClickCBOpzioneCandClick   TPanelPanel3Left TopЭWidth°HeightAlignalBottom
BevelOuter	bvLoweredTabOrder TSpeedButtonSpeedButton3LeftTopWidthHeightHintlegenda e parametri
Glyph.Data
z  v  BMv      v   (                                    А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 33333333333€€у3333?y33333ч77?у33;ыЩЫы3337swsw?33њњyњ≥33s3733sу;ыыыыыы37у33у37у?њњєњњњ37337у37?ыыыщ{ыыу337€33њњњєЯњњ≥337у3ыыыыЩыыу333w€3њњwњyЯњ≥3?у73ыыЩыyЫыуsуwу73s?њЩyЯњ37уw€чs7у;ыщЩЩыы37?7ww3733њњЩЯњ≥33s€ws?s33;ыыыы3337s€€w3333?њњ333337ww333	NumGlyphsParentShowHintShowHint	OnClickSpeedButton3Click  TPanel	PanTimingLeft#TopWidthщHeight
BevelOuterbvNoneTabOrder  TLabelLabel1Left± TopWidthHeightCaptionTot.Font.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel2Leftл TopWidth HeightCaptionAttivitаFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel3Left2TopWidth± HeightCaption'--> inattivitа settata dopo 120 secondiFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLTimingLeftTopWidth† HeightCaption&TIMING (riferito all'utente Giustini):  TEditSecondiAttivitaLeftTopWidthHeightBiDiModebdLeftToRightEnabledFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Heightх	Font.NameMS Sans Serif
Font.Style ParentBiDiMode
ParentFontTabOrder Text0  TEditSecondiTotaliLeft« TopWidthHeightBiDiModebdLeftToRightEnabledFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Heightх	Font.NameMS Sans Serif
Font.Style ParentBiDiMode
ParentFontTabOrderText0    	TdxDBGrid	dxDBGrid1Left TopWidth°HeightЖBands  DefaultLayout	HeaderPanelRowCountKeyFieldIDShowGroupPanel	SummaryGroups SummarySeparator, AlignalClient	PopupMenuPMCandTabOrderOnClickdxDBGrid1Click
OnDblClickdxDBGrid1DblClick	OnMouseUpdxDBGrid1MouseUp
DataSourceDataRicerche.DsQCandRicFilter.AutoDataSetFilter	Filter.Criteria
       OptionsBehavioredgoAutoSortedgoDragScrolledgoEditingedgoEnterShowEditoredgoImmediateEditoredgoStoreToRegistryedgoTabThroughedgoVertThrough 	OptionsDBedgoCancelOnExitedgoCanDeleteedgoCanInsertedgoCanNavigationedgoConfirmDeleteedgoLoadAllRecordsedgoSyncSelectionedgoUseBookmarks OptionsViewedgoAutoWidthedgoBandHeaderWidthedgoIndicatoredgoUseBitmap RegistryPath#\Software\H1\DbgridSelPersCandidatiOnCustomDrawCelldxDBGrid1CustomDrawCell TdxDBGridMaskColumndxDBGrid1IDDisableCustomizing	DisableEditor	VisibleWidth-	BandIndex RowIndex 	FieldNameID  TdxDBGridMaskColumndxDBGrid1CVNumeroCaptionCV N∞DisableEditor	Width4	BandIndex RowIndex 	FieldNameCVNumero  TdxDBGridMaskColumndxDBGrid1CognomeDisableEditor	Widthh	BandIndex RowIndex 	FieldNameCognome  TdxDBGridMaskColumndxDBGrid1NomeDisableEditor	WidthT	BandIndex RowIndex 	FieldNameNome  TdxDBGridDateColumndxDBGrid1DataInsCaptionInserito ilDisableEditor	SortedcsDownWidthB	BandIndex RowIndex 	FieldNameDataIns  TdxDBGridDateColumndxDBGrid1DataImpegnoCaptionImpegnoDisableEditor	Font.CharsetDEFAULT_CHARSET
Font.ColorclBlueFont.Heightх	Font.NameMS Sans Serif
Font.Style WidthB	BandIndex RowIndex 	FieldNameDataImpegno  TdxDBGridDateColumndxDBGrid1DataUltimoContattoCaptionUltimo cont.DisableEditor	Width2	BandIndex RowIndex 	FieldNameDataUltimoContatto  TdxDBGridMaskColumndxDBGrid1StatoCaption
SituazioneDisableEditor	Widthу 	BandIndex RowIndex 	FieldNameStato  TdxDBGridMaskColumndxDBGrid1MiniValCaptionV.DisableEditor	Font.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Heightх	Font.NameMS Sans Serif
Font.Style Width	BandIndex RowIndex 	FieldNameMiniVal  TdxDBGridMaskColumndxDBGrid1IDRicercaDisableCustomizing	DisableEditor	VisibleWidth-	BandIndex RowIndex 	FieldName	IDRicerca  TdxDBGridMaskColumndxDBGrid1IDAnagraficaDisableCustomizing	DisableEditor	VisibleWidth-	BandIndex RowIndex 	FieldNameIDAnagrafica  TdxDBGridCheckColumndxDBGrid1EsclusoDisableCustomizing	DisableEditor	VisibleWidth$	BandIndex RowIndex 	FieldNameEsclusoValueCheckedTrueValueUncheckedFalse  TdxDBGridMaskColumndxDBGrid1CodstatoDisableCustomizing	DisableEditor	VisibleWidth-	BandIndex RowIndex 	FieldNameCodstato  TdxDBGridMaskColumndxDBGrid1TipoStatoCaptionStatoDisableEditor	VisibleWidth-	BandIndex RowIndex 	FieldName	TipoStato  TdxDBGridMaskColumndxDBGrid1IDStatoDisableCustomizing	DisableEditor	VisibleWidth-	BandIndex RowIndex 	FieldNameIDStato  TdxDBGridColumndxDBGrid1TelUffCellCaptionTelf.Ufficio/cell.DisableEditor	VisibleWidth-	BandIndex RowIndex 	FieldName
TelUffCell  TdxDBGridMaskColumndxDBGrid1CellulareDisableEditor	VisibleWidth-	BandIndex RowIndex 	FieldName	Cellulare  TdxDBGridMaskColumndxDBGrid1telUfficioCaptionTel.UfficioDisableEditor	VisibleWidth-	BandIndex RowIndex 	FieldName
telUfficio  TdxDBGridDateColumndxDBGrid1DataNascitaCaptionData di nascitaDisableEditor	VisibleWidth-	BandIndex RowIndex 	FieldNameDataNascita  TdxDBGridColumndxDBGrid1EtaCaptionEtаDisableEditor	VisibleWidth-	BandIndex RowIndex 	FieldNameEta  TdxDBGridLookupColumndxDBGrid1AziendaCaptionAzienda lav.attualeDisableEditor	VisibleWidth-	BandIndex RowIndex 	FieldNameAzienda  TdxDBGridColumndxDBGrid1TelAziendaCaptionTel.Azienda attualeVisible	BandIndex RowIndex 	FieldName
TelAzienda  TdxDBGridColumndxDBGrid1Column24CaptionTelefono casaVisible	BandIndex RowIndex 	FieldNameRecapitiTelefonici  TdxDBGridColumndxDBGrid1NoteWidth)	BandIndex RowIndex 	FieldNameNoteDisableFilter	  TdxDBGridColumndxDBGrid1Column25CaptionTitolo di studioVisible	BandIndex RowIndex 	FieldNameDiploma      	TTabSheetTSCandAnnunciCaptionAnnunci
ImageIndex TPanelPanel6Left Top Width§HeightљAlignalLeftAnchorsakLeftakTopakRightakBottom 
BevelOuter	bvLoweredTabOrder  TPanelPanel10LeftTopWidthҐHeightAlignalTop	AlignmenttaLeftJustifyCaptionH  Candidati associati agli annunci pervenuti per questa ricerca/commessaColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Heightф	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder OnMouseMoveFormMouseMove TLabelLTotCandAnnLefthTopWidth Height	AlignmenttaRightJustifyCaptionLTotFont.CharsetDEFAULT_CHARSET
Font.ColorclYellowFont.Heightу	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont   TPanelPanel11LeftTop†WidthҐHeightAlignalBottom
BevelOuter	bvLoweredTabOrder TLabelLabel5LeftTopWidthYHeightCaptionLegenda colori:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TShapeShape1LeftgTopWidth%HeightBrush.ColorclRed  TLabelLabel7LeftС TopWidthК HeightCaptionappartenenti a questa ricerca  	TCheckBoxCBColoriCandAnnunciLeft%TopWidthpHeightCaptionabilita colorazioneTabOrder OnClickCBColoriCandAnnunciClick   	TdxDBGridDBGAnnunciCandLeftTopWidthҐHeightИBands  DefaultLayout	HeaderPanelRowCountKeyFieldIDAnagraficaSummaryGroups SummarySeparator, AlignalClientTabOrder
DataSourceDataRicerche.DsQAnnunciCandFilter.Criteria
       OptionsBehavioredgoAutoSearchedgoAutoSortedgoDragScrolledgoEnterShowEditoredgoImmediateEditoredgoTabThroughedgoVertThrough 	OptionsDBedgoCancelOnExitedgoCanDeleteedgoCanInsertedgoCanNavigationedgoConfirmDeleteedgoLoadAllRecordsedgoUseBookmarks OptionsViewedgoAutoWidthedgoBandHeaderWidthedgoIndicatoredgoUseBitmap OnCustomDrawCellDBGAnnunciCandCustomDrawCell TdxDBGridMaskColumnDBGAnnunciCandCognomeSortedcsUpWidtho	BandIndex RowIndex 	FieldNameCognome  TdxDBGridMaskColumnDBGAnnunciCandNomeWidthl	BandIndex RowIndex 	FieldNameNome  TdxDBGridMaskColumnDBGAnnunciCandRifCaptionRif.annuncioWidth%	BandIndex RowIndex 	FieldNameRif  TdxDBGridMaskColumnDBGAnnunciCandTestataWidthИ 	BandIndex RowIndex 	FieldNameTestata  TdxDBGridMaskColumnDBGAnnunciCandNomeEdizioneCaptionEdizioneWidthИ 	BandIndex RowIndex 	FieldNameNomeEdizione  TdxDBGridDateColumnDBGAnnunciCandDataWidth3	BandIndex RowIndex 	FieldNameData  TdxDBGridMaskColumnDBGAnnunciCandCVNumeroCaptionN∞ CVWidth4	BandIndex RowIndex 	FieldNameCVNumero  TdxDBGridDateColumnDBGAnnunciCandCVInseritoIndataCaptionInserito ilWidth2	BandIndex RowIndex 	FieldNameCVInseritoIndata  TdxDBGridColumnDBGAnnunciCandIDAnagraficaVisible	BandIndex RowIndex 	FieldNameIDAnagrafica    TPanelPanel9Left§Top WidthSHeightљAlignalClientAnchorsakTopakRightakBottom 
BevelOuter	bvLoweredTabOrder TToolbarButton97BCurriculum1LeftTopfWidthRHeight#Hintvisualizza curriculumCaption
Curriculum
Glyph.Data
z  v  BMv      v   (                                       А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 3333333333333?3333330 333333чw333333 33333?ws333330 333333чw333333 333?€?ws337 ;p333?wsчw333ww ≥333w37ws330ППp3337у3у7у33wшщшчs33737у7?33ПЙПЗ333ч€у33щЩЩч337ww333ПЙПЗ33sу7у3s33wшщшчs337у737у330ППp3337?у3ч3333ww3333w?чs33337 333333ws3333Layout
blGlyphTop	NumGlyphsParentShowHintShowHint	Spacing OnClickBCurriculum1ClickOnMouseMoveFormMouseMove  TToolbarButton97
BInfoCand2LeftTopCWidthRHeight#Hintscheda sintetica candidatoCaption
Info cand.Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold 
Glyph.Data
z  v  BMv      v   (                                    А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 33333333?€€€€€€€        wwwwwwww€€€€€€р?у€?€?ч р  рw7swsw7€€€€€€р?у€3€€ч рр  рw7s7wwчоопрщьр3337ччч€€€рщьр?€у7ч7ч  €р€ьрwwу7у77  €р€€рww37337€€€€€€р€€€€€€чћћћћћћјwwwwwwwwМћћћћ»Аs7wwwws7        wwwwwwww33333333333333333333333333333333Layout
blGlyphTop	NumGlyphs
ParentFontParentShowHintShowHint	Spacing OnClickBInfoCand2ClickOnMouseMoveFormMouseMove  TToolbarButton97ToolbarButton977LeftTopЙ WidthRHeight#Hintvisualizza curriculumCaption	DocumentiFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold 
Glyph.Data
z  v  BMv      v   (                                       А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 3їїїїїї33wwwwww3ї їїїї33w333у3їїї∞ї33sу37€3ї∞її 337€3ws3ї∞∞ї3377s33її її333w333її її333w333оо оо33?w€33оА о33sчws33оаАоо3377333оо оо333w333ооаоо3337у33ооо оо3333wу33ооо оо3333w333оооооо33€€€€€3оооооо33wwwwwwsLayout
blGlyphTop	NumGlyphs
ParentFontParentShowHintShowHint	Spacing OnClickToolbarButton977ClickOnMouseMoveFormMouseMove  TToolbarButton97ToolbarButton979LeftTopђ WidthRHeight#Hintvisualizza curriculumCaption	File WordFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold 
Glyph.Data
z  v  BMv      v   (                                       А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 333333333333€333333<333333чwу3333<јр√33337w73333√3333sswу33< €рр√33чw3773<ј€€€37w?33swу√€€рр√ssу377 €р€€€w37?33sw€€€€ррsу3sу3770€€р€€€7?37?3?s3€€€ 33sу3s€w330€€р 3337?37w3333€33333s€s333330 3333337w333333333333333333333333333333333333Layout
blGlyphTop	NumGlyphs
ParentFontParentShowHintShowHint	Spacing OnClickToolbarButton979ClickOnMouseMoveFormMouseMove  TBevelBevel6LeftTop@WidthRHeightStylebsRaised  TBevelBevel7LeftTop– WidthRHeightStylebsRaised  TToolbarButton97ToolbarButton971LeftTopWidthRHeight#Hint%associa il candidato a questa ricercaCaption	InserisciFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold 
Glyph.Data
z  v  BMv      v   (                                    А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 3333333333€3333€39У3330 373337w39У3330 3ч€337w9ЩЩУ33337ww333?9ЩЩУ333 7wws333w39У3333 373333w39У3333337s3333?3333333 3333у33w333<333 3337€33w333<√3333€€ч€?у<ћћћћ√Щ37wwwwwу<ћћћћ√Щ37wwwwsw3333<√3333337s33€333<330 3337337w3333330 3333337w3333333333333333Layout
blGlyphTop	NumGlyphs
ParentFontParentShowHintShowHint	Spacing OnClickToolbarButton971ClickOnMouseMoveFormMouseMove  TBevelBevel8LeftTopWidthRHeightStylebsRaised  TToolbarButton97ToolbarButton976LeftTop” WidthRHeight#Hintdettaglio dati dell'annuncioCaptioninfo annuncio
Glyph.Data
z  v  BMv      v   (                                    А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 33303333337s€333330р3333377s€3333€р333337s€333€€р333s?у7s330€ €€А337уw?37330€€€р?€ч€уs?7    €€рwwww?уs7     €€р€€уw€у7ћћ   €www37w?s ћј  3 0ws37уw7    330733чу37   {p330€у7чу37	Щ ї∞330wwуww€37	Щ {p3wwу77s€s	Щ   0 3ww€€ччw3     333wwwww333Layout
blGlyphTop	NumGlyphsParentShowHintShowHint	Spacing OnClickToolbarButton976ClickOnMouseMoveFormMouseMove    	TTabSheet	TSCliCandCaptionSituaz.candidati
ImageIndex фTCliCandidatiFrameCliCandidatiFrame1WidthчHeightљAlignalClient сTPanelPanel1WidthчHeightљ сTPanelPanel104Widthх  с	TTreeViewTVWidthхHeight¶     	TTabSheetTSRicParamsCaption	Parametri
ImageIndex TPanelPanel16Left Top WidthчHeightљAlignalClient
BevelOuter	bvLoweredTabOrder  TLabelLabel11LeftTopWidthuHeightCaptionNNOTA: se i valori sono nulli o zero, verranno considerati i parametri globali.Font.CharsetANSI_CHARSET
Font.ColorclPurpleFont.Heightх	Font.NameArial
Font.StylefsItalic 
ParentFont  TLabelLabel12LeftTopѕ WidthЯHeightCaptionРSono gli stessi che appaiono nel motore di ricerca ma salvati per questa commessa:
per modificarli и necessario utilizzare il motore di ricercaFont.CharsetANSI_CHARSET
Font.ColorclPurpleFont.Heightх	Font.NameArial
Font.StylefsItalic 
ParentFont  TPanelPanel17LeftTop-WidthuHeight)
BevelInnerbvSpace
BevelOuter	bvLoweredTabOrder  TLabelLabel13Left&TopWidthґ HeightCaption7n∞ di giorni dalla data di prima apertura della ricercaWordWrap	  TLabelLabel14LeftTopWidthHeightCaptionApFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightу	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel15Leftй TopWidthWHeightCaption!carattere rosso se piщ di giorni Font.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontWordWrap	  TdxDBSpinEditdxDBSpinEdit1LeftFTopWidth)Style.ButtonStylebtsFlatStyle.ButtonTransparenceebtNoneStyle.ShadowTabOrder 	DataFieldggDiffApRic
DataSourceDataRicerche.DsRicerchePend   TPanelPanel18LeftTopZWidthuHeight)
BevelInnerbvRaised
BevelOuter	bvLoweredTabOrder TLabelLabel16Left&TopWidth≥ HeightCaptionHn∞ di giorni dall'ultimo CONTATTO con un soggetto associato alla ricercaWordWrap	  TLabelLabel17LeftTopWidthHeightCaptionUcFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightу	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel9Leftй TopWidthWHeightCaption!carattere rosso se piщ di giorni Font.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontWordWrap	  TdxDBSpinEditdxDBSpinEdit2LeftFTopWidth)Style.ButtonStylebtsFlatTabOrder 	DataFieldggDiffUcRic
DataSourceDataRicerche.DsRicerchePend   TPanelPanel19LeftTopЗ WidthuHeight)
BevelInnerbvRaised
BevelOuter	bvLoweredTabOrder TLabelLabel19LeftTopWidthHeightCaptionColFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightу	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel20Left&TopWidth≥ HeightCaptionIn∞ di giorni dall'ultimo COLLOQUIO con un soggetto associato alla ricercaWordWrap	  TLabelLabel10Leftй TopWidthWHeightCaption!carattere rosso se piщ di giorni Font.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontWordWrap	  TdxDBSpinEditdxDBSpinEdit3LeftFTopWidth)Style.ButtonStylebtsFlatTabOrder 	DataFieldggDiffColRic
DataSourceDataRicerche.DsRicerchePend   TPanelPanel20LeftTopWidthхHeightAlignalTop	AlignmenttaLeftJustifyCaption/  parametri temporali specifici per la commessaColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Heightф	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderOnMouseMoveFormMouseMove  TPanelPanel21LeftTopµ WidthвHeight	AlignmenttaLeftJustifyCaption2  criteri di ricerca impostati per questa commessaColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Heightф	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderOnMouseMoveFormMouseMove  TDBGridDBGrid1LeftTopо Width<HeightuAnchorsakLeftakTopakRightakBottom 
DataSourceDataRicerche.DsQRicLineeQueryOptionsdgTitlesdgIndicatordgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgConfirmDeletedgCancelOnExit ReadOnly	TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.HeightхTitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameDescrizioneTitle.CaptioncriterioVisible	 Expanded	FieldNameNextVisible	      	TTabSheetTSTimeSheetCaptionTime Report
ImageIndex 	TSplitter	Splitter2LeftЋTop WidthHeightљCursorcrHSplit  TPanelPanTimeSheetLeft Top WidthЋHeightљAlignalLeft
BevelOuter	bvLoweredTabOrder  TPanelPanel24LeftTopWidth…Height'AlignalTop
BevelOuter	bvLoweredTabOrder  TToolbarButton97BTimeSheetNewLeftTopWidth:Height%Caption
Nuova voceOpaqueWordWrap	OnClickBTimeSheetNewClick  TToolbarButton97BTimeSheetDelLeft;TopWidth:Height%CaptionElimina voceOpaqueWordWrap	OnClickBTimeSheetDelClick  TToolbarButton97BTimeSheetCanLeftѓ TopWidth:Height%CaptionAnnulla modificheEnabledOpaqueWordWrap	OnClickBTimeSheetCanClick  TToolbarButton97BTimeSheetOKLeftuTopWidth:Height%CaptionConferma modificheEnabledOpaqueWordWrap	OnClickBTimeSheetOKClick   	TdxDBGrid	dxDBGrid5LeftTop(Width…HeightФBands  DefaultLayout	HeaderPanelRowCountKeyFieldIDShowSummaryFooter	SummaryGroups SummarySeparator, AlignalClientTabOrder
DataSourceDataRicerche.DsQRicTimeSheetFilter.Criteria
       	OptionsDBedgoCancelOnExitedgoCanDeleteedgoCanInsertedgoCanNavigationedgoConfirmDeleteedgoLoadAllRecordsedgoUseBookmarks  TdxDBGridExtLookupColumndxDBGrid5UtenteSortedcsUpWidth|	BandIndex RowIndex 	FieldNameUtenteDBGridLayoutdxDBGridLayoutList1Item1  TdxDBGridExtLookupColumndxDBGrid5Column5CaptionGruppo Profess.Width}	BandIndex RowIndex 	FieldNameGruppiProfessDBGridLayoutdxDBGridLayoutList1Item2  TdxDBGridDateColumndxDBGrid5DataWidthN	BandIndex RowIndex 	FieldNameData  TdxDBGridMaskColumndxDBGrid5OreConsuntivo	AlignmenttaCenterCaptionGiorni cons.Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold WidthA	BandIndex RowIndex 	FieldNameOreConsuntivoSummaryFooterTypecstSumSummaryFooterFieldOreConsuntivo  TdxDBGridBlobColumndxDBGrid5NoteWidth%	BandIndex RowIndex HeaderMaxLineCount 	FieldNameNote    TPanelPanel25LeftќTop Width)HeightљAlignalClient
BevelOuter	bvLoweredTabOrder TPanelPanel30LeftTopWidth'HeightAlignalTop	AlignmenttaLeftJustifyCaption2  Dettaglio per la voce di time-report selezionataColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Heightф	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder OnMouseMoveFormMouseMove  TPanelPanel31LeftTopWidth'Height%AlignalTop
BevelOuter	bvLoweredTabOrder TToolbarButton97BTRepDettNewLeftTopWidth7Height#Caption
Nuova voceOpaqueWordWrap	OnClickBTRepDettNewClick  TToolbarButton97BTRepDettDelLeft8TopWidth7Height#CaptionElimina voceOpaqueWordWrap	OnClickBTRepDettDelClick  TToolbarButton97BTRepDettCanLeft¶ TopWidth7Height#CaptionAnnulla modificheEnabledOpaqueWordWrap	OnClickBTRepDettCanClick  TToolbarButton97BTRepDettOKLeftoTopWidth7Height#CaptionConferma modificheEnabledOpaqueWordWrap	OnClickBTRepDettOKClick  TToolbarButton97BAggTotTRepLeftа TopWidth"HeightHintaggiorna totale time report
Glyph.Data
z  v  BMv      v   (                                    А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 3p    s337wwww?33ИИИИ33???€33ЩШ33ssws33ИИИИ33????3333ssss33ИИИИ33????3333ssss33ИИИИ33????3333ssss33ИИИИ33?€€€33   33www33 33€€33   33wwws33ИИИИ33333333ИИИИ33s€€€€s33p    s337wwww33	NumGlyphsOpaqueParentShowHintShowHint	WordWrap	OnClickBAggTotTRepClick  TToolbarButton97ToolbarButton9711LeftTopWidth"HeightHinttabella causali
Glyph.Data
:  6  BM6      6  (                                Д   ДДД ∆∆∆ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€                             OpaqueParentShowHintShowHint	WordWrap	OnClickToolbarButton9711Click   	TdxDBGrid	dxDBGrid6LeftTop=Width'HeightBands  DefaultLayout	HeaderPanelRowCountKeyFieldIDSummaryGroups SummarySeparator, AlignalClientTabOrder
DataSourceDataRicerche.DsQTimeRepDettFilter.Criteria
       	OptionsDBedgoCancelOnExitedgoCanDeleteedgoCanInsertedgoCanNavigationedgoConfirmDeleteedgoLoadAllRecordsedgoUseBookmarks OptionsViewedgoAutoWidthedgoBandHeaderWidthedgoUseBitmap  TdxDBGridLookupColumndxDBGrid6CausaleSortedcsUpWidth« 	BandIndex RowIndex 	FieldNameCausale  TdxDBGridMaskColumndxDBGrid6Ore	AlignmenttaCenterWidth.	BandIndex RowIndex 	FieldNameOre  TdxDBGridBlobColumndxDBGrid6NoteWidth.	BandIndex RowIndex HeaderMaxLineCount 	FieldNameNote     	TTabSheet
TSGestioneCaptionControllo gestione
ImageIndex TPanelPanel22Left Top WidthчHeightAlignalTop	AlignmenttaLeftJustifyCaption(  Dati relativi al controllo di gestioneColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Heightф	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder OnMouseMoveFormMouseMove TLabelLabel42LeftTopWidthш HeightCaption)ATTENZIONI: Funzioni in fase sperimentaleFont.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Heightф	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont   	TGroupBoxGBProvvigioneLeftTopWidthс Height?Caption Provvigione per il selezionatoreTabOrder TLabelLabel21LeftС TopWidth<HeightCaptionPercentuale:  TLabelLabel22LeftTop'Width6HeightCaption	Pagamento  TDBCheckBoxDBCheckBox1LeftTopWidthOHeightCaptionProvvigione	DataFieldProvvigione
DataSourceDataRicerche.DsRicerchePendTabOrder ValueCheckedTrueValueUncheckedFalse  TDBEditDBEdit12Leftѕ TopWidthHeight	DataFieldProvvigionePerc
DataSourceDataRicerche.DsRicerchePendTabOrder  TDBComboBoxDBComboBox1LeftCTop$Width® Height	DataFieldQuandoPagamUtente
DataSourceDataRicerche.DsRicerchePend
ItemHeightItems.Stringsemissione fattura!alla data di previsto pagam.fatt.alla data di pagamento reale TabOrder   	TGroupBoxGBOreLavLeftTop_Widthс HeightjCaptionOre lavorate sulla commessaTabOrder TLabelLabel23LeftМ TopWidth9HeightCaptionval.manualeWordWrap	  TLabelLabel24Left… TopWidthHeightCaptionTotaleWordWrap	  TEditEUtenteLav1LeftTopWidthВ HeightReadOnly	TabOrder   TEditEUtenteLav2LeftTop5WidthВ HeightReadOnly	TabOrder  TEditEUtenteLav3LeftTopMWidthВ HeightReadOnly	TabOrder  TdxDBSpinEdit	SEOreLav1LeftМ TopWidth1TabOrderOnExitSEOreLav1Exit	DataFieldOreLavUtente1
DataSourceDataRicerche.DsRicerchePend  TdxDBSpinEdit	SEOreLav2LeftМ Top5Width1TabOrder	DataFieldOreLavUtente2
DataSourceDataRicerche.DsRicerchePend  TdxDBSpinEdit	SEOreLav3LeftМ TopMWidth1TabOrder	DataFieldOreLavUtente3
DataSourceDataRicerche.DsRicerchePend  TEditETotOreLav1Leftј TopWidth)HeightColorclInfoBkReadOnly	TabOrder   	TGroupBoxGBRiepTimingLeftTopѕ Widthс HeightИ Caption)Riepilogo timing calcolato sulla commessaTabOrder 	TdxDBGrid	dxDBGrid2LeftTopWidthб HeightpBands  DefaultLayout	HeaderPanelRowCountKeyFieldIDSummaryGroups SummarySeparator, TabOrder 
DataSourceDataRicerche.DsQRicTimingFilter.Criteria
       OptionsBehavioredgoAutoSortedgoDragScrolledgoEnterShowEditoredgoImmediateEditoredgoTabThroughedgoVertThrough 	OptionsDBedgoCancelOnExitedgoCanDeleteedgoCanInsertedgoCanNavigationedgoConfirmDeleteedgoLoadAllRecordsedgoUseBookmarks OptionsViewedgoBandHeaderWidthedgoRowSelectedgoUseBitmap  TdxDBGridMaskColumndxDBGrid2NominativoCaptionUtenteWidthЙ 	BandIndex RowIndex 	FieldName
Nominativo  TdxDBGridMaskColumndxDBGrid2TotCaption	Tot (min)SortedcsDownWidthB	BandIndex RowIndex 	FieldNameTot    	TGroupBox	GBCalcoloLeftэ TopWidth!HeightUCaptionCalcolo costi e ricaviTabOrder TLabelLabel26Left	Top:Width8HeightCaption
(1) RicaviFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel27Leftґ Top;WidthHeightCaption+Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel28Left	TopmWidthaHeightCaption(2) Costi annunciFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel29LeftЈ TopnWidthHeightCaption-Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel30Left	TopК WidthЬ HeightCaption(3) Costi fissi risorse umaneFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel31LeftЈ TopЛ WidthHeightCaption-Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel32Left	TopЮ WidthwHeightCaption)costo medio orario (fisso) x ore lavorateWordWrap	  TLabelLabel33Left	TopLWidthqHeightCaption'somma compensi pattuiti per la commessaWordWrap	  TLabelLabel34Left	Topј Width¶ HeightCaption(4) Provvigione selezionatoreFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel35LeftЈ TopЅ WidthHeightCaption-Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel36Left	Top” WidthgHeightCaption(% provvigione sul complessivo dei ricaviWordWrap	  TLabelLabel37Left	Top9WidthwHeightCaptionMargine complessivoFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TBevelBevel9Left	Top.WidthHeightShape	bsTopLine  TBevelBevel10Left	TopJWidthє HeightShape	bsTopLineStylebsRaised  TBevelBevel11Left	Top}Widthє HeightShape	bsTopLineStylebsRaised  TBevelBevel12Left	TopЫ Widthє HeightShape	bsTopLineStylebsRaised  TBevelBevel13Left	Top– Widthє HeightShape	bsTopLineStylebsRaised  TLabelLabel38Left	Topч WidthМ HeightCaption(5) Costi fissi di gestioneFont.CharsetDEFAULT_CHARSET
Font.ColorclTealFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel39LeftЈ Topш WidthHeightCaption-Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel40Left	TopWidthІ HeightCaptionKproporzione dei costi fissi aziendali sulla base delle ore lavoro nell'annoFont.CharsetDEFAULT_CHARSET
Font.ColorclTealFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontWordWrap	  TBevelBevel14Left	TopWidthє HeightShape	bsTopLineStylebsRaised  TLabelLabel25LeftNTopЂ WidthbHeightCaptionper ora solo 1∞utenteFont.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel41Left≥ TopWidthiHeightCaption(quello di inizio comm.)Font.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFont  TdxCurrencyEditCE1Leftј Top7WidthYColorclInfoBkFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontStyle.BorderColorclWindowFrameStyle.BorderStylexbsFlatStyle.EdgesedgRight	edgBottom Style.Shadow	TabOrder 	AlignmenttaRightJustifyReadOnly	StoredValuesA  TdxCurrencyEditCE2Leftј TopjWidthYColorclInfoBkFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontStyle.BorderColorclWindowFrameStyle.BorderStylexbsFlatStyle.EdgesedgRight	edgBottom Style.Shadow	TabOrder	AlignmenttaRightJustifyReadOnly	StoredValuesA  TdxCurrencyEditCE3Leftј TopЗ WidthYColorclInfoBkFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontStyle.BorderColorclWindowFrameStyle.BorderStylexbsFlatStyle.EdgesedgRight	edgBottom Style.Shadow	TabOrder	AlignmenttaRightJustifyReadOnly	StoredValuesA  TdxCurrencyEditCE4Leftј Topљ WidthYColorclInfoBkFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontStyle.BorderColorclWindowFrameStyle.BorderStylexbsFlatStyle.EdgesedgRight	edgBottom Style.Shadow	TabOrder	AlignmenttaRightJustifyReadOnly	StoredValuesA  TPanelPanel23LeftTopWidthHeight#
BevelOuter	bvLoweredTabOrder TToolbarButton97ToolbarButton9710LeftTopWidthPHeight!Caption	Ricalcola
Glyph.Data
z  v  BMv      v   (                                    А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 3p    s337wwww?33ИИИИ33???€33ЩШ33ssws33ИИИИ33????3333ssss33ИИИИ33????3333ssss33ИИИИ33????3333ssss33ИИИИ33?€€€33   33www33 33€€33   33wwws33ИИИИ33333333ИИИИ33s€€€€s33p    s337wwww33	NumGlyphsOnClickToolbarButton9710Click   TdxCurrencyEditCETotLeftј Top6WidthYColorclInfoBkFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontStyle.BorderColorclWindowFrameStyle.BorderStylexbsFlatStyle.EdgesedgRight	edgBottom Style.Shadow	TabOrder	AlignmenttaRightJustifyReadOnly	StoredValuesA  TdxCurrencyEditCE5Leftј Topф WidthYColorclInfoBkFont.CharsetDEFAULT_CHARSET
Font.ColorclTealFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontStyle.BorderColorclWindowFrameStyle.BorderStylexbsFlatStyle.EdgesedgRight	edgBottom Style.Shadow	TabOrder	AlignmenttaRightJustifyReadOnly	StoredValuesA    	TTabSheetTSAltriDatiCaption
altri dati
ImageIndex TPanelPanel12Left Top WidthчHeightљAlignalClient
BevelOuter	bvLoweredTabOrder  TPanelPanel13LeftTop© WidthхHeightAlignalTop	AlignmenttaLeftJustifyCaption-  Note interne relative alla commessa/ricercaColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Heightф	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder OnMouseMoveFormMouseMove  TPanelPanel14LeftTopWidthхHeightAlignalTop	AlignmenttaLeftJustifyCaption+  altri dati relativi alla commessa/ricercaColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Heightф	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderOnMouseMoveFormMouseMove  TPanelPanel15LeftTopWidthхHeightС AlignalTop
BevelOuter	bvLoweredTabOrder TLabelLabel43LeftTopfWidth∆ HeightCaption0Ruolo richiesto (titolo attribuito dal cliente):Font.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFont  	TGroupBox	GroupBox8LeftTopWidthу Height.Caption'Sede operativa che gestisce la commessaTabOrder OnMouseMoveFormMouseMove TSpeedButtonSpeedButton4Left‘ TopWidthHeightHintmodifica tipo commessa
Glyph.Data
z  v  BMv      v   (                                    А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 3333333333333333333333333333333€3333330 333€€учw330  0 ∞337ww7wч33ыы а 33€3wчw33 њ∞а 33ws37чw30ыыыра 3ч€€37чw0  њ∞а 7wws37чwыыыыра s€€€€7чw0   ∞а 7wwws7чw333ы   333s€www3330 3333337w3333333333333333333333333333333333333333333333333333333333333333333	NumGlyphsParentShowHintShowHint	OnClickSpeedButton4Click  TDBEditDBEdit10LeftTopWidth… Height	DataFieldSede
DataSourceDataRicerche.DsRicerchePendReadOnly	TabOrder    	TGroupBox	GroupBox9LeftTop5Widthу Height.Caption1Referente interno per il cliente (resp.aziendale)TabOrderOnMouseMoveFormMouseMove TSpeedButtonSpeedButton5Left‘ TopWidthHeightHintmodifica tipo commessa
Glyph.Data
z  v  BMv      v   (                                    А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 3333333333333333333333333333333€3333330 333€€учw330  0 ∞337ww7wч33ыы а 33€3wчw33 њ∞а 33ws37чw30ыыыра 3ч€€37чw0  њ∞а 7wws37чwыыыыра s€€€€7чw0   ∞а 7wwws7чw333ы   333s€www3330 3333337w3333333333333333333333333333333333333333333333333333333333333333333	NumGlyphsParentShowHintShowHint	OnClickSpeedButton5Click  TDBEditDBEdit11LeftTopWidth… Height	DataField
Rifinterno
DataSourceDataRicerche.DsRicerchePendReadOnly	TabOrder    	TGroupBox
GroupBox10Leftэ TopWidthHeight`Caption$Altri utenti/selezionatori associatiTabOrder TSpeedButtonBUtente2Leftь TopWidthHeightHintinserisci utente
Glyph.Data
z  v  BMv      v   (                                    А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 3333333333€3333€39У3330 373337w39У3330 3ч€337w9ЩЩУ33337ww333?9ЩЩУ333 7wws333w39У3333 373333w39У3333337s3333?3333333 3333у33w333<333 3337€33w333<√3333€€ч€?у<ћћћћ√Щ37wwwwwу<ћћћћ√Щ37wwwwsw3333<√3333337s33€333<330 3337337w3333330 3333337w3333333333333333	NumGlyphsParentShowHintShowHint	OnClickBUtente2Click  TSpeedButtonSpeedButton6Leftь Top'WidthHeightHint0modifica ruolo-commessa per l'utente selezionato
Glyph.Data
z  v  BMv      v   (                                    А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 333     333wwwww333€€€р?у€у??ч 0  € рwчww?sw7аыы€€рws3?33чањњ €ррwу3wsуч7аыыырррwу3?чч7ањњ  рwу3wwss7аыыыыр€рwу?€€ч37ањ   €рw€wwws?ч ы ∞€ рws€w73w730 €€р37wss3?ч330∞€€  33773€ww33рр33s7s730∞€€37ч33s3	€€ 33ws€€w3303   3373wwws3	NumGlyphsParentShowHintShowHint	OnClickSpeedButton6Click  TSpeedButtonSpeedButton7Leftь Top@WidthHeight
Glyph.Data
z  v  BMv      v   (                                    А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 30    3337wwwwу330чwwp3337ууучу330рААp3337ччччу330рАpp3337ччччу330рААp3337ччччу330рАpp3337ччччу330рААp333чччччуу00рАpp0377ччччч33 рААp33wччччs330рАpp3337ччччу330pppp3337чччч€33     33wwwww33ИЗww33€€€€33     33wwwwws3330wp333337€чу33330  333337ww333	NumGlyphsOnClickSpeedButton7Click  TDBGridDBGrid2LeftTopWidthу HeightL
DataSourceDataRicerche.DsQRicUtentiOptionsdgTitlesdgIndicatordgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgConfirmDeletedgCancelOnExit ReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.HeightхTitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameUtenteWidthdVisible	 Expanded	FieldNameRuoloRicTitle.CaptionRuolo commessaWidthkVisible	     	TGroupBox
GroupBox11LeftZTop.WidthЦ Height*CaptionData prevista chiusuraTabOrderOnMouseMoveFormMouseMove TSpeedButton	BPasswordLeftwTopWidthHeightHintmodifica data chiusura
Glyph.Data
:  6  BM6      6  (                                 ДД ДДД ∆∆∆  €€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€                              ParentShowHintShowHint	OnClickBPasswordClick  TDbDateEdit97
DBDataPrevLeftTopWidthnHeightEnabledFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder ColorCalendar.ColorValidclBlueDate      DН@DayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanClear
doCanPopup
doIsMaskeddoShowCanceldoShowToday 	DataFieldDataPrevChiusura
DataSourceDataRicerche.DsRicerchePend   	TGroupBox
GroupBox12Left≠TopWidthCHeight)CaptionRif. OffertaEnabledTabOrder TLabelLabel18LeftTopWidth
HeightCaptionn∞  TEditERifOffertaLeftTopWidth(HeightTabOrder    TDBEditDBEdit13LeftTopuWidthт Height	DataFieldTitoloCliente
DataSourceDataRicerche.DsRicerchePendFont.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TPanel
PanCausaliLeftю TopcWidthHeight+
BevelOuterbvNoneTabOrderVisible TLabelLabel8Left TopWidth&HeightCaptionCausaleFocusControlDBLookupComboBox1Font.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFont  TSpeedButtonBTabCausaliLeftы TopWidthHeightHintGestione tabella causali
Glyph.Data
:  6  BM6      6  (                                Д   €   ДДД ∆∆∆ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€                              ParentShowHintShowHint	OnClickBTabCausaliClick  TDBLookupComboBoxDBLookupComboBox1Left TopWidthш Height	DataFieldCausale
DataSourceDataRicerche.DsRicerchePendFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder     фTFrameDBRichFrameDBRich1LeftTopј WidthхHeightь AlignalClientTabOrder сTPanelRulerWidthх сTBevelBevel1Widthх   с
TStatusBar	StatusBarTopй Widthх  сTToolBarStandardToolBarWidthх  сTDBRichEditEditorWidthхHeightѓ 	DataFieldNote
DataSourceDataRicerche.DsRicerchePend     	TTabSheetTSTargetListCaptionTarget List
ImageIndex	 TPanelPanel26Left Top WidthчHeightAlignalTop	AlignmenttaLeftJustifyCaption;  Aziende appartenenti alla Target List per questa commessaColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Heightф	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder OnMouseMoveFormMouseMove  TPanelPanel27LeftҐTopWidthUHeight¶AlignalRight
BevelOuter	bvLoweredTabOrder TToolbarButton97BRicClientiLeftTopWidthSHeight#CaptionInserisci da ric.aziende
Glyph.Data
z  v  BMv      v   (                                    А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 333333333333333€3333330 ?у€€у37w 0  330 wчww?37wаыы333ws3?33?ањњ 33 wу3wsу3wаыыыр33 wу3?ч€уwањњ   33wу3www??аыыыыыЩwу?€€€swањ    3Щw€wwww3w ы3333ws€s333?30 3333 37w3333w3333333 3333333w333333333333333€3333330 3333337w3333330 3333337w3333333333333333	NumGlyphsWordWrap	OnClickBRicClientiClick  TToolbarButton97BTargetListStampaLeftTopЅ WidthRHeight#DropdownMenu
PMStampaTLCaptionstampa/esporta
Glyph.Data
z  v  BMv      v   (                                       А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 0      ?€€€€€€€ИИИИИИАwwwwwwwИИИИИИАwwwwwww        €€€€€€€€ППППППАwwwwwwwшшшшшщрwwwwwwwППППППАwwwwww        wwwwwww30€€€€337у€€?330р 337чwss330€€€€337у€?€330р  337чswws330€€р3337у€73330рП3337чss3330€€ 33337€€w33330  33337wws333Layout
blGlyphTop	NumGlyphsParentShowHintShowHint	Spacing WordWrap	  TToolbarButton97BTLOrgLeftTopKWidthSHeight#Hintsi posiziona sull'organigrammaCaption	Organigr.
Glyph.Data
ъ   ц   BMц       v   (               А                        А  А   АА А   А А АА  ААА јјј   €  €   €€ €   € € €€  €€€ ИИИИИИИИА  ИИ  wwАЗwp
™ІАЩЩp
™®АЩЩАА  ИИ  ИАИИИИИИА    ИИИИИИИИИИИ  ИИИИАЗwpИИИИАїїpИИИИАїїАИИИИИ  ИИИИИИИИИИИИИИИИИИParentShowHintShowHint	WordWrap	OnClickBTLOrgClick  TBevelBevel15LeftTopWidthRHeightStylebsRaised  TBevelBevel16LeftTopЉ WidthRHeightStylebsRaised  TBevelBevel17LeftTopз WidthRHeightStylebsRaised  TToolbarButton97	BTLDeleteLeftTopЧ WidthSHeight#CaptionElimina da target list
Glyph.Data
z  v  BMv      v   (                                    А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 30    3337wwwwу330чwwp3337ууучу330рААp3337ччччу330рАpp3337ччччу330рААp3337ччччу330рАpp3337ччччу330рААp333чччччуу00рАpp0377ччччч33 рААp33wччччs330рАpp3337ччччу330pppp3337чччч€33     33wwwww33ИЗww33€€€€33     33wwwwws3330wp333337€чу33330  333337ww333	NumGlyphsParentShowHintShowHint	WordWrap	OnClickBTLDeleteClick  TBevelBevel18LeftTopУ WidthRHeightStylebsRaised  TToolbarButton97
BTLCandVaiLeftTopр WidthRHeight#Captionvai al candidato
Glyph.Data
ъ   ц   BMц       v   (               А                    А  А   АА А   А А АА  ААА јјј   €  €   €€ €   € € €€  €€€ ЁЁЁЁЁЁЁЁЁ‘DDDDЁЁЁLƒDДƒMЁЁ‘HПЗDЁЁЁЁ€pMЁЁЁ– шИАЁЁЁ– €рЁЁЁ  €АЁЁЁ  €€шЁЁ  ш»ЁЁ   ПpЁЁЁ   шЁЁ–    ЁЁЁ    ЁЁЁ–   ЁЁЁЁЁЁЁЁЁЁParentShowHintShowHint	WordWrap	OnClickBTLCandVaiClick  TBevelBevel19LeftTopо WidthRHeightStylebsRaised  TBevelBevel20LeftTop}WidthRHeightStylebsRaised  TToolbarButton97BTLElencoCliLeftTop(WidthSHeight#CaptionElenco aziende
Glyph.Data
:  6  BM6      6  (                                Д   €   ДДД ∆∆∆ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€                              SpacingWordWrap	OnClickBTLElencoCliClick  TToolbarButton97
BTLCandNewLeftTopWidthRHeight#Captioncerca da elenco
Glyph.Data
R  N  BMN      v   (               Ў                        А  А   АА А   А А АА  ААА јјј   €  €   €€ €   € € €€  €€€ ИИИА        ИИИА€€€€€   ИИИАр р   ИИИА€€€€€   ИИИАр р   ИИИА€€€€€   ИИАр р   ИИСp€€€€€   ИИЩ∞ ∞   ЩЫїїїїї∞  ЩЩЩЩєЫЩєЫ∞  ИИЩЫїїїїї∞  ИИЩА∞ ∞   ИИША€€€€€   ИИИАћћћћћ   ИИИАИћћћИ   ИИИА        ИИИИИИИИИА  ParentShowHintShowHint	WordWrap	OnClickBTLCandNewClick  TToolbarButton97BTLCandInsNuovoLeftTop6WidthRHeight#Hint*inserimento di un nuovo soggetto "al volo"Captionnuovo soggetto
Glyph.Data
ъ   ц   BMц       v   (               А                    А  А   АА А   А А АА  ААА јјј   €  €   €€ €   € € €€  €€€ €€€€€€€€€€€Щ€фDDчwЩwLƒDЩфHП	ЩЩЩЩ€€	ЩЩЩЩр шИАЩ€р €рЩ€  €АЩ€  €€ш€€  ш»€€   Пp€€€   ш€€р    €€€    €€€р   €€€ParentShowHintShowHint	WordWrap	OnClickBTLCandInsNuovoClick  TToolbarButton97ToolbarButton9715LeftTopАWidthRHeight#DropdownMenuPMStampaTLCandCaptionstampa/esporta
Glyph.Data
z  v  BMv      v   (                                       А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 0      ?€€€€€€€ИИИИИИАwwwwwwwИИИИИИАwwwwwww        €€€€€€€€ППППППАwwwwwwwшшшшшщрwwwwwwwППППППАwwwwww        wwwwwww30€€€€337у€€?330р 337чwss330€€€€337у€?€330р  337чswws330€€р3337у€73330рП3337чss3330€€ 33337€€w33330  33337wws333Layout
blGlyphTop	NumGlyphsParentShowHintShowHint	Spacing WordWrap	  TToolbarButton97BTLNoteClienteLeftTopnWidthSHeight#CaptionNote azienda
Glyph.Data
ъ   ц   BMц       v   (               А                    А  А   АА А   А А АА  ААА јјј   €  €   €€ €   € € €€  €€€ ЁЁЁЁЁЁЁЁ–      Ё–ИИИИИАЁ€€€€АЁ–€ИА€АЁ€чp€АЁ–€ИЗыАЁ€€∞АЁ–€ИИГы Ё€€€∞Ё–€ИИИЗЗ€€€€qС–€ИИИПЙ€€€€АЁ–€€€€€АЁ„wwwww}ЁParentShowHintShowHint	WordWrap	OnClickBTLNoteClienteClick  TToolbarButton97
BTLDelSoggLeftTopYWidthRHeight#Hint2eliminazione completa del 
soggetto dall'archivioCaptionelimina soggetto
Glyph.Data
z  v  BMv      v   (                                    А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 30    3337wwwwу330чwwp3337ууучу330рААp3337ччччу330рАpp3337ччччу330рААp3337ччччу330рАpp3337ччччу330рААp333чччччуу00рАpp0377ччччч33 рААp33wччччs330рАpp3337ччччу330pppp3337чччч€33     33wwwww33ИЗww33€€€€33     33wwwwws3330wp333337€чу33330  333337ww333	NumGlyphsParentShowHintShowHint	WordWrap	OnClickBTLDelSoggClick   TPanelPanel28Left TopWidthҐHeight¶AlignalClient
BevelOuter	bvLoweredTabOrder 	TSplitter	Splitter1LeftTopЌ Width†HeightCursorcrVSplitAlignalTop  	TdxDBGrid	dxDBGrid3LeftTopWidth†Heightћ Bands  DefaultLayout	HeaderPanelRowCountKeyFieldIDShowGroupPanel	SummaryGroups SummarySeparator, AlignalTop	PopupMenuPMTargetListTabOrder 	OnMouseUpdxDBGrid3MouseUp
DataSourceDataRicerche.DsQRicTargetListFilter.Active	Filter.Criteria
       OptionsBehavioredgoAutoSortedgoDragScrolledgoEditingedgoEnterShowEditoredgoImmediateEditoredgoStoreToRegistryedgoTabThroughedgoVertThrough 	OptionsDBedgoCancelOnExitedgoCanDeleteedgoCanInsertedgoCanNavigationedgoConfirmDeleteedgoLoadAllRecordsedgoUseBookmarks OptionsViewedgoAutoWidthedgoBandHeaderWidthedgoIndicatoredgoUseBitmap RegistryPath$\Software\H1\DbGridSelPersTargetListOnChangeColumndxDBGrid3ChangeColumnOnContextPopupdxDBGrid3ContextPopup TdxDBGridMaskColumndxDBGridMaskColumn1DisableEditor	VisibleWidthQ	BandIndex RowIndex 	FieldNameID  TdxDBGridMaskColumndxDBGridMaskColumn2DisableEditor	Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold SortedcsUpWidthН 	BandIndex RowIndex DisableGrouping		FieldNameAzienda  TdxDBGridMaskColumndxDBGrid1AttivitaDisableEditor	WidthХ 	BandIndex RowIndex 	FieldNameAttivita  TdxDBGridMaskColumndxDBGrid1telefonoDisableEditor	WidthX	BandIndex RowIndex DisableGrouping		FieldNametelefonoDisableFilter	  TdxDBGridMaskColumndxDBGrid1ComuneDisableEditor	Widthf	BandIndex RowIndex 	FieldNameComune  TdxDBGridDateColumndxDBGrid1DataUltimaEsplorazCaptionData ultima esploraz.DisableEditor	Width]	BandIndex RowIndex 	FieldNameDataUltimaEsploraz  TdxDBGridBlobColumndxDBGrid3Column7WidthU	BandIndex RowIndex 	FieldNameNoteBlobPaintStylebpsText   TPanelPanel29LeftTop– Width†HeightAlignalTop	AlignmenttaLeftJustifyCaptionN  Candidati associati all'azienda sopra selezionata ed inseriti nella commessaColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Heightф	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderOnMouseMoveFormMouseMove 	TCheckBoxCBOpzioneCand2LeftСTopWidthHeightCaption3non visualizzare i candidati esclusi dalla commessaChecked	Font.CharsetDEFAULT_CHARSET
Font.ColorclYellowFont.Heightф	Font.NameMS Sans Serif
Font.Style 
ParentFontState	cbCheckedTabOrder OnClickCBOpzioneCand2Click   	TdxDBGrid	dxDBGrid4LeftTopз Width†HeightЊ Bands  DefaultLayout	HeaderPanelRowCountKeyFieldIDSummaryGroups SummarySeparator, AlignalClientTabOrder
OnDblClickdxDBGrid4DblClick	OnMouseUpdxDBGrid4MouseUp
DataSourceDataRicerche.DsQTLCandidatiFilter.Criteria
       OptionsBehavioredgoAutoSortedgoDragScrolledgoEditingedgoEnterShowEditoredgoImmediateEditoredgoStoreToRegistryedgoTabThroughedgoVertThrough 	OptionsDBedgoCancelOnExitedgoCanDeleteedgoCanInsertedgoCanNavigationedgoConfirmDeleteedgoLoadAllRecordsedgoUseBookmarks OptionsViewedgoAutoWidthedgoBandHeaderWidthedgoIndicatoredgoRowSelectedgoUseBitmap RegistryPath \Software\H1\DbGridSelPersTLCandOnCustomDrawCelldxDBGrid4CustomDrawCell TdxDBGridMaskColumndxDBGrid4IDVisible	BandIndex RowIndex 	FieldNameID  TdxDBGridMaskColumndxDBGrid4IDAnagraficaVisible	BandIndex RowIndex 	FieldNameIDAnagrafica  TdxDBGridMaskColumndxDBGrid4CVNumeroCaptionCV N∞	BandIndex RowIndex 	FieldNameCVNumero  TdxDBGridMaskColumndxDBGrid4CognomeSortedcsUp	BandIndex RowIndex 	FieldNameCognome  TdxDBGridMaskColumndxDBGrid4Nome	BandIndex RowIndex 	FieldNameNome  TdxDBGridMaskColumndxDBGrid4IDStatoVisible	BandIndex RowIndex 	FieldNameIDStato  TdxDBGridMaskColumndxDBGrid4Cellulare	BandIndex RowIndex 	FieldName	Cellulare  TdxDBGridMaskColumndxDBGrid4TelUfficioCaptionTel.UfficioWidthG	BandIndex RowIndex 	FieldName
TelUfficio  TdxDBGridDateColumndxDBGrid4DataNascitaCaptionData di nascitaVisible	BandIndex RowIndex 	FieldNameDataNascita  TdxDBGridColumndxDBGrid4EtaCaptionEtа	BandIndex RowIndex 	FieldNameEta  TdxDBGridColumndxDBGrid4RuoloCaptionRuolo ricoperto nell'azienda	BandIndex RowIndex 	FieldNameRuoloAttuale     	TTabSheetTSFileCaptionFile
ImageIndex TPanelPanel123Left Top WidthчHeight'AlignalTop
BevelOuter	bvLoweredTabOrder  TToolbarButton97BAnagFileNewLeftTopWidthLHeight%Hint>cerca, associa e definisci il 
tipo per un file giа esistenteCaptionAssocia nuovo file
Glyph.Data
z  v  BMv      v   (                                    А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 3333333333€3333€39У3330 373337w39У3330 3ч€337w9ЩЩУ33337ww333?9ЩЩУ333 7wws333w39У3333 373333w39У3333337s3333?3333333 3333у33w333<333 3337€33w333<√3333€€ч€?у<ћћћћ√Щ37wwwwwу<ћћћћ√Щ37wwwwsw3333<√3333337s33€333<330 3337337w3333330 3333337w3333333333333333	NumGlyphsOpaqueParentShowHintShowHint	WordWrap	OnClickBAnagFileNewClick  TToolbarButton97BAnagFileModLeftMTopWidthKHeight%CaptionModifica dati
Glyph.Data
z  v  BMv      v   (                                    А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 333     333wwwww333€€€р?у€у??ч 0  € рwчww?sw7аыы€€рws3?33чањњ €ррwу3wsуч7аыыырррwу3?чч7ањњ  рwу3wwss7аыыыыр€рwу?€€ч37ањ   €рw€wwws?ч ы ∞€ рws€w73w730 €€р37wss3?ч330∞€€  33773€ww33рр33s7s730∞€€37ч33s3	€€ 33ws€€w3303   3373wwws3	NumGlyphsOpaqueParentShowHintShowHint	WordWrap	OnClickBAnagFileModClick  TToolbarButton97BAnagFileDelLeftШ TopWidthKHeight%CaptionElimina associaz.
Glyph.Data
z  v  BMv      v   (                                    А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 30    3337wwwwу330чwwp3337ууучу330рААp3337ччччу330рАpp3337ччччу330рААp3337ччччу330рАpp3337ччччу330рААp333чччччуу00рАpp0377ччччч33 рААp33wччччs330рАpp3337ччччу330pppp3337чччч€33     33wwwww33ИЗww33€€€€33     33wwwwws3330wp333337€чу33330  333337ww333	NumGlyphsOpaqueWordWrap	OnClickBAnagFileDelClick  TToolbarButton97BFileCandOpenLeftе TopWidthKHeight%Caption	Apri fileFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold 
Glyph.Data
z  v  BMv      v   (                                    А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 3333≥33;3€3333ч;≥w{Јw{≥7€ч€чs3∞    33wwwwww330€€€€337у333330€€€€337у€?€330р 337чsws330€€€€3?чу€€?€ї∞р ≥wwчwssw;∞€€€€ї7wу€?€ww30р  337чswws330€€р3337у€7у330рП3337чsw€330€€ ;≥337€€w7у3∞  3ї33www3w€;≥3;≥3;≥7s37s37s≥33;333;s3373337	NumGlyphs
ParentFontWordWrap	OnClickBFileCandOpenClick   	TdxDBGrid	dxDBGrid7Left Top'WidthЩHeightЦBands  DefaultLayout	HeaderPanelRowCountKeyFieldIDSummaryGroups SummarySeparator, AlignalLeftTabOrder
DataSource
DsQRicFileFilter.Criteria
       OptionsBehavioredgoAutoSortedgoDragScrolledgoEnterShowEditoredgoImmediateEditoredgoTabThroughedgoVertThrough 	OptionsDBedgoCancelOnExitedgoCanDeleteedgoCanInsertedgoCanNavigationedgoConfirmDeleteedgoLoadAllRecordsedgoUseBookmarks  TdxDBGridMaskColumndxDBGrid7DescrizioneWidth	BandIndex RowIndex 	FieldNameDescrizione  TdxDBGridMaskColumndxDBGrid7NomeFileCaption	Nome FileWidthа 	BandIndex RowIndex 	FieldNameNomeFile  TdxDBGridDateColumndxDBGrid7DataCreazioneCaptionData CreazioneWidthЧ 	BandIndex RowIndex 	FieldNameDataCreazione     TTimerOraOnTimerOraTimerLeftа Topш  TTimerTimerInattivoIntervalј‘ OnTimerTimerInattivoTimerLeft(Topч  TQueryQDatabaseNameEBCDBLeftyTop  
TPopupMenuPMStampaLeftЎTopш 	TMenuItemStampaelenco1CaptionStampa elenco
ImageIndexOnClickStampaelenco1Click  	TMenuItemEsportainExcel1CaptionEsporta in Excel
ImageIndexOnClickEsportainExcel1Click  	TMenuItemesportainHTML1CaptionEsporta in HTML
ImageIndexOnClickesportainHTML1Click   TdxComponentPrinter
dxPrinter1CurrentLinkdxPrinter1Link1Version LeftTopш TdxDBGridReportLinkdxPrinter1Link1CaptiondxPrinter1Link1	Component	dxDBGrid1DesignerHelpContext "PrinterPage.Background.Brush.StylebsClearPrinterPage.FooterќPrinterPage.HeaderќPrinterPage.Margins.Bottom8cPrinterPage.Margins.Left8cPrinterPage.Margins.Right8cPrinterPage.Margins.Top'#PrinterPage.PageFooter.Font.CharsetDEFAULT_CHARSET!PrinterPage.PageFooter.Font.ColorclWindowText"PrinterPage.PageFooter.Font.Heightх PrinterPage.PageFooter.Font.NameTahoma!PrinterPage.PageFooter.Font.Style #PrinterPage.PageHeader.Font.CharsetDEFAULT_CHARSET!PrinterPage.PageHeader.Font.ColorclWindowText"PrinterPage.PageHeader.Font.Heightх PrinterPage.PageHeader.Font.NameTahoma!PrinterPage.PageHeader.Font.Style PrinterPage.PageSize.XP4 PrinterPage.PageSize.Y(И  PrinterPage._dxMeasurementUnits_ PrinterPage._dxLastMU_ReportTitle.Font.CharsetDEFAULT_CHARSETReportTitle.Font.ColorclWindowTextReportTitle.Font.HeightнReportTitle.Font.NameTimes New RomanReportTitle.Font.StylefsBold 	BandColor	clBtnFaceBandFont.CharsetDEFAULT_CHARSETBandFont.ColorclWindowTextBandFont.HeightхBandFont.NameMS Sans SerifBandFont.Style ColorclWindowEvenFont.CharsetDEFAULT_CHARSETEvenFont.ColorclWindowTextEvenFont.HeightхEvenFont.NameTimes New RomanEvenFont.Style Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.Style FooterFont.CharsetDEFAULT_CHARSETFooterFont.ColorclWindowTextFooterFont.HeightхFooterFont.NameMS Sans SerifFooterFont.Style GridLineColor	clBtnFaceGroupNodeFont.CharsetDEFAULT_CHARSETGroupNodeFont.ColorclWindowTextGroupNodeFont.HeightхGroupNodeFont.NameTimes New RomanGroupNodeFont.Style GroupNodeColor	clBtnFaceHeaderColor	clBtnFaceHeaderFont.CharsetDEFAULT_CHARSETHeaderFont.ColorclWindowTextHeaderFont.HeightхHeaderFont.NameMS Sans SerifHeaderFont.Style OddColorclWindowOddFont.CharsetDEFAULT_CHARSETOddFont.ColorclWindowTextOddFont.HeightхOddFont.NameTimes New RomanOddFont.Style Options	tlpoBandstlpoHeaderstlpoFooterstlpoRowFooterstlpoPreviewtlpoPreviewGridtlpoGridtlpoFlatCheckMarks
tlpoImagestlpoStateImages PreviewFont.CharsetDEFAULT_CHARSETPreviewFont.ColorclBluePreviewFont.HeightхPreviewFont.NameMS Sans SerifPreviewFont.Style RowFooterColor	cl3DLightRowFooterFont.CharsetDEFAULT_CHARSETRowFooterFont.ColorclWindowTextRowFooterFont.HeightхRowFooterFont.NameMS Sans SerifRowFooterFont.Style BuiltInReportLink	  TdxDBGridReportLinkdxPrinter1Link2CaptiondxPrinter1Link2	Component	dxDBGrid3DesignerHelpContext "PrinterPage.Background.Brush.StylebsClearPrinterPage.FooterќPrinterPage.HeaderќPrinterPage.Margins.Bottom8cPrinterPage.Margins.Left8cPrinterPage.Margins.Right8cPrinterPage.Margins.Top'#PrinterPage.PageFooter.Font.CharsetDEFAULT_CHARSET!PrinterPage.PageFooter.Font.ColorclWindowText"PrinterPage.PageFooter.Font.Heightх PrinterPage.PageFooter.Font.NameTahoma!PrinterPage.PageFooter.Font.Style #PrinterPage.PageHeader.Font.CharsetDEFAULT_CHARSET!PrinterPage.PageHeader.Font.ColorclWindowText"PrinterPage.PageHeader.Font.Heightх PrinterPage.PageHeader.Font.NameTahoma!PrinterPage.PageHeader.Font.Style PrinterPage.PageSize.XP4 PrinterPage.PageSize.Y(И  PrinterPage._dxMeasurementUnits_ PrinterPage._dxLastMU_ReportTitle.Font.CharsetDEFAULT_CHARSETReportTitle.Font.ColorclWindowTextReportTitle.Font.HeightнReportTitle.Font.NameTimes New RomanReportTitle.Font.StylefsBold 	BandColor	clBtnFaceBandFont.CharsetDEFAULT_CHARSETBandFont.ColorclWindowTextBandFont.HeightхBandFont.NameMS Sans SerifBandFont.Style ColorclWindowEvenFont.CharsetDEFAULT_CHARSETEvenFont.ColorclWindowTextEvenFont.HeightхEvenFont.NameTimes New RomanEvenFont.Style Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.Style FooterFont.CharsetDEFAULT_CHARSETFooterFont.ColorclWindowTextFooterFont.HeightхFooterFont.NameMS Sans SerifFooterFont.Style GridLineColor	clBtnFaceGroupNodeFont.CharsetDEFAULT_CHARSETGroupNodeFont.ColorclWindowTextGroupNodeFont.HeightхGroupNodeFont.NameTimes New RomanGroupNodeFont.Style GroupNodeColor	clBtnFaceHeaderColor	clBtnFaceHeaderFont.CharsetDEFAULT_CHARSETHeaderFont.ColorclWindowTextHeaderFont.HeightхHeaderFont.NameMS Sans SerifHeaderFont.Style OddColorclWindowOddFont.CharsetDEFAULT_CHARSETOddFont.ColorclWindowTextOddFont.HeightхOddFont.NameTimes New RomanOddFont.Style Options	tlpoBandstlpoHeaderstlpoFooterstlpoRowFooterstlpoPreviewtlpoPreviewGridtlpoGridtlpoFlatCheckMarks
tlpoImagestlpoStateImages PreviewFont.CharsetDEFAULT_CHARSETPreviewFont.ColorclBluePreviewFont.HeightхPreviewFont.NameMS Sans SerifPreviewFont.Style RowFooterColor	cl3DLightRowFooterFont.CharsetDEFAULT_CHARSETRowFooterFont.ColorclWindowTextRowFooterFont.HeightхRowFooterFont.NameMS Sans SerifRowFooterFont.Style BuiltInReportLink	  TdxDBGridReportLinkdxPrinter1Link3CaptiondxPrinter1Link3	Component	dxDBGrid4DesignerHelpContext "PrinterPage.Background.Brush.StylebsClearPrinterPage.FooterќPrinterPage.HeaderќPrinterPage.Margins.Bottom8cPrinterPage.Margins.Left8cPrinterPage.Margins.Right8cPrinterPage.Margins.Top'#PrinterPage.PageFooter.Font.CharsetDEFAULT_CHARSET!PrinterPage.PageFooter.Font.ColorclWindowText"PrinterPage.PageFooter.Font.Heightх PrinterPage.PageFooter.Font.NameTahoma!PrinterPage.PageFooter.Font.Style #PrinterPage.PageHeader.Font.CharsetDEFAULT_CHARSET!PrinterPage.PageHeader.Font.ColorclWindowText"PrinterPage.PageHeader.Font.Heightх PrinterPage.PageHeader.Font.NameTahoma!PrinterPage.PageHeader.Font.Style PrinterPage.PageSize.XP4 PrinterPage.PageSize.Y(И  PrinterPage._dxMeasurementUnits_ PrinterPage._dxLastMU_ReportTitle.Font.CharsetDEFAULT_CHARSETReportTitle.Font.ColorclWindowTextReportTitle.Font.HeightнReportTitle.Font.NameTimes New RomanReportTitle.Font.StylefsBold 	BandColor	clBtnFaceBandFont.CharsetDEFAULT_CHARSETBandFont.ColorclWindowTextBandFont.HeightхBandFont.NameMS Sans SerifBandFont.Style ColorclWindowEvenFont.CharsetDEFAULT_CHARSETEvenFont.ColorclWindowTextEvenFont.HeightхEvenFont.NameTimes New RomanEvenFont.Style Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.Style FooterFont.CharsetDEFAULT_CHARSETFooterFont.ColorclWindowTextFooterFont.HeightхFooterFont.NameMS Sans SerifFooterFont.Style GridLineColor	clBtnFaceGroupNodeFont.CharsetDEFAULT_CHARSETGroupNodeFont.ColorclWindowTextGroupNodeFont.HeightхGroupNodeFont.NameTimes New RomanGroupNodeFont.Style GroupNodeColor	clBtnFaceHeaderColor	clBtnFaceHeaderFont.CharsetDEFAULT_CHARSETHeaderFont.ColorclWindowTextHeaderFont.HeightхHeaderFont.NameMS Sans SerifHeaderFont.Style OddColorclWindowOddFont.CharsetDEFAULT_CHARSETOddFont.ColorclWindowTextOddFont.HeightхOddFont.NameTimes New RomanOddFont.Style Options	tlpoBandstlpoHeaderstlpoFooterstlpoRowFooterstlpoPreviewtlpoPreviewGridtlpoGridtlpoFlatCheckMarks
tlpoImagestlpoStateImages PreviewFont.CharsetDEFAULT_CHARSETPreviewFont.ColorclBluePreviewFont.HeightхPreviewFont.NameMS Sans SerifPreviewFont.Style RowFooterColor	cl3DLightRowFooterFont.CharsetDEFAULT_CHARSETRowFooterFont.ColorclWindowTextRowFooterFont.HeightхRowFooterFont.NameMS Sans SerifRowFooterFont.Style BuiltInReportLink	   
TImageList
ImageList1LeftmTopкBitmap
¶"  IL     €€€€€€€€€€€€€BM6       6   (   @   @                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      | @ @ @ @ @ @ @ @            јјјјјјјјјјјјј                                                                   @ @   |ч^  ч^  ч^  ч^ @          `N`N`N`N`N`N`N`N`N`N`N`Nјј                                                  B€€€€€   @       | | | | | | | | @          `N€3у3у3333,g`Nјј      €cc B                                    B€   €   @         |ч^  ч^  ч^ |          `N€уу3у3у333,gј`Nј   Bа€ccа  аcc                        B€B€€€€€   @       @   | | | | |   @        `N€ууууу3у33`Nј`Nј   Bа€cc  аа€cc                      B€B€   €  п=   @ @ @               @ @ @  `N€уууу3уу3у,gјN  ј   Bа€cc  аа€€c                 B€B€B€€€€€       | | |   @ @ @ @ @   | | |  `N€уууууу3у3,gј?g?g     Bа€cc  аа€€c                B€B€B€   €         | | | | | | | | | | |    `N`N`N`N`N`N`N`N`N`N`N`NN€g?g     Bа€ccа  а€€                   B€B€B€€€€€         @                   @       BУ1€g?gN?gN?g?g?g?g  ј   Bа€ccаc                          B€B€BBBBBB                                     B,gУ1€g€ў€O€ў€€g?g  ,gј   Bаc B B B B B                        B€B€€€€€                                         B,g,gУ1€g€Я€O€€g?g  `N`N     B€а€а€€ B                        B€BBBBBB                                           B,g,gУ1€g€€g?g            €а€а€а€а                        B€€€€€                                                 B B BУ1€g€?g                €а€а€а                          BBBBBB                                                        У1€g                                                                                                                              У1                                                  п=      п=      п=      п=                                                                            а€а€а€              п=  п=  п=  п=  п=  п=  п=                                              ааа                        €а€а€а   B      п=п=                                       | | | | |                                                      а€а€а€   B                                             | |ч^€€€ч^ | |                    ааа                        €а€а€а   B        п=   |                            | |€€€ €€€ | |                                                               B               |   | |                   |€€€€€€€ € |                ааааа                                            п=п=  п= |п= |п= |п=п= п=п=п=п=   |ч^€€  €€€€€€ч^ |            аа€€€аа                                                     |     |                |€€€€  €€€€€€ |            а€€€€€а                                            п=             |     |         |€ €€€ |        € |            а€€  п=п=а                                                           |   |  |       |€€€€€€€€€€€ |            а€€п=€€а                            а            п=п=  п= п= п=п=п= |п=п=  |п=   |ч^€€€€€€€€€ч^ |            аа€п=€аа                     €а€                                               |€ €€€€€ € |                аап=аа                           а       @ @    п=                               | |€€€ €€€ | |                                                              @ @                                         | |ч^€ €ч^ | |                    ааа                                              п=п=  п=п=п=п=п=п=п=п=п=п=п=п=п=           | | | | |                                                                                                                                                        ааа                                                                                                                                       c                                                              €€                                                cc  cB                       ccccccc                  €€    @  @                                          B    B     c    B          B€c    €                    €€€  c        €€€€€€€€€€                    cccc    cB    B€      c  c€€€€€€              cc€€€€  c        €    €          €                                B       B€€€ccB€c    €              cc  €€€€  c      €€€€€€€€€€                  B      c  cccc    B€€€cc  c€€€€€€              cc  €€    cc      €    €          €                            c            B€€€ccB€c    €    Bccc  ccc  €€  ccc    €€€€€€€€€€                  B    c c      c    B€€€cc  c€€            Bcccc  ccccc  c        €€€€€€€  €€                      c  B          c  B€€€ccB€cBcccc    Bccccc          Bcc      €    €€€      €                            c    c c  B€€€cc  €B€c€ccc  BcBBBc€B  BBBBB      €      €                                        c  B    B€€€cB  BB€€      c  Bc  BBc€€€                €€                                                    c    B€€cB    B€c  c€c  Bc  cBc€    €                                                                          BBB      B€€  €c€  BcB    c€€€€€€                                                                                        B€€€€    Bccccc€€€€€€                                                                                          BBBB    BBBBBBBBBBBB                                      BM>       >   (   @   @                                €€€                                                                                                                                 €€€€а€€€ь€€аа €ш€ААј бс€А`ј Аю pА  ~   А  8       А     ј    аА  8 €€А  ?x€€А ?ш€€ј ?ш€€аАш€€ю?ј€€€€€€рЁЁ€€ш?р’Uш?ш?р  аш?ря€јш?рПяАрр‘ѓАалг   а№џw јлсОЈ јчЯ’£ јО   аџыАаЫэАрРя€јш?Пр  аш?€€€€ш?ш?А?€€ш€€ юь€€Д|ь ћ 8ш И! р З£ р    Э       <   ПР   DА	  
ЄјГ  |а «  ъью €Б эш€€√ ю                        
TPopupMenu
PMStampaTLLeft`Topш 	TMenuItem	MenuItem1CaptionStampa elenco
ImageIndexOnClickMenuItem1Click  	TMenuItem	MenuItem2CaptionEsporta in Excel
ImageIndexOnClickMenuItem2Click  	TMenuItem	MenuItem3CaptionEsporta in HTML
ImageIndexOnClickMenuItem3Click   
TPopupMenuPMStampaTLCandLeftАTopш 	TMenuItem	MenuItem4CaptionStampa elenco
ImageIndexOnClickMenuItem4Click  	TMenuItem	MenuItem5CaptionEsporta in Excel
ImageIndexOnClickMenuItem5Click  	TMenuItem	MenuItem6CaptionEsporta in HTML
ImageIndexOnClickMenuItem6Click   TdxDBGridLayoutListdxDBGridLayoutList1LeftђTop TdxDBGridLayoutdxDBGridLayoutList1Item1Data
T  P  TPF0TdxDBGridWrapper Bands  DefaultLayout	HeaderPanelRowCountSummaryGroups SummarySeparator, 
DataSourceDataRicerche.DsQUsersLK TdxDBGridMaskColumn
NominativoCaptionUtenteWidthИ 	BandIndex RowIndex 	FieldName
Nominativo  TdxDBGridMaskColumnDescrizioneWidth§ 	BandIndex RowIndex 	FieldNameDescrizione     TdxDBGridLayoutdxDBGridLayoutList1Item2Data
    TPF0TdxDBGridWrapper Bands  DefaultLayout	HeaderPanelRowCountSummaryGroups SummarySeparator, 
DataSourceDataRicerche.DsQGruppiProfLK TdxDBGridMaskColumnGruppoProfessionaleCaptionGruppo Professionale	BandIndex RowIndex 	FieldNameGruppoProfessionale      
TPopupMenuPMTargetListLeft\Top— 	TMenuItemmodificanumeroditelefono1Captionmodifica numero di telefonoSubMenuImages
ImageList1
ImageIndex
OnClickmodificanumeroditelefono1Click   
TPopupMenuPMCandLeftTopЋ 	TMenuItem modificaaziendaattualecandidato1Caption"modifica/inserisci azienda attualeOnClick%modificaaziendaattualecandidato1Click  	TMenuItem!modificanumeroditelefonodufficio1Caption%modifica numero di telefono d'ufficioOnClick&modificanumeroditelefonodufficio1Click  	TMenuItem"modificanumeroditelefonocellulare1Caption%modifica numero di telefono cellulareOnClick'modificanumeroditelefonocellulare1Click   TQueryQRicFileDatabaseNameEBCDB
DataSourceDataRicerche.DsRicerchePendSQL.Stringsselect * from EBC_RicercheFilewhere IDRicerca=:ID LeftДTop≤	ParamDataDataType	ftAutoIncNameID	ParamType	ptUnknown   TAutoIncField
QRicFileID	FieldNameIDOriginEBCDB.EBC_RicercheFile.ID  TIntegerFieldQRicFileIDRicerca	FieldName	IDRicercaOrigin EBCDB.EBC_RicercheFile.IDRicerca  TIntegerFieldQRicFileIDTipo	FieldNameIDTipoOriginEBCDB.EBC_RicercheFile.IDTipo  TStringFieldQRicFileDescrizione	FieldNameDescrizioneOrigin"EBCDB.EBC_RicercheFile.Descrizione	FixedChar	Sized  TStringFieldQRicFileNomeFile	FieldNameNomeFileOriginEBCDB.EBC_RicercheFile.NomeFile	FixedChar	Size»   TDateTimeFieldQRicFileDataCreazione	FieldNameDataCreazioneOrigin$EBCDB.EBC_RicercheFile.DataCreazione   TDataSource
DsQRicFileDataSetQRicFileLeftДTop“   