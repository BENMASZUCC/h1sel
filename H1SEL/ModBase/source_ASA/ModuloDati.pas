unit ModuloDati;

interface

uses
     SysUtils, Windows, Classes, Graphics, Controls,
     Forms, Dialogs, DB, DBTables;

type
     TData = class(TDataModule)
          DsAnagrafica: TDataSource;
          DsTitoliStudio: TDataSource;
          DsStati: TDataSource;
          TStati: TTable;
          DsStorico: TDataSource;
          DsCorsiExtra: TDataSource;
          DsLingueConosc: TDataSource;
          DsEspLav: TDataSource;
          TStatiCivili: TTable;
          TStatiCiviliID: TAutoIncField;
          TStatiCiviliDescrizione: TStringField;
          TStatiCiviliSesso: TStringField;
          DsGlobal: TDataSource;
          DsUsers: TDataSource;
          TPromemoria: TTable;
          DsPromemoria: TDataSource;
          TPromemoriaID: TAutoIncField;
          TPromemoriaIDUtente: TIntegerField;
          TPromemoriaIDUtenteDa: TIntegerField;
          TPromemoriaDataDaLeggere: TDateTimeField;
          TPromemoriaTesto: TStringField;
          TPromemoriaDataLettura: TDateTimeField;
          TPromemoriaEvaso: TBooleanField;
          TPromemoriaDataIns: TDateTimeField;
          TAnagLK: TTable;
          TAnagLKID: TAutoIncField;
          TAnagLKMatricola: TIntegerField;
          TAnagLKCognome: TStringField;
          TAnagLKNome: TStringField;
          TPromemABS: TTable;
          TPromemABSID: TAutoIncField;
          TPromemABSIDUtente: TIntegerField;
          TPromemABSIDUtenteDa: TIntegerField;
          TPromemABSDataIns: TDateTimeField;
          TPromemABSDataDaLeggere: TDateTimeField;
          TPromemABSTesto: TStringField;
          TPromemABSDataLettura: TDateTimeField;
          TPromemABSEvaso: TBooleanField;
          TDisponibAnag: TTable;
          DsDisponibAnag: TDataSource;
          TDisponibAnagID: TAutoIncField;
          TDisponibAnagIDAnagrafica: TIntegerField;
          TDisponibAnagIDMansione: TIntegerField;
          TDisponibAnagNote: TMemoField;
          TMansioniLK: TTable;
          TMansioniLKID: TAutoIncField;
          TMansioniLKDescrizione: TStringField;
          TMansioniLKIDArea: TIntegerField;
          TDisponibAnagMansione: TStringField;
          TDisponibAnagMotivazioni: TStringField;
          TStatiLK: TTable;
          TDiplomi: TTable;
          TDiplomiID: TAutoIncField;
          TDiplomiIDArea: TIntegerField;
          TDiplomiDescrizione: TStringField;
          TDiplomiPunteggioMax: TSmallintField;
          TAreeDiplomi: TTable;
          TAreeDiplomiID: TAutoIncField;
          TAreeDiplomiArea: TStringField;
          TDiplomiArea: TStringField;
          DsDiplomi: TDataSource;
          DsAreeDiplomi: TDataSource;
          TTipiStato: TTable;
          TTipiStatoID: TAutoIncField;
          TTipiStatoTipoStato: TStringField;
          TStatiID: TAutoIncField;
          TStatiStato: TStringField;
          TStatiIDTipoStato: TIntegerField;
          TStatiTipoStato: TStringField;
          TStatiLKID: TAutoIncField;
          TStatiLKStato: TStringField;
          TStatiLKIDTipoStato: TIntegerField;
          TEventi: TTable;
          DsEventi: TDataSource;
          TEventiID: TAutoIncField;
          TEventiIDdaStato: TIntegerField;
          TEventiEvento: TStringField;
          TEventiIDaStato: TIntegerField;
          TEventiAStato: TStringField;
          TEventiIDTipoStatoA: TIntegerField;
          TEventiNonCancellabile: TBooleanField;
          TEventiDaStato: TStringField;
          TEventiLK: TTable;
          TEventiLKID: TAutoIncField;
          TEventiLKIDdaStato: TIntegerField;
          TEventiLKEvento: TStringField;
          TEventiLKIDaStato: TIntegerField;
          TEventiLKNonCancellabile: TBooleanField;
          DsAnagMansioni: TDataSource;
          TAreeLK: TTable;
          TAreeLKID: TAutoIncField;
          TAreeLKDescrizione: TStringField;
          TMansioniLKArea: TStringField;
          TLivelloLingue: TTable;
          TLivelloLingueID: TAutoIncField;
          TLivelloLingueLivelloConoscenza: TStringField;
          TEventiIDProcedura: TIntegerField;
          TEventiLKIDProcedura: TIntegerField;
          TPromScaduti: TTable;
          DsPromScaduti: TDataSource;
          TPromScadutiID: TAutoIncField;
          TPromScadutiIDUtente: TIntegerField;
          TPromScadutiIDUtenteDa: TIntegerField;
          TPromScadutiDataIns: TDateTimeField;
          TPromScadutiDataDaLeggere: TDateTimeField;
          TPromScadutiTesto: TStringField;
          TPromScadutiDataLettura: TDateTimeField;
          TPromScadutiEvaso: TBooleanField;
          Q1: TQuery;
          TCVFile: TTable;
          DsCVFile: TDataSource;
          TCVFileIDAnagrafica: TIntegerField;
          TCVFilePagina: TSmallintField;
          TCVFileFile: TStringField;
          TDiplomiTipo: TStringField;
          TTipiStatiLK: TTable;
          TTipiStatiLKID: TAutoIncField;
          TTipiStatiLKTipoStato: TStringField;
          TStatiLKTipoStato: TStringField;
          TDiplomiLK: TTable;
          TDiplomiLKID: TAutoIncField;
          TDiplomiLKIDArea: TIntegerField;
          TDiplomiLKTipo: TStringField;
          TDiplomiLKDescrizione: TStringField;
          TDiplomiLKPunteggioMax: TSmallintField;
          TAnagLKIDTipoStato: TIntegerField;
          QPromOggi: TQuery;
          DsQPromOggi: TDataSource;
          QPromOggiID: TIntegerField;
          QPromOggiIDUtente: TIntegerField;
          QPromOggiIDUtenteDa: TIntegerField;
          QPromOggiDataIns: TDateTimeField;
          QPromOggiDataDaLeggere: TDateTimeField;
          QPromOggiTesto: TStringField;
          QPromOggiDataLettura: TDateTimeField;
          QPromOggiEvaso: TBooleanField;
          TPromemABSPerChi: TStringField;
          QTemp: TQuery;
          QPromFuturi: TQuery;
          QPromFuturiID: TIntegerField;
          QPromFuturiIDUtente: TIntegerField;
          QPromFuturiIDUtenteDa: TIntegerField;
          QPromFuturiDataIns: TDateTimeField;
          QPromFuturiDataDaLeggere: TDateTimeField;
          QPromFuturiTesto: TStringField;
          QPromFuturiDataLettura: TDateTimeField;
          QPromFuturiEvaso: TBooleanField;
          QStorico: TQuery;
          QStoricoID: TIntegerField;
          QStoricoIDAnagrafica: TIntegerField;
          QStoricoIDEvento: TIntegerField;
          QStoricoDataEvento: TDateTimeField;
          QStoricoAnnotazioni: TStringField;
          TStorico: TTable;
          TStoricoID: TAutoIncField;
          TStoricoIDAnagrafica: TIntegerField;
          TStoricoIDEvento: TIntegerField;
          TStoricoDataEvento: TDateTimeField;
          TStoricoAnnotazioni: TStringField;
          QStoricoDescEvento: TStringField;
          TAgenda: TTable;
          TAgendaData: TDateTimeField;
          TAgendaOre: TDateTimeField;
          TAgendaIDUtente: TIntegerField;
          TAgendaTipo: TSmallintField;
          TAgendaIDCandRic: TIntegerField;
          DsAziende: TDataSource;
          TLingue: TTable;
          DsLingue: TDataSource;
          TLingueID: TAutoIncField;
          TLingueLingua: TStringField;
          TAgendaDescrizione: TStringField;
          DsComuni: TDataSource;
          TZone: TTable;
          TZoneID: TAutoIncField;
          TZoneZona: TStringField;
          DsZone: TDataSource;
          TStoricoIDRicerca: TIntegerField;
          TStoricoIDUtente: TIntegerField;
          TStoricoIDCliente: TIntegerField;
          QStoricoNominativo: TStringField;
          QStoricoIDRicerca: TIntegerField;
          QStoricoIDCliente: TIntegerField;
          TRicercheLK: TTable;
          TRicercheLKID: TAutoIncField;
          TRicercheLKProgressivo: TStringField;
          TRicercheLKDataInizio: TDateTimeField;
          TRicercheLKConclusa: TBooleanField;
          QStoricoProgRicerca: TStringField;
          QStoricoDataInizioRic: TDateTimeField;
          QStoricoCliente: TStringField;
          TAgendaIDRisorsa: TIntegerField;
          TRisorse: TTable;
          DsRisorse: TDataSource;
          TRisorseID: TAutoIncField;
          TRisorseRisorsa: TStringField;
          TRisorseColor: TStringField;
          DB: TDatabase;
          TAnagrafica: TQuery;
          UpdAnag: TUpdateSQL;
          TAnagraficaID: TAutoIncField;
          TAnagraficaMatricola: TIntegerField;
          TAnagraficaCognome: TStringField;
          TAnagraficaNome: TStringField;
          TAnagraficaDataNascita: TDateTimeField;
          TAnagraficaGiornoNascita: TSmallintField;
          TAnagraficaMeseNascita: TSmallintField;
          TAnagraficaLuogoNascita: TStringField;
          TAnagraficaSesso: TStringField;
          TAnagraficaIndirizzo: TStringField;
          TAnagraficaCap: TStringField;
          TAnagraficaComune: TStringField;
          TAnagraficaIDComuneRes: TIntegerField;
          TAnagraficaIDZonaRes: TIntegerField;
          TAnagraficaProvincia: TStringField;
          TAnagraficaDomicilioIndirizzo: TStringField;
          TAnagraficaDomicilioCap: TStringField;
          TAnagraficaDomicilioComune: TStringField;
          TAnagraficaIDComuneDom: TIntegerField;
          TAnagraficaIDZonaDom: TIntegerField;
          TAnagraficaDomicilioProvincia: TStringField;
          TAnagraficaDomicilioStato: TStringField;
          TAnagraficaRecapitiTelefonici: TStringField;
          TAnagraficaFax: TStringField;
          TAnagraficaCodiceFiscale: TStringField;
          TAnagraficaPartitaIVA: TStringField;
          TAnagraficaIDStatoCivile: TIntegerField;
          TAnagraficaIDStato: TIntegerField;
          TAnagraficaIDTipoStato: TIntegerField;
          TAnagraficaCVNumero: TIntegerField;
          TAnagraficaCVIDAnnData: TIntegerField;
          TAnagraficaCVinseritoInData: TDateTimeField;
          TAnagraficaIDUtenteMod: TIntegerField;
          TAnagraficaDubbiIns: TStringField;
          TAnagraficaIDProprietaCV: TIntegerField;
          TAnagraficaStatoCivile: TStringField;
          TAnagraficaEta: TStringField;
          QClientiLK: TQuery;
          QClientiLKID: TAutoIncField;
          QClientiLKDescrizione: TStringField;
          TAnagraficaProprietaCV: TStringField;
          TAnagraficaDescStato: TStringField;
          TAnagABS: TQuery;
          TAnagABSID: TAutoIncField;
          TAnagABSCognome: TStringField;
          TAnagABSNome: TStringField;
          TAnagABSIDStato: TIntegerField;
          TAnagABSIDTipoStato: TIntegerField;
          TAnagABSCVNumero: TIntegerField;
          UpdAnagABS: TUpdateSQL;
          Global: TQuery;
          GlobalLastLogin: TStringField;
          GlobalLastLoginID: TIntegerField;
          GlobalUltimoProgRicerca: TSmallintField;
          GlobalUltimoProgFatt: TSmallintField;
          GlobalDirFileCV: TStringField;
          GlobalDirFileDoc: TStringField;
          GlobalOrarioInizio1: TDateTimeField;
          GlobalOrarioFine1: TDateTimeField;
          GlobalOrarioInizio2: TDateTimeField;
          GlobalOrarioFine2: TDateTimeField;
          GlobalCheckBoxIndici: TStringField;
          GlobalIndiciPeso1: TSmallintField;
          GlobalIndiciPeso2: TSmallintField;
          GlobalIndiciPeso3: TSmallintField;
          GlobalIndiciPeso4: TSmallintField;
          GlobalCoefficiente: TFloatField;
          GlobalNomeAzienda: TStringField;
          GlobalSedeAzienda: TStringField;
          GlobalSettoreAzienda: TStringField;
          GlobalComuneAzienda: TStringField;
          GlobalAppoggioBancario1: TStringField;
          GlobalAppoggioBancario2: TStringField;
          GlobalAppoggioBancario3: TStringField;
          Users: TQuery;
          UsersID: TAutoIncField;
          UsersNominativo: TStringField;
          UsersDescrizione: TStringField;
          UsersPassword: TStringField;
          UsersDataCreazione: TDateTimeField;
          UsersDataUltimoAccesso: TDateTimeField;
          UsersDataScadenza: TDateTimeField;
          UsersDataRevoca: TDateTimeField;
          UsersTipo: TSmallintField;
          TTitoliStudio: TQuery;
          TTitoliStudioDiploma: TStringField;
          TTitoliStudioID: TAutoIncField;
          TTitoliStudioIDAnagrafica: TIntegerField;
          TTitoliStudioIDDiploma: TIntegerField;
          TTitoliStudioLuogoConseguimento: TStringField;
          TTitoliStudioDataConseguimento: TStringField;
          TTitoliStudioLaureaBreve: TBooleanField;
          TTitoliStudioSpecializzazione: TStringField;
          TTitoliStudioTesiTitoloArgomento: TMemoField;
          TTitoliStudioKeywordsTesi: TStringField;
          TTitoliStudioAbilitazProfessionale: TStringField;
          UpdTitoliStudio: TUpdateSQL;
          TCorsiExtra: TQuery;
          UpdCorsiExtra: TUpdateSQL;
          TCorsiExtraID: TAutoIncField;
          TCorsiExtraIDAnagrafica: TIntegerField;
          TCorsiExtraTipologia: TStringField;
          TCorsiExtraOrganizzazione: TStringField;
          TCorsiExtraArgomenti: TMemoField;
          TCorsiExtraDurataOre: TSmallintField;
          TCorsiExtraDurataStageGiorni: TSmallintField;
          TCorsiExtraNomeAzienda: TStringField;
          TLingueConosc: TQuery;
          TLingueConoscID: TAutoIncField;
          TLingueConoscIDAnagrafica: TIntegerField;
          TLingueConoscLivello: TSmallintField;
          TLingueConoscSoggiorno: TStringField;
          TLingueConoscDescLivello: TStringField;
          TEspLav: TQuery;
          TEspLavID: TAutoIncField;
          TEspLavIDAnagrafica: TIntegerField;
          TEspLavIDAzienda: TIntegerField;
          TEspLavIDSettore: TIntegerField;
          TEspLavAttuale: TBooleanField;
          TEspLavAziendaNome: TStringField;
          TEspLavAziendaSede: TStringField;
          TEspLavAziendaComune: TStringField;
          TEspLavAziendaProvincia: TStringField;
          TEspLavAziendaStato: TStringField;
          TEspLavAziendaFatturato: TFloatField;
          TEspLavAziendaNumDipendenti: TSmallintField;
          TEspLavAziendaSettore: TStringField;
          TEspLavPosizioneGerarchia: TStringField;
          TEspLavAnnoDal: TSmallintField;
          TEspLavAnnoAl: TSmallintField;
          TEspLavDurataMesi: TSmallintField;
          TEspLavTipoContratto: TStringField;
          TEspLavLivelloContrattuale: TStringField;
          TEspLavRetribNettaMensile: TIntegerField;
          TEspLavMensilita: TSmallintField;
          TEspLavTitoloMansione: TStringField;
          TEspLavDescrizioneMansione: TMemoField;
          TEspLavIDArea: TIntegerField;
          TEspLavIDMansione: TIntegerField;
          TEspLavMotivoCessazione: TMemoField;
          TEspLavAzDenominazione: TStringField;
          TEspLavAzIndirizzo: TStringField;
          TEspLavAzCap: TStringField;
          TEspLavAzComune: TStringField;
          TEspLavAzProv: TStringField;
          TEspLavAzSettore: TStringField;
          TEspLavSettore: TStringField;
          UpdEspLav: TUpdateSQL;
          UpdLingueConosc: TUpdateSQL;
          TEspLavDescrizione: TStringField;
          TEspLavDescrizione_1: TStringField;
          TAziende: TQuery;
          TAziendeProvincia: TStringField;
          TAziendeIDAttivita: TIntegerField;
          TAziendeTelefono: TStringField;
          TAziendeFax: TStringField;
          TAziendeID: TAutoIncField;
          TAziendeDescrizione: TStringField;
          TAziendeIndirizzo: TStringField;
          TAziendeCap: TStringField;
          TAziendeComune: TStringField;
          TAziendePartitaIVA: TStringField;
          TAziendeCodiceFiscale: TStringField;
          TAziendeBancaAppoggio: TStringField;
          TAziendeSistemaPagamento: TStringField;
          TAziendeNoteContratto: TMemoField;
          TAziendeResponsabile: TStringField;
          TAziendeConosciutoInData: TDateTimeField;
          TAziendeIndirizzoLegale: TStringField;
          TAziendeCapLegale: TStringField;
          TAziendeComuneLegale: TStringField;
          TAziendeProvinciaLegale: TStringField;
          TAziendeAttivita: TStringField;
          TAnagraficaEmail: TStringField;
          UsersEmailConnectInfo: TMemoField;
          TAnagraficaTelUfficio: TStringField;
          TAnagraficaOldCVNumero: TStringField;
          QAnagNote: TQuery;
          DsQAnagNote: TDataSource;
          UpdAnagNote: TUpdateSQL;
          QAnagNoteNote: TMemoField;
          QAnagNoteIDAnagrafica: TIntegerField;
          QAnagFile: TQuery;
          DsQAnagFile: TDataSource;
          QAnagFileID: TAutoIncField;
          QAnagFileIDAnagrafica: TIntegerField;
          QAnagFileDescrizione: TStringField;
          QAnagFileIDTipo: TIntegerField;
          QAnagFileTipo: TStringField;
          Q2: TQuery;
          QSedi: TQuery;
          DsQSedi: TDataSource;
          QSediID: TAutoIncField;
          QSediDescrizione: TStringField;
          QSediIndirizzo: TStringField;
          QSediCap: TStringField;
          QSediComune: TStringField;
          QSediProvincia: TStringField;
          TAnagraficaTipoStrada: TStringField;
          TAnagraficaDomicilioTipoStrada: TStringField;
          TAnagraficaNumCivico: TStringField;
          TAnagraficaDomicilioNumCivico: TStringField;
          TAziendeTipoStrada: TStringField;
          TAziendeTipoStradaLegale: TStringField;
          TAziendeNumCivico: TStringField;
          TAziendeNumCivicoLegale: TStringField;
          QSediTipoStrada: TStringField;
          QSediNumCivico: TStringField;
          QProvCV: TQuery;
          DsQProvCV: TDataSource;
          QProvCVID: TAutoIncField;
          QProvCVProvenienza: TStringField;
          TAnagraficaIDUtente: TIntegerField;
          QComuni: TQuery;
          QComuniID: TAutoIncField;
          QComuniCodice: TStringField;
          QComuniDescrizione: TStringField;
          QComuniProv: TStringField;
          QComuniCap: TStringField;
          QComuniPrefisso: TStringField;
          QComuniIDZona: TIntegerField;
          QComuniZona: TStringField;
          TAnagraficaStato: TStringField;
          QNazioni: TQuery;
          DsQNazioni: TDataSource;
          QNazioniID: TAutoIncField;
          QNazioniAbbrev: TStringField;
          QNazioniDescNazione: TStringField;
          QNazioniAreaGeografica: TStringField;
          TAziendeNazioneAbb: TStringField;
          TAnagMansioni: TQuery;
          TAnagMansioniID: TAutoIncField;
          TAnagMansioniAttuale: TBooleanField;
          TAnagMansioniAnniEsperienza: TSmallintField;
          TAnagMansioniDisponibile: TBooleanField;
          TAnagMansioniMansione: TStringField;
          TAnagMansioniArea: TStringField;
          TAnagMansioniIDMansione: TIntegerField;
          TAnagraficaFoto: TBlobField;
          QTipiStrada: TQuery;
          DsQTipiStrada: TDataSource;
          QTipiStradaID: TAutoIncField;
          QTipiStradaTipoStrada: TStringField;
          QTipiStradaNazioneAbb: TStringField;
          GlobalNazioneAbbAzienda: TStringField;
          QAnagFileDataCreazione: TDateTimeField;
          QAnagNoteRetribuzione: TStringField;
          QNazioniDescNazionalita: TStringField;
          TAziendeStato: TStringField;
          TAziendeCartellaDoc: TStringField;
          TAziendeNazioneAbbLegale: TStringField;
          TAziendeNote: TMemoField;
          TAziendeFatturato: TFloatField;
          TAziendeNumDipendenti: TSmallintField;
          TEspLavFatturato: TFloatField;
          TEspLavNumDipendenti: TIntegerField;
          TLingueConoscLivelloScritto: TSmallintField;
          TLingueConoscDescLivelloScritto: TStringField;
          QAnagNoteNazionalita: TStringField;
          QAnagNoteTipoRetrib: TStringField;
          QAnagNoteTipoRetribEng: TStringField;
          TAziendeLK: TQuery;
          TAziendeLKDescrizione: TStringField;
          TAziendeLKIndirizzo: TStringField;
          TAziendeLKCap: TStringField;
          TAziendeLKComune: TStringField;
          TAziendeLKProvincia: TStringField;
          TAziendeLKSettore: TStringField;
          TAziendeLKFatturato: TFloatField;
          TAziendeLKNumDipendenti: TSmallintField;
          TAziendeLKID: TAutoIncField;
          QAnagNoteInquadramento: TStringField;
          QAnagNoteBenefits: TStringField;
          TAnagraficaLibPrivacy: TBooleanField;
          QAnagFileNomeFile: TStringField;
          TAnagraficaCVDataReale: TDateTimeField;
          GlobalAddAnnoRic: TBooleanField;
          GlobalUltimoProgOfferta: TSmallintField;
          GlobalUltimoProgAnnuncio: TSmallintField;
          QComuniRegione: TStringField;
          TAnagraficaCVDataAgg: TDateTimeField;
          TEspLavDescrizioneAttivitaAz: TMemoField;
          TTitoliStudioVotazione: TStringField;
          TEspLavPreavvisoGiorni: TSmallintField;
          TEspLavPreavvisoCalendario: TBooleanField;
          TEspLavPreavvisoTermQualsGiorno: TBooleanField;
          TEspLavPreavvisoTermGiorno: TSmallintField;
          TAnagMansioniAspirazione: TBooleanField;
          TAziendeLKTelefono: TStringField;
          TEspLavAzTelefono: TStringField;
          QComuniAreaNielsen: TSmallintField;
          TAnagraficaIDTipologia: TIntegerField;
          QTipologieSoggLK: TQuery;
          QTipologieSoggLKID: TAutoIncField;
          QTipologieSoggLKTipologia: TStringField;
          TAnagraficaTipologia: TStringField;
          TAnagraficaCellulare: TStringField;
          TAnagraficaTitolo: TStringField;
          TEspLavIDMansioneTemp: TIntegerField;
          TEspLavRuolo: TStringField;
          TDiplomiTipo_ENG: TStringField;
          TDiplomiDescrizione_ENG: TStringField;
          TDiplomiLKTipo_ENG: TStringField;
          TDiplomiLKDescrizione_ENG: TStringField;
          TLingueLingua_ENG: TStringField;
          TLingueConoscIdLingua: TIntegerField;
          QLingueLK: TQuery;
          QLingueLKID: TAutoIncField;
          QLingueLKLingua: TStringField;
          QLingueLKIDAzienda: TIntegerField;
          QLingueLKLingua_ENG: TStringField;
          TLingueConoscLingua: TStringField;
          TLivelloLingueLivelloConoscenza_ENG: TStringField;
          TCorsiExtraConseguitoAnno: TIntegerField;
          QSettoriLK: TQuery;
          QSettoriLKID: TAutoIncField;
          QSettoriLKAttivita: TStringField;
          QSettoriLKIDAreaSettore: TIntegerField;
          QSettoriLKIDAzienda: TStringField;
          QSettoriLKAttivita_ENG: TStringField;
          TEspLavDescrizione_ENG: TStringField;
          TEspLavDescrizione_ENG_1: TStringField;
          TAnagMansioniMansione_ENG: TStringField;
          TAnagMansioniArea_ENG: TStringField;
          TEspLavMeseDal: TSmallintField;
          TEspLavMeseAl: TSmallintField;
          TTitoliStudioTipoDiploma: TStringField;
          GlobalDirFileReports: TStringField;
          procedure DsAnagraficaStateChange(Sender: TObject);
          procedure DsTitoliStudioStateChange(Sender: TObject);
          procedure DsCorsiExtraStateChange(Sender: TObject);
          procedure TAnagraficaCalcFields(DataSet: TDataSet);
          procedure TAnagraficaBeforePost(DataSet: TDataSet);
          procedure TPromemoriaAfterPost(DataSet: TDataSet);
          procedure DataCreate(Sender: TObject);
          procedure DsStatiStateChange(Sender: TObject);
          procedure TTitoliStudioAfterInsert(DataSet: TDataSet);
          procedure DsDiplomiStateChange(Sender: TObject);
          procedure DsAreeDiplomiStateChange(Sender: TObject);
          procedure DsEventiStateChange(Sender: TObject);
          procedure TEventiAfterInsert(DataSet: TDataSet);
          procedure TEspLavAfterInsert(DataSet: TDataSet);
          procedure TAnagraficaAfterPost(DataSet: TDataSet);
          procedure TStoricoAfterPost(DataSet: TDataSet);
          procedure TPromemABSAfterInsert(DataSet: TDataSet);
          procedure TPromemABSAfterPost(DataSet: TDataSet);
          procedure TAnagraficaAfterInsert(DataSet: TDataSet);
          procedure DsLingueStateChange(Sender: TObject);
          procedure TAnagraficaAfterScroll(DataSet: TDataSet);
          procedure TAnagraficaAfterDelete(DataSet: TDataSet);
          procedure TTitoliStudioBeforeEdit(DataSet: TDataSet);
          procedure TCorsiExtraBeforeEdit(DataSet: TDataSet);
          procedure TLingueConoscBeforeEdit(DataSet: TDataSet);
          procedure TEspLavBeforeEdit(DataSet: TDataSet);
          procedure QStoricoBeforeOpen(DataSet: TDataSet);
          procedure QStoricoAfterClose(DataSet: TDataSet);
          procedure TEspLavBeforeOpen(DataSet: TDataSet);
          procedure TEspLavAfterClose(DataSet: TDataSet);
          procedure TDisponibAnagBeforeOpen(DataSet: TDataSet);
          procedure TDisponibAnagAfterClose(DataSet: TDataSet);
          procedure TLingueConoscBeforeOpen(DataSet: TDataSet);
          procedure TLingueConoscAfterClose(DataSet: TDataSet);
          procedure TTitoliStudioBeforeOpen(DataSet: TDataSet);
          procedure TTitoliStudioAfterClose(DataSet: TDataSet);
          procedure TAziendeBeforeOpen(DataSet: TDataSet);
          procedure TAziendeAfterClose(DataSet: TDataSet);
          procedure TMansioniLKBeforeOpen(DataSet: TDataSet);
          procedure TStatiAfterPost(DataSet: TDataSet);
          procedure TDiplomiAfterPost(DataSet: TDataSet);
          procedure TAreeDiplomiAfterPost(DataSet: TDataSet);
          procedure TLingueAfterPost(DataSet: TDataSet);
          procedure DsQAnagNoteStateChange(Sender: TObject);
          procedure DsGlobalStateChange(Sender: TObject);
          procedure TTitoliStudioAfterPost(DataSet: TDataSet);
          procedure TTitoliStudioAfterDelete(DataSet: TDataSet);
          procedure TCorsiExtraAfterPost(DataSet: TDataSet);
          procedure TCorsiExtraAfterDelete(DataSet: TDataSet);
          procedure TEspLavAfterPost(DataSet: TDataSet);
          procedure TZoneAfterPost(DataSet: TDataSet);
          procedure TRisorseAfterPost(DataSet: TDataSet);
          procedure QAnagNoteAfterPost(DataSet: TDataSet);
          procedure TEventiAfterPost(DataSet: TDataSet);
          procedure DBBeforeConnect(Sender: TObject);
          procedure TAnagraficaBeforeOpen(DataSet: TDataSet);
          procedure TAnagraficaAfterClose(DataSet: TDataSet);
          procedure TAnagraficaBeforeEdit(DataSet: TDataSet);
     private
          { private declarations }
          xIDRuolo, xIDArea, xIDFeature, xVecchioIDAnag: integer;
     public
          { public declarations }
          xInterrForm, xValutazForm, xIntervCons, xOKcheck, xFiltraArea, xControllaComune: boolean;
          xNuovoRuolo, xSelRuolo, xInsLingua, xControlla, xSchedaCand: boolean;
          xLinkAnnuncio: integer;
          xPwdSa: TStringList;
          function GetEnvValue (varName : string) : string;

     end;

var
     Data: TData;

implementation

uses Main, Curriculum, SelArea,
     ModuloDati2, ValutazDip, CompInserite, InsRuolo,
     InsCaratt, Lingue, uUtilsVarie, SchedaCand;

{$R *.DFM}

procedure TData.DsAnagraficaStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsAnagrafica.State in [dsEdit, dsInsert];
     MainForm.TbBAnagNew.Enabled := not b;
     MainForm.TbBAnagDel.Enabled := not b;
     MainForm.TbBAnagMod.Enabled := not b;
     MainForm.PanAnag.Enabled := b;
     MainForm.PanAnag2.Enabled := b;
     MainForm.TbBAnagOK.Enabled := b;
     MainForm.TbBAnagCan.Enabled := b;
     //     ColloquioForm.TbBAnagOK.Enabled:=b;
     //     ColloquioForm.TbBAnagCan.Enabled:=b;
end;

procedure TData.DsTitoliStudioStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsTitoliStudio.State in [dsEdit, dsInsert];
     CurriculumForm.TbBTitoliNew.Enabled := not b;
     CurriculumForm.TbBTitoliDel.Enabled := not b;
     CurriculumForm.TbBTitoliOK.Enabled := b;
     CurriculumForm.TbBTitoliCan.Enabled := b;
end;

procedure TData.DsCorsiExtraStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsCorsiExtra.State in [dsEdit, dsInsert];
     CurriculumForm.TbBCorsiExtraNew.Enabled := not b;
     CurriculumForm.TbBCorsiExtraDel.Enabled := not b;
     CurriculumForm.TbBCorsiExtraOK.Enabled := b;
     CurriculumForm.TbBCorsiExtraCan.Enabled := b;
end;

procedure TData.TAnagraficaCalcFields(DataSet: TDataSet);
var xAnno, xMese, xgiorno: Word;
begin
     if TAnagraficaDataNascita.asString = '' then
          TAnagraficaEta.asString := ''
     else begin
          DecodeDate((Date - TAnagraficaDataNascita.Value + 1), xAnno, xMese, xgiorno);
          // TAnagraficaEta.Value:=StrToInt(copy(xAnno,3,2));
          TAnagraficaEta.Value := copy(IntToStr(xAnno), 3, 2);
     end;
end;

procedure TData.TAnagraficaBeforePost(DataSet: TDataSet);
var xAnno, xMese, xgiorno: Word;
     xData: TDateTime;
     i, k: integer;
     xCogn, xNome: string;
begin
     if xOKcheck then begin
          if (TAnagraficaCognome.Value = '') or (TAnagraficaNome.Value = '') then
               MessageDlg('ATTENZIONE: il cognome o il nome sono vuoti !!', mtError, [mbOK], 0);
          // controllo omonimia
          xCogn := TAnagraficaCognome.Value;
          xNome := TAnagraficaNome.Value;
          Q1.close;
          Q1.SQL.clear;
          Q1.SQL.add('select count(*) Num from Anagrafica where cognome like "%' + xCogn + '%"');
          Q1.SQL.add('and Nome like "%' + xNome + '%"');
          Q1.Open;
          if Data.Q1.FieldByName('Num').asInteger > 1 then
               MessageDlg('ATTENZIONE: verificare possibile omonimia', mtWarning, [mbOK], 0);
     end;

     if TAnagraficaDataNascita.asString <> '' then begin
          DecodeDate(TAnagraficaDataNascita.VAlue, xAnno, xMese, xGiorno);
          TAnagraficaGiornoNascita.Value := xGiorno;
          TAnagraficaMeseNascita.Value := xMese;
     end;
     // Ultimo utente ad aver aggiornato i dati
     TAnagraficaIDUtenteMod.Value := MainForm.xIDUtenteAttuale;
end;

procedure TData.TPromemoriaAfterPost(DataSet: TDataSet);
begin
     QPromOggi.Close;
     QPromOggi.Open;
     case QPromOggi.RecordCount of
          0: MainForm.StatusBar1.Panels[2].Text := 'nessun messaggio in scadenza oggi';
          1: MainForm.StatusBar1.Panels[2].Text := '� presente un messaggio in scadenza oggi';
     else MainForm.StatusBar1.Panels[2].Text := 'Sono presenti ' + IntToStr(QPromOggi.RecordCount) + ' messaggi in scadenza oggi';
     end;
     QPromOggi.Close;
end;

procedure TData.DataCreate(Sender: TObject);
begin
     xInterrForm := False;
     xValutazForm := False;
     xIntervCons := False;
     xSelRuolo := True;
     xNuovoRuolo := False;
     xInsLingua := True;
     xOKcheck := True;
     xFiltraArea := True;
     xLinkAnnuncio := 0;
     xControlla := False;
     xSchedaCand := False;
     xControllaComune := True;
     // caricamento codici Anag dei primi consentiti
     CaricaPrimi;
end;

procedure TData.DsStatiStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsStati.State in [dsEdit, dsInsert];
     MainForm.TbBStatiNew.Enabled := not b;
     MainForm.TbBStatiDel.Enabled := not b;
     MainForm.TbBStatiOK.Enabled := b;
     MainForm.TbBStatiCan.Enabled := b;
end;

procedure TData.TTitoliStudioAfterInsert(DataSet: TDataSet);
begin
     TTitoliStudioLaureaBreve.Value := False;
end;

procedure TData.DsDiplomiStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsDiplomi.State in [dsEdit, dsInsert];
     MainForm.TbBDiplomiNew.Enabled := not b;
     MainForm.TbBDiplomiDel.Enabled := not b;
     MainForm.TbBDiplomiOK.Enabled := b;
     MainForm.TbBDiplomiCan.Enabled := b;
end;

procedure TData.DsAreeDiplomiStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsAreeDiplomi.State in [dsEdit, dsInsert];
     MainForm.TbBAreeDiplNew.Enabled := not b;
     MainForm.TbBAreeDiplDel.Enabled := not b;
     MainForm.TbBAreeDiplOK.Enabled := b;
     MainForm.TbBAreeDiplCan.Enabled := b;
end;

procedure TData.DsEventiStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsEventi.State in [dsEdit, dsInsert];
     MainForm.TbBEventiNew.Enabled := not b;
     MainForm.TbBEventiDel.Enabled := not b;
     MainForm.TbBEventiMod.Enabled := not b;
     //MainForm.DBGEventi.ReadOnly:=not b;
     MainForm.TbBEventiOK.Enabled := b;
     MainForm.TbBEventiCan.Enabled := b;
end;

procedure TData.TEventiAfterInsert(DataSet: TDataSet);
begin
     TEventiNonCancellabile.Value := False;
end;

procedure TData.TEspLavAfterInsert(DataSet: TDataSet);
begin
     TEspLavMensilita.Value := 14;
end;

procedure TData.TAnagraficaAfterPost(DataSet: TDataSet);
begin
     //MainForm.LTotCand.Caption:=IntToStr(TAnagrafica.RecordCount);
     Log_Operation(MainForm.xIDUtenteAttuale, 'Anagrafica', TAnagraficaID.Value, 'U');
end;

procedure TData.TStoricoAfterPost(DataSet: TDataSet);
begin
     QStorico.Close;
     QStorico.Open;
end;

procedure TData.TPromemABSAfterInsert(DataSet: TDataSet);
begin
     TPromemABSIDUtente.Value := MainForm.xIDUtenteAttuale;
     TPromemABSIDUtenteDa.Value := MainForm.xIDUtenteAttuale;
     TPromemABSDataIns.Value := Date;
     TPromemABSDataDaLeggere.Value := Date;
     TPromemABSEvaso.Value := False;
end;

procedure TData.TPromemABSAfterPost(DataSet: TDataSet);
begin
     if TPromemoria.Active then TPromemoria.Refresh;
     QPromOggi.Close;
     QPromOggi.Open;
end;

procedure TData.TAnagraficaAfterInsert(DataSet: TDataSet);
begin
     if MainForm.Pagecontrol5.ActivePage = MainForm.TsStatoDip then begin
          MainForm.TSAnagStorico.TabVisible := True;
     end;
     if MainForm.Pagecontrol5.ActivePage = MainForm.TsStatoEsterno then begin
          MainForm.TSAnagStorico.TabVisible := True;
     end;
     if MainForm.Pagecontrol5.ActivePage = MainForm.TsStatoUscito then begin
          MainForm.TSAnagStorico.TabVisible := True;
     end;
end;

procedure TData.DsLingueStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsLingue.State in [dsEdit, dsInsert];
     MainForm.TbBLingueNew.Enabled := not b;
     MainForm.TbBLingueDel.Enabled := not b;
     MainForm.TbBLingueOK.Enabled := b;
     MainForm.TbBLingueCan.Enabled := b;
end;

procedure TData.TAnagraficaAfterScroll(DataSet: TDataSet);
begin
     if xControlla then SettaDiritti(TAnagraficaID.Value);
     if (TAnagraficaIDProprietaCV.asString = '') or (TAnagraficaIDProprietaCV.Value = 0) then
          Mainform.EPropCV.Text := GlobalNomeAzienda.Value
     else begin
          Q1.Close;
          Q1.SQL.text := 'select Descrizione from EBC_Clienti where ID=' + TAnagraficaIDProprietaCV.asString;
          Q1.Open;
          Mainform.EPropCV.text := Q1.FieldbyName('Descrizione').asString;
          Q1.Close;
     end;
     // Utente associato al candidato
     if (TAnagraficaIDUtente.asString <> '') and (TAnagraficaIDUtente.Value > 0) then begin
          Q1.Close;
          Q1.SQL.text := 'select Nominativo from Users where ID=' + TAnagraficaIDUtente.asString;
          Q1.Open;
          Mainform.ECandUtente.Text := Q1.FieldbyName('Nominativo').asString;
          Q1.Close;
     end else Mainform.ECandUtente.Text := '';
end;

procedure TData.TAnagraficaAfterDelete(DataSet: TDataSet);
begin
     //MainForm.LTotCand.Caption:=IntToStr(TAnagrafica.RecordCount);
end;

procedure TData.TTitoliStudioBeforeEdit(DataSet: TDataSet);
begin
     if not MainForm.CheckProfile('05112') then Abort;
end;

procedure TData.TCorsiExtraBeforeEdit(DataSet: TDataSet);
begin
     if not MainForm.CheckProfile('05122') then Abort;
end;

procedure TData.TLingueConoscBeforeEdit(DataSet: TDataSet);
begin
     if not MainForm.CheckProfile('05132') then Abort;
end;

procedure TData.TEspLavBeforeEdit(DataSet: TDataSet);
begin
     if not MainForm.CheckProfile('05142') then Exit;
end;

procedure TData.QStoricoBeforeOpen(DataSet: TDataSet);
begin
     TEventiLK.Open;
     TRicercheLK.Open;
     //TClientiLK.Open;
end;

procedure TData.QStoricoAfterClose(DataSet: TDataSet);
begin
     TEventiLK.Close;
     TRicercheLK.Close;
     //TClientiLK.Close;
end;

procedure TData.TEspLavBeforeOpen(DataSet: TDataSet);
begin
  //   if TMansioniLK.Active = false then TMansioniLK.Open;
     //if QSettoriLK.Active = false then QSettoriLK.Open;
    // if TAziendeLK.Active = false then TAziendeLK.Open;
end;

procedure TData.TEspLavAfterClose(DataSet: TDataSet);
begin
     TMansioniLK.Close;
     TAziendeLK.Close;
     QSettoriLK.Close;
end;

procedure TData.TDisponibAnagBeforeOpen(DataSet: TDataSet);
begin
     TMansioniLK.Open;
end;

procedure TData.TDisponibAnagAfterClose(DataSet: TDataSet);
begin
     TMansioniLK.Close;
end;

procedure TData.TLingueConoscBeforeOpen(DataSet: TDataSet);
begin
     TLivelloLingue.Open;
     QLingueLK.Open;
end;

procedure TData.TLingueConoscAfterClose(DataSet: TDataSet);
begin
     TLivelloLingue.Close;
     QLingueLK.Close;
end;

procedure TData.TTitoliStudioBeforeOpen(DataSet: TDataSet);
begin
     TDiplomiLK.Open;
end;

procedure TData.TTitoliStudioAfterClose(DataSet: TDataSet);
begin
     TDiplomiLK.Close;
end;

procedure TData.TAziendeBeforeOpen(DataSet: TDataSet);
begin
     QSettoriLK.Open;
end;

procedure TData.TAziendeAfterClose(DataSet: TDataSet);
begin
     QSettoriLK.Close;
end;

procedure TData.TMansioniLKBeforeOpen(DataSet: TDataSet);
begin
     if not TAreeLK.Active then TAreeLK.Open;
end;

procedure TData.TStatiAfterPost(DataSet: TDataSet);
begin
     TStati.Close;
     TStati.Open;
end;

procedure TData.TDiplomiAfterPost(DataSet: TDataSet);
begin
     TDiplomi.Close;
     TDiplomi.Open;
end;

procedure TData.TAreeDiplomiAfterPost(DataSet: TDataSet);
begin
     TAreeDiplomi.Close;
     TAreeDiplomi.Open;
end;

procedure TData.TLingueAfterPost(DataSet: TDataSet);
begin
     TLingue.Close;
     TLingue.Open;
end;

procedure TData.DsQAnagNoteStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsQAnagNote.State in [dsEdit, dsInsert];
     //MainForm.BAnagNoteOK.Enabled:=b;
     //MainForm.BAnagNoteCan.Enabled:=b;
end;

procedure TData.DsGlobalStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsGlobal.State in [dsEdit, dsInsert];
     MainForm.BGlobalOK.Enabled := b;
     MainForm.BGlobalCAn.Enabled := b;
end;

procedure TData.TTitoliStudioAfterPost(DataSet: TDataSet);
begin
     Log_Operation(MainForm.xIDUtenteAttuale, 'TitoliStudio', TTitoliStudioID.Value, 'U');
end;

procedure TData.TTitoliStudioAfterDelete(DataSet: TDataSet);
begin
     Log_Operation(MainForm.xIDUtenteAttuale, 'TitoliStudio', TTitoliStudioID.Value, 'D');
     with TTitoliStudio do begin
          DB.BeginTrans;
          try ApplyUpdates; DB.CommitTrans;
          except DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
          end;
          CommitUpdates;
     end;
end;

procedure TData.TCorsiExtraAfterPost(DataSet: TDataSet);
begin
     Log_Operation(MainForm.xIDUtenteAttuale, 'CorsiExtra', TCorsiExtraID.Value, 'U');
end;

procedure TData.TCorsiExtraAfterDelete(DataSet: TDataSet);
begin
     Log_Operation(MainForm.xIDUtenteAttuale, 'CorsiExtra', TCorsiExtraID.Value, 'D');
end;

procedure TData.TEspLavAfterPost(DataSet: TDataSet);
begin
     Log_Operation(MainForm.xIDUtenteAttuale, 'EsperienzeLavorative', TEspLavID.value, 'U');
end;

procedure TData.TZoneAfterPost(DataSet: TDataSet);
begin
     TZone.Close;
     TZone.Open;
end;

procedure TData.TRisorseAfterPost(DataSet: TDataSet);
begin
     TRisorse.Close;
     TRisorse.Open;
end;

procedure TData.QAnagNoteAfterPost(DataSet: TDataSet);
begin
     QAnagNote.ApplyUpdates;
     QAnagNote.CommitUpdates;
end;

procedure TData.TEventiAfterPost(DataSet: TDataSet);
begin
     TEventi.Close;
     TEventi.Open;
end;

procedure TData.DBBeforeConnect(Sender: TObject);
var UserP : String;
begin
     // gestione temporanea password
     xPwdSa := TStringList.create;
     DB.Params.clear;
     if FileExists(ExtractFileDir(Application.ExeName) + '\Ugh1PdSa.txt') then begin
          xPwdSa.LoadFromFile(ExtractFileDir(Application.ExeName) + '\Ugh1PdSa.txt');
          DB.Params.add('USER NAME=' + xPwdSa.Strings[0]);
          DB.Params.add('PASSWORD=' + xPwdSa.Strings[1]);
     end else begin
          DB.Params.add('USER NAME=sa');
          DB.Params.add('PASSWORD=sa');
     end;
     //DB.Connected:=true;
     DB.Session.PrivateDir := GetEnvValue('TEMP');

     xPwdSa.Free;

end;

procedure TData.TAnagraficaBeforeOpen(DataSet: TDataSet);
begin
     QTipologieSoggLK.Open;
end;

procedure TData.TAnagraficaAfterClose(DataSet: TDataSet);
begin
     QTipologieSoggLK.Close;
end;

procedure TData.TAnagraficaBeforeEdit(DataSet: TDataSet);
begin
     //     if CurriculumForm.dsAnagAltriDati.State in [dsInsert,dsEdit] then
     if CurriculumForm.TAnagAltriDati.Active then begin
          with CurriculumForm.TAnagAltriDati do begin
               Data.DB.BeginTrans;
               try
                    ApplyUpdates;
                    Data.DB.CommitTrans;
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate in AnagAltriDati', mtError, [mbOK], 0);
               end;
               CommitUpdates;
          end;
     end;
end;

function TData.GetEnvValue(varName: string): string;
var  size           : integer;
     envVal         : pChar;

begin
size := GetEnvironmentVariable (pChar (varName), nil, 0);
if size > 0
then begin
     envVal := AllocMem (size);
     try
          if GetEnvironmentVariable (pChar (varName), envVal, size) > 0
          then result := string (envVal)
          else result := '';
     finally
          FreeMem (envVal, size);
          end;
     end
else result := '';
end;
end.

