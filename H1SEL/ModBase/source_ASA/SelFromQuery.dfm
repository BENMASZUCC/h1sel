object SelFromQueryForm: TSelFromQueryForm
  Left = 230
  Top = 106
  Width = 552
  Height = 245
  Caption = 'Selezione da elenco'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 544
    Height = 41
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object BitBtn1: TBitBtn
      Left = 444
      Top = 3
      Width = 97
      Height = 35
      Anchors = [akTop, akRight]
      TabOrder = 0
      Kind = bkOK
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 41
    Width = 544
    Height = 177
    Align = alClient
    DataSource = DsQGen
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object QGen: TQuery
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select Ann_AnnunciRicerche.ID,Codice,'
      '       EBC_Ricerche.Progressivo Rif,'
      '       EBC_Ricerche.NumRicercati Num,'
      '       Mansioni.Descrizione Ruolo,'
      '       EBC_Clienti.Descrizione Cliente'
      'from Ann_AnnunciRicerche,EBC_Ricerche,Mansioni,EBC_Clienti'
      'where Ann_AnnunciRicerche.IDRicerca=EBC_Ricerche.ID'
      '  and EBC_Ricerche.IDMansione=Mansioni.ID'
      '  and EBC_Ricerche.IDCliente=EBC_Clienti.ID')
    Left = 56
    Top = 96
  end
  object DsQGen: TDataSource
    DataSet = QGen
    Left = 56
    Top = 128
  end
end
