�
 TINSCOMPETENZAFORM 0�  TPF0TInsCompetenzaFormInsCompetenzaFormLeftTopnActiveControlDBGrid1BorderStylebsDialogCaptionScelta competenzaClientHeightiClientWidth!Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrder	PositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TLabelLabel2LeftTop� Width� HeightCaption+(nessuna competenza per l'area selezionata)  TPanelPanel1Left� Top-WidthDHeight	AlignmenttaLeftJustifyCaption,  Elenco competenze (per l'area selezionata)ColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderTabStop	  TBitBtnBitBtn1LeftoTopWidthRHeight"TabOrder OnClickBitBtn1ClickKindbkOK  TBitBtnBitBtn2Left�TopWidthRHeight"CaptionAnnullaTabOrderKindbkCancel  TPanelPanel2LeftTopWidth� Height	AlignmenttaLeftJustifyCaption  Aree competenzeColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderTabStop	  TDBGridDBGrid1LeftTop-Width� Height5
DataSource
DsAreeCompTabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style 
OnKeyPressDBGrid1KeyPressColumnsExpanded	FieldNameDescrizioneTitle.CaptionAreaWidth� Visible	    TAdvStringGridASG1Left� TopCWidthCHeightColCountDefaultRowHeight	FixedCols OptionsgoFixedVertLinegoFixedHorzLine
goVertLine
goHorzLinegoRangeSelectgoColSizing	goEditing 
ScrollBars
ssVerticalTabOrderAutoNumAlignAutoSize
VAlignment	vtaCenterEnhTextSizeEnhRowColMoveSortFixedColsSizeWithFormMultilinecellsSortDirectionsdAscendingOnGetEditorTypeASG1GetEditorTypeSortFull	SortAutoFormat	SortShowEnableGraphics	
SortColumn 	HintColorclYellowSelectionColorclHighlightSelectionTextColorclHighlightTextSelectionRectangleSelectionRTFKeepHintShowCellsOleDropTargetOleDropSource
OleDropRTFPrintSettings.FooterSize PrintSettings.HeaderSize PrintSettings.TimeppNonePrintSettings.DateppNonePrintSettings.DateFormat
dd/mm/yyyyPrintSettings.PageNrppNonePrintSettings.TitleppNonePrintSettings.Font.CharsetDEFAULT_CHARSETPrintSettings.Font.ColorclWindowTextPrintSettings.Font.Height�PrintSettings.Font.NameMS Sans SerifPrintSettings.Font.Style  PrintSettings.HeaderFont.CharsetDEFAULT_CHARSETPrintSettings.HeaderFont.ColorclWindowTextPrintSettings.HeaderFont.Height�PrintSettings.HeaderFont.NameMS Sans SerifPrintSettings.HeaderFont.Style  PrintSettings.FooterFont.CharsetDEFAULT_CHARSETPrintSettings.FooterFont.ColorclWindowTextPrintSettings.FooterFont.Height�PrintSettings.FooterFont.NameMS Sans SerifPrintSettings.FooterFont.Style PrintSettings.Borders
pbNoborderPrintSettings.BorderStylepsSolidPrintSettings.CenteredPrintSettings.RepeatFixedRowsPrintSettings.RepeatFixedColsPrintSettings.LeftSize PrintSettings.RightSize PrintSettings.ColumnSpacing PrintSettings.RowSpacing PrintSettings.TitleSpacing PrintSettings.Orientation
poPortraitPrintSettings.PagePrefixpagePrintSettings.FixedWidth PrintSettings.FixedHeight PrintSettings.UseFixedHeightPrintSettings.UseFixedWidthPrintSettings.FitToPagefpNeverPrintSettings.PageNumSep/PrintSettings.NoAutoSizePrintSettings.PrintGraphicsHTMLSettings.WidthdNavigation.AllowInsertRowNavigation.AllowDeleteRowNavigation.AdvanceOnEnterNavigation.AdvanceInsertNavigation.AutoGotoWhenSortedNavigation.AutoGotoIncrementalNavigation.AutoComboDropSizeNavigation.AdvanceDirectionadLeftRight"Navigation.AllowClipboardShortCutsNavigation.AllowSmartClipboardNavigation.AllowRTFClipboardNavigation.AdvanceAutoNavigation.InsertPositionpInsertBeforeNavigation.CursorWalkEditorNavigation.MoveRowOnSortNavigation.ImproveMaskSelNavigation.AlwaysEditColumnSize.SaveColumnSize.StretchColumnSize.Location
clRegistryCellNode.ColorclSilverCellNode.NodeTypecnFlatCellNode.NodeColorclBlackSizeWhileTyping.HeightSizeWhileTyping.WidthMouseActions.AllSelectMouseActions.ColSelectMouseActions.RowSelectMouseActions.DirectEdit	MouseActions.DisjunctRowSelectMouseActions.AllColumnSizeMouseActions.CaretPositioning
IntelliPan
ipVerticalURLColorclBlackURLShowURLFullURLEdit
ScrollTypessNormalScrollColorclNoneScrollWidthScrollProportionalScrollHintsshNone
OemConvertFixedFooters FixedRightCols FixedColWidthFixedRowHeightFixedFont.CharsetDEFAULT_CHARSETFixedFont.ColorclWindowTextFixedFont.Height�FixedFont.NameMS Sans SerifFixedFont.Style FixedAsButtonsFloatFormat%.2fWordWrapLookupLookupCaseSensitiveLookupHistoryHideFocusRectBackGround.Top BackGround.Left BackGround.DisplaybdTileHoveringFilter FilterActive	ColWidths* 
RowHeights   TTable	TCompAreaActive	DatabaseNameEBCDB	IndexNameIdxAreaMasterFieldsIDMasterSource
DsAreeComp	TableNamedbo.CompetenzeLeftTop�  TAutoIncFieldTCompAreaID	FieldNameID  TIntegerFieldTCompAreaIDArea	FieldNameIDArea  TStringFieldTCompAreaDescrizione	FieldNameDescrizioneSize(  TStringFieldTCompAreaTipologia	FieldName	TipologiaSize  TIntegerFieldTCompAreaQuantiValori	FieldNameQuantiValori  TStringFieldTCompAreaArrayDescrizione	FieldNameArrayDescrizioneSized   TDataSource
DsCompAreaDataSet	TCompAreaLeftTop�   TTable	TAreeCompActive	AfterScrollTAreeCompAfterScrollDatabaseNameEBCDB	IndexNameIdxDescrizAreaReadOnly		TableNamedbo.AreeCompetenzeLeft0Top�  TAutoIncFieldTAreeCompID	FieldNameIDReadOnly	  TStringFieldTAreeCompDescrizione	FieldNameDescrizioneSize   TDataSource
DsAreeCompDataSet	TAreeCompLeft0Top�   TTimerTimer1OnTimerTimer1TimerLeftTop@   