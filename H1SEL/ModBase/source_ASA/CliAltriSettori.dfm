object CliAltriSettoriForm: TCliAltriSettoriForm
  Left = 353
  Top = 111
  BorderStyle = bsDialog
  Caption = 'Altri settori per l'#39'azienda'
  ClientHeight = 290
  ClientWidth = 432
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 432
    Height = 43
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 6
      Width = 83
      Height = 13
      Caption = 'Attivit� principale:'
    end
    object DBText1: TDBText
      Left = 7
      Top = 21
      Width = 233
      Height = 16
      DataField = 'Attivita'
      DataSource = Data2.DsEBCclienti
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object BitBtn1: TBitBtn
      Left = 339
      Top = 2
      Width = 90
      Height = 38
      Caption = 'Esci'
      TabOrder = 0
      Kind = bkOK
    end
    object BitBtn2: TBitBtn
      Left = 247
      Top = 2
      Width = 90
      Height = 38
      Caption = 'Annulla'
      TabOrder = 1
      Visible = False
      Kind = bkCancel
    end
  end
  object Panel2: TPanel
    Left = 339
    Top = 43
    Width = 93
    Height = 247
    Align = alRight
    BevelOuter = bvLowered
    TabOrder = 1
    object BAnagFileNew: TToolbarButton97
      Left = 1
      Top = 1
      Width = 91
      Height = 37
      Hint = 'cerca, associa e definisci il '#13#10'tipo per un file gi� esistente'
      Caption = 'Associa settore'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        33333333FF33333333FF333993333333300033377F3333333777333993333333
        300033F77FFF3333377739999993333333333777777F3333333F399999933333
        33003777777333333377333993333333330033377F3333333377333993333333
        3333333773333333333F333333333333330033333333F33333773333333C3333
        330033333337FF3333773333333CC333333333FFFFF77FFF3FF33CCCCCCCCCC3
        993337777777777F77F33CCCCCCCCCC3993337777777777377333333333CC333
        333333333337733333FF3333333C333330003333333733333777333333333333
        3000333333333333377733333333333333333333333333333333}
      NumGlyphs = 2
      Opaque = False
      ParentShowHint = False
      ShowHint = True
      WordWrap = True
      OnClick = BAnagFileNewClick
    end
    object BAnagFileDel: TToolbarButton97
      Left = 1
      Top = 38
      Width = 91
      Height = 37
      Caption = 'Elimina associaz.'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333000000000
        3333333777777777F3333330F777777033333337F3F3F3F7F3333330F0808070
        33333337F7F7F7F7F3333330F080707033333337F7F7F7F7F3333330F0808070
        33333337F7F7F7F7F3333330F080707033333337F7F7F7F7F3333330F0808070
        333333F7F7F7F7F7F3F33030F080707030333737F7F7F7F7F7333300F0808070
        03333377F7F7F7F773333330F080707033333337F7F7F7F7F333333070707070
        33333337F7F7F7F7FF3333000000000003333377777777777F33330F88877777
        0333337FFFFFFFFF7F3333000000000003333377777777777333333330777033
        3333333337FFF7F3333333333000003333333333377777333333}
      NumGlyphs = 2
      Opaque = False
      WordWrap = True
      OnClick = BAnagFileDelClick
    end
  end
  object dxDBGrid1: TdxDBGrid
    Left = 0
    Top = 43
    Width = 339
    Height = 247
    Bands = <
      item
      end>
    DefaultLayout = True
    HeaderPanelRowCount = 1
    KeyField = 'ID'
    SummaryGroups = <>
    SummarySeparator = ', '
    Align = alClient
    TabOrder = 2
    DataSource = DsQCliSettori
    OptionsBehavior = [edgoAutoSort, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoTabThrough, edgoVertThrough]
    OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
    OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoIndicator, edgoRowSelect, edgoUseBitmap]
    object dxDBGrid1IDCliente: TdxDBGridMaskColumn
      Visible = False
      BandIndex = 0
      RowIndex = 0
      FieldName = 'IDCliente'
    end
    object dxDBGrid1Attivita: TdxDBGridMaskColumn
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Attivita'
    end
    object dxDBGrid1IDAttivita: TdxDBGridMaskColumn
      Visible = False
      BandIndex = 0
      RowIndex = 0
      FieldName = 'IDAttivita'
    end
    object dxDBGrid1ID: TdxDBGridMaskColumn
      Visible = False
      BandIndex = 0
      RowIndex = 0
      FieldName = 'ID'
    end
    object dxDBGrid1AreaSettore: TdxDBGridMaskColumn
      Caption = 'Area'
      BandIndex = 0
      RowIndex = 0
      FieldName = 'AreaSettore'
    end
  end
  object QCliSettori: TQuery
    Active = True
    DatabaseName = 'EBCDB'
    DataSource = Data2.DsEBCclienti
    SQL.Strings = (
      'select EBC_ClientiAttivita.*, EBC_Attivita.Attivita, AreaSettore'
      'from EBC_ClientiAttivita, EBC_Attivita, AreeSettori'
      'where EBC_ClientiAttivita.IDAttivita=EBC_Attivita.ID'
      '   and EBC_Attivita.IDAreaSettore *= AreeSettori.ID          '
      'and IDCliente=:ID')
    Left = 120
    Top = 96
    ParamData = <
      item
        DataType = ftAutoInc
        Name = 'ID'
        ParamType = ptUnknown
      end>
    object QCliSettoriIDCliente: TIntegerField
      FieldName = 'IDCliente'
      Origin = 'EBCDB.EBC_ClientiAttivita.IDCliente'
    end
    object QCliSettoriAttivita: TStringField
      FieldName = 'Attivita'
      Origin = 'EBCDB.EBC_Attivita.Attivita'
      FixedChar = True
      Size = 50
    end
    object QCliSettoriIDAttivita: TIntegerField
      FieldName = 'IDAttivita'
      Origin = 'EBCDB.EBC_ClientiAttivita.IDAttivita'
    end
    object QCliSettoriID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.EBC_ClientiAttivita.ID'
    end
    object QCliSettoriAreaSettore: TStringField
      FieldName = 'AreaSettore'
      FixedChar = True
      Size = 50
    end
  end
  object DsQCliSettori: TDataSource
    DataSet = QCliSettori
    Left = 120
    Top = 128
  end
end
