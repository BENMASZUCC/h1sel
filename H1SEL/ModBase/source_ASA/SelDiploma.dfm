�
 TSELDIPLOMAFORM 0�  TPF0TSelDiplomaFormSelDiplomaFormLeftgTop� ActiveControlDBGrid1BorderStylebsDialogCaptionSelezione titolo di studioClientHeight/ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrder	PositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TLabelLabel1LeftTopWidthcHeightCaptionTipo di titolo di studio  TLabelLabel2Left� Top*Width� HeightCaption,Titoli di studio della tipologia selezionata  TSpeedButtonBITALeftTopWidth%Height
GroupIndexDown	CaptionITAFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontVisibleOnClick	BITAClick  TSpeedButtonBENGLeft)TopWidth%Height
GroupIndexCaptionENGFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontVisibleOnClick	BENGClick  TDBGridDBGrid1Left� Top:WidthNHeight� 
DataSource	DsDiplomiReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameDescrizioneWidth,Visible	    TBitBtnBitBtn1Left1TopWidthVHeight"TabOrderKindbkOK  TDBGridDBGrid2LeftTop+Width� Height� 
DataSourceDsQTipiDiplomiReadOnly	TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameTipoWidthdVisible	    TBitBtnBitBtn2Left�TopWidthVHeight"CaptionAnnullaTabOrderKindbkCancel  TDataSource	DsDiplomiDataSetDiplomiLeft�TopX  TQueryQTipiDiplomiActive	DatabaseNameEBCDBSQL.Strings&select Tipo from Diplomi group by Tipo LeftTop�  TStringFieldQTipiDiplomiTipo	FieldNameTipoOrigin"Diplomi".Tipo   TDataSourceDsQTipiDiplomiDataSetQTipiDiplomiLeft(Top�   TQueryDiplomiActive	DatabaseNameEBCDB
DataSourceDsQTipiDiplomiSQL.Strings;select * from Diplomi where Tipo=:Tipo order by Descrizione Left�TopX	ParamDataDataTypeftStringNameTipo	ParamType	ptUnknown   TAutoIncField	DiplomiID	FieldNameIDOriginEBCDB.Diplomi.ID  TIntegerFieldDiplomiIDArea	FieldNameIDAreaOriginEBCDB.Diplomi.IDArea  TStringFieldDiplomiTipo	FieldNameTipoOriginEBCDB.Diplomi.Tipo	FixedChar	  TStringFieldDiplomiDescrizione	FieldNameDescrizioneOriginEBCDB.Diplomi.Descrizione	FixedChar	Size<  TSmallintFieldDiplomiPunteggioMax	FieldNamePunteggioMaxOriginEBCDB.Diplomi.PunteggioMax  TStringFieldDiplomiIDAzienda	FieldName	IDAziendaOriginEBCDB.Diplomi.IDAzienda	FixedChar	Size
  TStringFieldDiplomiTipo_ENG	FieldNameTipo_ENGOriginEBCDB.Diplomi.Tipo_ENG	FixedChar	  TStringFieldDiplomiDescrizione_ENG	FieldNameDescrizione_ENGOriginEBCDB.Diplomi.Descrizione_ENG	FixedChar	Size<    