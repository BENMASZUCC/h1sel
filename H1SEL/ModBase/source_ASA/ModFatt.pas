unit ModFatt;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Spin, Mask, ToolEdit, CurrEdit, StdCtrls, Buttons;

type
  TModFattForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    EDescrizione: TEdit;
    Label1: TLabel;
    RxImponibile: TRxCalcEdit;
    Imponibile: TLabel;
    Label2: TLabel;
    SEPercIVA: TSpinEdit;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ModFattForm: TModFattForm;

implementation

{$R *.DFM}

procedure TModFattForm.FormShow(Sender: TObject);
begin
     Caption:='[S/28] - '+Caption;
end;

end.
