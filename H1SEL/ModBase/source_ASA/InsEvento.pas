unit InsEvento;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Grids, DBGrids, Db, DBTables, DtEdit97, Buttons, ExtCtrls;

type
  TInsEventoForm = class(TForm)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    DEData: TDateEdit97;
    Label1: TLabel;
    Label2: TLabel;
    TEventi: TTable;
    DsEventi: TDataSource;
    TEventiID: TAutoIncField;
    TEventiIDdaStato: TIntegerField;
    TEventiEvento: TStringField;
    TEventiIDaStato: TIntegerField;
    TEventiNonCancellabile: TBooleanField;
    TEventiIDProcedura: TIntegerField;
    DBGrid1: TDBGrid;
    TStatiLK: TTable;
    TStatiLKID: TAutoIncField;
    TStatiLKStato: TStringField;
    TEventiDaStato: TStringField;
    TEventiAStato: TStringField;
    Panel19: TPanel;
    EDesc: TEdit;
    DBGrid2: TDBGrid;
    Panel2: TPanel;
    DsUsers: TDataSource;
    QUsers: TQuery;
    QUsersID: TIntegerField;
    QUsersNominativo: TStringField;
    QUsersDescrizione: TStringField;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    xIDUtente:integer;
  end;

var
  InsEventoForm: TInsEventoForm;

implementation

{$R *.DFM}

procedure TInsEventoForm.FormShow(Sender: TObject);
begin
     Caption:='[S/26] - '+Caption;
     QUsers.ParamByName('xOggi').asDateTime:=Date;
     QUsers.Open;
     if xIDUtente<>0 then
        QUsers.Locate('ID',xIDUtente,[]);
end;

procedure TInsEventoForm.FormCreate(Sender: TObject);
begin
     xIDUtente:=0;
end;

end.
