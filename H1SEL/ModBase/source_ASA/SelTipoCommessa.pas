unit SelTipoCommessa;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Grids, DBGrids, Db, DBTables;

type
  TSelTipoCommessaForm = class(TForm)
    QTipo: TQuery;
    DsQTipo: TDataSource;
    QTipoID: TAutoIncField;
    QTipoTipoCommessa: TStringField;
    QTipoColore: TStringField;
    DBGrid1: TDBGrid;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SelTipoCommessaForm: TSelTipoCommessaForm;

implementation

{$R *.DFM}

procedure TSelTipoCommessaForm.FormShow(Sender: TObject);
begin
     Caption:='[M/211] - '+caption;
end;

end.
