object NuovoSoggettoForm: TNuovoSoggettoForm
  Left = 233
  Top = 108
  ActiveControl = ECogn
  BorderStyle = bsDialog
  Caption = 'Nuovo soggetto per l'#39'azienda:  dati salienti'
  ClientHeight = 132
  ClientWidth = 344
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 6
    Top = 10
    Width = 45
    Height = 13
    Caption = 'Cognome'
  end
  object Label2: TLabel
    Left = 23
    Top = 34
    Width = 28
    Height = 13
    Caption = 'Nome'
  end
  object Label3: TLabel
    Left = 19
    Top = 58
    Width = 32
    Height = 13
    Caption = 'Sesso:'
  end
  object Label4: TLabel
    Left = 22
    Top = 80
    Width = 26
    Height = 13
    Caption = 'Note:'
  end
  object Label5: TLabel
    Left = 5
    Top = 109
    Width = 75
    Height = 13
    Caption = 'Ruolo ricoperto:'
  end
  object SpeedButton1: TSpeedButton
    Left = 313
    Top = 104
    Width = 28
    Height = 24
    Hint = 'seleziona ruolo'
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000120B0000120B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      33333333333333333333333333333333333333333333333333FF333333333333
      3000333333FFFFF3F77733333000003000B033333777773777F733330BFBFB00
      E00033337FFF3377F7773333000FBFB0E000333377733337F7773330FBFBFBF0
      E00033F7FFFF3337F7773000000FBFB0E000377777733337F7770BFBFBFBFBF0
      E00073FFFFFFFF37F777300000000FB0E000377777777337F7773333330BFB00
      000033333373FF77777733333330003333333333333777333333333333333333
      3333333333333333333333333333333333333333333333333333333333333333
      3333333333333333333333333333333333333333333333333333}
    NumGlyphs = 2
    ParentShowHint = False
    ShowHint = True
    OnClick = SpeedButton1Click
  end
  object BitBtn1: TBitBtn
    Left = 251
    Top = 3
    Width = 91
    Height = 34
    TabOrder = 4
    OnClick = BitBtn1Click
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 251
    Top = 38
    Width = 91
    Height = 34
    Caption = 'Annulla'
    TabOrder = 5
    Kind = bkCancel
  end
  object ECogn: TEdit
    Left = 60
    Top = 6
    Width = 185
    Height = 21
    CharCase = ecUpperCase
    Color = clYellow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    MaxLength = 30
    ParentFont = False
    TabOrder = 0
  end
  object ENome: TEdit
    Left = 60
    Top = 30
    Width = 185
    Height = 21
    CharCase = ecUpperCase
    Color = clYellow
    MaxLength = 30
    TabOrder = 1
  end
  object CBSesso: TComboBox
    Left = 60
    Top = 54
    Width = 37
    Height = 21
    ItemHeight = 13
    TabOrder = 2
    Text = 'M'
    Items.Strings = (
      'M'
      'F')
  end
  object ENote: TEdit
    Left = 59
    Top = 77
    Width = 282
    Height = 21
    TabOrder = 3
  end
  object ERuolo: TEdit
    Left = 85
    Top = 105
    Width = 224
    Height = 21
    TabStop = False
    ReadOnly = True
    TabOrder = 6
  end
end
