object ASAcommessaForm: TASAcommessaForm
  Left = 303
  Top = 149
  Width = 895
  Height = 595
  Caption = 'Scheda ricerca'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 137
    Width = 887
    Height = 3
    Cursor = crVSplit
    Align = alTop
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 887
    Height = 48
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object BCustomize: TToolbarButton97
      Left = 647
      Top = 1
      Width = 89
      Height = 46
      Caption = 'Personalizza Layout'
      Glyph.Data = {
        36050000424D3605000000000000360400002800000010000000100000000100
        08000000000000010000120B0000120B00000001000000010000392118002129
        2100292929004A31290018392900182931002931310031313100313931007342
        3100102139001029390031393900393939002942390039423900734A39007B4A
        39001029420039394200424242007B4A42008C4A42009C524200A55A4200AD5A
        420029394A0029424A004A4A4A0039524A0063524A00A55A4A00AD5A4A001863
        4A00214A520052525200AD5A5200B5635200CE6B520010735200187352005A73
        520018315A0021395A0084525A0063635A00946B5A009C6B5A00187B5A00088C
        5A003142630063426300295A6300315A63005A63630063636300526B6300E77B
        6300EF7B630029846300F7846300FF846300108C630010946300314A6B00394A
        6B0094636B006B6B6B0018946B0031946B00089C6B0008A56B00183973007352
        7300186B7300426B730073737300737B73003194730008A5730008AD73001842
        7B0021427B00294A7B0029737B005A737B0073737B005A7B7B007B7B7B0008A5
        7B0008B57B000839840010428400294A8400217B84007B848400848484008C84
        840008AD840008C6840010428C008C8C8C0008A58C0008D68C00104A9400184A
        9400298494008C949400949494009C9494008C9C940008AD940008DE940008E7
        9400104A9C00108C9C00188C9C00089C9C00949C9C009C9C9C0008F79C00104A
        A500089CA500A5A5A50008ADA5001052AD00089CAD0008A5AD00A5ADAD00ADAD
        AD000894B50008A5B50021A5B50008ADB500ADADB500B5B5B50008ADBD00BDBD
        BD00105ACE0010B5CE0008BDD600D6D6D6001063DE000884DE0008CEDE00DEDE
        DE001063E70008CEE70010CEE700E7E7E700086BEF00106BEF00087BEF00EFEF
        EF00106BF7001073F70008E7F700F7F7F7001073FF0010EFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00A0A0917B3737
        58819595773737588795A095581018110D438156517948134C89A061173C263A
        2014367D9B8A97681477A02F3909231E3D031A9E2A23408E3277A02F3A151402
        1900129B0A072B924189A06D1F3D18162C3352965B5C9A726B95A09D872E2524
        42495D98646953588D9DA0A0916C2D082901128F0B061C588195A0914C28505A
        626F7C8588834A0C4389A05F3E78504F5966757F7A7E9C741477A0457027140F
        31041B82051C54933577A04E70211C0E711D559F341C22904B89A06E44673F63
        465F868494738C746595A0A0873B47304D91A0806A8B5E6095A0A0A0A0803865
        95A0A0A076577699A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0}
      Visible = False
      WordWrap = True
    end
    object BNuovo: TToolbarButton97
      Left = 2
      Top = 1
      Width = 75
      Height = 46
      Caption = 'Nuova ricerca'
      Glyph.Data = {
        36050000424D3605000000000000360400002800000010000000100000000100
        08000000000000010000120B0000120B00000001000000010000006B00000073
        0000007B000000940000009C0000086B0800087B08000884080008A5080008AD
        080008B5080010731000107B10001084100010A5100010AD100010B510001873
        1800187B180018841800188C1800189C180018A5180018AD180018B51800217B
        2100218C210021942100219C210021AD210021B5210029942900299C29003194
        3100319C3100399C390039A5390039AD3900429C420042A5420042AD42004AA5
        4A004AAD4A0052A5520052AD52005AAD5A0063AD630063B563006BB56B0073BD
        73007BB57B007BBD7B0084B5840084C684008CBD8C008CC68C009CCE9C00A5D6
        A500B5DEB500FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF003B3B3B3B3B3B
        3B3B3B3B3B3B3B3B3B3B3B34050B1111120C0C0606020201323B3B0D141F1F1F
        1F201C160E080403003B3B141F22232324393B1D18100904023B3B1B23272927
        273B3B3B1E180A08023B3B1F2729292927283B3B3B180F08073B3B22292C2C2A
        2928253B3B3B17160D3B3B232C3B3B3B3B3B3B3B3B3B3B15133B3B272D3B3B3B
        3B3B3B3B3B3B3B1C123B3B292F2E2D2B2923223B3B3B1C1F193B3B2C30302D2C
        29263B3B3B1F201F193B3B2D33312F2D2B3B3B3B2222221F193B3B303735302F
        2E3A3B2929272321123B3B3138373331302F2F2D2C29271F113B3B3A312F2D2C
        292929272423221A363B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B3B}
      WordWrap = True
      OnClick = BNuovoClick
    end
    object BEliminaRic: TToolbarButton97
      Left = 77
      Top = 1
      Width = 75
      Height = 46
      Caption = 'Elimina ricerca'
      Glyph.Data = {
        36050000424D3605000000000000360400002800000010000000100000000100
        08000000000000010000120B0000120B000000010000000100000021B5000831
        B5001839B5000029BD001031BD001039BD001839BD002142BD000029C6000831
        C6001039C6001839C6000031D6001039D6007B94D6001842DE002142DE00184A
        DE00214ADE00294ADE002952DE003152DE008494DE008C9CDE000039E7000839
        E7000842E7001042E700184AE700214AE7002152E7002952E7003152E700295A
        E700315AE700395AE7003963E7004263E7004A63E700426BE7004A6BE700526B
        E7005273E7000842EF00084AEF00104AEF002152EF00295AEF003163EF005A73
        EF005A7BEF00637BEF006384EF006B84EF007384EF006B8CEF00738CEF007B94
        EF008494EF00849CEF008C9CEF008CA5EF006B8CF7007394F70094ADF7009CAD
        F7009CB5F700A5B5F700B5BDF700B5C6F700BDC6F700BDCEF700B5C6FF00BDCE
        FF00DEE7FF00E7E7FF00E7EFFF00EFEFFF00EFF7FF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF004F4F4F4F4F4F
        4F4F4F4F4F4F4F4F4F4F4F160104060206050409090308000E4F4F0D0F131414
        1414121C1B1A180C004F4F0F1F2225394C4F4F4A3F2D2B18034F4F122325434F
        473837494F422C19084F4F1F253C4F3B2725302F3F4F3E1A094F4F22284D4628
        284F4F2F2E484A1C0A4F4F23294F3A29284F4F211E344F110B4F4F25314F3A29
        264F4F1F1E344F12064F4F28334E4529264F4F1F1D444B14074F4F2935404F3A
        26252320354F3814074F4F323938434F443635444F402014074F4F353D3A3740
        4E4F4F4D3C252315064F4F38413D3938353333322A282514064F4F443834312A
        2828282524232010174F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F}
      WordWrap = True
    end
    object BSchedaCand: TToolbarButton97
      Left = 288
      Top = 1
      Width = 81
      Height = 46
      Caption = 'Scheda candidato'
      WordWrap = True
      OnClick = BSchedaCandClick
    end
    object BOK: TToolbarButton97
      Left = 11
      Top = 1
      Width = 75
      Height = 46
      Caption = 'Conferma modifiche'
      Glyph.Data = {
        36050000424D3605000000000000360400002800000010000000100000000100
        08000000000000010000120B0000120B000000010000000100005A2910008442
        18009C4A1800393121006B3921006B422100A5422100523929008C4A29009C52
        2900B55229005A4A3100634A31007B4A3100844A3100D66331006B6B3100B563
        39007B5A4200845A42005A6342008C6342008C6B4200D6734200DE7B42005A63
        4A009C634A00AD6B4A008C734A009C734A00B5734A00B57B4A00C6844A008494
        4A008C6B5200AD7B5200A5845200BD845200EF945200847B5A0084845A005273
        63008C8463009C846300A58C630094BD63007B736B00848C6B006B9C6B00E7AD
        6B0073847300B59C730084BD730063947B007B9C7B00B59C7B00B5A57B00C6B5
        7B00B5C67B00ADA58400BDAD84008C8C8C009CA58C0084AD8C00BDB58C00B5BD
        9400ADBD9C00C6C69C00ADC6A500A5ADAD0084B5AD00A5C6AD00D6DEAD00CEEF
        AD00C6F7B5009CD6BD00CEDEC6009CDECE00A5EFCE00C6F7D600FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00501916011432
        2A2216120E0100072201504C390F304848413837231B0812310950504418244A
        4C433C37231A0D1B26125050474823444F4740372C15052606505050504A483A
        4F4A41372B12152507505050504C4A3A4A4C423B280D1C0C5050505050504F34
        444F473E2705145050505050505050503E4E4B3F143D50505050505050505050
        503035293D50505050505050505050500B04010602505050505050505050502F
        101D1E170F02505050505050505050213A3920170F0A5050505050505050502D
        4948331F11085050505050505050502D4A4F422A130050505050505050505050
        344D4632032E5050505050505050505050363529455050505050}
      Visible = False
      WordWrap = True
      OnClick = BOKClick
    end
    object BCan: TToolbarButton97
      Left = 96
      Top = 1
      Width = 75
      Height = 46
      Caption = 'Annulla modifiche'
      Glyph.Data = {
        36050000424D3605000000000000360400002800000010000000100000000100
        08000000000000010000120B0000120B00000001000000010000181839002121
        520021215A002929630029296B0031316B003131730031317B0039397B003939
        84004242840039398C004A4A8C004A528C004242940042429C004A4A9C006363
        9C004242A5004A4AA5004A4AAD004A4AB5005252B5005252BD005A5ABD006363
        C6006B6BC6007373C600A5A5C6007B7BCE008484CE00B5B5CE008C8CD6009494
        D6009C9CD600BDC6D6009C9CDE00A5A5DE00ADADDE00B5B5E700BDBDE700C6C6
        E700CECEEF00D6D6EF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF002C0E0E060814
        140F0E0B090503030F072C1C1D16172627201D1A18150B0E1E0E2C2C26191227
        29221D19171309151B0B2C2C252514262B261E1A1712071D0E2C2C2C2C292520
        2B27201A160B1317022C2C2C2C1F2A212A28211A140912062C2C2C2C2C2C2320
        2728211A1304072C2C2C2C2C2C2C2C2C2027211A060C2C2C2C2C2C2C2C2C2C2C
        2C16170E0D2C2C2C2C2C2C2C2C2C2C2C0303090E0B2C2C2C2C2C2C2C2C2C2C10
        0614151717092C2C2C2C2C2C2C2C2C0F1B1B1819190F2C2C2C2C2C2C2C2C2C19
        28261D17140B2C2C2C2C2C2C2C2C2C18292A20150E012C2C2C2C2C2C2C2C2C2C
        1A241B14000A2C2C2C2C2C2C2C2C2C2C2C1A1409112C2C2C2C2C}
      Visible = False
      WordWrap = True
      OnClick = BCanClick
    end
    object BInsCandElenco: TToolbarButton97
      Left = 369
      Top = 1
      Width = 103
      Height = 46
      Caption = 'Aggiungi cand. da elenco'
      WordWrap = True
      OnClick = BInsCandElencoClick
    end
    object BEliminaCand: TToolbarButton97
      Left = 472
      Top = 1
      Width = 81
      Height = 46
      Caption = 'Elimina candidato'
      WordWrap = True
      OnClick = BEliminaCandClick
    end
    object ToolbarButton971: TToolbarButton97
      Left = 553
      Top = 1
      Width = 81
      Height = 46
      Caption = 'Modifica note candidato'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      WordWrap = True
      OnClick = ToolbarButton971Click
    end
    object dxLayoutControl1: TdxLayoutControl
      Left = 750
      Top = 1
      Width = 136
      Height = 46
      Align = alRight
      TabOrder = 0
      LookAndFeel = dxLayoutStandardLookAndFeel1
      object dxDBLookupEdit1: TdxDBLookupEdit
        Left = 4
        Top = 19
        Width = 121
        Style.BorderColor = clWindowFrame
        Style.BorderStyle = xbsFlat
        Style.ButtonStyle = btsFlat
        TabOrder = 0
        DataField = 'StatoRic'
        DataSource = DsQRicerca
        PopupBorder = pbFlat
        LookupKeyValue = Null
      end
      object dxLayoutControl1Group_Root: TdxLayoutGroup
        ShowCaption = False
        Hidden = True
        ShowBorder = False
        object dxLayoutControl1Item1: TdxLayoutItem
          Caption = 'Stato ricerca:'
          CaptionOptions.Layout = clTop
          Control = dxDBLookupEdit1
          ControlOptions.ShowBorder = False
        end
      end
    end
  end
  object PanDatiRicerca: TPanel
    Left = 0
    Top = 48
    Width = 887
    Height = 89
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 1
    object dxLayoutControl2: TdxLayoutControl
      Left = 1
      Top = 1
      Width = 885
      Height = 87
      Align = alClient
      TabOrder = 0
      LookAndFeel = dxLayoutStandardLookAndFeel1
      object dxDBEdit1: TdxDBEdit
        Left = 172
        Top = 19
        Width = 197
        Style.BorderColor = clWindowFrame
        Style.BorderStyle = xbsFlat
        Style.ButtonStyle = btsFlat
        TabOrder = 1
        DataField = 'TitoloCliente'
        DataSource = DsQRicerca
      end
      object dxDBLookupEdit2: TdxDBLookupEdit
        Left = 4
        Top = 19
        Width = 165
        Style.BorderColor = clWindowFrame
        Style.BorderStyle = xbsFlat
        Style.ButtonStyle = btsFlat
        TabOrder = 0
        DataField = 'Cliente'
        DataSource = DsQRicerca
        PopupBorder = pbFlat
        LookupKeyValue = Null
      end
      object dxDBLookupEdit3: TdxDBLookupEdit
        Left = 564
        Top = 19
        Width = 173
        Style.BorderColor = clWindowFrame
        Style.BorderStyle = xbsFlat
        Style.ButtonStyle = btsFlat
        TabOrder = 3
        DataField = 'Ruolo'
        DataSource = DsQRicerca
        PopupBorder = pbFlat
        LookupKeyValue = Null
      end
      object dxDBEdit2: TdxDBEdit
        Left = 374
        Top = 19
        Width = 187
        Style.BorderColor = clWindowFrame
        Style.BorderStyle = xbsFlat
        Style.ButtonStyle = btsFlat
        TabOrder = 2
        DataField = 'Settore'
        DataSource = DsQRicerca
      end
      object dxDBDateEdit1: TdxDBDateEdit
        Left = 4
        Top = 58
        Width = 85
        Style.BorderColor = clWindowFrame
        Style.BorderStyle = xbsFlat
        Style.ButtonStyle = btsFlat
        TabOrder = 4
        DataField = 'DataInizio'
        DataSource = DsQRicerca
        PopupBorder = pbFlat
      end
      object dxDBDateEdit2: TdxDBDateEdit
        Left = 92
        Top = 58
        Width = 85
        Style.BorderColor = clWindowFrame
        Style.BorderStyle = xbsFlat
        Style.ButtonStyle = btsFlat
        TabOrder = 5
        DataField = 'DataFine'
        DataSource = DsQRicerca
        PopupBorder = pbFlat
      end
      object dxDBLookupEdit4: TdxDBLookupEdit
        Left = 180
        Top = 58
        Width = 121
        Style.BorderColor = clWindowFrame
        Style.BorderStyle = xbsFlat
        Style.ButtonStyle = btsFlat
        TabOrder = 6
        DataField = 'Utente'
        DataSource = DsQRicerca
        PopupBorder = pbFlat
        LookupKeyValue = Null
      end
      object dxLayoutControl2Group_Root: TdxLayoutGroup
        ShowCaption = False
        Hidden = True
        ShowBorder = False
        object dxLayoutControl2Group1: TdxLayoutGroup
          ShowCaption = False
          Hidden = True
          LayoutDirection = ldHorizontal
          ShowBorder = False
          object dxLayoutControl2Item2: TdxLayoutItem
            Caption = 'Azienda'
            CaptionOptions.Layout = clTop
            Control = dxDBLookupEdit2
            ControlOptions.ShowBorder = False
          end
          object dxLayoutControl2Item1: TdxLayoutItem
            Caption = 'Ruolo richiesto (titolo attribuito dal cliente):'
            CaptionOptions.Layout = clTop
            Control = dxDBEdit1
            ControlOptions.ShowBorder = False
          end
          object dxLayoutControl2Item4: TdxLayoutItem
            Caption = 'Industry (settore)'
            CaptionOptions.Layout = clTop
            Control = dxDBEdit2
            ControlOptions.ShowBorder = False
          end
          object dxLayoutControl2Item3: TdxLayoutItem
            Caption = 'Function (Ruolo)'
            CaptionOptions.Layout = clTop
            Control = dxDBLookupEdit3
            ControlOptions.ShowBorder = False
          end
        end
        object dxLayoutControl2Group2: TdxLayoutGroup
          ShowCaption = False
          Hidden = True
          LayoutDirection = ldHorizontal
          ShowBorder = False
          object dxLayoutControl2Item5: TdxLayoutItem
            AutoAligns = [aaVertical]
            Caption = 'Data inizio'
            CaptionOptions.Layout = clTop
            Control = dxDBDateEdit1
            ControlOptions.ShowBorder = False
          end
          object dxLayoutControl2Item6: TdxLayoutItem
            Caption = 'Data chiusura'
            CaptionOptions.Layout = clTop
            Control = dxDBDateEdit2
            ControlOptions.ShowBorder = False
          end
          object dxLayoutControl2Item7: TdxLayoutItem
            Caption = 'Consulente/partner'
            CaptionOptions.Layout = clTop
            Control = dxDBLookupEdit4
            ControlOptions.ShowBorder = False
          end
        end
      end
    end
  end
  object PanCandidati: TPanel
    Left = 0
    Top = 140
    Width = 887
    Height = 428
    Align = alClient
    BevelOuter = bvLowered
    TabOrder = 2
    object dxDBGrid1: TdxDBGrid
      Left = 1
      Top = 1
      Width = 885
      Height = 426
      Bands = <
        item
        end>
      DefaultLayout = True
      HeaderPanelRowCount = 1
      KeyField = 'IDCandRic'
      SummaryGroups = <>
      SummarySeparator = ', '
      Align = alClient
      TabOrder = 0
      DataSource = DsQCandRic
      Filter.Active = True
      Filter.Criteria = {00000000}
      OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
      OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoUseBitmap]
      object dxDBGrid1Titolo: TdxDBGridMaskColumn
        DisableEditor = True
        Width = 35
        BandIndex = 0
        RowIndex = 0
        FieldName = 'Titolo'
        DisableFilter = True
      end
      object dxDBGrid1Cognome: TdxDBGridMaskColumn
        DisableEditor = True
        Sorted = csUp
        Width = 122
        BandIndex = 0
        RowIndex = 0
        FieldName = 'Cognome'
        DisableFilter = True
      end
      object dxDBGrid1Nome: TdxDBGridMaskColumn
        DisableEditor = True
        Width = 127
        BandIndex = 0
        RowIndex = 0
        FieldName = 'Nome'
        DisableFilter = True
      end
      object dxDBGrid1Column9: TdxDBGridColumn
        Caption = 'Et�'
        DisableEditor = True
        Width = 31
        BandIndex = 0
        RowIndex = 0
        FieldName = 'Eta'
        DisableFilter = True
      end
      object dxDBGrid1DataNascita: TdxDBGridDateColumn
        Caption = 'Data di nascita'
        DisableEditor = True
        Visible = False
        Width = 91
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DataNascita'
        DisableFilter = True
      end
      object dxDBGrid1Azienda: TdxDBGridLookupColumn
        DisableEditor = True
        Width = 129
        BandIndex = 0
        RowIndex = 0
        FieldName = 'Azienda'
        DisableFilter = True
      end
      object dxDBGrid1TitoloMansione: TdxDBGridLookupColumn
        Caption = 'Posizione'
        DisableEditor = True
        Width = 141
        BandIndex = 0
        RowIndex = 0
        FieldName = 'TitoloMansione'
      end
      object dxDBGrid1Stato: TdxDBGridMaskColumn
        Caption = 'Status'
        DisableEditor = True
        Width = 72
        BandIndex = 0
        RowIndex = 0
        FieldName = 'Stato'
      end
      object dxDBGrid1Note2: TdxDBGridMaskColumn
        Caption = 'Note'
        Width = 164
        BandIndex = 0
        RowIndex = 0
        FieldName = 'Note2'
        DisableFilter = True
      end
      object dxDBGrid1Escluso: TdxDBGridCheckColumn
        DisableEditor = True
        Width = 60
        BandIndex = 0
        RowIndex = 0
        FieldName = 'Escluso'
        ValueChecked = 'True'
        ValueUnchecked = 'False'
      end
    end
  end
  object dxLayoutLookAndFeelList1: TdxLayoutLookAndFeelList
    Left = 568
    Top = 8
    object dxLayoutStandardLookAndFeel1: TdxLayoutStandardLookAndFeel
      ItemOptions.ControlBorderStyle = lbsFlat
      Offsets.ControlOffsetVert = 1
      Offsets.ItemOffset = 2
      Offsets.RootItemsAreaOffsetHorz = 3
      Offsets.RootItemsAreaOffsetVert = 3
    end
    object dxLayoutOfficeLookAndFeel1: TdxLayoutOfficeLookAndFeel
    end
    object dxLayoutWebLookAndFeel1: TdxLayoutWebLookAndFeel
    end
  end
  object QRicerca: TQuery
    CachedUpdates = True
    AfterOpen = QRicercaAfterOpen
    AfterPost = QRicercaAfterPost
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select * from EBC_Ricerche'
      'where ID=:xIDRic')
    UpdateObject = UpdRicerca
    Left = 192
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'xIDRic'
        ParamType = ptUnknown
      end>
    object QRicercaID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.EBC_Ricerche.ID'
    end
    object QRicercaIDCliente: TIntegerField
      FieldName = 'IDCliente'
      Origin = 'EBCDB.EBC_Ricerche.IDCliente'
    end
    object QRicercaIDUtente: TIntegerField
      FieldName = 'IDUtente'
      Origin = 'EBCDB.EBC_Ricerche.IDUtente'
    end
    object QRicercaProgressivo: TStringField
      FieldName = 'Progressivo'
      Origin = 'EBCDB.EBC_Ricerche.Progressivo'
      FixedChar = True
      Size = 15
    end
    object QRicercaDataInizio: TDateTimeField
      FieldName = 'DataInizio'
      Origin = 'EBCDB.EBC_Ricerche.DataInizio'
    end
    object QRicercaDataFine: TDateTimeField
      FieldName = 'DataFine'
      Origin = 'EBCDB.EBC_Ricerche.DataFine'
    end
    object QRicercaStato: TStringField
      FieldName = 'Stato'
      Origin = 'EBCDB.EBC_Ricerche.Stato'
      FixedChar = True
      Size = 30
    end
    object QRicercaConclusa: TBooleanField
      FieldName = 'Conclusa'
      Origin = 'EBCDB.EBC_Ricerche.Conclusa'
    end
    object QRicercaIDStatoRic: TIntegerField
      FieldName = 'IDStatoRic'
      Origin = 'EBCDB.EBC_Ricerche.IDStatoRic'
    end
    object QRicercaDallaData: TDateTimeField
      FieldName = 'DallaData'
      Origin = 'EBCDB.EBC_Ricerche.DallaData'
    end
    object QRicercaIDArea: TIntegerField
      FieldName = 'IDArea'
      Origin = 'EBCDB.EBC_Ricerche.IDArea'
    end
    object QRicercaIDMansione: TIntegerField
      FieldName = 'IDMansione'
      Origin = 'EBCDB.EBC_Ricerche.IDMansione'
    end
    object QRicercaIDSpec: TIntegerField
      FieldName = 'IDSpec'
      Origin = 'EBCDB.EBC_Ricerche.IDSpec'
    end
    object QRicercaNumRicercati: TSmallintField
      FieldName = 'NumRicercati'
      Origin = 'EBCDB.EBC_Ricerche.NumRicercati'
    end
    object QRicercaNote: TMemoField
      FieldName = 'Note'
      Origin = 'EBCDB.EBC_Ricerche.Note'
      BlobType = ftMemo
    end
    object QRicercaIDFattura: TIntegerField
      FieldName = 'IDFattura'
      Origin = 'EBCDB.EBC_Ricerche.IDFattura'
    end
    object QRicercaSospesa: TBooleanField
      FieldName = 'Sospesa'
      Origin = 'EBCDB.EBC_Ricerche.Sospesa'
    end
    object QRicercaSospesaDal: TDateTimeField
      FieldName = 'SospesaDal'
      Origin = 'EBCDB.EBC_Ricerche.SospesaDal'
    end
    object QRicercaStipendioNetto: TFloatField
      FieldName = 'StipendioNetto'
      Origin = 'EBCDB.EBC_Ricerche.StipendioNetto'
    end
    object QRicercaStipendioLordo: TFloatField
      FieldName = 'StipendioLordo'
      Origin = 'EBCDB.EBC_Ricerche.StipendioLordo'
    end
    object QRicercaFatturata: TBooleanField
      FieldName = 'Fatturata'
      Origin = 'EBCDB.EBC_Ricerche.Fatturata'
    end
    object QRicercaSpecifiche: TMemoField
      FieldName = 'Specifiche'
      Origin = 'EBCDB.EBC_Ricerche.Specifiche'
      BlobType = ftMemo
    end
    object QRicercaSpecifEst: TMemoField
      FieldName = 'SpecifEst'
      Origin = 'EBCDB.EBC_Ricerche.SpecifEst'
      BlobType = ftMemo
    end
    object QRicercaTipo: TStringField
      FieldName = 'Tipo'
      Origin = 'EBCDB.EBC_Ricerche.Tipo'
      FixedChar = True
      Size = 30
    end
    object QRicercaIDSede: TIntegerField
      FieldName = 'IDSede'
      Origin = 'EBCDB.EBC_Ricerche.IDSede'
    end
    object QRicercaIDContattoCli: TIntegerField
      FieldName = 'IDContattoCli'
      Origin = 'EBCDB.EBC_Ricerche.IDContattoCli'
    end
    object QRicercaIDUtente2: TIntegerField
      FieldName = 'IDUtente2'
      Origin = 'EBCDB.EBC_Ricerche.IDUtente2'
    end
    object QRicercaIDUtente3: TIntegerField
      FieldName = 'IDUtente3'
      Origin = 'EBCDB.EBC_Ricerche.IDUtente3'
    end
    object QRicercaDataPrevChiusura: TDateTimeField
      FieldName = 'DataPrevChiusura'
      Origin = 'EBCDB.EBC_Ricerche.DataPrevChiusura'
    end
    object QRicercaggDiffApRic: TSmallintField
      FieldName = 'ggDiffApRic'
      Origin = 'EBCDB.EBC_Ricerche.ggDiffApRic'
    end
    object QRicercaggDiffUcRic: TSmallintField
      FieldName = 'ggDiffUcRic'
      Origin = 'EBCDB.EBC_Ricerche.ggDiffUcRic'
    end
    object QRicercaggDiffColRic: TSmallintField
      FieldName = 'ggDiffColRic'
      Origin = 'EBCDB.EBC_Ricerche.ggDiffColRic'
    end
    object QRicercaIDOfferta: TIntegerField
      FieldName = 'IDOfferta'
      Origin = 'EBCDB.EBC_Ricerche.IDOfferta'
    end
    object QRicercaProvvigione: TBooleanField
      FieldName = 'Provvigione'
      Origin = 'EBCDB.EBC_Ricerche.Provvigione'
    end
    object QRicercaProvvigionePerc: TIntegerField
      FieldName = 'ProvvigionePerc'
      Origin = 'EBCDB.EBC_Ricerche.ProvvigionePerc'
    end
    object QRicercaQuandoPagamUtente: TStringField
      FieldName = 'QuandoPagamUtente'
      Origin = 'EBCDB.EBC_Ricerche.QuandoPagamUtente'
      FixedChar = True
      Size = 50
    end
    object QRicercaOreLavUtente1: TIntegerField
      FieldName = 'OreLavUtente1'
      Origin = 'EBCDB.EBC_Ricerche.OreLavUtente1'
    end
    object QRicercaOreLavUtente2: TIntegerField
      FieldName = 'OreLavUtente2'
      Origin = 'EBCDB.EBC_Ricerche.OreLavUtente2'
    end
    object QRicercaOreLavUtente3: TIntegerField
      FieldName = 'OreLavUtente3'
      Origin = 'EBCDB.EBC_Ricerche.OreLavUtente3'
    end
    object QRicercaTitoloCliente: TStringField
      FieldName = 'TitoloCliente'
      Origin = 'EBCDB.EBC_Ricerche.TitoloCliente'
      FixedChar = True
      Size = 50
    end
    object QRicercaIDContrattoCG: TIntegerField
      FieldName = 'IDContrattoCG'
      Origin = 'EBCDB.EBC_Ricerche.IDContrattoCG'
    end
    object QRicercaIdAzienda: TIntegerField
      FieldName = 'IdAzienda'
      Origin = 'EBCDB.EBC_Ricerche.IdAzienda'
    end
    object QRicercaLKRuolo: TStringField
      FieldName = 'LKRuolo'
      Origin = 'EBCDB.EBC_Ricerche.LKRuolo'
      FixedChar = True
      Size = 50
    end
    object QRicercaLkArea: TStringField
      FieldName = 'LkArea'
      Origin = 'EBCDB.EBC_Ricerche.LkArea'
      FixedChar = True
      Size = 50
    end
    object QRicercaLkStatoRic: TStringField
      FieldName = 'LkStatoRic'
      Origin = 'EBCDB.EBC_Ricerche.LkStatoRic'
      FixedChar = True
      Size = 50
    end
    object QRicercaIDCausale: TIntegerField
      FieldName = 'IDCausale'
      Origin = 'EBCDB.EBC_Ricerche.IDCausale'
    end
    object QRicercaStatoRic: TStringField
      FieldKind = fkLookup
      FieldName = 'StatoRic'
      LookupDataSet = QStatiRicLK
      LookupKeyFields = 'ID'
      LookupResultField = 'StatoRic'
      KeyFields = 'IDStatoRic'
      Lookup = True
    end
    object QRicercaCliente: TStringField
      FieldKind = fkLookup
      FieldName = 'Cliente'
      LookupDataSet = QClientiLK
      LookupKeyFields = 'ID'
      LookupResultField = 'Descrizione'
      KeyFields = 'IDCliente'
      Size = 50
      Lookup = True
    end
    object QRicercaRuolo: TStringField
      FieldKind = fkLookup
      FieldName = 'Ruolo'
      LookupDataSet = QRuoliLK
      LookupKeyFields = 'ID'
      LookupResultField = 'Descrizione'
      KeyFields = 'IDMansione'
      Size = 40
      Lookup = True
    end
    object QRicercaSettore: TStringField
      FieldKind = fkLookup
      FieldName = 'Settore'
      LookupDataSet = QClientiLK
      LookupKeyFields = 'ID'
      LookupResultField = 'Attivita'
      KeyFields = 'IDCliente'
      Size = 50
      Lookup = True
    end
    object QRicercaUtente: TStringField
      FieldKind = fkLookup
      FieldName = 'Utente'
      LookupDataSet = QUsersLK
      LookupKeyFields = 'ID'
      LookupResultField = 'Nominativo'
      KeyFields = 'IDUtente'
      Size = 30
      Lookup = True
    end
  end
  object DsQRicerca: TDataSource
    DataSet = QRicerca
    OnStateChange = DsQRicercaStateChange
    Left = 192
    Top = 28
  end
  object QStatiRicLK: TQuery
    Active = True
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select * from EBC_StatiRic'
      'order by StatoRic')
    Left = 272
    Top = 8
    object QStatiRicLKID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.EBC_StatiRic.ID'
    end
    object QStatiRicLKStatoRic: TStringField
      FieldName = 'StatoRic'
      Origin = 'EBCDB.EBC_StatiRic.StatoRic'
      FixedChar = True
    end
  end
  object UpdRicerca: TUpdateSQL
    ModifySQL.Strings = (
      'update EBC_Ricerche'
      'set'
      '  IDCliente = :IDCliente,'
      '  IDUtente = :IDUtente,'
      '  Progressivo = :Progressivo,'
      '  DataInizio = :DataInizio,'
      '  DataFine = :DataFine,'
      '  Stato = :Stato,'
      '  Conclusa = :Conclusa,'
      '  IDStatoRic = :IDStatoRic,'
      '  DallaData = :DallaData,'
      '  IDArea = :IDArea,'
      '  IDMansione = :IDMansione,'
      '  IDSpec = :IDSpec,'
      '  NumRicercati = :NumRicercati,'
      '  Note = :Note,'
      '  IDFattura = :IDFattura,'
      '  Sospesa = :Sospesa,'
      '  SospesaDal = :SospesaDal,'
      '  StipendioNetto = :StipendioNetto,'
      '  StipendioLordo = :StipendioLordo,'
      '  Fatturata = :Fatturata,'
      '  Specifiche = :Specifiche,'
      '  SpecifEst = :SpecifEst,'
      '  Tipo = :Tipo,'
      '  IDSede = :IDSede,'
      '  IDContattoCli = :IDContattoCli,'
      '  IDUtente2 = :IDUtente2,'
      '  IDUtente3 = :IDUtente3,'
      '  DataPrevChiusura = :DataPrevChiusura,'
      '  ggDiffApRic = :ggDiffApRic,'
      '  ggDiffUcRic = :ggDiffUcRic,'
      '  ggDiffColRic = :ggDiffColRic,'
      '  IDOfferta = :IDOfferta,'
      '  Provvigione = :Provvigione,'
      '  ProvvigionePerc = :ProvvigionePerc,'
      '  QuandoPagamUtente = :QuandoPagamUtente,'
      '  OreLavUtente1 = :OreLavUtente1,'
      '  OreLavUtente2 = :OreLavUtente2,'
      '  OreLavUtente3 = :OreLavUtente3,'
      '  TitoloCliente = :TitoloCliente,'
      '  IDContrattoCG = :IDContrattoCG,'
      '  IdAzienda = :IdAzienda,'
      '  LKRuolo = :LKRuolo,'
      '  LkArea = :LkArea,'
      '  LkStatoRic = :LkStatoRic,'
      '  IDCausale = :IDCausale'
      'where'
      '  ID = :OLD_ID')
    InsertSQL.Strings = (
      'insert into EBC_Ricerche'
      
        '  (IDCliente, IDUtente, Progressivo, DataInizio, DataFine, Stato' +
        ', Conclusa, '
      
        '   IDStatoRic, DallaData, IDArea, IDMansione, IDSpec, NumRicerca' +
        'ti, Note, '
      
        '   IDFattura, Sospesa, SospesaDal, StipendioNetto, StipendioLord' +
        'o, Fatturata, '
      
        '   Specifiche, SpecifEst, Tipo, IDSede, IDContattoCli, IDUtente2' +
        ', IDUtente3, '
      
        '   DataPrevChiusura, ggDiffApRic, ggDiffUcRic, ggDiffColRic, IDO' +
        'fferta, '
      
        '   Provvigione, ProvvigionePerc, QuandoPagamUtente, OreLavUtente' +
        '1, OreLavUtente2, '
      
        '   OreLavUtente3, TitoloCliente, IDContrattoCG, IdAzienda, LKRuo' +
        'lo, LkArea, '
      '   LkStatoRic, IDCausale)'
      'values'
      
        '  (:IDCliente, :IDUtente, :Progressivo, :DataInizio, :DataFine, ' +
        ':Stato, '
      
        '   :Conclusa, :IDStatoRic, :DallaData, :IDArea, :IDMansione, :ID' +
        'Spec, :NumRicercati, '
      
        '   :Note, :IDFattura, :Sospesa, :SospesaDal, :StipendioNetto, :S' +
        'tipendioLordo, '
      
        '   :Fatturata, :Specifiche, :SpecifEst, :Tipo, :IDSede, :IDConta' +
        'ttoCli, '
      
        '   :IDUtente2, :IDUtente3, :DataPrevChiusura, :ggDiffApRic, :ggD' +
        'iffUcRic, '
      
        '   :ggDiffColRic, :IDOfferta, :Provvigione, :ProvvigionePerc, :Q' +
        'uandoPagamUtente, '
      
        '   :OreLavUtente1, :OreLavUtente2, :OreLavUtente3, :TitoloClient' +
        'e, :IDContrattoCG, '
      '   :IdAzienda, :LKRuolo, :LkArea, :LkStatoRic, :IDCausale)')
    DeleteSQL.Strings = (
      'delete from EBC_Ricerche'
      'where'
      '  ID = :OLD_ID')
    Left = 192
    Top = 56
  end
  object QClientiLK: TQuery
    Active = True
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select EBC_Clienti.ID,Descrizione, Attivita'
      'from EBC_Clienti, EBC_Attivita'
      'where EBC_Clienti.IDAttivita=EBC_Attivita.ID'
      'order by Descrizione')
    Left = 305
    Top = 9
    object QClientiLKID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.EBC_Clienti.ID'
    end
    object QClientiLKDescrizione: TStringField
      FieldName = 'Descrizione'
      Origin = 'EBCDB.EBC_Clienti.Descrizione'
      FixedChar = True
      Size = 50
    end
    object QClientiLKAttivita: TStringField
      FieldName = 'Attivita'
      Origin = 'EBCDB.EBC_Attivita.Attivita'
      FixedChar = True
      Size = 50
    end
  end
  object QRuoliLK: TQuery
    Active = True
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select ID,Descrizione from Mansioni'
      'order by Descrizione')
    Left = 313
    Top = 89
    object QRuoliLKID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.Mansioni.ID'
    end
    object QRuoliLKDescrizione: TStringField
      FieldName = 'Descrizione'
      Origin = 'EBCDB.Mansioni.Descrizione'
      FixedChar = True
      Size = 40
    end
  end
  object QUsersLK: TQuery
    Active = True
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select ID,Nominativo'
      'from Users'
      'order by Nominativo')
    Left = 345
    Top = 89
    object QUsersLKID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.Users.ID'
    end
    object QUsersLKNominativo: TStringField
      FieldName = 'Nominativo'
      Origin = 'EBCDB.Users.Nominativo'
      FixedChar = True
      Size = 30
    end
  end
  object QCandRic: TQuery
    OnCalcFields = QCandRicCalcFields
    DatabaseName = 'EBCDB'
    DataSource = DsQRicerca
    SQL.Strings = (
      'select Anagrafica.ID,Titolo,Cognome,Nome,DataNascita, '
      
        '          EBC_Stati.Stato, Note2, EBC_CandidatiRicerche.ID IDCan' +
        'dRic, Escluso'
      'from Anagrafica, EBC_CandidatiRicerche, EBC_Stati'
      'where Anagrafica.ID = EBC_CandidatiRicerche.IDAnagrafica'
      '    and Anagrafica.IDStato=EBC_Stati.ID'
      'and EBC_CandidatiRicerche.IDRicerca=:ID')
    Left = 104
    Top = 212
    ParamData = <
      item
        DataType = ftAutoInc
        Name = 'ID'
        ParamType = ptUnknown
      end>
    object QCandRicID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.Anagrafica.ID'
    end
    object QCandRicTitolo: TStringField
      FieldName = 'Titolo'
      Origin = 'EBCDB.Anagrafica.Titolo'
      FixedChar = True
    end
    object QCandRicCognome: TStringField
      FieldName = 'Cognome'
      Origin = 'EBCDB.Anagrafica.Cognome'
      FixedChar = True
      Size = 30
    end
    object QCandRicNome: TStringField
      FieldName = 'Nome'
      Origin = 'EBCDB.Anagrafica.Nome'
      FixedChar = True
      Size = 30
    end
    object QCandRicDataNascita: TDateTimeField
      FieldName = 'DataNascita'
      Origin = 'EBCDB.Anagrafica.DataNascita'
    end
    object QCandRicAzienda: TStringField
      FieldKind = fkLookup
      FieldName = 'Azienda'
      LookupDataSet = QLastEspLav
      LookupKeyFields = 'IDAnagrafica'
      LookupResultField = 'Azienda'
      KeyFields = 'ID'
      Size = 50
      Lookup = True
    end
    object QCandRicTitoloMansione: TStringField
      FieldKind = fkLookup
      FieldName = 'TitoloMansione'
      LookupDataSet = QLastEspLav
      LookupKeyFields = 'IDAnagrafica'
      LookupResultField = 'TitoloMansione'
      KeyFields = 'ID'
      Size = 80
      Lookup = True
    end
    object QCandRicStato: TStringField
      FieldName = 'Stato'
      Origin = 'EBCDB.EBC_Stati.Stato'
      FixedChar = True
      Size = 30
    end
    object QCandRicNote2: TStringField
      FieldName = 'Note2'
      Origin = 'EBCDB.EBC_CandidatiRicerche.Note2'
      FixedChar = True
      Size = 255
    end
    object QCandRicIDCandRic: TAutoIncField
      FieldName = 'IDCandRic'
      Origin = 'EBCDB.EBC_CandidatiRicerche.ID'
    end
    object QCandRicEta: TStringField
      FieldKind = fkCalculated
      FieldName = 'Eta'
      Size = 2
      Calculated = True
    end
    object QCandRicEscluso: TBooleanField
      FieldName = 'Escluso'
      Origin = 'EBCDB.EBC_CandidatiRicerche.Escluso'
    end
  end
  object DsQCandRic: TDataSource
    DataSet = QCandRic
    Left = 104
    Top = 244
  end
  object QEspLavLK: TQuery
    Active = True
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select Esperienzelavorative.ID, TitoloMansione,'
      '       EBC_Clienti.Descrizione Azienda,'
      '       Mansioni.Descrizione Ruolo '
      'from Esperienzelavorative,EBC_Clienti,Mansioni '
      'where Esperienzelavorative.IDAzienda *= EBC_Clienti.ID'
      '  and Esperienzelavorative.IDMansione *= Mansioni.ID')
    Left = 217
    Top = 281
    object QEspLavLKID: TAutoIncField
      FieldName = 'ID'
    end
    object QEspLavLKAzienda: TStringField
      FieldName = 'Azienda'
      FixedChar = True
      Size = 30
    end
    object QEspLavLKRuolo: TStringField
      FieldName = 'Ruolo'
      FixedChar = True
      Size = 40
    end
    object QEspLavLKTitoloMansione: TStringField
      FieldName = 'TitoloMansione'
      FixedChar = True
      Size = 80
    end
  end
  object QLastEspLav: TQuery
    Active = True
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select IDAnagrafica,max(ID) MaxID'
      'from EsperienzeLavorative '
      'where Attuale=1'
      'group by IDAnagrafica')
    Left = 177
    Top = 281
    object QLastEspLavIDAnagrafica: TIntegerField
      FieldName = 'IDAnagrafica'
    end
    object QLastEspLavMaxID: TIntegerField
      FieldName = 'MaxID'
    end
    object QLastEspLavAzienda: TStringField
      FieldKind = fkLookup
      FieldName = 'Azienda'
      LookupDataSet = QEspLavLK
      LookupKeyFields = 'ID'
      LookupResultField = 'Azienda'
      KeyFields = 'MaxID'
      Size = 30
      Lookup = True
    end
    object QLastEspLavRuolo: TStringField
      FieldKind = fkLookup
      FieldName = 'Ruolo'
      LookupDataSet = QEspLavLK
      LookupKeyFields = 'ID'
      LookupResultField = 'Ruolo'
      KeyFields = 'MaxID'
      Size = 40
      Lookup = True
    end
    object QLastEspLavTitoloMansione: TStringField
      FieldKind = fkLookup
      FieldName = 'TitoloMansione'
      LookupDataSet = QEspLavLK
      LookupKeyFields = 'ID'
      LookupResultField = 'TitoloMansione'
      KeyFields = 'MaxID'
      Size = 80
      Lookup = True
    end
  end
  object Q: TQuery
    DatabaseName = 'EBCDB'
    Left = 440
    Top = 8
  end
  object Q1: TQuery
    DatabaseName = 'EBCDB'
    Left = 224
    Top = 180
  end
end
