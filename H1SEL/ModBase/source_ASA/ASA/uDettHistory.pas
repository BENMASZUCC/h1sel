unit uDettHistory;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, dxDBTLCl, dxGrClms, dxTL, dxDBCtrl,
  dxDBGrid, dxCntner, Db, DBTables, TB97;

type
  TDettHistoryForm = class(TForm)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    QDettHistory: TQuery;
    DsQDettHistory: TDataSource;
    QDettHistoryEvento: TStringField;
    QDettHistoryNominativo: TStringField;
    QDettHistoryID: TAutoIncField;
    QDettHistoryIDAnagrafica: TIntegerField;
    QDettHistoryIDEvento: TIntegerField;
    QDettHistoryDataEvento: TDateTimeField;
    QDettHistoryAnnotazioni: TStringField;
    QDettHistoryIDRicerca: TIntegerField;
    QDettHistoryIDUtente: TIntegerField;
    QDettHistoryIDCliente: TIntegerField;
    dxDBGrid1: TdxDBGrid;
    dxDBGrid1Evento: TdxDBGridMaskColumn;
    dxDBGrid1Nominativo: TdxDBGridMaskColumn;
    dxDBGrid1DataEvento: TdxDBGridDateColumn;
    dxDBGrid1Annotazioni: TdxDBGridMaskColumn;
    BEliminaRiga: TToolbarButton97;
    procedure BEliminaRigaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DettHistoryForm: TDettHistoryForm;

implementation

uses ModuloDati;

{$R *.DFM}

procedure TDettHistoryForm.BEliminaRigaClick(Sender: TObject);
begin
     if MessageDlg('Sei sicuro di voler cancellare la riga selezionata ?',mtWarning, [mbYes,mbNo],0)=mrNo then exit;
     Data.Q1.Close;
     Data.Q1.SQL.text:='delete from storico where id='+QDettHistoryID.AsString;
     Data.Q1.ExecSQL;
     QDettHistory.Close;
     QDettHistory.Open;
end;

end.
