unit uInsEvento;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, dxTL, dxDBCtrl, dxDBGrid, dxCntner, Db, DBTables, Buttons,
  ExtCtrls, dxEditor, dxExEdtr, dxEdLib;

type
  TASAInsEventoForm = class(TForm)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    QEventi: TQuery;
    DsQEventi: TDataSource;
    dxDBGrid1: TdxDBGrid;
    dxDBGrid1evento: TdxDBGridMaskColumn;
    dxDBGrid1StatoSuccessivo: TdxDBGridMaskColumn;
    Label1: TLabel;
    Label2: TLabel;
    EDesc: TEdit;
    DeDataEvento: TdxDateEdit;
    Label3: TLabel;
    CBAssociaCommessa: TCheckBox;
    QCommesse: TQuery;
    DsQCommesse: TDataSource;
    dxDBGrid2: TdxDBGrid;
    dxDBGrid2RifCommessa: TdxDBGridMaskColumn;
    dxDBGrid2Cliente: TdxDBGridMaskColumn;
    QEventiMaxID: TIntegerField;
    QEventievento: TStringField;
    QEventiStatoSuccessivo: TStringField;
    QEventiIDAStato: TIntegerField;
    QCommesseRifCommessa: TStringField;
    QCommesseCliente: TStringField;
    QCommesseID: TAutoIncField;
    Label4: TLabel;
    QCommesseIDCliente: TIntegerField;
    CBModStato: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure CBAssociaCommessaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ASAInsEventoForm: TASAInsEventoForm;

implementation

uses uASACand;

{$R *.DFM}

procedure TASAInsEventoForm.FormShow(Sender: TObject);
begin
     DeDataEvento.Date:=Date;
     QCommesse.Open;
end;

procedure TASAInsEventoForm.CBAssociaCommessaClick(Sender: TObject);
begin
     if CBAssociaCommessa.Checked then dxDBGrid2.color:=clWindow
     else dxDBGrid2.color:=clBtnFace;
end;

end.
