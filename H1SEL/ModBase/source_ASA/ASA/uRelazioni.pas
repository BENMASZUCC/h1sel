unit uRelazioni;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables, ExtCtrls, dxDBTLCl, dxGrClms, dxTL, dxDBCtrl, dxCntner,
  dxDBGrid, TB97, dxEditor, dxExEdtr, dxEdLib, dxDBELib;

type
  TRelazioniForm = class(TForm)
    Panel1: TPanel;
    QAnagRelaz: TQuery;
    DsQAnagRelaz: TDataSource;
    QAnagRelazID: TAutoIncField;
    QAnagRelazIDAnagrafica: TIntegerField;
    QAnagRelazIDUtente: TIntegerField;
    QAnagRelazIDTipoRelazione: TIntegerField;
    QAnagRelazNote: TMemoField;
    QTipiRelazLK: TQuery;
    QUtentiLK: TQuery;
    QTipiRelazLKID: TAutoIncField;
    QTipiRelazLKTipoRelazione: TStringField;
    QAnagRelazTipoRelazione: TStringField;
    QUtentiLKID: TAutoIncField;
    QUtentiLKNominativo: TStringField;
    QAnagRelazUtente: TStringField;
    dxDBGrid1: TdxDBGrid;
    dxDBGrid1TipoRelazione: TdxDBGridLookupColumn;
    dxDBGrid1Utente: TdxDBGridLookupColumn;
    dxDBGrid1Note: TdxDBGridBlobColumn;
    BNew: TToolbarButton97;
    BDel: TToolbarButton97;
    BOK: TToolbarButton97;
    BCan: TToolbarButton97;
    BTabTipiRelaz: TToolbarButton97;
    QClientiLK: TQuery;
    QClientiLKID: TAutoIncField;
    QClientiLKDescrizione: TStringField;
    QAnagRelazIDCliente: TIntegerField;
    QAnagRelazCliente: TStringField;
    UpdAnagRelazione: TUpdateSQL;
    dxDBGrid1Column4: TdxDBGridLookupColumn;
    procedure DsQAnagRelazStateChange(Sender: TObject);
    procedure BNewClick(Sender: TObject);
    procedure BDelClick(Sender: TObject);
    procedure BOKClick(Sender: TObject);
    procedure BCanClick(Sender: TObject);
    procedure QAnagRelazAfterInsert(DataSet: TDataSet);
    procedure QAnagRelazAfterPost(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure BTabTipiRelazClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  RelazioniForm: TRelazioniForm;

implementation

uses uASACand, uUtilsVarie;

{$R *.DFM}

procedure TRelazioniForm.DsQAnagRelazStateChange(Sender: TObject);
var b: boolean;
begin
     b:=DsQAnagRelaz.State in [dsEdit,dsInsert];
     BNew.enabled:=not b;
     BDel.enabled:=not b;
     BOK.enabled:=b;
     BCan.enabled:=b;
end;

procedure TRelazioniForm.BNewClick(Sender: TObject);
begin
     QAnagRelaz.Insert;
end;

procedure TRelazioniForm.BDelClick(Sender: TObject);
begin
     QAnagRelaz.Delete;
end;

procedure TRelazioniForm.BOKClick(Sender: TObject);
begin
     QAnagRelaz.Post;
end;

procedure TRelazioniForm.BCanClick(Sender: TObject);
begin
     QAnagRelaz.Cancel;
end;

procedure TRelazioniForm.QAnagRelazAfterInsert(DataSet: TDataSet);
begin
     QAnagRelazIDAnagrafica.Value:=ASASchedaCandForm.QAnagID.Value;
end;

procedure TRelazioniForm.QAnagRelazAfterPost(DataSet: TDataSet);
begin
     QAnagRelaz.ApplyUpdates;
     QAnagRelaz.CommitUpdates;
     //QAnagRelaz.Close;
     //QAnagRelaz.Open;
end;

procedure TRelazioniForm.FormShow(Sender: TObject);
begin
     QAnagRelaz.Open;
end;

procedure TRelazioniForm.BTabTipiRelazClick(Sender: TObject);
begin
     OpenTab('TipiRelazioniUtenti',['ID','TipoRelazione'],['Tipo di relazione']);
     QTipiRelazLK.Close;
     QTipiRelazLK.Open;
end;

end.
