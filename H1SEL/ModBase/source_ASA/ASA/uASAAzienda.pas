unit uASAAzienda;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  dxLayoutControl, cxControls, ExtCtrls, Db, DBTables, dxExEdtr, dxEdLib,
  dxDBELib, dxCntner, dxEditor, StdCtrls, Mask, DBCtrls, dxDBEdtr, TB97,
  dxTL, dxDBCtrl, dxDBTLCl, dxGrClms, dxDBGrid, dxLayoutLookAndFeels, ShellAPI;

type
  TASAAziendaForm = class(TForm)
    Panel1: TPanel;
    lcMainGroup_Root: TdxLayoutGroup;
    lcMain: TdxLayoutControl;
    QAzienda: TQuery;
    DsQAzienda: TDataSource;
    UpdAzienda: TUpdateSQL;
    QAziendaID: TAutoIncField;
    QAziendaDescrizione: TStringField;
    QAziendaStato: TStringField;
    QAziendaIndirizzo: TStringField;
    QAziendaCap: TStringField;
    QAziendaComune: TStringField;
    QAziendaProvincia: TStringField;
    QAziendaIDAttivita: TIntegerField;
    QAziendaTelefono: TStringField;
    QAziendaFax: TStringField;
    QAziendaPartitaIVA: TStringField;
    QAziendaCodiceFiscale: TStringField;
    QAziendaBancaAppoggio: TStringField;
    QAziendaSistemaPagamento: TStringField;
    QAziendaNoteContratto: TMemoField;
    QAziendaResponsabile: TStringField;
    QAziendaConosciutoInData: TDateTimeField;
    QAziendaIndirizzoLegale: TStringField;
    QAziendaCapLegale: TStringField;
    QAziendaComuneLegale: TStringField;
    QAziendaProvinciaLegale: TStringField;
    QAziendaCartellaDoc: TStringField;
    QAziendaTipoStrada: TStringField;
    QAziendaTipoStradaLegale: TStringField;
    QAziendaNumCivico: TStringField;
    QAziendaNumCivicoLegale: TStringField;
    QAziendaNazioneAbb: TStringField;
    QAziendaNazioneAbbLegale: TStringField;
    QAziendaNote: TMemoField;
    QAziendaFatturato: TFloatField;
    QAziendaNumDipendenti: TSmallintField;
    QAziendaBlocco1: TBooleanField;
    QAziendaEnteCertificatore: TStringField;
    QAziendaLibPrivacy: TBooleanField;
    QAziendaIdAzienda: TIntegerField;
    QAziendaSitoInternet: TStringField;
    QAziendaDescAttivitaAzienda: TStringField;
    lcMainGroup1: TdxLayoutGroup;
    lcMainItem1: TdxLayoutItem;
    lcMainItem2: TdxLayoutItem;
    lcMainItem3: TdxLayoutItem;
    lcMainItem4: TdxLayoutItem;
    lcMainItem5: TdxLayoutItem;
    lcMainItem6: TdxLayoutItem;
    lcMainItem7: TdxLayoutItem;
    lcMainItem8: TdxLayoutItem;
    lcMainGroup2: TdxLayoutGroup;
    lcMainGroup4: TdxLayoutGroup;
    dxDBEdit1: TdxDBEdit;
    dxDBEdit2: TdxDBEdit;
    dxDBEdit3: TdxDBEdit;
    dxDBEdit4: TdxDBEdit;
    dxDBEdit5: TdxDBEdit;
    dxDBEdit6: TdxDBEdit;
    dxDBEdit7: TdxDBEdit;
    lcMainGroup3: TdxLayoutGroup;
    lcMainGroup5: TdxLayoutGroup;
    lcMainItem9: TdxLayoutItem;
    lcMainItem10: TdxLayoutItem;
    lcMainItem11: TdxLayoutItem;
    lcMainItem12: TdxLayoutItem;
    dxDBEdit8: TdxDBEdit;
    dxDBEdit9: TdxDBEdit;
    dxDBEdit10: TdxDBEdit;
    dxDBEdit11: TdxDBEdit;
    lcMainItem13: TdxLayoutItem;
    lcMainItem14: TdxLayoutItem;
    QSettoriLK: TQuery;
    QSettoriLKID: TAutoIncField;
    QSettoriLKAttivita: TStringField;
    QSettoriLKIDAreaSettore: TIntegerField;
    QSettoriLKIDAzienda: TStringField;
    QAziendaAttivita: TStringField;
    dxDBLookupEdit1: TdxDBLookupEdit;
    dxDBMemo1: TdxDBMemo;
    BNuovo: TToolbarButton97;
    BModCand: TToolbarButton97;
    BElimina: TToolbarButton97;
    BOK: TToolbarButton97;
    BCan: TToolbarButton97;
    BPrec: TToolbarButton97;
    BSucc: TToolbarButton97;
    dxLayoutControl1Group_Root: TdxLayoutGroup;
    dxLayoutControl1: TdxLayoutControl;
    QAltriSettori: TQuery;
    DsQAltriSettori: TDataSource;
    QAltriSettoriID: TAutoIncField;
    QAltriSettoriIDCliente: TIntegerField;
    QAltriSettoriIDAttivita: TIntegerField;
    QAltriSettoriAttivita: TStringField;
    dxDBGrid1: TdxDBGrid;
    lcMainItem15: TdxLayoutItem;
    dxDBGrid1Attivita: TdxDBGridLookupColumn;
    lcMainGroup6: TdxLayoutGroup;
    lcMainGroup8: TdxLayoutGroup;
    lcMainGroup9: TdxLayoutGroup;
    lcMainGroup10: TdxLayoutGroup;
    lcMainGroup12: TdxLayoutGroup;
    dxDBPickEdit1: TdxDBPickEdit;
    dxLayoutControl1Item1: TdxLayoutItem;
    DsQRifInterni: TDataSource;
    dxDBGrid2: TdxDBGrid;
    lcMainItem16: TdxLayoutItem;
    QContattiFatti: TQuery;
    DsQContattiFatti: TDataSource;
    dxDBGrid3: TdxDBGrid;
    lcMainItem17: TdxLayoutItem;
    dxDBGrid3TipoContatto: TdxDBGridMaskColumn;
    dxDBGrid3Ore: TdxDBGridDateColumn;
    dxDBGrid3ParlatoCon: TdxDBGridMaskColumn;
    dxDBGrid3Descrizione: TdxDBGridBlobColumn;
    QContattiFattiID: TAutoIncField;
    QContattiFattiIDCliente: TIntegerField;
    QContattiFattiTipoContatto: TStringField;
    QContattiFattiData: TDateTimeField;
    QContattiFattiOre: TDateTimeField;
    QContattiFattiParlatoCon: TStringField;
    QContattiFattiDescrizione: TStringField;
    QContattiFattiIDOfferta: TIntegerField;
    QContattiFattiIDContattoCli: TIntegerField;
    BCandPresenti: TToolbarButton97;
    BOfferte: TToolbarButton97;
    BDocs: TToolbarButton97;
    Panel2: TPanel;
    lcMainItem18: TdxLayoutItem;
    Panel3: TPanel;
    lcMainItem19: TdxLayoutItem;
    Panel4: TPanel;
    lcMainItem20: TdxLayoutItem;
    lcMainGroup7: TdxLayoutGroup;
    BAltreAttivOK: TToolbarButton97;
    BAltreAttivCan: TToolbarButton97;
    BAltreAttivNew: TToolbarButton97;
    BAltreAttivDel: TToolbarButton97;
    dxLayoutLookAndFeelList1: TdxLayoutLookAndFeelList;
    dxLayoutStandardLookAndFeel1: TdxLayoutStandardLookAndFeel;
    dxLayoutOfficeLookAndFeel1: TdxLayoutOfficeLookAndFeel;
    dxLayoutWebLookAndFeel1: TdxLayoutWebLookAndFeel;
    QAziendaDallanno: TIntegerField;
    dxDBEdit12: TdxDBEdit;
    lcMainItem21: TdxLayoutItem;
    QAziendaParentCompany: TStringField;
    ToolbarButton971: TToolbarButton97;
    ToolbarButton972: TToolbarButton97;
    BMod: TToolbarButton97;
    QRifInterni: TQuery;
    dxDBGrid2Cognome: TdxDBGridMaskColumn;
    dxDBGrid2Nome: TdxDBGridMaskColumn;
    dxDBGrid2RecapitiTelefonici: TdxDBGridMaskColumn;
    QRifInterniID: TAutoIncField;
    QRifInterniCognome: TStringField;
    QRifInterniNome: TStringField;
    QRifInterniRecapitiTelefonici: TStringField;
    QRifInterniCellulare: TStringField;
    dxDBGrid2Column4: TdxDBGridColumn;
    BApriSchedaSogg: TToolbarButton97;
    DBEdit1: TDBEdit;
    procedure FormShow(Sender: TObject);
    procedure DsQAziendaStateChange(Sender: TObject);
    procedure BOKClick(Sender: TObject);
    procedure BCanClick(Sender: TObject);
    procedure BPrecClick(Sender: TObject);
    procedure BSuccClick(Sender: TObject);
    procedure QAziendaAfterPost(DataSet: TDataSet);
    procedure QAziendaAfterOpen(DataSet: TDataSet);
    procedure QAziendaBeforeClose(DataSet: TDataSet);
    procedure BCandPresentiClick(Sender: TObject);
    procedure BOfferteClick(Sender: TObject);
    procedure BAltreAttivNewClick(Sender: TObject);
    procedure BAltreAttivDelClick(Sender: TObject);
    procedure BAltreAttivOKClick(Sender: TObject);
    procedure BAltreAttivCanClick(Sender: TObject);
    procedure QAltriSettoriAfterPost(DataSet: TDataSet);
    procedure DsQAltriSettoriStateChange(Sender: TObject);
    procedure BDocsClick(Sender: TObject);
    procedure BNuovoClick(Sender: TObject);
    procedure BEliminaClick(Sender: TObject);
    procedure QAltriSettoriAfterInsert(DataSet: TDataSet);
    procedure ToolbarButton971Click(Sender: TObject);
    procedure BModClick(Sender: TObject);
    procedure ToolbarButton972Click(Sender: TObject);
    procedure BApriSchedaSoggClick(Sender: TObject);
    procedure DBEdit1DblClick(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
    
  end;

var
  ASAAziendaForm: TASAAziendaForm;

implementation

uses ModuloDati, uAzCandPresenti, uAzOfferte, ModuloDati2, Main,
  ContattoCliFatto, uUtilsVarie, uASACand, uAziendaFile, RicClienti;

{$R *.DFM}

procedure TASAAziendaForm.FormShow(Sender: TObject);
begin
  // posizionati sul soggetto selezionato
  QAzienda.Close;
  QAzienda.ParamByName('xIDAzienda').asInteger := Data2.TEBCClientiID.Value;
  QAzienda.Open;
  if Data2.TEBCClientiID.Value = 0 then begin
    lcMain.Enabled := false;
    dxDBPickEdit1.Enabled := False;
    if QRifInterni.active then QRifInterni.Close;
  end else begin
    lcMain.Enabled := true;
    dxDBPickEdit1.Enabled := true;
    if QRifInterni.active = false then QRifInterni.Open;
  end;
  // pulsanti prec. e succ.
  BPrec.Enabled := not Data2.TEBCClienti.BOF;
  BSucc.Enabled := not Data2.TEBCClienti.EOF;
end;

procedure TASAAziendaForm.DsQAziendaStateChange(Sender: TObject);
var b: boolean;
begin
  b := DsQAzienda.State in [dsEdit, dsInsert];
  BNuovo.Visible := not b;
  BElimina.Visible := not b;
  //BModCand.Visible:=not b;
  //lcMain.enabled:=b;
  BOK.Visible := b;
  BCan.Visible := b;
end;

procedure TASAAziendaForm.BOKClick(Sender: TObject);
begin
  QAzienda.Post;
end;

procedure TASAAziendaForm.BCanClick(Sender: TObject);
begin
  QAzienda.Cancel;
end;

procedure TASAAziendaForm.BPrecClick(Sender: TObject);
begin
  if Data2.TEBCClienti.BOF then exit;
  Data2.TEBCClienti.Prior;
  QAzienda.Close;
  QAzienda.ParamByName('xIDAzienda').asInteger := Data2.TEBCClientiID.Value;
  QAzienda.Open;
  BPrec.Enabled := not Data2.TEBCClienti.BOF;
  BSucc.Enabled := not Data2.TEBCClienti.EOF;
end;

procedure TASAAziendaForm.BSuccClick(Sender: TObject);
begin
  if Data2.TEBCClienti.EOF then exit;
  Data2.TEBCClienti.Next;
  QAzienda.Close;
  QAzienda.ParamByName('xIDAzienda').asInteger := Data2.TEBCClientiID.Value;
  QAzienda.Open;
  BPrec.Enabled := not Data2.TEBCClienti.BOF;
  BSucc.Enabled := not Data2.TEBCClienti.EOF;
end;

procedure TASAAziendaForm.QAziendaAfterPost(DataSet: TDataSet);
begin
  QAzienda.ApplyUpdates;
  QAzienda.CommitUpdates;
end;

procedure TASAAziendaForm.QAziendaAfterOpen(DataSet: TDataSet);
begin
  QAltriSettori.Open;
  QRifInterni.Open;
  QContattiFatti.Open;
end;

procedure TASAAziendaForm.QAziendaBeforeClose(DataSet: TDataSet);
begin
  QAltriSettori.Close;
  QRifInterni.Close;
  QContattiFatti.Close;
end;

procedure TASAAziendaForm.BCandPresentiClick(Sender: TObject);
begin
  AzCandPresentiForm := TAzCandPresentiForm.create(self);
  AzCandPresentiForm.ShowModal;
  AzCandPresentiForm.Free;
end;

procedure TASAAziendaForm.BOfferteClick(Sender: TObject);
begin
  AzOfferteForm := TAzOfferteForm.create(self);
  AzOfferteForm.ShowModal;
  AzOfferteForm.Free;
end;

procedure TASAAziendaForm.BAltreAttivNewClick(Sender: TObject);
begin
  QAltriSettori.Insert;
end;

procedure TASAAziendaForm.BAltreAttivDelClick(Sender: TObject);
begin
  if MessageDlg('Sei sicuro di voler cancellare il settore selezionato ?', mtWarning, [mbYes, mbNo], 0) = mrNo then exit;
  QAltriSettori.Delete;
end;

procedure TASAAziendaForm.BAltreAttivOKClick(Sender: TObject);
begin
  QAltriSettori.Post;
end;

procedure TASAAziendaForm.BAltreAttivCanClick(Sender: TObject);
begin
  QAltriSettori.Cancel;
end;

procedure TASAAziendaForm.QAltriSettoriAfterPost(DataSet: TDataSet);
begin
  QAltriSettori.Close;
  QAltriSettori.Open;
end;

procedure TASAAziendaForm.DsQAltriSettoriStateChange(Sender: TObject);
var b: boolean;
begin
  b := DsQAltriSettori.State in [dsEdit, dsInsert];
  BAltreAttivNew.Enabled := not b;
  BAltreAttivCan.Enabled := not b;
  BAltreAttivOK.Enabled := b;
  BAltreAttivCan.Enabled := b;
end;

procedure TASAAziendaForm.BDocsClick(Sender: TObject);
begin
  FileAziendaForm := TFileAziendaForm.create(self);
  FileAziendaForm.ShowModal;
  FileAziendaForm.Free;
end;

procedure TASAAziendaForm.BNuovoClick(Sender: TObject);
begin
  MainForm.TbBClientiNewClick(nil);
  QAzienda.Close;
  QAzienda.ParamByName('xIDAzienda').asInteger := Data2.TEBCClientiID.Value;
  QAzienda.Open;
  if Data2.TEBCClientiID.Value = 0 then begin
    lcMain.Enabled := false;
    dxDBPickEdit1.Enabled := False;
    if QRifInterni.active then QRifInterni.Close;
  end else begin
    lcMain.Enabled := true;
    dxDBPickEdit1.Enabled := true;
    if QRifInterni.active = false then QRifInterni.Open;
  end;
  // pulsanti prec. e succ.
  BPrec.Enabled := not Data2.TEBCClienti.BOF;
  BSucc.Enabled := not Data2.TEBCClienti.EOF;
end;

procedure TASAAziendaForm.BEliminaClick(Sender: TObject);
begin
  MainForm.TbBClientiDelClick(nil);
  Close;
end;

procedure TASAAziendaForm.QAltriSettoriAfterInsert(DataSet: TDataSet);
begin
  QAltriSettoriIDCliente.Value := QAziendaID.Value;
end;

procedure TASAAziendaForm.ToolbarButton971Click(Sender: TObject);
begin
  ContattoCliFattoForm := TContattoCliFattoForm.create(self);
  ContattoCliFattoForm.DEData.Date := Date;
  ContattoCliFattoForm.MEOre.Text := TimeToStr(Now);
  ContattoCliFattoForm.xIDContattoCli := 0;
  ContattoCliFattoForm.xIDOfferta := 0;
  ContattoCliFattoForm.Showmodal;
  if ContattoCliFattoForm.ModalResult = mrOK then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'insert into EBC_ContattiFatti (IDCliente,TipoContatto,Data,Ore,ParlatoCon,Descrizione,IDOfferta,IDContattoCli) ' +
          'values (:xIDCliente,:xTipoContatto,:xData,:xOre,:xParlatoCon,:xDescrizione,:xIDOfferta,:xIDContattoCli)';
        Q1.ParamByName('xIDCliente').asInteger := QAziendaID.Value;
        Q1.ParamByName('xTipoContatto').asString := ContattoCliFattoForm.CBTipoContatto.Text;
        Q1.ParamByName('xData').asDateTime := ContattoCliFattoForm.DEData.Date;
        Q1.ParamByName('xOre').asDateTime := StrToDateTime(DatetoStr(ContattoCliFattoForm.DEData.Date) + ' ' + ContattoCliFattoForm.MEOre.Text);
        Q1.ParamByName('xParlatoCon').asString := ContattoCliFattoForm.EParlatoCon.Text;
        Q1.ParamByName('xDescrizione').asString := ContattoCliFattoForm.EDescrizione.Text;
        Q1.ParamByName('xIDOfferta').asInteger := ContattoCliFattoForm.xIDOfferta;
        Q1.ParamByName('xIDContattoCli').asInteger := ContattoCliFattoForm.xIDContattoCli;
        Q1.ExecSQL;
        DB.CommitTrans;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
    QContattiFatti.Close;
    QContattiFatti.Open;
  end;
  ContattoCliFattoForm.Free;
end;

procedure TASAAziendaForm.BModClick(Sender: TObject);
begin
  ContattoCliFattoForm := TContattoCliFattoForm.create(self);
  ContattoCliFattoForm.CBTipoContatto.Text := QContattiFattiTipoContatto.Value;
  ContattoCliFattoForm.DEData.Date := QContattiFattiData.Value;
  ContattoCliFattoForm.MEOre.Text := QContattiFattiOre.AsString;
  ContattoCliFattoForm.EParlatoCon.Text := QContattiFattiParlatoCon.Value;
  ContattoCliFattoForm.EDescrizione.Text := QContattiFattiDescrizione.Value;
  ContattoCliFattoForm.xIDContattoCli := QContattiFattiIDContattoCli.Value;
  ContattoCliFattoForm.xIDOfferta := 0;
  ContattoCliFattoForm.CBRifOfferta.Checked := False;
  ContattoCliFattoForm.DBGrid2.color := clBtnFace;
  ContattoCliFattoForm.Showmodal;
  if ContattoCliFattoForm.ModalResult = mrOK then begin
    with Data do begin
      DB.BeginTrans;
      try
        Q1.Close;
        Q1.SQL.text := 'update EBC_ContattiFatti set TipoContatto=:xTipoContatto,Data=:xData,Ore=:xOre,ParlatoCon=:xParlatoCon, ' +
          'Descrizione=:xDescrizione,IDOfferta=:xIDOfferta,IDContattoCli=:xIDContattoCli ' +
          'where ID=' + QContattiFattiID.AsString;
        Q1.ParamByName('xTipoContatto').asString := ContattoCliFattoForm.CBTipoContatto.Text;
        Q1.ParamByName('xData').asDateTime := ContattoCliFattoForm.DEData.Date;
        Q1.ParamByName('xOre').asDateTime := StrToDateTime(DatetoStr(ContattoCliFattoForm.DEData.Date) + ' ' + ContattoCliFattoForm.MEOre.Text);
        Q1.ParamByName('xParlatoCon').asString := ContattoCliFattoForm.EParlatoCon.Text;
        Q1.ParamByName('xDescrizione').asString := ContattoCliFattoForm.EDescrizione.Text;
        Q1.ParamByName('xIDOfferta').asInteger := ContattoCliFattoForm.xIDOfferta;
        Q1.ParamByName('xIDContattoCli').asInteger := ContattoCliFattoForm.xIDContattoCli;
        Q1.ExecSQL;
        DB.CommitTrans;
      except
        DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
      end;
    end;
    QContattiFatti.Close;
    QContattiFatti.Open;
  end;
  ContattoCliFattoForm.Free;
end;

procedure TASAAziendaForm.ToolbarButton972Click(Sender: TObject);
begin
  if not QContattiFatti.IsEmpty then
    if MessageDlg('Sei sicuro di voler eliminare il contatto ?', mtWarning, [mbNo, mbYes], 0) = mrYes then begin
      with Data do begin
        DB.BeginTrans;
        try
          Q1.Close;
          Q1.SQL.text := 'delete from EBC_ContattiFatti where ID=' + QContattiFattiID.AsString;
          Q1.ExecSQL;
          DB.CommitTrans;
        except
          DB.RollbackTrans;
          MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
        end;
      end;
    end;
  QContattiFatti.Close;
  QContattiFatti.open;
end;

procedure TASAAziendaForm.BApriSchedaSoggClick(Sender: TObject);
begin
  if QRifInterni.IsEmpty then exit;
  CaricaApriAnag(QRifInterniCognome.Value, QRifInterniNome.Value);
  //Data.TAnagrafica.locate('IDAzienda',QAziendaID.Value, []);
  ASASchedaCandForm := TASASchedaCandForm.create(self);
  ASASchedaCandForm.ShowModal;
  ASASchedaCandForm.Free;
end;

procedure TASAAziendaForm.DBEdit1DblClick(Sender: TObject);
begin
  if DBEdit1.text <> '' then
    ShellExecute(GetParentForm(Self).Handle, 'OPEN', PChar(dbedit1.Text), nil, nil, SW_SHOWMAXIMIZED);

end;

end.

