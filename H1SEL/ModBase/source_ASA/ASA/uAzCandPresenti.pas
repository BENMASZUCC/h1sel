unit uAzCandPresenti;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  dxTL, dxDBCtrl, dxDBGrid, dxCntner, Db, StdCtrls, Buttons, ExtCtrls,
  DBTables;

type
  TAzCandPresentiForm = class(TForm)
    QCandAzienda: TQuery;
    DsQCandAzienda: TDataSource;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    QCandAziendaID: TAutoIncField;
    QCandAziendaCVNumero: TIntegerField;
    QCandAziendaCognome: TStringField;
    QCandAziendaNome: TStringField;
    dxDBGrid1: TdxDBGrid;
    dxDBGrid1CVNumero: TdxDBGridMaskColumn;
    dxDBGrid1Cognome: TdxDBGridMaskColumn;
    dxDBGrid1Nome: TdxDBGridMaskColumn;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  AzCandPresentiForm: TAzCandPresentiForm;

implementation

uses uASAAzienda;

{$R *.DFM}

procedure TAzCandPresentiForm.FormShow(Sender: TObject);
begin
     QCandAzienda.Open;
end;

end.
