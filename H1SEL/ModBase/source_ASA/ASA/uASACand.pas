unit uASACand;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  dxExEdtr, dxEdLib, dxDBELib, dxCntner, dxEditor, dxLayoutControl,
  cxControls, StdCtrls, Buttons, ComCtrls, dxLayoutLookAndFeels, Db,
  DBTables, ExtCtrls, TB97, dxDBEdtr, dxTL, dxDBCtrl, dxDBGrid, dxDBTLCl,
  dxGrClms, dxLayout, Mask, DBCtrls, FR_DSet, FR_DBSet, FR_Class,
  FR_ADODB, FR_BDEDB;

type
  TASASchedaCandForm = class(TForm)
    LcMainGroup_Root: TdxLayoutGroup;
    LcMain: TdxLayoutControl;
    Panel1: TPanel;
    LcMainDatiSoggetto: TdxLayoutGroup;
    dxLayoutLookAndFeelList1: TdxLayoutLookAndFeelList;
    dxLayoutStandardLookAndFeel1: TdxLayoutStandardLookAndFeel;
    QAnag: TQuery;
    DsQAnag: TDataSource;
    UpdAnag: TUpdateSQL;
    dxDBEdit1: TdxDBEdit;
    LcMainItem1: TdxLayoutItem;
    LcMainItem2: TdxLayoutItem; dxDBEdit2: TdxDBEdit;
    BCustomize: TToolbarButton97;
    dxLayoutOfficeLookAndFeel1: TdxLayoutOfficeLookAndFeel;
    dxLayoutWebLookAndFeel1: TdxLayoutWebLookAndFeel;
    dxDBEdit3: TdxDBEdit;
    dxDBPickEdit1: TdxDBPickEdit;
    dxDBDateEdit1: TdxDBDateEdit;
    dxDBEdit5: TdxDBEdit;
    QAnagID: TAutoIncField;
    QAnagMatricola: TIntegerField;
    QAnagCognome: TStringField;
    QAnagNome: TStringField;
    QAnagTitolo: TStringField;
    QAnagDataNascita: TDateTimeField;
    QAnagGiornoNascita: TSmallintField;
    QAnagMeseNascita: TSmallintField;
    QAnagLuogoNascita: TStringField;
    QAnagSesso: TStringField;
    QAnagIndirizzo: TStringField;
    QAnagCap: TStringField;
    QAnagComune: TStringField;
    QAnagIDComuneRes: TIntegerField;
    QAnagIDZonaRes: TIntegerField;
    QAnagProvincia: TStringField;
    QAnagStato: TStringField;
    QAnagDomicilioIndirizzo: TStringField;
    QAnagDomicilioCap: TStringField;
    QAnagDomicilioComune: TStringField;
    QAnagIDComuneDom: TIntegerField;
    QAnagIDZonaDom: TIntegerField;
    QAnagDomicilioProvincia: TStringField;
    QAnagDomicilioStato: TStringField;
    QAnagRecapitiTelefonici: TStringField;
    QAnagCellulare: TStringField;
    QAnagFax: TStringField;
    QAnagCodiceFiscale: TStringField;
    QAnagPartitaIVA: TStringField;
    QAnagEmail: TStringField;
    QAnagIDStatoCivile: TIntegerField;
    QAnagIDStato: TIntegerField;
    QAnagIDTipoStato: TIntegerField;
    QAnagCVNumero: TIntegerField;
    QAnagCVIDAnnData: TIntegerField;
    QAnagCVinseritoInData: TDateTimeField;
    QAnagFoto: TBlobField;
    QAnagIDUtenteMod: TIntegerField;
    QAnagDubbiIns: TStringField;
    QAnagIDProprietaCV: TIntegerField;
    QAnagTelUfficio: TStringField;
    QAnagOldCVNumero: TStringField;
    QAnagTipoStrada: TStringField;
    QAnagDomicilioTipoStrada: TStringField;
    QAnagNumCivico: TStringField;
    QAnagDomicilioNumCivico: TStringField;
    QAnagIDUtente: TIntegerField;
    QAnagIDQualifContr: TIntegerField;
    QAnagTagliaMaglia: TStringField;
    QAnagTagliaPantaloni: TStringField;
    QAnagTagliaScarpe: TStringField;
    QAnagIDAzienda: TIntegerField;
    QAnagTotValutaz: TIntegerField;
    QAnagLibPrivacy: TBooleanField;
    QAnagCVDataReale: TDateTimeField;
    QAnagCVDataAgg: TDateTimeField;
    QAnagDomicilioRegione: TStringField;
    QAnagDomicilioAreaNielsen: TSmallintField;
    QAnagIdTipoStrada: TIntegerField;
    QAnagIdDomicilioTipoStrada: TIntegerField;
    QAnagAziendaPaghe: TIntegerField;
    QAnagCodicePaghe: TIntegerField;
    QAnagIDTipologia: TIntegerField;
    QAnagEta: TStringField;
    LcMainItem3: TdxLayoutItem;
    LcMainItem4: TdxLayoutItem;
    dxDBEdit4: TdxDBEdit;
    LcMainItem5: TdxLayoutItem;
    LcMainItem6: TdxLayoutItem;
    LcMainGroup3: TdxLayoutGroup;
    LcMainGroup2: TdxLayoutGroup;
    LcMainItem7: TdxLayoutItem;
    QStatiCiviliLK: TQuery;
    QStatiCiviliLKID: TAutoIncField;
    QStatiCiviliLKDescrizione: TStringField;
    QStatiCiviliLKSesso: TStringField;
    QAnagStatoCivile: TStringField;
    dxDBLookupEdit1: TdxDBLookupEdit;
    LcMainItem8: TdxLayoutItem;
    QAnagNumFigli: TStringField;
    dxDBEdit6: TdxDBEdit;
    LcMainItem9: TdxLayoutItem;
    LcMainGroup1: TdxLayoutGroup;
    LcMainGroup4: TdxLayoutGroup;
    LcMainGroup6: TdxLayoutGroup;
    LcMainGroup5: TdxLayoutGroup;
    LcMainItem11: TdxLayoutItem;
    LcMainGroup8: TdxLayoutGroup;
    dxDBEdit7: TdxDBEdit;
    dxDBEdit8: TdxDBEdit;
    dxDBEdit9: TdxDBEdit;
    LcMainItem12: TdxLayoutItem;
    LcMainItem13: TdxLayoutItem;
    dxDBEdit10: TdxDBEdit;
    LcMainItem14: TdxLayoutItem;
    LcMainResidenza: TdxLayoutGroup;
    LcMainItem10: TdxLayoutItem;
    LcMainItem15: TdxLayoutItem;
    LcMainItem16: TdxLayoutItem;
    LcMainGroup7: TdxLayoutGroup;
    LcMainGroup10: TdxLayoutGroup;
    dxDBEdit11: TdxDBEdit;
    dxDBEdit12: TdxDBEdit;
    dxDBEdit13: TdxDBEdit;
    LcMainGroup12: TdxLayoutGroup;
    BDomicilio: TSpeedButton;
    BResidenza: TSpeedButton;
    LcMainItem18: TdxLayoutItem;
    LcMainItem19: TdxLayoutItem;
    LcMainDomicilio: TdxLayoutGroup;
    LcMainGroup14: TdxLayoutGroup;
    LcMainItem20: TdxLayoutItem;
    LcMainItem21: TdxLayoutItem;
    LcMainItem22: TdxLayoutItem;
    LcMainItem23: TdxLayoutItem;
    LcMainGroup15: TdxLayoutGroup;
    LcMainGroup16: TdxLayoutGroup;
    dxDBEdit15: TdxDBEdit;
    dxDBEdit16: TdxDBEdit;
    dxDBEdit17: TdxDBEdit;
    dxDBEdit18: TdxDBEdit;
    QNazioniLK: TQuery;
    QNazioniLKID: TAutoIncField;
    QNazioniLKAbbrev: TStringField;
    QNazioniLKDescNazione: TStringField;
    QNazioniLKAreaGeografica: TStringField;
    QNazioniLKDescNazionalita: TStringField;
    QAnagNazione: TStringField;
    QAnagDomicilioNazione: TStringField;
    dxDBLookupEdit2: TdxDBLookupEdit;
    dxDBLookupEdit3: TdxDBLookupEdit;
    LcMainItem17: TdxLayoutItem;
    LcMainItem24: TdxLayoutItem;
    LcMainGroup9: TdxLayoutGroup;
    LcMainGroup13: TdxLayoutGroup;
    QTipiStatiLK: TQuery;
    QTipiStatiLKID: TAutoIncField;
    QTipiStatiLKTipoStato: TStringField;
    dxDBLookupEdit4: TdxDBLookupEdit;
    LCStatusGroup_Root: TdxLayoutGroup;
    LCStatus: TdxLayoutControl;
    LCStatusItem1: TdxLayoutItem;
    LCStatusItem2: TdxLayoutItem;
    BRelazioni: TSpeedButton;
    QAnagTipoStato: TStringField;
    LcMainGroup11: TdxLayoutGroup;
    LcMainGroup17: TdxLayoutGroup;
    LcMainGroup18: TdxLayoutGroup;
    LcMainGroup19: TdxLayoutGroup;
    LcMainGroup20: TdxLayoutGroup;
    LcMainGroup21: TdxLayoutGroup;
    QAnagLingue: TQuery;
    DsQAnagLingue: TDataSource;
    LcMainItem25: TdxLayoutItem;
    DBGLingue: TdxDBGrid;
    QLingueLK: TQuery;
    QLingueLKID: TAutoIncField;
    QLingueLKLingua: TStringField;
    QAnagLingueID: TAutoIncField;
    QAnagLingueIDAnagrafica: TIntegerField;
    QAnagLingueLingua: TStringField;
    QAnagLingueLivello: TSmallintField;
    QAnagLingueSoggiorno: TStringField;
    QAnagLingueLivelloScritto: TSmallintField;
    QAnagLingueIdLingua: TIntegerField;
    QLivConoscLingueLK: TQuery;
    QLivConoscLingueLKID: TAutoIncField;
    QLivConoscLingueLKLivelloConoscenza: TStringField;
    QAnagLingueLivelloConoscenza: TStringField;
    DBGLingueLinguaTab: TdxDBGridLookupColumn;
    DBGLingueLivelloConoscenza: TdxDBGridLookupColumn;
    LcMainGroup22: TdxLayoutGroup;
    LcMainGroup24: TdxLayoutGroup;
    LcMainGroup25: TdxLayoutGroup;
    LcMainGroup26: TdxLayoutGroup;
    QEspLav: TQuery;
    DsQEspLav: TDataSource;
    QEspLavAnnoDal: TSmallintField;
    QEspLavAnnoAl: TSmallintField;
    QEspLavRetribNettaMensile: TIntegerField;
    QEspLavTitoloMansione: TStringField;
    QEspLavCliente: TStringField;
    dxDBGridLayoutList1: TdxDBGridLayoutList;
    DBGEspLav: TdxDBGrid;
    QEspLavID: TAutoIncField;
    DBGEspLavAnnoDal: TdxDBGridMaskColumn;
    DBGEspLavAnnoAl: TdxDBGridMaskColumn;
    DBGEspLavRetribNettaMensile: TdxDBGridMaskColumn;
    DBGEspLavTitoloMansione: TdxDBGridMaskColumn;
    DBGEspLavCliente: TdxDBGridMaskColumn;
    LcMainItem26: TdxLayoutItem;
    QEspLavAttuale: TBooleanField;
    QAnagNoteRic: TQuery;
    DsQAnagNoteRic: TDataSource;
    LcMainGroup23: TdxLayoutGroup;
    LcMainItem28: TdxLayoutItem;
    DBGContatti: TdxDBGrid;
    DBGContattiData: TdxDBGridDateColumn;
    DBGContattiNote: TdxDBGridMaskColumn;
    DBGContattiCliente: TdxDBGridMaskColumn;
    LcMainItem29: TdxLayoutItem;
    LcMainItem30: TdxLayoutItem;
    LcMainItem31: TdxLayoutItem;
    LcMainItem32: TdxLayoutItem;
    LcMainItem33: TdxLayoutItem;
    LcMainItem34: TdxLayoutItem;
    LcMainItem35: TdxLayoutItem;
    LcMainItem36: TdxLayoutItem;
    LcMainItem37: TdxLayoutItem;
    LcMainItem38: TdxLayoutItem;
    LcMainItem39: TdxLayoutItem;
    LcMainGroup28: TdxLayoutGroup;
    LcMainGroup29: TdxLayoutGroup;
    BNuovo: TToolbarButton97;
    BModCand: TToolbarButton97;
    BEliminaCand: TToolbarButton97;
    BVediCv: TToolbarButton97;
    BPrec: TToolbarButton97;
    BSucc: TToolbarButton97;
    BOK: TToolbarButton97;
    BCan: TToolbarButton97;
    QAziendaAttuale: TQuery;
    DsQAziendaAttuale: TDataSource;
    QAziendaAttualeID: TAutoIncField;
    QAziendaAttualeAzienda: TStringField;
    QAziendaAttualeIndirizzo: TStringField;
    QAziendaAttualeCap: TStringField;
    QAziendaAttualeComune: TStringField;
    QAziendaAttualeProvincia: TStringField;
    dxDBEdit14: TdxDBEdit;
    dxDBEdit19: TdxDBEdit;
    dxDBEdit20: TdxDBEdit;
    dxDBEdit21: TdxDBEdit;
    dxDBEdit22: TdxDBEdit;
    dxDBEdit23: TdxDBEdit;
    dxDBEdit24: TdxDBEdit;
    QDiploma: TQuery;
    DsQDiploma: TDataSource;
    QDiplomaID: TAutoIncField;
    QDiplomaIDDiploma: TIntegerField;
    QDiplomaTesiTitoloArgomento: TMemoField;
    QDiplomiLK: TQuery;
    QDiplomiLKID: TAutoIncField;
    QDiplomiLKDescrizione: TStringField;
    QDiplomaDiploma: TStringField;
    dxDBLookupEdit5: TdxDBLookupEdit;
    LcMainItem40: TdxLayoutItem;
    dxDBMemo1: TdxDBMemo;
    LcMainItem41: TdxLayoutItem;
    UpdDiploma: TUpdateSQL;
    QLaurea: TQuery;
    DsQLaurea: TDataSource;
    UpdLaurea: TUpdateSQL;
    QLaureaID: TAutoIncField;
    QLaureaIDDiploma: TIntegerField;
    QLaureaTesiTitoloArgomento: TMemoField;
    QLaureaDiploma: TStringField;
    dxDBLookupEdit6: TdxDBLookupEdit;
    LcMainItem42: TdxLayoutItem;
    dxDBMemo2: TdxDBMemo;
    LcMainItem43: TdxLayoutItem;
    QMaster: TQuery;
    DsQMaster: TDataSource;
    UpdMaster: TUpdateSQL;
    QMasterID: TAutoIncField;
    QMasterArgomenti: TMemoField;
    dxDBMemo3: TdxDBMemo;
    LcMainItem44: TdxLayoutItem;
    QAnagAltreInfo: TQuery;
    DsQAnagAltreInfo: TDataSource;
    dxDBEdit25: TdxDBEdit;
    LcMainItem45: TdxLayoutItem;
    UpdAltreInfo: TUpdateSQL;
    Panel2: TPanel;
    LcMainItem46: TdxLayoutItem;
    LcMainGroup30: TdxLayoutGroup;
    BDiplomaOK: TToolbarButton97;
    BDiplomaCan: TToolbarButton97;
    Panel3: TPanel;
    LcMainItem47: TdxLayoutItem;
    LcMainGroup31: TdxLayoutGroup;
    BLaureaOK: TToolbarButton97;
    BLaureaCan: TToolbarButton97;
    QDiplomaIDAnagrafica: TIntegerField;
    QLaureaIDAnagrafica: TIntegerField;
    QDiplomaTipo: TStringField;
    QLaureaTipo: TStringField;
    Panel4: TPanel;
    LcMainItem48: TdxLayoutItem;
    BAbilitazOK: TToolbarButton97;
    BAbilitazCan: TToolbarButton97;
    QLaureeLK: TQuery;
    AutoIncField1: TAutoIncField;
    StringField1: TStringField;
    QAnagCellulareUfficio: TStringField;
    QAnagEmailUfficio: TStringField;
    QMasterTipologia: TStringField;
    QMasterIDAnagrafica: TIntegerField;
    Panel5: TPanel;
    LcMainItem49: TdxLayoutItem;
    BMasterOK: TToolbarButton97;
    BMasterCan: TToolbarButton97;
    BFile: TToolbarButton97;
    LcMainGroup32: TdxLayoutGroup;
    Panel6: TPanel;
    LcMainItem50: TdxLayoutItem;
    BLinguaOK: TToolbarButton97;
    BLinguaCan: TToolbarButton97;
    BLinguaNew: TToolbarButton97;
    BLinguaDel: TToolbarButton97;
    Panel7: TPanel;
    LcMainItem52: TdxLayoutItem;
    BEspLavDett: TSpeedButton;
    BEspLavNew: TSpeedButton;
    BEspLavDel: TSpeedButton;
    QTemp: TQuery;
    QDatiMod: TQuery;
    DsQDatiMod: TDataSource;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    QDatiIns: TQuery;
    DsQDatiIns: TDataSource;
    QDatiInsNominativo: TStringField;
    QDatiInsDataOra: TDateTimeField;
    QDatiModNominativo: TStringField;
    QDatiModData: TDateTimeField;
    QEspLavTotValRetribLK: TQuery;
    QEspLavTotValRetribLKIDEspLav: TIntegerField;
    QEspLavTotValRetribLKTotValore: TFloatField;
    QEspLavTotValRetrib: TFloatField;
    DBGEspLavColumn6: TdxDBGridColumn;
    QAziendaAttualeTelefono: TStringField;
    dxDBEdit26: TdxDBEdit;
    LcMainItem27: TdxLayoutItem;
    QAnagNoteRicID: TAutoIncField;
    QAnagNoteRicCliente: TStringField;
    QAnagNoteRicDataUltimoEvento: TDateTimeField;
    Panel8: TPanel;
    LcMainItem51: TdxLayoutItem;
    BDettHistory: TSpeedButton;
    LcMainGroup27: TdxLayoutGroup;
    LcMainGroup34: TdxLayoutGroup;
    SpeedButton1: TSpeedButton;
    Q1: TQuery;
    QClientiLK: TQuery;
    QClientiLKID: TAutoIncField;
    QClientiLKDescrizione: TStringField;
    QAnagCliente: TStringField;
    dxDBLookupEdit7: TdxDBLookupEdit;
    LcMainItem53: TdxLayoutItem;
    QAnagLingueLinguaTab: TStringField;
    bInsComm: TToolbarButton97;
    QReports: TQuery;
    QCReport: TQuery;
    QCReportid: TAutoIncField;
    frReport1: TfrReport;
    procedure BCustomizeClick(Sender: TObject);
    procedure QAnagCalcFields(DataSet: TDataSet);
    procedure BResidenzaClick(Sender: TObject);
    procedure BDomicilioClick(Sender: TObject);
    procedure QAnagBeforePost(DataSet: TDataSet);
    procedure BModCandClick(Sender: TObject);
    procedure DsQAnagStateChange(Sender: TObject);
    procedure BEliminaCandClick(Sender: TObject);
    procedure BOKClick(Sender: TObject);
    procedure BCanClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BPrecClick(Sender: TObject);
    procedure BSuccClick(Sender: TObject);
    procedure QAnagAfterOpen(DataSet: TDataSet);
    procedure QAnagBeforeClose(DataSet: TDataSet);
    procedure BRelazioniClick(Sender: TObject);
    procedure QAnagAfterPost(DataSet: TDataSet);
    procedure BEspLavDettClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QAnagAfterScroll(DataSet: TDataSet);
    procedure QDiplomaAfterPost(DataSet: TDataSet);
    procedure DsQDiplomaStateChange(Sender: TObject);
    procedure BDiplomaOKClick(Sender: TObject);
    procedure BDiplomaCanClick(Sender: TObject);
    procedure BLaureaOKClick(Sender: TObject);
    procedure BLaureaCanClick(Sender: TObject);
    procedure DsQLaureaStateChange(Sender: TObject);
    procedure QLaureaAfterPost(DataSet: TDataSet);
    procedure QDiplomaAfterInsert(DataSet: TDataSet);
    procedure QLaureaAfterInsert(DataSet: TDataSet);
    procedure QMasterAfterPost(DataSet: TDataSet);
    procedure BAbilitazOKClick(Sender: TObject);
    procedure BAbilitazCanClick(Sender: TObject);
    procedure QAnagAltreInfoAfterPost(DataSet: TDataSet);
    procedure DsQAnagAltreInfoStateChange(Sender: TObject);
    procedure QMasterAfterInsert(DataSet: TDataSet);
    procedure BMasterOKClick(Sender: TObject);
    procedure BMasterCanClick(Sender: TObject);
    procedure DsQMasterStateChange(Sender: TObject);
    procedure BNuovoClick(Sender: TObject);
    procedure BVediCvClick(Sender: TObject);
    procedure BFileClick(Sender: TObject);
    procedure DsQAnagLingueStateChange(Sender: TObject);
    procedure QAnagLingueAfterPost(DataSet: TDataSet);
    procedure BLinguaNewClick(Sender: TObject);
    procedure BLinguaDelClick(Sender: TObject);
    procedure BLinguaOKClick(Sender: TObject);
    procedure BLinguaCanClick(Sender: TObject);
    procedure QAnagLingueAfterInsert(DataSet: TDataSet);
    procedure BEspLavNewClick(Sender: TObject);
    procedure BEspLavDelClick(Sender: TObject);
    procedure QAnagLingueAfterEdit(DataSet: TDataSet);
    procedure QDiplomaAfterEdit(DataSet: TDataSet);
    procedure QLaureaAfterEdit(DataSet: TDataSet);
    procedure QMasterAfterEdit(DataSet: TDataSet);
    procedure BDettHistoryClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure bInsCommClick(Sender: TObject);
  private
    xAnagLingueIns, xEspLavIns, xDiplomaIns, xLaureaIns, xMasterIns: boolean;
    procedure SalvaAziendaAttuale;
  public
    { Public declarations }
  end;

var
  ASASchedaCandForm: TASASchedaCandForm;

implementation

uses ModuloDati, uRelazioni, uDettagliCand, Main, uUtilsVarie, uAnagFile,
  uDettHistory, uInsEvento, uAnagFile2, ElencoRicPend, MDRicerche,
  ModuloDati2;

{$R *.DFM}

procedure TASASchedaCandForm.BCustomizeClick(Sender: TObject);
begin
  //lcMain.Customization := True;
//     QCReport.Close;
//     QCReport.ParamByName('ID').AsInteger := Data.TAnagraficaID.AsInteger;
//     QCReport.Open;
  frReport1.ShowReport;
end;

procedure TASASchedaCandForm.QAnagCalcFields(DataSet: TDataSet);
var xAnno, xMese, xgiorno: Word;
begin
  if QAnagDataNascita.asString = '' then
    QAnagEta.asString := ''
  else begin
    DecodeDate((Date - QAnagDataNascita.Value), xAnno, xMese, xgiorno);
    // TAnagraficaEta.Value:=StrToInt(copy(xAnno,3,2));
    QAnagEta.Value := copy(IntToStr(xAnno), 3, 2);
  end;
end;

procedure TASASchedaCandForm.BResidenzaClick(Sender: TObject);
begin
  LcMainResidenza.Visible := True;
  LcMainDomicilio.Visible := False;
end;

procedure TASASchedaCandForm.BDomicilioClick(Sender: TObject);
begin
  LcMainResidenza.Visible := False;
  LcMainDomicilio.Visible := True;
end;

procedure TASASchedaCandForm.QAnagBeforePost(DataSet: TDataSet);
begin
  if QAnagIDTipoStato.Value = 1 then QAnagIDStato.Value := 27;
  if QAnagIDTipoStato.Value = 2 then QAnagIDStato.Value := 28;
  if QAnagIDTipoStato.Value = 3 then QAnagIDStato.Value := 29;
  if QAnagIDTipoStato.Value = 5 then QAnagIDStato.Value := 30;
end;

procedure TASASchedaCandForm.BModCandClick(Sender: TObject);
begin
  QAnag.Edit;
end;

procedure TASASchedaCandForm.DsQAnagStateChange(Sender: TObject);
var b: boolean;
begin
  b := DsQAnag.State in [dsEdit, dsInsert];
  BNuovo.Visible := not b;
  BEliminaCand.Visible := not b;
  //BModCand.Visible:=not b;
  //lcMain.enabled:=b;
  BOK.Visible := b;
  BCan.Visible := b;
end;

procedure TASASchedaCandForm.BEliminaCandClick(Sender: TObject);
begin
  MainForm.TbBAnagDelClick(nil);
  Close;
end;

procedure TASASchedaCandForm.BOKClick(Sender: TObject);
begin
  QAnag.Post;
end;

procedure TASASchedaCandForm.BCanClick(Sender: TObject);
begin
  QAnag.Cancel;
end;

procedure TASASchedaCandForm.FormShow(Sender: TObject);
begin
  // posizionati sul soggetto selezionato
  QAnag.Close;
  QAnag.ParamByName('xIDAnag').asInteger := Data.TAnagraficaID.Value;
  QAnag.Open;
  QAziendaAttuale.Open;
  QDiploma.Open;
  QLaurea.Open;
  QMaster.Open;
  QAnagAltreInfo.Open;
  // pulsanti prec. e succ.
  BPrec.Enabled := not Data.TAnagrafica.BOF;
  BSucc.Enabled := not Data.TAnagrafica.EOF;
end;

procedure TASASchedaCandForm.BPrecClick(Sender: TObject);
begin
  SalvaAziendaAttuale;
  if Data.TAnagrafica.BOF then exit;
  Data.TAnagrafica.Prior;
  QAnag.Close;
  QAnag.ParamByName('xIDAnag').asInteger := Data.TAnagraficaID.Value;
  QAnag.Open;
  BPrec.Enabled := not Data.TAnagrafica.BOF;
  BSucc.Enabled := not Data.TAnagrafica.EOF;
end;

procedure TASASchedaCandForm.BSuccClick(Sender: TObject);
begin
  SalvaAziendaAttuale;
  if Data.TAnagrafica.EOF then exit;
  Data.TAnagrafica.Next;
  QAnag.Close;
  QAnag.ParamByName('xIDAnag').asInteger := Data.TAnagraficaID.Value;
  QAnag.Open;
  BPrec.Enabled := not Data.TAnagrafica.BOF;
  BSucc.Enabled := not Data.TAnagrafica.EOF;
end;

procedure TASASchedaCandForm.QAnagAfterOpen(DataSet: TDataSet);
begin
  // apri tabelle collegate
  QEspLav.Open;
  QAnagLingue.Open;
  QAnagNoteRic.Open;
  QDatiIns.Open;
  QDatiMod.Open;
end;

procedure TASASchedaCandForm.QAnagBeforeClose(DataSet: TDataSet);
begin
  QEspLav.Close;
  QAnagLingue.Close;
  QAnagNoteRic.Close;
end;

procedure TASASchedaCandForm.BRelazioniClick(Sender: TObject);
begin
  RelazioniForm := TRelazioniForm.create(self);
  //RelazioniForm.Top:=ASASchedaCandForm.Top+80;
  //RelazioniForm.Left:=LcStatus.Left+BRelazioni.Left+100;
  RelazioniForm.ShowModal;
  RelazioniForm.Free;
end;

procedure TASASchedaCandForm.QAnagAfterPost(DataSet: TDataSet);
begin
  QAnag.ApplyUpdates;
  QAnag.CommitUpdates;
  Log_Operation(MainForm.xIDUtenteAttuale, 'Anagrafica', Data.TAnagraficaID.Value, 'U');
  QDatiMod.Close;
  QDatiMod.Open;
end;

procedure TASASchedaCandForm.BEspLavDettClick(Sender: TObject);
var xID: integer;
begin
  if QEspLav.IsEmpty then exit;
  DettagliCandForm := TDettagliCandForm.create(self);
  DettagliCandForm.QEspLav.ParamByName('xID').asInteger := QEspLavID.Value;
  DettagliCandForm.QEspLav.Open;
  DettagliCandForm.ShowModal;
  DettagliCandForm.Free;
  QEspLavTotValRetribLK.Close;
  QEspLavTotValRetribLK.Open;
  xID := QEspLavID.Value;
  QEspLav.Close;
  QEspLav.Open;
  QEspLav.Locate('ID', xID, []);
end;

procedure TASASchedaCandForm.SalvaAziendaAttuale;
begin
  // PER ORA NON SALVA: D� errore di "stack overflow"
  //if DsQAziendaAttuale.state=dsedit then
  //     QAziendaAttuale.Post;
end;

procedure TASASchedaCandForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  SalvaAziendaAttuale;
end;

procedure TASASchedaCandForm.QAnagAfterScroll(DataSet: TDataSet);
begin
  if DsQDiploma.state = dsEdit then QDiploma.Post;
end;

procedure TASASchedaCandForm.QDiplomaAfterPost(DataSet: TDataSet);
var xID: integer;
begin
  QDiploma.ApplyUpdates;
  QDiploma.CommitUpdates;
  xID := QDiplomaID.Value;
  QDiploma.Close;
  QDiploma.Open;
  if xDiplomaIns then begin
    QDiploma.Last;
    Log_Operation(MainForm.xIDUtenteAttuale, 'TitoliStudio', QDiplomaID.Value, 'I');
  end else begin
    QDiploma.Locate('ID', xID, []);
    Log_Operation(MainForm.xIDUtenteAttuale, 'TitoliStudio', QDiplomaID.Value, 'U');
  end;
  QDatiMod.Close;
  QDatiMod.Open;
end;

procedure TASASchedaCandForm.DsQDiplomaStateChange(Sender: TObject);
var b: boolean;
begin
  b := DsQDiploma.State in [dsEdit, dsInsert];
  BDiplomaOK.Enabled := b;
  BDiplomaCan.Enabled := b;
end;

procedure TASASchedaCandForm.BDiplomaOKClick(Sender: TObject);
begin
  QDiploma.Post;
end;

procedure TASASchedaCandForm.BDiplomaCanClick(Sender: TObject);
begin
  QDiploma.Cancel;
end;

procedure TASASchedaCandForm.BLaureaOKClick(Sender: TObject);
begin
  QLaurea.Post;
end;

procedure TASASchedaCandForm.BLaureaCanClick(Sender: TObject);
begin
  QLaurea.Cancel;
end;

procedure TASASchedaCandForm.DsQLaureaStateChange(Sender: TObject);
var b: boolean;
begin
  b := DsQLaurea.State in [dsEdit, dsInsert];
  BLaureaOK.Enabled := b;
  BLaureaCan.Enabled := b;
end;

procedure TASASchedaCandForm.QLaureaAfterPost(DataSet: TDataSet);
var xID: integer;
begin
  QLaurea.ApplyUpdates;
  QLaurea.CommitUpdates;
  xID := QLaureaID.Value;
  QLaurea.Close;
  QLaurea.Open;
  if xLaureaIns then begin
    QLaurea.Last;
    Log_Operation(MainForm.xIDUtenteAttuale, 'TitoliStudio', QLaureaID.Value, 'I');
  end else begin
    QLaurea.Locate('ID', xID, []);
    Log_Operation(MainForm.xIDUtenteAttuale, 'TitoliStudio', QLaureaID.Value, 'U');
  end;
  QDatiMod.Close;
  QDatiMod.Open;
end;

procedure TASASchedaCandForm.QDiplomaAfterInsert(DataSet: TDataSet);
begin
  QDiplomaIDAnagrafica.Value := QAnagID.Value;
  QDiplomaTipo.Value := 'Diploma';
  xDiplomaIns := True;
end;

procedure TASASchedaCandForm.QLaureaAfterInsert(DataSet: TDataSet);
begin
  QLaureaIDAnagrafica.Value := QAnagID.Value;
  QLaureaTipo.Value := 'Laurea';
  xLaureaIns := True;
end;

procedure TASASchedaCandForm.QMasterAfterPost(DataSet: TDataSet);
var xID: integer;
begin
  QMaster.ApplyUpdates;
  QMaster.CommitUpdates;
  xID := QMasterID.Value;
  QMaster.Close;
  QMaster.Open;
  if xMasterIns then begin
    QMaster.Last;
    Log_Operation(MainForm.xIDUtenteAttuale, 'CorsiExtra', QMasterID.Value, 'I');
  end else begin
    QMaster.Locate('ID', xID, []);
    Log_Operation(MainForm.xIDUtenteAttuale, 'CorsiExtra', QMasterID.Value, 'U');
  end;
  QDatiMod.Close;
  QDatiMod.Open;
end;

procedure TASASchedaCandForm.BAbilitazOKClick(Sender: TObject);
begin
  QAnagAltreInfo.Post;
end;

procedure TASASchedaCandForm.BAbilitazCanClick(Sender: TObject);
begin
  QAnagAltreInfo.Cancel;
end;

procedure TASASchedaCandForm.QAnagAltreInfoAfterPost(DataSet: TDataSet);
begin
  QAnagAltreInfo.ApplyUpdates;
  QAnagAltreInfo.CommitUpdates;
  Log_Operation(MainForm.xIDUtenteAttuale, 'AnagAltreInfo', Data.TAnagraficaID.value, 'U');
  QDatiMod.Close;
  QDatiMod.Open;
end;

procedure TASASchedaCandForm.DsQAnagAltreInfoStateChange(Sender: TObject);
var b: boolean;
begin
  b := DsQAnagAltreInfo.State in [dsEdit, dsInsert];
  BAbilitazOK.Enabled := b;
  BAbilitazCan.Enabled := b;
end;

procedure TASASchedaCandForm.QMasterAfterInsert(DataSet: TDataSet);
begin
  QMasterIDAnagrafica.Value := QAnagID.Value;
  QMasterTipologia.Value := 'Master';
  xMasterIns := True;
end;

procedure TASASchedaCandForm.BMasterOKClick(Sender: TObject);
begin
  QMaster.Post;
end;

procedure TASASchedaCandForm.BMasterCanClick(Sender: TObject);
begin
  QMaster.Cancel;
end;

procedure TASASchedaCandForm.DsQMasterStateChange(Sender: TObject);
var b: boolean;
begin
  b := DsQMaster.State in [dsEdit, dsInsert];
  BMasterOK.Enabled := b;
  BMasterCan.Enabled := b;
end;

procedure TASASchedaCandForm.BNuovoClick(Sender: TObject);
begin
  MainForm.TbBAnagNewClick(nil);
  QAnag.Close;
  QAnag.ParamByName('xIDAnag').asInteger := Data.TAnagraficaID.Value;
  QAnag.Open;
  QAziendaAttuale.Open;
  QDiploma.Open;
  QLaurea.Open;
  QMaster.Open;
  QAnagAltreInfo.Open;
  // pulsanti prec. e succ.
  BPrec.Enabled := not Data.TAnagrafica.BOF;
  BSucc.Enabled := not Data.TAnagrafica.EOF;
end;

procedure TASASchedaCandForm.BVediCvClick(Sender: TObject);
begin
  //ApriCV(Data.TAnagraficaID.value);
  ApriFileWord(Data.TAnagraficaCVNumero.asString);
end;

procedure TASASchedaCandForm.BFileClick(Sender: TObject);
begin
  FileSoggetto2Form := TFileSoggetto2Form.create(self);
  FileSoggetto2Form.ShowModal;
  FileSoggetto2Form.Free
end;

procedure TASASchedaCandForm.DsQAnagLingueStateChange(Sender: TObject);
var b: boolean;
begin
  b := DsQAnagLingue.State in [dsEdit, dsInsert];
  BLinguaNew.Enabled := not b;
  BLinguaDel.Enabled := not b;
  BLinguaOK.Enabled := b;
  BLinguaCan.Enabled := b;
end;

procedure TASASchedaCandForm.QAnagLingueAfterPost(DataSet: TDataSet);
var xID: integer;
begin
  xID := QAnagLingueID.Value;
  QAnagLingue.Close;
  QAnagLingue.Open;
  if xAnagLingueIns then begin
    QAnagLingue.Last;
    Log_Operation(MainForm.xIDUtenteAttuale, 'LingueConosciute', QAnagLingueID.Value, 'I');
  end else begin
    QAnagLingue.Locate('ID', xID, []);
    Log_Operation(MainForm.xIDUtenteAttuale, 'LingueConosciute', QAnagLingueID.Value, 'U');
  end;
  QDatiMod.Close;
  QDatiMod.Open;
end;

procedure TASASchedaCandForm.BLinguaNewClick(Sender: TObject);
begin
  QAnagLingue.Insert;
end;

procedure TASASchedaCandForm.BLinguaDelClick(Sender: TObject);
begin
  if MessageDlg('Sei sicuro di voler cancellare la lingua selezionata ?', mtWarning, [mbYes, mbNo], 0) = mrNo then exit;
  QAnagLingue.Delete;
end;

procedure TASASchedaCandForm.BLinguaOKClick(Sender: TObject);
begin
  QAnagLingue.Post;
end;

procedure TASASchedaCandForm.BLinguaCanClick(Sender: TObject);
begin
  QAnagLingue.Cancel;
end;

procedure TASASchedaCandForm.QAnagLingueAfterInsert(DataSet: TDataSet);
begin
  QAnagLingueIDAnagrafica.Value := QAnagID.Value;
  xAnagLingueIns := False;
end;

procedure TASASchedaCandForm.BEspLavNewClick(Sender: TObject);
begin
  DettagliCandForm := TDettagliCandForm.create(self);
  DettagliCandForm.QEspLav.ParamByName('xID').asInteger := QEspLavID.Value;
  DettagliCandForm.QEspLav.Open;
  // inserimento nuova
  //DettagliCandForm.QEspLav.InsertRecord([QAnagID.value]);
  DettagliCandForm.QEspLav.Insert;
  //DettagliCandForm.QEspLav.Post;
  DettagliCandForm.ShowModal;
  DettagliCandForm.Free;
  QEspLavTotValRetribLK.Close;
  QEspLavTotValRetribLK.Open;
  QEspLav.Close;
  QEspLav.Open;
end;

procedure TASASchedaCandForm.BEspLavDelClick(Sender: TObject);
begin
  if MessageDlg('Sei sicuro di voler cancellare l''esperienza lavorativa selezionata ?', mtWarning, [mbYes, mbNo], 0) = mrNo then exit;
  DettagliCandForm := TDettagliCandForm.create(self);
  DettagliCandForm.QEspLav.ParamByName('xID').asInteger := QEspLavID.Value;
  DettagliCandForm.QEspLav.Open;
  // inserimento nuova
  DettagliCandForm.QEspLav.Delete;
  DettagliCandForm.QEspLav.ApplyUpdates;
  DettagliCandForm.QEspLav.CommitUpdates;
  DettagliCandForm.Free;
  QEspLav.Close;
  QEspLav.Open;
  QAziendaAttuale.Close;
  QAziendaAttuale.Open;
end;

procedure TASASchedaCandForm.QAnagLingueAfterEdit(DataSet: TDataSet);
begin
  xAnagLingueIns := False;
end;

procedure TASASchedaCandForm.QDiplomaAfterEdit(DataSet: TDataSet);
begin
  xDiplomaIns := False;
end;

procedure TASASchedaCandForm.QLaureaAfterEdit(DataSet: TDataSet);
begin
  xLaureaIns := False;
end;

procedure TASASchedaCandForm.QMasterAfterEdit(DataSet: TDataSet);
begin
  xMasterIns := False;
end;

procedure TASASchedaCandForm.BDettHistoryClick(Sender: TObject);
begin
  DettHistoryForm := TDettHistoryForm.create(self);
  with DettHistoryForm do begin
    if QAnagNoteRicID.Value > 0 then
      QDettHistory.SQL.text := 'select EBC_Eventi.Evento, Nominativo, Storico.* ' +
        'from storico, EBC_Eventi, Users, EBC_Ricerche ' +
        'where storico.IDEvento = EBC_Eventi.ID ' +
        '  and storico.IDRicerca = EBC_Ricerche.ID ' +
        '  and storico.IDUtente = Users.ID ' +
        ' and EBC_Ricerche.IDCliente=' + QAnagNoteRicID.AsString
    else
      QDettHistory.SQL.text := 'select EBC_Eventi.Evento, Nominativo, Storico.* ' +
        'from storico, EBC_Eventi, Users ' +
        'where storico.IDEvento = EBC_Eventi.ID ' +
        '  and storico.IDUtente = Users.ID ' +
        '  and (storico.IDRicerca is null or storico.IDRicerca=0)';
    QDettHistory.SQL.Add('and Storico.IDAnagrafica=' + QAnagID.AsString);
    QDettHistory.Open;
  end;
  DettHistoryForm.Caption := 'Dettaglio history - Cliente: ' + QAnagNoteRicCliente.Value;
  DettHistoryForm.ShowModal;
  DettHistoryForm.Free;
  QAnagNoteRic.Close;
  QAnagNoteRic.Open;
end;

procedure TASASchedaCandForm.SpeedButton1Click(Sender: TObject);
var xIDRic, xIDCliente: integer;
begin
  ASAInsEventoForm := TASAInsEventoForm.create(self);
  ASAInsEventoForm.ShowModal;
  if ASAInsEventoForm.ModalResult = mrOK then begin
    if (ASAInsEventoForm.CBAssociaCommessa.Checked) and (not ASAInsEventoForm.QCommesse.IsEmpty) then begin
      xIDRic := ASAInsEventoForm.QCommesseID.Value;
      xIDCliente := ASAInsEventoForm.QCommesseIDCliente.value;
    end else begin
      xIDRic := 0;
      xIDCliente := 0;
    end;
    QTemp.Close;
    QTemp.SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
      'values (:xIDAnagrafica,:xIDEvento,:xDataEvento,:xAnnotazioni,:xIDRicerca,:xIDUtente,:xIDCliente)';
    QTemp.ParamByName('xIDAnagrafica').asInteger := QAnagID.Value;
    QTemp.ParamByName('xIDEvento').asInteger := ASAInsEventoForm.QEventiMaxID.Value;
    QTemp.ParamByName('xDataEvento').asDateTime := ASAInsEventoForm.DeDataEvento.Date;
    QTemp.ParamByName('xAnnotazioni').asString := ASAInsEventoForm.EDesc.text;
    QTemp.ParamByName('xIDRicerca').asInteger := xIDRic;
    QTemp.ParamByName('xIDCliente').asInteger := xIDCliente;
    QTemp.ParamByName('xIDUtente').asInteger := MainForm.xIDUtenteAttuale;
    QTemp.ExecSQL;
    if ASAInsEventoForm.CBModStato.Checked then begin
      Q1.Close;
      Q1.SQL.text := 'select IDTipoStato from EBC_Stati where ID=' + ASAInsEventoForm.QEventiIDAStato.asString;
      Q1.Open;
      QTemp.Close;
      QTemp.SQL.text := 'update anagrafica set IDStato=:xIDStato, IDTipoStato=:xIDTipoStato ' +
        ' where ID=' + QAnagID.AsString;
      Qtemp.ParamByName('xIDStato').asInteger := ASAInsEventoForm.QEventiIDAStato.Value;
      Qtemp.ParamByName('xIDTipoStato').asInteger := Q1.FieldByName('IDTipoStato').asInteger;
      Q1.Close;
      QTemp.ExecSQL;
      QAnag.Close;
      QAnag.ParamByName('xIDAnag').asInteger := Data.TAnagraficaID.Value;
      QAnag.Open;
      QAziendaAttuale.Open;
      QDiploma.Open;
      QLaurea.Open;
      QMaster.Open;
      QAnagAltreInfo.Open;
    end;
    QAnagNoteRic.Close;
    QAnagNoteRic.Open;
  end;
  ASAInsEventoForm.Free;
end;

procedure TASASchedaCandForm.bInsCommClick(Sender: TObject);
var xIDEvento, xIDClienteBlocco: integer;
  xVai: boolean;
  xRic, xLivProtez, xDicMess, xPassword, xUtenteResp, xClienteBlocco: string;
  QLocal: TQuery;
begin
  ElencoRicPendForm := TElencoRicPendForm.create(self);
  ElencoRicPendForm.ShowModal;
  DataRicerche.TRicAnagIDX.Open;
  if ElencoRicPendForm.ModalResult = mrOK then begin
    xVai := true;
    // controllo se � gi� associato ad un'altra ricerca per la STESSA azienda
    Data.Q1.Close;
    Data.Q1.SQL.clear;
    Data.Q1.SQl.Add('select EBC_Ricerche.Progressivo,EBC_StatiRic.StatoRic from EBC_CandidatiRicerche,EBC_Ricerche,EBC_StatiRic ');
    Data.Q1.SQl.Add('where EBC_CandidatiRicerche.IDRicerca=EBC_Ricerche.ID and EBC_Ricerche.IDStatoRic=EBC_StatiRic.ID');
    Data.Q1.SQl.Add('and EBC_CandidatiRicerche.IDAnagrafica=:xIDAnag and EBC_Ricerche.IDCliente=:xIDCliente');
    Data.Q1.Prepare;
    Data.Q1.ParamByName('xIDAnag').asInteger := data.TAnagraficaID.Value;
    Data.Q1.ParamByName('xIDCliente').asInteger := ElencoRicPendForm.QRicAttiveIDCliente.Value;
    Data.Q1.Open;
    if not Data.Q1.IsEmpty then begin
      xRic := '';
      while not Data.Q1.EOF do begin xRic := xRic + 'N�' + Data.Q1.FieldByName('Progressivo').asString + ' (' + Data.Q1.FieldByName('StatoRic').asString + ') '; Data.Q1.Next; end;
      if MessageDlg('Il soggetto � associato gi� alle seguenti ricerche per lo stesso cliente: ' + chr(13) + xRic + chr(13) + 'PROCEDERE COMUNQUE ?', mtWarning, [mbYes, mbNo], 0) = mrNo then xVai := False;
    end;
    Data.Q1.Close;

    // controllo incompatibilit�
    if IncompAnagCli(Data.TAnagraficaID.Value, ElencoRicPendForm.QRicAttiveIDCliente.Value) then
      if MessageDlg('Il soggetto risulta INCOMPATIBILE con questo cliente o con un cliente associato' + chr(13) +
        'PROCEDERE COMUNQUE ?', mtWarning, [mbYes, mbNo], 0) = mrNo then begin
        DataRicerche.TRicAnagIDX.Close;
        ElencoRicPendForm.Free;
        exit;
      end;
    // controllo blocco livello 1 o 2
    {xIDClienteBlocco:=CheckAnagInseritoBlocco1(Data.TAnagraficaID.Value);
    if xIDClienteBlocco>0 then begin
         xLivProtez:=copy(IntToStr(xIDClienteBlocco),1,1);
         if xLivProtez='1' then begin
              xIDClienteBlocco:=xIDClienteBlocco-10000;
              xClienteBlocco:=GetDescCliente(xIDClienteBlocco);
              xDicMess:='Stai inserendo in commessa un candidato appartenente ad una azienda ('+xClienteBlocco+') con blocco di livello 1, ';
         end else begin
              xIDClienteBlocco:=xIDClienteBlocco-20000;
              xClienteBlocco:=GetDescCliente(xIDClienteBlocco);
              xDicMess:='Stai inserendo in commessa un candidato appartenente ad una azienda ATTIVA ('+xClienteBlocco+'), ';
         end;
         if MessageDlg('ATTENZIONE!! PROTEZIONE CLIENTI LIVELLO '+xLivProtez+chr(13)+xDicMess+
              'protetta dalle normative interne, se sei autorizzato premi SI, altrimenti premi NO. '+
              'Se si prosegue, l''operazione verr� registrata nello storico del soggetto come forzatura. '+
              'Verr� inviato un messaggio di promemoria al responsabile diretto',mtWarning, [mbYes,mbNo],0)=mrNo then xVai:=False
         else begin
              // pwd
              xUtenteResp:=GetDescUtenteResp(MainForm.xIDUtenteAttuale);
              if not InputQuery('Forzatura blocco livello '+xLivProtez,'Password di '+xUtenteResp,xPassword) then exit;
              if not CheckPassword(xPassword,GetPwdUtenteResp(MainForm.xIDUtenteAttuale)) then begin
                   MessageDlg('Password errata',mtError, [mbOK],0);
                   xVai:=False;
              end;
              if xVai then begin
                   // promemoria al resp.
                   QLocal:=CreateQueryFromString('insert into Promemoria (IDUtente,IDUtenteDa,DataIns,Testo,Evaso,DataDaLeggere) '+
                        'values (:xIDUtente,:xIDUtenteDa,:xDataIns,:xTesto,:xEvaso,:xDataDaLeggere)');
                   QLocal.ParamByName('xIDUtente').asInteger:=GetIDUtenteResp(MainForm.xIDUtenteAttuale);
                   QLocal.ParamByName('xIDUtenteDa').asInteger:=MainForm.xIDUtenteAttuale;
                   QLocal.ParamByName('xDataIns').asDateTime:=Date;
                   QLocal.ParamByName('xTesto').asString:='forzatura livello '+xLivProtez+' per CV n� '+Data.TAnagraficaCVNumero.AsString;
                   QLocal.ParamByName('xEvaso').asBoolean:=False;
                   QLocal.ParamByName('xDataDaLeggere').asDateTime:=Date;
                   QLocal.ExecSQL;
                   // registra nello storico soggetto
                   xIDEvento:=GetIDEvento('forzatura Blocco livello '+xLivProtez);
                   if xIDEvento>0 then begin
                        with Data.Q1 do begin
                             close;
                             SQL.text:='insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) '+
                                  'values (:xIDAnagrafica,:xIDEvento,:xDataEvento,:xAnnotazioni,:xIDRicerca,:xIDUtente,:xIDCliente)';
                             ParamByName('xIDAnagrafica').asInteger:=Data.TAnagraficaID.Value;
                             ParamByName('xIDEvento').asInteger:=xIDevento;
                             ParamByName('xDataEvento').asDateTime:=Date;
                             ParamByName('xAnnotazioni').asString:='cliente: '+xClienteBlocco;
                             ParamByName('xIDRicerca').asInteger:=DataRicerche.TRicerchePendID.Value;
                             ParamByName('xIDUtente').asInteger:=MainForm.xIDUtenteAttuale;
                             ParamByName('xIDCliente').asInteger:=xIDClienteBlocco;
                             ExecSQL;
                        end;
                   end;
              end;
         end;
    end;}
    if xvai then begin
      if not DataRicerche.TRicAnagIDX.FindKey([ElencoRicPendForm.QRicAttiveID.Value, Data.TAnagraficaID.Value]) then
       Data.DB.BeginTrans;
      try


        Data.Q1.Close;
        Data.Q1.SQL.text := 'insert into EBC_CandidatiRicerche (IDRicerca,IDAnagrafica,Escluso,Stato,DataIns) ' +
          'values (:xIDRicerca,:xIDAnagrafica,:xEscluso,:xStato,:xDataIns)';
        Data.Q1.ParambyName('xIDRicerca').asInteger := ElencoRicPendForm.QRicAttiveID.Value;
        Data.Q1.ParambyName('xIDAnagrafica').asInteger := Data.TAnagraficaID.Value;
        Data.Q1.ParambyName('xEscluso').asBoolean := False;
        Data.Q1.ParambyName('xStato').asString := 'inserito direttam. in data ' + DateToStr(Date);
        Data.Q1.ParambyName('xDataIns').asDateTime := Date;
        Data.Q1.ExecSQL;
        // storico
        Data.Q1.SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
          'values (:xIDAnagrafica,:xIDEvento,:xDataEvento,:xAnnotazioni,:xIDRicerca,:xIDUtente,:xIDCliente)';
        Data.Q1.ParambyName('xIDAnagrafica').asInteger := Data.TAnagraficaID.Value;
        Data.Q1.ParambyName('xIDEvento').asInteger := 58;
        Data.Q1.ParambyName('xDataEvento').asDateTime := Date;
        Data.Q1.ParambyName('xAnnotazioni').asString := 'inserito nella ric.' + ElencoRicPendForm.QRicAttiveProgressivo.Value + ' (' + ElencoRicPendForm.QRicAttiveCliente.value + ')';
        Data.Q1.ParambyName('xIDRicerca').asInteger := ElencoRicPendForm.QRicAttiveID.Value;
        Data.Q1.ParambyName('xIDUtente').asInteger := MainForm.xIDUtenteAttuale;
        Data.Q1.ParambyName('xIDCliente').asInteger := ElencoRicPendForm.QRicAttiveIDCliente.Value;
        Data.Q1.ExecSQL;
        // Anagrafica: cambio stato
        Data.Q1.SQL.text := 'update Anagrafica set IDTipoStato=1,IDStato=27 where ID=' + Data.TAnagraficaID.asString;
        Data.Q1.ExecSQL;
        Data.DB.CommitTrans;
      except
        Data.DB.RollbackTrans;
        MessageDlg('ERRORE SUL DATABASE:  operazione non completata', mtError, [mbOK], 0);
      end;
    end;
  end;
  DataRicerche.TRicAnagIDX.Close;
  Data2.QAnagRicerche.Close;
  Data2.QAnagRicerche.Open;
  ElencoRicPendForm.Free;
end;

end.

