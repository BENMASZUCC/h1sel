unit uASAcommessa;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     TB97,ExtCtrls,dxLayoutControl,dxCntner,dxEditor,dxExEdtr,dxDBEdtr,
     dxDBELib,cxControls,Db,DBTables,dxLayoutLookAndFeels,dxEdLib,
     dxDBTLCl,dxGrClms,dxTL,dxDBCtrl,dxDBGrid, ADODB, U_ADOLinkCl;

type
     TASAcommessaForm=class(TForm)
          Panel1: TPanel;
          BCustomize: TToolbarButton97;
          BNuovo: TToolbarButton97;
          BEliminaRic: TToolbarButton97;
          BSchedaCand: TToolbarButton97;
          BOK: TToolbarButton97;
          BCan: TToolbarButton97;
          BInsCandElenco: TToolbarButton97;
          PanDatiRicerca: TPanel;
          PanCandidati: TPanel;
          BEliminaCand: TToolbarButton97;
          dxLayoutLookAndFeelList1: TdxLayoutLookAndFeelList;
          dxLayoutStandardLookAndFeel1: TdxLayoutStandardLookAndFeel;
          dxLayoutOfficeLookAndFeel1: TdxLayoutOfficeLookAndFeel;
          dxLayoutWebLookAndFeel1: TdxLayoutWebLookAndFeel;
    QRicerca: TQuery;
          DsQRicerca: TDataSource;
          QStatiRicLK: TQuery;
          QStatiRicLKID: TAutoIncField;
          QStatiRicLKStatoRic: TStringField;
    QRicercaID: TAutoIncField;
    QRicercaIDCliente: TIntegerField;
    QRicercaIDUtente: TIntegerField;
    QRicercaProgressivo: TStringField;
    QRicercaDataInizio: TDateTimeField;
    QRicercaDataFine: TDateTimeField;
    QRicercaStato: TStringField;
    QRicercaConclusa: TBooleanField;
    QRicercaIDStatoRic: TIntegerField;
    QRicercaDallaData: TDateTimeField;
    QRicercaIDArea: TIntegerField;
    QRicercaIDMansione: TIntegerField;
    QRicercaIDSpec: TIntegerField;
    QRicercaNumRicercati: TSmallintField;
    QRicercaNote: TMemoField;
    QRicercaIDFattura: TIntegerField;
    QRicercaSospesa: TBooleanField;
    QRicercaSospesaDal: TDateTimeField;
    QRicercaStipendioNetto: TFloatField;
    QRicercaStipendioLordo: TFloatField;
    QRicercaFatturata: TBooleanField;
    QRicercaSpecifiche: TMemoField;
    QRicercaSpecifEst: TMemoField;
    QRicercaTipo: TStringField;
    QRicercaIDSede: TIntegerField;
    QRicercaIDContattoCli: TIntegerField;
    QRicercaIDUtente2: TIntegerField;
    QRicercaIDUtente3: TIntegerField;
    QRicercaDataPrevChiusura: TDateTimeField;
    QRicercaggDiffApRic: TSmallintField;
    QRicercaggDiffUcRic: TSmallintField;
    QRicercaggDiffColRic: TSmallintField;
    QRicercaIDOfferta: TIntegerField;
    QRicercaProvvigione: TBooleanField;
    QRicercaProvvigionePerc: TIntegerField;
    QRicercaQuandoPagamUtente: TStringField;
    QRicercaOreLavUtente1: TIntegerField;
    QRicercaOreLavUtente2: TIntegerField;
    QRicercaOreLavUtente3: TIntegerField;
    QRicercaTitoloCliente: TStringField;
    QRicercaIDContrattoCG: TIntegerField;
    QRicercaIdAzienda: TIntegerField;
    QRicercaLKRuolo: TStringField;
    QRicercaLkArea: TStringField;
    QRicercaLkStatoRic: TStringField;
    QRicercaIDCausale: TIntegerField;
    QRicercaStatoRic: TStringField;
          dxLayoutControl1Group_Root: TdxLayoutGroup;
          dxLayoutControl1: TdxLayoutControl;
          dxDBLookupEdit1: TdxDBLookupEdit;
          dxLayoutControl1Item1: TdxLayoutItem;
          UpdRicerca: TUpdateSQL;
          dxLayoutControl2Group_Root: TdxLayoutGroup;
          dxLayoutControl2: TdxLayoutControl;
          Splitter1: TSplitter;
          dxDBEdit1: TdxDBEdit;
          dxLayoutControl2Item1: TdxLayoutItem;
          QClientiLK: TQuery;
          QClientiLKID: TAutoIncField;
          QClientiLKDescrizione: TStringField;
    QRicercaCliente: TStringField;
          dxDBLookupEdit2: TdxDBLookupEdit;
          dxLayoutControl2Item2: TdxLayoutItem;
          QRuoliLK: TQuery;
          QRuoliLKID: TAutoIncField;
          QRuoliLKDescrizione: TStringField;
    QRicercaRuolo: TStringField;
          dxDBLookupEdit3: TdxDBLookupEdit;
          dxLayoutControl2Item3: TdxLayoutItem;
          QClientiLKAttivita: TStringField;
          dxDBEdit2: TdxDBEdit;
          dxLayoutControl2Item4: TdxLayoutItem;
    QRicercaSettore: TStringField;
          dxDBDateEdit1: TdxDBDateEdit;
          dxLayoutControl2Item5: TdxLayoutItem;
          dxLayoutControl2Group1: TdxLayoutGroup;
          dxDBDateEdit2: TdxDBDateEdit;
          dxLayoutControl2Item6: TdxLayoutItem;
          dxLayoutControl2Group2: TdxLayoutGroup;
          QUsersLK: TQuery;
          QUsersLKID: TAutoIncField;
          QUsersLKNominativo: TStringField;
    QRicercaUtente: TStringField;
          dxDBLookupEdit4: TdxDBLookupEdit;
          dxLayoutControl2Item7: TdxLayoutItem;
          QCandRic: TQuery;
          DsQCandRic: TDataSource;
          QEspLavLK: TQuery;
          QEspLavLKID: TAutoIncField;
          QEspLavLKAzienda: TStringField;
          QEspLavLKRuolo: TStringField;
          QLastEspLav: TQuery;
          QLastEspLavIDAnagrafica: TIntegerField;
          QLastEspLavMaxID: TIntegerField;
          QLastEspLavAzienda: TStringField;
          QLastEspLavRuolo: TStringField;
          QCandRicID: TAutoIncField;
          QCandRicTitolo: TStringField;
          QCandRicCognome: TStringField;
          QCandRicNome: TStringField;
          QCandRicDataNascita: TDateTimeField;
          QCandRicAzienda: TStringField;
          QEspLavLKTitoloMansione: TStringField;
          QLastEspLavTitoloMansione: TStringField;
          QCandRicTitoloMansione: TStringField;
          QCandRicStato: TStringField;
          QCandRicNote2: TStringField;
          dxDBGrid1: TdxDBGrid;
          QCandRicIDCandRic: TAutoIncField;
          dxDBGrid1Titolo: TdxDBGridMaskColumn;
          dxDBGrid1Cognome: TdxDBGridMaskColumn;
          dxDBGrid1Nome: TdxDBGridMaskColumn;
          dxDBGrid1DataNascita: TdxDBGridDateColumn;
          dxDBGrid1Azienda: TdxDBGridLookupColumn;
          dxDBGrid1TitoloMansione: TdxDBGridLookupColumn;
          dxDBGrid1Stato: TdxDBGridMaskColumn;
          dxDBGrid1Note2: TdxDBGridMaskColumn;
          QCandRicEta: TStringField;
          dxDBGrid1Column9: TdxDBGridColumn;
          Q: TQuery;
          QCandRicEscluso: TBooleanField;
          dxDBGrid1Escluso: TdxDBGridCheckColumn;
          ToolbarButton971: TToolbarButton97;
          Q1: TQuery;
          procedure BSchedaCandClick(Sender: TObject);
          procedure DsQRicercaStateChange(Sender: TObject);
          procedure QRicercaAfterPost(DataSet: TDataSet);
          procedure BOKClick(Sender: TObject);
          procedure BCanClick(Sender: TObject);
          procedure QCandRicCalcFields(DataSet: TDataSet);
          procedure FormShow(Sender: TObject);
          procedure QRicercaAfterOpen(DataSet: TDataSet);
          procedure BInsCandElencoClick(Sender: TObject);
          procedure BEliminaCandClick(Sender: TObject);
          procedure ToolbarButton971Click(Sender: TObject);
          procedure BNuovoClick(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     ASAcommessaForm: TASAcommessaForm;

implementation

uses uASACand,MDRicerche,uUtilsVarie,ElencoDip,ModuloDati,Main,
     CheckPass,InsRicerca;

{$R *.DFM}

procedure TASAcommessaForm.BSchedaCandClick(Sender: TObject);
begin
     if QCandRic.IsEmpty then exit;
     // puntare al candidato
     PosizionaAnag(QCandRicID.Value);
     ASASchedaCandForm:=TASASchedaCandForm.create(self);
     ASASchedaCandForm.ShowModal;
     ASASchedaCandForm.Free;
end;

procedure TASAcommessaForm.DsQRicercaStateChange(Sender: TObject);
var b: boolean;
begin
     b:=DsQRicerca.State in [dsEdit,dsInsert];
     BNuovo.Visible:=not b;
     BEliminaRic.Visible:=not b;
     BOK.Visible:=b;
     BCan.Visible:=b;
end;

procedure TASAcommessaForm.QRicercaAfterPost(DataSet: TDataSet);
begin
     QRicerca.ApplyUpdates;
     QRicerca.CommitUpdates;
end;

procedure TASAcommessaForm.BOKClick(Sender: TObject);
begin
     QRicerca.Post;
end;

procedure TASAcommessaForm.BCanClick(Sender: TObject);
begin
     QRicerca.Cancel;
end;

procedure TASAcommessaForm.QCandRicCalcFields(DataSet: TDataSet);
var xAnno,xMese,xgiorno: Word;
begin
     if QCandRicDataNascita.asString='' then
          QCandRicEta.asString:=''
     else begin
          DecodeDate((Date-QCandRicDataNascita.Value),xAnno,xMese,xgiorno);
          // TAnagraficaEta.Value:=StrToInt(copy(xAnno,3,2));
          QCandRicEta.Value:=copy(IntToStr(xAnno),3,2);
     end;
end;

procedure TASAcommessaForm.FormShow(Sender: TObject);
begin
     // posizionati sul soggetto selezionato
     QRicerca.Close;
     QRicerca.ParamByName('xIDRic').asInteger:=DataRicerche.QRicAttiveID.Value;
     QRicerca.Open;
     dxDBGrid1.Filter.Add(dxDBGrid1Escluso,False,'no');
end;

procedure TASAcommessaForm.QRicercaAfterOpen(DataSet: TDataSet);
begin
     QCandRic.Open;
end;

procedure TASAcommessaForm.BInsCandElencoClick(Sender: TObject);
var xVai: boolean;
     xRic,xLivProtez,xDicMess,xPassword,xUtenteResp,xClienteBlocco: string;
     xIDEvento,xIDClienteBlocco: integer;
     QLocal: TQuery;
begin
     if not Mainform.CheckProfile('210') then Exit;
     ElencoDipForm:=TElencoDipForm.create(self);
     ElencoDipForm.ShowModal;
     if ElencoDipForm.ModalResult=mrOK then begin
          // controllo propriet� CV
          if (not((ElencoDipForm.TAnagDipIDProprietaCV.asString='')or(ElencoDipForm.TAnagDipIDProprietaCV.Value=0)))and
               (ElencoDipForm.TAnagDipIDProprietaCV.value<>DataRicerche.TRicerchePendIDCliente.value) then
               if MessageDlg('ATTENZIONE: il CV risulta di propriet� di un altro cliente .'+chr(13)+
                    'Vuoi proseguire lo stesso ?',mtWarning, [mbYes,mbNo],0)=mrNo then begin
                    ElencoDipForm.Free;
                    exit;
               end;

          DataRicerche.TRicAnagIDX.Open;
          if not DataRicerche.TRicAnagIDX.FindKey([DataRicerche.TRicerchePendID.Value,ElencoDipForm.TAnagDipID.Value]) then begin
               xVai:=True;
               // controllo se � in selezione
               if ElencoDipForm.TAnagDipIDTipoStato.Value=1 then begin
                    if MessageDlg('Il soggetto risulta IN SELEZIONE: '+chr(13)+xRic+chr(13)+'PROCEDERE COMUNQUE ?',mtWarning, [mbYes,mbNo],0)=mrNo then xVai:=False;
               end;
               // controllo se � gi� associato ad un'altra ricerca per la STESSA azienda
               Q.Close;
               Q.SQL.clear;
               Q.SQl.Add('select EBC_Ricerche.Progressivo,EBC_StatiRic.StatoRic from EBC_CandidatiRicerche,EBC_Ricerche,EBC_StatiRic ');
               Q.SQl.Add('where EBC_CandidatiRicerche.IDRicerca=EBC_Ricerche.ID and EBC_Ricerche.IDStatoRic=EBC_StatiRic.ID');
               Q.SQl.Add('and EBC_CandidatiRicerche.IDAnagrafica=:xIDAnag and EBC_Ricerche.IDCliente=:xIDCliente');
               Q.Prepare;
               Q.ParamByName('xIDAnag').asInteger:=ElencoDipForm.TAnagDipID.Value;
               Q.ParamByName('xIDCliente').asInteger:=DataRicerche.TRicerchePendIDCliente.Value;
               Q.Open;
               if not Q.IsEmpty then begin
                    xRic:='';
                    while not Q.EOF do begin xRic:=xRic+'N�'+Q.FieldByName('Progressivo').asString+' ('+Q.FieldByName('StatoRic').asString+') '; Q.Next; end;
                    if MessageDlg('Il soggetto � associato gi� alle seguenti ricerche per lo stesso cliente: '+chr(13)+xRic+chr(13)+'PROCEDERE COMUNQUE ?',mtWarning, [mbYes,mbNo],0)=mrNo then xVai:=False;
               end;
               // controllo incompatibilit�
               if IncompAnagCli(ElencoDipForm.TAnagDipID.Value,DataRicerche.TRicerchePendIDCliente.Value) then
                    if MessageDlg('Il soggetto risulta INCOMPATIBILE con questo cliente o con un cliente associato'+chr(13)+
                         'PROCEDERE COMUNQUE ?',mtWarning, [mbYes,mbNo],0)=mrNo then xVai:=False;

               // controllo blocco livello 1 o 2
               xIDClienteBlocco:=CheckAnagInseritoBlocco1(ElencoDipForm.TAnagDipID.Value);
               if xIDClienteBlocco>0 then begin
                    xLivProtez:=copy(IntToStr(xIDClienteBlocco),1,1);
                    if xLivProtez='1' then begin
                         xIDClienteBlocco:=xIDClienteBlocco-10000;
                         xClienteBlocco:=GetDescCliente(xIDClienteBlocco);
                         xDicMess:='Stai inserendo in commessa un candidato appartenente ad una azienda ('+xClienteBlocco+') con blocco di livello 1, ';
                    end else begin
                         xIDClienteBlocco:=xIDClienteBlocco-20000;
                         xClienteBlocco:=GetDescCliente(xIDClienteBlocco);
                         xDicMess:='Stai inserendo in commessa un candidato appartenente ad una azienda ATTIVA ('+xClienteBlocco+'), ';
                    end;
                    if MessageDlg('ATTENZIONE!! PROTEZIONE CLIENTI LIVELLO '+xLivProtez+chr(13)+xDicMess+
                         'protetta dalle normative interne, se sei autorizzato premi SI, altrimenti premi NO. '+
                         'Se si prosegue, l''operazione verr� registrata nello storico del soggetto come forzatura. '+
                         'Verr� inviato un messaggio di promemoria al responsabile diretto',mtWarning, [mbYes,mbNo],0)=mrNo then xVai:=False
                    else begin
                         // pwd
                         xPassword:='';
                         xUtenteResp:=GetDescUtenteResp(MainForm.xIDUtenteAttuale);
                         if not InputQuery('Forzatura blocco livello '+xLivProtez,'Password di '+xUtenteResp,xPassword) then exit;
                         if not CheckPassword(xPassword,GetPwdUtenteResp(MainForm.xIDUtenteAttuale)) then begin
                              MessageDlg('Password errata',mtError, [mbOK],0);
                              xVai:=False;
                         end;
                         if xVai then begin
                              // promemoria al resp.
                              QLocal:=CreateQueryFromString('insert into Promemoria (IDUtente,IDUtenteDa,DataIns,Testo,Evaso,DataDaLeggere) '+
                                   'values (:xIDUtente,:xIDUtenteDa,:xDataIns,:xTesto,:xEvaso,:xDataDaLeggere)');
                              QLocal.ParamByName('xIDUtente').asInteger:=GetIDUtenteResp(MainForm.xIDUtenteAttuale);
                              QLocal.ParamByName('xIDUtenteDa').asInteger:=MainForm.xIDUtenteAttuale;
                              QLocal.ParamByName('xDataIns').asDateTime:=Date;
                              QLocal.ParamByName('xTesto').asString:='forzatura livello '+xLivProtez+' per CV n� '+ElencoDipForm.TAnagDipCVNumero.AsString;
                              QLocal.ParamByName('xEvaso').asBoolean:=False;
                              QLocal.ParamByName('xDataDaLeggere').asDateTime:=Date;
                              QLocal.ExecSQL;
                              // registra nello storico soggetto
                              xIDEvento:=GetIDEvento('forzatura Blocco livello '+xLivProtez);
                              if xIDEvento>0 then begin
                                   with Data.Q1 do begin
                                        close;
                                        SQL.text:='insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) '+
                                             'values (:xIDAnagrafica,:xIDEvento,:xDataEvento,:xAnnotazioni,:xIDRicerca,:xIDUtente,:xIDCliente)';
                                        ParamByName('xIDAnagrafica').asInteger:=ElencoDipForm.TAnagDipID.Value;
                                        ParamByName('xIDEvento').asInteger:=xIDevento;
                                        ParamByName('xDataEvento').asDateTime:=Date;
                                        ParamByName('xAnnotazioni').asString:='cliente: '+xClienteBlocco;
                                        ParamByName('xIDRicerca').asInteger:=DataRicerche.TRicerchePendID.Value;
                                        ParamByName('xIDUtente').asInteger:=MainForm.xIDUtenteAttuale;
                                        ParamByName('xIDCliente').asInteger:=xIDClienteBlocco;
                                        ExecSQL;
                                   end;
                              end;
                         end;
                    end;
               end;

               if xVai then begin
                    Data.DB.BeginTrans;
                    try
                         if Q.Active then Q.Close;
                         Q.SQL.text:='insert into EBC_CandidatiRicerche (IDRicerca,IDAnagrafica,Escluso,Stato,DataIns) '+
                              'values (:xIDRicerca,:xIDAnagrafica,:xEscluso,:xStato,:xDataIns)';
                         Q.ParamByName('xIDRicerca').asInteger:=DataRicerche.TRicerchePendID.Value;
                         Q.ParamByName('xIDAnagrafica').asInteger:=ElencoDipForm.TAnagDipID.Value;
                         Q.ParamByName('xEscluso').asBoolean:=False;
                         Q.ParamByName('xStato').asString:='inserito direttamente da elenco';
                         Q.ParamByName('xDataIns').asDateTime:=Date;
                         Q.ExecSQL;
                         // aggiornamento stato anagrafica
                         if Q.Active then Q.Close;
                         Q.SQL.text:='update Anagrafica set IDStato=27,IDTipoStato=1 where ID='+ElencoDipForm.TAnagDipID.asString;
                         Q.ExecSQL;
                         // aggiornamento storico
                         if Q.Active then Q.Close;
                         Q.SQL.text:='insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) '+
                              'values (:xIDAnagrafica,:xIDEvento,:xDataEvento,:xAnnotazioni,:xIDRicerca,:xIDUtente,:xIDCliente)';
                         Q.ParamByName('xIDAnagrafica').asInteger:=ElencoDipForm.TAnagDipID.Value;
                         Q.ParamByName('xIDEvento').asInteger:=19;
                         Q.ParamByName('xDataEvento').asDateTime:=date;
                         Q.ParamByName('xAnnotazioni').asString:='ric.n�'+DataRicerche.TRicerchePendProgressivo.Value+' ('+DataRicerche.TRicerchePendCliente.Value+')';
                         Q.ParamByName('xIDRicerca').asInteger:=DataRicerche.TRicerchePendID.Value;
                         Q.ParamByName('xIDUtente').asInteger:=DataRicerche.TRicerchePendIDUtente.Value;
                         Q.ParamByName('xIDCliente').asInteger:=DataRicerche.TRicerchePendIDCliente.Value;
                         Q.ExecSQL;
                         Data.DB.CommitTrans;
                         QCandRic.Close;
                         QCandRic.Open;
                         //LTotCand.Caption:=IntToStr(DataRicerche.QCandRic.RecordCount);
                    except
                         Data.DB.RollbackTrans;
                         MessageDlg('Errore sul database:  inserimento non avvenuto',mtError, [mbOK],0);
                    end;
               end;
          end;
          DataRicerche.TRicAnagIDX.Close;
     end;
     ElencoDipForm.Free;
end;

procedure TASAcommessaForm.BEliminaCandClick(Sender: TObject);
var xPerche: string;
begin
     if not Mainform.CheckProfile('211') then Exit;
     if QCandRicEscluso.Value then begin
          MessageDlg('Il candidato � gi� escluso',mtError, [mbOK],0);
          exit;
     end;
     if not QCandRic.IsEmpty then begin
          if InputQuery('Elimina dalla ricerca','Motivo:',xPerche) then begin
               Data.DB.BeginTrans;
               try
                    DataRicerche.QGen.SQL.Clear;
                    DataRicerche.QGen.SQL.Add('update EBC_CandidatiRicerche set Escluso=1,');
                    DataRicerche.QGen.SQL.Add('Stato="eliminato (motivo: '+xPerche+')" where IDAnagrafica='+QCandRicID.asString+' and IDRicerca='+DataRicerche.TRicerchePendID.asString);
                    DataRicerche.QGen.execSQL;
                    // aggiornamento storico
                    if Q.Active then Q.Close;
                    Q.SQL.text:='insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) '+
                         'values (:xIDAnagrafica,:xIDEvento,:xDataEvento,:xAnnotazioni,:xIDRicerca,:xIDUtente,:xIDCliente)';
                    Q.ParamByName('xIDAnagrafica').asInteger:=QCandRicID.Value;
                    Q.ParamByName('xIDEvento').asInteger:=17;
                    Q.ParamByName('xDataEvento').asDateTime:=date;
                    Q.ParamByName('xAnnotazioni').asString:='ric.'+DataRicerche.TRicerchePendProgressivo.asString+' per '+DataRicerche.TRicerchePendCliente.asString+' (motivo: '+xPerche+')';
                    Q.ParamByName('xIDRicerca').asInteger:=DataRicerche.TRicerchePendID.Value;
                    Q.ParamByName('xIDUtente').asInteger:=DataRicerche.TRicerchePendIDUtente.Value;
                    Q.ParamByName('xIDCliente').asInteger:=DataRicerche.TRicerchePendIDCliente.Value;
                    Q.ExecSQL;
                    // controllo che non sia associato a ricerche diverse da questa dove non � gi� escluso
                    DataRicerche.QGen.Close;
                    DataRicerche.QGen.SQL.Clear;
                    DataRicerche.QGen.SQL.Add('select IDRicerca from EBC_CandidatiRicerche where IDAnagrafica='+QCandRicID.asString+' and IDRicerca<>'+DataRicerche.TRicerchePendID.asString+' and Escluso=0');
                    DataRicerche.QGen.Open;
                    if not DataRicerche.QGen.IsEmpty then begin
                         // lasciato in selezione
                         MessageDlg('Il candidato � associato ad altre ricerche dove non � gi� escluso - verr� quindi mantenuto in selezione',mtInformation, [mbOK],0);
                    end else begin
                         // archiviazione CV
                         if Q.Active then Q.Close;
                         Q.SQL.text:='update Anagrafica set IDStato=28,IDTipoStato=2 where ID='+QCandRicID.asString;
                         Q.ExecSQL;
                    end;
                    Data.DB.CommitTrans;
                    QCandRic.Close;
                    QCandRic.Open;
                    //LTotCand.Caption:=IntToStr(QCandRic.RecordCount);
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('Errore sul database:  inserimento non avvenuto',mtError, [mbOK],0);
               end;
          end;
     end;
end;

procedure TASAcommessaForm.ToolbarButton971Click(Sender: TObject);
var xNote: string;
begin
     xNote:=QCandRicNote2.Value;
     if InputQuery('Modifica note candidato','Note:',xNote) then begin
          Q1.Close;
          Q1.SQL.text:='update EBC_CandidatiRicerche set Note2=:xNote '+
               ' where ID='+QCandRicIDCandRic.AsString;
          Q1.ParamByName('xNote').asString:=xNote;
          Q1.ExecSQL;
          QCandRic.CLose;
          QCandRic.Open;
     end;
end;

procedure TASAcommessaForm.BNuovoClick(Sender: TObject);
var xAnno,xMese,xGiorno: Word;
     xIDRic,xIDAnagUtente,xProvvDef: integer;
     xCodRic: string;
     xProvv: boolean;
begin
     InsRicercaForm:=TInsRicercaForm.create(self);
     InsRicercaForm.DEData.Date:=Date;
     InsRicercaForm.SENumRic.Value:=1;
     InsRicercaForm.TUsers.FindKey([mainForm.xIDUtenteAttuale]);
     InsRicercaForm.ShowModal;
     if InsRicercaForm.ModalResult=mrOK then begin
          // dati sulla provvigione
          Data.Qtemp.Close;
          Data.Qtemp.SQL.Text:='select IDAnagrafica from Users where ID='+InsRicercaForm.TUsersID.asString;
          Data.Qtemp.Open;
          if Data.Qtemp.FieldByName('IDAnagrafica').AsInteger>0 then begin
               xIDAnagUtente:=Data.Qtemp.FieldByName('IDAnagrafica').AsInteger;
               Data.Qtemp.Close;
               Data.Qtemp.SQL.Text:='select * from AnagRetribuzHRMS where IDAnagrafica='+IntToStr(xIDAnagUtente);
               Data.Qtemp.Open;
               if not Data.Qtemp.IsEmpty then begin
                    xProvv:=Data.Qtemp.FieldByName('Provvigione').asBoolean;
                    xProvvDef:=Data.Qtemp.FieldByName('ProvvigioneDefault').asInteger; ;
               end else begin
                    xProvv:=False;
                    xProvvDef:=0;
               end;
          end;
          Data.Qtemp.Close;

          Data.DB.BeginTrans;
          try
               Data.Qtemp.Close;
               Data.Qtemp.SQL.Text:='insert into EBC_Ricerche (IDCliente,IDUtente,DataInizio,'+
                    'IDStatoRic,DallaData,IDMansione,IDArea,NumRicercati,Tipo,IDContattoCli,IDSede,'+
                    'Provvigione,ProvvigionePerc,QuandoPagamUtente) '+
                    '  values (:xIDCliente,:xIDUtente,:xDataInizio,'+
                    ':xIDStatoRic,:xDallaData,:xIDMansione,:xIDArea,:xNumRicercati,:xTipo,:xIDContattoCli,:xIDSede,'+
                    ':xProvvigione,:xProvvigionePerc,:xQuandoPagamUtente)';
               Data.Qtemp.ParamByName('xIDCliente').asInteger:=InsRicercaForm.xIDCliente;
               Data.Qtemp.ParamByName('xIDUtente').asInteger:=InsRicercaForm.TUsersID.Value;
               Data.Qtemp.ParamByName('xDataInizio').asDateTime:=InsRicercaForm.DEData.Date;
               Data.Qtemp.ParamByName('xIDStatoRic').asInteger:=1;
               Data.Qtemp.ParamByName('xDallaData').asDateTime:=InsRicercaForm.DEData.Date;
               Data.Qtemp.ParamByName('xIDMansione').asInteger:=InsRicercaForm.xIDRuolo;
               Data.Qtemp.ParamByName('xIDArea').asInteger:=InsRicercaForm.xIDArea;
               Data.Qtemp.ParamByName('xNumRicercati').asInteger:=InsRicercaForm.SENumRic.Value;
               Data.Qtemp.ParamByName('xTipo').asString:=InsRicercaForm.CBTipo.Text;
               Data.Qtemp.ParamByName('xIDContattoCli').asInteger:=InsRicercaForm.xIDRifInterno;
               Data.Qtemp.ParamByName('xIDSede').asInteger:=InsRicercaForm.QSediID.Value;
               Data.Qtemp.ParamByName('xProvvigione').asBoolean:=xProvv;
               Data.Qtemp.ParamByName('xProvvigionePerc').asInteger:=xProvvDef;
               Data.Qtemp.ParamByName('xQuandoPagamUtente').asString:='1'; // a default TIPO=1
               Data.Qtemp.ExecSQL;

               Data.Qtemp.SQL.text:='select @@IDENTITY as LastID';
               Data.Qtemp.Open;
               xIDRic:=Data.Qtemp.FieldByName('LastID').asInteger;
               Data.Qtemp.Close;

               // progressivo dal nuovo contatore (non pi� dall'ID)
               xCodRic:=GetCodRicerca;
               Data.Qtemp.Close;
               Data.Qtemp.SQL.text:='update EBC_Ricerche set Progressivo=:xProg where ID='+IntToStr(xIDRic);
               Data.Qtemp.ParamByName('xProg').asString:=xCodRic;
               Data.Qtemp.ExecSQL;

               // se � impostata la data prevista di chiusura
               if InsRicercaForm.DEDataPrevChius.Text<>'' then begin
                    Data.Qtemp.SQL.text:='update EBC_Ricerche set DataPrevChiusura=:xDataPrevChiusura where ID='+IntToStr(xIDRic);
                    Data.Qtemp.ParamByName('xDataPrevChiusura').asDateTime:=InsRicercaForm.DEDataPrevChius.Date;
                    Data.Qtemp.ExecSQL;
               end;

               // se � impostato il ruolo: inserire la job description in Specifiche
               if InsRicercaForm.xIDRuolo>0 then begin
                    Data.Q1.Close;
                    Data.Q1.SQL.text:='select Mansioni from Mansioni where ID='+IntToStr(InsRicercaForm.xIDRuolo);
                    Data.Q1.Open;
                    Data.Qtemp.SQL.text:='update EBC_Ricerche set Specifiche=:xSpecifiche where ID='+IntToStr(xIDRic);
                    Data.Qtemp.ParamByName('xSpecifiche').asString:='JOB DESCRIPTION:'+chr(13)+Data.Q1.FieldByName('Mansioni').asString;
                    Data.Qtemp.ExecSQL;
               end;

               DecodeDate(Date,xAnno,xMese,xGiorno);

               // riga in storico ricerca
               Data.Q1.Close;
               Data.Q1.SQL.text:='insert into EBC_StoricoRic (IDRicerca,IDStatoRic,DallaData,Note) '+
                    'values (:xIDRicerca,:xIDStatoRic,:xDallaData,:xNote)';
               Data.Q1.ParamByName('xIDRicerca').asInteger:=xIDRic;
               Data.Q1.ParamByName('xIDStatoRic').asInteger:=1;
               Data.Q1.ParamByName('xDallaData').asDateTime:=Date;
               Data.Q1.ParamByName('xNote').asString:='inizio ricerca';
               Data.Q1.ExecSQL;

               // riga in ricerche-utenti con ruolo "capo commessa" (o comunque il primo)
               Data.Q1.Close;
               Data.Q1.SQL.text:='insert into EBC_RicercheUtenti (IDRicerca,IDUtente,IDRuoloRic) '+
                    'values (:xIDRicerca,:xIDUtente,:xIDRuoloRic)';
               Data.Q1.ParamByName('xIDRicerca').asInteger:=xIDRic;
               Data.Q1.ParamByName('xIDUtente').asInteger:=InsRicercaForm.TUsersID.Value;
               Data.Q1.ParamByName('xIDRuoloRic').asInteger:=1;
               Data.Q1.ExecSQL;

               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ATTENZIONE: errore sul database - Inserimento non avvenuto',mtError, [mbOK],0);
               raise;
               Exit;
          end;

          // visualizza ricerca
          CaricaApriRicPend(xIDRic);
          QRicerca.Close;
          QRicerca.ParamByName('xIDRic').asInteger:=xIDRic;
          QRicerca.Open;

     end;
     InsRicercaForm.Free;
end;

end.

