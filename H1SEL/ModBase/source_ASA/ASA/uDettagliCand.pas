unit uDettagliCand;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, ExtCtrls, ComCtrls, Db, DBTables, DBCtrls,
     dxLayoutControl, cxControls, dxCntner, dxEditor, dxExEdtr, dxDBEdtr,
     dxDBELib, dxEdLib, FrmDatiRetribuz, TB97, dxTL, dxDBCtrl, dxDBGrid;

type
     TDettagliCandForm = class(TForm)
          Panel1: TPanel;
          PCDettCand: TPageControl;
          TSDettEspLav: TTabSheet;
          TSDettRetrib: TTabSheet;
          QEspLav: TQuery;
          DsQEspLav: TDataSource;
          lcEspLavGroup_Root: TdxLayoutGroup;
          lcEspLav: TdxLayoutControl;
          QEspLavID: TAutoIncField;
          QEspLavIDAzienda: TIntegerField;
          QEspLavIDSettore: TIntegerField;
          QEspLavAnnoDal: TSmallintField;
          QEspLavAnnoAl: TSmallintField;
          QEspLavIDMansione: TIntegerField;
          QAziendeLK: TQuery;
          QAziendeLKID: TAutoIncField;
          QAziendeLKCliente: TStringField;
          QAziendeLKStato: TStringField;
          QEspLavAzienda: TStringField;
          QSettoriLK: TQuery;
          QSettoriLKID: TAutoIncField;
          QSettoriLKAttivita: TStringField;
          QSettoriLKIDAreaSettore: TIntegerField;
          QSettoriLKIDAzienda: TStringField;
          QEspLavSettore: TStringField;
          QRuoliLK: TQuery;
          QRuoliLKID: TAutoIncField;
          QRuoliLKDescrizione: TStringField;
          QRuoliLKIDArea: TIntegerField;
          QRuoliLKFileMansionario: TStringField;
          QRuoliLKMansioni: TMemoField;
          QRuoliLKIDAzienda: TIntegerField;
          QRuoliLKLKArea: TStringField;
          QRuoliLKTolleranzaPerc: TIntegerField;
          QEspLavRuolo: TStringField;
          dxDBLookupEdit1: TdxDBLookupEdit;
          lcEspLavItem1: TdxLayoutItem;
          lcEspLavItem2: TdxLayoutItem;
          lcEspLavItem3: TdxLayoutItem;
          lcEspLavGroup1: TdxLayoutGroup;
          dxDBEdit1: TdxDBEdit;
          lcEspLavItem4: TdxLayoutItem;
          lcEspLavItem5: TdxLayoutItem;
          lcEspLavItem6: TdxLayoutItem;
          lcEspLavGroup3: TdxLayoutGroup;
          lcEspLavGroup4: TdxLayoutGroup;
          dxDBEdit2: TdxDBEdit;
          dxDBEdit3: TdxDBEdit;
          UpdEspLav: TUpdateSQL;
          QEspLavTitoloMansione: TStringField;
          BitBtn1: TBitBtn;
          QEspLavDescrizioneMansione: TMemoField;
          Label1: TLabel;
          QEspLavAttuale: TBooleanField;
          dxDBCheckEdit1: TdxDBCheckEdit;
          lcEspLavItem8: TdxLayoutItem;
          QEspLavIDAnagrafica: TIntegerField;
          DatiRetribuzFrame1: TDatiRetribuzFrame;
          BOK: TToolbarButton97;
          BCan: TToolbarButton97;
          BMod: TToolbarButton97;
          lcEspLavItem9: TdxLayoutItem;
          SpeedButton1: TSpeedButton;
          Q: TQuery;
          lcEspLavGroup5: TdxLayoutGroup;
          lcEspLavGroup8: TdxLayoutGroup;
          Panel2: TPanel;
          lcEspLavItem10: TdxLayoutItem;
          Panel3: TPanel;
          lcEspLavItem11: TdxLayoutItem;
          dxDBGrid1: TdxDBGrid;
          lcEspLavItem12: TdxLayoutItem;
          dxDBGrid2: TdxDBGrid;
          lcEspLavItem13: TdxLayoutItem;
          lcEspLavGroup10: TdxLayoutGroup;
          QAltreFunzioni: TQuery;
          DsQAltreFunzioni: TDataSource;
          QAltriSettori: TQuery;
          DsQAltriSettori: TDataSource;
          dxDBGrid1Ruolo: TdxDBGridMaskColumn;
          dxDBGrid2Settore: TdxDBGridMaskColumn;
          BEspLavNew: TSpeedButton;
          BEspLavDel: TSpeedButton;
          SpeedButton2: TSpeedButton;
          SpeedButton3: TSpeedButton;
          lcEspLavGroup9: TdxLayoutGroup;
          lcEspLavGroup7: TdxLayoutGroup;
          Panel4: TPanel;
          lcEspLavItem14: TdxLayoutItem;
          SpeedButton4: TSpeedButton;
          SpeedButton5: TSpeedButton;
          dxDBEdit4: TdxDBEdit;
          dxDBEdit5: TdxDBEdit;
          Panel5: TPanel;
          lcEspLavItem15: TdxLayoutItem;
          SpeedButton6: TSpeedButton;
          SpeedButton7: TSpeedButton;
          QEspLavIDArea: TIntegerField;
          Label2: TLabel;
          DBMemo1: TDBMemo;
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
          procedure BitBtn1Click(Sender: TObject);
          procedure QEspLavAfterInsert(DataSet: TDataSet);
          procedure QEspLavAfterEdit(DataSet: TDataSet);
          procedure QEspLavAfterPost(DataSet: TDataSet);
          procedure FormShow(Sender: TObject);
          procedure DsQEspLavStateChange(Sender: TObject);
          procedure BOKClick(Sender: TObject);
          procedure BCanClick(Sender: TObject);
          procedure BModClick(Sender: TObject);
          procedure SpeedButton1Click(Sender: TObject);
          procedure BEspLavNewClick(Sender: TObject);
          procedure SpeedButton2Click(Sender: TObject);
          procedure BEspLavDelClick(Sender: TObject);
          procedure SpeedButton3Click(Sender: TObject);
          procedure PCDettCandChange(Sender: TObject);
          procedure SpeedButton4Click(Sender: TObject);
          procedure SpeedButton5Click(Sender: TObject);
          procedure SpeedButton6Click(Sender: TObject);
          procedure SpeedButton7Click(Sender: TObject);
     private
          xEspLavIns: boolean;
     public
          { Public declarations }
     end;

var
     DettagliCandForm: TDettagliCandForm;

implementation

uses uASACand, ModuloDati, Main, uUtilsVarie, Azienda, InsRuolo, SelRuolo,
     SelAttivita, uASAAzienda;

{$R *.DFM}

procedure TDettagliCandForm.FormClose(Sender: TObject;
     var Action: TCloseAction);
begin
     if DsQEspLav.state = dsEdit then QEspLav.Post;
     with QEspLav do begin
          Data.DB.BeginTrans;
          try
               ApplyUpdates;
               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate in AnagAltriDati', mtError, [mbOK], 0);
          end;
          CommitUpdates;
     end;
     ASASchedaCandForm.QAziendaAttuale.Close;
     ASASchedaCandForm.QAziendaAttuale.Open;
     if not Data.TEspLav.active then Data.TEspLav.Open;
end;

procedure TDettagliCandForm.BitBtn1Click(Sender: TObject);
begin
     close;
end;

procedure TDettagliCandForm.QEspLavAfterInsert(DataSet: TDataSet);
begin
     QEspLavIDAnagrafica.Value := ASASchedaCandForm.QAnagID.Value;
     if ASASchedaCandForm.QAziendaAttuale.IsEmpty then
          QEspLavAttuale.Value := True
     else QEspLavAttuale.Value := False;
     xEspLavIns := True;
end;

procedure TDettagliCandForm.QEspLavAfterEdit(DataSet: TDataSet);
begin
     xEspLavIns := False;
end;

procedure TDettagliCandForm.QEspLavAfterPost(DataSet: TDataSet);
var xID: integer;
begin
     QEspLav.ApplyUpdates;
     QEspLav.CommitUpdates;
     if xEspLavIns then begin
          ASASchedaCandForm.QEspLav.Close;
          ASASchedaCandForm.QEspLav.Open;
          Data.QTemp.Close;
          Data.QTemp.SQL.text := 'select Max(ID) MaxID from EsperienzeLavorative where IDAnagrafica=' + ASASchedaCandForm.QAnagID.AsString;
          Data.QTemp.Open;
          ASASchedaCandForm.QEspLav.Locate('ID', Data.QTemp.FieldByName('MaxID').asInteger, []);
          Data.QTemp.Close;
     end;
     QEspLav.Close;
     QEspLav.ParamByName('xID').asInteger := ASASchedaCandForm.QEspLavID.Value;
     QEspLav.Open;

     Log_Operation(MainForm.xIDUtenteAttuale, 'EsperienzeLavorative', QEspLavID.Value, 'U');

     ASASchedaCandForm.QDatiMod.Close;
     ASASchedaCandForm.QDatiMod.Open;
     Data.TEspLav.Close;
     Data.TEspLav.Open;
     Data.TEspLav.Locate('ID', QEspLavID.Value, []);
end;

procedure TDettagliCandForm.FormShow(Sender: TObject);
begin
     Data.TEspLav.Open;
     Data.TEspLav.Locate('ID', QEspLavID.Value, []);
     DatiRetribuzFrame1.QDatiRetribuz.Open;
     DatiRetribuzFrame1.QTot.Open;
     QAltreFunzioni.Open;
     QAltriSettori.Open;
end;

procedure TDettagliCandForm.DsQEspLavStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsQEspLav.State in [dsEdit, dsInsert];
     lcEspLav.Enabled := b;
     BMod.Enabled := not b;
     BOK.Enabled := b;
     BCan.Enabled := b;
     DBMemo1.Enabled := b;
end;

procedure TDettagliCandForm.BOKClick(Sender: TObject);
begin
     QEspLav.Post;
end;

procedure TDettagliCandForm.BCanClick(Sender: TObject);
begin
     QEspLav.Cancel;
end;

procedure TDettagliCandForm.BModClick(Sender: TObject);
begin
     QEspLav.Edit;
end;

procedure TDettagliCandForm.SpeedButton1Click(Sender: TObject);
var xA:string;
begin
     //AziendaForm:=TAziendaForm.create(self);
     //AziendaForm.xUltimaProv:=MainForm.xUltimaProv;
     //AziendaForm.ShowModal;

     //ASAAziendaForm.ShowModal;

    //040205 ASAAziendaForm := TASAAziendaForm.create(self);
    //040205  ASAAziendaForm.QContattiFatti.Close;
   //040205   ASAAziendaForm.ShowModal;
   // MainForm.TbBClientiNewClick(nil);
     {if AziendaForm.ModalResult=mrOK then begin
          Data.DB.BeginTrans;
          try
               Q.SQL.text:='insert into EBC_Clienti (Descrizione,TipoStrada,Indirizzo,NumCivico,Comune,Cap,Provincia,Telefono,Fax,SitoInternet,DescAttivitaAzienda,Fatturato,NumDipendenti,NazioneAbb,Stato,IDAttivita) '+
                    'values (:xDescrizione,:xTipoStrada,:xIndirizzo,:xNumCivico,:xComune,:xCap,:xProvincia,:xTelefono,:xFax,:xSitoInternet,:xDescAttivitaAzienda,:xFatturato,:xNumDipendenti,:xNazioneAbb,:xStato,:xIDAttivita)';
               Q.ParamByName('xDescrizione').asString:=AziendaForm.EDescrizione.Text;
               Q.ParamByName('xTipoStrada').asString:=AziendaForm.CBTipoStrada.Text;
               Q.ParamByName('xIndirizzo').asString:=AziendaForm.EIndirizzo.text;
               Q.ParamByName('xNumCivico').asString:=AziendaForm.ENumCivico.text;
               Q.ParamByName('xComune').asString:=AziendaForm.EComune.Text;
               Q.ParamByName('xCap').asString:=AziendaForm.ECap.Text;
               Q.ParamByName('xProvincia').asString:=AziendaForm.EProv.Text;
               Q.ParamByName('xTelefono').asString:=AziendaForm.ETelefono.Text;
               Q.ParamByName('xFax').asString:=AziendaForm.EFax.Text;
               Q.ParamByName('xSitoInternet').asString:=AziendaForm.ESitoInternet.Text;
               Q.ParamByName('xDescAttivitaAzienda').asString:=AziendaForm.EDescAttAzienda.Text;
               Q.ParamByName('xFatturato').asFloat:=AziendaForm.FEFatturato.Value;
               Q.ParamByName('xNumDipendenti').asInteger:=StrToIntDef(AziendaForm.FENumDip.Text,0);
               Q.ParamByName('xNazioneAbb').asString:=AziendaForm.ENazione.text;
               Q.ParamByName('xStato').asString:='azienda esterna';
               if AziendaForm.RG1.ItemIndex=0 then
                    Q.ParamByName('xIDAttivita').asInteger:=0
               else Q.ParamByName('xIDAttivita').asInteger:=AziendaForm.TSettoriID.Value;

               Q.ExecSQL;
               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
          end;
          QAziendeLK.Close;
          QAziendeLK.Open;
     end;}

     if (Uppercase(copy(Data.GlobalNomeAzienda.Value, 1, 6)) = 'ADVANT') or
          (Uppercase(copy(Data.GlobalNomeAzienda.Value, 1, 3)) = 'EBC') then begin


          ASAAziendaForm := TASAAziendaForm.create(self);
          AsaAziendaForm.BNuovoClick(self);
          ASAAziendaForm.ShowModal;

          //ASAAziendaForm.Free;
          xa := AsaAziendaForm.dxDBEdit1.Text;
          QAziendeLK.Close;
          QAziendeLK.Open;
          ASAAziendaForm.Free;
          QAziendeLK.Locate('Cliente',xa,[]);
          QEspLavIDAzienda.Value := QAziendeLKID.Value;

     end;
end;

procedure TDettagliCandForm.BEspLavNewClick(Sender: TObject);
begin
     if DsQEspLav.state = dsInsert then QEspLav.Post;
     InsRuoloForm := TInsRuoloForm.create(self);
     InsRuoloForm.Height := 425;
     InsRuoloForm.ShowModal;
     if InsRuoloForm.ModalResult = mrOK then begin
          Data.DB.BeginTrans;
          try
               Q.SQL.text := 'insert into EspLavAltriRuoli (IDEspLav,IDRuolo) ' +
                    'values (:xIDEspLav,:xIDRuolo)';
               Q.ParamByName('xIDEspLav').asInteger := QEspLavID.Value;
               Q.ParamByName('xIDRuolo').asInteger := InsRuoloForm.TRuoliID.Value;
               Q.ExecSQL;
               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
          end;
          QAltreFunzioni.Close;
          QAltreFunzioni.Open;
     end;
     InsRuoloForm.Free;
end;

procedure TDettagliCandForm.SpeedButton2Click(Sender: TObject);
begin
     if DsQEspLav.state = dsInsert then QEspLav.Post;
     SelAttivitaForm := TSelAttivitaForm.create(self);
     SelAttivitaForm.ShowModal;
     if SelAttivitaForm.ModalResult = mrOK then begin
          Data.DB.BeginTrans;
          try
               Q.SQL.text := 'insert into EspLavAltriSettori (IDEspLav,IDSettore) ' +
                    'values (:xIDEspLav,:xIDSettore)';
               Q.ParamByName('xIDEspLav').asInteger := QEspLavID.Value;
               Q.ParamByName('xIDSettore').asInteger := SelAttivitaForm.TSettoriID.Value;
               Q.ExecSQL;
               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
          end;
          QAltriSettori.Close;
          QAltriSettori.Open;
     end;
     SelAttivitaForm.Free;
end;

procedure TDettagliCandForm.BEspLavDelClick(Sender: TObject);
begin
     if QAltreFunzioni.IsEmpty then exit;
     if MessageDlg('Sei sicuro di voler cancellare il record selezionato ?', mtWarning, [mbYes, mbNo], 0) = mrNo then exit;
     Data.DB.BeginTrans;
     try
          Q.SQL.text := 'delete from EspLavAltriRuoli ' +
               'where ID=' + QAltreFunzioni.FieldByName('ID').asString;
          Q.ExecSQL;
          Data.DB.CommitTrans;
     except
          Data.DB.RollbackTrans;
          MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
     end;
     QAltreFunzioni.Close;
     QAltreFunzioni.Open;
end;

procedure TDettagliCandForm.SpeedButton3Click(Sender: TObject);
begin
     if QAltriSettori.IsEmpty then exit;
     if MessageDlg('Sei sicuro di voler cancellare il record selezionato ?', mtWarning, [mbYes, mbNo], 0) = mrNo then exit;
     Data.DB.BeginTrans;
     try
          Q.SQL.text := 'delete from EspLavAltriSettori ' +
               'where ID=' + QAltriSettori.FieldByName('ID').asString;
          Q.ExecSQL;
          Data.DB.CommitTrans;
     except
          Data.DB.RollbackTrans;
          MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
     end;
     QAltriSettori.Close;
     QAltriSettori.Open;
end;

procedure TDettagliCandForm.PCDettCandChange(Sender: TObject);
begin
     if PCDettCand.ActivePageIndex = 1 then
          if DsQEspLav.state in [dsEdit, dsInsert] then
               QEspLav.Post;
end;

procedure TDettagliCandForm.SpeedButton4Click(Sender: TObject);
begin
     SelAttivitaForm := TSelAttivitaForm.create(self);
     SelAttivitaForm.ShowModal;
     if SelAttivitaForm.ModalResult = mrOK then begin
          QEspLavIDSettore.Value := SelAttivitaForm.TSettoriID.Value;

     end;
     SelAttivitaForm.Free;
end;

procedure TDettagliCandForm.SpeedButton5Click(Sender: TObject);
begin
     QEspLavIDSettore.Value := 0;
end;

procedure TDettagliCandForm.SpeedButton6Click(Sender: TObject);
begin
     InsRuoloForm := TInsRuoloForm.create(self);
     InsRuoloForm.Height := 425;
     InsRuoloForm.ShowModal;
     if InsRuoloForm.ModalResult = mrOK then begin
          QEspLavIDMansione.Value := InsRuoloForm.TRuoliID.Value;
          QEspLavIDArea.Value := InsRuoloForm.TAreeID.Value;
     end;
     InsRuoloForm.Free;
end;

procedure TDettagliCandForm.SpeedButton7Click(Sender: TObject);
begin
     QEspLavIDMansione.Value := 0;
end;

end.

