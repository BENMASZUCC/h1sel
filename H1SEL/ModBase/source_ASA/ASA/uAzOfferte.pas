unit uAzOfferte;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     dxTL,dxDBCtrl,dxDBGrid,dxCntner,Db,StdCtrls,Buttons,ExtCtrls,
     DBTables,dxDBTLCl,dxGrClms,TB97;

type
     TAzOfferteForm=class(TForm)
          QOfferte: TQuery;
          DsQOfferte: TDataSource;
          Panel1: TPanel;
          BitBtn1: TBitBtn;
          dxDBGrid1: TdxDBGrid;
          QOfferteRif: TStringField;
          QOfferteData: TDateTimeField;
          QOfferteAttenzioneDi: TStringField;
          QOfferteStato: TStringField;
          QOfferteImportoTotale: TFloatField;
          QOfferteID: TAutoIncField;
          dxDBGrid1Rif: TdxDBGridMaskColumn;
          dxDBGrid1Data: TdxDBGridDateColumn;
          dxDBGrid1AttenzioneDi: TdxDBGridMaskColumn;
          dxDBGrid1ImportoTotale: TdxDBGridMaskColumn;
          BNuovo: TToolbarButton97;
          BElimina: TToolbarButton97;
          BOK: TToolbarButton97;
          BCan: TToolbarButton97;
          dxDBGrid1Stato: TdxDBGridPickColumn;
    QOfferteIDCliente: TIntegerField;
          procedure FormShow(Sender: TObject);
          procedure QOfferteAfterPost(DataSet: TDataSet);
          procedure DsQOfferteStateChange(Sender: TObject);
          procedure BNuovoClick(Sender: TObject);
          procedure BEliminaClick(Sender: TObject);
          procedure BOKClick(Sender: TObject);
          procedure BCanClick(Sender: TObject);
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QOfferteAfterInsert(DataSet: TDataSet);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     AzOfferteForm: TAzOfferteForm;

implementation

uses uASAAzienda;

{$R *.DFM}

procedure TAzOfferteForm.FormShow(Sender: TObject);
begin
     QOfferte.Open;
end;

procedure TAzOfferteForm.QOfferteAfterPost(DataSet: TDataSet);
var xID: integer;
begin
     xID:=QOfferteID.Value;
     QOfferte.Close;
     QOfferte.Open;
     QOfferte.Locate('ID',xID, []);
end;

procedure TAzOfferteForm.DsQOfferteStateChange(Sender: TObject);
var b: boolean;
begin
     b:=DsQOfferte.State in [dsEdit,dsInsert];
     BNuovo.Enabled:=not b;
     BElimina.Enabled:=not b;
     BOK.Enabled:=b;
     BCan.Enabled:=b;
end;

procedure TAzOfferteForm.BNuovoClick(Sender: TObject);
begin
     QOfferte.Insert;
end;

procedure TAzOfferteForm.BEliminaClick(Sender: TObject);
begin
     QOfferte.Delete;
end;

procedure TAzOfferteForm.BOKClick(Sender: TObject);
begin
     QOfferte.Post;
end;

procedure TAzOfferteForm.BCanClick(Sender: TObject);
begin
     QOfferte.cancel;
end;

procedure TAzOfferteForm.FormClose(Sender: TObject;
     var Action: TCloseAction);
begin
     if DsQOfferte.state in [dsinsert,dsedit] then QOfferte.Post;
end;

procedure TAzOfferteForm.QOfferteAfterInsert(DataSet: TDataSet);
begin
     QOfferteIDCliente.Value:=ASAAziendaForm.QAziendaID.Value;
end;

end.

