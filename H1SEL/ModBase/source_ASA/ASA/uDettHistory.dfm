object DettHistoryForm: TDettHistoryForm
  Left = 242
  Top = 265
  Width = 679
  Height = 334
  Caption = 'Dettaglio History'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 671
    Height = 43
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object BEliminaRiga: TToolbarButton97
      Left = 5
      Top = 1
      Width = 92
      Height = 39
      Caption = 'Elimina riga'
      Glyph.Data = {
        36050000424D3605000000000000360400002800000010000000100000000100
        08000000000000010000120B0000120B000000010000000100000021B5000831
        B5001839B5000029BD001031BD001039BD001839BD002142BD000029C6000831
        C6001039C6001839C6000031D6001039D6007B94D6001842DE002142DE00184A
        DE00214ADE00294ADE002952DE003152DE008494DE008C9CDE000039E7000839
        E7000842E7001042E700184AE700214AE7002152E7002952E7003152E700295A
        E700315AE700395AE7003963E7004263E7004A63E700426BE7004A6BE700526B
        E7005273E7000842EF00084AEF00104AEF002152EF00295AEF003163EF005A73
        EF005A7BEF00637BEF006384EF006B84EF007384EF006B8CEF00738CEF007B94
        EF008494EF00849CEF008C9CEF008CA5EF006B8CF7007394F70094ADF7009CAD
        F7009CB5F700A5B5F700B5BDF700B5C6F700BDC6F700BDCEF700B5C6FF00BDCE
        FF00DEE7FF00E7E7FF00E7EFFF00EFEFFF00EFF7FF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF004F4F4F4F4F4F
        4F4F4F4F4F4F4F4F4F4F4F160104060206050409090308000E4F4F0D0F131414
        1414121C1B1A180C004F4F0F1F2225394C4F4F4A3F2D2B18034F4F122325434F
        473837494F422C19084F4F1F253C4F3B2725302F3F4F3E1A094F4F22284D4628
        284F4F2F2E484A1C0A4F4F23294F3A29284F4F211E344F110B4F4F25314F3A29
        264F4F1F1E344F12064F4F28334E4529264F4F1F1D444B14074F4F2935404F3A
        26252320354F3814074F4F323938434F443635444F402014074F4F353D3A3740
        4E4F4F4D3C252315064F4F38413D3938353333322A282514064F4F443834312A
        2828282524232010174F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F}
      WordWrap = True
      OnClick = BEliminaRigaClick
    end
    object BitBtn1: TBitBtn
      Left = 565
      Top = 4
      Width = 101
      Height = 35
      Anchors = [akTop, akRight]
      Caption = 'Esci'
      TabOrder = 0
      Kind = bkOK
    end
  end
  object dxDBGrid1: TdxDBGrid
    Left = 0
    Top = 43
    Width = 671
    Height = 264
    Bands = <
      item
      end>
    DefaultLayout = True
    HeaderPanelRowCount = 1
    KeyField = 'ID'
    ShowGroupPanel = True
    SummaryGroups = <>
    SummarySeparator = ', '
    Align = alClient
    TabOrder = 1
    DataSource = DsQDettHistory
    Filter.Active = True
    Filter.Criteria = {00000000}
    OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
    OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoIndicator, edgoUseBitmap]
    object dxDBGrid1Evento: TdxDBGridMaskColumn
      Width = 152
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Evento'
    end
    object dxDBGrid1DataEvento: TdxDBGridDateColumn
      Caption = 'Data evento'
      Sorted = csDown
      Width = 99
      BandIndex = 0
      RowIndex = 0
      FieldName = 'DataEvento'
    end
    object dxDBGrid1Nominativo: TdxDBGridMaskColumn
      Caption = 'Utente'
      Width = 67
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Nominativo'
    end
    object dxDBGrid1Annotazioni: TdxDBGridMaskColumn
      Caption = 'Note evento'
      Width = 348
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Annotazioni'
      DisableFilter = True
    end
  end
  object QDettHistory: TQuery
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select EBC_Eventi.Evento, Nominativo, Storico.*'
      'from storico, EBC_Eventi, Users'
      'where storico.IDEvento = EBC_Eventi.ID'
      '  and storico.IDUtente = Users.ID'
      'and Storico.IDAnagrafica=875')
    Left = 48
    Top = 104
    object QDettHistoryEvento: TStringField
      FieldName = 'Evento'
      Origin = 'EBCDB.storico.ID'
      FixedChar = True
      Size = 30
    end
    object QDettHistoryNominativo: TStringField
      FieldName = 'Nominativo'
      Origin = 'EBCDB.storico.IDAnagrafica'
      FixedChar = True
      Size = 30
    end
    object QDettHistoryID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.storico.IDEvento'
    end
    object QDettHistoryIDAnagrafica: TIntegerField
      FieldName = 'IDAnagrafica'
      Origin = 'EBCDB.storico.DataEvento'
    end
    object QDettHistoryIDEvento: TIntegerField
      FieldName = 'IDEvento'
      Origin = 'EBCDB.storico.Annotazioni'
    end
    object QDettHistoryDataEvento: TDateTimeField
      FieldName = 'DataEvento'
      Origin = 'EBCDB.storico.IDRicerca'
    end
    object QDettHistoryAnnotazioni: TStringField
      FieldName = 'Annotazioni'
      Origin = 'EBCDB.storico.IDUtente'
      FixedChar = True
      Size = 50
    end
    object QDettHistoryIDRicerca: TIntegerField
      FieldName = 'IDRicerca'
      Origin = 'EBCDB.storico.IDCliente'
    end
    object QDettHistoryIDUtente: TIntegerField
      FieldName = 'IDUtente'
      Origin = 'EBCDB.EBC_Eventi.Evento'
    end
    object QDettHistoryIDCliente: TIntegerField
      FieldName = 'IDCliente'
      Origin = 'EBCDB.Users.Nominativo'
    end
  end
  object DsQDettHistory: TDataSource
    DataSet = QDettHistory
    Left = 48
    Top = 136
  end
end
