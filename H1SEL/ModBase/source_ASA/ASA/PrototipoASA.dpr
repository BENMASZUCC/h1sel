program PrototipoASA;

uses
  Forms,
  uASACand in 'uASACand.pas' {ASASchedaCandForm},
  uMainPrototipo in 'uMainPrototipo.pas' {MainForm},
  uData in 'uData.pas' {Data: TDataModule},
  uRelazioni in 'uRelazioni.pas' {RelazioniForm},
  uDettagliCand in 'uDettagliCand.pas' {DettagliCandForm},
  uASAAzienda in 'uASAAzienda.pas' {ASAAziendaForm},
  uAzOfferte in 'uAzOfferte.pas' {AzOfferteForm},
  uAzCandPresenti in 'uAzCandPresenti.pas' {AzCandPresentiForm};

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(TMainForm, MainForm);
  Application.CreateForm(TData, Data);
  Application.CreateForm(TASASchedaCandForm, ASASchedaCandForm);
  Application.CreateForm(TASAAziendaForm, ASAAziendaForm);
  Application.Run;
end.
