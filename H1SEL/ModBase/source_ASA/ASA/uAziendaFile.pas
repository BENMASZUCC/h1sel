unit uAziendaFile;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     StdCtrls,Buttons,dxDBTLCl,dxGrClms,dxTL,dxDBCtrl,dxDBGrid,
     dxCntner,Db,DBTables,TB97,ExtCtrls,ShellApi;

type
     TFileAziendaForm=class(TForm)
          Panel123: TPanel;
          BFileCandOpen: TToolbarButton97;
          DsQAziendaFile: TDataSource;
          dxDBGrid1: TdxDBGrid;
          BitBtn1: TBitBtn;
          BNew: TToolbarButton97;
          BDel: TToolbarButton97;
          BOK: TToolbarButton97;
          BCan: TToolbarButton97;
          QAziendaFile: TQuery;
          QAziendaFileID: TAutoIncField;
          QAziendaFileIDCliente: TIntegerField;
          QAziendaFileIDTipo: TIntegerField;
          QAziendaFileDescrizione: TStringField;
          QAziendaFileNomeFile: TStringField;
          QAziendaFileDataCreazione: TDateTimeField;
          dxDBGrid1Descrizione: TdxDBGridMaskColumn;
          UpdAnagFile: TUpdateSQL;
    dxDBGrid1NomeFile: TdxDBGridButtonColumn;
    OpenDialog1: TOpenDialog;
          procedure BFileCandOpenClick(Sender: TObject);
          procedure BNewClick(Sender: TObject);
          procedure BDelClick(Sender: TObject);
          procedure BOKClick(Sender: TObject);
          procedure BCanClick(Sender: TObject);
          procedure DsQAziendaFileStateChange(Sender: TObject);
          procedure QAziendaFileAfterPost(DataSet: TDataSet);
          procedure QAziendaFileAfterInsert(DataSet: TDataSet);
    procedure dxDBGrid1NomeFileButtonClick(Sender: TObject;
      AbsoluteIndex: Integer);
    procedure FormShow(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     FileAziendaForm: TFileAziendaForm;

implementation

uses AnagFile,ModuloDati,uASACand,uUtilsVarie,Main,uASAAzienda;

{$R *.DFM}

procedure TFileAziendaForm.BFileCandOpenClick(Sender: TObject);
var xFileName: string;
begin
     xFileName:=QAziendaFileNomeFile.Value;
     ShellExecute(0,'Open',pchar(xFileName),'','',SW_SHOW);
end;

procedure TFileAziendaForm.BNewClick(Sender: TObject);
begin
     QAziendaFile.Insert;
end;

procedure TFileAziendaForm.BDelClick(Sender: TObject);
begin
     if MessageDlg('Sei sicuro di voler cancellare la riga selezionata ?',mtWarning, [mbYes,mbNo],0)=mrNo then exit;
     QAziendaFile.Delete;
end;

procedure TFileAziendaForm.BOKClick(Sender: TObject);
begin
     QAziendaFile.Post;
end;

procedure TFileAziendaForm.BCanClick(Sender: TObject);
begin
     QAziendaFile.Cancel;
end;

procedure TFileAziendaForm.DsQAziendaFileStateChange(Sender: TObject);
var b: boolean;
begin
     b:=DsQAziendaFile.State in [dsEdit,dsInsert];
     BNew.enabled:=not b;
     BDel.enabled:=not b;
     BOK.enabled:=b;
     BCan.enabled:=b;
end;

procedure TFileAziendaForm.QAziendaFileAfterPost(DataSet: TDataSet);
begin
     QAziendaFile.ApplyUpdates;
     QAziendaFile.CommitUpdates;
     QAziendaFile.Close;
     QAziendaFile.Open;
end;

procedure TFileAziendaForm.QAziendaFileAfterInsert(DataSet: TDataSet);
begin
     QAziendaFileIDCliente.Value:=ASAAziendaForm.QAziendaID.Value;
end;

procedure TFileAziendaForm.dxDBGrid1NomeFileButtonClick(Sender: TObject;
  AbsoluteIndex: Integer);
begin
     if QAziendaFileNomeFile.Value<>'' then
        OpenDialog1.Filename:=QAziendaFileNomeFile.Value;
     if OpenDialog1.Execute then begin
        if not (dsQAziendaFile.state=dsEdit) then QAziendaFile.Edit;
        QAziendaFileNomeFile.Value:=OpenDialog1.FileName;
     end;
end;

procedure TFileAziendaForm.FormShow(Sender: TObject);
begin
     if not QAziendaFile.active then QAziendaFile.Open; 
end;

end.

