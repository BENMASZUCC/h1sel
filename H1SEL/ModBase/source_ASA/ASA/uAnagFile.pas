unit uAnagFile;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, dxDBTLCl, dxGrClms, dxTL, dxDBCtrl, dxDBGrid,
  dxCntner, Db, DBTables, TB97, ExtCtrls, ShellApi;

type
  TFileSoggettoForm = class(TForm)
    Panel123: TPanel;
    BAnagFileNew: TToolbarButton97;
    BAnagFileMod: TToolbarButton97;
    BAnagFileDel: TToolbarButton97;
    BFileCandOpen: TToolbarButton97;
    QAnagFile: TQuery;
    QAnagFileID: TAutoIncField;
    QAnagFileIDAnagrafica: TIntegerField;
    QAnagFileDescrizione: TStringField;
    QAnagFileNomeFile: TStringField;
    QAnagFileIDTipo: TIntegerField;
    QAnagFileTipo: TStringField;
    QAnagFileDataCreazione: TDateTimeField;
    DsQAnagFile: TDataSource;
    dxDBGrid1: TdxDBGrid;
    dxDBGrid1Descrizione: TdxDBGridMaskColumn;
    dxDBGrid1NomeFile: TdxDBGridMaskColumn;
    dxDBGrid1Tipo: TdxDBGridMaskColumn;
    dxDBGrid1DataCreazione: TdxDBGridDateColumn;
    BitBtn1: TBitBtn;
    procedure BAnagFileNewClick(Sender: TObject);
    procedure BAnagFileModClick(Sender: TObject);
    procedure BAnagFileDelClick(Sender: TObject);
    procedure BFileCandOpenClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FileSoggettoForm: TFileSoggettoForm;

implementation

uses AnagFile, ModuloDati, uASACand, uUtilsVarie, Main;

{$R *.DFM}

procedure TFileSoggettoForm.BAnagFileNewClick(Sender: TObject);
begin
     if not MainForm.CheckProfile('070') then Exit;
     AnagFileForm:=TAnagFileForm.create(self);
     AnagFileForm.ECognome.text:=ASASchedaCandForm.QAnagCognome.Value;
     AnagFileForm.ENome.text:=ASASchedaCandForm.QAnagNOme.Value;
     AnagFileForm.FEFile.InitialDir:=GetDocPath;
     AnagFileForm.ShowModal;
     if AnagFileForm.ModalResult=mrOK then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text:='insert into AnagFile (IDAnagrafica,IDTipo,Descrizione,NomeFile,DataCreazione) '+
                         'values (:xIDAnagrafica,:xIDTipo,:xDescrizione,:xNomeFile,:xDataCreazione)';
                    Q1.ParamByName('xIDAnagrafica').asInteger:=ASASchedaCandForm.QAnagID.Value;
                    Q1.ParamByName('xIDTipo').asInteger:=AnagFileForm.QTipiFileID.Value;
                    Q1.ParamByName('xDescrizione').asString:=AnagFileForm.EDesc.text;
                    Q1.ParamByName('xNomeFile').asString:=AnagFileForm.FEFile.filename;
                    Q1.ParamByName('xDataCreazione').asDateTime:=FileDateToDateTime(FileAge(AnagFileForm.FEFile.filename));
                    Q1.ExecSQL;
                    DB.CommitTrans;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               end;
          end;
          QAnagFile.Close;
          QAnagFile.Open;
     end;
     AnagFileForm.Free;
end;

procedure TFileSoggettoForm.BAnagFileModClick(Sender: TObject);
begin
     if not MainForm.CheckProfile('071') then Exit;
     AnagFileForm:=TAnagFileForm.create(self);
     AnagFileForm.ECognome.text:=ASASchedaCandForm.QAnagCognome.Value;
     AnagFileForm.ENome.text:=ASASchedaCandForm.QAnagNome.Value;
     while (AnagFileForm.QTipiFileID.value<>QAnagFileIDTipo.Value)and
          (not AnagFileForm.QTipiFile.EOF) do AnagFileForm.QTipiFile.Next;
     AnagFileForm.EDesc.text:=QAnagFileDescrizione.Value;
     AnagFileForm.FEFile.FileName:=QAnagFileNomeFile.Value;
     AnagFileForm.ShowModal;
     if AnagFileForm.ModalResult=mrOK then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text:='update AnagFile set IDTipo=:xIDTipo,Descrizione=:xDescrizione,NomeFile=:xNomeFile,DataCreazione=:xDataCreazione '+
                         'where ID='+QAnagFileID.asString;
                    Q1.ParamByName('xIDTipo').asInteger:=AnagFileForm.QTipiFileID.Value;
                    Q1.ParamByName('xDescrizione').asString:=AnagFileForm.EDesc.text;
                    Q1.ParamByName('xNomeFile').asString:=AnagFileForm.FEFile.filename;
                    Q1.ParamByName('xDataCreazione').asDateTime:=FileDateToDateTime(FileAge(AnagFileForm.FEFile.filename));
                    Q1.ExecSQL;
                    DB.CommitTrans;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               end;
          end;
          QAnagFile.Close;
          QAnagFile.Open;
     end;
     AnagFileForm.Free;
end;

procedure TFileSoggettoForm.BAnagFileDelClick(Sender: TObject);
begin
     if not MainForm.CheckProfile('071') then Exit;
     if MessageDlg('Sei sicuro di voler eliminare l''associazione ?',mtWarning, [mbNo,mbYes],0)=mrNo then exit;
     if MessageDlg('Vuoi eliminare il file fisico ?',mtWarning, [mbNo,mbYes],0)=mrYes then begin
          DeleteFile(QAnagFileNomeFile.Value);
     end;
     with Data do begin
          DB.BeginTrans;
          try
               Q1.Close;
               Q1.SQL.text:='delete from AnagFile where ID='+QAnagFileID.asString;
               Q1.ExecSQL;
               DB.CommitTrans;
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
          end;
     end;
     QAnagFile.Close;
     QAnagFile.Open;
end;

procedure TFileSoggettoForm.BFileCandOpenClick(Sender: TObject);
var xFileName: string;
begin
     xFileName:=QAnagFileNomeFile.Value;
     ShellExecute(0,'Open',pchar(xFileName),'','',SW_SHOW);
end;

end.
