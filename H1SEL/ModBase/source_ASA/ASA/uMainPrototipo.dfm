object MainForm: TMainForm
  Left = 241
  Top = 106
  Width = 604
  Height = 508
  ActiveControl = BitBtn1
  Caption = 'Main prototipo (1.3 - 13/06/2002)'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object PCMain: TPageControl
    Left = 0
    Top = 33
    Width = 596
    Height = 448
    ActivePage = TSCandidati
    Align = alClient
    TabOrder = 0
    object TSCandidati: TTabSheet
      Caption = 'Candidati'
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 588
        Height = 57
        Align = alTop
        BevelOuter = bvLowered
        TabOrder = 0
        object BitBtn1: TBitBtn
          Left = 8
          Top = 8
          Width = 109
          Height = 40
          Caption = 'Scheda candidato'
          TabOrder = 0
          OnClick = BitBtn1Click
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 57
        Width = 250
        Height = 363
        Align = alLeft
        TabOrder = 1
        object Panel3: TPanel
          Left = 1
          Top = 1
          Width = 248
          Height = 32
          Align = alTop
          BevelOuter = bvLowered
          TabOrder = 0
          object Label1: TLabel
            Left = 7
            Top = 9
            Width = 72
            Height = 13
            Caption = 'Ricerca rapida:'
          end
          object SpeedButton1: TSpeedButton
            Left = 218
            Top = 4
            Width = 26
            Height = 24
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              04000000000000010000120B0000120B00001000000000000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
              33333333333333333333333333333333333333333333333333FF333333333333
              3000333333FFFFF3F77733333000003000B033333777773777F733330BFBFB00
              E00033337FFF3377F7773333000FBFB0E000333377733337F7773330FBFBFBF0
              E00033F7FFFF3337F7773000000FBFB0E000377777733337F7770BFBFBFBFBF0
              E00073FFFFFFFF37F777300000000FB0E000377777777337F7773333330BFB00
              000033333373FF77777733333330003333333333333777333333333333333333
              3333333333333333333333333333333333333333333333333333333333333333
              3333333333333333333333333333333333333333333333333333}
            NumGlyphs = 2
            OnClick = SpeedButton1Click
          end
          object Edit1: TEdit
            Left = 84
            Top = 5
            Width = 130
            Height = 21
            TabOrder = 0
            Text = 'BORSARI'
            OnKeyPress = Edit1KeyPress
          end
        end
        object DBGrid1: TDBGrid
          Left = 1
          Top = 33
          Width = 248
          Height = 329
          Align = alClient
          DataSource = Data.DsAnagrafica
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Cognome'
              Width = 113
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Width = 100
              Visible = True
            end>
        end
      end
    end
    object TSAziende: TTabSheet
      Caption = 'Aziende'
      ImageIndex = 1
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 588
        Height = 57
        Align = alTop
        BevelOuter = bvLowered
        TabOrder = 0
        object BitBtn2: TBitBtn
          Left = 8
          Top = 8
          Width = 109
          Height = 40
          Caption = 'Scheda azienda'
          TabOrder = 0
          OnClick = BitBtn2Click
        end
      end
      object Panel6: TPanel
        Left = 0
        Top = 57
        Width = 250
        Height = 363
        Align = alLeft
        TabOrder = 1
        object Panel7: TPanel
          Left = 1
          Top = 1
          Width = 248
          Height = 32
          Align = alTop
          BevelOuter = bvLowered
          TabOrder = 0
          object Label2: TLabel
            Left = 7
            Top = 9
            Width = 72
            Height = 13
            Caption = 'Ricerca rapida:'
          end
          object SpeedButton2: TSpeedButton
            Left = 218
            Top = 4
            Width = 26
            Height = 24
            Glyph.Data = {
              76010000424D7601000000000000760000002800000020000000100000000100
              04000000000000010000120B0000120B00001000000000000000000000000000
              800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
              33333333333333333333333333333333333333333333333333FF333333333333
              3000333333FFFFF3F77733333000003000B033333777773777F733330BFBFB00
              E00033337FFF3377F7773333000FBFB0E000333377733337F7773330FBFBFBF0
              E00033F7FFFF3337F7773000000FBFB0E000377777733337F7770BFBFBFBFBF0
              E00073FFFFFFFF37F777300000000FB0E000377777777337F7773333330BFB00
              000033333373FF77777733333330003333333333333777333333333333333333
              3333333333333333333333333333333333333333333333333333333333333333
              3333333333333333333333333333333333333333333333333333}
            NumGlyphs = 2
            OnClick = SpeedButton2Click
          end
          object Edit2: TEdit
            Left = 84
            Top = 5
            Width = 130
            Height = 21
            TabOrder = 0
            Text = 'DELTA'
            OnKeyPress = Edit2KeyPress
          end
        end
        object DBGrid2: TDBGrid
          Left = 1
          Top = 33
          Width = 248
          Height = 329
          Align = alClient
          DataSource = Data.DsQAziende
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Descrizione'
              Title.Caption = 'Denominazione'
              Width = 214
              Visible = True
            end>
        end
      end
    end
    object TSCommesse: TTabSheet
      Caption = 'Commesse'
      ImageIndex = 2
      object Panel8: TPanel
        Left = 0
        Top = 0
        Width = 588
        Height = 57
        Align = alTop
        BevelOuter = bvLowered
        TabOrder = 0
        object BitBtn3: TBitBtn
          Left = 8
          Top = 8
          Width = 109
          Height = 40
          Caption = 'Scheda commessa'
          TabOrder = 0
          OnClick = BitBtn3Click
        end
      end
    end
  end
  object Panel4: TPanel
    Left = 0
    Top = 0
    Width = 596
    Height = 33
    Align = alTop
    BevelOuter = bvLowered
    Caption = 
      'ATTENZIONE:  i dati sono quelli VERI del database !!  Occhio all' +
      'e modifiche !!!'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
  end
end
