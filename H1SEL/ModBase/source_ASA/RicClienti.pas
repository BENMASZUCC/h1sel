unit RicClienti;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Mask, DBCtrls, Grids, DBGrids, Db, DBTables, Buttons, TB97,
     ExtCtrls, ComCtrls, ComObj, RXDBCtrl, Psock, NMsmtp, dxDBTLCl, dxGrClms,
     dxTL, dxDBCtrl, dxDBGrid, dxCntner, dxGridMenus, dxPSCore, dxPSdxTLLnk,
     dxPSdxDBGrLnk, ImgList, Menus, dxPSdxDBCtrlLnk;

type
     TSaveMethod = procedure(const FileName: string; ASaveAll: Boolean) of object;
     TRicClientiForm = class(TForm)
          TTabQuery: TTable;
          DsTabQuery: TDataSource;
          TTabQueryTabella: TStringField;
          TTabQueryDescTabella: TStringField;
          TTabQueryDescCampo: TStringField;
          TTabQueryTabLookup: TStringField;
          TTabOp: TTable;
          DsTabOp: TDataSource;
          TTabQueryID: TAutoIncField;
          DsLinee: TDataSource;
          Query1: TQuery;
          TTabQueryTipoCampo: TStringField;
          TTabLookup: TTable;
          DsTabLookup: TDataSource;
          TTabLookupID: TAutoIncField;
          TTabQueryFieldLookup: TStringField;
          TTabLookupLingua: TStringField;
          TTabQueryFieldLookupSize: TSmallintField;
          QRes1: TQuery;
          DsQRes1: TDataSource;
          QTabelleLista: TQuery;
          QTabelleListaTabella: TStringField;
          DsQTabLista: TDataSource;
          TTabQueryCampo: TStringField;
          TTabQueryValoriPossibili: TStringField;
          QTabelle: TQuery;
          QTabelleDescTabella: TStringField;
          PanRic: TPanel;
          DBGrid3: TDBGrid;
          BitBtn3: TBitBtn;
          GroupBox1: TGroupBox;
          Label1: TLabel;
          Label2: TLabel;
          Label3: TLabel;
          Label4: TLabel;
          DBGrid4: TDBGrid;
          ComboBox1: TComboBox;
          DBGrid1: TDBGrid;
          DBGrid2: TDBGrid;
          Edit1: TEdit;
          BitBtn1: TBitBtn;
          ComboBox2: TComboBox;
          BitBtn6: TBitBtn;
          BitBtn2: TBitBtn;
          TTabQueryTabLookupIdx: TStringField;
          Panel1: TPanel;
          TTabQueryFormLookupID: TIntegerField;
          BApriForm: TBitBtn;
          Q: TQuery;
          Panel2: TPanel;
          Panel3: TPanel;
          Panel10: TPanel;
          LTot: TLabel;
          TTabOpIDCliTabQuery: TIntegerField;
          TTabOpOperatore: TStringField;
          TTabOpDescOperatore: TStringField;
          QRes1Cliente: TStringField;
          QRes1Stato: TStringField;
          QRes1Comune: TStringField;
          QRes1Provincia: TStringField;
          QRes1Contatto: TStringField;
          QRes1Telefono: TStringField;
          BitBtn4: TBitBtn;
          QRes1IDCliente: TAutoIncField;
          QRes1IDContatto: TAutoIncField;
          QRes1DataUltimoCont: TDateTimeField;
          BitBtn5: TBitBtn;
          QRes1Cap: TStringField;
          QRes1Indirizzo: TStringField;
          QRes1Fax: TStringField;
          BitBtn7: TBitBtn;
          NMSMTP1: TNMSMTP;
          QRes1email: TStringField;
          BitBtn8: TBitBtn;
          QRes1EtichettaAbitazione: TBooleanField;
          QRes1IndirizzoPrivato: TStringField;
          QRes1CapPrivato: TStringField;
          QRes1ProvinciaPrivato: TStringField;
          QRes1Titolo: TStringField;
          QRes1Paese: TStringField;
          QRes1ComunePrivato: TStringField;
          QRes1Sesso: TStringField;
          QRes1NazioneAbb: TStringField;
          QRes1DivisioneAziendale: TStringField;
          QRes1CaricaAziendale: TStringField;
          QRes1TipoStradaPrivato: TStringField;
          QRes1NumCivicoPrivato: TStringField;
          QRes1TipoStrada: TStringField;
          QRes1NumCivico: TStringField;
          DBGrid5: TdxDBGrid;
          PMExport: TPopupMenu;
          Stampagriglia1: TMenuItem;
          EsportainExcel1: TMenuItem;
          EsportainHTML1: TMenuItem;
          ImageList1: TImageList;
          dxPrinter1: TdxComponentPrinter;
          dxPrinter1Link1: TdxDBGridReportLink;
          SaveDialog: TSaveDialog;
          ToolbarButton972: TToolbarButton97;
          DBGrid5Cliente: TdxDBGridMaskColumn;
          DBGrid5Stato: TdxDBGridMaskColumn;
          DBGrid5Comune: TdxDBGridMaskColumn;
          DBGrid5Provincia: TdxDBGridMaskColumn;
          DBGrid5Contatto: TdxDBGridMaskColumn;
          DBGrid5Telefono: TdxDBGridMaskColumn;
          DBGrid5IDCliente: TdxDBGridMaskColumn;
          DBGrid5IDContatto: TdxDBGridMaskColumn;
          DBGrid5DataUltimoCont: TdxDBGridDateColumn;
          DBGrid5Titolo: TdxDBGridMaskColumn;
          DBGrid5Paese: TdxDBGridMaskColumn;
          DBGrid5Sesso: TdxDBGridMaskColumn;
          BAddTargetList: TBitBtn;
          TLinee: TQuery;
          TLineeID: TAutoIncField;
          TLineeIDAnagrafica: TIntegerField;
          TLineeDescrizione: TStringField;
          TLineeStringa: TStringField;
          TLineeTabella: TStringField;
          TLineeOpSucc: TStringField;
          TLineeOpNext: TStringField;
          BitBtn10: TBitBtn;
          procedure ComboBox1Change(Sender: TObject);
          procedure BitBtn1Click(Sender: TObject);
          procedure TTabQueryAfterScroll(DataSet: TDataSet);
          procedure BitBtn3Click(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure BitBtn6Click(Sender: TObject);
          procedure BitBtn2Click(Sender: TObject);
          procedure DBGrid4KeyPress(Sender: TObject; var Key: Char);
          procedure TLineeBeforePost(DataSet: TDataSet);
          procedure BApriFormClick(Sender: TObject);
          procedure BitBtn4Click(Sender: TObject);
          procedure BitBtn5Click(Sender: TObject);
          procedure BitBtn7Click(Sender: TObject);
          procedure BitBtn8Click(Sender: TObject);
          procedure DBGrid5MouseUp(Sender: TObject; Button: TMouseButton;
               Shift: TShiftState; X, Y: Integer);
          procedure Stampagriglia1Click(Sender: TObject);
          procedure EsportainExcel1Click(Sender: TObject);
          procedure EsportainHTML1Click(Sender: TObject);
          procedure BAddTargetListClick(Sender: TObject);
          procedure TLineeAfterPost(DataSet: TDataSet);
          procedure BitBtn10Click(Sender: TObject);
     private
          { Private declarations }
          xStringa, xIndexName, xOrdine: string;
          xFormID: integer;
          xggDiffDi, xggDiffUc, xggDiffCol: integer;
          xInvio: integer;
          function CreaFileWorddaRicClienti(xIDModello: integer; xModelloWord: string; xSalva, xStampa: boolean): string;
          function SpedisciMail: string;
          procedure Save(ADefaultExt, AFilter, AFileName: string; AMethod: TSaveMethod);
     public
          xDaEscludere: array[1..500] of integer;
          xChiamante, xIDRicerca, xIDClienteRic: integer;
          procedure StoricizzaInvio(xIDContattoCli, xIDTipoInvio: integer; xAbitazione: boolean; xTipo, xNote: string);
     end;

var
     RicClientiForm: TRicClientiForm;

implementation

uses ModuloDati, Main, InsRuolo, SelCompetenza, Comuni,
     SceltaModello, SceltaModelloMailCli, EmailMessage, QREtichette1,
     OpzioniEtichette, uutilsvarie, SelPers, uASAAzienda, ModuloDati2;

{$R *.DFM}

procedure TRicClientiForm.ComboBox1Change(Sender: TObject);
var MyList: TStrings;
     i: integer;
     xTrovato: boolean;
begin
     TTabQuery.Filter := 'DescTabella=''' + ComboBox1.Text + '''';
     BApriForm.Visible := False;
     if TTabQueryTabLookup.AsString <> '' then begin
          if TTabQueryFormLookupID.asString <> '' then begin
               BApriForm.Visible := True;
               xFormID := TTabQueryFormLookupID.Value;
               xIndexName := TTabQueryTabLookupIdx.Value;
          end;
          ComboBox2.Visible := False;
          ComboBox2.Enabled := False;
          ComboBox2.Color := clBtnFace;
          Edit1.Visible := False;
          Edit1.Enabled := False;
          Edit1.Color := clBtnFace;

          TTabLookup.Close;
          if TTabQueryTabLookupIdx.Value <> '' then
               TTabLookup.IndexName := TTabQueryTabLookupIdx.Value
          else TTabLookup.IndexName := '';

          TTabLookup.TableName := 'dbo.' + TTabQueryTabLookup.asString;
          TTabLookup.Fields[1].FieldName := 'Pippo';
          TTabLookup.Fields[1].FieldName := TTabQueryFieldLookup.asString;
          TTabLookup.Fields[1].Size := TTabQueryFieldLookupSize.VAlue;
          DBGrid4.Columns[0].FieldName := TTabQueryFieldLookup.asString;
          TTabLookup.Open;
          DBGrid4.Visible := True;
          DBGrid4.Enabled := True;
          DBGrid4.Color := clWindow;
          xStringa := '';
     end else begin
          DBGrid4.Visible := False;
          DBGrid4.Enabled := False;
          DBGrid4.Color := clBtnFace;
          BApriForm.Visible := False;
          Edit1.Visible := True;
          Edit1.Enabled := true;
          Edit1.Color := clWindow;
     end;
     DBGrid1.Enabled := True;
     DBGrid1.Color := clWindow;
     DBGrid2.Enabled := True;
     DBGrid2.Color := clWindow;
end;

procedure TRicClientiForm.BitBtn1Click(Sender: TObject);
var xVal, xValDesc, xOp: string;
     xTipoStringa: boolean;
begin
     if (xInvio > 0) and (TTabQueryTabella.asString = 'ContattoTipoInvio') then begin
          ShowMessage('esiste gi� un invio selezionato');
          exit;
     end;

     DBGrid5.Visible := False;
     LTot.Visible := False;
     if TTabQueryValoriPossibili.asString <> '' then begin
          xVal := ComboBox2.text;
          if ComboBox2.text = 's�' then xVal := '1';
          if ComboBox2.text = 'no' then xVal := '0';
          xValDesc := ComboBox2.text;
     end else begin
          xVal := UpperCase(Edit1.text);
          xValDesc := Edit1.Text;
     end;

     xTipoStringa := False;
     if TTabOpOperatore.asString = 'like' then
          xVal := '''%' + xVal + '%'''
     else
          if not ((TTabQueryTipoCampo.asString = 'boolean') or
               (TTabQueryTipoCampo.asString = 'integer')) then begin
               xVal := '''' + xVal + '''';
               xTipoStringa := True;
          end;

     if TTabQueryTabLookup.AsString <> '' then begin
          xVal := TTabLookupID.asString;
          xValDesc := TTabLookup.Fields[1].asString;
     end;

     if xTipoStringa then begin
          if TTabQueryTipoCampo.asString = 'date' then
               TLinee.InsertRecord([TTabQueryDescCampo.asString + ' ' +
                    TTabOpDescOperatore.asString + ' ' +
                         xValDesc,
                         TTabQueryTabella.asString + '.' + TTabQueryCampo.asString +
                         TTabOpOperatore.asString + ' ' +
                         xVal,
                         TTabQueryTabella.asString,
                         'and', 'e',
                         MainForm.xIDUtenteAttuale])
          else
               TLinee.InsertRecord([TTabQueryDescCampo.asString + ' ' +
                    TTabOpDescOperatore.asString + ' ' +
                         xValDesc,
                         'UPPER(' + TTabQueryTabella.asString + '.' + TTabQueryCampo.asString + ') ' +
                         TTabOpOperatore.asString + ' ' +
                         xVal,
                         TTabQueryTabella.asString,
                         'and', 'e',
                         MainForm.xIDUtenteAttuale]);
     end else
          TLinee.InsertRecord([TTabQueryDescCampo.asString + ' ' +
               TTabOpDescOperatore.asString + ' ' +
                    xValDesc,
                    TTabQueryTabella.asString + '.' + TTabQueryCampo.asString + ' ' +
                    TTabOpOperatore.asString + ' ' +
                    xVal,
                    TTabQueryTabella.asString,
                    'and', 'e',
                    MainForm.xIDUtenteAttuale]);
     if TTabQueryTabella.asString = 'ContattoTipoInvio' then
          xInvio := TTabLookupID.Value;
     TLinee.Close;
     TLinee.Open;
end;

procedure TRicClientiForm.TTabQueryAfterScroll(DataSet: TDataSet);
var MyList: TStrings;
     i, xPos: integer;
     xTrovato: boolean;
     xS: string;
begin
     if (TTabQueryValoriPossibili.asString <> '') and (TTabQueryTabLookup.AsString = '') then begin
          if TTabQueryTipoCampo.asString <> 'boolean' then begin
               // riempimento
               ComboBox2.items.Clear;
               xS := TTabQueryValoriPossibili.asString;
               while pos(',', xS) > 0 do begin
                    ComboBox2.items.Add(copy(xS, 1, pos(',', xS) - 1));
                    xS := copy(xS, pos(',', xS) + 1, length(xS));
               end;
               ComboBox2.items.Add(xS);
          end;
          ComboBox2.Visible := True;
          ComboBox2.Enabled := True;
          ComboBox2.Color := clWindow;
          DBGrid4.Visible := False;
          BApriForm.Visible := False;
          Edit1.Visible := False;
          Edit1.Enabled := False;
          Edit1.Color := clBtnFace;
     end else begin
          if TTabQueryTabLookup.AsString <> '' then begin
               if TTabQueryFormLookupID.asString <> '' then begin
                    BApriForm.Visible := True;
                    xFormID := TTabQueryFormLookupID.Value;
                    xIndexName := TTabQueryTabLookupIdx.Value;
               end;
               ComboBox2.Visible := False;
               ComboBox2.Enabled := False;
               ComboBox2.Color := clBtnFace;
               Edit1.Visible := False;
               Edit1.Enabled := False;
               Edit1.Color := clBtnFace;

               TTabLookup.Close;
               TTabLookup.TableName := 'dbo.' + TTabQueryTabLookup.asString;
               if TTabQueryTabLookupIdx.Value <> '' then
                    TTabLookup.IndexName := TTabQueryTabLookupIdx.Value
               else TTabLookup.IndexName := '';

               TTabLookup.Fields[1].FieldName := 'Pippo';
               TTabLookup.Fields[1].FieldName := TTabQueryFieldLookup.asString;
               TTabLookup.Fields[1].Size := TTabQueryFieldLookupSize.VAlue;
               DBGrid4.Columns[0].FieldName := TTabQueryFieldLookup.asString;
               TTabLookup.Open;
               DBGrid4.Visible := True;
               DBGrid4.Enabled := True;
               xStringa := '';
               DBGrid4.Color := clWindow;
          end else begin
               DBGrid4.Visible := False;
               DBGrid4.Enabled := False;
               DBGrid4.Color := clBtnFace;
               BApriForm.Visible := False;
               Edit1.Visible := True;
               Edit1.Enabled := true;
               Edit1.Color := clWindow;
          end;

          ComboBox2.Visible := False;
          ComboBox2.Enabled := False;
          ComboBox2.Color := clBtnFace;
          {        Edit1.Visible:=True;
                  Edit1.Enabled:=true;
                  Edit1.Color:=clWindow;
                  Edit1.text:='';
          }
     end;
end;

procedure TRicClientiForm.BitBtn3Click(Sender: TObject);
begin
     DBGrid5.Visible := False;
     LTot.Visible := False;
     if TLineeTabella.Value = 'ContattoTipoInvio' then xInvio := 0;
     TLinee.Delete;
end;

procedure TRicClientiForm.FormShow(Sender: TObject);
var i: integer;
begin
     Caption := '[M/171] - ' + Caption;
     xOrdine := 'order by EBC_Clienti.Descrizione';
     Edit1.Enabled := False;
     Edit1.Color := clBtnFace;
     xInvio := 0;
     TLinee.ParamByName('xIDAnag').asInteger := MainForm.xIDUtenteAttuale;
     TLinee.Open;
     while not TLinee.EOF do begin
          if TLineeTabella.Value = 'ContattoTipoInvio' then begin
               TLinee.Delete;
               break;
          end;
          TLinee.Next;
     end;
end;

procedure TRicClientiForm.BitBtn6Click(Sender: TObject);
begin
     DBGrid5.Visible := False;
     LTot.Visible := False;
     Query1.Close;
     Query1.SQl.Clear;
     Query1.SQl.add('delete from CliLineeQuery where IDAnagrafica=' + IntToStr(MainForm.xIDUtenteAttuale));
     Query1.ExecSQL;
     TTabQuery.Refresh;
     TLinee.Close;
     TLinee.Open;
     xInvio := 0;
end;

procedure TRicClientiForm.BitBtn2Click(Sender: TObject);
var xTab: array[1..5] of string;
     i, j: integer;
     xS, xS1, xd, xLineeStringa: string;
     xApertaPar, xEspLav: boolean;
begin
     //
     if DsLinee.State in [dsInsert, dsEdit] then TLinee.Post;
     TLinee.Last;
     if TLineeOpSucc.Value <> '' then begin
          TLinee.Edit;
          TLineeOpNext.Value := 'fine';
          TLinee.Post;
     end;
     // tabelle implicate
     QTabelleLista.Close;
     QTabelleLista.SQL.clear;
     QTabelleLista.SQL.add('select distinct Tabella from CliLineeQuery ');
     QTabelleLista.SQL.add('where Tabella<>''EBC_Clienti'' and Tabella<>''EBC_ContattiClienti'' and IDAnagrafica=' + IntToStr(MainForm.xIDUtenteAttuale));
     QTabelleLista.Open;
     QTabelleLista.First;
     for i := 1 to 5 do xTab[i] := '';
     i := 1;
     while not QTabelleLista.EOF do begin
          xTab[i] := QTabelleListaTabella.AsString;
          QTabelleLista.Next; inc(i);
     end;

     xEspLav := False;
     xS := 'from EBC_Clienti,EBC_ContattiClienti,EBC_ContattiFatti'; //*********
     for i := 1 to 5 do begin
          if xTab[i] <> '' then
               xS := xs + ',' + xTab[i];
     end;

     QRes1.Close;
     QRes1.SQL.Clear;
     // le note non funzionano !!!
     if (pos('ADVANT', Uppercase(Data.GlobalNomeAzienda.Value)) > 0) then
          QRes1.SQL.Add('select distinct EBC_Clienti.ID IDCliente, EBC_Clienti.Descrizione Cliente,EBC_Clienti.Stato,Comune,Provincia,Sesso, ' +
               'EBC_ContattiClienti.Contatto, EBC_Clienti.Telefono, EBC_ContattiClienti.ID IDContatto, EBC_ContattiFatti.Data DataUltimoCont, ' +
               'EBC_Clienti.Indirizzo,EBC_Clienti.Cap,EBC_ContattiClienti.Fax,EBC_ContattiClienti.email,NazioneAbb, CaricaAziendale, DivisioneAziendale, ' +
               'EtichettaAbitazione,IndirizzoPrivato,CapPrivato,ProvinciaPrivato,Titolo,EBC_ContattiClienti.Stato Paese,ComunePrivato, ' +
               'TipoStradaPrivato,NumCivicoPrivato,EBC_Clienti.TipoStrada,EBC_Clienti.NumCivico')
     else
          QRes1.SQL.Add('select distinct EBC_Clienti.ID IDCliente, EBC_Clienti.Descrizione Cliente,EBC_Clienti.Stato,Comune,Provincia,Sesso, ' +
               'EBC_ContattiClienti.Contatto, EBC_ContattiClienti.Telefono, EBC_ContattiClienti.ID IDContatto, EBC_ContattiFatti.Data DataUltimoCont, ' +
               'EBC_Clienti.Indirizzo,EBC_Clienti.Cap,EBC_ContattiClienti.Fax,EBC_ContattiClienti.email,NazioneAbb, CaricaAziendale, DivisioneAziendale, ' +
               'EtichettaAbitazione,IndirizzoPrivato,CapPrivato,ProvinciaPrivato,Titolo,EBC_ContattiClienti.Stato Paese,ComunePrivato, ' +
               'TipoStradaPrivato,NumCivicoPrivato,EBC_Clienti.TipoStrada,EBC_Clienti.NumCivico');
     QRes1.SQL.Add(xS + ' ');
     QRes1.SQL.Add('where EBC_Clienti.ID *= EBC_ContattiClienti.IDCliente');
     QRes1.SQL.Add('and EBC_Clienti.ID *= EBC_ContattiFatti.IDCLiente');
     QRes1.SQL.Add('and EBC_ContattiFatti.Data = (select max(Data) from EBC_ContattiFatti ');
     QRes1.SQL.Add('                              where EBC_Clienti.ID *= EBC_ContattiFatti.IDCLiente)');

     xS1 := '';
     for i := 1 to 5 do begin
          if xTab[i] <> '' then
               if xTab[i] = 'ContattoTipoInvio' then
                    xS1 := xS1 + ' and EBC_ContattiClienti.ID=' + xTab[i] + '.IDContattocli'
               else xS1 := xS1 + ' and EBC_Clienti.ID=' + xTab[i] + '.IDCliente';
     end;
     QRes1.SQL.Add(xS1);
     if TLinee.RecordCount > 0 then
          QRes1.SQL.Add('and');

     TLinee.First;
     xLineeStringa := '';
     xApertaPar := False;
     while not TLinee.EOF do begin
          xLineeStringa := TLineeStringa.asString;
          TLinee.Next;
          if TLinee.EOF then
               // ultimo record -> senza AND alla fine
               if xApertaPar then QRes1.SQL.Add(' ' + xLineeStringa + ')')
               else QRes1.SQL.Add(' ' + xLineeStringa)
          else begin
               TLinee.Prior;
               if TLineeOpSucc.AsString = 'and' then
                    if not xApertaPar then
                         QRes1.SQL.Add(' ' + xLineeStringa + ' and ')
                    else begin
                         QRes1.SQL.Add(' ' + xLineeStringa + ') and ');
                         xApertaPar := False;
                    end;
               if TLineeOpSucc.AsString = 'or' then
                    if not xApertaPar then begin
                         QRes1.SQL.Add(' (' + xLineeStringa + ' or ');
                         xApertaPar := True;
                    end else begin
                         QRes1.SQL.Add(' ' + xLineeStringa + ' or ');
                         xApertaPar := True;
                    end;
          end;
          TLinee.Next;
     end;

     QRes1.SQL.Add(xOrdine);
     ScriviRegistry('QRes1Clienti', QRes1.SQL.text);

     //QRes1.ExecSQL;
     QRes1.Open;
     DBGrid5.Visible := True;
     // totale
     LTot.Caption := IntToStr(QRes1.RecordCount);
     LTot.Visible := True;
end;

procedure TRicClientiForm.DBGrid4KeyPress(Sender: TObject; var Key: Char);
begin
     if TTabLookup.IndexName <> '' then begin
          if Key <> chr(13) then begin
               xStringa := xStringa + key;
               TTabLookup.FindNearest([xStringa]);
          end;
     end;
end;

procedure TRicClientiForm.TLineeBeforePost(DataSet: TDataSet);
begin
     if TLineeOpNext.Value = 'e' then TLineeOpSucc.Value := 'and';
     if TLineeOpNext.Value = 'o' then TLineeOpSucc.Value := 'or';
     if TLineeOpNext.Value = 'fine' then TLineeOpSucc.Value := '';
end;

procedure TRicClientiForm.BApriFormClick(Sender: TObject);
begin
     case xFormID of
          1: begin
                    //
               end;
     end;
end;

procedure TRicClientiForm.BitBtn4Click(Sender: TObject);
begin
     Close;
end;

procedure TRicClientiForm.BitBtn5Click(Sender: TObject);
var xFile, xDesc: string;
     xSalva, xStampa: boolean;
     k: integer;
begin
     if (not (QRes1.active)) or (QRes1.RecordCount = 0) then exit;
     SceltaModelloForm := TSceltaModelloForm.create(self);
     SceltaModelloForm.QModelliWord.SQL.text := 'select * from ModelliWord ' +
          'where TabellaMaster=:xTabMaster';
     SceltaModelloForm.QModelliWord.ParamByName('xTabMaster').asString := '*RicClienti';
     SceltaModelloForm.QModelliWord.Open;
     if xInvio > 0 then begin
          SceltaModelloForm.Height := 351;
          // posizionati sul modello word (--> SE ASSOCIATO AL TIPO DI INVIO)
          Data.Q1.Close;
          Data.Q1.SQL.text := 'select IDModelloWord from TipiInviiContatti where ID=' + IntTostr(xInvio);
          Data.Q1.Open;
          if Data.Q1.FieldByName('IDModelloWord').asString <> '' then
               while (not SceltaModelloForm.QModelliWord.Eof) and (SceltaModelloForm.QModelliWordID.Value <> Data.Q1.FieldByName('IDModelloWord').asInteger) do
                    SceltaModelloForm.QModelliWord.Next;
          Data.Q1.Close;
     end;
     SceltaModelloForm.ShowModal;
     if SceltaModelloForm.ModalResult = mrOK then begin
          if (SceltaModelloForm.RGOpzione.ItemIndex = 0) or (SceltaModelloForm.RGOpzione.ItemIndex = 2) then xStampa := True
          else xStampa := False;
          xSalva := SceltaModelloForm.CBSalva.Checked;
          if SceltaModelloForm.RGOpzione.ItemIndex = 0 then begin
               // PER tutti quelli selezionati
               for k := 0 to DBGrid5.SelectedCount - 1 do begin
                    QRes1.BookMark := DBGrid5.SelectedRows[k];
                    // crea file Word (con opzioni stampa/salva)
                    xFile := CreaFileWorddaRicClienti(SceltaModelloForm.QModelliWordID.Value,
                         SceltaModelloForm.QModelliWordNomeModello.Value,
                         xSalva, xStampa);
                    if SceltaModelloForm.CBStoricizza.Checked then
                         // storicizzazione (se impostata)
                         StoricizzaInvio(QRes1IDContatto.Value,
                              SceltaModelloForm.QModelliWordID.Value,
                              QRes1EtichettaAbitazione.Value,
                              'W',
                              SceltaModelloForm.QModelliWordDescrizione.Value);

               end;
          end else begin
               // SINGOLO SELEZIONATO
               xFile := CreaFileWorddaRicClienti(SceltaModelloForm.QModelliWordID.Value,
                    SceltaModelloForm.QModelliWordNomeModello.Value,
                    xSalva, xStampa);
               if SceltaModelloForm.CBStoricizza.Checked then
                    // storicizzazione (se impostata)
                    StoricizzaInvio(QRes1IDContatto.Value,
                         SceltaModelloForm.QModelliWordID.Value,
                         QRes1EtichettaAbitazione.Value,
                         'W',
                         SceltaModelloForm.QModelliWordDescrizione.Value);
          end;

          xDesc := SceltaModelloForm.QModelliWordDescrizione.Value;
          // salvataggio file in CliFileWord
          //
          // ==> DA VERIFICARE NECESSITA' e se va bene qui !!
          //
          if (xFile <> '') and (xSalva) then begin
               with Data do begin
                    DB.BeginTrans;
                    try
                         Q1.close;
                         Q1.SQL.Text := 'insert into CliFileWord (IDCliente,IDModelloWord,Descrizione,NomeFile,Data) ' +
                              'values (:xIDCliente,:xIDModelloWord,:xDescrizione,:xNomeFile,:xData)';
                         Q1.ParamByName('xIDCliente').asInteger := QRes1IDCliente.value;
                         Q1.ParamByName('xIDModelloWord').asInteger := SceltaModelloForm.QModelliWordID.Value;
                         Q1.ParamByName('xDescrizione').asString := xDesc;
                         Q1.ParamByName('xNomeFile').asString := xFile;
                         Q1.ParamByName('xData').asDateTime := Date;
                         Q1.ExecSQL;
                         DB.CommitTrans;
                    except
                         DB.RollbackTrans;
                         MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                    end;
               end;
          end;
     end;
     SceltaModelloForm.Free;
end;

function TRicClientiForm.CreaFileWorddaRicClienti(xIDModello: integer; xModelloWord: string; xSalva, xStampa: boolean): string;
var xFileModello, xFileWord: string;
     xAnno, xMese, xGiorno: Word;
     MSWord: Variant;
     xApriWord, xNuovoFile: boolean;
begin
     if not FileExists(GetDocPath + '\' + xModelloWord + '.doc') then begin
          MessageDlg('Il modello non esiste o la cartella non � impostata correttamente' + chr(13) +
               'IMPOSSIBILE PROSEGUIRE', mtError, [mbOK], 0);
          Result := '';
          exit;
     end;
     with Data.QTemp do begin
          Close;
          SQL.text := 'select * from ModelliWord where ID=' + IntToStr(xIDModello);
          Open;
          DecodeDate(Date, xAnno, xMese, xGiorno);
          xFileWord := GetDocPath + '\' +
               FieldbyName('InizialiFileGenerato').asString + IntToStr(QRes1IDCliente.Value) +
               '-' + IntToStr(xGiorno) + IntToStr(xMese) + '.doc';
          // Apri Word
          try MsWord := CreateOleObject('Word.Basic');
          except
               ShowMessage('Non riesco ad aprire Microsoft Word.');
               exit;
          end;
          MsWord.AppShow;
          // apri modello
          MsWord.FileOpen(GetDocPath + '\' + xModelloWord + '.doc');
          // riempimento campi
          Close;
          SQL.text := 'select NomeCampo,Campo,TipoCampo from ModelliWordCampi where IDModello=:xIDMod order by ID';
          ParamByName('xIDMod').asInteger := xIDModello;
          Open;
          while not EOF do begin
               MsWord.EditFind(FieldbyName('NomeCampo').asString);
               MsWord.Insert(QRes1.FieldbyName(FieldByName('Campo').asString).asString);
               Next;
          end;
          Close;
          Data.QTemp.Close;

          if xSalva then
               // salvataggio
               MsWord.FileSaveAs(xFileWord);
          Result := xFileWord;
          if xStampa then begin
               MsWord.FilePrint;
               MsWord.FileClose;
               MsWord.AppClose;
          end;
     end;
end;

procedure TRicClientiForm.BitBtn7Click(Sender: TObject);
var xNonInviati, xinviato: string;
     k: integer;
begin
     if (not (QRes1.active)) or (QRes1.RecordCount = 0) then exit;
     SceltaModelloMailCliForm := TSceltaModelloMailCliForm.create(self);

     if xInvio > 0 then begin
          // posizionati sul modello di mail (--> SE ASSOCIATO AL TIPO DI INVIO)
          Data.Q1.Close;
          Data.Q1.SQL.text := 'select IDTipoMailContatto from TipiInviiContatti where ID=' + IntTostr(xInvio);
          Data.Q1.Open;
          if Data.Q1.FieldByName('IDTipoMailContatto').asString <> '' then
               while (not SceltaModelloMailCliForm.QTipiMailContattiCli.Eof) and (SceltaModelloMailCliForm.QTipiMailContattiCliID.Value <> Data.Q1.FieldByName('IDTipoMailContatto').asInteger) do
                    SceltaModelloMailCliForm.QTipiMailContattiCli.Next;
          Data.Q1.Close;
     end;

     SceltaModelloMailCliForm.ShowModal;
     if SceltaModelloMailCliForm.ModalResult = mrOK then begin
          // PER tutti quelli selezionati
          xNonInviati := '';
          for k := 0 to DBGrid5.SelectedCount - 1 do begin
               xinviato := '';
               QRes1.BookMark := DBGrid5.SelectedRows[k];

               xinviato := SpedisciMail;
               xNonInviati := xNonInviati + xinviato + chr(13);
               // se ha spedito ==> STORICO
               if xinviato <> '' then
                    // storicizzazione (se impostata)
                    StoricizzaInvio(QRes1IDContatto.Value,
                         SceltaModelloMailCliForm.QTipiMailContattiCliID.Value,
                         QRes1EtichettaAbitazione.Value,
                         'M',
                         SceltaModelloMailCliForm.QTipiMailContattiCliDescrizione.Value);

               if (xinviato <> '') and (SceltaModelloMailCliForm.CBStoricoInvio.checked) then begin
               end;
          end;
          if xNonInviati <> '' then
               ShowMessage('Messaggi non inviati ai seguenti soggetti per mancanza indirizzo e-mail:' + chr(13) +
                    xnonInviati)
          else ShowMessage('Messaggi inviati con successo');
     end;
     SceltaModelloMailCliForm.Free;
end;

function TRicClientiForm.SpedisciMail: string;
var i, xTot, xCRpos: integer;
     xMess, xSl: TstringList;
     Vero: boolean;
     xNonInviati, xNonInviatiSMS, xFirma, xS: string;
begin
     // impostazione parametri
     NMSMTP1.Host := SceltaModelloMailCliForm.EHost.Text;
     NMSMTP1.Port := StrToInt(SceltaModelloMailCliForm.EPort.Text);
     NMSMTP1.UserID := SceltaModelloMailCliForm.EUserID.Text;
     // oggetto (da tabella)
     NMSMTP1.PostMessage.Subject := SceltaModelloMailCliForm.QTipiMailContattiCliOggetto.Value;
     // COMPOSIZIONE MESSAGGIO
     xMess := TStringList.create;
     // intestazione (da tabella)
     xMess.Add(SceltaModelloMailCliForm.QTipiMailContattiCliIntestazione.Value);
     xMess.Add('');
     Q.Close;
     Q.SQL.Text := 'select Descrizione,FirmaMail from Users where ID=' + IntToStr(MainForm.xIDUtenteAttuale);
     Q.Open;
     // corpo (da tabella)
     xMess.Add(SceltaModelloMailCliForm.QTipiMailContattiCliCorpo.Value);
     // saluti e firma (da tabella Utenti) con ritorno a capo
     xMess.Add('');
     xS := Q.FieldByName('FirmaMail').asString;
     while pos(chr(13), xS) > 0 do begin
          xCRpos := pos(chr(13), xS);
          xMess.Add(copy(xS, 1, xCRpos - 1));
          xS := copy(xS, xCRpos + 1, length(xS));
     end;
     xMess.Add(xS);

     // edit messaggio
     EmailMessageForm := TEmailMessageForm.create(self);
     EmailMessageForm.Memo1.Lines := xMess;
     EmailMessageForm.ShowModal;
     if EmailMessageForm.ModalResult = mrCancel then begin
          EmailMessageForm.Free;
          exit;
     end;
     xmess.Clear;
     for i := 0 to EmailMessageForm.Memo1.Lines.count - 1 do
          xMess.Add(EmailMessageForm.Memo1.Lines[i]);
     ShowMessage('Predisporre connessione Internet e premere OK quando pronti');
     EmailMessageForm.Free;
     NMSMTP1.PostMessage.Body := xMess;
     // connessione
     NMSMTP1.Connect;
     // header (mittente)
     NMSMTP1.PostMessage.FromAddress := SceltaModelloMailCliForm.EAddress.Text;
     NMSMTP1.PostMessage.FromName := SceltaModelloMailCliForm.EName.Text;
     // invio vero e proprio dei messaggi
     xNonInviati := '';
     xNonInviatiSMS := '';

     if QRes1email.Value = '' then
          Result := QRes1Contatto.Value
     else begin
          NMSMTP1.PostMessage.ToAddress.Add(QRes1email.Value);
          // invio
          NMSMTP1.SendMail;
          Result := '';
     end;

     // disconnessione
     Q.Close;
     NMSMTP1.Disconnect;
     xMess.Free;
end;

procedure TRicClientiForm.StoricizzaInvio(xIDContattoCli, xIDTipoInvio: integer; xAbitazione: boolean; xTipo, xNote: string);
begin
     with Data do begin
          DB.BeginTrans;
          try
               Q1.Close;
               Q1.SQL.text := 'insert into StoricoInviiContatto (IDContattoCli,IDTipoInvio,Data,Abitazione,Note,Tipo) ' +
                    'values (:xIDContattoCli,:xIDTipoInvio,:xData,:xAbitazione,:xNote,:xTipo)';
               Q1.ParamByName('xIDContattoCli').asInteger := xIDContattoCli;
               Q1.ParamByName('xIDTipoInvio').asInteger := xIDTipoInvio;
               Q1.ParamByName('xData').asDateTime := Date;
               Q1.ParamByName('xAbitazione').asBoolean := xAbitazione;
               Q1.ParamByName('xNote').asString := xNote;
               Q1.ParamByName('xTipo').asString := xTipo;
               Q1.ExecSQL;
               DB.CommitTrans;
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
          end;
     end;
end;

procedure TRicClientiForm.BitBtn8Click(Sender: TObject);
begin
     OpzioniEtichetteForm := TOpzioniEtichetteForm.create(self);
     OpzioniEtichetteForm.ShowModal;
     if OpzioniEtichetteForm.ModalResult = mrOK then begin
          QREtichette1Form := TQREtichette1Form.create(self);
          if OpzioniEtichetteForm.RGStampa.ItemIndex = 0 then
               QREtichette1Form.Preview
          else QREtichette1Form.Print;
          QREtichette1Form.Free;
          if OpzioniEtichetteForm.CBStoricizza.Checked then begin
               QRes1.First;
               while not QRes1.EOF do begin
                    // storicizzazione invio etichetta
                    StoricizzaInvio(QRes1IDContatto.Value,
                         xInvio,
                         QRes1EtichettaAbitazione.Value,
                         'E',
                         'etichetta');
                    QRes1.Next;
               end;
          end;
     end;
     OpzioniEtichetteForm.Free;
end;

procedure TRicClientiForm.DBGrid5MouseUp(Sender: TObject;
     Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
     if (Button <> mbRight) or (Shift <> []) then Exit;
     TdxDBGridPopupMenuManager.Instance.ShowGridPopupMenu(Sender as TdxDBGrid);
end;

procedure TRicClientiForm.Stampagriglia1Click(Sender: TObject);
begin
     dxPrinter1.Preview(True, nil);
end;

procedure TRicClientiForm.EsportainExcel1Click(Sender: TObject);
begin
     Save('xls', 'Microsoft Excel 4.0 Worksheet (*.xls)|*.xls', 'ExpGrid.xls', DbGrid5.SaveToXLS);
end;

procedure TRicClientiForm.Save(ADefaultExt, AFilter, AFileName: string;
     AMethod: TSaveMethod);
begin
     with SaveDialog do
     begin
          DefaultExt := ADefaultExt;
          Filter := AFilter;
          FileName := AFileName;
          if Execute then AMethod(FileName, True);
     end;
end;

procedure TRicClientiForm.EsportainHTML1Click(Sender: TObject);
begin
     Save('htm', 'HTML File (*.htm; *.html)|*.htm', 'ExpGrid.htm', DbGrid5.SaveToHTML);
end;

procedure TRicClientiForm.BAddTargetListClick(Sender: TObject);
var xIDTargetList: integer;
begin
     // stesso cliente della commessa
     if xIDClienteRic = QRes1IDCliente.Value then begin
          messageDlg('L''azienda � il cliente della commessa !!', mtError, [mbOK], 0);
          exit;
     end;
     // controllo se � gi� un cliente in target list
     Data.QTemp.Close;
     Data.QTemp.SQL.text := 'select count(*) Tot from EBC_RicercheTargetList ' +
          'where IDRicerca=:xIDRicerca and IDCliente=:xIDCliente';
     Data.QTemp.ParambyName('xIDRicerca').asInteger := xIDRicerca;
     Data.QTemp.ParambyName('xIDCliente').asInteger := QRes1IDCliente.Value;
     Data.QTemp.Open;
     if Data.QTemp.FieldByName('Tot').asInteger > 0 then begin
          messageDlg('Azienda gi� presente nella target list !', mtError, [mbOK], 0);
          Data.QTemp.Close;
          exit;
     end;
     // controllo blocco 1
     Data.QTemp.Close;
     Data.QTemp.SQL.text := 'select count(*) Tot from EBC_Clienti where ID=:xID and Blocco1=1';
     Data.QTemp.ParambyName('xID').asInteger := QRes1IDCliente.Value;
     Data.QTemp.Open;
     if Data.QTemp.FieldByName('Tot').asInteger > 0 then begin
          if messageDlg('ATTENZIONE:  l''azienda ha impostato il BLOCCO di livello 1 - VUOI PROSEGUIRE ? ', mtWarning, [mbYes, mbNo], 0) = mrNo then begin
               Data.QTemp.Close;
               exit;
          end;
     end;
     // controllo storico target list
     if SelPersForm.CheckStoricoTL(QRes1IDCliente.Value) = False then exit;
     if messageDlg('Sei sicuro di voler aggiungere l''azienda ' + QRes1Cliente.Value + ' alla target List ?',
          mtWarning, [mbYes, mbNo], 0) = mrNo then exit;
     Data.QTemp.Close;
     with Data do begin
          DB.BeginTrans;
          try
               Q1.SQL.text := 'insert into EBC_RicercheTargetList (IDRicerca,IDCliente) ' +
                    'values (:xIDRicerca,:xIDCliente)';
               Q1.ParambyName('xIDRicerca').asInteger := xIDRicerca;
               Q1.ParambyName('xIDCliente').asInteger := QRes1IDCliente.Value;
               Q1.ExecSQL;
               Q1.SQL.text := 'select @@IDENTITY as LastID';
               Q1.Open;
               xIDTargetList := Q1.FieldByName('LastID').asInteger;
               Q1.Close;
               Q1.SQL.text := 'insert into EBC_LogEsplorazioni (IDTargetList,Data) ' +
                    'values (:xIDTargetList,:xData)';
               Q1.ParambyName('xIDTargetList').asInteger := xIDTargetList;
               Q1.ParambyName('xData').asDateTime := Date;
               Q1.ExecSQL;
               DB.CommitTrans;
               ShowMessage('Inserimento in Target list avvenuto');
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: operazione non effettuata', mtError, [mbOK], 0);
          end;
     end;
end;

procedure TRicClientiForm.TLineeAfterPost(DataSet: TDataSet);
begin
     TLinee.Close;
     TLinee.Open;
end;

procedure TRicClientiForm.BitBtn10Click(Sender: TObject);
var xSQL: string;
begin
     if (pos('ADVANT', Uppercase(Data.GlobalNomeAzienda.Value)) > 0) or
          (pos('MCS', Uppercase(Data.GlobalNomeAzienda.Value)) > 0) then begin
          // QRes1 dalla 10a riga in poi
          //xSQL := 'select distinct Anagrafica.ID';
          {for i := 9 to QRes1.SQL.Count - 2 do begin
            xSQL := xSQL + chr(13) + QRes1.SQL.Strings[i];
          end; }
          // predisponi Data.TAnagrafica
          Data2.TEBCClienti.SQL.text := 'SELECT EBC_Clienti.ID, EBC_Clienti.Descrizione, EBC_Clienti.Stato, ' +
               'EBC_Clienti.Indirizzo, EBC_Clienti.Cap, EBC_Clienti.Comune, ' +
               'EBC_Clienti.Provincia, EBC_Clienti.IDAttivita, EBC_Clienti.Telefono, ' +
               'EBC_Clienti.Fax, EBC_Clienti.PartitaIVA, EBC_Clienti.CodiceFiscale, ' +
               'EBC_Clienti.BancaAppoggio, EBC_Clienti.SistemaPagamento, ' +
               'EBC_Clienti.NoteContratto, EBC_Clienti.Responsabile, ' +
               'EBC_Clienti.ConosciutoInData, EBC_Clienti.IndirizzoLegale, ' +
               'EBC_Clienti.CapLegale, EBC_Clienti.ComuneLegale, SitoInternet, ' +
               'EBC_Clienti.ProvinciaLegale, Ebc_attivita.Attivita, NumDipendenti, Fatturato, ' +
               'EBC_Clienti.CartellaDoc, EBC_Clienti.TipoStrada,EBC_Clienti.NumCivico, ' +
               'EBC_Clienti.TipoStradaLegale, EBC_Clienti.NumCivicoLegale, ' +
               'EBC_Clienti.NazioneAbb, EBC_Clienti.NazioneAbbLegale, ' +
               'Blocco1,EnteCertificatore,LibPrivacy, CCIAA ' +
               'FROM EBC_Clienti,EBC_Attivita ' +
               'WHERE  (EBC_Clienti.IDAttivita = Ebc_attivita.ID) ' +
               'and EBC_Clienti.ID = ' + QRes1.FieldByName('IDCliente').AsString +
               ' ORDER BY EBC_Clienti.Descrizione';


          Data2.TEBCClienti.Open;
          ASAAziendaForm := TASAAziendaForm.create(self);
          ASAAziendaForm.ShowModal;
          ASAAziendaForm.Free;
     end;
end;

end.

