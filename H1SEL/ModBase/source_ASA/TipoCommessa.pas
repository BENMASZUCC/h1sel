unit TipoCommessa;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons;

type
  TTipoCommessaForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Label1: TLabel;
    ETipo: TEdit;
    Label2: TLabel;
    CBColore: TComboBox;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  TipoCommessaForm: TTipoCommessaForm;

implementation

{$R *.DFM}

procedure TTipoCommessaForm.FormShow(Sender: TObject);
begin
     Caption:='[S/44] - '+Caption;
end;

end.
