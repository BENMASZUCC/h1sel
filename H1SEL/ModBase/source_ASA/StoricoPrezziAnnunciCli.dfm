object StoricoPrezziAnnunciCliForm: TStoricoPrezziAnnunciCliForm
  Left = 246
  Top = 117
  BorderStyle = bsDialog
  Caption = 'Storico Prezzi Annunci per il cliente'
  ClientHeight = 467
  ClientWidth = 737
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object BitBtn1: TBitBtn
    Left = 633
    Top = 3
    Width = 94
    Height = 36
    Caption = 'Esci'
    TabOrder = 0
    Kind = bkOK
  end
  object Panel1: TPanel
    Left = 3
    Top = 4
    Width = 246
    Height = 45
    BevelOuter = bvLowered
    Enabled = False
    TabOrder = 1
    object Label1: TLabel
      Left = 5
      Top = 4
      Width = 35
      Height = 13
      Caption = 'Cliente:'
    end
    object ECliente: TEdit
      Left = 5
      Top = 18
      Width = 235
      Height = 21
      Color = clAqua
      TabOrder = 0
    end
  end
  object PCValute: TPageControl
    Left = 3
    Top = 55
    Width = 733
    Height = 407
    ActivePage = TSEuro
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 2
    object TSLire: TTabSheet
      Caption = 'Prezzi in Lire'
      object RxDBGrid1: TRxDBGrid
        Left = 0
        Top = 0
        Width = 725
        Height = 342
        Align = alClient
        DataSource = dsQStoricoCli
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnTitleClick = RxDBGrid1TitleClick
        Columns = <
          item
            Expanded = False
            FieldName = 'Data'
            Width = 69
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Testata'
            Width = 96
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NomeEdizione'
            Title.Caption = 'Edizione'
            Width = 83
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'NumModuli'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Title.Alignment = taCenter
            Title.Caption = 'moduli'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clBlue
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CostoLire'
            Title.Caption = 'Costo al cliente'
            Visible = True
          end
          item
            Color = 15456255
            Expanded = False
            FieldName = 'ScontoAlClienteLire'
            Title.Caption = 'sconto'
            Width = 76
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CostoANoiLire'
            Title.Caption = 'Costo a noi'
            Width = 76
            Visible = True
          end
          item
            Color = 13431295
            Expanded = False
            FieldName = 'ScontoANoiLire'
            Title.Caption = 'sconto (a noi)'
            Width = 76
            Visible = True
          end
          item
            Color = clAqua
            Expanded = False
            FieldName = 'GuadagnoLire'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clPurple
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Title.Caption = 'Guadagno'
            Width = 89
            Visible = True
          end>
      end
      object Panel2: TPanel
        Left = 0
        Top = 342
        Width = 725
        Height = 37
        Align = alBottom
        BevelOuter = bvLowered
        TabOrder = 1
        object Label2: TLabel
          Left = 208
          Top = 12
          Width = 49
          Height = 13
          Caption = 'TOTALI:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object DBEdit1: TDBEdit
          Left = 306
          Top = 8
          Width = 78
          Height = 21
          DataField = 'sumCostoLire'
          DataSource = DsQTotali
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 462
          Top = 8
          Width = 78
          Height = 21
          DataField = 'sumCostoANoiLire'
          DataSource = DsQTotali
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 385
          Top = 8
          Width = 77
          Height = 21
          Color = 15456255
          DataField = 'sumScontoAlClienteLire'
          DataSource = DsQTotali
          TabOrder = 2
        end
        object DBEdit4: TDBEdit
          Left = 539
          Top = 8
          Width = 77
          Height = 21
          Color = 13431295
          DataField = 'sumScontoANoiLire'
          DataSource = DsQTotali
          TabOrder = 3
        end
        object DBEdit5: TDBEdit
          Left = 615
          Top = 8
          Width = 91
          Height = 21
          Color = clAqua
          DataField = 'GuadagnoLire'
          DataSource = DsQTotali
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clPurple
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 4
        end
        object DBEdit6: TDBEdit
          Left = 264
          Top = 8
          Width = 41
          Height = 21
          DataField = 'Totmoduli'
          DataSource = DsQTotali
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 5
        end
      end
    end
    object TSEuro: TTabSheet
      Caption = 'Prezzi in Euro'
      ImageIndex = 1
      object Panel3: TPanel
        Left = 0
        Top = 342
        Width = 725
        Height = 37
        Align = alBottom
        BevelOuter = bvLowered
        TabOrder = 0
        object Label3: TLabel
          Left = 208
          Top = 12
          Width = 49
          Height = 13
          Caption = 'TOTALI:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object DBEdit7: TDBEdit
          Left = 306
          Top = 8
          Width = 78
          Height = 21
          DataField = 'sumCostoEuro'
          DataSource = DsQTotali
          TabOrder = 0
        end
        object DBEdit8: TDBEdit
          Left = 462
          Top = 8
          Width = 78
          Height = 21
          DataField = 'sumCostoANoiEuro'
          DataSource = DsQTotali
          TabOrder = 1
        end
        object DBEdit9: TDBEdit
          Left = 385
          Top = 8
          Width = 77
          Height = 21
          Color = 15456255
          DataField = 'sumScontoAlClienteEuro'
          DataSource = DsQTotali
          TabOrder = 2
        end
        object DBEdit10: TDBEdit
          Left = 539
          Top = 8
          Width = 77
          Height = 21
          Color = 13431295
          DataField = 'sumScontoANoiEuro'
          DataSource = DsQTotali
          TabOrder = 3
        end
        object DBEdit11: TDBEdit
          Left = 615
          Top = 8
          Width = 91
          Height = 21
          Color = clAqua
          DataField = 'GuadagnoEuro'
          DataSource = DsQTotali
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clPurple
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 4
        end
        object DBEdit12: TDBEdit
          Left = 264
          Top = 8
          Width = 41
          Height = 21
          DataField = 'Totmoduli'
          DataSource = DsQTotali
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 5
        end
      end
      object RxDBGrid2: TRxDBGrid
        Left = 0
        Top = 0
        Width = 725
        Height = 342
        Align = alClient
        DataSource = dsQStoricoCli
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnTitleClick = RxDBGrid1TitleClick
        Columns = <
          item
            Expanded = False
            FieldName = 'Data'
            Width = 69
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Testata'
            Width = 96
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NomeEdizione'
            Title.Caption = 'Edizione'
            Width = 83
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'NumModuli'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Title.Alignment = taCenter
            Title.Caption = 'moduli'
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clBlue
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = []
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CostoEuro'
            Title.Caption = 'Costo al cliente'
            Visible = True
          end
          item
            Color = 15456255
            Expanded = False
            FieldName = 'ScontoAlClienteEuro'
            Title.Caption = 'sconto'
            Width = 76
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CostoANoiEuro'
            Title.Caption = 'Costo a noi'
            Width = 76
            Visible = True
          end
          item
            Color = 13431295
            Expanded = False
            FieldName = 'ScontoANoiEuro'
            Title.Caption = 'sconto (a noi)'
            Width = 76
            Visible = True
          end
          item
            Color = clAqua
            Expanded = False
            FieldName = 'GuadagnoEuro'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clPurple
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Title.Caption = 'Guadagno'
            Width = 89
            Visible = True
          end>
      end
    end
  end
  object QStoricoCli: TQuery
    OnCalcFields = QStoricoCliCalcFields
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select Ann_AnnEdizData.Data,CostoLire,CostoEuro,'
      '       CostoANoiLire,CostoANoiEuro,Ann_AnnEdizData.NumModuli,'
      '       ScontoAlClienteLire,ScontoAlClienteEuro,'
      '       ScontoANoiLire,ScontoANoiEuro,'
      '       NomeEdizione, Ann_Testate.Denominazione Testata'
      'from Ann_AnnEdizData, Ann_Edizioni, Ann_Testate, Ann_Annunci'
      'where Ann_AnnEdizData.IDEdizione=Ann_Edizioni.ID'
      '  and Ann_Edizioni.IDTestata=Ann_Testate.ID'
      '  and Ann_AnnEdizData.IDAnnuncio=Ann_Annunci.ID'
      '  and Ann_Annunci.IDCliente=:xIDCliente'
      'order by Ann_AnnEdizData.Data desc')
    Left = 16
    Top = 128
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'xIDCliente'
        ParamType = ptUnknown
      end>
    object QStoricoCliData: TDateTimeField
      FieldName = 'Data'
      Origin = 'EBCDB.Ann_AnnEdizData.Data'
    end
    object QStoricoCliNomeEdizione: TStringField
      FieldName = 'NomeEdizione'
      Origin = 'EBCDB.Ann_Edizioni.NomeEdizione'
      FixedChar = True
      Size = 50
    end
    object QStoricoCliTestata: TStringField
      FieldName = 'Testata'
      Origin = 'EBCDB.Ann_Testate.Denominazione'
      FixedChar = True
      Size = 50
    end
    object QStoricoCliCostoLire: TFloatField
      FieldName = 'CostoLire'
      Origin = 'EBCDB.Ann_AnnEdizData.CostoLire'
      DisplayFormat = '#,###'
    end
    object QStoricoCliCostoEuro: TFloatField
      FieldName = 'CostoEuro'
      Origin = 'EBCDB.Ann_AnnEdizData.CostoEuro'
      DisplayFormat = '#,###.00'
    end
    object QStoricoCliCostoANoiLire: TFloatField
      FieldName = 'CostoANoiLire'
      Origin = 'EBCDB.Ann_AnnEdizData.CostoANoiLire'
      DisplayFormat = '#,###'
    end
    object QStoricoCliCostoANoiEuro: TFloatField
      FieldName = 'CostoANoiEuro'
      Origin = 'EBCDB.Ann_AnnEdizData.CostoANoiEuro'
      DisplayFormat = '#,###.00'
    end
    object QStoricoCliScontoAlClienteLire: TFloatField
      FieldName = 'ScontoAlClienteLire'
      Origin = 'EBCDB.Ann_AnnEdizData.ScontoAlClienteLire'
      DisplayFormat = '#,###'
    end
    object QStoricoCliScontoAlClienteEuro: TFloatField
      FieldName = 'ScontoAlClienteEuro'
      Origin = 'EBCDB.Ann_AnnEdizData.ScontoAlClienteEuro'
      DisplayFormat = '#,###.00'
    end
    object QStoricoCliScontoANoiLire: TFloatField
      FieldName = 'ScontoANoiLire'
      Origin = 'EBCDB.Ann_AnnEdizData.ScontoANoiLire'
      DisplayFormat = '#,###'
    end
    object QStoricoCliScontoANoiEuro: TFloatField
      FieldName = 'ScontoANoiEuro'
      Origin = 'EBCDB.Ann_AnnEdizData.ScontoANoiEuro'
      DisplayFormat = '#,###.00'
    end
    object QStoricoCliNumModuli: TIntegerField
      FieldName = 'NumModuli'
    end
    object QStoricoCliGuadagnoLire: TFloatField
      FieldKind = fkCalculated
      FieldName = 'GuadagnoLire'
      DisplayFormat = '#,###'
      Calculated = True
    end
    object QStoricoCliGuadagnoEuro: TFloatField
      FieldKind = fkCalculated
      FieldName = 'GuadagnoEuro'
      DisplayFormat = '#,###.00'
      Calculated = True
    end
  end
  object dsQStoricoCli: TDataSource
    DataSet = QStoricoCli
    Left = 16
    Top = 160
  end
  object QTotali: TQuery
    OnCalcFields = QTotaliCalcFields
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select sum(CostoLire) sumCostoLire,'
      '       sum(CostoEuro) sumCostoEuro,'
      '       sum(CostoANoiLire) sumCostoANoiLire,'
      '       sum(CostoANoiEuro) sumCostoANoiEuro,'
      '       sum(ScontoAlClienteLire) sumScontoAlClienteLire,'
      '       sum(ScontoAlClienteEuro) sumScontoAlClienteEuro,'
      '       sum(ScontoANoiLire) sumScontoANoiLire,'
      '       sum(ScontoANoiEuro) sumScontoANoiEuro,'
      '       sum(Ann_AnnEdizData.NumModuli) TotModuli'
      'from Ann_AnnEdizData, Ann_Annunci'
      'where Ann_AnnEdizData.IDAnnuncio=Ann_Annunci.ID'
      '  and Ann_Annunci.IDCliente=:xIDCLiente')
    Left = 23
    Top = 327
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'xIDCLiente'
        ParamType = ptUnknown
      end>
    object QTotalisumCostoLire: TFloatField
      FieldName = 'sumCostoLire'
      Origin = 'EBCDB.Ann_AnnEdizData.CostoLire'
      DisplayFormat = '#,###'
    end
    object QTotalisumCostoEuro: TFloatField
      FieldName = 'sumCostoEuro'
      Origin = 'EBCDB.Ann_AnnEdizData.CostoEuro'
      DisplayFormat = '#,###.##'
    end
    object QTotalisumCostoANoiLire: TFloatField
      FieldName = 'sumCostoANoiLire'
      Origin = 'EBCDB.Ann_AnnEdizData.CostoANoiLire'
      DisplayFormat = '#,###'
    end
    object QTotalisumCostoANoiEuro: TFloatField
      FieldName = 'sumCostoANoiEuro'
      Origin = 'EBCDB.Ann_AnnEdizData.CostoANoiEuro'
      DisplayFormat = '#,###.00'
    end
    object QTotalisumScontoAlClienteLire: TFloatField
      FieldName = 'sumScontoAlClienteLire'
      Origin = 'EBCDB.Ann_AnnEdizData.ScontoAlClienteLire'
      DisplayFormat = '#,###'
    end
    object QTotalisumScontoAlClienteEuro: TFloatField
      FieldName = 'sumScontoAlClienteEuro'
      Origin = 'EBCDB.Ann_AnnEdizData.ScontoAlClienteEuro'
      DisplayFormat = '#,###.00'
    end
    object QTotalisumScontoANoiLire: TFloatField
      FieldName = 'sumScontoANoiLire'
      Origin = 'EBCDB.Ann_AnnEdizData.ScontoANoiLire'
      DisplayFormat = '#,###'
    end
    object QTotalisumScontoANoiEuro: TFloatField
      FieldName = 'sumScontoANoiEuro'
      Origin = 'EBCDB.Ann_AnnEdizData.ScontoANoiEuro'
      DisplayFormat = '#,###.00'
    end
    object QTotaliGuadagnoLire: TFloatField
      FieldKind = fkCalculated
      FieldName = 'GuadagnoLire'
      DisplayFormat = '#,###'
      Calculated = True
    end
    object QTotaliGuadagnoEuro: TFloatField
      FieldKind = fkCalculated
      FieldName = 'GuadagnoEuro'
      DisplayFormat = '#,###.00'
      Calculated = True
    end
    object QTotaliTotmoduli: TIntegerField
      FieldName = 'Totmoduli'
    end
  end
  object DsQTotali: TDataSource
    DataSet = QTotali
    Left = 23
    Top = 359
  end
end
