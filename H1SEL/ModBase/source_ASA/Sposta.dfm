�
 TSPOSTAFORM 03  TPF0TSpostaForm
SpostaFormLeftfTopIBorderStylebsDialogCaptionVedi/Modifica impegnoClientHeightClientWidthwColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TPanelPanel1Left Top WidthwHeightAlignalClient
BevelOuter	bvLoweredTabOrder  TLabelLabel3LeftTopWidthHeightCaptionData  TLabelLabel4LeftfTopWidth*HeightCaption	Dalle ore  TLabelLabel5LeftTop+Width7HeightCaptionDescrizione  TLabelLabel1Left� TopWidth#HeightCaptionAlle ore  TBitBtnBitBtn1LeftTopWidthUHeightTabOrderOnClickBitBtn1ClickKindbkOK  TBitBtnBitBtn2LeftTop#WidthUHeightCaptionAnnullaTabOrderKindbkCancel  TDateEdit97DEDataLeftTopWidth\HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder ColorCalendar.ColorValidclBlueDayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanPopup
doIsMaskeddoShowCanceldoShowToday   	TMaskEditMEOreLeftfTopWidth+HeightEditMask
!90:00;1;_	MaxLengthTabOrderText  .    TEditEDescrizLeftTop9Width	Height	MaxLength2TabOrder  TBitBtnBitBtn3LeftTop@WidthUHeightCaptionEliminaTabOrderOnClickBitBtn3Click
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 30    3337wwww�330�wwp3337�����330���p3337�����330��pp3337�����330���p3337�����330��pp3337�����330���p333�������00��pp0377�����33 ���p33w����s330��pp3337�����330pppp3337�����33     33wwwww33��ww33����33     33wwwwws3330wp333337���33330  333337ww333	NumGlyphs  TPanelPanLuogoLeftTop|Width� Height� 
BevelOuter	bvLoweredTabOrder TLabelLabel2LeftTopWidth!HeightCaptionLuogo:  TDBGridDBGrid1LeftTopWidth� Height_
DataSource	dsRisorseTabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameRisorsaTitle.CaptionLuogoWidth� Visible	    	TCheckBoxCBLuogoLeftToptWidth� HeightCaptionnon registrare luogoTabOrder   TRadioGroupRGTipoLeftTopRWidth
Height$CaptionTipoColumns	ItemIndex Items.Strings	colloquio
telefonatavarie TabOrderOnClickRGTipoClick  	TMaskEdit	MEAlleOreLeft� TopWidth+HeightEditMask
!90:00;1;_	MaxLengthTabOrderText  .    	TGroupBox	GroupBox1LeftTopWidthhHeight� CaptionInformazioni commessaEnabledTabOrder	 TLabelLabel6LeftTopWidthHeightCaptionRif.FocusControlDBEdit1  TLabelLabel7LeftJTopWidth(HeightCaptionClienteFocusControlDBEdit2Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel8LeftTop8WidthNHeightCaptionRuolo/PosizioneFocusControlDBEdit3  TLabelLabel9LeftTop`Width� HeightCaption Ruolo richiesto (Titolo cliente)FocusControlDBEdit4Font.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TDBEditDBEdit1LeftTop Width@Height	DataFieldProgressivo
DataSource
DsQInfoRicTabOrder   TDBEditDBEdit2LeftJTop WidthHeightColorclAqua	DataFieldcliente
DataSource
DsQInfoRicFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder  TDBEditDBEdit3LeftTopHWidth� Height	DataFieldRuolo
DataSource
DsQInfoRicTabOrder  TDBEditDBEdit4LeftToppWidth� Height	DataFieldTitoloCliente
DataSource
DsQInfoRicFont.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder    TQueryTRisorseActive	DatabaseNameEBCDBSQL.Stringsselect * from Risorse  LeftHTop�  TAutoIncField
TRisorseID	FieldNameID  TStringFieldTRisorseRisorsa	FieldNameRisorsa	FixedChar	Size  TStringFieldTRisorseColor	FieldNameColor	FixedChar	   TDataSource	dsRisorseDataSetTRisorseLeftHTop�   TQueryQCheckDatabaseNameEBCDBSQL.Strings+select Descrizione,Ore,AlleOre from Agenda ;where ((Ore >:xDalle and Ore <:xalle and IDRisorsa=:xIDRis)B  or (AlleOre >:xDalle and AlleOre <:xalle  and IDRisorsa=:xIDRis)A  or (Ore <= :xDalle and AlleOre >= :xalle and IDRisorsa=:xIDRis)B  or (Ore >= :xDalle and AlleOre <= :xalle and IDRisorsa=:xIDRis))and ID<>:xIDAgenda Left� Top� 	ParamDataDataType	ftUnknownNamexDalle	ParamType	ptUnknown DataType	ftUnknownNamexalle	ParamType	ptUnknown DataType	ftUnknownNamexIDRis	ParamType	ptUnknown DataType	ftUnknownNamexDalle	ParamType	ptUnknown DataType	ftUnknownNamexalle	ParamType	ptUnknown DataType	ftUnknownNamexIDRis	ParamType	ptUnknown DataType	ftUnknownNamexDalle	ParamType	ptUnknown DataType	ftUnknownNamexalle	ParamType	ptUnknown DataType	ftUnknownNamexIDRis	ParamType	ptUnknown DataType	ftUnknownNamexDalle	ParamType	ptUnknown DataType	ftUnknownNamexalle	ParamType	ptUnknown DataType	ftUnknownNamexIDRis	ParamType	ptUnknown DataType	ftUnknownName	xIDAgenda	ParamType	ptUnknown    TQueryQInfoRicDatabaseNameEBCDBSQL.Strings'select EBC_Clienti.Descrizione cliente,"       Mansioni.Descrizione Ruolo,        Progressivo,TitoloCliente?from EBC_CandidatiRicerche, EBC_Ricerche, EBC_CLienti, Mansioni7where EBC_CandidatiRicerche.IDRicerca = EBC_Ricerche.ID+  and EBC_Ricerche.IDMansione = Mansioni.ID-  and EBC_Ricerche.IDCliente = EBC_Clienti.ID(and EBC_CandidatiRicerche.ID=:xIDCandRic Left(Top� 	ParamDataDataType	ftUnknownName
xIDCandRic	ParamType	ptUnknown   TStringFieldQInfoRiccliente	FieldNameclienteOriginEBCDB.EBC_CLienti.Descrizione	FixedChar	Size  TStringFieldQInfoRicRuolo	FieldNameRuoloOriginEBCDB.Mansioni.Descrizione	FixedChar	Size(  TStringFieldQInfoRicProgressivo	FieldNameProgressivoOriginEBCDB.EBC_Ricerche.Progressivo	FixedChar	Size
  TStringFieldQInfoRicTitoloCliente	FieldNameTitoloClienteOrigin EBCDB.EBC_Ricerche.TitoloCliente	FixedChar	Size2   TDataSource
DsQInfoRicDataSetQInfoRicLeft(Top�    