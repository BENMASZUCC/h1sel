unit SelTabGen;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Grids, DBGrids, Db, DBTables, ExtCtrls, GridDB;

type
  TSelTabGenForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    QTab: TQuery;
    FrameDB1: TFrameDB;
    procedure QTabAfterOpen(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    xFieldName,xFieldTitle:string;
    xTitles:array [1..10] of string;
    xModify:boolean;
  end;

var
  SelTabGenForm: TSelTabGenForm;

implementation

uses FrmFrameDB;

{$R *.DFM}

procedure TSelTabGenForm.QTabAfterOpen(DataSet: TDataSet);
var i:integer;
begin
     FrameDB1.DBGrid1.Columns[0].Visible:=False;
     if xFieldName<>'' then
        FrameDB1.DBGrid1.Columns[1].FieldName:=xFieldName;
     if xFieldTitle<>'' then
        FrameDB1.DBGrid1.Columns[1].Title.Caption:=xFieldTitle;
     if xTitles[1]<>'' then begin
        for i:=1 to FrameDB1.DBGrid1.Columns.Count-1 do begin
            FrameDB1.DBGrid1.Columns[i].Title.Caption:=xTitles[i];
        end;
     end;
end;

procedure TSelTabGenForm.FormShow(Sender: TObject);
begin
     if not xModify then begin
        FrameDB1.BIns.Enabled:=False;
        FrameDB1.BMod.Enabled:=False;
        FrameDB1.BDel.Enabled:=False;
     end;
     Caption:='[S/0] - '+Caption;
end;

procedure TSelTabGenForm.FormCreate(Sender: TObject);
var i:integer;
begin
     for i:=1 to 10 do
         xTitles[i]:='';
end;

end.
