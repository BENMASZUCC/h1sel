object EspLavRetribForm: TEspLavRetribForm
  Left = 215
  Top = 162
  BorderStyle = bsDialog
  Caption = 'Dati retributivi per l'#39'esperienza lavorativa'
  ClientHeight = 271
  ClientWidth = 513
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 513
    Height = 44
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object BitBtn1: TBitBtn
      Left = 420
      Top = 4
      Width = 90
      Height = 37
      Caption = 'Esci'
      TabOrder = 0
      Kind = bkOK
    end
  end
  inline DatiRetribuzFrame1: TDatiRetribuzFrame
    Top = 44
    Width = 513
    Height = 227
    Align = alClient
    TabOrder = 1
    inherited Panel1: TPanel
      Left = 421
      Height = 206
    end
    inherited Panel2: TPanel
      Width = 421
      Height = 206
      inherited RxDBGrid1: TRxDBGrid
        Width = 419
        Height = 172
      end
      inherited Panel3: TPanel
        Top = 173
        Width = 419
      end
    end
    inherited Panel74: TPanel
      Width = 513
    end
  end
end
