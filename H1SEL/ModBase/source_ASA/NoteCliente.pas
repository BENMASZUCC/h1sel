unit NoteCliente;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Db, DBTables, FrameDBRichEdit2, ExtCtrls;

type
  TNoteClienteForm = class(TForm)
    Panel1: TPanel;
    FrameDBRich1: TFrameDBRich;
    QNoteCliente: TQuery;
    QNoteClienteID: TAutoIncField;
    QNoteClienteNote: TMemoField;
    DsQNoteCliente: TDataSource;
    BitBtn1: TBitBtn;
    UpdNoteCliente: TUpdateSQL;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  NoteClienteForm: TNoteClienteForm;

implementation

uses ModuloDati;

{$R *.DFM}

procedure TNoteClienteForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     with QNoteCliente do begin
          Data.DB.BeginTrans;
          try
               ApplyUpdates;
               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               raise;
          end;
          CommitUpdates;
     end;
end;

end.
