object LogOperazioniSoggForm: TLogOperazioniSoggForm
  Left = 231
  Top = 103
  BorderStyle = bsDialog
  Caption = 'Log operazioni apportate sul soggetto'
  ClientHeight = 357
  ClientWidth = 495
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 495
    Height = 42
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object BitBtn1: TBitBtn
      Left = 388
      Top = 4
      Width = 102
      Height = 35
      Caption = 'Esci'
      TabOrder = 0
      Kind = bkOK
    end
  end
  object dxDBGrid1: TdxDBGrid
    Left = 0
    Top = 42
    Width = 495
    Height = 315
    Bands = <
      item
      end>
    DefaultLayout = True
    HeaderPanelRowCount = 1
    KeyField = 'ID'
    SummaryGroups = <>
    SummarySeparator = ', '
    Align = alClient
    TabOrder = 1
    DataSource = DsQLog
    Filter.Active = True
    OptionsBehavior = [edgoAutoSort, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoTabThrough, edgoVertThrough]
    OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
    OptionsView = [edgoBandHeaderWidth, edgoIndicator, edgoRowSelect, edgoUseBitmap]
    object dxDBGrid1DataOra: TdxDBGridDateColumn
      Caption = 'Data e Ora'
      Sorted = csDown
      Width = 112
      BandIndex = 0
      RowIndex = 0
      FieldName = 'DataOra'
      DisableFilter = True
    end
    object dxDBGrid1Column4: TdxDBGridColumn
      Width = 95
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Utente'
    end
    object dxDBGrid1Tabella: TdxDBGridMaskColumn
      Caption = 'Tabella'
      Width = 138
      BandIndex = 0
      RowIndex = 0
      FieldName = 'DescTabella'
    end
    object dxDBGrid1Operation: TdxDBGridMaskColumn
      Caption = 'Operazione'
      Width = 113
      BandIndex = 0
      RowIndex = 0
      FieldName = 'DescOperation'
    end
  end
  object QLog: TQuery
    OnCalcFields = QLogCalcFields
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select Log_TableOp.*  '
      'from Log_TableOp'
      'where Tabella='#39'Anagrafica'#39' and KeyValue=:xIDAnag'
      'union'
      'select Log_TableOp.*  '
      'from anagrafica,AnagAltreInfo,Log_TableOp'
      'where anagrafica.ID = AnagAltreInfo.IDAnagrafica'
      
        '  and Tabella='#39'AnagAltreInfo'#39' and KeyValue=AnagAltreInfo.IDAnagr' +
        'afica'
      'and Anagrafica.ID=:xIDAnag'
      'union'
      'select Log_TableOp.*  '
      'from anagrafica,EsperienzeLavorative,Log_TableOp'
      'where anagrafica.ID = EsperienzeLavorative.IDAnagrafica'
      
        '  and Tabella='#39'EsperienzeLavorative'#39' and KeyValue=EsperienzeLavo' +
        'rative.ID'
      'and Anagrafica.ID=:xIDAnag'
      'union'
      'select Log_TableOp.*  '
      'from anagrafica,LingueConosciute,Log_TableOp'
      'where anagrafica.ID = LingueConosciute.IDAnagrafica'
      
        '  and Tabella='#39'LingueConosciute'#39' and KeyValue=LingueConosciute.I' +
        'D'
      'and Anagrafica.ID=:xIDAnag'
      'union'
      'select Log_TableOp.*  '
      'from anagrafica,AnagMansioni,Log_TableOp'
      'where anagrafica.ID = AnagMansioni.IDAnagrafica'
      '  and Tabella='#39'AnagMansioni'#39' and KeyValue=AnagMansioni.ID'
      'and Anagrafica.ID=:xIDAnag'
      'union'
      'select Log_TableOp.*  '
      'from anagrafica,TitoliStudio,Log_TableOp'
      'where anagrafica.ID = TitoliStudio.IDAnagrafica'
      '  and Tabella='#39'TitoliStudio'#39' and KeyValue=TitoliStudio.ID'
      'and Anagrafica.ID=:xIDAnag'
      'union'
      'select Log_TableOp.*  '
      'from anagrafica,CorsiExtra,Log_TableOp'
      'where anagrafica.ID = CorsiExtra.IDAnagrafica'
      '  and Tabella='#39'CorsiExtra'#39' and KeyValue=CorsiExtra.ID'
      'and Anagrafica.ID=:xIDAnag')
    Left = 368
    Top = 88
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'xIDAnag'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'xIDAnag'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'xIDAnag'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'xIDAnag'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'xIDAnag'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'xIDAnag'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'xIDAnag'
        ParamType = ptUnknown
      end>
    object QLogID: TAutoIncField
      FieldName = 'ID'
    end
    object QLogIDUtente: TIntegerField
      FieldName = 'IDUtente'
    end
    object QLogDataOra: TDateTimeField
      FieldName = 'DataOra'
    end
    object QLogTabella: TStringField
      FieldName = 'Tabella'
      FixedChar = True
      Size = 30
    end
    object QLogKeyValue: TIntegerField
      FieldName = 'KeyValue'
    end
    object QLogOperation: TStringField
      FieldName = 'Operation'
      FixedChar = True
      Size = 1
    end
    object QLogUtente: TStringField
      FieldKind = fkLookup
      FieldName = 'Utente'
      LookupDataSet = QUsersLK
      LookupKeyFields = 'ID'
      LookupResultField = 'Nominativo'
      KeyFields = 'IDUtente'
      Size = 30
      Lookup = True
    end
    object QLogDescTabella: TStringField
      FieldKind = fkCalculated
      FieldName = 'DescTabella'
      Size = 30
      Calculated = True
    end
    object QLogDescOperation: TStringField
      FieldKind = fkCalculated
      FieldName = 'DescOperation'
      Calculated = True
    end
  end
  object DsQLog: TDataSource
    DataSet = QLog
    Left = 368
    Top = 120
  end
  object QUsersLK: TQuery
    Active = True
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select ID,Nominativo'
      'from Users')
    Left = 416
    Top = 96
    object QUsersLKID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.Users.ID'
    end
    object QUsersLKNominativo: TStringField
      FieldName = 'Nominativo'
      Origin = 'EBCDB.Users.Nominativo'
      FixedChar = True
      Size = 30
    end
  end
end
