object DettSedeForm: TDettSedeForm
  Left = 329
  Top = 117
  ActiveControl = EDesc
  BorderStyle = bsDialog
  Caption = 'Dettaglio/modifica sede'
  ClientHeight = 123
  ClientWidth = 422
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 32
    Width = 41
    Height = 13
    Caption = 'Indirizzo:'
  end
  object Label2: TLabel
    Left = 8
    Top = 55
    Width = 22
    Height = 13
    Caption = 'Cap:'
  end
  object Label3: TLabel
    Left = 8
    Top = 78
    Width = 42
    Height = 13
    Caption = 'Comune:'
  end
  object Label4: TLabel
    Left = 8
    Top = 102
    Width = 47
    Height = 13
    Caption = 'Provincia:'
  end
  object Label5: TLabel
    Left = 8
    Top = 8
    Width = 71
    Height = 13
    Caption = 'Descrizione:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object BitBtn1: TBitBtn
    Left = 318
    Top = 3
    Width = 89
    Height = 34
    TabOrder = 7
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 318
    Top = 39
    Width = 89
    Height = 34
    Caption = 'Annulla'
    TabOrder = 8
    Kind = bkCancel
  end
  object EIndirizzo: TEdit
    Left = 139
    Top = 28
    Width = 139
    Height = 21
    MaxLength = 40
    TabOrder = 2
  end
  object ECap: TEdit
    Left = 83
    Top = 51
    Width = 46
    Height = 21
    MaxLength = 5
    TabOrder = 4
  end
  object EComune: TEdit
    Left = 83
    Top = 74
    Width = 196
    Height = 21
    MaxLength = 20
    TabOrder = 5
  end
  object EProv: TEdit
    Left = 83
    Top = 97
    Width = 26
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 2
    TabOrder = 6
  end
  object EDesc: TEdit
    Left = 83
    Top = 4
    Width = 228
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    MaxLength = 40
    ParentFont = False
    TabOrder = 0
  end
  object CBTipoStrada: TComboBox
    Left = 83
    Top = 28
    Width = 54
    Height = 21
    ItemHeight = 13
    TabOrder = 1
    Items.Strings = (
      'Via'
      'Viale'
      'Piazza'
      'Largo'
      'Corso')
  end
  object ENumCivico: TEdit
    Left = 280
    Top = 28
    Width = 31
    Height = 21
    MaxLength = 10
    TabOrder = 3
  end
end
