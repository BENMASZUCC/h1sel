unit InValutazione;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, Buttons, ExtCtrls, DBCtrls, Mask, DBCGrids;

type
  TInValutazioneForm = class(TForm)
    PageControl1: TPageControl;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    TsColloqui: TTabSheet;
    DBCtrlGrid1: TDBCtrlGrid;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBLookupComboBox1: TDBLookupComboBox;
    Panel2: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    GroupBox1: TGroupBox;
    Label8: TLabel;
    DataEvento: TDateTimePicker;
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  InValutazioneForm: TInValutazioneForm;

implementation

uses ModuloDati5, ModuloDati3, ModuloDati, FissaColloquio;

{$R *.DFM}

procedure TInValutazioneForm.BitBtn1Click(Sender: TObject);
var xDavantiA:string;
begin
        //fissazione colloquio
        FissaColloquioForm:=TFissaColloquioForm.create(self);
        FissaColloquioForm.Data.Date:=Date;
        FissaColloquioForm.DBGrid1.Visible:=True;
        FissaColloquioForm.ShowModal;

        xDavantia:=DataSel_EBC.TSelezionatoriNominativo.asString;
        DataSel_EBC.TColloqui.InsertRecord([DataSel_EBC.TSelezionatoriID.Value,
                                            FissaColloquioForm.Data.Date,
                                            StrToTime(FissaColloquioForm.Ora.Text),
                                            null,null,null,
                                            Data.TAnagraficaID.value]);

{        DataSel_EBC.TCandRicerca.Edit;
        DataSel_EBC.TCandRicercaStato.Value:='fissato colloquio per il giorno '+
                    DateToStr(Data.Date)+' alle ore '+Ora.Text+' ('+xDavantiA+')';
        DataSel_EBC.TCandRicerca.Post;
}
        // aggiornamento promemoria
        Data.TPromemoria.InsertRecord([DataSel_EBC.TSelezionatoriID.Value,
                                       Data.GlobalLastLoginID.Value,
                                       FissaColloquioForm.Data.Date,
                                       'Colloquio col candidato '+
                                        Data.TAnagraficaCognome.asString+
                                        ' - in valutazione.',
                                        null,False,Date]);
        FissaColloquioForm.Free;
        DataAssunzione.TColloquiCand.Refresh;
end;



end.
