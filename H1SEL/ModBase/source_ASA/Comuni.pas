unit Comuni;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Grids, DBGrids, Db, DBTables, StdCtrls, Buttons;

type
  TComuniForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    DsComuni: TDataSource;
    DBGrid1: TDBGrid;
    RGOpzioni: TRadioGroup;
    PanProv: TPanel;
    Label1: TLabel;
    EProv: TEdit;
    SpeedButton1: TSpeedButton;
    Label2: TLabel;
    ECerca: TEdit;
    QComuni: TQuery;
    QComuniID: TAutoIncField;
    QComuniCodice: TStringField;
    QComuniDescrizione: TStringField;
    QComuniProv: TStringField;
    QComuniCap: TStringField;
    QComuniPrefisso: TStringField;
    QComuniIDZona: TIntegerField;
    TZoneLK: TTable;
    TZoneLKID: TAutoIncField;
    TZoneLKZona: TStringField;
    QComuniZona: TStringField;
    procedure RGOpzioniClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure ECercaChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    xStringa:string;
  public
    { Public declarations }
  end;

var
  ComuniForm: TComuniForm;

implementation

{$R *.DFM}

procedure TComuniForm.RGOpzioniClick(Sender: TObject);
begin
     if RGOpzioni.ItemIndex=0 then begin
        QComuni.Filtered:=False;
        PanProv.Visible:=False;
     end else PanProv.Visible:=True;
end;

procedure TComuniForm.SpeedButton1Click(Sender: TObject);
begin
     QComuni.Filter:='Prov='''+EProv.Text+'''';
     QComuni.Filtered:=True;
end;

procedure TComuniForm.ECercaChange(Sender: TObject);
begin
     QComuni.Close;
     QComuni.ParamByName('xStringa').asString:=ECerca.text+'%';
     QComuni.Open;
end;

procedure TComuniForm.FormShow(Sender: TObject);
begin
     Caption:='[S/1] - '+Caption;
end;

end.
