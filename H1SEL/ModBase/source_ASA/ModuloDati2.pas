unit ModuloDati2;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     Db,DBTables,FileCtrl;

type
     TData2=class(TDataModule)
          TAziende: TTable;
          DsAziende: TDataSource;
          TAziendeID: TAutoIncField;
          TAziendeDescrizione: TStringField;
          TAziendeIndirizzo: TStringField;
          TAziendeCap: TStringField;
          TAziendeComune: TStringField;
          TAziendeProvincia: TStringField;
          DsMansioni: TDataSource;
          DsAree: TDataSource;
          TCompMansioni: TTable;
          TCompMansioniID: TAutoIncField;
          TCompMansioniIDCompetenza: TIntegerField;
          TCompMansioniIDMansione: TIntegerField;
          TCompMansioniOperatore: TStringField;
          TCompMansioniValore: TIntegerField;
          DsCompetenze: TDataSource;
          TCompMansioniDescCompetenza: TStringField;
          DsCompMansioni: TDataSource;
          DsCompDipendente: TDataSource;
          TCompMansioniTipologia: TStringField;
          TCompMansioniIndispensabile: TBooleanField;
          TCaratteristiche: TTable;
          TCaratteristicheID: TAutoIncField;
          TCaratteristicheDescrizione: TStringField;
          DsCarattDip: TDataSource;
          DsCaratteristiche: TDataSource;
          TCompMansioniArea: TStringField;
          TCompMansioniPeso: TSmallintField;
          TStoricoCompABS: TTable;
          TStoricoCompABSID: TAutoIncField;
          TStoricoCompABSIDCompetenza: TIntegerField;
          TStoricoCompABSIDAnagrafica: TIntegerField;
          TStoricoCompABSDallaData: TDateTimeField;
          TStoricoCompABSMotivoAumento: TStringField;
          TStoricoCompABSIDCorso: TIntegerField;
          TStoricoCompABSValore: TIntegerField;
          TAnagMansIDX: TTable;
          TAnagMansIDXID: TAutoIncField;
          TAnagMansIDXIDAnagrafica: TIntegerField;
          TAnagMansIDXIDMansione: TIntegerField;
          TAnagMansIDXAnniEsperienza: TSmallintField;
          TAnagMansIDXDisponibile: TBooleanField;
          TSpec: TTable;
          DsSpec: TDataSource;
          TSpecID: TAutoIncField;
          TSpecIDArea: TIntegerField;
          TSpecDescrizione: TStringField;
          TAreeLK: TTable;
          TAreeLKID: TAutoIncField;
          TAreeLKDescrizione: TStringField;
          TSpecArea: TStringField;
          TAnagCompIDX: TTable;
          TAnagCompIDXID: TAutoIncField;
          TAnagCompIDXIDCompetenza: TIntegerField;
          TAnagCompIDXIDAnagrafica: TIntegerField;
          TAnagCompIDXValore: TIntegerField;
          TStoricoCompABSVariazPerc: TSmallintField;
          TMansioniLK: TTable;
          TMansioniLKID: TAutoIncField;
          TMansioniLKDescrizione: TStringField;
          TMansioniLKIDArea: TIntegerField;
          TCompetenzeLK: TTable;
          TCompetenzeLKID: TAutoIncField;
          TCompetenzeLKIDArea: TIntegerField;
          TCompetenzeLKDescrizione: TStringField;
          TCompetenzeLKTipologia: TStringField;
          TCompetenzeLKQuantiValori: TIntegerField;
          TCompetenzeLKArrayDescrizione: TStringField;
          TCompetenzeLKArea: TStringField;
          QCompRuolo: TQuery;
          QCompRuoloID: TIntegerField;
          QCompRuoloIDCompetenza: TIntegerField;
          QCompRuoloIDMansione: TIntegerField;
          QCompRuoloPeso: TSmallintField;
          QCompRuoloOperatore: TStringField;
          QCompRuoloValore: TIntegerField;
          QCompRuoloIndispensabile: TBooleanField;
          QCompRuoloDescCompetenza: TStringField;
          QCompRuoloTipologia: TStringField;
          QCompRuoloArea: TStringField;
          TAnagMansIDXRuolo: TStringField;
          QTemp: TQuery;
          TAnagIDX: TTable;
          TAnagIDXID: TAutoIncField;
          TAnagIDXCognome: TStringField;
          TAnagIDXNome: TStringField;
          TAnagIDXIDTipoStato: TIntegerField;
          TCompMansIDX: TTable;
          TCompMansIDXID: TAutoIncField;
          TCompMansIDXIDCompetenza: TIntegerField;
          TCompMansIDXIDMansione: TIntegerField;
          TCompMansIDXPeso: TSmallintField;
          TCompMansIDXOperatore: TStringField;
          TCompMansIDXValore: TIntegerField;
          TCompMansIDXIndispensabile: TBooleanField;
          TCompIDX: TTable;
          TCompIDXID: TAutoIncField;
          TCompIDXDescrizione: TStringField;
          TAreeComp: TTable;
          DsAreeComp: TDataSource;
          TAreeCompID: TAutoIncField;
          TAreeCompDescrizione: TStringField;
          TAreaCompIDX: TTable;
          TAreaCompIDXID: TAutoIncField;
          TAreaCompIDXDescrizione: TStringField;
          TMansioniLKFileMansionario: TStringField;
          DsEBCclienti: TDataSource;
          DsContattiClienti: TDataSource;
          DsInserimCliente: TDataSource;
          DsAttivita: TDataSource;
          DsRicClienti: TDataSource;
          QTotCompenso: TQuery;
          QTotCompensoTotCompenso: TFloatField;
          DsTotCompenso: TDataSource;
          TFattClienti: TTable;
          DsFattClienti: TDataSource;
          TClientiLK: TTable;
          TClientiLKID: TAutoIncField;
          TClientiLKDescrizione: TStringField;
          TClientiLKIndirizzo: TStringField;
          TClientiLKCap: TStringField;
          TClientiLKComune: TStringField;
          TClientiLKProvincia: TStringField;
          TClientiLKIDAttivita: TIntegerField;
          TInserimentiABS: TTable;
          TInserimentiABSIDAnagrafica: TIntegerField;
          TInserimentiABSIDCliente: TIntegerField;
          TInserimentiABSDallaData: TDateTimeField;
          TInserimentiABSAllaData: TDateTimeField;
          TInserimentiABSIDRicerca: TIntegerField;
          TInserimentiABSMotivoCessazione: TStringField;
          TInserimentiABSNote: TMemoField;
          TInserimentiABSID: TAutoIncField;
          TMansioniLKArea: TStringField;
          TUsersLK: TTable;
          TUsersLKID: TAutoIncField;
          TUsersLKNominativo: TStringField;
          DsAnagRicerche: TDataSource;
          TCliContattiFatti: TTable;
          DsCliContattiFatti: TDataSource;
          DsCliOfferte: TDataSource;
          TCliContattiFattiID: TAutoIncField;
          TCliContattiFattiIDCliente: TIntegerField;
          TCliContattiFattiTipoContatto: TStringField;
          TCliContattiFattiData: TDateTimeField;
          TCliContattiFattiOre: TDateTimeField;
          TCliContattiFattiParlatoCon: TStringField;
          TCliContattiFattiIDOfferta: TIntegerField;
          TOfferteLK: TTable;
          TOfferteLKID: TAutoIncField;
          TOfferteLKIDCliente: TIntegerField;
          TOfferteLKData: TDateTimeField;
          TOfferteLKAMezzo: TStringField;
          TOfferteLKAttenzioneDi: TStringField;
          TOfferteLKEsito: TStringField;
          TOfferteLKNote: TStringField;
          TOfferteLKRif: TStringField;
          TCliContattiFattiRifOfferta: TStringField;
          TClientiABS: TTable;
          TClientiABSID: TAutoIncField;
          TClientiABSDescrizione: TStringField;
          TFattClientiID: TAutoIncField;
          TFattClientiProgressivo: TIntegerField;
          TFattClientiTipo: TStringField;
          TFattClientiRifProg: TIntegerField;
          TFattClientiIDCliente: TIntegerField;
          TFattClientiData: TDateTimeField;
          TFattClientiImporto: TFloatField;
          TFattClientiIVA: TFloatField;
          TFattClientiTotale: TFloatField;
          TFattClientiAppoggioBancario: TStringField;
          TFattClientiAssegno: TStringField;
          TFattClientiDecorrenza: TDateTimeField;
          TFattClientiScadenzaPagamento: TDateTimeField;
          TFattClientiPagata: TBooleanField;
          TFattClientiPagataInData: TDateTimeField;
          TFattClientiModalitaPagamento: TStringField;
          QTotFattCli: TQuery;
          DsQTotFattCli: TDataSource;
          QTotFattCliTotCliente: TFloatField;
          TTimingRic: TTable;
          TTimingRicIDUtente: TIntegerField;
          TTimingRicIDRicerca: TIntegerField;
          TTimingRicData: TDateTimeField;
          TTimingRicTotSec: TIntegerField;
          TStatoRicLK: TTable;
          TStatoRicLKID: TAutoIncField;
          TStatoRicLKStatoRic: TStringField;
          DsNoteSpeseCli: TDataSource;
          TFattureLK: TTable;
          TFattureLKID: TAutoIncField;
          TFattureLKProgressivo: TIntegerField;
          DsCompensiRic: TDataSource;
          QTotComp2: TQuery;
          DsQTotComp2: TDataSource;
          QTotComp2TotCompenso: TFloatField;
          TEBCClienti: TQuery;
          UpdClienti: TUpdateSQL;
          TEBCClientiID: TAutoIncField;
          TEBCClientiStato: TStringField;
          TEBCClientiIndirizzo: TStringField;
          TEBCClientiCap: TStringField;
          TEBCClientiComune: TStringField;
          TEBCClientiProvincia: TStringField;
          TEBCClientiIDAttivita: TIntegerField;
          TEBCClientiTelefono: TStringField;
          TEBCClientiFax: TStringField;
          TEBCClientiPartitaIVA: TStringField;
          TEBCClientiCodiceFiscale: TStringField;
          TEBCClientiBancaAppoggio: TStringField;
          TEBCClientiSistemaPagamento: TStringField;
          TEBCClientiNoteContratto: TMemoField;
          TEBCClientiResponsabile: TStringField;
          TEBCClientiConosciutoInData: TDateTimeField;
          TEBCClientiIndirizzoLegale: TStringField;
          TEBCClientiCapLegale: TStringField;
          TEBCClientiComuneLegale: TStringField;
          TEBCClientiProvinciaLegale: TStringField;
          TEBCClientiAttivita: TStringField;
          QCliCV: TQuery;
          DsQCliCV: TDataSource;
          QCliCVID: TAutoIncField;
          QCliCVCognome: TStringField;
          QCliCVNome: TStringField;
          QCliCVCVNumero: TIntegerField;
          QCliCVCVInseritoInData: TDateTimeField;
          TCompetenze: TQuery;
          TCompetenzeID: TAutoIncField;
          TCompetenzeIDArea: TIntegerField;
          TCompetenzeDescrizione: TStringField;
          TCompetenzeTipologia: TStringField;
          TCompetenzeQuantiValori: TIntegerField;
          TCompetenzeArrayDescrizione: TStringField;
          TCompetenzeArea: TStringField;
          TAreeCompLK: TTable;
          TAreeCompLKID: TAutoIncField;
          TAreeCompLKDescrizione: TStringField;
          TCompensiRic: TQuery;
          TCompensiRicProgFatt: TIntegerField;
          TCompensiRicID: TAutoIncField;
          TCompensiRicIDRicerca: TIntegerField;
          TCompensiRicTipo: TStringField;
          TCompensiRicImporto: TFloatField;
          TCompensiRicDataPrevFatt: TDateTimeField;
          TCompensiRicIDFattura: TIntegerField;
          TCompensiRicNote: TStringField;
          TEBCClientiCartellaDoc: TStringField;
          QCheck: TQuery;
          TCliContattiFattiIDContattoCli: TIntegerField;
          QRicClienti: TQuery;
          QRicClientiProgressivo: TStringField;
          QRicClientiDataInizio: TDateTimeField;
          QRicClientiDataFine: TDateTimeField;
          QRicClientiNumRicercati: TSmallintField;
          QRicClientiDallaData: TDateTimeField;
          QRicClientiRuolo: TStringField;
          QRicClientiUtente: TStringField;
          QRicClientiStatoRic: TStringField;
          QRicClientiCompensoStimato: TFloatField;
          QRicClientiStipendioLordo: TFloatField;
          QRicClientiID: TAutoIncField;
          QInserimCli: TQuery;
          QInserimCliCognome: TStringField;
          QInserimCliNome: TStringField;
          QInserimCliRifRicerca: TStringField;
          QInserimCliDallaData: TDateTimeField;
          QInserimCliAllaData: TDateTimeField;
          QInserimCliProgFattura: TIntegerField;
          QInserimCliMotivoCessazione: TStringField;
          QInserimCliNote: TMemoField;
          QInserimCliRuoloRicercato: TStringField;
          QInserimCliID: TAutoIncField;
          QInserimCliIDRicerca: TIntegerField;
          TNoteSpeseCli: TQuery;
          TNoteSpeseCliID: TAutoIncField;
          TNoteSpeseCliData: TDateTimeField;
          TNoteSpeseCliDescrizione: TStringField;
          TNoteSpeseCliRifFattura: TIntegerField;
          TNoteSpeseCliCognome: TStringField;
          TNoteSpeseCliNome: TStringField;
          TNoteSpeseCliRifRicerca: TStringField;
          TNoteSpeseCliUtente: TStringField;
          TNoteSpeseCliTotale: TFloatField;
          TNoteSpeseCliAutomobile: TFloatField;
          TNoteSpeseCliAutostrada: TFloatField;
          TNoteSpeseCliAereoTreno: TFloatField;
          TNoteSpeseCliTaxi: TFloatField;
          TNoteSpeseCliAlbergo: TFloatField;
          TNoteSpeseCliRistoranteBar: TFloatField;
          TNoteSpeseCliParking: TFloatField;
          TNoteSpeseCliAltroDesc: TStringField;
          TNoteSpeseCliAltroImporto: TFloatField;
          TNoteSpeseCliIDCliente: TIntegerField;
          TNoteSpeseCliIDRicerca: TIntegerField;
          TNoteSpeseCliIDUtente: TIntegerField;
          TNoteSpeseCliIDAnagrafica: TIntegerField;
          TNoteSpeseCliIDFattura: TIntegerField;
          QAnagRicerche: TQuery;
          QAnagRicercheProgressivo: TStringField;
          QAnagRicercheCliente: TStringField;
          QAnagRicercheRuolo: TStringField;
          QAnagRicercheEscluso: TBooleanField;
          QAnagRicercheStatoRic: TStringField;
          QAnagRicercheDataInizio: TDateTimeField;
          QAnagRicercheDataFine: TDateTimeField;
          QAnagRicercheID: TAutoIncField;
          QAnagRicercheIDRicerca: TIntegerField;
          QInserimCliIDAnagrafica: TIntegerField;
          TContattiClienti: TQuery;
          TContattiClientiID: TAutoIncField;
          TContattiClientiIDCliente: TIntegerField;
          TContattiClientiContatto: TStringField;
          TContattiClientiTelefono: TStringField;
          TContattiClientiFax: TStringField;
          TContattiClientiNote: TMemoField;
          TContattiClientiemail: TStringField;
          TContattiClientiDataIns: TDateTimeField;
          TContattiClientiIDUtente: TIntegerField;
          TEBCClientiTipoStrada: TStringField;
          TEBCClientiNumCivico: TStringField;
          TEBCClientiTipoStradaLegale: TStringField;
          TEBCClientiNumCivicoLegale: TStringField;
          TEBCClientiNazioneAbb: TStringField;
          TEBCClientiNazioneAbbLegale: TStringField;
          TCarattDip: TQuery;
          TCarattDipID: TAutoIncField;
          TCarattDipIDCaratt: TIntegerField;
          TCarattDipCaratteristica: TStringField;
          TCarattDipPunteggio: TIntegerField;
          TCompDipendente: TQuery;
          TCompDipendenteID: TAutoIncField;
          TCompDipendenteIDCompetenza: TIntegerField;
          TCompDipendenteIDAnagrafica: TIntegerField;
          TCompDipendenteValore: TIntegerField;
          TCompDipendenteDescCompetenza: TStringField;
          TCompDipendenteTipologia: TStringField;
          TCompDipendenteArea: TStringField;
          TContattiClientiSesso: TStringField;
          QCliContratti: TQuery;
          DsQCliContratti: TDataSource;
          QCliContrattiID: TAutoIncField;
          QCliContrattiIDCliente: TIntegerField;
          QCliContrattiData: TDateTimeField;
          QCliContrattiDescrizione: TStringField;
          QCliContrattiStato: TStringField;
          TCliOfferte: TQuery;
          TCliOfferteID: TAutoIncField;
          TCliOfferteIDCliente: TIntegerField;
          TCliOfferteRif: TStringField;
          TCliOfferteData: TDateTimeField;
          TCliOfferteAMezzo: TStringField;
          TCliOfferteAttenzioneDi: TStringField;
          TCliOfferteAnticipoRichiesto: TFloatField;
          TCliOfferteCondizioni: TStringField;
          TCliOfferteEsito: TStringField;
          TCliOfferteNote: TStringField;
          TCliOfferteStato: TStringField;
          TCliOfferteTipo: TStringField;
          TCliOfferteIDUtente: TIntegerField;
          TCliOfferteImportoTotale: TFloatField;
          TCliOfferteIDRicerca: TIntegerField;
          TCliOfferteIDContratto: TIntegerField;
          QCliContrattiCodice: TStringField;
          TCliOfferteCodContratto: TStringField;
          QAttivita: TQuery;
          QAttivitaID: TAutoIncField;
          QAttivitaAttivita: TStringField;
          QJobDesc: TQuery;
          DsQJobDesc: TDataSource;
          UpdJobDesc: TUpdateSQL;
          QJobDescMansioni: TMemoField;
          QJobDescID: TAutoIncField;
          QCliRefCli: TQuery;
          DsQCliRefCli: TDataSource;
          QCliRefCliID: TAutoIncField;
          QCliRefCliIDCliente: TIntegerField;
          QCliRefCliIDClienteRef: TIntegerField;
          QCliRefCliDescrizione: TStringField;
          QCliRefCliCheckCand: TBooleanField;
          QCliRefCliClienteRef: TStringField;
          TEBCClientiBlocco1: TBooleanField;
          QFornContiCosto: TQuery;
          DsQFornContiCosto: TDataSource;
          QFornContiCostoID: TAutoIncField;
          QFornContiCostoIDCliente: TIntegerField;
          QFornContiCostoIDContoCosto: TIntegerField;
          QFornContiCostoNote: TStringField;
          QFornContiCostoConto: TStringField;
          QFornCosti: TQuery;
          DsQFornCosti: TDataSource;
          QFornCostiID: TAutoIncField;
          QFornCostiIDCliente: TIntegerField;
          QFornCostiIDContoCosto: TIntegerField;
          QFornCostiAnno: TIntegerField;
          QFornCostiMese: TIntegerField;
          QFornCostiDataIns: TDateTimeField;
          QFornCostiDescrizione: TStringField;
          QFornCostiImportoLire: TFloatField;
          QFornCostiImportoEuro: TFloatField;
          QFornCostiIDUtente: TIntegerField;
          QFornCostiConto: TStringField;
          TEBCClientiEnteCertificatore: TStringField;
          TEBCClientiLibPrivacy: TBooleanField;
          QAnagRicercheUtente: TStringField;
          TCliContattiFattiDescrizione: TStringField;
          QAreeAttivita: TQuery;
          DsQAreeAttivita: TDataSource;
          QAreeAttivitaID: TAutoIncField;
          QAreeAttivitaAreaSettore: TStringField;
          QAttivitaIDAreaSettore: TIntegerField;
          QAttivitaAreaSettore: TStringField;
          UpdAttivita: TUpdateSQL;
          TEBCClientiDescrizione: TStringField;
          TCliOfferteIDContrattoCG: TIntegerField;
          TCliOfferteIDArea: TIntegerField;
          TCliOfferteIDLineaProdotto: TIntegerField;
          QClienteFile: TQuery;
          DsQClienteFile: TDataSource;
          QClienteFileID: TAutoIncField;
          QClienteFileIDCliente: TIntegerField;
          QClienteFileIDTipo: TIntegerField;
          QClienteFileDescrizione: TStringField;
          QClienteFileNomeFile: TStringField;
          QClienteFileDataCreazione: TDateTimeField;
          QRicClientiIDUtente: TIntegerField;
          TContattiClientiTitolo: TStringField;
          TEBCClientiNumDipendenti: TSmallintField;
          TEBCClientiFatturato: TFloatField;
          TEBCClientiSitoInternet: TStringField;
          QAttivitaAttivita_ENG: TStringField;
          TAree: TQuery;
          TAreeID: TAutoIncField;
          TAreeDescrizione: TStringField;
          TAreeIDAzienda: TIntegerField;
          TAreeDescrizione_ENG: TStringField;
          TMansioni: TQuery;
          TMansioniID: TAutoIncField;
          TMansioniDescrizione: TStringField;
          TMansioniIDArea: TIntegerField;
          TMansioniFileMansionario: TStringField;
          TMansioniMansioni: TMemoField;
          TMansioniIDAzienda: TIntegerField;
          TMansioniLKArea2: TStringField;
          TMansioniTolleranzaPerc: TIntegerField;
          TMansioniDescrizione_ENG: TStringField;
          TAreeABS: TQuery;
          TAreeABSID: TAutoIncField;
          TAreeABSDescrizione: TStringField;
          TAreeABSIDAzienda: TIntegerField;
          TAreeABSDescrizione_ENG: TStringField;
          TMansioniABS: TQuery;
          TMansioniABSID: TAutoIncField;
          TMansioniABSIDArea: TIntegerField;
          TMansioniABSDescrizione: TStringField;
          TMansioniABSDescrizione_ENG: TStringField;
          TEBCClientiCCIAA: TStringField;
          procedure DsCaratteristicheStateChange(Sender: TObject);
          procedure DsCompMansioniStateChange(Sender: TObject);
          procedure Data2Create(Sender: TObject);
          procedure TMansDipAfterScroll(DataSet: TDataSet);
          procedure TCompMansioniAfterPost(DataSet: TDataSet);
          procedure TAnagMansIDXAfterPost(DataSet: TDataSet);
          procedure TCompMansioniAfterInsert(DataSet: TDataSet);
          procedure DsEBCclientiStateChange(Sender: TObject);
          procedure TEBCclientiAfterPost(DataSet: TDataSet);
          procedure TEBCclientiBeforePost(DataSet: TDataSet);
          procedure TFattClientiBeforeDelete(DataSet: TDataSet);
          procedure TCompetenzeLKBeforeOpen(DataSet: TDataSet);
          procedure TCompetenzeLKAfterClose(DataSet: TDataSet);
          procedure TSpecBeforeOpen(DataSet: TDataSet);
          procedure TSpecAfterClose(DataSet: TDataSet);
          procedure TMansioniLKBeforeOpen(DataSet: TDataSet);
          procedure TMansioniLKAfterClose(DataSet: TDataSet);
          procedure TAnagMansIDXBeforeOpen(DataSet: TDataSet);
          procedure TAnagMansIDXAfterClose(DataSet: TDataSet);
          procedure TCompMansioniBeforeOpen(DataSet: TDataSet);
          procedure TCompMansioniAfterClose(DataSet: TDataSet);
          procedure QCompRuoloBeforePost(DataSet: TDataSet);
          procedure QCompRuoloAfterClose(DataSet: TDataSet);
          procedure TCliContattiFattiBeforeOpen(DataSet: TDataSet);
          procedure TCliContattiFattiAfterClose(DataSet: TDataSet);
          procedure TNoteSpeseCliCalcFields(DataSet: TDataSet);
          procedure TCaratteristicheAfterPost(DataSet: TDataSet);
          procedure TEBCClientiAfterScroll(DataSet: TDataSet);
          procedure QRicClientiCalcFields(DataSet: TDataSet);
          procedure TEBCClientiBeforeOpen(DataSet: TDataSet);
          procedure TEBCClientiAfterClose(DataSet: TDataSet);
          procedure DsQJobDescStateChange(Sender: TObject);
          procedure TMansioniBeforeScroll(DataSet: TDataSet);
          procedure DsAttivitaStateChange(Sender: TObject);
          procedure QAttivitaAfterPost(DataSet: TDataSet);
          procedure QRicClientiAfterScroll(DataSet: TDataSet);
     private
          { Private declarations }
          xVecchioVal,xIDComp,xIDCaratt,xValore,xIDAreaComp,xIDAnag: integer;
          xVecchioCC,xVecchioDip,xVecchioIDDip,xIDCentroCosto: integer;
          xComp,xAreaComp,xGradoCC: string;
          xNuovaComp,xCompSingola: boolean;
     public
          { Public declarations }
          xValutazDipForm,xColloquioForm,xSelComp,
               xRaffrontoForm,xValutazCandForm,xNuovoRuolo,xSelArea,xCheckLivello: boolean;
          xMot: string;
     end;

var
     Data2: TData2;

implementation

uses ModuloDati,Main,SelArea,ValutazDip,CompInserite,InsCompetenza,
     InsRuolo,InsCaratt,NuovaComp,Competenza,ElencoDip,ElencoRicCliente;

{$R *.DFM}

procedure TData2.DsCaratteristicheStateChange(Sender: TObject);
var b: boolean;
begin
     b:=DsCaratteristiche.State in [dsEdit,dsInsert];
     MainForm.TbBCarattNew.Enabled:=not b;
     MainForm.TbBCarattDel.Enabled:=not b;
     MainForm.TbBCarattOK.Enabled:=b;
     MainForm.TbBCarattCan.Enabled:=b;
end;

procedure TData2.DsCompMansioniStateChange(Sender: TObject);
var b: boolean;
begin
     b:=DsCompMansioni.State in [dsEdit,dsInsert];
     MainForm.TbBCompMansNew.Enabled:=not b;
     MainForm.TbBCompMansDel.Enabled:=not b;
     MainForm.TbBCompMansOK.Enabled:=b;
     MainForm.TbBCompMansCan.Enabled:=b;
end;

procedure TData2.Data2Create(Sender: TObject);
begin
     xValutazDipForm:=False;
     xValutazCandForm:=False;
     xColloquioForm:=False;
     xRaffrontoForm:=False;
     xCheckLivello:=True;
     xSelComp:=True;
     xMot:='';
     xVecchioCC:=0;
     xVecchioIDDip:=0;
     xNuovaComp:=False;
     xSelArea:=True;
end;

procedure TData2.TMansDipAfterScroll(DataSet: TDataSet);
begin
     {     if xRaffrontoForm then begin
             RaffrontoCompForm.DBChart3.RefreshData;
             RaffrontoCompForm.DBChart4.RefreshData;
          end;
     }
     if xValutazDipForm then begin
          ValutazDipForm.DBChart3.RefreshData;
          ValutazDipForm.DBChart4.RefreshData;
     end;
end;

procedure TData2.TCompMansioniAfterPost(DataSet: TDataSet);
var B: TBookmark;
     xTotPerc: integer;
     AlmenoUnoVuoto: boolean;
begin
     TCompetenzeLK.Filter:='';
     TCompetenzeLK.Filtered:=False;
     if TCompDipendente.Active then TCompDipendente.Refresh;

     // controllo somma percentuali
     TCompMansioni.DisableControls;
     B:=TCompMansioni.GetBookmark;
     TCompMansioni.First;
     xTotPerc:=0;
     AlmenoUnoVuoto:=False;
     while not TCompMansioni.EOF do begin
          xTotPerc:=xTotPerc+TCompMansioniPeso.asInteger;
          if TCompMansioniPeso.asString='' then AlmenoUnoVuoto:=True;
          TCompMansioni.Next;
     end;
     TCompMansioni.GotoBookmark(B);
     TCompMansioni.FreeBookmark(B);
     TCompMansioni.EnableControls;
     if (xTotPerc<>100)and(not AlmenoUnoVuoto) then MessageDlg('Totale percentuali diverso da 100 !!',mtError, [mbOK],0);
end;

procedure TData2.TAnagMansIDXAfterPost(DataSet: TDataSet);
var xIDAnag,xIDMans,xTotComp,i: integer;
     xComp: array[1..50] of integer;
begin
     {     xIDMans:=TAnagMansIDXIDMansione.Value;
          xIDAnag:=TAnagMansIDXIDAnagrafica.Value;
          if (xNuovoRuolo) and (TAnagMansIDXIDTipoStato.Value<>2) then begin
             // inserisci le competenze se mancanti
             CompInseriteForm:=TCompInseriteForm.create(self);
             CompInseriteForm.ERuolo.Text:=TAnagMansIDXRuolo.Value;
             QCompRuolo.Close;
             QCompRuolo.Prepare;
             QCompRuolo.Params[0].asInteger:=xIDMans;
             QCompRuolo.Open;
             QCompRuolo.First;
             xTotComp:=0;
             if TAnagCompIDX.active then TAnagCompIDX.Refresh;
             CompInseriteForm.SG1.RowCount:=2;
             CompInseriteForm.SG1.RowHeights[0]:=16;
             while not QCompRuolo.EOF do begin
                 TAnagCompIDX.Open;
                 if not TAnagCompIDX.FindKey([xIDAnag,QCompRuoloIDCompetenza.Value]) then begin
                    CompInseriteForm.SG1.RowHeights[xTotComp+1]:=16;
                    CompInseriteForm.SG1.Cells[0,xTotComp+1]:=Data2.QCompRuoloDescCompetenza.Value;
                    CompInseriteForm.SG1.Cells[1,xTotComp+1]:='0';
                    CompInseriteForm.SG1.Cells[2,xTotComp+1]:='�';
                    CompInseriteForm.SG1.ColorRow[xTotComp+1]:=clLime;
                    xComp[xTotComp+1]:=Data2.QCompRuoloIDCompetenza.Value;
                    Inc(xTotComp);
                    CompInseriteForm.SG1.RowCount:=CompInseriteForm.SG1.RowCount+1;
                 end;
                 QCompRuolo.Next;
                 TAnagCompIDX.Close;
             end;
             CompInseriteForm.SG1.RowCount:=CompInseriteForm.SG1.RowCount-1;
             if xTotComp>0 then CompInseriteForm.ShowModal;
             if CompInseriteForm.ModalResult=mrOK then begin
                for i:=1 to CompInseriteForm.SG1.RowCount-1 do begin
                    if CompInseriteForm.SG1.ColorRow[i]=clLime then begin
                      with Data do begin
                       DB.BeginTrans;
                       try
                          Q1.Close;
                          Q1.SQL.text:='insert into CompetenzeAnagrafica (IDAnagrafica,IDCompetenza,Valore) '+
                                       'values (:xIDAnagrafica,:xIDCompetenza,:xValore)';
                          Q1.ParamByName('xIDAnagrafica').asInteger:=xIDAnag;
                          Q1.ParamByName('xIDCompetenza').asInteger:=xComp[i];
                          Q1.ParamByName('xValore').asInteger:=StrToInt(CompInseriteForm.SG1.Cells[1,i]);
                          Q1.ExecSQL;

                          Q1.SQL.text:='insert into StoricoCompAnag (IDCompetenza,IDAnagrafica,DallaData,MotivoAumento,valore) '+
                                       'values (:xIDCompetenza,:xIDAnagrafica,:xDallaData,:xMotivoAumento,:xvalore)';
                          Q1.ParamByName('xIDCompetenza').asInteger:=xComp[i];
                          Q1.ParamByName('xIDAnagrafica').asInteger:=xIDAnag;
                          Q1.ParamByName('xDallaData').asDateTime:=Date;
                          Q1.ParamByName('xMotivoAumento').asString:='valore iniziale';
                          Q1.ParamByName('xValore').asInteger:=StrToInt(CompInseriteForm.SG1.Cells[1,i]);
                          Q1.ExecSQL;

                          DB.CommitTrans;
                       except
                             DB.RollbackTrans;
                             MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError,[mbOK],0);
                       end;
                      end;
                    end;
                end;
             end;
             CompInseriteForm.Free;
             if TCompDipendente.Active then TCompDipendente.Refresh;
          end;
          xNuovoRuolo:=False;}
end;

procedure TData2.TCompMansioniAfterInsert(DataSet: TDataSet);
begin
     //     TCompMansioniIDCompetenza.Value:=InsCompetenzaForm.TCompAreaID.Value;
     //     InsCompetenzaForm.Free;
end;

procedure TData2.DsEBCclientiStateChange(Sender: TObject);
var b: boolean;
begin
     b:=DsEBCclienti.State in [dsEdit,dsInsert];
     MainForm.TbBClientiNew.Enabled:=not b;
     MainForm.TbBClientiDel.Enabled:=not b;
     MainForm.TbBClientiMod.Enabled:=not b;
     MainForm.PanCliente.Enabled:=b;
     MainForm.TbBClientiOK.Enabled:=b;
     MainForm.TbBClientiCan.Enabled:=b;
     //MainForm.TbBClientiNoteOK.Enabled:=b;
     //MainForm.TbBClientiNoteCan.Enabled:=b;
end;

procedure TData2.TEBCclientiAfterPost(DataSet: TDataSet);
begin
     MainForm.LTotAz.Caption:=IntToStr(TEBCclienti.RecordCount);
     with Data2.TEBCclienti do begin
          Data.DB.BeginTrans;
          try
               ApplyUpdates;
               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               raise;
          end;
          CommitUpdates;
     end;
end;

procedure TData2.TEBCclientiBeforePost(DataSet: TDataSet);
begin
     if trim(TEBCclientiDescrizione.value)='' then begin
          MessageDlg('ATTENZIONE: denominazione vuota ',mtError, [mbOK],0);
          Abort;
     end;
end;

procedure TData2.TFattClientiBeforeDelete(DataSet: TDataSet);
begin
     // cancellazione Dettagli
     QTemp.Close;
     QTemp.SQL.Clear;
     QTemp.SQL.Add('delete from FattDett where IDFattura='+TFattClientiID.asString);
     QTemp.ExecSQL;

     // recupero inserimenti
     if MessageDlg('Vuoi recuperare gli inserimenti per poterli inserire in altre fatture ?',mtInformation, [mbYes,mbNo],0)=mrYes then begin
          QTemp.Close;
          QTemp.SQL.Clear;
          QTemp.SQL.Add('update EBC_Inserimenti set ProgFattura=null where ProgFattura='+TFattClientiProgressivo.asString);
          QTemp.ExecSQL;
     end;
end;

procedure TData2.TCompetenzeLKBeforeOpen(DataSet: TDataSet);
begin
     TAreeCompLK.Open;
end;

procedure TData2.TCompetenzeLKAfterClose(DataSet: TDataSet);
begin
     TAreeCompLK.Close;
end;

procedure TData2.TSpecBeforeOpen(DataSet: TDataSet);
begin
     TAreeLK.Open;
end;

procedure TData2.TSpecAfterClose(DataSet: TDataSet);
begin
     TAreeLK.Close;
end;

procedure TData2.TMansioniLKBeforeOpen(DataSet: TDataSet);
begin
     TAreeLK.Open;
end;

procedure TData2.TMansioniLKAfterClose(DataSet: TDataSet);
begin
     TAreeLK.Close;
end;

procedure TData2.TAnagMansIDXBeforeOpen(DataSet: TDataSet);
begin
     TMansioniLK.Open;
end;

procedure TData2.TAnagMansIDXAfterClose(DataSet: TDataSet);
begin
     TMansioniLK.Close;
end;

procedure TData2.TCompMansioniBeforeOpen(DataSet: TDataSet);
begin
     TCompetenzeLK.Open;
end;

procedure TData2.TCompMansioniAfterClose(DataSet: TDataSet);
begin
     TCompetenzeLK.Close;
end;

procedure TData2.QCompRuoloBeforePost(DataSet: TDataSet);
begin
     TCompetenzeLK.Open;
end;

procedure TData2.QCompRuoloAfterClose(DataSet: TDataSet);
begin
     TCompetenzeLK.Close;
end;

procedure TData2.TCliContattiFattiBeforeOpen(DataSet: TDataSet);
begin
     TOfferteLK.Open;
end;

procedure TData2.TCliContattiFattiAfterClose(DataSet: TDataSet);
begin
     TOfferteLK.Close;
end;

procedure TData2.TNoteSpeseCliCalcFields(DataSet: TDataSet);
begin
     TNoteSpeseCliTotale.Value:=TNoteSpeseCliAutomobile.Value+
          TNoteSpeseCliAutostrada.Value+
          TNoteSpeseCliAereoTreno.Value+
          TNoteSpeseCliTaxi.Value+
          TNoteSpeseCliAlbergo.Value+
          TNoteSpeseCliRistoranteBar.Value+
          TNoteSpeseCliParking.Value+
          TNoteSpeseCliAltroImporto.Value;
end;

procedure TData2.TCaratteristicheAfterPost(DataSet: TDataSet);
begin
     TCaratteristiche.Close;
     TCaratteristiche.open;
end;

procedure TData2.TEBCClientiAfterScroll(DataSet: TDataSet);
begin
     if TEBCClientiStato.value='fornitore' then MainForm.TSCliForn.TabVisible:=True
     else MainForm.TSCliForn.TabVisible:=False;
     if MainForm.PCClienti.ActivePage=MainForm.TSClientiContratto then begin
          if TEBCClientiCartellaDoc.Value<>'' then begin
               if DirectoryExists(TEBCClientiCartellaDoc.Value) then
                    MainForm.DirCartellaCli.Directory:=TEBCClientiCartellaDoc.Value;
          end else MainForm.DirCartellaCli.Directory:='c:\';
     end;
     if MainForm.PCClienti.ActivePage=MainForm.TSClientiCV then begin
          MainForm.CliCandidatiFrame1.xIDCliente:=TEBCClientiID.Value;
          MainForm.CliCandidatiFrame1.xIDRicerca:=0;
          MainForm.CliCandidatiFrame1.CreaAlbero;
     end;
     if MainForm.PCClienti.ActivePage=MainForm.TSClientiAnnunci then begin
          MainForm.ClienteAnnunciFrame.xIDCliente:=Data2.TEBCClientiID.Value;
          MainForm.ClienteAnnunciFrame.xCliente:=TEBCClientiDescrizione.Value;
          MainForm.ClienteAnnunciFrame.QAnnunciCli.Close;
          MainForm.ClienteAnnunciFrame.QListinoCliente.Close;
          MainForm.ClienteAnnunciFrame.QAnnunciCli.ParambyName('xIDCliente').asInteger:=TEBCClientiID.Value;
          MainForm.ClienteAnnunciFrame.QListinoCliente.ParambyName('xIDCliente').asInteger:=TEBCClientiID.Value;
          MainForm.ClienteAnnunciFrame.QAnnunciCli.Open;
          MainForm.ClienteAnnunciFrame.QListinoCliente.Open;
     end;
end;

procedure TData2.QRicClientiCalcFields(DataSet: TDataSet);
begin
     QRicClientiCompensoStimato.Value:=QRicClientiStipendioLordo.Value*
          QRicClientiNumRicercati.Value;
end;

procedure TData2.TEBCClientiBeforeOpen(DataSet: TDataSet);
begin
     TContattiClienti.open;
end;

procedure TData2.TEBCClientiAfterClose(DataSet: TDataSet);
begin
     TContattiClienti.Close;
end;

procedure TData2.DsQJobDescStateChange(Sender: TObject);
var b: boolean;
begin
     b:=DsQJobDesc.State in [dsEdit,dsInsert];
     MainForm.BJobDescOK.Enabled:=b;
     MainForm.BJobDescCan.Enabled:=b;
end;

procedure TData2.TMansioniBeforeScroll(DataSet: TDataSet);
begin
     if DsQJobDesc.state=dsEdit then Mainform.BJobDescOKClick(self);
end;

procedure TData2.DsAttivitaStateChange(Sender: TObject);
var b: boolean;
begin
     b:=DsAttivita.State in [dsEdit,dsInsert];
     MainForm.TbBAttivitaNew.Enabled:=not b;
     MainForm.TbBAttivitaDel.Enabled:=not b;
     MainForm.TbBAttivitaOK.Enabled:=b;
     MainForm.TbBAttivitaCan.Enabled:=b;
end;

procedure TData2.QAttivitaAfterPost(DataSet: TDataSet);
begin
     with QAttivita do begin
          Data.DB.BeginTrans;
          try
               ApplyUpdates;
               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               raise;
          end;
          CommitUpdates;
     end;
end;

procedure TData2.QRicClientiAfterScroll(DataSet: TDataSet);
begin
     if MainForm.PCClienti.ActivePage=MainForm.TSClientiRic then
          if (not MainForm.CheckProfile('126',False))and(QRicClientiIDUtente.Value<>MainForm.xIDUtenteAttuale) then
               MainForm.PanCliCompensi.visible:=False
          else MainForm.PanCliCompensi.visible:=True;
end;

end.

