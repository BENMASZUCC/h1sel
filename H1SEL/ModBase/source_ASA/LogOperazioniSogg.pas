unit LogOperazioniSogg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  dxDBGrid, dxDBCtrl, dxTL, dxDBTLCl, dxGrClms, dxCntner, Db, DBTables,
  StdCtrls, Buttons, ExtCtrls;

type
  TLogOperazioniSoggForm = class(TForm)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    QLog: TQuery;
    DsQLog: TDataSource;
    QLogID: TAutoIncField;
    QLogIDUtente: TIntegerField;
    QLogDataOra: TDateTimeField;
    QLogTabella: TStringField;
    QLogKeyValue: TIntegerField;
    QLogOperation: TStringField;
    dxDBGrid1: TdxDBGrid;
    dxDBGrid1DataOra: TdxDBGridDateColumn;
    dxDBGrid1Tabella: TdxDBGridMaskColumn;
    dxDBGrid1Operation: TdxDBGridMaskColumn;
    QUsersLK: TQuery;
    QUsersLKID: TAutoIncField;
    QUsersLKNominativo: TStringField;
    QLogUtente: TStringField;
    dxDBGrid1Column4: TdxDBGridColumn;
    QLogDescTabella: TStringField;
    QLogDescOperation: TStringField;
    procedure QLogCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  LogOperazioniSoggForm: TLogOperazioniSoggForm;

implementation

{$R *.DFM}

procedure TLogOperazioniSoggForm.QLogCalcFields(DataSet: TDataSet);
begin
     if QLogTabella.value='AnagAltreInfo' then QLogDescTabella.value:='altri dati anagrafici';
     if QLogTabella.value='AnagMansioni' then QLogDescTabella.value:='ruoli/mansioni';
     if QLogTabella.value='Anagrafica' then QLogDescTabella.value:='dati anagrafici';
     if QLogTabella.value='CompetenzeAnagrafica' then QLogDescTabella.value:='competenze';
     if QLogTabella.value='CorsiExtra' then QLogDescTabella.value:='corsi/stage/master';
     if QLogTabella.value='EsperienzeLavorative' then QLogDescTabella.value:='esperienze lavorative';
     if QLogTabella.value='LingueConosciute' then QLogDescTabella.value:='lingue conosciute';
     if QLogTabella.value='TitoliStudio' then QLogDescTabella.value:='titoli di studio';

     if QLogOperation.value='U' then QLogDescOperation.value:='aggiornamento';
     if QLogOperation.value='I' then QLogDescOperation.value:='inserimento';
     if QLogOperation.value='D' then QLogDescOperation.value:='cancellazione';
end;

end.
