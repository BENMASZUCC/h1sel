unit CreaAnnuncio;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Grids, DBGrids, ExtCtrls;

type
  TCreaAnnuncioForm = class(TForm)
    GroupBox1: TGroupBox;
    RadioGroup1: TRadioGroup;
    DBGrid1: TDBGrid;
    BitBtn1: TBitBtn;
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CreaAnnuncioForm: TCreaAnnuncioForm;

implementation

uses ModuloDati3, SchedaTestate;

{$R *.DFM}

procedure TCreaAnnuncioForm.BitBtn1Click(Sender: TObject);
begin
     SchedaTestateForm:=TSchedaTestateForm.create(self);
     SchedaTestateForm.ShowModal;
     SchedaTestateForm.Free;
end;

end.
