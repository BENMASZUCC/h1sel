object SelRicCandForm: TSelRicCandForm
  Left = 230
  Top = 116
  ActiveControl = DBGrid1
  BorderStyle = bsDialog
  Caption = 'Selezione ricerca'
  ClientHeight = 222
  ClientWidth = 493
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object BitBtn1: TBitBtn
    Left = 397
    Top = 2
    Width = 93
    Height = 36
    TabOrder = 0
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 397
    Top = 40
    Width = 93
    Height = 36
    Caption = 'Annulla'
    TabOrder = 1
    Kind = bkCancel
  end
  object Panel124: TPanel
    Left = 3
    Top = 3
    Width = 390
    Height = 21
    Alignment = taLeftJustify
    Caption = '  Ricerche attive cui � associato il soggetto'
    Color = clGray
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
  end
  object DBGrid1: TDBGrid
    Left = 4
    Top = 25
    Width = 389
    Height = 193
    DataSource = DsQRicCand
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'Cliente'
        Width = 179
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Posizione'
        Width = 175
        Visible = True
      end>
  end
  object QRicCand: TQuery
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      
        'select EBC_Clienti.Descrizione Cliente, Mansioni.Descrizione Pos' +
        'izione,'
      
        '          EBC_CandidatiRicerche.ID IDCandRic, EBC_Clienti.ID IDC' +
        'liente,'
      '          IDMansione, TitoloCliente'
      
        'from EBC_Ricerche,EBC_Clienti,Mansioni,EBC_CandidatiRicerche wit' +
        'h (updlock)'
      'where EBC_Ricerche.IDCliente=EBC_Clienti.ID'
      '  and EBC_Ricerche.IDMansione=Mansioni.ID'
      '  and EBC_Ricerche.ID=EBC_CandidatiRicerche.IDRicerca'
      'and EBC_Ricerche.IDStatoRic=1'
      'and EBC_CandidatiRicerche.IDAnagrafica=:xIDAnag')
    Left = 96
    Top = 88
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'xIDAnag'
        ParamType = ptUnknown
      end>
  end
  object DsQRicCand: TDataSource
    DataSet = QRicCand
    Left = 96
    Top = 120
  end
end
