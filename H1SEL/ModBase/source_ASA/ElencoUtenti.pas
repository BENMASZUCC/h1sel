unit ElencoUtenti;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables, ExtCtrls, Grids, DBGrids, StdCtrls, Buttons;

type
  TElencoUtentiForm = class(TForm)
    DsUsers: TDataSource;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    DBGrid1: TDBGrid;
    Panel2: TPanel;
    QUsers: TQuery;
    QUsersID: TIntegerField;
    QUsersNominativo: TStringField;
    QUsersDescrizione: TStringField;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    xIDUtente:integer;
  end;

var
  ElencoUtentiForm: TElencoUtentiForm;

implementation

{$R *.DFM}

procedure TElencoUtentiForm.FormShow(Sender: TObject);
begin
     Caption:='[S/8] - '+caption;
     QUsers.ParamByName('xOggi').asDateTime:=Date;
     QUsers.Open;
     if xIDUtente>0 then
        while QUsersID.Value<>xIDUtente do QUsers.Next;
end;

procedure TElencoUtentiForm.FormCreate(Sender: TObject);
begin
     xIDUtente:=0;
end;

end.
