unit ElencoRicCliente;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, StdCtrls, Buttons, Mask, DBCtrls, ExtCtrls, Db, DBTables;

type
  TElencoRicClienteForm = class(TForm)
    Panel1: TPanel;
    Panel7: TPanel;
    DBEdit12: TDBEdit;
    BitBtn1: TBitBtn;
    DBGrid7: TDBGrid;
    BitBtn2: TBitBtn;
    QRicCliente: TQuery;
    DsQRicCliente: TDataSource;
    QRicClienteID: TAutoIncField;
    QRicClienteProgressivo: TStringField;
    QRicClienteDataInizio: TDateTimeField;
    QRicClienteDataFine: TDateTimeField;
    QRicClienteNumRicercati: TSmallintField;
    QRicClienteDallaData: TDateTimeField;
    QRicClienteRuolo: TStringField;
    QRicClienteUtente: TStringField;
    QRicClienteStatoRic: TStringField;
    QRicClienteStipendioLordo: TFloatField;
    QRicClienteTipo: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ElencoRicClienteForm: TElencoRicClienteForm;

implementation

uses ModuloDati, ModuloDati2, MDRicerche, InsRicerca, Main;

{$R *.DFM}

end.
