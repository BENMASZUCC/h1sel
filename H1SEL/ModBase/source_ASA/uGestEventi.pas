unit uGestEventi;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     StdCtrls,Buttons,Grids,DBGrids,Mask,DBCtrls,ExtCtrls,Db,DBTables,
     ModuloDati,ModuloDati2,MDRicerche,Main,
     InSelezione,ElencoRicPend,Inserimento,ContattoCand,FissaColloquio,
     EsitoColloquio,Presentaz,uUtilsVarie,DateInserimento;

function InsEvento(xIDAnag,xIDProc,xIDevento,xIDaStato,xIDTipoStatoA: integer; xData: TDateTime; xCognome,xNome,xMot: string; xIDRic,xIDutente,xIDcliente: integer): boolean;

implementation

function InsEvento(xIDAnag,xIDProc,xIDevento,xIDaStato,xIDTipoStatoA: integer; xData: TDateTime; xCognome,xNome,xMot: string; xIDRic,xIDutente,xIDcliente: integer): boolean;
var xAnnulla,xAggiornaStato: boolean;
     xHour,xMin,xSec,xMSec: Word;
     xYear,xMonth,xDay: Word;
     xIDCandRic: integer;
     Q: TQuery;
begin
     xAnnulla:=False;
     xAggiornaStato:=True;
     Data.DB.BeginTrans;
     try
          case xIDProc of
               1: begin// immissione in selezione
                         if xIDRic>0 then begin
                              if MessageDlg('Vuoi associare il candidato a QUESTA ricerca ?',mtInformation, [mbYes,mbNO],0)=mrYes then begin
                                   Q:=CreateQueryFromString('update EBC_CandidatiRicerche set Escluso=0, '+
                                        'where IDAnagrafica='+IntToStr(xIDAnag)+' and IDRicerca='+IntToStr(xIDRic));
                                   Q.ExecSQL;
                                   xMot:='associato alla ricerca '+DataRicerche.TRicerchePendProgressivo.asString;
                              end;
                         end else begin
                              Application.CreateForm(TInSelezioneForm,InSelezioneForm);
                              InSelezioneForm.TAnag.Findkey([xIDAnag]);
                              InSelezioneForm.ShowModal;
                              if InSelezioneForm.ModalResult=mrOK then begin
                                   if InSelezioneForm.RGopzioni.ItemIndex=1 then begin
                                        // selezione ricerca da abbinare
                                        Application.CreateForm(TElencoRicPendForm,ElencoRicPendForm);
                                        ElencoRicPendForm.ShowModal;
                                        if ElencoRicPendForm.ModalResult=mrOK then begin
                                             // registrazione abbinamento nome-ricerca
                                             DataRicerche.TRicAnagIDX.Open;
                                             if not DataRicerche.TRicAnagIDX.FindKey([ElencoRicPendForm.QRicAttiveID.Value,xIDAnag]) then begin
                                                  Q:=CreateQueryFromString('insert into EBC_CandidatiRicerche (IDRicerca,IDAnagrafica,Escluso) '+
                                                       'values (:xIDRicerca,:xIDAnagrafica,:xEscluso)');
                                                  Q.ParamByName('xIDRicerca').asInteger:=ElencoRicPendForm.QRicAttiveID.Value;
                                                  Q.ParamByName('xIDAnagrafica').asInteger:=xIDAnag;
                                                  Q.ParamByName('xEscluso').asBoolean:=False;
                                                  Q.ExecSQL;
                                             end;
                                             xMot:='associato alla ricerca '+ElencoRicPendForm.QRicAttiveProgressivo.Value;
                                             DataRicerche.TRicAnagIDX.Close;
                                        end;
                                        ElencoRicPendForm.Free;
                                   end;
                              end else xAnnulla:=True;
                              InSelezioneForm.Free;
                         end;
                    end;
               2: begin// inserimento in azienda
                         if xIDcliente=0 then begin
                              // non dalla ricerca
                              Application.CreateForm(TInserimentoForm,InserimentoForm);
                              InserimentoForm.TEBCclienti.IndexName:='';
                              InserimentoForm.TEBCclienti.FindKey([xIDcliente]);
                              InserimentoForm.TEBCclienti.IndexName:='IdxDescrizione';
                              InserimentoForm.ShowModal;
                              if InserimentoForm.ModalResult=mrCancel then xAnnulla:=True
                              else xIDCliente:=InserimentoForm.TEBCclientiID.Value;
                              InserimentoForm.Free;
                         end;
                         if not xAnnulla then begin
                              // richiesta date (data presunta inizio reale e data controllo
                              Application.CreateForm(TDateInserimentoForm,DateInserimentoForm);
                              DateInserimentoForm.DEDataInizio.Date:=Date;
                              DateInserimentoForm.DEDataControllo.Date:=Date;
                              DateInserimentoForm.ShowModal;
                              if DateInserimentoForm.ModalResult=mrCancel then xAnnulla:=True
                              else begin
                                   // eventuale messaggio in promemoria
                                   if DateInserimentoForm.CBProm.Checked then begin
                                        // nome cliente
                                        Data2.Qtemp.Close;
                                        Data2.Qtemp.SQL.text:='select Descrizione from EBC_Clienti where ID='+IntToStr(xIDCliente);
                                        Data2.Qtemp.Open;
                                        Q:=CreateQueryFromString('insert into Promemoria (IDUtente,IDUtenteDa,DataIns,DataDaLeggere,Testo,Evaso,IDCandUtente) '+
                                             'values (:xIDUtente,:xIDUtenteDa,:xDataIns,:xDataDaLeggere,:xTesto,:xEvaso,:xIDCandUtente)');
                                        Q.ParamByName('xIDUtente').asInteger:=xIDutente;
                                        Q.ParamByName('xIDUtenteDa').asInteger:=xIDutente;
                                        Q.ParamByName('xDataIns').asDateTime:=Date;
                                        Q.ParamByName('xDataDaLeggere').asDateTime:=DateInserimentoForm.DEDataControllo.Date;
                                        Q.ParamByName('xTesto').asString:='verificare l''inserimento nell'' azienda '+Data2.Qtemp.FieldByName('Descrizione').asString+
                                             ' di '+xCognome+' '+xNome;
                                        Q.ParamByName('xEvaso').asBoolean:=False;
                                        Q.ParamByName('xIDCandUtente').asInteger:=xIDAnag;
                                        Q.ExecSQL;
                                        Data2.Qtemp.Close;
                                   end;
                                   Data2.TClientiABS.Open;
                                   Data2.TClientiABS.Findkey([xIDCliente]);
                                   xMot:=xMot+' '+Data2.TClientiABSDescrizione.Value;
                                   Q:=CreateQueryFromString('insert into EBC_Inserimenti (IDAnagrafica,IDCliente,DallaData,IDRicerca,DataControllo) '+
                                        'values (:xIDAnagrafica,:xIDCliente,:xDallaData,:xIDRicerca,:xDataControllo)');
                                   Q.ParamByName('xIDAnagrafica').asInteger:=xIDAnag;
                                   Q.ParamByName('xIDCliente').asInteger:=xIDCliente;
                                   Q.ParamByName('xDallaData').asDateTime:=DateInserimentoForm.DEDataInizio.Date;
                                   Q.ParamByName('xIDRicerca').asInteger:=xIDRic;
                                   Q.ParamByName('xDataControllo').asDateTime:=DateInserimentoForm.DEDataControllo.Date;
                                   Q.ExecSQL;
                                   DateInserimentoForm.Free;
                                   // rimozione dalle ricerche dove risulta attivo (su richiesta)
                                   if MessageDlg('Escludere automaticamente il candidato dalle ricerche cui � associato ?',mtWarning, [mbYes,mbNo],0)=mrYes then begin
                                        DataRicerche.QGen.SQL.Clear;
                                        DataRicerche.QGen.SQL.Add('update EBC_CandidatiRicerche set Escluso=1,');
                                        DataRicerche.QGen.SQL.Add('Stato="inserimento nell''azienda '+Data2.TClientiABSDescrizione.Value+'" where IDAnagrafica='+IntToStr(xIDAnag));
                                        DataRicerche.QGen.execSQL;
                                   end;
                                   if xIDRic>0 then begin
                                        if MessageDlg('Inserire in esperienze lavorative (come attuale) ?',mtWarning, [mbYes,mbNo],0)=mrYes then begin
                                             // inserimento in esperienze lavorative (prima tutte le esp.lav. NON ATTUALI del sogg.)
                                             Data.Q1.Close;
                                             Data.Q1.SQL.text:='select IDArea,IDMansione from EBC_Ricerche where ID='+IntToStr(xIDRic);
                                             Data.Q1.Open;
                                             Q:=CreateQueryFromString('update EsperienzeLavorative set Attuale=0 '+
                                                  'where IDAnagrafica=:xIDAnagrafica');
                                             Q.ParamByName('xIDAnagrafica').asInteger:=xIDAnag;
                                             Q.ExecSQL;
                                             Q:=CreateQueryFromString('insert into EsperienzeLavorative (IDAnagrafica,IDAzienda,Attuale,AnnoDal,IDArea,IDMansione,IDSettore) '+
                                                  'values (:xIDAnagrafica,:xIDAzienda,:xAttuale,:xAnnoDal,:xIDArea,:xIDMansione,:xIDSettore)');
                                             Q.ParamByName('xIDAnagrafica').asInteger:=xIDAnag;
                                             Q.ParamByName('xIDAzienda').asInteger:=xIDCliente;
                                             Q.ParamByName('xAttuale').asBoolean:=True;
                                             DecodeDate(Date,xYear,xMonth,xDay);
                                             Q.ParamByName('xAnnoDal').asInteger:=xYear;
                                             if Data.Q1.IsEmpty then begin
                                                  Q.ParamByName('xIDArea').asInteger:=0;
                                                  Q.ParamByName('xIDMansione').asInteger:=0;
                                             end else begin
                                                  Q.ParamByName('xIDArea').asInteger:=Data.Q1.FieldByName('IDArea').asInteger;
                                                  Q.ParamByName('xIDMansione').asInteger:=Data.Q1.FieldByName('IDMansione').asInteger;
                                             end;
                                             // settore dell'azienda
                                             Data.Q1.Close;
                                             Data.Q1.SQL.text:='select IDAttivita from EBC_Clienti where ID='+IntToStr(xIDCliente);
                                             Data.Q1.Open;
                                             Q.ParamByName('xIDSettore').asInteger:=Data.Q1.FieldByName('IDAttivita').asInteger;
                                             Data.Q1.Close;
                                             Q.ExecSQL;
                                             Data.Q1.Close;
                                        end;
                                        // aggiornamento dati candidato-ricerca
                                        Q:=CreateQueryFromString('update EBC_CandidatiRicerche set Escluso=0,DataImpegno=null,Stato=:xStato '+
                                             'where IDAnagrafica='+IntToStr(xIDAnag)+' and IDRicerca='+IntToStr(xIDRic));
                                        Q.ParamByName('xStato').asString:='inserito nell''azienda';
                                        Q.ExecSQL;
                                        // richiesta di conclusione ricerca
                                        MessageDlg('ATTENZIONE: verificare se rendere conclusa la ricerca.',mtWarning, [mbOK],0);
                                        Data2.TClientiABS.Close;
                                   end;
                              end;
                         end;
                    end;
               3: begin// archiviazione
                         // rimozione dalle ricerche dove risulta attivo (su richiesta)
                         if MessageDlg('Eliminare il candidato dalle ricerche cui � associato ?',mtWarning, [mbYes,mbNo],0)=mrYes then begin
                              DataRicerche.QGen.SQL.Clear;
                              DataRicerche.QGen.SQL.Add('update EBC_CandidatiRicerche set Escluso=1,');
                              DataRicerche.QGen.SQL.Add('Stato="ELIMINATO '+xMot+'" where IDAnagrafica='+IntToStr(xIDAnag));
                              DataRicerche.QGen.execSQL;
                         end;
                    end;
               4: begin// registrazione contatto
                         Application.CreateForm(TContattoCandForm,ContattoCandForm);
                         ContattoCandForm.DEData.Date:=xData;
                         DecodeTime(Now,xHour,xMin,xSec,xMSec);
                         ContattoCandForm.MEOra.Text:=IntToStr(xHour)+'.'+IntToStr(xMin);
                         ContattoCandForm.xIDAnag:=xIDAnag;
                         ContattoCandForm.ShowModal;
                         if ContattoCandForm.Modalresult=mrCancel then xAnnulla:=True
                         else begin
                              case ContattoCandForm.RadioGroup1.ItemIndex of
                                   // NON DOVREBBE SERVIRE A NIENTE !
                                   0: xIDEvento:=66;
                                   1: xIDEvento:=68;
                                   2: xIDEvento:=69;
                                   3: xIDEvento:=70;
                                   4: xIDEvento:=71;
                                   5: xIDEvento:=72;
                              end;
                              xMot:=ContattoCandForm.RadioGroup2.Items[ContattoCandForm.RadioGroup2.ItemIndex];
                              if ContattoCandForm.RadioGroup2.ItemIndex=0 then begin
                                   // da richiamare
                                   if xIDRic>0 then begin
                                        // aggiornamento dati contatti-candidato
                                        Q:=CreateQueryFromString('Insert into EBC_ContattiCandidati (IDRicerca,IDAnagrafica,TipoContatto,Data,Ora,Esito,Evaso,Note,IDUtente) '+
                                             'values (:xIDRicerca,:xIDAnagrafica,:xTipoContatto,:xData,:xOra,:xEsito,:xEvaso,:xNote,:xIDUtente)');
                                        Q.ParamByName('xIDRicerca').asInteger:=xIDRic;
                                        Q.ParamByName('xIDAnagrafica').asInteger:=xIDAnag;
                                        Q.ParamByName('xTipoContatto').asString:=ContattoCandForm.RadioGroup1.Items[ContattoCandForm.RadioGroup1.ItemIndex];
                                        Q.ParamByName('xData').asDateTime:=ContattoCandForm.DEData.date;
                                        Q.ParamByName('xOra').asDateTime:=StrToDateTime(DatetoStr(ContattoCandForm.DEData.date)+' '+ContattoCandForm.MEOra.Text);
                                        Q.ParamByName('xEsito').asString:='da richiamare';
                                        Q.ParamByName('xEvaso').asBoolean:=False;
                                        Q.ParamByName('xNote').asString:=ContattoCandForm.ENote.Text;
                                        Q.ParamByName('xIDUtente').asInteger:=MainForm.xIDUtenteAttuale;
                                        Q.ExecSQL;
                                        // aggiornamento dati candidato-ricerca
                                        Q:=CreateQueryFromString('update EBC_CandidatiRicerche set Escluso=0,DataImpegno=null,Stato=:xStato '+
                                             'where IDAnagrafica='+IntToStr(xIDAnag)+' and IDRicerca='+IntToStr(xIDRic));
                                        Q.ParamByName('xStato').asString:='da richiamare '+ContattoCandForm.ENote.Text;
                                        Q.ExecSQL;
                                   end;
                              end;
                              if ContattoCandForm.RadioGroup2.ItemIndex=1 then begin
                                   // deve richiamare
                                   if xIDRic>0 then begin
                                        // aggiornamento dati contatti-candidato
                                        Q:=CreateQueryFromString('Insert into EBC_ContattiCandidati (IDRicerca,IDAnagrafica,TipoContatto,Data,Ora,Esito,Evaso,Note,IDUtente) '+
                                             'values (:xIDRicerca,:xIDAnagrafica,:xTipoContatto,:xData,:xOra,:xEsito,:xEvaso,:xNote,:xIDUtente)');
                                        Q.ParamByName('xIDRicerca').asInteger:=xIDRic;
                                        Q.ParamByName('xIDAnagrafica').asInteger:=xIDAnag;
                                        Q.ParamByName('xTipoContatto').asString:=ContattoCandForm.RadioGroup1.Items[ContattoCandForm.RadioGroup1.ItemIndex];
                                        Q.ParamByName('xData').asDateTime:=ContattoCandForm.DEData.date;
                                        Q.ParamByName('xOra').asDateTime:=StrToDateTime(DatetoStr(ContattoCandForm.DEData.date)+' '+ContattoCandForm.MEOra.Text);
                                        Q.ParamByName('xEsito').asString:='deve richiamare';
                                        Q.ParamByName('xEvaso').asBoolean:=False;
                                        Q.ParamByName('xNote').asString:=ContattoCandForm.ENote.Text;
                                        Q.ParamByName('xIDUtente').asInteger:=MainForm.xIDUtenteAttuale;
                                        Q.ExecSQL;
                                        // aggiornamento dati candidato-ricerca
                                        Q:=CreateQueryFromString('update EBC_CandidatiRicerche set Escluso=0,DataImpegno=null,Stato=:xStato '+
                                             'where IDAnagrafica='+IntToStr(xIDAnag)+' and IDRicerca='+IntToStr(xIDRic));
                                        Q.ParamByName('xStato').asString:='deve richiamare '+ContattoCandForm.ENote.Text;
                                        Q.ExecSQL;
                                   end;
                              end;
                              if ContattoCandForm.RadioGroup2.ItemIndex=2 then begin
                                   // fissare colloquio
                                   Application.CreateForm(TFissaColloquioForm,FissaColloquioForm);
                                   FissaColloquioForm.TSelezionatori.IndexName:='';
                                   FissaColloquioForm.TSelezionatori.FindKey([MainForm.xIDUtenteAttuale]);
                                   FissaColloquioForm.TSelezionatori.IndexName:='IdxNominativo';
                                   FissaColloquioForm.DEData.Date:=Date;
                                   DecodeTime(Now,xHour,xMin,xSec,xMSec);
                                   FissaColloquioForm.Ora.Text:=IntToStr(xHour)+'.'+IntToStr(xMin);
                                   if xMin+30>=60 then begin inc(xHour); xMin:=xMin-60; end;
                                   FissaColloquioForm.MEAlleOre.Text:=IntToStr(xHour+1)+'.'+IntToStr(xMin);
                                   FissaColloquioForm.Showmodal;
                                   if FissaColloquioForm.ModalResult=mrCancel then xAnnulla:=True else begin
                                        xMot:=xMot+' il '+DateToStr(FissaColloquioForm.DEData.Date)+' alle '+FissaColloquioForm.Ora.Text;
                                        xIDCandRic:=0;
                                        if xIDRic>0 then begin
                                             // aggiornamento dati candidato-ricerca
                                             if not xAnnulla then begin
                                                  Q:=CreateQueryFromString('update EBC_CandidatiRicerche set Escluso=0,DataImpegno=:xDataImpegno,Stato=:xStato '+
                                                       'where IDAnagrafica='+IntToStr(xIDAnag)+' and IDRicerca='+IntToStr(xIDRic));
                                                  Q.ParamByName('xDataImpegno').asDateTime:=FissaColloquioForm.DEData.Date;
                                                  if FissaColloquioForm.CBInterno.Checked then
                                                     if FissaColloquioForm.CBLuogo.Checked then
                                                        Q.ParamByName('xStato').asString:='colloquio alle '+FissaColloquioForm.Ora.Text
                                                     else Q.ParamByName('xStato').asString:='colloquio alle '+FissaColloquioForm.Ora.Text+' ('+FissaColloquioForm.TRisorseRisorsa.value+') '+ContattoCandForm.ENote.Text
                                                  else Q.ParamByName('xStato').asString:='colloquio ESTERNO alle '+FissaColloquioForm.Ora.Text+' '+ContattoCandForm.ENote.Text;
                                                  Q.ExecSQL;
                                             end;
                                             // aggiornamento dati contatti-candidato
                                             Q:=CreateQueryFromString('Insert into EBC_ContattiCandidati (IDRicerca,IDAnagrafica,TipoContatto,Data,Ora,Esito,Evaso,Note,IDUtente) '+
                                                  'values (:xIDRicerca,:xIDAnagrafica,:xTipoContatto,:xData,:xOra,:xEsito,:xEvaso,:xNote,:xIDUtente)');
                                             Q.ParamByName('xIDRicerca').asInteger:=xIDRic;
                                             Q.ParamByName('xIDAnagrafica').asInteger:=xIDAnag;
                                             Q.ParamByName('xTipoContatto').asString:=ContattoCandForm.RadioGroup1.Items[ContattoCandForm.RadioGroup1.ItemIndex];
                                             Q.ParamByName('xData').asDateTime:=ContattoCandForm.DEData.date;
                                             Q.ParamByName('xOra').asDateTime:=StrToDateTime(DatetoStr(ContattoCandForm.DEData.date)+' '+ContattoCandForm.MEOra.Text);
                                             Q.ParamByName('xEsito').asString:='fissato colloquio';
                                             Q.ParamByName('xEvaso').asBoolean:=False;
                                             Q.ParamByName('xNote').asString:=ContattoCandForm.ENote.Text;
                                             Q.ParamByName('xIDUtente').asInteger:=MainForm.xIDUtenteAttuale;
                                             Q.ExecSQL;
                                        end;
                                        if FissaColloquioForm.CBInterno.Checked then begin
                                             // agenda
                                             Q:=CreateQueryFromString('select ID from EBC_CandidatiRicerche where IDAnagrafica='+IntToStr(xIDAnag)+' and IDRicerca='+IntToStr(xIDRic));
                                             Q.Open;
                                             xIDCandRic:=Q.FieldByName('ID').asInteger;
                                             Q.Close;
                                             Q:=CreateQueryFromString('Insert into Agenda (Data,Ore,AlleOre,IDUtente,IDRisorsa,Tipo,IDCandRic,Descrizione) '+
                                                  'values (:xData,:xOre,:xAlleOre,:xIDUtente,:xIDRisorsa,:xTipo,:xIDCandRic,:xDescrizione)');
                                             Q.ParamByName('xData').asDateTime:=FissaColloquioForm.DEData.Date;
                                             Q.ParamByName('xOre').asDateTime:=StrToDateTime(DatetoStr(FissaColloquioForm.DEData.Date)+' '+FissaColloquioForm.Ora.Text);
                                             Q.ParamByName('xAlleOre').asDateTime:=StrToDateTime(DatetoStr(FissaColloquioForm.DEData.Date)+' '+FissaColloquioForm.MEAlleOre.Text);
                                             Q.ParamByName('xIDUtente').asInteger:=FissaColloquioForm.TSelezionatoriID.Value;
                                             if FissaColloquioForm.CBLuogo.Checked then
                                                  Q.ParamByName('xIDRisorsa').asInteger:=0
                                             else Q.ParamByName('xIDRisorsa').asInteger:=FissaColloquioForm.TRisorseID.Value;
                                             Q.ParamByName('xTipo').asInteger:=1;
                                             Q.ParamByName('xIDCandRic').asInteger:=xIDCandRic;
                                             Q.ParamByName('xDescrizione').asString:=xCognome+' '+copy(xNome,1,1);
                                             Q.ExecSQL;
                                        end else xMot:=xMot+' ESTERNO';
                                   end;
                                   FissaColloquioForm.Free;
                              end;
                              if ContattoCandForm.RadioGroup2.ItemIndex=3 then begin
                                   // NON ACCETTA
                                   xMot:=ContattoCandForm.EDesc.Text+' '+ContattoCandForm.EMotNonAcc.Text;
                                   // aggiornamento dati contatti-candidato
                                   if xIDRic>0 then begin
                                        Q:=CreateQueryFromString('Insert into EBC_ContattiCandidati (IDRicerca,IDAnagrafica,TipoContatto,Data,Ora,Esito,Evaso,Note,IDMotivoNonAcc,IDUtente) '+
                                             'values (:xIDRicerca,:xIDAnagrafica,:xTipoContatto,:xData,:xOra,:xEsito,:xEvaso,:xNote,:xIDMotivoNonAcc,:xIDUtente)');
                                        Q.ParamByName('xIDRicerca').asInteger:=xIDRic;
                                        Q.ParamByName('xIDAnagrafica').asInteger:=xIDAnag;
                                        Q.ParamByName('xTipoContatto').asString:=ContattoCandForm.RadioGroup1.Items[ContattoCandForm.RadioGroup1.ItemIndex];
                                        Q.ParamByName('xData').asDateTime:=ContattoCandForm.DEData.date;
                                        Q.ParamByName('xOra').asDateTime:=StrToDateTime(DatetoStr(ContattoCandForm.DEData.date)+' '+ContattoCandForm.MEOra.Text);
                                        Q.ParamByName('xEsito').asString:='NON ACCETTA';
                                        Q.ParamByName('xEvaso').asBoolean:=False;
                                        Q.ParamByName('xNote').asString:=ContattoCandForm.ENote.Text;
                                        Q.ParamByName('xIDMotivoNonAcc').asInteger:=ContattoCandForm.xIDMotivoNonAcc;
                                        Q.ParamByName('xIDUtente').asInteger:=MainForm.xIDUtenteAttuale;
                                        Q.ExecSQL;
                                        // aggiornamento dati candidato-ricerca --> ESCLUSIONE
                                        Q:=CreateQueryFromString('update EBC_CandidatiRicerche set DataImpegno=null, Stato=:xStato, Escluso=1 '+
                                             'where IDAnagrafica='+IntToStr(xIDAnag)+' and IDRicerca='+IntToStr(xIDRic));
                                        Q.ParamByName('xStato').asString:='NON ACCETTA '+ContattoCandForm.EMotNonAcc.Text+' '+ContattoCandForm.ENote.Text;
                                        Q.ExecSQL;
                                        // aggiornamento storico
                                        Q.SQL.text:='insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) '+
                                             'values (:xIDAnagrafica,:xIDEvento,:xDataEvento,:xAnnotazioni,:xIDRicerca,:xIDUtente,:xIDCliente)';
                                        Q.ParamByName('xIDAnagrafica').asInteger:=xIDAnag;
                                        Q.ParamByName('xIDEvento').asInteger:=17;
                                        Q.ParamByName('xDataEvento').asDateTime:=date;
                                        Q.ParamByName('xAnnotazioni').asString:='NON ACCETTA '+ContattoCandForm.EMotNonAcc.Text+' '+ContattoCandForm.ENote.Text;
                                        Q.ParamByName('xIDRicerca').asInteger:=xIDRic;
                                        Q.ParamByName('xIDUtente').asInteger:=xIDutente;
                                        Q.ParamByName('xIDCliente').asInteger:=xIDCliente;
                                        Q.ExecSQL;
                                        // archiviazione CV
                                        Q.SQL.text:='update Anagrafica set IDStato=28,IDTipoStato=2 where ID='+IntToStr(xIDAnag);
                                        Q.ExecSQL;
                                        xAggiornaStato:=False;
                                   end;
                              end;

                              if ContattoCandForm.RadioGroup2.ItemIndex=4 then begin
                                   // altro esito -> semplice registrazione
                                   // aggiornamento dati contatti-candidato
                                   xMot:=ContattoCandForm.EDesc.Text+' '+ContattoCandForm.ENote.Text;
                                   if xIDRic>0 then begin
                                        Q:=CreateQueryFromString('Insert into EBC_ContattiCandidati (IDRicerca,IDAnagrafica,TipoContatto,Data,Ora,Esito,Evaso,Note,IDUtente) '+
                                             'values (:xIDRicerca,:xIDAnagrafica,:xTipoContatto,:xData,:xOra,:xEsito,:xEvaso,:xNote,:xIDUtente)');
                                        Q.ParamByName('xIDRicerca').asInteger:=xIDRic;
                                        Q.ParamByName('xIDAnagrafica').asInteger:=xIDAnag;
                                        Q.ParamByName('xTipoContatto').asString:=ContattoCandForm.RadioGroup1.Items[ContattoCandForm.RadioGroup1.ItemIndex];
                                        Q.ParamByName('xData').asDateTime:=ContattoCandForm.DEData.date;
                                        Q.ParamByName('xOra').asDateTime:=StrToDateTime(DatetoStr(ContattoCandForm.DEData.date)+' '+ContattoCandForm.MEOra.Text);
                                        Q.ParamByName('xEsito').asString:=ContattoCandForm.EDesc.Text;
                                        Q.ParamByName('xEvaso').asBoolean:=False;
                                        Q.ParamByName('xNote').asString:=ContattoCandForm.ENote.Text;
                                        Q.ParamByName('xIDUtente').asInteger:=MainForm.xIDUtenteAttuale;
                                        Q.ExecSQL;
                                        // aggiornamento dati candidato-ricerca
                                        Q:=CreateQueryFromString('update EBC_CandidatiRicerche set DataImpegno=null,Stato=:xStato '+
                                             'where IDAnagrafica='+IntToStr(xIDAnag)+' and IDRicerca='+IntToStr(xIDRic));
                                        Q.ParamByName('xStato').asString:=ContattoCandForm.EDesc.Text+' '+ContattoCandForm.ENote.Text;
                                        Q.ExecSQL;
                                   end;
                              end;
                         end;
                         ContattoCandForm.Free;
                    end;
               5: begin// avvenuto colloquio
                         Application.CreateForm(TEsitoColloquioForm,EsitoColloquioForm);
                         EsitoColloquioForm.ECogn.Text:=xCognome;
                         EsitoColloquioForm.ENome.Text:=xNome;
                         EsitoColloquioForm.TSelezionatori.FindKey([xIDutente]);
                         EsitoColloquioForm.DataColloquio.Date:=xData;
                         EsitoColloquioForm.xIDAnag:=xIDAnag;
                         if xIDRic=0 then begin
                              EsitoColloquioForm.CBEsito.Color:=clBtnFace;
                              EsitoColloquioForm.CBEsito.Enabled:=False;
                         end;
                         EsitoColloquioForm.ShowModal;
                         if EsitoColloquioForm.ModalResult=mrOK then begin
                              xData:=EsitoColloquioForm.DataColloquio.Date;
                              xIDUtente:=EsitoColloquioForm.TSelezionatoriID.Value;
                              xMot:='Giudizio: '+EsitoColloquioForm.CBGiudizio.text;
                              Q:=CreateQueryFromString('Insert into EBC_Colloqui (IDAnagrafica,IDRicerca,IDSelezionatore,data,ValutazioneGenerale,Punteggio,Esito) '+
                                   'values (:xIDAnagrafica,:xIDRicerca,:xIDSelezionatore,:xdata,:xValutazioneGenerale,:xPunteggio,:xEsito)');
                              Q.ParamByName('xIDAnagrafica').asInteger:=xIDAnag;
                              Q.ParamByName('xIDRicerca').asInteger:=xIDRic;
                              Q.ParamByName('xIDSelezionatore').asInteger:=EsitoColloquioForm.TSelezionatoriID.Value;
                              Q.ParamByName('xdata').asDateTime:=EsitoColloquioForm.DataColloquio.Date;
                              Q.ParamByName('xValutazioneGenerale').asString:=EsitoColloquioForm.CBGiudizio.text;
                              Q.ParamByName('xPunteggio').asInteger:=EsitoColloquioForm.SEPunteggio.Value;
                              Q.ParamByName('xEsito').asString:=EsitoColloquioForm.CBEsito.text;
                              Q.ExecSQL;
                              // gestione dei possibili esiti
                              if (xIDRic>0)and(EsitoColloquioForm.CBEsito.Text='mantenere nella ricerca') then begin
                                   xMot:='il '+DateToStr(EsitoColloquioForm.DataColloquio.Date)+' ('+EsitoColloquioForm.CBGiudizio.text+'-'+IntToStr(EsitoColloquioForm.SEPunteggio.Value)+')';
                                   // aggiornamento dati candidato-ricerca
                                   Q:=CreateQueryFromString('update EBC_CandidatiRicerche set DataImpegno=null,Escluso=0,Stato=:xStato '+
                                        'where IDAnagrafica='+IntToStr(xIDAnag)+' and IDRicerca='+IntToStr(xIDRic));
                                   Q.ParamByName('xStato').asString:='colloquio sostenuto il '+DateToStr(EsitoColloquioForm.DataColloquio.Date)+' ('+EsitoColloquioForm.CBGiudizio.text+'-'+IntToStr(EsitoColloquioForm.SEPunteggio.Value)+')';
                                   Q.ExecSQL;
                              end;
                              if (xIDRic>0)and(EsitoColloquioForm.CBEsito.Text='eliminare dalla ricerca') then begin
                                   xMot:='ric.'+DataRicerche.TRicerchePendProgressivo.asString+' - colloquio '+DateToStr(EsitoColloquioForm.DataColloquio.Date)+' ('+EsitoColloquioForm.CBGiudizio.text+'-'+IntToStr(EsitoColloquioForm.SEPunteggio.Value)+')';
                                   // registrare anche l'avvenuto colloquio
                                   Q:=CreateQueryFromString('insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) '+
                                        'values (:xIDAnagrafica,:xIDEvento,:xDataEvento,:xAnnotazioni,:xIDRicerca,:xIDUtente,:xIDCliente)');
                                   Q.ParamByName('xIDAnagrafica').asInteger:=xIDAnag;
                                   Q.ParamByName('xIDEvento').asInteger:=xIDevento;
                                   Q.ParamByName('xDataEvento').asDateTime:=xData;
                                   Q.ParamByName('xAnnotazioni').asString:=xMot;
                                   Q.ParamByName('xIDRicerca').asInteger:=xIDRic;
                                   Q.ParamByName('xIDUtente').asInteger:=xIDutente;
                                   Q.ParamByName('xIDCliente').asInteger:=xIDCliente;
                                   Q.ExecSQL;
                                   xIDevento:=17;

                                   DataRicerche.QGen.Close;
                                   DataRicerche.QGen.SQL.Clear;
                                   DataRicerche.QGen.SQL.Add('select IDRicerca from EBC_CandidatiRicerche where IDAnagrafica='+IntToStr(xIDAnag));
                                   DataRicerche.QGen.Open;
                                   if not DataRicerche.QGen.IsEmpty then begin
                                        if MessageDlg('Il candidato � associato a una o pi� ricerche: eliminarlo da queste ?',mtWarning, [mbYes,mbNO],0)=mrYes then begin
                                             DataRicerche.QGen.Close;
                                             DataRicerche.QGen.SQL.Clear;
                                             DataRicerche.QGen.SQL.Add('update EBC_CandidatiRicerche set Escluso=1,');
                                             DataRicerche.QGen.SQL.Add('Stato="ELIMINATO '+xMot+'" where IDAnagrafica='+IntToStr(xIDAnag));
                                             DataRicerche.QGen.execSQL;
                                        end;
                                   end;
                              end;
                              if EsitoColloquioForm.CBEsito.Text='altro colloquio' then begin
                                   xMot:='fissato colloquio';
                                   // fissazione colloquio
                                   Application.CreateForm(TFissaColloquioForm,FissaColloquioForm);
                                   FissaColloquioForm.DEData.Date:=Date;
                                   FissaColloquioForm.Showmodal;
                                   xMot:=xMot+' il '+DateToStr(FissaColloquioForm.DEData.Date)+' alle '+FissaColloquioForm.Ora.Text;
                                   // aggiornamento dati candidato-ricerca
                                   if xIDRic>0 then begin
                                        Q:=CreateQueryFromString('update EBC_CandidatiRicerche set DataImpegno=:xData,Escluso=0,Stato=:xStato '+
                                             'where IDAnagrafica='+IntToStr(xIDAnag)+' and IDRicerca='+IntToStr(xIDRic));
                                        Q.ParamByName('xData').asDateTime:=FissaColloquioForm.DEData.Date;
                                        Q.ParamByName('xStato').asString:='fissato colloquio alle ore '+FissaColloquioForm.Ora.Text+' ('+FissaColloquioForm.TRisorseRisorsa.value+')';
                                        Q.ExecSQL;
                                   end;
                                   // agenda
                                   Q:=CreateQueryFromString('Insert into Agenda (Data,Ore,AlleOre,IDUtente,IDRisorsa,Tipo,IDCandRic,Descrizione) '+
                                        'values (:xData,:xOre,:xAlleOre,:xIDUtente,:xIDRisorsa,:xTipo,:xIDCandRic,:xDescrizione)');
                                   Q.ParamByName('xData').asDateTime:=FissaColloquioForm.DEData.Date;
                                   Q.ParamByName('xOre').asDateTime:=StrToDateTime(DatetoStr(FissaColloquioForm.DEData.Date)+' '+FissaColloquioForm.Ora.Text);
                                   Q.ParamByName('xAlleOre').asDateTime:=StrToDateTime(DatetoStr(FissaColloquioForm.DEData.Date)+' '+FissaColloquioForm.MEAlleOre.Text);
                                   Q.ParamByName('xIDUtente').asInteger:=xIDUtente;
                                   Q.ParamByName('xIDRisorsa').asInteger:=FissaColloquioForm.TRisorseID.Value;
                                   Q.ParamByName('xTipo').asInteger:=2;
                                   Q.ParamByName('xIDCandRic').asInteger:=DataRicerche.TRicAnagIDXID.Value;
                                   Q.ParamByName('xDescrizione').asString:=xCognome+' '+copy(xNome,1,1);
                                   Q.ExecSQL;
                                   FissaColloquioForm.Free;
                              end;
                         end else xAnnulla:=True;
                         EsitoColloquioForm.Free;
                    end;
               6: begin// presentazione all'azienda
                         Application.CreateForm(TPresentazForm,PresentazForm);
                         PresentazForm.ShowModal;
                         if PresentazForm.ModalResult=mrCancel then xAnnulla:=True
                         else begin
                              if xIDcliente=0 then begin
                                   Application.CreateForm(TInserimentoForm,InserimentoForm);
                                   InserimentoForm.Caption:='Selezione dell''azienda per presentazione';
                                   InserimentoForm.TEBCclienti.IndexName:='';
                                   InserimentoForm.TEBCclienti.FindKey([xIDcliente]);
                                   InserimentoForm.TEBCclienti.IndexName:='IdxDescrizione';
                                   InserimentoForm.ShowModal;
                                   if InserimentoForm.ModalResult=mrCancel then xAnnulla:=True
                                   else xIDCliente:=InserimentoForm.TEBCclientiID.Value;
                                   InserimentoForm.Free;
                              end;
                              // messaggio PRIVACY
                              MessageDlg('ATTENZIONE!! Prima di procedere con la presentazione di personale '+
                                   'accertarsi che il/i candidato/i sia/siano consenziente/i. (legge sulla privaci 675)',mtInformation, [mbOK],0);
                              if xIDRic>0 then begin
                                   // aggiornamento dati candidato-ricerca
                                   Q:=CreateQueryFromString('update EBC_CandidatiRicerche set DataImpegno=null,Escluso=0,Stato=:xStato '+
                                        'where IDAnagrafica='+IntToStr(xIDAnag)+' and IDRicerca='+IntToStr(xIDRic));
                                   if PresentazForm.RGOpzioni.ItemIndex>=0 then
                                        Q.ParamByName('xStato').asString:='presentato il '+DateToStr(xData)+' '+xmot+' '+PresentazForm.RGOpzioni.Items[PresentazForm.RGOpzioni.ItemIndex]
                                   else Q.ParamByName('xStato').asString:='presentato il '+DateToStr(xData)+' '+xmot;
                                   Q.ExecSQL;
                              end;
                              if PresentazForm.RGOpzioni.ItemIndex>=0 then
                                   xMot:=PresentazForm.RGOpzioni.Items[PresentazForm.RGOpzioni.ItemIndex]+' '+xMot;
                              if PresentazForm.RGOpzioni2.ItemIndex>=0 then
                                   xMot:=PresentazForm.RGOpzioni2.Items[PresentazForm.RGOpzioni2.ItemIndex]+' '+xMot;
                         end;
                         PresentazForm.Free;
                    end;
               7: begin// eliminazione dall'archivio
                         DataRicerche.QGen.Close;
                         DataRicerche.QGen.SQL.Clear;
                         DataRicerche.QGen.SQL.Add('select IDRicerca from EBC_CandidatiRicerche where IDAnagrafica='+IntToStr(xIDAnag));
                         DataRicerche.QGen.Open;
                         if not DataRicerche.QGen.IsEmpty then begin
                              if MessageDlg('Il candidato � associato a una o pi� ricerche: eliminarlo da queste ?',mtWarning, [mbYes,mbNO],0)=mrYes then begin
                                   DataRicerche.QGen.Close;
                                   DataRicerche.QGen.SQL.Clear;
                                   DataRicerche.QGen.SQL.Add('update EBC_CandidatiRicerche set Escluso=1,');
                                   DataRicerche.QGen.SQL.Add('Stato="ELIMINATO '+xMot+'" where IDAnagrafica='+IntToStr(xIDAnag));
                                   DataRicerche.QGen.execSQL;
                              end;
                         end;
                    end;
          end;

          if xAnnulla then begin
               if Data.DB.InTransaction then Data.DB.RollbackTrans;
               Result:=False;
          end else begin
               // aggiornam. storico
               Q:=CreateQueryFromString('insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) '+
                    'values (:xIDAnagrafica,:xIDEvento,:xDataEvento,:xAnnotazioni,:xIDRicerca,:xIDUtente,:xIDCliente)');
               Q.ParamByName('xIDAnagrafica').asInteger:=xIDAnag;
               Q.ParamByName('xIDEvento').asInteger:=xIDevento;
               Q.ParamByName('xDataEvento').asDateTime:=xData;
               Q.ParamByName('xAnnotazioni').asString:=xMot;
               Q.ParamByName('xIDRicerca').asInteger:=xIDRic;
               Q.ParamByName('xIDUtente').asInteger:=xIDutente;
               Q.ParamByName('xIDCliente').asInteger:=xIDCliente;
               Q.ExecSQL;
               // aggiornamento stato e tipo stato in anagrafica
               if xAggiornaStato then begin
                    Q:=CreateQueryFromString('update Anagrafica set IDStato=:xIDStato,IDTipoStato=:xIDTipoStato '+
                         'where ID='+IntToStr(xIDAnag));
                    Q.ParamByName('xIDStato').asInteger:=xIDaStato;
                    Q.ParamByName('xIDTipoStato').asInteger:=xIDTipoStatoA;
                    Q.ExecSQL;
               end;
               Data.DB.CommitTrans;
               //Data.TAnagrafica.Refresh;
               Result:=True;
          end;
     except
          Data.DB.RollbackTrans;
          MessageDlg('Errore sul database: registrazione non avvenuta',mtError, [mbOK],0);
     end;
end;
end.

