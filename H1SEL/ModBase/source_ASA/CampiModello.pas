unit CampiModello;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons;

type
  TCampiModelloForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    CBCampo: TComboBox;
    EDescWord: TEdit;
    Label3: TLabel;
    procedure CBCampoChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CampiModelloForm: TCampiModelloForm;

implementation

{$R *.DFM}

procedure TCampiModelloForm.CBCampoChange(Sender: TObject);
begin
     if EDescWord.text='' then EDescWord.text:='#'+copy(CBCampo.Text,1,pos('(',CBCampo.Text)-1);
end;

end.
