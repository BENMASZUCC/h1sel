unit Costo;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     Buttons,StdCtrls,Spin,Mask,ToolEdit,CurrEdit;

type
     TCostoForm=class(TForm)
          BitBtn1: TBitBtn;
          BitBtn2: TBitBtn;
          Label1: TLabel;
          Label2: TLabel;
          Label3: TLabel;
          Label4: TLabel;
          Label5: TLabel;
          SEAnno: TSpinEdit;
          SEMese: TSpinEdit;
          EContoCosto: TEdit;
          SpeedButton1: TSpeedButton;
          EDesc: TEdit;
          RxCalcEdit1: TRxCalcEdit;
    procedure SpeedButton1Click(Sender: TObject);
     private
          { Private declarations }
     public
          xIDContoCosto: integer;
     end;

var
     CostoForm: TCostoForm;

implementation

uses uUtilsVarie, Main;

{$R *.DFM}

procedure TCostoForm.SpeedButton1Click(Sender: TObject);
var xID:string;
begin
     xID:=OpenSelFromTab('ContiCosto','ID','Conto','Conto di costo',IntToStr(xIDContoCosto));
     if xID='' then exit;
     xIDContoCosto:=StrToInt(xID);
     EContoCosto.Text:=MainForm.xgenericString;
end;

end.

