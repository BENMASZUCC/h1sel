unit ContattoCli;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, DBCtrls, Buttons, ExtCtrls;

type
  TContattoCliForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Panel1: TPanel;
    DBEDenomCliente: TDBEdit;
    Label1: TLabel;
    EContatto: TEdit;
    EFax: TEdit;
    ENote: TEdit;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label3: TLabel;
    ETelefono: TEdit;
    Label6: TLabel;
    EEmail: TEdit;
    ETitolo: TEdit;
    Label7: TLabel;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ContattoCliForm: TContattoCliForm;

implementation

{$R *.DFM}

procedure TContattoCliForm.FormShow(Sender: TObject);
begin
     Caption:='[M/103] - '+Caption;
end;

end.
