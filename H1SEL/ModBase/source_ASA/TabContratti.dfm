object TabContrattiForm: TTabContrattiForm
  Left = 248
  Top = 178
  BorderStyle = bsDialog
  Caption = 'Tabella contratti'
  ClientHeight = 494
  ClientWidth = 379
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 0
    Top = 317
    Width = 379
    Height = 3
    Cursor = crVSplit
    Align = alTop
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 379
    Height = 45
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object BitBtn1: TBitBtn
      Left = 274
      Top = 4
      Width = 100
      Height = 38
      Anchors = [akTop, akRight]
      Caption = 'Esci'
      TabOrder = 0
      OnClick = BitBtn1Click
      Kind = bkOK
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 45
    Width = 379
    Height = 272
    Align = alTop
    BevelOuter = bvLowered
    Caption = 'Panel2'
    TabOrder = 1
    object dxDBGrid1: TdxDBGrid
      Left = 1
      Top = 1
      Width = 297
      Height = 270
      Bands = <
        item
        end>
      DefaultLayout = True
      HeaderPanelRowCount = 1
      KeyField = 'ID'
      ShowGroupPanel = True
      SummaryGroups = <>
      SummarySeparator = ', '
      Align = alClient
      TabOrder = 0
      DataSource = DsQQualifContr
      Filter.Active = True
      OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
      OptionsView = [edgoBandHeaderWidth, edgoIndicator, edgoUseBitmap]
      object dxDBGrid1ID: TdxDBGridMaskColumn
        Visible = False
        Width = 46
        BandIndex = 0
        RowIndex = 0
        FieldName = 'ID'
      end
      object dxDBGrid1IDTipoContratto: TdxDBGridMaskColumn
        Visible = False
        Width = 58
        BandIndex = 0
        RowIndex = 0
        FieldName = 'IDTipoContratto'
      end
      object dxDBGrid1tipoContratto: TdxDBGridLookupColumn
        Caption = 'Tipo di contratto'
        Sorted = csUp
        Width = 124
        BandIndex = 0
        RowIndex = 0
        FieldName = 'tipoContratto'
      end
      object dxDBGrid1Qualifica: TdxDBGridMaskColumn
        Caption = 'Qualifica contrattuale'
        Width = 139
        BandIndex = 0
        RowIndex = 0
        FieldName = 'Qualifica'
      end
      object dxDBGrid1Ordine: TdxDBGridMaskColumn
        Visible = False
        Width = 46
        BandIndex = 0
        RowIndex = 0
        FieldName = 'Ordine'
      end
    end
    object Panel3: TPanel
      Left = 298
      Top = 1
      Width = 80
      Height = 270
      Align = alRight
      BevelOuter = bvLowered
      TabOrder = 1
      object BNew: TToolbarButton97
        Left = 1
        Top = 1
        Width = 77
        Height = 37
        Caption = 'Nuova qualifica'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          33333333FF33333333FF333993333333300033377F3333333777333993333333
          300033F77FFF3333377739999993333333333777777F3333333F399999933333
          33003777777333333377333993333333330033377F3333333377333993333333
          3333333773333333333F333333333333330033333333F33333773333333C3333
          330033333337FF3333773333333CC333333333FFFFF77FFF3FF33CCCCCCCCCC3
          993337777777777F77F33CCCCCCCCCC3993337777777777377333333333CC333
          333333333337733333FF3333333C333330003333333733333777333333333333
          3000333333333333377733333333333333333333333333333333}
        NumGlyphs = 2
        Opaque = False
        WordWrap = True
        OnClick = BNewClick
      end
      object BDel: TToolbarButton97
        Left = 1
        Top = 38
        Width = 77
        Height = 37
        Caption = 'Elimina qualifica'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333000000000
          3333333777777777F3333330F777777033333337F3F3F3F7F3333330F0808070
          33333337F7F7F7F7F3333330F080707033333337F7F7F7F7F3333330F0808070
          33333337F7F7F7F7F3333330F080707033333337F7F7F7F7F3333330F0808070
          333333F7F7F7F7F7F3F33030F080707030333737F7F7F7F7F7333300F0808070
          03333377F7F7F7F773333330F080707033333337F7F7F7F7F333333070707070
          33333337F7F7F7F7FF3333000000000003333377777777777F33330F88877777
          0333337FFFFFFFFF7F3333000000000003333377777777777333333330777033
          3333333337FFF7F3333333333000003333333333377777333333}
        NumGlyphs = 2
        Opaque = False
        WordWrap = True
        OnClick = BDelClick
      end
      object BCan: TToolbarButton97
        Left = 1
        Top = 112
        Width = 77
        Height = 37
        Caption = 'Annulla modifiche'
        Enabled = False
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00500005000555
          555557777F777555F55500000000555055557777777755F75555005500055055
          555577F5777F57555555005550055555555577FF577F5FF55555500550050055
          5555577FF77577FF555555005050110555555577F757777FF555555505099910
          555555FF75777777FF555005550999910555577F5F77777775F5500505509990
          3055577F75F77777575F55005055090B030555775755777575755555555550B0
          B03055555F555757575755550555550B0B335555755555757555555555555550
          BBB35555F55555575F555550555555550BBB55575555555575F5555555555555
          50BB555555555555575F555555555555550B5555555555555575}
        NumGlyphs = 2
        Opaque = False
        WordWrap = True
        OnClick = BCanClick
      end
      object BOK: TToolbarButton97
        Left = 1
        Top = 75
        Width = 77
        Height = 37
        Caption = 'Conferma modifiche'
        Enabled = False
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
          555555555555555555555555555555555555555555FF55555555555559055555
          55555555577FF5555555555599905555555555557777F5555555555599905555
          555555557777FF5555555559999905555555555777777F555555559999990555
          5555557777777FF5555557990599905555555777757777F55555790555599055
          55557775555777FF5555555555599905555555555557777F5555555555559905
          555555555555777FF5555555555559905555555555555777FF55555555555579
          05555555555555777FF5555555555557905555555555555777FF555555555555
          5990555555555555577755555555555555555555555555555555}
        NumGlyphs = 2
        Opaque = False
        WordWrap = True
        OnClick = BOKClick
      end
    end
  end
  object Panel4: TPanel
    Left = 0
    Top = 320
    Width = 379
    Height = 21
    Align = alTop
    Alignment = taLeftJustify
    Caption = '  Tabella Tipi di contratto'
    Color = clGray
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
  end
  object Panel5: TPanel
    Left = 299
    Top = 341
    Width = 80
    Height = 153
    Align = alRight
    BevelOuter = bvLowered
    TabOrder = 3
    object BTipoNew: TToolbarButton97
      Left = 1
      Top = 1
      Width = 77
      Height = 37
      Caption = 'Nuovo tipo'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        33333333FF33333333FF333993333333300033377F3333333777333993333333
        300033F77FFF3333377739999993333333333777777F3333333F399999933333
        33003777777333333377333993333333330033377F3333333377333993333333
        3333333773333333333F333333333333330033333333F33333773333333C3333
        330033333337FF3333773333333CC333333333FFFFF77FFF3FF33CCCCCCCCCC3
        993337777777777F77F33CCCCCCCCCC3993337777777777377333333333CC333
        333333333337733333FF3333333C333330003333333733333777333333333333
        3000333333333333377733333333333333333333333333333333}
      NumGlyphs = 2
      Opaque = False
      WordWrap = True
      OnClick = BTipoNewClick
    end
    object BTipoDel: TToolbarButton97
      Left = 1
      Top = 38
      Width = 77
      Height = 37
      Caption = 'Elimina tipo'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333000000000
        3333333777777777F3333330F777777033333337F3F3F3F7F3333330F0808070
        33333337F7F7F7F7F3333330F080707033333337F7F7F7F7F3333330F0808070
        33333337F7F7F7F7F3333330F080707033333337F7F7F7F7F3333330F0808070
        333333F7F7F7F7F7F3F33030F080707030333737F7F7F7F7F7333300F0808070
        03333377F7F7F7F773333330F080707033333337F7F7F7F7F333333070707070
        33333337F7F7F7F7FF3333000000000003333377777777777F33330F88877777
        0333337FFFFFFFFF7F3333000000000003333377777777777333333330777033
        3333333337FFF7F3333333333000003333333333377777333333}
      NumGlyphs = 2
      Opaque = False
      WordWrap = True
      OnClick = BTipoDelClick
    end
    object BTipoCan: TToolbarButton97
      Left = 1
      Top = 112
      Width = 77
      Height = 37
      Caption = 'Annulla modifiche'
      Enabled = False
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00500005000555
        555557777F777555F55500000000555055557777777755F75555005500055055
        555577F5777F57555555005550055555555577FF577F5FF55555500550050055
        5555577FF77577FF555555005050110555555577F757777FF555555505099910
        555555FF75777777FF555005550999910555577F5F77777775F5500505509990
        3055577F75F77777575F55005055090B030555775755777575755555555550B0
        B03055555F555757575755550555550B0B335555755555757555555555555550
        BBB35555F55555575F555550555555550BBB55575555555575F5555555555555
        50BB555555555555575F555555555555550B5555555555555575}
      NumGlyphs = 2
      Opaque = False
      WordWrap = True
      OnClick = BTipoCanClick
    end
    object BTipoOK: TToolbarButton97
      Left = 1
      Top = 75
      Width = 77
      Height = 37
      Caption = 'Conferma modifiche'
      Enabled = False
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
        555555555555555555555555555555555555555555FF55555555555559055555
        55555555577FF5555555555599905555555555557777F5555555555599905555
        555555557777FF5555555559999905555555555777777F555555559999990555
        5555557777777FF5555557990599905555555777757777F55555790555599055
        55557775555777FF5555555555599905555555555557777F5555555555559905
        555555555555777FF5555555555559905555555555555777FF55555555555579
        05555555555555777FF5555555555557905555555555555777FF555555555555
        5990555555555555577755555555555555555555555555555555}
      NumGlyphs = 2
      Opaque = False
      WordWrap = True
      OnClick = BTipoOKClick
    end
  end
  object dxDBGrid2: TdxDBGrid
    Left = 0
    Top = 341
    Width = 299
    Height = 153
    Bands = <
      item
      end>
    DefaultLayout = True
    HeaderPanelRowCount = 1
    KeyField = 'ID'
    SummaryGroups = <>
    SummarySeparator = ', '
    Align = alClient
    TabOrder = 4
    DataSource = DsQTipiContratti
    OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
    object dxDBGrid2ID: TdxDBGridMaskColumn
      Visible = False
      BandIndex = 0
      RowIndex = 0
      FieldName = 'ID'
    end
    object dxDBGrid2TipoContratto: TdxDBGridMaskColumn
      Caption = 'Tipo di contratto'
      Sorted = csUp
      BandIndex = 0
      RowIndex = 0
      FieldName = 'TipoContratto'
    end
  end
  object QQualifContr: TQuery
    Active = True
    BeforePost = QQualifContrBeforePost
    AfterPost = QQualifContrAfterPost
    DatabaseName = 'EBCDB'
    RequestLive = True
    SQL.Strings = (
      'select *'
      'from QualificheContrattuali')
    Left = 32
    Top = 120
    object QQualifContrID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.QualificheContrattuali.ID'
    end
    object QQualifContrIDTipoContratto: TIntegerField
      FieldName = 'IDTipoContratto'
      Origin = 'EBCDB.QualificheContrattuali.IDTipoContratto'
    end
    object QQualifContrQualifica: TStringField
      FieldName = 'Qualifica'
      Origin = 'EBCDB.QualificheContrattuali.Qualifica'
      FixedChar = True
      Size = 40
    end
    object QQualifContrOrdine: TSmallintField
      FieldName = 'Ordine'
      Origin = 'EBCDB.QualificheContrattuali.Ordine'
    end
    object QQualifContrtipoContratto: TStringField
      FieldKind = fkLookup
      FieldName = 'tipoContratto'
      LookupDataSet = QTipiContrattiLK
      LookupKeyFields = 'ID'
      LookupResultField = 'TipoContratto'
      KeyFields = 'IDTipoContratto'
      Lookup = True
    end
  end
  object DsQQualifContr: TDataSource
    DataSet = QQualifContr
    OnStateChange = DsQQualifContrStateChange
    Left = 32
    Top = 152
  end
  object QTipiContrattiLK: TQuery
    Active = True
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select * '
      'from TipiContratti')
    Left = 64
    Top = 128
    object QTipiContrattiLKID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.TipiContratti.ID'
    end
    object QTipiContrattiLKTipoContratto: TStringField
      FieldName = 'TipoContratto'
      Origin = 'EBCDB.TipiContratti.TipoContratto'
      FixedChar = True
    end
  end
  object QTipiContratti: TQuery
    Active = True
    AfterPost = QTipiContrattiAfterPost
    DatabaseName = 'EBCDB'
    RequestLive = True
    SQL.Strings = (
      'select * '
      'from TipiContratti')
    Left = 176
    Top = 384
    object QTipiContrattiID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.TipiContratti.ID'
    end
    object QTipiContrattiTipoContratto: TStringField
      FieldName = 'TipoContratto'
      Origin = 'EBCDB.TipiContratti.TipoContratto'
      FixedChar = True
    end
  end
  object DsQTipiContratti: TDataSource
    DataSet = QTipiContratti
    OnStateChange = DsQTipiContrattiStateChange
    Left = 175
    Top = 432
  end
end
