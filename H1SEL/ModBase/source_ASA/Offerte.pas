unit Offerte;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     StdCtrls,Mask,DBCtrls,Db,RXDBCtrl,ExtCtrls,DBTables,Grids,
     DBGrids,Buttons,TB97,dxDBTLCl,dxGrClms,dxTL,dxDBCtrl,dxDBGrid,
     dxCntner,dxGridMenus, Menus, dxPSCore, dxPSdxTLLnk, dxPSdxDBGrLnk,
  dxPSdxDBCtrlLnk;

type
     TOfferteForm=class(TForm)
          QUsers: TQuery;
          QUsersID: TAutoIncField;
          QUsersNominativo: TStringField;
          QUsersDescrizione: TStringField;
          DsUsers: TDataSource;
          QOfferte: TQuery;
          DsQOfferte: TDataSource;
          QOfferteID: TAutoIncField;
          QOfferteIDCliente: TIntegerField;
          QOfferteRif: TStringField;
          QOfferteData: TDateTimeField;
          QOfferteAMezzo: TStringField;
          QOfferteAttenzioneDi: TStringField;
          QOfferteAnticipoRichiesto: TFloatField;
          QOfferteCondizioni: TStringField;
          QOfferteEsito: TStringField;
          QOfferteNote: TStringField;
          QOfferteStato: TStringField;
          QOfferteTipo: TStringField;
          QOfferteIDUtente: TIntegerField;
          QOfferteImportoTotale: TFloatField;
          QOfferteCliente: TStringField;
          Panel4: TPanel;
          ToolbarButton9723: TToolbarButton97;
          QOfferteIDRicerca: TIntegerField;
          QOfferteIDContratto: TIntegerField;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1ID: TdxDBGridMaskColumn;
          dxDBGrid1IDCliente: TdxDBGridMaskColumn;
          dxDBGrid1Rif: TdxDBGridMaskColumn;
          dxDBGrid1Data: TdxDBGridDateColumn;
          dxDBGrid1AMezzo: TdxDBGridMaskColumn;
          dxDBGrid1AttenzioneDi: TdxDBGridMaskColumn;
          dxDBGrid1Condizioni: TdxDBGridMaskColumn;
          dxDBGrid1Esito: TdxDBGridMaskColumn;
          dxDBGrid1Note: TdxDBGridMaskColumn;
          dxDBGrid1Stato: TdxDBGridMaskColumn;
          dxDBGrid1Tipo: TdxDBGridMaskColumn;
          dxDBGrid1IDUtente: TdxDBGridMaskColumn;
          dxDBGrid1Cliente: TdxDBGridMaskColumn;
          dxDBGrid1IDRicerca: TdxDBGridMaskColumn;
          dxDBGrid1IDContratto: TdxDBGridMaskColumn;
    BitBtn1: TBitBtn;
    dxDBGrid1ImportoTotale: TdxDBGridCurrencyColumn;
    ToolbarButton971: TToolbarButton97;
    PMStampa: TPopupMenu;
    stampagriglia1: TMenuItem;
    esportainExcel1: TMenuItem;
    esportainHTML1: TMenuItem;
    dxPrinter1: TdxComponentPrinter;
    dxPrinter1Link1: TdxDBGridReportLink;
    dxDBGrid1LineaProdotto: TdxDBGridColumn;
    QOfferteIDLineaProdotto: TIntegerField;
    QOfferteLineaProdotto: TStringField;
    QOfferteIDArea: TIntegerField;
    QOfferteArea: TStringField;
    dxDBGrid1Area: TdxDBGridColumn;
          procedure FormShow(Sender: TObject);
          procedure BitBtn1Click(Sender: TObject);
          procedure ToolbarButton9723Click(Sender: TObject);
          procedure dxDBGrid1MouseUp(Sender: TObject; Button: TMouseButton;
               Shift: TShiftState; X,Y: Integer);
          procedure dxDBGrid1CustomDrawCell(Sender: TObject; ACanvas: TCanvas;
               ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
               ASelected,AFocused,ANewItemRow: Boolean; var AText: string;
               var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
               var ADone: Boolean);
    procedure dxDBGrid1CustomDrawFooter(Sender: TObject; ACanvas: TCanvas;
      ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
      var AText: String; var AColor: TColor; AFont: TFont;
      var AAlignment: TAlignment; var ADone: Boolean);
    procedure stampagriglia1Click(Sender: TObject);
    procedure esportainExcel1Click(Sender: TObject);
    procedure esportainHTML1Click(Sender: TObject);
     private
          xColumn: TColumn;
     public
          xIDUtente: integer;
     end;

var
     OfferteForm: TOfferteForm;

implementation

uses Main,OffertaCliente,ModuloDati,SelCliente,CreaCompensi;

{$R *.DFM}

procedure TOfferteForm.FormShow(Sender: TObject);
begin
     Caption:='[M/172] - '+Caption;
     QUsers.ParamByName('xOggi').asDateTime:=Date;
     QUsers.Open;
     while (not QUsers.EOF)and(QUsersID.value<>xIDUtente) do
          QUsers.Next;
     // filtro su presentate
     dxDBGrid1.Filter.Add(dxDBGrid1Stato,'presentata','presentata');
end;

procedure TOfferteForm.BitBtn1Click(Sender: TObject);
begin
     close;
end;

procedure TOfferteForm.ToolbarButton9723Click(Sender: TObject);
var xStato,xVecchioStato: string;
     i,xIDOfferta: integer;
     Vero: boolean;
begin
     if not mainForm.CheckProfile('144') then Exit;
     xVecchioStato:=QOfferteStato.Value;
     OffertaClienteForm:=TOffertaClienteForm.create(self);
     OffertaClienteForm.ERif.Text:=QOfferteRif.AsString;
     OffertaClienteForm.DEData.Date:=QOfferteData.Value;
     OffertaClienteForm.CBAMezzo.Text:=QOfferteAMezzo.Value;
     OffertaClienteForm.EAttenzioneDi.Text:=QOfferteAttenzioneDi.Value;
     OffertaClienteForm.ENote.text:=QOfferteNote.Value;
     OffertaClienteForm.ECondizioni.Text:=QOfferteCondizioni.Value;
     OffertaClienteForm.xIDUtente:=QOfferteIDUtente.Value;
     OffertaClienteForm.CBStato.text:=QOfferteStato.Value;
     OffertaClienteForm.Panel1.Visible:=False;
     xStato:=QOfferteStato.Value;
     OffertaClienteForm.xIDContratto:=QOfferteIDContratto.Value;
     OffertaClienteForm.xIDCliente:=QOfferteIDCliente.Value;
     if QOfferteIDRicerca.asString<>'' then
          OffertaClienteForm.xIDRicerca:=QOfferteIDRicerca.Value;
     OffertaClienteForm.xIDOfferta:=QOfferteID.Value;
     // tutto readonly
     OffertaClienteForm.PanOfferta.enabled:=False;
     OffertaClienteForm.BitBtn1.Visible:=False;
     OffertaClienteForm.BitBtn2.Caption:='Esci';
     OffertaClienteForm.Showmodal;
     OffertaClienteForm.Free;
end;

procedure TOfferteForm.dxDBGrid1MouseUp(Sender: TObject;
     Button: TMouseButton; Shift: TShiftState; X,Y: Integer);
begin
     if (Button<>mbRight)or(Shift<> []) then Exit;
     TdxDBGridPopupMenuManager.Instance.ShowGridPopupMenu(Sender as TdxDBGrid);
end;

procedure TOfferteForm.dxDBGrid1CustomDrawCell(Sender: TObject;
     ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
     AColumn: TdxTreeListColumn; ASelected,AFocused,ANewItemRow: Boolean;
     var AText: string; var AColor: TColor; AFont: TFont;
     var AAlignment: TAlignment; var ADone: Boolean);
begin
     if not MainForm.CheckProfile('147',false) then begin
          // l'utente non pu� visualizzare gli importi di TUTTE le offerte
          if AColumn=dxDBGrid1ImportoTotale then begin
               dxDBGrid1ImportoTotale.Visible:=False;
               dxDBGrid1ImportoTotale.DisableCustomizing:=False;
          end;
     end;
     if (not MainForm.CheckProfile('148',false))and
          (QUsersID.Value<>QOfferteIDUtente.Value) then begin
          // l'utente non pu� visualizzare gli importi delle offerte degli ALTRI UTENTI
          if AColumn=dxDBGrid1ImportoTotale then begin
               dxDBGrid1ImportoTotale.Visible:=False;
               dxDBGrid1ImportoTotale.DisableCustomizing:=False;
          end;
     end;
end;

procedure TOfferteForm.dxDBGrid1CustomDrawFooter(Sender: TObject;
  ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
  AColumn: TdxTreeListColumn; var AText: String; var AColor: TColor;
  AFont: TFont; var AAlignment: TAlignment; var ADone: Boolean);
begin
     AFont.Style:=[fsBold];
end;

procedure TOfferteForm.stampagriglia1Click(Sender: TObject);
begin
     dxPrinter1.Preview(True,dxPrinter1Link1);
end;

procedure TOfferteForm.esportainExcel1Click(Sender: TObject);
begin
     Mainform.Save('xls','Microsoft Excel 4.0 Worksheet (*.xls)|*.xls','ExpGrid.xls',dxDBGrid1.SaveToXLS);
end;

procedure TOfferteForm.esportainHTML1Click(Sender: TObject);
begin
     Mainform.Save('htm','HTML File (*.htm; *.html)|*.htm','ExpGrid.htm',dxDBGrid1.SaveToHTML);
end;

end.

