object FrmSUITEXAnagraficaExt: TFrmSUITEXAnagraficaExt
  Left = 0
  Top = 0
  Width = 551
  Height = 354
  TabOrder = 0
  object PanTitolo: TPanel
    Left = 0
    Top = 0
    Width = 551
    Height = 21
    Align = alTop
    Alignment = taLeftJustify
    Caption = '  Campi personalizzati SUITEX'
    Color = clGray
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
  end
  object PCSuitex: TPageControl
    Left = 0
    Top = 21
    Width = 551
    Height = 333
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 1
    OnChange = PCSuitexChange
    object TabSheet1: TTabSheet
      Caption = 'Dati agenti'
      object Label7: TLabel
        Left = 4
        Top = 10
        Width = 62
        Height = 13
        Caption = 'Tipo persona'
      end
      object Label2: TLabel
        Left = 5
        Top = 32
        Width = 43
        Height = 13
        Caption = 'Tipologia'
        FocusControl = DBLookupComboBox2
      end
      object DBComboBox1: TDBComboBox
        Left = 76
        Top = 6
        Width = 109
        Height = 21
        DataField = 'TipoPersona'
        DataSource = DsQSUITEXAnagExt
        ItemHeight = 13
        Items.Strings = (
          'persona fisica'
          'societ�')
        TabOrder = 0
      end
      object Panel1: TPanel
        Left = 4
        Top = 56
        Width = 534
        Height = 244
        Anchors = [akLeft, akTop, akRight, akBottom]
        BevelOuter = bvLowered
        Caption = 'Panel1'
        TabOrder = 1
        object dxDBGrid1: TdxDBGrid
          Left = 1
          Top = 61
          Width = 532
          Height = 182
          Bands = <
            item
            end>
          DefaultLayout = False
          HeaderPanelRowCount = 2
          KeyField = 'ID'
          SummaryGroups = <>
          SummarySeparator = ', '
          Align = alClient
          TabOrder = 0
          DataSource = DsQAnagSUITEX
          Filter.Criteria = {00000000}
          OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
          OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoIndicator, edgoUseBitmap]
          object dxDBGrid1Articolo: TdxDBGridLookupColumn
            Color = clInfoBk
            Width = 110
            BandIndex = 0
            RowIndex = 0
            FieldName = 'Articolo'
          end
          object dxDBGrid1Fascia: TdxDBGridLookupColumn
            Color = clInfoBk
            Width = 110
            BandIndex = 0
            RowIndex = 0
            FieldName = 'Fascia'
          end
          object dxDBGrid1Immagine: TdxDBGridLookupColumn
            Color = clInfoBk
            Width = 109
            BandIndex = 0
            RowIndex = 0
            FieldName = 'Immagine'
          end
          object dxDBGrid1Marchio: TdxDBGridLookupColumn
            Color = clInfoBk
            Width = 88
            BandIndex = 0
            RowIndex = 0
            FieldName = 'Marchio'
          end
          object dxDBGrid1TipoDist: TdxDBGridLookupColumn
            Caption = 'Tipo distrib.'
            Width = 117
            BandIndex = 0
            RowIndex = 1
            FieldName = 'TipoDist'
            StoredRowIndex = 1
          end
          object dxDBGrid1Settore: TdxDBGridLookupColumn
            Width = 109
            BandIndex = 0
            RowIndex = 1
            FieldName = 'Settore'
            StoredRowIndex = 1
          end
          object dxDBGrid1Cliente: TdxDBGridLookupColumn
            Width = 106
            BandIndex = 0
            RowIndex = 1
            FieldName = 'Cliente'
            StoredRowIndex = 1
          end
          object dxDBGrid1DataDal: TdxDBGridDateColumn
            Caption = 'Dal'
            Color = clAqua
            Width = 78
            BandIndex = 0
            RowIndex = 1
            FieldName = 'DataDal'
            StoredRowIndex = 1
          end
          object dxDBGrid1DataAl: TdxDBGridDateColumn
            Caption = 'Al'
            Color = clAqua
            Width = 70
            BandIndex = 0
            RowIndex = 1
            FieldName = 'DataAl'
            StoredRowIndex = 1
          end
          object dxDBGrid1Nazione: TdxDBGridMaskColumn
            Width = 51
            BandIndex = 0
            RowIndex = 0
            FieldName = 'Nazione'
          end
          object dxDBGrid1Attuale: TdxDBGridCheckColumn
            Color = clAqua
            Width = 56
            BandIndex = 0
            RowIndex = 1
            FieldName = 'Attuale'
            ValueChecked = 'True'
            ValueUnchecked = 'False'
            StoredRowIndex = 1
          end
          object dxDBGrid1Regione: TdxDBGridMaskColumn
            Width = 48
            BandIndex = 0
            RowIndex = 0
            FieldName = 'Regione'
          end
        end
        object Panel3: TPanel
          Left = 1
          Top = 22
          Width = 532
          Height = 39
          Align = alTop
          BevelOuter = bvLowered
          TabOrder = 1
          object BRigaNew: TToolbarButton97
            Left = 1
            Top = 1
            Width = 61
            Height = 37
            Caption = 'Nuova riga'
            Opaque = False
            WordWrap = True
            OnClick = BRigaNewClick
          end
          object BRigaDel: TToolbarButton97
            Left = 62
            Top = 1
            Width = 61
            Height = 37
            Caption = 'elimina riga'
            Opaque = False
            WordWrap = True
            OnClick = BRigaDelClick
          end
          object SpeedButton1: TSpeedButton
            Left = 342
            Top = 16
            Width = 24
            Height = 21
            Hint = 'articoli'
            Glyph.Data = {
              36050000424D3605000000000000360400002800000010000000100000000100
              08000000000000010000120B0000120B00000001000000010000000000008400
              0000FF00000084848400C6C6C600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00050505050505
              0505050505050505050500000000000000000000000000000005030303030303
              0303030303030303000503040505050505050505050505030005030405050505
              0505050505050503000503040503030303050303030305030005030405050505
              0505050505050503000503040503030303050303030305030005030405050505
              0505050505050503000503040503030303050303030305030005030405050505
              0505050505050503000503040303030303030303030303030005030401020102
              0102010500050003000503040404040404040404040404030005030303030303
              0303030303030303030505050505050505050505050505050505}
            ParentShowHint = False
            ShowHint = True
            OnClick = SpeedButton1Click
          end
          object SpeedButton2: TSpeedButton
            Left = 374
            Top = 16
            Width = 24
            Height = 21
            Hint = 'fasce'
            Glyph.Data = {
              36050000424D3605000000000000360400002800000010000000100000000100
              08000000000000010000120B0000120B00000001000000010000000000008400
              0000FF00000084848400C6C6C600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00050505050505
              0505050505050505050500000000000000000000000000000005030303030303
              0303030303030303000503040505050505050505050505030005030405050505
              0505050505050503000503040503030303050303030305030005030405050505
              0505050505050503000503040503030303050303030305030005030405050505
              0505050505050503000503040503030303050303030305030005030405050505
              0505050505050503000503040303030303030303030303030005030401020102
              0102010500050003000503040404040404040404040404030005030303030303
              0303030303030303030505050505050505050505050505050505}
            ParentShowHint = False
            ShowHint = True
            OnClick = SpeedButton2Click
          end
          object SpeedButton3: TSpeedButton
            Left = 406
            Top = 17
            Width = 24
            Height = 20
            Hint = 'immagini'
            Glyph.Data = {
              36050000424D3605000000000000360400002800000010000000100000000100
              08000000000000010000120B0000120B00000001000000010000000000008400
              0000FF00000084848400C6C6C600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00050505050505
              0505050505050505050500000000000000000000000000000005030303030303
              0303030303030303000503040505050505050505050505030005030405050505
              0505050505050503000503040503030303050303030305030005030405050505
              0505050505050503000503040503030303050303030305030005030405050505
              0505050505050503000503040503030303050303030305030005030405050505
              0505050505050503000503040303030303030303030303030005030401020102
              0102010500050003000503040404040404040404040404030005030303030303
              0303030303030303030505050505050505050505050505050505}
            ParentShowHint = False
            ShowHint = True
            OnClick = SpeedButton3Click
          end
          object SpeedButton4: TSpeedButton
            Left = 438
            Top = 16
            Width = 24
            Height = 21
            Hint = 'marchi'
            Glyph.Data = {
              36050000424D3605000000000000360400002800000010000000100000000100
              08000000000000010000120B0000120B00000001000000010000000000008400
              0000FF00000084848400C6C6C600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00050505050505
              0505050505050505050500000000000000000000000000000005030303030303
              0303030303030303000503040505050505050505050505030005030405050505
              0505050505050503000503040503030303050303030305030005030405050505
              0505050505050503000503040503030303050303030305030005030405050505
              0505050505050503000503040503030303050303030305030005030405050505
              0505050505050503000503040303030303030303030303030005030401020102
              0102010500050003000503040404040404040404040404030005030303030303
              0303030303030303030505050505050505050505050505050505}
            ParentShowHint = False
            ShowHint = True
            OnClick = SpeedButton4Click
          end
          object SpeedButton5: TSpeedButton
            Left = 470
            Top = 16
            Width = 24
            Height = 21
            Hint = 'tipologia di distribuzione'
            Glyph.Data = {
              36050000424D3605000000000000360400002800000010000000100000000100
              08000000000000010000120B0000120B00000001000000010000000000008400
              0000FF00000084848400C6C6C600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00050505050505
              0505050505050505050500000000000000000000000000000005030303030303
              0303030303030303000503040505050505050505050505030005030405050505
              0505050505050503000503040503030303050303030305030005030405050505
              0505050505050503000503040503030303050303030305030005030405050505
              0505050505050503000503040503030303050303030305030005030405050505
              0505050505050503000503040303030303030303030303030005030401020102
              0102010500050003000503040404040404040404040404030005030303030303
              0303030303030303030505050505050505050505050505050505}
            ParentShowHint = False
            ShowHint = True
            OnClick = SpeedButton5Click
          end
          object SpeedButton6: TSpeedButton
            Left = 502
            Top = 16
            Width = 24
            Height = 21
            Hint = 'tipologia'
            Glyph.Data = {
              36050000424D3605000000000000360400002800000010000000100000000100
              08000000000000010000120B0000120B00000001000000010000000000008400
              0000FF00000084848400C6C6C600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
              FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00050505050505
              0505050505050505050500000000000000000000000000000005030303030303
              0303030303030303000503040505050505050505050505030005030405050505
              0505050505050503000503040503030303050303030305030005030405050505
              0505050505050503000503040503030303050303030305030005030405050505
              0505050505050503000503040503030303050303030305030005030405050505
              0505050505050503000503040303030303030303030303030005030401020102
              0102010500050003000503040404040404040404040404030005030303030303
              0303030303030303030505050505050505050505050505050505}
            ParentShowHint = False
            ShowHint = True
            OnClick = SpeedButton6Click
          end
          object Bevel1: TBevel
            Left = 343
            Top = 7
            Width = 182
            Height = 3
            Shape = bsBottomLine
          end
          object Label1: TLabel
            Left = 341
            Top = 1
            Width = 72
            Height = 13
            Caption = 'Tabelle di base'
          end
          object BRigaOK: TToolbarButton97
            Left = 123
            Top = 1
            Width = 61
            Height = 37
            Caption = 'Conferma modifiche'
            Enabled = False
            Opaque = False
            WordWrap = True
            OnClick = BRigaOKClick
          end
          object BRigaCan: TToolbarButton97
            Left = 184
            Top = 1
            Width = 61
            Height = 37
            Caption = 'Annulla modifiche'
            Enabled = False
            Opaque = False
            WordWrap = True
            OnClick = BRigaCanClick
          end
        end
        object Panel4: TPanel
          Left = 1
          Top = 1
          Width = 532
          Height = 21
          Align = alTop
          Alignment = taLeftJustify
          Caption = '  Specifiche prodotti trattati'
          Color = clGray
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
        end
      end
      object DBLookupComboBox2: TDBLookupComboBox
        Left = 76
        Top = 29
        Width = 177
        Height = 21
        DataField = 'TdGTipologia'
        DataSource = DsQSUITEXAnagExt
        TabOrder = 2
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'Soci agenti'
      ImageIndex = 3
      object Panel5: TPanel
        Left = 4
        Top = 2
        Width = 535
        Height = 298
        Anchors = [akLeft, akTop, akRight, akBottom]
        BevelOuter = bvLowered
        Caption = 'Panel5'
        TabOrder = 0
        object Panel6: TPanel
          Left = 1
          Top = 1
          Width = 533
          Height = 21
          Align = alTop
          Alignment = taLeftJustify
          Caption = '  Soci'
          Color = clGray
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object Panel7: TPanel
          Left = 1
          Top = 22
          Width = 533
          Height = 39
          Align = alTop
          BevelOuter = bvLowered
          TabOrder = 1
          object BSocioNew: TToolbarButton97
            Left = 1
            Top = 1
            Width = 61
            Height = 37
            Hint = 'Nuovo socio'
            Caption = 'Nuovo socio'
            NumGlyphs = 2
            Opaque = False
            ParentShowHint = False
            ShowHint = True
            WordWrap = True
            OnClick = BSocioNewClick
          end
          object BSocioOK: TToolbarButton97
            Left = 123
            Top = 1
            Width = 61
            Height = 37
            Hint = 'conferma modifiche'
            Caption = 'conferma modifiche'
            Enabled = False
            NumGlyphs = 2
            Opaque = False
            ParentShowHint = False
            ShowHint = True
            WordWrap = True
            OnClick = BSocioOKClick
          end
          object BSocioCan: TToolbarButton97
            Left = 184
            Top = 1
            Width = 61
            Height = 37
            Hint = 'annulla modifiche'
            Caption = 'annulla modifiche'
            Enabled = False
            NumGlyphs = 2
            Opaque = False
            ParentShowHint = False
            ShowHint = True
            WordWrap = True
            OnClick = BSocioCanClick
          end
          object BSocioDel: TToolbarButton97
            Left = 62
            Top = 1
            Width = 61
            Height = 37
            Hint = 'Elimina socio'
            Caption = 'Elimina socio'
            NumGlyphs = 2
            Opaque = False
            ParentShowHint = False
            ShowHint = True
            WordWrap = True
            OnClick = BSocioDelClick
          end
        end
        object dxDBGrid2: TdxDBGrid
          Left = 1
          Top = 61
          Width = 533
          Height = 236
          Bands = <
            item
            end>
          DefaultLayout = True
          HeaderPanelRowCount = 1
          KeyField = 'ID'
          SummaryGroups = <>
          SummarySeparator = ', '
          Align = alClient
          TabOrder = 2
          DataSource = DsQAnagSoci
          Filter.Criteria = {00000000}
          OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
          OptionsView = [edgoBandHeaderWidth, edgoIndicator, edgoUseBitmap]
          object dxDBGrid2Socio: TdxDBGridLookupColumn
            Sorted = csUp
            Width = 328
            BandIndex = 0
            RowIndex = 0
            FieldName = 'Socio'
            DropDownWidth = 300
            ListFieldName = 'Cognome;nome'
          end
          object dxDBGrid2Note: TdxDBGridBlobColumn
            Width = 100
            BandIndex = 0
            RowIndex = 0
            HeaderMaxLineCount = 0
            FieldName = 'Note'
          end
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Aspirazioni agenti'
      ImageIndex = 2
      object Panel8: TPanel
        Left = 4
        Top = 4
        Width = 534
        Height = 297
        Anchors = [akLeft, akTop, akRight, akBottom]
        BevelOuter = bvLowered
        Caption = 'Panel1'
        TabOrder = 0
        object dxDBGrid3: TdxDBGrid
          Left = 1
          Top = 61
          Width = 532
          Height = 235
          Bands = <
            item
            end>
          DefaultLayout = False
          HeaderPanelRowCount = 2
          KeyField = 'ID'
          SummaryGroups = <>
          SummarySeparator = ', '
          Align = alClient
          TabOrder = 0
          DataSource = DsQAnagAspiraz
          Filter.Criteria = {00000000}
          OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
          OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoIndicator, edgoUseBitmap]
          object dxDBGrid3Settore: TdxDBGridLookupColumn
            Width = 184
            BandIndex = 0
            RowIndex = 1
            FieldName = 'Settore'
            StoredRowIndex = 1
          end
          object dxDBGrid3Articolo: TdxDBGridLookupColumn
            Color = clInfoBk
            Width = 484
            BandIndex = 0
            RowIndex = 0
            FieldName = 'Articolo'
          end
          object dxDBGrid3Fascia: TdxDBGridLookupColumn
            Color = clInfoBk
            Width = 484
            BandIndex = 0
            RowIndex = 0
            FieldName = 'Fascia'
          end
          object dxDBGrid3Immagine: TdxDBGridLookupColumn
            Color = clInfoBk
            Width = 484
            BandIndex = 0
            RowIndex = 0
            FieldName = 'Immagine'
          end
          object dxDBGrid3Marchio: TdxDBGridLookupColumn
            Color = clInfoBk
            Width = 484
            BandIndex = 0
            RowIndex = 0
            FieldName = 'Marchio'
          end
          object dxDBGrid3TipoDist: TdxDBGridLookupColumn
            Caption = 'Tipo Distribuzione'
            Width = 163
            BandIndex = 0
            RowIndex = 1
            FieldName = 'TipoDist'
            StoredRowIndex = 1
          end
          object dxDBGrid3Nazione: TdxDBGridMaskColumn
            Width = 85
            BandIndex = 0
            RowIndex = 1
            FieldName = 'Nazione'
            StoredRowIndex = 1
          end
          object dxDBGrid3Regione: TdxDBGridMaskColumn
            Width = 84
            BandIndex = 0
            RowIndex = 1
            FieldName = 'Regione'
            StoredRowIndex = 1
          end
        end
        object Panel9: TPanel
          Left = 1
          Top = 22
          Width = 532
          Height = 39
          Align = alTop
          BevelOuter = bvLowered
          TabOrder = 1
          object BAspirazNew: TToolbarButton97
            Left = 1
            Top = 1
            Width = 61
            Height = 37
            Caption = 'Nuova aspirazione'
            Opaque = False
            WordWrap = True
            OnClick = BAspirazNewClick
          end
          object BAspirazDel: TToolbarButton97
            Left = 62
            Top = 1
            Width = 61
            Height = 37
            Caption = 'elimina aspirazione'
            Opaque = False
            WordWrap = True
            OnClick = BAspirazDelClick
          end
          object BAspirazOK: TToolbarButton97
            Left = 123
            Top = 1
            Width = 61
            Height = 37
            Caption = 'Conferma modifiche'
            Enabled = False
            Opaque = False
            WordWrap = True
            OnClick = BAspirazOKClick
          end
          object BAspirazCan: TToolbarButton97
            Left = 184
            Top = 1
            Width = 61
            Height = 37
            Caption = 'Annulla modifiche'
            Enabled = False
            Opaque = False
            WordWrap = True
            OnClick = BAspirazCanClick
          end
        end
        object Panel10: TPanel
          Left = 1
          Top = 1
          Width = 532
          Height = 21
          Align = alTop
          Alignment = taLeftJustify
          Caption = '  Aspirazioni agenti'
          Color = clGray
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Dirigenti'
      ImageIndex = 1
      object GroupBox2: TGroupBox
        Left = 4
        Top = 2
        Width = 537
        Height = 64
        Caption = 'Dirigenti'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        object Label8: TLabel
          Left = 8
          Top = 16
          Width = 64
          Height = 13
          Caption = 'Tipo dirigente'
          FocusControl = DBEdit1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label9: TLabel
          Left = 8
          Top = 39
          Width = 22
          Height = 13
          Caption = 'Ditta'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label10: TLabel
          Left = 256
          Top = 16
          Width = 25
          Height = 13
          Caption = 'Stato'
          FocusControl = DBEdit3
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object DBEdit1: TDBEdit
          Left = 83
          Top = 13
          Width = 164
          Height = 21
          DataField = 'DirigenteTipo'
          DataSource = DsQSUITEXAnagExt
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object DBEdit3: TDBEdit
          Left = 291
          Top = 13
          Width = 164
          Height = 21
          DataField = 'DirigenteStato'
          DataSource = DsQSUITEXAnagExt
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
        object DBLookupComboBox1: TDBLookupComboBox
          Left = 83
          Top = 36
          Width = 238
          Height = 21
          DataField = 'DirCliente'
          DataSource = DsQSUITEXAnagExt
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
        end
      end
    end
  end
  object QSUITEXAnagExt: TQuery
    CachedUpdates = True
    AfterOpen = QSUITEXAnagExtAfterOpen
    BeforeClose = QSUITEXAnagExtBeforeClose
    AfterPost = QSUITEXAnagExtAfterPost
    DatabaseName = 'EBCDB'
    DataSource = Data.DsAnagrafica
    RequestLive = True
    SQL.Strings = (
      'select IDAnagrafica, TipoPersona, DirigenteIDCliente, '
      '          DirigenteTipo, DirigenteStato, IDTDgTipologia'
      'from AnagAltreInfo'
      'where IDAnagrafica=:ID')
    UpdateObject = UpdQSUITEXAnagExt
    Left = 200
    Top = 32
    ParamData = <
      item
        DataType = ftAutoInc
        Name = 'ID'
        ParamType = ptUnknown
      end>
    object QSUITEXAnagExtDirigenteIDCliente: TIntegerField
      FieldName = 'DirigenteIDCliente'
    end
    object QSUITEXAnagExtDirCliente: TStringField
      FieldKind = fkLookup
      FieldName = 'DirCliente'
      LookupDataSet = QClienti2LK
      LookupKeyFields = 'ID'
      LookupResultField = 'Descrizione'
      KeyFields = 'DirigenteIDCliente'
      Size = 50
      Lookup = True
    end
    object QSUITEXAnagExtIDAnagrafica: TIntegerField
      FieldName = 'IDAnagrafica'
      Origin = 'EBCDB.AnagAltreInfo.IDAnagrafica'
    end
    object QSUITEXAnagExtTipoPersona: TStringField
      FieldName = 'TipoPersona'
      Origin = 'EBCDB.AnagAltreInfo.TipoPersona'
      FixedChar = True
      Size = 30
    end
    object QSUITEXAnagExtDirigenteTipo: TStringField
      FieldName = 'DirigenteTipo'
      Origin = 'EBCDB.AnagAltreInfo.DirigenteTipo'
      FixedChar = True
      Size = 30
    end
    object QSUITEXAnagExtDirigenteStato: TStringField
      FieldName = 'DirigenteStato'
      Origin = 'EBCDB.AnagAltreInfo.DirigenteStato'
      FixedChar = True
      Size = 30
    end
    object QSUITEXAnagExtIDTDgTipologia: TIntegerField
      FieldName = 'IDTDgTipologia'
      Origin = 'EBCDB.AnagAltreInfo.IDTDgTipologia'
    end
    object QSUITEXAnagExtTdGTipologia: TStringField
      FieldKind = fkLookup
      FieldName = 'TdGTipologia'
      LookupDataSet = QTDgTipologia
      LookupKeyFields = 'ID'
      LookupResultField = 'Tipologia'
      KeyFields = 'IDTDgTipologia'
      Size = 80
      Lookup = True
    end
  end
  object DsQSUITEXAnagExt: TDataSource
    DataSet = QSUITEXAnagExt
    Left = 200
    Top = 64
  end
  object QProdArticolo: TQuery
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select * from SUITEX_ProdArticolo')
    Left = 328
    Top = 24
    object QProdArticoloID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.SUITEX_ProdArticolo.ID'
    end
    object QProdArticoloArticolo: TStringField
      FieldName = 'Articolo'
      Origin = 'EBCDB.SUITEX_ProdArticolo.Articolo'
      FixedChar = True
      Size = 80
    end
  end
  object QProdFascia: TQuery
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select * from SUITEX_ProdFascia')
    Left = 360
    Top = 22
    object QProdFasciaID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.SUITEX_ProdFascia.ID'
    end
    object QProdFasciaFascia: TStringField
      FieldName = 'Fascia'
      Origin = 'EBCDB.SUITEX_ProdFascia.Fascia'
      FixedChar = True
      Size = 80
    end
  end
  object QProdImmagine: TQuery
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select * from SUITEX_ProdImmagine')
    Left = 392
    Top = 26
    object QProdImmagineID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.SUITEX_ProdImmagine.ID'
    end
    object QProdImmagineImmagine: TStringField
      FieldName = 'Immagine'
      Origin = 'EBCDB.SUITEX_ProdImmagine.Immagine'
      FixedChar = True
      Size = 80
    end
  end
  object QProdMarchio: TQuery
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select * from SUITEX_ProdMarchio')
    Left = 424
    Top = 24
    object QProdMarchioID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.SUITEX_ProdMarchio.ID'
    end
    object QProdMarchioMarchio: TStringField
      FieldName = 'Marchio'
      Origin = 'EBCDB.SUITEX_ProdMarchio.Marchio'
      FixedChar = True
      Size = 80
    end
  end
  object QProdTipoDisp: TQuery
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select * from SUITEX_ProdTipoDist')
    Left = 456
    Top = 24
    object QProdTipoDispID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.SUITEX_ProdTipoDist.ID'
    end
    object QProdTipoDispTipoDist: TStringField
      FieldName = 'TipoDist'
      Origin = 'EBCDB.SUITEX_ProdTipoDist.TipoDist'
      FixedChar = True
      Size = 80
    end
  end
  object QTDgTipologia: TQuery
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select * from SUITEX_TDgTipologia')
    Left = 241
    Top = 65
    object QTDgTipologiaID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.SUITEX_TDgTipologia.ID'
    end
    object QTDgTipologiaTipologia: TStringField
      FieldName = 'Tipologia'
      Origin = 'EBCDB.SUITEX_TDgTipologia.Tipologia'
      FixedChar = True
      Size = 80
    end
  end
  object UpdQSUITEXAnagExt: TUpdateSQL
    ModifySQL.Strings = (
      'update AnagAltreInfo'
      'set'
      '  TipoPersona = :TipoPersona,'
      '  DirigenteIDCliente = :DirigenteIDCliente,'
      '  DirigenteTipo = :DirigenteTipo,'
      '  DirigenteStato = :DirigenteStato,'
      '  IDTDgTipologia = :IDTDgTipologia'
      'where'
      '  IDAnagrafica = :OLD_IDAnagrafica')
    InsertSQL.Strings = (
      'insert into AnagAltreInfo'
      
        '  (TipoPersona, DirigenteIDCliente, DirigenteTipo, DirigenteStat' +
        'o, IDTDgTipologia)'
      'values'
      
        '  (:TipoPersona, :DirigenteIDCliente, :DirigenteTipo, :Dirigente' +
        'Stato, '
      '   :IDTDgTipologia)')
    DeleteSQL.Strings = (
      'delete from AnagAltreInfo'
      'where'
      '  IDAnagrafica = :OLD_IDAnagrafica')
    Left = 200
    Top = 95
  end
  object QAnagSUITEX: TQuery
    CachedUpdates = True
    BeforeClose = QAnagSUITEXBeforeClose
    AfterInsert = QAnagSUITEXAfterInsert
    AfterPost = QAnagSUITEXAfterPost
    AfterDelete = QAnagSUITEXAfterPost
    DatabaseName = 'EBCDB'
    DataSource = Data.DsAnagrafica
    SQL.Strings = (
      'select * from SUITEX_AnagVarie'
      'where IDAnagrafica=:ID')
    UpdateObject = UpdQAnagSUITEX
    Left = 288
    Top = 197
    ParamData = <
      item
        DataType = ftAutoInc
        Name = 'ID'
        ParamType = ptUnknown
      end>
    object QAnagSUITEXID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.SUITEX_AnagVarie.ID'
    end
    object QAnagSUITEXIDAnagrafica: TIntegerField
      FieldName = 'IDAnagrafica'
      Origin = 'EBCDB.SUITEX_AnagVarie.IDAnagrafica'
    end
    object QAnagSUITEXIDProdArticolo: TIntegerField
      FieldName = 'IDProdArticolo'
      Origin = 'EBCDB.SUITEX_AnagVarie.IDProdArticolo'
    end
    object QAnagSUITEXIDProdFascia: TIntegerField
      FieldName = 'IDProdFascia'
      Origin = 'EBCDB.SUITEX_AnagVarie.IDProdFascia'
    end
    object QAnagSUITEXIDProdImmagine: TIntegerField
      FieldName = 'IDProdImmagine'
      Origin = 'EBCDB.SUITEX_AnagVarie.IDProdImmagine'
    end
    object QAnagSUITEXIDProdMarchio: TIntegerField
      FieldName = 'IDProdMarchio'
      Origin = 'EBCDB.SUITEX_AnagVarie.IDProdMarchio'
    end
    object QAnagSUITEXIDProdTipoDist: TIntegerField
      FieldName = 'IDProdTipoDist'
      Origin = 'EBCDB.SUITEX_AnagVarie.IDProdTipoDist'
    end
    object QAnagSUITEXIDCliente: TIntegerField
      FieldName = 'IDCliente'
      Origin = 'EBCDB.SUITEX_AnagVarie.IDCliente'
    end
    object QAnagSUITEXArticolo: TStringField
      FieldKind = fkLookup
      FieldName = 'Articolo'
      LookupDataSet = QProdArticolo
      LookupKeyFields = 'ID'
      LookupResultField = 'Articolo'
      KeyFields = 'IDProdArticolo'
      Size = 80
      Lookup = True
    end
    object QAnagSUITEXFascia: TStringField
      FieldKind = fkLookup
      FieldName = 'Fascia'
      LookupDataSet = QProdFascia
      LookupKeyFields = 'ID'
      LookupResultField = 'Fascia'
      KeyFields = 'IDProdFascia'
      Size = 80
      Lookup = True
    end
    object QAnagSUITEXImmagine: TStringField
      FieldKind = fkLookup
      FieldName = 'Immagine'
      LookupDataSet = QProdImmagine
      LookupKeyFields = 'ID'
      LookupResultField = 'Immagine'
      KeyFields = 'IDProdImmagine'
      Size = 80
      Lookup = True
    end
    object QAnagSUITEXMarchio: TStringField
      FieldKind = fkLookup
      FieldName = 'Marchio'
      LookupDataSet = QProdMarchio
      LookupKeyFields = 'ID'
      LookupResultField = 'Marchio'
      KeyFields = 'IDProdMarchio'
      Size = 80
      Lookup = True
    end
    object QAnagSUITEXTipoDist: TStringField
      FieldKind = fkLookup
      FieldName = 'TipoDist'
      LookupDataSet = QProdTipoDisp
      LookupKeyFields = 'ID'
      LookupResultField = 'TipoDist'
      KeyFields = 'IDProdTipoDist'
      Size = 80
      Lookup = True
    end
    object QAnagSUITEXCliente: TStringField
      FieldKind = fkLookup
      FieldName = 'Cliente'
      LookupDataSet = QClientiLK
      LookupKeyFields = 'ID'
      LookupResultField = 'Descrizione'
      KeyFields = 'IDCliente'
      Size = 50
      Lookup = True
    end
    object QAnagSUITEXIDSettore: TIntegerField
      FieldName = 'IDSettore'
      Origin = 'EBCDB.SUITEX_AnagVarie.IDSettore'
    end
    object QAnagSUITEXDataDal: TDateTimeField
      FieldName = 'DataDal'
      Origin = 'EBCDB.SUITEX_AnagVarie.DataDal'
    end
    object QAnagSUITEXDataAl: TDateTimeField
      FieldName = 'DataAl'
      Origin = 'EBCDB.SUITEX_AnagVarie.DataAl'
    end
    object QAnagSUITEXAttuale: TBooleanField
      FieldName = 'Attuale'
      Origin = 'EBCDB.SUITEX_AnagVarie.Attuale'
    end
    object QAnagSUITEXNazione: TStringField
      FieldName = 'Nazione'
      Origin = 'EBCDB.SUITEX_AnagVarie.Nazione'
      FixedChar = True
      Size = 30
    end
    object QAnagSUITEXRegione: TStringField
      FieldName = 'Regione'
      Origin = 'EBCDB.SUITEX_AnagVarie.Regione'
      FixedChar = True
      Size = 30
    end
    object QAnagSUITEXSettore: TStringField
      FieldKind = fkLookup
      FieldName = 'Settore'
      LookupDataSet = QSettoriLK
      LookupKeyFields = 'ID'
      LookupResultField = 'Attivita'
      KeyFields = 'IDSettore'
      Size = 50
      Lookup = True
    end
  end
  object DsQAnagSUITEX: TDataSource
    DataSet = QAnagSUITEX
    OnStateChange = DsQAnagSUITEXStateChange
    Left = 288
    Top = 232
  end
  object UpdQAnagSUITEX: TUpdateSQL
    ModifySQL.Strings = (
      'update SUITEX_AnagVarie'
      'set'
      '  IDAnagrafica = :IDAnagrafica,'
      '  IDProdArticolo = :IDProdArticolo,'
      '  IDProdFascia = :IDProdFascia,'
      '  IDProdImmagine = :IDProdImmagine,'
      '  IDProdMarchio = :IDProdMarchio,'
      '  IDProdTipoDist = :IDProdTipoDist,'
      '  IDCliente = :IDCliente,'
      '  DataDal = :DataDal,'
      '  DataAl = :DataAl,'
      '  Attuale = :Attuale,'
      '  Nazione = :Nazione,'
      '  Regione = :Regione'
      'where'
      '  ID = :OLD_ID')
    InsertSQL.Strings = (
      'insert into SUITEX_AnagVarie'
      
        '  (IDAnagrafica, IDProdArticolo, IDProdFascia, IDProdImmagine, I' +
        'DProdMarchio, '
      
        '   IDProdTipoDist, IDCliente, DataDal, DataAl, Attuale, Nazione,' +
        ' Regione)'
      'values'
      
        '  (:IDAnagrafica, :IDProdArticolo, :IDProdFascia, :IDProdImmagin' +
        'e, :IDProdMarchio, '
      
        '   :IDProdTipoDist, :IDCliente, :DataDal, :DataAl, :Attuale, :Na' +
        'zione, '
      '   :Regione)')
    DeleteSQL.Strings = (
      'delete from SUITEX_AnagVarie'
      'where'
      '  ID = :OLD_ID')
    Left = 288
    Top = 264
  end
  object QClientiLK: TQuery
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select ID,Descrizione'
      'from EBC_Clienti'
      'order by descrizione')
    Left = 512
    Top = 29
  end
  object QClienti2LK: TQuery
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select ID,Descrizione'
      'from EBC_Clienti'
      'order by descrizione')
    Left = 504
    Top = 64
    object QClienti2LKID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.EBC_Clienti.ID'
    end
    object QClienti2LKDescrizione: TStringField
      FieldName = 'Descrizione'
      Origin = 'EBCDB.EBC_Clienti.Descrizione'
      FixedChar = True
      Size = 50
    end
  end
  object QAnagSoci: TQuery
    CachedUpdates = True
    AfterOpen = QAnagSociAfterOpen
    BeforeClose = QAnagSociBeforeClose
    AfterInsert = QAnagSociAfterInsert
    AfterPost = QAnagSociAfterPost
    AfterDelete = QAnagSociAfterPost
    DatabaseName = 'EBCDB'
    DataSource = Data.DsAnagrafica
    SQL.Strings = (
      'select * from SUITEX_AnagSoci'
      'where IDAnagrafica=:ID')
    UpdateObject = UpdQAnagSoci
    Left = 212
    Top = 196
    ParamData = <
      item
        DataType = ftAutoInc
        Name = 'ID'
        ParamType = ptUnknown
      end>
    object QAnagSociID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.SUITEX_AnagSoci.ID'
    end
    object QAnagSociIDAnagrafica: TIntegerField
      FieldName = 'IDAnagrafica'
      Origin = 'EBCDB.SUITEX_AnagSoci.IDAnagrafica'
    end
    object QAnagSociIDAnagSocio: TIntegerField
      FieldName = 'IDAnagSocio'
      Origin = 'EBCDB.SUITEX_AnagSoci.IDAnagSocio'
    end
    object QAnagSociNote: TMemoField
      FieldName = 'Note'
      Origin = 'EBCDB.SUITEX_AnagSoci.Note'
      BlobType = ftMemo
    end
    object QAnagSociSocio: TStringField
      FieldKind = fkLookup
      FieldName = 'Socio'
      LookupDataSet = QAnagLK
      LookupKeyFields = 'ID'
      LookupResultField = 'Cognome'
      KeyFields = 'IDAnagSocio'
      Size = 30
      Lookup = True
    end
  end
  object DsQAnagSoci: TDataSource
    DataSet = QAnagSoci
    OnStateChange = DsQAnagSociStateChange
    Left = 212
    Top = 228
  end
  object UpdQAnagSoci: TUpdateSQL
    ModifySQL.Strings = (
      'update SUITEX_AnagSoci'
      'set'
      '  IDAnagrafica = :IDAnagrafica,'
      '  IDAnagSocio = :IDAnagSocio,'
      '  Note = :Note'
      'where'
      '  ID = :OLD_ID')
    InsertSQL.Strings = (
      'insert into SUITEX_AnagSoci'
      '  (IDAnagrafica, IDAnagSocio, Note)'
      'values'
      '  (:IDAnagrafica, :IDAnagSocio, :Note)')
    DeleteSQL.Strings = (
      'delete from SUITEX_AnagSoci'
      'where'
      '  ID = :OLD_ID')
    Left = 212
    Top = 260
  end
  object QAnagLK: TQuery
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select ID,Cognome,Nome'
      'from Anagrafica'
      'order by Cognome,Nome')
    Left = 188
    Top = 292
    object QAnagLKID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.Anagrafica.ID'
    end
    object QAnagLKCognome: TStringField
      FieldName = 'Cognome'
      Origin = 'EBCDB.Anagrafica.Cognome'
      FixedChar = True
      Size = 30
    end
    object QAnagLKNome: TStringField
      FieldName = 'Nome'
      Origin = 'EBCDB.Anagrafica.Nome'
      FixedChar = True
      Size = 30
    end
  end
  object DsQAnagLK: TDataSource
    DataSet = QAnagLK
    Left = 220
    Top = 292
  end
  object QAnagAspiraz: TQuery
    CachedUpdates = True
    BeforeClose = QAnagAspirazBeforeClose
    AfterInsert = QAnagAspirazAfterInsert
    AfterPost = QAnagAspirazAfterPost
    AfterDelete = QAnagAspirazAfterPost
    DatabaseName = 'EBCDB'
    DataSource = Data.DsAnagrafica
    SQL.Strings = (
      'select * from SUITEX_AnagAspiraz'
      'where IDAnagrafica=:ID')
    UpdateObject = UpdQAnagAspiraz
    Left = 440
    Top = 208
    ParamData = <
      item
        DataType = ftAutoInc
        Name = 'ID'
        ParamType = ptUnknown
      end>
    object QAnagAspirazID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.SUITEX_AnagAspiraz.ID'
    end
    object QAnagAspirazIDAnagrafica: TIntegerField
      FieldName = 'IDAnagrafica'
      Origin = 'EBCDB.SUITEX_AnagAspiraz.IDAnagrafica'
    end
    object QAnagAspirazIDSettore: TIntegerField
      FieldName = 'IDSettore'
      Origin = 'EBCDB.SUITEX_AnagAspiraz.IDSettore'
    end
    object QAnagAspirazIDProdArticolo: TIntegerField
      FieldName = 'IDProdArticolo'
      Origin = 'EBCDB.SUITEX_AnagAspiraz.IDProdArticolo'
    end
    object QAnagAspirazIDProdFascia: TIntegerField
      FieldName = 'IDProdFascia'
      Origin = 'EBCDB.SUITEX_AnagAspiraz.IDProdFascia'
    end
    object QAnagAspirazIDProdImmagine: TIntegerField
      FieldName = 'IDProdImmagine'
      Origin = 'EBCDB.SUITEX_AnagAspiraz.IDProdImmagine'
    end
    object QAnagAspirazIDProdMarchio: TIntegerField
      FieldName = 'IDProdMarchio'
      Origin = 'EBCDB.SUITEX_AnagAspiraz.IDProdMarchio'
    end
    object QAnagAspirazIDProdTipoDist: TIntegerField
      FieldName = 'IDProdTipoDist'
      Origin = 'EBCDB.SUITEX_AnagAspiraz.IDProdTipoDist'
    end
    object QAnagAspirazNazione: TStringField
      FieldName = 'Nazione'
      Origin = 'EBCDB.SUITEX_AnagAspiraz.Nazione'
      FixedChar = True
      Size = 30
    end
    object QAnagAspirazRegione: TStringField
      FieldName = 'Regione'
      Origin = 'EBCDB.SUITEX_AnagAspiraz.Regione'
      FixedChar = True
      Size = 30
    end
    object QAnagAspirazSettore: TStringField
      FieldKind = fkLookup
      FieldName = 'Settore'
      LookupDataSet = QSettoriLK
      LookupKeyFields = 'ID'
      LookupResultField = 'Attivita'
      KeyFields = 'IDSettore'
      Size = 50
      Lookup = True
    end
    object QAnagAspirazArticolo: TStringField
      FieldKind = fkLookup
      FieldName = 'Articolo'
      LookupDataSet = QProdArticolo
      LookupKeyFields = 'ID'
      LookupResultField = 'Articolo'
      KeyFields = 'IDProdArticolo'
      Size = 80
      Lookup = True
    end
    object QAnagAspirazFascia: TStringField
      FieldKind = fkLookup
      FieldName = 'Fascia'
      LookupDataSet = QProdFascia
      LookupKeyFields = 'ID'
      LookupResultField = 'Fascia'
      KeyFields = 'IDProdFascia'
      Size = 80
      Lookup = True
    end
    object QAnagAspirazImmagine: TStringField
      FieldKind = fkLookup
      FieldName = 'Immagine'
      LookupDataSet = QProdImmagine
      LookupKeyFields = 'ID'
      LookupResultField = 'Immagine'
      KeyFields = 'IDProdImmagine'
      Size = 80
      Lookup = True
    end
    object QAnagAspirazMarchio: TStringField
      FieldKind = fkLookup
      FieldName = 'Marchio'
      LookupDataSet = QProdMarchio
      LookupKeyFields = 'ID'
      LookupResultField = 'Marchio'
      KeyFields = 'IDProdMarchio'
      Size = 80
      Lookup = True
    end
    object QAnagAspirazTipoDist: TStringField
      FieldKind = fkLookup
      FieldName = 'TipoDist'
      LookupDataSet = QProdTipoDisp
      LookupKeyFields = 'ID'
      LookupResultField = 'TipoDist'
      KeyFields = 'IDProdTipoDist'
      Size = 80
      Lookup = True
    end
  end
  object DsQAnagAspiraz: TDataSource
    DataSet = QAnagAspiraz
    OnStateChange = DsQAnagAspirazStateChange
    Left = 440
    Top = 240
  end
  object UpdQAnagAspiraz: TUpdateSQL
    ModifySQL.Strings = (
      'update SUITEX_AnagAspiraz'
      'set'
      '  IDAnagrafica = :IDAnagrafica,'
      '  IDSettore = :IDSettore,'
      '  IDProdArticolo = :IDProdArticolo,'
      '  IDProdFascia = :IDProdFascia,'
      '  IDProdImmagine = :IDProdImmagine,'
      '  IDProdMarchio = :IDProdMarchio,'
      '  IDProdTipoDist = :IDProdTipoDist,'
      '  Nazione = :Nazione,'
      '  Regione = :Regione'
      'where'
      '  ID = :OLD_ID')
    InsertSQL.Strings = (
      'insert into SUITEX_AnagAspiraz'
      
        '  (IDAnagrafica, IDSettore, IDProdArticolo, IDProdFascia, IDProd' +
        'Immagine, '
      '   IDProdMarchio, IDProdTipoDist, Nazione, Regione)'
      'values'
      
        '  (:IDAnagrafica, :IDSettore, :IDProdArticolo, :IDProdFascia, :I' +
        'DProdImmagine, '
      '   :IDProdMarchio, :IDProdTipoDist, :Nazione, :Regione)')
    DeleteSQL.Strings = (
      'delete from SUITEX_AnagAspiraz'
      'where'
      '  ID = :OLD_ID')
    Left = 440
    Top = 272
  end
  object QSettoriLK: TQuery
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select * from EBC_Attivita'
      'order by attivita')
    Left = 465
    Top = 65
    object QSettoriLKID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.EBC_Attivita.ID'
    end
    object QSettoriLKAttivita: TStringField
      FieldName = 'Attivita'
      Origin = 'EBCDB.EBC_Attivita.Attivita'
      FixedChar = True
      Size = 50
    end
  end
end
