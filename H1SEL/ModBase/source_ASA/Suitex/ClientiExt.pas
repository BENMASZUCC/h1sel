unit ClientiExt;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     DBTables,Db,TB97,dxDBCtrl,dxDBGrid,dxTL,dxDBTLCl,dxGrClms,
     dxCntner,ExtCtrls,ComCtrls,StdCtrls,DBCtrls,Mask;

type
     TFrmSUITEXClientiExt=class(TFrame)
          Panel8: TPanel;
          Panel10: TPanel;
          QDitteExt: TQuery;
          DsQDitteExt: TDataSource;
          UpdQDitteExt: TUpdateSQL;
          QProdArticolo: TQuery;
          QProdArticoloID: TAutoIncField;
          QProdArticoloArticolo: TStringField;
          QProdFascia: TQuery;
          QProdFasciaID: TAutoIncField;
          QProdFasciaFascia: TStringField;
          QProdImmagine: TQuery;
          QProdImmagineID: TAutoIncField;
          QProdImmagineImmagine: TStringField;
          QProdMarchio: TQuery;
          QProdMarchioID: TAutoIncField;
          QProdMarchioMarchio: TStringField;
          QProdTipoDisp: TQuery;
          QProdTipoDispID: TAutoIncField;
          QProdTipoDispTipoDist: TStringField;
          QSettoriLK: TQuery;
          QSettoriLKID: TAutoIncField;
          QSettoriLKAttivita: TStringField;
          QDitteExtID: TAutoIncField;
          QDitteExtIDCliente: TIntegerField;
          QDitteExtIDSettore: TIntegerField;
          QDitteExtIDProdArticolo: TIntegerField;
          QDitteExtIDProdFascia: TIntegerField;
          QDitteExtIDProdImmagine: TIntegerField;
          QDitteExtIDProdMarchio: TIntegerField;
          QDitteExtIDProdTipoDist: TIntegerField;
          QDitteExtArticolo: TStringField;
          QDitteExtSettore: TStringField;
          QDitteExtFascia: TStringField;
          QDitteExtImmagine: TStringField;
          QDitteExtMarchio: TStringField;
          QDitteExtTipoDist: TStringField;
          PCSuitex: TPageControl;
          TSSuitex1: TTabSheet;
          TSSuitex2: TTabSheet;
          dxDBGrid3: TdxDBGrid;
          dxDBGrid3Articolo: TdxDBGridLookupColumn;
          dxDBGrid3Fascia: TdxDBGridLookupColumn;
          dxDBGrid3Immagine: TdxDBGridLookupColumn;
          dxDBGrid3Marchio: TdxDBGridLookupColumn;
          dxDBGrid3TipoDist: TdxDBGridLookupColumn;
          dxDBGrid3Settore: TdxDBGridLookupColumn;
          Panel9: TPanel;
          BNew: TToolbarButton97;
          BDel: TToolbarButton97;
          BOK: TToolbarButton97;
          BCan: TToolbarButton97;
          Panel1: TPanel;
          BAltriDatiOK: TToolbarButton97;
          BAltriDatiCan: TToolbarButton97;
          QDittaAltriDati: TQuery;
          DsQDittaAltriDati: TDataSource;
          UpdDittaAltriDati: TUpdateSQL;
          QDittaAltriDatiID: TAutoIncField;
          QDittaAltriDatiIDCliente: TIntegerField;
          QDittaAltriDatiDistNazDett: TStringField;
          QDittaAltriDatiDistNazIngrosso: TBooleanField;
          QDittaAltriDatiDistNazGD: TBooleanField;
          QDittaAltriDatiDistNazDO: TBooleanField;
          QDittaAltriDatiDistNazCNT3: TBooleanField;
          QDittaAltriDatiDistNazCorn: TBooleanField;
          QDittaAltriDatiDistNazPP: TBooleanField;
          QDittaAltriDatiDistEstDett: TStringField;
          QDittaAltriDatiDistEstIngrosso: TBooleanField;
          QDittaAltriDatiDistESTGD: TBooleanField;
          QDittaAltriDatiDistEstDO: TBooleanField;
          QDittaAltriDatiDistEstCNT3: TBooleanField;
          QDittaAltriDatiDistEstCorn: TBooleanField;
          QDittaAltriDatiDistEstPP: TBooleanField;
          GroupBox1: TGroupBox;
          Label1: TLabel;
          DBEdit1: TDBEdit;
          DBCheckBox1: TDBCheckBox;
          DBCheckBox2: TDBCheckBox;
          DBCheckBox3: TDBCheckBox;
          DBCheckBox4: TDBCheckBox;
          DBCheckBox5: TDBCheckBox;
          DBCheckBox6: TDBCheckBox;
          GroupBox2: TGroupBox;
          Label2: TLabel;
          DBEdit2: TDBEdit;
          DBCheckBox7: TDBCheckBox;
          DBCheckBox8: TDBCheckBox;
          DBCheckBox9: TDBCheckBox;
          DBCheckBox10: TDBCheckBox;
          DBCheckBox11: TDBCheckBox;
          DBCheckBox12: TDBCheckBox;
          Panel2: TPanel;
          Panel3: TPanel;
          Panel4: TPanel;
          BMezziDistNew: TToolbarButton97;
          BMezziDistDel: TToolbarButton97;
          BMezziDistOK: TToolbarButton97;
          BMezziDistCan: TToolbarButton97;
          QDittaMezziDist: TQuery;
          DsQDittaMezziDist: TDataSource;
          UpdDittaMezziDist: TUpdateSQL;
          QDittaMezziDistID: TAutoIncField;
          QDittaMezziDistIDCliente: TIntegerField;
          QDittaMezziDistIDMezzoDistNaz: TIntegerField;
          QDittaMezziDistIDMezzoDistEst: TIntegerField;
          dxDBGrid1: TdxDBGrid;
          QMezziDist: TQuery;
          QMezziDistID: TAutoIncField;
          QMezziDistMezzoDitribuzione: TStringField;
          QMezziDist2: TQuery;
          QMezziDist2ID: TAutoIncField;
          QMezziDist2MezzoDitribuzione: TStringField;
          QDittaMezziDistMezzoDistNaz: TStringField;
          QDittaMezziDistMezzoDistEst: TStringField;
          dxDBGrid1MezzoDistNaz: TdxDBGridLookupColumn;
          dxDBGrid1MezzoDistEst: TdxDBGridLookupColumn;
          BTabMezziDist: TToolbarButton97;
          TSSuitex3: TTabSheet;
          QDittaAltriDati2: TQuery;
          DsQDittaAltriDati2: TDataSource;
          UpdQDittaAltriDati2: TUpdateSQL;
          QDittaAltriDati2ID: TAutoIncField;
          QDittaAltriDati2classe: TStringField;
          QDittaAltriDati2TipoFatturaz: TStringField;
          QDittaAltriDati2FatturazPerc: TStringField;
          QDittaAltriDati2TipoProduzione: TStringField;
          Label3: TLabel;
          DBEdit3: TDBEdit;
          Label4: TLabel;
          DBEdit4: TDBEdit;
          Label5: TLabel;
          DBEdit5: TDBEdit;
          Label6: TLabel;
          DBEdit6: TDBEdit;
          Panel5: TPanel;
          BAltriDati2OK: TToolbarButton97;
          BAltriDati2Can: TToolbarButton97;
          TSSuitex4: TTabSheet;
          Panel6: TPanel;
          BAssociaAgente: TToolbarButton97;
          BEliminaAssociaz: TToolbarButton97;
          QDitteAgenti: TQuery;
          DsQDitteAgenti: TDataSource;
          QDitteAgentiCognome: TStringField;
          QDitteAgentiNome: TStringField;
          QDitteAgentiID: TAutoIncField;
          dxDBGrid2: TdxDBGrid;
          dxDBGrid2Cognome: TdxDBGridMaskColumn;
          dxDBGrid2Nome: TdxDBGridMaskColumn;
          procedure BNewClick(Sender: TObject);
          procedure BDelClick(Sender: TObject);
          procedure BOKClick(Sender: TObject);
          procedure BCanClick(Sender: TObject);
          procedure QDitteExtAfterPost(DataSet: TDataSet);
          procedure QDitteExtAfterInsert(DataSet: TDataSet);
          procedure QDitteExtBeforeClose(DataSet: TDataSet);
          procedure DsQDitteExtStateChange(Sender: TObject);
          procedure QDittaAltriDatiAfterPost(DataSet: TDataSet);
          procedure DsQDittaAltriDatiStateChange(Sender: TObject);
          procedure BAltriDatiOKClick(Sender: TObject);
          procedure BAltriDatiCanClick(Sender: TObject);
          procedure PCSuitexChange(Sender: TObject);
          procedure QDittaMezziDistAfterPost(DataSet: TDataSet);
          procedure QDittaMezziDistAfterInsert(DataSet: TDataSet);
          procedure BMezziDistNewClick(Sender: TObject);
          procedure BMezziDistDelClick(Sender: TObject);
          procedure BMezziDistOKClick(Sender: TObject);
          procedure BMezziDistCanClick(Sender: TObject);
          procedure DsQDittaMezziDistStateChange(Sender: TObject);
          procedure QDittaAltriDatiBeforePost(DataSet: TDataSet);
          procedure BTabMezziDistClick(Sender: TObject);
          procedure BAltriDati2OKClick(Sender: TObject);
          procedure BAltriDati2CanClick(Sender: TObject);
          procedure QDittaAltriDati2AfterPost(DataSet: TDataSet);
          procedure DsQDittaAltriDati2StateChange(Sender: TObject);
          procedure BAssociaAgenteClick(Sender: TObject);
          procedure BEliminaAssociazClick(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

implementation

uses ModuloDati,ModuloDati2,uUtilsVarie,ElencoDip;

{$R *.DFM}

procedure TFrmSUITEXClientiExt.BNewClick(Sender: TObject);
begin
     QDitteExt.Insert;
end;

procedure TFrmSUITEXClientiExt.BDelClick(Sender: TObject);
begin
     if MessageDlg('Sei sicuro di voler eliminare la riga ?',mtWarning, [mbNo,mbYes],0)=mrNo then exit;
     QDitteExt.Delete;
end;

procedure TFrmSUITEXClientiExt.BOKClick(Sender: TObject);
begin
     QDitteExt.Post;
end;

procedure TFrmSUITEXClientiExt.BCanClick(Sender: TObject);
begin
     QDitteExt.Cancel;
end;

procedure TFrmSUITEXClientiExt.QDitteExtAfterPost(DataSet: TDataSet);
begin
     with QDitteExt do begin
          Data.DB.BeginTrans;
          try
               ApplyUpdates;
               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate in AnagAltriDati',mtError, [mbOK],0);
          end;
          CommitUpdates;
     end;
end;

procedure TFrmSUITEXClientiExt.QDitteExtAfterInsert(DataSet: TDataSet);
begin
     QDitteExtIDCliente.Value:=Data2.TEBCClientiID.Value;
end;

procedure TFrmSUITEXClientiExt.QDitteExtBeforeClose(DataSet: TDataSet);
begin
     if DsQDitteExt.state in [dsInsert,dsEdit] then QDitteExt.Post;
end;

procedure TFrmSUITEXClientiExt.DsQDitteExtStateChange(Sender: TObject);
var b: boolean;
begin
     b:=DsQDitteExt.State in [dsEdit,dsInsert];
     BNew.Enabled:=not b;
     BDel.Enabled:=not b;
     BOK.Enabled:=b;
     BCan.Enabled:=b;
end;

procedure TFrmSUITEXClientiExt.QDittaAltriDatiAfterPost(DataSet: TDataSet);
begin
     with QDittaAltriDati do begin
          Data.DB.BeginTrans;
          try
               ApplyUpdates;
               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate in AnagAltriDati',mtError, [mbOK],0);
          end;
          CommitUpdates;
     end;
end;

procedure TFrmSUITEXClientiExt.DsQDittaAltriDatiStateChange(Sender: TObject);
var b: boolean;
begin
     b:=DsQDittaAltriDati.State in [dsEdit,dsInsert];
     BAltriDatiOK.Enabled:=b;
     BAltriDatiCan.Enabled:=b;
end;

procedure TFrmSUITEXClientiExt.BAltriDatiOKClick(Sender: TObject);
begin
     QDittaAltriDati.Post;
end;

procedure TFrmSUITEXClientiExt.BAltriDatiCanClick(Sender: TObject);
begin
     QDittaAltriDati.Cancel;
end;

procedure TFrmSUITEXClientiExt.PCSuitexChange(Sender: TObject);
begin
     if PCSuitex.ActivePageIndex=1 then begin
          QDittaAltriDati.Open;
          if QDittaAltriDati.IsEmpty then begin
               QDittaAltriDati.Insert;
               QDittaAltriDatiIDCliente.Value:=Data2.TEBCClientiID.Value;
               QDittaAltriDati.Post;
          end;
          QDittaMezziDist.Open;
     end else begin
          QDittaAltriDati.Close;
          QDittaMezziDist.Close;
     end;

     if PCSuitex.ActivePageIndex=2 then
          QDittaAltriDati2.Open
     else QDittaAltriDati2.Close;

     if PCSuitex.ActivePageIndex=3 then
          QDitteAgenti.Open
     else QDitteAgenti.Close;
end;

procedure TFrmSUITEXClientiExt.QDittaMezziDistAfterPost(DataSet: TDataSet);
begin
     with QDittaMezziDist do begin
          Data.DB.BeginTrans;
          try
               ApplyUpdates;
               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate in AnagAltriDati',mtError, [mbOK],0);
          end;
          CommitUpdates;
     end;
end;

procedure TFrmSUITEXClientiExt.QDittaMezziDistAfterInsert(
     DataSet: TDataSet);
begin
     QDittaMezziDistIDCliente.Value:=Data2.TEBCClientiID.Value;
end;

procedure TFrmSUITEXClientiExt.BMezziDistNewClick(Sender: TObject);
begin
     QDittaMezziDist.Insert;
end;

procedure TFrmSUITEXClientiExt.BMezziDistDelClick(Sender: TObject);
begin
     if MessageDlg('Sei sicuro di voler eliminare la riga ?',mtWarning, [mbNo,mbYes],0)=mrNo then exit;
     QDittaMezziDist.Delete;
end;

procedure TFrmSUITEXClientiExt.BMezziDistOKClick(Sender: TObject);
begin
     QDittaMezziDist.Post;
end;

procedure TFrmSUITEXClientiExt.BMezziDistCanClick(Sender: TObject);
begin
     QDittaMezziDist.Cancel;
end;

procedure TFrmSUITEXClientiExt.DsQDittaMezziDistStateChange(
     Sender: TObject);
var b: boolean;
begin
     b:=DsQDittaMezziDist.State in [dsEdit,dsInsert];
     BMezziDistNew.Enabled:=not b;
     BMezziDistDel.Enabled:=not b;
     BMezziDistOK.Enabled:=b;
     BMezziDistCan.Enabled:=b;
end;

procedure TFrmSUITEXClientiExt.QDittaAltriDatiBeforePost(
     DataSet: TDataSet);
begin
     if QDittaAltriDatiIDCliente.asString='' then begin
          QDittaAltriDatiIDCliente.Value:=Data2.TEBCClientiID.Value;
     end;
end;

procedure TFrmSUITEXClientiExt.BTabMezziDistClick(Sender: TObject);
begin
     OpenTab('SUITEX_MezziDistrib', ['ID','MezzoDitribuzione'], ['Mezzo di distribuzione']);
     QMezziDist.Close;
     QMezziDist.Open;
     QMezziDist2.Close;
     QMezziDist2.Open;
end;

procedure TFrmSUITEXClientiExt.BAltriDati2OKClick(Sender: TObject);
begin
     QDittaAltriDati2.Post;
end;

procedure TFrmSUITEXClientiExt.BAltriDati2CanClick(Sender: TObject);
begin
     QDittaAltriDati2.Cancel;
end;

procedure TFrmSUITEXClientiExt.QDittaAltriDati2AfterPost(
     DataSet: TDataSet);
begin
     with QDittaAltriDati2 do begin
          Data.DB.BeginTrans;
          try
               ApplyUpdates;
               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate in AnagAltriDati',mtError, [mbOK],0);
          end;
          CommitUpdates;
     end;
end;

procedure TFrmSUITEXClientiExt.DsQDittaAltriDati2StateChange(
     Sender: TObject);
var b: boolean;
begin
     b:=DsQDittaAltriDati2.State in [dsEdit,dsInsert];
     BAltriDati2OK.Enabled:=b;
     BAltriDati2Can.Enabled:=b;
end;

procedure TFrmSUITEXClientiExt.BAssociaAgenteClick(Sender: TObject);
begin
     ElencoDipForm:=TElencoDipForm.create(self);
     ElencoDipForm.ShowModal;
     if ElencoDipForm.ModalResult=mrOK then begin
          Data.QTemp.Close;
          Data.QTemp.SQL.text:='insert into SUITEX_AnagVarie (IDAnagrafica,IDCliente) '+
               'values (:xIDAnagrafica,:xIDCliente)';
          Data.QTemp.ParamByName('xIDAnagrafica').asInteger:=ElencoDipForm.TAnagDipID.Value;
          Data.QTemp.ParamByName('xIDCliente').asInteger:=Data2.TEBCClientiID.Value;
          Data.QTemp.ExecSQL;
          QDitteAgenti.Close;
          QDitteAgenti.Open;
     end;
     ElencoDipForm.Free;
end;

procedure TFrmSUITEXClientiExt.BEliminaAssociazClick(Sender: TObject);
begin
     if QDitteAgenti.IsEmpty then exit;
     if MessageDlg('Sei sicuro di voler eliminare l''associazione selezionata ?',mtWarning, [mbNo,mbYes],0)=mrNo then exit;
     Data.QTemp.Close;
     Data.QTemp.SQL.text:='delete from SUITEX_AnagVarie where IDAnagrafica=:xIDAnagrafica '+
          'and IDCliente=:xIDCliente';
     Data.QTemp.ParamByName('xIDAnagrafica').asInteger:=QDitteAgentiID.Value;
     Data.QTemp.ParamByName('xIDCliente').asInteger:=Data2.TEBCClientiID.Value;
     Data.QTemp.ExecSQL;
     QDitteAgenti.Close;
     QDitteAgenti.Open;
end;

end.

