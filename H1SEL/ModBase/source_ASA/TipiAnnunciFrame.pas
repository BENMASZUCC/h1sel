unit TipiAnnunciFrame;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     TB97,Grids,DBGrids,RXDBCtrl,Db,DBTables,ExtCtrls;

type
     TFrmTipiAnnunci=class(TFrame)
          Panel1: TPanel;
          QTipiAnnunci: TQuery;
          DsQTipiAnnunci: TDataSource;
          RxDBGrid1: TRxDBGrid;
          ToolbarButton971: TToolbarButton97;
          ToolbarButton972: TToolbarButton97;
          ToolbarButton973: TToolbarButton97;
          QTipiAnnunciID: TAutoIncField;
          QTipiAnnunciTipoAnnuncio: TStringField;
          QTipiAnnunciNote: TStringField;
          procedure ToolbarButton971Click(Sender: TObject);
          procedure ToolbarButton972Click(Sender: TObject);
          procedure ToolbarButton973Click(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

implementation

uses ModuloDati;

{$R *.DFM}

procedure TFrmTipiAnnunci.ToolbarButton971Click(Sender: TObject);
var xDic: string;
begin
     xDic:='';
     if not InputQuery('Nuovo tipo di annuncio','Tipo:',xDic) then exit;
     with Data do begin
          DB.BeginTrans;
          try
               Q1.Close;
               Q1.SQL.text:='insert into Ann_TipiAnnuncio (TipoAnnuncio) '+
                    'values (:xTipoAnnuncio)';
               Q1.ParamByName('xTipoAnnuncio').asString:=xDic;
               Q1.ExecSQL;
               DB.CommitTrans;
               QTipiAnnunci.Close;
               QTipiAnnunci.Open;
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
          end;
     end;
end;

procedure TFrmTipiAnnunci.ToolbarButton972Click(Sender: TObject);
var xDic: string;
begin
     xDic:=QTipiAnnunciTipoAnnuncio.Value;
     if not InputQuery('Modifica tipo di annuncio','Tipo:',xDic) then exit;
     with Data do begin
          DB.BeginTrans;
          try
               Q1.Close;
               Q1.SQL.text:='update Ann_TipiAnnuncio set TipoAnnuncio=:xTipoAnnuncio '+
                    'where ID='+QTipiAnnunciID.AsString;
               Q1.ParamByName('xTipoAnnuncio').asString:=xDic;
               Q1.ExecSQL;
               DB.CommitTrans;
               QTipiAnnunci.Close;
               QTipiAnnunci.Open;
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
          end;
     end;
end;

procedure TFrmTipiAnnunci.ToolbarButton973Click(Sender: TObject);
begin
     Data.Q1.Close;
     Data.Q1.SQL.text:='select count(*) Tot from Ann_Annunci where IDTipoAnnuncio='+QTipiAnnunciID.asstring;
     Data.Q1.Open;
     if Data.Q1.FieldbyName('Tot').asInteger>0 then begin
          MessageDlg('Ci sono annunci associati a questo tipo - IMPOSSIBILE CONTINUARE',mtError, [mbOK],0);
          Data.Q1.Close;
          Exit;
     end;
     Data.Q1.Close;
     if MessageDlg('Sei sicuro di voler cancellare il ruolo selezionato ?',mtWarning, [mbNo,mbYes],0)=mrNo then exit;
     with Data do begin
          DB.BeginTrans;
          try
               Q1.Close;
               Q1.SQL.text:='delete from Ann_TipiAnnuncio '+
                    'where ID='+QTipiAnnunciID.AsString;
               Q1.ExecSQL;
               DB.CommitTrans;
               QTipiAnnunci.Close;
               QTipiAnnunci.Open;
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
          end;
     end;

end;

end.

