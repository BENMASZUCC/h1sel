object EmailMessageForm: TEmailMessageForm
  Left = 214
  Top = 103
  BorderStyle = bsDialog
  Caption = 'Messaggio E-mail'
  ClientHeight = 346
  ClientWidth = 517
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 4
    Top = 299
    Width = 508
    Height = 44
    BevelOuter = bvLowered
    TabOrder = 0
    object BitBtn1: TBitBtn
      Left = 330
      Top = 5
      Width = 85
      Height = 33
      TabOrder = 0
      Kind = bkOK
    end
    object BitBtn2: TBitBtn
      Left = 418
      Top = 5
      Width = 85
      Height = 33
      Caption = 'Annulla'
      TabOrder = 1
      Kind = bkCancel
    end
  end
  object Panel15: TPanel
    Left = 4
    Top = 3
    Width = 510
    Height = 21
    Alignment = taLeftJustify
    Caption = '  Testo del messaggio in fase di invio'
    Color = clGray
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
  end
  object Memo1: TMemo
    Left = 4
    Top = 30
    Width = 509
    Height = 264
    Lines.Strings = (
      'Memo1')
    TabOrder = 2
  end
end
