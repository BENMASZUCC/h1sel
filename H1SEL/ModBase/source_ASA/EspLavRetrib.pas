unit EspLavRetrib;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FrmDatiRetribuz, StdCtrls, Buttons, ExtCtrls;

type
  TEspLavRetribForm = class(TForm)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    DatiRetribuzFrame1: TDatiRetribuzFrame;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  EspLavRetribForm: TEspLavRetribForm;

implementation

{$R *.DFM}

procedure TEspLavRetribForm.FormShow(Sender: TObject);
begin
     DatiRetribuzFrame1.QDatiRetribuz.Open;
     DatiRetribuzFrame1.QTot.Open;
end;

end.
