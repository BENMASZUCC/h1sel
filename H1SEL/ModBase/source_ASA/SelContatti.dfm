object SelContattiForm: TSelContattiForm
  Left = 207
  Top = 106
  BorderStyle = bsDialog
  Caption = 'Seleziona riferimento interno'
  ClientHeight = 165
  ClientWidth = 478
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object BitBtn1: TBitBtn
    Left = 384
    Top = 3
    Width = 92
    Height = 33
    TabOrder = 0
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 384
    Top = 38
    Width = 92
    Height = 33
    Caption = 'Annulla'
    TabOrder = 1
    Kind = bkCancel
  end
  object DBGrid1: TDBGrid
    Left = 3
    Top = 3
    Width = 377
    Height = 158
    DataSource = DsQContatti
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'Contatto'
        Width = 239
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Telefono'
        Width = 104
        Visible = True
      end>
  end
  object QContatti: TQuery
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select ID,Contatto,Telefono'
      'from EBC_ContattiClienti'
      'where IDCliente=:xIDCliente')
    Left = 48
    Top = 32
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'xIDCliente'
        ParamType = ptUnknown
      end>
    object QContattiID: TAutoIncField
      FieldName = 'ID'
    end
    object QContattiContatto: TStringField
      FieldName = 'Contatto'
      FixedChar = True
      Size = 50
    end
    object QContattiTelefono: TStringField
      FieldName = 'Telefono'
      FixedChar = True
      Size = 30
    end
  end
  object DsQContatti: TDataSource
    DataSet = QContatti
    Left = 48
    Top = 64
  end
end
