unit GestStoricoPos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  TB97, StdCtrls, Mask, DBCtrls, ExtCtrls, DB, Grids, DBGrids, DBTables;

type
  TGestStoricoPosForm = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    ToolbarButton971: TToolbarButton97;
    Panel3: TPanel;
    DBGrid1: TDBGrid;
    BEventoIns: TToolbarButton97;
    BEventoDel: TToolbarButton97;
    TStoricoAnagPos: TTable;
    TStoricoAnagPosID: TAutoIncField;
    TStoricoAnagPosIDAnagrafica: TIntegerField;
    TStoricoAnagPosIDOrganigramma: TIntegerField;
    TStoricoAnagPosDallaData: TDateField;
    TStoricoAnagPosAllaData: TDateField;
    TStoricoAnagPosIDAutorizzatoDa: TIntegerField;
    TStoricoAnagPosAnnotazioni: TStringField;
    BEventoMod: TToolbarButton97;
    ToolbarButton972: TToolbarButton97;
    procedure ToolbarButton971Click(Sender: TObject);
    procedure BEventoDelClick(Sender: TObject);
    procedure BEventoInsClick(Sender: TObject);
    procedure BEventoModClick(Sender: TObject);
    procedure ToolbarButton972Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  GestStoricoPosForm: TGestStoricoPosForm;

implementation

uses ModuloDati, InsEvento, ModuloDati2, InsStoricoPos, QRStoricoAnagPos;

{$R *.DFM}

procedure TGestStoricoPosForm.ToolbarButton971Click(Sender: TObject);
begin
     close;
end;

procedure TGestStoricoPosForm.BEventoDelClick(Sender: TObject);
begin
     if not Data2.QStoricoAnagPos.EOF then
        if MessageDlg('Sei sicuro di voler eliminare l''evento selezionato ?',mtWarning,
           [mbNo,mbYes],0)=mrYes then begin
           TStoricoAnagPos.FindKey([Data2.QStoricoAnagPosID.Value]);
           TStoricoAnagPos.Delete;
           Data2.QStoricoAnagPos.Close;
           Data2.QStoricoAnagPos.Open;
        end;
end;

procedure TGestStoricoPosForm.BEventoInsClick(Sender: TObject);
begin
     InsStoricoPosForm:=TInsStoricoPosForm.create(self);
     InsStoricoPosForm.DEDallaData.Date:=Date;
     InsStoricoPosForm.DEAllaData.Date:=Date;
     InsStoricoPosForm.xIDPosiz:=0;
     InsStoricoPosForm.ShowModal;
     if InsStoricoPosForm.ModalResult=mrOK then begin
        TStoricoAnagPos.Insert;
        TStoricoAnagPosIDAnagrafica.Value:=Data.TAnagraficaID.Value;
        TStoricoAnagPosIDOrganigramma.Value:=InsStoricoPosForm.xIDPosiz;
        TStoricoAnagPosDallaData.Value:=InsStoricoPosForm.DEDallaData.Date;
        TStoricoAnagPosAllaData.Value:=InsStoricoPosForm.DEAllaData.Date;
        if InsStoricoPosForm.TAnag.Active then
           TStoricoAnagPosIDAutorizzatoDa.Value:=InsStoricoPosForm.TAnagID.Value;
        TStoricoAnagPosAnnotazioni.Value:=InsStoricoPosForm.EDesc.Text;
        TStoricoAnagPos.Post;
        Data2.QStoricoAnagPos.Close;
        Data2.QStoricoAnagPos.Open;
     end;
     InsStoricoPosForm.Free;
end;

procedure TGestStoricoPosForm.BEventoModClick(Sender: TObject);
begin
     InsStoricoPosForm:=TInsStoricoPosForm.create(self);
     TStoricoAnagPos.FindKey([Data2.QStoricoAnagPosID.Value]);
     InsStoricoPosForm.DEDallaData.Date:=TStoricoAnagPosDallaData.Value;
     InsStoricoPosForm.DEAllaData.Date:=TStoricoAnagPosAllaData.Value;
     InsStoricoPosForm.EDesc.Text:=TStoricoAnagPosAnnotazioni.Value;
     if TStoricoAnagPosIDAutorizzatoDa.Value<>null then begin
        InsStoricoPosForm.TAnag.Open;
        InsStoricoPosForm.TAnag.FindKey([TStoricoAnagPosIDAutorizzatoDa.Value]);
     end;
     InsStoricoPosForm.EPosiz.Text:=Data2.QStoricoAnagPosDescrizPosiz.Value;
     InsStoricoPosForm.xIDPosiz:=Data2.QStoricoAnagPosIDOrganigramma.Value;
     InsStoricoPosForm.ShowModal;
     if InsStoricoPosForm.ModalResult=mrOK then begin
        TStoricoAnagPos.Edit;
        TStoricoAnagPosIDOrganigramma.Value:=InsStoricoPosForm.xIDPosiz;
        TStoricoAnagPosDallaData.Value:=InsStoricoPosForm.DEDallaData.Date;
        TStoricoAnagPosAllaData.Value:=InsStoricoPosForm.DEAllaData.Date;
        if InsStoricoPosForm.TAnag.Active then
           TStoricoAnagPosIDAutorizzatoDa.Value:=InsStoricoPosForm.TAnagID.Value;
        TStoricoAnagPosAnnotazioni.Value:=InsStoricoPosForm.EDesc.Text;
        TStoricoAnagPos.Post;
        Data2.QStoricoAnagPos.Close;
        Data2.QStoricoAnagPos.Open;
     end;
     InsStoricoPosForm.Free;
end;

procedure TGestStoricoPosForm.ToolbarButton972Click(Sender: TObject);
begin
     QRStoricoAnagPosForm:=TQRStoricoAnagPosForm.create(self);
     try QRStoricoAnagPosForm.QuickRep1.Preview
     finally QRStoricoAnagPosForm.Free
     end;
end;

end.
