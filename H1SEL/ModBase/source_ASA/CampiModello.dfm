object CampiModelloForm: TCampiModelloForm
  Left = 429
  Top = 372
  BorderStyle = bsDialog
  Caption = 'Campi modello Word'
  ClientHeight = 78
  ClientWidth = 421
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 3
    Top = 40
    Width = 143
    Height = 13
    Caption = 'Campo da inserire nel modello:'
  end
  object Label2: TLabel
    Left = 3
    Top = 1
    Width = 100
    Height = 13
    Caption = 'Campo del database:'
  end
  object Label3: TLabel
    Left = 148
    Top = 49
    Width = 164
    Height = 28
    Caption = 
      'si consiglia di anteporre un '#13#10'carattere particolare (es. #,@,,.' +
      '..)'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsItalic]
    ParentFont = False
  end
  object BitBtn1: TBitBtn
    Left = 326
    Top = 2
    Width = 93
    Height = 36
    TabOrder = 2
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 326
    Top = 40
    Width = 93
    Height = 36
    Caption = 'Annulla'
    TabOrder = 3
    Kind = bkCancel
  end
  object CBCampo: TComboBox
    Left = 3
    Top = 16
    Width = 310
    Height = 21
    ItemHeight = 13
    TabOrder = 1
    OnChange = CBCampoChange
    Items.Strings = (
      'uno|desc'
      'due|desc')
  end
  object EDescWord: TEdit
    Left = 3
    Top = 55
    Width = 141
    Height = 21
    TabOrder = 0
  end
end
