object LegendaRicForm: TLegendaRicForm
  Left = 312
  Top = 150
  BorderStyle = bsDialog
  Caption = 'Legenda e parametri'
  ClientHeight = 294
  ClientWidth = 458
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object BitBtn1: TBitBtn
    Left = 368
    Top = 5
    Width = 86
    Height = 33
    Caption = 'Chiudi'
    TabOrder = 0
    Kind = bkOK
  end
  object Panel1: TPanel
    Left = 4
    Top = 5
    Width = 357
    Height = 41
    BevelInner = bvSpace
    BevelOuter = bvLowered
    TabOrder = 1
    object Label6: TLabel
      Left = 38
      Top = 8
      Width = 191
      Height = 26
      Caption = 
        'n� di giorni dall'#39'ultima data di inserimento in una ricerca qual' +
        'siasi'
      WordWrap = True
    end
    object Label1: TLabel
      Left = 8
      Top = 8
      Width = 16
      Height = 16
      Caption = 'Di'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 237
      Top = 8
      Width = 52
      Height = 26
      Caption = 'rosso se meno di'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object RxSpinEdit1: TRxSpinEdit
      Left = 299
      Top = 11
      Width = 50
      Height = 21
      TabOrder = 0
    end
  end
  object Panel2: TPanel
    Left = 4
    Top = 50
    Width = 357
    Height = 41
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 2
    object Label7: TLabel
      Left = 38
      Top = 8
      Width = 111
      Height = 26
      Caption = 'n� di giorni dall'#39'ultimo contatto con il soggetto'
      WordWrap = True
    end
    object Label2: TLabel
      Left = 7
      Top = 8
      Width = 20
      Height = 16
      Caption = 'Uc'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 237
      Top = 8
      Width = 52
      Height = 26
      Caption = 'rosso se meno di'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object RxSpinEdit2: TRxSpinEdit
      Left = 299
      Top = 11
      Width = 50
      Height = 21
      TabOrder = 0
    end
  end
  object Panel3: TPanel
    Left = 4
    Top = 95
    Width = 357
    Height = 41
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 3
    object Label5: TLabel
      Left = 38
      Top = 8
      Width = 78
      Height = 26
      Caption = 'colloquio fissato nel futuro'
      WordWrap = True
    end
    object Label8: TLabel
      Left = 7
      Top = 8
      Width = 18
      Height = 16
      Caption = 'Fc'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label9: TLabel
      Left = 237
      Top = 8
      Width = 52
      Height = 26
      Caption = 'giallo se fissato'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clYellow
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
  end
  object Panel4: TPanel
    Left = 4
    Top = 139
    Width = 357
    Height = 41
    BevelInner = bvSpace
    BevelOuter = bvLowered
    TabOrder = 4
    object Label10: TLabel
      Left = 38
      Top = 8
      Width = 156
      Height = 26
      Caption = 'n� di giorni dall'#39'ultimo colloquio in una ricerca qualsiasi'
      WordWrap = True
    end
    object Label11: TLabel
      Left = 8
      Top = 8
      Width = 24
      Height = 16
      Caption = 'Col'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label12: TLabel
      Left = 237
      Top = 8
      Width = 63
      Height = 26
      Caption = 'azzurro se meno di'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clAqua
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object RxSpinEdit3: TRxSpinEdit
      Left = 299
      Top = 11
      Width = 50
      Height = 21
      TabOrder = 0
    end
  end
  object Panel5: TPanel
    Left = 4
    Top = 184
    Width = 208
    Height = 106
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 5
    object Label13: TLabel
      Left = 8
      Top = 8
      Width = 193
      Height = 13
      Caption = 'Colorazione carattere Numero CV:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label14: TLabel
      Left = 8
      Top = 24
      Width = 131
      Height = 13
      Caption = 'verde: almeno 30 gg. di vita'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label15: TLabel
      Left = 8
      Top = 40
      Width = 151
      Height = 13
      Caption = 'arancione: almeno 60 gg. di vita'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 33023
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label16: TLabel
      Left = 8
      Top = 56
      Width = 128
      Height = 13
      Caption = 'giallo: almeno 90 gg. di vita'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clYellow
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label17: TLabel
      Left = 8
      Top = 72
      Width = 135
      Height = 13
      Caption = 'rosso: almeno 120 gg. di vita'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label18: TLabel
      Left = 8
      Top = 87
      Width = 148
      Height = 13
      Caption = 'marrone: almeno 180 gg. di vita'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
  end
  object Q: TQuery
    DatabaseName = 'EBCDB'
    Left = 392
    Top = 72
  end
end
