object NazioneForm: TNazioneForm
  Left = 255
  Top = 105
  ActiveControl = EAbbrev
  BorderStyle = bsDialog
  Caption = 'Nazione'
  ClientHeight = 101
  ClientWidth = 398
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 20
    Top = 7
    Width = 70
    Height = 13
    Caption = 'Abbreviazione:'
  end
  object Label2: TLabel
    Left = 35
    Top = 31
    Width = 55
    Height = 13
    Caption = 'Descrizione'
  end
  object Label3: TLabel
    Left = 15
    Top = 55
    Width = 75
    Height = 13
    Caption = 'Area geografica'
  end
  object Label4: TLabel
    Left = 9
    Top = 79
    Width = 81
    Height = 13
    Caption = 'Descr.nazionalitÓ'
  end
  object BitBtn1: TBitBtn
    Left = 305
    Top = 3
    Width = 91
    Height = 35
    TabOrder = 4
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 305
    Top = 40
    Width = 91
    Height = 35
    Caption = 'Annulla'
    TabOrder = 5
    Kind = bkCancel
  end
  object EAbbrev: TEdit
    Left = 94
    Top = 4
    Width = 41
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 3
    TabOrder = 0
  end
  object EDesc: TEdit
    Left = 94
    Top = 28
    Width = 197
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 30
    TabOrder = 1
  end
  object EArea: TEdit
    Left = 94
    Top = 52
    Width = 169
    Height = 21
    MaxLength = 20
    TabOrder = 2
  end
  object EDescNaz: TEdit
    Left = 94
    Top = 76
    Width = 197
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 30
    TabOrder = 3
  end
end
