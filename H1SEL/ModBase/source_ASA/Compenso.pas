unit Compenso;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, FloatEdit, Buttons, Mask, DBCtrls, ExtCtrls, DtEdit97,
  ToolEdit, CurrEdit;

type
  TCompensoForm = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    CBTipo: TComboBox;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    ENote: TEdit;
    DEDataFatt: TDateEdit97;
    Label8: TLabel;
    RxImporto: TRxCalcEdit;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  CompensoForm: TCompensoForm;

implementation

uses ModuloDati2;

{$R *.DFM}

procedure TCompensoForm.FormShow(Sender: TObject);
begin
     Caption:='[M/122] - '+caption;
end;

end.
