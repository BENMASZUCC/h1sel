unit CVdatiEss;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Grids, Spin, Db, DBTables;

type
  TCVdatiEssForm = class(TForm)
    SG1: TStringGrid;
    BitBtn1: TBitBtn;
    TAnagMansABS: TTable;
    TAnagMansABSID: TAutoIncField;
    TAnagMansABSIDAnagrafica: TIntegerField;
    TAnagMansABSIDMansione: TIntegerField;
    TAnagMansABSNote: TMemoField;
    TAnagMansABSAnniEsperienza: TSmallintField;
    TAnagMansABSDisponibile: TBooleanField;
    TMansioniLK: TTable;
    TMansioniLKID: TAutoIncField;
    TMansioniLKDescrizione: TStringField;
    TAnagMansABSMansione: TStringField;
    procedure SG1SelectCell(Sender: TObject; Col, Row: Integer;
      var CanSelect: Boolean);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    xIDDiploma,xIDDiploma2:integer;
    xIDRuolo,xIDTipoStato:integer;
    xLingua,xLingua2,xLingua3,xLingua4:string;
    Procedure InsCompetenze;
  end;

var
  CVdatiEssForm: TCVdatiEssForm;

implementation

uses SelDiploma, SelLingua, InsRuolo, CompInserite, ModuloDati2, ModuloDati;

{$R *.DFM}

procedure TCVdatiEssForm.SG1SelectCell(Sender: TObject; Col, Row: Integer;
  var CanSelect: Boolean);
begin
     if (col=1) and (row=8) then begin
        SelDiplomaForm:=TSelDiplomaForm.create(self);
        SelDiplomaForm.ShowModal;
        if SelDiplomaForm.ModalResult=mrOK then begin
           Sg1.Cells[1,8]:=SelDiplomaForm.DiplomiDescrizione.asString;
           xIDDiploma:=SelDiplomaForm.DiplomiID.Value;
        end;
        SelDiplomaForm.Free;
     end;
     if (col=1) and (row=9) then begin
        SelDiplomaForm:=TSelDiplomaForm.create(self);
        SelDiplomaForm.ShowModal;
        if SelDiplomaForm.ModalResult=mrOK then begin
           Sg1.Cells[1,9]:=SelDiplomaForm.DiplomiDescrizione.asString;
           xIDDiploma2:=SelDiplomaForm.DiplomiID.Value;
        end;
        SelDiplomaForm.Free;
     end;

     if (col=1) and (row=10) then begin
        InsRuoloForm:=TInsRuoloForm.create(self);
        InsRuoloForm.CBInsComp.Visible:=False;
        InsRuoloForm.ShowModal;
        if InsRuoloForm.ModalResult=mrOK then begin
           Sg1.Cells[1,10]:=InsRuoloForm.TRuoliDescrizione.asString;
           xIDRuolo:=InsRuoloForm.TRuoliID.Value;
        end;
        InsRuoloForm.Free;
     end;

     if (col=1) and (row=15) then begin
        SelLinguaForm:=TSelLinguaForm.create(self);
        SelLinguaForm.ShowModal;
        if SelLinguaForm.ModalResult=mrOK then begin
           Sg1.Cells[1,15]:=SelLinguaForm.LingueLingua.asString;
           xLingua:=SelLinguaForm.LingueLingua.asString;
        end;
        SelLinguaForm.Free;
     end;

     if (col=1) and (row=16) then begin
        SelLinguaForm:=TSelLinguaForm.create(self);
        SelLinguaForm.ShowModal;
        if SelLinguaForm.ModalResult=mrOK then begin
           Sg1.Cells[1,16]:=SelLinguaForm.LingueLingua.asString;
           xLingua2:=SelLinguaForm.LingueLingua.asString;
        end;
        SelLinguaForm.Free;
     end;

     if (col=1) and (row=17) then begin
        SelLinguaForm:=TSelLinguaForm.create(self);
        SelLinguaForm.ShowModal;
        if SelLinguaForm.ModalResult=mrOK then begin
           Sg1.Cells[1,17]:=SelLinguaForm.LingueLingua.asString;
           xLingua3:=SelLinguaForm.LingueLingua.asString;
        end;
        SelLinguaForm.Free;
     end;

     if (col=1) and (row=18) then begin
        SelLinguaForm:=TSelLinguaForm.create(self);
        SelLinguaForm.ShowModal;
        if SelLinguaForm.ModalResult=mrOK then begin
           Sg1.Cells[1,18]:=SelLinguaForm.LingueLingua.asString;
           xLingua4:=SelLinguaForm.LingueLingua.asString;
        end;
        SelLinguaForm.Free;
     end;

end;

procedure TCVdatiEssForm.FormShow(Sender: TObject);
var c,r:integer;
begin
    xIDDiploma:=0;
    xIDDiploma2:=0;
    xIDRuolo:=0;
    xLingua:='';
    xLingua2:='';
    xLingua3:='';
    xLingua4:='';

     SG1.ColWidths[0]:=120;
     SG1.ColWidths[1]:=290;
     SG1.Cells[0,0]:='Campo';
     SG1.Cells[1,0]:='Valore';

     SG1.Cells[0,1]:='Numero CV';
     SG1.Cells[0,2]:='Cognome';
     SG1.Cells[0,3]:='Nome';
     SG1.Cells[0,4]:='Data di Nascita';
     SG1.Cells[0,5]:='Sesso (M/F)';
     SG1.Cells[0,6]:='Comune di residenza';
     SG1.Cells[0,7]:='Provincia di residenza';
     SG1.Cells[0,8]:='Titolo di studio';
     SG1.Cells[0,9]:='Titolo di studio (2)';
     SG1.Cells[0,10]:='Ruolo (attribuito)';
     SG1.Cells[0,11]:='Attuale azienda';
     SG1.Cells[0,12]:='Ultima azienda';
     SG1.Cells[0,13]:='Penultima azienda';
     SG1.Cells[0,14]:='Terzultima azienda';
     SG1.Cells[0,15]:='Lingua conosciuta';
     SG1.Cells[0,16]:='Lingua conosciuta (2)';
     SG1.Cells[0,17]:='Lingua conosciuta (3)';
     SG1.Cells[0,18]:='Lingua conosciuta (4)';
{     SG1.Cells[0,11]:='* Professionalit�';
     SG1.Cells[0,12]:='* Stipendio attuale';
     SG1.Cells[0,18]:='* Liv.conoscenza informatica';
}
     for r:=0 to 19 do
         SG1.RowHeights[r]:=18;
end;

procedure TCVdatiEssForm.InsCompetenze;
var xIDAnag,xIDMans,xTotComp,i:integer;
    xComp:array[1..50] of integer;
begin
     xIDAnag:=TAnagMansABSIDAnagrafica.Value;
     xIDMans:=TAnagMansABSIDMansione.Value;
     CompInseriteForm:=TCompInseriteForm.create(self);
     CompInseriteForm.ERuolo.Text:=TAnagMansABSMansione.Value;
     Data2.QCompRuolo.Close;
     Data2.QCompRuolo.Prepare;
     Data2.QCompRuolo.Params[0].asInteger:=xIDMans;
     Data2.QCompRuolo.Open;
     Data2.QCompRuolo.First;
     xTotComp:=0;
     Data2.TAnagCompIDX.Open;
     CompInseriteForm.SG1.RowCount:=2;
     CompInseriteForm.SG1.RowHeights[0]:=16;
      while not Data2.QCompRuolo.EOF do begin
            if not Data2.TAnagCompIDX.FindKey([xIDAnag,Data2.QCompRuoloIDCompetenza.Value]) then begin
               CompInseriteForm.SG1.RowHeights[xTotComp+1]:=16;
               CompInseriteForm.SG1.Cells[0,xTotComp+1]:=Data2.QCompRuoloDescCompetenza.Value;
               CompInseriteForm.SG1.Cells[1,xTotComp+1]:='0';
               if xIDTipoStato<>2 then begin
                  CompInseriteForm.SG1.Cells[2,xTotComp+1]:='�';
                  CompInseriteForm.SG1.ColorRow[xTotComp+1]:=clLime;
               end else begin
                  CompInseriteForm.SG1.Cells[2,xTotComp+1]:='x';
                  CompInseriteForm.SG1.ColorRow[xTotComp+1]:=clRed;
               end;
               xComp[xTotComp+1]:=Data2.QCompRuoloIDCompetenza.Value;
               Inc(xTotComp);
               CompInseriteForm.SG1.RowCount:=CompInseriteForm.SG1.RowCount+1;
            end;
            Data2.QCompRuolo.Next;
       end;
     CompInseriteForm.SG1.RowCount:=CompInseriteForm.SG1.RowCount-1;
     if xTotComp>0 then CompInseriteForm.ShowModal;
        if CompInseriteForm.ModalResult=mrOK then begin
           for i:=1 to CompInseriteForm.SG1.RowCount-1 do begin
               if CompInseriteForm.SG1.ColorRow[i]=clLime then begin
                  Data2.TAnagCompIDX.InsertRecord([xIDAnag,xComp[i],StrToInt(CompInseriteForm.SG1.Cells[1,i])]);
                  Data2.TStoricoCompABS.OPen;
                  Data2.TStoricoCompABS.InsertRecord([xComp[i],xIDAnag,Date,'valore iniziale',null,StrToInt(CompInseriteForm.SG1.Cells[1,i])]);
                  Data2.TStoricoCompABS.Close;
               end;
           end;
        end;
     Data2.TAnagCompIDX.Close;
     CompInseriteForm.Free;
     if Data2.TCompDipendente.Active then Data2.TCompDipendente.Refresh;
end;

end.
