unit InfoEdizione;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, StdCtrls, Mask, DBCtrls, ComCtrls;

type
  TInfoEdizioneForm = class(TForm)
    PageControl4: TPageControl;
    TSEdizCosti: TTabSheet;
    GroupBox3: TGroupBox;
    Label23: TLabel;
    Label24: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    DBEdit5: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    GroupBox4: TGroupBox;
    Label29: TLabel;
    Label30: TLabel;
    Label34: TLabel;
    Label36: TLabel;
    Label37: TLabel;
    DBEdit25: TDBEdit;
    DBEdit26: TDBEdit;
    DBEdit27: TDBEdit;
    DBEdit28: TDBEdit;
    DBEdit29: TDBEdit;
    DBEdit30: TDBEdit;
    TSEdizPenetr: TTabSheet;
    Label54: TLabel;
    Label55: TLabel;
    GroupBox5: TGroupBox;
    Label39: TLabel;
    Label52: TLabel;
    Label53: TLabel;
    DBEdit31: TDBEdit;
    DBEdit34: TDBEdit;
    DBEdit35: TDBEdit;
    DBEdit36: TDBEdit;
    TSEdizSconti: TTabSheet;
    DBGrid26: TDBGrid;
    TSEdizGiorni: TTabSheet;
    DBGrid27: TDBGrid;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  InfoEdizioneForm: TInfoEdizioneForm;

implementation

uses ModuloDati4;

{$R *.DFM}


end.
