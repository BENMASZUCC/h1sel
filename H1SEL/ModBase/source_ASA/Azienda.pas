unit Azienda;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     Grids,DBGrids,Db,DBTables,StdCtrls,Buttons,ExtCtrls,FloatEdit,Mask,
     ToolEdit,CurrEdit, dxCntner, dxEditor, dxExEdtr, dxEdLib;

type
     TAziendaForm=class(TForm)
          Label1: TLabel;
          EDescrizione: TEdit;
          BitBtn1: TBitBtn;
          BitBtn2: TBitBtn;
          Label2: TLabel;
          Label3: TLabel;
          ECap: TEdit;
          EComune: TEdit;
          Label5: TLabel;
          EProv: TEdit;
          TSettori: TTable;
          DsSettori: TDataSource;
          TSettoriID: TAutoIncField;
          TSettoriAttivita: TStringField;
          DBGrid1: TDBGrid;
          RG1: TRadioGroup;
          GroupBox1: TGroupBox;
          Label6: TLabel;
          Label7: TLabel;
          FENumDip: TFloatEdit;
          SpeedButton1: TSpeedButton;
          EIndirizzo: TEdit;
          CBTipoStrada: TComboBox;
          ENumCivico: TEdit;
          Label8: TLabel;
          ENazione: TEdit;
          BNaz: TSpeedButton;
          FEFatturato: TRxCalcEdit;
          Q: TQuery;
    ETelefono: TEdit;
    EFax: TEdit;
    Label4: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    ESitoInternet: TdxHyperLinkEdit;
    Label11: TLabel;
    EDescAttAzienda: TMemo;
          procedure DBGrid1KeyPress(Sender: TObject; var Key: Char);
          procedure SpeedButton1Click(Sender: TObject);
          procedure CBTipoStradaDropDown(Sender: TObject);
          procedure BNazClick(Sender: TObject);
          procedure FormShow(Sender: TObject);
     private
          { Private declarations }
     public
          xUltimaProv:string;
     end;

var
     AziendaForm: TAziendaForm;

implementation

uses Comuni,SelNazione;

{$R *.DFM}

procedure TAziendaForm.DBGrid1KeyPress(Sender: TObject; var Key: Char);
begin
     if key=chr(13) then BitBtn1.Click;
end;

procedure TAziendaForm.SpeedButton1Click(Sender: TObject);
begin
     ComuniForm:=TComuniForm.create(self);
     ComuniForm.EProv.Text:=xUltimaProv;
     ComuniForm.ShowModal;
     if ComuniForm.ModalResult=mrOK then begin
          ECap.Text:=ComuniForm.QComuniCap.Value;
          EComune.Text:=ComuniForm.QComuniDescrizione.Value;
          EProv.text:=ComuniForm.QComuniProv.Value;
     end else begin
          ECap.Text:='';
          EComune.Text:='';
          EProv.text:='';
     end;
     ComuniForm.Free;
end;

procedure TAziendaForm.CBTipoStradaDropDown(Sender: TObject);
var xNazioneAzienda: string;
begin
     // tipo di strada in funzione della Nazione - RESIDENZA
     Q.Close;
     Q.SQL.text:='select NazioneAbbAzienda from Global';
     Q.Open;
     xNazioneAzienda:=Q.FieldByName('NazioneAbbAzienda').asString;
     Q.Close;
     Q.SQL.text:='select TipoStrada from TipiStrade where NazioneAbb=:xNazioneAbb';
     if ENazione.text<>'' then
          Q.ParamByName('xNazioneAbb').asString:=ENazione.text
     else Q.ParamByName('xNazioneAbb').asString:=xNazioneAzienda;
     Q.Open;
     CBTipoStrada.Items.Clear;
     while not Q.EOF do begin
          CBTipoStrada.Items.Add(Q.FieldByName('TipoStrada').asString);
          Q.next;
     end;
     Q.Close;
end;

procedure TAziendaForm.BNazClick(Sender: TObject);
begin
     SelNazioneForm:=TSelNazioneForm.create(self);
     SelNazioneForm.ShowModal;
     if (SelNazioneForm.ModalResult=mrOK)and(SelNazioneForm.QNazioni.RecordCount>0) then begin
          ENazione.text:=SelNazioneForm.QNazioniAbbrev.Value;
     end else begin
          ENazione.text:='';
     end;
     SelNazioneForm.Free;
end;

procedure TAziendaForm.FormShow(Sender: TObject);
begin
     Caption:='[S/15] - '+Caption;
end;

end.

