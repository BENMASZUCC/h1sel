object StoricoAnagPosForm: TStoricoAnagPosForm
  Left = 266
  Top = 135
  BorderStyle = bsDialog
  Caption = 'Storico posizione organigramma'
  ClientHeight = 344
  ClientWidth = 724
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inline StoricoAnagPosFrame1: TStoricoAnagPosFrame
    Width = 724
    Height = 302
    Align = alClient
    inherited PanButtons: TPanel
      Width = 724
    end
    inherited RxDBGrid1: TRxDBGrid
      Width = 724
      Height = 263
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 302
    Width = 724
    Height = 42
    Align = alBottom
    BevelOuter = bvLowered
    TabOrder = 1
    object BitBtn1: TBitBtn
      Left = 631
      Top = 4
      Width = 88
      Height = 34
      Caption = 'Esci'
      TabOrder = 0
      OnClick = BitBtn1Click
      Kind = bkOK
    end
  end
end
