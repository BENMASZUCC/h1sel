object SelTabGenForm: TSelTabGenForm
  Left = 322
  Top = 121
  BorderStyle = bsDialog
  Caption = 'Selezione da tabella'
  ClientHeight = 268
  ClientWidth = 323
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object BitBtn1: TBitBtn
    Left = 136
    Top = 2
    Width = 92
    Height = 35
    TabOrder = 0
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 230
    Top = 2
    Width = 92
    Height = 35
    Caption = 'Annulla'
    TabOrder = 1
    Kind = bkCancel
  end
  inline FrameDB1: TFrameDB
    Left = 2
    Top = 40
    Width = 320
    Height = 225
    TabOrder = 2
    inherited DBGrid1: TDBGrid
      Width = 246
      Height = 225
    end
    inherited PanelRight: TPanel
      Left = 246
      Height = 225
    end
    inherited DataSourceGrid: TDataSource
      DataSet = QTab
    end
  end
  object QTab: TQuery
    AfterOpen = QTabAfterOpen
    DatabaseName = 'EBCDB'
    RequestLive = True
    SQL.Strings = (
      'select * from Features')
    Left = 24
    Top = 8
  end
end
