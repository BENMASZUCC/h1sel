object ElencoSediForm: TElencoSediForm
  Left = 243
  Top = 103
  BorderStyle = bsDialog
  Caption = 'Elenco sedi operative'
  ClientHeight = 158
  ClientWidth = 494
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object BitBtn1: TBitBtn
    Left = 400
    Top = 4
    Width = 91
    Height = 35
    TabOrder = 0
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 400
    Top = 41
    Width = 91
    Height = 35
    Caption = 'Annulla'
    TabOrder = 1
    Kind = bkCancel
  end
  object DBGrid1: TDBGrid
    Left = 3
    Top = 4
    Width = 392
    Height = 149
    DataSource = DsQSedi
    ReadOnly = True
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'Descrizione'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Comune'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Provincia'
        Visible = True
      end>
  end
  object QSedi: TQuery
    Active = True
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select * from Aziende')
    Left = 32
    Top = 40
    object QSediID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.Aziende.ID'
    end
    object QSediDescrizione: TStringField
      FieldName = 'Descrizione'
      Origin = 'EBCDB.Aziende.Descrizione'
      FixedChar = True
      Size = 30
    end
    object QSediIndirizzo: TStringField
      FieldName = 'Indirizzo'
      Origin = 'EBCDB.Aziende.Indirizzo'
      FixedChar = True
      Size = 40
    end
    object QSediCap: TStringField
      FieldName = 'Cap'
      Origin = 'EBCDB.Aziende.Cap'
      FixedChar = True
      Size = 5
    end
    object QSediComune: TStringField
      FieldName = 'Comune'
      Origin = 'EBCDB.Aziende.Comune'
      FixedChar = True
    end
    object QSediProvincia: TStringField
      FieldName = 'Provincia'
      Origin = 'EBCDB.Aziende.Provincia'
      FixedChar = True
      Size = 2
    end
  end
  object DsQSedi: TDataSource
    DataSet = QSedi
    Left = 32
    Top = 80
  end
end
