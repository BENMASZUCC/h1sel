unit EsitoColloquio;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, StdCtrls, Mask, DBCtrls, Buttons, ExtCtrls, ComCtrls, DtEdit97,
  DtEdDB97, Grids, DBGrids, DBTables, Spin;

type
  TEsitoColloquioForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    DsSelezionatori: TDataSource;
    DBGrid1: TDBGrid;
    Label4: TLabel;
    Panel2: TPanel;
    Label6: TLabel;
    CBEsito: TComboBox;
    DataColloquio: TDateEdit97;
    CBGiudizio: TComboBox;
    SEPunteggio: TSpinEdit;
    ECogn: TEdit;
    ENome: TEdit;
    TSelezionatori: TTable;
    TSelezionatoriID: TAutoIncField;
    TSelezionatoriNominativo: TStringField;
    BitBtn3: TBitBtn;
    procedure BitBtn3Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    xIDAnag:integer;
  end;

var
  EsitoColloquioForm: TEsitoColloquioForm;

implementation

uses MDRicerche, FissaColloquio, ModuloDati, ValutazColloquio;

{$R *.DFM}

procedure TEsitoColloquioForm.BitBtn3Click(Sender: TObject);
begin
     ValutazColloquioForm:=TValutazColloquioForm.create(self);
     ValutazColloquioForm.xIDAnag:=xIDAnag;
     ValutazColloquioForm.BInfoCand.Visible:=True;
     ValutazColloquioForm.ShowModal;
     if ValutazColloquioForm.ETot.Text<>'' then
        SEPunteggio.Value:=StrToInt(ValutazColloquioForm.ETot.Text);
     ValutazColloquioForm.Free;
end;

procedure TEsitoColloquioForm.FormShow(Sender: TObject);
begin
     Caption:='[E/6] - '+Caption;
end;

end.
