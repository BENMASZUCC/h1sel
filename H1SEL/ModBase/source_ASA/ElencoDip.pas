unit ElencoDip;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Grids, DBGrids, Db, DBTables, ExtCtrls;

type
  TElencoDipForm = class(TForm)
    Panel1: TPanel;
    DsAnagDip: TDataSource;
    DBGrid1: TDBGrid;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Panel2: TPanel;
    Label1: TLabel;
    ECerca: TEdit;
    TAnagDip: TQuery;
    TAnagDipID: TAutoIncField;
    TAnagDipCognome: TStringField;
    TAnagDipNome: TStringField;
    TAnagDipCVNumero: TIntegerField;
    TAnagDipTipoStato: TStringField;
    TAnagDipIDTipoStato: TIntegerField;
    TAnagDipIDProprietaCV: TIntegerField;
    TAnagDipCVIDAnndata: TIntegerField;
    TAnagDipCVInseritoInData: TDateTimeField;
    procedure DBGrid1KeyPress(Sender: TObject; var Key: Char);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure ECercaChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    xStringa:string;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ElencoDipForm: TElencoDipForm;

implementation

uses uUtilsVarie;

{$R *.DFM}

procedure TElencoDipForm.DBGrid1KeyPress(Sender: TObject; var Key: Char);
begin
     if Key=chr(13) then BitBtn1.Click;
end;

procedure TElencoDipForm.DBGrid1DblClick(Sender: TObject);
begin
     BitBtn1.Click;
end;

procedure TElencoDipForm.BitBtn1Click(Sender: TObject);
begin
     if TAnagDip.EOF then begin
        MessageDlg('Nessun soggetto selezionato !',mtError,[mbOK],0);
        ModalResult:=mrNone;
        Abort;
     end;
     if not OkIDAnag(TAnagDipID.Value) then begin
        ModalResult:=mrNone;
        Abort;
     end;
     if TAnagDipIDTipoStato.Value=3 then begin
        if MessageDlg('Il candidato risulta INSERITO in azienda. Continuare?',mtWarning,[mbYes,mbNo],0)=mrNo then begin
           ModalResult:=mrNone;
           Abort;
        end;
     end;
end;

procedure TElencoDipForm.Timer1Timer(Sender: TObject);
begin
     xStringa:='';
end;

procedure TElencoDipForm.ECercaChange(Sender: TObject);
begin
     TAnagDip.Close;
     TAnagDip.ParamByName('xCerca').asString:=ECerca.text+'%';
     TAnagDip.Open;
end;

procedure TElencoDipForm.FormShow(Sender: TObject);
begin
     Caption:='[S/14] - '+Caption;
end;

end.
