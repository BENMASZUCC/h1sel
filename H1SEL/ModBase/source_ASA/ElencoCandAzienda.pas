unit ElencoCandAzienda;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Grids, DBGrids, Db, DBTables, ExtCtrls;

type
  TElencoCandAziendaForm = class(TForm)
    Panel1: TPanel;
    QCandAzienda: TQuery;
    DsQCandAzienda: TDataSource;
    DBGrid1: TDBGrid;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    QCandAziendaID: TIntegerField;
    QCandAziendaCVNumero: TIntegerField;
    QCandAziendaCognome: TStringField;
    QCandAziendaNome: TStringField;
    QCandAziendaAttuale: TBooleanField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ElencoCandAziendaForm: TElencoCandAziendaForm;

implementation

{$R *.DFM}

end.
