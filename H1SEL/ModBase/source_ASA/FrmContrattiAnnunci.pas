unit FrmContrattiAnnunci;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     Db,DBTables,Grids,DBGrids,TB97,Wall,ExtCtrls;

type
     TContrattiAnnunciFrame=class(TFrame)
          Panel93: TPanel;
          Wallpaper34: TWallpaper;
          TbBContrNew: TToolbarButton97;
          TbBContrDel: TToolbarButton97;
          TbBContrMod: TToolbarButton97;
          DBGrid48: TDBGrid;
          QAnnContratti: TQuery;
          QAnnContrattiID: TAutoIncField;
          QAnnContrattiIDEdizione: TIntegerField;
          QAnnContrattiDataStipula: TDateTimeField;
          QAnnContrattiDataScadenza: TDateTimeField;
          QAnnContrattiTotaleModuli: TSmallintField;
          QAnnContrattiModuliUtilizzati: TSmallintField;
          QAnnContrattiCostoSingoloLire: TFloatField;
          QAnnContrattiCostoSingoloEuro: TFloatField;
          QAnnContrattiTestata: TStringField;
          QAnnContrattiEdizione: TStringField;
          DsQAnnContratti: TDataSource;
          procedure TbBContrNewClick(Sender: TObject);
          procedure TbBContrModClick(Sender: TObject);
          procedure TbBContrDelClick(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

implementation

uses ContrattoAnnunci,ModuloDati4,ModuloDati,Main;

{$R *.DFM}

procedure TContrattiAnnunciFrame.TbBContrNewClick(Sender: TObject);
begin
     if not MainForm.CheckProfile('3504') then Exit;
     ContrattoAnnunciForm:=TContrattoAnnunciForm.create(self);
     ContrattoAnnunciForm.DEDataStipula.Date:=Date;
     ContrattoAnnunciForm.ShowModal;
     if ContrattoAnnunciForm.ModalResult=mrOK then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text:='insert into Ann_Contratti (IDEdizione,DataStipula, ';
                    if ContrattoAnnunciForm.DEDataScadenza.Text<>'' then
                         Q1.SQL.Add('DataScadenza,');
                    Q1.SQL.Add('TotaleModuli,ModuliUtilizzati,CostoSingoloLire,CostoSingoloEuro)');
                    Q1.SQL.Add('values (:xIDEdizione,:xDataStipula,');
                    if ContrattoAnnunciForm.DEDataScadenza.Text<>'' then
                         Q1.SQL.Add(':xDataScadenza,');
                    Q1.SQL.Add(':xTotaleModuli,:xModuliUtilizzati,:xCostoSingoloLire,:xCostoSingoloEuro)');
                    Q1.ParamByName('xIDEdizione').asInteger:=ContrattoAnnunciForm.QTestEdizIDEdizione.Value;
                    Q1.ParamByName('xDataStipula').asDateTime:=ContrattoAnnunciForm.DEDataStipula.date;
                    if ContrattoAnnunciForm.DEDataScadenza.Text<>'' then
                         Q1.ParamByName('xDataScadenza').asDateTime:=ContrattoAnnunciForm.DEDataScadenza.date;
                    Q1.ParamByName('xTotaleModuli').asInteger:=ContrattoAnnunciForm.TotModuli.AsInteger;
                    Q1.ParamByName('xModuliUtilizzati').asInteger:=ContrattoAnnunciForm.ModuliUtilizzati.AsInteger;
                    Q1.ParamByName('xCostoSingoloLire').asFloat:=ContrattoAnnunciForm.CostoLire.Value;
                    Q1.ParamByName('xCostoSingoloEuro').asFloat:=ContrattoAnnunciForm.CostoEuro.Value;
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    QAnnContratti.Close;
                    QAnnContratti.open;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               end;
          end;
     end;
     ContrattoAnnunciForm.Free;
end;

procedure TContrattiAnnunciFrame.TbBContrModClick(Sender: TObject);
begin
     if not MainForm.CheckProfile('3505') then Exit;
     ContrattoAnnunciForm:=TContrattoAnnunciForm.create(self);
     while ContrattoAnnunciForm.QTestEdizIDEdizione.Value<>QAnnContrattiIDEdizione.Value do
          ContrattoAnnunciForm.QTestEdiz.Next;
     ContrattoAnnunciForm.DEDataStipula.Date:=QAnnContrattiDataStipula.Value;
     ContrattoAnnunciForm.DEDataScadenza.Date:=QAnnContrattiDataScadenza.Value;
     ContrattoAnnunciForm.TotModuli.Value:=QAnnContrattiTotaleModuli.Value;
     ContrattoAnnunciForm.ModuliUtilizzati.Value:=QAnnContrattiModuliUtilizzati.Value;
     ContrattoAnnunciForm.CostoLire.Value:=QAnnContrattiCostoSingoloLire.Value;
     ContrattoAnnunciForm.CostoEuro.Value:=QAnnContrattiCostoSingoloEuro.Value;
     ContrattoAnnunciForm.ShowModal;
     if ContrattoAnnunciForm.ModalResult=mrOK then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text:='update Ann_Contratti set IDEdizione=:xIDEdizione,DataStipula=:xDataStipula, ';
                    if ContrattoAnnunciForm.DEDataScadenza.Text<>'' then
                         Q1.SQL.Add('DataScadenza=:xDataScadenza,');
                    Q1.SQL.Add('TotaleModuli=:xTotaleModuli,ModuliUtilizzati=:xModuliUtilizzati,'+
                         'CostoSingoloLire=:xCostoSingoloLire,CostoSingoloEuro=:xCostoSingoloEuro');
                    Q1.SQL.Add('where ID='+QAnnContrattiID.AsString);
                    Q1.ParamByName('xIDEdizione').asInteger:=ContrattoAnnunciForm.QTestEdizIDEdizione.Value;
                    Q1.ParamByName('xDataStipula').asDateTime:=ContrattoAnnunciForm.DEDataStipula.date;
                    if ContrattoAnnunciForm.DEDataScadenza.Text<>'' then
                         Q1.ParamByName('xDataScadenza').asDateTime:=ContrattoAnnunciForm.DEDataScadenza.date;
                    Q1.ParamByName('xTotaleModuli').asInteger:=ContrattoAnnunciForm.TotModuli.AsInteger;
                    Q1.ParamByName('xModuliUtilizzati').asInteger:=ContrattoAnnunciForm.ModuliUtilizzati.AsInteger;
                    Q1.ParamByName('xCostoSingoloLire').asFloat:=ContrattoAnnunciForm.CostoLire.Value;
                    Q1.ParamByName('xCostoSingoloEuro').asFloat:=ContrattoAnnunciForm.CostoEuro.Value;
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    QAnnContratti.Close;
                    QAnnContratti.open;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               end;
          end;
     end;
     ContrattoAnnunciForm.Free;
end;

procedure TContrattiAnnunciFrame.TbBContrDelClick(Sender: TObject);
begin
     if not MainForm.CheckProfile('3506') then Exit;
     if QAnnContratti.IsEmpty then exit;
     if MessageDlg('Sei sicuro di voler eliminare il contratto ?',mtWarning, [mbNo,mbYes],0)=mrYes then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text:='delete from Ann_Contratti '+
                         'where ID='+QAnnContrattiID.AsString;
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    QAnnContratti.Close;
                    QAnnContratti.Open;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               end;
          end;
     end;
end;

end.

