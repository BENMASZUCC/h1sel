object LegendaGestRicForm: TLegendaGestRicForm
  Left = 231
  Top = 336
  BorderStyle = bsDialog
  Caption = 'Legenda e parametri'
  ClientHeight = 164
  ClientWidth = 550
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel3: TPanel
    Left = 4
    Top = 5
    Width = 449
    Height = 41
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    object Label5: TLabel
      Left = 94
      Top = 8
      Width = 124
      Height = 26
      Caption = 'data in cui � stato associato a questa ricerca'
      WordWrap = True
    end
    object Label8: TLabel
      Left = 7
      Top = 8
      Width = 64
      Height = 16
      Caption = 'Inserito il'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label9: TLabel
      Left = 249
      Top = 8
      Width = 107
      Height = 26
      Caption = 'sfondo giallo se sono passati pi� di'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clYellow
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object Label1: TLabel
      Left = 418
      Top = 14
      Width = 25
      Height = 13
      Caption = 'giorni'
    end
    object RxSpinEdit1: TRxSpinEdit
      Left = 365
      Top = 11
      Width = 48
      Height = 21
      TabOrder = 0
    end
  end
  object BitBtn1: TBitBtn
    Left = 458
    Top = 5
    Width = 89
    Height = 33
    Caption = 'Chiudi'
    TabOrder = 1
    Kind = bkOK
  end
  object Panel1: TPanel
    Left = 4
    Top = 52
    Width = 449
    Height = 41
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 2
    object Label2: TLabel
      Left = 94
      Top = 8
      Width = 143
      Height = 26
      Caption = 'data in cui � avvenuto l'#39'ultimo contatto per questa ricerca'
      WordWrap = True
    end
    object Label3: TLabel
      Left = 7
      Top = 8
      Width = 82
      Height = 16
      Caption = 'Ultimo cont.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clTeal
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 249
      Top = 8
      Width = 107
      Height = 26
      Caption = 'sfondo giallo se sono passati pi� di'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clYellow
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object Label6: TLabel
      Left = 418
      Top = 14
      Width = 25
      Height = 13
      Caption = 'giorni'
    end
    object RxSpinEdit2: TRxSpinEdit
      Left = 365
      Top = 11
      Width = 48
      Height = 21
      TabOrder = 0
    end
  end
  object GroupBox1: TGroupBox
    Left = 4
    Top = 99
    Width = 349
    Height = 62
    Caption = 'Segnalazioni assenza dati'
    TabOrder = 3
    object Label7: TLabel
      Left = 168
      Top = 19
      Width = 156
      Height = 13
      Caption = 'Numero CV rosso se manca'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object CBMancaCV: TCheckBox
      Left = 7
      Top = 18
      Width = 158
      Height = 17
      Caption = 'manca file CV scannerizzato:'
      TabOrder = 0
    end
    object CBMancaTel: TCheckBox
      Left = 7
      Top = 38
      Width = 170
      Height = 17
      Caption = 'manca rec.telefonici e cellulare:'
      Enabled = False
      TabOrder = 1
    end
  end
  object Q: TQuery
    DatabaseName = 'EBCDB'
    Left = 448
    Top = 56
  end
end
