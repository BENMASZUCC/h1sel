object SceltaAnagFileForm: TSceltaAnagFileForm
  Left = 236
  Top = 164
  BorderStyle = bsDialog
  Caption = 'Apertura file candidato'
  ClientHeight = 220
  ClientWidth = 711
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 4
    Top = 2
    Width = 396
    Height = 26
    Caption = 
      'Per questo soggetto non � stato trovato il file GIF scannerizzat' +
      'o.'#13#10'Scegliere tra gli altri tipi di file a lui associati e preme' +
      're OK, oppure annulla per uscire.'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clPurple
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 4
    Top = 31
    Width = 272
    Height = 13
    Caption = 'L'#39'elenco � in ordine descrescente rispetto alla data del file'
  end
  object BitBtn1: TBitBtn
    Left = 509
    Top = 2
    Width = 98
    Height = 38
    TabOrder = 0
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 608
    Top = 2
    Width = 98
    Height = 38
    Caption = 'Annulla'
    TabOrder = 1
    Kind = bkCancel
  end
  object RxDBGrid1: TRxDBGrid
    Left = 3
    Top = 47
    Width = 704
    Height = 169
    DataSource = DsQAnagFile
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'Tipo'
        Width = 125
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Descrizione'
        Width = 105
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NomeFile'
        Title.Caption = 'Nome file'
        Width = 368
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DataCreazione'
        Title.Caption = 'Data del file'
        Width = 68
        Visible = True
      end>
  end
  object QAnagFile: TQuery
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select TipiFileCand.Tipo,AnagFile.*'
      'from AnagFile,TipiFileCand'
      'where AnagFile.IDTipo *= TipiFileCand.ID'
      'and AnagFile.IDAnagrafica=:xIDAnag'
      'order by DataCreazione desc')
    Left = 56
    Top = 96
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'xIDAnag'
        ParamType = ptUnknown
      end>
    object QAnagFileTipo: TStringField
      FieldName = 'Tipo'
      Origin = 'EBCDB.AnagFile.ID'
      FixedChar = True
      Size = 30
    end
    object QAnagFileID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.AnagFile.IDAnagrafica'
    end
    object QAnagFileIDAnagrafica: TIntegerField
      FieldName = 'IDAnagrafica'
      Origin = 'EBCDB.AnagFile.IDTipo'
    end
    object QAnagFileIDTipo: TIntegerField
      FieldName = 'IDTipo'
      Origin = 'EBCDB.AnagFile.Descrizione'
    end
    object QAnagFileDescrizione: TStringField
      FieldName = 'Descrizione'
      Origin = 'EBCDB.AnagFile.NomeFile'
      FixedChar = True
      Size = 100
    end
    object QAnagFileNomeFile: TStringField
      FieldName = 'NomeFile'
      Origin = 'EBCDB.AnagFile.DataCreazione'
      FixedChar = True
      Size = 200
    end
    object QAnagFileDataCreazione: TDateTimeField
      FieldName = 'DataCreazione'
      Origin = 'EBCDB.TipiFileCand.Tipo'
      DisplayFormat = 'dd/mm/yyyy'
    end
  end
  object DsQAnagFile: TDataSource
    DataSet = QAnagFile
    Left = 56
    Top = 128
  end
end
