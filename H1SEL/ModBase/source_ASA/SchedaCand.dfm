�
 TSCHEDACANDFORM 0�K  TPF0TSchedaCandFormSchedaCandFormLeftTTopnBorderStylebsDialogCaptionScheda candidatoClientHeight�ClientWidthColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnClose	FormCloseOnShowFormShowPixelsPerInch`
TextHeight TPanelPanel1Left Top WidthHeightEAlignalTopTabOrder  TSpeedButtonBValutazLeftYTopWidth\Height$Hintgriglia di valutazioneCaptionValutazione
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 33333333?�������        wwwwwwww�������?��?��� � � �w7sw7w7�������?��?��� � � �w7sw7w7�������?��?��� � � �w7sw7w7�������?��?��� � � �w7sw7w7���������������������wwwwwwww�����Ȁ�wwww�        wwwwwwww33333333333333333333333333333333	NumGlyphsParentShowHintShowHint	OnClickBValutazClick  TPanelPanel2LeftTopWidthTHeightCAlignalLeft
BevelOuterbvNoneEnabledTabOrder  TLabelLabel1Left2TopWidth-HeightCaptionCognomeFocusControl
DBECognome  TLabelLabel2Left� TopWidthHeightCaptionNomeFocusControlDBEdit2  TLabelLabel11LeftTopWidthHeightCaptionCV n�FocusControlDBEdit14  TLabelLabel15LeftTop,WidthHeightCaptionStato:Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TDBEdit
DBECognomeLeft2TopWidth� HeightCharCaseecUpperCaseColorclYellow	DataFieldCognome
DataSourceData.DsAnagraficaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder   TDBEditDBEdit2Left� TopWidthuHeightCharCaseecUpperCaseColorclYellow	DataFieldNome
DataSourceData.DsAnagraficaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBEditDBEdit14LeftTopWidth+HeightColorclAqua	DataFieldCVNumero
DataSourceData.DsAnagraficaTabOrder  TDBEditDBEdit42Left2Top)Width� HeightColor	clBtnFace	DataField	DescStato
DataSourceData.DsAnagraficaFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontReadOnly	TabOrder   TBitBtnBitBtn1Left�TopWidth\Height$TabOrderKindbkOK   TPageControlPageControl2Left TopEWidthHeight�
ActivePageTSAnagNoteIntAlignalClientFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 	MultiLine	
ParentFontTabOrderOnChangePageControl2Change 	TTabSheetTSAnagDatiAnagHelpContext�� CaptionDati personali TPanelPanAnagLeft Top&WidthHeightWHelpContext�� AlignalClient
BevelOuter	bvLoweredEnabledTabOrder  TLabelLabel38LeftgTopWidth5HeightCaptionStato CivileFocusControlDBLookupComboBox1  TLabelLabel100Left� TopWidthGHeightCaptionData di nascita  TLabelLabel101LeftTopWidthNHeightCaptionLuogo di nascitaFocusControlDBEdit65  TLabelLabel35LeftITopWidthHeightCaptionEt�FocusControlDBEdit16  TDBLookupComboBoxDBLookupComboBox1LefteTopWidthTHeight	DataFieldStatoCivile
DataSourceData.DsAnagraficaTabOrder  TDBEditDBEdit65LeftTopWidth� Height	DataFieldLuogoNascita
DataSourceData.DsAnagraficaTabOrder   TDBEditDBEdit16LeftHTopWidthHeightTabStopColor	clBtnFace	DataFieldEta
DataSourceData.DsAnagraficaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder  TDbDateEdit97DbDateEdit971Left� TopWidthnHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder
BevelOuterbvNoneColorCalendar.ColorValidclBlueDate      ^�@DayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanClear
doCanPopup
doIsMaskeddoShowCanceldoShowToday 	DataFieldDataNascita
DataSourceData.DsAnagrafica  	TGroupBox	GroupBox1LeftTop+Width� HeightxCaption	ResidenzaTabOrder TLabelLabel7LeftTop`WidthHeightCaptionCapFocusControlDBEdit7  TLabelLabel8LeftTop5Width'HeightCaptionComuneFocusControlDBEdit8  TLabelLabel9LeftRTop_WidthHeightCaptionProv.FocusControlDBEdit9  TSpeedButtonSpeedButton2Left� Top[Width@HeightCaptioncerca
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333333333333333333333333�3333330 333����w330  0 �337ww7w�33�� � 33�3w�w33 ��� 33ws37�w30����� 3���37�w0  ��� 7wws37�w������ s����7�w0   �� 7wwws7�w333�   333s�www3330 3333337w3333333333333333333333333333333333333333333333333333333333333333333	NumGlyphsOnClickSpeedButton2Click  TLabelLabel6LeftTopWidth5HeightCaptionTipo strada  TLabelLabel93Left?TopWidth&HeightCaption	Indirizzo  TLabelLabel99Left� TopWidthHeightCaptionn� civ.  TDBEditDBEdit7Left"Top\Width*Height	DataFieldCap
DataSourceData.DsAnagraficaTabOrder   TDBEditDBEdit8LeftTopDWidth� Height	DataFieldComune
DataSourceData.DsAnagraficaReadOnly	TabOrder  TDBEditDBEdit9LeftpTop\WidthHeight	DataField	Provincia
DataSourceData.DsAnagraficaReadOnly	TabOrder  TDBEditDBEdit6Left?TopWidthqHeight	DataField	Indirizzo
DataSourceData.DsAnagraficaTabOrder  TDBComboBoxDBComboBox7LeftTopWidth6Height	DataField
TipoStrada
DataSourceData.DsAnagrafica
ItemHeightItems.StringsViaVialePiazzaLargoCorso TabOrder  TDBEditDBEdit54Left� TopWidthHeight	DataField	NumCivico
DataSourceData.DsAnagraficaTabOrder   	TGroupBox	GroupBox2Left� Top+Width� HeightxCaption	DomicilioTabOrder TLabelLabel4LeftTop`WidthHeightCaptionCapFocusControlDBEdit4  TLabelLabel19LeftTop5Width'HeightCaptionComuneFocusControlDBEdit10  TLabelLabel22LeftRTop_WidthHeightCaptionProv.FocusControlDBEdit11  TSpeedButtonSpeedButton4Left� Top[Width@HeightCaptioncerca
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333333333333333333333333�3333330 333����w330  0 �337ww7w�33�� � 33�3w�w33 ��� 33ws37�w30����� 3���37�w0  ��� 7wws37�w������ s����7�w0   �� 7wwws7�w333�   333s�www3330 3333337w3333333333333333333333333333333333333333333333333333333333333333333	NumGlyphsOnClickSpeedButton4Click  TSpeedButtonSpeedButton3Left� Top	Width8HeightCaptioncopiaOnClickSpeedButton3Click  TLabelLabel3LeftTopWidth5HeightCaptionTipo strada  TLabelLabel105Left?TopWidth&HeightCaption	Indirizzo  TDBEditDBEdit4Left"Top\Width*Height	DataFieldDomicilioCap
DataSourceData.DsAnagraficaTabOrder   TDBEditDBEdit10LeftTopDWidth� Height	DataFieldDomicilioComune
DataSourceData.DsAnagraficaReadOnly	TabOrder  TDBEditDBEdit11LeftpTop\WidthHeight	DataFieldDomicilioProvincia
DataSourceData.DsAnagraficaReadOnly	TabOrder  TDBEditDBEdit3Left?TopWidthqHeight	DataFieldDomicilioIndirizzo
DataSourceData.DsAnagraficaTabOrder  TDBComboBoxDBComboBox8LeftTopWidth6Height	DataFieldDomicilioTipoStrada
DataSourceData.DsAnagrafica
ItemHeightItems.StringsViaVialePiazzaLargoCorso TabOrder  TDBEditDBEdit79Left� TopWidthHeight	DataFieldDomicilioNumCivico
DataSourceData.DsAnagraficaTabOrder   TPanelPanel5LeftTop� Width� Heightl
BevelInnerbvRaised
BevelOuter	bvLoweredTabOrder TLabelLabel5LeftTopWidth-HeightCaption	Telefono:FocusControlDBEdit1  TLabelLabel12LeftTop=Width(HeightCaption	CellulareFocusControlDBEdit12  TLabelLabel13LeftTopTWidthHeightCaptionEmailFocusControlDBEdit18  TLabelLabel85LeftTop%Width3HeightCaptionTel.Ufficio:FocusControlDBEdit70  TDBEditDBEdit1LeftBTop	Width� Height	DataFieldRecapitiTelefonici
DataSourceData.DsAnagraficaTabOrder   TDBEditDBEdit12LeftBTop9Width� Height	DataField	Cellulare
DataSourceData.DsAnagraficaTabOrderOnExitDBEdit15Exit  TDBEditDBEdit18LeftBTopQWidth� Height	DataFieldEmail
DataSourceData.DsAnagraficaTabOrder  TDBEditDBEdit70LeftBTop!Width� Height	DataField
TelUfficio
DataSourceData.DsAnagraficaTabOrder   TPanelPanCFLeftTopWidthHeight-
BevelInnerbvRaised
BevelOuter	bvLoweredTabOrder TLabelLabel49LeftTopWidthBHeightCaptionCodice fiscaleFocusControlDBEdit17  TLabelLabPartitaIVALeft� TopWidth2HeightCaptionPartita IVAFocusControlDBEPartitaIVA  TDBEditDBEdit17LeftTopWidthvHeight	DataFieldCodiceFiscale
DataSourceData.DsAnagraficaTabOrder   TDBEditDBEPartitaIVALeft� TopWidthgHeight	DataField
PartitaIVA
DataSourceData.DsAnagrafica	MaxLengthTabOrder   TPanelPanOldCVLeftTopWidthOHeight-
BevelInnerbvRaised
BevelOuter	bvLoweredTabOrderVisible TLabelLabel86LeftTopWidthDHeightCaptionvecchio n� CVFocusControlDBEdit71  TDBEditDBEdit71LeftTopWidthCHeightColorclSilver	DataFieldOldCVNumero
DataSourceData.DsAnagraficaReadOnly	TabOrder     TPanelPanel3Left Top WidthHeight&AlignalTop
BevelOuter	bvLoweredTabOrder TToolbarButton97	BModificaLeftTopWidthPHeight$Captionmodifica dati
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 333     333wwwww333����?���??� 0  � �w�ww?sw7������ws3?33�࿿ ���w�3ws��7�������w�3?��7࿿  �w�3wwss7��������w�?���37�   ��w�wwws?� � �� �ws�w73w730 ���37wss3?�330���  33773�ww33��33s7s730���37�33s3	�� 33ws��w3303   3373wwws3	NumGlyphsWordWrap	OnClickBModificaClick    	TTabSheetTSAnagRicercheCaptionRicerche
ImageIndex TDBGridDBGrid7Left TopWidthHeightAlignalClient
DataSourceData2.DsAnagRicercheReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameProgressivoTitle.CaptionRif.Width2Visible	 Expanded	FieldNameClienteWidth� Visible	 Expanded	FieldNameRuoloWidth� Visible	 Expanded	FieldNameStatoRicTitle.CaptionStatoWidthZVisible	    TPanelPanel12Left Top WidthHeightAlignalTop	AlignmenttaLeftJustifyCaption;  Ricerche nelle quali � attualmente inserito il nominativoColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TPanelPanel126Left Top.WidthHeightOAlignalBottom
BevelOuter	bvLoweredEnabledTabOrder TLabelLabel88LeftTop6Width HeightCaptionCliente  TLabelLabel89Left� Top6WidthJHeightCaptionSelez.principale  TLabelLabel90LeftTopWidthXHeightCaptionData Inizio ricerca:  TLabelLabel10LeftTopWidthEHeightCaptionchiusa in data:  TPanelPanel127LeftTopWidthHeightAlignalTop	AlignmenttaLeftJustifyCaption2  Ulteriori informazioni sulla ricerca selezionataColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   TDBEditDBEdit21Left/Top3Width� Height	DataFieldCliente
DataSourceData2.DsAnagRicercheTabOrder  TDBEditDBEdit22Left8Top3Width� Height	DataFieldUtente
DataSourceData2.DsAnagRicercheTabOrder  TDBEditDBEdit5LefteTopWidthQHeight	DataField
DataInizio
DataSourceData2.DsAnagRicercheTabOrder  TDBEditDBEdit20LefthTopWidthQHeight	DataFieldDataFine
DataSourceData2.DsAnagRicercheTabOrder    	TTabSheet
TSAnagCompHelpContext�� Caption
Competenze TDBGridDBGrid14Left TopWidthHeighthAlignalClient
DataSourceData2.DsCompDipendenteReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameDescCompetenzaTitle.CaptionDescriz.competenzaWidth� Visible	 Expanded	FieldNameAreaWidth� Visible	 Expanded	FieldName	TipologiaTitle.CaptionTipoWidthVisible	 	AlignmenttaCenterExpanded	FieldNameValoreWidth#Visible	    TPanelPanel73Left Top WidthHeightAlignalTop	AlignmenttaLeftJustifyCaption  Competenze posseduteColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   	TTabSheet
TsAnagMansHelpContext�� CaptionRuoli/Caratt. 	TSplitter	Splitter4Left Top� WidthHeightCursorcrVSplitAlignalTop  TPanelPanel75Left Top&WidthHeightAlignalTop	AlignmenttaLeftJustifyCaption:  Ruoli/Mansioni proprie (a prescindere dall'organigramma)ColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   TDBGridDBGrid41Left Top9WidthHeight� AlignalTop
DataSourceData.DsAnagMansioniTabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameAreaReadOnly	Width� Visible	 Expanded	FieldNameMansioneReadOnly	Title.CaptionRuoloWidth� Visible	 	AlignmenttaCenterExpanded	FieldNameAnniEsperienzaTitle.Captionesper.(mesi)Visible	 	AlignmenttaCenterExpanded	FieldNameDisponibilePickList.Stringss�no Title.CaptionDisp.Visible	    TPanelPanel17Left Top� WidthHeight� AlignalClient
BevelOuter	bvLoweredCaptionPanel17TabOrder TPanelPanel74LeftTopWidthHeightAlignalTop	AlignmenttaLeftJustifyCaption  Caratteristiche personaliColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   TDBGridDBGrid19LeftTopWidthHeight� AlignalClient
DataSourceData2.DsCarattDipTabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameCaratteristicaWidth
Visible	 	AlignmenttaCenterExpanded	FieldName	PunteggioTitle.AlignmenttaCenterVisible	     TPanelPanel4Left Top WidthHeight&AlignalTop
BevelOuter	bvLoweredTabOrder TToolbarButton97TbBAnagMansNewLeftTopWidth2Height%CaptionNuovo ruoloOpaqueWordWrap	OnClickTbBAnagMansNewClick  TToolbarButton97TbBAnagMansDelLefttTopWidth2Height%CaptionElimina ruoloOpaqueWordWrap	OnClickTbBAnagMansDelClick  TToolbarButton97TbBAnagMansModLeft4TopWidth?Height%CaptionModifica dati ruoloOpaqueWordWrap	OnClickTbBAnagMansModClick    	TTabSheetTSAnagStoricoHelpContext�� CaptionStorico gen. TPanelPanel7Left TopWidthHeighthAlignalClient
BevelOuter	bvLoweredCaptionPanel7TabOrder  TDBGridDBGrid3LeftTopWidthHeightfAlignalClient
DataSourceData.DsStoricoOptions	dgEditingdgTitlesdgColumnResize
dgColLines
dgRowLinesdgConfirmDeletedgCancelOnExit ReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style Columns	AlignmenttaCenterExpanded	FieldName
DataEventoTitle.CaptionData eventoWidthFVisible	 Expanded	FieldName
DescEventoTitle.CaptionEventoWidth� Visible	 Expanded	FieldNameAnnotazioniWidthVisible	     TPanelPanel19Left Top WidthHeightAlignalTop	AlignmenttaLeftJustifyCaption  Storico EventiColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   	TTabSheetTSAnagNoteIntCaptionNote interne
ImageIndex 	TSplitter	Splitter1Left TopWidthHeightCursorcrVSplitAlignalTop  �TFrameDBRichFrameDBRich1WidthHeightAlignalTop �TPanelRulerWidth �TBevelBevel1Width   �
TStatusBar	StatusBarTop� Width  �TToolBarStandardToolBarWidth  �TDBRichEditEditorWidthHeight� 	DataFieldNote
DataSourceDsQAnagNote   TPanelPanel153Left Top	WidthHeightAlignalTop	AlignmenttaLeftJustifyCaptionP  Note associate alle commesse in cui � presente il candidato (non modificabili)ColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  	TdxDBGrid	dxDBGrid5Left TopWidthHeight_Bands  DefaultLayout	HeaderPanelRowCountKeyFieldIDSummaryGroups SummarySeparator, AlignalClientTabOrder
DataSourceDataRicerche.DsQAnagNoteRic	OptionsDBedgoCancelOnExitedgoCanDeleteedgoCanInsertedgoCanNavigationedgoConfirmDeleteedgoLoadAllRecordsedgoUseBookmarks OptionsViewedgoAutoWidthedgoBandHeaderWidthedgoUseBitmap PreviewLines TdxDBGridMaskColumndxDBGrid5IDVisibleWidthq	BandIndex RowIndex 	FieldNameID  TdxDBGridMaskColumndxDBGrid5ProgressivoCaptionRif.DisableEditor	SortedcsUpWidth4	BandIndex RowIndex 	FieldNameProgressivo  TdxDBGridBlobColumndxDBGrid5NoteWidth�	BandIndex RowIndex HeaderMaxLineCount 	FieldNameNoteBlobPaintStylebpsTextImmediatePopup	MemoScrollBarsssNoneMemoWantReturns     TQuery	QAnagNoteActive	CachedUpdates	DatabaseNameEBCDB
DataSourceData.DsAnagraficaRequestLive	SQL.Stringsselect IDAnagrafica,Note from AnagAltreInfo where IDAnagrafica=:ID UpdateObjectUpdAnagNoteLeft� Top� 	ParamDataDataType	ftAutoIncNameID	ParamType	ptUnknown   
TMemoFieldQAnagNoteNote	FieldNameNoteBlobTypeftMemo  TIntegerFieldQAnagNoteIDAnagrafica	FieldNameIDAnagrafica   TDataSourceDsQAnagNoteDataSet	QAnagNoteOnStateChangeDsQAnagNoteStateChangeLeft� Top�   
TUpdateSQLUpdAnagNoteModifySQL.Stringsupdate AnagAltreInfoset  Note = :Notewhere"  IDAnagrafica = :OLD_IDAnagrafica InsertSQL.Stringsinsert into AnagAltreInfo  (Note)values	  (:Note) DeleteSQL.Stringsdelete from AnagAltreInfowhere"  IDAnagrafica = :OLD_IDAnagrafica Left� Top�    