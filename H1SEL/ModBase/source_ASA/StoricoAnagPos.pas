unit StoricoAnagPos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, FrmStoricoAnagPos;

type
  TStoricoAnagPosForm = class(TForm)
    StoricoAnagPosFrame1: TStoricoAnagPosFrame;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  StoricoAnagPosForm: TStoricoAnagPosForm;

implementation

{$R *.DFM}

procedure TStoricoAnagPosForm.BitBtn1Click(Sender: TObject);
begin
     close;
end;

procedure TStoricoAnagPosForm.FormShow(Sender: TObject);
begin
     Caption:='[S/42] - '+Caption;
     with StoricoAnagPosFrame1 do begin
          QStoricoPos.SQL.text:='select Anag1.Cognome,Anag1.Nome,StoricoAnagPosOrg.*, '+
                                'Anag2.Cognome AutorizzatoDa, Anag2.Nome AutorizzatoDaNome, '+
                                'Organigramma.Descrizione '+
                                'from StoricoAnagPosOrg, Anagrafica Anag1, Anagrafica Anag2, Organigramma '+
                                'where StoricoAnagPosOrg.IDAnagrafica = Anag1.ID '+
                                '   and StoricoAnagPosOrg.IDAutorizzatoDa *= Anag2.ID '+
                                '   and StoricoAnagPosOrg.IDOrganigramma = Organigramma.ID ';
          if (xIDOrganigramma=0) and (xIDAnagrafica>0) then
             QStoricoPos.SQL.add('and StoricoAnagPosOrg.IDAnagrafica='+IntToStr(xIDAnagrafica));
          if (xIDOrganigramma>0) and (xIDAnagrafica=0) then
             QStoricoPos.SQL.add('and StoricoAnagPosOrg.IDOrganigramma='+IntToStr(xIDOrganigramma));
          QStoricoPos.SQL.add('order by DallaData');
          QStoricoPos.Open;
    end;
end;

end.
