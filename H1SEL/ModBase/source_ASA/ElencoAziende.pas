unit ElencoAziende;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Grids, DBGrids, Db, DBTables, StdCtrls, Buttons, TB97;

type
  TElencoAziendeForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    DsElencoAziende: TDataSource;
    DBGrid1: TDBGrid;
    TSettoriLK: TTable;
    TSettoriLKID: TAutoIncField;
    TSettoriLKAttivita: TStringField;
    Label1: TLabel;
    Edit1: TEdit;
    TElencoAziende: TQuery;
    TElencoAziendeID: TAutoIncField;
    TElencoAziendeDescrizione: TStringField;
    TElencoAziendeIndirizzo: TStringField;
    TElencoAziendeCap: TStringField;
    TElencoAziendeComune: TStringField;
    TElencoAziendeProvincia: TStringField;
    TElencoAziendeIDAttivita: TIntegerField;
    TElencoAziendeAttivita: TStringField;
    procedure Edit1Change(Sender: TObject);
  private
  public
    { Public declarations }
  end;

var
  ElencoAziendeForm: TElencoAziendeForm;

implementation

uses Azienda;

{$R *.DFM}

procedure TElencoAziendeForm.Edit1Change(Sender: TObject);
begin
     TElencoAziende.Close;
     TElencoAziende.SQL.text:='select * from EBC_Clienti '+
                              'where Descrizione like :x';
     TElencoAziende.ParamByName('x').asString:=Edit1.Text+'%';
     TElencoAziende.Open;
end;

end.
