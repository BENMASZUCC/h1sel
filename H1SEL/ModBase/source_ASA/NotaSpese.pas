unit NotaSpese;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FloatEdit, DtEdit97, ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls,
  ToolEdit, CurrEdit;

type
  TNotaSpeseForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Panel1: TPanel;
    Label1: TLabel;
    DBEDenomCliente: TDBEdit;
    GroupBox1: TGroupBox;
    ERicRif: TEdit;
    ERicRuolo: TEdit;
    SpeedButton1: TSpeedButton;
    GroupBox2: TGroupBox;
    SpeedButton2: TSpeedButton;
    ESoggCogn: TEdit;
    RGTipoSogg: TRadioGroup;
    ESoggNome: TEdit;
    Label2: TLabel;
    DEData: TDateEdit97;
    Label3: TLabel;
    EDescrizione: TEdit;
    GroupBox3: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    EAltroDesc: TEdit;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label12: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    GroupBox4: TGroupBox;
    EFattNum: TEdit;
    EFattData: TEdit;
    SpeedButton3: TSpeedButton;
    RxCalcEdit1: TRxCalcEdit;
    RxCalcEdit2: TRxCalcEdit;
    RxCalcEdit3: TRxCalcEdit;
    RxCalcEdit4: TRxCalcEdit;
    RxCalcEdit5: TRxCalcEdit;
    RxCalcEdit6: TRxCalcEdit;
    RxCalcEdit7: TRxCalcEdit;
    RxCalcEdit8: TRxCalcEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
  public
    xIDRicerca,xIDSoggetto,xIDFattura:integer;
  end;

var
  NotaSpeseForm: TNotaSpeseForm;

implementation

uses ModuloDati2, ElencoRicCliente, ElencoDip, ElencoUtenti,
  ElencoFattCliente;

{$R *.DFM}

procedure TNotaSpeseForm.SpeedButton1Click(Sender: TObject);
begin
     ElencoRicClienteForm:=TElencoRicClienteForm.create(self);
     Data2.QRicClienti.Open;
     ElencoRicClienteForm.ShowModal;
     if ElencoRicClienteForm.Modalresult=mrOK then begin
        xIDRicerca:=Data2.QRicClientiID.Value;
        ERicRif.Text:=Data2.QRicClientiProgressivo.Value;
        ERicRuolo.text:=Data2.QRicClientiRuolo.Value;
     end else begin
        xIDRicerca:=0;
        ERicRif.Text:='';
        ERicRuolo.text:='';
     end;
     Data2.QRicClienti.Close;
     ElencoRicClienteForm.Free;
end;

procedure TNotaSpeseForm.SpeedButton2Click(Sender: TObject);
begin
   if RGTipoSogg.ItemIndex=0 then begin
     ElencoDipForm:=TElencoDipForm.create(self);
     ElencoDipForm.ShowModal;
     if ElencoDipForm.Modalresult=mrOK then begin
        xIDSoggetto:=ElencoDipForm.TAnagDipID.Value;
        ESoggCogn.Text:=ElencoDipForm.TAnagDipCognome.Value;
        ESoggNome.text:=ElencoDipForm.TAnagDipNome.Value;
     end else begin
        xIDSoggetto:=0;
        ESoggCogn.Text:='';
        ESoggNome.text:='';
     end;
     ElencoDipForm.Free;
   end else begin
     ESoggNome.text:='';
     ElencoUtentiForm:=TElencoUtentiForm.create(self);
     ElencoUtentiForm.ShowModal;
     if ElencoUtentiForm.Modalresult=mrOK then begin
        xIDSoggetto:=ElencoUtentiForm.QUsersID.Value;
        ESoggCogn.Text:=ElencoUtentiForm.QUsersNominativo.Value;
     end else begin
        xIDSoggetto:=0;
        ESoggCogn.Text:='';
     end;
     ElencoUtentiForm.Free;
   end;
end;

procedure TNotaSpeseForm.SpeedButton3Click(Sender: TObject);
begin
     ElencoFattClienteForm:=TElencoFattClienteForm.create(self);
     ElencoFattClienteForm.ShowModal;
     if ElencoFattClienteForm.Modalresult=mrOK then begin
        xIDFattura:=ElencoFattClienteForm.QFattClienteID.Value;
        EFattNum.Text:=ElencoFattClienteForm.QFattClienteProgressivo.asString;
        EFattData.text:=ElencoFattClienteForm.QFattClienteData.asString;
     end else begin
        xIDFattura:=0;
        EFattNum.Text:='';
        EFattData.text:='';
     end;
     ElencoFattClienteForm.Free;
end;

procedure TNotaSpeseForm.FormCreate(Sender: TObject);
begin
     xIDRicerca:=0;
     xIDSoggetto:=0;
     xIDFattura:=0;
end;

procedure TNotaSpeseForm.FormShow(Sender: TObject);
begin
     Caption:='[M/150] - '+caption;
end;

end.
