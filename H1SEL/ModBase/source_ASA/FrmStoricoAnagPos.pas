unit FrmStoricoAnagPos;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     Db,DBTables,Grids,DBGrids,RXDBCtrl,ExtCtrls,TB97;

type
     TStoricoAnagPosFrame=class(TFrame)
          PanButtons: TPanel;
          RxDBGrid1: TRxDBGrid;
          QStoricoPos: TQuery;
          dsQStoricoPos: TDataSource;
          ToolbarButton971: TToolbarButton97;
          ToolbarButton972: TToolbarButton97;
          QStoricoPosCognome: TStringField;
          QStoricoPosNome: TStringField;
          QStoricoPosID: TAutoIncField;
          QStoricoPosIDAnagrafica: TIntegerField;
          QStoricoPosIDOrganigramma: TIntegerField;
          QStoricoPosDallaData: TDateTimeField;
          QStoricoPosAlladata: TDateTimeField;
          QStoricoPosNote: TStringField;
          QStoricoPosIDAutorizzatoDa: TIntegerField;
          QStoricoPosAutorizzatoDa: TStringField;
          QStoricoPosDescrizione: TStringField;
          QStoricoPosAutorizzatoDaNome: TStringField;
          ToolbarButton973: TToolbarButton97;
          procedure ToolbarButton971Click(Sender: TObject);
          procedure ToolbarButton972Click(Sender: TObject);
          procedure ToolbarButton973Click(Sender: TObject);
     private
          { Private declarations }
     public
          xIDOrganigramma,xIDAnagrafica: integer;
     end;

implementation

uses ModStoricoAnagPos,ModuloDati;

{$R *.DFM}

procedure TStoricoAnagPosFrame.ToolbarButton971Click(Sender: TObject);
begin
     ModStoricoAnagPosForm:=TModStoricoAnagPosForm.create(self);
     ModStoricoAnagPosForm.xIDAnagrafica:=QStoricoPosIDAnagrafica.Value;
     ModStoricoAnagPosForm.ECogn1.Text:=QStoricoPosCognome.Value;
     ModStoricoAnagPosForm.ENome1.Text:=QStoricoPosNome.Value;
     ModStoricoAnagPosForm.xIDAutorizzatoDa:=QStoricoPosIDAutorizzatoDa.Value;
     ModStoricoAnagPosForm.ECogn2.Text:=QStoricoPosAutorizzatoDa.Value;
     ModStoricoAnagPosForm.ENome2.Text:=QStoricoPosAutorizzatoDaNome.Value;
     ModStoricoAnagPosForm.DEDallaData.Date:=QStoricoPosDallaData.Value;
     ModStoricoAnagPosForm.DEAllaData.Date:=QStoricoPosAllaData.Value;
     ModStoricoAnagPosForm.ENote.text:=QStoricoPosNote.Value;
     ModStoricoAnagPosForm.ShowModal;
     if ModStoricoAnagPosForm.Modalresult=mrOK then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text:='update StoricoAnagPosOrg set IDAnagrafica=:xIDAnagrafica,'+
                         'DallaData=:xDallaData,Note=:xNote,IDAutorizzatoDa=:xIDAutorizzatoDa';
                    if ModStoricoAnagPosForm.DEAllaData.Text<>'' then
                         Q1.SQL.Add(',Alladata=:xAlladata ')
                    else Q1.SQL.Add(',Alladata=NULL ');
                    Q1.SQL.Add('where ID='+QStoricoPosID.asString);
                         Q1.ParamByName('xIDAnagrafica').asInteger:=ModStoricoAnagPosForm.xIDAnagrafica;
                         Q1.ParamByName('xDallaData').asDateTime:=ModStoricoAnagPosForm.DEDallaData.Date;
                         if ModStoricoAnagPosForm.DEAllaData.Text<>'' then
                              Q1.ParamByName('xAlladata').asDateTime:=ModStoricoAnagPosForm.DEAllaData.Date;
                         Q1.ParamByName('xNote').asString:=ModStoricoAnagPosForm.ENote.text;
                         Q1.ParamByName('xIDAutorizzatoDa').asInteger:=ModStoricoAnagPosForm.xIDAutorizzatoDa;
                         Q1.ExecSQL;
                         DB.CommitTrans;
                         QStoricoPos.Close;
                         QStoricoPos.Open;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               end;
          end;
     end;
     ModStoricoAnagPosForm.Free;
end;

procedure TStoricoAnagPosFrame.ToolbarButton972Click(Sender: TObject);
begin
     if QStoricoPos.RecordCount=0 then exit;
     if MessageDlg('Sei sicuro di voler eliminare DEFINITIVAMENTE la riga di storico ?',mtWarning,
          [mbNo,mbYes],0)=mrYes then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text:='delete from StoricoAnagPosOrg '+
                         'where ID='+QStoricoPosID.AsString;
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    QStoricoPos.Close;
                    QStoricoPos.Open;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               end;
          end;
     end;

end;

procedure TStoricoAnagPosFrame.ToolbarButton973Click(Sender: TObject);
begin
     ModStoricoAnagPosForm:=TModStoricoAnagPosForm.create(self);
     ModStoricoAnagPosForm.DEDallaData.Date:=Date;
     ModStoricoAnagPosForm.DEAllaData.Date:=Date;
     ModStoricoAnagPosForm.ENote.text:='';
     ModStoricoAnagPosForm.ShowModal;
     if ModStoricoAnagPosForm.Modalresult=mrOK then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    if ModStoricoAnagPosForm.DEAllaData.Text<>'' then
                         Q1.SQL.text:='insert into StoricoAnagPosOrg (IDAnagrafica,IDOrganigramma,DallaData,Alladata,Note,IDAutorizzatoDa) '+
                              ' values (:xIDAnagrafica,:xIDOrganigramma,:xDallaData,:xAlladata,:xNote,:xIDAutorizzatoDa)'
                    else
                         Q1.SQL.text:='insert into StoricoAnagPosOrg (IDAnagrafica,IDOrganigramma,DallaData,Note,IDAutorizzatoDa) '+
                              ' values (:xIDAnagrafica,:xIDOrganigramma,:xDallaData,:xNote,:xIDAutorizzatoDa)';
                    Q1.ParamByName('xIDAnagrafica').asInteger:=ModStoricoAnagPosForm.xIDAnagrafica;
                    Q1.ParamByName('xIDOrganigramma').asInteger:=xIDOrganigramma;
                    Q1.ParamByName('xDallaData').asDateTime:=ModStoricoAnagPosForm.DEDallaData.Date;
                    if ModStoricoAnagPosForm.DEAllaData.Text<>'' then
                         Q1.ParamByName('xAlladata').asDateTime:=ModStoricoAnagPosForm.DEAllaData.Date;
                    Q1.ParamByName('xNote').asString:=ModStoricoAnagPosForm.ENote.text;
                    Q1.ParamByName('xIDAutorizzatoDa').asInteger:=ModStoricoAnagPosForm.xIDAutorizzatoDa;
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    QStoricoPos.Close;
                    QStoricoPos.Open;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               end;
          end;
     end;
     ModStoricoAnagPosForm.Free;
end;

end.

