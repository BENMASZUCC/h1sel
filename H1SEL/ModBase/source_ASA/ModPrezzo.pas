unit ModPrezzo;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     StdCtrls,Buttons,Mask,ToolEdit,CurrEdit,ExtCtrls;

type
     TModPrezzoForm=class(TForm)
          BitBtn1: TBitBtn;
          BitBtn2: TBitBtn;
          Label3: TLabel;
          Label6: TLabel;
          Label7: TLabel;
          Bevel1: TBevel;
          Bevel2: TBevel;
          PrezzoLire: TRxCalcEdit;
          PrezzoEuro: TRxCalcEdit;
          procedure PrezzoLireChange(Sender: TObject);
          procedure FormShow(Sender: TObject);
    procedure PrezzoEuroChange(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     ModPrezzoForm: TModPrezzoForm;

implementation

{$R *.DFM}

procedure TModPrezzoForm.PrezzoLireChange(Sender: TObject);
begin
     if PrezzoLire.Focused then
          PrezzoEuro.Value:=PrezzoLire.value/1936.27;
end;

procedure TModPrezzoForm.FormShow(Sender: TObject);
begin
     Caption:='[M/181] - '+caption;
end;

procedure TModPrezzoForm.PrezzoEuroChange(Sender: TObject);
begin
     if PrezzoEuro.Focused then
          PrezzoLire.Value:=PrezzoEuro.value*1936.27;
end;

end.

