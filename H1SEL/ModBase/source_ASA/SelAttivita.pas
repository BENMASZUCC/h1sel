unit SelAttivita;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, Db, DBTables, StdCtrls, Buttons, ExtCtrls, dxTL,
  dxDBCtrl, dxDBGrid, dxCntner;

type
  TSelAttivitaForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    DsSettori: TDataSource;
    Timer1: TTimer;
    TSettori: TQuery;
    dxDBGrid1: TdxDBGrid;
    dxDBGrid1Attivita: TdxDBGridMaskColumn;
    dxDBGrid1AreaSettore: TdxDBGridMaskColumn;
    TSettoriID: TAutoIncField;
    TSettoriAttivita: TStringField;
    TSettoriIDAreaSettore: TIntegerField;
    TSettoriAreaSettore: TStringField;
    TSettoriAttivita_ENG: TStringField;
    dxDBGrid1Column3: TdxDBGridColumn;
    procedure Timer1Timer(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    xStringa:string;
  public
    { Public declarations }
  end;

var
  SelAttivitaForm: TSelAttivitaForm;

implementation

{$R *.DFM}

procedure TSelAttivitaForm.Timer1Timer(Sender: TObject);
begin
     xStringa:='';
end;

procedure TSelAttivitaForm.FormShow(Sender: TObject);
begin
     Caption:='[S/6] - '+caption;
end;

end.
