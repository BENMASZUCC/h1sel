unit Lingue;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, TB97, Grids, DBGrids, Db, DBTables, ExtCtrls, Wall, Buttons;

type
  TLingueForm = class(TForm)
    Lingue: TTable;
    DsLingue: TDataSource;
    LingueID: TAutoIncField;
    LingueLingua: TStringField;
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    Edit1: TEdit;
    Label1: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Panel2: TPanel;
    Label2: TLabel;
    QLivLingue: TQuery;
    DsQLivLingue: TDataSource;
    QLivLingueID: TAutoIncField;
    QLivLingueLivelloConoscenza: TStringField;
    DBGrid2: TDBGrid;
    Label3: TLabel;
    ESoggiorno: TEdit;
    Label4: TLabel;
    DBGrid3: TDBGrid;
    QLivLingue2: TQuery;
    DsQLivLingue2: TDataSource;
    QLivLingue2ID: TAutoIncField;
    QLivLingue2LivelloConoscenza: TStringField;
    BITA: TSpeedButton;
    BENG: TSpeedButton;
    LingueLingua_ENG: TStringField;
    QLivLingueLivelloConoscenza_ENG: TStringField;
    QLivLingue2LivelloConoscenza_ENG: TStringField;
    procedure Edit1Change(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BITAClick(Sender: TObject);
    procedure BENGClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  LingueForm: TLingueForm;

implementation

uses Curriculum, ModuloDati;

{$R *.DFM}

procedure TLingueForm.Edit1Change(Sender: TObject);
begin
     Lingue.FindNearest([Edit1.text]);
end;

procedure TLingueForm.FormShow(Sender: TObject);
begin
     Caption:='[S/50] - '+Caption;
     Edit1.text:='';
     if (Uppercase(copy(Data.GlobalNomeAzienda.Value,1,7))='FERRARI') or
        (Uppercase(copy(Data.GlobalNomeAzienda.Value,1,3))='EBC') then begin
        BITA.visible:=True;
        BENG.visible:=True;
     end;
end;

procedure TLingueForm.BITAClick(Sender: TObject);
begin
     DBGrid1.Columns[0].FieldName:='Lingua';
     DBGrid1.Columns[0].Title.Caption:='Lingua';
     DBGrid2.Columns[0].FieldName:='LivelloConoscenza';
     DBGrid3.Columns[0].FieldName:='LivelloConoscenza';
end;

procedure TLingueForm.BENGClick(Sender: TObject);
begin
     DBGrid1.Columns[0].FieldName:='Lingua_ENG';
     DBGrid1.Columns[0].Title.Caption:='Lingua (ENG)';
     DBGrid2.Columns[0].FieldName:='LivelloConoscenza_ENG';
     DBGrid3.Columns[0].FieldName:='LivelloConoscenza_ENG';
end;

end.
