object ValutazColloquioForm: TValutazColloquioForm
  Left = 204
  Top = 118
  Width = 776
  Height = 589
  Caption = 'Griglia di valutazione candidato'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 3
    Top = 4
    Width = 581
    Height = 47
    Anchors = [akLeft, akTop, akRight]
    BevelOuter = bvLowered
    Enabled = False
    TabOrder = 0
    object Label1: TLabel
      Left = 5
      Top = 4
      Width = 48
      Height = 13
      Caption = 'Candidato'
    end
    object ECognome: TEdit
      Left = 5
      Top = 19
      Width = 169
      Height = 21
      Color = clYellow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      Text = 'ECognome'
    end
    object ENome: TEdit
      Left = 175
      Top = 19
      Width = 138
      Height = 21
      Color = clYellow
      TabOrder = 1
      Text = 'ENome'
    end
  end
  object BitBtn1: TBitBtn
    Left = 679
    Top = 4
    Width = 86
    Height = 33
    Anchors = [akTop, akRight]
    TabOrder = 1
    OnClick = BitBtn1Click
    Kind = bkOK
  end
  object ASG1: TAdvStringGrid
    Left = 3
    Top = 56
    Width = 764
    Height = 465
    Anchors = [akLeft, akTop, akRight, akBottom]
    ColCount = 3
    DefaultRowHeight = 16
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goEditing]
    ScrollBars = ssHorizontal
    TabOrder = 2
    AutoNumAlign = False
    AutoSize = False
    VAlignment = vtaCenter
    EnhTextSize = False
    EnhRowColMove = False
    SortFixedCols = False
    SizeWithForm = False
    Multilinecells = False
    OnGetAlignment = ASG1GetAlignment
    OnRightClickCell = ASG1RightClickCell
    SortDirection = sdAscending
    OnCheckBoxClick = ASG1CheckBoxClick
    SortFull = True
    SortAutoFormat = True
    SortShow = False
    EnableGraphics = True
    SortColumn = 0
    HintColor = clYellow
    SelectionColor = clHighlight
    SelectionTextColor = clHighlightText
    SelectionRectangle = False
    SelectionRTFKeep = False
    HintShowCells = False
    OleDropTarget = False
    OleDropSource = False
    OleDropRTF = False
    PrintSettings.FooterSize = 0
    PrintSettings.HeaderSize = 0
    PrintSettings.Time = ppNone
    PrintSettings.Date = ppNone
    PrintSettings.DateFormat = 'dd/mm/yyyy'
    PrintSettings.PageNr = ppNone
    PrintSettings.Title = ppNone
    PrintSettings.Font.Charset = DEFAULT_CHARSET
    PrintSettings.Font.Color = clWindowText
    PrintSettings.Font.Height = -11
    PrintSettings.Font.Name = 'MS Sans Serif'
    PrintSettings.Font.Style = []
    PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
    PrintSettings.HeaderFont.Color = clWindowText
    PrintSettings.HeaderFont.Height = -11
    PrintSettings.HeaderFont.Name = 'MS Sans Serif'
    PrintSettings.HeaderFont.Style = []
    PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
    PrintSettings.FooterFont.Color = clWindowText
    PrintSettings.FooterFont.Height = -11
    PrintSettings.FooterFont.Name = 'MS Sans Serif'
    PrintSettings.FooterFont.Style = []
    PrintSettings.Borders = pbNoborder
    PrintSettings.BorderStyle = psSolid
    PrintSettings.Centered = False
    PrintSettings.RepeatFixedRows = False
    PrintSettings.RepeatFixedCols = False
    PrintSettings.LeftSize = 0
    PrintSettings.RightSize = 0
    PrintSettings.ColumnSpacing = 0
    PrintSettings.RowSpacing = 0
    PrintSettings.TitleSpacing = 0
    PrintSettings.Orientation = poPortrait
    PrintSettings.PagePrefix = 'page'
    PrintSettings.FixedWidth = 0
    PrintSettings.FixedHeight = 0
    PrintSettings.UseFixedHeight = False
    PrintSettings.UseFixedWidth = False
    PrintSettings.FitToPage = fpNever
    PrintSettings.PageNumSep = '/'
    PrintSettings.NoAutoSize = False
    PrintSettings.PrintGraphics = False
    HTMLSettings.Width = 100
    Navigation.AllowInsertRow = False
    Navigation.AllowDeleteRow = False
    Navigation.AdvanceOnEnter = False
    Navigation.AdvanceInsert = False
    Navigation.AutoGotoWhenSorted = False
    Navigation.AutoGotoIncremental = False
    Navigation.AutoComboDropSize = False
    Navigation.AdvanceDirection = adLeftRight
    Navigation.AllowClipboardShortCuts = False
    Navigation.AllowSmartClipboard = False
    Navigation.AllowRTFClipboard = False
    Navigation.AdvanceAuto = False
    Navigation.InsertPosition = pInsertBefore
    Navigation.CursorWalkEditor = False
    Navigation.MoveRowOnSort = False
    Navigation.ImproveMaskSel = False
    Navigation.AlwaysEdit = False
    ColumnSize.Save = False
    ColumnSize.Stretch = False
    ColumnSize.Location = clRegistry
    CellNode.Color = clSilver
    CellNode.NodeType = cnFlat
    CellNode.NodeColor = clBlack
    SizeWhileTyping.Height = False
    SizeWhileTyping.Width = False
    MouseActions.AllSelect = False
    MouseActions.ColSelect = False
    MouseActions.RowSelect = False
    MouseActions.DirectEdit = True
    MouseActions.DisjunctRowSelect = False
    MouseActions.AllColumnSize = False
    MouseActions.CaretPositioning = False
    IntelliPan = ipVertical
    URLColor = clBlack
    URLShow = False
    URLFull = False
    URLEdit = False
    ScrollType = ssNormal
    ScrollColor = clNone
    ScrollWidth = 16
    ScrollProportional = False
    ScrollHints = shNone
    OemConvert = False
    FixedFooters = 0
    FixedRightCols = 0
    FixedColWidth = 156
    FixedRowHeight = 16
    FixedFont.Charset = DEFAULT_CHARSET
    FixedFont.Color = clWindowText
    FixedFont.Height = -11
    FixedFont.Name = 'MS Sans Serif'
    FixedFont.Style = []
    FixedAsButtons = False
    FloatFormat = '%.2f'
    WordWrap = False
    ColumnHeaders.Strings = (
      'soggetto'
      'E'
      'C')
    Lookup = False
    LookupCaseSensitive = False
    LookupHistory = False
    HideFocusRect = False
    BackGround.Top = 0
    BackGround.Left = 0
    BackGround.Display = bdTile
    Hovering = False
    Filter = <>
    FilterActive = False
    ColWidths = (
      156
      15
      14)
    RowHeights = (
      16
      16
      16
      16
      16)
  end
  object Panel2: TPanel
    Left = 3
    Top = 526
    Width = 763
    Height = 33
    Anchors = [akLeft, akRight, akBottom]
    BevelOuter = bvLowered
    TabOrder = 3
    object Label2: TLabel
      Left = 581
      Top = 9
      Width = 131
      Height = 13
      Anchors = [akTop, akRight]
      Caption = 'Punteggio accumulato:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 8
      Top = 9
      Width = 390
      Height = 13
      Caption = 
        'fare click con il pulsante destro per visualizzare la descrizion' +
        'e dettagliata (se esiste)'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clPurple
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object ETot: TEdit
      Left = 717
      Top = 6
      Width = 41
      Height = 21
      Anchors = [akTop, akRight]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
  end
  object BInfoCand: TBitBtn
    Left = 590
    Top = 4
    Width = 86
    Height = 33
    Caption = 'Info cand.'
    TabOrder = 4
    Visible = False
    OnClick = BInfoCandClick
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000120B0000120B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      33333FFFFFFFFFFFFFFF000000000000000077777777777777770FFFFFFFFFFF
      FFF07F3FF3FF3FFF3FF70F00F00F000F00F07F773773777377370FFFFFFFFFFF
      FFF07F3FF3FF33FFFFF70F00F00FF00000F07F773773377777F70FEEEEEFF0F9
      FCF07F33333337F7F7F70FFFFFFFF0F9FCF07F3FFFF337F737F70F0000FFF0FF
      FCF07F7777F337F337370F0000FFF0FFFFF07F777733373333370FFFFFFFFFFF
      FFF07FFFFFFFFFFFFFF70CCCCCCCCCCCCCC07777777777777777088CCCCCCCCC
      C880733777777777733700000000000000007777777777777777333333333333
      3333333333333333333333333333333333333333333333333333}
    NumGlyphs = 2
  end
  object QGriglia: TQuery
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select ValutazModel.*,AnagValutaz.Valore'
      'from AnagValutaz,ValutazModel with (updlock)'
      'where ValutazModel.ID *= AnagValutaz.IDVoce'
      'and IDAnagrafica=:xIDAnag'
      'order by ValutazModel.ID')
    Left = 104
    Top = 224
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'xIDAnag'
        ParamType = ptUnknown
      end>
    object QGrigliaID: TAutoIncField
      FieldName = 'ID'
    end
    object QGrigliaVoce: TStringField
      FieldName = 'Voce'
      FixedChar = True
      Size = 30
    end
    object QGrigliaDescVoce: TStringField
      FieldName = 'DescVoce'
      FixedChar = True
      Size = 100
    end
    object QGrigliaVal1Dic: TStringField
      FieldName = 'Val1Dic'
      FixedChar = True
      Size = 30
    end
    object QGrigliaVal1Desc: TStringField
      FieldName = 'Val1Desc'
      FixedChar = True
      Size = 100
    end
    object QGrigliaVal2Dic: TStringField
      FieldName = 'Val2Dic'
      FixedChar = True
      Size = 30
    end
    object QGrigliaVal2Desc: TStringField
      FieldName = 'Val2Desc'
      FixedChar = True
      Size = 100
    end
    object QGrigliaVal3Dic: TStringField
      FieldName = 'Val3Dic'
      FixedChar = True
      Size = 30
    end
    object QGrigliaVal3Desc: TStringField
      FieldName = 'Val3Desc'
      FixedChar = True
      Size = 100
    end
    object QGrigliaVal4Dic: TStringField
      FieldName = 'Val4Dic'
      FixedChar = True
      Size = 30
    end
    object QGrigliaVal4Desc: TStringField
      FieldName = 'Val4Desc'
      FixedChar = True
      Size = 100
    end
    object QGrigliaVal5Dic: TStringField
      FieldName = 'Val5Dic'
      FixedChar = True
      Size = 30
    end
    object QGrigliaVal5Desc: TStringField
      FieldName = 'Val5Desc'
      FixedChar = True
      Size = 100
    end
    object QGrigliaValore: TSmallintField
      FieldName = 'Valore'
    end
  end
  object Q: TQuery
    DatabaseName = 'EBCDB'
    Left = 104
    Top = 280
  end
  object QUpd: TQuery
    DatabaseName = 'EBCDB'
    Left = 144
    Top = 280
  end
end
