object ModelloWordForm: TModelloWordForm
  Left = 564
  Top = 389
  BorderStyle = bsDialog
  Caption = 'Modello word'
  ClientHeight = 156
  ClientWidth = 421
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 80
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nome modello'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 4
    Top = 32
    Width = 84
    Height = 13
    Alignment = taRightJustify
    Caption = 'Breve descrizione'
  end
  object Label4: TLabel
    Left = 45
    Top = 135
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = 'iniziali file'
  end
  object BitBtn1: TBitBtn
    Left = 326
    Top = 2
    Width = 93
    Height = 36
    TabOrder = 4
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 326
    Top = 40
    Width = 93
    Height = 36
    Caption = 'Annulla'
    TabOrder = 5
    Kind = bkCancel
  end
  object EModello: TEdit
    Left = 96
    Top = 4
    Width = 185
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    MaxLength = 30
    ParentFont = False
    TabOrder = 0
  end
  object EDesc: TEdit
    Left = 96
    Top = 28
    Width = 225
    Height = 21
    MaxLength = 50
    TabOrder = 1
  end
  object EIniziali: TEdit
    Left = 96
    Top = 131
    Width = 41
    Height = 21
    MaxLength = 3
    TabOrder = 3
  end
  object RGTabella: TRadioGroup
    Left = 10
    Top = 52
    Width = 270
    Height = 74
    Caption = 'Tabella database'
    ItemIndex = 0
    Items.Strings = (
      'Anagrafica   (candidati)'
      'EBC_Clienti  (clienti-aziende)'
      '*RicClienti    (risultato della ricerca contatti-clienti)')
    TabOrder = 2
  end
end
