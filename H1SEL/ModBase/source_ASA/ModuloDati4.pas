unit ModuloDati4;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     Db,DBTables;

type
     TDataAnnunci=class(TDataModule)
          DsAnnunci: TDataSource;
          DsTestate: TDataSource;
          DsEdizioniTest: TDataSource;
          DsAnnunciRic: TDataSource;
          TGiorniIndicati: TTable;
          DsGiorniIndicati: TDataSource;
          TGiorniIndicatiID: TAutoIncField;
          TGiorniIndicatiIDEdizione: TIntegerField;
          TGiorniIndicatiIDQualifica: TIntegerField;
          TGiorniIndicatiGiorno: TSmallintField;
          TQualifiche: TTable;
          DsQualifiche: TDataSource;
          TQualificheID: TAutoIncField;
          TQualificheQualifica: TStringField;
          TGiorniIndicatiQualifica: TStringField;
          TSconti: TTable;
          DsSconti: TDataSource;
          DsAnnEdizDataXann: TDataSource;
          TEdizioniLK: TTable;
          TEdizioniLKID: TAutoIncField;
          TEdizioniLKIDTestata: TIntegerField;
          TEdizioniLKNomeEdizione: TStringField;
          TEdizioniLKContatto: TStringField;
          TEdizioniLKTelefoni: TStringField;
          TEdizioniLKFax: TStringField;
          TTestateLK: TTable;
          DsTestateLK: TDataSource;
          TTestateLKID: TAutoIncField;
          TTestateLKDenominazione: TStringField;
          TTestateLKTipologia: TStringField;
          TEdizioniLKTestata: TStringField;
          TAnnunciLK: TTable;
          TAnnunciLKID: TAutoIncField;
          TAnnunciLKRif: TStringField;
          TAnnunciLKData: TDateTimeField;
          TAnnunciLKTesto: TMemoField;
          TAnnunciLKNumModuli: TSmallintField;
          TAnnunciLKNumParole: TSmallintField;
          TAnnunciLKTotaleLire: TCurrencyField;
          TAnnunciLKTotaleEuro: TFloatField;
          TAnnunciLKNote: TMemoField;
          TEdizioniLKGiornoRPQ: TSmallintField;
          TEdizioniLKCostoParolaFerLire: TCurrencyField;
          TEdizioniLKCostoParolaFerEuro: TFloatField;
          TEdizioniLKCostoParolaFestLire: TCurrencyField;
          TEdizioniLKCostoParolaFestEuro: TFloatField;
          TEdizioniLKCostoParolaRPQLire: TCurrencyField;
          TEdizioniLKCostoParolaRPQEuro: TFloatField;
          TEdizioniLKCostoModuloFerLire: TCurrencyField;
          TEdizioniLKCostoModuloFerEuro: TFloatField;
          TEdizioniLKCostoModuloFestLire: TCurrencyField;
          TEdizioniLKCostoModuloFestEuro: TFloatField;
          TEdizioniLKCostoModuloRPQLire: TCurrencyField;
          TEdizioniLKCostoModuloRPQEuro: TFloatField;
          TScontiID: TAutoIncField;
          TScontiIDEdizione: TIntegerField;
          TScontiFinoAModuli: TIntegerField;
          TScontiPercSconto: TSmallintField;
          TFestivi: TTable;
          TFestiviData: TDateTimeField;
          TPenetraz: TTable;
          DsPenetraz: TDataSource;
          TPenetrazID: TAutoIncField;
          TPenetrazIDEdizione: TIntegerField;
          TPenetrazLocalita: TStringField;
          TPenetrazPenetrazione: TSmallintField;
          TPenetrazNumCopie: TSmallintField;
          TPenetrazNumLettori: TSmallintField;
          TGiorniSettLK: TTable;
          TGiorniSettLKID: TAutoIncField;
          TGiorniSettLKGiorno: TStringField;
          TGiorniIndicatiDescGiorno: TStringField;
          TAnnunciRicIDX: TTable;
          TAnnunciRicIDXID: TAutoIncField;
          TAnnunciRicIDXIDAnnuncio: TIntegerField;
          TAnnunciRicIDXIDRicerca: TIntegerField;
          TAnnunciRicIDXNote: TMemoField;
          QAnnAnag: TQuery;
          DsQAnnAnag: TDataSource;
          TAnnunciLKFileTesto: TStringField;
          TAnagABS: TTable;
          TAnagABSID: TAutoIncField;
          TAnagABSCVIDAnnData: TIntegerField;
          TAnagABSCVinseritoInData: TDateTimeField;
          TScontiIDX: TTable;
          TScontiIDXID: TAutoIncField;
          TScontiIDXIDEdizione: TIntegerField;
          TScontiIDXFinoAModuli: TIntegerField;
          TScontiIDXPercSconto: TSmallintField;
          TFattureLK: TTable;
          TFattureLKID: TAutoIncField;
          TFattureLKProgressivo: TIntegerField;
          TFattureLKData: TDateTimeField;
          TAnnunci: TQuery;
          TAnnunciID: TAutoIncField;
          TAnnunciData: TDateTimeField;
          TAnnunciTesto: TMemoField;
          TAnnunciCivetta: TBooleanField;
          TAnnunciFileTesto: TStringField;
          TAnnunciNumModuli: TSmallintField;
          TAnnunciNumParole: TSmallintField;
          TAnnunciTotaleLire: TFloatField;
          TAnnunciTotaleEuro: TFloatField;
          TAnnunciNote: TMemoField;
          TAnnunciArchiviato: TBooleanField;
          TAnnunciCVPervenuti: TSmallintField;
          TAnnunciCVIdonei: TSmallintField;
          TAnnunciCVChiamati: TSmallintField;
          TAnnunciIDFattura: TIntegerField;
          TAnnunciProgFattura: TIntegerField;
          UpdAnnunci: TUpdateSQL;
          Q: TQuery;
          QID: TAutoIncField;
          QCostoParolaFerLire: TFloatField;
          QCostoParolaFerEuro: TFloatField;
          QCostoParolaFestLire: TFloatField;
          QCostoParolaFestEuro: TFloatField;
          QCostoParolaRPQLire: TFloatField;
          QCostoParolaRPQEuro: TFloatField;
          QCostoModuloFerLire: TFloatField;
          QCostoModuloFerEuro: TFloatField;
          QCostoModuloFestLire: TFloatField;
          QCostoModuloFestEuro: TFloatField;
          QCostoModuloRPQLire: TFloatField;
          QCostoModuloRPQEuro: TFloatField;
          TAnnunciRic: TQuery;
          TAnnunciRicIDAnnuncio: TIntegerField;
          TAnnunciRicIDRicerca: TIntegerField;
          TAnnunciRicNote: TMemoField;
          TAnnunciRicID: TAutoIncField;
          TAnnunciRicProgressivo: TStringField;
          TAnnunciRicDataInizio: TDateTimeField;
          TAnnunciRicNumRicercati: TSmallintField;
          TAnnunciRicMansione: TStringField;
          TAnnunciRicCliente: TStringField;
          QAnnEdizDataXAnn: TQuery;
          QAnnEdizDataXAnnID: TAutoIncField;
          QAnnEdizDataXAnnIDAnnuncio: TIntegerField;
          QAnnEdizDataXAnnIDEdizione: TIntegerField;
          QAnnEdizDataXAnnData: TDateTimeField;
          QAnnEdizDataXAnnCostoLire: TFloatField;
          QAnnEdizDataXAnnCostoEuro: TFloatField;
          QAnnEdizDataXAnnTestata: TStringField;
          QAnnEdizDataXAnnEdizione: TStringField;
          QAnnEdizDataXAnnGiornoRPQ: TSmallintField;
          QAnnEdizDataXAnnTipoGiorno: TStringField;
          QAnnEdizDataXAnnGiorno: TStringField;
          TAnnunciIDCliente: TIntegerField;
          TAnnunciCliente: TStringField;
          TAnnunciIDUtente: TIntegerField;
          TAnnunciUtente: TStringField;
          QAnnEdizDataXAnnNumModuli: TIntegerField;
          QTestate: TQuery;
          QTestateID: TAutoIncField;
          QTestateDenominazione: TStringField;
          QTestateTipologia: TStringField;
          QEdizioniTest: TQuery;
          QEdizioniTestID: TAutoIncField;
          QEdizioniTestIDTestata: TIntegerField;
          QEdizioniTestNomeEdizione: TStringField;
          QEdizioniTestContatto: TStringField;
          QEdizioniTestTelefoni: TStringField;
          QEdizioniTestFax: TStringField;
          QEdizioniTestCostoParolaFerLire: TFloatField;
          QEdizioniTestCostoParolaFerEuro: TFloatField;
          QEdizioniTestCostoParolaFestLire: TFloatField;
          QEdizioniTestCostoParolaFestEuro: TFloatField;
          QEdizioniTestCostoParolaRPQLire: TFloatField;
          QEdizioniTestCostoParolaRPQEuro: TFloatField;
          QEdizioniTestCostoModuloFerLire: TFloatField;
          QEdizioniTestCostoModuloFerEuro: TFloatField;
          QEdizioniTestCostoModuloFestLire: TFloatField;
          QEdizioniTestCostoModuloFestEuro: TFloatField;
          QEdizioniTestCostoModuloRPQLire: TFloatField;
          QEdizioniTestCostoModuloRPQEuro: TFloatField;
          QEdizioniTestPenetrazioneProvincia: TStringField;
          QEdizioniTestPenetrazioneComune: TStringField;
          QEdizioniTestPenetrazioneZona: TStringField;
          QEdizioniTestGiornoRPQ: TSmallintField;
          TAnnunciIDTipoAnnuncio: TIntegerField;
          TAnnunciIDSede: TIntegerField;
          TAnnunciIDUtenteProj: TIntegerField;
          TAnnunciIDMansione: TIntegerField;
          TAnnunciUtenteProj: TStringField;
          TAnnunciRuoloAnnuncio: TStringField;
          TAnnunciSede: TStringField;
          TAnnunciTipoAnnuncio: TStringField;
          TAnnunciRicIDUtente: TIntegerField;
          QEdizioniTestNote: TStringField;
          QTestateNote: TStringField;
          QAnnEdizDataXAnnCostoANoiLire: TFloatField;
          QAnnEdizDataXAnnCostoANoiEuro: TFloatField;
          QAnnEdizDataXAnnScontoAlClienteLire: TFloatField;
          QAnnEdizDataXAnnScontoAlClienteEuro: TFloatField;
          QAnnEdizDataXAnnScontoANoiLire: TFloatField;
          QAnnEdizDataXAnnScontoANoiEuro: TFloatField;
          QEdizioniTestIDCliente: TIntegerField;
          QEdizioniTestConcessionario: TStringField;
          TAnnunciEmailHost: TStringField;
          TAnnunciEmailPort: TStringField;
          TAnnunciEmailPassword: TStringField;
          TAnnunciEmailUserID: TStringField;
          TAnnunciRicCodice: TStringField;
          QAnnRuoli: TQuery;
          DsQAnnRuoli: TDataSource;
          QAnnRuoliID: TAutoIncField;
          QAnnRuoliIDAnnuncio: TIntegerField;
          QAnnRuoliIDMansione: TIntegerField;
          QAnnRuoliCodice: TStringField;
          QAnnRuoliNote: TMemoField;
          QAnnRuoliRuolo: TStringField;
          QAnnEdizDataXAnnCodice: TStringField;
          QAnnAnagData: TQuery;
          DsQAnnAnagData: TDataSource;
          QAnnAnagDataID: TAutoIncField;
          QAnnAnagDataIDAnnEdizData: TIntegerField;
          QAnnAnagDataDataPervenuto: TDateTimeField;
          QAnnAnagDataCVNumero: TIntegerField;
          QAnnAnagDataCognome: TStringField;
          QAnnAnagDataNome: TStringField;
          QAnnAnagID: TAutoIncField;
          QAnnAnagIDAnnEdizData: TIntegerField;
          QAnnAnagDataPervenuto: TDateTimeField;
          QAnnAnagCVNumero: TIntegerField;
          QAnnAnagCognome: TStringField;
          QAnnAnagNome: TStringField;
          QAnnAnagIDAnagrafica: TIntegerField;
          QAnnEdizDataXAnnListinoLire: TFloatField;
          QAnnEdizDataXAnnListinoEuro: TFloatField;
          TAnnunciIDCentroCostoCG: TIntegerField;
          QCentriCostoCGLK: TQuery;
          TAnnunciCentroCostoCG: TStringField;
          DsQCentriCostoCGLK: TDataSource;
          QAnnContrattiCG: TQuery;
          DsQAnnContrattiCG: TDataSource;
          QAnnContrattiCGID: TAutoIncField;
          QAnnContrattiCGCodiceContratto: TStringField;
          QAnnContrattiCGDescrizione: TStringField;
          QAnnTimeReport: TQuery;
          DsQAnnTimeReport: TDataSource;
          QAnnTimeReportID: TAutoIncField;
          QAnnTimeReportIDAnnuncio: TIntegerField;
          QAnnTimeReportIDUtente: TIntegerField;
          QAnnTimeReportData: TDateTimeField;
          QAnnTimeReportIDGruppoProfess: TIntegerField;
          QAnnTimeReportOreConsuntivo: TFloatField;
          QAnnTimeReportNote: TMemoField;
          UpdAnnTimeRep: TUpdateSQL;
          QUtentiLK: TQuery;
          DsQUtentiLK: TDataSource;
          QUtentiLKID: TAutoIncField;
          QUtentiLKNominativo: TStringField;
          QUtentiLKDescrizione: TStringField;
          QAnnTimeReportUtente: TStringField;
          QGruppiProfLK: TQuery;
          DsQGruppiProfLK: TDataSource;
          QGruppiProfLKID: TAutoIncField;
          QGruppiProfLKGruppoProfessionale: TStringField;
          QAnnTimeReportGruppoProfess: TStringField;
          QTemp: TQuery;
          TAnnunciIDContrattoCG: TIntegerField;
          QContrattiCGLK: TQuery;
          DsQContrattiCGLK: TDataSource;
          QContrattiCGLKID: TAutoIncField;
          QContrattiCGLKCodiceContratto: TStringField;
          QContrattiCGLKDescrizione: TStringField;
          TAnnunciCodiceContrattoCG: TStringField;
          QUtentiLKIDGruppoProfess: TIntegerField;
          TAnnunciRif: TStringField;
          procedure DataAnnunciCreate(Sender: TObject);
          procedure DsPenetrazStateChange(Sender: TObject);
          procedure DsScontiStateChange(Sender: TObject);
          procedure DsGiorniIndicatiStateChange(Sender: TObject);
          procedure DsAnnunciStateChange(Sender: TObject);
          procedure TAnnunciBeforeOpen(DataSet: TDataSet);
          procedure TAnnunciAfterClose(DataSet: TDataSet);
          procedure QAnnEdizDataXAnnCalcFields(DataSet: TDataSet);
          procedure QUtentiLKBeforeOpen(DataSet: TDataSet);
          procedure QAnnTimeReportBeforeOpen(DataSet: TDataSet);
          procedure QAnnTimeReportAfterClose(DataSet: TDataSet);
          procedure QAnnTimeReportAfterInsert(DataSet: TDataSet);
          procedure QAnnTimeReportNewRecord(DataSet: TDataSet);
          procedure QAnnTimeReportAfterPost(DataSet: TDataSet);
          procedure DsQAnnTimeReportStateChange(Sender: TObject);
          procedure QAnnTimeReportAfterDelete(DataSet: TDataSet);
          procedure QAnnTimeReportBeforeDelete(DataSet: TDataSet);
          procedure QAnnTimeReportBeforePost(DataSet: TDataSet);
          procedure TAnnunciAfterPost(DataSet: TDataSet);
     private
          xIDTimeSheet: integer;
     public
          { Public declarations }
          xAnnuncioForm: boolean;
     end;

var
     DataAnnunci: TDataAnnunci;

implementation

uses Main,ModuloDati;

{$R *.DFM}

procedure TDataAnnunci.DataAnnunciCreate(Sender: TObject);
begin
     xAnnuncioForm:=False;
end;

procedure TDataAnnunci.DsPenetrazStateChange(Sender: TObject);
var b: boolean;
begin
     b:=DsPenetraz.State in [dsEdit,dsInsert];
     MainForm.TbBPenetrazNew.Enabled:=not b;
     MainForm.TbBPenetrazDel.Enabled:=not b;
     MainForm.TbBPenetrazOK.Enabled:=b;
     MainForm.TbBPenetrazCan.Enabled:=b;
end;

procedure TDataAnnunci.DsScontiStateChange(Sender: TObject);
var b: boolean;
begin
     b:=DsSconti.State in [dsEdit,dsInsert];
     MainForm.TbBScontiNew.Enabled:=not b;
     MainForm.TbBScontiDel.Enabled:=not b;
     MainForm.TbBScontiOK.Enabled:=b;
     MainForm.TbBScontiCan.Enabled:=b;
end;

procedure TDataAnnunci.DsGiorniIndicatiStateChange(Sender: TObject);
var b: boolean;
begin
     b:=DsGiorniIndicati.State in [dsEdit,dsInsert];
     MainForm.TbBGiorniNew.Enabled:=not b;
     MainForm.TbBGiorniDel.Enabled:=not b;
     MainForm.TbBGiorniOK.Enabled:=b;
     MainForm.TbBGiorniCan.Enabled:=b;
end;

procedure TDataAnnunci.DsAnnunciStateChange(Sender: TObject);
var b: boolean;
begin
     b:=DsAnnunci.State in [dsEdit,dsInsert];
     MainForm.TbBAnnNew.Enabled:=not b;
     MainForm.TbBAnnDel.Enabled:=not b;
     MainForm.TbBAnnOK.Enabled:=b;
     MainForm.TbBAnnCan.Enabled:=b;
end;

procedure TDataAnnunci.TAnnunciBeforeOpen(DataSet: TDataSet);
begin
     TFattureLK.Open;
     QCentriCostoCGLK.Open;
     QContrattiCGLK.Open;
end;

procedure TDataAnnunci.TAnnunciAfterClose(DataSet: TDataSet);
begin
     TFattureLK.Close;
     QCentriCostoCGLK.Close;
     QContrattiCGLK.Close;
end;

procedure TDataAnnunci.QAnnEdizDataXAnnCalcFields(DataSet: TDataSet);
var xGiorno: string;
begin
     case dayofweek(QAnnEdizDataXannData.Value) of
          1: QAnnEdizDataXannGiorno.Value:='Dom';
          2: QAnnEdizDataXannGiorno.Value:='Lun';
          3: QAnnEdizDataXannGiorno.Value:='Mar';
          4: QAnnEdizDataXannGiorno.Value:='Mer';
          5: QAnnEdizDataXannGiorno.Value:='Gio';
          6: QAnnEdizDataXannGiorno.Value:='Ven';
          7: QAnnEdizDataXannGiorno.Value:='Sab';
     end;

     xGiorno:='FER';
     if dayofweek(QAnnEdizDataXannData.Value)=1 then xGiorno:='FES';
     if TFestivi.FindKey([QAnnEdizDataXannData.Value]) then xGiorno:='FES';
     if dayofweek(QAnnEdizDataXannData.Value)=QAnnEdizDataXannGiornoRPQ.Value then xGiorno:='RPQ';
     QAnnEdizDataXannTipoGiorno.Value:=xGiorno;
end;

procedure TDataAnnunci.QUtentiLKBeforeOpen(DataSet: TDataSet);
begin
     // setta parametro
     QUtentiLK.ParamByName('xoggi').asDateTime:=Date;
end;

procedure TDataAnnunci.QAnnTimeReportBeforeOpen(DataSet: TDataSet);
begin
     QUtentiLK.Open;
     QGruppiProfLK.Open;
end;

procedure TDataAnnunci.QAnnTimeReportAfterClose(DataSet: TDataSet);
begin
     QUtentiLK.Close;
     QGruppiProfLK.Close;
end;

procedure TDataAnnunci.QAnnTimeReportAfterInsert(DataSet: TDataSet);
begin
     QAnnTimeReportIDAnnuncio.Value:=TAnnunciID.Value;
end;

procedure TDataAnnunci.QAnnTimeReportNewRecord(DataSet: TDataSet);
begin
     QAnnTimeReportIDUtente.Value:=MainForm.xIDUtenteAttuale;
     QAnnTimeReportData.Value:=Date;
     QUtentiLK.Locate('ID',QAnnTimeReportIDUtente.Value, []);
     QAnnTimeReportIDGruppoProfess.Value:=QUtentiLKIDGruppoProfess.Value;
end;

procedure TDataAnnunci.QAnnTimeReportAfterPost(DataSet: TDataSet);
var xID: integer;
begin
     with QAnnTimeReport do begin
          Data.DB.BeginTrans;
          try
               ApplyUpdates;
               // se c'� il controllo di gestione, allora inserisci o aggiorna time report
               if copy(Data.GlobalCheckBoxIndici.Value,6,1)='1' then begin
                    if TAnnunciIDContrattoCG.AsString<>'' then begin
                         Data.QTemp.Close;
                         Data.QTemp.SQL.text:='select ID from CG_ContrattiTimeSheet '+
                              'where IDTimeRepAnnH1Sel=:xIDTimeRepAnnH1Sel';
                         Data.QTemp.ParamByName('xIDTimeRepAnnH1Sel').asInteger:=QAnnTimeReportID.Value;
                         Data.QTemp.Open;
                         if Data.QTemp.IsEmpty then begin
                              // ID questo record inserito
                              QTemp.Close;
                              QTemp.SQL.text:='select @@IDENTITY as LastID';
                              QTemp.Open;
                              xID:=QTemp.FieldByName('LastID').asInteger;
                              QTemp.Close;
                              Data.Q1.Close;
                              Data.Q1.SQL.text:='insert into CG_ContrattiTimeSheet (IDContratto, IDUtente, Data, OreConsuntivo, Note, IDGruppoProfess, IDTimeRepAnnH1Sel) '+
                                   'values (:xIDContratto, :xIDUtente, :xData, :xOreConsuntivo, :xNote, :xIDGruppoProfess, :xIDTimeRepAnnH1Sel)';
                              Data.Q1.ParamByName('xIDContratto').asInteger:=TAnnunciIDContrattoCG.Value;
                              Data.Q1.ParamByName('xIDUtente').asInteger:=QAnnTimeReportIDUtente.Value;
                              Data.Q1.ParamByName('xData').asDateTime:=QAnnTimeReportData.Value;
                              Data.Q1.ParamByName('xOreConsuntivo').asFloat:=QAnnTimeReportOreConsuntivo.Value;
                              Data.Q1.ParamByName('xNote').asString:=QAnnTimeReportNote.Value;
                              Data.Q1.ParamByName('xIDGruppoProfess').asInteger:=QAnnTimeReportIDGruppoProfess.Value;
                              Data.Q1.ParamByName('xIDTimeRepAnnH1Sel').asInteger:=xID;
                              Data.Q1.ExecSQL;
                         end else begin
                              Data.Q1.Close;
                              Data.Q1.SQL.text:='update CG_ContrattiTimeSheet set IDUtente=:xIDUtente, Data=:xData,'+
                                   ' OreConsuntivo=:xOreConsuntivo, Note=:xNote, IDGruppoProfess=:xIDGruppoProfess '+
                                   'where ID=:xID';
                              Data.Q1.ParamByName('xIDUtente').asInteger:=QAnnTimeReportIDUtente.Value;
                              Data.Q1.ParamByName('xData').asDateTime:=QAnnTimeReportData.Value;
                              Data.Q1.ParamByName('xOreConsuntivo').asFloat:=QAnnTimeReportOreConsuntivo.Value;
                              Data.Q1.ParamByName('xNote').asString:=QAnnTimeReportNote.Value;
                              Data.Q1.ParamByName('xIDGruppoProfess').asInteger:=QAnnTimeReportIDGruppoProfess.Value;
                              Data.Q1.ParamByName('xID').asInteger:=Data.QTemp.FieldByName('ID').asInteger;
                              Data.Q1.ExecSQL;
                         end;
                         Data.QTemp.Close;
                    end;
               end;
               Data.DB.CommitTrans;
               xID:=QAnnTimeReportID.Value;
               Close;
               Open;
               Locate('ID',xID, []);
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               raise;
          end;
          CommitUpdates;
     end;
end;

procedure TDataAnnunci.DsQAnnTimeReportStateChange(Sender: TObject);
var b: boolean;
begin
     b:=DsQAnnTimeReport.State in [dsEdit,dsInsert];
     MainForm.BTimeSheetNew.Enabled:=not b;
     MainForm.BTimeSheetDel.Enabled:=not b;
     MainForm.BTimeSheetOK.Enabled:=b;
     MainForm.BTimeSheetCan.Enabled:=b;
end;

procedure TDataAnnunci.QAnnTimeReportAfterDelete(DataSet: TDataSet);
begin
     // se c'� il controllo di gestione, allora inserisci o aggiorna time report
     with QAnnTimeReport do begin
          Data.DB.BeginTrans;
          try
               ApplyUpdates;
               if copy(Data.GlobalCheckBoxIndici.Value,6,1)='1' then begin
                    Data.QTemp.Close;
                    Data.QTemp.SQL.text:='delete from CG_ContrattiTimeSheet '+
                         'where IDTimeRepAnnH1Sel=:xIDTimeRepAnnH1Sel';
                    Data.QTemp.ParambyName('xIDTimeRepAnnH1Sel').asInteger:=xIDTimeSheet;
                    Data.QTemp.ExecSQL;
               end;
               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: operazione non effettuata',mtError, [mbOK],0);
          end;
     end;
end;

procedure TDataAnnunci.QAnnTimeReportBeforeDelete(DataSet: TDataSet);
begin
     xIDTimeSheet:=QAnnTimeReportID.Value;
end;

procedure TDataAnnunci.QAnnTimeReportBeforePost(DataSet: TDataSet);
begin
     // controllo utente-data
{     Data.QTemp.Close;
     Data.QTemp.SQL.text:='select count(*) Tot from Ann_AnnunciTimeSheet '+
          'where IDAnnuncio=:xIDAnnuncio and IDUtente=:xIDUtente and Data=:xData';
     Data.QTemp.ParamByName('xIDAnnuncio').asInteger:=TAnnunciID.Value;
     Data.QTemp.ParamByName('xIDUtente').asInteger:=QAnnTimeReportIDUtente.Value;
     Data.QTemp.ParamByName('xData').asDateTime:=QAnnTimeReportData.Value;
     Data.QTemp.Open;
     if Data.QTemp.FieldByName('Tot').asInteger>0 then begin
        ShowMessage('Duplicazione utente-data - IMPOSSIBILE REGISTRARE');
        Abort;
     end;}
end;

procedure TDataAnnunci.TAnnunciAfterPost(DataSet: TDataSet);
var xID: integer;
begin
     with TAnnunci do begin
          Data.DB.BeginTrans;
          try
               ApplyUpdates;
               Data.DB.CommitTrans;
               xID:=TAnnunciID.Value;
               Close;
               Open;
               TAnnunci.Locate('ID',xID, []);
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate in Ann_Annunci',mtError, [mbOK],0);
               raise;
          end;
          CommitUpdates;
     end;
end;

end.

