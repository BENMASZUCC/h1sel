�
 TLINGUEFORM 0  TPF0TLingueForm
LingueFormLeft'Top`BorderStylebsDialogCaptionSeleziona linguaClientHeight�ClientWidth� Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrder	PositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TDBGridDBGrid1Left TopGWidth� HeightbAlignalClient
DataSourceDsLingueTabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameLinguaWidth� Visible	    TPanelPanel1Left Top Width� HeightGAlignalTop
BevelOuterbvNoneTabOrder TLabelLabel1LeftTopWidthEHeightCaptionRicerca rapida  TSpeedButtonBITALeftTop/Width%Height
GroupIndexDown	CaptionITAFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontVisibleOnClick	BITAClick  TSpeedButtonBENGLeft'Top/Width%Height
GroupIndexCaptionENGFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontVisibleOnClick	BENGClick  TEditEdit1LeftTopWidthIHeightTabOrder OnChangeEdit1Change  TBitBtnBitBtn1LeftVTopWidthOHeight"TabOrderKindbkOK  TBitBtnBitBtn2Left� TopWidthOHeight"CaptionAnnullaTabOrderKindbkCancel   TPanelPanel2Left Top�Width� HeightNAlignalBottom
BevelOuter	bvLoweredTabOrder TLabelLabel2LeftTop	WidthJHeightCaptionLivello (parlato):  TLabelLabel3LeftTop7Width3HeightCaption
Soggiorno:  TLabelLabel4LeftTop!WidthFHeightCaptionLivello (scritto):  TDBGridDBGrid2LeftUTopWidth� Height
DataSourceDsQLivLingueOptions	dgEditingdgColumnResizedgTabsdgConfirmDeletedgCancelOnExit TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameLivelloConoscenzaVisible	    TEdit
ESoggiornoLeftUTop4Width� HeightTabOrder  TDBGridDBGrid3LeftUTopWidth� Height
DataSourceDsQLivLingue2Options	dgEditingdgColumnResizedgTabsdgConfirmDeletedgCancelOnExit TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameLivelloConoscenzaVisible	     TTableLingueActive	DatabaseNameEBCDB	IndexName	IdxLinguaReadOnly		TableName
dbo.LingueLeftTopP TAutoIncFieldLingueID	FieldNameID  TStringFieldLingueLingua	FieldNameLinguaSize  TStringFieldLingueLingua_ENG	FieldName
Lingua_ENG	FixedChar	Size   TDataSourceDsLingueDataSetLingueLeft8TopX  TQuery
QLivLingueActive	DatabaseNameEBCDBSQL.Stringsselect * from LivelloLingue  Left� Topp TAutoIncFieldQLivLingueID	FieldNameID  TStringFieldQLivLingueLivelloConoscenza	FieldNameLivelloConoscenza	FixedChar	Size  TStringFieldQLivLingueLivelloConoscenza_ENG	FieldNameLivelloConoscenza_ENGOrigin)EBCDB.LivelloLingue.LivelloConoscenza_ENG	FixedChar	Size   TDataSourceDsQLivLingueDataSet
QLivLingueLeft� Topp  TQueryQLivLingue2Active	DatabaseNameEBCDBSQL.Stringsselect * from LivelloLingue  Left� Top� TAutoIncFieldQLivLingue2ID	FieldNameIDOriginEBCDB.LivelloLingue.ID  TStringFieldQLivLingue2LivelloConoscenza	FieldNameLivelloConoscenzaOrigin%EBCDB.LivelloLingue.LivelloConoscenza	FixedChar	Size  TStringField QLivLingue2LivelloConoscenza_ENG	FieldNameLivelloConoscenza_ENGOrigin)EBCDB.LivelloLingue.LivelloConoscenza_ENG	FixedChar	Size   TDataSourceDsQLivLingue2DataSetQLivLingue2Left� Top�   