unit SelDiploma;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Grids, DBGrids, Db, DBTables;

type
  TSelDiplomaForm = class(TForm)
    DsDiplomi: TDataSource;
    DBGrid1: TDBGrid;
    BitBtn1: TBitBtn;
    QTipiDiplomi: TQuery;
    QTipiDiplomiTipo: TStringField;
    DsQTipiDiplomi: TDataSource;
    DBGrid2: TDBGrid;
    Label1: TLabel;
    Label2: TLabel;
    BitBtn2: TBitBtn;
    BITA: TSpeedButton;
    BENG: TSpeedButton;
    Diplomi: TQuery;
    DiplomiID: TAutoIncField;
    DiplomiIDArea: TIntegerField;
    DiplomiTipo: TStringField;
    DiplomiDescrizione: TStringField;
    DiplomiPunteggioMax: TSmallintField;
    DiplomiIDAzienda: TStringField;
    DiplomiTipo_ENG: TStringField;
    DiplomiDescrizione_ENG: TStringField;
    procedure FormShow(Sender: TObject);
    procedure BITAClick(Sender: TObject);
    procedure BENGClick(Sender: TObject);
  private
    { Private declarations }
    xStringa:string;
  public
    { Public declarations }
  end;

var
  SelDiplomaForm: TSelDiplomaForm;

implementation

uses ModuloDati;

{$R *.DFM}

procedure TSelDiplomaForm.FormShow(Sender: TObject);
begin
     Caption:='[S/38] - '+Caption;
     xStringa:='';
     if (Uppercase(copy(Data.GlobalNomeAzienda.Value,1,7))='FERRARI') or
        (Uppercase(copy(Data.GlobalNomeAzienda.Value,1,3))='EBC') then begin
        BITA.visible:=True;
        BENG.visible:=True;
     end;
end;

procedure TSelDiplomaForm.BITAClick(Sender: TObject);
begin
     QTipiDiplomi.Close;
     QTipiDiplomi.SQL.text:='select Tipo from Diplomi where Tipo is not null group by Tipo';
     QTipiDiplomi.Open;
     Diplomi.Close;
     Diplomi.SQL.text:='select * from Diplomi where Tipo=:Tipo order by Descrizione';
     Diplomi.Open;
     DBGrid1.Columns[0].FieldName:='Descrizione';
     DBGrid1.Columns[0].title.Caption:='Diploma';
end;

procedure TSelDiplomaForm.BENGClick(Sender: TObject);
begin
     QTipiDiplomi.Close;
     QTipiDiplomi.SQL.text:='select Tipo_ENG Tipo from Diplomi where Tipo_ENG is not null group by Tipo_ENG';
     QTipiDiplomi.Open;
     Diplomi.Close;
     Diplomi.SQL.text:='select * from Diplomi where Tipo_ENG=:Tipo order by Descrizione_ENG';
     Diplomi.Open;
     DBGrid1.Columns[0].FieldName:='Descrizione_ENG';
     DBGrid1.Columns[0].title.Caption:='Diploma (ENG)';
end;

end.
