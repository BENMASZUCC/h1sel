unit OffertaCliente;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     Grids,DBGrids,Buttons,StdCtrls,Mask,DtEdit97,DBCtrls,ExtCtrls,
     FloatEdit,ComObj,ToolEdit,CurrEdit,Db,DBTables,TB97,dxDBTLCl,dxTL,
     dxDBCtrl,dxCntner,dxDBTL,ImgList,dxLayout,dxGrClEx,ComCtrls,dxTLClms,
     dxGrClms,dxDBGrid;

type
     TOffertaClienteForm=class(TForm)
          BitBtn1: TBitBtn;
          BitBtn2: TBitBtn;
          Panel1: TPanel;
          DBEdit1: TDBEdit;
          DBEdit2: TDBEdit;
          DBEdit3: TDBEdit;
          PanOfferta: TPanel;
          Label1: TLabel;
          Label3: TLabel;
          Label4: TLabel;
          SpeedButton1: TSpeedButton;
          Label6: TLabel;
          Label8: TLabel;
          DEData: TDateEdit97;
          CBaMezzo: TComboBox;
          EAttenzioneDi: TEdit;
          ENote: TEdit;
          ECondizioni: TEdit;
          GroupBox1: TGroupBox;
          BStoricoRic: TSpeedButton;
          CBStato: TComboBox;
          GroupBox2: TGroupBox;
          SpeedButton3: TSpeedButton;
          Label11: TLabel;
          ERifCommessa: TEdit;
          ETipo: TEdit;
          GroupBox3: TGroupBox;
          SpeedButton2: TSpeedButton;
          Label12: TLabel;
          EIDContratto: TEdit;
          Q: TQuery;
          GroupBox4: TGroupBox;
          ERif: TEdit;
          QMansioniLK: TQuery;
          QMansioniLKID: TAutoIncField;
          QMansioniLKDescrizione: TStringField;
          QDettOfferta: TQuery;
          QDettOffertaID: TAutoIncField;
          QDettOffertaIDOfferta: TIntegerField;
          QDettOffertaTipo: TStringField;
          QDettOffertaDescrizione: TStringField;
          QDettOffertaImporto: TFloatField;
          QDettOffertaParent: TIntegerField;
          QDettOffertaNumRicercati: TSmallintField;
          QDettOffertaIDMansione: TIntegerField;
          QDettOffertaTipoCommessa: TStringField;
          QDettOffertaRuolo: TStringField;
          DsQDettOfferta: TDataSource;
          ImageList1: TImageList;
          QDettOffertaAccettata: TBooleanField;
          QTipiCommesseLK: TQuery;
          dxDBGridLayoutList1: TdxDBGridLayoutList;
          DsQTipiCommesseLK: TDataSource;
          dxDBGridLayoutList1Item1: TdxDBGridLayout;
          DsQMansioniLK: TDataSource;
          QMansioniLKArea: TStringField;
          dxDBGridLayoutList1Item2: TdxDBGridLayout;
          PCOfferta: TPageControl;
          TSDettaglio: TTabSheet;
          DBTreeList: TdxDBTreeList;
          DBTreeListID: TdxDBTreeListMaskColumn;
          DBTreeListIDOfferta: TdxDBTreeListMaskColumn;
          DBTreeListTipoCommessa: TdxDBTreeListExtLookupColumn;
          DBTreeListRuolo: TdxDBTreeListExtLookupColumn;
          DBTreeListNumRicercati: TdxDBTreeListSpinColumn;
          DBTreeListCodice: TdxDBTreeListColumn;
          DBTreeListTipo: TdxDBTreeListPickColumn;
          DBTreeListImporto: TdxDBTreeListCurrencyColumn;
          DBTreeListDescrizione: TdxDBTreeListMaskColumn;
          DBTreeListParent: TdxDBTreeListMaskColumn;
          DBTreeListIDMansione: TdxDBTreeListMaskColumn;
          DBTreeListColumn11: TdxDBTreeListCheckColumn;
          Panel2: TPanel;
          BNodoNew1: TToolbarButton97;
          BNodoNew2: TToolbarButton97;
          BNodoDel: TToolbarButton97;
          BNodoOK: TToolbarButton97;
          BNodoCan: TToolbarButton97;
          TSTimeSheet: TTabSheet;
          Panel4: TPanel;
          QOffertaTimeReport: TQuery;
          DsQOffertaTimeReport: TDataSource;
          UpdOffertaTimeReport: TUpdateSQL;
          QOffertaTimeReportID: TAutoIncField;
          QOffertaTimeReportIDOfferta: TIntegerField;
          QOffertaTimeReportIDUtente: TIntegerField;
          QOffertaTimeReportData: TDateTimeField;
          QOffertaTimeReportOreConsuntivo: TFloatField;
          QOffertaTimeReportNote: TMemoField;
          QUsersLK: TQuery;
          DsQUsersLK: TDataSource;
          QUsersLKID: TAutoIncField;
          QUsersLKNominativo: TStringField;
          QUsersLKDescrizione: TStringField;
          QOffertaTimeReportUtente: TStringField;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1Data: TdxDBGridDateColumn;
          dxDBGrid1Utente: TdxDBGridExtLookupColumn;
          dxDBGrid1Note: TdxDBGridBlobColumn;
          dxDBGridLayoutList1Item3: TdxDBGridLayout;
          Panel6: TPanel;
          BTimeSheetNew: TToolbarButton97;
          BTimeSheetDel: TToolbarButton97;
          BTimeSheetCan: TToolbarButton97;
          BTimeSheetOK: TToolbarButton97;
          QOffertaUtenti: TQuery;
          QOffertaUtentiID: TAutoIncField;
          QOffertaUtentiIDOfferta: TIntegerField;
          QOffertaUtentiIDUtente: TIntegerField;
          QMansioniLKIDArea: TIntegerField;
          QDettOffertaIDArea: TIntegerField;
          QOffertaTimeReportIDGruppoProfess: TIntegerField;
          QGruppiProfessLK: TQuery;
          DsQGruppiProfessLK: TDataSource;
          QGruppiProfessLKID: TAutoIncField;
          QGruppiProfessLKGruppoProfessionale: TStringField;
          QOffertaTimeReportGruppoProfess: TStringField;
          dxDBGridLayoutList1Item4: TdxDBGridLayout;
          dxDBGrid1Column5: TdxDBGridExtLookupColumn;
          QDettOffertaCodice: TStringField;
          Label2: TLabel;
          EArea: TEdit;
          BSelArea: TSpeedButton;
          Q3: TQuery;
          QOffertaTimeReportIDRicTimeSheet: TIntegerField;
          QDettOffertaTitoloCliente: TStringField;
          DBTreeListTitoloCliente: TdxDBTreeListColumn;
          dxDBGrid1IDRicTimeSheet: TdxDBGridColumn;
          dxDBGrid1OreConsuntivo: TdxDBGridCalcColumn;
          QUsersLKIDGruppoProfess: TIntegerField;
          GroupBox5: TGroupBox;
          DBGrid1: TDBGrid;
          QTipiCommesseLKID: TAutoIncField;
          QTipiCommesseLKTipoCommessa: TStringField;
          QTipiCommesseLKColore: TStringField;
          QLineeProdotto: TQuery;
          DsQLineeProdotto: TDataSource;
          QLineeProdottoID: TAutoIncField;
          QLineeProdottoLineaProdotto: TStringField;
          Label5: TLabel;
          procedure SpeedButton1Click(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure BStoricoRicClick(Sender: TObject);
          procedure FormCreate(Sender: TObject);
          procedure SpeedButton3Click(Sender: TObject);
          procedure SpeedButton2Click(Sender: TObject);
          procedure DBTreeListGetImageIndex(Sender: TObject;
               Node: TdxTreeListNode; var Index: Integer);
          procedure DBTreeListGetSelectedIndex(Sender: TObject;
               Node: TdxTreeListNode; var Index: Integer);
          procedure DBTreeListEditing(Sender: TObject; Node: TdxTreeListNode;
               var Allow: Boolean);
          procedure DBTreeListCustomDrawCell(Sender: TObject;
               ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
               AColumn: TdxTreeListColumn; ASelected,AFocused,
               ANewItemRow: Boolean; var AText: string; var AColor: TColor;
               AFont: TFont; var AAlignment: TAlignment; var ADone: Boolean);
          procedure DBTreeListCustomDrawBand(Sender: TObject;
               ABand: TdxTreeListBand; ACanvas: TCanvas; ARect: TRect;
               var AText: string; var AColor: TColor; AFont: TFont;
               var AAlignment: TAlignment; var ADone: Boolean);
          procedure DBTreeListCustomDrawColumnHeader(Sender: TObject;
               AColumn: TdxTreeListColumn; ACanvas: TCanvas; ARect: TRect;
               var AText: string; var AColor: TColor; AFont: TFont;
               var AAlignment: TAlignment; var ASorted: TdxTreeListColumnSort;
               var ADone: Boolean);
          procedure BNodoNew1Click(Sender: TObject);
          procedure BNodoNew2Click(Sender: TObject);
          procedure BNodoDelClick(Sender: TObject);
          procedure DsQDettOffertaStateChange(Sender: TObject);
          procedure BitBtn1Click(Sender: TObject);
          procedure BNodoOKClick(Sender: TObject);
          procedure BNodoCanClick(Sender: TObject);
          procedure QDettOffertaAfterOpen(DataSet: TDataSet);
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
          procedure QDettOffertaAfterPost(DataSet: TDataSet);
          procedure QOffertaTimeReportAfterPost(DataSet: TDataSet);
          procedure QOffertaTimeReportAfterInsert(DataSet: TDataSet);
          procedure BTimeSheetNewClick(Sender: TObject);
          procedure BTimeSheetDelClick(Sender: TObject);
          procedure BTimeSheetOKClick(Sender: TObject);
          procedure BTimeSheetCanClick(Sender: TObject);
          procedure DsQOffertaTimeReportStateChange(Sender: TObject);
          procedure QOffertaTimeReportNewRecord(DataSet: TDataSet);
          procedure QOffertaTimeReportBeforePost(DataSet: TDataSet);
          procedure BSelAreaClick(Sender: TObject);
          procedure dxDBGrid1Editing(Sender: TObject; Node: TdxTreeListNode;
               var Allow: Boolean);
          procedure dxDBGrid1CustomDrawCell(Sender: TObject; ACanvas: TCanvas;
               ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
               ASelected,AFocused,ANewItemRow: Boolean; var AText: string;
               var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
               var ADone: Boolean);
     private
          FParentValue: Variant;
     public
          xIDUtente,xIDCliente,xIDOfferta,xIDArea: integer;
          xIDRicerca,xIDContratto: integer;
          xCodContratto,xStato: string;
          xImportoTotale: real;
     end;

var
     OffertaClienteForm: TOffertaClienteForm;

implementation

uses ModuloDati2,ModuloDati,Main,StoricoOfferta,SelContatti,
     DettOfferta,ElencoRicCliente,uUtilsVarie,SelContrattoCli,MDRicerche,
     ElencoUtenti,SelArea;

{$R *.DFM}

procedure TOffertaClienteForm.SpeedButton1Click(Sender: TObject);
begin
     SelContattiForm:=TSelContattiForm.create(self);
     SelContattiForm.QContatti.ParamByName('xIDCliente').asInteger:=Data2.TEBCClientiID.Value;
     SelContattiForm.QContatti.Open;
     SelContattiForm.ShowModal;
     if SelContattiForm.ModalResult=mrOK then begin
          EAttenzioneDi.text:=SelContattiForm.QContattiContatto.Value;
     end else begin
          if MessageDlg('Vuoi togliere il contatto ?',mtInformation, [mbNo,mbYes],0)=mrYes then
               EAttenzioneDi.text:='';
     end;
     SelContattiForm.Free;
end;

procedure TOffertaClienteForm.FormShow(Sender: TObject);
begin
     // riferimento a ricerca
     if xIDRicerca>0 then begin
          Q.Close;
          Q.SQL.text:='select Progressivo,Tipo from EBC_Ricerche where ID='+IntToStr(xIDRicerca);
          Q.Open;
          ERifCommessa.text:=Q.FieldByName('Progressivo').asString;
          ETipo.text:=Q.FieldByName('Tipo').asString;
          Q.Close;
     end;
     if Uppercase(copy(Data.GlobalNomeAzienda.Value,1,7))='ERREMME' then begin
          GroupBox3.caption:='Riferimento a contratto';
          Label12.caption:='Contratto.n�:';
     end;

     // rif. a contratto
     EIDContratto.Text:=xCodContratto;
     // dettaglio offerta
     QDettOfferta.ParamByName('xIDOfferta').asinteger:=xIDOfferta;
     QDettOfferta.Open;
     QUsersLK.ParamByName('xOggi').asDateTime:=date;
     QUsersLK.Open;
     QOffertaTimeReport.ParamByName('xIDOfferta').asinteger:=xIDOfferta;
     QOffertaTimeReport.Open;
     DBTreeList.FullExpand;
     Caption:='[M/143] - '+caption;
     PCOfferta.ActivePageIndex:=0;
end;

procedure TOffertaClienteForm.BStoricoRicClick(Sender: TObject);
begin
     StoricoOffertaForm:=TStoricoOffertaForm.create(self);
     StoricoOffertaForm.QStoricoOfferta.ParamByName('xIDOfferta').asInteger:=Data2.TCliOfferteID.Value;
     StoricoOffertaForm.QStoricoOfferta.Open;
     StoricoOffertaForm.ShowModal;
     StoricoOffertaForm.Free;
end;

procedure TOffertaClienteForm.FormCreate(Sender: TObject);
begin
     xIDRicerca:=0;
end;

procedure TOffertaClienteForm.SpeedButton3Click(Sender: TObject);
begin
     ElencoRicClienteForm:=TElencoRicClienteForm.create(self);
     ElencoRicClienteForm.DBGrid7.DataSource:=ElencoRicClienteForm.DsQRicCliente;
     ElencoRicClienteForm.QRicCliente.ParamByName('xID').asInteger:=xIDCliente;
     ElencoRicClienteForm.QRicCliente.Open;
     ElencoRicClienteForm.ShowModal;
     if ElencoRicClienteForm.ModalResult=mrOK then begin
          xIDRicerca:=ElencoRicClienteForm.QRicClienteID.Value;
          ERifCommessa.Text:=ElencoRicClienteForm.QRicClienteProgressivo.Value;
          ETipo.Text:=ElencoRicClienteForm.QRicClienteTipo.Value;
     end;
     ElencoRicClienteForm.Free;
end;

procedure TOffertaClienteForm.SpeedButton2Click(Sender: TObject);
begin
     SelContrattoCliForm:=TSelContrattoCliForm.create(self);
     SelContrattoCliForm.QContrattiCli.ParambyName('xIDCliente').asInteger:=xIDCliente;
     SelContrattoCliForm.QContrattiCli.Open;
     if xIDContratto>0 then
          while SelContrattoCliForm.QContrattiCliID.value<>xIDContratto do
               SelContrattoCliForm.QContrattiCli.Next;
     SelContrattoCliForm.ShowModal;
     if SelContrattoCliForm.ModalResult=mrOK then begin
          xIDContratto:=SelContrattoCliForm.QContrattiCliID.value;
          EIDContratto.text:=SelContrattoCliForm.QContrattiCliCodice.asString;
     end else begin
          xIDContratto:=0;
          EIDContratto.text:='';
     end;
     SelContrattoCliForm.Free;
end;

procedure TOffertaClienteForm.DBTreeListGetImageIndex(Sender: TObject;
     Node: TdxTreeListNode; var Index: Integer);
begin
     if Node.level=0 then Index:=0
     else Index:=1;
end;

procedure TOffertaClienteForm.DBTreeListGetSelectedIndex(
     Sender: TObject; Node: TdxTreeListNode; var Index: Integer);
begin
     if Node.level=0 then Index:=0
     else Index:=1;
end;

procedure TOffertaClienteForm.DBTreeListEditing(Sender: TObject;
     Node: TdxTreeListNode; var Allow: Boolean);
begin
     if (Node.level=0)and(DBTreeList.FocusedField=DBTreeListTipo.Field) then
          Allow:=False;
     if (Node.level=0)and(DBTreeList.FocusedField=DBTreeListDescrizione.Field) then
          Allow:=False;
     if (Node.level=0)and(DBTreeList.FocusedField=DBTreeListImporto.Field) then
          Allow:=False;

     if (Node.level=1)and(DBTreeList.FocusedField=DBTreeListTipoCommessa.Field) then
          Allow:=False;
     if (Node.level=1)and(DBTreeList.FocusedField=DBTreeListRuolo.Field) then
          Allow:=False;
     if (Node.level=1)and(DBTreeList.FocusedField=DBTreeListNumRicercati.Field) then
          Allow:=False;
     if (Node.level=1)and(DBTreeList.FocusedField=DBTreeListCodice.Field) then
          Allow:=False;
     if (Node.level=1)and(DBTreeList.FocusedField=DBTreeListTitoloCliente.Field) then
          Allow:=False;
end;

procedure TOffertaClienteForm.DBTreeListCustomDrawCell(Sender: TObject;
     ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
     AColumn: TdxTreeListColumn; ASelected,AFocused,ANewItemRow: Boolean;
     var AText: string; var AColor: TColor; AFont: TFont;
     var AAlignment: TAlignment; var ADone: Boolean);
begin
     if Anode.level=0 then begin
          AColor:=clBtnFace;
          if AColumn=DBTreeListTipoCommessa then
               AFont.style:= [fsBold];
          if AColumn=DBTreeListImporto then
               AFont.Color:=clBtnface;
     end;
     if Anode.level=1 then begin
          if AColumn=DBTreeListTipoCommessa then begin
               //AColor:=clBtnFace;
               AText:=' -- dettaglio importo --';
          end;
          //if AColumn=dxDBTreeList1Ruolo then
          //   AColor:=clBtnFace;
          //if AColumn=dxDBTreeList1NumRicercati then
          //   AColor:=clBtnFace;
          if AColumn=DBTreeListImporto then
               AFont.style:= [fsBold];
     end;
end;

procedure TOffertaClienteForm.DBTreeListCustomDrawBand(Sender: TObject;
     ABand: TdxTreeListBand; ACanvas: TCanvas; ARect: TRect;
     var AText: string; var AColor: TColor; AFont: TFont;
     var AAlignment: TAlignment; var ADone: Boolean);
begin
     AColor:=clGray;
     AFont.Color:=clWindow;
end;

procedure TOffertaClienteForm.DBTreeListCustomDrawColumnHeader(
     Sender: TObject; AColumn: TdxTreeListColumn; ACanvas: TCanvas;
     ARect: TRect; var AText: string; var AColor: TColor; AFont: TFont;
     var AAlignment: TAlignment; var ASorted: TdxTreeListColumnSort;
     var ADone: Boolean);
begin
     AFont.name:='Arial';
     AFont.Style:= [fsItalic];
     AFont.color:=clNavy;
end;

procedure TOffertaClienteForm.BNodoNew1Click(Sender: TObject);
begin
     FParentValue:=-1;
     Q.SQL.text:='insert into EBC_OffertaDettaglio (IDOfferta,Parent,Accettata,Codice,IDMansione,NumRicercati) '+
          'values (:xIDOfferta,:xParent,:xAccettata,:xCodice,:xIDMansione,:xNumRicercati)';
     Q.ParamByName('xIDOfferta').asInteger:=xIDOfferta;
     Q.ParamByName('xParent').asInteger:=FParentValue;
     Q.ParamByName('xAccettata').asBoolean:=False;
     Q.ParamByName('xCodice').asString:=GetCodRicerca;
     Q.ParamByName('xIDMansione').asInteger:=0;
     Q.ParamByName('xNumRicercati').asInteger:=0;
     Q.ExecSQL;
     QDettOfferta.Close;
     QDettOfferta.Open;
     QDettOfferta.Last;
     DBTreeList.ShowEditor;
end;

procedure TOffertaClienteForm.BNodoNew2Click(Sender: TObject);
begin
     if DBTreeList.FocusedNode.Level=1 then
          FParentValue:=TdxDBTreeListNode(DBTreeList.FocusedNode.Parent).Id
     else FParentValue:=TdxDBTreeListNode(DBTreeList.FocusedNode).Id;
     Q.SQL.text:='insert into EBC_OffertaDettaglio (IDOfferta,Parent,TipoCommessa,Accettata) '+
          'values (:xIDOfferta,:xParent,:xTipoCommessa,:xAccettata)';
     Q.ParamByName('xIDOfferta').asInteger:=xIDOfferta;
     Q.ParamByName('xParent').asInteger:=FParentValue;
     Q.ParamByName('xTipoCommessa').asString:='';
     Q.ParamByName('xAccettata').asBoolean:=False;
     Q.ExecSQL;
     QDettOfferta.Close;
     QDettOfferta.Open;
     QDettOfferta.Last;
     DBTreeList.ShowEditor;
end;

procedure TOffertaClienteForm.BNodoDelClick(Sender: TObject);
begin
     if (DBTreeList.FocusedNode.Level=0)and(DBTreeList.FocusedNode.HasChildren) then begin
          MessageDlg('Ci sono sottonodi per il nodo selezionato: IMPOSSIBILE CANCELLARE',mtError, [mbOK],0);
          exit;
     end;
     if MessageDlg('Sei sicuro di voler eliminare il nodo selezionato ?',mtWarning, [mbNo,mbYes],0)=mrYes then begin
          QDettOfferta.Delete;
          QDettOfferta.Close;
          QDettOfferta.Open;
     end;
end;

procedure TOffertaClienteForm.DsQDettOffertaStateChange(Sender: TObject);
var b: boolean;
begin
     b:=DsQDettOfferta.State in [dsEdit,dsInsert];
     BNodoNew1.Enabled:=not b;
     BNodoNew2.Enabled:=not b;
     BNodoDel.Enabled:=not b;
     BNodoOK.Enabled:=b;
     BNodoCan.Enabled:=b;
end;

procedure TOffertaClienteForm.BitBtn1Click(Sender: TObject);
var i,k,w,xIDRicerca,xIDAnagUtente,xProvvDef,xIDContrattoCG,xIDPrimaRicerca: integer;
     xCodRicerca,xMess,xIDRicCreate: string;
     xProvv,xAlmenoUnaAccettata,xUltimaCreata,xCreaCompenso: boolean;
     xArrayIDTimeRep: array[1..100] of integer;
     xArrayIDRicCreate: array[1..100] of integer;
begin
     if DsQDettOfferta.state in [dsInsert,dsEdit] then QDettOfferta.Post;
     if DsQOffertaTimeReport.state in [dsInsert,dsEdit] then QOffertaTimeReport.Post;
     // offerta accettata (se prima non lo era...)
     if (CBStato.text='accettata')and(xStato<>'accettata') then begin
          ModalResult:=mrNone;

          // controllo almeno una accettazione
          QDettOfferta.First;
          xAlmenoUnaAccettata:=False;
          while not QDettOfferta.EOF do begin
               if QDettOffertaAccettata.Value then xAlmenoUnaAccettata:=True;
               QDettOfferta.Next;
          end;
          if not xAlmenoUnaAccettata then begin
               if MessageDlg('ATTENZIONE: nessuna commessa ipotetica � stata accettata.'+chr(13)+
                    'NON VERRA'' CREATA NESSUNA COMMESSA OPERATIVA !'+chr(13)+
                    'SE SICURO DI VOLER PROSEGUIRE ?',mtWarning, [mbYes,mbNO],0)=mrNo then begin
                    exit;
               end;
          end;

          // controllo coerenza linea di prodotto
          QDettOfferta.First;
          if not QDettOfferta.locate('TipoCommessa',QLineeProdottoLineaProdotto.Value, []) then begin
               if MessageDlg('ATTENZIONE: Possibile incoerenza con la LINEA DI PRODOTTO dell''offerta'+chr(13)+
                    'e il tipo di commessa inserito nelle commesse operative...'+chr(13)+
                    'SE SICURO DI VOLER PROSEGUIRE ?',mtWarning, [mbYes,mbNO],0)=mrNo then begin
                    exit;
               end;
          end;

          // implementazione utenti offerta
          with Data do begin
               DB.BeginTrans;
               try
                    Qtemp.Close;
                    Qtemp.SQl.text:='select distinct IDUtente from EBC_OfferteTimeSheet';
                    Qtemp.Open;
                    Qtemp.First;
                    while not Qtemp.EOF do begin
                         Q1.SQL.text:='insert into EBC_OfferteUtenti (IDOfferta,IDUtente) '+
                              'values (:xIDOfferta,:xIDUtente)';
                         Q1.ParambyName('xIDOfferta').asInteger:=Data2.TCliOfferteID.Value;
                         Q1.ParambyName('xIDUtente').asInteger:=QTemp.FieldByName('IDUtente').asInteger;
                         Q1.ExecSQL;
                         Qtemp.Next;
                    end;
                    Qtemp.Close;
                    DB.CommitTrans;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: operazione non effettuata',mtError, [mbOK],0);
               end;
          end;
          QOffertaUtenti.ParamByName('xIDOfferta').asinteger:=xIDOfferta;
          QOffertaUtenti.Open;

          if not QDettOfferta.IsEmpty then begin
               // richiesta conferma
               if MessageDlg('L''offerta � stata accettata.  Vuoi procedere alla creazione automatica delle commesse e dei compensi ?'+chr(13)+
                    'IMPORTANTE: in caso positivo si consideri che TUTTE le voci di dettaglio ACCETTATE verranno considerate.',mtWarning, [mbYes,mbNO],0)=mrNo then begin
                    if MessageDlg('Vuoi comunque confermare l''operazione e rendere l''offerta ACCETTATA',mtWarning, [mbYes,mbNO],0)=mrYes then
                         ModalResult:=mrOK;
                    exit;
               end;
               // richiesta conferma x time-sheet
               if MessageDlg('ATTENZIONE: Le ore imputate nel time-report verranno portate nelle commesse.'+chr(13)+
                    'Queste non saranno pi� modificabili. SE SICURO DI VOLER PROSEGUIRE ?',mtWarning, [mbYes,mbNO],0)=mrNo then begin
                    exit;
               end;

               // crea commesse e compensi
               QDettOfferta.First;
               xMess:='';
               with Data do begin
                    DB.BeginTrans;
                    try
                         for i:=1 to 100 do xArrayIDRicCreate[i]:=0;
                         i:=1;
                         k:=1;
                         while not QDettOfferta.EOF do begin
                              if QDettOffertaAccettata.Value then begin
                                   // dati sulla provvigione --> NO
                                   xProvv:=False;
                                   xProvvDef:=0;
                                   Q1.Close;
                                   if QDettOffertaTipoCommessa.Value<>'' then begin
                                        // se la commessa esiste gi�, allora SKIP
                                        Q2.Close;
                                        Q2.SQL.text:='select count(*) Tot from EBC_Ricerche where Progressivo=:xProgressivo';
                                        Q2.ParamByName('xProgressivo').asString:=QDettOffertaCodice.Value;
                                        Q2.Open;
                                        if Q2.FieldByName('Tot').asInteger=0 then begin
                                             // � una commessa
                                             Q1.SQL.Text:='insert into EBC_Ricerche (IDCliente,IDUtente,DataInizio,TitoloCliente,'+
                                                  'IDStatoRic,DallaData,IDMansione,IDArea,NumRicercati,Tipo,IDOfferta,Provvigione,ProvvigionePerc,QuandoPagamUtente) '+
                                                  '  values (:xIDCliente,:xIDUtente,:xDataInizio,:xTitoloCliente,'+
                                                  ':xIDStatoRic,:xDallaData,:xIDMansione,:xIDArea,:xNumRicercati,:xTipo,:xIDOfferta,:xProvvigione,:xProvvigionePerc,:xQuandoPagamUtente)';
                                             Q1.ParamByName('xIDCliente').asInteger:=Data2.TEBCClientiID.Value;
                                             // ATTENZIONE:  il selezionatore principale della commessa � il primo utente del TIME REPORT !!
                                             QOffertaUtenti.First;
                                             Q1.ParamByName('xIDUtente').asInteger:=MainForm.xIDUtenteAttuale;
                                             Q1.ParamByName('xDataInizio').asDateTime:=Date;
                                             Q1.ParamByName('xTitoloCliente').asString:=QDettOffertaTitoloCliente.Value;
                                             Q1.ParamByName('xIDStatoRic').asInteger:=1;
                                             Q1.ParamByName('xDallaData').asDateTime:=DEData.Date;
                                             Q1.ParamByName('xIDMansione').asInteger:=QDettOffertaIDMansione.value;
                                             Data.Qtemp.Close;
                                             Data.Qtemp.SQL.text:='select IDArea from Mansioni where ID='+QDettOffertaIDMansione.asString;
                                             Data.Qtemp.Open;
                                             Q1.ParamByName('xIDArea').asInteger:=Data.Qtemp.FieldByName('IDArea').asInteger;
                                             Data.Qtemp.Close;
                                             Q1.ParamByName('xNumRicercati').asInteger:=QDettOffertaNumRicercati.Value;
                                             Q1.ParamByName('xTipo').asString:=QDettOffertaTipoCommessa.Value;
                                             Q1.ParamByName('xIDOfferta').asInteger:=xIDOfferta;
                                             Q1.ParamByName('xProvvigione').asBoolean:=xProvv;
                                             Q1.ParamByName('xProvvigionePerc').asInteger:=xProvvDef;
                                             Q1.ParamByName('xQuandoPagamUtente').asString:='1'; // a default TIPO=1
                                             Q1.execSQL;
                                             // ID offerta
                                             Data.QTemp.Close;
                                             Data.QTemp.SQL.text:='select @@IDENTITY as LastID';
                                             Data.QTemp.Open;
                                             xIDRicerca:=Data.QTemp.FieldByName('LastID').asInteger;
                                             xArrayIDRicCreate[i]:=xIDRicerca;
                                             if k=1 then xIDPrimaRicerca:=xIDRicerca;
                                             Data.QTemp.Close;
                                             // progressivo ricerca = codice dett.offerta
                                             // se non c'� non � l'ID ma vai a prenderlo dal contatore
                                             Q1.SQL.text:='update EBC_Ricerche set Progressivo=:xProg where ID='+IntToStr(xIDRicerca);
                                             if (QDettOffertaCodice.Value<>'') then
                                                  q1.ParamByName('xProg').asString:=QDettOffertaCodice.Value
                                             else begin
                                                  xCodRicerca:=GetCodRicerca;
                                                  q1.ParamByName('xProg').asString:=xCodRicerca;
                                             end;
                                             Q1.ExecSQL;

                                             // riga in storico ricerca
                                             Q1.Close;
                                             Q1.SQL.text:='insert into EBC_StoricoRic (IDRicerca,IDStatoRic,DallaData,Note) '+
                                                  'values (:xIDRicerca,:xIDStatoRic,:xDallaData,:xNote)';
                                             Q1.ParamByName('xIDRicerca').asInteger:=xIDRicerca;
                                             Q1.ParamByName('xIDStatoRic').asInteger:=1;
                                             Q1.ParamByName('xDallaData').asDateTime:=Date;
                                             Q1.ParamByName('xNote').asString:='inizio ricerca';
                                             Q1.ExecSQL;
                                             // riga in ricerche-utenti con ruolo "capo commessa" (o comunque il primo)
                                             Q1.Close;
                                             Q1.SQL.text:='insert into EBC_RicercheUtenti (IDRicerca,IDUtente,IDRuoloRic) '+
                                                  'values (:xIDRicerca,:xIDUtente,:xIDRuoloRic)';
                                             Q1.ParamByName('xIDRicerca').asInteger:=xIDRicerca;
                                             Q1.ParamByName('xIDUtente').asInteger:=MainForm.xIDUtenteAttuale;
                                             Q1.ParamByName('xIDRuoloRic').asInteger:=1;
                                             Q1.ExecSQL;

                                             if (QDettOffertaCodice.Value<>'') then
                                                  xMess:=xMess+'Ricerca n� '+QDettOffertaCodice.Value+chr(13)
                                             else xMess:=xMess+'Ricerca n� '+xCodRicerca+' (codice attribuito dal sistema)'+chr(13);
                                        end;

                                   end else begin
                                        // inserisce il compenso se la commessa � creata (quindi � nell'array)
                                        xCreaCompenso:=False;
                                        for w:=1 to 100 do
                                             if xArrayIDRicCreate[w]=xIDRicerca then xCreaCompenso:=True;
                                        if (xCreaCompenso)and(xIDRicerca>0) then begin
                                             // � un compenso dell'ultima commessa inserita
                                             Q1.SQL.Text:='insert into EBC_CompensiRicerche (IDRicerca,Tipo,Importo,Note,IDOffertaDett) '+
                                                  '  values (:xIDRicerca,:xTipo,:xImporto,:xNote,:xIDOffertaDett)';
                                             Q1.ParamByName('xIDRicerca').asInteger:=xIDRicerca;
                                             Q1.ParamByName('xTipo').asString:=QDettOffertaTipo.Value;
                                             Q1.ParamByName('xImporto').asFloat:=QDettOffertaImporto.Value;
                                             Q1.ParamByName('xNote').asString:=QDettOffertaDescrizione.Value;
                                             Q1.ParamByName('xIDOffertaDett').asInteger:=QDettOffertaID.Value;
                                             Q1.execSQL;
                                        end;
                                   end;
                              end;
                              Q2.Close;
                              inc(k);
                              inc(i);
                              QDettOfferta.Next;
                         end;

                         // RIPORTO TIME REPORT (ATTENZIONE: solo prima commessa !!!)
                         QDettOfferta.First;
                         for i:=1 to 100 do xArrayIDTimeRep[i]:=0;
                         QOffertaTimeReport.First;
                         i:=1;
                         while not QOffertaTimeReport.EOF do begin
                              // se non esiste lo inserisce, altrimenti lo modifica
                              if QOffertaTimeReportIDRicTimeSheet.asString='' then begin
                                   Q1.SQL.text:='insert into EBC_RicercheTimeSheet (IDRicerca,IDUtente,IDGruppoProfess,Data,OreConsuntivo,Note) '+
                                        'values (:xIDRicerca,:xIDUtente,:xIDGruppoProfess,:xData,:xOreConsuntivo,:xNote)';
                                   Q1.ParambyName('xIDRicerca').asInteger:=xIDPrimaRicerca;
                                   Q1.ParambyName('xIDUtente').asInteger:=QOffertaTimeReportIDUtente.Value;
                                   Q1.ParambyName('xIDGruppoProfess').asInteger:=QOffertaTimeReportIDGruppoProfess.Value;
                                   Q1.ParambyName('xData').asDateTime:=QOffertaTimeReportData.Value;
                                   Q1.ParambyName('xOreConsuntivo').asFloat:=QOffertaTimeReportOreConsuntivo.Value;
                                   Q1.ParambyName('xNote').AsString:=QOffertaTimeReportNote.Value;
                                   Q1.ExecSQL;
                                   // IDTimeReport
                                   Data.QTemp.Close;
                                   Data.QTemp.SQL.text:='select @@IDENTITY as LastID';
                                   Data.QTemp.Open;
                                   xArrayIDTimeRep[i]:=Data.QTemp.FieldByName('LastID').asInteger;
                                   // aggiorna Time-rep. per l'offerta
                                   Q1.SQL.text:='update EBC_OfferteTimeSheet set IDRicTimeSheet=:xIDRicTimeSheet '+
                                        'where ID='+QOffertaTimeReportID.AsString;
                                   Q1.ParambyName('xIDRicTimeSheet').asInteger:=Data.QTemp.FieldByName('LastID').asInteger;
                                   Q1.ExecSQL;
                                   Data.QTemp.Close;

                                   inc(i);
                              end else begin
                                   Q1.SQL.text:='update EBC_RicercheTimeSheet set IDUtente=:xIDUtente,IDGruppoProfess=:xIDGruppoProfess,Data=:xData,'+
                                        'OreConsuntivo=:xOreConsuntivo,Note=:xNote '+
                                        'where ID='+QOffertaTimeReportIDRicTimeSheet.AsString;
                                   Q1.ParambyName('xIDUtente').asInteger:=QOffertaTimeReportIDUtente.Value;
                                   Q1.ParambyName('xIDGruppoProfess').asInteger:=QOffertaTimeReportIDGruppoProfess.Value;
                                   Q1.ParambyName('xData').asDateTime:=QOffertaTimeReportData.Value;
                                   Q1.ParambyName('xOreConsuntivo').asFloat:=QOffertaTimeReportOreConsuntivo.Value;
                                   Q1.ParambyName('xNote').AsString:=QOffertaTimeReportNote.Value;
                                   Q1.ExecSQL;
                              end;
                              QOffertaTimeReport.Next;
                         end;

                         DB.CommitTrans;
                    except
                         DB.RollbackTrans;
                         MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
                         raise;
                    end;
               end;
               // refresh commesse
               if DataRicerche.QRicAttive.Active then MainForm.EseguiQRicAttive;
               if xMess='' then
                    ShowMessage('NON SONO STATE CREATE COMMESSE OPERATIVE')
               else ShowMessage('SONO STATE CREATE LE SEGUENTI COMMESSE:'+chr(13)+xMess);
          end;

          // creazione contratto CONTROLLO DI GESTIONE (se abilitato)
          if copy(Data.GlobalCheckBoxIndici.Value,6,1)='1' then begin
               if MessageDlg('E'' stato rilevato il modulo di controllo di gestione:  vuoi creare il contratto-commessa per questa offerta ?'+chr(13)+
                    'Verr� anche riportato il Time Report registrato per questa offerta.',mtWarning, [mbYes,mbNO],0)=mrYes then begin

                    QDettOfferta.First;
                    with Data do begin
                         DB.BeginTrans;
                         try
                              // crea il contratto solo se non c'�
                              Q3.Close;
                              Q3.SQL.text:='select ID from CG_Contratti where CodiceContratto=:xCodiceContratto';
                              Q3.ParamByName('xCodiceContratto').asString:=ERif.Text;
                              Q3.Open;
                              if Q3.IsEmpty then begin
                                   Q1.SQL.text:='insert into CG_Contratti (CodiceContratto,Descrizione,Diretta,IDTipoContratto,IDStatoContratto,'+
                                        'IDCliente,IDCentroCosto,IDMansione,IDArea,IDUtenteResp,OrePreventivo,Tipologia,LineaProdotto,DataInizio,Note) '+
                                        'values (:xCodiceContratto,:xDescrizione,:xDiretta,:xIDTipoContratto,:xIDStatoContratto,'+
                                        ':xIDCliente,:xIDCentroCosto,:xIDMansione,:xIDArea,:xIDUtenteResp,:xOrePreventivo,:xTipologia,:xLineaProdotto,:xDataInizio,:xNote)';
                                   Q1.ParambyName('xCodiceContratto').asString:=ERif.Text;
                                   Q1.ParambyName('xDescrizione').asString:='';
                                   Q1.ParambyName('xDiretta').asBoolean:=True;
                                   Q1.ParambyName('xIDTipoContratto').asInteger:=0;
                                   Q1.ParambyName('xIDStatoContratto').asInteger:=1;
                                   Q1.ParambyName('xIDCliente').asInteger:=Data2.TEBCClientiID.Value;
                                   Q1.ParambyName('xIDCentroCosto').asInteger:=0;
                                   Q1.ParambyName('xIDMansione').asInteger:=QDettOffertaIDMansione.Value;
                                   Q1.ParambyName('xIDArea').asInteger:=xIDArea;
                                   Q1.ParambyName('xIDUtenteResp').asInteger:=xIDUtente;
                                   Q1.ParambyName('xOrePreventivo').asFloat:=0;
                                   Q1.ParambyName('xTipologia').asString:='';
                                   Q1.ParambyName('xLineaProdotto').asString:=QLineeProdottoLineaProdotto.Value;
                                   Q1.ParamByName('xDataInizio').asDateTime:=Date;
                                   Q1.ParamByName('xNote').asString:=ENote.text;
                                   Q1.ExecSQL;
                                   // ID contratto
                                   Data.QTemp.Close;
                                   Data.QTemp.SQL.text:='select @@IDENTITY as LastID';
                                   Data.QTemp.Open;
                                   xIDContrattoCG:=Data.QTemp.FieldByName('LastID').asInteger;
                                   Data.QTemp.Close;
                              end else xIDContrattoCG:=Q3.FieldbyName('ID').AsInteger;
                              Q3.Close;
                              // per tutte le ricerche create, imposta l'IDContrattoCG
                              Q1.SQL.Text:='update EBC_Ricerche set IDContrattoCG=:xIDContrattoCG '+
                                   '  where IDOfferta=:xIDOfferta';
                              Q1.ParamByName('xIDContrattoCG').asInteger:=xIDContrattoCG;
                              Q1.ParamByName('xIDOfferta').asInteger:=xIDOfferta;
                              Q1.execSQL;
                              // RIPORTO TIME REPORT con LINK a questo
                              QOffertaTimeReport.First;
                              i:=1;
                              while not QOffertaTimeReport.EOF do begin
                                   if QOffertaTimeReportIDRicTimeSheet.asString='' then begin
                                        Q1.SQL.text:='insert into CG_ContrattiTimeSheet (IDContratto,IDUtente,IDGruppoProfess,Data,OreConsuntivo,Note,IDTimeRepH1Sel) '+
                                             'values (:xIDContratto,:xIDUtente,:xIDGruppoProfess,:xData,:xOreConsuntivo,:xNote,:xIDTimeRepH1Sel)';
                                        Q1.ParambyName('xIDContratto').asInteger:=xIDContrattoCG;
                                        Q1.ParambyName('xIDUtente').asInteger:=QOffertaTimeReportIDUtente.Value;
                                        Q1.ParambyName('xIDGruppoProfess').asInteger:=QOffertaTimeReportIDGruppoProfess.Value;
                                        Q1.ParambyName('xData').asDateTime:=QOffertaTimeReportData.Value;
                                        Q1.ParambyName('xOreConsuntivo').asFloat:=QOffertaTimeReportOreConsuntivo.Value;
                                        Q1.ParambyName('xNote').AsString:=QOffertaTimeReportNote.Value;
                                        Q1.ParambyName('xIDTimeRepH1Sel').asInteger:=xArrayIDTimeRep[i];
                                        Q1.ExecSQL;
                                        inc(i);
                                   end else begin
                                        Q1.SQL.text:='update CG_ContrattiTimeSheet set IDUtente=:xIDUtente,IDGruppoProfess=:xIDGruppoProfess,Data=:xData,'+
                                             'OreConsuntivo=:xOreConsuntivo,Note=:xNote '+
                                             'where IDTimeRepH1Sel='+QOffertaTimeReportIDRicTimeSheet.asString;
                                        Q1.ParambyName('xIDUtente').asInteger:=QOffertaTimeReportIDUtente.Value;
                                        Q1.ParambyName('xIDGruppoProfess').asInteger:=QOffertaTimeReportIDGruppoProfess.Value;
                                        Q1.ParambyName('xData').asDateTime:=QOffertaTimeReportData.Value;
                                        Q1.ParambyName('xOreConsuntivo').asFloat:=QOffertaTimeReportOreConsuntivo.Value;
                                        Q1.ParambyName('xNote').AsString:=QOffertaTimeReportNote.Value;
                                        Q1.ExecSQL;
                                   end;
                                   QOffertaTimeReport.Next;
                              end;
                              // inserimento voci di ricavo come COMPENSI per le sole ricerche create
                              xIDRicCreate:='';
                              for i:=1 to 100 do begin
                                   if xArrayIDRicCreate[i]<>0 then
                                        xIDRicCreate:=xIDRicCreate+IntToStr(xArrayIDRicCreate[i])+',';
                              end;
                              xIDRicCreate:=copy(xIDRicCreate,1,Length(xIDRicCreate)-1);
                              if xIDRicCreate<>'' then begin
                                   Data.QTemp.Close;
                                   Data.QTemp.SQL.text:='select * from EBC_CompensiRicerche '+
                                        'where IDRicerca in ('+xIDRicCreate+')';
                                   Data.QTemp.Open;
                                   while not Data.QTemp.EOF do begin
                                        Q1.SQL.Text:='insert into CG_ContrattiRicavi (IDContratto,Tipo,ImportoPrev,Note,IDCompenso) '+
                                             '  values (:xIDContratto,:xTipo,:xImportoPrev,:xNote,:xIDCompenso)';
                                        Q1.ParamByName('xIDContratto').asInteger:=xIDContrattoCG;
                                        Q1.ParamByName('xTipo').asString:=Data.QTemp.FieldByName('Tipo').asString;
                                        Q1.ParamByName('xImportoPrev').asFloat:=Data.QTemp.FieldByName('Importo').AsFloat;
                                        Q1.ParamByName('xNote').asString:=Data.QTemp.FieldByName('Note').asString;
                                        Q1.ParamByName('xIDCompenso').asInteger:=Data.QTemp.FieldByName('ID').AsInteger;
                                        Q1.execSQL;
                                        Data.QTemp.Next;
                                   end;
                                   Data.QTemp.Close;
                              end;

                              DB.CommitTrans;
                         except
                              DB.RollbackTrans;
                              MessageDlg('ERRORE SUL DATABASE: operazione non effettuata',mtError, [mbOK],0);
                         end;
                    end;
                    ShowMessage('E'' STATO CREATO IL CONTRATTO CON CODICE = '+ERif.Text);
               end;
          end;
          // puoi uscire
          ModalResult:=mrOK;
     end;
end;

procedure TOffertaClienteForm.BNodoOKClick(Sender: TObject);
begin
     QDettOfferta.Post;
end;

procedure TOffertaClienteForm.BNodoCanClick(Sender: TObject);
begin
     QDettOfferta.Cancel;
end;

procedure TOffertaClienteForm.QDettOffertaAfterOpen(DataSet: TDataSet);
begin
     DBTreeList.FullExpand;
end;

procedure TOffertaClienteForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     // calcolo totale
     xImportoTotale:=0;
     QDettOfferta.First;
     while not QDettOfferta.EOF do begin
          xImportoTotale:=xImportoTotale+QDettOffertaImporto.Value;
          QDettOfferta.Next;
     end;
end;

procedure TOffertaClienteForm.QDettOffertaAfterPost(DataSet: TDataSet);
var xID: integer;
begin
     // se disabiliti "accettata" per una commessa, disabilita anche i suoi compensi "figli"
     if not QDettOffertaAccettata.Value then begin
          xID:=QDettOffertaID.Value;
          with Data.QTemp do begin
               Close;
               SQL.text:='update EBC_OffertaDettaglio set Accettata=0 where Parent='+QDettOffertaID.AsString;
               ExecSQL;
          end;
          QDettOfferta.Close;
          QDettOfferta.Open;
          QDettOfferta.Locate('ID',xID, []);
     end;
end;

procedure TOffertaClienteForm.QOffertaTimeReportAfterPost(DataSet: TDataSet);
var xID: integer;
begin
     with QOffertaTimeReport do begin
          Data.DB.BeginTrans;
          try
               ApplyUpdates;
               Data.DB.CommitTrans;
               xID:=QOffertaTimeReportID.Value;
               Close;
               Open;
               Locate('ID',xID, []);
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               raise;
          end;
          CommitUpdates;
     end;
end;

procedure TOffertaClienteForm.QOffertaTimeReportAfterInsert(
     DataSet: TDataSet);
begin
     QOffertaTimeReportIDOfferta.Value:=xIDOfferta;
end;

procedure TOffertaClienteForm.BTimeSheetNewClick(Sender: TObject);
begin
     QOffertaTimeReport.Insert;
end;

procedure TOffertaClienteForm.BTimeSheetDelClick(Sender: TObject);
begin
     if MessageDlg('Sei sicuro di voler eliminare la voce ?',mtWarning, [mbYes,mbNo],0)=mrYes then
          QOffertaTimeReport.Delete;
end;

procedure TOffertaClienteForm.BTimeSheetOKClick(Sender: TObject);
begin
     QOffertaTimeReport.Post;
end;

procedure TOffertaClienteForm.BTimeSheetCanClick(Sender: TObject);
begin
     QOffertaTimeReport.Cancel;
end;

procedure TOffertaClienteForm.DsQOffertaTimeReportStateChange(Sender: TObject);
var b: boolean;
begin
     b:=DsQOffertaTimeReport.State in [dsEdit,dsInsert];
     BTimeSheetNew.Enabled:=not b;
     BTimeSheetDel.Enabled:=not b;
     BTimeSheetOK.Enabled:=b;
     BTimeSheetCan.Enabled:=b;
end;

procedure TOffertaClienteForm.QOffertaTimeReportNewRecord(
     DataSet: TDataSet);
begin
     QOffertaTimeReportIDUtente.Value:=MainForm.xIDUtenteAttuale;
     QOffertaTimeReportData.Value:=Date;
     QOffertaTimeReportOreConsuntivo.Value:=0;
     QUsersLK.locate('ID',QOffertaTimeReportIDUtente.Value, []);
     QOffertaTimeReportIDGruppoProfess.Value:=QUsersLKIDGruppoProfess.Value;
end;

procedure TOffertaClienteForm.QOffertaTimeReportBeforePost(
     DataSet: TDataSet);
begin
     // controllo utente-data
{     Data.QTemp.Close;
     Data.QTemp.SQL.text:='select count(*) Tot from EBC_OfferteTimeSheet '+
          'where IDOfferta=:xIDOfferta and IDUtente=:xIDUtente and Data=:xData';
     Data.QTemp.ParamByName('xIDOfferta').asInteger:=xIDOfferta;
     Data.QTemp.ParamByName('xIDUtente').asInteger:=QOffertaTimeReportIDUtente.Value;
     Data.QTemp.ParamByName('xData').asDateTime:=QOffertaTimeReportData.Value;
     Data.QTemp.Open;
     if Data.QTemp.FieldByName('Tot').asInteger>0 then begin
          ShowMessage('Duplicazione utente-data - IMPOSSIBILE REGISTRARE');
          Abort;
     end;}
end;

procedure TOffertaClienteForm.BSelAreaClick(Sender: TObject);
begin
     SelAreaForm:=TSelAreaForm.create(self);
     SelAreaForm.ShowModal;
     if SelAreaForm.ModalResult=mrOK then begin
          xIDArea:=SelAreaForm.QAreeID.Value;
          EArea.text:=SelAreaForm.QAreeDescrizione.Value;
     end else begin
          xIDArea:=0;
          EArea.text:='';
     end;
     SelAreaForm.Free;
end;

procedure TOffertaClienteForm.dxDBGrid1Editing(Sender: TObject;
     Node: TdxTreeListNode; var Allow: Boolean);
begin
     Allow:=True;
     //if Node.Values[dxDBGrid1IDRicTimeSheet.Index]<>null then Allow:=False;
end;

procedure TOffertaClienteForm.dxDBGrid1CustomDrawCell(Sender: TObject;
     ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
     AColumn: TdxTreeListColumn; ASelected,AFocused,ANewItemRow: Boolean;
     var AText: string; var AColor: TColor; AFont: TFont;
     var AAlignment: TAlignment; var ADone: Boolean);
begin
     //if ANode.Values[dxDBGrid1IDRicTimeSheet.Index]<>null then begin
     //     AFont.Color:=clRed;
     //end;
end;

end.

