unit LegendaRic;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Buttons, RXSpin, Db, DBTables;

type
  TLegendaRicForm = class(TForm)
    BitBtn1: TBitBtn;
    Q: TQuery;
    Panel1: TPanel;
    Label6: TLabel;
    Label1: TLabel;
    Label3: TLabel;
    RxSpinEdit1: TRxSpinEdit;
    Panel2: TPanel;
    Label7: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    RxSpinEdit2: TRxSpinEdit;
    Panel3: TPanel;
    Label5: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Panel4: TPanel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    RxSpinEdit3: TRxSpinEdit;
    Panel5: TPanel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  LegendaRicForm: TLegendaRicForm;

implementation

uses ModuloDati;

{$R *.DFM}

procedure TLegendaRicForm.FormShow(Sender: TObject);
begin
     Caption:='[M/50] - '+Caption;
     Q.SQl.text:='select ggDiffDi,ggDiffUc,ggDiffCol from global ';
     Q.Open;
     RxSpinEdit1.value:=Q.FieldByname('ggDiffDi').asInteger;
     RxSpinEdit2.value:=Q.FieldByname('ggDiffUc').asInteger;
     RxSpinEdit3.value:=Q.FieldByname('ggDiffCol').asInteger;
     Q.Close;
end;

procedure TLegendaRicForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
        with Data do begin
             DB.BeginTrans;
             try
                Q1.Close;
                Q1.SQL.text:='update global set ggDiffDi=:xggDiffDi,ggDiffUc=:xggDiffUc,ggDiffCol=:xggDiffCol';
                Q1.ParamByName('xggDiffDi').asInteger:=RxSpinEdit1.AsInteger;
                Q1.ParamByName('xggDiffUc').asInteger:=RxSpinEdit2.AsInteger;
                Q1.ParamByName('xggDiffCol').asInteger:=RxSpinEdit3.AsInteger;
                Q1.ExecSQL;
                DB.CommitTrans;
             except
                   DB.RollbackTrans;
                   MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError,[mbOK],0);
             end;
        end;
end;

end.
