unit ValutazDip;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     Grids,DBGrids,TB97,Wall,DBCtrls,StdCtrls,ComCtrls,Mask,Buttons,
     ExtCtrls,TeEngine,Series,TeeProcs,Chart,DBChart,Db,DBTables;

type
     TValutazDipForm=class(TForm)
          Panel1: TPanel;
          Q1: TQuery;
          Q1IDMansione: TIntegerField;
          Q1IDAnagrafica: TIntegerField;
          Q1Descrizione: TStringField;
          Q1Tipologia: TStringField;
          Q1Peso: TSmallintField;
          Q1ValRichiesto: TIntegerField;
          Q1ValNome: TIntegerField;
          Q1Punteggio: TFloatField;
          DsQ1: TDataSource;
          QTot: TQuery;
          Q1IDCompetenza: TIntegerField;
          BitBtn20: TBitBtn;
          BitBtn1: TBitBtn;
          BitBtn2: TBitBtn;
          DsRuoli: TDataSource;
          PageControl1: TPageControl;
          TsDatiAnag: TTabSheet;
          TsCompetTutte: TTabSheet;
          Wallpaper6: TWallpaper;
          TbBCompDipNew: TToolbarButton97;
          TbBCompDipDel: TToolbarButton97;
          ToolbarButton971: TToolbarButton97;
          DBGrid14: TDBGrid;
          TSCompRuolo: TTabSheet;
          Wallpaper1: TWallpaper;
          ToolbarButton972: TToolbarButton97;
          Panel4: TPanel;
          DBChart3: TDBChart;
          BarSeries1: TBarSeries;
          Panel5: TPanel;
          DBGrid2: TDBGrid;
          DBChart4: TDBChart;
          BarSeries3: TBarSeries;
          BarSeries2: TLineSeries;
          TsTutteCar: TTabSheet;
          DBGrid19: TDBGrid;
          Wallpaper7: TWallpaper;
          TbBCarattDipNew: TToolbarButton97;
          TbBCarattDipDel: TToolbarButton97;
          Panel6: TPanel;
          DBEdit1: TDBEdit;
          Label1: TLabel;
          ToolbarButton974: TToolbarButton97;
          PanCompNo: TPanel;
          Panel8: TPanel;
          Panel7: TPanel;
          DBEdit12: TDBEdit;
          DBEdit14: TDBEdit;
          TAnagCompNonPoss: TTable;
          TAnagCompNonPossID: TAutoIncField;
          TAnagCompNonPossIDAnagrafica: TIntegerField;
          TAnagCompNonPossIDCompetenza: TIntegerField;
          TAnagCompNonPossCompetenza: TStringField;
          DBGrid1: TDBGrid;
          DsAnagCompNonPoss: TDataSource;
          ToolbarButton975: TToolbarButton97;
          Qtemp: TQuery;
          ToolbarButton976: TToolbarButton97;
          Panel2: TPanel;
          Label38: TLabel;
          Label49: TLabel;
          Label50: TLabel;
          GroupBox1: TGroupBox;
          Label6: TLabel;
          Label7: TLabel;
          Label8: TLabel;
          Label9: TLabel;
          DBEdit6: TDBEdit;
          DBEdit7: TDBEdit;
          DBEdit8: TDBEdit;
          DBEdit9: TDBEdit;
          GroupBox2: TGroupBox;
          Label4: TLabel;
          Label10: TLabel;
          Label32: TLabel;
          Label33: TLabel;
          DBEdit3: TDBEdit;
          DBEdit4: TDBEdit;
          DBEdit10: TDBEdit;
          DBEdit11: TDBEdit;
          DBLookupComboBox1: TDBLookupComboBox;
          DBEdit17: TDBEdit;
          DBEdit23: TDBEdit;
          Panel9: TPanel;
          Label46: TLabel;
          Label47: TLabel;
          Label48: TLabel;
          DBEdit13: TDBEdit;
          DBEdit15: TDBEdit;
          DBEdit16: TDBEdit;
          QTotTot: TIntegerField;
          TbBCarattDipMod: TToolbarButton97;
          Truoli: TQuery;
          TruoliID: TAutoIncField;
          TruoliDescrizione: TStringField;
          TCompRuolo: TQuery;
          TCompRuoloID: TAutoIncField;
          TCompRuoloIDCompetenza: TIntegerField;
          TCompRuoloIDMansione: TIntegerField;
          TCompRuoloPeso: TSmallintField;
          TCompRuoloOperatore: TStringField;
          TCompRuoloValore: TIntegerField;
          TCompRuoloIndispensabile: TBooleanField;
          TCompRuoloDescCompetenza: TStringField;
          QCompLK: TQuery;
          QCompLKID: TAutoIncField;
          QCompLKdescrizione: TStringField;
          procedure TbBCompDipNewClick(Sender: TObject);
          procedure TbBCompDipDelClick(Sender: TObject);
          procedure TbBCarattDipNewClick(Sender: TObject);
          procedure TbBCarattDipDelClick(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure ToolbarButton972Click(Sender: TObject);
          procedure BitBtn20Click(Sender: TObject);
          procedure ToolbarButton971Click(Sender: TObject);
          procedure BitBtn2Click(Sender: TObject);
          procedure TRuoliAfterScroll(DataSet: TDataSet);
          procedure ToolbarButton974Click(Sender: TObject);
          procedure ToolbarButton975Click(Sender: TObject);
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
          procedure BitBtn1Click(Sender: TObject);
          procedure ToolbarButton976Click(Sender: TObject);
          procedure TbBCarattDipModClick(Sender: TObject);
     private
          { Private declarations }
          xVecchioVal: integer;
     public
          { Public declarations }
     end;

var
     ValutazDipForm: TValutazDipForm;

implementation

uses Curriculum,ModuloDati,ModuloDati2,
     StoricoComp,RepSchedaVal,RuoloRif,NuovaComp,InsCompetenza,Main,
     InsCaratt;

{$R *.DFM}


procedure TValutazDipForm.TbBCompDipNewClick(Sender: TObject);
var i,xVal: integer;
     Vero: boolean;
begin
     if not MainForm.CheckProfile('020') then Exit;
     InsCompetenzaForm:=TInsCompetenzaForm.create(self);
     InsCompetenzaForm.RiempiGriglia;
     InsCompetenzaForm.ShowModal;
     if InsCompetenzaForm.ModalResult=mrOK then begin
          for i:=1 to InsCompetenzaForm.ASG1.RowCount-1 do begin
               InsCompetenzaForm.ASG1.GetCheckBoxState(0,i,Vero);
               if Vero then begin
                    with Data do begin
                         DB.BeginTrans;
                         try
                              Q1.Close;
                              Q1.SQL.text:='insert into CompetenzeAnagrafica (IDAnagrafica,IDCompetenza,Valore) '+
                                   'values (:xIDAnagrafica,:xIDCompetenza,:xValore)';
                              Q1.ParamByName('xIDAnagrafica').asInteger:=Data.TAnagraficaID.value;
                              Q1.ParamByName('xIDCompetenza').asInteger:=InsCompetenzaForm.xArrayIDComp[i];
                              Q1.ParamByName('xValore').asInteger:=InsCompetenzaForm.xArrayCompVal[i];
                              Q1.ExecSQL;

                              Q1.SQL.text:='insert into StoricoCompAnag (IDCompetenza,IDAnagrafica,DallaData,MotivoAumento,valore) '+
                                   'values (:xIDCompetenza,:xIDAnagrafica,:xDallaData,:xMotivoAumento,:xvalore)';
                              Q1.ParamByName('xIDCompetenza').asInteger:=InsCompetenzaForm.xArrayIDComp[i];
                              Q1.ParamByName('xIDAnagrafica').asInteger:=Data.TAnagraficaID.value;
                              Q1.ParamByName('xDallaData').asDateTime:=Date;
                              Q1.ParamByName('xMotivoAumento').asString:='valore iniziale';
                              Q1.ParamByName('xValore').asInteger:=InsCompetenzaForm.xArrayCompVal[i];
                              Q1.ExecSQL;

                              DB.CommitTrans;
                              Data2.TCompDipendente.Close;
                              Data2.TCompDipendente.Open;
                         except
                              DB.RollbackTrans;
                              MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
                         end;
                    end;
               end;
          end;

     end;
     InsCompetenzaForm.Free;

     Q1.Close;
     Q1.Open;
     QTot.Close;
     QTot.Open;
     if TAnagCompNonPoss.FindKey([Data2.TCompDipendenteIDAnagrafica.Value,Data2.TCompDipendenteIDCompetenza.Value]) then begin
          TAnagCompNonPoss.Delete;
          if TAnagCompNonPoss.RecordCount=0 then begin
               PanCompNo.Visible:=False;
               DBChart3.Visible:=True;
          end;
     end;
end;

procedure TValutazDipForm.TbBCompDipDelClick(Sender: TObject);
begin
     if not MainForm.CheckProfile('021') then Exit;
     if Data2.TCompDipendente.RecordCount>0 then
          if MessageDlg('Sei sicuro di voler cancellare questa competenza ?',mtWarning,
               [mbNo,mbYes],0)=mrYes then begin
               with Data do begin
                    DB.BeginTrans;
                    try
                         Q1.Close;
                         Q1.SQL.text:='delete from CompetenzeAnagrafica '+
                              'where ID='+Data2.TCompDipendenteID.asString;
                         Q1.ExecSQL;
                         DB.CommitTrans;
                    except
                         DB.RollbackTrans;
                         MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
                    end;
               end;
               Data2.TCompDipendente.Close;
               Data2.TCompDipendente.Open;
          end;

     Q1.Close;
     Q1.Open;
     QTot.Close;
     QTot.Open;
end;

procedure TValutazDipForm.TbBCarattDipNewClick(Sender: TObject);
begin
     if not MainForm.CheckProfile('033') then Exit;
     InsCarattForm:=TInsCarattForm.create(self);
     InsCarattForm.ShowModal;
     if InsCarattForm.Modalresult=mrOK then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text:='insert into AnagCaratteristiche (IDAnagrafica,IDCaratt) '+
                         'values (:xIDAnagrafica,:xIDCaratt)';
                    Q1.ParamByName('xIDAnagrafica').asInteger:=Data.TAnagraficaID.Value;
                    Q1.ParamByName('xIDCaratt').asInteger:=InsCarattForm.TCarattID.Value;
                    Q1.ExecSQL;
                    DB.CommitTrans;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               end;
          end;
          Data2.TCarattDip.Close;
          Data2.TCarattDip.Open;
     end;
     InsCarattForm.Free;
end;

procedure TValutazDipForm.TbBCarattDipDelClick(Sender: TObject);
begin
     if not MainForm.CheckProfile('034') then Exit;
     if Data2.TCarattDip.RecordCount=0 then exit;
     if MessageDlg('Sei sicuro di voler cancellare ?',mtWarning,
          [mbNo,mbYes],0)=mrYes then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text:='delete from AnagCaratteristiche '+
                         'where ID='+Data2.TCarattDipID.asString;
                    Q1.ExecSQL;
                    DB.CommitTrans;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               end;
          end;
          Data2.TCarattDip.Close;
          Data2.TCarattDip.Open;
     end;
end;

procedure TValutazDipForm.FormShow(Sender: TObject);
begin
     Caption:='[M/052] - '+Caption;
     PageControl1.ActivePage:=TSCompRuolo;
     Data2.TCarattDip.Open;
     Data2.TCompDipendente.Open;
     if Uppercase(copy(Data.GlobalNomeAzienda.Value,1,6))='BOYDEN' then begin
          // tutti i wallpaper senza il marmo
          WallPaper1.Wallpaper:=nil; WallPaper1.caption:='';
          WallPaper6.Wallpaper:=nil; WallPaper6.caption:='';
          WallPaper7.Wallpaper:=nil; WallPaper7.caption:='';
     end;
end;

procedure TValutazDipForm.ToolbarButton972Click(Sender: TObject);
var xVal: string;
     xVecchioVal,xVarPerc: integer;
begin
     if not Q1.EOF then begin
          NuovaCompForm:=TNuovaCompForm.create(self);
          NuovaCompForm.xComp:=Q1Descrizione.Value;
          NuovaCompForm.SEVecchioVal.Value:=Q1ValNome.asInteger;
          NuovaCompForm.SENuovoVal.Value:=Q1ValNome.asInteger;
          NuovaCompForm.xMot:='a seguito di valutazione';
          NuovaCompForm.DEData.Date:=Date;
          NuovaCompForm.ShowModal;
          if NuovaCompForm.ModalResult=mrOK then begin
               with Data do begin
                    DB.BeginTrans;
                    try
                         Q1.Close;
                         Q1.SQL.text:='update CompetenzeAnagrafica set Valore=:xValore '+
                              'where IDAnagrafica=:xIDAnag and IDCompetenza=:xIDComp';
                         Q1.ParamByName('xValore').asInteger:=NuovaCompForm.SENuovoVal.Value;
                         Q1.ParambyName('xIDAnag').asInteger:=Data.TAnagraficaID.Value;
                         Q1.ParambyName('xIDComp').asInteger:=TCompRuoloIDCompetenza.Value;
                         Q1.ExecSQL;

                         xVecchioVal:=NuovaCompForm.SEVecchioVal.Value;
                         if xVecchioVal>0 then
                              xVarPerc:=Trunc(((NuovaCompForm.SENuovoVal.Value-xVecchioVal)/xVecchioVal)*100)
                         else xVarPerc:=NuovaCompForm.SENuovoVal.Value*100;

                         Q1.SQL.text:='insert into StoricoCompAnag (IDCompetenza,IDAnagrafica,DallaData,MotivoAumento,valore,VariazPerc) '+
                              'values (:xIDCompetenza,:xIDAnagrafica,:xDallaData,:xMotivoAumento,:xvalore,:xVariazPerc)';
                         Q1.ParamByName('xIDCompetenza').asInteger:=Q1IDCompetenza.Value;
                         Q1.ParamByName('xIDAnagrafica').asInteger:=Q1IDAnagrafica.Value;
                         Q1.ParamByName('xDallaData').asDateTime:=NuovaCompForm.DEData.Date;
                         Q1.ParamByName('xMotivoAumento').asString:=NuovaCompForm.xMot;
                         Q1.ParamByName('xValore').asInteger:=Q1ValNome.asInteger;
                         Q1.ParamByName('xVariazPerc').asInteger:=xVarPerc;
                         Q1.ExecSQL;

                         DB.CommitTrans;
                    except
                         DB.RollbackTrans;
                         MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
                    end;
               end;

               Q1.Close;
               Q1.Open;
               QTot.Close;
               QTot.Open;
               Data2.TCompDipendente.Close;
               Data2.TCompDipendente.Open;
          end;
          NuovaCompForm.Free;
     end;
end;

procedure TValutazDipForm.BitBtn20Click(Sender: TObject);
var x: string;
     i: integer;
begin
     Data.TTitoliStudio.Open;
     Data.TCorsiExtra.Open;
     Data.TLingueConosc.Open;
     Data.TEspLav.Open;
     CurriculumForm.ShowModal;
     Data.TTitoliStudio.Close;
     Data.TCorsiExtra.Close;
     Data.TLingueConosc.Close;
     Data.TEspLav.Close;
end;

procedure TValutazDipForm.ToolbarButton971Click(Sender: TObject);
begin
     if Data2.TCompDipendente.RecordCount>0 then begin
          StoricoCompForm:=TStoricoCompForm.create(self);
          StoricoCompForm.ShowModal;
          StoricoCompForm.Free;
     end;
end;

procedure TValutazDipForm.BitBtn2Click(Sender: TObject);
begin
     QRSchedaVal:=TQRSchedaVal.create(self);
     try QRSchedaVal.Preview except
          ShowMessage('problemi con il Report') end;
     QRSchedaVal.Free;
end;

procedure TValutazDipForm.TRuoliAfterScroll(DataSet: TDataSet);
begin
     DBChart3.RefreshData;
     DBChart4.RefreshData;
end;

procedure TValutazDipForm.ToolbarButton974Click(Sender: TObject);
var xHeight: integer;
begin
     RuoloRifForm:=TRuoloRifForm.create(self);
     RuoloRifForm.ShowModal;
     if TCompRuolo.RecordCount=0 then
          MessageDlg('Nessuna competenza richiesta da questo ruolo',mtWarning, [mbOK],0)
     else begin
          if TCompRuolo.RecordCount>Q1.RecordCount then begin
               Qtemp.SQL.clear;
               Qtemp.SQL.Add('delete from AnagCompNonPoss');
               Qtemp.ExecSQL;
               TAnagCompNonPoss.Close;
               TAnagCompNonPoss.Open;
               TCompRuolo.First;
               xHeight:=30;
               while not TCompRuolo.EOF do begin
                    Data.QTemp.Close;
                    Data.QTemp.SQL.text:='select count(ID) Tot from CompetenzeAnagrafica where IDAnagrafica=:xIDAnag and IDCompetenza=:xIDComp';
                    Data.QTemp.ParambyName('xIDAnag').asInteger:=Data.TAnagraficaID.Value;
                    Data.QTemp.ParambyName('xIDComp').asInteger:=TCompRuoloIDCompetenza.Value;
                    Data.QTemp.Open;
                    if Data.QTemp.FieldByName('Tot').asInteger=0 then begin
                         TAnagCompNonPoss.InsertRecord([Data.TAnagraficaID.Value,TCompRuoloIDCompetenza.Value]);
                         xHeight:=xHeight+16;
                    end;
                    TCompRuolo.Next;
               end;
               Data.QTemp.Close;
               MessageDlg('Le competenze richieste dal ruolo sono di pi� di quelle possedute',mtWarning, [mbOK],0);
               PanCompNo.Height:=xHeight;
               PanCompNo.Visible:=True;
               DBChart3.Visible:=False;
          end else begin
               PanCompNo.Visible:=False;
               DBChart3.Visible:=True;
          end;
          RuoloRifForm.Free;
     end;
end;

procedure TValutazDipForm.ToolbarButton975Click(Sender: TObject);
var xVal: string;
     xValInt: integer;
begin
     InputQuery('inserimento competenza','valore: ',xVal);
     xValInt:=StrToIntDef(xVal,0);
     with Data do begin
          DB.BeginTrans;
          try
               Q1.Close;
               Q1.SQL.text:='insert into CompetenzeAnagrafica (IDAnagrafica,IDCompetenza,Valore) '+
                    'values (:xIDAnagrafica,:xIDCompetenza,:xValore)';
               Q1.ParamByName('xIDAnagrafica').asInteger:=Data.TAnagraficaID.value;
               Q1.ParamByName('xIDCompetenza').asInteger:=TAnagCompNonPossIDCompetenza.Value;
               Q1.ParamByName('xValore').asInteger:=xValInt;
               Q1.ExecSQL;

               Q1.SQL.text:='insert into StoricoCompAnag (IDCompetenza,IDAnagrafica,DallaData,MotivoAumento,valore) '+
                    'values (:xIDCompetenza,:xIDAnagrafica,:xDallaData,:xMotivoAumento,:xvalore)';
               Q1.ParamByName('xIDCompetenza').asInteger:=TAnagCompNonPossIDCompetenza.Value;
               Q1.ParamByName('xIDAnagrafica').asInteger:=Data.TAnagraficaID.value;
               Q1.ParamByName('xDallaData').asDateTime:=Date;
               Q1.ParamByName('xMotivoAumento').asString:='valore iniziale';
               Q1.ParamByName('xValore').asInteger:=xValInt;
               Q1.ExecSQL;

               DB.CommitTrans;

               Q1.Close;
               Q1.Open;
               QTot.Close;
               QTot.Open;
               Data2.TCompDipendente.Close;
               Data2.TCompDipendente.Open;
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
          end;
     end;

     if Data2.TCompDipendente.Active then Data2.TCompDipendente.Open;
     TAnagCompNonPoss.Delete;
     DBChart4.RefreshData;
     if TAnagCompNonPoss.RecordCount=0 then begin
          PanCompNo.Visible:=False;
          DBChart3.RefreshData;
          DBChart3.Visible:=True;
     end;
end;

procedure TValutazDipForm.FormClose(Sender: TObject;
     var Action: TCloseAction);
begin
     Data2.TCarattDip.Close;
     Data2.TCompDipendente.Open;
end;

procedure TValutazDipForm.BitBtn1Click(Sender: TObject);
begin
     close
end;

procedure TValutazDipForm.ToolbarButton976Click(Sender: TObject);
var xData: string;
     xVecchioVal,xVarPerc: integer;
begin
     if not MainForm.CheckProfile('022') then Exit;
     if Data2.TCompDipendente.RecordCount=0 then exit;
     NuovaCompForm:=TNuovaCompForm.create(self);
     NuovaCompForm.xComp:=Data2.TCompDipendenteDescCompetenza.Value;
     NuovaCompForm.SEVecchioVal.Value:=Data2.TCompDipendenteValore.asInteger;
     NuovaCompForm.SENuovoVal.Value:=Data2.TCompDipendenteValore.asInteger;
     NuovaCompForm.xMot:='a seguito di valutazione';
     NuovaCompForm.DEData.Date:=Date;
     NuovaCompForm.ShowModal;
     if NuovaCompForm.ModalResult=mrOK then begin
          Data.DB.BeginTrans;
          try
               // se il valore � cambiato ==> aggiorna storico
               if NuovaCompForm.SENuovoVal.Value<>NuovaCompForm.SEVecchioVal.Value then begin
                    xVecchioVal:=NuovaCompForm.SEVecchioVal.Value;
                    if xVecchioVal>0 then
                         xVarPerc:=Trunc(((Data2.TCompDipendenteValore.Value-xVecchioVal)/xVecchioVal)*100)
                    else xVarPerc:=Data2.TCompDipendenteValore.Value*100;

                    Data.Q1.SQL.text:='insert into StoricoCompAnag (IDCompetenza,IDAnagrafica,DallaData,MotivoAumento,valore,VariazPerc) '+
                         'values (:xIDCompetenza,:xIDAnagrafica,:xDallaData,:xMotivoAumento,:xvalore,:xVariazPerc)';
                    Data.Q1.ParamByName('xIDCompetenza').asInteger:=Data2.TCompDipendenteIDCompetenza.Value;
                    Data.Q1.ParamByName('xIDAnagrafica').asInteger:=Data2.TCompDipendenteIDAnagrafica.Value;
                    Data.Q1.ParamByName('xDallaData').asDateTime:=NuovaCompForm.DEData.Date;
                    Data.Q1.ParamByName('xMotivoAumento').asString:=NuovaCompForm.xMot;
                    Data.Q1.ParamByName('xValore').asInteger:=NuovaCompForm.SENuovoVal.Value; //nuovo valore !!
                    Data.Q1.ParamByName('xVariazPerc').asInteger:=xVarPerc;
                    Data.Q1.ExecSQL;
               end;
               // aggiornamento CompetenzeAnagrafica
               Data.Q1.SQL.text:='update CompetenzeAnagrafica set Valore=:xValore '+
                    'where ID='+Data2.TCompDipendenteID.asString;
               Data.Q1.ParamByName('xValore').asInteger:=NuovaCompForm.SENuovoVal.Value;
               Data.Q1.ExecSQL;

               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
          end;

          Data2.TCompDipendente.Close;
          Data2.TCompDipendente.Open;
     end;
     NuovaCompForm.Free;
end;

procedure TValutazDipForm.TbBCarattDipModClick(Sender: TObject);
var xPunti: string;
begin
     if not MainForm.CheckProfile('035') then Exit;
     if Data2.TCarattDip.RecordCount=0 then exit;
     xPunti:=Data2.TCarattDipPunteggio.asString;
     if inputquery('Modifica punteggio caratteristica','Nuovo punteggio:',xPunti) then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text:='update AnagCaratteristiche set Punteggio=:xPunteggio '+
                         'where ID='+Data2.TCarattDipID.asString;
                    Q1.ParamByName('xPunteggio').asInteger:=StrToIntDef(xPunti,0);
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    Data2.TCarattDip.Close;
                    Data2.TCarattDip.Open;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               end;
          end;

     end;
end;

end.

