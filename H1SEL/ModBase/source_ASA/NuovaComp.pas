unit NuovaComp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, DtEdit97, Mask, DBCtrls, Db, DBTables, Spin;

type
  TNuovaCompForm = class(TForm)
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    Label1: TLabel;
    EMot: TEdit;
    DEData: TDateEdit97;
    Label2: TLabel;
    Panel2: TPanel;
    EComp: TEdit;
    Label6: TLabel;
    Label7: TLabel;
    SEVecchioVal: TSpinEdit;
    Label8: TLabel;
    SENuovoVal: TSpinEdit;
    BitBtn2: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    xMot,xComp:string;
  end;

var
  NuovaCompForm: TNuovaCompForm;

implementation


{$R *.DFM}

procedure TNuovaCompForm.FormShow(Sender: TObject);
begin
     EMot.Text:=xMot;
     EComp.Text:=xComp;
end;

procedure TNuovaCompForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     xMot:=EMot.Text;
end;

end.
