unit SelContrattoCli;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, Db, DBTables, StdCtrls, Buttons;

type
  TSelContrattoCliForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    QContrattiCli: TQuery;
    DsQContrattiCli: TDataSource;
    DBGrid1: TDBGrid;
    QContrattiCliID: TAutoIncField;
    QContrattiCliIDCliente: TIntegerField;
    QContrattiCliData: TDateTimeField;
    QContrattiCliDescrizione: TStringField;
    QContrattiCliStato: TStringField;
    QContrattiCliCodice: TStringField;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SelContrattoCliForm: TSelContrattoCliForm;

implementation

{$R *.DFM}

procedure TSelContrattoCliForm.FormShow(Sender: TObject);
begin
     Caption:='[S/37] - '+Caption;
end;

end.
