unit SelCliente;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Grids, DBGrids, Db, DBTables, StdCtrls, Buttons;

type
  TSelClienteForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Panel2: TPanel;
    DsClienti: TDataSource;
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    EDesc: TEdit;
    Label1: TLabel;
    TClienti: TQuery;
    TClientiID: TAutoIncField;
    TClientiStato: TStringField;
    RGFiltro: TRadioGroup;
    TClientiComune: TStringField;
    TClientiProvincia: TStringField;
    BitBtn3: TBitBtn;
    CBTestoCon: TCheckBox;
    TClientiDescrizione: TStringField;
    procedure DBGrid1DblClick(Sender: TObject);
    procedure EDescChange(Sender: TObject);
    procedure RGFiltroClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure CBTestoConClick(Sender: TObject);
  private
    xstringa:string;
  public
    { Public declarations }
  end;

var
  SelClienteForm: TSelClienteForm;

implementation

uses InsCliente, ModuloDati, Main;

{$R *.DFM}

procedure TSelClienteForm.DBGrid1DblClick(Sender: TObject);
begin
     BitBtn1.Click;
end;

procedure TSelClienteForm.EDescChange(Sender: TObject);
var xstato:string;
begin
     TClienti.Close;
     TClienti.SQL.text:='select ID,Descrizione,Stato,Comune,Provincia from EBC_Clienti where Descrizione like :xDesc';
     if RGFiltro.ItemIndex>0 then TClienti.SQL.Add('and stato=:xStato');
     case RGFiltro.Itemindex of
     1: xStato:='attivo';
     2: xStato:='non attivo';
     3: xStato:='contattato';
     4: xStato:='eliminato';
     5: xStato:='azienda esterna';
     end;
     TClienti.SQL.Add('order by Descrizione');
     if RGFiltro.ItemIndex>0 then TClienti.ParamByName('xStato').asString:=xStato;
     if CBTestoCon.Checked then
        TClienti.ParamByName('xDesc').asString:='%'+EDesc.text+'%'
     else TClienti.ParamByName('xDesc').asString:=EDesc.text+'%';
     TClienti.Open;
end;

procedure TSelClienteForm.RGFiltroClick(Sender: TObject);
begin
     EDescChange(self);
end;

procedure TSelClienteForm.FormShow(Sender: TObject);
begin
     Caption:='[S/3] - '+Caption;
end;

procedure TSelClienteForm.BitBtn3Click(Sender: TObject);
var xDenom: string;
begin
     //if not MainForm.CheckProfile('100') then Exit;
     InsClienteForm:=TInsClienteForm.create(self);
     InsClienteForm.DEDataCon.date:=Date;
     InsClienteForm.CBStato.text:='azienda esterna';
     InsClienteForm.ShowModal;
     if InsClienteForm.ModalResult=mrOK then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text:='insert into EBC_Clienti (Descrizione,Stato,IDAttivita,ConosciutoInData) '+
                         'values (:xDescrizione,:xStato,:xIDAttivita,:xConosciutoInData)';
                    Q1.ParamByName('xDescrizione').asString:=InsClienteForm.EDenominazione.text;
                    Q1.ParamByName('xStato').asString:=InsClienteForm.CBStato.text;
                    Q1.ParamByName('xIDAttivita').asInteger:=InsClienteForm.xIDAttivita;
                    Q1.ParamByName('xConosciutoInData').asDateTime:=InsClienteForm.DEDataCon.Date;
                    Q1.ExecSQL;
                    DB.CommitTrans;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               end;
          end;
     end;
     // posizionati su questo nuovo cliente
     EDesc.text:=InsClienteForm.EDenominazione.text;
     InsClienteForm.Free;
     EDescChange(self);
end;

procedure TSelClienteForm.CBTestoConClick(Sender: TObject);
begin
     EDescChange(self);
end;

end.
