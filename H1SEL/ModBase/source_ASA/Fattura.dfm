�
 TFATTURAFORM 0�E  TPF0TFatturaFormFatturaFormLeft� TopoBorderStylebsDialogCaptionFattura (o nota di accredito)ClientHeight%ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnClose	FormCloseOnShowFormShowPixelsPerInch`
TextHeight TPanelPanel1Left Top Width�Height,AlignalTopTabOrder  TBitBtnBitBtn1Left_TopWidthcHeight!CaptionOK-EsciTabOrder OnClickBitBtn1ClickKindbkOK  TBitBtnBitBtn2Left^TopWidth|Height!CaptionStampa con WordTabOrderOnClickBitBtn2Click
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 0      ?��������������wwwwwww�������wwwwwww        ���������������wwwwwww�������wwwwwww�������wwwwww        wwwwwww30����337���?330� 337�wss330����337��?�330�  337�swws330���3337��73330��3337�ss3330�� 33337��w33330  33337wws333	NumGlyphs  	TCheckBoxCBFronteLeft�TopWidthkHeightCaptionPagina copertinaTabOrder  	TCheckBoxCBNoteSpeseLeft�TopWidthyHeightCaptionallegato note speseTabOrder   TPanelPanel2Left TopBWidth�Height� AlignalTop
BevelOuter	bvLoweredTabOrder TLabelLabel1LeftTopWidth%HeightCaptionNumeroFocusControlDBEdit1  TLabelLabel2Left3TopWidthHeightCaptionTipo  TLabelLabel4LeftTopWidth HeightCaptionCliente  TLabelLabel5Left� TopWidthHeightCaptionData  TLabelLabel142LeftTop2WidthkHeightCaptionModalit� di pagamento  TLabelLabel6Left)Top2WidthZHeightCaptionAppoggio Bancario  TLabelLabel7LeftTop\Width7HeightCaption
Decorrenza  TLabelLabel8LeftdTop\Width0HeightCaptionScadenza  TLabelLabel9LeftMTop\Width)HeightCaption	Pagata il  TLabelLabel10Left� Top\Width@HeightCaptionResponsabileFocusControlDBEdit4  TDBEditDBEdit1LeftTopWidth)Height	DataFieldProgressivo
DataSource	DsFatturaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder   TDBComboBoxDBComboBox1Left3TopWidth� Height	DataFieldTipo
DataSource	DsFattura
ItemHeightItems.StringsFatturaNota di accredito TabOrder  TDBEditDBEdit3LeftTopWidthLHeightTabStopColorclAqua	DataFieldDescrizione
DataSource	DsFatturaTabOrder  TDbDateEdit97DbDateEdit971Left� TopWidthXHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderColorCalendar.ColorValidclBlueDate      ��@DayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanPopup
doIsMaskeddoShowCanceldoShowToday 	DataFieldData
DataSource	DsFattura  TDBComboBoxDBComboBox3LeftTopBWidthHeight	DataFieldModalitaPagamento
DataSource	DsFattura
ItemHeightItems.Strings$Rimessa Diretta - scadenza immediata!Rimessa Diretta - scadenza 30 gg.!Rimessa Diretta - scadenza 60 gg.!Rimessa Diretta - scadenza 90 gg.+Ricevuta Bancaria - scadenza 30 gg. DF f.m.+Ricevuta Bancaria - scadenza 60 gg. DF f.m.+Ricevuta Bancaria - scadenza 90 gg. DF f.m..Ricevuta Bancaria - scadenza 30/60 gg. DF f.m..Ricevuta Bancaria - scadenza 60/90 gg. DF f.m.come da offerta TabOrderOnChangeDBComboBox3Change  TDbDateEdit97DbDateEdit972LeftcToplWidthXHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderColorCalendar.ColorValidclBlueDate      ��@DayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanPopup
doIsMaskeddoShowCanceldoShowToday 	DataFieldScadenzaPagamento
DataSource	DsFattura  TDbDateEdit97DbDateEdit973LeftToplWidthXHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderColorCalendar.ColorValidclBlueDate      ��@DayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanPopup
doIsMaskeddoShowCanceldoShowToday 	DataField
Decorrenza
DataSource	DsFattura  TDbDateEdit97DbDateEdit974LeftLToplWidthXHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderColorCalendar.ColorValidclBlueDate      ��@DayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanPopup
doIsMaskeddoShowCanceldoShowToday 	DataFieldPagataInData
DataSource	DsFattura  	TGroupBox	GBRifProgLeftbTopWidthAHeight-CaptionRif.Fatt.N�TabOrder	 TDBEditDBEdit2LeftTopWidth1Height	DataFieldRifProg
DataSource	DsFatturaTabOrder    TDBComboBoxDBCBAppoggioLeft(TopBWidth|Height	DataFieldAppoggioBancario
DataSource	DsFattura
ItemHeightTabOrder
  TDBEditDBEdit4Left� ToplWidth!Height	DataFieldResponsabile
DataSource	DsFattura	MaxLengthPTabOrder  TDBCheckBoxDBCBFlagIVALeftTop� WidthBHeightCaption=Fuori campo applicazione IVA (art.7, co.4, lett.D DPR 633/72)	DataFieldFlagFuoriAppIVA63372
DataSource	DsFatturaTabOrderValueCheckedTrueValueUncheckedFalse   TPanelPanel3Left Top,Width�HeightAlignalTop	AlignmenttaLeftJustifyCaption  Dati fatturaColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TPanelPanel4Left Top� Width�HeightAlignalTop	AlignmenttaLeftJustifyCaption  Dettaglio fatturaColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TPanelPanel5Left TopWidth�Height$AlignalBottomTabOrder TLabelLabel3Left�Top
Width:HeightCaptionTOTALI:FocusControlDBEdit5Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TDBEditDBEdit5Left�TopWidthPHeight	DataFieldImporto
DataSource	DsFatturaTabOrder   TDBEditDBEdit6LeftgTopWidthNHeight	DataFieldTotale
DataSource	DsFatturaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder   TPanelPanel6Left Top� Width�HeightAlignalClient
BevelOuter	bvLoweredTabOrder TDBGridDBGrid1LeftTop)Width�Height� AlignalClient
DataSource
DsFattDettReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameDescrizioneWidth�Visible	 Expanded	FieldName
ImponibileTitle.AlignmenttaRightJustifyWidthSVisible	 	AlignmenttaCenterExpanded	FieldNamePercentualeIVATitle.AlignmenttaCenterTitle.CaptionIVA %Width#Visible	 Expanded	FieldNameTotaleFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold Title.AlignmenttaRightJustifyTitle.Font.CharsetDEFAULT_CHARSETTitle.Font.ColorclWindowTextTitle.Font.Height�Title.Font.NameMS Sans SerifTitle.Font.StylefsBold WidthMVisible	    TPanelPanel7LeftTopWidth�Height(AlignalTopTabOrder TToolbarButton97TbBFattDettNewLeftTopWidthTHeight&CaptionNuovo dettaglio
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 33333333?�������        wwwwwwww�������?��?��� � � �w7sw7w7�������?��?��� � � �w7sw7w7�������?��?��� � � �w7sw7w7�������?��?��� � � �w7sw7w7���������������������wwwwwwww�����Ȁ�wwww�        wwwwwwww33333333333333333333333333333333	NumGlyphsWordWrap	OnClickTbBFattDettNewClick  TToolbarButton97TbBFattDettDelLeft� TopWidthTHeight&CaptionElimina dettaglio
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 30    3337wwww�330�wwp3337�����330���p3337�����330��pp3337�����330���p3337�����330��pp3337�����330���p333�������00��pp0377�����33 ���p33w����s330��pp3337�����330pppp3337�����33     33wwwww33��ww33����33     33wwwwws3330wp333337���33330  333337ww333	NumGlyphsWordWrap	OnClickTbBFattDettDelClick  TToolbarButton97TbBFattDettModLeft� TopWidthTHeight&CaptionModifica dettaglio
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 333     333wwwww333����?���??� 0  � �w�ww?sw7������ws3?33�࿿ ���w�3ws��7�������w�3?��7࿿  �w�3wwss7��������w�?���37�   ��w�wwws?� � �� �ws�w73w730 ���37wss3?�330���  33773�ww33��33s7s730���37�33s3	�� 33ws��w3303   3373wwws3	NumGlyphsWordWrap	OnClickTbBFattDettModClick  TToolbarButton97ToolbarButton971LeftUTopWidthTHeight&CaptionNuovo generico
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333�3333�39�3330 373337w39�3330 3��337w9���33337ww333?9���333 7wws333w39�3333 373333w39�3333337s3333?3333333 3333�33w333<333 3337�33w333<�3333����?�<����Ù37wwwww�<����Ù37wwwwsw3333<�3333337s33�333<330 3337337w3333330 3333337w3333333333333333	NumGlyphsWordWrap	OnClickToolbarButton971Click    TDataSource	DsFatturaDataSetTFatturaLeft�Top  TDataSource
DsFattDettDataSet	TFattDettLeft(Top}  TTableTRicercheABSDatabaseNameEBCDB	TableNamedbo.EBC_RicercheLeft8Topr TStringFieldTRicercheABSProgressivo	FieldNameProgressivoSize
  TDateTimeFieldTRicercheABSDataInizio	FieldName
DataInizio  TFloatFieldTRicercheABSStipendioLordo	FieldNameStipendioLordo  TAutoIncFieldTRicercheABSID	FieldNameIDReadOnly	   TQuery	QAppoggioDatabaseNameEBCDBSQL.Strings<select AppoggioBancario1,AppoggioBancario2,AppoggioBancario3from Global LefthTopz TStringFieldQAppoggioAppoggioBancario1	FieldNameAppoggioBancario1Origin"Global".AppoggioBancario1SizeP  TStringFieldQAppoggioAppoggioBancario2	FieldNameAppoggioBancario2Origin"Global".AppoggioBancario2SizeP  TStringFieldQAppoggioAppoggioBancario3	FieldNameAppoggioBancario3Origin"Global".AppoggioBancario3SizeP   TQueryQNoteSpeseFattOnCalcFieldsQNoteSpeseFattCalcFieldsDatabaseNameEBCDB
DataSource	DsFatturaSQL.Stringsselect * from RicNoteSpesewhere IDFattura=:ID Left�Topz	ParamDataDataType	ftAutoIncNameID	ParamType	ptUnknown   TIntegerFieldQNoteSpeseFattID	FieldNameIDOrigin"RicNoteSpese".ID  TIntegerFieldQNoteSpeseFattIDCliente	FieldName	IDClienteOrigin"RicNoteSpese".IDCliente  TIntegerFieldQNoteSpeseFattIDRicerca	FieldName	IDRicercaOrigin"RicNoteSpese".IDRicerca  TIntegerFieldQNoteSpeseFattIDUtente	FieldNameIDUtenteOrigin"RicNoteSpese".IDUtente  TIntegerFieldQNoteSpeseFattIDAnagrafica	FieldNameIDAnagraficaOrigin"RicNoteSpese".IDAnagrafica  TDateTimeFieldQNoteSpeseFattData	FieldNameDataOrigin"RicNoteSpese".Data  TStringFieldQNoteSpeseFattDescrizione	FieldNameDescrizioneOrigin"RicNoteSpese".DescrizioneSize2  TFloatFieldQNoteSpeseFattAutomobile	FieldName
AutomobileOrigin"RicNoteSpese".Automobile  TFloatFieldQNoteSpeseFattAutostrada	FieldName
AutostradaOrigin"RicNoteSpese".Autostrada  TFloatFieldQNoteSpeseFattAereoTreno	FieldName
AereoTrenoOrigin"RicNoteSpese".AereoTreno  TFloatFieldQNoteSpeseFattTaxi	FieldNameTaxiOrigin"RicNoteSpese".Taxi  TFloatFieldQNoteSpeseFattAlbergo	FieldNameAlbergoOrigin"RicNoteSpese".Albergo  TFloatFieldQNoteSpeseFattRistoranteBar	FieldNameRistoranteBarOrigin"RicNoteSpese".RistoranteBar  TFloatFieldQNoteSpeseFattParking	FieldNameParkingOrigin"RicNoteSpese".Parking  TStringFieldQNoteSpeseFattAltroDesc	FieldName	AltroDescOrigin"RicNoteSpese".AltroDescSize  TFloatFieldQNoteSpeseFattAltroImporto	FieldNameAltroImportoOrigin"RicNoteSpese".AltroImporto  TIntegerFieldQNoteSpeseFattIDFattura	FieldName	IDFatturaOrigin"RicNoteSpese".IDFattura  TFloatFieldQNoteSpeseFattTotale	FieldKindfkCalculated	FieldNameTotale
Calculated	   TQueryQTempDatabaseNameEBCDBLeftTop�   TQueryTFatturaCachedUpdates	DatabaseNameEBCDBRequestLive	SQL.Strings"select * from Fatture,EBC_Clienti &where Fatture.IDCliente=EBC_Clienti.IDand Fatture.ID=:xID  UpdateObject
UpdFattureLeft�Topb	ParamDataDataType	ftUnknownNamexID	ParamType	ptUnknown   TAutoIncField
TFatturaID	FieldNameID  TIntegerFieldTFatturaProgressivo	FieldNameProgressivo  TStringFieldTFatturaTipo	FieldNameTipo	FixedChar	  TIntegerFieldTFatturaRifProg	FieldNameRifProg  TIntegerFieldTFatturaIDCliente	FieldName	IDCliente  TDateTimeFieldTFatturaData	FieldNameData  TDateTimeFieldTFatturaDecorrenza	FieldName
Decorrenza  TFloatFieldTFatturaImporto	FieldNameImportoDisplayFormat#,###.##  TFloatFieldTFatturaIVA	FieldNameIVA  TFloatFieldTFatturaTotale	FieldNameTotaleDisplayFormat#,###.##  TStringFieldTFatturaModalitaPagamento	FieldNameModalitaPagamento	FixedChar	Size<  TStringFieldTFatturaAssegno	FieldNameAssegno	FixedChar	Size  TDateTimeFieldTFatturaScadenzaPagamento	FieldNameScadenzaPagamento  TBooleanFieldTFatturaPagata	FieldNamePagata  TDateTimeFieldTFatturaPagataInData	FieldNamePagataInData  TStringFieldTFatturaAppoggioBancario	FieldNameAppoggioBancario	FixedChar	SizeP  TStringFieldTFatturaDescrizione	FieldNameDescrizione	FixedChar	Size  TStringFieldTFatturaResponsabile	FieldNameResponsabileOriginEBCDB.Fatture.Responsabile	FixedChar	SizeP  TBooleanFieldTFatturaFlagFuoriAppIVA63372	FieldNameFlagFuoriAppIVA63372Origin"EBCDB.Fatture.FlagFuoriAppIVA63372   
TUpdateSQL
UpdFattureModifySQL.Stringsupdate Fattureset  Progressivo = :Progressivo,  Tipo = :Tipo,  RifProg = :RifProg,  IDCliente = :IDCliente,  Data = :Data,  Decorrenza = :Decorrenza,  Importo = :Importo,  IVA = :IVA,  Totale = :Totale,)  ModalitaPagamento = :ModalitaPagamento,  Assegno = :Assegno,)  ScadenzaPagamento = :ScadenzaPagamento,  Pagata = :Pagata,  PagataInData = :PagataInData,'  AppoggioBancario = :AppoggioBancario,  Responsabile = :Responsabile,.  FlagFuoriAppIVA63372 = :FlagFuoriAppIVA63372where  ID = :OLD_ID InsertSQL.Stringsinsert into FattureJ  (Progressivo, Tipo, RifProg, IDCliente, Data, Decorrenza, Importo, IVA, P   Totale, ModalitaPagamento, Assegno, ScadenzaPagamento, Pagata, PagataInData, 8   AppoggioBancario, Responsabile, FlagFuoriAppIVA63372)valuesL  (:Progressivo, :Tipo, :RifProg, :IDCliente, :Data, :Decorrenza, :Importo, M   :IVA, :Totale, :ModalitaPagamento, :Assegno, :ScadenzaPagamento, :Pagata, J   :PagataInData, :AppoggioBancario, :Responsabile, :FlagFuoriAppIVA63372) DeleteSQL.Stringsdelete from Fatturewhere  ID = :OLD_ID Left�Topj  TQuery	TFattDettOnCalcFieldsTFattDettCalcFieldsDatabaseNameEBCDB
DataSource	DsFatturaSQL.Stringsselect * from FattDettwhere IDFattura=:ID Left(TopZ	ParamDataDataType	ftAutoIncNameID	ParamType	ptUnknown   TAutoIncFieldTFattDettID	FieldNameIDOriginEBCDB.FattDett.ID  TIntegerFieldTFattDettIDFattura	FieldName	IDFatturaOriginEBCDB.FattDett.IDFattura  TStringFieldTFattDettDescrizione	FieldNameDescrizioneOriginEBCDB.FattDett.Descrizione	FixedChar	Size�   TFloatFieldTFattDettImponibile	FieldName
ImponibileOriginEBCDB.FattDett.ImponibileDisplayFormat#,###.##  TFloatFieldTFattDettPercentualeIVA	FieldNamePercentualeIVAOriginEBCDB.FattDett.PercentualeIVA  TIntegerFieldTFattDettIDCompenso	FieldName
IDCompensoOriginEBCDB.FattDett.IDCompenso  TFloatFieldTFattDettTotale	FieldKindfkCalculated	FieldNameTotaleDisplayFormat#,###.##
Calculated	    