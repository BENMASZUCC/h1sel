�
 TGESTSTORICOPOSFORM 0�  TPF0TGestStoricoPosFormGestStoricoPosFormLeft� Top� BorderStylebsDialogCaption/Gestione dello storico relativo alla posizione ClientHeightyClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterPixelsPerInch`
TextHeight TPanelPanel1Left Top Width�Height)AlignalTopTabOrder  TToolbarButton97ToolbarButton971LeftJTopWidthaHeight!CaptionEsci
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3     33wwwww33333333333333333333333333333333333333333333333?33�33333s3333333333333���33��337ww�33��337���33��337ww3333333333333����33     33wwwwws3	NumGlyphsOnClickToolbarButton971Click  TPanelPanel2LeftTopWidth�Height'AlignalLeft
BevelOuterbvNoneEnabledTabOrder  TDBEditDBEdit1LeftTop	Width� HeightColorclYellow	DataFieldCognome
DataSourceData.DsAnagraficaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder   TDBEditDBEdit2Left� Top	Width� HeightColorclYellow	DataFieldNome
DataSourceData.DsAnagraficaTabOrder    TPanelPanel3Left Top)Width�Height)AlignalTop
BevelOuter	bvLoweredTabOrder TToolbarButton97
BEventoInsLeftTopWidthNHeight'CaptionNuovo storico
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333�3333�39�3330 373337w39�3330 3��337w9���33337ww333?9���333 7wws333w39�3333 373333w39�3333337s3333?3333333 3333�33w333<333 3337�33w333<�3333����?�<����Ù37wwwww�<����Ù37wwwwsw3333<�3333337s33�333<330 3337337w3333330 3333337w3333333333333333	NumGlyphsWordWrap	OnClickBEventoInsClick  TToolbarButton97
BEventoDelLeft� TopWidthNHeight'CaptionElimina storico
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 30    3337wwww�330�wwp3337�����330���p3337�����330��pp3337�����330���p3337�����330��pp3337�����330���p333�������00��pp0377�����33 ���p33w����s330��pp3337�����330pppp3337�����33     33wwwww33��ww33����33     33wwwwws3330wp333337���33330  333337ww333	NumGlyphsWordWrap	OnClickBEventoDelClick  TToolbarButton97
BEventoModLeftOTopWidthNHeight'CaptionModifica storico
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 333     333wwwww333����?���??� 0  � �w�ww?sw7������ws3?33�࿿ ���w�3ws��7�������w�3?��7࿿  �w�3wwss7��������w�?���37�   ��w�wwws?� � �� �ws�w73w730 ���37wss3?�330���  33773�ww33��33s7s730���37�33s3	�� 33ws��w3303   3373wwws3	NumGlyphsWordWrap	OnClickBEventoModClick  TToolbarButton97ToolbarButton972LeftTopWidthQHeight'Captionstampa elenco
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 0      ?��������������wwwwwww�������wwwwwww        ���������������wwwwwww�������wwwwwww�������wwwwww        wwwwwww30����337���?330� 337�wss330����337��?�330�  337�swws330���3337��73330��3337�ss3330�� 33337��w33330  33337wws333	NumGlyphsWordWrap	OnClickToolbarButton972Click   TDBGridDBGrid1Left TopRWidth�Height'AlignalClient
DataSourceData2.DsQStoricoAnagPosReadOnly	TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldName	DallaDataTitle.Caption
Dalla dataWidthEVisible	 Expanded	FieldNameAllaDataTitle.Caption	Alla dataWidthFVisible	 Expanded	FieldNameDescrizPosizTitle.Caption	PosizioneWidth� Visible	 Expanded	FieldNameAutorizzatoDaTitle.CaptionAutorizzato daWidthxVisible	 Expanded	FieldNameAnnotazioniTitle.CaptionNoteWidth� Visible	    TTableTStoricoAnagPosActive	DatabaseNameHR	TableNamedbo.StoricoAnagPosizioniLeftxTop�  TAutoIncFieldTStoricoAnagPosID	FieldNameIDReadOnly	  TIntegerFieldTStoricoAnagPosIDAnagrafica	FieldNameIDAnagrafica  TIntegerFieldTStoricoAnagPosIDOrganigramma	FieldNameIDOrganigramma  TDateTimeFieldTStoricoAnagPosDallaData	FieldName	DallaData  TDateTimeFieldTStoricoAnagPosAllaData	FieldNameAllaData  TIntegerFieldTStoricoAnagPosIDAutorizzatoDa	FieldNameIDAutorizzatoDa  TStringFieldTStoricoAnagPosAnnotazioni	FieldNameAnnotazioniSize2    