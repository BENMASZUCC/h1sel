unit ModStatoRic;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DtEdit97, Grids, DBGrids, Db, DBTables, StdCtrls, Mask, DBCtrls,
  ExtCtrls, Buttons;

type
  TModStatoRicForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Panel1: TPanel;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DBEdit5: TDBEdit;
    Label2: TLabel;
    DBEdit6: TDBEdit;
    Label3: TLabel;
    TStatiRic: TTable;
    DsStatiRic: TDataSource;
    TStatiRicID: TAutoIncField;
    TStatiRicStatoRic: TStringField;
    DBGrid1: TDBGrid;
    Label4: TLabel;
    Label5: TLabel;
    DEDallaData: TDateEdit97;
    Label6: TLabel;
    ENote: TEdit;
    Label7: TLabel;
    DEDataFine: TDateEdit97;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ModStatoRicForm: TModStatoRicForm;

implementation

uses MDRicerche;

{$R *.DFM}

procedure TModStatoRicForm.BitBtn1Click(Sender: TObject);
begin
     if (TStatiRicStatoRic.Value='conclusa') and (DEDataFine.Text='') then begin
        ModalResult:=mrNone;
        MEssageDlg('Data fine procedura mancante',mtError,[mbOK],0);
        Abort;
     end;
     if (TStatiRicStatoRic.Value='attiva') and (DEDataFine.Text<>'') then begin
        ModalResult:=mrNone;
        MEssageDlg('Se la ricerca � attiva non ci pu� essere data di fine procedura',mtError,[mbOK],0);
        Abort;
     end;
end;

procedure TModStatoRicForm.FormClose(Sender: TObject; var Action: TCloseAction);
var x:integer;
begin
     // se la ricerca viene conclusa:
     // mettere in archivio tutti tranne chi � stato inserito ???
//     if TStatiRicStatoRic.Value='conclusa' then
//        messageDlg('S T O P:  riflettete su cosa volete che il programma faccia automaticamente'+chr(13)+
//                   'a ricerca conclusa (e poi, ordinatamente e con precisione, me lo dite ...!!!)'+chr(13)+
//                   'PER ORA NON FA NIENTE !',mtWarning,[mbOK],0);
end;

procedure TModStatoRicForm.FormShow(Sender: TObject);
begin
     Caption:='[M/23] - '+caption;
end;

end.
