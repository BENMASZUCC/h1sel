unit Inserimento;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Mask, DBCtrls, ExtCtrls, Db, DBTables, Grids, DBGrids,
  DtEdit97;

type
  TInserimentoForm = class(TForm)
    Panel1: TPanel;
    Panel7: TPanel;
    DBEdit12: TDBEdit;
    DBEdit14: TDBEdit;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    PanClienti: TPanel;
    Panel12: TPanel;
    DBGrid1: TDBGrid;
    TEBCclienti: TTable;
    TEBCclientiID: TAutoIncField;
    TEBCclientiDescrizione: TStringField;
    TEBCclientiIndirizzo: TStringField;
    TEBCclientiCap: TStringField;
    TEBCclientiComune: TStringField;
    TEBCclientiProvincia: TStringField;
    TEBCclientiIDAttivita: TIntegerField;
    DsEBCclienti: TDataSource;
    TAnagInserim: TTable;
    TAnagInserimID: TAutoIncField;
    TAnagInserimIDAnagrafica: TIntegerField;
    TAnagInserimIDCliente: TIntegerField;
    TAnagInserimDallaData: TDateTimeField;
    TAnagInserimAllaData: TDateTimeField;
    TAnagInserimIDRicerca: TIntegerField;
    TAnagInserimMotivoCessazione: TStringField;
    TAnagInserimNote: TMemoField;
    procedure DBGrid1KeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    xStringa:string;
  public
    { Public declarations }
  end;

var
  InserimentoForm: TInserimentoForm;

implementation

uses ModuloDati;

{$R *.DFM}

procedure TInserimentoForm.DBGrid1KeyPress(Sender: TObject; var Key: Char);
begin
     if Key=chr(13) then begin
        BitBtn1.Click;
     end else begin
        xStringa:=xStringa+key;
        TEBCclienti.FindNearest([xStringa]);
     end;
end;

procedure TInserimentoForm.FormShow(Sender: TObject);
begin
     Caption:='[E/2] - '+Caption;
end;

end.
