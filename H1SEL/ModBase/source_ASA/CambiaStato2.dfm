�
 TCAMBIASTATO2FORM 0  TPF0TCambiaStato2FormCambiaStato2FormLeftdTopdWidthBHeight�ActiveControlDBGrid1CaptionNuovo eventoColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrder	PositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TPanelPanel1Left Top Width:Height%AlignalTopEnabledTabOrder  TEditECognLeftTopWidth� HeightColorclYellowFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder TextECogn  TEditENomeLeft� TopWidth� HeightColorclYellowTabOrderTextENome   TPanelPanStatoLeft Top%Width:Height!AlignalTopEnabledTabOrder TLabelLabel2Left	Top	Width?HeightCaptionStato attuale:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TEditEStatoLeftPTopWidth� HeightTabOrder TextEStato   TPanelPanel3Left TopvWidth:Height2AlignalBottomTabOrder TBitBtnBitBtn1Left� TopWidthUHeight$Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder KindbkOK  TBitBtnBitBtn2Left� TopWidthRHeight$CaptionAnnullaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderKindbkCancel   TPanelPanel4Left TopFWidth:Height0AlignalClientTabOrder TDBGridDBGrid1LeftTop3Width8Height� AlignalClient
DataSourcedsQNextStatiFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameEventoTitle.Font.CharsetDEFAULT_CHARSETTitle.Font.ColorclWindowTextTitle.Font.Height�Title.Font.NameMS Sans SerifTitle.Font.Style Width� Visible	 Expanded	FieldNameAStatoTitle.Captionstato successivoTitle.Font.CharsetDEFAULT_CHARSETTitle.Font.ColorclWindowTextTitle.Font.Height�Title.Font.NameMS Sans SerifTitle.Font.Style WidthsVisible	    TPanelPanel5LeftTopWidth8Height2AlignalTop
BevelOuterbvNoneTabOrder TLabelLabel4LeftTopWidth>HeightCaptionData evento:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TDateEdit97DEDataLeftNTopWidthcHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder ColorCalendar.ColorValidclBlueDayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanPopup
doIsMaskeddoShowCanceldoShowToday   TPanelPanel127Left TopWidth8Height	AlignmenttaLeftJustifyCaption&  Eventi possibili dallo stato attualeColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   TPanelPanel6LeftTop	Width8Height&AlignalBottomTabOrderVisible TLabelLabel1LeftTopWidthHeightCaptionNote:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TEditEdit1Left)TopWidthHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 	MaxLength2
ParentFontTabOrder     TQuery
QNextStatiDatabaseNameEBCDBSQL.Strings;select EBC_Eventi.ID,EBC_Eventi.Evento,EBC_Eventi.IDaStato,!          EBC_Stati.Stato AStato,!          EBC_Eventi.IDProcedura,,          EBC_Stati.IDTipoStato IDTipoStatoAfrom EBC_Eventi,EBC_Stati &where EBC_Eventi.IDaStato=EBC_Stati.ID"and EBC_Eventi.IDdaStato=:xDaStato Left� Top� 	ParamDataDataType	ftUnknownNamexDaStato	ParamType	ptUnknown   TAutoIncFieldQNextStatiID	FieldNameIDOriginEBCDB.EBC_Eventi.ID  TStringFieldQNextStatiEvento	FieldNameEventoOriginEBCDB.EBC_Eventi.Evento	FixedChar	Size  TStringFieldQNextStatiAStato	FieldNameAStatoOriginEBCDB.EBC_Stati.Stato	FixedChar	Size  TIntegerFieldQNextStatiIDProcedura	FieldNameIDProceduraOriginEBCDB.EBC_Eventi.IDProcedura  TIntegerFieldQNextStatiIDaStato	FieldNameIDaStatoOriginEBCDB.EBC_Eventi.IDaStato  TIntegerFieldQNextStatiIDTipoStatoA	FieldNameIDTipoStatoAOriginEBCDB.EBC_Stati.IDTipoStato   TDataSourcedsQNextStatiDataSet
QNextStatiLeft� Top�    