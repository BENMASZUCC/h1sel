unit SelArea;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Grids, DBGrids, Db, DBTables;

type
  TSelAreaForm = class(TForm)
    DBGrid1: TDBGrid;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    QAree: TQuery;
    DsQAree: TDataSource;
    QAreeID: TAutoIncField;
    QAreeDescrizione: TStringField;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  SelAreaForm: TSelAreaForm;

implementation

uses ModuloDati2;

{$R *.DFM}

procedure TSelAreaForm.FormShow(Sender: TObject);
begin
     Caption:='[S/34] - '+Caption;
end;

end.
