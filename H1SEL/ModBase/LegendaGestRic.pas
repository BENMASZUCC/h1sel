unit LegendaGestRic;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, RXSpin, ExtCtrls, Db, DBTables, ADODB, U_ADOLinkCl;

type
     TLegendaGestRicForm = class(TForm)
          Panel3: TPanel;
          Label5: TLabel;
          Label8: TLabel;
          Label9: TLabel;
          RxSpinEdit1: TRxSpinEdit;
          BitBtn1: TBitBtn;
          Label1: TLabel;
          Panel1: TPanel;
          Label2: TLabel;
          Label3: TLabel;
          Label4: TLabel;
          Label6: TLabel;
          RxSpinEdit2: TRxSpinEdit;
          Q_OLD: TQuery;
          GroupBox1: TGroupBox;
          CBMancaCV: TCheckBox;
          Label7: TLabel;
          CBMancaTel: TCheckBox;
          Q: TADOLinkedQuery;
          GroupBox2: TGroupBox;
          Label10: TLabel;
          Label11: TLabel;
          procedure FormShow(Sender: TObject);
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     LegendaGestRicForm: TLegendaGestRicForm;

implementation

uses ModuloDati;

{$R *.DFM}

procedure TLegendaGestRicForm.FormShow(Sender: TObject);
begin
     Caption := '[M/214] - ' + Caption;
     Q.SQl.text := 'select ggDiffDataIns,ggDiffDataUltimoContatto,CheckRicCandCV,CheckRicCandTel  from global ';
     Q.Open;
     RxSpinEdit1.value := Q.FieldByname('ggDiffDataIns').asInteger;
     RxSpinEdit2.value := Q.FieldByname('ggDiffDataUltimoContatto').asInteger;
     CBMancaCV.Checked := Q.FieldByname('CheckRicCandCV').AsBoolean;
     CBMancaTel.Checked := Q.FieldByname('CheckRicCandTel').AsBoolean;
     Q.Close;
end;

//[/TONI20020905\]

procedure TLegendaGestRicForm.FormClose(Sender: TObject;
     var Action: TCloseAction);
begin
     with Data do begin
          DB.BeginTrans;
          try
               Q1.Close;
               {               Q1.SQL.text:='update global set ggDiffDataIns=:xggDiffDataIns,ggDiffDataUltimoContatto=:xggDiffDataUltimoContatto,'+
                                   'CheckRicCandCV=:xCheckRicCandCV,CheckRicCandTel=:xCheckRicCandTel';
                              Q1.ParamByName('xggDiffDataIns').asInteger:=RxSpinEdit1.AsInteger;
                              Q1.ParamByName('xggDiffDataUltimoContatto').asInteger:=RxSpinEdit2.AsInteger;
                              Q1.ParamByName('xCheckRicCandCV').AsBoolean:=CBMancaCV.Checked;
                              Q1.ParamByName('xCheckRicCandTel').AsBoolean:=CBMancaTel.Checked;}
               Q1.SQL.text := 'update global set ggDiffDataIns=:xggDiffDataIns:,ggDiffDataUltimoContatto=:xggDiffDataUltimoContatto:,' +
                    'CheckRicCandCV=:xCheckRicCandCV:,CheckRicCandTel=:xCheckRicCandTel:';
               Q1.ParamByName['xggDiffDataIns'] := RxSpinEdit1.AsInteger;
               Q1.ParamByName['xggDiffDataUltimoContatto'] := RxSpinEdit2.AsInteger;
               Q1.ParamByName['xCheckRicCandCV'] := CBMancaCV.Checked;
               Q1.ParamByName['xCheckRicCandTel'] := CBMancaTel.Checked;
               Q1.ExecSQL;
               DB.CommitTrans;
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
          end;
     end;
end;
//[/TONI20020905\]FINE
end.
