unit GoogleMap;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     OleCtrls, SHDocVw, TB97, ExtCtrls, StdCtrls, ComCtrls, MSHTML;

type
     TGoogleMapForm = class(TForm)
          Panel1: TPanel;
          Panel2: TPanel;
          WebBrowser1: TWebBrowser;
          Panel4: TPanel;
          BCancellaMarker: TToolbarButton97;
          MarkCli: TToolbarButton97;
          MarkCand: TToolbarButton97;
          Panel3: TPanel;
          BEsci: TToolbarButton97;
          Panel5: TPanel;
          MemoAddress: TMemo;
          MarkAddress: TToolbarButton97;
          Latitudine: TEdit;
          Longitudine: TEdit;
          MarkGotoCoordinate: TToolbarButton97;
          CheckBoxTraffic: TCheckBox;
          CheckBoxStreeView: TCheckBox;
          Label1: TLabel;
          Label2: TLabel;
          Label3: TLabel;
          procedure FormShow(Sender: TObject);
          procedure BEsciClick(Sender: TObject);
          procedure BCancellaMarkerClick(Sender: TObject);
          procedure CaricaMappaGoogle;
          procedure ToolbarButton971Click(Sender: TObject);
          procedure MarkGotoCoordinateClick(Sender: TObject);
          procedure CheckBoxStreeViewClick(Sender: TObject);
          procedure CheckBoxTrafficClick(Sender: TObject);
          procedure MarkAddressClick(Sender: TObject);
          procedure MarkCandClick(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
          HTMLWindow2: IHTMLWindow2;
          LatCand, LngCand: string;
          LatCliente, LngCLiente: string;
          partenza, arrivo: string;
          idRicerca, idAnagrafica: string;
          HTMLStr: string;
     end;

var
     GoogleMapForm: TGoogleMapForm;

implementation

uses ActiveX, Main;

{$R *.DFM}

procedure TGoogleMapForm.CaricaMappaGoogle;
var aStream: TMemoryStream;
begin
     HTMLStr :=
          '<html> ' +
          '<head> ' +
          '<meta name="viewport" content="initial-scale=1.0, user-scalable=yes" /> ' +
          '<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script> ' +
          '<script type="text/javascript"> ' +
          '' +
          '' +
          '  var geocoder; ' +
          '  var map;  ' +
          '  var trafficLayer;' +
          '  var bikeLayer;' +
          '' +
          '' +
          '  function initialize() { ' +
          '    geocoder = new google.maps.Geocoder();' +
          '    var latlng = new google.maps.LatLng(11.343430003463709,44.49420068725708); ' +
          '    var myOptions = { ' +
          '      zoom: 13, ' +
          '      center: latlng, ' +
          '      mapTypeId: google.maps.MapTypeId.ROADMAP ' +
          '    }; ' +
          '    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions); ' +
          '    StreetViewOff();' +
          '    trafficLayer = new google.maps.TrafficLayer();' +
          '  } ' +
          '' +
          '' +
          '  function codeAddress(address) { ' +
          '    if (geocoder) {' +
          '      geocoder.geocode( { address: address}, function(results, status) { ' +
          '        if (status == google.maps.GeocoderStatus.OK) {' +
          '          map.setCenter(results[0].geometry.location);' +
          '          var marker = new google.maps.Marker({' +
          '              map: map,' +
          '              position: results[0].geometry.location' +
          '          });' +
          '        } else {' +
          '          alert("Geocode was not successful for the following reason: " + status);' +
          '        }' +
          '      });' +
          '    }' +
          '  }' +
          '' +
          '' +
          '  function ClearMarkers() {  ' +
          '    if (markersArray) {        ' +
          '      for (i in markersArray) {  ' +
          '        markersArray[i].setMap(null); ' +
          '      } ' +
          '    } ' +
          '  }  ' +
          '' +
          '' +
          '  function GotoLatLng(Lat, Lang) { ' +
          '   var latlng = new google.maps.LatLng(Lat,Lang);' +
          '   map.setCenter(latlng);' +
          '   var marker = new google.maps.Marker({' +
          '      position: latlng, ' +
          '      map: map,' +
          '      title:Lat+","+Lang' +
          '  });' +
          '  }' +
          '' +
          '' +
          '  function PutMarker(Lat, Lang, Msg) { ' +
          '   var latlng = new google.maps.LatLng(Lat,Lang);' +  
          '   map.setCenter(latlng);' +
          '   var marker = new google.maps.Marker({' +
          '      position: latlng, ' +
          '      map: map,' +
          '      title: Msg+" ("+Lat+","+Lang+")"' +
          '  });' +
          '  if (Msg="Residenza") {        ' +
          '   icon = "http://www.google.com/mapfiles/kml/paddle/R.png"; ' +
          '  } ' +
          '  if (Msg="Domicilio") {        ' +
          '   icon = "http://www.google.com/mapfiles/kml/paddle/D.png"; ' +
          '  } ' +
          '  marker.setIcon(icon); ' +
          '  }' +
          '' +
          '' +
          '  function TrafficOn()   { trafficLayer.setMap(map); }' +
          '' +
          '  function TrafficOff()  { trafficLayer.setMap(null); }' +
          '' +
          '  function StreetViewOn() { map.set("streetViewControl", true); }' +
          '' +
          '  function StreetViewOff() { map.set("streetViewControl", false); }' +
          '' +
          '' + '</script> ' +
          '</head> ' +
          '<body onload="initialize()"> ' +
          '  <div id="map_canvas" style="width:100%; height:100%"></div> ' +
          '</body> ' +
          '</html> ';

     WebBrowser1.Navigate('about:blank');
     if Assigned(WebBrowser1.Document) then
     begin
          aStream := TMemoryStream.Create;
          try
               aStream.WriteBuffer(Pointer(HTMLStr)^, Length(HTMLStr));
               aStream.Seek(0, soFromBeginning);
               (WebBrowser1.Document as IPersistStreamInit).Load(TStreamAdapter.Create(aStream));
          finally
               aStream.Free;
          end;
          HTMLWindow2 := (WebBrowser1.Document as IHTMLDocument2).parentWindow;
     end;
end;

procedure TGoogleMapForm.FormShow(Sender: TObject);
begin
     //Grafica
     GoogleMapForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel3.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel3.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel3.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel4.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel4.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel4.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     CaricaMappaGoogle;
end;

procedure TGoogleMapForm.BEsciClick(Sender: TObject);
begin
     GoogleMapForm.ModalResult := mrOk;
end;

procedure TGoogleMapForm.BCancellaMarkerClick(Sender: TObject);
begin
     HTMLWindow2.execScript('ClearMarkers()', 'JavaScript');
end;

procedure TGoogleMapForm.ToolbarButton971Click(Sender: TObject);
begin
     {*

     GoogleMapForm.HTMLWindow2.execScript(Format('PutMarker(%s,%s,"")', [LatCliente, LngCliente]), 'JavaScript');
     GoogleMapForm.HTMLWindow2.execScript(Format('PutMarker(%s,%s,"")', [LatCand, LngCand]), 'JavaScript');
     //GoogleMapForm.HTMLWindow2.execScript(Format('calcRoute(%s,%s)', [partenza, arrivo]), 'JavaScript');
     //GoogleMapForm.HTMLWindow2.execScript(Format('GotoLatLng(%s,%s)', [LatCliente, LngCliente]), 'JavaScript');

          DataRicerche.qCandRic.Bookmark := dxDBGrid1.SelectedRows[0];
          DataRicerche.QDistanza.close;
          DataRicerche.QDistanza.SQL.Text := 'select * from GoogleDistanzaRic ' +
               ' where idRicerca=' + DataRicerche.qCandRicIDRicerca.AsString +
               ' and idAnagrafica=' + DataRicerche.qCandRicIDAnagrafica.AsString;
          DataRicerche.QDistanza.Open;
          if DataRicerche.QDistanza.IsEmpty then begin
               MessageDlg('Attenzione, Non hai calcolato la Distanza tra ' +
                    chr(13) + 'l''indirizzo del Cliente e l''indirizzo del Candidato!', mtWarning, [mbOK], 0);
               Exit;
               if MessageDlg('Vuoi procedere al colcolo della distanza tra ' + chr(13) +
                    'l''indirizzo del Cliente e l''indirizzo del Candidato?', mtWarning, [mbYes, mbNo], 0) = mrYes then begin
                    AnagraficaResidenza1Click
               end else Exit;
          end;
          GoogleMapForm := TGoogleMapForm.create(self);
          GoogleMapForm.partenza := IndirizzoCliente;
          GoogleMapForm.arrivo := IndirizzoAna;
          GoogleMapForm.LatCand := DataRicerche.QDistanza.FieldByName('LatitudineCand').AsString;
          GoogleMapForm.LngCand := DataRicerche.QDistanza.FieldByName('LongitudineCand').AsString;
          GoogleMapForm.LatCliente := DataRicerche.QDistanza.FieldByName('LatitudineCliente').AsString;
          GoogleMapForm.LngCliente := DataRicerche.QDistanza.FieldByName('LongitudineCliente').AsString;
          GoogleMapForm.BMakerCliCand.Visible := True;
          GoogleMapForm.ShowModal;
          GoogleMapForm.Free;

     *}
end;

procedure TGoogleMapForm.MarkGotoCoordinateClick(Sender: TObject);
begin
     HTMLWindow2.execScript(Format('GotoLatLng(%s,%s)', [Latitudine.Text, Longitudine.Text]), 'JavaScript'); //Call the function GotoLatLng to go the coordinates
end;

procedure TGoogleMapForm.CheckBoxStreeViewClick(Sender: TObject);
begin
     if CheckBoxStreeView.Checked then
          HTMLWindow2.execScript('StreetViewOn()', 'JavaScript') //Activate the Street View option
     else
          HTMLWindow2.execScript('StreetViewOff()', 'JavaScript'); //Deactivate the Street View option
end;

procedure TGoogleMapForm.CheckBoxTrafficClick(Sender: TObject);
begin
     if CheckBoxTraffic.Checked then
          HTMLWindow2.execScript('TrafficOn()', 'JavaScript') //Activate the Traffic View option
     else
          HTMLWindow2.execScript('TrafficOff()', 'JavaScript'); //Deactivate the Traffic View option

end;

procedure TGoogleMapForm.MarkAddressClick(Sender: TObject);
var
     address: string;
begin
     address := MemoAddress.Lines.Text;
     address := StringReplace(StringReplace(Trim(address), #13, ' ', [rfReplaceAll]), #10, ' ', [rfReplaceAll]);
     HTMLWindow2.execScript(Format('codeAddress(%s)', [QuotedStr(address)]), 'JavaScript'); //Call the function codeAddress to go the address
end;

procedure TGoogleMapForm.MarkCandClick(Sender: TObject);
begin
     HTMLWindow2.execScript(Format('PutMarker(%s,%s,"Residenza")', [LatCand, LngCand]), 'JavaScript');
end;

end.

