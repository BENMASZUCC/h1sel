unit SceltaModelloAnag;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Grids, DBGrids, Db, DBTables, StdCtrls, Buttons, ExtCtrls, ADODB,
     U_ADOLinkCl, TB97;

type
     TSceltaModelloAnagForm = class(TForm)
          DsModelliWord: TDataSource;
          DBGrid1: TDBGrid;
          CBSalva: TCheckBox;
          RGOpzione: TRadioGroup;
          QModelliWord: TADOLinkedQuery;
          QModelliAvanzati: TADOQuery;
          QModelliAvanzatiIDModello: TIntegerField;
          QModelliAvanzatiIDCampo: TIntegerField;
          QModelliAvanzatiTabellare: TBooleanField;
          CBFlagInvio: TCheckBox;
          CBVisibleCliente: TCheckBox;
          Panel1: TPanel;
          BitBtn1: TToolbarButton97;
    BitBtn2: TToolbarButton97;
    BitBtn3: TToolbarButton97;
    SpeedButton1: TToolbarButton97;
          procedure FormCreate(Sender: TObject);
          procedure BitBtn3Click(Sender: TObject);
          procedure BitBtn1Click(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure SpeedButton1Click(Sender: TObject);
          procedure BitBtn2Click(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     SceltaModelloAnagForm: TSceltaModelloAnagForm;

implementation

uses ModuloDati, GestModelliWord, Main, uUtilsVarie;

{$R *.DFM}

procedure TSceltaModelloAnagForm.FormCreate(Sender: TObject);
begin
     Height := 330;
end;

procedure TSceltaModelloAnagForm.BitBtn3Click(Sender: TObject);
begin
     GestModelliWordForm := TGestModelliWordForm.create(self);
     GestModelliWordForm.ShowModal;
     QmodelliWord.Close;
     QModelliWord.Open;
     QmodelliWord.Locate('ID', GestModelliWordForm.QModelli.FieldByName('ID').AsInteger, []);
     GestModelliWordForm.Free;
end;

procedure TSceltaModelloAnagForm.BitBtn1Click(Sender: TObject);
begin
     SceltaModelloAnagForm.ModalResult := mrOK;
     if QModelliWord.IsEmpty then begin
          MessageDlg('Nessun modello disponibile: IMPOSSIBILE PROSEGUIRE' + chr(13) +
               'Premere "Annulla" o creare un modello usando il pulsante "Modelli"', mtError, [mbOK], 0);
          Abort;
     end;
     if (QModelliAvanzati.IsEmpty = FALSE) then
          if mainform.VerificaFunzLicenzeNew('106')=false then
          begin
               ShowMessage('Attenzione : Funzionalitą non prevista dalla licenza. Rivolgersi al fornitore.');
               ModalResult := mrAbort;
          end;
end;

procedure TSceltaModelloAnagForm.FormShow(Sender: TObject);
begin
     //Grafica
     SceltaModelloAnagForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     Caption := '[M/072] - ' + Caption;
     QModelliWord.Open;
     QModelliAvanzati.Open;
end;

procedure TSceltaModelloAnagForm.SpeedButton1Click(Sender: TObject);
begin
     Aprihelp('072');
end;

procedure TSceltaModelloAnagForm.BitBtn2Click(Sender: TObject);
begin
     SceltaModelloAnagForm.ModalResult := mrCancel;
end;

end.

