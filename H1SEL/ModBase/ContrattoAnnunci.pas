unit ContrattoAnnunci;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, ExtCtrls, Mask, ToolEdit, CurrEdit, RXSpin, DtEdit97,
     Db, DBTables, Grids, DBGrids, ADODB, U_ADOLinkCl, TB97;

type
     TContrattoAnnunciForm = class(TForm)
          DBGrid1: TDBGrid;
          QTestEdiz_OLD: TQuery;
          QTestEdiz_OLDIDTestata: TIntegerField;
          QTestEdiz_OLDIDEdizione: TAutoIncField;
          QTestEdiz_OLDTestata: TStringField;
          QTestEdiz_OLDEdizione: TStringField;
          DsQTestEdiz: TDataSource;
          DEDataStipula: TDateEdit97;
          DEDataScadenza: TDateEdit97;
          TotModuli: TRxSpinEdit;
          ModuliUtilizzati: TRxSpinEdit;
          CostoLire: TRxCalcEdit;
          CostoEuro: TRxCalcEdit;
          Label1: TLabel;
          Label2: TLabel;
          Label3: TLabel;
          Label4: TLabel;
          Label5: TLabel;
          Label6: TLabel;
          Bevel1: TBevel;
          Bevel2: TBevel;
          Label7: TLabel;
          QTestEdiz: TADOLinkedQuery;
          Panel1: TPanel;
          BitBtn1: TToolbarButton97;
          BitBtn2: TToolbarButton97;
          procedure CostoLireChange(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure CostoEuroChange(Sender: TObject);
          procedure BitBtn1Click(Sender: TObject);
          procedure BitBtn2Click(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     ContrattoAnnunciForm: TContrattoAnnunciForm;

implementation
//[/TONI20020726\]
uses ModuloDati, Main;
//[/TONI20020726\]FINE
{$R *.DFM}

procedure TContrattoAnnunciForm.CostoLireChange(Sender: TObject);
begin
     if CostoLire.Focused then
          CostoEuro.Value := CostoLire.value / 1936.27;
end;

procedure TContrattoAnnunciForm.FormShow(Sender: TObject);
begin
      //Grafica
     ContrattoAnnunciForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     Caption := '[M/3504] - ' + caption;
end;

procedure TContrattoAnnunciForm.CostoEuroChange(Sender: TObject);
begin
     if CostoEuro.Focused then
          CostoLire.Value := CostoEuro.value * 1936.27;
end;

procedure TContrattoAnnunciForm.BitBtn1Click(Sender: TObject);
begin
     ContrattoAnnunciForm.ModalResult := mrOk;
end;

procedure TContrattoAnnunciForm.BitBtn2Click(Sender: TObject);
begin
     ContrattoAnnunciForm.ModalResult := mrCancel;
end;

end.

