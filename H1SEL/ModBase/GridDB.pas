//TONI OK 05/5
unit GridDB;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Buttons, Db, ExtCtrls, Grids, DBGrids, stdctrls, mask, TB97, dxCntner, dxTL,
     dxDBCtrl, dxDBGrid;
type
     TOperation = (modifica, inserisci, cancella);

     TFrameDB = class(TFrame)
          DBGrid1: TDBGrid;
          PanelRight: TPanel;
          DataSourceGrid: TDataSource;
          PanelControl: TPanel;
          BIns: TToolbarButton97;
          BMod: TToolbarButton97;
          BDel: TToolbarButton97;
          DBGrid2: TdxDBGrid;
          procedure DBGrid1DrawDataCell(Sender: TObject; const Rect: TRect;
               Field: TField; State: TGridDrawState);
          procedure BInsClick(Sender: TObject);
          procedure BModClick(Sender: TObject);
          procedure BDelClick(Sender: TObject);
     private
          { Private declarations }
          procedure GoEdit(op: TOperation);
          procedure RiempiService(op: Toperation);
          procedure CopiaDati;
          procedure DistruggiService;
          procedure InserisciRiga;
          procedure ModificaRiga;
          procedure CancellaRiga;
          procedure ProcAnnulla(Sender: TObject);
          procedure ProcOK(Sender: TObject);
     public
          { Public declarations }
          serviceform: TForm;
          xAltezzaLabel: integer;
     end;

implementation

uses main;

{$R *.DFM}
const q = 100; //serve per visualizzare memo grande oppure piccolo tipo edit, in base al numero di caratteri del campo varchar



procedure TFrameDB.ProcAnnulla(Sender: TObject);
begin
     serviceform.ModalResult := mrCancel;
end;

procedure TFrameDB.ProcOK(Sender: TObject);
begin
     serviceform.ModalResult := mrOk;
end;

procedure TFrameDB.DBGrid1DrawDataCell(Sender: TObject; const Rect: TRect; Field: TField; State: TGridDrawState);
var testo: string;
begin
     if Assigned(DataSourceGrid.DataSet) then begin
          if not ((field.DataType = ftString) or
               (field.DataType = ftinteger)
               or
               (field.DataType = ftinteger) or
               (field.DataType = ftsmallint) or
               (field.DataType = ftWord) or
               (field.DataType = ftfloat) or
               (field.DataType = ftAutoInc) or
               (field.DataType = ftFixedChar) or
               (field.DataType = ftWideString) or
               (field.DataType = ftLargeInt) or
               (field.DataType = ftAutoInc)) then
               dbgrid1.DefaultDrawDataCell(rect, field, state)
          else
               if ((field.Index mod 2) = 1) then
               begin
                    //DBGrid1.Canvas.brush.Color := clred;
                    DBGrid1.Canvas.brush.Style := bssolid;
                    DBGrid1.Canvas.FillRect(rect);
                    testo := field.asstring;
                    DBGrid1.Canvas.TextOut(rect.left + 2, rect.top + 2, testo);
               end;
     end;

end;

procedure TFrameDB.GoEdit(op: TOperation);
var i: integer;
begin
     if Assigned(DataSourceGrid.DataSet) then
     begin
          serviceform.Position := poMainFormCenter;
          RiempiService(op);
          if op = cancella then
               for i := 1 to serviceform.ComponentCount do
                    if serviceform.Components[i - 1] is TEdit then
                         TEdit(serviceform.Components[i - 1]).ReadOnly := TRUE;
          //serviceform.show;
          case op of
               inserisci: serviceform.Caption := 'Inserimento nuovo';
               modifica: serviceform.Caption := 'Modifica';
               cancella: serviceform.Caption := 'Cancellazione';
          end;

          if serviceform.ShowModal = mrok then
          begin
               case op of
                    inserisci: InserisciRiga;
                    modifica: ModificaRiga;
                    cancella: CancellaRiga;
               end;
          end
          else
               DistruggiService;
     end;
end;

procedure TFrameDB.DistruggiService;
begin
     while serviceform.ComponentCount > 0 do
     begin
          if serviceform.Components[0] is TEdit then
          begin
               TEdit(serviceform.Components[0]).Free;
          end
          else if serviceform.Components[0] is TMemo then
          begin
               TMemo(serviceform.Components[0]).Free;
          end
          else if serviceform.Components[0] is TCheckBox then
          begin
               TCheckBox(serviceform.Components[0]).Free;
          end
          else if serviceform.Components[0] is TLabel then
          begin
               TLabel(serviceform.Components[0]).Free;
          end
          else if serviceform.Components[0] is TToolbarButton97 then
          begin
               TSpeedButton(serviceform.Components[0]).Free;
          end;
     end;
end;




procedure TFrameDB.RiempiService(op: Toperation);
var i, tempHeight, maxwidth: integer;
    // tempEdit: TEdit;
     tempEdit: Tmemo;
     tempLabel: TLabel;
     tempButton: TToolbarButton97; //TBitBtn;
     Field: TField;
     tempcheck: TCheckBox;
     bmpok, bmpAbort: TBitmap;
begin
     DistruggiService;

     serviceform.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;

    // serviceform.BorderStyle := bsSizeable;

     with serviceform do begin
          Height := 10;
          //     Width := 400;
          tempHeight := 5;
          maxwidth := 0;
          for i := 1 to dbgrid2.ColumnCount do begin
               field := DBGrid2.DataSource.DataSet.fields[i - 1];
               //for i:=0 to dbgrid1.Columns.Count do begin
               //     field:=DBGrid1.DataSource.DataSet.fields[i];


               if (field.DataType = ftString) or
                    (field.DataType = ftinteger) or
                    (field.DataType = ftsmallint) or
                    (field.DataType = ftWord) or
                    (field.DataType = ftfloat) or
                    (field.DataType = ftFixedChar) or
                    (field.DataType = ftWideString) or
                    (field.DataType = ftCurrency) or
                    (field.DataType = ftLargeInt) then begin
                    //label
                    tempLabel := TLabel.Create(serviceform);
                    tempLabel.parent := serviceform;
                    tempLabel.Left := 5;
                    tempLabel.Caption := DBGrid1.Columns.Items[i - 1].Title.Caption;
                    tempLabel.Top := tempHeight;
                    tempHeight := tempHeight + tempLabel.Height + 3;

                    //Edit
                //   tempedit := TEdit.Create(serviceform);
                    tempEdit := Tmemo.Create(serviceform);
                    //tempedit.Anchors :=[akLeft,akRight];
                    tempedit.Height := 21;

                    tempedit.parent := serviceform;
                    tempedit.Left := 5;
                    tempedit.MaxLength := field.size;
                    { if (field.DataType = ftinteger) or
                        (field.DataType = ftinteger)or
                        (field.DataType = ftsmallint)or
                        (field.DataType = ftWord	)or
                        (field.DataType = ftfloat	)or
                        (field.DataType = ftCurrency )or
                        (field.DataType = ftLargeInt ) then
                         // tempedit.EditMask := '; '+stringofchar('0',field.size)+'; '
                     else
                          // tempedit.EditMask := '; '+stringofchar('A',field.size)+'; ';}
                    if op <> inserisci then tempedit.Text := field.AsString;
                    tempedit.Top := tempHeight;
                    tempEdit.Width := 200; //DBGrid1.Fields[i-1].Size; //DBGrid1.Columns.Items[i - 1].Width;


                    if ((DBGrid1.Fields[i - 1].Size > q) and (xAltezzaLabel = 0)) then begin
                         tempedit.WordWrap := true;
                         tempedit.ScrollBars := ssVertical;
                         tempedit.Width := 500;
                         tempedit.Height := 70;
                    end else
                         tempedit.Height := xAltezzaLabel;

                    if tempedit.Height = 0 then tempedit.Height := 21;
                    //questo serve perch� nelle funzioni tipo OpenTab, non � ancora gestista la variabile xAltezzaLabel

                    if tempEdit.Width > maxwidth then maxwidth := tempEdit.Width;

                    tempHeight := tempHeight + tempedit.Height + 5;
               end;

               if (field.DataType = ftBoolean) then begin
                    tempcheck := TCheckbox.Create(serviceform);
                    tempcheck.Left := 5;
                    tempcheck.Width := 130;
                    tempcheck.Top := tempHeight;
                    tempcheck.parent := serviceform;
                    tempcheck.Checked := field.AsBoolean;
                    tempcheck.Caption := DBGrid2.Columns[i - 1].caption;
                    tempHeight := tempHeight + 5;
               end;



          end;

          BmpOK := TBitmap.Create;
          BmpAbort := TBitmap.Create;
          mainform.ImageList5.GetBitmap(5, BmpOK);
          mainform.ImageList5.GetBitmap(4, BmpAbort);

          //buttonOK
          tempHeight := tempHeight + 20;
          tempbutton := TToolbarButton97.Create(serviceform);
         //  tempbutton := TBitBtn.Create(serviceform);
          tempbutton.parent := serviceform;
          //tempbutton.Kind := bkOK;
          tempButton.Glyph := bmpok;
          tempbutton.Left := 5;
          tempButton.Width := 80;
          tempButton.Height := 35;
          tempbutton.Caption := 'OK';
          tempbutton.Top := tempHeight;
          //tempButton.ModalResult := mrok;
          tempButton.OnClick := ProcOK;
          //buttonCancel
          tempbutton := TToolbarButton97.Create(serviceform);
          // tempbutton := TBitBtn.Create(serviceform);
          tempbutton.parent := serviceform;
          //tempButton.Glyph :=
         // tempbutton.Kind := bkCancel;
          tempButton.Glyph := bmpabort;
          maxwidth := maxwidth + 30;
          if maxwidth > 180 then serviceform.Width := maxwidth
          else serviceform.Width := 180;
          tempbutton.Left := serviceform.Width - 90;
          tempButton.Width := 80;
          tempButton.Height := 35;
          tempbutton.caption := 'Annulla';
          tempbutton.Top := tempHeight;
          tempButton.OnClick := ProcAnnulla;
         // tempButton.ModalResult := mrCancel;

          tempHeight := tempHeight + tempbutton.Height + 35;
          serviceform.Height := tempHeight;

          bmpok.free;
          bmpabort.free;

     end;
end;

procedure TFrameDB.copiaDati;
var field: Tfield;
     i, CurEdit: integer;
     found: boolean;
begin

     curEdit := -1;
     for i := 1 to dbgrid2.ColumnCount do
     begin
   //  showmessage(DBGrid2.DataSource.DataSet.fields[i - 1].AsString);
       //   field := DBGrid2.Columns[i - 1].Field;
          field := DBGrid2.DataSource.DataSet.Fields[i - 1];



          if (field.DataType = ftString) or
               (field.DataType = ftinteger) or
               (field.DataType = ftinteger) or
               (field.DataType = ftsmallint) or
               (field.DataType = ftWord) or
               (field.DataType = ftfloat) or
               (field.DataType = ftFixedChar) or
               (field.DataType = ftWideString) or
               (field.DataType = ftLargeInt) or
               (field.DataType = ftBoolean) then
          begin
               found := false;
               inc(curEdit);
               while not found do
               begin
                    if (serviceform.Components[curEdit] is TMemo) or
                         (serviceform.Components[curEdit] is TCheckBox) then
                         found := true
                    else
                         inc(CurEdit);
               end;
               if (field.DataType = ftsmallint) or
                    (field.DataType = ftWord) or
                    (field.DataType = ftLargeInt) or
                    (field.DataType = ftinteger) then begin
                    if (TEdit(serviceform.Components[CurEdit]).text) <> '' then
                         field.AsInteger := strtoint(TEdit(serviceform.Components[CurEdit]).text)
                    else
                         field.value := null;
               end
               else if
                    (field.DataType = ftfloat) or
                    (field.DataType = ftCurrency) then
                    field.AsFloat := strtofloat(StringReplace(TEdit(serviceform.Components[CurEdit]).text, '.', ',', [rfReplaceAll]))
               else if (field.DataType = ftBoolean) then
                    field.AsBoolean := TCheckBox(serviceform.Components[CurEdit]).Checked
               else
                    field.AsString := Tmemo(serviceform.Components[CurEdit]).text;
          end;
     end;
     // DistruggiService;
end;

procedure TFrameDB.CancellaRiga;
begin
     if MessageDlg('I dati selezionati e quelli a loro collegati verranno persi irrimediabilmente. Continuare?', mtwarning, [mbyes, mbno], 0) = mrYes then
     try
          DataSourceGrid.DataSet.Delete;
     finally DistruggiService; end;
end;

procedure TFrameDB.InserisciRiga;
begin
     DataSourceGrid.DataSet.Insert;
     CopiaDati;
     try
          DataSourceGrid.DataSet.Post;
          DataSourceGrid.DataSet.Close;
          DataSourceGrid.DataSet.Open;
          //  except showmessage ('Dati inseriti inconsistenti, riprovare prego.');
     finally DistruggiService; end;
end;

procedure TFrameDB.ModificaRiga;
begin
     DataSourceGrid.DataSet.Edit;
     CopiaDati;
     try
          DataSourceGrid.DataSet.Post;
     finally DistruggiService; end;
     // except showmessage ('Dati inseriti inconsistenti, riprovare prego.');DataSourceGrid.dataset.refresh; DistruggiService; end;
end;

procedure TFrameDB.BInsClick(Sender: TObject);
begin
     goedit(inserisci);
end;

procedure TFrameDB.BModClick(Sender: TObject);
begin
     goedit(modifica);
end;

procedure TFrameDB.BDelClick(Sender: TObject);
begin
     goedit(cancella);
end;



end.

