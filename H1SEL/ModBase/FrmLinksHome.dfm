object FrmLinkwebHome: TFrmLinkwebHome
  Left = 0
  Top = 0
  Width = 104
  Height = 245
  DragCursor = crHandPoint
  TabOrder = 0
  object AdvPicture2: TAdvPicture
    Left = 8
    Top = 27
    Width = 89
    Height = 17
    Animate = False
    Picture.Stretch = False
    Picture.Frame = 0
    PicturePosition = bpTopLeft
    DragCursor = crHandPoint
    StretchMode = smShrink
    Visible = False
    OnClick = AdvPicture2Click
    Version = '1.3.1.2'
  end
  object AdvPicture1: TAdvPicture
    Left = 8
    Top = 8
    Width = 89
    Height = 17
    Animate = True
    Picture.Stretch = False
    Picture.Frame = 0
    PicturePosition = bpTopLeft
    DragKind = dkDock
    DragCursor = crHandPoint
    StretchMode = smShrink
    Visible = False
    OnClick = AdvPicture1Click
    Version = '1.3.1.2'
  end
  object AdvPicture3: TAdvPicture
    Left = 8
    Top = 47
    Width = 89
    Height = 17
    Animate = False
    Picture.Stretch = False
    Picture.Frame = 0
    PicturePosition = bpTopLeft
    DragCursor = crHandPoint
    StretchMode = smShrink
    Visible = False
    OnClick = AdvPicture3Click
    Version = '1.3.1.2'
  end
  object AdvPicture4: TAdvPicture
    Left = 8
    Top = 67
    Width = 89
    Height = 17
    Animate = True
    Picture.Stretch = False
    Picture.Frame = 0
    PicturePosition = bpTopLeft
    DragKind = dkDock
    DragCursor = crHandPoint
    StretchMode = smShrink
    Visible = False
    OnClick = AdvPicture4Click
    Version = '1.3.1.2'
  end
  object AdvPicture5: TAdvPicture
    Left = 8
    Top = 87
    Width = 89
    Height = 17
    Animate = True
    Picture.Stretch = False
    Picture.Frame = 0
    PicturePosition = bpTopLeft
    DragKind = dkDock
    DragCursor = crHandPoint
    StretchMode = smShrink
    Visible = False
    OnClick = AdvPicture5Click
    Version = '1.3.1.2'
  end
  object AdvPicture6: TAdvPicture
    Left = 8
    Top = 107
    Width = 89
    Height = 17
    Animate = True
    Picture.Stretch = False
    Picture.Frame = 0
    PicturePosition = bpTopLeft
    DragKind = dkDock
    DragCursor = crHandPoint
    StretchMode = smShrink
    Visible = False
    OnClick = AdvPicture6Click
    Version = '1.3.1.2'
  end
  object AdvPicture7: TAdvPicture
    Left = 8
    Top = 127
    Width = 89
    Height = 17
    Animate = True
    Picture.Stretch = False
    Picture.Frame = 0
    PicturePosition = bpTopLeft
    DragKind = dkDock
    DragCursor = crHandPoint
    StretchMode = smShrink
    Visible = False
    OnClick = AdvPicture7Click
    Version = '1.3.1.2'
  end
  object AdvPicture8: TAdvPicture
    Left = 8
    Top = 146
    Width = 89
    Height = 17
    Animate = True
    Picture.Stretch = False
    Picture.Frame = 0
    PicturePosition = bpTopLeft
    DragKind = dkDock
    DragCursor = crHandPoint
    StretchMode = smShrink
    Visible = False
    OnClick = AdvPicture8Click
    Version = '1.3.1.2'
  end
  object AdvPicture9: TAdvPicture
    Left = 8
    Top = 166
    Width = 89
    Height = 17
    Animate = True
    Picture.Stretch = False
    Picture.Frame = 0
    PicturePosition = bpTopLeft
    DragKind = dkDock
    DragCursor = crHandPoint
    StretchMode = smShrink
    Visible = False
    OnClick = AdvPicture9Click
    Version = '1.3.1.2'
  end
  object DSWebLink: TDataSource
    DataSet = QWebLink
    Left = 48
    Top = 184
  end
  object QWebLink: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    AfterOpen = QWebLinkAfterOpen
    DataSource = Data.DsAnagrafica
    Parameters = <
      item
        Name = 'ID'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select WebLinks.*, AnagWebLinks.Link'
      'from WebLinks'
      'left join AnagWebLinks on WebLinks.id=AnagWebLinks.IDWebLink'
      ''
      'where VisibileHome=1'
      'and IDAnagrafica=:ID')
    Left = 16
    Top = 184
    object QWebLinksID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QWebLinksDescrizione: TStringField
      FieldName = 'Descrizione'
      Size = 1024
    end
    object QWebLinksLogo: TBlobField
      FieldName = 'Logo'
      BlobType = ftBlob
    end
    object QWebLinkVisibileHome: TBooleanField
      FieldName = 'VisibileHome'
    end
    object QWebLinkLink: TStringField
      FieldName = 'Link'
      Size = 512
    end
  end
end
