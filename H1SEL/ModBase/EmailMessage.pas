unit EmailMessage;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, ExtCtrls, TB97;

type
     TEmailMessageForm = class(TForm)
          Panel1: TPanel;
          Panel15: TPanel;
          Memo1: TMemo;
          BitBtn1: TToolbarButton97;
          BitBtn2: TToolbarButton97;
          procedure FormShow(Sender: TObject);
          procedure FormCreate(Sender: TObject);
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
          procedure BitBtn1Click(Sender: TObject);
          procedure BitBtn2Click(Sender: TObject);
     private
          { Private declarations }
     public
          xCheckLines: boolean;
     end;

var
     EmailMessageForm: TEmailMessageForm;

implementation

uses Main;

{$R *.DFM}

procedure TEmailMessageForm.FormShow(Sender: TObject);
begin
     //Grafica
     EmailMessageForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel15.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel15.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel15.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     Caption := '[S/25] - ' + Caption;
end;

procedure TEmailMessageForm.FormCreate(Sender: TObject);
begin
     xCheckLines := False;
end;

procedure TEmailMessageForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     if (ModalResult = mrOK) and (xCheckLines) and (length(Memo1.lines.text) > 600) then begin
          MessageDlg('la lunghezza del messaggio eccede la dimensione massima consentita', mtError, [mbOK], 0);
          ModalResult := mrNone;
          Abort;
     end;
end;

procedure TEmailMessageForm.BitBtn1Click(Sender: TObject);
begin
     EmailMessageForm.ModalResult := mrOk;
end;

procedure TEmailMessageForm.BitBtn2Click(Sender: TObject);
begin
     EmailMessageForm.ModalResult := mrCancel;
end;

end.

