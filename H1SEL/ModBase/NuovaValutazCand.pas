unit NuovaValutazCand;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, ExtCtrls, dxDBTLCl, dxGrClms, dxTL, dxDBCtrl,
     dxDBGrid, dxCntner, Db, ADODB, TB97;

type
     TNuovaValutazCandForm = class(TForm)
          Panel1: TPanel;
          Panel2: TPanel;
          Label1: TLabel;
          dxDBGrid9: TdxDBGrid;
          QCandRic: TADOQuery;
          DsQCandRic: TDataSource;
          QCandRicID: TAutoIncField;
          QCandRicCognome: TStringField;
          QCandRicNome: TStringField;
          dxDBGrid4: TdxDBGrid;
          dxDBGrid4ID: TdxDBGridMaskColumn;
          dxDBGrid4Cognome: TdxDBGridMaskColumn;
          dxDBGrid4Nome: TdxDBGridMaskColumn;
          QValutaz: TADOQuery;
          DsQValutaz: TDataSource;
          QValutazID: TAutoIncField;
          QValutazVoce: TStringField;
          QValutazLegenda: TStringField;
          QValutazValore: TSmallintField;
          QValutazNote: TMemoField;
          dxDBGrid9ID: TdxDBGridMaskColumn;
          dxDBGrid9Voce: TdxDBGridMaskColumn;
          dxDBGrid9Legenda: TdxDBGridMaskColumn;
          dxDBGrid9Note: TdxDBGridBlobColumn;
          dxDBGrid9Valore: TdxDBGridSpinColumn;
          BitBtn1: TToolbarButton97;
          BitBtn2: TToolbarButton97;
          procedure QCandRicBeforeOpen(DataSet: TDataSet);
          procedure FormShow(Sender: TObject);
          procedure BitBtn2Click(Sender: TObject);
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
          procedure QValutazBeforeOpen(DataSet: TDataSet);
          procedure QCandRicAfterScroll(DataSet: TDataSet);
          procedure BitBtn1Click(Sender: TObject);
     private
    { Private declarations }
     public
          xIDRicerca, xIDUtente: integer;
     end;

var
     NuovaValutazCandForm: TNuovaValutazCandForm;

implementation

uses ModuloDati, Main;

{$R *.DFM}

procedure TNuovaValutazCandForm.QCandRicBeforeOpen(DataSet: TDataSet);
begin
     QCandRic.Parameters[0].value := xIDRicerca;
end;

procedure TNuovaValutazCandForm.FormShow(Sender: TObject);
begin
     //Grafica
     NuovaValutazCandForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     dxDBGrid9.Visible := False;
     QCandRic.open;
end;

procedure TNuovaValutazCandForm.BitBtn2Click(Sender: TObject);
begin
     Data.Q1.Close;
     Data.Q1.SQL.clear;
     Data.Q1.SQL.Text := 'insert into EBC_CandRic_Valutaz (IDCandRic,IDSelezionatore,IDVoce) ' +
          'select :x0,:x1,V.ID ' +
          'from EBC_Ricerche_valutaz_voci V ' +
          'where ID not in ( ' +
          '	select distinct IDVoce ' +
          '	from EBC_CandRic_Valutaz ' +
          '	where IDCandRic=:x2 ' +
          '       ) ' +
          'and V.IDRicerca = :x3';
     Data.Q1.Parameters[0].value := QCandRicID.Value;
     Data.Q1.Parameters[1].value := xIDUtente;
     Data.Q1.Parameters[2].value := QCandRicID.Value;
     Data.Q1.Parameters[3].value := xIDRicerca;
     Data.Q1.ExecSQL;

     QValutaz.Close;
     QValutaz.Open;
     dxDBGrid9.Visible := True;
end;

procedure TNuovaValutazCandForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     if DsQValutaz.state = dsEdit then QValutaz.Post;
end;

procedure TNuovaValutazCandForm.QValutazBeforeOpen(DataSet: TDataSet);
begin
     QValutaz.Parameters[0].value := QCandRicID.Value;
end;

procedure TNuovaValutazCandForm.QCandRicAfterScroll(DataSet: TDataSet);
begin
     dxDBGrid9.Visible := False;
end;

procedure TNuovaValutazCandForm.BitBtn1Click(Sender: TObject);
begin
     NuovaValutazCandForm.ModalResult := mrOK;
end;

end.

