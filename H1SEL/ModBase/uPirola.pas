unit uPirola;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     DtEdit97, DtEdDB97, StdCtrls, Mask, DBCtrls, ComCtrls, ExtCtrls, Db,
     ADODB, Buttons, TB97, shellAPI, JvSimpleXml, AdvPanel;

type
     TPirolaForm = class(TForm)
          Panel1: TPanel;
          Panel2: TPanel;
          Label1: TLabel;
          DBECognome: TDBEdit;
          Label2: TLabel;
          DBEdit2: TDBEdit;
          Label100: TLabel;
          DbDateEdit971: TDbDateEdit97;
          Label101: TLabel;
          DBEdit65: TDBEdit;
          Splitter1: TSplitter;
          qAnag: TADOQuery;
          dsAnag: TDataSource;
          qAnagid: TAutoIncField;
          qAnagcognome: TStringField;
          qAnagnome: TStringField;
          qAnagdatanascita: TDateTimeField;
          qAnagluogonascita: TStringField;
          qAnagDataUltimoCV: TDateTimeField;
          qAnagcliente: TStringField;
          qAnagRuolo: TStringField;
          qAnagprogressivo: TStringField;
          Panel3: TPanel;
          Label3: TLabel;
          Label4: TLabel;
          DbDateEdit972: TDbDateEdit97;
          DBEdit1: TDBEdit;
          qAnagRicerca: TStringField;
          BitBtn1: TBitBtn;
          Splitter2: TSplitter;
          qSelPirola: TADOQuery;
          dsSelPirola: TDataSource;
          qSelPirolaID: TAutoIncField;
          qSelPirolaIDAnagrafica: TIntegerField;
          qSelPirolaIDRicerca: TIntegerField;
          qSelPirolaDataInvioTest: TDateTimeField;
          qSelPirolaDataTest: TDateTimeField;
          qSelPirolaEsitoTest: TStringField;
          qSelPirolaDataPrimoColloquio: TDateTimeField;
          qSelPirolaNotePrimocolloquio: TMemoField;
          qSelPirolaDataEsitoPrimoColloquio: TDateTimeField;
          qSelPirolaEsitoPrimoColloquio: TStringField;
          qSelPirolaDataSecondoColloquio: TDateTimeField;
          qSelPirolaNoteSecondocolloquio: TMemoField;
          qSelPirolaDataEsitoSecondoColloquio: TDateTimeField;
          qSelPirolaEsitoSecondoColloquio: TStringField;
          qSelPirolaDataDecisione: TDateTimeField;
          qSelPirolaDecisione: TStringField;
          qSelPirolaNoteDecisione: TMemoField;
          qSelPirolaIDCandRic: TIntegerField;
          Splitter4: TSplitter;
          Panel4: TPanel;
          Panel5: TPanel;
          Panel6: TPanel;
          DbDateEdit974: TDbDateEdit97;
          Label7: TLabel;
          DbDateEdit975: TDbDateEdit97;
          Label8: TLabel;
          DBMemo1: TDBMemo;
          Label9: TLabel;
          DbDateEdit976: TDbDateEdit97;
          Label10: TLabel;
          Label11: TLabel;
          DBComboBox1: TDBComboBox;
          DBComboBox2: TDBComboBox;
          Label12: TLabel;
          DbDateEdit977: TDbDateEdit97;
          Label13: TLabel;
          DBMemo2: TDBMemo;
          Label14: TLabel;
          DbDateEdit978: TDbDateEdit97;
          Label15: TLabel;
          Label18: TLabel;
          DBEdit4: TDBEdit;
          qAnagResponsabile: TStringField;
          bInvioiTest: TToolbarButton97;
          bSalva1: TToolbarButton97;
          bSalva2: TToolbarButton97;
          bCancEsito1: TToolbarButton97;
          bCV: TToolbarButton97;
          qAnagRespEMAil: TStringField;
          qAnagusername: TStringField;
          qAnagpassword: TStringField;
          OpenDialog1: TOpenDialog;
          bFile: TToolbarButton97;
          Panel7: TPanel;
          Label5: TLabel;
          DbDateEdit973: TDbDateEdit97;
          Label6: TLabel;
          DBEdit3: TDBEdit;
          bCancTest: TToolbarButton97;
          ToolbarButton971: TToolbarButton97;
          Coll1: TAdvPanel;
          Label19: TLabel;
          DbDateEdit9710: TDbDateEdit97;
          Label20: TLabel;
          DBMemo4: TDBMemo;
          Label21: TLabel;
          DbDateEdit9711: TDbDateEdit97;
          Label22: TLabel;
          DBComboBox4: TDBComboBox;
          ToolbarButton972: TToolbarButton97;
          Coll2: TAdvPanel;
          Label23: TLabel;
          Label24: TLabel;
          DbDateEdit9712: TDbDateEdit97;
          Label25: TLabel;
          Label26: TLabel;
          DbDateEdit9713: TDbDateEdit97;
          Label27: TLabel;
          DBComboBox5: TDBComboBox;
          ToolbarButton973: TToolbarButton97;
          DBMemo5: TDBMemo;
          ToolbarButton974: TToolbarButton97;
          qSelPirolaIDUtenteRespPrimoColloquio: TIntegerField;
          qSelPirolaIDUtenteRespSecondoColloquio: TIntegerField;
          qSelPirolaDataTerzoColloquio: TDateTimeField;
          qSelPirolaNoteTerzocolloquio: TMemoField;
          qSelPirolaDataEsitoTerzoColloquio: TDateTimeField;
          qSelPirolaEsitoTerzoColloquio: TStringField;
          qSelPirolaIDUtenteRespTerzoColloquio: TIntegerField;
          qSelPirolaDataQuartoColloquio: TDateTimeField;
          qSelPirolaNoteQuartocolloquio: TMemoField;
          qSelPirolaDataEsitoQuartoColloquio: TDateTimeField;
          qSelPirolaEsitoQuartoColloquio: TStringField;
          qSelPirolaIDUtenteRespQuartoColloquio: TIntegerField;
          qSelPirolaDataQuintoColloquio: TDateTimeField;
          qSelPirolaNoteQuintocolloquio: TMemoField;
          qSelPirolaDataEsitoQuintoColloquio: TDateTimeField;
          qSelPirolaEsitoQuintoColloquio: TStringField;
          qSelPirolaIDUtenteRespQuintoColloquio: TIntegerField;
          qSelPirolaDataSestoColloquio: TDateTimeField;
          qSelPirolaNoteSestocolloquio: TMemoField;
          qSelPirolaDataEsitoSestoColloquio: TDateTimeField;
          qSelPirolaEsitoSestoColloquio: TStringField;
          qSelPirolaIDUtenteRespSestoColloquio: TIntegerField;
          qSelPirolaDataSettimoColloquio: TDateTimeField;
          qSelPirolaNoteSettimocolloquio: TMemoField;
          qSelPirolaDataEsitoSettimoColloquio: TDateTimeField;
          qSelPirolaEsitoSettimoColloquio: TStringField;
          qSelPirolaIDUtenteRespSettimoColloquio: TIntegerField;
          qSelPirolaDataOttavoColloquio: TDateTimeField;
          qSelPirolaNoteOttavocolloquio: TMemoField;
          qSelPirolaDataEsitoOttavoColloquio: TDateTimeField;
          qSelPirolaEsitoOttavoColloquio: TStringField;
          qSelPirolaIDUtenteRespOttavoColloquio: TIntegerField;
          qSelPirolaDataNonoColloquio: TDateTimeField;
          qSelPirolaNoteNonocolloquio: TMemoField;
          qSelPirolaDataEsitoNonoColloquio: TDateTimeField;
          qSelPirolaEsitoNonoColloquio: TStringField;
          qSelPirolaIDUtenteRespNonoColloquio: TIntegerField;
          qSelPirolaNumColloquioAttivo: TIntegerField;
          Coll3: TAdvPanel;
          Label28: TLabel;
          Label29: TLabel;
          Label30: TLabel;
          Label31: TLabel;
          Label32: TLabel;
          ToolbarButton975: TToolbarButton97;
          ToolbarButton976: TToolbarButton97;
          DbDateEdit9714: TDbDateEdit97;
          DbDateEdit9715: TDbDateEdit97;
          DBComboBox6: TDBComboBox;
          DBMemo6: TDBMemo;
          Coll4: TAdvPanel;
          Label33: TLabel;
          Label34: TLabel;
          Label35: TLabel;
          Label36: TLabel;
          Label37: TLabel;
          ToolbarButton977: TToolbarButton97;
          ToolbarButton978: TToolbarButton97;
          DbDateEdit9716: TDbDateEdit97;
          DbDateEdit9717: TDbDateEdit97;
          DBComboBox7: TDBComboBox;
          DBMemo7: TDBMemo;
          Coll5: TAdvPanel;
          Label38: TLabel;
          Label39: TLabel;
          Label40: TLabel;
          Label41: TLabel;
          Label42: TLabel;
          ToolbarButton979: TToolbarButton97;
          ToolbarButton9710: TToolbarButton97;
          DbDateEdit9718: TDbDateEdit97;
          DbDateEdit9719: TDbDateEdit97;
          DBComboBox8: TDBComboBox;
          DBMemo8: TDBMemo;
          Coll6: TAdvPanel;
          Label43: TLabel;
          Label44: TLabel;
          Label45: TLabel;
          Label46: TLabel;
          Label47: TLabel;
          ToolbarButton9711: TToolbarButton97;
          ToolbarButton9712: TToolbarButton97;
          DbDateEdit9720: TDbDateEdit97;
          DbDateEdit9721: TDbDateEdit97;
          DBComboBox9: TDBComboBox;
          DBMemo9: TDBMemo;
          Coll7: TAdvPanel;
          Label48: TLabel;
          Label49: TLabel;
          Label50: TLabel;
          Label51: TLabel;
          Label52: TLabel;
          ToolbarButton9713: TToolbarButton97;
          ToolbarButton9714: TToolbarButton97;
          DbDateEdit9722: TDbDateEdit97;
          DbDateEdit9723: TDbDateEdit97;
          DBComboBox10: TDBComboBox;
          DBMemo10: TDBMemo;
          Coll9: TAdvPanel;
          Label53: TLabel;
          Label54: TLabel;
          Label55: TLabel;
          Label56: TLabel;
          Label57: TLabel;
          ToolbarButton9715: TToolbarButton97;
          ToolbarButton9716: TToolbarButton97;
          DbDateEdit9724: TDbDateEdit97;
          DbDateEdit9725: TDbDateEdit97;
          DBComboBox11: TDBComboBox;
          DBMemo11: TDBMemo;
          Coll8: TAdvPanel;
          Label58: TLabel;
          Label59: TLabel;
          Label60: TLabel;
          Label61: TLabel;
          Label62: TLabel;
          ToolbarButton9717: TToolbarButton97;
          ToolbarButton9718: TToolbarButton97;
          DbDateEdit9726: TDbDateEdit97;
          DbDateEdit9727: TDbDateEdit97;
          DBComboBox12: TDBComboBox;
          DBMemo12: TDBMemo;
          coll10: TAdvPanel;
          bCancEsito2: TToolbarButton97;
          bSalva3: TToolbarButton97;
          DBMemo3: TDBMemo;
          DBComboBox3: TDBComboBox;
          Label17: TLabel;
          DbDateEdit979: TDbDateEdit97;
          Label16: TLabel;
          ToolbarButton9719: TToolbarButton97;
          ToolbarButton9720: TToolbarButton97;
          DBMemo13: TDBMemo;
          DBComboBox13: TDBComboBox;
          Label63: TLabel;
          DbDateEdit9728: TDbDateEdit97;
          Label64: TLabel;
          ToolbarButton9721: TToolbarButton97;
          ToolbarButton9722: TToolbarButton97;
          ToolbarButton9723: TToolbarButton97;
          ToolbarButton9724: TToolbarButton97;
          ToolbarButton9725: TToolbarButton97;
          ToolbarButton9726: TToolbarButton97;
          ToolbarButton9727: TToolbarButton97;
          ToolbarButton9728: TToolbarButton97;
          ToolbarButton9729: TToolbarButton97;
          ToolbarButton9730: TToolbarButton97;
          qTemp: TADOQuery;
          DBEdit5: TDBEdit;
          qUtentiLK: TADOQuery;
          qUtentiLKID: TAutoIncField;
          qUtentiLKNominativo: TStringField;
          qUtentiLKDescrizione: TStringField;
          qUtentiLKEmail: TStringField;
          qSelPirolaUntentePrimoColloquio: TStringField;
          qSelPirolaUtenteSecondoColloquio: TStringField;
          Label65: TLabel;
          Label66: TLabel;
          DBEdit6: TDBEdit;
          Label67: TLabel;
          DBEdit7: TDBEdit;
          Label68: TLabel;
          DBEdit8: TDBEdit;
          Label69: TLabel;
          DBEdit9: TDBEdit;
          Label70: TLabel;
          DBEdit10: TDBEdit;
          Label71: TLabel;
          DBEdit11: TDBEdit;
          Label72: TLabel;
          DBEdit12: TDBEdit;
          Label73: TLabel;
          DBEdit13: TDBEdit;
          qSelPirolaUtenteTerzoColloquio: TStringField;
          qSelPirolaUtenteQuartoColloquio: TStringField;
          qSelPirolaUtenteQuintoColloquio: TStringField;
          qSelPirolaUtenteSestoColloquio: TStringField;
          qSelPirolautenteSettimoColloquio: TStringField;
          qSelPirolaUtenteottavoColloquio: TStringField;
          qSelPirolaUtenteNonoColloquio: TStringField;
          ToolbarButton9731: TToolbarButton97;
          q1: TADOQuery;
          qSelPirolaIDUtenteValFinale: TIntegerField;
          procedure FormShow(Sender: TObject);
          procedure bCVClick(Sender: TObject);
          procedure bSalva1Click(Sender: TObject);
          procedure bSalva2Click(Sender: TObject);
          procedure bSalva3Click(Sender: TObject);
          procedure qSelPirolaAfterPost(DataSet: TDataSet);
          procedure qSelPirolaAfterOpen(DataSet: TDataSet);
          procedure bCancEsito1Click(Sender: TObject);
          procedure bCancEsito2Click(Sender: TObject);
          procedure bInvioiTestClick(Sender: TObject);
          procedure ToolbarButton971Click(Sender: TObject);
          procedure bCancTestClick(Sender: TObject);
          procedure bFileClick(Sender: TObject);
          procedure Coll1DblClick(Sender: TObject);
          procedure ToolbarButton975Click(Sender: TObject);
          procedure ToolbarButton977Click(Sender: TObject);
          procedure ToolbarButton979Click(Sender: TObject);
          procedure ToolbarButton9711Click(Sender: TObject);
          procedure ToolbarButton9713Click(Sender: TObject);
          procedure ToolbarButton9715Click(Sender: TObject);
          procedure ToolbarButton974Click(Sender: TObject);
          procedure ToolbarButton976Click(Sender: TObject);
          procedure ToolbarButton978Click(Sender: TObject);
          procedure ToolbarButton9710Click(Sender: TObject);
          procedure ToolbarButton9712Click(Sender: TObject);
          procedure ToolbarButton9714Click(Sender: TObject);
          procedure ToolbarButton9718Click(Sender: TObject);
          procedure ToolbarButton9716Click(Sender: TObject);
          procedure Coll2DblClick(Sender: TObject);
          procedure Coll3DblClick(Sender: TObject);
          procedure Coll4DblClick(Sender: TObject);
          procedure Coll5DblClick(Sender: TObject);
          procedure Coll6DblClick(Sender: TObject);
          procedure Coll7DblClick(Sender: TObject);
          procedure Coll8DblClick(Sender: TObject);
          procedure Coll9DblClick(Sender: TObject);
          procedure ToolbarButton9721Click(Sender: TObject);
          procedure ToolbarButton9728Click(Sender: TObject);
          procedure ToolbarButton9731Click(Sender: TObject);
          procedure ToolbarButton9717Click(Sender: TObject);
     private
    { Private declarations }
     public
    { Public declarations }
          xidcandric, xidanag: integer;
          xPositivo: boolean;
          procedure BloccaPannelli;
          procedure inviamail;
          procedure qualepannello;
          procedure vaiavalutazionefinale;


     end;

var
     PirolaForm: TPirolaForm;

implementation

uses ModuloDati, Main, Curriculum, GestFile, ElencoUtenti,
     AnagTestELearning;

{$R *.DFM}

procedure TPirolaForm.FormShow(Sender: TObject);
begin
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel3.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel4.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel5.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel6.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;


     Coll1.color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Coll2.color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Coll3.color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Coll4.color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Coll5.color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Coll6.color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Coll7.color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Coll8.color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Coll9.color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Coll10.color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;

     coll1.caption.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     coll2.caption.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     coll3.caption.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     coll4.caption.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     coll5.caption.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     coll6.caption.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     coll7.caption.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     coll8.caption.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     coll9.caption.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     coll10.caption.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;

     coll1.caption.CloseColor := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     coll2.caption.CloseColor := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     coll3.caption.CloseColor := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     coll4.caption.CloseColor := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     coll5.caption.CloseColor := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     coll6.caption.CloseColor := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     coll7.caption.CloseColor := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     coll8.caption.CloseColor := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     coll9.caption.CloseColor := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     coll10.caption.CloseColor := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;

     PirolaForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;



     Panel7.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     qanag.Close;
     qanag.Parameters[0].Value := xidanag;
     qanag.open;

     qSelPirola.Close;
     qSelPirola.Parameters[0].Value := xidcandric;
     qSelPirola.open;

    { if qSelPirolaEsitoPrimoColloquio.Value = 'Positivo' then Panel6.Enabled := true else Panel6.Enabled := false;
     if qSelPirolaEsitoSecondoColloquio.Text = 'Positivo' then Panel5.Enabled := true else Panel5.Enabled := false;
      }
     BloccaPannelli;
     qualepannello;
end;

procedure TPirolaForm.bCVClick(Sender: TObject);
var x: string;
     i: integer;
begin
    { if not MainForm.CheckProfile('051') then Exit;
     if data.Global.fieldbyname('debughrms').AsBoolean then Showmessage('0');
     if not Data.TAnagrafica.IsEmpty then begin

          if MainForm.xQualeSchedaCurr = '' then CurriculumForm.xQualeScheda := ''
          else CurriculumForm.xQualeScheda := MainForm.xQualeSchedaCurr;
          CurriculumForm.ShowModal;

          // refresh anagrafica
          if Data.dsAnagrafica.State = dsEdit then begin
               //[/TONI20020905\]
               with Data.TAnagrafica do begin
                    Data.DB.BeginTrans;
                    try
                         //                        ApplyUpdates;
                         Post;
                         Data.DB.CommitTrans;
                    except
                         Data.DB.RollbackTrans;
                         MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                         raise;
                    end;
                    //                    CommitUpdates;
                    //[/TONI20020905\]FINE
               end;
          end;
     end;    }
     Mainform.BAnagCurriculumClick(self);
end;


procedure TPirolaForm.bSalva1Click(Sender: TObject);
begin
{     if qselpirola.state = dsedit then begin
          qSelPirolaNumColloquioAttivo.Value := 2;
          qSelPirola.Post;
          inviamail;
     end;
 }
     if qselpirola.state = dsedit then qSelPirola.Post;
     if qSelPirolaEsitoPrimoColloquio.AsString = 'Positivo' then
          xpositivo := true else xpositivo := false;
     ToolbarButton9728Click(self);
end;

procedure TPirolaForm.bSalva2Click(Sender: TObject);
begin
    { if qselpirola.state = dsedit then begin
          qSelPirolaNumColloquioAttivo.Value := 3;
          qSelPirola.Post;
          inviamail;
     end;     }
     if qSelPirolaEsitoSecondoColloquio.AsString = 'Positivo' then
          xpositivo := true else xpositivo := false;
     ToolbarButton9728Click(self);
end;

procedure TPirolaForm.bSalva3Click(Sender: TObject);
begin

    { if qselpirola.state = dsedit then begin qSelPirola.Post;
          qSelPirolaNumColloquioAttivo.Value := 10;
          inviamail;

     end;  }
     ToolbarButton9728Click(self);
end;

procedure TPirolaForm.BloccaPannelli;
begin
     if qSelPirolaDataInvioTest.Value > 0 then begin
          Panel4.Enabled := true;
          if qSelPirolaDataTest.Value > 0 then begin
               dbdateedit975.Color := clWindow;
               dbdateedit976.Color := clWindow;
               dbmemo1.Color := clWindow;
               dbCombobox1.Color := clWindow;
               dbdateedit975.Enabled := true;
               dbdateedit976.Enabled := true;
               dbmemo1.Enabled := true;
               dbCombobox1.Enabled := true;
          end else begin
               dbdateedit975.Color := clInactiveBorder;
               dbdateedit976.Color := clInactiveBorder;
               dbmemo1.Color := clInactiveBorder;
               dbCombobox1.Color := clInactiveBorder;
               dbdateedit975.Enabled := false;
               dbdateedit976.Enabled := false;
               dbmemo1.Enabled := false;
               dbCombobox1.Enabled := false;
          end;
     end else begin
          Panel4.Enabled := false;
          dbdateedit975.Color := clInactiveBorder;
          dbdateedit976.Color := clInactiveBorder;
          dbmemo1.Color := clInactiveBorder;
          dbCombobox1.Color := clInactiveBorder;
     end;

     if qSelPirolaEsitoPrimoColloquio.asstring = 'Positivo' then begin
          Panel6.Enabled := true;
          DbDateEdit977.Color := clWindow;
          DbDateEdit978.Color := clWindow;
          dbmemo2.Color := clWindow;
          dbcombobox2.Color := clWindow;
          Panel4.Enabled := false;
     end else begin
          Panel6.Enabled := false;
          DbDateEdit977.Color := clInactiveBorder;
          DbDateEdit978.Color := clInactiveBorder;
          dbmemo2.Color := clInactiveBorder;
          dbcombobox2.Color := clInactiveBorder;
          Panel4.Enabled := true;
     end;


     if qSelPirolaEsitoSecondoColloquio.asstring = 'Positivo' then begin
          Panel5.Enabled := true;
          DbDateEdit979.Color := clWindow;
          dbmemo3.Color := clWindow;
          dbcombobox3.Color := clWindow;
          Panel4.Enabled := false;
          Panel6.Enabled := false;


     end else begin
          Panel5.Enabled := false;
          DbDateEdit979.Color := clInactiveBorder;
          dbmemo3.Color := clInactiveBorder;
          dbcombobox3.Color := clInactiveBorder;
     end;
end;

procedure TPirolaForm.qSelPirolaAfterPost(DataSet: TDataSet);
begin
     BloccaPannelli;
     qualepannello;
end;

procedure TPirolaForm.qSelPirolaAfterOpen(DataSet: TDataSet);
begin
     BloccaPannelli;
end;

procedure TPirolaForm.bCancEsito1Click(Sender: TObject);
begin
     if dsSelPirola.State = dsBrowse then begin
          qselpirola.Edit
     end;
     qSelPirolaEsitoPrimoColloquio.Value := '';
     qSelPirolaNumColloquioAttivo.Value := qSelPirolaNumColloquioAttivo.Value - 1;
     qSelPirola.Post;


end;

procedure TPirolaForm.bCancEsito2Click(Sender: TObject);
var x: integer;
begin

     if dsSelPirola.State = dsBrowse then begin
          qselpirola.Edit
     end;
     qSelPirolaDecisione.Value := '';
     qSelPirolaIDUtenteValFinale.AsString := '';

     if qSelPirolaEsitoPrimoColloquio.AsString <> '' then x := 1;
     if qSelPirolaEsitosecondoColloquio.AsString <> '' then x := 2;
     if qSelPirolaEsitoTerzoColloquio.AsString <> '' then x := 3;
     if qSelPirolaEsitoQuartoColloquio.AsString <> '' then x := 4;
     if qSelPirolaEsitoQuintoColloquio.AsString <> '' then x := 5;
     if qSelPirolaEsitoSestoColloquio.AsString <> '' then x := 6;
     if qSelPirolaEsitoSettimoColloquio.AsString <> '' then x := 7;
     if qSelPirolaEsitoOttavoColloquio.AsString <> '' then x := 8;
     if qSelPirolaEsitoNonoColloquio.AsString <> '' then x := 9;
     qSelPirolaNumColloquioAttivo.Value := x;
     qSelPirola.Post;
end;

procedure TPirolaForm.inviamail;
var tlog: tstringlist;
begin
     tlog := tstringlist.Create;

    { if (qSelPirolaEsitoPrimoColloquio.Value = 'Positivo') and (qSelPirolaEsitoSecondoColloquio.asstring <> 'Positivo') then begin
          if messagedlg('vuoi inviare una mail al responsabile della divisione?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then begin

               tlog.Add('mailto:' + qAnagRespEMAil.Value + '?subject=' + 'Esito primo colloquio di ' + qAnagcognome.AsString + ' ' + qAnagnome.AsString + '&body=' + qSelPirolaDataPrimoColloquio.AsString + chr(13) + stringreplace(dbmemo1.text, '&', '%26', [rfReplaceAll]));
               ShellExecute(0, 'Open', pchar('mailto:' + qAnagRespEMAil.Value + '?subject=' + 'Esito primo colloquio di ' + qAnagcognome.AsString + ' ' + qAnagnome.AsString + '&body=' + qSelPirolaDataPrimoColloquio.AsString + chr(13) + stringreplace(dbmemo1.text, '&', '%26', [rfReplaceAll])), '', '', SW_SHOW);
          end;

     end;
     if qSelPirolaEsitoSecondoColloquio.Value = 'Positivo' then begin
          if messagedlg('vuoi inviare una mail al rag. Pennuto?', mtConfirmation, [mbYes, mbNo], 0) = mrYes then begin
               tlog.Add('mailto:pennuto@pennuto.it?subject=' + 'Esito secondo colloquio di ' + qAnagcognome.AsString + ' ' + qAnagnome.AsString + '&body=' + qSelPirolaDataSecondoColloquio.AsString + chr(13) + stringreplace(dbmemo2.text, '&', '%26', [rfReplaceAll]));
               ShellExecute(0, 'Open', pchar('mailto:pennuto@pennuto.it?subject=' + 'Esito secondo colloquio di ' + qAnagcognome.AsString + ' ' + qAnagnome.AsString + '&body=' + qSelPirolaDataSecondoColloquio.AsString + chr(13) + stringreplace(dbmemo2.text, '&', '%26', [rfReplaceAll])), '', '', SW_SHOW);
          end;
     end;    }

     tlog.SaveToFile(data.Global.fieldbyname('DirFileLog').AsString + '\LogEmailPirola.txt');
     tlog.free;
end;

procedure TPirolaForm.bInvioiTestClick(Sender: TObject);
var xTestoXML: tstringList;
     d, m, y: WORD;
     ds, ms, ys: string;
     xdatainvio: string;
     xCodiceCorso: string;
begin
     DecodeDate(qSelPirolaDataInvioTest.AsDateTime, y, m, d);
     ds := IntToStr(d);
     ms := IntToStr(m);
     ys := IntToStr(y);

     if length(ds) = 1 then ds := '0' + ds;
     if length(ms) = 1 then ms := '0' + ms;

     xdatainvio := ys + '-' + ms + '-' + ds;
     xcodicecorso := 'TV01';



     q1.close;
     q1.sql.text := 'select t.* from testelearning T join anagtestelearning at on t.id=at.idtest where idanagrafica=:x0';
     q1.Parameters[0].Value := qAnagid.Value;
     q1.Open;
     if q1.RecordCount > 0 then begin

          xTestoXML := TStringList.Create;
          xTestoXML.Text := '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>' + chr(13) +
               '<corsi_docebo data_richiesta="' + xdatainvio + '">' + chr(13) +
               '<utente LoginUtente="' + 'HR-' + qAnagid.AsString + '" Nome="' + qAnagnome.Value + '" Cognome="' + qAnagcognome.Value + '" Password="' + qAnagpassword.value + '">'; //+ chr(13);
          while not q1.eof do begin
               xTestoXML.add('<corso_abilitato idCorso="' + q1.fieldbyname('CodiceTest').AsString + '">'); //+ chr(13) +
               xTestoXML.add('</corso_abilitato>'); //+ chr(13) +
               q1.next;
          end;
          xTestoXML.add('</utente>'); // + chr(13) +
          xTestoXML.add('</corsi_docebo>');
          xTestoXML.SaveToFile(ExtractFileDir(application.ExeName) + '\HR-' + qAnagid.AsString + 'DoceboIN.xml');

          if dsSelPirola.State = dsbrowse then
               qselpirola.Edit;
          qSelPirolaDataInvioTest.Value := Date;
          qselPirola.Post;
          MessageDLG('File HR-' + qAnagid.AsString + 'DoceboIN.xml correttamente creato', mtWarning, [mbok], 0);
     end else
          MessageDLG('Non � stato selezionato nessun test.', mtWarning, [mbok], 0);
     xTestoXML.Free;
end;

procedure TPirolaForm.ToolbarButton971Click(Sender: TObject);
var xXML: TJvSimpleXml;
     xnomecorso, xidcorso, xpunteggio: string;
     xdatacorso, xidutente: string;
begin
     xXML := TJvSimpleXml.Create(nil);
     try
          OpenDialog1.Execute;

          xXML.LoadFromFile(Opendialog1.FileName);

          xnomecorso := xXML.root.Items.ItemNamed['utente'].Items.ItemNamed['corso_sostenuto'].Properties.ItemNamed['NomeCorso'].Value;
          xidcorso := xXML.root.Items.ItemNamed['utente'].Items.ItemNamed['corso_sostenuto'].Properties.ItemNamed['idCorso'].Value;
          xpunteggio := xXML.root.Items.ItemNamed['utente'].Items.ItemNamed['corso_sostenuto'].Properties.ItemNamed['punteggio'].Value;


          xdatacorso := xXML.Root.Properties.ItemNamed['data_export'].Value;
          xidutente := xXML.root.Items.ItemNamed['utente'].Properties.ItemNamed['LoginUtente'].Value;

          xidutente := copy(xidutente, pos('-', xidutente) + 1, length(xidutente));

          if dsSelPirola.State = dsbrowse then
               qselpirola.Edit;

          qSelPirolaDataTest.Value := strtodatetime(copy(xdatacorso, 9, 2) + '/' + copy(xdatacorso, 6, 2) + '/' + copy(xdatacorso, 1, 4));
          qSelPirolaEsitoTest.Value := xpunteggio;

          qselpirola.Post;

     except

     end;
     xXML.Free;
end;

procedure TPirolaForm.bCancTestClick(Sender: TObject);
begin
     if Messagedlg('Vuoi davvero cancellare l''esito e la data del test? Sar� poi necessario procedere con un nuovo caricamento dell''esito', mtWarning, [mbYes, mbNo], 0) = mryes then begin
          if dsSelPirola.State = dsbrowse then
               qselpirola.Edit;

          qSelPirolaDataTest.Value := 0;
          qSelPirolaEsitoTest.Value := '';

          qselpirola.Post;

     end;
end;

procedure TPirolaForm.bFileClick(Sender: TObject);
begin
     GestFileForm := TGestFileForm.Create(self);
     if data.QAnagFile.active = false then data.qAnagFile.open;
     GEstFileForm.ShowModal;
     GestFileForm.Free;
end;

procedure TPirolaForm.Coll1DblClick(Sender: TObject);
begin
     if Coll1.Collaps = true then Coll1.Collaps := false
     else
          Coll1.Collaps := true;
end;

procedure TPirolaForm.qualepannello;
var labelpanel: array[1..10] of TLabel;
     i: integer;
begin
     coll10.enabled := false;
     coll10.collaps := true;
     {labelpanel[1] := lColl1;
     labelpanel[2] := lColl2;
     labelpanel[3] := lColl3;
     labelpanel[4] := lColl4;
     labelpanel[5] := lColl5;
     labelpanel[6] := lColl6;
     labelpanel[7] := lColl7;
     labelpanel[8] := lColl8;
     labelpanel[9] := lColl9;
     labelpanel[10] := lColl10;
      }
     if qSelPirolaNumColloquioAttivo.AsInteger < 1 then begin
          coll1.Collaps := true;
          coll2.collaps := true;
          coll3.collaps := true;
          coll4.collaps := true;
          coll5.collaps := true;
          coll6.collaps := true;
          coll7.collaps := true;
          coll8.collaps := true;
          coll9.collaps := true;


          coll1.enabled := false;
          coll2.enabled := false;
          coll3.enabled := false;
          coll4.enabled := false;
          coll5.enabled := false;
          coll6.enabled := false;
          coll7.enabled := false;
          coll8.enabled := false;
          coll9.enabled := false;

          coll1.Caption.Font.Style := [];
          coll2.Caption.Font.Style := [];
          coll3.Caption.Font.Style := [];
          coll4.Caption.Font.Style := [];
          coll5.Caption.Font.Style := [];
          coll6.Caption.Font.Style := [];
          coll7.Caption.Font.Style := [];
          coll8.Caption.Font.Style := [];
          coll9.Caption.Font.Style := [];
          coll10.Caption.Font.Style := [];

     end else begin
          if qSelPirolaNumColloquioAttivo.AsInteger = 1 then begin
               coll1.Collaps := false;
               coll2.collaps := true;
               coll3.collaps := true;
               coll4.collaps := true;
               coll5.collaps := true;
               coll6.collaps := true;
               coll7.collaps := true;
               coll8.collaps := true;
               coll9.collaps := true;


               coll1.enabled := true;
               coll2.enabled := false;
               coll3.enabled := false;
               coll4.enabled := false;
               coll5.enabled := false;
               coll6.enabled := false;
               coll7.enabled := false;
               coll8.enabled := false;
               coll9.enabled := false;

               coll1.Caption.Font.Style := [fsBold];
               coll2.Caption.Font.Style := [];
               coll3.Caption.Font.Style := [];
               coll4.Caption.Font.Style := [];
               coll5.Caption.Font.Style := [];
               coll6.Caption.Font.Style := [];
               coll7.Caption.Font.Style := [];
               coll8.Caption.Font.Style := [];
               coll9.Caption.Font.Style := [];
               coll10.Caption.Font.Style := [];

          end;
          if qSelPirolaNumColloquioAttivo.AsInteger = 2 then begin
               coll1.Collaps := true;
               coll2.collaps := false;
               coll3.collaps := true;
               coll4.collaps := true;
               coll5.collaps := true;
               coll6.collaps := true;
               coll7.collaps := true;
               coll8.collaps := true;
               coll9.collaps := true;


               coll1.enabled := true;
               coll2.enabled := true;
               coll3.enabled := false;
               coll4.enabled := false;
               coll5.enabled := false;
               coll6.enabled := false;
               coll7.enabled := false;
               coll8.enabled := false;
               coll9.enabled := false;

               coll1.Caption.Font.Style := [fsBold];
               coll2.Caption.Font.Style := [fsBold];
               coll3.Caption.Font.Style := [];
               coll4.Caption.Font.Style := [];
               coll5.Caption.Font.Style := [];
               coll6.Caption.Font.Style := [];
               coll7.Caption.Font.Style := [];
               coll8.Caption.Font.Style := [];
               coll9.Caption.Font.Style := [];
               coll10.Caption.Font.Style := [];

          end;
          if qSelPirolaNumColloquioAttivo.AsInteger = 3 then begin
               coll1.Collaps := true;
               coll2.collaps := true;
               coll3.collaps := false;
               coll4.collaps := true;
               coll5.collaps := true;
               coll6.collaps := true;
               coll7.collaps := true;
               coll8.collaps := true;
               coll9.collaps := true;

               coll1.enabled := false;
               coll2.enabled := true;
               coll3.enabled := true;
               coll4.enabled := false;
               coll5.enabled := false;
               coll6.enabled := false;
               coll7.enabled := false;
               coll8.enabled := false;
               coll9.enabled := false;

               coll1.Caption.Font.Style := [fsBold];
               coll2.Caption.Font.Style := [fsBold];
               coll3.Caption.Font.Style := [fsBold];
               coll4.Caption.Font.Style := [];
               coll5.Caption.Font.Style := [];
               coll6.Caption.Font.Style := [];
               coll7.Caption.Font.Style := [];
               coll8.Caption.Font.Style := [];
               coll9.Caption.Font.Style := [];
               coll10.Caption.Font.Style := [];
          end;
          if qSelPirolaNumColloquioAttivo.AsInteger = 4 then begin
               coll1.Collaps := true;
               coll2.collaps := true;
               coll3.collaps := true;
               coll4.collaps := false;
               coll5.collaps := true;
               coll6.collaps := true;
               coll7.collaps := true;
               coll8.collaps := true;
               coll9.collaps := true;


               coll1.enabled := false;
               coll2.enabled := false;
               coll3.enabled := true;
               coll4.enabled := true;
               coll5.enabled := false;
               coll6.enabled := false;
               coll7.enabled := false;
               coll8.enabled := false;
               coll9.enabled := false;

               coll1.Caption.Font.Style := [fsBold];
               coll2.Caption.Font.Style := [fsBold];
               coll3.Caption.Font.Style := [fsBold];
               coll4.Caption.Font.Style := [fsBold];
               coll5.Caption.Font.Style := [];
               coll6.Caption.Font.Style := [];
               coll7.Caption.Font.Style := [];
               coll8.Caption.Font.Style := [];
               coll9.Caption.Font.Style := [];
               coll10.Caption.Font.Style := [];

          end;
          if qSelPirolaNumColloquioAttivo.AsInteger = 5 then begin
               coll1.Collaps := true;
               coll2.collaps := true;
               coll3.collaps := true;
               coll4.collaps := true;
               coll5.collaps := false;
               coll6.collaps := true;
               coll7.collaps := true;
               coll8.collaps := true;
               coll9.collaps := true;

               coll1.enabled := false;
               coll2.enabled := false;
               coll3.enabled := false;
               coll4.enabled := true;
               coll5.enabled := true;
               coll6.enabled := false;
               coll7.enabled := false;
               coll8.enabled := false;
               coll9.enabled := false;


               coll1.Caption.Font.Style := [fsBold];
               coll2.Caption.Font.Style := [fsBold];
               coll3.Caption.Font.Style := [fsBold];
               coll4.Caption.Font.Style := [fsBold];
               coll5.Caption.Font.Style := [fsBold];
               coll6.Caption.Font.Style := [];
               coll7.Caption.Font.Style := [];
               coll8.Caption.Font.Style := [];
               coll9.Caption.Font.Style := [];
               coll10.Caption.Font.Style := [];
          end;
          if qSelPirolaNumColloquioAttivo.AsInteger = 6 then begin
               coll1.Collaps := true;
               coll2.collaps := true;
               coll3.collaps := true;
               coll4.collaps := true;
               coll5.collaps := true;
               coll6.collaps := false;
               coll7.collaps := true;
               coll8.collaps := true;
               coll9.collaps := true;

               coll1.enabled := false;
               coll2.enabled := false;
               coll3.enabled := false;
               coll4.enabled := false;
               coll5.enabled := true;
               coll6.enabled := true;
               coll7.enabled := false;
               coll8.enabled := false;
               coll9.enabled := false;

               coll1.Caption.Font.Style := [fsBold];
               coll2.Caption.Font.Style := [fsBold];
               coll3.Caption.Font.Style := [fsBold];
               coll4.Caption.Font.Style := [fsBold];
               coll5.Caption.Font.Style := [fsBold];
               coll6.Caption.Font.Style := [fsBold];
               coll7.Caption.Font.Style := [];
               coll8.Caption.Font.Style := [];
               coll9.Caption.Font.Style := [];
               coll10.Caption.Font.Style := [];
          end;
          if qSelPirolaNumColloquioAttivo.AsInteger = 7 then begin
               coll1.Collaps := true;
               coll2.collaps := true;
               coll3.collaps := true;
               coll4.collaps := true;
               coll5.collaps := true;
               coll6.collaps := true;
               coll7.collaps := false;
               coll8.collaps := true;
               coll9.collaps := true;

               coll1.enabled := false;
               coll2.enabled := false;
               coll3.enabled := false;
               coll4.enabled := false;
               coll5.enabled := false;
               coll6.enabled := true;
               coll7.enabled := true;
               coll8.enabled := false;
               coll9.enabled := false;


               coll1.Caption.Font.Style := [fsBold];
               coll2.Caption.Font.Style := [fsBold];
               coll3.Caption.Font.Style := [fsBold];
               coll4.Caption.Font.Style := [fsBold];
               coll5.Caption.Font.Style := [fsBold];
               coll6.Caption.Font.Style := [fsBold];
               coll7.Caption.Font.Style := [fsBold];
               coll8.Caption.Font.Style := [];
               coll9.Caption.Font.Style := [];
               coll10.Caption.Font.Style := [];
          end;
          if qSelPirolaNumColloquioAttivo.AsInteger = 8 then begin
               coll1.Collaps := true;
               coll2.collaps := true;
               coll3.collaps := true;
               coll4.collaps := true;
               coll5.collaps := true;
               coll6.collaps := true;
               coll7.collaps := true;
               coll8.collaps := false;
               coll9.collaps := true;

               coll1.enabled := false;
               coll2.enabled := false;
               coll3.enabled := false;
               coll4.enabled := false;
               coll5.enabled := false;
               coll6.enabled := false;
               coll7.enabled := true;
               coll8.enabled := true;
               coll9.enabled := false;

               coll1.Caption.Font.Style := [fsBold];
               coll2.Caption.Font.Style := [fsBold];
               coll3.Caption.Font.Style := [fsBold];
               coll4.Caption.Font.Style := [fsBold];
               coll5.Caption.Font.Style := [fsBold];
               coll6.Caption.Font.Style := [fsBold];
               coll7.Caption.Font.Style := [fsBold];
               coll8.Caption.Font.Style := [fsBold];
               coll9.Caption.Font.Style := [];
               coll10.Caption.Font.Style := [];
          end;
          if qSelPirolaNumColloquioAttivo.AsInteger = 9 then begin
               coll1.Collaps := true;
               coll2.collaps := true;
               coll3.collaps := true;
               coll4.collaps := true;
               coll5.collaps := true;
               coll6.collaps := true;
               coll7.collaps := true;
               coll8.collaps := true;
               coll9.collaps := false;

               coll1.enabled := false;
               coll2.enabled := false;
               coll3.enabled := false;
               coll4.enabled := false;
               coll5.enabled := false;
               coll6.enabled := false;
               coll7.enabled := false;
               coll8.enabled := true;
               coll9.enabled := true;


               coll1.Caption.Font.Style := [fsBold];
               coll2.Caption.Font.Style := [fsBold];
               coll3.Caption.Font.Style := [fsBold];
               coll4.Caption.Font.Style := [fsBold];
               coll5.Caption.Font.Style := [fsBold];
               coll6.Caption.Font.Style := [fsBold];
               coll7.Caption.Font.Style := [fsBold];
               coll8.Caption.Font.Style := [fsBold];
               coll9.Caption.Font.Style := [fsBold];
               coll10.Caption.Font.Style := [];


          end;
          if qSelPirolaNumColloquioAttivo.AsInteger = 10 then begin
               coll1.Collaps := true;
               coll2.collaps := true;
               coll3.collaps := true;
               coll4.collaps := true;
               coll5.collaps := true;
               coll6.collaps := true;
               coll7.collaps := true;
               coll8.collaps := true;
               coll9.collaps := true;
               coll10.Collaps := false;

               coll1.enabled := true;
               coll2.enabled := true;
               coll3.enabled := true;
               coll4.enabled := true;
               coll5.enabled := true;
               coll6.enabled := true;
               coll7.enabled := true;
               coll8.enabled := true;
               coll9.enabled := true;
               coll10.enabled := true;


               if qSelPirolaEsitoPrimoColloquio.asString <> '' then
                    coll1.Caption.Font.Style := [fsBold];
               if qSelPirolaEsitoSecondoColloquio.asString <> '' then
                    coll2.Caption.Font.Style := [fsBold];
               if qSelPirolaEsitoTerzoColloquio.asString <> '' then
                    coll3.Caption.Font.Style := [fsBold];
               if qSelPirolaEsitoQuartoColloquio.asString <> '' then
                    coll4.Caption.Font.Style := [fsBold];
               if qSelPirolaEsitoQuintoColloquio.asString <> '' then
                    coll5.Caption.Font.Style := [fsBold];
               if qSelPirolaEsitoSestoColloquio.asString <> '' then
                    coll6.Caption.Font.Style := [fsBold];
               if qSelPirolaEsitoSettimoColloquio.asString <> '' then
                    coll7.Caption.Font.Style := [fsBold];
               if qSelPirolaEsitoOttavoColloquio.asString <> '' then
                    coll8.Caption.Font.Style := [fsBold];
               if qSelPirolaEsitoNonoColloquio.asString <> '' then
                    coll9.Caption.Font.Style := [fsBold];


               coll10.Caption.Font.Style := [fsBold];

          end;
         { for i := 1 to 10 do begin

               if i <= qSelPirolaNumColloquioAttivo.Value then
                    labelpanel[i].Canvas.Font.Style := [fsbold];
          end;}

     end;
end;

procedure TPirolaForm.ToolbarButton975Click(Sender: TObject);
begin
    { if qselpirola.state = dsedit then begin
          qSelPirolaNumColloquioAttivo.Value := 4;
          qSelPirola.Post;
          inviamail;
     end;}
     if qSelPirolaEsitoTerzoColloquio.value = 'Positivo' then
          xpositivo := true else xpositivo := false;
     ToolbarButton9728Click(self);
end;

procedure TPirolaForm.ToolbarButton977Click(Sender: TObject);
begin
     {if qselpirola.state = dsedit then begin
          qSelPirolaNumColloquioAttivo.Value := 5;
          qSelPirola.Post;
          inviamail;
     end;}
     if qSelPirolaEsitoQuartoColloquio.value = 'Positivo' then
          xpositivo := true else xpositivo := false;
     ToolbarButton9728Click(self);
end;

procedure TPirolaForm.ToolbarButton979Click(Sender: TObject);
begin
     {if qselpirola.state = dsedit then begin
          qSelPirolaNumColloquioAttivo.Value := 6;
          qSelPirola.Post;
          inviamail;
     end;}
     if qSelPirolaEsitoQuintoColloquio.value = 'Positivo' then
          xpositivo := true else xpositivo := false;
     ToolbarButton9728Click(self);
end;

procedure TPirolaForm.ToolbarButton9711Click(Sender: TObject);
begin
     {if qselpirola.state = dsedit then begin
          qSelPirolaNumColloquioAttivo.Value := 7;
          qSelPirola.Post;
          inviamail;
     end;}

     if qSelPirolaEsitoSestoColloquio.value = 'Positivo' then
          xpositivo := true else xpositivo := false;
     ToolbarButton9728Click(self);
end;

procedure TPirolaForm.ToolbarButton9713Click(Sender: TObject);
begin
     if qSelPirolaEsitoSettimoColloquio.value = 'Positivo' then
          xpositivo := true else xpositivo := false;
     ToolbarButton9728Click(self);


end;

procedure TPirolaForm.ToolbarButton9715Click(Sender: TObject);
begin
     {if qselpirola.state = dsedit then begin
          qSelPirolaNumColloquioAttivo.Value := 9;
          qSelPirola.Post;
          inviamail;
     end;}
     if qSelPirolaEsitoNonoColloquio.value = 'Positivo' then
          xpositivo := true else xpositivo := false;
     ToolbarButton9728Click(self);

     ToolbarButton9728Click(self);
end;

procedure TPirolaForm.ToolbarButton974Click(Sender: TObject);
begin
     if dsSelPirola.State = dsBrowse then begin
          qselpirola.Edit
     end;
     qSelPirolaEsitoPrimoColloquio.Value := '';
     qSelPirolaNumColloquioAttivo.Value := 1;
     qSelPirola.Post;
end;

procedure TPirolaForm.ToolbarButton976Click(Sender: TObject);
begin
     if dsSelPirola.State = dsBrowse then begin
          qselpirola.Edit
     end;
     qSelPirolaEsitoSecondoColloquio.Value := '';
     qSelPirolaNumColloquioAttivo.Value := 2;
     qSelPirola.Post;
end;

procedure TPirolaForm.ToolbarButton978Click(Sender: TObject);
begin
     if dsSelPirola.State = dsBrowse then begin
          qselpirola.Edit
     end;
     qSelPirolaEsitoTerzoColloquio.Value := '';
     qSelPirolaNumColloquioAttivo.Value := 3;
     qSelPirola.Post;
end;

procedure TPirolaForm.ToolbarButton9710Click(Sender: TObject);
begin
     if dsSelPirola.State = dsBrowse then begin
          qselpirola.Edit
     end;
     qSelPirolaEsitoQuartoColloquio.Value := '';
     qSelPirolaNumColloquioAttivo.Value := 4;
     qSelPirola.Post;
end;

procedure TPirolaForm.ToolbarButton9712Click(Sender: TObject);
begin
     if dsSelPirola.State = dsBrowse then begin
          qselpirola.Edit
     end;
     qSelPirolaEsitoQuintoColloquio.Value := '';
     qSelPirolaNumColloquioAttivo.Value := 5;
     qSelPirola.Post;
end;

procedure TPirolaForm.ToolbarButton9714Click(Sender: TObject);
begin
     if dsSelPirola.State = dsBrowse then begin
          qselpirola.Edit
     end;
     qSelPirolaEsitoSestoColloquio.Value := '';
     qSelPirolaNumColloquioAttivo.Value := 6;
     qSelPirola.Post;
end;

procedure TPirolaForm.ToolbarButton9718Click(Sender: TObject);
begin
     if dsSelPirola.State = dsBrowse then begin
          qselpirola.Edit
     end;
     qSelPirolaEsitoSettimoColloquio.Value := '';
     qSelPirolaNumColloquioAttivo.Value := 7;
     qSelPirola.Post;
end;

procedure TPirolaForm.ToolbarButton9716Click(Sender: TObject);
begin
     if dsSelPirola.State = dsBrowse then begin
          qselpirola.Edit
     end;
     qSelPirolaEsitoOttavoColloquio.Value := '';
     qSelPirolaNumColloquioAttivo.Value := 8;
     qSelPirola.Post;
end;

procedure TPirolaForm.Coll2DblClick(Sender: TObject);
begin
     if Coll2.Collaps = true then Coll2.Collaps := false
     else
          Coll2.Collaps := true;
end;

procedure TPirolaForm.Coll3DblClick(Sender: TObject);
begin
     if Coll3.Collaps = true then Coll3.Collaps := false
     else
          Coll3.Collaps := true;
end;

procedure TPirolaForm.Coll4DblClick(Sender: TObject);
begin
     if Coll4.Collaps = true then Coll4.Collaps := false
     else
          Coll4.Collaps := true;
end;

procedure TPirolaForm.Coll5DblClick(Sender: TObject);
begin
     if Coll5.Collaps = true then Coll5.Collaps := false
     else
          Coll5.Collaps := true;
end;

procedure TPirolaForm.Coll6DblClick(Sender: TObject);
begin
     if Coll6.Collaps = true then Coll6.Collaps := false
     else
          Coll6.Collaps := true;
end;

procedure TPirolaForm.Coll7DblClick(Sender: TObject);
begin
     if Coll7.Collaps = true then Coll7.Collaps := false
     else
          Coll7.Collaps := true;
end;

procedure TPirolaForm.Coll8DblClick(Sender: TObject);
begin
     if Coll8.Collaps = true then Coll8.Collaps := false
     else
          Coll8.Collaps := true;
end;

procedure TPirolaForm.Coll9DblClick(Sender: TObject);
begin
     if Coll9.Collaps = true then Coll9.Collaps := false
     else
          Coll9.Collaps := true;
end;

procedure TPirolaForm.vaiavalutazionefinale;
begin
     if qselpirola.state = dsBrowse then qselpirola.Edit;
     qSelPirolaNumColloquioAttivo.Value := 10;
     qSelPirola.Post;
end;

procedure TPirolaForm.ToolbarButton9721Click(Sender: TObject);
begin
     vaiavalutazionefinale;
end;

procedure TPirolaForm.ToolbarButton9728Click(Sender: TObject);
var xidutente: integer;
begin

     if (qSelPirolaNumColloquioAttivo.Value <> 10) and ((xPositivo) or (qSelPirolaNumColloquioAttivo.Value = null) or (qSelPirolaNumColloquioAttivo.Value = 0)) then begin
          ElencoUtentiForm := TElencoUtentiForm.create(self);
          ElencoUtentiForm.ShowModal;
          if ElencoUtentiForm.ModalResult = mrOK then begin
               xidutente := ElencoUtentiForm.QUsers.FieldByName('ID').AsInteger;
               qtemp.close;
               qtemp.SQL.Text := 'select isnull(numcolloquioattivo,0)+1 as Count from selpirola where id=:x0';
               qtemp.Parameters[0].Value := qSelPirolaID.Value;
               qtemp.Open;


               if dsSelPirola.State = dsBrowse then qSelPirola.Edit;
               case qtemp.fields[0].AsInteger of
                    1: begin qSelPirolaIDUtenteRespPrimoColloquio.Value := xidutente;
                              qSelPirolaNumColloquioAttivo.value := qtemp.Fields[0].Value;
                         end;
                    2: begin qSelPirolaIDUtenteRespSecondoColloquio.Value := xidutente;

                         end;
                    3: begin qSelPirolaIDUtenteRespTerzoColloquio.Value := xidutente;

                         end;
                    4: begin qSelPirolaIDUtenteRespQuartoColloquio.Value := xidutente;

                         end;
                    5: begin qSelPirolaIDUtenteRespQuintoColloquio.Value := xidutente;

                         end;
                    6: begin qSelPirolaIDUtenteRespSestoColloquio.Value := xidutente;

                         end;
                    7: begin qSelPirolaIDUtenteRespSettimoColloquio.Value := xidutente;

                         end;
                    8: begin qSelPirolaIDUtenteRespOttavoColloquio.Value := xidutente;

                         end;
                    9: begin qSelPirolaIDUtenteRespNonoColloquio.Value := xidutente;

                         end;

               end;
               qSelPirolaNumColloquioAttivo.value := qtemp.Fields[0].Value;
               qselpirola.Post;

               {if MessageDlg('Vuoi inviare uan mail a ' + ElencoUtentiForm.QUsers.FieldByName('Nominativo').AsString, mtConfirmation, [mbYes, mbNo], 0) = mryes then begin
                    ShellExecute(0, 'Open', pchar('mailto:' + 'test@test.it' + '?subject=' + 'testo mail'), '', '', SW_SHOW);

               end;}

          end;

          ElencoUtentiForm.Free;
     end;
     if qSelPirolaNumColloquioAttivo.Value = 10 then begin
          if dsSelPirola.State = dsBrowse then qSelPirola.Edit;
          qSelPirolaIDUtenteValFinale.Value := MainForm.xIDUtenteAttuale;
          qselpirola.Post;
     end;
end;

procedure TPirolaForm.ToolbarButton9731Click(Sender: TObject);
begin
//GEstione Test
     AnagTestelearningForm := TAnagTestelearningForm.Create(self);
     AnagTestelearningForm.ShowModal;

     AnagTestelearningForm.Free;
end;

procedure TPirolaForm.ToolbarButton9717Click(Sender: TObject);
begin
     if qSelPirolaEsitoOttavoColloquio.value = 'Positivo' then
          xpositivo := true else xpositivo := false;
     ToolbarButton9728Click(self);

end;

end.

