unit VarColloquio;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Mask, ComCtrls, ExtCtrls, Grids, DBGrids, DB;

type
  TVarColloquioForm = class(TForm)
    RadioGroup1: TRadioGroup;
    RadioGroup2: TRadioGroup;
    GroupBox1: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    Data: TDateTimePicker;
    Ora: TMaskEdit;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    DBGrid1: TDBGrid;
    procedure RadioGroup1Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  VarColloquioForm: TVarColloquioForm;

implementation

uses ModuloDati3;

{$R *.DFM}


procedure TVarColloquioForm.RadioGroup1Click(Sender: TObject);
begin
     case RadioGroup1.ItemIndex of
     0: VarColloquioForm.Height:=220;
     1: begin VarColloquioForm.Height:=188;
              RadioGroup2.Visible:=True; end;
     end;
end;

procedure TVarColloquioForm.BitBtn1Click(Sender: TObject);
begin
     case RadioGroup1.ItemIndex of
     0: begin DataSel_EBC.TColloqui2.Edit;
              DataSel_EBC.TColloqui2Data.Value:=Data.Date;
              DataSel_EBC.TColloqui2Ora.Value:=StrToTime(Ora.Text);
              DataSel_EBC.TColloqui2IDSelezionatore.Value:=DataSel_EBC.TSelezionatoriID.Value;
              DataSel_EBC.TColloqui2.Post;
              // - aggiornamento situazione ricerca
              DataSel_EBC.TCandRicAnag.FindKey([DataSel_EBC.QColloquiSelIDRicerca.Value,
                                                DataSel_EBC.QColloquiSelIDAnagrafica.Value]);
              DataSel_EBC.TCandRicAnag.Edit;
              DataSel_EBC.TCandRicAnagStato.Value:='fissato colloquio per il giorno '+
                                                   DateToStr(Data.Date)+' alle ore '+Ora.Text+' ('+
                                                   DataSel_EBC.TSelezionatoriNominativo.asString+')';
              DataSel_EBC.TCandRicAnag.Post;
              // aggiornamento promemoria generale
              DataSel_EBC.TPromemoria.InsertRecord([DataSel_EBC.TSelezionatoriID.Value,
                                                    null,
                                                    Date,
                                                    Data.Date,
                                                    'colloquio con '+DataSel_EBC.TAnagCognome.asString+' alle ore '+
                                                     Ora.Text+
                                                     ' per ricerca '+DataSel_EBC.TRicerchePendProgressivo.asString+' ('+
                                                     DataSel_EBC.TRicerchePendMansione.asString+')',
                                                    null,False]);
        end;
     1: begin
         DataSel_EBC.TContattiCand2TipoContatto.value:='telefonico';
         DataSel_EBC.TCandRicAnag.FindKey([DataSel_EBC.TContattiCand2IDRicerca.Value,
                                           DataSel_EBC.TContattiCand2IDAnagrafica.Value]);
         DataSel_EBC.TColloqui2.Delete;
         if RadioGroup2.Itemindex=0 then begin
            //deve richiamare
            DataSel_EBC.TContattiCand2RichiamaData.Value:=Data.Date;
            DataSel_EBC.TContattiCand2RichiamaOra.Value:=StrToTime(Ora.Text);
            DataSel_EBC.TCandRicAnag.Edit;
            DataSel_EBC.TCandRicAnagStato.Value:='deve richiamare in data '+
                    DataSel_EBC.TContattiCand2RichiamaData.asString+' alle ore '+
                    DataSel_EBC.TContattiCand2RichiamaOra.asString;
            DataSel_EBC.TCandRicAnag.Post;
            DataSel_EBC.QtelDaRicSel.Close;
            DataSel_EBC.QtelDaRicSel.Open;
            // aggiornamento promemoria generale
            DataSel_EBC.TPromemoria.InsertRecord([DataSel_EBC.TRicerchePendIDUtente.Value,
                                                  null,
                                                  Date,
                                                  DataSel_EBC.TContattiCand2RichiamaData.Value,
                                                  'deve richiamare '+DataSel_EBC.TAnagCognome.asString+' alle ore '+
                                                   DataSel_EBC.TContattiCand2RichiamaOra.asString+
                                                   ' per ricerca '+DataSel_EBC.TRicerchePendProgressivo.asString+' ('+
                                                   DataSel_EBC.TRicerchePendMansione.asString+')',
                                                  null,False]);
         end;
         if RadioGroup2.Itemindex=1 then begin
            //da richiamare
            DataSel_EBC.TContattiCand2RichiamareData.Value:=Data.Date;
            DataSel_EBC.TContattiCand2RichiamareOra.Value:=StrToTime(Ora.Text);
            DataSel_EBC.TCandRicAnag.Edit;
            DataSel_EBC.TCandRicAnagStato.Value:='da richiamare in data '+
                    DataSel_EBC.TContattiCand2RichiamareData.asString+' alle ore '+
                    DataSel_EBC.TContattiCand2RichiamareOra.asString;
            DataSel_EBC.TCandRicAnag.Post;
            DataSel_EBC.QtelDaFareSel.Close;
            DataSel_EBC.QtelDaFareSel.Open;
            // aggiornamento promemoria generale
            DataSel_EBC.TPromemoria.InsertRecord([DataSel_EBC.TRicerchePendIDUtente.Value,
                                                  null,
                                                  Date,
                                                  DataSel_EBC.TContattiCand2RichiamareData.Value,
                                                  'richiamare '+DataSel_EBC.TAnagCognome.asString+' alle ore '+
                                                   DataSel_EBC.TContattiCand2RichiamareOra.asString+
                                                   ' per ricerca '+DataSel_EBC.TRicerchePendProgressivo.asString+' ('+
                                                   DataSel_EBC.TRicerchePendMansione.asString+')',
                                                  null,False]);
         end;
        end;
     2: begin
          // eliminazione dalla ricerca e eliminazione colloqui
          DataSel_EBC.TCandRicAnag.FindKey([DataSel_EBC.TColloqui2IDRicerca.Value,
                                            DataSel_EBC.TColloqui2IDAnagrafica.Value]);
          DataSel_EBC.TCandRicAnag.Delete;
          DataSel_EBC.TColloqui2.Delete;
        end;
     end;

     if DataSel_EBC.DsContattiCand2.State in [dsInsert,dsEdit] then
        DataSel_EBC.TContattiCand2.Post;

     close;
end;

procedure TVarColloquioForm.BitBtn2Click(Sender: TObject);
begin
     close
end;


end.
