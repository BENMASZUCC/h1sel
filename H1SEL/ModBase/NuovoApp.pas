//[TONI200218092002DEBUGOK]
unit NuovoApp;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, Mask, DtEdit97, ListComb, ExtCtrls, Db, DBTables,
     Grids, DBGrids, ADODB, U_ADOLinkCl;

type
     TNuovoImpForm = class(TForm)
          DEData: TDateEdit97;
          MEOre: TMaskEdit;
          BitBtn1: TBitBtn;
          BitBtn2: TBitBtn;
          Label1: TLabel;
          Label2: TLabel;
          EDescriz: TEdit;
          Label3: TLabel;
          RGTipo: TRadioGroup;
          dsRisorse: TDataSource;
          PanLuogo: TPanel;
          DBGrid1: TDBGrid;
          Label4: TLabel;
          CBLuogo: TCheckBox;
          TRisorse_OLD: TQuery;
          TRisorse_OLDID: TAutoIncField;
          TRisorse_OLDRisorsa: TStringField;
          TRisorse_OLDColor: TStringField;
          Label5: TLabel;
          MEAlleOre: TMaskEdit;
          QCheck_OLD: TQuery;
          QCheck: TADOLinkedQuery;
          TRisorse: TADOLinkedQuery;
          procedure RGTipoClick(Sender: TObject);
          procedure BitBtn1Click(Sender: TObject);
          procedure FormShow(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     NuovoImpForm: TNuovoImpForm;

implementation

uses ModuloDati, AgendaSett;

{$R *.DFM}

procedure TNuovoImpForm.RGTipoClick(Sender: TObject);
begin
     if RGTipo.ItemIndex = 0 then
          PanLuogo.Visible := true
     else PanLuogo.Visible := False;
end;

//[/TONI20020911\]

procedure TNuovoImpForm.BitBtn1Click(Sender: TObject);
var xHour, xMin, xSec, xMSec, xHour2, xMin2, xSec2, xMSec2: Word;
begin
     // controllo orari
     if MEAlleOre.Text <> '  .  ' then
          if StrToDateTime(DatetoStr(DEData.Date) + ' ' + MEOre.Text) >
               StrToDateTime(DatetoStr(DEData.Date) + ' ' + MEAlleOre.Text) then begin
               ModalResult := mrNone;
               ShowMessage('Orari non corretti');
               Abort;
          end;
     // controllo concomitanza (solo colloquio)
     if (NuovoImpForm.MEAlleOre.Text <> '  .  ') and (RGTipo.ItemIndex = 0) then begin
          QCheck.Close;
          QCheck.ReloadSQL;
          //          QCheck.Prepare;
          {          QCheck.ParamByName('xDalle').asDateTime:=StrToDateTime(DatetoStr(DEData.Date)+' '+MEOre.Text);
                    QCheck.ParamByName('xAlle').asDateTime:=StrToDateTime(DatetoStr(DEData.Date)+' '+MEAlleOre.Text);
                    QCheck.ParamByName('xIDRis').asInteger:=TRisorseID.Value;}
          QCheck.ParamByName['xDalle'] := StrToDateTime(DatetoStr(DEData.Date) + ' ' + MEOre.Text);
          QCheck.ParamByName['xAlle'] := StrToDateTime(DatetoStr(DEData.Date) + ' ' + MEAlleOre.Text);
          QCheck.ParamByName['xIDRis'] := TRisorse.FieldByName('ID').AsInteger;
          QCheck.Open;
          if not QCheck.IsEmpty then begin
               ModalResult := mrNone;
               DecodeTime(QCheck.FieldByName('Ore').asDateTime, xHour, xMin, xSec, xMSec);
               DecodeTime(QCheck.FieldByName('AlleOre').asDateTime, xHour2, xMin2, xSec2, xMSec2);
               MessageDlg('Risulta gi� presente il seguente impegno:' + chr(13) +
                    QCheck.FieldByName('Descrizione').asString + chr(13) +
                    'dalle ' + IntToStr(xHour) + '.' + IntToStr(xMin) +
                    '  alle ' + IntToStr(xHour2) + '.' + IntToStr(xMin2) + chr(13) +
                    'Luogo: ' + TRisorse.FieldByName('Risorsa').AsString, mtError, [mbOK], 0);
               Abort;
          end;
     end;
end;
//[/TONI20020911\]FINE

procedure TNuovoImpForm.FormShow(Sender: TObject);
begin
     Caption := '[M/41] - ' + Caption;
end;

end.
