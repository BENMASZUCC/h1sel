unit ValutazDip;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Grids, DBGrids, TB97, Wall, DBCtrls, StdCtrls, ComCtrls, Mask, Buttons,
     ExtCtrls, TeEngine, Series, TeeProcs, Chart, DBChart, Db, DBTables, ADODB,
     U_ADOLinkCl, dxTL, dxDBCtrl, dxDBGrid, dxCntner;

type
     TValutazDipForm = class(TForm)
          Panel1: TPanel;
          DsRuoli: TDataSource;
          PageControl1: TPageControl;
          TsDatiAnag: TTabSheet;
          TsCompetTutte: TTabSheet;
          Wallpaper6: TPanel;
          TbBCompDipNew: TToolbarButton97;
          TbBCompDipDel: TToolbarButton97;
          ToolbarButton971: TToolbarButton97;
          DBGrid14: TDBGrid;
          TSCompRuolo: TTabSheet;
          Wallpaper1: TPanel;
          ToolbarButton972: TToolbarButton97;
          Panel4: TPanel;
          DBChart3: TDBChart;
          BarSeries1: TBarSeries;
          Panel5: TPanel;
          DBChart4: TDBChart;
          BarSeries3: TBarSeries;
          BarSeries2: TLineSeries;
          TsTutteCar: TTabSheet;
          DBGrid19: TDBGrid;
          Wallpaper7: TPanel;
          TbBCarattDipNew: TToolbarButton97;
          TbBCarattDipDel: TToolbarButton97;
          Panel6: TPanel;
          DBEdit1: TDBEdit;
          Label1: TLabel;
          ToolbarButton974: TToolbarButton97;
          PanCompNo: TPanel;
          Panel8: TPanel;
          Panel7: TPanel;
          DBEdit12: TDBEdit;
          DBEdit14: TDBEdit;
          DBGrid1: TDBGrid;
          DsAnagCompNonPoss: TDataSource;
          ToolbarButton975: TToolbarButton97;
          ToolbarButton976: TToolbarButton97;
          Panel2: TPanel;
          Label38: TLabel;
          Label49: TLabel;
          Label50: TLabel;
          GroupBox1: TGroupBox;
          Label6: TLabel;
          Label7: TLabel;
          Label8: TLabel;
          Label9: TLabel;
          DBEdit6: TDBEdit;
          DBEdit7: TDBEdit;
          DBEdit8: TDBEdit;
          DBEdit9: TDBEdit;
          GroupBox2: TGroupBox;
          Label4: TLabel;
          Label10: TLabel;
          Label32: TLabel;
          Label33: TLabel;
          DBEdit3: TDBEdit;
          DBEdit4: TDBEdit;
          DBEdit10: TDBEdit;
          DBEdit11: TDBEdit;
          DBLookupComboBox1: TDBLookupComboBox;
          DBEdit17: TDBEdit;
          DBEdit23: TDBEdit;
          Panel9: TPanel;
          Label46: TLabel;
          Label47: TLabel;
          Label48: TLabel;
          DBEdit13: TDBEdit;
          DBEdit15: TDBEdit;
          DBEdit16: TDBEdit;
          TbBCarattDipMod: TToolbarButton97;
          Truoli: TADOLinkedQuery;
          TCompRuolo: TADOLinkedQuery;
          TAnagCompNonPoss: TADOLinkedTable;
          Qtemp: TADOLinkedQuery;
          QCompLK: TADOLinkedQuery;
          BitBtn1: TToolbarButton97;
          BitBtn2: TToolbarButton97;
          BitBtn20: TToolbarButton97;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1Descrizione: TdxDBGridColumn;
          dxDBGrid1Peso: TdxDBGridColumn;
          dxDBGrid1Richiesto: TdxDBGridColumn;
          dxDBGrid1Valore: TdxDBGridColumn;
          Q1: TADOQuery;
          Q1IDMansione: TIntegerField;
          Q1IDCompetenza: TAutoIncField;
          Q1Descrizione: TStringField;
          Q1Tipologia: TStringField;
          Q1Peso: TBCDField;
          Q1ValRichiesto: TIntegerField;
          Q1IDAnagrafica: TIntegerField;
          DsQ1: TDataSource;
    Qtot: TADOQuery;
    QtotTot: TBCDField;
    Q1ValNome: TBCDField;
          procedure TbBCompDipNewClick(Sender: TObject);
          procedure TbBCompDipDelClick(Sender: TObject);
          procedure TbBCarattDipNewClick(Sender: TObject);
          procedure TbBCarattDipDelClick(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure ToolbarButton972Click(Sender: TObject);
          procedure BitBtn20Click(Sender: TObject);
          procedure ToolbarButton971Click(Sender: TObject);
          procedure BitBtn2Click(Sender: TObject);
          procedure Truoli_OLDAfterScroll(DataSet: TDataSet);
          procedure ToolbarButton974Click(Sender: TObject);
          procedure ToolbarButton975Click(Sender: TObject);
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
          procedure BitBtn1Click(Sender: TObject);
          procedure ToolbarButton976Click(Sender: TObject);
          procedure TbBCarattDipModClick(Sender: TObject);
     private
          { Private declarations }
          xVecchioVal: integer;
     public
          xidRuolo: Integer;
          { Public declarations }
     end;

var
     ValutazDipForm: TValutazDipForm;

implementation

uses Curriculum, ModuloDati, ModuloDati2,
     StoricoComp, RepSchedaVal, RuoloRif, NuovaComp, InsCompetenza, Main,
     InsCaratt;

{$R *.DFM}


procedure TValutazDipForm.TbBCompDipNewClick(Sender: TObject);
var i, xVal: integer;
     Vero: boolean;
begin
     if not MainForm.CheckProfile('020') then Exit;
     InsCompetenzaForm := TInsCompetenzaForm.create(self);
     InsCompetenzaForm.RiempiGriglia;
     InsCompetenzaForm.ShowModal;
     if InsCompetenzaForm.ModalResult = mrOK then begin
          for i := 1 to InsCompetenzaForm.ASG1.RowCount - 1 do begin
               InsCompetenzaForm.ASG1.GetCheckBoxState(0, i, Vero);
               if Vero then begin
                    with Data do begin
                         DB.BeginTrans;
                         try

                              //[ALBERTO 20020912]

                              Q1.Close;
                              Q1.SQL.text := 'insert into CompetenzeAnagrafica (IDAnagrafica,IDCompetenza,Valore) ' +
                                   'values (:xIDAnagrafica:,:xIDCompetenza:,:xValore:)';
                              Q1.ParamByName['xIDAnagrafica'] := Data.TAnagrafica.FieldByName('ID').AsInteger;
                              Q1.ParamByName['xIDCompetenza'] := InsCompetenzaForm.xArrayIDComp[i];
                              Q1.ParamByName['xValore'] := InsCompetenzaForm.xArrayCompVal[i];
                              Q1.ExecSQL;

                              Q1.SQL.text := 'insert into StoricoCompAnag (IDCompetenza,IDAnagrafica,DallaData,MotivoAumento,valore) ' +
                                   'values (:xIDCompetenza:,:xIDAnagrafica:,:xDallaData:,:xMotivoAumento:,:xvalore:)';
                              Q1.ParamByName['xIDCompetenza'] := InsCompetenzaForm.xArrayIDComp[i];
                              Q1.ParamByName['xIDAnagrafica'] := Data.TAnagrafica.FieldByName('ID').AsInteger;
                              Q1.ParamByName['xDallaData'] := Date;
                              Q1.ParamByName['xMotivoAumento'] := 'valore iniziale';
                              Q1.ParamByName['xValore'] := InsCompetenzaForm.xArrayCompVal[i];
                              Q1.ExecSQL;

                              DB.CommitTrans;
                              Data2.TCompDipendente.Close;
                              Data2.TCompDipendente.Open;
                         except
                              DB.RollbackTrans;
                              MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                         end;
                    end;
               end;
          end;

     end;
     InsCompetenzaForm.Free;

     Q1.Close;
     Q1.Open;
     QTot.Close;
     QTot.Open;
     if TAnagCompNonPoss.FindKeyADO(VarArrayOf([Data2.TCompDipendente.FieldByName('IDAnagrafica').AsInteger, Data2.TCompDipendente.FieldByName('IDCompetenza').AsInteger])) then begin
          TAnagCompNonPoss.Delete;
          if TAnagCompNonPoss.RecordCount = 0 then begin
               PanCompNo.Visible := False;
               DBChart3.Visible := True;
          end;
     end;
end;

procedure TValutazDipForm.TbBCompDipDelClick(Sender: TObject);
begin
     if not MainForm.CheckProfile('021') then Exit;
     if Data2.TCompDipendente.RecordCount > 0 then
          if MessageDlg('Sei sicuro di voler cancellare questa competenza ?', mtWarning,
               [mbNo, mbYes], 0) = mrYes then begin
               with Data do begin
                    DB.BeginTrans;
                    try
                         Q1.Close;
                         Q1.SQL.text := 'delete from CompetenzeAnagrafica ' +
                              'where ID=' + Data2.TCompDipendente.FieldByName('ID').asString;
                         Q1.ExecSQL;
                         DB.CommitTrans;
                    except
                         DB.RollbackTrans;
                         MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                    end;
               end;
               Data2.TCompDipendente.Close;
               Data2.TCompDipendente.Open;
          end;

     Q1.Close;
     Q1.Open;
     QTot.Close;
     QTot.Open;
end;

procedure TValutazDipForm.TbBCarattDipNewClick(Sender: TObject);
begin
     if not MainForm.CheckProfile('033') then Exit;
     InsCarattForm := TInsCarattForm.create(self);
     InsCarattForm.ShowModal;
     if InsCarattForm.Modalresult = mrOK then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text := 'insert into AnagCaratteristiche (IDAnagrafica,IDCaratt) ' +
                         'values (:xIDAnagrafica:,:xIDCaratt:)';
                    Q1.ParamByName['xIDAnagrafica'] := Data.TAnagrafica.FieldByName('ID').AsInteger;
                    Q1.ParamByName['xIDCaratt'] := InsCarattForm.TCaratt.FieldByName('ID').AsInteger;
                    Q1.ExecSQL;
                    DB.CommitTrans;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               end;
          end;
          Data2.TCarattDip.Close;
          Data2.TCarattDip.Open;
     end;
     InsCarattForm.Free;
end;

procedure TValutazDipForm.TbBCarattDipDelClick(Sender: TObject);
begin
     if not MainForm.CheckProfile('034') then Exit;
     if Data2.TCarattDip.RecordCount = 0 then exit;
     if MessageDlg('Sei sicuro di voler cancellare ?', mtWarning,
          [mbNo, mbYes], 0) = mrYes then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text := 'delete from AnagCaratteristiche ' +
                         'where ID=' + Data2.TCarattDip.FieldByName('ID').asString;
                    Q1.ExecSQL;
                    DB.CommitTrans;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               end;
          end;
          Data2.TCarattDip.Close;
          Data2.TCarattDip.Open;
     end;
end;

procedure TValutazDipForm.FormShow(Sender: TObject);
begin
//grafica
     ValutazDipForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel7.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel7.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel7.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel9.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel9.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel9.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel4.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel4.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel4.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel6.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel6.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel6.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel5.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel5.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel5.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     PanCompNo.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     PanCompNo.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     PanCompNo.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel8.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel8.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel8.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Wallpaper1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Wallpaper7.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Wallpaper6.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     DBChart3.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     DBChart4.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;

     Caption := '[M/052] - ' + Caption;
     //PageControl1.ActivePage := TSCompRuolo;
     PageControl1.ActivePage := TsDatiAnag;
     Data2.TCarattDip.Open;
     Data2.TCompDipendente.Open;
     //TCompRuolo.Open;
     //if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString,1,6))='BOYDEN' then begin
          // tutti i wallpaper senza il marmo
     //WallPaper1.Wallpaper := nil; WallPaper1.caption := '';
     //WallPaper6.Wallpaper := nil; WallPaper6.caption := '';
     //WallPaper7.Wallpaper := nil; WallPaper7.caption := '';
     //end;
end;

procedure TValutazDipForm.ToolbarButton972Click(Sender: TObject);
var xVal: string;
      xVecchioVal,xVarPerc: integer;
begin
     if not Q1.EOF then begin
          NuovaCompForm := TNuovaCompForm.create(self);
          NuovaCompForm.xComp := Q1.FieldByName('Descrizione').Value;
          NuovaCompForm.SEVecchioVal.Text := Q1.FieldByName('ValNome').AsString;
          NuovaCompForm.SENuovoVal.Text := Q1.FieldByName('ValNome').AsString;
          NuovaCompForm.xMot := 'a seguito di valutazione';
          NuovaCompForm.DEData.Date := Date;
          NuovaCompForm.ShowModal;
          if NuovaCompForm.ModalResult = mrOK then begin
               with Data do begin
                    DB.BeginTrans;
                    try
                         Q2.Close;
                         Q2.SQL.text := 'update CompetenzeAnagrafica set Valore=:xValore: ' +
                              'where IDAnagrafica=:xIDAnag: and IDCompetenza=:xIDComp:';
                         Q2.ParamByName['xValore'] := NuovaCompForm.SENuovoVal.Text;
                         Q2.ParamByName['xIDAnag'] := Data.TAnagrafica.FieldByName('ID').AsInteger;
                         Q2.ParamByName['xIDComp'] := ValutazDipForm.Q1.FieldByName('IDCompetenza').AsInteger;
                         Q2.ExecSQL;

                         xVecchioVal := StrToInt(NuovaCompForm.SEVecchioVal.Text);
                         if xVecchioVal > 0 then
                              xVarPerc := Trunc(((StrToInt(NuovaCompForm.SENuovoVal.Text) - xVecchioVal) / xVecchioVal) * 100)
                         else xVarPerc := StrToInt(NuovaCompForm.SENuovoVal.Text) * 100;

                         Q2.SQL.text := 'insert into StoricoCompAnag (IDCompetenza,IDAnagrafica,DallaData,MotivoAumento,valore,VariazPerc) ' +
                              'values (:xIDCompetenza:,:xIDAnagrafica:,:xDallaData:,:xMotivoAumento:,:xvalore:,:xVariazPerc:)';
                         Q2.ParamByName['xIDCompetenza'] := ValutazDipForm.Q1.FieldByName('IDCompetenza').AsInteger;
                         Q2.ParamByName['xIDAnagrafica'] := ValutazDipForm.Q1.FieldByName('IDAnagrafica').AsInteger;
                         Q2.ParamByName['xDallaData'] := NuovaCompForm.DEData.Date;
                         Q2.ParamByName['xMotivoAumento'] := NuovaCompForm.xMot;
                         Q2.ParamByName['xValore'] := ValutazDipForm.Q1.FieldByName('ValNome').Value;
                         Q2.ParamByName['xVariazPerc'] := xVarPerc;
                         Q2.ExecSQL;

                         DB.CommitTrans;
                    except
                         DB.RollbackTrans;
                         MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                    end;
               end;

               Q1.Close;
               Q1.Open;
               QTot.Close;
               QTot.Open;
               Data2.TCompDipendente.Close;
               Data2.TCompDipendente.Open;
          end;
          NuovaCompForm.Free;
     end;
end;

procedure TValutazDipForm.BitBtn20Click(Sender: TObject);
var x: string;
     i: integer;
begin
     CurriculumForm.ShowModal;
end;

procedure TValutazDipForm.ToolbarButton971Click(Sender: TObject);
begin
     if Data2.TCompDipendente.RecordCount > 0 then begin
          StoricoCompForm := TStoricoCompForm.create(self);
          StoricoCompForm.ShowModal;
          StoricoCompForm.Free;
     end;
end;

procedure TValutazDipForm.BitBtn2Click(Sender: TObject);
begin
     QRSchedaVal := TQRSchedaVal.create(self);
     try QRSchedaVal.Preview except
          ShowMessage('problemi con il Report') end;
     QRSchedaVal.Free;
end;

procedure TValutazDipForm.Truoli_OLDAfterScroll(DataSet: TDataSet);
begin
     DBChart3.RefreshData;
     DBChart4.RefreshData;
end;

procedure TValutazDipForm.ToolbarButton974Click(Sender: TObject);
var xHeight: integer;
begin
     RuoloRifForm := TRuoloRifForm.create(self);
     //TCompRuolo.Close;
     //TCompRuolo.Open;
     RuoloRifForm.ShowModal;
     if RuoloRifForm.ModalResult = mrOK then begin
          Q1.Close;
          TRuoli.Locate('ID', xidRuolo, []);
          Q1.Parameters.ParamByName('xIDAnagrafica').Value := Data.TAnagrafica.FieldByName('ID').Value;
          Q1.Parameters.ParamByName('xIDMansione').Value := xidRuolo;
          Q1.Open;
          DBChart3.Visible := True;
          {if TCompRuolo.RecordCount = 0 then
               MessageDlg('Nessuna competenza richiesta da questo ruolo', mtWarning, [mbOK], 0)
          else begin
               if TCompRuolo.RecordCount > Q1.RecordCount then begin
               //Qtemp.SQL.clear;
               //Qtemp.SQL.Add('delete from AnagCompNonPoss');
               //Qtemp.ExecSQL;
               //TAnagCompNonPoss.Close;
               //TAnagCompNonPoss.Open;
                    TCompRuolo.First;
                    xHeight := 30;
                    while not TCompRuolo.EOF do begin
                         Data.QTemp.Close;
                         Data.QTemp.SQL.text := 'select count(ID) Tot from CompetenzeAnagrafica where IDAnagrafica=:xIDAnag: and IDCompetenza=:xIDComp:';
                         Data.QTemp.ParamByName['xIDAnag'] := Data.TAnagrafica.FieldByName('ID').AsInteger;
                         Data.QTemp.ParamByName['xIDComp'] := TCompRuolo.FieldByName('IDCompetenza').AsInteger;
                         Data.QTemp.Open;
                         if Data.QTemp.FieldByName('Tot').asInteger = 0 then begin
                         //TAnagCompNonPoss.InsertRecord([Data.TAnagrafica.FieldByName('ID').AsInteger, TCompRuolo.FieldByName('IDCompetenza').AsInteger]);
                              xHeight := xHeight + 16;
                         end;
                         TCompRuolo.Next;
                    end;
               //Data.QTemp.Close;
                    MessageDlg('Le competenze richieste dal ruolo sono di pi� di quelle possedute', mtWarning, [mbOK], 0);
               //PanCompNo.Height := xHeight;
               //PanCompNo.Visible := True;
                    DBChart3.Visible := False;
               end else begin
               //PanCompNo.Visible := False;
                    DBChart3.Visible := True;
               end;
          end; }
     end;
     RuoloRifForm.Free;
end;

procedure TValutazDipForm.ToolbarButton975Click(Sender: TObject);
var xVal: string;
     xValInt: integer;
begin
     InputQuery('inserimento competenza', 'valore: ', xVal);
     xValInt := StrToIntDef(xVal, 0);
     with Data do begin
          DB.BeginTrans;
          try
               Q1.Close;
               Q1.SQL.text := 'insert into CompetenzeAnagrafica (IDAnagrafica,IDCompetenza,Valore) ' +
                    'values (:xIDAnagrafica:,:xIDCompetenza:,:xValore:)';
               Q1.ParamByName['xIDAnagrafica'] := Data.TAnagrafica.FieldByName('ID').AsInteger;
               Q1.ParamByName['xIDCompetenza'] := TAnagCompNonPoss.FieldByName('IDCompetenza').AsInteger;
               Q1.ParamByName['xValore'] := xValInt;
               Q1.ExecSQL;

               Q1.SQL.text := 'insert into StoricoCompAnag (IDCompetenza,IDAnagrafica,DallaData,MotivoAumento,valore) ' +
                    'values (:xIDCompetenza:,:xIDAnagrafica:,:xDallaData:,:xMotivoAumento:,:xvalore:)';
               Q1.ParamByName['xIDCompetenza'] := TAnagCompNonPoss.FieldByName('IDCompetenza').AsInteger;
               Q1.ParamByName['xIDAnagrafica'] := Data.TAnagrafica.FieldByName('ID').AsInteger;
               Q1.ParamByName['xDallaData'] := Date;
               Q1.ParamByName['xMotivoAumento'] := 'valore iniziale';
               Q1.ParamByName['xValore'] := xValInt;
               Q1.ExecSQL;

               DB.CommitTrans;

               Q1.Close;
               Q1.Open;
               QTot.Close;
               QTot.Open;
               Data2.TCompDipendente.Close;
               Data2.TCompDipendente.Open;
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
          end;
     end;

     if Data2.TCompDipendente.Active then Data2.TCompDipendente.Open;
     //TAnagCompNonPoss.Delete;
     DBChart4.RefreshData;
     {if TAnagCompNonPoss.RecordCount = 0 then begin
          PanCompNo.Visible := False;
          DBChart3.RefreshData;
          DBChart3.Visible := True;
     end;}
end;

procedure TValutazDipForm.FormClose(Sender: TObject;
     var Action: TCloseAction);
begin
     Data2.TCarattDip.Close;
     Data2.TCompDipendente.Open;
end;

procedure TValutazDipForm.BitBtn1Click(Sender: TObject);
begin
     ValutazDipForm.ModalResult := mrOK;
     //close;
end;

procedure TValutazDipForm.ToolbarButton976Click(Sender: TObject);
var xData: string;
     xVecchioVal, xVarPerc: integer;
begin
     if not MainForm.CheckProfile('022') then Exit;
     if Data2.TCompDipendente.RecordCount = 0 then exit;
     NuovaCompForm := TNuovaCompForm.create(self);
     NuovaCompForm.xComp := Data2.TCompDipendente.FieldByName('DescCompetenza').AsString;
     NuovaCompForm.SEVecchioVal.Text := Data2.TCompDipendente.FieldByName('Valore').AsString;
     NuovaCompForm.SENuovoVal.Text := Data2.TCompDipendente.FieldByName('Valore').AsString;
     NuovaCompForm.xMot := 'a seguito di valutazione';
     NuovaCompForm.DEData.Date := Date;
     NuovaCompForm.ShowModal;
     if NuovaCompForm.ModalResult = mrOK then begin
          Data.DB.BeginTrans;
          try
               // se il valore � cambiato ==> aggiorna storico
               if NuovaCompForm.SENuovoVal.Text <> NuovaCompForm.SEVecchioVal.Text then begin
                    xVecchioVal := StrToInt(NuovaCompForm.SEVecchioVal.Text);
                    if xVecchioVal > 0 then
                         xVarPerc := Trunc(((Data2.TCompDipendente.FieldByName('Valore').AsInteger - xVecchioVal) / xVecchioVal) * 100)
                    else xVarPerc := Data2.TCompDipendente.FieldByName('Valore').AsInteger * 100;

                    Data.Q1.SQL.text := 'insert into StoricoCompAnag (IDCompetenza,IDAnagrafica,DallaData,MotivoAumento,valore,VariazPerc) ' +
                         'values (:xIDCompetenza:,:xIDAnagrafica:,:xDallaData:,:xMotivoAumento:,:xvalore:,:xVariazPerc:)';
                    Data.Q1.ParamByName['xIDCompetenza'] := Data2.TCompDipendente.FieldByName('IDCompetenza').AsInteger;
                    Data.Q1.ParamByName['xIDAnagrafica'] := Data2.TCompDipendente.FieldByName('IDAnagrafica').AsInteger;
                    Data.Q1.ParamByName['xDallaData'] := NuovaCompForm.DEData.Date;
                    Data.Q1.ParamByName['xMotivoAumento'] := NuovaCompForm.xMot;
                    Data.Q1.ParamByName['xValore'] := NuovaCompForm.SENuovoVal.Text; //nuovo valore !!
                    Data.Q1.ParamByName['xVariazPerc'] := xVarPerc;
                    Data.Q1.ExecSQL;
               end;
               // aggiornamento CompetenzeAnagrafica
               Data.Q1.SQL.text := 'update CompetenzeAnagrafica set Valore=:xValore: ' +
                    'where ID=' + Data2.TCompDipendente.FieldByName('ID').asString;
               Data.Q1.ParamByName['xValore'] := NuovaCompForm.SENuovoVal.Text;
               Data.Q1.ExecSQL;

               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
          end;

          Data2.TCompDipendente.Close;
          Data2.TCompDipendente.Open;
     end;
     NuovaCompForm.Free;
end;

procedure TValutazDipForm.TbBCarattDipModClick(Sender: TObject);
var xPunti: string;
begin
     if not MainForm.CheckProfile('035') then Exit;
     if Data2.TCarattDip.RecordCount = 0 then exit;
     xPunti := Data2.TCarattDip.FieldByName('Punteggio').asString;
     if inputquery('Modifica punteggio caratteristica', 'Nuovo punteggio:', xPunti) then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text := 'update AnagCaratteristiche set Punteggio=:xPunteggio: ' +
                         'where ID=' + Data2.TCarattDip.FieldByName('ID').asString;
                    Q1.ParamByName['xPunteggio'] := StrToIntDef(xPunti, 0);
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    Data2.TCarattDip.Close;
                    Data2.TCarattDip.Open;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               end;

               //[ALBERTO 20020912]FINE

          end;

     end;
end;

end.

