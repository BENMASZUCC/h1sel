unit ModPrezzo;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, Mask, ToolEdit, CurrEdit, ExtCtrls, TB97;

type
     TModPrezzoForm = class(TForm)
          Label3: TLabel;
          Label6: TLabel;
          Label7: TLabel;
          Bevel1: TBevel;
          Bevel2: TBevel;
          PrezzoLire: TRxCalcEdit;
          PrezzoEuro: TRxCalcEdit;
          Panel1: TPanel;
          BitBtn1: TToolbarButton97;
          BitBtn2: TToolbarButton97;
          procedure PrezzoLireChange(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure PrezzoEuroChange(Sender: TObject);
          procedure BitBtn1Click(Sender: TObject);
          procedure BitBtn2Click(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     ModPrezzoForm: TModPrezzoForm;

implementation

uses ModuloDati, Main;

{$R *.DFM}

procedure TModPrezzoForm.PrezzoLireChange(Sender: TObject);
begin
     if PrezzoLire.Focused then
          PrezzoEuro.Value := PrezzoLire.value / 1936.27;
end;

procedure TModPrezzoForm.FormShow(Sender: TObject);
begin
     //Grafica
     ModPrezzoForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     Caption := '[M/181] - ' + caption;
     if pos('BFK', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) > 0 then begin
          Label7.Caption := 'Chf';
          PrezzoEuro.DisplayFormat := 'chf. ,0.00';
     end;
end;

procedure TModPrezzoForm.PrezzoEuroChange(Sender: TObject);
begin
     if PrezzoEuro.Focused then
          PrezzoLire.Value := PrezzoEuro.value * 1936.27;
end;

procedure TModPrezzoForm.BitBtn1Click(Sender: TObject);
begin
     ModPrezzoForm.ModalResult := mrOk;
end;

procedure TModPrezzoForm.BitBtn2Click(Sender: TObject);
begin
     ModPrezzoForm.ModalResult := mrCancel;
end;

end.

