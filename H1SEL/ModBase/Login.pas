unit Login;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Grids, DBGrids, Buttons, Mask, Db, DBTables, ImgList,
     ImageWindow, ExtCtrls, Nb, ADODB, U_ADOLinkCl, LbCipher, LbClass, LbAsym,
     LbRSA, JvSimpleXml, TB97, MSXML_TLB;

type
     TLoginForm = class(TForm)
          QUsers: TADOLinkedQuery;
          Q1: TADOLinkedQuery;
          QUserObj: TADOLinkedQuery;
          QDubbi: TADOLinkedQuery;
          Panel3: TPanel;
          Image2: TImage;
          Label20: TLabel;
          Username: TEdit;
          Label1: TLabel;
          MaskEdit1: TMaskEdit;
          LbRSASSA1: TLbRSASSA;
          LLicenza: TLabel;
          Label2: TLabel;
          LLicWarning: TLabel;
          Q2: TADOQuery;
          QUsersID: TAutoIncField;
          QUsersNominativo: TStringField;
          QUsersDescrizione: TStringField;
          QUsersPassword: TStringField;
          QUsersDataCreazione: TDateTimeField;
          QUsersDataUltimoAccesso: TDateTimeField;
          QUsersDataScadenza: TDateTimeField;
          QUsersDataRevoca: TDateTimeField;
          QUsersTipo: TSmallintField;
          QUsersEmailConnectInfo: TMemoField;
          QUsersFirmaMail: TMemoField;
          QUsersggDiffCol: TSmallintField;
          QUsersggDiffUc: TSmallintField;
          QUsersLana0: TStringField;
          QUsersIDUtenteResp: TIntegerField;
          QUsersIDAnagrafica: TIntegerField;
          QUsersIDGruppoProfess: TIntegerField;
          QUsersIDCentroCostoDef: TIntegerField;
          QUsersTipoRicDefault: TSmallintField;
          QUsersOrg_SoloSotto: TBooleanField;
          QUsersWin_User: TStringField;
          QUsersSoloClientiCollegati: TBooleanField;
          Q1B: TADOQuery;
          pbLOCmAIUSC: TPanel;
          Label3: TLabel;
          Image1: TImage;
          BitBtn1: TToolbarButton97;
          BitBtn2: TToolbarButton97;
          QUsersSendMail_RemoteSel: TBooleanField;
          QUsersDataUltimaModifica: TDateTimeField;
          QUsersDataOraFineBlocco: TDateTimeField;
          QUsersIDProfiloHRMS: TIntegerField;
          QUsersIDProfiloSel: TIntegerField;
          procedure BitBtn1Click(Sender: TObject);
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
          procedure FormShow(Sender: TObject);
          procedure FormCreate(Sender: TObject);
          procedure BitBtn2Click(Sender: TObject);
          procedure LbRSASSA1GetSignature(Sender: TObject; var Sig: TRSASignatureBlock);
          procedure MaskEdit1KeyDown(Sender: TObject; var Key: Word;
               Shift: TShiftState);
          procedure MaskEdit1Enter(Sender: TObject);
          procedure MaskEdit1KeyPress(Sender: TObject; var Key: Char);
          procedure MaskEdit1KeyUp(Sender: TObject; var Key: Word;
               Shift: TShiftState);
     private
          xMACaddress: string;
          xEsci: boolean;
          xUtente: string;
     public
          Accedi: string;
          xLicenziatario, xLicWarning, xFirma: string;
          xFunctionsNO: TStringList;
          xNumMaxSoggetti: integer;
          xNomeUtente_file, xNomeUtente: string;
          xNumErroriPwd: integer;
          function AccessVerify: string;
          function GetMachineID: longint; stdcall;
          function RecuperaUserName: string;
          function CheckUserName: string;
          procedure Procedi(xchiudi: boolean);
          function GetEnvVarValue(const VarName: string): string;
     end;

var
     LoginForm: TLoginForm;

implementation

uses Main, ModuloDati, MessIniziale, Splash3, CheckPass, uUtilsVarie, uQM, LbUtils,
     Utente, NuovoUtente;

{$R *.DFM}

procedure TLoginForm.BitBtn1Click(Sender: TObject);
var xAnno, xMese, xGiorno: Word;
     xTot, xCompVicini: integer;
     xData: TDateTime;
     i, xggDiffCol, xTotScaduti: integer;
     xDataCheck: TDateTime;
begin
     Q1.Close;
     Q1.SQL.Text := 'SET DATEFORMAT dmy';
     Q1.ExecSQL;

     if data.Global.FieldByName('debughrms').asboolean = true then ShowMessage('8A');

     //showmessage('Inizio');
     QUsers.Close;
     //QUsers.SQL.Text := 'select * from Users where nominativo=''' + Username.text + '''';
     Qusers.SQL.Text := 'select * from users a,userssoftwares b where a.id = b.idutente and idsoftware = 1 and nominativo = ''' + Username.text + '''';
     QUsers.Open;
     if QUsers.FieldByName('DataOraFineBlocco').AsDateTime <= Now then begin
     //showmessage('5');
          if not xEsci then begin
               if (not QUsers.isempty) or (Uppercase(UserName.Text) = 'ADMINISTRATOR') then begin
                    if Uppercase(UserName.Text) = 'ADMINISTRATOR' then begin
                         QUsers.Close;
                         QUsers.SQL.Text := 'select * from Users where Nominativo = ''Administrator''';
                         QUsers.Open;
                    end;
               //showmessage('6');
                    if data.Global.FieldByName('debughrms').asboolean = true then ShowMessage('8A_2');
                    if CheckPassword(MaskEdit1.Text, TrimRight(QUsers.Fieldbyname('Password').asString)) then begin
                         if data.Global.FieldByName('debughrms').asboolean = true then ShowMessage('8A_3');
                    // controllo scadenza e revoca
                         if QUsers.FieldByName('Nominativo').AsString <> 'Administrator' then begin

                              if data.Global.FieldByName('debughrms').asboolean = true then ShowMessage('8B');

                              if ((QUsers.FieldByName('DataScadenza').asString <> '') and (QUsers.FieldByName('DataScadenza').AsDateTime < Date)) or
                                   ((QUsers.FieldByName('DataRevoca').asString <> '') and (QUsers.FieldByName('DataRevoca').AsDateTime < Date)) then begin
                                   MessageDlg('ERRORE: L''utente � scaduto o � stato revocato !', mtError, [mbOK], 0);
                                   if data.Global.FieldByName('debughrms').asboolean = true then ShowMessage('8C');
                                   Exit;
                              end;
                         // controllo inutilizzo (se c'� la data di ultimo utilizzo)
                              if data.Global.FieldByName('debughrms').asboolean = true then ShowMessage('8D');
                              if (QUsers.FieldByName('DataUltimoAccesso').AsString <> '') and (QUsers.FieldByName('DataUltimoAccesso').AsDateTime < Date - 180) then begin
                                   MessageDlg('ERRORE: L''utente � inutilizzato da pi� di sei mesi (180 giorni solari).' +
                                        chr(13) + 'A norma del DPR 318/99 verr� automaticamente revocato !', mtError, [mbOK], 0);
                                   Q1.Close;
                                   Q1.SQL.Text := 'update Users set DataRevoca=:xData: where ID=' + QUsers.FieldByName('ID').asString;
                                   Q1.ParamByName['xData'] := Date;
                                   Q1.ExecSQL;
                                   if data.Global.FieldByName('debughrms').asboolean = true then ShowMessage('8E');
                                   Exit;
                              end;
                              if data.Global.FieldByName('debughrms').asboolean = true then ShowMessage('8F');
                         // controllo inutilizzo (se non � mai stato utilizzato ==> sulla data di creazione)
                              if (QUsers.FieldByName('DataUltimoAccesso').AsString = '') and (QUsers.FieldByName('DataCreazione').AsDateTime < Date - 180) then begin
                                   MessageDlg('ERRORE: L''utente � stato creato pi� di sei mesi (180 giorni solari) fa' +
                                        chr(13) + 'ma non risulta aver mai utilizzato il programma.' +
                                        chr(13) + 'A norma del DPR 318/99 verr� automaticamente revocato !', mtError, [mbOK], 0);
                                   Q1.Close;
                              //                         Q1.SQL.Text:='update Users set DataRevoca=:xData where ID='+QUsersID.asString;
                              //                         Q1.ParamByName('xData').AsDateTimeTime:=Date;
                                   Q1.SQL.Text := 'update Users set DataRevoca=:xData: where ID=' + QUsers.FieldByName('ID').asString;
                                   Q1.ParamByName['xData'] := Date;
                                   Q1.ExecSQL;
                                   if data.Global.FieldByName('debughrms').asboolean = true then ShowMessage('8G');
                                   Exit;
                              end;

                              if data.Global.FieldByName('debughrms').asboolean = true then ShowMessage('8H');
                         //Controllo Data ultimo aggiornamento password
                              if QUsers.FieldByName('Nominativo').AsString <> 'Administrator' then begin
                                   if Data.Global.FieldByName('GGValiditaPwd').AsInteger > 0 then begin
                                        if qUsers.FieldByName('DataUltimaModifica').Value > 0 then
                                             xDataCheck := qUsers.FieldByName('DataUltimaModifica').value
                                        else
                                             xDataCheck := qUsers.FieldByName('DataCreazione').value;



                                        if ((Date) - (xDataCheck)) >= Data.Global.FieldByName('GGValiditaPwd').AsInteger then begin
                                             if MessageDlg('Sono trascorsi pi� di ' + Data.Global.FieldByName('GGValiditaPwd').AsString + ' giorni dall''ultima modifica della password.' + chr(13) +
                                                  'Modificare ora la password?', mtWarning, [mbOK], 0) = mrOk then begin
                                                  repeat
                                                  until ModificaPwd(QUsersID.Value) = true;
                                             end else
                                                  Exit;

                                        end;
                                {if (Date - (QUsers.FieldByName('DataCreazione').AsDateTime)) >= 90 then begin
                                   if MessageDlg('Sono trascorsi pi� di 90 giorni dall''ultima modifica della password.' + chr(13) +
                                        'In base al Decreto Legislativo 30 Giugno 2003 N. 196 (Trattamento dati personali) la password ' + chr(13) +
                                        'deve essere di almeno 8 caratteri e modificata ogni 90 giorni solari', mtError, [mbOK], 0) = mrOk then begin
                                        UtenteForm := TUtenteForm.Create(self);
                                        UtenteForm.EUtente.Text := QUsers.FieldByName('Nominativo').AsString;
                                        UtenteForm.ShowModal;
                                        if UtenteForm.ModalResult = mrOk then begin
                                             Q1.Close;
                                             Q1.SQL.Text := 'Update Users set Password=:xPassword:, DataCreazione=:xDataCreazione: ' +
                                                  'where ID= ' + QUsers.FieldByName('ID').AsString;
                                             Q1.ParamByName['xPassword'] := EncodePwd(UtenteForm.EPassword.Text);
                                             Q1.ParamByName['xDataCreazione'] := Date;
                                             Q1.ExecSQL;

                                             // MessageDlg('In base al Decreto Legislativo 30 Giugno 2003 N. 196 la password appena modificata\inserita' + chr(13) +
                                               //    'deve essere comunicata al custode delle password', mtError, [mbOK], 0)
                                        end else
                                             Exit;
                                   end;
                                   UtenteForm.Free;
                              end; }
                                   end;
                              end;

                         end;

                         if data.Global.FieldByName('debughrms').asboolean = true then ShowMessage('8I');

                    // controllo utente gi� collegato con altro MAC ADDRESS
                         if (copy(Data.Global.FieldByName('CheckBoxIndici').AsString, 8, 1) = '1') or (Data.Global.FieldByName('CheckBoxIndici').AsString = '') then begin
                              if (TrimRight(QUsers.FieldByName('Lana0').AsString) <> '') and (TrimRight(QUsers.FieldByName('Lana0').AsString) <> xMACaddress) then begin
                                   MessageDlg('ERRORE: L''utente risulta attualmente connesso da un altro PC (Lana0=' + TrimRight(QUsers.FieldByName('Lana0').AsString) + ')' +
                                        chr(13) + 'E'' CONSENTITO ACCEDERE SOLTANTO DA QUELLA MACCHINA!', mtError, [mbOK], 0);
                                   if data.Global.FieldByName('debughrms').asboolean = true then ShowMessage('8L');
                                   //Exit;
                              end;
                         end;

                         if data.Global.FieldByName('debughrms').asboolean = true then ShowMessage('8M');
                    //showmessage('4');

                    // modifica data ultimo accesso e MAC Address
                         Q1B.Close;
                         Q1B.SQL.Text := 'update Users set DataUltimoAccesso=:xData, Lana0=:xLana0, MachineID=:xMachineID where ID=' + QUsers.FieldByName('ID').asString;
                         Q1B.Parameters.ParamByName('xData').value := Date;
                         Q1B.Parameters.ParamByName('xLana0').value := TrimRight(xMACaddress);
                         Q1B.Parameters.ParamByName('xMachineID').value := GetMachineID;
                         if data.Global.FieldByName('debughrms').asboolean = true then ShowMessage('8M_2');
                         try
                              Q1B.ExecSQL;
                         except
                              on E: Exception do
                                   ShowMessage(E.ClassName + ' error raised, with message : ' + E.Message);
                         end;

                         if data.Global.FieldByName('debughrms').asboolean = true then ShowMessage('8M_3');

                         Accedi := 'S';
                    // aggiornamento Last Login
                    //showmessage('1');
                         Q1.Close;
                         Q1.SQL.Text := 'update Global set LastLogin=:xLastLogin:,LastLoginID=:xLastLoginID:';
                         Q1.ParamByName['xLastLogin'] := QUsers.FieldByName('Nominativo').AsString;
                         Q1.ParamByName['xLastLoginID'] := QUsers.FieldByName('ID').AsInteger;
                         if data.Global.FieldByName('debughrms').asboolean = true then ShowMessage('8M_4');
                         Q1.ExecSQL;
                    //showmessage('2');

                         if data.Global.FieldByName('debughrms').asboolean = true then ShowMessage('8M_5');

                    // nominativo last login in locale
                         ScriviRegistry('LastLoginUsername', QUsers.FieldByName('Nominativo').AsString);

                    // inserimento accesso nel log accessi
                         Q1B.Close;
                         Q1B.SQL.Text := 'set dateformat dmy ' +
                              ' insert into Log_accessi (IDUtente,Data,OraEntrata,Modulo) ' +
                              ' values (:xIDUtente,:xData,:xOraEntrata,:xModulo)';
                         Q1B.Parameters.ParamByName('xIDUtente').value := QUsers.FieldByName('ID').Value;
                         Q1B.Parameters.ParamByName('xData').value := Date;
                         Q1B.Parameters.ParamByName('xOraEntrata').value := Now;
                         Q1B.Parameters.ParamByName('xModulo').value := 'MB';
                         Q1B.ExecSQL;
                         Q1B.Close;
                    //showmessage('3');

                         if data.Global.FieldByName('debughrms').asboolean = true then ShowMessage('8N');


                  //gestione cloud
                         mainform.xSendMail_Remote := QUsersSendMail_RemoteSel.asBoolean;


                    //showmessage('3');



                         Data.Global.Close;
                         Data.Global.Open;
                         Data.QTemp.Close;
                         Data.QTemp.SQL.text := 'select @@SERVERNAME ServerName';
                         Data.QTemp.Open;
                         MainForm.Statusbar1.Panels[0].text := 'Utente attuale: ' + Data.Global.FieldByName('LastLogin').AsString;
                         MainForm.Statusbar1.Panels[1].text := Data.QTemp.FieldByName('ServerName').asString + '/' + Data.DB.DefaultDatabase;
                         MainForm.Statusbar1.Panels[5].text := Data.Global.FieldByName('NomeAzienda').AsString;

                         MainForm.AdvOfficeStatusBar1.Panels[0].text := 'Utente attuale: ' + Data.Global.FieldByName('LastLogin').AsString;
                         MainForm.AdvOfficeStatusBar1.Panels[1].text := Data.QTemp.FieldByName('ServerName').asString + '/' + Data.DB.DefaultDatabase;
                         MainForm.AdvOfficeStatusBar1.Panels[3].text := Data.Global.FieldByName('NomeAzienda').AsString;


                         Data.QTemp.Close;
                         MainForm.xUtenteAttuale := Data.Global.FieldByName('LastLogin').AsString;
                         MainForm.xIDUtenteAttuale := QUsers.FieldByName('ID').AsInteger;
                         MainForm.xSoloClientiCollegati := QUsers.FieldByName('SoloClientiCollegati').AsBoolean;
                         MainForm.xUtenteAttualeTipo := QUsers.FieldByName('Tipo').AsINteger;
                         if QUsers.FieldByName('Tipo').AsINteger > 1 then
                              MainForm.DBEDubbiIns.ReadOnly := True;


                         if data.Global.FieldByName('debughrms').asboolean = true then ShowMessage('8O');
                    // cancellazione vecchi messaggi
                         Q1B.SQL.Clear;
                         Q1B.SQL.Add('delete from Promemoria where DataDaLeggere<=:xData');
                         Q1B.Parameters.ParamByName('xData').value := dateToStr(Date - 30);
                         Q1B.ExecSQL;
                         if data.Global.FieldByName('debughrms').asboolean = true then ShowMessage('8P');

                    // Obj rights
                         for i := 1 to 1000 do Mainform.xUserObj[i] := '';
                         QUserObj.Close;
                         QUserOBj.SQL.Text := 'select * from UsersObj where IDSoftware = 1 and IDUtente=''' + Data.Global.FieldByName('LastLoginID').AsString + '''';
                         QUserObj.Open;
                         i := 1;
                         mainForm.xReportMasterFattProprio := TRUE;
                         MainForm.dxNB_Tab_RepMaster.Visible := true;
                         MainForm.BReportMaster.Visible := true;
                    //mainForm.LBRiepilogo.Visible := true;
                         mainform.Pagecontrol1.ActivePage := mainform.TSInizio; //mainform.Pagecontrol1.ActivePage := mainform.TSRiepilogo;   // Da riattivare per visualizzazione reportTone
                         mainForm.xReportMasterFattAz := true;
                         while not QUserObj.EOF do begin
                         //                         Mainform.xUserObj[i]:=QUserObjCodOggetto.Value;
                              Mainform.xUserObj[i] := QUserObj.FieldByName('CodOggetto').Value;
                              QUserObj.Next;
                              inc(i);
                         end;
                    //report master
                         if not MainForm.CheckProfile('78', False) then begin
                              MainForm.dxNB_Tab_RepMaster.Visible := FALSE;
                              MainForm.BReportMaster.Visible := false;
                              mainForm.PageControl1.Activepage := mainform.TSInizio;
                         end else begin
                              MainForm.dxNB_Tab_RepMaster.Visible := TRUE;
                              MainForm.BReportMaster.Visible := true;
                              mainForm.PageControl1.Activepage := mainform.TSRiepilogo;
                         end;


                         if mainform.PageControl1.ActivePage = mainform.TSRiepilogo then begin
                              if MainForm.dxNB_Tab_RepMaster.Visible then
                              begin
                              //if mainform.FrmStatistiche1.TabSheet1.TabVisible then
                                   mainform.FrmStatistiche1.RefreshQueries;
                                   mainform.FrmStatistiche1.CheckRights;
                                   mainform.FrmStatistiche1.Visible := true;
                              end
                              else
                              begin
                                   mainform.FrmStatistiche1.Visible := FALSE;
                                   mainform.FrmStatistiche1.CloseQueries;
                              end;
                         end;
                    // CONTROLLI QUALITA'
                  //  if mainform.VerificaFunzLicenze('104') = false then
                         if xFunctionsNO.IndexOf('104') > -1 then // la chiamata a VerificaFunzLicenze non va perch� ci sono due stringlist    xFunctionsNOuna nella login e una nella main, quella nella main non � ancora creata
                              EseguiQMQueryUtente(QUsers.FieldByName('ID').AsInteger);

                    // PASSAGGIO PARAMETRI LICENZA
                    // - numero massimo soggetti consentiti in anagrafica
                  //  showmessage(inttostr(xNumMaxSoggetti));
                         if xNumMaxSoggetti = 0 then
                              MainForm.xMaxCons := -1
                         else MainForm.xMaxCons := xNumMaxSoggetti;
                    // caricamento codici Anag dei primi consentiti in db
                         if MainForm.xMaxCons <> -1 then
                              CaricaPrimi;

                    // - funzioni disabilitati
                         MainForm.xFunctionsNO := TStringList.Create;
                         MainForm.xFunctionsNO.Text := xFunctionsNO.Text;
                         close;

                    end else begin

                         MessageDlg('ERRORE: La password non � corretta', mtError, [mbOK], 0);
                         if Data.Global.FieldByName('ErroriBloccoPwd').AsInteger > 0 then begin
                              inc(xNumErroriPwd);
                              if (xNumErroriPwd > Data.Global.FieldByName('ErroriBloccoPwd').AsInteger) and (Data.Global.FieldByName('MinutiBloccoPwd').AsInteger > 0) then begin
                                   Q1.Close;
                                   Q1.SQL.Text := 'Update users set DataOraFineBlocco = :xdata: where id = :xidutente:';
                                   //showmessage(datetimetostr(((Now * 1440) + Data.Global.FieldByName('MinutiBloccoPwd').AsInteger) / 1440));
                                   Q1.ParamByName['xdata'] := VarToDateTime(((Now * 1440) + Data.Global.FieldByName('MinutiBloccoPwd').AsInteger) / 1440);
                                   //Q1.ParamByName['xdata'] := Now + strtoint(vartostr(Data.Global.FieldByName('MinutiBloccoPwd').AsInteger / 1440 * 100));
                                   Q1.ParamByName['xidutente'] := QUsers.FieldByName('ID').Value;
                                   Q1.ExecSQL;
                                   MessageDlg('ERRORE: Superato il limite di errori impostato dall''amministratore. L''utente sar� bloccato per i prossimi ' + Data.Global.FieldByName('MinutiBloccoPwd').AsString + ' minuti', mtError, [mbOK], 0);
                                   Accedi := 'S';
                                   MainForm.xEsci := true;
                                   Close;
                              end;
                         end;

                    end;
               end else begin
                    MessageDlg('ERRORE: L''utente non esiste o non � autorizzato ad accedere ad H1Sel', mtError, [mbOK], 0);
                    Username.SetFocus;
               end;
          end else begin
               if data.Global.FieldByName('debughrms').asboolean = true then ShowMessage('esci=true B');
               Accedi := 'S';
               MainForm.xEsci := true;
               Close;
          end;
     end else begin
          MessageDlg('L''utente risulta attualmente bloccato a causa di un elevato numero di tentativi di accesso.', mtError, [mbOK], 0);
          Accedi := 'S';
          MainForm.xEsci := true;
          Close;
     end;
end;
//[/TONI20020905\]FINE

procedure TLoginForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     //     Data.QPromOggi.Close;
     //     Data.QPromFuturi.Close;
     begin
          if pos('Integrated Security=SSPI', Data.DB.ConnectionString) = 0 then begin
               QUsers.Close;
               QUsers.SQL.Text := 'select * from Users where id=' + inttostr(MainForm.xIDUtenteAttuale);
               QUsers.Open;
               if not QUsers.isempty then begin
                    if not CheckPassword(Trim(MaskEdit1.Text), Trim(QUsers.Fieldbyname('Password').asString)) then begin

                         Application.Terminate;
                    end;
               end;
          end;
     end;
end;

procedure TLoginForm.FormShow(Sender: TObject);
var xValRegistry: string;
     i: integer;
begin
     // Licenza
     LLicenza.Caption := xLicenziatario;
     LLicWarning.Caption := xLicWarning;

     Caption := '[A/0] - Accesso al sistema';

     if data.Global.fieldbyname('debughrms').asboolean then begin
          showmessage('Lettura user reg= ' + LeggiRegistry('LastLoginUsername'));
          // showmessage('StatiRicerca=' + LeggiRegistry('StatiRicerca'));
     end;

     Username.Text := TrimRight(LeggiRegistry('LastLoginUsername'));

     //Grafica
     LoginForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel3.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel3.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel3.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     pbLOCmAIUSC.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     pbLOCmAIUSC.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     pbLOCmAIUSC.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

end;

{---------------------------------------------}
{ enumerate the lana's  - works only on WIN32 }
{---------------------------------------------}

function NbLanaEnum: TLana_Enum;
var
     NCB: TNCB;
     L_Enum: TLana_Enum;
     RetCode: Word;
begin
{$IFDEF WIN32}
     FillChar(NCB, SizeOf(NCB), 0);
     FillChar(L_Enum, SizeOf(TLana_Enum), 0);
     NCB.Command := NCB_ENUM;
     NCB.Buf := @L_Enum;
     NCB.Length := Sizeof(L_Enum);
     RetCode := NetBiosCmd(NCB);
     if RetCode <> NRC_GOODRET then begin
          L_Enum.Length := 0;
          L_Enum.Lana[0] := Byte(RetCode);
     end;
{$ELSE} { not supported for WIN16, fake LANA 0 }
     L_Enum.Length := 1;
     L_Enum.Lana[0] := 0;
{$ENDIF}
     Result := L_Enum;
end;

{----------------------------------------}
{ Reset the lana - don't for WIN16 !     }
{----------------------------------------}

function NbReset(l: Byte): Word;
var
     NCB: TNCB;
begin
{$IFNDEF WIN32} { will reset all your connections for WIN16 }
     Result := NRC_GOODRET; { so just fake a reset for Win16            }
{$ELSE}
     FillChar(NCB, SizeOf(NCB), 0);
     NCB.Command := NCB_RESET;
     NCB.Lana_Num := l;
     Result := NetBiosCmd(NCB);
{$ENDIF}
end;
{----------------------------------------}
{ return the MAC address of an interface }
{ in the form of a string like :         }
{ 'xx:xx:xx:xx:xx:xx'                    }
{ using the definitions in nb.pas        }
{----------------------------------------}

function NbGetMacAddr(LanaNum: Integer): string;
var
     NCB: TNCB;
     AdpStat: TAdpStat;
     RetCode: Word;
begin
     FillChar(NCB, SizeOf(NCB), 0);
     FillChar(AdpStat, SizeOf(AdpStat), 0);
     NCB.Command := NCB_ADPSTAT;
     NCB.Buf := @AdpStat;
     NCB.Length := Sizeof(AdpStat);
     FillChar(NCB.CallName, Sizeof(TNBName), $20);
     NCB.CallName[0] := Byte('*');
     NCB.Lana_Num := LanaNum;
     RetCode := NetBiosCmd(NCB);
     if RetCode = NRC_GOODRET then begin
          Result := Format('%2.2x:%2.2x:%2.2x:%2.2x:%2.2x:%2.2x',
               [AdpStat.ID[0],
               AdpStat.ID[1],
                    AdpStat.ID[2],
                    AdpStat.ID[3],
                    AdpStat.ID[4],
                    AdpStat.ID[5]
                    ]);
     end else begin
          Result := '??:??:??:??:??:??';
     end;
end;

procedure TLoginForm.FormCreate(Sender: TObject);
var
     L_Enum: TLana_Enum;
     RetCode: Word;
     i: Integer;
begin
     Accedi := 'N';
     xEsci := False;
     L_Enum := NbLanaEnum; { enumerate lanas for WIN NT }
     if L_Enum.Length = 0 then begin
          //Button1.Caption := Format('LanaEnum err=%2.2x', [L_Enum.Lana[0]]);
          exit;
     end;

     for i := 0 to (L_Enum.Length - 1) do begin { for every lana found       }

          RetCode := NbReset(L_Enum.Lana[i]); { Reset lana for WIN NT      }
          if RetCode <> NRC_GOODRET then begin
               //Button1.Caption := Format('Reset Lana %d err=%2.2x',[i, RetCode]);
               exit;
          end;
          { Get MAC Address = SOLO PER LANA = 0}
          xMACaddress := NbGetMacAddr(0);
     end;
end;

procedure TLoginForm.BitBtn2Click(Sender: TObject);
begin
     if data.Global.FieldByName('debughrms').asboolean = true then ShowMessage('esci=true C');
     xEsci := True;
     BitBtn1Click(self);
end;

function fGetDomainName(): string;
var
     vlDomainName: array[0..30] of char;
     vlSize: ^DWORD;
begin
     New(vlSize);
     vlSize^ := 30;
     ExpandEnvironmentStrings(PChar('%USERDOMAIN%'), vlDomainName, vlSize^);
     Dispose(vlSize);
     Result := vlDomainName;
end;

function GetUserFromWindows: string;
var
     UserName: string;
     UserNameLen: Dword;
begin
     UserNameLen := 255;
     SetLength(userName, UserNameLen);
     if GetUserName(PChar(UserName), UserNameLen) then
          Result := Copy(UserName, 1, UserNameLen - 1)
     else
          Result := 'Unknown';
end;

function TLoginForm.AccessVerify: string;
var xProcedi, xNuovaLicenza: boolean;
     xConfigString, xTestoDaVerif, xListaMachineID, xListTemp: TStringList;
     i, xNumAvvioCpC, xNumrighe, xNumClient, k, n, nClient: integer;
     xScadenzaLic, xMachineID, xFunctions, xdistributore, xIDCLiente, xDominio, s: string;
     xMaxDataLog, xDataGenerazLicenza: TDateTime;
     xDataServer: Tdate;
     XMLDocument1: IXMLDOMDocument;
     ANode, anodetemp: IXMLDOMNode;
     nodes_se: IXMLDomNodeList;
     //  XMLDoc: IXMLDOMDocument;
begin
     xProcedi := False;
     // caricamento configFile
     xConfigString := TStringList.create;
     try
          if FileExists(ExtractFileDir(Application.ExeName) + '\ConfigFile.xml') then begin
               XMLDocument1 := CoDOMDocument.create;
               xConfigString.LoadFromFile(ExtractFileDir(Application.ExeName) + '\ConfigFile.xml');
               XMLDocument1.loadXML(WideString(xConfigString.text));
               xNuovaLicenza := true;
               MainForm.xNewLic := true;
          end else begin
               xConfigString.LoadFromFile(ExtractFileDir(Application.ExeName) + '\ConfigFile.txt');
               xNuovaLicenza := false;
               MainForm.xNewLic := false;
          end;

     except
          Result := 'Non riesco ad aprire o a trovare il "ConfigFile"';
          exit;
     end;

     // eliminazione righe vuote
     if xNuovaLicenza = false then begin
          if xConfigString.strings[1] = '' then begin
               for i := 0 to xConfigString.count - 1 do begin
                    if i <= xConfigString.count then
                         if xConfigString.strings[i] = '' then xConfigString.Delete(i);
               end;
          end;
     end;

     // CONTROLLO FIRMA
     // caricamento chiave pubblica
     try
          LbRSASSA1.PublicKey.LoadFromFile(ExtractFileDir(Application.ExeName) + '\RSASSAPublic.txt');
     except
          Result := 'Non riesco ad aprire o a trovare il file "RSASSAPublic.txt"';
          exit;
     end;

     // verifica firma nuova licenza
     if xNuovaLicenza = true then begin
          xTestoDaVerif := TStringList.Create;
          xNumrighe := xConfigString.Count;

          xFirma := Copy(xConfigString.strings[xNumrighe - 1], 5, length(xConfigString.strings[xNumrighe - 1]) - 7);

          for i := 0 to xConfigString.Count - 2 do
               xTestoDaVerif.Add(xConfigString.strings[i]);


          if LbRSASSA1.VerifyString(xTestoDaVerif.Text) = true then begin
               xProcedi := True;
            // showmessage('licenza xml OK');
          end else begin
               Result := 'Firma non corrispondente: "ConfigFile.xml" modificato';
               exit;
          end;
          xTestoDaVerif.Free;

     end else begin
        // verifica firma vecchia licenza
          xTestoDaVerif := TStringList.create;
          for i := 0 to xConfigString.Count - 2 do xTestoDaVerif.Add(xConfigString.strings[i]);
          xFirma := xConfigString.strings[xConfigString.Count - 1]; // l'ultima riga � sempre la firma
     // verifica
          if LbRSASSA1.VerifyString(xTestoDaVerif.Text) then xProcedi := True
          else begin
               Result := 'Firma non corrispondente: "ConfigFile.txt" modificato';
               exit;
          end;
          xTestoDaVerif.Free;
     end;



     if xNuovaLicenza = true then begin
          anode := XMLDocument1.documentElement.selectSingleNode('//IntestatarioLicenza/Software/LicenzaParametri/@DataScadenza');
          xScadenzaLic := anode.text;

          anode := XMLDocument1.documentElement.selectSingleNode('//IntestatarioLicenza/@NomeCliente');
          xLicenziatario := anode.text;

          anode := XMLDocument1.documentElement.selectSingleNode('//IntestatarioLicenza/Software/LicenzaParametri/@NumSoggettiAnag');
          xNumMaxSoggetti := strtoint(anode.text);

          anode := XMLDocument1.documentElement.selectSingleNode('//IntestatarioLicenza/Software/LicenzaParametri/@Distributore');
          xDistributore := anode.text;

        //  showmessage('1');
          anode := XMLDocument1.documentElement.selectSingleNode('//IntestatarioLicenza/@DataGenerazione');
         // showmessage('2');
         // xDataGenerazLicenza := StrToDateTime(anode.text);
         // showmessage('3');
          //showmessage(vartostr( xDataGenerazLicenza));

          anode := XMLDocument1.documentElement.selectSingleNode('//IntestatarioLicenza/Software/LicenzaParametri/@NumSoggettiAnag');
          xNumMaxSoggetti := strtoint(anode.text);

     end else begin
          xLicenziatario := copy(xConfigString.strings[1], pos('=', xConfigString.strings[1]) + 1, length(xConfigString.strings[1]));
          xScadenzaLic := copy(xConfigString.strings[4], pos('=', xConfigString.strings[4]) + 1, length(xConfigString.strings[4]));
     // Riempimento variabile Numero soggetti massimi consentito
          xNumMaxSoggetti := StrToInt(copy(xConfigString.strings[6], pos('=', xConfigString.strings[6]) + 1, length(xConfigString.strings[6])));
          xDistributore := xConfigString.strings[11];
          xDistributore := copy(xDistributore, 0, (pos(';', xdistributore) - 1));
          xNumMaxSoggetti := StrToInt(copy(xConfigString.strings[6], pos('=', xConfigString.strings[6]) + 1, length(xConfigString.strings[6])));

     end;

     if data.Global.FieldByName('debughrms').asboolean = true then ShowMessage('data letta da licenza= ' + xScadenzaLic);

     // --> controllo data scadenza solo se valorizzata
     if xScadenzaLic <> '' then begin
          try
               Data.QTemp.Close;
               Data.Qtemp.SQL.text := 'set dateformat dmy select getdate() Today ';
               Data.Qtemp.Open;
               xDataServer := Data.Qtemp.FieldByName('Today').asDateTime;

               //showmessage(vartostr(xDataServer));
               // CONTROLLO CONGRUITA' DATA
               // --> solo se ci sono record in Log_TableOp
               Data.Qtemp.Close;
               Data.Qtemp.SQL.text := 'set dateformat dmy select max(dataOra) MaxTime from Log_TableOp';
               Data.Qtemp.Open;
               if not Data.Qtemp.IsEmpty then begin
                    xMaxDataLog := Data.Qtemp.FieldByName('MaxTime').asDateTime;
                    Data.Qtemp.Close;
                    if xDataServer + 1 >= xMaxDataLog then xProcedi := True
                         //1 aggiungo un giorno, prima era impostato lo scarto di un ora cio� + 0.041667
                    else begin
                         Result := 'La data del server dovrebbe essere non antecedente il ' + DateToStr(xMaxDataLog);
                         exit;
                    end;
               end;
               // CONTROLLO DATA SCADENZA
              // ShowMessage('7');
               xScadenzaLic := xScadenzaLic; //+ ' ' + xOraServer; //+ ' 00.00.00';

               // if data.Global.FieldByName('debughrms').asboolean = true then showmessage('data server= ' + VarToStr(xDataServer));
              //  if data.Global.FieldByName('debughrms').asboolean = true then showmessage('scadenza licenza= ' + VarToStr(xScadenzaLic));

              //confronto tra dataserver e data di licenza
               data.qtemp.Close;
               data.qtemp.SQL.Text := 'if  getdate() <=' + '''' + xScadenzaLic + '''' +
                    ' begin select 1 controllo end ' +
                    ' else select 0 controllo';
               if data.Global.FieldByName('debughrms').asboolean = true then data.qtemp.sql.SaveToFile('QControlloDataLicenza.sql');
               data.QTemp.open;


               //if xDataServer <= StrToDate(xScadenzaLic) then
               if data.qtemp.FieldByName('controllo').asInteger = 1 then
               begin
                    if data.Global.FieldByName('debughrms').asboolean = true then ShowMessage('7B');
                    xProcedi := True;
               end
               else begin
                    if Data.Global.FieldByName('DEBUGHRMS').AsBoolean = true then
                         showmessage('Data server:' + DateToStr(xDataServer) + ' - Data scadenza:' + DatetoStr(StrToDateTime(xScadenzaLic)));
                    Result := 'Licenza scaduta';
                    exit;
               end;
               // Warning data scadenza
              // if StrToDateTime(xScadenzaLic) - xDataServer <= 30 then

               if data.Global.FieldByName('debughrms').asboolean = true then ShowMessage('7C');
               data.qtemp.close;
               data.QTemp.SQL.Text := 'select DATEDIFF ( day ,  getdate(),' + '''' + xScadenzaLic + '''' + ' ) Differenza ';
               data.QTemp.Open;
               if data.Global.FieldByName('debughrms').asboolean = true then ShowMessage('7D');

               if data.qtemp.fieldbyname('Differenza').asinteger <= 30 then
                    xLicWarning := 'La licenza scade tra giorni: ' + IntToStr(Round(StrToDateTime(xScadenzaLic) - xDataServer) + 1);


          except
               on Exception do raise;
          end;
     end;


     // CONTROLLO PRESENZA MACHINE ID
     // caricamento machine ID abilitati dal ConfigFile
     // --> solo se NumClient>0
     //manchine id nuova licenza
     if xNuovaLicenza = true then begin
          anode := XMLDocument1.documentElement.selectSingleNode('//IntestatarioLicenza/Software/LicenzaParametri/@Numclient');
          xNumClient := strtoint(anode.text);
          if xNumClient > 0 then begin
               xListaMachineID := TStringList.create;
               //anode := XMLDocument1.documentElement.selectSingleNode('//IntestatarioLicenza/Software/LicenzaParametri/@Numclient');
               nodes_se := XMLDocument1.selectNodes('//IntestatarioLicenza/Software/LicenzaParametri/MachineID/Client');
               nClient := nodes_se.length;
              // showmessage(inttostr(nodes_se.length));

              // n := XMLDocument1.DocumentElement.ChildNodes['Software'].ChildNodes['LicenzaParametri'].ChildNodes['MachineID'].ChildNodes.Count;
                //    XMLDocument1.documentElement.selectSingleNode('//IntestatarioLicenza/Software/LicenzaParametri/Machine').;

               for k := 0 to nClient - 1 do begin
                  //  xListaMachineID.Add(XMLDocument1.DocumentElement.ChildNodes['Software'].ChildNodes['LicenzaParametri'].ChildNodes['MachineID'].ChildNodes[k].Text);
                    anode := nodes_se.item[k];
                   // showmessage(anode.text);
                    xListaMachineID.Add(anode.text);
               end;
               if xListaMachineID.IndexOf(IntToStr(GetMachineID)) > -1 then xProcedi := True
               else begin
                    Result := 'Macchina non abilitata - utilizzare l''applicazione "RegisterClient" per registrarsi';
                    exit;
               end;
               xListaMachineID.Free;
          end;

     end else begin
     // machine id vecchia licenza
          if StrToInt(copy(xConfigString.strings[5], pos('=', xConfigString.strings[5]) + 1, length(xConfigString.strings[5]))) > 0 then begin
               xListaMachineID := TStringList.create;
               xMachineID := xConfigString.strings[9];
               while xMachineID <> '' do begin
                    xListaMachineID.Add(copy(xMachineID, 1, pos(';', xMachineID) - 1));
                    xMachineID := copy(xMachineID, pos(';', xMachineID) + 1, length(xMachineID))
               end;
               if xListaMachineID.IndexOf(IntToStr(GetMachineID)) > -1 then xProcedi := True
               else begin
                    Result := 'Macchina non abilitata - utilizzare l''applicazione "RegisterClient" per registrarsi';
                    exit;
               end;
               xListaMachineID.Free;
          end;
     end;





   // Riempimento StringList funzioni non abilitate
     // ** la stringList � una variabile public
     // attenzione che nella nuova licenza la variabile xFunctionsNO contiene le funzioni abilitate mentre nella vecchia gestione licenze contiene le funzioni NON abilitate
     if xNuovaLicenza = true then begin
          xFunctionsNO := TStringList.create;
          xFunctionsNO.Clear;
         // n := XMLDocument1.DocumentElement.ChildNodes['Software'].ChildNodes['LicenzaParametri'].ChildNodes['FunzioniModuli'].ChildNodes.Count;
          nodes_se := XMLDocument1.selectNodes('//IntestatarioLicenza/Software/LicenzaParametri/FunzioniModuli/Funzione');
          n := nodes_se.length;
          data.xSelAziende := false;
          for k := 0 to n - 1 do begin
               anode := nodes_se.item[k];
               // showmessage(anode.text);

               anodetemp := nodes_se.item[k].selectSingleNode('@Attivo');

            //   showmessage(anodetemp.text);

               if (anodetemp.text = '1') then
                    xFunctionsNO.Add(anode.text);

               if (uppercase(anode.text) = 'SELAZ') and (anodetemp.text = '1') then
                    data.xSelAziende := true;

          end;

     end else begin

          xFunctionsNO := TStringList.create;
          xFunctionsNO.Clear;
          xFunctions := xConfigString.strings[8];
          while xFunctions <> '' do begin
               xFunctionsNO.Add(copy(xFunctions, 1, pos(';', xFunctions) - 1));
               xFunctions := copy(xFunctions, pos(';', xFunctions) + 1, length(xFunctions))
          end;
     end;


   //  xFunctionsNO.savetofile('xFunctionsNO.txt');

     xDistributore := xConfigString.strings[11];
     xDistributore := copy(xDistributore, 0, (pos(';', xdistributore) - 1));
     // showmessage(xDistributore);
     if xDistributore = 'Team System' then begin
          MainForm.Image1.Visible := false;
          MainForm.Image2.Visible := false;
          mainform.Label67.Visible := false;
          mainform.Label45.Visible := false;
          MainForm.Caption := copy(MainForm.caption, 9, length(mainform.caption));
          MainForm.ImageTeamSystem.Picture.LoadFromFile(ExtractFileDir(Application.ExeName) + '\Logo.jpg');
          MainForm.ImageTeamSystem.Visible := true;
     end;


     // parte nuova per evitare che i clienti amiconi si passino il configfile e non paghino la licenza
     data.qtemp.close;
     data.qtemp.SQL.Text := 'select id from AFG_CoseFatte where descrizione=''AvviaCpc'' ';
     data.qtemp.Open;
     xNumAvvioCpC := data.qtemp.RecordCount;
     if (xNumAvvioCpC = 0) and (data.Global.FieldByName('CpC').AsString <> '') then begin
          Result := 'Licenza non corretta - Codice Z3CE ';
          exit;
     end;

     if xNumAvvioCpC = 0 then begin
          data.global.Edit;
          data.Global.FieldByName('CpC').Value := Data.TwCrypter1.CryptString('sauro1234');
          data.Global.Post;

          data.qtemp.close;
          data.qtemp.SQL.Text := ' insert into AFG_CoseFatte (descrizione, num) values( ''AvviaCpc'', 1)';
          data.qtemp.ExecSQL;
          xNumAvvioCpC := -1;
     end;


     if xNuovaLicenza = true then begin
          anode := XMLDocument1.documentElement.selectSingleNode('//IntestatarioLicenza/@IDCliente');
          xIDCliente := anode.text;
     end else
          xIDCliente := trim(copy(xConfigString.strings[2], 11, Length(xConfigString.strings[3]) - 1));



      // pezzo che fa switch tra vecchia licenza e nuova licenza
      // in realt� non serve perch� sia nella nuova licenza sia nella vecchia c'� sempre l'idcliente che � identico.. me ne sono accorto dopo ormai lo lascio almeno cancello il configfile txt
     if (xNuovaLicenza = true) and (FileExists(ExtractFileDir(Application.ExeName) + '\ConfigFile.txt')) then begin
          xListTemp := TStringList.Create;
          xListTemp.LoadFromFile(ExtractFileDir(Application.ExeName) + '\ConfigFile.txt');

          s := trim(copy(xListTemp.strings[2], 11, Length(xListTemp.strings[3]) - 1));
          if s <> Data.TwCrypter1.DecryptString(data.Global.FieldByName('CpC').asString) then begin //DecriptaStringa(DataEBC.GlobalCpC.AsString) then begin
               Result := 'Licenza non corretta - Codice Z4CE ';
               exit;
          end else begin
               Data.global.Edit;
               data.Global.FieldByName('CpC').value := Data.TwCrypter1.CryptString(xIDCliente); //CriptaStringa(xIDCliente);
               Data.Global.Post;

               Data.qtemp.close;
               Data.qtemp.SQL.Text := ' insert into AFG_CoseFatte (descrizione, num) values( ''AvviaCpc2'', 1)';
               Data.qtemp.ExecSQL;
          end;

          DeleteFile(ExtractFileDir(Application.ExeName) + '\ConfigFile.txt');

          Data.qtemp.close;
          Data.qtemp.SQL.Text := ' insert into AFG_CoseFatte (descrizione, num) values( ''DeleteConfigFile.txt'', 1)';
          Data.qtemp.ExecSQL;

          xListTemp.Free;
     end;


     if (data.Global.FieldByName('CpC').asString = 'z2CECw6GvmBe') and (xNumAvvioCpC = -1) then begin
          Data.global.Edit;
          data.Global.FieldByName('CpC').Value := Data.TwCrypter1.CryptString(xIDCliente);
          Data.Global.Post;
     end;



     // showmessage(DataEBC.TwCrypter1.DecryptString(DataEBC.GlobalCpC.AsString) + ' - ' + xIDClienteCat);
     XDominio := fGetDomainName;
     xdominio := UpperCase(xDominio);
    // if (xDominio <> 'EBCCONSULTING') and (copy(xDominio, 0, 6) <> 'WKTDEV') then begin
     if 1 = 1 then begin
          // if DataEBC.TwCrypter1.DecryptString(DataEBC.GlobalCpC.AsString) <> xIDCliente then begin
          if data.TwCrypter1.DecryptString(data.Global.fieldbyname('CpC').AsString) <> xIDCliente then begin
               Result := 'Licenza non corretta - Codice Z2CE ';
               exit;
          end;
     end;




     // FINE
     Result := '';
     xConfigString.Free;

     if data.Global.FieldByName('debughrms').asboolean = true then ShowMessage('Fine AccessVerify con result = ' + Result);

end;

function TLoginForm.GetMachineID: longint;
var i: dword;
     buf: array[0..1023] of byte;
begin
     GetVolumeInformation('c:\', nil, 0, PDWord(@Buf[1]) {serial number}, I {not used}, I {not used}, nil, 0);
     result := longint((@buf[1])^)
end;

procedure TLoginForm.LbRSASSA1GetSignature(Sender: TObject; var Sig: TRSASignatureBlock);
begin
     HexToBuffer(xFirma, Sig, SizeOf(Sig));
end;

function TLoginForm.RecuperaUserName: string;
const
     MAX_NAME_STRING = 1024;
var userName: array[0..MAX_NAME_STRING] of Char;
     userNameSize: DWORD;
begin
     userNameSize := MAX_NAME_STRING;
     GetUserName(userName, userNameSize);
     Result := UserName;
end;

function TLoginForm.CheckUserName: string;
var xFileXML, xLog: TStringList;
     xJvXML: TJvSimpleXml;
     k, BufSize: integer;
     xVarName, xVarContent, XDominio, XUserPC: string;
     xFileName: string;
begin
     Q1.Close;
     Q1.SQL.Text := 'SET DATEFORMAT dmy';
     Q1.ExecSQL;

     // SINGLE SIGN-ON CON TEAMSYSTEM
     xFileName := GetEnvVarValue('XMLSESSION');
     if (xFileName <> 'Variabile XMLSESSION non trovata') and (FileExists(xFileName)) then begin
          // viene passato come parametro (ATTENZIONE: necessariamente il secondo) il path del file XML
          xLog := TStringList.create;

          xLog.Text := 'Inizio ' + DateToStr(Date) + chr(13);
          xLog.SaveToFile(ExtractFileDir(Application.exename) + '\Log_SSO_Teamsy.txt');

          // apertura file
          xFileXML := TStringList.create;
          xFileXML.LoadFromFile(xFileName);

          xLog.Text := xLog.Text + 'Caricato file ' + xFileName + chr(13);
          xLog.SaveToFile(ExtractFileDir(Application.exename) + '\Log_SSO_Teamsy.txt');

          xJvXML := TJvSimpleXml.Create(nil);
          xJvXML.LoadFromString(xFileXML.Text);

          xLog.Text := xLog.Text + 'Aperto XML ' + chr(13);
          xLog.SaveToFile(ExtractFileDir(Application.exename) + '\Log_SSO_Teamsy.txt');

          // lettura utente
          xNomeUtente_file := xJvXML.root.Items.ItemNamed['xmlsessione'].Items.ItemNamed['utente'].Items.ItemNamed['nomeutente'].Value;

          xLog.Text := xLog.Text + 'Nome utente nel file XML = ' + xNomeUtente_file + chr(13);
          xLog.SaveToFile(ExtractFileDir(Application.exename) + '\Log_SSO_Teamsy.txt');

          xUtente := '';
          QUsers.Close;
          QUsers.SQL.Text := 'select * from Users a,userssoftwares b where a.id=b.idutente and b.idsoftware = 2 and Win_User=:x';
          QUsers.Parameters[0].Value := xNomeUtente_file;
          QUsers.Open;
          if not xEsci then begin
               if Qusers.RecordCount > 0 then begin
                    xUtente := QUsers.FieldByName('Nominativo').asString;

                    xLog.Text := xLog.Text + 'Trovato utente H1 = ' + xUtente + chr(13);
                    xLog.SaveToFile(ExtractFileDir(Application.exename) + '\Log_SSO_Teamsy.txt');

                    Procedi(true);
                    if data.Global.FieldByName('debughrms').asboolean = true then ShowMessage('LoginForm.CheckUserName - Ritorno alla procedura dopo Procedi');
               end else begin
                    xLog.Text := xLog.Text + 'NON Trovato utente H1 ' + chr(13);
                    xLog.SaveToFile(ExtractFileDir(Application.exename) + '\Log_SSO_Teamsy.txt');
               end;
          end else begin
               MainForm.xEsci := true;
               Close;
          end;

          Result := xUtente;
          if data.Global.FieldByName('debughrms').asboolean = true then ShowMessage('CheckUserName.Result = ' + Result);

     end else begin

          if pos('Integrated Security=SSPI', Data.DB.ConnectionString) > 0 then begin
               // autenticazione Windows
               xUtente := '';
               QUsers.Close;
               QUsers.SQL.Text := 'select * from Users a, UsersSoftwares b where a.id=b.idutente and b.idsoftware = 1 and Win_User=system_user';
               QUsers.Open;
               if not xEsci then begin
                    if Qusers.RecordCount > 0 then begin
                         xUtente := QUsers.FieldByName('Nominativo').asString;
                         Procedi(true);
                    end;
               end else begin
                    if data.Global.FieldByName('debughrms').asboolean = true then ShowMessage('xesci=true / A');
                    MainForm.xEsci := true;
                    Close;
               end;
               Result := xUtente;
          end else begin
               //autenticazione mista (15/10/2012)
               if data.Global.fieldbyname('DebugHrms').asBoolean = true then
                    showmessage('AutenticazioneMista');
               XUserPC := GetUserFromWindows;
               XDominio := fGetDomainName;
               //showmessage(XDominio+'\'+XUserPC);

               QUsers.Close;
               QUsers.SQL.Text := 'select * from Users a,userssoftwares b where a.id=b.idutente and b.idsoftware = 1 and Win_User=''' + XDominio + '\' + XUserPC + '''';
               QUsers.Open;
               if data.Global.fieldbyname('DebugHrms').asBoolean = true then
                    showmessage('utenti trovati con ' + XDominio + '\' + XUserPC + ' =' + inttostr(QUsers.recordcount));
               if qUsers.RecordCount > 0 then begin
                    xUtente := QUsers.FieldByName('Nominativo').asString;
                    if data.Global.fieldbyname('DebugHrms').asBoolean = true then
                         showmessage('xutente=' + xutente);
                    Procedi(false);
                    //showmessage(xutente);

               end else begin
                    // autenticazione "vecchia": effettua login come una volta
                    ShowModal;
                    xUtente := QUsers.FieldByName('Nominativo').asString;
                    //Procedi;
               end;
               Result := xUtente;
          end;
     end;
end;


procedure TLoginForm.Procedi(xchiudi: boolean);
var i, xIDOrg: integer;
begin
     if data.Global.FieldByName('debughrms').asboolean = true then ShowMessage('LoginForm.Procedi - INIZIO');

     if (xchiudi) then
          close;

     if data.Global.FieldByName('debughrms').asboolean = true then ShowMessage('LoginForm.Procedi - A');

     Accedi := 'S';
     // aggiornamento Last Login
     Q1.Close;
     Q1.SQL.Text := 'update Global set LastLogin=:xLastLogin,LastLoginID=:xLastLoginID';
     Q1.Parameters.ParamByName('xLastLogin').Value := QUsers.FieldByName('Nominativo').AsString;
     Q1.Parameters.ParamByName('xLastLoginID').Value := QUsers.FieldByName('ID').AsInteger;
     Q1.ExecSQL;

     if data.Global.FieldByName('debughrms').asboolean = true then ShowMessage('LoginForm.Procedi - B');

     // nominativo last login in locale
     ScriviRegistry('LastLoginUsername', QUsers.FieldByName('Nominativo').AsString);

     if data.Global.FieldByName('debughrms').asboolean = true then ShowMessage('LoginForm.Procedi - C');

     // inserimento accesso nel log accessi
     Q1.SQL.Text := 'insert into Log_accessi (IDUtente,Data,OraEntrata,Modulo) ' +
          'values (:xIDUtente,:xData,:xOraEntrata,:xModulo)';
     Q1.Parameters.ParamByName('xIDUtente').Value := QUsersID.Value;
     Q1.Parameters.ParamByName('xData').Value := Date;
     Q1.Parameters.ParamByName('xOraEntrata').Value := Now;
     Q1.Parameters.ParamByName('xModulo').Value := 'MB';
     Q1.ExecSQL;

     if data.Global.FieldByName('debughrms').asboolean = true then ShowMessage('LoginForm.Procedi - punto 1');

     // modifica data ultimo accesso e MAC Address
     Q1B.Close;
     Q1B.SQL.Text := 'update Users set DataUltimoAccesso=:xData, Lana0=:xLana0, MachineID=:xMachineID where ID=' + QUsers.FieldByName('ID').asString;
     Q1B.Parameters.ParamByName('xData').value := Date;
     Q1B.Parameters.ParamByName('xLana0').value := TrimRight(xMACaddress);
     Q1B.Parameters.ParamByName('xMachineID').value := GetMachineID;
     Q1B.ExecSQL;

     if data.Global.FieldByName('debughrms').asboolean = true then ShowMessage('LoginForm.Procedi - punto 2');

     Data.Global.Close;
     Data.Global.Open;
     Data.QTemp.Close;
     Data.QTemp.SQL.text := 'select @@SERVERNAME ServerName';
     Data.QTemp.Open;
     MainForm.Statusbar1.Panels[0].text := 'Utente attuale: ' + Data.Global.FieldByName('LastLogin').AsString;
     MainForm.Statusbar1.Panels[1].text := Data.QTemp.FieldByName('ServerName').asString + '/' + Data.DB.DefaultDatabase;
     MainForm.Statusbar1.Panels[5].text := Data.Global.FieldByName('NomeAzienda').AsString;

     MainForm.AdvOfficeStatusBar1.Panels[0].text := 'Utente attuale: ' + Data.Global.FieldByName('LastLogin').AsString;
     MainForm.AdvOfficeStatusBar1.Panels[1].text := Data.QTemp.FieldByName('ServerName').asString + '/' + Data.DB.DefaultDatabase;
     MainForm.AdvOfficeStatusBar1.Panels[3].text := Data.Global.FieldByName('NomeAzienda').AsString;




     Data.QTemp.Close;
     MainForm.xUtenteAttuale := Data.Global.FieldByName('LastLogin').AsString;
     MainForm.xIDUtenteAttuale := QUsers.FieldByName('ID').AsInteger;
     MainForm.xSoloClientiCollegati := QUsers.FieldByName('SoloClientiCollegati').AsBoolean;
     MainForm.xUtenteAttualeTipo := QUsers.FieldByName('Tipo').AsINteger;
     if QUsers.FieldByName('Tipo').AsINteger > 1 then
          MainForm.DBEDubbiIns.ReadOnly := True;

     if data.Global.FieldByName('debughrms').asboolean = true then ShowMessage('LoginForm.Procedi - punto 3');

     // cancellazione vecchi messaggi
     Q1.SQL.Clear;
     Q1.SQL.Add('delete from Promemoria where DataDaLeggere<=:xData');
     Q1.Prepared := TRUE;
     Q1.Parameters.items[0].Value := Date - 30;
     Q1.ExecSQL;
     // Filtri Promemorie
     {Data.TPromScaduti.Open;
     Data.TPromScaduti.Filter := '(DataLettura=null) and (DataDaLeggere<''' + DateToStr(date) + ''') and IDUtente=' + Data.GlobalLastLoginID.asString;

     Data.QPromFuturi.Prepared := TRUE;
     Data.QPromFuturi.Parameters.Items[0].Value := DateToStr(Date);
     Data.QPromFuturi.Parameters.Items[1].Value := Data.GlobalLastLoginID.Value;
     Data.QPromFuturi.Open;

     Data.QPromOggi.Prepared := TRUE;
     Data.QPromOggi.Parameters.Items[0].Value := DateToStr(Date);
     Data.QPromOggi.Parameters.Items[1].Value := Data.GlobalLastLoginID.Value;
     Data.QPromOggi.Open;
     case Data.QPromOggi.RecordCount of
          0: MainForm.StatusBar1.Panels[3].Text := 'nessun messaggio in scadenza oggi';
          1: MainForm.StatusBar1.Panels[3].Text := '� presente un messaggio in scadenza oggi';
     else MainForm.StatusBar1.Panels[3].Text := 'Sono presenti ' + IntToStr(Data.QPromOggi.RecordCount) + ' messaggi in scadenza oggi';
     end;      }

     // TOLTO con il MyQuickView
     //Application.CreateForm(Tmessinizialeform,messinizialeform);
     //Messinizialeform.LOggi.Caption:=IntToStr(Data.QPromOggi.RecordCount);
     //Messinizialeform.LfUTURI.Caption:=Inttostr(Data.QPromFuturi.RecordCount);
     //Messinizialeform.LScaduti.Caption:=Inttostr(Data.TPromScaduti.RecordCount);
     //Messinizialeform.LBenvenuto.caption:='Benvenuto, utente '+xUtente;
     //Messinizialeform.ShowModal;

     {Data.QPromOggi.Close;
     Data.QPromFuturi.Close;  }

     // Obj rights
     for i := 1 to 1000 do Mainform.xUserObj[i] := '';
     QUserObj.Close;
     QUserObj.Prepared := TRUE;
     QUserObj.Parameters.Items[0].Value := Data.Global.FieldByName('LastLoginID').Value;
     QUserObj.Open;
     i := 1;
     mainForm.xReportMasterFattProprio := TRUE;
     MainForm.dxNB_Tab_RepMaster.Visible := true;
     MainForm.BReportMaster.Visible := true;
     mainform.Pagecontrol1.ActivePage := mainform.TSInizio; //mainform.Pagecontrol1.ActivePage := mainform.TSRiepilogo;   // Da riattivare per visualizzazione reportTone
     mainForm.xReportMasterFattAz := true;
     while not QUserObj.EOF do begin
          //                         Mainform.xUserObj[i]:=QUserObjCodOggetto.Value;
          Mainform.xUserObj[i] := QUserObj.FieldByName('CodOggetto').Value;
          //Controllo diritti per il report master
         { if Mainform.xUserObj[i] = '78' then
          begin
               MainForm.dxNB_Tab_RepMaster.Visible := FALSE;
               mainForm.PageControl1.Activepage := mainform.TSInizio;
          end else begin
               MainForm.dxNB_Tab_RepMaster.Visible := TRUE;
               mainForm.PageControl1.Activepage := mainform.TSRiepilogo;
          end;
          if Mainform.xUserObj[i] = '780' then mainForm.xReportMasterFattProprio := FALSE;
          if Mainform.xUserObj[i] = '781' then mainForm.xReportMasterFattAz := FALSE;    }
          QUserObj.Next;
          inc(i);
     end;

     if data.Global.FieldByName('debughrms').asboolean = true then ShowMessage('LoginForm.Procedi - punto 4');

     if not MainForm.CheckProfile('78', False) then begin
          MainForm.dxNB_Tab_RepMaster.Visible := FALSE;
          MainForm.BReportMaster.Visible := false;
          mainForm.PageControl1.Activepage := mainform.TSInizio;
     end else begin
          MainForm.dxNB_Tab_RepMaster.Visible := TRUE;
          MainForm.BReportMaster.Visible := true;
          mainForm.PageControl1.Activepage := mainform.TSRiepilogo;
          //end;
          //if Mainform.xUserObj[i] = '780' then mainForm.xReportMasterFattProprio := FALSE;
         // if Mainform.xUserObj[i] = '781' then mainForm.xReportMasterFattAz := FALSE;

     end;

     if data.Global.FieldByName('debughrms').asboolean = true then ShowMessage('LoginForm.Procedi - punto 5');


     /// fede:29/02/2016  qua in giro sul report master c'� il bug del 29
     if mainform.PageControl1.ActivePage = mainform.TSRiepilogo then begin
          if MainForm.dxNB_Tab_RepMaster.Visible then
          begin
               //if mainform.FrmStatistiche1.TabSheet1.TabVisible then
               mainform.FrmStatistiche1.RefreshQueries;
               mainform.FrmStatistiche1.CheckRights;
               mainform.FrmStatistiche1.Visible := true;
          end
          else
          begin
               mainform.FrmStatistiche1.Visible := FALSE;
               mainform.FrmStatistiche1.CloseQueries;
          end;
     end;


     if data.Global.FieldByName('debughrms').asboolean = true then ShowMessage('LoginForm.Procedi - punto 6');

     //gestione cloud
     mainform.xSendMail_Remote := QUsersSendMail_RemoteSel.asBoolean;

     // PASSAGGIO PARAMETRI LICENZA
     // - numero massimo soggetti consentiti in anagrafica
     if xNumMaxSoggetti = 0 then
          MainForm.xMaxCons := -1
     else MainForm.xMaxCons := xNumMaxSoggetti;
     // caricamento codici Anag dei primi consentiti
     //MainForm.xSoggettiOK := TStringList.Create;
     if MainForm.xMaxCons <> -1 then
          CaricaPrimi;
     // - funzioni disabilitati
     MainForm.xFunctionsNO := TStringList.Create;
     MainForm.xFunctionsNO.Text := xFunctionsNO.Text;

     // flag visibilit� organigramma: crea lista nodi visibili
     {DataRicerche.xListaStr := '';
     DataRicerche.xOrgSoloSotto := QUsers.FieldByName('Org_SoloSotto').asBoolean;
     if (QUsers.FieldByName('Org_SoloSotto').asBoolean) and (QUsers.FieldByName('IDAnagrafica').asString <> '') then begin
          DataRicerche.xListaStr := '(';
          Q1.Close;
          Q1.SQL.text := 'select distinct Organigramma.ID,Parent,Descrizione ' +
               'from OrgDipendenti,Organigramma where Organigramma.ID=OrgDipendenti.IDOrganigramma ' +
               'and OrgDipendenti.IDDipendente=' + QUsers.FieldByName('IDAnagrafica').asString;
          Q1.Open;
          xIDOrg := Q1.FieldByName('ID').asInteger;
          DataRicerche.CreaListaIDOrg(xIDOrg);
          DataRicerche.xListaStr := Copy(DataRicerche.xListaStr, 1, length(DataRicerche.xListaStr) - 1) + ')';
     end; }

     {if (xNomeUtente_file = '') then
          close;}

     if data.Global.FieldByName('debughrms').asboolean = true then ShowMessage('LoginForm.Procedi - FINE');
end;


procedure TLoginForm.MaskEdit1KeyDown(Sender: TObject; var Key: Word;
     Shift: TShiftState);
var QTemp: TAdoQuery;
begin
     if ((GetKeyState(VK_CONTROL) and 128) = 128) and
          ((GetKeyState(ord('L')) and 128) = 128) and
          ((GetKeyState(ord('0')) and 128) = 128)
          then begin
          qtemp := TADOQuery.Create(nil);
          qtemp.Create(nil);
          qtemp.ConnectionString := data.DB.ConnectionObject.ConnectionString;
          qtemp.close;
          QTemp.SQL.Text := 'update users set lana0=null ';
          qtemp.ExecSQL;
          windows.beep(300, 500);
          QTemp.close;
          QTemp.free;
     end;

     if GetKeyState(VK_CAPITAL) > 0 then
          pbLOCmAIUSC.Visible := true
     else
          pbLOCmAIUSC.Visible := false;

end;

procedure TLoginForm.MaskEdit1Enter(Sender: TObject);
begin
     if GetKeyState(VK_CAPITAL) > 0 then
          pbLOCmAIUSC.Visible := true
     else
          pbLOCmAIUSC.Visible := false;
end;

procedure TLoginForm.MaskEdit1KeyPress(Sender: TObject; var Key: Char);
begin
     if GetKeyState(VK_CAPITAL) > 0 then
          pbLOCmAIUSC.Visible := true
     else
          pbLOCmAIUSC.Visible := false;

     if key = chr(13) then BitBtn1Click(self);
end;

procedure TLoginForm.MaskEdit1KeyUp(Sender: TObject; var Key: Word;
     Shift: TShiftState);
begin
     if GetKeyState(VK_CAPITAL) > 0 then
          pbLOCmAIUSC.Visible := true
     else
          pbLOCmAIUSC.Visible := false;
end;

function TLoginForm.GetEnvVarValue(const VarName: string): string;
var
     BufSize: Integer; // buffer size required for value
begin
     // Get required buffer size (inc. terminal #0)
     BufSize := GetEnvironmentVariable(
          PChar(VarName), nil, 0);
     if BufSize > 0 then begin
          // Read env var value into result string
          SetLength(Result, BufSize - 1);
          GetEnvironmentVariable(PChar(VarName),
               PChar(Result), BufSize);
     end else
          // No such environment variable
          Result := 'Variabile XMLSESSION non trovata';
end;

end.

