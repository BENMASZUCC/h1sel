//[TONI20021003]DEBUGOK
unit DettAnnuncio;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, DBTables, StdCtrls, DBCtrls, Mask, Grids, DBGrids, Buttons, ADODB,
     U_ADOLinkCl, TB97, ExtCtrls;

type
     TDettAnnuncioForm = class(TForm)
          QAnnuncio_OLD: TQuery;
          DsQAnnuncio: TDataSource;
          QAnnuncio_OLDID: TAutoIncField;
          QAnnuncio_OLDRif: TStringField;
          QAnnuncio_OLDData: TDateTimeField;
          QAnnuncio_OLDTesto: TMemoField;
          QAnnuncio_OLDCivetta: TBooleanField;
          QAnnuncio_OLDFileTesto: TStringField;
          QAnnuncio_OLDNumModuli: TSmallintField;
          QAnnuncio_OLDNumParole: TSmallintField;
          QAnnuncio_OLDTotaleLire: TFloatField;
          QAnnuncio_OLDTotaleEuro: TFloatField;
          QAnnuncio_OLDNote: TMemoField;
          QAnnuncio_OLDArchiviato: TBooleanField;
          QAnnuncio_OLDCVPervenuti: TSmallintField;
          QAnnuncio_OLDCVIdonei: TSmallintField;
          QAnnuncio_OLDCVChiamati: TSmallintField;
          QAnnuncio_OLDIDFattura: TIntegerField;
          QAnnuncio_OLDDataPubblicazione: TDateTimeField;
          QAnnuncio_OLDCostoLire: TFloatField;
          QAnnuncio_OLDNomeEdizione: TStringField;
          QAnnuncio_OLDTestata: TStringField;
          DBGrid1: TDBGrid;
          Label1: TLabel;
          DBEdit1: TDBEdit;
          Label2: TLabel;
          DBMemo1: TDBMemo;
          DBCheckBox1: TDBCheckBox;
          Label3: TLabel;
          DBEdit2: TDBEdit;
          Label4: TLabel;
          DBEdit3: TDBEdit;
          Label5: TLabel;
          DBEdit4: TDBEdit;
          Label6: TLabel;
          DBEdit5: TDBEdit;
          DBCheckBox2: TDBCheckBox;
          Label7: TLabel;
          QAnnuncio: TADOLinkedQuery;
    Panel1: TPanel;
    BOK: TToolbarButton97;
          procedure FormShow(Sender: TObject);
    procedure BOKClick(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     DettAnnuncioForm: TDettAnnuncioForm;

implementation
//[/TONI20020726\]
uses MOduloDati, Main;
//[/TONI20020726\]FINE
{$R *.DFM}

procedure TDettAnnuncioForm.FormShow(Sender: TObject);
begin
//Grafica
     DettAnnuncioForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;

     Caption := '[S/19] - ' + Caption;
end;

procedure TDettAnnuncioForm.BOKClick(Sender: TObject);
begin
      DettAnnuncioForm.ModalResult:=mrOK;
end;

end.
