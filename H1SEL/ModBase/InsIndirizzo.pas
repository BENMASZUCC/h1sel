unit InsIndirizzo;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, Db, ADODB, dxCntner, dxEditor, dxExEdtr, dxDBEdtr,
     dxDBELib, TB97, ExtCtrls;

type
     TInsindirizzoForm = class(TForm)
          GroupBox8: TGroupBox;
          LTipoStradaCli: TLabel;
          Label63: TLabel;
          Label71: TLabel;
          Label72: TLabel;
          LIndirizzoCli: TLabel;
          Label113: TLabel;
          eIndirizzo: TEdit;
          eNumCivico: TEdit;
          eComune: TEdit;
          eProvincia: TEdit;
          Label1: TLabel;
          Label2: TLabel;
          eCap: TEdit;
          cbTipoStrada: TComboBox;
          eNazione: TEdit;
          Label3: TLabel;
          eTelefono: TEdit;
          eFax: TEdit;
          Label4: TLabel;
          EDescriz: TEdit;
          Label5: TLabel;
          DBLKTipoIndirizzo: TdxDBLookupEdit;
          QChekTipologia: TADOQuery;
          Panel1: TPanel;
          BitBtn1: TToolbarButton97;
          BAnnulla: TToolbarButton97;
          SpeedButton12: TToolbarButton97;
          BCliSelNaz: TToolbarButton97;
          procedure SpeedButton12Click(Sender: TObject);
          procedure BCliSelNazClick(Sender: TObject);
          procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
          procedure BitBtn2Click(Sender: TObject);
          procedure BitBtn1Click(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure BAnnullaClick(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     InsindirizzoForm: TInsindirizzoForm;

implementation

uses Comuni, SelNazione, modulodati, indirizzicliente, modulodati2, Main;

{$R *.DFM}

procedure TInsindirizzoForm.SpeedButton12Click(Sender: TObject);
begin
     ComuniForm := TComuniForm.create(self);
     ComuniForm.ShowModal;
     if ComuniForm.ModalResult = mrOK then begin
          eComune.Text := ComuniForm.QComuni.FieldByname('Descrizione').AsString;
          eCap.Text := ComuniForm.QComuni.FieldByname('Cap').AsString;
          eProvincia.Text := ComuniForm.QComuni.FieldByname('Prov').AsString;
     end else begin
          eComune.Text := '';
          eCap.Text := '';
          eProvincia.Text := '';
     end;
     ComuniForm.Free;
end;

procedure TInsindirizzoForm.BCliSelNazClick(Sender: TObject);
begin
     SelNazioneForm := TSelNazioneForm.create(self);
     SelNazioneForm.ShowModal;
     if (SelNazioneForm.ModalResult = mrOK) and (SelNazioneForm.QNazioni.RecordCount > 0) then begin
          eNazione.Text := SelNazioneForm.QNazioni.FieldByName('Abbrev').AsString;
     end else begin
          eNazione.Text := '';
     end;
     SelNazioneForm.Free;
end;

procedure TInsindirizzoForm.FormCloseQuery(Sender: TObject;
     var CanClose: Boolean);
begin
     if (DBLKTipoIndirizzo.LookupKeyValue = 2) and (InsindirizzoForm.ModalResult = mrOK) then begin
          QChekTipologia.close;
          QChekTipologia.Parameters.ParamByName('ID').value := IndirizziClienteForm.qClienteIndirizziID.Value;
          QChekTipologia.Parameters.ParamByName('IDCliente').value := Data2.TEbcClientiID.Value;
          QChekTipologia.Open;
          if QChekTipologia.RecordCount > 0 then begin
               MessageDlg('Per questo cliente � gia presente un indirizzo per il "Recapito Fatture"', mtConfirmation, [mbOK], 0);
               CanClose := false;

          end
          else
               CanClose := true;
     end
     else
          CanClose := true;
end;

procedure TInsindirizzoForm.BitBtn2Click(Sender: TObject);
begin
     IndirizziClienteForm.qClienteIndirizzi.cancel;
     IndirizziClienteForm.ModalResult := mrCancel;
end;

procedure TInsindirizzoForm.BitBtn1Click(Sender: TObject);
begin
     InsindirizzoForm.ModalResult := mrOK;
end;

procedure TInsindirizzoForm.FormShow(Sender: TObject);
begin
     InsindirizzoForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
end;

procedure TInsindirizzoForm.BAnnullaClick(Sender: TObject);
begin
     InsindirizzoForm.ModalResult := mrCancel;
end;

end.

