unit Inserimento;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, Mask, DBCtrls, ExtCtrls, Db, DBTables, Grids, DBGrids,
     DtEdit97, ADODB, U_ADOLinkCl, dxTL, dxDBCtrl, dxDBGrid, dxCntner;

type
     TInserimentoForm = class(TForm)
          Panel1: TPanel;
          Panel7: TPanel;
          DBEdit12: TDBEdit;
          DBEdit14: TDBEdit;
          BitBtn1: TBitBtn;
          BitBtn2: TBitBtn;
          PanClienti: TPanel;
          Panel12: TPanel;
          DsEBCclienti: TDataSource;
          TAnagInserim: TADOLinkedTable;
          TEBCClienti: TADOLinkedQuery;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1Descrizione: TdxDBGridMaskColumn;
          dxDBGrid1Comune: TdxDBGridMaskColumn;
          procedure FormShow(Sender: TObject);
     private
          { Private declarations }
          xStringa: string;
     public
          { Public declarations }
     end;

var
     InserimentoForm: TInserimentoForm;

implementation

uses ModuloDati;

{$R *.DFM}

procedure TInserimentoForm.FormShow(Sender: TObject);
begin
     Caption := '[E/2] - ' + Caption;
end;

end.
