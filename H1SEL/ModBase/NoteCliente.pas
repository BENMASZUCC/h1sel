//[TONI20021004]DEBUGOK
unit NoteCliente;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, Db, DBTables, FrameDBRichEdit2, ExtCtrls, ADODB,
     U_ADOLinkCl;

type
     TNoteClienteForm = class(TForm)
          Panel1: TPanel;
          FrameDBRich1: TFrameDBRich;
          DsQNoteCliente: TDataSource;
          BitBtn1: TBitBtn;
          UpdNoteCliente: TUpdateSQL;
          QNoteCliente: TADOLinkedQuery;
          QNoteClienteID: TAutoIncField;
          QNoteClienteNote: TMemoField;
          ADOQuery1: TADOQuery;
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
          procedure FrameDBRich1ToolButton1Click(Sender: TObject);
          procedure FrameDBRich1SaveButtonClick(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     NoteClienteForm: TNoteClienteForm;

implementation

uses ModuloDati;

{$R *.DFM}

//[/TONI20020911\]

procedure TNoteClienteForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     // ALE 21/11/02 2.0.0.19 - controllo stato, altrimenti d� errore
     if QNoteCliente.State in [dsInsert, dsEdit] then begin
          with QNoteCliente do begin
               Data.DB.BeginTrans;
               try
                    Post;
                    Data.DB.CommitTrans;
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                    raise;
               end;
          end;
     end;
     DsQNoteCliente.dataset := qnotecliente
          // ALE 21/11/02 2.0.0.19 - FINE
end;
//[/TONI20020911\]FINE

procedure TNoteClienteForm.FrameDBRich1ToolButton1Click(Sender: TObject);
begin
     FrameDBRich1.PostExecute(Sender);

end;

procedure TNoteClienteForm.FrameDBRich1SaveButtonClick(Sender: TObject);
begin
     FrameDBRich1.FileSaveCmdExecute(Sender);

end;

end.
