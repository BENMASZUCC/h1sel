unit RuoloRif;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, Grids, DBGrids, Db, DBTables, ExtCtrls, ADODB,
     U_ADOLinkCl, TB97, dxTL, dxDBCtrl, dxDBGrid, dxCntner;

type
     TRuoloRifForm = class(TForm)
          DsRuoli: TDataSource;
          Panel1: TPanel;
          TRuoli_Old: TADOLinkedTable;
          TRuoli: TADOQuery;
          BitBtn1: TToolbarButton97;
          TRuoliDescrizione: TStringField;
    dxDBGrid1: TdxDBGrid;
    dxDBGrid1Descrizione: TdxDBGridColumn;
    TRuoliID: TAutoIncField;
    TRuoliMansioni: TMemoField;
          procedure BitBtn1Click(Sender: TObject);
          procedure DBGrid1KeyPress(Sender: TObject; var Key: Char);
          procedure FormShow(Sender: TObject);
     private
          xStringa: string;
          { Private declarations }
     public
          { Public declarations }
     end;

var
     RuoloRifForm: TRuoloRifForm;

implementation

uses MOduloDati, ValutazDip, Main;

{$R *.DFM}

procedure TRuoloRifForm.BitBtn1Click(Sender: TObject);
begin
     RuoloRifForm.ModalResult := mrOk;
     //ValutazDipForm.TRuoli.Locate('ID', TRuoli.FieldByName('ID').AsInteger, []);
     ValutazDipForm.xidRuolo := TRuoli.FieldByName('ID').AsInteger;
end;

procedure TRuoloRifForm.DBGrid1KeyPress(Sender: TObject; var Key: Char);
begin
     if Key = chr(13) then
          BitBtn1.Click;
     {if Key = chr(13) then begin
          BitBtn1.Click;
     end else begin
          xStringa := xStringa + key;
          TRuoli.FindNearestADO(VarArrayOf([xStringa]));
     end;}
end;

procedure TRuoloRifForm.FormShow(Sender: TObject);
begin
//grafica
     RuoloRifForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     TRuoli.Close;
     TRuoli.Open;
end;
end.

