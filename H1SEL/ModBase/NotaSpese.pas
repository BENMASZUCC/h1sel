unit NotaSpese;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     FloatEdit, DtEdit97, ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls,
     ToolEdit, CurrEdit;

type
     TNotaSpeseForm = class(TForm)
          BitBtn1: TBitBtn;
          BitBtn2: TBitBtn;
          Panel1: TPanel;
          Label1: TLabel;
          DBEDenomCliente: TDBEdit;
          GroupBox1: TGroupBox;
          ERicRif: TEdit;
          ERicRuolo: TEdit;
          SpeedButton1: TSpeedButton;
          GroupBox2: TGroupBox;
          SpeedButton2: TSpeedButton;
          ESoggCogn: TEdit;
          RGTipoSogg: TRadioGroup;
          ESoggNome: TEdit;
          Label2: TLabel;
          DEData: TDateEdit97;
          Label3: TLabel;
          EDescrizione: TEdit;
          GroupBox3: TGroupBox;
          Label4: TLabel;
          Label5: TLabel;
          Label6: TLabel;
          Label7: TLabel;
          Label8: TLabel;
          Label9: TLabel;
          Label10: TLabel;
          Label11: TLabel;
          EAltroDesc: TEdit;
          Label13: TLabel;
          Label14: TLabel;
          Label15: TLabel;
          Label16: TLabel;
          Label17: TLabel;
          Label18: TLabel;
          Label19: TLabel;
          Label20: TLabel;
          Label12: TLabel;
          Label21: TLabel;
          Label22: TLabel;
          Label23: TLabel;
          Label24: TLabel;
          Label25: TLabel;
          Label26: TLabel;
          GroupBox4: TGroupBox;
          EFattNum: TEdit;
          EFattData: TEdit;
          SpeedButton3: TSpeedButton;
          RxCalcEdit1: TRxCalcEdit;
          RxCalcEdit2: TRxCalcEdit;
          RxCalcEdit3: TRxCalcEdit;
          RxCalcEdit4: TRxCalcEdit;
          RxCalcEdit5: TRxCalcEdit;
          RxCalcEdit6: TRxCalcEdit;
          RxCalcEdit7: TRxCalcEdit;
          RxCalcEdit8: TRxCalcEdit;
          SpeedButton50: TSpeedButton;
          procedure SpeedButton1Click(Sender: TObject);
          procedure SpeedButton2Click(Sender: TObject);
          procedure SpeedButton3Click(Sender: TObject);
          procedure FormCreate(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure SpeedButton50Click(Sender: TObject);
     private
     public
          xIDRicerca, xIDSoggetto, xIDFattura: integer;
     end;

var
     NotaSpeseForm: TNotaSpeseForm;

implementation

uses ModuloDati2, ElencoRicCliente, ElencoDip, ElencoUtenti,
     ElencoFattCliente;

{$R *.DFM}

//[/TONI20020911\]

procedure TNotaSpeseForm.SpeedButton1Click(Sender: TObject);
begin
     ElencoRicClienteForm := TElencoRicClienteForm.create(self);
     ElencoRicClienteForm.QRicCliente.Parameters[0].value := Data2.TEbcClienti.FieldByName('ID').asInteger;
     ElencoRicClienteForm.QRicCliente.Open;
     ElencoRicClienteForm.ShowModal;
     if ElencoRicClienteForm.Modalresult = mrOK then begin
          xIDRicerca := ElencoRicClienteForm.QRicCliente.FieldByName('ID').AsInteger;
          ERicRif.Text := ElencoRicClienteForm.QRicCliente.FieldByName('Progressivo').AsString;
          ERicRuolo.text := ElencoRicClienteForm.QRicCliente.FieldByName('Ruolo').AsString;
     end else begin
          xIDRicerca := 0;
          ERicRif.Text := '';
          ERicRuolo.text := '';
     end;
     ElencoRicClienteForm.QRicCliente.Close;
     ElencoRicClienteForm.Free;
end;

procedure TNotaSpeseForm.SpeedButton2Click(Sender: TObject);
begin
     if RGTipoSogg.ItemIndex = 0 then begin
          if xIDRIcerca = 0 then begin
               MessageDlg('Nessuna commessa impostata.', mtError, [mbOK], 0);
               exit;
          end else begin
               ElencoDipForm := TElencoDipForm.create(self);
               ElencoDipForm.Panel2.visible := False;
               ElencoDipForm.TAnagDip.SQL.Text := 'select A.ID,A.dacontattocliente,A.Cognome,A.Nome,A.CVNumero, ' +
                    '	TipoStato,A.IDTipoStato,IDProprietaCV,CVIDAnndata,CVInseritoInData ' +
                    'from Anagrafica A ' +
                    'join EBC_TipiStato TS on A.IDTipoStato = TS.ID ' +
                    'where A.ID in ( ' +
                    '	select distinct IDAnagrafica ' +
                    '	from EBC_CandidatiRicerche ' +
                    '	where IDRicerca = ' + IntToStr(xIDRicerca) +
                    ') ' +
                    'order by Cognome,Nome ';
               ElencoDipForm.TAnagDip.Open;
               ElencoDipForm.ShowModal;
               if ElencoDipForm.Modalresult = mrOK then begin
                    xIDSoggetto := ElencoDipForm.TAnagDip.FieldByName('ID').AsInteger;
                    ESoggCogn.Text := ElencoDipForm.TAnagDip.FieldByName('Cognome').AsString;
                    ESoggNome.text := ElencoDipForm.TAnagDip.FieldByName('Nome').AsString;
               end else begin
                    xIDSoggetto := 0;
                    ESoggCogn.Text := '';
                    ESoggNome.text := '';
               end;
               ElencoDipForm.Free;
          end;
     end;
     if RGTipoSogg.ItemIndex = 1 then begin
          ElencoDipForm := TElencoDipForm.create(self);
          ElencoDipForm.ShowModal;
          if ElencoDipForm.Modalresult = mrOK then begin
               xIDSoggetto := ElencoDipForm.TAnagDip.FieldByName('ID').AsInteger;
               ESoggCogn.Text := ElencoDipForm.TAnagDip.FieldByName('Cognome').AsString;
               ESoggNome.text := ElencoDipForm.TAnagDip.FieldByName('Nome').AsString;
          end else begin
               xIDSoggetto := 0;
               ESoggCogn.Text := '';
               ESoggNome.text := '';
          end;
          ElencoDipForm.Free;
     end;
     if RGTipoSogg.ItemIndex = 2 then begin
          ESoggNome.text := '';
          ElencoUtentiForm := TElencoUtentiForm.create(self);
          ElencoUtentiForm.ShowModal;
          if ElencoUtentiForm.Modalresult = mrOK then begin
               xIDSoggetto := ElencoUtentiForm.QUsers.FieldByName('ID').Asinteger;
               ESoggCogn.Text := ElencoUtentiForm.QUsers.FieldByName('Nominativo').AsString;
          end else begin
               xIDSoggetto := 0;
               ESoggCogn.Text := '';
          end;
          ElencoUtentiForm.Free;
     end;
end;

procedure TNotaSpeseForm.SpeedButton3Click(Sender: TObject);
begin
     ElencoFattClienteForm := TElencoFattClienteForm.create(self);

     ElencoFattClienteForm.QFattCliente.SQL.text := 'select * from Fatture  ' +
          'where idcliente = ' + Data2.TEbcClienti.FieldByName('ID').asString;
     //if xIDRicerca > 0 then
     //     ElencoFattClienteForm.QFattCliente.SQL.Add('and IDRicerca=' + IntToStr(xIDRicerca));

     ElencoFattClienteForm.ShowModal;
     if ElencoFattClienteForm.Modalresult = mrOK then begin
          xIDFattura := ElencoFattClienteForm.QFattCliente.FieldByName('ID').AsInteger;
          EFattNum.Text := ElencoFattClienteForm.QFattCliente.FieldByName('Progressivo').asString;
          EFattData.text := ElencoFattClienteForm.QFattCliente.FieldByName('Data').asString;
     end else begin
          xIDFattura := 0;
          EFattNum.Text := '';
          EFattData.text := '';
     end;
     ElencoFattClienteForm.Free;
end;
//[/TONI20020911\]FINE

procedure TNotaSpeseForm.FormCreate(Sender: TObject);
begin
     xIDRicerca := 0;
     xIDSoggetto := 0;
     xIDFattura := 0;
end;

procedure TNotaSpeseForm.FormShow(Sender: TObject);
begin
     Caption := '[M/150] - ' + caption;
end;

procedure TNotaSpeseForm.SpeedButton50Click(Sender: TObject);
begin
     if MessageDlg('Sei sicuro di voler eliminare l''associazione ?', mtWarning,
          [mbNo, mbYes], 0) = mrYes then begin
          EFattNum.text:='';
          EFattData.text:='';
          xIDFattura:=0;
     end;
end;

end.

