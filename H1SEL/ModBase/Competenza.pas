unit Competenza;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Grids, DBGrids, Db, DBTables, StdCtrls, Buttons, ADODB, U_ADOLinkCl,
     TB97, ExtCtrls;

type
     TCompetenzaForm = class(TForm)
          EComp: TEdit;
          Label1: TLabel;
          DsAreeComp: TDataSource;
          DBGrid1: TDBGrid;
          Label2: TLabel;
          Label3: TLabel;
          ETipo: TEdit;
          TAreeComp: TADOLinkedQuery;
          Panel1: TPanel;
          BitBtn1: TToolbarButton97;
          BitBtn2: TToolbarButton97;
          procedure BitBtn1Click(Sender: TObject);
          procedure BitBtn2Click(Sender: TObject);
          procedure FormShow(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     CompetenzaForm: TCompetenzaForm;

implementation

uses ModuloDati2,
     //[/TONI20020726\];
     ModuloDati, Main;
//[/TONI20020726\];FINE

{$R *.DFM}

procedure TCompetenzaForm.BitBtn1Click(Sender: TObject);
begin
     CompetenzaForm.ModalResult := mrOk;
     if trim(EComp.Text) = '' then begin
          MessageDlg('La competenza non pu� avere valore nullo', mtError, [mbOK], 0);
          ModalResult := mrNone;
          Abort;
     end;
end;

procedure TCompetenzaForm.BitBtn2Click(Sender: TObject);
begin
     CompetenzaForm.ModalResult := mrCancel;
end;

procedure TCompetenzaForm.FormShow(Sender: TObject);
begin
     //Grafica
     CompetenzaForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
end;

end.

