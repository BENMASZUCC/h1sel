unit ZoneComuni;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     TB97, Grids, DBGrids, Db, DBTables, StdCtrls, Buttons, ExtCtrls, ADODB,
     U_ADOLinkCl;

type
     TZoneComuniForm = class(TForm)
          DsZone: TDataSource;
          DSComuniZona: TDataSource;
          DBGrid1: TDBGrid;
          DBGrid2: TDBGrid;
          ToolbarButton971: TToolbarButton97;
          Panel73: TPanel;
          Panel1: TPanel;
          TZone: TADOLinkedTable;
          TComuniZona: TADOLinkedTable;
          Panel2: TPanel;
    BitBtn1: TToolbarButton97;
          procedure ToolbarButton971Click(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure BitBtn1Click(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     ZoneComuniForm: TZoneComuniForm;

implementation

uses ModuloDati, Main;

{$R *.DFM}

procedure TZoneComuniForm.ToolbarButton971Click(Sender: TObject);
begin
     if MessageDlg('Sei sicuro di voler eliminare l''associazione ?', mtWarning,
          [mbNo, mbYes], 0) = mrYes then begin
          TComuniZona.Edit;
          TComuniZona.FieldByName('IDZona').asString := '';
          TComuniZona.Post;
          // modifica Anagrafica per quel comune (sia residenza che domicilio)
          Data.QTemp.Close;
          Data.QTemp.SQL.Clear;
          Data.QTemp.SQL.Add('update Anagrafica set IDZonaDom=null');
          Data.QTemp.SQL.Add('where IDComuneDom=' + TComuniZona.FieldByName('ID').asString);
          Data.QTemp.ExecSQL;
          Data.QTemp.SQL.Clear;
          Data.QTemp.SQL.Add('update Anagrafica set IDZonaRes=null');
          Data.QTemp.SQL.Add('where IDComuneRes=' + TComuniZona.FieldByName('ID').asString);
          Data.QTemp.ExecSQL;
     end;
end;

procedure TZoneComuniForm.FormShow(Sender: TObject);
begin
     //Grafica
     ZoneComuniForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;    
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel73.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel73.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel73.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     Caption := '[S/45] - ' + Caption;
end;

procedure TZoneComuniForm.BitBtn1Click(Sender: TObject);
begin
     ZoneComuniForm.ModalResult := mrOk;
end;

end.

