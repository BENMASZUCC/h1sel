unit SelCliente;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ExtCtrls, Grids, DBGrids, Db, DBTables, StdCtrls, Buttons, ADODB,
     U_ADOLinkCl, dxTL, dxDBCtrl, dxDBGrid, dxCntner, TB97;

type
     TSelClienteForm = class(TForm)
          Panel2: TPanel;
          DsClienti: TDataSource;
          Panel1: TPanel;
          EDesc: TEdit;
          Label1: TLabel;
          RGFiltro: TRadioGroup;
          CBTestoCon: TCheckBox;
          TClienti: TADOLinkedQuery;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1Descrizione: TdxDBGridMaskColumn;
          dxDBGrid1Comune: TdxDBGridMaskColumn;
          dxDBGrid1Provincia: TdxDBGridMaskColumn;
          dxDBGrid1TipoAzienda: TdxDBGridMaskColumn;
          TClientiID: TAutoIncField;
          TClientiDescrizione: TStringField;
          TClientiStato: TStringField;
          TClientiComune: TStringField;
          TClientiProvincia: TStringField;
          TClientiIDTipoAzienda: TIntegerField;
          TClientiTipoAzienda: TStringField;
          TClientiIDAttivita: TIntegerField;
          dxDBGrid1Telefono: TdxDBGridColumn;
          TClientiTelefono: TStringField;
          Panel3: TPanel;
          BOk: TToolbarButton97;
          BAnnulla: TToolbarButton97;
          BitBtn3: TToolbarButton97;
    SpeedButton1: TToolbarButton97;
          procedure DBGrid1DblClick(Sender: TObject);
          procedure EDescChange(Sender: TObject);
          procedure RGFiltroClick(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure BitBtn3Click(Sender: TObject);
          procedure CBTestoConClick(Sender: TObject);
          procedure SpeedButton1Click(Sender: TObject);
          procedure BOkClick(Sender: TObject);
          procedure BAnnullaClick(Sender: TObject);
     private
          xstringa: string;
     public
          { Public declarations }
     end;

var
     SelClienteForm: TSelClienteForm;

implementation

uses InsCliente, ModuloDati, Main, uUtilsVarie;

{$R *.DFM}

procedure TSelClienteForm.DBGrid1DblClick(Sender: TObject);
begin
     BOkClick(nil);
end;

procedure TSelClienteForm.EDescChange(Sender: TObject);
var xstato: string;
begin
     TClienti.Close;
     {TClienti.SQL.text := 'select EBC_Clienti.ID,Descrizione,Stato,Comune,Provincia,IDTipoAzienda,TipoAzienda,IDAttivita,Telefono ' +
          'from EBC_Clienti, EBC_TipiAziende ' +
          'where EBC_Clienti.IDTipoAzienda *= EBC_TipiAziende.ID ' +
          'and Descrizione like :xDesc: ';}
     //modifica query da Thomas
     TClienti.SQL.text := 'select EBC_Clienti.ID,Descrizione,Stato,Comune,Provincia,IDTipoAzienda,TipoAzienda,IDAttivita,Telefono ' +
          ' from EBC_Clienti left join EBC_TipiAziende ' +
          ' on EBC_Clienti.IDTipoAzienda = EBC_TipiAziende.ID ' +
          ' where Descrizione like :xDesc: ';
     if (Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 6)) <> 'ADVANT') and (pos('MCS', UpperCase(Data.Global.FieldByName('NomeAzienda').Value)) = 0) then begin
          if RGFiltro.ItemIndex > 0 then TClienti.SQL.Add('and stato=:xStato:');
          case RGFiltro.Itemindex of
               1: xStato := 'attivo';
               2: xStato := 'non attivo';
               3: xStato := 'contattato';
               4: xStato := 'eliminato';
          end;
     end;
     TClienti.SQL.Add('order by Descrizione');
     if RGFiltro.ItemIndex > 0 then TClienti.ParamByName['xStato'] := xStato;
     if CBTestoCon.Checked then
          TClienti.ParamByName['xDesc'] := '%' + EDesc.text + '%'
     else TClienti.ParamByName['xDesc'] := EDesc.text + '%';
     TClienti.Open;
end;

procedure TSelClienteForm.RGFiltroClick(Sender: TObject);
begin
     EDescChange(self);
end;

procedure TSelClienteForm.FormShow(Sender: TObject);
begin
     Caption := '[S/3] - ' + Caption;
     SelClienteForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel3.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color; 
     Panel3.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel3.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
end;

procedure TSelClienteForm.BitBtn3Click(Sender: TObject);
var xDenom: string;
begin
     //if not MainForm.CheckProfile('100') then Exit;


     InsClienteForm := TInsClienteForm.create(self);
     InsClienteForm.DEDataCon.date := Date;
     InsClienteForm.CBStato.text := 'attivo';
     InsClienteForm.ShowModal;
     if InsClienteForm.ModalResult = mrOK then begin
          with Data do begin
               //  DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text := 'insert into EBC_Clienti (Descrizione,Stato,IDAttivita,ConosciutoInData,IDTipoAzienda,Cap,Comune,Provincia,Telefono) ' +
                         'values (:xDescrizione:,:xStato:,:xIDAttivita:,:xConosciutoInData:,:xIDTipoAzienda:,:xCap:,:xComune:,:xProvincia:,:xTelefono:)';
                    Q1.ParamByName['xDescrizione'] := TrimRight(InsClienteForm.EDenominazione.text);
                    Q1.ParamByName['xStato'] := InsClienteForm.CBStato.text;
                    Q1.ParamByName['xIDAttivita'] := InsClienteForm.xIDAttivita;
                    Q1.ParamByName['xConosciutoInData'] := InsClienteForm.DEDataCon.Date;
                    Q1.ParamByName['xIDTipoAzienda'] := InsClienteForm.QTipiAziendaID.Value;
                    Q1.ParamByName['xCap'] := InsClienteForm.ECap.Text;
                    Q1.ParamByName['xComune'] := InsClienteForm.EComune.Text;
                    Q1.ParamByName['xProvincia'] := InsClienteForm.EProv.Text;
                    Q1.ParamByName['xTelefono'] := InsClienteForm.ETelefono.Text;
                    Q1.ExecSQL;
                    //  DB.CommitTrans;
                    TClienti.Close;
                    Tclienti.Open;
               except
                    //DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               end;
          end;
     end;
     // posizionati su questo nuovo cliente
     EDesc.text := InsClienteForm.EDenominazione.text;
     InsClienteForm.Free;
     EDescChange(self);

end;

procedure TSelClienteForm.CBTestoConClick(Sender: TObject);
begin
     EDescChange(self);
end;

procedure TSelClienteForm.SpeedButton1Click(Sender: TObject);
begin
     ApriHelp('041');
end;

procedure TSelClienteForm.BOkClick(Sender: TObject);
begin
     SelClienteForm.ModalResult := mrOk;
end;

procedure TSelClienteForm.BAnnullaClick(Sender: TObject);
begin
     SelClienteForm.ModalResult := mrCancel;
end;

end.

