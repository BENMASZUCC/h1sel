unit FissaColloquio;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, Grids, DBGrids, Mask, ComCtrls, Db, DBTables, DtEdit97,
     ADODB, U_ADOLinkCl, dxCntner, dxEditor, dxExEdtr, dxEdLib, TB97,
     ExtCtrls;

type
     TFissaColloquioForm = class(TForm)
          GroupBox1: TGroupBox;
          Label4: TLabel;
          Label5: TLabel;
          Ora: TMaskEdit;
          DBGrid1: TDBGrid;
          DsSelez: TDataSource;
          DEData: TDateEdit97;
          CBInterno: TCheckBox;
          DsRisorse: TDataSource;
          DBGrid2: TDBGrid;
          Label1: TLabel;
          MEAlleOre: TMaskEdit;
          CBLuogo: TCheckBox;
          TSelezionatori: TADOLinkedTable;
          TRisorse: TADOLinkedTable;
          dxTimeEdit1: TdxTimeEdit;
          Panel1: TPanel;
          BitBtn1: TToolbarButton97;
          BitBtn3: TToolbarButton97;
          BitBtn2: TToolbarButton97;
          procedure BitBtn2Click(Sender: TObject);
          procedure CBInternoClick(Sender: TObject);
          procedure BitBtn1Click(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure FormCreate(Sender: TObject);
          procedure BitBtn3Click(Sender: TObject);
     private
          { Private declarations }
     public
     end;

var
     FissaColloquioForm: TFissaColloquioForm;

implementation

uses AgendaSett, ModuloDati, ModuloDati2, Main, uUtilsVarie;

{$R *.DFM}

procedure TFissaColloquioForm.BitBtn2Click(Sender: TObject);
var xMin, xHour: integer;
begin
     // VERIFICA LICENZA
     if MainForm.VerificaFunzLicenze('101') =true then begin
          MessageDlg('FUNZIONE O MODULO ' + GetFunctionName('101') + ' NON COMPRESA NELLA LICENZA' + #13 + 'Consulta l''About per maggiori informazioni', mtError, [mbOK], 0);
          Exit;
     end;

     // DISABILITAZIONE PROPOSTE Torino -- ALIAS='PROPOS' - per ora abilitata
     //if Uppercase(copy(Data.GlobalNomeAzienda.Value,1,6))='PROPOS' then begin
     //     ShowMessage('FUNZIONE NON ABILITATA');
     //     Exit;
     //end;
     AgendaSettForm := TAgendaSettForm.create(self);
     //[/TONI20020729\]
     //     AgendaSettForm.xIDSel:=TSelezionatoriID.Value;
     AgendaSettForm.xIDSel := TSelezionatori.FieldByName('ID').Value;
     //[/TONI20020729\]FINE
     AgendaSettForm.Panel3.Visible := False;
     AgendaSettForm.Panel4.Visible := False;
     AgendaSettForm.TSCandidati.TabVisible := False;
     AgendaSettForm.ASG1.PopupMenu := nil;
     AgendaSettForm.ShowModal;
     if AgendaSettForm.PageControl1.ActivePage = AgendaSettForm.TSAgenda then begin
          DEData.Date := AgendaSettForm.xPrimaData + AgendaSettForm.xSelCol - 1;
          Ora.Text := IntToStr(AgendaSettForm.xOre[AgendaSettForm.xSelRow]) + '.00';
          MEAlleOre.Text := IntToStr(AgendaSettForm.xOre[AgendaSettForm.xSelRow] + 1) + '.00';
     end else begin
          DEData.Date := AgendaSettForm.xPrimaData + AgendaSettForm.xSelColRis - 1;
          Ora.Text := IntToStr(AgendaSettForm.xOre[AgendaSettForm.xSelRowRis]) + '.00';
          MEAlleOre.Text := IntToStr(AgendaSettForm.xOre[AgendaSettForm.xSelRowRis] + 1) + '.00';
          //[/TONI20020729\]
          //          TRisorse.FindKey([AgendaSettForm.TRisorseID.Value]);
          TRisorse.FindKeyADO(VarArrayOf([AgendaSettForm.TRisorse.FieldByName('ID').AsInteger]));
          //[/TONI20020729\]FINE
     end;
     AgendaSettForm.Free;
end;

procedure TFissaColloquioForm.CBInternoClick(Sender: TObject);
begin
     DBGrid2.Visible := CBInterno.Checked;
end;

procedure TFissaColloquioForm.BitBtn1Click(Sender: TObject);
var xHour, xMin, xSec, xMSec, xHour2, xMin2, xSec2, xMSec2: Word;
begin
     ModalResult := mrOk;
     // controllo orari
     if MEAlleOre.Text <> '  .  ' then begin
          if StrToDateTime(DatetoStr(DEData.Date) + ' ' + Ora.Text) >
               StrToDateTime(DatetoStr(DEData.Date) + ' ' + MEAlleOre.Text) then begin
               ModalResult := mrNone;
               ShowMessage('Orari non corretti');
               Abort;
          end;
     end else MEAlleOre.Text := '00.00';
     if Ora.Text = '  .  ' then Ora.Text := '00.00';
     // controllo concomitanza (nello stesso luogo)
     if not CBLuogo.Checked then begin
          // with Data2 do begin
          Data2.QCheck.Close;
          Data2.QCheck.SQL.Text := 'select Descrizione,Ore,AlleOre from Agenda ' +
               'where (Ore >:xDalle: and Ore <:xalle: and IDRisorsa=:xIDRis:) ' +
               'or (AlleOre >:xDalle: and AlleOre <:xalle:  and IDRisorsa=:xIDRis:) ' +
               'or (Ore <= :xDalle: and AlleOre >= :xalle: and IDRisorsa=:xIDRis:) ' +
               'or (Ore >= :xDalle: and AlleOre <= :xalle: and IDRisorsa=:xIDRis:) ';
          Data2.QCheck.ParamByName['xDalle'] := StrToDateTime(DatetoStr(FissaColloquioForm.DEData.Date) + ' ' + FissaColloquioForm.Ora.Text);
          Data2.QCheck.ParamByName['xAlle'] := StrToDateTime(DatetoStr(FissaColloquioForm.DEData.Date) + ' ' + FissaColloquioForm.MEAlleOre.Text);
          Data2.QCheck.ParamByName['xIDRis'] := FissaColloquioForm.TRisorse.FieldByName('ID').AsInteger;
          Data2.QCheck.Open;
          //showmessage(inttostr(Data2.QCheck.RecordCount));
          if not Data2.QCheck.IsEmpty then begin
               ModalResult := mrNone;
               DecodeTime(Data2.QCheck.FieldByName('Ore').asDateTime, xHour, xMin, xSec, xMSec);
               DecodeTime(Data2.QCheck.FieldByName('AlleOre').asDateTime, xHour2, xMin2, xSec2, xMSec2);
               MessageDlg('Risulta gi� presente il seguente impegno (nello stesso luogo):' + chr(13) +
                    Data2.QCheck.FieldByName('Descrizione').asString + chr(13) +
                    'dalle ' + IntToStr(xHour) + '.' + IntToStr(xMin) +
                    '  alle ' + IntToStr(xHour2) + '.' + IntToStr(xMin2) + chr(13) +
                    'Luogo: ' + TRisorse.FieldByName('Risorsa').AsString, mtError, [mbOK], 0);
               Abort;
          end; //else
          // showmessage('qcheck piena');
          // end;
     end;
end;

procedure TFissaColloquioForm.FormShow(Sender: TObject);
begin
     Caption := '[E/5] - ' + Caption;

     //Grafica
     FissaColloquioForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     if pos('EMERGENCY', Uppercase(Data.Global.fieldbyname('NomeAzienda').AsString)) > 0 then
          CBLuogo.Checked := False;

end;

procedure TFissaColloquioForm.FormCreate(Sender: TObject);
begin
     // DoubleBuffered := True;
     TSelezionatori.close;
     TSelezionatori.Open;
     TRisorse.close;
     TRisorse.Open;
end;

procedure TFissaColloquioForm.BitBtn3Click(Sender: TObject);
begin
     ModalResult := mrCancel;
end;

end.

