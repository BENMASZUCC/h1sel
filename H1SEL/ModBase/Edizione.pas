unit Edizione;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, Grids, DBGrids, Db, DBTables, ADODB, U_ADOLinkCl,
  TB97, ExtCtrls;

type
     TEdizioneForm = class(TForm)
          Label1: TLabel;
          ENomeEdizione: TEdit;
          Label5: TLabel;
          ENote: TEdit;
          DsQConcess: TDataSource;
          DBGrid1: TDBGrid;
          QConcess: TADOLinkedQuery;
    Panel1: TPanel;
    BitBtn1: TToolbarButton97;
    BitBtn2: TToolbarButton97;
    Label2: TLabel;
    CBVisibileForm: TCheckBox;
          procedure FormShow(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     EdizioneForm: TEdizioneForm;

implementation
//[/TONI20020726\]
uses ModuloDati, Main;
//[/TONI20020726\]FINE
{$R *.DFM}

procedure TEdizioneForm.FormShow(Sender: TObject);
begin
     //Grafica
     EdizioneForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     if QConcess.Active then QConcess.Close;
     QConcess.Open;
end;

procedure TEdizioneForm.BitBtn1Click(Sender: TObject);
begin
      EdizioneForm.ModalResult:=mrOk;
end;

procedure TEdizioneForm.BitBtn2Click(Sender: TObject);
begin
      EdizioneForm.ModalResult:=mrOk;
end;

end.

