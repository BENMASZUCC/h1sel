unit SelRuolo;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, Grids, DBGrids, Db, DBTables, ExtCtrls, DBCtrls,
     ADODB, U_ADOLinkCl;

type
     TSelRuoloForm = class(TForm)
          DataSource1: TDataSource;
          DBGrid1: TDBGrid;
          BitBtn1: TBitBtn;
          BitBtn2: TBitBtn;
          DBNavigator1: TDBNavigator;
          Ruoli: TADOLinkedTable;
          Aree: TADOLinkedTable;
          procedure DBGrid1KeyPress(Sender: TObject; var Key: Char);
          procedure FormShow(Sender: TObject);
          procedure FormCreate(Sender: TObject);
     private
          { Private declarations }
          xStringa: string;
     public
          { Public declarations }
          xAbilitato: boolean;
     end;

var
     SelRuoloForm: TSelRuoloForm;

implementation
uses
     ModuloDatI;
{$R *.DFM}

procedure TSelRuoloForm.DBGrid1KeyPress(Sender: TObject; var Key: Char);
begin
     if xAbilitato then begin
          if Key = chr(13) then begin
               BitBtn1.Click;
          end else begin
               xStringa := xStringa + key;
               //        Ruoli.FindNearest([xStringa]);
               Ruoli.FindNearestADO(xStringa);
          end;
     end;
end;

procedure TSelRuoloForm.FormShow(Sender: TObject);
begin
     xStringa := '';
end;

procedure TSelRuoloForm.FormCreate(Sender: TObject);
begin
     xAbilitato := True;
end;

end.
