{ QuickReport list template }

unit RepElencoSel;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Qrctrls, quickrpt, DB, DBTables, ExtCtrls;

type
     TQRElencoSel = class(TQuickRep)
          DetailBand1: TQRBand;
          PageFooterBand1: TQRBand;
          TitleBand1: TQRBand;
          ColumnHeaderBand1: TQRBand;
          QRLabel3: TQRLabel;
          QRSysData1: TQRSysData;
          QRLabel4: TQRLabel;
          QRDBText1: TQRDBText;
          QRDBText2: TQRDBText;
          QRDBText3: TQRDBText;
          QRDBText4: TQRDBText;
          QRLabel1: TQRLabel;
          QRLabel2: TQRLabel;
          QRLabel5: TQRLabel;
          QRLabel6: TQRLabel;
          QRShape1: TQRShape;
          QRShape2: TQRShape;
          QRMemo1: TQRMemo;
          QRDBText5: TQRDBText;
          QRLabel7: TQRLabel;
          QRDBText6: TQRDBText;
          QRLabel8: TQRLabel;
          QRDBText7: TQRDBText;
          QRDBText8: TQRDBText;
          procedure QRLabel8Print(sender: TObject; var Value: string);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     QRElencoSel: TQRElencoSel;

implementation

uses SelPers, SelPersNew, MDRicerche;

{$R *.DFM}


procedure TQRElencoSel.QRLabel8Print(sender: TObject; var Value: string);
begin
     if QRElencoSel.QRDBText1.DataSet = DataRicerche.QCandRic then
          value := '';
end;

end.
