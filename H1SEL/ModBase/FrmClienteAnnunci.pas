unit FrmClienteAnnunci;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     TB97, ExtCtrls, Grids, DBGrids, RXDBCtrl, Db, DBTables, ADODB, U_ADOLinkCl,
     Buttons, dxTL, dxDBCtrl, dxDBGrid, dxCntner;

type
     TClienteAnnunciFrame = class(TFrame)
          PanTitoloCompensi: TPanel;
          Splitter1: TSplitter;
          Panel2: TPanel;
          ToolbarButton972: TToolbarButton97;
          DsQAnnunciCli: TDataSource;
          DsQListinoCliente: TDataSource;
          ToolbarButton971: TToolbarButton97;
          PanListini: TPanel;
          Panel1: TPanel;
          Panel3: TPanel;
          ToolbarButton974: TToolbarButton97;
          QAnnunciCli: TADOLinkedQuery;
          QListinoCliente: TADOLinkedQuery;
          QListinoClienteNomeEdizione: TStringField;
          QListinoClienteTestata: TStringField;
          QListinoClienteID: TAutoIncField;
          QListinoClienteIDEdizione: TIntegerField;
          QListinoClienteDallaData: TDateTimeField;
          QListinoClientePrezzoListinoLire: TFloatField;
          QListinoClientePrezzoANoiLire: TFloatField;
          QListinoClientePrezzoAlClienteLire: TFloatField;
          QListinoClientePrezzoListinoEuro: TFloatField;
          QListinoClientePrezzoANoiEuro: TFloatField;
          QListinoClientePrezzoAlClienteEuro: TFloatField;
          QListinoClienteNote: TStringField;
          QListinoClienteQtaDa: TSmallintField;
          QListinoClienteQtaA: TSmallintField;
          QListinoClienteIDCliente: TIntegerField;
          QListinoClienteIDEdizListino: TIntegerField;
          QListinoClienteIDCliente_1: TIntegerField;
          QListinoClientePrezzoQuestoClienteLire: TFloatField;
          QListinoClientePrezzoQuestoClienteEuro: TFloatField;
          QListinoClienteNote_1: TStringField;
          QListinoClienteTestataEdizione: TStringField;
          SpeedButton1: TSpeedButton;   
          RxDBGrid1: TdxDBGrid;
          RxDBGrid2: TdxDBGrid;
          dxDBGrid1Testata: TdxDBGridColumn;
          dxDBGrid1NomeEdizione: TdxDBGridColumn;
          dxDBGrid1Data: TdxDBGridColumn;
          dxDBGrid1NumModuli: TdxDBGridColumn;
          dxDBGrid1Nominativo: TdxDBGridColumn;
          dxDBGrid2TestataEdizione: TdxDBGridColumn;
          dxDBGrid2DallaData: TdxDBGridColumn;
          dxDBGrid2QtaDa: TdxDBGridColumn;
          dxDBGrid2QtaA: TdxDBGridColumn;
          dxDBGrid2PrezzoListinoEuro: TdxDBGridColumn;
          dxDBGrid2PrezzoANoiEuro: TdxDBGridColumn;
          dxDBGrid2PrezzoQuestoClienteEuro: TdxDBGridColumn;
          procedure ToolbarButton972Click(Sender: TObject);
          procedure QListinoCliente_OLDCalcFields(DataSet: TDataSet);
          procedure ToolbarButton974Click(Sender: TObject);
          procedure SpeedButton1Click(Sender: TObject);
     private
          { Private declarations }
     public
          xIDCliente: integer;
          xCliente: string;
     end;

implementation

uses StoricoPrezziAnnunciCli, PrezzoListino, ModuloDati, SelTestataEdizione,
     ModPrezzo, uUtilsVarie;

{$R *.DFM}

procedure TClienteAnnunciFrame.ToolbarButton972Click(Sender: TObject);
begin
     StoricoPrezziAnnunciCliForm := TStoricoPrezziAnnunciCliForm.create(self);
     StoricoPrezziAnnunciCliForm.xIDCliente := xIDCliente;
     StoricoPrezziAnnunciCliForm.ECliente.text := xCliente;
     StoricoPrezziAnnunciCliForm.ShowModal;
     StoricoPrezziAnnunciCliForm.Free;
end;

procedure TClienteAnnunciFrame.QListinoCliente_OLDCalcFields(DataSet: TDataSet);
begin
     QListinoCliente.FieldByName('TestataEdizione').value := QListinoCliente.FieldByName('Testata').value + ' ' + QListinoCliente.FieldByName('NomeEdizione').value;
end;

procedure TClienteAnnunciFrame.ToolbarButton974Click(Sender: TObject);
var xIDClienteListino, xID: integer;
begin
     if QListinoCliente.RecordCount = 0 then exit;
     ModPrezzoForm := TModPrezzoForm.create(self);
     if QListinoCliente.FieldByName('PrezzoQuestoClienteLire').asString <> '' then begin
          ModPrezzoForm.PrezzoLire.value := QListinoCliente.FieldByName('PrezzoQuestoClienteLire').value;
          ModPrezzoForm.PrezzoEuro.value := QListinoCliente.FieldByName('PrezzoQuestoClienteEuro').value;
     end;
     ModPrezzoForm.ShowModal;
     if ModPrezzoForm.ModalResult = mrOK then begin
          if QListinoCliente.FieldByName('IDCliente').asString = '' then begin
               with Data do begin
                    DB.BeginTrans;
                    try
                         Q1.Close;
                         Q1.SQL.text := 'insert into Ann_ListinoClienti (IDEdizListino,IDCliente,PrezzoQuestoClienteLire,PrezzoQuestoClienteEuro) ' +
                              ' values (:xIDEdizListino:,:xIDCliente:,:xPrezzoQuestoClienteLire:,:xPrezzoQuestoClienteEuro:)';
                         Q1.ParamByName['xIDEdizListino'] := QListinoCliente.FieldByName('ID').Value;
                         Q1.ParamByName['xIDCliente'] := xIDCliente;
                         Q1.ParamByName['xPrezzoQuestoClienteLire'] := INTEGER(round(ModPrezzoForm.PrezzoLire.value));
                         Q1.ParamByName['xPrezzoQuestoClienteEuro'] := ModPrezzoForm.PrezzoEuro.value;
                         Q1.ExecSQL;
                         DB.CommitTrans;
                         QListinoCliente.Close;
                         QListinoCliente.Open;
                    except
                         DB.RollbackTrans;
                         MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                    end;
               end;
          end else begin
               with Data do begin
                    DB.BeginTrans;
                    try
                         Q1.Close;
                         Q1.SQL.text := 'update Ann_ListinoClienti set PrezzoQuestoClienteLire=:xPrezzoQuestoClienteLire:, ' +
                              ' PrezzoQuestoClienteEuro=:xPrezzoQuestoClienteEuro: ' +
                              ' where ID=' + QListinoCliente.FieldByName('ID').asString;
                         Q1.ParamByName['xPrezzoQuestoClienteLire'] := INTEGER(round(ModPrezzoForm.PrezzoLire.value));
                         Q1.ParamByName['xPrezzoQuestoClienteEuro'] := ModPrezzoForm.PrezzoEuro.value;
                         Q1.ExecSQL;
                         DB.CommitTrans;
                         xID := QListinoClienteID.Value;
                         QListinoCliente.Close;
                         QListinoCliente.Open;
                         QListinoCliente.Locate('ID', xID, []);
                    except
                         DB.RollbackTrans;
                         MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                    end;
               end;
          end;
     end;
     ModPrezzoForm.Free;

     // record da inserire

  // VECCHIO CODICE
  {if QListinoCliente.RecordCount=0 then exit;
  PrezzoListinoForm:=TPrezzoListinoForm.create(self);
  PrezzoListinoForm.DEDallaData.Date:=QListinoClienteDallaData.Value;
  PrezzoListinoForm.ListinoLire.Value:=QListinoClientePrezzoListinoLire.Value;
  PrezzoListinoForm.AnoiLire.Value:=QListinoClientePrezzoANoiLire.Value;
  PrezzoListinoForm.AlClienteLire.Value:=QListinoClientePrezzoAlClienteLire.Value;
  PrezzoListinoForm.ListinoEuro.Value:=QListinoClientePrezzoListinoEuro.value;
  PrezzoListinoForm.AnoiEuro.Value:=QListinoClientePrezzoANoiEuro.Value;
  PrezzoListinoForm.AlClienteEuro.Value:=QListinoClientePrezzoAlClienteEuro.Value;
  PrezzoListinoForm.ENote.text:=QListinoClienteNote.Value;
  PrezzoListinoForm.RxSpinEdit1.value:=QListinoClienteQtaDa.Value;
  PrezzoListinoForm.RxSpinEdit2.value:=QListinoClienteQtaA.Value;
  xIDClienteListino:=xIDCliente;
  PrezzoListinoForm.ShowModal;
  if PrezzoListinoForm.ModalResult=mrOK then begin
       with Data do begin
            DB.BeginTrans;
            try
                 Q1.Close;
                 Q1.SQL.text:='update Ann_EdizListino set DallaData=:xDallaData,PrezzoListinoLire=:xPrezzoListinoLire,PrezzoANoiLire=:xPrezzoANoiLire,'+
                      'PrezzoAlClienteLire=:xPrezzoAlClienteLire,PrezzoListinoEuro=:xPrezzoListinoEuro, '+
                      'PrezzoANoiEuro=:xPrezzoANoiEuro,PrezzoAlClienteEuro=:xPrezzoAlClienteEuro,Note=:xNote,QtaDa=:XQtaDa,QtaA=:xQtaA,IDCliente=:xIDCliente '+
                      'where ID='+QListinoClienteID.asString;
                 Q1.ParamByName('xDallaData').asDateTime:=PrezzoListinoForm.DEDallaData.Date;
                 Q1.ParamByName('xPrezzoListinoLire').AsFloat:=PrezzoListinoForm.ListinoLire.Value;
                 Q1.ParamByName('xPrezzoANoiLire').AsFloat:=PrezzoListinoForm.AnoiLire.Value;
                 Q1.ParamByName('xPrezzoAlClienteLire').AsFloat:=PrezzoListinoForm.AlClienteLire.Value;
                 Q1.ParamByName('xPrezzoListinoEuro').AsFloat:=PrezzoListinoForm.ListinoEuro.Value;
                 Q1.ParamByName('xPrezzoANoiEuro').AsFloat:=PrezzoListinoForm.AnoiEuro.Value;
                 Q1.ParamByName('xPrezzoAlClienteEuro').AsFloat:=PrezzoListinoForm.AlClienteEuro.Value;
                 Q1.ParamByName('xNote').asString:=PrezzoListinoForm.ENote.text;
                 Q1.ParamByName('xQtaDa').asInteger:=Round(PrezzoListinoForm.RxSpinEdit1.value);
                 Q1.ParamByName('xQtaA').asInteger:=Round(PrezzoListinoForm.RxSpinEdit2.value);
                 Q1.ParamByName('xIDCliente').asInteger:=xIDClienteListino;
                 Q1.ExecSQL;
                 DB.CommitTrans;
                 QListinoCliente.Close;
                 QListinoCliente.Open;
            except
                 DB.RollbackTrans;
                 MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
            end;
       end;
  end;
  PrezzoListinoForm.free;}
end;

procedure TClienteAnnunciFrame.SpeedButton1Click(Sender: TObject);
begin
     ApriHelp('18');
end;

end.

