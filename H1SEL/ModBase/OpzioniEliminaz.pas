unit OpzioniEliminaz;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, ExtCtrls, Mask, DBCtrls, Buttons;

type
     TOpzioniEliminazForm = class(TForm)
          BitBtn1: TBitBtn;
          Panel1: TPanel;
          DBEdit1: TDBEdit;
          DBEdit2: TDBEdit;
          RGOpzioni: TRadioGroup;
          Edit1: TEdit;
          Edit2: TEdit;
          procedure FormShow(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     OpzioniEliminazForm: TOpzioniEliminazForm;

implementation

uses MDRicerche, Main;

{$R *.DFM}

procedure TOpzioniEliminazForm.FormShow(Sender: TObject);
begin
     Caption := '[S/30] - ' + Caption;
     edit1.text := MainForm.DBECognome.Text;
     edit2.text := MainForm.DBEdit2.Text;
end;

end.
