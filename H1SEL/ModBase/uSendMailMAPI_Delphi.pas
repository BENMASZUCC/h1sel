unit uSendMailMAPI;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, ShellAPI, ModuloDati,
     StdCtrls, Buttons, ExtCtrls, ComObj, IdComponent, IdBaseComponent, IdMessage, FileCtrl, EmailMessage, Main, Splash3,
     Registry, StrUtils;

function SendEMailMAPI(xSubject, xTesto, xDestinatari, xDestBcc, xAllegati: string; xRemote, xDebug, xShowDialog, xHTML: boolean): boolean;
procedure SendMailOutlook(aRecipient, aSubject, aNote, aFile, aBcc: string; Silent, HTML: boolean);
procedure SendMailThroughClient(SubMail, BodyMail, ToMail, BccMail, AttMail: string; HTMLMail: boolean; var j: integer);

implementation

// ------------- funzioni per "tunnel" TS -------------------------------------------------------------

function TS_ShellExecute(hwnd: integer; lpOperation, lpFile, lpParameters, lpDirectory: widestring; nShowCmd: integer): integer; cdecl; external 'H1TS_Server.dll';

function TS_UserTemp(lpbuffer: PWideChar): integer; cdecl; external 'H1TS_Server.dll';

function SendEMailMAPI(xSubject, xTesto, xDestinatari, xDestBcc, xAllegati: string; xRemote, xDebug, xShowDialog, xHTML: boolean): boolean;
var //xMail: TAFQuickMail;
     xTempFilePath, xComando_ws, xParameters_ws, xCartellaTemp, xCartellaTempTS, xs, xFile, xFileName, xBody, xAttachments, xFormato, xDest, xBcc: string;
     res, i, j: integer;
     xErrorMsg: string;
     CopyRes: boolean;
     xLog, xFiles: TStringList;
     xarray: array[1..1024] of WideChar;
     ps: PWideChar;
     reg: TRegistry;
     regpostastring, key, find: string;


begin

     xTempFilePath := data.Global.fieldbyname('DirFileLog').asstring;

     if xRemote then begin

          // --- REMOTO ------------------------------

          // Controllo numero righe di testo
          if Length(xTesto) > 650 then begin

               if MessageDlg('la lunghezza del messaggio eccede la dimensione massima consentita. Modificare (Yes) o annullare (No) ?', mtWarning, [mbYes, mbNo], 0) = mrNo then
                    exit
               else begin
                    EmailMessageForm := TEmailMessageForm.create(nil);
                    EmailMessageForm.Memo1.Lines.text := xTesto;
                    EmailMessageForm.xCheckLines := True;
                    EmailMessageForm.ShowModal;
                    if EmailMessageForm.ModalResult = mrCancel then begin
                         EmailMessageForm.Free;
                         exit;
                    end;
                    for i := 0 to EmailMessageForm.Memo1.Lines.count - 1 do begin
                         xBody := xBody + chr(13) + EmailMessageForm.Memo1.Lines[i];
                    end;
                    EmailMessageForm.Free;
               end;
          end else xBody := xTesto;

          xLog := TStringList.create;

          // porto i file sul client
          xLog.Add('File da copiare: ' + xAllegati); xLog.SaveToFile(xTempFilePath + '\Log_InvioMailRemota.txt');

          ps := @xarray;
          res := TS_UserTemp(ps);

          xCartellaTemp := WideCharToString(ps);
          xCartellaTempTS := '\\tsclient\' + StringReplace(xCartellaTemp, ':', '', []);

          if not DirectoryExists(xCartellaTempTS) then begin
               xLog.Add('Cartella temp remota (client) NON ESISTE: ' + xCartellaTempTS); xLog.SaveToFile(xTempFilePath + '\Log_InvioMailRemota.txt')
          end else
               xLog.Add('Cartella temp remota (client): ' + xCartellaTempTS); xLog.SaveToFile(xTempFilePath + '\Log_InvioMailRemota.txt');


          if (xAllegati = '-') or (xAllegati = '') or (not DirectoryExists(xCartellaTempTS)) then xAttachments := '-' else begin

               Splash3Form := TSplash3Form.create(nil);
               Splash3Form.Show;
               Application.ProcessMessages;

               xS := xAllegati;

               while pos(';', xS) > 0 do begin

                    xFile := copy(xS, 1, pos(';', xS) - 1);

                    if FileExists(xFile) then begin
                         xFileName := ExtractFileName(xFile);
                         CopyRes := CopyFile(pchar(xFile), pchar(xCartellaTempTS + xFileName), false);
                         if CopyRes then begin
                              xAttachments := xAttachments + xCartellaTemp + xFileName + ';';

                              MainForm.xFileTempClient.Add(xCartellaTempTS + xFileName);

                              xLog.Add('Copia file sul client: ' + xFile + ' > ' + xCartellaTempTS + xFileName + ' > OK'); xLog.SaveToFile(xTempFilePath + '\Log_InvioMailRemota.txt');
                         end else begin
                              xLog.Add('Copia file sul client: ' + xFile + ' > ' + xCartellaTempTS + xFileName + ' > ERRORE'); xLog.SaveToFile(xTempFilePath + '\Log_InvioMailRemota.txt');
                         end;
                    end else
                         xLog.Add('Copia file sul client: ' + xFile + ' NON TROVATO'); xLog.SaveToFile(xTempFilePath + '\Log_InvioMailRemota.txt');

                    xS := copy(xS, pos(';', xS) + 1, length(xS));

                    Application.ProcessMessages;
               end;

               Splash3Form.Hide;
               Splash3Form.Free;
          end;

          xComando_ws := 'c:\temp\InvioMailShell.exe'; // #### <<<<<<<<<<<<<<<  ############################

          if xHTML then xFormato := '1' else xFormato := '0';
          if xDestinatari = '' then xDest := '(inserire)' else xDest := xDestinatari;
          if xDestBcc = '' then xBcc := '-' else xBcc := xDestBcc;

          if xDebug then
               xParameters_ws := '"' + xSubject + '" "' + xBody + '" "' + xDest + '" "' + xBcc + '" "' + xAttachments + '" "' + xFormato + '" "1"'
          else
               xParameters_ws := '"' + xSubject + '" "' + xBody + '" "' + xDest + '" "' + xBcc + '" "' + xAttachments + '" "' + xFormato + '" "0"';

          try
               xLog.Add('Pronto alla chiamata: ' + chr(13) + '  Comando = ' + xComando_ws + chr(13) + '  Parametri: ' + xParameters_ws + chr(13)); xLog.SaveToFile(xTempFilePath + '\Log_InvioMailRemota.txt');

               res := TS_ShellExecute(0, 'Open', xComando_ws, xParameters_ws, '', SW_SHOW);

          finally
               // cancellazione file sul client
               // >>> NON FUNZIONA, perch� lo cancella prima che il client predisponga il messaggio
               //  i file vengono cancellati all'uscita dal programma (alla fine di MainForm.OnClose)

          end;

          case res of
               0: xErrorMsg := '0 = The operating system is out of memory or resources.';
               1: xErrorMsg := '1 = Non riesco ad aprire il virtual channel (TS_ShellExecute)';
               //2: xErrorMsg := '2 = The specified file was not found';
               2: xErrorMsg := '2 = il virtual channel si chiude prima di inviare i dati (TS_ShellExecute)';
               //3: xErrorMsg := '3 = The specified path was not found.';
               3: xErrorMsg := '3 = fallito l''invio dei dati al client (TS_ShellExecute)';

               5: xErrorMsg := '5 = Windows 95 only: The operating system denied access to the specified file';
               8: xErrorMsg := '8 = Windows 95 only: There was not enough memory to complete the operation.';
               10: xErrorMsg := '10 = Wrong Windows version';
               11: xErrorMsg := '11 = The.EXE file is invalid(non - Win32.EXE or error in .EXE image).';
               12: xErrorMsg := '12 = Application was designed for a different operating system.';
               13: xErrorMsg := '13 = Application was designed for MS - DOS 4.0';
               15: xErrorMsg := '15 = Attempt to load a real - mode program';
               16: xErrorMsg := '16 = Attempt to load a second instance of an application with non - readonly data segments';
               19: xErrorMsg := '19 = Attempt to load a compressed application file';
               20: xErrorMsg := '20 = Dynamic - link library(DLL)file failure".';
               26: xErrorMsg := '26 = A sharing violation occurred.';
               27: xErrorMsg := '27 = The filename association is incomplete or invalid.';
               28: xErrorMsg := '28 = The DDE transaction could not be completed because the request timed out.';
               29: xErrorMsg := '29 = The DDE transaction failed.';
               30: xErrorMsg := '30 = The DDE transaction could not be completed because other DDE transactions were being processed.';
               31: xErrorMsg := '31 = There is no application associated with the given filename extension.';
               32: xErrorMsg := '32 = Windows 95 only: The specified dynamic - link library was not found.';
          else xErrorMsg := IntToStr(res) + ' = (errore non documentato)';
          end;
          if (res < 0) or (res > 32) then
               xErrorMsg := 'OK (' + inttostr(res) + ')';

             // CODICI ERRORE COMUNICATI DA STEFANO ma NON gestiti:
             // 1: non riesce ad aprire il virtual channel
             // 2: il virtual channel si chiude prima di inviare i dati
             // 3: fallisce l'invio dei dai al client
             // 0: la ShellExecute fallisce (quindi la comunicazione � corretta ma c'� qualcosa che non va nel passaggio dei parametri)

          xLog.Add('Chiamata effettuata con risultato: ' + xErrorMsg); xLog.SaveToFile(xTempFilePath + '\Log_InvioMailRemota.txt');

          if copy(xErrorMsg, 1, 2) = 'OK' then
               MessageDlg('Operazione completata', mtInformation, [mbOK], 0)
          else MessageDlg('ERRORE: ' + xErrorMsg, mtError, [mbOK], 0);       

     end else begin
          // --- LOCALE ------------------------------

             //!!.. Controllo registro Qual'� la client posta Predefinta. ..!!
          reg := TRegistry.Create;
          reg.RootKey := HKEY_CURRENT_USER;
          key := 'Software\Clients\Mail';
          find := '';
          Reg.OpenKeyReadOnly(key);
          regpostastring := reg.ReadString(find);
          reg.CloseKey;
          reg.Free;
     //!!.. Serve dopo nella sendmailthrough ..!!
          j := 0;
     //showmessage('Contenuto del registro - ' + regpostastring);

          // --- OUTLOOK ---------------------------------------
          if regpostastring = 'Microsoft Outlook' then
               SendMailOutlook(xDestinatari, xSubject, xTesto, xAllegati, xDestBcc, False, xHTML)
          else if regpostastring = '' then
          try
               SendMailOutlook(xDestinatari, xSubject, xTesto, xAllegati, xDestBcc, False, xHTML);
          except
               SendMailThroughClient(xSubject, xTesto, xDestinatari, xDestBcc, xAllegati, xHTML, j);
          end
          else
               SendMailThroughClient(xSubject, xTesto, xDestinatari, xDestBcc, xAllegati, xHTML, j);

     end;
end;

procedure SendMailOutlook(aRecipient, aSubject, aNote, aFile, aBcc: string; Silent, HTML: boolean);
const olMailItem = 0;
var ii: integer; MyOutlook, MyMail: variant;
     xS: string;
begin
     MyOutlook := CreateOLEObject('Outlook.Application');
     MyMail := MyOutlook.CreateItem(olMailItem);

     //*** create a mail message...
     MyMail. to := aRecipient;
     MyMail.Subject := aSubject;

     if aNote <> '' then begin
          if HTML then
               MyMail.HTMLBody := aNote
          else begin
               MyMail.Body := aNote;
          end;
     end;

     //*** bcc ***
     MyMail.Bcc := aBcc;

     //*** Attachment...
     if (aFile <> '') and (aFile <> '-') then begin
          xS := aFile;
          while pos(';', xS) > 0 do begin
               MyMail.Attachments.Add(copy(xS, 1, pos(';', xS) - 1));
               xS := copy(xS, pos(';', xS) + 1, length(xS));
          end;
     end;

     if Silent then
          MyMail.Send
     else
          MyMail.Display;

     MyOutlook := UnAssigned;
end;

procedure SendMailThroughClient(SubMail, BodyMail, ToMail, BccMail, AttMail: string; HTMLMail: boolean; var j: integer);
var
     i: integer;
     Pathname, adMail: string;
     html: TStrings;
     textpart: TIdText;
     IdMessage1: TIdMessage;
begin
  //setup mail message
     IdMessage1 := TIdMessage.Create(nil);
     if AttMail <> '' then
          TIdAttachment.Create(IdMessage1.MessageParts, AttMail);
     html := TStringList.Create;
     html.Add('<html>');
     html.Add('<head>');
     html.Add('</head>');
     html.Add('<body>');
     html.Add(BodyMail + '<br/>');
     html.Add('</body>');
     html.Add('</html>');
     IdMessage1.Recipients.EMailAddresses := ToMail;
     IdMessage1.Subject := SubMail;
     IdMessage1.ExtraHeaders.Add('Bcc:' + BccMail);
     IdMessage1.ExtraHeaders.Add('X-Unsent: 1');
     if HTMLMail then begin
          IdMessage1.ContentType := 'multipart/mixed';
          IdMessage1.Body.Assign(html);
          IdMessage1.Body.Clear;
          textpart := TIdText.Create(IdMessage1.MessageParts);
          textpart.ContentType := 'text/plain';
          textpart.Body.Text := '';
          textpart.Free;
          textpart := TIdText.Create(IdMessage1.MessageParts, html);
          textpart.ContentType := 'text/html';
          textpart.Free;
     end
     else
          IdMessage1.Body.Text := BodyMail;
     Pathname := ExtractFilePath(Application.ExeName);
     try
          IdMessage1.SaveToFile(Data.Global.fieldbyname('DirFileLog').asString + '\' + 'test' + inttostr(j) + '.eml');
     except
          IdMessage1.SaveToFile(Pathname + '\' + 'test' + inttostr(j) + '.eml');
     end;
     ShellExecute(0, nil, PAnsiChar(Pathname + 'test' + inttostr(j) + '.eml'), nil, nil, SW_SHOW);
     j := j + 1;
     IdMessage1.Free;
end;

end.

