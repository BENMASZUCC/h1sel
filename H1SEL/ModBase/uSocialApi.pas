unit uSocialApi;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
     Dialogs, StdCtrls, activex, ShellApi;

function LinkedInImportDati(c_key, c_secret, t_key, t_secret,
     searchstring, percorsofile, nomefile, url: string): boolean;
function ExecAndWait(sExe, sCommandLine: string): Boolean;

implementation 

function ExecAndWait(sExe, sCommandLine: string): Boolean;
var
     dwExitCode: DWORD;
     tpiProcess: TProcessInformation;
     tsiStartup: TStartupInfo;
begin
     Result := False;
     FillChar(tsiStartup, SizeOf(TStartupInfo), 0);
     tsiStartup.cb := SizeOf(TStartupInfo);
     if CreateProcess(PChar(sExe), PChar(sCommandLine), nil, nil, False, 0,
          nil, nil, tsiStartup, tpiProcess) then
     begin
          if WAIT_OBJECT_0 = WaitForSingleObject(tpiProcess.hProcess, INFINITE) then
          begin
               if GetExitCodeProcess(tpiProcess.hProcess, dwExitCode) then
               begin
                    if dwExitCode = 0 then
                         Result := True
                    else
                         SetLastError(dwExitCode + $2000);
               end;
          end;
          dwExitCode := GetLastError;
          CloseHandle(tpiProcess.hProcess);
          CloseHandle(tpiProcess.hThread);
          SetLastError(dwExitCode);
     end;
end;

function LinkedInImportDati(c_key, c_secret, t_key, t_secret,
     searchstring, percorsofile, nomefile, url: string): boolean;
var c_param: string;
begin
     c_param := PChar(c_key + ' ' + c_secret + ' ' + t_key + ' ' + t_secret + ' ' +
          searchstring + ' "' + percorsofile + '" ' + nomefile + ' ' + url);
     Result := ExecAndWait('LinkedIn.exe', 'LinkedIn.exe' + ' ' + c_param);
end;

end.

