//[/TONI20021909DEBUGOK]
unit Sposta;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, Mask, DtEdit97, Db, DBCtrls, ExtCtrls, DtEdDB97,
     Grids, DBGrids, DBTables, ADODB, U_ADOLinkCl;

type
     TSpostaForm = class(TForm)
          Panel1: TPanel;
          Label3: TLabel;
          Label4: TLabel;
          Label5: TLabel;
          BitBtn1: TBitBtn;
          BitBtn2: TBitBtn;
          DEData: TDateEdit97;
          MEOre: TMaskEdit;
          EDescriz: TEdit;
          BitBtn3: TBitBtn;
          dsRisorse: TDataSource;
          PanLuogo: TPanel;
          Label2: TLabel;
          DBGrid1: TDBGrid;
          RGTipo: TRadioGroup;
          Label1: TLabel;
          MEAlleOre: TMaskEdit;
          CBLuogo: TCheckBox;
          GroupBox1: TGroupBox;
          DsQInfoRic: TDataSource;
          Label6: TLabel;
          DBEdit1: TDBEdit;
          Label7: TLabel;
          DBEdit2: TDBEdit;
          Label8: TLabel;
          DBEdit3: TDBEdit;
          Label9: TLabel;
          DBEdit4: TDBEdit;
          TRisorse: TADOLinkedQuery;
          QCheck: TADOLinkedQuery;
          QInfoRic: TADOLinkedQuery;
          QInfoRiccliente: TStringField;
          QInfoRicRuolo: TStringField;
          QInfoRicProgressivo: TStringField;
          QInfoRicTitoloCliente: TStringField;
          TRisorseID: TAutoIncField;
          TRisorseRisorsa: TStringField;
          TRisorseColor: TStringField;
          procedure BitBtn3Click(Sender: TObject);
          procedure RGTipoClick(Sender: TObject);
          procedure BitBtn1Click(Sender: TObject);
          procedure FormShow(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     SpostaForm: TSpostaForm;

implementation

uses AgendaSett, ModuloDati, Main,uutilsvarie;

{$R *.DFM}

procedure TSpostaForm.BitBtn3Click(Sender: TObject);
begin
     if Messagedlg('Sei sicuro di eliminare l''appuntamento ?', mtWarning, [mbNo, mbYes], 0) = mrYes then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text := 'delete from Agenda where ID=' + IntToStr(AgendaSettForm.xArrayIDAgenda[AgendaSettForm.xSelCol, AgendaSettForm.xSelRow]);
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    Log_Operation(mainform.xIDUtenteAttuale, 'Agenda', AgendaSettForm.xArrayIDAgenda[AgendaSettForm.xSelCol, AgendaSettForm.xSelRow], 'D', datetostr(date));
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               end;
          end;
     end;
end;

procedure TSpostaForm.RGTipoClick(Sender: TObject);
begin
     if RGTipo.ItemIndex = 0 then
          PanLuogo.Visible := true
     else begin
          PanLuogo.Visible := False;
          MEAlleOre.text := '  .  ';
     end;
end;

procedure TSpostaForm.BitBtn1Click(Sender: TObject);
var xHour, xMin, xSec, xMSec, xHour2, xMin2, xSec2, xMSec2: Word;
begin
     // controllo orari
     if MEAlleOre.Text <> '  .  ' then
          if StrToDateTime(DatetoStr(DEData.Date) + ' ' + MEOre.Text) >
               StrToDateTime(DatetoStr(DEData.Date) + ' ' + MEAlleOre.Text) then begin
               ModalResult := mrNone;
               ShowMessage('Orari non corretti');
               Abort;
          end;
     // controllo concomitanza (solo colloquio)
     if not CBLuogo.Checked then begin
          if (MEAlleOre.Text <> '  .  ') and (RGTipo.ItemIndex = 0) then begin
               QCheck.Close;
               //          QCheck.Prepare;
               //[ALBERTO 20020912]

               //[/TONI20020912DEBUG\]
               QCheck.ReloadSQL;
               QCheck.ParamByName['xDalle'] := StrToDateTime(DatetoStr(DEData.Date) + ' ' + MEOre.Text);
               QCheck.ParamByName['xAlle'] := StrToDateTime(DatetoStr(DEData.Date) + ' ' + MEAlleOre.Text);
               QCheck.ParamByName['xIDRis'] := TRisorse.FieldByName('ID').AsInteger;
               QCheck.ParamByName['xIDAgenda'] := AgendaSettForm.QAgendaABS.FieldByName('ID').AsInteger;
               QCheck.Open;
               //[/TONI20020912DEBUG\]FINE
               if not QCheck.IsEmpty then begin
                    ModalResult := mrNone;
                    DecodeTime(QCheck.FieldByName('Ore').asDateTime, xHour, xMin, xSec, xMSec);
                    DecodeTime(QCheck.FieldByName('AlleOre').asDateTime, xHour2, xMin2, xSec2, xMSec2);
                    MessageDlg('Risulta gi� presente il seguente impegno:' + chr(13) +
                         QCheck.FieldByName('Descrizione').asString + chr(13) +
                         'dalle ' + IntToStr(xHour) + '.' + IntToStr(xMin) +
                         '  alle ' + IntToStr(xHour2) + '.' + IntToStr(xMin2) + chr(13) +
                         'Luogo: ' + TRisorse.FieldByName('Risorsa').AsString, mtError, [mbOK], 0); 
                    Exit;
               end;
          end;
     end;
end;

procedure TSpostaForm.FormShow(Sender: TObject);
begin
     Caption := '[S/41] - ' + Caption;
     // info commessa
     if not ((AgendaSettForm.QAgendaABS.FieldByName('IDCandRic').AsInteger = 0) or
          (AgendaSettForm.QAgendaABS.FieldByName('IDCandRic').asString = '')) then begin
          QInfoRic.ParamByName['xIDCandRic'] := AgendaSettForm.QAgendaABS.FieldByName('IDCandRic').AsInteger;
          QInfoRic.Open;

          //[ALBERTO 20020912]FINE

          SpostaForm.Height := 447;
     end else SpostaForm.Height := 296;
     trisorse.close;
     TRisorse.Open;
end;

end.

