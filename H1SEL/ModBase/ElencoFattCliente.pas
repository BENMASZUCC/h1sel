unit ElencoFattCliente;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, Grids, DBGrids, DBTables, StdCtrls, Mask, DBCtrls, ExtCtrls, Buttons,
     ADODB, U_ADOLinkCl, dxDBTLCl, dxGrClms, dxTL, dxDBCtrl, dxDBGrid,
  dxCntner;

type
     TElencoFattClienteForm = class(TForm)
          BitBtn1: TBitBtn;
          BitBtn2: TBitBtn;
          Panel7: TPanel;
          DBEdit12: TDBEdit;
          Label1: TLabel;
          DsQFattCliente: TDataSource;
          Label2: TLabel;
          QFattCliente: TADOLinkedQuery;
          QFattClienteID: TAutoIncField;
          QFattClienteTipo: TStringField;
          QFattClienteIDCliente: TIntegerField;
          QFattClienteData: TDateTimeField;
          QFattClienteDecorrenza: TDateTimeField;
          QFattClienteImporto: TFloatField;
          QFattClienteIVA: TFloatField;
          QFattClienteTotale: TFloatField;
          QFattClienteModalitaPagamento: TStringField;
          QFattClienteAssegno: TStringField;
          QFattClienteScadenzaPagamento: TDateTimeField;
          QFattClientePagata: TBooleanField;
          QFattClientePagataInData: TDateTimeField;
          QFattClienteAppoggioBancario: TStringField;
          QFattClienteIDRicerca: TIntegerField;
          QFattClienteResponsabile: TStringField;
          QFattClienteFlagFuoriAppIVA63372: TBooleanField;
    QFattClienteProgressivo: TStringField;
    dxDBGrid35: TdxDBGrid;
    dxDBGrid35ID: TdxDBGridMaskColumn;
    dxDBGrid35Tipo: TdxDBGridMaskColumn;
    dxDBGrid35RifProg: TdxDBGridMaskColumn;
    dxDBGrid35IDCliente: TdxDBGridMaskColumn;
    dxDBGrid35Data: TdxDBGridDateColumn;
    dxDBGrid35Decorrenza: TdxDBGridDateColumn;
    dxDBGrid35Importo: TdxDBGridMaskColumn;
    dxDBGrid35IVA: TdxDBGridMaskColumn;
    dxDBGrid35Totale: TdxDBGridMaskColumn;
    dxDBGrid35ModalitaPagamento: TdxDBGridMaskColumn;
    dxDBGrid35Assegno: TdxDBGridMaskColumn;
    dxDBGrid35ScadenzaPagamento: TdxDBGridDateColumn;
    dxDBGrid35Pagata: TdxDBGridCheckColumn;
    dxDBGrid35PagataInData: TdxDBGridDateColumn;
    dxDBGrid35AppoggioBancario: TdxDBGridMaskColumn;
    dxDBGrid35IDRicerca: TdxDBGridMaskColumn;
    dxDBGrid35Responsabile: TdxDBGridMaskColumn;
    dxDBGrid35FlagFuoriAppIVA63372: TdxDBGridCheckColumn;
    dxDBGrid35Progressivo: TdxDBGridMaskColumn;
    QFattClienteRifProg: TStringField;
          procedure FormShow(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     ElencoFattClienteForm: TElencoFattClienteForm;

implementation

uses ModuloDati2,
     //[/TONI20020726\]
     ModuloDati;
//[/TONI20020726\]FINE


{$R *.DFM}

procedure TElencoFattClienteForm.FormShow(Sender: TObject);
begin
     Caption := '[S/23] - ' + Caption;
     QFattCliente.Open;
end;

end.
