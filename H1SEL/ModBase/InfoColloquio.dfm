object InfoColloquioForm: TInfoColloquioForm
  Left = 501
  Top = 218
  Width = 623
  Height = 626
  Caption = 'Informativa colloquio'
  Color = clBtnFace
  Constraints.MinHeight = 480
  Constraints.MinWidth = 424
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 615
    Height = 47
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object ToolbarButton971: TToolbarButton97
      Left = 6
      Top = 5
      Width = 113
      Height = 37
      DropdownMenu = PMFunzioniCand
      Caption = 'Tutte le funzioni'
      Glyph.Data = {
        36050000424D3605000000000000360400002800000010000000100000000100
        08000000000000010000120B0000120B00000001000000010000390800007310
        000029180000421800009C18000010210000392100004A210000942100003129
        00005A2900006B2900003131000073310000AD310000943900009C420000BD42
        0000C64200005A2908007B2908006B42080094420800A54A0800A5520800BD52
        0800AD5A08004A630800293910007B391000634A10009C4A10006B5210009C52
        10004A5A1000A5631000AD631000736B100063521800946B1800B56B1800294A
        21009C4A2100846321009C632100A56B2100AD6B2100B56B21009C6329009473
        2900AD732900638C2900634A3100215231004A523100847331009C733100AD73
        3100A57B3100424A3900C6633900636B39007B7B39008C7B39009C7B39009C84
        39007BA539008C844200948C42009C8C4200A58C4200635A4A00427B4A00C684
        4A007B8C4A0084944A009C944A0073AD4A0084AD4A006B7352009C9452007BAD
        5200FFB552005A6B5A007B735A00947B5A004A845A007B945A0094945A008C9C
        5A0084A55A00528C630094946300849C63008CA563009CA563007BAD63008CB5
        630084BD63009CBD6300AD7B6B00739C6B00849C6B0084AD6B00B5BD6B0084C6
        6B006BB573009CB57300ADB57300B59C7B0084A57B006BBD7B00FFBD7B009CC6
        7B009CCE7B00738C84008C8C8400F7AD840094B584009CBD8400B5CE84007384
        8C0063B58C00A5BD8C00A5C68C00BDCE8C0094C69400B5D69400ADDE94009CE7
        94006B9C9C00A5DE9C0094E79C00BDE79C006BA5A5007BCEA5008CCEA5007BD6
        A500ADDEA500F7DEA500BDEFA500B5F7AD009CEFB500BDFFB5008CCEBD00ADE7
        BD00B5EFBD00B5FFBD0094CEC600BDDEC600B5FFCE00ADFFD6009CE7DE00C6EF
        DE00DEFFDE00FFFFDE00FFFFE700C6F7EF00D6FFF700FFFFF700E7FFFF00EFFF
        FF00F7FFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00A3A3A3A3A3A3
        A35E4A37313949A3A3A3A3A3A343A357335A5944382D1A0E2AA3A3A366250822
        51716B5041322F1714A3A3A37B4E212062807C5F453A2E1F0A24A3A3A3696827
        618C8A6B4C402C1D0D19A3A3A37F817D638F9277583F2B131016704F9A9F728E
        838D967E5D3E26070BA38B5C9EA39B6C6A849788653D090274A399959DA3A375
        3060897A56293BA3A3A3A39498A2A15564A31C060334A3A3A3A3A3A3A38673A3
        A3360C150F04A3A3A3A3A3A3542311A3A31B4B45281208A3A3A3A3A3769C523C
        A3428578461801A3A3A3A3A390A06DA3A34D93915D1E00A3A3A3A3A3A38279A3
        A3676F875B0547A3A3A3A3A3A3A3A3A3A3A36E483553A3A3A3A3}
    end
    object Panel4: TPanel
      Left = 436
      Top = 3
      Width = 178
      Height = 41
      BevelOuter = bvNone
      TabOrder = 0
      object BOK: TToolbarButton97
        Left = 4
        Top = 3
        Width = 80
        Height = 35
        Caption = 'OK'
        Glyph.Data = {
          76060000424D7606000000000000360400002800000018000000180000000100
          080000000000400200000000000000000000000100000000000000000000FFFF
          FF00FF00FF0040D86800B1AEAC00545149001D973F0076967600C4F1D1002827
          270079D2910045A35E00716D5E00CFC7C00024C04D009B958D005FBA77005CE6
          8000E4EFE70090B69900B1D9BC005C8864003D3C390076B3870034934E003ABD
          5D007C7B730054D073005F5E5E00938F7800AAA695002CD358009BC6A60062A4
          73006AC98300E0D5C70049EC7500C1BBB7004FB96A00D5EEDB00697C6D008187
          820081C091002AAF4D00B5E9C300F1F7F2009D9D9C003432310044C666005397
          60009F9B7F0037CC5E004A49430039A0550072A67F007C9B83005F7763006E6E
          6D0055A66C006BB87F00A7A6A5008B8C880030C257004ADC700086B48800668B
          6C0060B06C0077C48B00259F470092C29C00C8C2B00054C27100E7FAEC0065B0
          790085827000C5C3C20056DC790071D08A005B584E005BCA79009A939900DCF3
          E3004DCC6E0086C4960080AC8C0032B054007572710064C17D003DA95B0041B4
          6000C9E7D100A39F9200B9F1C800F8FBF900D9D1C000BBB5B50046E67000B1AE
          A20048AB630080C793008DBB91006D9276006EAF7F0064D081004EE3750043CF
          680078BB890056B66F002ABD52007FB68D00CEECD6003FC262007E7E7B006F88
          7000329F500094908800D0CCB90031CC5A00C1B9AE00489B5F00A7A49E003BD1
          6300C4BBC100679F6F0077CD8E00CCC7C60055E57A004FA66700888886005DB5
          720072C0870095BD9700ACABA8005CA76F008CC09A006360610030BD56004DD1
          700046BF66005BC37700CBC8B600BBB8B90038B7590049A85B00AFAA98008482
          7500A09A8D0073C9890033D05D00B5B2A80059B16F00D9EEDF0087BB940033C6
          5A0096C3A1006DC4850069AB7A0040DE6A0051D775005B59590049D76D009B98
          80008F8D8500ECF6EF00E5F4E90028C35100B8AFB1003FCB63005AE07E009C97
          880065CA7F007B967A0062B47300D3EAD9004949480042BA610079767500A2A1
          9F006CCD8600A2A1990070B5820074B98600D4CFBC00C4BFB9002BC756003CA4
          58004AC66B008DB68E007CC08D00BFBDBE0068BD7E003CC6610049E773003131
          2F00E0F2E500C9C1BF0045E06E004FD472006B866D0082817D0094BE9F005FBF
          790065A97700C1EDCD003BD6640053E1790063786600B1AFA700ABA9A4007CCC
          9100C9BFB40037BC5A003DB45D00657C6A009BC1A600D1EFD90051B56C007DBA
          8E007A7870009B9B9B007BB88B00D9F2DF00B8ECC60026B04B002BB34F004C9A
          61008F8F8C00E8F5EB0059C3740093C69F0062B478008CB6970085BE9300A5A4
          A20055D476009D969A0053B96E005CBE76007CC78F00DEEEE300CAEDD300D3CB
          BD00BFB6B400B7B3B20056544B00DCD3C200CFC9BC00C6BDBD0037CF5F0041DA
          6A0042A45D00ADADAC0057E17C00A9A8A800A4A0950086868300020202020202
          022E39AEC10909C1AE392E020202020202020202020202B11C2F1C507ABDBD7A
          EB872F9F2E0202020202020202028016567DE5E488757588E4E57DB016FF0202
          0202020202C734A69A3E033FCDFCFCCD3FCC3E455F34700202020202E205F7AC
          94A03F69687E1111A84CA09481C3F43D0202023C4EF2AC7579CC30E31B60C068
          CDFC4CC57596254EE902021A7383B833796FC26EA3EA24C0C43F4C9E89B8640F
          DA023C0C0D2B993319C26E6E6EA31B60C4F9039EC5A7DF0D0CE973A9BB6C998E
          C26E6ED76ED7A3529D03797989526C4092A29176428855C2AD6EDD4851D727A3
          BACC7933A752198FD2914A23358AA4ADAD51081F5CC22727A38AF8339930AF06
          234A1DB6B98BD797C2087579945CA42727A3AF9999D3AF448C1DA174FAEC47D7
          CBA533333375DEA42797A3D46CE026724632A1F5776BEDB24FBF3E999933B8DE
          E39797A35859D8185EA1A9F67B961057224D6752BF3E3E0E2CE3EF122D966231
          F1A9B346ABE6E6BE9B937C0A0A0A4DB2AAF02D12A3ADFA0746FE841ECF21493B
          8243EED1D1D17C939BC95A5DAD0BE1D0908402FEF671B5B46ABC63636363EE43
          823B81147F3AC6F6FE0202FB5BB741D9DC2A535353532ABCB53B4985851525FE
          FB0202028461CF41869886868686E8D91766CA9C15D095D00202020202D0CF5F
          CEE7209A9A86986D17173638F395D00202020202020284787D3D283713D6C854
          65D53D7D7884020202020202020202FBB1044B8D2E2929DB8D4B04B1FB020202
          020202020202020202FBB1DBB1FDFD2EDBB18402020202020202}
        OnClick = BOKClick
      end
      object BAnnulla: TToolbarButton97
        Left = 93
        Top = 3
        Width = 80
        Height = 35
        Caption = 'Annulla'
        Glyph.Data = {
          76060000424D7606000000000000360400002800000018000000180000000100
          080000000000400200000000000000000000000100000000000000000000FFFF
          FF00FF00FF0082817D001A1AA9005A65E500B1B2EC003D3C3A00DFDCC100B1AE
          A2004F4FA3008787C20062615D003437D300D8D9F6006566BF00626284009F9B
          7E00C3C3BB007878A6007E81E300282827009F9FD6003736B1004E4EC500C4C4
          DE009D9C9C00ECECF800535249007474CE006D6D6D002525BD004C53DC008F8F
          8C009090E2006362A700938E77006363D1004346B4005858B8003F47CE005656
          9500C1BFA9009191CC00AAA59300D4D0BA00A9ACDC00333330006D6C8D008484
          D3009D9FE7007C7CBB00D0D2EB00BFBFED00484848007B7A7300706E5D002D2E
          C900282AAE006A6CDC00616197005A5BCA00E1E1F0009391BF00B6B6AE006F6F
          B70085816F0073729A00565AD700A4A3A100CDCCC400B8B8E2004343C3008080
          8A008B89B6007C7CC800A19D8A003338C5004D4DB5003030A700C9C5AD007373
          C10088878600545EE2005151D1006969A0002D32B500474BD60096938400BBBB
          B600898ED500ADACAA00343BBA005F5FB300595A57006060C0004949AB00777C
          D4009898CF00C7C8E700ABABD2003B3ED1008482B600E4E5F7004A4ACC005957
          4D009A9A92005656C0008A8ACB005C5C8000D6D7EE00BDBDBF00646DE300C1C0
          B200A3A096006E6ECB00292DBD002323B000AFAB9700DDDDEA008383C800CFCB
          BB004146D9005157C8004040C8007171D5002D2FCE0075746D003F3FB4004949
          BA004E4EBD00D3D3F5009C988500393ACA00BCBBAE00ABAAA6005961E0006C6C
          BD003333CD00DDDEF1009696CA007E7ED200D9D5BB002929C9008E8D87008D8D
          DC00898A8E008C8CC400A3A5D7009B978000BFC0E100A7A49C0068678D007877
          9A00A1A09E009494DE004244D300B5B3A7004B54C700B4B6EE006068E1006E6E
          C200F0F1F800C8C7BC004447C9004E4FD500545ADD00777BCD004A4942005B5B
          BF008B8ABE00E7E7F300838174006262CB00B5B5E300424AD3005151CA005F5F
          BC007C7CC300CAC8C500393ED5009397D400848483004645AF0066668500C6C5
          C0003636BB0030302F00E1E1F600DCD9BD00CECBC0002F2FB100CAC7B7005C5C
          5B00C5C3B6003939B5007E7E7B007777D600EBEBF3004546D0004E56DF004949
          BF00B1AFAB005151C2005E5EC7008686CE007A7AC7003434C700BBBDE2005D64
          E0005355D800565DDD00DADBF0005353C6007E7ECC00D0CDB8003638CE009D9D
          94009B9BD3006161B9008C8CCD007B7BD000B8B7AA00454ADA00B1AFA6005A5A
          CE005E5BB500383BD100383DCE00414BCB009494CE008484C100E9E9F8003C3B
          3600C4C5E7003335B2003535C3003538C8004747B6005959BB005B5ECA006262
          D5006A6AC1007272C500E2E3F200E0E0EE00D8D8F10056544B002929BC002B2B
          C0003031C90079786E00A4A09300606081009C998900A7A6A400020202020202
          021A1E36BB1515BB361E1A0202020202020202020202029AC12F0C6A126F6F12
          D90C2F5E1A02020202020202020252077FB3E6F04DE4E44D3D2BB337E9B60202
          020202020203A8DE8CEDDFA688D1D188C87AED2B86A8C40202020202211CC25F
          B420A6D388A07070A0D1A474A9A3F790020202FF6986B10D5626AFC853535388
          D14494960DEF716945020237583F7EA46EA261C85305055320A7ABA2D27EAAFE
          FB02FF38BEF88A328BD434AF530505539E8B8B9FA59C1FBE384590844A8F8A3B
          676E3E5A205353E5638B6757A5A539664C90AC2AE2CF8A7E06D46EF47BC8282E
          8B6714E39CA585B750AC42081768FAD8E30ED4D4345CB5F48B9FE365D8C77C04
          0842242DC3B0688AD8A567D4D434F4D483B465D88AED4875C02495D780D5E154
          8A8A448B6E6E6E6E9CE3D88AFAF918BFC21195BDB7D53D25F1853AD06E6E6EEA
          EBFA8A39EC48CB4F8E95FED7236BCCAD27E73E8B6EBCF66ED427BAB0D5C9810A
          2D847250995F5FA119C6F53E1B22351B3EC664608281EE4350725B2CE05D5FAE
          ABF5F4E891C57D9B67F4F57789EE6087768702727930F34B1B1BF6318D8DDD1D
          73AE1BAB474E9879FC02025BFCC23CCE4B16D6CDCDCDD6CEF30FF2DA0F2971FC
          5B0202028709E03C6C786CDCDCCD78CE5189DBDB29879D87020202020287E086
          FD0B62E62B6C78B25151416D409D87020202020202025B974692B81393622B33
          5510924697870202020202020202025B9ACAB9591A49491A59B9CA9A5B020202
          0202020202020202025B451A9A87871A1A9A5B02020202020202}
        OnClick = BAnnullaClick
      end
    end
  end
  object PCUnico: TPageControl
    Left = 0
    Top = 328
    Width = 615
    Height = 271
    ActivePage = TSCheckList
    Align = alClient
    TabOrder = 1
    object TSDatiRetribuz: TTabSheet
      Caption = 'Dati retributivi'
      object Panel7: TPanel
        Left = 0
        Top = 0
        Width = 607
        Height = 243
        Align = alClient
        BevelOuter = bvNone
        Caption = 'Panel7'
        TabOrder = 0
        object dxDBGrid1: TdxDBGrid
          Left = 0
          Top = 0
          Width = 607
          Height = 243
          Bands = <
            item
            end>
          DefaultLayout = True
          HeaderPanelRowCount = 1
          KeyField = 'IDColonna'
          SummaryGroups = <>
          SummarySeparator = ', '
          Align = alClient
          TabOrder = 0
          DataSource = DsQEspLavRetribuz
          Filter.Criteria = {00000000}
          OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
          OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoUseBitmap]
          object dxDBGrid1ID: TdxDBGridMaskColumn
            Visible = False
            Width = 77
            BandIndex = 0
            RowIndex = 0
            FieldName = 'ID'
          end
          object dxDBGrid1Voce: TdxDBGridMaskColumn
            DisableEditor = True
            Sorted = csUp
            Width = 298
            BandIndex = 0
            RowIndex = 0
            FieldName = 'Voce'
          end
          object dxDBGrid1Flag: TdxDBGridCheckColumn
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Width = 64
            BandIndex = 0
            RowIndex = 0
            FieldName = 'Flag'
            ValueChecked = 'True'
            ValueUnchecked = 'False'
          end
          object dxDBGrid1Valore: TdxDBGridMaskColumn
            Width = 100
            BandIndex = 0
            RowIndex = 0
            FieldName = 'Valore'
          end
        end
      end
    end
    object TSMotIns: TTabSheet
      Caption = 'Motivi di insoddisfazione'
      ImageIndex = 1
      object ASG1: TAdvStringGrid
        Left = 0
        Top = 20
        Width = 607
        Height = 223
        Cursor = crDefault
        Align = alClient
        ColCount = 3
        DefaultRowHeight = 16
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goEditing]
        ScrollBars = ssHorizontal
        TabOrder = 0
        OnRightClickCell = ASG1RightClickCell
        OnCheckBoxClick = ASG1CheckBoxClick
        HintColor = clYellow
        ActiveCellFont.Charset = DEFAULT_CHARSET
        ActiveCellFont.Color = clWindowText
        ActiveCellFont.Height = -11
        ActiveCellFont.Name = 'Tahoma'
        ActiveCellFont.Style = [fsBold]
        CellNode.NodeType = cnFlat
        ColumnHeaders.Strings = (
          'soggetto'
          'E'
          'C')
        ControlLook.DropDownHeader.Font.Charset = DEFAULT_CHARSET
        ControlLook.DropDownHeader.Font.Color = clWindowText
        ControlLook.DropDownHeader.Font.Height = -11
        ControlLook.DropDownHeader.Font.Name = 'Tahoma'
        ControlLook.DropDownHeader.Font.Style = []
        ControlLook.DropDownHeader.Visible = True
        ControlLook.DropDownHeader.Buttons = <>
        ControlLook.DropDownFooter.Font.Charset = DEFAULT_CHARSET
        ControlLook.DropDownFooter.Font.Color = clWindowText
        ControlLook.DropDownFooter.Font.Height = -11
        ControlLook.DropDownFooter.Font.Name = 'MS Sans Serif'
        ControlLook.DropDownFooter.Font.Style = []
        ControlLook.DropDownFooter.Visible = True
        ControlLook.DropDownFooter.Buttons = <>
        EnhRowColMove = False
        Filter = <>
        FilterDropDown.Font.Charset = DEFAULT_CHARSET
        FilterDropDown.Font.Color = clWindowText
        FilterDropDown.Font.Height = -11
        FilterDropDown.Font.Name = 'MS Sans Serif'
        FilterDropDown.Font.Style = []
        FilterDropDownClear = '(All)'
        FixedColWidth = 220
        FixedRowHeight = 16
        FixedFont.Charset = DEFAULT_CHARSET
        FixedFont.Color = clWindowText
        FixedFont.Height = -11
        FixedFont.Name = 'MS Sans Serif'
        FixedFont.Style = []
        FloatFormat = '%.2f'
        MouseActions.DirectEdit = True
        PrintSettings.DateFormat = 'dd/mm/yyyy'
        PrintSettings.Font.Charset = DEFAULT_CHARSET
        PrintSettings.Font.Color = clWindowText
        PrintSettings.Font.Height = -11
        PrintSettings.Font.Name = 'MS Sans Serif'
        PrintSettings.Font.Style = []
        PrintSettings.FixedFont.Charset = DEFAULT_CHARSET
        PrintSettings.FixedFont.Color = clWindowText
        PrintSettings.FixedFont.Height = -11
        PrintSettings.FixedFont.Name = 'MS Sans Serif'
        PrintSettings.FixedFont.Style = []
        PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
        PrintSettings.HeaderFont.Color = clWindowText
        PrintSettings.HeaderFont.Height = -11
        PrintSettings.HeaderFont.Name = 'MS Sans Serif'
        PrintSettings.HeaderFont.Style = []
        PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
        PrintSettings.FooterFont.Color = clWindowText
        PrintSettings.FooterFont.Height = -11
        PrintSettings.FooterFont.Name = 'MS Sans Serif'
        PrintSettings.FooterFont.Style = []
        PrintSettings.Borders = pbNoborder
        PrintSettings.Centered = False
        PrintSettings.PagePrefix = 'page'
        PrintSettings.PageNumSep = '/'
        ScrollWidth = 16
        SearchFooter.FindNextCaption = 'Find &next'
        SearchFooter.FindPrevCaption = 'Find &previous'
        SearchFooter.Font.Charset = DEFAULT_CHARSET
        SearchFooter.Font.Color = clWindowText
        SearchFooter.Font.Height = -11
        SearchFooter.Font.Name = 'MS Sans Serif'
        SearchFooter.Font.Style = []
        SearchFooter.HighLightCaption = 'Highlight'
        SearchFooter.HintClose = 'Close'
        SearchFooter.HintFindNext = 'Find next occurence'
        SearchFooter.HintFindPrev = 'Find previous occurence'
        SearchFooter.HintHighlight = 'Highlight occurences'
        SearchFooter.MatchCaseCaption = 'Match case'
        SelectionColor = clHighlight
        SelectionTextColor = clHighlightText
        URLColor = clBlack
        VAlignment = vtaCenter
        Version = '5.0.0.3'
        WordWrap = False
        ColWidths = (
          220
          15
          14)
        RowHeights = (
          16
          16
          16
          16
          16
          16
          16
          16
          16
          16)
      end
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 607
        Height = 20
        Align = alTop
        BevelOuter = bvLowered
        TabOrder = 1
        object Label16: TLabel
          Left = 4
          Top = 3
          Width = 340
          Height = 13
          Caption = 
            'fare click con il pulsante destro per visualizzare la descrizion' +
            'e dettagliata'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clPurple
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
      end
    end
    object TSCheckList: TTabSheet
      Caption = 'CheckList colloquio'
      ImageIndex = 2
      object dxDBGrid2: TdxDBGrid
        Left = 0
        Top = 0
        Width = 607
        Height = 243
        Bands = <
          item
          end>
        DefaultLayout = True
        HeaderPanelRowCount = 1
        KeyField = 'ID'
        SummaryGroups = <>
        SummarySeparator = ', '
        Align = alClient
        TabOrder = 0
        DataSource = DsQCLColloquioTemp
        Filter.Criteria = {00000000}
        OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
        OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoIndicator, edgoUseBitmap]
        object dxDBGrid2Voce: TdxDBGridMaskColumn
          DisableEditor = True
          Width = 269
          BandIndex = 0
          RowIndex = 0
          FieldName = 'Voce'
        end
        object dxDBGrid2Flag: TdxDBGridCheckColumn
          Width = 154
          BandIndex = 0
          RowIndex = 0
          FieldName = 'Flag'
          ValueChecked = 'True'
          ValueUnchecked = 'False'
        end
        object dxDBGrid2Risposta: TdxDBGridMaskColumn
          Width = 168
          BandIndex = 0
          RowIndex = 0
          FieldName = 'Risposta'
        end
      end
    end
    object TSEsito: TTabSheet
      Caption = 'Esito colloquio e altro'
      ImageIndex = 3
      object Label10: TLabel
        Left = 158
        Top = 6
        Width = 71
        Height = 13
        Caption = 'Data colloquio:'
      end
      object Label11: TLabel
        Left = 4
        Top = 6
        Width = 94
        Height = 13
        Caption = 'Colloquio tenuto da:'
      end
      object Label15: TLabel
        Left = 5
        Top = 115
        Width = 95
        Height = 13
        Caption = 'Motivo eliminazione:'
        Visible = False
      end
      object Panel6: TPanel
        Left = 4
        Top = 51
        Width = 341
        Height = 51
        BevelInner = bvLowered
        TabOrder = 0
        object Label12: TLabel
          Left = 8
          Top = 6
          Width = 23
          Height = 13
          Caption = 'Esito'
        end
        object Label13: TLabel
          Left = 157
          Top = 6
          Width = 37
          Height = 13
          Caption = 'Giudizio'
        end
        object Label14: TLabel
          Left = 282
          Top = 6
          Width = 48
          Height = 13
          Caption = 'Punteggio'
        end
        object CBEsito: TComboBox
          Left = 8
          Top = 22
          Width = 145
          Height = 21
          ItemHeight = 13
          TabOrder = 0
          Text = 'mantenere nella ricerca'
          OnChange = CBEsitoChange
          Items.Strings = (
            'mantenere nella ricerca'
            'altro colloquio'
            'eliminare dalla ricerca')
        end
        object CBGiudizio: TComboBox
          Left = 156
          Top = 21
          Width = 123
          Height = 21
          Hint = 
            'Questa lista pu� essere riempita nella sezione dei parametri del' +
            'le Tabelle.'
          ItemHeight = 13
          TabOrder = 1
          Items.Strings = (
            'ottimo'
            'buono'
            'discreto'
            'sufficiente'
            'insufficiente'
            'SOTTODIMESIONATO'
            'ALLINEATO'
            'SOVRADIMENSIONATO'
            'STAND BY')
        end
        object SEPunteggio: TSpinEdit
          Left = 283
          Top = 21
          Width = 49
          Height = 22
          MaxValue = 100
          MinValue = 0
          TabOrder = 2
          Value = 0
        end
      end
      object DataColloquio: TDateEdit97
        Left = 158
        Top = 22
        Width = 104
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        ColorCalendar.ColorValid = clBlue
        DayNames.Monday = 'lu'
        DayNames.Tuesday = 'ma'
        DayNames.Wednesday = 'me'
        DayNames.Thursday = 'gi'
        DayNames.Friday = 've'
        DayNames.Saturday = 'sa'
        DayNames.Sunday = 'do'
        MonthNames.January = 'gennaio'
        MonthNames.February = 'febbraio'
        MonthNames.March = 'marzo'
        MonthNames.April = 'aprile'
        MonthNames.May = 'maggio'
        MonthNames.June = 'giugno'
        MonthNames.July = 'luglio'
        MonthNames.August = 'agosto'
        MonthNames.September = 'settembre'
        MonthNames.October = 'ottobre'
        MonthNames.November = 'novembre'
        MonthNames.December = 'dicembre'
        Options = [doButtonTabStop, doCanPopup, doIsMasked, doShowCancel, doShowToday]
      end
      object CBFlagAccPres: TCheckBox
        Left = 5
        Top = 112
        Width = 588
        Height = 17
        Caption = 'Accetta di essere presentato all'#39'azienda '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clPurple
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
      end
      object eMotivoElim: TEdit
        Left = 5
        Top = 129
        Width = 597
        Height = 21
        TabOrder = 3
        Visible = False
      end
      object GBColloquioAzienda: TGroupBox
        Left = 4
        Top = 157
        Width = 298
        Height = 66
        Hint = '<'
        Caption = 'Colloquio presso il cliente'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 4
        object ToolbarButton9711: TToolbarButton97
          Left = 266
          Top = 37
          Width = 24
          Height = 22
          Hint = 'Tabella dettagli colloquio presso il cliente'
          Glyph.Data = {
            36050000424D3605000000000000360400002800000010000000100000000100
            08000000000000010000120B0000120B00000001000000010000000000008400
            000084848400C6C6C600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00040404040404
            0404040404040404040404040404040404040404040404040404000000000000
            0000000000000000000002040404040404040404040404040400020403030303
            0303030303030303040002040304030404040302020202030400020403030303
            0303030303030303040002040304030404040304040404030400020403030303
            0303030303030303040002040304030404040304040404030400020403030303
            0303030303030303040002040404040404040404040404040400020301030101
            0101010101010101010002030303030303030303030303030300020202020202
            0202020202020202020004040404040404040404040404040404}
          Opaque = False
          ParentShowHint = False
          ShowHint = True
          WordWrap = True
          OnClick = ToolbarButton9711Click
        end
        object CBColloquioCliente: TCheckBox
          Left = 7
          Top = 17
          Width = 138
          Height = 17
          Caption = 'Colloquio presso cliente'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          OnClick = CBColloquioClienteClick
        end
        object CBDettCollCliente: TComboBox
          Left = 7
          Top = 37
          Width = 255
          Height = 21
          Color = clBtnFace
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ItemHeight = 13
          ParentFont = False
          TabOrder = 1
        end
      end
      object CBSelezionatori: TComboBox
        Left = 5
        Top = 21
        Width = 145
        Height = 21
        ItemHeight = 13
        TabOrder = 5
        OnChange = CBSelezionatoriChange
        Items.Strings = (
          'mantenere nella ricerca'
          'altro colloquio'
          'eliminare dalla ricerca')
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 80
    Width = 615
    Height = 109
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 2
    object Label1: TLabel
      Left = 6
      Top = 25
      Width = 38
      Height = 13
      Caption = 'Azienda'
      FocusControl = DBEdit1
    end
    object Label2: TLabel
      Left = 231
      Top = 25
      Width = 72
      Height = 13
      Caption = 'Attivita azienda'
      FocusControl = DBEdit2
    end
    object Label3: TLabel
      Left = 62
      Top = 66
      Width = 25
      Height = 13
      Caption = 'Anno'
      FocusControl = DBEdit3
    end
    object Label4: TLabel
      Left = 31
      Top = 66
      Width = 26
      Height = 13
      Caption = 'Mese'
      FocusControl = DBEdit4
    end
    object Label5: TLabel
      Left = 6
      Top = 85
      Width = 16
      Height = 13
      Caption = 'Dal'
    end
    object Label6: TLabel
      Left = 408
      Top = 26
      Width = 86
      Height = 13
      Caption = 'Ruolo nell'#39'azienda'
    end
    object Label7: TLabel
      Left = 231
      Top = 66
      Width = 91
      Height = 13
      Caption = 'Contratto nazionale'
    end
    object Label8: TLabel
      Left = 408
      Top = 66
      Width = 83
      Height = 13
      Caption = 'Qualifica e Livello'
    end
    object Panel12: TPanel
      Left = 1
      Top = 1
      Width = 613
      Height = 21
      Align = alTop
      Alignment = taLeftJustify
      Caption = '  Esperienza lavorativa attuale'
      Color = clGray
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
    end
    object DBEdit1: TDBEdit
      Left = 6
      Top = 41
      Width = 222
      Height = 21
      Color = clAqua
      DataField = 'Azienda'
      DataSource = DsQEspLavAttuale
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 5
    end
    object DBEdit2: TDBEdit
      Left = 231
      Top = 41
      Width = 175
      Height = 21
      DataField = 'Attivita'
      DataSource = DsQEspLavAttuale
      ReadOnly = True
      TabOrder = 6
    end
    object DBEdit3: TDBEdit
      Left = 61
      Top = 81
      Width = 41
      Height = 21
      DataField = 'AnnoDal'
      DataSource = DsQEspLavAttuale
      TabOrder = 2
    end
    object DBEdit4: TDBEdit
      Left = 30
      Top = 81
      Width = 28
      Height = 21
      DataField = 'MeseDal'
      DataSource = DsQEspLavAttuale
      TabOrder = 1
    end
    object dxDBExtLookupEdit1: TdxDBExtLookupEdit
      Left = 408
      Top = 41
      Width = 199
      TabOrder = 0
      Visible = False
      DataField = 'Ruolo'
      DataSource = DsQEspLavAttuale
      DBGridLayout = dxDBGridLayoutListRuoli
    end
    object DBEdit5: TDBEdit
      Left = 231
      Top = 81
      Width = 174
      Height = 21
      DataField = 'ContrattoNaz'
      DataSource = DsQEspLavAttuale
      ReadOnly = True
      TabOrder = 7
    end
    object dxDBExtLookupEdit2: TdxDBExtLookupEdit
      Left = 408
      Top = 81
      Width = 200
      TabOrder = 3
      Visible = False
      DataField = 'TipoQualif'
      DataSource = DsQEspLavAttuale
      DBGridLayout = dxDBGridLayoutList1QualifContr
    end
    object DBEdit10: TDBEdit
      Left = 408
      Top = 41
      Width = 198
      Height = 21
      DataField = 'Ruolo'
      DataSource = DsQEspLavAttuale
      TabOrder = 8
    end
    object DBEdit11: TDBEdit
      Left = 408
      Top = 81
      Width = 199
      Height = 21
      DataField = 'TipoQualif'
      DataSource = DsQEspLavAttuale
      TabOrder = 9
    end
  end
  object Panel5: TPanel
    Left = 0
    Top = 47
    Width = 615
    Height = 33
    Align = alTop
    BevelOuter = bvLowered
    Enabled = False
    TabOrder = 3
    object Label9: TLabel
      Left = 6
      Top = 9
      Width = 51
      Height = 13
      Caption = 'Candidato:'
    end
    object ECogn: TEdit
      Left = 61
      Top = 6
      Width = 164
      Height = 21
      Color = clYellow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object ENome: TEdit
      Left = 227
      Top = 6
      Width = 149
      Height = 21
      Color = clYellow
      TabOrder = 1
    end
  end
  object Panel8: TPanel
    Left = 0
    Top = 189
    Width = 615
    Height = 139
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 4
    object Panel9: TPanel
      Left = 1
      Top = 1
      Width = 613
      Height = 21
      Align = alTop
      Alignment = taLeftJustify
      Caption = '  Dati Commessa'
      Color = clGray
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object GBCliente: TGroupBox
      Left = 163
      Top = 22
      Width = 451
      Height = 45
      Align = alClient
      Caption = 'Cliente'
      TabOrder = 1
      object DBEdit6: TDBEdit
        Left = 6
        Top = 14
        Width = 436
        Height = 21
        Anchors = [akLeft, akTop, akRight]
        Color = clAqua
        DataField = 'Cliente'
        DataSource = DSQRicPend
        ReadOnly = True
        TabOrder = 0
      end
    end
    object GroupBox7: TGroupBox
      Left = 1
      Top = 22
      Width = 162
      Height = 45
      Align = alLeft
      Caption = 'Rif./Codice'
      TabOrder = 2
      object DBEdit7: TDBEdit
        Left = 6
        Top = 16
        Width = 145
        Height = 21
        TabStop = False
        Color = clBtnFace
        DataField = 'Progressivo'
        DataSource = DSQRicPend
        Enabled = False
        ReadOnly = True
        TabOrder = 0
      end
    end
    object GroupBox3: TGroupBox
      Left = 1
      Top = 67
      Width = 613
      Height = 71
      Align = alBottom
      Caption = 'Ruolo richiesto'
      TabOrder = 3
      object Label21: TLabel
        Left = 8
        Top = 43
        Width = 25
        Height = 13
        Caption = 'Area:'
      end
      object DBEdit8: TDBEdit
        Left = 38
        Top = 40
        Width = 180
        Height = 21
        Color = clBtnFace
        DataField = 'Area'
        DataSource = DSQRicPend
        Enabled = False
        ReadOnly = True
        TabOrder = 0
      end
      object DBEdit9: TDBEdit
        Left = 11
        Top = 15
        Width = 207
        Height = 21
        DataField = 'Mansione'
        DataSource = DSQRicPend
        ReadOnly = True
        TabOrder = 1
      end
      object DBEdit13: TDBEdit
        Left = 227
        Top = 40
        Width = 378
        Height = 21
        Hint = 'Ruolo richiesto (titolo attribuito dal cliente):'
        Anchors = [akLeft, akTop, akRight]
        DataField = 'TitoloCliente'
        DataSource = DSQRicPend
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 2
      end
    end
  end
  object DsQEspLavRetribuz: TDataSource
    DataSet = QEspLavRetribuz
    Left = 24
    Top = 416
  end
  object DsQEspLavAttuale: TDataSource
    DataSet = QEspLavAttuale
    Left = 96
    Top = 111
  end
  object QEspLavAttuale: TADOLinkedQuery
    Connection = Data.DB
    CursorType = ctStatic
    AfterPost = QEspLavAttualeAfterPost
    Parameters = <
      item
        Name = 'xIDAnag:'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      ''
      'select EBC_Clienti.Descrizione Azienda, EBC_Attivita.Attivita,'
      '       EsperienzeLavorative.ID,EsperienzeLavorative.IDMansione,'
      '       AnnoDal,MeseDal, EsperienzeLavorative.IDQualifContr'
      
        'from EsperienzeLavorative left join  EBC_Clienti on EsperienzeLa' +
        'vorative.IDAzienda = EBC_Clienti.ID'
      
        'left join EBC_Attivita on EsperienzeLavorative.IDSettore = EBC_A' +
        'ttivita.ID'
      'where IDAnagrafica=:xIDAnag:'
      'and attuale=1'
      ''
      '')
    UseFilter = False
    Left = 64
    Top = 112
    object QEspLavAttualeAzienda: TStringField
      FieldName = 'Azienda'
      Size = 50
    end
    object QEspLavAttualeAttivita: TStringField
      FieldName = 'Attivita'
      Size = 50
    end
    object QEspLavAttualeID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QEspLavAttualeIDMansione: TIntegerField
      FieldName = 'IDMansione'
    end
    object QEspLavAttualeAnnoDal: TSmallintField
      FieldName = 'AnnoDal'
    end
    object QEspLavAttualeMeseDal: TSmallintField
      FieldName = 'MeseDal'
    end
    object QEspLavAttualeRuolo: TStringField
      FieldKind = fkLookup
      FieldName = 'Ruolo'
      LookupDataSet = QRuoli
      LookupKeyFields = 'ID'
      LookupResultField = 'Ruolo'
      KeyFields = 'IDMansione'
      Size = 40
      Lookup = True
    end
    object QEspLavAttualeIDQualifContr: TIntegerField
      FieldName = 'IDQualifContr'
    end
    object QEspLavAttualeContrattoNaz: TStringField
      FieldKind = fkLookup
      FieldName = 'ContrattoNaz'
      LookupDataSet = QQualifContr
      LookupKeyFields = 'ID'
      LookupResultField = 'ContrattoNaz'
      KeyFields = 'IDQualifContr'
      Size = 40
      Lookup = True
    end
    object QEspLavAttualeTipoQualif: TStringField
      FieldKind = fkLookup
      FieldName = 'TipoQualif'
      LookupDataSet = QQualifContr
      LookupKeyFields = 'ID'
      LookupResultField = 'TipoQualif'
      KeyFields = 'IDQualifContr'
      Size = 70
      Lookup = True
    end
  end
  object DsQRuoli: TDataSource
    DataSet = QRuoli
    Left = 552
    Top = 55
  end
  object QRuoli: TADOLinkedQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select Mansioni.ID,Mansioni.Descrizione Ruolo, '
      '          Aree.ID IDArea, Aree.Descrizione Area'
      'from Mansioni, Aree'
      'where Mansioni.IDArea = Aree.ID')
    UseFilter = False
    Left = 520
    Top = 79
    object QRuoliID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QRuoliRuolo: TStringField
      FieldName = 'Ruolo'
      Size = 40
    end
    object QRuoliArea: TStringField
      FieldName = 'Area'
      Size = 30
    end
    object QRuoliIDArea: TAutoIncField
      FieldName = 'IDArea'
      ReadOnly = True
    end
  end
  object dxDBGridLayoutList1: TdxDBGridLayoutList
    Left = 552
    Top = 400
    object dxDBGridLayoutListRuoli: TdxDBGridLayout
      Data = {
        AC020000545046301054647844424772696457726170706572000542616E6473
        0E0100000D44656661756C744C61796F7574091348656164657250616E656C52
        6F77436F756E740201084B65794669656C64060249440D53756D6D6172794772
        6F7570730E001053756D6D617279536570617261746F7206022C200A44617461
        536F75726365071A496E666F436F6C6C6F7175696F466F726D2E44735152756F
        6C690F46696C7465722E43726974657269610A04000000000000000F4F707469
        6F6E734265686176696F720B0E6564676F4175746F5365617263680C6564676F
        4175746F536F72740E6564676F447261675363726F6C6C136564676F456E7465
        7253686F77456469746F72136564676F496D6D656469617465456469746F720E
        6564676F5461625468726F7567680F6564676F566572745468726F7567680009
        4F7074696F6E7344420B106564676F43616E63656C4F6E457869740D6564676F
        43616E44656C6574650D6564676F43616E496E73657274116564676F43616E4E
        617669676174696F6E116564676F436F6E6669726D44656C657465126564676F
        4C6F6164416C6C5265636F726473106564676F557365426F6F6B6D61726B7300
        0B4F7074696F6E73566965770B0D6564676F4175746F5769647468136564676F
        42616E6448656164657257696474680D6564676F5573654269746D6170000013
        5464784442477269644D61736B436F6C756D6E0441726561094D61784C656E67
        7468026405576964746803AD000942616E64496E646578020008526F77496E64
        65780200094669656C644E616D65060441726561000013546478444247726964
        4D61736B436F6C756D6E0552756F6C6F094D61784C656E677468026405576964
        746803C1000942616E64496E646578020008526F77496E646578020009466965
        6C644E616D65060552756F6C6F000000}
    end
    object dxDBGridLayoutList1QualifContr: TdxDBGridLayout
      Data = {
        87030000545046301054647844424772696457726170706572000542616E6473
        0E0100000D44656661756C744C61796F7574091348656164657250616E656C52
        6F77436F756E740201084B65794669656C64060249440D53756D6D6172794772
        6F7570730E001053756D6D617279536570617261746F7206022C200A44617461
        536F757263650720496E666F436F6C6C6F7175696F466F726D2E447351517561
        6C6966436F6E74720D46696C7465722E416374697665090F46696C7465722E43
        726974657269610A04000000000000000F4F7074696F6E734265686176696F72
        0B0E6564676F4175746F5365617263680C6564676F4175746F536F72740E6564
        676F447261675363726F6C6C136564676F456E74657253686F77456469746F72
        136564676F496D6D656469617465456469746F720E6564676F5461625468726F
        7567680F6564676F566572745468726F75676800094F7074696F6E7344420B10
        6564676F43616E63656C4F6E457869740D6564676F43616E44656C6574650D65
        64676F43616E496E73657274116564676F43616E4E617669676174696F6E1165
        64676F436F6E6669726D44656C657465126564676F4C6F6164416C6C5265636F
        726473106564676F557365426F6F6B6D61726B73000B4F7074696F6E73566965
        770B0D6564676F4175746F5769647468136564676F42616E6448656164657257
        696474680D6564676F496E64696361746F720D6564676F5573654269746D6170
        0000135464784442477269644D61736B436F6C756D6E0C436F6E74726174746F
        4E617A0743617074696F6E0613436F6E74726174746F206E617A696F6E616C65
        094D61784C656E6774680264055769647468038F000942616E64496E64657802
        0008526F77496E6465780200094669656C644E616D65060C436F6E7472617474
        6F4E617A0000135464784442477269644D61736B436F6C756D6E0D5469706F43
        6F6E74726174746F0743617074696F6E06115469706F20646920636F6E747261
        74746F094D61784C656E6774680264055769647468027A0942616E64496E6465
        78020008526F77496E6465780200094669656C644E616D65060D5469706F436F
        6E74726174746F0000135464784442477269644D61736B436F6C756D6E095175
        616C6966696361094D61784C656E677468026405576964746803B3000942616E
        64496E646578020008526F77496E6465780200094669656C644E616D65060951
        75616C6966696361000000}
    end
  end
  object QGriglia: TADOLinkedQuery
    Connection = Data.DB
    Parameters = <>
    OriginalSQL.Strings = (
      'select EspLavMotInsModel.*,EspLavMotIns.Valore'
      
        'from EspLavMotInsModel left join EspLavMotIns on EspLavMotInsMod' +
        'el.ID = EspLavMotIns.IDVoce'
      'and IDEspLav=:xIDEspLav:'
      'order by EspLavMotInsModel.ID')
    UseFilter = False
    Left = 152
    Top = 440
  end
  object QUpd: TADOLinkedQuery
    Connection = Data.DB
    Parameters = <>
    UseFilter = False
    Left = 184
    Top = 440
  end
  object Q: TADOLinkedQuery
    Connection = Data.DB
    Parameters = <>
    UseFilter = False
    Left = 216
    Top = 440
  end
  object QEspLavDB: TADOLinkedQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      ''
      
        'select distinct EspLavRetribuzioni.ID,NomeColonna Voce,Valore,Fl' +
        'ag,'
      'EspLavRetribColonne.ID IDColonna'
      'from EspLavRetribColonne left join EspLavRetribuzioni  '
      'on EspLavRetribColonne.ID = EspLavRetribuzioni.IDColonna'
      'and EspLavRetribuzioni.IDEspLav=1')
    UseFilter = False
    Left = 60
    Top = 401
    object QEspLavDBID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QEspLavDBVoce: TStringField
      FieldName = 'Voce'
      FixedChar = True
      Size = 30
    end
    object QEspLavDBValore: TFloatField
      FieldName = 'Valore'
    end
    object QEspLavDBFlag: TBooleanField
      FieldName = 'Flag'
    end
    object QEspLavDBIDColonna: TAutoIncField
      FieldName = 'IDColonna'
      ReadOnly = True
    end
  end
  object DsQCLColloquioTemp: TDataSource
    DataSet = QCLColloquioTemp
    Left = 316
    Top = 409
  end
  object TSelezionatori_OLD: TADOLinkedTable
    Connection = Data.DB
    CursorType = ctStatic
    TableName = 'Users'
    Left = 352
    Top = 376
  end
  object DsSelezionatori: TDataSource
    DataSet = TSelezionatori
    Left = 352
    Top = 408
  end
  object PMFunzioniCand: TPopupMenu
    Images = ImageList1
    Left = 104
    Top = 12
    object Schedacandidato1: TMenuItem
      Caption = 'Scheda candidato'
      ImageIndex = 0
      OnClick = Schedacandidato1Click
    end
    object Curriculum1: TMenuItem
      Caption = 'Curriculum'
      ImageIndex = 1
      OnClick = Curriculum1Click
    end
    object Documenti1: TMenuItem
      Caption = 'Documenti'
      ImageIndex = 2
      OnClick = Documenti1Click
    end
    object AppuntiWord1: TMenuItem
      Caption = 'Appunti Word'
      ImageIndex = 3
      OnClick = AppuntiWord1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Valutazionecandidato1: TMenuItem
      Caption = 'Valutazione candidato'
      OnClick = Valutazionecandidato1Click
    end
    object NuovaEsperienzalavorativaattuale1: TMenuItem
      Caption = 'Nuova Esperienza lavorativa attuale'
      OnClick = NuovaEsperienzalavorativaattuale1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Tabella1: TMenuItem
      Caption = 'Tabella voci retributive'
      ImageIndex = 4
      OnClick = Tabella1Click
    end
    object Tabellaqualificheecontratti1: TMenuItem
      Caption = 'Tabella qualifiche e contratti'
      ImageIndex = 4
      OnClick = Tabellaqualificheecontratti1Click
    end
    object Tabellamoticidiinsoddisfazione1: TMenuItem
      Caption = 'Tabella motivi di insoddisfazione'
      ImageIndex = 4
      OnClick = Tabellamoticidiinsoddisfazione1Click
    end
  end
  object ImageList1: TImageList
    Left = 136
    Top = 13
    Bitmap = {
      494C010105000900040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000003000000001001000000000000018
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001042104210421042104210421042
      1042104210421042104210421042000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001042186300000000000000000000
      0000000000000000000000001042000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001042186300000000000000000000
      0000000000000000000000001042000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001042186300001042104210421042
      0000104210421042104200001042000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001042186300000000000000000000
      0000000000000000000000001042000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001042186300001042104210421042
      0000104210421042104200001042000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001042186300000000000000000000
      0000000000000000000000001042000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001042186300001042104210421042
      0000104210421042104200001042000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001042186300000000000000000000
      0000000000000000000000001042000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001042186310421042104210421042
      1042104210421042104210421042000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001042186310001F0010001F001000
      1F00100000000000000000001042000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001042186318631863186318631863
      1863186318631863186318631042000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001042104210421042104210421042
      1042104210421042104210421042104200000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E07FE07FE07FE07FE07F
      E07FE07FE07FE07FE07FE07FE07F000000000000000000001042000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E07FE07F00000000E07F
      E07FE07FE07FE07FE07FE07FE07F000000000000000000001042186318631863
      1863186318631863186318631863186300000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000000000000000000000000000
      00000000000000000000000000000000000000000000E07FE07F0000E07FE07F
      E07FE07FE07FE07F0000E07FE07F000000000000000000001042FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F186300000000FF7F00000000FF7F00000000
      FF7F000000000000FF7F00000000FF7F00000000000000000000000000000000
      00000000000000000000000000000000000000000000E07FE07FE07F0000E07F
      E07FE07FE07F000000000000E07F000000000000000000001042FF7FFF7FFF7F
      FF7FFF7F1863186318631863FF7F186300000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000000000000000000000000000
      00000000000000000000000000000000000000000000E07FE07FE07F00000000
      E07FE07F00000000E07FE07FE07F000000000000000000001042FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F186300000000FF7F00000000FF7F00000000
      FF7FFF7F00000000000000000000FF7F0000000000000000EF3D000000000000
      EF3D0000E07FEF3D0000000000000000000000000000E07FE07FE07FE07F0000
      E07F00000000E07FE07FE07FE07F000000000000000000001042FF7F18631863
      186318631863186318631863FF7F186300000000FF7FFF03FF03FF03FF03FF03
      FF7FFF7F0000FF7F007CFF7F1F00FF7F0000000000000000EF3DEF3DEF3DEF3D
      EF3D00000000E07F0000000000000000000000000000E07FE07FE07FE07F0000
      00000000E07FE07FE07FE07FE07F000000000000000000001042FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F186300000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0000FF7F007CFF7F1F00FF7F000000000000EF3DFF7FF75EFF7FF75E
      FF7FEF3D000000000000000000000000000000000000FF03FF03FF03FF030000
      00000000FF03FF03FF03FF03FF03000000000000000000000000104200000000
      FF7FFF7F1863186318631863FF7F186300000000FF7F0000000000000000FF7F
      FF7FFF7F0000FF7FFF7FFF7F1F00FF7F0000EF3DEF3DFF7FF75EFF7F007CFF7F
      F75EFF7FEF3DEF3D0000000000000000000000000000FF03FF030000FF03F75E
      0000000000000000FF03FF03FF030000000000000000FF03FF030000FF03FF03
      0000FF7F18631863FF7F1863FF7F186300000000FF7F0000000000000000FF7F
      FF7FFF7F0000FF7FFF7FFF7FFF7FFF7F00000000EF3DF75EFF7FF75E007CF75E
      FF7FF75EEF3D00000000000000000000000000000000FF03FF03FF030000F75E
      00000000FF03FF03FF03FF03FF030000000000000000FF03FF030000FF03FF03
      0000FF7F18631863FF7F1863FF7F186300000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000EF3DFF7F007C007C007C007C
      007CFF7FEF3D00000000000000000000000000000000FF03FF03FF03FF030000
      00000000FF03FF03FF03FF03FF030000000000000000FF030000FF03FF030000
      FF0300001863FF7FFF7F1863FF7F1863000000001F001F001F001F001F001F00
      1F001F001F001F001F001F001F001F0000000000EF3DF75EFF7FF75E007CF75E
      FF7FF75EEF3D00000000000000000000000000000000FF03FF03FF03FF03FF03
      00000000FF03FF03FF03FF03FF030000000000000000FF0300000000FF030000
      0000FF030000186318631863FF7F186300000000F75EF75E1F001F001F001F00
      1F001F001F001F001F001F00F75EF75E0000EF3DEF3DFF7FF75EFF7F007CFF7F
      F75EFF7FEF3DEF3D0000000000000000000000000000FF03FF03FF03FF03FF03
      FF0300000000FF03FF03FF03FF03000000000000FF03FF03FF030000FF030000
      FF03FF03FF030000FF7F00000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EF3DFF7FF75EFF7FF75E
      FF7FEF3D000000000000000000000000000000000000FF03FF03FF03FF03FF03
      FF0300000000FF03FF03FF03FF03000000000000000000000000000000000000
      0000000000000000FF7F1042FF7F000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000EF3DEF3DEF3DEF3D
      EF3D0000000000000000000000000000000000000000FF03FF03FF03FF03FF03
      FF03FF03FF03FF03FF03FF03FF03000000000000000000001042FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F10420000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000EF3D000000000000
      EF3D0000000000000000000000000000000000000000FF03FF03FF03FF03FF03
      FF03FF03FF03FF03FF03FF03FF03000000000000000000001042104210421042
      104210421042104210421042000000000000424D3E000000000000003E000000
      2800000040000000300000000100010000000000800100000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FFFF0000000000000001000000000000
      00010000000000003FF90000000000003FF90000000000002109000000000000
      3FF900000000000021090000000000003FF90000000000002109000000000000
      3FF9000000000000000100000000000000510000000000000001000000000000
      0001000000000000FFFF000000000000FFFFFFFDC003E0000000FFF8C003E000
      0000FFF1C003E0000000FFE3C003E0000000FFC7C003E0000000E08FC003E000
      0000C01FC003E0000000803FC00380000000001FC00380000000001FC0038000
      0000001FC00380000000001FC00380000000001FC00300000000803FC0030001
      FFFFC07FC003E003FFFFE0FFC003E00700000000000000000000000000000000
      000000000000}
  end
  object QQualifContr: TADOLinkedQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'SELECT QualificheContrattuali.ID,TipoContratto,Qualifica,Descriz' +
        'ione ContrattoNaz,'
      '       TipoContratto+'#39' - '#39'+Qualifica TipoQualif'
      
        'FROM QualificheContrattuali join TipiContratti on QualificheCont' +
        'rattuali.IDTipoContratto = TipiContratti.ID'
      
        'left join ContrattiNaz on QualificheContrattuali.IDContrattoNaz ' +
        '= ContrattiNaz.ID'
      'order by TipoContratto')
    UseFilter = False
    Left = 521
    Top = 156
    object QQualifContrID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QQualifContrTipoContratto: TStringField
      FieldName = 'TipoContratto'
    end
    object QQualifContrQualifica: TStringField
      FieldName = 'Qualifica'
      Size = 40
    end
    object QQualifContrContrattoNaz: TStringField
      FieldName = 'ContrattoNaz'
      Size = 40
    end
    object QQualifContrTipoQualif: TStringField
      FieldName = 'TipoQualif'
      ReadOnly = True
      Size = 61
    end
  end
  object DsQQualifContr: TDataSource
    DataSet = QQualifContr
    Left = 554
    Top = 156
  end
  object QEspLavRetribuz: TkbmMemTable
    DesignActivation = True
    AttachedAutoRefresh = True
    AttachMaxCount = 1
    FieldDefs = <
      item
        Name = 'ID'
        DataType = ftInteger
      end
      item
        Name = 'Voce'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'Flag'
        DataType = ftBoolean
      end
      item
        Name = 'Valore'
        DataType = ftFloat
      end
      item
        Name = 'IDColonna'
        DataType = ftInteger
      end>
    IndexDefs = <>
    SortOptions = []
    PersistentBackup = False
    ProgressFlags = [mtpcLoad, mtpcSave, mtpcCopy]
    FilterOptions = []
    Version = '3.01'
    LanguageID = 0
    SortID = 0
    SubLanguageID = 1
    LocaleID = 1024
    Left = 25
    Top = 384
    object QEspLavRetribuzID: TIntegerField
      FieldName = 'ID'
    end
    object QEspLavRetribuzVoce: TStringField
      FieldName = 'Voce'
      Size = 30
    end
    object QEspLavRetribuzFlag: TBooleanField
      FieldName = 'Flag'
    end
    object QEspLavRetribuzValore: TFloatField
      FieldName = 'Valore'
    end
    object QEspLavRetribuzIDColonna: TIntegerField
      FieldName = 'IDColonna'
    end
  end
  object QCLColloquioTemp: TkbmMemTable
    DesignActivation = True
    AttachedAutoRefresh = True
    AttachMaxCount = 1
    FieldDefs = <>
    IndexDefs = <>
    SortOptions = []
    PersistentBackup = False
    ProgressFlags = [mtpcLoad, mtpcSave, mtpcCopy]
    FilterOptions = []
    Version = '3.01'
    LanguageID = 0
    SortID = 0
    SubLanguageID = 1
    LocaleID = 1024
    Left = 316
    Top = 376
    object QCLColloquioTempID: TIntegerField
      FieldName = 'ID'
    end
    object QCLColloquioTempIDCommessaVoce: TIntegerField
      FieldName = 'IDCommessaVoce'
    end
    object QCLColloquioTempVoce: TStringField
      FieldName = 'Voce'
      Size = 100
    end
    object QCLColloquioTempFlag: TBooleanField
      FieldName = 'Flag'
    end
    object QCLColloquioTempRisposta: TStringField
      FieldName = 'Risposta'
      Size = 1024
    end
  end
  object Q2: TADOLinkedQuery
    Connection = Data.DB
    Parameters = <>
    UseFilter = False
    Left = 252
    Top = 441
  end
  object Q6: TADOQuery
    Connection = Data.DB
    Parameters = <>
    Left = 276
    Top = 357
  end
  object QRicPend: TADOQuery
    Connection = Data.DB
    Parameters = <
      item
        Name = 'xid'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT EBC_Ricerche.Progressivo, Mansioni.Descrizione as Mansion' +
        'e,'
      
        'EBC_Clienti.Descrizione as Cliente, Mansioni.Descrizione as Ruol' +
        'o,'
      'Aree.Descrizione as Area, EBC_Ricerche.TitoloCliente'
      
        'FROM EBC_Ricerche left join Aree on ebc_ricerche.IDArea = aree.I' +
        'D'
      'join Mansioni on ebc_ricerche.IDMansione = mansioni.ID'
      'join EBC_Clienti on ebc_ricerche.IDCliente = ebc_clienti.ID '
      'where EBC_Ricerche.ID = :xid')
    Left = 273
    Top = 258
    object QRicPendProgressivo: TStringField
      FieldName = 'Progressivo'
      Size = 15
    end
    object QRicPendMansione: TStringField
      FieldName = 'Mansione'
      Size = 100
    end
    object QRicPendCliente: TStringField
      FieldName = 'Cliente'
      Size = 50
    end
    object QRicPendRuolo: TStringField
      FieldName = 'Ruolo'
      Size = 100
    end
    object QRicPendArea: TStringField
      FieldName = 'Area'
      Size = 100
    end
    object QRicPendTitoloCliente: TStringField
      FieldName = 'TitoloCliente'
      Size = 50
    end
  end
  object DSQRicPend: TDataSource
    DataSet = QRicPend
    Left = 313
    Top = 258
  end
  object TSelezionatori: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      ' select distinct users.id,users.Nominativo from users '
      ' join UsersSoftwares on Users.ID=UsersSoftwares.IDUtente'
      
        'where (datarevoca='#39#39' or datarevoca is null or datarevoca>getdate' +
        '())'
      
        'and (datascadenza='#39#39' or datascadenza is null or datascadenza>get' +
        'date())'
      'and UsersSoftwares.IDSoftware=1'
      'or Users.id=9'
      'order by users.Nominativo')
    Left = 388
    Top = 368
    object TSelezionatoriID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object TSelezionatoriNominativo: TStringField
      FieldName = 'Nominativo'
      Size = 30
    end
  end
end
