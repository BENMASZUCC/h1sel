object DettAnagQuestForm: TDettAnagQuestForm
  Left = 301
  Top = 170
  Width = 815
  Height = 510
  Caption = 'Dettaglio risposte date al questionario'
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 807
    Height = 45
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object Label1: TLabel
      Left = 6
      Top = 5
      Width = 65
      Height = 13
      Caption = 'Questionario:'
    end
    object DBText1: TDBText
      Left = 78
      Top = 5
      Width = 366
      Height = 14
      DataField = 'Questionario'
      DataSource = DsQInfo
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 6
      Top = 20
      Width = 48
      Height = 13
      Caption = 'Soggetto:'
    end
    object DBText2: TDBText
      Left = 78
      Top = 21
      Width = 273
      Height = 14
      DataField = 'Soggetto'
      DataSource = DsQInfo
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object BOK: TToolbarButton97
      Left = 726
      Top = 1
      Width = 80
      Height = 43
      Caption = 'Esci'
      Glyph.Data = {
        76060000424D7606000000000000360400002800000018000000180000000100
        080000000000400200000000000000000000000100000000000000000000FFFF
        FF00FF00FF0040D86800B1AEAC00545149001D973F0076967600C4F1D1002827
        270079D2910045A35E00716D5E00CFC7C00024C04D009B958D005FBA77005CE6
        8000E4EFE70090B69900B1D9BC005C8864003D3C390076B3870034934E003ABD
        5D007C7B730054D073005F5E5E00938F7800AAA695002CD358009BC6A60062A4
        73006AC98300E0D5C70049EC7500C1BBB7004FB96A00D5EEDB00697C6D008187
        820081C091002AAF4D00B5E9C300F1F7F2009D9D9C003432310044C666005397
        60009F9B7F0037CC5E004A49430039A0550072A67F007C9B83005F7763006E6E
        6D0055A66C006BB87F00A7A6A5008B8C880030C257004ADC700086B48800668B
        6C0060B06C0077C48B00259F470092C29C00C8C2B00054C27100E7FAEC0065B0
        790085827000C5C3C20056DC790071D08A005B584E005BCA79009A939900DCF3
        E3004DCC6E0086C4960080AC8C0032B054007572710064C17D003DA95B0041B4
        6000C9E7D100A39F9200B9F1C800F8FBF900D9D1C000BBB5B50046E67000B1AE
        A20048AB630080C793008DBB91006D9276006EAF7F0064D081004EE3750043CF
        680078BB890056B66F002ABD52007FB68D00CEECD6003FC262007E7E7B006F88
        7000329F500094908800D0CCB90031CC5A00C1B9AE00489B5F00A7A49E003BD1
        6300C4BBC100679F6F0077CD8E00CCC7C60055E57A004FA66700888886005DB5
        720072C0870095BD9700ACABA8005CA76F008CC09A006360610030BD56004DD1
        700046BF66005BC37700CBC8B600BBB8B90038B7590049A85B00AFAA98008482
        7500A09A8D0073C9890033D05D00B5B2A80059B16F00D9EEDF0087BB940033C6
        5A0096C3A1006DC4850069AB7A0040DE6A0051D775005B59590049D76D009B98
        80008F8D8500ECF6EF00E5F4E90028C35100B8AFB1003FCB63005AE07E009C97
        880065CA7F007B967A0062B47300D3EAD9004949480042BA610079767500A2A1
        9F006CCD8600A2A1990070B5820074B98600D4CFBC00C4BFB9002BC756003CA4
        58004AC66B008DB68E007CC08D00BFBDBE0068BD7E003CC6610049E773003131
        2F00E0F2E500C9C1BF0045E06E004FD472006B866D0082817D0094BE9F005FBF
        790065A97700C1EDCD003BD6640053E1790063786600B1AFA700ABA9A4007CCC
        9100C9BFB40037BC5A003DB45D00657C6A009BC1A600D1EFD90051B56C007DBA
        8E007A7870009B9B9B007BB88B00D9F2DF00B8ECC60026B04B002BB34F004C9A
        61008F8F8C00E8F5EB0059C3740093C69F0062B478008CB6970085BE9300A5A4
        A20055D476009D969A0053B96E005CBE76007CC78F00DEEEE300CAEDD300D3CB
        BD00BFB6B400B7B3B20056544B00DCD3C200CFC9BC00C6BDBD0037CF5F0041DA
        6A0042A45D00ADADAC0057E17C00A9A8A800A4A0950086868300020202020202
        022E39AEC10909C1AE392E020202020202020202020202B11C2F1C507ABDBD7A
        EB872F9F2E0202020202020202028016567DE5E488757588E4E57DB016FF0202
        0202020202C734A69A3E033FCDFCFCCD3FCC3E455F34700202020202E205F7AC
        94A03F69687E1111A84CA09481C3F43D0202023C4EF2AC7579CC30E31B60C068
        CDFC4CC57596254EE902021A7383B833796FC26EA3EA24C0C43F4C9E89B8640F
        DA023C0C0D2B993319C26E6E6EA31B60C4F9039EC5A7DF0D0CE973A9BB6C998E
        C26E6ED76ED7A3529D03797989526C4092A29176428855C2AD6EDD4851D727A3
        BACC7933A752198FD2914A23358AA4ADAD51081F5CC22727A38AF8339930AF06
        234A1DB6B98BD797C2087579945CA42727A3AF9999D3AF448C1DA174FAEC47D7
        CBA533333375DEA42797A3D46CE026724632A1F5776BEDB24FBF3E999933B8DE
        E39797A35859D8185EA1A9F67B961057224D6752BF3E3E0E2CE3EF122D966231
        F1A9B346ABE6E6BE9B937C0A0A0A4DB2AAF02D12A3ADFA0746FE841ECF21493B
        8243EED1D1D17C939BC95A5DAD0BE1D0908402FEF671B5B46ABC63636363EE43
        823B81147F3AC6F6FE0202FB5BB741D9DC2A535353532ABCB53B4985851525FE
        FB0202028461CF41869886868686E8D91766CA9C15D095D00202020202D0CF5F
        CEE7209A9A86986D17173638F395D00202020202020284787D3D283713D6C854
        65D53D7D7884020202020202020202FBB1044B8D2E2929DB8D4B04B1FB020202
        020202020202020202FBB1DBB1FDFD2EDBB18402020202020202}
      OnClick = BOKClick
    end
  end
  object dxDBGrid1: TdxDBGrid
    Left = 0
    Top = 45
    Width = 807
    Height = 438
    Bands = <
      item
      end>
    DefaultLayout = True
    HeaderPanelRowCount = 1
    KeyField = 'ID'
    ShowGroupPanel = True
    ShowSummaryFooter = True
    SummaryGroups = <
      item
        DefaultGroup = False
        SummaryItems = <
          item
            ColumnName = 'dxDBGrid1Punteggio'
            SummaryField = 'Punteggio'
            SummaryType = cstSum
          end>
        Name = 'dxDBGrid1SummaryGroup2'
      end>
    SummarySeparator = ', '
    Align = alClient
    TabOrder = 1
    DataSource = DsQRisposte
    Filter.Criteria = {00000000}
    OptionsBehavior = [edgoAutoSort, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoTabThrough, edgoVertThrough]
    OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
    OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoIndicator, edgoUseBitmap]
    object dxDBGrid1ID: TdxDBGridMaskColumn
      Visible = False
      Width = 60
      BandIndex = 0
      RowIndex = 0
      FieldName = 'ID'
    end
    object dxDBGrid1Raggruppamento: TdxDBGridMaskColumn
      Width = 114
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Raggruppamento'
      SummaryType = cstSum
      SummaryField = 'Punteggio'
      SummaryFormat = '- Totale punteggio = #,###'
    end
    object dxDBGrid1Sezione: TdxDBGridMaskColumn
      Width = 146
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Sezione'
    end
    object dxDBGrid1Rif: TdxDBGridMaskColumn
      Width = 26
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Rif'
    end
    object dxDBGrid1Domanda: TdxDBGridMaskColumn
      Width = 259
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Domanda'
    end
    object dxDBGrid1Risposta: TdxDBGridMaskColumn
      Width = 144
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Risposta'
    end
    object dxDBGrid1Peso: TdxDBGridMaskColumn
      Width = 37
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Peso'
    end
    object dxDBGrid1Punteggio: TdxDBGridMaskColumn
      Width = 65
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Punteggio'
      SummaryFooterType = cstSum
      SummaryFooterField = 'Punteggio'
    end
  end
  object DsQRisposte: TDataSource
    DataSet = QRisposte
    Left = 28
    Top = 359
  end
  object QRisposte: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'x'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select AR.ID, RAGG.Raggruppamento, SEZ.Gruppo Sezione,'
      
        #9'D.Rif, D.TestoDomanda Domanda, D.Peso, R.TestoRisposta Risposta' +
        ', R.Punteggio'
      'from QUEST_AnagRisposte AR'
      'join QUEST_Domande D on AR.IDDomanda = D.ID'
      'join QUEST_Risposte R on AR.IDRisposta = R.ID'
      'join QUEST_GruppiDomande SEZ on D.IDGruppo = SEZ.ID'
      
        'left outer join QUEST_Raggruppamenti RAGG on D.IDRaggruppamento ' +
        '= RAGG.ID'
      'where AR.IDAnagQuest = :x'
      'order by Raggruppamento')
    Left = 28
    Top = 325
    object QRisposteID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QRisposteRaggruppamento: TStringField
      FieldName = 'Raggruppamento'
      Size = 100
    end
    object QRisposteSezione: TStringField
      FieldName = 'Sezione'
      Size = 100
    end
    object QRisposteRif: TStringField
      FieldName = 'Rif'
      Size = 5
    end
    object QRisposteDomanda: TStringField
      FieldName = 'Domanda'
      Size = 1024
    end
    object QRispostePeso: TIntegerField
      FieldName = 'Peso'
    end
    object QRisposteRisposta: TStringField
      FieldName = 'Risposta'
      Size = 1024
    end
    object QRispostePunteggio: TFloatField
      FieldName = 'Punteggio'
    end
  end
  object QInfo: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'x'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select A.Cognome+'#39' '#39'+A.Nome Soggetto, Q.TipoDomanda Questionario'
      'from QUEST_AnagQuest AQ'
      'join Anagrafica A on AQ.IDAnagCompilatore = A.ID'
      'join QUEST_TipiDomande Q on AQ.IDQuest = Q.ID'
      'where AQ.ID = :x')
    Left = 560
    Top = 8
    object QInfoSoggetto: TStringField
      FieldName = 'Soggetto'
      ReadOnly = True
      Size = 61
    end
    object QInfoQuestionario: TStringField
      FieldName = 'Questionario'
      Size = 50
    end
  end
  object DsQInfo: TDataSource
    DataSet = QInfo
    Left = 592
    Top = 8
  end
end
