unit uAbout;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ExtCtrls, StdCtrls, ShellAPI, JvLabel, {JvHotLink, }jpeg{, EnBmpGr};

type
     TAboutForm = class(TForm)
          Label1: TLabel;
          Panel1: TPanel;
          Label8: TLabel;
          Label9: TLabel;
          Memo1: TMemo;
          Memo2: TMemo;
          Panel2: TPanel;
          Image2: TImage;
          Label11: TLabel;
          Label12: TLabel;
          Label13: TLabel;
          Panel3: TPanel;
          Label2: TLabel;
          Label3: TLabel;
          Label4: TLabel;
          Label5: TLabel;
          Label6: TLabel;
          Label7: TLabel;
          Label10: TLabel;
          Edit1: TEdit;
          Edit2: TEdit;
          Edit3: TEdit;
          Edit4: TEdit;
          Edit5: TEdit;
          Edit6: TEdit;
          Edit7: TEdit;
//          JvHotLink1: TJvHotLink;
          Panel4: TPanel;
          Image1: TImage;
          Label14: TLabel;
          LDistributore: TLabel;
          LRiga1: TLabel;
          LRiga2: TLabel;
          LRiga3: TLabel;
          LRiga4: TLabel;
          LRiga5: TLabel;
//          HLLink: TJvHotLink;
          EVersione: TEdit;
          procedure FormShow(Sender: TObject);
          procedure URLLabel1Click(Sender: TObject);
          procedure URLLabel2Click(Sender: TObject);
     private
          { Private declarations }
     public
          procedure ParsaConfigFile;
     end;

var
     AboutForm: TAboutForm;

implementation

uses ModuloDati;



{$R *.DFM}

procedure TAboutForm.FormShow(Sender: TObject);
begin
     ParsaConfigFile;
end;

procedure TAboutForm.ParsaConfigFile;
var xConfigString, xLista: TStringList;
     xS: string;
     i, xItem: integer;
begin
     xConfigString:=TStringList.create;
     xConfigString.LoadFromFile(ExtractFileDir(Application.ExeName) + '\ConfigFile.txt');
     // eliminazione righe vuote
     if xConfigString.strings[1] = '' then begin
          for i:=0 to xConfigString.count - 1 do begin
               if i <= xConfigString.count then
                    if xConfigString.strings[i] = '' then xConfigString.Delete(i);
          end;
     end;
     Edit7.Text:= ' ' + copy(xConfigString.strings[1], pos('=', xConfigString.strings[1]) + 1, length(xConfigString.strings[1]));
     Edit1.Text:= ' ' + copy(xConfigString.strings[2], pos('=', xConfigString.strings[2]) + 1, length(xConfigString.strings[2]));
     Edit2.Text:= ' ' + copy(xConfigString.strings[3], pos('=', xConfigString.strings[3]) + 1, length(xConfigString.strings[3]));
     if copy(xConfigString.strings[4], pos('=', xConfigString.strings[4]) + 1, length(xConfigString.strings[4])) = '' then
          Edit3.Text:= ' nessuna'
     else Edit3.Text:= ' ' + copy(xConfigString.strings[4], pos('=', xConfigString.strings[4]) + 1, length(xConfigString.strings[4]));
     if copy(xConfigString.strings[5], pos('=', xConfigString.strings[5]) + 1, length(xConfigString.strings[5])) = '0' then
          Edit4.Text:= ' illimitati'
     else Edit4.Text:= ' ' + copy(xConfigString.strings[5], pos('=', xConfigString.strings[5]) + 1, length(xConfigString.strings[5]));
     if copy(xConfigString.strings[6], pos('=', xConfigString.strings[6]) + 1, length(xConfigString.strings[6])) = '0' then
          Edit5.Text:= ' illimitati'
     else Edit5.Text:= ' ' + copy(xConfigString.strings[6], pos('=', xConfigString.strings[6]) + 1, length(xConfigString.strings[6]));
     if copy(xConfigString.strings[7], pos('=', xConfigString.strings[7]) + 1, length(xConfigString.strings[7])) = '' then
          Edit6.Text:= ' nessuna'
     else Edit6.Text:= ' ' + copy(xConfigString.strings[7], pos('=', xConfigString.strings[7]) + 1, length(xConfigString.strings[7]));
     // elenco funzioni
     xLista:=TStringList.create;
     xS:=xConfigString.strings[8];
     while xS <> '' do begin
          Data.QTemp.Close;
          Data.QTemp.SQL.text:= 'select Funzione from SAC_ModuliFunzioni where CodModulo=' + copy(xS, 1, pos(';', xS) - 1);
          Data.QTemp.Open;
          if not Data.QTemp.IsEmpty then
               xLista.Add(Data.QTemp.FieldByName('Funzione').asString)
          else xLista.Add(copy(xS, 1, pos(';', xS) - 1));
          xS:=copy(xS, pos(';', xS) + 1, length(xS))
     end;
     Data.QTemp.Close;
     Memo1.text:=xLista.text;
     // elenco client
     xLista.Clear;
     xS:=xConfigString.strings[9];
     while xS <> '' do begin
          xLista.Add(copy(xS, 1, pos(';', xS) - 1));
          xS:=copy(xS, pos(';', xS) + 1, length(xS))
     end;
     Memo2.text:=xLista.text;

     if xConfigString.Count > 12 then begin
          // 10 = versione
          EVersione.Text:= ' ' + xConfigString.strings[10];

          // 11 = Distributore
          xS:=xConfigString.strings[11];
          xItem:=1;
          while xS <> '' do begin
               case xItem of
                    1: LDistributore.Caption:=copy(xS, 1, pos(';', xS) - 1);
                    2: LRiga1.Caption:=copy(xS, 1, pos(';', xS) - 1);
                    3: LRiga2.Caption:=copy(xS, 1, pos(';', xS) - 1);
                    4: LRiga3.Caption:=copy(xS, 1, pos(';', xS) - 1);
                    5: LRiga4.Caption:=copy(xS, 1, pos(';', xS) - 1);
                    6: LRiga5.Caption:=copy(xS, 1, pos(';', xS) - 1);
//                    7: begin HLLink.Caption:=copy(xS, 1, pos(';', xS) - 1); HLLink.url:=copy(xS, 1, pos(';', xS) - 1); end;
                    8: begin
                              if FileExists(ExtractFileDir(Application.ExeName) + '\' + copy(xS, 1, pos(';', xS) - 1)) then
                                   Image1.Picture.LoadFromFile(ExtractFileDir(Application.ExeName) + '\' + copy(xS, 1, pos(';', xS) - 1));
                         end;
               end;
               xS:=copy(xS, pos(';', xS) + 1, length(xS));
               inc(xItem);
          end;
     end else EVersione.Text:='Versione base';

     xLista.Free;
     xConfigString.Free;
end;

procedure TAboutForm.URLLabel1Click(Sender: TObject);
begin
     ShellExecute(0, 'Open', pchar('www.ebcconsulting.com'), '', '', SW_SHOW);
end;

procedure TAboutForm.URLLabel2Click(Sender: TObject);
begin
     ShellExecute(0, 'Open', pchar('mailto:softwareh1@ebcconsulting.com'), '', '', SW_SHOW);
end;

end.

