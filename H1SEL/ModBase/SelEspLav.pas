unit SelEspLav;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     dxTL, dxDBCtrl, dxDBGrid, dxCntner, Db, ADODB, StdCtrls, Buttons,
     ExtCtrls, U_ADOLinkCl;

type
     TSelEspLavForm = class(TForm)
          Panel1: TPanel;
          BitBtn1: TBitBtn;
          BitBtn2: TBitBtn;
          DsQEspLav: TDataSource;
          QEspLav: TADOQuery;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1Azienda: TdxDBGridMaskColumn;
          dxDBGrid1Attivita: TdxDBGridMaskColumn;
          dxDBGrid1AnnoDal: TdxDBGridMaskColumn;
          dxDBGrid1MeseDal: TdxDBGridMaskColumn;
          dxDBGrid1Ruolo: TdxDBGridMaskColumn;
          QEspLavAzienda: TStringField;
          QEspLavAttivita: TStringField;
          QEspLavID: TAutoIncField;
          QEspLavIDMansione: TIntegerField;
          QEspLavAnnoDal: TSmallintField;
          QEspLavMeseDal: TSmallintField;
          QEspLavRuolo: TStringField;
          BNuova: TBitBtn;
          Q: TADOLinkedQuery;
          procedure BNuovaClick(Sender: TObject);
     private
          { Private declarations }
     public
          xIDAnagrafica: integer;
     end;

var
     SelEspLavForm: TSelEspLavForm;

implementation

uses ModuloDati, SelCliente;

{$R *.DFM}

procedure TSelEspLavForm.BNuovaClick(Sender: TObject);
begin
     SelClienteForm := TSelClienteForm.create(self);
     SelClienteForm.Caption := 'Selezione azienda per nuova esp.lav. attuale';
     SelClienteForm.ShowModal;
     if SelClienteForm.ModalResult = mrOK then begin
          Q.Close;
          // togli flag di ATTUALE su tutte le esperienze lav. per il soggetto
          Q.SQL.text := 'update EsperienzeLavorative set Attuale=0 where IDAnagrafica=' + IntToStr(xIDAnagrafica);
          Q.ExecSQL;
          // inserimento nuova esperienza lavorativa attuale per il soggetto
          Q.SQL.text := 'insert into EsperienzeLavorative (IDAnagrafica,IDAzienda,IDSettore,Attuale) ' +
               'values (:xIDAnagrafica:,:xIDAzienda:,:xIDSettore:,:xAttuale:)';
          Q.ParambyName['xIDAnagrafica'] := xIDAnagrafica;
          Q.ParambyName['xIDAzienda'] := SelClienteForm.TClienti.FieldbyName('ID').asInteger;
          Q.ParambyName['xIDSettore'] := SelClienteForm.TClienti.FieldbyName('IDAttivita').asInteger;
          Q.ParambyName['xAttuale'] := 0;
          Q.ExecSQL;
          // mettiti sull'ultima inserita
          QEspLav.Close;
          QEspLav.Open;
          QEspLav.Last;
     end;
     SelClienteForm.Free;
end;

end.
