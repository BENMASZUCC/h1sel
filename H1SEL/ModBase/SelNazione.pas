unit SelNazione;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, ExtCtrls, Grids, DBGrids, Db, DBTables, Buttons, ADODB,
     U_ADOLinkCl, dxTL, dxDBCtrl, dxDBGrid, dxCntner, TB97;

type
     TSelNazioneForm = class(TForm)
          QNazioni_OLD: TQuery;
          DsQNazioni: TDataSource;
          QNazioni_OLDID: TAutoIncField;
          QNazioni_OLDAbbrev: TStringField;
          QNazioni_OLDDescNazione: TStringField;
          QNazioni_OLDAreaGeografica: TStringField;
          Panel1: TPanel;
          ECerca: TEdit;
          Label1: TLabel;
          QNazioni: TADOLinkedQuery;
          PanCitta: TPanel;
          Label2: TLabel;
          ECitta: TEdit;
          DBGrid1: TDBGrid;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1Column1: TdxDBGridColumn;
          dxDBGrid1Column2: TdxDBGridColumn;
          dxDBGrid1Column3: TdxDBGridColumn;
          dxDBGrid1Column4: TdxDBGridColumn;
          dxDBGrid1Column5: TdxDBGridColumn;
          QNazioniID: TAutoIncField;
          QNazioniAbbrev: TStringField;
          QNazioniDescNazione: TStringField;
          QNazioniAreaGeografica: TStringField;
          QNazioniDescNazionalita: TStringField;
          Panel2: TPanel;
          BOK: TToolbarButton97;
          BAnnulla: TToolbarButton97;
          procedure ECercaChange(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure BOKClick(Sender: TObject);
          procedure BAnnullaClick(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     SelNazioneForm: TSelNazioneForm;

implementation
uses ModuloDati, Main;
{$R *.DFM}

procedure TSelNazioneForm.ECercaChange(Sender: TObject);
begin
     QNazioni.Close;
     //[/TONI20021809\]
     QNazioni.ReloadSQL;
     //[/TONI20021809\]FINE
     QNazioni.ParamByName['xDesc'] := ECerca.text + '%';
     QNazioni.Open;
end;

procedure TSelNazioneForm.FormShow(Sender: TObject);
begin
//Grafica

     SelNazioneForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     PanCitta.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;

     Caption := '[S/2] - ' + Caption;

     QNazioni.Close;
     QNazioni.ReloadSQL;
     QNazioni.Open;

     ECErca.setFocus;
end;

procedure TSelNazioneForm.BOKClick(Sender: TObject);
begin
     SelNazioneForm.ModalResult := mrOK;
end;

procedure TSelNazioneForm.BAnnullaClick(Sender: TObject);
begin
     SelNazioneForm.ModalResult := mrCancel;
end;

end.

