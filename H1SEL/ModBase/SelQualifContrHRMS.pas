//TONI OK 30/4
unit SelQualifContrHRMS;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, Grids, DBGrids, Db, DBTables, ExtCtrls, ADODB;

type
     TSelQualifContrHRMSForm = class(TForm)
          dsQQualif: TDataSource;
          DBGrid1: TDBGrid;
          Panel1: TPanel;
          Panel2: TPanel;
          BitBtn3: TBitBtn;
          Panel3: TPanel;
          BitBtn1: TBitBtn;
          BitBtn2: TBitBtn;
          Panel4: TPanel;
          Image1: TImage;
          QQualif: TADOQuery;
          QQualifID: TAutoIncField;
          QQualifTipoContratto: TStringField;
          QQualifQualifica: TStringField;
          QQualifIDContrattoNaz: TIntegerField;
          procedure BitBtn3Click(Sender: TObject);
     private
    { Private declarations }
     public
    { Public declarations }
     end;

var
     SelQualifContrHRMSForm: TSelQualifContrHRMSForm;

implementation

uses ModuloDati, TabContratti;

{$R *.DFM}

procedure TSelQualifContrHRMSForm.BitBtn3Click(Sender: TObject);
begin
     TabContrattiForm := TTabContrattiForm.create(self);
     TabContrattiForm.ShowModal;
     TabContrattiForm.Free;
     QQualif.Close;
     QQualif.Open;
end;

end.

