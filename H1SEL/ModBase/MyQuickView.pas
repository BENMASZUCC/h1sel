unit MyQuickView;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ComCtrls, ExtCtrls, Db, ADODB, ImgList, Menus, dxCntner, dxTL, StdCtrls,
     dxTLClms, TB97, JvExStdCtrls, JvCheckBox;

type
     TMyQuickViewForm = class(TForm)
          Panel1: TPanel;
          QItems: TADOQuery;
          QSubItems: TADOQuery;
          ImageList1: TImageList;
          PMUnico: TPopupMenu;
          PMvoceIDRicerca: TMenuItem;
          PMVoceIDAnagrafica: TMenuItem;
          PMVoceIDCandRic: TMenuItem;
          Q: TADOQuery;
          dxTreeList: TdxTreeList;
          dxDicitura: TdxTreeListColumn;
          dxID: TdxTreeListSpinColumn;
          dxItemsCount: TdxTreeListSpinColumn;
          BConfig: TToolbarButton97;
          dxIDRicerca: TdxTreeListSpinColumn;
          dxIDAnagrafica: TdxTreeListSpinColumn;
          dxIDCandRic: TdxTreeListSpinColumn;
          dxIDAgenda: TdxTreeListSpinColumn;
          dxIDCliente: TdxTreeListSpinColumn;
          PMVoceIDAgenda: TMenuItem;
          PMVoceIDCliente: TMenuItem;
          QItemsID: TAutoIncField;
          QItemsDicNodoMaster: TStringField;
          QItemsImageIndex: TSmallintField;
          QItemsStringaSQL: TStringField;
          QItemsSmallIcon: TBlobField;
          dxIcona: TdxTreeListGraphicColumn;
          Splitter1: TSplitter;
          dxTreeListQM: TdxTreeList;
          dxQMDicitura: TdxTreeListColumn;
          dxQMID: TdxTreeListSpinColumn;
          dxTQMItemsCount: TdxTreeListSpinColumn;
          dxQMIDRicerca: TdxTreeListSpinColumn;
          dxQMIDAnagrafica: TdxTreeListSpinColumn;
          dxQMIDCandRic: TdxTreeListSpinColumn;
          Panel2: TPanel;
          QItemsQM: TADOQuery;
          QSubItemsQM: TADOQuery;
          PMQM: TPopupMenu;
          PMQMCommessa: TMenuItem;
          PMQMCandidato: TMenuItem;
          N1: TMenuItem;
          PMQMDett: TMenuItem;
          dxQMCodice: TdxTreeListColumn;
          PMQMCandCommessa: TMenuItem;
          Label1: TLabel;
          QFigli: TADOQuery;
          QFigliID: TAutoIncField;
          QFigliIDModel: TIntegerField;
          QFigliDicitura: TStringField;
          QFigliStringaSQL: TMemoField;
          QQueryFiglio: TADOQuery;
          QItemsOnlyFirstLevel: TBooleanField;
          QOnlyFirstLevel: TADOQuery;
          CBMyQuickView: TJvCheckBox;
          CBMyQuickViewfigli: TJvCheckBox;
          QUsersParam: TADOQuery;
          QUsersParammyquickview_sel: TBooleanField;
          QUsersParammyquickviewfigli_sel: TBooleanField;
          procedure PMUnicoPopup(Sender: TObject);
          procedure PMvoceIDRicercaClick(Sender: TObject);
          procedure PMVoceIDAnagraficaClick(Sender: TObject);
          procedure dxTreeListCustomDrawCell(Sender: TObject; ACanvas: TCanvas;
               ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
               ASelected, AFocused, ANewItemRow: Boolean; var AText: string;
               var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
               var ADone: Boolean);
          procedure BConfigClick(Sender: TObject);
          procedure PMVoceIDCandRicClick(Sender: TObject);
          procedure PMVoceIDAgendaClick(Sender: TObject);
          procedure PMVoceIDClienteClick(Sender: TObject);
          procedure PMQMCommessaClick(Sender: TObject);
          procedure PMQMCandidatoClick(Sender: TObject);
          procedure PMQMPopup(Sender: TObject);
          procedure dxTreeListQMCustomDrawCell(Sender: TObject; ACanvas: TCanvas;
               ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
               ASelected, AFocused, ANewItemRow: Boolean; var AText: string;
               var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
               var ADone: Boolean);
          procedure PMQMDettClick(Sender: TObject);
          procedure PMQMCandCommessaClick(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
          procedure CBMyQuickViewfigliClick(Sender: TObject);
     private
          xIDNodo: integer;
     public
          procedure CreaAlbero;
          procedure CreaAlberoQM;
     end;

var
     MyQuickViewForm: TMyQuickViewForm;

implementation

uses ModuloDati, Main, MDRicerche, uGestEventi, ModuloDati2, MyQuickViewConfig,
     QMDettaglio;

{$R *.DFM}

{ TMyQuickViewForm }

procedure TMyQuickViewForm.CreaAlbero;
var Item, SubItem, SubSubItem: TdxTreeListNode;
     xBitMap: TBitMap;
     xImage: TImage;
     xQueryFigli, xValoreFiglio: string;
begin
     // creazione nuova dxTreeList
     dxTreeList.ClearNodes;
     dxTreeList.BeginUpdate;

     QItems.Close;
     QItems.Parameters[0].Value := MainForm.xIDUtenteAttuale;
     QItems.Open;
     // caricamento iconcine nella ImageList
     // ### NON SO COME FARE !!!
     {ImageList1.Clear;
     while not QItems.EOF do begin
         ImageList1.Add(QItemsSmallIcon.AsVariant,nil);
         QItems.Next;
     end;}
     // caricamento nodo
     QItems.First;
     while not QItems.EOF do begin
          // nodo master - POINTER = 0
          Item := dxTreeList.Add;
          Item.Strings[0] := QItems.FieldByName('DicNodoMaster').asString;
          // caricamento iconcina
          // ### PER ORA TIENE LA IMAGELIST
          // Item.Values[0]:=QItems.FieldByName('SmallIcon').AsVariant;

          // iconcina
          Item.ImageIndex := QItems.FieldByName('ImageIndex').asInteger;
          Item.SelectedIndex := QItems.FieldByName('ImageIndex').asInteger;
          Item.StateIndex := QItems.FieldByName('ImageIndex').asInteger;


          if QItemsOnlyFirstLevel.Value then begin
               QOnlyFirstLevel.Close;
               QOnlyFirstLevel.SQL.Clear;
               QOnlyFirstLevel.SQL.text := QItems.FieldByName('StringaSQL').asString;
               //showmessage(QItems.FieldByName('StringaSQL').asString);
               QOnlyFirstLevel.Open;

               Item.Values[2] := QOnlyFirstLevel.Fields[0].Value;

          end else begin

               // eventuali figli (che saranno al 3� livello)
               if CBMyQuickViewfigli.Checked then begin
                    QFigli.Close;
                    QFigli.Parameters[0].value := QItemsID.Value;
                    QFigli.Open;
               end;

               // sottonodi
               QSubItems.Close;
               QSubItems.SQL.text := QItems.FieldByName('StringaSQL').asString;
             // showmessage( QItems.FieldByName('DicNodoMaster').asString);
               // parametri fissi (se ci sono): 1� parametro = IDUtente; 2� parametro = Data (oggi)
               if QSubItems.Parameters.Count > 0 then begin
                    QSubItems.Parameters[0].Value := MainForm.xIDUtenteAttuale;
                    if QSubItems.Parameters.Count > 1 then
                         QSubItems.Parameters[1].Value := Date;
               end;
               QSubItems.Open;
               // numero (conta)
               Item.Values[2] := QSubItems.RecordCount;
               while not QSubItems.EOF do begin
                    // nodi figli - POINTER = ID dell'entit�
                    SubItem := Item.AddChild;
                    // caricamento iconcina
                    //SubItem.Values[0]:=QItems.FieldByName('SmallIcon').AsVariant;
                    SubItem.Strings[0] := QSubItems.FieldByName('Dicitura').asString;

                    // mette gli ID che trova nei campi giusti della TreeList
                    if QSubItems.FindField('IDRicerca') <> nil then
                         SubItem.Values[3] := QSubItems.FieldByName('IDRicerca').asInteger
                    else SubItem.Values[3] := 0;
                    if QSubItems.FindField('IDAnagrafica') <> nil then
                         SubItem.Values[4] := QSubItems.FieldByName('IDAnagrafica').asInteger
                    else SubItem.Values[4] := 0;
                    if QSubItems.FindField('IDCandRic') <> nil then
                         SubItem.Values[5] := QSubItems.FieldByName('IDCandRic').asInteger
                    else SubItem.Values[5] := 0;
                    if QSubItems.FindField('IDAgenda') <> nil then
                         SubItem.Values[6] := QSubItems.FieldByName('IDAgenda').asInteger
                    else SubItem.Values[6] := 0;
                    if QSubItems.FindField('IDCliente') <> nil then
                         SubItem.Values[7] := QSubItems.FieldByName('IDCliente').asInteger
                    else SubItem.Values[7] := 0;

                    SubItem.ImageIndex := QItems.FieldByName('ImageIndex').asInteger;
                    SubItem.SelectedIndex := QItems.FieldByName('ImageIndex').asInteger;
                    SubItem.StateIndex := QItems.FieldByName('ImageIndex').asInteger;

                    // eventuali figli
                    if CBMyQuickViewfigli.Checked then
                         QFigli.first;
                    while not QFigli.EOF do begin
                         QQueryFiglio.Close;
                         QQueryFiglio.SQL.clear;
                         QQueryFiglio.SQL.text := QFigliStringaSQL.value;
                         // parametro unico > corrisponde al primo campo della query a secondo livello
                         if QQueryFiglio.Parameters.count > 0 then
                              QQueryFiglio.Parameters[0].value := QSubItems.Fields[0].Value;
                         QQueryFiglio.Open;
                         if not QQueryFiglio.IsEmpty then begin

                              xValoreFiglio := QQueryFiglio.FieldByName('Valore').asString;

                              SubSubItem := SubItem.AddChild;
                              SubSubItem.Strings[0] := QFigliDicitura.Value + ': ' + xValoreFiglio;

                              // iconcina = puntino
                              SubSubItem.ImageIndex := 9;
                              SubSubItem.SelectedIndex := 9;
                              SubSubItem.StateIndex := 9;
                         end;
                         QFigli.Next;
                    end;

                    QSubItems.Next;
               end;
          end;

          QItems.Next;
     end;
     dxTreeList.EndUpdate;
     dxTreeList.FullCollapse;
end;

procedure TMyQuickViewForm.CreaAlberoQM;
var Item, SubItem: TdxTreeListNode;
     xBitMap: TBitMap;
     xImage: TImage;
     xTipo: string;
begin
     // creazione nuova dxTreeList
     dxTreeListQM.ClearNodes;
     dxTreeListQM.BeginUpdate;

     QItemsQM.Close;
     QItemsQM.Parameters[0].Value := MainForm.xIDUtenteAttuale;
     QItemsQM.Parameters[1].Value := MainForm.xIDUtenteAttuale;
     QItemsQM.Parameters[2].Value := MainForm.xIDUtenteAttuale;
     QItemsQM.Open;
     // caricamento nodo
     QItemsQM.First;
     while not QItemsQM.EOF do begin
          // nodo master - POINTER = 0
          Item := dxTreeListQM.Add;
          if QItemsQM.FieldByName('Stato').asInteger = 1 then begin
               Item.Strings[0] := 'Allarmi';
               Item.ImageIndex := 6;
               Item.SelectedIndex := 6;
               Item.StateIndex := 6;
          end;
          if QItemsQM.FieldByName('Stato').asInteger = 2 then begin
               Item.Strings[0] := 'Non Conformit�';
               Item.ImageIndex := 7;
               Item.SelectedIndex := 7;
               Item.StateIndex := 7;
          end;
          if QItemsQM.FieldByName('Stato').asInteger = 3 then begin
               Item.Strings[0] := 'Sospesi';
               Item.ImageIndex := 8;
               Item.SelectedIndex := 8;
               Item.StateIndex := 8;
          end;
          Item.Values[2] := QItemsQM.FieldByName('Tot').asInteger;

          QSubItemsQM.Close;
          QSubItemsQM.Parameters[0].Value := QItemsQM.FieldByName('Stato').asInteger;
          QSubItemsQM.Parameters[1].Value := MainForm.xIDUtenteAttuale;
          QSubItemsQM.Open;
          while not QSubItemsQM.EOF do begin
               // nodi figli - POINTER = ID dell'entit�
               SubItem := Item.AddChild;

               // mette gli ID che trova nei campi giusti della TreeList
               if QSubItemsQM.FieldByName('IDAnagrafica').AsString <> '' then begin
                    SubItem.Strings[0] := QSubItemsQM.FieldByName('Codice').AsString + ' - ' + QSubItemsQM.FieldByName('Candidato').AsString + ' (' + QSubItemsQM.FieldByName('Valore').AsString + ')';
                    SubItem.Values[4] := QSubItemsQM.FieldByName('IDAnagrafica').asInteger
               end else SubItem.Values[4] := 0;
               if QSubItemsQM.FieldByName('IDRicerca').AsString <> '' then begin
                    SubItem.Strings[0] := QSubItemsQM.FieldByName('Codice').AsString + ' - commessa rif. ' + QSubItemsQM.FieldByName('RifCommessa').AsString + ' (' + QSubItemsQM.FieldByName('Valore').AsString + ')';
                    SubItem.Values[3] := QSubItemsQM.FieldByName('IDRicerca').asInteger
               end else SubItem.Values[3] := 0;
               if QSubItemsQM.FieldByName('IDCandRic').AsString <> '' then begin
                    // prendi dettagli candidato+commessa
                    Q.Close;
                    Q.SQL.text := 'select Progressivo Rif, Cognome+'' ''+substring(Nome,1,1)+''.'' Candidato from EBC_CandidatiRicerche, EBC_Ricerche, Anagrafica ' +
                         'where EBC_CandidatiRicerche.IDRicerca=EBC_Ricerche.ID and EBC_CandidatiRicerche.IDAnagrafica=Anagrafica.ID ' +
                         'and EBC_CandidatiRicerche.ID=' + QSubItemsQM.FieldByName('IDCandRic').AsString;
                    Q.Open;
                    SubItem.Strings[0] := QSubItemsQM.FieldByName('Codice').AsString + ' - ' + Q.FieldByName('Candidato').asString + ' nella commessa ' + Q.FieldByName('Rif').asString + ' (' + QSubItemsQM.FieldByName('Valore').AsString + ')';
                    Q.Close;
                    SubItem.Values[5] := QSubItemsQM.FieldByName('IDCandRic').asInteger
               end else SubItem.Values[5] := 0;

               // codice query
               SubItem.Values[6] := QSubItemsQM.FieldByName('Codice').AsString;

               SubItem.ImageIndex := Item.ImageIndex;
               SubItem.SelectedIndex := Item.SelectedIndex;
               SubItem.StateIndex := Item.StateIndex;

               QSubItemsQM.Next;

          end;
          QItemsQM.Next;
     end;
     dxTreeListQM.EndUpdate;
     dxTreeListQM.FullCollapse;
end;

procedure TMyQuickViewForm.PMUnicoPopup(Sender: TObject);
begin
     PMvoceIDRicerca.Visible := False;
     PMVoceIDAnagrafica.Visible := False;
     PMVoceIDCandRic.Visible := False;
     PMVoceIDAgenda.Visible := False;
     PMVoceIDCliente.Visible := False;
     if dxTreeList.FocusedNode.Level = 1 then begin
          // a seconda di quale ID � valorizzato, rendi visibile la relativa voce
          if dxTreeList.FocusedNode.Values[3] > 0 then
               PMvoceIDRicerca.Visible := True;
          if dxTreeList.FocusedNode.Values[4] > 0 then
               PMVoceIDAnagrafica.Visible := True;
          if dxTreeList.FocusedNode.Values[5] > 0 then
               PMVoceIDCandRic.Visible := True;
          if dxTreeList.FocusedNode.Values[6] > 0 then
               PMVoceIDAgenda.Visible := True;
          if dxTreeList.FocusedNode.Values[7] > 0 then
               PMVoceIDCliente.Visible := True;
     end;
end;

procedure TMyQuickViewForm.PMvoceIDRicercaClick(Sender: TObject);
begin
     // apertura commessa
     MainForm.EseguiQRicAttive;
     DataRicerche.QRicAttive.Locate('ID', dxTreeList.FocusedNode.Values[3], []);
     MainForm.BRiprendiRicClick(self);
     // ricreazione albero (in caso di modifiche)
     CreaAlbero;
end;

procedure TMyQuickViewForm.PMVoceIDAnagraficaClick(Sender: TObject);
begin
     // apertura scheda candidato
     MainForm.LOBSchedeClick(self);
     // prendi dati anagrafici
     Q.Close;
     Q.SQL.text := 'select ID,Cognome,Nome from Anagrafica ' +
          'where ID=' + IntToStr(dxTreeList.FocusedNode.Values[4]);
     Q.Open;
     MainForm.Edit1.Text := Q.FieldByName('Cognome').asString;
     MainForm.ENome.Text := Q.FieldByName('Nome').asString;
     MainForm.BAnagSearchClick(self);
     Data.TAnagrafica.Locate('ID', Q.FieldByName('ID').asInteger, []);
     // ricreazione albero (in caso di modifiche)
     CreaAlbero;
end;

procedure TMyQuickViewForm.dxTreeListCustomDrawCell(Sender: TObject;
     ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
     AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
     var AText: string; var AColor: TColor; AFont: TFont;
     var AAlignment: TAlignment; var ADone: Boolean);
begin
     if ANode.Strings[2] <> '' then begin
          if (ANode.Values[2] > 0) and (AColumn = dxDicitura) then
               AFont.style := [fsBold]
     end;
end;

procedure TMyQuickViewForm.BConfigClick(Sender: TObject);
begin
     MyQuickViewConfigForm := TMyQuickViewConfigForm.create(self);
     MyQuickViewConfigForm.xIDUtente := MainForm.xIDUtenteAttuale;
     MyQuickViewConfigForm.ShowModal;
     MyQuickViewConfigForm.Free;
     CreaAlbero;
end;

procedure TMyQuickViewForm.PMVoceIDCandRicClick(Sender: TObject);
var xIDevento, xIDaStato, xIDTipoStatoA, xIDAnag, xIDRic, xIDcliente: integer;
     xCognome, xNome: string;
begin
     // Gestione colloquio
     // prendi dati evento COLLOQUIO
     Q.Close;
     Q.SQL.text := 'select * from EBC_Eventi where evento like ''%colloquio%''';
     Q.Open;
     xIDevento := Q.FieldByName('ID').asInteger;
     xIDaStato := Q.FieldByName('IDAStato').asInteger;
     Q.Close;
     Q.SQL.text := 'select IDTipoStato from EBC_Stati where ID=' + IntToStr(xIDaStato);
     Q.Open;
     xIDTipoStatoA := Q.FieldByName('IDTipoStato').asInteger;
     // prendi dati anagrafici
     Q.Close;
     Q.SQL.text := 'select ID,Cognome,Nome from Anagrafica ' +
          'where ID=(select distinct IDAnagrafica from EBC_CandidatiRicerche where ID=' + IntToStr(dxTreeList.FocusedNode.Values[5]) + ')';
     Q.Open;
     xIDAnag := Q.FieldByName('ID').asInteger;
     xCognome := Q.FieldByName('Cognome').asString;
     xNome := Q.FieldByName('Nome').asString;
     // prendi IDRicerca e IDCliente
     Q.Close;
     Q.SQL.text := 'select distinct IDRicerca,IDCliente from EBC_CandidatiRicerche, EBC_Ricerche ' +
          'where EBC_CandidatiRicerche.IDRicerca=EBC_Ricerche.ID ' +
          'and EBC_CandidatiRicerche.ID=' + IntToStr(dxTreeList.FocusedNode.Values[5]);
     Q.Open;
     xIDRic := Q.FieldByName('IDRicerca').asInteger;
     xIDcliente := Q.FieldByName('IDCliente').asInteger;
     // evento gestione colloquio
     InsEvento(xIDAnag, 5, xIDevento, xIDaStato, xIDTipoStatoA, Date, xCognome, xNome, '', xIDRic, MainForm.xIDUtenteAttuale, xIDcliente);
     // ricreazione albero (in caso di modifiche)
     CreaAlbero;
end;

procedure TMyQuickViewForm.PMVoceIDAgendaClick(Sender: TObject);
begin
     // apertura agenda
     MainForm.LOBPromemoriaClick(self);
     // ricreazione albero (in caso di modifiche)
     CreaAlbero;
end;

procedure TMyQuickViewForm.PMVoceIDClienteClick(Sender: TObject);
begin
     // apertura scheda cliente
     MainForm.LOBClientiEBCClick(self);
     // prendi dati cliente
     Q.Close;
     Q.SQL.text := 'select ID,Descrizione Cliente from EBC_Clienti ' +
          'where ID=' + IntToStr(dxTreeList.FocusedNode.Values[7]);
     Q.Open;
     MainForm.EClienti.text := Q.FieldByName('Cliente').asString;
     MainForm.BCliSearchClick(self);
     Data2.TEBCclienti.Locate('ID', dxTreeList.FocusedNode.Values[7], []);
     // ricreazione albero (in caso di modifiche)
     CreaAlbero;
end;

procedure TMyQuickViewForm.PMQMCommessaClick(Sender: TObject);
begin
     // apertura commessa
     MainForm.EseguiQRicAttive;
     DataRicerche.QRicAttive.Locate('ID', dxTreeListQM.FocusedNode.Values[3], []);
     MainForm.BRiprendiRicClick(self);
     // ricreazione albero (in caso di modifiche)
     if MainForm.VerificaFunzLicenzeNew('104') = true then
          CreaAlberoQM;
end;

procedure TMyQuickViewForm.PMQMCandidatoClick(Sender: TObject);
begin
     // apertura scheda candidato
     MainForm.LOBSchedeClick(self);
     // prendi dati anagrafici
     Q.Close;
     Q.SQL.text := 'select ID,Cognome,Nome from Anagrafica ' +
          'where ID=' + IntToStr(dxTreeListQM.FocusedNode.Values[4]);
     Q.Open;
     MainForm.Edit1.Text := Q.FieldByName('Cognome').asString;
     MainForm.ENome.Text := Q.FieldByName('Nome').asString;
     MainForm.BAnagSearchClick(self);
     Data.TAnagrafica.Locate('ID', Q.FieldByName('ID').asInteger, []);
     // ricreazione albero (in caso di modifiche)
     if MainForm.VerificaFunzLicenzeNew('104') = true then
          CreaAlberoQM;
end;

procedure TMyQuickViewForm.PMQMPopup(Sender: TObject);
begin
     PMQMCommessa.Visible := False;
     PMQMCandidato.Visible := False;
     PMQMCandCommessa.Visible := False;
     if dxTreeListQM.FocusedNode.Level = 1 then begin
          // a seconda di quale ID � valorizzato, rendi visibile la relativa voce
          if dxTreeListQM.FocusedNode.Values[3] > 0 then
               PMQMCommessa.Visible := True;
          if dxTreeListQM.FocusedNode.Values[4] > 0 then
               PMQMCandidato.Visible := True;
          if dxTreeListQM.FocusedNode.Values[5] > 0 then
               PMQMCandCommessa.Visible := True;
     end;
end;

procedure TMyQuickViewForm.dxTreeListQMCustomDrawCell(Sender: TObject;
     ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
     AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
     var AText: string; var AColor: TColor; AFont: TFont;
     var AAlignment: TAlignment; var ADone: Boolean);
begin
     if ANode.Strings[2] <> '' then begin
          if (ANode.Values[2] > 0) and (AColumn = dxQMDicitura) then
               AFont.style := [fsBold]
     end;
end;

procedure TMyQuickViewForm.PMQMDettClick(Sender: TObject);
begin
     // visualizza dettaglio
     QMDettaglioForm := TQMDettaglioForm.create(self);
     QMDettaglioForm.Label1.Caption := dxTreeListQM.FocusedNode.Strings[6];
     Q.Close;
     Q.SQL.text := 'select Descrizione,SogliaWarning,SogliaNC from QM_Queries where Codice=:xCodice';
     Q.Parameters[0].Value := dxTreeListQM.FocusedNode.Strings[6];
     Q.Open;
     QMDettaglioForm.Label2.Caption := Q.FieldByName('Descrizione').asString;
     QMDettaglioForm.LAllarme.Caption := Q.FieldByName('SogliaWarning').asString;
     QMDettaglioForm.LNC.Caption := Q.FieldByName('SogliaNC').asString;
     Q.Close;
     QMDettaglioForm.ShowModal;
     QMDettaglioForm.Free;
end;

procedure TMyQuickViewForm.PMQMCandCommessaClick(Sender: TObject);
begin
     // recupera ID commessa
     Q.Close;
     Q.SQL.text := 'select IDRicerca from EBC_CandidatiRicerche where ID=' + dxTreeListQM.FocusedNode.Strings[5];
     Q.Open;
     // apertura commessa
     MainForm.EseguiQRicAttive;
     DataRicerche.QRicAttive.Locate('ID', Q.FieldByName('IDRicerca').asInteger, []);
     Q.Close;
     MainForm.BRiprendiRicClick(self);
     // ricreazione albero (in caso di modifiche)
     if MainForm.VerificaFunzLicenzeNew('104') = true then
          CreaAlberoQM;
end;

procedure TMyQuickViewForm.FormShow(Sender: TObject);
begin
     dxTreeListQM.Visible := MainForm.VerificaFunzLicenzeNew('104') = true;

     QUsersParam.close;
     QUsersParam.Parameters.ParamByName('xidutente').value := mainform.xIDUtenteAttuale;
     QUsersParam.Open;

     if QUsersParammyquickview_sel.Value <> NULL then
          CBMyQuickView.Checked := QUsersParammyquickview_sel.AsBoolean
     else CBMyQuickView.Checked := true;

     if QUsersParammyquickviewfigli_sel.Value <> NULL then
          CBMyQuickViewfigli.Checked := QUsersParammyquickviewfigli_sel.AsBoolean
     else CBMyQuickViewfigli.Checked := true;

     //Grafici
     MyQuickViewForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;   
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

end;

procedure TMyQuickViewForm.FormClose(Sender: TObject;
     var Action: TCloseAction);
var MyQuickView, MyQuickViewFigli: string;
begin
     if CBMyQuickView.checked then MyQuickView := '1'
     else MyQuickView := '0';
     if CBMyQuickViewFigli.checked then MyQuickViewFigli := '1'
     else MyQuickViewFigli := '0';

     data.QTemp.Close;
     data.QTemp.SQL.Text := 'update users set MyQuickView_Sel=' + MyQuickView +
          ', MyQuickViewFigli_Sel=' + MyQuickViewFigli +
          ' where id=' + inttostr(MainForm.xidutenteattuale);
     data.qtemp.ExecSQL;

end;

procedure TMyQuickViewForm.CBMyQuickViewfigliClick(Sender: TObject);
begin
     CreaAlbero;
end;

end.

