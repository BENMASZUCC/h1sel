unit LogOperazioniSogg;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     dxDBGrid, dxDBCtrl, dxTL, dxDBTLCl, dxGrClms, dxCntner, Db, DBTables,
     StdCtrls, Buttons, ExtCtrls, ADODB, U_ADOLinkCl;

type
     TLogOperazioniSoggForm = class(TForm)
          Panel1: TPanel;
          BitBtn1: TBitBtn;
          DsQLog: TDataSource;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1DataOra: TdxDBGridDateColumn;
          dxDBGrid1Tabella: TdxDBGridMaskColumn;
          dxDBGrid1Operation: TdxDBGridMaskColumn;
          dxDBGrid1Column4: TdxDBGridColumn;
          QLog: TADOLinkedQuery;
          QUsersLK: TADOLinkedQuery;
          QLogID: TAutoIncField;
          QLogIDUtente: TIntegerField;
          QLogDataOra: TDateTimeField;
          QLogTabella: TStringField;
          QLogKeyValue: TIntegerField;
          QLogOperation: TStringField;
          QLogAnnotazioni: TStringField;
          QLogUtente: TStringField;
          QLogDesctabella: TStringField;
          QLogDescOperation: TStringField;
          procedure QLog_OLDCalcFields(DataSet: TDataSet);
          procedure FormShow(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     LogOperazioniSoggForm: TLogOperazioniSoggForm;

implementation
uses ModuloDati;

{$R *.DFM}

procedure TLogOperazioniSoggForm.QLog_OLDCalcFields(DataSet: TDataSet);
begin
     if QLog.FieldByName('Tabella').AsString = 'AnagAltreInfo' then QLogDEscTabella.Value := 'altri dati anagrafici';
     if QLog.FieldByName('Tabella').AsString = 'AnagMansioni' then QLogDEscTabella.Value := 'ruoli/mansioni';
     if QLog.FieldByName('Tabella').AsString = 'Anagrafica' then QLogDEscTabella.Value := 'dati anagrafici';
     if QLog.FieldByName('Tabella').AsString = 'CompetenzeAnagrafica' then QLogDEscTabella.Value := 'competenze';
     if QLog.FieldByName('Tabella').AsString = 'CorsiExtra' then QLogDEscTabella.Value := 'corsi/stage/master';
     if QLog.FieldByName('Tabella').AsString = 'EsperienzeLavorative' then QLogDEscTabella.Value := 'esperienze lavorative';
     if QLog.FieldByName('Tabella').AsString = 'LingueConosciute' then QLogDEscTabella.Value := 'lingue conosciute';
     if QLog.FieldByName('Tabella').AsString = 'TitoliStudio' then QLogDEscTabella.Value := 'titoli di studio';

     if QLog.FieldByName('Operation').AsString = 'U' then QLogDescOperation.Value := 'aggiornamento';
     if QLog.FieldByName('Operation').AsString = 'I' then QLogDescOperation.Value := 'inserimento';
     if QLog.FieldByName('Operation').AsString = 'D' then QLogDescOperation.Value := 'cancellazione';
end;

procedure TLogOperazioniSoggForm.FormShow(Sender: TObject);
begin
     QLog.SQL.text := 'select Log_TableOp.* from Log_TableOp ' +
          'where Tabella=''Anagrafica'' and KeyValue=' + Data.Tanagrafica.FieldByName('ID').asstring + ' ' +
          'union ' +
          'select Log_TableOp.* ' +
          'from anagrafica,AnagAltreInfo,Log_TableOp ' +
          'where anagrafica.ID = AnagAltreInfo.IDAnagrafica ' +
          '  and Tabella=''AnagAltreInfo'' and KeyValue=AnagAltreInfo.IDAnagrafica ' +
          'and Anagrafica.ID=' + Data.Tanagrafica.FieldByName('ID').asstring + ' ' +
          'union ' +
          'select Log_TableOp.* ' +
          'from anagrafica,EsperienzeLavorative,Log_TableOp ' +
          'where anagrafica.ID = EsperienzeLavorative.IDAnagrafica ' +
          '  and Tabella=''EsperienzeLavorative'' and KeyValue=EsperienzeLavorative.ID ' +
          'and Anagrafica.ID=' + Data.Tanagrafica.FieldByName('ID').asstring + ' ' +
          'union ' +
          'select Log_TableOp.* ' +
          'from anagrafica,LingueConosciute,Log_TableOp ' +
          'where anagrafica.ID = LingueConosciute.IDAnagrafica ' +
          ' and Tabella=''LingueConosciute'' and KeyValue=LingueConosciute.ID ' +
          'and Anagrafica.ID=' + Data.Tanagrafica.FieldByName('ID').asstring + ' ' +
          'union ' +
          'select Log_TableOp.* ' +
          'from anagrafica,AnagMansioni,Log_TableOp ' +
          'where anagrafica.ID = AnagMansioni.IDAnagrafica ' +
          '  and Tabella=''AnagMansioni'' and KeyValue=AnagMansioni.ID ' +
          'and Anagrafica.ID=' + Data.Tanagrafica.FieldByName('ID').asstring + ' ' +
          'union ' +
          'select Log_TableOp.* ' +
          'from anagrafica,TitoliStudio,Log_TableOp ' +
          'where anagrafica.ID = TitoliStudio.IDAnagrafica ' +
          '  and Tabella=''TitoliStudio'' and KeyValue=TitoliStudio.ID ' +
          'and Anagrafica.ID=' + Data.Tanagrafica.FieldByName('ID').asstring + ' ' +
          'union ' +
          'select Log_TableOp.* ' +
          'from anagrafica,CorsiExtra,Log_TableOp ' +
          'where anagrafica.ID = CorsiExtra.IDAnagrafica ' +
          '  and Tabella=''CorsiExtra'' and KeyValue=CorsiExtra.ID ' +
          'and Anagrafica.ID=' + Data.Tanagrafica.FieldByName('ID').asstring;
     QLog.Open;
end;

end.
