unit EBCLink;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ExtCtrls,ShellAPI;

type
     TEBCLinkForm = class(TForm)
          Panel1: TPanel;
          Splitter1: TSplitter;
          Panel2: TPanel;
          Image1: TImage;
          procedure FormShow(Sender: TObject);
          procedure Image1Click(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     EBCLinkForm: TEBCLinkForm;

implementation

uses Main;

{$R *.DFM}

procedure TEBCLinkForm.FormShow(Sender: TObject);
begin
     //Grafica
     EBCLinkForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
end;

procedure TEBCLinkForm.Image1Click(Sender: TObject);
begin
     ShellExecute(Application.Handle, 'open',
          pchar('http://www.ebcconsulting.com/h1-sel-software-gestione-ricerca-e-selezione-personale.html'),
          nil, nil, SW_SHOWNORMAL);
end;

end.

