object FattureForm: TFattureForm
  Left = 194
  Top = 119
  Width = 1042
  Height = 540
  Caption = 'Visualizzazione fatture'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel4: TPanel
    Left = 0
    Top = 0
    Width = 1034
    Height = 39
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object ToolbarButton971: TToolbarButton97
      Left = 4
      Top = 2
      Width = 102
      Height = 34
      DropdownMenu = PMStampa
      Caption = 'Stampa/esporta'
    end
    object BitBtn1: TToolbarButton97
      Left = 948
      Top = 1
      Width = 85
      Height = 37
      Caption = 'Esci'
      Glyph.Data = {
        76060000424D7606000000000000360400002800000018000000180000000100
        080000000000400200000000000000000000000100000000000000000000FFFF
        FF00FF00FF0040D86800B1AEAC00545149001D973F0076967600C4F1D1002827
        270079D2910045A35E00716D5E00CFC7C00024C04D009B958D005FBA77005CE6
        8000E4EFE70090B69900B1D9BC005C8864003D3C390076B3870034934E003ABD
        5D007C7B730054D073005F5E5E00938F7800AAA695002CD358009BC6A60062A4
        73006AC98300E0D5C70049EC7500C1BBB7004FB96A00D5EEDB00697C6D008187
        820081C091002AAF4D00B5E9C300F1F7F2009D9D9C003432310044C666005397
        60009F9B7F0037CC5E004A49430039A0550072A67F007C9B83005F7763006E6E
        6D0055A66C006BB87F00A7A6A5008B8C880030C257004ADC700086B48800668B
        6C0060B06C0077C48B00259F470092C29C00C8C2B00054C27100E7FAEC0065B0
        790085827000C5C3C20056DC790071D08A005B584E005BCA79009A939900DCF3
        E3004DCC6E0086C4960080AC8C0032B054007572710064C17D003DA95B0041B4
        6000C9E7D100A39F9200B9F1C800F8FBF900D9D1C000BBB5B50046E67000B1AE
        A20048AB630080C793008DBB91006D9276006EAF7F0064D081004EE3750043CF
        680078BB890056B66F002ABD52007FB68D00CEECD6003FC262007E7E7B006F88
        7000329F500094908800D0CCB90031CC5A00C1B9AE00489B5F00A7A49E003BD1
        6300C4BBC100679F6F0077CD8E00CCC7C60055E57A004FA66700888886005DB5
        720072C0870095BD9700ACABA8005CA76F008CC09A006360610030BD56004DD1
        700046BF66005BC37700CBC8B600BBB8B90038B7590049A85B00AFAA98008482
        7500A09A8D0073C9890033D05D00B5B2A80059B16F00D9EEDF0087BB940033C6
        5A0096C3A1006DC4850069AB7A0040DE6A0051D775005B59590049D76D009B98
        80008F8D8500ECF6EF00E5F4E90028C35100B8AFB1003FCB63005AE07E009C97
        880065CA7F007B967A0062B47300D3EAD9004949480042BA610079767500A2A1
        9F006CCD8600A2A1990070B5820074B98600D4CFBC00C4BFB9002BC756003CA4
        58004AC66B008DB68E007CC08D00BFBDBE0068BD7E003CC6610049E773003131
        2F00E0F2E500C9C1BF0045E06E004FD472006B866D0082817D0094BE9F005FBF
        790065A97700C1EDCD003BD6640053E1790063786600B1AFA700ABA9A4007CCC
        9100C9BFB40037BC5A003DB45D00657C6A009BC1A600D1EFD90051B56C007DBA
        8E007A7870009B9B9B007BB88B00D9F2DF00B8ECC60026B04B002BB34F004C9A
        61008F8F8C00E8F5EB0059C3740093C69F0062B478008CB6970085BE9300A5A4
        A20055D476009D969A0053B96E005CBE76007CC78F00DEEEE300CAEDD300D3CB
        BD00BFB6B400B7B3B20056544B00DCD3C200CFC9BC00C6BDBD0037CF5F0041DA
        6A0042A45D00ADADAC0057E17C00A9A8A800A4A0950086868300020202020202
        022E39AEC10909C1AE392E020202020202020202020202B11C2F1C507ABDBD7A
        EB872F9F2E0202020202020202028016567DE5E488757588E4E57DB016FF0202
        0202020202C734A69A3E033FCDFCFCCD3FCC3E455F34700202020202E205F7AC
        94A03F69687E1111A84CA09481C3F43D0202023C4EF2AC7579CC30E31B60C068
        CDFC4CC57596254EE902021A7383B833796FC26EA3EA24C0C43F4C9E89B8640F
        DA023C0C0D2B993319C26E6E6EA31B60C4F9039EC5A7DF0D0CE973A9BB6C998E
        C26E6ED76ED7A3529D03797989526C4092A29176428855C2AD6EDD4851D727A3
        BACC7933A752198FD2914A23358AA4ADAD51081F5CC22727A38AF8339930AF06
        234A1DB6B98BD797C2087579945CA42727A3AF9999D3AF448C1DA174FAEC47D7
        CBA533333375DEA42797A3D46CE026724632A1F5776BEDB24FBF3E999933B8DE
        E39797A35859D8185EA1A9F67B961057224D6752BF3E3E0E2CE3EF122D966231
        F1A9B346ABE6E6BE9B937C0A0A0A4DB2AAF02D12A3ADFA0746FE841ECF21493B
        8243EED1D1D17C939BC95A5DAD0BE1D0908402FEF671B5B46ABC63636363EE43
        823B81147F3AC6F6FE0202FB5BB741D9DC2A535353532ABCB53B4985851525FE
        FB0202028461CF41869886868686E8D91766CA9C15D095D00202020202D0CF5F
        CEE7209A9A86986D17173638F395D00202020202020284787D3D283713D6C854
        65D53D7D7884020202020202020202FBB1044B8D2E2929DB8D4B04B1FB020202
        020202020202020202FBB1DBB1FDFD2EDBB18402020202020202}
      OnClick = BitBtn1Click
    end
  end
  object dxDBGrid1: TdxDBGrid
    Left = 0
    Top = 39
    Width = 1034
    Height = 474
    Bands = <
      item
      end>
    DefaultLayout = True
    HeaderPanelRowCount = 1
    KeyField = 'ID'
    ShowGroupPanel = True
    ShowSummaryFooter = True
    SummaryGroups = <
      item
        DefaultGroup = False
        SummaryItems = <
          item
            ColumnName = 'dxDBGrid1Stato'
            SummaryField = 'ImportoTotale'
            SummaryFormat = '#,###'
            SummaryType = cstSum
          end
          item
            ColumnName = 'dxDBGrid1Stato'
            SummaryField = 'SoloAccettati'
            SummaryFormat = '#,###'
            SummaryType = cstSum
          end>
        Name = 'Default'
      end>
    SummarySeparator = ', '
    Align = alClient
    TabOrder = 1
    OnMouseUp = dxDBGrid1MouseUp
    DataSource = DsQFatture
    Filter.Active = True
    Filter.Criteria = {00000000}
    LookAndFeel = lfFlat
    OptionsBehavior = [edgoAutoSort, edgoCaseInsensitive, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoStoreToRegistry, edgoTabThrough, edgoVertThrough]
    OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
    OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoRowSelect, edgoUseBitmap]
    RegistryPath = '\Software\H1'
    OnCustomDrawFooter = dxDBGrid1CustomDrawFooter
    object dxDBGrid1Cliente: TdxDBGridMaskColumn
      Sorted = csUp
      Width = 131
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Cliente'
      SummaryFooterType = cstCount
      SummaryFooterField = 'IDCliente'
    end
    object dxDBGrid1ID: TdxDBGridMaskColumn
      Visible = False
      Width = 111
      BandIndex = 0
      RowIndex = 0
      FieldName = 'ID'
    end
    object dxDBGrid1Progressivo: TdxDBGridMaskColumn
      Visible = False
      Width = 66
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Progressivo'
    end
    object dxDBGrid1Tipo: TdxDBGridMaskColumn
      Width = 56
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Tipo'
    end
    object dxDBGrid1RifProg: TdxDBGridMaskColumn
      Visible = False
      Width = 84
      BandIndex = 0
      RowIndex = 0
      FieldName = 'RifProg'
    end
    object dxDBGrid1IDCliente: TdxDBGridMaskColumn
      Visible = False
      Width = 111
      BandIndex = 0
      RowIndex = 0
      FieldName = 'IDCliente'
    end
    object dxDBGrid1Data: TdxDBGridDateColumn
      Width = 49
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Data'
    end
    object dxDBGrid1Decorrenza: TdxDBGridDateColumn
      Visible = False
      Width = 93
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Decorrenza'
    end
    object dxDBGrid1Importo: TdxDBGridCurrencyColumn
      Width = 83
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Importo'
      SummaryFooterType = cstSum
      SummaryFooterField = 'Importo'
      SummaryFooterFormat = '� ,0.00;-� ,0.00'
      Nullable = False
    end
    object dxDBGrid1IVA: TdxDBGridMaskColumn
      Visible = False
      Width = 84
      BandIndex = 0
      RowIndex = 0
      FieldName = 'IVA'
    end
    object dxDBGrid1Totale: TdxDBGridCurrencyColumn
      Caption = 'con IVA'
      Visible = False
      Width = 83
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Totale'
      SummaryFooterType = cstSum
      SummaryFooterField = 'Totale'
      SummaryFooterFormat = '� ,0.00;-� ,0.00'
      Nullable = False
    end
    object dxDBGrid1ModalitaPagamento: TdxDBGridMaskColumn
      Visible = False
      Width = 648
      BandIndex = 0
      RowIndex = 0
      FieldName = 'ModalitaPagamento'
    end
    object dxDBGrid1Assegno: TdxDBGridMaskColumn
      Visible = False
      Width = 330
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Assegno'
    end
    object dxDBGrid1Pagata: TdxDBGridCheckColumn
      Visible = False
      Width = 178
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Pagata'
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object dxDBGrid1ScadenzaPagamento: TdxDBGridDateColumn
      Caption = 'Scadenza'
      Visible = False
      Width = 75
      BandIndex = 0
      RowIndex = 0
      FieldName = 'ScadenzaPagamento'
    end
    object dxDBGrid1PagataInData: TdxDBGridDateColumn
      Caption = 'Pagata il'
      Visible = False
      Width = 63
      BandIndex = 0
      RowIndex = 0
      FieldName = 'PagataInData'
    end
    object dxDBGrid1AppoggioBancario: TdxDBGridMaskColumn
      Visible = False
      Width = 861
      BandIndex = 0
      RowIndex = 0
      FieldName = 'AppoggioBancario'
    end
    object dxDBGrid1IDRicerca: TdxDBGridMaskColumn
      Visible = False
      Width = 111
      BandIndex = 0
      RowIndex = 0
      FieldName = 'IDRicerca'
    end
    object dxDBGrid1Responsabile: TdxDBGridMaskColumn
      Visible = False
      Width = 861
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Responsabile'
    end
    object dxDBGrid1FlagFuoriAppIVA63372: TdxDBGridCheckColumn
      Visible = False
      Width = 178
      BandIndex = 0
      RowIndex = 0
      FieldName = 'FlagFuoriAppIVA63372'
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object dxDBGrid1RimbSpese: TdxDBGridCheckColumn
      Visible = False
      Width = 178
      BandIndex = 0
      RowIndex = 0
      FieldName = 'RimbSpese'
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object dxDBGrid1Column22: TdxDBGridColumn
      Caption = 'Filiale Origine'
      Visible = False
      Width = 60
      BandIndex = 0
      RowIndex = 0
      FieldName = 'FilialeOrigine'
    end
    object dxDBGrid1Column23: TdxDBGridColumn
      Caption = 'Filiale Chiusura'
      Visible = False
      Width = 51
      BandIndex = 0
      RowIndex = 0
      FieldName = 'FilialeChiusura'
    end
    object dxDBGrid1Column24: TdxDBGridColumn
      Caption = '% Fil Origine'
      Visible = False
      Width = 46
      BandIndex = 0
      RowIndex = 0
      FieldName = 'PercOrigine'
    end
    object dxDBGrid1Column25: TdxDBGridColumn
      Caption = 'Imp. Fil. Origine'
      Visible = False
      Width = 55
      BandIndex = 0
      RowIndex = 0
      FieldName = 'ImportoOrigine'
    end
    object dxDBGrid1Column26: TdxDBGridColumn
      Caption = '% Fil Chiusura'
      Visible = False
      Width = 50
      BandIndex = 0
      RowIndex = 0
      FieldName = 'PercChiusura'
    end
    object dxDBGrid1Column27: TdxDBGridColumn
      Caption = 'Imp. Fil Chiusura'
      Visible = False
      Width = 55
      BandIndex = 0
      RowIndex = 0
      FieldName = 'ImportoChiusura'
    end
    object dxDBGrid1Comune: TdxDBGridColumn
      CharCase = ecUpperCase
      Visible = False
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Comune'
    end
    object dxDBGrid1Provincia: TdxDBGridColumn
      CharCase = ecUpperCase
      Visible = False
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Provincia'
    end
    object dxDBGrid1Regione: TdxDBGridColumn
      CharCase = ecUpperCase
      Visible = False
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Regione'
    end
    object dxDBGrid1Anno: TdxDBGridColumn
      Caption = 'Anno Fattura'
      Visible = False
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Anno'
    end
  end
  object DsQFatture: TDataSource
    DataSet = QFatture
    Left = 144
    Top = 256
  end
  object PMStampa: TPopupMenu
    Left = 144
    Top = 152
    object stampagriglia1: TMenuItem
      Caption = 'stampa griglia'
      ImageIndex = 18
      OnClick = stampagriglia1Click
    end
    object esportainExcel1: TMenuItem
      Caption = 'esporta in Excel'
      ImageIndex = 19
      OnClick = esportainExcel1Click
    end
    object esportainHTML1: TMenuItem
      Caption = 'esporta in HTML'
      ImageIndex = 20
      OnClick = esportainHTML1Click
    end
  end
  object dxPrinter1: TdxComponentPrinter
    CurrentLink = dxPrinter1Link1
    PreviewOptions.PreviewBoundsRect = {0000000000000000800700001A040000}
    Version = 0
    Left = 216
    Top = 176
    object dxPrinter1Link1: TdxDBGridReportLink
      Caption = 'dxPrinter1Link1'
      Component = dxDBGrid1
      DesignerHelpContext = 0
      PrinterPage.Background.Brush.Style = bsClear
      PrinterPage.Footer = 6350
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 25400
      PrinterPage.Margins.Left = 25400
      PrinterPage.Margins.Right = 25400
      PrinterPage.Margins.Top = 10000
      PrinterPage.PageFooter.Font.Charset = DEFAULT_CHARSET
      PrinterPage.PageFooter.Font.Color = clWindowText
      PrinterPage.PageFooter.Font.Height = -11
      PrinterPage.PageFooter.Font.Name = 'Tahoma'
      PrinterPage.PageFooter.Font.Style = []
      PrinterPage.PageHeader.Font.Charset = DEFAULT_CHARSET
      PrinterPage.PageHeader.Font.Color = clWindowText
      PrinterPage.PageHeader.Font.Height = -11
      PrinterPage.PageHeader.Font.Name = 'Tahoma'
      PrinterPage.PageHeader.Font.Style = []
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportTitle.Font.Charset = DEFAULT_CHARSET
      ReportTitle.Font.Color = clWindowText
      ReportTitle.Font.Height = -19
      ReportTitle.Font.Name = 'Times New Roman'
      ReportTitle.Font.Style = [fsBold]
      BandColor = clBtnFace
      BandFont.Charset = DEFAULT_CHARSET
      BandFont.Color = clWindowText
      BandFont.Height = -11
      BandFont.Name = 'MS Sans Serif'
      BandFont.Style = []
      Color = clWindow
      EvenFont.Charset = DEFAULT_CHARSET
      EvenFont.Color = clWindowText
      EvenFont.Height = -11
      EvenFont.Name = 'Times New Roman'
      EvenFont.Style = []
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      FooterFont.Charset = DEFAULT_CHARSET
      FooterFont.Color = clWindowText
      FooterFont.Height = -11
      FooterFont.Name = 'MS Sans Serif'
      FooterFont.Style = []
      GridLineColor = clBtnFace
      GroupNodeFont.Charset = DEFAULT_CHARSET
      GroupNodeFont.Color = clWindowText
      GroupNodeFont.Height = -11
      GroupNodeFont.Name = 'Times New Roman'
      GroupNodeFont.Style = []
      GroupNodeColor = clBtnFace
      HeaderColor = clBtnFace
      HeaderFont.Charset = DEFAULT_CHARSET
      HeaderFont.Color = clWindowText
      HeaderFont.Height = -11
      HeaderFont.Name = 'MS Sans Serif'
      HeaderFont.Style = []
      OddColor = clWindow
      OddFont.Charset = DEFAULT_CHARSET
      OddFont.Color = clWindowText
      OddFont.Height = -11
      OddFont.Name = 'Times New Roman'
      OddFont.Style = []
      Options = [tlpoBands, tlpoHeaders, tlpoFooters, tlpoRowFooters, tlpoPreview, tlpoPreviewGrid, tlpoGrid, tlpoFlatCheckMarks, tlpoImages, tlpoStateImages]
      PreviewFont.Charset = DEFAULT_CHARSET
      PreviewFont.Color = clBlue
      PreviewFont.Height = -11
      PreviewFont.Name = 'MS Sans Serif'
      PreviewFont.Style = []
      RowFooterColor = cl3DLight
      RowFooterFont.Charset = DEFAULT_CHARSET
      RowFooterFont.Color = clWindowText
      RowFooterFont.Height = -11
      RowFooterFont.Name = 'MS Sans Serif'
      RowFooterFont.Style = []
      BuiltInReportLink = True
    end
  end
  object QFatture: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        '/*select distinct C.Descrizione Cliente, cf.Descrizione FilialeO' +
        'rigine, PercOrigine,Importo*PercOrigine/100 ImportoOrigine,'
      
        'cff.Descrizione FilialeChiusura, PercChiusura,Importo*PercChiusu' +
        'ra/100 ImportoChiusura,F.*'
      'from Fatture F'
      'join EBC_Clienti C on F.IDCliente = C.ID'
      'left  join Ebc_ClientiFiliali cf on f.idfilialeorigine=cf.id'
      'left  join Ebc_ClientiFiliali cff on f.idfilialechiusura=cff.id'
      'left join AccordiFiliali AF'
      'on AF.id=F.idaccordo'
      'order by F.ID desc*/'
      
        'select distinct C.Descrizione Cliente, cf.Descrizione FilialeOri' +
        'gine, PercOrigine,Importo*PercOrigine/100 ImportoOrigine,'
      
        'cff.Descrizione FilialeChiusura, PercChiusura,Importo*PercChiusu' +
        'ra/100 ImportoChiusura,F.ID'
      
        ',F.Progressivo,F.Tipo,F.RifProg,F.IDCliente,F.Data,F.Decorrenza,' +
        'F.Importo,F.IVA,F.Totale'
      
        ',F.ModalitaPagamento,F.Assegno,F.ScadenzaPagamento,F.Pagata,F.Pa' +
        'gataInData,F.AppoggioBancario'
      ',F.IDRicerca,F.Responsabile,F.FlagFuoriAppIVA63372,F.RimbSpese,'
      'C.Comune,C.Provincia,tr.regione,year (F.Data) as Anno '
      'from Fatture F'
      'join EBC_Clienti C on F.IDCliente = C.ID'
      'left  join Ebc_ClientiFiliali cf on f.idfilialeorigine=cf.id'
      'left  join Ebc_ClientiFiliali cff on f.idfilialechiusura=cff.id'
      'left join AccordiFiliali AF on AF.id=F.idaccordo'
      'left join tabcom tc on C.Provincia = tc.Prov'
      'left join TabRegioni tr on tc.Regione= tr.CodRegioneH1'
      'order by F.ID desc')
    Left = 144
    Top = 224
    object QFattureCliente: TStringField
      FieldName = 'Cliente'
      Size = 50
    end
    object QFattureID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QFattureProgressivo: TStringField
      FieldName = 'Progressivo'
      Size = 50
    end
    object QFattureTipo: TStringField
      FieldName = 'Tipo'
    end
    object QFattureIDCliente: TIntegerField
      FieldName = 'IDCliente'
    end
    object QFattureData: TDateTimeField
      FieldName = 'Data'
    end
    object QFattureDecorrenza: TDateTimeField
      FieldName = 'Decorrenza'
    end
    object QFattureImporto: TFloatField
      FieldName = 'Importo'
    end
    object QFattureIVA: TFloatField
      FieldName = 'IVA'
    end
    object QFattureTotale: TFloatField
      FieldName = 'Totale'
    end
    object QFattureModalitaPagamento: TStringField
      FieldName = 'ModalitaPagamento'
      Size = 60
    end
    object QFattureAssegno: TStringField
      FieldName = 'Assegno'
      Size = 30
    end
    object QFattureScadenzaPagamento: TDateTimeField
      FieldName = 'ScadenzaPagamento'
    end
    object QFatturePagata: TBooleanField
      FieldName = 'Pagata'
    end
    object QFatturePagataInData: TDateTimeField
      FieldName = 'PagataInData'
    end
    object QFattureAppoggioBancario: TStringField
      FieldName = 'AppoggioBancario'
      Size = 80
    end
    object QFattureIDRicerca: TIntegerField
      FieldName = 'IDRicerca'
    end
    object QFattureResponsabile: TStringField
      FieldName = 'Responsabile'
      FixedChar = True
      Size = 80
    end
    object QFattureFlagFuoriAppIVA63372: TBooleanField
      FieldName = 'FlagFuoriAppIVA63372'
    end
    object QFattureRimbSpese: TBooleanField
      FieldName = 'RimbSpese'
    end
    object QFattureFilialeOrigine: TStringField
      FieldName = 'FilialeOrigine'
      Size = 255
    end
    object QFattureFilialeChiusura: TStringField
      FieldName = 'FilialeChiusura'
      Size = 255
    end
    object QFatturePercOrigine: TFloatField
      FieldName = 'PercOrigine'
    end
    object QFattureImportoOrigine: TFloatField
      FieldName = 'ImportoOrigine'
      ReadOnly = True
    end
    object QFatturePercChiusura: TFloatField
      FieldName = 'PercChiusura'
    end
    object QFattureImportoChiusura: TFloatField
      FieldName = 'ImportoChiusura'
      ReadOnly = True
    end
    object QFattureRifProg: TStringField
      FieldName = 'RifProg'
      Size = 50
    end
    object QFattureComune: TStringField
      FieldName = 'Comune'
      Size = 100
    end
    object QFattureProvincia: TStringField
      FieldName = 'Provincia'
      Size = 2
    end
    object QFattureregione: TStringField
      FieldName = 'Regione'
      Size = 100
    end
    object QFattureAnno: TIntegerField
      FieldName = 'Anno'
      ReadOnly = True
    end
  end
  object qAccordiFilialiLK: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from accordifiliali')
    Left = 264
    Top = 256
    object qAccordiFilialiLKID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object qAccordiFilialiLKDescrizione: TStringField
      FieldName = 'Descrizione'
      Size = 100
    end
    object qAccordiFilialiLKPercOrigine: TFloatField
      FieldName = 'PercOrigine'
    end
    object qAccordiFilialiLKPercChiusura: TFloatField
      FieldName = 'PercChiusura'
    end
    object qAccordiFilialiLKAttivo: TBooleanField
      FieldName = 'Attivo'
    end
  end
end
