unit GestQualifContr;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ComCtrls, StdCtrls, Buttons, ExtCtrls, Db, ADODB, dxDBTLCl, dxGrClms,
     dxTL, dxDBCtrl, dxDBGrid, dxCntner, TB97;

type
     TGestQualifContrForm = class(TForm)
          Panel1: TPanel;
          PCUnico: TPageControl;
          TSInsieme: TTabSheet;
          TSTipiContratti: TTabSheet;
          TSContrattiNaz: TTabSheet;
          Panel2: TPanel;
          Panel3: TPanel;
          Panel4: TPanel;
          QQualif: TADOQuery;
          QTipiContratti: TADOQuery;
          QContrattiNaz: TADOQuery;
          DsQQualif: TDataSource;
          DsQTipiContratti: TDataSource;
          DsQContrattiNaz: TDataSource;
          QTipiContrattiLK: TADOQuery;
          QContrattiNazLK: TADOQuery;
          QTipiContrattiLKID: TAutoIncField;
          QTipiContrattiLKTipoContratto: TStringField;
          QTipiContrattiLKIDAzienda: TIntegerField;
          QContrattiNazLKID: TAutoIncField;
          QContrattiNazLKDescrizione: TStringField;
          QQualifID: TAutoIncField;
          QQualifIDTipoContratto: TIntegerField;
          QQualifQualifica: TStringField;
          QQualifOrdine: TSmallintField;
          QQualifIDContrattoNaz: TIntegerField;
          QQualifTipoContratto: TStringField;
          QQualifContrattoNaz: TStringField;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1Qualifica: TdxDBGridMaskColumn;
          dxDBGrid1TipoContratto: TdxDBGridLookupColumn;
          dxDBGrid1ContrattoNaz: TdxDBGridLookupColumn;
          dxDBGrid2: TdxDBGrid;
          dxDBGrid2TipoContratto: TdxDBGridMaskColumn;
          dxDBGrid3: TdxDBGrid;
          dxDBGrid3Descrizione: TdxDBGridMaskColumn;
          BNew: TToolbarButton97;
          BDel: TToolbarButton97;
          BOK: TToolbarButton97;
          BCan: TToolbarButton97;
          B2New: TToolbarButton97;
          B2Del: TToolbarButton97;
          B2OK: TToolbarButton97;
          B2Can: TToolbarButton97;
          B3New: TToolbarButton97;
          B3Del: TToolbarButton97;
          B3OK: TToolbarButton97;
          B3Can: TToolbarButton97;
          QTipiContrattiID: TAutoIncField;
          QTipiContrattiTipoContratto: TStringField;
          QTipiContrattiIDAzienda: TIntegerField;
          QContrattiNazID: TAutoIncField;
          QContrattiNazDescrizione: TStringField;
          BitBtn1: TToolbarButton97;
          procedure FormShow(Sender: TObject);
          procedure QTipiContrattiAfterPost(DataSet: TDataSet);
          procedure QContrattiNazAfterPost(DataSet: TDataSet);
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
          procedure DsQQualifStateChange(Sender: TObject);
          procedure DsQTipiContrattiStateChange(Sender: TObject);
          procedure DsQContrattiNazStateChange(Sender: TObject);
          procedure BNewClick(Sender: TObject);
          procedure BDelClick(Sender: TObject);
          procedure BOKClick(Sender: TObject);
          procedure BCanClick(Sender: TObject);
          procedure B2NewClick(Sender: TObject);
          procedure B2DelClick(Sender: TObject);
          procedure B2OKClick(Sender: TObject);
          procedure B2CanClick(Sender: TObject);
          procedure B3NewClick(Sender: TObject);
          procedure B3DelClick(Sender: TObject);
          procedure B3OKClick(Sender: TObject);
          procedure B3CanClick(Sender: TObject);
          procedure QQualifBeforeDelete(DataSet: TDataSet);
          procedure QTipiContrattiBeforeDelete(DataSet: TDataSet);
          procedure QContrattiNazBeforeDelete(DataSet: TDataSet);
          procedure BitBtn1Click(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     GestQualifContrForm: TGestQualifContrForm;

implementation

uses ModuloDati, Main;

{$R *.DFM}

procedure TGestQualifContrForm.FormShow(Sender: TObject);
begin
     //Grafica
     GestQualifContrForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel3.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel3.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel3.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel4.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel4.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel4.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     PCUnico.ActivePageIndex := 0;
end;

procedure TGestQualifContrForm.QTipiContrattiAfterPost(DataSet: TDataSet);
begin
     QTipiContrattiLK.Close;
     QTipiContrattiLK.Open;
end;

procedure TGestQualifContrForm.QContrattiNazAfterPost(DataSet: TDataSet);
begin
     QContrattiNazLK.Close;
     QContrattiNazLK.Open;
end;

procedure TGestQualifContrForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     if DsQQualif.state in [dsinsert, dsedit] then QQualif.Post;
     if DsQTipiContratti.state in [dsinsert, dsedit] then QTipiContratti.Post;
     if DsQContrattiNaz.state in [dsinsert, dsedit] then QContrattiNaz.Post;
end;

procedure TGestQualifContrForm.DsQQualifStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsQQualif.State in [dsEdit, dsInsert];
     BNew.Enabled := not b;
     BDel.Enabled := not b;
     BOK.Enabled := b;
     BCan.Enabled := b;
end;

procedure TGestQualifContrForm.DsQTipiContrattiStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsQTipiContratti.State in [dsEdit, dsInsert];
     B2New.Enabled := not b;
     B2Del.Enabled := not b;
     B2OK.Enabled := b;
     B2Can.Enabled := b;
end;

procedure TGestQualifContrForm.DsQContrattiNazStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsQContrattiNaz.State in [dsEdit, dsInsert];
     B3New.Enabled := not b;
     B3Del.Enabled := not b;
     B3OK.Enabled := b;
     B3Can.Enabled := b;
end;

procedure TGestQualifContrForm.BNewClick(Sender: TObject);
begin
     QQualif.Insert;
end;

procedure TGestQualifContrForm.BDelClick(Sender: TObject);
begin
     if MessageDlg('Sei sicuro di voler eliminare la voce selezionata ?', mtWarning, [mbNo, mbYes], 0) <> mrYes then exit;
     QQualif.Delete;
end;

procedure TGestQualifContrForm.BOKClick(Sender: TObject);
begin
     QQualif.Post;
end;

procedure TGestQualifContrForm.BCanClick(Sender: TObject);
begin
     QQualif.Cancel;
end;

procedure TGestQualifContrForm.B2NewClick(Sender: TObject);
begin
     QTipiContratti.Insert;
end;

procedure TGestQualifContrForm.B2DelClick(Sender: TObject);
begin
     if MessageDlg('Sei sicuro di voler eliminare la voce selezionata ?', mtWarning, [mbNo, mbYes], 0) <> mrYes then exit;
     QTipiContratti.Delete;
end;

procedure TGestQualifContrForm.B2OKClick(Sender: TObject);
begin
     QTipiContratti.Post;
end;

procedure TGestQualifContrForm.B2CanClick(Sender: TObject);
begin
     QTipiContratti.Cancel;
end;

procedure TGestQualifContrForm.B3NewClick(Sender: TObject);
begin
     QContrattiNaz.Insert;
end;

procedure TGestQualifContrForm.B3DelClick(Sender: TObject);
begin
     if MessageDlg('Sei sicuro di voler eliminare la voce selezionata ?', mtWarning, [mbNo, mbYes], 0) <> mrYes then exit;
     QContrattiNaz.Delete;
end;

procedure TGestQualifContrForm.B3OKClick(Sender: TObject);
begin
     QContrattiNaz.Post;
end;

procedure TGestQualifContrForm.B3CanClick(Sender: TObject);
begin
     QContrattiNaz.Cancel;
end;

procedure TGestQualifContrForm.QQualifBeforeDelete(DataSet: TDataSet);
begin
     // controllo esistenza esp.lav.
     Data.QTemp.Close;
     Data.QTemp.SQL.text := 'select count(*) Tot from EsperienzeLavorative where IDQualifContr=' + QQualifID.AsString;
     Data.QTemp.Open;
     if Data.QTemp.FieldByName('Tot').asInteger > 0 then begin
          MessageDlg('Ci sono esperienze lavorative collegate - IMPOSSIBILE CANCELLARE', mtError, [mbOK], 0);
          Abort;
          Exit;
     end;
end;

procedure TGestQualifContrForm.QTipiContrattiBeforeDelete(DataSet: TDataSet);
begin
     // controllo esistenza qualif.contr.
     Data.QTemp.Close;
     Data.QTemp.SQL.text := 'select count(*) Tot from QualificheContrattuali where IDTipoContratto=' + QTipiContrattiID.AsString;
     Data.QTemp.Open;
     if Data.QTemp.FieldByName('Tot').asInteger > 0 then begin
          MessageDlg('Ci sono esperienze qualifiche contrattuali collegate - IMPOSSIBILE CANCELLARE', mtError, [mbOK], 0);
          Abort;
          Exit;
     end;
end;

procedure TGestQualifContrForm.QContrattiNazBeforeDelete(DataSet: TDataSet);
begin
     // controllo esistenza qualif.contr.
     Data.QTemp.Close;
     Data.QTemp.SQL.text := 'select count(*) Tot from QualificheContrattuali where IDContrattoNaz=' + QContrattiNazID.AsString;
     Data.QTemp.Open;
     if Data.QTemp.FieldByName('Tot').asInteger > 0 then begin
          MessageDlg('Ci sono esperienze qualifiche contrattuali collegate - IMPOSSIBILE CANCELLARE', mtError, [mbOK], 0);
          Abort;
          Exit;
     end;
end;

procedure TGestQualifContrForm.BitBtn1Click(Sender: TObject);
begin
     GestQualifContrForm.ModalResult := mrOk;
end;

end.

