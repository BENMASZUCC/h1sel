object AnagAnnunciForm: TAnagAnnunciForm
  Left = 242
  Top = 154
  Width = 773
  Height = 272
  Caption = 'Annunci associati al soggetto'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 765
    Height = 41
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object ToolbarButton971: TToolbarButton97
      Left = 4
      Top = 1
      Width = 131
      Height = 36
      Caption = 'Nuova associazione ad annuncio'
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        04000000000080000000120B0000120B00001000000010000000000000008400
        000084848400C6C6C600DEE7E700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00444444444444
        4444444444400000000404444403333333042044402531111504250003035555
        5504255533253111150425553303555555042555332531111504255533035500
        0004255533253233330425553305253533302555324225500030425532442530
        3530442224442550535044444444425555044444444444222244}
      WordWrap = True
      OnClick = ToolbarButton971Click
    end
    object ToolbarButton972: TToolbarButton97
      Left = 136
      Top = 1
      Width = 106
      Height = 36
      Caption = 'Elimina associazione'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333000000000
        3333333777777777F3333330F777777033333337F3F3F3F7F3333330F0808070
        33333337F7F7F7F7F3333330F080707033333337F7F7F7F7F3333330F0808070
        33333337F7F7F7F7F3333330F080707033333337F7F7F7F7F3333330F0808070
        333333F7F7F7F7F7F3F33030F080707030333737F7F7F7F7F7333300F0808070
        03333377F7F7F7F773333330F080707033333337F7F7F7F7F333333070707070
        33333337F7F7F7F7FF3333000000000003333377777777777F33330F88877777
        0333337FFFFFFFFF7F3333000000000003333377777777777333333330777033
        3333333337FFF7F3333333333000003333333333377777333333}
      NumGlyphs = 2
      WordWrap = True
      OnClick = ToolbarButton972Click
    end
    object BitBtn1: TBitBtn
      Left = 667
      Top = 4
      Width = 94
      Height = 34
      Anchors = [akTop, akRight]
      TabOrder = 0
      Kind = bkOK
    end
  end
  object dxDBGrid1: TdxDBGrid
    Left = 0
    Top = 41
    Width = 765
    Height = 204
    Bands = <
      item
      end>
    DefaultLayout = True
    HeaderPanelRowCount = 1
    KeyField = 'ID'
    ShowGroupPanel = True
    SummaryGroups = <>
    SummarySeparator = ', '
    Align = alClient
    TabOrder = 1
    DataSource = DsQAnagAnnunci
    Filter.Active = True
    Filter.Criteria = {00000000}
    OptionsBehavior = [edgoAutoSort, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoTabThrough, edgoVertThrough]
    OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
    OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoIndicator, edgoRowSelect, edgoUseBitmap]
    object dxDBGrid1ID: TdxDBGridMaskColumn
      Visible = False
      Width = 38
      BandIndex = 0
      RowIndex = 0
      FieldName = 'ID'
    end
    object dxDBGrid1IDAnagrafica: TdxDBGridMaskColumn
      Visible = False
      Width = 42
      BandIndex = 0
      RowIndex = 0
      FieldName = 'IDAnagrafica'
    end
    object dxDBGrid1IDAnnEdizData: TdxDBGridMaskColumn
      Visible = False
      Width = 48
      BandIndex = 0
      RowIndex = 0
      FieldName = 'IDAnnEdizData'
    end
    object dxDBGrid1Testata: TdxDBGridMaskColumn
      Width = 125
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Testata'
    end
    object dxDBGrid1NomeEdizione: TdxDBGridMaskColumn
      Caption = 'Edizione'
      Width = 121
      BandIndex = 0
      RowIndex = 0
      FieldName = 'NomeEdizione'
    end
    object dxDBGrid1Rif: TdxDBGridMaskColumn
      Caption = 'Cod.ann.'
      Width = 51
      BandIndex = 0
      RowIndex = 0
      DisableGrouping = True
      FieldName = 'Rif'
      DisableFilter = True
    end
    object dxDBGrid1CodPubb: TdxDBGridColumn
      Caption = 'Cod.Pubb'
      Width = 53
      BandIndex = 0
      RowIndex = 0
      DisableGrouping = True
      FieldName = 'CodPubb'
      DisableFilter = True
    end
    object dxDBGrid1Data: TdxDBGridDateColumn
      Caption = 'Data pubb.'
      Width = 89
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Data'
    end
    object dxDBGrid1DataPervenuto: TdxDBGridDateColumn
      Caption = 'Pervenuto il'
      Sorted = csDown
      Width = 79
      BandIndex = 0
      RowIndex = 0
      FieldName = 'DataPervenuto'
    end
    object dxDBGrid1IDMansione: TdxDBGridMaskColumn
      Visible = False
      Width = 38
      BandIndex = 0
      RowIndex = 0
      FieldName = 'IDMansione'
    end
    object dxDBGrid1IDRicerca: TdxDBGridMaskColumn
      Visible = False
      Width = 38
      BandIndex = 0
      RowIndex = 0
      FieldName = 'IDRicerca'
    end
    object dxDBGrid1IDEmailAnnunciRic: TdxDBGridMaskColumn
      Visible = False
      Width = 59
      BandIndex = 0
      RowIndex = 0
      FieldName = 'IDEmailAnnunciRic'
    end
    object dxDBGrid1Progressivo: TdxDBGridMaskColumn
      Caption = 'Rif.Ricerca'
      Width = 58
      BandIndex = 0
      RowIndex = 0
      DisableGrouping = True
      FieldName = 'Progressivo'
      DisableFilter = True
    end
    object dxDBGrid1Ruolo: TdxDBGridMaskColumn
      Caption = 'Ruolo associato all'#39'annuncio'
      Width = 174
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Ruolo'
    end
  end
  object DsQAnagAnnunci: TDataSource
    DataSet = QAnagAnnunci
    Left = 72
    Top = 176
  end
  object QAnagAnnunci: TADOLinkedQuery
    Connection = Data.DB
    CursorType = ctStatic
    BeforeOpen = QAnagAnnunciBeforeOpen
    Parameters = <>
    SQL.Strings = (
      
        'select distinct Ann_AnagAnnEdizData.*, EBC_Ricerche.Progressivo,' +
        ' '
      '       Mansioni.Descrizione Ruolo,Ann_AnnEdizData.Data,'
      '       Rif,Ann_AnnEdizData.Codice CodPubb,'
      '       NomeEdizione,Denominazione Testata'
      'from Ann_AnagAnnEdizData,EBC_Ricerche,Mansioni,'
      '     Ann_AnnEdizData,Ann_Annunci,Ann_Edizioni,Ann_Testate'
      'where Ann_AnagAnnEdizData.IDAnnEdizData = Ann_AnnEdizData.ID'
      '  and Ann_AnnEdizData.IDAnnuncio = Ann_Annunci.ID'
      '  and Ann_AnnEdizData.IDEdizione = Ann_Edizioni.ID'
      '  and Ann_Edizioni.IDTestata = Ann_Testate.ID'
      '  and Ann_AnagAnnEdizData.IDRicerca *= EBC_Ricerche.ID'
      '  and Ann_AnagAnnEdizData.IDMansione *= Mansioni.ID')
    MasterDataSet = Data.TAnagrafica
    LinkedMasterField = 'ID'
    LinkedDetailField = 'IDAnagrafica'
    UseFilter = False
    Left = 72
    Top = 144
  end
end
