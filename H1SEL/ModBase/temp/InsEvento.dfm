�
 TINSEVENTOFORM 0k  TPF0TInsEventoFormInsEventoFormLeftLToppBorderStylebsDialogCaptionInserimento EventoClientHeight�ClientWidthOColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnCreate
FormCreateOnShowFormShowPixelsPerInch`
TextHeight TLabelLabel1LeftTop7Width>HeightCaptionData evento:  TLabelLabel2LeftTopLWidthsHeightCaptionDescrizione/Annotazioni  TPanelPanel1Left Top WidthOHeight*AlignalTop
BevelOuterbvNoneTabOrder  TBitBtnBitBtn1Left�TopWidthVHeight%TabOrder KindbkOK  TBitBtnBitBtn2Left�TopWidthVHeight%CaptionAnnullaTabOrderKindbkCancel   TDateEdit97
DEData_oldLeftITop3WidthqHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderColorCalendar.ColorValidclBlueDayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanClear
doCanPopup
doIsMaskeddoShowCanceldoShowToday   TPanelPanel19LeftTopzWidthEHeight	AlignmenttaLeftJustifyCaption  Elenco eventiColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TEditEDescLeftTop\WidthEHeight	MaxLength TabOrder  TDBGridDBGrid2LeftTopvWidthHeight� 
DataSourceDsUsersOptionsdgTitlesdgIndicatordgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgConfirmDeletedgCancelOnExit TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldName
NominativoWidth^Visible	 Expanded	FieldNameDescrizioneWidth� Visible	    TPanelPanel2LeftTop`WidthHeight	AlignmenttaLeftJustifyCaption-  Utente (tra quelli abilitati e non scaduti)ColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  	TdxDBGrid	dxDBGrid1LeftTop� WidthDHeight� Bands  DefaultLayout	HeaderPanelRowCountKeyFieldIDShowGroupPanel	SummaryGroups SummarySeparator, TabOrder
DataSourceDsEventiFilter.Criteria
       OptionsBehavioredgoAutoSortedgoDragScrolledgoEnterShowEditoredgoImmediateEditoredgoTabThroughedgoVertThrough 	OptionsDBedgoCancelOnExitedgoCanDeleteedgoCanInsertedgoCanNavigationedgoConfirmDeleteedgoLoadAllRecordsedgoUseBookmarks OptionsViewedgoAutoWidthedgoBandHeaderWidthedgoIndicatoredgoUseBitmap  TdxDBGridLookupColumndxDBGrid1DaStatoCaptionDallo statoSortedcsUpWidth� 	BandIndex RowIndex 	FieldNameDaStato  TdxDBGridMaskColumndxDBGrid1EventoWidth� 	BandIndex RowIndex 	FieldNameEvento  TdxDBGridLookupColumndxDBGrid1AStatoCaption
Allo statoWidth� 	BandIndex RowIndex 	FieldNameAStato   TJvDateTimePickerDEDataLeftJTop4WidthnHeightCalAlignmentdtaLeftDate�J��@Time�J��@
DateFormatdfShortDateMode
dmComboBoxKinddtkDate
ParseInputTabOrderDropDownDate�    TDataSourceDsEventiDataSetTEventiLeft4Top�   TDataSourceDsUsersDataSetQUsersLeft?Top�  TADOLinkedTableTEventi
ConnectionData.DB
CursorTypectStatic	IndexName
IdxDaStato	TableName
EBC_EventiLeftTop�  TAutoIncField	TEventiID	FieldNameIDReadOnly	  TIntegerFieldTEventiIDdaStato	FieldName	IDdaStato  TStringFieldTEventiEvento	FieldNameEventoSize  TIntegerFieldTEventiIDaStato	FieldNameIDaStato  TBooleanFieldTEventiNonCancellabile	FieldNameNonCancellabile  TIntegerFieldTEventiIDProcedura	FieldNameIDProcedura  TStringFieldTEventiDaStato	FieldKindfkLookup	FieldNameDaStatoLookupDataSetTStatiLKLookupKeyFieldsIDLookupResultFieldStato	KeyFields	IDdaStatoSizeLookup	  TStringFieldTEventiAStato	FieldKindfkLookup	FieldNameAStatoLookupDataSetTStatiLKLookupKeyFieldsIDLookupResultFieldStato	KeyFieldsIDaStatoSizeLookup	   TADOLinkedTableTStatiLK
ConnectionData.DB
CursorTypectStatic	TableName	EBC_StatiLeftxTop�  TAutoIncField
TStatiLKID	FieldNameIDReadOnly	  TStringFieldTStatiLKStato	FieldNameStatoSize  TIntegerFieldTStatiLKIDTipoStato	FieldNameIDTipoStato  TBooleanFieldTStatiLKPRman	FieldNamePRman  TIntegerFieldTStatiLKIDAzienda	FieldName	IDAzienda   TADOLinkedQueryQUsers
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsset dateformat dmy select ID,Nominativo,Descrizionefrom Users where DataScadenza>=getdate()   or DataRevoca>=getdate()1   or DataCreazione is null or datarevoca is nullorder by Nominativo   	UseFilterLeft`Top�   