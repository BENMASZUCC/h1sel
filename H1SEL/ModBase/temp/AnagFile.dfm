object AnagFileForm: TAnagFileForm
  Left = 149
  Top = 351
  BorderStyle = bsDialog
  Caption = 'File associato al candidato'
  ClientHeight = 297
  ClientWidth = 511
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 4
    Top = 174
    Width = 55
    Height = 13
    Caption = 'Descrizione'
  end
  object Label3: TLabel
    Left = 4
    Top = 215
    Width = 96
    Height = 13
    Caption = 'Percorso e nome file'
  end
  object SpeedButton1: TSpeedButton
    Left = 188
    Top = 56
    Width = 27
    Height = 24
    Hint = 'gestione tabella tipi'
    Glyph.Data = {
      36050000424D3605000000000000360400002800000010000000100000000100
      08000000000000010000120B0000120B00000001000000010000000000008400
      0000FF00000084848400C6C6C600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00050505050505
      0505050505050505050500000000000000000000000000000005030303030303
      0303030303030303000503040505050505050505050505030005030405050505
      0505050505050503000503040503030303050303030305030005030405050505
      0505050505050503000503040503030303050303030305030005030405050505
      0505050505050503000503040503030303050303030305030005030405050505
      0505050505050503000503040303030303030303030303030005030401020102
      0102010500050003000503040404040404040404040404030005030303030303
      0303030303030303030505050505050505050505050505050505}
    ParentShowHint = False
    ShowHint = True
    OnClick = SpeedButton1Click
  end
  object Label4: TLabel
    Left = 368
    Top = 48
    Width = 132
    Height = 13
    Caption = 'Impostazione di inserimento:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsUnderline]
    ParentFont = False
  end
  object DBText1: TDBText
    Left = 336
    Top = 64
    Width = 169
    Height = 17
    Alignment = taRightJustify
    DataField = 'Opzione'
    DataSource = DsQGlobal
  end
  object SpeedButton2: TSpeedButton
    Left = 280
    Top = 8
    Width = 20
    Height = 20
    Glyph.Data = {
      AA030000424DAA03000000000000360000002800000011000000110000000100
      1800000000007403000000000000000000000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FF00FFFFFFFFFFFFFFFFFFDEDFDEBDAEA5EFDFD6F7EFE7F7EFE7F7EFE7E7D7CE
      7B696B525152DEDFDEFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFF7F7EF
      E7DEF7F7F7E7DFDECEB6A5DEB6A5E7CFBDEFEFEFF7F7F7DECFC6393031CECFCE
      FFFFFFFFFFFFFFFFFF00FFFFFFF7F7EFF7F7EFF7F7EFD69E7BC66131C6714ADE
      B6A5C66131C66131DEB69CF7F7F7EFDFD6393831FFFFFFFFFFFFFFFFFF00FFFF
      FFF7E7DEF7F7F7CE795AC66129CE6129C69E84FFFFFFDE966BCE6131C65929D6
      9E7BF7F7F7C6B6B5848684FFFFFFFFFFFF00F7EFE7FFFFFFDEAE8CCE6129CE61
      31CE6131CE6939DE8E63CE6129CE6131CE6131C65929E7C7B5F7EFEF524942FF
      FFFFFFFFFF00F7DFD6FFF7F7CE6939CE6931CE6131CE6131CE8663F7E7D6D671
      42CE6131CE6131CE6131CE7952F7FFFFA5968CADAEADFFFFFF00FFEFEFF7DFCE
      CE6131D66931CE6131CE6131C6795AFFFFFFE7A684CE6129CE6131CE6931CE69
      31F7F7EFDECFBD9C9E9CFFFFFF00FFEFEFF7D7C6D66931D66931CE6131CE6131
      CE6131D6AE9CFFFFFFE79E73CE6129CE6931CE6931F7EFEFDECFC69C9E9CFFFF
      FF00FFEFEFFFE7DEE77142DE6939CE6129CE6129CE6131CE6129E7C7BDFFFFF7
      D67139D66939D67142FFF7F7D6C7BDB5B6B5FFFFFF00F7E7DEFFFFFFF79E6BEF
      7942D68E6BEFDFCEDE7952CE6129DEA684FFFFFFDE8E63DE7139E79E73FFFFFF
      A5968CFFFFFFFFFFFF00F7EFE7FFFFFFFFE7CEFF9E63E78E63EFEFEFFFEFE7EF
      BEA5FFF7EFF7EFEFE78652EF8652FFEFDEFFF7EFC6BEBDFFFFFFFFFFFF00FFFF
      FFF7E7DEFFFFFFFFE7C6FFBE84EFBE94EFE7DEE7E7E7EFE7DEFFB68CFF9E6BFF
      DFC6FFFFFFDECFC6FFFFFFFFFFFFFFFFFF00FFFFFFF7F7EFF7E7E7FFFFFFFFFF
      EFFFF7CEFFE7B5FFD7A5FFD79CFFDFB5FFF7EFFFFFFFEFDFDEEFEFEFFFFFFFFF
      FFFFFFFFFF00FFFFFFFFFFFFF7F7EFF7E7D6FFFFF7FFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFF7F7F7EFE7F7F7EFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF
      FFFFFFFFFFFFF7EFE7F7E7DEFFEFE7FFEFEFFFEFE7EFE7DEFFF7F7FFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFF00}
    OnClick = SpeedButton2Click
  end
  object LInfo: TLabel
    Left = 256
    Top = 128
    Width = 246
    Height = 39
    Caption = 
      'NOTA: i tipi di file visualizzati in rosso'#13#10'indicano che il file' +
      ' sar� visibile sul web dai candidati '#13#10'che entrano nella loro ar' +
      'ea riservata'
    Visible = False
  end
  object BTipiFile: TToolbarButton97
    Left = 256
    Top = 64
    Width = 73
    Height = 33
    Caption = 'Tab.Tipi File'
    Glyph.Data = {
      36050000424D3605000000000000360400002800000010000000100000000100
      080000000000000100000000000000000000000100000000000000000000FFFF
      FF00FF00FF0087837F00AEA49600F8EEE4009B948A00FBF7F200918B8500A59C
      9000FAF2EB008C878200968F88009F978E00AAA09400FCF9F600FBF5EE009892
      8900A2998F00A79E9200F9F0E900F9F0E600898580008E898300938D86009D96
      8C00ACA29600FBF6F000FBF3ED00FAF2E900FAF4EC008A8681008F8A8400948E
      8700979089009C958B00A1988E00A39A9000A89F9300FCF8F500FCF7F300FBF4
      EF00F9F2EA0088847F008B8781008C888300908A8500928C8500958F87009992
      8A009A938A00A69D9200ABA19400ACA19500FBF7F300FBF5EF00FAF5EE00FBF3
      EE00FAF3ED00FAF2EA00F9EFE6008A8581008D898300908B8500928C86009690
      8800979189009B948B009E968C009F978D00A1998E00A99F9300AEA396000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000C2121182F08
      2E20172D0B1F3D16030322010101012F010101012D010101012B110110370118
      011B36011701270F011631013A39012101291B0120010727011F32010101010C
      010101013F010101012C4332323142220C2121402F3F20173E2D440101010142
      010101014001010101170D010A1E0131013A3A0121011B28012046010A1D0132
      012A3B013001291B012E25010101014301010101410101010108092512244519
      433231112241302121401301010101450101010111010101012147012A050124
      0115140132013837012134011505011201053C0106011C10010C1A0101010109
      010101012301010101220448350E26330925460D442306323111}
    WordWrap = True
    OnClick = BTipiFileClick
  end
  object BitBtn1: TBitBtn
    Left = 311
    Top = 2
    Width = 93
    Height = 37
    TabOrder = 0
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 406
    Top = 2
    Width = 93
    Height = 37
    Caption = 'Annulla'
    TabOrder = 1
    Kind = bkCancel
  end
  object Panel1: TPanel
    Left = 3
    Top = 4
    Width = 249
    Height = 45
    BevelOuter = bvLowered
    Enabled = False
    TabOrder = 2
    object Label1: TLabel
      Left = 5
      Top = 4
      Width = 51
      Height = 13
      Caption = 'Candidato:'
    end
    object ECognome: TEdit
      Left = 5
      Top = 18
      Width = 127
      Height = 21
      Color = clYellow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      Text = 'ECognome'
    end
    object ENome: TEdit
      Left = 133
      Top = 18
      Width = 111
      Height = 21
      Color = clYellow
      TabOrder = 1
      Text = 'ENome'
    end
  end
  object EDesc: TEdit
    Left = 4
    Top = 190
    Width = 490
    Height = 21
    MaxLength = 100
    TabOrder = 3
  end
  object FEFile: TFilenameEdit
    Left = 4
    Top = 230
    Width = 492
    Height = 21
    NumGlyphs = 1
    TabOrder = 4
  end
  object CBFlagInvio: TCheckBox
    Left = 4
    Top = 257
    Width = 405
    Height = 17
    Caption = 'File da considerare per l'#39'invio'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clPurple
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
  end
  object CBVisibleCliente: TCheckBox
    Left = 4
    Top = 275
    Width = 405
    Height = 17
    Caption = 'File visibile al cliente'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clPurple
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 6
  end
  object dxDBGrid1: TdxDBGrid
    Left = 8
    Top = 56
    Width = 243
    Height = 113
    Bands = <
      item
      end>
    DefaultLayout = True
    HeaderPanelRowCount = 1
    KeyField = 'ID'
    SummaryGroups = <>
    SummarySeparator = ', '
    TabOrder = 7
    DataSource = DsQTipiFile
    Filter.Criteria = {00000000}
    OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
    OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoIndicator, edgoUseBitmap]
    OnCustomDrawCell = dxDBGrid1CustomDrawCell
    object dxDBGrid1Tipo: TdxDBGridColumn
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Tipo'
    end
    object dxDBGrid1DefaultVisibileWeb: TdxDBGridCheckColumn
      Visible = False
      Width = 100
      BandIndex = 0
      RowIndex = 0
      FieldName = 'DefaultVisibileWeb'
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
  end
  object DsQTipiFile: TDataSource
    DataSet = QTipiFile
    Left = 16
    Top = 128
  end
  object QTipiFile: TADOLinkedQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from TipiFileCand order by ordinamento')
    UseFilter = False
    Left = 16
    Top = 96
    object QTipiFileID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QTipiFileTipo: TStringField
      FieldName = 'Tipo'
      Size = 30
    end
    object QTipiFileOrdinamento: TIntegerField
      FieldName = 'Ordinamento'
    end
    object QTipiFileDefaultVisibileWeb: TBooleanField
      FieldName = 'DefaultVisibileWeb'
    end
  end
  object QGlobal: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    OnCalcFields = QGlobalCalcFields
    Parameters = <>
    SQL.Strings = (
      'select AnagFileDentroDB'
      'from Global')
    Left = 208
    Top = 80
    object QGlobalAnagFileDentroDB: TBooleanField
      FieldName = 'AnagFileDentroDB'
    end
    object QGlobalOpzione: TStringField
      FieldKind = fkCalculated
      FieldName = 'Opzione'
      Size = 100
      Calculated = True
    end
  end
  object DsQGlobal: TDataSource
    DataSet = QGlobal
    Left = 208
    Top = 112
  end
end
