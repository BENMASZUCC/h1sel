�
 TMODDETTCANDRICFORM 0<  TPF0TModDettCandRicFormModDettCandRicFormLeft�Top� ActiveControl	DEDataInsBorderStylebsDialogCaption!Dettaglio candidato nella ricercaClientHeightqClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TLabelLabel1LeftTopBWidthvHeightCaptionInserito in elenco in data:  TLabelLabel2LeftTop� Width4HeightCaptionSituazione:  TLabelLabel3LeftTopmWidth&HeightCaptionValutaz.  TLabelLabel4LeftTop� Width� HeightCaption#Note Commessa: (max 2000 caratteri)  TLabelLabel5LeftTopbWidth HeightCaption8Note Cliente interno / selezionatore (max 500 caratteri)  TLabelLabel6LeftTop�WidthpHeightCaptionIndicazione (da tabella):  TSpeedButtonSpeedButton1Left� Top�Width^HeightCaptionModifica tabellaOnClickSpeedButton1Click  TBitBtnBitBtn1Left*TopWidthTHeight!TabOrderKindbkOK  TBitBtnBitBtn2Left*Top%WidthTHeight!CaptionAnnullaTabOrderKindbkCancel  TPanelPanel1LeftTopWidthHeight5
BevelOuter	bvLoweredEnabledTabOrder TDBEditDBEdit1LeftTopWidth9Height	DataFieldCVNumero
DataSourceDataRicerche.DsQCandRicTabOrder   TDBEditDBEdit2Left@TopWidth� Height	DataFieldCognome
DataSourceDataRicerche.DsQCandRicFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder  TDBEditDBEdit3Left@TopWidth� Height	DataFieldNome
DataSourceDataRicerche.DsQCandRicTabOrder   TDateEdit97	DEDataInsLeftTopQWidth_HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder ColorCalendar.ColorValidclBlueDayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanPopup
doIsMaskeddoShowCanceldoShowToday   TEditEMiniValLeftTop}WidthvHeightCharCaseecUpperCase	MaxLength� TabOrder  TMemoESituazioneLeftTop� WidthxHeight;	MaxLength TabOrder  	TCheckBoxcbVisibileClienteLeftTop�Width� HeightCaptionVisibile al clienteFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TMemoENoteLeftTop� Width}Heighti	MaxLength�
ScrollBars
ssVerticalTabOrder  TMemoeNoteCliLeftToprWidth}HeightY
ScrollBars
ssVerticalTabOrder  TDBGridDBGrid1LeftTop�Width� Heights
DataSourceDsQIndicazioniOptionsdgTitlesdgIndicatordgColumnResize
dgColLines
dgRowLinesdgTabsdgConfirmDeletedgCancelOnExit TabOrder	TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameIDVisible Expanded	FieldNameIndicazioneWidth~Visible	    	TCheckBoxCB_VisionatoLeft� TopSWidtheHeightCaption	VisionatoFont.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder
  	TADOQueryQIndicazioni
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect ID,Indicazionefrom EBC_Ricerche_indicazioniunion 'select NULL ID, '(nessuna)' Indicazioneorder by Indicazione LeftTop� TAutoIncFieldQIndicazioniID	FieldNameIDReadOnly	  TStringFieldQIndicazioniIndicazione	FieldNameIndicazioneSize2   TDataSourceDsQIndicazioniDataSetQIndicazioniLeft0Top�  	TADOQueryQ
ConnectionData.DB
Parameters Left0Top�   