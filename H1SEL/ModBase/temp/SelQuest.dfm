object SelQuestForm: TSelQuestForm
  Left = 358
  Top = 108
  Width = 627
  Height = 470
  Caption = 'Selezione questionario'
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 619
    Height = 45
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object BitBtn1: TBitBtn
      Left = 413
      Top = 5
      Width = 100
      Height = 35
      Anchors = [akTop, akRight]
      TabOrder = 0
      Kind = bkOK
    end
    object BitBtn2: TBitBtn
      Left = 517
      Top = 5
      Width = 96
      Height = 35
      Anchors = [akTop, akRight]
      Caption = 'Annulla'
      TabOrder = 1
      Kind = bkCancel
    end
  end
  object dxDBGrid1: TdxDBGrid
    Left = 0
    Top = 45
    Width = 619
    Height = 398
    Bands = <
      item
      end>
    DefaultLayout = True
    HeaderPanelRowCount = 1
    KeyField = 'ID'
    ShowGroupPanel = True
    ShowSummaryFooter = True
    SummaryGroups = <
      item
        DefaultGroup = False
        SummaryItems = <
          item
            SummaryField = 'Punteggio'
            SummaryType = cstSum
          end>
        Name = 'dxDBGrid1SummaryGroup2'
      end>
    SummarySeparator = ', '
    Align = alClient
    TabOrder = 1
    DataSource = DsQElencoQuest
    Filter.Criteria = {00000000}
    OptionsBehavior = [edgoAutoSort, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoTabThrough, edgoVertThrough]
    OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
    OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoIndicator, edgoUseBitmap]
    object dxDBGrid1ID: TdxDBGridMaskColumn
      Visible = False
      BandIndex = 0
      RowIndex = 0
      FieldName = 'ID'
    end
    object dxDBGrid1Questionario: TdxDBGridMaskColumn
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Questionario'
    end
    object dxDBGrid1Tipologia: TdxDBGridMaskColumn
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Tipologia'
    end
    object dxDBGrid1GruppoQuestionario: TdxDBGridMaskColumn
      Caption = 'Gruppo'
      BandIndex = 0
      RowIndex = 0
      FieldName = 'GruppoQuestionario'
    end
    object dxDBGrid1Attivo: TdxDBGridCheckColumn
      Width = 100
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Attivo'
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
  end
  object QElencoQuest: TADOQuery
    Connection = Data.DB
    Parameters = <>
    SQL.Strings = (
      'select Q.ID, Q.TipoDomanda Questionario, '
      #9'TQ.TipoQuestionario Tipologia, GQ.GruppoQuestionario, Q.Attivo'
      'from QUEST_TipiDomande Q'
      'left outer join QUEST_TipiQuest TQ on Q.TipoQuest = TQ.ID'
      'left outer join QUEST_GruppiQuest GQ on Q.IDGruppoQuest = GQ.ID')
    Left = 80
    Top = 112
    object QElencoQuestID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QElencoQuestQuestionario: TStringField
      FieldName = 'Questionario'
      Size = 50
    end
    object QElencoQuestTipologia: TStringField
      FieldName = 'Tipologia'
      Size = 50
    end
    object QElencoQuestGruppoQuestionario: TStringField
      FieldName = 'GruppoQuestionario'
      Size = 100
    end
    object QElencoQuestAttivo: TBooleanField
      FieldName = 'Attivo'
    end
  end
  object DsQElencoQuest: TDataSource
    DataSet = QElencoQuest
    Left = 80
    Top = 144
  end
end
