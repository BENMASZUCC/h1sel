unit uUtilsVarie;

interface



uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, Registry,
     StdCtrls, Buttons, Grids, DBGrids, Mask, DBCtrls, ExtCtrls, Db, DBTables, FrmFrameDB,
     ComObj, ModuloDati, ModuloDati2, Main, MDRicerche, SelTabGen, SceltaAnagFile, View,
     dxDBELib, ShellApi, U_ADOLinkCl, shdocvw, MAPI, AdoDB, FileAperto, ModelCompositionTVD5b, Help,
     JclFileUtils, SchedaCand, activex, SelPers, AFQuickMail;

type TMsgDlgBtns = set of TMsgDlgBtn;

const xAnagSQL = 'SELECT Anagrafica.ID,dacontattocliente, Cognome, Nome, Titolo,' + chr(13) +
     'DataNascita, GiornoNascita, MeseNascita, LuogoNascita,' + chr(13) +
          'Sesso, Indirizzo, Cap, Comune, IDComuneRes, IDZonaRes,' + chr(13) +
          'Provincia, DomicilioIndirizzo, DomicilioCap, DomicilioComune,' + chr(13) +
          'IDComuneDom, IDZonaDom, DomicilioProvincia, DomicilioStato,' + chr(13) +
          'RecapitiTelefonici, Cellulare, Fax, CodiceFiscale, PartitaIVA,' + chr(13) +
          'Email, IDStatoCivile, Anagrafica.IDStato, Anagrafica.IDTipoStato,' + chr(13) +
          'CVNumero, CVIDAnnData, CVinseritoInData, Foto, IDTipologia, ' + chr(13) +
          'IDUtenteMod, DubbiIns, IDProprietaCV, TelUfficio, OldCVNumero, ' + chr(13) +
          'EBC_Stati.Stato DescStato, TipoStrada, DomicilioTipoStrada, NumCivico, DomicilioNumCivico, ' + chr(13) +
          'IDUtente, Anagrafica.Stato, LibPrivacy, CVDataReale, CVDataAgg,InteressanteInternet,VIPList, ' + chr(13) +
          'TelAziendaCentralino,TelAziendaSegretaria,NoteCellulare,Cellulare2,NoteCellulare2,EMailUfficio, Regione, DomicilioRegione,StatoNascita ' + chr(13) +
          //'DATEDIFF (yy,DataNascita,GetDate()) as Eta '+chr(13)+
     'FROM Anagrafica,EBC_Stati ' + chr(13) +
          'WHERE Anagrafica.IDStato=EBC_Stati.ID';
const xClientiSQL = 'SELECT EBC_Clienti.ID, EBC_Clienti.Descrizione, ' +
     'EBC_Clienti.Stato, EBC_Clienti.Indirizzo, EBC_Clienti.Cap, ' +
          'EBC_Clienti.Comune, EBC_Clienti.Provincia, EBC_Clienti.IDAttivita, ' +
          'EBC_Clienti.Telefono, EBC_Clienti.Fax, EBC_Clienti.PartitaIVA, ' +
          'EBC_Clienti.CodiceFiscale, EBC_Clienti.BancaAppoggio, ' +
          'EBC_Clienti.SistemaPagamento, EBC_Clienti.NoteContratto, ' +
          'EBC_Clienti.Responsabile, EBC_Clienti.ConosciutoInData, ' +
          'EBC_Clienti.IndirizzoLegale, EBC_Clienti.CapLegale, ' +
          'EBC_Clienti.ComuneLegale, EBC_Clienti.ProvinciaLegale,EBC_Clienti.IDRegolaCliente, ' +
          'EBC_Clienti.CartellaDoc, ' +
          'EBC_Clienti.TipoStrada, EBC_Clienti.NumCivico, NumDipendenti, Fatturato, SitoInternet, ' +
          'EBC_Clienti.TipoStradaLegale, EBC_Clienti.NumCivicoLegale, ' +
          'EBC_Clienti.NazioneAbb, EBC_Clienti.NazioneAbbLegale,EBC_Clienti.IDClienteFiliale, Blocco1, EnteCertificatore, LibPrivacy, IDTipoAzienda, DescrizAnnunci, IDConoscTramite, LetteraIntento, DataLetteraIntenti ' +
          'FROM EBC_Clienti where ID is not null ';
const xRicPendSQL = 'SELECT ebc_ricerche.ID, ebc_ricerche.IDCliente, ' +
     'ebc_ricerche.IDUtente, ebc_ricerche.Progressivo, TitoloCliente, ' +
          'ebc_ricerche.DataInizio, ebc_ricerche.DataFine, ' +
          'ebc_ricerche.Stato, ebc_ricerche.Conclusa, ' +
          'ebc_ricerche.IDStatoRic, ebc_ricerche.DallaData, ' +
          'ebc_ricerche.IDArea, ebc_ricerche.IDMansione, ' +
          'ebc_ricerche.IDSpec, ebc_ricerche.NumRicercati, ' +
          'ebc_ricerche.Note, ebc_ricerche.IDFattura, ' +
          'ebc_ricerche.Sospesa, ebc_ricerche.SospesaDal, ' +
          'ebc_ricerche.StipendioNetto, ebc_ricerche.StipendioLordo, ' +
          'ebc_ricerche.Fatturata, ebc_ricerche.Specifiche, ebc_ricerche.SpecifEst, ' +
          'aree.Descrizione Area, mansioni.Descrizione Mansione, ' +
          'ggDiffApRic,ggDiffUcRic,ggDiffColRic,DataPrevChiusura, ' +
          'mansioni.IDArea IDAreaMans, ebc_ricerche.IDOfferta, ' +
          'ebc_clienti.Descrizione Cliente, IDContrattoCG, ' +
          'users.Nominativo Utente, IDUtente2, IDUtente3, ' +
          'ebc_statiric.StatoRic StatoRic, ebc_ricerche.Tipo, ' +
          'EBC_Ricerche.IDSede, EBC_Ricerche.IDContattoCli, ' +
          'Aziende.Descrizione Sede, EBC_ContattiClienti.Contatto RifInterno, ' +
          'Provvigione,ProvvigionePerc,QuandoPagamUtente, ' +
          'OreLavUtente1,OreLavUtente2,OreLavUtente3, IDCausale, DataPresuntaPres, InBudget, ' +
          'IDStatoSecondario, EBC_StatiRicSecondari.StatoSecondario, NoteStatoSec, ' +
          'IDIndirizzoCliente,EBC_ClienteIndirizzi.Provincia IndProv, EBC_ClienteIndirizzi.Descrizione DescIndirizzo,  ' +
          'IDTipologiaContratto, TipologiaContratto ' +
          'FROM EBC_Ricerche,Aree, ' +
          '  Mansioni,EBC_Clienti, ' +
          '  Users,EBC_StatiRic, Aziende, EBC_ContattiClienti, EBC_StatiRicSecondari, EBC_ClienteIndirizzi, TipologieContratti  ' +
          'WHERE (ebc_ricerche.IDArea *= aree.ID) ' +
          'AND   (ebc_ricerche.IDMansione *= mansioni.ID) ' +
          'AND   (ebc_ricerche.IDCliente = ebc_clienti.ID) ' +
          'AND   (ebc_ricerche.IDUtente = users.ID) ' +
          'AND   (ebc_ricerche.IDStatoRic = ebc_statiric.ID) ' +
          'AND   (ebc_ricerche.IDStatoSecondario *= EBC_StatiRicSecondari.ID) ' +
          'AND  (ebc_ricerche.IDSede *= Aziende.ID) ' +
          'AND  (ebc_ricerche.IDIndirizzoCliente *= EBC_ClienteIndirizzi.ID) ' +
          'AND  (ebc_ricerche.IDContattoCli *= EBC_ContattiClienti.ID) ' +
          'AND  (ebc_ricerche.IDTipologiaContratto *= TipologieContratti.ID)';
var xArrayIDAnag: array[1..150000] of integer;
var xReg: TRegistry;
var xCheckVisionato: boolean;
var Qrt: TADOQuery;
procedure ApriFileWord(xNumCv: string);
procedure ApriFileWordFromFile(xNomeFile, xNumCv: string);
procedure CaricaPrimi;
procedure SettaDiritti(xID: integer);
function OkIDAnag(xID: integer): boolean;
function CreateQueryFromString(s: string): TAdoLinkedQuery;
function PosizionaAnag(xIDAnag: integer): boolean;
function DammiCVNumero(xIDAnag: integer): integer;
procedure CaricaApriAnag(xStringa, xNome: string);
procedure CaricaApriClienti(xStringa, xFiltro: string; xTestoCont: boolean = False);
procedure CaricaApriRicPend(xIDRicerca: Integer);
function SpreadString(s: string): TStringList; // trasforma stringa con CR in StringList
function AccodaSMSaDispatcher(xIDAnag, xIDRic, xIDUtente, xIDCliente: integer; xMessaggio, xDispatcher: string; xStorico: boolean = True): boolean;
function CreaFileWord(xModelloWord, xTabellaMaster: string; xIDAnag: integer): string;
procedure Log_Operation(xIDUtente: integer; xTabella: string; xKeyValue: integer; xOperation: string; xNote: string = '');
//function CreaFileWordAnag(xIDModello: integer; xModelloWord: string; xSalva, xStampa: boolean): string;
function CreaFileWordAnag(xIDAnag, xIDModello: integer; xModelloWord: string; xSalva, xStampa: boolean; struttura: TModelCompositionTV): string;
function OpenSelFromTab(xTable, xKeyField, xFieldName, xFieldTitle, xID: string; xModify: boolean = True): string;
function OpenSelFromQuery(xQuery, xKeyField, xFieldName, xFieldTitle, xID: string; xModify: boolean = True): string;
function SelFromQueryMultiCampi(xQuery, xKeyField: string; xFieldName, xFieldTitle: array of string; xID: string; xOrder: string = ''; xModify: boolean = false): string;
function VerifIntegrRef(xTable, xFKField, xID: string; xMessStop: boolean = True): boolean;
procedure ApriCV(xIDAnag: integer);
function IncompAnagCli(xIDAnag, xIDCliente: integer): boolean;
procedure OpenTab(xTable: string; xFields, xFieldtitles: array of string; xModify: boolean = True; xOrder: string = ''; xParGenerico: string = ''; titolo: string = 'Tabella'; xAltezzaLabel: Integer = 0);
function CheckAnagInseritoBlocco1(xIDAnag: integer): integer;
function GetIDEvento(xDic: string): integer;
function GetPwdUtenteResp(xIDUtente: integer): string;
function GetPwdAdministrator: string;
function GetPwdUtente(xIDUtente: integer): string;
function GetIDUtenteResp(xIDUtente: integer): integer;
function GetDescUtenteResp(xIDUtente: integer): string;
function GetDescCliente(xIDCliente: integer): string;
function GetCodRicerca: string;
function GetCodOfferta: string;
function GetDocPath: string;
function GetWinTempPath: string;
function GetCVPath: string;
function GetIDAnagUtente(xIDUtente: integer): integer;
procedure CancellaSogg(xIDAnag: integer);
procedure ScriviRegistry(xChiave, xValore: string);
function LeggiRegistry(xChiave: string): string;
function GetFunctionName(xCodModulo: string): string;
procedure CheckVisionati(xIDSogg, xIDEdizione: integer; xMessage: boolean);
procedure MailTo(xAddress, xObject, xBody: string);
function SendEMail(Handle: THandle; Mail: TStrings): Cardinal;
procedure RegistraVisioneCand(xIDutente, xIDAnagrafica: integer);
function CheckVisioneCand(xIDutente, xIDAnagrafica: integer): boolean;
function CopiaFileAll(xFileName: string; xRename: Boolean; TipoTabPercorsoDir: string; xidnomefile: integer): string;
procedure ColoraRiga(xIDUtente, xcolore, xValore: integer; xCampo: string);
function GetCodFattura: string;
function CheckRicercheInUso(xIDRicerca, xIDUtente: integer): string;
procedure InsertRicercheInUso(xIDRicerca, xIDUtente: integer);
procedure DeleteRicercheInUso(xIDRicerca, xIDUtente: integer);
function GetPathFromTable(xTipo: string): string;
function CheckAssociazCandCompetenza(IDAnagrafica, IDCompetenza: integer): integer;
procedure ApriHelp(xCodice: string);
procedure InsertStoricoNoteQCandRic(xIDUtente, xIDCandRic, xCampo: string; xDataora: TDateTime);
procedure ShellExecute_AndWait(FileName: string; Params: string);
//procedure salvafile(formchiamante: string);
//procedure aprifile(formchiamante: string);
procedure BloccaAnagFile(xCheck: boolean; chiamante: string);
procedure aprifileDue(IDFile: integer; formchiamante: string);
procedure salvafileDue(IDfile: integer; formchiamante: string);
procedure AggiornaPesiAnnunci(xID: integer; xchiamante: string = 'AnnunciRicerca');
function CreateGuid: string;
function GeneraUsername(xemail, xcognome, xnome, xusername: string): string;
function DataModificaFile(xFile: string): TDateTime;
function MessaggioATempo(Messaggio, Titolo: string; Bottoni: TMsgDlgBtns; xtempo: integer): TModalResult;
procedure Delay(Millisec: Longint);
implementation

procedure CaricaPrimi;
var xMax, i: integer;
begin
   //  if MainForm.xMaxCons <> -1 then begin
     xMax := MainForm.xMaxCons;
     if xMax <> 999999 then
     begin
          for i := 1 to xMax do xArrayIDAnag[i] := 0;
          Data.TAnagABS.Open;
          for i := 1 to xMax do begin
               xArrayIDAnag[i] := Data.TAnagABS.FieldByName('ID').AsInteger;
               Data.TAnagABS.Next;
               if Data.TAnagABS.EOF then break;
          end;
          Data.TAnagABS.Close;
     end;
   //  end;
end;

procedure SettaDiritti(xID: integer);
var i: integer;
     xTrovato: boolean;
begin
     if MainForm.xMaxCons <> -1 then begin
          if (MainForm.xMaxCons = 999999) or (xID = 0) then
               xTrovato := True
          else begin
               xTrovato := False;
               for i := 1 to MainForm.xMaxCons do begin
                    if xArrayIDAnag[i] = xID then begin
                         xTrovato := True;
                         break;
                    end;
               end;
          end;
     end else xTrovato := true;

     if xTrovato then begin
          MainForm.PageControl2.Visible := True;
          MainForm.TbAnag.Visible := MainForm.VisibCavalieri;
          if (Uppercase(copy(Data.Global.FieldByName('NomeAzienda').Value, 1, 6)) = 'ADVANT') or (pos('MCS', UpperCase(Data.Global.FieldByName('NomeAzienda').Value)) > 0) then
               MainForm.TbAnag.Visible := False;
     end else begin
          MainForm.PageControl2.Visible := False;
          MainForm.TbAnag.Visible := False;
     end;
end;

function OkIDAnag(xID: integer): boolean;
var i: integer;
     xTrovato: boolean;
begin
     if MainForm.xMaxCons <> -1 then begin
          xTrovato := true;
          if pos('INTERMEDIA', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) = 0 then begin
              // if MainForm.xMaxCons <> -1 then
               //     xTrovato := True
              // else begin
               xTrovato := False;
               for i := 1 to MainForm.xMaxCons do begin
                    if xArrayIDAnag[i] = xID then begin
                         xTrovato := True;
                         break;
                    end;
               end;
             //  end;
               if xTrovato = false then
                    MessageDlg('Il soggetto non rientra nel numero di quelli consentiti' + chr(13) + 'IMPOSSIBILE PROSEGUIRE', mtError, [mbOK], 0);
          end;
     end else xTrovato := true;

     if xTrovato then
          Result := True
     else
          Result := False;
end;

procedure ApriFileWord(xNumCv: string);
var xFileWord: string;
     MSWord: Variant;
     xApriWord, xNuovoFile: boolean;
begin
     if xNumCv = '' then MessageDlg('Numero CV mancante - � necessario averlo', mtError, [mbOK], 0)
     else begin
          {if Data.Global.FieldByName('AnagFileDentroDB').AsBoolean then begin

               xApriWord := True;
               xNuovoFile := False;
               xFileWord := '\w' + xNumCv + '.doc';

               Data.Q4.Close;
               Data.Q4.SQL.Text := 'select * from anagfile where nomefile = ' + xFileWord;
               Data.Q4.Open;
               if Data.Q4.RecordCount = 0 then
                    if MessageDlg('Il file non esiste - Vuoi crearlo ?', mtWarning, [mbYes, mbNo], 0) = mrYes then begin
                         xApriWord := True;
                         xNuovoFile := True;
                    end else xApriWord := False;

               if xApriWord then begin
                    try MsWord := CreateOleObject('Word.Basic');
                    except
                         ShowMessage('Non riesco ad aprire Microsoft Word.');
                         exit;
                    end;
                    MsWord.AppShow;
                    if xNuovoFile then MsWord.FileNew
                    else MsWord.FileOpen(xFileWord);

                    //Salvataggio
                    if not Data.Q4.FieldByName('FileBloccato').AsString then begin
                         MessageDlg('Il file selezionato non � aperto', mtError, [mbOK], 0);
                         exit;
                    end;

                    try
                         Data.QAnagFileFile.Close;
                         Data.QAnagFileFile.Parameters[0].value := Data.QAnagFileID.Value;
                         Data.QAnagFileFile.Open;
                         Data.QAnagFileFile.Edit;
          //Data.QAnagFileFileFileContent.LoadFromFile(xFileName);
                         Data.QAnagFileFileDocFile.LoadFromFile(xFileName);
                         Data.QAnagFileFileDocExt.Value := Stringreplace(ExtractFileExt(xFileName), '.', '', [rfReplaceAll]);
                         Data.QAnagFileFileFileBloccato.value := False;
                         Data.QAnagFileFile.Post;
                         Data.QAnagFileFile.Close;
                    except
                         on e: Exception do begin
                              if copy(e.message, 1, 16) = 'Cannot open file' then
                                   MessageDlg('ERRORE: problemi a salvare il file ' + xFileName + chr(13) + chr(13) +
                                        'Possibili cause:' + chr(13) +
                                        '- il file potrebbe essere aperto da un''altra applicazione: occorre chiuderla;' + chr(13) +
                                        '- il file potrebbe essere stato cancellato dalla cartella', mtError, [mbOK], 0)
                              else
                                   MessageDlg('ERRORE: ' + e.message, mtError, [mbOK], 0);

                              Data.QAnagFileFile.Cancel;
                              exit;
                         end;
                    end;

                    FileDelete(xFileName, False);
                    ShowMessage('Salvataggio del file completato');


               end else begin  }
          xApriWord := True;
          xNuovoFile := False;
          xFileWord := GetDocPath + '\w' + xNumCv + '.doc';
          if not FileExists(xFileWord) then
               if MessageDlg('Il file non esiste - Vuoi crearlo ?', mtWarning, [mbYes, mbNo], 0) = mrYes then begin
                    xApriWord := True;
                    xNuovoFile := True;
               end else xApriWord := False;

          if xApriWord then begin
               try MsWord := CreateOleObject('Word.Basic');
               except
                    ShowMessage('Non riesco ad aprire Microsoft Word.');
                    exit;
               end;
               MsWord.AppShow;
               if xNuovoFile then begin
                    MsWord.FileNew;
                    MsWord.FileSaveAs(xFileWord);
               end else MsWord.FileOpen(xFileWord);
               MsWord.FileSaveAs(xFileWord);
          end;
     end;
          //end;
//end;
end;

procedure ApriFileWordFromFile(xNomeFile, xNumCv: string);
var xFileWord: string;
     MSWord: Variant;
     xApriWord, xNuovoFile: boolean;
begin
     {xFileWord := xNomeFile;

     if not FileExists(xFileWord) then begin
          MessageDlg('Il file non esiste', mtError, [mbOK], 0);
          Exit;
     end;
     try MsWord := CreateOleObject('Word.Basic');
     except
          ShowMessage('Non riesco ad aprire Microsoft Word.');
          exit;
     end;
     MsWord.AppShow;
     MsWord.FileOpen(xFileWord); }
     if xNumCv = '' then MessageDlg('Numero CV mancante - � necessario averlo', mtError, [mbOK], 0)
     else begin

          xApriWord := True;
          xNuovoFile := False;
          xFileWord := GetDocPath + '\w' + xNumCv + '.doc';
          if not FileExists(xFileWord) then
               if MessageDlg('Il file non esiste - Vuoi crearlo ?', mtWarning, [mbYes, mbNo], 0) = mrYes then begin
                    xApriWord := True;
                    xNuovoFile := True;
               end else xApriWord := False;

          if xApriWord then begin
               try MsWord := CreateOleObject('Word.Basic');
               except
                    ShowMessage('Non riesco ad aprire Microsoft Word.');
                    exit;
               end;
               MsWord.AppShow;
               if xNuovoFile then MsWord.FileOpen(xNomeFile)
               else MsWord.FileOpen(xFileWord);
               MsWord.FileSaveAs(xFileWord);
          end;

     end;
end;

function CreateQueryFromString(s: string): TAdoLinkedQuery;
begin

     Data.QTemp.close;
     Data.QTemp.SQL.text := s;
     Result := Data.QTemp;
end;

//[/TONI20020722\]

function PosizionaAnag(xIDAnag: integer): boolean;
begin
     {   if Data.TAnagrafica.Active then Data.TAnagrafica.Open;
          Data.TAnagrafica.SQL.clear;
          Data.TAnagrafica.SQL.text:=xAnagSQL+' '+'AND Anagrafica.ID=:xID';
          Data.TAnagrafica.Prepare;
          Data.TAnagrafica.ParamByName('xID').asInteger:=xIDAnag;
          Data.TAnagrafica.Open;
          Result:=not Data.TAnagrafica.isEmpty;}

     if Data.TAnagrafica.Active then Data.TAnagrafica.Close; //Chiudi Anagrafica
     if Data.TAnagrafica.State = dsInactive then //controlla stato
     begin
          Data.TAnagrafica.SQL.clear;
          //Posiziona su dato
          Data.TAnagrafica.SQL.text := xAnagSQL + ' ' + 'AND Anagrafica.ID=' + IntToStr(xIDAnag);
          MainForm.Edit1.Text := '';
          MainForm.ENome.Text := '';
          Data.TAnagrafica.Open;
          MainForm.Edit1.Text := Data.TAnagrafica.FieldByName('Cognome').asString;
          MainForm.ENome.Text := Data.TAnagrafica.FieldByName('Nome').asString;
          Result := not Data.TAnagrafica.isEmpty;
     end
     else Result := FALSE;
end;
//[/TONI20020723\]FINE

function DammiCVNumero(xIDAnag: integer): integer;
begin
     Data.Q1.Close;
     Data.Q1.SQl.Text := 'select CVNumero from Anagrafica where ID=' + IntToStr(xIDAnag);
     Data.Q1.Open;
     if Data.Q1.RecordCount = 0 then Result := 0
     else Result := Data.Q1.FieldByName('CVNumero').asInteger;
     Data.Q1.Close;
end;

//[/TONI20020723\]

procedure CaricaApriAnag(xStringa, xNome: string);
begin
     {  if Data.TAnagrafica.Active then Data.TAnagrafica.Close;
        Data.TAnagrafica.SQL.clear;
        if xNome='' then
          Data.TAnagrafica.SQL.Add('AND Cognome LIKE :xCerca ORDER BY Cognome,Nome')
        else Data.TAnagrafica.SQL.Add('AND Cognome LIKE :xCerca AND Nome LIKE :xNome ORDER BY Cognome,Nome');
        Data.TAnagrafica.Prepare;
        Data.TAnagrafica.ParamByName('xCerca').asString:=xStringa+'%';
        if xNome<>'' then
             Data.TAnagrafica.ParamByName('xNome').asString:=xNome+'%';
        Data.TAnagrafica.Open;
        if not Data.QAnagNote.active then Data.QAnagNote.Open;}

     if Data.TAnagrafica.Active then Data.TAnagrafica.Close;
     if Data.TAnagrafica.State = dsInactive then
     begin
          Data.TAnagrafica.SQL.clear;
         // if xNome = '' then
             //  Data.TAnagrafica.SQL.Text := xAnagSQL + ' AND Cognome LIKE ''' + StringReplace(xStringa, '''', '''''', [rfreplaceall]) + '%'' ORDER BY Cognome,Nome';

         // if xstringa = '' then
          Data.TAnagrafica.SQL.Text := xAnagSQl + ' AND Cognome LIKE ''' + StringReplace(xStringa, '''', '''''', [rfreplaceall]) + '%'' AND Nome LIKE ''' + StringReplace(xNome, '''', '''''', [rfreplaceall]) + '%'' ORDER BY Cognome,Nome';

          //if xnome <> '' then
            //   Data.TAnagrafica.SQL.Text := xAnagSQl + ' AND Nome LIKE ''' + StringReplace(xNome, '''', '''''', [rfreplaceall]) + '%'' ORDER BY Cognome,Nome';



          Data.TAnagrafica.Filter := 'IDTipoStato<>6';
          Data.TAnagrafica.Filtered := true;
          Data.TAnagrafica.Open;
          //Non ce ne dovrebbe pi� essere bisogno..ma cmq si lascia per sicurezza
          if not Data.QAnagNote.active then Data.QAnagNote.Open;
     end
end;
//[/TONI20020723\]FINE

//[/TONI20020723\]

procedure CaricaApriClienti(xStringa, xFiltro: string; xTestoCont: boolean = False);
begin
     {     if Data2.TEBCClienti.Active then Data2.TEBCClienti.Close;
          Data2.TEBCClienti.SQL.clear;
          Data2.TEBCClienti.SQL.text:=xClientiSQL;
          if xFiltro<>'' then
               Data2.TEBCClienti.SQL.Add('AND EBC_Clienti.Stato=:xFiltro');
          Data2.TEBCClienti.SQL.Add('AND EBC_Clienti.Descrizione LIKE :xCerca '+
               'ORDER BY EBC_Clienti.Descrizione');
          Data2.TEBCClienti.Prepare;
          if xTestoCont then
               Data2.TEBCClienti.ParamByName('xCerca').asString:='%'+xStringa+'%'
          else Data2.TEBCClienti.ParamByName('xCerca').asString:=xStringa+'%';
          if xFiltro<>'' then
               Data2.TEBCClienti.ParamByName('xFiltro').asString:=xFiltro;
          Data2.TEBCClienti.Open;
          MainForm.LTotAz.Caption:=IntToStr(Data2.TEBCclienti.RecordCount);}
     if Data2.TEBCClienti.Active then Data2.TEBCClienti.Close; //Chiudi Query
     Data2.TEBCClienti.SQL.clear;
     Data2.TEBCClienti.SQL.text := xClientiSQL;
     if xFiltro <> '' then
          Data2.TEBCClienti.SQL.text := xClientiSQL + ' AND EBC_Clienti.Stato=''' + xFiltro + '''';
     if MainForm.xSoloClientiCollegati then
          //Global.FieldByName('SoloClientiCollegati').AsBoolean then
          Data2.TEBCClienti.SQL.Add(' AND EBC_Clienti.ID in (select idcliente from clientelinkutenti where idutente =' + inttostr(MainForm.xIDUtenteAttuale) + ')');
     if xTestoCont then
          Data2.TEBCClienti.SQL.Add(' AND EBC_Clienti.Descrizione LIKE ''%' + StringReplace(xStringa, '''', '''''', [rfreplaceall]) + '%'' ORDER BY EBC_Clienti.Descrizione')
     else
          Data2.TEBCClienti.SQL.Add(' AND EBC_Clienti.Descrizione LIKE ''' + StringReplace(xStringa, '''', '''''', [rfreplaceall]) + '%'' ORDER BY EBC_Clienti.Descrizione');
     Data2.TEBCClienti.Open;

  //   Data2.TEBCClienti.sql.savetofile('qclienti.sql');

end;

procedure CaricaApriRicPend(xIDRicerca: Integer);
begin
     if DataRicerche.TRicerchePend.Active then DataRicerche.TRicerchePend.Close;
     DataRicerche.TRicerchePend.SQL.clear;
     DataRicerche.TRicerchePend.SQL.text := xRicPendSQL + ' AND EBC_Ricerche.ID=' + IntToStr(xIDRicerca);
     DataRicerche.TRicerchePend.Open;
end;

function SpreadString(s: string): TStringList;
var xSl: TStringList;
     xS: string;
     xCRpos: integer;
begin
     xS := s;
     xSl := TStringList.create;
     while pos(chr(13), xS) > 0 do begin
          xCRpos := pos(chr(13), xS);
          xSl.Add(copy(xS, 1, xCRpos - 1));
          xS := copy(xS, xCRpos + 1, length(xS));
     end;
     Result := xSl;
     xsl.Free;
end;

function AccodaSMSaDispatcher(xIDAnag, xIDRic, xIDUtente, xIDCliente: integer; xMessaggio, xDispatcher: string; xStorico: boolean): boolean;
var MyStringList: TStrings;
     i: integer;
     xAliasExists: boolean;
     xCellulare, xPrefissoSZ, xNumero: string;
begin
     ShowMessage('funzione disabilitata');
     Result := False;
     exit;

end;

function CreaFileWord(xModelloWord, xTabellaMaster: string; xIDAnag: integer): string;
var xFileModello, xFileWord: string;
     xAnno, xMese, xGiorno: Word;
     MSWord: Variant;
     xApriWord, xNuovoFile: boolean;
     xIDModello: integer;
begin
     if not FileExists(GetDocPath + '\' + xModelloWord + '.doc') then begin
          MessageDlg('Il modello non esiste o la cartella non � impostata correttamente' + chr(13) +
               'IMPOSSIBILE PROSEGUIRE', mtError, [mbOK], 0);
          Result := '';
          exit;
     end;
     with Data.QTemp do begin
          Close;
          SQL.text := 'select * from ModelliWord where NomeModello=''' + xModelloWord + ''' and TabellaMaster=''' + xTabellaMaster + '''';
          Open;
          DecodeDate(Date, xAnno, xMese, xGiorno);
          xFileWord := GetDocPath + '\' +
               FieldbyName('InizialiFileGenerato').asString + IntToStr(xIDAnag) +
               '-' + IntToStr(xGiorno) + IntToStr(xMese) + '.doc';
          xIDModello := FieldbyName('ID').asInteger;
          // controllo esistenza file
          if FileExists(xFileWord) then
               if MessageDlg('Il file esiste gi�: vuoi ricrearlo ?', mtWarning, [mbYes, mbNo], 0) = mrNo then begin
                    Result := '';
                    exit;
               end;
          // Apri Word
          try MsWord := CreateOleObject('Word.Basic');
          except
               ShowMessage('Non riesco ad aprire Microsoft Word.');
               exit;
          end;
          MsWord.AppShow;
          // apri modello
          MsWord.FileOpen(GetDocPath + '\' + xModelloWord + '.doc');
          // riempimento campi
          Close;
          SQL.text := 'select NomeCampo,Campo,TipoCampo from ModelliWordCampi where IDModello=' + IntToStr(xIDModello);
          Open;
          while not EOF do begin
               Data.QTemp.Close;
               Data.QTemp.SQL.text := 'select ' + FieldByName('Campo').asString + ' ValoreCampo from ' + xTabellaMaster + ' where ID=' + IntToStr(xIDAnag);
               Data.QTemp.Open;
               MsWord.EditFind(FieldbyName('NomeCampo').asString);
               if FieldbyName('TipoCampo').asString = 'string' then
                    MsWord.Insert(Data.QTemp.FieldbyName('ValoreCampo').asString);
               if FieldbyName('TipoCampo').asString = 'datetime' then
                    MsWord.Insert(DateToStr(Data.QTemp.FieldbyName('ValoreCampo').asDateTime));
               if FieldbyName('TipoCampo').asString = 'integer' then
                    MsWord.Insert(IntToStr(Data.QTemp.FieldbyName('ValoreCampo').asInteger));
               Next;
          end;
          Close;
          Data.QTemp.Close;
          // salvataggio
          MsWord.FileSaveAs(xFileWord);
          Result := xFileWord;
     end;

end;
//[/TONI20020724\]FINE

//[TONI20020923]DEBUGOK

procedure Log_Operation(xIDUtente: integer; xTabella: string; xKeyValue: integer; xOperation: string; xNote: string = '');
begin
     // inserisci l'operazione nel log delle operazioni
     // ATTENZIONE: LA TRANSAZIONE DOVREBBE GIA' ESSERE IN CORSO !!
    //showmessage(xnote);
     if xKeyValue = 0 then exit;
     with Data do begin
          {Q1.Close;
          Q1.SQL.text := 'insert into Log_TableOp (IDUtente,DataOra,Tabella,KeyValue,Operation,Annotazioni) ' +
               'values (:xIDUtente:,:xDataOra:,:xTabella:,:xKeyValue:,:xOperation:,:xAnnotazioni:)';
          Q1.ParamByName['xIDUtente'] := xIDUtente;
          Q1.ParamByName['xDataOra'] := Now;
          Q1.ParamByName['xTabella'] := xTabella;
          Q1.ParamByName['xKeyValue'] := xKeyValue;
          Q1.ParamByName['xOperation'] := TrimRight(xOperation);
          Q1.ParamByName['xAnnotazioni'] := TrimRight(copy(xNote, 1, 100));
          Q1.ExecSQL;  }
          Q1.Close;
          Q1.SQL.text := //'set dateformat dmy '+
               'insert into Log_TableOp (IDUtente,DataOra,Tabella,KeyValue,Operation,Annotazioni) ' +
               'values (:xIDUtente,:xDataOra,:xTabella,:xKeyValue,:xOperation,:xAnnotazioni)';
          Q1.Parameters.paramByName('xIDUtente').Value := xIDUtente;
          Q1.Parameters.paramByName('xDataOra').Value := Now;
          Q1.Parameters.paramByName('xTabella').Value := xTabella;
          Q1.Parameters.paramByName('xKeyValue').Value := xKeyValue;
          Q1.Parameters.paramByName('xOperation').Value := xOperation;
          Q1.Parameters.paramByName('xAnnotazioni').Value := xNote;
          Q1.ExecSQL;
     end;
end;
//[TONI20020923]DEBUGFINE

//[/TONI20020724\]

{function CreaFileWordAnag(xIDModello: integer; xModelloWord: string; xSalva, xStampa: boolean): string;
var xFileModello, xFileWord: string;
  xAnno, xMese, xGiorno: Word;
  MSWord: Variant;
  xApriWord, xNuovoFile: boolean;
begin
  if not FileExists(GetDocPath + '\' + trim(xModelloWord) + '.doc') then begin
    MessageDlg('Il modello non esiste o la cartella non � impostata correttamente' + chr(13) +
      'IMPOSSIBILE PROSEGUIRE', mtError, [mbOK], 0);
    Result := '';
    exit;
  end;
  with Data.QTemp do begin
    Close;
    SQL.text := 'select * from ModelliWord where ID=' + IntToStr(xIDModello);
    Open;
    DecodeDate(Date, xAnno, xMese, xGiorno);
    if Data.QTemp.FieldByName('InizialiFileGenerato').asString = 'iw' then
      xFileWord := GetDocPath + '\CandInteressanti\' +
        FieldbyName('InizialiFileGenerato').asString + IntToStr(Data.TAnagrafica.FieldByName('ID').AsInteger) +
        '-' + IntToStr(xGiorno) + IntToStr(xMese) + IntToStr(xAnno) + '.doc'
    else
      xFileWord := GetDocPath + '\' +
        FieldbyName('InizialiFileGenerato').asString + IntToStr(Data.TAnagrafica.FieldByName('ID').AsInteger) +
        '-' + IntToStr(xGiorno) + IntToStr(xMese) + IntToStr(xAnno) + '.doc';
    // Apri Word
    try MsWord := CreateOleObject('Word.Basic');
    except
      ShowMessage('Non riesco ad aprire Microsoft Word.');
      exit;
    end;
    MsWord.AppShow;
    // apri modello
    MsWord.FileOpen(GetDocPath + '\' + xModelloWord + '.doc');
    // riempimento campi
    Close;
    SQL.text := 'select NomeCampo,Campo,TipoCampo from ModelliWordCampi where IDModello=''' + INtToStr(xIDModello) + ''' order by ID';
    Open;
    while not EOF do begin
      MsWord.EditFind(FieldbyName('NomeCampo').asString);
      if Data.TAnagrafica.FieldbyName(FieldByName('Campo').asString).asString <> '' then
        MsWord.Insert(Data.TAnagrafica.FieldbyName(FieldByName('Campo').asString).asString)
      else MsWord.Insert('---');
      Next;
    end;
    Close;
    Data.QTemp.Close;

    if xSalva then
      // salvataggio
      MsWord.FileSaveAs(xFileWord);
    Result := xFileWord;
    if xStampa then begin
      MsWord.FilePrint;
      MsWord.FileClose;
      MsWord.AppClose;
    end;
  end;
end;}

function CreaFileWordAnag(xIDAnag, xIDModello: integer; xModelloWord: string; xSalva, xStampa: boolean; struttura: TModelCompositionTV): string;
var xFileModello, xFileWord, xTabella: string;
     xAnno, xMese, xGiorno: Word;
     MSWord: Variant;
     xApriWord, xNuovoFile: boolean;
     q: TADOQuery;
begin
     if not FileExists(Data.Global.FieldByName('DirFileDoc').Value + '\' + xModelloWord + '.doc') then begin
          MessageDlg('Il modello non esiste o la cartella non  impostata correttamente' + chr(13) +
               'IMPOSSIBILE PROSEGUIRE', mtError, [mbOK], 0);
          Result := '';
          exit;
     end;
     q := TADOQuery.Create(nil);
     q.Connection := Data.DB;
     q.sql.text := 'select * from Modellidefinizioni where idmodello=' + IntToStr(xIDModello);
     q.Open;
     with Data do
     begin
          QTemp.Close;
          QTemp.SQL.text := 'select * from ModelliWord where ID=' + IntToStr(xIDModello);
          QTemp.Open;
          DecodeDate(Date, xAnno, xMese, xGiorno);
          xFileWord := Data.Global.FieldByName('DirFileDoc').Value + '\' +
               QTemp.FieldbyName('InizialiFileGenerato').asString + IntToStr(xIDAnag) +
               '-' + IntToStr(xGiorno) + IntToStr(xMese) + '.doc';
          if q.Isempty then
          begin
               Q1.Close;
               Q1.SQL.text := 'select * from Anagrafica,AnagAltreInfo ' +
                    ' where Anagrafica.ID=AnagAltreInfo.IDAnagrafica ' +
                    ' and Anagrafica.ID=' + IntToStr(xIDAnag);
               Q1.Open;
               // Apri Word
               try MsWord := CreateOleObject('Word.Basic');
               except
                    ShowMessage('Non riesco ad aprire Microsoft Word.');
                    exit;
               end;
               MsWord.AppShow;
               // apri modello
               //Showmessage(Data.Global.FieldByName ('DirFileDoc').Value + '\' + xModelloWord + '.doc');
               MsWord.FileOpen(GetDocPath + '\' + xModelloWord + '.doc');
               //MsWord.FileName('asddas');
               // riempimento campi
               QTemp.Close;
               QTemp.SQL.text := 'select NomeCampo,Campo,TipoCampo from ModelliWordCampi where IDModello=:xIDMod order by ID';
               QTemp.Parameters.ParamByName('xIDMod').Value := xIDModello;
               QTemp.Open;
               while not QTemp.EOF do
               begin
                    MsWord.EditFind(TrimRight(QTemp.FieldbyName('NomeCampo').asString));
                    // Anagrafica o AnagAltreInfo
                    if Q1.FieldbyName(TrimRight(QTemp.FieldByName('Campo').asString)).asString <> '' then
                         MsWord.Insert(Q1.FieldbyName(TrimRight(QTemp.FieldByName('Campo').asString)).asString)
                    else MsWord.Insert('---');
                    QTemp.Next;
               end;
               QTemp.Close;
               if xSalva then begin
                    // salvataggio
                    MsWord.FileSaveAs(xFileWord);
                    MsWord.FileClose;
                    MsWord.AppClose;
               end;
               Result := xFileWord;
               if xStampa then begin
                    MsWord.FilePrint;

               end;
          end
          else //Struttura!
          begin
               struttura.Parent := TWinControl(struttura.Owner);
               struttura.Connection := q.Connection;
               struttura.CaricaSchemaModello(xidModello);
               Result := struttura.GeneraModello(Data.Global.FieldByName('DirFileDoc').Value + '\' + xModelloWord + '.doc', xFileWord, xIDAnag);

               struttura.free;
               q.close;
               q.free;
          end;
     end
end;


//[/TONI20020724\]FINE

//[TONI20020923]DEBUGOK

function OpenSelFromTab(xTable, xKeyField, xFieldName, xFieldTitle, xID: string; xModify: boolean = True): string;
begin
     // apre una form di selezione con una tabella che visualizza UN campo solo
     // Usage:  xTable = tabella da cui selezionare
     //         xKeyField = campo chiave della tabella
     //         xFieldName = campo da visualizzare
     //         xFieldTitle = titolo per la griglia
     //         xID = valore del campo chiave (per la modifica: si posiziona sull'ID)
     //               � stringa perch� cos� va bene per gli interi e per le stringhe.
     // --> restituisce il campo chiave come stringa (cos� va bene per gli interi e per le stringhe)
     // --> imposta inoltre la variabile globale "MainForm.xgenericString" con il valore di xFieldName
     Application.CreateForm(TSelTabGenForm, SelTabGenForm);
     SelTabGenForm.FrameDB1.serviceform := FrameDBForm;
     SelTabGenForm.xFieldName := xFieldName;
     SelTabGenForm.xFieldTitle := xFieldTitle;
     SelTabGenForm.QTab.SQL.text := 'select ' + xKeyField + ',' + xFieldName + ' from ' + xTable;
     SelTabGenForm.QTab.Open;
     if xID <> '' then
          while (SelTabGenForm.QTab.fieldbyName(xKeyField).asString <> xID) and
               (not SelTabGenForm.QTab.EOF) do SelTabGenForm.QTab.Next;
     SelTabGenForm.xModify := xModify;
     SelTabGenForm.showmodal;
     MainForm.xgenericString := '';
     if SelTabGenForm.Modalresult = mrOK then begin
          Result := SelTabGenForm.QTab.fieldbyName(xKeyField).asstring;
          MainForm.xgenericString := TrimRight(SelTabGenForm.QTab.fieldbyName(xFieldName).asstring);
     end else Result := '';
     SelTabGenForm.Free;
end;

function OpenSelFromQuery(xQuery, xKeyField, xFieldName, xFieldTitle, xID: string; xModify: boolean = True): string;
begin
     // apre una form di selezione con una tabella che visualizza UN campo solo
     // Usage:  xQuery = testo della query
     //         xKeyField = campo chiave della tabella
     //         xFieldName = campo da visualizzare
     //         xFieldTitle = titolo per la griglia
     //         xID = valore del campo chiave (per la modifica: si posiziona sull'ID)
     //               � stringa perch� cos� va bene per gli interi e per le stringhe.
     // --> restituisce il campo chiave come stringa (cos� va bene per gli interi e per le stringhe)
     // --> imposta inoltre la variabile globale "MainForm.xgenericString" con il valore di xFieldName
     Application.CreateForm(TSelTabGenForm, SelTabGenForm);
     SelTabGenForm.FrameDB1.serviceform := FrameDBForm;
     SelTabGenForm.xFieldName := xFieldName;
     SelTabGenForm.xFieldTitle := xFieldTitle;
     SelTabGenForm.QTab.SQL.text := xQuery;
     SelTabGenForm.QTab.Open;
     if xID <> '' then
          while (SelTabGenForm.QTab.fieldbyName(xKeyField).asString <> xID) and
               (not SelTabGenForm.QTab.EOF) do SelTabGenForm.QTab.Next;
     SelTabGenForm.xModify := xModify;
     SelTabGenForm.showmodal;
     MainForm.xgenericString := '';
     if SelTabGenForm.Modalresult = mrOK then begin
          Result := SelTabGenForm.QTab.fieldbyName(xKeyField).asstring;
          MainForm.xgenericString := TrimRight(SelTabGenForm.QTab.fieldbyName(xFieldName).asstring);
     end else Result := '';
     SelTabGenForm.Free;
end;


procedure ApriCV(xIDAnag: integer);
var xFile, xFileName: string;
     xViewCV, xProcedi: boolean;
begin
     if MainForm.CheckProfile('053') then
     begin
          xCheckVisionato := false;
          Data.Q1.Close;
          Data.Q1.SQL.text := 'select CVNumero,Cognome,Nome from Anagrafica ' +
               'where Anagrafica.ID=' + IntToStr(xIDAnag);
          Data.Q1.Open;
          xFile := GetCVPath + '\i' + Data.Q1.FieldByName('CVNumero').asString + 'a.gif';
          if FileExists(xFile) then xViewCV := True
          else begin
               // se esiste in altri formati (TIFF, BMP e PDF)
               xFile := GetCVPath + '\i' + Data.Q1.FieldByName('CVNumero').asString + 'a.tif';
               if FileExists(xFile) then begin
                    ShellExecute(0, 'Open', pchar(xFile), '', '', SW_SHOW);
                    exit;
               end;
               xFile := GetCVPath + '\i' + Data.Q1.FieldByName('CVNumero').asString + '.tif';
               if FileExists(xFile) then begin
                    ShellExecute(0, 'Open', pchar(xFile), '', '', SW_SHOW);
                    exit;
               end;
               xFile := GetCVPath + '\i' + Data.Q1.FieldByName('CVNumero').asString + 'a.bmp';
               if FileExists(xFile) then begin
                    ShellExecute(0, 'Open', pchar(xFile), '', '', SW_SHOW);
                    exit;
               end;
               xFile := GetCVPath + '\i' + Data.Q1.FieldByName('CVNumero').asString + 'a.pdf';
               if FileExists(xFile) then begin
                    ShellExecute(0, 'Open', pchar(xFile), '', '', 0);
                    exit;
               end;
               xViewCV := False;
               Application.CreateForm(TSceltaAnagFileForm, SceltaAnagFileForm);

               //DEBUG
               //SceltaAnagFileForm.QAnagFile.ReloadSQL;
         //               SceltaAnagFileForm.QAnagFile.ParamByName['xIDAnag']:=xIDAnag;
               //DEBUG FINE
               {
              SceltaAnagFileForm.QAnagFile.SQL.text := 'select TipiFileCand.Tipo,AnagFile.* ' +
                    'from AnagFile,TipiFileCand ' +
                    'where AnagFile.IDTipo *= TipiFileCand.ID ' +
                    'and AnagFile.IDAnagrafica=' + IntToStr(xIDAnag) + ' ' +
                    'order by DataCreazione desc ';
               SceltaAnagFileForm.QAnagFile.Open;
               }
               //if SceltaAnagFileForm.QAnagFile.IsEmpty then begin

               data.QAnagFileApriCV.close;
               data.QAnagFileApriCV.Parameters.parambyname('xIDAnagrafica').Value := xIDAnag;
               data.QAnagFileApriCV.open;


               if data.QAnagFileApriCV.Recordset.RecordCount = 0 then begin
                    if MessageDlg('Nessuna immagine e nessun file associato ' + chr(13) +
                         'Aprire comunque per la scannerizzazione ?', mtWarning, [mbYES, mbNO], 0) = mrYes then
                         xViewCV := True;
               end else begin
                    xCheckVisionato := true;
                    SceltaAnagFileForm.Show;
                   // if SceltaAnagFileForm.QAnagFile.RecordCount = 1 then begin
                   // if data.QAnagFile.Recordset.RecordCount = 1 then begin
                         // se ce n'� uno solo, apri direttamente questo
                         //xFileName := SceltaAnagFileForm.QAnagFile.FieldByName('NomeFile').asString;
                        // xFileName := data.QAnagFile.fieldbyname('nomefile').asstring;
                        // ShellExecute(0, 'Open', pchar(xFileName), '', '', SW_SHOWDEFAULT);
                   // end else begin
                         // altrimenti scegli tra quelli nell'elenco

                        { if SceltaAnagFileForm.ModalResult = mrOK then begin
                              // apertura file
                              //xFileName := SceltaAnagFileForm.QAnagFile.FieldByName('NomeFile').asString;
                            // ShellExecute(0, 'Open', pchar(xFileName), '', '', SW_SHOW);
                              aprifile('vuoto');
                         end;   }
                   // end;
               end;
          end;
          //end;
          if xViewCV then begin
               Application.CreateForm(TViewForm, ViewForm);
               if FileExists(xFile) then ViewForm.xFileName := xFile
               else ViewForm.xFileName := '';
               ViewForm.xCVNumero := Data.Q1.FieldByName('CVNumero').asString;
               ViewForm.Caption := ViewForm.Caption + ' ' + Data.Q1.FieldByName('Cognome').asString + ' ' + Data.Q1.FieldByName('Nome').asString;
               ViewForm.ShowModal;
               ViewForm.Free;
          end;
          Data.Q1.Close;
     end;
end;
//[/TONI20020724\]FINE
//[TONI20020923]DEBUGFINEOK

function VerifIntegrRef(xTable, xFKField, xID: string; xMessStop: boolean = True): boolean;
begin
     // verifica l'integrit� referenziale
     // Usage:  xTable = tabella (figlia) su cui operare la verifica
     //         xFKField = campo di questa tabella (foreign key)
     //         xID = valore della foreign key dalla tabella che si vuole cancellare.
     // --> restituisce TRUE se NON ci sono problemi (ovvero nessun record nella tab.figlia)
     //     restituisce FALSE se ci sono record.
     Data.Q1.Close;
     Data.Q1.SQL.text := 'select count(*) Tot from ' + xTable + ' where ' + xFKField + '=' + xID;
     Data.Q1.Open;
     if Data.Q1.FieldByname('Tot').asInteger > 0 then begin
          if xMessStop then
               MessageDlg('Violazione di integrit� referenziale con la tabella collegata:' + chr(13) +
                    xTable + ' (campo: ' + xFKField + ')' + chr(13) +
                    'IMPOSSIBILE CANCELLARE', mtError, [mbOK], 0);
          Result := False;
     end else Result := True;
     Data.Q1.Close;
end;

//[/TONI20020724\]

function IncompAnagCli(xIDAnag, xIDCliente: integer): boolean;
begin
     // incompatibilit� Anagrafica - cliente: tabella "AnagIncompClienti"
     // restituisce TRUE se c'� incompatibilit�
   {     with Data.Q1 do begin
          Close;
          SQL.Text:='select count(*) Tot from AnagIncompClienti where IDAnagrafica=:xIDAnagrafica and IDCliente=:xIDCliente';
          ParamByName('xIDAnagrafica').asInteger:=xIDAnag;
          ParamByName('xIDCliente').asInteger:=xIDCliente;
          Open;
          if FieldByName('Tot').asInteger>0 then Result:=True
          else begin
               // controllo incompatibilit� con clienti collegati, se ce ne sono
               Close;
               SQL.Text:='select IDClienteRef from ClienteLinkCliente where IDCliente=:xIDCliente and CheckCand=1';
               ParamByName('xIDCliente').asInteger:=xIDCliente;
               Open;
               Result:=False;
               while not EOF do begin
                    with Data.QTemp do begin
                         Close;
                         SQL.Text:='select count(*) Tot from AnagIncompClienti where IDAnagrafica=:xIDAnagrafica and IDCliente=:xIDCliente';
                         ParamByName('xIDAnagrafica').asInteger:=xIDAnag;
                         ParamByName('xIDCliente').asInteger:=Data.Q1.FieldByName('IDClienteRef').asInteger;
                         Open;
                         if FieldByName('Tot').asInteger>0 then Result:=True;
                    end;
                    Next;
               end;
          end;
          Close;
     end;}
     with Data.Q1 do begin
          Close;
          SQL.Text := 'select count(*) Tot from AnagIncompClienti where IDAnagrafica=' + IntToStr(xIDAnag) + ' and IDCliente=' + INtToStr(xIDCliente);
          Open;
          if FieldByName('Tot').asInteger > 0 then Result := True
          else begin
               // controllo incompatibilit� con clienti collegati, se ce ne sono
               Close;
               SQL.Text := 'select IDClienteRef from ClienteLinkCliente where IDCliente=' + IntTOStr(xIDCliente) + ' and CheckCand=1';
               Open;
               Result := False;
               while not EOF do begin
                    with Data.QTemp do begin
                         Close;
                         SQL.Text := 'select count(*) Tot from AnagIncompClienti where IDAnagrafica=' + IntToStr(xIDAnag) + ' and IDCliente=' + Data.Q1.FieldByName('IDClienteRef').asString;
                         Open;
                         if FieldByName('Tot').asInteger > 0 then Result := True;
                    end;
                    Next;
               end;
          end;
          Close;
     end;
end;
//[TONI20020724]FINE

procedure OpenTab(xTable: string; xFields, xFieldtitles: array of string; xModify: boolean = True; xOrder: string = ''; xParGenerico: string = ''; titolo: string = 'Tabella'; xAltezzaLabel: Integer = 0);
var i: integer;
     xCampi: string;
begin
     // apre una form per la gestione la tabella (che visualizza tutti i campi)
     // Usage:  xTable = tabella da gestire
     //         xFields = array dei campi (stringhe); il primo � la CHIAVE PRIMARIA
     //         xFieldtitles = array dei titoli dei campi (tolta la CHIAVE PRIMARIA)
     //         xOrder = campo su cui fare l'ordine (se non specificato non � ordinata)
     //
     Application.CreateForm(TSelTabGenForm, SelTabGenForm);
     SelTabGenForm.xAltezzaLabel := xAltezzaLabel;
     SelTabGenForm.FrameDB1.serviceform := FrameDBForm;
     SelTabGenForm.xparamGenerico := xParGenerico;
     SelTabGenForm.xModify := xModify;
     SelTabGenForm.xFieldName := '';
     SelTabGenForm.xFieldTitle := '';

     SelTabGenForm.Caption := 'Gestione Tabella ' + xTable;
     for i := 0 to Length(xFieldtitles) - 1 do begin
          SelTabGenForm.xTitles[i + 1] := xFieldtitles[i];
     end;
     xcampi := '';
     for i := 0 to Length(xFields) - 1 do begin
          xCampi := xCampi + xFields[i];
          if i + 1 <= Length(xFields) - 1 then xCampi := xcampi + ',';
     end;
     //SelTabGenForm.Width:=400;
     //SelTabGenForm.Height:=300;
     for i := 0 to Length(xFields) - 1 do
          SelTabGenForm.xFields[i] := xfields[i];
     SelTabGenForm.QTab.SQL.text := 'select ' + xCampi + ' from ' + xTable;
     if xOrder <> '' then
          SelTabGenForm.QTab.SQL.Add('Order by ' + xOrder);
     SelTabGenForm.QTab.Open;
     SelTabGenForm.BitBtn1.Visible := False;
     SelTabGenForm.BitBtn2.caption := 'Esci';
     SelTabGenForm.Label1.Caption := titolo;
     SelTabGenForm.showmodal;
     SelTabGenForm.Free;
end;
//[/TONI20020724\]

//[/TONI20021003\]DEBUGOK

function CheckAnagInseritoBlocco1(xIDAnag: integer): integer;
begin
     // controlla se il soggetto ha Esp.Lav. ATTUALI in un'azienda per cui c'� il BLOCCO1 attivo
     // o comunque se � un cliente attivo
     // ritorna ZERO se NON c'� blocco, altrimenti ritorna l'ID del Cliente
     // + 10.000 se di livello 1 e + 20.000 se di livello 2
     with Data.Q1 do begin
          Close;
          SQL.text := 'select EsperienzeLavorative.IDAzienda,stato from EsperienzeLavorative,EBC_Clienti ' +
               'where EsperienzeLavorative.IDAzienda=EBC_Clienti.ID ' +
               'and (Blocco1=1 or stato=''attivo'')' +
               'and IDTipoAzienda=1 and Attuale=1 and IDAnagrafica=' + IntToStr(xIDAnag);
          Open;
          if IsEmpty then Result := 0
          else begin
               if FieldByName('stato').asString = 'attivo' then
                    Result := 20000 + FieldByName('IDAzienda').AsInteger
               else Result := 10000 + FieldByName('IDAzienda').AsInteger;
          end;
          Close;
     end;
end;

function GetIDEvento(xDic: string): integer;
begin
     // restituisce l'ID dell'evento data la sua dicitura
     {with Data.Q1 do begin
          Close;
          SQL.text:='select ID from EBC_Eventi '+
               'where Evento=:xEvento ';
          ParamByName('xEvento').asString:=xDic;
          Open;
          if IsEmpty then Result:=0
          else Result:=FieldByName('ID').asInteger;
          Close;
     end;}
     with Data.Q1 do begin
          Close;
          SQL.text := 'select ID from EBC_Eventi where Evento=''' + xDic + '''';
          Open;
          if IsEmpty then Result := 0
          else Result := FieldByName('ID').asInteger;
          Close;
     end;
end;
//[/TONI20020724\]FINE

function GetPwdUtenteResp(xIDUtente: integer): string;
var xIDUtenteResp: integer;
begin
     // restituisce la PASSWORD dell'utente "sopra" (IDUtenteResp)
     // altrimenti (se non c'� tale utente) quella di Administrator
     with Data.Q1 do begin
          Close;
          SQL.text := 'select IDUtenteResp from Users where ID=' + IntToStr(xIDUtente);
          Open;
          if (IsEmpty) or (FieldByName('IDUtenteResp').asInteger = 0) then begin
               Close;
               SQL.text := 'select Password from Users where SUBSTRING (Nominativo,1,LEN(NOMINATIVO))=''Administrator''';
          end else begin
               xIDUtenteResp := FieldByName('IDUtenteResp').asInteger;
               Close;
               SQL.text := 'select Password from Users where ID=' + IntToStr(xIDUtenteResp);
          end;
          Open;
          Result := TrimRight(FieldByName('Password').asString);
          Close;
     end;
end;

function GetPwdAdministrator: string;
var xIDUtenteResp: integer;
begin
     // restituisce la PASSWORD dell'utente Administrator
     with Data.Q1 do begin
          Close;
          SQl.clear;
          SQL.text := 'select Password from Users where SUBSTRING (Nominativo,1,LEN(NOMINATIVO))=''Administrator''';
          Open;
          Result := TrimRight(FieldByName('Password').asString);
          Close;
     end;
end;

function SelFromQueryMultiCampi(xQuery, xKeyField: string; xFieldName, xFieldTitle: array of string; xID: string; xOrder: string = ''; xModify: boolean = false): string;
var i: integer;
begin
     // apre una form di selezione con una Query che visualizza PIU campo
     // Usage:  xQuery = query
     //         xKeyField = campo chiave della tabella
     //         xFieldName = campo da visualizzare
     //         xFieldTitle = titolo per la griglia
     //         xID = valore del campo chiave (per la modifica: si posiziona sull'ID)
     //               � stringa perch� cos� va bene per gli interi e per le stringhe.
     // --> restituisce il campo chiave come stringa (cos� va bene per gli interi e per le stringhe)

     Application.CreateForm(TSelTabGenForm, SelTabGenForm);
     SelTabGenForm.xModify := xModify;
     SelTabGenForm.FrameDB1.serviceform := FrameDBForm;
     SelTabGenForm.xFieldName := '';
     SelTabGenForm.xFieldTitle := '';
    { if xTable = 'Dipartimenti' then
          SelTabGenForm.Caption := 'Gestione Tabella ' + Data.GlobalEtichettadipartimento.Value
     else
          SelTabGenForm.Caption := 'Gestione Tabella ' + xTable; }
     for i := 0 to Length(xFieldtitle) - 1 do begin
          SelTabGenForm.xTitles[i + 1] := xFieldtitle[i];
     end;
    { xcampi := '';
     for i := 0 to Length(xFields) - 1 do begin
          xCampi := xCampi + xFields[i];
          if i + 1 <= Length(xFields) - 1 then xCampi := xcampi + ',';
     end;  }
     //SelTabGenForm.Width:=400;
     //SelTabGenForm.Height:=300;
     for i := 0 to Length(xFieldName) - 1 do
          SelTabGenForm.xFields[i] := xfieldname[i];
     SelTabGenForm.QTab.SQL.text := xQuery;
     if xOrder <> '' then
          SelTabGenForm.QTab.SQL.Add(' Order by ' + xOrder);
     SelTabGenForm.QTab.Open;
     SelTabGenForm.BitBtn1.Visible := true;
     SelTabGenForm.BitBtn2.caption := 'Esci';
    // SelTabGenForm.label1.caption := Intestazione;
     SelTabGenForm.Caption := '';
     SelTabGenForm.showmodal;
     if SelTabGenForm.ModalResult = mrok then
          result := SelTabGenForm.QTab.FieldByName(xKeyField).asstring;

     SelTabGenForm.Free;
end;

function GetPwdUtente(xIDUtente: integer): string;
begin
     // restituisce la PASSWORD dell'utente
     with Data.Q1 do begin
          Close;
          SQL.text := 'select Password from Users where ID=' + IntToStr(xIDUtente);
          Open;
          Result := TrimRight(FieldByName('Password').asString);
          Close;
     end;
end;

function GetDescUtenteResp(xIDUtente: integer): string;
var xIDUtenteResp: integer;
begin
     // restituisce la DESCRIZIONE dell'utente "sopra" (IDUtenteResp)
     // altrimenti (se non c'� tale utente) quella di Administrator
     with Data.Q1 do begin
          Close;
          SQL.text := 'select IDUtenteResp from Users where ID=' + IntToStr(xIDUtente);
          Open;
          if (IsEmpty) or (FieldByName('IDUtenteResp').asInteger = 0) then begin
               Close;
               SQL.text := 'select Descrizione from Users where Nominativo=''Administrator''';
          end else begin
               xIDUtenteResp := FieldByName('IDUtenteResp').asInteger;
               Close;
               SQL.text := 'select Descrizione from Users where ID=' + IntToStr(xIDUtenteResp);
          end;
          Open;
          Result := FieldByName('Descrizione').asString;
          Close;
     end;
end;

function GetIDUtenteResp(xIDUtente: integer): integer;
var xIDUtenteResp: integer;
begin
     // restituisce l'ID dell'utente "sopra" (IDUtenteResp)
     // oppure quello dell'amministratore se non c'�
     with Data.Q1 do begin
          Close;
          SQL.text := 'select IDUtenteResp from Users where ID=' + IntToStr(xIDUtente);
          Open;
          if (IsEmpty) or (FieldByName('IDUtenteResp').asInteger = 0) then Result := 9 // amministratore
          else Result := FieldByName('IDUtenteResp').asInteger;
          Close;
     end;
end;

function GetDescCliente(xIDCliente: integer): string;
begin
     // restituisce la descrizione del cliente
     with Data.Q1 do begin
          Close;
          SQL.text := 'select Descrizione from EBC_Clienti where ID=' + IntToStr(xIDCliente);
          Open;
          if IsEmpty then Result := ''
          else Result := FieldByName('Descrizione').asString;
          Close;
     end;
end;

//[/TONI20020724\]

function GetCodRicerca: string;
var xNewTrans: boolean;
     xAnno, xMese, xgiorno: Word;
begin
     {     // restituisce il codice per una nuova commessa
          // prendendolo dall'ultimo codice in GLOBAL
          // e incrementando il relativo contatore
          Result:='';
          xNewTrans:=False;
          with Data do begin
               if not DB.InTransaction then begin
                    DB.BeginTrans;
                    xNewTrans:=True;
               end;
               try
                    QTemp.Close;
                    QTemp.SQL.Text:='select UltimoProgRicerca,AddAnnoRic from global';
                    QTemp.Open;
                    if QTemp.fieldByName('AddAnnoRic').AsBoolean=true then begin
                         DecodeDate(Date,xAnno,xMese,xgiorno);
                         Result:=IntToStr(QTemp.fieldByName('UltimoProgRicerca').asInteger+1)+'/'+copy(IntToStr(xAnno),3,2);
                    end else Result:=IntToStr(QTemp.fieldByName('UltimoProgRicerca').asInteger+1);
                    QTemp.Close;
                    Q1.SQL.text:='update global set UltimoProgRicerca=UltimoProgRicerca+1 ';
                    Q1.ExecSQL;
                    if xNewTrans then DB.CommitTrans;
               except
                    if xNewTrans then DB.RollbackTrans;
                    Result:='';
                    MessageDlg('ERRORE SUL DATABASE: progressivo ricerca non impostato',mtError, [mbOK],0);
                    raise;
               end;
          end;}
          // restituisce il codice per una nuova commessa
          // prendendolo dall'ultimo codice in GLOBAL
          // e incrementando il relativo contatore
     Result := '';
     xNewTrans := False;
     with Data do begin
          if not DB.InTransaction then begin
               DB.BeginTrans;
               xNewTrans := True;
          end;
          try
               QTemp.Close;
               QTemp.SQL.Text := 'select UltimoProgRicerca,AddAnnoRic from global';
               QTemp.Open;
               if QTemp.fieldByName('AddAnnoRic').AsBoolean = true then begin
                    DecodeDate(Date, xAnno, xMese, xgiorno);
                    Result := IntToStr(QTemp.fieldByName('UltimoProgRicerca').asInteger + 1) + '/' + copy(IntToStr(xAnno), 3, 2);
               end else Result := IntToStr(QTemp.fieldByName('UltimoProgRicerca').asInteger + 1);
               QTemp.Close;
               Q1.SQL.text := 'update global set UltimoProgRicerca=UltimoProgRicerca+1 ';
               Q1.ExecSQL;
               if xNewTrans then DB.CommitTRans;
          except
               if xNewTrans then DB.RollbackTrans;
               Result := '';
               MessageDlg('ERRORE SUL DATABASE: progressivo ricerca non impostato', mtError, [mbOK], 0);
               raise;
          end;
     end;
end;

function GetCodOfferta: string;
var xNewTrans: boolean;
     xAnno, xMese, xgiorno: Word;
begin
     // restituisce il codice per una nuova offerta
     // prendendolo dall'ultimo codice in GLOBAL
     // e incrementando il relativo contatore
   {     Result:='';
     xNewTrans:=False;
     with Data do begin
          if not DB.InTransaction then begin
               DB.BeginTrans;
               xNewTrans:=True;
          end;
          try
               QTemp.Close;
               QTemp.SQL.Text:='select UltimoProgOfferta,AddAnnoRic from global';
               QTemp.Open;
               if QTemp.fieldByName('AddAnnoRic').AsBoolean=true then begin
                    DecodeDate(Date,xAnno,xMese,xgiorno);
                    Result:=IntToStr(QTemp.fieldByName('UltimoProgOfferta').asInteger+1)+'/'+copy(IntToStr(xAnno),3,2);
               end else Result:=IntToStr(QTemp.fieldByName('UltimoProgOfferta').asInteger+1);
               QTemp.Close;
               Q1.SQL.text:='update global set UltimoProgOfferta=UltimoProgOfferta+1 ';
               Q1.ExecSQL;
               if xNewTrans then DB.CommitTrans;
          except
               if xNewTrans then DB.RollbackTrans;
               Result:='';
               MessageDlg('ERRORE SUL DATABASE: progressivo offerta non impostato',mtError, [mbOK],0);
               raise;
          end;
     end;}
     Result := '';
     xNewTrans := False;
     with Data do begin
          if not DB.InTransaction then begin
               DB.BeginTrans;
               xNewTrans := True;
          end;
          try
               QTemp.Close;
               QTemp.SQL.Text := 'select UltimoProgOfferta,AddAnnoRic from global';
               QTemp.Open;
               if QTemp.fieldByName('AddAnnoRic').AsBoolean = true then begin
                    DecodeDate(Date, xAnno, xMese, xgiorno);
                    Result := IntToStr(QTemp.fieldByName('UltimoProgOfferta').asInteger + 1) + '/' + copy(IntToStr(xAnno), 3, 2);
               end else Result := IntToStr(QTemp.fieldByName('UltimoProgOfferta').asInteger + 1);
               QTemp.Close;
               Q1.SQL.text := 'update global set UltimoProgOfferta=UltimoProgOfferta+1 ';
               Q1.ExecSQL;
               if xNewTrans then DB.CommitTrans;
          except
               if xNewTrans then DB.RollbackTrans;
               Result := '';
               MessageDlg('ERRORE SUL DATABASE: progressivo offerta non impostato', mtError, [mbOK], 0);
               raise;
          end;
     end;

end;
//[/TONI20020724\]FINE

function GetDocPath: string;
var xH1DocPath: string;
begin
     // restituisce il path della cartella documenti
     xH1DocPath := LeggiRegistry('H1DocPath');
     if xH1DocPath <> '' then begin
          Result := xH1DocPath;
     end else begin
          with Data.Q2 do begin
               Close;
               SQL.text := 'select DirFileDoc from Global';
               Open;
               if IsEmpty then Result := ''
               else begin
                    if Pos('\', copy(FieldByName('DirFileDoc').asString, length(FieldByName('DirFileDoc').asString) - 1, length(FieldByName('DirFileDoc').asString))) > 0 then
                         Result := Copy(FieldByName('DirFileDoc').asString, 1, length(FieldByName('DirFileDoc').asString) - 1)
                    else
                         Result := FieldByName('DirFileDoc').asString;
               end;
               Close;
          end;
     end;
end;

function GetCVPath: string;
var xH1CVPath: string;
begin
     // restituisce il path della cartella cv
     xH1CVPath := LeggiRegistry('H1DocPath');
     if xH1CVPath <> '' then begin
          Result := xH1CVPath;
     end else begin
          Data.QTempPath.Close;
          Data.QTempPath.SQL.text := 'select DirFileCV from Global';
          Data.QTempPath.Open;
          if Data.QTempPath.IsEmpty then Result := ''
          else begin
               if Pos('\', copy(TrimRight(Data.QTempPath.FieldByName('DirFileCV').asString), length(TrimRight(Data.QTempPath.FieldByName('DirFileCV').asString)) - 1, length(TrimRight(Data.QTempPath.FieldByName('DirFileCV').asString)))) > 0 then
                    Result := Copy(TrimRight(Data.QTempPath.FieldByName('DirFileCV').asString), 1, length(TrimRight(Data.QTempPath.FieldByName('DirFileCV').asString)) - 1)
               else
                    Result := TrimRight(Data.QTempPath.FieldByName('DirFileCV').asString);
          end;
          Data.QTempPath.Close;
     end;
end;

function GetIDAnagUtente(xIDUtente: integer): integer;
begin
     with Data.QTemp do begin
          Close;
          SQL.text := 'select IDAnagrafica from Users where ID=' + IntToStr(xIDUtente);
          Open;
          if IsEmpty then Result := 0
          else Result := FieldByName('IDAnagrafica').asInteger;
          Close;
     end;
end;

//[/TONI20020724\]

procedure CancellaSogg(xIDAnag: integer);
var xCognome, xNome, xCVNumero, xNatoA, xNatoIl, xCVInseritoIndata, xCVDataAgg: string;
begin
     {     xNatoA:='--';
          xNatoIl:='--';
          xCVInseritoIndata:='--';
          xCVDataAgg:='--';
          with Data.QTemp do begin
               Close;
               SQL.text:='select Cognome,Nome,CVNumero,LuogoNascita,DataNascita,CVInseritoIndata,CVDataAgg from Anagrafica where ID='+IntToStr(xIDAnag);
               Open;
               xCognome:=FieldByName('Cognome').asString;
               xNome:=FieldByName('Nome').asString;
               xCVNumero:=FieldByName('CVNumero').asString;
               xNatoA:=FieldByName('LuogoNascita').asString;
               xNatoIl:=FieldByName('DataNascita').asString;
               xCVInseritoIndata:=FieldByName('CVInseritoIndata').asString;
               xCVDataAgg:=FieldByName('CVDataAgg').asString;
               Close;
          end;
          Data.DB.BeginTrans;
          try
               // eliminare tabelle collegate da CONSTRAINT di integrit� referenziale                         Data.Q1.Close;
               Data.Q1.Close;
               Data.Q1.SQL.Clear;
               Data.Q1.SQL.Add('delete from AnagAltreInfo where IDAnagrafica='+IntToStr(xIDAnag));
               Data.Q1.ExecSQL;
               Data.Q1.SQL.Clear;
               Data.Q1.SQL.Add('delete from AnagCaratteristiche where IDAnagrafica='+IntToStr(xIDAnag));
               Data.Q1.ExecSQL;
               Data.Q1.SQL.Clear;
               Data.Q1.SQL.Add('delete from AnagFile where IDAnagrafica='+IntToStr(xIDAnag));
               Data.Q1.ExecSQL;
               Data.Q1.SQL.Clear;
               Data.Q1.SQL.Add('delete from AnagIncompClienti where IDAnagrafica='+IntToStr(xIDAnag));
               Data.Q1.ExecSQL;
               Data.Q1.SQL.Clear;
               Data.Q1.SQL.Add('delete from AnagMansioni where IDAnagrafica='+IntToStr(xIDAnag));
               Data.Q1.ExecSQL;
               Data.Q1.SQL.Clear;
               Data.Q1.SQL.Add('delete from AnagValutaz where IDAnagrafica='+IntToStr(xIDAnag));
               Data.Q1.ExecSQL;
               Data.Q1.SQL.Clear;
               Data.Q1.SQL.Add('delete from CompetenzeAnagrafica where IDAnagrafica='+IntToStr(xIDAnag));
               Data.Q1.ExecSQL;
               Data.Q1.SQL.Clear;
               Data.Q1.SQL.Add('delete from EBC_CandidatiRicerche where IDAnagrafica='+IntToStr(xIDAnag));
               Data.Q1.ExecSQL;
               Data.Q1.SQL.Clear;
               Data.Q1.SQL.Add('delete from EBC_ContattiCandidati where IDAnagrafica='+IntToStr(xIDAnag));
               Data.Q1.ExecSQL;
               Data.Q1.SQL.Clear;
               Data.Q1.SQL.Add('delete from EsperienzeLavorative where IDAnagrafica='+IntToStr(xIDAnag));
               Data.Q1.ExecSQL;
               Data.Q1.SQL.Clear;
               Data.Q1.SQL.Add('delete from LingueConosciute where IDAnagrafica='+IntToStr(xIDAnag));
               Data.Q1.ExecSQL;
               Data.Q1.SQL.Clear;
               Data.Q1.SQL.Add('delete from Organigramma where IDDipendente='+IntToStr(xIDAnag));
               Data.Q1.ExecSQL;
               Data.Q1.SQL.Clear;
               Data.Q1.SQL.Add('delete from Storico where IDAnagrafica='+IntToStr(xIDAnag));
               Data.Q1.ExecSQL;
               Data.Q1.SQL.Clear;
               Data.Q1.SQL.Add('delete from TitoliStudio where IDAnagrafica='+IntToStr(xIDAnag));
               Data.Q1.ExecSQL;
               Data.Q1.SQL.Clear;
               Data.Q1.SQL.Add('delete from Anagrafica where ID='+IntToStr(xIDAnag));
               Data.Q1.ExecSQL;

               // log cancellazione
               Log_Operation(MainForm.xIDUtenteAttuale,'Anagrafica',xIDAnag,'D',xCVNumero+' '+xCognome+' '+xNome+', nato a '+xNatoA+' il '+xNatoIl+' CV ins.il '+xCVInseritoIndata+' ult.agg.il '+xCVDataAgg);

               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('Errore nel database: cancellazione non eseguita',mtError, [mbOK],0);
               raise;
          end;}
     xNatoA := '--';
     xNatoIl := '--';
     xCVInseritoIndata := '--';
     xCVDataAgg := '--';
     with Data.QTemp do begin
          Close;
          SQL.text := 'select Cognome,Nome,CVNumero,LuogoNascita,DataNascita,CVInseritoIndata,CVDataAgg from Anagrafica where ID=' + IntToStr(xIDAnag);
          Open;
          xCognome := FieldByName('Cognome').asString;
          xNome := FieldByName('Nome').asString;
          xCVNumero := FieldByName('CVNumero').asString;
          xNatoA := FieldByName('LuogoNascita').asString;
          xNatoIl := FieldByName('DataNascita').asString;
          xCVInseritoIndata := FieldByName('CVInseritoIndata').asString;
          xCVDataAgg := FieldByName('CVDataAgg').asString;
          Close;
     end;
     Data.DB.BeginTrans;
     try
          // eliminare tabelle collegate da CONSTRAINT di integrit� referenziale                         Data.Q1.Close;
          Data.Q1.Close;
          Data.Q1.SQL.Clear;
          Data.Q1.SQL.Add('delete from AnagAltreInfo where IDAnagrafica=' + IntToStr(xIDAnag));
          Data.Q1.ExecSQL;
          Data.Q1.SQL.Clear;
          Data.Q1.SQL.Add('delete from AnagCaratteristiche where IDAnagrafica=' + IntToStr(xIDAnag));
          Data.Q1.ExecSQL;
          Data.Q1.SQL.Clear;
          Data.Q1.SQL.Add('delete from AnagFile where IDAnagrafica=' + IntToStr(xIDAnag));
          Data.Q1.ExecSQL;
          Data.Q1.SQL.Clear;
          Data.Q1.SQL.Add('delete from AnagIncompClienti where IDAnagrafica=' + IntToStr(xIDAnag));
          Data.Q1.ExecSQL;
          Data.Q1.SQL.Clear;
          Data.Q1.SQL.Add('delete from AnagMansioni where IDAnagrafica=' + IntToStr(xIDAnag));
          Data.Q1.ExecSQL;
          Data.Q1.SQL.Clear;
          Data.Q1.SQL.Add('delete from AnagValutaz where IDAnagrafica=' + IntToStr(xIDAnag));
          Data.Q1.ExecSQL;
          Data.Q1.SQL.Clear;
          Data.Q1.SQL.Add('delete from CompetenzeAnagrafica where IDAnagrafica=' + IntToStr(xIDAnag));
          Data.Q1.ExecSQL;
          Data.Q1.SQL.Clear;
          Data.Q1.SQL.Add('delete from EBC_CandidatiRicerche where IDAnagrafica=' + IntToStr(xIDAnag));
          Data.Q1.ExecSQL;
          Data.Q1.SQL.Clear;
          Data.Q1.SQL.Add('delete from EBC_ContattiCandidati where IDAnagrafica=' + IntToStr(xIDAnag));
          Data.Q1.ExecSQL;
          Data.Q1.SQL.Clear;
          Data.Q1.SQL.Add('delete from EsperienzeLavorative where IDAnagrafica=' + IntToStr(xIDAnag));
          Data.Q1.ExecSQL;
          Data.Q1.SQL.Clear;
          Data.Q1.SQL.Add('delete from LingueConosciute where IDAnagrafica=' + IntToStr(xIDAnag));
          Data.Q1.ExecSQL;
          Data.Q1.SQL.Clear;
          Data.Q1.SQL.Add('delete from Organigramma where IDDipendente=' + IntToStr(xIDAnag));
          Data.Q1.ExecSQL;
          Data.Q1.SQL.Clear;
          Data.Q1.SQL.Add('delete from Storico where IDAnagrafica=' + IntToStr(xIDAnag));
          Data.Q1.ExecSQL;
          Data.Q1.SQL.Clear;
          Data.Q1.SQL.Add('delete from TitoliStudio where IDAnagrafica=' + IntToStr(xIDAnag));
          Data.Q1.ExecSQL;
          Data.Q1.SQL.Clear;
          Data.Q1.SQL.Add('delete from Anagrafica where ID=' + IntToStr(xIDAnag));
          Data.Q1.ExecSQL;

          // log cancellazione
          Log_Operation(MainForm.xIDUtenteAttuale, 'Anagrafica', xIDAnag, 'D', xCVNumero + ' ' + xCognome + ' ' + xNome + ', nato a ' + xNatoA + ' il ' + xNatoIl + ' CV ins.il ' + xCVInseritoIndata + ' ult.agg.il ' + xCVDataAgg);

          Data.DB.CommitTrans;
     except
          Data.DB.RollbackTrans;
          MessageDlg('Errore nel database: cancellazione non eseguita', mtError, [mbOK], 0);
          raise;
     end;
end;
//[/TONI20020724\]FINE

function GetWinTempPath: string;
var s: pchar;
begin
     s := stralloc(255);
     gettemppath(255, s);
     Result := s;
     strdispose(s);
end;

procedure ScriviRegistry(xChiave, xValore: string);
begin
     xReg := TRegistry.create;
     xReg.RootKey := HKEY_CURRENT_USER;
     xReg.OpenKey('\Software\H1', True);
     xReg.WriteString(xChiave, xValore);
     xReg.CloseKey;
     xReg.Free;
end;

function LeggiRegistry(xChiave: string): string;
begin
     Result := '';
     xReg := TRegistry.create;
     xReg.RootKey := HKEY_CURRENT_USER;
     xReg.OpenKey('\Software\H1', True);
     Result := xReg.ReadString(xChiave);
     xReg.CloseKey;
     xReg.Free;
end;

function GetFunctionName(xCodModulo: string): string;
begin
     Data.QTemp.Close;
     Data.QTemp.SQL.text := 'select Funzione from SAC_ModuliFunzioni where IDModulo=11 and CodModulo=' + xCodModulo;
     Data.QTemp.Open;
     if not Data.QTemp.IsEmpty then
          Result := '"' + Data.QTemp.FieldByName('Funzione').asString + '"'
     else Result := '';
end;

procedure CheckVisionati(xIDSogg, xIDEdizione: integer; xMessage: boolean);
//var xIDLoc: integer;
begin

     Data.QTemp.Close;
     Data.QTemp.SQL.Text := 'Select Visionato from Ann_AnagAnnEdizData ' +
          'where idanagrafica = :xIDAnagrafica: and idannedizdata = :xIDAnnEdizData: and Visionato = 1';
     Data.QTemp.ParamByName['xIDAnagrafica'] := xIDSogg;
     Data.QTemp.ParamByName['xIDAnnEdizData'] := xIDEdizione;
     Data.QTemp.Open;
     if Data.QTemp.IsEmpty then begin

          if xMessage = true then begin
               if MessageDlg('Vuoi segnare il candidato come "visionato"?', mtInformation, [mbYes, mbNo], 0) = mrYes then begin

                    DataRicerche.QAnnunciCand.Edit;
                    DataRicerche.qAnnunciCandVisionato.AsBoolean := true;
                    DataRicerche.QAnnunciCand.post;

                   { Data.QTemp.Close;
                    Data.QTemp.SQL.Text := 'update ann_anagannedizdata set Visionato = 1 ' +
                         'where idanagrafica = :xIDAnagrafica: and idannedizdata = :xIDAnnEdizData:';
                    Data.QTemp.ParamByName['xIDAnagrafica'] := xIDSogg;
                    Data.QTemp.ParamByName['xIDAnnEdizData'] := xIDEdizione;
                    Data.QTemp.ExecSQL; }
               end;
          end else begin
               DataRicerche.QAnnunciCand.Edit;
               DataRicerche.qAnnunciCandVisionato.AsBoolean := true;
               DataRicerche.QAnnunciCand.post;

                                       {  Data.QTemp.Close;
               Data.QTemp.SQL.Text := 'update ann_anagannedizdata set Visionato = 1 ' +
                    'where idanagrafica = :xIDAnagrafica: and idannedizdata = :xIDAnnEdizData:';
               Data.QTemp.ParamByName['xIDAnagrafica'] := xIDSogg;
               Data.QTemp.ParamByName['xIDAnnEdizData'] := xIDEdizione;
               Data.QTemp.ExecSQL; }
          end;
       //   xIDLoc := DataRicerche.QAnnunciCand.FieldByname('IDAnagrafica').AsInteger;
         // DataRicerche.QAnnunciCand.Close;
         // DataRicerche.QAnnunciCand.Open;
         // DataRicerche.QAnnunciCand.Locate('IDAnagrafica', xIDLoc, []);

     end;
end;

procedure MailTo(xAddress, xObject, xBody: string);
var Browser: TWebBrowser;
begin
     Browser := TWebBrowser.create(nil);
     if xObject <> '' then begin
          xObject := StringReplace(xObject, '"', '''''', [rfreplaceAll]);
          ShellExecute(0, 'Open', pchar('mailto:' + xAddress + '?subject=' + xObject + '&body=' + xBody), '', '', SW_SHOW);
          //Browser.Navigate('mailto:' + xAddress + '?subject=' + xObject + '&body=' + xBody);
     end else //Browser.Navigate('mailto:' + xAddress);
          ShellExecute(0, 'Open', pchar('mailto:' + xAddress), '', '', SW_SHOW);

     Browser.Free;
end;

function SendEMail(Handle: THandle; Mail: TStrings): Cardinal;
type
     TAttachAccessArray = array[0..0] of TMapiFileDesc;
     PAttachAccessArray = ^TAttachAccessArray;
var
     MapiMessage: TMapiMessage;
     Receip: TMapiRecipDesc;
     Attachments: PAttachAccessArray;
     AttachCount: Integer;
     i1: integer;
     FileName: string;
     dwRet: Cardinal;
     MAPI_Session: Cardinal;
     WndList: Pointer;
begin
     dwRet := MapiLogon(Handle,
          PChar(''),
          PChar(''),
          MAPI_LOGON_UI or MAPI_NEW_SESSION,
          0, @MAPI_Session);

     if (dwRet <> SUCCESS_SUCCESS) then
     begin
          MessageBox(Handle,
               PChar('Error while trying to send email'),
               PChar('Error'),
               MB_ICONERROR or MB_OK);
     end
     else
     begin
          FillChar(MapiMessage, SizeOf(MapiMessage), #0);
          Attachments := nil;
          FillChar(Receip, SizeOf(Receip), #0);

          if Mail.Values['to'] <> '' then
          begin
               Receip.ulReserved := 0;
               Receip.ulRecipClass := MAPI_TO;
               Receip.lpszName := StrNew(PChar(Mail.Values['to']));
               Receip.lpszAddress := StrNew(PChar('SMTP:' + Mail.Values['to']));
               Receip.ulEIDSize := 0;
               MapiMessage.nRecipCount := 1;
               MapiMessage.lpRecips := @Receip;
          end;

          AttachCount := 0;

          for i1 := 0 to MaxInt do
          begin
               if Mail.Values['attachment' + IntToStr(i1)] = '' then
                    break;
               Inc(AttachCount);
          end;

          if AttachCount > 0 then
          begin
               GetMem(Attachments, SizeOf(TMapiFileDesc) * AttachCount);

               for i1 := 0 to AttachCount - 1 do
               begin
                    FileName := Mail.Values['attachment' + IntToStr(i1)];
                    Attachments[i1].ulReserved := 0;
                    Attachments[i1].flFlags := 0;
                    Attachments[i1].nPosition := ULONG($FFFFFFFF);
                    Attachments[i1].lpszPathName := StrNew(PChar(FileName));
                    Attachments[i1].lpszFileName :=
                         StrNew(PChar(ExtractFileName(FileName)));
                    Attachments[i1].lpFileType := nil;
               end;
               MapiMessage.nFileCount := AttachCount;
               MapiMessage.lpFiles := @Attachments^;
          end;

          if Mail.Values['subject'] <> '' then
               MapiMessage.lpszSubject := StrNew(PChar(Mail.Values['subject']));
          if Mail.Values['body'] <> '' then
               MapiMessage.lpszNoteText := StrNew(PChar(Mail.Values['body']));

          WndList := DisableTaskWindows(0);
          try
               Result := MapiSendMail(MAPI_Session, Handle,
                    MapiMessage, MAPI_DIALOG, 0);
          finally
               EnableTaskWindows(WndList);
          end;

          for i1 := 0 to AttachCount - 1 do
          begin
               StrDispose(Attachments[i1].lpszPathName);
               StrDispose(Attachments[i1].lpszFileName);
          end;

          if Assigned(MapiMessage.lpszSubject) then
               StrDispose(MapiMessage.lpszSubject);
          if Assigned(MapiMessage.lpszNoteText) then
               StrDispose(MapiMessage.lpszNoteText);
          if Assigned(Receip.lpszAddress) then
               StrDispose(Receip.lpszAddress);
          if Assigned(Receip.lpszName) then
               StrDispose(Receip.lpszName);
          MapiLogOff(MAPI_Session, Handle, 0, 0);
     end;
end;

procedure RegistraVisioneCand(xIDutente, xIDAnagrafica: integer);
begin
     if pos('PROPOSTE', Uppercase(Data.Global.FieldByName('NomeAzienda').AsString)) = 0 then begin
          Qrt := TADOQuery.Create(nil);
          Qrt.Connection := Data.DB;

          Qrt.Close;
          Qrt.SQL.Text := 'select * from Users_CandidatiVisionati where idutente = :xIDUtente ' +
               'and idanagrafica=:xidanagrafica and data=:xdata';
          Qrt.Parameters[0].Value := xIDutente;
          Qrt.Parameters[1].Value := xIDAnagrafica;
          Qrt.Parameters[2].Value := Date;
          Qrt.Open;

          if qrt.RecordCount = 0 then begin

               Qrt.Close;
               Qrt.SQL.Text := 'Insert into Users_CandidatiVisionati (idUtente,IDAnagrafica,Data) ' +
                    ' values (:xIDutente, :xIDAnagrafica, :xdata)';
               Qrt.Parameters[0].Value := xIDutente;
               Qrt.Parameters[1].Value := xIDAnagrafica;
               Qrt.Parameters[2].Value := Date;

               Qrt.ExecSQL;

          end;

          Qrt.Free;
     end;
end;


function CheckVisioneCand(xIDutente, xIDAnagrafica: integer): boolean;
begin
     Qrt := TADOQuery.Create(nil);
     Qrt.Connection := Data.DB;

     Qrt.Close;
     Qrt.SQL.Text := 'select * from Users_CandidatiVisionati where idutente = :xIDUtente ' +
          'and idanagrafica=:xidanagrafica and data=:xdata';
     Qrt.Parameters[0].Value := xIDutente;
     Qrt.Parameters[1].Value := xIDAnagrafica;
     Qrt.Parameters[2].Value := Date;
     Qrt.Open;

     if qrt.RecordCount = 1 then
          result := true
     else
          result := false;

     Qrt.Free;

end;


function CopiaFileAll(xFileName: string; xRename: Boolean; TipoTabPercorsoDir: string; xidnomefile: integer): string;
var xNewFilename, xPath: string;
begin
     if xRename then begin
          if TipoTabPercorsoDir = '' then begin
              // xPath := GetDocPath;
               xNewFileName := GetDocPath + '\' + Data.TAnagraficaID.AsString + '-' + StringReplace(DateToStr(Date), '/', '', [rfReplaceAll]) + ExtractFileExt(xFileName)
          end else begin
               xPath := GetPathFromTable(TipoTabPercorsoDir);
               xNewFileName := xPath + inttostr(xidnomefile) + ExtractFileExt(xFileName)
          end;

     end else
          xNewFileName := GetDocPath + '\' + ExtractFileName(xFileName);





     try
          CopyFile(PChar(xFilename), PChar(xNewFileName), TRUE);
          CopiaFileAll := xNewFilename;
     except
     end;
end;

procedure ColoraRiga(xIDUtente, xColore, xValore: integer; xCampo: string);
var xTempID: integer;
begin
     Data.Q1.Close;
     Data.Q1.SQL.Text := 'select Id from AnagColoriModuli where IDUtente = :xIDUtente: ' +
          '      and ' + xCampo + ' = :xValore:';
     Data.Q1.ParamByName['xIDUtente'] := xIDUtente;
     Data.Q1.ParamByName['xValore'] := xValore;
     Data.Q1.Open;

     if Data.Q1.RecordCount > 0 then begin

          xTempID := Data.Q1.FieldByName('ID').AsInteger;
          Data.Q1.Close;
          Data.Q1.SQL.Text := 'Delete from  AnagColoriModuli where id = ' + inttostr(xTempID);
          Data.Q1.ExecSQL;
     end;
     if xColore <> 16777215 then begin

          Data.Q1.Close;
          Data.Q1.SQL.Text := 'insert into AnagColoriModuli (IDUtente,Colore,' + xCampo + ')' +
               ' values (:xIDUtente:,:xColore:,:xValore:)';
          Data.Q1.ParamByName['xIDUtente'] := xIDUtente;
          Data.Q1.ParamByName['xColore'] := xColore;
          Data.Q1.ParamByName['xValore'] := xValore;
          Data.Q1.ExecSQL;
     end;

end;

function GetCodFattura: string;
var xNewTrans: boolean;
     xAnno, xMese, xgiorno: Word;
begin
     Result := '';
     xNewTrans := False;
     with Data do begin
          if not DB.InTransaction then begin
               DB.BeginTrans;
               xNewTrans := True;
          end;
          try
               QTemp.Close;
               QTemp.SQL.Text := 'select UltimoProgFatt,AddAnnoRic from global';
               QTemp.Open;
               if QTemp.fieldByName('AddAnnoRic').AsBoolean = true then begin
                    DecodeDate(Date, xAnno, xMese, xgiorno);
                    Result := IntToStr(QTemp.fieldByName('UltimoProgFatt').asInteger + 1) + '/' + copy(IntToStr(xAnno), 3, 2);
               end else Result := IntToStr(QTemp.fieldByName('UltimoProgFatt').asInteger + 1);
               QTemp.Close;
               Q1.Close;
               Q1.SQL.text := 'update global set UltimoProgFatt=UltimoProgFatt+1 ';
               Q1.ExecSQL;
               if xNewTrans then DB.CommitTrans;
          except
               if xNewTrans then DB.RollbackTrans;
               Result := '';
               MessageDlg('ERRORE SUL DATABASE: progressivo fattura non impostato', mtError, [mbOK], 0);
               raise;
          end;
     end;

end;

function CheckRicercheInUso(xIDRicerca, xIDUtente: integer): string;
var q, q1: TADOQuery;
begin
     {q1 := TADOQuery.Create(nil);
     q1.Connection := Data.DB;

     q1.sql.text := 'select * from EBC_RicercheUtenti where IDUtente=:x0 and IDRicerca=:x1';  //
     q1.parameters[0].value := xIDUtente;
     q1.parameters[1].value := xIDRicerca;
       //q1.parameters[0].value := xIDRicerca;

     q1.Open;
     if q1.recordcount = 0 then begin
          result := '';
          exit;
     end;                }

     q := TADOQuery.Create(nil);
     q.Connection := Data.DB;
     q.SQL.Text := 'select RU.IDUtente, U.Nominativo Utente from EBC_RicercheInUsoUtenti RU ' +
          'join Users U on RU.IDUtente = U.ID where RU.idricerca = :x1 ';
     q.Parameters[0].Value := xIDRicerca;
     q.Open;

     if q.RecordCount > 0 then begin
          if q.FieldByName('IDUtente').Value <> xIDUtente then begin
               result := 'ATTENZIONE: la commessa (progetto) risulta gi� aperta dall''utente ' + q.FieldByName('Utente').asString + chr(13) +
                    'Sar� comunque possibile entrare nel progetto/commessa in sola lettura.';
                    //+ chr(13) + 'Per sbloccare usare l''apposito pulsante (occorre la password di Administrator)'
          end else
               result := '';
     end else
          result := '';
     q.Free;
end;

procedure InsertRicercheInUso(xIDRicerca, xIDUtente: integer);
var q: TADOQuery;
begin
     q := TADOQuery.Create(nil);
     q.Connection := Data.DB;
     q.SQL.Text := 'insert into EBC_RicercheInUsoUtenti (idUtente,IDRicerca) ' +
          ' values (:x1, :x2)';
     q.Parameters[0].Value := xidutente;
     q.Parameters[1].Value := xIDRicerca;
     q.ExecSQL;

     q.Free;
end;

procedure DeleteRicercheInUso(xIDRicerca, xIDUtente: integer);
var q: TADOQuery;
begin
     q := TADOQuery.Create(nil);
     q.Connection := Data.DB;
     q.SQL.Text := 'delete from EBC_RicercheInUsoUtenti where idricerca = :x1 and idutente = :x2';
     q.Parameters[0].Value := xIDRicerca;
     q.Parameters[1].Value := xIDutente;
     q.ExecSQL;

     q.Free;
end;


function GetPathFromTable(xTipo: string): string;
var q: TADOQuery;
begin
     q := TADOQuery.Create(nil);
     q.Connection := Data.DB;
     q.SQL.Text := 'Select percorso,prefisso from tabpercorsidir where Tipo = :x1 ';
     q.Parameters[0].Value := xTipo;
     q.Open;

     if q.RecordCount = 0 then
          GetPathFromTable := ''
     else
          GetPathFromTable := q.FieldByName('percorso').AsString + '\' + q.FieldByName('prefisso').AsString;
     q.Free
end;

function CheckAssociazCandCompetenza(IDAnagrafica, IDCompetenza: integer): integer;
var Q: TADOQuery;
begin
     // controlla che una competenza non si gi� associata al candidato
     // Restituisce:
     //   l'ID della competenza eventualmente trovata
     //   -1 se non trovata
     Q := TADOQuery.Create(nil);
     Q.Connection := Data.DB;
     Q.SQL.Text := 'select ID from CompetenzeAnagrafica where IDAnagrafica=:x0 and IDCompetenza=:x1';
     Q.Parameters[0].Value := IDAnagrafica;
     Q.Parameters[1].Value := IDCompetenza;
     Q.Open;

     if Q.IsEmpty then
          Result := -1
     else Result := Q.FieldByName('ID').asInteger;

     Q.Free;
end;

procedure ApriHelp(xCodice: string);
begin
     HelpForm := THelpForm.Create(nil);
     HelpForm.qHelp.Close;
     HelpForm.qHelp.Parameters[0].Value := xCodice;
     HelpForm.qHelp.Open;

     if HelpForm.qHelp.RecordCount = 0 then begin
          data.QTemp.close;
          data.qtemp.sql.text := 'insert into tabHelp (idSoftware,codSezione) values ' +
               '(2,' + xcodice + ')';
          data.qtemp.ExecSQL;
          HelpForm.qHelp.Close;
          HelpForm.qHelp.Parameters[0].Value := xCodice;
          HelpForm.qHelp.Open;
     end;


     if MainForm.xUtenteAttuale = 'Sabattini' then
          HelpForm.dxDBHyperLinkEdit1.ReadOnly := false;

     //if HelpForm.qHelp.RecordCount > 0 then
     HelpForm.ShowModal;
    { else begin
          MessageDlg('Attenzione: Funzione help e appunti non disponibile per questa sezione!', mtWarning, [mbOk], 0);
          HelpForm.Close;
     end;}
     //HelpForm.ShowModal;

     HelpForm.Free;
end;


procedure InsertStoricoNoteQCandRic(xIDUtente, xIDCandRic, xCampo: string; xDataora: TDateTime);
var q: TAdoQuery;
begin
     try
          q := TADOQuery.Create(nil);
          q.Connection := Data.DB;
          if xCampo = 'NoteBefore' then begin
               q.SQL.Text := 'insert into StoricoNoteQCandRic (IDCandRic,IDUtente,DataOra, NoteBefore) ' +
                    'values (:xIDCandRic,:xIDUtente,:xDataOra,:xNote)';
               q.Parameters[0].Value := xIDCandRic;
               q.Parameters[1].Value := MainForm.xIDUtenteAttuale;
               q.Parameters[2].Value := xDataora;
               q.Parameters[3].Value := DataRicerche.qCandRicNote.Value;
          end else begin
               q.SQL.Text := 'update  StoricoNoteQCandRic set NoteAfter = :xNote ' +
                    'where idutente = :xIDUtente and IDCandRic = :xIDCandRic and Noteafter is NULL ';
               q.Parameters[0].Value := DataRicerche.qCandRicNote.Value;
               q.Parameters[1].Value := MainForm.xIDUtenteAttuale;
               q.Parameters[2].Value := xIDCandRic;
          end;
          q.execSQL;

          q.Free;
     except
     end;
end;


procedure ShellExecute_AndWait(FileName: string; Params: string);
var
     exInfo: TShellExecuteInfo;
     Ph: DWORD;
     x: integer;
begin
     FillChar(exInfo, SizeOf(exInfo), 0);
     with exInfo do
     begin
          cbSize := SizeOf(exInfo);
          fMask := SEE_MASK_NOCLOSEPROCESS or SEE_MASK_FLAG_DDEWAIT;
          Wnd := GetActiveWindow();
          ExInfo.lpVerb := 'open';
          ExInfo.lpParameters := PChar(Params);
          lpFile := PChar(FileName);
          nShow := SW_SHOWNORMAL;
     end;
     if ShellExecuteEx(@exInfo) then begin
          Ph := exInfo.HProcess;
     end else
     begin
          ShowMessage(SysErrorMessage(GetLastError));
          Exit;
     end;
     while WaitForSingleObject(ExInfo.hProcess, 50) <> WAIT_OBJECT_0 do
     begin
     //DEBUG showmessage(inttostr(WaitForSingleObject(ExInfo.hProcess, 50)));
          Application.ProcessMessages;
     end;
     CloseHandle(Ph);
end;

 {// SOSTITUITA DA SALVAFILE2
procedure salvafile(formchiamante: string);
var xFileName: string;
     xID: integer;
begin
     if not Data.QAnagFileFileBloccato.value then begin
          MessageDlg('Il file selezionato non � aperto', mtError, [mbOK], 0);
          exit;
     end;

     // check-in
     xFileName := PathGetTempPath + Data.QAnagFileSoloNome.value;

     try
          Data.QAnagFileFile.Close;
          Data.QAnagFileFile.Parameters[0].value := Data.QAnagFileID.Value;
          Data.QAnagFileFile.Open;
          Data.QAnagFileFile.Edit;
          Data.QAnagFileFileDocFile.LoadFromFile(xFileName);
          Data.QAnagFileFileDocExt.Value := Stringreplace(ExtractFileExt(xFileName), '.', '', [rfReplaceAll]);
          Data.QAnagFileFileFileBloccato.value := False;
          Data.QAnagFileFile.Post;
          Data.QAnagFileFile.Close;
     except
          on e: Exception do begin
               if copy(e.message, 1, 16) = 'Cannot open file' then
                    MessageDlg('ERRORE: problemi a salvare il file ' + xFileName + chr(13) + chr(13) +
                         'Possibili cause:' + chr(13) +
                         '- il file potrebbe essere aperto da un''altra applicazione: occorre chiuderla;' + chr(13) +
                         '- il file potrebbe essere stato cancellato dalla cartella', mtError, [mbOK], 0)
               else
                    MessageDlg('ERRORE: ' + e.message, mtError, [mbOK], 0);

               Data.QAnagFileFile.Cancel;
               exit;
          end;
     end;

     FileDelete(xFileName, False);
     BloccaAnagFile(false, formchiamante);
     ShowMessage('Salvataggio del file completato');
     Data.QAnagFile.Refresh;

end;
}

procedure salvafileDue(IDfile: integer; formchiamante: string);
var xFileName: string;
     xID: integer;
     qAnagFiletemp: TadoQuery;
begin
     if idfile > 0 then begin
          qAnagFiletemp := TADOQuery.Create(nil);
          qAnagFiletemp.Create(nil);
     //qAnagFiletemp.ConnectionString := data.DB.ConnectionObject.ConnectionString;
          qAnagFiletemp.Connection := data.DB;
          qAnagFiletemp.Close;
          qAnagFileTemp.sql.text := 'select * from AnagFile where anagfile.id=:xIDFile ';
          qAnagFileTemp.parameters.paramByName('xIDFile').value := IDFile;
          qAnagFiletemp.Open;
    // data.QAnagFileApriFile.close;
     //data.QAnagFileApriFile.Parameters.ParamByName('xIDFile').value:=IDfile;
     //data.QAnagFileApriFile.Open;

          if not qAnagFiletemp.fieldbyname('FileBloccato').value then begin
               MessageDlg('Il file selezionato non � aperto', mtError, [mbOK], 0);
               exit;
          end;

     // check-in
          xFileName := data.global.fieldbyname('DirFileApriCV').asstring + '\' + qAnagFiletemp.fieldbyname('SoloNome').value;

          try
               Data.QAnagFileFile.Close;
               Data.QAnagFileFile.Parameters[0].value := IDfile;
               Data.QAnagFileFile.Open;
               data.QAnagFileFile.Edit;
               data.QAnagFileFileDocFile.LoadFromFile(xFileName);
               data.QAnagFileFile.fieldByName('DocExt').Value := Stringreplace(ExtractFileExt(xFileName), '.', '', [rfReplaceAll]);
               Data.QAnagFileFileFileBloccato.value := False;
               Data.QAnagFileFile.Post;
               Data.QAnagFileFile.Close;
          except
               on e: Exception do begin
                    if copy(e.message, 1, 16) = 'Cannot open file' then
                         MessageDlg('ERRORE: problemi a salvare il file ' + xFileName + chr(13) + chr(13) +
                              'Possibili cause:' + chr(13) +
                              '- il file potrebbe essere aperto da un''altra applicazione: occorre chiuderla;' + chr(13) +
                              '- il file potrebbe essere stato cancellato dalla cartella', mtError, [mbOK], 0)
                    else
                         MessageDlg('ERRORE: ' + e.message, mtError, [mbOK], 0);

                    Data.QAnagFileFile.Cancel;

                    {if  FileExists(xFileName) = False then begin
                         QanagFiletemp.Close;
                         QanagFileTemp.SQL.text := 'update anagfile set filebloccato = 0 where id = '+ IntToStr(IDFile);
                         qanagfileTemp.ExecSQL;
                    end;  }

                    exit;
               end;
          end;

          FileDelete(xFileName, False);
          BloccaAnagFile(false, formchiamante);
          ShowMessage('Salvataggio del file completato');
     //Data.QAnagFile.Refresh;
          qAnagFiletemp.Close;
          qAnagFiletemp.Free;
     end;
end;



{//VECCHIA SOSTITUITA CON APRIFILEDUE
procedure aprifile(formchiamante: string);
var xFileName, xTempFileName, xTempFilePath: string;
     xID, idtemp: integer;
   //  qAnagFiletemp: TadoQuery;
begin

    // qAnagFiletemp := TADOQuery.Create(nil);
    // qAnagFiletemp.Create(nil);
    // qAnagFiletemp.ConnectionString := data.DB.ConnectionObject.ConnectionString;
    // qAnagFileTemp.sql.text := 'select * from AnagFile,TipiFileCand ' +
    //      ' where AnagFile.IDTipo *= TipiFileCand.ID and anagfile.IDAnagrafica=:xIDAnagrafica ';
     //qAnafFileTemp.parameters.paramByName('xIDAnagrafica').value:=IDAnag;
     //xID :=IDAnag;

     xID := data.QAnagFileID.AsInteger;
     data.QAnagFile.Refresh;
     data.QLKNominatUtente.Close;
     data.QLKNominatUtente.Open;
     data.QAnagFile.Locate('ID', xID, []);
     if Data.QAnagFileFileBloccato.value then begin
          MessageDlg('Il file selezionato � bloccato da ' + data.QLKNominatUtentenominativo.AsString, mtError, [mbOK], 0);
          exit;
     end;

     xFileName := Data.QAnagFile.FieldByName('NomeFile').AsString;


     if Data.QAnagFileDocFile.asString = '' then begin
          // file salvato nel file system
          if FileExists(xFileName) then
               ShellExecute(0, 'Open', pchar(xFileName), '', '', SW_SHOWDEFAULT)
          else MessageDlg('Il file non esiste:' + chr(13) + xFileName, mtError, [mbOK], 0);
     end else begin
          // file salvato nel DB
          if (formchiamante <> 'vuoto') then BloccaAnagFile(true, formchiamante);

          xTempFilePath := PathGetTempPath;
          xTempFileName := xTempFilePath + Data.QAnagFileSoloNome.Value;
          try
               Data.QAnagFileDocFile.SaveToFile(xTempFileName);

               // apri file
               Data.QAnagFileFile.Close;
               Data.QAnagFileFile.Parameters[0].value := Data.QAnagFileID.Value;
               Data.QAnagFileFile.Open;

               Data.QAnagFileFile.Edit;
               Data.QAnagFileFileFileBloccato.value := True;
               Data.QAnagFileFileIDUtente.value := mainform.xIDUtenteAttuale;
               Data.QAnagFileFile.Post;
               Data.QAnagFileFile.Close;
               Data.QAnagFile.Refresh;

               //FileApertoForm := TFileApertoForm.create(self);
               //FileApertoForm.LNomeFile.caption := ExtractFileName(xTempFileName);
               //FileApertoForm.xFile := xTempFileName;
               //FileApertoForm.ShowModal;
               //if FileApertoForm.ModalResult = mrOK then

               //ShellExecute_AndWait(xTempFileName, '');
               ShellExecute(0, 'Open', pchar(xTempFileName), '', '', SW_SHOWDEFAULT);
               {salvafile();
               BloccaAnagFile(false, formchiamante);
               ShowMessage('Salvataggio del file completato');}

    //      finally



               {Data.QAnagFileFile.Edit;
               Data.QAnagFileFileFileBloccato.value := True;
               Data.QAnagFileFile.Post;
               Data.QAnagFileFile.Close; }

               //Data.QAnagFile.Close;
               //Data.QAnagFile.Open;
               //Data.QAnagFile.Locate('ID', xID, []);
               //Data.QAnagFile.Refresh;

               //FileApertoForm := TFileApertoForm.create(self);
               //FileApertoForm.LNomeFile.caption := ExtractFileName(xTempFileName);
               //FileApertoForm.ShowModal;
               //if FileApertoForm.ModalResult = mrOK then
               //BAnagFileSalvaClick(self);
               //FileApertoForm.Free;

         // end;
    // end;

     {if Uppercase(ExtractFileExt(xFileName))='.DOC' then
          ApriFileWordFromFile(xFileName);
     if length(ExtractFileName(xFileName))>8 then // nomi lunghi
          xFileName:=ExtractShortPathName(xFileName);
     //  WinExec(pointer('winword.exe '+),SW_SHOW);
     if Uppercase(ExtractFileExt(xFileName))='.XLS' then
          WinExec(pointer('Excel.exe '+xFileName),SW_SHOW);
     if Uppercase(ExtractFileExt(xFileName))='.MDB' then
          WinExec(pointer('Msaccess.exe '+xFileName),SW_SHOW);
     if Uppercase(ExtractFileExt(xFileName))='.TXT' then
          WinExec(pointer('notepad.exe '+xFileName),SW_SHOW);
     if (Uppercase(ExtractFileExt(xFileName))='.GIF')or
          (Uppercase(ExtractFileExt(xFileName))='.JPG')or
          (Uppercase(ExtractFileExt(xFileName))='.BMP') then
          WinExec(pointer('acdsee32.exe '+xFileName),SW_SHOW);
     if Uppercase(ExtractFileExt(xFileName))='.PDF' then
          WinExec(pointer('AcroRd32.exe '+xFileName),SW_SHOW);
     if (Uppercase(ExtractFileExt(xFileName))='.HTM')or
          (Uppercase(ExtractFileExt(xFileName))='.HTML') then
          WinExec(pointer('IEXPLORE.EXE '+xFileName),SW_SHOW);
end;  }



procedure aprifileDue(IDFile: integer; formchiamante: string);
var xFileName, xTempFileName, xTempFilePath: string;
     idtemp, Shellerror: integer;
     filetemp: TBlobField;
begin

     data.QApriFile.Close;
     data.QApriFile.Parameters.ParamByName('xIDFile').value := IDFile;
     data.QApriFile.Open;

    // data.QLKNominatUtente.Close;
     //data.QLKNominatUtente.Open;
     data.QApriFile.Locate('ID', IDFile, []);
     if data.QApriFile.fieldByName('FileBloccato').value then begin
          MessageDlg('Il file selezionato � bloccato da ' + data.QApriFileNominativo.AsString, mtError, [mbOK], 0);
          exit;
     end;

     xFileName := data.QApriFile.FieldByName('NomeFile').AsString;


     //if data.QApriFile.FieldByName('DocFile').asString = '' then begin
     if data.Global.FieldByName('AnagFileDentroDB').Value = FALSE then begin
          // file salvato nel file system
          if FileExists(xFileName) then begin
               if data.global.fieldbyname('debughrms').asboolean = true then showmessage('Prima della ShellExecute - filesystem');
               Shellerror := ShellExecute(0, 'Open', pchar(xFileName), '', '', SW_NORMAL);
               {SW_NORMAL aggiunto perch� ODGERS BERNDTSON non riesce ad aprire i file txt, prima come parametro c'era 0
               da ODGERS BERNDTSON apriva il file, ma notepad risultava nascosto e bisognava troncarlo dal task manager}
               //decodifica errori shellexecute:  http://www.tek-tips.com/viewthread.cfm?qid=1310393&page=1
               if data.global.fieldbyname('debughrms').asboolean = true then showmessage('Dopo la ShellExecute - file system');
               if data.global.fieldbyname('debughrms').asboolean = true then showmessage('ritorno della ShellExecute: ' + inttostr(Shellerror));
          end
          else MessageDlg('Il file non esiste:' + chr(13) + xFileName, mtError, [mbOK], 0);
     end else begin
          // file salvato nel DB
          if (formchiamante <> 'vuoto') then BloccaAnagFile(true, formchiamante);

         // xTempFilePath := PathGetTempPath;  //non viene pi� scaricato il file nella %temp% ma in una cartella precisa
          xTempFilePath := data.Global.fieldbyname('DirFileApriCV').asstring + '\';

          xTempFileName := xTempFilePath + data.QApriFile.FieldByname('SoloNome').Value;
          try

               data.QApriFileDocFile.SaveToFile(xTempFileName);

                // apri file
               Data.QAnagFileFile.Close;
               Data.QAnagFileFile.Parameters[0].value := IDFile;
               Data.QAnagFileFile.Open;

               Data.QAnagFileFile.Edit;
               Data.QAnagFileFileFileBloccato.value := True;
               Data.QAnagFileFileIDUtente.value := mainform.xIDUtenteAttuale;
               Data.QAnagFileFile.Post;
               Data.QAnagFileFile.Close;
              // Data.QAnagFile.Refresh;


               if data.global.fieldbyname('debughrms').asboolean = true then showmessage('Prima della ShellExecute - file DB');

               Shellerror := ShellExecute(0, 'Open', pchar(xTempFileName), '', '', SW_NORMAL);
               {SW_NORMAL aggiunto perch� intermedia non riesce ad aprire i file ppt, prima come parametro c'era 0
               da intermedia apriva il file, ma power point risultava nascosto e bisognava trobcarlo dal task manager}
               //decodifica errori shellexecute:  http://www.tek-tips.com/viewthread.cfm?qid=1310393&page=1


               if data.global.fieldbyname('debughrms').asboolean = true then showmessage('Dopo la ShellExecute - file DB');

               if data.global.fieldbyname('debughrms').asboolean = true then showmessage('ritorno della ShellExecute: ' + inttostr(Shellerror));

          finally
          end;
     end;
end;





procedure BloccaAnagFile(xCheck: boolean; chiamante: string);
begin
     if chiamante = 'main' then
     begin
          Mainform.BAnagSearch.Enabled := not xCheck;
          MainForm.edit1.Enabled := not xCheck;
          MainForm.enome.Enabled := not xCheck;
          MainForm.dbgrid6.Enabled := not xCheck;
          MainForm.dxNavBar1.enabled := not xCheck;
          MainForm.BAnagFileNew.Enabled := not xCheck;
          mainform.BFileDaModello.Enabled := not xCheck;
          MainForm.ToolbarButton9745.Enabled := not xCheck;
          MainForm.BAnagFileMod.Enabled := not xCheck;
          MainForm.BAnagFileDel.Enabled := not xCheck;
          if xcheck = false then Data.TAnagrafica.Refresh;
     end;
     if chiamante = 'schedacandform' then
     begin
          SchedaCandForm.BAnagFileNew.Enabled := not xCheck;
          SchedaCandForm.BAnagFileDel.Enabled := not xCheck;
          SchedaCandForm.ToolbarButton9745.Enabled := not xCheck;
          SchedaCandForm.DBGrid14.Enabled := not xCheck;
     end;
     if chiamante = 'sceltaanagfile' then
     begin
          SceltaAnagFileForm.TBEsci.Enabled := not xCheck;
     end;
end;

procedure AggiornaPesiAnnunci(xID: integer; xchiamante: string = 'AnnunciRicerca');
var Q1, Q2: TAdoQuery;
begin
     Q1 := TADOQuery.Create(nil);
     Q2 := TADOQuery.Create(nil);
     Q1.Connection := Data.DB;
     Q2.Connection := Data.DB;

     if xChiamante = 'AnnunciRicerca' then begin
          Q1.Close;
          Q1.Parameters.AddParameter;
          Q1.SQL.Text := 'delete from ann_AnagAnnEdizDataQuery where idanagannedizdata in ' +
               ' (select id from ann_anagannedizdata where idannedizdata in (select aed.id from ann_annedizdata aed  ' +
               'join ann_annunci a ' +
               'on a.id=aed.idannuncio ' +
               'join ann_annunciricerche ar ' +
               'on ar.idannuncio=a.id ' +
               'where ar.idricerca = :x0 )) and IDutente=:x1';
          Q1.Parameters[0].Value := xID;
          Q1.Parameters[1].Value := MainForm.xIDUtenteAttuale;
          Q1.ExecSQL;


          Q1.Close;
          Q1.SQL.Text := 'insert into ann_AnagAnnEdizDataQuery(IDAnagAnnEdizData,PesoRaggiunto,IDUtente) ' +
               'select distinct ID,0,:x0 from ann_anagannedizdata ' +
               'where idannedizdata in (select aed.id from ann_annedizdata aed ' +
               'join ann_annunci a ' +
               'on a.id=aed.idannuncio ' +
               'join ann_annunciricerche ar ' +
               'on ar.idannuncio=a.id ' +
               'where ar.idricerca = :x1 )';
          Q1.Parameters[0].Value := MainForm.xIDUtenteAttuale;
          Q1.Parameters[1].Value := xID;
          Q1.ExecSQL;

          Q1.Close;
          Q1.Parameters.Clear;
          Q1.SQL.Text := 'select * from Ann_LineeQuery where idricerca= :x0 and idutente = ' + inttostr(mainform.xIDUtenteAttuale);
          Q1.Parameters[0].Value := xID;
          Q1.Open;


          while not Q1.Eof do begin

               Q2.Close;
               //Q2.Parameters.AddParameter;
               //Q2.Parameters.AddParameter;
               Q2.SQl.Clear;
               Q2.Sql.text := 'update ann_AnagAnnEdizDataQuery set PesoRaggiunto = PesoRaggiunto + :x0 ' +
                    'where IDAnagAnnEdizData in ( ' +
                    'select ID from ann_anagannedizdata where idannedizdata in (select aed.id from ann_annedizdata aed ' +
                    'join ann_annunci a ' +
                    'on a.id=aed.idannuncio ' +
                    'join ann_annunciricerche ar ' +
                    'on ar.idannuncio=a.id ' +
                    'where ar.idricerca = :x1 ) ' +
                    Q1.FieldByName('Stringa').AsString + ')';
               if Q1.FieldByName('Peso').AsString = '' then
                    Q2.Parameters[0].Value := 0
               else
                    Q2.Parameters[0].Value := Q1.FieldByName('Peso').Value;
               Q2.Parameters[1].Value := xID;
               Q2.ExecSQL;

               Q1.Next;
          end;
     end;
      ///////////////////////////////////////////////////////////////////////////
     if xChiamante = 'AnnunciElenco' then begin
          Q1.Close;
          Q1.Parameters.AddParameter;
          Q1.SQL.Text := 'delete from ann_AnagAnnEdizDataQuery where idanagannedizdata in ' +
               ' (select id from ann_anagannedizdata where idannedizdata in (select aed.id from ann_annedizdata aed  ' +
               'join ann_annunci a ' +
               'on a.id=aed.idannuncio ' +
               'join ann_annunciricerche ar ' +
               'on ar.idannuncio=a.id ' +
               'where ar.idannuncio = :x0 ) )and IDutente=:x1';
          Q1.Parameters[0].Value := xID;
          Q1.Parameters[1].Value := MainForm.xIDUtenteAttuale;
          Q1.ExecSQL;

          Q1.Close;
          Q1.SQL.Text := 'insert into ann_AnagAnnEdizDataQuery(IDAnagAnnEdizData,PesoRaggiunto,IDUtente) ' +
               'select distinct ID,0,:x0 from ann_anagannedizdata ' +
               'where idannedizdata in (select aed.id from ann_annedizdata aed ' +
               'join ann_annunci a ' +
               'on a.id=aed.idannuncio ' +
               'join ann_annunciricerche ar ' +
               'on ar.idannuncio=a.id ' +
               'where ar.idannuncio = :x1 )';
          Q1.Parameters[0].Value := MainForm.xIDUtenteAttuale;
          Q1.Parameters[1].Value := xID;
          Q1.ExecSQL;

          Q1.Close;
          Q1.Parameters.Clear;
          Q1.SQL.Text := 'select * from Ann_LineeQuery where idannuncio= :x0 and idutente = ' + inttostr(mainform.xIDUtenteAttuale);
          Q1.Parameters[0].Value := xID;
          Q1.Open;


          while not Q1.Eof do begin

               Q2.Close;
               //Q2.Parameters.AddParameter;
               //Q2.Parameters.AddParameter;
               Q2.SQl.Clear;
               Q2.Sql.text := 'update ann_AnagAnnEdizDataQuery set PesoRaggiunto = PesoRaggiunto + :x0 ' +
                    'where IDAnagAnnEdizData in ( ' +
                    'select ID from ann_anagannedizdata where idannedizdata in (select aed.id from ann_annedizdata aed ' +
                    'join ann_annunci a ' +
                    'on a.id=aed.idannuncio ' +
                    'join ann_annunciricerche ar ' +
                    'on ar.idannuncio=a.id ' +
                    'where ar.idannuncio = :x1 ) ' +
                    Q1.FieldByName('Stringa').AsString + ')';
               if Q1.FieldByName('Peso').AsString = '' then
                    Q2.Parameters[0].Value := 0
               else
                    Q2.Parameters[0].Value := Q1.FieldByName('Peso').Value;
               Q2.Parameters[1].Value := xID;
               Q2.ExecSQL;

               Q1.Next;
          end;

     end;
     Q1.Close;
     Q2.Close;

     Q1.Free;
     Q2.Free;
end;


function CreateGuid: string;
var
     ID: TGUID;
begin
     Result := '';
     if CoCreateGuid(ID) = S_OK then
          Result := GUIDToString(ID);
end;

function GeneraUsername(xemail, xcognome, xnome, xusername: string): string;
var UserNameTemp: string;
     Q1: TAdoQuery;
begin
     if data.global.fieldbyname('GeneraUsername').asboolean = false then begin
          GeneraUsername := '';
          exit;
     end else begin

          if xusername = '' then begin
               Q1 := TADOQuery.Create(nil);
               q1.ConnectionString := Data.db.ConnectionString;
               Q1.close;
               Q1.sql.text := 'select id from anagrafica where email=' + '''' + xemail + '''';
               Q1.open;
               if q1.RecordCount = 0 then result := xemail else
               begin
          //regola  creazione username*
                    UserNameTemp := StringReplace(StringReplace(xcognome, ' ', '', [rfReplaceAll]) + copy(trim(xnome), 1, 1), '''', '', [rfReplaceAll]);
                    Q1.close;
                    Q1.sql.text := 'select id from anagrafica where username=' + '''' + UserNameTemp + '''';
                    Q1.open;
                    if Q1.RecordCount > 0 then
                         result := StringReplace(xcognome, ' ', '', [rfReplaceAll]) + copy(CreateGuid, 2, 8)
                    else
                         result := UserNameTemp;
               end;
               Q1.free;
          end;
     end;
     {* REGOLA CREAZIONE USERNAME
     crea username=cognome + 1 lettera nome
     se esiste gia all'interno del db allora
     username= cognome + 8 caratteri casuali
     }
end;

function DataModificaFile(xFile: string): TDateTime;
var
     FileH: THandle;
     LocalFT: TFileTime;
     DosFT: DWORD;
     LastAccessedTime: TDateTime;
     FindData: TWin32FindData;
begin
     //Result := '';
     FileH := FindFirstFile(PChar(xFile), FindData);
     {if FileH <> INVALID_HANDLE_VALUE then begin
          Windows.FindClose(Handle);
          if (FindData.dwFileAttributes and
               FILE_ATTRIBUTE_DIRECTORY) = 0 then
          begin     }
     FileTimeToLocalFileTime
          (FindData.ftLastWriteTime, LocalFT);
     FileTimeToDosDateTime
          (LocalFT, LongRec(DosFT).Hi, LongRec(DosFT).Lo);
     LastAccessedTime := FileDateToDateTime(DosFT);
     //Result := DateTimeToStr(LastAccessedTime);
     Result := LastAccessedTime;
          //end;
    // end;
end;

procedure Delay(Millisec: Longint);
var
     Conta: DWord;
begin
     Conta := GetTickCount + DWord(Millisec);
     repeat
          Application.ProcessMessages;
     until (GetTickCount > Conta);
end;



function MessaggioATempo(Messaggio, Titolo: string; Bottoni: TMsgDlgBtns; xtempo: integer): TModalResult;
var
     nf: TForm;
     lb: TLabel;
     b1, b2, b3, b4, b5: TButton;
begin

     // crea
     nf := TForm.Create(nil);
     lb := TLabel.Create(nf);
// finestra -> posizione, atezza, larghezza
     nf.Position := poScreenCenter;
     nf.Height := 100;
     nf.Width := 400;
     nf.position := poDesktopCenter;
     nf.BorderIcons := [];
// label -> posizione, atezza, larghezza
     lb.Parent := nf;
     lb.AutoSize := false;
     lb.top := 8;
     lb.left := 8;
     lb.Height := 200 - 16;
     lb.Width := 400 - 16;
     lb.Align := alClient;
     lb.WordWrap := true;
// titolo, messaggio
     nf.Caption := Titolo;
     lb.Caption := Messaggio;
// bottoni
     if mbYes in Bottoni then
     begin
          b1 := TButton.Create(nf);
          b1.Parent := nf;
          b1.Height := 23;
          b1.Width := 40;
          b1.top := lb.ClientHeight - 48;
          b1.left := lb.ClientWidth - 80;
          b1.Caption := '&Si';
          b1.ModalResult := mrYes;
     end;
   //    nf.Borderstyle:= bsToolWindow;
     nf.show;
     delay(xtempo);
     nf.close;
     nf.free;

end;


end.

