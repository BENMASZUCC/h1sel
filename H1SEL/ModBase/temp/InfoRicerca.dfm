�
 TINFORICERCAFORM 0   TPF0TInfoRicercaFormInfoRicercaFormLeft9Top� WidthWHeight�CaptionInfo RicercaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style PositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight 	TGroupBox	GroupBox3Left(Top5WidthHeight� CaptionCaratteristiche ricercateTabOrder  TDBGridDBGrid2Left
TopWidthHeightm
DataSourceDataSel_EBC.DsCarattRicReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style Columns	FieldNameCaratteristicaWidth�      	TGroupBox	GroupBox4LeftTop� WidthHeight9CaptionDati personali (anagrafici)EnabledTabOrder TLabelLabel1LeftTopWidthHeightCaptionEt�:  TLabelLabel2LeftTop0WidthgHeightCaptionDomiciliato in prov. di:  TLabelLabel3LeftTopJWidth HeightCaptionSesso:  TLabelLabel4LeftTopbWidth8HeightCaptionStato Civile:  TLabelLabel5LeftTopyWidthTHeightCaptionDisponibilit� Auto:  TLabelLabel6LeftTop� WidthLHeightCaptionServizio Militare:  TLabelLabel7LeftTop� WidthqHeightCaptionDisponibilit� movimento:  TDBEditDBEdit1Left� TopWidth/Height	DataFieldC_Eta
DataSourceDataSel_EBC.DsRicerchePendTabOrder   TDBEditDBEdit2Left� Top.WidthHeight	DataFieldC_DomicilioProv
DataSourceDataSel_EBC.DsRicerchePendTabOrder  TDBEditDBEdit3Left� TopFWidthHeight	DataFieldC_Sesso
DataSourceDataSel_EBC.DsRicerchePendTabOrder  TDBEditDBEdit4Left� Top^WidthVHeight	DataFieldC_StatoCivile
DataSourceDataSel_EBC.DsRicerchePendTabOrder  TDBEditDBEdit5Left� TopvWidthHeight	DataField
C_DispAuto
DataSourceDataSel_EBC.DsRicerchePendTabOrder  TDBEditDBEdit6Left� Top� WidthoHeight	DataFieldC_ServMilitare
DataSourceDataSel_EBC.DsRicerchePendTabOrder  TDBEditDBEdit7LeftTop� Width� Height	DataField	C_DispMov
DataSourceDataSel_EBC.DsRicerchePendTabOrder   	TGroupBox	GroupBox1LeftTop%WidthHeight^CaptionAree-ruoli-specializzazioniEnabledTabOrder 	TCheckBoxCBAreaLeftTopWidthoHeightCaptionArea:EnabledState	cbCheckedTabOrder   	TCheckBoxCBRuoloLeftTop.WidthoHeightCaptionRuolo:EnabledState	cbCheckedTabOrder  	TCheckBoxCBSpecLeftTopFWidthoHeightCaptionSpecializzazione:TabOrder  TDBLookupComboBoxDBLAreaLeftxTopWidth� Height	DataFieldArea
DataSourceDataSel_EBC.DsRicerchePendTabOrder  TDBLookupComboBoxDBLRuoloLeftxTop*Width� Height	DataFieldMansione
DataSourceDataSel_EBC.DsRicerchePendTabOrder  TDBLookupComboBoxDBLSpecLeftxTopBWidth� HeightColor	clBtnFace	DataFieldSpec
DataSourceDataSel_EBC.DsRicerchePendEnabledTabOrder   	TGroupBox	GroupBox6Left)Top%WidthHeight� Caption-Competenze predefinite per la mansione sceltaTabOrder TDBGridDBGrid3LeftTopWidth	Height� Color	clBtnFace
DataSourceDataSel_EBC.DsCompMansioneReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style Columns	FieldName
CompetenzaWidth�  	FieldName	OperatoreTitle.CaptionOp.Width 	FieldNameValoreTitle.CaptionVal.Width 	FieldNameMaxCompTitle.CaptionMaxWidth 	AlignmenttaCenter	FieldNameIndispensabileFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold Title.CaptionInd.Width     	TGroupBox	GroupBox2Left)Top� WidthHeightaCaptionTitoli di studioTabOrder  TBitBtnBitBtn1Left�TopWidthPHeightCaptionEsciTabOrderKindbkClose   