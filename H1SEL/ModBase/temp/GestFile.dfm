object GestFileForm: TGestFileForm
  Left = 364
  Top = 195
  Width = 913
  Height = 481
  Caption = 'Gestione File'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  inline GestFileFrame1: TGestFileFrame
    Width = 905
    Height = 454
    Align = alClient
    inherited Panel177: TPanel
      Top = 429
      Width = 905
    end
    inherited dxDBGrid14: TdxDBGrid
      Width = 905
      Height = 368
      Filter.Criteria = {00000000}
      inherited dxDBGrid14DataCreazione: TdxDBGridDateColumn
        StoredRowIndex = 1
      end
      inherited dxDBGrid14Tipo: TdxDBGridMaskColumn
        StoredRowIndex = 1
      end
      inherited dxDBGrid14FlagInvio: TdxDBGridCheckColumn
        StoredRowIndex = 1
      end
      inherited dxDBGrid14Column7: TdxDBGridCheckColumn
        StoredRowIndex = 1
      end
    end
    inherited Panel123: TPanel
      Width = 905
    end
    inherited Panel124: TPanel
      Width = 905
      inherited SpeedButton19: TSpeedButton
        Left = 876
      end
    end
  end
end
