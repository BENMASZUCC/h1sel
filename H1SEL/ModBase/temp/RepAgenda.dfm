�
 TQRAGENDA 0R!  TPF0	TQRAgendaQRAgendaLeft� TopWidthfHeight�CaptionQRAgendaColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderScaledPixelsPerInch`
TextHeight 	TQuickRepQR1Left Top WidthHeightcFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightDataSetQAgendaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style Functions.Strings
PAGENUMBERCOLUMNNUMBERREPORTTITLE Functions.DATA00'' OptionsFirstPageHeaderLastPageFooter Page.ColumnsPage.Orientation
poPortraitPage.PaperSizeA4Page.Values�4  PrinterSettings.CopiesPrinterSettings.DuplexPrinterSettings.FirstPage PrinterSettings.LastPage PrinterSettings.OutputBinFirstPrintIfEmpty	
SnapToGrid	UnitsNativeZoomd TQRBandDetailBand1Left0Top� Width�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values      @�@��UUUU��	@ BandTyperbDetail 	TQRDBText	QRDBText1LeftSTopWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values2Ъ����@~������@IUUUUU� @      ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetQAgenda	DataFieldDataFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize
  TQRShapeQRShape1Left TopWidth�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values       �@ �/UUUUU�@c5������	@ Shape
qrsHorLine  TQRLabelQRLabel7Left TopWidthOHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@ [sUUUUU�@��TUUU�@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionQRLabel7ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsItalic OnPrintQRLabel7Print
ParentFontTransparentWordWrap	FontSize  	TQRDBText	QRDBText3Left.TopWidthEHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values2Ъ����@ª�����@IUUUUU� @      ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetQAgenda	DataFieldDescrizioneTransparentWordWrap	FontSize
  	TQRDBText	QRDBText2Left� TopWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values2Ъ����@      0�@IUUUUU� @�/UUUU��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetQAgenda	DataFieldOreTransparentWordWrap	FontSize
  TQRLabelQRLabel2Left� TopWidth:HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values2Ъ����@      ؒ@IUUUUU� @��TUUUu�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionQRLabel2ColorclWhiteOnPrintQRLabel2PrintTransparentWordWrap	FontSize
  TQRLabelQRLabel8Left�TopWidth0HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@c5�����	@IUUUUU� @ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionQRLabel8ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style OnPrintQRLabel8Print
ParentFontTransparentWordWrap	FontSize   TQRBandPageFooterBand1Left0Top� Width�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values       �@��UUUU��	@ BandTyperbPageFooter  TQRBand
TitleBand1Left0Top0Width�HeightGFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values~����ڻ@��UUUU��	@ BandTyperbTitle TQRLabelQRLabel1Left TopWidth�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values痪���J�@ e�UUUUU�@c5������	@ 	AlignmenttaCenterAlignToBandAutoSizeAutoStretchCaptionStampa agendaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBoldfsUnderline 
ParentFontTransparentWordWrap	FontSize  TQRLabelQRDateLeft Top1Width/HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values2Ъ����@ ��TUUU��@�VUUU��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionQRDateColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsItalic 
ParentFontTransparentWordWrap	FontSize
   TQRBandColumnHeaderBand1Left0TopwWidth�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values2Ъ���j�@��UUUU��	@ BandTyperbColumnHeader TQRLabelQRLabel3LeftTopWidth� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values2Ъ����@IUUUUU� @7�TUUUU�@�{����z�@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionData e giornoColorclSilverFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsItalic 
ParentFontTransparentWordWrap	FontSize
  TQRLabelQRLabel4Left� TopWidthIHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values2Ъ����@?�UUUU��@7�TUUUU�@��TUUU%�@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionOreColorclSilverFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsItalic 
ParentFontTransparentWordWrap	FontSize
  TQRLabelQRLabel5Left� TopWidth� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values2Ъ����@ª���.�@7�TUUUU�@      ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionTipoColorclSilverFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsItalic 
ParentFontTransparentWordWrap	FontSize
  TQRLabelQRLabel6Left-TopWidth� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values2Ъ����@�=UUUU�@7�TUUUU�@�=UUUUi�@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionDescrizioneColorclSilverFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsItalic 
ParentFontTransparentWordWrap	FontSize
  TQRLabelQRLabel9Left�TopWidth� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values2Ъ����@      ��	@7�TUUUU�@�=UUUU��@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionRif. ricerca (se esistente)ColorclSilverFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsItalic 
ParentFontTransparentWordWrap	FontSize    TADOLinkedQueryQAgenda
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect * from agenda where IDUtente=1and datepart(year,data)=2002order by data,ore OriginalSQL.Stringsselect * from agenda where Data=:Dataand IDUtente=5order by Ore 	UseFilterLeftTop TAutoIncField	QAgendaID	FieldNameIDReadOnly	  TDateTimeFieldQAgendaData	FieldNameData  TDateTimeField
QAgendaOre	FieldNameOreDisplayFormathh.mm  TIntegerFieldQAgendaIDUtente	FieldNameIDUtente  TSmallintFieldQAgendaTipo	FieldNameTipo  TIntegerFieldQAgendaIDCandRic	FieldName	IDCandRic  TStringFieldQAgendaDescrizione	FieldNameDescrizioneSize2  TIntegerFieldQAgendaIDRisorsa	FieldName	IDRisorsa  TDateTimeFieldQAgendaAlleOre	FieldNameAlleOre   TADOLinkedQueryQ
ConnectionData.DB
Parameters 	UseFilterLeftTop8   