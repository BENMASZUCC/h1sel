object SelEspLavForm: TSelEspLavForm
  Left = 256
  Top = 106
  BorderStyle = bsDialog
  Caption = 'Selezione esperienza lavorativa attuale'
  ClientHeight = 201
  ClientWidth = 629
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 629
    Height = 49
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object BitBtn1: TBitBtn
      Left = 417
      Top = 5
      Width = 102
      Height = 39
      TabOrder = 0
      Kind = bkOK
    end
    object BitBtn2: TBitBtn
      Left = 521
      Top = 5
      Width = 102
      Height = 39
      Caption = 'Annulla'
      TabOrder = 1
      Kind = bkCancel
    end
    object BNuova: TBitBtn
      Left = 7
      Top = 5
      Width = 102
      Height = 39
      Caption = 'Inserisci nuova'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnClick = BNuovaClick
    end
  end
  object dxDBGrid1: TdxDBGrid
    Left = 0
    Top = 49
    Width = 629
    Height = 152
    Bands = <
      item
      end>
    DefaultLayout = True
    HeaderPanelRowCount = 1
    KeyField = 'ID'
    SummaryGroups = <>
    SummarySeparator = ', '
    Align = alClient
    TabOrder = 1
    DataSource = DsQEspLav
    Filter.Criteria = {00000000}
    LookAndFeel = lfFlat
    OptionsBehavior = [edgoAutoSearch, edgoAutoSort, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoTabThrough, edgoVertThrough]
    OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
    OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoIndicator, edgoUseBitmap]
    object dxDBGrid1Azienda: TdxDBGridMaskColumn
      Width = 149
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Azienda'
    end
    object dxDBGrid1Attivita: TdxDBGridMaskColumn
      Width = 167
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Attivita'
    end
    object dxDBGrid1AnnoDal: TdxDBGridMaskColumn
      Caption = 'Dall'#39'anno'
      Sorted = csDown
      Width = 72
      BandIndex = 0
      RowIndex = 0
      FieldName = 'AnnoDal'
    end
    object dxDBGrid1MeseDal: TdxDBGridMaskColumn
      Caption = 'Dal mese'
      Width = 70
      BandIndex = 0
      RowIndex = 0
      FieldName = 'MeseDal'
    end
    object dxDBGrid1Ruolo: TdxDBGridMaskColumn
      Width = 157
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Ruolo'
    end
  end
  object DsQEspLav: TDataSource
    DataSet = QEspLav
    Left = 48
    Top = 112
  end
  object QEspLav: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'xIDAnag'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select EBC_Clienti.Descrizione Azienda, EBC_Attivita.Attivita,'
      '       EsperienzeLavorative.ID,EsperienzeLavorative.IDMansione,'
      '       AnnoDal,MeseDal, Mansioni.Descrizione Ruolo'
      'from EsperienzeLavorative, EBC_Clienti, EBC_Attivita, Mansioni'
      'where EsperienzeLavorative.IDAzienda = EBC_Clienti.ID'
      '  and EBC_Clienti.IDAttivita = EBC_Attivita.ID'
      '  and EsperienzeLavorative.IDMansione *= Mansioni.ID'
      'and IDAnagrafica=:xIDAnag'
      'and (attuale=0 or attuale is null)'
      'order by EsperienzeLavorative.ID'
      '')
    Left = 48
    Top = 80
    object QEspLavAzienda: TStringField
      FieldName = 'Azienda'
      Size = 50
    end
    object QEspLavAttivita: TStringField
      FieldName = 'Attivita'
      Size = 50
    end
    object QEspLavID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QEspLavIDMansione: TIntegerField
      FieldName = 'IDMansione'
    end
    object QEspLavAnnoDal: TSmallintField
      FieldName = 'AnnoDal'
    end
    object QEspLavMeseDal: TSmallintField
      FieldName = 'MeseDal'
    end
    object QEspLavRuolo: TStringField
      FieldName = 'Ruolo'
      Size = 40
    end
  end
  object Q: TADOLinkedQuery
    Connection = Data.DB
    Parameters = <>
    UseFilter = False
    Left = 48
    Top = 160
  end
end
