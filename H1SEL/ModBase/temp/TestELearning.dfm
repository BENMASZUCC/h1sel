object TestELearningForm: TTestELearningForm
  Left = 362
  Top = 240
  Width = 515
  Height = 246
  Caption = 'Elenco Test E-learning'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object AdvPanel1: TAdvPanel
    Left = 0
    Top = 195
    Width = 507
    Height = 24
    Align = alBottom
    TabOrder = 0
    UseDockManager = True
    Version = '1.9.0.3'
    Caption.Color = clHighlight
    Caption.ColorTo = clNone
    Caption.Font.Charset = DEFAULT_CHARSET
    Caption.Font.Color = clHighlightText
    Caption.Font.Height = -11
    Caption.Font.Name = 'MS Sans Serif'
    Caption.Font.Style = []
    StatusBar.Font.Charset = DEFAULT_CHARSET
    StatusBar.Font.Color = clWindowText
    StatusBar.Font.Height = -11
    StatusBar.Font.Name = 'Tahoma'
    StatusBar.Font.Style = []
    FullHeight = 0
    object bIns: TToolbarButton97
      Left = 2
      Top = 1
      Width = 51
      Height = 22
      Caption = 'Nuovo'
      OnClick = bInsClick
    end
    object bDel: TToolbarButton97
      Left = 53
      Top = 1
      Width = 51
      Height = 22
      Caption = 'Cancella'
      OnClick = bDelClick
    end
    object bCanc: TToolbarButton97
      Left = 172
      Top = 1
      Width = 51
      Height = 22
      Caption = 'Annulla'
      OnClick = bCancClick
    end
    object bOK: TToolbarButton97
      Left = 121
      Top = 1
      Width = 51
      Height = 22
      Caption = 'Conferma'
      OnClick = bOKClick
    end
    object BitBtn1: TBitBtn
      Left = 432
      Top = -1
      Width = 75
      Height = 25
      TabOrder = 0
      Kind = bkOK
    end
  end
  object dxDBGrid1: TdxDBGrid
    Left = 0
    Top = 0
    Width = 507
    Height = 195
    Bands = <
      item
      end>
    DefaultLayout = True
    HeaderPanelRowCount = 1
    KeyField = 'ID'
    SummaryGroups = <>
    SummarySeparator = ', '
    Align = alClient
    TabOrder = 1
    DataSource = dsTest
    Filter.Active = True
    Filter.Criteria = {00000000}
    OptionsBehavior = [edgoAutoSort, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoTabThrough, edgoVertThrough]
    OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
    object dxDBGrid1ID: TdxDBGridMaskColumn
      Visible = False
      Width = 50
      BandIndex = 0
      RowIndex = 0
      FieldName = 'ID'
    end
    object dxDBGrid1CodiceTest: TdxDBGridMaskColumn
      Caption = 'Codice'
      Width = 166
      BandIndex = 0
      RowIndex = 0
      FieldName = 'CodiceTest'
    end
    object dxDBGrid1DescrizioneTest: TdxDBGridMaskColumn
      Caption = 'Descrizione'
      Width = 335
      BandIndex = 0
      RowIndex = 0
      FieldName = 'DescrizioneTest'
    end
  end
  object qTest: TADOQuery
    Connection = Data.DB
    Parameters = <>
    SQL.Strings = (
      'select * from TestELearning'
      '')
    Left = 40
    Top = 32
    object qTestID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object qTestDescrizioneTest: TStringField
      FieldName = 'DescrizioneTest'
      Size = 500
    end
    object qTestCodiceTest: TStringField
      FieldName = 'CodiceTest'
      Size = 100
    end
  end
  object dsTest: TDataSource
    DataSet = qTest
    OnStateChange = dsTestStateChange
    Left = 96
    Top = 32
  end
end
