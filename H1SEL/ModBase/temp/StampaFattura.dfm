�
 TSTAMPAFATTURAREP1 0�9  TPF0TStampaFatturaRep1StampaFatturaRep1Left Top Width�HeightMFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightDataSetData.FattDettFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style OptionsFirstPageHeaderLastPageFooter Page.ColumnsPage.Orientation
poPortraitPage.PaperSizeLetterPage.Valuesd�
xo22  PrintIfEmptyPrinterSettings.CopiesPrinterSettings.DuplexPrinterSettings.FirstPage PrinterSettings.LastPage PrinterSettings.OutputBinFirst
SnapToGrid	UnitsMMZoomP TQRBandColumnHeaderBand1LeftTop� WidthoHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameArial
Font.StylefsBold ForceNewColumnForceNewPage
ParentFontSize.Values痪���J�@�UUUǀ
@ BandTyperbColumnHeader TQRShapeQRShape7LeftTop WidthCHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values痪���J�@��������@      �]�	@ Brush.ColorclSilverShapeqrsRectangle  TQRLabelQRLabel8Left	TopWidth7HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�/UUUU5�@       �@      ��@~�����@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionDescrizioneColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsItalic 
ParentFontTransparent	WordWrap	FontSize
  TQRLabelQRLabel9Left�TopWidth7HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�/UUUU5�@      ��	@�@������@~�����@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchCaptionImportoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsItalic 
ParentFontTransparent	WordWrap	FontSize
   TQRBandDetailBand1LeftTop� WidthoHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style ForceNewColumnForceNewPage
ParentFontSize.Values      �@�UUUǀ
@ BandTyperbDetail TQRShapeQRShape6LeftTop WidthCHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      �@��������@      �]�	@ ShapeqrsRectangle  	TQRDBText	QRDBText6Left
TopWidth7HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�/UUUU5�@2Ъ���J�@      ��@~�����@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetData.FattDett	DataFieldDescrizioneTransparentWordWrap	FontSize
  	TQRDBText
QRDBText19LeftTopWidth#HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�/UUUU5�@��UUUձ�	@      ��@頻����@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetData.FattDett	DataFieldImportoMask
##,###,###TransparentWordWrap	FontSize
   TQRBand
TitleBand1LeftTop$WidthoHeight� Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values      Y�@�UUUǀ
@ BandTyperbPageHeader 	TQRDBText	QRDBText2Left�TopWidthQHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�/UUUU5�@�hUUUU5�@      �@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetData.Clienti	DataFieldDenominazioneFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize
  	TQRDBText	QRDBText4Left�Top9Width6HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�/UUUU5�@c5�����	@      ��@      ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetData.Clienti	DataFieldCitta'LegaleTransparentWordWrap	FontSize
  	TQRDBText	QRDBText3Left�Top+WidthFHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�/UUUU5�@�~����6�@~������@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetData.Clienti	DataFieldIndirizzoLegaleTransparentWordWrap	FontSize
  	TQRDBText	QRDBText9Left�Top:Width%HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�/UUUU5�@�~����ҿ@�VUUU��@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetData.Clienti	DataField	CAPLegaleTransparentWordWrap	FontSize
  TQRLabelQRLabel1LeftTopWidthIHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values2Ъ���Z�@�@������@��������@~����n�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionFATTURAColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBoldfsUnderline 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText12Left$Top� WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�/UUUU5�@       �@?�UUUU��@hUUUU��@ 	AlignmenttaCenterAlignToBandAutoSize	AutoStretchColorclWhiteDataSetData.Fatture	DataFieldDataTransparentWordWrap	FontSize
  	TQRDBText
QRDBText14Left�TopMWidth0HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�/UUUU5�@c5���*$�	@��TUUU��@      ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetData.Clienti	DataField
PartitaIVATransparentWordWrap	FontSize
  	TQRDBText
QRDBText23Left�Top^WidthBHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�/UUUU5�@     ���	@?�UUUUq�@      H�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetData.Clienti	DataFieldCodiceFiscaleTransparentWordWrap	FontSize
  TQRLabelQRLabel2LeftTop� WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�/UUUU5�@�_������@      R�@      ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionN�ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsUnderline 
ParentFontTransparentWordWrap	FontSize
  TQRLabelQRLabel3LeftTop� WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�/UUUU5�@�_������@?�UUUU��@痪�����@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionData:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsUnderline 
ParentFontTransparentWordWrap	FontSize
  TQRLabelQRLabel4LeftTopNWidth6HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�/UUUU5�@c5���*V�	@      ��@      ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionPartita IVA:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsUnderline 
ParentFontTransparentWordWrap	FontSize
  TQRLabelQRLabel5LeftTop_WidthEHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�/UUUU5�@c5���*V�	@�{�����@      4�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionCodice fiscale:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsUnderline 
ParentFontTransparentWordWrap	FontSize
  TQRLabelQRLabel6Left[TopWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�/UUUU5�@c5���*t�	@hUUUU5�@痪���ҿ@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionSpett.ColorclWhiteTransparentWordWrap	FontSize
   TQRBandQRBand1LeftTopvWidthoHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values�VUUU�@�UUUǀ
@ BandTyperbPageFooter  	TQRDBText	QRDBText5Left3Top� Width$HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      p�@      ��@�=UUUU��@       �@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetData.Fatture	DataFieldProgressivoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize
  TQRBandSummaryBand1LeftTop� WidthoHeightxFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values      p�@�UUUǀ
@ BandType	rbSummary TQRShapeQRShape9Left�TopWidth}HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValueshUUUU��@      ��	@      �@�{������@ ShapeqrsRectangle  TQRShape	QRShape11Left�Top*Width}HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValueshUUUU��@      ��	@      �@�{������@ ShapeqrsRectangle  TQRShape	QRShape12LefthTop*WidthaHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValueshUUUU��@      Ԕ	@      �@?�UUUUg�@ Brush.ColorclSilverShapeqrsRectangle  TQRShape	QRShape10LefthTopWidthaHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValueshUUUU��@      Ԕ	@      �@?�UUUUg�@ Brush.ColorclSilverShapeqrsRectangle  TQRShapeQRShape8LefthTop WidthaHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValueshUUUU��@      Ԕ	@ ?�UUUUg�@ Brush.ColorclSilverShapeqrsRectangle  TQRShapeQRShape3Left�Top Width}HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValueshUUUU��@      ��	@ �{������@ ShapeqrsRectangle  	TQRDBText
QRDBText22LeftTopWidth#HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�/UUUU5�@      H�	@�@������@頻����@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetData.Fatture	DataFieldImportoMask
##,###,###TransparentWordWrap	FontSize
  	TQRDBText
QRDBText21LeftTop-Width!HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�/UUUU5�@c5�����	@      Ԕ@      H�@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetData.Fatture	DataFieldTotaleFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold Mask
##,###,###
ParentFontTransparentWordWrap	FontSize
  TQRLabel	QRLabel11LeftsTopWidth:HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�/UUUU5�@c5���*`�	@�@������@~����ҿ@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionImponibile:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	WordWrap	FontSize
  	TQRDBText	QRDBText7LeftTopWidth#HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�/UUUU5�@      H�	@hUUUU]�@頻����@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetData.Fatture	DataFieldIVAMask
##,###,###TransparentWordWrap	FontSize
  TQRLabel	QRLabel12LeftsTopWidth)HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�/UUUU5�@c5���*`�	@hUUUU]�@��TUUU��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionIVA 20%ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	WordWrap	FontSize
  TQRLabel	QRLabel13LeftsTop.WidthKHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�/UUUU5�@c5���*`�	@~����"�@      �@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionTotale Fattura:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFontTransparent	WordWrap	FontSize
  TQRLabel	QRLabel10Left	TopAWidth^HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�/UUUU5�@       �@��TUUU��@?�UUUUq�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionModalit� pagamentoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsUnderline 
ParentFontTransparentWordWrap	FontSize
  	TQRDBText	QRDBText1Left	TopPWidth]HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�/UUUU5�@       �@�{����J�@      ʙ@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetData.Fatture	DataFieldModalitaPagamentoTransparentWordWrap	FontSize
  	TQRDBText
QRDBText25Left	Top^Width*HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�/UUUU5�@       �@?�UUUUq�@      �@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetData.Fatture	DataFieldAssegnoTransparentWordWrap	FontSize
    