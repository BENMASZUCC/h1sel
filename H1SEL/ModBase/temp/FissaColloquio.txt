object FissaColloquioForm: TFissaColloquioForm
  Left = 541
  Top = 331
  ActiveControl = Ora
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'Fissazione colloquio'
  ClientHeight = 252
  ClientWidth = 285
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 5
    Top = 4
    Width = 183
    Height = 57
    Caption = 'Agenda'
    TabOrder = 0
    object Label4: TLabel
      Left = 7
      Top = 14
      Width = 26
      Height = 13
      Caption = 'Data:'
    end
    object Label5: TLabel
      Left = 100
      Top = 14
      Width = 24
      Height = 13
      Caption = 'Dalle'
    end
    object Label1: TLabel
      Left = 140
      Top = 14
      Width = 17
      Height = 13
      Caption = 'Alle'
    end
    object Ora: TMaskEdit
      Left = 100
      Top = 28
      Width = 37
      Height = 21
      EditMask = '!90:00;1;_'
      MaxLength = 5
      TabOrder = 0
      Text = '  .  '
    end
    object DEData: TDateEdit97
      Left = 7
      Top = 28
      Width = 90
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      ColorCalendar.ColorValid = clBlue
      DayNames.Monday = 'lu'
      DayNames.Tuesday = 'ma'
      DayNames.Wednesday = 'me'
      DayNames.Thursday = 'gi'
      DayNames.Friday = 've'
      DayNames.Saturday = 'sa'
      DayNames.Sunday = 'do'
      MonthNames.January = 'gennaio'
      MonthNames.February = 'febbraio'
      MonthNames.March = 'marzo'
      MonthNames.April = 'aprile'
      MonthNames.May = 'maggio'
      MonthNames.June = 'giugno'
      MonthNames.July = 'luglio'
      MonthNames.August = 'agosto'
      MonthNames.September = 'settembre'
      MonthNames.October = 'ottobre'
      MonthNames.November = 'novembre'
      MonthNames.December = 'dicembre'
      Options = [doButtonTabStop, doCanPopup, doIsMasked, doShowCancel, doShowToday]
    end
    object MEAlleOre: TMaskEdit
      Left = 139
      Top = 28
      Width = 37
      Height = 21
      EditMask = '!90:00;1;_'
      MaxLength = 5
      TabOrder = 1
      Text = '  .  '
    end
  end
  object DBGrid1: TDBGrid
    Left = 5
    Top = 68
    Width = 145
    Height = 22
    DataSource = DsSelez
    Options = [dgColumnResize, dgTabs, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'Nominativo'
        Width = 105
        Visible = True
      end>
  end
  object BitBtn1: TBitBtn
    Left = 193
    Top = 5
    Width = 89
    Height = 33
    TabOrder = 2
    OnClick = BitBtn1Click
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 193
    Top = 73
    Width = 89
    Height = 33
    Caption = 'Agenda'
    TabOrder = 3
    OnClick = BitBtn2Click
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      0400000000000001000000000000000000001000000010000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333330000033
      33333333377777F33333333330BBB0333333333337FFF7F33333333330000033
      33333333377777F33333333330BBB0333333333337FFF7FF3333333300000003
      333333337777777FF33333300BBBBB003333333773333377F3333330BBFFFBB0
      33333337F3333337FF333300BFFFFFB003333377F333FFF773F33300BFF077B0
      30333377F3377737F7333300BFF7FFB003333377F337F33773333330BBF7FBB0
      33333337FF37F337F33333300BB7BB00333333377FF7FF773333333300000003
      33333333777777733333333330BBB0333333333337FFF7F33333333330000033
      33333333377777F33333333330BBB03333333333373337333333}
    NumGlyphs = 2
  end
  object CBInterno: TCheckBox
    Left = 5
    Top = 96
    Width = 129
    Height = 17
    Caption = 'Colloquio Interno'
    Checked = True
    State = cbChecked
    TabOrder = 4
    OnClick = CBInternoClick
  end
  object BitBtn3: TBitBtn
    Left = 193
    Top = 39
    Width = 89
    Height = 33
    Caption = 'Annulla'
    TabOrder = 5
    Kind = bkCancel
  end
  object DBGrid2: TDBGrid
    Left = 5
    Top = 118
    Width = 144
    Height = 110
    DataSource = DsRisorse
    ReadOnly = True
    TabOrder = 6
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'Risorsa'
        Title.Caption = 'Luogo'
        Width = 110
        Visible = True
      end>
  end
  object CBLuogo: TCheckBox
    Left = 5
    Top = 232
    Width = 153
    Height = 17
    Caption = 'non registrare luogo'
    Checked = True
    State = cbChecked
    TabOrder = 7
  end
  object dxTimeEdit1: TdxTimeEdit
    Left = 160
    Top = 168
    Width = 65
    TabOrder = 8
    Visible = False
    TimeEditFormat = tfHourMin
    StoredValues = 4
  end
  object DsSelez: TDataSource
    DataSet = TSelezionatori
    Left = 197
    Top = 127
  end
  object DsRisorse: TDataSource
    DataSet = TRisorse
    Left = 53
    Top = 192
  end
  object TSelezionatori: TADOLinkedTable
    Connection = Data.DB
    CursorType = ctStatic
    TableName = 'Users'
    Left = 165
    Top = 124
  end
  object TRisorse: TADOLinkedTable
    Connection = Data.DB
    CursorType = ctStatic
    TableName = 'Risorse'
    Left = 48
    Top = 128
  end
end
