object ContattoCand2Form: TContattoCand2Form
  Left = 322
  Top = 193
  Width = 286
  Height = 215
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  Caption = 'Esito contatto telefonico Candidato'
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label7: TLabel
    Left = 5
    Top = 3
    Width = 84
    Height = 13
    Caption = 'Recapiti telefonici'
    FocusControl = DBEdit2
  end
  object Label8: TLabel
    Left = 5
    Top = 42
    Width = 40
    Height = 13
    Caption = 'Cellulare'
    FocusControl = DBEdit4
  end
  object BitBtn1: TBitBtn
    Left = 184
    Top = 6
    Width = 83
    Height = 26
    TabOrder = 0
    OnClick = BitBtn1Click
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 184
    Top = 33
    Width = 83
    Height = 26
    TabOrder = 1
    OnClick = BitBtn2Click
    Kind = bkCancel
  end
  object Panel2: TPanel
    Left = 6
    Top = 88
    Width = 261
    Height = 95
    BevelInner = bvLowered
    TabOrder = 2
    object RadioGroup2: TRadioGroup
      Left = 7
      Top = 3
      Width = 109
      Height = 84
      Caption = 'Esito contatto tel.'
      Items.Strings = (
        'da richiamare'
        'deve richiamare'
        'fissato colloquio')
      TabOrder = 0
      OnClick = RadioGroup2Click
    end
    object GroupBox1: TGroupBox
      Left = 121
      Top = 3
      Width = 131
      Height = 57
      Caption = 'Agenda'
      TabOrder = 1
      object Label4: TLabel
        Left = 7
        Top = 14
        Width = 26
        Height = 13
        Caption = 'Data:'
      end
      object Label5: TLabel
        Left = 87
        Top = 14
        Width = 20
        Height = 13
        Caption = 'Ora:'
      end
      object Data: TDateTimePicker
        Left = 6
        Top = 30
        Width = 76
        Height = 21
        CalAlignment = dtaLeft
        Date = 35903.6309679398
        Time = 35903.6309679398
        DateFormat = dfShort
        DateMode = dmComboBox
        Kind = dtkDate
        ParseInput = False
      end
      object Ora: TMaskEdit
        Left = 87
        Top = 30
        Width = 37
        Height = 21
        EditMask = '!90:00;1;_'
        MaxLength = 5
        TabOrder = 1
        Text = '18.00'
      end
    end
    object DBGrid1: TDBGrid
      Left = 121
      Top = 64
      Width = 131
      Height = 22
      DataSource = DataSel_EBC.DsSelezionatori
      Options = [dgColumnResize, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Visible = False
      Columns = <
        item
          FieldName = 'Nominativo'
          Width = 105
        end>
    end
  end
  object DBEdit2: TDBEdit
    Left = 5
    Top = 19
    Width = 161
    Height = 21
    DataField = 'RecapitiTelefonici'
    DataSource = DataSel_EBC.DaAnag
    ReadOnly = True
    TabOrder = 3
  end
  object DBEdit4: TDBEdit
    Left = 5
    Top = 58
    Width = 162
    Height = 21
    DataField = 'Cellulare'
    DataSource = DataSel_EBC.DaAnag
    ReadOnly = True
    TabOrder = 4
  end
end
