�
 TVALUTAZFORMAZFORM 0�  TPF0TValutazFormazFormValutazFormazFormLeft� Top.Width�Height�Caption7Valutazione dei partecipanti all'attivit� di formazioneColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrder	PositionpoScreenCenterPixelsPerInch`
TextHeight TPanelPanel1Left Top Width�Height;AlignalTopTabOrder  TBitBtnBitBtn1Left�TopWidthYHeightTabOrder OnClickBitBtn1ClickKindbkOK  TPanelPanel3LeftTopWidth�Height9AlignalLeft
BevelOuterbvNoneEnabledTabOrder TDBEditDBEdit2LeftVTopWidthqHeightColorclYellow	DataFieldTipo
DataSourceDataFormazione.DsFormazAttivaTabOrder   TDBEditDBEdit1LeftTopWidth�HeightColorclYellow	DataFieldDescrizione
DataSourceDataFormazione.DsFormazAttivaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder  TDBEditDBEdit3LeftTopWidthKHeightColor	clBtnFace	DataFieldCodiceCorso
DataSourceDataFormazione.DsFormazAttivaTabOrder   TBitBtnBitBtn2Left3TopWidthWHeightCaptionAnnullaTabOrderKindbkCancel  TBitBtnBRiempiLeft�TopWidthLHeightCaptionRiempiFont.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderVisibleOnClickBRiempiClick   TPanelPanel2Left Top;Width�Height,AlignalClientTabOrder TStringAlignGridSG1LeftTopWidth�HeightAlignalClientOptionsgoFixedVertLinegoFixedHorzLine
goVertLine
goHorzLinegoRangeSelect	goEditing TabOrder Editable  TPanelPanel101LeftTopWidth�HeightAlignalTop	AlignmenttaLeftJustifyCaptionH  Griglia di valutazione dei partecipanti in riferimento alle competenzeColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   TTableTAnagCompIDXActive	DatabaseNameHR	IndexNameIdxAnagComp	TableNamedbo.CompetenzeAnagraficaLeft8Top5 TIntegerFieldTAnagCompIDXIDAnagrafica	FieldNameIDAnagrafica  TIntegerFieldTAnagCompIDXIDCompetenza	FieldNameIDCompetenza  TIntegerFieldTAnagCompIDXValore	FieldNameValore  TAutoIncFieldTAnagCompIDXID	FieldNameID   TTableTStoricoAnagCompActive	DatabaseNameHR	IndexNameIdxAnagComp	TableNamedbo.StoricoCompAnagLeftXTop5 TIntegerFieldTStoricoAnagCompIDAnagrafica	FieldNameIDAnagrafica  TIntegerFieldTStoricoAnagCompIDCompetenza	FieldNameIDCompetenza  TDateTimeFieldTStoricoAnagCompDallaData	FieldName	DallaData  TStringFieldTStoricoAnagCompMotivoAumento	FieldNameMotivoAumentoSize(  TIntegerFieldTStoricoAnagCompIDCorso	FieldNameIDCorso  TIntegerFieldTStoricoAnagCompValore	FieldNameValore  TSmallintFieldTStoricoAnagCompVariazPerc	FieldName
VariazPerc  TAutoIncFieldTStoricoAnagCompID	FieldNameID    