object LegendaCandUtenteForm: TLegendaCandUtenteForm
  Left = 312
  Top = 150
  BorderStyle = bsDialog
  Caption = 'Legenda e parametri'
  ClientHeight = 95
  ClientWidth = 478
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object BitBtn1: TBitBtn
    Left = 390
    Top = 5
    Width = 86
    Height = 33
    Caption = 'Chiudi'
    TabOrder = 0
    Kind = bkOK
  end
  object Panel7: TPanel
    Left = 2
    Top = 3
    Width = 382
    Height = 41
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 1
    object Label19: TLabel
      Left = 7
      Top = 8
      Width = 24
      Height = 16
      Caption = 'Col'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label20: TLabel
      Left = 38
      Top = 8
      Width = 165
      Height = 26
      Caption = 'n� di giorni dall'#39'ultimo COLLOQUIO con il soggetto'
      WordWrap = True
    end
    object Label2: TLabel
      Left = 233
      Top = 8
      Width = 87
      Height = 26
      Caption = 'carattere rosso se pi� di giorni '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      WordWrap = True
    end
    object RxSpinEdit3: TRxSpinEdit
      Left = 325
      Top = 11
      Width = 50
      Height = 21
      TabOrder = 0
    end
  end
  object Panel6: TPanel
    Left = 2
    Top = 50
    Width = 382
    Height = 41
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 2
    object Label16: TLabel
      Left = 38
      Top = 8
      Width = 179
      Height = 26
      Caption = 
        'n� di giorni dall'#39'ultimo CONTATTO con un soggetto associato alla' +
        ' ricerca'
      WordWrap = True
    end
    object Label17: TLabel
      Left = 7
      Top = 8
      Width = 20
      Height = 16
      Caption = 'Uc'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 233
      Top = 8
      Width = 76
      Height = 26
      Caption = 'carattere fucsia se pi� di giorni '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clFuchsia
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      WordWrap = True
    end
    object RxSpinEdit2: TRxSpinEdit
      Left = 325
      Top = 11
      Width = 50
      Height = 21
      TabOrder = 0
    end
  end
  object Q_OLD: TQuery
    DatabaseName = 'EBCDB'
    Left = 168
    Top = 24
  end
  object Q: TADOLinkedQuery
    Connection = Data.DB
    Parameters = <>
    Left = 123
    Top = 25
  end
end
