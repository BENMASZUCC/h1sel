//[TONI20021004]DEBUGOK
unit SceltaModello;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     Grids,DBGrids,Db,DBTables,StdCtrls,Buttons,ExtCtrls,ADODB,
     U_ADOLinkCl;

type
     TSceltaModelloForm=class(TForm)
          BitBtn1: TBitBtn;
          BitBtn2: TBitBtn;
          DsModelliWord: TDataSource;
          DBGrid1: TDBGrid;
          RGOpzione: TRadioGroup;
          CBSalva: TCheckBox;
          CBStoricizza: TCheckBox;
          BitBtn3: TBitBtn;
          QModelliWord: TADOLinkedQuery;
          procedure FormCreate(Sender: TObject);
          procedure BitBtn3Click(Sender: TObject);
          procedure FormShow(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     SceltaModelloForm: TSceltaModelloForm;

implementation

uses ModuloDati,GestModelliWord;

{$R *.DFM}

procedure TSceltaModelloForm.FormCreate(Sender: TObject);
begin
     Height:=330;
end;

procedure TSceltaModelloForm.BitBtn3Click(Sender: TObject);
begin
     GestModelliWordForm:=TGestModelliWordForm.create(self);
     GestModelliWordForm.ShowModal;
     QModelliWord.Close;
     QModelliWord.Open;
     GestModelliWordForm.Free;
end;

procedure TSceltaModelloForm.FormShow(Sender: TObject);
begin
     Caption:='[S/33] - '+Caption;
end;

end.
