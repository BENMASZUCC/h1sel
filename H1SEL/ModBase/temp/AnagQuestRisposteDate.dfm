object AnagQuestRisposteDateForm: TAnagQuestRisposteDateForm
  Left = 235
  Top = 100
  Width = 745
  Height = 511
  Caption = 'Risposte date al questionario'
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 737
    Height = 41
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object Label1: TLabel
      Left = 6
      Top = 5
      Width = 65
      Height = 13
      Caption = 'Questionario:'
    end
    object DBText1: TDBText
      Left = 78
      Top = 5
      Width = 366
      Height = 14
      DataField = 'Questionario'
      DataSource = DsQRisposteDate
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 6
      Top = 20
      Width = 48
      Height = 13
      Caption = 'Soggetto:'
    end
    object DBText2: TDBText
      Left = 78
      Top = 21
      Width = 273
      Height = 14
      DataField = 'Nominativo'
      DataSource = DsQRisposteDate
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object BitBtn1: TBitBtn
      Left = 645
      Top = 4
      Width = 87
      Height = 33
      Anchors = [akTop, akRight]
      Caption = 'Esci'
      TabOrder = 0
      Kind = bkClose
    end
  end
  object dxDBGrid1: TdxDBGrid
    Left = 0
    Top = 41
    Width = 737
    Height = 443
    Bands = <
      item
      end>
    DefaultLayout = True
    HeaderPanelRowCount = 1
    KeyField = 'ID'
    ShowGroupPanel = True
    SummaryGroups = <>
    SummarySeparator = ', '
    Align = alClient
    TabOrder = 1
    DataSource = DsQRisposteDate
    Filter.Criteria = {00000000}
    OptionsBehavior = [edgoAutoSort, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoTabThrough, edgoVertThrough]
    OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
    OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoUseBitmap]
    object dxDBGrid1ID: TdxDBGridMaskColumn
      Visible = False
      Width = 40
      BandIndex = 0
      RowIndex = 0
      FieldName = 'ID'
    end
    object dxDBGrid1Questionario: TdxDBGridMaskColumn
      Visible = False
      Width = 189
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Questionario'
    end
    object dxDBGrid1Domanda: TdxDBGridMaskColumn
      Width = 235
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Domanda'
    end
    object dxDBGrid1Risposta: TdxDBGridMaskColumn
      Width = 331
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Risposta'
    end
    object dxDBGrid1Testo: TdxDBGridMaskColumn
      Width = 146
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Testo'
    end
    object dxDBGrid1Punteggio: TdxDBGridMaskColumn
      Width = 71
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Punteggio'
    end
  end
  object DsQRisposteDate: TDataSource
    DataSet = QRisposteDate
    Left = 32
    Top = 184
  end
  object QRisposteDate: TADOQuery
    Connection = Data.DB
    Parameters = <
      item
        Name = 'x'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'select AQR.ID, Q.TipoDomanda Questionario, A.Cognome+'#39' '#39'+A.Nome ' +
        'Nominativo,'
      #9'D.TestoDomanda Domanda, R.TestoRisposta Risposta, AQR.Testo, '
      #9'R.Punteggio'
      'from QUEST_AnagRisposte AQR'
      'join QUEST_DOmande D on AQR.IDDomanda = D.ID'
      'join QUEST_Risposte R on AQR.IDRisposta = R.ID'
      'join QUEST_AnagQuest AQ on AQR.IDAnagQuest = AQ.ID'
      'join QUEST_TipiDomande Q on AQ.IDQuest = Q.ID'
      'join Anagrafica A on AQ.IDAnagrafica = A.ID'
      'left outer join Anagrafica AC on AQ.IDAnagCompilatore = AC.ID'
      'where AQR.IDAnagQuest = :x')
    Left = 32
    Top = 152
    object QRisposteDateID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QRisposteDateQuestionario: TStringField
      FieldName = 'Questionario'
      Size = 50
    end
    object QRisposteDateDomanda: TStringField
      FieldName = 'Domanda'
      Size = 1024
    end
    object QRisposteDateRisposta: TStringField
      FieldName = 'Risposta'
      Size = 1024
    end
    object QRisposteDateTesto: TStringField
      FieldName = 'Testo'
      Size = 1024
    end
    object QRisposteDatePunteggio: TFloatField
      FieldName = 'Punteggio'
    end
    object QRisposteDateNominativo: TStringField
      FieldName = 'Nominativo'
      ReadOnly = True
      Size = 61
    end
  end
end
