�
 TPROMOZIONEFORM 0�  TPF0TPromozioneFormPromozioneFormLeft� TopjBorderStylebsDialogCaption%Promozione o Retrocessione DipendenteClientHeight.ClientWidthkColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TLabelLabel1LeftTopLWidthPHeightCaptionData di efficacia:  TPanelPanel1Left Top WidthkHeightGAlignalTop
BevelOuterbvNoneTabOrder  TPanelPanel17Left Top Width� HeightGAlignalLeft
BevelOuterbvNoneEnabledTabOrder  TDBEditDBEdit12LeftTop
Width� HeightColorclYellow	DataFieldCognome
DataSourceData.DsAnagraficaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontReadOnly	TabOrder   TDBEditDBEdit14LeftTop"Width� HeightColorclYellow	DataFieldNome
DataSourceData.DsAnagraficaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontReadOnly	TabOrder   TBitBtnBitBtn1LeftTopWidthZHeightTabOrderOnClickBitBtn1ClickKindbkOK  TBitBtnBitBtn2LeftTop#WidthZHeightCaptionAnnullaTabOrderKindbkCancel   	TGroupBox	GroupBox1LeftTop� Width\Height.CaptionPosizione successivaTabOrder TSpeedButton
BPosizNextLeft8TopWidthHeightHint$Inserisci o modifica da organigramma
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 033333337�333333033333337�?��?��03   07�www7s0 �3337w333303�3337�s�s33303033337�7s3333033333337�?��?��03   07�www7s0 �3337w333303�3337�s�s33303033337�7s3333033333337��?���3   0 33www7w33�33333333333�33333s�s3333303333337s333333	NumGlyphsParentShowHintShowHint	VisibleOnClickBPosizNextClick  TEdit
EPosizNextLeftTopWidth)HeightEnabledTabOrder    	TGroupBox	GroupBox2LeftTopgWidth\HeightTCaptionBPosizione attuale (scegliere nel caso ci sia pi� di una posizione)TabOrder TDBGridDBGrid1LeftTopWidthKHeight;
DataSource
DsPosizNowOptions	dgEditingdgIndicatordgColumnResize
dgColLines
dgRowLinesdgTabsdgConfirmDeletedgCancelOnExit ReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameDescrizioneWidth� Visible	 Expanded	FieldName	DescBreveWidth_Visible	     	TGroupBox	GroupBox3LeftTop� Width\Height/CaptionAutorizzato da:TabOrder TSpeedButtonSpeedButton3Left7TopWidthHeightHintinserisci o modifica nominativo
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333333333333333333333333�3333330 333����w330  0 �337ww7w�33�� � 33�3w�w33 ��� 33ws37�w30����� 3���37�w0  ��� 7wws37�w������ s����7�w0   �� 7wwws7�w333�   333s�www3330 3333337w3333333333333333333333333333333333333333333333333333333333333333333	NumGlyphsParentShowHintShowHint	OnClickSpeedButton3Click  TDBEditDBEdit5Left� TopWidth� HeightColorclWhite	DataFieldNome
DataSourceDsAnagEnabledFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontReadOnly	TabOrder   TDBEditDBEdit6LeftTopWidth� HeightColorclWhite	DataFieldCognome
DataSourceDsAnagEnabledFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontReadOnly	TabOrder   TDateEdit97DEDataLeft^TopHWidthcHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderColorCalendar.ColorValidclBlueDayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanPopup
doIsMaskeddoShowCanceldoShowToday   TTableTAnagDatabaseNameHR	TableNamedbo.AnagraficaLeft� Top�  TAutoIncFieldTAnagID	FieldNameIDReadOnly	  TStringFieldTAnagCognome	FieldNameCognomeSize  TStringField	TAnagNome	FieldNameNomeSize   TDataSourceDsAnagDataSetTAnagLeftTop�   TTable	TPosizNowActive	DatabaseNameHR	IndexNameIdxDipendenteMasterFieldsIDMasterSourceData.DsAnagrafica	TableNamedbo.OrganigrammaLeft� Top�  TAutoIncFieldTPosizNowID	FieldNameIDReadOnly	  TStringFieldTPosizNowDescrizione	FieldNameDescrizioneSize<  TStringFieldTPosizNowDescBreve	FieldName	DescBreveSize
  TIntegerFieldTPosizNowNodoPadre	FieldName	NodoPadre  TIntegerFieldTPosizNowIDDipendente	FieldNameIDDipendente   TDataSource
DsPosizNowDataSet	TPosizNowLeft� Top�   TTableTOrgABSActive	DatabaseNameHR	TableNamedbo.OrganigrammaLeft� Top�  TAutoIncField	TOrgABSID	FieldNameIDReadOnly	  TIntegerFieldTOrgABSIDDipendente	FieldNameIDDipendente    