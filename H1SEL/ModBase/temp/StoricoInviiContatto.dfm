object StoricoInviiContattoForm: TStoricoInviiContattoForm
  Left = 236
  Top = 106
  Width = 613
  Height = 447
  Caption = 'Storico invii per il contatto'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 605
    Height = 45
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object BitBtn1: TBitBtn
      Left = 502
      Top = 4
      Width = 99
      Height = 37
      Anchors = [akTop, akRight]
      Caption = 'Esci'
      TabOrder = 0
      Kind = bkOK
    end
  end
  object dxDBGrid1: TdxDBGrid
    Left = 0
    Top = 45
    Width = 605
    Height = 375
    Bands = <
      item
      end>
    DefaultLayout = True
    HeaderPanelRowCount = 1
    KeyField = 'ID'
    SummaryGroups = <>
    SummarySeparator = ', '
    Align = alClient
    TabOrder = 1
    DataSource = DsQStoricoInvii
    Filter.Criteria = {00000000}
    LookAndFeel = lfFlat
    OptionsBehavior = [edgoAutoSort, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoTabThrough, edgoVertThrough]
    OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
    OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoUseBitmap]
    object dxDBGrid1Data: TdxDBGridDateColumn
      Sorted = csDown
      Width = 171
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Data'
    end
    object dxDBGrid1Abitazione: TdxDBGridCheckColumn
      Visible = False
      Width = 105
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Abitazione'
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object dxDBGrid1Tipo: TdxDBGridMaskColumn
      Width = 42
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Tipo'
    end
    object dxDBGrid1TipoInvio: TdxDBGridMaskColumn
      Caption = 'Tipo (tabella)'
      Visible = False
      Width = 224
      BandIndex = 0
      RowIndex = 0
      FieldName = 'TipoInvio'
    end
    object dxDBGrid1Note: TdxDBGridMaskColumn
      Width = 390
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Note'
    end
  end
  object QStoricoInvii: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select StoricoInviiContatto.*, TipiInviiContatti.Tipo TipoInvio'
      'from StoricoInviiContatto, TipiInviiContatti'
      'where StoricoInviiContatto.IDTipoInvio *= TipiInviiContatti.ID')
    Left = 32
    Top = 96
    object QStoricoInviiID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QStoricoInviiIDContattoCli: TIntegerField
      FieldName = 'IDContattoCli'
    end
    object QStoricoInviiIDTipoInvio: TIntegerField
      FieldName = 'IDTipoInvio'
    end
    object QStoricoInviiData: TDateTimeField
      FieldName = 'Data'
    end
    object QStoricoInviiAbitazione: TBooleanField
      FieldName = 'Abitazione'
    end
    object QStoricoInviiNote: TStringField
      FieldName = 'Note'
      Size = 100
    end
    object QStoricoInviiTipo: TStringField
      FieldName = 'Tipo'
      Size = 1
    end
    object QStoricoInviiTipoInvio: TStringField
      FieldName = 'TipoInvio'
      Size = 30
    end
  end
  object DsQStoricoInvii: TDataSource
    DataSet = QStoricoInvii
    Left = 32
    Top = 128
  end
end
