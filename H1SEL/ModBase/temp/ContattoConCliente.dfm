object ContattoConClienteForm: TContattoConClienteForm
  Left = 286
  Top = 106
  BorderStyle = bsDialog
  Caption = 'Contatto con il cliente'
  ClientHeight = 169
  ClientWidth = 304
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object BitBtn1: TBitBtn
    Left = 213
    Top = 2
    Width = 87
    Height = 31
    TabOrder = 0
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 213
    Top = 34
    Width = 87
    Height = 31
    Caption = 'Annulla'
    TabOrder = 1
    Kind = bkCancel
  end
  object Panel1: TPanel
    Left = 4
    Top = 2
    Width = 203
    Height = 57
    BevelOuter = bvLowered
    Enabled = False
    TabOrder = 2
    object DBEdit1: TDBEdit
      Left = 5
      Top = 6
      Width = 193
      Height = 21
      Color = clYellow
      DataField = 'Cognome'
      DataSource = DataRicerche.DsQCandRic
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object DBEdit2: TDBEdit
      Left = 5
      Top = 30
      Width = 193
      Height = 21
      Color = clYellow
      DataField = 'Nome'
      DataSource = DataRicerche.DsQCandRic
      TabOrder = 1
    end
  end
  object RGTipoFeedback: TRadioGroup
    Left = 4
    Top = 69
    Width = 204
    Height = 95
    Caption = 'Tipo feedback'
    ItemIndex = 0
    Items.Strings = (
      'feedback positivo'
      'feedback negativo'
      'feedback stand-by'
      'offerta')
    TabOrder = 3
  end
end
