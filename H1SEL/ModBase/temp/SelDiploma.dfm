�
 TSELDIPLOMAFORM 0`  TPF0TSelDiplomaFormSelDiplomaFormLeftgTop� BorderStylebsDialogCaptionSelezione titolo di studioClientHeight-ClientWidth-Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrder	PositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TLabelLabel2LeftTop*Width� HeightCaption,Titoli di studio della tipologia selezionata  TSpeedButtonBITALeftTopWidth%Height
GroupIndexDown	CaptionITAFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontVisibleOnClick	BITAClick  TSpeedButtonBENGLeft)TopWidth%Height
GroupIndexCaptionENGFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontVisibleOnClick	BENGClick  TBitBtnBitBtn1LeftzTopWidthVHeight"TabOrder KindbkOK  TBitBtnBitBtn2Left�TopWidthVHeight"CaptionAnnullaTabOrderKindbkCancel  	TdxDBGrid	dxDBGrid1Left Top8Width-Height� Bands  DefaultLayout	HeaderPanelRowCountKeyFieldIDShowGroupPanel	SummaryGroups SummarySeparator, AlignalBottomTabOrder
DataSource	DsDiplomiFilter.Active	Filter.CaseInsensitive	Filter.Criteria
       OptionsBehavioredgoAutoSortedgoDragScrolledgoEnterShowEditoredgoImmediateEditoredgoTabThroughedgoVertThrough 	OptionsDBedgoCancelOnExitedgoCanDeleteedgoCanInsertedgoCanNavigationedgoConfirmDeleteedgoLoadAllRecordsedgoUseBookmarks OptionsViewedgoBandHeaderWidthedgoIndicatoredgoUseBitmap  TdxDBGridMaskColumndxDBGrid1IDVisibleWidth!	BandIndex RowIndex 	FieldNameID  TdxDBGridMaskColumndxDBGrid1IDAreaVisibleWidth!	BandIndex RowIndex 	FieldNameIDArea  TdxDBGridMaskColumndxDBGrid1TipoWidthO	BandIndex RowIndex 	FieldNameTipo  TdxDBGridLookupColumndxDBGrid1AreaSortedcsUpWidthn	BandIndex RowIndex 	FieldNameArea  TdxDBGridMaskColumndxDBGrid1DescrizioneWidth� 	BandIndex RowIndex 	FieldNameDescrizione  TdxDBGridMaskColumndxDBGrid1PunteggioMaxVisibleWidth&	BandIndex RowIndex 	FieldNamePunteggioMax  TdxDBGridMaskColumndxDBGrid1IDAziendaVisibleWidth!	BandIndex RowIndex 	FieldName	IDAzienda  TdxDBGridMaskColumndxDBGrid1Tipo_ENGVisibleWidthD	BandIndex RowIndex 	FieldNameTipo_ENG  TdxDBGridMaskColumndxDBGrid1Descrizione_ENGVisibleWidth� 	BandIndex RowIndex 	FieldNameDescrizione_ENG  TdxDBGridMaskColumndxDBGrid1IDGruppoVisibleWidth!	BandIndex RowIndex 	FieldNameIDGruppo  TdxDBGridLookupColumndxDBGrid1GruppoWidthX	BandIndex RowIndex 	FieldNameGruppo   TDataSource	DsDiplomiDataSetDiplomiLeft�Top�   TADOLinkedQueryDiplomiActive	
ConnectionData.DB
CursorTypectStaticFiltered	
Parameters SQL.Stringsselect *from diplomi  LinkedMasterFieldTipoLinkedDetailFieldTipo	UseFilterLeft�Top�  TAutoIncField	DiplomiID	FieldNameIDReadOnly	  TIntegerFieldDiplomiIDArea	FieldNameIDArea  TStringFieldDiplomiTipo	FieldNameTipo  TStringFieldDiplomiDescrizione	FieldNameDescrizioneSize<  TSmallintFieldDiplomiPunteggioMax	FieldNamePunteggioMax  TStringFieldDiplomiIDAzienda	FieldName	IDAzienda	FixedChar	Size
  TStringFieldDiplomiTipo_ENG	FieldNameTipo_ENG  TStringFieldDiplomiDescrizione_ENG	FieldNameDescrizione_ENGSize<  TIntegerFieldDiplomiIDGruppo	FieldNameIDGruppo  TStringFieldDiplomiGruppo	FieldKindfkLookup	FieldNameGruppoLookupDataSetQGruppiDiplomiLKLookupKeyFieldsIDLookupResultFieldGruppo	KeyFieldsIDGruppoSize2Lookup	  TStringFieldDiplomiArea	FieldKindfkLookup	FieldNameAreaLookupDataSetQareeDiplomiLKLookupKeyFieldsIDLookupResultFieldArea	KeyFieldsIDAreaSizeLookup	   	TADOQueryQGruppiDiplomiLKActive	
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect *from gruppodiplomi Left�Top�  TAutoIncFieldQGruppiDiplomiLKID	FieldNameIDReadOnly	  TStringFieldQGruppiDiplomiLKGruppo	FieldNameGruppoSize2   	TADOQueryQareeDiplomiLKActive	
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect *from areediplomi Left�Top�  TAutoIncFieldQareeDiplomiLKID	FieldNameIDReadOnly	  TStringFieldQareeDiplomiLKArea	FieldNameAreaSize  TIntegerFieldQareeDiplomiLKIdAzienda	FieldName	IdAzienda    