�
 TCONTATTOCLIFATTOFORM 0�  TPF0TContattoCliFattoFormContattoCliFattoFormLeft|Top� ActiveControlDEDataBorderStylebsDialogCaptionContatto con il clienteClientHeightClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnClose	FormCloseOnShowFormShowPixelsPerInch`
TextHeight TLabelLabel1LeftTopeWidthHeightCaptionData  TLabelLabel2LeftTop|WidthHeightCaptionOre  TLabelLabel3LeftTop� Width?HeightCaptionTipo contatto  TLabelLabel4LeftTop� Width5HeightCaptionparlato con  TLabelLabel5LeftTop%Width:HeightCaptionDescrizione   TSpeedButtonSpeedButton1Left&Top� WidthwHeightHint%copia nominativo dai contatti clienteCaptioninserisci da elenco
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333333333333333333333333�3333330 333����w330  0 �337ww7w�33�� � 33�3w�w33 ��� 33ws37�w30����� 3���37�w0  ��� 7wws37�w������ s����7�w0   �� 7wwws7�w333�   333s�www3330 3333337w3333333333333333333333333333333333333333333333333333333333333333333	NumGlyphsParentShowHintShowHint	OnClickSpeedButton1Click  TLabelLabel6LeftTopNWidth#HeightCaptionUtente:  TLabelLabel7Left� TopeWidthWHeightCaptionProssimo contatto:  TBitBtnBitBtn1LeftHTopWidthVHeightTabOrderKindbkOK  TBitBtnBitBtn2LeftHTop$WidthVHeightCaptionAnnullaTabOrderKindbkCancel  TPanelPanel1LeftTopWidth>Height=
BevelOuter	bvLoweredEnabledTabOrderTabStop	 TDBEditDBEdit1LeftTopWidth1HeightColorclAqua	DataFieldDescrizione
DataSourceData2.dsEBCClientiFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder   TDBEditDBEdit2LeftTopWidth� Height	DataFieldComune
DataSourceData2.dsEBCClientiTabOrder  TDBEditDBEdit3Left� TopWidthHeight	DataField	Provincia
DataSourceData2.dsEBCClientiTabOrder   TDateEdit97DEDataLeftXTopaWidth^HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder ColorCalendar.ColorValidclBlueDayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanPopup
doIsMaskeddoShowCanceldoShowToday   	TMaskEditMEOreLeftXTopxWidth&HeightEditMask
!90:00;1;_	MaxLengthTabOrderText  .    	TComboBoxCBTipoContattoLeftXTop� Width^Height
ItemHeightTabOrderTexttelefonoItems.Stringstelefonofaxe-maildiretto   TEditEParlatoConLeftXTop� Width� HeightTabOrder  TDBGridDBGrid1LeftXTop� WidthDHeighta
DataSourceData2.DsContattiClientiReadOnly	TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameContattoWidth� Visible	 Expanded	FieldNameTelefonoWidth� Visible	    	TCheckBoxCBRifOffertaLeftTop�Width� HeightCaptionRiferimento a offertaTabOrder	OnClickCBRifOffertaClick  TDBGridDBGrid2LeftXTop�WidthCHeight]Color	clBtnFace
DataSourceData2.DsCliOfferteReadOnly	TabOrder
TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameRifWidth4Visible	 Expanded	FieldNameDataWidthGVisible	 Expanded	FieldNameAttenzioneDiTitle.Captionc.a.Width� Visible	    TDBGridDBGrid3LeftXTopHWidth{Height
DataSourceDsQUsersOptions	dgEditingdgColumnResizedgTabsdgConfirmDeletedgCancelOnExit TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldName
NominativoWidthdVisible	    TMemoEDescrizioneLeftTop7Width�HeightO
ScrollBars
ssVerticalTabOrder  TDateEdit97DEDataProssContLeft)TopaWidth^HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderColorCalendar.ColorValidclBlueDayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanPopup
doIsMaskeddoShowCanceldoShowToday   TADOLinkedQueryQUsers
ConnectionData.DB
CursorTypectStatic
ParametersNamexoggi:
Attributes
paNullable DataType
ftDateTime	PrecisionSizeValue  Namexoggi:
Attributes
paNullable DataType
ftDateTime	PrecisionSizeValue   SQL.Strings!select ID,Nominativo,Descrizione from Users ?where not ((DataScadenza is not null and DataScadenza<:xoggi: )3or (DataRevoca is not null and DataRevoca<:xoggi:) $or DataCreazione is null or Tipo=0) order by Nominativo  	UseFilterLeftTop( TAutoIncFieldQUsersID	FieldNameIDReadOnly	  TStringFieldQUsersNominativo	FieldName
NominativoSize  TStringFieldQUsersDescrizione	FieldNameDescrizioneSize2   TDataSourceDsQUsersDataSetQUsersLeft(Top(   