object OfferteForm: TOfferteForm
  Left = 388
  Top = 95
  Width = 892
  Height = 540
  Caption = 'Visualizzazione offerte ai clienti'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel4: TPanel
    Left = 0
    Top = 0
    Width = 884
    Height = 39
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object ToolbarButton9723: TToolbarButton97
      Left = 2
      Top = 1
      Width = 86
      Height = 37
      Caption = 'Dettaglio offerta'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
        555555FFFFFFFFFF5F5557777777777505555777777777757F55555555555555
        055555555555FF5575F555555550055030555555555775F7F7F55555550FB000
        005555555575577777F5555550FB0BF0F05555555755755757F555550FBFBF0F
        B05555557F55557557F555550BFBF0FB005555557F55575577F555500FBFBFB0
        305555577F555557F7F5550E0BFBFB003055557575F55577F7F550EEE0BFB0B0
        305557FF575F5757F7F5000EEE0BFBF03055777FF575FFF7F7F50000EEE00000
        30557777FF577777F7F500000E05555BB05577777F75555777F5500000555550
        3055577777555557F7F555000555555999555577755555577755}
      NumGlyphs = 2
      Opaque = False
      WordWrap = True
      OnClick = ToolbarButton9723Click
    end
    object ToolbarButton971: TToolbarButton97
      Left = 104
      Top = 1
      Width = 102
      Height = 37
      DropdownMenu = PMStampa
      Caption = 'Stampa/esporta'
    end
    object BitBtn1: TBitBtn
      Left = 788
      Top = 4
      Width = 93
      Height = 32
      Anchors = [akTop, akRight]
      Caption = 'Esci'
      TabOrder = 0
      OnClick = BitBtn1Click
      Kind = bkOK
    end
  end
  object dxDBGrid1: TdxDBGrid
    Left = 0
    Top = 39
    Width = 884
    Height = 474
    Bands = <
      item
      end>
    DefaultLayout = True
    HeaderPanelRowCount = 1
    KeyField = 'ID'
    ShowGroupPanel = True
    ShowSummaryFooter = True
    SummaryGroups = <
      item
        DefaultGroup = False
        SummaryItems = <
          item
            ColumnName = 'dxDBGrid1Stato'
            SummaryField = 'TotaleCalc'
            SummaryFormat = '#,###'
            SummaryType = cstSum
          end
          item
            ColumnName = 'dxDBGrid1Stato'
            SummaryField = 'SoloAccettati'
            SummaryFormat = '#,###'
            SummaryType = cstSum
          end>
        Name = 'Default'
      end>
    SummarySeparator = ', '
    Align = alClient
    TabOrder = 1
    OnMouseUp = dxDBGrid1MouseUp
    DataSource = DsQOfferte
    Filter.Active = True
    Filter.Criteria = {00000000}
    LookAndFeel = lfFlat
    OptionsBehavior = [edgoAutoSort, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoStoreToRegistry, edgoTabThrough, edgoVertThrough]
    OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
    OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoRowSelect, edgoUseBitmap]
    RegistryPath = '\Software\H1\DBGOfferte'
    OnCustomDrawCell = dxDBGrid1CustomDrawCell
    OnCustomDrawFooter = dxDBGrid1CustomDrawFooter
    object dxDBGrid1ID: TdxDBGridMaskColumn
      Alignment = taCenter
      Caption = 'Rif'
      Color = clBtnFace
      Visible = False
      Width = 59
      BandIndex = 0
      RowIndex = 0
      FieldName = 'ID'
      DisableFilter = True
    end
    object dxDBGrid1Rif: TdxDBGridMaskColumn
      Color = clBtnFace
      Width = 20
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Rif'
      DisableFilter = True
    end
    object dxDBGrid1Cliente: TdxDBGridMaskColumn
      Sorted = csUp
      Width = 131
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Cliente'
      SummaryType = cstCount
      SummaryField = 'ID'
      SummaryFormat = '  Totale offerte = #,###'
      SummaryGroupName = 'Default'
    end
    object dxDBGrid1Data: TdxDBGridDateColumn
      Width = 81
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Data'
    end
    object dxDBGrid1Tipo: TdxDBGridMaskColumn
      Width = 42
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Tipo'
      SummaryType = cstCount
      SummaryField = 'ID'
      SummaryFormat = '  Totale offerte = #,###'
      SummaryGroupName = 'Default'
    end
    object dxDBGrid1IDCliente: TdxDBGridMaskColumn
      Visible = False
      Width = 59
      BandIndex = 0
      RowIndex = 0
      FieldName = 'IDCliente'
    end
    object dxDBGrid1AMezzo: TdxDBGridMaskColumn
      Caption = 'a mezzo'
      Width = 66
      BandIndex = 0
      RowIndex = 0
      FieldName = 'AMezzo'
      SummaryType = cstCount
      SummaryField = 'ID'
      SummaryFormat = '  Totale offerte = #,###'
    end
    object dxDBGrid1AttenzioneDi: TdxDBGridMaskColumn
      Caption = 'all'#39'attenzione di'
      Width = 93
      BandIndex = 0
      RowIndex = 0
      FieldName = 'AttenzioneDi'
    end
    object dxDBGrid1IDContratto: TdxDBGridMaskColumn
      Visible = False
      Width = 59
      BandIndex = 0
      RowIndex = 0
      FieldName = 'IDContratto'
    end
    object dxDBGrid1Stato: TdxDBGridMaskColumn
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clPurple
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Width = 37
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Stato'
      SummaryType = cstCount
      SummaryField = 'ID'
      SummaryFormat = '  Totale offerte = #,###'
      SummaryGroupName = 'Default'
    end
    object dxDBGrid1Condizioni: TdxDBGridMaskColumn
      Visible = False
      Width = 110
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Condizioni'
    end
    object dxDBGrid1ImportoTotale: TdxDBGridCurrencyColumn
      Caption = 'Imp.Totale'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      Width = 44
      BandIndex = 0
      RowIndex = 0
      FieldName = 'TotaleCalc'
      SummaryFooterType = cstSum
      SummaryFooterField = 'TotaleCalc'
      SummaryFooterFormat = ',0.00'
      DisplayFormat = ',0.00'
      Nullable = False
      SummaryType = cstSum
    end
    object dxDBGrid1Esito: TdxDBGridMaskColumn
      Visible = False
      Width = 59
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Esito'
    end
    object dxDBGrid1Note: TdxDBGridMaskColumn
      Visible = False
      Width = 175
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Note'
    end
    object dxDBGrid1IDUtente: TdxDBGridMaskColumn
      Visible = False
      Width = 59
      BandIndex = 0
      RowIndex = 0
      FieldName = 'IDUtente'
    end
    object dxDBGrid1IDRicerca: TdxDBGridMaskColumn
      Visible = False
      Width = 59
      BandIndex = 0
      RowIndex = 0
      FieldName = 'IDRicerca'
    end
    object dxDBGrid1LineaProdotto: TdxDBGridColumn
      Caption = 'Linea di Prodotto'
      Visible = False
      Width = 85
      BandIndex = 0
      RowIndex = 0
      FieldName = 'LineaProdotto'
    end
    object dxDBGrid1Area: TdxDBGridColumn
      Caption = 'Area/Filone prof.'
      Visible = False
      Width = 59
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Area'
    end
    object dxDBGrid1SoloAccettati: TdxDBGridCurrencyColumn
      Caption = 'Importi accettati'
      Width = 59
      BandIndex = 0
      RowIndex = 0
      FieldName = 'SoloAccettati'
      SummaryFooterType = cstSum
      SummaryFooterField = 'SoloAccettati'
      SummaryFooterFormat = ',0.00'
      DisplayFormat = ',0.00'
      Nullable = False
      SummaryType = cstSum
    end
    object dxDBGrid1Column20: TdxDBGridColumn
      Caption = 'Sede Operativa'
      Width = 120
      BandIndex = 0
      RowIndex = 0
      FieldName = 'SedeOperativa'
    end
    object dxDBGrid1ColumnComune: TdxDBGridColumn
      Width = 132
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Comune'
    end
    object dxDBGrid1ColumnProvincia: TdxDBGridColumn
      Width = 57
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Provincia'
    end
    object dxDBGrid1ConoscTramite: TdxDBGridColumn
      Caption = 'Conosc.tramite'
      Width = 100
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Tramite'
    end
    object dxDBGrid1Column24: TdxDBGridColumn
      Caption = 'Data acc.'
      Width = 80
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Data_acc'
    end
    object dxDBGrid1Column25: TdxDBGridCheckColumn
      Caption = 'Prima vend.'
      Width = 50
      BandIndex = 0
      RowIndex = 0
      FieldName = 'PrimaVendita'
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object dxDBGrid1Column26: TdxDBGridCheckColumn
      Caption = 'UpSell.'
      Width = 50
      BandIndex = 0
      RowIndex = 0
      FieldName = 'UpSelling'
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
  end
  object QOfferte_OLD: TQuery
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      
        'select EBC_Offerte.*,EBC_Clienti.Descrizione Cliente, LineaProdo' +
        'tto, '
      '          Aree.Descrizione Area'
      'from EBC_Offerte,EBC_Clienti,CG_LineeProdotto, Aree'
      'where EBC_Offerte.IDCliente=EBC_Clienti.ID'
      '    and EBC_Offerte.IDLineaProdotto *= CG_LineeProdotto.ID'
      '    and EBC_Offerte.IDArea *= Aree.ID'
      '')
    Left = 144
    Top = 224
    object QOfferte_OLDID: TAutoIncField
      FieldName = 'ID'
    end
    object QOfferte_OLDIDCliente: TIntegerField
      FieldName = 'IDCliente'
    end
    object QOfferte_OLDRif: TStringField
      FieldName = 'Rif'
      FixedChar = True
      Size = 10
    end
    object QOfferte_OLDData: TDateTimeField
      FieldName = 'Data'
    end
    object QOfferte_OLDAMezzo: TStringField
      FieldName = 'AMezzo'
      FixedChar = True
      Size = 15
    end
    object QOfferte_OLDAttenzioneDi: TStringField
      FieldName = 'AttenzioneDi'
      FixedChar = True
      Size = 40
    end
    object QOfferte_OLDAnticipoRichiesto: TFloatField
      FieldName = 'AnticipoRichiesto'
      DisplayFormat = '#,###'
    end
    object QOfferte_OLDCondizioni: TStringField
      FieldName = 'Condizioni'
      FixedChar = True
      Size = 50
    end
    object QOfferte_OLDEsito: TStringField
      FieldName = 'Esito'
      FixedChar = True
    end
    object QOfferte_OLDNote: TStringField
      FieldName = 'Note'
      FixedChar = True
      Size = 80
    end
    object QOfferte_OLDStato: TStringField
      FieldName = 'Stato'
      FixedChar = True
    end
    object QOfferte_OLDTipo: TStringField
      FieldName = 'Tipo'
      FixedChar = True
      Size = 30
    end
    object QOfferte_OLDIDUtente: TIntegerField
      FieldName = 'IDUtente'
    end
    object QOfferte_OLDImportoTotale: TFloatField
      FieldName = 'ImportoTotale'
      DisplayFormat = '#,###.##'
    end
    object QOfferte_OLDCliente: TStringField
      FieldName = 'Cliente'
      FixedChar = True
      Size = 30
    end
    object QOfferte_OLDIDRicerca: TIntegerField
      FieldName = 'IDRicerca'
    end
    object QOfferte_OLDIDContratto: TIntegerField
      FieldName = 'IDContratto'
    end
    object QOfferte_OLDIDLineaProdotto: TIntegerField
      FieldName = 'IDLineaProdotto'
    end
    object QOfferte_OLDLineaProdotto: TStringField
      FieldName = 'LineaProdotto'
      FixedChar = True
      Size = 50
    end
    object QOfferte_OLDIDArea: TIntegerField
      FieldName = 'IDArea'
    end
    object QOfferte_OLDArea: TStringField
      FieldName = 'Area'
      FixedChar = True
      Size = 30
    end
  end
  object DsQOfferte: TDataSource
    DataSet = QOfferte
    Left = 144
    Top = 256
  end
  object PMStampa: TPopupMenu
    Images = MainForm.ImageList3
    Left = 144
    Top = 152
    object stampagriglia1: TMenuItem
      Caption = 'stampa griglia'
      ImageIndex = 18
      OnClick = stampagriglia1Click
    end
    object esportainExcel1: TMenuItem
      Caption = 'esporta in Excel'
      ImageIndex = 19
      OnClick = esportainExcel1Click
    end
    object esportainHTML1: TMenuItem
      Caption = 'esporta in HTML'
      ImageIndex = 20
      OnClick = esportainHTML1Click
    end
  end
  object dxPrinter1: TdxComponentPrinter
    CurrentLink = dxPrinter1Link1
    Version = 0
    Left = 216
    Top = 176
    object dxPrinter1Link1: TdxDBGridReportLink
      Caption = 'dxPrinter1Link1'
      Component = dxDBGrid1
      DesignerHelpContext = 0
      PrinterPage.Background.Brush.Style = bsClear
      PrinterPage.Footer = 6350
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 25400
      PrinterPage.Margins.Left = 25400
      PrinterPage.Margins.Right = 25400
      PrinterPage.Margins.Top = 10000
      PrinterPage.PageFooter.Font.Charset = DEFAULT_CHARSET
      PrinterPage.PageFooter.Font.Color = clWindowText
      PrinterPage.PageFooter.Font.Height = -11
      PrinterPage.PageFooter.Font.Name = 'Tahoma'
      PrinterPage.PageFooter.Font.Style = []
      PrinterPage.PageHeader.Font.Charset = DEFAULT_CHARSET
      PrinterPage.PageHeader.Font.Color = clWindowText
      PrinterPage.PageHeader.Font.Height = -11
      PrinterPage.PageHeader.Font.Name = 'Tahoma'
      PrinterPage.PageHeader.Font.Style = []
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportTitle.Font.Charset = DEFAULT_CHARSET
      ReportTitle.Font.Color = clWindowText
      ReportTitle.Font.Height = -19
      ReportTitle.Font.Name = 'Times New Roman'
      ReportTitle.Font.Style = [fsBold]
      BandColor = clBtnFace
      BandFont.Charset = DEFAULT_CHARSET
      BandFont.Color = clWindowText
      BandFont.Height = -11
      BandFont.Name = 'MS Sans Serif'
      BandFont.Style = []
      Color = clWindow
      EvenFont.Charset = DEFAULT_CHARSET
      EvenFont.Color = clWindowText
      EvenFont.Height = -11
      EvenFont.Name = 'Times New Roman'
      EvenFont.Style = []
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      FooterFont.Charset = DEFAULT_CHARSET
      FooterFont.Color = clWindowText
      FooterFont.Height = -11
      FooterFont.Name = 'MS Sans Serif'
      FooterFont.Style = []
      GridLineColor = clBtnFace
      GroupNodeFont.Charset = DEFAULT_CHARSET
      GroupNodeFont.Color = clWindowText
      GroupNodeFont.Height = -11
      GroupNodeFont.Name = 'Times New Roman'
      GroupNodeFont.Style = []
      GroupNodeColor = clBtnFace
      HeaderColor = clBtnFace
      HeaderFont.Charset = DEFAULT_CHARSET
      HeaderFont.Color = clWindowText
      HeaderFont.Height = -11
      HeaderFont.Name = 'MS Sans Serif'
      HeaderFont.Style = []
      OddColor = clWindow
      OddFont.Charset = DEFAULT_CHARSET
      OddFont.Color = clWindowText
      OddFont.Height = -11
      OddFont.Name = 'Times New Roman'
      OddFont.Style = []
      Options = [tlpoBands, tlpoHeaders, tlpoFooters, tlpoRowFooters, tlpoPreview, tlpoPreviewGrid, tlpoGrid, tlpoFlatCheckMarks, tlpoImages, tlpoStateImages]
      PreviewFont.Charset = DEFAULT_CHARSET
      PreviewFont.Color = clBlue
      PreviewFont.Height = -11
      PreviewFont.Name = 'MS Sans Serif'
      PreviewFont.Style = []
      RowFooterColor = cl3DLight
      RowFooterFont.Charset = DEFAULT_CHARSET
      RowFooterFont.Color = clWindowText
      RowFooterFont.Height = -11
      RowFooterFont.Name = 'MS Sans Serif'
      RowFooterFont.Style = []
      BuiltInReportLink = True
    end
  end
  object QOfferte: TADOLinkedQuery
    Connection = Data.DB
    CursorType = ctStatic
    OnCalcFields = QOfferteCalcFields
    Parameters = <>
    SQL.Strings = (
      
        'select Data_acc, EBC_Offerte.*,EBC_Clienti.Descrizione Cliente, ' +
        'LineaProdotto, '
      
        '          Aree.Descrizione Area, Aziende.descrizione SedeOperati' +
        'va,('
      #9'select sum(Importo)'
      #9'from ebc_offertadettaglio'
      #9'where accettata = 1'
      #9'and ebc_offertadettaglio.idofferta=EBC_Offerte.ID'
      #9'group by idofferta'
      
        #9') as SoloAccettati, EBC_Clienti.Comune, EBC_Clienti.Provincia, ' +
        'TabConoscTramite.Tramite'
      'from EBC_Offerte'
      'join EBC_Clienti on EBC_Offerte.IDCliente = EBC_Clienti.ID'
      
        'left outer join CG_LineeProdotto on EBC_Offerte.IDLineaProdotto ' +
        '= CG_LineeProdotto.ID'
      'left outer join Aree on EBC_Offerte.IDArea = Aree.ID'
      
        'left outer join Aziende on EBC_offerte.IDSedeOperativa = aziende' +
        '.id'
      
        'left outer join TabConoscTramite on EBC_Clienti.IDConoscTramite ' +
        '= TabConoscTramite.ID'
      'left outer join ('
      #9'select IDOfferta, max(DallaData) Data_acc'
      #9'from EBC_OfferteStorico '
      #9'where Stato = '#39'accettata'#39
      #9'group by IDOfferta'
      ') Off_Storico on EBC_Offerte.ID = Off_Storico.IDOfferta'
      'order by EBC_Offerte.ID desc')
    UseFilter = False
    Left = 144
    Top = 288
    object QOfferteID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QOfferteIDCliente: TIntegerField
      FieldName = 'IDCliente'
    end
    object QOfferteRif: TStringField
      FieldName = 'Rif'
      Size = 10
    end
    object QOfferteData: TDateTimeField
      FieldName = 'Data'
    end
    object QOfferteAMezzo: TStringField
      FieldName = 'AMezzo'
      Size = 15
    end
    object QOfferteAttenzioneDi: TStringField
      FieldName = 'AttenzioneDi'
      Size = 40
    end
    object QOfferteAnticipoRichiesto: TFloatField
      FieldName = 'AnticipoRichiesto'
    end
    object QOfferteCondizioni: TStringField
      FieldName = 'Condizioni'
      Size = 50
    end
    object QOfferteEsito: TStringField
      FieldName = 'Esito'
    end
    object QOfferteNote: TStringField
      FieldName = 'Note'
      Size = 80
    end
    object QOfferteStato: TStringField
      FieldName = 'Stato'
    end
    object QOfferteTipo: TStringField
      FieldName = 'Tipo'
      Size = 30
    end
    object QOfferteIDUtente: TIntegerField
      FieldName = 'IDUtente'
    end
    object QOfferteImportoTotale: TFloatField
      FieldName = 'ImportoTotale'
      DisplayFormat = '#,###.00'
    end
    object QOfferteIDRicerca: TIntegerField
      FieldName = 'IDRicerca'
    end
    object QOfferteIDContratto: TIntegerField
      FieldName = 'IDContratto'
    end
    object QOfferteIDContrattoCG: TIntegerField
      FieldName = 'IDContrattoCG'
    end
    object QOfferteIDArea: TIntegerField
      FieldName = 'IDArea'
    end
    object QOfferteIDLineaProdotto: TIntegerField
      FieldName = 'IDLineaProdotto'
    end
    object QOfferteCliente: TStringField
      FieldName = 'Cliente'
      Size = 50
    end
    object QOfferteLineaProdotto: TStringField
      FieldName = 'LineaProdotto'
      Size = 50
    end
    object QOfferteArea: TStringField
      FieldName = 'Area'
      Size = 30
    end
    object QOfferteSoloAccettati: TFloatField
      FieldName = 'SoloAccettati'
      ReadOnly = True
    end
    object QOfferteSedeOperativa: TStringField
      FieldName = 'SedeOperativa'
      FixedChar = True
      Size = 30
    end
    object QOfferteComune: TStringField
      FieldName = 'Comune'
      Size = 100
    end
    object QOfferteProvincia: TStringField
      FieldName = 'Provincia'
      Size = 2
    end
    object QOfferteTramite: TStringField
      FieldName = 'Tramite'
      Size = 50
    end
    object QOfferteData_acc: TDateTimeField
      FieldName = 'Data_acc'
      ReadOnly = True
    end
    object QOffertePrimaVendita: TBooleanField
      FieldName = 'PrimaVendita'
    end
    object QOfferteUpSelling: TBooleanField
      FieldName = 'UpSelling'
    end
    object QOfferteTotaleCalc: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TotaleCalc'
      Calculated = True
    end
    object QOfferteTotProdServ: TFloatField
      FieldName = 'TotProdServ'
    end
  end
  object QUsers: TADOLinkedQuery
    Connection = Data.DB
    Parameters = <>
    SQL.Strings = (
      'select ID,Nominativo,Descrizione from Users ')
    UseFilter = False
    Left = 96
    Top = 240
    object QUsersID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QUsersNominativo: TStringField
      FieldName = 'Nominativo'
      Size = 30
    end
  end
end
