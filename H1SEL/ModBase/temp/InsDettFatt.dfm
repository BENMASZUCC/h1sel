�
 TINSDETTFATTFORM 0NY  TPF0TInsDettFattFormInsDettFattFormLeft� TopvBorderStylebsDialogCaptionInserimento dettaglio fatturaClientHeight ClientWidthxColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TBitBtnBitBtn1LeftTopWidth`Height!CaptionEsciTabOrder KindbkOK  TPanelPanel3LeftTopWidth6Height+
BevelOuter	bvLoweredEnabledTabOrder TLabelLabel1LeftTopWidth HeightCaptionClienteFocusControlDBEdit1  TDBEditDBEdit1LeftTopWidth,HeightColorclAqua	DataFieldDescrizione
DataSourceData2.dsEBCClientiTabOrder    TPageControlPCLeftTop2WidthrHeight�
ActivePage
TSCompensiTabOrder 	TTabSheet
TSCompensiCaptionCompensi TDBGridDBGrid2_oldLeftTopWidth
Heightq
DataSourceDsQCompensiOptionsdgTitlesdgIndicatordgColumnResize
dgColLines
dgRowLinesdgTabsdgRowSelectdgConfirmDeletedgCancelOnExitdgMultiSelect TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameTipoWidth� Visible	 Expanded	FieldNameImportoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold Title.Font.CharsetDEFAULT_CHARSETTitle.Font.ColorclWindowTextTitle.Font.Height�Title.Font.NameMS Sans SerifTitle.Font.StylefsBold WidthEVisible	 Expanded	FieldName
RifRicercaTitle.CaptionRic.n�Width'Visible	 Expanded	FieldNameNoteWidth� Visible	 Expanded	FieldName	RimbSpeseVisible	    TBitBtnBitBtn4LeftTopWidthWHeight!Caption	InserisciFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickBitBtn4Click
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333�3333�39�3330 373337w39�3330 3��337w9���33337ww333?9���333 7wws333w39�3333 373333w39�3333337s3333?3333333 3333�33w333<333 3337�33w333<�3333����?�<����Ù37wwwww�<����Ù37wwwwsw3333<�3333337s33�333<330 3337337w3333330 3333337w3333333333333333	NumGlyphs  TRadioGroupRGOpzLeftTop� Width!HeightGCaptionOpzione inserimento	ItemIndex Items.Strings%non far riferimento ad alcun soggetto'far riferimento ad un soggetto inserito/far riferimento a uno o pi� soggetti in ricerca TabOrderOnClick
RGOpzClick  TPanelPanel2LeftTopWidth
Height	AlignmenttaLeftJustifyCaptionD  Compensi non ancora fatturati al cliente (in ordine di n� ricerca)ColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TPageControl
PCSoggettiLeftTop� WidthHeight� 
ActivePageTSSoggInseritiTabOrderVisible 	TTabSheetTSSoggInseritiCaptionTSSoggInseriti TPanelPanel1LeftTopWidth�Height� 
BevelOuter	bvLoweredCaptionPanel1TabOrder  TPanelPanel4LeftTopWidth�HeightAlignalTop	AlignmenttaLeftJustifyCaptionP  Inserimenti presso il cliente (e non ancora fatturati) per la ricerca relativaColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   TDBGridDBGrid1LeftTopWidth�HeightAlignalClient
DataSourceDsQInsNonFattTabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameCognomeWidth}Visible	 Expanded	FieldNameNomeWidthwVisible	 Expanded	FieldNameRuoloWidth� Visible	      	TTabSheetTSSoggCandRicCaptionTSSoggCandRic
ImageIndex TPanelPanel6Left Top WidthHeightAlignalTop	AlignmenttaLeftJustifyCaptionR  Candidati associati alle ricerche del cliente (non esclusi) in ordine alfabeticoColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   TAdvStringGridASG1Left TopWidthHeight� Cursor	crDefaultAlignalClientColCountDefaultRowHeight	FixedCols OptionsgoFixedVertLinegoFixedHorzLine
goVertLine
goHorzLinegoRangeSelectgoColSizing	goEditing 
ScrollBars
ssVerticalTabOrder	HintColorclYellowActiveCellFont.CharsetDEFAULT_CHARSETActiveCellFont.ColorclWindowTextActiveCellFont.Height�ActiveCellFont.NameTahomaActiveCellFont.StylefsBold CellNode.NodeTypecnFlat'ControlLook.DropDownHeader.Font.CharsetDEFAULT_CHARSET%ControlLook.DropDownHeader.Font.ColorclWindowText&ControlLook.DropDownHeader.Font.Height�$ControlLook.DropDownHeader.Font.NameTahoma%ControlLook.DropDownHeader.Font.Style "ControlLook.DropDownHeader.Visible	"ControlLook.DropDownHeader.Buttons 'ControlLook.DropDownFooter.Font.CharsetDEFAULT_CHARSET%ControlLook.DropDownFooter.Font.ColorclWindowText&ControlLook.DropDownFooter.Font.Height�$ControlLook.DropDownFooter.Font.NameMS Sans Serif%ControlLook.DropDownFooter.Font.Style "ControlLook.DropDownFooter.Visible	"ControlLook.DropDownFooter.Buttons EnhRowColMoveFilter FilterDropDown.Font.CharsetDEFAULT_CHARSETFilterDropDown.Font.ColorclWindowTextFilterDropDown.Font.Height�FilterDropDown.Font.NameMS Sans SerifFilterDropDown.Font.Style FilterDropDownClear(All)FixedColWidth�FixedRowHeightFixedFont.CharsetDEFAULT_CHARSETFixedFont.ColorclWindowTextFixedFont.Height�FixedFont.NameMS Sans SerifFixedFont.Style FloatFormat%.2fMouseActions.DirectEdit	PrintSettings.DateFormat
dd/mm/yyyyPrintSettings.Font.CharsetDEFAULT_CHARSETPrintSettings.Font.ColorclWindowTextPrintSettings.Font.Height�PrintSettings.Font.NameMS Sans SerifPrintSettings.Font.Style PrintSettings.FixedFont.CharsetDEFAULT_CHARSETPrintSettings.FixedFont.ColorclWindowTextPrintSettings.FixedFont.Height�PrintSettings.FixedFont.NameMS Sans SerifPrintSettings.FixedFont.Style  PrintSettings.HeaderFont.CharsetDEFAULT_CHARSETPrintSettings.HeaderFont.ColorclWindowTextPrintSettings.HeaderFont.Height�PrintSettings.HeaderFont.NameMS Sans SerifPrintSettings.HeaderFont.Style  PrintSettings.FooterFont.CharsetDEFAULT_CHARSETPrintSettings.FooterFont.ColorclWindowTextPrintSettings.FooterFont.Height�PrintSettings.FooterFont.NameMS Sans SerifPrintSettings.FooterFont.Style PrintSettings.Borders
pbNoborderPrintSettings.CenteredPrintSettings.PagePrefixpagePrintSettings.PageNumSep/ScrollWidthSearchFooter.FindNextCaption
Find &nextSearchFooter.FindPrevCaptionFind &previousSearchFooter.Font.CharsetDEFAULT_CHARSETSearchFooter.Font.ColorclWindowTextSearchFooter.Font.Height�SearchFooter.Font.NameMS Sans SerifSearchFooter.Font.Style SearchFooter.HighLightCaption	HighlightSearchFooter.HintCloseCloseSearchFooter.HintFindNextFind next occurenceSearchFooter.HintFindPrevFind previous occurenceSearchFooter.HintHighlightHighlight occurencesSearchFooter.MatchCaseCaption
Match caseSelectionColorclHighlightSelectionTextColorclHighlightTextURLColorclBlack
VAlignment	vtaCenterVersion5.0.0.3WordWrap	ColWidths�     	TdxDBGridDBGrid2LeftTopWidth
HeightqBands  DefaultLayout	HeaderPanelRowCountKeyFieldIDSummaryGroups SummarySeparator, TabOrder
DataSourceDsQCompensiFilter.Active	Filter.Criteria
       OptionsBehavioredgoAutoSortedgoDragScrolledgoEnterShowEditoredgoImmediateEditoredgoMultiSelectedgoTabThroughedgoVertThrough 	OptionsDBedgoCancelOnExitedgoCanDeleteedgoCanInsertedgoCanNavigationedgoConfirmDeleteedgoLoadAllRecordsedgoUseBookmarks OptionsViewedgoAutoWidthedgoBandHeaderWidthedgoIndicatoredgoUseBitmap  TdxDBGridMaskColumnDBGrid2TipoWidth`	BandIndex RowIndex 	FieldNameTipo  TdxDBGridMaskColumnDBGrid2ImportoWidthJ	BandIndex RowIndex 	FieldNameImporto  TdxDBGridMaskColumnDBGrid2IDFatturaVisibleWidth"	BandIndex RowIndex 	FieldName	IDFattura  TdxDBGridMaskColumnDBGrid2RifRicercaCaptionRif.commessaWidth^	BandIndex RowIndex 	FieldName
RifRicerca  TdxDBGridMaskColumnDBGrid2IDRicercaVisibleWidth"	BandIndex RowIndex 	FieldName	IDRicerca  TdxDBGridMaskColumnDBGrid2NoteWidth� 	BandIndex RowIndex 	FieldNameNote  TdxDBGridMaskColumn	DBGrid2IDVisibleWidth"	BandIndex RowIndex 	FieldNameID  TdxDBGridCheckColumnDBGrid2RimbSpeseCaption
Rimb.speseWidthd	BandIndex RowIndex 	FieldName	RimbSpeseValueCheckedTrueValueUncheckedFalse    	TTabSheetTSNoteSpeseCaption
Note Spese
ImageIndex TPanelPanel5LeftTopWidth
Height	AlignmenttaLeftJustifyCaption@  Note spese non ancora fatturate al cliente (in ordine di data)ColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   TBitBtnBNotaSpeseInsLeftTopWidthWHeight!Caption	InserisciFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickBNotaSpeseInsClick
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333�3333�39�3330 373337w39�3330 3��337w9���33337ww333?9���333 7wws333w39�3333 373333w39�3333337s3333?3333333 3333�33w333<333 3337�33w333<�3333����?�<����Ù37wwwww�<����Ù37wwwwsw3333<�3333337s33�333<330 3337337w3333330 3333337w3333333333333333	NumGlyphs  	TdxDBGrid	dxDBGrid1LeftTopWidth
Height�Bands  DefaultLayout	HeaderPanelRowCountKeyFieldIDSummaryGroups SummarySeparator, TabOrder
DataSourceDsQNoteSpeseCliFilter.Active	Filter.Criteria
       OptionsBehavioredgoAutoSortedgoDragScrolledgoEnterShowEditoredgoImmediateEditoredgoTabThroughedgoVertThrough 	OptionsDBedgoCancelOnExitedgoCanDeleteedgoCanInsertedgoCanNavigationedgoConfirmDeleteedgoLoadAllRecordsedgoUseBookmarks OptionsViewedgoAutoWidthedgoBandHeaderWidthedgoIndicatoredgoUseBitmap  TdxDBGridMaskColumndxDBGrid1IDVisibleWidthN	BandIndex RowIndex 	FieldNameID  TdxDBGridDateColumndxDBGrid1DataWidth[	BandIndex RowIndex 	FieldNameData  TdxDBGridMaskColumndxDBGrid1DescrizioneWidth� 	BandIndex RowIndex 	FieldNameDescrizione  TdxDBGridMaskColumndxDBGrid1AutomobileVisibleWidthN	BandIndex RowIndex 	FieldName
Automobile  TdxDBGridMaskColumndxDBGrid1AutostradaVisibleWidthN	BandIndex RowIndex 	FieldName
Autostrada  TdxDBGridMaskColumndxDBGrid1AereotrenoVisibleWidthN	BandIndex RowIndex 	FieldName
Aereotreno  TdxDBGridMaskColumndxDBGrid1TaxiVisibleWidthN	BandIndex RowIndex 	FieldNameTaxi  TdxDBGridMaskColumndxDBGrid1AlbergoVisibleWidthN	BandIndex RowIndex 	FieldNameAlbergo  TdxDBGridMaskColumndxDBGrid1RistoranteBarVisibleWidthU	BandIndex RowIndex 	FieldNameRistoranteBar  TdxDBGridMaskColumndxDBGrid1ParkingVisibleWidthN	BandIndex RowIndex 	FieldNameParking  TdxDBGridMaskColumndxDBGrid1AltroImportoVisibleWidthN	BandIndex RowIndex 	FieldNameAltroImporto  TdxDBGridMaskColumndxDBGrid1RifRicercaCaptionRif.commessaWidth\	BandIndex RowIndex 	FieldName
RifRicerca  TdxDBGridColumndxDBGrid1TotSpeseCaptionTotale speseWidthY	BandIndex RowIndex 	FieldNameTotSpese    	TTabSheet	TSAnnunciCaptionAnnunci
ImageIndex TPanelPanel7LeftTopWidth
Height	AlignmenttaLeftJustifyCaption[  Annunci non ancora fatturati al cliente (in ordine di rif.annuncio) - riferiti a ricercheColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   TDBGridDBGrid4LeftTopWidth
Height� 
DataSourceDsQAnnunciCliTabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameDataWidthJVisible	 Expanded	FieldNameRifTitle.CaptionRif.annuncioVisible	 Expanded	FieldName	NumModuliTitle.Caption	N� ModuliVisible	 Expanded	FieldName	NumParoleTitle.Caption	N� ParoleVisible	 Expanded	FieldName
TotaleEuroFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold Title.CaptionTotale EuroTitle.Font.CharsetDEFAULT_CHARSETTitle.Font.ColorclWindowTextTitle.Font.Height�Title.Font.NameMS Sans SerifTitle.Font.StylefsBold WidthLVisible	 Expanded	FieldName
RifRicercaTitle.Caption
Rif.ric.n�Visible	    TBitBtnBInsAnnuncioLeftTopWidthWHeight!Caption	InserisciFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickBInsAnnuncioClick
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333�3333�39�3330 373337w39�3330 3��337w9���33337ww333?9���333 7wws333w39�3333 373333w39�3333337s3333?3333333 3333�33w333<333 3337�33w333<�3333����?�<����Ù37wwwww�<����Ù37wwwwsw3333<�3333337s33�333<330 3337337w3333330 3333337w3333333333333333	NumGlyphs  TPanelPanel8LeftTop� Width
Height	AlignmenttaLeftJustifyCaption_  Annunci non ancora fatturati al cliente (in ordine di rif.annuncio) - NON riferiti a ricercheColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBGridDBGrid5LeftTop� Width
Height� 
DataSourceDsQAnnunciCli2TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameDataWidthJVisible	 Expanded	FieldNameRifTitle.CaptionRif.annuncioVisible	 Expanded	FieldName	NumModuliTitle.Caption	N� ModuliVisible	 Expanded	FieldName	NumParoleTitle.Caption	N� ParoleVisible	 Expanded	FieldName
TotaleEuroFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold Title.CaptionTotale EuroTitle.Font.CharsetDEFAULT_CHARSETTitle.Font.ColorclWindowTextTitle.Font.Height�Title.Font.NameMS Sans SerifTitle.Font.StylefsBold WidthLVisible	    TBitBtnBInsAnnuncio2LeftTop� WidthWHeight!Caption	InserisciFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderOnClickBInsAnnuncio2Click
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333�3333�39�3330 373337w39�3330 3��337w9���33337ww333?9���333 7wws333w39�3333 373333w39�3333337s3333?3333333 3333�33w333<333 3337�33w333<�3333����?�<����Ù37wwwww�<����Ù37wwwwsw3333<�3333337s33�333<330 3337337w3333330 3333337w3333333333333333	NumGlyphs    TDataSourceDsQInsNonFattDataSetQInsNonFattLeft� Top�  TDataSourceDsQCompensiDataSet	QCompensiLeft(Top�   TDataSourceDsQNoteSpeseCliDataSetQNoteSpeseCliLeft�Top�  TDataSourceDsQAnnunciCliDataSetQAnnunciCliLeft�Top�  TDataSourceDsQAnnunciCli2DataSetQAnnunciCli2Left7Top�  TADOLinkedQuery	QCompensi
ConnectionData.DB
CursorTypectStatic
BeforeOpenQCompensiBeforeOpen
Parameters 	UseFilterLeftGTop�   TADOLinkedQueryQInsNonFatt_old
ConnectionData.DB
CursorTypectStatic
ParametersNameidric:
AttributespaSigned DataType	ftInteger	Precision
SizeValue   SQL.StringsGselect EBC_Inserimenti.ID,Cognome,Nome,Sesso,EBC_Inserimenti.DallaData,I          EBC_Ricerche.Progressivo,DataInizio,Mansioni.Descrizione Ruolo,;          EBC_Ricerche.StipendioLordo,EBC_Ricerche.Id idric5from EBC_Inserimenti,Anagrafica,EBC_Ricerche,Mansioni0where EBC_Inserimenti.IDAnagrafica=Anagrafica.ID1    and EBC_Inserimenti.IDRicerca=EBC_Ricerche.ID+    and EBC_Ricerche.IDMansione=Mansioni.ID    and ProgFattura is nulland EBC_Ricerche.Id =:idric: 	UseFilterLeft� Topd TAutoIncFieldQInsNonFatt_oldID	FieldNameIDReadOnly	  TStringFieldQInsNonFatt_oldCognome	FieldNameCognomeSize  TStringFieldQInsNonFatt_oldNome	FieldNameNomeSize  TStringFieldQInsNonFatt_oldSesso	FieldNameSessoSize  TDateTimeFieldQInsNonFatt_oldDallaData	FieldName	DallaData  TStringFieldQInsNonFatt_oldProgressivo	FieldNameProgressivoSize  TDateTimeFieldQInsNonFatt_oldDataInizio	FieldName
DataInizio  TStringFieldQInsNonFatt_oldRuolo	FieldNameRuoloSized  TFloatFieldQInsNonFatt_oldStipendioLordo	FieldNameStipendioLordo  TAutoIncFieldQInsNonFatt_oldidric	FieldNameidricReadOnly	   TADOLinkedTableTInserimentiABS
ConnectionData.DB
CursorTypectStatic	TableNameEBC_InserimentiLeft� TopT  TADOLinkedQueryQ
ConnectionData.DB
Parameters 	UseFilterLeft(Topd  TADOLinkedTableTAnagABS
ConnectionData.DB	TableName
AnagraficaLeftpTop�  TADOLinkedQueryQNoteSpeseCli
ConnectionData.DB
CursorTypectStatic
BeforeOpenQNoteSpeseCliBeforeOpenOnCalcFieldsQNoteSpeseCli_OLDCalcFields
Parameters SQL.StringsBselect RicNoteSpese.ID,RicNoteSpese.Data,RicNoteSpese.Descrizione,"          RicNoteSpese.Automobile,"          RicNoteSpese.Autostrada,"          RicNoteSpese.Aereotreno,          RicNoteSpese.Taxi,          RicNoteSpese.Albergo,%          RicNoteSpese.RistoranteBar,          RicNoteSpese.Parking,$          RicNoteSpese.AltroImporto,/          EBC_Ricerche.Progressivo RifRicerca  from RicNoteSpese,EBC_Ricerche,where RicNoteSpese.IDRicerca=EBC_Ricerche.IDB  and (RicNoteSpese.IDFattura is null or RicNoteSpese.IDFattura=0)order by Data LinkedMasterFieldIDLinkedDetailField	IDCliente	UseFilterLeft�TopM TAutoIncFieldQNoteSpeseCliID	FieldNameIDReadOnly	  TDateTimeFieldQNoteSpeseCliData	FieldNameData  TStringFieldQNoteSpeseCliDescrizione	FieldNameDescrizioneSize2  TFloatFieldQNoteSpeseCliAutomobile	FieldName
Automobile  TFloatFieldQNoteSpeseCliAutostrada	FieldName
Autostrada  TFloatFieldQNoteSpeseCliAereotreno	FieldName
Aereotreno  TFloatFieldQNoteSpeseCliTaxi	FieldNameTaxi  TFloatFieldQNoteSpeseCliAlbergo	FieldNameAlbergo  TFloatFieldQNoteSpeseCliRistoranteBar	FieldNameRistoranteBar  TFloatFieldQNoteSpeseCliParking	FieldNameParking  TFloatFieldQNoteSpeseCliAltroImporto	FieldNameAltroImporto  TStringFieldQNoteSpeseCliRifRicerca	FieldName
RifRicercaSize  TFloatFieldQNoteSpeseCliTotSpese	FieldKindfkCalculated	FieldNameTotSpese
Calculated	   TADOLinkedQueryQAnnunciCli
ConnectionData.DB
CursorTypectStatic
BeforeOpenQAnnunciCliBeforeOpen
Parameters SQL.Stringsselect Ann_annunci.Data,          Ann_annunci.Rif,          Ann_annunci.ID,           Ann_annunci.NumModuli,           Ann_annunci.NumParole,!          Ann_annunci.TotaleLire,!          Ann_annunci.TotaleEuro,-          EBC_Ricerche.Progressivo RifRicerca1from Ann_annunci,Ann_annunciRicerche,EBC_Ricerche3where Ann_annunci.ID=Ann_annunciRicerche.IDAnnuncio3  and Ann_annunciRicerche.IDRicerca=EBC_Ricerche.ID@  and (Ann_annunci.IDFattura is null or Ann_annunci.IDFattura=0)order by Data  LinkedMasterFieldIDLinkedDetailField	IDCliente	UseFilterLeft�TopM  TADOLinkedQueryQAnnunciCli2
ConnectionData.DB
CursorTypectStatic
BeforeOpenQAnnunciCli2BeforeOpen
Parameters SQL.Stringsselect Ann_annunci.Data,          Ann_annunci.Rif,          Ann_annunci.ID,           Ann_annunci.NumModuli,           Ann_annunci.NumParole,!          Ann_annunci.TotaleLire,           Ann_annunci.TotaleEurofrom Ann_annunci@where (Ann_annunci.IDFattura is null or Ann_annunci.IDFattura=0)order by Data LinkedMasterFieldIDLinkedDetailField	IDCliente	UseFilterLeft7TopJ  	TADOQueryQInsNonFatt
ConnectionData.DB
ParametersNameidric
AttributespaSigned DataType	ftInteger	Precision
SizeValue   SQL.StringsGselect EBC_Inserimenti.ID,Cognome,Nome,Sesso,EBC_Inserimenti.DallaData,I          EBC_Ricerche.Progressivo,DataInizio,Mansioni.Descrizione Ruolo,;          EBC_Ricerche.StipendioLordo,EBC_Ricerche.Id idric5from EBC_Inserimenti,Anagrafica,EBC_Ricerche,Mansioni0where EBC_Inserimenti.IDAnagrafica=Anagrafica.ID1    and EBC_Inserimenti.IDRicerca=EBC_Ricerche.ID+    and EBC_Ricerche.IDMansione=Mansioni.ID    and ProgFattura is nulland EBC_Ricerche.Id =:idric Left8Top� TAutoIncFieldQInsNonFattID	FieldNameIDReadOnly	  TStringFieldQInsNonFattCognome	FieldNameCognomeSize  TStringFieldQInsNonFattNome	FieldNameNomeSize  TStringFieldQInsNonFattSesso	FieldNameSessoSize  TDateTimeFieldQInsNonFattDallaData	FieldName	DallaData  TStringFieldQInsNonFattProgressivo	FieldNameProgressivoSize  TDateTimeFieldQInsNonFattDataInizio	FieldName
DataInizio  TStringFieldQInsNonFattRuolo	FieldNameRuoloSized  TFloatFieldQInsNonFattStipendioLordo	FieldNameStipendioLordo  TAutoIncFieldQInsNonFattidric	FieldNameidricReadOnly	   	TADOQueryQCandRicCliente
ConnectionData.DB
BeforeOpenQCandRicClienteBeforeOpen
ParametersNamex
AttributespaSigned
paNullable DataType	ftInteger	Precision
SizeValue  SQL.Strings8select Anagrafica.ID,Anagrafica.Cognome,Anagrafica.Nome,.          EBC_Ricerche.Progressivo RifRicerca,#          EBC_Ricerche.ID IDRicerca2from EBC_CandidatiRicerche,EBC_Ricerche,Anagrafica5where EBC_CandidatiRicerche.IDRicerca=EBC_Ricerche.ID6  and EBC_CandidatiRicerche.IDAnagrafica=Anagrafica.IDP  and (EBC_CandidatiRicerche.Escluso=0 or EBC_CandidatiRicerche.Escluso is null)and EBC_Ricerche.IDCliente =:xorder by Cognome,Nome LeftpTopY TAutoIncFieldQCandRicClienteID	FieldNameIDReadOnly	  TStringFieldQCandRicClienteCognome	FieldNameCognomeSize  TStringFieldQCandRicClienteNome	FieldNameNomeSize  TStringFieldQCandRicClienteRifRicerca	FieldName
RifRicercaSize  TAutoIncFieldQCandRicClienteIDRicerca	FieldName	IDRicercaReadOnly	   	TADOQuery	QFindAnag
ConnectionData.DB
ParametersNamex
AttributespaSigned DataType	ftInteger	Precision
SizeValue  SQL.Stringsselect ID,Cognome,Nome,Sessofrom Anagraficawhere ID = :x Left Top� TAutoIncFieldQFindAnagID	FieldNameIDReadOnly	  TStringFieldQFindAnagCognome	FieldNameCognomeSize  TStringFieldQFindAnagNome	FieldNameNomeSize  TStringFieldQFindAnagSesso	FieldNameSessoSize    