object ElencoUtentiForm: TElencoUtentiForm
  Left = 339
  Top = 129
  BorderStyle = bsDialog
  Caption = 'Elenco utenti sistema'
  ClientHeight = 285
  ClientWidth = 381
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object BitBtn1: TBitBtn
    Left = 288
    Top = 4
    Width = 89
    Height = 35
    TabOrder = 0
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 288
    Top = 41
    Width = 89
    Height = 35
    Caption = 'Annulla'
    TabOrder = 1
    Kind = bkCancel
  end
  object DBGrid1: TDBGrid
    Left = 4
    Top = 27
    Width = 278
    Height = 254
    DataSource = DsUsers
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'Nominativo'
        Width = 94
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Descrizione'
        Width = 150
        Visible = True
      end>
  end
  object Panel2: TPanel
    Left = 4
    Top = 5
    Width = 278
    Height = 21
    Alignment = taLeftJustify
    Caption = '  Elenco utenti abilitati e non scaduti'
    Color = clGray
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
  end
  object DsUsers: TDataSource
    DataSet = QUsers
    Left = 40
    Top = 208
  end
  object QUsers: TADOLinkedQuery
    Connection = Data.DB
    Parameters = <>
    OriginalSQL.Strings = (
      'select ID,Nominativo,Descrizione'
      'from Users '
      'where not ((DataScadenza is not null and DataScadenza<:xoggi:)'
      '   or (DataRevoca is not null and DataRevoca<:xoggi:)'
      '   or DataCreazione is null'
      '   or Tipo=0)'
      'order by Nominativo'
      '')
    UseFilter = False
    Left = 40
    Top = 176
  end
end
