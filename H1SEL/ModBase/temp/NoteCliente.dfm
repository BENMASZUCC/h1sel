object NoteClienteForm: TNoteClienteForm
  Left = 327
  Top = 155
  Width = 605
  Height = 420
  Caption = 'Note Cliente'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 597
    Height = 47
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object BitBtn1: TBitBtn
      Left = 493
      Top = 6
      Width = 99
      Height = 35
      Caption = 'Esci'
      TabOrder = 0
      Kind = bkOK
    end
  end
  inline FrameDBRich1: TFrameDBRich
    Top = 47
    Height = 346
    Align = alClient
    TabOrder = 1
    inherited StatusBar: TStatusBar
      Top = 327
    end
    inherited Editor: TDBRichEdit
      Height = 269
      DataField = 'Note'
      DataSource = DsQNoteCliente
    end
  end
  object DsQNoteCliente: TDataSource
    DataSet = QNoteCliente
    Left = 40
    Top = 256
  end
  object UpdNoteCliente: TUpdateSQL
    ModifySQL.Strings = (
      'update EBC_Clienti'
      'set'
      '  Note = :Note'
      'where'
      '  ID = :OLD_ID')
    InsertSQL.Strings = (
      'insert into EBC_Clienti'
      '  (Note)'
      'values'
      '  (:Note)')
    DeleteSQL.Strings = (
      'delete from EBC_Clienti'
      'where'
      '  ID = :OLD_ID')
    Left = 80
    Top = 232
  end
  object QNoteCliente: TADOLinkedQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select ID,Note'
      'from EBC_Clienti'
      ''
      '')
    OriginalSQL.Strings = (
      'select ID,Note'
      'from EBC_Clienti'
      'where ID=:xIDCliente:')
    UseFilter = False
    Left = 40
    Top = 216
    object QNoteClienteID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QNoteClienteNote: TMemoField
      FieldName = 'Note'
      BlobType = ftMemo
    end
  end
  object ADOQuery1: TADOQuery
    Connection = Data.DB
    Parameters = <>
    Left = 480
    Top = 152
  end
end
