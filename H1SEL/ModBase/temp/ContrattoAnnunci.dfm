object ContrattoAnnunciForm: TContrattoAnnunciForm
  Left = 391
  Top = 108
  ActiveControl = DBGrid1
  BorderStyle = bsDialog
  Caption = 'Contratto con testata-edizione per annunci'
  ClientHeight = 347
  ClientWidth = 431
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 19
    Top = 215
    Width = 56
    Height = 13
    Caption = 'Data stipula'
  end
  object Label2: TLabel
    Left = 3
    Top = 239
    Width = 72
    Height = 13
    Caption = 'Data scadenza'
  end
  object Label3: TLabel
    Left = 12
    Top = 263
    Width = 63
    Height = 13
    Caption = 'Totale moduli'
  end
  object Label4: TLabel
    Left = 5
    Top = 287
    Width = 70
    Height = 13
    Caption = 'Moduli utilizzati'
  end
  object Label5: TLabel
    Left = 10
    Top = 325
    Width = 65
    Height = 13
    Caption = 'Costo Singolo'
  end
  object Label6: TLabel
    Left = 187
    Top = 302
    Width = 19
    Height = 14
    Caption = 'Lire'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsItalic]
    ParentFont = False
  end
  object Bevel1: TBevel
    Left = 83
    Top = 316
    Width = 122
    Height = 5
    Shape = bsTopLine
  end
  object Bevel2: TBevel
    Left = 211
    Top = 316
    Width = 122
    Height = 5
    Shape = bsTopLine
  end
  object Label7: TLabel
    Left = 309
    Top = 302
    Width = 23
    Height = 14
    Caption = 'Euro'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsItalic]
    ParentFont = False
  end
  object BitBtn1: TBitBtn
    Left = 337
    Top = 2
    Width = 93
    Height = 38
    TabOrder = 6
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 337
    Top = 40
    Width = 93
    Height = 38
    Caption = 'Annulla'
    TabOrder = 7
    Kind = bkCancel
  end
  object DBGrid1: TDBGrid
    Left = 3
    Top = 3
    Width = 330
    Height = 202
    DataSource = DsQTestEdiz
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'Testata'
        Width = 159
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Edizione'
        Width = 135
        Visible = True
      end>
  end
  object DEDataStipula: TDateEdit97
    Left = 84
    Top = 211
    Width = 101
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    ColorCalendar.ColorValid = clBlue
    DayNames.Monday = 'lu'
    DayNames.Tuesday = 'ma'
    DayNames.Wednesday = 'me'
    DayNames.Thursday = 'gi'
    DayNames.Friday = 've'
    DayNames.Saturday = 'sa'
    DayNames.Sunday = 'do'
    MonthNames.January = 'gennaio'
    MonthNames.February = 'febbraio'
    MonthNames.March = 'marzo'
    MonthNames.April = 'aprile'
    MonthNames.May = 'maggio'
    MonthNames.June = 'giugno'
    MonthNames.July = 'luglio'
    MonthNames.August = 'agosto'
    MonthNames.September = 'settembre'
    MonthNames.October = 'ottobre'
    MonthNames.November = 'novembre'
    MonthNames.December = 'dicembre'
    Options = [doButtonTabStop, doCanPopup, doIsMasked, doShowCancel, doShowToday]
  end
  object DEDataScadenza: TDateEdit97
    Left = 84
    Top = 235
    Width = 121
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    ColorCalendar.ColorValid = clBlue
    DayNames.Monday = 'lu'
    DayNames.Tuesday = 'ma'
    DayNames.Wednesday = 'me'
    DayNames.Thursday = 'gi'
    DayNames.Friday = 've'
    DayNames.Saturday = 'sa'
    DayNames.Sunday = 'do'
    MonthNames.January = 'gennaio'
    MonthNames.February = 'febbraio'
    MonthNames.March = 'marzo'
    MonthNames.April = 'aprile'
    MonthNames.May = 'maggio'
    MonthNames.June = 'giugno'
    MonthNames.July = 'luglio'
    MonthNames.August = 'agosto'
    MonthNames.September = 'settembre'
    MonthNames.October = 'ottobre'
    MonthNames.November = 'novembre'
    MonthNames.December = 'dicembre'
    Options = [doButtonTabStop, doCanClear, doCanPopup, doIsMasked, doShowCancel, doShowToday]
  end
  object TotModuli: TRxSpinEdit
    Left = 84
    Top = 259
    Width = 62
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
  end
  object ModuliUtilizzati: TRxSpinEdit
    Left = 84
    Top = 283
    Width = 62
    Height = 21
    TabOrder = 4
  end
  object CostoLire: TRxCalcEdit
    Left = 84
    Top = 321
    Width = 121
    Height = 21
    AutoSize = False
    DisplayFormat = '�'#39'.'#39' ,0.'
    NumGlyphs = 2
    TabOrder = 5
    OnChange = CostoLireChange
  end
  object CostoEuro: TRxCalcEdit
    Left = 212
    Top = 321
    Width = 121
    Height = 21
    TabStop = False
    AutoSize = False
    DisplayFormat = '�'#39'.'#39' ,0.00'
    NumGlyphs = 2
    TabOrder = 8
    OnChange = CostoEuroChange
  end
  object QTestEdiz_OLD: TQuery
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select IDTestata,Ann_Edizioni.ID IDEdizione,'
      
        '          Ann_Testate.Denominazione Testata, NomeEdizione Edizio' +
        'ne'
      'from Ann_Edizioni,Ann_Testate'
      'where Ann_Edizioni.IDTestata=Ann_Testate.ID'
      'order by Ann_Testate.Denominazione,NomeEdizione')
    Left = 32
    Top = 56
    object QTestEdiz_OLDIDTestata: TIntegerField
      FieldName = 'IDTestata'
      Origin = 'EBCDB.Ann_Edizioni.IDTestata'
    end
    object QTestEdiz_OLDIDEdizione: TAutoIncField
      FieldName = 'IDEdizione'
      Origin = 'EBCDB.Ann_Edizioni.ID'
    end
    object QTestEdiz_OLDTestata: TStringField
      FieldName = 'Testata'
      Origin = 'EBCDB.Ann_Testate.Denominazione'
      FixedChar = True
      Size = 50
    end
    object QTestEdiz_OLDEdizione: TStringField
      FieldName = 'Edizione'
      Origin = 'EBCDB.Ann_Edizioni.NomeEdizione'
      FixedChar = True
      Size = 50
    end
  end
  object DsQTestEdiz: TDataSource
    DataSet = QTestEdiz
    Left = 32
    Top = 88
  end
  object QTestEdiz: TADOLinkedQuery
    Active = True
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select IDTestata,Ann_Edizioni.ID IDEdizione,'
      
        '          Ann_Testate.Denominazione Testata, NomeEdizione Edizio' +
        'ne'
      'from Ann_Edizioni,Ann_Testate'
      'where Ann_Edizioni.IDTestata=Ann_Testate.ID'
      'order by Ann_Testate.Denominazione,NomeEdizione'
      '')
    Left = 64
    Top = 56
  end
end
