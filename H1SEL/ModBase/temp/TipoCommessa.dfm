object TipoCommessaForm: TTipoCommessaForm
  Left = 207
  Top = 106
  BorderStyle = bsDialog
  Caption = 'Tipo commessa'
  ClientHeight = 69
  ClientWidth = 317
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 5
    Top = 12
    Width = 24
    Height = 13
    Caption = 'Tipo:'
  end
  object Label2: TLabel
    Left = 5
    Top = 36
    Width = 32
    Height = 13
    Caption = 'colore:'
  end
  object BitBtn1: TBitBtn
    Left = 224
    Top = 3
    Width = 89
    Height = 30
    TabOrder = 0
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 224
    Top = 35
    Width = 89
    Height = 30
    Caption = 'Annulla'
    TabOrder = 1
    Kind = bkCancel
  end
  object ETipo: TEdit
    Left = 47
    Top = 9
    Width = 167
    Height = 21
    MaxLength = 30
    TabOrder = 2
  end
  object CBColore: TComboBox
    Left = 47
    Top = 33
    Width = 134
    Height = 21
    ItemHeight = 13
    TabOrder = 3
    Items.Strings = (
      'rosso'
      'verde'
      'giallo'
      'azzurro'
      'fucsia'
      'bianco'
      'oliva'
      'grigio'
      'marrone')
  end
end
