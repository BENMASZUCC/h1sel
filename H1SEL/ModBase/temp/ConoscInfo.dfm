object ConoscInfoForm: TConoscInfoForm
  Left = 231
  Top = 106
  Width = 619
  Height = 401
  Caption = ' '
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 611
    Height = 44
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object BVoceNew: TToolbarButton97
      Left = 4
      Top = 4
      Width = 62
      Height = 37
      Caption = 'Nuovo'
      Opaque = False
      WordWrap = True
      OnClick = BVoceNewClick
    end
    object BVoceDel: TToolbarButton97
      Left = 66
      Top = 4
      Width = 62
      Height = 37
      Caption = 'Elimina'
      Opaque = False
      WordWrap = True
      OnClick = BVoceDelClick
    end
    object BVoceCan: TToolbarButton97
      Left = 190
      Top = 4
      Width = 62
      Height = 37
      Caption = 'Annulla modifica'
      Enabled = False
      Opaque = False
      WordWrap = True
      OnClick = BVoceCanClick
    end
    object BVoceOK: TToolbarButton97
      Left = 128
      Top = 4
      Width = 62
      Height = 37
      Caption = 'Conferma modifica'
      Enabled = False
      Opaque = False
      WordWrap = True
      OnClick = BVoceOKClick
    end
    object BitBtn1: TBitBtn
      Left = 406
      Top = 4
      Width = 99
      Height = 38
      TabOrder = 0
      Kind = bkOK
    end
    object BitBtn2: TBitBtn
      Left = 508
      Top = 4
      Width = 99
      Height = 38
      Caption = 'Annulla'
      TabOrder = 1
      Kind = bkCancel
    end
  end
  object dxDBGrid1: TdxDBGrid
    Left = 0
    Top = 44
    Width = 611
    Height = 330
    Bands = <
      item
      end>
    DefaultLayout = True
    HeaderPanelRowCount = 1
    KeyField = 'ID'
    SummaryGroups = <>
    SummarySeparator = ', '
    Align = alClient
    TabOrder = 1
    DataSource = DsQConoscInfo
    Filter.Criteria = {00000000}
    OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
    OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoUseBitmap]
    object dxDBGrid1ID: TdxDBGridMaskColumn
      Visible = False
      Width = 164
      BandIndex = 0
      RowIndex = 0
      FieldName = 'ID'
    end
    object dxDBGrid1Area: TdxDBGridPickColumn
      Width = 124
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Area'
      Items.Strings = (
        'COMMERCIALISTI'
        'GESTIONE DEL PERSONALE'
        'GESTIONALI')
    end
    object dxDBGrid1SottoArea: TdxDBGridPickColumn
      Caption = 'Sottoarea'
      Width = 114
      BandIndex = 0
      RowIndex = 0
      FieldName = 'SottoArea'
      Items.Strings = (
        'Area Contabile'
        'Area Fiscale'
        'Area Gestione Studio')
    end
    object dxDBGrid1Proprieta: TdxDBGridMaskColumn
      Width = 90
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Proprieta'
    end
    object dxDBGrid1Procedura: TdxDBGridMaskColumn
      Caption = 'Procedura/Applicativo'
      Width = 131
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Procedura'
    end
    object dxDBGrid1Procedura_ENG: TdxDBGridColumn
      Caption = 'Procedura/Applicativo (ENG)'
      Width = 148
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Procedura_ENG'
    end
  end
  object QConoscInfo_OLD: TQuery
    CachedUpdates = True
    AfterPost = QConoscInfo_OLDAfterPost
    AfterDelete = QConoscInfo_OLDAfterPost
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select * from ConoscenzeInfo'
      'order by Area,SottoArea,Proprieta,Procedura')
    UpdateObject = UpdConoscInfo
    Left = 56
    Top = 160
    object QConoscInfo_OLDID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.ConoscenzeInfo.ID'
    end
    object QConoscInfo_OLDArea: TStringField
      FieldName = 'Area'
      Origin = 'EBCDB.ConoscenzeInfo.Area'
      FixedChar = True
      Size = 30
    end
    object QConoscInfo_OLDSottoArea: TStringField
      FieldName = 'SottoArea'
      Origin = 'EBCDB.ConoscenzeInfo.SottoArea'
      FixedChar = True
      Size = 30
    end
    object QConoscInfo_OLDProprieta: TStringField
      FieldName = 'Proprieta'
      Origin = 'EBCDB.ConoscenzeInfo.Proprieta'
      FixedChar = True
      Size = 30
    end
    object QConoscInfo_OLDProcedura: TStringField
      FieldName = 'Procedura'
      Origin = 'EBCDB.ConoscenzeInfo.Procedura'
      FixedChar = True
      Size = 50
    end
  end
  object DsQConoscInfo: TDataSource
    DataSet = QConoscInfo
    OnStateChange = DsQConoscInfoStateChange
    Left = 24
    Top = 192
  end
  object UpdConoscInfo: TUpdateSQL
    ModifySQL.Strings = (
      'update ConoscenzeInfo'
      'set'
      '  Area = :Area,'
      '  SottoArea = :SottoArea,'
      '  Proprieta = :Proprieta,'
      '  Procedura = :Procedura'
      'where'
      '  ID = :OLD_ID')
    InsertSQL.Strings = (
      'insert into ConoscenzeInfo'
      '  (Area, SottoArea, Proprieta, Procedura)'
      'values'
      '  (:Area, :SottoArea, :Proprieta, :Procedura)')
    DeleteSQL.Strings = (
      'delete from ConoscenzeInfo'
      'where'
      '  ID = :OLD_ID')
    Left = 96
    Top = 160
  end
  object QConoscInfo: TADOLinkedQuery
    Active = True
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from ConoscenzeInfo'
      'order by Area,SottoArea,Proprieta,Procedura')
    UseFilter = False
    Left = 24
    Top = 160
  end
end
