object MyQuickViewConfigForm: TMyQuickViewConfigForm
  Left = 339
  Top = 192
  BorderStyle = bsDialog
  Caption = 'Configuratore My Quick View'
  ClientHeight = 274
  ClientWidth = 607
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 277
    Top = 45
    Width = 3
    Height = 229
    Cursor = crHSplit
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 607
    Height = 45
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object BitBtn1: TBitBtn
      Left = 513
      Top = 4
      Width = 90
      Height = 38
      Anchors = [akTop, akRight]
      Caption = 'Esci'
      TabOrder = 0
      Kind = bkClose
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 45
    Width = 277
    Height = 229
    Align = alLeft
    BevelOuter = bvLowered
    TabOrder = 1
    object PanTitoloCompensi: TPanel
      Left = 1
      Top = 1
      Width = 275
      Height = 21
      Align = alTop
      Alignment = taLeftJustify
      Caption = '  Voci disponibili'
      Color = clGray
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object dxDBGrid1: TdxDBGrid
      Left = 1
      Top = 22
      Width = 275
      Height = 206
      Bands = <
        item
        end>
      DefaultLayout = True
      HeaderPanelRowCount = 1
      KeyField = 'ID'
      SummaryGroups = <>
      SummarySeparator = ', '
      Align = alClient
      TabOrder = 1
      DataSource = DsQDisp
      Filter.Criteria = {00000000}
      LookAndFeel = lfFlat
      OptionsBehavior = [edgoAutoSearch, edgoAutoSort, edgoDragScroll, edgoEditing, edgoEnterShowEditor, edgoImmediateEditor, edgoTabThrough, edgoVertThrough]
      OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
      OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoIndicator, edgoUseBitmap]
      object dxDBGrid1Voce: TdxDBGridMaskColumn
        DisableEditor = True
        Sorted = csUp
        Width = 210
        BandIndex = 0
        RowIndex = 0
        FieldName = 'Voce'
      end
      object dxDBGrid1Column2: TdxDBGridGraphicColumn
        Alignment = taCenter
        Caption = 'Icona'
        Visible = False
        Width = 51
        BandIndex = 0
        RowIndex = 0
        FieldName = 'SmallIcon'
      end
    end
  end
  object Panel3: TPanel
    Left = 327
    Top = 45
    Width = 280
    Height = 229
    Align = alClient
    BevelOuter = bvLowered
    TabOrder = 2
    object Panel4: TPanel
      Left = 1
      Top = 1
      Width = 278
      Height = 21
      Align = alTop
      Alignment = taLeftJustify
      Caption = '  Voci visibili per l'#39'utente e in quale ordine'
      Color = clGray
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object dxDBGrid2: TdxDBGrid
      Left = 1
      Top = 22
      Width = 278
      Height = 206
      Bands = <
        item
        end>
      DefaultLayout = True
      HeaderPanelRowCount = 1
      KeyField = 'ID'
      SummaryGroups = <>
      SummarySeparator = ', '
      Align = alClient
      TabOrder = 1
      DataSource = DsQVisibili
      Filter.Criteria = {00000000}
      LookAndFeel = lfFlat
      OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
      OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoIndicator, edgoUseBitmap]
      object dxDBGrid2DicNodoMaster: TdxDBGridMaskColumn
        Caption = 'Voce'
        DisableEditor = True
        Width = 205
        BandIndex = 0
        RowIndex = 0
        FieldName = 'DicNodoMaster'
      end
      object dxDBGrid2Ordine: TdxDBGridMaskColumn
        Sorted = csUp
        Width = 57
        BandIndex = 0
        RowIndex = 0
        FieldName = 'Ordine'
      end
    end
  end
  object Panel5: TPanel
    Left = 280
    Top = 45
    Width = 47
    Height = 229
    Align = alLeft
    BevelOuter = bvLowered
    TabOrder = 3
    object BInserisci: TToolbarButton97
      Left = 5
      Top = 41
      Width = 38
      Height = 32
      Hint = 'Rendi visibili la voce selezionata all'#39'utente'
      Caption = '>'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      OnClick = BInserisciClick
    end
    object BTogli: TToolbarButton97
      Left = 5
      Top = 73
      Width = 38
      Height = 32
      Hint = 'Togli la visibilitÓ per la voce selezionata all'#39'utente'
      Caption = '<'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      OnClick = BTogliClick
    end
    object BTutte: TToolbarButton97
      Left = 5
      Top = 115
      Width = 38
      Height = 32
      Hint = 'Rendi visibili all'#39'utente tutte le voci'
      Caption = '>>'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      OnClick = BTutteClick
    end
  end
  object QDisp: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select ID,DicNodoMaster Voce, SmallIcon'
      'from MyQuickViewModel'
      'where idsoftware=1')
    Left = 200
    Top = 200
  end
  object DsQDisp: TDataSource
    DataSet = QDisp
    Left = 200
    Top = 232
  end
  object QVisibili: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'xIDUtente'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select MyQuickViewModel.DicNodoMaster , MyQuickViewUsers.*'
      'from MyQuickViewModel, MyQuickViewUsers'
      'where MyQuickViewModel.ID = MyQuickViewUsers.IDModelItem'
      'and IDUtente=:xIDUtente')
    Left = 359
    Top = 173
  end
  object DsQVisibili: TDataSource
    DataSet = QVisibili
    Left = 359
    Top = 205
  end
  object Q: TADOQuery
    Connection = Data.DB
    Parameters = <>
    Left = 288
    Top = 205
  end
end
