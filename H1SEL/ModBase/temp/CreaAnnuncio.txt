object CreaAnnuncioForm: TCreaAnnuncioForm
  Left = 321
  Top = 162
  Width = 560
  Height = 418
  Caption = 'Creazione Annuncio'
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 5
    Top = 5
    Width = 461
    Height = 265
    Caption = 'Testata'
    TabOrder = 0
    object RadioGroup1: TRadioGroup
      Left = 9
      Top = 15
      Width = 336
      Height = 37
      Caption = 'Tipo testata'
      Columns = 4
      Items.Strings = (
        'tutte'
        'nazionale'
        'regionale'
        'locale')
      TabOrder = 0
    end
    object DBGrid1: TDBGrid
      Left = 10
      Top = 60
      Width = 439
      Height = 195
      ReadOnly = True
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
    end
    object BitBtn1: TBitBtn
      Left = 357
      Top = 21
      Width = 90
      Height = 32
      Caption = 'Scheda testata'
      TabOrder = 2
      OnClick = BitBtn1Click
    end
  end
end
