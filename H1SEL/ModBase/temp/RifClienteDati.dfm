object RifClienteDatiForm: TRifClienteDatiForm
  Left = 319
  Top = 106
  ActiveControl = ETitStudio
  BorderStyle = bsDialog
  Caption = 'Riferimento interno cliente - altri dati'
  ClientHeight = 536
  ClientWidth = 324
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label4: TLabel
    Left = 43
    Top = 53
    Width = 71
    Height = 13
    Caption = 'Titolo di studio:'
  end
  object Label5: TLabel
    Left = 5
    Top = 233
    Width = 81
    Height = 13
    Caption = 'Carica aziendale:'
  end
  object Label6: TLabel
    Left = 3
    Top = 195
    Width = 80
    Height = 13
    Caption = 'Nome segretaria:'
  end
  object Label11: TLabel
    Left = 3
    Top = 489
    Width = 121
    Height = 13
    Caption = 'Utente che lo ha inserito :'
  end
  object Label12: TLabel
    Left = 3
    Top = 513
    Width = 82
    Height = 13
    Caption = 'Data inserimento:'
  end
  object Label13: TLabel
    Left = 5
    Top = 53
    Width = 29
    Height = 13
    Caption = 'Sesso'
  end
  object Label15: TLabel
    Left = 3
    Top = 158
    Width = 94
    Height = 13
    Caption = 'Divisione aziendale:'
  end
  object Label17: TLabel
    Left = 3
    Top = 118
    Width = 86
    Height = 13
    Caption = 'Ruolo (da tabella):'
  end
  object SpeedButton3: TSpeedButton
    Left = 206
    Top = 132
    Width = 24
    Height = 24
    Hint = 'Scegli da tabella'
    Glyph.Data = {
      36050000424D3605000000000000360400002800000010000000100000000100
      08000000000000010000120B0000120B00000001000000010000000000008400
      0000FF00000084848400C6C6C600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00050505050505
      0505050505050505050500000000000000000000000000000005030303030303
      0303030303030303000503040505050505050505050505030005030405050505
      0505050505050503000503040503030303050303030305030005030405050505
      0505050505050503000503040503030303050303030305030005030405050505
      0505050505050503000503040503030303050303030305030005030405050505
      0505050505050503000503040303030303030303030303030005030401020102
      0102010500050003000503040404040404040404040404030005030303030303
      0303030303030303030505050505050505050505050505050505}
    ParentShowHint = False
    ShowHint = True
    OnClick = SpeedButton3Click
  end
  object BitBtn1: TBitBtn
    Left = 229
    Top = 2
    Width = 93
    Height = 35
    TabOrder = 8
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 229
    Top = 38
    Width = 93
    Height = 35
    Caption = 'Annulla'
    TabOrder = 9
    Kind = bkCancel
  end
  object Panel1: TPanel
    Left = 3
    Top = 4
    Width = 222
    Height = 45
    BevelOuter = bvLowered
    Enabled = False
    TabOrder = 10
    object Label1: TLabel
      Left = 6
      Top = 4
      Width = 56
      Height = 13
      Caption = 'Nominativo:'
    end
    object ENominativo: TEdit
      Left = 5
      Top = 19
      Width = 212
      Height = 21
      TabStop = False
      Color = clAqua
      TabOrder = 0
    end
  end
  object GroupBox1: TGroupBox
    Left = 3
    Top = 298
    Width = 178
    Height = 45
    Caption = 'Compleanno:'
    TabOrder = 1
    object Label2: TLabel
      Left = 8
      Top = 20
      Width = 34
      Height = 13
      Caption = 'Giorno:'
    end
    object Label3: TLabel
      Left = 95
      Top = 20
      Width = 29
      Height = 13
      Caption = 'Mese:'
    end
    object SEGiorno: TSpinEdit
      Left = 46
      Top = 17
      Width = 41
      Height = 22
      MaxValue = 31
      MinValue = 0
      TabOrder = 0
      Value = 0
    end
    object SEMese: TSpinEdit
      Left = 133
      Top = 17
      Width = 38
      Height = 22
      MaxValue = 12
      MinValue = 0
      TabOrder = 1
      Value = 0
    end
  end
  object Panel2: TPanel
    Left = 3
    Top = 276
    Width = 222
    Height = 21
    Alignment = taLeftJustify
    Caption = '  Dati personali'
    Color = clGray
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 11
  end
  object Panel3: TPanel
    Left = 3
    Top = 95
    Width = 222
    Height = 21
    Alignment = taLeftJustify
    Caption = '  Dati aziendali'
    Color = clGray
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 12
  end
  object ETitStudio: TEdit
    Left = 43
    Top = 68
    Width = 182
    Height = 21
    MaxLength = 30
    TabOrder = 2
  end
  object ECaricaAz: TEdit
    Left = 4
    Top = 248
    Width = 222
    Height = 21
    MaxLength = 40
    TabOrder = 6
  end
  object ENomeSeg: TEdit
    Left = 3
    Top = 210
    Width = 222
    Height = 21
    MaxLength = 40
    TabOrder = 5
  end
  object GroupBox2: TGroupBox
    Left = 3
    Top = 346
    Width = 222
    Height = 108
    Caption = 'Residenza privata'
    TabOrder = 7
    object Label7: TLabel
      Left = 8
      Top = 15
      Width = 38
      Height = 13
      Caption = 'Indirizzo'
    end
    object Label8: TLabel
      Left = 8
      Top = 60
      Width = 19
      Height = 13
      Caption = 'Cap'
    end
    object Label10: TLabel
      Left = 80
      Top = 59
      Width = 25
      Height = 13
      Caption = 'Prov.'
    end
    object Label9: TLabel
      Left = 8
      Top = 84
      Width = 45
      Height = 13
      Caption = 'Telefono:'
    end
    object Label14: TLabel
      Left = 6
      Top = 17
      Width = 53
      Height = 13
      Caption = 'Tipo strada'
    end
    object Label93: TLabel
      Left = 63
      Top = 17
      Width = 38
      Height = 13
      Caption = 'Indirizzo'
    end
    object Label99: TLabel
      Left = 178
      Top = 17
      Width = 30
      Height = 13
      Caption = 'n� civ.'
    end
    object Label16: TLabel
      Left = 140
      Top = 59
      Width = 39
      Height = 13
      Caption = 'Nazione'
    end
    object EIndirizzo: TEdit
      Left = 63
      Top = 32
      Width = 113
      Height = 21
      MaxLength = 50
      TabOrder = 1
    end
    object ECap: TEdit
      Left = 33
      Top = 55
      Width = 41
      Height = 21
      MaxLength = 5
      TabOrder = 3
    end
    object EProv: TEdit
      Left = 107
      Top = 56
      Width = 27
      Height = 21
      CharCase = ecUpperCase
      MaxLength = 2
      TabOrder = 4
    end
    object ETelAb: TEdit
      Left = 58
      Top = 80
      Width = 156
      Height = 21
      MaxLength = 40
      TabOrder = 6
    end
    object CBTipoStradaRes: TComboBox
      Left = 8
      Top = 32
      Width = 54
      Height = 21
      ItemHeight = 13
      TabOrder = 0
      OnDropDown = CBTipoStradaResDropDown
    end
    object ENumCivico: TEdit
      Left = 178
      Top = 32
      Width = 36
      Height = 21
      TabOrder = 2
    end
    object ENazione: TEdit
      Left = 183
      Top = 56
      Width = 31
      Height = 21
      CharCase = ecUpperCase
      MaxLength = 3
      TabOrder = 5
    end
  end
  object Panel4: TPanel
    Left = 3
    Top = 460
    Width = 222
    Height = 21
    Alignment = taLeftJustify
    Caption = '  Nostri dati:'
    Color = clGray
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 13
  end
  object EUtente: TEdit
    Left = 124
    Top = 486
    Width = 102
    Height = 21
    TabStop = False
    Enabled = False
    TabOrder = 14
  end
  object CBSesso: TComboBox
    Left = 4
    Top = 68
    Width = 37
    Height = 21
    ItemHeight = 13
    TabOrder = 0
    Items.Strings = (
      'M'
      'F')
  end
  object EDivisioneAz: TEdit
    Left = 3
    Top = 173
    Width = 222
    Height = 21
    MaxLength = 40
    TabOrder = 4
  end
  object DEDataIns: TdxDateEdit
    Left = 124
    Top = 510
    Width = 103
    TabOrder = 15
    Date = -700000
  end
  object ERuolo: TEdit
    Left = 3
    Top = 133
    Width = 194
    Height = 21
    TabStop = False
    Enabled = False
    MaxLength = 40
    TabOrder = 3
  end
  object QAltriDati_OLD: TQuery
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select EBC_ContattiClienti.*, Users.Nominativo'
      'from EBC_ContattiClienti,Users with (updlock)'
      'where EBC_ContattiClienti.IDUtente *= Users.ID'
      '  and EBC_ContattiClienti.ID=:xID')
    Left = 195
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'xID'
        ParamType = ptUnknown
      end>
  end
  object QAltriDati: TADOLinkedQuery
    Connection = Data.DB
    Parameters = <
      item
        Name = 'xID:'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select EBC_ContattiClienti.*, Users.Nominativo'
      'from EBC_ContattiClienti,Users'
      'where EBC_ContattiClienti.IDUtente *= Users.ID'
      '  and EBC_ContattiClienti.ID=:xID:'
      '')
    UseFilter = False
    Left = 195
    Top = 44
  end
  object QMansioni: TADOQuery
    Connection = Data.DB
    Parameters = <
      item
        Name = 'x'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select Descrizione '
      'from Mansioni'
      'where ID=:x')
    Left = 232
    Top = 168
  end
end
