object FattureForm: TFattureForm
  Left = 194
  Top = 119
  Width = 1042
  Height = 540
  Caption = 'Visualizzazione fatture'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel4: TPanel
    Left = 0
    Top = 0
    Width = 1034
    Height = 39
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object ToolbarButton971: TToolbarButton97
      Left = 4
      Top = 2
      Width = 102
      Height = 34
      DropdownMenu = PMStampa
      Caption = 'Stampa/esporta'
    end
    object BitBtn1: TBitBtn
      Left = 938
      Top = 4
      Width = 93
      Height = 32
      Anchors = [akTop, akRight]
      Caption = 'Esci'
      TabOrder = 0
      OnClick = BitBtn1Click
      Kind = bkOK
    end
  end
  object dxDBGrid1: TdxDBGrid
    Left = 0
    Top = 39
    Width = 1034
    Height = 474
    Bands = <
      item
      end>
    DefaultLayout = True
    HeaderPanelRowCount = 1
    KeyField = 'ID'
    ShowGroupPanel = True
    ShowSummaryFooter = True
    SummaryGroups = <
      item
        DefaultGroup = False
        SummaryItems = <
          item
            ColumnName = 'dxDBGrid1Stato'
            SummaryField = 'ImportoTotale'
            SummaryFormat = '#,###'
            SummaryType = cstSum
          end
          item
            ColumnName = 'dxDBGrid1Stato'
            SummaryField = 'SoloAccettati'
            SummaryFormat = '#,###'
            SummaryType = cstSum
          end>
        Name = 'Default'
      end>
    SummarySeparator = ', '
    Align = alClient
    TabOrder = 1
    OnMouseUp = dxDBGrid1MouseUp
    DataSource = DsQFatture
    Filter.Active = True
    Filter.Criteria = {00000000}
    LookAndFeel = lfFlat
    OptionsBehavior = [edgoAutoSort, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoStoreToRegistry, edgoTabThrough, edgoVertThrough]
    OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
    OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoRowSelect, edgoUseBitmap]
    OnCustomDrawFooter = dxDBGrid1CustomDrawFooter
    object dxDBGrid1Cliente: TdxDBGridMaskColumn
      Width = 131
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Cliente'
    end
    object dxDBGrid1ID: TdxDBGridMaskColumn
      Visible = False
      Width = 111
      BandIndex = 0
      RowIndex = 0
      FieldName = 'ID'
    end
    object dxDBGrid1Progressivo: TdxDBGridMaskColumn
      Width = 66
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Progressivo'
    end
    object dxDBGrid1Tipo: TdxDBGridMaskColumn
      Width = 56
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Tipo'
    end
    object dxDBGrid1RifProg: TdxDBGridMaskColumn
      Visible = False
      Width = 84
      BandIndex = 0
      RowIndex = 0
      FieldName = 'RifProg'
    end
    object dxDBGrid1IDCliente: TdxDBGridMaskColumn
      Visible = False
      Width = 111
      BandIndex = 0
      RowIndex = 0
      FieldName = 'IDCliente'
    end
    object dxDBGrid1Data: TdxDBGridDateColumn
      Width = 49
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Data'
    end
    object dxDBGrid1Decorrenza: TdxDBGridDateColumn
      Width = 93
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Decorrenza'
    end
    object dxDBGrid1Importo: TdxDBGridCurrencyColumn
      Width = 83
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Importo'
      SummaryFooterType = cstSum
      SummaryFooterField = 'Importo'
      SummaryFooterFormat = '� ,0.00;-� ,0.00'
      Nullable = False
    end
    object dxDBGrid1IVA: TdxDBGridMaskColumn
      Visible = False
      Width = 84
      BandIndex = 0
      RowIndex = 0
      FieldName = 'IVA'
    end
    object dxDBGrid1Totale: TdxDBGridCurrencyColumn
      Caption = 'con IVA'
      Width = 83
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Totale'
      SummaryFooterType = cstSum
      SummaryFooterField = 'Totale'
      SummaryFooterFormat = '� ,0.00;-� ,0.00'
      Nullable = False
    end
    object dxDBGrid1ModalitaPagamento: TdxDBGridMaskColumn
      Visible = False
      Width = 648
      BandIndex = 0
      RowIndex = 0
      FieldName = 'ModalitaPagamento'
    end
    object dxDBGrid1Assegno: TdxDBGridMaskColumn
      Visible = False
      Width = 330
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Assegno'
    end
    object dxDBGrid1Pagata: TdxDBGridCheckColumn
      Visible = False
      Width = 178
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Pagata'
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object dxDBGrid1ScadenzaPagamento: TdxDBGridDateColumn
      Caption = 'Scadenza'
      Width = 75
      BandIndex = 0
      RowIndex = 0
      FieldName = 'ScadenzaPagamento'
    end
    object dxDBGrid1PagataInData: TdxDBGridDateColumn
      Caption = 'Pagata il'
      Width = 63
      BandIndex = 0
      RowIndex = 0
      FieldName = 'PagataInData'
    end
    object dxDBGrid1AppoggioBancario: TdxDBGridMaskColumn
      Visible = False
      Width = 861
      BandIndex = 0
      RowIndex = 0
      FieldName = 'AppoggioBancario'
    end
    object dxDBGrid1IDRicerca: TdxDBGridMaskColumn
      Visible = False
      Width = 111
      BandIndex = 0
      RowIndex = 0
      FieldName = 'IDRicerca'
    end
    object dxDBGrid1Responsabile: TdxDBGridMaskColumn
      Visible = False
      Width = 861
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Responsabile'
    end
    object dxDBGrid1FlagFuoriAppIVA63372: TdxDBGridCheckColumn
      Visible = False
      Width = 178
      BandIndex = 0
      RowIndex = 0
      FieldName = 'FlagFuoriAppIVA63372'
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object dxDBGrid1RimbSpese: TdxDBGridCheckColumn
      Visible = False
      Width = 178
      BandIndex = 0
      RowIndex = 0
      FieldName = 'RimbSpese'
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object dxDBGrid1Column22: TdxDBGridColumn
      Caption = 'Filiale Origine'
      Width = 60
      BandIndex = 0
      RowIndex = 0
      FieldName = 'FilialeOrigine'
    end
    object dxDBGrid1Column23: TdxDBGridColumn
      Caption = 'Filiale Chiusura'
      Width = 51
      BandIndex = 0
      RowIndex = 0
      FieldName = 'FilialeChiusura'
    end
    object dxDBGrid1Column24: TdxDBGridColumn
      Caption = '% Fil Origine'
      Width = 46
      BandIndex = 0
      RowIndex = 0
      FieldName = 'PercOrigine'
    end
    object dxDBGrid1Column25: TdxDBGridColumn
      Caption = 'Imp. Fil. Origine'
      Width = 55
      BandIndex = 0
      RowIndex = 0
      FieldName = 'ImportoOrigine'
    end
    object dxDBGrid1Column26: TdxDBGridColumn
      Caption = '% Fil Chiusura'
      Width = 50
      BandIndex = 0
      RowIndex = 0
      FieldName = 'PercChiusura'
    end
    object dxDBGrid1Column27: TdxDBGridColumn
      Caption = 'Imp. Fil Chiusura'
      Width = 55
      BandIndex = 0
      RowIndex = 0
      FieldName = 'ImportoChiusura'
    end
  end
  object DsQFatture: TDataSource
    DataSet = QFatture
    Left = 144
    Top = 256
  end
  object PMStampa: TPopupMenu
    Images = MainForm.ImageList3
    Left = 144
    Top = 152
    object stampagriglia1: TMenuItem
      Caption = 'stampa griglia'
      ImageIndex = 18
      OnClick = stampagriglia1Click
    end
    object esportainExcel1: TMenuItem
      Caption = 'esporta in Excel'
      ImageIndex = 19
      OnClick = esportainExcel1Click
    end
    object esportainHTML1: TMenuItem
      Caption = 'esporta in HTML'
      ImageIndex = 20
      OnClick = esportainHTML1Click
    end
  end
  object dxPrinter1: TdxComponentPrinter
    CurrentLink = dxPrinter1Link1
    PreviewOptions.PreviewBoundsRect = {0000000000000000800700001A040000}
    Version = 0
    Left = 216
    Top = 176
    object dxPrinter1Link1: TdxDBGridReportLink
      Caption = 'dxPrinter1Link1'
      Component = dxDBGrid1
      DesignerHelpContext = 0
      PrinterPage.Background.Brush.Style = bsClear
      PrinterPage.Footer = 6350
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 25400
      PrinterPage.Margins.Left = 25400
      PrinterPage.Margins.Right = 25400
      PrinterPage.Margins.Top = 10000
      PrinterPage.PageFooter.Font.Charset = DEFAULT_CHARSET
      PrinterPage.PageFooter.Font.Color = clWindowText
      PrinterPage.PageFooter.Font.Height = -11
      PrinterPage.PageFooter.Font.Name = 'Tahoma'
      PrinterPage.PageFooter.Font.Style = []
      PrinterPage.PageHeader.Font.Charset = DEFAULT_CHARSET
      PrinterPage.PageHeader.Font.Color = clWindowText
      PrinterPage.PageHeader.Font.Height = -11
      PrinterPage.PageHeader.Font.Name = 'Tahoma'
      PrinterPage.PageHeader.Font.Style = []
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportTitle.Font.Charset = DEFAULT_CHARSET
      ReportTitle.Font.Color = clWindowText
      ReportTitle.Font.Height = -19
      ReportTitle.Font.Name = 'Times New Roman'
      ReportTitle.Font.Style = [fsBold]
      BandColor = clBtnFace
      BandFont.Charset = DEFAULT_CHARSET
      BandFont.Color = clWindowText
      BandFont.Height = -11
      BandFont.Name = 'MS Sans Serif'
      BandFont.Style = []
      Color = clWindow
      EvenFont.Charset = DEFAULT_CHARSET
      EvenFont.Color = clWindowText
      EvenFont.Height = -11
      EvenFont.Name = 'Times New Roman'
      EvenFont.Style = []
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      FooterFont.Charset = DEFAULT_CHARSET
      FooterFont.Color = clWindowText
      FooterFont.Height = -11
      FooterFont.Name = 'MS Sans Serif'
      FooterFont.Style = []
      GridLineColor = clBtnFace
      GroupNodeFont.Charset = DEFAULT_CHARSET
      GroupNodeFont.Color = clWindowText
      GroupNodeFont.Height = -11
      GroupNodeFont.Name = 'Times New Roman'
      GroupNodeFont.Style = []
      GroupNodeColor = clBtnFace
      HeaderColor = clBtnFace
      HeaderFont.Charset = DEFAULT_CHARSET
      HeaderFont.Color = clWindowText
      HeaderFont.Height = -11
      HeaderFont.Name = 'MS Sans Serif'
      HeaderFont.Style = []
      OddColor = clWindow
      OddFont.Charset = DEFAULT_CHARSET
      OddFont.Color = clWindowText
      OddFont.Height = -11
      OddFont.Name = 'Times New Roman'
      OddFont.Style = []
      Options = [tlpoBands, tlpoHeaders, tlpoFooters, tlpoRowFooters, tlpoPreview, tlpoPreviewGrid, tlpoGrid, tlpoFlatCheckMarks, tlpoImages, tlpoStateImages]
      PreviewFont.Charset = DEFAULT_CHARSET
      PreviewFont.Color = clBlue
      PreviewFont.Height = -11
      PreviewFont.Name = 'MS Sans Serif'
      PreviewFont.Style = []
      RowFooterColor = cl3DLight
      RowFooterFont.Charset = DEFAULT_CHARSET
      RowFooterFont.Color = clWindowText
      RowFooterFont.Height = -11
      RowFooterFont.Name = 'MS Sans Serif'
      RowFooterFont.Style = []
      BuiltInReportLink = True
    end
  end
  object QFatture: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'select distinct C.Descrizione Cliente, cf.Descrizione FilialeOri' +
        'gine, PercOrigine,Importo*PercOrigine/100 ImportoOrigine,'
      
        'cff.Descrizione FilialeChiusura, PercChiusura,Importo*PercChiusu' +
        'ra/100 ImportoChiusura,F.*'
      'from Fatture F'
      'join EBC_Clienti C on F.IDCliente = C.ID'
      'left  join Ebc_ClientiFiliali cf on f.idfilialeorigine=cf.id'
      'left  join Ebc_ClientiFiliali cff on f.idfilialechiusura=cff.id'
      'left join AccordiFiliali AF'
      'on AF.id=F.idaccordo'
      'order by F.ID desc')
    Left = 144
    Top = 224
    object QFattureCliente: TStringField
      FieldName = 'Cliente'
      Size = 50
    end
    object QFattureID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QFattureProgressivo: TStringField
      FieldName = 'Progressivo'
      Size = 50
    end
    object QFattureTipo: TStringField
      FieldName = 'Tipo'
    end
    object QFattureIDCliente: TIntegerField
      FieldName = 'IDCliente'
    end
    object QFattureData: TDateTimeField
      FieldName = 'Data'
    end
    object QFattureDecorrenza: TDateTimeField
      FieldName = 'Decorrenza'
    end
    object QFattureImporto: TFloatField
      FieldName = 'Importo'
    end
    object QFattureIVA: TFloatField
      FieldName = 'IVA'
    end
    object QFattureTotale: TFloatField
      FieldName = 'Totale'
    end
    object QFattureModalitaPagamento: TStringField
      FieldName = 'ModalitaPagamento'
      Size = 60
    end
    object QFattureAssegno: TStringField
      FieldName = 'Assegno'
      Size = 30
    end
    object QFattureScadenzaPagamento: TDateTimeField
      FieldName = 'ScadenzaPagamento'
    end
    object QFatturePagata: TBooleanField
      FieldName = 'Pagata'
    end
    object QFatturePagataInData: TDateTimeField
      FieldName = 'PagataInData'
    end
    object QFattureAppoggioBancario: TStringField
      FieldName = 'AppoggioBancario'
      Size = 80
    end
    object QFattureIDRicerca: TIntegerField
      FieldName = 'IDRicerca'
    end
    object QFattureResponsabile: TStringField
      FieldName = 'Responsabile'
      FixedChar = True
      Size = 80
    end
    object QFattureFlagFuoriAppIVA63372: TBooleanField
      FieldName = 'FlagFuoriAppIVA63372'
    end
    object QFattureRimbSpese: TBooleanField
      FieldName = 'RimbSpese'
    end
    object QFattureFilialeOrigine: TStringField
      FieldName = 'FilialeOrigine'
      Size = 255
    end
    object QFattureFilialeChiusura: TStringField
      FieldName = 'FilialeChiusura'
      Size = 255
    end
    object QFatturePercOrigine: TFloatField
      FieldName = 'PercOrigine'
    end
    object QFattureImportoOrigine: TFloatField
      FieldName = 'ImportoOrigine'
      ReadOnly = True
    end
    object QFatturePercChiusura: TFloatField
      FieldName = 'PercChiusura'
    end
    object QFattureImportoChiusura: TFloatField
      FieldName = 'ImportoChiusura'
      ReadOnly = True
    end
    object QFattureRifProg: TStringField
      FieldName = 'RifProg'
      Size = 50
    end
  end
  object qAccordiFilialiLK: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from accordifiliali')
    Left = 264
    Top = 256
    object qAccordiFilialiLKID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object qAccordiFilialiLKDescrizione: TStringField
      FieldName = 'Descrizione'
      Size = 100
    end
    object qAccordiFilialiLKPercOrigine: TFloatField
      FieldName = 'PercOrigine'
    end
    object qAccordiFilialiLKPercChiusura: TFloatField
      FieldName = 'PercChiusura'
    end
    object qAccordiFilialiLKAttivo: TBooleanField
      FieldName = 'Attivo'
    end
  end
end
