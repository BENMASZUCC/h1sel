object DefCampiPersForm: TDefCampiPersForm
  Left = 439
  Top = 146
  ActiveControl = ENomeCampo
  BorderStyle = bsDialog
  Caption = 'Definizione campo personalizzato'
  ClientHeight = 238
  ClientWidth = 320
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 9
    Top = 56
    Width = 82
    Height = 13
    Caption = 'Nome del campo:'
  end
  object Label2: TLabel
    Left = 44
    Top = 94
    Width = 47
    Height = 13
    Caption = 'Etichetta:'
  end
  object Label3: TLabel
    Left = 67
    Top = 133
    Width = 24
    Height = 13
    Caption = 'Tipo:'
  end
  object Label4: TLabel
    Left = 210
    Top = 133
    Width = 55
    Height = 13
    Caption = 'Lunghezza:'
  end
  object Label5: TLabel
    Left = 64
    Top = 160
    Width = 27
    Height = 13
    Caption = 'Note:'
  end
  object Label6: TLabel
    Left = 106
    Top = 76
    Width = 204
    Height = 11
    Caption = '(senza spazi: � il nome della colonna nella tabella)'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -9
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label7: TLabel
    Left = 106
    Top = 113
    Width = 91
    Height = 11
    Caption = '(massimo 30 caratteri)'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -9
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Bevel1: TBevel
    Left = 97
    Top = 53
    Width = 2
    Height = 156
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 320
    Height = 41
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 6
    object BitBtn1: TBitBtn
      Left = 141
      Top = 3
      Width = 88
      Height = 35
      Anchors = [akTop, akRight]
      TabOrder = 0
      OnClick = BitBtn1Click
      Kind = bkOK
    end
    object BitBtn2: TBitBtn
      Left = 229
      Top = 3
      Width = 88
      Height = 35
      Anchors = [akTop, akRight]
      Caption = 'Annulla'
      TabOrder = 1
      Kind = bkCancel
    end
  end
  object ENomeCampo: TEdit
    Left = 105
    Top = 53
    Width = 104
    Height = 21
    TabOrder = 0
    OnExit = ENomeCampoExit
  end
  object ELabelCampo: TEdit
    Left = 105
    Top = 91
    Width = 144
    Height = 21
    MaxLength = 30
    TabOrder = 1
  end
  object CBTipoCampo: TComboBox
    Left = 106
    Top = 130
    Width = 95
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 2
    OnChange = CBTipoCampoChange
    Items.Strings = (
      'stringa'
      'data'
      'booleano'
      'intero'
      'decimale')
  end
  object SELunghezza: TdxSpinEdit
    Left = 270
    Top = 130
    Width = 41
    TabOrder = 3
    Value = 1
    StoredValues = 48
  end
  object MNote: TdxMemo
    Left = 105
    Top = 158
    Width = 206
    TabOrder = 4
    Height = 51
  end
  object CBRicerche: TCheckBox
    Left = 5
    Top = 216
    Width = 174
    Height = 17
    Caption = 'Inserisci come criterio di ricerca'
    Checked = True
    State = cbChecked
    TabOrder = 5
  end
end
