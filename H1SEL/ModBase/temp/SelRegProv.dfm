object SelRegProvForm: TSelRegProvForm
  Left = 313
  Top = 133
  Width = 337
  Height = 461
  Caption = 'Selezione Regione e Provincia'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 174
    Top = 41
    Width = 3
    Height = 393
    Cursor = crHSplit
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 329
    Height = 41
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object BitBtn1: TBitBtn
      Left = 135
      Top = 3
      Width = 95
      Height = 34
      Anchors = [akTop, akRight]
      TabOrder = 0
      OnClick = BitBtn1Click
      Kind = bkOK
    end
    object BitBtn2: TBitBtn
      Left = 231
      Top = 3
      Width = 95
      Height = 34
      Anchors = [akTop, akRight]
      Caption = 'Annulla'
      TabOrder = 1
      Kind = bkCancel
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 41
    Width = 174
    Height = 393
    Align = alLeft
    BevelOuter = bvLowered
    Caption = 'Panel2'
    TabOrder = 1
    object dxDBGrid1: TdxDBGrid
      Left = 1
      Top = 1
      Width = 172
      Height = 391
      Bands = <
        item
        end>
      DefaultLayout = True
      HeaderPanelRowCount = 1
      SummaryGroups = <>
      SummarySeparator = ', '
      Align = alClient
      TabOrder = 0
      DataSource = DsQReg
      Filter.Criteria = {00000000}
      OptionsBehavior = [edgoAutoSearch, edgoAutoSort, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoTabThrough, edgoVertThrough]
      OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoIndicator, edgoUseBitmap]
      object dxDBGrid1ID: TdxDBGridMaskColumn
        Visible = False
        BandIndex = 0
        RowIndex = 0
        FieldName = 'ID'
      end
      object dxDBGrid1Regione: TdxDBGridMaskColumn
        BandIndex = 0
        RowIndex = 0
        FieldName = 'Regione'
      end
    end
  end
  object Panel3: TPanel
    Left = 177
    Top = 41
    Width = 152
    Height = 393
    Align = alClient
    BevelOuter = bvLowered
    Caption = 'Panel3'
    TabOrder = 2
    object Panel4: TPanel
      Left = 1
      Top = 1
      Width = 150
      Height = 27
      Align = alTop
      BevelOuter = bvLowered
      TabOrder = 0
      object CBAncheProv: TCheckBox
        Left = 5
        Top = 6
        Width = 139
        Height = 17
        Caption = 'Inserisci anche provincia'
        Checked = True
        State = cbChecked
        TabOrder = 0
        OnClick = CBAncheProvClick
      end
    end
    object dxDBGrid2: TdxDBGrid
      Left = 1
      Top = 28
      Width = 150
      Height = 364
      Bands = <
        item
        end>
      DefaultLayout = True
      HeaderPanelRowCount = 1
      SummaryGroups = <>
      SummarySeparator = ', '
      Align = alClient
      TabOrder = 1
      DataSource = DsQProv
      Filter.Criteria = {00000000}
      OptionsBehavior = [edgoAutoSearch, edgoAutoSort, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoMultiSelect, edgoTabThrough, edgoVertThrough]
      OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoIndicator, edgoUseBitmap]
      object dxDBGrid2Prov: TdxDBGridMaskColumn
        BandIndex = 0
        RowIndex = 0
        FieldName = 'Prov'
      end
      object dxDBGrid2Regione: TdxDBGridMaskColumn
        Visible = False
        BandIndex = 0
        RowIndex = 0
        FieldName = 'Regione'
      end
    end
  end
  object QReg: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select ID, Regione, CodRegioneH1'
      'from TabRegioni'
      'order by Regione')
    Left = 40
    Top = 144
    object QRegID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QRegRegione: TStringField
      FieldName = 'Regione'
      Size = 100
    end
    object QRegCodRegioneH1: TStringField
      FieldName = 'CodRegioneH1'
      Size = 3
    end
  end
  object DsQReg: TDataSource
    DataSet = QReg
    Left = 40
    Top = 176
  end
  object QProv: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    DataSource = DsQReg
    Parameters = <
      item
        Name = 'CodRegioneH1'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 3
        Value = 'ABR'
      end>
    SQL.Strings = (
      'select distinct C.Prov, C.Regione'
      'from TabCom C'
      'join TabRegioni R on C.Regione = R.CodRegioneH1'
      'where C.Regione =:CodRegioneH1'
      'order by C.Prov')
    Left = 232
    Top = 136
    object QProvProv: TStringField
      FieldName = 'Prov'
      Size = 2
    end
    object QProvRegione: TStringField
      FieldName = 'Regione'
      Size = 3
    end
  end
  object DsQProv: TDataSource
    DataSet = QProv
    Left = 232
    Top = 168
  end
end
