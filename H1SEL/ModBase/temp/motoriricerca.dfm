object formMotoriRicerca: TformMotoriRicerca
  Left = 264
  Top = 267
  Width = 469
  Height = 178
  Caption = 'Modifica/Inserisci Link e Note Personali'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 16
    Width = 73
    Height = 13
    Caption = 'Link Personale '
  end
  object Label2: TLabel
    Left = 16
    Top = 48
    Width = 69
    Height = 13
    Caption = 'Note Personali'
  end
  object Edit1: TEdit
    Left = 96
    Top = 8
    Width = 249
    Height = 21
    TabOrder = 0
    Text = 'Edit1'
  end
  object Memo1: TMemo
    Left = 96
    Top = 40
    Width = 249
    Height = 89
    Lines.Strings = (
      'Memo1')
    TabOrder = 1
  end
  object BitBtn1: TBitBtn
    Left = 376
    Top = 104
    Width = 75
    Height = 25
    Caption = 'Salva'
    ModalResult = 1
    TabOrder = 2
  end
end
