object TestataForm: TTestataForm
  Left = 219
  Top = 103
  ActiveControl = EDenom
  BorderStyle = bsDialog
  Caption = 'TestataForm'
  ClientHeight = 83
  ClientWidth = 426
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 5
    Top = 8
    Width = 63
    Height = 13
    Caption = 'Nome testata'
  end
  object Label2: TLabel
    Left = 25
    Top = 32
    Width = 43
    Height = 13
    Caption = 'Tipologia'
  end
  object Label3: TLabel
    Left = 45
    Top = 56
    Width = 23
    Height = 13
    Caption = 'Note'
  end
  object BitBtn1: TBitBtn
    Left = 332
    Top = 4
    Width = 93
    Height = 37
    TabOrder = 3
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 332
    Top = 44
    Width = 93
    Height = 37
    Caption = 'Annulla'
    TabOrder = 4
    Kind = bkCancel
  end
  object EDenom: TEdit
    Left = 77
    Top = 5
    Width = 249
    Height = 21
    MaxLength = 50
    TabOrder = 0
  end
  object ETipo: TEdit
    Left = 77
    Top = 29
    Width = 161
    Height = 21
    MaxLength = 15
    TabOrder = 1
  end
  object ENote: TEdit
    Left = 77
    Top = 53
    Width = 249
    Height = 21
    MaxLength = 50
    TabOrder = 2
  end
end
