object SettaColoriGrigliaCandForm: TSettaColoriGrigliaCandForm
  Left = 292
  Top = 224
  Width = 395
  Height = 339
  Caption = 'Impostazione colori'
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxDBGrid1: TdxDBGrid
    Left = 0
    Top = 41
    Width = 387
    Height = 271
    Bands = <
      item
      end>
    DefaultLayout = True
    HeaderPanelRowCount = 1
    KeyField = 'ID'
    SummaryGroups = <>
    SummarySeparator = ', '
    Align = alClient
    TabOrder = 0
    DataSource = DsColori
    Filter.Criteria = {00000000}
    OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
    OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoIndicator, edgoUseBitmap]
    OnCustomDrawCell = dxDBGrid1CustomDrawCell
    object dxDBGrid1ID: TdxDBGridMaskColumn
      Visible = False
      Width = 54
      BandIndex = 0
      RowIndex = 0
      FieldName = 'ID'
    end
    object dxDBGrid1IDUtente: TdxDBGridMaskColumn
      Visible = False
      Width = 54
      BandIndex = 0
      RowIndex = 0
      FieldName = 'IDUtente'
    end
    object dxDBGrid1IDEvento: TdxDBGridMaskColumn
      Visible = False
      Width = 54
      BandIndex = 0
      RowIndex = 0
      FieldName = 'IDEvento'
    end
    object dxDBGrid1Utente: TdxDBGridLookupColumn
      Visible = False
      Width = 120
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Utente'
    end
    object dxDBGrid1Evento: TdxDBGridLookupColumn
      Width = 174
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Evento'
    end
    object dxDBGrid1Colore: TdxDBGridMaskColumn
      Visible = False
      Width = 64
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Colore'
    end
    object dxDBGrid1Note: TdxDBGridMemoColumn
      Visible = False
      Width = 54
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Note'
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 387
    Height = 41
    Align = alTop
    BevelOuter = bvLowered
    Constraints.MinWidth = 387
    TabOrder = 1
    object BNew: TToolbarButton97
      Left = 2
      Top = 2
      Width = 48
      Height = 36
      Caption = 'Nuovo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Layout = blGlyphTop
      NumGlyphs = 2
      Opaque = False
      ParentFont = False
      Spacing = 1
      WordWrap = True
      OnClick = BNewClick
    end
    object BDel: TToolbarButton97
      Left = 50
      Top = 2
      Width = 48
      Height = 36
      Caption = 'Elimina'
      Layout = blGlyphTop
      NumGlyphs = 2
      Opaque = False
      Spacing = 1
      WordWrap = True
      OnClick = BDelClick
    end
    object BOK: TToolbarButton97
      Left = 98
      Top = 2
      Width = 48
      Height = 36
      Caption = 'OK'
      Enabled = False
      Layout = blGlyphTop
      NumGlyphs = 2
      Opaque = False
      Spacing = 1
      WordWrap = True
      OnClick = BOKClick
    end
    object BCan: TToolbarButton97
      Left = 146
      Top = 2
      Width = 48
      Height = 36
      Caption = 'Annulla'
      Enabled = False
      Layout = blGlyphTop
      NumGlyphs = 2
      Opaque = False
      Spacing = 1
      WordWrap = True
      OnClick = BCanClick
    end
    object ToolbarButton971: TToolbarButton97
      Left = 217
      Top = 2
      Width = 59
      Height = 36
      Caption = 'Imposta colore'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clPurple
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Layout = blGlyphTop
      NumGlyphs = 2
      Opaque = False
      ParentFont = False
      Spacing = 1
      WordWrap = True
      OnClick = ToolbarButton971Click
    end
    object BitBtn2: TBitBtn
      Left = 305
      Top = 3
      Width = 77
      Height = 35
      Anchors = [akTop, akRight]
      Caption = 'Esci'
      TabOrder = 0
      Kind = bkOK
    end
  end
  object ColorDialog1: TColorDialog
    Ctl3D = True
    Left = 416
    Top = 104
  end
  object DsColori: TDataSource
    DataSet = QColori
    OnStateChange = DsColoriStateChange
    Left = 160
    Top = 192
  end
  object QColori: TADOQuery
    Connection = Data.DB
    AfterInsert = QColoriAfterInsert
    Parameters = <>
    SQL.Strings = (
      'select *'
      'from EBC_Ricerche_ColoriGriglia')
    Left = 160
    Top = 160
    object QColoriID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QColoriIDUtente: TIntegerField
      FieldName = 'IDUtente'
    end
    object QColoriIDEvento: TIntegerField
      FieldName = 'IDEvento'
    end
    object QColoriColore: TIntegerField
      FieldName = 'Colore'
    end
    object QColoriNote: TMemoField
      FieldName = 'Note'
      BlobType = ftMemo
    end
    object QColoriUtente: TStringField
      FieldKind = fkLookup
      FieldName = 'Utente'
      LookupDataSet = QUsersLK
      LookupKeyFields = 'ID'
      LookupResultField = 'Nominativo'
      KeyFields = 'IDUtente'
      Size = 30
      Lookup = True
    end
    object QColoriEvento: TStringField
      FieldKind = fkLookup
      FieldName = 'Evento'
      LookupDataSet = QEventiLK
      LookupKeyFields = 'ID'
      LookupResultField = 'Evento'
      KeyFields = 'IDEvento'
      Size = 30
      Lookup = True
    end
  end
  object QUsersLK: TADOQuery
    Connection = Data.DB
    Parameters = <>
    SQL.Strings = (
      'select ID,Nominativo'
      'from Users')
    Left = 160
    Top = 248
    object QUsersLKID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QUsersLKNominativo: TStringField
      FieldName = 'Nominativo'
      Size = 30
    end
  end
  object QEventiLK: TADOQuery
    Connection = Data.DB
    Parameters = <>
    SQL.Strings = (
      'select ID, Evento'
      'from EBC_Eventi'
      
        '--where IDDaStato in (select ID from EBC_Stati where IDTipoStato' +
        ' =1)'
      'order by Evento')
    Left = 192
    Top = 248
    object QEventiLKID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QEventiLKEvento: TStringField
      FieldName = 'Evento'
      Size = 30
    end
  end
end
