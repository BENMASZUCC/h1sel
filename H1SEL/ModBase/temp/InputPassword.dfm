object InputPasswordForm: TInputPasswordForm
  Left = 312
  Top = 106
  ActiveControl = EPassword
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'Inserimento password'
  ClientHeight = 67
  ClientWidth = 206
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 6
    Top = 9
    Width = 46
    Height = 13
    Caption = 'Password'
  end
  object BitBtn1: TBitBtn
    Left = 32
    Top = 34
    Width = 85
    Height = 29
    TabOrder = 1
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 118
    Top = 34
    Width = 85
    Height = 29
    Caption = 'Annulla'
    TabOrder = 2
    Kind = bkCancel
  end
  object EPassword: TEdit
    Left = 60
    Top = 6
    Width = 142
    Height = 21
    PasswordChar = '*'
    TabOrder = 0
    Text = 'EPassword'
  end
end
