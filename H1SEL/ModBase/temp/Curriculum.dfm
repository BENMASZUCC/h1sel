€
 TCURRICULUMFORM 0mя  TPF0TCurriculumFormCurriculumFormLeft_Topќ WidthЫHeight®HelpContextЌЖ CaptionCurriculum VitaeColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.Style OldCreateOrder	PositionpoScreenCenterOnClose	FormCloseOnShowFormShowPixelsPerInch`
TextHeight TLabelLabel43Leftј TopЄWidthDHeightCaptionNomeEdizione  TLabelLabel68LeftiTopbWidth&HeightCaptionBenefits  TPageControlPageControl1Left Top+WidthУHeightb
ActivePage
TSCVespLavAlignalClientTabOrder OnChangePageControl1Change
OnChangingPageControl1Changing 	TTabSheetTSCVAltriDatiHelpContextЌЖ Caption
Altri dati TLabelLabel47Left)TopЫWidth¶ HeightCaption&Disponibilita movimento sul territorio  TLabelLabel45LeftTop^Width;HeightCaptionOrario ideale  TLabelLabel54Left)ToprWidthoHeightCaptionProvincia/e di interesseFocusControlDBEdit40  TLabelLabel58Left)Top(WidthGHeightCaptionInquadramentoFocusControlDBEdit42  TSpeedButtonBAziendaLeftTopWidthHeightHintVedi/seleziona Azienda cliente
Glyph.Data
z  v  BMv      v   (                                    А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 33      33wwwwww33€€€€р33?€??ч33  р33wssw733€€€€р33?у€?ч33 р р33w7sw733€€€€р33у3?уч33 €€ рр33w?уwч730ы рр€р373w77уч3∞њ∞рр3s73s7770ыыры€р7у373s370њњ∞€€р7у3s73?ч ыыы€  wу33уwwањњ∞ €рw€37w?7оыыwуs?ssоањњ € 3ww€€w€w3оо    3wwwwwws3	NumGlyphsParentShowHintShowHint	OnClickBAziendaClick  TLabelLabel52Left)TopWidth&HeightCaptionBenefitsFocusControlDBEdit31  TLabelLabel62LeftTop Width4HeightCaptionNazionalitа  TLabelLabel57LeftTop+WidthbHeightCaptionNumero passaporto:   TLabelLabel65Left(TopKWidthOHeightCaptionSettore contrattoFocusControlDBLookupComboBox4  TSpeedButtonSpeedButton2LeftiTopYWidthHeightHint"Gestione Tabella tipi retribuzioni
Glyph.Data
:  6  BM6      6  (                                Д   €   ДДД ∆∆∆ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€                              ParentShowHintShowHint	OnClickSpeedButton2Click  TLabellNoteLeft)TopЏ WidthHeightCaptionNoteVisible  	TGroupBox	GroupBox1LeftЭ Top€Width≈ Height'CaptionMezziTabOrder TLabelLabel1LeftП TopWidthHeightCaptionTipo:FocusControlDBEdit4  TDBCheckBoxDBCheckBox1LeftUTopWidth;HeightCaptionPatente	DataFieldPatente
DataSourcedsAnagAltriDatiTabOrderValueCheckedTrueValueUncheckedFalse  TDBEditDBEdit4Left™ TopWidthHeight	DataFieldTipoPatente
DataSourcedsAnagAltriDatiTabOrder  TDBCheckBoxDBCheckBox2LeftTopWidthKHeightCaption
Automunito	DataFieldDispAuto
DataSourcedsAnagAltriDatiTabOrder ValueCheckedTrueValueUncheckedFalse   	TGroupBox	GroupBox2LeftTop=Width^HeightBCaptionServizio Militare di levaTabOrder TLabelLabel2Left
TopWidthHeightCaptionStato  TLabelLabel3LeftЬ TopWidth#HeightCaptionPresso:FocusControlDBEdit7  TLabelLabel4Left	Top*Width'HeightCaptionPeriodo:FocusControlDBEdit8  TLabelLabel5Leftґ Top+Width HeightCaptionGrado:FocusControlDBEdit9  TDBEditDBEdit7Left≈ TopWidthТ Height	DataFieldServizioMilitareDove
DataSourcedsAnagAltriDatiTabOrder  TDBEditDBEdit8Left7Top(WidthzHeight	DataFieldServizioMilitareQuando
DataSourcedsAnagAltriDatiTabOrder  TDBEditDBEdit9Left№ Top(Width{Height	DataFieldServizioMilitareGrado
DataSourcedsAnagAltriDatiTabOrder  TDBComboBoxDBComboBox1Left7TopWidth^Height	DataFieldServizioMilitareStato
DataSourcedsAnagAltriDati
ItemHeightItems.Stringsassoltoda assolvereesentein svolgimento TabOrder    TPanelPanCatProtetteLeftTopВ Width^Height-
BevelInnerbvRaised
BevelOuter	bvLoweredTabOrder TLabelLabel6LeftTopWidthTHeightCaptionCategorie protette  TLabelLabel7LeftЏ TopWidthHeightCaptionTipoVisible  TLabelLabel8LeftЂ TopWidthHeightCaption%FocusControlDBEdit11  TDBEditDBEdit11Left© TopWidth-Height	DataFieldPercentualeInvalidita
DataSourcedsAnagAltriDati	MaxLengthTabOrder  TDBComboBoxDBComboBox2LeftTopWidthЯ Height	DataFieldCateogorieProtette
DataSourcedsAnagAltriDati
ItemHeightItems.Stringsiscrittonon iscritto TabOrder   TDBComboBoxDBComboBox3Left№ TopWidth~Height	DataField
Invalidita
DataSourcedsAnagAltriDati
ItemHeightItems.StringsDisabileInvalido del LavoroOrfano di lavoro o di guerra TabOrderVisible   TDBLookupComboBoxDBLookupComboBox2Left)TopЂWidthXHeight	DataFieldDispMovimento
DataSourcedsAnagAltriDatiTabOrder  TDBEditDBEdit38LeftETopZWidthё Height	DataFieldOrarioIdeale
DataSourcedsAnagAltriDatiTabOrder  	TGroupBox	GroupBox5LeftTopxWidth!HeightБ CaptionInformazioni C.V.TabOrder TLabelLabel51Left≤ TopWidthHHeightCaptionInserito in data:Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel53LeftTopWidth;HeightCaptionProvenienza  TSpeedButton
BTabProvCVLeftФ TopWidthHeightHinttabella provenienza
Glyph.Data
z  v  BMv      v   (                                       А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 33      33wwwwww33€€€€р33?€??ч33  р33wssw733€€€€р33?у€?ч33 р р33w7sw733€€€€р33у3?уч33 €€ рр33w?уwч730ы рр€р373w77уч3∞њ∞рр3s73s7770ыыры€р7у373s370њњ∞€€р7у3s73?ч ыыы€  wу33уwwањњ∞ €рw€37w?7оыыwуs?ssоањњ € 3ww€€w€w3оо    3wwwwwws3	NumGlyphsParentShowHintShowHint	OnClickBTabProvCVClick  TLabelLDataRealeCVLeft≥ TopUWidth2HeightCaptiondata reale:Font.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel37LeftDTopTWidth`HeightCaptionData aggiornamento  TLabelLabel74LeftTop8Width:HeightCaptionDettaglio provenienzaWordWrap	  TDbDateEdit97DbDateEdit971Left± TopWidthjHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder ColorCalendar.ColorValidclBlueDateєО  DayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanClear
doCanPopup
doIsMaskeddoShowCanceldoShowToday OnChangeDbDateEdit971Change	DataFieldCVinseritoInData
DataSourceData.DsAnagrafica  TDBComboBox
DBCBProvCVLeftTopWidthК Height	DataFieldCVProvenienza
DataSourcedsAnagAltriDati
ItemHeightTabOrder  TDbDateEdit97DbDEDataRealeCVLeft≤ TopdWidthjHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderColorCalendar.ColorValidclBlueDateєО  DayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanClear
doCanPopup
doIsMaskeddoShowCanceldoShowToday 	DataFieldCVDataReale
DataSourceData.DsAnagrafica  TDBCheckBoxDbCbNoCVLeftTopfWidth7HeightCaptionNo CV	DataFieldNoCV
DataSourcedsAnagAltriDatiTabOrderValueCheckedTrueValueUncheckedFalseVisible  TDbDateEdit97DbDateEdit973LeftDTopeWidthjHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderColorCalendar.ColorValidclBlueDateєО  DayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanClear
doCanPopup
doIsMaskeddoShowCanceldoShowToday 	DataField	CVDataAgg
DataSourceData.DsAnagrafica  TDBEditDBEdit57LeftOTop:Width  Height	DataFieldSpecCVProvenienza
DataSourcedsAnagAltriDatiTabOrder   	TGroupBox	GroupBox6LefthTop€WidthHeight± TabOrder TLabelLabel48LeftTop6WidthtHeightCaptionin Cigs da almeno 6 mesi  TLabelLabel49LeftToppWidthЩ HeightCaption con trattam.spec. di disoccupaz.  TDBCheckBoxDBCheckBox4LeftTop
WidthaHeightCaptionin mobilitа	DataFieldMobilita
DataSourcedsAnagAltriDatiTabOrder ValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox5LeftTopWidthyHeightCaptionin cassa integrazione	DataFieldCassaIntegrazione
DataSourcedsAnagAltriDatiTabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox6LeftTop~WidthaHeightCaptiondisp. Part-time	DataFieldDispPartTime
DataSourcedsAnagAltriDatiTabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox7LeftTopН WidthaHeightCaptiondisp. Interinale	DataFieldDispInterinale
DataSourcedsAnagAltriDatiTabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox11LeftTop'Widthґ HeightCaption CIGS da almeno 3 anni da imprese	DataFieldCassaIntegrStraord3
DataSourcedsAnagAltriDatiTabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox12LeftTopDWidthЬ HeightCaptionCIGS da almeno 24 mesi	DataFieldCassaIntegrStraord24
DataSourcedsAnagAltriDatiTabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox13LeftTopSWidth± HeightCaptionDisoccupato da almeno 24 mesi	DataFieldDisoccupato24
DataSourcedsAnagAltriDatiTabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox14LeftTopaWidthі HeightCaptionDisoccupato da almeno 12 mesi	DataFieldDisoccupato12
DataSourcedsAnagAltriDatiTabOrderValueCheckedTrueValueUncheckedFalse   TDBCheckBoxDBCheckBox9Left)TopWidthД HeightCaptionAbilitazione professione	DataFieldAbilitazProfessioni
DataSourcedsAnagAltriDatiTabOrderValueCheckedTrueValueUncheckedFalse  TDBEditDBEdit40Left)TopАWidthXHeight	DataFieldProvInteresse
DataSourcedsAnagAltriDatiTabOrder  TDBEditDBEdit42Left)Top6WidthXHeight	DataFieldInquadramento
DataSourcedsAnagAltriDatiTabOrder  	TGroupBox	GroupBox7LeftTopWidthД Height6CaptionRiferimento annuncioTabOrder
 TSpeedButtonSpeedButton1LeftTopWidthwHeightHintTrova annuncioCaptionAnnunci associati
Glyph.Data
z  v  BMv      v   (                                       А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 33      33wwwwww33€€€€р33?€??ч33  р33wssw733€€€€р33?у€?ч33 р р33w7sw733€€€€р33у3?уч33 €€ рр33w?уwч730ы рр€р373w77уч3∞њ∞рр3s73s7770ыыры€р7у373s370њњ∞€€р7у3s73?ч ыыы€  wу33уwwањњ∞ €рw€37w?7оыыwуs?ssоањњ € 3ww€€w€w3оо    3wwwwwws3	NumGlyphsParentShowHintShowHint	OnClickSpeedButton1Click   TRadioGroupRGProprietaLeftМ TopWidthxHeight6CaptionProprietа CV	ItemIndex Items.Stringsnostroazienda cliente TabOrderVisibleOnClickRGProprietaClick  TDBEditDBEdit31Left(TopWidthYHeight	DataFieldBenefits
DataSourcedsAnagAltriDatiTabOrder	  TDBEditDBEdit46LeftTopWidthХ Height	DataFieldNazionalita
DataSourcedsAnagAltriDatiTabOrder   TDBCheckBoxDBCheckBox10Left∞Top
WidthuHeightCaptionTemporary Manager	DataFieldTemporaryMG
DataSourcedsAnagAltriDatiTabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox15Left)Top“Widthб HeightCaption%Disponibilitа a trasferte giornaliere	DataFieldDispTraferteGiorn
DataSourcedsAnagAltriDatiTabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox16Left)TopгWidthў HeightCaptionDisponibilitа al pernottamento	DataFieldDispPernottamento
DataSourcedsAnagAltriDatiTabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox17Left)TopфWidthў HeightCaptionDisponibilitа al trasferimento	DataFieldDisptrasferimento
DataSourcedsAnagAltriDatiTabOrderValueCheckedTrueValueUncheckedFalse  фTMultiDBCheckBoxFrameMultiDBCheckBoxFrame1LeftTop∞ WidthHeight§ TabOrder сTRxCheckListBoxCLBoxWidthHeightП   сTPanel	PanTitoloWidth
Font.StylefsBold   сTPanelPanel1LeftHeightП  сTSpeedButtonSpeedButton3OnClick&MultiDBCheckBoxFrame1SpeedButton3Click    TButtonButton1Left–Top„WidthKHeightCaptionButton1TabOrderVisible  TDBEditDBEdit50LeftsTop(Widthн Height	DataFieldNumPassaporto
DataSourcedsAnagAltriDatiTabOrder  	TGroupBox	GroupBox8Left(Top≤ Width\HeightNCaptionRetribuzioneFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder TLabelLabel59LeftTopWidth)HeightCaption
Fissa a.l.FocusControlDBEdit43Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel61LeftЏ TopWidthNHeightCaptionTipo retribuzioneFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFont  TSpeedButtonSpeedButton3Left@TopWidthHeightHint"Gestione Tabella tipi retribuzioni
Glyph.Data
:  6  BM6      6  (                                Д   €   ДДД ∆∆∆ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€                              ParentShowHintShowHint	OnClickSpeedButton3Click  TLabelLabel63LeftTop9Width9HeightCaptionVariabile a.l.Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel64Leftі Top9WidthHeightCaptionTotaleFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFont  TDBEditDBEdit43LeftTopWidthћ Height	DataFieldRetribuzione
DataSourcedsAnagAltriDatiFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   TDBLookupComboBoxDBLookupComboBox1Leftџ TopWidthcHeight	DataField	TipoRetib
DataSourcedsAnagAltriDatiFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBEditDBEdit51LeftHTop5WidthiHeight	DataFieldRetribVariabile
DataSourcedsAnagAltriDatiFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBEditDBEdit52Left№ Top5WidthwHeight	DataFieldRetribTotale
DataSourcedsAnagAltriDatiFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TdxDBCurrencyEditRetribIntermediaLeftTop(WidthsFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderVisible
DataSourcedsAnagAltriDatiDisplayFormat,0.00;-,0.00NullableStoredValues   TdxDBCurrencyEditRetribVarIntermediaLeftMTop@WidthbFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderVisible
DataSourcedsAnagAltriDatiNullable  TdxDBCurrencyEditRetribTotIntermediaLeftµ Top@WidthCFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderVisible
DataSourcedsAnagAltriDatiNullable   TDBLookupComboBoxDBLookupComboBox4Left)TopZWidth8Height	DataFieldContrattoSettore
DataSourcedsAnagAltriDatiTabOrder  TDBMemoeNotecvLeft)Topу WidthPHeight	DataFieldNoteAltriDati
DataSourcedsAnagAltriDati	MaxLength2TabOrderVisible  TDBCheckBoxDBCheckBox24Left)TopЅWidthў HeightCaption)Disponibilitа avvicinamento a Sede lavoro	DataFieldDispAvvicinSede
DataSourcedsAnagAltriDatiTabOrderValueCheckedTrueValueUncheckedFalse   	TTabSheetTSCVTitoliStudioHelpContextќЖ Caption
Accademici TDBCtrlGridDBCtrlGrid1LeftTop!WidthБHeight’AllowInsertColCount
DataSourceData.DsTitoliStudioPanelHeightС 
PanelWidthqTabOrder RowCountSelectedColorclInfoBk	ShowFocus TLabelLabel9LeftXTopWidthyHeightCaptionIstituto/Scuola/UniversitаFocusControlDBEdit6  TLabelLabel10LeftTopWidth/HeightCaption	Votazione  TLabelLabel12LeftTop)WidthNHeightCaptionSpecializzazioneFocusControlDBEdit13  TLabelLabel16Left± Top)WidthxHeightCaptionData/periodo conseguim.FocusControlDBEdit15  TLabelLabel11LeftTopWidthHeightCaptionTitolo  TLabelLabel14Left,Top)WidthwHeightCaptionAbilitazione ProfessionaleFocusControlDBEdit14  TLabelLabel72LeftTopPWidth'HeightCaptionNazione  TLabelLabel73LeftЄ TopaWidthHeightCaptionNote  TDBEditDBEdit6LeftXTopWidth	Height	DataFieldLuogoConseguimento
DataSourceData.DsTitoliStudioTabOrder  TDBCheckBoxDBCheckBox3LeftѓTop'WidthTHeightCaptionLaurea breve	DataFieldLaureaBreve
DataSourceData.DsTitoliStudioTabOrderValueCheckedTrueValueUncheckedFalse  TDBEditDBEdit13LeftTop8WidthЂ Height	DataFieldSpecializzazione
DataSourceData.DsTitoliStudioTabOrder  TDBEditDBEdit15Left≤ Top8WidthvHeight	DataFieldDataConseguimento
DataSourceData.DsTitoliStudioTabOrder  TDBEditDBEdit14Left*Top8Width7Height	DataFieldAbilitazProfessionale
DataSourceData.DsTitoliStudioTabOrder  TDBEditDBEdit44LeftА TopWidthФ Height	DataFieldDescrizione
DataSourceData.DsTitoliStudioFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontReadOnly	TabOrder  TDBEditDBEdit10LeftTopWidthAHeight	DataField	Votazione
DataSourceData.DsTitoliStudioTabOrder  TDBEditDBEdit49LeftTopWidthxHeight	DataFieldTipo
DataSourceData.DsTitoliStudioFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontReadOnly	TabOrder   TDBEditDBEdit56LeftTop`WidthЂ Height	DataFieldNazioneConseg
DataSourceData.DsTitoliStudioTabOrder  TDBMemoDBMemo2LeftЎ TopXWidthЙHeight1	DataFieldNote
DataSourceData.DsTitoliStudioTabOrder	   TPanelPanel9LeftTopWidthHeightTabOrder TToolbarButton97TbBTitoliNewLeftATopWidth2HeightCaptionNuovoOpaqueOnClickTbBTitoliNewClick  TToolbarButton97TbBTitoliDelLeftsTopWidth2HeightCaptionEliminaOpaqueOnClickTbBTitoliDelClick  TToolbarButton97TbBTitoliOKLeft•TopWidth2HeightCaptionOKEnabledOpaqueOnClickTbBTitoliOKClick  TToolbarButton97TbBTitoliCanLeft„TopWidth2HeightCaptionAnnullaEnabledOpaqueOnClickTbBTitoliCanClick  TToolbarButton97ToolbarButton972Leftк TopWidthOHeightCaptionDettaglio tesiOnClickToolbarButton972Click  TLabelLabel15LeftTopWidthcHeightCaptionTitoli di studioFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Heightу	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TSpeedButton
BTitoloITALeftФ TopWidth%Height
GroupIndexDown	CaptionITAFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontVisibleOnClickBTitoloITAClick  TSpeedButton
BTitoloENGLeftє TopWidth%Height
GroupIndexCaptionENGFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontVisibleOnClickBTitoloENGClick    	TTabSheetTSCVCorsiStageHelpContextѕЖ CaptionCorsi-Stage TDBCtrlGridDBCtrlGrid2LeftTop!WidthБHeightАAllowInsertColCount
DataSourceData.DsCorsiExtraPanelHeight`
PanelWidthqTabOrder RowCountSelectedColorclInfoBk	ShowFocus TLabelLabel13LeftTopWidth+HeightCaption	Tipologia  TLabelLabel17Left» TopWidthHHeightCaptionOrganizzazioneFocusControlDBEdit17  TLabelLabel18LeftTop,WidthК HeightCaptionTitolo/Descrizione/ArgomentiFocusControlDBMemo1  TLabelLabel20LeftЉTop7Width>HeightCaptionGiorni (stage)FocusControlDBEdit19  TLabelLabel22Left&TopWidth;HeightCaptionDurata (ore):  TLabelLabel21Leftµ Top7Width&HeightCaptionAziendaFocusControlDBEdit20  TLabelLabel32LeftюTop8WidthbHeightCaptionConseguito nell'annoFocusControlDBEdit34  TDBComboBoxDBComboBox4LeftTopWidthЅ Height	DataField	Tipologia
DataSourceData.DsCorsiExtraFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold 
ItemHeightItems.StringsFormazione/SpecializzazioneMasterDottorato di Ricerca/Ph DStage 
ParentFontTabOrder   TDBEditDBEdit17Left» TopWidthIHeight	DataFieldOrganizzazione
DataSourceData.DsCorsiExtraTabOrder  TDBMemoDBMemo1LeftTop;Widthђ Height 	DataField	Argomenti
DataSourceData.DsCorsiExtraTabOrder  TDBEditDBEdit18Left&TopWidth<Height	DataField	DurataOre
DataSourceData.DsCorsiExtraTabOrder  TDBEditDBEdit19LeftЉTopFWidth>Height	DataFieldDurataStageGiorni
DataSourceData.DsCorsiExtraTabOrder  TDBEditDBEdit20Leftі TopFWidthэ Height	DataFieldNomeAzienda
DataSourceData.DsCorsiExtraTabOrder  TDBEditDBEdit34LeftюTopFWidthcHeight	DataFieldConseguitoAnno
DataSourceData.DsCorsiExtraTabOrder   TPanelPanel2LeftTopWidth	HeightTabOrder TToolbarButton97TbBCorsiExtraNewLeftATopWidth2HeightCaptionNuovoOpaqueOnClickTbBCorsiExtraNewClick  TToolbarButton97TbBCorsiExtraDelLeftsTopWidth2HeightCaptionEliminaOpaqueOnClickTbBCorsiExtraDelClick  TToolbarButton97TbBCorsiExtraOKLeft•TopWidth2HeightCaptionOKEnabledOpaqueOnClickTbBCorsiExtraOKClick  TToolbarButton97TbBCorsiExtraCanLeft„TopWidth2HeightCaptionAnnullaEnabledOpaqueOnClickTbBCorsiExtraCanClick  TLabelLabel23LeftTopWidth≤ HeightCaptionCorsi o stage c/o aziendeFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Heightу	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont    	TTabSheet
TSCVLingueHelpContext–Ж CaptionLingue TDBCtrlGridDBCtrlGrid3LeftTop!WidthyHeightШAllowInsertColCount
DataSourceData.DsLingueConoscPanelHeight3
PanelWidthiTabOrder RowCountSelectedColorclInfoBk	ShowFocus TLabelLabel24LeftTopWidth HeightCaptionLinguaFocusControlDBEdit16  TLabelLabel25Leftд TopWidthGHeightCaptionLivello (parlato)  TLabelLabel26LeftјTopWidth0HeightCaption	SoggiornoFocusControlDBEdit22  TLabelLabel60LeftNTopWidthCHeightCaptionLivello (scritto)  TDBEditDBEdit16LeftTopWidth” Height	DataFieldLingua
DataSourceData.DsLingueConoscFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontReadOnly	TabOrder   TDBEditDBEdit22LeftјTopWidth° Height	DataField	Soggiorno
DataSourceData.DsLingueConoscReadOnly	TabOrder  TDBEditDBEdit21Leftг TopWidthhHeight	DataFieldDescLivello
DataSourceData.DsLingueConoscReadOnly	TabOrder  TDBEditDBEdit45LeftMTopWidthiHeight	DataFieldDescLivelloScritto
DataSourceData.DsLingueConoscReadOnly	TabOrder   TPanelPanel1LeftTopWidthHeightTabOrder TToolbarButton97TbBLinConNewLeftcTopWidth6HeightCaptionNuovoOpaqueOnClickTbBLinConNewClick  TToolbarButton97TbBLinConDelLeftѕTopWidth6HeightCaptionEliminaOpaqueOnClickTbBLinConDelClick  TToolbarButton97TbBLinConModLeftЩTopWidth6HeightCaptionModificaOpaqueOnClickTbBLinConModClick  TLabelLabel30LeftTopWidth~HeightCaptionLingue conosciuteFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Heightу	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TSpeedButton
BLinguaITALeftTopWidth%Height
GroupIndexDown	CaptionITAFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontVisibleOnClickBLinguaITAClick  TSpeedButton
BLinguaENGLeft1TopWidth%Height
GroupIndexCaptionENGFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontVisibleOnClickBLinguaENGClick    	TTabSheet
TSCVespLavHelpContext—Ж CaptionEsperienze Lav. TPanelPanel3Left Top WidthЛHeight)AlignalTopTabOrder  TToolbarButton97TbBEspLavDelLeftё TopWidth5Height'CaptionEliminaOpaqueWordWrap	OnClickTbBEspLavDelClick  TToolbarButton97TbBEspLavModLeftTopWidthRHeight'Hintmodifica azienda e ruoloCaptionModifica azienda/settoreOpaqueWordWrap	OnClickTbBEspLavModClick  TToolbarButton97ToolbarButton9710LeftЬTopWidth2Height'Captionaltri dettagliWordWrap	OnClickToolbarButton9710Click  TLabelLabel34LeftTopWidthSHeight CaptionEsperienze lavorativeFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Heightу	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontWordWrap	  TToolbarButton97TbBEspLavNewLeftђ TopWidth2Height'CaptionNuovaOpaqueWordWrap	OnClickTbBEspLavNewClick  TToolbarButton97ToolbarButton975LefteTopWidth7Height'Hintmodifica azienda e ruoloCaptionModifica ruoloOpaqueWordWrap	OnClickToolbarButton975Click  TToolbarButton97BEspLavRetribLeftќTopWidth9Height'Hint9voci retributive di dettaglio
per questa esperienza lav.Captionvoci retributiveFont.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontParentShowHintShowHint	WordWrap	OnClickBEspLavRetribClick  TSpeedButton
BEspLavITALeft]TopWidth%Height
GroupIndexDown	CaptionITAFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontVisibleOnClickBEspLavITAClick  TSpeedButton
BEspLavENGLeftВ TopWidth%Height
GroupIndexCaptionENGFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontVisibleOnClickBEspLavENGClick  TToolbarButton97ToolbarButton976LeftTopWidth9Height'Hint/qualifica e livello
per questa esperienza lav.CaptionQualifica LivelloFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontParentShowHintShowHint	WordWrap	OnClickToolbarButton976Click   TPanelPanEspProfMaturataLeft Top%WidthЛHeight!AlignalBottom
BevelOuter	bvLoweredTabOrderVisible TLabelLabel41LeftTopWidth§ HeightCaption"Esperienza professionale maturata:FocusControlDBLookupComboBox3Font.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFont  TSpeedButtonBEspProfmatITALeftОTopWidth%Height
GroupIndexDown	CaptionITAFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontOnClickBEspProfmatITAClick  TSpeedButtonBEspProfmatENGLeft≥TopWidth%Height
GroupIndexCaptionENGFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontOnClickBEspProfmatENGClick  TDBLookupComboBoxDBLookupComboBox3Left∞ TopWidthў Height	DataFieldEspProfMaturata
DataSourcedsAnagAltriDatiFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder    TDBCtrlGridDBCtrlGrid4Left Top)WidthЛHeightьAlignalClientAllowInsertColCount
DataSourceData.DsEspLavPanelHeightю 
PanelWidth{TabOrderRowCountSelectedColorclInfoBk	ShowFocus TLabelLabel38LeftЇTopWidth[HeightCaptionDal-Al (mese/anno)FocusControlDBEdit32Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLTipoContrattoLeftЇTopgWidtheHeightCaptionQualifica ContrattualeFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLLivelloContrLeftЇTopЛ WidthYHeightCaptionLivello contrattualeFocusControlDBELivelloContrFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel42LeftЇTop∞ Width;HeightCaptionRetribuzioneFocusControlDBEdit35Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel44LeftіTop√ Width)HeightCaption	MensilitaFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontVisible  TLabel
LMensilitaLeftTop∞ Width,HeightCaption
Mensilitа:Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel56LeftBTopWidthHeightCaptionMesiFocusControlDBEdit41Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFont  TDBTextDBText1LeftTopWidthPHeight	AlignmenttaRightJustify	DataFieldAttuale
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel19LeftTop† Width]HeightCaptionCodifica pers.unica:Font.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontVisible  TLabelLabel40LeftЈTop… Width:HeightCaptionTel.azienda:Visible  TLabelLabel39LeftфTop#WidthHeightCaption--  TLabelLabel69LeftЇTop’ Width&HeightCaptionBenefitsFocusControlDBEdit35Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel70LeftїTopCWidthpHeightCaptionCondizione Contrattuale  	TGroupBox	GroupBox3LeftTopWidthЪHeightП CaptionAziendaEnabledFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TLabelLabel27LeftTopWidth8HeightCaption
Denominaz.FocusControlDBEdit23Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel28LeftTop(Width&HeightCaption	IndirizzoFocusControlDBEdit24Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel29LeftTop>Width'HeightCaptionComuneFocusControlDBEdit25Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel31LeftRTop(WidthHeightCaptionProv.FocusControlDBEdit26  TLabelLabel33LeftTop?Width-HeightCaption
Fatturato:FocusControlDBEdit28  TLabelLabel35LeftTopWWidthHeightCaptionN∞Dip.FocusControlDBEdit29Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel36LeftYTopVWidth"HeightCaptionSettore  TLabelLabel71LeftToplWidthHeightCaptionStato  TDBEditDBEdit23LeftDTopWidthMHeight	DataFieldNomeAzienda
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder   TDBEditDBEdit24Left6Top$WidthHeight	DataFieldAzIndirizzo
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBEditDBEdit25Left5Top;Width‘ Height	DataFieldComuneAzienda
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBEditDBEdit26LeftmTop$Width#Height	DataFieldProvinciaAzienda
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBEditDBEdit28Left@Top;WidthPHeight	DataField	Fatturato
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBEditDBEdit29Left5TopRWidthHeight	DataFieldNumDipendenti
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBEditDBEdit27LeftА TopRWidthHeight	DataFieldSettoreCalc
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder  TDBEditDBEdit55Left5TopiWidth\Height	DataFieldAziendaStato
DataSourceData.DsEspLavTabOrder   TDBEditDBEdit32Left–Top Width#Height	DataFieldAnnoDal
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBEditDBEdit33LeftTop Width#Height	DataFieldAnnoAl
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBComboBoxDBCTipoContratto_oldLeftЉTopuWidth± Height	DataFieldTipoContratto
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ItemHeightItems.Strings
StagionaleApprendistaFormazione LavoroTempo IndeterminatoTempo Determinato&Collaborazione Coordinata ContinuativaPrestatore d'opera occasionale
Consulente 
ParentFontTabOrderVisible
OnDropDownDBCTipoContratto_oldDropDown  TDBEditDBELivelloContrLeftЇTopЩ Width∞ Height	DataFieldLivelloContrattuale
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontReadOnly	TabOrder  TDBEditDBEdit35LeftЇTopЊ WidthqHeight	DataFieldRetribNettaMensile
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder	  	TGroupBox	GroupBox4LeftTopЪ WidthЪHeightVCaptionRuolo e Area ruoloFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder TLabelLabel66LeftTop)Width'HeightCaption
Job title:FocusControlDBEdit53Font.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel67LeftTop+Width(HeightCaptionRegione  TDBEditDBEdit3Leftй TopWidth® Height	DataFieldDescrizione_1
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontReadOnly	TabOrder  TDBEditDBEdit5LeftTopWidthЏ Height	DataFieldDescrizione
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontReadOnly	TabOrder   TDBEditDBEdit53Left4Top'Width’ Height	DataFieldRuoloPresentaz
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBCheckBoxDBCheckBox20LeftTop@Width-HeightHintConsiglio di AmministrazioneCaptionCdA	DataFieldFlag_CdA
DataSourceData.DsEspLavParentShowHintShowHint	TabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox21Left6Top@Width\HeightHintCollegio sindacaleCaptionColl.sindacale	DataFieldFlag_CollegioSind
DataSourceData.DsEspLavParentShowHintShowHint	TabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox22LeftР Top@Width]HeightHintConsiglio di gestioneCaptionCons.Gestione	DataFieldFlag_ConsiglioGest
DataSourceData.DsEspLavParentShowHintShowHint	TabOrderValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox23Leftн Top@WidthNHeightHintConsiglio di sorveglianzaCaption
Cons.Sorv.	DataFieldFlag_ConsiglioSorv
DataSourceData.DsEspLavParentShowHintShowHint	TabOrderValueCheckedTrueValueUncheckedFalse  TDBComboBoxDBComboBox5Left>Top(WidthRHeightStylecsDropDownList	DataFieldRegione
DataSourceData.DsEspLav
ItemHeightTabOrderOnChangeDBComboBox5Change   TDBEditDBEMensilitaLeft-TopЊ WidthHeight	DataField	Mensilita
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder
  TDBEditDBEdit41LeftBTop Width)Height	DataField
DurataMesi
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBCheckBoxDBCheckBox8Left[TopWidthHeight	AlignmenttaLeftJustify	DataFieldAttuale
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrderValueCheckedTrueValueUncheckedFalse  TDBEditDBEdit30LeftTopќ WidthQHeight	DataFieldTitoloMansione
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderVisible  TDBEditDBEdit12LeftџTop∆ WidthЕ Height	DataFieldTelefono
DataSourceData.DsEspLavReadOnly	TabOrderVisible  TDBEditDBEdit36LeftЇTop WidthHeight	DataFieldMeseDal
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBEditDBEdit37LeftьTop WidthHeight	DataFieldMeseAl
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBEditDBEdit54LeftЇTopг Widthѓ Height	DataFieldBenefits
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBLookupComboBoxDBLookupComboBox5LeftєTopQWidth∞ Height	DataFieldCondizioneContrattuale
DataSourceData.DsEspLavTabOrder  TDBEditDBCTipoContrattoLeftїTopuWidthЃ Height	DataFieldTipoContratto
DataSourceData.DsEspLavReadOnly	TabOrder    	TTabSheetTSCVConoscInfoCaptionApplicativi conosc.
ImageIndex фTFrmAnagConoscInfoFrmAnagConoscInfo1LeftTopWidthЕHeight£ сTPanelPanel1WidthЕ сTToolbarButton97BVoceNewOnClickFrmAnagConoscInfo1BVoceNewClick  сTToolbarButton97BVoceDelOnClickFrmAnagConoscInfo1BVoceDelClick   сTDBCtrlGridDBCtrlGrid1WidthЕHeightwPanelHeight}
PanelWidthu сTLabelLabel2LeftН  сTLabelLabel7Leftc  сTLabelLabel8Leftз  сTDBEditDBEdit1Widthя   сTDBEditDBEdit2LeftНWidth№   сTDBEditDBEdit5Width?  сTDBEditDBEdit6Left!Width@  сTDBComboBoxDBComboBox1Left¬TopS  сTDBComboBoxDBComboBox2LeftзWidthВ Items.StringsAS/400Mac OSPC MonoutenzaReteUnix / Linux
Windows XPWindows 2000Microsoft Office   сTDBEditDBEdit7Widthe  сTDBLookupComboBoxDBLKLivelliLeftcWidth~     	TTabSheetTSCVCampiPersCaptionForm Web - Pers.
ImageIndex TPanel
PanFerrariLeft Top WidthЛHeightП AlignalTop
BevelOuter	bvLoweredTabOrder  TLabelLabel46LeftTop2WidthGHeightCaptionSe sм quando ?FocusControlDBEdit39  TLabelLabel50Leftƒ Top2WidthOHeightCaptionTipo di selezioneFocusControlDBEdit47  TLabelLabel55LeftTopaWidthЗ HeightCaptionParenti conosciuti in aziendaFocusControlDBEdit48  TDBCheckBoxDBCheckBox18LeftTopWidth)HeightCaption6Ha inviato precedenti domande presso la nostra societа	DataFieldDomandePrec
DataSourcedsAnagAltriDatiTabOrder ValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox19LeftTop Width)HeightCaption7Ha giа partecipato ad altre selezioni in questa azienda	DataFieldPartecSelezPrec
DataSourcedsAnagAltriDatiTabOrderValueCheckedTrueValueUncheckedFalse  TDBEditDBEdit39LeftTopBWidthє Height	DataFieldPartecSelezQuando
DataSourcedsAnagAltriDatiTabOrder  TDBEditDBEdit47Leftƒ TopBWidthЄ Height	DataFieldTipoSelezione
DataSourcedsAnagAltriDatiTabOrder  TDBEditDBEdit48LeftTopqWidth0Height	DataFieldParentiInAzienda
DataSourcedsAnagAltriDatiTabOrder    	TTabSheetTSCampiPersCaptionCandidato - Pers
ImageIndex фTCampiPersFrameCampiPersFrame1WidthЛHeightFAlignalClient сTPanelPanTopWidthЛ  сTPanelPan1WidthЛ с	TdxDBEdit	dxDBEdit1Widthч   сTPanelPan2WidthЛ с	TdxDBEdit	dxDBEdit2Widthч   сTPanelPan3WidthЛ с	TdxDBEdit	dxDBEdit3Widthч   сTPanelPan4WidthЛ с	TdxDBEdit	dxDBEdit4Widthч   сTPanelPan5WidthЛ с	TdxDBEdit	dxDBEdit5Widthч   сTPanelPan6WidthЛ с	TdxDBEdit	dxDBEdit6Widthч   сTPanelPan7WidthЛ с	TdxDBEdit	dxDBEdit7Widthч   сTPanelPan8WidthЛ с	TdxDBEdit	dxDBEdit8Widthч   сTPanelPan9WidthЛ с	TdxDBEdit	dxDBEdit9Widthч   сTPanelPan10WidthЛ с	TdxDBEdit
dxDBEdit10Widthч     	TTabSheetTSNoteCaptionNote
ImageIndex TDBMemoDBNoteLeft TopWidthЛHeightш AlignalTop	DataFieldNoteCurriculum
DataSourcedsAnagAltriDatiTabOrder   TPanelPanel4Left Top WidthЛHeightAlignalTop
BevelOuterbvNoneTabOrder    
TWallpaper
Wallpaper1Left Top WidthУHeight+AlignalTopTabOrder TToolbarButton97ToolbarButton973LeftџTopWidthPHeight CaptionEsciFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.Style 
Glyph.Data
z  v  BMv      v   (                                       А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 3     33wwwww33їїїї33w?33333їїї33sу3333їїї337?3333її3333333її3333333її3333333її3333333її3333333її333у333∞її333w3333її3333333її33?3333бїї333333оїї33w€€33     33wwwwws3	NumGlyphsOpaque
ParentFontOnClickToolbarButton973Click  TToolbarButton97ToolbarButton971LeftЛTopWidthNHeight CaptionCV ViewFont.CharsetDEFAULT_CHARSET
Font.ColorclBlueFont.Heightх	Font.NameMS Sans Serif
Font.Style 
Glyph.Data
z  v  BMv      v   (                                       А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 0       7wwwwwww€€€€€€pw?у333?ww€€€€wрw?€€€wч€w     3wwwwww€€їїї∞3??у?ч€p ї ∞?wwуw7p∞∞∞w37чs7 ~о∞∞ї∞w€€7чу7wwа оаwww7wуч0~о аа7s€swч73pа а37s7ws733pо оа3373w€7333оаа333377333оаа33337s7333оооа333€€€ч333     333wwwww	NumGlyphsOpaque
ParentFontOnClickToolbarButton971Click  TToolbarButton97ToolbarButton974Left8TopWidthPHeightCaption	File Word
Glyph.Data
z  v  BMv      v   (                                       А  А   АА А   А А АА   њњњ   €  €   €€ €   € € €€  €€€ 333333333333€333333<333333чwу3333<јр√33337w73333√3333sswу33< €рр√33чw3773<ј€€€37w?33swу√€€рр√ssу377 €р€€€w37?33sw€€€€ррsу3sу3770€€р€€€7?37?3?s3€€€ 33sу3s€w330€€р 3337?37w3333€33333s€s333330 3333337w333333333333333333333333333333333333	NumGlyphsOnClickToolbarButton974Click  TPanelPanel7LeftTopWidth4Height)AlignalLeft
BevelOuterbvNoneEnabledTabOrder  TDBEditDBEdit1LeftTop
WidthЛ HeightColorclYellow	DataFieldCognome
DataSourceData.DsAnagraficaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontReadOnly	TabOrder   TDBEditDBEdit2LeftЦ Top
WidthТ HeightColorclYellow	DataFieldNome
DataSourceData.DsAnagraficaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontReadOnly	TabOrder    TDataSourcedsAnagAltriDatiDataSetTAnagAltriDatiLeft© Top	  TADOLinkedQueryQTipiRetribLK
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Strings*select ID,TipoRetrib from TipiRetribuzione 	UseFilterLeftTop  TADOLinkedQueryTAnagAltriDati
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect * from AnagAltreInfo  MasterDataSetData.TAnagraficaLinkedMasterFieldIDLinkedDetailFieldIDAnagrafica	UseFilterLeftД Top	 TStringFieldTAnagAltriDatiTipoRetib	FieldKindfkLookup	FieldName	TipoRetibLookupDataSetQTipiRetribLKLookupKeyFieldsIDLookupResultField
TipoRetrib	KeyFieldsIDTipoRetribLookup	  TStringFieldTAnagAltriDatiDispMovimento	FieldKindfkLookup	FieldNameDispMovimentoLookupDataSet
TDispMovLKLookupKeyFieldsIDLookupResultFieldDescrizione	KeyFields	IDDispMovSize(Lookup	  TIntegerFieldTAnagAltriDatiIDAnagrafica	FieldNameIDAnagrafica  TBooleanFieldTAnagAltriDatiFiguraChiave	FieldNameFiguraChiave  TBooleanField$TAnagAltriDatiAssunzioneObbligatoria	FieldNameAssunzioneObbligatoria  TBooleanField!TAnagAltriDatiIdoneoAffiancamento	FieldNameIdoneoAffiancamento  TStringField TAnagAltriDatiCateogorieProtette	FieldNameCateogorieProtette	FixedChar	Size  TStringFieldTAnagAltriDatiInvalidita	FieldName
Invalidita	FixedChar	Size
  TIntegerField#TAnagAltriDatiPercentualeInvalidita	FieldNamePercentualeInvalidita  TBooleanFieldTAnagAltriDatiPatente	FieldNamePatente  TStringFieldTAnagAltriDatiTipoPatente	FieldNameTipoPatente	FixedChar	Size  TBooleanFieldTAnagAltriDatiDispAuto	FieldNameDispAuto  TStringField#TAnagAltriDatiServizioMilitareStato	FieldNameServizioMilitareStato	FixedChar	Size  TStringField"TAnagAltriDatiServizioMilitareDove	FieldNameServizioMilitareDove	FixedChar	Size  TStringField$TAnagAltriDatiServizioMilitareQuando	FieldNameServizioMilitareQuando	FixedChar	  TStringField#TAnagAltriDatiServizioMilitareGrado	FieldNameServizioMilitareGrado	FixedChar	Size  TStringFieldTAnagAltriDatiTipoContratto	FieldNameTipoContratto	FixedChar	Size(  TStringField#TAnagAltriDatiQualificaContrattuale	FieldNameQualificaContrattuale	FixedChar	Size(  TDateTimeFieldTAnagAltriDatiDataAssunzione	FieldNameDataAssunzione  TDateTimeFieldTAnagAltriDatiDataScadContratto	FieldNameDataScadContratto  TDateTimeFieldTAnagAltriDatiDataUscita	FieldName
DataUscita  TDateTimeFieldTAnagAltriDatiDataFineProva	FieldNameDataFineProva  TDateTimeFieldTAnagAltriDatiDataProssScatto	FieldNameDataProssScatto  TDateTimeFieldTAnagAltriDatiDataPensione	FieldNameDataPensione  TIntegerFieldTAnagAltriDatiIDStabilimento	FieldNameIDStabilimento  TStringFieldTAnagAltriDatiPosizioneIdeale	FieldNamePosizioneIdeale	FixedChar	Size  TStringFieldTAnagAltriDatiOrarioIdeale	FieldNameOrarioIdeale	FixedChar	  TIntegerFieldTAnagAltriDatiIDDispMov	FieldName	IDDispMov  TStringFieldTAnagAltriDatiDisponibContratti	FieldNameDisponibContratti	FixedChar	  TStringFieldTAnagAltriDatiCVProvenienza	FieldNameCVProvenienza	FixedChar	Size  TStringFieldTAnagAltriDatiCVFile	FieldNameCVFile	FixedChar	Size(  TStringField!TAnagAltriDatiAbilitazProfessioni	FieldNameAbilitazProfessioni	FixedChar	Size  
TMemoFieldTAnagAltriDatiNote	FieldNameNoteBlobTypeftMemo  TBooleanFieldTAnagAltriDatiMobilita	FieldNameMobilita  TBooleanFieldTAnagAltriDatiCassaIntegrazione	FieldNameCassaIntegrazione  TBooleanFieldTAnagAltriDatiDispPartTime	FieldNameDispPartTime  TBooleanFieldTAnagAltriDatiDispInterinale	FieldNameDispInterinale  TStringFieldTAnagAltriDatiProvInteresse	FieldNameProvInteresse	FixedChar	SizeP  TStringFieldTAnagAltriDatiInquadramento	FieldNameInquadramentoSize2  TStringFieldTAnagAltriDatiRetribuzione	FieldNameRetribuzione	FixedChar	Sized  TStringFieldTAnagAltriDatiBenefits	FieldNameBenefits	FixedChar	Sized  TFloatFieldTAnagAltriDatiPercPartTime	FieldNamePercPartTime  TDateTimeFieldTAnagAltriDatiDataAssunzRicon	FieldNameDataAssunzRicon  TDateTimeField!TAnagAltriDatiDataIngressoAzienda	FieldNameDataIngressoAzienda  TStringFieldTAnagAltriDatiCodSindacato	FieldNameCodSindacato	FixedChar	Size
  TStringFieldTAnagAltriDatiCodTipoAssunzione	FieldNameCodTipoAssunzione	FixedChar	Size  TSmallintFieldTAnagAltriDatiPeriodoProvaGG	FieldNamePeriodoProvaGG  TIntegerFieldTAnagAltriDatiIDMotAssunz	FieldNameIDMotAssunz  TStringFieldTAnagAltriDatiNazionalita	FieldNameNazionalita	FixedChar	Size2  TIntegerFieldTAnagAltriDatiIDTipoRetrib	FieldNameIDTipoRetrib  TIntegerFieldTAnagAltriDatiIDSindacato	FieldNameIDSindacato  TStringFieldTAnagAltriDatiNoteInfoPos	FieldNameNoteInfoPos	FixedChar	Size  TBooleanFieldTAnagAltriDatiTemporaryMG	FieldNameTemporaryMG  TBooleanFieldTAnagAltriDatiNoCV	FieldNameNoCV  TBooleanField!TAnagAltriDatiCassaIntegrStraord3	FieldNameCassaIntegrStraord3  TBooleanField"TAnagAltriDatiCassaIntegrStraord24	FieldNameCassaIntegrStraord24  TBooleanFieldTAnagAltriDatiDisoccupato24	FieldNameDisoccupato24  TBooleanFieldTAnagAltriDatiDisoccupato12	FieldNameDisoccupato12  TBooleanFieldTAnagAltriDatiDispTraferteGiorn	FieldNameDispTraferteGiorn  TBooleanFieldTAnagAltriDatiDispPernottamento	FieldNameDispPernottamento  TBooleanFieldTAnagAltriDatiDisptrasferimento	FieldNameDisptrasferimento  TIntegerFieldTAnagAltriDatiIDContrattoNaz	FieldNameIDContrattoNaz  TIntegerFieldTAnagAltriDatiIDCatProfess	FieldNameIDCatProfess  TDateTimeFieldTAnagAltriDatiDataLicenziamento	FieldNameDataLicenziamento  TDateTimeFieldTAnagAltriDatiDataProroga	FieldNameDataProroga  TDateTimeFieldTAnagAltriDatiDataUltimaProm	FieldNameDataUltimaProm  TIntegerFieldTAnagAltriDatiIDEspProfMaturata	FieldNameIDEspProfMaturata  TBooleanFieldTAnagAltriDatiDomandePrec	FieldNameDomandePrec  TBooleanFieldTAnagAltriDatiPartecSelezPrec	FieldNamePartecSelezPrec  TStringFieldTAnagAltriDatiPartecSelezQuando	FieldNamePartecSelezQuando	FixedChar	Size(  TStringFieldTAnagAltriDatiTipoSelezione	FieldNameTipoSelezione	FixedChar	Size  TStringFieldTAnagAltriDatiParentiInAzienda	FieldNameParentiInAzienda	FixedChar	Size2  TStringFieldTAnagAltriDatiTipoPersona	FieldNameTipoPersona	FixedChar	Size  TStringFieldTAnagAltriDatiDirigenteTipo	FieldNameDirigenteTipo	FixedChar	Size  TStringFieldTAnagAltriDatiDirigenteStato	FieldNameDirigenteStato	FixedChar	Size  TIntegerField TAnagAltriDatiDirigenteIDCliente	FieldNameDirigenteIDCliente  TIntegerFieldTAnagAltriDatiIDTDgTipologia	FieldNameIDTDgTipologia  TStringFieldTAnagAltriDatiEspProfMaturata	FieldKindfkLookup	FieldNameEspProfMaturataLookupDataSetQEspProfMatLKLookupKeyFieldsIDLookupResultFieldEspProfMaturata	KeyFieldsIDEspProfMaturataSize(Lookup	  TStringFieldTAnagAltriDatiNumPassaporto	FieldNameNumPassaportoSize2  TStringFieldTAnagAltriDatiRetribVariabile	FieldNameRetribVariabileSize2  TStringFieldTAnagAltriDatiRetribTotale	FieldNameRetribTotaleSize2  TIntegerField TAnagAltriDatiIDContrattoSettore	FieldNameIDContrattoSettore  TStringFieldTAnagAltriDatiContrattoSettore	FieldKindfkLookup	FieldNameContrattoSettoreLookupDataSetQContrattiSettoriLKLookupKeyFieldsIDLookupResultFieldSettore	KeyFieldsIDContrattoSettoreSize2Lookup	  
TMemoFieldTAnagAltriDatiNoteCurriculum	FieldNameNoteCurriculumBlobTypeftMemo  
TMemoFieldTAnagAltriDatiNoteAltriDati	FieldNameNoteAltriDatiBlobTypeftMemo  TBooleanFieldTAnagAltriDatiDispAvvicinSede	FieldNameDispAvvicinSede  TStringFieldTAnagAltriDatiSpecCVProvenienza	FieldNameSpecCVProvenienzaSizeф   TADOLinkedQueryQEspProfMatLK
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect * from EspProfMaturata 	UseFilterLeftь Top  TADOLinkedTable
TDispMovLK
ConnectionData.DB
CursorTypectStatic	TableNameDispMovimentoLeft№ Top	  TADOLinkedTableTAnagMansioni
ConnectionData.DB	TableNameAnagMansioniLeftdTopa  TADOLinkedTableTMansioniLK
ConnectionData.DB	TableNameMansioniLeftЬTopa  	TADOQueryQContrattiSettoriLK
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect *from ContrattiSettoriorder by settore LeftС TopY TAutoIncFieldQContrattiSettoriLKID	FieldNameIDReadOnly	  TStringFieldQContrattiSettoriLKSettore	FieldNameSettoreSize2    