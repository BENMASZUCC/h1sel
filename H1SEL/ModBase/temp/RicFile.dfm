object RicFileForm: TRicFileForm
  Left = 214
  Top = 404
  BorderStyle = bsDialog
  Caption = 'File associato'
  ClientHeight = 85
  ClientWidth = 504
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 3
    Top = 3
    Width = 55
    Height = 13
    Caption = 'Descrizione'
  end
  object Label3: TLabel
    Left = 3
    Top = 44
    Width = 96
    Height = 13
    Caption = 'Percorso e nome file'
  end
  object BitBtn1: TBitBtn
    Left = 402
    Top = 2
    Width = 99
    Height = 39
    TabOrder = 2
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 402
    Top = 42
    Width = 99
    Height = 39
    Caption = 'Annulla'
    TabOrder = 3
    Kind = bkCancel
  end
  object EDesc: TEdit
    Left = 3
    Top = 19
    Width = 390
    Height = 21
    MaxLength = 100
    TabOrder = 0
  end
  object FEFile: TFilenameEdit
    Left = 3
    Top = 59
    Width = 392
    Height = 21
    NumGlyphs = 1
    TabOrder = 1
  end
end
