�
 TESPLAVFORM 0.  TPF0TEspLavForm
EspLavFormLeft�Top� BorderStylebsDialogCaptionEsperienza lavorativaClientHeight�ClientWidth:Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TSpeedButtonBAreeRuoliITALeftTop� Width%Height
GroupIndexDown	CaptionITAFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontVisibleOnClickBAreeRuoliITAClick  TSpeedButtonBAreeRuoliENGLeft2Top� Width%Height
GroupIndexCaptionENGFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontVisibleOnClickBAreeRuoliENGClick  TBitBtnBitBtn1LeftTopWidthYHeight"TabOrder KindbkOK  TBitBtnBitBtn2Left�TopWidthYHeight"CaptionAnnullaTabOrderKindbkCancel  TPanelPanel73LeftTop� WidthHeight	AlignmenttaLeftJustifyCaption  AreeColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TDBGridDBGrid2LeftTopWidthHeight� 
DataSourceDsAreeReadOnly	TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameDescrizioneWidth� Visible	    TDBGridDBGrid3LeftTop)Width'Height� 
DataSourceDsRuoliReadOnly	TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameDescrizioneWidthVisible	    TPanelPanel2LeftTopWidth)Height	AlignmenttaLeftJustifyCaption*  Ruoli disponibili per l'area selezionataColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  	TCheckBox	CBInsCompLeftTop�Width)HeightCaption;attribuisci il ruolo e le relative competenze al nominativoChecked	Font.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontState	cbCheckedTabOrder  TPanel
PanAziendeLeftTopOWidth2Height� 
BevelOuterbvNoneTabOrderVisible TDBGridDBGrid1Left Top3Width�HeightuAlignalLeft
DataSourceDsElencoAziendeReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameDescrizioneTitle.CaptionNome aziendaWidth� Visible	 Expanded	FieldNameAttivitaWidth� Visible	 Expanded	FieldNameComuneWidth� Visible	 	AlignmenttaCenterExpanded	FieldName	ProvinciaTitle.CaptionProv.Visible	    TPanelPanel1Left�Top3WidthLHeightuAlignalClient
BevelOuter	bvLoweredCaptionPanel1TabOrder TToolbarButton97BAziendaNewLeftTopWidthJHeight#CaptionNuova azienda
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333�3333�39�3330 373337w39�3330 3��337w9���33337ww333?9���333 7wws333w39�3333 373333w39�3333337s3333?3333333 3333�33w333<333 3337�33w333<�3333����?�<����Ù37wwwww�<����Ù37wwwwsw3333<�3333337s33�333<330 3337337w3333330 3333337w3333333333333333	NumGlyphsWordWrap	OnClickBAziendaNewClick  TToolbarButton97BAziendaDelLeftTopGWidthJHeight#CaptionElimina
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 30    3337wwww�330�wwp3337�����330���p3337�����330��pp3337�����330���p3337�����330��pp3337�����330���p333�������00��pp0377�����33 ���p33w����s330��pp3337�����330pppp3337�����33     33wwwww33��ww33����33     33wwwwws3330wp333337���33330  333337ww333	NumGlyphsWordWrap	OnClickBAziendaDelClick  TToolbarButton97BAziendaModLeftTop$WidthJHeight#CaptionModifica
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 333     333wwwww333����?���??� 0  � �w�ww?sw7������ws3?33�࿿ ���w�3ws��7�������w�3?��7࿿  �w�3wwss7��������w�?���37�   ��w�wwws?� � �� �ws�w73w730 ���37wss3?�330���  33773�ww33��33s7s730���37�33s3	�� 33ws��w3303   3373wwws3	NumGlyphsWordWrap	OnClickBAziendaModClick   TPanelPanel3Left Top Width2HeightAlignalTop	AlignmenttaLeftJustifyCaption	  AziendeColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TPanelPanel6Left TopWidth2HeightAlignalTop
BevelOuter	bvLoweredTabOrder TLabelLabel1LeftTopWidthHeightCaptionCerca:  TEditEAziendaLeft)TopWidth� HeightTabOrder OnChangeEAziendaChange  	TCheckBoxCBTestoContLeft� TopWidth� HeightCaptionRicerca per testo contenutoChecked	State	cbCheckedTabOrderOnClickCBTestoContClick    TRadioGroup	RGOpzioneLeftTopWidth� HeightGCaptionOpzione	ItemIndex Items.Stringssolo settoreazienda (e suo settore)nessuno dei due TabOrderOnClickRGOpzioneClick  TPanel
PanSettoriLeftTopOWidth�Height� 
BevelOuterbvNoneTabOrder	 TPanelPanel4Left Top Width�HeightAlignalTop	AlignmenttaLeftJustifyCaption  Settori aziendaliColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   TDBGridDBGrid4Left TopWidthRHeight� AlignalLeft
DataSource	DsSettoriReadOnly	TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameAttivitaVisible	    TPanelPanel5LeftRTopWidthMHeight� AlignalClient
BevelOuter	bvLoweredTabOrderVisible TToolbarButton97ToolbarButton971LeftTopWidthJHeight#CaptionNuovo settore
Glyph.Data
z  v  BMv      v   (                                       �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 3333333333�3333�39�3330 373337w39�3330 3��337w9���33337ww333?9���333 7wws333w39�3333 373333w39�3333337s3333?3333333 3333�33w333<333 3337�33w333<�3333����?�<����Ù37wwwww�<����Ù37wwwwsw3333<�3333337s33�333<330 3337337w3333330 3333337w3333333333333333	NumGlyphsVisibleWordWrap	OnClickToolbarButton971Click  TSpeedButtonBSettoriITALeftTop*Width%Height
GroupIndexDown	CaptionITAFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontVisibleOnClickBSettoriITAClick  TSpeedButtonBSettoriENGLeftTop@Width%Height
GroupIndexCaptionENGFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontVisibleOnClickBSettoriENGClick    TDataSourceDsElencoAziendeDataSetTElencoAziendeLeftTop�   TTimerTimer1OnTimerTimer1TimerLeftoTopu  TDataSourceDsAreeDataSetTAreeLeftTopU  TDataSourceDsRuoliDataSetTRuoliLeft?Top]  TTimerTimer2OnTimerTimer2TimerLeftWTopE  TDataSource	DsSettoriDataSetTSettoriLeftTop�   TADOLinkedQueryTElencoAziende
ConnectionData.DB
CursorTypectStatic
ParametersNamexDesc
Attributes
paNullable DataTypeftStringNumericScale� 	Precision� Size2Value   SQL.Stringsselect * from EBC_Clienti where Descrizione like :xDesc 	UseFilterLeftTopw TAutoIncFieldTElencoAziendeID	FieldNameIDReadOnly	  TStringFieldTElencoAziendeDescrizione	FieldNameDescrizione	FixedChar	Size2  TStringFieldTElencoAziendeStato	FieldNameStato	FixedChar	Size  TStringFieldTElencoAziendeIndirizzo	FieldName	Indirizzo	FixedChar	Size(  TStringFieldTElencoAziendeCap	FieldNameCap	FixedChar	Size  TStringFieldTElencoAziendeComuneDisplayWidthd	FieldNameComune	FixedChar	Sized  TStringFieldTElencoAziendeProvincia	FieldName	Provincia	FixedChar	Size  TIntegerFieldTElencoAziendeIDAttivita	FieldName
IDAttivita  TStringFieldTElencoAziendeTelefono	FieldNameTelefono	FixedChar	Size  TStringFieldTElencoAziendeFax	FieldNameFax	FixedChar	Size  TStringFieldTElencoAziendePartitaIVA	FieldName
PartitaIVA	FixedChar	Size  TStringFieldTElencoAziendeCodiceFiscale	FieldNameCodiceFiscale	FixedChar	Size  TStringFieldTElencoAziendeBancaAppoggio	FieldNameBancaAppoggio	FixedChar	SizeP  TStringFieldTElencoAziendeSistemaPagamento	FieldNameSistemaPagamento	FixedChar	Size<  
TMemoFieldTElencoAziendeNoteContratto	FieldNameNoteContrattoBlobTypeftMemo  TStringFieldTElencoAziendeResponsabile	FieldNameResponsabile	FixedChar	Size(  TDateTimeFieldTElencoAziendeConosciutoInData	FieldNameConosciutoInData  TStringFieldTElencoAziendeIndirizzoLegale	FieldNameIndirizzoLegale	FixedChar	Size(  TStringFieldTElencoAziendeCapLegale	FieldName	CapLegale	FixedChar	Size  TStringFieldTElencoAziendeComuneLegaleDisplayWidthd	FieldNameComuneLegale	FixedChar	Sized  TStringFieldTElencoAziendeProvinciaLegale	FieldNameProvinciaLegale	FixedChar	Size  TStringFieldTElencoAziendeCartellaDoc	FieldNameCartellaDoc	FixedChar	SizeP  TStringFieldTElencoAziendeTipoStrada	FieldName
TipoStrada	FixedChar	Size
  TStringFieldTElencoAziendeTipoStradaLegale	FieldNameTipoStradaLegale	FixedChar	Size
  TStringFieldTElencoAziendeNumCivico	FieldName	NumCivico	FixedChar	Size
  TStringFieldTElencoAziendeNumCivicoLegale	FieldNameNumCivicoLegale	FixedChar	Size
  TStringFieldTElencoAziendeNazioneAbb	FieldName
NazioneAbb	FixedChar	Size  TStringFieldTElencoAziendeNazioneAbbLegale	FieldNameNazioneAbbLegale	FixedChar	Size  
TMemoFieldTElencoAziendeNote	FieldNameNoteBlobTypeftMemo  TFloatFieldTElencoAziendeFatturato	FieldName	Fatturato  TSmallintFieldTElencoAziendeNumDipendenti	FieldNameNumDipendenti  TStringFieldTElencoAziendeAttivita	FieldKindfkLookup	FieldNameAttivitaLookupDataSet
TSettoriLKLookupKeyFieldsIDLookupResultFieldAttivita	KeyFields
IDAttivitaSize2Lookup	  TStringFieldTElencoAziendeSitoInternet	FieldNameSitoInternet	FixedChar	Size2  TStringField!TElencoAziendeDescAttivitaAzienda	FieldNameDescAttivitaAzienda	FixedChar	Size�   TIntegerFieldTElencoAziendeIDTipoAzienda	FieldNameIDTipoAzienda   TADOLinkedTable
TSettoriLK
ConnectionData.DB
CursorTypectStatic	TableNameEBC_AttivitaLeftCTop�  TAutoIncFieldTSettoriLKID	FieldNameIDReadOnly	  TStringFieldTSettoriLKAttivita	FieldNameAttivita	FixedChar	Size2   TADOLinkedQueryTSettoriActive	
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Strings"select EBC_Attivita.*, AreaSettorefrom EBC_Attivita, AreeSettori2where EBC_Attivita.IDAreaSettore *= AreeSettori.IDorder by Attivita 	UseFilterLeftTop�   TADOLinkedQueryQ
ConnectionData.DB
Parameters 	UseFilterLeft Top8  TADOLinkedQueryTAreeActive	
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect * from Areeorder by Descrizione 	UseFilterLeft Top(  TADOLinkedQueryTRuoliActive	
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Strings�select ID,IDArea,Descrizione, Descrizione_ENG  from Mansioni  /*BEGIN_AUTO_ADD*/ WHERE (IDArea=40)/*END_AUTO_ADD*/order by Descrizione MasterDataSetTAreeLinkedMasterFieldIDLinkedDetailFieldIDArea	UseFilterLeft@Top    