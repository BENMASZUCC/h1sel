object ModelloWordForm: TModelloWordForm
  Left = 363
  Top = 282
  BorderStyle = bsDialog
  Caption = 'Modello word'
  ClientHeight = 198
  ClientWidth = 429
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 80
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nome modello'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 8
    Top = 32
    Width = 84
    Height = 13
    Alignment = taRightJustify
    Caption = 'Breve descrizione'
  end
  object Label4: TLabel
    Left = 7
    Top = 55
    Width = 44
    Height = 13
    Alignment = taRightJustify
    Caption = 'Iniziali file'
  end
  object Label5: TLabel
    Left = 8
    Top = 83
    Width = 40
    Height = 13
    Caption = 'Tipo File'
  end
  object BitBtn1: TBitBtn
    Left = 326
    Top = 2
    Width = 93
    Height = 36
    TabOrder = 4
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 326
    Top = 40
    Width = 93
    Height = 36
    Caption = 'Annulla'
    TabOrder = 5
    Kind = bkCancel
  end
  object EModello: TEdit
    Left = 96
    Top = 4
    Width = 185
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    MaxLength = 30
    ParentFont = False
    TabOrder = 0
  end
  object EDesc: TEdit
    Left = 96
    Top = 28
    Width = 225
    Height = 21
    MaxLength = 50
    TabOrder = 1
  end
  object EIniziali: TEdit
    Left = 96
    Top = 51
    Width = 41
    Height = 21
    MaxLength = 3
    TabOrder = 3
  end
  object RGTabella: TRadioGroup
    Left = 10
    Top = 109
    Width = 270
    Height = 74
    Caption = 'Tabella database'
    ItemIndex = 0
    Items.Strings = (
      'Anagrafica   (candidati)'
      'EBC_Clienti  (clienti-aziende)'
      '*RicClienti    (risultato della ricerca contatti-clienti)')
    TabOrder = 2
  end
  object GroupBox1: TGroupBox
    Left = 288
    Top = 108
    Width = 81
    Height = 74
    Caption = 'Applicativi'
    TabOrder = 6
    object CBH1Sel: TCheckBox
      Left = 8
      Top = 20
      Width = 57
      Height = 17
      Caption = 'H1Sel'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
    object CBHRMS: TCheckBox
      Left = 8
      Top = 44
      Width = 65
      Height = 17
      Caption = 'HRMS'
      Checked = True
      State = cbChecked
      TabOrder = 1
    end
  end
  object CBTipoFile: TComboBox
    Left = 96
    Top = 75
    Width = 225
    Height = 21
    Style = csDropDownList
    ItemHeight = 13
    TabOrder = 7
    OnChange = CBTipoFileChange
  end
  object QtipiFile: TADOQuery
    Connection = Data.DB
    Parameters = <>
    Left = 384
    Top = 80
  end
end
