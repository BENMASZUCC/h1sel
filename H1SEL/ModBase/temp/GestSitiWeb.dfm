object GestSitiWebForm: TGestSitiWebForm
  Left = 300
  Top = 106
  Width = 549
  Height = 413
  Caption = 'Gestione siti web'
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 541
    Height = 41
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object SpeedButton1: TSpeedButton
      Left = 280
      Top = 4
      Width = 113
      Height = 34
      Caption = 'Carica immagine'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = SpeedButton1Click
    end
    object DBNavigator1: TDBNavigator
      Left = 5
      Top = 4
      Width = 225
      Height = 34
      DataSource = DsQWebLinks
      VisibleButtons = [nbInsert, nbDelete, nbEdit, nbPost, nbCancel]
      TabOrder = 0
    end
    object BitBtn1: TBitBtn
      Left = 447
      Top = 4
      Width = 89
      Height = 34
      Anchors = [akTop, akRight]
      Caption = 'Esci'
      TabOrder = 1
      Kind = bkOK
    end
  end
  object dxDBGrid1: TdxDBGrid
    Left = 0
    Top = 41
    Width = 541
    Height = 345
    Bands = <
      item
      end>
    DefaultLayout = True
    HeaderPanelRowCount = 1
    KeyField = 'ID'
    SummaryGroups = <>
    SummarySeparator = ', '
    Align = alClient
    TabOrder = 1
    DataSource = DsQWebLinks
    DefaultRowHeight = 40
    Filter.Criteria = {00000000}
    OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
    OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoIndicator, edgoUseBitmap]
    object dxDBGrid1ID: TdxDBGridMaskColumn
      Visible = False
      Width = 721
      BandIndex = 0
      RowIndex = 0
      FieldName = 'ID'
    end
    object dxDBGrid1Column4: TdxDBGridGraphicColumn
      Caption = 'Sito'
      Width = 143
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Logo'
      CustomGraphic = True
      DblClickActivate = False
      QuickDraw = True
      OnGetGraphicClass = dxDBGrid1Column4GetGraphicClass
    end
    object dxDBGrid1Descrizione: TdxDBGridMaskColumn
      Alignment = taLeftJustify
      VertAlignment = tlCenter
      Width = 315
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Descrizione'
    end
    object dxDBGrid1VisibileHome: TdxDBGridCheckColumn
      Caption = 'Visibile Home'
      Width = 67
      BandIndex = 0
      RowIndex = 0
      FieldName = 'VisibileHome'
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
  end
  object DsQWebLinks: TDataSource
    DataSet = QWebLinks
    Left = 48
    Top = 112
  end
  object QWebLinks: TADOQuery
    Active = True
    Connection = Data.DB
    CursorType = ctStatic
    BeforeDelete = QWebLinksBeforeDelete
    Parameters = <>
    SQL.Strings = (
      'select *'
      'from WebLinks')
    Left = 48
    Top = 80
    object QWebLinksID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QWebLinksDescrizione: TStringField
      FieldName = 'Descrizione'
      Size = 1024
    end
    object QWebLinksLogo: TBlobField
      FieldName = 'Logo'
      BlobType = ftBlob
    end
    object QWebLinksVisibileHome: TBooleanField
      FieldName = 'VisibileHome'
    end
  end
  object OpenDialog1: TOpenDialog
    Left = 64
    Top = 184
  end
end
