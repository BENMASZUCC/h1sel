object GestQualifContrForm: TGestQualifContrForm
  Left = 269
  Top = 194
  Width = 405
  Height = 405
  Caption = 
    'Gestione qualifiche contrattuali, tipi contratti e contratti naz' +
    'ionali'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 397
    Height = 48
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object BitBtn1: TBitBtn
      Left = 292
      Top = 5
      Width = 101
      Height = 38
      Anchors = [akTop, akRight]
      Caption = 'Esci'
      TabOrder = 0
      Kind = bkClose
    end
  end
  object PCUnico: TPageControl
    Left = 0
    Top = 48
    Width = 397
    Height = 330
    ActivePage = TSInsieme
    Align = alClient
    TabOrder = 1
    object TSInsieme: TTabSheet
      Caption = 'Qualifiche contrattuali'
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 389
        Height = 40
        Align = alTop
        BevelOuter = bvLowered
        TabOrder = 0
        object BNew: TToolbarButton97
          Left = 1
          Top = 1
          Width = 59
          Height = 37
          Caption = 'Nuova riga'
          WordWrap = True
          OnClick = BNewClick
        end
        object BDel: TToolbarButton97
          Left = 60
          Top = 1
          Width = 59
          Height = 37
          Caption = 'Cancella riga'
          WordWrap = True
          OnClick = BDelClick
        end
        object BOK: TToolbarButton97
          Left = 119
          Top = 1
          Width = 59
          Height = 37
          Caption = 'conferma modifiche'
          Enabled = False
          WordWrap = True
          OnClick = BOKClick
        end
        object BCan: TToolbarButton97
          Left = 178
          Top = 1
          Width = 59
          Height = 37
          Caption = 'annulla modifiche'
          Enabled = False
          WordWrap = True
          OnClick = BCanClick
        end
      end
      object dxDBGrid1: TdxDBGrid
        Left = 0
        Top = 40
        Width = 389
        Height = 262
        Bands = <
          item
          end>
        DefaultLayout = True
        HeaderPanelRowCount = 1
        KeyField = 'ID'
        SummaryGroups = <>
        SummarySeparator = ', '
        Align = alClient
        TabOrder = 1
        DataSource = DsQQualif
        Filter.Active = True
        Filter.Criteria = {00000000}
        OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
        OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoIndicator, edgoUseBitmap]
        object dxDBGrid1Qualifica: TdxDBGridMaskColumn
          Sorted = csUp
          Width = 126
          BandIndex = 0
          RowIndex = 0
          FieldName = 'Qualifica'
        end
        object dxDBGrid1TipoContratto: TdxDBGridLookupColumn
          Caption = 'Tipo di contratto'
          Width = 116
          BandIndex = 0
          RowIndex = 0
          FieldName = 'TipoContratto'
        end
        object dxDBGrid1ContrattoNaz: TdxDBGridLookupColumn
          Caption = 'Contratto nazionale'
          Width = 115
          BandIndex = 0
          RowIndex = 0
          FieldName = 'ContrattoNaz'
        end
      end
    end
    object TSTipiContratti: TTabSheet
      Caption = 'Tipi contratti'
      ImageIndex = 1
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 389
        Height = 40
        Align = alTop
        BevelOuter = bvLowered
        TabOrder = 0
        object B2New: TToolbarButton97
          Left = 1
          Top = 1
          Width = 59
          Height = 37
          Caption = 'Nuova riga'
          WordWrap = True
          OnClick = B2NewClick
        end
        object B2Del: TToolbarButton97
          Left = 60
          Top = 1
          Width = 59
          Height = 37
          Caption = 'Cancella riga'
          WordWrap = True
          OnClick = B2DelClick
        end
        object B2OK: TToolbarButton97
          Left = 119
          Top = 1
          Width = 59
          Height = 37
          Caption = 'conferma modifiche'
          Enabled = False
          WordWrap = True
          OnClick = B2OKClick
        end
        object B2Can: TToolbarButton97
          Left = 178
          Top = 1
          Width = 59
          Height = 37
          Caption = 'annulla modifiche'
          Enabled = False
          WordWrap = True
          OnClick = B2CanClick
        end
      end
      object dxDBGrid2: TdxDBGrid
        Left = 0
        Top = 40
        Width = 389
        Height = 262
        Bands = <
          item
          end>
        DefaultLayout = True
        HeaderPanelRowCount = 1
        KeyField = 'ID'
        SummaryGroups = <>
        SummarySeparator = ', '
        Align = alClient
        TabOrder = 1
        DataSource = DsQTipiContratti
        Filter.Criteria = {00000000}
        OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
        OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoIndicator, edgoUseBitmap]
        object dxDBGrid2TipoContratto: TdxDBGridMaskColumn
          Caption = 'Tipo di contratto'
          Sorted = csUp
          BandIndex = 0
          RowIndex = 0
          FieldName = 'TipoContratto'
        end
      end
    end
    object TSContrattiNaz: TTabSheet
      Caption = 'Contratti nazionali'
      ImageIndex = 2
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 389
        Height = 40
        Align = alTop
        BevelOuter = bvLowered
        TabOrder = 0
        object B3New: TToolbarButton97
          Left = 1
          Top = 1
          Width = 59
          Height = 37
          Caption = 'Nuova riga'
          WordWrap = True
          OnClick = B3NewClick
        end
        object B3Del: TToolbarButton97
          Left = 60
          Top = 1
          Width = 59
          Height = 37
          Caption = 'Cancella riga'
          WordWrap = True
          OnClick = B3DelClick
        end
        object B3OK: TToolbarButton97
          Left = 119
          Top = 1
          Width = 59
          Height = 37
          Caption = 'conferma modifiche'
          Enabled = False
          WordWrap = True
          OnClick = B3OKClick
        end
        object B3Can: TToolbarButton97
          Left = 178
          Top = 1
          Width = 59
          Height = 37
          Caption = 'annulla modifiche'
          Enabled = False
          WordWrap = True
          OnClick = B3CanClick
        end
      end
      object dxDBGrid3: TdxDBGrid
        Left = 0
        Top = 40
        Width = 389
        Height = 262
        Bands = <
          item
          end>
        DefaultLayout = True
        HeaderPanelRowCount = 1
        KeyField = 'ID'
        SummaryGroups = <>
        SummarySeparator = ', '
        Align = alClient
        TabOrder = 1
        DataSource = DsQContrattiNaz
        Filter.Criteria = {00000000}
        OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
        OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoIndicator, edgoUseBitmap]
        object dxDBGrid3Descrizione: TdxDBGridMaskColumn
          Caption = 'contratto nazionale'
          Sorted = csUp
          BandIndex = 0
          RowIndex = 0
          FieldName = 'Descrizione'
        end
      end
    end
  end
  object QQualif: TADOQuery
    Active = True
    Connection = Data.DB
    CursorType = ctStatic
    BeforeDelete = QQualifBeforeDelete
    Parameters = <>
    SQL.Strings = (
      'select * from QualificheContrattuali')
    Left = 44
    Top = 160
    object QQualifID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QQualifIDTipoContratto: TIntegerField
      FieldName = 'IDTipoContratto'
    end
    object QQualifQualifica: TStringField
      FieldName = 'Qualifica'
      Size = 40
    end
    object QQualifOrdine: TSmallintField
      FieldName = 'Ordine'
    end
    object QQualifIDContrattoNaz: TIntegerField
      FieldName = 'IDContrattoNaz'
    end
    object QQualifTipoContratto: TStringField
      FieldKind = fkLookup
      FieldName = 'TipoContratto'
      LookupDataSet = QTipiContrattiLK
      LookupKeyFields = 'ID'
      LookupResultField = 'TipoContratto'
      KeyFields = 'IDTipoContratto'
      Lookup = True
    end
    object QQualifContrattoNaz: TStringField
      FieldKind = fkLookup
      FieldName = 'ContrattoNaz'
      LookupDataSet = QContrattiNazLK
      LookupKeyFields = 'ID'
      LookupResultField = 'Descrizione'
      KeyFields = 'IDContrattoNaz'
      Size = 40
      Lookup = True
    end
  end
  object QTipiContratti: TADOQuery
    Active = True
    Connection = Data.DB
    CursorType = ctStatic
    AfterPost = QTipiContrattiAfterPost
    BeforeDelete = QTipiContrattiBeforeDelete
    Parameters = <>
    SQL.Strings = (
      'select * from TipiContratti')
    Left = 148
    Top = 160
    object QTipiContrattiID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QTipiContrattiTipoContratto: TStringField
      FieldName = 'TipoContratto'
    end
    object QTipiContrattiIDAzienda: TIntegerField
      FieldName = 'IDAzienda'
    end
  end
  object QContrattiNaz: TADOQuery
    Active = True
    Connection = Data.DB
    CursorType = ctStatic
    AfterPost = QContrattiNazAfterPost
    BeforeDelete = QContrattiNazBeforeDelete
    Parameters = <>
    SQL.Strings = (
      'select * from ContrattiNaz')
    Left = 252
    Top = 160
    object QContrattiNazID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QContrattiNazDescrizione: TStringField
      FieldName = 'Descrizione'
      Size = 40
    end
  end
  object DsQQualif: TDataSource
    DataSet = QQualif
    OnStateChange = DsQQualifStateChange
    Left = 44
    Top = 192
  end
  object DsQTipiContratti: TDataSource
    DataSet = QTipiContratti
    OnStateChange = DsQTipiContrattiStateChange
    Left = 148
    Top = 192
  end
  object DsQContrattiNaz: TDataSource
    DataSet = QContrattiNaz
    OnStateChange = DsQContrattiNazStateChange
    Left = 252
    Top = 192
  end
  object QTipiContrattiLK: TADOQuery
    Active = True
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from TipiContratti')
    Left = 28
    Top = 232
    object QTipiContrattiLKID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QTipiContrattiLKTipoContratto: TStringField
      FieldName = 'TipoContratto'
    end
    object QTipiContrattiLKIDAzienda: TIntegerField
      FieldName = 'IDAzienda'
    end
  end
  object QContrattiNazLK: TADOQuery
    Active = True
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from ContrattiNaz')
    Left = 60
    Top = 232
    object QContrattiNazLKID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QContrattiNazLKDescrizione: TStringField
      FieldName = 'Descrizione'
      Size = 40
    end
  end
end
