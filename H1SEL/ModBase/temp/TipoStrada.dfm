object TipoStradaForm: TTipoStradaForm
  Left = 219
  Top = 106
  ActiveControl = ETipo
  BorderStyle = bsDialog
  Caption = 'Tipo di strada'
  ClientHeight = 243
  ClientWidth = 287
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 3
    Top = 8
    Width = 67
    Height = 13
    Caption = 'Tipo di strada:'
  end
  object BitBtn1: TBitBtn
    Left = 191
    Top = 3
    Width = 94
    Height = 37
    TabOrder = 2
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 191
    Top = 41
    Width = 94
    Height = 37
    Caption = 'Annulla'
    TabOrder = 3
    Kind = bkCancel
  end
  object DBGrid1: TDBGrid
    Left = 4
    Top = 30
    Width = 183
    Height = 209
    DataSource = DsQNazioni
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'Abbrev'
        Title.Caption = 'Abbrev.'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DescNazione'
        Title.Caption = 'Nazione'
        Width = 107
        Visible = True
      end>
  end
  object ETipo: TEdit
    Left = 75
    Top = 5
    Width = 112
    Height = 21
    MaxLength = 10
    TabOrder = 0
  end
  object QNazioni_OLD: TQuery
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select * from Nazioni'
      'order by Abbrev')
    Left = 56
    Top = 88
    object QNazioni_OLDID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.Nazioni.ID'
    end
    object QNazioni_OLDAbbrev: TStringField
      FieldName = 'Abbrev'
      Origin = 'EBCDB.Nazioni.Abbrev'
      FixedChar = True
      Size = 3
    end
    object QNazioni_OLDDescNazione: TStringField
      FieldName = 'DescNazione'
      Origin = 'EBCDB.Nazioni.DescNazione'
      FixedChar = True
      Size = 30
    end
    object QNazioni_OLDAreaGeografica: TStringField
      FieldName = 'AreaGeografica'
      Origin = 'EBCDB.Nazioni.AreaGeografica'
      FixedChar = True
    end
  end
  object DsQNazioni: TDataSource
    DataSet = QNazioni
    Left = 56
    Top = 120
  end
  object QNazioni: TADOLinkedQuery
    Active = True
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from Nazioni'
      'order by Abbrev')
    Left = 56
    Top = 56
  end
end
