object SelAttivitaForm: TSelAttivitaForm
  Left = 233
  Top = 110
  ActiveControl = dxDBGrid1
  BorderStyle = bsDialog
  Caption = 'Selezione attivit�'
  ClientHeight = 337
  ClientWidth = 672
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object BitBtn1: TBitBtn
    Left = 580
    Top = 3
    Width = 91
    Height = 35
    TabOrder = 0
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 580
    Top = 40
    Width = 91
    Height = 35
    Caption = 'Annulla'
    TabOrder = 1
    Kind = bkCancel
  end
  object dxDBGrid1: TdxDBGrid
    Left = 4
    Top = 4
    Width = 570
    Height = 329
    Bands = <
      item
      end>
    DefaultLayout = True
    HeaderPanelRowCount = 1
    KeyField = 'ID'
    ShowGroupPanel = True
    SummaryGroups = <>
    SummarySeparator = ', '
    TabOrder = 2
    DataSource = DsSettori
    Filter.Criteria = {00000000}
    OptionsBehavior = [edgoAutoSearch, edgoAutoSort, edgoDragScroll, edgoEditing, edgoEnterShowEditor, edgoImmediateEditor, edgoTabThrough, edgoVertThrough]
    OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
    OptionsView = [edgoBandHeaderWidth, edgoIndicator, edgoUseBitmap]
    RegistryPath = '\Software\H1\DbGridSelAttivita'
    object dxDBGrid1Attivita: TdxDBGridMaskColumn
      Sorted = csUp
      Width = 201
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Attivita'
    end
    object dxDBGrid1Column3: TdxDBGridColumn
      Caption = 'Attivita (ENG)'
      Width = 192
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Attivita_ENG'
    end
    object dxDBGrid1AreaSettore: TdxDBGridMaskColumn
      Caption = 'Area'
      Width = 144
      BandIndex = 0
      RowIndex = 0
      FieldName = 'AreaSettore'
    end
  end
  object DsSettori: TDataSource
    DataSet = TSettori
    Left = 48
    Top = 112
  end
  object Timer1: TTimer
    OnTimer = Timer1Timer
    Left = 120
    Top = 232
  end
  object TSettori: TADOLinkedQuery
    Active = True
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select EBC_Attivita.*, AreaSettore'
      'from EBC_Attivita, AreeSettori'
      'where EBC_Attivita.IDAreaSettore *= AreeSettori.ID'
      'order by attivita')
    UseFilter = False
    Left = 48
    Top = 48
  end
end
