object FrameWebLinks: TFrameWebLinks
  Left = 0
  Top = 0
  Width = 475
  Height = 356
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  ParentFont = False
  TabOrder = 0
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 475
    Height = 41
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object SpeedButton1: TSpeedButton
      Left = 381
      Top = 4
      Width = 89
      Height = 33
      Anchors = [akTop, akRight]
      Caption = 'Gestione siti'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      OnClick = SpeedButton1Click
    end
    object BCan: TToolbarButton97
      Left = 80
      Top = 1
      Width = 77
      Height = 37
      Caption = 'Annulla modifiche'
      Enabled = False
      Opaque = False
      WordWrap = True
      OnClick = BCanClick
    end
    object BOK: TToolbarButton97
      Left = 3
      Top = 1
      Width = 77
      Height = 37
      Caption = 'Conferma modifiche'
      Enabled = False
      Opaque = False
      WordWrap = True
      OnClick = BOKClick
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 41
    Width = 475
    Height = 315
    Align = alClient
    BevelOuter = bvLowered
    TabOrder = 1
    object dxDBGrid1: TdxDBGrid
      Left = 1
      Top = 1
      Width = 473
      Height = 313
      Bands = <
        item
        end>
      DefaultLayout = True
      HeaderPanelRowCount = 1
      KeyField = 'ID'
      SummaryGroups = <>
      SummarySeparator = ', '
      Align = alClient
      TabOrder = 0
      DataSource = DsQWebLinks
      DefaultRowHeight = 35
      Filter.Criteria = {00000000}
      OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
      OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoIndicator, edgoUseBitmap]
      object dxDBGrid1ID: TdxDBGridMaskColumn
        Visible = False
        Width = 650
        BandIndex = 0
        RowIndex = 0
        FieldName = 'ID'
      end
      object dxDBGrid1Column4: TdxDBGridGraphicColumn
        Caption = 'Sito'
        Width = 116
        BandIndex = 0
        RowIndex = 0
        FieldName = 'Logo'
        CustomGraphic = True
        DblClickActivate = False
        QuickDraw = True
        ShadowSelection = False
        OnGetGraphicClass = dxDBGrid1Column4GetGraphicClass
      end
      object dxDBGrid1Descrizione: TdxDBGridMaskColumn
        Alignment = taLeftJustify
        VertAlignment = tlCenter
        Visible = False
        Width = 167
        BandIndex = 0
        RowIndex = 0
        FieldName = 'Descrizione'
      end
      object dxDBGrid1Column5: TdxDBGridHyperLinkColumn
        VertAlignment = tlCenter
        Width = 341
        BandIndex = 0
        RowIndex = 0
        FieldName = 'Link'
      end
    end
  end
  object DsQWebLinks: TDataSource
    DataSet = QWebLinks
    OnStateChange = DsQWebLinksStateChange
    Left = 64
    Top = 169
  end
  object QWebLinks: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    BeforeOpen = QWebLinksBeforeOpen
    Parameters = <
      item
        Name = 'x'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 1
      end>
    SQL.Strings = (
      'select *'
      'from AnagWebLinks AWL'
      'join WebLinks WL on AWL.IDWebLink = WL.ID'
      'where AWL.IDAnagrafica=:x')
    Left = 64
    Top = 137
    object QWebLinksID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QWebLinksDescrizione: TStringField
      FieldName = 'Descrizione'
      Size = 1024
    end
    object QWebLinksLogo: TBlobField
      FieldName = 'Logo'
      BlobType = ftBlob
    end
    object QWebLinksLink: TStringField
      DisplayWidth = 512
      FieldName = 'Link'
      Size = 512
    end
  end
  object OpenDialog1: TOpenDialog
    Left = 40
    Top = 225
  end
end
