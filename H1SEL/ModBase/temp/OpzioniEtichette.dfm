object OpzioniEtichetteForm: TOpzioniEtichetteForm
  Left = 261
  Top = 105
  ActiveControl = RGStampa
  BorderStyle = bsDialog
  Caption = 'Opzioni etichette'
  ClientHeight = 95
  ClientWidth = 304
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object BitBtn1: TBitBtn
    Left = 208
    Top = 2
    Width = 93
    Height = 36
    TabOrder = 2
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 208
    Top = 40
    Width = 93
    Height = 36
    Caption = 'Annulla'
    TabOrder = 3
    Kind = bkCancel
  end
  object CBStoricizza: TCheckBox
    Left = 5
    Top = 76
    Width = 196
    Height = 17
    Caption = 'Storicizza l'#39'invio per il o i destinatari'
    TabOrder = 1
  end
  object RGStampa: TRadioGroup
    Left = 4
    Top = 0
    Width = 137
    Height = 55
    Caption = 'Opzione di stampa'
    ItemIndex = 0
    Items.Strings = (
      'anteprima di stampa'
      'stampa diretta')
    TabOrder = 0
  end
  object CBNote: TCheckBox
    Left = 5
    Top = 58
    Width = 134
    Height = 17
    Caption = 'visualizza campo note'
    TabOrder = 4
  end
end
