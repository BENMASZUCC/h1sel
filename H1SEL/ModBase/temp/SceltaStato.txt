object SceltaStatoForm: TSceltaStatoForm
  Left = 288
  Top = 194
  Width = 252
  Height = 403
  HelpContext = 100054
  ActiveControl = ECogn
  Caption = 'Nuovo inserimento'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 0
    Top = 182
    Width = 244
    Height = 138
    Align = alClient
    DataSource = DsQStati
    ReadOnly = True
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'Stato'
        Width = 134
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TipoStato'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Title.Caption = 'Tipo stato'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clMaroon
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 72
        Visible = True
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 244
    Height = 161
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Label3: TLabel
      Left = 7
      Top = 53
      Width = 48
      Height = 13
      Caption = 'Cognome:'
    end
    object Label4: TLabel
      Left = 6
      Top = 77
      Width = 31
      Height = 13
      Caption = 'Nome:'
    end
    object SpeedButton1: TSpeedButton
      Left = 1
      Top = 2
      Width = 20
      Height = 20
      Glyph.Data = {
        AA030000424DAA03000000000000360000002800000011000000110000000100
        1800000000007403000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF00FFFFFFFFFFFFFFFFFFDEDFDEBDAEA5EFDFD6F7EFE7F7EFE7F7EFE7E7D7CE
        7B696B525152DEDFDEFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFF7F7EF
        E7DEF7F7F7E7DFDECEB6A5DEB6A5E7CFBDEFEFEFF7F7F7DECFC6393031CECFCE
        FFFFFFFFFFFFFFFFFF00FFFFFFF7F7EFF7F7EFF7F7EFD69E7BC66131C6714ADE
        B6A5C66131C66131DEB69CF7F7F7EFDFD6393831FFFFFFFFFFFFFFFFFF00FFFF
        FFF7E7DEF7F7F7CE795AC66129CE6129C69E84FFFFFFDE966BCE6131C65929D6
        9E7BF7F7F7C6B6B5848684FFFFFFFFFFFF00F7EFE7FFFFFFDEAE8CCE6129CE61
        31CE6131CE6939DE8E63CE6129CE6131CE6131C65929E7C7B5F7EFEF524942FF
        FFFFFFFFFF00F7DFD6FFF7F7CE6939CE6931CE6131CE6131CE8663F7E7D6D671
        42CE6131CE6131CE6131CE7952F7FFFFA5968CADAEADFFFFFF00FFEFEFF7DFCE
        CE6131D66931CE6131CE6131C6795AFFFFFFE7A684CE6129CE6131CE6931CE69
        31F7F7EFDECFBD9C9E9CFFFFFF00FFEFEFF7D7C6D66931D66931CE6131CE6131
        CE6131D6AE9CFFFFFFE79E73CE6129CE6931CE6931F7EFEFDECFC69C9E9CFFFF
        FF00FFEFEFFFE7DEE77142DE6939CE6129CE6129CE6131CE6129E7C7BDFFFFF7
        D67139D66939D67142FFF7F7D6C7BDB5B6B5FFFFFF00F7E7DEFFFFFFF79E6BEF
        7942D68E6BEFDFCEDE7952CE6129DEA684FFFFFFDE8E63DE7139E79E73FFFFFF
        A5968CFFFFFFFFFFFF00F7EFE7FFFFFFFFE7CEFF9E63E78E63EFEFEFFFEFE7EF
        BEA5FFF7EFF7EFEFE78652EF8652FFEFDEFFF7EFC6BEBDFFFFFFFFFFFF00FFFF
        FFF7E7DEFFFFFFFFE7C6FFBE84EFBE94EFE7DEE7E7E7EFE7DEFFB68CFF9E6BFF
        DFC6FFFFFFDECFC6FFFFFFFFFFFFFFFFFF00FFFFFFF7F7EFF7E7E7FFFFFFFFFF
        EFFFF7CEFFE7B5FFD7A5FFD79CFFDFB5FFF7EFFFFFFFEFDFDEEFEFEFFFFFFFFF
        FFFFFFFFFF00FFFFFFFFFFFFF7F7EFF7E7D6FFFFF7FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFF7F7F7EFE7F7F7EFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF
        FFFFFFFFFFFFF7EFE7F7E7DEFFEFE7FFEFEFFFEFE7EFE7DEFFF7F7FFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFF00}
      OnClick = SpeedButton1Click
    end
    object BitBtn1: TBitBtn
      Left = 62
      Top = 4
      Width = 88
      Height = 38
      TabOrder = 2
      Kind = bkOK
    end
    object BitBtn2: TBitBtn
      Left = 152
      Top = 4
      Width = 88
      Height = 38
      Caption = 'Annulla'
      TabOrder = 3
      Kind = bkCancel
    end
    object ECogn: TEdit
      Left = 62
      Top = 49
      Width = 176
      Height = 21
      CharCase = ecUpperCase
      Color = clYellow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object ENome: TEdit
      Left = 62
      Top = 73
      Width = 176
      Height = 21
      CharCase = ecUpperCase
      Color = clYellow
      TabOrder = 1
    end
    object PanAnn: TPanel
      Left = 0
      Top = 96
      Width = 244
      Height = 65
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 4
      object Label2: TLabel
        Left = 6
        Top = 39
        Width = 77
        Height = 13
        Caption = 'Data pervenuto:'
      end
      object Label5: TLabel
        Left = 8
        Top = 13
        Width = 28
        Height = 13
        Caption = 'Email:'
      end
      object DataPervenuto: TDateEdit97
        Left = 93
        Top = 35
        Width = 88
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        ColorCalendar.ColorValid = clBlue
        Date = 36161
        DayNames.Monday = 'lu'
        DayNames.Tuesday = 'ma'
        DayNames.Wednesday = 'me'
        DayNames.Thursday = 'gi'
        DayNames.Friday = 've'
        DayNames.Saturday = 'sa'
        DayNames.Sunday = 'do'
        MonthNames.January = 'gennaio'
        MonthNames.February = 'febbraio'
        MonthNames.March = 'marzo'
        MonthNames.April = 'aprile'
        MonthNames.May = 'maggio'
        MonthNames.June = 'giugno'
        MonthNames.July = 'luglio'
        MonthNames.August = 'agosto'
        MonthNames.September = 'settembre'
        MonthNames.October = 'ottobre'
        MonthNames.November = 'novembre'
        MonthNames.December = 'dicembre'
        Options = [doButtonTabStop, doCanPopup, doIsMasked, doShowCancel, doShowToday]
      end
      object EDEmail: TEdit
        Left = 63
        Top = 7
        Width = 177
        Height = 21
        TabOrder = 1
      end
    end
  end
  object Panel73: TPanel
    Left = 0
    Top = 161
    Width = 244
    Height = 21
    Align = alTop
    Alignment = taLeftJustify
    Caption = '  Scelta dello stato iniziale'
    Color = clGray
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
  end
  object Panel2: TPanel
    Left = 0
    Top = 320
    Width = 244
    Height = 56
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 3
    object Label1: TLabel
      Left = 6
      Top = 8
      Width = 79
      Height = 13
      Caption = 'Provenienza CV:'
    end
    object EProvCV: TEdit
      Left = 5
      Top = 24
      Width = 232
      Height = 21
      TabOrder = 0
    end
  end
  object DsQStati: TDataSource
    DataSet = QStati
    Left = 48
    Top = 208
  end
  object QStati: TADOLinkedQuery
    Active = True
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * '
      'from EBC_Stati, EBC_TipiStato'
      'where EBC_Stati.IDTipoStato=EBC_TipiStato.ID'
      'and PRMan is null')
    UseFilter = False
    Left = 48
    Top = 176
    object QStatiID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QStatiStato: TStringField
      FieldName = 'Stato'
      Size = 30
    end
    object QStatiIDTipoStato: TIntegerField
      FieldName = 'IDTipoStato'
    end
    object QStatiTipoStato: TStringField
      FieldName = 'TipoStato'
    end
  end
end
