€
 TSELEDIZFORM 0„6  TPF0TSelEdizFormSelEdizFormLeftTop}BorderStylebsDialogCaption@Nuova pubblicazione:  selezione data, testata, edizione e prezziClientHeight)ClientWidthіColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TBitBtnBitBtn1LeftцTopWidth\Height"TabOrder KindbkOK  TBitBtnBitBtn2LeftVTopWidth\Height"CaptionAnnullaTabOrderKindbkCancel  TPanelPanDataLeftTopWidth— Height!
BevelOuter	bvLoweredTabOrder TLabelLabel3LeftTop
Width\HeightCaptionData pubbicazione:  TDateEdit97DEDataLefthTopWidthdHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder ColorCalendar.ColorValidclBlueDayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanPopup
doIsMaskeddoShowCanceldoShowToday    	TGroupBox	GroupBox1LeftKToplWidth HeightЇ Caption)Prezzi praticati per questa pubblicazioneTabOrder TLabelLabel1LeftTop"WidthOHeightCaptionPrezzo di Listino:  TLabelLabel4LeftTop:Width=HeightCaptionPrezzo a noi:  TLabelLabel5Left
TopiWidthPHeightCaptionPrezzo al cliente:  TLabelLabel6Left« Top
WidthHeightCaptionLireFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameArial
Font.StylefsItalic 
ParentFont  TLabelLabel7LeftBTop
WidthHeightCaptionEuroFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameArial
Font.StylefsItalic 
ParentFont  TBevelBevel1Left`TopWidthzHeightShape	bsTopLine  TBevelBevel2Leftа TopWidthzHeightShape	bsTopLine  TLabelLabel2LeftTopRWidth<HeightCaptionSconto a noi  TLabelLabel8LeftTopВ WidthRHeightCaptionSconto al cliente:  TLabelLabel9LeftTop£ Width?HeightCaption	Guadagno:Font.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TRxCalcEditListinoLireLeft`TopWidth{HeightAutoSizeDisplayFormat£'.' ,0.FormatOnEditing		NumGlyphsTabOrder VisibleOnChangeListinoLireChange  TRxCalcEditAnoiLireLeft`Top6Width{HeightAutoSizeDisplayFormat£'.' ,0.FormatOnEditing		NumGlyphsTabOrderVisibleOnChangeAnoiLireChange  TRxCalcEditAlClienteLireLeft`TopfWidth{HeightAutoSizeDisplayFormat£'.' ,0.Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold FormatOnEditing		NumGlyphs
ParentFontTabOrderVisibleOnChangeAlClienteLireChange  TRxCalcEditListinoEuroLeftя TopWidth{HeightAutoSizeDisplayFormat
А'.' ,0.00FormatOnEditing		NumGlyphsTabOrderOnChangeListinoEuroChange  TRxCalcEditAnoiEuroLeftя Top6Width{HeightAutoSizeDisplayFormat
А'.' ,0.00FormatOnEditing		NumGlyphsTabOrderOnChangeAnoiEuroChange  TRxCalcEditAlClienteEuroLeftя TopfWidth{HeightAutoSizeDisplayFormat
А'.' ,0.00Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold FormatOnEditing		NumGlyphs
ParentFontTabOrderOnChangeAlClienteEuroChange  TPanelPanel1Left_TopWidthdHeightЬ 
BevelOuter	bvLoweredTabOrder TToolbarButton97BCopiaValoriLeftTopWidthbHeight$Captioncopia valori da listino
Glyph.Data
:  6  BM6      6  (                                   А  А   АА А   А А АА  јјј ј№ј р ¶   @   `   А   †   ј   а  @   @   @@  @`  @А  @†  @ј  @а  `   `   `@  ``  `А  `†  `ј  `а  А   А   А@  А`  АА  А†  Ај  Аа  †   †   †@  †`  †А  ††  †ј  †а  ј   ј   ј@  ј`  јА  ј†  јј  ја  а   а   а@  а`  аА  а†  ај  аа @   @   @ @ @ ` @ А @ † @ ј @ а @   @   @ @ @ ` @ А @ † @ ј @ а @@  @@  @@@ @@` @@А @@† @@ј @@а @`  @`  @`@ @`` @`А @`† @`ј @`а @А  @А  @А@ @А` @АА @А† @Ај @Аа @†  @†  @†@ @†` @†А @†† @†ј @†а @ј  @ј  @ј@ @ј` @јА @ј† @јј @ја @а  @а  @а@ @а` @аА @а† @ај @аа А   А   А @ А ` А А А † А ј А а А   А   А @ А ` А А А † А ј А а А@  А@  А@@ А@` А@А А@† А@ј А@а А`  А`  А`@ А`` А`А А`† А`ј А`а АА  АА  АА@ АА` ААА АА† ААј ААа А†  А†  А†@ А†` А†А А†† А†ј А†а Ај  Ај  Ај@ Ај` АјА Ај† Ајј Аја Аа  Аа  Аа@ Аа` АаА Аа† Аај Ааа ј   ј   ј @ ј ` ј А ј † ј ј ј а ј   ј   ј @ ј ` ј А ј † ј ј ј а ј@  ј@  ј@@ ј@` ј@А ј@† ј@ј ј@а ј`  ј`  ј`@ ј`` ј`А ј`† ј`ј ј`а јА  јА  јА@ јА` јАА јА† јАј јАа ј†  ј†  ј†@ ј†` ј†А ј†† ј†ј ј†а јј  јј  јј@ јј` јјА јј† ры€ §†† ААА   €  €   €€ €   € € €€  €€€ „„„„щщ „„„„„„„„„„„„щщщ        „„„„щщщщщщщщщщщ „щщщщщщщщщщщщ €„„щщщщщщщщщщщ €„  щщщ €€щщщ €„ €€щщ € щщщ €„ €  щ €€щщщ €„ €€€€€€ щщщ „ €    €€щщщ „„ €€€€€€€щщщ „„„ €  € щщщ „„„ €€€€ € „щщщ „„„ €€€€  „„щщщ „„„      „„„щщщ„„„„„„„„„„„„„„„„„„„WordWrap	OnClickBCopiaValoriClick  TToolbarButton97ToolbarButton972LeftTop&WidthbHeight$CaptionStorico prezzi al cliente
Glyph.Data
:  6  BM6      6  (                                 €  €€  ДДД ∆∆∆ ∆B€  €€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€ €€€                                         WordWrap	OnClickToolbarButton972Click   TRxCalcEditScontoAnoiLireLeft`TopNWidth{HeightAutoSizeColor€сћ DisplayFormat£'.' ,0.EnabledFormatOnEditing		NumGlyphsTabOrderVisibleOnChangeScontoAnoiLireChange  TRxCalcEditScontoAnoiEuroLeftя TopNWidth{HeightAutoSizeColor€сћ DisplayFormat
А'.' ,0.00EnabledFormatOnEditing		NumGlyphsTabOrder  TRxCalcEditScontoAlClienteLireLeft`Top~Width{HeightAutoSizeColor€’€ DisplayFormat£'.' ,0.EnabledFormatOnEditing		NumGlyphsTabOrder	VisibleOnChangeScontoAlClienteLireChange  TRxCalcEditScontoAlClienteEuroLeftя Top~Width{HeightAutoSizeColor€’€ DisplayFormat
А'.' ,0.00EnabledFormatOnEditing		NumGlyphsTabOrder
  TRxCalcEditGuadagnoLireLeft`TopЯ Width{HeightAutoSizeColorclAquaDisplayFormat£'.' ,0.EnabledFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold FormatOnEditing		NumGlyphs
ParentFontTabOrderVisible  TRxCalcEditGuadagnoEuroLeftя TopЯ Width{HeightAutoSizeColorclAquaDisplayFormat
А'.' ,0.00EnabledFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold FormatOnEditing		NumGlyphs
ParentFontTabOrder   	TGroupBox	GroupBox2LeftToplWidthAHeight/Caption
N∞ moduli:TabOrder TRxSpinEditSENumModuliLeftTopWidth2HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightу	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder    TRadioGroupRGOpzioneListinoLeftToplWidthЧ Height8CaptionOpzione copia da listino	ItemIndex Items.Stringscalcola quantitаriga selezionata TabOrder  TPageControl	PCListiniLeftэTop+WidthЃHeight>
ActivePage	TSListinoTabOrder
OnChangingPCListiniChanging 	TTabSheet	TSListinoCaptionListini testate фTListinoAnnunciFrameListinoAnnunciFrame1Width¶Height"AlignalClient с	TSplitter	Splitter1Height"  сTPanelPanel2Height" сTDBGridDBGrid1Height   сTPanelPanel4Width”Height" сTPanelPanel5Width—  сTPanelPanel1Width— сTToolbarButton97ToolbarButton972OnClick)ListinoAnnunciFrame1ToolbarButton972Click  сTToolbarButton97ToolbarButton973OnClick)ListinoAnnunciFrame1ToolbarButton973Click  сTToolbarButton97ToolbarButton974OnClick)ListinoAnnunciFrame1ToolbarButton974Click   сTPageControlPCValutaWidth—Height»  с	TTabSheetTSLire с	TRxDBGrid	RxDBGrid1Width…HeightWColumnsExpanded	FieldName	DallaDataPickList.Strings Title.CaptionDalWidth>Visible	 Expanded	FieldNameQtaDaPickList.Strings Title.AlignmenttaRightJustifyTitle.CaptionDaWidthVisible	 Expanded	FieldNameQtaAPickList.Strings Title.AlignmenttaRightJustifyTitle.CaptionAWidthVisible	 Colorщщ– Expanded	FieldNamePrezzoListinoLirePickList.Strings Title.AlignmenttaRightJustifyTitle.CaptionListinoWidth;Visible	 Expanded	FieldNamePrezzoANoiLireFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold PickList.Strings Title.AlignmenttaRightJustifyTitle.CaptionA noiWidth<Visible	 Expanded	FieldNamePrezzoAlClienteLireFont.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Heightх	Font.NameMS Sans Serif
Font.Style PickList.Strings Title.AlignmenttaRightJustifyTitle.Caption
Al clienteWidth:Visible	 Expanded	FieldNameDiffLireFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold PickList.Strings Title.AlignmenttaRightJustifyTitle.CaptionGuadagnoWidth<Visible	 Expanded	FieldNamePrezzoQuestoClienteLireFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Heightх	Font.NameMS Sans Serif
Font.Style PickList.Strings Title.Captionquesto clienteTitle.Font.CharsetDEFAULT_CHARSETTitle.Font.ColorclPurpleTitle.Font.HeightхTitle.Font.NameMS Sans SerifTitle.Font.Style WidthGVisible	    сTPanelPanel7TopWWidth… сTPanelPanel8Width«    с	TTabSheetTSEuro сTPanelPanel9TopVWidth… сTPanelPanel10Width«   с	TRxDBGrid	RxDBGrid2Width…HeightVColumnsExpanded	FieldName	DallaDataPickList.Strings Title.CaptionDalWidth>Visible	 Expanded	FieldNameQtaDaPickList.Strings Title.AlignmenttaRightJustifyTitle.CaptionDaWidthVisible	 Expanded	FieldNameQtaAPickList.Strings Title.AlignmenttaRightJustifyTitle.CaptionAWidthVisible	 Colorщщ– Expanded	FieldNamePrezzoListinoEuroPickList.Strings Title.AlignmenttaRightJustifyTitle.CaptionListinoWidth:Visible	 Expanded	FieldNamePrezzoANoiEuroFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold PickList.Strings Title.AlignmenttaRightJustifyTitle.CaptionA noiWidth<Visible	 Expanded	FieldNamePrezzoAlClienteEuroFont.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Heightх	Font.NameMS Sans Serif
Font.Style PickList.Strings Title.AlignmenttaRightJustifyTitle.Caption
Al clienteWidth;Visible	 Expanded	FieldNameDiffEuroFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Heightх	Font.NameMS Sans Serif
Font.StylefsBold PickList.Strings Title.AlignmenttaRightJustifyTitle.CaptionGuadagnoWidth<Visible	 Expanded	FieldNamePrezzoQuestoClienteEuroFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Heightх	Font.NameMS Sans Serif
Font.Style PickList.Strings Title.Captionquesto clienteTitle.Font.CharsetDEFAULT_CHARSETTitle.Font.ColorclPurpleTitle.Font.HeightхTitle.Font.NameMS Sans SerifTitle.Font.Style WidthEVisible	      сTPanelPanel6Width—   сTADOLinkedQueryQListinoEdizSQL.Strings MasterDataSetLinkedMasterField LinkedDetailField   сTADOLinkedQuery	QEdizioniAfterScroll(ListinoAnnunciFrame1QEdizioniAfterScroll    	TTabSheetTSContrattiCaptionContratti in essere
ImageIndex фTContrattiAnnunciFrameContrattiAnnunciFrame1Width¶Height"AlignalClient сTPanelPanel93Width¶  с
TWallpaperWallpaper34Width¶  сTDBGridDBGrid48Width¶Heightд      TPanelPanel2Leftџ TopWidth∆ Height!
BevelOuter	bvLoweredTabOrder TLabelLabel10LeftTop
WidthKHeightCaptionData scadenza:  TDateEdit97DEDataScadenzaLeftXTopWidthdHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightх	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder ColorCalendar.ColorValidclBlueDayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanPopup
doIsMaskeddoShowCanceldoShowToday     