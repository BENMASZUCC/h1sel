�
 TDETTESPLAVFORM 0f  TPF0TDettEspLavFormDettEspLavFormLeftbTop� WidthNHeight�Caption*Esperienze lavorative - Dettagli ulterioriColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrder	PositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TLabelLabel44LeftTop3WidthfHeightCaptionDescrizione posizioneFocusControlDBMemo2  TLabelLabel45LeftTop� Width� HeightCaptionMotivazione cambio di lavoroFocusControlDBMemo3  TLabelLabel2LeftTop� Width� HeightCaptionDescrizione attivit� aziendaFocusControlDBMemo1  TLabelLabel5Left� Top� Width� HeightCaption,attenzione:  per ora � collegato al soggettoFont.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont  TLabelLabel6LeftTop�Width� HeightCaption*Ruolo temporaneo (indicato dal candidato):Font.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLabel7LeftTop�Width� HeightCaption&Ruolo indicato da form autocandidatura  TLabelLabel8LeftTopWidth?HeightCaptionNote generali  TDBMemoDBMemo2LeftTopBWidth�HeightO	DataFieldDescrizioneMansione
DataSourceData.DsEspLavTabOrder   TDBMemoDBMemo3LeftTop� Width�Height(	DataFieldMotivoCessazione
DataSourceData.DsEspLavTabOrder  TBitBtnBitBtn1Left�TopWidthYHeight!TabOrderOnClickBitBtn1ClickKindbkOK  TPanelPanel1LeftTopWidth�Height*
BevelOuter	bvLoweredEnabledTabOrder TLabelLabel1LeftTopWidth)HeightCaptionAzienda:  TLabelLabel36LeftTopWidth"HeightCaptionSettoreFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TDBEditDBEdit23LeftTopWidth HeightColorclLime	DataFieldAzDenominazione
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder   TDBEditDBEdit27LeftTopWidth� Height	DataFieldSettore
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   TBitBtnBitBtn2Left�Top&WidthYHeight!CaptionAnnullaTabOrderKindbkCancel  TDBMemoDBMemo1LeftTop� Width�Height.	DataFieldDescrizioneAttivitaAz
DataSourceData.DsEspLavTabOrder  	TGroupBox	GroupBox2LeftTop[Width~HeightKCaption	PreavvisoTabOrder TLabelLabel3LeftTopWidthHeightCaptionGiorni  TLabelLabel4LeftTop2WidthbHeightCaptionTermine di preavviso  TDBCheckBoxDBCheckBox1LeftfTopWidthHeightCaption8di calendario (se non selezionato = di lavoro effettivo)	DataFieldPreavvisoCalendario
DataSourceData.DsEspLavTabOrder ValueCheckedTrueValueUncheckedFalse  TDBCheckBoxDBCheckBox2Left� Top1Width� HeightCaptionQualsiasi giorno del mese	DataFieldPreavvisoTermQualsGiorno
DataSourceData.DsEspLavTabOrderValueCheckedTrueValueUncheckedFalse  TdxDBSpinEditdxDBSpinEdit1Left'TopWidth9TabOrder	DataFieldPreavvisoGiorni
DataSourceData.DsEspLav  TdxDBSpinEditdxDBSpinEdit2LeftoTop/Width9TabOrder	DataFieldPreavvisoTermGiorno
DataSourceData.DsEspLav   TDBEditDBEdit1Left� Top�Width� Height	DataFieldRuolo
DataSourceData.DsEspLavFont.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontReadOnly	TabOrder  TDBEditDBEdit2Left� Top�Width� Height	DataFieldTitoloMansione
DataSourceData.DsEspLavTabOrder  TDBMemoDBMemo4LeftTop Width�Height1	DataFieldNote
DataSourceData.DsEspLavTabOrder	   