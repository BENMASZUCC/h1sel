object NuovaValutazCandForm: TNuovaValutazCandForm
  Left = 236
  Top = 103
  Width = 523
  Height = 462
  Caption = 'Nuova valutazione candidato'
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 515
    Height = 41
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object BitBtn1: TBitBtn
      Left = 423
      Top = 4
      Width = 87
      Height = 34
      Anchors = [akTop, akRight]
      Caption = 'Esci'
      TabOrder = 0
      Kind = bkOK
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 41
    Width = 515
    Height = 163
    Align = alTop
    BevelOuter = bvLowered
    Caption = 'Panel2'
    TabOrder = 1
    object Label1: TLabel
      Left = 6
      Top = 6
      Width = 100
      Height = 13
      Caption = 'Scelta del candidato:'
    end
    object BitBtn2: TBitBtn
      Left = 7
      Top = 132
      Width = 105
      Height = 25
      Caption = 'Procedi'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = BitBtn2Click
    end
    object dxDBGrid4: TdxDBGrid
      Left = 6
      Top = 22
      Width = 267
      Height = 105
      Bands = <
        item
        end>
      DefaultLayout = True
      HeaderPanelRowCount = 1
      KeyField = 'ID'
      SummaryGroups = <>
      SummarySeparator = ', '
      TabOrder = 1
      DataSource = DsQCandRic
      Filter.Criteria = {00000000}
      OptionsBehavior = [edgoAutoSort, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoStoreToRegistry, edgoTabThrough, edgoVertThrough]
      OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
      OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoIndicator, edgoUseBitmap]
      RegistryPath = '\Software\H1\DbGridSelPersTLCand'
      Anchors = [akLeft, akTop, akBottom]
      object dxDBGrid4ID: TdxDBGridMaskColumn
        Visible = False
        BandIndex = 0
        RowIndex = 0
        FieldName = 'ID'
      end
      object dxDBGrid4Cognome: TdxDBGridMaskColumn
        Sorted = csUp
        BandIndex = 0
        RowIndex = 0
        FieldName = 'Cognome'
      end
      object dxDBGrid4Nome: TdxDBGridMaskColumn
        BandIndex = 0
        RowIndex = 0
        FieldName = 'Nome'
      end
    end
  end
  object dxDBGrid9: TdxDBGrid
    Left = 0
    Top = 204
    Width = 515
    Height = 231
    Bands = <
      item
      end>
    DefaultLayout = True
    HeaderPanelRowCount = 1
    KeyField = 'ID'
    ShowGroupPanel = True
    SummaryGroups = <>
    SummarySeparator = ', '
    Align = alClient
    TabOrder = 2
    DataSource = DsQValutaz
    Filter.Active = True
    Filter.Criteria = {00000000}
    OptionsBehavior = [edgoAutoSort, edgoDragScroll, edgoEditing, edgoEnterShowEditor, edgoImmediateEditor, edgoStoreToRegistry, edgoTabThrough, edgoVertThrough]
    OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
    OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoIndicator, edgoUseBitmap]
    RegistryPath = '\Software\H1\DbGridSelPersTargetList'
    object dxDBGrid9ID: TdxDBGridMaskColumn
      Visible = False
      Width = 134
      BandIndex = 0
      RowIndex = 0
      FieldName = 'ID'
    end
    object dxDBGrid9Voce: TdxDBGridMaskColumn
      Width = 161
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Voce'
    end
    object dxDBGrid9Legenda: TdxDBGridMaskColumn
      Width = 218
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Legenda'
    end
    object dxDBGrid9Valore: TdxDBGridSpinColumn
      MaxLength = 10
      Width = 53
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Valore'
    end
    object dxDBGrid9Note: TdxDBGridBlobColumn
      Width = 31
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Note'
      BlobKind = bkMemo
    end
  end
  object QCandRic: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    BeforeOpen = QCandRicBeforeOpen
    AfterScroll = QCandRicAfterScroll
    Parameters = <
      item
        Name = 'x'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 2379
      end>
    SQL.Strings = (
      'select CR.ID, A.Cognome, A.Nome'
      'from EBC_CandidatiRicerche CR'
      'join Anagrafica A on CR.IDAnagrafica = A.ID'
      'where CR.IDRicerca=:x'
      'and (Escluso = 0 or Escluso is null)'
      'order by A.Cognome')
    Left = 32
    Top = 89
    object QCandRicID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QCandRicCognome: TStringField
      FieldName = 'Cognome'
      Size = 30
    end
    object QCandRicNome: TStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsQCandRic: TDataSource
    DataSet = QCandRic
    Left = 32
    Top = 121
  end
  object QValutaz: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    BeforeOpen = QValutazBeforeOpen
    Parameters = <
      item
        Name = 'x'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 22200
      end>
    SQL.Strings = (
      'select VAL.ID, VOCI.Voce, VOCI.Legenda, VAL.Valore, VAL.Note'
      'from EBC_CandRic_Valutaz VAL'
      'join EBC_Ricerche_valutaz_voci VOCI on VAL.IDVoce = VOCI.ID'
      'where IDCandRic=:x')
    Left = 80
    Top = 296
    object QValutazID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QValutazVoce: TStringField
      FieldName = 'Voce'
      Size = 100
    end
    object QValutazLegenda: TStringField
      FieldName = 'Legenda'
      Size = 512
    end
    object QValutazValore: TSmallintField
      FieldName = 'Valore'
    end
    object QValutazNote: TMemoField
      FieldName = 'Note'
      BlobType = ftMemo
    end
  end
  object DsQValutaz: TDataSource
    DataSet = QValutaz
    Left = 80
    Top = 328
  end
end
