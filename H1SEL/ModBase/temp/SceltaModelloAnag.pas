unit SceltaModelloAnag;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     Grids,DBGrids,Db,DBTables,StdCtrls,Buttons,ExtCtrls,ADODB,
     U_ADOLinkCl;

type
     TSceltaModelloAnagForm=class(TForm)
          BitBtn1: TBitBtn;
          BitBtn2: TBitBtn;
          DsModelliWord: TDataSource;
          DBGrid1: TDBGrid;
          CBSalva: TCheckBox;
          BitBtn3: TBitBtn;
          RGOpzione: TRadioGroup;
          QModelliWord: TADOLinkedQuery;
          procedure FormCreate(Sender: TObject);
          procedure BitBtn3Click(Sender: TObject);
          procedure BitBtn1Click(Sender: TObject);
          procedure FormShow(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     SceltaModelloAnagForm: TSceltaModelloAnagForm;

implementation

uses ModuloDati,GestModelliWord;

{$R *.DFM}

procedure TSceltaModelloAnagForm.FormCreate(Sender: TObject);
begin
     Height:=330;
end;

procedure TSceltaModelloAnagForm.BitBtn3Click(Sender: TObject);
begin
     GestModelliWordForm:=TGestModelliWordForm.create(self);
     GestModelliWordForm.ShowModal;
     QModelliWord.Close;
     QModelliWord.Open;
     GestModelliWordForm.Free;
end;

procedure TSceltaModelloAnagForm.BitBtn1Click(Sender: TObject);
begin
     if QModelliWord.IsEmpty then begin
          MessageDlg('Nessun modello disponibile: IMPOSSIBILE PROSEGUIRE'+chr(13)+
               'Premere "Annulla" o creare un modello usando il pulsante "Modelli"',mtError, [mbOK],0);
          Abort;
     end;
end;

procedure TSceltaModelloAnagForm.FormShow(Sender: TObject);
begin
     Caption:='[M/072] - '+Caption;
end;

end.
