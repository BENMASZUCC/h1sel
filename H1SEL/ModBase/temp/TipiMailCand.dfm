object TipiMailCandForm: TTipiMailCandForm
  Left = 273
  Top = 149
  BorderStyle = bsDialog
  Caption = 'Gestione tipi di mail'
  ClientHeight = 390
  ClientWidth = 660
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 5
    Top = 6
    Width = 218
    Height = 378
    Anchors = [akLeft, akTop, akRight, akBottom]
    DataSource = DsQTipiCand
    ReadOnly = True
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'Descrizione'
        Visible = True
      end>
  end
  object Panel1: TPanel
    Left = 229
    Top = 6
    Width = 345
    Height = 378
    Anchors = [akTop, akRight, akBottom]
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 1
    object Label1: TLabel
      Left = 8
      Top = 50
      Width = 55
      Height = 13
      Caption = 'Descrizione'
      FocusControl = DBEdit1
    end
    object Label2: TLabel
      Left = 8
      Top = 130
      Width = 57
      Height = 13
      Caption = 'Intestazione'
      FocusControl = DBMemo1
    end
    object Label3: TLabel
      Left = 8
      Top = 246
      Width = 28
      Height = 13
      Caption = 'Corpo'
      FocusControl = DBMemo2
    end
    object Label4: TLabel
      Left = 8
      Top = 90
      Width = 38
      Height = 13
      Caption = 'Oggetto'
      FocusControl = DBEdit2
    end
    object DBEdit1: TDBEdit
      Left = 8
      Top = 66
      Width = 329
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      DataField = 'Descrizione'
      DataSource = DsQTipiCand
      TabOrder = 0
    end
    object DBMemo1: TDBMemo
      Left = 8
      Top = 146
      Width = 329
      Height = 57
      Anchors = [akLeft, akTop, akRight]
      DataField = 'Intestazione'
      DataSource = DsQTipiCand
      TabOrder = 1
    end
    object DBMemo2: TDBMemo
      Left = 8
      Top = 262
      Width = 329
      Height = 90
      Anchors = [akLeft, akTop, akRight, akBottom]
      DataField = 'Corpo'
      DataSource = DsQTipiCand
      TabOrder = 2
    end
    object DBEdit2: TDBEdit
      Left = 8
      Top = 106
      Width = 304
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      DataField = 'Oggetto'
      DataSource = DsQTipiCand
      TabOrder = 3
    end
    object DBCheckBox1: TDBCheckBox
      Left = 8
      Top = 210
      Width = 129
      Height = 17
      Caption = 'riporta dati ricerca'
      DataField = 'InfoRicerca'
      DataSource = DsQTipiCand
      TabOrder = 4
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object Panel2: TPanel
      Left = 7
      Top = 8
      Width = 330
      Height = 38
      Anchors = [akLeft, akTop, akRight]
      BevelOuter = bvLowered
      TabOrder = 5
      object BNew: TToolbarButton97
        Left = 1
        Top = 1
        Width = 67
        Height = 36
        Caption = 'Nuovo tipo'
        WordWrap = True
        OnClick = BNewClick
      end
      object Bdel: TToolbarButton97
        Left = 68
        Top = 1
        Width = 67
        Height = 36
        Caption = 'Elimina'
        WordWrap = True
        OnClick = BdelClick
      end
      object BOK: TToolbarButton97
        Left = 135
        Top = 1
        Width = 67
        Height = 36
        Caption = 'Conferma modifiche'
        Enabled = False
        WordWrap = True
        OnClick = BOKClick
      end
      object BCan: TToolbarButton97
        Left = 202
        Top = 1
        Width = 67
        Height = 36
        Caption = 'Annulla modifiche'
        Enabled = False
        WordWrap = True
        OnClick = BCanClick
      end
    end
    object DBCheckBox2: TDBCheckBox
      Left = 8
      Top = 228
      Width = 189
      Height = 17
      Caption = 'Aggiungi specifiche (esterne)'
      DataField = 'AddSpecifiche'
      DataSource = DsQTipiCand
      TabOrder = 6
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object DBCheckBox3: TDBCheckBox
      Left = 8
      Top = 355
      Width = 189
      Height = 17
      Caption = 'Aggiungi link a questionario'
      DataField = 'AddQuest'
      DataSource = DsQTipiCand
      TabOrder = 7
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object Panel3: TPanel
      Left = 31
      Top = 359
      Width = 330
      Height = 38
      Anchors = [akLeft, akTop, akRight]
      BevelOuter = bvLowered
      TabOrder = 8
      Visible = False
      object BCliNew: TToolbarButton97
        Left = 1
        Top = 1
        Width = 67
        Height = 36
        Caption = 'Nuovo tipo'
        WordWrap = True
        OnClick = BCliNewClick
      end
      object BCliDel: TToolbarButton97
        Left = 68
        Top = 1
        Width = 67
        Height = 36
        Caption = 'Elimina'
        WordWrap = True
        OnClick = BCliDelClick
      end
      object BCliOK: TToolbarButton97
        Left = 135
        Top = 1
        Width = 67
        Height = 36
        Caption = 'Conferma modifiche'
        Enabled = False
        WordWrap = True
        OnClick = BCliOKClick
      end
      object BCliCan: TToolbarButton97
        Left = 202
        Top = 1
        Width = 67
        Height = 36
        Caption = 'Annulla modifiche'
        Enabled = False
        WordWrap = True
        OnClick = BCliCanClick
      end
    end
  end
  object BitBtn1: TBitBtn
    Left = 579
    Top = 6
    Width = 80
    Height = 31
    Anchors = [akTop, akRight]
    Caption = 'Esci'
    TabOrder = 2
    Kind = bkOK
  end
  object DsQTipiCand: TDataSource
    DataSet = QTipiCand
    OnStateChange = DsQTipiCandStateChange
    Left = 32
    Top = 64
  end
  object QTipiCand: TADOLinkedQuery
    Active = True
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from TipiMailCand with (updlock)')
    UseFilter = False
    Left = 32
    Top = 24
  end
  object QTipiCli: TADOLinkedQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from TipiMailContattiCli with (updlock)')
    UseFilter = False
    Left = 88
    Top = 24
  end
  object dsQTipiCli: TDataSource
    DataSet = QTipiCli
    OnStateChange = dsQTipiCliStateChange
    Left = 88
    Top = 64
  end
end
