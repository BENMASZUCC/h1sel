object SelContattiForm: TSelContattiForm
  Left = 265
  Top = 295
  Width = 486
  Height = 192
  Caption = 'Seleziona riferimento interno'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  Scaled = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object SpeedButton1: TSpeedButton
    Left = 384
    Top = 96
    Width = 92
    Height = 33
    Anchors = [akTop, akRight]
    Caption = 'Nuovo '
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      1800000000000003000000000000000000000000000000000000FF00FFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFF00FFFFFFFF85B7850F6F0F1674161A761A1A761A18781817
      7917137D130D7F0D0A7E0A077C07027B020070007FB07FFFFFFFFFFFFF118311
      1F8C1F2A912A2F932F2E942E2C962C299A29239E231CA31C15A4150DA40D059F
      05019101057E05FFFFFFFFFFFF198D192C962C379C373D9F3D3C9F3C39A139A3
      D6A3FFFFFF24AF241CB11C13B2130AAD0A049F04057E05FFFFFFFFFFFF229122
      389C3843A24348A44845A54542A642FFFFFFFFFFFFFFFFFF21B52118B6180EB1
      0E08A308057E05FFFFFFFFFFFF2C962C42A0424CA54C4FA74F4CA74C46A74640
      AA40FFFFFFFFFFFFFFFFFF1AB31A14AF140FA30F0B800BFFFFFFFFFFFF359A35
      4BA54B52A85253A9534EA84E49A74941A84138AA38FFFFFFFFFFFFFFFFFF19AC
      1918A218128212FFFFFFFFFFFF3F9F3F53A953FFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF1F9E1F188118FFFFFFFFFFFF45A245
      5AAC5AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FF259A251D7F1DFFFFFFFFFFFF4FA74F63B16361AF6159AB5951A65148A2483F
      9F3F369C36FFFFFFFFFFFFFFFFFF2699262A972A217E21FFFFFFFFFFFF53A953
      6CB66C68B4685EAD5E54A8544CA34C429F42FFFFFFFFFFFFFFFFFF2997292B98
      2B2D952D237E23FFFFFFFFFFFF5EAF5E7ABD7A70B87063B0635AAB5A52A652FF
      FFFFFFFFFFFFFFFF3399333099303098302F942F237D23FFFFFFFFFFFF6BB56B
      8DC68D80C0806FB76F67B26760AE60B4D9B4FFFFFF4CA54C49A44941A1413A9D
      3A3095301E7A1EFFFFFFFFFFFF77BB779DCF9D8CC68C79BC7970B87069B46965
      B26562B0625DAE5D56AB564EA74E41A1412F942F197719FFFFFFFFFFFFB1D8B1
      76BB7667B3675BAD5B54A9544FA74F4AA44A4BA54B46A3463FA03F3B9E3B3198
      31238C238ABB8AFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FF}
    OnClick = SpeedButton1Click
  end
  object BitBtn1: TBitBtn
    Left = 384
    Top = 3
    Width = 92
    Height = 33
    Anchors = [akTop, akRight]
    TabOrder = 0
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 384
    Top = 38
    Width = 92
    Height = 33
    Anchors = [akTop, akRight]
    Caption = 'Annulla'
    TabOrder = 1
    Kind = bkCancel
  end
  object dxDBGrid23: TdxDBGrid
    Left = 2
    Top = 2
    Width = 377
    Height = 160
    Bands = <
      item
      end>
    DefaultLayout = True
    HeaderPanelRowCount = 1
    KeyField = 'ID'
    SummaryGroups = <>
    SummarySeparator = ', '
    TabOrder = 2
    DataSource = DsQContatti
    Filter.Active = True
    Filter.Criteria = {00000000}
    OptionsBehavior = [edgoAutoSort, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoTabThrough, edgoVertThrough]
    OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
    OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoIndicator, edgoUseBitmap]
    Anchors = [akLeft, akTop, akRight, akBottom]
    object dxDBGrid23ID: TdxDBGridMaskColumn
      Visible = False
      BandIndex = 0
      RowIndex = 0
      FieldName = 'ID'
    end
    object dxDBGrid23Contatto: TdxDBGridMaskColumn
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Contatto'
    end
    object dxDBGrid23Telefono: TdxDBGridMaskColumn
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Telefono'
    end
  end
  object DsQContatti: TDataSource
    DataSet = QContatti
    Left = 48
    Top = 64
  end
  object QContatti: TADOLinkedQuery
    Connection = Data.DB
    Parameters = <
      item
        Name = 'xIDCliente:'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select ID,Contatto,Telefono'
      'from EBC_ContattiClienti'
      'where IDCliente=:xIDCliente:'
      '')
    OriginalSQL.Strings = (
      'select ID,Contatto,Telefono'
      'from EBC_ContattiClienti'
      'where IDCliente=:xIDCliente')
    UseFilter = False
    Left = 48
    Top = 32
    object QContattiID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QContattiContatto: TStringField
      FieldName = 'Contatto'
      Size = 50
    end
    object QContattiTelefono: TStringField
      FieldName = 'Telefono'
      Size = 30
    end
  end
  object QContatti1: TADOQuery
    Connection = Data.DB
    Parameters = <>
    Left = 128
    Top = 88
  end
  object Q: TADOQuery
    Connection = Data.DB
    Parameters = <>
    Left = 216
    Top = 88
  end
end
