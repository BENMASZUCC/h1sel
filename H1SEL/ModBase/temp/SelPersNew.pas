unit SelPersNew;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Mask, DBCtrls, Grids, DBGrids, Db, DBTables, Buttons, TB97,
     ExtCtrls, ComCtrls, ComObj, RXDBCtrl, dxDBGrid, dxDBTLCl, dxGrClms, dxTL,
     dxDBCtrl, dxCntner, dxGridMenus, ImgList, Menus, dxPSCore, dxPSdxTLLnk,
     dxPSdxDBGrLnk, Spin, dxPSdxDBCtrlLnk, ADODB, U_ADOLinkCl, DBCGrids,
     dxEditor, dxExEdtr, dxEdLib, dxDBELib;

type
     TSaveMethod = procedure(const FileName: string; ASaveAll: Boolean) of object;
     TSelPersNewForm = class(TForm)
          DsTabQuery: TDataSource;
          DsTabOp: TDataSource;
          DsLinee: TDataSource;
          DsTabLookup: TDataSource;
          DsQRes1: TDataSource;
          DsQTabLista: TDataSource;
          PanRic: TPanel;
          BitBtn2: TBitBtn;
          Panel1: TPanel;
          BitBtn4: TBitBtn;
          BitBtn8: TBitBtn;
          BAggiungiRic_old: TBitBtn;
          BCurriculum: TBitBtn;
          BitBtn9: TBitBtn;
          BitBtn10: TBitBtn;
          Panel3: TPanel;
          Panel10: TPanel;
          LTot: TLabel;
          GroupBox2: TGroupBox;
          CBColloquiati: TCheckBox;
          CBPropri: TCheckBox;
          dxDBGrid1: TdxDBGrid;
          dxDBGrid1Cognome: TdxDBGridMaskColumn;
          dxDBGrid1Nome: TdxDBGridMaskColumn;
          dxDBGrid1DataNascita: TdxDBGridDateColumn;
          dxDBGrid1TipoStato: TdxDBGridLookupColumn;
          dxDBGrid1CVNumero: TdxDBGridMaskColumn;
          dxDBGrid1Eta: TdxDBGridColumn;
          dxDBGrid1DomicilioComune: TdxDBGridMaskColumn;
          dxDBGrid1OldCVNumero: TdxDBGridMaskColumn;
          dxDBGrid1Azienda: TdxDBGridLookupColumn;
          dxDBGrid1Ruolo: TdxDBGridLookupColumn;
          dxDBGrid1TelUfficio: TdxDBGridMaskColumn;
          dxDBGrid1Cellulare: TdxDBGridMaskColumn;
          dxDBGrid1TelUffCell: TdxDBGridColumn;
          dxDBGrid1ggUltimaDataIns: TdxDBGridMaskColumn;
          dxDBGrid1ggDataUltimoContatto: TdxDBGridMaskColumn;
          dxDBGrid1DataProssimoColloquio: TdxDBGridDateColumn;
          dxDBGrid1ggDataUltimoColloquio: TdxDBGridMaskColumn;
          dxDBGrid1ggDataInsCV: TdxDBGridMaskColumn;
          dxDBGrid1IDStato: TdxDBGridColumn;
          PMExport: TPopupMenu;
          Stampaelenco1: TMenuItem;
          EsportainExcel1: TMenuItem;
          EsportainHTML1: TMenuItem;
          ImageList1: TImageList;
          SaveDialog: TSaveDialog;
          BLegenda: TToolbarButton97;
          ToolbarButton972: TToolbarButton97;
          BEmail: TToolbarButton97;
          Stampagriglia1: TMenuItem;
          dxPrinter1: TdxComponentPrinter;
          GroupBox3: TGroupBox;
          CBAnzCV: TCheckBox;
          SEAnzCV: TSpinEdit;
          dxDBGrid1Column21: TdxDBGridDateColumn;
          dxDBGrid1DataRealeCV: TdxDBGridDateColumn;
          Label5: TLabel;
          dxDBGrid1Column23: TdxDBGridColumn;
          cancellasoggetti1: TMenuItem;
          dxDBGrid1Column24: TdxDBGridColumn;
          dxDBGrid1NoCv: TdxDBGridCheckColumn;
          BFiltroStati: TToolbarButton97;
          PMStati: TPopupMenu;
          PMStati1: TMenuItem;
          PMStati2: TMenuItem;
          PMStati3: TMenuItem;
          PMStati4: TMenuItem;
          PMStati5: TMenuItem;
          cancellatuttiisoggettiselezionati1: TMenuItem;
          mettiinstatoeliminato1: TMenuItem;
          TLinee: TADOLinkedQuery;
          QTabelleLista: TADOLinkedQuery;
          TTabLookup: TADOLinkedTable;
          TTabQuery: TADOLinkedQuery;
          QTabelle: TADOLinkedQuery;
          Query1: TADOLinkedQuery;
          QRicLineeQuery: TADOLinkedQuery;
          TTabOp: TADOLinkedTable;
          TRicLineeQuery: TADOLinkedTable;
          Q: TADOLinkedQuery;
          QRes1: TADOLinkedQuery;
          TStatiLK: TADOLinkedTable;
          TTipiStatiLK: TADOLinkedTable;
          QLastEspLav: TADOLinkedQuery;
          QEspLavLK: TADOLinkedQuery;
          QRes1ID: TAutoIncField;
          QRes1Cognome: TStringField;
          QRes1Nome: TStringField;
          QRes1CVNumero: TIntegerField;
          QRes1OldCVNumero: TStringField;
          QRes1TelUfficio: TStringField;
          QRes1Cellulare: TStringField;
          QRes1DataNascita: TDateTimeField;
          QRes1IDStato: TIntegerField;
          QRes1IDTipoStato: TIntegerField;
          QRes1DomicilioComune: TStringField;
          QRes1ggUltimaDataIns: TIntegerField;
          QRes1ggDataUltimoContatto: TIntegerField;
          QRes1DataProssimoColloquio: TDateTimeField;
          QRes1ggDataUltimoColloquio: TIntegerField;
          QRes1ggDataInsCV: TIntegerField;
          TStatiLKID: TAutoIncField;
          TStatiLKStato: TStringField;
          TStatiLKIDTipoStato: TIntegerField;
          TStatiLKPRman: TBooleanField;
          TStatiLKIDAzienda: TIntegerField;
          TTipiStatiLKID: TAutoIncField;
          TTipiStatiLKTipoStato: TStringField;
          QRes1Stato: TStringField;
          QRes1TipoStato: TStringField;
          QRes1Eta: TStringField;
          QEspLavLKID: TAutoIncField;
          QEspLavLKAzienda: TStringField;
          QEspLavLKRuolo: TStringField;
          QLastEspLavIDAnagrafica: TIntegerField;
          QLastEspLavMaxID: TIntegerField;
          QLastEspLavAzienda: TStringField;
          QLastEspLavRuolo: TStringField;
          QRes1Azienda: TStringField;
          QRes1Ruolo: TStringField;
          QRes1TelUffCell: TStringField;
          TLineeID: TAutoIncField;
          TLineeIDAnagrafica: TIntegerField;
          TLineeDescrizione: TStringField;
          TLineeOpSucc: TStringField;
          TLineeOpNext: TStringField;
          TLineeLivLinguaParlato: TIntegerField;
          TLineeLivLinguaScritto: TIntegerField;
          OpenDialog1: TOpenDialog;
          BRiepCand: TBitBtn;
          QRes1cvinseritoindata: TDateTimeField;
          QRes1cvdatareale: TDateTimeField;
          QRes1cvdataagg: TDateTimeField;
          TTabLookupID: TAutoIncField;
          TTabLookupAttivita: TStringField;
          TLineeDescTabella: TStringField;
          TLineeDescCampo: TStringField;
          TLineeDescOperatore: TStringField;
          TLineeValore: TStringField;
          TLineeQueryLookup: TStringField;
          QRes1DomicilioProvincia: TStringField;
          QRes1Provincia: TStringField;
          QRes1Comune: TStringField;
          dxDBGrid1DomicilioProvincia: TdxDBGridMaskColumn;
          dxDBGrid1Provincia: TdxDBGridMaskColumn;
          dxDBGrid1Comune: TdxDBGridMaskColumn;
          PCTipoRic: TPageControl;
          TSRicSemplice: TTabSheet;
          TSRicAv: TTabSheet;
          PanCriteri: TPanel;
          Panel2: TPanel;
          PanCriteriButtons: TPanel;
          BCriterioNew: TToolbarButton97;
          BCriterioMod: TToolbarButton97;
          BCriterioDel: TToolbarButton97;
          BLineeOK: TToolbarButton97;
          BLineeCan: TToolbarButton97;
          BCriterioDelTutti: TToolbarButton97;
          DBGrid3: TdxDBGrid;
          DBGrid3DescTabella: TdxDBGridButtonColumn;
          DBGrid3DescCampo: TdxDBGridButtonColumn;
          DBGrid3DescOperatore: TdxDBGridButtonColumn;
          DBGrid3Valore: TdxDBGridButtonColumn;
          DBGrid3Descrizione: TdxDBGridMaskColumn;
          DBGrid3LivLinguaParlato: TdxDBGridButtonColumn;
          DBGrid3LivLinguaScritto: TdxDBGridButtonColumn;
          DBGrid3OpNext: TdxDBGridPickColumn;
          DBGrid3ID: TdxDBGridColumn;
          DBGrid3QueryLookup: TdxDBGridColumn;
          Panel4: TPanel;
          PanInfoRet: TPanel;
          Label6: TLabel;
          CBIRnote: TCheckBox;
          EIRNote: TEdit;
          CBIRNoteInflectional: TCheckBox;
          CBIRFileSystem: TCheckBox;
          CBIRaltreRegole: TCheckBox;
          TLineeDescCompleta: TStringField;
          DBGrid2: TdxDBGrid;
          DBGrid2ID: TdxDBGridMaskColumn;
          DBGrid2DescCompleta: TdxDBGridMaskColumn;
          DBGrid2Valore: TdxDBGridButtonColumn;
          TLineeIDTabQueryOp: TIntegerField;
          DBGrid2QueryLookup: TdxDBGridColumn;
          dxPrinter1Link1: TdxDBGridReportLink;
          BInfoColloquio: TBitBtn;
          Q2: TADOLinkedQuery;
          TLineeTabella: TStringField;
          TLineeStringa: TStringField;
          QRes1Retribuzione: TStringField;
          dxDBGrid1Retribuzione: TdxDBGridColumn;
          QRes1NoCV: TBooleanField;
          QRes1EMail: TStringField;
          dxDBGrid1EMail: TdxDBGridColumn;
          dxDBGrid1IDAnagrafica: TdxDBGridColumn;
          pmInsRic: TPopupMenu;
          Aggiungiinricerca1: TMenuItem;
          Aggiungiadunaricerca1: TMenuItem;
          Panel5: TPanel;
          BAggiungiRic: TToolbarButton97;
          cbanzianita: TComboBox;
          dxDBGrid1File: TdxDBGridCheckColumn;
          QRes1FileCheck: TBooleanField;
          QLastEspLavCodPersUnica: TStringField;
          QEspLavLKtitolomansione: TStringField;
          QRes1CodPers: TStringField;
          dxDBGrid1Column32: TdxDBGridColumn;
          QEspLavLKdescrizionemansione: TMemoField;
          QLastEspLavDEscrizioneMansione: TStringField;
          QRes1DEscrizioneMansione: TStringField;
          dxDBGrid1Column33: TdxDBGridColumn;
          QRes1RecapitiTelefonici: TStringField;
          Query2: TMenuItem;
          ApriQuery1: TMenuItem;
          SalvaQuery1: TMenuItem;
          N1: TMenuItem;
          Salvaquerysufile1: TMenuItem;
          qUserLineeQuerySalvate: TADOLinkedQuery;
          EliminaQuery1: TMenuItem;
          QEspLavLKJobTitle: TStringField;
          QLastEspLavJobTitle: TStringField;
          QRes1JobTitle: TStringField;
          dxDBGrid1Column34: TdxDBGridColumn;
          TSMotRic: TTabSheet;
          DBCtrlGrid1: TDBCtrlGrid;
          QMotoriRicerca: TADOQuery;
          DSMotoriRicerca: TDataSource;
          QsaveMotoriRicercaPers: TADOQuery;
          dxDBHyperLinkEdit1: TdxDBHyperLinkEdit;
          dxDBHyperLinkEdit2: TdxDBHyperLinkEdit;
          DBMemo1: TDBMemo;
          DBMemo2: TDBMemo;
          Label1: TLabel;
          Label2: TLabel;
          Label3: TLabel;
          Label4: TLabel;
          Button1: TButton;
          QMotoriRicercaID: TAutoIncField;
          QMotoriRicercaLinkMotore: TStringField;
          QMotoriRicercaNoteazienda: TMemoField;
          QMotoriRicercaLinkpers: TStringField;
          QMotoriRicercaNotepers: TMemoField;
          qDatiIns: TADOQuery;
          qDatiMod: TADOQuery;
          qDatiModNominativo: TStringField;
          qDatiModDataOra: TDateTimeField;
          qDatiModKeyValue: TIntegerField;
          qDatiInsNominativo: TStringField;
          qDatiInsDataOra: TDateTimeField;
          qDatiInsKeyValue: TIntegerField;
          QRes1DataAgg: TDateField;
          dxDBGrid1Column35: TdxDBGridDateColumn;
          dxDBGrid1Column36: TdxDBGridDateColumn;
          QRes1DataIns: TDateField;
          procedure ComboBox1Change(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure DBGrid4KeyPress(Sender: TObject; var Key: Char);
          procedure BitBtn8Click(Sender: TObject);
          //procedure BAggiungiRicClick(Sender: TObject);
          procedure TLinee_OLDBeforePost(DataSet: TDataSet);
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
          procedure BCurriculumClick(Sender: TObject);
          procedure BitBtn9Click(Sender: TObject);
          procedure BitBtn10Click(Sender: TObject);
          procedure QRes1_OLDCalcFields(DataSet: TDataSet);
          procedure CBColloquiatiClick(Sender: TObject);
          procedure QRes1_OLDBeforeOpen(DataSet: TDataSet);
          procedure QRes1_OLDAfterClose(DataSet: TDataSet);
          procedure dxDBGrid1Click(Sender: TObject);
          procedure dxDBGrid1CustomDrawCell(Sender: TObject; ACanvas: TCanvas;
               ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
               ASelected, AFocused, ANewItemRow: Boolean; var AText: string;
               var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
               var ADone: Boolean);
          procedure dxDBGrid1MouseUp(Sender: TObject; Button: TMouseButton;
               Shift: TShiftState; X, Y: Integer);
          procedure Stampaelenco1Click(Sender: TObject);
          procedure EsportainHTML1Click(Sender: TObject);
          procedure EsportainExcel1Click(Sender: TObject);
          procedure ToolbarButton971Click(Sender: TObject);
          procedure ToolbarButton973Click(Sender: TObject);
          procedure Stampagriglia1Click(Sender: TObject);
          procedure TTabQuery_OLDAfterOpen(DataSet: TDataSet);
          procedure cancellasoggetti1Click(Sender: TObject);
          procedure PMStati1Click(Sender: TObject);
          procedure PMStati2Click(Sender: TObject);
          procedure PMStati3Click(Sender: TObject);
          procedure PMStati4Click(Sender: TObject);
          procedure PMStati5Click(Sender: TObject);
          procedure cancellatuttiisoggettiselezionati1Click(Sender: TObject);
          procedure mettiinstatoeliminato1Click(Sender: TObject);
          procedure DBGrid3LivLinguaParlatoButtonClick(Sender: TObject;
               AbsoluteIndex: Integer);
          procedure DBGrid3LivLinguaScrittoButtonClick(Sender: TObject;
               AbsoluteIndex: Integer);
          procedure Salvaquerysufile1Click(Sender: TObject);
          procedure BRiepCandClick(Sender: TObject);
          procedure DBGrid3DescCampoButtonClick(Sender: TObject;
               AbsoluteIndex: Integer);
          procedure DBGrid3DescTabellaButtonClick(Sender: TObject;
               AbsoluteIndex: Integer);
          procedure DBGrid3DescOperatoreButtonClick(Sender: TObject;
               AbsoluteIndex: Integer);
          procedure CBIRFileSystemClick(Sender: TObject);
          procedure DsLineeStateChange(Sender: TObject);
          procedure BLineeOKClick(Sender: TObject);
          procedure BLineeCanClick(Sender: TObject);
          procedure DBGrid3CustomDrawCell(Sender: TObject; ACanvas: TCanvas;
               ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
               ASelected, AFocused, ANewItemRow: Boolean; var AText: string;
               var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
               var ADone: Boolean);
          procedure DBGrid3Editing(Sender: TObject; Node: TdxTreeListNode;
               var Allow: Boolean);
          procedure DBGrid3ValoreEditButtonClick(Sender: TObject);
          procedure BCriterioNewClick(Sender: TObject);
          procedure BCriterioModClick(Sender: TObject);
          procedure BCriterioDelClick(Sender: TObject);
          procedure BCriterioDelTuttiClick(Sender: TObject);
          procedure DBGrid2CustomDrawCell(Sender: TObject; ACanvas: TCanvas;
               ARect: TRect; ANode: TdxTreeListNode; AColumn: TdxTreeListColumn;
               ASelected, AFocused, ANewItemRow: Boolean; var AText: string;
               var AColor: TColor; AFont: TFont; var AAlignment: TAlignment;
               var ADone: Boolean);
          procedure DBGrid2ValoreEditButtonClick(Sender: TObject);
          procedure BInfoColloquioClick(Sender: TObject);
          procedure PCTipoRicChange(Sender: TObject);
          procedure Aggiungiinricerca1Click(Sender: TObject);
          procedure Aggiungiadunaricerca1Click(Sender: TObject);
          procedure BitBtn2Click(Sender: TObject);
          procedure ApriQuery1DrawItem(Sender: TObject; ACanvas: TCanvas;
               ARect: TRect; Selected: Boolean);
          procedure PMExportPopup(Sender: TObject);
          procedure SalvaQuery1Click(Sender: TObject);
          procedure Button1Click(Sender: TObject);
     private
          { Private declarations }
          xStringa, xIndexName, xOrdine: string;
          xFormID: integer;
          xggDiffDi, xggDiffUc, xggDiffCol: integer;
          xCheckRicCandCV: boolean;
          procedure Save(ADefaultExt, AFilter, AFileName: string; AMethod: TSaveMethod);
          function LogEsportazioniCand: boolean;
          procedure SalvaDaWizard(x: integer = 0);
          procedure RicreaStringaSQL;
          procedure PredisponiWizard;
          procedure CaricaRicSemplice;
          procedure EseguiQueryRic;

     public
          xDaEscludere: array[1..500] of integer;
          xChiamante, xTipoRic, xTipoQuery: integer;
          xIsFulltextInstalled, xAnagAltreInfoOK, xFileSystemOK, xRegoleCollImpostate: boolean;
          xIRFileSystemScope: string;
          xEIrNote: string;
          procedure MyPopup(Sender: TObject);
          procedure MyPopupDel(Sender: TObject);
     end;

var
     SelPersNewForm: TSelPersNewForm;

implementation

uses ModuloDati, RepElencoSel, View, SelPers, MDRicerche, InsRuolo,
     SelCompetenza, Main, Curriculum, uUtilsVarie, Comuni, SchedaCand,
     FaxPresentaz, LegendaRic, SchedaSintetica, CheckPass, SelDiploma,
     riepilogocand, SelCriterioWizard, InfoColloquio, uASACand, ElencoRicPend,
     ModuloDati2, motoriricerca;

{$R *.DFM}


//[TONI20021003]DEBUGOK

procedure TSelPersNewForm.ComboBox1Change(Sender: TObject);
begin
end;

//[TONI20021003]DEBUGOK

procedure TSelPersNewForm.FormShow(Sender: TObject);
var i: integer;
begin
     xOrdine := 'order by Anagrafica.Cognome,Anagrafica.Nome';
     TLinee.ParamByName['xIDAnag'] := MainForm.xIDUtenteAttuale;
     TLinee.Open;
     // se viene da Ricerca -> caricamento criteri salvati
     if xChiamante = 1 then begin
          Query1.SQL.Clear;
          Query1.SQL.Add('delete from UserLineeQuery where IDAnagrafica=' + IntToStr(MainForm.xIDUtenteAttuale));
          Query1.ExecSQL;
          TLinee.Close;
          TLinee.Open;
          QRicLineeQuery.Close;
          QRicLIneeQuery.ReloadSQL;
          QRicLineeQuery.ParamByName['xIDRic'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
          QRicLineeQuery.Open;
          while not QRicLineeQuery.EOF do begin
               TLinee.InsertRecord([QRicLineeQuery.FieldByName('Descrizione').AsString,
                    QRicLineeQuery.FieldByName('Stringa').AsString,
                         QRicLineeQuery.FieldByName('Tabella').AsString,
                         QRicLineeQuery.FieldByName('OpSucc').AsString,
                         QRicLineeQuery.FieldByName('Next').AsString,
                         QRicLineeQuery.FieldByName('LivLinguaParlato').Value,
                         QRicLineeQuery.FieldByName('LivLinguaScritto').Value,
                         QRicLineeQuery.FieldByName('DescTabella').AsString,
                         QRicLineeQuery.FieldByName('DescOperatore').AsString,
                         QRicLineeQuery.FieldByName('Valore').AsString,
                         QRicLineeQuery.FieldByName('DescCampo').AsString,
                         {QRicLineeQuery.FieldByName('DescCampo').AsString,
QRicLineeQuery.FieldByName('DescOperatore').AsString,
QRicLineeQuery.FieldByName('Valore').AsString,}
                    QRicLineeQuery.FieldByName('QueryLookup').AsString,
                         MainForm.xIDUtenteAttuale]);
               { TLinee.Edit;
                TLineeDescrizione.Value := QRicLineeQuery.FieldByName('Descrizione').AsString;
                TLineeStringa.Value := QRicLineeQRicLineeQuery.FieldByName('Stringa').AsString;
                TLineeTabella }

               QRicLineeQuery.Next;
          end;
          TLinee.Close;
          TLinee.Open;
     end;
     // parametri sulle date da GLOBAL
     Q.Close;
     Q.SQl.text := 'select ggDiffDi,ggDiffUc,ggDiffCol,CheckRicCandCV from global ';
     Q.Open;
     xggDiffDi := Q.FieldByname('ggDiffDi').asInteger;
     xggDiffUc := Q.FieldByname('ggDiffUc').asInteger;
     xggDiffCol := Q.FieldByname('ggDiffCol').asInteger;
     xCheckRicCandCV := Q.FieldByname('CheckRicCandCV').asBoolean;
     Q.Close;
     // personalizzazione Ergon:
     if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 5)) = 'ERGON' then begin
          BitBtn10.caption := 'scheda cand.';
          BitBtn10.Font.Style := [];
          BCurriculum.Visible := False;
     end;
     if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 6)) = 'BOYDEN' then
          dxDBGrid1DataRealeCV.DisableCustomizing := True;

     if (Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 7)) = 'FERRARI') or (Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 8)) = 'MASERATI') then begin
          CBPropri.Checked := True;
          PMStati5.Checked := True;
     end;

     if (Uppercase(copy(Data.Global.FieldByName('NomeAzienda').Value, 1, 6)) = 'ADVANT') or (pos('MCS', UpperCase(Data.Global.FieldByName('NomeAzienda').Value)) > 0) then begin
          BCurriculum.enabled := False;
          BitBtn8.enabled := False;
          CBIRaltreRegole.Checked := true;
          CBIRnote.Checked := false;
          CBIRnote.Enabled := false;
          dxDBGrid1Column32.Visible := true;
          // CBIRFileSystem.Checked := false;
     end;
     //BCurriculum.enabled := False;
     //BitBtn8.enabled := False;
     CBIRaltreRegole.Checked := true;
     CBIRnote.Checked := false;
     CBIRnote.Enabled := false;
     // CBIRFileSystem.Checked := false;


     Caption := '[M/5] - ' + Caption;

     // Bottone Maschera riepilogativa
     if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 6)) = 'BOYDEN' then
          BRiepCand.visible := true
     else
          BRiepCand.visible := false;

     // INFORMATION RETRIEVAL
     // -- 1) verifica corretta installazione componente full-text
     xIsFulltextInstalled := MainForm.xIsFulltextInstalled;
     if (not xIsFulltextInstalled) and (Data.Global.FieldByName('AnagFileDentroDB').AsBoolean = not TRUE) then begin
          PanInfoRet.Visible := False;
     end else begin
          // -- 3) verifica esistenza indice full-text sulla tabella AnagAltreInfo
          Q.Close;
          Q.SQL.text := 'select OBJECTPROPERTY ( object_id(''AnagAltreInfo''),''TableHasActiveFulltextIndex'') AnagAltreInfoOK';
          Q.Open;
          xAnagAltreInfoOK := Q.FieldByName('AnagAltreInfoOK').asInteger = 1;
          // -- VERIFICA SERVIZIO DI INDICIZZAZIONE a livello di File-Systemo o esistenza indice full-text su AnagFile
          if Data.Global.FieldByName('AnagFileDentroDB').AsBoolean = TRUE then begin
               Q.Close;
               Q.SQL.text := 'select OBJECTPROPERTY ( object_id(''AnagFile''),''TableHasActiveFulltextIndex'') FileSystemOK';
               Q.Open;
               xFileSystemOK := Q.FieldByName('FileSystemOK').asInteger = 1;
          end else begin
               Q.Close;
               Q.SQL.text := 'select count(*) FileSystemOK from master.dbo.sysservers where srvname=''FileSystem''';
               Q.Open;
               xFileSystemOK := Q.FieldByName('FileSystemOK').asInteger = 1;
          end;
          // abilitazione checkbox
          CBIRnote.Enabled := xAnagAltreInfoOK;
          CBIRnote.Checked := xAnagAltreInfoOK;
          CBIRFileSystem.Enabled := xFileSystemOK;
          CBIRFileSystem.Checked := xFileSystemOK;
     end;

     // riempimento colonne griglia
     //RiempiColDescTabella;
     SelCriterioWizardForm := TSelCriterioWizardForm.create(self);

     // tipologia di ricerca a default (impostato per utente in Users.TipoRicDefault)
     Q.Close;
     Q.SQL.text := 'select TipoRicDefault from Users where ID=' + IntToStr(MainForm.xIDUtenteAttuale);
     Q.Open;
     xTipoRic := Q.FieldByName('TipoRicDefault').asInteger;
     if xTipoRic = 1 then begin
          //
          PCTipoRic.ActivePageIndex := 0;
          CaricaRicSemplice;
     end else begin
          PCTipoRic.ActivePageIndex := 1;
     end;
     Q.Close;

     // RICERCA SEMPLICE
     // riempire i campi che non ci sono gi�
     {Q2.Close;
     Q2.SQL.text := 'select DescTabella,Tabella,Campo,DescCampo,DescOperatore,DescCompleta,TabQueryOperatori.ID IDTabQueryOp ' +
          'from TabQuery,TabQueryOperatori where TabQuery.ID=TabQueryOperatori.IDTabQuery ' +
          'and DescCompleta is not null and TabQueryOperatori.ID not in  ' +
          ' (select distinct IDTabQueryOp from UserLineeQuery where IDTabQueryOp is not null  ' +
          '  and IDAnagrafica=' + IntToStr(MainForm.xIDUtenteAttuale) + ')';
     Q2.Open;
     while not Q2.EOF do begin
          with SelCriterioWizardForm do begin
               TLinee.Insert;
               TLineeIDAnagrafica.Value := MainForm.xIDUtenteAttuale;
               TLineeDescTabella.Value := Q2.FieldByName('DescTabella').asString;
               TLineeTabella.Value := Q2.FieldByName('Tabella').asString;
               TLineeDescCampo.Value := Q2.FieldByName('DescCampo').asString;
               TLineeDescOperatore.Value := Q2.FieldByName('DescOperatore').asString;
               TLineeDescCompleta.Value := Q2.FieldByName('DescCompleta').asString;
               TLineeIDTabQueryOp.Value := Q2.FieldByName('IDTabQueryOp').asInteger;
               TLineeOpSucc.Value := 'and';
               TLineeOpNext.Value := 'e';
               TLinee.Post;
          end;
          Q2.Next;
     end;
     Q2.Close;}

     // ### Non far vedere le pagine
     //for i:=0 to PCTipoRic.PageCount-1 do
     //PCTipoRic.Pages[0].TabVisible := False;
end;

procedure TSelPersNewForm.DBGrid4KeyPress(Sender: TObject; var Key: Char);
begin
     if TTabLookup.IndexName <> '' then begin
          if Key <> chr(13) then begin
               xStringa := xStringa + key;
               //               TTabLookup.FindNearest([xStringa]);
               TTabLookup.FindNearestADO(VarArrayOf([xStringa]));
          end;
     end;
end;

procedure TSelPersNewForm.BitBtn8Click(Sender: TObject);
var xFile: string;
     xProcedi: boolean;
begin
     // prosegui timing
     if xChiamante = 1 then SelPersForm.RiprendiAttivita;
     if not OkIDAnag(QRes1.FieldByName('ID').AsInteger) then exit;
     ApriCV(QRes1.FieldByName('ID').AsInteger);
     // prosegui timing
     if xChiamante = 1 then SelPersForm.RiprendiAttivita;

     RegistraVisioneCand(MainForm.xIDUtenteAttuale, QRes1ID.Value);
end;
//[TONI20021003]DEBUGOK

{procedure TSelPersNewForm.BAggiungiRicClick(Sender: TObject);
var i: integer;
  xMiniVal, xRic: string;
  xVai: boolean;
  SavePlace: TBookmark;
  xIDEvento, xIDClienteBlocco, xIDLocate: integer;
  xLivProtez, xDicMess, xPassword, xUtenteResp, xClienteBlocco: string;
  QLocal: TADOLinkedQuery;
begin

  if not Mainform.CheckProfile('210') then Exit;
  if not OkIDAnag(QRes1.FieldByName('ID').AsInteger) then exit;
  if QRes1.FieldByName('IDTipoStato').AsInteger = 3 then
    if MessageDlg('Il candidato risulta INSERITO in azienda. Continuare?', mtWarning, [mbYes, mbNo], 0) = mrNo then exit;
  xMiniVal := '';
  if not InputQuery('Inserimento nella ricerca', 'Valutazione (1 lettera):', xMiniVal) then exit;
  Q.Close;
  Q.SQL.Text := 'select ID from EBC_CandidatiRicerche where IDRicerca=' + Dataricerche.TRicerchePend.FieldByName('ID').asString + ' and IDAnagrafica=' + QRes1.FieldByname('ID').asString;
  Q.Open;
  if Q.IsEmpty then begin
    xVai := True;
    // controllo se � in selezione
    if QRes1.FieldByName('IDTipoStato').AsInteger = 1 then begin
      if MessageDlg('Il soggetto risulta IN SELEZIONE: ' + chr(13) + xRic + chr(13) + 'PROCEDERE COMUNQUE ?', mtWarning, [mbYes, mbNo], 0) = mrNo then xVai := False;
    end;
    // controllo se � gi� associato ad un'altra ricerca per la STESSA azienda
    Q.Close; Q.SQL.clear;
    Q.SQl.Add('select EBC_Ricerche.Progressivo,EBC_StatiRic.StatoRic from EBC_CandidatiRicerche,EBC_Ricerche,EBC_StatiRic ');
    Q.SQl.Add('where EBC_CandidatiRicerche.IDRicerca=EBC_Ricerche.ID and EBC_Ricerche.IDStatoRic=EBC_StatiRic.ID');
    Q.SQl.Add('and EBC_CandidatiRicerche.IDAnagrafica=:xIDAnag: and EBC_Ricerche.IDCliente=:xIDCliente:');
    //          Q.Prepare;
    Q.ParamByName['xIDAnag'] := QRes1.FieldByName('ID').AsInteger;
    Q.ParamByName['xIDCliente'] := DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger;
    Q.Open;
    if not Q.IsEmpty then begin
      xRic := '';
      while not Q.EOF do begin xRic := xRic + 'N�' + Q.FieldByName('Progressivo').asString + ' (' + Q.FieldByName('StatoRic').asString + ') '; Q.Next; end;
      if MessageDlg('Il soggetto � associato gi� alle seguenti ricerche per lo stesso cliente: ' + chr(13) + xRic + chr(13) + 'PROCEDERE COMUNQUE ?', mtWarning, [mbYes, mbNo], 0) = mrNo then xVai := False;
    end;
    // controllo incompatibilit� (per questa azienda e per le aziende a questa correlate)
    if IncompAnagCli(QRes1.FieldByName('ID').AsInteger, DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger) then
      if MessageDlg('Il soggetto risulta INCOMPATIBILE con questo cliente o con un cliente associato' + chr(13) +
        'PROCEDERE COMUNQUE ?', mtWarning, [mbYes, mbNo], 0) = mrNo then xVai := False;
    // controllo appartenenza CV
    Q.Close;
    Q.SQL.Text := 'select IDProprietaCV from Anagrafica where ID=' + QRes1.FieldByName('ID').asString;
    Q.Open;
    if (not ((Q.FieldByName('IDProprietaCV').asString = '') or (Q.FieldByName('IDProprietaCV').asInteger = 0))) and
      (Q.FieldByName('IDProprietaCV').asInteger <> DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger) then
      if MessageDlg('ATTENZIONE: il CV risulta di propriet� di un altro cliente.' + chr(13) +
        'Vuoi proseguire lo stesso ?', mtWarning, [mbYes, mbNo], 0) = mrNo then
        xVai := False;
    // controllo blocco livello 1 o 2
    xIDClienteBlocco := CheckAnagInseritoBlocco1(QRes1.FieldByName('ID').AsInteger);
    if xIDClienteBlocco > 0 then begin
      xLivProtez := copy(IntToStr(xIDClienteBlocco), 1, 1);
      if xLivProtez = '1' then begin
        xIDClienteBlocco := xIDClienteBlocco - 10000;
        xClienteBlocco := GetDescCliente(xIDClienteBlocco);
        xDicMess := 'Stai inserendo in commessa un candidato appartenente ad una azienda (' + xClienteBlocco + ') con blocco di livello 1, ';
      end else begin
        xIDClienteBlocco := xIDClienteBlocco - 20000;
        xClienteBlocco := GetDescCliente(xIDClienteBlocco);
        xDicMess := 'Stai inserendo in commessa un candidato appartenente ad una azienda ATTIVA (' + xClienteBlocco + '), ';
      end;
      if MessageDlg('ATTENZIONE!! PROTEZIONE CLIENTI LIVELLO ' + xLivProtez + chr(13) + xDicMess +
        'protetta dalle normative interne, se sei autorizzato premi SI, altrimenti premi NO. ' +
        'Se si prosegue, l''operazione verr� registrata nello storico del soggetto come forzatura. ' +
        'Verr� inviato un messaggio di promemoria al responsabile diretto', mtWarning, [mbYes, mbNo], 0) = mrNo then xVai := False
      else begin
        // pwd
        xUtenteResp := GetDescUtenteResp(MainForm.xIDUtenteAttuale);
        if not InputQuery('Forzatura blocco livello ' + xLivProtez, 'Password di ' + xUtenteResp, xPassword) then exit;
        if not CheckPassword(xPassword, GetPwdUtenteResp(MainForm.xIDUtenteAttuale)) then begin
          MessageDlg('Password errata', mtError, [mbOK], 0);
          xVai := False;
        end;
        if xVai then begin
          // promemoria al resp.
          QLocal := CreateQueryFromString('insert into Promemoria (IDUtente,IDUtenteDa,DataIns,Testo,Evaso,DataDaLeggere) ' +
            'values (:xIDUtente:,:xIDUtenteDa:,:xDataIns:,:xTesto:,:xEvaso:,:xDataDaLeggere:)');
          QLocal.ParamByName['xIDUtente'] := GetIDUtenteResp(MainForm.xIDUtenteAttuale);
          QLocal.ParamByName['xIDUtenteDa'] := MainForm.xIDUtenteAttuale;
          QLocal.ParamByName['xDataIns'] := Date;
          QLocal.ParamByName['xTesto'] := 'forzatura livello ' + xLivProtez + ' per CV n� ' + QRes1.FieldByName('CVNumero').AsString;
          QLocal.ParamByName['xEvaso'] := 0;
          QLocal.ParamByName['xDataDaLeggere'] := Date;
          QLocal.ExecSQL;
          // registra nello storico soggetto
          xIDEvento := GetIDEvento('forzatura Blocco livello ' + xLivProtez);
          if xIDEvento > 0 then begin
            with Data.Q1 do begin
              close;
              SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
                'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xIDCliente:)';
              ParamByName['xIDAnagrafica'] := QRes1.FieldByName('ID').AsInteger;
              ParamByName['xIDEvento'] := xIDevento;
              ParamByName['xDataEvento'] := Date;
              ParamByName['xAnnotazioni'] := 'cliente: ' + xClienteBlocco;
              ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
              ParamByName['xIDUtente'] := MainForm.xIDUtenteAttuale;
              ParamByName['xIDCliente'] := xIDClienteBlocco;
              ExecSQL;
            end;
          end;
        end;
      end;
    end;

    if xVai then begin
      Data.DB.BeginTrans;
      try
        if Q.Active then Q.Close;
        Q.SQL.text := 'insert into EBC_CandidatiRicerche (IDRicerca,IDAnagrafica,Escluso,Stato,DataIns,Minival) ' +
          'values (:xIDRicerca:,:xIDAnagrafica:,:xEscluso:,:xStato:,:xDataIns:,:xMinival:)';
        Q.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
        Q.ParamByName['xIDAnagrafica'] := QRes1.FieldByName('ID').AsInteger;
        Q.ParamByName['xEscluso'] := 0;
        Q.ParamByName['xStato'] := 'inserito da ricerca nomi';
        Q.ParamByName['xDataIns'] := Date;
        Q.ParamByName['xMiniVal'] := xMiniVal;
        Q.ExecSQL;
        // aggiornamento stato anagrafica
        if QRes1.FieldByName('IDStato').AsInteger <> 27 then begin
          if Q.Active then Q.Close;
          Q.SQL.text := 'update Anagrafica set IDStato=27,IDTipoStato=1 where ID=' + QRes1.FieldByName('ID').asString;
          Q.ExecSQL;
        end;
        // aggiornamento storico
        if Q.Active then Q.Close;
        Q.SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
          'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xIDCliente:)';
        Q.ParamByName['xIDAnagrafica'] := QRes1.FieldByName('ID').AsInteger;
        Q.ParamByName['xIDEvento'] := 19;
        Q.ParamByName['xDataEvento'] := date;
        Q.ParamByName['xAnnotazioni'] := 'ric.n� ' + TrimRight(DataRicerche.TRicerchePend.FieldByName('Progressivo').AsString) + ' (' + TrimRight(DataRicerche.TRicerchePend.FieldByName('Cliente').AsString) + ')';
        Q.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
        Q.ParamByName['xIDUtente'] := DataRicerche.TRicerchePend.FieldByName('IDUtente').AsInteger;
        Q.ParamByName['xIDCliente'] := DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger;
        Q.ExecSQL;
        Data.DB.CommitTrans;
        //DataRicerche.QCandRic.Next;
        //SavePlace:=DataRicerche.QCandRic.GetBookmark;
        DataRicerche.QCandRic.Close;
        DataRicerche.QCandRic.Open;
        //DataRicerche.QCandRic.GotoBookmark(SavePlace);
        //DataRicerche.QCandRic.FreeBookmark(SavePlace);
  // metti anche questo in quelli da escludere per le prossime ricerche
        for i := 1 to 500 do if xDaEscludere[i] = 0 then break;
        xDaEscludere[i] := QRes1.FieldByName('ID').AsInteger;
        //recuper ID per locate su record sucessivo
        QRes1.Next;
        xIDLocate := Qres1.FieldByname('ID').AsInteger;

        BitBtn2Click(self);

        Qres1.Locate('ID', xIDLocate, []);
      except
        Data.DB.RollbackTrans;
        MessageDlg('Errore sul database:  inserimento non avvenuto', mtError, [mbOK], 0);
      end;
    end;
  end;
  // prosegui timing
  if xChiamante = 1 then SelPersForm.RiprendiAttivita;
end;                                                            }

procedure TSelPersNewForm.TLinee_OLDBeforePost(DataSet: TDataSet);
begin
     if TLinee.FieldByName('OpNext').AsString = 'e' then TLinee.FieldByName('OpSucc').AsString := 'and';
     if TLinee.FieldByName('OpNext').AsString = 'o' then TLinee.FieldByName('OpSucc').AsString := 'or';
     if TLinee.FieldByName('OpNext').AsString = 'fine' then TLinee.FieldByName('OpSucc').AsString := '';
     RicreaStringaSQL;


end;

procedure TSelPersNewForm.FormClose(Sender: TObject;
     var Action: TCloseAction);
begin
     if xChiamante = 1 then begin
          // cancellazione vecchie linee query
          Query1.Close;
          Query1.SQL.Clear;
          Query1.SQL.Add('delete from RicLineeQuery where IDRicerca=' + DataRicerche.TRicerchePend.FieldByName('ID').asString);
          Query1.ExecSQL;
          TLinee.First;
          // salvataggio linee query
          TRicLineeQuery.Open;
          while not TLinee.EOF do begin
               Data.DB.BeginTrans;
               try
                    Q.Close;
                    Q.SQL.text := 'Insert Into RicLineeQuery (IDRicerca,Descrizione,Stringa,Tabella,OpSucc,Next,LivLinguaParlato,LivLinguaScritto,DescTabella,DescCampo,DescOperatore,Valore,QueryLookup) ' +
                         'values (:xIDRicerca:,:xDescrizione:,:xStringa:,:xTabella:,:xOpSucc:,:xNext:,:xLivLinguaParlato:,:xLivLinguaScritto:,:xDescTabella:,:xDescCampo:,:xDescOperatore:,:xValore:,:xQueryLookup:)';
                    Q.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
                    Q.ParamByName['xDescrizione'] := TLinee.FieldByName('Descrizione').AsString;
                    Q.ParamByName['xStringa'] := TLinee.FieldByName('Stringa').AsString;
                    Q.ParamByName['xTabella'] := TLinee.FieldByName('Tabella').AsString;
                    Q.ParamByName['xOpSucc'] := TLinee.FieldByName('OpSucc').AsString;
                    Q.ParamByName['xNext'] := TLinee.FieldByName('OpNext').AsString;
                    if TLineeLivLinguaParlato.Value = 0 then
                         Q.ParamByName['xLivLinguaParlato'] := NULL
                    else
                         Q.ParamByName['xLivLinguaParlato'] := TLineeLivLinguaParlato.Value;
                    if TLineeLivLinguaScritto.Value = 0 then
                         Q.ParamByName['xLivLinguaScritto'] := NULL
                    else
                         Q.ParamByName['xLivLinguaScritto'] := TLineeLivLinguaScritto.Value;
                    Q.ParamByName['xDescTabella'] := TLineeDescTabella.Value;
                    Q.ParamByName['xDescCampo'] := TLineeDescCampo.Value;
                    Q.ParamByName['xDescOperatore'] := TLineeDescOperatore.Value;
                    Q.ParamByName['xValore'] := TLineeValore.Value;
                    Q.ParamByName['xQueryLookup'] := TLineeQueryLookup.Value;
                    Q.ExecSQL;
                    Data.DB.CommitTrans;
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE:  operazione non completata', mtError, [mbOK], 0);
                    raise;
               end;
               TLinee.Next;
          end;
          TRicLineeQuery.Close;
          // registrazione evento di prima visione archivio, se non c'�
          if not TLinee.IsEmpty then begin
               Data.QTemp.Close;
               Data.QTemp.SQL.text := 'select count(*) Tot from EBC_StoricoRic where IDEventoRic=1 and IDRicerca=' + DataRicerche.TRicerchePend.FieldByName('ID').AsString;
               Data.QTemp.Open;
               if Data.QTemp.FieldByname('Tot').asInteger = 0 then begin
                    Q.Close;
                    Q.SQL.text := 'insert into EBC_StoricoRic (IDRicerca,DallaData,IDEventoRic) ' +
                         ' values (:xIDRicerca:,:xDallaData:,:xIDEventoRic:)';
                    Q.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
                    Q.ParamByName['xDallaData'] := Date;
                    Q.ParamByName['xIDEventoRic'] := 1;
                    Q.execSQL;
               end;
               Data.QTemp.Close;
          end;
     end;
     SelCriterioWizardForm.Free;
end;

procedure TSelPersNewForm.BCurriculumClick(Sender: TObject);
var x: string;
     i: integer;
begin
     // prosegui timing
     if xChiamante = 1 then SelPersForm.RiprendiAttivita;
     if not OkIDAnag(QRes1.FieldByName('ID').AsInteger) then exit;
     if not QRes1.Eof then begin
          PosizionaAnag(QRes1.FieldByName('ID').AsInteger);

          CurriculumForm.ShowModal;

          MainForm.Pagecontrol5.ActivePage := MainForm.TSStatoTutti;
     end;
     // prosegui timing
     if xChiamante = 1 then SelPersForm.RiprendiAttivita;

     RegistraVisioneCand(MainForm.xIDUtenteAttuale, QRes1ID.Value);
end;

procedure TSelPersNewForm.BitBtn9Click(Sender: TObject);
begin
     // prosegui timing
     if xChiamante = 1 then SelPersForm.RiprendiAttivita;
     if not OkIDAnag(QRes1.FieldByName('ID').AsInteger) then exit;
     ApriFileWord(QRes1.FieldByName('CVNumero').asString);
     // prosegui timing
     if xChiamante = 1 then SelPersForm.RiprendiAttivita;

     RegistraVisioneCand(MainForm.xIDUtenteAttuale, QRes1ID.Value);
end;

//[TONI20021003]DEBUGOK

procedure TSelPersNewForm.BitBtn10Click(Sender: TObject);
var i, xID: integer;
     xSQL: string;
begin // prosegui timing
     if xChiamante = 1 then SelPersForm.RiprendiAttivita;
     if not OkIDAnag(QRes1.FieldByName('ID').AsInteger) then exit;
     if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 5)) = 'ERGON' then begin
          SchedaSinteticaForm := TSchedaSinteticaForm.create(self);
          SchedaSinteticaForm.xIDAnag := QRes1.FieldByName('ID').AsInteger;
          SchedaSinteticaForm.ShowModal;
          SchedaSinteticaForm.Free;
     end else begin
          if (Uppercase(copy(Data.Global.FieldByName('NomeAzienda').Value, 1, 6)) = 'ADVANT') or (pos('MCS', UpperCase(Data.Global.FieldByName('NomeAzienda').Value)) > 0) then begin
               // QRes1 dalla 10a riga in poi
              { xSQL := 'select distinct Anagrafica.ID';
               for i := 9 to QRes1.SQL.Count - 2 do begin
                 xSQL := xSQL + chr(13) + QRes1.SQL.Strings[i];
               end;    }
               // predisponi Data.TAnagrafica
               Data.Tanagrafica.Close;
               Data.TAnagrafica.SQL.text := 'SELECT Anagrafica.ID, Matricola, Cognome, Nome, Titolo,' + chr(13) +
                    'DataNascita, GiornoNascita, MeseNascita, LuogoNascita,' + chr(13) +
                    'Sesso, Indirizzo, Cap, Comune, IDComuneRes, IDZonaRes,' + chr(13) +
                    'Provincia, DomicilioIndirizzo, DomicilioCap, DomicilioComune,' + chr(13) +
                    'IDComuneDom, IDZonaDom, DomicilioProvincia, DomicilioStato,' + chr(13) +
                    'RecapitiTelefonici, Cellulare, Fax, CodiceFiscale, PartitaIVA,' + chr(13) +
                    'Email, IDStatoCivile, Anagrafica.IDStato, Anagrafica.IDTipoStato,' + chr(13) +
                    'CVNumero, CVIDAnnData, CVinseritoInData, Foto, IDTipologia, ' + chr(13) +
                    'IDUtenteMod, DubbiIns, IDProprietaCV, TelUfficio, OldCVNumero, ' + chr(13) +
                    'EBC_Stati.Stato DescStato, TipoStrada, DomicilioTipoStrada, NumCivico, DomicilioNumCivico, ' + chr(13) +
                    'IDUtente, Anagrafica.Stato, LibPrivacy, CVDataReale, CVDataAgg, Interessanteinternet, VIPList, ' + chr(13) +
                    'telaziendacentralino, telaziendasegretaria,notecellulare,cellulare2,notecellulare2,emailufficio,regione,domicilioregione ' + chr(13) +
                    'FROM Anagrafica,EBC_Stati ' + chr(13) +
                    'WHERE Anagrafica.IDStato=EBC_Stati.ID ' + chr(13) +
                    'and anagrafica.id = ' + Qres1Id.asstring;
               //'and Anagrafica.ID in (' + chr(13) + xSQL + ')';
               //Data.TAnagrafica.SQL.SaveToFile('AnagQRes1.txt');
               Data.TAnagrafica.Open;
               xID := QRes1ID.Value;
               Data.TAnagrafica.Locate('ID', xID, []);
               ASASchedaCandForm := TASASchedaCandForm.create(self);
               ASASchedaCandForm.ShowModal;
               ASASchedaCandForm.Free;
               QRes1.Close;
               QRes1.Open;
               if Data.TAnagrafica.active then xID := Data.TAnagraficaID.value;
               QRes1.Locate('ID', xID, []);
          end else begin

               if QRes1.RecordCount > 0 then begin
                    SchedaCandForm := TSchedaCandForm.create(self);
                    if not PosizionaAnag(QRes1.FieldByName('ID').AsInteger) then exit;
                    SchedaCandForm.ShowModal;
                    SchedaCandForm.Free;
               end;
          end;
     end;
     // prosegui timing
     if xChiamante = 1 then SelPersForm.RiprendiAttivita;

     RegistraVisioneCand(MainForm.xIDUtenteAttuale, QRes1ID.Value);
end;

procedure TSelPersNewForm.QRes1_OLDCalcFields(DataSet: TDataSet);
var xAnno, xMese, xgiorno: Word;
begin
     if QRes1.FieldByName('DataNascita').asString = '' then
          QRes1.FieldByName('Eta').asString := ''
     else begin
          DecodeDate((Date - QRes1.FieldByName('DataNascita').AsDateTime), xAnno, xMese, xgiorno);
          // TAnagraficaEta.Value:=StrToInt(copy(xAnno,3,2));
          QRes1.FieldByName('Eta').Value := copy(IntToStr(xAnno), 3, 2);
     end;
     QRes1.FieldByName('TelUffCell').AsString := QRes1.FieldByName('TelUfficio').AsString + ' - ' + QRes1.FieldByName('Cellulare').AsString;
end;

procedure TSelPersNewForm.CBColloquiatiClick(Sender: TObject);
var i: integer;
begin
     xOrdine := 'order by Anagrafica.CVNumero';
     if QRes1.Active then begin
          xTipoQuery := 0;
          EseguiQueryRic;
     end;
end;

procedure TSelPersNewForm.QRes1_OLDBeforeOpen(DataSet: TDataSet);
begin
     QEspLavLK.Open;
     QLastEspLav.Open;
end;

procedure TSelPersNewForm.QRes1_OLDAfterClose(DataSet: TDataSet);
begin
     QEspLavLK.Close;
     QLastEspLav.Close;
end;

procedure TSelPersNewForm.dxDBGrid1Click(Sender: TObject);
begin
     //if xChiamante=1 then SelPersForm.RiprendiAttivita;
end;

procedure TSelPersNewForm.dxDBGrid1CustomDrawCell(Sender: TObject;
     ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
     AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
     var AText: string; var AColor: TColor; AFont: TFont;
     var AAlignment: TAlignment; var ADone: Boolean);
var Value: variant;
     xFile: string;
begin
     if ANode.HasChildren then Exit;
     if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 6)) <> 'BOYDEN' then begin
          if AColumn = dxDBGrid1CVNumero then begin
               Value := ANode.Values[dxDBGrid1ggDataInsCV.Index];
               if not VarIsNull(Value) then begin
                    if Value > 30 then AFont.Color := clGreen; // verde: almeno 30 gg. di vita
                    if Value > 60 then AFont.Color := $000080FF; // arancione: almeno 60 gg. di vita
                    if Value > 90 then AFont.Color := clYellow; // giallo: almeno 90 gg. di vita
                    if Value > 120 then AFont.Color := clRed; // rosso: almeno 120 gg. di vita
                    if Value > 180 then AFont.Color := clMaroon; // marrone: almeno 180 gg. di vita
               end;
          end;
     end;
     if (Acolumn = dxDBGrid1ggUltimaDataIns) and (ANode.Values[dxDBGrid1ggUltimaDataIns.index] < xggDiffDi)
          and (ANode.Values[dxDBGrid1ggUltimaDataIns.index] > 0) then Acolor := clRed;
     if (Acolumn = dxDBGrid1ggDataUltimoContatto) and (ANode.Values[dxDBGrid1ggDataUltimoContatto.index] < xggDiffUc)
          and (ANode.Values[dxDBGrid1ggDataUltimoContatto.Index] > 0) then Acolor := clRed;
     if (Acolumn = dxDBGrid1DataProssimoColloquio) and (ANode.Values[dxDBGrid1DataProssimoColloquio.index] > 0)
          and (ANode.Values[dxDBGrid1DataProssimoColloquio.index] >= date) then Acolor := clYellow;
     if (Acolumn = dxDBGrid1ggDataUltimoColloquio) and (ANode.Values[dxDBGrid1ggDataUltimoColloquio.index] < xggDiffCol)
          and (ANode.Values[dxDBGrid1ggDataUltimoColloquio.index] > 0) then Acolor := clAqua;

     // controllo mancanza CV
     if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 6)) = 'BOYDEN' then begin
          // Solo per la Boyden guarda il campo "No CV"
          {if (Acolumn=dxDBGrid1CVNumero) then
               if (ANode.Values[dxDBGrid1NoCv.Index]<>'') then
                    if ANode.Values[dxDBGrid1NoCv.Index]=True then
                         AFont.Color:=clRed;}
     end else begin
          // per tutti gli altri guarda l'esistenza del file
          if xCheckRicCandCV then begin
               if (Acolumn = dxDBGrid1CVNumero) then begin
                    xFile := GetCVPath + '\i' + AText + 'a.gif';
                    if not FileExists(xFile) then AFont.Color := clRed;
               end;
          end;
     end;

     if (pos('INTERMEDIA', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) = 0) then begin
          if ((Acolumn = dxDBGrid1Cognome) or (Acolumn = dxDBGrid1Nome)) and (CheckVisioneCand(MainForm.xIDUtenteAttuale, ANode.Values[dxDBGrid1IDAnagrafica.index]))
               then begin
               AColor := $00FFB66C;
          end;
     end;
end;

procedure TSelPersNewForm.dxDBGrid1MouseUp(Sender: TObject;
     Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
     if (Button <> mbRight) or (Shift <> []) then Exit;
     TdxDBGridPopupMenuManager.Instance.ShowGridPopupMenu(Sender as TdxDBGrid);
end;

procedure TSelPersNewForm.Stampaelenco1Click(Sender: TObject);
begin
     if not MainForm.CheckProfile('51') then Exit;
     if not LogEsportazioniCand then exit;
     QRElencoSel := TQRElencoSel.create(self);
     QRElencoSel.QRMemo1.Lines.Clear;
     TLinee.First;
     while not TLinee.EOF do begin
          QRElencoSel.QRMemo1.Lines.Add(TLinee.FieldByName('Descrizione').AsString);
          TLinee.Next;
     end;
     if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 5)) = 'ERGON' then begin
          QRElencoSel.QRLabel1.Caption := 'vecchio n�';
          QRElencoSel.QRDBText1.DataField := 'OLDCVNumero';
          // non funziona: QRElencoSel.QRLabel6.Visible:=False;
          //               QRElencoSel.QRDBText4.Visible:=False;
     end;
     QRElencoSel.Preview;
     QRElencoSel.Free;
end;

procedure TSelPersNewForm.Save(ADefaultExt, AFilter, AFileName: string; AMethod: TSaveMethod);
begin
     with SaveDialog do
     begin
          DefaultExt := ADefaultExt;
          Filter := AFilter;
          FileName := AFileName;
          if Execute then AMethod(FileName, True);
     end;
end;

procedure TSelPersNewForm.EsportainHTML1Click(Sender: TObject);
begin
     if not MainForm.CheckProfile('53') then Exit;
     if not LogEsportazioniCand then exit;
     Save('htm', 'HTML File (*.htm; *.html)|*.htm', 'ExpGrid.htm', dxDBGrid1.SaveToHTML);
end;

procedure TSelPersNewForm.EsportainExcel1Click(Sender: TObject);
begin
     if not MainForm.CheckProfile('53') then Exit;
     if not LogEsportazioniCand then exit;
     Save('xls', 'Microsoft Excel 4.0 Worksheet (*.xls)|*.xls', 'ExpGrid.xls', dxDBGrid1.SaveToXLS);
end;

procedure TSelPersNewForm.ToolbarButton971Click(Sender: TObject);
begin
     if not MainForm.CheckProfile('50') then Exit;
     LegendaRicForm := TLegendaRicForm.create(self);
     LegendaRicForm.ShowModal;
     LegendaRicForm.Free;
     Q.Close;
     Q.SQl.text := 'select ggDiffDi,ggDiffUc,ggDiffCol,CheckRicCandCV from global ';
     Q.Open;
     xggDiffDi := Q.FieldByname('ggDiffDi').asInteger;
     xggDiffUc := Q.FieldByname('ggDiffUc').asInteger;
     xggDiffCol := Q.FieldByname('ggDiffCol').asInteger;
     xCheckRicCandCV := Q.FieldByname('CheckRicCandCV').asBoolean;
     Q.Close;
     xTipoQuery := 0;
     EseguiQueryRic;
end;

procedure TSelPersNewForm.ToolbarButton973Click(Sender: TObject);
var i: integer;
begin
     if not MainForm.CheckProfile('52') then Exit;
     if QRes1.isEmpty then exit;
     if Qres1.RecordCount > 501 then begin
          Messagedlg('Attenzione: il risultato della ricerca � troppo alto (Max 500) per procedere con l''invio delle mail', mtWarning, [mbOk], 0);
     end else begin
          FaxPresentazForm := TFaxPresentazForm.create(self);
          FaxPresentazForm.ASG1.RowCount := QRes1.RecordCount + 1;
          QRes1.First;
          QRes1.DisableControls;
          i := 1;
          while not QRes1.EOF do begin
               FaxPresentazForm.ASG1.addcheckbox(0, i, false, false);
               FaxPresentazForm.ASG1.Cells[0, i] := QRes1.FieldByName('Cognome').AsString + ' ' + QRes1.FieldByName('Nome').AsString;
               FaxPresentazForm.xArrayIDAnag[i] := QRes1.FieldByName('ID').AsInteger;
               QRes1.Next;
               inc(i);
          end;
          QRes1.EnableControls;
          FaxPresentazForm.PanClienti.Visible := False;
          FaxPresentazForm.ShowModal;
          FaxPresentazForm.Free;
     end;
end;

procedure TSelPersNewForm.Stampagriglia1Click(Sender: TObject);
begin
     if not MainForm.CheckProfile('53') then Exit;
     if not LogEsportazioniCand then exit;
     dxPrinter1.Preview(True, nil);
end;

procedure TSelPersNewForm.TTabQuery_OLDAfterOpen(DataSet: TDataSet);
begin
     TTabOp.Open;
end;

function TSelPersNewForm.LogEsportazioniCand: boolean;
var xCriteri: string;
begin
     // restituisce FALSE se l'utente ha abortito l'operazione
     Result := True;
     if MessageDlg('ATTENZIONE:  l''esportazione di dati � un''operazione soggetta a registrazione' + chr(13) +
          'secondo la normativa sulla privacy.  SEI SICURO DI VOLER PROSEGUIRE ?', mtWarning, [mbYes, mbNo], 0) = mrNo then begin
          Result := False;
          exit;
     end;
     with Data do begin
          DB.BeginTrans;
          try
               TLinee.First;
               xCriteri := '';
               while not TLinee.EOF do begin
                    xCriteri := xCriteri + TLinee.FieldByName('Descrizione').AsString + chr(13);
                    TLinee.Next;
               end;
               Q1.SQL.text := 'insert into LogEsportazCand (IDUtente,Data,Criteri,StringaSQL,ResultNum) ' +
                    ' values (:xIDUtente:,:xData:,:xCriteri:,:xStringaSQL:,:xResultNum:)';
               Q1.ParamByName['xIDUtente'] := MainForm.xIDUtenteAttuale;
               Q1.ParamByName['xData'] := Date;
               Q1.ParamByName['xCriteri'] := xCriteri;
               Q1.ParamByName['xStringaSQL'] := QRes1.SQL.Text;
               Q1.ParamByName['xResultNum'] := QRes1.RecordCount;
               Q1.ExecSQL;

               DB.CommitTrans;
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: operazione non effettuata', mtError, [mbOK], 0);
          end;
     end;
end;

procedure TSelPersNewForm.cancellasoggetti1Click(Sender: TObject);
var xPassword: string;
begin
     if QRes1.IsEmpty then exit;
     // cancellazione soggetti selezionati
     if MessageDlg('ATTENZIONE: la procedura � IRREVERSIBILE e DEFINITIVA !!' + chr(13) +
          'E'' necessario verificare il rispetto della normativa sulla Privacy e sulla' + chr(13) +
          'conservazione dei relativi dati.' + chr(13) + chr(13) +
          'SEI DAVVERO SICURO DI VOLER PROCEDERE ?', mtWarning,
          [mbNo, mbYes], 0) = mrNo then exit;

     // richiesta password amministratore
     xPassword := '';
     if not InputQuery('cancellazione soggetti', 'Password di amministratore', xPassword) then exit;
     Q.Close;
     Q.SQL.text := 'select Password from Users where Nominativo=''Administrator''';
     Q.Open;
     if not CheckPassword(xPassword, Q.Fieldbyname('Password').asString) then begin
          MessageDlg('Password di amministratore errata', mtError, [mbOK], 0);
          Q.Close;
          exit;
     end;
     Q.Close;
     if MessageDlg('Verranno eliminati TUTTI i dati di TUTTE le tabelle collegate per TUTTI i SOGGETTI !' + chr(13) +
          'IMPORTANTE: L''OPERAZIONE E'' IRREVERSIBILE e  non sar� pi� possibile recuperare i dati !!!' + chr(13) + chr(13) +
          'SEI DAVVERO SICURO DI VOLER PROCEDERE ?', mtWarning,
          [mbNo, mbYes], 0) = mrYes then begin
          // vai - DEFINITIVA per tutti i soggetti !!
          QRes1.First;
          while not QRes1.EOF do begin
               CancellaSogg(QRes1.FieldByName('ID').AsInteger);
               QRes1.Next;
          end;
          xTipoQuery := 0;
          EseguiQueryRic;
     end;
end;

procedure TSelPersNewForm.PMStati1Click(Sender: TObject);
begin
     PMStati1.Checked := not PMStati1.Checked;
end;

procedure TSelPersNewForm.PMStati2Click(Sender: TObject);
begin
     PMStati2.Checked := not PMStati2.Checked;
end;

procedure TSelPersNewForm.PMStati3Click(Sender: TObject);
begin
     PMStati3.Checked := not PMStati3.Checked;
end;

procedure TSelPersNewForm.PMStati4Click(Sender: TObject);
begin
     PMStati4.Checked := not PMStati4.Checked;
end;

procedure TSelPersNewForm.PMStati5Click(Sender: TObject);
begin
     PMStati5.Checked := not PMStati5.Checked;
end;

procedure TSelPersNewForm.cancellatuttiisoggettiselezionati1Click(Sender: TObject);
var xPassword: string;
     k: integer;
begin
     if QRes1.IsEmpty then exit;
     // cancellazione soggetti selezionati
     if MessageDlg('ATTENZIONE: la procedura � IRREVERSIBILE e DEFINITIVA !!' + chr(13) +
          'E'' necessario verificare il rispetto della normativa sulla Privacy e sulla' + chr(13) +
          'conservazione dei relativi dati.' + chr(13) + chr(13) +
          'SEI DAVVERO SICURO DI VOLER PROCEDERE ?', mtWarning,
          [mbNo, mbYes], 0) = mrNo then exit;

     // richiesta password amministratore
     xPassword := '';
     if not InputQuery('cancellazione soggetti', 'Password di amministratore', xPassword) then exit;
     Q.Close;
     Q.SQL.text := 'select Password from Users where Nominativo=''Administrator''';
     Q.Open;
     if not CheckPassword(xPassword, Q.Fieldbyname('Password').asString) then begin
          MessageDlg('Password di amministratore errata', mtError, [mbOK], 0);
          Q.Close;
          exit;
     end;
     Q.Close;
     if MessageDlg('Verranno eliminati TUTTI i dati di TUTTE le tabelle collegate per TUTTI i SOGGETTI SELEZIONATI !' + chr(13) +
          'IMPORTANTE: L''OPERAZIONE E'' IRREVERSIBILE e  non sar� pi� possibile recuperare i dati !!!' + chr(13) + chr(13) +
          'SEI DAVVERO SICURO DI VOLER PROCEDERE ?', mtWarning,
          [mbNo, mbYes], 0) = mrYes then begin
          // vai - DEFINITIVA per tutti i soggetti SELEZIONATI !!
          for k := 0 to dxDBGrid1.SelectedCount - 1 do begin
               QRes1.BookMark := dxDBGrid1.SelectedRows[k];
               CancellaSogg(QRes1.FieldByName('ID').AsInteger);
          end;
          xTipoQuery := 0;
          EseguiQueryRic;
     end;
end;

procedure TSelPersNewForm.mettiinstatoeliminato1Click(Sender: TObject);
var k: integer;
begin
     if MessageDlg('TUTTI i SOGGETTI SELEZIONATI verranno messi nello stato "eliminato".' + chr(13) +
          'SEI DAVVERO SICURO DI VOLER PROCEDERE ?', mtWarning,
          [mbNo, mbYes], 0) = mrYes then begin
          // metti in stato ELIMINATO
          with Data do begin
               DB.BeginTrans;
               try
                    for k := 0 to dxDBGrid1.SelectedCount - 1 do begin
                         QRes1.BookMark := dxDBGrid1.SelectedRows[k];
                         // modifica stato e tipostato
                         Q1.SQL.text := 'update Anagrafica set IDStato=:xIDStato:,IDTipoStato=:xIDTipoStato: where ID=' + QRes1.FieldByName('ID').AsString;
                         Q1.ParamByName['xIDStato'] := 30;
                         Q1.ParamByName['xIDTipoStato'] := 5;
                         Q1.ExecSQL;
                         // evento in storico
                         Q1.SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
                              'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xIDCliente:)';
                         Q1.ParamByName['xIDAnagrafica'] := QRes1.FieldByName('ID').AsInteger;
                         Q1.ParamByName['xIDEvento'] := 80;
                         Q1.ParamByName['xDataEvento'] := Date;
                         Q1.ParamByName['xAnnotazioni'] := 'eliminazione da motore di ricerca';
                         Q1.ParamByName['xIDRicerca'] := 0;
                         Q1.ParamByName['xIDUtente'] := MainForm.xIDUtenteAttuale;
                         Q1.ParamByName['xIDCliente'] := 0;
                         Q1.ExecSQL;
                    end;
                    DB.CommitTrans;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: operazione non effettuata', mtError, [mbOK], 0);
               end;
          end;
          xTipoQuery := 0;
          EseguiQueryRic;
     end;
end;

procedure TSelPersNewForm.DBGrid3LivLinguaParlatoButtonClick(
     Sender: TObject; AbsoluteIndex: Integer);
var xID: string;
begin
     if TrimRight(TLineeTabella.Value) = 'LingueConosciute' then begin
          xID := OpenSelFromTab('LivelloLingue', 'ID', 'LivelloConoscenza', 'Livello conoscenza', TrimRight(TLineeLivLinguaParlato.asString), False);
          if xID <> '' then begin
               if not (dsLinee.state = dsEdit) then TLinee.Edit;
               TLineeLivLinguaParlato.Value := StrToIntDef(xID, 0);
               TLinee.Post;
          end;
     end;
end;

procedure TSelPersNewForm.DBGrid3LivLinguaScrittoButtonClick(
     Sender: TObject; AbsoluteIndex: Integer);
var xID: string;
begin
     if TrimRight(TLineeTabella.Value) = 'LingueConosciute' then begin
          xID := OpenSelFromTab('LivelloLingue', 'ID', 'LivelloConoscenza', 'Livello conoscenza', TrimRight(TLineeLivLinguaScritto.asString), False);
          if xID <> '' then begin
               if not (dsLinee.state = dsEdit) then TLinee.Edit;
               TLineeLivLinguaScritto.Value := StrToIntDef(xID, 0);
               TLinee.Post;
          end;
     end;
end;

procedure TSelPersNewForm.Salvaquerysufile1Click(Sender: TObject);
begin
     if not QRes1.Active then exit;
     if opendialog1.Execute then begin
          QRes1.SQL.SaveToFile(OpenDialog1.FileName);
     end;
end;

procedure TSelPersNewForm.BRiepCandClick(Sender: TObject);
var xID: integer;
begin
     MainForm.xIDAnagrafica := QRes1.FieldByName('ID').AsString;
     RiepilogoCandForm := TRiepilogoCandForm.Create(self);
     RiepilogoCandForm.ShowModal;
     RiepilogoCandForm.Free;
     xID := QRes1.FieldByName('ID').asInteger;
     QRes1.Close;
     QRes1.Open;
     QRes1.Locate('ID', xID, []);
end;

procedure TSelPersNewForm.DBGrid3DescCampoButtonClick(Sender: TObject; AbsoluteIndex: Integer);
begin
     PredisponiWizard;
     // setta variabile "pagina attiva"
     SelCriterioWizardForm.xPaginaAttiva := 1;
     SelCriterioWizardForm.ShowModal;
     if SelCriterioWizardForm.ModalResult = mrOK then begin
          SalvaDaWizard;
          RicreaStringaSQL;
     end;
end;

procedure TSelPersNewForm.DBGrid3DescTabellaButtonClick(Sender: TObject; AbsoluteIndex: Integer);
begin
     PredisponiWizard;
     // setta variabile "pagina attiva"
     SelCriterioWizardForm.xPaginaAttiva := 0;
     SelCriterioWizardForm.ShowModal;
     if SelCriterioWizardForm.ModalResult = mrOK then begin
          SalvaDaWizard;
          RicreaStringaSQL;
     end;
end;

procedure TSelPersNewForm.RicreaStringaSQL;
var xStringa, xTabella, xCampo, xTipoCampo, xValoriPossibili, xTabLookup, xOperatore, xValore: string;
     xVal1, xVal0, xQueryLookup, xSubQuery: string;
     xIDTabQuery: integer;
begin
     // ricrea la stringa SQL per la riga
     Q.Close;
     Q.SQL.text := 'select * from TabQuery where DescTabella=:xDescTabella: and DescCampo=:xDescCampo:';
     Q.ParamByName['xDescTabella'] := TLineeDescTabella.Value;
     Q.ParamByName['xDescCampo'] := TLineeDescCampo.Value;
     Q.Open;
     // tabella e campo
     xTabella := Q.FieldByName('Tabella').asString;
     xCampo := Q.FieldByName('Campo').asString;
     xTipoCampo := Q.FieldByName('TipoCampo').asString;
     xSubQuery := Q.FieldByName('SubQuery').AsString;
     if (Q.FieldByName('TabLookup').asString <> '') {and (xSubQuery = '')} then
          xQueryLookup := 'select ' + Q.FieldByName('FieldLookup').asString + ' Dicitura from ' + Q.FieldByName('TabLookup').asString + ' where id=:xID:'
     else xQueryLookup := '';
     xValoriPossibili := Q.FieldByName('ValoriPossibili').asString;
     if xValoriPossibili <> '' then begin
          xVal1 := copy(xValoriPossibili, 1, pos(',', xValoriPossibili) - 1);
          xVal0 := copy(xValoriPossibili, pos(',', xValoriPossibili) + 1, length(xValoriPossibili));
     end;
     xTabLookup := Q.FieldByName('TabLookup').asString;
     xIDTabQuery := Q.FieldByName('ID').asInteger;

     // operatore
     Q.Close;
     Q.SQL.text := 'select * from TabQueryOperatori where IDTabQuery=:xIDTabQuery: and DescOperatore=:xDescOperatore:';
     Q.ParamByName['xIDTabQuery'] := xIDTabQuery;
     Q.ParamByName['xDescOperatore'] := TLineeDescOperatore.Value;
     Q.Open;
     xOperatore := Q.FieldByName('Operatore').asString;
     // casistica valori
     if xTipoCampo = '' then begin
          if xOperatore = 'like' then xValore := '''%' + TLineeValore.Value + '%'''
          else xValore := '''' + TLineeValore.Value + '''';
     end;
     if xTipoCampo = 'boolean' then begin
          if TLineeValore.Value = xVal1 then xValore := '1'
          else xValore := '0';
     end;
     if xTipoCampo = 'date' then begin
          xValore := '''' + TLineeValore.Value + '''';
     end;
     if Uppercase(xTipoCampo) = 'DATESP' then begin
          if UpperCase(TLineeDescCampo.Value) = 'COMPLEANNO' then begin
               // xStringa := 'month (' + TTabQueryCampo.AsString + ') =  Month(GetDate()) ' +
                 //    'and day (' + TTabQueryCampo.AsString + ') =  day(GetDate())';

          end;
     end;
     if xTipoCampo = 'integer' then begin
          xValore := TLineeValore.Value;
     end;
     // composizione stringa
     if xSubQuery = '' then begin
          if UpperCase(xTipoCampo) = 'DATESP' then begin
               if UpperCase(TLineeDescCampo.Value) = 'COMPLEANNO' then
                    xStringa := 'month (' + xCampo + ') =  Month(GetDate()) ' +
                         'and day (' + xCampo + ') =  day(GetDate())';
          end else
               xStringa := xTabella + '.' + xCampo + ' ' + xOperatore + ' ' + xValore
     end else begin
          xStringa := StringReplace(xSubQuery, ':xOperatore', xOperatore, [rfReplaceAll, rfIgnoreCase]);
          xStringa := StringReplace(xStringa, ':xValore', xValore, [rfReplaceAll, rfIgnoreCase]);
          if UpperCase(copy(xSubQuery, 1, 3)) = 'IN ' then
               xStringa := 'Anagrafica.ID' + ' ' + xStringa;
     end;


     // query specifica per l'et�
     if copy(TLineeDescCampo.Value, 1, 3) = 'et�' then
          xStringa := 'DATEDIFF(year, Anagrafica.DataNascita, getdate()) ' + xOperatore + ' ' + xValore;

     // salvataggio
     TLineeStringa.Value := xStringa;
     TLineeQueryLookup.Value := xQueryLookup;
end;

procedure TSelPersNewForm.SalvaDaWizard(x: integer);
var xdesc: string;
begin
     // x=0 --> normale
     // x=1 --> senza chiamare la form SelCriterioWizardForm
     if not (DsLinee.State = dsEdit) then TLinee.edit;
     TLineeDescTabella.Value := SelCriterioWizardForm.QTabelle.FieldByName('DescTabella').asString;
     //Q.Close;
     //Q.SqL.Text:='select Tabella from TabQuery where DescTabella=:xDesc:';
     //Q.ParamByName['xDesc']:=SelCriterioWizardForm.QTabelle.FieldByName('DescTabella').asString;
     //Q.Open;
     //TLineeTabella.Value:=Q.FieldByName('Tabella').asString;
     TLineeTabella.Value := SelCriterioWizardForm.QTabelle.FieldByName('Tabella').asString;
     //Q.Close;
     if SelCriterioWizardForm.QCampi.Active then
          TLineeDescCampo.Value := SelCriterioWizardForm.QCampi.FieldByName('DescCampo').asString;
     if SelCriterioWizardForm.QOperatori.Active then
          TLineeDescOperatore.Value := SelCriterioWizardForm.QOperatori.FieldByName('DescOperatore').asString;
     if x = 0 then begin
          with SelCriterioWizardForm do begin
               if dxEdit1.visible then begin
                    TLineeValore.Value := dxEdit1.Text;
                    xDesc := dxEdit1.Text;
               end;
               if dxPickEdit1.visible then begin
                    TLineeValore.Value := dxPickEdit1.Text;
                    xDesc := dxPickEdit1.Text;
               end;
               if dxDateEdit1.visible then begin
                    TLineeValore.Value := DateToStr(dxDateEdit1.Date);
                    xDesc := DateToStr(dxDateEdit1.Date);
               end;
               if dxSpinEdit1.visible then begin
                    TLineeValore.Value := IntToStr(round(dxSpinEdit1.value));
                    xDesc := IntToStr(round(dxSpinEdit1.value));
               end;
               if PanGrid.visible then begin
                    TLineeValore.Value := QTable.FieldByName('ID').AsString;
                    xDesc := QTable.Fields[1].AsString;
               end;
          end;
     end;
     // descrizione completa (per ricerca semplice)
     TLineeDescCompleta.Value := SelCriterioWizardForm.QCampi.FieldByName('DescCampo').asString + ' ' +
          SelCriterioWizardForm.QOperatori.FieldByName('DescOperatore').asString; //SelCriterioWizardForm.QOperatori.FieldByName('DescCompleta').asString;
     // puntatore alla riga in TabQueryOperatori
     TLineeIDTabQueryOp.Value := SelCriterioWizardForm.QOperatori.FieldByName('ID').asInteger;
     //TLineeDescrizione.Value := SelCriterioWizardForm.QOperatori.FieldByName('DescCompleta').asString;
     TLineeDescrizione.Value := SelCriterioWizardForm.QCampi.FieldByName('DescCampo').asString + ' ' +
          SelCriterioWizardForm.QOperatori.FieldByName('DescOperatore').asString + ' ' + xDesc;
     {TTabQueryDescCampo.asString + ' ' +
               TTabOpDescOperatore.asString + ' ' +
                    xValDesc}
     if TLineeOpSucc.Value = '' then TLineeOpSucc.Value := 'and';
     if TLineeOpNext.Value = '' then TLineeOpNext.Value := 'e';
end;

procedure TSelPersNewForm.PredisponiWizard;
begin
     // carica dati / predisponi wizard (setta le sue variabli)
     with SelCriterioWizardForm do begin
          // TABELLA
          if not QTabelle.Active then QTabelle.Open;
          xTabella := TLineeDescTabella.Value;
          if xTabella <> '' then QTabelle.Locate('DescTabella', xTabella, []);
          // CAMPO
          xCampo := TLineeDescCampo.Value;
          if xCampo <> '' then begin
               if not QCampi.Active then QCampi.Open else begin QCampi.Close; QCampi.Open; end;
               QCampi.Locate('DescCampo', xCampo, []);
          end;
          // OPERATORE
          xOperatore := TLineeDescOperatore.Value;
          if xOperatore <> '' then begin
               if not QOperatori.Active then QOperatori.Open else begin QOperatori.Close; QOperatori.Open; end;
               QOperatori.Locate('DescOperatore', xOperatore, []);
          end;
          // VALORE
          xValore := TLineeValore.Value;
     end;
end;

procedure TSelPersNewForm.DBGrid3DescOperatoreButtonClick(Sender: TObject; AbsoluteIndex: Integer);
begin
     PredisponiWizard;
     // setta variabile "pagina attiva"
     SelCriterioWizardForm.xPaginaAttiva := 2;
     SelCriterioWizardForm.ShowModal;
     if SelCriterioWizardForm.ModalResult = mrOK then begin
          SalvaDaWizard;
          RicreaStringaSQL;
          TLinee.Post;
     end;
end;

procedure TSelPersNewForm.CBIRFileSystemClick(Sender: TObject);
begin
     CBIRaltreRegole.Visible := CBIRFileSystem.Checked;
end;

procedure TSelPersNewForm.DsLineeStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsLinee.State in [dsEdit, dsInsert];
     BCriterioNew.Enabled := not b;
     BCriterioMod.Enabled := not b;
     BCriterioDel.Enabled := not b;
     BCriterioDelTutti.Enabled := not b;
     BLineeOK.Enabled := b;
     BLineeCan.Enabled := b;
end;

procedure TSelPersNewForm.BLineeOKClick(Sender: TObject);
begin
     TLinee.Post;
     xTipoQuery := 1;
     EseguiQueryRic;
end;

procedure TSelPersNewForm.BLineeCanClick(Sender: TObject);
begin
     TLinee.Cancel;
end;

procedure TSelPersNewForm.DBGrid3CustomDrawCell(Sender: TObject;
     ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
     AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
     var AText: string; var AColor: TColor; AFont: TFont;
     var AAlignment: TAlignment; var ADone: Boolean);
var xID: integer;
begin
     if AColumn = DBGrid3Valore then begin
          if ANode.Strings[DBGrid3QueryLookup.Index] <> '' then begin
               xID := ANode.Values[DBGrid3ID.index];
               Q.Close;
               Q.SQL.text := ANode.Strings[DBGrid3QueryLookup.Index];
               Q.ParamByName['xID'] := StrToIntDef(ANode.Strings[DBGrid3Valore.Index], 0);
               Q.Open;
               AText := Q.FieldByName('Dicitura').asString;
          end;
     end;
end;

procedure TSelPersNewForm.DBGrid3Editing(Sender: TObject;
     Node: TdxTreeListNode; var Allow: Boolean);
begin
     //Allow:=Node.Strings[DBGrid3QueryLookup.Index]='';
end;

procedure TSelPersNewForm.DBGrid3ValoreEditButtonClick(Sender: TObject);
begin
     PredisponiWizard;
     // setta variabile "pagina attiva"
     SelCriterioWizardForm.xPaginaAttiva := 3;
     SelCriterioWizardForm.ShowModal;
     if SelCriterioWizardForm.ModalResult = mrOK then begin
          SalvaDaWizard;
          TLinee.Post;
     end;
end;

procedure TSelPersNewForm.BCriterioNewClick(Sender: TObject);
var K: INTEGER;
begin
     SelCriterioWizardForm.xPaginaAttiva := 0;
     Panel2.Caption := '  Criteri impostati';
     with SelCriterioWizardForm do begin
          xTabella := '';
          xCampo := '';
          xOperatore := '';
          xValore := '';
     end;
     if not SelCriterioWizardForm.QTabelle.Active then SelCriterioWizardForm.QTabelle.Open;
     SelCriterioWizardForm.ShowModal;
     {if SelCriterioWizardForm.ModalResult = mrOK then begin
          TLinee.Insert;
          TLineeIDAnagrafica.Value := MainForm.xIDUtenteAttuale;
          SalvaDaWizard;
          TLinee.Post;
     end;    }
     if SelCriterioWizardForm.ModalResult = mrOK then begin
          {TLinee.Insert;
          TLineeIDAnagrafica.Value := MainForm.xIDUtenteAttuale;
          if (SelCriterioWizardForm.PanGrid.Visible = true) and (SelCriterioWizardForm.dxDBgrid1.SelectedCount > 1) then begin
               for k := 0 to SelCriterioWizardForm.dxDBGrid1.SelectedCount - 1 do begin
                    SalvaDaWizard(0, k);
                    SelCriterioWizardForm.QTabelle.Next;
               end;
          end else begin
               TLinee.Insert;
               TLineeIDAnagrafica.Value := MainForm.xIDUtenteAttuale;
               SalvaDaWizard;
               TLinee.Post;
          end; }
          //TLinee.Post;
          if SelCriterioWizardForm.PanGrid.Visible then begin
               for k := 0 to SelCriterioWizardForm.dxDBGrid1.SelectedCount - 1 do begin
                    SelCriterioWizardForm.QTable.BookMark := SelCriterioWizardForm.dxDBGrid1.SelectedRows[k];
                    TLinee.Insert;
                    TLineeIDAnagrafica.Value := MainForm.xIDUtenteAttuale;
                    SalvaDaWizard;
                    TLinee.Post;

               end;
          end else begin
               TLinee.Insert;
               TLineeIDAnagrafica.Value := MainForm.xIDUtenteAttuale;
               SalvaDaWizard;
               TLinee.Post;
          end;
     end;
     // TLinee.Close;
      //TLinee.Open;

     xTipoQuery := 1;
     EseguiQueryRic;
end;

procedure TSelPersNewForm.BCriterioModClick(Sender: TObject);
begin
     if TLinee.IsEmpty then exit;
     PredisponiWizard;
     // setta variabile "pagina attiva"
     SelCriterioWizardForm.xPaginaAttiva := 0;
     SelCriterioWizardForm.ShowModal;
     if SelCriterioWizardForm.ModalResult = mrOK then begin
          SalvaDaWizard;
          TLinee.Post;
     end;
     xTipoQuery := 1;
     EseguiQueryRic;
end;

procedure TSelPersNewForm.BCriterioDelClick(Sender: TObject);
begin
     if TLinee.IsEmpty then exit;
     // prosegui timing
     if xChiamante = 1 then SelPersForm.RiprendiAttivita;
     dxDBGrid1.Visible := False;
     LTot.Visible := False;
     TLinee.Delete;
     TLinee.Close;
     TLinee.Open;

     xTipoQuery := 1;
     EseguiQueryRic;
end;

procedure TSelPersNewForm.BCriterioDelTuttiClick(Sender: TObject);
begin
     if TLinee.IsEmpty then exit;
     // prosegui timing
     if xChiamante = 1 then SelPersForm.RiprendiAttivita;
     dxDBGrid1.Visible := False;
     LTot.Visible := False;
     Query1.Close;
     Query1.SQl.Clear;
     Query1.SQl.add('delete from UserLineeQuery where IDAnagrafica=' + IntToStr(MainForm.xIDUtenteAttuale));
     Query1.ExecSQL;
     TLinee.Close;
     TLinee.Open;
end;

procedure TSelPersNewForm.DBGrid2CustomDrawCell(Sender: TObject;
     ACanvas: TCanvas; ARect: TRect; ANode: TdxTreeListNode;
     AColumn: TdxTreeListColumn; ASelected, AFocused, ANewItemRow: Boolean;
     var AText: string; var AColor: TColor; AFont: TFont;
     var AAlignment: TAlignment; var ADone: Boolean);
var xID: integer;
begin
     if AColumn = DBGrid2Valore then begin
          if ANode.Strings[DBGrid2QueryLookup.Index] <> '' then begin
               xID := ANode.Values[DBGrid2ID.index];
               Q.Close;
               Q.SQL.text := ANode.Strings[DBGrid2QueryLookup.Index];
               Q.ParamByName['xID'] := StrToIntDef(ANode.Strings[DBGrid2Valore.Index], 0);
               Q.Open;
               AText := Q.FieldByName('Dicitura').asString;
          end;
     end;
end;

procedure TSelPersNewForm.DBGrid2ValoreEditButtonClick(Sender: TObject);
begin
     PredisponiWizard;
     // setta variabile "pagina attiva"
     SelCriterioWizardForm.xPaginaAttiva := 3;
     SelCriterioWizardForm.ShowModal;
     if SelCriterioWizardForm.ModalResult = mrOK then begin
          SalvaDaWizard;
          TLinee.Post;
     end;
end;

procedure TSelPersNewForm.BInfoColloquioClick(Sender: TObject);
begin
     InfoColloquioForm := TInfoColloquioForm.create(self);
     InfoColloquioForm.xIDAnagrafica := QRes1ID.Value;
     // esperienza lavorativa
     with InfoColloquioForm do begin
          // Apre i valori retributivi con l'ID dell'esp.lav. attuale
          QEspLavAttuale.ParamByName['xIDAnag'] := QRes1ID.value;
          QEspLavAttuale.Open;
          // se non c'� esp.lav.attuale ?
          if QEspLavAttuale.IsEmpty then begin
               MessageDlg('Nessuna esperienza lavorativa attuale. Impossibile aprire la maschera', mtError, [mbOK], 0);
          end else ShowModal;
     end;
     InfoColloquioForm.Free;
end;

procedure TSelPersNewForm.CaricaRicSemplice;
begin
     Q2.Close;
     Q2.SQL.text := 'select DescTabella,Tabella,Campo,DescCampo,DescOperatore,DescCompleta,TabQueryOperatori.ID IDTabQueryOp ' +
          'from TabQuery,TabQueryOperatori where TabQuery.ID=TabQueryOperatori.IDTabQuery ' +
          'and DescCompleta is not null and TabQueryOperatori.ID not in  ' +
          ' (select distinct IDTabQueryOp from UserLineeQuery where IDTabQueryOp is not null  ' +
          '  and IDAnagrafica=' + IntToStr(MainForm.xIDUtenteAttuale) + ')';
     Q2.Open;
     while not Q2.EOF do begin
          with SelCriterioWizardForm do begin
               TLinee.Insert;
               TLineeIDAnagrafica.Value := MainForm.xIDUtenteAttuale;
               TLineeDescTabella.Value := Q2.FieldByName('DescTabella').asString;
               TLineeTabella.Value := Q2.FieldByName('Tabella').asString;
               TLineeDescCampo.Value := Q2.FieldByName('DescCampo').asString;
               TLineeDescOperatore.Value := Q2.FieldByName('DescOperatore').asString;
               TLineeDescCompleta.Value := Q2.FieldByName('DescCompleta').asString;
               TLineeIDTabQueryOp.Value := Q2.FieldByName('IDTabQueryOp').asInteger;
               TLineeOpSucc.Value := 'and';
               TLineeOpNext.Value := 'e';
               TLinee.Post;
          end;
          Q2.Next;
     end;
     Q2.Close;
end;

procedure TSelPersNewForm.PCTipoRicChange(Sender: TObject);
begin
     if (PCTipoRic.ActivePageIndex = 0) and (xTipoRic = 0) then
          CaricaRicSemplice;
     if (PCTipoRic.ActivePageIndex = 0) or (PCTipoRic.ActivePageIndex = 1) then
     begin
          PCTipoRic.Height := 193;
          button1.visible := false;
          BitBtn2.visible := true;
          BAggiungiRic_old.visible := true;
          BRiepCand.visible := true;
          BInfoColloquio.visible := true;
          BitBtn10.visible := true;
          BCurriculum.visible := true;
          BitBtn8.visible := true;
          BitBtn9.visible := true;
          panel5.Visible := true;
     end
     else
     begin
          PCTipoRic.Height := panric.Height - 16;
          QMotoriRicerca.Parameters[0].value := main.MainForm.xIDUtenteAttuale;
          QMotoriRicerca.Open;
          Button1.visible := true;
          BitBtn2.visible := false;
          BAggiungiRic_old.visible := false;
          BRiepCand.visible := false;
          BInfoColloquio.visible := false;
          BitBtn10.visible := false;
          BCurriculum.visible := false;
          BitBtn8.visible := false;
          BitBtn9.visible := false;
          panel5.Visible := false;
     end;
end;

procedure TSelPersNewForm.Aggiungiinricerca1Click(Sender: TObject);
var i: integer;
     xMiniVal, xRic: string;
     xVai: boolean;
     SavePlace: TBookmark;
     xIDEvento, xIDClienteBlocco, xIDLocate: integer;
     xLivProtez, xDicMess, xPassword, xUtenteResp, xClienteBlocco: string;
     QLocal: TADOLinkedQuery;
begin

     if not Mainform.CheckProfile('210') then Exit;

     if pos('INTERMEDIA', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) > 0 then begin
          DataRicerche.QCkRegoleClienti.Close;
          DataRicerche.QCkRegoleClienti.SQL.Text := 'select c.id idcliente,el.attuale,el.idanagrafica,el.aziendanome,c.descrizione,c.idregolacliente,trc.descrizione regola, trc.flag FlagRegola ' +
               ' from esperienzelavorative el' +
               ' join ebc_clienti c on c.id=el.idazienda join tabregolecliente trc on trc.id=c.idregolacliente' +
               ' where el.idanagrafica=' + QRes1.FieldByname('ID').asString + ' and el.attuale=1';
          DataRicerche.QCkRegoleClienti.Open;
          if DataRicerche.QCkRegoleClienti.RecordCount > 0 then
               if DataRicerche.QCkRegoleClienti.FieldByName('FlagRegola').asboolean then
                    if MessageDlg('Limite per questo soggetto: ' + DataRicerche.QCkRegoleClienti.fieldByName('Regola').asstring + #10#13 + 'Vuoi Proseguire ?', mtWarning, [mbYes, mbNo], 0) = mrNO then
                         exit;
     end;


     if not OkIDAnag(QRes1.FieldByName('ID').AsInteger) then exit;
     if QRes1.FieldByName('IDTipoStato').AsInteger = 3 then
          if MessageDlg('Il candidato risulta INSERITO in azienda. Continuare?', mtWarning, [mbYes, mbNo], 0) = mrNo then exit;
     xMiniVal := '';
     if not InputQuery('Inserimento nella ricerca', 'Valutazione (1 lettera):', xMiniVal) then exit;
     Q.Close;
     Q.SQL.Text := 'select ID from EBC_CandidatiRicerche where IDRicerca=' + Dataricerche.TRicerchePend.FieldByName('ID').asString + ' and IDAnagrafica=' + QRes1.FieldByname('ID').asString;
     Q.Open;
     if Q.IsEmpty then begin
          xVai := True;
          // controllo se � in selezione
          if QRes1.FieldByName('IDTipoStato').AsInteger = 1 then begin
               if MessageDlg('Il soggetto risulta IN SELEZIONE: ' + chr(13) + xRic + chr(13) + 'PROCEDERE COMUNQUE ?', mtWarning, [mbYes, mbNo], 0) = mrNo then xVai := False;
          end;
          // controllo se � gi� associato ad un'altra ricerca per la STESSA azienda
          Q.Close; Q.SQL.clear;
          Q.SQl.Add('select EBC_Ricerche.Progressivo,EBC_StatiRic.StatoRic from EBC_CandidatiRicerche,EBC_Ricerche,EBC_StatiRic ');
          Q.SQl.Add('where EBC_CandidatiRicerche.IDRicerca=EBC_Ricerche.ID and EBC_Ricerche.IDStatoRic=EBC_StatiRic.ID');
          Q.SQl.Add('and EBC_CandidatiRicerche.IDAnagrafica=:xIDAnag: and EBC_Ricerche.IDCliente=:xIDCliente:');
          //          Q.Prepare;
          Q.ParamByName['xIDAnag'] := QRes1.FieldByName('ID').AsInteger;
          Q.ParamByName['xIDCliente'] := DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger;
          Q.Open;
          if not Q.IsEmpty then begin
               xRic := '';
               while not Q.EOF do begin xRic := xRic + 'N�' + Q.FieldByName('Progressivo').asString + ' (' + Q.FieldByName('StatoRic').asString + ') '; Q.Next; end;
               if MessageDlg('Il soggetto � associato gi� alle seguenti ricerche per lo stesso cliente: ' + chr(13) + xRic + chr(13) + 'PROCEDERE COMUNQUE ?', mtWarning, [mbYes, mbNo], 0) = mrNo then xVai := False;
          end;
          // controllo incompatibilit� (per questa azienda e per le aziende a questa correlate)
          if IncompAnagCli(QRes1.FieldByName('ID').AsInteger, DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger) then
               if MessageDlg('Il soggetto risulta INCOMPATIBILE con questo cliente o con un cliente associato' + chr(13) +
                    'PROCEDERE COMUNQUE ?', mtWarning, [mbYes, mbNo], 0) = mrNo then xVai := False;
          // controllo appartenenza CV
          Q.Close;
          Q.SQL.Text := 'select IDProprietaCV from Anagrafica where ID=' + QRes1.FieldByName('ID').asString;
          Q.Open;
          if (not ((Q.FieldByName('IDProprietaCV').asString = '') or (Q.FieldByName('IDProprietaCV').asInteger = 0))) and
               (Q.FieldByName('IDProprietaCV').asInteger <> DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger) then
               if MessageDlg('ATTENZIONE: il CV risulta di propriet� di un altro cliente.' + chr(13) +
                    'Vuoi proseguire lo stesso ?', mtWarning, [mbYes, mbNo], 0) = mrNo then
                    xVai := False;
          // controllo blocco livello 1 o 2
          xIDClienteBlocco := CheckAnagInseritoBlocco1(QRes1.FieldByName('ID').AsInteger);
          if xIDClienteBlocco > 0 then begin
               xLivProtez := copy(IntToStr(xIDClienteBlocco), 1, 1);
               if xLivProtez = '1' then begin
                    xIDClienteBlocco := xIDClienteBlocco - 10000;
                    xClienteBlocco := GetDescCliente(xIDClienteBlocco);
                    xDicMess := 'Stai inserendo in commessa un candidato appartenente ad una azienda (' + xClienteBlocco + ') con blocco di livello 1, ';
               end else begin
                    xIDClienteBlocco := xIDClienteBlocco - 20000;
                    xClienteBlocco := GetDescCliente(xIDClienteBlocco);
                    xDicMess := 'Stai inserendo in commessa un candidato appartenente ad una azienda ATTIVA (' + xClienteBlocco + '), ';
               end;
               if MessageDlg('ATTENZIONE!! PROTEZIONE CLIENTI LIVELLO ' + xLivProtez + chr(13) + xDicMess +
                    'protetta dalle normative interne, se sei autorizzato premi SI, altrimenti premi NO. ' +
                    'Se si prosegue, l''operazione verr� registrata nello storico del soggetto come forzatura.', mtWarning, [mbYes, mbNo], 0) = mrNo then xVai := False
               else begin
                    if pos('INTERMEDIA', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) = 0 then begin
                         // pwd
                         xUtenteResp := GetDescUtenteResp(MainForm.xIDUtenteAttuale);
                         if not InputQuery('Forzatura blocco livello ' + xLivProtez, 'Password di ' + xUtenteResp, xPassword) then exit;
                         if not CheckPassword(xPassword, GetPwdUtenteResp(MainForm.xIDUtenteAttuale)) then begin
                              MessageDlg('Password errata', mtError, [mbOK], 0);
                              xVai := False;
                         end;
                    end;
                    if xVai then begin
                         // promemoria al resp.
                         QLocal := CreateQueryFromString('insert into Promemoria (IDUtente,IDUtenteDa,DataIns,Testo,Evaso,DataDaLeggere) ' +
                              'values (:xIDUtente:,:xIDUtenteDa:,:xDataIns:,:xTesto:,:xEvaso:,:xDataDaLeggere:)');
                         QLocal.ParamByName['xIDUtente'] := GetIDUtenteResp(MainForm.xIDUtenteAttuale);
                         QLocal.ParamByName['xIDUtenteDa'] := MainForm.xIDUtenteAttuale;
                         QLocal.ParamByName['xDataIns'] := Date;
                         QLocal.ParamByName['xTesto'] := 'forzatura livello ' + xLivProtez + ' per CV n� ' + QRes1.FieldByName('CVNumero').AsString;
                         QLocal.ParamByName['xEvaso'] := 0;
                         QLocal.ParamByName['xDataDaLeggere'] := Date;
                         QLocal.ExecSQL;
                         // registra nello storico soggetto
                         xIDEvento := GetIDEvento('forzatura Blocco livello ' + xLivProtez);
                         if xIDEvento > 0 then begin
                              with Data.Q1 do begin
                                   close;
                                   SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
                                        'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xIDCliente:)';
                                   ParamByName['xIDAnagrafica'] := QRes1.FieldByName('ID').AsInteger;
                                   ParamByName['xIDEvento'] := xIDevento;
                                   ParamByName['xDataEvento'] := Date;
                                   ParamByName['xAnnotazioni'] := 'cliente: ' + xClienteBlocco;
                                   ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
                                   ParamByName['xIDUtente'] := MainForm.xIDUtenteAttuale;
                                   ParamByName['xIDCliente'] := xIDClienteBlocco;
                                   ExecSQL;
                              end;
                         end;
                    end;
               end;
          end;

          if xVai then begin
               Data.DB.BeginTrans;
               try
                    if Q.Active then Q.Close;
                    Q.SQL.text := 'insert into EBC_CandidatiRicerche (IDRicerca,IDAnagrafica,Escluso,Stato,DataIns,Minival) ' +
                         'values (:xIDRicerca:,:xIDAnagrafica:,:xEscluso:,:xStato:,:xDataIns:,:xMinival:)';
                    Q.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
                    Q.ParamByName['xIDAnagrafica'] := QRes1.FieldByName('ID').AsInteger;
                    Q.ParamByName['xEscluso'] := 0;
                    Q.ParamByName['xStato'] := 'inserito da ricerca nomi';
                    Q.ParamByName['xDataIns'] := Date;
                    Q.ParamByName['xMiniVal'] := xMiniVal;
                    Q.ExecSQL;
                    // aggiornamento stato anagrafica
                    if QRes1.FieldByName('IDStato').AsInteger <> 27 then begin
                         if Q.Active then Q.Close;
                         Q.SQL.text := 'update Anagrafica set IDStato=27,IDTipoStato=1 where ID=' + QRes1.FieldByName('ID').asString;
                         Q.ExecSQL;
                    end;
                    // aggiornamento storico
                    if Q.Active then Q.Close;
                    Q.SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
                         'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xIDCliente:)';
                    Q.ParamByName['xIDAnagrafica'] := QRes1.FieldByName('ID').AsInteger;
                    Q.ParamByName['xIDEvento'] := 19;
                    Q.ParamByName['xDataEvento'] := date;
                    Q.ParamByName['xAnnotazioni'] := 'ric.n� ' + TrimRight(DataRicerche.TRicerchePend.FieldByName('Progressivo').AsString) + ' (' + TrimRight(DataRicerche.TRicerchePend.FieldByName('Cliente').AsString) + ')';
                    Q.ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
                    Q.ParamByName['xIDUtente'] := DataRicerche.TRicerchePend.FieldByName('IDUtente').AsInteger;
                    Q.ParamByName['xIDCliente'] := DataRicerche.TRicerchePend.FieldByName('IDCliente').AsInteger;
                    Q.ExecSQL;
                    Data.DB.CommitTrans;
                    //DataRicerche.QCandRic.Next;
                    //SavePlace:=DataRicerche.QCandRic.GetBookmark;
                    DataRicerche.QCandRic.Close;
                    DataRicerche.QCandRic.Open;
                    //DataRicerche.QCandRic.GotoBookmark(SavePlace);
                    //DataRicerche.QCandRic.FreeBookmark(SavePlace);
              // metti anche questo in quelli da escludere per le prossime ricerche
                    for i := 1 to 500 do if xDaEscludere[i] = 0 then break;
                    xDaEscludere[i] := QRes1.FieldByName('ID').AsInteger;
                    //recuper ID per locate su record sucessivo
                    QRes1.Next;
                    xIDLocate := Qres1.FieldByname('ID').AsInteger;

                    xTipoQuery := 0;
                    EseguiQueryRic;

                    Qres1.Locate('ID', xIDLocate, []);
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('Errore sul database:  inserimento non avvenuto', mtError, [mbOK], 0);
               end;
          end;
     end;
     // prosegui timing
     if xChiamante = 1 then SelPersForm.RiprendiAttivita;
end;

procedure TSelPersNewForm.Aggiungiadunaricerca1Click(Sender: TObject);
var i: integer;
     xMiniVal, xRic: string;
     xVai: boolean;
     SavePlace: TBookmark;
     xIDEvento, xIDClienteBlocco, xIDLocate: integer;
     xLivProtez, xDicMess, xPassword, xUtenteResp, xClienteBlocco: string;
     QLocal: TADOLinkedQuery;
begin

     if not Mainform.CheckProfile('210') then Exit;
     if not OkIDAnag(QRes1.FieldByName('ID').AsInteger) then exit;

     if pos('INTERMEDIA', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) > 0 then begin
          DataRicerche.QCkRegoleClienti.Close;
          DataRicerche.QCkRegoleClienti.SQL.Text := 'select c.id idcliente,el.attuale,el.idanagrafica,el.aziendanome,c.descrizione,c.idregolacliente,trc.descrizione regola, trc.flag FlagRegola ' +
               ' from esperienzelavorative el' +
               ' join ebc_clienti c on c.id=el.idazienda join tabregolecliente trc on trc.id=c.idregolacliente' +
               ' where el.idanagrafica=' + QRes1.FieldByname('ID').asString + ' and el.attuale=1';
          DataRicerche.QCkRegoleClienti.Open;
          if DataRicerche.QCkRegoleClienti.RecordCount > 0 then
               if DataRicerche.QCkRegoleClienti.FieldByName('FlagRegola').asboolean then
                    if MessageDlg('Limite per questo soggetto: ' + DataRicerche.QCkRegoleClienti.fieldByName('Regola').asstring + #10#13 + 'Vuoi Proseguire ?', mtWarning, [mbYes, mbNo], 0) = mrNO then
                         exit;
     end;

     if QRes1.FieldByName('IDTipoStato').AsInteger = 3 then
          if MessageDlg('Il candidato risulta INSERITO in azienda. Continuare?', mtWarning, [mbYes, mbNo], 0) = mrNo then exit;
     xMiniVal := '';

     ElencoRicPendForm := TElencoRicPendForm.create(self);
     ElencoRicpendform.dxDBGrid1.Filter.Add(ElencoRicpendform.dxDBGrid1Stato, 'attiva', 'attiva');
     ElencoRicPendForm.ShowModal;
     DataRicerche.TRicAnagIDX.Open;
     if ElencoRicPendForm.ModalResult = mrOK then begin
          xVai := true;
          // controllo se � gi� associato ad un'altra ricerca per la STESSA azienda
          Data.Q1.Close;
          Data.Q1.SQL.clear;
          Data.Q1.SQl.Add('select EBC_Ricerche.Progressivo,EBC_StatiRic.StatoRic from EBC_CandidatiRicerche,EBC_Ricerche,EBC_StatiRic ');
          Data.Q1.SQl.Add('where EBC_CandidatiRicerche.IDRicerca=EBC_Ricerche.ID and EBC_Ricerche.IDStatoRic=EBC_StatiRic.ID');
          //          Data.Q1.SQl.Add('and EBC_CandidatiRicerche.IDAnagrafica=:xIDAnag and EBC_Ricerche.IDCliente=:xIDCliente');
          Data.Q1.SQl.Add('and EBC_CandidatiRicerche.IDAnagrafica=:xIDAnag: and EBC_Ricerche.IDCliente=:xIDCliente:');
          //          Data.Q1.Prepare;
          //          Data.Q1.ParamByName('xIDAnag').asInteger:=data.TAnagraficaID.Value;
          //          Data.Q1.ParamByName('xIDCliente').asInteger:=ElencoRicPendForm.QRicAttiveIDCliente.Value;
          Data.Q1.ParamByName['xIDAnag'] := QRes1ID.Value; //data.TAnagrafica.FieldByName('ID').AsINteger;
          Data.Q1.ParamByName['xIDCliente'] := ElencoRicPendForm.QRicAttive.FieldByName('IDCliente').AsInteger;
          Data.Q1.Open;
          if not Data.Q1.IsEmpty then begin
               xRic := '';
               while not Data.Q1.EOF do begin xRic := xRic + 'N�' + Data.Q1.FieldByName('Progressivo').asString + ' (' + Data.Q1.FieldByName('StatoRic').asString + ') '; Data.Q1.Next; end;
               if MessageDlg('Il soggetto � associato gi� alle seguenti ricerche per lo stesso cliente: ' + chr(13) + xRic + chr(13) + 'PROCEDERE COMUNQUE ?', mtWarning, [mbYes, mbNo], 0) = mrNo then xVai := False;
          end;
          Data.Q1.Close;

          // controllo incompatibilit�
      //          if IncompAnagCli(Data.TAnagraficaID.Value,ElencoRicPendForm.QRicAttiveIDCliente.Value) then
          if IncompAnagCli(QRes1ID.Value, ElencoRicPendForm.QRicAttive.FieldByname('IDCliente').AsInteger) then
               if MessageDlg('Il soggetto risulta INCOMPATIBILE con questo cliente o con un cliente associato' + chr(13) +
                    'PROCEDERE COMUNQUE ?', mtWarning, [mbYes, mbNo], 0) = mrNo then begin
                    DataRicerche.TRicAnagIDX.Close;
                    ElencoRicPendForm.Free;
                    exit;
               end;
          // controllo blocco livello 1 o 2
      //          xIDClienteBlocco:=CheckAnagInseritoBlocco1(Data.TAnagraficaID.Value);
          xIDClienteBlocco := CheckAnagInseritoBlocco1(QRes1ID.Value);
          if xIDClienteBlocco > 0 then begin
               xLivProtez := copy(IntToStr(xIDClienteBlocco), 1, 1);
               if xLivProtez = '1' then begin
                    xIDClienteBlocco := xIDClienteBlocco - 10000;
                    xClienteBlocco := GetDescCliente(xIDClienteBlocco);
                    xDicMess := 'Stai inserendo in commessa un candidato appartenente ad una azienda (' + xClienteBlocco + ') con blocco di livello 1, ';
               end else begin
                    xIDClienteBlocco := xIDClienteBlocco - 20000;
                    xClienteBlocco := GetDescCliente(xIDClienteBlocco);
                    xDicMess := 'Stai inserendo in commessa un candidato appartenente ad una azienda ATTIVA (' + xClienteBlocco + '), ';
               end;
               if MessageDlg('ATTENZIONE!! PROTEZIONE CLIENTI LIVELLO ' + xLivProtez + chr(13) + xDicMess +
                    'protetta dalle normative interne, se sei autorizzato premi SI, altrimenti premi NO. ' +
                    'Se si prosegue, l''operazione verr� registrata nello storico del soggetto come forzatura.', mtWarning, [mbYes, mbNo], 0) = mrNo then xVai := False
               else begin
                    if pos('INTERMEDIA', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) = 0 then begin
                         // pwd
                         xUtenteResp := GetDescUtenteResp(MainForm.xIDUtenteAttuale);
                         if not InputQuery('Forzatura blocco livello ' + xLivProtez, 'Password di ' + xUtenteResp, xPassword) then exit;
                         if not CheckPassword(xPassword, GetPwdUtenteResp(MainForm.xIDUtenteAttuale)) then begin
                              MessageDlg('Password errata', mtError, [mbOK], 0);
                              xVai := False;
                         end;
                    end;
                    if xVai then begin
                         // promemoria al resp.
               {                         QLocal:=CreateQueryFromString('insert into Promemoria (IDUtente,IDUtenteDa,DataIns,Testo,Evaso,DataDaLeggere) '+
                              'values (:xIDUtente,:xIDUtenteDa,:xDataIns,:xTesto,:xEvaso,:xDataDaLeggere)');
                         QLocal.ParamByName('xIDUtente').asInteger:=GetIDUtenteResp(MainForm.xIDUtenteAttuale);
                         QLocal.ParamByName('xIDUtenteDa').asInteger:=MainForm.xIDUtenteAttuale;
                         QLocal.ParamByName('xDataIns').asDateTime:=Date;
                         QLocal.ParamByName('xTesto').asString:='forzatura livello '+xLivProtez+' per CV n� '+Data.TAnagraficaCVNumero.AsString;
                         QLocal.ParamByName('xEvaso').asBoolean:=False;
                         QLocal.ParamByName('xDataDaLeggere').asDateTime:=Date;}
                         QLocal := CreateQueryFromString('insert into Promemoria (IDUtente,IDUtenteDa,DataIns,Testo,Evaso,DataDaLeggere) ' +
                              'values (:xIDUtente:,:xIDUtenteDa:,:xDataIns:,:xTesto:,:xEvaso:,:xDataDaLeggere:)');
                         QLocal.ParamByName['xIDUtente'] := GetIDUtenteResp(MainForm.xIDUtenteAttuale);
                         QLocal.ParamByName['xIDUtenteDa'] := MainForm.xIDUtenteAttuale;
                         QLocal.ParamByName['xDataIns'] := Date;
                         QLocal.ParamByName['xTesto'] := 'forzatura livello ' + xLivProtez + ' per CV n� ' + QRes1CVNumero.AsString;
                         QLocal.ParamByName['xEvaso'] := 0;
                         QLocal.ParamByName['xDataDaLeggere'] := Date;

                         QLocal.ExecSQL;
                         // registra nello storico soggetto
                         xIDEvento := GetIDEvento('forzatura Blocco livello ' + xLivProtez);
                         if xIDEvento > 0 then begin
                              with Data.Q1 do begin
                                   close;
                                   SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
                                        'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xIDCliente:)';
                                   ParamByName['xIDAnagrafica'] := QRes1ID.Value;
                                   ParamByName['xIDEvento'] := xIDevento;
                                   ParamByName['xDataEvento'] := Date;
                                   ParamByName['xAnnotazioni'] := 'cliente: ' + xClienteBlocco;
                                   ParamByName['xIDRicerca'] := DataRicerche.TRicerchePend.FieldByName('ID').AsInteger;
                                   ParamByName['xIDUtente'] := MainForm.xIDUtenteAttuale;
                                   ParamByName['xIDCliente'] := xIDClienteBlocco;

                                   ExecSQL;
                              end;
                         end;
                    end;
               end;
          end;
          if xvai then begin
               //               if not DataRicerche.TRicAnagIDX.FindKey([ElencoRicPendForm.QRicAttiveID.Value,Data.TAnagraficaID.Value]) then
               if not DataRicerche.TRicAnagIDX.FindKeyADO(VarArrayOF([ElencoRicPendForm.QRicAttive.FieldByName('ID').AsINteger, QRes1ID.Value])) then
                    Data.DB.BeginTrans;
               try
                    Data.Q1.Close;
                    {                    Data.Q1.SQL.text:='insert into EBC_CandidatiRicerche (IDRicerca,IDAnagrafica,Escluso,Stato,DataIns) '+
                                             'values (:xIDRicerca,:xIDAnagrafica,:xEscluso,:xStato,:xDataIns)';
                                        Data.Q1.ParambyName('xIDRicerca').asInteger:=ElencoRicPendForm.QRicAttiveID.Value;
                                        Data.Q1.ParambyName('xIDAnagrafica').asInteger:=Data.TAnagraficaID.Value;
                                        Data.Q1.ParambyName('xEscluso').asBoolean:=False;
                                        Data.Q1.ParambyName('xStato').asString:='inserito direttam. in data '+DateToStr(Date);
                                        Data.Q1.ParambyName('xDataIns').asDateTime:=Date;}
                    Data.Q1.SQL.text := 'insert into EBC_CandidatiRicerche (IDRicerca,IDAnagrafica,Escluso,Stato,DataIns) ' +
                         'values (:xIDRicerca:,:xIDAnagrafica:,:xEscluso:,:xStato:,:xDataIns:)';
                    Data.Q1.ParambyName['xIDRicerca'] := ElencoRicPendForm.QRicAttive.FieldByName('ID').AsInteger;
                    Data.Q1.ParambyName['xIDAnagrafica'] := QRes1ID.Value;
                    Data.Q1.ParambyName['xEscluso'] := 0;
                    Data.Q1.ParambyName['xStato'] := 'inserito direttam. in data ' + DateToStr(Date);
                    Data.Q1.ParambyName['xDataIns'] := Date;

                    Data.Q1.ExecSQL;
                    // storico
                    Data.Q1.SQL.text := 'insert into Storico (IDAnagrafica,IDEvento,DataEvento,Annotazioni,IDRicerca,IDUtente,IDCliente) ' +
                         'values (:xIDAnagrafica:,:xIDEvento:,:xDataEvento:,:xAnnotazioni:,:xIDRicerca:,:xIDUtente:,:xIDCliente:)';
                    Data.Q1.ParambyName['xIDAnagrafica'] := QRes1ID.Value;
                    Data.Q1.ParambyName['xIDEvento'] := 58;
                    Data.Q1.ParambyName['xDataEvento'] := Date;
                    Data.Q1.ParambyName['xAnnotazioni'] := copy('inserito nella ric.' + ElencoRicPendForm.QRicAttive.FieldByName('Progressivo').AsString + ' (' + ElencoRicPendForm.QRicAttive.FieldByName('Cliente').AsString + ')', 1, 256);
                    Data.Q1.ParambyName['xIDRicerca'] := ElencoRicPendForm.QRicAttive.FieldByname('ID').AsInteger;
                    Data.Q1.ParambyName['xIDUtente'] := MainForm.xIDUtenteAttuale;
                    Data.Q1.ParambyName['xIDCliente'] := ElencoRicPendForm.QRicAttive.FieldByName('IDCliente').AsInteger;

                    Data.Q1.ExecSQL;
                    // Anagrafica: cambio stato
            //                    Data.Q1.SQL.text:='update Anagrafica set IDTipoStato=1,IDStato=27 where ID='+Data.TAnagraficaID.asString;
                    Data.Q1.SQL.text := 'update Anagrafica set IDTipoStato=1,IDStato=27 where ID=' + QRes1ID.AsString;
                    Data.Q1.ExecSQL;
                    Data.DB.CommitTrans;
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE:  operazione non completata', mtError, [mbOK], 0);
               end;
          end;
     end;
     DataRicerche.TRicAnagIDX.Close;
     //Data2.QAnagRicerche.Close;
     //Data2.QAnagRicerche.Open;
     ElencoRicPendForm.Free;
     //[/TONI20020909\]FINE
   //end;
     // prosegui timing
     if xChiamante = 1 then SelPersForm.RiprendiAttivita;
end;

procedure TSelPersNewForm.EseguiQueryRic;
var xTab: array[1..5] of string;
     i, j, k, xIDEvento: integer;
     xS, xS1, xd, xLineeStringa, xStringaSQL: string;
     xApertaPar, xEspLav, xIRNote, xIRFileSystem: boolean;
     xQueryCount: TADOQuery;
begin
     // prosegui timing
     if xChiamante = 1 then SelPersForm.RiprendiAttivita;

     if DsLinee.State in [dsInsert, dsEdit] then begin
          TLinee.Post;
          TLinee.Close;
          TLinee.Open;
     end;
     TLinee.Last;
     if TLinee.FieldByName('OpSucc').AsString <> '' then begin
          TLinee.Edit;
          TLinee.FieldByName('OpNext').AsString := 'fine';
          TLinee.Post;
     end;
     TLinee.Close;
     TLinee.SQL.text := 'select * from UserLineeQuery where IDAnagrafica=:xIDAnag: and Valore is not null and Valore<>''''';
     TLinee.ParamByName['xIDAnag'] := MainForm.xIDUtenteAttuale;
     TLinee.Open;
     // filtro solo dove ci sono valori

     // tabelle implicate tranne Anagrafica
     QTabelleLista.Close;
     QTabelleLista.SQL.clear;
     QTabelleLista.SQL.add('select distinct Tabella from UserLineeQuery ');
     QTabelleLista.SQL.add('where Tabella<>''Anagrafica'' and Tabella<>''EBC_CandRic'' and IDAnagrafica=' + IntToStr(MAinForm.xIDUtenteAttuale));
     QTabelleLista.Open;
     QTabelleLista.First;
     for i := 1 to 5 do xTab[i] := '';
     i := 1;
     while not QTabelleLista.EOF do begin
          xTab[i] := TrimRight(QTabelleLista.FieldByName('Tabella').AsString);
          QTabelleLista.Next; inc(i);
     end;

     xEspLav := False;
     xS := 'from Anagrafica,AnagAltreInfo';
     k := 1;
     while k <= 5 do begin
          //if xTab[i]='EsperienzeLavorative' then xEspLav:=True;
          //if (xTab[i]<>'')and(xTab[i]<>'EsperienzeLavorative') then begin
          if (xTab[k] <> '') and (UpperCase(xTab[k]) <> 'ANAGALTREINFO') then
               xS := xs + ',' + xTab[k];
          if xTab[k] = 'AnagCampiPers' then
               xS := xS + ',CampiPers';
          inc(k);
     end;

     QRes1.Close;
     QRes1.SQL.Clear;

     QRes1.SQL.Add('select distinct Anagrafica.ID,Anagrafica.Cognome,Anagrafica.Nome,Anagrafica.CVNumero,Anagrafica.OldCVNumero,');
     QRes1.SQL.Add('                Anagrafica.TelUfficio,Anagrafica.Cellulare,CVInseritoInData,CVDataReale,CVDataAgg,RecapitiTelefonici,');
     QRes1.SQL.Add('                Anagrafica.DataNascita,Anagrafica.IDStato,Anagrafica.IDTipoStato,Anagrafica.DomicilioComune, Anagrafica.DomicilioProvincia,');
     QRes1.SQL.Add('                Anagrafica.Provincia, Anagrafica.Comune,');
     QRes1.SQL.Add('                DATEDIFF(day, EBC_CandidatiRicerche.DataIns, getdate()) ggUltimaDataIns,');
     QRes1.SQL.Add('                DATEDIFF(day, EBC_ContattiCandidati.Data, getdate()) ggDataUltimoContatto,');
     QRes1.SQL.Add('                EBC_CandidatiRicerche.DataImpegno DataProssimoColloquio,');
     QRes1.SQL.Add('                DATEDIFF(day, EBC_Colloqui.Data, getdate()) ggDataUltimoColloquio,');
     QRes1.SQL.Add('                DATEDIFF(day, Anagrafica.CVInseritoInData, getdate()) ggDataInsCV, NoCv,AnagAltreInfo.Retribuzione,EMail, AnagAltreInfo.Inquadramento, cast (anagfileD.IDAnagfile/anagfileD.IDAnagfile as bit) as FileCheck');
     // colonne visibili o meno a seconda del cliente
     if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').AsString, 1, 5)) = 'ERGON' then begin
          dxDBGrid1CVNumero.FieldName := 'OldCVNumero';
          dxDBGrid1CVNumero.Caption := 'Old n�CV';
          dxDBGrid1TipoStato.Visible := False;
          dxDBGrid1DomicilioComune.Visible := False;
     end;
     if (EIRNote.Text <> '') and (Data.Global.FieldByName('AnagFileDentroDB').AsBoolean) then
          QRes1.SQL.Add(xS + ',EBC_CandidatiRicerche,EBC_ContattiCandidati,EBC_Colloqui, (select distinct idanagrafica as IDAnagFile from anagfile) as anagfileD , AnagFile  ')
     else
          QRes1.SQL.Add(xS + ',EBC_CandidatiRicerche,EBC_ContattiCandidati,EBC_Colloqui, (select distinct idanagrafica as IDAnagFile from anagfile) as anagfileD  ');
     QRes1.SQL.Add('where Anagrafica.ID is not null and Anagrafica.ID=AnagAltreInfo.IDAnagrafica');
     QRes1.SQL.Add(' and anagfileD.IDAnagfile =* anagrafica.id ');
     if not xEspLav then begin
          //QRes1.SQL.Add('and Anagrafica.ID *= EsperienzeLavorative.IDAnagrafica');
          //QRes1.SQL.Add('and EsperienzeLavorative.ID = (select max(ID) from EsperienzeLavorative');
          //QRes1.SQL.Add('                               where Anagrafica.ID *= EsperienzeLavorative.IDAnagrafica)');
     end else begin
          // *** Questo codice non viene pi� considerato (vedi riga 458 e 459 commentate)
          QRes1.SQL.Add('and Anagrafica.ID = EsperienzeLavorative.IDAnagrafica');
          QRes1.SQL.Add('and EsperienzeLavorative.ID = (select max(ID) from EsperienzeLavorative');
          QRes1.SQL.Add('                               where Anagrafica.ID = EsperienzeLavorative.IDAnagrafica and');
          // oondizioni su EspLav
          TLinee.First;
          xLineeStringa := '';
          xApertaPar := False;
          while not TLinee.EOF do begin
               xLineeStringa := TrimRight(TLinee.FieldByName('Stringa').asString);
               if copy(TrimRight(TLinee.FieldByName('Stringa').AsString), 1, 13) = 'EsperienzeLav' then begin
                    TLinee.Next;
                    if TLinee.EOF then
                         // ultimo record -> senza AND alla fine
                         if xApertaPar then QRes1.SQL.Add(' ' + xLineeStringa + ')')
                         else QRes1.SQL.Add(' ' + xLineeStringa)
                    else begin
                         TLinee.Prior;
                         if TLinee.FieldByName('OpSucc').AsString = 'and' then
                              if not xApertaPar then
                                   QRes1.SQL.Add(' ' + xLineeStringa + ' and ')
                              else begin
                                   QRes1.SQL.Add(' ' + xLineeStringa + ') and ');
                                   xApertaPar := False;
                              end;
                         if TLinee.FieldbYnAME('OpSucc').AsString = 'or' then
                              if not xApertaPar then begin
                                   QRes1.SQL.Add(' (' + xLineeStringa + ' or ');
                                   xApertaPar := True;
                              end else begin
                                   QRes1.SQL.Add(' ' + xLineeStringa + ' or ');
                                   xApertaPar := True;
                              end;
                    end;
               end;
               TLinee.Next;
          end;
          // verifica ultime tre lettere sono "and" o "or" --> allora toglile
          xd := QRes1.SQL[QRes1.SQL.count - 1];
          if copy(xd, Length(xd) - 3, 3) = 'and' then
               QRes1.SQL[QRes1.SQL.count - 1] := copy(xd, 1, Length(xd) - 4);
          if copy(xd, Length(xd) - 2, 2) = 'or' then
               QRes1.SQL[QRes1.SQL.count - 1] := copy(xd, 1, Length(xd) - 3);
          QRes1.SQL.Add(')');
     end;

     // anzianit� CV
     if CBAnzCV.Checked then begin
          if cbanzianita.Text = 'Minore di mesi' then
               QRes1.SQL.Add('and (anagrafica.id in (select id from anagrafica where CVInseritoInData>=:xCVInseritoInData:) ' +
                    'or ' +
                    'anagrafica.id in (select id from anagrafica where CVdataagg>=:xCVInseritoInData:))')
          else
               QRes1.SQL.Add('and (anagrafica.id in (select id from anagrafica where CVInseritoInData<=:xCVInseritoInData:) ' +
                    'or ' +
                    'anagrafica.id in (select id from anagrafica where CVdataagg<=:xCVInseritoInData:))');
     end;
     // togliere gli esclusi (cio� gi� presenti in ricerca)
     if xChiamante = 1 then begin
          for j := 1 to DataRicerche.QCandRic.RecordCount do
               QRes1.SQL.Add('and Anagrafica.ID<>' + IntToStr(xDaEscludere[j]));
     end;

     xS1 := '';
     for i := 1 to 5 do begin
          if xTab[i] <> '' then
               if xTab[i] = '' then
                    xS1 := xS1 + ' and Anagrafica.ID=' + xTab[i] + '.IDAnagrafica and CampiPers.ID = AnagCampiPers.IDCampo'
               else xS1 := xS1 + ' and Anagrafica.ID=' + xTab[i] + '.IDAnagrafica';
     end;
     QRes1.SQL.Add(xS1);
     if TLinee.RecordCount > 0 then
          QRes1.SQL.Add('and');

     TLinee.First;
     xLineeStringa := '';
     xApertaPar := False;
     while not TLinee.EOF do begin
          xLineeStringa := TrimRight(TLinee.FieldByName('Stringa').asString);
          // per le LINGUE --> apposita clausola
          if Trim(TLinee.FieldByName('Tabella').AsString) = 'LingueConosciute' then begin
               // ## OLD ## xLineeStringa:='Anagrafica.ID in (select IDAnagrafica from LingueConosciute where lingua='''+copy(TLineeDescrizione.Value,9,Length(TLineeDescrizione.Value))+''')';
               xLineeStringa := 'Anagrafica.ID in (select IDAnagrafica from LingueConosciute where ' + TLineeStringa.Value;
               if TLineeLivLinguaParlato.asString <> '' then
                    xLineeStringa := xLineeStringa + ' and livello>=' + TrimRight(TLineeLivLinguaParlato.asString);
               if TLineeLivLinguaScritto.asString <> '' then
                    xLineeStringa := xLineeStringa + ' and livelloScritto>=' + TrimRight(TLineeLivLinguaScritto.asString);
               xLineeStringa := TrimRight(xLineeStringa) + ')';
          end;
          // per gli eventi occorsi --> apposita clausola
          if copy(TLinee.FieldByName('Stringa').asString, 1, 16) = 'Storico.IDEvento' then begin
               if pos('=', xLineeStringa) > 0 then begin
                    // evento occorso
                    xIDEvento := StrToIntDef(copy(xLineeStringa, pos('=', xLineeStringa) + 2, 4), 0);
                    xLineeStringa := IntToStr(xIDEvento) + ' in (select distinct IDEvento from storico st where anagrafica.id=st.idanagrafica ';
               end else begin
                    // evento non occorso
                    xIDEvento := StrToIntDef(copy(xLineeStringa, pos('>', xLineeStringa) + 2, 4), 0);
                    xLineeStringa := IntToStr(xIDEvento) + ' not in (select distinct IDEvento from storico st where anagrafica.id=st.idanagrafica ';
               end;
               //if TLineeGiorniDiff.Value>0 then
               //     xLineeStringa:=xLineeStringa+' and datediff(day,st.DataEvento,getdate())>'+TLineeGiorniDiff.AsString;
               xLineeStringa := xLineeStringa + ')';
          end;
          if copy(TLinee.FieldByName('Stringa').asString, 1, 16) = 'Storico.IDUtente' then begin
               if pos('=', xLineeStringa) > 0 then begin
                    // evento occorso
                    xIDEvento := StrToIntDef(copy(xLineeStringa, pos('=', xLineeStringa) + 2, 4), 0);
                    xLineeStringa := IntToStr(xIDEvento) + ' in (select distinct IDUtente from storico st where anagrafica.id=st.idanagrafica ';
               end else begin
                    // evento non occorso
                    xIDEvento := StrToIntDef(copy(xLineeStringa, pos('>', xLineeStringa) + 2, 4), 0);
                    xLineeStringa := IntToStr(xIDEvento) + ' not in (select distinct IDUtente from storico st where anagrafica.id=st.idanagrafica ';
               end;
               //if TLineeGiorniDiff.Value>0 then
               //     xLineeStringa:=xLineeStringa+' and datediff(day,st.DataEvento,getdate())>'+TLineeGiorniDiff.AsString;
               xLineeStringa := xLineeStringa + ')';
          end;


          TLinee.Next;
          if TLinee.Eof then
               // ultimo record -> senza AND alla fine
               if xApertaPar then QRes1.SQL.Add(' ' + TrimRight(xLineeStringa) + ')')
               else QRes1.SQL.Add(' ' + xLineeStringa)
          else begin
               TLinee.Prior;
               if TrimRight(TLinee.FieldByName('OpSucc').AsString) = 'and' then
                    if not xApertaPar then
                         QRes1.SQL.Add(' ' + xLineeStringa + ' and ')
                    else begin
                         QRes1.SQL.Add(' ' + xLineeStringa + ') and ');
                         xApertaPar := False;
                    end;
               if TrimRight(TLinee.FieldByName('OpSucc').AsString) = 'or' then
                    if not xApertaPar then begin
                         QRes1.SQL.Add(' (' + xLineeStringa + ' or ');
                         xApertaPar := True;
                    end else begin
                         QRes1.SQL.Add(' ' + xLineeStringa + ' or ');
                         xApertaPar := True;
                    end;
          end;
          TLinee.Next;
     end;

     // data ultimo inserimento in una ricerca
     QRes1.SQL.Add('and  Anagrafica.ID *= EBC_CandidatiRicerche.IDAnagrafica');
     QRes1.SQL.Add('     and EBC_CandidatiRicerche.DataIns =');
     QRes1.SQL.Add('     (select max(DataIns) from EBC_CandidatiRicerche');
     QRes1.SQL.Add('      where Anagrafica.ID *= EBC_CandidatiRicerche.IDAnagrafica)');
     // data ultimo contatto con il soggetto
     QRes1.SQL.Add('and Anagrafica.ID *= EBC_ContattiCandidati.IDAnagrafica');
     QRes1.SQL.Add('    and EBC_ContattiCandidati.Data=');
     QRes1.SQL.Add('    (select max(Data) from EBC_ContattiCandidati');
     QRes1.SQL.Add('     where Anagrafica.ID *= EBC_ContattiCandidati.IDAnagrafica)');
     // data ultimo colloquio
     QRes1.SQL.Add('and Anagrafica.ID *= EBC_Colloqui.IDAnagrafica');
     QRes1.SQL.Add('and EBC_Colloqui.Data=');
     QRes1.SQL.Add('    (select max(Data) from EBC_Colloqui');
     QRes1.SQL.Add('     where Anagrafica.ID *= EBC_Colloqui.IDAnagrafica)');

     // stati secondo la selezione
     if not PMStati1.Checked then
          QRes1.SQL.Add('and Anagrafica.IDTipoStato<>1');
     if not PMStati2.Checked then
          QRes1.SQL.Add('and Anagrafica.IDTipoStato<>2');
     if not PMStati3.Checked then
          QRes1.SQL.Add('and Anagrafica.IDTipoStato<>3');
     if not PMStati4.Checked then
          QRes1.SQL.Add('and Anagrafica.IDTipoStato<>5');
     if not PMStati5.Checked then
          QRes1.SQL.Add('and Anagrafica.IDTipoStato<>9');


     // Escludi dipendenti e altri stati H1HRMS
     QRes1.SQL.Add('and Anagrafica.IDTipoStato<>6');
     QRes1.SQL.Add('and Anagrafica.IDTipoStato<>7');
     QRes1.SQL.Add('and Anagrafica.IDTipoStato<>8');
     QRes1.SQL.Add('and Anagrafica.IDTipoStato<>11');
     QRes1.SQL.Add('and Anagrafica.IDTipoStato<>12');

     if CBColloquiati.Checked then
          QRes1.SQL.Add('and Anagrafica.ID in (select IDAnagrafica from EBC_Colloqui)');

     if not CBPropri.Checked then
          QRes1.SQL.Add('and (Anagrafica.IDProprietaCV is null or Anagrafica.IDProprietaCV=0)');

     // COMPOSIZIONE FINALE STRINGA SQL
     xStringaSQL := QRes1.SQL.Text;
     QRes1.SQL.Clear;
     QRes1.SQL.Add('SET DATEFORMAT dmy');
     QRes1.SQL.Add(xStringaSQL);


     // INFORMATION RETRIEVAL
     if EIRNote.text <> '' then begin

          xEIRNote := '"' + EIRNote.Text + '"';
          if pos(' AND ', UpperCase(xEIRNote)) > 0 then xEIRNote := StringReplace(xEIrNote, ' and ', '" and "', [rfIgnoreCase, rfReplaceAll]);
          if pos(' OR ', UpperCase(xEIrNote)) > 0 then xEIRNote := StringReplace(xEIrNote, ' or ', '" or "', [rfIgnoreCase, rfReplaceAll]);
          if Data.Global.FieldByName('AnagFileDentroDB').AsBoolean then begin
               QRes1.SQL.Add(' and anagrafica.id *= anagfile.idanagrafica');
               QRes1.SQL.Add(' and CONTAINS(AnagFile.DocFile, ''' + xEIRNote + ''')');
          end else begin

          // SU FILE SYSTEM
          // sapere se � abilitata sul CAMPO NOTE (AnagAltreInfo.Note)
               if (xIsFulltextInstalled) and (xAnagAltreInfoOK) and (CBIRnote.Checked) then
                    xIRNote := True
               else xIRNote := False;
          // sotto-query file-system
               if (xIsFulltextInstalled) and (xFileSystemOK) and (CBIRFileSystem.Checked) then begin
                    xIRFileSystem := True;
                    Q.Close;
                    Q.SQL.text := 'select IRFileSystemScope from global';
                    Q.Open;
                    xIRFileSystemScope := Q.FieldByName('IRFileSystemScope').asString;
               // sotto-query AnagFile
                    QRes1.SQL.Add('and (Anagrafica.ID in ( SELECT distinct IDAnagrafica FROM ' +
                         'OPENQUERY(FileSystem, ''SELECT FileName,Characterization,DocAuthor,DocComments,DocSubject,DocTitle ' +
                         ' FROM SCOPE ('''' "' + xIRFileSystemScope + '" '''') ' +
                         ' WHERE CONTAINS( Contents, ''''' + xEIrNote + ''''' ) '') AS Q, AnagFile ' +
                         ' WHERE Q.FileName = AnagFile.SoloNome COLLATE SQL_Latin1_General_CP1_CI_AS ');
               // altre regole
                    if CBIRaltreRegole.Checked then begin
                         Q.Close;
                         Q.SQL.text := 'select Regola from IR_LinkRules';
                         Q.Open;
                         while not Q.EOF do begin
                              QRes1.SQL.Add('UNION');
                              QRes1.SQL.Add('SELECT distinct Anagrafica.ID IDAnagrafica FROM ' +
                                   'OPENQUERY(FileSystem, ''SELECT FileName,Characterization,DocAuthor,DocComments,DocSubject,DocTitle ' +
                                   ' FROM SCOPE ('''' "' + xIRFileSystemScope + '" '''') ' +
                                   ' WHERE CONTAINS( Contents, ''''' + xEIrNote + ''''' ) '') AS Q, Anagrafica ' +
                                   ' WHERE ' + Q.FieldByName('Regola').asString);
                              Q.Next;
                         end;
                    end;

                    QRes1.SQL.Add(')');
                    Q.Close;
                    QRes1.SQL.Add(')');

               end else xIRFileSystem := False;
          end;
          // INFORMATION RETRIEVAL SUL CAMPO NOTE (AnagAltreInfo.Note)
          if xIRNote then begin
               if not xIRFileSystem then begin
                    QRes1.SQL.Add('and CONTAINS(AnagAltreInfo.Note, ''' + xEIrNote + ''') ');
               end else begin
                    QRes1.SQL.Add('UNION');
                    QRes1.SQL.add(xStringaSQL); // ancora tutta la stringa
                    QRes1.SQL.Add('and CONTAINS(AnagAltreInfo.Note, ''' + xEIrNote + ''') ');
               end;
          end;

     end;

     // ORDINAMENTO
     if xTipoQuery = 0 then
          QRes1.SQL.Add(xOrdine);

     // anzianit� CV
     if CBAnzCV.Checked then
          QRes1.ParamByName['xCVInseritoInData'] := DateToStr(Date - (SEAnzCV.Value * 30));

     ScriviRegistry('QRes1', QRes1.SQL.text);

     //QRes1.ExecSQL;
     if xTipoQuery = 0 then
          QRes1.Open
     else begin
          xQueryCount := TADOQuery.Create(self);
          xQueryCount.Connection := Data.DB;
          xQueryCount.SQL.Text := 'select count (distinct anagrafica.id) ' + copy(qres1.sql.Text, pos('FROM', UpperCase(qres1.sql.Text)), length(qres1.sql.Text));
          xQueryCount.Open;
     end;

     TLinee.Close;
     TLinee.SQL.text := 'select * from UserLineeQuery where IDAnagrafica=:xIDAnag:';
     TLinee.ParamByName['xIDAnag'] := MainForm.xIDUtenteAttuale;
     TLinee.Open;
     if xTipoQuery = 0 then begin
          dxDBGrid1.Visible := True;
          LTot.Caption := IntToStr(QRes1.RecordCount);
          LTot.Visible := True;
     end else begin
          dxDBGrid1.Visible := False;
          LTot.Caption := IntToStr(xQueryCount.Fields[0].Value);
          LTot.Visible := True;

          xQueryCount.Close;
          xQueryCount.Free;
     end;
end;

procedure TSelPersNewForm.BitBtn2Click(Sender: TObject);
begin
     xTipoQuery := 0;
     EseguiQueryRic;
end;

procedure TSelPersNewForm.ApriQuery1DrawItem(Sender: TObject;
     ACanvas: TCanvas; ARect: TRect; Selected: Boolean);
begin;

end;

procedure TSelPersNewForm.PMExportPopup(Sender: TObject);
var MyPopUpItems: array[0..100] of TMenuItem;
     MyPopUpItemsDel: array[0..100] of TMenuItem;
     i, k: Integer;
     xMenuQuery: TADOQuery;
begin
     xMenuQuery := TADOQuery.Create(self);
     xMenuQuery.Connection := Data.DB;
     xMenuQuery.SQL.Text := 'select distinct NomeSalvataggio from UserLineeQuerySalvate where idanagrafica = :xidanagrafica';
     xMenuQuery.Parameters[0].Value := MainForm.xIDUtenteAttuale;
     xMenuQuery.Open;

     for k := 0 to ApriQuery1.Count - 1 do begin
          ApriQuery1.Delete(0);
          EliminaQuery1.Delete(0);
     end;

     i := 0;
     while not xMenuQuery.Eof do begin
          MyPopUpItems[i] := TMenuItem.Create(Self);
          MyPopUpItems[i].Caption := xMenuQuery.Fields[0].AsString;
          ApriQuery1.Insert(i, MyPopupItems[i]);
          MyPopUpItems[i].OnClick := MyPopup;

          MyPopUpItemsDel[i] := TMenuItem.Create(Self);
          MyPopUpItemsDel[i].Caption := xMenuQuery.Fields[0].AsString;
          EliminaQuery1.Insert(i, MyPopUpItemsDel[i]);
          MyPopUpItemsDel[i].OnClick := MyPopupDel;

          inc(i);
          xMenuQuery.Next;
     end;

     xMenuQuery.Free;
end;


procedure TSelPersNewForm.MyPopup(Sender: TObject);
var xNome: string;
begin

     {with Sender as TMenuItem do begin

     end;    }

     //with Sender as TMenuItem do xNome := Copy(Caption, 2, length(Caption));
     with Sender as TMenuItem do xNome := Caption;
     //showmessage(xnome);

     Query1.Close;
     Query1.SQL.text := 'delete from userlineequery where idanagrafica = ' + inttostr(MainForm.xIDUtenteAttuale);
     Query1.ExecSQL;

     Query1.Close;
     Query1.SQL.text := 'insert into userlineequery (IDAnagrafica ,Descrizione ,Stringa ,Tabella ,OpSucc , ' +
          'OpNext ,LivLinguaParlato ,LivLinguaScritto ,DescTabella ,DescCampo , ' +
          'DescOperatore ,Valore ,QueryLookup ,DescCompleta ,IDTabQueryOp ) ' +
          'select IDAnagrafica ,Descrizione ,Stringa ,Tabella ,OpSucc , ' +
          'OpNext ,LivLinguaParlato ,LivLinguaScritto ,DescTabella ,DescCampo , ' +
          'DescOperatore ,Valore ,QueryLookup ,DescCompleta ,IDTabQueryOp ' +
          'from userlineequerysalvate ' +
          'where IDAnagrafica=:xIDAnag: and NomeSalvataggio=:xNomeSalvataggio:';
     Query1.ParamByName['xIDAnag'] := MainForm.xIDUtenteAttuale;
     Query1.ParamByName['xNomeSalvataggio'] := xNome;
     Query1.ExecSQL;

     Query1.close;

     TLinee.Close;
     TLinee.Open;
     Panel2.Caption := '  Criteri impostati caricati da Query "' + xNome + '"';
     xTipoQuery := 1;
     EseguiQueryRic;

end;

procedure TSelPersNewForm.SalvaQuery1Click(Sender: TObject);
var xNomeQuery: string;
begin
     if InputQuery('Salva con nome', 'Nome:', xNomeQuery) then begin
          Query1.Close;
          Query1.sql.Text := 'select * from userlineequerysalvate where IDAnagrafica=:xIDAnag: ' +
               ' and NomeSalvataggio=:xNomeSalvataggio:';
          Query1.ParamByName['xIDAnag'] := MainForm.xIDUtenteAttuale;
          Query1.ParamByName['xNomeSalvataggio'] := xNomeQuery;
          Query1.Open;

          if (Query1.RecordCount > 0) and (MessageDlg('Attenzione: Nome gi� esistente - Vuoi sovrascrivere il salvataggio precedente', mtWarning, [mbYes, mbNo], 0) = mrNO) then begin

               SalvaQuery1Click(self);

          end else begin
               if Query1.RecordCount > 0 then begin
                    Query1.Close;
                    Query1.sql.Text := 'delete from userlineequerysalvate where IDAnagrafica=:xIDAnag: ' +
                         ' and NomeSalvataggio=:xNomeSalvataggio:';
                    Query1.ParamByName['xIDAnag'] := MainForm.xIDUtenteAttuale;
                    Query1.ParamByName['xNomeSalvataggio'] := xNomeQuery;
                    Query1.ExecSQL;
               end;
               Query1.Close;
               Query1.SQL.Text := 'insert into userlineequerysalvate (IDAnagrafica ,Descrizione ,Stringa ,Tabella ,OpSucc , ' +
                    'OpNext ,LivLinguaParlato ,LivLinguaScritto ,DescTabella ,DescCampo , ' +
                    'DescOperatore ,Valore ,QueryLookup ,DescCompleta ,IDTabQueryOp,NomeSalvataggio ) ' +
                    'select :xIDAnag:, Descrizione ,Stringa ,Tabella ,OpSucc , ' +
                    'OpNext ,LivLinguaParlato ,LivLinguaScritto ,DescTabella ,DescCampo , ' +
                    'DescOperatore ,Valore ,QueryLookup ,DescCompleta ,IDTabQueryOp, :xNomeSalvataggio: ' +
                    'from userlineequery where IDAnagrafica= ' + inttostr(MainForm.xIDUtenteAttuale);
               Query1.ParamByName['xIDAnag'] := MainForm.xIDUtenteAttuale;
               Query1.ParamByName['xNomeSalvataggio'] := xNomeQuery;
               Query1.ExecSQL;
          end;
     end;
end;

procedure TSelPersNewForm.MyPopupDel(Sender: TObject);
var xNome: string;
begin

     {with Sender as TMenuItem do begin

     end;    }

     //with Sender as TMenuItem do xNome := Copy(Caption, 2, length(Caption));
     with Sender as TMenuItem do xNome := Caption;
     //showmessage(xnome);
     if MessageDlg('Sei sicuro di voler eliminare questa interrogazione?', mtWarning, [mbYes, mbNo], 0) = mrYes then begin
          Query1.Close;
          Query1.SQL.text := 'delete from userlineequerysalvate where idanagrafica = :xIDAnag: and ' +
               'NomeSalvataggio = :xNomeSalvataggio: ';
          Query1.ParamByName['xIDAnag'] := MainForm.xIDUtenteAttuale;
          Query1.ParamByName['xNomeSalvataggio'] := xNome;
          Query1.ExecSQL;

          PMExportPopup(self);

     end;
end;

procedure TSelPersNewForm.Button1Click(Sender: TObject);
begin
     formMotoriRicerca := TformMotoriRicerca.create(self);
     formMotoriRicerca.Edit1.text := QMotoriRicerca.fieldbyname('LinkPers').asString;
     formMotoriRicerca.memo1.text := QMotoriRicerca.fieldbyname('Notepers').asString;
     formMotoriRicerca.ShowModal;

     QsaveMotoriRicercaPers.Parameters[0].Value := main.MainForm.xIDUtenteAttuale;
     QsaveMotoriRicercaPers.Parameters[1].value := QMotoriRicerca.fieldbyname('ID').asInteger;
     QsaveMotoriRicercaPers.Parameters[2].value := formmotoriricerca.edit1.text;
     QsaveMotoriRicercaPers.Parameters[3].value := formmotoriricerca.Memo1.text;
     QsaveMotoriRicercaPers.Parameters[4].value := main.MainForm.xIDUtenteAttuale;
     QsaveMotoriRicercaPers.Parameters[5].value := QMotoriRicerca.fieldbyname('ID').asInteger;
     QsaveMotoriRicercaPers.Parameters[6].value := main.MainForm.xIDUtenteAttuale;
     QsaveMotoriRicercaPers.Parameters[7].value := QMotoriRicerca.fieldbyname('ID').asInteger;
     QsaveMotoriRicercaPers.Parameters[8].value := formmotoriricerca.edit1.text;
     QsaveMotoriRicercaPers.Parameters[9].value := formmotoriricerca.Memo1.text;
     QsaveMotoriRicercaPers.ExecSQL;

     formmotoriricerca.Free;

     QMotoriRicerca.close;
     QMotoriRicerca.Parameters[0].value := main.MainForm.xIDUtenteAttuale;
     QMotoriRicerca.Open;
end;

end.

