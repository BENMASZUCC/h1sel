object StoricoTargetListForm: TStoricoTargetListForm
  Left = 262
  Top = 160
  BorderStyle = bsDialog
  Caption = 'Storico target list per l'#39'azienda'
  ClientHeight = 372
  ClientWidth = 525
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 525
    Height = 47
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object Label1: TLabel
      Left = 7
      Top = 27
      Width = 240
      Height = 13
      Caption = 'ATTENZIONE: I dati non sono modificabili'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clPurple
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object BitBtn1: TBitBtn
      Left = 328
      Top = 5
      Width = 97
      Height = 36
      TabOrder = 0
      Kind = bkOK
    end
    object BitBtn2: TBitBtn
      Left = 428
      Top = 5
      Width = 93
      Height = 36
      Caption = 'Annulla'
      TabOrder = 1
      Kind = bkCancel
    end
  end
  object dxDBGrid1: TdxDBGrid
    Left = 0
    Top = 47
    Width = 525
    Height = 325
    Bands = <
      item
      end>
    DefaultLayout = True
    HeaderPanelRowCount = 1
    KeyField = 'ID'
    SummaryGroups = <>
    SummarySeparator = ', '
    Align = alClient
    TabOrder = 1
    DataSource = DsQStoricoTL
    Filter.Criteria = {00000000}
    OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
    OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoUseBitmap]
    object dxDBGrid1ID: TdxDBGridMaskColumn
      Visible = False
      Width = 96
      BandIndex = 0
      RowIndex = 0
      FieldName = 'ID'
    end
    object dxDBGrid1IDRicerca: TdxDBGridMaskColumn
      Visible = False
      Width = 96
      BandIndex = 0
      RowIndex = 0
      FieldName = 'IDRicerca'
    end
    object dxDBGrid1IDCliente: TdxDBGridMaskColumn
      Visible = False
      Width = 96
      BandIndex = 0
      RowIndex = 0
      FieldName = 'IDCliente'
    end
    object dxDBGrid1Progressivo: TdxDBGridMaskColumn
      Caption = 'Rif.commessa'
      DisableEditor = True
      Width = 87
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Progressivo'
    end
    object dxDBGrid1Cliente: TdxDBGridMaskColumn
      DisableEditor = True
      Width = 192
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Cliente'
    end
    object dxDBGrid1DataUltimaEsploraz: TdxDBGridDateColumn
      Caption = 'Data Ultima Esploraz.'
      DisableEditor = True
      Width = 127
      BandIndex = 0
      RowIndex = 0
      FieldName = 'DataUltimaEsploraz'
    end
    object dxDBGrid1Note: TdxDBGridBlobColumn
      Width = 115
      BandIndex = 0
      RowIndex = 0
      HeaderMaxLineCount = 0
      FieldName = 'Note'
      BlobPaintStyle = bpsText
    end
  end
  object QStoricoTL_OLD: TQuery
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select EBC_RicercheTargetList.*,'
      '       Progressivo, EBC_Clienti.Descrizione Cliente, '
      '       EBC_LogEsplorazioni.Data DataUltimaEsploraz'
      'from EBC_RicercheTargetList,'
      '     EBC_LogEsplorazioni, EBC_Ricerche, EBC_Clienti'
      'where EBC_RicercheTargetList.IDRicerca = EBC_Ricerche.ID'
      
        '  and EBC_RicercheTargetList.ID *= EBC_LogEsplorazioni.IDTargetL' +
        'ist'
      
        '  and EBC_LogEsplorazioni.Data = (select max(Data) from EBC_LogE' +
        'splorazioni'
      
        '                                  where EBC_RicercheTargetList.I' +
        'D *= EBC_LogEsplorazioni.IDTargetList) '
      '  and EBC_Ricerche.IDCliente = EBC_Clienti.ID'
      'and EBC_RicercheTargetList.IDCliente=:xIDCliente')
    Left = 72
    Top = 104
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'xIDCliente'
        ParamType = ptUnknown
      end>
    object QStoricoTL_OLDID: TAutoIncField
      FieldName = 'ID'
    end
    object QStoricoTL_OLDIDRicerca: TIntegerField
      FieldName = 'IDRicerca'
    end
    object QStoricoTL_OLDIDCliente: TIntegerField
      FieldName = 'IDCliente'
    end
    object QStoricoTL_OLDNote: TMemoField
      FieldName = 'Note'
      BlobType = ftMemo
    end
    object QStoricoTL_OLDProgressivo: TStringField
      FieldName = 'Progressivo'
      FixedChar = True
      Size = 10
    end
    object QStoricoTL_OLDCliente: TStringField
      FieldName = 'Cliente'
      FixedChar = True
      Size = 30
    end
    object QStoricoTL_OLDDataUltimaEsploraz: TDateTimeField
      FieldName = 'DataUltimaEsploraz'
    end
  end
  object DsQStoricoTL: TDataSource
    DataSet = QStoricoTL
    Left = 72
    Top = 136
  end
  object QStoricoTL: TADOLinkedQuery
    Connection = Data.DB
    Parameters = <>
    OriginalSQL.Strings = (
      'select EBC_RicercheTargetList.*,'
      '       Progressivo, EBC_Clienti.Descrizione Cliente, '
      '       EBC_LogEsplorazioni.Data DataUltimaEsploraz'
      'from EBC_RicercheTargetList,'
      '     EBC_LogEsplorazioni, EBC_Ricerche, EBC_Clienti'
      'where EBC_RicercheTargetList.IDRicerca = EBC_Ricerche.ID'
      
        '  and EBC_RicercheTargetList.ID *= EBC_LogEsplorazioni.IDTargetL' +
        'ist'
      
        '  and EBC_LogEsplorazioni.Data = (select max(Data) from EBC_LogE' +
        'splorazioni'
      
        '                                  where EBC_RicercheTargetList.I' +
        'D *= EBC_LogEsplorazioni.IDTargetList) '
      '  and EBC_Ricerche.IDCliente = EBC_Clienti.ID'
      'and EBC_RicercheTargetList.IDCliente=:xIDCliente:')
    UseFilter = False
    Left = 64
    Top = 72
  end
end
