object CampiPersFrame: TCampiPersFrame
  Left = 0
  Top = 0
  Width = 487
  Height = 361
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  ParentFont = False
  TabOrder = 0
  object PanTop: TPanel
    Left = 0
    Top = 0
    Width = 487
    Height = 40
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object BitBtn1: TToolbarButton97
      Left = 2
      Top = 1
      Width = 82
      Height = 37
      Caption = 'Conferma Modifica'
      WordWrap = True
      OnClick = BitBtn1Click
    end
    object BitBtn2: TToolbarButton97
      Left = 84
      Top = 1
      Width = 82
      Height = 37
      Caption = 'Annulla modifiche'
      WordWrap = True
      OnClick = BitBtn2Click
    end
    object ToolbarButton971: TToolbarButton97
      Left = 200
      Top = 1
      Width = 82
      Height = 37
      Caption = 'Definisci campi'
      Glyph.Data = {
        16050000424D16050000000000003604000028000000100000000E0000000100
        080000000000E0000000130B0000130B00000001000000000000102830006048
        3000103040002038400020507000407840004058700040607000506070007167
        630040805000508860006098700050A8600050B0600050B87000A0A075006078
        80002088B00070A8800060C080002098D00020A0D00030A8D00020B0E00030B0
        E00030B8F00060B0D00040C0F00050C8F00060C0E00060D0F00070D8F0008088
        9000B0989000B0A09000B7A59A008090A0008098A00090A0A00090A0B00090A8
        B000B0A0A000B0A8A000C0A09000C0A89000E0A89000E0B09000C0A8A000C0B0
        A000D0B0A000D0B8A000E0B8A00090D0A000E0C0A000F0C8B000B0B8C00080D0
        E00080E0F000C0C8C000C0C8D000E0D2C300E0D3C500E0D4C700F0D0C000F0E0
        D000D8E9EC00E0E0E000F0E8E000FFF0E000E0E8F000FFF8F000FFFFF000FFFF
        FF00000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        00000000000000000000000000000000000000000000000000002C0101010101
        010101010101424242422D47454441403736342F2E01424242423048302A4423
        222222222F01424242423048484745444140373634014242424230482B2A4723
        2222222236014242424231484848484745444140370142424242314848484848
        473B07414001424242423149461143483C0817032A014242100A324929390838
        081E081A00020009050D334946293A0839081F07191512040B0E33333331293A
        083A061F1D1C18160C0F424242423F293A063A201F1D1C1A131442424242423E
        293A3A3A201F1B2142354242424242423D292827262521244242}
      WordWrap = True
      OnClick = ToolbarButton971Click
    end
  end
  object Pan1: TPanel
    Left = 0
    Top = 40
    Width = 487
    Height = 27
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 1
    Visible = False
    object Label1: TLabel
      Left = 7
      Top = 6
      Width = 31
      Height = 13
      Caption = 'Label1'
    end
    object dxDBEdit1: TdxDBEdit
      Left = 144
      Top = 3
      Width = 339
      TabOrder = 0
      Anchors = [akLeft, akTop, akRight]
      DataSource = DsQCampiPers
    end
    object dxDBDateEdit1: TdxDBDateEdit
      Left = 144
      Top = 3
      Width = 97
      TabOrder = 1
      Visible = False
      DataSource = DsQCampiPers
    end
    object dxDBCheckEdit1: TdxDBCheckEdit
      Left = 144
      Top = 4
      Width = 22
      TabOrder = 2
      Visible = False
      DataSource = DsQCampiPers
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object dxDBSpinEdit1: TdxDBSpinEdit
      Left = 144
      Top = 3
      Width = 57
      TabOrder = 3
      Visible = False
      DataSource = DsQCampiPers
    end
  end
  object Pan2: TPanel
    Left = 0
    Top = 67
    Width = 487
    Height = 27
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 2
    Visible = False
    object Label2: TLabel
      Left = 7
      Top = 6
      Width = 31
      Height = 13
      Caption = 'Label1'
    end
    object dxDBEdit2: TdxDBEdit
      Left = 144
      Top = 3
      Width = 339
      TabOrder = 0
      Anchors = [akLeft, akTop, akRight]
      DataSource = DsQCampiPers
    end
    object dxDBDateEdit2: TdxDBDateEdit
      Left = 144
      Top = 3
      Width = 97
      TabOrder = 1
      Visible = False
      DataSource = DsQCampiPers
    end
    object dxDBCheckEdit2: TdxDBCheckEdit
      Left = 144
      Top = 4
      Width = 22
      TabOrder = 2
      Visible = False
      DataSource = DsQCampiPers
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object dxDBSpinEdit2: TdxDBSpinEdit
      Left = 144
      Top = 3
      Width = 57
      TabOrder = 3
      Visible = False
      DataSource = DsQCampiPers
    end
  end
  object Pan3: TPanel
    Left = 0
    Top = 94
    Width = 487
    Height = 27
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 3
    Visible = False
    object Label3: TLabel
      Left = 7
      Top = 6
      Width = 31
      Height = 13
      Caption = 'Label1'
    end
    object dxDBEdit3: TdxDBEdit
      Left = 144
      Top = 3
      Width = 339
      TabOrder = 0
      Anchors = [akLeft, akTop, akRight]
      DataSource = DsQCampiPers
    end
    object dxDBDateEdit3: TdxDBDateEdit
      Left = 144
      Top = 3
      Width = 97
      TabOrder = 1
      Visible = False
      DataSource = DsQCampiPers
    end
    object dxDBCheckEdit3: TdxDBCheckEdit
      Left = 144
      Top = 4
      Width = 22
      TabOrder = 2
      Visible = False
      DataSource = DsQCampiPers
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object dxDBSpinEdit3: TdxDBSpinEdit
      Left = 144
      Top = 3
      Width = 57
      TabOrder = 3
      Visible = False
      DataSource = DsQCampiPers
    end
  end
  object Pan4: TPanel
    Left = 0
    Top = 121
    Width = 487
    Height = 27
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 4
    Visible = False
    object Label4: TLabel
      Left = 7
      Top = 6
      Width = 31
      Height = 13
      Caption = 'Label1'
    end
    object dxDBEdit4: TdxDBEdit
      Left = 144
      Top = 3
      Width = 339
      TabOrder = 0
      Anchors = [akLeft, akTop, akRight]
      DataSource = DsQCampiPers
    end
    object dxDBDateEdit4: TdxDBDateEdit
      Left = 144
      Top = 3
      Width = 97
      TabOrder = 1
      Visible = False
      DataSource = DsQCampiPers
    end
    object dxDBCheckEdit4: TdxDBCheckEdit
      Left = 144
      Top = 4
      Width = 22
      TabOrder = 2
      Visible = False
      DataSource = DsQCampiPers
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object dxDBSpinEdit4: TdxDBSpinEdit
      Left = 144
      Top = 3
      Width = 57
      TabOrder = 3
      Visible = False
      DataSource = DsQCampiPers
    end
  end
  object Pan5: TPanel
    Left = 0
    Top = 148
    Width = 487
    Height = 27
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 5
    Visible = False
    object Label5: TLabel
      Left = 7
      Top = 6
      Width = 31
      Height = 13
      Caption = 'Label1'
    end
    object dxDBEdit5: TdxDBEdit
      Left = 144
      Top = 3
      Width = 339
      TabOrder = 0
      Anchors = [akLeft, akTop, akRight]
      DataSource = DsQCampiPers
    end
    object dxDBDateEdit5: TdxDBDateEdit
      Left = 144
      Top = 3
      Width = 97
      TabOrder = 1
      Visible = False
      DataSource = DsQCampiPers
    end
    object dxDBCheckEdit5: TdxDBCheckEdit
      Left = 144
      Top = 4
      Width = 22
      TabOrder = 2
      Visible = False
      DataSource = DsQCampiPers
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object dxDBSpinEdit5: TdxDBSpinEdit
      Left = 144
      Top = 3
      Width = 57
      TabOrder = 3
      Visible = False
      DataSource = DsQCampiPers
    end
  end
  object Pan6: TPanel
    Left = 0
    Top = 175
    Width = 487
    Height = 27
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 6
    Visible = False
    object Label6: TLabel
      Left = 7
      Top = 6
      Width = 31
      Height = 13
      Caption = 'Label1'
    end
    object dxDBEdit6: TdxDBEdit
      Left = 144
      Top = 3
      Width = 339
      TabOrder = 0
      Anchors = [akLeft, akTop, akRight]
      DataSource = DsQCampiPers
    end
    object dxDBDateEdit6: TdxDBDateEdit
      Left = 144
      Top = 3
      Width = 97
      TabOrder = 1
      Visible = False
      DataSource = DsQCampiPers
    end
    object dxDBCheckEdit6: TdxDBCheckEdit
      Left = 144
      Top = 4
      Width = 22
      TabOrder = 2
      Visible = False
      DataSource = DsQCampiPers
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object dxDBSpinEdit6: TdxDBSpinEdit
      Left = 144
      Top = 3
      Width = 57
      TabOrder = 3
      Visible = False
      DataSource = DsQCampiPers
    end
  end
  object Pan7: TPanel
    Left = 0
    Top = 202
    Width = 487
    Height = 27
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 7
    Visible = False
    object Label7: TLabel
      Left = 7
      Top = 6
      Width = 31
      Height = 13
      Caption = 'Label1'
    end
    object dxDBEdit7: TdxDBEdit
      Left = 144
      Top = 3
      Width = 339
      TabOrder = 0
      Anchors = [akLeft, akTop, akRight]
      DataSource = DsQCampiPers
    end
    object dxDBDateEdit7: TdxDBDateEdit
      Left = 144
      Top = 3
      Width = 97
      TabOrder = 1
      Visible = False
      DataSource = DsQCampiPers
    end
    object dxDBCheckEdit7: TdxDBCheckEdit
      Left = 144
      Top = 4
      Width = 22
      TabOrder = 2
      Visible = False
      DataSource = DsQCampiPers
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object dxDBSpinEdit7: TdxDBSpinEdit
      Left = 144
      Top = 3
      Width = 57
      TabOrder = 3
      Visible = False
      DataSource = DsQCampiPers
    end
  end
  object Pan8: TPanel
    Left = 0
    Top = 229
    Width = 487
    Height = 27
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 8
    Visible = False
    object Label8: TLabel
      Left = 7
      Top = 6
      Width = 31
      Height = 13
      Caption = 'Label1'
    end
    object dxDBEdit8: TdxDBEdit
      Left = 144
      Top = 3
      Width = 339
      TabOrder = 0
      Anchors = [akLeft, akTop, akRight]
      DataSource = DsQCampiPers
    end
    object dxDBDateEdit8: TdxDBDateEdit
      Left = 144
      Top = 3
      Width = 97
      TabOrder = 1
      Visible = False
      DataSource = DsQCampiPers
    end
    object dxDBCheckEdit8: TdxDBCheckEdit
      Left = 144
      Top = 4
      Width = 22
      TabOrder = 2
      Visible = False
      DataSource = DsQCampiPers
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object dxDBSpinEdit8: TdxDBSpinEdit
      Left = 144
      Top = 3
      Width = 57
      TabOrder = 3
      Visible = False
      DataSource = DsQCampiPers
    end
  end
  object Pan9: TPanel
    Left = 0
    Top = 256
    Width = 487
    Height = 27
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 9
    Visible = False
    object Label9: TLabel
      Left = 7
      Top = 6
      Width = 31
      Height = 13
      Caption = 'Label1'
    end
    object dxDBEdit9: TdxDBEdit
      Left = 144
      Top = 3
      Width = 339
      TabOrder = 0
      Anchors = [akLeft, akTop, akRight]
      DataSource = DsQCampiPers
    end
    object dxDBDateEdit9: TdxDBDateEdit
      Left = 144
      Top = 3
      Width = 97
      TabOrder = 1
      Visible = False
      DataSource = DsQCampiPers
    end
    object dxDBCheckEdit9: TdxDBCheckEdit
      Left = 144
      Top = 4
      Width = 22
      TabOrder = 2
      Visible = False
      DataSource = DsQCampiPers
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object dxDBSpinEdit9: TdxDBSpinEdit
      Left = 144
      Top = 3
      Width = 57
      TabOrder = 3
      Visible = False
      DataSource = DsQCampiPers
    end
  end
  object Pan10: TPanel
    Left = 0
    Top = 283
    Width = 487
    Height = 27
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 10
    Visible = False
    object Label10: TLabel
      Left = 7
      Top = 6
      Width = 31
      Height = 13
      Caption = 'Label1'
    end
    object dxDBEdit10: TdxDBEdit
      Left = 144
      Top = 3
      Width = 339
      TabOrder = 0
      Anchors = [akLeft, akTop, akRight]
      DataSource = DsQCampiPers
    end
    object dxDBDateEdit10: TdxDBDateEdit
      Left = 144
      Top = 3
      Width = 97
      TabOrder = 1
      Visible = False
      DataSource = DsQCampiPers
    end
    object dxDBCheckEdit10: TdxDBCheckEdit
      Left = 144
      Top = 4
      Width = 22
      TabOrder = 2
      Visible = False
      DataSource = DsQCampiPers
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object dxDBSpinEdit10: TdxDBSpinEdit
      Left = 144
      Top = 3
      Width = 57
      TabOrder = 3
      Visible = False
      DataSource = DsQCampiPers
    end
  end
  object QCampiPers: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from AnagCampiPers'
      'where IDAnagrafica=875')
    Left = 24
    Top = 288
  end
  object DsQCampiPers: TDataSource
    DataSet = TCampiPers
    OnStateChange = DsQCampiPersStateChange
    Left = 24
    Top = 320
  end
  object Q: TADOQuery
    Connection = Data.DB
    Parameters = <>
    Left = 200
    Top = 296
  end
  object TCampiPers: TADOTable
    Connection = Data.DB
    BeforeClose = TCampiPersBeforeClose
    Left = 64
    Top = 288
  end
  object Qattivitaformaz: TADOQuery
    Connection = Data.DB
    Parameters = <>
    SQL.Strings = (
      'select Form_Attivita.*, '
      '          AnagTutor.Cognome TutorCognome, '
      
        '          AnagTutor.Nome TutorNome, AnagTutor.Matricola TutorMat' +
        'ricola,'
      '          Form_Attivita.id idcorso'
      'from Form_Attivita,Anagrafica AnagTutor'
      'where Form_Attivita.IDTutor *= AnagTutor.ID'
      'and Form_Attivita.SoloAnagrafica = 0')
    Left = 328
    Top = 320
    object QattivitaformazID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QattivitaformazIDProgettoForm: TIntegerField
      FieldName = 'IDProgettoForm'
    end
    object QattivitaformazCodiceCorso: TStringField
      FieldName = 'CodiceCorso'
      Size = 15
    end
    object QattivitaformazInterno: TBooleanField
      FieldName = 'Interno'
    end
    object QattivitaformazIDArea: TIntegerField
      FieldName = 'IDArea'
    end
    object QattivitaformazIDResponsabile: TIntegerField
      FieldName = 'IDResponsabile'
    end
    object QattivitaformazIDEnteFormaz: TIntegerField
      FieldName = 'IDEnteFormaz'
    end
    object QattivitaformazIDTutor: TIntegerField
      FieldName = 'IDTutor'
    end
    object QattivitaformazCodiceArgomento: TStringField
      FieldName = 'CodiceArgomento'
      Size = 3
    end
    object QattivitaformazLivello: TStringField
      FieldName = 'Livello'
      Size = 1
    end
    object QattivitaformazAttiva: TBooleanField
      FieldName = 'Attiva'
    end
    object QattivitaformazStato: TStringField
      FieldName = 'Stato'
    end
    object QattivitaformazTipo: TStringField
      FieldName = 'Tipo'
    end
    object QattivitaformazDataInizio: TDateTimeField
      FieldName = 'DataInizio'
    end
    object QattivitaformazDataFine: TDateTimeField
      FieldName = 'DataFine'
    end
    object QattivitaformazPeriodoPrevisto: TStringField
      FieldName = 'PeriodoPrevisto'
      Size = 30
    end
    object QattivitaformazDurataPrevista: TStringField
      FieldName = 'DurataPrevista'
      Size = 30
    end
    object QattivitaformazOrarioDalle: TDateTimeField
      FieldName = 'OrarioDalle'
    end
    object QattivitaformazOrarioAlle: TDateTimeField
      FieldName = 'OrarioAlle'
    end
    object QattivitaformazDescrizione: TStringField
      FieldName = 'Descrizione'
      Size = 255
    end
    object QattivitaformazLuogoSvolgimento: TStringField
      FieldName = 'LuogoSvolgimento'
      Size = 40
    end
    object QattivitaformazProgramma: TMemoField
      FieldName = 'Programma'
      BlobType = ftMemo
    end
    object QattivitaformazNote: TMemoField
      FieldName = 'Note'
      BlobType = ftMemo
    end
    object QattivitaformazNumMinPartec: TIntegerField
      FieldName = 'NumMinPartec'
    end
    object QattivitaformazNumMaxPartec: TIntegerField
      FieldName = 'NumMaxPartec'
    end
    object QattivitaformazTotOreLavoro: TIntegerField
      FieldName = 'TotOreLavoro'
    end
    object QattivitaformazTotOreExtra: TIntegerField
      FieldName = 'TotOreExtra'
    end
    object QattivitaformazImportoFinanziatoLire: TFloatField
      FieldName = 'ImportoFinanziatoLire'
    end
    object QattivitaformazImportoFinanziatoEuro: TFloatField
      FieldName = 'ImportoFinanziatoEuro'
    end
    object QattivitaformazCostoOraLavorative: TFloatField
      FieldName = 'CostoOraLavorative'
    end
    object QattivitaformazCostoOraStraord: TFloatField
      FieldName = 'CostoOraStraord'
    end
    object QattivitaformazCostoOraLavFestiva: TFloatField
      FieldName = 'CostoOraLavFestiva'
    end
    object QattivitaformazNumProgetto: TStringField
      FieldName = 'NumProgetto'
    end
    object QattivitaformazFondoSociale: TBooleanField
      FieldName = 'FondoSociale'
    end
    object QattivitaformazIDMacroGruppo: TIntegerField
      FieldName = 'IDMacroGruppo'
    end
    object QattivitaformazTotOrePrev: TIntegerField
      FieldName = 'TotOrePrev'
    end
    object QattivitaformazCrediti: TSmallintField
      FieldName = 'Crediti'
    end
    object QattivitaformazIDQuest: TIntegerField
      FieldName = 'IDQuest'
    end
    object QattivitaformazIDFormTipiAtt: TIntegerField
      FieldName = 'IDFormTipiAtt'
    end
    object QattivitaformazSoloAnagrafica: TBooleanField
      FieldName = 'SoloAnagrafica'
    end
    object QattivitaformazTutorCognome: TStringField
      FieldName = 'TutorCognome'
      Size = 30
    end
    object QattivitaformazTutorNome: TStringField
      FieldName = 'TutorNome'
      Size = 30
    end
    object Qattivitaformazidcorso: TAutoIncField
      FieldName = 'idcorso'
      ReadOnly = True
    end
  end
end
