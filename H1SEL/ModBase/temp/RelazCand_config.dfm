object RelazCand_configForm: TRelazCand_configForm
  Left = 241
  Top = 236
  Width = 312
  Height = 335
  Caption = 'Personalizza visualizzazione'
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 304
    Height = 41
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object BitBtn1: TBitBtn
      Left = 214
      Top = 4
      Width = 86
      Height = 34
      Anchors = [akTop, akRight]
      Caption = 'Esci'
      TabOrder = 0
      Kind = bkOK
    end
  end
  object dxDBGrid1: TdxDBGrid
    Left = 0
    Top = 41
    Width = 304
    Height = 267
    Bands = <
      item
      end>
    DefaultLayout = True
    HeaderPanelRowCount = 1
    KeyField = 'ID'
    SummaryGroups = <>
    SummarySeparator = ', '
    Align = alClient
    TabOrder = 1
    DataSource = DsQTab
    Filter.Criteria = {00000000}
    OptionsDB = [edgoCancelOnExit, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
    OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoIndicator, edgoUseBitmap]
    object dxDBGrid1ID: TdxDBGridMaskColumn
      Visible = False
      Width = 61
      BandIndex = 0
      RowIndex = 0
      FieldName = 'ID'
    end
    object dxDBGrid1DicNodoMaster: TdxDBGridMaskColumn
      Caption = 'Sezione'
      Width = 217
      BandIndex = 0
      RowIndex = 0
      FieldName = 'DicNodoMaster'
    end
    object dxDBGrid1Ordine: TdxDBGridSpinColumn
      Alignment = taCenter
      Width = 55
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Ordine'
    end
    object dxDBGrid1Invisibile: TdxDBGridCheckColumn
      Width = 64
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Invisibile'
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
  end
  object QTab: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    Left = 56
    Top = 152
    object QTabID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QTabDicNodoMaster: TStringField
      FieldName = 'DicNodoMaster'
      Size = 30
    end
    object QTabOrdine: TSmallintField
      FieldName = 'Ordine'
    end
    object QTabInvisibile: TBooleanField
      FieldName = 'Invisibile'
    end
  end
  object DsQTab: TDataSource
    DataSet = QTab
    Left = 56
    Top = 184
  end
end
