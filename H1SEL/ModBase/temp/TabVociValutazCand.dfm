object TabVociValutazCandForm: TTabVociValutazCandForm
  Left = 246
  Top = 137
  Width = 545
  Height = 361
  Caption = 'Voci valutazione candidati'
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 537
    Height = 41
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object BNew: TToolbarButton97
      Left = 1
      Top = 1
      Width = 58
      Height = 37
      Caption = 'Nuova voce'
      Opaque = False
      WordWrap = True
      OnClick = BNewClick
    end
    object BDel: TToolbarButton97
      Left = 59
      Top = 1
      Width = 58
      Height = 37
      Caption = 'Elimina voce'
      Opaque = False
      WordWrap = True
      OnClick = BDelClick
    end
    object BCan: TToolbarButton97
      Left = 175
      Top = 1
      Width = 58
      Height = 37
      Caption = 'Annulla modifiche'
      Enabled = False
      Opaque = False
      WordWrap = True
      OnClick = BCanClick
    end
    object BOK: TToolbarButton97
      Left = 117
      Top = 1
      Width = 58
      Height = 37
      Caption = 'Conferma modifiche'
      Enabled = False
      Opaque = False
      WordWrap = True
      OnClick = BOKClick
    end
    object BitBtn1: TBitBtn
      Left = 444
      Top = 4
      Width = 88
      Height = 33
      Anchors = [akTop, akRight]
      Caption = 'Esci'
      TabOrder = 0
      Kind = bkOK
    end
  end
  object dxDBGrid7: TdxDBGrid
    Left = 0
    Top = 41
    Width = 537
    Height = 293
    Bands = <
      item
      end>
    DefaultLayout = True
    HeaderPanelRowCount = 1
    KeyField = 'ID'
    SummaryGroups = <>
    SummarySeparator = ', '
    Align = alClient
    TabOrder = 1
    DataSource = DsQTabVoci
    Filter.Criteria = {00000000}
    OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
    OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoIndicator, edgoUseBitmap]
    object dxDBGrid7ID: TdxDBGridMaskColumn
      Visible = False
      Width = 51
      BandIndex = 0
      RowIndex = 0
      FieldName = 'ID'
    end
    object dxDBGrid7IDRicerca: TdxDBGridMaskColumn
      Visible = False
      Width = 51
      BandIndex = 0
      RowIndex = 0
      FieldName = 'IDRicerca'
    end
    object dxDBGrid7Voce: TdxDBGridMaskColumn
      Width = 173
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Voce'
    end
    object dxDBGrid7Legenda: TdxDBGridMaskColumn
      Width = 348
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Legenda'
    end
  end
  object QTabVoci: TADOQuery
    Connection = Data.DB
    BeforeOpen = QTabVociBeforeOpen
    AfterInsert = QTabVociAfterInsert
    Parameters = <
      item
        Name = 'x'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 2437
      end>
    SQL.Strings = (
      'select *'
      'from EBC_Ricerche_valutaz_voci'
      'where IDRicerca=:x')
    Left = 40
    Top = 80
    object QTabVociID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
      Required = True
    end
    object QTabVociIDRicerca: TIntegerField
      FieldName = 'IDRicerca'
    end
    object QTabVociVoce: TStringField
      FieldName = 'Voce'
      Size = 100
    end
    object QTabVociLegenda: TStringField
      FieldName = 'Legenda'
      Size = 512
    end
  end
  object DsQTabVoci: TDataSource
    DataSet = QTabVoci
    OnStateChange = DsQTabVociStateChange
    Left = 40
    Top = 112
  end
end
