object InsRuoloForm: TInsRuoloForm
  Left = 330
  Top = 220
  HelpContext = 100112
  ActiveControl = dxDBGrid1
  BorderStyle = bsDialog
  Caption = 'Nuovo Ruolo'
  ClientHeight = 426
  ClientWidth = 512
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object BAreeRuoliITA: TSpeedButton
    Left = 230
    Top = 17
    Width = 37
    Height = 22
    GroupIndex = 1
    Down = True
    Caption = 'ITA'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clPurple
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Visible = False
    OnClick = BAreeRuoliITAClick
  end
  object BAreeRuoliENG: TSpeedButton
    Left = 267
    Top = 17
    Width = 37
    Height = 22
    GroupIndex = 1
    Caption = 'ENG'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clPurple
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Visible = False
    OnClick = BAreeRuoliENGClick
  end
  object Label1: TLabel
    Left = 4
    Top = 20
    Width = 56
    Height = 13
    Caption = 'cerca ruolo:'
  end
  object SpeedButton1: TSpeedButton
    Left = 189
    Top = 15
    Width = 29
    Height = 24
    Hint = 'esegui filtro per testo contenuto'
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000120B0000120B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF003FF999903333
      333333377777FFF33333FF9FFFF9993333333F7F3FF7773FF333009F00F03399
      3333777F7737FF773F33FF9FFFF9933393333F73FFF7733373F300F999903333
      393377377777F33337F3FFFFFFF0333339333FF33337F333373300FFFFF03333
      93337733FFF7F3337333FFF00000333333333F377777FF33FF330FF0FF999339
      93337337F3777FF77F33FFF0F993993993333337F77377F77F33FFF003339939
      93333337733F77377FFFFFF03399933999933FF733777337777F000339933339
      93997773377F3FF77F7733333993993993993333377F77377F77333333999339
      9993333333777337777333333333333333333333333333333333}
    NumGlyphs = 2
    ParentShowHint = False
    ShowHint = True
    OnClick = SpeedButton1Click
  end
  object SpeedButton2: TSpeedButton
    Left = 0
    Top = -1
    Width = 20
    Height = 20
    Glyph.Data = {
      AA030000424DAA03000000000000360000002800000011000000110000000100
      1800000000007403000000000000000000000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FF00FFFFFFFFFFFFFFFFFFDEDFDEBDAEA5EFDFD6F7EFE7F7EFE7F7EFE7E7D7CE
      7B696B525152DEDFDEFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFF7F7EF
      E7DEF7F7F7E7DFDECEB6A5DEB6A5E7CFBDEFEFEFF7F7F7DECFC6393031CECFCE
      FFFFFFFFFFFFFFFFFF00FFFFFFF7F7EFF7F7EFF7F7EFD69E7BC66131C6714ADE
      B6A5C66131C66131DEB69CF7F7F7EFDFD6393831FFFFFFFFFFFFFFFFFF00FFFF
      FFF7E7DEF7F7F7CE795AC66129CE6129C69E84FFFFFFDE966BCE6131C65929D6
      9E7BF7F7F7C6B6B5848684FFFFFFFFFFFF00F7EFE7FFFFFFDEAE8CCE6129CE61
      31CE6131CE6939DE8E63CE6129CE6131CE6131C65929E7C7B5F7EFEF524942FF
      FFFFFFFFFF00F7DFD6FFF7F7CE6939CE6931CE6131CE6131CE8663F7E7D6D671
      42CE6131CE6131CE6131CE7952F7FFFFA5968CADAEADFFFFFF00FFEFEFF7DFCE
      CE6131D66931CE6131CE6131C6795AFFFFFFE7A684CE6129CE6131CE6931CE69
      31F7F7EFDECFBD9C9E9CFFFFFF00FFEFEFF7D7C6D66931D66931CE6131CE6131
      CE6131D6AE9CFFFFFFE79E73CE6129CE6931CE6931F7EFEFDECFC69C9E9CFFFF
      FF00FFEFEFFFE7DEE77142DE6939CE6129CE6129CE6131CE6129E7C7BDFFFFF7
      D67139D66939D67142FFF7F7D6C7BDB5B6B5FFFFFF00F7E7DEFFFFFFF79E6BEF
      7942D68E6BEFDFCEDE7952CE6129DEA684FFFFFFDE8E63DE7139E79E73FFFFFF
      A5968CFFFFFFFFFFFF00F7EFE7FFFFFFFFE7CEFF9E63E78E63EFEFEFFFEFE7EF
      BEA5FFF7EFF7EFEFE78652EF8652FFEFDEFFF7EFC6BEBDFFFFFFFFFFFF00FFFF
      FFF7E7DEFFFFFFFFE7C6FFBE84EFBE94EFE7DEE7E7E7EFE7DEFFB68CFF9E6BFF
      DFC6FFFFFFDECFC6FFFFFFFFFFFFFFFFFF00FFFFFFF7F7EFF7E7E7FFFFFFFFFF
      EFFFF7CEFFE7B5FFD7A5FFD79CFFDFB5FFF7EFFFFFFFEFDFDEEFEFEFFFFFFFFF
      FFFFFFFFFF00FFFFFFFFFFFFF7F7EFF7E7D6FFFFF7FFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFF7F7F7EFE7F7F7EFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF
      FFFFFFFFFFFFF7EFE7F7E7DEFFEFE7FFEFEFFFEFE7EFE7DEFFF7F7FFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFF00}
    OnClick = SpeedButton2Click
  end
  object BitBtn1: TBitBtn
    Left = 314
    Top = 7
    Width = 93
    Height = 34
    TabOrder = 0
    OnClick = BitBtn1Click
    Kind = bkOK
  end
  object CBInsComp: TCheckBox
    Left = 5
    Top = 404
    Width = 297
    Height = 17
    Caption = 'attribuisci il ruolo e le relative competenze al nominativo'
    Checked = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clPurple
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    State = cbChecked
    TabOrder = 1
  end
  object BitBtn2: TBitBtn
    Left = 412
    Top = 7
    Width = 93
    Height = 34
    Caption = 'Annulla'
    TabOrder = 2
    Kind = bkCancel
  end
  object dxDBGrid1: TdxDBGrid
    Left = 5
    Top = 48
    Width = 499
    Height = 350
    Bands = <
      item
      end>
    DefaultLayout = True
    HeaderPanelRowCount = 1
    KeyField = 'ID'
    ShowGroupPanel = True
    SummaryGroups = <>
    SummarySeparator = ', '
    TabOrder = 3
    DataSource = DsRuoli
    Filter.Active = True
    Filter.Criteria = {00000000}
    LookAndFeel = lfFlat
    OptionsBehavior = [edgoAutoSearch, edgoAutoSort, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoTabThrough, edgoVertThrough]
    OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
    OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoIndicator, edgoUseBitmap]
    object dxDBGrid1Area: TdxDBGridMaskColumn
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Area'
    end
    object dxDBGrid1Ruolo: TdxDBGridMaskColumn
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Ruolo'
    end
  end
  object ERuolo: TEdit
    Left = 66
    Top = 16
    Width = 119
    Height = 21
    TabOrder = 4
  end
  object TRuoli: TADOQuery
    Active = True
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select Mansioni.ID, IDArea, '
      '       Mansioni.Descrizione Ruolo, Aree.Descrizione Area,'
      
        '       Mansioni.Descrizione_ENG Ruolo_ENG, Aree.Descrizione_ENG ' +
        'Area_ENG '
      'from Mansioni,Aree'
      'where Mansioni.IDArea=Aree.ID'
      'order by Aree.Descrizione, Mansioni.Descrizione')
    Left = 80
    Top = 288
  end
  object DsRuoli: TDataSource
    DataSet = TRuoli
    Left = 80
    Top = 320
  end
end
