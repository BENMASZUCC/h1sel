object SelQualifContrForm: TSelQualifContrForm
  Left = 293
  Top = 108
  BorderStyle = bsDialog
  Caption = 'Selezione qualifica contrattuale'
  ClientHeight = 246
  ClientWidth = 574
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object BitBtn1: TBitBtn
    Left = 475
    Top = 2
    Width = 93
    Height = 37
    Anchors = [akTop, akRight]
    TabOrder = 1
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 475
    Top = 40
    Width = 93
    Height = 37
    Anchors = [akTop, akRight]
    Caption = 'Annulla'
    TabOrder = 2
    Kind = bkCancel
  end
  object DBGrid1: TDBGrid
    Left = 4
    Top = 4
    Width = 465
    Height = 239
    Anchors = [akLeft, akTop, akRight, akBottom]
    DataSource = dsQQualif
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Visible = False
    Columns = <
      item
        Expanded = False
        FieldName = 'TipoContratto'
        Title.Caption = 'Qualifica'
        Width = 123
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Qualifica'
        Title.Caption = 'Livello'
        Width = 129
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Contratto'
        Width = 176
        Visible = True
      end>
  end
  object BitBtn3: TBitBtn
    Left = 475
    Top = 94
    Width = 93
    Height = 37
    Anchors = [akTop, akRight]
    Caption = 'Tab.contratti'
    TabOrder = 3
    OnClick = BitBtn3Click
    Glyph.Data = {
      36050000424D3605000000000000360400002800000010000000100000000100
      08000000000000010000120B0000120B00000001000000010000000000008400
      0000FF00000084848400C6C6C600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00050505050505
      0505050505050505050500000000000000000000000000000005030303030303
      0303030303030303000503040505050505050505050505030005030405050505
      0505050505050503000503040503030303050303030305030005030405050505
      0505050505050503000503040503030303050303030305030005030405050505
      0505050505050503000503040503030303050303030305030005030405050505
      0505050505050503000503040303030303030303030303030005030401020102
      0102010500050003000503040404040404040404040404030005030303030303
      0303030303030303030505050505050505050505050505050505}
  end
  object dxDBGrid1: TdxDBGrid
    Left = 0
    Top = 0
    Width = 473
    Height = 249
    Bands = <
      item
      end>
    DefaultLayout = True
    HeaderPanelRowCount = 1
    KeyField = 'ID'
    ShowGroupPanel = True
    SummaryGroups = <>
    SummarySeparator = ', '
    TabOrder = 4
    DataSource = dsQQualif
    Filter.Active = True
    Filter.Criteria = {00000000}
    OptionsBehavior = [edgoAutoSort, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoTabThrough, edgoVertThrough]
    OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
    Anchors = [akLeft, akTop, akRight, akBottom]
    object dxDBGrid1ID: TdxDBGridMaskColumn
      Visible = False
      Width = 20
      BandIndex = 0
      RowIndex = 0
      FieldName = 'ID'
    end
    object dxDBGrid1TipoContratto: TdxDBGridMaskColumn
      Caption = 'Qualifica'
      Width = 169
      BandIndex = 0
      RowIndex = 0
      FieldName = 'TipoContratto'
    end
    object dxDBGrid1Qualifica: TdxDBGridMaskColumn
      Caption = 'Livello'
      Width = 150
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Qualifica'
    end
    object dxDBGrid1Contratto: TdxDBGridMaskColumn
      Width = 130
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Contratto'
    end
  end
  object QQualif_OLD: TQuery
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'SELECT QualificheContrattuali.ID,TipoContratto,Qualifica'
      'FROM QualificheContrattuali,TipiContratti '
      'WHERE QualificheContrattuali.IDTipoContratto=TipiContratti.ID'
      'order by TipoContratto')
    Left = 48
    Top = 104
    object QQualif_OLDID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.QualificheContrattuali.ID'
    end
    object QQualif_OLDTipoContratto: TStringField
      FieldName = 'TipoContratto'
      Origin = 'EBCDB.TipiContratti.TipoContratto'
      FixedChar = True
    end
    object QQualif_OLDQualifica: TStringField
      FieldName = 'Qualifica'
      Origin = 'EBCDB.QualificheContrattuali.Qualifica'
      FixedChar = True
      Size = 40
    end
  end
  object dsQQualif: TDataSource
    DataSet = QQualif
    Left = 40
    Top = 136
  end
  object QQualif: TADOLinkedQuery
    Active = True
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'SELECT QualificheContrattuali.ID,TipoContratto,Qualifica,contrat' +
        'tinaz.descrizione Contratto'
      'FROM QualificheContrattuali join TipiContratti '
      'on QualificheContrattuali.IDTipoContratto=TipiContratti.ID'
      'left join contrattinaz'
      'on contrattinaz.id=qualifichecontrattuali.idcontrattonaz'
      'order by TipoContratto')
    UseFilter = False
    Left = 48
    Top = 80
    object QQualifID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QQualifTipoContratto: TStringField
      FieldName = 'TipoContratto'
      Size = 200
    end
    object QQualifQualifica: TStringField
      FieldName = 'Qualifica'
      Size = 40
    end
    object QQualifContratto: TStringField
      FieldName = 'Contratto'
      Size = 100
    end
  end
end
