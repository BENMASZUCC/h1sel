object IndirizzoForm: TIndirizzoForm
  Left = 327
  Top = 109
  ActiveControl = CBTipoStrada
  BorderStyle = bsDialog
  Caption = 'Dati Indirizzo (domicilio)'
  ClientHeight = 104
  ClientWidth = 328
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 7
    Top = 9
    Width = 53
    Height = 13
    Caption = 'Tipo strada'
  end
  object Label2: TLabel
    Left = 22
    Top = 33
    Width = 38
    Height = 13
    Caption = 'Indirizzo'
  end
  object Label3: TLabel
    Left = 33
    Top = 59
    Width = 27
    Height = 13
    Caption = 'n�civ.'
  end
  object Label8: TLabel
    Left = 18
    Top = 81
    Width = 42
    Height = 13
    Caption = 'Nazione:'
  end
  object SpeedButton1: TSpeedButton
    Left = 100
    Top = 76
    Width = 26
    Height = 24
    Hint = 'seleziona nazione'
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      04000000000000010000120B0000120B00001000000000000000000000000000
      800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
      5555555555FFFFF555555555544C4C5555555555F777775FF5555554C444C444
      5555555775FF55775F55554C4334444445555575577F55557FF554C4C334C4C4
      335557F5577FF55577F554CCC3334444335557555777F555775FCCCCC333CCC4
      C4457F55F777F555557F4CC33333CCC444C57F577777F5F5557FC4333333C3C4
      CCC57F777777F7FF557F4CC33333333C4C457F577777777F557FCCC33CC4333C
      C4C575F7755F777FF5755CCCCC3333334C5557F5FF777777F7F554C333333333
      CC55575777777777F755553333CC3C33C555557777557577755555533CC4C4CC
      5555555775FFFF77555555555C4CCC5555555555577777555555}
    NumGlyphs = 2
    ParentShowHint = False
    ShowHint = True
    OnClick = SpeedButton1Click
  end
  object BitBtn1: TBitBtn
    Left = 236
    Top = 3
    Width = 89
    Height = 34
    TabOrder = 4
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 236
    Top = 40
    Width = 89
    Height = 34
    Caption = 'Annulla'
    TabOrder = 5
    Kind = bkCancel
  end
  object CBTipoStrada: TComboBox
    Left = 68
    Top = 5
    Width = 67
    Height = 21
    ItemHeight = 13
    TabOrder = 0
    OnDropDown = CBTipoStradaDropDown
    Items.Strings = (
      'Via'
      'Viale'
      'Piazza'
      'Largo'
      'Corso')
  end
  object EIndirizzo: TEdit
    Left = 68
    Top = 29
    Width = 157
    Height = 21
    TabOrder = 1
  end
  object ENumCivico: TEdit
    Left = 68
    Top = 53
    Width = 45
    Height = 21
    TabOrder = 2
  end
  object ENazione: TEdit
    Left = 68
    Top = 77
    Width = 30
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 3
    TabOrder = 3
  end
end
