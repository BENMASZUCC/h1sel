object ArchivioMessForm: TArchivioMessForm
  Left = 298
  Top = 105
  Width = 641
  Height = 375
  Caption = 'Archivio messaggi letti'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 633
    Height = 47
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object BitBtn1: TBitBtn
      Left = 531
      Top = 4
      Width = 98
      Height = 39
      Caption = 'OK-Esci'
      TabOrder = 0
      Kind = bkOK
    end
    object BitBtn2: TBitBtn
      Left = 5
      Top = 4
      Width = 117
      Height = 39
      Caption = 'Elimina messaggio'
      TabOrder = 1
      OnClick = BitBtn2Click
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333000000000
        3333333777777777F3333330F777777033333337F3F3F3F7F3333330F0808070
        33333337F7F7F7F7F3333330F080707033333337F7F7F7F7F3333330F0808070
        33333337F7F7F7F7F3333330F080707033333337F7F7F7F7F3333330F0808070
        333333F7F7F7F7F7F3F33030F080707030333737F7F7F7F7F7333300F0808070
        03333377F7F7F7F773333330F080707033333337F7F7F7F7F333333070707070
        33333337F7F7F7F7FF3333000000000003333377777777777F33330F88877777
        0333337FFFFFFFFF7F3333000000000003333377777777777333333330777033
        3333333337FFF7F3333333333000003333333333377777333333}
      NumGlyphs = 2
    end
  end
  object dxDBGrid1: TdxDBGrid
    Left = 0
    Top = 47
    Width = 633
    Height = 301
    Bands = <
      item
      end>
    DefaultLayout = True
    HeaderPanelRowCount = 1
    KeyField = 'ID'
    SummaryGroups = <>
    SummarySeparator = ', '
    Align = alClient
    TabOrder = 1
    DataSource = DsQMessLetti
    Filter.Criteria = {00000000}
    OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
    OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoUseBitmap]
    object dxDBGrid1ID: TdxDBGridMaskColumn
      Visible = False
      Width = 66
      BandIndex = 0
      RowIndex = 0
      FieldName = 'ID'
    end
    object dxDBGrid1IDUtente: TdxDBGridMaskColumn
      Visible = False
      Width = 66
      BandIndex = 0
      RowIndex = 0
      FieldName = 'IDUtente'
    end
    object dxDBGrid1IDUtenteDa: TdxDBGridMaskColumn
      Visible = False
      Width = 66
      BandIndex = 0
      RowIndex = 0
      FieldName = 'IDUtenteDa'
    end
    object dxDBGrid1DataIns: TdxDBGridDateColumn
      Caption = 'Data Inserimento'
      Sorted = csUp
      Width = 103
      BandIndex = 0
      RowIndex = 0
      FieldName = 'DataIns'
    end
    object dxDBGrid1Testo: TdxDBGridMaskColumn
      Width = 441
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Testo'
    end
    object dxDBGrid1DataLettura: TdxDBGridDateColumn
      Caption = 'Data Lettura'
      Width = 85
      BandIndex = 0
      RowIndex = 0
      FieldName = 'DataLettura'
    end
    object dxDBGrid1IDCandUtente: TdxDBGridMaskColumn
      Visible = False
      Width = 76
      BandIndex = 0
      RowIndex = 0
      FieldName = 'IDCandUtente'
    end
  end
  object QMessLetti_OLD: TQuery
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select * from Promemoria'
      'where IDUtente=:xIDUtente'
      'and DataLettura is not null')
    Left = 104
    Top = 88
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'xIDUtente'
        ParamType = ptUnknown
      end>
    object QMessLetti_OLDID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.Promemoria.ID'
    end
    object QMessLetti_OLDIDUtente: TIntegerField
      FieldName = 'IDUtente'
      Origin = 'EBCDB.Promemoria.IDUtente'
    end
    object QMessLetti_OLDIDUtenteDa: TIntegerField
      FieldName = 'IDUtenteDa'
      Origin = 'EBCDB.Promemoria.IDUtenteDa'
    end
    object QMessLetti_OLDDataIns: TDateTimeField
      FieldName = 'DataIns'
      Origin = 'EBCDB.Promemoria.DataIns'
    end
    object QMessLetti_OLDDataDaLeggere: TDateTimeField
      FieldName = 'DataDaLeggere'
      Origin = 'EBCDB.Promemoria.DataDaLeggere'
    end
    object QMessLetti_OLDTesto: TStringField
      FieldName = 'Testo'
      Origin = 'EBCDB.Promemoria.Testo'
      FixedChar = True
      Size = 80
    end
    object QMessLetti_OLDDataLettura: TDateTimeField
      FieldName = 'DataLettura'
      Origin = 'EBCDB.Promemoria.DataLettura'
    end
    object QMessLetti_OLDEvaso: TBooleanField
      FieldName = 'Evaso'
      Origin = 'EBCDB.Promemoria.Evaso'
    end
    object QMessLetti_OLDIDCandUtente: TIntegerField
      FieldName = 'IDCandUtente'
      Origin = 'EBCDB.Promemoria.IDCandUtente'
    end
  end
  object DsQMessLetti: TDataSource
    DataSet = QMessLetti
    Left = 104
    Top = 120
  end
  object QMessLetti: TADOLinkedQuery
    Connection = Data.DB
    Parameters = <>
    Left = 72
    Top = 88
  end
end
