object InsindirizzoForm: TInsindirizzoForm
  Left = 654
  Top = 301
  Width = 391
  Height = 322
  ActiveControl = EDescriz
  Caption = 'Inserisci indirizzo'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCloseQuery = FormCloseQuery
  PixelsPerInch = 96
  TextHeight = 13
  object Label4: TLabel
    Left = 3
    Top = 1
    Width = 58
    Height = 13
    Caption = 'Descrizione:'
  end
  object GroupBox8: TGroupBox
    Left = 1
    Top = 33
    Width = 288
    Height = 248
    TabOrder = 0
    object LTipoStradaCli: TLabel
      Left = 7
      Top = 9
      Width = 53
      Height = 13
      Caption = 'Tipo strada'
    end
    object Label63: TLabel
      Left = 8
      Top = 92
      Width = 19
      Height = 13
      Caption = 'Cap'
    end
    object Label71: TLabel
      Left = 8
      Top = 46
      Width = 39
      Height = 13
      Caption = 'Comune'
    end
    object Label72: TLabel
      Left = 206
      Top = 46
      Width = 25
      Height = 13
      Caption = 'Prov.'
    end
    object LIndirizzoCli: TLabel
      Left = 64
      Top = 9
      Width = 38
      Height = 13
      Caption = 'Indirizzo'
    end
    object Label113: TLabel
      Left = 107
      Top = 91
      Width = 42
      Height = 13
      Caption = 'Nazione:'
    end
    object BCliSelNaz: TSpeedButton
      Left = 205
      Top = 86
      Width = 27
      Height = 23
      Hint = 'seleziona la nazione'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
        5555555555FFFFF555555555544C4C5555555555F777775FF5555554C444C444
        5555555775FF55775F55554C4334444445555575577F55557FF554C4C334C4C4
        335557F5577FF55577F554CCC3334444335557555777F555775FCCCCC333CCC4
        C4457F55F777F555557F4CC33333CCC444C57F577777F5F5557FC4333333C3C4
        CCC57F777777F7FF557F4CC33333333C4C457F577777777F557FCCC33CC4333C
        C4C575F7755F777FF5755CCCCC3333334C5557F5FF777777F7F554C333333333
        CC55575777777777F755553333CC3C33C555557777557577755555533CC4C4CC
        5555555775FFFF77555555555C4CCC5555555555577777555555}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = BCliSelNazClick
    end
    object SpeedButton12: TSpeedButton
      Left = 77
      Top = 85
      Width = 27
      Height = 23
      Hint = 'seleziona comune da elenco'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        0400000000000001000000000000000000001000000010000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        33333333333333333333333333333333333333333333333333FF333333333333
        3000333333FFFFF3F77733333000003000B033333777773777F733330BFBFB00
        E00033337FFF3377F7773333000FBFB0E000333377733337F7773330FBFBFBF0
        E00033F7FFFF3337F7773000000FBFB0E000377777733337F7770BFBFBFBFBF0
        E00073FFFFFFFF37F777300000000FB0E000377777777337F7773333330BFB00
        000033333373FF77777733333330003333333333333777333333333333333333
        3333333333333333333333333333333333333333333333333333333333333333
        3333333333333333333333333333333333333333333333333333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = SpeedButton12Click
    end
    object Label1: TLabel
      Left = 6
      Top = 155
      Width = 17
      Height = 13
      Caption = 'Fax'
    end
    object Label2: TLabel
      Left = 6
      Top = 118
      Width = 42
      Height = 13
      Caption = 'Telefono'
    end
    object Label3: TLabel
      Left = 207
      Top = 9
      Width = 56
      Height = 13
      Caption = 'Num. civico'
    end
    object Label5: TLabel
      Left = 5
      Top = 196
      Width = 83
      Height = 13
      Caption = 'Tipologia indirizzo'
    end
    object eIndirizzo: TEdit
      Left = 64
      Top = 24
      Width = 134
      Height = 21
      TabOrder = 0
    end
    object eNumCivico: TEdit
      Left = 206
      Top = 23
      Width = 38
      Height = 21
      TabOrder = 1
    end
    object eComune: TEdit
      Left = 9
      Top = 61
      Width = 189
      Height = 21
      TabOrder = 2
    end
    object eProvincia: TEdit
      Left = 205
      Top = 61
      Width = 39
      Height = 21
      TabOrder = 3
    end
    object eCap: TEdit
      Left = 30
      Top = 89
      Width = 44
      Height = 21
      TabOrder = 4
    end
    object cbTipoStrada: TComboBox
      Left = 8
      Top = 24
      Width = 52
      Height = 21
      ItemHeight = 13
      TabOrder = 5
      Items.Strings = (
        'Via'
        'Viale'
        'Piazza'
        'Largo'
        'Corso')
    end
    object eNazione: TEdit
      Left = 150
      Top = 88
      Width = 47
      Height = 21
      TabOrder = 6
    end
    object eTelefono: TEdit
      Left = 5
      Top = 132
      Width = 121
      Height = 21
      TabOrder = 7
    end
    object eFax: TEdit
      Left = 5
      Top = 171
      Width = 121
      Height = 21
      TabOrder = 8
    end
    object DBLKTipoIndirizzo: TdxDBLookupEdit
      Left = 5
      Top = 210
      Width = 121
      TabOrder = 9
      DataField = 'IDTipologia'
      DataSource = IndirizziClienteForm.dsClienteIndirizzi
      ListFieldName = 'Tipo'
      KeyFieldName = 'ID'
      ListSource = IndirizziClienteForm.DSTipoIndirizzi
      LookupKeyValue = Null
    end
  end
  object BitBtn1: TBitBtn
    Left = 294
    Top = 11
    Width = 83
    Height = 33
    TabOrder = 1
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 294
    Top = 43
    Width = 83
    Height = 33
    TabOrder = 2
    OnClick = BitBtn2Click
    Kind = bkCancel
  end
  object EDescriz: TEdit
    Left = 2
    Top = 14
    Width = 287
    Height = 21
    TabOrder = 3
  end
  object QChekTipologia: TADOQuery
    Connection = Data.DB
    Parameters = <
      item
        Name = 'ID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'IDCliente'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select * from ebc_clienteindirizzi'
      'where idtipologia=2'
      'and id <> :ID'
      'and idcliente=:IDCliente')
    Left = 320
    Top = 168
  end
end
