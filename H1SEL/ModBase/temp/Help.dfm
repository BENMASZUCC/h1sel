object HelpForm: THelpForm
  Left = 516
  Top = 223
  Width = 302
  Height = 453
  Caption = 'Help e appunti propri'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object HelpMemo: TDBMemo
    Left = 0
    Top = 97
    Width = 294
    Height = 288
    Align = alTop
    DataField = 'NoteCliente'
    DataSource = dsHelp
    ScrollBars = ssVertical
    TabOrder = 0
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 294
    Height = 97
    Align = alTop
    TabOrder = 1
    object lHelp: TLabel
      Left = 2
      Top = 0
      Width = 56
      Height = 13
      Caption = 'Help on-line'
    end
    object lNoteCli: TLabel
      Left = 2
      Top = 83
      Width = 57
      Height = 13
      Caption = 'Note cliente'
    end
    object Llinkcliente: TLabel
      Left = 0
      Top = 40
      Width = 55
      Height = 13
      Caption = 'Link Cliente'
    end
    object dxDBHyperLinkEdit1: TdxDBHyperLinkEdit
      Left = 0
      Top = 13
      Width = 293
      Color = clBtnFace
      TabOrder = 0
      Anchors = [akLeft, akTop, akRight, akBottom]
      DataField = 'LinkHelp'
      DataSource = dsHelp
      ReadOnly = True
      StoredValues = 64
    end
    object dxDBHyperLinkEdit2: TdxDBHyperLinkEdit
      Left = 0
      Top = 54
      Width = 293
      Cursor = crHandPoint
      TabOrder = 1
      Anchors = [akLeft, akTop, akRight, akBottom]
      DataField = 'LinkCliente'
      DataSource = dsHelp
    end
  end
  object BitBtn1: TBitBtn
    Left = 180
    Top = 397
    Width = 101
    Height = 23
    Anchors = []
    Caption = 'Salva e chiudi'
    TabOrder = 2
    Kind = bkOK
  end
  object qHelp: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'x'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'select *'
      'from tabhelp'
      'where codsezione = :x'
      'and idsoftware = 1')
    Top = 392
    object qHelpID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object qHelpIDSoftware: TIntegerField
      FieldName = 'IDSoftware'
    end
    object qHelpCodSezione: TStringField
      FieldName = 'CodSezione'
      Size = 10
    end
    object qHelpLinkHelp: TStringField
      FieldName = 'LinkHelp'
      Size = 255
    end
    object qHelpNoteCliente: TMemoField
      FieldName = 'NoteCliente'
      BlobType = ftMemo
    end
    object qHelpLinkCliente: TStringField
      FieldName = 'LinkCliente'
      Size = 255
    end
  end
  object dsHelp: TDataSource
    DataSet = qHelp
    Left = 32
    Top = 392
  end
end
