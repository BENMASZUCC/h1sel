�
 TCAMBIASTATOFORM 0�  TPF0TCambiaStatoFormCambiaStatoFormLeftbTopzWidthHeight�ActiveControlDBGrid1Captioncambiamento statoColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrder	PositionpoScreenCenterPixelsPerInch`
TextHeight TPanelPanel1Left Top WidthHeight%AlignalTopEnabledTabOrder  TDBEditDBEdit12LeftTop	Width� HeightColorclYellow	DataFieldCognome
DataSourceData.DsAnagraficaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontReadOnly	TabOrder   TDBEditDBEdit14Left� Top	WidthnHeightColorclYellow	DataFieldNome
DataSourceData.DsAnagraficaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontReadOnly	TabOrder   TPanelPanel2Left Top%WidthHeight!AlignalTopEnabledTabOrder TLabelLabel2Left	Top	Width?HeightCaptionStato attuale:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TDBEditDBEdit1LeftOTopWidth� HeightColorclWhite	DataField	DescStato
DataSourceData.DsAnagraficaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontReadOnly	TabOrder    TPanelPanel3Left Top<WidthHeight,AlignalBottomTabOrder TBitBtnBitBtn1LeftaTop	WidthVHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder KindbkOK  TBitBtnBitBtn2Left� Top	WidthSHeightCaptionAnnullaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderKindbkCancel   TPanelPanel4Left TopFWidthHeight� AlignalClientTabOrder TDBGridDBGrid1LeftTopWidthHeight� AlignalClient
DataSourceData.DsEventiFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameEventoTitle.Font.CharsetDEFAULT_CHARSETTitle.Font.ColorclWindowTextTitle.Font.Height�Title.Font.NameMS Sans SerifTitle.Font.Style Width� Visible	 Expanded	FieldNameAStatoTitle.Captionstato successivoTitle.Font.CharsetDEFAULT_CHARSETTitle.Font.ColorclWindowTextTitle.Font.Height�Title.Font.NameMS Sans SerifTitle.Font.Style WidthkVisible	    TPanelPanel5LeftTopWidthHeightAlignalTop
BevelOuterbvNoneTabOrder TLabelLabel3LeftTopWidthZHeightCaptionEventi possibili:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFont   TPanelPanel6LeftTop� WidthHeight&AlignalBottomTabOrder TLabelLabel1LeftTopWidthHeightCaptionNote:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TEditEdit1Left)TopWidth� HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder      