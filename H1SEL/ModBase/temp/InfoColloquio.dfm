object InfoColloquioForm: TInfoColloquioForm
  Left = 375
  Top = 123
  Width = 623
  Height = 502
  Caption = 'Informativa colloquio'
  Color = clBtnFace
  Constraints.MinHeight = 480
  Constraints.MinWidth = 424
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 615
    Height = 47
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object ToolbarButton971: TToolbarButton97
      Left = 6
      Top = 5
      Width = 113
      Height = 37
      DropdownMenu = PMFunzioniCand
      Caption = 'Tutte le funzioni'
      Glyph.Data = {
        36050000424D3605000000000000360400002800000010000000100000000100
        08000000000000010000120B0000120B00000001000000010000390800007310
        000029180000421800009C18000010210000392100004A210000942100003129
        00005A2900006B2900003131000073310000AD310000943900009C420000BD42
        0000C64200005A2908007B2908006B42080094420800A54A0800A5520800BD52
        0800AD5A08004A630800293910007B391000634A10009C4A10006B5210009C52
        10004A5A1000A5631000AD631000736B100063521800946B1800B56B1800294A
        21009C4A2100846321009C632100A56B2100AD6B2100B56B21009C6329009473
        2900AD732900638C2900634A3100215231004A523100847331009C733100AD73
        3100A57B3100424A3900C6633900636B39007B7B39008C7B39009C7B39009C84
        39007BA539008C844200948C42009C8C4200A58C4200635A4A00427B4A00C684
        4A007B8C4A0084944A009C944A0073AD4A0084AD4A006B7352009C9452007BAD
        5200FFB552005A6B5A007B735A00947B5A004A845A007B945A0094945A008C9C
        5A0084A55A00528C630094946300849C63008CA563009CA563007BAD63008CB5
        630084BD63009CBD6300AD7B6B00739C6B00849C6B0084AD6B00B5BD6B0084C6
        6B006BB573009CB57300ADB57300B59C7B0084A57B006BBD7B00FFBD7B009CC6
        7B009CCE7B00738C84008C8C8400F7AD840094B584009CBD8400B5CE84007384
        8C0063B58C00A5BD8C00A5C68C00BDCE8C0094C69400B5D69400ADDE94009CE7
        94006B9C9C00A5DE9C0094E79C00BDE79C006BA5A5007BCEA5008CCEA5007BD6
        A500ADDEA500F7DEA500BDEFA500B5F7AD009CEFB500BDFFB5008CCEBD00ADE7
        BD00B5EFBD00B5FFBD0094CEC600BDDEC600B5FFCE00ADFFD6009CE7DE00C6EF
        DE00DEFFDE00FFFFDE00FFFFE700C6F7EF00D6FFF700FFFFF700E7FFFF00EFFF
        FF00F7FFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00A3A3A3A3A3A3
        A35E4A37313949A3A3A3A3A3A343A357335A5944382D1A0E2AA3A3A366250822
        51716B5041322F1714A3A3A37B4E212062807C5F453A2E1F0A24A3A3A3696827
        618C8A6B4C402C1D0D19A3A3A37F817D638F9277583F2B131016704F9A9F728E
        838D967E5D3E26070BA38B5C9EA39B6C6A849788653D090274A399959DA3A375
        3060897A56293BA3A3A3A39498A2A15564A31C060334A3A3A3A3A3A3A38673A3
        A3360C150F04A3A3A3A3A3A3542311A3A31B4B45281208A3A3A3A3A3769C523C
        A3428578461801A3A3A3A3A390A06DA3A34D93915D1E00A3A3A3A3A3A38279A3
        A3676F875B0547A3A3A3A3A3A3A3A3A3A3A36E483553A3A3A3A3}
    end
    object BitBtn1: TBitBtn
      Left = 421
      Top = 5
      Width = 94
      Height = 37
      Anchors = [akTop, akRight]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      Kind = bkOK
    end
    object BitBtn2: TBitBtn
      Left = 516
      Top = 5
      Width = 94
      Height = 37
      Anchors = [akTop, akRight]
      Caption = 'Annulla'
      TabOrder = 1
      Kind = bkCancel
    end
  end
  object PCUnico: TPageControl
    Left = 0
    Top = 189
    Width = 615
    Height = 286
    ActivePage = TSMotIns
    Align = alClient
    TabOrder = 1
    object TSDatiRetribuz: TTabSheet
      Caption = 'Dati retributivi'
      object dxDBGrid1: TdxDBGrid
        Left = 0
        Top = 0
        Width = 607
        Height = 258
        Bands = <
          item
          end>
        DefaultLayout = True
        HeaderPanelRowCount = 1
        KeyField = 'IDColonna'
        SummaryGroups = <>
        SummarySeparator = ', '
        Align = alClient
        TabOrder = 0
        DataSource = DsQEspLavRetribuz
        Filter.Criteria = {00000000}
        OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
        OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoUseBitmap]
        object dxDBGrid1ID: TdxDBGridMaskColumn
          Visible = False
          Width = 77
          BandIndex = 0
          RowIndex = 0
          FieldName = 'ID'
        end
        object dxDBGrid1Voce: TdxDBGridMaskColumn
          DisableEditor = True
          Sorted = csUp
          Width = 298
          BandIndex = 0
          RowIndex = 0
          FieldName = 'Voce'
        end
        object dxDBGrid1Flag: TdxDBGridCheckColumn
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Width = 64
          BandIndex = 0
          RowIndex = 0
          FieldName = 'Flag'
          ValueChecked = 'True'
          ValueUnchecked = 'False'
        end
        object dxDBGrid1Valore: TdxDBGridMaskColumn
          Width = 100
          BandIndex = 0
          RowIndex = 0
          FieldName = 'Valore'
        end
      end
    end
    object TSMotIns: TTabSheet
      Caption = 'Motivi di insoddisfazione'
      ImageIndex = 1
      object ASG1: TAdvStringGrid
        Left = 0
        Top = 20
        Width = 607
        Height = 238
        Cursor = crDefault
        Align = alClient
        ColCount = 3
        DefaultRowHeight = 16
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goEditing]
        ScrollBars = ssHorizontal
        TabOrder = 0
        OnRightClickCell = ASG1RightClickCell
        OnCheckBoxClick = ASG1CheckBoxClick
        HintColor = clYellow
        ActiveCellFont.Charset = DEFAULT_CHARSET
        ActiveCellFont.Color = clWindowText
        ActiveCellFont.Height = -11
        ActiveCellFont.Name = 'Tahoma'
        ActiveCellFont.Style = [fsBold]
        CellNode.NodeType = cnFlat
        ColumnHeaders.Strings = (
          'soggetto'
          'E'
          'C')
        ControlLook.DropDownHeader.Font.Charset = DEFAULT_CHARSET
        ControlLook.DropDownHeader.Font.Color = clWindowText
        ControlLook.DropDownHeader.Font.Height = -11
        ControlLook.DropDownHeader.Font.Name = 'Tahoma'
        ControlLook.DropDownHeader.Font.Style = []
        ControlLook.DropDownHeader.Visible = True
        ControlLook.DropDownHeader.Buttons = <>
        ControlLook.DropDownFooter.Font.Charset = DEFAULT_CHARSET
        ControlLook.DropDownFooter.Font.Color = clWindowText
        ControlLook.DropDownFooter.Font.Height = -11
        ControlLook.DropDownFooter.Font.Name = 'MS Sans Serif'
        ControlLook.DropDownFooter.Font.Style = []
        ControlLook.DropDownFooter.Visible = True
        ControlLook.DropDownFooter.Buttons = <>
        EnhRowColMove = False
        Filter = <>
        FilterDropDown.Font.Charset = DEFAULT_CHARSET
        FilterDropDown.Font.Color = clWindowText
        FilterDropDown.Font.Height = -11
        FilterDropDown.Font.Name = 'MS Sans Serif'
        FilterDropDown.Font.Style = []
        FilterDropDownClear = '(All)'
        FixedColWidth = 220
        FixedRowHeight = 16
        FixedFont.Charset = DEFAULT_CHARSET
        FixedFont.Color = clWindowText
        FixedFont.Height = -11
        FixedFont.Name = 'MS Sans Serif'
        FixedFont.Style = []
        FloatFormat = '%.2f'
        MouseActions.DirectEdit = True
        PrintSettings.DateFormat = 'dd/mm/yyyy'
        PrintSettings.Font.Charset = DEFAULT_CHARSET
        PrintSettings.Font.Color = clWindowText
        PrintSettings.Font.Height = -11
        PrintSettings.Font.Name = 'MS Sans Serif'
        PrintSettings.Font.Style = []
        PrintSettings.FixedFont.Charset = DEFAULT_CHARSET
        PrintSettings.FixedFont.Color = clWindowText
        PrintSettings.FixedFont.Height = -11
        PrintSettings.FixedFont.Name = 'MS Sans Serif'
        PrintSettings.FixedFont.Style = []
        PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
        PrintSettings.HeaderFont.Color = clWindowText
        PrintSettings.HeaderFont.Height = -11
        PrintSettings.HeaderFont.Name = 'MS Sans Serif'
        PrintSettings.HeaderFont.Style = []
        PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
        PrintSettings.FooterFont.Color = clWindowText
        PrintSettings.FooterFont.Height = -11
        PrintSettings.FooterFont.Name = 'MS Sans Serif'
        PrintSettings.FooterFont.Style = []
        PrintSettings.Borders = pbNoborder
        PrintSettings.Centered = False
        PrintSettings.PagePrefix = 'page'
        PrintSettings.PageNumSep = '/'
        ScrollWidth = 16
        SearchFooter.FindNextCaption = 'Find &next'
        SearchFooter.FindPrevCaption = 'Find &previous'
        SearchFooter.Font.Charset = DEFAULT_CHARSET
        SearchFooter.Font.Color = clWindowText
        SearchFooter.Font.Height = -11
        SearchFooter.Font.Name = 'MS Sans Serif'
        SearchFooter.Font.Style = []
        SearchFooter.HighLightCaption = 'Highlight'
        SearchFooter.HintClose = 'Close'
        SearchFooter.HintFindNext = 'Find next occurence'
        SearchFooter.HintFindPrev = 'Find previous occurence'
        SearchFooter.HintHighlight = 'Highlight occurences'
        SearchFooter.MatchCaseCaption = 'Match case'
        SelectionColor = clHighlight
        SelectionTextColor = clHighlightText
        URLColor = clBlack
        VAlignment = vtaCenter
        Version = '5.0.0.3'
        WordWrap = False
        ColWidths = (
          220
          15
          14)
        RowHeights = (
          16
          16
          16
          16
          16
          16
          16
          16
          16
          16)
      end
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 607
        Height = 20
        Align = alTop
        BevelOuter = bvLowered
        TabOrder = 1
        object Label16: TLabel
          Left = 4
          Top = 3
          Width = 340
          Height = 13
          Caption = 
            'fare click con il pulsante destro per visualizzare la descrizion' +
            'e dettagliata'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clPurple
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
      end
    end
    object TSCheckList: TTabSheet
      Caption = 'CheckList colloquio'
      ImageIndex = 2
      object dxDBGrid2: TdxDBGrid
        Left = 0
        Top = 0
        Width = 607
        Height = 258
        Bands = <
          item
          end>
        DefaultLayout = True
        HeaderPanelRowCount = 1
        KeyField = 'ID'
        SummaryGroups = <>
        SummarySeparator = ', '
        Align = alClient
        TabOrder = 0
        DataSource = DsQCLColloquioTemp
        Filter.Criteria = {00000000}
        OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
        OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoIndicator, edgoUseBitmap]
        object dxDBGrid2Voce: TdxDBGridMaskColumn
          DisableEditor = True
          Width = 219
          BandIndex = 0
          RowIndex = 0
          FieldName = 'Voce'
        end
        object dxDBGrid2Flag: TdxDBGridCheckColumn
          Width = 59
          BandIndex = 0
          RowIndex = 0
          FieldName = 'Flag'
          ValueChecked = 'True'
          ValueUnchecked = 'False'
        end
        object dxDBGrid2Risposta: TdxDBGridMaskColumn
          Width = 410
          BandIndex = 0
          RowIndex = 0
          FieldName = 'Risposta'
        end
      end
    end
    object TSEsito: TTabSheet
      Caption = 'Esito colloquio e altro'
      ImageIndex = 3
      object Label10: TLabel
        Left = 158
        Top = 6
        Width = 71
        Height = 13
        Caption = 'Data colloquio:'
      end
      object Label11: TLabel
        Left = 4
        Top = 6
        Width = 94
        Height = 13
        Caption = 'Colloquio tenuto da:'
      end
      object Label15: TLabel
        Left = 5
        Top = 115
        Width = 95
        Height = 13
        Caption = 'Motivo eliminazione:'
        Visible = False
      end
      object Panel6: TPanel
        Left = 4
        Top = 51
        Width = 341
        Height = 51
        BevelInner = bvLowered
        TabOrder = 0
        object Label12: TLabel
          Left = 8
          Top = 6
          Width = 23
          Height = 13
          Caption = 'Esito'
        end
        object Label13: TLabel
          Left = 157
          Top = 6
          Width = 37
          Height = 13
          Caption = 'Giudizio'
        end
        object Label14: TLabel
          Left = 282
          Top = 6
          Width = 48
          Height = 13
          Caption = 'Punteggio'
        end
        object CBEsito: TComboBox
          Left = 8
          Top = 22
          Width = 145
          Height = 21
          ItemHeight = 13
          TabOrder = 0
          Text = 'mantenere nella ricerca'
          OnChange = CBEsitoChange
          Items.Strings = (
            'mantenere nella ricerca'
            'altro colloquio'
            'eliminare dalla ricerca')
        end
        object CBGiudizio: TComboBox
          Left = 156
          Top = 21
          Width = 123
          Height = 21
          ItemHeight = 13
          TabOrder = 1
          Items.Strings = (
            'ottimo'
            'buono'
            'discreto'
            'sufficiente'
            'insufficiente'
            'SOTTODIMESIONATO'
            'ALLINEATO'
            'SOVRADIMENSIONATO'
            'STAND BY')
        end
        object SEPunteggio: TSpinEdit
          Left = 283
          Top = 21
          Width = 49
          Height = 22
          MaxValue = 100
          MinValue = 0
          TabOrder = 2
          Value = 0
        end
      end
      object DBGrid1: TDBGrid
        Left = 3
        Top = 22
        Width = 148
        Height = 22
        DataSource = DsSelezionatori
        Options = [dgColumnResize, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Nominativo'
            Width = 105
            Visible = True
          end>
      end
      object DataColloquio: TDateEdit97
        Left = 158
        Top = 22
        Width = 104
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        ColorCalendar.ColorValid = clBlue
        DayNames.Monday = 'lu'
        DayNames.Tuesday = 'ma'
        DayNames.Wednesday = 'me'
        DayNames.Thursday = 'gi'
        DayNames.Friday = 've'
        DayNames.Saturday = 'sa'
        DayNames.Sunday = 'do'
        MonthNames.January = 'gennaio'
        MonthNames.February = 'febbraio'
        MonthNames.March = 'marzo'
        MonthNames.April = 'aprile'
        MonthNames.May = 'maggio'
        MonthNames.June = 'giugno'
        MonthNames.July = 'luglio'
        MonthNames.August = 'agosto'
        MonthNames.September = 'settembre'
        MonthNames.October = 'ottobre'
        MonthNames.November = 'novembre'
        MonthNames.December = 'dicembre'
        Options = [doButtonTabStop, doCanPopup, doIsMasked, doShowCancel, doShowToday]
      end
      object CBFlagAccPres: TCheckBox
        Left = 5
        Top = 112
        Width = 588
        Height = 17
        Caption = 'Accetta di essere presentato all'#39'azienda '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clPurple
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 3
      end
      object eMotivoElim: TEdit
        Left = 5
        Top = 129
        Width = 597
        Height = 21
        TabOrder = 4
        Visible = False
      end
      object GBColloquioAzienda: TGroupBox
        Left = 4
        Top = 157
        Width = 298
        Height = 66
        Hint = '<'
        Caption = 'Colloquio presso il cliente'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 5
        object ToolbarButton9711: TToolbarButton97
          Left = 266
          Top = 37
          Width = 24
          Height = 22
          Hint = 'Tabella dettagli colloquio presso il cliente'
          Glyph.Data = {
            36050000424D3605000000000000360400002800000010000000100000000100
            08000000000000010000120B0000120B00000001000000010000000000008400
            000084848400C6C6C600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00040404040404
            0404040404040404040404040404040404040404040404040404000000000000
            0000000000000000000002040404040404040404040404040400020403030303
            0303030303030303040002040304030404040302020202030400020403030303
            0303030303030303040002040304030404040304040404030400020403030303
            0303030303030303040002040304030404040304040404030400020403030303
            0303030303030303040002040404040404040404040404040400020301030101
            0101010101010101010002030303030303030303030303030300020202020202
            0202020202020202020004040404040404040404040404040404}
          Opaque = False
          ParentShowHint = False
          ShowHint = True
          WordWrap = True
          OnClick = ToolbarButton9711Click
        end
        object CBColloquioCliente: TCheckBox
          Left = 7
          Top = 17
          Width = 138
          Height = 17
          Caption = 'Colloquio presso cliente'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          OnClick = CBColloquioClienteClick
        end
        object CBDettCollCliente: TComboBox
          Left = 7
          Top = 37
          Width = 255
          Height = 21
          Color = clBtnFace
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ItemHeight = 13
          ParentFont = False
          TabOrder = 1
        end
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 80
    Width = 615
    Height = 109
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 2
    object Label1: TLabel
      Left = 6
      Top = 25
      Width = 38
      Height = 13
      Caption = 'Azienda'
      FocusControl = DBEdit1
    end
    object Label2: TLabel
      Left = 231
      Top = 25
      Width = 72
      Height = 13
      Caption = 'Attivita azienda'
      FocusControl = DBEdit2
    end
    object Label3: TLabel
      Left = 62
      Top = 66
      Width = 25
      Height = 13
      Caption = 'Anno'
      FocusControl = DBEdit3
    end
    object Label4: TLabel
      Left = 31
      Top = 66
      Width = 26
      Height = 13
      Caption = 'Mese'
      FocusControl = DBEdit4
    end
    object Label5: TLabel
      Left = 6
      Top = 85
      Width = 16
      Height = 13
      Caption = 'Dal'
    end
    object Label6: TLabel
      Left = 408
      Top = 26
      Width = 86
      Height = 13
      Caption = 'Ruolo nell'#39'azienda'
    end
    object Label7: TLabel
      Left = 231
      Top = 66
      Width = 91
      Height = 13
      Caption = 'Contratto nazionale'
      Color = clBtnFace
      ParentColor = False
    end
    object Label8: TLabel
      Left = 408
      Top = 66
      Width = 83
      Height = 13
      Caption = 'Qualifica e Livello'
      Color = clBtnFace
      ParentColor = False
    end
    object Panel12: TPanel
      Left = 1
      Top = 1
      Width = 613
      Height = 21
      Align = alTop
      Alignment = taLeftJustify
      Caption = '  Esperienza lavorativa attuale'
      Color = clGray
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
    end
    object DBEdit1: TDBEdit
      Left = 6
      Top = 41
      Width = 222
      Height = 21
      Color = clAqua
      DataField = 'Azienda'
      DataSource = DsQEspLavAttuale
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 5
    end
    object DBEdit2: TDBEdit
      Left = 231
      Top = 41
      Width = 175
      Height = 21
      DataField = 'Attivita'
      DataSource = DsQEspLavAttuale
      ReadOnly = True
      TabOrder = 6
    end
    object DBEdit3: TDBEdit
      Left = 61
      Top = 81
      Width = 41
      Height = 21
      DataField = 'AnnoDal'
      DataSource = DsQEspLavAttuale
      TabOrder = 2
    end
    object DBEdit4: TDBEdit
      Left = 30
      Top = 81
      Width = 28
      Height = 21
      DataField = 'MeseDal'
      DataSource = DsQEspLavAttuale
      TabOrder = 1
    end
    object dxDBExtLookupEdit1: TdxDBExtLookupEdit
      Left = 408
      Top = 41
      Width = 199
      TabOrder = 0
      DataField = 'Ruolo'
      DataSource = DsQEspLavAttuale
      DBGridLayout = dxDBGridLayoutListRuoli
    end
    object DBEdit5: TDBEdit
      Left = 231
      Top = 81
      Width = 174
      Height = 21
      DataField = 'ContrattoNaz'
      DataSource = DsQEspLavAttuale
      ReadOnly = True
      TabOrder = 7
    end
    object dxDBExtLookupEdit2: TdxDBExtLookupEdit
      Left = 408
      Top = 81
      Width = 200
      TabOrder = 3
      DataField = 'TipoQualif'
      DataSource = DsQEspLavAttuale
      DBGridLayout = dxDBGridLayoutList1QualifContr
    end
  end
  object Panel5: TPanel
    Left = 0
    Top = 47
    Width = 615
    Height = 33
    Align = alTop
    BevelOuter = bvLowered
    Enabled = False
    TabOrder = 3
    object Label9: TLabel
      Left = 6
      Top = 9
      Width = 51
      Height = 13
      Caption = 'Candidato:'
    end
    object ECogn: TEdit
      Left = 61
      Top = 6
      Width = 164
      Height = 21
      Color = clYellow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object ENome: TEdit
      Left = 227
      Top = 6
      Width = 149
      Height = 21
      Color = clYellow
      TabOrder = 1
    end
  end
  object DsQEspLavRetribuz: TDataSource
    DataSet = QEspLavRetribuz
    Left = 24
    Top = 416
  end
  object DsQEspLavAttuale: TDataSource
    DataSet = QEspLavAttuale
    Left = 96
    Top = 111
  end
  object QEspLavAttuale: TADOLinkedQuery
    Connection = Data.DB
    CursorType = ctStatic
    AfterPost = QEspLavAttualeAfterPost
    Parameters = <
      item
        Name = 'xIDAnag:'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select EBC_Clienti.Descrizione Azienda, EBC_Attivita.Attivita,'
      '       EsperienzeLavorative.ID,EsperienzeLavorative.IDMansione,'
      '       AnnoDal,MeseDal, EsperienzeLavorative.IDQualifContr'
      'from EsperienzeLavorative, EBC_Clienti, EBC_Attivita'
      'where EsperienzeLavorative.IDAzienda *= EBC_Clienti.ID'
      '  and EsperienzeLavorative.IDSettore *= EBC_Attivita.ID'
      'and IDAnagrafica=:xIDAnag:'
      'and attuale=1'
      ''
      '')
    UseFilter = False
    Left = 64
    Top = 112
    object QEspLavAttualeAzienda: TStringField
      FieldName = 'Azienda'
      Size = 50
    end
    object QEspLavAttualeAttivita: TStringField
      FieldName = 'Attivita'
      Size = 50
    end
    object QEspLavAttualeID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QEspLavAttualeIDMansione: TIntegerField
      FieldName = 'IDMansione'
    end
    object QEspLavAttualeAnnoDal: TSmallintField
      FieldName = 'AnnoDal'
    end
    object QEspLavAttualeMeseDal: TSmallintField
      FieldName = 'MeseDal'
    end
    object QEspLavAttualeRuolo: TStringField
      FieldKind = fkLookup
      FieldName = 'Ruolo'
      LookupDataSet = QRuoli
      LookupKeyFields = 'ID'
      LookupResultField = 'Ruolo'
      KeyFields = 'IDMansione'
      Size = 40
      Lookup = True
    end
    object QEspLavAttualeIDQualifContr: TIntegerField
      FieldName = 'IDQualifContr'
    end
    object QEspLavAttualeContrattoNaz: TStringField
      FieldKind = fkLookup
      FieldName = 'ContrattoNaz'
      LookupDataSet = QQualifContr
      LookupKeyFields = 'ID'
      LookupResultField = 'ContrattoNaz'
      KeyFields = 'IDQualifContr'
      Size = 40
      Lookup = True
    end
    object QEspLavAttualeTipoQualif: TStringField
      FieldKind = fkLookup
      FieldName = 'TipoQualif'
      LookupDataSet = QQualifContr
      LookupKeyFields = 'ID'
      LookupResultField = 'TipoQualif'
      KeyFields = 'IDQualifContr'
      Size = 70
      Lookup = True
    end
  end
  object DsQRuoli: TDataSource
    DataSet = QRuoli
    Left = 552
    Top = 111
  end
  object QRuoli: TADOLinkedQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select Mansioni.ID,Mansioni.Descrizione Ruolo, '
      '          Aree.ID IDArea, Aree.Descrizione Area'
      'from Mansioni, Aree'
      'where Mansioni.IDArea = Aree.ID')
    UseFilter = False
    Left = 520
    Top = 111
    object QRuoliID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QRuoliRuolo: TStringField
      FieldName = 'Ruolo'
      Size = 40
    end
    object QRuoliArea: TStringField
      FieldName = 'Area'
      Size = 30
    end
    object QRuoliIDArea: TAutoIncField
      FieldName = 'IDArea'
      ReadOnly = True
    end
  end
  object dxDBGridLayoutList1: TdxDBGridLayoutList
    Left = 552
    Top = 400
    object dxDBGridLayoutListRuoli: TdxDBGridLayout
      Data = {
        94020000545046301054647844424772696457726170706572000542616E6473
        0E0100000D44656661756C744C61796F7574091348656164657250616E656C52
        6F77436F756E740201084B65794669656C64060249440D53756D6D6172794772
        6F7570730E001053756D6D617279536570617261746F7206022C200A44617461
        536F75726365071A496E666F436F6C6C6F7175696F466F726D2E44735152756F
        6C690F46696C7465722E43726974657269610A04000000000000000F4F707469
        6F6E734265686176696F720B0E6564676F4175746F5365617263680C6564676F
        4175746F536F72740E6564676F447261675363726F6C6C136564676F456E7465
        7253686F77456469746F72136564676F496D6D656469617465456469746F720E
        6564676F5461625468726F7567680F6564676F566572745468726F7567680009
        4F7074696F6E7344420B106564676F43616E63656C4F6E457869740D6564676F
        43616E44656C6574650D6564676F43616E496E73657274116564676F43616E4E
        617669676174696F6E116564676F436F6E6669726D44656C657465126564676F
        4C6F6164416C6C5265636F726473106564676F557365426F6F6B6D61726B7300
        0B4F7074696F6E73566965770B0D6564676F4175746F5769647468136564676F
        42616E6448656164657257696474680D6564676F5573654269746D6170000013
        5464784442477269644D61736B436F6C756D6E044172656105576964746803AD
        000942616E64496E646578020008526F77496E6465780200094669656C644E61
        6D650604417265610000135464784442477269644D61736B436F6C756D6E0552
        756F6C6F05576964746803C1000942616E64496E646578020008526F77496E64
        65780200094669656C644E616D65060552756F6C6F000000}
    end
    object dxDBGridLayoutList1QualifContr: TdxDBGridLayout
      Data = {
        63030000545046301054647844424772696457726170706572000542616E6473
        0E0100000D44656661756C744C61796F7574091348656164657250616E656C52
        6F77436F756E740201084B65794669656C64060249440D53756D6D6172794772
        6F7570730E001053756D6D617279536570617261746F7206022C200A44617461
        536F757263650720496E666F436F6C6C6F7175696F466F726D2E447351517561
        6C6966436F6E74720D46696C7465722E416374697665090F46696C7465722E43
        726974657269610A04000000000000000F4F7074696F6E734265686176696F72
        0B0E6564676F4175746F5365617263680C6564676F4175746F536F72740E6564
        676F447261675363726F6C6C136564676F456E74657253686F77456469746F72
        136564676F496D6D656469617465456469746F720E6564676F5461625468726F
        7567680F6564676F566572745468726F75676800094F7074696F6E7344420B10
        6564676F43616E63656C4F6E457869740D6564676F43616E44656C6574650D65
        64676F43616E496E73657274116564676F43616E4E617669676174696F6E1165
        64676F436F6E6669726D44656C657465126564676F4C6F6164416C6C5265636F
        726473106564676F557365426F6F6B6D61726B73000B4F7074696F6E73566965
        770B0D6564676F4175746F5769647468136564676F42616E6448656164657257
        696474680D6564676F496E64696361746F720D6564676F5573654269746D6170
        0000135464784442477269644D61736B436F6C756D6E0C436F6E74726174746F
        4E617A0743617074696F6E0613436F6E74726174746F206E617A696F6E616C65
        055769647468038F000942616E64496E646578020008526F77496E6465780200
        094669656C644E616D65060C436F6E74726174746F4E617A0000135464784442
        477269644D61736B436F6C756D6E0D5469706F436F6E74726174746F07436170
        74696F6E06115469706F20646920636F6E74726174746F055769647468027A09
        42616E64496E646578020008526F77496E6465780200094669656C644E616D65
        060D5469706F436F6E74726174746F0000135464784442477269644D61736B43
        6F6C756D6E095175616C696669636105576964746803B3000942616E64496E64
        6578020008526F77496E6465780200094669656C644E616D6506095175616C69
        66696361000000}
    end
  end
  object QGriglia: TADOLinkedQuery
    Connection = Data.DB
    Parameters = <>
    OriginalSQL.Strings = (
      'select EspLavMotInsModel.*,EspLavMotIns.Valore'
      'from EspLavMotIns,EspLavMotInsModel'
      'where EspLavMotInsModel.ID *= EspLavMotIns.IDVoce'
      'and IDEspLav=:xIDEspLav:'
      'order by EspLavMotInsModel.ID')
    UseFilter = False
    Left = 152
    Top = 440
  end
  object QUpd: TADOLinkedQuery
    Connection = Data.DB
    Parameters = <>
    UseFilter = False
    Left = 184
    Top = 440
  end
  object Q: TADOLinkedQuery
    Connection = Data.DB
    Parameters = <>
    UseFilter = False
    Left = 216
    Top = 440
  end
  object QEspLavDB: TADOLinkedQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'select distinct EspLavRetribuzioni.ID,NomeColonna Voce,Valore,Fl' +
        'ag,'
      'EspLavRetribColonne.ID IDColonna'
      ' from EspLavRetribuzioni, EspLavRetribColonne '
      'where EspLavRetribColonne.ID *= EspLavRetribuzioni.IDColonna'
      '  and EspLavRetribuzioni.IDEspLav=1')
    UseFilter = False
    Left = 60
    Top = 401
    object QEspLavDBID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QEspLavDBVoce: TStringField
      FieldName = 'Voce'
      FixedChar = True
      Size = 30
    end
    object QEspLavDBValore: TFloatField
      FieldName = 'Valore'
    end
    object QEspLavDBFlag: TBooleanField
      FieldName = 'Flag'
    end
    object QEspLavDBIDColonna: TAutoIncField
      FieldName = 'IDColonna'
      ReadOnly = True
    end
  end
  object DsQCLColloquioTemp: TDataSource
    DataSet = QCLColloquioTemp
    Left = 316
    Top = 409
  end
  object TSelezionatori: TADOLinkedTable
    Connection = Data.DB
    CursorType = ctStatic
    TableName = 'Users'
    Left = 352
    Top = 376
  end
  object DsSelezionatori: TDataSource
    DataSet = TSelezionatori
    Left = 352
    Top = 408
  end
  object PMFunzioniCand: TPopupMenu
    Images = ImageList1
    Left = 104
    Top = 12
    object Schedacandidato1: TMenuItem
      Caption = 'Scheda candidato'
      ImageIndex = 0
      OnClick = Schedacandidato1Click
    end
    object Curriculum1: TMenuItem
      Caption = 'Curriculum'
      ImageIndex = 1
      OnClick = Curriculum1Click
    end
    object Documenti1: TMenuItem
      Caption = 'Documenti'
      ImageIndex = 2
      OnClick = Documenti1Click
    end
    object AppuntiWord1: TMenuItem
      Caption = 'Appunti Word'
      ImageIndex = 3
      OnClick = AppuntiWord1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Valutazionecandidato1: TMenuItem
      Caption = 'Valutazione candidato'
      OnClick = Valutazionecandidato1Click
    end
    object NuovaEsperienzalavorativaattuale1: TMenuItem
      Caption = 'Nuova Esperienza lavorativa attuale'
      OnClick = NuovaEsperienzalavorativaattuale1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Tabella1: TMenuItem
      Caption = 'Tabella voci retributive'
      ImageIndex = 4
      OnClick = Tabella1Click
    end
    object Tabellaqualificheecontratti1: TMenuItem
      Caption = 'Tabella qualifiche e contratti'
      ImageIndex = 4
      OnClick = Tabellaqualificheecontratti1Click
    end
    object Tabellamoticidiinsoddisfazione1: TMenuItem
      Caption = 'Tabella motivi di insoddisfazione'
      ImageIndex = 4
      OnClick = Tabellamoticidiinsoddisfazione1Click
    end
  end
  object ImageList1: TImageList
    Left = 136
    Top = 13
    Bitmap = {
      494C010105000900040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000003000000001001000000000000018
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001042104210421042104210421042
      1042104210421042104210421042000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001042186300000000000000000000
      0000000000000000000000001042000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001042186300000000000000000000
      0000000000000000000000001042000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001042186300001042104210421042
      0000104210421042104200001042000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001042186300000000000000000000
      0000000000000000000000001042000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001042186300001042104210421042
      0000104210421042104200001042000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001042186300000000000000000000
      0000000000000000000000001042000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001042186300001042104210421042
      0000104210421042104200001042000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001042186300000000000000000000
      0000000000000000000000001042000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001042186310421042104210421042
      1042104210421042104210421042000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001042186310001F0010001F001000
      1F00100000000000000000001042000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001042186318631863186318631863
      1863186318631863186318631042000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001042104210421042104210421042
      1042104210421042104210421042104200000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E07FE07FE07FE07FE07F
      E07FE07FE07FE07FE07FE07FE07F000000000000000000001042000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E07FE07F00000000E07F
      E07FE07FE07FE07FE07FE07FE07F000000000000000000001042186318631863
      1863186318631863186318631863186300000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000000000000000000000000000
      00000000000000000000000000000000000000000000E07FE07F0000E07FE07F
      E07FE07FE07FE07F0000E07FE07F000000000000000000001042FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F186300000000FF7F00000000FF7F00000000
      FF7F000000000000FF7F00000000FF7F00000000000000000000000000000000
      00000000000000000000000000000000000000000000E07FE07FE07F0000E07F
      E07FE07FE07F000000000000E07F000000000000000000001042FF7FFF7FFF7F
      FF7FFF7F1863186318631863FF7F186300000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000000000000000000000000000
      00000000000000000000000000000000000000000000E07FE07FE07F00000000
      E07FE07F00000000E07FE07FE07F000000000000000000001042FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F186300000000FF7F00000000FF7F00000000
      FF7FFF7F00000000000000000000FF7F0000000000000000EF3D000000000000
      EF3D0000E07FEF3D0000000000000000000000000000E07FE07FE07FE07F0000
      E07F00000000E07FE07FE07FE07F000000000000000000001042FF7F18631863
      186318631863186318631863FF7F186300000000FF7FFF03FF03FF03FF03FF03
      FF7FFF7F0000FF7F007CFF7F1F00FF7F0000000000000000EF3DEF3DEF3DEF3D
      EF3D00000000E07F0000000000000000000000000000E07FE07FE07FE07F0000
      00000000E07FE07FE07FE07FE07F000000000000000000001042FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7F186300000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7F0000FF7F007CFF7F1F00FF7F000000000000EF3DFF7FF75EFF7FF75E
      FF7FEF3D000000000000000000000000000000000000FF03FF03FF03FF030000
      00000000FF03FF03FF03FF03FF03000000000000000000000000104200000000
      FF7FFF7F1863186318631863FF7F186300000000FF7F0000000000000000FF7F
      FF7FFF7F0000FF7FFF7FFF7F1F00FF7F0000EF3DEF3DFF7FF75EFF7F007CFF7F
      F75EFF7FEF3DEF3D0000000000000000000000000000FF03FF030000FF03F75E
      0000000000000000FF03FF03FF030000000000000000FF03FF030000FF03FF03
      0000FF7F18631863FF7F1863FF7F186300000000FF7F0000000000000000FF7F
      FF7FFF7F0000FF7FFF7FFF7FFF7FFF7F00000000EF3DF75EFF7FF75E007CF75E
      FF7FF75EEF3D00000000000000000000000000000000FF03FF03FF030000F75E
      00000000FF03FF03FF03FF03FF030000000000000000FF03FF030000FF03FF03
      0000FF7F18631863FF7F1863FF7F186300000000FF7FFF7FFF7FFF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F00000000EF3DFF7F007C007C007C007C
      007CFF7FEF3D00000000000000000000000000000000FF03FF03FF03FF030000
      00000000FF03FF03FF03FF03FF030000000000000000FF030000FF03FF030000
      FF0300001863FF7FFF7F1863FF7F1863000000001F001F001F001F001F001F00
      1F001F001F001F001F001F001F001F0000000000EF3DF75EFF7FF75E007CF75E
      FF7FF75EEF3D00000000000000000000000000000000FF03FF03FF03FF03FF03
      00000000FF03FF03FF03FF03FF030000000000000000FF0300000000FF030000
      0000FF030000186318631863FF7F186300000000F75EF75E1F001F001F001F00
      1F001F001F001F001F001F00F75EF75E0000EF3DEF3DFF7FF75EFF7F007CFF7F
      F75EFF7FEF3DEF3D0000000000000000000000000000FF03FF03FF03FF03FF03
      FF0300000000FF03FF03FF03FF03000000000000FF03FF03FF030000FF030000
      FF03FF03FF030000FF7F00000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EF3DFF7FF75EFF7FF75E
      FF7FEF3D000000000000000000000000000000000000FF03FF03FF03FF03FF03
      FF0300000000FF03FF03FF03FF03000000000000000000000000000000000000
      0000000000000000FF7F1042FF7F000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000EF3DEF3DEF3DEF3D
      EF3D0000000000000000000000000000000000000000FF03FF03FF03FF03FF03
      FF03FF03FF03FF03FF03FF03FF03000000000000000000001042FF7FFF7FFF7F
      FF7FFF7FFF7FFF7FFF7F10420000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000EF3D000000000000
      EF3D0000000000000000000000000000000000000000FF03FF03FF03FF03FF03
      FF03FF03FF03FF03FF03FF03FF03000000000000000000001042104210421042
      104210421042104210421042000000000000424D3E000000000000003E000000
      2800000040000000300000000100010000000000800100000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FFFF0000000000000001000000000000
      00010000000000003FF90000000000003FF90000000000002109000000000000
      3FF900000000000021090000000000003FF90000000000002109000000000000
      3FF9000000000000000100000000000000510000000000000001000000000000
      0001000000000000FFFF000000000000FFFFFFFDC003E0000000FFF8C003E000
      0000FFF1C003E0000000FFE3C003E0000000FFC7C003E0000000E08FC003E000
      0000C01FC003E0000000803FC00380000000001FC00380000000001FC0038000
      0000001FC00380000000001FC00380000000001FC00300000000803FC0030001
      FFFFC07FC003E003FFFFE0FFC003E00700000000000000000000000000000000
      000000000000}
  end
  object QQualifContr: TADOLinkedQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      
        'SELECT QualificheContrattuali.ID,TipoContratto,Qualifica,Descriz' +
        'ione ContrattoNaz,'
      '       TipoContratto+'#39' - '#39'+Qualifica TipoQualif'
      'FROM QualificheContrattuali,TipiContratti, ContrattiNaz'
      'WHERE QualificheContrattuali.IDTipoContratto = TipiContratti.ID'
      '  and QualificheContrattuali.IDContrattoNaz *= ContrattiNaz.ID'
      'order by TipoContratto')
    UseFilter = False
    Left = 521
    Top = 156
    object QQualifContrID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QQualifContrTipoContratto: TStringField
      FieldName = 'TipoContratto'
    end
    object QQualifContrQualifica: TStringField
      FieldName = 'Qualifica'
      Size = 40
    end
    object QQualifContrContrattoNaz: TStringField
      FieldName = 'ContrattoNaz'
      Size = 40
    end
    object QQualifContrTipoQualif: TStringField
      FieldName = 'TipoQualif'
      ReadOnly = True
      Size = 61
    end
  end
  object DsQQualifContr: TDataSource
    DataSet = QQualifContr
    Left = 554
    Top = 156
  end
  object QEspLavRetribuz: TkbmMemTable
    DesignActivation = True
    AttachedAutoRefresh = True
    AttachMaxCount = 1
    FieldDefs = <
      item
        Name = 'ID'
        DataType = ftInteger
      end
      item
        Name = 'Voce'
        DataType = ftString
        Size = 30
      end
      item
        Name = 'Flag'
        DataType = ftBoolean
      end
      item
        Name = 'Valore'
        DataType = ftFloat
      end
      item
        Name = 'IDColonna'
        DataType = ftInteger
      end>
    IndexDefs = <>
    SortOptions = []
    PersistentBackup = False
    ProgressFlags = [mtpcLoad, mtpcSave, mtpcCopy]
    FilterOptions = []
    Version = '3.01'
    LanguageID = 0
    SortID = 0
    SubLanguageID = 1
    LocaleID = 1024
    Left = 25
    Top = 384
    object QEspLavRetribuzID: TIntegerField
      FieldName = 'ID'
    end
    object QEspLavRetribuzVoce: TStringField
      FieldName = 'Voce'
      Size = 30
    end
    object QEspLavRetribuzFlag: TBooleanField
      FieldName = 'Flag'
    end
    object QEspLavRetribuzValore: TFloatField
      FieldName = 'Valore'
    end
    object QEspLavRetribuzIDColonna: TIntegerField
      FieldName = 'IDColonna'
    end
  end
  object QCLColloquioTemp: TkbmMemTable
    DesignActivation = True
    AttachedAutoRefresh = True
    AttachMaxCount = 1
    FieldDefs = <>
    IndexDefs = <>
    SortOptions = []
    PersistentBackup = False
    ProgressFlags = [mtpcLoad, mtpcSave, mtpcCopy]
    FilterOptions = []
    Version = '3.01'
    LanguageID = 0
    SortID = 0
    SubLanguageID = 1
    LocaleID = 1024
    Left = 316
    Top = 376
    object QCLColloquioTempID: TIntegerField
      FieldName = 'ID'
    end
    object QCLColloquioTempIDCommessaVoce: TIntegerField
      FieldName = 'IDCommessaVoce'
    end
    object QCLColloquioTempVoce: TStringField
      FieldName = 'Voce'
      Size = 100
    end
    object QCLColloquioTempFlag: TBooleanField
      FieldName = 'Flag'
    end
    object QCLColloquioTempRisposta: TStringField
      FieldName = 'Risposta'
      Size = 1024
    end
  end
  object Q2: TADOLinkedQuery
    Connection = Data.DB
    Parameters = <>
    UseFilter = False
    Left = 252
    Top = 441
  end
  object Q6: TADOQuery
    Connection = Data.DB
    Parameters = <>
    Left = 148
    Top = 357
  end
end
