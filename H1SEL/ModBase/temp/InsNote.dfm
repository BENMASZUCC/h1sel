object InsNoteForm: TInsNoteForm
  Left = 310
  Top = 125
  BorderStyle = bsDialog
  Caption = 'InsNoteForm'
  ClientHeight = 212
  ClientWidth = 349
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 4
    Top = 46
    Width = 132
    Height = 13
    Caption = 'Inserisci qui eventuali note:'
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 349
    Height = 41
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object BitBtn1: TBitBtn
      Left = 174
      Top = 4
      Width = 84
      Height = 32
      Anchors = [akTop, akRight]
      TabOrder = 0
      Kind = bkOK
    end
    object BitBtn2: TBitBtn
      Left = 260
      Top = 4
      Width = 85
      Height = 32
      Anchors = [akTop, akRight]
      Caption = 'Annulla'
      TabOrder = 1
      Kind = bkCancel
    end
  end
  object MNote: TMemo
    Left = 4
    Top = 63
    Width = 340
    Height = 144
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 1
  end
end
