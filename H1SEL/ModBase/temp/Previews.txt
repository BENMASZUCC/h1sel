object fPreviews: TfPreviews
  Left = 188
  Top = 139
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Previews'
  ClientHeight = 365
  ClientWidth = 512
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 1
    Width = 37
    Height = 13
    Caption = 'Source:'
  end
  object Label2: TLabel
    Left = 231
    Top = 1
    Width = 33
    Height = 13
    Caption = 'Result:'
  end
  object Panel1: TPanel
    Left = 8
    Top = 17
    Width = 193
    Height = 137
    BevelOuter = bvLowered
    Caption = 'Panel1'
    TabOrder = 0
    object ImageEn1: TImageEn
      Left = 1
      Top = 1
      Width = 191
      Height = 135
      Cursor = 1780
      ScrollBars = True
      OnViewChange = ImageEn1ViewChange
      Align = alClient
    end
  end
  object Panel2: TPanel
    Left = 232
    Top = 17
    Width = 175
    Height = 120
    BevelOuter = bvLowered
    Caption = 'Panel2'
    TabOrder = 1
    object ImageEn2: TImageEn
      Left = 1
      Top = 1
      Width = 173
      Height = 118
      Cursor = 1780
      Align = alClient
      OnMouseDown = ImageEn2MouseUp
      OnMouseUp = ImageEn2MouseUp
    end
  end
  object Button1: TButton
    Left = 416
    Top = 15
    Width = 75
    Height = 23
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 416
    Top = 44
    Width = 75
    Height = 23
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 161
    Width = 76
    Height = 199
    Caption = ' Zoom 100% '
    TabOrder = 4
    object TrackBar4: TTrackBar
      Left = 16
      Top = 16
      Width = 41
      Height = 151
      Max = 1000
      Min = 1
      Orientation = trVertical
      Frequency = 100
      Position = 100
      SelEnd = 0
      SelStart = 0
      TabOrder = 0
      TickMarks = tmBoth
      TickStyle = tsAuto
      OnChange = TrackBar4Change
    end
    object Button3: TButton
      Left = 8
      Top = 171
      Width = 61
      Height = 19
      Caption = 'Fit'
      TabOrder = 1
      OnClick = Button3Click
    end
  end
  object PageControl1: TPageControl
    Left = 88
    Top = 161
    Width = 420
    Height = 202
    ActivePage = TabSheet4
    HotTrack = True
    MultiLine = True
    TabOrder = 5
    TabPosition = tpBottom
    OnChange = PageControl1Change
    object TabSheet1: TTabSheet
      Caption = 'Contrast'
      object Label3: TLabel
        Left = 8
        Top = 23
        Width = 42
        Height = 13
        Caption = '&Contrast:'
        FocusControl = Edit1
      end
      object Edit1: TEdit
        Left = 69
        Top = 20
        Width = 33
        Height = 21
        TabOrder = 0
        Text = '0'
        OnChange = Edit1Change
      end
      object TrackBar1: TTrackBar
        Left = 104
        Top = 16
        Width = 275
        Height = 33
        Max = 100
        Min = -100
        Orientation = trHorizontal
        Frequency = 10
        Position = 0
        SelEnd = 0
        SelStart = 0
        TabOrder = 1
        TickMarks = tmBottomRight
        TickStyle = tsAuto
        OnChange = TrackBar1Change
      end
    end
    object TabSheet5: TTabSheet
      Caption = 'HSV'
      object Label10: TLabel
        Left = 8
        Top = 12
        Width = 23
        Height = 13
        Caption = '&Hue:'
        FocusControl = Edit19
      end
      object Label11: TLabel
        Left = 8
        Top = 40
        Width = 51
        Height = 13
        Caption = '&Saturation:'
        FocusControl = Edit18
      end
      object Label12: TLabel
        Left = 8
        Top = 68
        Width = 27
        Height = 13
        Caption = '&Value'
        FocusControl = Edit17
      end
      object Label13: TLabel
        Left = 32
        Top = 120
        Width = 53
        Height = 13
        Hint = 'DoubleClick result image'
        Caption = 'Base color:'
        ParentShowHint = False
        ShowHint = True
      end
      object Label14: TLabel
        Left = 238
        Top = 120
        Width = 51
        Height = 13
        Caption = 'New color:'
      end
      object Edit17: TEdit
        Left = 69
        Top = 65
        Width = 33
        Height = 21
        TabOrder = 2
        Text = '0'
        OnChange = Edit19Change
      end
      object Edit18: TEdit
        Left = 69
        Top = 37
        Width = 33
        Height = 21
        TabOrder = 1
        Text = '0'
        OnChange = Edit19Change
      end
      object Edit19: TEdit
        Left = 69
        Top = 9
        Width = 33
        Height = 21
        TabOrder = 0
        Text = '0'
        OnChange = Edit19Change
      end
      object TrackBar9: TTrackBar
        Left = 104
        Top = 8
        Width = 313
        Height = 22
        Max = 180
        Min = -180
        Orientation = trHorizontal
        Frequency = 10
        Position = 0
        SelEnd = 0
        SelStart = 0
        TabOrder = 3
        TickMarks = tmBottomRight
        TickStyle = tsAuto
        OnChange = TrackBar9Change
      end
      object TrackBar10: TTrackBar
        Left = 104
        Top = 36
        Width = 313
        Height = 22
        Max = 100
        Min = -100
        Orientation = trHorizontal
        Frequency = 10
        Position = 0
        SelEnd = 0
        SelStart = 0
        TabOrder = 4
        TickMarks = tmBottomRight
        TickStyle = tsAuto
        OnChange = TrackBar9Change
      end
      object TrackBar11: TTrackBar
        Left = 104
        Top = 64
        Width = 313
        Height = 22
        Max = 100
        Min = -100
        Orientation = trHorizontal
        Frequency = 10
        Position = 0
        SelEnd = 0
        SelStart = 0
        TabOrder = 5
        TickMarks = tmBottomRight
        TickStyle = tsAuto
        OnChange = TrackBar9Change
      end
      object Panel3: TPanel
        Left = 111
        Top = 96
        Width = 97
        Height = 59
        BevelOuter = bvLowered
        TabOrder = 6
        object HSVBox3: THSVBox
          Left = 1
          Top = 1
          Width = 95
          Height = 57
          OnChange = HSVBox3Change
          Align = alClient
        end
      end
      object Panel4: TPanel
        Left = 303
        Top = 96
        Width = 98
        Height = 60
        BevelOuter = bvLowered
        Caption = 'Panel4'
        TabOrder = 7
        object HSVBox1: THSVBox
          Left = 1
          Top = 1
          Width = 96
          Height = 58
          OnChange = HSVBox3Change
          Align = alClient
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'HSL'
      object Label4: TLabel
        Left = 8
        Top = 23
        Width = 23
        Height = 13
        Caption = '&Hue:'
        FocusControl = Edit4
      end
      object Label5: TLabel
        Left = 8
        Top = 55
        Width = 51
        Height = 13
        Caption = '&Saturation:'
        FocusControl = Edit2
      end
      object Label6: TLabel
        Left = 8
        Top = 87
        Width = 52
        Height = 13
        Caption = '&Luminosity:'
        FocusControl = Edit3
      end
      object Edit3: TEdit
        Left = 69
        Top = 84
        Width = 33
        Height = 21
        TabOrder = 2
        Text = '0'
        OnChange = Edit4Change
      end
      object Edit2: TEdit
        Left = 69
        Top = 52
        Width = 33
        Height = 21
        TabOrder = 1
        Text = '0'
        OnChange = Edit4Change
      end
      object Edit4: TEdit
        Left = 69
        Top = 20
        Width = 33
        Height = 21
        TabOrder = 0
        Text = '0'
        OnChange = Edit4Change
      end
      object TrackBar2: TTrackBar
        Left = 104
        Top = 16
        Width = 275
        Height = 33
        Max = 180
        Min = -180
        Orientation = trHorizontal
        Frequency = 10
        Position = 0
        SelEnd = 0
        SelStart = 0
        TabOrder = 3
        TickMarks = tmBottomRight
        TickStyle = tsAuto
        OnChange = TrackBar2Change
      end
      object TrackBar3: TTrackBar
        Left = 104
        Top = 48
        Width = 275
        Height = 33
        Max = 100
        Min = -100
        Orientation = trHorizontal
        Frequency = 10
        Position = 0
        SelEnd = 0
        SelStart = 0
        TabOrder = 4
        TickMarks = tmBottomRight
        TickStyle = tsAuto
        OnChange = TrackBar2Change
      end
      object TrackBar5: TTrackBar
        Left = 104
        Top = 80
        Width = 275
        Height = 33
        Max = 100
        Min = -100
        Orientation = trHorizontal
        Frequency = 10
        Position = 0
        SelEnd = 0
        SelStart = 0
        TabOrder = 5
        TickMarks = tmBottomRight
        TickStyle = tsAuto
        OnChange = TrackBar2Change
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'RGB'
      object Label7: TLabel
        Left = 8
        Top = 23
        Width = 40
        Height = 13
        Caption = 'Red (R):'
        FocusControl = Edit7
      end
      object Label8: TLabel
        Left = 8
        Top = 55
        Width = 49
        Height = 13
        Caption = 'Green (G):'
        FocusControl = Edit6
      end
      object Label9: TLabel
        Left = 8
        Top = 87
        Width = 40
        Height = 13
        Caption = 'Blue (B):'
        FocusControl = Edit5
      end
      object Edit5: TEdit
        Left = 69
        Top = 84
        Width = 33
        Height = 21
        TabOrder = 2
        Text = '0'
        OnChange = Edit7Change
      end
      object Edit6: TEdit
        Left = 69
        Top = 52
        Width = 33
        Height = 21
        TabOrder = 1
        Text = '0'
        OnChange = Edit7Change
      end
      object Edit7: TEdit
        Left = 69
        Top = 20
        Width = 33
        Height = 21
        TabOrder = 0
        Text = '0'
        OnChange = Edit7Change
      end
      object TrackBar6: TTrackBar
        Left = 104
        Top = 16
        Width = 275
        Height = 33
        Max = 255
        Min = -255
        Orientation = trHorizontal
        Frequency = 10
        Position = 0
        SelEnd = 0
        SelStart = 0
        TabOrder = 3
        TickMarks = tmBottomRight
        TickStyle = tsAuto
        OnChange = TrackBar6Change
      end
      object TrackBar7: TTrackBar
        Left = 104
        Top = 48
        Width = 275
        Height = 33
        Max = 255
        Min = -255
        Orientation = trHorizontal
        Frequency = 10
        Position = 0
        SelEnd = 0
        SelStart = 0
        TabOrder = 4
        TickMarks = tmBottomRight
        TickStyle = tsAuto
        OnChange = TrackBar6Change
      end
      object TrackBar8: TTrackBar
        Left = 104
        Top = 80
        Width = 275
        Height = 33
        Max = 255
        Min = -255
        Orientation = trHorizontal
        Frequency = 10
        Position = 0
        SelEnd = 0
        SelStart = 0
        TabOrder = 5
        TickMarks = tmBottomRight
        TickStyle = tsAuto
        OnChange = TrackBar6Change
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'User filter'
      object GroupBox1: TGroupBox
        Left = 12
        Top = 4
        Width = 269
        Height = 149
        Caption = ' Filter values'
        TabOrder = 0
        object Label15: TLabel
          Left = 206
          Top = 32
          Width = 35
          Height = 13
          Caption = 'Divisor:'
        end
        object Edit8: TEdit
          Left = 8
          Top = 19
          Width = 33
          Height = 21
          TabOrder = 0
          Text = '0'
          OnChange = Edit8Change
        end
        object Edit9: TEdit
          Tag = 3
          Left = 8
          Top = 46
          Width = 33
          Height = 21
          TabOrder = 1
          Text = '0'
          OnChange = Edit8Change
        end
        object Edit10: TEdit
          Tag = 6
          Left = 8
          Top = 73
          Width = 33
          Height = 21
          TabOrder = 2
          Text = '0'
          OnChange = Edit8Change
        end
        object Edit11: TEdit
          Tag = 1
          Left = 75
          Top = 19
          Width = 33
          Height = 21
          TabOrder = 3
          Text = '0'
          OnChange = Edit8Change
        end
        object Edit12: TEdit
          Tag = 4
          Left = 75
          Top = 46
          Width = 33
          Height = 21
          TabOrder = 4
          Text = '0'
          OnChange = Edit8Change
        end
        object Edit13: TEdit
          Tag = 7
          Left = 75
          Top = 73
          Width = 33
          Height = 21
          TabOrder = 5
          Text = '0'
          OnChange = Edit8Change
        end
        object Edit14: TEdit
          Tag = 2
          Left = 142
          Top = 19
          Width = 33
          Height = 21
          TabOrder = 6
          Text = '0'
          OnChange = Edit8Change
        end
        object Edit15: TEdit
          Tag = 5
          Left = 142
          Top = 46
          Width = 33
          Height = 21
          TabOrder = 7
          Text = '0'
          OnChange = Edit8Change
        end
        object Edit16: TEdit
          Tag = 8
          Left = 142
          Top = 73
          Width = 33
          Height = 21
          TabOrder = 8
          Text = '0'
          OnChange = Edit8Change
        end
        object UpDown1: TUpDown
          Left = 41
          Top = 19
          Width = 15
          Height = 21
          Associate = Edit8
          Min = -100
          Position = 0
          TabOrder = 10
          Thousands = False
          Wrap = False
        end
        object UpDown2: TUpDown
          Left = 41
          Top = 46
          Width = 15
          Height = 21
          Associate = Edit9
          Min = -100
          Position = 0
          TabOrder = 11
          Thousands = False
          Wrap = False
        end
        object UpDown3: TUpDown
          Left = 41
          Top = 73
          Width = 15
          Height = 21
          Associate = Edit10
          Min = -100
          Position = 0
          TabOrder = 12
          Thousands = False
          Wrap = False
        end
        object UpDown4: TUpDown
          Left = 108
          Top = 19
          Width = 15
          Height = 21
          Associate = Edit11
          Min = -100
          Position = 0
          TabOrder = 13
          Thousands = False
          Wrap = False
        end
        object UpDown5: TUpDown
          Left = 108
          Top = 46
          Width = 15
          Height = 21
          Associate = Edit12
          Min = -100
          Position = 0
          TabOrder = 14
          Thousands = False
          Wrap = False
        end
        object UpDown6: TUpDown
          Left = 108
          Top = 73
          Width = 15
          Height = 21
          Associate = Edit13
          Min = -100
          Position = 0
          TabOrder = 15
          Thousands = False
          Wrap = False
        end
        object UpDown7: TUpDown
          Left = 175
          Top = 19
          Width = 15
          Height = 21
          Associate = Edit14
          Min = -100
          Position = 0
          TabOrder = 16
          Thousands = False
          Wrap = False
        end
        object UpDown8: TUpDown
          Left = 175
          Top = 46
          Width = 15
          Height = 21
          Associate = Edit15
          Min = -100
          Position = 0
          TabOrder = 17
          Thousands = False
          Wrap = False
        end
        object UpDown9: TUpDown
          Left = 175
          Top = 73
          Width = 15
          Height = 21
          Associate = Edit16
          Min = -100
          Position = 0
          TabOrder = 18
          Thousands = False
          Wrap = False
        end
        object Button4: TButton
          Left = 8
          Top = 112
          Width = 70
          Height = 21
          Caption = 'Load'
          TabOrder = 19
          OnClick = Button4Click
        end
        object Button5: TButton
          Left = 88
          Top = 112
          Width = 70
          Height = 21
          Caption = 'Save'
          TabOrder = 20
          OnClick = Button5Click
        end
        object Edit20: TEdit
          Tag = 9
          Left = 206
          Top = 46
          Width = 33
          Height = 21
          TabOrder = 9
          Text = '0'
          OnChange = Edit8Change
        end
        object UpDown10: TUpDown
          Left = 239
          Top = 46
          Width = 15
          Height = 21
          Associate = Edit20
          Min = -100
          Position = 0
          TabOrder = 21
          Thousands = False
          Wrap = False
        end
      end
      object GroupBox3: TGroupBox
        Left = 296
        Top = 4
        Width = 113
        Height = 149
        Caption = ' Presets'
        TabOrder = 1
        object ListBox1: TListBox
          Left = 8
          Top = 18
          Width = 97
          Height = 127
          ItemHeight = 13
          Items.Strings = (
            'None'
            'Blur'
            'Edges'
            'Emboss'
            'High Pass 1'
            'High Pass 2'
            'High Pass 3'
            'Low Pass 1'
            'Low Pass 2')
          TabOrder = 0
          OnClick = ListBox1Click
        end
      end
    end
    object TabSheet6: TTabSheet
      Caption = 'Equalization'
      object Label16: TLabel
        Left = 8
        Top = 144
        Width = 6
        Height = 13
        Caption = '0'
      end
      object Label17: TLabel
        Left = 280
        Top = 144
        Width = 18
        Height = 13
        Caption = '255'
      end
      object Label18: TLabel
        Left = 120
        Top = 144
        Width = 47
        Height = 13
        Caption = 'Threshold'
      end
      object Label19: TLabel
        Left = 120
        Top = 0
        Width = 57
        Height = 13
        Caption = 'Equalization'
      end
      object Label20: TLabel
        Left = 8
        Top = 1
        Width = 6
        Height = 13
        Caption = '0'
      end
      object Label21: TLabel
        Left = 280
        Top = 1
        Width = 18
        Height = 13
        Caption = '255'
      end
      object SpeedButton1: TSpeedButton
        Left = 326
        Top = 115
        Width = 73
        Height = 25
        AllowAllUp = True
        GroupIndex = 1
        Caption = 'Auto equalize'
        OnClick = SpeedButton1Click
      end
      object Panel5: TPanel
        Left = 8
        Top = 16
        Width = 293
        Height = 123
        BevelOuter = bvNone
        BorderStyle = bsSingle
        TabOrder = 0
        object HistogramBox1: THistogramBox
          Left = 0
          Top = 14
          Width = 289
          Height = 67
          AttachedImageEnProc = ImageEnProc1
          Background = clSilver
          Labels = []
        end
        object RulerBox2: TRulerBox
          Left = 0
          Top = 0
          Width = 289
          Height = 13
          Background = clSilver
          GripsDir = gdDown
          Ruler = False
          DotPerUnit = 1.13333333333333
          Frequency = 16
          LabelFreq = 32
          RulerColor = clGray
          ViewMax = 255
          OnRulerPosChange = RulerBox2RulerPosChange
          FitInView = True
          GripsCount = 2
          Align = alTop
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = []
        end
        object RulerBox1: TRulerBox
          Left = 0
          Top = 87
          Width = 289
          Height = 32
          Background = clSilver
          DotPerUnit = 1.13333333333333
          Frequency = 16
          LabelFreq = 32
          RulerColor = clSilver
          ViewMax = 255
          OnRulerPosChange = RulerBox1RulerPosChange
          FitInView = True
          GripsCount = 2
          Align = alBottom
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Times New Roman'
          Font.Style = []
        end
      end
      object GroupBox4: TGroupBox
        Left = 325
        Top = 10
        Width = 74
        Height = 88
        Caption = ' Histogram '
        TabOrder = 1
        object CheckListBox1: TCheckListBox
          Left = 8
          Top = 21
          Width = 57
          Height = 56
          BorderStyle = bsNone
          Color = clBtnFace
          Ctl3D = True
          ItemHeight = 13
          Items.Strings = (
            'Red'
            'Green'
            'Blue'
            'Gray')
          ParentCtl3D = False
          TabOrder = 0
          OnClick = CheckListBox1Click
        end
      end
    end
  end
  object Button6: TButton
    Left = 206
    Top = 65
    Width = 21
    Height = 25
    Caption = '<<'
    TabOrder = 6
    OnClick = Button6Click
  end
  object ProgressBar1: TProgressBar
    Left = 232
    Top = 143
    Width = 175
    Height = 11
    Min = 0
    Max = 100
    TabOrder = 7
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = 'flt'
    Filter = 'Filter (*.flt)|*.flt'
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist]
    Title = 'Load filter'
    Left = 416
    Top = 83
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = 'flt'
    Filter = 'Filter (*.flt)|*.flt'
    Options = [ofHideReadOnly, ofPathMustExist]
    Title = 'Save filter'
    Left = 472
    Top = 90
  end
  object ImageEnProc1: TImageEnProc
    AttachedImageEn = ImageEn1
    Left = 416
    Top = 121
  end
end
