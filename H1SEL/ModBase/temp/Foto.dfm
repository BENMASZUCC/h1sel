object FotoForm: TFotoForm
  Left = 532
  Top = 142
  Width = 218
  Height = 312
  Caption = 'Foto del soggetto'
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 210
    Height = 77
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object SpeedButton1: TSpeedButton
      Left = 5
      Top = 51
      Width = 69
      Height = 22
      Caption = 'Inserisci'
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FF00FFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFF00FFFFFFFF85B7850F6F0F1674161A761A1A761A18781817
        7917137D130D7F0D0A7E0A077C07027B020070007FB07FFFFFFFFFFFFF118311
        1F8C1F2A912A2F932F2E942E2C962C299A29239E231CA31C15A4150DA40D059F
        05019101057E05FFFFFFFFFFFF198D192C962C379C373D9F3D3C9F3C39A139A3
        D6A3FFFFFF24AF241CB11C13B2130AAD0A049F04057E05FFFFFFFFFFFF229122
        389C3843A24348A44845A54542A642FFFFFFFFFFFFFFFFFF21B52118B6180EB1
        0E08A308057E05FFFFFFFFFFFF2C962C42A0424CA54C4FA74F4CA74C46A74640
        AA40FFFFFFFFFFFFFFFFFF1AB31A14AF140FA30F0B800BFFFFFFFFFFFF359A35
        4BA54B52A85253A9534EA84E49A74941A84138AA38FFFFFFFFFFFFFFFFFF19AC
        1918A218128212FFFFFFFFFFFF3F9F3F53A953FFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF1F9E1F188118FFFFFFFFFFFF45A245
        5AAC5AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF259A251D7F1DFFFFFFFFFFFF4FA74F63B16361AF6159AB5951A65148A2483F
        9F3F369C36FFFFFFFFFFFFFFFFFF2699262A972A217E21FFFFFFFFFFFF53A953
        6CB66C68B4685EAD5E54A8544CA34C429F42FFFFFFFFFFFFFFFFFF2997292B98
        2B2D952D237E23FFFFFFFFFFFF5EAF5E7ABD7A70B87063B0635AAB5A52A652FF
        FFFFFFFFFFFFFFFF3399333099303098302F942F237D23FFFFFFFFFFFF6BB56B
        8DC68D80C0806FB76F67B26760AE60B4D9B4FFFFFF4CA54C49A44941A1413A9D
        3A3095301E7A1EFFFFFFFFFFFF77BB779DCF9D8CC68C79BC7970B87069B46965
        B26562B0625DAE5D56AB564EA74E41A1412F942F197719FFFFFFFFFFFFB1D8B1
        76BB7667B3675BAD5B54A9544FA74F4AA44A4BA54B46A3463FA03F3B9E3B3198
        31238C238ABB8AFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FF}
      OnClick = SpeedButton1Click
    end
    object SpeedButton2: TSpeedButton
      Left = 75
      Top = 51
      Width = 69
      Height = 22
      Caption = 'Rimuovi'
      Glyph.Data = {
        36050000424D3605000000000000360400002800000010000000100000000100
        080000000000000100000000000000000000000100000000000000000000FFFF
        FF0000000000FFDFFF000E48E900134BE9002C5DE9007294F10098B1F600BACA
        F800DCE4FB000439E000053AE000083EE1000A43E8000D42E0000F43E1001547
        E0001848E0001C4BE0002352E6002456E8002C58E6002D5CE8003763E8006587
        EE006B8DF0006F8FEF007192F100B6C7F800DBE3FB00E2E8FB00EBEFFC000132
        D700022CC000022BBE00052EC100072FBE000A32C0000B33C1000D35C0001238
        C2001337BD00183CC0001F4CDF002450E400244FDF00254EDE00264EE2002951
        DC002A50DC002C55E4002C51DB004066E7004669E6006483EC00728FEE008097
        ED00B0BFF500BBC8F600E0E6FB000026B6000026B5001138D4000F30B5001941
        DE001636B8001739B9001839BB001939BA001F44D9001A39B8001A39B7001D3F
        BF002248DF001E3EBC002347D8002141BF002342BE002342BD002C52E2002A4D
        DA002C50E1002C51E1002D51DC002E51DB003054E2003053E1002F52DB003154
        E2003053DC002F51DA003558E2003759E300385AE3003A5BE3003A5CE3003B5D
        E3003F5FE4003F60E3004162E4004261E4004363E4004464E5004564E5004665
        E5004867E5004868E500496AE6004A69E6004B68E5004B69E5004E6CE6004E6D
        E6004F6CE600506DE600536FE7005470E7005673E7005A75E8005B76E8005D78
        E8005E79E800627CE900637DE800657EE9006781EA006A83E9006C86EB006B85
        EA006B83E9006C85EA006F88EB007089EB00728AEC00768DEC00778EEC007990
        EC007A91EC007E94ED008398EE00899DEE008CA0EF008B9EEE008DA1EF008596
        DC007F90D10095A8F0008A9BDE0097A9F10097A8F0009DAEF100A2B2F200A6B5
        F200B0BDF400B2BFF400BBC6F500E9EDFC004865E4004C67E500506CE600516C
        E6007086EA008093ED008195ED00B1BDF400B7C2F500EFF1FD00EEF0FC00FFFF
        FF00000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000101010101
        0101010101010101010001914042474844432A282625223D9201013F46515B55
        34312E13110F0C213E010141525D678B1F01010A1C050E0B2301014A5E669801
        3B381B090108040D24010153658F018C3635180607011A102701015C6E209C71
        6C010117151D1E12290101627401A4736B0101161419012C2B0101687701A4A0
        6A0101332D37012F490101727CA7A6A19E010150303A3C324D010174839501A3
        9F655F56800186544E01017A8A859901A5A2829A019357584F01018190398496
        A801019D8D64605A4B010188978E89857F7D7B79767064584501019B877E7875
        726D6F696361594C940100010101010101010101010101010100}
      OnClick = SpeedButton2Click
    end
    object DBEdit1: TDBEdit
      Left = 5
      Top = 4
      Width = 200
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      Color = clInfoBk
      DataField = 'Cognome'
      DataSource = Data.DsAnagrafica
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
    end
    object DBEdit2: TDBEdit
      Left = 5
      Top = 25
      Width = 200
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      Color = clInfoBk
      DataField = 'Nome'
      DataSource = Data.DsAnagrafica
      ReadOnly = True
      TabOrder = 1
    end
    object CBAdatta: TCheckBox
      Left = 149
      Top = 54
      Width = 56
      Height = 17
      Caption = 'Adatta'
      Checked = True
      State = cbChecked
      TabOrder = 2
      OnClick = CBAdattaClick
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 77
    Width = 210
    Height = 208
    Align = alClient
    BevelOuter = bvLowered
    TabOrder = 1
    object DbAnagFotoOLd: TdxDBGraphicEdit
      Left = 1
      Top = 1
      Width = 208
      Align = alClient
      Color = clBtnFace
      TabOrder = 0
      Caption = 'Foto non presente'
      CustomGraphic = True
      DblClickActivate = False
      OnGetGraphicClass = DbAnagFotoOLdGetGraphicClass
      Height = 206
    end
    object DbAnagFoto: TJvDBImage
      Left = 1
      Top = 1
      Width = 208
      Height = 206
      Align = alClient
      DataField = 'Foto'
      DataSource = Data.DsQanagFoto
      TabOrder = 1
    end
  end
end
