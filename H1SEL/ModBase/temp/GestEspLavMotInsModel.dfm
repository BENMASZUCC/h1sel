object GestEspLavMotInsModelForm: TGestEspLavMotInsModelForm
  Left = 256
  Top = 106
  Width = 783
  Height = 331
  Caption = 'Gestione modello Motivi di Insoddisfazione'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 775
    Height = 48
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 27
      Width = 114
      Height = 13
      Caption = 'Premere INS per inserire'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clPurple
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object BitBtn1: TBitBtn
      Left = 672
      Top = 5
      Width = 97
      Height = 38
      Anchors = [akTop, akRight]
      Caption = 'Esci'
      TabOrder = 0
      Kind = bkClose
    end
  end
  object dxDBGrid1: TdxDBGrid
    Left = 0
    Top = 48
    Width = 775
    Height = 256
    Bands = <
      item
        Width = 187
      end
      item
        Caption = 'Valore 1'
        Width = 120
      end
      item
        Caption = 'Valore 2'
        Width = 117
      end
      item
        Caption = 'Valore 3'
        Width = 117
      end
      item
        Caption = 'Valore 4'
        Width = 117
      end
      item
        Caption = 'Valore 5'
        Width = 113
      end>
    DefaultLayout = False
    HeaderPanelRowCount = 1
    KeyField = 'ID'
    SummaryGroups = <>
    SummarySeparator = ', '
    Align = alClient
    TabOrder = 1
    DataSource = DsQModel
    Filter.Criteria = {00000000}
    OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoPartialLoad, edgoUseBookmarks]
    OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoUseBitmap]
    ShowBands = True
    object dxDBGrid1Voce: TdxDBGridMaskColumn
      Color = clInfoBk
      Width = 136
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Voce'
    end
    object dxDBGrid1DescVoce: TdxDBGridBlobColumn
      Caption = 'dettaglio'
      Width = 51
      BandIndex = 0
      RowIndex = 0
      FieldName = 'DescVoce'
      BlobKind = bkMemo
    end
    object dxDBGrid1Val1Dic: TdxDBGridMaskColumn
      Caption = 'Dicitura'
      Color = clInfoBk
      Width = 69
      BandIndex = 1
      RowIndex = 0
      FieldName = 'Val1Dic'
    end
    object dxDBGrid1Val1Desc: TdxDBGridBlobColumn
      Caption = 'Dettaglio'
      Width = 51
      BandIndex = 1
      RowIndex = 0
      FieldName = 'Val1Desc'
      BlobKind = bkMemo
    end
    object dxDBGrid1Val2Dic: TdxDBGridMaskColumn
      Caption = 'Dicitura'
      Color = clInfoBk
      Width = 69
      BandIndex = 2
      RowIndex = 0
      FieldName = 'Val2Dic'
    end
    object dxDBGrid1Val2Desc: TdxDBGridBlobColumn
      Caption = 'Dettaglio'
      Width = 48
      BandIndex = 2
      RowIndex = 0
      FieldName = 'Val2Desc'
      BlobKind = bkMemo
    end
    object dxDBGrid1Val3Dic: TdxDBGridMaskColumn
      Caption = 'Dicitura'
      Color = clInfoBk
      Width = 68
      BandIndex = 3
      RowIndex = 0
      FieldName = 'Val3Dic'
    end
    object dxDBGrid1Val3Desc: TdxDBGridBlobColumn
      Caption = 'Dettaglio'
      Width = 49
      BandIndex = 3
      RowIndex = 0
      FieldName = 'Val3Desc'
      BlobKind = bkMemo
    end
    object dxDBGrid1Val4Dic: TdxDBGridMaskColumn
      Caption = 'Dicitura'
      Color = clInfoBk
      Width = 66
      BandIndex = 4
      RowIndex = 0
      FieldName = 'Val4Dic'
    end
    object dxDBGrid1Val4Desc: TdxDBGridBlobColumn
      Caption = 'Dettaglio'
      Width = 51
      BandIndex = 4
      RowIndex = 0
      FieldName = 'Val4Desc'
      BlobKind = bkMemo
    end
    object dxDBGrid1Val5Dic: TdxDBGridMaskColumn
      Caption = 'Dicitura'
      Color = clInfoBk
      Width = 66
      BandIndex = 5
      RowIndex = 0
      FieldName = 'Val5Dic'
    end
    object dxDBGrid1Val5Desc: TdxDBGridBlobColumn
      Caption = 'Dettaglio'
      Width = 47
      BandIndex = 5
      RowIndex = 0
      FieldName = 'Val5Desc'
      BlobKind = bkMemo
    end
  end
  object QModel: TADOQuery
    Active = True
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from EspLavMotInsModel')
    Left = 40
    Top = 144
  end
  object DsQModel: TDataSource
    DataSet = QModel
    Left = 40
    Top = 184
  end
end
