//[TONI20021004]DEBUGOK
unit GestModelliWord;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     TB97,Grids,DBGrids,RXDBCtrl,ExtCtrls,StdCtrls,Buttons,Db,DBTables,
     DBCtrls,ComObj,ADODB,U_ADOLinkCl;

type
     TGestModelliWordForm=class(TForm)
          DsQModelli: TDataSource;
          BitBtn1: TBitBtn;
          DsQCampiModello: TDataSource;
          Panel3: TPanel;
          RxDBGrid2: TRxDBGrid;
          Panel2: TPanel;
          Panel4: TPanel;
          BCampoNew: TToolbarButton97;
          BCampoMod: TToolbarButton97;
          BCampoDel: TToolbarButton97;
          Panel5: TPanel;
          Panel6: TPanel;
          ToolbarButton971: TToolbarButton97;
          ToolbarButton972: TToolbarButton97;
          ToolbarButton973: TToolbarButton97;
          Panel7: TPanel;
          RxDBGrid1: TRxDBGrid;
          DBText1: TDBText;
          ToolbarButton974: TToolbarButton97;
          BAprimodello: TToolbarButton97;
          QModelli: TADOLinkedQuery;
          QCampiModello: TADOLinkedQuery;
    Panel1: TPanel;
    PanelCampi: TPanel;
    Label1: TLabel;
          procedure ToolbarButton971Click(Sender: TObject);
          procedure ToolbarButton972Click(Sender: TObject);
          procedure ToolbarButton973Click(Sender: TObject);
          procedure BCampoNewClick(Sender: TObject);
          procedure BCampoModClick(Sender: TObject);
          procedure BCampoDelClick(Sender: TObject);
          procedure ToolbarButton974Click(Sender: TObject);
          procedure BAprimodelloClick(Sender: TObject);
          procedure FormShow(Sender: TObject);
     private
          function GetDocPath: string;
     public
          { Public declarations }
     end;

var
     GestModelliWordForm: TGestModelliWordForm;

implementation

uses ModelloWord,ModuloDati,CampiModello,uUtilsVarie;

{$R *.DFM}

//[TONI20021004]DEBUGOK

procedure TGestModelliWordForm.ToolbarButton971Click(Sender: TObject);
begin
     ModelloWordForm:=TModelloWordForm.create(self);
     ModelloWordForm.showModal;
     if ModelloWordForm.Modalresult=mrOK then begin
          with Data do begin
               DB.BeginTrans;
               try
{                    Q1.Close;
                    Q1.SQL.text:='insert into ModelliWord (NomeModello,Descrizione,TabellaMaster,InizialiFileGenerato) '+
                         'values (:xNomeModello:,:xDescrizione:,:xTabellaMaster:,:xInizialiFileGenerato:)';
                    Q1.ParamByName['xNomeModello']:=ModelloWordForm.EModello.text;
                    Q1.ParamByName['xDescrizione']:=ModelloWordForm.EDesc.text;
                    case ModelloWordForm.RGTabella.ItemIndex of
                         0: Q1.ParamByName['xTabellaMaster']:='Anagrafica';
                         1: Q1.ParamByName['xTabellaMaster']:='EBC_Clienti';
                         2: Q1.ParamByName['xTabellaMaster']:='*RicClienti';
                    end;
                    Q1.ParamByName['xInizialiFileGenerato']:=ModelloWordForm.EIniziali.text;
                    Q1.ExecSQL;}
                    qmodelli.Insert;
                    qmodelli.fieldbyname ('NomeModello').AsString := ModelloWordForm.EModello.text;
                    qmodelli.fieldbyname ('Descrizione').AsString := ModelloWordForm.EDesc.text;
                    case ModelloWordForm.RGTabella.ItemIndex of
                         0: qmodelli.fieldbyname ('TabellaMaster').AsString:='Anagrafica';
                         1: qmodelli.fieldbyname ('TabellaMaster').AsString:='EBC_Clienti';
                         2: qmodelli.fieldbyname ('TabellaMaster').AsString:='*RicClienti';
                    else qmodelli.fieldbyname ('TabellaMaster').AsString:='';
                    end;
                    qmodelli.fieldbyname ('InizialiFileGenerato').AsString := ModelloWordForm.EIniziali.text;
//                    qmodelli.fieldbyname ('H1SEL').AsBoolean := ModelloWordForm.CBH1Sel.Checked;
//                    qmodelli.fieldbyname ('H1HRMS').AsBoolean := ModelloWordForm.CBHRMS.Checked;
                    qmodelli.post;
                    DB.CommitTrans;
                    //[/GIULIO20020929\]
                    QModelli.Close;
                    QModelli.Open;
                    //[/TONI20020729\]FINE
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               end;
          end;
     end;
     ModelloWordForm.Free;
end;
//[TONI20021004]DEBUGOK

procedure TGestModelliWordForm.ToolbarButton972Click(Sender: TObject);
begin
     if QModelli.RecordCount=0 then exit;
     ModelloWordForm:=TModelloWordForm.create(self);
     //[/TONI20020729\]
     {     ModelloWordForm.EModello.text:=QModelliNomeModello.Value;
          ModelloWordForm.EDesc.text:=QModelliDescrizione.Value;}
     ModelloWordForm.EModello.text:=QModelli.FieldByName('NomeModello').Value;
     ModelloWordForm.EDesc.text:=QModelli.FieldByName('Descrizione').Value;
     //     if QModelliTabellaMaster.Value='Anagrafica' then
     if QModelli.FieldByName('TabellaMaster').Value='Anagrafica' then
          ModelloWordForm.RGTabella.ItemIndex:=0;
     //     if QModelliTabellaMaster.Value='EBC_Clienti' then
     if QModelli.FieldByName('TabellaMaster').Value='EBC_Clienti' then
          ModelloWordForm.RGTabella.ItemIndex:=1;
     //     if QModelliTabellaMaster.Value='*RicClienti' then
     if QModelli.FieldByName('TabellaMaster').Value='*RicClienti' then
          ModelloWordForm.RGTabella.ItemIndex:=2;
     //    ModelloWordForm.EIniziali.text:=QModelliInizialiFileGenerato.Value;
     ModelloWordForm.EIniziali.text:=QModelli.FieldByName('InizialiFileGenerato').Value;
     ModelloWordForm.showModal;
     if ModelloWordForm.Modalresult=mrOK then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    {                    Q1.SQL.text:='update ModelliWord set NomeModello=:xNomeModello,Descrizione=:xDescrizione,'+
                                             'TabellaMaster=:xTabellaMaster,InizialiFileGenerato=:xInizialiFileGenerato '+
                                             'where ID='+QModelliID.AsString;
                                        Q1.ParamByName('xNomeModello').asString:=ModelloWordForm.EModello.text;
                                        Q1.ParamByName('xDescrizione').asString:=ModelloWordForm.EDesc.text;
                                        case ModelloWordForm.RGTabella.ItemIndex of
                                             0: Q1.ParamByName('xTabellaMaster').asString:='Anagrafica';
                                             1: Q1.ParamByName('xTabellaMaster').asString:='EBC_Clienti';
                                             2: Q1.ParamByName('xTabellaMaster').asString:='*RicClienti';
                                        end;
                                        Q1.ParamByName('xInizialiFileGenerato').asString:=ModelloWordForm.EIniziali.text;}
                    Q1.SQL.text:='update ModelliWord set NomeModello=:xNomeModello:,Descrizione=:xDescrizione:,'+
                         'TabellaMaster=:xTabellaMaster:,InizialiFileGenerato=:xInizialiFileGenerato: '+
                         'where ID='+QModelli.FieldByName('ID').AsString;
                    Q1.ParamByName['xNomeModello']:=ModelloWordForm.EModello.text;
                    Q1.ParamByName['xDescrizione']:=ModelloWordForm.EDesc.text;
                    case ModelloWordForm.RGTabella.ItemIndex of
                         0: Q1.ParamByName['xTabellaMaster']:='Anagrafica';
                         1: Q1.ParamByName['xTabellaMaster']:='EBC_Clienti';
                         2: Q1.ParamByName['xTabellaMaster']:='*RicClienti';
                    end;
                    Q1.ParamByName['xInizialiFileGenerato']:=ModelloWordForm.EIniziali.text;

                    Q1.ExecSQL;
                    DB.CommitTrans;
                    {                    QModelli.Close;
                                        QModelli.Open;}
                    //[/TONI20020729\]
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               end;
          end;
     end;
     ModelloWordForm.Free;
end;

//[TONI20021004]DEBUGOK

procedure TGestModelliWordForm.ToolbarButton973Click(Sender: TObject);
begin
     if QModelli.isEmpty then exit;
     if QCampiModello.RecordCount>0 then begin
          ShowMessage('IMPOSSIBILE CANCELLARE: ci sono campi che referenziano questo modello');
          exit;
     end;
     if MessageDlg('Sei sicuro di voler cancellare il modello selezionato ?',mtWarning, [mbYes,mbNo],0)=mrYes then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    //[/TONI20020729\]
                    {                    Q1.SQL.text:='delete from ModelliWord '+
                                             'where ID='+QModelliID.asString;}
                    Q1.SQL.text:='delete from ModelliWord '+
                         'where ID='+QModelli.FieldByName('ID').asString;
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    QModelli.Close;
                    QModelli.Open;
                    //[/TONI20020729\]FINE
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               end;
          end;
     end;
end;

//[TONI20021004]DEBUGOK

procedure TGestModelliWordForm.BCampoNewClick(Sender: TObject);
begin
     CampiModelloForm:=TCampiModelloForm.create(self);
     // elenco campi in funzione della tabella
     // ogni tabella ha un file di testo con tale elenco
     // ogni riga ha la sintassi:  <campo> (<sua descrizione>)
     CampiModelloForm.CBCampo.items.Clear;
     //[/TONI20020729\]
     //     if QModelliTabellaMaster.value='Anagrafica' then // PER ORA NO AnagAltreInfo
     if TrimRight(QModelli.FieldByName('TabellaMaster').AsString)='Anagrafica' then // PER ORA NO AnagAltreInfo
          CampiModelloForm.CBCampo.items.LoadFromFile(ExtractFilePath(Application.ExeName)+'\MWAnagrafica.txt');
     //     if QModelliTabellaMaster.value='EBC_Clienti' then
     if TrimRight(QModelli.FieldByName('TabellaMaster').AsString)='EBC_Clienti' then
          CampiModelloForm.CBCampo.items.LoadFromFile('');
     //     if QModelliTabellaMaster.value='*RicClienti' then
     if TrimRight(QModelli.FieldByName('TabellaMaster').AsString)='*RicClienti' then
          CampiModelloForm.CBCampo.items.LoadFromFile(ExtractFilePath(Application.ExeName)+'\MWRicClienti.txt');
     CampiModelloForm.ShowModal;
     if CampiModelloForm.ModalResult=mrOK then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    {                    Q1.SQL.text:='insert into ModelliWordCampi (IDModello,NomeCampo,Campo) '+
                                             'values (:xIDModello,:xNomeCampo,:xCampo)';
                                        Q1.ParamByName('xIDModello').asInteger:=QModelliID.value;
                                        Q1.ParamByName('xNomeCampo').asString:=CampiModelloForm.EDescWord.text;
                                        Q1.ParamByName('xCampo').asString:=copy(CampiModelloForm.CBCampo.text,1,pos('(',CampiModelloForm.CBCampo.text)-1);}
                    Q1.SQL.text:='insert into ModelliWordCampi (IDModello,NomeCampo,Campo) '+
                         'values (:xIDModello:,:xNomeCampo:,:xCampo:)';
                    Q1.ParamByName['xIDModello']:=QModelli.FieldByName('ID').AsInteger;
                    Q1.ParamByName['xNomeCampo']:=CampiModelloForm.EDescWord.text;
                    Q1.ParamByName['xCampo']:=copy(CampiModelloForm.CBCampo.text,1,pos('(',CampiModelloForm.CBCampo.text)-1);
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    QCampiModello.Close;
                    QCampiModello.Open;
                    //[/TONI20020729\]FINE
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               end;
          end;
     end;
     CampiModelloForm.Free;
end;

//[TONI20021004]DEBUGOK

procedure TGestModelliWordForm.BCampoModClick(Sender: TObject);
begin
     CampiModelloForm:=TCampiModelloForm.create(self);
     // elenco campi in funzione della tabella
     // ogni tabella ha un file di testo con tale elenco
     // ogni riga ha la sintassi:  <campo> (<sua descrizione>)
     CampiModelloForm.CBCampo.items.Clear;
     //[/TONI20020729\]
     //     if QModelliTabellaMaster.value='Anagrafica' then // PER ORA NO AnagAltreInfo
     if TrimRight(QModelli.FieldByName('TabellaMaster').AsString)='Anagrafica' then // PER ORA NO AnagAltreInfo
          CampiModelloForm.CBCampo.items.LoadFromFile('MWAnagrafica.txt');
     //     if QModelliTabellaMaster.value='EBC_Clienti' then
     if TrimRight(QModelli.FieldByName('TabellaMaster').AsString)='EBC_Clienti' then
          CampiModelloForm.CBCampo.items.LoadFromFile('');
     //     if QModelliTabellaMaster.value='*RicClienti' then
     if Trimright(QModelli.FieldByName('TabellaMaster').AsString)='*RicClienti' then
          CampiModelloForm.CBCampo.items.LoadFromFile('MWRicClienti.txt');
     {     CampiModelloForm.CBCampo.text:=QCampiModelloCampo.Value;
          CampiModelloForm.EDescWord.text:=QCampiModelloNomeCampo.Value;}
     CampiModelloForm.CBCampo.text:=TrimRight(QCampiModello.FieldbyName('Campo').AsString);
     CampiModelloForm.EDescWord.text:=TrimRight(QCampiModello.FieldbyName('NomeCampo').AsString);

     CampiModelloForm.ShowModal;
     if CampiModelloForm.ModalResult=mrOK then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    {                    Q1.SQL.text:='update ModelliWordCampi set NomeCampo=:xNomeCampo,Campo=:xCampo '+
                                             'where ID='+QCampiModelloID.asString;
                                        Q1.ParamByName('xNomeCampo').asString:=CampiModelloForm.EDescWord.text;
                                        Q1.ParamByName('xCampo').asString:=copy(CampiModelloForm.CBCampo.text,1,pos('(',CampiModelloForm.CBCampo.text)-1);}
                    Q1.SQL.text:='update ModelliWordCampi set NomeCampo=:xNomeCampo:,Campo=:xCampo: '+
                         'where ID='+QCampiModello.FieldByName('ID').asString;
                    Q1.ParamByName['xNomeCampo']:=CampiModelloForm.EDescWord.text;
                    Q1.ParamByName['xCampo']:=copy(CampiModelloForm.CBCampo.text,1,pos('(',CampiModelloForm.CBCampo.text)-1);
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    QCampiModello.Close;
                    QCampiModello.Open;
                    //[/TONI20020729\]FINE
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               end;
          end;
     end;
     CampiModelloForm.Free;
end;

//[TONI20021004]DEBUGOK

procedure TGestModelliWordForm.BCampoDelClick(Sender: TObject);
begin
     if QCampiModello.isEmpty then exit;
     if MessageDlg('Sei sicuro di voler cancellare il campo selezionato ?',mtWarning, [mbYes,mbNo],0)=mrYes then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    //[/TONI20020729\]
                    {                    Q1.SQL.text:='delete from ModelliWordCampi '+
                                             'where ID='+QCampiModelloID.asString;}
                    Q1.SQL.text:='delete from ModelliWordCampi '+
                         'where ID='+QCampiModello.FieldByName('ID').asString;
                    Q1.ExecSQL;
                    DB.CommitTrans;
                    QCampiModello.Close;
                    QCampiModello.Open;
                    //[/TONI20020729\]FINE
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
               end;
          end;
     end;
end;

//[TONI20021004]DEBUGOK

procedure TGestModelliWordForm.ToolbarButton974Click(Sender: TObject);
var xModelloWord: string;
     MSWord: Variant;
begin
     if QModelli.IsEmpty then exit;
     if QCampiModello.IsEmpty then begin
          ShowMessage('Inserire prima i campi del modello');
          exit;
     end;
     xModelloWord:=QModelli.FieldByName('NomeModello').AsString;
     if FileExists(GetDocPath+'\'+xModelloWord+'.doc') then
          if MessageDlg('ATTENZIONE: il modello esiste gi�. Vuoi ricrearlo ?',mtWarning, [mbYes,mbNo],0)=mrNo then exit
          else if not DeleteFile(GetDocPath+'\'+xModelloWord+'.doc') then begin
               ShowMessage('PROBLEMI NELLA CANCELLAZIONE DEL FILE');
               exit;
          end;

     // Apri Word
     try MsWord:=CreateOleObject('Word.Basic');
     except
          ShowMessage('Non riesco ad aprire Microsoft Word.');
          exit;
     end;
     MsWord.AppShow;
     MsWord.FileNew;
     // intestazione
//     MsWord.Insert('MODELLO PERSONALIZZATO '+QModelliDescrizione.Value+chr(13)+chr(13));
//[/TONI20020929\]
     MsWord.Insert('MODELLO PERSONALIZZATO '+QModelli.FieldByName('Descrizione').Value+chr(13)+chr(13));
     // riempimento con i campi
     QCampiModello.First;
     while not QCampiModello.EOF do begin
          MsWord.Insert(QCampiModello.FieldByName('Campo').Value+': '+QCampiModello.FieldByName('NomeCampo').AsString+chr(13));
          QCampiModello.Next;
          //[/TONI20020929\]FINE
     end;
     MsWord.FileSaveAs(GetDocPath+'\'+xModelloWord+'.doc');
     //MsWord.FileClose;
     //MsWord.AppClose;
end;

//[TONI20021004]DEBUGOK

procedure TGestModelliWordForm.BAprimodelloClick(Sender: TObject);
var xModelloWord: string;
     MSWord: Variant;
begin
     if QModelli.IsEmpty then exit;
     //[/TONI20020929\]
     //     xModelloWord:=QModelliNomeModello.Value;
     xModelloWord:=QModelli.FieldByName('NomeModello').Value;
     //[/TONI20020929\]FINE
     if not FileExists(GetDocPath+'\'+xModelloWord+'.doc') then begin
          MessageDlg('Il file non esiste',mtError, [mbOK],0);
          exit;
     end;
     // Apri Word
     try MsWord:=CreateOleObject('Word.Basic');
     except
          ShowMessage('Non riesco ad aprire Microsoft Word.');
          exit;
     end;
     MsWord.AppShow;
     MsWord.FileOpen(GetDocPath+'\'+xModelloWord+'.doc');
end;

procedure TGestModelliWordForm.FormShow(Sender: TObject);
begin
     Caption:='[M/792] - '+Caption;
     QModelli.Open;
     QCampiModello.open;
end;

function TGestModelliWordForm.GetDocPath: string;
var xH1DocPath: string;
begin
     // restituisce il path della cartella documenti
     xH1DocPath:=LeggiRegistry('H1DocPath');
     if xH1DocPath<>'' then begin
          Result:=xH1DocPath;
     end else begin
          with Data.Q2 do begin
               Close;
               SQL.text:='select DirFileDoc from Global';
               Open;
               if IsEmpty then Result:=''
               else Result:=FieldByName('DirFileDoc').asString;
               Close;
          end;
     end;
end;

end.
