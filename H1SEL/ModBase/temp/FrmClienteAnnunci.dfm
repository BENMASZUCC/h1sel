object ClienteAnnunciFrame: TClienteAnnunciFrame
  Left = 0
  Top = 0
  Width = 472
  Height = 392
  TabOrder = 0
  object Splitter1: TSplitter
    Left = 0
    Top = 196
    Width = 472
    Height = 3
    Cursor = crVSplit
    Align = alTop
  end
  object PanTitoloCompensi: TPanel
    Left = 0
    Top = 0
    Width = 472
    Height = 21
    Align = alTop
    Alignment = taLeftJustify
    Caption = '  Sintesi annunci associati al cliente'
    Color = clGray
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object SpeedButton1: TSpeedButton
      Left = 200
      Top = 0
      Width = 20
      Height = 20
      Glyph.Data = {
        AA030000424DAA03000000000000360000002800000011000000110000000100
        1800000000007403000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FF00FFFFFFFFFFFFFFFFFFDEDFDEBDAEA5EFDFD6F7EFE7F7EFE7F7EFE7E7D7CE
        7B696B525152DEDFDEFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFF7F7EF
        E7DEF7F7F7E7DFDECEB6A5DEB6A5E7CFBDEFEFEFF7F7F7DECFC6393031CECFCE
        FFFFFFFFFFFFFFFFFF00FFFFFFF7F7EFF7F7EFF7F7EFD69E7BC66131C6714ADE
        B6A5C66131C66131DEB69CF7F7F7EFDFD6393831FFFFFFFFFFFFFFFFFF00FFFF
        FFF7E7DEF7F7F7CE795AC66129CE6129C69E84FFFFFFDE966BCE6131C65929D6
        9E7BF7F7F7C6B6B5848684FFFFFFFFFFFF00F7EFE7FFFFFFDEAE8CCE6129CE61
        31CE6131CE6939DE8E63CE6129CE6131CE6131C65929E7C7B5F7EFEF524942FF
        FFFFFFFFFF00F7DFD6FFF7F7CE6939CE6931CE6131CE6131CE8663F7E7D6D671
        42CE6131CE6131CE6131CE7952F7FFFFA5968CADAEADFFFFFF00FFEFEFF7DFCE
        CE6131D66931CE6131CE6131C6795AFFFFFFE7A684CE6129CE6131CE6931CE69
        31F7F7EFDECFBD9C9E9CFFFFFF00FFEFEFF7D7C6D66931D66931CE6131CE6131
        CE6131D6AE9CFFFFFFE79E73CE6129CE6931CE6931F7EFEFDECFC69C9E9CFFFF
        FF00FFEFEFFFE7DEE77142DE6939CE6129CE6129CE6131CE6129E7C7BDFFFFF7
        D67139D66939D67142FFF7F7D6C7BDB5B6B5FFFFFF00F7E7DEFFFFFFF79E6BEF
        7942D68E6BEFDFCEDE7952CE6129DEA684FFFFFFDE8E63DE7139E79E73FFFFFF
        A5968CFFFFFFFFFFFF00F7EFE7FFFFFFFFE7CEFF9E63E78E63EFEFEFFFEFE7EF
        BEA5FFF7EFF7EFEFE78652EF8652FFEFDEFFF7EFC6BEBDFFFFFFFFFFFF00FFFF
        FFF7E7DEFFFFFFFFE7C6FFBE84EFBE94EFE7DEE7E7E7EFE7DEFFB68CFF9E6BFF
        DFC6FFFFFFDECFC6FFFFFFFFFFFFFFFFFF00FFFFFFF7F7EFF7E7E7FFFFFFFFFF
        EFFFF7CEFFE7B5FFD7A5FFD79CFFDFB5FFF7EFFFFFFFEFDFDEEFEFEFFFFFFFFF
        FFFFFFFFFF00FFFFFFFFFFFFF7F7EFF7E7D6FFFFF7FFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFF7F7F7EFE7F7F7EFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF
        FFFFFFFFFFFFF7EFE7F7E7DEFFEFE7FFEFEFFFEFE7EFE7DEFFF7F7FFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFF00}
      OnClick = SpeedButton1Click
    end
  end
  object RxDBGrid1: TRxDBGrid
    Left = 0
    Top = 61
    Width = 472
    Height = 135
    Align = alTop
    DataSource = DsQAnnunciCli
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'Testata'
        Width = 107
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NomeEdizione'
        Title.Caption = 'Edizione'
        Width = 90
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Data'
        Width = 77
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NumModuli'
        Title.Caption = 'N� moduli'
        Width = 50
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Nominativo'
        Title.Caption = 'Responsabile'
        Width = 88
        Visible = True
      end>
  end
  object Panel2: TPanel
    Left = 0
    Top = 21
    Width = 472
    Height = 40
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 2
    object ToolbarButton972: TToolbarButton97
      Left = 359
      Top = 2
      Width = 110
      Height = 36
      Caption = 'Storico/dettaglio prezzi al cliente'
      Glyph.Data = {
        36050000424D3605000000000000360400002800000010000000100000000100
        08000000000000010000120B0000120B000000010000000100000000000000FF
        0000FFFF000084848400C6C6C600C642FF0000FFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00050000000000
        0000000000000000050503030303030303030303030303030005030407070707
        0707070707070403030003040303030303030303030304030300030404040404
        0400000000000000000003040404040404030303030003030300030707070707
        0703030303030303030005030404040404030303000703030300030303030303
        0303030300070303030003040404030003030303030303030300030404010007
        0003070707070707070003040101030003030707070707070700050301020606
        0403030303030303030305030202060604040400050505050505050503030606
        0403030505050505050505050505030303050505050505050505}
      WordWrap = True
      OnClick = ToolbarButton972Click
    end
    object ToolbarButton971: TToolbarButton97
      Left = 1
      Top = 2
      Width = 87
      Height = 36
      Caption = 'Riprendi annuncio'
      Enabled = False
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000130B0000130B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333FF33333333333330003FF3FFFFF3333777003000003333
        300077F777773F333777E00BFBFB033333337773333F7F33333FE0BFBF000333
        330077F3337773F33377E0FBFBFBF033330077F3333FF7FFF377E0BFBF000000
        333377F3337777773F3FE0FBFBFBFBFB039977F33FFFFFFF7377E0BF00000000
        339977FF777777773377000BFB03333333337773FF733333333F333000333333
        3300333777333333337733333333333333003333333333333377333333333333
        333333333333333333FF33333333333330003333333333333777333333333333
        3000333333333333377733333333333333333333333333333333}
      NumGlyphs = 2
      WordWrap = True
    end
  end
  object PanListini: TPanel
    Left = 0
    Top = 199
    Width = 472
    Height = 193
    Align = alClient
    BevelOuter = bvNone
    Caption = 'PanListini'
    TabOrder = 3
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 472
      Height = 21
      Align = alTop
      Alignment = taLeftJustify
      Caption = '  Listino annunci ed eventuali prezzi riservati al cliente'
      Color = clGray
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object Panel3: TPanel
      Left = 0
      Top = 21
      Width = 472
      Height = 40
      Align = alTop
      BevelOuter = bvLowered
      TabOrder = 1
      object ToolbarButton974: TToolbarButton97
        Left = 2
        Top = 1
        Width = 98
        Height = 38
        Caption = 'Imposta prezzo cliente'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00500000000055
          555557777777775F55550FFFFFFFFF0555557F5555555F7FFF5F0FEEEEEE0000
          05007F555555777775770FFFFFF0BFBFB00E7F5F5557FFF557770F0EEEE000FB
          FB0E7F75FF57775555770FF00F0FBFBFBF0E7F57757FFFF555770FE0B00000FB
          FB0E7F575777775555770FFF0FBFBFBFBF0E7F5575FFFFFFF5770FEEE0000000
          FB0E7F555777777755770FFFFF0B00BFB0007F55557577FFF7770FEEEEE0B000
          05557F555557577775550FFFFFFF0B0555557FF5F5F57575F55500F0F0F0F0B0
          555577F7F7F7F7F75F5550707070700B055557F7F7F7F7757FF5507070707050
          9055575757575757775505050505055505557575757575557555}
        NumGlyphs = 2
        WordWrap = True
        OnClick = ToolbarButton974Click
      end
    end
    object RxDBGrid2: TRxDBGrid
      Left = 0
      Top = 61
      Width = 472
      Height = 132
      Align = alClient
      DataSource = DsQListinoCliente
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
      ReadOnly = True
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'TestataEdizione'
          Title.Caption = 'Testata-edizione'
          Width = 139
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DallaData'
          Title.Caption = 'Dal'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QtaDa'
          Title.Alignment = taRightJustify
          Title.Caption = 'Da'
          Width = 25
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QtaA'
          Title.Alignment = taRightJustify
          Title.Caption = 'A'
          Width = 23
          Visible = True
        end
        item
          Color = 13695481
          Expanded = False
          FieldName = 'PrezzoListinoEuro'
          Title.Alignment = taRightJustify
          Title.Caption = 'Listino �.'
          Width = 58
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PrezzoANoiEuro'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          Title.Alignment = taRightJustify
          Title.Caption = 'A noi �.'
          Width = 59
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PrezzoQuestoClienteEuro'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMaroon
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Title.Alignment = taRightJustify
          Title.Caption = 'Al cliente �.'
          Width = 63
          Visible = True
        end>
    end
  end
  object DsQAnnunciCli: TDataSource
    DataSet = QAnnunciCli
    Left = 56
    Top = 120
  end
  object DsQListinoCliente: TDataSource
    DataSet = QListinoCliente
    Left = 56
    Top = 336
  end
  object QAnnunciCli: TADOLinkedQuery
    Connection = Data.DB
    Parameters = <
      item
        Name = 'xIDCliente:'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select Ann_AnnEdizData.ID, Ann_AnnEdizData.IDAnnuncio,'
      '          Ann_AnnEdizData.Data, Ann_AnnEdizData.NumModuli,'
      '          NomeEdizione, Ann_Testate.Denominazione Testata,'
      '          Users.Nominativo'
      
        'from Ann_AnnEdizData, Ann_Edizioni, Ann_Testate, Ann_Annunci,Use' +
        'rs'
      'where Ann_AnnEdizData.IDEdizione=Ann_Edizioni.ID'
      '  and Ann_Edizioni.IDTestata=Ann_Testate.ID'
      '  and Ann_AnnEdizData.IDAnnuncio=Ann_Annunci.ID'
      '  and Ann_Annunci.IDUtente=Users.ID'
      '  and Ann_Annunci.IDCliente=:xIDCliente:'
      'order by Ann_AnnEdizData.Data desc')
    OriginalSQL.Strings = (
      'select Ann_AnnEdizData.ID, Ann_AnnEdizData.IDAnnuncio,'
      '          Ann_AnnEdizData.Data, Ann_AnnEdizData.NumModuli,'
      '          NomeEdizione, Ann_Testate.Denominazione Testata,'
      '          Users.Nominativo'
      
        'from Ann_AnnEdizData, Ann_Edizioni, Ann_Testate, Ann_Annunci,Use' +
        'rs'
      'where Ann_AnnEdizData.IDEdizione=Ann_Edizioni.ID'
      '  and Ann_Edizioni.IDTestata=Ann_Testate.ID'
      '  and Ann_AnnEdizData.IDAnnuncio=Ann_Annunci.ID'
      '  and Ann_Annunci.IDUtente=Users.ID'
      '  and Ann_Annunci.IDCliente=:xIDCliente:'
      'order by Ann_AnnEdizData.Data desc')
    UseFilter = False
    Left = 56
    Top = 88
  end
  object QListinoCliente: TADOLinkedQuery
    Connection = Data.DB
    OnCalcFields = QListinoCliente_OLDCalcFields
    Parameters = <
      item
        Name = 'xIDCliente'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select NomeEdizione, Ann_Testate.Denominazione Testata,'
      '       Ann_EdizListino.*,'
      
        '       Ann_ListinoClienti.ID IDListCli, Ann_ListinoClienti.IDEdi' +
        'zListino, Ann_ListinoClienti.IDCliente, Ann_ListinoClienti.Prezz' +
        'oQuestoClienteLire, Ann_ListinoClienti.PrezzoQuestoClienteEuro, ' +
        'Ann_ListinoClienti.Note'
      'from Ann_EdizListino,Ann_Edizioni,Ann_Testate,Ann_ListinoClienti'
      'where Ann_EdizListino.IDEdizione=Ann_Edizioni.ID'
      '    and Ann_Edizioni.IDTestata=Ann_Testate.ID'
      '    and Ann_EdizListino.ID *= Ann_ListinoClienti.IDEdizListino'
      'and Ann_ListinoClienti.IDCliente=:xIDCliente'
      'order by QtaDa,QtaA,dallaData desc')
    OriginalSQL.Strings = (
      'select NomeEdizione, Ann_Testate.Denominazione Testata,'
      '       Ann_EdizListino.*,'
      '       Ann_ListinoClienti.*'
      'from Ann_EdizListino,Ann_Edizioni,Ann_Testate,Ann_ListinoClienti'
      'where Ann_EdizListino.IDEdizione=Ann_Edizioni.ID'
      '    and Ann_Edizioni.IDTestata=Ann_Testate.ID'
      '    and Ann_EdizListino.ID *= Ann_ListinoClienti.IDEdizListino'
      'and Ann_ListinoClienti.IDCliente=:xIDCliente:'
      'order by QtaDa,QtaA,dallaData desc')
    UseFilter = False
    Left = 56
    Top = 303
    object QListinoClienteNomeEdizione: TStringField
      FieldName = 'NomeEdizione'
      Size = 50
    end
    object QListinoClienteTestata: TStringField
      FieldName = 'Testata'
      Size = 50
    end
    object QListinoClienteID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QListinoClienteIDEdizione: TIntegerField
      FieldName = 'IDEdizione'
    end
    object QListinoClienteDallaData: TDateTimeField
      FieldName = 'DallaData'
    end
    object QListinoClientePrezzoListinoLire: TFloatField
      FieldName = 'PrezzoListinoLire'
    end
    object QListinoClientePrezzoANoiLire: TFloatField
      FieldName = 'PrezzoANoiLire'
    end
    object QListinoClientePrezzoAlClienteLire: TFloatField
      FieldName = 'PrezzoAlClienteLire'
    end
    object QListinoClientePrezzoListinoEuro: TFloatField
      FieldName = 'PrezzoListinoEuro'
      DisplayFormat = '#,###.00'
    end
    object QListinoClientePrezzoANoiEuro: TFloatField
      FieldName = 'PrezzoANoiEuro'
      DisplayFormat = '#,###.##'
    end
    object QListinoClientePrezzoAlClienteEuro: TFloatField
      FieldName = 'PrezzoAlClienteEuro'
      DisplayFormat = '#,###.00'
    end
    object QListinoClienteNote: TStringField
      FieldName = 'Note'
      Size = 80
    end
    object QListinoClienteQtaDa: TSmallintField
      FieldName = 'QtaDa'
    end
    object QListinoClienteQtaA: TSmallintField
      FieldName = 'QtaA'
    end
    object QListinoClienteIDCliente: TIntegerField
      FieldName = 'IDCliente'
    end
    object QListinoClienteIDEdizListino: TIntegerField
      FieldName = 'IDEdizListino'
    end
    object QListinoClienteIDCliente_1: TIntegerField
      FieldName = 'IDCliente_1'
    end
    object QListinoClientePrezzoQuestoClienteLire: TFloatField
      FieldName = 'PrezzoQuestoClienteLire'
    end
    object QListinoClientePrezzoQuestoClienteEuro: TFloatField
      FieldName = 'PrezzoQuestoClienteEuro'
      DisplayFormat = '#,###.00'
    end
    object QListinoClienteNote_1: TStringField
      FieldName = 'Note_1'
      Size = 80
    end
    object QListinoClienteTestataEdizione: TStringField
      FieldKind = fkCalculated
      FieldName = 'TestataEdizione'
      Size = 60
      Calculated = True
    end
  end
end
