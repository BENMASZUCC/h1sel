object DettOffertaForm: TDettOffertaForm
  Left = 233
  Top = 143
  ActiveControl = CBTipo
  BorderStyle = bsDialog
  Caption = 'Dettaglio offerta'
  ClientHeight = 76
  ClientWidth = 368
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 5
    Top = 8
    Width = 21
    Height = 13
    Caption = 'Tipo'
  end
  object Label2: TLabel
    Left = 5
    Top = 32
    Width = 55
    Height = 13
    Caption = 'Descrizione'
  end
  object Label3: TLabel
    Left = 5
    Top = 56
    Width = 35
    Height = 13
    Caption = 'Importo'
  end
  object BitBtn1: TBitBtn
    Left = 280
    Top = 3
    Width = 86
    Height = 33
    TabOrder = 3
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 280
    Top = 37
    Width = 86
    Height = 33
    Caption = 'Annulla'
    TabOrder = 4
    Kind = bkCancel
  end
  object CBTipo: TComboBox
    Left = 69
    Top = 5
    Width = 156
    Height = 21
    ItemHeight = 13
    TabOrder = 0
    Items.Strings = (
      'anticipo ricerca'
      'presentazione candidato'
      'inserimento candidato'
      'pubblicazione annuncio'
      'altro')
  end
  object EDesc: TEdit
    Left = 69
    Top = 29
    Width = 195
    Height = 21
    TabOrder = 1
  end
  object RxImporto: TRxCalcEdit
    Left = 69
    Top = 52
    Width = 118
    Height = 21
    AutoSize = False
    DisplayFormat = '#,###'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    FormatOnEditing = True
    NumGlyphs = 2
    ParentFont = False
    TabOrder = 2
  end
end
