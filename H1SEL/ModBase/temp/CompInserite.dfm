�
 TCOMPINSERITEFORM 0u  TPF0TCompInseriteFormCompInseriteFormLeftETop?BorderStylebsDialogCaption1Inserimento competenze relative al ruolo inseritoClientHeight#ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrder	PositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TLabelLabel1LeftTopWidth$HeightCaptionwper abilitare o disabilitare l'inserimento di una competenza fare DOPPIO CLICK sulla casella relativa alla colonna "s�"Font.CharsetDEFAULT_CHARSET
Font.ColorclPurpleFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontWordWrap	  TBitBtnBitBtn1Left[TopWidthaHeight!TabOrder OnClickBitBtn1ClickKindbkOK  TPanelPanel1LeftTopWidthCHeight
BevelOuterbvNoneCaptionPanel1EnabledTabOrder TLabelLabel2Left TopWidthCHeightCaptionRuolo inserito:  TEditERuoloLeftHTopWidth� HeightTabOrder    TPanelPanel73LeftTop#WidthKHeight	AlignmenttaLeftJustifyCaption8  Competenze associate al ruolo (se non gi� in possesso)ColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclWhiteFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TStringAlignGridSG1LeftTop8WidthJHeight� ColCount	FixedCols OptionsgoFixedVertLinegoFixedHorzLine
goVertLine
goHorzLine	goEditing TabOrder
OnDblClickSG1DblClickOnGetEditMaskSG1GetEditMaskOnSelectCellSG1SelectCellEditable	ColWidths� & Cells  
Competenza Valore s� PropCell PropCol     ��	Wingdings  �  PropRow PropFixedCol PropFixedRow   TBitBtnBitBtn2Left[Top(WidthaHeight!Hint*evita l'inserimento di tutte le competenzeCaptionAnnullaParentShowHintShowHint	TabOrderOnClickBitBtn2ClickKindbkCancel   