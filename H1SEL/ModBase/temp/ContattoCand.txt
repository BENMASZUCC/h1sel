object ContattoCandForm: TContattoCandForm
  Left = 526
  Top = 152
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  ActiveControl = DEData
  BorderIcons = []
  BorderStyle = bsDialog
  Caption = 'Registrazione contatto'
  ClientHeight = 605
  ClientWidth = 371
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label9: TLabel
    Left = 372
    Top = 7
    Width = 17
    Height = 13
    Caption = 'Fax'
    FocusControl = DBEdit5
  end
  object Label6: TLabel
    Left = 5
    Top = 514
    Width = 23
    Height = 13
    Caption = 'Note'
  end
  object Panel1: TPanel
    Left = 5
    Top = 118
    Width = 210
    Height = 33
    BevelInner = bvLowered
    TabOrder = 0
    object Label2: TLabel
      Left = 9
      Top = 10
      Width = 23
      Height = 13
      Caption = 'Data'
    end
    object Label3: TLabel
      Left = 145
      Top = 9
      Width = 17
      Height = 13
      Caption = 'Ora'
    end
    object DEData: TDateEdit97
      Left = 40
      Top = 6
      Width = 91
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      ColorCalendar.ColorValid = clBlue
      DayNames.Monday = 'lu'
      DayNames.Tuesday = 'ma'
      DayNames.Wednesday = 'me'
      DayNames.Thursday = 'gi'
      DayNames.Friday = 've'
      DayNames.Saturday = 'sa'
      DayNames.Sunday = 'do'
      MonthNames.January = 'gennaio'
      MonthNames.February = 'febbraio'
      MonthNames.March = 'marzo'
      MonthNames.April = 'aprile'
      MonthNames.May = 'maggio'
      MonthNames.June = 'giugno'
      MonthNames.July = 'luglio'
      MonthNames.August = 'agosto'
      MonthNames.September = 'settembre'
      MonthNames.October = 'ottobre'
      MonthNames.November = 'novembre'
      MonthNames.December = 'dicembre'
      Options = [doButtonTabStop, doCanPopup, doIsMasked, doShowCancel, doShowToday]
    end
    object MEOra: TMaskEdit
      Left = 169
      Top = 6
      Width = 35
      Height = 21
      EditMask = '!90:00;1;_'
      MaxLength = 5
      TabOrder = 1
      Text = '18.00'
    end
  end
  object BitBtn1: TBitBtn
    Left = 274
    Top = 532
    Width = 93
    Height = 32
    TabOrder = 3
    OnClick = BitBtn1Click
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 274
    Top = 567
    Width = 93
    Height = 32
    Caption = 'Annulla'
    TabOrder = 4
    Kind = bkCancel
  end
  object DBEdit5: TDBEdit
    Left = 372
    Top = 23
    Width = 173
    Height = 21
    DataField = 'Fax'
    TabOrder = 5
  end
  object Panel3: TPanel
    Left = 0
    Top = 0
    Width = 371
    Height = 113
    Align = alTop
    BevelOuter = bvNone
    Enabled = False
    TabOrder = 6
    object Label1: TLabel
      Left = 6
      Top = 2
      Width = 51
      Height = 13
      Caption = 'Candidato:'
    end
    object Label7: TLabel
      Left = 5
      Top = 41
      Width = 84
      Height = 13
      Caption = 'Recapiti telefonici'
    end
    object Label8: TLabel
      Left = 208
      Top = 41
      Width = 40
      Height = 13
      Caption = 'Cellulare'
    end
    object Label5: TLabel
      Left = 5
      Top = 78
      Width = 54
      Height = 13
      Caption = 'email (Priv.)'
    end
    object ECogn: TEdit
      Left = 6
      Top = 16
      Width = 139
      Height = 21
      Color = clYellow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      Text = 'ECogn'
    end
    object ENome: TEdit
      Left = 148
      Top = 16
      Width = 121
      Height = 21
      Color = clYellow
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      Text = 'ENome'
    end
    object ERec: TEdit
      Left = 5
      Top = 56
      Width = 202
      Height = 21
      TabOrder = 2
      Text = 'ERec'
    end
    object ECell: TEdit
      Left = 209
      Top = 56
      Width = 155
      Height = 21
      TabOrder = 3
      Text = 'ECell'
    end
    object eMail: TEdit
      Left = 5
      Top = 93
      Width = 359
      Height = 21
      TabOrder = 4
      Text = 'ERec'
    end
  end
  object RadioGroup2: TRadioGroup
    Left = 5
    Top = 382
    Width = 116
    Height = 130
    Caption = 'Esito contatto'
    Items.Strings = (
      'da richiamare'
      'deve richiamare'
      'fissato colloquio'
      'non accetta'
      'altro esito')
    TabOrder = 1
    OnClick = RadioGroup2Click
  end
  object PanAltroEsito: TPanel
    Left = 126
    Top = 466
    Width = 142
    Height = 46
    BevelOuter = bvLowered
    TabOrder = 2
    Visible = False
    object Label10: TLabel
      Left = 4
      Top = 4
      Width = 87
      Height = 13
      Caption = 'Breve descrizione:'
    end
    object EDesc: TEdit
      Left = 5
      Top = 19
      Width = 131
      Height = 21
      MaxLength = 20
      TabOrder = 0
    end
  end
  object PanMotivoNonAcc: TPanel
    Left = 126
    Top = 416
    Width = 240
    Height = 46
    BevelOuter = bvLowered
    TabOrder = 7
    Visible = False
    object Label4: TLabel
      Left = 4
      Top = 4
      Width = 117
      Height = 13
      Caption = 'Motivo non accettazione'
    end
    object SpeedButton1: TSpeedButton
      Left = 209
      Top = 17
      Width = 27
      Height = 25
      Hint = 'seleziona motivo '#13#10'non accettazione'
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        33333333333333333333333333333333333333333333333333FF333333333333
        3000333333FFFFF3F77733333000003000B033333777773777F733330BFBFB00
        E00033337FFF3377F7773333000FBFB0E000333377733337F7773330FBFBFBF0
        E00033F7FFFF3337F7773000000FBFB0E000377777733337F7770BFBFBFBFBF0
        E00073FFFFFFFF37F777300000000FB0E000377777777337F7773333330BFB00
        000033333373FF77777733333330003333333333333777333333333333333333
        3333333333333333333333333333333333333333333333333333333333333333
        3333333333333333333333333333333333333333333333333333}
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = SpeedButton1Click
    end
    object EMotNonAcc: TEdit
      Left = 5
      Top = 19
      Width = 201
      Height = 21
      MaxLength = 20
      ReadOnly = True
      TabOrder = 0
    end
  end
  object DBGrid1: TDBGrid
    Left = 6
    Top = 157
    Width = 210
    Height = 220
    DataSource = DsQTipiContatti
    ReadOnly = True
    TabOrder = 8
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'TipoContatto'
        Title.Caption = 'Tipologia di contatto'
        Width = 175
        Visible = True
      end>
  end
  object eNote: TMemo
    Left = 6
    Top = 527
    Width = 257
    Height = 73
    ScrollBars = ssVertical
    TabOrder = 9
  end
  object QAnag: TADOLinkedQuery
    Connection = Data.DB
    Parameters = <>
    UseFilter = False
    Left = 296
    Top = 16
  end
  object QTipiContatti: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from TipiContattiCand')
    Left = 160
    Top = 184
    object QTipiContattiID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QTipiContattiTipoContatto: TStringField
      FieldName = 'TipoContatto'
      Size = 50
    end
    object QTipiContattiIDEvento: TIntegerField
      FieldName = 'IDEvento'
    end
  end
  object DsQTipiContatti: TDataSource
    DataSet = QTipiContatti
    Left = 152
    Top = 216
  end
end
