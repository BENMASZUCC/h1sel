object SelRicCandForm: TSelRicCandForm
  Left = 288
  Top = 146
  ActiveControl = DBGrid1
  BorderStyle = bsDialog
  Caption = 'Selezione ricerca'
  ClientHeight = 222
  ClientWidth = 493
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object SpeedButton1: TSpeedButton
    Left = 456
    Top = 80
    Width = 20
    Height = 20
    Glyph.Data = {
      AA030000424DAA03000000000000360000002800000011000000110000000100
      1800000000007403000000000000000000000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FF00FFFFFFFFFFFFFFFFFFDEDFDEBDAEA5EFDFD6F7EFE7F7EFE7F7EFE7E7D7CE
      7B696B525152DEDFDEFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFF7F7EF
      E7DEF7F7F7E7DFDECEB6A5DEB6A5E7CFBDEFEFEFF7F7F7DECFC6393031CECFCE
      FFFFFFFFFFFFFFFFFF00FFFFFFF7F7EFF7F7EFF7F7EFD69E7BC66131C6714ADE
      B6A5C66131C66131DEB69CF7F7F7EFDFD6393831FFFFFFFFFFFFFFFFFF00FFFF
      FFF7E7DEF7F7F7CE795AC66129CE6129C69E84FFFFFFDE966BCE6131C65929D6
      9E7BF7F7F7C6B6B5848684FFFFFFFFFFFF00F7EFE7FFFFFFDEAE8CCE6129CE61
      31CE6131CE6939DE8E63CE6129CE6131CE6131C65929E7C7B5F7EFEF524942FF
      FFFFFFFFFF00F7DFD6FFF7F7CE6939CE6931CE6131CE6131CE8663F7E7D6D671
      42CE6131CE6131CE6131CE7952F7FFFFA5968CADAEADFFFFFF00FFEFEFF7DFCE
      CE6131D66931CE6131CE6131C6795AFFFFFFE7A684CE6129CE6131CE6931CE69
      31F7F7EFDECFBD9C9E9CFFFFFF00FFEFEFF7D7C6D66931D66931CE6131CE6131
      CE6131D6AE9CFFFFFFE79E73CE6129CE6931CE6931F7EFEFDECFC69C9E9CFFFF
      FF00FFEFEFFFE7DEE77142DE6939CE6129CE6129CE6131CE6129E7C7BDFFFFF7
      D67139D66939D67142FFF7F7D6C7BDB5B6B5FFFFFF00F7E7DEFFFFFFF79E6BEF
      7942D68E6BEFDFCEDE7952CE6129DEA684FFFFFFDE8E63DE7139E79E73FFFFFF
      A5968CFFFFFFFFFFFF00F7EFE7FFFFFFFFE7CEFF9E63E78E63EFEFEFFFEFE7EF
      BEA5FFF7EFF7EFEFE78652EF8652FFEFDEFFF7EFC6BEBDFFFFFFFFFFFF00FFFF
      FFF7E7DEFFFFFFFFE7C6FFBE84EFBE94EFE7DEE7E7E7EFE7DEFFB68CFF9E6BFF
      DFC6FFFFFFDECFC6FFFFFFFFFFFFFFFFFF00FFFFFFF7F7EFF7E7E7FFFFFFFFFF
      EFFFF7CEFFE7B5FFD7A5FFD79CFFDFB5FFF7EFFFFFFFEFDFDEEFEFEFFFFFFFFF
      FFFFFFFFFF00FFFFFFFFFFFFF7F7EFF7E7D6FFFFF7FFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFF7F7F7EFE7F7F7EFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF
      FFFFFFFFFFFFF7EFE7F7E7DEFFEFE7FFEFEFFFEFE7EFE7DEFFF7F7FFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFF00}
    OnClick = SpeedButton1Click
  end
  object BitBtn1: TBitBtn
    Left = 397
    Top = 2
    Width = 93
    Height = 36
    TabOrder = 0
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 397
    Top = 40
    Width = 93
    Height = 36
    Caption = 'Annulla'
    TabOrder = 1
    Kind = bkCancel
  end
  object Panel124: TPanel
    Left = 3
    Top = 3
    Width = 390
    Height = 21
    Alignment = taLeftJustify
    Caption = '  Ricerche attive cui � associato il soggetto'
    Color = clGray
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
  end
  object DBGrid1: TDBGrid
    Left = 4
    Top = 25
    Width = 389
    Height = 193
    DataSource = DsQRicCand
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'Cliente'
        Width = 179
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Posizione'
        Width = 175
        Visible = True
      end>
  end
  object DsQRicCand: TDataSource
    DataSet = QRicCand
    Left = 96
    Top = 120
  end
  object QRicCand: TADOLinkedQuery
    Connection = Data.DB
    Parameters = <
      item
        Name = 'xIDAnag:'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'select EBC_Clienti.Descrizione Cliente, Mansioni.Descrizione Pos' +
        'izione,'
      
        '          EBC_CandidatiRicerche.ID IDCandRic, EBC_Clienti.ID IDC' +
        'liente,'
      '          IDMansione, TitoloCliente,ebc_ricerche.id idricerca'
      'from EBC_Ricerche,EBC_Clienti,Mansioni,EBC_CandidatiRicerche'
      'where EBC_Ricerche.IDCliente=EBC_Clienti.ID'
      '  and EBC_Ricerche.IDMansione=Mansioni.ID'
      '  and EBC_Ricerche.ID=EBC_CandidatiRicerche.IDRicerca'
      'and EBC_Ricerche.IDStatoRic=1'
      'and EBC_CandidatiRicerche.IDAnagrafica=:xIDAnag:')
    OriginalSQL.Strings = (
      
        'select EBC_Clienti.Descrizione Cliente, Mansioni.Descrizione Pos' +
        'izione,'
      
        '          EBC_CandidatiRicerche.ID IDCandRic, EBC_Clienti.ID IDC' +
        'liente,'
      '          IDMansione, TitoloCliente'
      
        'from EBC_Ricerche,EBC_Clienti,Mansioni,EBC_CandidatiRicerche wit' +
        'h (updlock)'
      'where EBC_Ricerche.IDCliente=EBC_Clienti.ID'
      '  and EBC_Ricerche.IDMansione=Mansioni.ID'
      '  and EBC_Ricerche.ID=EBC_CandidatiRicerche.IDRicerca'
      'and EBC_Ricerche.IDStatoRic=1'
      'and EBC_CandidatiRicerche.IDAnagrafica=:xIDAnag:')
    UseFilter = False
    Left = 96
    Top = 80
    object QRicCandCliente: TStringField
      FieldName = 'Cliente'
      Size = 50
    end
    object QRicCandPosizione: TStringField
      FieldName = 'Posizione'
      Size = 40
    end
    object QRicCandIDCandRic: TAutoIncField
      FieldName = 'IDCandRic'
      ReadOnly = True
    end
    object QRicCandIDCliente: TAutoIncField
      FieldName = 'IDCliente'
      ReadOnly = True
    end
    object QRicCandIDMansione: TIntegerField
      FieldName = 'IDMansione'
    end
    object QRicCandTitoloCliente: TStringField
      FieldName = 'TitoloCliente'
      Size = 50
    end
    object QRicCandidricerca: TAutoIncField
      FieldName = 'idricerca'
      ReadOnly = True
    end
  end
end
