object CandUtenteFrame: TCandUtenteFrame
  Left = 0
  Top = 0
  Width = 666
  Height = 373
  TabOrder = 0
  object Panel12: TPanel
    Left = 0
    Top = 0
    Width = 666
    Height = 21
    Align = alTop
    Alignment = taLeftJustify
    Caption = '  Situazione candidati associati all'#39'utente attualmente connesso'
    Color = clGray
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
  end
  object Panel1: TPanel
    Left = 0
    Top = 21
    Width = 666
    Height = 41
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 1
    object BLegenda: TBitBtn
      Left = 627
      Top = 5
      Width = 35
      Height = 33
      Hint = 'legenda e parametri'
      Anchors = [akTop, akRight]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clMaroon
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BLegendaClick
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333FFFFF3333333333F797F3333333333F737373FF333333BFB999BFB
        33333337737773773F3333BFBF797FBFB33333733337333373F33BFBFBFBFBFB
        FB3337F33333F33337F33FBFBFB9BFBFBF3337333337F333373FFBFBFBF97BFB
        FBF37F333337FF33337FBFBFBFB99FBFBFB37F3333377FF3337FFBFBFBFB99FB
        FBF37F33333377FF337FBFBF77BF799FBFB37F333FF3377F337FFBFB99FB799B
        FBF373F377F3377F33733FBF997F799FBF3337F377FFF77337F33BFBF99999FB
        FB33373F37777733373333BFBF999FBFB3333373FF77733F7333333BFBFBFBFB
        3333333773FFFF77333333333FBFBF3333333333377777333333}
      NumGlyphs = 2
    end
  end
  object RxDBGrid1: TRxDBGrid
    Left = 0
    Top = 62
    Width = 666
    Height = 311
    Align = alClient
    DataSource = DsQCandUtente
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnTitleClick = RxDBGrid1TitleClick
    OnGetCellParams = RxDBGrid1GetCellParams
    Columns = <
      item
        Expanded = False
        FieldName = 'CVNumero'
        Title.Caption = 'N� CV'
        Width = 43
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Cognome'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        Width = 129
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Nome'
        Width = 124
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DataNascita'
        Title.Caption = 'Nato il'
        Width = 68
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'RecapitiTelefonici'
        Title.Caption = 'Telefono'
        Width = 109
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Cellulare'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ggDataUltimoColloquio'
        Title.Caption = 'Col'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clMaroon
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 28
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ggDataUltimoContatto'
        Title.Caption = 'Uc'
        Width = 24
        Visible = True
      end>
  end
  object QCandUtente_OLD: TQuery
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      
        'select distinct Anagrafica.ID,CVNumero,Cognome,Nome,DataNascita,' +
        'RecapitiTelefonici,Cellulare,'
      
        '       DATEDIFF(day, EBC_Colloqui.Data, getdate()) ggDataUltimoC' +
        'olloquio,'
      
        '       DATEDIFF(day, EBC_ContattiCandidati.Data, getdate()) ggDa' +
        'taUltimoContatto  '
      'from Anagrafica,EBC_Colloqui,EBC_ContattiCandidati'
      'where Anagrafica.ID *= EBC_Colloqui.IDAnagrafica'
      'and EBC_Colloqui.Data='
      '    (select max(Data) from EBC_Colloqui'
      '     where Anagrafica.ID *= EBC_Colloqui.IDAnagrafica)'
      'and Anagrafica.ID *= EBC_ContattiCandidati.IDAnagrafica'
      '    and EBC_ContattiCandidati.Data='
      '    (select max(Data) from EBC_ContattiCandidati'
      '     where Anagrafica.ID *= EBC_ContattiCandidati.IDAnagrafica)'
      'and Anagrafica.IDUtente=:xIDUtente'
      'order by Cognome,Nome')
    Left = 96
    Top = 200
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'xIDUtente'
        ParamType = ptUnknown
      end>
    object QCandUtente_OLDID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.Anagrafica.ID'
    end
    object QCandUtente_OLDCVNumero: TIntegerField
      FieldName = 'CVNumero'
      Origin = 'EBCDB.Anagrafica.CVNumero'
    end
    object QCandUtente_OLDCognome: TStringField
      FieldName = 'Cognome'
      Origin = 'EBCDB.Anagrafica.Cognome'
      FixedChar = True
      Size = 30
    end
    object QCandUtente_OLDNome: TStringField
      FieldName = 'Nome'
      Origin = 'EBCDB.Anagrafica.Nome'
      FixedChar = True
      Size = 30
    end
    object QCandUtente_OLDDataNascita: TDateTimeField
      FieldName = 'DataNascita'
      Origin = 'EBCDB.Anagrafica.DataNascita'
    end
    object QCandUtente_OLDRecapitiTelefonici: TStringField
      FieldName = 'RecapitiTelefonici'
      Origin = 'EBCDB.Anagrafica.RecapitiTelefonici'
      FixedChar = True
      Size = 30
    end
    object QCandUtente_OLDCellulare: TStringField
      FieldName = 'Cellulare'
      Origin = 'EBCDB.Anagrafica.Cellulare'
      FixedChar = True
      Size = 15
    end
    object QCandUtente_OLDggDataUltimoColloquio: TIntegerField
      FieldName = 'ggDataUltimoColloquio'
    end
    object QCandUtente_OLDggDataUltimoContatto: TIntegerField
      FieldName = 'ggDataUltimoContatto'
    end
  end
  object DsQCandUtente: TDataSource
    DataSet = QCandUtente
    Left = 168
    Top = 200
  end
  object QCandUtente: TADOLinkedQuery
    Connection = Data.DB
    Parameters = <>
    OriginalSQL.Strings = (
      
        'select distinct Anagrafica.ID,CVNumero,Cognome,Nome,DataNascita,' +
        'RecapitiTelefonici,Cellulare,'
      
        '       DATEDIFF(day, EBC_Colloqui.Data, getdate()) ggDataUltimoC' +
        'olloquio,'
      
        '       DATEDIFF(day, EBC_ContattiCandidati.Data, getdate()) ggDa' +
        'taUltimoContatto  '
      'from Anagrafica,EBC_Colloqui,EBC_ContattiCandidati'
      'where Anagrafica.ID *= EBC_Colloqui.IDAnagrafica'
      'and EBC_Colloqui.Data='
      '    (select max(Data) from EBC_Colloqui'
      '     where Anagrafica.ID *= EBC_Colloqui.IDAnagrafica)'
      'and Anagrafica.ID *= EBC_ContattiCandidati.IDAnagrafica'
      '    and EBC_ContattiCandidati.Data='
      '    (select max(Data) from EBC_ContattiCandidati'
      '     where Anagrafica.ID *= EBC_ContattiCandidati.IDAnagrafica)'
      'and Anagrafica.IDUtente=:xIDUtente:'
      'order by Cognome,Nome'
      '')
    UseFilter = False
    Left = 136
    Top = 200
  end
end
