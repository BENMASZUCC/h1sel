object FileApertoForm: TFileApertoForm
  Left = 274
  Top = 186
  BorderStyle = bsDialog
  Caption = 'File aperto'
  ClientHeight = 128
  ClientWidth = 513
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCloseQuery = FormCloseQuery
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 5
    Top = 48
    Width = 138
    Height = 13
    Caption = 'Nome e percorso del file:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object LNomeFile: TLabel
    Left = 5
    Top = 63
    Width = 48
    Height = 13
    Caption = 'LNomeFile'
  end
  object Label3: TLabel
    Left = 5
    Top = 93
    Width = 506
    Height = 26
    Caption = 
      'Quando terminata la visualizzazione o la modifica del file, chiu' +
      'dere la relativa applicazione'#13#10'e premere il pulsante "Salva ed e' +
      'sci"'
    Font.Charset = ANSI_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 513
    Height = 41
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object Label2: TLabel
      Left = 11
      Top = 13
      Width = 160
      Height = 13
      Caption = 'Il file sotto indicato � aperto'
      Font.Charset = ANSI_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object BitBtn1: TBitBtn
      Left = 401
      Top = 4
      Width = 108
      Height = 32
      Anchors = [akTop, akRight]
      Caption = 'Salva ed esci'
      TabOrder = 0
      OnClick = BitBtn1Click
      Kind = bkOK
    end
  end
end
