object DettAnagQuestForm: TDettAnagQuestForm
  Left = 280
  Top = 103
  Width = 815
  Height = 510
  Caption = 'Dettaglio risposte date al questionario'
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 807
    Height = 45
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object Label1: TLabel
      Left = 6
      Top = 5
      Width = 65
      Height = 13
      Caption = 'Questionario:'
    end
    object DBText1: TDBText
      Left = 78
      Top = 5
      Width = 366
      Height = 14
      DataField = 'Questionario'
      DataSource = DsQInfo
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 6
      Top = 20
      Width = 48
      Height = 13
      Caption = 'Soggetto:'
    end
    object DBText2: TDBText
      Left = 78
      Top = 21
      Width = 273
      Height = 14
      DataField = 'Soggetto'
      DataSource = DsQInfo
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object BitBtn1: TBitBtn
      Left = 703
      Top = 5
      Width = 100
      Height = 35
      Anchors = [akTop, akRight]
      Caption = 'Esci'
      TabOrder = 0
      Kind = bkOK
    end
  end
  object dxDBGrid1: TdxDBGrid
    Left = 0
    Top = 45
    Width = 807
    Height = 438
    Bands = <
      item
      end>
    DefaultLayout = True
    HeaderPanelRowCount = 1
    KeyField = 'ID'
    ShowGroupPanel = True
    ShowSummaryFooter = True
    SummaryGroups = <
      item
        DefaultGroup = False
        SummaryItems = <
          item
            ColumnName = 'dxDBGrid1Punteggio'
            SummaryField = 'Punteggio'
            SummaryType = cstSum
          end>
        Name = 'dxDBGrid1SummaryGroup2'
      end>
    SummarySeparator = ', '
    Align = alClient
    TabOrder = 1
    DataSource = DsQRisposte
    Filter.Criteria = {00000000}
    OptionsBehavior = [edgoAutoSort, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoTabThrough, edgoVertThrough]
    OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
    OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoIndicator, edgoUseBitmap]
    object dxDBGrid1ID: TdxDBGridMaskColumn
      Visible = False
      Width = 60
      BandIndex = 0
      RowIndex = 0
      FieldName = 'ID'
    end
    object dxDBGrid1Raggruppamento: TdxDBGridMaskColumn
      Width = 114
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Raggruppamento'
      SummaryType = cstSum
      SummaryField = 'Punteggio'
      SummaryFormat = '- Totale punteggio = #,###'
    end
    object dxDBGrid1Sezione: TdxDBGridMaskColumn
      Width = 146
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Sezione'
    end
    object dxDBGrid1Rif: TdxDBGridMaskColumn
      Width = 26
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Rif'
    end
    object dxDBGrid1Domanda: TdxDBGridMaskColumn
      Width = 259
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Domanda'
    end
    object dxDBGrid1Risposta: TdxDBGridMaskColumn
      Width = 144
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Risposta'
    end
    object dxDBGrid1Peso: TdxDBGridMaskColumn
      Width = 37
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Peso'
    end
    object dxDBGrid1Punteggio: TdxDBGridMaskColumn
      Width = 65
      BandIndex = 0
      RowIndex = 0
      FieldName = 'Punteggio'
      SummaryFooterType = cstSum
      SummaryFooterField = 'Punteggio'
    end
  end
  object DsQRisposte: TDataSource
    DataSet = QRisposte
    Left = 28
    Top = 359
  end
  object QRisposte: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'x'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select AR.ID, RAGG.Raggruppamento, SEZ.Gruppo Sezione,'
      
        #9'D.Rif, D.TestoDomanda Domanda, D.Peso, R.TestoRisposta Risposta' +
        ', R.Punteggio'
      'from QUEST_AnagRisposte AR'
      'join QUEST_Domande D on AR.IDDomanda = D.ID'
      'join QUEST_Risposte R on AR.IDRisposta = R.ID'
      'join QUEST_GruppiDomande SEZ on D.IDGruppo = SEZ.ID'
      
        'left outer join QUEST_Raggruppamenti RAGG on D.IDRaggruppamento ' +
        '= RAGG.ID'
      'where AR.IDAnagQuest = :x'
      'order by Raggruppamento')
    Left = 28
    Top = 325
    object QRisposteID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object QRisposteRaggruppamento: TStringField
      FieldName = 'Raggruppamento'
      Size = 100
    end
    object QRisposteSezione: TStringField
      FieldName = 'Sezione'
      Size = 100
    end
    object QRisposteRif: TStringField
      FieldName = 'Rif'
      Size = 5
    end
    object QRisposteDomanda: TStringField
      FieldName = 'Domanda'
      Size = 1024
    end
    object QRispostePeso: TIntegerField
      FieldName = 'Peso'
    end
    object QRisposteRisposta: TStringField
      FieldName = 'Risposta'
      Size = 1024
    end
    object QRispostePunteggio: TFloatField
      FieldName = 'Punteggio'
    end
  end
  object QInfo: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'x'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select A.Cognome+'#39' '#39'+A.Nome Soggetto, Q.TipoDomanda Questionario'
      'from QUEST_AnagQuest AQ'
      'join Anagrafica A on AQ.IDAnagCompilatore = A.ID'
      'join QUEST_TipiDomande Q on AQ.IDQuest = Q.ID'
      'where AQ.ID = :x')
    Left = 560
    Top = 8
    object QInfoSoggetto: TStringField
      FieldName = 'Soggetto'
      ReadOnly = True
      Size = 61
    end
    object QInfoQuestionario: TStringField
      FieldName = 'Questionario'
      Size = 50
    end
  end
  object DsQInfo: TDataSource
    DataSet = QInfo
    Left = 592
    Top = 8
  end
end
