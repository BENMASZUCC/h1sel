object SelCriterioWizardForm: TSelCriterioWizardForm
  Left = 391
  Top = 258
  BorderStyle = bsDialog
  Caption = 'Wizard di creazione/modifica criterio di ricerca'
  ClientHeight = 354
  ClientWidth = 480
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PanSituazione: TPanel
    Left = 0
    Top = 0
    Width = 480
    Height = 60
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object Label1: TLabel
      Left = 6
      Top = 6
      Width = 38
      Height = 13
      Caption = 'Tabella:'
    end
    object DBText1: TDBText
      Left = 70
      Top = 6
      Width = 201
      Height = 17
      DataField = 'DescTabella'
      DataSource = DsQTabelle
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 6
      Top = 22
      Width = 37
      Height = 13
      Caption = 'Campo:'
    end
    object DBText2: TDBText
      Left = 70
      Top = 22
      Width = 201
      Height = 17
      DataField = 'DescCampo'
      DataSource = DsQCampi
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 6
      Top = 38
      Width = 54
      Height = 13
      Caption = 'Operatore:'
    end
    object DBText3: TDBText
      Left = 70
      Top = 38
      Width = 227
      Height = 17
      DataField = 'DescOperatore'
      DataSource = DsQOperatori
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object PanButtons: TPanel
    Left = 0
    Top = 308
    Width = 480
    Height = 46
    Align = alBottom
    BevelOuter = bvLowered
    TabOrder = 1
    object BIndietro: TBitBtn
      Left = 165
      Top = 6
      Width = 78
      Height = 34
      Caption = '< Indietro'
      TabOrder = 0
      OnClick = BIndietroClick
    end
    object BAvanti: TBitBtn
      Left = 243
      Top = 6
      Width = 78
      Height = 34
      Caption = 'Avanti >'
      TabOrder = 1
      OnClick = BAvantiClick
    end
    object BitBtn1: TBitBtn
      Left = 7
      Top = 6
      Width = 78
      Height = 34
      Caption = 'Annulla'
      TabOrder = 2
      OnClick = BIndietroClick
      Kind = bkCancel
    end
    object BFine: TBitBtn
      Left = 401
      Top = 6
      Width = 78
      Height = 34
      Caption = 'Fine'
      TabOrder = 3
      Kind = bkOK
    end
  end
  object PCUnico: TPageControl
    Left = 0
    Top = 60
    Width = 480
    Height = 248
    ActivePage = TSValore
    Align = alClient
    TabOrder = 2
    OnChange = PCUnicoChange
    object TsTabella: TTabSheet
      Caption = 'TsTabella'
      object Panel10: TPanel
        Left = 0
        Top = 0
        Width = 472
        Height = 21
        Align = alTop
        Alignment = taLeftJustify
        Caption = '  Seleziona la tabella dall'#39'elenco'
        Color = clGray
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object LTot: TLabel
          Left = 591
          Top = 4
          Width = 37
          Height = 13
          Anchors = [akTop, akRight]
          AutoSize = False
          Caption = 'LTot'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clYellow
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          Visible = False
        end
        object Label5: TLabel
          Left = 518
          Top = 4
          Width = 69
          Height = 13
          Alignment = taRightJustify
          Anchors = [akTop, akRight]
          AutoSize = False
          Caption = 'Totale query:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clYellow
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
      end
      object DBGTabelle: TdxDBGrid
        Left = 0
        Top = 21
        Width = 472
        Height = 199
        Bands = <
          item
          end>
        DefaultLayout = True
        HeaderPanelRowCount = 1
        KeyField = 'DescTabella'
        SummaryGroups = <>
        SummarySeparator = ', '
        Align = alClient
        Color = clInfoBk
        TabOrder = 1
        DataSource = DsQTabelle
        Filter.Criteria = {00000000}
        LookAndFeel = lfFlat
        OptionsBehavior = [edgoAutoSearch, edgoAutoSort, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoTabThrough, edgoVertThrough]
        OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
        OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoIndicator, edgoUseBitmap]
        object DBGTabelleDescTabella: TdxDBGridMaskColumn
          Caption = 'Tabella'
          BandIndex = 0
          RowIndex = 0
          FieldName = 'DescTabella'
        end
      end
    end
    object TSCampo: TTabSheet
      Caption = 'TSCampo'
      ImageIndex = 1
      object DBGCampi: TdxDBGrid
        Left = 0
        Top = 21
        Width = 472
        Height = 199
        Bands = <
          item
          end>
        DefaultLayout = True
        HeaderPanelRowCount = 1
        KeyField = 'ID'
        SummaryGroups = <>
        SummarySeparator = ', '
        Align = alClient
        Color = clInfoBk
        TabOrder = 0
        DataSource = DsQCampi
        Filter.Criteria = {00000000}
        LookAndFeel = lfFlat
        OptionsBehavior = [edgoAutoSearch, edgoAutoSort, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoTabThrough, edgoVertThrough]
        OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
        OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoIndicator, edgoUseBitmap]
        object DBGCampiDescCampo: TdxDBGridMaskColumn
          Caption = 'Campo'
          BandIndex = 0
          RowIndex = 0
          FieldName = 'DescCampo'
        end
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 472
        Height = 21
        Align = alTop
        Alignment = taLeftJustify
        Caption = '  Seleziona il campo della tabella dall'#39'elenco'
        Color = clGray
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        object Label4: TLabel
          Left = 591
          Top = 4
          Width = 37
          Height = 13
          Anchors = [akTop, akRight]
          AutoSize = False
          Caption = 'LTot'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clYellow
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          Visible = False
        end
        object Label6: TLabel
          Left = 518
          Top = 4
          Width = 69
          Height = 13
          Alignment = taRightJustify
          Anchors = [akTop, akRight]
          AutoSize = False
          Caption = 'Totale query:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clYellow
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
      end
    end
    object TsOperatore: TTabSheet
      Caption = 'TsOperatore'
      ImageIndex = 2
      object DBGOperatori: TdxDBGrid
        Left = 0
        Top = 21
        Width = 472
        Height = 199
        Bands = <
          item
          end>
        DefaultLayout = True
        HeaderPanelRowCount = 1
        KeyField = 'IDTabQuery'
        SummaryGroups = <>
        SummarySeparator = ', '
        Align = alClient
        Color = clInfoBk
        TabOrder = 0
        DataSource = DsQOperatori
        Filter.Criteria = {00000000}
        LookAndFeel = lfFlat
        OptionsBehavior = [edgoAutoSearch, edgoAutoSort, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoTabThrough, edgoVertThrough]
        OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
        OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoIndicator, edgoUseBitmap]
        object DBGOperatoriDescOperatore: TdxDBGridMaskColumn
          Caption = 'Operatore'
          Sorted = csUp
          BandIndex = 0
          RowIndex = 0
          FieldName = 'DescOperatore'
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 472
        Height = 21
        Align = alTop
        Alignment = taLeftJustify
        Caption = '  Seleziona l'#39'operatore da applicare dall'#39'elenco'
        Color = clGray
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        object Label7: TLabel
          Left = 591
          Top = 4
          Width = 37
          Height = 13
          Anchors = [akTop, akRight]
          AutoSize = False
          Caption = 'LTot'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clYellow
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          Visible = False
        end
        object Label8: TLabel
          Left = 518
          Top = 4
          Width = 69
          Height = 13
          Alignment = taRightJustify
          Anchors = [akTop, akRight]
          AutoSize = False
          Caption = 'Totale query:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clYellow
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
      end
    end
    object TSValore: TTabSheet
      Caption = 'TSValore'
      ImageIndex = 3
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 472
        Height = 21
        Align = alTop
        Alignment = taLeftJustify
        Caption = '  Valore da cercare'
        Color = clGray
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object Label9: TLabel
          Left = 591
          Top = 4
          Width = 37
          Height = 13
          Anchors = [akTop, akRight]
          AutoSize = False
          Caption = 'LTot'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clYellow
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          Visible = False
        end
        object Label10: TLabel
          Left = 518
          Top = 4
          Width = 69
          Height = 13
          Alignment = taRightJustify
          Anchors = [akTop, akRight]
          AutoSize = False
          Caption = 'Totale query:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clYellow
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
      end
      object dxPickEdit1: TdxPickEdit
        Left = 10
        Top = 32
        Width = 73
        Color = clInfoBk
        TabOrder = 1
        StyleController = dxEditStyleController1
      end
      object dxDateEdit1: TdxDateEdit
        Left = 10
        Top = 57
        Width = 107
        Color = clInfoBk
        TabOrder = 2
        StyleController = dxEditStyleController1
        Date = -700000
      end
      object dxEdit1: TdxEdit
        Left = 10
        Top = 82
        Width = 199
        Color = clInfoBk
        TabOrder = 3
        StyleController = dxEditStyleController1
      end
      object dxSpinEdit1: TdxSpinEdit
        Left = 10
        Top = 108
        Width = 65
        Color = clInfoBk
        TabOrder = 4
        StyleController = dxEditStyleController1
      end
      object PanGrid: TPanel
        Left = 113
        Top = 33
        Width = 221
        Height = 153
        BevelOuter = bvNone
        TabOrder = 5
        object dxDBGrid1: TdxDBGrid
          Left = 0
          Top = 30
          Width = 221
          Height = 123
          Bands = <
            item
            end>
          DefaultLayout = True
          HeaderPanelRowCount = 1
          KeyField = 'ID'
          ShowSummaryFooter = True
          SummaryGroups = <>
          SummarySeparator = ', '
          Align = alClient
          Color = clInfoBk
          TabOrder = 0
          DataSource = DsQTable
          Filter.Active = True
          Filter.Criteria = {00000000}
          LookAndFeel = lfFlat
          OptionsBehavior = [edgoAutoSearch, edgoAutoSort, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoMultiSelect, edgoTabThrough, edgoVertThrough]
          OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
          OptionsView = [edgoAutoWidth, edgoBandHeaderWidth, edgoIndicator, edgoUseBitmap]
          object dxDBGrid1Desc: TdxDBGridMaskColumn
            Sorted = csUp
            BandIndex = 0
            RowIndex = 0
            FieldName = 'Descrizione'
            SummaryFooterType = cstCount
            SummaryFooterField = 'ID'
          end
          object dxDBGrid1Desc2: TdxDBGridColumn
            Visible = False
            BandIndex = 0
            RowIndex = 0
          end
        end
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 221
          Height = 30
          Align = alTop
          BevelOuter = bvLowered
          TabOrder = 1
          object Label11: TLabel
            Left = 6
            Top = 8
            Width = 85
            Height = 13
            Caption = 'Testo da cercare:'
          end
          object SpeedButton1: TSpeedButton
            Left = 188
            Top = 3
            Width = 28
            Height = 24
            Anchors = [akTop, akRight]
            Glyph.Data = {
              36050000424D3605000000000000360400002800000010000000100000000100
              08000000000000010000C40E0000C40E0000000100000001000000000000FFFF
              FF00787599008380A3007F7AA5006C666E00D698DA00A087A00083407900A357
              8F00D78EB200636162008A536D00B47290009F657D00A36B7D00B97C8C00C88B
              9B00C0949B00A88387009D696D00996666009D6A6A00A16E6E00A57272009B6B
              6B00A3717100A97676009F6F6F00A6767600B4818100825D5D00B6838300B885
              8500B5838300785757009E737300805D5D0083606000C18E8E00B2838300C794
              9400C6939300CB989800C9969600B0878700B5929200614F4F00C7A4A4007561
              61006C5C5C00A6A4A40074737300B6B5B500B17F7E00BE8B8A00A7757200A372
              6F00AD7C7900BC898600AF807B00AA7C7700BB8F8A00C99A9400CC9E9700BB92
              8A00D1A49A00C2918300B68D8000D4A49300C4988500C39B8A00D5B4A400DEAF
              9100CAA58E00DAB39B00E1BBA300D0AC9300C8A79100E7B79500F1BF9600EAC3
              A000E3C9B100FFCE9B00FFD19E00FFD2A000F8D1A500E0C4A600FFD4A100FFD5
              A200FFD6A300FBD3A300FFD8A900FFD9AA00F7D3A700F5D2A900EBCDA800D7C1
              A800FFD7A400FFD9A600F9D5A500FFDAAB00FFDDB000FFDBA800FFDCA900FFDD
              AA00FFDDAC00FFE1B500FFDEAB00FFDFAC00FFE0AD00FFE3B700FFE5BD00FFE1
              AE00FFE2AF00FFE3B000FFE4B600FFE5B800EED6AE00FFE8BE00FFE4B100FFE5
              B200FFE6B300FFEAC200FFF1D600FFE7B400FFE8B500FFE8B700FFE9BA00FFEE
              CA00FFF0D100FFEAB700FFEBB800FFEBBA00FBE8B800FFECBD00FFECBF00FFEE
              C300F4E4BC00FFEEC600ECE0C300FFECB900FFEDBA00FFEEBF00FFF2CD00FDF6
              E200FFEEBB00FFEFBC00FFF1C400FFF2CA00FFF0BD00FFF1BE00FFF5D400FFFA
              E800FFFDF600FFF2BF00FFF3C000FFF4C700FFF7D800FFFDF500FFF4C100FFF6
              CD00FFF7D100FFF8D900FFF9DE00FAF7E900FFF5C200FFF6C300F9F1C500F7F0
              CA00FFF8D300F4EECD00FFF8C500F3EDC400FFFDEF00FFFDF000FFF9C600FFFA
              C700FFFBC800FFFBD300FFFCD500FFFCC900FFFCD000FFFCD200FFFDD700FFFD
              DA00FFFDCA00FFFDCC00FFFECB00FFFFCC00FFFFCF00FFFFD100FFFFD300FFFF
              D500FFFFD600FFFFD900FFFFDB00FFFFDC00FFFFDF00FFFFE100FFFFE200FFFF
              E500FFFFE600FFFFE900FFFFEA00FFFFED00FFFFEE00FFFFF000FFFFF300FFFF
              F400FFFFF700FFFFF800FFFFFA00FFFFFD000000000061C7FF0051B7FF0055BB
              FF0059ACE2006FC2F90065B8F40042A8FF004F9BE900BBC1C7003694F600408C
              DB0064686D00676B7000407DD20064666A0095969800496AB5004E6AA700526D
              AA005F6DAA008294FF008A9CFD008B8B8B00848484007B7B7B006B6B6B005E5E
              5E00000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000002FF1ED0000
              000000000000000000002F0213F1ED0000000000000000000000DCE10413F1ED
              00000000000000000000EBD8E10413F1ED00000000000000000000EBD8E1042D
              F1ED00000000000000000000EBD9E404130505050505ED000000000000EBD8E1
              EDEF464D52483405000000000000EBEB2D5F9DB6C4C59113050000000000004D
              609BA7C3CBD301AB31ED0000000000489478B0C4CDD4D2C4470500000000004D
              A169A6C2C9CDCAC34D050000000000489D7593BDC2C4C3B64D05000000000048
              A17C6F97A6B0A7AC44ED00000000004B8C0199886D73965F2400000000000000
              48AB99A1929D564100000000000000000048484A4C4BED000000}
            OnClick = SpeedButton1Click
          end
          object ECerca: TEdit
            Left = 95
            Top = 4
            Width = 88
            Height = 21
            Anchors = [akLeft, akTop, akRight]
            TabOrder = 0
          end
        end
      end
    end
  end
  object QTabelle: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select distinct DescTabella'
      'from TabQuery '
      'where visibile=1'
      'order by desctabella')
    Left = 12
    Top = 233
  end
  object DsQTabelle: TDataSource
    DataSet = QTabelle
    Left = 12
    Top = 265
  end
  object DsQCampi: TDataSource
    DataSet = QCampi
    Left = 60
    Top = 265
  end
  object QCampi: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    BeforeOpen = QCampiBeforeOpen
    Parameters = <
      item
        Name = 'xTabella'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 30
        Value = Null
      end>
    SQL.Strings = (
      'select * from TabQuery'
      'where DescTabella=:xTabella'
      'and visibile=1'
      'order by DescCampo')
    Left = 60
    Top = 233
  end
  object QOperatori: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    BeforeOpen = QOperatoriBeforeOpen
    AfterOpen = QOperatoriAfterOpen
    Parameters = <
      item
        Name = 'xID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select * from TabQueryOperatori'
      'where IDTabQuery=:xID'
      'order by DescOperatore')
    Left = 116
    Top = 233
  end
  object DsQOperatori: TDataSource
    DataSet = QOperatori
    Left = 116
    Top = 265
  end
  object dxEditStyleController1: TdxEditStyleController
    ButtonStyle = btsFlat
    HotTrack = True
    Shadow = True
    Left = 156
    Top = 252
  end
  object QTable: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'select * from Diplomi')
    Left = 212
    Top = 236
  end
  object DsQTable: TDataSource
    DataSet = QTable
    Left = 212
    Top = 268
  end
end
