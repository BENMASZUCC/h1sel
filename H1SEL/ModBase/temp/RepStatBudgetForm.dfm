�
 TQRSTATBUDGETFORM 0�  TPF0TQRStatBudgetFormQRStatBudgetFormLeft Top WidthHeightcFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style Functions.Strings
PAGENUMBERCOLUMNNUMBERREPORTTITLE Functions.DATA00'' OptionsFirstPageHeaderLastPageFooter Page.ColumnsPage.Orientation
poPortraitPage.PaperSizeA4Page.Valuesd�d4dd  PrinterSettings.CopiesPrinterSettings.DuplexPrinterSettings.FirstPage PrinterSettings.LastPage PrinterSettings.OutputBinFirstPrintIfEmpty
SnapToGrid	UnitsMMZoomd TQRChartQRChart1Left(TopXWidth�Height� Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@頻����@��TUUU��@��UUUU��	@  
TQRDBChart
QRDBChart1Left�Top�WidthHeightBackWall.Brush.StylebsClearTitle.Text.Strings andamento delle spese negli anni Legend.AlignmentlaBottomView3D TLineSeriesSeries1Marks.ArrowLengthMarks.Visible
DataSourceDataFormazione.QTotPerAnnoTitle	AssegnatiXLabelsSourceAnnoSeriesColorclRedPointer.InflateMargins	Pointer.StylepsRectanglePointer.VisibleStairs	XValues.DateTimeXValues.NameXXValues.MultiplierXValues.OrderloAscendingYValues.DateTimeYValues.NameYYValues.MultiplierYValues.OrderloNoneYValues.ValueSourceTotAssegnati  TLineSeriesSeries2Marks.ArrowLengthMarks.Visible
DataSourceDataFormazione.QTotPerAnnoTitle
UtilizzatiXLabelsSourceAnnoSeriesColorclGreenPointer.InflateMargins	Pointer.StylepsRectanglePointer.VisibleStairs	XValues.DateTimeXValues.NameXXValues.MultiplierXValues.OrderloAscendingYValues.DateTimeYValues.NameYYValues.MultiplierYValues.OrderloNoneYValues.ValueSourceTotUtilizzati  TLineSeriesSeries3Marks.ArrowLengthMarks.Visible
DataSourceSeries1Title	RimanentiSeriesColorclYellowPointer.InflateMargins	Pointer.StylepsRectanglePointer.VisibleStairs	XValues.DateTimeXValues.NameXXValues.MultiplierXValues.OrderloAscendingYValues.DateTimeYValues.NameYYValues.MultiplierYValues.OrderloNoneDataSourcesSeries1Series2  TSubtractTeeFunctionTeeFunction1     TQRChartQRChart2Left(TopXWidth�Height� Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�=UUUUm�@頻����@ª�����@��UUUU��	@  
TQRDBChart
QRDBChart2Left�Top�WidthHeightBackWall.Brush.StylebsClearTitle.Text.StringsConfronto tra gli anni Legend.AlignmentlaBottomView3D 
TBarSeriesSeries4Marks.ArrowLengthMarks.StylesmsValueMarks.Visible	
DataSourceDataFormazione.QTotPerAnnoTitle	AssegnatiXLabelsSourceAnnoSeriesColor��� XValues.DateTimeXValues.NameXXValues.MultiplierXValues.OrderloAscendingYValues.DateTimeYValues.NameBarYValues.MultiplierYValues.OrderloNoneYValues.ValueSourceTotAssegnati  
TBarSeriesSeries5Marks.ArrowLengthMarks.StylesmsValueMarks.Transparent	Marks.Visible	
DataSourceDataFormazione.QTotPerAnnoTitle
UtilizzatiXLabelsSourceAnnoSeriesColor� @ XValues.DateTimeXValues.NameXXValues.MultiplierXValues.OrderloAscendingYValues.DateTimeYValues.NameBarYValues.MultiplierYValues.OrderloNoneYValues.ValueSourceTotUtilizzati    TQRChartQRChart3LeftpTopjWidth9Height� Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Valuesª���"�@�{����*�@      d�	@��UUUU/�	@  
TQRDBChart
QRDBChart3Left�Top�WidthHeightBackWall.Brush.StylebsClearBackWall.Pen.VisibleTitle.Text.StringsImporti assegnati AxisVisibleChart3DPercent
ClipPointsFrame.VisibleView3DOptions.Elevation;View3DOptions.OrthogonalView3DOptions.RotationhView3DWalls 
TPieSeriesSeries6Marks.ArrowLengthMarks.Style
smsPercentMarks.Visible	
DataSourceDataFormazione.QTotXAreaTitle	AssegnatiXLabelsSourceDescrizioneOtherSlice.TextOtherPieValues.DateTimePieValues.NamePiePieValues.MultiplierPieValues.OrderloNonePieValues.ValueSourceTotAssegnati    TQRChartQRChart4LeftpTopYWidth9Height� Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Valuesª���"�@�{����*�@O媪����
@��UUUU/�	@  
TQRDBChart
QRDBChart4Left�Top�WidthHeightBackWall.Brush.StylebsClearBackWall.Pen.VisibleTitle.Text.StringsImporti utilizzati AxisVisibleChart3DPercent

ClipPointsFrame.VisibleView3DOptions.Elevation;View3DOptions.OrthogonalView3DOptions.RotationhView3DWalls 
TPieSeriesSeries7Marks.ArrowLengthMarks.Style
smsPercentMarks.Visible	
DataSourceDataFormazione.QTotXAreaXLabelsSourceDescrizioneOtherSlice.TextOtherPieValues.DateTimePieValues.NamePiePieValues.MultiplierPieValues.OrderloNonePieValues.ValueSourceTotUtilizzati    TQRLabelQRLabel1Left(Top(Width\HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values2Ъ���j�@頻����@頻����@      0�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionSTATISTICHE BUDGET FORMAZIONEColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBoldfsUnderline 
ParentFontTransparentWordWrap	FontSize  
TQRSysData
QRSysData1Left�Top(WidthQHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values2Ъ����@�頻����@      P�@ 	AlignmenttaRightJustifyAlignToBandAutoSizeColorclWhiteDataqrsDateTransparentFontSize
   