object DettAnnuncioForm: TDettAnnuncioForm
  Left = 263
  Top = 102
  BorderStyle = bsDialog
  Caption = 'Informazioni di dettaglio annuncio'
  ClientHeight = 380
  ClientWidth = 422
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 4
    Top = 5
    Width = 72
    Height = 13
    Caption = 'Data creazione'
    FocusControl = DBEdit1
  end
  object Label2: TLabel
    Left = 4
    Top = 45
    Width = 27
    Height = 13
    Caption = 'Testo'
    FocusControl = DBMemo1
  end
  object Label3: TLabel
    Left = 4
    Top = 157
    Width = 53
    Height = 13
    Caption = 'File di testo'
    FocusControl = DBEdit2
  end
  object Label4: TLabel
    Left = 84
    Top = 5
    Width = 46
    Height = 13
    Caption = 'N� Moduli'
    FocusControl = DBEdit3
  end
  object Label5: TLabel
    Left = 134
    Top = 5
    Width = 45
    Height = 13
    Caption = 'N� Parole'
    FocusControl = DBEdit4
  end
  object Label6: TLabel
    Left = 184
    Top = 5
    Width = 58
    Height = 13
    Caption = 'Corso in Lire'
    FocusControl = DBEdit5
  end
  object Label7: TLabel
    Left = 4
    Top = 199
    Width = 62
    Height = 13
    Caption = 'Pubblicazioni'
  end
  object DBGrid1: TDBGrid
    Left = 4
    Top = 215
    Width = 414
    Height = 161
    DataSource = DsQAnnuncio
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'Testata'
        Width = 135
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NomeEdizione'
        Title.Caption = 'Edizione'
        Width = 96
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DataPubblicazione'
        Title.Caption = 'Data pubblicaz.'
        Width = 81
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CostoLire'
        Title.Caption = 'Costo (�.)'
        Visible = True
      end>
  end
  object DBEdit1: TDBEdit
    Left = 4
    Top = 21
    Width = 75
    Height = 21
    DataField = 'Data'
    DataSource = DsQAnnuncio
    TabOrder = 1
  end
  object DBMemo1: TDBMemo
    Left = 4
    Top = 61
    Width = 414
    Height = 89
    DataField = 'Testo'
    DataSource = DsQAnnuncio
    ScrollBars = ssVertical
    TabOrder = 2
  end
  object DBCheckBox1: TDBCheckBox
    Left = 254
    Top = 11
    Width = 79
    Height = 17
    Caption = 'Civetta'
    DataField = 'Civetta'
    DataSource = DsQAnnuncio
    TabOrder = 3
    ValueChecked = 'True'
    ValueUnchecked = 'False'
  end
  object DBEdit2: TDBEdit
    Left = 4
    Top = 173
    Width = 285
    Height = 21
    DataField = 'FileTesto'
    DataSource = DsQAnnuncio
    TabOrder = 4
  end
  object DBEdit3: TDBEdit
    Left = 84
    Top = 21
    Width = 47
    Height = 21
    DataField = 'NumModuli'
    DataSource = DsQAnnuncio
    TabOrder = 5
  end
  object DBEdit4: TDBEdit
    Left = 134
    Top = 21
    Width = 47
    Height = 21
    DataField = 'NumParole'
    DataSource = DsQAnnuncio
    TabOrder = 6
  end
  object DBEdit5: TDBEdit
    Left = 184
    Top = 21
    Width = 64
    Height = 21
    DataField = 'TotaleLire'
    DataSource = DsQAnnuncio
    TabOrder = 7
  end
  object DBCheckBox2: TDBCheckBox
    Left = 254
    Top = 27
    Width = 79
    Height = 17
    Caption = 'Archiviato'
    DataField = 'Archiviato'
    DataSource = DsQAnnuncio
    TabOrder = 8
    ValueChecked = 'True'
    ValueUnchecked = 'False'
  end
  object BitBtn1: TBitBtn
    Left = 336
    Top = 3
    Width = 84
    Height = 31
    Caption = 'Esci'
    TabOrder = 9
    Kind = bkOK
  end
  object QAnnuncio_OLD: TQuery
    DatabaseName = 'EBCDB'
    SQL.Strings = (
      'select Ann_Annunci.*,'
      '       Ann_AnnEdizData.Data DataPubblicazione,CostoLire,'
      '       Ann_Edizioni.NomeEdizione,'
      '       Ann_Testate.Denominazione Testata'
      'from Ann_Annunci,Ann_AnnEdizData,Ann_Edizioni,Ann_Testate'
      'where Ann_Annunci.ID=Ann_AnnEdizData.IDAnnuncio'
      '  and Ann_AnnEdizData.IDEdizione=Ann_Edizioni.ID'
      '  and Ann_Edizioni.IDTestata=Ann_Testate.ID'
      'and Ann_Annunci.ID=:xID')
    Left = 118
    Top = 319
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'xID'
        ParamType = ptUnknown
      end>
    object QAnnuncio_OLDID: TAutoIncField
      FieldName = 'ID'
      Origin = 'EBCDB.Ann_Annunci.ID'
    end
    object QAnnuncio_OLDRif: TStringField
      FieldName = 'Rif'
      Origin = 'EBCDB.Ann_Annunci.Rif'
      FixedChar = True
      Size = 6
    end
    object QAnnuncio_OLDData: TDateTimeField
      FieldName = 'Data'
      Origin = 'EBCDB.Ann_Annunci.Data'
    end
    object QAnnuncio_OLDTesto: TMemoField
      FieldName = 'Testo'
      Origin = 'EBCDB.Ann_Annunci.Testo'
      BlobType = ftMemo
    end
    object QAnnuncio_OLDCivetta: TBooleanField
      FieldName = 'Civetta'
      Origin = 'EBCDB.Ann_Annunci.Civetta'
    end
    object QAnnuncio_OLDFileTesto: TStringField
      FieldName = 'FileTesto'
      Origin = 'EBCDB.Ann_Annunci.FileTesto'
      FixedChar = True
      Size = 50
    end
    object QAnnuncio_OLDNumModuli: TSmallintField
      FieldName = 'NumModuli'
      Origin = 'EBCDB.Ann_Annunci.NumModuli'
    end
    object QAnnuncio_OLDNumParole: TSmallintField
      FieldName = 'NumParole'
      Origin = 'EBCDB.Ann_Annunci.NumParole'
    end
    object QAnnuncio_OLDTotaleLire: TFloatField
      FieldName = 'TotaleLire'
      Origin = 'EBCDB.Ann_Annunci.TotaleLire'
      DisplayFormat = '#,###'
    end
    object QAnnuncio_OLDTotaleEuro: TFloatField
      FieldName = 'TotaleEuro'
      Origin = 'EBCDB.Ann_Annunci.TotaleEuro'
    end
    object QAnnuncio_OLDNote: TMemoField
      FieldName = 'Note'
      Origin = 'EBCDB.Ann_Annunci.Note'
      BlobType = ftMemo
    end
    object QAnnuncio_OLDArchiviato: TBooleanField
      FieldName = 'Archiviato'
      Origin = 'EBCDB.Ann_Annunci.Archiviato'
    end
    object QAnnuncio_OLDCVPervenuti: TSmallintField
      FieldName = 'CVPervenuti'
      Origin = 'EBCDB.Ann_Annunci.CVPervenuti'
    end
    object QAnnuncio_OLDCVIdonei: TSmallintField
      FieldName = 'CVIdonei'
      Origin = 'EBCDB.Ann_Annunci.CVIdonei'
    end
    object QAnnuncio_OLDCVChiamati: TSmallintField
      FieldName = 'CVChiamati'
      Origin = 'EBCDB.Ann_Annunci.CVChiamati'
    end
    object QAnnuncio_OLDIDFattura: TIntegerField
      FieldName = 'IDFattura'
      Origin = 'EBCDB.Ann_Annunci.IDFattura'
    end
    object QAnnuncio_OLDDataPubblicazione: TDateTimeField
      FieldName = 'DataPubblicazione'
      Origin = 'EBCDB.Ann_AnnEdizData.Data'
    end
    object QAnnuncio_OLDCostoLire: TFloatField
      FieldName = 'CostoLire'
      Origin = 'EBCDB.Ann_AnnEdizData.CostoLire'
      DisplayFormat = '#,###'
    end
    object QAnnuncio_OLDNomeEdizione: TStringField
      FieldName = 'NomeEdizione'
      Origin = 'EBCDB.Ann_Edizioni.NomeEdizione'
      FixedChar = True
      Size = 50
    end
    object QAnnuncio_OLDTestata: TStringField
      FieldName = 'Testata'
      Origin = 'EBCDB.Ann_Testate.Denominazione'
      FixedChar = True
      Size = 50
    end
  end
  object DsQAnnuncio: TDataSource
    DataSet = QAnnuncio
    Left = 150
    Top = 319
  end
  object QAnnuncio: TADOLinkedQuery
    Connection = Data.DB
    Parameters = <>
    OriginalSQL.Strings = (
      'select Ann_Annunci.*,'
      '       Ann_AnnEdizData.Data DataPubblicazione,CostoLire,'
      '       Ann_Edizioni.NomeEdizione,'
      '       Ann_Testate.Denominazione Testata'
      'from Ann_Annunci,Ann_AnnEdizData,Ann_Edizioni,Ann_Testate'
      'where Ann_Annunci.ID=Ann_AnnEdizData.IDAnnuncio'
      '  and Ann_AnnEdizData.IDEdizione=Ann_Edizioni.ID'
      '  and Ann_Edizioni.IDTestata=Ann_Testate.ID'
      'and Ann_Annunci.ID=:xID:'
      '')
    UseFilter = False
    Left = 120
    Top = 280
  end
end
