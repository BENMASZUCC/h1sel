�
 TMODSTATORICFORM 0-  TPF0TModStatoRicFormModStatoRicFormLeftUTop3ActiveControlDEDallaDataBorderStylebsDialogCaptionModifica stato ricercaClientHeight4ClientWidth[Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnClose	FormCloseOnShowFormShowPixelsPerInch`
TextHeight TLabelLabel4LeftTopoWidthTHeightCaptionStato successivo:  TLabelLabel5LeftTopXWidth3HeightCaptionDalla data:  TLabelLabel6LeftTopWidthnHeightCaptionNote (max 50 caratteri):  TLabelLabel7LeftTop� WidthaHeightCaptionData fine procedura:  TBitBtnBitBtn1LeftTopWidthTHeight#TabOrderOnClickBitBtn1ClickKindbkOK  TBitBtnBitBtn2LeftTop'WidthTHeight#CaptionAnnullaTabOrderKindbkCancel  TPanelPanel1LeftTopWidth� HeightD
BevelOuter	bvLoweredEnabledTabOrder TLabelLabel1LeftTopWidthHeightCaptionRif.FocusControlDBEdit1  TLabelLabel2Left:TopWidth HeightCaptionCliente  TLabelLabel3LeftTop-Width?HeightCaptionStato attuale:  TDBEditDBEdit1LeftTopWidth3Height	DataFieldProgressivo
DataSourceDataRicerche.DsRicerchePendTabOrder   TDBEditDBEdit5Left9TopWidth� HeightColorclAqua	DataFieldCliente
DataSourceDataRicerche.DsRicerchePendReadOnly	TabOrder  TDBEditDBEdit6LeftITop)Width� Height	DataFieldStatoRic
DataSourceDataRicerche.DsRicerchePendTabOrder   TDBGridDBGrid1Left~TopnWidth� Height� 
DataSource
DsStatiRicTabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameStatoRicTitle.CaptionStatoWidth`Visible	    TDateEdit97DEDallaDataLeft~TopUWidth]HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder ColorCalendar.ColorValidclBlueDayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanPopup
doIsMaskeddoShowCanceldoShowToday   TEditENoteLeftTopWidth� Height	MaxLength2TabOrder  TDateEdit97
DEDataFineLeft~Top� WidthqHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderColorCalendar.ColorValidclBlueDayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanClear
doCanPopup
doIsMaskeddoShowCanceldoShowToday   TDataSource
DsStatiRicDataSet	TStatiRicLeft� Top�   TADOLinkedTable	TStatiRic
ConnectionData.DB
CursorTypectStatic	TableNameEBC_StatiRicLeft� Top�    