object AnagTestelearningForm: TAnagTestelearningForm
  Left = 386
  Top = 194
  Width = 449
  Height = 291
  Caption = 'AnagTestelearningForm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object AdvPanel1: TAdvPanel
    Left = 0
    Top = 237
    Width = 441
    Height = 27
    Align = alBottom
    TabOrder = 0
    UseDockManager = True
    Version = '1.9.0.3'
    Caption.Color = clHighlight
    Caption.ColorTo = clNone
    Caption.Font.Charset = DEFAULT_CHARSET
    Caption.Font.Color = clHighlightText
    Caption.Font.Height = -11
    Caption.Font.Name = 'MS Sans Serif'
    Caption.Font.Style = []
    StatusBar.Font.Charset = DEFAULT_CHARSET
    StatusBar.Font.Color = clWindowText
    StatusBar.Font.Height = -11
    StatusBar.Font.Name = 'Tahoma'
    StatusBar.Font.Style = []
    FullHeight = 0
    object ToolbarButton971: TToolbarButton97
      Left = 357
      Top = 2
      Width = 84
      Height = 22
      Caption = 'Elenco Test'
      OnClick = ToolbarButton971Click
    end
    object ToolbarButton972: TToolbarButton97
      Left = 2
      Top = 3
      Width = 63
      Height = 22
      Caption = 'Associa'
      OnClick = ToolbarButton972Click
    end
    object ToolbarButton973: TToolbarButton97
      Left = 65
      Top = 3
      Width = 63
      Height = 22
      Caption = 'Elimina'
      OnClick = ToolbarButton973Click
    end
  end
  object dxDBGrid1: TdxDBGrid
    Left = 0
    Top = 0
    Width = 441
    Height = 237
    Bands = <
      item
      end>
    DefaultLayout = True
    HeaderPanelRowCount = 1
    KeyField = 'ID'
    SummaryGroups = <>
    SummarySeparator = ', '
    Align = alClient
    TabOrder = 1
    DataSource = dsAnagTest
    Filter.Active = True
    Filter.Criteria = {00000000}
    OptionsBehavior = [edgoAutoSort, edgoDragScroll, edgoEnterShowEditor, edgoImmediateEditor, edgoTabThrough, edgoVertThrough]
    OptionsDB = [edgoCancelOnExit, edgoCanDelete, edgoCanInsert, edgoCanNavigation, edgoConfirmDelete, edgoLoadAllRecords, edgoUseBookmarks]
    object dxDBGrid1ID: TdxDBGridMaskColumn
      Visible = False
      Width = 55
      BandIndex = 0
      RowIndex = 0
      FieldName = 'ID'
    end
    object dxDBGrid1IDAnagrafica: TdxDBGridMaskColumn
      Visible = False
      Width = 55
      BandIndex = 0
      RowIndex = 0
      FieldName = 'IDAnagrafica'
    end
    object dxDBGrid1IDTest: TdxDBGridMaskColumn
      Visible = False
      Width = 55
      BandIndex = 0
      RowIndex = 0
      FieldName = 'IDTest'
    end
    object dxDBGrid1ID_1: TdxDBGridMaskColumn
      Visible = False
      Width = 55
      BandIndex = 0
      RowIndex = 0
      FieldName = 'ID_1'
    end
    object dxDBGrid1Codice: TdxDBGridMaskColumn
      Caption = 'Codice'
      Width = 99
      BandIndex = 0
      RowIndex = 0
      FieldName = 'CodiceTest'
    end
    object dxDBGrid1Descrizione: TdxDBGridMaskColumn
      Caption = 'Descrizione'
      Width = 312
      BandIndex = 0
      RowIndex = 0
      FieldName = 'DescrizioneTest'
    end
  end
  object qAnagtest: TADOQuery
    Connection = Data.DB
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'x0'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select * from AnagTestELearning '
      'join TestELearning '
      'on AnagTestELearning.IDtest=TestELearning .id'
      'where idanagrafica=:x0')
    Left = 24
    Top = 48
    object qAnagtestID: TAutoIncField
      FieldName = 'ID'
      ReadOnly = True
    end
    object qAnagtestIDAnagrafica: TIntegerField
      FieldName = 'IDAnagrafica'
    end
    object qAnagtestIDTest: TIntegerField
      FieldName = 'IDTest'
    end
    object qAnagtestID_1: TAutoIncField
      FieldName = 'ID_1'
      ReadOnly = True
    end
    object qAnagtestDescrizioneTest: TStringField
      FieldName = 'DescrizioneTest'
      Size = 500
    end
    object qAnagtestCodiceTest: TStringField
      FieldName = 'CodiceTest'
      Size = 100
    end
  end
  object dsAnagTest: TDataSource
    DataSet = qAnagtest
    Left = 64
    Top = 48
  end
end
