unit ModuloDati4;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Db, DBTables, ADODB, U_ADOLinkCl, uAnnunciRoutines, ShellApi;

type
     TDataAnnunci = class(TDataModule)
          DsAnnunci: TDataSource;
          DsTestate: TDataSource;
          DsEdizioniTest: TDataSource;
          DsAnnunciRic: TDataSource;
          DsGiorniIndicati: TDataSource;
          DsQualifiche: TDataSource;
          DsSconti: TDataSource;
          DsAnnEdizDataXann: TDataSource;
          DsTestateLK: TDataSource;
          DsPenetraz: TDataSource;
          DsQAnnAnag: TDataSource;
          DsQAnnRuoli: TDataSource;
          DsQAnnAnagData: TDataSource;
          DsQCentriCostoCGLK: TDataSource;
          DsQAnnContrattiCG: TDataSource;
          DsQAnnTimeReport: TDataSource;
          DsQUtentiLK: TDataSource;
          DsQGruppiProfLK: TDataSource;
          DsQContrattiCGLK: TDataSource;
          TAnnunci: TADOLinkedQuery;
          QAnnRuoli: TADOLinkedQuery;
          QTestate: TADOLinkedQuery;
          QEdizioniTest_old: TADOLinkedQuery;
          QAnnEdizDataXAnn: TADOLinkedQuery;
          QAnnAnagData: TADOLinkedQuery;
          QAnnContrattiCG: TADOLinkedQuery;
          QAnnTimeReport: TADOLinkedQuery;
          QUtentiLK: TADOLinkedQuery;
          QGruppiProfLK: TADOLinkedQuery;
          Q: TADOLinkedQuery;
          QTemp: TADOLinkedQuery;
          QCentriCostoCGLK: TADOLinkedQuery;
          QContrattiCGLK: TADOLinkedQuery;
          TSconti: TADOLinkedTable;
          TPenetraz: TADOLinkedTable;
          TTestateLK: TADOLinkedTable;
          TGiorniIndicati: TADOLinkedTable;
          TQualifiche: TADOLinkedTable;
          TEdizioniLK: TADOLinkedTable;
          TAnagABS: TADOLinkedTable;
          TFattureLK: TADOLinkedTable;
          TFestivi: TADOLinkedTable;
          TAnnunciRicIDX: TADOLinkedTable;
          TGiorniSettLK: TADOLinkedTable;
          TScontiIDX: TADOLinkedTable;
          QAnnTimeReportID: TAutoIncField;
          QAnnTimeReportIDAnnuncio: TIntegerField;
          QAnnTimeReportIDUtente: TIntegerField;
          QAnnTimeReportData: TDateTimeField;
          QAnnTimeReportIDGruppoProfess: TIntegerField;
          QAnnTimeReportOreConsuntivo: TFloatField;
          QAnnTimeReportNote: TMemoField;
          QAnnTimeReportOrePreventivo: TIntegerField;
          QAnnTimeReportUtente: TStringField;
          QAnnTimeReportGruppoProfess: TStringField;
          QGruppiProfLKID: TAutoIncField;
          QGruppiProfLKGruppoProfessionale: TStringField;
          QGruppiProfLKRateOrarioPrev: TFloatField;
          QGruppiProfLKRateOrarioCons: TFloatField;
          QGruppiProfLKIDMansione: TIntegerField;
          QGruppiProfLKRateOrarioDirectPrev: TFloatField;
          QGruppiProfLKRateOrarioDirectCons: TFloatField;
          QUtentiLKID: TAutoIncField;
          QUtentiLKNominativo: TStringField;
          QUtentiLKDescrizione: TStringField;
          QUtentiLKIDGruppoProfess: TIntegerField;
          QCentriCostoCGLKID: TAutoIncField;
          QCentriCostoCGLKCodice: TStringField;
          QCentriCostoCGLKCentroCosto: TStringField;
          QCentriCostoCGLKNote: TMemoField;
          QContrattiCGLKID: TAutoIncField;
          QContrattiCGLKCodiceContratto: TStringField;
          QContrattiCGLKDescrizione: TStringField;
          TAnnunciCentroCostoCG: TStringField;
          TAnnunciCodiceContrattoCG: TStringField;
          TAnnunciIDContrattoCG: TIntegerField;
          TAnnunciIDCentroCostoCG: TIntegerField;
          TAnnunciID: TAutoIncField;
          TAnnunciRif: TStringField;
          TAnnunciData: TDateTimeField;
          TAnnunciCivetta: TBooleanField;
          TAnnunciFileTesto: TStringField;
          TAnnunciNumModuli: TSmallintField;
          TAnnunciNumParole: TSmallintField;
          TAnnunciTotaleLire: TFloatField;
          TAnnunciTotaleEuro: TFloatField;
          TAnnunciNote: TMemoField;
          TAnnunciArchiviato: TBooleanField;
          TAnnunciCVPervenuti: TSmallintField;
          TAnnunciCVIdonei: TSmallintField;
          TAnnunciCVChiamati: TSmallintField;
          TAnnunciIDFattura: TIntegerField;
          TAnnunciIDCliente: TIntegerField;
          TAnnunciIDUtente: TIntegerField;
          TAnnunciIDTipoAnnuncio: TIntegerField;
          TAnnunciIDSede: TIntegerField;
          TAnnunciIDUtenteProj: TIntegerField;
          TAnnunciIDMansione: TIntegerField;
          TAnnunciEmailHost: TStringField;
          TAnnunciEmailPort: TStringField;
          TAnnunciEmailUserID: TStringField;
          TAnnunciEmailPassword: TStringField;
          TAnnunciLKCliente: TStringField;
          TAnnunciLKMansione: TStringField;
          TAnnunciCliente: TStringField;
          TAnnunciUtente: TStringField;
          TAnnunciUtenteProj: TStringField;
          TAnnunciRuoloAnnuncio: TStringField;
          TAnnunciSede: TStringField;
          TAnnunciTipoAnnuncio: TStringField;
          TFattureLKID: TAutoIncField;
          TAnnunciProgFattura: TStringField;
          TAnnunciRic: TADOQuery;
          TAnnunciJobTitle: TStringField;
          TAnnunciJobZone: TStringField;
          TAnnunciSkills: TStringField;
          TAnnunciJobDescription: TStringField;
          QAnnFile_old: TADOQuery;
          QAnnFile_oldID: TAutoIncField;
          QAnnFile_oldFileAnnuncio: TMemoField;
          QAnnFile: TADOQuery;
          QAnnFileID: TAutoIncField;
          QAnnFileFileAnnuncio: TMemoField;
          qTestoAnn: TADOQuery;
          dsTestoAnn: TDataSource;
          qTestoAnntesto: TMemoField;
          qTestoAnnsolotesto: TMemoField;
          QAnnFileFileNome: TStringField;
          QAnnFileFileType: TStringField;
          TFattureLKProgressivo: TStringField;
          QAnnFileFileBloccato: TBooleanField;
          TAnnunciQualeRuoloUsare: TSmallintField;
          TAnnunciIndirizzoCliente: TStringField;
          TAnnunciIDIndirizzoCliente: TIntegerField;
          TAnnunciDescIndirizzo: TStringField;
          QAnnEdizDataXAnnID: TAutoIncField;
          QAnnEdizDataXAnnIDAnnuncio: TIntegerField;
          QAnnEdizDataXAnnIDEdizione: TIntegerField;
          QAnnEdizDataXAnnData: TDateTimeField;
          QAnnEdizDataXAnnCostoLire: TFloatField;
          QAnnEdizDataXAnnCostoEuro: TFloatField;
          QAnnEdizDataXAnnCostoANoiLire: TFloatField;
          QAnnEdizDataXAnnCostoANoiEuro: TFloatField;
          QAnnEdizDataXAnnScontoAlClienteLire: TFloatField;
          QAnnEdizDataXAnnScontoAlClienteEuro: TFloatField;
          QAnnEdizDataXAnnScontoANoiLire: TFloatField;
          QAnnEdizDataXAnnScontoANoiEuro: TFloatField;
          QAnnEdizDataXAnnNumModuli: TIntegerField;
          QAnnEdizDataXAnnCodice: TStringField;
          QAnnEdizDataXAnnListinoLire: TFloatField;
          QAnnEdizDataXAnnListinoEuro: TFloatField;
          QAnnEdizDataXAnnDataScadenza: TDateTimeField;
          QAnnEdizDataXAnnDataInvioJP: TDateTimeField;
          QAnnEdizDataXAnnIDMP_Sito: TIntegerField;
          QAnnEdizDataXAnnIDAnnuncio_1: TIntegerField;
          QAnnEdizDataXAnnIDEdizione_1: TIntegerField;
          QAnnEdizDataXAnnIDMP_Sito_1: TIntegerField;
          QAnnEdizDataXAnnTestata: TStringField;
          QAnnEdizDataXAnnEdizione: TStringField;
          QAnnEdizDataXAnnGiornoRPQ: TSmallintField;
          QMPSitiLK: TADOQuery;
          QMPSitiLKID: TIntegerField;
          QMPSitiLKNome: TStringField;
          QAnnEdizDataXAnnMPSito: TStringField;
          QAnnEdizDataXAnnGiorno: TStringField;
          QAnnEdizDataXAnnTipoGiorno: TStringField;
          QPubb_Spec: TADOQuery;
          DsQPubb_Spec: TDataSource;
          QPubb_SpecID: TAutoIncField;
          QPubb_SpecIDAnnEdizData: TIntegerField;
          QPubb_SpecIDTabSito: TIntegerField;
          QPubb_SpecValore: TStringField;
          QPubb_SpecDescValore: TStringField;
          QPubb_SpecNote: TStringField;
          QSitiTabLK: TADOQuery;
          QSitiTabLKID: TAutoIncField;
          QSitiTabLKIDSito: TIntegerField;
          QSitiTabLKNomeTabella: TStringField;
          QSitiTabLKDescrizione: TStringField;
          QSitiTabLKQueryPickUp: TStringField;
          QSitiTabLKNote: TStringField;
          QPubb_SpecNomeTabella: TStringField;
          QPubb_SpecDescTabella: TStringField;
          QPubb_SpecQueryPickUp: TStringField;
          QSiti2: TADOQuery;
          QSiti2ID: TIntegerField;
          QSiti2Nome: TStringField;
          QSiti2OutputFileName: TStringField;
          QSiti2OutputFileDir: TStringField;
          QSiti2TipoConnessione: TStringField;
          QSiti2FTP_host: TStringField;
          QSiti2FTP_Username: TStringField;
          QSiti2FTP_password: TStringField;
          QSiti2FTP_dir: TStringField;
          QMPConfig: TADOQuery;
          QMPConfigWS_FTP_URL: TStringField;
          QMPConfigDirLog: TStringField;
          TAnnunciIDQuestionario: TIntegerField;
          QQuestLK: TADOQuery;
          QQuestLKID: TAutoIncField;
          QQuestLKCodice: TStringField;
          QQuestLKTipoDomanda: TStringField;
          TAnnunciQuestionario: TStringField;
          TAnnuncifileannunciohtml: TMemoField;
          QAnnAnag: TADOQuery;
          QAnnAnagID: TAutoIncField;
          QAnnAnagIDAnnEdizData: TIntegerField;
          QAnnAnagDataPervenuto: TDateTimeField;
          QAnnAnagCVNumero: TIntegerField;
          QAnnAnagCognome: TStringField;
          QAnnAnagNome: TStringField;
          QAnnAnagIDAnagrafica: TIntegerField;
          QAnnAnagIDAnnuncio: TIntegerField;
          QAnnAnagPesoRaggiunto: TFloatField;
          QAnnAnagTotPunteggio: TFloatField;
          QAnnAnagIDAnagQuest: TAutoIncField;
          QAnnEdizDataXAnnDataUltimoPrelievoStatici: TDateTimeField;
          QAnnEdizDataXAnnFeed_Facebook: TBooleanField;
          QAnnEdizDataXAnnFeed_Linkedin: TBooleanField;
          QTemplateHTML: TADOQuery;
          QTemplateHTMLID: TAutoIncField;
          QTemplateHTMLDescrizione: TStringField;
          QTemplateHTMLTemplateHTML: TMemoField;
          QTemplateHTMLURLIndicizzazione: TStringField;
          QTemplateHTMLURLPrelievoFeed: TStringField;
          QTemplateHTMLURLFeed: TStringField;
          QTemplateHTMLFeed_Facebook: TMemoField;
          QTemplateHTMLFeed_Linkedin: TMemoField;
          QTemplateHTMLFeed_Generico: TMemoField;
          QTemplateHTMLFeed_SimplyHired: TMemoField;
          QApriAnnunciHtml: TADOQuery;
          QApriAnnunciHtmlfileannunciohtml: TMemoField;
          QTemplateHTMLFeed_Indeed: TMemoField;
          QTemplateHTMLFeed_Trovit: TMemoField;
          QAnnSettori: TADOQuery;
          DsQAnnSettori: TDataSource;
          QAnnSettoriID: TAutoIncField;
          QAnnSettoriIDAnnuncio: TIntegerField;
          QAnnSettoriIDSettore: TIntegerField;
          QAnnSettoriNote: TMemoField;
          QSettoriLK: TADOQuery;
          QSettoriLKID: TAutoIncField;
          QSettoriLKAttivita: TStringField;
          QAnnSettoriSettore: TStringField;
          QAnnSettoriFlag_feed: TBooleanField;
          QAnnProv: TADOQuery;
          DsQAnnProv: TDataSource;
          QAnnProvID: TAutoIncField;
          QAnnProvIDAnnuncio: TIntegerField;
          QAnnProvRegione: TStringField;
          QAnnProvProvincia: TStringField;
          QAnnProvFlag_feed: TBooleanField;
          QAnnProvNote: TMemoField;
          TAnnunciView_RegProv: TStringField;
          Q2: TADOQuery;
          QTemplateHTMLFeed_Cambiolavoro: TMemoField;
          QAnnSediEstere: TADOQuery;
          DsQAnnSediEstere: TDataSource;
          QAnnSediEstereID: TAutoIncField;
          QAnnSediEstereIDAnnuncio: TIntegerField;
          QAnnSediEstereNazione: TStringField;
          QAnnSediEstereCitta: TStringField;
          QAnnSediEstereFlag_feed: TBooleanField;
          QAnnSediEstereNote: TMemoField;
          TAnnunciInEvidenza: TBooleanField;
          QAnnEdizDataXAnnFeed_RaccoltaEBC: TBooleanField;
          QTemplateHTMLFeed_RaccoltaEBC: TMemoField;
          QEdizioniTest: TADOQuery;
          TAnnunciVisibilitaAnnunciWeb: TBooleanField;
          qTestoAnntestopulito: TMemoField;
          TAnnunciRuoloWeb: TStringField;
    QEdizioniTestID: TAutoIncField;
    QEdizioniTestIDTestata: TIntegerField;
    QEdizioniTestNomeEdizione: TStringField;
    QEdizioniTestContatto: TStringField;
    QEdizioniTestTelefoni: TStringField;
    QEdizioniTestFax: TStringField;
    QEdizioniTestCostoParolaFerLire: TFloatField;
    QEdizioniTestCostoParolaFerEuro: TFloatField;
    QEdizioniTestCostoParolaFestLire: TFloatField;
    QEdizioniTestCostoParolaFestEuro: TFloatField;
    QEdizioniTestCostoParolaRPQLire: TFloatField;
    QEdizioniTestCostoParolaRPQEuro: TFloatField;
    QEdizioniTestCostoModuloFerLire: TFloatField;
    QEdizioniTestCostoModuloFerEuro: TFloatField;
    QEdizioniTestCostoModuloFestLire: TFloatField;
    QEdizioniTestCostoModuloFestEuro: TFloatField;
    QEdizioniTestCostoModuloRPQLire: TFloatField;
    QEdizioniTestCostoModuloRPQEuro: TFloatField;
    QEdizioniTestPenetrazioneProvincia: TStringField;
    QEdizioniTestPenetrazioneComune: TStringField;
    QEdizioniTestPenetrazioneZona: TStringField;
    QEdizioniTestGiornoRPQ: TSmallintField;
    QEdizioniTestNote: TStringField;
    QEdizioniTestIDCliente: TIntegerField;
    QEdizioniTestVisibileWeb: TBooleanField;
    QEdizioniTestConcessionario: TStringField;
          procedure DataAnnunciCreate(Sender: TObject);
          procedure DsPenetrazStateChange(Sender: TObject);
          procedure DsScontiStateChange(Sender: TObject);
          procedure DsGiorniIndicatiStateChange(Sender: TObject);
          procedure DsAnnunciStateChange(Sender: TObject);
          procedure TAnnunci_OLDAfterClose(DataSet: TDataSet);
          procedure QAnnEdizDataXAnn_OLDCalcFields(DataSet: TDataSet);
          procedure QUtentiLK_OLDBeforeOpen(DataSet: TDataSet);
          procedure QAnnTimeReport_OLDBeforeOpen(DataSet: TDataSet);
          procedure QAnnTimeReport_OLDAfterClose(DataSet: TDataSet);
          procedure QAnnTimeReport_OLDAfterInsert(DataSet: TDataSet);
          procedure QAnnTimeReport_OLDNewRecord(DataSet: TDataSet);
          procedure QAnnTimeReport_OLDAfterPost(DataSet: TDataSet);
          procedure DsQAnnTimeReportStateChange(Sender: TObject);
          procedure QAnnTimeReport_OLDAfterDelete(DataSet: TDataSet);
          procedure QAnnTimeReport_OLDBeforeDelete(DataSet: TDataSet);
          procedure TAnnunci_OLDAfterPost(DataSet: TDataSet);
          procedure QUtentiLKBeforePost(DataSet: TDataSet);
          procedure QAnnFile_oldBeforeOpen(DataSet: TDataSet);
          procedure TAnnunciAfterScroll(DataSet: TDataSet);
          procedure QAnnFileBeforeOpen(DataSet: TDataSet);
          procedure QAnnAnagBeforeOpen(DataSet: TDataSet);
          procedure QAnnEdizDataXAnnAfterOpen(DataSet: TDataSet);
          procedure QAnnEdizDataXAnnAfterPost(DataSet: TDataSet);
          procedure QAnnEdizDataXAnnBeforeEdit(DataSet: TDataSet);
          procedure QSiti2AfterOpen(DataSet: TDataSet);
          procedure QAnnSettoriBeforeOpen(DataSet: TDataSet);
          procedure qTestoAnnBeforePost(DataSet: TDataSet);
     private
          xIDTimeSheet: integer;
     public
          { Public declarations }
          xSitoPubb: string;
          xAnnuncioForm: boolean;
          procedure Aggiorna_View_RegProv(xIDAnnuncio: integer);
          procedure IndicizzaAnnuncio(xIDPubb: integer);
          procedure AggiornaFeed(xQuale, xURLFeed, xURLPrelievoFeed: string);
          procedure AggiornaXMLSimplyHired(xURLFeed, xURLPrelievoFeed: string);
          procedure AggiornaXML_Indeed(xURLFeed, xURLPrelievoFeed: string);
          procedure AggiornaXML_Trovit(xURLFeed, xURLPrelievoFeed: string);
          procedure AggiornaXML_CambioLavoro(xURLFeed, xURLPrelievoFeed: string);
     end;

var
     DataAnnunci: TDataAnnunci;

implementation

uses Main, ModuloDati, uUtilsVarie;

{$R *.DFM}

procedure TDataAnnunci.DataAnnunciCreate(Sender: TObject);
begin
     xAnnuncioForm := False;
end;

procedure TDataAnnunci.DsPenetrazStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsPenetraz.State in [dsEdit, dsInsert];
     MainForm.TbBPenetrazNew.Enabled := not b;
     MainForm.TbBPenetrazDel.Enabled := not b;
     MainForm.TbBPenetrazOK.Enabled := b;
     MainForm.TbBPenetrazCan.Enabled := b;
end;

procedure TDataAnnunci.DsScontiStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsSconti.State in [dsEdit, dsInsert];
     MainForm.TbBScontiNew.Enabled := not b;
     MainForm.TbBScontiDel.Enabled := not b;
     MainForm.TbBScontiOK.Enabled := b;
     MainForm.TbBScontiCan.Enabled := b;
end;

procedure TDataAnnunci.DsGiorniIndicatiStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsGiorniIndicati.State in [dsEdit, dsInsert];
     MainForm.TbBGiorniNew.Enabled := not b;
     MainForm.TbBGiorniDel.Enabled := not b;
     MainForm.TbBGiorniOK.Enabled := b;
     MainForm.TbBGiorniCan.Enabled := b;
end;

procedure TDataAnnunci.DsAnnunciStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsAnnunci.State in [dsEdit, dsInsert];
     MainForm.TbBAnnNew.Enabled := not b;
     MainForm.TbBAnnDel.Enabled := not b;
     MainForm.TbBAnnOK.Enabled := b;
     MainForm.TbBAnnCan.Enabled := b;
     MainForm.TbBAnnOK2.Enabled := b;
     MainForm.TbBAnnCan2.Enabled := b;
end;

procedure TDataAnnunci.TAnnunci_OLDAfterClose(DataSet: TDataSet);
begin
     TAnnunci.SQL.text :=
          ' select Ann_Annunci.ID,Rif,Data,Civetta,FileTesto,NumModuli,NumParole, ' +
          ' TotaleLire,TotaleEuro,Ann_Annunci.Note,Archiviato,CVPervenuti, ' +
          ' CVIdonei,CVChiamati,IDFattura,Ann_Annunci.IDCliente,IDUtente, ' +
          ' IDTipoAnnuncio,IDSede,IDUtenteProj,IDMansione,Ann_Annunci.RuoloWeb, ' +
          ' EmailHost,EmailPort,EmailUserID,EmailPassword, ' +
          ' IDCentroCostoCG,IDContrattoCG,LKCliente,LKMansione, ' +
          ' JobTitle,JobZone,JobDescription, fileannunciohtml, InEvidenza, ' +
          ' Skills,IDQuestionario,  EBC_Clienti.Descrizione Cliente, ' +
          '           Users1.Nominativo Utente, Users2.Nominativo UtenteProj, ' +
          '           Mansioni.Descrizione RuoloAnnuncio,VisibilitaAnnunciWeb, ' +
          '           Aziende.descrizione Sede, Ann_TipiAnnuncio.TipoAnnuncio, QualeRuoloUsare, ' +
          '           IDIndirizzoCliente ,EBC_ClienteIndirizzi.Comune IndirizzoCliente, EBC_ClienteIndirizzi.Descrizione DescIndirizzo, View_RegProv ' +
          ' from Ann_Annunci left join EBC_Clienti on Ann_Annunci.IDCliente = EBC_Clienti.ID ' +
          ' left join Users Users1 on Ann_Annunci.IDUtente = Users1.ID ' +
          ' left join Users Users2 on Ann_Annunci.IDUtenteProj = Users2.ID ' +
          ' left join Mansioni on Ann_Annunci.IDMansione = Mansioni.ID ' +
          ' left join Aziende on Ann_Annunci.IDSede = Aziende.ID ' +
          ' left join Ann_TipiAnnuncio on Ann_Annunci.IDtipoAnnuncio = Ann_TipiAnnuncio.ID ' +
          ' left join EBC_ClienteIndirizzi on Ann_Annunci.IDIndirizzoCliente = EBC_ClienteIndirizzi.ID ';
end;

//[/TONI20020724\]

procedure TDataAnnunci.QAnnEdizDataXAnn_OLDCalcFields(DataSet: TDataSet);
var xGiorno: string;
begin
     {     case dayofweek(QAnnEdizDataXannData.Value) of
               1: QAnnEdizDataXannGiorno.Value:='Dom';
               2: QAnnEdizDataXannGiorno.Value:='Lun';
               3: QAnnEdizDataXannGiorno.Value:='Mar';
               4: QAnnEdizDataXannGiorno.Value:='Mer';
               5: QAnnEdizDataXannGiorno.Value:='Gio';
               6: QAnnEdizDataXannGiorno.Value:='Ven';
               7: QAnnEdizDataXannGiorno.Value:='Sab';
          end;

          xGiorno:='FER';
          if dayofweek(QAnnEdizDataXannData.Value)=1 then xGiorno:='FES';
          if TFestivi.FindKey([QAnnEdizDataXannData.Value]) then xGiorno:='FES';
          if dayofweek(QAnnEdizDataXannData.Value)=QAnnEdizDataXannGiornoRPQ.Value then xGiorno:='RPQ';
          QAnnEdizDataXannTipoGiorno.Value:=xGiorno;}

     case dayofweek(DataSet.FieldByName('Data').Value) of
          1: DataSet.FieldByName('Giorno').Value := 'Dom';
          2: DataSet.FieldByName('Giorno').Value := 'Lun';
          3: DataSet.FieldByName('Giorno').Value := 'Mar';
          4: DataSet.FieldByName('Giorno').Value := 'Mer';
          5: DataSet.FieldByName('Giorno').Value := 'Gio';
          6: DataSet.FieldByName('Giorno').Value := 'Ven';
          7: DataSet.FieldByName('Giorno').Value := 'Sab';
     end;

     xGiorno := 'FER';
     if dayofweek(DataSet.FieldByName('Data').Value) = 1 then xGiorno := 'FES';
     //     if TFestivi.FindKey([DataSet.FieldByName ('Data').Value]) then xGiorno:='FES';
     if TFestivi.FindKeyADO(VarArrayOf([DataSet.FieldByName('Data').Value])) then xGiorno := 'FES';
     if dayofweek(DataSet.FieldByName('Data').Value) = DataSet.FieldByName('GiornoRPQ').Value then xGiorno := 'RPQ';
     DataSet.FieldByName('TipoGiorno').Value := xGiorno;
end;
//[/TONI20020724\]FINE

//[/TONI20020920\]DEBUGOK

procedure TDataAnnunci.QUtentiLK_OLDBeforeOpen(DataSet: TDataSet);
begin
     // setta parametro
end;
//[/TONI20020920\]DEBUGOKFINE

procedure TDataAnnunci.QAnnTimeReport_OLDBeforeOpen(DataSet: TDataSet);
begin
     QUtentiLK.Close;
     QGruppiProfLK.Close;
     QUtentiLK.Open;
     QGruppiProfLK.Open;
end;

procedure TDataAnnunci.QAnnTimeReport_OLDAfterClose(DataSet: TDataSet);
begin
     QUtentiLK.Close;
     QGruppiProfLK.Close;
end;

//[/TONI20020724\]

procedure TDataAnnunci.QAnnTimeReport_OLDAfterInsert(DataSet: TDataSet);
begin
     //     QAnnTimeReportIDAnnuncio.Value:=TAnnunciID.Value;
     DataSet.FieldByName('IDAnnuncio').AsINteger := TAnnunci.FieldByName('ID').AsINteger;
end;

procedure TDataAnnunci.QAnnTimeReport_OLDNewRecord(DataSet: TDataSet);
begin
     //[/GIULIO20020928\]
     // ripristinato (per prova) il vecchio codice
     try
          // se Delphi � aperto, d� "Errore sconosciuto", altrimenti va tranquillo !
          QAnnTimeReportIDUtente.Value := MainForm.xIDUtenteAttuale;
          QAnnTimeReportData.Value := Date;
          QUtentiLK.Locate('ID', QAnnTimeReportIDUtente.Value, []);
          QAnnTimeReportIDGruppoProfess.Value := QUtentiLKIDGruppoProfess.Value;
     finally
     end;
     {     QAnnTimeReport.FieldByname ('IDUtente').Value:=MainForm.xIDUtenteAttuale;
          QAnnTimeReport.FieldByName ('Data').Value:=Date;
          QUtentiLK.Locate('ID',MainForm.xIDUtenteAttuale, []);
          QAnnTimeReport.FieldByName ('IDGruppoProfess').Value:=QUtentiLK.FieldByName ('IDGruppoProfess').Value;}
end;

procedure TDataAnnunci.QAnnTimeReport_OLDAfterPost(DataSet: TDataSet);
var xID: integer;
begin
     {     with QAnnTimeReport do begin
               Data.DB.BeginTrans;
               try
                    ApplyUpdates;
                    // se c'� il controllo di gestione, allora inserisci o aggiorna time report
                    if copy(Data.GlobalCheckBoxIndici.Value,6,1)='1' then begin
                         if TAnnunciIDContrattoCG.AsString<>'' then begin
                              Data.QTemp.Close;
                              Data.QTemp.SQL.text:='select ID from CG_ContrattiTimeSheet '+
                                   'where IDTimeRepAnnH1Sel=:xIDTimeRepAnnH1Sel';
                              Data.QTemp.ParamByName('xIDTimeRepAnnH1Sel').asInteger:=QAnnTimeReportID.Value;
                              Data.QTemp.Open;
                              if Data.QTemp.IsEmpty then begin
                                   // ID questo record inserito
                                   QTemp.Close;
                                   QTemp.SQL.text:='select @@IDENTITY as LastID';
                                   QTemp.Open;
                                   xID:=QTemp.FieldByName('LastID').asInteger;
                                   QTemp.Close;
                                   Data.Q1.Close;
                                   Data.Q1.SQL.text:='insert into CG_ContrattiTimeSheet (IDContratto, IDUtente, Data, OreConsuntivo, Note, IDGruppoProfess, IDTimeRepAnnH1Sel) '+
                                        'values (:xIDContratto, :xIDUtente, :xData, :xOreConsuntivo, :xNote, :xIDGruppoProfess, :xIDTimeRepAnnH1Sel)';
                                   Data.Q1.ParamByName('xIDContratto').asInteger:=TAnnunciIDContrattoCG.Value;
                                   Data.Q1.ParamByName('xIDUtente').asInteger:=QAnnTimeReportIDUtente.Value;
                                   Data.Q1.ParamByName('xData').asDateTime:=QAnnTimeReportData.Value;
                                   Data.Q1.ParamByName('xOreConsuntivo').asFloat:=QAnnTimeReportOreConsuntivo.Value;
                                   Data.Q1.ParamByName('xNote').asString:=QAnnTimeReportNote.Value;
                                   Data.Q1.ParamByName('xIDGruppoProfess').asInteger:=QAnnTimeReportIDGruppoProfess.Value;
                                   Data.Q1.ParamByName('xIDTimeRepAnnH1Sel').asInteger:=xID;
                                   Data.Q1.ExecSQL;
                              end else begin
                                   Data.Q1.Close;
                                   Data.Q1.SQL.text:='update CG_ContrattiTimeSheet set IDUtente=:xIDUtente, Data=:xData,'+
                                        ' OreConsuntivo=:xOreConsuntivo, Note=:xNote, IDGruppoProfess=:xIDGruppoProfess '+
                                        'where ID=:xID';
                                   Data.Q1.ParamByName('xIDUtente').asInteger:=QAnnTimeReportIDUtente.Value;
                                   Data.Q1.ParamByName('xData').asDateTime:=QAnnTimeReportData.Value;
                                   Data.Q1.ParamByName('xOreConsuntivo').asFloat:=QAnnTimeReportOreConsuntivo.Value;
                                   Data.Q1.ParamByName('xNote').asString:=QAnnTimeReportNote.Value;
                                   Data.Q1.ParamByName('xIDGruppoProfess').asInteger:=QAnnTimeReportIDGruppoProfess.Value;
                                   Data.Q1.ParamByName('xID').asInteger:=Data.QTemp.FieldByName('ID').asInteger;
                                   Data.Q1.ExecSQL;
                              end;
                              Data.QTemp.Close;
                         end;
                    end;
                    Data.DB.CommitTrans;
                    xID:=QAnnTimeReportID.Value;
                    Close;
                    Open;
                    Locate('ID',xID,[]);
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate',mtError, [mbOK],0);
                    raise;
               end;
               CommitUpdates;
          end;}
     with QAnnTimeReport do begin
          Data.DB.BeginTrans;
          try
               // se c'� il controllo di gestione, allora inserisci o aggiorna time report
               if copy(Data.Global.FieldByName('CheckBoxIndici').Value, 6, 1) = '1' then begin
                    if TAnnunci.FieldByName('IDContrattoCG').AsString <> '' then begin
                         Data.QTemp.Close;
                         Data.QTemp.SQL.text := 'select ID from CG_ContrattiTimeSheet ' +
                              'where IDTimeRepAnnH1Sel=' + IntToStr(DataSet.FieldByName('ID').Value);
                         Data.QTemp.Open;
                         if Data.QTemp.IsEmpty then begin
                              // ID questo record inserito
                              QTemp.Close;
                              QTemp.SQL.text := 'select @@IDENTITY as LastID';
                              QTemp.Open;
                              xID := QTemp.FieldByName('LastID').asInteger;
                              QTemp.Close;
                              Data.Q1.Close;
                              Data.Q1.SQL.text := 'insert into CG_ContrattiTimeSheet (IDContratto, IDUtente, Data, OreConsuntivo, Note, IDGruppoProfess, IDTimeRepAnnH1Sel) ' +
                                   'values (:xIDContratto:, :xIDUtente:, :xData:, :xOreConsuntivo:, :xNote:, :xIDGruppoProfess:, :xIDTimeRepAnnH1Sel:)';
                              Data.Q1.ParamByName['xIDContratto'] := TAnnunci.FieldByName('IDContrattoCG').Asinteger;
                              Data.Q1.ParamByName['xIDUtente'] := QAnnTimeReport.FieldByName('IDUtente').AsInteger;
                              Data.Q1.ParamByName['xData'] := QAnnTimeReport.FieldByName('Data').AsString;
                              Data.Q1.ParamByName['xOreConsuntivo'] := QAnnTimeReport.FieldByName('OreConsuntivo').AsFloat;
                              Data.Q1.ParamByName['xNote'] := QAnnTimeReport.FieldByName('Note').Value;
                              Data.Q1.ParamByName['xIDGruppoProfess'] := QAnnTimeReport.FieldByName('IDGruppoProfess').AsInteger;
                              Data.Q1.ParamByName['xIDTimeRepAnnH1Sel'] := xID;
                              Data.Q1.ExecSQL;
                         end else begin
                              Data.Q1.Close;
                              Data.Q1.SQL.text := 'update CG_ContrattiTimeSheet set IDUtente=:xIDUtente:, Data=:xData:,' +
                                   ' OreConsuntivo=:xOreConsuntivo:, Note=:xNote:, IDGruppoProfess=:xIDGruppoProfess: ' +
                                   'where ID=:xID:';
                              Data.Q1.ParamByName['xIDUtente'] := QAnnTimeReport.FieldByName('IDUtente').AsInteger;
                              Data.Q1.ParamByName['xData'] := QAnnTimeReport.FieldByName('Data').Value;
                              Data.Q1.ParamByName['xOreConsuntivo'] := QAnnTimeReport.FieldByName('OreConsuntivo').AsFloat;
                              Data.Q1.ParamByName['xNote'] := QAnnTimeReport.FieldByName('Note').AsString;
                              Data.Q1.ParamByName['xIDGruppoProfess'] := QAnnTimeReport.FieldByName('IDGruppoProfess').AsInteger;
                              Data.Q1.ParamByName['xID'] := Data.QTemp.FieldByName('ID').asInteger;
                              Data.Q1.ExecSQL;
                         end;
                         Data.QTemp.Close;
                    end;
               end;
               Data.DB.CommitTRans;
               xID := FieldByName('ID').Value;
               Locate('ID', xID, []);
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               raise;
          end;
          //          CommitUpdates;
     end;
end;
//[/TONI20020724\]FINE

procedure TDataAnnunci.DsQAnnTimeReportStateChange(Sender: TObject);
var b: boolean;
begin
     b := DsQAnnTimeReport.State in [dsEdit, dsInsert];
     MainForm.BTimeSheetNew.Enabled := not b;
     MainForm.BTimeSheetDel.Enabled := not b;
     MainForm.BTimeSheetOK.Enabled := b;
     MainForm.BTimeSheetCan.Enabled := b;
end;

//[/TONI20020724\]

procedure TDataAnnunci.QAnnTimeReport_OLDAfterDelete(DataSet: TDataSet);
begin
     {     // se c'� il controllo di gestione, allora inserisci o aggiorna time report
          with QAnnTimeReport do begin
               Data.DB.BeginTrans;
               try
                    ApplyUpdates;
                    if copy(Data.GlobalCheckBoxIndici.Value,6,1)='1' then begin
                         Data.QTemp.Close;
                         Data.QTemp.SQL.text:='delete from CG_ContrattiTimeSheet '+
                              'where IDTimeRepAnnH1Sel=:xIDTimeRepAnnH1Sel';
                         Data.QTemp.ParambyName('xIDTimeRepAnnH1Sel').asInteger:=xIDTimeSheet;
                         Data.QTemp.ExecSQL;
                    end;
                    Data.DB.CommitTrans;
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: operazione non effettuata',mtError, [mbOK],0);
               end;
          end;}
          // se c'� il controllo di gestione, allora inserisci o aggiorna time report
     with QAnnTimeReport do begin
          //Post;
          Data.DB.beginTrans;
          try
               //ApplyUpdates;
               if copy(Data.Global.FieldByName('CheckBoxIndici').Value, 6, 1) = '1' then begin
                    Data.QTemp.Close;
                    Data.QTemp.SQL.text := 'delete from CG_ContrattiTimeSheet ' +
                         'where IDTimeRepAnnH1Sel=' + IntToStr(xIDTimeSheet);
                    Data.QTemp.ExecSQL;
               end;
               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: operazione non effettuata', mtError, [mbOK], 0);
          end;
     end;

end;

procedure TDataAnnunci.QAnnTimeReport_OLDBeforeDelete(DataSet: TDataSet);
begin
     xIDTimeSheet := QAnnTimeReport.FieldByName('ID').Value;
end;

procedure TDataAnnunci.TAnnunci_OLDAfterPost(DataSet: TDataSet);
var xID: integer;
     log: tStringlist;
     xIDTemplate: string;
begin
     xID := DataAnnunci.TAnnunci.FieldByName('ID').Value;

     // Aggiornamento HTML
     log := tstringlist.Create;
     //log.text := CreaFileHTMLAnnuncio(Data.DB.ConnectionString, data.Global.fieldbyname('DirFileLog').asString + '\', '1', IntToStr(xID));
     {if DataAnnunci.QTemplateHTML.Active then DataAnnunci.QTemplateHTML.Close;
     DataAnnunci.QTemplateHTML.Open;
     if DataAnnunci.QTemplateHTML.RecordCount > 1 then
          xIDTemplate := OpenSelFromTab('Ann_TemplateHTML', 'ID', 'Descrizione', 'Template HTML', '', False)
     else xIDTemplate := '1';}
     if not DataAnnunci.QAnnEdizDataXAnn.Active then DataAnnunci.QAnnEdizDataXAnn.Open;
     RigeneraFeedWeb;
     log.text := CreaFileHTMLAnnuncio(Data.DB.ConnectionString, data.Global.fieldbyname('DirFileLog').asString + '\', xIDTemplate, IntToStr(xID), DataAnnunci.QAnnEdizDataXAnnID.AsString);
     log.SaveToFile(data.Global.fieldbyname('DirFileLog').asString + '\' + 'CreaFileHTML_' + IntToStr(xID) + '.txt');
     log.Free;

     QTemp.Close;
     QTemp.SQL.text := 'update Ann_Annunci set RuoloWeb= ' +
          ' (select case when ann.qualeruolousare=0 then (select top 1 descrizione ' +
          ' from Ann_AnnunciRuoli A JOIN mansioni m on m.id=a.idmansione where a.idannuncio=ann.id)   ' +
          ' when ann.qualeruolousare=1 then (select test= ' +
          ' case when r.titolocliente='' or r.titolocliente=null then m.descrizione ' +
          ' when r.titolocliente <> ''  then r.titolocliente ' +
          ' end ' +
          ' from Ann_AnnunciRicerche AR ' +
          ' join EBC_Ricerche R on AR.IDRicerca = R.ID ' +
          ' join Mansioni M on R.IDMansione = M.Id where AR.IDAnnuncio=ann.id) ' +
          ' when ann.qualeruolousare=2 then ann.JobTitle ' +
          ' end ' +
          ' from  Ann_Annunci ann where ID=' + inttostr(xID) + ')' +
          ' where ID=' + inttostr(xID);
     QTemp.ExecSql;
     MainForm.RGAnnFiltroClick(nil);
     DataAnnunci.TAnnunci.Locate('ID', xID, []);
end;

procedure TDataAnnunci.QUtentiLKBeforePost(DataSet: TDataSet);
begin
     QUtentiLK.ReloadSQL;
     QUtentiLK.ParamByName['xoggi'] := DateToStr(Date);
end;

procedure TDataAnnunci.QAnnFile_oldBeforeOpen(DataSet: TDataSet);
begin
     QAnnFile.SQL.text := 'select ID,FileAnnuncio from Ann_Annunci where ID=' + TAnnunciID.AsString;
end;

procedure TDataAnnunci.TAnnunciAfterScroll(DataSet: TDataSet);
var log: tStringlist;
     xIDTemplate: string;
     xID: integer;
begin
     xID := DataAnnunci.TAnnunci.FieldByName('ID').Value;
     DataAnnunci.QAnnEdizDataXann.Close;
     DataAnnunci.QAnnEdizDataXann.Open;
     if MainForm.PCAnnunci.activePage = MainForm.TSAnnTesto then begin
          QAnnFile.Close;
          QAnnFile.Open;
          MainForm.PulsantiFileAnnunci;
     end;
     if (Data.Global.FieldByName('LinkForm').AsString <> '') and (TAnnunciArchiviato.Value = FALSE) then
          MainForm.bLinkAnn.Visible := true
     else
          MainForm.bLinkAnn.Visible := false;

     /// campi pers
     if MainForm.PCAnnunci.ActivePage = MainForm.TSAnnCampiPers then begin
          if MainForm.CampiPersFrame4.TCampiPers.Active then MainForm.CampiPersFrame4.TCampiPers.close;
          MainForm.CampiPersFrame4.CaricaValori;
          MainForm.CampiPersFrame4.ImpostaCampi;
     end;
     if MainForm.PCAnnunci.activePage = MainForm.TSAnnCVPerv then begin
          AggiornaPesiAnnunci(TAnnunciID.Value, 'AnnunciElenco');
     end;

     // Aggiornamento HTML
     {log := tstringlist.Create;
     RigeneraFeedWeb;
     log.text := CreaFileHTMLAnnuncio(Data.DB.ConnectionString, data.Global.fieldbyname('DirFileLog').asString + '\', xIDTemplate, IntToStr(xID), DataAnnunci.QAnnEdizDataXAnnID.AsString);
     log.SaveToFile(data.Global.fieldbyname('DirFileLog').asString + '\' + 'CreaFileHTML_' + IntToStr(xID) + '.txt');
     log.Free;  } 
end;

procedure TDataAnnunci.QAnnFileBeforeOpen(DataSet: TDataSet);
begin
     QAnnFile.SQL.text := 'select ID,FileAnnuncio,FileNome,FileType,FileBloccato ' +
          ' from Ann_Annunci where ID=' + TAnnunciID.AsString;
end;

procedure TDataAnnunci.QAnnAnagBeforeOpen(DataSet: TDataSet);
begin
     if not mainform.CBAnnAnag.Checked then
          QAnnAnag.Parameters.ParamByName('xidutente').Value := mainform.xIDUtenteAttuale;
     //QAnnAnag.Parameters[1].Value := mainform.xIDUtenteAttuale;
//sleep(10);
end;

procedure TDataAnnunci.QAnnEdizDataXAnnAfterOpen(DataSet: TDataSet);
begin
     QPubb_Spec.Open;
end;

procedure TDataAnnunci.QAnnEdizDataXAnnAfterPost(DataSet: TDataSet);
var xID: integer;
begin
     if QAnnEdizDataXAnnMPSito.Value <> '' then begin

          if (xSitoPubb <> '') and (xSitoPubb <> QAnnEdizDataXAnnMPSito.Value) then begin
               if MessageDlg('ATTENZIONE: � stato modificato il sito di post.' + chr(13) +
                    'Verranno cancellati i valori del sito precedente.' + chr(13) +
                    'SEI SICURO DI VOLER PROCEDERE ?', mtWarning, [mbYes, mbNo], 0) = mrYes then begin
                    // cancellazione righe tabella
                    QTemp.Close;
                    QTemp.SQL.Clear;
                    QTemp.SQL.text := 'delete from MP_Pubb_Spec ' +
                         'where IDAnnEdizData=' + QAnnEdizDataXAnnID.AsString;
                    QTemp.ExecSQL;
               end;
          end;

          // inserimento righe tabella specifiche
          QTemp.Close;
          QTemp.SQL.Clear;
          QTemp.SQL.text := 'insert into MP_Pubb_Spec (IDAnnEdizData, IDTabSito) ' +
               'select ' + QAnnEdizDataXAnnID.AsString + ',ID from MP_Siti_Tabelle ' +
               'where IDSito = ' + QAnnEdizDataXAnnIDMP_Sito.AsString +
               ' and OKPickUp=1 ' +
               ' and ID not in (select IDTabSito from MP_Pubb_Spec ' +
               '                where IDAnnEdizData=' + QAnnEdizDataXAnnID.AsString + ')';
          QTemp.ExecSQL;

          QPubb_Spec.Close;
          QPubb_Spec.Open;
     end;
end;

procedure TDataAnnunci.QAnnEdizDataXAnnBeforeEdit(DataSet: TDataSet);
begin
     xSitoPubb := QAnnEdizDataXAnnMPSito.Value;
end;

procedure TDataAnnunci.QSiti2AfterOpen(DataSet: TDataSet);
begin
     QMPConfig.Open;
end;

procedure TDataAnnunci.IndicizzaAnnuncio(xIDPubb: integer);
var xURLIndicizzazione, xRuolo, xCliente, xNomeFile: string;
     xDebugFile: TStringList;
begin
     xDebugFile := TStringList.create;
     xDebugFile.add('PARAMETRO xIDPubb = ' + IntToStr(xIDPubb) + chr(13));

     // indicizzazione
     QTemp.Close;
     QTemp.SQL.clear;
     QTemp.SQL.text := 'select ANN.ID, AED.ID IDPubb, Rif, convert(nvarchar,AED.Data,105) DataPubb, C.Descrizione Cliente, ' + chr(13) +
          '  Ruolo= ' + chr(13) +
          'case ' + chr(13) +
          'when ann.qualeruolousare=0 then (select top 1 descrizione from Ann_AnnunciRuoli A JOIN mansioni m on m.id=a.idmansione where a.idannuncio=ann.id) ' + chr(13) +
          'when ann.qualeruolousare=1 then (select ' + chr(13) +
          'test= ' + chr(13) +
          'case ' + chr(13) +
          'when r.titolocliente='''' or r.titolocliente=null then m.descrizione ' + chr(13) +
          'when r.titolocliente < '''' or r.titolocliente > '''' then r.titolocliente ' + chr(13) +
          'end ' + chr(13) +
          ' from Ann_AnnunciRicerche AR ' + chr(13) +
          '	join EBC_Ricerche R on AR.IDRicerca = R.ID ' + chr(13) +
          '	join Mansioni M on R.IDMansione = M.Id where AR.IDAnnuncio=ann.id) ' + chr(13) +
          'when ann.qualeruolousare=2 then ann.JobTitle ' + chr(13) +
          'end , ' + chr(13) +
          '  Aree.Descrizione Area, SETT.Attivita Attivita, ' + chr(13) +
          '  ANN.IDIndirizzoCliente, isnull(CI.Provincia,c.Provincia) Provincia ' + chr(13) +
          'from Ann_Annunci ANN ' + chr(13) +
          'join Ann_AnnEdizData AED on ANN.ID = AED.IDAnnuncio ' + chr(13) +
          'join Ann_Edizioni ED on AED.IDEdizione = ED.ID ' + chr(13) +
          'join Ann_AnnunciRicerche AR on ANN.ID = AR.IDAnnuncio  ' + chr(13) +
          'join EBC_Ricerche R on AR.IDRicerca = R.ID  ' + chr(13) +
          'join Mansioni M on R.IDMansione = M.Id  ' + chr(13) +
          'join Aree on M.IDArea = Aree.ID   ' + chr(13) +
          'join EBC_Clienti C on R.IDCliente = C.ID   ' + chr(13) +
          'join EBC_Attivita SETT on C.IDAttivita = SETT.ID   ' + chr(13) +
          'left outer join EBC_ClienteIndirizzi CI on ANN.IDIndirizzoCliente = CI.ID  ' + chr(13) +
          'where AED.ID = ' + IntToStr(xIDPubb);
     QTemp.Open;

     xDebugFile.add('QUERY = ' + QTemp.SQL.Text + chr(13));

     Q.Close;
     Q.SQL.clear;
     Q.SQL.text := 'select URLIndicizzazione from Ann_TemplateHTML';
     Q.Open;
     if Q.FieldByName('URLIndicizzazione').asString <> '' then begin
          xRuolo := QTemp.FieldByName('Ruolo').asString;
          xRuolo := StringReplace(xRuolo, '/', '_', [rfReplaceAll]);
          xCliente := QTemp.FieldByName('Cliente').asString;
          xNomeFile := StringReplace(xRuolo, ' ', '_', [rfReplaceAll]) + '-' + StringReplace(xCliente, ' ', '_', [rfReplaceAll]) + '-' + QTemp.FieldByName('Provincia').asString + '-' + IntToStr(xIDPubb) + '.html';

          xDebugFile.add('xRuolo = ' + xRuolo);
          xDebugFile.add('xCliente = ' + xCliente);
          xDebugFile.add('xNomeFile = ' + xNomeFile);

          xURLIndicizzazione := Q.FieldByName('URLIndicizzazione').asString;
          xURLIndicizzazione := StringReplace(xURLIndicizzazione, '#IDAnnuncio', QTemp.FieldByName('ID').asString, [rfReplaceAll]);
          xURLIndicizzazione := StringReplace(xURLIndicizzazione, '#NomeFile', xNomeFile, [rfReplaceAll]);

          xDebugFile.add('URLIndicizzazione = ' + Q.FieldByName('URLIndicizzazione').asString);

          ShellExecute(0, 'Open', pchar(xURLIndicizzazione), '', '', SW_SHOWDEFAULT);

          QTemp.Close;
          QTemp.SQL.clear;
          QTemp.SQL.text := 'update Ann_AnnEdizData set DataUltimoPrelievoStatici=getdate(), NomeFileStatico=:x0 where ID=' + IntToStr(xIDPubb);
          QTemp.Parameters[0].value := xNomeFile;
          QTemp.ExecSQL;

     end;

     xDebugFile.SaveToFile(data.Global.fieldbyname('DirFileLog').asString + '\Debug_Indicizza_' + IntToStr(xIDPubb) + '.txt');
end;

procedure TDataAnnunci.AggiornaFeed(xQuale, xURLFeed, xURLPrelievoFeed: string);
var xXML, xDebugFile: TStringList;
     xNomeAzienda, xNomeFile, xDay, xMonth, xContent: string;
     xDataPubb: tdatetime;
     xAnno, xMese, xGiorno: Word;
begin
     Q.Close;
     Q.SQL.clear;
     Q.SQL.text := 'select NomeAzienda from Global';
     Q.Open;
     xNomeAzienda := Q.FieldByName('NomeAzienda').asString;

     xDebugFile := TStringList.create;
     xDebugFile.Add('PARAMETRI');
     xDebugFile.Add('xURLFeed = ' + xURLFeed);
     xDebugFile.Add('xURLPrelievoFeed = ' + xURLPrelievoFeed + chr(13));

     // testa
     xXML := TStringList.create;
     xXML.text := '<?xml version="1.0" encoding="UTF-8"?> ' + chr(13) +
          '<rss xmlns:dc="http://purl.org/dc/elements/1.1/" version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">' + chr(13) +
          '<channel>' + chr(13) +
          ' <title>Annunci di lavoro - ' + xNomeAzienda + '</title>' + chr(13) +
          ' <link>' + xURLFeed + '</link>' + chr(13) +
          ' <description>Notifiche annunci di ' + xNomeAzienda + '</description>' + chr(13) +
          ' <atom:link href="' + xURLFeed + '/feed_' + xQuale + '.xml" rel="self" type="application/rss+xml"/>' + chr(13);

     // singoli annunci

     QTemp.Close;
     QTemp.SQL.Clear;
     {QTemp.SQL.text := 'select distinct ANN.ID, AED.ID IDPubb, Rif, AED.Data DataPubb, C.Descrizione Cliente, ' +
          'AED.NomeFileStatico, Ruolo= ' +
          'case ' +
          '  when ann.qualeruolousare=0 then (select top 1 descrizione from Ann_AnnunciRuoli A JOIN mansioni m on m.id=a.idmansione where a.idannuncio=ann.id) ' +
          '  when ann.qualeruolousare=1 then (select ' +
          '  test= ' +
          '  case ' +
          '  when r.titolocliente='''' or r.titolocliente=null then m.descrizione ' +
          '  when r.titolocliente < '''' or r.titolocliente > '''' then r.titolocliente ' +
          '  end ' +
          ' from Ann_AnnunciRicerche AR ' +
          '	join EBC_Ricerche R on AR.IDRicerca = R.ID ' +
          '	join Mansioni M on R.IDMansione = M.Id where AR.IDAnnuncio=ann.id) ' +
          'when ann.qualeruolousare=2 then ann.JobTitle ' +
          'end , ' +
          '  cast(ANN.SoloTesto as varchar(5000)) SoloTesto, Aree.Descrizione Area, SETT.Attivita Attivita, ' +
          '  ANN.IDIndirizzoCliente, CI.Comune, isnull(CI.Provincia,c.Provincia) Provincia, ' +
          '  PR.Regione, CI.CAP, TCONTR.TipologiaContratto ' +
          'from Ann_Annunci ANN ' +
          'join Ann_AnnEdizData AED on ANN.ID = AED.IDAnnuncio ' +
          'join Ann_Edizioni ED on AED.IDEdizione = ED.ID ' +
          'join Ann_AnnunciRicerche AR on ANN.ID = AR.IDAnnuncio  ' +
          'join EBC_Ricerche R on AR.IDRicerca = R.ID  ' +
          'join Mansioni M on R.IDMansione = M.Id  ' +
          'join Aree on M.IDArea = Aree.ID   ' +
          'join EBC_Clienti C on R.IDCliente = C.ID   ' +
          'join EBC_Attivita SETT on C.IDAttivita = SETT.ID   ' +
          'left outer join EBC_ClienteIndirizzi CI on ANN.IDIndirizzoCliente = CI.ID  ' +
          'left outer join ( select Prov, REG.Regione from TabCom TC ' +
          'join TabRegioni REG on TC.Regione = REG.CodRegioneH1 ) PR on CI.Provincia = PR.Prov ' +
          'left outer join TipologieContratti TCONTR on R.IDTipologiaContratto = TCONTR.ID ' +
          'where (DataScadenza is null or DataScadenza>getdate()) ';
     if xQuale <> 'Generico' then
          QTemp.SQL.Add('and AED.Feed_' + xQuale + ' = 1 ');
     }
     QTemp.SQL.text := 'select distinct ANN.ID, AED.ID IDPubb, Rif, AED.Data DataPubb, AED.NomeFileStatico, Ruolo= ' +
          'case ' +
          '  when ann.qualeruolousare=0 then (select top 1 descrizione from Ann_AnnunciRuoli A JOIN mansioni m on m.id=a.idmansione where a.idannuncio=ann.id) ' +
          '  when ann.qualeruolousare=1 then (select ' +
          '     test= ' +
          '     case ' +
          '     when r.titolocliente='''' or r.titolocliente=null then m.descrizione ' +
          '     when r.titolocliente < '''' or r.titolocliente > '''' then r.titolocliente ' +
          '     end ' +
          '     from Ann_AnnunciRicerche AR ' +
          '	join EBC_Ricerche R on AR.IDRicerca = R.ID ' +
          '	join Mansioni M on R.IDMansione = M.Id where AR.IDAnnuncio=ann.id) ' +
          '  when ann.qualeruolousare=2 then ann.JobTitle ' +
          'end , ' +
          '  cast(ANN.SoloTesto as varchar(2000)) SoloTesto, SETT.Attivita Attivita, ' +
          '  AP.Regione, AP.Provincia, TC.Cap, GL.LinkForm ' +
          'from Ann_Annunci ANN ' +
          'join Ann_AnnEdizData AED on ANN.ID = AED.IDAnnuncio ' +
          'join Ann_Edizioni ED on AED.IDEdizione = ED.ID ' +
          'join Ann_AnnunciProvince AP on ANN.ID = AP.IDAnnuncio  ' +
          'join TabCom TC on (AP.Provincia = TC.Prov and substring(TC.CAP,3,3) = ''100'' and TC.Codice is not null ) ' +
          'join Ann_AnnunciSettori ASET on ANN.ID = ASET.IDAnnuncio ' +
          'join EBC_Attivita SETT on ASET.IDSettore = SETT.ID  ' +
          'join Global GL on GL.LinkForm is not null ' +
          'where (DataScadenza is null or DataScadenza>getdate()) ' +
          'and AP.Flag_feed = 1 and ASET.Flag_feed = 1 ';
     if xQuale <> 'Generico' then
          QTemp.SQL.Add('and AED.Feed_' + xQuale + ' = 1 ');

     QTemp.Open;

     xDebugFile.Add('Query = ' + QTemp.sql.text);
     xDebugFile.Add('Records = ' + IntToStr(QTemp.RecordCount) + chr(13));

     while not QTemp.EOF do begin

          // data pubb
          xDataPubb := QTemp.FieldByName('DataPubb').asDateTime;
          DecodeDate(xDataPubb, xAnno, xMese, xGiorno);
          if DayOfWeek(xDataPubb) = 1 then xDay := 'Sun';
          if DayOfWeek(xDataPubb) = 2 then xDay := 'Mon';
          if DayOfWeek(xDataPubb) = 3 then xDay := 'Tue';
          if DayOfWeek(xDataPubb) = 4 then xDay := 'Wed';
          if DayOfWeek(xDataPubb) = 5 then xDay := 'Thu';
          if DayOfWeek(xDataPubb) = 6 then xDay := 'Fri';
          if DayOfWeek(xDataPubb) = 7 then xDay := 'Sat';
          case xMese of
               1: xMonth := 'Jan';
               2: xMonth := 'Feb';
               3: xMonth := 'Mar';
               4: xMonth := 'Apr';
               5: xMonth := 'May';
               6: xMonth := 'Jun';
               7: xMonth := 'Jul';
               8: xMonth := 'Aug';
               9: xMonth := 'Sep';
               10: xMonth := 'Oct';
               11: xMonth := 'Nov';
               12: xMonth := 'Dec';
          end;

          xContent := QTemp.FieldByName('SoloTesto').asString;

          xXML.add(' <item> ' + chr(13) +
               '  <title>Annuncio rif. ' + QTemp.FieldByName('Rif').asString + ' - ' + QTemp.FieldByName('Ruolo').asString + '</title> ' + chr(13) +
               '  <link>' + xURLFeed + '/' + QTemp.FieldByName('NomeFileStatico').asString + '</link> ' + chr(13) +
               '  <guid>' + xURLFeed + '/' + QTemp.FieldByName('NomeFileStatico').asString + '</guid> ' + chr(13) +
               '  <description>' + QTemp.FieldByName('Ruolo').asString + '</description> ' + chr(13) +
               '  <dc:creator>' + xNomeAzienda + '</dc:creator> ' + chr(13) +
               '  <pubDate>' + xDay + ', ' + IntToStr(xGiorno) + ' ' + xMonth + ' ' + IntToStr(xAnno) + ' 08:00:00 GMT</pubDate> ' + chr(13));
          if (xQuale = 'Generico') or (xQuale = 'RaccoltaEBC') then
               xXML.add('  <id><![CDATA[' + QTemp.FieldByName('Rif').asString + ']]></id>' + chr(13) +
                    '  <url><![CDATA[' + xURLFeed + '/' + QTemp.FieldByName('NomeFileStatico').asString + ']]></url>' + chr(13) +
                    '  <content><![CDATA[' + xContent + ']]></content>' + chr(13) +
                    '  <city><![CDATA[' + QTemp.FieldByName('Provincia').asString + ']]></city>' + chr(13) +
                    '  <region><![CDATA[' + QTemp.FieldByName('Regione').asString + ']]></region>' + chr(13) +
                    '  <postcode><![CDATA[' + QTemp.FieldByName('Cap').asString + ']]></postcode>' + chr(13) +
                    '  <location><![CDATA[' + QTemp.FieldByName('Provincia').asString + ']]></location>' + chr(13) +
                    '  <city_area><![CDATA[' + QTemp.FieldByName('Provincia').asString + ']]></city_area>' + chr(13) +
                    '  <date><![CDATA[' + IntToStr(xGiorno) + '/' + IntToStr(xMese) + '/' + IntToStr(xAnno) + ']]></date>' + chr(13));

          xXML.add(' </item>' + chr(13));

          QTemp.Next;
     end;

     // coda
     xXML.add(' </channel>' + chr(13) + '</rss>');

     xXML.text := StringReplace(xXML.text, '�', 'a''', [rfReplaceAll]);
     xXML.text := StringReplace(xXML.text, '�', 'e''', [rfReplaceAll]);
     xXML.text := StringReplace(xXML.text, '�', 'e''', [rfReplaceAll]);
     xXML.text := StringReplace(xXML.text, '�', 'i''', [rfReplaceAll]);
     xXML.text := StringReplace(xXML.text, '�', 'o''', [rfReplaceAll]);
     xXML.text := StringReplace(xXML.text, '�', 'u''', [rfReplaceAll]);

     xDebugFile.Add('XML = ' + xXML.Text + chr(13));

     // salvataggio nel DB
     QTemplateHTML.Close;
     QTemplateHTML.Open;
     QTemplateHTML.Edit;
     if xQuale = 'Facebook' then QTemplateHTMLFeed_Facebook.value := xXML.text;
     if xQuale = 'Linkedin' then QTemplateHTMLFeed_Linkedin.value := xXML.text;
     if xQuale = 'Generico' then QTemplateHTMLFeed_Generico.value := xXML.text;
     if xQuale = 'RaccoltaEBC' then QTemplateHTMLFeed_RaccoltaEBC.value := xXML.text;
     QTemplateHTML.Post;

     // prelievo da front-end
     xURLPrelievoFeed := StringReplace(xURLPrelievoFeed, '#Qualefeed', xQuale, [rfReplaceAll]);
     xNomeFile := 'Feed_' + xQuale + '.xml';
     xURLPrelievoFeed := StringReplace(xURLPrelievoFeed, '#NomeFile', xNomeFile, [rfReplaceAll]);

     xDebugFile.Add('xURLPrelievoFeed = ' + xURLPrelievoFeed);
     xDebugFile.Add('xNomeFile = ' + xNomeFile);

     ShellExecute(0, 'Open', pchar(xURLPrelievoFeed), '', '', SW_SHOWDEFAULT);

     xDebugFile.SaveToFile(data.Global.fieldbyname('DirFileLog').asString + '\Debug_feed_' + xQuale + '.txt');

     xXML.free;
end;

procedure TDataAnnunci.AggiornaXMLSimplyHired(xURLFeed, xURLPrelievoFeed: string);
var xXML, xDebugFile: TStringList;
     xQuale, xNomeAzienda, xNomeFile, xDay, xMonth, xContent: string;
     xDataPubb: tdatetime;
     xAnno, xMese, xGiorno: Word;
begin
     Q.Close;
     Q.SQL.clear;
     Q.SQL.text := 'select NomeAzienda from Global';
     Q.Open;
     xNomeAzienda := Q.FieldByName('NomeAzienda').asString;

     xQuale := 'SimplyHired';

     xDebugFile := TStringList.create;
     xDebugFile.Add('PARAMETRI');
     xDebugFile.Add('xURLFeed = ' + xURLFeed + chr(13));
     xDebugFile.Add('xURLPrelievoFeed = ' + xURLPrelievoFeed + chr(13));

     // testa
     xXML := TStringList.create;
     xXML.text := '<jobs>';

     // singoli annunci
     QTemp.Close;
     QTemp.SQL.Clear;
     QTemp.SQL.text := 'select ANN.ID, AED.ID IDPubb, Rif, AED.Data DataPubb, C.Descrizione Cliente, ' +
          'AED.NomeFileStatico, Ruolo= ' +
          'case ' +
          '  when ann.qualeruolousare=0 then (select top 1 descrizione from Ann_AnnunciRuoli A JOIN mansioni m on m.id=a.idmansione where a.idannuncio=ann.id) ' +
          '  when ann.qualeruolousare=1 then (select ' +
          '  test= ' +
          '  case ' +
          '  when r.titolocliente='''' or r.titolocliente=null then m.descrizione ' +
          '  when r.titolocliente < '''' or r.titolocliente > '''' then r.titolocliente ' +
          '  end ' +
          ' from Ann_AnnunciRicerche AR ' +
          '	join EBC_Ricerche R on AR.IDRicerca = R.ID ' +
          '	join Mansioni M on R.IDMansione = M.Id where AR.IDAnnuncio=ann.id) ' +
          'when ann.qualeruolousare=2 then ann.JobTitle ' +
          'end , ' +
          '  ANN.SoloTesto, Aree.Descrizione Area, SETT.Attivita Attivita, ' +
          '  ANN.IDIndirizzoCliente, CI.Comune, isnull(CI.Provincia,c.Provincia) Provincia ' +
          'from Ann_Annunci ANN ' +
          'join Ann_AnnEdizData AED on ANN.ID = AED.IDAnnuncio ' +
          'join Ann_Edizioni ED on AED.IDEdizione = ED.ID ' +
          'join Ann_AnnunciRicerche AR on ANN.ID = AR.IDAnnuncio  ' +
          'join EBC_Ricerche R on AR.IDRicerca = R.ID  ' +
          'join Mansioni M on R.IDMansione = M.Id  ' +
          'join Aree on M.IDArea = Aree.ID   ' +
          'join EBC_Clienti C on R.IDCliente = C.ID   ' +
          'join EBC_Attivita SETT on C.IDAttivita = SETT.ID   ' +
          'left outer join EBC_ClienteIndirizzi CI on ANN.IDIndirizzoCliente = CI.ID  ' +
          'where (DataScadenza is null or DataScadenza>getdate()) ';
     QTemp.Open;

     xDebugFile.Add('Query = ' + QTemp.sql.text);
     xDebugFile.Add('Records = ' + IntToStr(QTemp.RecordCount) + chr(13));

     while not QTemp.EOF do begin

          // data pubb
          xDataPubb := QTemp.FieldByName('DataPubb').asDateTime;
          DecodeDate(xDataPubb, xAnno, xMese, xGiorno);

          xContent := QTemp.FieldByName('SoloTesto').asString;
          xContent := StringReplace(xContent, '�', 'a''', [rfReplaceAll]);
          xContent := StringReplace(xContent, '�', 'e''', [rfReplaceAll]);
          xContent := StringReplace(xContent, '�', 'e''', [rfReplaceAll]);
          xContent := StringReplace(xContent, '�', 'i''', [rfReplaceAll]);
          xContent := StringReplace(xContent, '�', 'o''', [rfReplaceAll]);
          xContent := StringReplace(xContent, '�', 'u''', [rfReplaceAll]);

          xXML.add(' <job>' + chr(13) +
               '    <title>' + QTemp.FieldByName('Ruolo').asString + '</title>' + chr(13) +
               '    <job-code>' + QTemp.FieldByName('Rif').asString + '</job-code> ' + chr(13) +
               '    <detail-url>' + xURLFeed + '/' + QTemp.FieldByName('NomeFileStatico').asString + '</detail-url> ' + chr(13) +
               '    <description> ' + chr(13) +
               '       <summary>' + xContent + '</summary> ' + chr(13) +
               '    </description> ' + chr(13) +
               '    <posted-date>' + IntToStr(xGiorno) + '/' + IntToStr(xMese) + '/' + IntToStr(xAnno) + '</posted-date> ' + chr(13) +
               '    <location> ' + chr(13) +
               '       <city>' + QTemp.FieldByName('Comune').asString + '</city> ' + chr(13) +
               '    </location> ' + chr(13) +
               '  </job> ' + chr(13));
          QTemp.Next;
     end;

     // coda
     xXML.add(' </jobs>');

     xDebugFile.Add('XML = ' + xXML.Text + chr(13));

     // salvataggio nel DB
     QTemplateHTML.Close;
     QTemplateHTML.Open;
     QTemplateHTML.Edit;
     QTemplateHTMLFeed_SimplyHired.value := xXML.text;
     QTemplateHTML.Post;

     // prelievo da front-end
     xURLPrelievoFeed := StringReplace(xURLPrelievoFeed, '#Qualefeed', xQuale, [rfReplaceAll]);
     xNomeFile := 'Feed_' + xQuale + '.xml';
     xURLPrelievoFeed := StringReplace(xURLPrelievoFeed, '#NomeFile', xNomeFile, [rfReplaceAll]);

     xDebugFile.Add('xURLPrelievoFeed = ' + xURLPrelievoFeed);
     xDebugFile.Add('xNomeFile = ' + xNomeFile);

     ShellExecute(0, 'Open', pchar(xURLPrelievoFeed), '', '', SW_SHOWDEFAULT);

     xDebugFile.SaveToFile(data.Global.fieldbyname('DirFileLog').asString + '\Debug_feed_' + xQuale + '.txt');

     xXML.free;
end;

procedure TDataAnnunci.AggiornaXML_Indeed(xURLFeed, xURLPrelievoFeed: string);
var xXML, xDebugFile: TStringList;
     xQuale, xNomeAzienda, xNomeFile, xDay, xMonth, xContent: string;
     xDataPubb: tdatetime;
     xAnno, xMese, xGiorno: Word;
begin
     Q.Close;
     Q.SQL.clear;
     Q.SQL.text := 'select NomeAzienda, web_linkAnnunci from Global';
     Q.Open;
     xNomeAzienda := Q.FieldByName('NomeAzienda').asString;

     xQuale := 'Indeed';

     xDebugFile := TStringList.create;
     xDebugFile.Add('PARAMETRI');
     xDebugFile.Add('xURLFeed = ' + xURLFeed + chr(13));
     xDebugFile.Add('xURLPrelievoFeed = ' + xURLPrelievoFeed + chr(13));

     // testa
     xXML := TStringList.create;
     xXML.text := '<?xml version="1.0" encoding="utf-8"?>' + chr(13) +
          '<source>' + chr(13) +
          '<publisher>' + xNomeAzienda + '</publisher> ' + chr(13) +
          '<publisherurl>' + Q.FieldByName('web_linkAnnunci').asString + '</publisherurl>';

     // singoli annunci
     QTemp.Close;
     QTemp.SQL.Clear;
     QTemp.SQL.text := 'select ANN.ID, AED.ID IDPubb, Rif, AED.Data DataPubb, C.Descrizione Cliente, ' +
          'AED.NomeFileStatico, Ruolo= ' +
          'case ' +
          '  when ann.qualeruolousare=0 then (select top 1 descrizione from Ann_AnnunciRuoli A JOIN mansioni m on m.id=a.idmansione where a.idannuncio=ann.id) ' +
          '  when ann.qualeruolousare=1 then (select ' +
          '  test= ' +
          '  case ' +
          '  when r.titolocliente='''' or r.titolocliente=null then m.descrizione ' +
          '  when r.titolocliente < '''' or r.titolocliente > '''' then r.titolocliente ' +
          '  end ' +
          ' from Ann_AnnunciRicerche AR ' +
          '	join EBC_Ricerche R on AR.IDRicerca = R.ID ' +
          '	join Mansioni M on R.IDMansione = M.Id where AR.IDAnnuncio=ann.id) ' +
          'when ann.qualeruolousare=2 then ann.JobTitle ' +
          'end , ' +
          '  ANN.SoloTesto, Aree.Descrizione Area, SETT.Attivita Attivita, ' +
          '  ANN.IDIndirizzoCliente, CI.Comune, isnull(CI.Provincia,c.Provincia) Provincia, isnull(CI.NazioneAbb,''ITA'') Nazione ' +
          'from Ann_Annunci ANN ' +
          'join Ann_AnnEdizData AED on ANN.ID = AED.IDAnnuncio ' +
          'join Ann_Edizioni ED on AED.IDEdizione = ED.ID ' +
          'join Ann_AnnunciRicerche AR on ANN.ID = AR.IDAnnuncio  ' +
          'join EBC_Ricerche R on AR.IDRicerca = R.ID  ' +
          'join Mansioni M on R.IDMansione = M.Id  ' +
          'join Aree on M.IDArea = Aree.ID   ' +
          'join EBC_Clienti C on R.IDCliente = C.ID   ' +
          'join EBC_Attivita SETT on C.IDAttivita = SETT.ID   ' +
          'left outer join EBC_ClienteIndirizzi CI on ANN.IDIndirizzoCliente = CI.ID  ' +
          'where (DataScadenza is null or DataScadenza>getdate()) ';
     QTemp.Open;

     xDebugFile.Add('Query = ' + QTemp.sql.text);
     xDebugFile.Add('Records = ' + IntToStr(QTemp.RecordCount) + chr(13));

     while not QTemp.EOF do begin

          // data pubb
          xDataPubb := QTemp.FieldByName('DataPubb').asDateTime;
          DecodeDate(xDataPubb, xAnno, xMese, xGiorno);

          xXML.add('<job>' + chr(13) +
               '    <title><![CDATA[' + QTemp.FieldByName('Ruolo').asString + ']]></title>' + chr(13) +
               '    <date><![CDATA[' + IntToStr(xGiorno) + '/' + IntToStr(xMese) + '/' + IntToStr(xAnno) + ']]></date> ' + chr(13) +
               '    <referencenumber><![CDATA[' + QTemp.FieldByName('Rif').asString + ']]></referencenumber> ' + chr(13) +
               '    <url><![CDATA[' + xURLFeed + '/' + QTemp.FieldByName('NomeFileStatico').asString + ']]></url> ' + chr(13) +
               '    <city><![CDATA[' + QTemp.FieldByName('Comune').asString + ']]></city> ' + chr(13) +
               '    <country><![CDATA[' + QTemp.FieldByName('Nazione').asString + ']]></country> ' + chr(13) +
               '    <description><![CDATA[' + QTemp.FieldByName('SoloTesto').asString + ']]></description> ' + chr(13) +
               '  </job> ' + chr(13));
          QTemp.Next;
     end;

     // coda
     xXML.add(' </source>');

     xDebugFile.Add('XML = ' + xXML.Text + chr(13));

     // salvataggio nel DB
     QTemplateHTML.Close;
     QTemplateHTML.Open;
     QTemplateHTML.Edit;
     QTemplateHTMLFeed_Indeed.value := xXML.text;
     QTemplateHTML.Post;

     // prelievo da front-end
     xURLPrelievoFeed := StringReplace(xURLPrelievoFeed, '#Qualefeed', xQuale, [rfReplaceAll]);
     xNomeFile := 'Feed_' + xQuale + '.xml';
     xURLPrelievoFeed := StringReplace(xURLPrelievoFeed, '#NomeFile', xNomeFile, [rfReplaceAll]);

     xDebugFile.Add('xURLPrelievoFeed = ' + xURLPrelievoFeed);
     xDebugFile.Add('xNomeFile = ' + xNomeFile);

     ShellExecute(0, 'Open', pchar(xURLPrelievoFeed), '', '', SW_SHOWDEFAULT);

     xDebugFile.SaveToFile(data.Global.fieldbyname('DirFileLog').asString + '\Debug_feed_' + xQuale + '.txt');

     xXML.free;
end;

procedure TDataAnnunci.AggiornaXML_Trovit(xURLFeed, xURLPrelievoFeed: string);
var xXML, xDebugFile: TStringList;
     xQuale, xNomeAzienda, xNomeFile, xDay, xMonth, xContent: string;
     xDataPubb: tdatetime;
     xAnno, xMese, xGiorno: Word;
begin
     Q.Close;
     Q.SQL.clear;
     Q.SQL.text := 'select NomeAzienda, web_linkAnnunci from Global';
     Q.Open;
     xNomeAzienda := Q.FieldByName('NomeAzienda').asString;

     xQuale := 'Trovit';

     xDebugFile := TStringList.create;
     xDebugFile.Add('PARAMETRI');
     xDebugFile.Add('xURLFeed = ' + xURLFeed + chr(13));
     xDebugFile.Add('xURLPrelievoFeed = ' + xURLPrelievoFeed + chr(13));

     // testa
     xXML := TStringList.create;
     xXML.text := '<?xml version="1.0" encoding="utf-8"?>' + chr(13) +
          '<trovit>';

     // singoli annunci
     QTemp.Close;
     QTemp.SQL.Clear;
     QTemp.SQL.text := 'select ANN.ID, AED.ID IDPubb, Rif, AED.Data DataPubb, C.Descrizione Cliente, ' +
          'AED.NomeFileStatico, Ruolo= ' +
          'case ' +
          '  when ann.qualeruolousare=0 then (select top 1 descrizione from Ann_AnnunciRuoli A JOIN mansioni m on m.id=a.idmansione where a.idannuncio=ann.id) ' +
          '  when ann.qualeruolousare=1 then (select ' +
          '  test= ' +
          '  case ' +
          '  when r.titolocliente='''' or r.titolocliente=null then m.descrizione ' +
          '  when r.titolocliente < '''' or r.titolocliente > '''' then r.titolocliente ' +
          '  end ' +
          ' from Ann_AnnunciRicerche AR ' +
          '	join EBC_Ricerche R on AR.IDRicerca = R.ID ' +
          '	join Mansioni M on R.IDMansione = M.Id where AR.IDAnnuncio=ann.id) ' +
          'when ann.qualeruolousare=2 then ann.JobTitle ' +
          'end , ' +
          '  ANN.SoloTesto, Aree.Descrizione Area, SETT.Attivita Attivita, ' +
          '  ANN.IDIndirizzoCliente, CI.Comune, isnull(CI.Provincia,c.Provincia) Provincia, isnull(CI.NazioneAbb,''ITA'') Nazione ' +
          'from Ann_Annunci ANN ' +
          'join Ann_AnnEdizData AED on ANN.ID = AED.IDAnnuncio ' +
          'join Ann_Edizioni ED on AED.IDEdizione = ED.ID ' +
          'join Ann_AnnunciRicerche AR on ANN.ID = AR.IDAnnuncio  ' +
          'join EBC_Ricerche R on AR.IDRicerca = R.ID  ' +
          'join Mansioni M on R.IDMansione = M.Id  ' +
          'join Aree on M.IDArea = Aree.ID   ' +
          'join EBC_Clienti C on R.IDCliente = C.ID   ' +
          'join EBC_Attivita SETT on C.IDAttivita = SETT.ID   ' +
          'left outer join EBC_ClienteIndirizzi CI on ANN.IDIndirizzoCliente = CI.ID  ' +
          'where (DataScadenza is null or DataScadenza>getdate()) ' +
          'and len(cast(solotesto as varchar(256))) > 30';
     QTemp.Open;

     xDebugFile.Add('Query = ' + QTemp.sql.text);
     xDebugFile.Add('Records = ' + IntToStr(QTemp.RecordCount) + chr(13));

     while not QTemp.EOF do begin

          // data pubb
          xDataPubb := QTemp.FieldByName('DataPubb').asDateTime;
          DecodeDate(xDataPubb, xAnno, xMese, xGiorno);

          xXML.add('<ad>' + chr(13) +
               '    <id><![CDATA[' + QTemp.FieldByName('IDPubb').asString + ']]></id>' + chr(13) +
               '    <title><![CDATA[' + QTemp.FieldByName('Ruolo').asString + ']]></title>' + chr(13) +
               '    <url><![CDATA[' + xURLFeed + '/' + QTemp.FieldByName('NomeFileStatico').asString + ']]></url> ' + chr(13) +
               '    <content><![CDATA[' + QTemp.FieldByName('SoloTesto').asString + ']]></content> ' + chr(13) +
               '    <city><![CDATA[' + QTemp.FieldByName('Comune').asString + ']]></city> ' + chr(13) +
               '    <date><![CDATA[' + IntToStr(xGiorno) + '/' + IntToStr(xMese) + '/' + IntToStr(xAnno) + ']]></date> ' + chr(13) +
               '  </ad> ' + chr(13));
          QTemp.Next;
     end;

     // coda
     xXML.add(' </trovit>');

     xDebugFile.Add('XML = ' + xXML.Text + chr(13));

     // salvataggio nel DB
     QTemplateHTML.Close;
     QTemplateHTML.Open;
     QTemplateHTML.Edit;
     QTemplateHTMLFeed_Trovit.value := xXML.text;
     QTemplateHTML.Post;

     // prelievo da front-end
     xURLPrelievoFeed := StringReplace(xURLPrelievoFeed, '#Qualefeed', xQuale, [rfReplaceAll]);
     xNomeFile := 'Feed_' + xQuale + '.xml';
     xURLPrelievoFeed := StringReplace(xURLPrelievoFeed, '#NomeFile', xNomeFile, [rfReplaceAll]);

     xDebugFile.Add('xURLPrelievoFeed = ' + xURLPrelievoFeed);
     xDebugFile.Add('xNomeFile = ' + xNomeFile);

     ShellExecute(0, 'Open', pchar(xURLPrelievoFeed), '', '', SW_SHOWDEFAULT);

     xDebugFile.SaveToFile(data.Global.fieldbyname('DirFileLog').asString + '\Debug_feed_' + xQuale + '.txt');

     xXML.free;
end;

procedure TDataAnnunci.AggiornaXML_CambioLavoro(xURLFeed, xURLPrelievoFeed: string);
var xXML, xDebugFile: TStringList;
     xQuale, xNomeAzienda, xNomeFile, xDay, xMonth, xContent, xLink: string;
     xDataPubb: tdatetime;
     xAnno, xMese, xGiorno: Word;
begin
     Q.Close;
     Q.SQL.clear;
     Q.SQL.text := 'select NomeAzienda, web_linkAnnunci from Global';
     Q.Open;
     xNomeAzienda := Q.FieldByName('NomeAzienda').asString;

     xQuale := 'CambioLavoro';

     xDebugFile := TStringList.create;
     xDebugFile.Add('PARAMETRI');
     xDebugFile.Add('xURLFeed = ' + xURLFeed + chr(13));
     xDebugFile.Add('xURLPrelievoFeed = ' + xURLPrelievoFeed + chr(13));

     // testa
     xXML := TStringList.create;
     xXML.text := '<?xml version="1.0" encoding="utf-8"?>' + chr(13) +
          '<CambioLavoro>';

     // singoli annunci
     QTemp.Close;
     QTemp.SQL.Clear;
     QTemp.SQL.text := 'select ANN.ID, AED.ID IDPubb, Rif, AED.Data DataPubb, AED.NomeFileStatico, Ruolo= ' +
          'case ' +
          '  when ann.qualeruolousare=0 then (select top 1 descrizione from Ann_AnnunciRuoli A JOIN mansioni m on m.id=a.idmansione where a.idannuncio=ann.id) ' +
          '  when ann.qualeruolousare=1 then (select ' +
          '     test= ' +
          '     case ' +
          '     when r.titolocliente='''' or r.titolocliente=null then m.descrizione ' +
          '     when r.titolocliente < '''' or r.titolocliente > '''' then r.titolocliente ' +
          '     end ' +
          '     from Ann_AnnunciRicerche AR ' +
          '	join EBC_Ricerche R on AR.IDRicerca = R.ID ' +
          '	join Mansioni M on R.IDMansione = M.Id where AR.IDAnnuncio=ann.id) ' +
          '  when ann.qualeruolousare=2 then ann.JobTitle ' +
          'end , ' +
          '  ANN.SoloTesto, SETT.Attivita Attivita, ' +
          '  isnull(AP.Regione,'''')+'' - ''+isnull(AP.Provincia,'''') Sede, GL.LinkForm ' +
          'from Ann_Annunci ANN ' +
          'join Ann_AnnEdizData AED on ANN.ID = AED.IDAnnuncio ' +
          'join Ann_Edizioni ED on AED.IDEdizione = ED.ID ' +
          'join Ann_AnnunciProvince AP on ANN.ID = AP.IDAnnuncio  ' +
          'join Ann_AnnunciSettori ASET on ANN.ID = ASET.IDAnnuncio ' +
          'join EBC_Attivita SETT on ASET.IDSettore = SETT.ID  ' +
          'join Global GL on GL.LinkForm is not null ' +
          'where (DataScadenza is null or DataScadenza>getdate()) ' +
          'and AP.Flag_feed = 1 and ASET.Flag_feed = 1 ';
     QTemp.Open;

     xDebugFile.Add('Query = ' + QTemp.sql.text);
     xDebugFile.Add('Records = ' + IntToStr(QTemp.RecordCount) + chr(13));

     while not QTemp.EOF do begin

          // data pubb
          xDataPubb := QTemp.FieldByName('DataPubb').asDateTime;
          DecodeDate(xDataPubb, xAnno, xMese, xGiorno);

          // Link
          xLink := QTemp.FieldByName('LinkForm').asString;
          if pos('IDAnnEdizData', xLink) > 0 then
               xLink := xLink + QTemp.FieldByName('IDPubb').asString
          else
               xLink := StringReplace(xLink, '#', QTemp.FieldByName('IDPubb').asString, [rfReplaceAll]);

          xXML.add('<annuncio>' + chr(13) +
               '    <id><![CDATA[' + QTemp.FieldByName('IDPubb').asString + ']]></id>' + chr(13) +
               '    <title><![CDATA[' + QTemp.FieldByName('Ruolo').asString + ']]></title>' + chr(13) +
               '    <content><![CDATA[' + QTemp.FieldByName('SoloTesto').asString + ']]></content> ' + chr(13) +
               '    <region><![CDATA[' + QTemp.FieldByName('Sede').asString + ']]></region>' + chr(13) +
               '    <url><![CDATA[' + xLink + ']]></url> ' + chr(13) +
               '    <funzione><![CDATA[' + QTemp.FieldByName('Ruolo').asString + ']]></funzione> ' + chr(13) +
               '    <canale><![CDATA[' + QTemp.FieldByName('Attivita').asString + ']]></canale> ' + chr(13) +
               '    <company><![CDATA[' + xNomeAzienda + ']]></company>' + chr(13) +
               '    <date><![CDATA[' + IntToStr(xGiorno) + '/' + IntToStr(xMese) + '/' + IntToStr(xAnno) + ']]></date> ' + chr(13) +
               '  </annuncio> ' + chr(13));
          QTemp.Next;
     end;

     // coda
     xXML.add(' </CambioLavoro>');

     xDebugFile.Add('XML = ' + xXML.Text + chr(13));

     // salvataggio nel DB
     QTemplateHTML.Close;
     QTemplateHTML.Open;
     QTemplateHTML.Edit;
     QTemplateHTMLFeed_CambioLavoro.value := xXML.text;
     QTemplateHTML.Post;

     // prelievo da front-end
     xURLPrelievoFeed := StringReplace(xURLPrelievoFeed, '#Qualefeed', xQuale, [rfReplaceAll]);
     xNomeFile := 'Feed_' + xQuale + '.xml';
     xURLPrelievoFeed := StringReplace(xURLPrelievoFeed, '#NomeFile', xNomeFile, [rfReplaceAll]);

     xDebugFile.Add('xURLPrelievoFeed = ' + xURLPrelievoFeed);
     xDebugFile.Add('xNomeFile = ' + xNomeFile);

     ShellExecute(0, 'Open', pchar(xURLPrelievoFeed), '', '', SW_SHOWDEFAULT);

     xDebugFile.SaveToFile(data.Global.fieldbyname('DirFileLog').asString + '\Debug_feed_' + xQuale + '.txt');

     xXML.free;
end;

procedure TDataAnnunci.Aggiorna_View_RegProv(xIDAnnuncio: integer);
var xDic, xReg, xProv, xNaz, xCitta: string;
begin

     // ----- OLD --------------------
     {Q2.Close;
     Q2.SQL.text := 'select distinct(Regione) from Ann_AnnunciProvince where IDAnnuncio=' + IntToStr(xIDAnnuncio);
     Q2.Open;
     xDic := '';
     while not Q2.EOF do begin
          xDic := xDic + Q2.FieldByName('Regione').asString;
          Q2.Next;
          if not Q2.EOF then xDic := xDic + ',';
     end;

     Q2.Close;
     Q2.SQL.clear;
     Q2.SQL.text := 'select distinct(Provincia) from Ann_AnnunciProvince where IDAnnuncio=' + IntToStr(xIDAnnuncio) + ' and Provincia is not null and Provincia <> ''''';
     Q2.Open;
     if not Q2.EOF then xDic := xDic + ',';
     while not Q2.EOF do begin
          xDic := xDic + Q2.FieldByName('Provincia').asString;
          Q2.Next;
          if not Q2.EOF then xDic := xDic + ',';
     end;
     }

     Q2.Close;
     Q2.SQL.clear;
     Q2.SQL.text := 'select distinct Regione,Provincia ' +
          'from Ann_AnnunciProvince where IDAnnuncio=' + IntToStr(xIDAnnuncio) + ' order by Regione, Provincia';
     Q2.Open;
     xDic := '';
     xReg := '';
     while not Q2.EOF do begin
          if Q2.FieldByName('Regione').asString <> xReg then
               if Q2.FieldByName('Provincia').asString = '' then
                    xDic := xDic + Q2.FieldByName('Regione').asString
               else xDic := xDic + Q2.FieldByName('Regione').asString + ' (' + Q2.FieldByName('Provincia').asString
          else xDic := xDic + Q2.FieldByName('Provincia').asString;
          xReg := Q2.FieldByName('Regione').asString;
          xProv := Q2.FieldByName('Provincia').asString;

          Q2.Next;

          if Q2.EOF then begin
               if xProv = '' then xDic := xDic + ' '
               else xDic := xDic + ') ';
               //if Q2.FieldByName('Regione').asString <> xReg then xDic := xDic + ') ' else xDic := xDic + ' ';
          end else begin
               if Q2.FieldByName('Regione').asString <> xReg then begin
                    if xProv = '' then xDic := xDic + '; '
                    else xDic := xDic + '); ';
               end else xDic := xDic + ', ';
          end;
     end;

     // sedi estere
     Q2.Close;
     Q2.SQL.clear;
     Q2.SQL.text := 'select distinct Nazione, Citta ' +
          'from Ann_AnnunciSediEstere where IDAnnuncio=' + IntToStr(xIDAnnuncio) + ' order by Nazione, Citta';
     Q2.Open;
     if not Q2.isempty then begin
          if xDic <> '' then
               xDic := copy(xDic, 1, length(xDic) - 1) + '; ';
          xNaz := '';
          while not Q2.EOF do begin
               if Q2.FieldByName('Nazione').asString <> xNaz then
                    if Q2.FieldByName('Citta').asString = '' then
                         xDic := xDic + Q2.FieldByName('Nazione').asString
                    else xDic := xDic + Q2.FieldByName('Nazione').asString + ' (' + Q2.FieldByName('Citta').asString
               else xDic := xDic + Q2.FieldByName('Citta').asString;
               xNaz := Q2.FieldByName('Nazione').asString;
               xCitta := Q2.FieldByName('Citta').asString;

               Q2.Next;

               if Q2.EOF then begin
                    if xCitta = '' then xDic := xDic + ' '
                    else xDic := xDic + ') ';
               end else begin
                    if Q2.FieldByName('Nazione').asString <> xNaz then begin
                         if xCitta = '' then xDic := xDic + '; '
                         else xDic := xDic + '); ';
                    end else xDic := xDic + ', ';
               end;
          end;
     end;

     Q2.Close;
     Q2.SQL.clear;
     Q2.SQL.text := 'update Ann_Annunci set View_RegProv=:x where ID = ' + IntToStr(xIDAnnuncio);
     Q2.Parameters[0].value := xDic;
     Q2.ExecSQL;

end;

procedure TDataAnnunci.QAnnSettoriBeforeOpen(DataSet: TDataSet);
begin
     QSettoriLK.Close;
     QSettoriLK.Open;
end;

procedure TDataAnnunci.qTestoAnnBeforePost(DataSet: TDataSet);
begin
     if QtestoAnn.State in [dsEdit, dsInsert] then begin
          MainForm.RichEdit1.text := qTestoAnntesto.Value;
          qTestoAnntestopulito.Value := MainForm.RichEdit1.text;
     end;

end;

end.

