unit Fattura;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     Grids, DBGrids, DtEdit97, DtEdDB97, StdCtrls, DBCtrls, Mask, Db,
     DBTables, ExtCtrls, TB97, Buttons, ComObj, ADODB, U_ADOLinkCl,
     dxCntner, dxEditor, dxExEdtr, dxDBEdtr, dxDBELib, dxTL, dxDBCtrl,
     dxDBGrid, dxDBTLCl, dxGrClms, dxLayout, dxExGrEd, dxExELib;

type
     TFatturaForm = class(TForm)
          Panel1: TPanel;
          Panel2: TPanel;
          Panel3: TPanel;
          Panel6: TPanel;
          DsFattura: TDataSource;
          DsFattDett: TDataSource;
          Label1: TLabel;
          DBEdit1: TDBEdit;
          Label2: TLabel;
          DBComboBox1: TDBComboBox;
          Label4: TLabel;
          DBEdit3: TDBEdit;
          DbDateEdit971: TDbDateEdit97;
          Label5: TLabel;
          DBComboBox3: TDBComboBox;
          Label142: TLabel;
          Label6: TLabel;
          Label8: TLabel;
          DbDateEdit972: TDbDateEdit97;
          Label9: TLabel;
          DbDateEdit974: TDbDateEdit97;
          Panel7: TPanel;
          GBRifProg: TGroupBox;
          DBEdit2: TDBEdit;
          BitBtn1: TBitBtn;
          TbBFattDettNew: TToolbarButton97;
          TbBFattDettDel: TToolbarButton97;
          TbBFattDettMod: TToolbarButton97;
          CBFronte: TCheckBox;
          CBNoteSpese: TCheckBox;
          DBCBAppoggio: TDBComboBox;
          ToolbarButton971: TToolbarButton97;
          Label10: TLabel;
          DBEdit4: TDBEdit;
          DBCBFlagIVA: TDBCheckBox;
          TFattDett_OLD: TADOLinkedQuery;
          TFattura: TADOLinkedQuery;
          QNoteSpeseFatt: TADOLinkedQuery;
          QTemp: TADOLinkedQuery;
          QAppoggio: TADOLinkedQuery;
          TRicercheABS: TThreadADOLinkedTable;
          TFattDett_OLDID: TAutoIncField;
          TFattDett_OLDIDFattura: TIntegerField;
          TFattDett_OLDDescrizione: TStringField;
          TFattDett_OLDImponibile: TFloatField;
          TFattDett_OLDPercentualeIVA: TFloatField;
          TFattDett_OLDIDCompenso: TIntegerField;
          TFattDett_OLDIDAnnuncio: TIntegerField;
          TFattDett_OLDTotale: TFloatField;
          TFatturaID: TAutoIncField;
          TFatturaTipo: TStringField;
          TFatturaIDCliente: TIntegerField;
          TFatturaData: TDateTimeField;
          TFatturaDecorrenza: TDateTimeField;
          TFatturaImporto: TFloatField;
          TFatturaIVA: TFloatField;
          TFatturaTotale: TFloatField;
          TFatturaModalitaPagamento: TStringField;
          TFatturaAssegno: TStringField;
          TFatturaScadenzaPagamento: TDateTimeField;
          TFatturaPagata: TBooleanField;
          TFatturaPagataInData: TDateTimeField;
          TFatturaAppoggioBancario: TStringField;
          TFatturaIDRicerca: TIntegerField;
          TFatturaResponsabile: TStringField;
          TFatturaFlagFuoriAppIVA63372: TBooleanField;
          TFatturaID_1: TAutoIncField;
          TFatturaDescrizione: TStringField;
          TFatturaStato: TStringField;
          TFatturaIndirizzo: TStringField;
          TFatturaCap: TStringField;
          TFatturaComune: TStringField;
          TFatturaProvincia: TStringField;
          TFatturaIDAttivita: TIntegerField;
          TFatturaTelefono: TStringField;
          TFatturaFax: TStringField;
          TFatturaPartitaIVA: TStringField;
          TFatturaCodiceFiscale: TStringField;
          TFatturaBancaAppoggio: TStringField;
          TFatturaSistemaPagamento: TStringField;
          TFatturaNoteContratto: TMemoField;
          TFatturaResponsabile_1: TStringField;
          TFatturaConosciutoInData: TDateTimeField;
          TFatturaIndirizzoLegale: TStringField;
          TFatturaCapLegale: TStringField;
          TFatturaComuneLegale: TStringField;
          TFatturaProvinciaLegale: TStringField;
          TFatturaCartellaDoc: TStringField;
          TFatturaTipoStrada: TStringField;
          TFatturaTipoStradaLegale: TStringField;
          TFatturaNumCivico: TStringField;
          TFatturaNumCivicoLegale: TStringField;
          TFatturaNazioneAbb: TStringField;
          TFatturaNazioneAbbLegale: TStringField;
          TFatturaNote: TMemoField;
          TFatturaFatturato: TFloatField;
          TFatturaNumDipendenti: TSmallintField;
          TFatturaBlocco1: TBooleanField;
          TFatturaEnteCertificatore: TStringField;
          TFatturaLibPrivacy: TBooleanField;
          TFatturaIdAzienda: TIntegerField;
          TFatturaSitoInternet: TStringField;
          TFatturaDescAttivitaAzienda: TStringField;
          TFatturaDallAnno: TIntegerField;
          TFatturaParentCompany: TStringField;
          QAppoggioAppoggioBancario1: TStringField;
          QAppoggioAppoggioBancario2: TStringField;
          QAppoggioAppoggioBancario3: TStringField;
          TFatturaRimbSpese: TBooleanField;
          DBCBRimbSpese: TDBCheckBox;
          TFatturaProgressivo: TStringField;
          SpeedButton1: TSpeedButton;
          DBCheckBox1: TDBCheckBox;
          TFatturaLetteraIntento: TBooleanField;
          Label11: TLabel;
          Label12: TLabel;
          DBLKFilOrigine: TdxDBLookupEdit;
          DBLKFilChiusura: TdxDBLookupEdit;
          QClientiFiliali: TADOQuery;
          DSClientiFiliali: TDataSource;
          TFatturaIDFilialeOrigine: TIntegerField;
          TFatturaIDFilialeChiusura: TIntegerField;
          TFatturaScadenzaPagFatt: TStringField;
          TFatturaDataStampa: TStringField;
          TFatturaIDAccordo: TIntegerField;
          dxDBLookupEdit1: TdxDBLookupEdit;
          Label151: TLabel;
          SpeedButton50: TSpeedButton;
          qaccordifiliali: TADOQuery;
          dsAccordiFiliali: TDataSource;
          qaccordifilialiID: TAutoIncField;
          qaccordifilialiDescrizione: TStringField;
          qaccordifilialiPercOrigine: TFloatField;
          qaccordifilialiPercChiusura: TFloatField;
          qaccordifilialiAttivo: TBooleanField;
          qAccordiFilialiLK: TADOQuery;
          AutoIncField1: TAutoIncField;
          StringField1: TStringField;
          FloatField1: TFloatField;
          FloatField2: TFloatField;
          BooleanField1: TBooleanField;
          TFatturaAccordoDEscrizione: TStringField;
          TFatturaAccordoPercOrigine: TFloatField;
          TFatturaAccordoPercChiusura: TFloatField;
          Label13: TLabel;
          Label14: TLabel;
          TFatturaIDFatturaA: TIntegerField;
          QPartnerLK: TADOQuery;
          QPartnerLKID: TAutoIncField;
          QPartnerLKPartner: TStringField;
          QPartnerLKTipoStradaLegale: TStringField;
          QPartnerLKIndirizzoLegale: TStringField;
          QPartnerLKNumCivicoLegale: TStringField;
          QPartnerLKCapLegale: TStringField;
          QPartnerLKComuneLegale: TStringField;
          QPartnerLKProvinciaLegale: TStringField;
          QPartnerLKPartitaIVA: TStringField;
          QPartnerLKCodiceFiscale: TStringField;
          TFatturaFatturaA: TStringField;
          Label7: TLabel;
          DBLookupComboBox1: TDBLookupComboBox;
          TFattDett: TADOQuery;
          TFattDettID: TAutoIncField;
          TFattDettIDFattura: TIntegerField;
          TFattDettDescrizione: TStringField;
          TFattDettImponibile: TFloatField;
          TFattDettPercentualeIVA: TFloatField;
          TFattDettIDCompenso: TIntegerField;
          TFattDettIDAnnuncio: TIntegerField;
          TFattDettTotale: TFloatField;
          ToolbarButton972: TToolbarButton97;
          TFatturaDataLetteraIntenti: TDateTimeField;
          DbDateEdit973: TDbDateEdit97;
          TFatturaRifProg: TStringField;
          DBGrid2: TdxDBGrid;
          DBGrid2ID: TdxDBGridMaskColumn;
          DBGrid2IDFattura: TdxDBGridMaskColumn;
          DBGrid2Descrizione: TdxDBGridMaskColumn;
          DBGrid2Imponibile: TdxDBGridCurrencyColumn;
          DBGrid2PercentualeIVA: TdxDBGridMaskColumn;
          DBGrid2IDCompenso: TdxDBGridMaskColumn;
          DBGrid2IDAnnuncio: TdxDBGridMaskColumn;
          DBGrid2Totale: TdxDBGridCurrencyColumn;
          QTemp2: TADOQuery;
          QNoteSpeseFattID: TAutoIncField;
          QNoteSpeseFattIDCliente: TIntegerField;
          QNoteSpeseFattIDRicerca: TIntegerField;
          QNoteSpeseFattIDUtente: TIntegerField;
          QNoteSpeseFattIDAnagrafica: TIntegerField;
          QNoteSpeseFattData: TDateTimeField;
          QNoteSpeseFattDescrizione: TStringField;
          QNoteSpeseFattAutomobile: TFloatField;
          QNoteSpeseFattAutostrada: TFloatField;
          QNoteSpeseFattAereoTreno: TFloatField;
          QNoteSpeseFattTaxi: TFloatField;
          QNoteSpeseFattAlbergo: TFloatField;
          QNoteSpeseFattRistoranteBar: TFloatField;
          QNoteSpeseFattParking: TFloatField;
          QNoteSpeseFattAltroDesc: TStringField;
          QNoteSpeseFattAltroImporto: TFloatField;
          QNoteSpeseFattIDFattura: TIntegerField;
          QNoteSpeseFattTotale: TFloatField;
          TFattDettIDAttivitaCommessa: TIntegerField;
          Panel4: TPanel;
          Panel5: TPanel;
          Label3: TLabel;
          DBEdit5: TDBEdit;
          DBEdit6: TDBEdit;
          Label15: TLabel;
          dxDBLookupEdit2: TdxDBLookupEdit;
          Splitter1: TSplitter;
          TFatturaIDSede: TIntegerField;
          QSedi: TADOQuery;
          QSediID: TAutoIncField;
          QSediDescrizione: TStringField;
          DSSedi: TDataSource;
          Label16: TLabel;
          QValute: TADOQuery;
          QValuteID: TAutoIncField;
          QValuteValuta: TStringField;
          QValuteSimbolo: TStringField;
          QValuteDiminutivo: TStringField;
          QValuteCarattere: TStringField;
          dxDBLookupEdit3: TdxDBLookupEdit;
          DSValute: TDataSource;
          TFatturaIDValuta: TIntegerField;
          dxDBExtLookupEdit1: TdxDBExtLookupEdit;
          dxDBGridLayoutList1: TdxDBGridLayoutList;
          dxDBGridLayoutList1Item1: TdxDBGridLayout;
          TFatturaValutaLK: TStringField;
          DBCBSplitPayment: TDBCheckBox;
          TFatturaSplitPayment: TBooleanField;
    bTabAttivitaFuture: TToolbarButton97;
          procedure TbBFattDettNewClick(Sender: TObject);
          procedure TFattDett_OLDCalcFields(DataSet: TDataSet);
          procedure BitBtn1Click(Sender: TObject);
          procedure TbBFattDettDelClick(Sender: TObject);
          procedure BitBtn2Click(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure DBComboBox3Change(Sender: TObject);
          procedure QNoteSpeseFatt_OLDCalcFields(DataSet: TDataSet);
          procedure TbBFattDettModClick(Sender: TObject);
          procedure ToolbarButton971Click(Sender: TObject);
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
          procedure SpeedButton1Click(Sender: TObject);
          procedure FormCreate(Sender: TObject);
          procedure SpeedButton50Click(Sender: TObject);
          procedure TFatturaAfterPost(DataSet: TDataSet);
          procedure dxDBLookupEdit1Change(Sender: TObject);
          procedure TFatturaAfterOpen(DataSet: TDataSet);
          procedure TFattDettCalcFields(DataSet: TDataSet);
          procedure TFattDettBeforeOpen(DataSet: TDataSet);
          procedure DBCBFlagIVAClick(Sender: TObject);
          procedure DBCheckBox1Click(Sender: TObject);
          procedure QNoteSpeseFattCalcFields(DataSet: TDataSet);
    procedure bTabAttivitaFutureClick(Sender: TObject);
     private
          { Private declarations }
     public
          xUltimoInserito: integer;
     end;

var
     FatturaForm: TFatturaForm;

implementation

uses InsDettFatt, ModuloDati, ModuloDati2, ElencoRicCliente, ModFatt,
     uUtilsVarie, main, uOLEMessageFilter, uModelliWord_remote, uSendMailMAPI;

{$R *.DFM}

procedure TFatturaForm.TbBFattDettNewClick(Sender: TObject);
begin
     if DsFattura.State in [dsInsert, dsEdit] then begin
          with TFattura do begin
               Data.DB.BeginTrans;
               try
                    Post;
                    Data.DB.CommitTrans;
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               end;
          end;
     end;
     InsDettFattForm := TInsDettFattForm.create(self);
     InsDettFattForm.ShowModal;
     InsDettFattForm.Free;
     TFattDett.Close;
     TFattDett.Open;
end;

procedure TFatturaForm.TFattDett_OLDCalcFields(DataSet: TDataSet);
begin
     //   TFattDettTotale.Value := TFattDettImponibile.Value + TFattDettImponibile.Value * TFattDettPercentualeIVA.Value / 100;
end;

procedure TFatturaForm.BitBtn1Click(Sender: TObject);
begin
     if DsFattura.State in [dsInsert, dsEdit] then begin
          with TFattura do begin
               Data.DB.BeginTrans;
               try
                    //[/TONI20020726\]
                    //                    ApplyUpdates;
                    POst;
                    Data.DB.CommitTrans;
               except
                    Data.DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               end;
               //               CommitUpdates;
               //[/TONI20020726\]FINE
          end;
     end;
end;

procedure TFatturaForm.TbBFattDettDelClick(Sender: TObject);
var tImporto, tTotal: double;
     xIDFattura: integer;
begin
     if TFattDett.RecordCount > 0 then
          if MessageDlg('Sei sicuro di voler cancellare la riga ?', mtWarning,
               [mbNo, mbYes], 0) = mrYes then begin
               with Data do begin
                    DB.BeginTrans;
                    try
                         if TFattDettIDAttivitaCommessa.asString <> '' then begin
                              Q1.Close;
                              Q1.SQL.text := 'update EBC_Ricerche_attivita set IDFattura=null where ID=' + TFattDettIDAttivitaCommessa.asString;
                              Q1.ExecSQL;
                         end;

                         Q1.Close;
                         Q1.SQL.text := 'delete FattDett where ID=' + TFattDett.FieldByName('ID').AsString;
                         Q1.ExecSQL;
                         // ricalcolo imponibile fattura
                         TFattDett.DisableControls;
                         TFattDett.Close;
                         TFattDett.Open;
                         tImporto := 0;
                         tTotal := 0;
                         while not TFattDett.EOF do begin
                              tImporto := tImporto + TFattDett.FieldByName('Imponibile').Value;
                              tTotal := tTotal + TFattDett.FieldByname('Totale').Value;
                              TFattDett.Next;
                         end;

                         Q1.SQL.text := 'update Fatture set Importo=:xImporto:,Totale=:xTotale: ' +
                              'where ID=' + TFattura.FieldByName('ID').asString;
                         Q1.ParamByName['xImporto'] := tImporto;
                         Q1.ParamByName['xTotale'] := tTotal;
                         Q1.ExecSQL;
                         TFattDett.EnableControls;
                         DB.CommitTrans;

                         TFattDett.Close;
                         xIDfattura := TFattura.FieldByName('ID').Value;
                         TFattura.Close;
                         TFattura.ParamByName['xID'] := xIDfattura;
                         TFattura.open;
                         TFattDett.Open;
                    except
                         DB.RollbackTrans;
                         MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                    end;
               end;
          end;
end;

procedure TFatturaForm.BitBtn2Click(Sender: TObject);
var xFileWord, xFattNum, xErrore, xFileModello, xCampiXML, xValDett: string;
     MSWord, MsExcel: Variant;
     xApriWord, xNuovoFile, xApriModello, xExcel: boolean;
     x, r: integer;
     xTotIva : Real;
     xgetfilepath, xSplitPayment: string;
     mesi: array[1..12] of string;
     g, m, a: word;
     IO: IOleMessageFilter;
     xXML: TStringList;
begin
     if TFattura.State = dsEdit then TFattura.Post;

     mesi[1] := 'Gennaio';
     mesi[2] := 'Febbraio';
     mesi[3] := 'Marzo';
     mesi[4] := 'Aprile';
     mesi[5] := 'Maggio';
     mesi[6] := 'Giugno';
     mesi[7] := 'Luglio';
     mesi[8] := 'Agosto';
     mesi[9] := 'Settembre';
     mesi[10] := 'Ottobre';
     mesi[11] := 'Novembre';
     mesi[12] := 'Dicembre';

     DecodeDate(TFattura.FieldByName('Data').AsDateTime, a, m, g);

     xExcel := false;

     IO := IOleMessageFilter.Create;
     IO.RegisterFilter;

     QNoteSpeseFatt.Open;
     if (QNoteSpeseFatt.RecordCount > 0) and (CBNoteSpese.Checked = False) then
          if MessageDlg('ATTENZIONE: risultano note spese collegate a questa fattura ma non � stato richiesto l''allegato:' + chr(13) +
               'si vuole stampare la fattura con l''allegato (Yes) o senza (No) ?', mtWarning, [mbYes, mbNo], 0) = mrYes then
               CBNoteSpese.Checked := true;
     QNoteSpeseFatt.Close;
     xErrore := '';
     if Data2.TEBCclienti.FieldByName('IndirizzoLegale').AsString = '' then xErrore := chr(13) + 'Indirizzo legale mancante';
     if Data2.TEBCclienti.FieldByName('ComuneLegale').AsString = '' then xErrore := xErrore + chr(13) + 'Comune (legale) mancante';
     if Data2.TEBCclienti.FieldByName('ProvinciaLegale').AsString = '' then xErrore := xErrore + chr(13) + 'Provincia (legale) mancante';
     if (Data2.TEBCclienti.FieldByName('PartitaIVA').AsString = '') or
          (Data2.TEBCclienti.FieldByName('CodiceFiscale').AsString = '') then xErrore := xErrore + chr(13) + 'Partita IVA o codice fiscale mancante';
     if (Data2.TEBCclienti.FieldByName('IndirizzoLegale').AsString = '') or
          (Data2.TEBCclienti.FieldByName('ComuneLegale').AsString = '') or
          (Data2.TEBCclienti.FieldByName('ProvinciaLegale').AsString = '') or
          (Data2.TEBCclienti.FieldByName('PartitaIVA').AsString = '') then begin
          MessageDlg('Dati sull''azienda INCOMPLETI:' + xErrore + chr(13) + 'IMPOSSIBILE PROSEGUIRE', mtError, [mbOK], 0);
          Exit;
     end else begin
          //          xFattNum:=TFatturaProgressivo.asString;
          xFattNum := TFattura.FieldByName('Progressivo').asString;
          //xFattNum := StringReplace(xFattNum, '/', '-', [rfReplaceAll]);
          if pos('/', xFattNum) > 0 then
               xFattNum := copy(xFattNum, 1, pos('/', xFattNum) - 1);

          xgetfilepath := GetPathFromTable('Fattura');

          if pos('OPENJOB', UPPERCASE(data.global.fieldbyname('nomeazienda').asstring)) > 0 then
               xExcel := true;

          if xExcel = true then begin

               // --- nome file --------------------------------------------------------------
               if xgetfilepath = '' then
                    xFileWord := GetDocPath + '\Fatt' + xFattNum + '-' + IntToStr(a) + '.xls'
               else
                    xFileWord := xgetfilepath + xFattNum + '-' + IntToStr(a) + '.xls';

               xApriModello := True;
               if FileExists(xFileWord) then
                    if MessageDlg('Il file esiste gi� - Aprirlo (Yes) o crearne uno nuovo, riscrivendo il vecchio (No) ?', mtWarning, [mbYes, mbNo], 0) = mrYes then
                         xApriModello := False;

               if (xApriModello) and (not (FileExists(GetDocPath + '\ModFattura.xls'))) then begin
                    MessageDlg('File ModFattura.xls non trovato' + chr(13) + 'controllare la directory:' + chr(13) + GetDocPath, mtError, [mbOK], 0);
                    exit;
               end;

               try
                    MsExcel := CreateOleObject('Excel.Application');
               except
                    ShowMessage('Non riesco ad aprire Microsoft Excel.');
                    Exit;
               end;

               if xApriModello then begin
                    MsExcel.workbooks.Open(GetDocPath + '\ModFattura.xls');
                    MsExcel.visible := true;


                    MsExcel.activeworkbook.activesheet.cells[13, 1] := TFattura.FieldByName('Tipo').asString;
                    //indirizzo legale
                    data.QTemp.close;
                    data.qtemp.sql.text := 'select descrizione, TipoStradaLegale,indirizzoLegale,comuneLegale,provinciaLegale,NumCivicoLegale,' +
                         'CapLegale,nazioneAbbLegale, PartitaIva,CodiceFiscale from  ebc_clienti where id=' + data2.TEbcClientiID.AsString;
                    data.qtemp.Open;

                    MsExcel.activeworkbook.activesheet.cells[7, 1] := data.qtemp.FieldByName('Descrizione').asstring;
                    MsExcel.activeworkbook.activesheet.cells[8, 1] := data.qtemp.FieldByName('TipoStradaLegale').asstring + ' ' +
                         data.qtemp.FieldByName('indirizzoLegale').asstring + ', ' + data.qtemp.FieldByName('NumCivicoLegale').asstring;
                    MsExcel.activeworkbook.activesheet.cells[9, 1] := data.qtemp.FieldByName('capLegale').asstring + ' ' +
                         data.qtemp.FieldByName('ComuneLegale').asstring + ' (' + data.qtemp.FieldByName('provinciaLegale').asstring + ')';
                    {if data.qtemp.FieldByName('PartitaIva').asstring <> '' then begin
                         MsExcel.activeworkbook.activesheet.cells[9, 1] := 'Partita Iva: ' + data.qtemp.FieldByName('PartitaIva').asstring;
                         MsExcel.activeworkbook.activesheet.cells[9, 6] := 'Partita Iva: ' + data.qtemp.FieldByName('PartitaIva').asstring;
                    end; }
                    if Trim(data.qtemp.FieldByName('CodiceFiscale').asstring) = Trim(data.qtemp.FieldByName('PartitaIva').asstring) then
                         MsExcel.activeworkbook.activesheet.cells[14, 8] := data.qtemp.FieldByName('CodiceFiscale').asstring
                    else
                         MsExcel.activeworkbook.activesheet.cells[14, 8] := data.qtemp.FieldByName('CodiceFiscale').asstring + ' / ' + data.qtemp.FieldByName('PartitaIva').asstring;

                    // indirizzo recapito fatture
                    data.QTemp.close;
                    data.qtemp.sql.text := 'select Descrizione,TipoStrada,indirizzo,comune,provincia,NumCivico,Cap,telefono,Fax,' +
                         'nazioneAbb,2 from  ebc_clienteIndirizzi where idtipologia=2 and idcliente=' + data2.TEbcClientiID.AsString;
                    data.qtemp.Open;
                    if data.qtemp.RecordCount > 0 then begin
                         MsExcel.activeworkbook.activesheet.cells[7, 6] := data.qtemp.FieldByName('Descrizione').asstring;
                         if data.qtemp.FieldByName('indirizzo').asstring <> '' then
                              MsExcel.activeworkbook.activesheet.cells[8, 6] := data.qtemp.FieldByName('TipoStrada').asstring + ' ' +
                                   data.qtemp.FieldByName('indirizzo').asstring + ', ' + data.qtemp.FieldByName('NumCivico').asstring;
                         MsExcel.activeworkbook.activesheet.cells[9, 6] := data.qtemp.FieldByName('cap').asstring + ' ' +
                              data.qtemp.FieldByName('comune').asstring + ' (' + data.qtemp.FieldByName('provincia').asstring + ')';
                    end;



                    MsExcel.activeworkbook.activesheet.cells[14, 1] := TFattura.FieldByName('Progressivo').asString;
                    MsExcel.activeworkbook.activesheet.cells[14, 2] := TFattura.FieldByName('DataStampa').asString;
                    //showmessage(TFattura.FieldByName('DataStampa').asString);
                    //showmessage(MsExcel.activeworkbook.activesheet.cells[14, 2]);
                    MsExcel.activeworkbook.activesheet.cells[14, 3] := mesi[m] + ' ' + inttostr(a);


                    MsExcel.activeworkbook.activesheet.cells[14, 4] := '---';

                    data.QTemp.close;
                    data.qtemp.sql.text := 'select * from fattdett f' +
                         ' where f.idfattura=' + TFatturaID.AsString;
                    data.qtemp.Open;
                    MsExcel.activeworkbook.activesheet.cells[50, 4] := data.qtemp.FieldByName('PercentualeIVA').asString + '%';

                    data.QTemp.close;
                    data.qtemp.sql.text := 'select cf.descrizione filialechiusura from fatture f ' +
                         ' join  ebc_clientifiliali cf on f.idfilialechiusura= cf.id ' +
                         ' where f.id=' + TFatturaID.AsString;
                    data.qtemp.Open;
                    MsExcel.activeworkbook.activesheet.cells[14, 6] := data.qtemp.fieldbyname('filialechiusura').asstring;

                    //MsExcel.activeworkbook.activesheet.cells[14, 7] := Data2.TEBCclienti.FieldByName('CodiceFiscale').AsString + ' / ' + Data2.TEBCclienti.FieldByName('PartitaIVA').AsString;

                   // tfattura.sql.SaveToFile('tfattura.sql');
                    MsExcel.activeworkbook.activesheet.cells[17, 1] := TFattura.FieldByName('ModalitaPagamento').asString;
                    MsExcel.activeworkbook.activesheet.cells[17, 5] := TFatturaScadenzaPagFatt.AsString; //TFattura.FieldByName('ScadenzaPagFatt').AsString;
                    MsExcel.activeworkbook.activesheet.cells[18, 1] := TFattura.FieldByName('AppoggioBancario').asString;


                    //riempimento colonne dettagli

                    TFattDett.First;

                    r := 25;
                    //xTotIva := 0;
                    TFattDett.First;
                    while not tfattdett.Eof do begin
                         MsExcel.activeworkbook.activesheet.cells[r, 1] := TFattDett.FieldByName('Descrizione').asString;
                         MsExcel.activeworkbook.activesheet.cells[r, 8] := TFattDett.FieldByName('Imponibile').Value;
                         TFattDett.next;
                         inc(r);
                    end;
                    MsExcel.activeworkbook.SaveAs(xFileWord);

               end else begin
                    MsExcel.workbooks.Open(xFileWord);
                    MsExcel.visible := true;
               end;

          end else begin

               // --- nome file --------------------------------------------------------------
               if xgetfilepath = '' then
                    xFileWord := GetDocPath + '\Fatt' + xFattNum + '-' + IntToStr(a) + '.doc'
               else
                    xFileWord := xgetfilepath + xFattNum + '-' + IntToStr(a) + '.doc';

               xApriModello := True;
               if FileExists(xFileWord) then
                    if MessageDlg('Il file esiste gi� - Aprirlo (Yes) o crearne uno nuovo, riscrivendo il vecchio (No) ?', mtWarning, [mbYes, mbNo], 0) = mrYes then
                         xApriModello := False;

               // controllo esistenza file modello
               if TFattura.FieldByName('Tipo').asString = 'Nota di accredito' then begin
                    if (xApriModello) and (not (FileExists(GetDocPath + '\ModNotaAccr.doc'))) then begin
                         MessageDlg('File ModNotaAccr.doc non trovato' + chr(13) + 'controllare la directory:' + chr(13) + GetDocPath, mtError, [mbOK], 0);
                         exit;
                    end;
               end else begin
                    if (xApriModello) and (not (FileExists(GetDocPath + '\ModFattura.doc'))) then begin
                         MessageDlg('File ModFattura.doc non trovato' + chr(13) + 'controllare la directory:' + chr(13) + GetDocPath, mtError, [mbOK], 0);
                         exit;
                    end;
               end;

               if MainForm.xSendMail_Remote then begin

                    if xApriModello then begin

                    // Dati XML
                         if pos('PROPOSTE', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) > 0 then
                              xCampiXML := xCampiXML + '    <campo etichetta="#Tipo" tabellare="0" valore="' + TFattura.FieldByName('Tipo').AsString + '"/>' + chr(13);

                         xCampiXML := xCampiXML + '    <campo etichetta="#Numero" tabellare="0" valore="' + TFattura.FieldByName('Progressivo').asString + '"/>' + chr(13);


                         xCampiXML := xCampiXML + '    <campo etichetta="#DataFattura" tabellare="0" valore="' + TFattura.FieldByName('Data').asString + '"/>' + chr(13);

                         // solo per nota di accredito
                         if TFattura.FieldByName('Tipo').asString = 'Nota di accredito' then begin
                              xCampiXML := xCampiXML + '    <campo etichetta="#RifProg" tabellare="0" valore="' + TFattura.FieldByName('RifProg').asString + '"/>' + chr(13);

                              QTemp2.Close;
                              QTemp2.SQL.text := 'select convert(nvarchar,data,103) DataFatt from fatture where Progressivo = :x';
                              QTemp2.Parameters[0].value := TFattura.FieldByName('RifProg').asString;
                              QTemp2.Open;
                              if not QTemp2.isempty then begin
                                   xCampiXML := xCampiXML + '    <campo etichetta="#DataRifProg" tabellare="0" valore="' + QTemp2.FieldByName('DataFatt').asString + '"/>' + chr(13);
                              end else begin
                                   xCampiXML := xCampiXML + '    <campo etichetta="#DataRifProg" tabellare="0" valore=""/>' + chr(13);
                              end;
                              QTemp2.Close;
                         end;
                         if TFatturaIDCliente.value = TFatturaIDFatturaA.Value then begin
                              xCampiXML := xCampiXML + '    <campo etichetta="#NomeAzienda" tabellare="0" valore="' + Data2.TEBCclienti.FieldByName('Descrizione').AsString + '"/>' + chr(13);
                              xCampiXML := xCampiXML + '    <campo etichetta="#RifRespFatt" tabellare="0" valore="' + Trim(TFattura.FieldByName('Responsabile').AsString) + '"/>' + chr(13);
                              xCampiXML := xCampiXML + '    <campo etichetta="#IndirizzoAzienda" tabellare="0" valore="' + Data2.TEBCClienti.FieldByName('TipoStradaLegale').AsString + ' ' + Data2.TEBCclienti.FieldByName('IndirizzoLegale').AsString + ' ' + Data2.TEBCClienti.FieldByName('NumCivicoLegale').AsString + '"/>' + chr(13);
                              xCampiXML := xCampiXML + '    <campo etichetta="#CapComuneProvAzienda" tabellare="0" valore="' + Data2.TEBCclienti.FieldByName('CapLegale').AsString + ' ' + Data2.TEBCclienti.FieldByName('ComuneLegale').AsString + ' (' + Data2.TEBCclienti.FieldByName('ProvinciaLegale').AsString + ')' + '"/>' + chr(13);
                              xCampiXML := xCampiXML + '    <campo etichetta="#PartitaIVA" tabellare="0" valore="' + Data2.TEBCclienti.FieldByName('PartitaIVA').AsString + ' / ' + Data2.TEBCclienti.FieldByName('CodiceFiscale').AsString + '"/>' + chr(13);
                         end else begin
                              xCampiXML := xCampiXML + '    <campo etichetta="#NomeAzienda" tabellare="0" valore="' + QPartnerLKPartner.AsString + '"/>' + chr(13);
                              xCampiXML := xCampiXML + '    <campo etichetta="#RifRespFatt" tabellare="0" valore="' + Trim(TFattura.FieldByName('Responsabile').AsString) + '"/>' + chr(13);
                              xCampiXML := xCampiXML + '    <campo etichetta="#IndirizzoAzienda" tabellare="0" valore="' + QPartnerLKTipoStradaLegale.AsString + ' ' + QPartnerLKIndirizzoLegale.AsString + ' ' + QPartnerLKNumCivicoLegale.AsString + '"/>' + chr(13);
                              xCampiXML := xCampiXML + '    <campo etichetta="#CapComuneProvAzienda" tabellare="0" valore="' + QPartnerLKCapLegale.AsString + ' ' + QPartnerLKComuneLegale.AsString + ' (' + QPartnerLKProvinciaLegale.AsString + ')' + '"/>' + chr(13);
                              xCampiXML := xCampiXML + '    <campo etichetta="#PartitaIVA" tabellare="0" valore="' + QPartnerLKPartitaIVA.AsString + ' / ' + QPartnerLKCodiceFiscale.AsString + '"/>' + chr(13);
                         end;

                         if pos('PROPOSTE', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) > 0 then begin
                              xCampiXML := xCampiXML + '    <campo etichetta="#Ordine" tabellare="0" valore="' + TFattura.FieldByName('RifProg').AsString + '"/>' + chr(13);
                              xCampiXML := xCampiXML + '    <campo etichetta="#Valuta" tabellare="0" valore="' + QValute.FieldByName('Simbolo').AsString + '"/>' + chr(13);
                              xCampiXML := xCampiXML + '    <campo etichetta="#Valuta" tabellare="0" valore="' + QValute.FieldByName('Simbolo').AsString + '"/>' + chr(13);
                         end;

                    //riempimento colonne dettagli
                         TFattDett.First;
                         xTotIva := 0;
                         xValDett := '';
                         while not TFattDett.EOF do begin
                              xValDett := xValDett + TFattDett.FieldByName('Descrizione').asString + '$';
                              xValDett := xValDett + FormatFloat('###,###,###.00', TFattDett.FieldByName('Imponibile').Value) + '$';
                              xValDett := xValDett + TFattDett.FieldByName('PercentualeIVA').asString + ' %' + '$';
                              xValDett := xValDett + FormatFloat('###,###,###.00', TFattDett.FieldByName('Imponibile').Value + TFattDett.FieldByName('Imponibile').Value * TFattDett.FieldByName('PercentualeIVA').Value / 100);


                                xTotIVA := xTotIVA + (TFattDett.FieldByName('Imponibile').Value * TFattDett.FieldByName('PercentualeIVA').Value / 100);

                              TFattDett.Next;
                              if not TFattDett.EOF then
                                   xValDett := xValDett + '$';
                         end;
                         xCampiXML := xCampiXML + '    <campo etichetta="#dett" tabellare="0" valore="' + xValDett + '"/>' + chr(13);


                        // showmessage('dd='+ vartostr(xTotIVA));

                         if pos('PROPOSTE', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) > 0 then begin
                              xCampiXML := xCampiXML + '    <campo etichetta="#Valuta" tabellare="0" valore="' + QValute.FieldByName('Simbolo').AsString + '"/>' + chr(13);
                         end;



                         xCampiXML := xCampiXML + '    <campo etichetta="#TotImponibile" tabellare="0" valore="' + FormatFloat('###,###,###.00', TFattura.FieldByName('Importo').Value) + '"/>' + chr(13);

                         if (TFattura.FieldByName('SplitPayment').AsBoolean = false) then begin

                              if (TFattura.FieldByName('Totale').Value - TFattura.FieldByName('Importo').Value = 0) then
                                   xCampiXML := xCampiXML + '    <campo etichetta="#TotIva" tabellare="0" valore="0"/>' + chr(13)
                              else
                                   xCampiXML := xCampiXML + '    <campo etichetta="#TotIva" tabellare="0" valore="' + FormatFloat('###,###,###.00', TFattura.FieldByName('Totale').Value - TFattura.FieldByName('Importo').Value) + '"/>' + chr(13);

                         end else begin

                              //ShowMessage(vartostr(xTotIva));
                             // xCampiXML := xCampiXML + '    <campo etichetta="#TotIva" tabellare="0" valore="' + FormatFloat('###,###,###.00', TFattura.FieldByName('Totale').Value *TFattDett.FieldByName('PercentualeIVA').value + '"/>' + chr(13);
                             xCampiXML := xCampiXML + '    <campo etichetta="#TotIva" tabellare="0" valore="' + FormatFloat('###,###,###.00', xTotIva) + '"/>' + chr(13);

                         end;

                         if TFattura.FieldByName('SplitPayment').AsBoolean = false then
                              xCampiXML := xCampiXML + '    <campo etichetta="#TotFattura" tabellare="0" valore="' + FormatFloat('###,###,###.00', TFattura.FieldByName('Totale').Value) + '"/>' + chr(13)
                         else begin
                           //se SplitPayment non devo considerare l'iva quindi il totale della fattura � = all'importo

                              xCampiXML := xCampiXML + '    <campo etichetta="Totale fattura" tabellare="0" valore="' + 'Netto a Pagare' + '"/>' + chr(13);

                              xCampiXML := xCampiXML + '    <campo etichetta="#TotFattura" tabellare="0" valore="' + FormatFloat('###,###,###.00', TFattura.FieldByName('Importo').Value) + '"/>' + chr(13);

                         end;


                         if TFattura.FieldByName('Tipo').asString = 'Fattura' then begin

                              //SplitPayment
                              if TFattura.FieldByName('SplitPayment').AsBoolean = true then begin
                                   xSplitPayment := 'Operazione soggetta a scissione dei pagamenti ex art. 17 - ter del DPR 633/72';
                              end else begin
                                   xSplitPayment := '';
                              end;

                              xCampiXML := xCampiXML + '    <campo etichetta="#SplitPayment" tabellare="0" valore="' + xSplitPayment + '"/>' + chr(13);



                              xCampiXML := xCampiXML + '    <campo etichetta="#ModalitaPagamento" tabellare="0" valore="' + TFattura.FieldByName('ModalitaPagamento').asString + '"/>' + chr(13);

                              if pos('PROPOSTE', UpperCase(Data.Global.FieldByName('NomeAzienda').value)) > 0 then begin
                                   xCampiXML := xCampiXML + '    <campo etichetta="#Decorrenza" tabellare="0" valore="' + TFattura.FieldByName('Decorrenza').AsString + '"/>' + chr(13);
                                   xCampiXML := xCampiXML + '    <campo etichetta="#Scadenza" tabellare="0" valore="' + TFattura.FieldByName('ScadenzaPagamento').AsString + '"/>' + chr(13);
                                  //aggiunte fede
                                  // xCampiXML := xCampiXML + '    <campo etichetta="#AppoggioBancario" tabellare="0" valore="' + TFattura.FieldByName('AppoggioBancario').AsString + '"/>' + chr(13);
                              end;

                              if TFattura.FieldByName('FlagFuoriAppIVA63372').Value <> NULL then begin
                                   if TFattura.FieldByName('FlagFuoriAppIVA63372').Value then begin
                                        if TFattura.FieldByname('AppoggioBAncario').asString <> '' then begin
                                             xCampiXML := xCampiXML +
                                                  '    <campo etichetta="#AppoggioBancario" tabellare="0" valore="' + TFattura.FieldByName('AppoggioBAncario').asString + chr(13) + DBCBFlagIVA.Caption + '"/>' + chr(13)
                                        end else begin
                                             xCampiXML := xCampiXML +
                                                  '    <campo etichetta="#AppoggioBancario" tabellare="0" valore="' + '----' +
                                                  chr(13) + DBCBFlagIVA.Caption + '"/>' + chr(13)
                                        end;
                                   end else begin

                                        if TFattura.FieldByname('AppoggioBAncario').asString <> '' then begin
                                             xCampiXML := xCampiXML + '    <campo etichetta="#AppoggioBancario" tabellare="0" valore="' + TFattura.FieldByName('AppoggioBancario').asString + '"/>' + chr(13)
                                        end else begin
                                         xCampiXML := xCampiXML + '    <campo etichetta="#AppoggioBancario" tabellare="0" valore="' + '-' + '"/>' + chr(13)
                                        end;

                                   end;
                              end else begin
                                   if TFattura.FieldByname('AppoggioBAncario').asString <> '' then begin
                                        xCampiXML := xCampiXML + '    <campo etichetta="#AppoggioBancario" tabellare="0" valore="' + 'APPOGGIO BANCARIO: ' +
                                             TFattura.FieldByName('AppoggioBAncario').asString + '"/>' + chr(13);
                                   end else begin
                                        xCampiXML := xCampiXML + '    <campo etichetta="#AppoggioBancario" tabellare="0" valore="---"/>' + chr(13);
                                   end;
                              end;
                                  // end else xCampiXML := xCampiXML + '    <campo etichetta="#AppoggioBancario" tabellare="0" valore="' + 'APPOGGIO BANCARIO: ' + TFattura.FieldByName('AppoggioBAncario').asString + '"/>' + chr(13)
                             // end else xCampiXML := xCampiXML + '    <campo etichetta="#AppoggioBancario" tabellare="0" valore="---"/>' + chr(13);

                         end;

                         // allegato note spese
                         if CBNoteSpese.Checked then begin
                              xCampiXML := xCampiXML + '    <campo etichetta="#NumFattura" tabellare="0" valore="' + Tfattura.FieldByName('Progressivo').asString + '"/>' + chr(13);
                              xCampiXML := xCampiXML + '    <campo etichetta="#DataFattura" tabellare="0" valore="' + TFattura.FieldByName('Data').asString + '"/>' + chr(13);

                              xValDett := '';
                              QNoteSpeseFatt.Open;
                              if QNoteSpeseFatt.RecordCount > 0 then begin
                                   while not QNoteSpeseFatt.EOF do begin
                                        QTemp.Close;
                                        if QNoteSpeseFatt.FieldByName('IDUtente').value <> 0 then begin
                                             QTemp.SQL.text := 'select Nominativo from Users where ID=' + QNoteSpeseFatt.FieldByName('IDUtente').asString;
                                             QTemp.Open;
                                             xValDett := xValDett + QNoteSpeseFatt.FieldByName('Descrizione').asString + ': ' + QTemp.FieldByName('Nominativo').asString + '$';
                                        end else begin
                                             QTemp.SQL.text := 'select Cognome,Nome from Anagrafica where ID=' + QNoteSpeseFatt.FieldByName('IDAnagrafica').asString;
                                             QTemp.Open;
                                             xValDett := xValDett + QNoteSpeseFatt.FieldByName('Descrizione').asString + ': ' + QTemp.FieldByName('Cognome').asString + ' ' + QTemp.FieldByName('Nome').asString + '$';
                                        end;

                                        xValDett := xValDett + FormatFloat('###,###,###.##', QNoteSpeseFatt.FieldByName('Automobile').Value) + '$';
                                        xValDett := xValDett + FormatFloat('###,###,###.##', QNoteSpeseFatt.FieldByName('Autostrada').Value) + '$';
                                        xValDett := xValDett + FormatFloat('###,###,###.##', QNoteSpeseFatt.FieldByName('AereoTreno').Value) + '$';
                                        xValDett := xValDett + FormatFloat('###,###,###.##', QNoteSpeseFatt.FieldByName('Taxi').Value) + '$';
                                        xValDett := xValDett + FormatFloat('###,###,###.##', QNoteSpeseFatt.FieldByName('Albergo').Value) + '$';
                                        xValDett := xValDett + FormatFloat('###,###,###.##', QNoteSpeseFatt.FieldByname('RistoranteBar').Value) + '$';
                                        xValDett := xValDett + FormatFloat('###,###,###.##', QNoteSpeseFatt.FieldByname('Parking').Value) + '$';
                                        xValDett := xValDett + QNoteSpeseFatt.FieldByName('AltroDesc').AsString + '  ';
                                        if QNoteSpeseFatt.FieldByName('AltroDesc').value <> '' then
                                             xValDett := xValDett + FormatFloat('###,###,###.##', QNoteSpeseFatt.FieldByName('AltroImporto').Value);
                                        xValDett := xValDett + '$';
                                        xValDett := xValDett + FormatFloat('###,###,###.##', QNoteSpeseFatt.FieldByname('Totale').Value);

                                        QNoteSpeseFatt.Next;
                                        if not QNoteSpeseFatt.EOF then
                                             xValDett := xValDett + '$';
                                   end;
                              end;
                              xCampiXML := xCampiXML + '    <campo etichetta="#dett" tabellare="0" valore="' + xValDett + '"/>' + chr(13);
                              QNoteSpeseFatt.Close;
                         end;

                         // quale modello
                         if TFattura.FieldByName('Tipo').asString = 'Nota di accredito' then
                              xFileModello := 'ModNotaAccr.doc'
                         else begin
                              if not CBNoteSpese.Checked then
                                   xFileModello := 'ModFattura.doc'
                              else
                                   xFileModello := 'ModFatturaNS.doc';
                         end;

                         xXML := TStringList.create;
                         xXML.Text := '<?xml version="1.0" encoding="ISO-8859-1"?> ' + chr(13) +
                              '<operazioni> ' + chr(13) +
                              '  <operazione nome="RiempiModelloWord" nomefile="' + copy(xFileModello, 1, pos('.', xFileModello) - 1) + '" fileword="' + ExtractFileName(xFileWord) + '"> ' + chr(13) +
                              xCampiXML +
                              '  </operazione> ' + chr(13) +
                              '</operazioni> ';

                         xXML.Text := StringReplace(xXML.Text, '&', '&amp;', [rfIgnoreCase, rfReplaceAll]);

                         // vai
                         RiempiModelloGenerico_remoto('Fattura', xFileModello, ExtractFileName(xFileWord), xXML.text);

                    end else begin

                         if data.global.fieldbyname('debughrms').asboolean = true then showmessage('Prima del trasferimento file sul client - file DB');
                         CopiaApriFileSulClient(xFileWord, true);
                         ShowMessage('Modificare il file sul client e poi premere OK');
                         CopiaRimuoviDalClient(xFileWord, GetDocPath + '\');
                         if data.global.fieldbyname('debughrms').asboolean = true then showmessage('Dopo il trasferimento file sul client - file DB');

                    end;

               end else begin

                    try
                         MsWord := CreateOleObject('Word.Basic');
                    except
                         ShowMessage('Non riesco ad aprire Microsoft Word.');
                         Exit;
                    end;

                    MsWord.AppShow;
                    if xApriModello then begin

                         if TFattura.FieldByName('Tipo').asString = 'Nota di accredito' then
                              MsWord.FileOpen(GetDocPath + '\ModNotaAccr.doc')
                         else begin
                              if not CBNoteSpese.Checked then
                                   MsWord.FileOpen(GetDocPath + '\ModFattura.doc')
                              else
                                   MsWord.FileOpen(GetDocPath + '\ModFatturaNS.doc');
                         end;

                         if CBFronte.checked then begin
                              // ATTENZIONE: il file deve avere la copertina
                              // >>>>> nascosto il checkbox, quindi codice mai eseguito
                              MsWord.EditFind('#Data');
                              MsWord.Insert(DateToStr(Date));
                              MsWord.EditFind('#NomeAzienda');
                              MsWord.Insert(Data2.TEBCclienti.FieldByName('Descrizione').AsString);
                              MsWord.EditFind('#IndirizzoAzienda');
                              MsWord.Insert(Data2.TEBCclienti.FieldByname('IndirizzoLegale').AsString);
                              MsWord.EditFind('#CapComuneProvAzienda');
                              MsWord.Insert(Data2.TEBCclienti.FieldByName('CapLegale').AsString + ' ' + Data2.TEBCclienti.FieldByName('ComuneLegale').AsString + ' (' + Data2.TEBCclienti.FieldByName('ProvinciaLegale').AsString + ')');
                         end;

                         // riempimento campi

                         //MsWord.EditFind('#Tipo');
                         //MsWord.Insert(TFattura.FieldByName('Tipo').AsString);
                         MsWord.EditFind('#Numero');
                         MsWord.Insert(TFattura.FieldByName('Progressivo').asString);
                         MsWord.EditFind('#DataFattura');
                         MsWord.Insert(TFattura.FieldByName('Data').asString);

                         // solo per nota di accredito
                         if TFattura.FieldByName('Tipo').asString = 'Nota di accredito' then begin
                              MsWord.EditFind('#RifProg');
                              MsWord.Insert(TFattura.FieldByName('RifProg').asString);

                              QTemp2.Close;
                              QTemp2.SQL.text := 'select convert(nvarchar,data,103) DataFatt from fatture where Progressivo = :x';
                              QTemp2.Parameters[0].value := TFattura.FieldByName('RifProg').asString;
                              QTemp2.Open;
                              if not QTemp2.isempty then begin
                                   MsWord.EditFind('#DataRifProg');
                                   MsWord.Insert(QTemp2.FieldByName('DataFatt').asString);
                              end else begin
                                   MsWord.EditFind('#DataRifProg');
                                   MsWord.Insert(' ');
                              end;
                              QTemp2.Close;

                         end;

                         if pos('ERREMME', Uppercase(Data.Global.FieldByName('NomeAzienda').AsString)) > 0 then begin
                              MsWord.EditFind('#Responsabile');
                              if (TFattura.FieldByName('Responsabile').asString = NULL) or (TFattura.FieldByName('Responsabile').asString = '') then
                                   MsWord.Insert('')
                              else
                                   MsWord.Insert(TFattura.FieldByName('Responsabile').asString);
                         end;
                         if TFatturaIDCliente.value = TFatturaIDFatturaA.Value then begin
                              MsWord.EditFind('#NomeAzienda');
                              MsWord.Insert(Data2.TEBCclienti.FieldByName('Descrizione').AsString);
                              if pos('ERREMME', Uppercase(Data.Global.FieldByName('NomeAzienda').AsString)) = 0 then begin
                                   MsWord.EditFind('#Responsabile');
                                   if (TFattura.FieldByName('Responsabile').asString = NULL) or (TFattura.FieldByName('Responsabile').asString = '') then
                                        MsWord.Insert('')
                                   else
                                        MsWord.Insert(' c.a. ' + TFattura.FieldByName('Responsabile').asString);
                              end;

                              MsWord.EditFind('#IndirizzoAzienda');
                              MsWord.Insert(Data2.TEBCClienti.FieldByName('TipoStradaLegale').AsString + ' ' + Data2.TEBCclienti.FieldByName('IndirizzoLegale').AsString + ' ' + Data2.TEBCClienti.FieldByName('NumCivicoLegale').AsString);
                              MsWord.EditFind('#CapComuneProvAzienda');
                              MsWord.Insert(Data2.TEBCclienti.FieldByName('CapLegale').AsString + ' ' + Data2.TEBCclienti.FieldByName('ComuneLegale').AsString + ' (' + Data2.TEBCclienti.FieldByName('ProvinciaLegale').AsString + ')');
                              MsWord.EditFind('#PartitaIVA');
                              MsWord.Insert(Data2.TEBCclienti.FieldByName('PartitaIVA').AsString + ' / ' + Data2.TEBCclienti.FieldByName('CodiceFiscale').AsString);
                         end else begin
                              MsWord.EditFind('#NomeAzienda');
                              MsWord.Insert(QPartnerLKPartner.AsString);
                              MsWord.EditFind('#IndirizzoAzienda');
                              MsWord.Insert(QPartnerLKTipoStradaLegale.AsString + ' ' + QPartnerLKIndirizzoLegale.AsString + ' ' + QPartnerLKNumCivicoLegale.AsString);
                              MsWord.EditFind('#CapComuneProvAzienda');
                              MsWord.Insert(QPartnerLKCapLegale.AsString + ' ' + QPartnerLKComuneLegale.AsString + ' (' + QPartnerLKProvinciaLegale.AsString + ')');
                              MsWord.EditFind('#PartitaIVA');
                              MsWord.Insert(QPartnerLKPartitaIVA.AsString + ' / ' + QPartnerLKCodiceFiscale.AsString);
                         end;

                         //MsWord.EditFind('#Ordine');
                         //if TFattura.FieldByName('RifProg').asString <> '' then
                         //     MsWord.Insert(TFattura.FieldByName('RifProg').asString)
                         //else MsWord.Insert('---');

                         //riempimento colonne dettagli
                         MsWord.EditFind('#dett');
                         TFattDett.First;
                         x := 0;
                         xTotIva := 0;
                         while x <= TFattDett.RecordCount do begin
                              // descrizione
                              if not TFattDett.EOF then
                                   MsWord.Insert(TFattDett.FieldByName('Descrizione').asString);

                              MsWord.NextCell;
                              if not TFattDett.EOF then
                                   MsWord.Insert(FormatFloat('###,###,###.00', TFattDett.FieldByName('Imponibile').Value));

                              MsWord.NextCell;
                              if not TFattDett.EOF then
                                   MsWord.Insert(TFattDett.FieldByName('PercentualeIVA').asString + ' %');

                              MsWord.NextCell;
                              if not TFattDett.EOF then
                                   MsWord.Insert(FormatFloat('###,###,###.00', TFattDett.FieldByName('Imponibile').Value + TFattDett.FieldByName('Imponibile').Value * TFattDett.FieldByName('PercentualeIVA').Value / 100));

                              xTotIVA := xTotIVA + (TFattDett.FieldByName('Imponibile').Value * TFattDett.FieldByName('PercentualeIVA').Value / 100);
                              //MsWord.Insert(FormatFloat('###,###,###.00', xTotIVA));

                              //MsWord.NextCell;

                              TFattDett.Next;
                              if not TFattDett.EOF then
                                   MsWord.NextCell;
                              x := x + 1;
                         end;

                        // showmessage('ddd=' +vartostr(xTotIVA));


                         MsWord.EditFind('#TotImponibile');
                         MsWord.Insert(FormatFloat('###,###,###.00', TFattura.FieldByName('Importo').Value));

                         MsWord.EditFind('#TotIva');
                         if TFattura.FieldByName('Totale').Value - TFattura.FieldByName('Importo').Value = 0 then
                              MsWord.Insert('0') //altrimenti verrebbe scritto ,00
                         else
                              MsWord.Insert(FormatFloat('###,###,###.00', TFattura.FieldByName('Totale').Value - TFattura.FieldByName('Importo').Value));

                         MsWord.EditFind('#TotFattura');
                         MsWord.Insert(FormatFloat('###,###,###.00', TFattura.FieldByName('Totale').Value));

                         if TFattura.FieldByName('Tipo').asString = 'Fattura' then begin

                              MsWord.EditFind('#ModalitaPagamento');
                              MsWord.Insert(TFattura.FieldByName('ModalitaPagamento').asString);
                              if Uppercase(copy(Data.Global.FieldByName('NomeAzienda').Value, 1, 6)) <> 'PROPOS' then begin
                                   //MsWord.EditFind('#Decorrenza');
                                   //MsWord.Insert(TFattura.FieldByName('Decorrenza').asString);
                                   MsWord.EditFind('#Scadenza');
                                   MsWord.Insert(TFattura.FieldByName('ScadenzaPagamento').asString);
                              end;
                              MsWord.EditFind('#AppoggioBancario');

                              //personalizzazione della manu
                              if Pos('mmartelli', mainform.xUtenteAttuale) > 0 then begin
                                   if copy(TFatturaModalitaPagamento.value, 1, 15) = 'Rimessa Diretta' then begin
                                        MsWord.Insert('NS. ISTITUTO DI CREDITO: ');
                                   end else begin
                                        if TFattura.FieldByname('AppoggioBAncario').asString <> '' then
                                             MsWord.Insert({'APPOGGIO BANCARIO: ' + }TFattura.FieldByName('AppoggioBAncario').asString)
                                        else MsWord.Insert('   ');
                                   end;

                              end else begin
                                   if TFattura.FieldByname('AppoggioBAncario').asString <> '' then
                                        MsWord.Insert('APPOGGIO BANCARIO: ' + TFattura.FieldByName('AppoggioBAncario').asString)
                                   else MsWord.Insert('   ');
                              end;

                              if TFattura.FieldByName('FlagFuoriAppIVA63372').Value <> NULL then begin
                                   if TFattura.FieldByName('FlagFuoriAppIVA63372').Value then begin
                                        MsWord.Insert(chr(13));
                                        MsWord.Insert(chr(13));
                                        MsWord.Insert(DBCBFlagIVA.Caption);
                                   end;
                              end;
                         end;

                         // allegato note spese
                         if CBNoteSpese.Checked then begin
                              MsWord.EditFind('#NumFattura');
                              MsWord.Insert(Tfattura.FieldByName('Progressivo').asString);
                              MsWord.EditFind('#DataFattura');
                              MsWord.Insert(TFattura.FieldByName('Data').asString);
                              MsWord.EditFind('#dett');
                              QNoteSpeseFatt.Open;
                              if QNoteSpeseFatt.RecordCount > 0 then begin
                                   while not QNoteSpeseFatt.EOF do begin
                                        QTemp.Close;
                                        if QNoteSpeseFatt.FieldByName('IDUtente').value <> 0 then begin
                                             QTemp.SQL.text := 'select Nominativo from Users where ID=' + QNoteSpeseFatt.FieldByName('IDUtente').asString;
                                             QTemp.Open;
                                             MsWord.Insert(QNoteSpeseFatt.FieldByName('Descrizione').asString + ': ' + QTemp.FieldByName('Nominativo').asString);
                                        end else begin
                                             QTemp.SQL.text := 'select Cognome,Nome from Anagrafica where ID=' + QNoteSpeseFatt.FieldByName('IDAnagrafica').asString;
                                             QTemp.Open;
                                             MsWord.Insert(QNoteSpeseFatt.FieldByName('Descrizione').asString + ': ' + QTemp.FieldByName('Cognome').asString + ' ' + QTemp.FieldByName('Nome').asString);
                                        end;
                                        MsWord.NextCell;
                                        MsWord.Insert(FormatFloat('###,###,###.##', QNoteSpeseFatt.FieldByName('Automobile').Value));
                                        MsWord.NextCell;
                                        MsWord.Insert(FormatFloat('###,###,###.##', QNoteSpeseFatt.FieldByName('Autostrada').Value));
                                        MsWord.NextCell;
                                        MsWord.Insert(FormatFloat('###,###,###.##', QNoteSpeseFatt.FieldByName('AereoTreno').Value));
                                        MsWord.NextCell;
                                        MsWord.Insert(FormatFloat('###,###,###.##', QNoteSpeseFatt.FieldByName('Taxi').Value));
                                        MsWord.NextCell;
                                        MsWord.Insert(FormatFloat('###,###,###.##', QNoteSpeseFatt.FieldByName('Albergo').Value));
                                        MsWord.NextCell;
                                        MsWord.Insert(FormatFloat('###,###,###.##', QNoteSpeseFatt.FieldByname('RistoranteBar').Value));
                                        MsWord.NextCell;
                                        MsWord.Insert(FormatFloat('###,###,###.##', QNoteSpeseFatt.FieldByname('Parking').Value));
                                        MsWord.NextCell;
                                        MsWord.Insert(QNoteSpeseFatt.FieldByName('AltroDesc').AsString + '  ');
                                        if QNoteSpeseFatt.FieldByName('AltroDesc').value <> '' then
                                             MsWord.Insert(FormatFloat('###,###,###.##', QNoteSpeseFatt.FieldByName('AltroImporto').Value));
                                        MsWord.NextCell;
                                        MsWord.Insert(FormatFloat('###,###,###.##', QNoteSpeseFatt.FieldByname('Totale').Value));

                                        QNoteSpeseFatt.Next;
                                        if not QNoteSpeseFatt.EOF then
                                             MsWord.NextCell;
                                   end;
                              end;
                              QNoteSpeseFatt.Close;
                         end;

                         MsWord.FileSaveAs(xFileWord);
                    end else MsWord.FileOpen(xFileWord);
               end;
          end;
     end;
     IO.RevokeFilter;
end;

procedure TFatturaForm.FormShow(Sender: TObject);
begin
     //grafica
     panel1.color := mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;
     panel2.color := mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;
     panel5.color := mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;
     panel7.color := mainform.AdvToolBarOfficeStyler1.PageAppearance.Color;


     QSedi.open;
     QValute.Open;
     TFattura.open;

     if qaccordifiliali.active = false then qaccordifiliali.open;
     QClientiFiliali.Open;
     xUltimoInserito := 0;
     if Data2.DsEBCclienti.state = dsEdit then Data2.TEBCclienti.Post;
     Caption := '[M/130] - ' + caption;
     if TFatturaIDAccordo.AsInteger > 0 then begin
          Label13.visible := True;
          Label14.Visible := true;
          Label13.Caption := '( ' + TFatturaAccordoPercOrigine.AsString + '%)';
          Label14.Caption := '( ' + TFatturaAccordoPercChiusura.AsString + '%)';
     end;

     if Pos('OPENJOB', UpperCase(Data.Global.FieldByName('NomeAzienda').AsString)) > 0 then
          DBComboBox3Change(self);

     if Pos('EBC', UpperCase(Data.Global.FieldByName('NomeAzienda').AsString)) > 0 then
     begin
          //DBComboBox3.Items.Clear;
          //DBComboBox3.Items.Add('Bonifico bancario - scadenza 30 gg fm');
          //DBComboBox3.Items.Add('Bonifico bancario - scadenza 60 gg fm');
          //DBComboBox3.Items.Add('Bonifico bancario - scadenza 90 gg fm');
          //DBComboBox3.Items.Add('come da offerta');
          //DBComboBox3.Items.Add('Ricevuta bancaria - scadenza 30 gg fm');
          //DBComboBox3.Items.Add('Ricevuta bancaria - scadenza 60 gg fm');
          //DBComboBox3.Items.Add('Ricevuta bancaria - scadenza 90 gg fm');
     end;

end;

procedure TFatturaForm.DBComboBox3Change(Sender: TObject);
var xDataScadenza: TDateTime;
     xAnno, xMese, xGiorno: Word;
begin
     if (copy(DBComboBox3.Text, 1, 3) = 'Bon') or (copy(DBComboBox3.Text, 1, 3) = 'Rim')
          and (DBComboBox1.text = 'Fattura') then begin
          // riempo la combo con le nostre banche da Global
          DBCBAppoggio.Text := '';
          QAppoggio.Open;
          DBCBAppoggio.Items.Clear;
          DBCBAppoggio.Items.Add(QAppoggioAppoggioBancario1.Value);
          if (QAppoggioAppoggioBancario2.Value <> '') and
               (QAppoggioAppoggioBancario2.Value <> NULL) then
               DBCBAppoggio.Items.Add(QAppoggio.FieldByName('AppoggioBancario2').Value);
          if (QAppoggioAppoggioBancario3.Value <> '') and
               (QAppoggioAppoggioBancario3.Value <> NULL) then
               DBCBAppoggio.Items.Add(QAppoggio.FieldByName('AppoggioBancario3').Value);
          QAppoggio.Close;
     end else begin
          DBCBAppoggio.Text := '';
          DBCBAppoggio.Items.Clear;
          DBCBAppoggio.Items.Add(Data2.TEBCclienti.FieldByname('BancaAppoggio').asString);
          DBCBAppoggio.ItemIndex := 0;
          DBCBAppoggio.Text := Data2.TEBCclienti.FieldByname('BancaAppoggio').asString;
          if not (DsFattura.state = dsEdit) then TFattura.Edit;
          TFatturaAppoggioBancario.value := Data2.TEBCclienti.FieldByname('BancaAppoggio').asString;
     end;

     // ricalcolo scadenza
     if MessageDlg('Vuoi ricalcolare in automatico la data di scadenza ?', mtWarning, [mbYes, mbNo], 0) = mrYes then begin
          xDataScadenza := StrToDate(DbDateEdit971.Text); // Date;

          if DBComboBox3.Text = 'Bonifico bancario - scadenza 30 gg fm' then begin
               xDataScadenza := xDataScadenza + 30;
               DecoDeDate(xDataScadenza, xAnno, xMese, xGiorno);
               // fine mese
               if xMese = 12 then xDataScadenza := EncodeDate(xAnno, 12, 31)
               else xDataScadenza := EncodeDate(xAnno, xMese + 1, 1) - 1;
          end;
          if DBComboBox3.Text = 'Bonifico bancario - scadenza 45 gg fm' then begin
               DecoDeDate(xDataScadenza, xAnno, xMese, xGiorno);
               // fine mese
               if xMese = 12 then xDataScadenza := EncodeDate(xAnno, 12, 31)
               else xDataScadenza := EncodeDate(xAnno, xMese + 1, 1) - 1;
          end;
          if DBComboBox3.Text = 'Bonifico bancario - scadenza 45 gg' then begin
               xDataScadenza := xDataScadenza + 45;
          end;
          if DBComboBox3.Text = 'Bonifico bancario - scadenza 60 gg fm' then begin
               xDataScadenza := xDataScadenza + 60;
               DecoDeDate(xDataScadenza, xAnno, xMese, xGiorno);
               // fine mese
               if xMese = 12 then xDataScadenza := EncodeDate(xAnno, 12, 31)
               else xDataScadenza := EncodeDate(xAnno, xMese + 1, 1) - 1;
          end;
          if DBComboBox3.Text = 'Bonifico bancario - scadenza 90 gg fm' then begin
               xDataScadenza := xDataScadenza + 90;
               DecoDeDate(xDataScadenza, xAnno, xMese, xGiorno);
               // fine mese
               if xMese = 12 then xDataScadenza := EncodeDate(xAnno, 12, 31)
               else xDataScadenza := EncodeDate(xAnno, xMese + 1, 1) - 1;
          end;
          if DBComboBox3.Text = 'Bonifico bancario - scadenza 120 gg fm' then begin
               xDataScadenza := xDataScadenza + 120;
               DecoDeDate(xDataScadenza, xAnno, xMese, xGiorno);
               // fine mese
               if xMese = 12 then xDataScadenza := EncodeDate(xAnno, 12, 31)
               else xDataScadenza := EncodeDate(xAnno, xMese + 1, 1) - 1;
          end;
          if DBComboBox3.Text = 'Bonifico bancario - scadenza 150 gg fm' then begin
               xDataScadenza := xDataScadenza + 150;
               DecoDeDate(xDataScadenza, xAnno, xMese, xGiorno);
               // fine mese
               if xMese = 12 then xDataScadenza := EncodeDate(xAnno, 12, 31)
               else xDataScadenza := EncodeDate(xAnno, xMese + 1, 1) - 1;
          end;

          if DBComboBox3.Text = 'Ricevuta bancaria - scadenza 30 gg fm' then begin
               xDataScadenza := xDataScadenza + 30;
               DecoDeDate(xDataScadenza, xAnno, xMese, xGiorno);
               // fine mese
               if xMese = 12 then xDataScadenza := EncodeDate(xAnno, 12, 31)
               else xDataScadenza := EncodeDate(xAnno, xMese + 1, 1) - 1;
          end;
          if DBComboBox3.Text = 'Ricevuta bancaria - scadenza 45 gg' then begin
               xDataScadenza := xDataScadenza + 45;
          end;
          if DBComboBox3.Text = 'Ricevuta bancaria - scadenza 45 gg fm' then begin
               xDataScadenza := xDataScadenza + 45;
               DecoDeDate(xDataScadenza, xAnno, xMese, xGiorno);
               // fine mese
               if xMese = 12 then xDataScadenza := EncodeDate(xAnno, 12, 31)
               else xDataScadenza := EncodeDate(xAnno, xMese + 1, 1) - 1;
          end;
          if DBComboBox3.Text = 'Ricevuta bancaria - scadenza 60 gg fm' then begin
               xDataScadenza := xDataScadenza + 60;
               DecoDeDate(xDataScadenza, xAnno, xMese, xGiorno);
               // fine mese
               if xMese = 12 then xDataScadenza := EncodeDate(xAnno, 12, 31)
               else xDataScadenza := EncodeDate(xAnno, xMese + 1, 1) - 1;
          end;
          if DBComboBox3.Text = 'Ricevuta bancaria - scadenza 90 gg fm' then begin
               xDataScadenza := xDataScadenza + 90;
               DecoDeDate(xDataScadenza, xAnno, xMese, xGiorno);
               // fine mese
               if xMese = 12 then xDataScadenza := EncodeDate(xAnno, 12, 31)
               else xDataScadenza := EncodeDate(xAnno, xMese + 1, 1) - 1;
          end;

          if DBComboBox3.Text = 'Rimessa Diretta - scadenza 30 gg.' then
               xDataScadenza := xDataScadenza + 30;
          if DBComboBox3.Text = 'Rimessa Diretta - scadenza 45 gg.' then
               xDataScadenza := xDataScadenza + 45;
          if DBComboBox3.Text = 'Rimessa Diretta - scadenza 60 gg.' then
               xDataScadenza := xDataScadenza + 60;
          if DBComboBox3.Text = 'Rimessa Diretta - scadenza 90 gg.' then
               xDataScadenza := xDataScadenza + 90;

          if DBComboBox3.Text = 'Ricevuta Bancaria - scadenza 30 gg. fm' then begin
               xDataScadenza := xDataScadenza + 30;
               DecoDeDate(xDataScadenza, xAnno, xMese, xGiorno);
               if xMese = 12 then xDataScadenza := EncodeDate(xAnno, 12, 31)
               else xDataScadenza := EncodeDate(xAnno, xMese + 1, 1) - 1;
          end;
          if DBComboBox3.Text = 'Ricevuta Bancaria - scadenza 60 gg. fm' then begin
               xDataScadenza := xDataScadenza + 60;
               DecoDeDate(xDataScadenza, xAnno, xMese, xGiorno);
               if xMese = 12 then xDataScadenza := EncodeDate(xAnno, 12, 31)
               else xDataScadenza := EncodeDate(xAnno, xMese + 1, 1) - 1;
          end;
          if DBComboBox3.Text = 'Ricevuta Bancaria - scadenza 90 gg. fm' then begin
               xDataScadenza := xDataScadenza + 90;
               DecoDeDate(xDataScadenza, xAnno, xMese, xGiorno);
               if xMese = 12 then xDataScadenza := EncodeDate(xAnno, 12, 31)
               else xDataScadenza := EncodeDate(xAnno, xMese + 1, 1) - 1;
          end;

          if DBComboBox3.Text = 'Rimessa Diretta - scadenza 30 gg. fm' then begin
               xDataScadenza := xDataScadenza + 30;
               DecoDeDate(xDataScadenza, xAnno, xMese, xGiorno);
               if xMese = 12 then xDataScadenza := EncodeDate(xAnno, 12, 31)
               else xDataScadenza := EncodeDate(xAnno, xMese + 1, 1) - 1;
          end;
          if DBComboBox3.Text = 'Rimessa Diretta - scadenza 60 gg. fm' then begin
               xDataScadenza := xDataScadenza + 60;
               DecoDeDate(xDataScadenza, xAnno, xMese, xGiorno);
               if xMese = 12 then xDataScadenza := EncodeDate(xAnno, 12, 31)
               else xDataScadenza := EncodeDate(xAnno, xMese + 1, 1) - 1;
          end;
          if DBComboBox3.Text = 'Rimessa Diretta - scadenza 90 gg. fm' then begin
               xDataScadenza := xDataScadenza + 90;
               DecoDeDate(xDataScadenza, xAnno, xMese, xGiorno);
               if xMese = 12 then xDataScadenza := EncodeDate(xAnno, 12, 31)
               else xDataScadenza := EncodeDate(xAnno, xMese + 1, 1) - 1;
          end;

          DbDateEdit972.Date := xDataScadenza;
          if not (DsFattura.state = dsEdit) then TFattura.Edit;
          TFatturaScadenzaPagamento.value := xDataScadenza;

     end;

end;

procedure TFatturaForm.QNoteSpeseFatt_OLDCalcFields(DataSet: TDataSet);
begin
     QNoteSpeseFatt.FieldByName('Totale').Value := QNoteSpeseFatt.FieldByName('Automobile').Value +
          QNoteSpeseFatt.FieldByName('Autostrada').Value +
          QNoteSpeseFatt.FieldByName('AereoTreno').Value +
          QNoteSpeseFatt.FieldByName('Taxi').Value +
          QNoteSpeseFatt.FieldByName('Albergo').Value +
          QNoteSpeseFatt.FieldByName('RistoranteBar').Value +
          QNoteSpeseFatt.FieldByName('Parking').Value +
          QNoteSpeseFatt.FieldByName('AltroImporto').Value;
end;

procedure TFatturaForm.TbBFattDettModClick(Sender: TObject);
var tImporto, tTotal: double;
     xIDFattura: integer;
begin
     if TFattDett.IsEmpty then exit;
     ModFattForm := TModFattForm.create(self);
     ModFattForm.EDescrizione.text := TFattDett.FieldByName('Descrizione').Value;
     ModFattForm.RxImponibile.Value := TFattDett.FieldByName('Imponibile').Value;
     //ModFattForm.SEPercIVA.Value:=TFattDett.FieldByName('PercentualeIVA').AsInteger;
     ModFattForm.SEPercIVA.Text := TFattDettPercentualeIVA.AsString;
     {     ModFattForm.EDescrizione.text:=TFattDettDescrizione.Value;
          ModFattForm.RxImponibile.Value:=TFattDettImponibile.Value;
          ModFattForm.SEPercIVA.Value:=TFattDettPercentualeIVA.AsInteger;}
     ModFattForm.ShowModal;
     if ModFattForm.ModalResult = mrOK then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    {                    Q1.SQL.text:='update FattDett set Descrizione=:xDescrizione,Imponibile=:xImponibile,PercentualeIVA=:xPercentualeIVA '+
                                             'where ID='+TFattDettID.asString;
                                        Q1.ParamByName('xDescrizione').asString:=ModFattForm.EDescrizione.text;
                                        Q1.ParamByName('xImponibile').asFloat:=ModFattForm.RxImponibile.Value;
                                        Q1.ParamByName('xPercentualeIVA').asInteger:=ModFattForm.SEPercIVA.Value;}
                    Q1.SQL.text := 'update FattDett set Descrizione=:xDescrizione:,Imponibile=:xImponibile:,PercentualeIVA=:xPercentualeIVA: ' +
                         'where ID=' + TFattDett.FieldByName('ID').asString;
                    Q1.ParamByName['xDescrizione'] := ModFattForm.EDescrizione.text;
                    Q1.ParamByName['xImponibile'] := ModFattForm.RxImponibile.Value;
                    Q1.ParamByName['xPercentualeIVA'] := StrtoFloat(ModFattForm.SEPercIVA.Text);
                    Q1.ExecSQL;
                    // ricalcolo imponibile fattura
                    TFattDett.DisableControls;
                    TFattDett.Close;
                    TFattDett.Open;
                    tImporto := 0;
                    tTotal := 0;
                    while not TFattDett.EOF do begin
                         {                         tImporto:=tImporto+TFattDettImponibile.Value;
                                                  tTotal:=tTotal+TFattDettTotale.Value;}
                         tImporto := tImporto + TFattDett.FieldByname('Imponibile').Value;
                         tTotal := tTotal + TFattDett.FieldByName('Totale').Value;
                         TFattDett.Next;
                    end;
                    {                    Q1.SQL.text:='update Fatture set Importo=:xImporto,Totale=:xTotale '+
                                             'where ID='+TFatturaID.asString;
                                        Q1.ParamByName('xImporto').asFloat:=tImporto;
                                        Q1.ParamByName('xTotale').asFloat:=tTotal;}
                    Q1.SQL.text := 'update Fatture set Importo=:xImporto:,Totale=:xTotale: ' +
                         'where ID=' + TFattura.FieldByname('ID').asString;
                    Q1.ParamByName['xImporto'] := tImporto;
                    Q1.ParamByName['xTotale'] := tTotal;
                    Q1.ExecSQL;
                    TFattDett.EnableControls;

                    DB.CommitTrans;
                    // refresh fattura e dettaglio
                    TFattDett.Close;
                    xIDfattura := TFattura.FieldByname('ID').AsINteger;
                    TFattura.Close;
                    //                    TFattura.ParamByName('xID').asInteger:=xIDfattura;
                    TFattura.ParamByName['xID'] := xIDfattura;
                    TFattura.open;
                    TFattDett.Open;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                    raise;
               end;
          end;

     end;
     ModFattForm.Free;
end;

procedure TFatturaForm.ToolbarButton971Click(Sender: TObject);
var tImporto, tTotal: double;
     xIDfattura: integer;
begin
     ModFattForm := TModFattForm.create(self);
     ModFattForm.Caption := 'Inserimento nuovo dettaglio fattura';

     if DBCBFlagIVA.Checked = true then
          ModFattForm.SEPercIVA.Text := '0'
     else
          ModFattForm.SEPercIVA.Text := data.Global.fieldbyname('Iva').asString;

     ModFattForm.ShowModal;
     if ModFattForm.ModalResult = mrOK then begin
          with Data do begin
               DB.BeginTrans;
               try
                    Q1.Close;
                    Q1.SQL.text := 'insert into FattDett (IDFattura,Descrizione,Imponibile,PercentualeIVA) ' +
                         'values (:xIDFattura:,:xDescrizione:,:xImponibile:,:xPercentualeIVA:)';
                    Q1.ParamByName['xIDFattura'] := TFattura.FieldByName('ID').Value;
                    Q1.ParamByName['xDescrizione'] := ModFattForm.EDescrizione.text;
                    Q1.ParamByName['xImponibile'] := ModFattForm.RxImponibile.Value;

                    if DBCBFlagIVA.Checked = true then
                         Q1.ParamByName['xPercentualeIVA'] := 0
                    else
                         Q1.ParamByName['xPercentualeIVA'] := StrToFloat(ModFattForm.SEPercIVA.Text);

                    Q1.ExecSQL;
                    // ricalcolo imponibile fattura
                    TFattDett.DisableControls;
                    TFattDett.Close;
                    TFattDett.Open;
                    tImporto := 0;
                    tTotal := 0;
                    while not TFattDett.EOF do begin
                         tImporto := tImporto + TFattDett.FieldByName('Imponibile').Value;
                         tTotal := tTotal + TFattDett.FieldByName('Totale').Value;
                         TFattDett.Next;
                    end;
                    Q1.SQL.text := 'update Fatture set Importo=:xImporto:,Totale=:xTotale: ' +
                         'where ID=' + TFattura.FieldByName('ID').asString;
                    Q1.ParamByName['xImporto'] := tImporto;
                    Q1.ParamByName['xTotale'] := tTotal;
                    Q1.ExecSQL;
                    TFattDett.EnableControls;

                    DB.CommitTrans;
                    // refresh fattura e dettaglio
                    TFattDett.Close;
                    xIDfattura := TFattura.FieldByName('ID').Asinteger;
                    TFattura.Close;
                    TFattura.ReloadSQL;
                    TFattura.ParamByName['xID'] := xIDfattura;
                    TFattura.open;
                    TFattDett.Open;
               except
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                    raise;
               end;
          end;

     end;
     ModFattForm.Free;
end;

procedure TFatturaForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     // SE C'E' il CONTROLLO DI GESTIONE:
     // per ogni dettaglio di fattura legato a compenso
     // ==> aggiorna il valore A CONSUNTIVO in H1SelCG
//     if copy(Data.GlobalCheckBoxIndici.Value,6,1)='1' then begin
     if copy(Data.Global.FieldByName('CheckBoxIndici').Value, 6, 1) = '1' then begin
          TFattDett.First;
          while not TFattDett.EOF do begin
               //               if (TFattDettIDCompenso.AsString<>'')and(TFattDettIDCompenso.Value>0) then begin
               if (TFattDett.FieldByName('IDCompenso').AsString <> '') and (TFattDett.FieldByName('IDCompenso').Value > 0) then begin
                    with Data do begin
                         DB.BeginTrans;
                         try
                              // compensi ricerche
{                              Q1.SQL.text:='update CG_ContrattiRicavi set Importo=:xImporto '+
                                   'where IDCompenso=:xIDCompenso';
                              Q1.ParambyName('xImporto').asFloat:=TFattDettImponibile.Value;
                              Q1.ParambyName('xIDCompenso').asInteger:=TFattDettIDCompenso.Value;
                              Q1.ExecSQL;}
                              Q1.SQL.text := 'update CG_ContrattiRicavi set Importo=:xImporto: ' +
                                   'where IDCompenso=:xIDCompenso:';
                              Q1.ParambyName['xImporto'] := TFattDett.FieldByName('Imponibile').Value;
                              Q1.ParambyName['xIDCompenso'] := TFattDett.FieldByName('IDCompenso').Value;
                              Q1.ExecSQL;
                              // annunci
                              {Q1.SQL.text:='update CG_ContrattiRicavi set Importo=:xImporto '+
                                   'where IDCompenso=:xIDCompenso';
                              Q1.ParambyName('xImporto').asFloat:=TFattDettImponibile.Value;
                              Q1.ParambyName('xIDCompenso').asInteger:=TFattDettIDCompenso.Value;
                              Q1.ExecSQL;}

                              DB.CommitTrans;
                         except
                              DB.RollbackTrans;
                              MessageDlg('ERRORE SUL DATABASE: operazione non effettuata', mtError, [mbOK], 0);
                         end;
                    end;
               end;
               TFattDett.Next;
          end;
     end;
end;

procedure TFatturaForm.SpeedButton1Click(Sender: TObject);
begin
     ApriHelp('130');
end;

procedure TFatturaForm.FormCreate(Sender: TObject);
begin
     if Pos('OPENJOB', UpperCase(Data.Global.FieldByName('NomeAzienda').AsString)) > 0 then
     begin
          DBComboBox3.Items.Clear;
          DBComboBox3.Items.Add('Bonifico bancario scadenza 30 gg fm');
          DBComboBox3.Items.Add('Bonifico bancario scadenza 45 gg');
          DBComboBox3.Items.Add('Bonifico bancario scadenza 60 gg fm');
          DBComboBox3.Items.Add('Bonifico bancario scadenza 90 gg fm');
          DBComboBox3.Items.Add('Bonifico bancario scadenza 120 gg fm');
          DBComboBox3.Items.Add('Bonifico bancario scadenza 150 gg fm');
          DBComboBox3.Items.Add('Openfido');
          DBComboBox3.Items.Add('Ricevuta bancaria scadenza 30 gg fm');
          DBComboBox3.Items.Add('Ricevuta bancaria scadenza 60 gg fm');
          DBComboBox3.Items.Add('Ricevuta bancaria scadenza 90 gg fm');
          DBComboBox3.Items.Add('Rimessa diretta');
     end;
end;

procedure TFatturaForm.SpeedButton50Click(Sender: TObject);
begin
     if TFattura.State = dsBrowse then tfattura.Edit;
     TFatturaIDAccordo.Value := 0;
     TFattura.Post;
end;

procedure TFatturaForm.TFatturaAfterPost(DataSet: TDataSet);
begin
     if TFatturaIDAccordo.AsInteger > 0 then begin
          Label13.visible := True;
          Label14.Visible := true;
          Label13.Caption := '(' + TFatturaAccordoPercOrigine.AsString + '%)';
          Label14.Caption := '(' + TFatturaAccordoPercChiusura.AsString + '%)';
     end else begin
          Label13.visible := False;
          Label14.Visible := false;
     end;


end;

procedure TFatturaForm.dxDBLookupEdit1Change(Sender: TObject);
begin
     TFattura.Post;
end;

procedure TFatturaForm.TFatturaAfterOpen(DataSet: TDataSet);
begin
     //showmessage(TFattura.FieldByName('IDCliente').asstring);
     QPartnerLK.close;
     QPartnerLK.Parameters.parambyname('x').value := TFattura.FieldByName('IDCliente').AsInteger;
     QPartnerLK.Open;
     //  showmessage(inttostr(QPartnerLK.RecordCount));
end;

procedure TFatturaForm.TFattDettCalcFields(DataSet: TDataSet);
begin
     //TFattDettTotale.Value := TFattDettImponibile.Value + TFattDettImponibile.Value * TFattDettPercentualeIVA.Value / 100;

     if TFattura.FieldByName('SplitPayment').asboolean = false then //se splitpayment non deve calcolare l'iva
          TFattDettTotale.Value := TFattDettImponibile.Value * ((100 + TFattDettPercentualeIVA.Value) / 100)
     else
          TFattDettTotale.Value := TFattDettImponibile.Value;

end;

procedure TFatturaForm.TFattDettBeforeOpen(DataSet: TDataSet);
begin
     TFattDett.Parameters.ParamByName('IDFattura').value := TFatturaid.value;
end;

procedure TFatturaForm.DBCBFlagIVAClick(Sender: TObject);
var xvalIva, tImporto, tTotal: double;
begin
     if TFattDett.Active then begin

          if DBCBFlagIVA.Checked = true then
               xvalIva := 0
          else
               xvalIva := data.Global.fieldbyname('iva').value;

          tImporto := 0;
          tTotal := 0;

          TFattDett.First;
          while not TFattDett.Eof do begin
               TFattDett.Edit;
               TFattDettPercentualeIVA.Value := xvalIva;

               tImporto := tImporto + TFattDett.FieldByname('Imponibile').Value;
               tTotal := tTotal + TFattDett.FieldByName('Totale').Value;

               TFattDett.next;
          end;

          TFattura.edit;
          TFatturaImporto.Value := tImporto;
          TFatturaTotale.Value := tTotal;

     end;
end;

procedure TFatturaForm.DBCheckBox1Click(Sender: TObject);
begin
     if DsFattura.state in [dsInsert, dsEdit] then
          TFatturaFlagFuoriAppIVA63372.Value := DBCheckBox1.Checked;
end;

procedure TFatturaForm.QNoteSpeseFattCalcFields(DataSet: TDataSet);
begin
     QNoteSpeseFattTotale.Value := QNoteSpeseFattAutomobile.Value +
          QNoteSpeseFattAutostrada.Value +
          QNoteSpeseFattAereoTreno.Value +
          QNoteSpeseFattTaxi.Value +
          QNoteSpeseFattAlbergo.Value +
          QNoteSpeseFattRistoranteBar.Value +
          QNoteSpeseFattParking.Value +
          QNoteSpeseFattAltroImporto.Value;
end;

procedure TFatturaForm.bTabAttivitaFutureClick(Sender: TObject);
begin
OpenTab('TabValute', ['ID', 'Valuta', 'Simbolo','Diminutivo','Carattere'], ['Valuta', 'Simbolo','Diminutivo','Carattere']);
     QValute.Close;
     QValute.Open;
end;

end.

