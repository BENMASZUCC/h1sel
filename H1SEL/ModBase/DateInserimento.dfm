object DateInserimentoForm: TDateInserimentoForm
  Left = 301
  Top = 206
  BorderStyle = bsDialog
  Caption = 'Inserimento in azienda:  date di riferimento'
  ClientHeight = 75
  ClientWidth = 394
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 5
    Top = 7
    Width = 179
    Height = 13
    Caption = 'Data (presunta) inizio reale in azienda:'
  end
  object Label2: TLabel
    Left = 5
    Top = 31
    Width = 183
    Height = 13
    Caption = 'Data di controllo del candidato inserito:'
  end
  object BitBtn1: TBitBtn
    Left = 297
    Top = 3
    Width = 94
    Height = 34
    TabOrder = 1
    OnClick = BitBtn1Click
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 297
    Top = 38
    Width = 94
    Height = 34
    Caption = 'Annulla'
    TabOrder = 2
    Kind = bkCancel
  end
  object CBProm: TCheckBox
    Left = 5
    Top = 53
    Width = 257
    Height = 17
    Caption = 'messaggio in promemoria (alla data di controllo)'
    Checked = True
    State = cbChecked
    TabOrder = 0
  end
  object DEDataInizio: TdxDateEdit
    Left = 195
    Top = 3
    Width = 97
    TabOrder = 3
    Date = -700000
  end
  object DEDataControllo: TdxDateEdit
    Left = 195
    Top = 27
    Width = 97
    TabOrder = 4
    Date = -700000
  end
end
