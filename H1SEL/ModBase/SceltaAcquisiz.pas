unit SceltaAcquisiz;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, ExtCtrls, Buttons;

type
     TSceltaAcquisizForm = class(TForm)
          BitBtn1: TBitBtn;
          BitBtn2: TBitBtn;
          RadioGroup1: TRadioGroup;
          procedure FormShow(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     SceltaAcquisizForm: TSceltaAcquisizForm;

implementation

{$R *.DFM}

procedure TSceltaAcquisizForm.FormShow(Sender: TObject);
begin
     Caption := '[S/32] - ' + Caption;
end;

end.
