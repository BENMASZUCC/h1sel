unit GoogleApi;

interface
uses MidItems, ActiveX, MSHTML, Math, shdocvw, IdSSLOpenSSL, AdoDB, Main, CaricaPriimiProgress,
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, uutilsvarie, MSXML_TLB, IdHTTP;


function ExecuteJavaScript(WebBrowser: TWebBrowser; Code: string): variant;
function GetElementIdValue(WebBrowser: TWebBrowser; TagName, TagId, TagAttrib: string): string;
procedure CalcolaDistanceDurata(xPartenza: string; xDestinazione: string; xMezzo: string; var xDistance: string;
     var xDuration: string; var xStartLatitudine: string; var xStartLongitudine: string;
     var xEndLatitudine: string; var xEndLongitudine: string; var xstatus: string);
procedure DistanceMatrix(xIDUtente, xIDRicerca: integer; xDestinazione: string);

//procedure Delay(Millisec: Longint);

implementation

uses ModuloDati;

function GetElementIdValue(WebBrowser: TWebBrowser; TagName, TagId, TagAttrib: string): string;
var
     Document: IHTMLDocument2;
     Body: IHTMLElement2;
     Tags: IHTMLElementCollection;
     Tag: IHTMLElement;
     I: Integer;
begin
     Result := '';
     if not Supports(WebBrowser.Document, IHTMLDocument2, Document) then
          raise Exception.Create('Invalid HTML document');
     if not Supports(Document.body, IHTMLElement2, Body) then
          raise Exception.Create('Can''t find <body> element');
     Tags := Body.getElementsByTagName(UpperCase(TagName));
     for I := 0 to (Tags.length) - 1 do begin
          Tag := Tags.item(I, EmptyParam) as IHTMLElement;
          if Tag.id = TagId then begin
               Result := Tag.getAttribute(TagAttrib, 0);
          end;
     end;
end;

function ExecuteJavaScript(WebBrowser: TWebBrowser; Code: string): variant;
var
     Document: IHTMLDocument2;
     Window: IHTMLWindow2;
     risultato, km, tempo, a: string;
begin
     //code := StringReplace(code, '''', '\', [rfReplaceAll]);
  // execute javascript in webbrowser
     Document := WebBrowser.Document as IHTMLDocument2;
     if not Assigned(Document) then Exit;
     Window := Document.parentWindow;
     if not Assigned(Window) then Exit;
     try begin
               Result := Window.execScript(Code, 'JavaScript');
               //SelPersNewForm.Memo1.Lines.Add('1');
            //   SelPersNewForm.Timer1.Enabled := true;
                 {  repeat //Sleep(1000);
       risultato:=getElementIdValue(form1.WebBrowser1, 'input', 'result', 'value') ;
      until risultato <>'' ;   }

              // SelPersNewForm.Timer1.Enabled := true;
     // If Result=Unassigned then showmessage('unassigned') else showmessage('esguito');
          end
     except
          on E: Exception do raise Exception.Create('Javascript error ' + E.Message + ' in: '#13#10 + Code);
     end;
end;


{procedure Delay(Millisec: Longint);
var
     Conta: DWord;
begin
     Conta := GetTickCount + DWord(Millisec);
     repeat
          Application.ProcessMessages;
     until (GetTickCount > Conta);
end;  }


procedure CalcolaDistanceDurata(xPartenza: string; xDestinazione: string; xMezzo: string; var xDistance: string;
     var xDuration: string; var xStartLatitudine: string; var xStartLongitudine: string;
     var xEndLatitudine: string; var xEndLongitudine: string; var xstatus: string);
var
     xfile, xLog: TStringList;
     XMLDoc: IXMLDOMDocument;
     ANode: IXMLDOMNode;
     HttpText: TIdHTTP;
     LHandler: TIdSSLIOHandlerSocket;
     d, m, y: Word;
     request: string;
     q: TADOQuery;
begin
     xfile := TStringList.Create;
     xLog := TStringList.Create;
     HttpText := TIdHTTP.Create(nil);


     q := Tadoquery.Create(nil);
     q.Connection := Data.DB;

     q.SQL.text := 'select IDKey from FormWeb_social where social = ''Google Distance'' ';
     q.Open;

     if q.RecordCount > 0 then begin

          request := '';
          request := 'https://maps.googleapis.com/maps/api/directions/' +
               'xml?origin=' + xPartenza + '&&destination=' + xDestinazione +
               '&&mode=' + xMezzo + '&&sensor=false' + '&&key=' + q.Fields[0].AsString;
          request := StringReplace(request, ' ', '%20',
               [rfReplaceAll, rfIgnoreCase]); ;
          if Data.Global.FieldByName('DebugHRMS').AsBoolean then begin
               if FileExists('LogGoogle.txt') then xLog.LoadFromFile('LogGoogle.txt');
               xLog.Add('-------------------------------------------------------------------------------------------------------');
          end;
          q.free;

          try
               LHandler := TIdSSLIOHandlerSocket.Create(nil);
               lhandler.SSLOptions.Mode := sslmClient;
               lhandler.SSLOptions.Method := sslvTLSv1;

               HttpText.IOHandler := LHandler;

               xfile.text := HttpText.Get(request);

               if Data.Global.FieldByName('DebugHRMS').AsBoolean then begin
          //showmessage(request);
                    xLog.Add('Request: ' + request);
                    xLog.Add('Partenza: ' + xPartenza);
                    xLog.Add('Destinazione: ' + xDestinazione);
                    xLog.Add('Mezzo: ' + xMezzo);
                    xLog.Add('Response: ' + xfile.Text);
                    xLog.Add('-------------------------------------------------------------------------------------------------------');
                    xLog.SaveToFile(data.Global.fieldbyname('DirFileLog').asstring + '\LogGoogle.txt');
               end;
               xLog.Free;
          //modifica per poterla utilizzare in Delphi 7
          //xfile.Strings[0]:='<?xml version="1.0" Standalone="yes"?>';
          //xfile.SaveToFile('filetesting.xml');
               XMLDoc := CoDOMDocument.create;
               if XMLDoc.loadXML(WideString(xfile.text)) then begin
                    ANode := XMLDoc.documentElement.selectSingleNode('/DirectionsResponse/status');
                    if Pos('OK', UpperCase(ANode.text)) = 0 then begin
               //MessageDlg('Attenzione, Impossibile Calcolare la Distanza' + chr(13) + ' -- Indirizzi Invalidi -- ', MTWarning, [mbOk], 0);
                         xstatus := 'Calcolo Non Avvenuto';
                         xfile.free;
                         HttpText.free;
                         exit;
                    end;
                    ANode := XMLDoc.documentElement.selectSingleNode('/DirectionsResponse/route/leg/duration/text');
                    xDuration := ANode.text;
                    ANode := XMLDoc.documentElement.selectSingleNode('/DirectionsResponse/route/leg/distance/text');
                    xDistance := ANode.text;
                    ANode := XMLDoc.documentElement.selectSingleNode('/DirectionsResponse/route/leg/start_location/lat');
                    xStartLatitudine := ANode.text;
                    ANode := XMLDoc.documentElement.selectSingleNode('/DirectionsResponse/route/leg/start_location/lng');
                    xStartLongitudine := ANode.text;
                    ANode := XMLDoc.documentElement.selectSingleNode('/DirectionsResponse/route/leg/end_location/lat');
                    xEndLatitudine := ANode.text;
                    ANode := XMLDoc.documentElement.selectSingleNode('/DirectionsResponse/route/leg/end_location/lng');
                    xEndLongitudine := ANode.text;
                    xstatus := 'Calcolo Avvenuto'
               end;
          // DeleteFile('filetesting.xml');
               xfile.free;
               lhandler.free;
               HttpText.free;
          except
               on E: Exception do begin

                    xfile.add(E.Message);
                    if Data.Global.FieldByName('DebugHRMS').AsBoolean then begin
                         xLog.add(E.Message);
                         xLog.Add('-------------------------------------------------------------------------------------------------------');
                         xLog.SaveToFile(data.Global.fieldbyname('DirFileLog').asstring + '\LogGoogle.txt');
                    end;
                    xLog.Free;
          //ShowMessage(E);
          //showmessage(xPartenza);
          //showmessage(xDestinazione);
                    DecodeDate(Date, y, m, d);
                    xfile.SaveToFile(data.Global.fieldbyname('DirFileLog').asString +
                         '\CalcolaDistanceDurata_' + vartostr(d) + '_' + vartostr(m) +
                         '_' + vartostr(y) + '.xml');

                    xfile.free;
                    HttpText.free;
               end;

          end;
     end else
          MessageDlg('Impossibile proseguire: Chiave "Google Distance" mancante"', mtError, [mbOk], 0);

end;



procedure DistanceMatrix(xIDUtente, xIDRicerca: integer; xDestinazione: string);

var
     xfile, xLog: TStringList;
     XMLDoc: IXMLDOMDocument;
     ANode, ANodePartenza, ANodeDati: IXMLDOMNode;
     HttpText: TIdHTTP;
     LHandler: TIdSSLIOHandlerSocket;
     d, m, y: Word;
     request: Ansistring;
     q, qIns: TADOQuery;
     i: integer;
     xDistance, xDuration: string;
     Comando: WideString;
     xChiave, xStatus: string;
     xCont, xContGiri: integer;
     numdistanze, numrigexml: integer;
     xPartenza: string;
begin
     xfile := TStringList.Create;
     xLog := TStringList.Create;
     HttpText := TIdHTTP.Create(nil);


     q := Tadoquery.Create(nil);
     q.Connection := Data.DB;

     qIns := Tadoquery.Create(nil);
     qIns.Connection := Data.DB;

     q.SQL.text := 'select IDKey from FormWeb_social where social = ''Google Distance'' ';
     q.Open;

     if q.RecordCount > 0 then begin



          //modifica per poterla utilizzare in Delphi 7
          //xfile.Strings[0]:='<?xml version="1.0" Standalone="yes"?>';
          //xfile.SaveToFile('filetesting.xml');
          xChiave := q.fields[0].AsString;



          LHandler := TIdSSLIOHandlerSocket.Create(nil);
          lhandler.SSLOptions.Mode := sslmClient;
          lhandler.SSLOptions.Method := sslvTLSv1;

          HttpText.IOHandler := LHandler;

          if Data.Global.fieldbyname('debughrms').AsBoolean then begin

               xlog := TStringList.Create;
               if FileExists('testmatrix_comando.txt') then
                    xlog.LoadFromFile('testmatrix_comando.txt');

          end;

          q.Close;
          q.SQL.Text := 'select id,idanagrafica,IDutente,IDRicerca,Prog,Partenza ' +
               'from googledistanzaric where idUtente=:xIDUtente ';
          if xIDRicerca > 0 then
               q.SQL.Add('and IDRicerca = :xIDRicerca')
          else
               q.SQL.add('and IDRIcerca is NULL');
          q.Parameters.paramByName('xIDUtente').Value := xidutente;

          if xIDRicerca > 0 then
               q.Parameters.ParamByName('xIDRicerca').Value := xIDRicerca;

          q.SQL.add('order by Prog');
          q.Open;

          CaricaPrimiProgressForm := TCaricaPrimiProgressForm.create(nil);
          CaricaPrimiProgressForm.Show;
          CaricaPrimiProgressForm.Caption := 'Calcolo della distanza per le Anagrafiche Ricercate';
          CaricaPrimiProgressForm.ProgressBar1.Min := 0;
          CaricaPrimiProgressForm.ProgressBar1.Position := 0;
          CaricaPrimiProgressForm.Update;
          CaricaPrimiProgressForm.ProgressBar1.Max := q.RecordCount;


          Comando := 'https://maps.googleapis.com/maps/api/distancematrix/xml?origins=';
          xcont := 0;
          xContGiri := 0;



          xDestinazione := StringReplace(xDestinazione, ' ', '%20', [rfReplaceAll]);
          xDestinazione := StringReplace(xDestinazione, '�', '', [rfReplaceAll]);
          xDestinazione := StringReplace(xDestinazione, '�', 'a', [rfReplaceAll]);
          xDestinazione := StringReplace(xDestinazione, '�', 'e', [rfReplaceAll]);
          xDestinazione := StringReplace(xDestinazione, '�', 'e', [rfReplaceAll]);
          xDestinazione := StringReplace(xDestinazione, '�', 'i', [rfReplaceAll]);
          xDestinazione := StringReplace(xDestinazione, '�', 'o', [rfReplaceAll]);
          xDestinazione := StringReplace(xDestinazione, '�', 'u', [rfReplaceAll]);
          xDestinazione := StringReplace(xDestinazione, '�', '''', [rfReplaceAll]);


          while not q.Eof do begin

               xpartenza := q.Fieldbyname('Partenza').Value;
               xPartenza := StringReplace(xPartenza, ' ', '%20', [rfReplaceAll]);
               xPartenza := StringReplace(xPartenza, '�', '', [rfReplaceAll]);
               xPartenza := StringReplace(xPartenza, '�', 'a', [rfReplaceAll]);
               xPartenza := StringReplace(xPartenza, '�', 'e', [rfReplaceAll]);
               xPartenza := StringReplace(xPartenza, '�', 'e', [rfReplaceAll]);
               xPartenza := StringReplace(xPartenza, '�', 'i', [rfReplaceAll]);
               xPartenza := StringReplace(xPartenza, '�', 'o', [rfReplaceAll]);
               xPartenza := StringReplace(xPartenza, '�', 'u', [rfReplaceAll]);
               xPartenza := StringReplace(xPartenza, '�', '''', [rfReplaceAll]);
               Comando := Comando + xPartenza + '|';
               inc(xcont);





               if (xCont = 100) or (q.RecordCount = q.recno) then begin



                    Comando := Comando + '&destinations=' + xDestinazione + '&mode=Driving&&sensor=false&language=it.IT&key=' + xChiave;
                    if Data.Global.fieldbyname('debughrms').AsBoolean then begin


                         xlog.Add(Comando);

                         xlog.SaveToFile('testmatrix_comando.txt');

                    end;
                    try
                         xfile.text := HttpText.Get(Comando);
                         XMLDoc := CoDOMDocument.create;
                         if XMLDoc.loadXML(WideString(xfile.text)) then begin
                              if Data.Global.fieldbyname('debughrms').AsBoolean then
                                   xfile.SaveToFile('testmatrix.xml');
                              ANode := XMLDoc.documentElement.selectSingleNode('/DistanceMatrixResponse/status');
                              if Pos('OK', UpperCase(ANode.text)) > 0 then begin

                              {xstatus := 'Calcolo Non Avvenuto';
                              xfile.free;

                              exit;    }


                                   ANode := XMLDoc.documentElement.selectSingleNode('/DistanceMatrixResponse');


                                   numrigexml := ANode.childNodes.Get_length;
                                   numdistanze := ((ANode.childNodes.Get_length - 2) div 2) + 1;

                        // showmessage(inttostr(numrigexml));
                        // showmessage(inttostr(numdistanze));



                                   for i := 1 to numdistanze - 1 do begin

                                        AnodePartenza := XMLDoc.documentElement.selectSingleNode('/DistanceMatrixResponse').childNodes[i];
                              //showmessage(ANodePartenza.text);

                                        ANodeDati := XMLDoc.documentElement.selectSingleNode('/DistanceMatrixResponse').childNodes[i + numdistanze];
                              //showmessage(ANodeDati.text);

                                        if AnodeDati.childNodes[0].childNodes[0].text = 'OK' then begin


                                             qins.close;
                                             qins.sql.text := 'update googledistanzaric set TempoNum=:xTemponum,Tempo=:xTempo,DistanzaNum=:xDistanzaNum,Distanza=:xDistanza ' +
                                                  'where idutente = :xIDUtente and Prog = :xProg ';
                                             if xIDRicerca > 0 then
                                                  qins.sql.add('and IDRicerca=:xidricerca');
                                             qIns.Parameters.ParamByName('xTemponum').Value := AnodeDati.childNodes[0].childNodes[1].childNodes[0].text;
                                             qIns.Parameters.ParamByName('xTempo').Value := AnodeDati.childNodes[0].childNodes[1].childNodes[1].text;
                                             qIns.Parameters.ParamByName('xDistanzaNum').Value := AnodeDati.childNodes[0].childNodes[2].childNodes[0].text;
                                             qIns.Parameters.ParamByName('xDistanza').Value := strtoint(AnodeDati.childNodes[0].childNodes[2].childNodes[0].text) / 1000; //AnodeDati.childNodes[0].childNodes[2].childNodes[1].text;
                                             qIns.Parameters.ParamByName('xIDUtente').Value := xIDUtente;
                                             qIns.Parameters.ParamByName('xProg').Value := i + xContGiri;
                                             if xIDRicerca > 0 then
                                                  qIns.Parameters.ParamByName('xidricerca').Value := xIDRicerca;
                                             qIns.execSQL;
                                        end;

                                   end;
                              end;
                         end;
                    except
                         on e: Exception do begin
                              if Data.Global.fieldbyname('debughrms').AsBoolean then
                                   xfile.SaveToFile('testmatrix_error.xml');
                         end;
                    end;

                    xCont := 0;
                    xContGiri := xcontgiri + 100;
                    Comando := 'https://maps.googleapis.com/maps/api/distancematrix/xml?origins=';
               end;

               q.next;

               CaricaPrimiProgressForm.ProgressBar1.Position := q.RecNo;
               CaricaPrimiProgressForm.ProgressBar1.Update;
               Application.ProcessMessages;
          end;
          CaricaPrimiProgressForm.Close;
          CaricaPrimiProgressForm.Free;
          // DeleteFile('filetesting.xml');
          xfile.free;






     end else
          MessageDlg('Impossibile proseguire: Chiave "Google Distance" mancante"', mtError, [mbOk], 0);

     q.free;
     qins.free;
end;

end.

