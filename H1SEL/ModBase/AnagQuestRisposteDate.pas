unit AnagQuestRisposteDate;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     DBCtrls, StdCtrls, dxTL, dxDBCtrl, dxDBGrid, Db, dxCntner, ADODB,
     Buttons, ExtCtrls, TB97;

type
     TAnagQuestRisposteDateForm = class(TForm)
          Panel1: TPanel;
          DsQRisposteDate: TDataSource;
          QRisposteDate: TADOQuery;
          dxDBGrid1: TdxDBGrid;
          QRisposteDateID: TAutoIncField;
          QRisposteDateQuestionario: TStringField;
          QRisposteDateDomanda: TStringField;
          QRisposteDateRisposta: TStringField;
          QRisposteDateTesto: TStringField;
          QRisposteDatePunteggio: TFloatField;
          dxDBGrid1ID: TdxDBGridMaskColumn;
          dxDBGrid1Questionario: TdxDBGridMaskColumn;
          dxDBGrid1Domanda: TdxDBGridMaskColumn;
          dxDBGrid1Risposta: TdxDBGridMaskColumn;
          dxDBGrid1Testo: TdxDBGridMaskColumn;
          dxDBGrid1Punteggio: TdxDBGridMaskColumn;
          Label1: TLabel;
          QRisposteDateNominativo: TStringField;
          DBText1: TDBText;
          Label2: TLabel;
          DBText2: TDBText;
    BitBtn1: TToolbarButton97;
          procedure BitBtn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
     private
    { Private declarations }
     public
    { Public declarations }
     end;

var
     AnagQuestRisposteDateForm: TAnagQuestRisposteDateForm;

implementation

uses ModuloDati, Main;

{$R *.DFM}

procedure TAnagQuestRisposteDateForm.BitBtn1Click(Sender: TObject);
begin
      AnagQuestRisposteDateForm.ModalResult := mrNone;
end;

procedure TAnagQuestRisposteDateForm.FormShow(Sender: TObject);
begin
     //Grafica
     AnagQuestRisposteDateForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
end;

end.

