//[TONI20021004]DEBUGOK
unit StoricoTargetList;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     dxDBTLCl, dxGrClms, dxTL, dxDBCtrl, dxDBGrid, Db, DBTables, dxCntner,
     StdCtrls, Buttons, ExtCtrls, ADODB, U_ADOLinkCl;

type
     TStoricoTargetListForm = class(TForm)
          Panel1: TPanel;
          BitBtn1: TBitBtn;
          BitBtn2: TBitBtn;
          dxDBGrid1: TdxDBGrid;
          QStoricoTL_OLD: TQuery;
          DsQStoricoTL: TDataSource;
          QStoricoTL_OLDID: TAutoIncField;
          QStoricoTL_OLDIDRicerca: TIntegerField;
          QStoricoTL_OLDIDCliente: TIntegerField;
          QStoricoTL_OLDNote: TMemoField;
          QStoricoTL_OLDProgressivo: TStringField;
          QStoricoTL_OLDCliente: TStringField;
          QStoricoTL_OLDDataUltimaEsploraz: TDateTimeField;
          dxDBGrid1ID: TdxDBGridMaskColumn;
          dxDBGrid1IDRicerca: TdxDBGridMaskColumn;
          dxDBGrid1IDCliente: TdxDBGridMaskColumn;
          dxDBGrid1Progressivo: TdxDBGridMaskColumn;
          dxDBGrid1Cliente: TdxDBGridMaskColumn;
          dxDBGrid1DataUltimaEsploraz: TdxDBGridDateColumn;
          dxDBGrid1Note: TdxDBGridBlobColumn;
          Label1: TLabel;
          QStoricoTL: TADOLinkedQuery;
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     StoricoTargetListForm: TStoricoTargetListForm;

implementation
uses ModuloDati;
{$R *.DFM}

end.
