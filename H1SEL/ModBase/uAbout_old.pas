unit uAbout_old;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     URLLabel, ExtCtrls, StdCtrls, ShellAPI, TB97;

type
     TAboutForm_Old = class(TForm)
          Label1: TLabel;
          Panel1: TPanel;
          Label8: TLabel;
          Label9: TLabel;
          Memo1: TMemo;
          Memo2: TMemo;
          Panel2: TPanel;
          Image2: TImage;
          Label11: TLabel;
          URLLabel1: TURLLabel;
          Image1: TImage;
          Label12: TLabel;
          Label13: TLabel;
          URLLabel2: TURLLabel;
          Panel3: TPanel;
          Label2: TLabel;
          Label3: TLabel;
          Label4: TLabel;
          Label5: TLabel;
          Label6: TLabel;
          Label7: TLabel;
          Label10: TLabel;
          Edit1: TEdit;
          Edit2: TEdit;
          Edit3: TEdit;
          Edit4: TEdit;
          Edit5: TEdit;
          Edit6: TEdit;
          Edit7: TEdit;
          ToolbarButton971: TToolbarButton97;
          procedure FormShow(Sender: TObject);
          procedure URLLabel1Click(Sender: TObject);
          procedure URLLabel2Click(Sender: TObject);
          procedure ToolbarButton971Click(Sender: TObject);
     private
          { Private declarations }
          xguid: string;
     public
          { Public declarations }
     end;

var
     AboutForm_Old: TAboutForm_Old;

implementation

uses ModuloDati, Main;

{$R *.DFM}

procedure TAboutForm_Old.FormShow(Sender: TObject);
var xConfigString, xLista: TStringList;
     xS: string;
     i: integer;
begin

     if not FileExists(ExtractFileDir(Application.ExeName) + '\ConfigFile.txt') then begin
       Exit;
     end;

     xConfigString := TStringList.create;
     xConfigString.LoadFromFile(ExtractFileDir(Application.ExeName) + '\ConfigFile.txt');
     // eliminazione righe vuote
     if xConfigString.strings[1] = '' then begin
          for i := 0 to xConfigString.count - 1 do begin
               if i <= xConfigString.count then
                    if xConfigString.strings[i] = '' then xConfigString.Delete(i);
          end;
     end;
     Edit7.Text := ' ' + copy(xConfigString.strings[1], pos('=', xConfigString.strings[1]) + 1, length(xConfigString.strings[1]));
     Edit1.Text := ' ' + copy(xConfigString.strings[2], pos('=', xConfigString.strings[2]) + 1, length(xConfigString.strings[2]));
     Edit2.Text := ' ' + copy(xConfigString.strings[3], pos('=', xConfigString.strings[3]) + 1, length(xConfigString.strings[3]));
     if copy(xConfigString.strings[4], pos('=', xConfigString.strings[4]) + 1, length(xConfigString.strings[4])) = '' then
          Edit3.Text := ' nessuna'
     else Edit3.Text := ' ' + copy(xConfigString.strings[4], pos('=', xConfigString.strings[4]) + 1, length(xConfigString.strings[4]));
     if copy(xConfigString.strings[5], pos('=', xConfigString.strings[5]) + 1, length(xConfigString.strings[5])) = '0' then
          Edit4.Text := ' illimitati'
     else Edit4.Text := ' ' + copy(xConfigString.strings[5], pos('=', xConfigString.strings[5]) + 1, length(xConfigString.strings[5]));
     if copy(xConfigString.strings[6], pos('=', xConfigString.strings[6]) + 1, length(xConfigString.strings[6])) = '0' then
          Edit5.Text := ' illimitati'
     else Edit5.Text := ' ' + copy(xConfigString.strings[6], pos('=', xConfigString.strings[6]) + 1, length(xConfigString.strings[6]));
     if copy(xConfigString.strings[7], pos('=', xConfigString.strings[7]) + 1, length(xConfigString.strings[7])) = '' then
          Edit6.Text := ' nessuna'
     else Edit6.Text := ' ' + copy(xConfigString.strings[7], pos('=', xConfigString.strings[7]) + 1, length(xConfigString.strings[7]));
     // elenco funzioni
     xLista := TStringList.create;
     xS := xConfigString.strings[8];
     while xS <> '' do begin
          Data.QTemp.Close;
          Data.QTemp.SQL.text := 'select Funzione from SAC_ModuliFunzioni where CodModulo=' + copy(xS, 1, pos(';', xS) - 1);
          Data.QTemp.Open;
          if not Data.QTemp.IsEmpty then
               xLista.Add(Data.QTemp.FieldByName('Funzione').asString)
          else xLista.Add(copy(xS, 1, pos(';', xS) - 1));
          xS := copy(xS, pos(';', xS) + 1, length(xS))
     end;
     Data.QTemp.Close;
     Memo1.text := xLista.text;
     // elenco client
     xLista.Clear;
     xS := xConfigString.strings[9];
     while xS <> '' do begin
          xLista.Add(copy(xS, 1, pos(';', xS) - 1));
          xS := copy(xS, pos(';', xS) + 1, length(xS))
     end;
     Memo2.text := xLista.text;

     //guid
     xguid:='-1';
     xguid := copy(xConfigString.strings[12], pos('=', xConfigString.strings[12]) + 1, length(xConfigString.strings[12]));
   //  showmessage(xguid);

     xLista.Free;
     xConfigString.Free;
     
     //Grafica
     AboutForm_old.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel3.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel3.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel3.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
end;

procedure TAboutForm_Old.URLLabel1Click(Sender: TObject);
begin
     ShellExecute(0, 'Open', pchar('www.ebcconsulting.com'), '', '', SW_SHOW);
end;

procedure TAboutForm_Old.URLLabel2Click(Sender: TObject);
begin
     ShellExecute(0, 'Open', pchar('mailto:helpdesk@ebcconsulting.com'), '', '', SW_SHOW);
end;

procedure TAboutForm_Old.ToolbarButton971Click(Sender: TObject);
begin
     if xguid <> '-1' then begin
          ShellExecute(Application.Handle, 'open',
               pchar('http://h1web.ebcconsulting.com/ServizioClienti/Default.aspx?guid=' + xguid ),
               nil, nil, SW_SHOWNORMAL);

     end;
end;

end.

