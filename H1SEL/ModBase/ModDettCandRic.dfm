�
 TMODDETTCANDRICFORM 0d&  TPF0TModDettCandRicFormModDettCandRicFormLeft�Top� ActiveControl	DEDataInsBorderStylebsDialogCaption!Dettaglio candidato nella ricercaClientHeightqClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnShowFormShowPixelsPerInch`
TextHeight TLabelLabel1LeftTopBWidthvHeightCaptionInserito in elenco in data:  TLabelLabel2LeftTop� Width4HeightCaptionSituazione:  TLabelLabel3LeftTopmWidth7HeightCaptionValutazione  TLabelLabel4LeftTop� Width� HeightCaption#Note Commessa: (max 8000 caratteri)  TLabelLabel5LeftTopbWidthHeightCaption9Note Cliente interno / selezionatore (max 8000 caratteri)  TLabelLabel6LeftTop�WidthpHeightCaptionIndicazione (da tabella):  TPanelPanel1LeftTopWidthHeight5
BevelOuter	bvLoweredEnabledTabOrder TDBEditDBEdit1LeftTopWidth9Height	DataFieldCVNumero
DataSourceDataRicerche.DsQCandRicTabOrder   TDBEditDBEdit2Left@TopWidth� Height	DataFieldCognome
DataSourceDataRicerche.DsQCandRicFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder  TDBEditDBEdit3Left@TopWidth� Height	DataFieldNome
DataSourceDataRicerche.DsQCandRicTabOrder   TDateEdit97	DEDataInsLeftTopQWidth_HeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder ColorCalendar.ColorValidclBlueDayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanPopup
doIsMaskeddoShowCanceldoShowToday   TEditEMiniValLeftTop}WidthvHeightCharCaseecUpperCase	MaxLength� TabOrder  TMemoESituazioneLeftTop� WidthxHeight;	MaxLength TabOrder  	TCheckBoxcbVisibileClienteLeftTop�Width� HeightCaptionVisibile al clienteFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TMemoENoteLeftTop� Width}Heighti	MaxLength@
ScrollBars
ssVerticalTabOrder  TMemoeNoteCliLeftToprWidth}HeightY	MaxLength@
ScrollBars
ssVerticalTabOrder  TDBGridDBGrid1LeftTop�Width� Heights
DataSourceDsQIndicazioniOptionsdgTitlesdgIndicatordgColumnResize
dgColLines
dgRowLinesdgTabsdgConfirmDeletedgCancelOnExit TabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style ColumnsExpanded	FieldNameIDVisible Expanded	FieldNameIndicazioneWidth~Visible	    	TCheckBoxCB_VisionatoLeft� TopSWidtheHeightCaption	VisionatoFont.CharsetDEFAULT_CHARSET
Font.ColorclMaroonFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TPanelPanel2Left'TopWidthZHeightU
BevelOuterbvNoneTabOrder	 TToolbarButton97BOkLeftTopWidthPHeight#CaptionOK
Glyph.Data
z  v  BMv      6  (               @                     ��� � � @�h ��� TQI �? v�v ��� ('' yґ E�^ qm^ ��� $�M ��� _�w \� ��� ��� �ټ \�d =<9 v�� 4�N :�] |{s T�s _^^ ��x ��� ,�X �Ʀ b�s jɃ ��� I�u ��� O�j ��� i|m ��� ��� *�M ��� ��� ��� 421 D�f S�` �� 7�^ JIC 9�U r� |�� _wc nnm U�l k� ��� ��� 0�W J�p ��� f�l `�l wċ %�G � �° T�q ��� e�y ��p ��� V�y qЊ [XN [�y ��� ��� M�n �Ė ��� 2�T urq d�} =�[ A�` ��� ��� ��� ��� ��� ��� F�p ��� H�c �Ǔ ��� m�v n� dЁ N�u C�h x�� V�o *�R �� ��� ?�b ~~{ o�p 2�P ��� �̹ 1�Z ��� H�_ ��� ;�c Ļ� g�o w͎ ��� U�z O�g ��� ]�r r�� ��� ��� \�o ��� c`a 0�V M�p F�f [�w �ȶ ��� 8�Y I�[ ��� ��u ��� sɉ 3�] ��� Y�o ��� ��� 3�Z �á mą i�z @�j Q�u [YY I�m ��� ��� ��� ��� (�Q ��� ?�c Z�~ ��� e� {�z b�s ��� IIH B�a yvu ��� l͆ ��� p�� t�� �ϼ Ŀ� +�V <�X J�k ��� |�� ��� h�~ <�a I�s 11/ ��� ��� E�n O�r k�m ��} ��� _�y e�w ��� ;�d S�y cxf ��� ��� |̑ ɿ� 7�Z =�] e|j ��� ��� Q�l }�� zxp ��� {�� ��� ��� &�K +�O L�a ��� ��� Y�t �Ɵ b�x ��� ��� ��� U�v ��� S�n \�v |Ǐ ��� ��� �˽ ��� ��� VTK ��� �ɼ ƽ� 7�_ A�j B�] ��� W�| ��� ��� ��� .9��		��9.�/Pz��z�/�.�V}��uu���}���4��>?����?�>E_4p�����?ih~�L�����=<N�uy�0�`�h��L�u�%N�s��3yo�n��$��?L���d�<+�3�nnn�`���ŧ��s��l���nn�nףR�yy�Rl@���vB�U­n�HQ�'���y3�R�ґJ#5����Q\�''���3�0�#J���ח�uy�\�''����ӯD��t��G�˥333uޤ'���l�&rF2��wk��O�>��3��㗗�XY�^���{�W"MgR�>>,��-�b1�F��澛�|


M���-���F���!I;�C����|���Z]��А���q��j�cccc�C�;�:����[�A��*SSSS*��;I��%���a�A��������fʜЕ���_�� ����m68���x}=(7��Te�=}x���K�.))ۍK����۱��.۱�OnClickBOkClick  TToolbarButton97BAnnullaLeftTop+WidthPHeight#CaptionAnnulla
Glyph.Data
z  v  BMv      6  (               @                     ��� � � ��} � Ze� ��� =<: ��� ��� OO� ��� ba] 47� ��� ef� bb� ��~ �û xx� ~�� ((' ��� 76� NN� ��� ��� ��� SRI tt� mmm %%� LS� ��� ��� cb� ��w cc� CF� XX� ?G� VV� ��� ��� ��� �к ��� 330 ml� ��� ��� ||� ��� ��� HHH {zs pn] -.� (*� jl� aa� Z[� ��� ��� ��� oo� ��o sr� VZ� ��� ��� ��� CC� ��� ��� ||� ��� 38� MM� 00� �ŭ ss� ��� T^� QQ� ii� -2� GK� ��� ��� ��� ��� 4;� __� YZW ``� II� w|� ��� ��� ��� ;>� ��� ��� JJ� YWM ��� VV� ��� \\� ��� ��� dm� ��� ��� nn� )-� ##� ��� ��� ��� �˻ AF� QW� @@� qq� -/� utm ??� II� NN� ��� ��� 9:� ��� ��� Ya� ll� 33� ��� ��� ~~� �ջ ))� ��� ��� ��� ��� ��� ��� ��� ��� hg� xw� ��� ��� BD� ��� KT� ��� `h� nn� ��� �Ǽ DG� NO� TZ� w{� JIB [[� ��� ��� ��t bb� ��� BJ� QQ� __� ||� ��� 9>� ��� ��� FE� ff� ��� 66� 00/ ��� �ٽ ��� //� �Ƿ \\[ �ö 99� ~~{ ww� ��� EF� NV� II� ��� QQ� ^^� ��� zz� 44� ��� ]d� SU� V]� ��� SS� ~~� �͸ 68� ��� ��� aa� ��� {{� ��� EJ� ��� ZZ� ^[� 8;� 8=� AK� ��� ��� ��� <;6 ��� 35� 55� 58� GG� YY� [^� bb� jj� rr� ��� ��� ��� VTK ))� ++� 01� yxn ��� ``� ��� ��� 6��6��/joo�/^R���M��M=+�7��ތ�ߦ��ш�z�+���!�_� �ӈ�pp�Ѥt�����i��V&��SSS��D���qiE7X?~�n�a�SS ����~����8���2��4�SS�������8E��J��;gn>Z SS�c�gW��9fL��*�ϊ~�n�{�(.�g㜥��P�Bh�����4\���e��|B$-ðh�إg��4�ԃ�e؊�Hu�$�׀��T��D�nnnn��؊��������=%�:�nnn����9�H�O����#k̭'�>�n��n�'���Ɂ
-�rP�__���>"5>�d`���CPr[,�]_������}�g��w��`�v�ry0�K�1���s��GN�y�[��<�K���������)q�[�	�<lxl���x�Q���)�������b�+lx�QQAm@��[�F���b+3U�F��[�ʹYIIY�ʚ[[E����[OnClickBAnnullaClick   TPanelPanel3Left� Top�WidthhHeight,
BevelOuterbvNoneTabOrder
 TToolbarButton97SpeedButton1LeftTopWidth\Height#CaptionModifica Tabella
Glyph.Data
:  6  BM6      6  (                                       ��� � � �� � ��� \XX {�� 6O� q� ��� s�� B�� "FY CO� ��� ,�� !!o ��� ==< grt ��� ?o Pb� B�� S�� ��� ��� $Us �~{ @�� r� oga ��� ��� }� ��� ��� 5_s ��� m�� 0�� $�� K�� ��� 8�� ��� uuu Lk}  � "s� ��� ��� ��� z� ��� ��� ��� ``` kll ��� ��� ��� ��� I�� ��� ��� ��� )�� ��� N�� 4�� ��� ��� ��� %�� ��� 4gw ��� ��� eee "�� ��� ��� ��� ��� v� ��� þ� ��� �� F�� ooo ��� {{{ @�� ��� ��� WUU Ghz ��� %Sm ��� ��� P�� ��� ��� A�� ~� ��� 1P� &�� ��� ��� y�� ��� ��� ��� ��� ��� ��� !�� ��� ��� ��� ��� ��� ��� N�� ��� ��� ��� ��� hhh ��� ��� ��� &�� ��� ��� |{v ��� ��� ��� ��� ��� ��� ��� |� ��� ��� ��� ��� ��� ��� s� u� ��� ��� E�� ��� ��� ��� ~~~ ��� ��� ��� ��� ��� ��� ��� ��� ��� �� ��� mmm I�� sss ��� www >�� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ##n ��� AN� ��� H�� ��� ��� B�� ��} ��� ��� ��� ��� ��� ��� ��� x� }� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� M�� xyy A�� ��� z�� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� ��� b ec�5�>4`�&-o2���4``�5�G)Q�C�^u5��`>���:^0D@FZ�"��5>4``4]u/PP���I<�r���VA5�4``�5T�P��]S��zz�B6�r[[�L5�>�4`�T�P�4�I�zz��}J�r��y	�>�4�`tT�SIzz��
f�ݪ�f��r++K	�``^�zzI���}ݪ��j؝B��ቛ���'�g$|.��Y�Ɇ���~l��h�*M4,�|.��ߠW'�'���W�W�Ul�k�T4��ff��I�I�
���gg����9H#_(�˺4������f�ff�������侢�a�𥵡���������qqqqqxq���n>�9��}���������RR��mT>�>sx9���������m�����;`�1�{]%�qmmmmmmm�ބ�ƽ�ۗ;5X����������ܕ��ڬ�a\�3�dddd������??�?p���=���������w����pp!�TE?���ppp������֑�Ď�ui����בp����ՑppppNu������ppv�vՑב��՚Ttv�Ցv��v����vvv��,Ttt]`j�vv���vv���������/]�>���jj�������j�����4ft�,>����jj������эҁ��4��`�`5�󩩩�7��.�O|8}��S4u�7�7��8g��z��t��,ut>>�5uut��>�4`WordWrap	OnClickSpeedButton1Click   	TADOQueryQIndicazioni
ConnectionData.DB
CursorTypectStatic
Parameters SQL.Stringsselect ID,Indicazionefrom EBC_Ricerche_indicazioniunion 'select NULL ID, '(nessuna)' Indicazioneorder by Indicazione LeftTop� TAutoIncFieldQIndicazioniID	FieldNameIDReadOnly	  TStringFieldQIndicazioniIndicazione	FieldNameIndicazioneSize2   TDataSourceDsQIndicazioniDataSetQIndicazioniLeft0Top�  	TADOQueryQ
ConnectionData.DB
Parameters Left0Top�   