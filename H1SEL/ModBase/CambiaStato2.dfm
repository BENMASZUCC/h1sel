�
 TCAMBIASTATO2FORM 0�'  TPF0TCambiaStato2FormCambiaStato2FormLeft8Top� WidthDHeight�CaptionNuovo eventoColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrder	PositionpoScreenCenterOnCreate
FormCreateOnShowFormShowPixelsPerInch`
TextHeight TPanelPanel1Left Top Width<Height%AlignalTopEnabledTabOrder  TEditECognLeftTopWidth� HeightColorclYellowFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontTabOrder TextECogn  TEditENomeLeft� TopWidth� HeightColorclYellowTabOrderTextENome   TPanelPanStatoLeft Top%Width<Height!AlignalTopEnabledTabOrder TLabelLabel2Left	Top	Width?HeightCaptionStato attuale:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TEditEStatoLeftPTopWidth� HeightTabOrder TextEStato   TPanelPanel4Left TopFWidth<Height0AlignalClientFont.CharsetDEFAULT_CHARSET
Font.Color
clMenuTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder TDBGridDBGrid1_oldLeftTop� Width8Height9
DataSourcedsQNextStatiFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontReadOnly	TabOrder TitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style VisibleColumnsExpanded	FieldNameEventoTitle.Font.CharsetDEFAULT_CHARSETTitle.Font.ColorclWindowTextTitle.Font.Height�Title.Font.NameMS Sans SerifTitle.Font.Style Width� Visible	 Expanded	FieldNameAStatoTitle.Captionstato successivoTitle.Font.CharsetDEFAULT_CHARSETTitle.Font.ColorclWindowTextTitle.Font.Height�Title.Font.NameMS Sans SerifTitle.Font.Style WidthsVisible	    TPanelPanel5LeftTopWidth:Height2AlignalTop
BevelOuterbvNoneTabOrder TLabelLabel4LeftTopWidth>HeightCaptionData evento:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TDateEdit97DEDataLeftNTopWidthcHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder ColorCalendar.ColorValidclBlueDayNames.MondayluDayNames.TuesdaymaDayNames.WednesdaymeDayNames.ThursdaygiDayNames.FridayveDayNames.SaturdaysaDayNames.SundaydoMonthNames.JanuarygennaioMonthNames.FebruaryfebbraioMonthNames.MarchmarzoMonthNames.AprilaprileMonthNames.MaymaggioMonthNames.JunegiugnoMonthNames.JulyluglioMonthNames.AugustagostoMonthNames.September	settembreMonthNames.OctoberottobreMonthNames.NovembernovembreMonthNames.DecemberdicembreOptionsdoButtonTabStop
doCanPopup
doIsMaskeddoShowCanceldoShowToday   TPanelPanel127Left TopWidth8Height	AlignmenttaLeftJustifyCaption&  Eventi possibili dallo stato attualeColorclGrayFont.CharsetDEFAULT_CHARSET
Font.ColorclBlueFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder   TPanelPanel6LeftTop	Width:Height&AlignalBottomTabOrderVisible TLabelLabel1LeftTopWidthHeightCaptionNote:Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TEditEdit1Left)TopWidthHeightFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 	MaxLength2
ParentFontTabOrder    	TdxDBGridDBGrid1LeftTop3Width:Height� Bands  DefaultLayout	HeaderPanelRowCountKeyFieldIDSummaryGroups SummarySeparator, AlignalClientFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderBandFont.CharsetDEFAULT_CHARSETBandFont.ColorclWindowTextBandFont.Height�BandFont.NameMS Sans SerifBandFont.Style 
DataSourcedsQNextStatiFilter.Active	Filter.CaseInsensitive	Filter.Criteria
       HeaderFont.CharsetDEFAULT_CHARSETHeaderFont.ColorclWindowTextHeaderFont.Height�HeaderFont.NameMS Sans SerifHeaderFont.Style OptionsBehavioredgoAutoSortedgoDragScrolledgoEnterShowEditoredgoImmediateEditoredgoTabThroughedgoVertThrough 	OptionsDBedgoCancelOnExitedgoCanDeleteedgoCanInsertedgoCanNavigationedgoConfirmDeleteedgoLoadAllRecordsedgoUseBookmarks OptionsViewedgoBandHeaderWidthedgoIndicatoredgoUseBitmap PreviewFont.CharsetDEFAULT_CHARSETPreviewFont.ColorclBluePreviewFont.Height�PreviewFont.NameMS Sans SerifPreviewFont.Style  TdxDBGridMaskColumn	DBGrid1IDDisableEditor	VisibleWidth	BandIndex RowIndex 	FieldNameID  TdxDBGridMaskColumnDBGrid1EventoDisableEditor	SortedcsUpWidth� 	BandIndex RowIndex 	FieldNameEvento  TdxDBGridMaskColumnDBGrid1IDaStatoDisableEditor	VisibleWidth	BandIndex RowIndex 	FieldNameIDaStato  TdxDBGridMaskColumnDBGrid1AStatoCaptionstato successivoDisableEditor	Width� 	BandIndex RowIndex 	FieldNameAStato  TdxDBGridMaskColumnDBGrid1IDProceduraDisableEditor	VisibleWidth	BandIndex RowIndex 	FieldNameIDProcedura  TdxDBGridMaskColumnDBGrid1IDTipoStatoADisableEditor	VisibleWidth	BandIndex RowIndex 	FieldNameIDTipoStatoA    TPanelPanel3Left TopvWidth<Height2AlignalBottomTabOrder TPanelPanel2Left� TopWidth� Height)
BevelOuterbvNoneTabOrder  TToolbarButton97BOKLeftTopWidthPHeight#CaptionOK
Glyph.Data
z  v  BMv      6  (               @                     ��� � � @�h ��� TQI �? v�v ��� ('' yґ E�^ qm^ ��� $�M ��� _�w \� ��� ��� �ټ \�d =<9 v�� 4�N :�] |{s T�s _^^ ��x ��� ,�X �Ʀ b�s jɃ ��� I�u ��� O�j ��� i|m ��� ��� *�M ��� ��� ��� 421 D�f S�` �� 7�^ JIC 9�U r� |�� _wc nnm U�l k� ��� ��� 0�W J�p ��� f�l `�l wċ %�G � �° T�q ��� e�y ��p ��� V�y qЊ [XN [�y ��� ��� M�n �Ė ��� 2�T urq d�} =�[ A�` ��� ��� ��� ��� ��� ��� F�p ��� H�c �Ǔ ��� m�v n� dЁ N�u C�h x�� V�o *�R �� ��� ?�b ~~{ o�p 2�P ��� �̹ 1�Z ��� H�_ ��� ;�c Ļ� g�o w͎ ��� U�z O�g ��� ]�r r�� ��� ��� \�o ��� c`a 0�V M�p F�f [�w �ȶ ��� 8�Y I�[ ��� ��u ��� sɉ 3�] ��� Y�o ��� ��� 3�Z �á mą i�z @�j Q�u [YY I�m ��� ��� ��� ��� (�Q ��� ?�c Z�~ ��� e� {�z b�s ��� IIH B�a yvu ��� l͆ ��� p�� t�� �ϼ Ŀ� +�V <�X J�k ��� |�� ��� h�~ <�a I�s 11/ ��� ��� E�n O�r k�m ��} ��� _�y e�w ��� ;�d S�y cxf ��� ��� |̑ ɿ� 7�Z =�] e|j ��� ��� Q�l }�� zxp ��� {�� ��� ��� &�K +�O L�a ��� ��� Y�t �Ɵ b�x ��� ��� ��� U�v ��� S�n \�v |Ǐ ��� ��� �˽ ��� ��� VTK ��� �ɼ ƽ� 7�_ A�j B�] ��� W�| ��� ��� ��� .9��		��9.�/Pz��z�/�.�V}��uu���}���4��>?����?�>E_4p�����?ih~�L�����=<N�uy�0�`�h��L�u�%N�s��3yo�n��$��?L���d�<+�3�nnn�`���ŧ��s��l���nn�nףR�yy�Rl@���vB�U­n�HQ�'���y3�R�ґJ#5����Q\�''���3�0�#J���ח�uy�\�''����ӯD��t��G�˥333uޤ'���l�&rF2��wk��O�>��3��㗗�XY�^���{�W"MgR�>>,��-�b1�F��澛�|


M���-���F���!I;�C����|���Z]��А���q��j�cccc�C�;�:����[�A��*SSSS*��;I��%���a�A��������fʜЕ���_�� ����m68���x}=(7��Te�=}x���K�.))ۍK����۱��.۱�OnClickBOKClick  TToolbarButton97BAnnullaLeft]TopWidthPHeight#CaptionAnnulla
Glyph.Data
z  v  BMv      6  (               @                     ��� � � ��} � Ze� ��� =<: ��� ��� OO� ��� ba] 47� ��� ef� bb� ��~ �û xx� ~�� ((' ��� 76� NN� ��� ��� ��� SRI tt� mmm %%� LS� ��� ��� cb� ��w cc� CF� XX� ?G� VV� ��� ��� ��� �к ��� 330 ml� ��� ��� ||� ��� ��� HHH {zs pn] -.� (*� jl� aa� Z[� ��� ��� ��� oo� ��o sr� VZ� ��� ��� ��� CC� ��� ��� ||� ��� 38� MM� 00� �ŭ ss� ��� T^� QQ� ii� -2� GK� ��� ��� ��� ��� 4;� __� YZW ``� II� w|� ��� ��� ��� ;>� ��� ��� JJ� YWM ��� VV� ��� \\� ��� ��� dm� ��� ��� nn� )-� ##� ��� ��� ��� �˻ AF� QW� @@� qq� -/� utm ??� II� NN� ��� ��� 9:� ��� ��� Ya� ll� 33� ��� ��� ~~� �ջ ))� ��� ��� ��� ��� ��� ��� ��� ��� hg� xw� ��� ��� BD� ��� KT� ��� `h� nn� ��� �Ǽ DG� NO� TZ� w{� JIB [[� ��� ��� ��t bb� ��� BJ� QQ� __� ||� ��� 9>� ��� ��� FE� ff� ��� 66� 00/ ��� �ٽ ��� //� �Ƿ \\[ �ö 99� ~~{ ww� ��� EF� NV� II� ��� QQ� ^^� ��� zz� 44� ��� ]d� SU� V]� ��� SS� ~~� �͸ 68� ��� ��� aa� ��� {{� ��� EJ� ��� ZZ� ^[� 8;� 8=� AK� ��� ��� ��� <;6 ��� 35� 55� 58� GG� YY� [^� bb� jj� rr� ��� ��� ��� VTK ))� ++� 01� yxn ��� ``� ��� ��� 6��6��/joo�/^R���M��M=+�7��ތ�ߦ��ш�z�+���!�_� �ӈ�pp�Ѥt�����i��V&��SSS��D���qiE7X?~�n�a�SS ����~����8���2��4�SS�������8E��J��;gn>Z SS�c�gW��9fL��*�ϊ~�n�{�(.�g㜥��P�Bh�����4\���e��|B$-ðh�إg��4�ԃ�e؊�Hu�$�׀��T��D�nnnn��؊��������=%�:�nnn����9�H�O����#k̭'�>�n��n�'���Ɂ
-�rP�__���>"5>�d`���CPr[,�]_������}�g��w��`�v�ry0�K�1���s��GN�y�[��<�K���������)q�[�	�<lxl���x�Q���)�������b�+lx�QQAm@��[�F���b+3U�F��[�ʹYIIY�ʚ[[E����[OnClickBAnnullaClick    TDataSourcedsQNextStatiDataSet
QNextStatiLeft� Top�   TADOLinkedQuery
QNextStati
ConnectionData.DB
CursorTypectStatic
ParametersNamexDaStato
AttributespaSigned
paNullable DataType	ftInteger	Precision
SizeValue   SQL.Strings;select EBC_Eventi.ID,EBC_Eventi.Evento,EBC_Eventi.IDaStato,!          EBC_Stati.Stato AStato,!          EBC_Eventi.IDProcedura,,          EBC_Stati.IDTipoStato IDTipoStatoAfrom EBC_Eventi2join EBC_Stati on EBC_Eventi.IDaStato=EBC_Stati.ID"and EBC_Eventi.IDdaStato=:xDaStato,and EBC_Stati.IDTipoStato not in (6,7,11,15)order by ordinamento 	UseFilterLeft� Top�  TAutoIncFieldQNextStatiID	FieldNameIDReadOnly	  TStringFieldQNextStatiEvento	FieldNameEventoSized  TIntegerFieldQNextStatiIDaStato	FieldNameIDaStato  TStringFieldQNextStatiAStato	FieldNameAStatoSize  TIntegerFieldQNextStatiIDProcedura	FieldNameIDProcedura  TIntegerFieldQNextStatiIDTipoStatoA	FieldNameIDTipoStatoA    