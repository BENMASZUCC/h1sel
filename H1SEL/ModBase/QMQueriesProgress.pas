unit QMQueriesProgress;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ComCtrls, dxTL, dxCntner, StdCtrls, ExtCtrls, ImgList;

type
     TQMQueriesProgressForm = class(TForm)
          Image1: TImage;
          Label1: TLabel;
          ProgressBar1: TProgressBar;
          PanWarning: TPanel;
          Image2: TImage;
          LWarning: TLabel;
          PanStop: TPanel;
          Image4: TImage;
          LStop: TLabel;
          PanOK: TPanel;
          Image5: TImage;
          Label2: TLabel;
          Label3: TLabel;
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     QMQueriesProgressForm: TQMQueriesProgressForm;

implementation

{$R *.DFM}

end.
