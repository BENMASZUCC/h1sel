unit TabCondizioniOfferte;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     TB97, ExtCtrls, Db, ADODB, dxTL, dxDBCtrl, dxDBGrid, dxCntner, DBCtrls;

type
     TTabCondizioniOfferteForm = class(TForm)
          Panel1: TPanel;
          BOk: TToolbarButton97;
          BAnnulla: TToolbarButton97;
    QTabCondizioni: TADOQuery;
    DsQTabCondizioni: TDataSource;
    QTabCondizioniID: TAutoIncField;
    QTabCondizioniCondizione: TStringField;
    QTabCondizioniPerc: TSmallintField;
    dxDBGrid1: TdxDBGrid;
    dxDBGrid1ID: TdxDBGridMaskColumn;
    dxDBGrid1Condizione: TdxDBGridMaskColumn;
    dxDBGrid1Perc: TdxDBGridMaskColumn;
    DBNavigator1: TDBNavigator;
          procedure BOkClick(Sender: TObject);
          procedure BAnnullaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     TabCondizioniOfferteForm: TTabCondizioniOfferteForm;

implementation

uses ModuloDati;

{$R *.DFM}

procedure TTabCondizioniOfferteForm.BOkClick(Sender: TObject);
begin
     TabCondizioniOfferteForm.ModalResult := mrOk;
end;

procedure TTabCondizioniOfferteForm.BAnnullaClick(Sender: TObject);
begin
     TabCondizioniOfferteForm.ModalResult := mrCancel;
end;

procedure TTabCondizioniOfferteForm.FormShow(Sender: TObject);
begin
     QTabCondizioni.open;
end;

procedure TTabCondizioniOfferteForm.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     if DsQTabCondizioni.State in [dsInsert, dsEdit] then QTabCondizioni.post;
end;

end.

