unit RifClienteDati;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Db, DBTables, Spin, DtEdit97;

type
  TRifClienteDatiForm = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Panel1: TPanel;
    Label1: TLabel;
    ENominativo: TEdit;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    SEGiorno: TSpinEdit;
    SEMese: TSpinEdit;
    QAltriDati: TQuery;
    Panel2: TPanel;
    Panel3: TPanel;
    Label4: TLabel;
    ETitStudio: TEdit;
    Label5: TLabel;
    ECaricaAz: TEdit;
    Label6: TLabel;
    ENomeSeg: TEdit;
    GroupBox2: TGroupBox;
    Label7: TLabel;
    Label8: TLabel;
    Label10: TLabel;
    EIndirizzo: TEdit;
    ECap: TEdit;
    EProv: TEdit;
    Label9: TLabel;
    ETelAb: TEdit;
    Panel4: TPanel;
    Label11: TLabel;
    Label12: TLabel;
    EUtente: TEdit;
    DEDataIns: TDateEdit97;
    CBSesso: TComboBox;
    Label13: TLabel;
    Label14: TLabel;
    Label93: TLabel;
    Label99: TLabel;
    CBTipoStradaRes: TComboBox;
    ENumCivico: TEdit;
    Label15: TLabel;
    EDivisioneAz: TEdit;
    Label16: TLabel;
    ENazione: TEdit;
    procedure FormShow(Sender: TObject);
    procedure CBTipoStradaResDropDown(Sender: TObject);
  private
    { Private declarations }
  public
    xIDContatto:integer;
  end;

var
  RifClienteDatiForm: TRifClienteDatiForm;

implementation

uses ModuloDati;

{$R *.DFM}

procedure TRifClienteDatiForm.FormShow(Sender: TObject);
begin

//[ALBERTO 20020910]

     QAltriDati.ParamByName['xID']:=xIDContatto;
     QAltriDati.Open;
     SEGiorno.Value:=QAltriDati.FieldByName('CompleannoGiorno').asInteger;
     SEMese.Value:=QAltriDati.FieldByName('CompleannoMese').asInteger;
     ETitStudio.Text:=QAltriDati.FieldByName('TitoloStudio').asString;
     ECaricaAz.Text:=QAltriDati.FieldByName('CaricaAziendale').asString;
     ENomeSeg.Text:=QAltriDati.FieldByName('NomeSegretaria').asString;
     EIndirizzo.Text:=QAltriDati.FieldByName('IndirizzoPrivato').asString;
     ECap.Text:=QAltriDati.FieldByName('CapPrivato').asString;
     EProv.Text:=QAltriDati.FieldByName('ProvinciaPrivato').asString;
     ETelAb.Text:=QAltriDati.FieldByName('TelefonoAbitazione').asString;
     EUtente.text:=QAltriDati.FieldByName('Nominativo').asString;
     DEDataIns.Date:=QAltriDati.FieldByName('DataIns').asDateTime;
     EDivisioneAz.text:=QAltriDati.FieldByName('DivisioneAziendale').asString;
     CBTipoStradaRes.text:=QAltriDati.FieldByName('TipoStradaPrivato').asString;
     ENumCivico.text:=QAltriDati.FieldByName('NumCivicoPrivato').asString;
     ENazione.text:=QAltriDati.FieldByName('Stato').asString;
     Caption:='[M/104] - '+caption;
end;

procedure TRifClienteDatiForm.CBTipoStradaResDropDown(Sender: TObject);
begin
     // tipo di strada in funzione della Nazione
     with Data do begin
          Q1.Close;
          Q1.SQL.text:='select TipoStrada from TipiStrade where NazioneAbb=:xNazioneAbb:';
          if ENazione.Text<>'' then
               Q1.ParamByName['xNazioneAbb']:=ENazione.Text
          else Q1.ParamByName['xNazioneAbb']:=Global.FieldByName('NazioneAbbAzienda').AsString;

//[ALBERTO 20020910]FINE

          Q1.Open;
          CBTipoStradaRes.Items.Clear;
          while not Q1.EOF do begin
               CBTipoStradaRes.Items.Add(Q1.FieldByName('TipoStrada').asString);
               Q1.next;
          end;
          Q1.Close;
     end;
end;

end.
