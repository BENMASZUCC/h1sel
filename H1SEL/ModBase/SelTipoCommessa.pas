unit SelTipoCommessa;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, Grids, DBGrids, Db, DBTables, ADODB, U_ADOLinkCl,
  TB97, ExtCtrls;

type
     TSelTipoCommessaForm = class(TForm)
          QTipo_OLD: TQuery;
          DsQTipo: TDataSource;
          QTipo_OLDID: TAutoIncField;
          QTipo_OLDTipoCommessa: TStringField;
          QTipo_OLDColore: TStringField;
          DBGrid1: TDBGrid;
          QTipo: TADOLinkedQuery;
          Panel1: TPanel;
          BOK: TToolbarButton97;
          BAnnulla: TToolbarButton97;
          procedure FormShow(Sender: TObject);
          procedure BOKClick(Sender: TObject);
          procedure BAnnullaClick(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     SelTipoCommessaForm: TSelTipoCommessaForm;

implementation
uses ModuloDati, Main;
{$R *.DFM}

procedure TSelTipoCommessaForm.FormShow(Sender: TObject);
begin
     Caption := '[M/211] - ' + caption;
     SelTipoCommessaForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
end;

procedure TSelTipoCommessaForm.BOKClick(Sender: TObject);
begin
     SelTipoCommessaForm.ModalResult := mrOK;
end;

procedure TSelTipoCommessaForm.BAnnullaClick(Sender: TObject);
begin
     SelTipoCommessaForm.ModalResult := mrCancel;
end;

end.

