unit LegendaCandUtente;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, ExtCtrls, Buttons, RXSpin, Db, DBTables, ADODB, U_ADOLinkCl,
     TB97;

type
     TLegendaCandUtenteForm = class(TForm)
          Q_OLD: TQuery;
          Panel7: TPanel;
          Label19: TLabel;
          Label20: TLabel;
          RxSpinEdit3: TRxSpinEdit;
          Label2: TLabel;
          Panel6: TPanel;
          Label16: TLabel;
          Label17: TLabel;
          Label1: TLabel;
          RxSpinEdit2: TRxSpinEdit;
          Q: TADOLinkedQuery;
          Panel1: TPanel;
          BChiudi: TToolbarButton97;
          procedure FormShow(Sender: TObject);
          procedure FormClose(Sender: TObject; var Action: TCloseAction);
          procedure BChiudiClick(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     LegendaCandUtenteForm: TLegendaCandUtenteForm;

implementation

uses ModuloDati, Main;

{$R *.DFM}

procedure TLegendaCandUtenteForm.FormShow(Sender: TObject);
begin
     Caption := '[S/27] - ' + Caption;
     Q.SQl.text := 'select ggDiffCol,ggDiffUc from users where ID=' + IntToStr(MainForm.xIDUtenteAttuale);
     Q.Open;
     RxSpinEdit3.value := Q.FieldByname('ggDiffCol').asInteger;
     RxSpinEdit2.value := Q.FieldByname('ggDiffUc').asInteger;
     Q.Close;

     //Grafica
     LegendaCandUtenteForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name; 
     Panel6.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel6.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel6.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel7.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel7.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel7.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
end;
//[/TONI20020905\]

procedure TLegendaCandUtenteForm.FormClose(Sender: TObject;
     var Action: TCloseAction);
begin
     with Data do begin
          DB.BeginTrans;
          try
               Q1.Close;
               {                Q1.SQL.text:='update Users set ggDiffCol=:xggDiffCol,ggDiffUc=:xggDiffUc where ID='+IntToStr(MainForm.xIDUtenteAttuale);
                               Q1.ParamByName('xggDiffCol').asInteger:=RxSpinEdit3.AsInteger;
                               Q1.ParamByName('xggDiffUc').asInteger:=RxSpinEdit2.AsInteger;}
               Q1.SQL.text := 'update Users set ggDiffCol=:xggDiffCol:,ggDiffUc=:xggDiffUc: where ID=' + IntToStr(MainForm.xIDUtenteAttuale);
               Q1.ParamByName['xggDiffCol'] := RxSpinEdit3.AsInteger;
               Q1.ParamByName['xggDiffUc'] := RxSpinEdit2.AsInteger;
               Q1.ExecSQL;
               DB.CommitTrans;
          except
               DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
          end;
     end;
end;
//[/TONI20020905\]FINE

procedure TLegendaCandUtenteForm.BChiudiClick(Sender: TObject);
begin
     ModalResult := mrOk;
end;

end.

