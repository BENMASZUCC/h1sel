unit EspLav;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     ExtCtrls, Grids, DBGrids, Db, DBTables, StdCtrls, Buttons, TB97, ADODB,
     U_ADOLinkCl;

type
     TEspLavForm = class(TForm)
          DsElencoAziende: TDataSource;
          Timer1: TTimer;
          DsAree: TDataSource;
          DsRuoli: TDataSource;
          Timer2: TTimer;
          DsSettori: TDataSource;
          TElencoAziende: TADOLinkedQuery;
          TSettoriLK: TADOLinkedTable;
          TSettori: TADOLinkedQuery;
          Q: TADOLinkedQuery;
          TRuoli: TADOLinkedQuery;
          TAree: TADOLinkedQuery;
          TElencoAziendeID: TAutoIncField;
          TElencoAziendeDescrizione: TStringField;
          TElencoAziendeStato: TStringField;
          TElencoAziendeIndirizzo: TStringField;
          TElencoAziendeCap: TStringField;
          TElencoAziendeComune: TStringField;
          TElencoAziendeProvincia: TStringField;
          TElencoAziendeIDAttivita: TIntegerField;
          TElencoAziendeTelefono: TStringField;
          TElencoAziendeFax: TStringField;
          TElencoAziendePartitaIVA: TStringField;
          TElencoAziendeCodiceFiscale: TStringField;
          TElencoAziendeBancaAppoggio: TStringField;
          TElencoAziendeSistemaPagamento: TStringField;
          TElencoAziendeNoteContratto: TMemoField;
          TElencoAziendeResponsabile: TStringField;
          TElencoAziendeConosciutoInData: TDateTimeField;
          TElencoAziendeIndirizzoLegale: TStringField;
          TElencoAziendeCapLegale: TStringField;
          TElencoAziendeComuneLegale: TStringField;
          TElencoAziendeProvinciaLegale: TStringField;
          TElencoAziendeCartellaDoc: TStringField;
          TElencoAziendeTipoStrada: TStringField;
          TElencoAziendeTipoStradaLegale: TStringField;
          TElencoAziendeNumCivico: TStringField;
          TElencoAziendeNumCivicoLegale: TStringField;
          TElencoAziendeNazioneAbb: TStringField;
          TElencoAziendeNazioneAbbLegale: TStringField;
          TElencoAziendeNote: TMemoField;
          TElencoAziendeFatturato: TFloatField;
          TElencoAziendeNumDipendenti: TSmallintField;
          TSettoriLKID: TAutoIncField;
          TSettoriLKAttivita: TStringField;
          TElencoAziendeAttivita: TStringField;
          TElencoAziendeSitoInternet: TStringField;
          TElencoAziendeDescAttivitaAzienda: TStringField;
          TElencoAziendeIDTipoAzienda: TIntegerField;
          Panel16: TPanel;
          Panel7: TPanel;
          BitBtn1: TToolbarButton97;
          BitBtn2: TToolbarButton97;
          RGOpzione: TRadioGroup;
          GroupBox1: TGroupBox;
          PanAziende: TPanel;
          DBGrid1: TDBGrid;
          Panel1: TPanel;
          BAziendaNew: TToolbarButton97;
          BAziendaDel: TToolbarButton97;
          BAziendaMod: TToolbarButton97;
          Panel3: TPanel;
          Panel6: TPanel;
          Label1: TLabel;
          EAzienda: TEdit;
          CBTestoCont: TCheckBox;
          GroupBox2: TGroupBox;
          PanSettori: TPanel;
          Panel4: TPanel;
          DBGrid4: TDBGrid;
          Panel5: TPanel;
          ToolbarButton971: TToolbarButton97;
          BSettoriITA: TToolbarButton97;
          BSettoriENG: TToolbarButton97;
          Panel13: TPanel;
          Label2: TLabel;
          ESettori: TEdit;
          GroupBox3: TGroupBox;
          Panel8: TPanel;
          DBGrid2: TDBGrid;
          Panel73: TPanel;
          Panel14: TPanel;
          Label3: TLabel;
          EAree: TEdit;
          Panel19: TPanel;
          CBInsComp: TCheckBox;
          Panel10: TPanel;
          DBGrid3: TDBGrid;
          Panel2: TPanel;
          Panel15: TPanel;
          Label4: TLabel;
          ERuoli: TEdit;
          Panel11: TPanel;
          BAreeRuoliITA: TToolbarButton97;
          BAreeRuoliENG: TToolbarButton97;
          TElencoAziendeDescrizione_1: TStringField;
          procedure Timer1Timer(Sender: TObject);
          procedure BAziendaDelClick(Sender: TObject);
          procedure BAziendaOKClick(Sender: TObject);
          procedure BAziendaCanClick(Sender: TObject);
          procedure BAziendaNewClick(Sender: TObject);
          procedure BAziendaModClick(Sender: TObject);
          procedure Timer2Timer(Sender: TObject);
          procedure RGOpzioneClick(Sender: TObject);
          procedure ToolbarButton971Click(Sender: TObject);
          procedure EAziendaChange(Sender: TObject);
          procedure FormShow(Sender: TObject);
          procedure CBTestoContClick(Sender: TObject);
          procedure BSettoriITAClick(Sender: TObject);
          procedure BSettoriENGClick(Sender: TObject);
          procedure BAreeRuoliITAClick(Sender: TObject);
          procedure BAreeRuoliENGClick(Sender: TObject);
          procedure BitBtn1Click(Sender: TObject);
          procedure BitBtn2Click(Sender: TObject);
          procedure ESettoriChange(Sender: TObject);
          procedure ERuoliChange(Sender: TObject);
          procedure EAreeChange(Sender: TObject);
     private
          xStringa, xStringa2: string;
     public
          { Public declarations }
     end;

var
     EspLavForm: TEspLavForm;

implementation

uses Azienda, ModuloDati, Main;

{$R *.DFM}

procedure TEspLavForm.Timer1Timer(Sender: TObject);
begin
     xStringa := '';
end;

procedure TEspLavForm.BAziendaDelClick(Sender: TObject);
begin
     if not TElencoAziende.IsEmpty then begin
          // controllo eventuali associazioni
          // SOGGETTI
          Q.Close;
          //[/TONI20020726\]
          {          Q.SQL.text:='select count(*) Tot from EsperienzeLavorative '+
                         'where IDAzienda='+TElencoAziendeID.asString;}
          Q.SQL.text := 'select count(*) Tot from EsperienzeLavorative ' +
               'where IDAzienda=' + TElencoAziende.FieldByName('ID').asString;
          Q.Open;
          if Q.FieldByName('Tot').asInteger > 0 then begin
               MessageDlg('Esistono candidati associati all''azienda.' + chr(13) +
                    'IMPOSSIBILE CANCELLARE', mtError, [mbOK], 0);
               Exit;
          end;
          // COMMESSE
          Q.Close;
          {          Q.SQL.text:='select count(*) Tot from EBC_Ricerche '+
                         'where IDCliente='+TElencoAziendeID.asString;}
          Q.SQL.text := 'select count(*) Tot from EBC_Ricerche ' +
               'where IDCliente=' + TElencoAziende.FieldByName('ID').asString;
          Q.Open;
          if Q.FieldByName('Tot').asInteger > 0 then begin
               MessageDlg('Esistono commesse/ricerche associate all''azienda.' + chr(13) +
                    'IMPOSSIBILE CANCELLARE', mtError, [mbOK], 0);
               Exit;
          end;
          // OFFERTE
          Q.Close;
          {          Q.SQL.text:='select count(*) Tot from EBC_Offerte '+
                         'where IDCliente='+TElencoAziendeID.asString;}
          Q.SQL.text := 'select count(*) Tot from EBC_Offerte ' +
               'where IDCliente=' + TElencoAziende.FieldByName('ID').asString;
          Q.Open;
          if Q.FieldByName('Tot').asInteger > 0 then begin
               MessageDlg('Esistono offerte associate all''azienda.' + chr(13) +
                    'IMPOSSIBILE CANCELLARE', mtError, [mbOK], 0);
               Exit;
          end;
          // Contatti fatti
          Q.Close;
          {          Q.SQL.text:='select count(*) Tot from EBC_ContattiFatti '+
                         'where IDCliente='+TElencoAziendeID.asString;}
          Q.SQL.text := 'select count(*) Tot from EBC_ContattiFatti ' +
               'where IDCliente=' + TElencoAziende.FieldByName('ID').asString;
          Q.Open;
          if Q.FieldByName('Tot').asInteger > 0 then begin
               MessageDlg('Esistono contatti avuti con l''azienda.' + chr(13) +
                    'IMPOSSIBILE CANCELLARE', mtError, [mbOK], 0);
               Exit;
          end;

          if MessageDlg('Sei DAVVERO sicuro di voler cancellare l''Azienda ?', mtWarning, [mbYes, mbNo], 0) = mrYes then begin
               with Data do begin
                    //                    DB.BeginTrans;
                    DB.BeginTrans;
                    try
                         Q.Close;
                         //                         Q.SQL.text:='delete from EBC_Clienti where ID='+TElencoAziendeID.asString;
                         Q.SQL.text := 'delete from EBC_Clienti where ID=' + TElencoAziende.FieldByname('ID').asString;
                         Q.ExecSQL;
                         //                         DB.CommitTrans;
                         DB.CommitTrans;
                    except
                         //                         DB.RollbackTrans;
                         DB.RollbackTrans;
                         MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
                    end;
               end;
               EAziendaChange(self);
          end;
     end;
     //[/TONI20020726\]FINE
end;

procedure TEspLavForm.BAziendaOKClick(Sender: TObject);
begin
     TElencoAziende.Post;
end;

procedure TEspLavForm.BAziendaCanClick(Sender: TObject);
begin
     TElencoAziende.Cancel;
end;

procedure TEspLavForm.BAziendaNewClick(Sender: TObject);
begin
     AziendaForm := TAziendaForm.create(self);
     AziendaForm.xUltimaProv := MainForm.xUltimaProv;
     AziendaForm.ShowModal;
     if AziendaForm.ModalResult = mrOK then begin
          //[/TONI20020726\]
          //          Data.DB.BeginTrans;
          Data.DB.BeginTrans;
          try
               if AziendaForm.RG1.ItemIndex = 0 then begin
                    Q.SQL.text := 'insert into EBC_Clienti (Descrizione,TipoStrada,Indirizzo,NumCivico,Comune,Cap,Provincia,Telefono,Fax,SitoInternet,DescAttivitaAzienda,Fatturato,NumDipendenti,NazioneAbb,Stato,IDAttivita,IDTipoAzienda) ' +
                         'values (:xDescrizione:,:xTipoStrada:,:xIndirizzo:,:xNumCivico:,:xComune:,:xCap:,:xProvincia:,:xTelefono:,:xFax:,:xSitoInternet:,:xDescAttivitaAzienda:,:xFatturato:,:xNumDipendenti:,:xNazioneAbb:,:xStato:,:xIDAttivita:,:xIDTipoAzienda:)';
                    Q.ParamByName['xDescrizione'] := trimright(AziendaForm.EDescrizione.Text);
                    Q.ParamByName['xTipoStrada'] := AziendaForm.CBTipoStrada.Text;
                    Q.ParamByName['xIndirizzo'] := AziendaForm.EIndirizzo.text;
                    Q.ParamByName['xNumCivico'] := AziendaForm.ENumCivico.text;
                    Q.ParamByName['xComune'] := AziendaForm.EComune.Text;
                    Q.ParamByName['xCap'] := AziendaForm.ECap.Text;
                    Q.ParamByName['xProvincia'] := AziendaForm.EProv.Text;
                    Q.ParamByName['xTelefono'] := AziendaForm.ETelefono.Text;
                    Q.ParamByName['xFax'] := AziendaForm.EFax.Text;
                    Q.ParamByName['xSitoInternet'] := AziendaForm.ESitoInternet.Text;
                    Q.ParamByName['xDescAttivitaAzienda'] := AziendaForm.EDescAttAzienda.Text;
                    Q.ParamByName['xFatturato'] := AziendaForm.FEFatturato.Value;
                    Q.ParamByName['xNumDipendenti'] := StrToIntDef(AziendaForm.FENumDip.Text, 0);
                    Q.ParamByName['xNazioneAbb'] := AziendaForm.ENazione.text;
                    Q.ParamByName['xStato'] := AziendaForm.cbStato.Text; //'attivo';
                    Q.ParamByName['xIDAttivita'] := 0;
                    Q.ParamByName['xIDTipoAzienda'] := AziendaForm.qTipiAziendaID.AsInteger; //4;
               end else begin
                    Q.SQL.text := 'insert into EBC_Clienti (Descrizione,TipoStrada,Indirizzo,NumCivico,Comune,Cap,Provincia,Telefono,Fax,SitoInternet,DescAttivitaAzienda,Fatturato,NumDipendenti,NazioneAbb,Stato,IDAttivita,IDTipoAzienda) ' +
                         'values (:xDescrizione:,:xTipoStrada:,:xIndirizzo:,:xNumCivico:,:xComune:,:xCap:,:xProvincia:,:xTelefono:,:xFax:,:xSitoInternet:,:xDescAttivitaAzienda:,:xFatturato:,:xNumDipendenti:,:xNazioneAbb:,:xStato:,:xIDAttivita:,:xIDTipoAzienda:)';
                    Q.ParamByName['xDescrizione'] := TrimRight(AziendaForm.EDescrizione.Text);
                    Q.ParamByName['xTipoStrada'] := AziendaForm.CBTipoStrada.Text;
                    Q.ParamByName['xIndirizzo'] := AziendaForm.EIndirizzo.text;
                    Q.ParamByName['xNumCivico'] := AziendaForm.ENumCivico.text;
                    Q.ParamByName['xComune'] := AziendaForm.EComune.Text;
                    Q.ParamByName['xCap'] := AziendaForm.ECap.Text;
                    Q.ParamByName['xProvincia'] := AziendaForm.EProv.Text;
                    Q.ParamByName['xTelefono'] := AziendaForm.ETelefono.Text;
                    Q.ParamByName['xFax'] := AziendaForm.EFax.Text;
                    Q.ParamByName['xSitoInternet'] := AziendaForm.ESitoInternet.Text;
                    Q.ParamByName['xDescAttivitaAzienda'] := AziendaForm.EDescAttAzienda.Text;
                    Q.ParamByName['xFatturato'] := AziendaForm.FEFatturato.Value;
                    Q.ParamByName['xNumDipendenti'] := StrToIntDef(AziendaForm.FENumDip.Text, 0);
                    Q.ParamByName['xNazioneAbb'] := AziendaForm.ENazione.text;
                    Q.ParamByName['xStato'] := AziendaForm.cbStato.Text;
                    Q.ParamByName['xIDAttivita'] := AziendaForm.TSettori.FieldByName('ID').Value;
                    Q.ParamByName['xIDTipoAzienda'] := AziendaForm.qTipiAziendaID.AsInteger;
               end;
               Q.ExecSQL;
               Data.DB.CommitTrans;
          except
               Data.DB.RollbackTrans;
               MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
          end;
          EAzienda.Text := AziendaForm.EDescrizione.Text;
          EAziendaChange(self);
     end;
     AziendaForm.Free;
end;

procedure TEspLavForm.BAziendaModClick(Sender: TObject);
begin
     AziendaForm := TAziendaForm.create(self);
     if AziendaForm.TSettori.Active = false then AziendaForm.TSettori.Open;
     AziendaForm.EDescrizione.Text := TElencoAziende.FieldByName('Descrizione').asString;
     AziendaForm.CBTipoStrada.text := TElencoAziende.FieldByName('TipoStrada').asString;
     AziendaForm.EIndirizzo.Text := TElencoAziende.FieldByName('Indirizzo').asString;
     AziendaForm.ENumCivico.text := TElencoAziende.FieldByName('NumCivico').asString;
     AziendaForm.ECap.Text := TElencoAziende.FieldByName('Cap').asString;
     AziendaForm.EComune.Text := TElencoAziende.FieldByName('Comune').asString;
     AziendaForm.EProv.Text := TElencoAziende.FieldBYName('Provincia').asString;
     AziendaForm.ENazione.Text := TElencoAziende.FieldByName('NazioneAbb').asString;
     AziendaForm.ETelefono.Text := TElencoAziende.FieldByName('Telefono').asString;
     AziendaForm.ESitoInternet.Text := TElencoAziende.FieldByName('SitoInternet').asString;
     AziendaForm.EFax.Text := TElencoAziende.FieldByName('Fax').asString;
     AziendaForm.FEFatturato.Text := TElencoAziende.FieldByNAme('Fatturato').asString;
     AziendaForm.EDescAttAzienda.Text := TElencoAziende.FieldByName('DescAttivitaAzienda').asString;
     AziendaForm.FENumDip.Text := TElencoAziende.FieldByName('NumDipendenti').asString;
     AziendaForm.cbStato.Text := TElencoAziende.FieldByName('Stato').AsString;
     AziendaForm.qTipiAzienda.locate('ID', TElencoAziende.FieldByNAme('IDTipoAzienda').Value, []);


     //     if TElencoAziendeIDAttivita.asString<>'' then begin
     if TElencoAziende.FieldByName('IDAttivita').asString <> '' then begin
          //          AziendaForm.TSettori.FindKey([TElencoAziendeIDAttivita.Value]);
          AziendaForm.TSettori.locate('ID', TElencoAziende.FieldByNAme('IDAttivita').Value, []);
     end else AziendaForm.RG1.Itemindex := 0;
     AziendaForm.ShowModal;
     if AziendaForm.ModalResult = mrOK then begin

          with Data do begin
               //               DB.BeginTrans;
               DB.Begintrans;
               try
                    Q.Close;
                    {                    Q.SQL.text:='update EBC_Clienti set Descrizione=:xDescrizione,TipoStrada=:xTipoStrada,Indirizzo=:xIndirizzo,NumCivico=:xNumCivico, '+
                                             'Comune=:xComune,Cap=:xCap,Provincia=:xProvincia,Fatturato=:xFatturato,Telefono=:xTelefono,Fax=:xFax,SitoInternet=:xSitoInternet, '+
                                             'NumDipendenti=:xNumDipendenti,IDAttivita=:xIDAttivita,NazioneAbb=:xNazioneAbb,DescAttivitaAzienda=:xDescAttivitaAzienda '+
                                             'where ID='+TElencoAziendeID.asString;
                                        Q.ParamByName('xDescrizione').asString:=AziendaForm.EDescrizione.Text;
                                        Q.ParamByName('xTipoStrada').asString:=AziendaForm.CBTipoStrada.Text;
                                        Q.ParamByName('xIndirizzo').asString:=AziendaForm.EIndirizzo.text;
                                        Q.ParamByName('xNumCivico').asString:=AziendaForm.ENumCivico.text;
                                        Q.ParamByName('xComune').asString:=AziendaForm.EComune.Text;
                                        Q.ParamByName('xCap').asString:=AziendaForm.ECap.Text;
                                        Q.ParamByName('xProvincia').asString:=AziendaForm.EProv.Text;
                                        Q.ParamByName('xTelefono').asString:=AziendaForm.ETelefono.Text;
                                        Q.ParamByName('xFax').asString:=AziendaForm.EFax.Text;
                                        Q.ParamByName('xSitoInternet').asString:=AziendaForm.ESitoInternet.Text;
                                        Q.ParamByName('xDescAttivitaAzienda').asString:=AziendaForm.EDescAttAzienda.Text;
                                        if AziendaForm.FEFatturato.Text<>'' then
                                             Q.ParamByName('xFatturato').asFloat:=StrToFloat(AziendaForm.FEFatturato.Text)
                                        else Q.ParamByName('xFatturato').asFloat:=0;
                                        Q.ParamByName('xNumDipendenti').asInteger:=StrToIntDef(AziendaForm.FENumDip.Text,0);
                                        if AziendaForm.RG1.ItemIndex=0 then
                                             Q.ParamByName('xIDAttivita').asInteger:=0
                                        else Q.ParamByName('xIDAttivita').asInteger:=AziendaForm.TSettoriID.Value;
                                        Q.ParamByName('xNazioneAbb').asString:=AziendaForm.ENazione.Text;}
                    Q.SQL.text := 'update EBC_Clienti set Descrizione=:xDescrizione:,TipoStrada=:xTipoStrada:,Indirizzo=:xIndirizzo:,NumCivico=:xNumCivico:, ' +
                         'Comune=:xComune:,Cap=:xCap:,Provincia=:xProvincia:,Fatturato=:xFatturato:,Telefono=:xTelefono:,Fax=:xFax:,SitoInternet=:xSitoInternet:, ' +
                         'NumDipendenti=:xNumDipendenti:,IDAttivita=:xIDAttivita:,NazioneAbb=:xNazioneAbb:,DescAttivitaAzienda=:xDescAttivitaAzienda: ' +
                         'where ID=' + TElencoAziende.FieldByName('ID').asString;
                    //Q.ParamByName propria!!! Vedi Sorgenti TADOLInkedQuery
                    Q.ParamByName['xDescrizione'] := AziendaForm.EDescrizione.Text;
                    Q.ParamByName['xTipoStrada'] := AziendaForm.CBTipoStrada.Text;
                    Q.ParamByName['xIndirizzo'] := AziendaForm.EIndirizzo.text;
                    Q.ParamByName['xNumCivico'] := AziendaForm.ENumCivico.text;
                    Q.ParamByName['xComune'] := AziendaForm.EComune.Text;
                    Q.ParamByName['xCap'] := AziendaForm.ECap.Text;
                    Q.ParamByname['xProvincia'] := AziendaForm.EProv.Text;
                    Q.ParamByname['xTelefono'] := AziendaForm.ETelefono.Text;
                    Q.ParamByname['xFax'] := AziendaForm.EFax.Text;
                    Q.ParamByname['xSitoInternet'] := AziendaForm.ESitoInternet.Text;
                    Q.ParamByname['xDescAttivitaAzienda'] := AziendaForm.EDescAttAzienda.Text;
                    Q.ParamByname['xStato'] := AziendaForm.cbStato.Text;
                    Q.ParamByname['xIDTipoAzienda'] := AziendaForm.qTipiAziendaID.Value;
                    if AziendaForm.FEFatturato.Text <> '' then
                         Q.ParamByname['xFatturato'] := StrToFloat(AziendaForm.FEFatturato.Text)
                    else Q.ParamByname['xFatturato'] := 0;
                    Q.ParamByname['xNumDipendenti'] := StrToIntDef(AziendaForm.FENumDip.Text, 0);
                    if AziendaForm.RG1.ItemIndex = 0 then
                         Q.ParamByname['xIDAttivita'] := 0
                    else Q.ParamByname['xIDAttivita'] := AziendaForm.TSettori.FieldByName('ID').Value;
                    Q.ParamByname['xNazioneAbb'] := AziendaForm.ENazione.Text;

                    Q.ExecSQL;
                    //                    DB.CommitTrans;
                    DB.CommitTrans;
               except
                    //                    DB.RollbackTrans;
                    DB.RollbackTrans;
                    MessageDlg('ERRORE SUL DATABASE: modifiche non effettuate', mtError, [mbOK], 0);
               end;
          end;
          EAziendaChange(self);
     end;
     AziendaForm.Free;
end;

procedure TEspLavForm.Timer2Timer(Sender: TObject);
begin
     xStringa2 := '';
end;

procedure TEspLavForm.RGOpzioneClick(Sender: TObject);
begin
     case RGOpzione.ItemIndex of
          0: begin
                    //PanAziende.Visible := False;
                    //PanSettori.Visible := True;
                    GroupBox1.Visible := False;
                    GroupBox2.Visible := True;
               end;
          1: begin
                    //PanAziende.Visible := True;
                    //PanSettori.Visible := False;
                    GroupBox1.Visible := True;
                    GroupBox2.Visible := False;
               end;
          2: begin
                    //PanAziende.Visible := False;
                    //PanSettori.Visible := False;
                    GroupBox1.Visible := False;
                    GroupBox2.Visible := False;
               end;
     end;
end;

procedure TEspLavForm.ToolbarButton971Click(Sender: TObject);
var xSettore: string;
begin
     //fede: tolto pulsante perch� da errore
     // i settori si inseriscono dalla sezione tabelle
     if not MainForm.CheckProfile('77') then Exit;
     if InputQuery('Nuovo settore', 'nome settore:', xSettore) then
          TSettori.InsertRecord([xSettore]);

end;

procedure TEspLavForm.EAziendaChange(Sender: TObject);
begin
     TElencoAziende.Close;
     TElencoAziende.SQL.text := 'select ec.Descrizione,t.Descrizione Descrizione_1,* from EBC_Clienti ec left join TabRegoleCliente t   ' +
          ' on ec.idregolacliente = t.ID ';
     if CBTestoCont.Checked then
          //[/TONI20020726\]
          {          TElencoAziende.ParamByName('xDesc').asString:='%'+EAzienda.text+'%'
               else TElencoAziende.ParamByName('xDesc').asString:=EAzienda.text+'%';
          TElencoAziende.SQL.add('where Descrizione like ''%'+EAzienda.text+'%''')
     else TElencoAziende.SQL.add('where Descrizione like '''+EAzienda.text+'%''');  }
          TElencoAziende.SQL.add('where ec.Descrizione like ''%' + StringReplace(EAzienda.Text, '''', '''''', [rfreplaceall]) + '%''')
     else TElencoAziende.SQL.add('where ec.Descrizione like ''' + StringReplace(EAzienda.Text, '''', '''''', [rfreplaceall]) + '%''');
     //[/TONI20020726\]TONI
     TElencoAziende.Open;
end;

procedure TEspLavForm.FormShow(Sender: TObject);
begin
     //Grafica
     EspLavForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     PanSettori.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     PanSettori.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     PanSettori.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     PanAziende.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     PanAziende.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     PanAziende.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel2.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel2.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel2.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel3.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel3.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel3.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel4.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel4.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel4.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel5.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel5.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel5.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel6.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel6.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel6.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel7.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel7.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel7.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel73.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel73.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel73.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     Panel8.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel8.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel8.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel10.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel10.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel10.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel11.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel11.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel11.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel13.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel13.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel13.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel14.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel14.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel14.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel15.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel15.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel15.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel16.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel16.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel16.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
     Panel19.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel19.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel19.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;

     Caption := '[M/05140] - ' + Caption;
     if (Uppercase(copy(Data.Global.FieldByName('NomeAzienda').Value, 1, 7)) = 'FERRARI') or
          (Uppercase(copy(Data.Global.FieldByName('NomeAzienda').Value, 1, 3)) = 'EBC') then begin
          BSettoriITA.visible := True;
          BSettoriENG.visible := True;
          BAreeRuoliITA.visible := True;
          BAreeRuoliENG.visible := True;
     end;
     if (Uppercase(copy(Data.Global.FieldByName('NomeAzienda').Value, 1, 8)) = 'PROPOSTE') then begin
          RGOpzione.ItemIndex := 1;
          CBTestoCont.Checked := false;
     end;

     GroupBox1.Visible := False;
     GroupBox2.Visible := True;
     RGOpzione.ItemIndex := 0;

     if pos('INTERMEDIA', Uppercase(Data.Global.FieldByName('NomeAzienda').Value)) > 0 then begin
          CBInsComp.Checked := false;
          DBGrid1.Columns[1].Visible := true;
          DBGrid1.Columns[1].Width := 50;
     end;

end;

procedure TEspLavForm.CBTestoContClick(Sender: TObject);
begin
     EAziendaChange(nil);
     {if CBTestoCont.Checked then begin
          if EAzienda.text<>'' then begin
               TElencoAziende.Close;
               //[/TONI20020726\]
               TElencoAziende.ReloadSQL;
               //               TElencoAziende.ParamByName('xDesc').asString:='%'+EAzienda.text+'%';
               TElencoAziende.ParamByName['xDesc']:='%'+EAzienda.text+'%';
               //[/TONI20020726\]FINE
               TElencoAziende.Open;

          end;
     end else begin
          if EAzienda.text<>'' then
               EAziendaChange(nil);
     end; }

end;

procedure TEspLavForm.BSettoriITAClick(Sender: TObject);
begin
     TSettori.Close;
     TSettori.SQL.text := 'select * from EBC_Attivita order by Attivita';
     DBGrid4.Columns[0].FieldName := 'Attivita';
     TSettori.Open;
end;

procedure TEspLavForm.BSettoriENGClick(Sender: TObject);
begin
     TSettori.Close;
     TSettori.SQL.text := 'select * from EBC_Attivita order by Attivita_ENG';
     DBGrid4.Columns[0].FieldName := 'Attivita_ENG';
     TSettori.Open;
end;

procedure TEspLavForm.BAreeRuoliITAClick(Sender: TObject);
begin
     DBGrid2.Columns[0].FieldName := 'Descrizione';
     DBGrid3.Columns[0].FieldName := 'Descrizione';
end;

procedure TEspLavForm.BAreeRuoliENGClick(Sender: TObject);
begin
     DBGrid2.Columns[0].FieldName := 'Descrizione_ENG';
     DBGrid3.Columns[0].FieldName := 'Descrizione_ENG';
end;

procedure TEspLavForm.BitBtn1Click(Sender: TObject);
begin
     EspLavForm.ModalResult := mrOk;
end;

procedure TEspLavForm.BitBtn2Click(Sender: TObject);
begin
     EspLavForm.ModalResult := mrCancel;
end;

procedure TEspLavForm.ESettoriChange(Sender: TObject);
begin
     TSettori.Close;
     TSettori.SQL.text := 'select EBC_Attivita.*, AreaSettore from EBC_Attivita' +
          ' left join AreeSettori' +
          ' on EBC_Attivita.IDAreaSettore = AreeSettori.ID' +
          ' where EBC_Attivita.Attivita like ''%' + ESettori.Text + '%''' +
          ' order by EBC_Attivita.Attivita';
     TSettori.Open;
end;

procedure TEspLavForm.ERuoliChange(Sender: TObject);
begin
     if DBGrid3.Columns[0].FieldName = 'Descrizione' then begin
          TRuoli.Close;
          TRuoli.SQL.text := 'select ID,IDArea,Descrizione, Descrizione_ENG from Mansioni' +
               ' where Descrizione like ''%' + ERuoli.Text + '%''' +
               ' order by Descrizione';
          TRuoli.Open;
          if Truoli.fieldbyname('IDArea').asstring <> '' then begin
               TAree.Close;
               TAree.SQL.text := 'select * from Aree' +
                    ' where id = ' + Truoli.fieldbyname('IDArea').asstring +
                    ' order by Descrizione';
               TAree.Open;
          end else begin
               TAree.Close;
               TAree.SQL.text := 'select * from Aree' +
                    ' order by Descrizione';
               TAree.Open;
          end;
     end else begin
          TRuoli.Close;
          TRuoli.SQL.text := 'select ID,IDArea,Descrizione, Descrizione_ENG from Mansioni' +
               ' where Descrizione_ENG like ''%' + ERuoli.Text + '%''' +
               ' order by Descrizione_ENG';
          TRuoli.Open;
          if Truoli.fieldbyname('IDArea').asstring <> '' then begin
               TAree.Close;
               TAree.SQL.text := 'select * from Aree' +
                    ' where id = ' + Truoli.fieldbyname('IDArea').asstring +
                    ' order by Descrizione_ENG';
               TAree.Open;
          end else begin
               TAree.Close;
               TAree.SQL.text := 'select * from Aree' +
                    ' order by Descrizione_ENG';
               TAree.Open;
          end;
     end;
end;

procedure TEspLavForm.EAreeChange(Sender: TObject);
begin
     TAree.Close;
     if DBGrid2.Columns[0].FieldName = 'Descrizione' then
          TAree.SQL.text := 'select * from Aree' +
               ' where Descrizione like ''%' + EAree.Text + '%''' +
               ' order by Descrizione'
     else
          TAree.SQL.text := 'select * from Aree' +
               ' where Descrizione_ENG like ''%' + EAree.Text + '%''' +
               ' order by Descrizione_ENG';
     TAree.Open;
end;

end.

