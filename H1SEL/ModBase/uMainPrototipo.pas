unit uMainPrototipo;

interface

uses
     Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
     Db,DBTables,StdCtrls,Buttons,ExtCtrls,Grids,DBGrids,ComCtrls;

type
     TMainForm=class(TForm)
          PCMain: TPageControl;
          TSCandidati: TTabSheet;
          Panel1: TPanel;
          BitBtn1: TBitBtn;
          Panel2: TPanel;
          Panel3: TPanel;
          Label1: TLabel;
          Edit1: TEdit;
          DBGrid1: TDBGrid;
          SpeedButton1: TSpeedButton;
          TSAziende: TTabSheet;
          Panel5: TPanel;
          BitBtn2: TBitBtn;
          Panel6: TPanel;
          Panel7: TPanel;
          Label2: TLabel;
          SpeedButton2: TSpeedButton;
          Edit2: TEdit;
          DBGrid2: TDBGrid;
          Panel4: TPanel;
          TSCommesse: TTabSheet;
          Panel8: TPanel;
          BitBtn3: TBitBtn;
          procedure BitBtn1Click(Sender: TObject);
          procedure SpeedButton1Click(Sender: TObject);
          procedure Edit1KeyPress(Sender: TObject; var Key: Char);
          procedure SpeedButton2Click(Sender: TObject);
          procedure BitBtn2Click(Sender: TObject);
          procedure Edit2KeyPress(Sender: TObject; var Key: Char);
          procedure BitBtn3Click(Sender: TObject);
     private
          { Private declarations }
     public
          { Public declarations }
     end;

var
     MainForm: TMainForm;

implementation

uses uASACand,uData,uASAAzienda;

{$R *.DFM}

procedure TMainForm.BitBtn1Click(Sender: TObject);
begin
     if Data.TAnagrafica.IsEmpty then begin
          ShowMessage('SELEZIONARE UNO O PIU'' CANDIDATI');
          exit;
     end;
     ASASchedaCandForm.ShowModal;
end;

procedure TMainForm.SpeedButton1Click(Sender: TObject);
begin
     Data.TAnagrafica.Close;
     Data.TAnagrafica.ParamByName('xTesto').asString:=Edit1.text+'%';
     Data.TAnagrafica.Open;
end;

procedure TMainForm.Edit1KeyPress(Sender: TObject; var Key: Char);
begin
     if key=chr(13) then SpeedButton1Click(self);
end;

procedure TMainForm.SpeedButton2Click(Sender: TObject);
begin
     Data.QAziende.Close;
     Data.QAziende.ParamByName('xTesto').asString:=Edit2.text+'%';
     Data.QAziende.Open;
end;

procedure TMainForm.BitBtn2Click(Sender: TObject);
begin
     if Data.QAziende.IsEmpty then begin
          ShowMessage('SELEZIONARE UNA O PIU'' AZIENDE');
          exit;
     end;
     ASAAziendaForm.ShowModal;
end;

procedure TMainForm.Edit2KeyPress(Sender: TObject; var Key: Char);
begin
     if key=chr(13) then SpeedButton2Click(self);
end;

procedure TMainForm.BitBtn3Click(Sender: TObject);
begin
     ShowMessage('Under construction');
end;

end.

