unit InsNote;

interface

uses
     Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, Buttons, ExtCtrls, TB97;

type
     TInsNoteForm = class(TForm)
          Panel1: TPanel;
          Label1: TLabel;
          MNote: TMemo;
          ToolbarButton971: TToolbarButton97;
          BAnnulla: TToolbarButton97;
          procedure ToolbarButton971Click(Sender: TObject);
          procedure BAnnullaClick(Sender: TObject);
          procedure FormShow(Sender: TObject);
     private
    { Private declarations }
     public
    { Public declarations }
     end;

var
     InsNoteForm: TInsNoteForm;

implementation

uses Main;

{$R *.DFM}

procedure TInsNoteForm.ToolbarButton971Click(Sender: TObject);
begin
     InsNoteForm.ModalResult := mrOK;
end;

procedure TInsNoteForm.BAnnullaClick(Sender: TObject);
begin
     InsNoteForm.ModalResult := mrCancel;
end;

procedure TInsNoteForm.FormShow(Sender: TObject);
begin
//Grafica
     InsNoteForm.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.Color := MainForm.AdvToolBarOfficeStyler1.PageAppearance.Color;
     Panel1.font.color := MainForm.AdvToolBarOfficeStyler1.font.Color;
     Panel1.font.name := MainForm.AdvToolBarOfficeStyler1.font.name;
end;

end.

